﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models;
using Focus.Web.ViewModels;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.Web.Code
{
  public class JobWizardControl : UserControlBase
  {
    public delegate void GoToStepHandler(object sender, CommandEventArgs eventArgs);
    public event GoToStepHandler GoToStep;

    /// <summary>
    /// Raises the <see cref="GoToStep"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected virtual void OnGoToStep(CommandEventArgs eventArgs)
    {
      if (GoToStep != null)
        GoToStep(this, eventArgs);
    }

    /// <summary>
    /// Binds the screening preferences minimum stars drop down.
    /// </summary>
    protected void BindScreeningPreferencesMinimumStarsDropDown(DropDownList screeningPreferencesMinimumStarsDropDown)
    {
      screeningPreferencesMinimumStarsDropDown.Items.Clear();
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("5Stars.DropDown", "5 stars"), ScreeningPreferences.JobSeekersMustHave5StarMatch.ToString()));
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("4Stars.DropDown", "4 stars"), ScreeningPreferences.JobSeekersMustHave4StarMatch.ToString()));
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("3Stars.DropDown", "3 stars"), ScreeningPreferences.JobSeekersMustHave3StarMatch.ToString()));
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("2Stars.DropDown", "2 stars"), ScreeningPreferences.JobSeekersMustHave2StarMatch.ToString()));
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("1Star.DropDown", "1 star"), ScreeningPreferences.JobSeekersMustHave1StarMatch.ToString()));

      if (App.Settings.ResumeReferralApprovalsMinimumStars >= 1 && App.Settings.ResumeReferralApprovalsMinimumStars <= 5)
        screeningPreferencesMinimumStarsDropDown.SelectedIndex = 5 - App.Settings.ResumeReferralApprovalsMinimumStars;
      else
        screeningPreferencesMinimumStarsDropDown.SelectedIndex = 2;
    }

    /// <summary>
    /// Binds the screening preferences minimum stars to apply drop down.
    /// </summary>
    protected void BindScreeningPreferencesMinimumStarsToApplyDropDown(DropDownList screeningPreferencesMinimumStarsDropDown)
    {
      screeningPreferencesMinimumStarsDropDown.Items.Clear();
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("5StarsToApply.DropDown", "5 stars"), ScreeningPreferences.JobSeekersMustHave5StarMatchToApply.ToString()));
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("4StarsToApply.DropDown", "4 stars"), ScreeningPreferences.JobSeekersMustHave4StarMatchToApply.ToString()));
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("3StarsToApply.DropDown", "3 stars"), ScreeningPreferences.JobSeekersMustHave3StarMatchToApply.ToString()));
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("2StarsToApply.DropDown", "2 stars"), ScreeningPreferences.JobSeekersMustHave2StarMatchToApply.ToString()));
      screeningPreferencesMinimumStarsDropDown.Items.Add(new ListItem(CodeLocalise("1StarToApply.DropDown", "1 star"), ScreeningPreferences.JobSeekersMustHave1StarMatchToApply.ToString()));

      var index = 11 - (int) App.Settings.ScreeningPrefExclusiveToMinStarMatchDefault;
      if (index >= 0 && index <= 4)
        screeningPreferencesMinimumStarsDropDown.SelectedIndex = index;
      else
        screeningPreferencesMinimumStarsDropDown.SelectedIndex = 2;
    }

    /// <summary>
    /// Binds the screening preferences controls for the step
    /// </summary>
    /// <param name="model">The job model</param>
    /// <param name="unqualifiedButton">The 'Allow unqualified applictions' radio button</param>
    /// <param name="mustBeScreenedButton">The 'Staff must screen' radio button</param>
    /// <param name="minimumStarMatchButton">The 'Minimum star approval' radio button</param>
    /// <param name="screeningPreferencesMinimumStarsDropDown">The 'Minimum star approval' drop-down</param>
    /// <param name="jobSeekersMustHaveMinimumStarMatchRow">The row holding the 'Minimum star approval' radio button</param>
    /// <param name="jobSeekersMinimumStarsToApplyRadioButton">The 'Minimum star to apply' radio button</param>
    /// <param name="screeningPreferencesMinimumStarsToApplyDropDown">The 'Minimum star to apply' drop-down</param>
    /// <param name="jobSeekersMinimumStarsToApplyRow">The row holding the 'Minimum star to apply' radio button</param>
    /// <param name="allowUnqualifiedApplicationsRow">The row holding the 'Allow unqualified applictions' radio button</param>
    protected void BindStepScreeningPreferences(
      JobWizardViewModel model, 
      RadioButton unqualifiedButton, 
      RadioButton mustBeScreenedButton, 
      RadioButton minimumStarMatchButton, 
      DropDownList screeningPreferencesMinimumStarsDropDown,
      HtmlTableRow jobSeekersMustHaveMinimumStarMatchRow,
      RadioButton jobSeekersMinimumStarsToApplyRadioButton,
      DropDownList screeningPreferencesMinimumStarsToApplyDropDown,
      HtmlTableRow jobSeekersMinimumStarsToApplyRow, // TO DO
      HtmlTableRow allowUnqualifiedApplicationsRow)
    {
      unqualifiedButton.Checked =
        mustBeScreenedButton.Checked =
        minimumStarMatchButton.Checked =
        jobSeekersMinimumStarsToApplyRadioButton.Checked = false;

      if (!App.Settings.ScreeningPrefExclusiveToMinStarMatch)
        jobSeekersMinimumStarsToApplyRow.Visible = false;

      if (!App.Settings.ScreeningPrefBelowStarMatchApproval || App.Settings.Theme == FocusThemes.Education)
        jobSeekersMustHaveMinimumStarMatchRow.Visible = false;

      ScreeningPreferences? currentPreferences;
      if (model.Job.ScreeningPreferences.IsNotNull())
      {
        currentPreferences = model.Job.ScreeningPreferences;
      }
      else
      {
        switch (App.Settings.ResumeReferralApprovalsMinimumStars)
        {
          case 0:
            currentPreferences = ScreeningPreferences.AllowUnqualifiedApplications;
            break;
          case 1:
            currentPreferences = ScreeningPreferences.JobSeekersMustHave1StarMatch;
            break;
          case 2:
            currentPreferences = ScreeningPreferences.JobSeekersMustHave2StarMatch;
            break;
          case 3:
            currentPreferences = ScreeningPreferences.JobSeekersMustHave3StarMatch;
            break;
          case 4:
            currentPreferences = ScreeningPreferences.JobSeekersMustHave4StarMatch;
            break;
          case 5:
            currentPreferences = ScreeningPreferences.JobSeekersMustHave5StarMatch;
            break;
          default:
            currentPreferences = ScreeningPreferences.JobSeekersMustBeScreened;
            break;
        }
      }

      switch (currentPreferences)
      {
        case ScreeningPreferences.JobSeekersMustBeScreened:
          mustBeScreenedButton.Checked = true;
          if (!App.Settings.EmployerPreferencesOverideSystemDefaults)
          {
            jobSeekersMustHaveMinimumStarMatchRow.Visible = false;
            allowUnqualifiedApplicationsRow.Visible = false;
          }
          break;

        case ScreeningPreferences.AllowUnqualifiedApplications:
          unqualifiedButton.Checked = true;
          if (!App.Settings.EmployerPreferencesOverideSystemDefaults)
            jobSeekersMustHaveMinimumStarMatchRow.Visible = false;

          break;

        case ScreeningPreferences.JobSeekersMustHave1StarMatchToApply:
        case ScreeningPreferences.JobSeekersMustHave2StarMatchToApply:
        case ScreeningPreferences.JobSeekersMustHave3StarMatchToApply:
        case ScreeningPreferences.JobSeekersMustHave4StarMatchToApply:
        case ScreeningPreferences.JobSeekersMustHave5StarMatchToApply:
          if (App.Settings.ScreeningPrefExclusiveToMinStarMatch)
          {
            jobSeekersMinimumStarsToApplyRadioButton.Checked = true;
            screeningPreferencesMinimumStarsToApplyDropDown.SelectedValue = currentPreferences.ToString();

            if (App.Settings.Theme == FocusThemes.Workforce && !App.Settings.EmployerPreferencesOverideSystemDefaults)
              allowUnqualifiedApplicationsRow.Visible = false;
          }
          else
          {
            unqualifiedButton.Checked = true;
          }
          break;

        default:
          if (App.Settings.Theme == FocusThemes.Workforce && App.Settings.ScreeningPrefBelowStarMatchApproval)
          {
            minimumStarMatchButton.Checked = true;
            screeningPreferencesMinimumStarsDropDown.SelectedValue = currentPreferences.ToString();

            if (!App.Settings.EmployerPreferencesOverideSystemDefaults)
              allowUnqualifiedApplicationsRow.Visible = false;
          }
          else
          {
            unqualifiedButton.Checked = true;
          }
          break;
      }
    }

    /// <summary>
    /// Gets the expiry date for a job based on the first posting date and the app setting
    /// </summary>
    /// <param name="app">The application.</param>
    /// <param name="jobPostedDate">Data job was first posted</param>
    /// <param name="isFederalContractorJob">If the job is a federal contractor job</param>
    /// <param name="isForeignLaborH2AJob">If the job is a Foreign Labor H2A job</param>
    /// <param name="isForeignLaborH2BJob">If the job is a Foreign Labor H2B job</param>
    /// <param name="isForeignLaborOtherJob">If the job is a Foreign Labor other job</param>
    /// <returns>
    /// The maximum allowed job closing date
    /// </returns>
    public static DateTime GetJobExpiryDate(IApp app, DateTime? jobPostedDate, bool isFederalContractorJob = false, bool isForeignLaborH2AJob = false,
      bool isForeignLaborH2BJob = false, bool isForeignLaborOtherJob = false)
    {
      var daysForJobPostingLifetime = app.Settings.DaysForJobPostingLifetime;

      if (isFederalContractorJob)
        daysForJobPostingLifetime = app.Settings.DaysForFederalContractorLifetime;
      else if (isForeignLaborH2AJob)
        daysForJobPostingLifetime = app.Settings.DaysForForeignLaborH2ALifetime;
      else if (isForeignLaborH2BJob)
        daysForJobPostingLifetime = app.Settings.DaysForForeignLaborH2BLifetime;
      else if (isForeignLaborOtherJob)
        daysForJobPostingLifetime = app.Settings.DaysForForeignLaborOtherLifetime;

      if (daysForJobPostingLifetime > 0)
      {
        return jobPostedDate.IsNull()
          ? DateTime.Today.AddDays(daysForJobPostingLifetime)
          : jobPostedDate.Value.Date.AddDays(daysForJobPostingLifetime);
      }

      return DateTime.MaxValue;
    }

    /// <summary>
    /// Builds the main work location.
    /// </summary>
    /// <param name="address">The address.</param>
    /// <returns></returns>
    protected JobLocationDto BuildWorkLocation(BusinessUnitAddressDto address)
    {
      var workLocation = "";

      var mainSiteLocation = new JobLocationDto();

      if (address.IsNotNull())
      {
        var city = address.TownCity;
        var state = "";
        var county = "";
        var postalCode = address.PostcodeZip;

        mainSiteLocation.City = city;
        mainSiteLocation.Zip = postalCode;

        // Get the state as the right part of the state key. i.e. State.KY = KY
        if (address.StateId.IsNotNull() && address.StateId > 0)
        {
          var stateLookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Id == address.StateId);
          if (stateLookup.IsNotNull())
          {
            state = stateLookup.Key.Replace("State.", "");
            mainSiteLocation.State = stateLookup.Text;
          }
        }

        if (App.Settings.CountyEnabled)
        {
          if (address.CountyId.IsNotNullOrZero())
          {
            var countyLookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties).FirstOrDefault(x => x.Id == address.CountyId);
            if (countyLookup.IsNotNull())
            {
              county = countyLookup.Text;
              mainSiteLocation.County = county;
            }
          }
          else
          {
            // Attempt to get the county from postal code
            var location = ServiceClientLocator.CoreClient(App).EstablishLocation(postalCode, null, null);
            if (location.IsNotNull() && location.StateKey.Replace("State.", "") == state)
            {
              county = location.CountyName;
              mainSiteLocation.County = location.CountyName;
            }
          }
        }

        // If we don't have all three parts then get them based on the postal code or city + state location search
        if (city.IsNullOrEmpty() || state.IsNullOrEmpty() || postalCode.IsNullOrEmpty())
        {
          PostalCodeViewDto location = null;

          if (postalCode.IsNotNullOrEmpty())
            location = ServiceClientLocator.CoreClient(App).EstablishLocation(postalCode, null, null);
          else if (city.IsNotNullOrEmpty() && state.IsNotNullOrEmpty())
            location = ServiceClientLocator.CoreClient(App).EstablishLocation(null, city, state);

          if (location != null)
          {
            mainSiteLocation.City = location.CityName;
            mainSiteLocation.County = App.Settings.CountyEnabled ? location.CountyName : null;
            mainSiteLocation.State = location.StateName;

            workLocation = BuildWorkLocationString(location.CityName, location.CountyName, location.StateKey.Replace("State.", ""), location.Code);
          }
        }
        else
          workLocation = BuildWorkLocationString(city, county, state, postalCode);

        mainSiteLocation.Location = workLocation;
      }

      return mainSiteLocation;
    }

    /// <summary>
    /// Builds up the work location string
    /// </summary>
    /// <param name="city">The city</param>
    /// <param name="county">The county</param>
    /// <param name="state">The state</param>
    /// <param name="postalCode">The postal code</param>
    /// <returns>The work location string</returns>
    private string BuildWorkLocationString(string city, string county, string state, string postalCode)
    {
      var addressFields = new List<string>();

      if (city.IsNotNullOrEmpty())
        addressFields.Add(city);

      if (App.Settings.CountyEnabled && county.IsNotNullOrEmpty())
        addressFields.Add(county);

      addressFields.Add(state);

      return string.Concat(string.Join(", ", addressFields), " (", postalCode, ")");
    }

    protected NameValueCollection InitialiseClientSideCodeValuesForSalary()
    {
      var maxSalaryThreshold = Convert.ToDouble(App.Settings.MaximumSalary);
      var minSalaryThreshold = Convert.ToDouble(App.Settings.MinimumSalary);

      var maxSalaryThresholdString = maxSalaryThreshold.ToString("0.00");
      var minSalaryThresholdString = minSalaryThreshold.ToString("0.00");

      var frequencies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Frequencies).ToDictionary(freq => freq.Key.SubstringAfter("."), freq => freq.Id.ToString(CultureInfo.InvariantCulture));

      var maxThresholds = new Dictionary<string, string>
      {
        {frequencies["Hourly"], App.Settings.AdjustSalaryRangeByType ? CalculatateThreshold(maxSalaryThreshold, 8 * 5 * 52, true) : maxSalaryThresholdString},
        {frequencies["Daily"], App.Settings.AdjustSalaryRangeByType ? CalculatateThreshold(maxSalaryThreshold, 5 * 52, true) : maxSalaryThresholdString},
        {frequencies["Weekly"], App.Settings.AdjustSalaryRangeByType ? CalculatateThreshold(maxSalaryThreshold, 52, true) : maxSalaryThresholdString},
        {frequencies["Monthly"], App.Settings.AdjustSalaryRangeByType ? CalculatateThreshold(maxSalaryThreshold, 12, true) : maxSalaryThresholdString},
        {frequencies["Yearly"], maxSalaryThresholdString}
      };

      var minThresholds = new Dictionary<string, string>
      {
        {frequencies["Hourly"], App.Settings.AdjustSalaryRangeByType ? CalculatateThreshold(minSalaryThreshold, 8 * 5 * 52, false) : minSalaryThresholdString},
        {frequencies["Daily"], App.Settings.AdjustSalaryRangeByType ? CalculatateThreshold(minSalaryThreshold, 5 * 52, false) : minSalaryThresholdString},
        {frequencies["Weekly"], App.Settings.AdjustSalaryRangeByType ? CalculatateThreshold(minSalaryThreshold, 52, false) : minSalaryThresholdString},
        {frequencies["Monthly"], App.Settings.AdjustSalaryRangeByType ? CalculatateThreshold(minSalaryThreshold, 12, false) : minSalaryThresholdString},
        {frequencies["Yearly"], minSalaryThresholdString}
      };

      var nvc = new NameValueCollection();

      foreach (var item in maxThresholds)
      {
        nvc.Add(string.Format("MaxThreshold{0}", item.Key), item.Value);
        nvc.Add(string.Format("MinThreshold{0}", item.Key), minThresholds[item.Key]);
        nvc.Add(string.Format("ErrorMaximumSalary{0}", item.Key), CodeLocalise("ErrorMaximumSalary{0}.ErrorMessage", "Maximum salary must be a numerical value less than or equal to ${0}<br />", item.Value));
        nvc.Add(string.Format("ErrorMinimumSalary{0}", item.Key), CodeLocalise("ErrorMinimumSalary{0}.ErrorMessage", "Minimum salary must be a numerical value greater than or equal to ${0}<br />", minThresholds[item.Key]));
      }

      return nvc;
    }

    /// <summary>
    /// Calculate the threshold for a time period
    /// </summary>
    /// <param name="value">The original value</param>
    /// <param name="divisor">The divisor representing the time period</param>
    /// <param name="floor">Whether to floor the value, or using ceiling</param>
    /// <returns>
    /// A formatting value
    /// </returns>
    private static string CalculatateThreshold(double value, double divisor, bool floor)
    {
			var preRounded = Math.Round((value / divisor) * 100, 10);
      var rounded = floor ? Math.Floor(preRounded) : Math.Ceiling(preRounded);
      return (rounded / 100).ToString("0.00");
    }
  }
}