﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

using Focus.Web.Controllers.Base;

namespace Focus.Web.Controllers.WebTalent
{
	public class JobsController : JobControllerBase
	{
		public JobsController(IApp app)	: base(app)
		{
		}

	}
}