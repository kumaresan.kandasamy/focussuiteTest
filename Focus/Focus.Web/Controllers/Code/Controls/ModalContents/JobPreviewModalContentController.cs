﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Collections.Generic;

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Web.Code;
using Focus.Web.Code.Controls.ModalContents;
using Framework.Core;

#endregion

namespace Focus.Web.Controllers.Code.Controls.ModalContents
{
	public class JobPreviewModalContentController : PageControllerBase
	{
		public JobPreviewModalContentController(IApp app)
			: base(app)
		{
		}

		/// <summary>
		/// Posts the job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="fromApproval">Whether this is being posted from the approval queue</param>
		/// <returns></returns>
		/// <exception cref="System.Exception"></exception>
		public IControllerResult PostJob(JobDto job, bool? fromApproval = null)
		{
			// Check the job has a hiring manager
			if (job.EmployeeId.IsNull())
				throw new Exception(FormatError(ErrorTypes.InvalidJob, "Job has no hiring manager"));

			// Post the Job
			ContentRatings contentRating;
			List<string> inappropiateWords;
			var specialConditions = false;

			// Now post the job
			var postedJob = ServiceClientLocator.JobClient(App).PostJob(job, fromApproval.GetValueOrDefault(false), out contentRating, out inappropiateWords, out specialConditions);

			var confirmationTitle = (contentRating == ContentRatings.BlackListed)
																? CodeLocalise("PostedInappropriateConfirmation.Title", "Your posting cannot be accepted due to inappropriate content.")
																: CodeLocalise("PostedConfirmation.Title", "Thank you for submitting your #EMPLOYMENTTYPE#:LOWER.");

			var confirmationBody = string.Empty;

            string employerName = ServiceClientLocator.EmployerClient(App).GetEmployer(postedJob.EmployerId).Name;

			if (postedJob.JobStatus == JobStatuses.AwaitingEmployerApproval)
			{
				if (postedJob.JobStatus == JobStatuses.AwaitingEmployerApproval)
					confirmationBody = CodeLocalise("PostedAwaitingEmployerApproval.Confirmation",
                                                                                    @"Thank you for submitting your #EMPLOYMENTTYPE#:LOWER for {0}, on behalf of {1}. Your job will be posted once your account and the posting have been approved.
                                          <br /><br />In the meantime, you can view your job posting in the Drafts tab on your dashboard.",
																				postedJob.JobTitle, employerName);
			}
			else if (postedJob.ApprovalStatus == ApprovalStatuses.WaitingApproval)
			{
				if (postedJob.JobStatus == JobStatuses.OnHold)
				{
                    if (App.User.IsShadowingUser || App.Settings.Module == FocusModules.Assist)
                    {
                        if (App.Settings.ApprovalDefaultForStaffCreatedJobs.Equals(JobApprovalOptions.ApproveAll) || App.Settings.ApprovalDefaultForStaffCreatedJobs.Equals(JobApprovalOptions.SpecifiedApproval))
                        {
                            confirmationBody = CodeLocalise("Posted.Confirmation",
                                            @"Thank you for submitting the #EMPLOYMENTTYPE#:LOWER for {0}, on behalf of {1}. The job posting will be sent to the Job Post approval queue. It is not yet in Active status or available in match results. Once approved, it will move to the account’s Active tab and match to job seeker’s resumes.",
                                postedJob.JobTitle, employerName);
                        }
                    }
                    else
                        confirmationBody = CodeLocalise("PostedAwaitingJobApprovalOnHold.Confirmation",
                                                                                    @"Thank you for submitting your #EMPLOYMENTTYPE#:LOWER for {0}, on behalf of {1}. The job posting will be sent to the Job Post approval queue. It is not yet in Active status or available in match results. Once approved, it will move to the account’s Active tab and match to job seeker’s resumes.",
																				postedJob.JobTitle,employerName);
				}
				else if (specialConditions)
				{
                    if (App.User.IsShadowingUser || App.Settings.Module == FocusModules.Assist)
                    {
                        if (App.Settings.ApprovalDefaultForStaffCreatedJobs.Equals(JobApprovalOptions.ApproveAll) || App.Settings.ApprovalDefaultForStaffCreatedJobs.Equals(JobApprovalOptions.SpecifiedApproval))
                        {
                            confirmationBody = CodeLocalise("Posted.Confirmation",
                                            @"Thank you for submitting the #EMPLOYMENTTYPE#:LOWER for {0}, on behalf of {1}.The job posting will be sent to the Job Post approval queue.It is not yet in Active status or available in match results.Once approved, it will move to the account’s Active tab and match to job seeker’s resumes.",
                                postedJob.JobTitle, employerName);
                        }
                    }
                    else
                        confirmationBody = CodeLocalise("PostedGreyListed.Confirmation",
                                                                                        @"Thank you for submitting your #EMPLOYMENTTYPE#:LOWER for {0}, on behalf of {1}. We will review it within the next business day and contact you if there are any issues. In the meantime, you can view your #EMPLOYMENTTYPE#:LOWER in your drafts tab until approval has been granted.
                                            <br /><br />
                                            If you have any questions about our review, please contact us at #SUPPORTEMAIL# or #SUPPORTPHONE#.",postedJob.JobTitle,employerName);
				}
				else
				{
                    if (App.User.IsShadowingUser || App.Settings.Module == FocusModules.Assist)
                    {
                        if(App.Settings.ApprovalDefaultForStaffCreatedJobs.Equals(JobApprovalOptions.ApproveAll) || App.Settings.ApprovalDefaultForStaffCreatedJobs.Equals(JobApprovalOptions.SpecifiedApproval))
                        {
                            
                            confirmationBody = CodeLocalise("Posted.Confirmation",
                                            @" Thank you for submitting the #EMPLOYMENTTYPE#:LOWER for {0}, on behalf of {1}. The job posting will be sent to the Job Post approval queue. It is not yet in Active status or available in match results. Once approved, it will move to the account’s Active tab and match to job seeker’s resumes. ",
                                postedJob.JobTitle, employerName);
                        }
                    }
                    else
                        confirmationBody = CodeLocalise("PostedAwaitingJobApproval.Confirmation",
                                                                                    @" Thank you for submitting the #EMPLOYMENTTYPE#:LOWER for {0}, on behalf of {1}. The job posting will be sent to the Job Post approval queue. It is not yet in Active status or available in match results. Once approved, it will move to the account’s Active tab and match to job seeker’s resumes. ",
																				postedJob.JobTitle,employerName);
				}
			}
      else if (postedJob.ApprovalStatus == ApprovalStatuses.Reconsider)
      {
        confirmationBody = CodeLocalise("PostedPreviouslyRejected.Confirmation",
						@"Thank you for submitting your #EMPLOYMENTTYPE#:LOWER for {0}. Your job will be posted once it has been approved.
                                  <br /><br />A previous version of this #EMPLOYMENTTYPE#:LOWER was denied. The denial reason will be visible to the staff member reconsidering this #EMPLOYMENTTYPE#:LOWER.
                                  <br /><br />In the meantime, you can view your #EMPLOYMENTTYPE#:LOWER in your {1} tab until approval is granted.", postedJob.JobTitle, CodeLocalise(postedJob.JobStatus, true).ToLower());
      }
      else if (postedJob.JobStatus == JobStatuses.OnHold && contentRating != ContentRatings.BlackListed && inappropiateWords.IsNullOrEmpty() && !specialConditions)
      {
        confirmationBody = CodeLocalise("PostedOnHold.Confirmation", "Thank you for submitting your changes to your {0} posting. These changes have been stored and can be viewed on your posting in the On Hold tab. You can re-activate your posting by clicking on the action to \"Re-activate\"", postedJob.JobTitle);
      }
      else
			{
			    switch (contentRating)
			    {
			        case ContentRatings.Acceptable:
                        if (App.User.IsShadowingUser || App.Settings.Module == FocusModules.Assist)
                        {
                            confirmationBody = CodeLocalise("Posted.Confirmation",
                                                @"Thank you for submitting the #EMPLOYMENTTYPE#:LOWER for {0} on behalf of {1}. This posting has been approved and is now in Active job status.",
                            postedJob.JobTitle, employerName);
                        }
                        else
                            confirmationBody = CodeLocalise("Posted.Confirmation",
                                            @"Thank you for submitting the #EMPLOYMENTTYPE#:LOWER for {0} on behalf of {1}. This posting has been approved and is now in Active job status.",
			                postedJob.JobTitle,employerName);
			            break;
			        case ContentRatings.GreyListed:
                        if (App.User.IsShadowingUser || App.Settings.Module == FocusModules.Assist)
                        {
                            if (App.Settings.ApprovalDefaultForStaffCreatedJobs.Equals(JobApprovalOptions.ApproveAll) || App.Settings.ApprovalDefaultForStaffCreatedJobs.Equals(JobApprovalOptions.SpecifiedApproval))
                            {
                                confirmationBody = CodeLocalise("Posted.Confirmation",
                                                @"Thank you for submitting the #EMPLOYMENTTYPE#:LOWER for {0}, on behalf of {1}. The job posting will be sent to the Job Post approval queue. It is not yet in Active status or available in match results. Once approved, it will move to the account’s Active tab and match to job seeker’s resumes.",
                                    postedJob.JobTitle, employerName);
                            }
                        }
                        else
                            confirmationBody = CodeLocalise("PostedGreyListed.Confirmation",
											@"Thank you for submitting your #EMPLOYMENTTYPE#:LOWER for {0}. We will review it within the next business day and contact you if there are any issues. In the meantime, you can view your #EMPLOYMENTTYPE#:LOWER in your drafts tab until approval has been granted.
                                            <br /><br />
                                            If you have any questions about our review, please contact us at #SUPPORTEMAIL# or #SUPPORTPHONE#.",
			                postedJob.JobTitle);
			            break;
			        case ContentRatings.BlackListed:
			            var words = string.Join(", ", inappropiateWords);
			            confirmationBody = CodeLocalise("PostedBlackListed.Confirmation",
			                @"Your posting includes the word(s):
																						<br /><br />{0} 
                                            <br /><br />
                                            We are not able to post jobs that include this/these term(s). Please go back and edit your posting to ensure that all content is fully appropriate",
			                words);
			            break;
			    }
			}

		    var closeLink = (App.Settings.Module == FocusModules.Assist) ? UrlBuilder.JobOrderDashboard() : UrlBuilder.Default(App.User, App);

			if (job.IsNull())
			{
				return new InformationModalResult
				{
					Title = confirmationTitle,
					Details = confirmationBody,
					OkText = CodeLocalise("PostedConfirmation.CloseText", "Return to dashboard"),
					OkResult = new RedirectResult { Url = closeLink }
				};
			}

			var url = App.Settings.Module == FocusModules.Assist 
				? UrlBuilder.AssistJobWizard(job.Id.GetValueOrDefault(0)) 
				: UrlBuilder.JobWizard(job.Id.GetValueOrDefault(0));

			return new ConfirmationModalResult
			{
				Title = confirmationTitle,
				Details = confirmationBody,
				CancelText = CodeLocalise("PostedConfirmation.CloseText", "Return to dashboard"),
				CancelResult = new RedirectResult { Url = closeLink },
				OkText = CodeLocalise("PostedConfirmation.OkText", "Return to job"),
				OkResult = new RedirectResult
				{
					Url = url
				}
			};
		}

	    /// <summary>
	    /// Notifies the employer.
	    /// </summary>
	    /// <param name="job">The job.</param>
	    /// <param name="LockVersion"></param>
	    /// <returns></returns>
	    public IControllerResult NotifyEmployer(JobDto job)
		{
			var properties = new Dictionary<string, object> {{"Job", job}};

			var customModalResult = new CustomModalResult
			{
				ModalContentUrl = ModalContentLocations.EditPostingReferralConfirmationModal,
				Properties = properties,
				PopupDragHandleControlId = "EditPostingReferralConfirmationModalHeading"
			};

			return customModalResult;
		}
	}
}