﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Collections.Generic;
using Focus.Common;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Messages.CandidateService;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.Controllers.Code.Controls.ModalContents
{
	public class EditPostingReferralConfirmationContentController : PageControllerBase
	{
		public EditPostingReferralConfirmationContentController( IApp app )
			: base(app)
		{
		}

	    /// <summary>
	    /// Notifies the employer.
	    /// </summary>
	    /// <param name="job">The job.</param>
	    /// <param name="sendEmailCopy"></param>
	    /// <param name="lockVersion"></param>
	    /// <returns></returns>
	    public IControllerResult NotifyEmployer(JobDto job, bool sendEmailCopy)
		{
			var jobId = job.Id.GetValueOrDefault(0);
			var jobPostingReferral = ServiceClientLocator.JobClient(App).GetJobPostingReferral(jobId);

			if (job.PostedOn.IsNotNull())
			{
				ServiceClientLocator.JobClient(App).HoldJob(jobId, ApprovalStatuses.None);
			}
			else
			{
				ServiceClientLocator.JobClient(App).SetJobToDraft(jobId, ApprovalStatuses.None);
			}
			var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() &&
			                   userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty())
				? userDetails.PrimaryPhoneNumber.Number
				: "N/A";

			var jobLinkUrl = string.Concat(App.Settings.TalentApplicationPath, UrlBuilder.JobView(jobId, false));
			var jobLinkHtml = string.Format("<a href='{0}'>{0}</a>", jobLinkUrl);

		  var postingHtml = Utilities.InsertPostingFooter(job.PostingHtml, job.CriminalBackgroundExclusionRequired, job.CreditCheckRequired);

			var templateValues = new EmailTemplateData
			{
				RecipientName = String.Format("{0} {1}", jobPostingReferral.EmployeeFirstName, jobPostingReferral.EmployeeLastName),
				JobTitle = jobPostingReferral.JobTitle,
				JobPosting = postingHtml,
				SenderPhoneNumber = phoneNumber,
				SenderEmailAddress = App.User.EmailAddress,
				SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName),
				JobLinkUrls = new List<string> {jobLinkHtml}
			};
			var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.EditPostingReferralConfirmation, templateValues);

			var senderAddress = ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.EditPostingReferralConfirmation);

			var employeeId = jobPostingReferral.EmployeeId.GetValueOrDefault(0);

			ServiceClientLocator.EmployeeClient(App).EmailEmployee(employeeId, emailTemplate.Subject, emailTemplate.Body, sendEmailCopy, senderAddress);

			var informationModalResult = new InformationModalResult
			{
				Title = CodeLocalise("NotifyEmployer.Title", "#BUSINESS# notification sent"),
				Details =
					CodeLocalise("NotifyEmployer.Body",
						"An email has been sent to the posting's hiring manager for them to review your suggested changes."),
				OkText = CodeLocalise("CloseModal.Text", "OK"),
				OkResult = new RedirectResult {Url = UrlBuilder.PostingReferrals()}
			};

			return informationModalResult;
		}
	}
}