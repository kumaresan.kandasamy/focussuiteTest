﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employer;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Controllers.Code.Controls.User
{
	public class EmployerReferralsController : PageControllerBase
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerReferralsController"/> class.
		/// </summary>
		/// <param name="app">The app.</param>
		public EmployerReferralsController(IApp app)
			: base(app)
		{
		}

		/// <summary>
		/// Gets the view model.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public IControllerResult GetViewModel(EmployerAccountReferralCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
				return null;

			if (criteria.OrderBy.IsNull())
				criteria.OrderBy = orderBy;

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;
			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;
			

			var model = new EmployerReferralsViewModel
			{
				EmployerAccountReferrals = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferrals(criteria)
			};

			var vModelResult = new ViewModelResult { ViewModel = model };
			return vModelResult;
		}
	}
}