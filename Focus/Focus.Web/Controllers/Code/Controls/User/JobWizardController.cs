﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using Focus.Common.Code.ControllerResults;
using Focus.Common;
using Focus.Core;
using Focus.Web.Code;
using Focus.Web.Controllers.Base;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Controllers.Code.Controls.User
{
	public class JobWizardController : JobControllerBase
	{
		public JobWizardController(IApp app)
			: base(app)
		{
		}

		/// <summary>
		/// Gets the view model.
		/// </summary>
		/// <param name="hiringManagerId">The hiring manager identifier.</param>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="fromApproval">if set to <c>true</c> [from approval].</param>
		/// <returns></returns>
		/// <exception cref="Exception">
		/// </exception>
		public IControllerResult GetViewModel(long hiringManagerId, long jobId, bool fromApproval)
		{
			JobWizardViewModel model;
			if (jobId > 0)
			{
				model = new JobWizardViewModel { Job = ServiceClientLocator.JobClient(App).GetJob(jobId) };

        if (fromApproval && (model.Job.ApprovalStatus.IsNotIn(ApprovalStatuses.WaitingApproval,ApprovalStatuses.Rejected, ApprovalStatuses.Reconsider)|| !App.User.IsInRole(Constants.RoleKeys.AssistPostingApprover)))
					return new RedirectResult { Url = UrlBuilder.Default(App.User, App) };

				if (model.Job.BusinessUnitId.HasValue) model.BusinessUnitAddress = ServiceClientLocator.EmployerClient(App).GetPrimaryBusinessUnitAddress(model.Job.BusinessUnitId.Value);
					model.AllJobLocations = App.Settings.ShowWorkLocationFullAddress && model.Job.BusinessUnitId.HasValue ?
						ServiceClientLocator.JobClient(App).GetAllJobLocationsForBusinessUnit(model.Job.BusinessUnitId.Value):
						ServiceClientLocator.JobClient(App).GetAllJobLocations(model.Job.EmployerId);	
				
				model.JobLocations = ServiceClientLocator.JobClient(App).GetJobLocations(jobId);
				model.JobAddress = ServiceClientLocator.JobClient(App).GetJobAddress(jobId);

				if (!model.Job.EmployeeId.HasValue)
					throw new Exception(FormatError(ErrorTypes.EmployeeNotFound, "Employee id not found for job"));

				GetHiringManagersUserDetails(model, model.Job.EmployeeId.Value);

				model.JobCertificates = ServiceClientLocator.JobClient(App).GetJobCertificates(jobId);
				model.JobLanguages = ServiceClientLocator.JobClient(App).GetJobLanguages(jobId);
				model.JobLicences = ServiceClientLocator.JobClient(App).GetJobLicences(jobId);
				model.JobSpecialRequirements = ServiceClientLocator.JobClient(App).GetJobSpecialRequirements(jobId);
				model.JobDrivingLicenceEndorsements = ServiceClientLocator.JobClient(App).GetJobDrivingLicenceEndorsements(jobId);
			}
			else
			{
				if (hiringManagerId.IsNull() || hiringManagerId == 0)
					throw new Exception(FormatError(ErrorTypes.InvalidEmployee, "No hiring manager id provided"));

				var employee = ServiceClientLocator.EmployeeClient(App).GetEmployee(hiringManagerId);

				if (employee.IsNull())
					throw new Exception(FormatError(ErrorTypes.EmployeeNotFound, string.Format("No employee found with id {0}", hiringManagerId)));

        model = new JobWizardViewModel(employee.EmployerId, hiringManagerId, App.Settings.DaysForDefaultJobClosingDate, App.Settings.ConfidentialEmployersDefault)
				{
					AllJobLocations = ServiceClientLocator.JobClient(App).GetAllJobLocations(employee.EmployerId)
				};

				GetHiringManagersUserDetails(model, hiringManagerId);

			}
			var vModelResult = new ViewModelResult { ViewModel = model };
			return vModelResult;
		}

		/// <summary>
		/// Gets the hiring managers user details.
		/// </summary>
		private void GetHiringManagersUserDetails(JobWizardViewModel model, long hiringManagerId)
		{
			if (hiringManagerId.IsNull() || hiringManagerId == 0)
				throw new Exception(FormatError(ErrorTypes.InvalidEmployee, "Invalid hiring manager for job"));

			var employeeModel = ServiceClientLocator.EmployeeClient(App).GetUserDetails(hiringManagerId);

			model.JobContact = employeeModel.PersonDetails;
			model.JobContactAddress = employeeModel.AddressDetails;
			model.JobContactPrimaryContactDetails = employeeModel.PrimaryPhoneNumber;
		}
	}
}