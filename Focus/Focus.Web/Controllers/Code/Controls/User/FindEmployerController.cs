﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common;
using Focus.Core.Criteria.Employee;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Controllers.Code.Controls.User
{
  public class FindEmployerController : PageControllerBase
  {
    public FindEmployerController(IApp app) : base(app)
    { }


    /// <summary>
    /// Gets a list employees.
    /// </summary>
    /// <param name="criteria">The criteria on which to search.</param>
    /// <param name="orderBy">The order in which employees should be returned</param>
    /// <param name="startRowIndex">First row on data required</param>
    /// <param name="maximumRows">The maximum number of rows.</param>
    public IControllerResult GetEmployees(EmployeeCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
      var model = new FindEmployerViewModel();

      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      if (criteria.IsNull())
      {
        model.EmployeesCount = 0;
      }
      else
      {
        criteria.PageSize = maximumRows;
        criteria.PageIndex = pageIndex;

        model.Employees = ServiceClientLocator.EmployeeClient(App).SearchEmployees(criteria);
        model.EmployeesCount = model.Employees.TotalCount;
      }

			var modelResult = new ViewModelResult { ViewModel = model };
			return modelResult;
    }

    /// <summary>
    /// Marks an employer as a preferred employer.
    /// </summary>
    /// <param name="businessUnitId">The business unit id.</param>
    public IControllerResult MarkAsPreferredConfirmation(long businessUnitId)
    {
      ServiceClientLocator.EmployerClient(App).SetEmployerPreferredStatus(businessUnitId, true);

      return new InformationModalResult
      {
        Title = CodeLocalise("MarkAsPreferredEmployer.Title", "Action completed"),
        Details = CodeLocalise("MarkAsPreferredEmployer.Body", "The selected employer record has been updated"),
        OkText = CodeLocalise("CloseModal.Text", "OK"),
        OkResult = null,
        Height = 150
      };
    }

    /// <summary>
    /// Unmarks an employer as no longer being a preferred employer.
    /// </summary>
    /// <param name="businessUnitId">The business unit id.</param>
    public IControllerResult UnmarkAsPreferredConfirmation(long businessUnitId)
    {
      ServiceClientLocator.EmployerClient(App).SetEmployerPreferredStatus(businessUnitId, false);

      return new InformationModalResult
      {
        Title = CodeLocalise("UnmarkAsPreferredConfirmation.Title", "Action completed"),
        Details = CodeLocalise("UnmarkAsPreferredConfirmation.Body", "The selected employer record has been updated"),
        OkText = CodeLocalise("CloseModal.Text", "OK"),
        OkResult = null,
        Height = 150
      };
    }

    public IControllerResult PreferredEmployerComplete()
    {
      return new ConfirmationResult();
    }
  }
}