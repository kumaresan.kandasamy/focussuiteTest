﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Core;

#endregion

namespace Focus.Web.Code.Controllers
{
	public class ErrorController : PageControllerBase
	{
		public ErrorController(IApp app)
			: base(app)
		{
		}

		public IControllerResult TryProcessError(ErrorTypes errorType)
		{
			switch (errorType)
			{
				case ErrorTypes.UserNotEnabled:
					return new InformationModalResult
					{
						Title = CodeLocalise("UnexpectedHold.Title", "Unexpected hold"),
						Details = CodeLocalise("UnexpectedHold.Message",
													 "An unexpected hold has been placed on your account, indicating a problem our staff needs to discuss with you. Please contact #SUPPORTHELPTEAM# at #SUPPORTPHONE# immediately. #SUPPORTHOURSOFBUSINESS# We apologize for this inconvenience."),
						OkResult = new RedirectToActionResult { Controller = GetType(), ActionName = "LogOutAndRedirectToLogin" }
					};

				case ErrorTypes.UserBlocked:
					return new InformationModalResult
					{
						Title = CodeLocalise("AssistUserBlocked.Title", "Account on hold"),
						Details = CodeLocalise("EmployerBlocked.Message",
													 "Your account has been placed on hold. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> Our business hours are between #SUPPORTHOURSOFBUSINESS#."),
						OkResult = new RedirectToActionResult { Controller = GetType(), ActionName = "LogOutAndRedirectToLogin" }
					};
				case ErrorTypes.InvalidHttpRequest:
					return new InformationModalResult
					{
						Title = CodeLocalise("InvalidRequest.Title", "Invalid Request"),
						Details =
							CodeLocalise("InvalidRequest.Message",
													 "A potentially invalid request has been detected."),
						OkResult = new RedirectToActionResult
						{
							Controller = GetType(),
							ActionName = "RedirectToHome"
						}
					};
			}
			return null;
		}

		public IControllerResult RedirectToHome()
		{
			return new RedirectResult { Url = UrlBuilder.LogIn() };
		}
	}
}