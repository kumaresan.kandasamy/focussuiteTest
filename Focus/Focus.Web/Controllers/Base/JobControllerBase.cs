﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System.Collections.Generic;

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Focus.Web.Code.Controls.ModalContents;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.Controllers.Base
{
    public abstract class JobControllerBase : PageControllerBase
    {
        protected JobControllerBase(IApp app) : base(app) { }

        /// <summary>
        /// Duplicates the job.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <returns></returns>
        public IControllerResult DuplicateJob(long jobId)
        {
            // Do the Duplication
            var duplicatedJobId = ServiceClientLocator.JobClient(App).DuplicateJob(jobId);

						var url = App.Settings.Module == FocusModules.Talent 
							? UrlBuilder.TalentJobWizard(duplicatedJobId) 
							: UrlBuilder.AssistJobWizard(duplicatedJobId);
            return new RedirectResult { Url = url };
        }

        /// <summary>
        /// Queries the duplicate job.
        /// </summary>
        /// <param name="jobId">The job identifier.</param>
        /// <param name="jobTitle">The job title.</param>
        /// <returns></returns>
        public IControllerResult QueryDuplicateJob(long jobId, string jobTitle)
        {
            return new ConfirmationModalResult
            {
                Title = CodeLocalise("DuplicateConfirmation.Title", "Duplicate Posting"),
                Details = CodeLocalise("DuplicateConfirmation.Body", "Are you sure you want to duplicate posting {0}?", jobTitle),
                CancelText = CodeLocalise("Global.Cancel.Text", "Cancel"),
                OkText = CodeLocalise("DuplicateConfirmation.Ok.Text", "Duplicate posting"),
                OkResult = new RedirectToActionResult { Controller = GetType(), ActionName = "DuplicateJob", ActionParameters = new object[] { jobId } }
            };
        }

        /// <summary>
        /// Previews the posting.
        /// </summary>
        /// <param name="job">The job.</param>
        /// <param name="showPrint">if set to <c>true</c> [show print].</param>
        /// <param name="allowPost">if set to <c>true</c> [allow post].</param>
        /// <returns></returns>
        public IControllerResult PreviewPosting(JobDto job, bool showPrint = false, bool allowPost = false)
        {
            return new CustomModalResult
            {
                Properties = new Dictionary<string, object>
							{
								{"Job", job},
								{"ShowPrint", showPrint},
								{"AllowPost", allowPost}
							},
                ModalContentUrl = ModalContentLocations.JobPreview,
                PopupDragHandleControlId = "JobPreviewModalHeading"
            };
        }

        /// <summary>
        /// Saves the job.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="stepsToSave">The steps to save.</param>
        /// <param name="fromApproval">if set to <c>true</c> [from approval].</param>
        /// <param name="currentStep">The current step.</param>
        /// <param name="finalStep">The final step.</param>
        /// <param name="isNewAddress">if set to <c>true</c> [is new address].</param>
        /// <returns></returns>
        public IControllerResult SaveJob(JobWizardViewModel model, List<int> stepsToSave, bool fromApproval, int currentStep, int finalStep, bool isNewAddress)
        {
            // The request must be stopped here as beyond this point the model is saved and LockVersion updated.
            if (model.Job.Id.IsNotNull())
            {
                if (ServiceClientLocator.JobClient(App).GetJob(model.Job.Id.Value).LockVersion != model.Job.LockVersion)
                {
                    var failedModalResult = new InformationModalResult
                    {
                        Title = CodeLocalise("UserUpdated.Title", "Already Updated"),
                        Details =
                            CodeLocalise("UserUpdated.Body",
                                "Another user has recently updated this record. Please exit the record and return to it to see the changes made."),
                        OkText = CodeLocalise("CloseModal.Text", "OK")
                    };

                    return failedModalResult;
                }
            }

            //Save job requirements
            if (stepsToSave.Contains(4))
            {
                ServiceClientLocator.JobClient(App).SaveJobCertificates(model.Job.Id.Value, model.JobCertificates);
                ServiceClientLocator.JobClient(App).SaveJobLanguages(model.Job.Id.Value, model.JobLanguages);
                ServiceClientLocator.JobClient(App).SaveJobLicences(model.Job.Id.Value, model.JobLicences);
                ServiceClientLocator.JobClient(App).SaveJobSpecialRequirements(model.Job.Id.Value, model.JobSpecialRequirements);
                ServiceClientLocator.JobClient(App).SaveJobDrivingLicenceEndorsements(model.Job.Id.Value, model.JobDrivingLicenceEndorsements);
                if (App.Settings.Theme == FocusThemes.Education) ServiceClientLocator.JobClient(App).SaveJobProgramsOfStudy(model.Job.Id.Value, model.JobProgramsOfStudy);
            }

            // Save the Job Locations
            if (stepsToSave.Contains(5))
            {
                if (model.Job.Id != null)
                    model.Job.Location = ServiceClientLocator.JobClient(App).SaveJobLocations(model.Job.Id.Value, model.JobLocations);
            }

            // Changes have been made so set the Approval status to none so that it goes through approval checks
            if (!fromApproval && model.Job.ApprovalStatus != ApprovalStatuses.Rejected)
                model.Job.ApprovalStatus = ApprovalStatuses.None;

            // Save the Job in it's current state	(moved to after other saves so job entity is updated)			
            model.Job = ServiceClientLocator.JobClient(App).SaveJob(model.Job);

            // Check to see if the job has a job Address if not set it to the employers address
            if ((model.JobAddress.IsNull() || isNewAddress) && model.BusinessUnitAddress.IsNotNull() && model.Job.Id.HasValue)
            {
                // Save the main company address as the job address
                var jobAddress = new JobAddressDto
                {
                    JobId = model.Job.Id.Value,
                    Line1 = model.BusinessUnitAddress.Line1,
                    Line2 = model.BusinessUnitAddress.Line2,
                    Line3 = model.BusinessUnitAddress.Line3,
                    TownCity = model.BusinessUnitAddress.TownCity,
                    CountyId = model.BusinessUnitAddress.CountyId,
                    PostcodeZip = model.BusinessUnitAddress.PostcodeZip,
                    StateId = model.BusinessUnitAddress.StateId,
                    CountryId = model.BusinessUnitAddress.CountryId,
                    IsPrimary = true
                };

                model.JobAddress = ServiceClientLocator.JobClient(App).SaveJobAddress(jobAddress);
            }

            var editingJobDescription = (currentStep == 2 || (currentStep == 3 && model.Job.WizardPath == 21));
            // Always save and post when not in draft and not some areas of editing a description
            if (currentStep == finalStep || ((model.Job.JobStatus != JobStatuses.Draft || fromApproval) && !editingJobDescription))
            {
	            var modalContentUrl = ModalContentLocations.JobPreview;
	            var properties = new Dictionary<string, object>
	            {
		            {"FromApproval", fromApproval},
		            {"AllowPost", true},
		            {"Job", model.Job}
	            };

                // Show the modal to post the job
	            return new CustomModalResult
	            {
		            Properties = properties,
		            ModalContentUrl = modalContentUrl,
		            PopupDragHandleControlId = "JobPreviewModalHeading"
	            };
            }
            // Move to the next step or if this is step 3 and path 2 part 1 (21)
            // then goto path 2 part 2 (22)
            if (currentStep == 3 && model.Job.WizardPath == 21) return new ConfirmationResult();
            return null;
        }
    }
}
