﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common;
using Focus.Core;

#endregion

namespace Focus.Web.Code.Controllers.WebAssist
{
	public class ManageOfficesController : PageControllerBase
	{
		public ManageOfficesController(IApp app)
			: base(app)
		{ }

		/// <summary>
		/// Determines whether [is office assigned] [the specified office id].
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public IControllerResult IsOfficeAssigned(long officeId)
		{
			var assignedModel = ServiceClientLocator.EmployerClient(App).IsOfficeAssigned(officeId);

			if (assignedModel.IsAssigned)
			{
				var errorText = string.Empty;

				if (assignedModel.Errors.Count > 0)
				{
					foreach (var error in assignedModel.Errors)
					{
						switch (error)
						{
							case ErrorTypes.OfficeHasEmployers:
								if (errorText.Length > 0) errorText += "</br>";
								errorText += CodeLocalise("OfficeHasEmployers.ErrorText", "There are #BUSINESSES#:LOWER assigned to this office");
								break;

							case ErrorTypes.OfficeHasJobOrders:
								if (errorText.Length > 0) errorText += "</br>";
								errorText += CodeLocalise("OfficeHasJobOrders.ErrorText", "There are #EMPLOYMENTTYPEs#:LOWER assigned to this office");
								break;

							case ErrorTypes.OfficeHasJobSeekers:
								if (errorText.Length > 0) errorText += "</br>";
								errorText += CodeLocalise("OfficeHasJobSeekers.ErrorText", "There are job seekers assigned to this office");
								break;

							case ErrorTypes.OfficeHasStaffMembers:
								if (errorText.Length > 0) errorText += "</br>";
								errorText += CodeLocalise("OfficeHasStaffMembers.ErrorText", "There are staff members assigned to this office");
								break;

							case ErrorTypes.OfficeHasAssignedZips:
								if (errorText.Length > 0) errorText += "</br>";
								errorText += CodeLocalise("OfficeHasStaffMembers.ErrorText", "There are zip codes assigned to this office");
								break;
						}
					}

					if (errorText.Length > 0) errorText += "</br></br>";
					errorText += CodeLocalise("OfficeHasStaffMembers.ErrorText", "Please resolve the above issues before continuing");

					return new InformationModalResult
					{
						Title = CodeLocalise("UnableToDeactivateOffice.Title", "Unable to deactivate"),
						Details = errorText
					};
				}
			}
			else
			{
				return new ConfirmationModalResult
				{
					Title = "Deactivate Office",
					Details = "If you are sure you want to deactivate this office, please select OK",
					OkResult = new RedirectToActionResult { Controller = GetType(), ActionName = "DeactivateOffice", ActionParameters = new object[] { officeId } }
				};
			}
			return null;
		}

		/// <summary>
		/// Deactivates the office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public IControllerResult DeactivateOffice(long officeId)
		{
			var activateDeactivateModel = ServiceClientLocator.EmployerClient(App).ActivateDeactivateOffice(officeId, true);

			if (activateDeactivateModel.Error == ErrorTypes.OfficeIsNotActive)
			{
				return new InformationModalResult
				{
					Title = CodeLocalise("UnableToDeactivateOffice.Title", "Unable to deactivate"),
					Details = CodeLocalise("UnableToDeactivateOffice.ErrorText", "Office is not active")
				};
			}

			return null;
		}

		/// <summary>
		/// Confirms the reactivate.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public IControllerResult ConfirmReactivate(long officeId)
		{
			return new ConfirmationModalResult
			{
				Title = "Reactivate Office",
				Details = "If you are sure you want to reactivate this office, please select OK",
				OkResult = new RedirectToActionResult { Controller = GetType(), ActionName = "ActivateOffice", ActionParameters = new object[] { officeId } }
			};
		}

		/// <summary>
		/// Activates the office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public IControllerResult ActivateOffice(long officeId)
		{
			var activateDeactivateModel = ServiceClientLocator.EmployerClient(App).ActivateDeactivateOffice(officeId, false);

			if (activateDeactivateModel.Error == ErrorTypes.OfficeIsNotInactive)
			{
				return new InformationModalResult
				{
					Title = CodeLocalise("UnableToDeactivateOffice.Title", "Unable to activate"),
					Details = CodeLocalise("UnableToDeactivateOffice.ErrorText", "Office is not inactive")
				};
			}

			return null;
		}
	}
}