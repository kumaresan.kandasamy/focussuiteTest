﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Directives

using System.Collections.Generic;

using Focus.Common.Code.ControllerResults;
using Focus.Common.Controllers.Base;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;

#endregion

namespace Focus.Web.Controllers.WebAssist
{
	public class TalentSettingsController : SettingsController
	{
		public TalentSettingsController(IApp app)
			: base(app)
		{ }

		public IControllerResult SaveTalentSettings(List<ConfigurationItemDto> configurationItems)
		{
			
			if (configurationItems.IsNotNullOrEmpty())
			{
        var resultSuccess = ServiceClientLocator.CoreClient(App).SaveConfigurationItems(configurationItems);
				App.RefreshSettings();

			  if (!resultSuccess)
			  {
			    return new InformationModalResult
			    {
			      Title = CodeLocalise("TalentDefaultsUpdatedConfirmation.Title", "Talent defaults failed to update"),
			      Details = CodeLocalise("TalentDefaultsUpdatedConfirmation.Body", "The default settings have failed to update."),
			      OkText = CodeLocalise("CloseModal.Text", "OK")
			    };
			  }
			}
      return new InformationModalResult
      {
        Title = CodeLocalise("TalentDefaultsUpdatedConfirmation.Title", "Talent Defaults Updated"),
        Details = CodeLocalise("TalentDefaultsUpdatedConfirmation.Body", "The default settings have been updated."),
        OkText = CodeLocalise("CloseModal.Text", "OK")
      };
		}

		public void SaveLogo(byte[] bytes)
		{
			var applicationImage = new ApplicationImageDto { Image = bytes, Type = ApplicationImageTypes.FocusTalentHeader };
			ServiceClientLocator.CoreClient(App).SaveApplicationImage(applicationImage);
		}
	}
}