﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common;
using Focus.Core;

#endregion


namespace Focus.Web.Code.Controllers.WebAssist
{
	public class ManageStaffController : PageControllerBase
	{
		public ManageStaffController(IApp app) : base(app){ }

		/// <summary>
		/// Deactivates the account.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public IControllerResult DeactivateAccount(long personId)
		{
			// If EnableOfficeDefaultAdmin = true then check if the staff member is assigned as an office default administrator
			if (App.Settings.EnableOfficeDefaultAdmin )
			{
				var defaultAdministratorModel = ServiceClientLocator.EmployerClient(App).IsDefaultAdministrator(personId);

				if (defaultAdministratorModel.IsDefaultAdministrator)
				{
					var offices = string.Join(",	", defaultAdministratorModel.Offices.Select(x => x.OfficeName).ToList());

					return new InformationModalResult
					{
						Title = CodeLocalise("UnableToDeactivate.Title", "Unable to deactivate"),
						Details = CodeLocalise("UnableToDeactivate.Body", "The user you have selected is the default administrator for the following office(s):<br><br>{0}<br><br>Please replace the default administrator before deactivating this account", offices)
					};
				}
			}

			var success = ServiceClientLocator.AccountClient(App).BlockPerson(personId);

			if (success)
			{
				return new InformationModalResult
				{
					Title = CodeLocalise("DeactivateAccountConfirmation.Title", "Account deactivated"),
					Details = CodeLocalise("DeactivateAccountConfirmation.Body", "The selected staff member's account has been deactivated.")
				};
			}
			else
			{
				return new InformationModalResult
				{
					Title = CodeLocalise("DeactivateAccountConfirmation.Title", "Account deactivated"),
					Details = CodeLocalise("DeactivateAccountConfirmation.Body", "This account has already been deactivated by another account administrator.")
				};
			}
		}

		/// <summary>
		/// Reactivates the account.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public IControllerResult ReactivateAccount(long personId)
		{
			var success = ServiceClientLocator.AccountClient(App).UnblockPerson(personId);

			if (success)
			{
				return new InformationModalResult
				{
					Title = CodeLocalise("ReactivateAccountConfirmation.Title", "Account reactivated"),
					Details = CodeLocalise("ReactivateAccountConfirmation.Body", "The selected staff member's account has been reactivated.")
				};
			}
			else
			{
				return new InformationModalResult
				{
					Title = CodeLocalise("ReactivateAccountConfirmation.Title", "Account reactivated"),
					Details = CodeLocalise("ReactivateAccountConfirmation.Body", "This account has already been reactivated by another account administrator.")
				};
			}
		}

	}
}