﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code.Controls.ModalContents;

#endregion

namespace Focus.Web.Controllers.WebAssist
{
	public class EmployerReferralController : PageControllerBase
	{
		public EmployerReferralController(IApp app)
			: base(app)
		{ }

		public IControllerResult PreviewPosting(JobDto job)
		{
			return new CustomModalResult
			{
				Properties = new Dictionary<string, object>
							{
								{"Job", job}
							},
				ModalContentUrl = ModalContentLocations.JobPreview,
				PopupDragHandleControlId = "JobPreviewModalHeading"
			};
		}
	}
}