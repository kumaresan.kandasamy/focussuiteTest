﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;

#endregion

namespace Focus.Web.Code.Controllers.WebAssist
{
	public class EmployerRegistrationStep1Controller : PageControllerBase
	{
		public EmployerRegistrationStep1Controller(IApp app)
			: base(app)
		{ }

		public IControllerResult ShowNeedFeinModal()
		{
			return new InformationModalResult
				{
					Title = CodeLocalise("NeedFein.Title", "Need an FEIN?"),
					Details = CodeLocalise("NeedFein.Text",
							            "The Internal Revenue Service (IRS) provides several options to obtain a Federal Employer Identification Number, " +
													"including an expedited, online application service available from #IRSONLINEAPPLICATIONHOURS# at " +
													"<a href='#IRSONLINEAPPLICATIONURL#' target='_blank'>#IRSONLINEAPPLICATIONURL#</a>")
				};
		}
	}
}