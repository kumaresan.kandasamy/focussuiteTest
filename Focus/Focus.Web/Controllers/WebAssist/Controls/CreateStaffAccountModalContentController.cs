﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.Controllers.WebAssist.Controls
{
	public class CreateStaffAccountModalContentController : PageControllerBase
	{
		public CreateStaffAccountModalContentController(IApp app): base(app)
		{
		}


		/// <summary>
		/// Creates the assist user.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		public IControllerResult CreateAssistUser(CreateAssistUserModel model)
		{
			model.UserExternalOffice = CodeLocalise("Global.UnassignedOffice.Text", "Unassigned");

			ServiceClientLocator.AccountClient(App).CreateAssistUser(model);
			
			if (model.RegistrationError.IsNotNull() && (!model.RegistrationError.Contains(ErrorTypes.Ok)))
			{
				var errorResult = new ErrorResult();

				if (model.RegistrationError.Contains(ErrorTypes.UserNameAlreadyExists) || model.RegistrationError.Contains(ErrorTypes.UserAlreadyExists))
				{
					var error = CodeLocalise("EmailAddressAlreadyInUse.ErrorMessage", "Email address is already in use by another user");
					errorResult = new ErrorResult { ContinueAfterHandle = false, ErrorMessage = error, Message = error};
				}
				else
				{
					var error = CodeLocalise(model.RegistrationError[0]);
					errorResult = new ErrorResult { ContinueAfterHandle = false, ErrorMessage = error, Message = error };
				}

				return errorResult;
			}

			return new InformationModalResult
			{
				Title = CodeLocalise("StaffAccountCreatedConfirmation.Title", "Staff account created."),
				Details = CodeLocalise("StaffAccountCreatedConfirmation.Confirmation", "Staff account successfully created.")
			};
		}
	}
}