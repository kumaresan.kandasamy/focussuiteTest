﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Common;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Core;

using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.Web.Code.Controllers
{
	public class WebAuthController : PageControllerBase
	{
		public WebAuthController(IApp app)
			: base(app)
		{
		}

		/// <summary>
		/// Attempts the log in.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password.</param>
		/// <returns></returns>
		public IControllerResult AttemptLogIn(string userName, string password)
		{
			try
			{
				var loginResult = new LoginResult();
				var validationUrl = (App.Settings.Module == FocusModules.Assist
															 ? App.Settings.AssistApplicationPath
															 : App.Settings.TalentApplicationPath) + UrlBuilder.AccountValidate(false);

				var loggedinUserModel = ServiceClientLocator.AuthenticationClient(App).LogIn(userName, password, App.Settings.Module, validationUrl, true);

			  if (loggedinUserModel.ResetSessionId.IsNotNull())
			  {
			    App.ResetSessionId(loggedinUserModel.ResetSessionId);
          App.ResetUserContext(loggedinUserModel.UserContext);
        }
        
        loginResult.UserData = loggedinUserModel.UserContext;
				App.UserData.Load(loggedinUserModel.UserData);

				// If not error was thrown in the last step we have successfully logged in
				loginResult.Successful = true;

				var redirect = App.GetCookieValue("Focus:Redirect", versionCookie: true);

				loginResult.RedirectUrl = !redirect.IsNullOrEmpty() ? redirect : UrlBuilder.Default(loginResult.UserData, App, loginResult.UserData.LastLoggedInOn == null);

				return loginResult;
			}
			catch (ServiceCallException ex)
			{
        return ShowConfirmationModal(ex, userName);
			}
		}

		/// <summary>
		/// Resends the email.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		public ConfirmationResult ResendEmail(string userName)
		{
			var singleSignOn = ServiceClientLocator.AuthenticationClient(App).CreateEmployeeSingleSignOn(userName);

			var validationUrl = (App.Settings.Module == FocusModules.Assist)
														? App.Settings.AssistApplicationPath +
															string.Format(UrlBuilder.AccountValidate(false), singleSignOn)
														: App.Settings.TalentApplicationPath + UrlBuilder.TalentValidate(singleSignOn, false);

			ServiceClientLocator.AccountClient(App).SendActivationEmail(userName, validationUrl);

			// If we have got to this point without errors then we will return a successful confirmation.
			return new ConfirmationResult { Successful = true };
		}

		/// <summary>
		/// Registers this instance.
		/// </summary>
		/// <returns></returns>
		public RedirectResult Register()
		{
			return new RedirectResult { Url = UrlBuilder.Register() };
		}

		private IControllerResult ShowConfirmationModal(ServiceCallException error, string actionData = null)
		{
			switch ((ErrorTypes)error.ErrorCode)
			{
				case ErrorTypes.UserBlocked:
					if (App.Settings.Module == FocusModules.Assist)
					{
						return new InformationModalResult
						{
							Title = CodeLocalise("AssistUserBlocked.Title", "Account on hold"),
							Details =
								CodeLocalise("AssistUserBlocked.Message",
														 "Your account has been placed on hold. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> Our business hours are between #SUPPORTHOURSOFBUSINESS#."),
						};
					}
					else
					{
						return new InformationModalResult
						{
							Title = CodeLocalise("EmployerBlocked.Title", "Account on hold"),
							Details = CodeLocalise("EmployerBlocked.Message",
																		 "An unexpected hold has been placed on your account. Please contact your Support team on #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a>. Our business hours are between #SUPPORTHOURSOFBUSINESS#."),
						};
					}

				case ErrorTypes.UserNotEnabled:
					return new InformationModalResult
					{
						Title = CodeLocalise("ResendEmail.Title", "User not enabled"),
						Details = CodeLocalise("ResendEmail.Message",
													 "Your account has yet to be authenticated. If you have not received an email to authenticate your account, please select 'Resend Email' below to request another one. "),
						OkText = "Resend email",
						DisplayCloseIcon = true,
						OkResult = new RedirectToActionResult
						{
							Controller = GetType(),
							ActionName = "ResendEmail",
							ActionParameters = new object[] { actionData }
						}
					};

				case ErrorTypes.EmployerNotApproved:
					return new InformationModalResult
					{
						Title = CodeLocalise("EmployerNotApproved.Title", "#BUSINESS# not approved"),
						Details = CodeLocalise("EmployerNotApproved.Message", "Your account has yet to be approved."),
					};

        case ErrorTypes.EmployerOnHold:
          return new InformationModalResult
          {
						Title = CodeLocalise("EmployerOnHold.Title", "#BUSINESS# on hold"),
            Details = CodeLocalise("EmployerOnHold.Message", "Your account has been placed on hold."),
          };
			}

			return new ErrorResult { ErrorCode = error.ErrorCode, ErrorMessage = error.Message, Message = error.Message };
		}
	}
}