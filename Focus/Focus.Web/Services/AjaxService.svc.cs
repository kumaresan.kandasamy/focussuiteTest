﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using System.Web.Script.Serialization;

using AjaxControlToolkit;

using Focus.Common.Helpers;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.Services
{
	[ServiceContract(Namespace = "")]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class AjaxService
	{
		/// <summary>
		/// The _app
		/// </summary>
		private readonly IApp _app;

    /// <summary>
    /// Default constructor to initializes a new instance of the <see cref="AjaxService"/> class.
    /// </summary>
    public AjaxService()
    {
      _app = new HttpApp();
    }

		/// <summary>
		/// Initializes a new instance of the <see cref="AjaxService"/> class.
		/// </summary>
		/// <param name="app">The application.</param>
		public AjaxService(IApp app)
		{
			_app = app ?? new HttpApp();
		}

		#region Talent / Assist methods

		/// <summary>
		/// Localises from code using the specified key and if not found it will return the default value.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		protected string CodeLocalise(string key, string defaultValue, params object[] args)
		{
			return AjaxHelper.CodeLocalise(key, defaultValue, args);
		}

		/// <summary>
		/// Gets the job titles.
		/// 
		/// This will be filtered based on the employer id passed in the contextKey
		/// 
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <param name="contextKey">The context key.</param>
		/// <returns></returns>
		[OperationContract]
		public string[] GetJobTitles(string prefixText, int count, string contextKey)
		{
			return AjaxHelper.GetJobTitles(prefixText, count, null);
		}

		/// <summary>
		/// Gets the licences and certifications.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		[OperationContract]
		public string[] GetLicencesAndCertifications(string prefixText, int count)
		{
			return AjaxHelper.GetLicencesAndCertifications(prefixText, count);
		}

		/// <summary>
		/// Gets the certifications.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		[OperationContract]
		public string[] GetLicences(string prefixText, int count)
		{
			return AjaxHelper.GetLicences(prefixText, count);
		}

		/// <summary>
		/// Gets the licences.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		[OperationContract]
		public string[] GetCertifications(string prefixText, int count)
		{
			return AjaxHelper.GetCertifications(prefixText, count);
		}

		/// <summary>
		/// Gets the languages.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		[OperationContract]
		public string[] GetLanguages(string prefixText, int count)
		{
			return AjaxHelper.GetLanguages(prefixText, count);
		}

		/// <summary>
		/// Gets the naics.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		[OperationContract]
		public string[] GetNaics(string prefixText, int count)
		{
			return AjaxHelper.GetNaics(prefixText, count, true);
		}

    /// <summary>
    /// Gets the naics.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetAllCounties(string prefixText, int count)
    {
      return AjaxHelper.GetAllCounties(prefixText, count);
    }

		/// <summary>
		/// Gets the drop down occupations.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		[OperationContract]
		public CascadingDropDownNameValue[] GetOccupations(string knownCategoryValues, string category)
		{
			return AjaxHelper.GetOccupations(knownCategoryValues, category);
		}

		/// <summary>
		/// Gets the counties for a given state
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		[OperationContract]
		public CascadingDropDownNameValue[] GetCounties(string knownCategoryValues, string category)
		{
			return AjaxHelper.GetCounties(knownCategoryValues, category);
		}

		[OperationContract]
		[WebInvoke(ResponseFormat = WebMessageFormat.Json)]
		public PostalCodeStateCityCounty GetStateCityCountyAndCountiesForPostalCode(string postalCode)
		{
			return AjaxHelper.GetStateCityCountyAndCountiesForPostalCode(postalCode);
		}

		/// <summary>
		/// Gets the uploaded job description.
		/// This would have been set by the UploadJobDescriptionAsyncFileUpload_UploadedComplete
		/// 
		/// What a HACK but it works!!
		/// 
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		public string GetUploadedJobDescription()
		{
			var s = OldApp_RefactorIfFound.GetSessionValue<string>(Constants.StateKeys.UploadedJobDescription);
			OldApp_RefactorIfFound.RemoveSessionValue(Constants.StateKeys.UploadedJobDescription);
			return (s ?? "");
		}

		/// <summary>
		/// Gets the size of the uploaded logo.
		/// This would have been set by the UploadLogoAsyncFileUpload_UploadedComplete
		/// 
		/// What a HACK but it works!!
		///  
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		public string IsUploadedLogoValid()
		{
			var b = OldApp_RefactorIfFound.GetSessionValue(Constants.StateKeys.UploadedLogoIsValid, false);
			OldApp_RefactorIfFound.RemoveSessionValue(Constants.StateKeys.UploadedLogoIsValid);
			return (b ? "1" : "0");
		}

		/// <summary>
		/// Gets the size of the uploaded image.
		/// This would have been set by the UploadImageAsyncFileUpload_UploadedComplete
		/// 
		/// What a HACK but it works!!
		///  
		/// </summary>
		/// <returns></returns>
		[OperationContract]
		public string IsUploadedImageValid()
		{
			var b = OldApp_RefactorIfFound.GetSessionValue(Constants.StateKeys.UploadedImageIsValid, false);
			OldApp_RefactorIfFound.RemoveSessionValue(Constants.StateKeys.UploadedImageIsValid);
			return (b ? "1" : "0");
		}

		/// <summary>
		/// Gets the business unit description.
		/// </summary>
		/// <param name="businessUnitDescriptionId">The business unit description id.</param>
		/// <returns></returns>
		[OperationContract]
		public string GetBusinessUnitDescription(string businessUnitDescriptionId)
		{
			return AjaxHelper.GetBusinessUnitDescription(businessUnitDescriptionId);
		}

		/// <summary>
		/// Gets the localisation items.
		/// </summary>
		/// <param name="contextKey">The context key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(ResponseFormat = WebMessageFormat.Json)]
		public JsonResponse<LookupItem[]> GetLocalisationItems(string contextKey)
		{
			return AjaxHelper.GetLocalisationItems(contextKey);
		}

		/// <summary>
		/// Gets the localisation item.
		/// </summary>
		/// <param name="culture">The culture.</param>
		/// <param name="contextKey">The context key.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(ResponseFormat = WebMessageFormat.Json)]
		public JsonResponse<LocalisationItem> GetLocalisationItem(string culture, string contextKey, string key)
		{
			return AjaxHelper.GetLocalisationItem(culture, contextKey, key);
		}


		/// <summary>
		/// Gets the degree education levels by program area.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
    /// <returns>An array of cascading drop-down items</returns>
    [OperationContract]
		[WebInvoke(ResponseFormat = WebMessageFormat.Json)]
		public CascadingDropDownNameValue[] GetDegreeEducationLevelsByProgramArea(string knownCategoryValues, string category)
		{
		  return AjaxHelper.GetDegreeEducationLevelsByProgramArea(knownCategoryValues, category);
		}

    /// <summary>
    /// Gets the degree education levels by program area.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns>An array of cascading drop-down items</returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetDegreeEducationLevelsByProgramAreaWithoutAll(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetDegreeEducationLevelsByProgramArea(knownCategoryValues, category, false);
    }


    /// <summary>
    /// Gets the industry detail.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetIndustryDetail(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetIndustryDetails(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets the MSA.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetMSA(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetMetropolitanStatisticalAreas(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets jobs by career area.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetJobsByCareerArea(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetJobsByCareerAreaId(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets jobs by career area (filtering out ronets that map to an onet with no tasks).
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetFilteredJobsByCareerArea(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetJobsByCareerAreaId(knownCategoryValues, category, true);
    }

		/// <summary>
		/// Gets the resume text.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		[OperationContract]
		public string GetResumeText(long personId)
		{
			return AjaxHelper.GetResumeText(personId);
		}

		/// <summary>
		/// Gets the staff members.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		[OperationContract]
		public CascadingDropDownNameValue[] GetStaffMembers(string knownCategoryValues, string category)
		{
			return AjaxHelper.GetStaffMembers(knownCategoryValues, category);
		}

		/// <summary>
		/// Gets the staff members.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		[OperationContract]
    public AutoCompleteItem[] GetNonStatewideStaffMemberNames(string prefixText, int count)
		{
			return AjaxHelper.GetNonStatewideStaffMemberNames(prefixText, count);
		}

		/// <summary>
		/// Sets selected job seeker IDs for use in job seeker messaging (EDU)
		/// </summary>
		/// <param name="ids">The selected job seeker IDs.</param>
		[OperationContract]
		public void SetJobSeekerIds(string[] ids)
		{
			HttpContext.Current.Session["MessageJobSeekers:JobSeekerIds"] = ids;
		}

		#endregion

		#region Talent / Assist type aheads


		/// <summary>
		/// Gets the degree education level auto complete items.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="contextKey">The context key.</param>
		/// <returns></returns>
		[WebInvoke(ResponseFormat = WebMessageFormat.Json)]
		public string[] GetDegreeEducationLevelAutoCompleteItems(string prefixText, string contextKey)
		{
			DetailedDegreeLevels detailedDegreeLevel;
			Enum.TryParse(contextKey, true, out detailedDegreeLevel);

			var degreeEducationLevels = ServiceClientLocator.ExplorerClient(_app).GetDegreeAliases(0, prefixText, false, detailedDegreeLevel);

			var items = new List<string>();
			var serializer = new JavaScriptSerializer();

			foreach (var degreeEducationLevel in degreeEducationLevels)
			{
				items.Add(AutoCompleteExtender.CreateAutoCompleteItem(degreeEducationLevel.AliasExtended, serializer.Serialize(degreeEducationLevel)));
			}
			
			return items.ToArray();
		}

		#endregion

    #region Reporting type aheads

    /// <summary>
    /// Gets the job seeker certifications and licences.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetReportingCertificationAndLicenses(string prefixText, int count)
    {
			return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.CertificationLicenses, prefixText);
    }

    /// <summary>
    /// Gets the job seeker skills.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetReportingSkills(string prefixText, int count)
    {
			return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.Skills, prefixText);
    }

    /// <summary>
    /// Gets the job seeker internships.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetReportingInternships(string prefixText, int count)
    {
			return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.Internships, prefixText);
    }

    /// <summary>
    /// Gets the job order Onet occupation groups
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetReportingOnetOccupationGroups(string prefixText, int count)
    {
      return new[] { "Group 1", "Group 2" };
    }
    /// <summary>
    /// Gets the Onet detailed occupations.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetReportingOnetDetailedOccupations(string prefixText, int count)
    {
      return new[] { "Occupation 1", "Occupation 2", "Occupation 3" };
    }

    /// <summary>
    /// Gets the activities.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetActivities(string knownCategoryValues, string category)
    {
      var activitiesValues = new List<CascadingDropDownNameValue>();

      long categoryId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out categoryId);
      if (categoryId > 0)
				activitiesValues.AddRange(ServiceClientLocator.CoreClient(_app).GetActivitiesLookUp(categoryId).Select(activity => new CascadingDropDownNameValue(activity.Value, activity.Key.ToString())));

      activitiesValues.Insert(0, new CascadingDropDownNameValue(CodeLocalise("Activities.Top.NoEdit", "- Activities/Services -"), "", true));

      return activitiesValues.ToArray();
    }

		[OperationContract]
		[WebGet]
		public ActivityViewDto[] GetAllActivities()
		{
			return ServiceClientLocator.CoreClient(_app).GetAllActivities();
		}

		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json)]
		public string SaveActivities(ActivityViewDto[] data)
		{
			return ServiceClientLocator.CoreClient(_app).SaveActivities(data.ToList());
		}

    [OperationContract]
    public string[] GetReportingMOCCOdes(string prefixText, int count)
    {
			return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.MilitaryOccupationCodes, prefixText);
    }

    [OperationContract]
    public string[] GetReportingMOCTitles(string prefixText, int count)
    {
			return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.MilitaryOccupationTitles, prefixText);
    }

    [OperationContract]
    public string[] GetReportingActivityCategories(string prefixText, int count)
    {
      throw new NotImplementedException();
    }

    [OperationContract]
    public string[] GetReportingEmployerNames(string prefixText, int count)
    {
			return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.EmployerNames, prefixText);
    }

    [OperationContract]
    public string[] GetReportingEmployerFEIN(string prefixText, int count)
    {
			return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.EmployerFEIN, prefixText);
    }

    [OperationContract]
    public string[] GetReportingJobOrderTitles(string prefixText, int count)
    {
			return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.JobOrderTitles, prefixText);
    }

    /// <summary>
    /// Gets the employer counties
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetReportingEmployerCounties(string prefixText, int count)
    {
      return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.EmployerCounties, prefixText);
    }

    /// <summary>
    /// Gets the job order counties
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetReportingJobOrderCounties(string prefixText, int count)
    {
      return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.JobOrderCounties, prefixText);
    }

    /// <summary>
    /// Gets the job seeker counties
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetReportingJobSeekerCounties(string prefixText, int count)
    {
      return ServiceClientLocator.ReportClient(_app).GetReportLookUpData(ReportLookUpType.JobSeekerCounties, prefixText);
    }

    /// <summary>
    /// Gets the known category value from the known category values.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <returns></returns>
    private static string GetKnownCategoryValue(string knownCategoryValues)
    {
      if (string.IsNullOrEmpty(knownCategoryValues)) return knownCategoryValues;
      if (knownCategoryValues.LastIndexOf(":") > 0) knownCategoryValues = knownCategoryValues.Substring(knownCategoryValues.LastIndexOf(":") + 1);
      if (knownCategoryValues.LastIndexOf(";") > 0) knownCategoryValues = knownCategoryValues.Substring(0, knownCategoryValues.LastIndexOf(";"));
      return knownCategoryValues;
    }

    #endregion

    #region NAICS

    /// <summary>
    /// Checks if a NAICS exists
    /// </summary>
    /// <param name="naicsText">The NAICS.</param>
    /// <param name="childrenOnly">Whether to check "child" NAICS only (i.e. exluce NAICS where ParentId = 0)</param>
    /// <returns>A boolean indicating existence</returns>
    [OperationContract]
    public bool CheckNaicsExists(string naicsText, bool childrenOnly = false)
    {
      return AjaxHelper.CheckNaicsExists(naicsText, childrenOnly);
    }

    #endregion

    #region Localisation Item Update

	  /// <summary>
	  /// Updates the localisation item.
	  /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="localisedValue">The localised value.</param>
    /// <param name="defaultValue">The default value</param>
    /// <returns></returns>
	  /// <returns></returns>
	  [OperationContract]
    public void UpdateLocalisationItem(string key, string localisedValue, string defaultValue)
	  {
	    var localisationModeHelper = new LocalisationModeHelper(_app);
      localisationModeHelper.SavePortalLocalisationItem(key, localisedValue, defaultValue);
    }

    #endregion

    #region Upload File

    /// <summary>
    /// Gets the processing state of an upload file.
    /// </summary>
    /// <param name="uploadFileId">The id of the upload file</param>
    /// <returns>The processing state</returns>
    [OperationContract]
    public int GetUploadFileProcessingState(long uploadFileId)
    {
      return AjaxHelper.GetUploadFileProcessingState(uploadFileId);
    }

    #endregion

		#region Documents

		/// <summary>
		/// Gets the document groups for a given document category
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		[OperationContract]
		public CascadingDropDownNameValue[] GetDocumentGroups(string knownCategoryValues, string category)
		{
			var groupValues = new List<CascadingDropDownNameValue>();

			long categoryId;
			long.TryParse(GetKnownCategoryValue(knownCategoryValues), out categoryId);
			if (categoryId > 0)
			{
				groupValues.AddRange(ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.DocumentGroups, categoryId)
																																	.Select(group => new CascadingDropDownNameValue(group.Text, group.Id.ToString(CultureInfo.InvariantCulture))));
			}

			return groupValues.ToArray();
		}

		#endregion

	}
}
