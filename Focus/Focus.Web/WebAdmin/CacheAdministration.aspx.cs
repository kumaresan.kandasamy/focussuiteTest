﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;

using Focus.Common;
using Focus.Core;

#endregion

namespace Focus.Web.WebAdmin
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistSystemAdministrator)]
  public partial class CacheAdministration : PageBase
	{
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Locallise();
        CacheTypeCheck();
      }
    }
    
    private void Locallise()
    {
      ResetCacheButton.Text = CodeLocalise("ResetCacheButton.Text", "Reset cache");
    }

    private void CacheTypeCheck()
    {
      if (App.Settings.CacheMethod == CachingMethod.HTTP)
      {
        ResetResultLiteral.Text = CodeLocalise("ResetCache.HTTP.Text", "As this system is using HTTP caching, the cache will need to be reset for all applications in the suite. (And on each web server if in a farm) Use IIS to do this");
        ResetCacheButton.Enabled = false;
        ResetResultLiteral.Visible = true;
      }
    }

    protected void ResetCacheButton_Clicked(object sender, EventArgs e)
    {
      var result = ServiceClientLocator.ProcessorClient(App).ResetCache();

      ResetResultLiteral.Visible = true;
      if (result != string.Empty)
        ResetResultLiteral.Text = CodeLocalise("ResetCache.Error.Text", "There was an error resetting the cache: {0}", result);
      else
        ResetResultLiteral.Text = CodeLocalise("ResetCache.Success.Text", "Cache reset");
    }
  }
}