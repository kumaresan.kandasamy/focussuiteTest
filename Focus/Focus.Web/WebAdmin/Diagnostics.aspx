﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Sandbox01.aspx.cs" Inherits="Focus.Web.WebAdmin.Diagnostics" %>
<%@ Import Namespace="Focus" %>



<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server"></asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	
<table>
	<tr><td width = "350">ResumeReferralApprovalsMinimumStars</td><td><asp:Label runat="server" ID="ResumeReferralApprovalsMinimumStarsLabel"></asp:Label></td>	</tr>
	<tr><td>ResumeReferralApprovalsMinimumStars (Cached)</td><td><asp:Label runat="server" ID="ResumeReferralApprovalsMinimumStarsCachedLabel"></asp:Label></td></tr>

		
</table>

<asp:Button runat="server" ID="RefreshAppSettingsButton" Text="RefreshAppSettings" OnClick="RefreshAppSettings_Click"/>
<asp:Button runat="server" ID="SetContextButton" Text="SetContext" OnClick="SetContext_Click"/>

<br/><br/>

Current Theme: <%= OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Workforce ? "Workforce" : "Education" %><br/>
<asp:Button runat="server" ID="ChangeThemeButton" OnClick="ChangeThemeButton_Click" Text="??" />

</asp:Content>