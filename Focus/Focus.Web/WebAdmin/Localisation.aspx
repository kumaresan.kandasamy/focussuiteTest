﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Localisation.aspx.cs" Inherits="Focus.Web.WebAdmin.Localisation" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	
	<asp:HiddenField ID="LocalisationModalDummyTarget" runat="server" />
	<act:ModalPopupExtender ID="LocalisationModal" runat="server" 
													TargetControlID="LocalisationModalDummyTarget"
													PopupControlID="LocalisationPanel"
													PopupDragHandleControlID="LocalisationPanel"
													RepositionMode="RepositionOnWindowResizeAndScroll"
													BackgroundCssClass="modalBackground" />
                            
	<asp:Panel ID="LocalisationPanel" runat="server" CssClass="modal" ClientIDMode="Static" >			
		<div style="float:left;">
			<table>		
				<tr>
					<th colspan="4">Localisation</th>
				</tr>
				<tr>
					<td style="vertical-align:top; width:300px;">
						<table style="width: 100%;" role="presentation">
							<tr>
								<td>
									Context:<br />
									<asp:DropDownList ID="ContextKeyDropDownList" runat="server" Width="100%" onchange="GetLocalisationItems();"  />
								</td>
							</tr>
							<tr>
								<td>
									<asp:ListBox ID="LocalisableItemsListBox" runat="server" Height="435px" Width="100%" onclick="GetLocalisationItem();" />
								</td>
							</tr>
						</table>
					</td>
					<td style="width:10px;"/>
					<td style="vertical-align:top; width:490px;" colspan="2">
						<table style="width: 100%;" role="presentation">
							<tr>
								<td>
									Languages:<br />
									<asp:DropDownList ID="LanguageDropDownList" runat="server" Width="100%" onchange="GetLocalisationItem();" />
								</td>
							</tr>
							<tr>
								<td>
									Value:<br />
									<asp:TextBox ID="ValueTextBox" runat="server" TextMode="MultiLine" Rows="20" Height="412px" Width="477px" />
									<asp:HiddenField ID="ValueIdHiddenField" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:Label ID="StatusLabel" runat="server" EnableViewState="false" Text="Ready" />
					</td>
					<td style="text-align:right;">
						<asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="button3" />&nbsp;
						<asp:Button ID="DeleteButton" runat="server" Text="Delete" CssClass="button3" />
					</td>
				</tr>
			</table>	
		</div>
		<div class="closeIcon"><img src="<%= UrlBuilder.Content("/Assets/Images/button_x_close_off.png") %>" alt="Close" onclick="" /></div>
	
	</asp:Panel>

	<script type="text/javascript">
		$(document).ready(
			function() 
			{
				$("#<%= LanguageDropDownList.ClientID %>").attr('disabled', '');
				$("#<%= LocalisableItemsListBox.ClientID %>").attr('disabled', '');
				$("#<%= ValueTextBox.ClientID %>").attr('disabled', '');
			}
		);

		function GetLocalisationItems()
		{
			var contextKey = $("#<%= ContextKeyDropDownList.ClientID %>").val();

			$("#<%= LocalisableItemsListBox.ClientID %>").html('');
			ShowStatus("success", "Getting localisation items: " + contextKey);

			var options =
			{
				url: "<%= UrlBuilder.AjaxService() %>/GetLocalisationItems",
				type: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: '{"contextKey": "' + contextKey + '"}',
				success: function (response)
				{
					var rt = response.d;
					if (rt.Success)
					{
						$("#<%= LocalisableItemsListBox.ClientID %>").html('');

						for (i = 0; i < rt.Data.length; i++)
							$('<option value="' + rt.Data[i].Value + '">' + rt.Data[i].Name + '</option>').appendTo("#<%= LocalisableItemsListBox.ClientID %>");

						$("#<%= LanguageDropDownList.ClientID %>").removeAttr('disabled');
						$("#<%= LocalisableItemsListBox.ClientID %>").removeAttr('disabled');						
						$("#<%= ValueTextBox.ClientID %>").removeAttr('disabled');

						ShowStatus("success", rt.Message);
					}
					else
					{
						$("#<%= LanguageDropDownList.ClientID %>").attr('disabled', '');
						$("#<%= LocalisableItemsListBox.ClientID %>").attr('disabled', '');
						$("#<%= ValueTextBox.ClientID %>").attr('disabled', '');
						ShowStatus("error", rt.Message);
					}
				}
			};

			$.ajax(options);
			return false;
		}

		function GetLocalisationItem()
		{
			var culture = $("#<%= LanguageDropDownList.ClientID %>").val();
			var contextKey = $("#<%= ContextKeyDropDownList.ClientID %>").val();
			var key = $("#<%= LocalisableItemsListBox.ClientID %>").val();

			$("#<%= ValueTextBox.ClientID %>").val('');
			ShowStatus("success", "Getting localisation item: " + contextKey);

			var options =
			{
				url: "<%= UrlBuilder.AjaxService() %>/GetLocalisationItem",
				type: "POST",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				data: '{"culture": "' + culture + '", "contextKey": "' + contextKey + '", "key": "' + key + '" }',
				success: function (response)
				{
					var rt = response.d;
					if (rt.Success)
					{
						$("#<%= ValueTextBox.ClientID %>").val(rt.Data.Value);
						$("#<%= ValueIdHiddenField.ClientID %>").val(rt.Data.Id);
						
						ShowStatus("success", rt.Message);
					}
					else
						ShowStatus("error", rt.Message);
				}
			};

			$.ajax(options);
			return false;
		}

		function ShowStatus(status, message)
		{
			if (status == "error")
				$("#<%= StatusLabel.ClientID %>").html("<font color='red'><strong>" + message + "</strong></font>");
			else
				$("#<%= StatusLabel.ClientID %>").html(message);

			return false;
		}
		</script>

	
</asp:Content>


