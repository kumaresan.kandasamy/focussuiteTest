﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SendImportedUserEmails.aspx.cs" Inherits="Focus.Web.WebAdmin.SendImportedUserEmails" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:PlaceHolder runat="server" ID="SendEmailsPanel">
      <h2>
        <asp:Literal runat="server" ID="EmailsToSendHeader"></asp:Literal>
      </h2>
      <p>
        <asp:Literal runat="server" ID="EmailsToSendLiteral"></asp:Literal>&nbsp;
        <asp:TextBox runat="server" ID="EmailsToSendTextBox"></asp:TextBox>
      </p>
      <asp:Button runat="server" ID="SendEmailsButton" CssClass="button3" OnClick="SendEmailsButton_Clicked" />
    </asp:PlaceHolder>
    <p>
      <asp:Literal runat="server" ID="SendEmailsResultLiteral" />
    </p>
</asp:Content>