﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;

using Focus.Common;
using Focus.Core;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebAdmin
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistSystemAdministrator)]
  public partial class SendImportedUserEmails : PageBase
  {
    private MigrationEmailRecordType RecordType
    {
      get { return GetViewStateValue<MigrationEmailRecordType>("SendImportedUserEmails:RecordType"); }
      set { SetViewStateValue("SendImportedUserEmails:RecordType", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    private void Localise()
    {
      MigrationEmailRecordType emailType;
      
      var type = Page.RouteData.Values["type"].ToString().ToLowerInvariant();
      var validType = Enum.TryParse(type, true, out emailType);

      if (validType)
      {
        RecordType = emailType;

        var displayText = (RecordType == MigrationEmailRecordType.JobSeeker) 
          ? CodeLocalise("JobSeeker.Text", "#CANDIDATETYPES#") 
          : CodeLocalise("Employee.Text", "Employees");

        if (Page.RouteData.Values.ContainsKey("number"))
          EmailsToSendTextBox.Text = Page.RouteData.Values["number"].ToString();

        EmailsToSendHeader.Text = CodeLocalise("EmailsToSendHeader.Text", "Imported {0} - Reset Password Emails", displayText);
        EmailsToSendLiteral.Text = CodeLocalise("EmailsToSendLiteral.Text", "Number of emails to send");
        SendEmailsButton.Text = CodeLocalise("SendEmailsButton.Text", "Send emails");
      }
      else
      {
        SendEmailsPanel.Visible = false;
        SendEmailsResultLiteral.Text = CodeLocalise("InvalidType.Error", "Type must be either 'jobseeker' or 'employer'");
      }
    }

    protected void SendEmailsButton_Clicked(object sender, EventArgs e)
    {
      var emailsToSend = int.Parse(EmailsToSendTextBox.Text);

      var resetPasswordUrl = (RecordType == MigrationEmailRecordType.JobSeeker)
                               ? string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.ResetPassword(false))
                               : string.Concat(App.Settings.TalentApplicationPath, UrlBuilder.ResetPassword(false));

      var result = ServiceClientLocator.ProcessorClient(App).SendEmailsToImportedUsers(emailsToSend, RecordType, resetPasswordUrl);

      SendEmailsResultLiteral.Text = CodeLocalise("EmailsSent.Success.Text", "Emails sent: {0}", result);
    }
  }
}