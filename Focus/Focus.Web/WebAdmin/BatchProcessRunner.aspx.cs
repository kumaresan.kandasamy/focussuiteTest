﻿#region Copyright © 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Common;
using Focus.Core;

using Framework.Core;
using Framework.DataAccess;
using Framework.Messaging;

#endregion

namespace Focus.Web.WebAdmin
{
	public partial class BatchProcessRunner : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			var identifier = Page.RouteData.Values.ContainsKey("id") ? Page.RouteData.Values["id"].ToString() : string.Empty;

		  if (identifier.IsNotNullOrEmpty())
		  {
		    ResultLabel.Text = ServiceClientLocator.ProcessorClient(App).EnqueueBatchProcess(identifier);
		  }
      else if (Page.RouteData.Values.ContainsKey("process"))
      {
        if (!App.User.IsInRole("AssistSystemAdministrator"))
          throw new Exception("Invalid access");

        var process = (BatchProcesses)(Page.RouteData.Values["process"].AsInt());
        var schedule = new MessageSchedulePlan
        {
          Interval = Page.RouteData.Values.ContainsKey("interval") ? Page.RouteData.Values["interval"].AsInt() : 0
        };

        if (Page.RouteData.Values.ContainsKey("minutes"))
        {
          schedule.StartMinutes = Page.RouteData.Values["minutes"].AsInt();
          schedule.ScheduleType = MessageScheduleType.Hourly;
        }
        else if (Page.RouteData.Values.ContainsKey("hhmm"))
        {
          var startTime = Page.RouteData.Values["hhmm"].ToString().PadLeft(4, '0');
          schedule.StartHour = startTime.Substring(0, 2).AsInt();
          schedule.StartMinutes = startTime.Substring(2, 2).AsInt();
          
          if (Page.RouteData.Values.ContainsKey("day"))
          {
            schedule.StartDay = (DayOfWeek) (Page.RouteData.Values["day"].ToString().AsInt());
            schedule.ScheduleType = MessageScheduleType.Weekly;
          }
          else
          {
            schedule.ScheduleType = MessageScheduleType.Daily;
          }
        }
        else
        {
          schedule.ScheduleType = MessageScheduleType.Minutes;
        }

        ResultLabel.Text = ServiceClientLocator.ProcessorClient(App).EnqueueBatchProcess(process, schedule);
      }
      else
      {
		    ResultLabel.Text = CodeLocalise("InvalidRequest.Text", "Bad Request");
		  }
		}
	}
}