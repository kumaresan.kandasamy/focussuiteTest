﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CacheAdministration.aspx.cs" Inherits="Focus.Web.WebAdmin.CacheAdministration" %>
    <%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Button runat="server" ID="ResetCacheButton" CssClass="button3" OnClick="ResetCacheButton_Clicked" /><br/>
    <asp:Literal runat="server" ID="ResetResultLiteral" Visible="False" />
</asp:Content>
