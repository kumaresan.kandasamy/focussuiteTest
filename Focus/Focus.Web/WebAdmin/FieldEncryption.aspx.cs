﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Common;

#endregion

namespace Focus.Web.WebAdmin
{
  public partial class FieldEncryption : PageBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      var key = Page.RouteData.Values["key"].ToString();
      var decrypt = Page.RouteData.Values.ContainsKey("direction") && Page.RouteData.Values["direction"].ToString().Equals("decrypt", StringComparison.OrdinalIgnoreCase);

      if (!decrypt && !App.Settings.EncryptionEnabled)
      {
        ResultLabel.Text = CodeLocalise("EncryptionNotEnabled.Text", "Encryption is not enabled.");
        return;
      }

      if (decrypt && App.Settings.EncryptionEnabled)
      {
        ResultLabel.Text = CodeLocalise("EncryptionNotEnabled.Text", "Encryption is still enabled.");
        return;
      }

      if (!key.Equals(App.Settings.EncryptionKey))
      {
        ResultLabel.Text = CodeLocalise("InvalidKey.Text", "Invalid key specified.");
        return;
      }

      var alreadyProcessed = decrypt ? ServiceClientLocator.ProcessorClient(App).DecryptField() : ServiceClientLocator.ProcessorClient(App).EncryptField();
      ResultLabel.Text = alreadyProcessed ? "Field has already been converted" : "Field has now been converted";
    }
  }
}