﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core.Settings;
using Framework.Core;

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.Web.WebAdmin
{
	//[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.Admin)]
	//[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.SystemAdmin)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistAccountAdministrator)]
	public partial class Configuration : PageBase
	{
		private ConfigSettingInfo[] _configSettings;
		
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				BindConfigSettingCategoryDropDownList();

			var appSettings = App.Settings;
			_configSettings = appSettings.GetConfigSettings();

			ConfigSettingCategory category;
			var isValidCategory = Enum.TryParse(ConfigSettingCategoryDropDownList.SelectedValue, out category);

			BindSubCategories(isValidCategory ? category : (ConfigSettingCategory?)null);

		}


		//protected void ConfigSettingCategorySelectionChanged (object sender, EventArgs e)
		//{
		//  Confirmation.Show("Confirm Config setting category change",
		//                    "Warning!<br/><br/>Changing category setting without first saving will lose all changes!<br/>Do you wish to continue?",
		//                    "Ok",
		//                    "<ENTERCLOSECOMMANDNAME>",
		//                    "lastselected?",
		//                    "<ENTEROKTEXT>",
		//                    "<ENTEROKCOMMAND>"
		//                    );

		//  Confirmation.Show(title: "Confirm save", "Do you wish to save these settings?", "Cancel", closeCommandName: "",
		//                      null, "Ok", "SaveDialogOkClicked");
		//}

		protected void SubCategoryRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			var item = e.Item;

			if ((item.ItemType == ListItemType.Item) || (item.ItemType == ListItemType.AlternatingItem))
			{
				var settingRepeater = (Repeater)item.FindControl("SettingRepeater");
				var subCategory = (string)item.DataItem;

				settingRepeater.DataSource = _configSettings.Where(x => x.SubCategory == subCategory).OrderBy(x => x.DisplayOrder);
				settingRepeater.DataBind();
			}
		}

		protected void SettingRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
		{
		if ( (e.Item.ItemType != ListItemType.Item) && (e.Item.ItemType != ListItemType.AlternatingItem))
		{
			return;
		}

			var rowDiv = new Panel();

		var isWebConfigSetting = ((ConfigSettingType)DataBinder.Eval(e.Item.DataItem, "Type") == ConfigSettingType.Web);

			var settingLabel = new Label
			{
				ID = DataBinder.Eval(e.Item.DataItem, "Name") + "^Label",
				Text = DataBinder.Eval(e.Item.DataItem, "DisplayName").ToString(),
				ToolTip = DataBinder.Eval(e.Item.DataItem, "Description").ToString()
			};
			rowDiv.Controls.Add(settingLabel);

			if (isWebConfigSetting)
			{
				var settingStringTextBox = new TextBox
				{
					ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^TextBox",
					Width = 450,
					MaxLength = 40,
					Text = "N/A - Web.config setting",
					Enabled = false
				};
				
				settingLabel.AssociatedControlID = settingStringTextBox.ID;
				rowDiv.Controls.Add(settingStringTextBox);
			}

			else
			{
				// ItemTemplate item if the property is null.
				if (DataBinder.Eval(e.Item.DataItem, "CurrentValue").IsNull())
				{
					var settingStringTextBox = new TextBox
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^TextBox",
						Width = 450,
						MaxLength = 75,
						Text = "NULL",
						Enabled = true
					};

					var blankFieldValidator = new RequiredFieldValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_BlankValidator",
						ControlToValidate = settingStringTextBox.ID,
						ErrorMessage = "Field cannot be blank."
					};

					settingLabel.AssociatedControlID = settingStringTextBox.ID;
					rowDiv.Controls.Add(settingStringTextBox);
					//rowDiv.Controls.Add(blankFieldValidator);
				}

				// ItemTemplate item if the property is a string.
				if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is string)
				{
					var settingStringTextBox = new TextBox
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^TextBox",
						Width = 450,
						MaxLength = 75,
						Text = DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString(),
						Enabled = true
					};

					var blankFieldValidator = new RequiredFieldValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_BlankValidator",
						ControlToValidate = settingStringTextBox.ID,
						ErrorMessage = "Field cannot be blank."
					};

					settingLabel.AssociatedControlID = settingStringTextBox.ID;
					rowDiv.Controls.Add(settingStringTextBox);
					//rowDiv.Controls.Add(blankFieldValidator);
				}

				// Itemtemplate item if the property is a boolean.
				else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is bool)
				{
					var settingBoolCheckbox = new CheckBox
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^CheckBox",
						Enabled = true
					};

					var hiddenBoolValue = new HiddenField
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^CheckBox_Hidden",
						Value = "false"
					};

					settingBoolCheckbox.InputAttributes.Add("Value", DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString().ToLower());
					settingBoolCheckbox.Checked = (bool)DataBinder.Eval(e.Item.DataItem, "CurrentValue");
					settingLabel.AssociatedControlID = settingBoolCheckbox.ID;
					rowDiv.Controls.Add(settingBoolCheckbox);
					rowDiv.Controls.Add(hiddenBoolValue);
				}

				// Itemtemplate item if property is an int.
				else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is int)
				{
					var settingIntTextBox = new TextBox
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^TextBox",
						Width = 450,
						MaxLength = 50,
						Text = DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString(),
						Enabled = true,
					};

					var blankFieldValidator = new RequiredFieldValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_BlankValidator",
						ControlToValidate = settingIntTextBox.ID,
						ErrorMessage = "Field cannot be blank."
					};

					var positiveIntValidator = new CompareValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_IntValidator",
						ControlToValidate = settingIntTextBox.ID,
						Type = ValidationDataType.Integer,
						Operator = ValidationCompareOperator.GreaterThan,
						ValueToCompare = "-1",
						ErrorMessage = "This field must be a positive number."
					};

					settingLabel.AssociatedControlID = settingIntTextBox.ID;
					rowDiv.Controls.Add(settingIntTextBox);
					//rowDiv.Controls.Add(blankFieldValidator);
					if (!(DataBinder.Eval(e.Item.DataItem, "Key").ToString().Equals("CareerSearchRadius")))
						rowDiv.Controls.Add(positiveIntValidator);
				}

				// ItemTemplate item if the property is a long.
				else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is long)
				{
					var settingLongTextBox = new TextBox
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^TextBox",
						Width = 450,
						MaxLength = 50,
						Text = DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString(),
						Enabled = true,
					};

					var blankFieldValidator = new RequiredFieldValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_BlankValidator",
						ControlToValidate = settingLongTextBox.ID,
						ErrorMessage = "Field cannot be blank."
					};

					var positiveLongValidator = new CompareValidator

					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_LongValidator",
						ControlToValidate = settingLongTextBox.ID,
						Type = ValidationDataType.Integer,
						Operator = ValidationCompareOperator.GreaterThan,
						ValueToCompare = "0",
						ErrorMessage = "This field must be a positive number."
					};

					settingLabel.AssociatedControlID = settingLongTextBox.ID;
					rowDiv.Controls.Add(settingLongTextBox);
					//rowDiv.Controls.Add(blankFieldValidator);
					if (!(DataBinder.Eval(e.Item.DataItem, "Key").ToString().Equals("CareerSearchRadius")))
						rowDiv.Controls.Add(positiveLongValidator);
				}

				// ItemTemplate item if the property is a string array.
				else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is string[])
				{
					var settingStringArrayTextBox = new TextBox
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^TextBox",
						Width = 450,
						MaxLength = 100,
					};

					var blankFieldValidator = new RequiredFieldValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_BlankValidator",
						ControlToValidate = settingStringArrayTextBox.ID,
						ErrorMessage = "Field cannot be blank."
					};

					var settingStringArrayValidator = new RegularExpressionValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_StringRegexValidator",
						ControlToValidate = settingStringArrayTextBox.ID,
						ValidationExpression = @"^[A-Za-z0-9().+,]+[A-Za-z0-9()*]$",
						ErrorMessage = "This field requires a comma separated list of strings."
					};

					var values = String.Join(",", DataBinder.Eval(e.Item.DataItem, "CurrentValue") as string[] ?? new string[0]);
					settingStringArrayTextBox.Text = values;
					settingStringArrayTextBox.Enabled = true;

					settingLabel.AssociatedControlID = settingStringArrayTextBox.ID;
					rowDiv.Controls.Add(settingStringArrayTextBox);
					rowDiv.Controls.Add(settingStringArrayValidator);
					//rowDiv.Controls.Add(blankFieldValidator);
				}

					// ItemTemplate item if the property is an int array.
				else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is int[])
				{
					var settingIntArrayTextBox = new TextBox
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^TextBox",
						Width = 450,
						MaxLength = 100,
					};

					var blankFieldValidator = new RequiredFieldValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_BlankValidator",
						ControlToValidate = settingIntArrayTextBox.ID,
						ErrorMessage = "Field cannot be blank."
					};

					var settingIntArrayValidator = new RegularExpressionValidator
					{
						ID = DataBinder.Eval(e.Item.DataItem, "Name") + "_IntRegexValidator",
						ControlToValidate = settingIntArrayTextBox.ID,
						ValidationExpression = @"^([0-9]+,)*[0-9]+$",
						ErrorMessage = "This field requires a comma separated list of positive integers."
					};

					var values = String.Join(",", DataBinder.Eval(e.Item.DataItem, "CurrentValue") as int[] ?? new int[0]);
					settingIntArrayTextBox.Text = values;
					settingIntArrayTextBox.Enabled = true;

					settingLabel.AssociatedControlID = settingIntArrayTextBox.ID;
					rowDiv.Controls.Add(settingIntArrayTextBox);
					//rowDiv.Controls.Add(blankFieldValidator);
					rowDiv.Controls.Add(settingIntArrayValidator);
				}

				// ItemTemplate item if the item is an Eumerated type.
				else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is Enum)
				{
					var settingDropDown = new DropDownList
					{
						ID = "ConfigItem_" + DataBinder.Eval(e.Item.DataItem, "Name") + "^DropDown"
					};

					if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is FocusModules)
					{
						var modules = Enum.GetNames(typeof(FocusModules));

						// Want the current value selected in the drop down.
						var selected = modules.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var module in modules.Where(module => module != selected))
						{
							settingDropDown.Items.Add(module);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is JobAlertsOptions)
					{
						var alertOptions = Enum.GetNames(typeof(JobAlertsOptions));

						// Want the current value selected in the drop down.
						var selected = alertOptions.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var option in alertOptions.Where(module => module != selected))
						{
							settingDropDown.Items.Add(option);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is AlertFrequencies)
					{
						var alertFreqs = Enum.GetNames(typeof(AlertFrequencies));

						// Want the current value selected in the drop down.
						var selected = alertFreqs.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var freq in alertFreqs.Where(module => module != selected))
						{
							settingDropDown.Items.Add(freq);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is DistanceUnits)
					{
						var distUnits = Enum.GetNames(typeof(DistanceUnits));

						// Want the current value selected in the drop down.
						var selected = distUnits.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var unit in distUnits.Where(module => module != selected))
						{
							settingDropDown.Items.Add(unit);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is RegistrationRoute)
					{
						var distUnits = Enum.GetNames(typeof(RegistrationRoute));

						// Want the current value selected in the drop down.
						var selected = distUnits.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var unit in distUnits.Where(module => module != selected))
						{
							settingDropDown.Items.Add(unit);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is SearchCentrePointOptions)
					{
						var scpOptions = Enum.GetNames(typeof(SearchCentrePointOptions));

						// Want the current value selected in the drop down.
						var selected = scpOptions.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var option in scpOptions.Where(module => module != selected))
						{
							settingDropDown.Items.Add(option);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is ResumeSearchableOptions)
					{
						var rsOptions = Enum.GetNames(typeof(ResumeSearchableOptions));

						// Want the current value selected in the drop down.
						var selected = rsOptions.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var option in rsOptions.Where(module => module != selected))
						{
							settingDropDown.Items.Add(option);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is DegreeFilteringType)
					{
						var rsOptions = Enum.GetNames(typeof(DegreeFilteringType));

						// Want the current value selected in the drop down.
						var selected = rsOptions.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var option in rsOptions.Where(module => module != selected))
						{
							settingDropDown.Items.Add(option);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is CareerExplorerFeatureEmphasis)
					{
						var rsOptions = Enum.GetNames(typeof(CareerExplorerFeatureEmphasis));

						// Want the current value selected in the drop down.
						var selected = rsOptions.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var option in rsOptions.Where(module => module != selected))
						{
							settingDropDown.Items.Add(option);
						}
					}

					else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is SchoolTypes)
					{
						var rsOptions = Enum.GetNames(typeof(SchoolTypes));

						// Want the current value selected in the drop down.
						var selected = rsOptions.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var option in rsOptions.Where(module => module != selected))
						{
							settingDropDown.Items.Add(option);
						}
					}

          else if (DataBinder.Eval(e.Item.DataItem, "CurrentValue") is TalentApprovalOptions)
					{
            var rsOptions = Enum.GetNames(typeof(TalentApprovalOptions));

						// Want the current value selected in the drop down.
						var selected = rsOptions.SingleOrDefault(x => x == (DataBinder.Eval(e.Item.DataItem, "CurrentValue").ToString()));
						settingDropDown.Items.Add(selected);

						foreach (var option in rsOptions.Where(module => module != selected))
						{
							settingDropDown.Items.Add(option);
						}
					}

					settingLabel.AssociatedControlID = settingDropDown.ID;
					rowDiv.Controls.Add(settingDropDown);
				}

			}
			e.Item.Controls.Add(rowDiv);
			//rowDiv.Controls.Add(new Literal() { Text = "<br />" });
		}

		private void BindSubCategories(ConfigSettingCategory? category)
		{
			SubCategoryRepeater.DataSource = _configSettings.Where(x => x.Category == category).Select(x => x.SubCategory).Distinct().OrderBy(x => x);
			SubCategoryRepeater.DataBind();
		}

		protected bool CheckAgainstCurrent(string control, string controlType, object configItemValue, ConfigSettingInfo setting)
		{
			//var sb = new StringBuilder();

			switch (controlType)
			{
				case ("TextBox"):
					return configItemValue.Equals(setting.CurrentValue);

				case ("TextBoxIntOrLong"):
					return configItemValue.Equals(setting.CurrentValue.ToString());

				case ("TextBoxIntArray"):
					return configItemValue.Equals(String.Join(",", setting.CurrentValue as int[]));

				case ("TextBoxStringArray"):
					return configItemValue.Equals(String.Join(",", setting.CurrentValue as string[]));

				case ("CheckBox"):
					return configItemValue.Equals(setting.CurrentValue.ToString());

				case ("CheckBox_Hidden"):
					return configItemValue.Equals(setting.CurrentValue.ToString());

				case ("DropDownFocusModules"):
					return configItemValue.Equals(setting.CurrentValue.ToString());

			}

			return false;
		}

		//protected void SaveChangesButtonClick (object sender, EventArgs e)
		//{
		//  Confirmation.Show(title: "Save confirmation", details: "Do you wish to save these settings?", closeText: "Cancel",
		//                      closeCommandName: "SaveDialogCancelClicked", okText: "OK", okCommandName: "SaveChanges");



		//}

		//protected void SaveDialogCancelClicked(object sender, EventArgs e)
		//{
		//  //nothing
		//  Debug.WriteLine("SaveDialogCancelClicked");
		//}

		/// <summary>
		/// Handles the Click event of the SaveChangesButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveChangesButtonClick(object sender, EventArgs e)
		{
			var sb = new StringBuilder("Changes to be saved<br/><br/>");
			var configurationItems = new List<ConfigurationItemDto>();

			var keys = Request.Form.AllKeys.Where(x => x.Contains("$ConfigItem_"));

			foreach (var key in keys)
			{
				var configItemStartPos = key.IndexOf("$ConfigItem_") + 12;
				var typeStartPos = key.IndexOf("^", configItemStartPos) + 1;

				var configItemName = key.Substring(configItemStartPos, typeStartPos - 1 - configItemStartPos);
				var typeName = key.Substring(typeStartPos);
				var configItemValue = Request.Form[key];

				//sb.AppendFormat("Config Item {0} ({1}) = {2}<br/>", configItemName, typeName, configItemValue);

				var setting = _configSettings.SingleOrDefault(x => x.Name == configItemName);

				if (setting.CurrentValue is string)
				{
					// To avoid a possible null reference.
					if (setting.CurrentValue.IsNull())
					{
						if (configItemValue.Equals("NULL"))
						{
							// Do nothing as setting hasnt changed.
						}
					}
					else
						if (!configItemValue.Equals(setting.CurrentValue))
							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValue
							});
				}
				else if (setting.CurrentValue is bool)
				{
					// An unchanged checkbox means true value return, but the hidden field's false value is caught and compared to unchaged true visible checkbox.
					// A check for "Hidden" will remove this behaviour.
					if (typeName.Contains("Hidden"))
					{
						var testKey = keys.FirstOrDefault(x => x.Equals(key.TrimEnd(("_Hidden".ToCharArray()))));
						if (keys.Contains(testKey))
							// Do nothing as we are interested in the ticked checkbox
							Debug.WriteLine(testKey + ": Nothing done with hidden checkbox value.");
						else
						{
							if (bool.Parse(configItemValue) != (bool)setting.CurrentValue)
								configurationItems.Add(new ConfigurationItemDto
								{
									Key = setting.Key,
									Value = configItemValue
								});
						}
						// The currentValue is still true as the setting was enabled. Now it has been unticked we want to take the hidden field's false value.

					}

					else
					{
						// If the configItem is bool and does not contain "Hidden" the checkbox must be ticked, hence true, so compare against currentValue.
						// Issues returning incorrect value, but as we know the control will only appear in the keys list if it is checked we can use the value "true".
						if (bool.Parse(configItemValue) != (bool)setting.CurrentValue)
							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = "true"
							});
					}
				}
				else if (setting.CurrentValue is int)
				{
					if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						configurationItems.Add(new ConfigurationItemDto
						{
							Key = setting.Key,
							Value = configItemValue
						});
				}
				else if (setting.CurrentValue is long)
				{
					if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						configurationItems.Add(new ConfigurationItemDto
						{
							Key = setting.Key,
							Value = configItemValue
						});
				}
				else if (setting.CurrentValue is string[])
				{
					if (!configItemValue.Equals(String.Join(",", setting.CurrentValue as string[])))
						configurationItems.Add(new ConfigurationItemDto
						{
							Key = setting.Key,
							Value = configItemValue
						});
				}
				else if (setting.CurrentValue is int[])
				{
					if (!configItemValue.Equals(String.Join(",", setting.CurrentValue as int[])))
						configurationItems.Add(new ConfigurationItemDto
						{
							Key = setting.Key,
							Value = configItemValue
						});
				}
				else if (setting.CurrentValue is Enum)
				{
					if (setting.CurrentValue is FocusModules)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							var configItemValueAsInt = (int)Enum.Parse(typeof(FocusModules), configItemValue, true);

							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValueAsInt.ToString(CultureInfo.InvariantCulture)
							});
						}
					}

					else if (setting.CurrentValue is SearchCentrePointOptions)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							var configItemValueAsInt = (int)Enum.Parse(typeof(SearchCentrePointOptions), configItemValue, true);

							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValueAsInt.ToString(CultureInfo.InvariantCulture)
							});
						}
					}

					else if (setting.CurrentValue is ResumeSearchableOptions)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							var configItemValueAsInt = (int)Enum.Parse(typeof(ResumeSearchableOptions), configItemValue, true);

							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValueAsInt.ToString(CultureInfo.InvariantCulture)
							});
						}
					}

					else if (setting.CurrentValue is DegreeFilteringType)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							var configItemValueAsInt = (int)Enum.Parse(typeof(DegreeFilteringType), configItemValue, true);

							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValueAsInt.ToString(CultureInfo.InvariantCulture)
							});
						}
					}

					else if (setting.CurrentValue is CareerExplorerFeatureEmphasis)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							var configItemValueAsInt = (int)Enum.Parse(typeof(CareerExplorerFeatureEmphasis), configItemValue, true);

							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValueAsInt.ToString(CultureInfo.InvariantCulture)
							});
						}
					}

					else if (setting.CurrentValue is SchoolTypes)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							var configItemValueAsInt = (int)Enum.Parse(typeof(SchoolTypes), configItemValue, true);

							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValueAsInt.ToString(CultureInfo.InvariantCulture)
							});
						}
					}

					else if (setting.CurrentValue is JobAlertsOptions)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValue
							});
						}
					}

					else if (setting.CurrentValue is AlertFrequencies)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValue
							});
						}
					}

					else if (setting.CurrentValue is DistanceUnits)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValue
							});
						}
					}

					else if (setting.CurrentValue is RegistrationRoute)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValue
							});
						}
					}

          else if (setting.CurrentValue is TalentApprovalOptions)
					{
						if (!configItemValue.Equals(setting.CurrentValue.ToString()))
						{
							configurationItems.Add(new ConfigurationItemDto
							{
								Key = setting.Key,
								Value = configItemValue
							});
						}
					}

				}
			}

			if (configurationItems.IsNotNullOrEmpty())
			{
				ServiceClientLocator.CoreClient(App).SaveConfigurationItems(configurationItems);
				App.RefreshSettings();
			}

			//var setting = _configSettings.Where(x => x.Name == configItemName) as AppSettings.ConfigSettingInfo;

			//if ((configItemValue != setting.CurrentValue) && (configItemValue != setting.DefaultValue))
			//    configurationItems.Add(new ConfigurationItemDto
			//                          {
			//                            Key = Constants.ConfigurationItemKeys.AppVersion, 
			//                            Value = configItemValue
			//                          });

			//OutputLiteral.Text = sb.ToString();

			//var bytes = App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedImage);

			//if (bytes.IsNotNullOrEmpty())
			//{
			//  var applicationImage = new ApplicationImageDto { Image = bytes, Type = ApplicationImageTypes.FocusTalentHeader };
			//  ServiceClientLocator.CoreClient(App).SaveApplicationImage(applicationImage);

			//  TalentApplicationLogoUploader.Clear();
			//  TalentApplicationLogoUploader.SetPreviewImage(UrlBuilder.ApplicationImage(ApplicationImageTypes.FocusTalentHeader));
			//}



			//if ( (ApplicationTextBox.TextTrimmed() != App.Settings.Application) && (ApplicationTextBox.TextTrimmed() != Defaults.GeneralSettings.Application) )
			//  configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys. (unused? Focus<module>ApplicationName instead?)

			//if ((ClientTagTextBox.TextTrimmed() != App.Settings.ClientTag) && (ClientTagTextBox.TextTrimmed() != Defaults.GeneralSettings.ClientTag))
			//configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.ClientTag, Value = ApplicationVersionTextBox.TextTrimmed() });



			//if ((CachePrefixTextBox.TextTrimmed() != App.Settings.CachePrefix) && (CachePrefixTextBox.TextTrimmed() != Defaults.GeneralSettings.CachePrefix))
			//  configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.CachePrefix, Value = CachePrefixTextBox.TextTrimmed() });

			//if ((OnErrorEmailTextBox.TextTrimmed() != App.Settings.OnErrorEmail) && (OnErrorEmailTextBox.TextTrimmed() != Defaults.GeneralSettings.OnErrorEmail))
			//configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.OnErrorEmail, Value = OnErrorEmailTextBox.TextTrimmed() });

		}

		/// <summary>
		/// Binds the Focus modules drop down.
		/// </summary>
		private void BindConfigSettingCategoryDropDownList()
		{
			ConfigSettingCategoryDropDownList.Items.Clear();
			ConfigSettingCategoryDropDownList.Items.AddEnum(ConfigSettingCategory.General);
			ConfigSettingCategoryDropDownList.Items.AddEnum(ConfigSettingCategory.Assist);
			ConfigSettingCategoryDropDownList.Items.AddEnum(ConfigSettingCategory.Talent);
			ConfigSettingCategoryDropDownList.Items.AddEnum(ConfigSettingCategory.Reporting);
			ConfigSettingCategoryDropDownList.Items.AddEnum(ConfigSettingCategory.CareerExplorer);
			ConfigSettingCategoryDropDownList.Items.AddEnum(ConfigSettingCategory.SSO);
		}

	}
}