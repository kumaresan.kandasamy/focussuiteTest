﻿<%@ Page Title="" Language="C#" ValidateRequest="false" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Configuration.aspx.cs" Inherits="Focus.Web.WebAdmin.Configuration" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server"></asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<div style="width:100%"><asp:Button ID="SaveChangesTopButton" runat="server" Text="Save changes" class="button1 right" OnClick="SaveChangesButtonClick" /></div>
	<asp:Panel ID="FocusModulesPanel" runat="server">
	<table width="100%">
		<tr id="FocusModulesRow" runat="server">
			<td width="350px"><h1><%= HtmlLocalise("ConfigSettingCategory.Label", "Configuration setting category")%></h1></td>
			<td><asp:DropDownList runat="server" ID="ConfigSettingCategoryDropDownList" AutoPostBack="True"/></td>
    </tr>
		<tr /><tr /><tr />
		<tr>
			<td width="200px"><h2><%=ConfigSettingCategoryDropDownList.SelectedItem%> settings</h2></td>
		</tr>
	</table>
	</asp:Panel>
	
		<asp:Repeater runat="server" ID="SubCategoryRepeater" EnableViewState="False" OnItemDataBound="SubCategoryRepeaterItemDataBound">
			
				<ItemTemplate>
					
					<asp:Panel ID="SubCategoryHeaderPanel" runat="server" CssClass="singleAccordionTitle">
						<table class="accordionTable">
							<tr  class="multipleAccordionTitle">
							<td>
							<asp:Image ID="SubCategoryHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="Sub Category Header Image" />
									&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"> <%#Container.DataItem %></a></span></td>
							</tr>
						</table>
					</asp:Panel>
					
					<asp:Panel ID="SubCategoryContentPanel" runat="server">
						<asp:Repeater runat="server" ID="SettingRepeater" OnItemDataBound="SettingRepeaterItemDataBound">
							<HeaderTemplate><div class="settingsRepeater"></HeaderTemplate>	
							  <ItemTemplate></ItemTemplate>
							<FooterTemplate></div></FooterTemplate>
						</asp:Repeater>
						
					</asp:Panel>
					<act:CollapsiblePanelExtender ID="SubCategoryContentPanelExtender" runat="server" TargetControlID="SubCategoryContentPanel" ExpandControlID="SubCategoryHeaderPanel" 
																  CollapseControlID="SubCategoryHeaderPanel" Collapsed="true" ImageControlID="SubCategoryHeaderImage" 
																  CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																  SuppressPostBack="true" />
				
				</ItemTemplate>
		
		</asp:Repeater>

<br />
<asp:Literal runat="server" ID="OutputLiteral" EnableViewState="False"/>
	
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
</asp:Content>

