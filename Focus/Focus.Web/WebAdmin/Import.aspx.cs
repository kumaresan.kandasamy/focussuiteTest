﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.WebAdmin
{

  // TODO: Restrict further?
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
  public partial class Import : PageBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!App.Settings.EnableFrontEndMigration)
        Response.RedirectToRoute("default");

      if (!Page.IsPostBack)
        Localise();
    }

    
    private void Localise()
    {
      ImportJobSeekerButton.Text = CodeLocalise("ImportJobSeekerButton.Text", "Import");
      ImportEmployerButton.Text = CodeLocalise("ImportEmployerButton.Text", "Import");
    }

    private string GetFileContents(Stream fileContents)
    {
      using (var reader = new StreamReader(fileContents, Encoding.UTF8))
      {
        return reader.ReadToEnd();
      }
    }


    protected void ImportJobSeekerButton_OnClick(object sender, EventArgs e)
    {
      var outcome = ServiceClientLocator.AccountClient(App).ImportJobSeeker(JobSeekerIdTextBox.Text);
      ClearOldResults();
      if (outcome.IsNotNullOrEmpty())
        JobSeekerImportResultsLiteral.Text = CodeLocalise("JobSeekerImportHeaderImage.Error", "Import of jobseeker {0} and resumes failed. RequestId: {1}", JobSeekerIdTextBox.Text, outcome);
      else
        JobSeekerImportResultsLiteral.Text = CodeLocalise("JobSeekerImportHeaderImage.Success", "Import of jobseeker {0} and resumes entities succeeded.", JobSeekerIdTextBox.Text);

    }

    protected void ImportEmployerButton_OnClick(object sender, EventArgs e)
    {
      var outcome = ServiceClientLocator.AccountClient(App).ImportEmployerAndArtifacts(EmployerIdTextBox.Text);
      ClearOldResults();
      if (outcome.IsNotNullOrEmpty())
        EmployerImportResultsLiteral.Text = CodeLocalise("EmployerImportHeaderImage.Error", "Import of employer {0} and dependent entities failed. RequestId: {1}", EmployerIdTextBox.Text, outcome);
      else
        EmployerImportResultsLiteral.Text = CodeLocalise("EmployerImportHeaderImage.Success", "Import of employer {0} and dependent entities succeeded.", EmployerIdTextBox.Text);
    }

    protected void ImportJobOrderButton_OnClick(object sender, EventArgs e)
    {
      ServiceClientLocator.JobClient(App).ImportJobOrder("");
    }

    private void ClearOldResults()
    {
      EmployerImportResultsLiteral.Text = JobSeekerImportResultsLiteral.Text = string.Empty;
    }
  }
}