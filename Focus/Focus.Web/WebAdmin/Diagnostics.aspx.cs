﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Focus.Common;
using Focus.Core.Views;
using Focus.Services.ServiceImplementations;

#endregion

namespace Focus.Web.WebAdmin
{
	public partial class Diagnostics : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ResumeReferralApprovalsMinimumStarsLabel.Text = App.Settings.ResumeReferralApprovalsMinimumStars.ToString();

			var cached = "???";
			try
			{
				cached = ((IEnumerable<ConfigurationItemView>)HttpContext.Current.Cache.Get("Focus.Configuration")).FirstOrDefault(x => x.Key == "ResumeReferralApprovalsMinimumStars").Value;
			}
			catch
			{ }
			
			ResumeReferralApprovalsMinimumStarsCachedLabel.Text = cached;
		}

		/// <summary>
		/// Handles the Click event of the SetContext control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SetContext_Click(object sender, EventArgs e)
		{
			App.SetContextValue("AppSettings", ServiceClientLocator.CoreClient(App).GetConfiguration());
		}

		/// <summary>
		/// Handles the Click event of the RefreshAppSettings control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void RefreshAppSettings_Click(object sender, EventArgs e)
		{
			var service = new CoreService();
			service.RefreshSettings();
		}

		/// <summary>
		/// Handles the Click event of the ChangeThemeButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ChangeThemeButton_Click(object sender, EventArgs e)
		{
			
		}
	}
}