﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Import.aspx.cs" Inherits="Focus.Web.WebAdmin.Import" %>

<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:content>
<asp:content id="Content3" contentplaceholderid="MainContent" runat="server">
    <asp:panel id="JobSeekerImportHeaderPanel" runat="server" cssclass="singleAccordionTitle on">
        <table class="accordionTable">
            <tr>
                <td>
                    <asp:image id="JobSeekerImportHeaderImage" runat="server" AlternateText="JobSeeker Import Header Image" />
                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobSeekerImport.Label", "Import Job Seekers")%></a></span>
                </td>
            </tr>
        </table>
    </asp:panel>
    <asp:panel id="JobSeekerImportPanel" runat="server"><br/>
        <asp:TextBox runat="server" ID="JobSeekerIdTextBox"></asp:TextBox>
            <asp:Button runat="server" ID="ImportJobSeekerButton" OnClick="ImportJobSeekerButton_OnClick" CssClass="button2"/><br/>
            <asp:Literal runat="server" ID="JobSeekerImportResultsLiteral"></asp:Literal>
    </asp:panel>
    <act:CollapsiblePanelExtender ID="JobSeekerImportCollapsiblePanelExtender" runat="server"
        TargetControlID="JobSeekerImportPanel" ExpandControlID="JobSeekerImportHeaderPanel"
        CollapseControlID="JobSeekerImportHeaderPanel" ImageControlID="JobSeekerImportHeaderImage"
        SuppressPostBack="true" />
    <asp:panel id="EmployerImportHeaderPanel" runat="server" cssclass="singleAccordionTitle on">
        <table class="accordionTable">
            <tr>
                <td>
                    <asp:image id="EmployerImportHeaderImage" runat="server" AlternateText="Employer Import Header Image" />
                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("EmployerImport.Label", "#BUSINESS#")%></a></span>
                </td>
            </tr>
        </table>
    </asp:panel>
    <asp:panel id="EmployerImportPanel" runat="server"><br/>
        <asp:TextBox runat="server" ID="EmployerIdTextBox"></asp:TextBox>
        <asp:Button runat="server" ID="ImportEmployerButton" OnClick="ImportEmployerButton_OnClick" ValidationGroup="Import1111Employer" CssClass="button2" OnClientClick="$('#UpdateProgressIndicator').show();"/><br/>
        <asp:Literal runat="server" ID="EmployerImportResultsLiteral"></asp:Literal>
    </asp:panel>
    <act:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
        TargetControlID="EmployerImportPanel" ExpandControlID="EmployerImportHeaderPanel"
        CollapseControlID="EmployerImportHeaderPanel" ImageControlID="EmployerImportHeaderImage"
        SuppressPostBack="true" />
        <asp:panel id="JobOrderImportHeaderPanel" runat="server" cssclass="singleAccordionTitle on" Visible="False">
        <table class="accordionTable">
            <tr>
                <td>
                    <asp:image id="JobOrderImportHeaderImage" runat="server" AlternateText="JobOrder Import Header Image" />
                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobOrderImport.Label", "JobOrder")%></a></span>
                </td>
            </tr>
        </table>
    </asp:panel>
    <asp:panel id="JobOrderImportPanel" runat="server" Visible="False"><br/>
        <asp:FileUpload runat="server" ID="JobOrderXMLUploader" CssClass="fileupload"  />
        <asp:RequiredFieldValidator ID="valJobOrderXMLUploader" runat="server" ControlToValidate="JobOrderXMLUploader"
			CssClass="error" SetFocusOnError="false" Display="Dynamic" ValidationGroup="ImportJobOrder"></asp:RequiredFieldValidator>
        <focus:InsensitiveRegularExpressionValidator ID="InsensitiveRegularExpressionValidator2" runat="server" ControlToValidate="JobOrderXMLUploader"
			CssClass="error" ValidationExpression="(.*)(\.)((xml)|(txt))$"
			SetFocusOnError="True" Display="Dynamic" ValidationGroup="ImportJobOrder"></focus:InsensitiveRegularExpressionValidator> &nbsp; 
            <asp:Button runat="server" ID="ImportJobOrderButton" OnClick="ImportJobOrderButton_OnClick" ValidationGroup="Import1111JobOrder" CssClass="button2"/><br/>
    </asp:panel>
    <act:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
        TargetControlID="JobOrderImportPanel" ExpandControlID="JobOrderImportHeaderPanel"
        CollapseControlID="JobOrderImportHeaderPanel" ImageControlID="JobOrderImportHeaderImage"
        SuppressPostBack="true" />
</asp:content>
