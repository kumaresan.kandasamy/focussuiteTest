﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Core;

#endregion

namespace Focus.Web.WebAdmin
{
	public partial class Localisation : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				Bind();

			LocalisationModal.Show();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			ContextKeyDropDownList.DataSource = (from lk in LocalisationKeys.Keys
																					 select new { lk.ContextKey, lk.ContextKeyName}).Distinct().ToList();
			ContextKeyDropDownList.DataValueField = "ContextKey";
			ContextKeyDropDownList.DataTextField = "ContextKeyName";
			ContextKeyDropDownList.DataBind();

			ContextKeyDropDownList.Items.Insert(0, new ListItem("-- select a context --", ""));
			
			LanguageDropDownList.Items.Clear();
			LanguageDropDownList.Items.AddRange((new List<ListItem>{ new ListItem("Default", "**-**"),
																					 new ListItem("Spanish", "es"),
																					 new ListItem("French", "fr"),
																					 new ListItem("German", "gr"),
																					 new ListItem("Portugese", "po"),
																					 new ListItem("Italian", "it")}).ToArray());

		}
	}
}