﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Sandbox01.aspx.cs" Inherits="Focus.Web.WebAdmin.Sandbox01" %>

<%@ Register src="~/WebReporting/Controls/ReportChart.ascx" tagname="ReportChart" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server"></asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

	<uc:ReportChart ID="Chart" runat="server" />

</asp:Content>