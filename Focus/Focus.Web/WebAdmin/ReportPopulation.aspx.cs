﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Security.Permissions;

using Focus.Common;
using Focus.Core;

#endregion

namespace Focus.Web.WebAdmin
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistSystemAdministrator)]
  public partial class ReportPopulation : PageBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      var type = Page.RouteData.Values["type"].ToString();

      ReportEntityType entityType;
      var validType = Enum.TryParse(type, true, out entityType);

      if (!validType)
      {
        ResultLabel.Text = CodeLocalise("UnknownType.Text", "Unknown Type.");
        return;
      }

      if (Page.RouteData.Values.ContainsKey("idlist"))
      {
        var idListValue = Page.RouteData.Values["idlist"].ToString();

        ResultLabel.Text = string.Compare(idListValue, "reset", StringComparison.CurrentCultureIgnoreCase) == 0 
          ? ServiceClientLocator.ProcessorClient(App).EnqueueReportPopulation(entityType, true) 
          : ServiceClientLocator.ProcessorClient(App).EnqueueReportPopulation(entityType, idListValue.Split(',').Select(long.Parse).ToList());
      }
      else
      {
        ResultLabel.Text = ServiceClientLocator.ProcessorClient(App).EnqueueReportPopulation(entityType, false);
      }
    }
  }
}