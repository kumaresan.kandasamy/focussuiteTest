﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;

using Focus.Common;
using Focus.Core;

#endregion

namespace Focus.Web.WebAdmin
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistSystemAdministrator)]
  public partial class IntegrationImport : PageBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      var type = Page.RouteData.Values["type"].ToString();

      IntegrationImportRecordType recordType;
      var valid = Enum.TryParse(type, true, out recordType);
      if (!valid)
      {
        int intType;
        valid = int.TryParse(type, out intType);

        if (valid)
          recordType = (IntegrationImportRecordType) intType;
      }

      ResultLabel.Text = valid 
        ? ServiceClientLocator.ProcessorClient(App).EnqueueImport(recordType) 
        : CodeLocalise("InvalidRequest.Text", "Bad Request");
    }
  }
}