﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Common.Helpers;

#endregion

namespace Focus.Web.WebAdmin
{
  public partial class Ping : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      PingHelper.DoPing(Response.OutputStream);
    }
  }
}