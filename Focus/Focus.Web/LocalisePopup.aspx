﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LocalisePopup.aspx.cs" Inherits="Focus.Web.LocalisePopup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
  <script src="<%= ResolveUrl("~/Assets/Scripts/jquery-1.9.1.min.js") %>"></script>
  <link rel="stylesheet" href="<%= ResolveUrl("~/Branding/Default/Focus.Assist.css") %>"/>
</head>
<body>
   <p>Please only complete the Localised Value field.  All other fields on screen are not editable.</p>
    <form id="LocaliseForm" runat="server">
			<table id="localiseItem" width="750">
				<tr>
					<td width="70" class="label"><asp:Label ID="lblLocaliseKey" runat="server" Text="Key"/></td>
					<td><asp:TextBox ID="txtKey" runat="server" ClientIDMode="Static" ReadOnly="true" CssClass="" Width="100%"/></td>
				</tr>
				<tr>
				  <td width="70" class="label"><asp:Label ID="lblDefaultValue" runat="server" Text="Default Value"/></td>
					<td><asp:TextBox ID="txtDefaultValue" runat="server" ClientIDMode="Static" ReadOnly="true" CssClass="" Width="100%"/></td>
				</tr>
				<tr>
					<td class="label" width="70" ><asp:Label ID="lblLocalisedValue" runat="server" Text="Localised Value"/></td>
					<td><asp:TextBox ID="txtLocalisedValue" ClientIDMode="Static" runat="server" TextMode="MultiLine" Rows="3" CssClass=""/></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td width="70" >&nbsp;</td>
					<td><input type="button" id="btnOK" value="OK"/>
						 <input type="button" id="btnCancel" value="Cancel" onclick="self.close()"/></td>
					</tr>
			</table>
    </form> 
      <script type="text/javascript">
        $(document).ready(
          $('#btnOK').click(function () {
            UpdateLocalisationItem();
          }));



          function UpdateLocalisationItem() {
          
          var defaultValue = JSON.stringify($('#hidDefaultValue').val());
          var key = $('#txtKey').val();
          $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "<%= UrlBuilder.AjaxService() %>/UpdateLocalisationItem",
            dataType: "json",
            data: '{"key": "' + key + '", "localisedValue": "' + $('#txtLocalisedValue').val() + '", "defaultValue": "' + encodeURIComponent(defaultValue) + '" }',
            async: false
          });

          window.close();
        }

       
      </script>
</body>
</html>
