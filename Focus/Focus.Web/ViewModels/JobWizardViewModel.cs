﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Focus.Common.Code;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;

#endregion

namespace Focus.Web.ViewModels
{
	[Serializable]
	[DataContract]
	public class JobWizardViewModel : ViewModelBase
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobWizardViewModel"/> class.
		/// </summary>
		public JobWizardViewModel()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobWizardViewModel" /> class.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="defaultClosingDays">The default closing days.</param>
		/// <param name="isConfidential">Wheter the job is confidential.</param>
		public JobWizardViewModel(long employerId, long employeeId, int defaultClosingDays, bool isConfidential)
		{
			Job = new JobDto
			{
				EmployerId = employerId,
				EmployeeId = employeeId,
				JobStatus = JobStatuses.Draft,
				EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.BelowJobPosting,
				JobLocationType = JobLocationTypes.MainSite,
				WizardStep = 1,
				WizardPath = 0,
        ClosingOn = DateTime.Today.AddDays(defaultClosingDays).Date,
				IsConfidential = isConfidential
				//CreatedBy = App.User.UserId,
				//UpdatedBy = App.User.UserId,
				//HideSalaryOnPosting = App.Settings.HideSalary,
			};
		}

		public JobDto Job { get; set; }

		[Required(ErrorMessage = "Closing date is required.")]
    [XmlIgnore] 
		public string JobClosingOn
		{
			get
			{
				return Job.ClosingOn.IsNull() ? String.Empty : Job.ClosingOn.GetValueOrDefault().ToShortDateString();
			}
			set { Job.ClosingOn = value.ToDate(); }
		}

		public JobTypes OriginalJobType { get; set; }

		public BusinessUnitAddressDto BusinessUnitAddress { get; set; }

		public JobAddressDto JobAddress { get; set; }

		public List<JobLocationDto> JobLocations { get; set; }

		public List<JobLocationDto> AllJobLocations { get; set; }

		public PersonDto JobContact { get; set; }

		public PersonAddressDto JobContactAddress { get; set; }

		public PhoneNumberDto JobContactPrimaryContactDetails { get; set; }

		public List<JobCertificateDto> JobCertificates { get; set; }

		public List<JobLanguageDto> JobLanguages { get; set; }

		public List<JobLicenceDto> JobLicences { get; set; }

		public List<JobProgramOfStudyDto> JobProgramsOfStudy { get; set; }

		public List<JobSpecialRequirementDto> JobSpecialRequirements { get; set; }

		public List<JobEducationInternshipSkillDto> JobEducationInternshipSkills { get; set; }

		public bool ShowScreeningPreferencesPanel { get; set; }

    public bool? ResetProgramsOfStudy { get; set; }

		public List<JobDrivingLicenceEndorsementDto> JobDrivingLicenceEndorsements { get; set; }

		public bool ResetDescription { get; set; }
	}
}
