﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Common.Code;
using Focus.Core;
using Focus.Core.Criteria.Employee;
using Focus.Core.Views;

#endregion

namespace Focus.Web.ViewModels
{
  public class FindEmployerViewModel : ViewModelBase
  {
    public EmployeeCriteria Criteria { get; set; }
    public int EmployeesCount { get; set; }
    public PagedList<EmployeeSearchResultView> Employees { get; set; }
	}
}