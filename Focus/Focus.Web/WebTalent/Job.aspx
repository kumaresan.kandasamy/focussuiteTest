﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Job.aspx.cs" Inherits="Focus.Web.WebTalent.Job" %>

<%@ Register Src="~/WebTalent/Controls/TabNavigation.ascx" TagName="TabNavigation" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/JobActionModal.ascx" TagName="JobActionModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/JobActivityList.ascx" TagName="JobActivityList" TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<table style="width: 100%;" role="presentation">
		<tr>
			<td style="width: 50%;">
				<h1>
					<asp:Literal ID="JobTitle" runat="server" EnableViewState="false" /></h1>
			</td>
			<td style="width: 50%; text-align: right;">
				<asp:Button ID="MatchJobButton" runat="server" CssClass="button2 right" OnCommand="JobAction_Command" CommandName="MatchJob" OnClientClick="javascript:showProcessing();" />
				<asp:Button ID="PreviewJobButton" runat="server" CssClass="button2 right" OnCommand="JobAction_Command" CommandName="PreviewJob"  />

				<asp:Panel ID="DuplicateJobPanel" runat="server" class="tooltip right DuplicateJob">
					<asp:LinkButton ID="DuplicateJobButton" runat="server" OnCommand="JobAction_Command" CommandName="DuplicateJob" aria-label="Duplicate Job">
						<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("RefreshJobToolTip", "Duplicate")%>"></div>
					</asp:LinkButton>
				</asp:Panel>
				<asp:Panel ID="HoldJobPanel" runat="server" class="tooltip right HoldJob">
					<asp:LinkButton ID="HoldJobButton" runat="server" OnCommand="JobAction_Command" CommandName="HoldJob" aria-label="Hold Job">
						<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("HoldJobToolTip", "Hold")%>"></div>
					</asp:LinkButton>
				</asp:Panel>
				<asp:Panel ID="ReactivateJobPanel" runat="server" class="tooltip right Reactivate">
					<asp:LinkButton ID="ReactivateButton" runat="server" OnCommand="JobAction_Command" CommandName="ReactivateJob" aria-label="Reactivate Job">
						<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ReactivateJobToolTip", "Reactivate")%>"></div>
					</asp:LinkButton>
				</asp:Panel>
				<asp:Panel ID="CloseJobPanel" runat="server" class="tooltip right CloseJob">
					<asp:LinkButton ID="CloseJobButton" runat="server"   OnCommand="JobAction_Command" CommandName="CloseJob" aria-label="Close Job">
						<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("CloseJobToolTip", "Close")%>"></div>
					</asp:LinkButton></asp:Panel><asp:Panel ID="RefreshJobPanel" runat="server" class="tooltip right Refresh">
					<asp:LinkButton ID="RefreshJobButton" runat="server" OnCommand="JobAction_Command" CommandName="RefreshJob" aria-label="Refresh Job">
						<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("RefreshJobToolTip", "Refresh")%>"></div>
					</asp:LinkButton></asp:Panel><asp:Panel ID="EditJobPanel" runat="server" class="tooltip right EditJob">
					<asp:LinkButton ID="EditJobButton" runat="server"  OnCommand="JobAction_Command" CommandName="EditJob" aria-label="Edit Job">
						<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("EditJobToolTip", "Edit")%>"></div>
					</asp:LinkButton></asp:Panel><asp:Panel ID="DeleteJobPanel" runat="server" class="tooltip right DeleteJob">
					<asp:LinkButton ID="DeleteJobButton" runat="server"  OnCommand="JobAction_Command" CommandName="DeleteJob" aria-label="Delete Job">
						<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("DeleteJobToolTip", "Delete")%>"></div>

                       
					</asp:LinkButton></asp:Panel></td></tr><tr>
			<td colspan="2">
				<asp:Button ID="PostJobButton" runat="server" CssClass="button2 right" OnCommand="JobAction_Command" CommandName="PostJob" Visible="false" />
			</td>
		</tr>
	</table>
	<table style="width: 100%;" role="presentation">
		<colgroup runat="server" id="TalentJobDetailTableCols">
		</colgroup>
		<tr>
			<td style="vertical-align: top">
				<table style="width: 100%;" id="TalentJobDetailTableDevice" role="presentation">
					<colgroup>
						<col style="width:25%" />
						<col style="width:75%" />
					</colgroup>
					<tr>
						<td colspan="2">
							<asp:Literal ID="StatusDetails" runat="server" EnableViewState="false" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<br />
							<strong>
								<asp:Literal runat="server" ID="JobDetailsLabel"></asp:Literal></strong><br />
							<br />
						</td>
					</tr>
					<tr>
						<td>
							<asp:Literal runat="server" ID="JobClosingDateLabel"></asp:Literal>:
							<asp:Literal ID="JobClosingDate" runat="server" EnableViewState="false" />
						</td>
						<td>
							<%= HtmlLocalise("PositionDetails.Label", "Position type")%>:
							<asp:Literal ID="PositionDetails" runat="server" EnableViewState="false" />
						</td>
					</tr>
					<tr>
						<td>
							<asp:Literal runat="server" ID="JobIdLabel"></asp:Literal>:
							<asp:Literal ID="JobReference" runat="server" EnableViewState="false" />
						</td>
						<td>
							<asp:Literal runat="server" ID="NumberOfOpeningsLabel"></asp:Literal>
							<asp:Literal ID="NumberOfOpenings" runat="server" EnableViewState="false" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:Literal runat="server" ID="ForeignLabourCertificationLabel" /><asp:Literal runat="server" ID="SemiColonLiteral" Text=":" />
							<asp:Literal ID="ForeignLabourCertification" runat="server" EnableViewState="false" />
						</td>
					</tr>
					<tr id="VeteranPriorityRow" runat="server">
						<td colspan="2">
							<br />
							<asp:Literal ID="VeteranPriorityLiteral" runat="server" EnableViewState="false" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>
								<asp:Literal runat="server" ID="JobLocationsLabel"></asp:Literal>:</strong>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:Literal ID="Locations" runat="server" EnableViewState="false" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<strong>
								<asp:Literal runat="server" ID="JobDescriptionLabel"></asp:Literal>:</strong>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:Literal ID="JobDescription" runat="server" EnableViewState="false" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table role="presentation">
								<tr id="JobRequirementsHeaderRow" runat="server">
									<td>
										<b>
											<asp:Literal ID="JobRequirementsLabel" runat="server" EnableViewState="false" />
										</b>
									</td>
								</tr>
								<tr id="JobRequirementsRow" runat="server">
									<td>
										<div>
											<p>
												<asp:Literal ID="JobRequirements" runat="server" /></p>
										</div>
									</td>
								</tr>
								<tr id="JobDetailsHeaderRow" runat="server">
									<td>
										<b>
											<%= HtmlLocalise("JobDetails.Label", "Job details")%></b>
									</td>
								</tr>
								<tr id="JobDetailsRow" runat="server">
									<td>
										<div>
											<p>
												<asp:Literal ID="JobDetails" runat="server" /></p>
										</div>
									</td>
								</tr>
								<tr id="JobSalaryBenefitsHeaderRow" runat="server">
									<td>
										<b>
											<asp:Literal ID="JobSalaryBenefitsLabel" runat="server" />
										</b>
									</td>
								</tr>
								<tr id="JobSalaryBenefitsRow" runat="server">
									<td>
										<div>
											<p>
												<asp:Literal ID="JobSalaryBenefits" runat="server" />
											</p>
										</div>
									</td>
								</tr>
								<tr id="JobRecruitmentInformationHeaderRow" runat="server">
									<td>
										<b>
												<asp:Literal ID="JobRecruitmentInformationLabel" runat="server" />
										</b>
									</td>
								</tr>
								<tr id="JobRecruitmentInformationRow" runat="server">
									<td>
										<div>
											<p>
												<asp:Literal ID="JobRecruitmentInformation" runat="server" /></p>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
			<td rowspan="10" style="display: table-cell; vertical-align: top" id="DifferencePane" runat="server">
				<div class="information-panel">
					<h3>Job Posting Status</h3>
					We have amended the following words / phrases in this posting:<br /><br />
					<asp:Literal ID="Differences" runat="server"></asp:Literal><br /><br />
					To confirm these changes, please edit your job and re-post it.<br /><br />
					<asp:Button ID="EditRepostJobButton" runat="server" Text="Edit job" OnCommand="JobAction_Command" CommandName="EditJob" CssClass="button1"/>
				</div>
			</td>
		</tr>
	</table>
	<asp:PlaceHolder ID="JobActivityLogPlaceHolder" runat="server">
		<br />
		<p>
			<b>
				<asp:Label runat="server" ID="JobActivityLogHeaderLabel"></asp:Label></b>
		</p>
		<div style="width: 100%">
			<uc:JobActivityList ID="ActivityList" runat="server" />
		</div>
	</asp:PlaceHolder>
	<%-- Modals --%>
	<uc:JobActionModal ID="JobAction" runat="server" OnCompleted="JobAction_Completed" />
	<uc:ConfirmationModal ID="Confirmation" runat="server" Height="175px" Width="300px" OnOkCommand="Confirmation_OkCommand" />
</asp:Content>
