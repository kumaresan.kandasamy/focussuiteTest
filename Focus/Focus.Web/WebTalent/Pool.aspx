﻿<%@ Page Title="Talent Pool" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Pool.aspx.cs" Inherits="Focus.Web.WebTalent.Pool" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebTalent/Controls/PoolSearch.ascx" tagname="PoolSearch" tagprefix="uc" %>
<%@ Register src="~/WebTalent/Controls/PoolResult.ascx" tagname="PoolResult" tagprefix="uc" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%# ResolveUrl("~/Assets/Scripts/jquery-ui-1.9.2.custom.new.min.js") %>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<h1 style="display:none;">
	<asp:Label ID="SearchTitleLabel" Text="" runat="server"></asp:Label>SearchTitle</h1> 
	<p><%= HtmlLocalise("Notification.Text", "Find and manage talent with the search features and tabbed areas below.")%></p>

	<uc:PoolSearch ID="Search" runat="server" />
	<uc:PoolResult ID="Result" runat="server"/>
</asp:Content>

