﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;

#endregion

namespace Focus.Web.WebTalent.Code
{
  public class PoolSearchUserControl : UserControlBase
  {
    #region Properties

    /// <summary>
    /// Gets or sets the saved search id.
    /// </summary>
    /// <value>The saved search id.</value>
    public long SavedSearchId
    {
      get { return GetViewStateValue<long>("PoolSearch:SavedSearchId"); }
      set { SetViewStateValue("PoolSearch:SavedSearchId", value); }
    }

    /// <summary>
    /// Gets or sets the search session.
    /// </summary>
    /// <value>The search session.</value>
    public string SearchSession { get; set; }

    /// <summary>
    /// Gets or sets the _search criteria.
    /// </summary>
    /// <value>The _search criteria.</value>
    public CandidateSearchCriteria SearchCriteria
    {
      get { return OldApp_RefactorIfFound.GetSessionValue<CandidateSearchCriteria>(Constants.StateKeys.CandidateSearchCriteria + SearchSession); }
      set { OldApp_RefactorIfFound.SetSessionValue(Constants.StateKeys.CandidateSearchCriteria + SearchSession, value); }
    }
    #endregion

    #region Events

    public event PoolSearchRequestHandler PoolSearchRequest;

    /// <summary>
    /// Raises the <see cref="E:PoolSearchRequest"/> event.
    /// </summary>
    /// <param name="e">The <see cref="Focus.Web.WebTalent.Controls.PoolSearch.PoolSearchRequestEventArgs"/> instance containing the event data.</param>
    protected virtual void OnPoolSearchRequest(PoolSearchRequestEventArgs e)
    {
      if (PoolSearchRequest != null)
        PoolSearchRequest(this, e);
    }

    public event CompletedHandler Completed;

    /// <summary>
    /// Raises the <see cref="E:Completed"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected virtual void OnCompleted(EventArgs eventArgs)
    {
      if (Completed != null)
        Completed(this, eventArgs);
    }

    #endregion

    #region Delegates

    public delegate void PoolSearchRequestHandler(object o, PoolSearchRequestEventArgs e);
    public delegate void CompletedHandler(object sender, EventArgs eventArgs);

    #endregion

    #region EventArgs
  
    public class PoolSearchRequestEventArgs : EventArgs
    {
      public readonly CandidateSearchCriteria SearchCriteria;

			/// <summary>
			/// Initializes a new instance of the <see cref="PoolSearchRequestEventArgs"/> class.
			/// </summary>
			/// <param name="searchCriteria">The search criteria.</param>
      public PoolSearchRequestEventArgs(CandidateSearchCriteria searchCriteria)
      {
        SearchCriteria = searchCriteria;
      }
    }

    #endregion
  }
}