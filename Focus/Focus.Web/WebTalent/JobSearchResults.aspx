﻿<%@ Page Title="Job search results" Language="C#" MasterPageFile="~/Site.Master"
	AutoEventWireup="True" CodeBehind="JobSearchResults.aspx.cs" Inherits="Focus.Web.WebTalent.JobSearchResults" %>
<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="SearchResults" Src="~/Code/Controls/User/JobPostingSearchResults.ascx" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript">
  	$(document).ready(function () {
  		Sys.WebForms.PageRequestManager.getInstance().add_endRequest(BindTooltips);
  	});
  </script>
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
      <div><uc:SearchResults ID="Results" runat="server" /></div>
</asp:Content>