﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Focus.Web.WebTalent.Register" EnableEventValidation="False"%>

<%@ Register src="~/Code/Controls/User/EmployerRegistration.ascx" tagname="EmployerRegistration" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content runat="server" ID="HeaderContent" ContentPlaceHolderID="HeaderContent">
	<ul class="navTab"></ul>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

	<uc:EmployerRegistration ID="Registration" runat="server" OnRegistrationCompleted="Registration_OnRegistrationCompleted" />

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="Confirmation" runat="server" Height="250px" Width="400px" />	

</asp:Content>
