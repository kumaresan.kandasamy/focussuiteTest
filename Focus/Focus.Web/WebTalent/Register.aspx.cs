﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Focus.Core.Models;
using Focus.Web.Code;
using Focus.Common;
using Focus.Web.Code.Controls.User;
using Framework.Core;

#endregion

namespace Focus.Web.WebTalent
{
	public partial class Register : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// If authentication then redirect
			if (App.User.IsAuthenticated)
			  Response.Redirect(UrlBuilder.Default(App.User, App));

			// Meta data: helpful for SEO (search engine optimization)
			Page.Title = (App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent) ? CodeLocalise("Page.Title", "Complete registration") : CodeLocalise("Page.Title", "Register for an account");
		}
		
		/// <summary>
		/// Handles the OnRegistrationCompleted event of the Registration control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.EmployerRegistration.RegistrationCompletedEventArgs"/> instance containing the event data.</param>
		protected void Registration_OnRegistrationCompleted(object sender, EmployerRegistration.RegistrationCompletedEventArgs e)
		{
			var validationUrl = App.Settings.AssistApplicationPath + UrlBuilder.AccountValidate(false);

			if (App.Settings.NewEmployerApproval == TalentApprovalOptions.ApprovalSystemNotAvailable || App.Settings.NewHiringManagerApproval == TalentApprovalOptions.ApprovalSystemNotAvailable || App.Settings.NewBusinessUnitApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
			{
				Confirmation.Show(
					CodeLocalise("SuccessfulSubmitInvalidFEINNoAccess.Title", "Thank you for registering with #FOCUSTALENT#"),
					CodeLocalise("SuccessfulSubmitInvalidFEINNoAccess.Body",
											 "Our staff will review your account request and get back to you shortly. Once approved, you will be able to search for qualified talent or create job listings to post. If you need immediate assistance, email us at #SUPPORTEMAIL# or call #SUPPORTPHONE# for registration assistance. Our business hours are between #SUPPORTHOURSOFBUSINESS#"),
					CodeLocalise("Global.OK.Text", "OK"),
					closeLink: UrlBuilder.Default(App.User, App));
			}
      else if (App.Settings.RegistrationCompleteAction == RegistrationCompleteAction.RestrictedAccess)
      {
				// Log the user in so that when navigating to the dashboard
        // they are logged in and don't get bounced
				LogIn(e.AccountUsername, e.AccountPassword, FocusModules.Talent, validationUrl);

        if (e.IsValidFederalEmployerIdentificationNumber && e.IsAlreadyApproved && e.EmployeeIsApproved)
        {
          Confirmation.Show(
            CodeLocalise("SuccessfulSubmitValidFEIN.Title", "Thank you for registering with #FOCUSTALENT#"),
            CodeLocalise("SuccessfulSubmitValidFEIN.Body",
												 "We've sent email to your address with a hyperlink to confirm your email address and complete your site registration. The email will also include information about the account validation process, confirm your username and password, and provide contact numbers and emails for questions or assistance. Email us at #SUPPORTEMAIL# or call #SUPPORTPHONE# for assistance with registration completion. Our business hours are between #SUPPORTHOURSOFBUSINESS#"),
            CodeLocalise("Global.ViewDashboard.Text", "View Dashboard"),
						closeLink: UrlBuilder.Default(App.User, App));
        }
        else
        {
          Confirmation.Show(
            CodeLocalise("SuccessfulSubmitInvalidFEIN.Title", "Thank you for requesting access to #FOCUSTALENT#"),
            CodeLocalise("SuccessfulSubmitInvalidFEIN.Body",
                                                    "Our staff will review your account request and get back to you shortly. In the meantime, you may create job postings to list once your account is approved. Bear in mind that any job post you create through our system will be queued up while your account is under review. Once approved, our system or staff will review your job postings.<br/> If you need immediate assistance, email us at #SUPPORTEMAIL# or call #SUPPORTPHONE# for registration assistance. Our business hours are between #SUPPORTHOURSOFBUSINESS#"),
            CodeLocalise("Global.ViewDashboard.Text", "View Dashboard"),
						closeLink: UrlBuilder.Default(App.User, App));
        }
      }
      else
      {
				if (e.IsValidFederalEmployerIdentificationNumber && e.IsAlreadyApproved && e.EmployeeIsApproved)
        {
          // only send out validation email if registering with existing FEIN and already approved by Assist user
          var url = App.Settings.TalentApplicationPath + UrlBuilder.TalentValidate(ServiceClientLocator.AuthenticationClient(App).CreateEmployeeSingleSignOn(e.EmployeeId, e.UserId), false);
          ServiceClientLocator.AccountClient(App).SendActivationEmail(e.EmployeeFirstName, e.EmployeeEmailAddress, url);
				
					Confirmation.Show(
						CodeLocalise("SuccessfulSubmitValidFEIN.Title", "Thank you for registering with #FOCUSTALENT#"),
            CodeLocalise("SuccessfulSubmitValidFEIN.Body",
                          "We've sent an email to your inbox with a hyperlink to confirm your email address and complete your account registration. The email will also include information about the account validation process and provide contact information for questions or assistance. When you check your email, click on the link to authenticate your account. If you do not receive this email within 24 hours, please check your computer or network's filters, which could block your receipt or place mail in a spam folder.<br />Email us at #SUPPORTEMAIL# for assistance with registration completion. Our business hours are between #SUPPORTHOURSOFBUSINESS#"),
            CodeLocalise("Global.Close.ToolTip", "Return to login"), closeLink: UrlBuilder.LogIn());
        }
        else
        {
	        // no email to send since assist user needs to approve
	        var user = LogIn(e.AccountUsername, e.AccountPassword, FocusModules.Talent, validationUrl);

	        Confirmation.Show(
            CodeLocalise("SuccessfulSubmitInvalidFEIN.Title", "Thank you for registering with #FOCUSTALENT#"),
            CodeLocalise("SuccessfulSubmitInvalidFEIN.Body",
                                                    "Our staff will review your account request and get back to you shortly. In the meantime, you may create job postings to list once your account is approved. Bear in mind that any job post you create through our system will be queued up while your account is under review. Once approved, our system or staff will review your job postings.<br/> If you need immediate assistance, email us at #SUPPORTEMAIL# or call #SUPPORTPHONE# for registration assistance. Our business hours are between #SUPPORTHOURSOFBUSINESS#"),
            CodeLocalise("Global.ViewDashboard.Text", "View Dashboard"),
						closeLink: UrlBuilder.Default(user.IsNull() || user.UserId.IsNull() || user.UserId == 0 ? App.User : user, App, App.User.LastLoggedInOn == null));
        }
      }
		}
  }
}