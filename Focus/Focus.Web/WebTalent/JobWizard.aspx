﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JobWizard.aspx.cs" Inherits="Focus.Web.WebTalent.JobWizard" %>

<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobWizard.ascx" tagname="JobWizard" tagprefix="uc" %>
<%@ Register src="~/WebTalent/Controls/JobsNavigation.ascx" tagname="JobsNavigation" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%# ResolveUrl("~/Assets/Scripts/jquery-ui-1.9.2.custom.new.min.js") %>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<%# ResolveUrl("~/Assets/Css/jquery-ui-1.9.2.custom.css") %>" />
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:JobsNavigation ID="PageNavigation" runat="server" />
	<uc:JobWizard ID="Wizard" runat="server" />
</asp:Content>
