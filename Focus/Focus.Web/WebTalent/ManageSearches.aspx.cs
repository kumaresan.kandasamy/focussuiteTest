﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;

using Focus.Core;

#endregion

namespace Focus.Web.WebTalent
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)] 
	public partial class ManageSearches : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void Page_Load(object sender, EventArgs e)
		{
			
		}
	}
}
