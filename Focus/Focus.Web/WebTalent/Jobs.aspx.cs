﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models;
using Focus.Web.Code;
using Focus.Web.Controllers.WebTalent;
using Focus.Web.WebTalent.Controls;

using Framework.Core;

#endregion

namespace Focus.Web.WebTalent
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)]
  public partial class Jobs : PageBase
  {
    private JobStatuses? _jobStatus;
    private string _problemWithDatesDetails, _activeDetails, _onHoldDetails, _closedDetails, _draftDetails;
    private int _jobsCount;
    private long? _businessUnitId;

    private string _viewApplicantsButtonText, _viewJobButtonText, _viewInternshipButtonText;
	  private bool? _employerAlreadyApproved = null;

    #region Properties

    /// <summary>
    /// Gets the page controller.
    /// </summary>
    /// <value>
    /// The page controller.
    /// </value>
    protected JobsController PageController { get { return PageControllerBaseProperty as JobsController; } }

    /// <summary>
    /// Registers the page controller.
    /// </summary>
    /// <returns></returns>
    protected override IPageController RegisterPageController()
    {
      return new JobsController(App);
    }

    /// <summary>
    /// Gets the no jobs help text.
    /// </summary>
    /// <value>The no jobs help text.</value>
    protected string NoJobsHelpText
    {
      get
      {
        const string defaultText = @"You currently have no {0} jobs. As soon as your first job has a status of {0} it will be listed here.";

        var statusText = CodeLocalise("JobStatus.Unknown.Text", "unknown");
        switch (_jobStatus)
        {
          case JobStatuses.Active: statusText = "active"; break;
          case JobStatuses.OnHold: statusText = "on-hold"; break;
          case JobStatuses.Closed: statusText = "closed"; break;
          case JobStatuses.Draft: statusText = "draft"; break;
        }

        return CodeLocalise("NoJobs.Help", defaultText, statusText);
      }
    }

    #endregion

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (JobStatus == JobStatuses.Active) { 
            if(HttpContext.Current.Request.Url.AbsoluteUri.Contains("active")) Page.Title = CodeLocalise("Page.Title", "Jobs-Active");
        }
        if (JobStatus == JobStatuses.OnHold) Page.Title = CodeLocalise("Page.Title", "Jobs-OnHold");
        if (JobStatus == JobStatuses.Closed) Page.Title = CodeLocalise("Page.Title", "Jobs-Closed");
        if (JobStatus == JobStatuses.Draft) Page.Title = CodeLocalise("Page.Title", "Jobs-Draft");

      if (App.User.EmployeeId == null)
        throw new Exception(FormatError(ErrorTypes.InvalidEmployer, "Invalid employee id"));

      // Show the correct tab as being selected
      if (JobStatus == JobStatuses.Active) ActiveTab.Attributes.Add("class", "active");
      if (JobStatus == JobStatuses.OnHold) OnHoldTab.Attributes.Add("class", "active");
      if (JobStatus == JobStatuses.Closed) ClosedTab.Attributes.Add("class", "active");
      if (JobStatus == JobStatuses.Draft) DraftTab.Attributes.Add("class", "active");

      _problemWithDatesDetails = CodeLocalise("JobRow.ProblemWithDates.Label", "Problem displaying the dates!");
      _activeDetails = CodeLocalise("JobRow.Active.Label", "Posted {0:MMM dd, yyyy}, expires {1:MMM dd, yyyy}");
      _onHoldDetails = CodeLocalise("JobRow.OnHold.Label", "Posted {0:MMM dd, yyyy}, on hold since {1:MMM dd, yyyy}");
      _closedDetails = CodeLocalise("JobRow.Closed.Label", "Posted {0:MMM dd, yyyy}, closed on {1:MMM dd, yyyy}");
      _draftDetails = CodeLocalise("JobRow.Expires.Label", "Draft last saved on {0:MMM dd, yyyy}");

      _viewApplicantsButtonText = CodeLocalise("ViewApplicantsButtonText", "View all applicants ({0})");
      _viewJobButtonText = CodeLocalise("ViewJobButtonText", "View Job");
      _viewInternshipButtonText = CodeLocalise("ViewInternshipButtonText", "View Internship");

      SortColumn = "UpdatedOn";
      SortDirection = "DESC";

      Localise();

      if (!Page.IsPostBack)
      {
        BuildBusinessUnitDropdown();

        UpdateHyperlinks();

				var showQuestions = App.Settings.EnableTalentPasswordSecurity && !ServiceClientLocator.AccountClient(App).HasUserSecurityQuestions(App.User.UserId);
				if (!WelcomeToTalent.Show(closeCommand: (showQuestions ? "QUESTIONS" : null)) && showQuestions)
				{
					ShowSecurityQuestionsModal();
				}

				var refusedAccessToSearchTalentPool = HttpUtility.ParseQueryString(Request.Url.Query).Get(UrlBuilder.refusedAccessToTalentPoolSearchQueryStringKey);
				bool result;
				if (refusedAccessToSearchTalentPool.IsNotNull() && bool.TryParse(refusedAccessToSearchTalentPool, out result) && result)
				{
					Confirmation.Show
						(
						CodeLocalise("NoTalentPoolAccessConfirmation.Title", "Access to Search Resumes prohibited"),
						CodeLocalise("NoTalentPoolAccessConfirmation.Details", "Please note you are unable to Search Resumes until your account has been approved."),
						CodeLocalise("Global.Close.Text", "Close")
						);
				}
      }
    }

    /// <summary>
    /// Localises the text on the page
    /// </summary>
    private void Localise()
    {
      ActiveTabHyperLink.Text = HtmlLocalise("ActiveTab.Label.Tab", "Active");
      OnHoldTabHyperLink.Text = HtmlLocalise("OnHoldTab.Label.Tab", "On Hold");
      ClosedTabHyperLink.Text = HtmlLocalise("ClosedTab.Label.Tab", "Closed");
      DraftTabHyperLink.Text = HtmlLocalise("DraftTab.Label.Tab", "Draft");

      ActiveToolTip.Attributes.Add("title", HtmlLocalise("ActiveToolTip.Title", "Active jobs are currently available to #CANDIDATETYPES#:LOWER"));
      OnHoldToolTip.Attributes.Add("title", HtmlLocalise("OnHoldToolTip.Title", "On hold jobs are temporarily unavailable to #CANDIDATETYPES#:LOWER until posted again"));
      ClosedToolTip.Attributes.Add("title", HtmlLocalise("ClosedToolTip.Title", "Closed jobs have expired or been closed permanently. Expired jobs may be reactivated for {0} days", App.Settings.DaysForJobPostingLifetime.ToString(CultureInfo.CurrentUICulture)));
      DraftToolTip.Attributes.Add("title", HtmlLocalise("DraftToolTip.Title", "Draft jobs are incomplete, may require editing or acceptance, or may be pending staff approval"));
    }

    /// <summary>
    /// 
    /// </summary>
    private void UpdateHyperlinks()
    {
      if (BusinessUnitId == 0)
      {
        ActiveTabHyperLink.NavigateUrl = UrlBuilder.TalentJobs(JobStatuses.Active);
        OnHoldTabHyperLink.NavigateUrl = UrlBuilder.TalentJobs(JobStatuses.OnHold);
        ClosedTabHyperLink.NavigateUrl = UrlBuilder.TalentJobs(JobStatuses.Closed);
        DraftTabHyperLink.NavigateUrl = UrlBuilder.TalentJobs(JobStatuses.Draft);
      }
      else if (BusinessUnitId > 0)
      {
        ActiveTabHyperLink.NavigateUrl = UrlBuilder.TalentJobsForBusinessUnit(JobStatuses.Active, BusinessUnitId);
        OnHoldTabHyperLink.NavigateUrl = UrlBuilder.TalentJobsForBusinessUnit(JobStatuses.OnHold, BusinessUnitId);
        ClosedTabHyperLink.NavigateUrl = UrlBuilder.TalentJobsForBusinessUnit(JobStatuses.Closed, BusinessUnitId);
        DraftTabHyperLink.NavigateUrl = UrlBuilder.TalentJobsForBusinessUnit(JobStatuses.Draft, BusinessUnitId);
      }
      else
      {
        ActiveTabHyperLink.NavigateUrl = UrlBuilder.TalentJobsForAllBusinessUnits(JobStatuses.Active);
        OnHoldTabHyperLink.NavigateUrl = UrlBuilder.TalentJobsForAllBusinessUnits(JobStatuses.OnHold);
        ClosedTabHyperLink.NavigateUrl = UrlBuilder.TalentJobsForAllBusinessUnits(JobStatuses.Closed);
        DraftTabHyperLink.NavigateUrl = UrlBuilder.TalentJobsForAllBusinessUnits(JobStatuses.Draft);
      }
    }

    /// <summary>
    /// Gets the job status.
    /// </summary>
    /// <value>The job status.</value>
    private JobStatuses JobStatus
    {
      get
      {
        if (_jobStatus == null)
        {
          var status = Page.RouteData.Values["status"].ToString();
          JobStatuses jobStatus;
          Enum.TryParse(status, true, out jobStatus);

          _jobStatus = jobStatus;
        }

        return _jobStatus.Value;
      }
    }

    /// <summary>
    /// Gets the business unit id being filtered on
    /// </summary>
    /// <value>The job status.</value>
    private long BusinessUnitId
    {
      get
      {
        if (_businessUnitId == null)
        {
          var businessUnitId = Page.RouteData.Values.ContainsKey("id") ? Page.RouteData.Values["id"].ToString().ToLowerInvariant() : string.Empty;
          switch (businessUnitId)
          {
            case "":
              _businessUnitId = 0;
              break;
            case "all":
              _businessUnitId = -1;
              break;
            default:
              _businessUnitId = businessUnitId.AsLong();
              break;
          }
        }

        return _businessUnitId.Value;
      }
    }

    #region ObjectDataSource Methods

    /// <summary>
    /// Gets the jobs.
    /// </summary>
    /// <param name="businessUnitId">The business unit identifier.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="startRowIndex">Start index of the row.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <returns></returns>
    public List<JobViewModel> GetJobs(long businessUnitId, string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      PagedList<JobViewModel> jobs;

      if (businessUnitId == 0)
        jobs = ServiceClientLocator.JobClient(App).GetJobs(JobStatus, pageIndex, maximumRows, orderBy, JobStatus == JobStatuses.Draft);
      else if (businessUnitId > 0)
        jobs = ServiceClientLocator.JobClient(App).GetJobsForBusinessUnit(JobStatus, businessUnitId, pageIndex, maximumRows, orderBy, JobStatus == JobStatuses.Draft);
      else
        jobs = ServiceClientLocator.JobClient(App).GetJobsForEmployer(JobStatus, App.User.EmployerId.GetValueOrDefault(0), pageIndex, maximumRows, orderBy, JobStatus == JobStatuses.Draft);

      _jobsCount = jobs.TotalCount;

      return jobs;
    }

    /// <summary>
    /// Gets the jobs count.
    /// </summary>
    /// <returns></returns>
    public int GetJobsCount()
    {
      return _jobsCount;
    }


    /// <summary>
    /// Gets the jobs count.
    /// </summary>
    /// <param name="businessUnitId">The business unit identifier.</param>
    /// <returns></returns>
    public int GetJobsCount(long businessUnitId)
    {
      return _jobsCount;
    }

    #endregion

    /// <summary>
    /// Formats the posted and expiry dates.
    /// </summary>
    /// <param name="job">The job summary.</param>
    /// <returns></returns>
    protected string FormatDates(JobViewModel job)
    {
      var formattedDates = _problemWithDatesDetails;

      var postedOn = CodeLocalise("JobRow.PostedOn.Label", "(not posted)");

      var jobId = job.Id.GetValueOrDefault();

      if (job.PostedOn.HasValue)
        postedOn = job.PostedOn.Value.ToString();

      switch (job.JobStatus)
      {
        case JobStatuses.Active:
          if (job.ClosingOn.HasValue)
            formattedDates = string.Format(_activeDetails, postedOn, job.ClosingOn.Value);
          break;

        case JobStatuses.OnHold:
          if (job.HeldOn.HasValue)
            formattedDates = CheckOnHoldMessage(job);
          break;

        case JobStatuses.Closed:
          if (job.ClosedOn.HasValue)
            formattedDates = string.Format(_closedDetails, postedOn, job.ClosedOn.Value);
          break;

        case JobStatuses.Draft:
          formattedDates = CheckForWaitingAndExclusion(job);
          break;

        case JobStatuses.AwaitingEmployerApproval:
          formattedDates = string.Format(_draftDetails, job.UpdatedOn);
          break;
      }
      return formattedDates;
    }

    /// <summary>
    /// Handles the ItemDataBound event of the JobsListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void JobsListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var jobView = (JobViewModel)e.Item.DataItem;

      var jobLocationLabel = (Label)e.Item.FindControl("JobLocationLabel");

      jobLocationLabel.Visible = false;
      if (App.Settings.Theme == FocusThemes.Workforce && App.Settings.SplitJobsByLocation)
      {
        if (JobStatus.IsNotIn(JobStatuses.Draft, JobStatuses.AwaitingEmployerApproval) && jobView.Location.IsNotNullOrEmpty())
        {
          jobLocationLabel.Text = string.Format("({0})", jobView.Location.Replace("(", "").Replace(")", ""));
          jobLocationLabel.Visible = true;
        }
      }

			var jobStatusLabel = (Label)e.Item.FindControl("JobStatusLabel");
	    var jobStatus = GetJobCurrentStatus(jobView);
	    jobStatusLabel.Visible = jobStatus.Length > 0;
			jobStatusLabel.Text = jobStatus;
	    
      var viewApplicantsButton = (Button)e.Item.FindControl("ViewApplicantsButton");
      var viewJobButton = (Button)e.Item.FindControl("ViewJobButton");
      var refreshJobButton = (LinkButton)e.Item.FindControl("RefreshJobButton");
      var closeJobButton = (LinkButton)e.Item.FindControl("CloseJobButton");
      var holdJobButton = (LinkButton)e.Item.FindControl("HoldJobButton");
      var duplicateJobButton = (LinkButton)e.Item.FindControl("DuplicateJobButton");
      var reactivateJobButton = (LinkButton)e.Item.FindControl("ReactivateJobButton");
      var editJobButton = (LinkButton)e.Item.FindControl("EditJobButton");
      var deleteJobButton = (LinkButton)e.Item.FindControl("DeleteJobButton");

	    

      refreshJobButton.Visible = JobStatus == JobStatuses.Active || (JobStatus == JobStatuses.Closed && jobView.AutoClosed && jobView.ClosingOn.HasValue && (DateTime.Now - (DateTime)jobView.ClosingOn).TotalDays <= 30);
      closeJobButton.Visible = (JobStatus == JobStatuses.Active || JobStatus == JobStatuses.OnHold);
      holdJobButton.Visible = JobStatus == JobStatuses.Active;
      duplicateJobButton.Visible = (JobStatus == JobStatuses.Active || JobStatus == JobStatuses.OnHold || JobStatus == JobStatuses.Closed);
      reactivateJobButton.Visible = (jobView.ApprovalStatus.IsNotIn(ApprovalStatuses.Rejected, ApprovalStatuses.Reconsider, ApprovalStatuses.WaitingApproval) && JobStatus == JobStatuses.OnHold && !jobView.RedProfanityWords.IsNotNullOrEmpty());
      editJobButton.Visible = (JobStatus == JobStatuses.Draft || JobStatus == JobStatuses.Active || JobStatus == JobStatuses.OnHold);
      deleteJobButton.Visible = JobStatus == JobStatuses.Draft;

      if (BusinessUnitId != 0 && jobView.EmployeeId != App.User.EmployeeId)
      {
        refreshJobButton.Visible =
          closeJobButton.Visible =
          holdJobButton.Visible =
          reactivateJobButton.Visible =
          editJobButton.Visible =
          deleteJobButton.Visible = false;
      }

      if (refreshJobButton.Visible) GlobalScriptManager.RegisterAsyncPostBackControl(refreshJobButton);
      if (closeJobButton.Visible) GlobalScriptManager.RegisterAsyncPostBackControl(closeJobButton);
      if (holdJobButton.Visible) GlobalScriptManager.RegisterAsyncPostBackControl(holdJobButton);
      if (duplicateJobButton.Visible) GlobalScriptManager.RegisterAsyncPostBackControl(duplicateJobButton);
      if (reactivateJobButton.Visible) GlobalScriptManager.RegisterAsyncPostBackControl(reactivateJobButton);
      // Edit Button is a redirect so we don't add it as an Async Post Back Control
      if (deleteJobButton.Visible) GlobalScriptManager.RegisterAsyncPostBackControl(deleteJobButton);

      var applicationCount = jobView.VeteranPriortyEndDate > DateTime.Now
                               ? jobView.VeteranApplicationCount
                               : jobView.ApplicationCount;

      if ((jobView.EmployeeId == App.User.EmployeeId) && applicationCount > 0)
        viewApplicantsButton.Text = string.Format(_viewApplicantsButtonText, applicationCount);
      else
        viewApplicantsButton.Visible = false;

      viewJobButton.Text = jobView.JobType.IsInternship()
  ? _viewInternshipButtonText
  : _viewJobButtonText;

      if (App.Settings.Theme != FocusThemes.Workforce)
      {
        var businessUnitName = e.Item.FindControl("BusinessUnitNameRow") as Panel;
				if (businessUnitName.IsNotNull())
					businessUnitName.Visible = false;
      }
    }

    /// <summary>
    /// Handles the Command event of the ViewApplicantsButton / ViewJobButton controls.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void ViewButton_Command(object sender, CommandEventArgs e)
    {
      var jobId = GetJobId(e.CommandArgument);
      Response.Redirect(e.CommandName == "ViewApplications" ? UrlBuilder.TalentPool(PoolResult.Tabs.Applicants, jobId)
                                                                                                                  : UrlBuilder.JobView(jobId), true);
    }

    /// <summary>
    /// Handles the Command event of the JobRowAction control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void JobRowAction_Command(object sender, CommandEventArgs e)
    {
      var jobId = GetJobId(e.CommandArgument);
      var jobTitle = Request.Form["JobTitle-" + jobId];
      var jobTypeText = Request.Form["JobType-" + jobId];
      var numberOfOpeningsString = Request.Form["NumberOfOpenings-" + jobId];
      var numberOfOpenings = numberOfOpeningsString.IsNotNullOrEmpty() ? Convert.ToInt32(numberOfOpeningsString) : (int?)null;

      var jobType = JobTypes.None;
      if (jobTypeText.Length > 0)
        Enum.TryParse(jobTypeText, true, out jobType);

      switch (e.CommandName)
      {
        case "RefreshJob":
          // Refresh the posted date
          JobAction.Show(ActionTypes.RefreshJob, jobId, jobTitle, jobType, numberOfOpenings: numberOfOpenings);
          break;

        case "CloseJob":
          // Close - Shows the close job modal
          JobAction.Show(ActionTypes.CloseJob, jobId, jobTitle, jobType);
          break;

        case "HoldJob":
          // Hold – moves job to hold tab and shows system status message
          Confirmation.Show(CodeLocalise("HoldConfirmation.Title", "Hold Posting"),
                                              CodeLocalise("HoldConfirmation.Body", "Are you sure you want to hold {0}?", jobTitle),
                                              CodeLocalise("Global.Cancel.Text", "Cancel"),
                  okText: CodeLocalise("HoldConfirmation.Ok.Text", "Hold posting"), height: 75, okCommandName: "Hold", okCommandArgument: jobId.ToString(CultureInfo.InvariantCulture), y: 50);
          break;

        case "DuplicateJob":
          // Duplicate – creates a new job at the top of the list.
          Handle(PageController.QueryDuplicateJob(jobId, jobTitle));
          break;

        case "ReactivateJob":
          // Reactivate – moves the job to the Active tab and triggers a status message about the reactivation.
          JobAction.Show(ActionTypes.ReactivateJob, jobId, jobTitle, jobType);
          break;

        case "EditJob":
          // Edit the job using the job wizard
          Response.Redirect(UrlBuilder.JobWizard(jobId));
          break;

        case "DeleteJob":
          Confirmation.Show(CodeLocalise("DeleteConfirmation.Title", "Delete Posting"),
                                              CodeLocalise("DeleteConfirmation.Body", "Are you sure you want to delete {0}?", jobTitle),
                                              CodeLocalise("Global.Cancel.Text", "Cancel"),
                                              okText: CodeLocalise("DeleteConfirmation.Ok.Text", "Delete posting"), height: 75, okCommandName: "Delete", okCommandArgument: jobId.ToString(CultureInfo.InvariantCulture), y: 50);
          break;
      }

      // Any command that has the OK button doesn't need to be bound here
      // as it is done in the Ok Command event handler
      JobsListView.DataBind();
    }

    /// <summary>
    /// Handles the Completed event of the JobAction control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void JobAction_Completed(object sender, EventArgs e)
    {
      JobsListView.DataBind();
    }

    /// <summary>
    /// Handles the OkCommand event of the Confirmation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void Confirmation_OkCommand(object sender, CommandEventArgs e)
    {
      var jobId = GetJobId(e.CommandArgument);

      switch (e.CommandName)
      {
        case "Hold":
          // Do the Holding
          ServiceClientLocator.JobClient(App).HoldJob(jobId);
          break;

        case "Delete":
          // Do the Deletion
          ServiceClientLocator.JobClient(App).DeleteJob(jobId);
          break;

        //case "Duplicate":
        //  // Do the Duplication
        //  var duplicatedJobId = ServiceClientLocator.JobClient(App).DuplicateJob(jobId);
        //  Response.Redirect(UrlBuilder.JobWizard(duplicatedJobId));

        //break;
      }

      JobsListView.DataBind();
    }

    /// <summary>
    /// Gets the job id.
    /// </summary>
    /// <param name="commandArgument">The command argument.</param>
    /// <returns></returns>
    private static long GetJobId(object commandArgument)
    {
      long jobId;
      long.TryParse(commandArgument.ToString(), out jobId);

      return jobId;
    }

    /// <summary>
    /// Builds the business unit dropdown
    /// </summary>
    private void BuildBusinessUnitDropdown()
    {
      var employerId = App.User.EmployerId.GetValueOrDefault();
      var employeeId = App.User.EmployeeId.GetValueOrDefault();

      ILookup<long, long> employeeLookup;

      var allBusinessUnits = ServiceClientLocator.EmployerClient(App).GetBusinessUnitsWithEmployeeIds(employerId, out employeeLookup);
      var employeeBusinessUnits = allBusinessUnits.Where(bu => employeeLookup[bu.Id.GetValueOrDefault()].Contains(employeeId)).ToList();
      var otherEmployees = employeeBusinessUnits.Any(bu => employeeLookup[bu.Id.GetValueOrDefault()].Count() > 1);

      BusinessUnitFilter.Items.Clear();
      BusinessUnitFilter.Items.Add(new ListItem(CodeLocalise("Global.Myself.Text", "Myself"), ""));

      if (otherEmployees || allBusinessUnits.Count > 1)
      {
        if (allBusinessUnits.Count > 1)
          BusinessUnitFilter.Items.Add(new ListItem(CodeLocalise("AllBusinessUnites.Text", "All business units"), "All"));

        employeeBusinessUnits.ForEach(bu => BusinessUnitFilter.Items.Add(new ListItem(bu.Name, bu.Id.ToString())));

        var nonEmployeeBusinessUnits = allBusinessUnits.Where(bu => !employeeLookup[bu.Id.GetValueOrDefault()].Contains(employeeId)).ToList();
        nonEmployeeBusinessUnits.ForEach(bu => BusinessUnitFilter.Items.Add(new ListItem(bu.Name, bu.Id.ToString())));
      }
      else
      {
        BusinessUnitFilterLabel.Visible = BusinessUnitFilter.Visible = false;
      }

      switch (BusinessUnitId)
      {
        case 0:
          BusinessUnitFilter.SelectValue("");
          break;
        case -1:
          BusinessUnitFilter.SelectValue("All");
          break;
        default:
          BusinessUnitFilter.SelectValue(BusinessUnitId.ToString(CultureInfo.InvariantCulture));
          break;
      }
    }

    /// <summary>
    /// Fires when a new business unit is selected from the dropdown
    /// </summary>
    /// <param name="sender">Dropdown raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void BusinessUnitFilter_OnSelectedIndexChanged(object sender, EventArgs e)
    {
      var businessUnit = BusinessUnitFilter.SelectedValue;

      if (businessUnit == string.Empty)
        _businessUnitId = 0;
      else if (businessUnit == "All")
        _businessUnitId = -1;
      else
        _businessUnitId = businessUnit.AsLong();

      UpdateHyperlinks();
      JobsListView.DataBind();
    }

    /// <summary>
    /// Fires when the Job List View is selecting data
    /// </summary>
    /// <param name="sender">Job ListView control raising the event</param>
    /// <param name="e">Event Arguments</param>
    protected void JobsDataSource_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["businessUnitId"] = BusinessUnitId;
    }

    /// <summary>
    /// Updates draft job message with details of the approval/denied status
    /// </summary>
    /// <param name="job">The job details</param>
    /// <returns>The status message</returns>
    private string CheckForWaitingAndExclusion(JobViewModel job)
    {
      if (job.ApprovalStatus == ApprovalStatuses.WaitingApproval)
        return CodeLocalise("JobRow.Expires.Label", "Awaiting staff approval since {0:MMM dd, yyyy}", job.UpdatedOn);
      
      if (job.ApprovalStatus == ApprovalStatuses.None && job.AwaitingApprovalOn.IsNotNull())
        return CodeLocalise("JobRow.Expires.Label","Awaiting hiring manager review since {0:MMM dd, yyyy}", job.UpdatedOn);

      if (job.ApprovalStatus == ApprovalStatuses.Reconsider)
        return CodeLocalise("JobRow.Reconsider.Label", "Submitted for reconsideration on {0:MMM dd, yyyy}", job.UpdatedOn);

      if(job.ApprovalStatus == ApprovalStatuses.Rejected)
      {
        if (job.ClosedBy.IsNotNull())
        {
          var closedByUser = ServiceClientLocator.AccountClient(App).GetUserDetails(job.ClosedBy.Value);
          if (closedByUser.IsNotNull())
            return CodeLocalise("JobRow.Denied.Label", "Denied by {0} {1} on {2:MMM dd, yyyy}", closedByUser.PersonDetails.FirstName, closedByUser.PersonDetails.LastName, job.ClosedOn);
        }

        return CodeLocalise("JobRow.Denied.Label", "Denied on {0:MMM dd, yyyy}", job.ClosedOn);
      }

      if (job.RedProfanityWords.IsNotNullOrEmpty() && job.JobStatus == JobStatuses.Draft && job.ApprovalStatus == ApprovalStatuses.None)
          return CodeLocalise("JobRow.Denied.Label", "Pending your edit for inappropriate language");
     

      return string.Format(_draftDetails, job.UpdatedOn);
    }

    /// <summary>
    /// Sets the messages for on-hold jobs
    /// </summary>
    /// <param name="job">The on-hold job</param>
    /// <returns>The status message</returns>
    private string CheckOnHoldMessage(JobViewModel job)
    {
      if (job.ApprovalStatus == ApprovalStatuses.Reconsider)
        return CodeLocalise("JobRow.Reconsider.Label", "Posted {0:MMM dd, yyyy}, submitted for reconsideration on {1:MMM dd, yyyy}", job.PostedOn.GetValueOrDefault(), job.UpdatedOn);
      
      if (job.ApprovalStatus == ApprovalStatuses.Rejected)
      {
        if (job.ClosedBy.IsNotNull())
        {
          var closedByUser = ServiceClientLocator.AccountClient(App).GetUserDetails(job.ClosedBy.Value);
          if (closedByUser.IsNotNull())
            return CodeLocalise("JobRow.Denied.Label", "Posted {0:MMM dd, yyyy}, denied by {1} {2} on {3:MMM dd, yyyy}", job.PostedOn.GetValueOrDefault(), closedByUser.PersonDetails.FirstName, closedByUser.PersonDetails.LastName, job.ClosedOn);
        }

        return CodeLocalise("JobRow.Denied.Label", "Posted {0:MMM dd, yyyy}, denied on {1:MMM dd, yyyy}", job.PostedOn.GetValueOrDefault(), job.ClosedOn);
      }

			if (job.ApprovalStatus == ApprovalStatuses.None && job.AwaitingApprovalOn.IsNotNull())
				return CodeLocalise("JobRow.Expires.Label", "Awaiting hiring manager review since {0:MMM dd, yyyy}", job.UpdatedOn);
            if (job.ApprovalStatus == ApprovalStatuses.OnHold)
                return CodeLocalise("JobRow.OnHold.Label", "Pending your edit for inappropriate language");

      return CodeLocalise("JobRow.OnHold.Label", "Posted {0:MMM dd, yyyy}, on hold since {1:MMM dd, yyyy}", job.PostedOn.GetValueOrDefault(), job.HeldOn.GetValueOrDefault());
    }

    public string GetJobCurrentStatus(JobViewModel jobView)
    {
	    if (_employerAlreadyApproved.IsNull())
		    _employerAlreadyApproved = (ServiceClientLocator.EmployerClient(App).GetEmployerApprovalStatus(jobView.EmployerId) == ApprovalStatuses.Approved);

      //FVN - 5748 - Checked whether the jobstatus is not draft
      if (jobView.ApprovalStatus == ApprovalStatuses.WaitingApproval && jobView.JobStatus != JobStatuses.Draft) 
				return "Pending approval";

			return !_employerAlreadyApproved.GetValueOrDefault(false) ? CodeLocalise("Employer.IsAlreadyApproved", "Release pending #BUSINESS#:LOWER account approval") : string.Empty;
    }

		/// <summary>
		/// Shows the security question modal when the welcome modal is closed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="eventargs"></param>
	  protected void WelcomeToTalent_OnCloseCommand(object sender, CommandEventArgs eventargs)
		{
			ShowSecurityQuestionsModal();
		}

		/// <summary>
		/// Shows the security question modal
		/// </summary>
		private void ShowSecurityQuestionsModal()
		{
			SecurityQuestionsModal.Show(true);
		}
  }
}
