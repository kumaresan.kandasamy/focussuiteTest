﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;

using Focus.Core;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebTalent
{
  public partial class PrintJob : PageBase
	{
    [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)]
    protected void Page_Load(object sender, EventArgs e)
    {
      long jobId = 0;

      if (Page.RouteData.Values.ContainsKey("id"))
        long.TryParse(Page.RouteData.Values["id"].ToString(), out jobId);

      if (jobId == 0)
        throw new Exception(FormatError(ErrorTypes.InvalidJob, "Invalid job id"));

      Utilities.PrintJobPreviewForPage(this, jobId);
    }
  }
}