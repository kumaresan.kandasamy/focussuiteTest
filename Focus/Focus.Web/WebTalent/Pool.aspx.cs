﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Security.Permissions;
using Focus.Common;
using Focus.Web.WebTalent.Code;
using Framework.Core;

using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Web.WebTalent.Controls;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebTalent
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)]
	public partial class Pool : PageBase
	{
		private string SearchSession
		{
			get { return GetViewStateValue<string>("Pool:SearchSession"); }
			set { SetViewStateValue("Pool:SearchSession", value); }
		}

		private string SearchTitle
		{
			get { return App.GetSessionValue<string>(Constants.StateKeys.CandidateSearchTitle + SearchSession); }
			set { App.SetSessionValue(Constants.StateKeys.CandidateSearchTitle + SearchSession, value); }
		}

    /// <summary>
		/// Gets or sets the tab.
		/// </summary>
		/// <value>The tab.</value>
		private CandidateSearchScopeTypes CandidateSearchScope
		{
			get
			{
				var tab = PoolResult.Tabs.AllResumes;

				if (Page.RouteData.Values.ContainsKey("tab"))
					Enum.TryParse(Page.RouteData.Values["tab"].ToString(), true, out tab);

				switch (tab)
				{
					case PoolResult.Tabs.AllResumes:
						return CandidateSearchScopeTypes.Open;
					case PoolResult.Tabs.Applicants:
						return CandidateSearchScopeTypes.JobApplicants;
					case PoolResult.Tabs.FlaggedResumes:
						return CandidateSearchScopeTypes.Flagged;
					case PoolResult.Tabs.Invitees:
						return CandidateSearchScopeTypes.JobInvitees;
				}

				return CandidateSearchScopeTypes.Open;
			}
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            
            if (!Utilities.ShowTalentPoolSearch()) Response.Redirect(UrlBuilder.Default(App.User, App, refusedAccessToTalentPoolSearch: true));
			Search.PoolSearchRequest += Search_PoolSearchRequest;
			Search.Completed += Search_Completed;
            Page.Title = CodeLocalise("Page.Title", "Talent Pool");

			if (SearchSession.IsNullOrEmpty() || SearchSession == "0")
			{
				if (Page.RouteData.Values.ContainsKey("searchsession")) SearchSession = Page.RouteData.Values["searchsession"].ToString();
			}

			long savedSearchId = 0;
			if (Page.RouteData.Values.ContainsKey("searchid"))
				long.TryParse(Page.RouteData.Values["searchid"].ToString(), out savedSearchId);

			CandidateSearchCriteria searchCriteria = null;

			if (!IsPostBack)
			{
				long jobId = 0;
				if (Page.RouteData.Values.ContainsKey("jobid"))
					long.TryParse(Page.RouteData.Values["jobid"].ToString(), out jobId);

				if (SearchSession.IsNullOrEmpty() || SearchSession == "0")
        {
          #region new search
          // New search session so create one
					SearchSession = Guid.NewGuid().ToString();

					if (jobId > 0)
					{
						var job = ServiceClientLocator.JobClient(App).GetJob(jobId);

						// Set the Search Title
						SearchTitle = job.JobTitle;

						searchCriteria = new CandidateSearchCriteria
						                     	{
						                     		SearchType = CandidateSearchTypes.ByJobDescription,
						                     		ScopeType = CandidateSearchScopeTypes.JobApplicants,
						                     		JobDetailsCriteria = new CandidateSearchJobDetailsCriteria {JobId = jobId}
																		

						                     	};

						#region Set Job Types Considered

						if (App.Settings.Theme == FocusThemes.Education)
						{
							JobTypes jobTypesConsidered;

							switch (job.JobType)
							{
								case JobTypes.InternshipPaid:
								case JobTypes.InternshipUnpaid:
									jobTypesConsidered = JobTypes.InternshipPaid | JobTypes.InternshipUnpaid;
									break;
								default:
									jobTypesConsidered = JobTypes.Job;
									break;
							}

							searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria
							                                    	{
							                                    		JobTypesConsidered = jobTypesConsidered,
							                                    		EducationCriteria = new CandidateSearchEducationCriteria()
							                                    		{
							                                    				IncludeAlumni= true
							                                    		}
																											
							                                    	};

							// Set expected completion date if this 
							if (jobTypesConsidered == JobTypes.Job)
							{
								searchCriteria.AdditionalCriteria.EducationCriteria.MonthsUntilExpectedCompletion = 6;
						
							}
		
							// Set enrollment status if this is an internship which requires a student to be enrolled
							if ((job.JobType == JobTypes.InternshipPaid || job.JobType == JobTypes.InternshipUnpaid) && job.StudentEnrolled.GetValueOrDefault())
							{
								searchCriteria.AdditionalCriteria.EducationCriteria.CurrentlyEnrolledUndergraduate = true;
								if (job.MinimumCollegeYears.HasValue && job.MinimumCollegeYears > 0)
									searchCriteria.AdditionalCriteria.EducationCriteria.EnrollmentStatus = SchoolStatus.Sophomore;
							}

							// Get programs of study for current job
							var programsOfStudy = ServiceClientLocator.JobClient(App).GetJobProgramsOfStudy(jobId);

							// If we have some then we need to update the controls
							if (programsOfStudy.IsNotNullOrEmpty())
							{
								searchCriteria.AdditionalCriteria.EducationCriteria.ProgramsOfStudy = new List<KeyValuePairSerializable<string, string>>();

								foreach (var programOfStudy in programsOfStudy)
								{
									searchCriteria.AdditionalCriteria.EducationCriteria.ProgramsOfStudy.Add(new KeyValuePairSerializable<string, string>(programOfStudy.Id.ToString(), programOfStudy.ProgramOfStudy));
								}
							}
						}

						#endregion

						Search.SearchCriteria = searchCriteria;

                       
          
          }
					else
					{
						if (savedSearchId > 0)
						{
							var savedSearchView = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearch(savedSearchId);

							// Set the search title
							SearchTitle = savedSearchView.SavedSearch.Name;
							searchCriteria = savedSearchView.Criteria;

							// Now set up the search
              Search.SavedSearchId = Result.SavedSearchId = savedSearchId;
                          
						}
          }

                    
          #endregion
        }
				else
				{
					// The user has triggered something that has returned them in the scope of a search session
					Search.SearchSession = SearchSession;
					searchCriteria = Search.SearchCriteria;
					if (savedSearchId > 0) Search.SavedSearchId = savedSearchId;
				}
			}

			Result.SearchSession = Search.SearchSession = SearchSession;

			if(searchCriteria.IsNotNull())
			{
				// Apply search scope
				searchCriteria.ScopeType = CandidateSearchScope;
				Search.SearchCriteria = searchCriteria;

				if(!IsPostBack) Result.Search(searchCriteria);

				if (SearchTitle.IsNull()) SearchTitle = GetSearchTitle(searchCriteria);

			}
			else if(CandidateSearchScope == CandidateSearchScopeTypes.Flagged)
			{
				// Could just be viewing Flagged resumes
				searchCriteria = new CandidateSearchCriteria {ScopeType = CandidateSearchScope};
				if (!IsPostBack) Result.Search(searchCriteria);
			}

			if ( SearchTitle.IsNotNullOrEmpty())
			{
				SearchTitleLabel.Text = SearchTitle;
				SearchTitleLabel.Visible = true;
			}
			else
			{
				SearchTitleLabel.Visible = false;
			}
                            
         }

		/// <summary>
		/// Handles the PoolSearchRequest event of the Search control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.WebTalent.Controls.PoolSearch.PoolSearchRequestEventArgs"/> instance containing the event data.</param>
		protected void Search_PoolSearchRequest(object sender, PoolSearchUserControl.PoolSearchRequestEventArgs e)
		{
			Result.Search(e.SearchCriteria);
		}

		/// <summary>
		/// Handles the Completed event of the Search control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Search_Completed(object sender, EventArgs e)
		{
			Result.Bind();
		}

		private string GetSearchTitle(CandidateSearchCriteria criteria)
		{
			if (criteria.IsNull()) return "";

			switch (criteria.SearchType)
			{
				case CandidateSearchTypes.ByJobDescription:
					return criteria.JobDetailsCriteria.IsNotNull() ? criteria.JobDetailsCriteria.JobTitle : "";
				case CandidateSearchTypes.ByKeywords:
					return criteria.KeywordCriteria.IsNotNull() ?  CodeLocalise("KeywordSearch.Title", "Results for keywords : {0}", criteria.KeywordCriteria.Keywords) : "";
				case CandidateSearchTypes.ByResume:
					return criteria.ResumeCriteria.IsNotNull() ? criteria.ResumeCriteria.ResumeFilename : "";
			}

			return "";
		}
	}
}