﻿using System;
using System.Security.Permissions;
using Framework.Core;
using Focus.Core;


namespace Focus.Web.WebTalent
{
		[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)]
		public partial class JobPosting : PageBase
		{
			#region Page Properties

			#endregion

			/// <summary>
			/// Handles the Load event of the Page control.
			/// </summary>
			/// <param name="sender">The source of the event.</param>
			/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
			protected void Page_Load(object sender, EventArgs e)
			{
				
			}
		}
	}
