﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master"  AutoEventWireup="true" CodeBehind="UpdateCompanyInformation.aspx.cs" Inherits="Focus.Web.WebTalent.UpdateCompanyInformation" %>
<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebTalent/Controls/TabNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<p class="pageNav">
		<a href="<%= UrlBuilder.ChangeLogin() %>"><%= HtmlLocalise("Global.ChangeLogin.LinkText", "Change login")%></a>&nbsp;|&nbsp;
		<a href="<%= UrlBuilder.UpdateContactInformation() %>"><%= HtmlLocalise("Global.UpdateContactInformation.LinkText", "Update contact information")%></a>&nbsp;|&nbsp;
		<a href="<%= UrlBuilder.ManageSavedSearches() %>"><%= HtmlLocalise("Global.ManageSavedSearches.LinkText", "Manage saved searches")%></a>
	</p>

	<h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>

	<table width="100%">
		<tr>
			<td>
				<asp:button ID="SaveButton" runat="server" CssClass="button2 right" />
				<p><b><%= HtmlLocalise("UpdateCompanyInformation.Label", "Update my #BUSINESS#:LOWER information")%></b> <%= HtmlRequiredFieldsInstruction() %></p>
			</td>	
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td width="150"><%= HtmlRequiredLabel(NameTextBox, "Name.Label", "Name")%></td>
			<td>
				<asp:TextBox ID="NameTextBox" runat="server" ClientIDMode="Static" TabIndex="1" MaxLength="200" />
				<asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="NameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			</td>
		</tr>
    <tr>
			<td colspan="2">&nbsp;
			</td>
		</tr>
		<tr>
			<td><%= HtmlRequiredLabel(FEINLabel, "FEIN.Label", "FEIN")%></td>
			<td>
				<asp:Literal ID="FEINLabel" runat="server" ClientIDMode="Static"/>
			</td>
		</tr>
     <tr>
			<td colspan="2">&nbsp;
			</td>
		</tr>
		<tr>
			<td><%= HtmlLabel("IndustrialClassification.Label", "Industrial classification")%></td>
			<td><asp:TextBox ID="IndustrialClassificationTextBox" runat="server" ClientIDMode="Static"  Width="95px" TabIndex="2" MaxLength="200"/></td>
		</tr>
		
	</table>
	<table width="100%">
		<tr>
			<td width="148"><%= HtmlRequiredLabel(PhoneTextBox, "ContactPhone.Label", "Phone number")%></td>
			<td>
				<table>
					<tr>
						<td><asp:TextBox ID="PhoneTextBox" runat="server" ClientIDMode="Static" TabIndex="3" MaxLength="20" /></td>
						<td>
							<%= HtmlInFieldLabel("PhoneExtensionTextBox", "PhoneExtension.Label", "extension", 50)%>
							<asp:TextBox ID="PhoneExtensionTextBox" runat="server" ClientIDMode="Static" TabIndex="4" MaxLength="10" Width="55" />
							<act:MaskedEditExtender ID="PhoneExtensionMaskedEdit" runat="server" MaskType="Number" TargetControlID="PhoneExtensionTextBox" PromptCharacter=" " ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" Mask="99999"/>
							<asp:DropDownList ID="PhoneTypeDropDown" runat="server" ClientIDMode="Static" TabIndex="5" />	
						</td>
						<td><asp:RequiredFieldValidator ID="PhoneRequired" runat="server" ControlToValidate="PhoneTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" /></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table width="100%">
		<tr>
			<td width="150"><%= HtmlLabel("AlternatePhone1.Label", "Alternate phone number 1")%></td>
			<td>
				<asp:TextBox ID="AlternatePhone1TextBox" runat="server" ClientIDMode="Static" TabIndex="6" MaxLength="20" />
				<asp:DropDownList ID="AlternatePhone1TypeDropDown" runat="server" ClientIDMode="Static" TabIndex="7" />	
			</td>
		</tr>
		<tr>
			<td><%= HtmlLabel("AlternatePhone2.Label", "Alternate phone number 2")%></td>
			<td>
				<asp:TextBox ID="AlternatePhone2TextBox" runat="server" ClientIDMode="Static" TabIndex="16" MaxLength="20" />
				<asp:DropDownList ID="AlternatePhone2TypeDropDown" runat="server" ClientIDMode="Static" TabIndex="8" />
			</td>
		</tr>
	</table>

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />

</asp:Content>
