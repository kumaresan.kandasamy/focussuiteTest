﻿<%@ Page Title="Manage Company" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageCompany.aspx.cs" Inherits="Focus.Web.WebTalent.ManageCompany" %>

<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebTalent/Controls/TabNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="UploadLogo" Src="~/WebTalent/Controls/UploadLogo.ascx" %>
<%@ Register TagPrefix="uc" TagName="EditBusinessUnit" Src="~/Code/Controls/User/EditBusinessUnit.ascx" %>
<%@ Register TagPrefix="uc" TagName="BusinessUnitSelector" Src="~/Code/Controls/User/BusinessUnitSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="SettingsLinks" Src="Controls/AccountSettingsLinks.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
  <uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <uc:SettingsLinks ID="SettingLinksMain" runat="server" />
	
	<h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>
	<br/>
	<p><b><%= HtmlLocalise("BusinessUnits.Label", "#BUSINESS# names")%></b></p>

	<asp:UpdatePanel ID="BusinessUnitUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<table role="presentation">
				<tr>
					<td class="label" width="200px"><%= HtmlLabel("BusinessUnit.Label", "Select #BUSINESS# name")%></td>
					<td><asp:DropDownList ID="BusinessUnitDropDown" runat="server" ClientIDMode="Static" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="BusinessUnitDropDown_SelectedIndexChanged" Title="Business"/><br /></td>
					<td><asp:Button ID="AssignNewBusinessUnitButton" runat="server" CausesValidation="false" CssClass="button2" ClientIDMode="Static" OnClick="AssignNewBusinessUnitButton_Click" /> </td>
				</tr>
			</table>
			<asp:Label ID="TempLabel" runat="server" />
			<br/>
			<br/>
			<asp:Panel runat="server" ID="BusinessUnitEditPanel" CssClass="hidden" >
				<table width="100%" role="presentation">
					<tr>
						<td>
							<asp:button ID="SaveMainDetailsButton" runat="server" ValidationGroup="BusinessUnit" CssClass="button2 right" OnClick="SaveMainDetailsButton_Click" ClientIDMode="Static" />
							<asp:button ID="MakeDefaultButton" runat="server" CssClass="button3 right" ClientIDMode="Static" CausesValidation="false" OnClick="MakeDefaultButton_Click" />
							<p><b><%= HtmlLocalise("MainDetails.Label", "Main details")%></b></p>
						</td>	
					</tr>
				</table>				
				<uc:EditBusinessUnit runat="server" ID="BusinessUnitEditor" ValidationGroup="BusinessUnit"  />
				<br/><hr/>
				<table width="100%" role="presentation">
					<tr>
						<td>
							<asp:button ID="SaveDescriptionsButton" runat="server" ValidationGroup="Descriptions" CssClass="button2 right" OnClick="SaveDescriptionsButton_Click" ClientIDMode="Static" />
							<p><b><%= HtmlLocalise("CompanyDescriptions.Label", "#BUSINESS# descriptions")%></b></p>
						</td>	
					</tr>
				</table>
				<asp:UpdatePanel ID="DescriptionsUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<asp:button ID="AddDescriptionTopButton" runat="server" CssClass="button1" ClientIDMode="Static" OnClick="AddDescriptionButton_Click" ValidationGroup="Descriptions"/>
						<br/><br/>
						<table role="presentation">
							<asp:Repeater ID="DescriptionsRepeater" runat="server" OnItemDataBound="DescriptionsRepeater_ItemDataBound" OnItemCommand="DescriptionsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td colspan="2">
											<span class="inFieldLabel"><asp:Label ID="DescriptionNameLabel" runat="server" Width="330"><%= HtmlLocalise("DescriptionName.InlineLabel", "enter description name")%></asp:Label></span>
											<asp:TextBox ID="DescriptionNameTextBox" runat="server" Width="330px" MaxLength="200" />
											<asp:RadioButton ID="DefaultDescriptionNameRadioButton" runat="server" GroupName="DefaultDescriptionNameRadioButtonGroup"/>
											<%= HtmlLocalise("DefaultDescriptionName.Label", "Default")%>
											<asp:RequiredFieldValidator ID="DescriptionNameRequired" runat="server" ControlToValidate="DescriptionNameTextBox" ValidationGroup="Descriptions" CssClass="error" />
											<asp:HiddenField ID="DescriptionIdHidden" runat="server" ClientIDMode="Static" />
										</td>
									</tr>
									<tr>
										<td>
											<span class="inFieldLabel"><asp:Label ID="DescriptionLabel" runat="server" Width="800"><%= HtmlLocalise("Description.InlineLabel", "enter description") %></asp:Label></span>
											<asp:TextBox ID="DescriptionTextBox" runat="server" Width="800px" TextMode="MultiLine" Rows="3" MaxLength="2000"/>
										</td>
										<td valign="top">
											<asp:ImageButton ID="RemoveDescriptionButton" runat="server" CommandName="Remove" ImageUrl="<%# UrlBuilder.ButtonDeleteIcon() %>" CausesValidation="False" ImageAlign="Top" alt="Remove description button"/>
											<asp:RequiredFieldValidator ID="DescriptionRequired" runat="server" ControlToValidate="DescriptionTextBox" ValidationGroup="Descriptions" CssClass="error" />
											<asp:CustomValidator runat="server" ID="DescriptionLengthValidator" ClientValidationFunction="ValidateDescriptionLength" CssClass="error" ValidationGroup="Descriptions" ControlToValidate="DescriptionTextBox" ErrorMessage="Error" SetFocusOnError="True"/>
										</td>
									</tr>
									<tr><td colspan="2">&nbsp;</td></tr>
								</ItemTemplate>
							</asp:Repeater>
						</table>
						<asp:button ID="AddDescriptionBottomButton" runat="server" CssClass="button1" ClientIDMode="Static" OnClick="AddDescriptionButton_Click" ValidationGroup="Descriptions" />
						<uc:ConfirmationModal ID="DescriptionsWarningModal" runat="server" />
						<br/><hr/>
						<table width="100%" role="presentation">
							<tr>
								<td>
									<asp:button ID="SaveLogosButton" runat="server" ValidationGroup="Logos" CssClass="button2 right" OnClick="SaveLogosButton_Click" ClientIDMode="Static" />
									<p><b><%= HtmlLocalise("CompanyLogos.Label", "#BUSINESS# logos")%></b></p>
								</td>	
							</tr>
						</table>
	
						<asp:UpdatePanel ID="LogosUpdatePanel" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<asp:button ID="AddLogoTopButton" runat="server" CssClass="button1" ClientIDMode="Static" OnClick="AddLogoButton_Click" />
								<br/><br/>
								<table  role="presentation">
									<asp:Repeater ID="LogoRepeater" runat="server" OnItemDataBound="LogoRepeater_ItemDataBound" OnItemCommand="LogoRepeater_ItemCommand">
										<ItemTemplate>
											<tr>
												<td width="100px"><%= HtmlLabel("LogoName.Label", "Logo name") %></td>
													<td><span class="inFieldLabel"><asp:Label ID="LogoNameLabel" runat="server" Width="330" AssociatedControlID="LogoNameTextBox"><%= HtmlLocalise("LogoName.InlineLabel", "name the logo")%></asp:Label></span>
													<asp:TextBox ID="LogoNameTextBox" runat="server" Width="330px" MaxLength="200" /><td>
													<td style="padding-top: 7px;vertical-align: top;"><asp:ImageButton ID="RemoveLogoButton" runat="server" CommandName="Remove" ImageUrl="<%# UrlBuilder.ButtonDeleteIcon() %>" CausesValidation="False" ImageAlign="Top" alt="Remove logo button"/></td>
												<td>
													<asp:RequiredFieldValidator ID="LogoNameRequired" runat="server" ControlToValidate="LogoNameTextBox" ValidationGroup="Logos" CssClass="error" />
													<asp:HiddenField ID="LogoIdHidden" runat="server" ClientIDMode="Static" />
												</td>
												</tr>
												<tr>
													<td></td>
												<td><asp:Image ID="LogoImage" runat="server" alt="Company logo image"/></td>
											</tr>
										</ItemTemplate>
									</asp:Repeater>
								</table>
								<asp:Panel ID="AddLogoPanel" runat="server" ClientIDMode="Static" >
									<asp:HiddenField ID="ShowLogoUploaderHidden" runat="server" Value="false" ClientIDMode="Static" />
									<table role="presentation">
										<tr>
											<td width="100px" valign="top" style="padding-top: 5px;" ><%= HtmlLabel("LogoName.Label", "Logo name") %><br/><br/></td>
											<td width="330px"><uc:UploadLogo ID="LogoUpload" runat="server" ValidationGroup="Logos"/></td>
											<td style="padding-top: 7px;vertical-align: top;"><asp:ImageButton ID="HideLogoUploadButton" runat="server" CommandName="Remove" ImageUrl="<%# UrlBuilder.ButtonDeleteIcon() %>" CausesValidation="False" ImageAlign="Top" OnClick="HideLogoUploadButton_Click" alt="Hide Logo Upload button"/></td>
										</tr>
									</table>
								</asp:Panel>
								<asp:button ID="AddLogoBottomButton" runat="server" CssClass="button1" ClientIDMode="Static" OnClick="AddLogoButton_Click" />
								<uc:ConfirmationModal ID="LogosWarningModal" runat="server" />
							</ContentTemplate>
						</asp:UpdatePanel>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
			<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
			<uc:BusinessUnitSelector ID="SelectBusinessUnitModal" runat="server" OnBusinessUnitSelected="SelectBusinessUnitModal_BusinessUnitSelected" Visible="False" />
		</ContentTemplate>
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="BusinessUnitDropDown" EventName="SelectedIndexChanged" />
		</Triggers>
	</asp:UpdatePanel>

	<script type="text/javascript">

		$(document).ready(
			function ()
			{
				documentReady();
			}
		);

		var prm = Sys.WebForms.PageRequestManager.getInstance();

		prm.add_endRequest(function ()
		{
			documentReady();

			// Apply jQuery to infield labels
			$(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();
		});

		function documentReady()
		{
			if ($("#ShowLogoUploaderHidden").val() == "false")
				$("#AddLogoPanel").hide();
			else
				$("#AddLogoPanel").show();
		}

		// Microsoft fix to address issue of asp:RadioButton in repeater having different Ids
		function SetUniqueRadioButton(nameregex, current)
		{
			var re = new RegExp(nameregex);
			for (var i = 0; i < document.forms[0].elements.length; i++)
			{
				var elm = document.forms[0].elements[i];
				if (elm.type == 'radio')
				{
					if (re.test(elm.name))
						elm.checked = false;
				}
			}
			current.checked = true;
		}

		function ValidateDescriptionLength(sender, args) {

			var description = document.getElementById(sender.controltovalidate).value;

			args.IsValid = description.length <= 2000;
		}

	</script>
</asp:Content>
