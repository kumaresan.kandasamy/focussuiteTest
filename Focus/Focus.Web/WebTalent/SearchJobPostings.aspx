﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchJobPostings.aspx.cs" Inherits="Focus.Web.WebTalent.SearchJobPostings" %>
<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register Src="~/Code/Controls/User/SearchJobPostings.ascx" TagName="SearchPostings" TagPrefix="uc" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <asp:UpdatePanel ID="UpdatePanelJobSearchCriteria" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<table style="width:100%;" role="presentation">
				<%--SEARCH JOB POSTINGS--%>
				<uc:SearchPostings ID="SearchPostings" runat="server" Visible="false" />
			</table>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

