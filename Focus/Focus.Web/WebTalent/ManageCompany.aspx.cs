﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Framework.Core;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;

#endregion

namespace Focus.Web.WebTalent
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)] 
	public partial class ManageCompany : PageBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets the employer id.
		/// </summary>
		/// <value>The employer id.</value>
		protected long EmployerId
		{
			get { return GetViewStateValue<long>("ManageCompany:EmployerId"); }
			set { SetViewStateValue("ManageCompany:EmployerId", value); }
		}

		/// <summary>
		/// Gets or sets the business unit id.
		/// </summary>
		/// <value>The business unit id.</value>
		protected long BusinessUnitId
		{
			get { return GetViewStateValue<long>("ManageCompany:BusinessUnitId"); }
			set { SetViewStateValue("ManageCompany:BusinessUnitId", value); }
		}

		/// <summary>
		/// Gets or sets the _business units.
		/// </summary>
		/// <value>The _business units.</value>
		private List<EmployeeBusinessUnitView> BusinessUnits
		{
			get { return GetViewStateValue<List<EmployeeBusinessUnitView>>("ManageCompany:BusinessUnits"); }
			set { SetViewStateValue("ManageCompany:BusinessUnits", value); }
		}

		/// <summary>
		/// Gets or sets the _model.
		/// </summary>
		/// <value>The _model.</value>
		private ManageBusinessUnitModel Model
		{
			get { return App.GetSessionValue<ManageBusinessUnitModel>(Constants.StateKeys.ManageBusinessUnitModel); }
			set { App.SetSessionValue(Constants.StateKeys.ManageBusinessUnitModel, value); }
		}

		protected int? LockVersion
		{
			get { return GetViewStateValue<int?>("UpdateCompany:LockVersion"); }
			set { SetViewStateValue("UpdateCompany:LockVersion", value); }
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				// If the user is not approved then don't allow them to change the company details.
        if (ServiceClientLocator.EmployeeClient(App).GetEmployeeByPerson(App.User.PersonId.Value).ApprovalStatus.IsNotIn(ApprovalStatuses.None, ApprovalStatuses.Approved))
					Response.Redirect(UrlBuilder.ManageSavedSearches());

				EmployerId = App.User.EmployerId.GetValueOrDefault();
				Bind();
				LocaliseUI();
				ApplyBranding();

				var hiringManagerApprovalStatus = ServiceClientLocator.EmployeeClient(App).GetEmployeeByPerson(App.User.PersonId.Value).ApprovalStatus;
			  AssignNewBusinessUnitButton.Enabled = hiringManagerApprovalStatus.IsIn(ApprovalStatuses.None, ApprovalStatuses.Approved);
              BusinessUnitChanged();
			}
		}

		/// <summary>
		/// Handles the LoadComplete even of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_LoadComplete(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				BusinessUnitChanged();
			}
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			BindBusinessUnitDropDown();
		}

		/// <summary>
		/// Binds the business unit drop down.
		/// </summary>
		private void BindBusinessUnitDropDown()
		{
			if (!App.User.EmployeeId.HasValue) return;

			BusinessUnits = ServiceClientLocator.EmployeeClient(App).GetEmployeesBusinessUnits(App.User.EmployeeId.Value);

			foreach (var businessUnit in BusinessUnits.Where(businessUnit => businessUnit.IsDefault))
			{
				businessUnit.BusinessUnitName = String.Format("{0} ({1})", businessUnit.BusinessUnitName, CodeLocalise("Global.Default.Text", "Default"));
				BusinessUnitId = Convert.ToInt64(businessUnit.BusinessUnitId);
			}

			BusinessUnitDropDown.DataSource = BusinessUnits;
			BusinessUnitDropDown.DataValueField = "BusinessUnitId";
			BusinessUnitDropDown.DataTextField = "BusinessUnitName";
			BusinessUnitDropDown.DataBind();
			BusinessUnitDropDown.Items.AddLocalisedTopDefault("Global.Company.TopDefault", "- select a #BUSINESS#:LOWER -");

			BusinessUnitDropDown.SelectValue(BusinessUnitId.ToString());
		}


		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SaveDescriptionsButton.Text = SaveLogosButton.Text = SaveMainDetailsButton.Text = CodeLocalise("Global.Save.Label", "Save");
			AddDescriptionTopButton.Text = AddDescriptionBottomButton.Text = CodeLocalise("AddDescriptionButton.Text", "Add another description");
			AddLogoTopButton.Text = AddLogoBottomButton.Text = CodeLocalise("AddLogoButton.Text", "Add another logo");
			AssignNewBusinessUnitButton.Text = CodeLocalise("AssignNewBusinessUnitButton.Text", "Add new or existing business unit");
			MakeDefaultButton.Text = CodeLocalise("MakeDefaultButton.Text", "Make default");
		}

		private void ApplyBranding()
		{
			HideLogoUploadButton.ImageUrl = UrlBuilder.ButtonDeleteIcon();
			
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the BusinessUnitDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void BusinessUnitDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			BusinessUnitChanged();
		}

		/// <summary>
		/// Handles the behaviour when a business unit is changed in the drop down, this can be by the user or programmatically
		/// </summary>
		protected void BusinessUnitChanged()
		{
			BusinessUnitId = BusinessUnitDropDown.SelectedValueToLong();

			if (BusinessUnitId > 0)
			{
				BusinessUnitEditor.Bind(BusinessUnitId, showCompanyDescription: false);

				GetModel();

				BindDescriptionsRepeater();
				BindLogoRepeater();
			}

			if (BusinessUnitId == 0)
			{
				BusinessUnitEditPanel.Visible = false;
				return;
			}

			BusinessUnitEditPanel.CssClass = "";
			BusinessUnitEditPanel.Visible = true;
		}

		/// <summary>
		/// Handles the BusinessUnitSelected event of the SelectBusinessUnitModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnitSelectedEventArgs"/> instance containing the event data.</param>
		protected void SelectBusinessUnitModal_BusinessUnitSelected(object sender, BusinessUnitSelector.BusinessUnitSelectedEventArgs e)
		{
			BusinessUnitId = (e.Model.BusinessUnit.Id.HasValue) ? e.Model.BusinessUnit.Id.Value : 0;

			if (BusinessUnitId == 0)
			{
				e.Model.BusinessUnitAddress.IsPrimary = true;
				e.Model.BusinessUnitDescription.IsPrimary = true;

				if(e.Model.BusinessUnitDescription.Description.IsNullOrEmpty())
					e.Model.BusinessUnitDescription.Description = e.Model.BusinessUnit.Name;

			  bool failedCensorshipCheck;
				var businessUnitModel = ServiceClientLocator.EmployerClient(App).SaveBusinessUnitModel(e.Model, true, true, out failedCensorshipCheck);

        if (failedCensorshipCheck)
        {
					ConfirmationModal.Show(CodeLocalise("CensorshipCheckNew.Title", "#BUSINESS# details change requires approval"),
                                   CodeLocalise("CensorshipCheckNew.Body", "Our staff will review your account amendment request and get back to you shortly. In the meantime, any active jobs will retain existing details. Once approved, these job listings can be amended by re-posting."),
                                   CodeLocalise("MainDetailsSavedCloseModal.Text", "Return to account settings"));
        }

				if (businessUnitModel.IsNotNull() && businessUnitModel.BusinessUnit.IsNotNull())
					BusinessUnitId = businessUnitModel.BusinessUnit.Id.GetValueOrDefault();

				if(BusinessUnitId > 0 && App.User.EmployeeId.HasValue)
					ServiceClientLocator.EmployeeClient(App).AssignEmployeeToBusinessUnit(App.User.EmployeeId.Value, BusinessUnitId);
			}
			else if (BusinessUnits.All(x => x.BusinessUnitId != e.Model.BusinessUnit.Id))
			{
				BusinessUnitId = e.Model.BusinessUnit.Id.GetValueOrDefault();
				
				if(BusinessUnitId > 0 && App.User.EmployeeId.HasValue)
					ServiceClientLocator.EmployeeClient(App).AssignEmployeeToBusinessUnit(App.User.EmployeeId.Value, BusinessUnitId);
			}

			// Rebind the UI
			BindBusinessUnitDropDown();
			
			BusinessUnitEditor.Bind(BusinessUnitId, showCompanyDescription:false);

			GetModel();

			BindDescriptionsRepeater();
			BindLogoRepeater();

			BusinessUnitEditPanel.CssClass = "";
			BusinessUnitEditPanel.Visible = true;
			
			BusinessUnitUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the Click event of the AssignNewBusinessUnitButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AssignNewBusinessUnitButton_Click(object sender, EventArgs e)
		{
		  SelectBusinessUnitModal.Visible = true;

			var legalName = ServiceClientLocator.EmployerClient(App).GetEmployerLegalName(App.User.EmployerId.GetValueOrDefault());
			SelectBusinessUnitModal.Show(App.User.EmployerId.Value, legalName: legalName);
		}

		/// <summary>
		/// Handles the Click event of the SaveMainDetailsButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveMainDetailsButton_Click(object sender, EventArgs e)
		{
			var model = BusinessUnitEditor.Unbind();

		  bool failedCensorshipCheck;
			ServiceClientLocator.EmployerClient(App).SaveBusinessUnitModel(model, true, false, out failedCensorshipCheck);

			// Bind the business unit dropdown as the name could have been changed
			BindBusinessUnitDropDown();
            BusinessUnitEditor.Bind(BusinessUnitId, showCompanyDescription: false);
			
      if (failedCensorshipCheck)
      {
				ConfirmationModal.Show(CodeLocalise("CensorshipCheckMain.Title", "#BUSINESS# details change requires approval"),
                                 CodeLocalise("CensorshipCheckMain.Body", "Our staff will review your account amendment request and get back to you shortly. In the meantime, any active jobs will retain existing details. Once approved, these job listings can be amended by re-posting."),
                                 CodeLocalise("MainDetailsSavedCloseModal.Text", "Return to account settings"));
      }
      else
      {
				ConfirmationModal.Show(CodeLocalise("MainDetailsSaved.Title", "#BUSINESS# saved"),
																 CodeLocalise("MainDetailsSaved.Body", "You have successfully saved the #BUSINESS# record."),
                                 CodeLocalise("MainDetailsSavedCloseModal.Text", "Return to account settings"));
      }
		}

		/// <summary>
		/// Handles the Click event of the MakeDefaultButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void MakeDefaultButton_Click(object sender, EventArgs e)
		{
			if(App.User.EmployeeId.HasValue)
				ServiceClientLocator.EmployeeClient(App).SetBusinessUnitAsDefault(App.User.EmployeeId.Value, BusinessUnitId);

			BindBusinessUnitDropDown();

			ConfirmationModal.Show(CodeLocalise("MakeDefault.Title", "#BUSINESS# default has been changed"),
															 CodeLocalise("MakeDefault.Body", "You have successfully changed your #BUSINESS#:LOWER default."),
															 CodeLocalise("MakeDefaultCloseModal.Text", "Return to account settings"));
			BusinessUnitUpdatePanel.Update();
		}

		/// <summary>
		/// Gets the model.
		/// </summary>
		private void GetModel()
		{
			Model = new ManageBusinessUnitModel
			{
				BusinessUnitDescriptions = ServiceClientLocator.EmployerClient(App).GetBusinessUnitDescriptions(BusinessUnitId),
				BusinessUnitLogos = ServiceClientLocator.EmployerClient(App).GetBusinessUnitLogos(BusinessUnitId),
				BusinessUnitDescriptionsAssociatedWithJobs = ServiceClientLocator.EmployerClient(App).GetBusinessUnitDescriptionsAssociatedWithJob(BusinessUnitId),
				LogosAssociatedWithJobs = ServiceClientLocator.EmployerClient(App).GetBusinessUnitLogosAssociatedWithJob(BusinessUnitId)
			};
		}

		#region Company descriptions

		/// <summary>
		/// Handles the Click event of the SaveDescriptionsButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveDescriptionsButton_Click(object sender, EventArgs e)
		{
			Model.BusinessUnitDescriptions = UnbindDescriptionsRepeater();

		  var failedCensorshipCheck = ServiceClientLocator.EmployerClient(App).SaveBusinessUnitDescriptions(BusinessUnitId, Model.BusinessUnitDescriptions, true);

			BindDescriptionsRepeater();

			// Bind business unit editor as primary description may have been updated
			BusinessUnitEditor.Bind(BusinessUnitId, showCompanyDescription: false);

      if (failedCensorshipCheck)
      {
				ConfirmationModal.Show(CodeLocalise("CensorshipCheckDescription.Title", "#BUSINESS# description change requires approval"),
                               CodeLocalise("CensorshipCheckDescription.Body", "Our staff will review your account amendment request and get back to you shortly. In the meantime, any active jobs will retain existing details. Once approved, these job listings can be amended by re-posting."),
                               CodeLocalise("CloseModal.Text", "Return to account settings"));
      }
      else
      {
				ConfirmationModal.Show(CodeLocalise("DescriptionsSaved.Title", "#BUSINESS# descriptions saved"),
																 CodeLocalise("DescriptionsSaved.Body", "You have successfully saved the #BUSINESS#:LOWER descriptions."),
                                 CodeLocalise("CloseModal.Text", "Return to account settings"));
      }
			BusinessUnitUpdatePanel.Update();
		}
		
		/// <summary>
		/// Unbinds the descriptions repeater.
		/// </summary>
		private List<BusinessUnitDescriptionDto> UnbindDescriptionsRepeater()
		{
			return (from RepeaterItem item in DescriptionsRepeater.Items
							let descriptionId = Convert.ToInt64(((HiddenField)item.FindControl("DescriptionIdHidden")).Value)
							let descriptionName = ((TextBox)item.FindControl("DescriptionNameTextBox")).TextTrimmed()
							let defaultDescriptionName = ((RadioButton)item.FindControl("DefaultDescriptionNameRadioButton")).Checked
							let description = ((TextBox)item.FindControl("DescriptionTextBox")).TextTrimmed().TruncateString(2000)
							select new BusinessUnitDescriptionDto
							{
								BusinessUnitId = BusinessUnitId,
								Id = descriptionId,
								Title = descriptionName.RemoveRogueCharacters(),
								IsPrimary = defaultDescriptionName,
								Description = description.RemoveRogueCharacters()
							}).ToList();
		}

		/// <summary>
		/// Binds the descriptions repeater.
		/// </summary>
		private void BindDescriptionsRepeater()
		{
			DescriptionsRepeater.DataSource = Model.BusinessUnitDescriptions;
			DescriptionsRepeater.DataBind();

      AddDescriptionBottomButton.Visible = Model.BusinessUnitDescriptions.Any();
		}

		/// <summary>
		/// Handles the Click event of the AddDescriptionButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddDescriptionButton_Click(object sender, EventArgs e)
		{
			Model.BusinessUnitDescriptions = UnbindDescriptionsRepeater();
			Model.BusinessUnitDescriptions.Add(new BusinessUnitDescriptionDto { Id = 0, IsPrimary = Model.BusinessUnitDescriptions.Count == 0});
			BindDescriptionsRepeater();
		}

		/// <summary>
		/// Handles the ItemCommand event of the DescriptionsRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
		protected void DescriptionsRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			Model.BusinessUnitDescriptions = UnbindDescriptionsRepeater();
			var descriptionToRemove = Model.BusinessUnitDescriptions[e.Item.ItemIndex];
			if(Model.BusinessUnitDescriptionsAssociatedWithJobs.Exists(x => x.Id == descriptionToRemove.Id))
			{
				DescriptionsWarningModal.Show(CodeLocalise("DescriptionCannotBeDeleted.Title", "Description cannot be deleted"),
				                              CodeLocalise("DescriptionCannotBeDeleted.Body", "This description cannot be deleted as it is associated with a job."),
																			CodeLocalise("Global.Close.Text", "Close"));
      }
      else if (Model.BusinessUnitDescriptions.Exists(x => x.Id == descriptionToRemove.Id && x.IsPrimary))
      {
        DescriptionsWarningModal.Show(CodeLocalise("DescriptionCannotBeDeleted.Title", "Description cannot be deleted"),
                                      CodeLocalise("DescriptionCannotBeDeletedAsDefault.Body", "This description cannot be deleted as it is the default description."),
                                      CodeLocalise("Global.Close.Text", "Close"));
      }
      else if (Model.BusinessUnitDescriptions.Count == 1)
      {
        DescriptionsWarningModal.Show(CodeLocalise("DescriptionCannotBeDeleted.Title", "Description cannot be deleted"),
                                      CodeLocalise("DescriptionCannotBeDeletedAsLast.Body", "This description cannot be deleted as you must have at least one description saved."),
                                      CodeLocalise("Global.Close.Text", "Close"));
      }
			else
			{
        Model.BusinessUnitDescriptions.RemoveAt(e.Item.ItemIndex);
        DescriptionsWarningModal.Show(CodeLocalise("DescriptionMarkedOfrDeletion.Title", "Description marked for deletion"),
                                      CodeLocalise("DescriptionMarkedOfrDeletion.Body", "This description has been marked for deletion, click 'save' to confirm."),
                                      CodeLocalise("Global.Close.Text", "Close"));
				BindDescriptionsRepeater();
			}
		}

		/// <summary>
		/// Handles the ItemDataBound event of the DescriptionsRepeater control.
		/// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void DescriptionsRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
		{
			// Do nothing if this isn't a Item or AlternateItem
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;
			
			var descriptionNameTextBox = (TextBox)e.Item.FindControl("DescriptionNameTextBox");
			descriptionNameTextBox.Text = ((BusinessUnitDescriptionDto)e.Item.DataItem).Title;

			var descriptionNameLabel = (Label)e.Item.FindControl("DescriptionNameLabel");
			descriptionNameLabel.AssociatedControlID = descriptionNameTextBox.ID;

			var defaultDescriptionNameRadioButton = (RadioButton)e.Item.FindControl("DefaultDescriptionNameRadioButton");
			defaultDescriptionNameRadioButton.Checked = ((BusinessUnitDescriptionDto)e.Item.DataItem).IsPrimary;
			defaultDescriptionNameRadioButton.Attributes.Add("onclick", "SetUniqueRadioButton('DescriptionsRepeater.*DefaultDescriptionName', this)");
			defaultDescriptionNameRadioButton.Attributes.Add("onkeypress", "SetUniqueRadioButton('DescriptionsRepeater.*DefaultDescriptionName', this)");
            defaultDescriptionNameRadioButton.InputAttributes.Add("Title","Default Description");

			var descriptionNameRequired = (RequiredFieldValidator)e.Item.FindControl("DescriptionNameRequired");
			descriptionNameRequired.ErrorMessage = CodeLocalise("DescriptionNameRequired.ErrorMessage", "Description name required");

			var descriptionTextBox = (TextBox)e.Item.FindControl("DescriptionTextBox");
			descriptionTextBox.Text = ((BusinessUnitDescriptionDto)e.Item.DataItem).Description;

			var descriptionLabel = (Label)e.Item.FindControl("DescriptionLabel");
			descriptionLabel.AssociatedControlID = descriptionTextBox.ID;

			var descriptionRequired = (RequiredFieldValidator)e.Item.FindControl("DescriptionRequired");
			descriptionRequired.ErrorMessage = CodeLocalise("DescriptionRequired.ErrorMessage", "Description required");

			var descriptionIdHidden = (HiddenField)e.Item.FindControl("DescriptionIdHidden");
			descriptionIdHidden.Value = ((BusinessUnitDescriptionDto)e.Item.DataItem).Id.ToString();

			var descriptionLengthValidator = (CustomValidator)e.Item.FindControl("DescriptionLengthValidator");
			descriptionLengthValidator.ErrorMessage = CodeLocalise("DescriptionLengthValidator.ErrorMessage", "Description must be 2000 characters or less");
		}

		#endregion

		#region Logos

		/// <summary>
		/// Handles the Click event of the AddLogoButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddLogoButton_Click(object sender, EventArgs e)
		{
			Model.BusinessUnitLogos = UnbindLogoRepeater();

			if (ShowLogoUploaderHidden.Value == "true")
				UnbindLogoUpload();

			ShowLogoUploaderHidden.Value = "true";
			LogoUpload.EnableValidation();
			BindLogoRepeater();
		}

		/// <summary>
		/// Binds the logo repeater.
		/// </summary>
		private void BindLogoRepeater()
		{
			LogoRepeater.DataSource = Model.BusinessUnitLogos;
			LogoRepeater.DataBind();

		  AddLogoBottomButton.Visible = Model.BusinessUnitLogos.Any();
		}

		/// <summary>
		/// Unbinds the logo repeater.
		/// </summary>
		/// <returns></returns>
		private List<BusinessUnitLogoDto> UnbindLogoRepeater()
		{
			return (from RepeaterItem item in LogoRepeater.Items
							let logoId = Convert.ToInt64(((HiddenField)item.FindControl("LogoIdHidden")).Value)
							let logoName = ((TextBox)item.FindControl("LogoNameTextBox")).TextTrimmed()
							let logo = Model.BusinessUnitLogos[item.ItemIndex].Logo
							select new BusinessUnitLogoDto
							{
								Id = logoId,
								BusinessUnitId = BusinessUnitId,
								Name = logoName,
								Logo = logo
							}).ToList();
		}

		/// <summary>
		/// Unbinds the logo upload.
		/// </summary>
		private void UnbindLogoUpload()
		{
			if (Model.BusinessUnitLogos.IsNull()) Model.BusinessUnitLogos = new List<BusinessUnitLogoDto>();

			if (LogoUpload.HasFile())
			{
				Model.BusinessUnitLogos.Add(new BusinessUnitLogoDto
				{
					Id = 0,
					BusinessUnitId = BusinessUnitId,
					Name = LogoUpload.LogoName,
					Logo = LogoUpload.Logo
				});
			}
		}

		/// <summary>
		/// Handles the Click event of the SaveLogosButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveLogosButton_Click(object sender, EventArgs e)
		{
      Model.BusinessUnitLogos = UnbindLogoRepeater();

			if (ShowLogoUploaderHidden.Value == "true")
				UnbindLogoUpload();

			ShowLogoUploaderHidden.Value = "false";
			LogoUpload.Clear();
			LogoUpload.DisableValidation();

      Model.BusinessUnitLogos = ServiceClientLocator.EmployerClient(App).SaveBusinessUnitLogos(BusinessUnitId, Model.BusinessUnitLogos);

			BindLogoRepeater();
			ConfirmationModal.Show(CodeLocalise("LogosSaved.Title", "#BUSINESS# logos saved"),
															 CodeLocalise("LogosSaved.Body", "You have successfully saved the #BUSINESS#:LOWER logos."),
															 CodeLocalise("CloseModal.Text", "Return to account settings"));
			BusinessUnitUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the LogoRepeater control.
		/// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void LogoRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
		{
			// Do nothing if this isn't a Item or AlternateItem
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			var logoNameTextBox = (TextBox)e.Item.FindControl("LogoNameTextBox");
			logoNameTextBox.Text = ((BusinessUnitLogoDto)e.Item.DataItem).Name;

			var logoNameLabel = (Label)e.Item.FindControl("LogoNameLabel");
			logoNameLabel.AssociatedControlID = logoNameTextBox.ID;

			var logoNameRequired = (RequiredFieldValidator)e.Item.FindControl("LogoNameRequired");
			logoNameRequired.ErrorMessage = CodeLocalise("LogoNameRequired.ErrorMessage", "Logo name required");

			var logoId = ((BusinessUnitLogoDto)e.Item.DataItem).Id.GetValueOrDefault();

			var logoIdHidden = (HiddenField)e.Item.FindControl("LogoIdHidden");
			logoIdHidden.Value = logoId.ToString();

			var logoImage = (Image)e.Item.FindControl("LogoImage");
			logoImage.ImageUrl = UrlBuilder.ViewLogo(logoId);
		}

		/// <summary>
		/// Handles the ItemCommand event of the LogoRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
		protected void LogoRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			Model.BusinessUnitLogos = UnbindLogoRepeater();
			var logoToRemove = Model.BusinessUnitLogos[e.Item.ItemIndex];
			if (Model.LogosAssociatedWithJobs.Exists(x => x.Id == logoToRemove.Id))
			{
				LogosWarningModal.Show(CodeLocalise("LogoCannotBeDeleted.Title", "Logo cannot be deleted"),
																			CodeLocalise("LogoCannotBeDeleted.Body", "This logo cannot be deleted as it is associated with a job."),
																			CodeLocalise("Global.Close.Text", "Close"));
			}
			else
			{
				Model.BusinessUnitLogos.RemoveAt(e.Item.ItemIndex);
				BindLogoRepeater();
			}
		}

		/// <summary>
		/// Handles the Click event of the HideLogoUploadButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void HideLogoUploadButton_Click(object sender, EventArgs e)
		{
			ShowLogoUploaderHidden.Value = "false";
			LogoUpload.Clear();
			LogoUpload.DisableValidation();
		}

		#endregion

	}
}