﻿<%@ Page Title="Manage Searches" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageSearches.aspx.cs" Inherits="Focus.Web.WebTalent.ManageSearches" %>
<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebTalent/Controls/TabNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="SavedSearchesList" Src="~/Code/Controls/User/SavedSearchesList.ascx" %>
<%@ Register TagPrefix="uc" TagName="SettingsLinks" Src="Controls/AccountSettingsLinks.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<uc:SettingsLinks ID="SettingLinksMain" runat="server" />

	<h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>
   <uc:SavedSearchesList ID="SavedSearchesList" runat="server" />	
	
</asp:Content>
