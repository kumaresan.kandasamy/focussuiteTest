﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True" CodeBehind="JobPosting.aspx.cs" Inherits="Focus.Web.WebTalent.JobPosting" %>
<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobPostingDetails.ascx" tagPrefix="uc" tagName="JobPostingDetails" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>
			    
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel ID="UpdatePanelJobPosting" runat="server" UpdateMode="Always">
		<ContentTemplate>
				<div>
					<uc:JobPostingDetails ID="JobPostingDetails" runat="server" />
				</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
