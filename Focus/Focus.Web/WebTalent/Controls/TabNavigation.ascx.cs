﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Code;
using Focus.Core;
using Focus.Web.Code;
using Focus.Common.Extensions;
using Focus.Web.Controllers.Code.Controls.User;
using Framework.Core;

#endregion

namespace Focus.Web.WebTalent.Controls
{
	public partial class TabNavigation : UserControlBase
	{
		protected NavigationController PageController { get { return PageControllerBaseProperty as NavigationController; } }

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		public override IPageController RegisterPageController()
		{
			return new NavigationController(App);
		}
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsJobsTab) 
				TabJobs.Attributes.Add("class", "active");
			else if (IsPoolTab)
				TabPool.Attributes.Add("class", "active");
			else if (IsJobSearchTab)
				TabJobSearch.Attributes.Add("class", "active");

		  if (!IsPostBack)
		    LocaliseUI();

			var talentPoolSearch = Utilities.ShowTalentPoolSearch();

			TabPool.Visible = talentPoolSearch;

			TabJobSearch.Visible = App.Settings.Theme == FocusThemes.Workforce;

      UploadJobsButton.Visible = !this.App.Settings.HideUploadMultipleJobs && Utilities.ShowJobUpload(App.User.EmployeeId);

      #region FVN-6056
      UploadJobsButton.Attributes.Add("class", "button1");
      if (UploadJobsButton.Visible && DisableUploadJobButton)
      {
          UploadJobsButton.Attributes.Remove("class");
          UploadJobsButton.Enabled = false;
          UploadJobsButton.CssClass = "button1Disabled";
      }

      PostNewJobButton.Attributes.Add("class", "button1");
      if (DisablePostJobButton)
      {
          PostNewJobButton.Attributes.Remove("class");
          PostNewJobButton.Enabled = false;
          PostNewJobButton.CssClass = "button1Disabled";
      } 
      #endregion
		}

    /// <summary>
    /// Localises the controls on the page
    /// </summary>
    private void LocaliseUI()
    {
      PostNewJobButton.Text = CodeLocalise("PostNewJob.Label.NoEdit", "Post a new #POSTINGTYPE#").ToSentenceCase();
      UploadJobsButton.Text = CodeLocalise("JobUpload.LinkText", "Upload multiple #POSTINGTYPES#").ToSentenceCase();
    }

		/// <summary>
		/// Gets a value indicating whether this instance is jobs tab.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is jobs tab; otherwise, <c>false</c>.
		/// </value>
		private bool IsJobsTab
		{
      get { return (Page.Is(UrlBuilder.TalentJobs()) || Page.Is(UrlBuilder.JobWizard()) || Page.Is(UrlBuilder.JobView())); }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is pool tab.
		/// </summary>
		/// <value>
		/// 	<c>true</c> if this instance is pool tab; otherwise, <c>false</c>.
		/// </value>
		private bool IsPoolTab
		{
			get { return Page.Is(UrlBuilder.TalentPoolForPathCompareOnly()); }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is job search tab.
		/// </summary>
		/// <value>
		/// <c>true</c> if this instance is job search tab; otherwise, <c>false</c>.
		/// </value>
		private bool IsJobSearchTab
		{
			get { return (Page.Is(UrlBuilder.TalentNewJobSearch()) || Page.Is(UrlBuilder.TalentJobSearch()) || Page.Is(UrlBuilder.TalentJobSearchResultsPage())); }
		}

		/// <summary>
		/// Handles the Click event of the PostNewJobButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PostNewJobButton_Click(object sender, EventArgs e)
		{
			Response.Redirect(UrlBuilder.JobWizard(), true);
		}

    /// <summary>
    /// Handles the Click event of the UploadJobButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void UploadJobButton_Click(object sender, EventArgs e)
    {
      Response.Redirect(UrlBuilder.JobUpload());
    }

    #region FVN-6056 - Deprecated
    /// <summary>
    /// Sets a value indicating whether  to enable post job button.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if [enable post job button]; otherwise, <c>false</c>.
    /// </value>
    public bool EnablePostJobButton
    {
        set
        {
            PostNewJobButton.Enabled = value;
            // Set style to override default aspNetDisabled class for disabled elements
            PostNewJobButton.CssClass = "button1Disabled";
        }
    }

    /// <summary>
    /// Sets a value indicating whether  to enable upload job button.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if [enable upload job button]; otherwise, <c>false</c>.
    /// </value>
    public bool EnableUploadJobButton
    {
        set
        {
            UploadJobsButton.Enabled = value;
            // Set style to override default aspNetDisabled class for disabled elements
            UploadJobsButton.CssClass = "button1Disabled";
        }
    } 
    #endregion

    public bool DisablePostJobButton { get; set; }

    public bool DisableUploadJobButton { get; set; }
	}
}