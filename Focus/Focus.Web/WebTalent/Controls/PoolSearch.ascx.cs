﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Services.Core.Extensions;
using Focus.Web.Code.Controls.User.SearchCriteria;
using Focus.Web.WebTalent.Code;
using Framework.Core;

using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Web.Code;
using Focus.Common.Extensions;
using Focus.Common;
using System.Runtime.Serialization.Formatters.Binary;

#endregion

namespace Focus.Web.WebTalent.Controls
{
  public partial class PoolSearch : PoolSearchUserControl
	{
		private const string ManualJobTitleSelectedValue = "Manual";
		private const string NoJobTitleSelectedValue = "0";

  	#region Private Variables

		/// <summary>
		/// Gets or sets the _resume filename.
		/// </summary>
		/// <value>The _resume filename.</value>
		private string ResumeFilename
		{
			get { return GetViewStateValue<string>("PoolSearch:ResumeFilename"); }
			set { SetViewStateValue("PoolSearch:ResumeFilename", value); }
		}

		/// <summary>
		/// Gets or sets the content of the _resume.
		/// </summary>
		/// <value>The content of the _resume.</value>
		private byte[] ResumeContent
		{
			get { return GetViewStateValue<byte[]>("PoolSearch:ResumeContent"); }
			set { SetViewStateValue("PoolSearch:ResumeContent", value); }
		}

		private long JobId
		{
			get
			{
			  if (SearchCriteria.IsNull() || SearchCriteria.JobDetailsCriteria.IsNull())
			    return 0;

        return SearchCriteria.JobDetailsCriteria.JobId;
			}
		}

        private bool AdvancedSearch
        {
            get;
            set;
        }

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ResumeFileUploadRegularEx.ValidationExpression = App.Settings.ResumeUploadAllowedFileRegExPattern;
			
			if (!IsPostBack)
			{
				if(JobId != 0)
           JobIdKnownHidden.Value = "true";
				
        Bind();

				if (JobId > 0) OpenJobsDropDownList.SelectValue(JobId.ToString());
				
				if (SearchSession.IsNullOrEmpty() || SearchSession == "0")
				{
					if (Page.RouteData.Values.ContainsKey("searchsession")) SearchSession = Page.RouteData.Values["searchsession"].ToString();
				}

				if(SearchCriteria.IsNotNull())
					LoadSearchCriteria(SearchCriteria);
				
        if (SavedSearchId > 0)
          SavedSearchDropDown.SelectedValue = SavedSearchId.ToString();

				if (JobId > 0) AdvancedSearchPanelExtender.Collapsed = true;
			}
            if (App.GetSessionValue<string>("advancedsearch") == "true")
            {
                AdvancedSearch = true;
                App.RemoveSessionValue("advancedsearch");
            }
                  
			LocaliseUI();
			ApplyBranding();
			ApplyTheme();

			if (ResumeFilename.IsNotNullOrEmpty())
				UploadedResumeLabel.Text = string.Format("{0} {1}", CodeLocalise("SampleResumeSearch.FileUploaded.Label", "Basing search on"), ResumeFilename);

			RegisterJavascript();

			SaveSearchErrorLabel.Text = "";
		}

		/// <summary>
		/// Handles the Click event of the KeywordSearchButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void KeywordSearchButton_Click(object sender, EventArgs e)
		{
			if (KeywordsTextBox.TextTrimmed().IsNullOrEmpty())
				return;

      //Clear other search controls
			ClearResume();
			JobTitleTextBox.Text = "";
			SavedSearchDropDown.SelectedIndex = 0;

			// Set new guid so new search session is used
			SearchSession = Guid.NewGuid().ToString();

			var additionalCriteriamodel = UnbindAdditionalCriteria();
			var jobDetailsCriteria = UnbindJobDetailsCriteria();

			if (jobDetailsCriteria.JobTitle.IsNull())
				jobDetailsCriteria = null;
			
			SearchCriteria = new CandidateSearchCriteria
			                     	{
															SearchType = jobDetailsCriteria.IsNull()
																			 ? CandidateSearchTypes.ByKeywords
																			 : CandidateSearchTypes.ByJobDescription,
															JobDetailsCriteria = jobDetailsCriteria,
															KeywordCriteria = UnbindKeywordSearchCriteria(),
															AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
															MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0
			                     	};

			Response.Redirect(UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, searchSession: SearchSession));
		}

		/// <summary>
		/// Handles the Click event of the OpenJobButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OpenJobButton_Click(object sender, EventArgs e)
		{
			if ((OpenJobsDropDownList.SelectedValue == ManualJobTitleSelectedValue &&
			     JobTitleTextBox.TextTrimmed().IsNullOrEmpty()) || OpenJobsDropDownList.SelectedValue == NoJobTitleSelectedValue)
				return;

			//Clear other search controls
			KeywordsTextBox.Text = "";
			KeywordContextDropDown.SelectedIndex = 0;
			KeywordScopeDropDown.SelectedIndex = 0;
			
			ClearResume();
			SavedSearchDropDown.SelectedIndex = 0;

			// Set new guid so new search session is used
			SearchSession = Guid.NewGuid().ToString();

			var additionalCriteriamodel = UnbindAdditionalCriteria();

			SearchCriteria = new CandidateSearchCriteria
			                     	{
			                     		SearchType = CandidateSearchTypes.ByJobDescription,
			                     		JobDetailsCriteria = UnbindJobDetailsCriteria(),
															AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
															MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0
			                     	};

			switch (OpenJobsDropDownList.SelectedValue)
			{
				case ManualJobTitleSelectedValue:
					Response.Redirect(UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, searchSession: SearchSession));
					break;
				default:
          if (App.Settings.Theme == FocusThemes.Education)
            Response.Redirect(UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, JobId));
          else
					  Response.Redirect(UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, JobId, searchSession: SearchSession));
					break;
			}
		}

		/// <summary>
		/// Handles the Click event of the SampleResumeButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SampleResumeButton_Click(object sender, EventArgs e)
		{
			//Clear other search controls
			KeywordsTextBox.Text = "";
			KeywordContextDropDown.SelectedIndex = 0;
			KeywordScopeDropDown.SelectedIndex = 0;
			JobTitleTextBox.Text = "";
			OpenJobsDropDownList.SelectedIndex = 0;
			SavedSearchDropDown.SelectedIndex = 0;

			GetResumeSearchDetails();

		  var validateResume = ServiceClientLocator.ResumeClient(App).GetTaggedResume(ResumeContent, Path.GetExtension(ResumeFilename));
      if (validateResume.Contains("<error>"))
      {
        if (validateResume.Contains("BGTEC005: Text too large"))
        {
          ResumeCustomValidator.IsValid = false;
          ResumeCustomValidator.ErrorMessage = CodeLocalise("ResumeCustomValidator.TooLarge", "Resume is too large");
        }
      }

		  if (!ResumeCustomValidator.IsValid)
		    return;

        // Set new guid so new search session is used
			SearchSession = Guid.NewGuid().ToString();

			var additionalCriteriamodel = UnbindAdditionalCriteria();


			SearchCriteria = new CandidateSearchCriteria
			                     	{
			                     		SearchType = CandidateSearchTypes.ByResume,
			                     		ResumeCriteria = UnbindResumeCriteria(),
															AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
															MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0
			                     	};

			Response.Redirect(UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, searchSession: SearchSession));
		}

		/// <summary>
		/// Handles the Click event of the SaveSearchButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveSearchButton_Click(object sender, EventArgs e)
		{
            var oldSearchCriteria = SearchCriteria;
			SearchCriteria = null;// new CandidateSearchCriteria();

			SavedSearchId = SavedSearchDropDown.SelectedIndex > 0 ? SavedSearchDropDown.SelectedValueToLong() : 0;

			if (SavedSearchId == 0)
			{
				var savedSearchCount = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearchView();
				if (savedSearchCount.Count >= App.Settings.MaximumSavedSearches)
				{
					Confirmation.Show(CodeLocalise("SavedSearchesMaximumExceeded.Title", "Saved searches exceeded"),
														CodeLocalise("SavedSearchesMaximumExceeded.Body",
																				 "Saving this search exceeds the maximum number of searches you are allowed.<br><br>Please go to your Manage Saved Searches page via Account Settings to delete any unwanted searches before continuing."),
														CodeLocalise("Global.Close.Text", "Close"));
					return;
				}
			}

			var additionalCriteriamodel = UnbindAdditionalCriteria();
            var blankAdditionalSearchCriteria = new CandidateSearchAdditionalCriteria();

            blankAdditionalSearchCriteria.HomeBasedJobSeekers = false;
            blankAdditionalSearchCriteria.JobTypesConsidered = JobTypes.None;
            blankAdditionalSearchCriteria.NCRCLevel = 0;
            blankAdditionalSearchCriteria.EducationCriteria = new CandidateSearchEducationCriteria();
            blankAdditionalSearchCriteria.EducationCriteria.EducationLevels = new System.Collections.Generic.List<EducationLevel>();
            blankAdditionalSearchCriteria.StatusCriteria = new CandidateSearchStatusCriteria();
		  // We need to work out if this is a search based on an Resume, Job or Keywords
			if (ResumeFilename.IsNotNullOrEmpty() || ResumeFileUpload.HasFile)
			{
				GetResumeSearchDetails();

				SearchCriteria = new CandidateSearchCriteria
				                 	{
				                 		SearchType = CandidateSearchTypes.ByResume,
				                 		ResumeCriteria = UnbindResumeCriteria(),
														AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
														MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0
				                 	};
			}
			else if ((OpenJobsDropDownList.SelectedValue == ManualJobTitleSelectedValue && JobTitleTextBox.TextTrimmed().IsNotNullOrEmpty()) || OpenJobsDropDownList.SelectedValueToLong() != 0 || (JobId != 0 && KeywordsTextBox.TextTrimmed().IsNullOrEmpty()))
			{
				SearchCriteria = new CandidateSearchCriteria
													{
														SearchType = CandidateSearchTypes.ByJobDescription,
														JobDetailsCriteria = UnbindJobDetailsCriteria(),
														AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
														MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0
													};
			}
			else if(KeywordsTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
                
                SearchCriteria = new CandidateSearchCriteria
                                    {
                                        SearchType = CandidateSearchTypes.ByKeywords,
                                        KeywordCriteria = UnbindKeywordSearchCriteria(),
                                        AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
                                        MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0
                                    };
			}
			else if (AdvancedSearch || IsDifferent(additionalCriteriamodel.AdditionalCriteria, blankAdditionalSearchCriteria))
			{
				SearchCriteria = new CandidateSearchCriteria
				{
					SearchType = CandidateSearchTypes.OpenSearch,
					AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
					MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0
				};
			}
			else
			{
				SaveSearchErrorLabel.Text = CodeLocalise("SaveSearch.ErrorMessage", "Please enter search criteria before saving a search.");
				OnCompleted(e);
                SearchCriteria = oldSearchCriteria;
				return;
			}

      if (SavedSearchId > 0)
      {
          //FVN-6966 AIP Remove popup
        //SaveSearchAction.Show(SavedSearchId,SearchCriteria);
          var savedSearchView = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearch(SavedSearchId);
          ServiceClientLocator.SearchClient(App).SaveCandidateSearch(SearchCriteria, SavedSearchId,  savedSearchView.SavedSearch.Name,  savedSearchView.SavedSearch.AlertEmailRequired,
                                                                                                    savedSearchView.SavedSearch.AlertEmailFrequency,
                                                                                                   savedSearchView.SavedSearch.AlertEmailFormat,
                                                                                                    savedSearchView.SavedSearch.AlertEmailAddress);

          Confirmation.Show(CodeLocalise("Confirmation.Title", "Search saved"),
                                              CodeLocalise("Confirmation.Details", "Confirmation that {0} search has been saved.", savedSearchView.SavedSearch.Name),
                                              CodeLocalise("Global.Close.Text", "Close"));
      }
      else
      {
        if (SearchCriteria.IsNotNull())
          SaveSearchAction.Show(SearchCriteria);
      }
		}

		/// <summary>
		/// Handles the Click event of the ApplyAdvancedSearch control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ApplyAdvancedSearch_Click(object sender, EventArgs e)
		{
			var candidateId = AdditionalCriteria.GetCandidateId();
            
			if (SearchCriteria.IsNull())
			{
				// Set new guid so new search session is used
				SearchSession = Guid.NewGuid().ToString();

        SearchCriteria = new CandidateSearchCriteria
        {
         SearchType = CandidateSearchTypes.OpenSearch,
        };

			  if (candidateId.HasValue && candidateId > 0)
			  {
			    SearchCriteria.CandidateId = candidateId;
			  }
			  else
			  {
					var additionalCriteriamodel = UnbindAdditionalCriteria();

					SearchCriteria.AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null;
					SearchCriteria.MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0;
			  }
			 
			  Response.Redirect(UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, searchSession: SearchSession, isAdvancedSearch:true));
			}
			else
			{
				var additionalCriteriamodel = UnbindAdditionalCriteria();

				SearchCriteria.CandidateId = candidateId;
				SearchCriteria.AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null;
				SearchCriteria.MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0;
				OnPoolSearchRequest(new PoolSearchRequestEventArgs(SearchCriteria));
                
                var blankAdditionalSearchCriteria = new CandidateSearchAdditionalCriteria();

                blankAdditionalSearchCriteria.HomeBasedJobSeekers = false;
                blankAdditionalSearchCriteria.JobTypesConsidered = JobTypes.None;
                blankAdditionalSearchCriteria.NCRCLevel = 0;
                blankAdditionalSearchCriteria.EducationCriteria = new CandidateSearchEducationCriteria();
                blankAdditionalSearchCriteria.EducationCriteria.EducationLevels = new System.Collections.Generic.List<EducationLevel>();
                blankAdditionalSearchCriteria.StatusCriteria= new CandidateSearchStatusCriteria();


              /*  var a = criteria1.Certifications == (criteria2.Certifications) ; var b = criteria1.DrivingLicenceCriteria == criteria2.DrivingLicenceCriteria ; var c= criteria1.EducationCriteria.EducationLevel == (criteria2.EducationCriteria.EducationLevel)
                    ; var d= criteria1.EducationCriteria.EducationLevels == (criteria2.EducationCriteria.EducationLevels) ; var ee =  criteria1.EducationCriteria.EnrollmentStatus == criteria2.EducationCriteria.EnrollmentStatus ; var f = criteria1.EducationCriteria.ExtraDegreeLevelEducationIds == (criteria2.EducationCriteria.ExtraDegreeLevelEducationIds) ; var g =criteria1.EducationCriteria.IncludeAlumni == (criteria2.EducationCriteria.IncludeAlumni)
                ; var h = criteria1.EducationCriteria.InternshipSkills == (criteria2.EducationCriteria.InternshipSkills) ; var j = criteria1.EducationCriteria.MinimumExperienceMonths == (criteria2.EducationCriteria.MinimumExperienceMonths) ; var k = criteria1.EducationCriteria.MinimumGPA == (criteria2.EducationCriteria.MinimumGPA) ; var l = criteria1.EducationCriteria.MinimumUndergraduateLevel == (criteria2.EducationCriteria.MinimumUndergraduateLevel)
                ; var m = criteria1.EducationCriteria.MonthsUntilExpectedCompletion == (criteria2.EducationCriteria.MonthsUntilExpectedCompletion) ; var n = criteria1.EducationCriteria.ProgramsOfStudy == (criteria2.EducationCriteria.ProgramsOfStudy) ; var o = criteria1.HomeBasedJobSeekers == (criteria2.HomeBasedJobSeekers) ; var p = criteria1.IndustriesCriteria == (criteria2.IndustriesCriteria) ; var q= criteria1.JobTypesConsidered == (criteria2.JobTypesConsidered)
                ; var r = criteria1.Languages == (criteria2.Languages) ; var s = criteria1.LanguagesWithProficiencies == criteria2.LanguagesWithProficiencies ; var t = criteria1.Licences == (criteria2.Licences) ; var u = criteria1.LocationCriteria == criteria2.LocationCriteria ; var v= criteria1.NCRCLevel == (criteria2.NCRCLevel)
                ; var w= criteria1.StatusCriteria.USCitizen == (criteria2.StatusCriteria.USCitizen) ; var x = criteria1.StatusCriteria.Veteran == (criteria2.StatusCriteria.Veteran);*/

                if(IsDifferent(SearchCriteria.AdditionalCriteria,blankAdditionalSearchCriteria))
                    App.SetSessionValue<string>("advancedsearch", "true");
                
                
			}
		}

      //FVN-6966 AIP
        public bool IsDifferent(CandidateSearchAdditionalCriteria criteria1, CandidateSearchAdditionalCriteria criteria2)
        {
            return (!(criteria1.Certifications == (criteria2.Certifications) && criteria1.DrivingLicenceCriteria == criteria2.DrivingLicenceCriteria && criteria1.EducationCriteria.EducationLevel == (criteria2.EducationCriteria.EducationLevel)
                    && criteria1.EducationCriteria.EducationLevels.Count() == criteria2.EducationCriteria.EducationLevels.Count() && criteria1.EducationCriteria.EnrollmentStatus == criteria2.EducationCriteria.EnrollmentStatus && criteria1.EducationCriteria.ExtraDegreeLevelEducationIds == (criteria2.EducationCriteria.ExtraDegreeLevelEducationIds) && criteria1.EducationCriteria.IncludeAlumni == (criteria2.EducationCriteria.IncludeAlumni)
                && criteria1.EducationCriteria.InternshipSkills == (criteria2.EducationCriteria.InternshipSkills) && criteria1.EducationCriteria.MinimumExperienceMonths == (criteria2.EducationCriteria.MinimumExperienceMonths) && criteria1.EducationCriteria.MinimumGPA == (criteria2.EducationCriteria.MinimumGPA) && criteria1.EducationCriteria.MinimumUndergraduateLevel == (criteria2.EducationCriteria.MinimumUndergraduateLevel)
                && criteria1.EducationCriteria.MonthsUntilExpectedCompletion == (criteria2.EducationCriteria.MonthsUntilExpectedCompletion) && criteria1.EducationCriteria.ProgramsOfStudy == (criteria2.EducationCriteria.ProgramsOfStudy) && criteria1.HomeBasedJobSeekers == (criteria2.HomeBasedJobSeekers) && criteria1.IndustriesCriteria == (criteria2.IndustriesCriteria) && criteria1.JobTypesConsidered == (criteria2.JobTypesConsidered)
                && criteria1.Languages == (criteria2.Languages) && criteria1.LanguagesWithProficiencies == criteria2.LanguagesWithProficiencies && criteria1.Licences == (criteria2.Licences) && criteria1.LocationCriteria == criteria2.LocationCriteria && criteria1.NCRCLevel == (criteria2.NCRCLevel)
                && criteria1.StatusCriteria.USCitizen == (criteria2.StatusCriteria.USCitizen) && criteria1.StatusCriteria.Veteran == (criteria2.StatusCriteria.Veteran) && criteria1.AvailabilityCriteria == criteria2.AvailabilityCriteria));
            
        }
		
    #region Clear search event

    /// <summary>
    /// Clear keyword and advance search criteria 
    /// </summary>
    /// <param name="sender">The source of the event</param>
    /// <param name="e">The system event</param>
    protected void ClearSearchButton_Click(object sender, EventArgs e)
    {
      //SearchCriteria.JobDetailsCriteria.JobId
      ClearSearchCriteria();
      Response.Redirect(UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, 0));
    }

    #endregion

    /// <summary>
		/// Handles the Click event of the SavedSearchButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SavedSearchButton_Click(object sender, EventArgs e)
		{
      if (SavedSearchDropDown.SelectedValueToLong() != 0)
			{
				Response.Redirect(UrlBuilder.TalentPool(PoolResult.Tabs.AllResumes, null, SavedSearchDropDown.Items[SavedSearchDropDown.SelectedIndex].Value.ToLong()));
			}
		}

		/// <summary>
		/// Handles the Completed event of the PoolSearchAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveSearchAction_Completed(object sender, EventArgs e)
		{
			BindSavedSearchDropDown();
      if (SavedSearchId > 0)
        SavedSearchDropDown.SelectedValue = SavedSearchId.ToString();
			OnCompleted(e);
		}

		/// <summary>
		/// Handles the Click event of the ClearResumeButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ClearResumeButton_Click(object sender, EventArgs e)
		{
			ClearResume();
		}

		#endregion

		/// <summary>
		/// Loads the search criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void LoadSearchCriteria(CandidateSearchCriteria criteria)
		{
			ClearSearchCriteria();

			#region Job Details Search

			if (criteria.IsNull()) return;

			AdvancedSearchPanelExtender.Collapsed = (criteria.KeywordCriteria != null && criteria.CandidatesLikeThisSearchCriteria == null && criteria.JobDetailsCriteria == null && criteria.ResumeCriteria == null);

			if (criteria.JobDetailsCriteria.IsNotNull())
			{
				if (criteria.JobDetailsCriteria.JobId != 0)
				{
					OpenJobsDropDownList.SelectValue(criteria.JobDetailsCriteria.JobId.ToString(), ManualJobTitleSelectedValue);
					JobIdKnownHidden.Value = "true";
				}
				else if (criteria.JobDetailsCriteria.JobTitle.IsNotNullOrEmpty())
				{
					OpenJobsDropDownList.SelectValue(ManualJobTitleSelectedValue);
					JobTitleTextBox.Text = criteria.JobDetailsCriteria.JobTitle;
				}
			}

			#endregion

			#region Resume Search

			if (criteria.ResumeCriteria.IsNotNull())
			{
				if(criteria.ResumeCriteria.ResumeFilename.IsNotNullOrEmpty())
				{
					ResumeFilename = criteria.ResumeCriteria.ResumeFilename;
					UploadedResumeLabel.Text = string.Format("{0} {1}", CodeLocalise("SampleResumeSearch.FileUploaded.Label", "Basing search on"), ResumeFilename);
				}

				if (criteria.ResumeCriteria.Resume.IsNotNullOrEmpty())
					ResumeContent = criteria.ResumeCriteria.Resume;
			}

			#endregion region

			#region Keyword Search

			if (criteria.KeywordCriteria.IsNotNull())
			{
				if (criteria.KeywordCriteria.Keywords.IsNotNullOrEmpty())
					KeywordsTextBox.Text = criteria.KeywordCriteria.Keywords;

				if (criteria.KeywordCriteria.Context.IsNotNull())
					KeywordContextDropDown.SelectValue(criteria.KeywordCriteria.Context.ToString());

				if (criteria.KeywordCriteria.Scope.IsNotNull())
					KeywordScopeDropDown.SelectValue(criteria.KeywordCriteria.Scope.ToString());
			}

			#endregion

			#region Additonal Search

			if (criteria.AdditionalCriteria.IsNotNull() && criteria.AdditionalCriteria.JobTypesConsidered.IsNotNull())
			{
				JobTypeDropDownList.SelectValue(criteria.AdditionalCriteria.JobTypesConsidered.ToString());
			}

			var additionalCriteriaModel = new ResumeSearchAdditionalCriteria.Model();

			if (criteria.AdditionalCriteria.IsNotNull() || criteria.CandidateId.IsNotNull() || criteria.MinimumScore.IsNotNull())
			{
				additionalCriteriaModel.AdditionalCriteria = (criteria.AdditionalCriteria.IsNotNull()) ? criteria.AdditionalCriteria : new CandidateSearchAdditionalCriteria();
				additionalCriteriaModel.CandidateId = criteria.CandidateId;
				additionalCriteriaModel.MinimumStarRating = criteria.MinimumScore.ToStarRating(App.Settings.StarRatingMinimumScores);
			}
			else
			{
				additionalCriteriaModel.AdditionalCriteria = new CandidateSearchAdditionalCriteria();
			}

			BindAdditionalCriteria(additionalCriteriaModel);	
			
			#endregion

		}

		/// <summary>
		/// Binds the additional criteria.
		/// </summary>
		/// <param name="model">The model.</param>
		private void BindAdditionalCriteria(ResumeSearchAdditionalCriteria.Model model)
		{
			AdditionalCriteria.Bind(model);
		}

		/// <summary>
		/// Unbinds the job details criteria.
		/// </summary>
		/// <returns></returns>
		private CandidateSearchJobDetailsCriteria UnbindJobDetailsCriteria()
		{
			var jobDetailsCriteria = new CandidateSearchJobDetailsCriteria();

			if (OpenJobsDropDownList.SelectedValue == ManualJobTitleSelectedValue)
			{
				jobDetailsCriteria.JobId = 0;
				jobDetailsCriteria.JobTitle = JobTitleTextBox.TextTrimmed();
			}
			else if (OpenJobsDropDownList.SelectedValueToLong() != 0)
			{
				jobDetailsCriteria.JobId = OpenJobsDropDownList.SelectedValueToLong();
				jobDetailsCriteria.JobTitle = OpenJobsDropDownList.SelectedItem.Text;
			}

			return jobDetailsCriteria;
		}

		/// <summary>
		/// Unbinds the resume criteria.
		/// </summary>
		/// <returns></returns>
		private CandidateSearchResumeCriteria UnbindResumeCriteria()
		{
			var resumeCriteria = new CandidateSearchResumeCriteria();

			if (ResumeFilename.IsNotNullOrEmpty())
			{
				resumeCriteria = new CandidateSearchResumeCriteria
				                 	{
				                 		Resume = ResumeContent,
				                 		ResumeFileExtension = Path.GetExtension(ResumeFilename),
				                 		ResumeFilename = ResumeFilename
				                 	};
			}

			return resumeCriteria;
		}

		/// <summary>
		/// Unbinds the keyword search criteria.
		/// </summary>
		/// <returns></returns>
		private CandidateSearchKeywordCriteria UnbindKeywordSearchCriteria()
		{
			#region Keywords

			return (KeywordsTextBox.TextTrimmed().IsNotNullOrEmpty())
			       	? new CandidateSearchKeywordCriteria
			       	  	{
			       	  		Keywords = KeywordsTextBox.TextTrimmed(),
			       	  		Context = KeywordContextDropDown.SelectedValueToEnum<KeywordContexts>(),
			       	  		Scope = KeywordScopeDropDown.SelectedValueToEnum<KeywordScopes>()
			       	  	}
			       	: null;

			#endregion
		}

		/// <summary>
		/// Unbinds the additional criteria.
		/// </summary>
		/// <returns></returns>
		private ResumeSearchAdditionalCriteria.Model UnbindAdditionalCriteria()
		{
			var jobTypesConsidered = (App.Settings.Theme == FocusThemes.Education) ? (JobTypes) JobTypeDropDownList.SelectedValueToFlagEnum<JobTypes>() : JobTypes.All;

			var criteria = AdditionalCriteria.Unbind(jobTypesConsidered);

			if (App.Settings.Theme == FocusThemes.Education)
				criteria.AdditionalCriteria.JobTypesConsidered = jobTypesConsidered;
			
			return criteria;
		}

		#region Clear Search Methods

		/// <summary>
		/// Clears the search criteria.
		/// </summary>
		private void ClearSearchCriteria()
		{
			JobIdKnownHidden.Value = "false";
			OpenJobsDropDownList.SelectedIndex = 0;
			JobTitleTextBox.Text = string.Empty;
			KeywordsTextBox.Text = string.Empty;
			KeywordContextDropDown.SelectedIndex = 0;
			KeywordScopeDropDown.SelectedIndex = 0;
			AdditionalCriteria.Clear();
			ClearResume();
		}

		/// <summary>
		/// Clears the resume.
		/// </summary>
		private void ClearResume()
		{
			ResumeContent = null;
			ResumeFilename = "";
			UploadedResumeLabel.Text = "";
		}

		#endregion

		#region Bind Drop Downs

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
      if (App.Settings.Theme == FocusThemes.Education)
      {
        BindJobTypeDropDownList();
      }
      else
      {
        JobTypeDropDownList.Visible = false;
        SelectOpenInternshipsLabel.Visible = false;
      }

		  EducationScriptPlaceHolder.Visible = (App.Settings.Theme == FocusThemes.Education);

			BindOpenJobDescriptionList();
			BindKeywordContextDropDown();
			BindKeywordScopeDropDown();
			BindSavedSearchDropDown();

			AdditionalCriteria.BindControls();
		}
		
		/// <summary>
		/// Binds the open job description list.
		/// </summary>
		private void BindOpenJobDescriptionList()
		{
      var jobs = ServiceClientLocator.JobClient(App).GetJobs(JobStatuses.Active, App.User.EmployeeId);
		  OpenJobsDropDownList.DataSource = jobs;
			OpenJobsDropDownList.DataValueField = "Id";
			OpenJobsDropDownList.DataTextField = "JobTitle";
			OpenJobsDropDownList.DataBind();

      PoolSearchJobIds.Value = string.Join(",", jobs.Where(j => !j.JobType.IsInternship()).Select(j => j.Id.ToString()));
      PoolSearchInternshipIds.Value = string.Join(",", jobs.Where(j => j.JobType.IsInternship()).Select(j => j.Id.ToString()));

			var defaultOpenJobsItem = new ListItem(CodeLocalise("OpenJobs.TopDefault", "- select open job -"), NoJobTitleSelectedValue);
			OpenJobsDropDownList.Items.Insert(0, defaultOpenJobsItem);

      if (App.Settings.Theme == FocusThemes.Education)
      {
        var defaultOpenInternshipsItem = new ListItem(CodeLocalise("OpenInternships.TopDefault", "- select open internship -"), NoJobTitleSelectedValue);
        OpenJobsDropDownList.Items.Insert(1, defaultOpenInternshipsItem);
      }

      var manualJobsItem = new ListItem(CodeLocalise("OpenJobs.ManualJobTitle.Text", "Enter job title manually"), ManualJobTitleSelectedValue);
      OpenJobsDropDownList.Items.Add(manualJobsItem);

      if (App.Settings.Theme == FocusThemes.Education)
      {
        var manualInternshipsItem = new ListItem(CodeLocalise("OpenInternships.ManualInternshipTitle.Text", "Enter internship title manually"), ManualJobTitleSelectedValue);
        OpenJobsDropDownList.Items.Add(manualInternshipsItem);
      }
    }

		/// <summary>
		/// Binds the keyword context drop down.
		/// </summary>
		private void BindKeywordContextDropDown()
		{
			KeywordContextDropDown.Items.AddEnum(KeywordContexts.FullResume, "Full resume");
			KeywordContextDropDown.Items.AddEnum(KeywordContexts.JobTitle, "Job title");
			KeywordContextDropDown.Items.AddEnum(KeywordContexts.EmployerName, "#BUSINESS# name");
			KeywordContextDropDown.Items.AddEnum(KeywordContexts.Education, "Education");
		}

		/// <summary>
		/// Binds the keyword scope drop down.
		/// </summary>
		private void BindKeywordScopeDropDown()
		{
			KeywordScopeDropDown.Items.AddEnum(KeywordScopes.EntireWorkHistory, "Entire work history");
			KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastJob, "Last job");
			KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastTwoJobs, "Last two jobs");
			KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastThreeJobs, "Last three jobs");
			// Commented out below as it would require a new filter to be apllied in Lens
			//KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastYear, "Last year");
			//KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastThreeYears, "Last three years");
			//KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastFiveYears, "Last five years");
		}

		/// <summary>
		/// Binds the save search drop down.
		/// </summary>
		private void BindSavedSearchDropDown()
		{
			SavedSearchDropDown.DataSource = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearchView();
			SavedSearchDropDown.DataValueField = "Id";
			SavedSearchDropDown.DataTextField = "Name";
			SavedSearchDropDown.DataBind();

      var defaultItem = new ListItem(CodeLocalise("SavedSearch.TopDefault", "- select saved search  -"), string.Empty);
      SavedSearchDropDown.Items.Insert(0, defaultItem);
		}

		/// <summary>
		/// Binds the job type drop down list.
		/// </summary>
		private void BindJobTypeDropDownList()
		{
			JobTypeDropDownList.Items.AddEnum(JobTypes.Job, "jobs");
			JobTypeDropDownList.Items.AddEnum(JobTypes.InternshipPaid | JobTypes.InternshipUnpaid, "internships");
		}

		#endregion

		#region Other Private Methods

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			var goText = CodeLocalise("Global.Go.Text.NoEdit", "Go");
			ClearSearchButton.Text = CodeLocalise("ClearSearch.Text.NoEdit", "Clear search");
            //FVN-6966 AIP Change button text
            if (SavedSearchId > 0)
            {
                SaveSearchButton.Text = CodeLocalise("SaveSearch.Label.NoEdit", "Save this search");
            }
            else
            {
                SaveSearchButton.Text = CodeLocalise("SaveSearch.Label.NoEdit", "Save this search and notify me of new talent");
            }
			KeywordSearchButton.Text = OpenJobButton.Text = SampleResumeButton.Text = SavedSearchButton.Text = goText;
			KeywordRequired.ErrorMessage = CodeLocalise("KeywordRequired.ErrorMessage", "Enter some keywords");
      KeywordsCustomValidator.ErrorMessage = CodeLocalise("KeywordsCustom.ErrorMessage", "Invalid keywords");
			ResumeFileUploadRegularEx.ErrorMessage = CodeLocalise("SampleResumeSearch.RegExErrorMessage", "Invalid resume format");
			ClearResumeButton.Text = CodeLocalise("ClearResume.Text", "Clear resume");
			SavedSearchRequired.ErrorMessage = CodeLocalise("SavedSearch.RequiredError", "Select a saved search");
      JobSearchValidator.ErrorMessage = CodeLocalise("JobSearchValidator.RequiredError", "Select either an open #POSTINGTYPE#:LOWER or enter a #POSTINGTYPE#:LOWER title to search against");
			ApplyAdvancedSearchButton.Text = CodeLocalise("ApplyAdvancedSearchButton.Text", "Apply advanced search");
			SearchLabel.Text = (App.Settings.Theme == FocusThemes.Education) ? CodeLocalise("SearchLabel.Text.Education", "Search for #CANDIDATETYPES#:LOWER looking for") : CodeLocalise("SearchLabel.Text", "Search");
		  SelectOpenJobsLabel.Text = HtmlLocalise("OpenJobSearch.Label", "Search open jobs");
      SelectOpenInternshipsLabel.Text = HtmlLocalise("OpenInternshipSearch.Label", "Search open internships");
      ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");
		}

		/// <summary>
		/// Applies the branding.
		/// </summary>
	  private void ApplyBranding()
	  {
		  AdvancedSearchHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
		  AdvancedSearchPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		  AdvancedSearchPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
	  }

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			ExpandCollapseAllButton.Visible = App.Settings.Theme == FocusThemes.Workforce;
		}

		/// <summary>
		/// Gets the resume search details.
		/// </summary>
		private void GetResumeSearchDetails()
		{
			if (ResumeFileUpload.HasFile)
			{
				ResumeContent = ResumeFileUpload.FileBytes;
				ResumeFilename = ResumeFileUpload.FileName;
				UploadedResumeLabel.Text = string.Format("{0} {1}", CodeLocalise("SampleResumeSearch.FileUploaded.Label", "Basing search on"), ResumeFilename);
			}
		}

		/// <summary>
		/// Registers the javascript.
		/// </summary>
		private void RegisterJavascript()
		{
			// Build the JS
			var js = @"function pageLoad() {
	$(document).ready(function()
	{
		// Apply jQuery to infield labels
		$("".inFieldLabel > label"").inFieldLabels();
	});

	var nestedPanelGroups = []; // an array of any nested panel groups on the page
   	// add our (only) nested panel group, specifying the behaviour ID of its outer panel, and the behaviour IDs of its inner panels
   	var nestedPanelGroup1 = {
   		outerPanel: 'AdvancedSearchPanelBehavior',
   		innerPanels: [
						'LocationCriteriaBehavior',
						'EducationCriteriaBehavior',
						'GradePointAverageCriteriaBehavior',
						'ProgramOfStudyCriteriaBehavior',
						'LanguageCriteriaBehavior',
						'SkillsCriteriaBehavior',
   			'StatusCriteriaBehavior',
   			'MatchScoreCriteriaBehavior',
   			'DrivingLicenceCriteriaBehavior',
   			'LicencesCriteriaBehavior',
   			'CertificationsCriteriaBehavior',
   			'AvailabilityCriteriaBehavior',
   			'CandidateCriteriaBehavior',
        'CareerReadinessCriteriaBehavior'
        " + (App.Settings.SearchByIndustries ? @",
        'IndustriesCriteriaBehavior'" : "") + @"
					]
   	};

   	nestedPanelGroups.push(nestedPanelGroup1); // lob it in the array of nested panel groups
   	bindExpandCollapseAllPanels(""" + ExpandCollapseAllButton.ClientID + @""", nestedPanelGroups); // bind the magic
}

$(document).ready(function()
{
	$(""#" + ResumeFileUpload.ClientID + @""").change(function () {
		var filename = $(""#" + ResumeFileUpload.ClientID + @""").val().replace(/(c:\\)*fakepath\\/i, '');
		$(""#UploadedResumeDiv"").show();
		$(""#" + UploadedResumeLabel.ClientID + @""").text(""" + CodeLocalise("SampleResumeSearch.FileUploaded.Label", "Basing search on") + @" "" + filename);
		$(""#SavedSearchLabelRow"").hide();
		$(""#SavedSearchRow"").hide();
	});

	if($(""#" + UploadedResumeLabel.ClientID + @""").text() != ''){
		$(""#UploadedResumeDiv"").show();
		$(""#SavedSearchLabelRow"").hide();
		$(""#SavedSearchRow"").hide();
	}

	$(""#" + OpenJobsDropDownList.ClientID + @""").change(function () {
		$(""#ManualJobTitleDiv"").hide();
		if ($(""#" + OpenJobsDropDownList.ClientID + @""").val() == """ + ManualJobTitleSelectedValue + @""") {
			$(""#ManualJobTitleDiv"").show();
		}
	});

	if ($(""#" + OpenJobsDropDownList.ClientID + @""").val() == """ + ManualJobTitleSelectedValue + @""") {
		$(""#ManualJobTitleDiv"").show();
	}


	$(""#" + KeywordContextDropDown.ClientID + @""").change(function () {
		if ($(""#" + KeywordContextDropDown.ClientID + @""").val() == """ + KeywordContexts.JobTitle + @"""
				|| $(""#" + KeywordContextDropDown.ClientID + @""").val() == """ + KeywordContexts.EmployerName + @""") {
			$(""#" + KeywordScopeDropDown.ClientID + @""").removeAttr('disabled');
		}
		else {
			$(""#" + KeywordScopeDropDown.ClientID + @""").attr('disabled', 'disabled');
			$(""#" + KeywordScopeDropDown.ClientID + @""").val(""" + KeywordScopes.EntireWorkHistory + @""");
		}	
	});
	
	if ($(""#" + KeywordContextDropDown.ClientID + @""").val() == """ + KeywordContexts.JobTitle + @"""
				|| $(""#" + KeywordContextDropDown.ClientID + @""").val() == """ + KeywordContexts.EmployerName + @""") {
			$(""#" + KeywordScopeDropDown.ClientID + @""").removeAttr('disabled');
		}
		else {
			$(""#" + KeywordScopeDropDown.ClientID + @""").attr('disabled', 'disabled');
			$(""#" + KeywordScopeDropDown.ClientID + @""").val(""" + KeywordScopes.EntireWorkHistory + @""");
		}
		
	// Validation
	$(""#" + KeywordSearchButton.ClientID + @""").click(function() {
		var valid = validateGroup(""PoolSearchKeywordSearch"") && validateGroup(""PoolSearchAdvancedSearch"");
		if(!valid) return false;
		showProcessing();	
	});

	$(""#" + OpenJobButton.ClientID + @""").click(function() {
		var valid = validateGroup(""PoolSearchJobSearch"") && validateGroup(""PoolSearchAdvancedSearch"");
		if(!valid) return false;
		showProcessing();	
	});

	$(""#" + SampleResumeButton.ClientID + @""").click(function() {
		var valid = validateGroup(""PoolSearchResumeSearch"") && validateGroup(""PoolSearchAdvancedSearch"");	
		if(!valid) return false;
		showProcessing();
	});

	$(""#" + SaveSearchButton.ClientID + @""").click(function() {
		return validateGroup(""PoolSearchResumeSearch"") && validateGroup(""PoolSearchAdvancedSearch"");
	});

	$(""#" + ApplyAdvancedSearchButton.ClientID + @""").click(function() {
		var valid = validateGroup(""PoolSearchAdvancedSearch"");
		if(!valid) return false;
		showProcessing();
	});

	$(""#" + SavedSearchButton.ClientID + @""").click(function() {
		var valid = validateGroup(""PoolSavedSearch"");
		if(!valid) return false;
		showProcessing();
	});

	// Apply jQuery to infield labels
	$("".inFieldLabel > label"").inFieldLabels();
		
	$(function() {
		$('.resumefileupload').customFileInput('" + CodeLocalise("Global.Browse.Text", "Browse") + @"', '" + CodeLocalise("Global.Change.Text", "Change") + @"', '" + CodeLocalise("UploadResumeFeedback.Text", "Upload a resume") + @"');
	});
});

function validateGroup(groupName)
{
	var isValid = false;
		try{
			isValid = Page_ClientValidate(groupName);
		} catch(e){}
		return isValid;
}

function validateJobSearch(oSrc, args){
	args.IsValid = ($(""#" + OpenJobsDropDownList.ClientID + @""").val() != """ + NoJobTitleSelectedValue + @"""
										&& (!($(""#" + OpenJobsDropDownList.ClientID + @""").val() == """ + ManualJobTitleSelectedValue + @""" && $(""#" + JobTitleTextBox.ClientID + @""").val() == '')));
}
";
			
			ScriptManager.RegisterClientScriptBlock(this, typeof(PoolSearch), "PoolSearchJavaScript", js, true);
		}

		#endregion

	}
}
