﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PoolResult.ascx.cs" Inherits="Focus.Web.WebTalent.Controls.PoolResult" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="Focus.Core.Views" %>

<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>
<%@ Register src="~/WebTalent/Controls/PoolActionModal.ascx" tagname="PoolActionModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ResumePreviewModal.ascx" tagname="ResumePreviewModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ReferralRequestSentModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/CandidateSearchCriteriaSummary.ascx" tagname="CandidateSearchCriteriaSummary" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/EmailDocumentModal.ascx" tagname="EmailResumeModal" tagprefix="uc" %>

<asp:Panel ID="SearchSummarySectionPanel" runat="server" Visible="false">
	<asp:Panel ID="SearchSummaryHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
		<asp:Image ID="SearchSummaryHeaderImage" runat="server" alt="Search summary image" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" />&nbsp;&nbsp;<%= HtmlLocalise("SearchSummary.Label", "Search summary")%></asp:Panel>
	<asp:Panel ID="SearchSummaryPanel" runat="server">
		<%= HtmlLocalise("SearchSummary.Text", "The following criteria was applied in the search:")%><br />
		<uc:CandidateSearchCriteriaSummary ID="SearchCriteriaSummary" runat="server" />
	</asp:Panel>
	<act:CollapsiblePanelExtender ID="SearchSummaryPanelExtender" runat="server" 
																				TargetControlID="SearchSummaryPanel" 
																				ExpandControlID="SearchSummaryHeaderPanel" 
																				CollapseControlID="SearchSummaryHeaderPanel" 
																				Collapsed="false" 
																				ImageControlID="SearchSummaryHeaderImage" 
																				CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
																				ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																				SuppressPostBack="true" />
</asp:Panel>

<div id="tabbedContentFilters">
	<asp:DropDownList runat="server" ID="SortByDropDown" title="Sort By DropDown" AutoPostBack="True" OnSelectedIndexChanged="SortByDropDown_SelectedIndexChanged" ClientIDMode="Static" />
	<asp:Button runat="server" ID="InviteMultipleButton" CssClass="button3" ClientIDMode="Static" OnClick="InviteMultipleButton_OnClick" />
</div>

<ul class="navTab localNavTab">
	<li id="TabAllResumes" runat="server" ><asp:HyperLink ID="AllResumesLink" runat="server"><%= HtmlLocalise("AllResumesTab.Label.Tab", "All Resumes")%></asp:HyperLink></li>
	<li id="TabApplicants" runat="server" ><asp:HyperLink ID="ApplicantsLink" runat="server"><%= HtmlLocalise("ApplicantsTab.Label.Tab", "Applicants")%></asp:HyperLink></li>
	<li id="TabFlaggedResumes" runat="server" ><asp:HyperLink ID="FlaggedResumesLink" runat="server"><%= HtmlLocalise("FlaggedResumesTab.Label.Tab", "Your flagged resumes")%></asp:HyperLink></li>
	<li id="TabInvitees" runat="server"><asp:HyperLink ID="InviteesLink" runat="server"><%= HtmlLocalise("InviteesTab.Label.Tab", "Invitees") %></asp:HyperLink></li>
</ul>

<div id="tabbedContent">
	<div style="width:100%;">
		<uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="SearchResultListView" PageSize="10" DisplayRecordCount="True" />
	</div>
	<asp:ListView ID="SearchResultListView" DataSourceID="SearchResultDataSource" runat="server" onitemdatabound="SearchResultListView_ItemDataBound"
								DataKeyNames="Id,Name,Status,Score,Flagged,IsVeteran,ApplicationApprovalStatus" ItemPlaceholderID="SearchResultPlaceHolder">
		<LayoutTemplate>
			<table class="genericTable" id="SearchResultsTable" role="presentation">
				<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
			</table>
		</LayoutTemplate>
		<ItemTemplate>
			<tr class="topRow">
				<td class="column1"><span><asp:image ID="SearchResultScoreImage" runat="server" AlternateText="Search result score" Width="80" Height="16"/></span></td>
				<td class="column2" data-cell="CandidateName">
					<span style="white-space:nowrap">
						<asp:LinkButton ID="SearchResultNameLink" runat="server" OnCommand="SearchResultRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="Preview" />
						<asp:image ID="VeteranImage" runat="server" Visible="false" Height="15" Width="17" AlternateText="Veteran"/>
						<asp:Image runat="server" ID="NcrcImage" CssClass="tooltipImage toolTipNew ncrcListIcon" Visible="False" AlternateText="Ncrc Image"/>
                        <br/>
                        <asp:Literal ID="SearchResultInviteesQueuedStatusLiteral" runat="server" Visible="false" />
					</span>
				</td>
				<td class="column2"><span><div data-cell="CandidateBranding"><asp:Label ID="SearchResultBrandingLabel" runat="server" /></div><div><asp:Literal ID="SearchResultEmploymentHistoryLiteral" runat="server" /></div></span></td>
				<td class="column2" data-cell="CandidateLocation"><span id="LocationSpan" runat="server"><%# ((ResumeView)Container.DataItem).CandidateLocation %></span></td>
				<td class="column2" data-cell="CandidateExperience"><span id="YearsExperienceSpan" runat="server"><%# HtmlLocalise("SearchResultList.Experience.Text", "{0} years", ((ResumeView)Container.DataItem).YearsExperience)%></span></td>
				<td class="column2" data-cell="CandidateInvitedOn"><span id="InviteCreatedOnSpan" runat="server" Visible="False"><%#HtmlLocalise("SearchResultList.InviteCreatedOn.Text", "{0}", ((ResumeView)Container.DataItem).DateInvitedCreatedOn) %></span></td>
				<td class="column3">
					<span>
						<asp:DropDownList runat="server" ID="SearchResultApplicationStatusDropDown" title="Search Result Application Status DropDown" Visible="false" OnSelectedIndexChanged="SearchResultApplicationStatusDropDown_SelectedIndexChanged" AutoPostBack="true"/>
						<asp:literal ID="SearchResultApplcationStatusLiteral" runat="server" Visible="False"/>
					</span>
				</td>
				<%--<td class="column3" id="SearchResultApplcationStatusLiteralCell" Visible="false"  runat="server"><span></span></td>--%>
			</tr>
			<tr class="bottomRow">
				<td class="column2" colspan="7" id="BottomRowCell" runat="server">
					<span>
						<table role="presentation" style="width:100%">
							<tr>
								<td>
									<div id="EmailDiv" runat="server" class="tooltip left Email">
										<asp:LinkButton ID="SearchResultEmailButton" runat="server" OnCommand="SearchResultRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="Email" aria-label="Email" >
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Email.ToolTip", "Email")%>"></div>
										</asp:LinkButton>
									</div>
									<div id="FlagDiv" runat="server" class="tooltip left Flag">
										<asp:LinkButton ID="SearchResultFlagButton"  runat="server" OnCommand="SearchResultRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="Flag" aria-label="Flag">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Flag.ToolTip", "Toggle Flag")%>"></div>
                                             <asp:Label ID="SearchResultFlagLabel"   text="Search Result Flag" runat="server" CssClass="sr-only" />
										</asp:LinkButton>
									</div>
									<div id="NoteDiv" runat="server" class="tooltip left Note">
										<asp:LinkButton ID="SearchResultNoteLinkButton" runat="server" OnCommand="SearchResultRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="Note" aria-label="Note">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Note.ToolTip", "Add/Edit Note")%>"></div>
										</asp:LinkButton>
									</div>
									<div class="tooltip left MoreCandidatesLikeThis">
										<asp:LinkButton ID="SearchResultCandidatesLikeThisButton" runat="server" OnCommand="SearchResultRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="CandidatesLikeThis" aria-label="Candidates Like This">
											<div class="tooltipImage toolTipNew" title="<%# HtmlLocalise("CandidatesLikeThis.ToolTip", "Candidates Like This")%>"></div>
										</asp:LinkButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<asp:panel ID="NotePanel" runat="server" Visible="false" Width="100%" >
										<br/>
										<table style="width:100%">
											<tr>
												<td><asp:TextBox ID="NotesTextBox" Width="99%" MaxLength="1000" runat="server" /></td>
												<td style="width:20px" style="align:center">
														<asp:LinkButton ID="SaveNoteLinkButton" runat="server" OnCommand="SearchResultRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="SaveNote">
															<img src="<%= UrlBuilder.SaveIcon() %>" alt="<%= HtmlLocalise("SaveNote.Text", "Save note") %>" style="width:16px" />
														</asp:LinkButton>
												</td>
												<td style="width:20px">
													<asp:LinkButton ID="CloseNoteLinkButton" runat="server" OnCommand="SearchResultRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="CloseNote">
														<img src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" />
													</asp:LinkButton>
												</td>
											</tr>
										</table>
										<br/>
									</asp:panel>
								</td>
							</tr>
						</table>
					</span>
				</td>
			</tr>
		</ItemTemplate>
		<EmptyDataTemplate>
			<p>
				<b><%= Request.Url.PathAndQuery.Contains("invitees") ? HtmlLocalise("NoInvitees.Text", "No invited job seekers found") : HtmlLocalise("NoResults.Text", "No matching candidates found")%></b>
			</p>
		</EmptyDataTemplate>
	</asp:ListView>

	<asp:Panel ID="InstructionsPanel" runat="server">
		<p>
			<i><%= HtmlLocalise("Instructions.Text", "Search resumes in #FOCUSTALENT# with any of the following options. Your results will appear below.")%></i>
		</p>
		<a style="cursor:pointer;" class="button7">
			<strong><%= HtmlLocalise("KeywordSearchButton.Header", "Keyword and advanced search") %></strong>&nbsp; 
			<%= HtmlLocalise("KeywordSearchButton.Text", "search hundreds of criteria to narrow in on candidates")%>
		</a> 
		<a style="cursor:pointer;" class="button7">
			<strong><%= HtmlLocalise("OpenJobSearchButton.Header", "Search based on your open jobs")%></strong>&nbsp; 
			<%= HtmlLocalise("OpenJobSearchButton.Text", "#FOCUSTALENT# will suggest people for your job")%>
		</a> 
		<a style="cursor:pointer;" class="button7">
			<strong><%= HtmlLocalise("ResumeSearchButton.Header", "Search based on a sample resume")%></strong>&nbsp; 
			<%= HtmlLocalise("ResumeSearchButton.Text", "Lose someone fabulous? Describe that individual and let #FOCUSTALENT# find your next fabulous employee.")%>
		</a> 
		<a style="cursor:pointer;" class="button7">
			<strong><%= HtmlLocalise("SavedSearchButton.Header", "Search based on a saved search")%></strong>&nbsp; 
			<%= HtmlLocalise("SavedSearchButton.Text", "Find people who match a previously saved search")%>
		</a>	
	</asp:Panel>
		
	<asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.WebTalent.Controls.PoolResult" EnablePaging="true" SelectMethod="GetCandidates" SelectCountMethod="GetCandidateCount" 
												SortParameterName="orderBy" OnSelected="SearchResultDataSource_Selected">
	</asp:ObjectDataSource>
</div>
			
<asp:HiddenField ID="CandidatesLikeThisModalDummyTarget" runat="server" />

<act:ModalPopupExtender ID="CandidatesLikeThisModalPopup" runat="server" ClientIDMode="Static"
											TargetControlID="CandidatesLikeThisModalDummyTarget"
											PopupControlID="CandidatesLikeThisModal" 
											PopupDragHandleControlID="CandidatesLikeThisModalHeader"
											RepositionMode="RepositionOnWindowResizeAndScroll"
											BackgroundCssClass="modalBackground" />

<asp:panel ID="CandidatesLikeThisModal" runat="server" CssClass="modal" Style="display:none; width:80%" >
	<div style="width:100%" class="modal-header" id="CandidatesLikeThisModalHeader">
		<div class="right">
			<asp:ImageButton ID="CandidatesLikeThisModalCloseButton" runat="server" ImageUrl="<%# UrlBuilder.ButtonCloseIcon() %>" OnClick="CandidatesLikeThisModalCloseButton_Click"/>
		</div>
		<div class="left">
			<strong><%= HtmlLocalise("CandidatesLikeThisModal.Title", "More candidates like")%></strong>
		</div>
	</div>
		<asp:ListView ID="CandidatesLikeThisListView" DataSourceID="CandidatesLikeThisDataSource" runat="server" onitemdatabound="CandidatesLikeThisListView_ItemDataBound"
								DataKeyNames="Name,Status,Id" ItemPlaceholderID="CandidatesLikeThisPlaceHolder">
		<LayoutTemplate>
			<table class="genericTable">
				<asp:PlaceHolder ID="CandidatesLikeThisPlaceHolder" runat="server" />
			</table>
		</LayoutTemplate>
		<ItemTemplate>
			<asp:TableRow ID="CandidatesLikeThisItemTopTableRow" CssClass="topRow" runat="server">
				<asp:TableCell runat="server" EnableViewState="false" CssClass="column1"><span><asp:image ID="CandidatesLikeThisScoreImage" runat="server" AlternateText="Candidates Like This Score Image" /></span></asp:TableCell>
				<asp:TableCell runat="server" EnableViewState="false" CssClass="column2">
					<span>
						<asp:LinkButton ID="CandidatesLikeThisNameLink" runat="server" />&nbsp;
						<asp:image ID="CandidatesLikeThisVeteranImage" runat="server" Visible="false" AlternateText="Candidates Like This Veteran Image" />
						<asp:Image runat="server" ID="CandidatesLikeThisNcrcImage" CssClass="tooltipImage toolTipNew ncrcListIcon" Visible="False" AlternateText="Candidates Like This Ncrc Image"/>
					</span>
				</asp:TableCell>
        <asp:TableCell runat="server" EnableViewState="false" CssClass="column2">
          <span>
						<div><asp:Literal ID="CandidatesLikeThisBrandingLiteral" runat="server" /></div>
						<div><asp:Literal ID="CandidatesLikeThisEmploymentHistoryLiteral" runat="server" /></div>
					</span>
        </asp:TableCell>
				<asp:TableCell runat="server" EnableViewState="false" CssClass="column2"><span id="CandidatesLikeThisLocationSpan" runat="server" Visible="False"><%# ((ResumeView)Container.DataItem).CandidateLocation %></span></asp:TableCell>
        <asp:TableCell runat="server" EnableViewState="false" CssClass="column2"><span id="CandidatesLikeThisYearsExperienceSpan" runat="server"><%# HtmlLocalise("CandidateList.Experience.Text", "{0} years", ((ResumeView)Container.DataItem).YearsExperience)%></span></asp:TableCell><asp:TableCell runat="server" EnableViewState="false" CssClass="column3">
					<span>
						<asp:DropDownList runat="server" ID="CandidatesLikeThisApplicationStatusDropDownList" Visible="false" />
					</span>
				</asp:TableCell></asp:TableRow><asp:TableRow ID="CandidatesLikeThisItemBottomTableRow" CssClass="bottomRow" runat="server">
				<asp:TableCell runat="server" CssClass="column2" colspan="6">
					<span>
						<table style="width:100%">
							<tr>
								<td>
									<div id="CandidatesLikeThisEmailDiv" runat="server" class="tooltip left Email">
										<asp:LinkButton ID="CandidatesLikeThisEmailButton" runat="server" OnCommand="CandidatesLikeThisRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="Email" aria-label="Email">
											<div class="tooltipImage toolTipNew" title="<%# HtmlLocalise("Email.ToolTip", "Email")%>"></div>
										</asp:LinkButton>
									</div>
									<div id="CandidatesLikeThisFlagDiv" runat="server" class="tooltip left Flag">
										<asp:LinkButton ID="CandidatesLikeThisFlagButton" runat="server" OnCommand="CandidatesLikeThisRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="Flag" aria-label="Flag">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Flag.ToolTip", "Toggle Flag")%>"></div>
										</asp:LinkButton>
									</div>
									<div id="CandidatesLikeThisNoteDiv" runat="server" class="tooltip left Note">
										<asp:LinkButton ID="CandidatesLikeThisNoteButton" runat="server" OnCommand="CandidatesLikeThisRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="Note" aria-label="Note">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Note.ToolTip", "Add/Edit Note")%>"></div>
										</asp:LinkButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<asp:panel ID="CandidatesLikeThisNotePanel" runat="server" Visible="false" Width="100%" >
										<br/>
										<table style="width:100%">
											<tr>
												<td><asp:TextBox ID="CandidatesLikeThisNotesTextBox" Width="99%" MaxLength="399" runat="server" /></td>
												<td style="width:20px" style="align:center">
														<asp:LinkButton ID="CandidatesLikeThisSaveNoteLinkButton" runat="server" OnCommand="CandidatesLikeThisRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="SaveNote">
															<img src="<%= UrlBuilder.SaveIcon() %>" alt="<%= HtmlLocalise("SaveNote.Text", "Save note") %>" style="width:16px" />
														</asp:LinkButton>
												</td>
												<td style="width:20px">
													<asp:LinkButton ID="CandidatesLikeThisCloseNoteLinkButton" runat="server" OnCommand="CandidatesLikeThisRowAction_Command"
																				CommandArgument="<%# Container.DisplayIndex %>" CommandName="CloseNote">
														<img src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" />
													</asp:LinkButton>
												</td>
											</tr>
										</table>
										<br/>
									</asp:panel>
								</td>
							</tr>
						</table>
					</span>
				</asp:TableCell>
			</asp:TableRow>
			<tr id="ResumeViewRow">
				<td colspan="6">
					<div class="ResumeDiv" style="display:none; max-height:250px; overflow:auto; overflow-x:hidden; margin-bottom:10px;"></div>
				</td>
			</tr>
		</ItemTemplate>
		<EmptyDataTemplate>
			<%= HtmlLocalise("CandidatesLikeThisNoResults.Text", "No matching candidates can be found.") %>
		</EmptyDataTemplate>
	</asp:ListView>
	<asp:ObjectDataSource ID="CandidatesLikeThisDataSource" runat="server" TypeName="Focus.Web.WebTalent.Controls.PoolResult" EnablePaging="true" SelectMethod="GetCandidatesLikeThis"
												SelectCountMethod="GetCandidatesLikeThisCount" SortParameterName="orderBy" OnSelected="CandidatesLikeThisDataSource_Selected" />
</asp:panel>

<uc:PoolActionModal ID="PoolAction" runat="server" OnCompleted="PoolAction_Completed" />
<uc:ResumePreviewModal ID="ResumePreview" runat="server" OnResumePreviewEmailResumeCandidateClick="ResumePreviewModalEmailResumeCandidate_Click" OnResumePreviewEmailCandidateClick="ResumePreviewModalEmailCandidate_Click" OnResumePreviewCandidatesLikeMeClick="ResumePreviewCandidatesLikeMe_Click" 
					OnResumePreviewFlagToggled="ResumePreviewFlag_Toggled" OnResumePreviewNoteSaved="ResumePreviewNote_Saved" />
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
<uc:ReferralRequestSentModal ID="ReferralRequestSent" runat="server" Width="300px" />
<uc:EmailResumeModal ID="EmailResume" runat="server"/>
	
<script type="text/javascript">
	
	var prm = Sys.WebForms.PageRequestManager.getInstance();
	prm.add_pageLoaded(poolResultUpdated);

	function poolResultUpdated(sender, args) {
	  $(document).ready(poolResultAdjustColumns);
	  $(window).resize(poolResultAdjustColumns);
	}

	function poolResultAdjustColumns() {
	  //			// Small icon tooltips
	  //			$(".tooltip").hover(function () {
	  //				// Show the tooltip
	  //				$(this).children(".tooltipPopup").css("display", "block");

	  //				// Position it horizontally half of the popup's width and half of the width of the icon
	  //				tooltipPopupLocation = ($(this).children(".tooltipPopup").width() / 2) - 13;
	  //				$('.tooltipPopupInner', this).css("right", tooltipPopupLocation);
	  //			},
	  //				function () { $(this).children(".tooltipPopup").css("display", "none"); }
	  //			);

	  //			// Small icon with arrow tooltips
	  //			$(".tooltipWithArrow").hover(function () {
	  //				// Show the tooltip
	  //				$(this).children(".tooltipPopup").css("display", "block");

	  //				// Position it horizontally half of the popup's width and half of the width of the icon
	  //				tooltipPopupLocation = ($(this).children(".tooltipPopup").height() / 2) + 5;
	  //				$('.tooltipPopupInner', this).css("bottom", tooltipPopupLocation);
	  //				arrowLocation = ($(this).children(".tooltipPopup").height() / 2) - 13;
	  //				$('.arrow', this).css("top", arrowLocation);
	  //			},
	  //				function () { $(this).children(".tooltipPopup").css("display", "none"); }
	  //			);

	  // check for any overflowing text in the result table cells, and reset heights if necessary - see FVN562
	  var resultCellsHeightsArray;
	  var resultCellsContent;
	  $('.genericTable .topRow, .genericTable .topRowHighlighted').each(function () {
	    resultCellsHeightsArray = [];
	    resultCellsContent = $(this).find('> td > span');
	    $(resultCellsContent).each(function () {
	      if ($(this).outerHeight() < $(this).prop('scrollHeight')) {
	        $(this).height('auto');
	        resultCellsHeightsArray.push($(this).height());
	      }
	    });
	    if (resultCellsHeightsArray.length > 0) {
	      $(resultCellsContent).height(Math.max.apply(Math, resultCellsHeightsArray));
	    }
	  });
	}

	$('.button7').click(function ()
	{
		$find('AdvancedSearchPanelBehavior')._doOpen();
		$('.advancedSearchAccordionTitle').addClass('on');
	});
	
	    Sys.Application.add_load(MyAccount_PageLoad);
    function MyAccount_PageLoad(sender, args) {

        // Jaws Compatibility
				var modal = $find('CandidatesLikeThisModalPopup');

				if (modal != null) {
	    		modal.add_shown(function () {
	    			$('.modal').attr('aria-hidden', false);
	    			$('.page').attr('aria-hidden', true);
	    		});
	    		modal.add_hiding(function () {
	    			$('.modal').attr('aria-hidden', true);
	    			$('.page').attr('aria-hidden', false);
	    		});
	    }
	    
    }

	function ResumeLinkButtonClicked(lnk, personId) {
		var resumeDiv = $("#" + lnk.id).parents('tr').nextUntil(".topRow", "#ResumeViewRow").find(".ResumeDiv");

		if(resumeDiv.is(':hidden')) {
			$(".ResumeDiv").hide();
		}

		if (resumeDiv.html() == "") {
			GetResumeText(resumeDiv, personId);
			resumeDiv.html('<%= HtmlLocalise("FetchingResume.Text", "Fetching Resume...") %>');
		}

		resumeDiv.toggle();

		return false;
	}

	function GetResumeText(resumeDiv, personId) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			url: "<%= UrlBuilder.AjaxService() %>/GetResumeText",
			dataType: "json",
			data: '{"personId":' + personId + '}',
			success: function (result) {
				resumeDiv.html(result.d);
			},
			error: function () {
				resumeDiv.html('<%= HtmlLocalise("NoResumeFound.Text", "No Resume Found.") %>');
			}
		});
	}
	
</script>
