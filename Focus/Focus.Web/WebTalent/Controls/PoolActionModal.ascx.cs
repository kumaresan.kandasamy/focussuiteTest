﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Web.Code;
using Framework.Core;

using Focus.Core;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.ServiceClients.Interfaces;

#endregion

namespace Focus.Web.WebTalent.Controls
{
	public partial class PoolActionModal : UserControlBase
	{

		#region Event handlers

		public delegate void CompletedHandler(object sender, EventArgs eventArgs);

		public event CompletedHandler Completed;

		#endregion

		#region Page properties

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>The title.</value>
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the person id.
		/// </summary>
		/// <value>The person id.</value>
		protected long PersonId
		{
			get { return GetViewStateValue<long>("PoolActionModal:PersonId"); }
			set { SetViewStateValue("PoolActionModal:PersonId", value); }
		}

		/// <summary>
		/// Gets or sets the name of the candidate.
		/// </summary>
		/// <value>The name of the candidate.</value>
		protected string CandidateName
		{
			get { return GetViewStateValue<string>("PoolActionModal:CandidateName"); }
			set { SetViewStateValue("PoolActionModal:CandidateName", value); }
		}

		/// <summary>
		/// Gets or sets the lens posting id.
		/// </summary>
		/// <value>
		/// The lens posting id.
		/// </value>
		protected string LensPostingId
		{
			get { return GetViewStateValue<string>("PoolActionModal:LensPostingId"); }
			set { SetViewStateValue("PoolActionModal:LensPostingId", value); }
		}

		/// <summary>
		/// Gets or sets the candidate status id.
		/// </summary>
		/// <value>The candidate status id.</value>
		protected ApplicationStatusTypes ApplicationStatus
		{
			get { return GetViewStateValue<ApplicationStatusTypes>("PoolActionModal:ApplicationStatus"); }
			set { SetViewStateValue("PoolActionModal:ApplicationStatus", value); }
		}

		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		/// <value>The job id.</value>
		protected long JobId
		{
			get { return GetViewStateValue<long>("JobActionModal:JobId"); }
			set { SetViewStateValue("JobActionModal:JobId", value); }
		}

		/// <summary>
		/// Gets or sets the matching score (if applicable)
		/// </summary>
		/// <value>The person id.</value>
		protected int Score
		{
			get { return GetViewStateValue<int>("PoolActionModal:Score"); }
			set { SetViewStateValue("PoolActionModal:Score", value); }
		}

		/// <summary>
		/// Gets or sets the type of the action.
		/// </summary>
		/// <value>The type of the action.</value>
		protected ActionTypes ActionType
		{
			get { return GetViewStateValue<ActionTypes>("PoolActionModal:ActionType"); }
			set { SetViewStateValue("PoolActionModal:ActionType", value); }
		}

		/// <summary>
		/// Gets or sets the _email template.
		/// </summary>
		/// <value>The _email template.</value>
		private EmailTemplateView EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>("PoolActionModal:EmailTemplate"); }
			set { SetViewStateValue("PoolActionModal:EmailTemplate", value); }
		}

		/// <summary>
		/// Gets or sets the search id.
		/// </summary>
		/// <value>
		/// The search session id.
		/// </value>
		private Guid SearchId
		{
			get { return GetViewStateValue<Guid>("PoolActionModal:SearchId"); }
			set { SetViewStateValue("PoolActionModal:SearchId", value); }
		}

		/// <summary>
		/// Gets or sets the job view.
		/// </summary>
		/// <value>
		/// The job view.
		/// </value>
		private JobViewDto JobView
		{
			get { return GetViewStateValue<JobViewDto>("PoolActionModal:JobView"); }
			set { SetViewStateValue("PoolActionModal:JobView", value); }
		}

    /// <summary>
    /// Gets or sets the mode which indicates the modal display 
    /// </summary>
    /// <value>
    /// Whether a job has been specified or if a job is specified and the application status is not 'Not Applicable'
    /// </value>
    private bool JobSpecifiedMode
    {
      get { return GetViewStateValue<bool>("PoolActionModal:JobSpecifiedMode"); }
      set { SetViewStateValue("PoolActionModal:JobSpecifiedMode", value); }
    }

    /// <summary>
    /// Gets or sets whether this is a Contact Request or Custom Message Message Type
    /// </summary>
    /// <value>
    /// Whether this is a Contact Request Message
    /// </value>
    private bool ContactRequest
    {
      get { return GetViewStateValue<bool>("PoolActionModal:ContactRequest"); }
      set { SetViewStateValue("PoolActionModal:ContactRequest", value); }
    }

    /// <summary>
    /// Gets or sets whether the candidate is a veteran
    /// </summary>
    /// <value>
    /// Whether the candidate is a veteran
    /// </value>
    private bool IsVeteran
    {
      get { return GetViewStateValue<bool>("PoolActionModal:IsVeteran"); }
      set { SetViewStateValue("PoolActionModal:IsVeteran", value); }
    }

		private bool JobsSeekerContactInfo { get; set; }

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			EmailBodyRequired.ErrorMessage = CodeLocalise("EmailBody.RequiredErrorMessage", "Email text is required.");
			EmailSubjectRequired.ErrorMessage = CodeLocalise("EmailSubject.RequiredErrorMessage", "Email subject is required.");
      JobTitleDropDownLiteral.Text = HtmlRequiredLabel(JobTitleDropDown, "JobTitle.Label", "Job title");

			if (!IsPostBack)
			{
				ContactMethodRow.Style.Add(HtmlTextWriterStyle.Visibility, "hidden");

				
			}
		}

		/// <summary>
		/// Shows the specified action type.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="searchId">The search id.</param>
		/// <param name="jobId">The job identifier.</param>
		public void Show(ActionTypes actionType, Guid searchId, long jobId)
		{
			ActionType = actionType;
			SearchId = searchId;
			JobId = jobId;

			switch (ActionType)
			{
				case ActionTypes.EmailCandidates:
					Title = CodeLocalise("EmailCandidatesTitle.Label", "Send Multiple Invites");
					OkButton.Text = CodeLocalise("EmailCandidatesOkButton.Label", "Send email");
					CloseButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");

					JobSpecifiedControls.Visible = false;
					JobNotSpecifiedControls.Visible = false;
					EmailCandidateInstructionsPanel.Visible = false;
					EmailCandidatesControlsPanel.Visible = true;

					break;

				default:
					throw new Exception("Invalid action type");
			}

			Bind();

			ModalPopup.Show();
		}

		/// <summary>
		/// Shows the specified action type.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="candidateName">Name of the candidate.</param>
		/// <param name="applicationStatus">The application status.</param>
		/// <param name="candidate">The candidate.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="activeJobs">Whether the current user has active jobs</param>
		/// <exception cref="System.Exception">Invalid action type</exception>
		public void Show(ActionTypes actionType, long personId, string candidateName, ApplicationStatusTypes applicationStatus,
		  ResumeView candidate, long jobId = 0, bool activeJobs = false)
		{
			ActionType = actionType;
			PersonId = personId;
			CandidateName = candidateName;
			ApplicationStatus = applicationStatus;
			JobId = jobId;
			LensPostingId = null;
			if (candidate.IsNotNull())
			{
				Score = candidate.Score;
			  IsVeteran = candidate.IsVeteran;
			}
			EmailCandidatesControlsPanel.Visible = false;

		  if (ActionType == ActionTypes.EmailCandidate)
		  {
		    Title = CodeLocalise("EmailCandidateTitle.Label", "Contact #CANDIDATETYPE#:LOWER");
		    OkButton.Text = CodeLocalise("EmailCandidateOkButton.Label", "Send email");
		    CloseButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");

		    if (JobId == 0 || (JobId > 0 && ApplicationStatus == ApplicationStatusTypes.NotApplicable))
		      ShowJobNotSpecifiedControls(activeJobs);
		    else
		      ShowJobSpecifiedControls();

		    EmailCandidateInstructionsPanel.Visible = true;

		    if (App.Settings.ShowPersonalEmail && candidate.ContactInfoVisible)
		    {
		      JobsSeekerContactInfo = true;
		      EmailCandidateSummary.Text = CodeLocalise("EmailCandidate.Summary", "Contact #CANDIDATETYPE#:LOWER directly:");
		      ContactDetails.Visible = true;
		      }
		    else
		    {
		      ContactDetails.Visible = false;
		      EmailCandidateSummary.Text = CodeLocalise("EmailCandidate.Summary",
		        "Contact #CANDIDATETYPE#:LOWER directly by confidential email");
		    }
		  }
      else
      {
		    throw new Exception("Invalid action type");
		  }

			Bind();

			ModalPopup.Show();
		}

		/// <summary>
		/// Handles the Click event of the OkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OkButton_Click(object sender, EventArgs e)
		{
      var jobClient = ServiceClientLocator.JobClient(App);
      var jobId = JobId != 0 ? JobId : JobTitleDropDown.SelectedValueToLong();

			switch (ActionType)
			{
				case ActionTypes.EmailCandidate:

          var messageType = (JobId == 0 && App.Settings.Theme == FocusThemes.Education)
            ? JobNotSpecifiedMessageTypeDropDown.SelectedValueToEnum<EmailTemplateTypes>()
			      : MessageTypeDropDown.SelectedValueToEnum<EmailTemplateTypes>();

			    var isExistingApplicant = (JobId == 0 && App.Settings.Theme == FocusThemes.Education)
			      ? new ExistingApplicant()
			      : ServiceClientLocator.CandidateClient(App).IsCandidateExistingJobInviteeOrApplicant(JobId != 0 ? JobId : JobTitleDropDown.SelectedValueToLong(), PersonId);

					if (isExistingApplicant.Exists && isExistingApplicant.IsApplicationPendingApproval)
					{
						var application = ServiceClientLocator.CandidateClient(App).GetApplication(PersonId, JobId != 0 ? JobId : JobTitleDropDown.SelectedValueToLong());
						var message = (application.IsNotNull() && application.Id.HasValue) 
							? ServiceClientLocator.CandidateClient(App).ApproveCandidateReferral(application.Id.Value, application.LockVersion, true)
							: string.Empty;
						if (message.Length == 0)
						{
							var candidateName = (ApplicationStatus != ApplicationStatusTypes.NotApplicable &&
								                    ApplicationStatus != ApplicationStatusTypes.SelfReferred &&
								                    CandidateName.IsNotNullOrEmpty() && JobId != 0)
								? CandidateName
								: CodeLocalise("TheJobSeeker.Text", "the job seeker");

							Confirmation.Show(CodeLocalise("PendingApplicationApproved.Title", "Pending application approved"),
								CodeLocalise("PendingApplicationApproved.Body", "An application has already been received from " + candidateName + " and was waiting to be approved.  This application has now been approved automatically due to your interest."),
								CodeLocalise("Global.Close.Text", "Close"));
						}
						else
						{
							Confirmation.Show(CodeLocalise("ReferralApprovedInvalid.Title", "Referral failed"),
								CodeLocalise("ReferralApprovedInvalid.Body", string.Format("There was a problem approving the referral.<br /><br />{0}", message)),
								CodeLocalise("CloseModal.Text", "OK"));
						}
					}
					else if (isExistingApplicant.Exists && (messageType.IsIn(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer) || ApplicationStatus == ApplicationStatusTypes.NotApplicable))
					{
						MasterPage.ShowError(CodeLocalise("InviteErrorModal.ErrorMessage", "This job seeker is already an invitee or applicant to this job"));
					}
					else if (ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(PersonId, true) == 0)
					{
					  MasterPage.ShowError(CodeLocalise("InviteErrorModal.NoResumeMessage",
					    "This job seeker currently does not have a completed resume"));
					}
					else
					{					  
            // Job available only to veterans for the life of posting
            if (JobView.ExtendVeteranPriority.IsNotNull() && 
              (!IsVeteran && (JobView.ExtendVeteranPriority.Value.Equals(ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting))))
					  {
              Confirmation.Show(CodeLocalise("ReferralVeteranPosting.Title", "Posting exclusive to veterans"),
                CodeLocalise("ReferralVeteranPosting.Body",
                  "This job is set to exclude non-veteran invitations or recommendations for the lifetime of the posting. You cannot invite or recommend that this job seeker apply."),
                  CodeLocalise("CloseModal.Text", "OK"));
					  }
					  // Allow non veterans to be invitied to apply for a Veteran Priority job however delay the email until after the Veteran Priority End Date
            else if (JobView.ExtendVeteranPriority.IsNotNull() &&
                     (!IsVeteran && JobView.ExtendVeteranPriority.Value.Equals(ExtendVeteranPriorityOfServiceTypes.ExtendUntil) &&
                      JobView.VeteranPriorityEndDate > DateTime.Now) ||
                     (!IsVeteran && JobView.VeteranPriorityEndDate > DateTime.Now))
					  {
					    jobClient.SaveInviteToApply(LensPostingId, jobId, PersonId, Score, true);

					    Confirmation.Show(CodeLocalise("ReferralVeteranPosting.Title", "Posting exclusive to veterans"),
					      CodeLocalise("ReferralVeteranPosting.Body",
					        "This posting is currently only available to veterans to apply. " +
					        "Your invite will be saved and sent automatically when the exclusivity period is over. " +
					        "No further action is required."),
					      CodeLocalise("CloseModal.Text", "OK"));
					  }					  
					  else
					  {
					    var body = (messageType == EmailTemplateTypes.CustomMessage)
					      ? EmailBodyTextBox.TextTrimmed()
					      : EmailTemplate.Body;

					    ServiceClientLocator.CandidateClient(App)
					      .SendCandidateEmail(PersonId, EmailTemplate.Subject, body, true,
					        jobId: JobId != 0 ? JobId : JobTitleDropDown.SelectedValueToLong(),
					        isInviteToApply:
					          (ApplicationStatus == ApplicationStatusTypes.NotApplicable) &&
					          (!messageType.IsIn(EmailTemplateTypes.CustomMessage)),
					        candidateName: CandidateName.Trim(),
					        senderAddress: ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(messageType),
					        ContactRequest: ContactRequest);

					    // Save invite if we have a lens posting and this is an invite to apply
					    if (LensPostingId.IsNotNullOrEmpty() && ApplicationStatus == ApplicationStatusTypes.NotApplicable)
					    {
					      if (Score == 0)
					      {
					        var defaultResumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(PersonId, true);
					        Score = ServiceClientLocator.OrganizationClient(App).GetMatchScore(defaultResumeId, LensPostingId);
					      }

					      jobClient.SaveInviteToApply(LensPostingId, jobId, PersonId, Score, false);
					      jobClient.ResolveJobIssues(jobId, new List<JobIssuesFilter> {JobIssuesFilter.SearchNotInviting}, null, true);
					    }

					    var candidateName = (ApplicationStatus != ApplicationStatusTypes.NotApplicable &&
					                         ApplicationStatus != ApplicationStatusTypes.SelfReferred &&
					                         CandidateName.IsNotNullOrEmpty() && JobId != 0)
					      ? CandidateName
					      : CodeLocalise("TheJobSeeker.Text", "the #CANDIDATETYPE#:LOWER");

					    Confirmation.Show(CodeLocalise("EmailCandidateConfirmation.Title", "Contact #CANDIDATETYPE#"),
					      CodeLocalise("EmailCandidateConfirmation.Body", "Thank you. We have sent an email to {0} on your behalf.",
					        candidateName),
					      CodeLocalise("Global.Close.Text", "Close"));
					  }
					}
			    break;

				case ActionTypes.EmailCandidates:

					var starRating = StarMatchDropdown.SelectedValueToInt(5);
					var allResumes = ServiceClientLocator.SearchClient(App).GetResumeSearchSession(SearchId, starRating);
			    bool inviteSent = false;

          // No action if no star matches
			    if (allResumes.IsNullOrEmpty())
			    {
			      MultipleInvitesValidationLabel.Visible = true;
			      ModalPopup.Show();
			      return;
			    }

          // Count the number of non veteran candidates
          var nonVetCount = allResumes.Count(x => !x.IsVeteran);

          // If this is a non veteran job, veteran exclusivity has expired or all selected jobseekers are veterans email all selected candidates
					if ((DateTime.Now > JobView.VeteranPriorityEndDate.GetValueOrDefault() && JobView.ExtendVeteranPriority.GetValueOrDefault() != ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting) 
						 || nonVetCount == 0)
			    {
			      ServiceClientLocator.JobClient(App).InviteJobseekersToApply(allResumes.ToDictionary(x => x.Id, x => x.Score), JobId, GenerateJobLink(),
			          LensPostingId,ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer));

			      Confirmation.Show(CodeLocalise("EmailCandidatesConfirmation.Title", "Contact multiple job seekers"),
			        CodeLocalise("EmailCandidatesConfirmation.Body", "Thank you. We have sent an email to the selected star matches in your search result"),
			        CodeLocalise("Global.Close.Text", "Close"));

            inviteSent = true;
			    }
          else if (JobView.ExtendVeteranPriority.GetValueOrDefault().Equals(ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting))
          {
            // Ensure that at least one of the candidates is a veteran
            if (allResumes.Count - nonVetCount > 0)
            {
              // Send out emails to only veterans
              ServiceClientLocator.JobClient(App).InviteJobseekersToApply(allResumes.Where(x => x.IsVeteran).ToDictionary(x => x.Id, x => x.Score), JobId,
                  GenerateJobLink(), LensPostingId, ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer));

              Confirmation.Show("Posting exclusive to veterans",
                "Thank you. Given this posting is currently only available to veterans to apply, we have sent invites to matching veterans only. " +
                "To invite non-veteran job seekers, you should consider reducing the exclusivity period assigned to this posting.",
                CodeLocalise("Global.Close.Text", "Close"));

              inviteSent = true;
            }
            else
            {
              Confirmation.Show("No invites sent",
                "This posting is currently exclusive to veterans only. There are no veterans that meet your selected star match to invite",
                CodeLocalise("Global.Close.Text", "Close"));
            }
          }
          else // JobView.ExtendVeteranPriority.Value.Equals(ExtendVeteranPriorityOfServiceTypes.ExtendUntil)
          {
            // Send invites to veterans if any
            if (allResumes.Count - nonVetCount > 0)
            {
              // Send out emails to just the veterans
              ServiceClientLocator.JobClient(App)
                .InviteJobseekersToApply(allResumes.Where(x => x.IsVeteran).ToDictionary(x => x.Id, x => x.Score), JobId, GenerateJobLink(), LensPostingId, 
                ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer));

							Confirmation.Show("Posting exclusive to veterans",
								"This posting is currently only available to veterans to apply. Your invites to non-veteran job seekers will be saved and sent automatically when the exclusivity period is over. No further action is required.",
								CodeLocalise("Global.Close.Text", "Close"));
            }
            else
            {
							Confirmation.Show("Posting exclusive to veterans",
								"This posting is currently only available to veterans to apply. There are no veterans that meet your selected star match to invite. Your invites to non-veteran " +
								"job seekers will be saved and sent automatically when the exclusivity period is over. No further action is required.",
								CodeLocalise("Global.Close.Text", "Close"));
            }

            // Create a delayed invitation for any remaining non veterans
            foreach (var candidate in allResumes.Where(x => !x.IsVeteran))
            {
              jobClient.SaveInviteToApply(LensPostingId, jobId, candidate.Id, Score, true);
            }


            inviteSent = true;
          }

			    if (inviteSent)
			    {
			      ServiceClientLocator.JobClient(App).ResolveJobIssues(JobId, new List<JobIssuesFilter> {JobIssuesFilter.SearchNotInviting}, null, true);
			    }

			    break;
			}

			OnCompleted(new EventArgs());
			ModalPopup.Hide();
		}

		/// <summary>
		/// Handles the Click event of the CloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Click(object sender, EventArgs e)
		{
			OnCompleted(new EventArgs());
			ModalPopup.Hide();
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the MessageTypeDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void MessageTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			BindEmailBodyTextBox();
		}

    /// <summary>
    /// Handles the SelectedIndexChanged event of the JobNotSpecifiedMessageTypeDropDown control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void JobNotSpecifiedMessageTypeDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
      bool enableJobFields = JobNotSpecifiedMessageTypeDropDown.SelectedValueToEnum<EmailTemplateTypes>() != EmailTemplateTypes.CustomMessage;
      JobTitleDropDown.Visible = enableJobFields;
      JobTitleRequired.Visible = enableJobFields;
      JobTitleDropDownLiteral.Visible = enableJobFields;

      BindEmailBodyTextBox();
    }

		/// <summary>
		/// Handles the SelectedIndexChanged event of the CompanyNameDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CompanyNameDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			BindEmailBodyTextBox();
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the JobTitleDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobTitleDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			JobId = JobTitleDropDown.SelectedValueToLong();
			JobView = ServiceClientLocator.JobClient(App).GetJobView(JobId);
			Score = 0;
			BindEmailBodyTextBox();
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the ContactMethodDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ContactMethodDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			BindEmailBodyTextBox();
		}

		/// <summary>
		/// Raises the <see cref="E:Completed"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompleted(EventArgs eventArgs)
		{
			if (Completed != null)
				Completed(this, eventArgs);
		}

		/// <summary>
		/// Shows the job not specified controls.
		/// </summary>
    /// <param name="activeJobs">Whether the current user has active jobs</param>
		private void ShowJobNotSpecifiedControls(bool activeJobs = false)
		{
		  JobSpecifiedMode = false;

			JobSpecifiedControls.Visible = false;
			JobNotSpecifiedControls.Visible = true;
            JobTitleRequired.ErrorMessage = CodeLocalise("JobTitleRequired.ErrorMessage", "Job title is required");

		  if (App.Settings.Theme == FocusThemes.Education)
		  {
		    JobNotSpecifiedMessageTypeRow.Visible = true;

        JobNotSpecifiedMessageTypeDropDown.Items.Clear();        
        JobNotSpecifiedMessageTypeDropDown.Items.AddEnum(EmailTemplateTypes.CustomMessage, "Contact request");
		    ContactRequest = true;

        // Provide the invite to apply option if the user has jobs
		    if (activeJobs)
		    {
		      JobNotSpecifiedMessageTypeDropDown.Items.AddEnum(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer, "Invite to apply");

          // Set the Message Type depending on whether there is a job currently in scope
          bool isJob = JobId != 0;
          JobNotSpecifiedMessageTypeDropDown.SelectedIndex = isJob ? 1 : 0;
          JobTitleDropDown.Visible = isJob;
          JobTitleDropDownLiteral.Visible = isJob;

		    }
		    else
		    {
		      JobNotSpecifiedJobTitlePanel.Visible = false;
		    }
		  }

		}

		/// <summary>
		/// Shows the job specified controls.
		/// </summary>
		private void ShowJobSpecifiedControls()
		{
		  JobSpecifiedMode = true;

			JobSpecifiedControls.Visible = true;
			JobNotSpecifiedControls.Visible = false;
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
		  if (JobId > 0 && ApplicationStatus == ApplicationStatusTypes.NotApplicable)
		  {
		    JobView = ServiceClientLocator.JobClient(App).GetJobView(JobId);
		  }

		  switch (ActionType)
			{
			  case ActionTypes.EmailCandidate:

			    if (JobId == 0 || (JobId > 0 && ApplicationStatus == ApplicationStatusTypes.NotApplicable))
			    {
			      CompanyNameDropDown.DataSource = ServiceClientLocator.EmployeeClient(App).GetEmployeesBusinessUnits(Convert.ToInt64(App.User.EmployeeId));
			      CompanyNameDropDown.DataValueField = "BusinessUnitId";
			      CompanyNameDropDown.DataTextField = "BusinessUnitName";
			      CompanyNameDropDown.DataBind();

						JobTitleDropDown.DataSource = ServiceClientLocator.JobClient(App).GetJobs(JobStatuses.Active, Convert.ToInt64(App.User.EmployeeId));
						JobTitleDropDown.DataValueField = "Id";
						JobTitleDropDown.DataTextField = "JobTitle";
						JobTitleDropDown.DataBind();

            if (JobId == 0) JobTitleDropDown.Items.AddLocalisedTopDefault("", "- select -");

						// Temporarily remove option as currently not relevant
						//var manualJobsItem = new ListItem(CodeLocalise("NoPositionSpecified.Text", "No Position Specified"), string.Empty);
						//JobTitleDropDown.Items.Add(manualJobsItem);

						ContactMethodDropDown.Items.Clear();
						ContactMethodDropDown.Items.AddEnum(ContactMethods.FocusCareer, "apply through #FOCUSCAREER#");

						if (JobId > 0 && ApplicationStatus == ApplicationStatusTypes.NotApplicable)
						{
							JobTitleDropDown.SelectValue(JobId.ToString());
							CompanyNameDropDown.SelectValue(JobView.BusinessUnitId.ToString());
						}
					}
					else
					{
						MessageTypeDropDown.Items.Clear();

						MessageTypeDropDown.Items.AddEnum(EmailTemplateTypes.AcknowledgeApplication, "Acknowledge application");
						MessageTypeDropDown.Items.AddEnum(EmailTemplateTypes.RejectApplication, "Rejection");
            if (App.Settings.ShowCustomMessageTemplate) MessageTypeDropDown.Items.AddEnum(EmailTemplateTypes.CustomMessage, "Custom message");
					}

					BindEmailBodyTextBox();
					break;

				case ActionTypes.EmailCandidates:

					BindStarMatchDropdown();
					BindEmailBodyTextBoxForMultipleInvites();
					break;

			}
		}

		/// <summary>
		/// Populates the email details.
		/// </summary>
		private void PopulateEmailDetails()
		{
			EmailTemplateTypes emailTemplateType;
			string jobTitle, companyName;

			if (JobsSeekerContactInfo)
			{
				var jobseekerprofile = ServiceClientLocator.CandidateClient(App).GetJobSeekerProfile(PersonId, true);
				JobSeekerName.Text = jobseekerprofile.JobSeekerProfileView.FirstName + @" " + jobseekerprofile.JobSeekerProfileView.LastName;
				JobSeekerPhoneNumber.Text = jobseekerprofile.JobSeekerProfileView.HomePhone;
				JobSeekerEmailAddress.Text = jobseekerprofile.JobSeekerProfileView.EmailAddress;
			}

            var jobId = JobId;
            JobView = ServiceClientLocator.JobClient(App).GetJobView(jobId);

      DropDownList messageDropDownList = JobSpecifiedMode ? MessageTypeDropDown : JobNotSpecifiedMessageTypeDropDown;

		  if (messageDropDownList.SelectedValueToEnum<EmailTemplateTypes>() == EmailTemplateTypes.CustomMessage && !JobSpecifiedMode)
		  {
		    emailTemplateType = messageDropDownList.SelectedValueToEnum<EmailTemplateTypes>();
        companyName = (CompanyNameDropDown.SelectedItem.IsNotNull())
            ? CompanyNameDropDown.SelectedItem.Text
            : string.Empty;
        jobTitle = null;
		  }
		  else
		  {
        if (JobId == 0)
		    {
		      companyName = (CompanyNameDropDown.SelectedItem.IsNotNull())
		        ? CompanyNameDropDown.SelectedItem.Text
		        : string.Empty;
          JobId = JobTitleDropDown.SelectedValueToLong();
          jobTitle = (JobId > 0 ? JobTitleDropDown.SelectedItem.Text : string.Empty);
		      emailTemplateType = EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer;
		    }
        else if (JobId > 0 && ApplicationStatus == ApplicationStatusTypes.NotApplicable)
		    {
		      //companyName = jobView.BusinessUnitName;
          companyName = (CompanyNameDropDown.SelectedItem.IsNotNull())
            ? CompanyNameDropDown.SelectedItem.Text
            : string.Empty;
		      jobTitle = JobView.JobTitle;
		      emailTemplateType = EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer;
		    }
        else
        {
          emailTemplateType = MessageTypeDropDown.SelectedValueToEnum<EmailTemplateTypes>();
          jobTitle = JobView.JobTitle;
          companyName = JobView.BusinessUnitName;
        }
		  }

      if (JobView.IsNotNull() && JobView.IsConfidential.IsNotNull() && JobView.IsConfidential.Value)
				companyName = CodeLocalise("IsConfidential.Label", "[Confidential]");

      LensPostingId = ServiceClientLocator.JobClient(App).GetJobLensPostingId(JobId);
			var jobLinkUrl = GenerateJobLink();

			var templateValues = new EmailTemplateData
			{
				RecipientName = CandidateName,
				JobTitle = jobTitle,
				EmployerName = companyName,
				SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName),
        ShowSalutation = (ApplicationStatus != ApplicationStatusTypes.NotApplicable && ApplicationStatus != ApplicationStatusTypes.SelfReferred && CandidateName.IsNotNullOrEmpty() && JobId != 0),
				JobLinkUrls = new List<string> { jobLinkUrl }
			};

			EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(emailTemplateType, templateValues);
		}

		/// <summary>
		/// Binds the email body text box.
		/// </summary>
		private void BindEmailBodyTextBox()
		{
			PopulateEmailDetails();

			bool readOnlyEmail;

			if (App.Settings.Theme == FocusThemes.Education)
				readOnlyEmail = false;
			else
				readOnlyEmail = JobSpecifiedMode ? (MessageTypeDropDown.SelectedValueToEnum<EmailTemplateTypes>() != EmailTemplateTypes.CustomMessage) : (JobNotSpecifiedMessageTypeDropDown.SelectedValueToEnum<EmailTemplateTypes>() != EmailTemplateTypes.CustomMessage);

			EmailBodyTextBox.ReadOnly = readOnlyEmail;
			EmailBodyTextBox.Text = EmailTemplate.Body;

			EmailSubjectTextBox.ReadOnly = readOnlyEmail;
			EmailSubjectTextBox.Text = EmailTemplate.Subject;
		}

		/// <summary>
		/// Binds the email body text box for multiple invites.
		/// </summary>
		private void BindEmailBodyTextBoxForMultipleInvites()
		{
      var jobTitle = JobView.JobTitle;
			var companyName = JobView.BusinessUnitName;

			LensPostingId = ServiceClientLocator.JobClient(App).GetJobLensPostingId(JobId);
			var jobLinkUrl = GenerateJobLink();

			var templateValues = new EmailTemplateData
			{
				JobTitle = jobTitle,
				EmployerName = companyName,
				SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName),
				ShowSalutation = false,
				JobLinkUrls = new List<string> { jobLinkUrl }
			};

			EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer, templateValues);

			EmailBodyTextBox.ReadOnly = true;
			EmailBodyTextBox.Text = EmailTemplate.Body;

			EmailSubjectTextBox.ReadOnly = true;
			EmailSubjectTextBox.Text = EmailTemplate.Subject;
		}

		/// <summary>
		/// Binds the star match dropdown.
		/// </summary>
		private void BindStarMatchDropdown()
		{
			StarMatchDropdown.Items.Clear();

			StarMatchDropdown.Items.Add(new ListItem(CodeLocalise("5StarMatchItem.Text", "5 stars"), "5"));
			StarMatchDropdown.Items.Add(new ListItem(CodeLocalise("4StarMatchItem.Text", "4 stars"), "4"));
			StarMatchDropdown.Items.Add(new ListItem(CodeLocalise("3StarMatchItem.Text", "3 stars"), "3"));
			StarMatchDropdown.Items.Add(new ListItem(CodeLocalise("2StarMatchItem.Text", "2 stars"), "2"));
			StarMatchDropdown.Items.Add(new ListItem(CodeLocalise("1StarMatchItem.Text", "1 star"), "1"));
		}

		/// <summary>
		/// Generates the job link.
		/// </summary>
		/// <returns></returns>
		private string GenerateJobLink()
		{
			return (LensPostingId.IsNotNullOrEmpty()) ? UrlBuilder.CareerInviteToJobPosting(LensPostingId) : string.Empty;
		}
	}
}
