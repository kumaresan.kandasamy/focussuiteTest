﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabNavigation.ascx.cs" Inherits="Focus.Web.WebTalent.Controls.TabNavigation" %>
<ul class="navTab">
	<li ID="TabJobs" runat="server"><a href="<%= UrlBuilder.TalentJobs() %>"><%= HtmlLocalise("TabJobs.Label.Tab", "#POSTINGTYPES#")%></a></li>
	<li ID="TabPool" runat="server"><a href="<%= UrlBuilder.TalentPool() %>"><%= HtmlLocalise("TabPool.Label.Tab", "Talent Pool")%></a></li>
  <li ID="TabJobSearch" runat="server"><a href="<%= UrlBuilder.TalentJobSearch() %>"><%= HtmlLocalise("TabJobSearch.Label.Tab", "#POSTINGTYPE# Search")%></a></li>
</ul>

<div id="headerButtons">
	<asp:Button ID="UploadJobsButton" runat="server" OnClick="UploadJobButton_Click" UseSubmitBehavior="False" CausesValidation="false"></asp:Button>
	<asp:Button ID="PostNewJobButton" runat="server" OnClick="PostNewJobButton_Click" UseSubmitBehavior="False" CausesValidation="false"></asp:Button>
</div>