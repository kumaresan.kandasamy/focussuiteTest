﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;


#endregion


namespace Focus.Web.WebTalent.Controls
{
  public partial class LogoResizeModal : UserControlBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      

    }

    /// <summary>
    /// Shows the specified top.
    /// </summary>
    /// <param name="top">The top.</param>
    /// <param name="imageBytes">The image bytes array.</param>
    public void Show(byte[] imageBytes, int top = 210)
    {
      string base64String = Convert.ToBase64String(imageBytes, 0, imageBytes.Length);
      //ResizedImage.ImageUrl = "data:image/png;base64," + base64String;
      ResizeModalPopup.Y = 600;
      ResizeModalPopup.Show();

     
    }

    
  }
}