﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobsNavigation.ascx.cs" Inherits="Focus.Web.WebTalent.Controls.JobsNavigation" %>

<p class="pageNav">
	<% 
    if (IsJobWizardPage() || IsJobUploadPage())
  	{%>
			<a href="<%= UrlBuilder.TalentJobs() %>"><%= HtmlLocalise("Dashboard.LinkText", "Dashboard")%></a>
		<% }
  	else
  	{ %>
			<strong><%= HtmlLocalise("Dashboard.LinkText", "Dashboard")%></strong>
		<% }

    if (!IsJobWizardPage())
		{%>
			&nbsp;|&nbsp;<a id="PostJobUrl" runat="server"><%= HtmlLocalise("JobWizard.LinkText", "Post a new #POSTINGTYPE#")%></a>
		<% }
		else
		{ %>
			&nbsp;|&nbsp;<strong><%= HtmlLocalise("JobWizard.LinkText", "Post a new #POSTINGTYPE#")%></strong>
		<% }

        if (!this.App.Settings.HideUploadMultipleJobs && Utilities.ShowJobUpload(App.User.EmployeeId))
    {
      if (!IsJobUploadPage())
      { 
      %>
			&nbsp;|&nbsp;<a id="JobUploadUrl" runat="server"><%= HtmlLocalise("JobUpload.LinkText", "Upload multiple #POSTINGTYPES#") %></a>
		  <% 
      }
      else
      { 
      %>
			&nbsp;|&nbsp;<strong><%= HtmlLocalise("JobUpload.LinkText", "Upload multiple #POSTINGTYPES#") %></strong>
		  <%
      }
    }
		%>
</p>
