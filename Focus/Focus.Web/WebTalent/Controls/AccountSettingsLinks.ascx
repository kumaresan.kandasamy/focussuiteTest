﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountSettingsLinks.ascx.cs" Inherits="Focus.Web.WebTalent.Controls.AccountSettingsLinks" %>
	<p class="pageNav">
	  <asp:PlaceHolder ID="TalentAuthenticationLinks" runat="server">
		  <asp:PlaceHolder runat="server" ID="ChangePasswordPlaceHolder">
		  <a id="TalentChangePasswordLink" runat="server"><%= HtmlLocalise("Global.ChangeLogin.LinkText", "Change login")%></a>&nbsp;|&nbsp;
			</asp:PlaceHolder>
		  <a id="TalentContactInformationLink" runat="server"><%= HtmlLocalise("Global.UpdateContactInformation.LinkText", "Update contact information")%></a>&nbsp;|&nbsp;
    </asp:PlaceHolder>

		<asp:PlaceHolder ID="TalentManageCompanyLinkPlaceholder" runat="server">
			<a id="TalentManageCompanyLink" runat="server"><%= HtmlLocalise("Global.ManageCompany.LinkText", "Manage #BUSINESS#:LOWER names, descriptions & logos")%></a>&nbsp;|&nbsp;
		</asp:PlaceHolder>

		<a id="TalentSavedSearchesLink" runat="server"><%= HtmlLocalise("Global.ManageSavedSearches.LinkText", "Manage saved searches")%></a>
		
		<asp:PlaceHolder ID="TalentChangePasswordSSOLinkPlaceholder" runat="server">
			&nbsp;|&nbsp;
		<a id="TalentChangePasswordSSOLink" runat="server"><%= HtmlLocalise("Global.ChangeLogin.LinkText", "Change login")%></a>
		</asp:PlaceHolder>
		<asp:PlaceHolder ID="TalentContactInformationSSOLinkPlaceholder" runat="server">
		&nbsp;|&nbsp;
		<a id="TalentContactInformationSSOLink" runat="server"><%= HtmlLocalise("Global.UpdateContactInformation.LinkText", "Update contact information")%></a>
		</asp:PlaceHolder>
	</p>