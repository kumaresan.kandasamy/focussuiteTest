﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogoResizeModal.ascx.cs" Inherits="Focus.Web.WebTalent.Controls.LogoResizeModal" %>


<asp:HiddenField ID="ResizeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ResizeModalPopup" runat="server" BehaviorID="Modal"
												TargetControlID="ResizeModalDummyTarget"
												PopupControlID="ResizeModalPanel" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ResizeModalPanel" runat="server" CssClass="modal">
	<div style="float:left;width:650px;" align="left">
		<h1>Test Modal</h1>
	
			<asp:Image ID="ResizedImage" runat="server"/>
		</div>

	<div class="closeIcon"><img src="<%= UrlBuilder.Content("~/Assets/Images/button_x_close_off.png") %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('NotesRemindersModal').hide(); return false;" /></div>
</asp:Panel>