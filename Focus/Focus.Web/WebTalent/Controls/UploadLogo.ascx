﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadLogo.ascx.cs" Inherits="Focus.Web.WebTalent.Controls.UploadLogo" %>

<table role="presentation">
	<tr>
		<td>
			<%= HtmlInFieldLabel("LogoNameTextBox", "LogoName.Label", "Name this logo", 330)%>
			<asp:TextBox ID="LogoNameTextBox" runat="server" MaxLength="200" Width="330px" ClientIDMode="Static" AutoCompleteType="Disabled" />			
			<asp:RequiredFieldValidator ID="LogoNameRequired" runat="server" ControlToValidate="LogoNameTextBox" CssClass="error"  />	
		</td>
	</tr>

	<tr>
		<td>
			<div id="UploaderDiv">
            <label>
				<act:AsyncFileUpload ID="UploadLogoAsyncFileUpload"  runat="server" ClientIDMode="Static" CssClass="asyncfileuploadfix fileupload" ThrobberID="UploadThrobberPanel"
																	OnClientUploadStarted="UploadLogoCheckExtension" OnClientUploadComplete="UploadLogoUploadComplete"
																	OnUploadedComplete="UploadLogoAsyncFileUpload_UploadedComplete" />
				
                <%= HtmlLabel("LogoRequirements.Label", "Logos must be in GIF, JPG, or PNG format at 72dpi. Maximum size 450 pixels wide by 113 pixels high.")%></label>
				</div>
				<div id="ImageDiv" style="display: none"><asp:Image ID="UploadedLogoImage" runat="server" ClientIDMode="Static" Width="10" Height="10" AlternateText="Uploaded image"/></div>
				<focus:LocalisedLabel runat="server" ID="UploadedLogoFileTypeLabel" AssociatedControlID="UploadedLogoFileType" LocalisationKey="UploadedLogoFilteType" DefaultText="Uploaded logo file type" CssClass="sr-only"/>
				<asp:TextBox ID="UploadedLogoFileType" runat="server" ClientIDMode="Static"/>
				<asp:CustomValidator ID="UploadLogoAsyncFileUploadedLogoFileTypeValidator" runat="server" ControlToValidate="UploadedLogoFileType" 
													ClientValidationFunction="UploadLogoAsyncFileUploadLogoFileTypeValidator_Validate" CssClass="error" />
				<focus:LocalisedLabel runat="server" ID="UploadedLogoIsValidLabel" AssociatedControlID="UploadedLogoIsValid" LocalisationKey="UploadedLogoIsValid" DefaultText="Is logo valid?" CssClass="sr-only"/>
				<asp:TextBox ID="UploadedLogoIsValid" runat="server" ClientIDMode="Static" /><br/>
				<asp:CustomValidator ID="UploadLogoAsyncFileUploadedLogoIsValidValidator" runat="server" ControlToValidate="UploadedLogoIsValid"  
													ClientValidationFunction="UploadLogoAsyncFileUploadedLogoIsValidValidator_Validate" CssClass="error" />
			
				<asp:Panel runat="server" ID="UploadThrobberPanel" EnableViewState="false" style="display:none;" CssClass="updateprogress" >
				&nbsp;&nbsp;&nbsp;Uploading...
				</asp:Panel>
			</td>
	</tr>
	<tr>
			<td style="text-align: left">				
				<i><asp:Label ID="ImageText" runat="server"  style="display: none"/></i> 
      </td>
		</tr>
</table>	
