﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PoolActionModal.ascx.cs"
    Inherits="Focus.Web.WebTalent.Controls.PoolActionModal" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal"
    TagPrefix="uc" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
    PopupControlID="ModalPanel" PopupDragHandleControlID="PoolActionModalHeader"
    RepositionMode="RepositionOnWindowResizeAndScroll" BackgroundCssClass="modalBackground" />
<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display: none">
    <table width="500px">
        <tr>
            <th valign="top" data-modal='title' class="modal-header" id="PoolActionModalHeader">
                <%= Title %>
            </th>
        </tr>
        <tr>
            <td>
                <asp:PlaceHolder runat="server" ID="EmailCandidateInstructionsPanel">
                    <p>
                        <strong>
                            <asp:Literal ID="EmailCandidateSummary" runat="server"></asp:Literal></strong></p>
                    <table id="ContactDetails" runat="server" role="presentation" style="margin-bottom: 10px;">
                        <tr id="JobSeekerNameRow">
                            <td width="100px">
                                <%= HtmlLocalise("EmailCandidate.Name", "Name")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobSeekerName" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr id="JobSeekerPhoneNumberRow">
                            <td width="100px">
                                <%= HtmlLocalise("EmailCandidate.Phone", "Phone")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobSeekerPhoneNumber" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr id="JobSeekerEmailAddressRow">
                            <td width="100px">
                                <%= HtmlLocalise("EmailCandidate.Email", "Email")%>
                            </td>
                            <td>
                                <asp:Literal ID="JobSeekerEmailAddress" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="EmailCandidatesControlsPanel" Visible="False">
                    <p>
                        <focus:LocalisedLabel runat="server" ID="StarMatchLabel" ClientIDMode="Static" LocalisationKey="StarMatchLabel.Text"
                            DefaultText="Select star match to invite" />
                        &nbsp;&nbsp;
                        <asp:DropDownList runat="server" ID="StarMatchDropdown" ClientIDMode="Static" Width="100px" />
                    </p>
                    <p>
                        <focus:LocalisedLabel runat="server" ID="MultipleInvitesValidationLabel" ClientIDMode="Static"
                            LocalisationKey="MultipleInvitesValidationLabel.Text" DefaultText="No job seekers exist with your selected star match"
                            CssClass="error" Visible="False" />
                    </p>
                </asp:PlaceHolder>
                <asp:UpdatePanel runat="server" ID="EmailActionUpdatePanel" UpdateMode="Always">
                    <ContentTemplate>
                        <p>
                            <asp:Panel runat="server" ID="JobSpecifiedControls">
                                <%= HtmlLabel("EmailCandidateMessageType.Label", "Message type")%>
                                <asp:DropDownList ID="MessageTypeDropDown" runat="server" ClientIDMode="Static" AutoPostBack="True"
                                    Width="300px" OnSelectedIndexChanged="MessageTypeDropDown_SelectedIndexChanged"
                                    Title="Message type" />
                            </asp:Panel>
                            <asp:Panel runat="server" ID="JobNotSpecifiedControls">
                                <table role="presentation">
                                    <tr id="JobNotSpecifiedMessageTypeRow" runat="server" visible="false">
                                        <td>
                                            <%= HtmlLabel("EmailCandidateMessageType.Label", "Message type")%>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="JobNotSpecifiedMessageTypeDropDown" runat="server" ClientIDMode="Static"
                                                AutoPostBack="True" Width="300px" OnSelectedIndexChanged="JobNotSpecifiedMessageTypeDropDown_SelectedIndexChanged"
                                                Style="margin-bottom: 10px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <focus:LocalisedLabel runat="server" ID="CompanyNameLabel" ClientIDMode="Static"
                                                LocalisationKey="CompanyNameLabel.Text" DefaultText="Company name" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="CompanyNameDropDown" runat="server" ClientIDMode="Static" AutoPostBack="True"
                                                Width="300px" OnSelectedIndexChanged="CompanyNameDropDown_SelectedIndexChanged"
                                                Style="margin-bottom: 10px;" Title="Company name" />
                                        </td>
                                    </tr>
                                    <tr>
                                    <td  colspan="2">
                                    <asp:Panel runat="server" ID="JobNotSpecifiedJobTitlePanel">
                                        <table role="presentation" width="100%">
                                            <tr>
                                                <td width="100%">
                                                    <asp:Literal runat="server" ID="JobTitleDropDownLiteral"></asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="JobTitleDropDown" runat="server" ClientIDMode="Static" AutoPostBack="True"
                                                        Width="300px" OnSelectedIndexChanged="JobTitleDropDown_SelectedIndexChanged" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator runat="server" ID="JobTitleRequired" ControlToValidate="JobTitleDropDown"
                                                        SetFocusOnError="true" CssClass="error" ValidationGroup="PoolActionModal" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    </td>
                                    </tr>
                                    <tr id="ContactMethodRow" runat="server">
                                        <td>
                                            <%= HtmlLabel("ContactMethod.Label", "Contact method")%>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ContactMethodDropDown" runat="server" ClientIDMode="Static"
                                                AutoPostBack="True" Width="300px" OnSelectedIndexChanged="ContactMethodDropDown_SelectedIndexChanged"
                                                Title="Contact method">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </p>
                        <p>
                            <asp:TextBox ID="EmailSubjectTextBox" runat="server" ClientIDMode="Static" MaxLength="200"
                                ReadOnly="True" TextMode="SingleLine" Width="480px" Title="Email Subject"></asp:TextBox></p>
                        <p>
                            <asp:TextBox ID="EmailBodyTextBox" runat="server" TextMode="MultiLine" Rows="10"
                                ClientIDMode="Static" ReadOnly="True" Title="Email Body"></asp:TextBox></p>
                        <div>
                            <asp:RequiredFieldValidator ID="EmailSubjectRequired" runat="server" ControlToValidate="EmailSubjectTextBox"
                                SetFocusOnError="true" CssClass="error" ValidationGroup="PoolActionModal" /></div>
                        <div>
                            <asp:RequiredFieldValidator ID="EmailBodyRequired" runat="server" ControlToValidate="EmailBodyTextBox"
                                SetFocusOnError="true" CssClass="error" ValidationGroup="PoolActionModal" /></div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="MessageTypeDropDown" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="JobNotSpecifiedMessageTypeDropDown" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="CompanyNameDropDown" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="JobTitleDropDown" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="ContactMethodDropDown" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button ID="OkButton" runat="server" SkinID="Button3" CausesValidation="true"
                    ValidationGroup="PoolActionModal" OnClick="OkButton_Click" Title="OkButton" />
                <asp:Literal ID="OkButtonSpacer" runat="server" EnableViewState="false" Text="  " />
                <asp:Button ID="CloseButton" runat="server" SkinID="Button4" CausesValidation="false"
                    OnClick="CloseButton_Click" Title="CloseButton" />
            </td>
        </tr>
    </table>
</asp:Panel>
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
