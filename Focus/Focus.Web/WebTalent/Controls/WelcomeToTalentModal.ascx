﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WelcomeToTalentModal.ascx.cs" Inherits="Focus.Web.WebTalent.Controls.WelcomeToTalentModal" %>

<asp:HiddenField ID="WelcomeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="WelcomeModal" runat="server" BehaviorID="WelcomeModal"
												TargetControlID="WelcomeModalDummyTarget"
												PopupControlID="WelcomePanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground"
												CancelControlID="ReturnHomeButton"
												Y="50" />

<asp:Panel ID="WelcomePanel" runat="server" CssClass="modal" Width="550px" ClientIDMode="Static" Style="display:none;" >
	<div style="float:left;">
		<table role="presentation">
			<tr>
				<td width="550px">
					<asp:Literal ID="WelcomeContentLiteral" runat="server" />
				</td>
			</tr>
		</table>
	</div>
	<div class="closeIcon">
		<input type="image" runat="server" ID="CloseIcon" onclick="$find('WelcomeModal').hide(); return false;"/>
		<asp:ImageButton ID="CloseButton" runat="server" CausesValidation="false" OnCommand="CloseButton_Command" Visible="False" />
	</div>
</asp:Panel>
