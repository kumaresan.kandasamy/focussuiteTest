﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebTalent.Controls
{
	public partial class JobsNavigation : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
		  PostJobUrl.HRef = UrlBuilder.JobWizard();
		  JobUploadUrl.HRef = UrlBuilder.JobUpload();
		}

	  /// <summary>
		/// Determines whether [is job wizard page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is job wizard page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsJobWizardPage()
		{
			return (Page.Is(UrlBuilder.JobWizard()));
		}

    /// <summary>
    /// Checks if the Talent page is the Job Upload page
    /// </summary>
    /// <returns>True for the job upload page, false otherwise</returns>
    protected bool IsJobUploadPage()
    {
      return Page.Is(UrlBuilder.JobUpload());
    }
	}
}