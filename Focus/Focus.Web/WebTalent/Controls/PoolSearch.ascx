﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PoolSearch.ascx.cs" Inherits="Focus.Web.WebTalent.Controls.PoolSearch" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register src="~/Code/Controls/User/SaveSearchActionModal.ascx" tagname="SaveSearchActionModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchAdditionalCriteria.ascx" tagname="AdditionalCriteria" tagprefix="uc" %>
	
<asp:Panel ID="SearchPanelTop" DefaultButton="KeywordSearchButton" runat="server">
	<div style="width:100%">
		<strong><%= HtmlLocalise("SearchCriteriaTitle.Label", "Search resumes")%></strong>
		&nbsp;
		<asp:Button ID="SaveSearchButton" runat="server"  class="button3" ClientIDMode="Static" onclick="SaveSearchButton_Click" />
		&nbsp;
		<asp:Label ID="SaveSearchErrorLabel" runat="server" CssClass="error" />
	</div>
	<div style="width:99%; white-space:nowrap; margin-top:2px; margin-bottom:10px; display: inline-block">
		<div style="display: inline-block">
			<asp:Literal ID="SearchLabel" runat="server" />
			<asp:DropDownList runat="server" ID="JobTypeDropDownList" ClientIDMode="Static" />
			&nbsp;<%= HtmlLocalise("Keywords.Text", "with keyword(s)")%>
		</div>
		<div style="display: inline-block">
			<%= HtmlInFieldLabel("KeywordsTextBox", "Keywords.InlineLabel", "type keywords", 250)%>
			<asp:TextBox runat="server" ID="KeywordsTextBox" ClientIDMode="Static" Width="250"/>
		</div>
		<%= HtmlLocalise("In.Text", "in")%>
		<asp:DropDownList runat="server" ID="KeywordContextDropDown" ClientIDMode="Static" Title="Keywords in"/>
		<%= HtmlLocalise("Within.Text", "within")%>
		<asp:DropDownList runat="server" ID="KeywordScopeDropDown" ClientIDMode="Static" Enabled="False" Title="Keywords within"/>
		<asp:Button ID="KeywordSearchButton" runat="server" class="button3" onclick="KeywordSearchButton_Click" ClientIDMode="Static" />
		&nbsp;
		<asp:RequiredFieldValidator ID="KeywordRequired" ControlToValidate="KeywordsTextBox" runat="server" ValidationGroup="PoolSearchKeywordSearch" CssClass="error" Display="Dynamic"/>
    <asp:CustomValidator ID="KeywordsCustomValidator" runat="server" ControlToValidate="KeywordsTextBox" ClientValidationFunction="validateKeywords"
						                             ValidationGroup="PoolSearchKeywordSearch" ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" ClientIDMode="Static" />
		<div style="float: right">
			<asp:Button ID="ClearSearchButton" runat="server" class="button3"  style="float:right" onclick="ClearSearchButton_Click" ClientIDMode="Static" />
			<asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3 is-collapsed" style="float:right" ClientIDMode="Static"/>
		</div>
	</div>
</asp:Panel>

<asp:Panel ID="AdvancedSearchHeaderPanel" runat="server" CssClass="advancedSearchAccordionTitle">
	<asp:Image ID="AdvancedSearchHeaderImage" runat="server" alt="Advanced Search Image"/>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Search.Label", "Search options")%></a></span>
</asp:Panel>
<asp:Panel ID="AdvancedSearchPanel" runat="server">
	<div id="SearchPanelMiddle">
	  <div class="SearchPanelMiddleAccordionContent">
		  <table role="presentation">
			  <tr>
				  <td class="column1">
				    <asp:Label runat="server" ID="SelectOpenJobsLabel" ClientIDMode="Static" />
            <asp:Label runat="server" ID="SelectOpenInternshipsLabel" ClientIDMode="Static"/>
				  </td>
				  <td class="column2"><%= HtmlLabel("SampleResumeSearch.Label", "Search by sample resume")%></td>
			  </tr>
			  <tr>
				  <td class="column1">
					  <table role="presentation">
						  <tr>
							  <td style="width:250">
							    <asp:DropDownList runat="server" ID="OpenJobsDropDownList" Width="250" ClientIDMode="Static" CausesValidation="false" Title="Open Jobs"/>
                  <asp:HiddenField runat="server" ID="PoolSearchJobIds" ClientIDMode="Static"/>
                  <asp:HiddenField runat="server" ID="PoolSearchInternshipIds" ClientIDMode="Static"/>
							  </td>
							  <td><asp:Button ID="OpenJobButton" runat="server" class="button3" ClientIDMode="Static" onclick="OpenJobButton_Click"  /></td>	
							  <td>
							    <span style="display: none" id="OpenJobsTooltip" clientidmode="Static">
								    <%= HtmlTooltipster("tooltipWithArrow", "OpenJobs.Tooltip", @"Select an open job and click &quot;Go&quot; to begin your search. You may narrow your search by selecting other criteria in Advanced search options.")%>
                  </span>
                  <span style="display: none" id="OpenInternshipsTooltip" clientidmode="Static">
							      <%= HtmlTooltipster("tooltipWithArrow", "OpenInternships.Tooltip", @"Select an open internship and click &quot;Go&quot; to begin your search. You may narrow your search by selecting other criteria in Advanced search options.")%>
                  </span>
							  </td>	
						    <td><asp:CustomValidator ID="JobSearchValidator" runat="server" ControlToValidate="JobTitleTextBox" ClientValidationFunction="validateJobSearch"
						                             ValidationGroup="PoolSearchJobSearch" ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" ClientIDMode="Static" /></td>
						  </tr>
					  </table>
					  <div id="ManualJobTitleDiv" style="display:none;">
						  <%= HtmlInFieldLabel("JobTitleTextBox", "JobTitle.InlineLabel", "#POSTINGTYPE#:LOWER title", 250)%>
						  <asp:TextBox ID="JobTitleTextBox" runat="server" ClientIDMode="Static" Width="250" />
						  <asp:HiddenField ID="JobIdKnownHidden" runat="server" ClientIDMode="Static" Value="false" />
					  </div>
				  </td>
				  <td class="column2">
					  <table role="presentation">
						  <tr >
							  <td style="width:275"><asp:FileUpload ID="ResumeFileUpload" title="Resume File Upload" runat="server" CssClass="resumefileupload" ClientIDMode="Static" /></td>
							  <td style="padding: 4px 0 0 3px; vertical-align:top;"><div class="tooltip"><%= HtmlTooltipster("tooltipWithArrow", "ResumeFileUpload.Tooltip", @"Upload a resume and click &quot;go&quot; to begin your search. You may narrow your search by selecting other criteria in Advanced search options.")%></div></td>	
							  <td>
							    <asp:RegularExpressionValidator ID="ResumeFileUploadRegularEx" runat="server" ControlToValidate="ResumeFileUpload" CssClass="error" ValidationGroup="PoolSearchResumeSearch" />
                  <asp:CustomValidator runat="server" ID="ResumeCustomValidator" CssClass="error"></asp:CustomValidator>
							  </td>
						  </tr>
					  </table>
				  </td>
			  </tr>
			  <tr>
				  <td class="column1">
				  </td>
				  <td class="column2">
					  <div id="UploadedResumeDiv" style="display:none;">
						  <asp:Label id="UploadedResumeLabel" runat="server" ClientIDMode="Static"></asp:Label>
						  <asp:Button ID="SampleResumeButton" runat="server" class="button3" ClientIDMode="Static" onclick="SampleResumeButton_Click" />
						  <asp:Button ID="ClearResumeButton" runat="server" class="button3" ClientIDMode="Static" onclick="ClearResumeButton_Click" CausesValidation="False" />
					  </div>
				  </td>
			  </tr>
			  <tr id="SavedSearchLabelRow">
				  <td class="column1"><%= HtmlLabel("SavedSearch.Label", "Search by saved query")%></td>
				  <td class="column2">
				  </td>
			  </tr>
			  <tr id="SavedSearchRow">
				  <td class="column1">
					  <focus:LocalisedLabel runat="server" ID="SavedSearchDropDownLabel" AssociatedControlID="SavedSearchDropDown" LocalisationKey="SavedSearches" DefaultText="Saved searches" CssClass="sr-only"/>
					  <asp:DropDownList runat="server" ID="SavedSearchDropDown" Width="250" ClientIDMode="Static" />
					  <asp:Button ID="SavedSearchButton" runat="server" class="button3" ClientIDMode="Static" ValidationGroup="PoolSavedSearch" onclick="SavedSearchButton_Click" />
					  <asp:RequiredFieldValidator ID="SavedSearchRequired" ControlToValidate="SavedSearchDropDown" runat="server" ValidationGroup="PoolSavedSearch" CssClass="error" />
				  </td>
				  <td class="column2">
				  </td>
			  </tr>
        </table>
      </div>
			<uc:AdditionalCriteria runat="server" ID="AdditionalCriteria" ValidationGroup="PoolSearchAdvancedSearch" />
     <table role="presentation" style="width:100%"> 
			<tr>
				<td colspan="2"><asp:Button ID="ApplyAdvancedSearchButton" runat="server" class="button3 right" ClientIDMode="Static" ValidationGroup="PoolSearchAdvancedSearch" onclick="ApplyAdvancedSearch_Click" Text="Apply advanced search"/>
					</td>
			</tr>
		</table>
	</div>
	
</asp:Panel>
<act:CollapsiblePanelExtender ID="AdvancedSearchPanelExtender" runat="server" 
														TargetControlID="AdvancedSearchPanel" 
														ExpandControlID="AdvancedSearchHeaderPanel" 
														CollapseControlID="AdvancedSearchHeaderPanel" 
														Collapsed="true" 
														ImageControlID="AdvancedSearchHeaderImage" 
														SuppressPostBack="true" BehaviorID="AdvancedSearchPanelBehavior"
														 />
<div id="SearchPanelBottom">
</div>
<div class="clear">
</div>

<!-- Modals -->
<uc:SaveSearchActionModal ID="SaveSearchAction" runat="server" OnCompleted="SaveSearchAction_Completed"/>
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />

<script type="text/javascript">
	function validateKeywords(oSrc, args) {
		args.IsValid = $("#<%=KeywordsTextBox.ClientID %>").val().match(/[^A-Za-z0-9_ ]{4,}/) == null;
	}
</script>
<asp:PlaceHolder runat="server" ID="EducationScriptPlaceHolder">
<script type="text/javascript">

	
	var PoolSearch_EducationCriteriaAlternativeTitle = '<%=CodeLocalise("EducationCriteria.AlternativeTitle", "Expected Completion Date & Enrollment Status")%>';
	var PoolSearch_EducationCriteriaTitle = '<%=CodeLocalise("EducationCriteria.Title", "Education")%>';
	var PoolSearch_JobSearchValidatorRequiredError = '<%=CodeLocalise("JobSearchValidator.RequiredError", "Select either an open job or enter a job title to search against")%>';
	var PoolSearch_InternshipSearchValidatorRequiredError = '<%=CodeLocalise("InternshipSearchValidator.RequiredError", "Select either an open internship or enter an internship title to search against")%>';
	var PoolSearch_JobTitleInlineLabel = '<%=CodeLocalise("JobTitle.InlineLabel", "job title")%>';
	var PoolSearch_InternshipTitleInlineLabel = '<%=CodeLocalise("InternshipTitle.InlineLabel", "internship title")%>';

	var PoolSearch_JobIdArray = [];
	var PoolSearch_InternshipIdArray = [];
	var originalValue = "";

	$(document).ready(function () {

		PoolSearch_JobIdArray = $("#PoolSearchJobIds").val().split(",");
		PoolSearch_InternshipIdArray = $("#PoolSearchInternshipIds").val().split(",");

		var select = $("#OpenJobsDropDownList");
		

		
		/*
    var options = [];
    
    select.find('option').each(function () {
    options.push({ value: $(this).val(), text: $(this).text() });
    });
    */
		var jobOptions = [];
		var internshipOptions = [];

		var allOptions = select.find('option');
		var lastJobIndex = allOptions.length - 2;

		allOptions.each(function(index) {
			if ($.inArray(this.value, PoolSearch_JobIdArray) !== -1 || index == 0 || index == lastJobIndex) {
				jobOptions.push(this);
			} else {
				internshipOptions.push(this);
			}
		});
		select.data('jobOptions', jobOptions);
		select.data('internshipOptions', internshipOptions);

		$("#JobTypeDropDownList").change(function () {
			var savedOriginalValue = originalValue;
			ShowHideCriteria(false);
			originalValue = savedOriginalValue;
			$("#OpenJobsDropDownList").val(originalValue);
		});

		ShowHideCriteria(true);
	});

	function ShowHideCriteria(isLoading) {

		var isJob = $("#JobTypeDropDownList").prop("selectedIndex") == 0;

		if (isJob) {
		
			HideSkillsCriteria();
			ShowExpectedCompletionSection();
			ShowEnrollmentStatusDropDown();
			ShowCertificationsCriteria();
			ShowAvailabilityCriteria();
			ShowScoreCriteria();
			SetEducationCriteriaTitle(PoolSearch_EducationCriteriaAlternativeTitle);

			$("#SelectOpenJobsLabel").show();
			$("#SelectOpenInternshipsLabel").hide();
			$("#OpenJobsTooltip").show();
			$("#OpenInternshipsTooltip").hide();

			$("#JobSearchValidator").text(PoolSearch_JobSearchValidatorRequiredError);
			$("#ManualJobTitleDiv").find("label").text(PoolSearch_JobTitleInlineLabel);
		} else {
			ShowSkillsCriteria();
			HideExpectedCompletionSection();
			HideEnrollmentStatusDropDown();
			HideCertificationsCriteria();
			HideAvailabilityCriteria();
			HideScoreCriteria();
			SetEducationCriteriaTitle(PoolSearch_EducationCriteriaTitle);

			$("#SelectOpenJobsLabel").hide();
			$("#SelectOpenInternshipsLabel").show();
			$("#OpenJobsTooltip").hide();
			$("#OpenInternshipsTooltip").show();

			$("#JobSearchValidator").text(PoolSearch_InternshipSearchValidatorRequiredError);
			$("#ManualJobTitleDiv").find("label").text(PoolSearch_InternshipTitleInlineLabel);
		}

		//var idList = isJob ? PoolSearch_JobIdArray : PoolSearch_InternshipIdArray;
		var select = $("#OpenJobsDropDownList");
		originalValue = select.val();

		
		var jobOptions = select.data('jobOptions');
		var internshipOptions = select.data('internshipOptions');

	  
		select.empty();
    
		if (isJob) {
			$(jobOptions).each(function() {
				select.append(this);
			});
		} else {
			$(internshipOptions).each(function () {
				select.append(this);
			});
		}

		$("#OpenJobsDropDownList").val(originalValue);

		$("#ManualJobTitleDiv").hide();
		$("#JobSearchValidator").css("visibility", "hidden");

		if (!isLoading) {
			select.prop("selectedIndex", 0);
		} else if (originalValue == "Manual") {
			select.prop("selectedIndex", isJob ? jobOptions.length - 1 : internshipOptions.length - 1);
			$("#ManualJobTitleDiv").show();
	
		}
		/*
    select.empty();

    var options = select.data('options');
    var selectIndex = isJob ? 0 : 1;
    var manualIndex = isJob ? options.length - 2 : options.length - 1;
    $.each(options, function (index) {
      if ($.inArray(this.value, idList) !== -1 || index == selectIndex || index == manualIndex) {
        $(select).append($('<option>').text(this.text).val(this.value));
      }
    });
    */

    
	}
</script>
</asp:PlaceHolder>

