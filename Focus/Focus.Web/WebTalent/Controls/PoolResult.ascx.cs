﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

  #region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Candidate;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Services.ServiceImplementations;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.WebTalent.Controls
{
	/// <summary>
	/// 
	/// </summary>
	public partial class PoolResult : UserControlBase
	{
		#region Page properties

		private Tabs? _tab;
		private int _candidateCount;
		private int _candidatesLikeThisCount;
		private bool _controlBound = false;


		private JobSeekerReferralAllStatusViewDto CandidateApplication
		{
			get { return GetViewStateValue<JobSeekerReferralAllStatusViewDto>("PoolResult:CandidateApplication"); }
			set { SetViewStateValue("PoolResult:CandidateApplication", value); }
		}

		private bool ActiveJobs 
		{ 
			get { return GetViewStateValue<bool>("PoolResult:ActiveJobs"); } 
			set { SetViewStateValue("PoolResult:ActiveJobs", value); } 
		}

		private List<ResumeView> CandidatesInList
		{
			get { return GetViewStateValue<List<ResumeView>>("PoolResult:CandidatesInList"); }
			set { SetViewStateValue("PoolResult:CandidatesInList", value); }
		}

		private List<ResumeView> CandidatesInCandidatesLikeThisList
		{
			get { return GetViewStateValue<List<ResumeView>>("PoolResult:CandidatesInCandidatesLikeThisList"); }
			set { SetViewStateValue("PoolResult:CandidatesInCandidatesLikeThisList", value); }
		}

	  private bool ApprovedUser
	  {
	    get
	    {
        var approvedUser = GetViewStateValue<bool?>("PoolResult:ApprovedUser");
	      if (approvedUser.IsNull())
	      {
	        var status = ServiceClientLocator.EmployeeClient(App)
	          .GetEmployeeByPerson(App.User.PersonId.GetValueOrDefault(0))
	          .ApprovalStatus;

	        approvedUser = (status != ApprovalStatuses.WaitingApproval);
	        SetViewStateValue("PoolResult:ApprovedUser", approvedUser);
	      }

	      return approvedUser.Value;
	    }
	  }

    private bool ShowEmailIcons
	  {
      get
      {
        var show = GetViewStateValue<bool?>("PoolResult:ShowEmailIcons");
        if (show.IsNull())
        {
          var showEmail = (JobId == 0 || JobStatus == JobStatuses.Active) && ActiveJobs;
          if (showEmail)
          {
            showEmail = ApprovedUser;
          }

          SetViewStateValue("PoolResult:ShowEmailIcons", showEmail);
          return showEmail;
        }

        return show.Value;
      }
    }

		private const string FlaggedText = "Flagged";
		private const string ApplicantsText = "Applicants";
		private const string OpenText = "Open";
		private const string InviteesText = "Invitees";

		/// <summary>
		/// Gets or sets the search session.
		/// </summary>
		/// <value>The search session.</value>
		public string SearchSession { get; set; }

		/// <summary>
		/// Gets or sets the saved search id.
		/// </summary>
		/// <value>The saved search id.</value>
		public long SavedSearchId { get; set; }

		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		/// <value>The job id.</value>
		protected long JobId
		{
			get { return GetViewStateValue<long>("PoolResult:JobId"); }
			set { SetViewStateValue("PoolResult:JobId", value); }
		}

    /// <summary>
    /// Gets or sets the job status.
    /// </summary>
    /// <value>The job id.</value>
    protected JobStatuses JobStatus
    {
      get { return GetViewStateValue<JobStatuses>("PoolResult:JobStatus"); }
      set { SetViewStateValue("PoolResult:JobStatus", value); }
    }

		/// <summary>
		/// Gets or sets the candidates like this person id.
		/// </summary>
		/// <value>The candidates like this person id.</value>
		protected long CandidatesLikeThisPersonId
		{
			get { return GetViewStateValue<long>("PoolResult:CandidatesLikeThisPersonId"); }
			set { SetViewStateValue("PoolResult:CandidatesLikeThisPersonId", value); }
		}

		/// <summary>
		/// Gets or sets the highest match rating.
		/// </summary>
		/// <value>
		/// The highest match rating.
		/// </value>
		private int HighestMatchRating
		{
			get { return GetViewStateValue<int>("PoolResult:HighestMatchRating"); }
			set { SetViewStateValue("PoolResult:HighestMatchRating", value); }
		}

		/// <summary>
		/// Gets or sets the search identifier.
		/// </summary>
		/// <value>
		/// The search identifier.
		/// </value>
		private Guid SearchId
		{
			get { return GetViewStateValue<Guid>("PoolResult:SearchId"); }
			set { SetViewStateValue("PoolResult:SearchId", value); }
		}

	/// <summary>
		/// Gets or sets the tab.
		/// </summary>
		/// <value>The tab.</value>
		internal Tabs Tab
		{
			get
			{
				if (_tab == null)
				{
					var tab = Tabs.AllResumes;

					if (Page.RouteData.Values.ContainsKey("tab"))
						Enum.TryParse(Page.RouteData.Values["tab"].ToString(), true, out tab);

					_tab = tab;
				}

				return _tab.Value;
			}
		}

		/// <summary>
		/// Gets or sets the application id.
		/// </summary>
		/// <value>
		/// The application id.
		/// </value>
		protected long ApplicationId
		{
			get { return GetViewStateValue<long>("PoolResult:ApplicationId"); }
			set { SetViewStateValue("PoolResult:ApplicationId", value); }
		}

		/// <summary>
		/// Gets or sets the application lock version.
		/// </summary>
		/// <value>
		/// The application lock version.
		/// </value>
		protected int ApplicationLockVersion
		{
			get { return GetViewStateValue<int>("PoolResult:ApplicationLockVersion"); }
			set { SetViewStateValue("PoolResult:ApplicationLockVersion", value); }
		}

		#endregion

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			ReferralRequestSent.OkCommand += ReferralRequestSent_OkCommand;
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

			if (!IsPostBack)
			{
				if (!_controlBound) Bind();
				BuildListTabs();
			}

			// Hide or show the instructions panel, if the data source has select parameters a search has been run
			InstructionsPanel.Visible = (SearchResultDataSource.SelectParameters.Count == 0);
			SearchResultListView.Visible = SearchSummarySectionPanel.Visible = (SearchResultDataSource.SelectParameters.Count > 0);
			
			TabAllResumes.Attributes["class"] = (Tab == Tabs.AllResumes ? "active" : "");
			TabApplicants.Attributes["class"] = (Tab == Tabs.Applicants ? "active" : "");
			TabFlaggedResumes.Attributes["class"] = (Tab == Tabs.FlaggedResumes ? "active" : "");
			TabInvitees.Attributes["class"] = (Tab == Tabs.Invitees ? "active" : "");

			TabApplicants.Visible = TabInvitees.Visible = (JobId > 0);

			CandidatesLikeThisModalCloseButton.AlternateText = CodeLocalise("Global.Close.Text", "Close");

   			ApplyBranding();
			
			Localise();
		}

		/// <summary>
		/// Searches the specified search criteria.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		internal void Search(CandidateSearchCriteria searchCriteria)
		{
			JobId = (searchCriteria.JobDetailsCriteria.IsNotNull()) ? searchCriteria.JobDetailsCriteria.JobId : 0;
			ActiveJobs = ServiceClientLocator.JobClient(App).GetJobs(JobStatuses.Active, Convert.ToInt64(App.User.EmployeeId)).Any();

            if (JobId > 0)
            {
                JobStatus = ServiceClientLocator.JobClient(App).GetJobStatus(JobId);
            }

			SearchResultListClear();

			// Only search if scope is open otherwise we will get information from database
			string searchId;
			
			if (searchCriteria.ScopeType != CandidateSearchScopeTypes.Flagged)
			{
				var results = ServiceClientLocator.SearchClient(App).SearchCandidates(Guid.Empty, searchCriteria);
				HighestMatchRating = results.HighestStarRating;
                searchId = (results.SearchId != Guid.Empty) ? results.SearchId.ToString() : "";
				SearchId = results.SearchId;
			}
			else
			{
				searchId = "New";
			}

			SearchResultDataSource.SelectParameters.Clear();
			var searchIdParameter = new Parameter("searchId", TypeCode.String, searchId);
			SearchResultDataSource.SelectParameters.Add(searchIdParameter);
			var jobIdParameter = new Parameter("jobId", TypeCode.Int64, JobId.ToString());
			SearchResultDataSource.SelectParameters.Add(jobIdParameter);
			var searchScopeParameter = new Parameter("searchScope", TypeCode.String, SearchScopeToString(searchCriteria.ScopeType));
			SearchResultDataSource.SelectParameters.Add(searchScopeParameter);
			
			SearchResultListPager.ReturnToFirstPage();
			SearchCriteriaSummary.UpdateSearchCriteria(searchCriteria);


			Bind();
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the candidates.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<ResumeView> GetCandidates(string orderBy, int startRowIndex, int maximumRows)
		{
			return GetCandidates(string.Empty, 0, orderBy, string.Empty, startRowIndex, maximumRows);
		}

		/// <summary>
		/// Gets the candidates.
		/// </summary>
		/// <param name="searchId">The search id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="searchScope">The search scope.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<ResumeView> GetCandidates(string searchId, long jobId, string searchScope, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int) Math.Floor((double)startRowIndex/maximumRows));
			if (searchId.IsNullOrEmpty()) return null;

			PagedList<ResumeView> candidates;

			switch (searchScope)
			{
				case FlaggedText:
					candidates = ServiceClientLocator.CandidateClient(App).GetFlaggedCandidates(new FlaggedCandidateCriteria
					{
					  PageIndex = pageIndex, 
            PageSize = maximumRows, 
            OrderBy = orderBy, 
            JobId = jobId
					});
					break;

				case ApplicantsText:
					candidates = ServiceClientLocator.CandidateClient(App).GetApplicantResumeViews(new ResumeViewCriteria
					                                                                 	{
					                                                                 		JobId = jobId,
																																							ApprovalStatus = ApprovalStatuses.Approved,
					                                                                 		FlagPersonId = (App.User.PersonId.HasValue) ? App.User.PersonId.Value : 0,
					                                                                 		PageIndex = pageIndex,
					                                                                 		PageSize = maximumRows,
					                                                                 		OrderBy = orderBy,
                                                                              ExcludeNonVeterans = true
					                                                                 	});
					break;

				case InviteesText:
					candidates = ServiceClientLocator.CandidateClient(App).GetInviteesForJob(new InviteesResumeViewCriteria
																																						{
																																							JobId = jobId,
																																							PageIndex = pageIndex
																																						});
					break;
					
				default:
					var jobCriteria = new CandidateSearchJobDetailsCriteria { JobId = jobId };

					try
					{
						var result = ServiceClientLocator.SearchClient(App).SearchCandidates(Guid.Parse(searchId), new CandidateSearchCriteria { PageIndex = pageIndex, PageSize = maximumRows, OrderBy = orderBy, JobDetailsCriteria = jobCriteria });
						candidates = result.Candidates;
					}
					catch
					{
						return null;
					}
					break;
			}

			_candidateCount = (candidates.IsNotNull()) ? candidates.TotalCount : 0;

			return candidates;
		}

		/// <summary>
		/// Gets the candidate count.
		/// </summary>
		/// <returns></returns>
		public int GetCandidateCount()
		{
			return GetCandidateCount(string.Empty, 0, string.Empty);
		}

		/// <summary>
		/// Gets the candidate count.
		/// </summary>
		/// <param name="searchId">The search id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="searchScope">The search scope.</param>
		/// <returns></returns>
		public int GetCandidateCount(string searchId, long jobId, string searchScope)
		{
			return _candidateCount;
		}

		/// <summary>
		/// Gets the candidates like this.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<ResumeView> GetCandidatesLikeThis(string orderBy, int startRowIndex, int maximumRows)
		{
			return GetCandidatesLikeThis(String.Empty, 0, orderBy, startRowIndex, maximumRows);
		}

		/// <summary>
		/// Gets the candidates like this.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<ResumeView> GetCandidatesLikeThis(string personId, long jobId, string orderBy, int startRowIndex, int maximumRows, bool logSearch = true)
		{
			if (personId.IsNullOrEmpty()) return null;

			CandidateSearchResultModel result;

			try
			{
				var searchCriteria = new CandidateSearchCriteria
				{
					SearchType = CandidateSearchTypes.CandidatesLikeThis,
					PageSize = 4,
					CandidatesLikeThisSearchCriteria = new CandidatesLikeThisCriteria {CandidateId = personId}
				};

				if(jobId > 0)
					searchCriteria.JobDetailsCriteria = new CandidateSearchJobDetailsCriteria{ JobId = jobId};

				searchCriteria.LogSearch = logSearch;

				result = ServiceClientLocator.SearchClient(App).SearchCandidates(Guid.Empty, searchCriteria);
			}
			catch
			{
				return null;
			}

			var candidates = result.Candidates;
			_candidatesLikeThisCount = candidates.TotalCount;

			return candidates;
		}

		/// <summary>
		/// Gets the candidates like this count.
		/// </summary>
		/// <returns></returns>
		public int GetCandidatesLikeThisCount()
		{
			return _candidatesLikeThisCount;
		}

		/// <summary>
		/// Gets the candidates like this count.
		/// </summary>
		/// <param name="personId">The pewrson id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="logSearch">if set to <c>true</c> [log search].</param>
		/// <returns></returns>
		public int GetCandidatesLikeThisCount(string personId, long jobId, bool logSearch = true)
		{
			return _candidatesLikeThisCount;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the ItemDataBound event of the SearchResultListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void SearchResultListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var candidate = (ResumeView)e.Item.DataItem;

			#region Name / Id

			var nameLink = (LinkButton) e.Item.FindControl("SearchResultNameLink");
      nameLink.Text = candidate.Name != "" ? candidate.Name : CodeLocalise("Unknown.Text", "Unknown");

			#endregion

      #region Email Image

		  if ((App.Settings.Theme == FocusThemes.Workforce) || (App.Settings.Theme == FocusThemes.Education && !ApprovedUser))
		  {
		    var emailDiv = (HtmlGenericControl) e.Item.FindControl("EmailDiv");
		    emailDiv.Visible = ShowEmailIcons;
		  }

		  #endregion

      #region Flagged Image

      var flagDiv = (HtmlGenericControl) e.Item.FindControl("FlagDiv");
      BindFlaggedImage(flagDiv, candidate.Flagged);
			
			#endregion

			#region Veteran Image

			var veteranImage = (Image)e.Item.FindControl("VeteranImage");
			if (candidate.IsVeteran) BindVeteranImage(veteranImage);

			#endregion

			#region Score

			var scoreImage = (Image)e.Item.FindControl("SearchResultScoreImage");
			BindScore(scoreImage, candidate.Score);
			if (Tab == Tabs.FlaggedResumes) scoreImage.ToolTip = CodeLocalise("NoScoringPossible.Text", "No scoring possible");
			
			#endregion

			#region Branding

			if (App.Settings.ShowBrandingStatement)
			{
				var brandingLabel = (Label) e.Item.FindControl("SearchResultBrandingLabel");
				BindBranding(brandingLabel, candidate.Branding);
			}

			#endregion

			#region Employment History

			var employmentHistoryLiteral = (Literal)e.Item.FindControl("SearchResultEmploymentHistoryLiteral");
			BindEmploymentHistory(employmentHistoryLiteral, candidate.EmploymentHistories, candidate.Branding);

			#endregion

			#region Application Status

			if (JobId > 0 && candidate.Status != ApplicationStatusTypes.NotApplicable && candidate.Status != ApplicationStatusTypes.SelfReferred && candidate.ApplicationApprovalStatus == ApprovalStatuses.Approved && Tab == Tabs.Applicants)
			{
				var applicationStatusDropDownList = (DropDownList) e.Item.FindControl("SearchResultApplicationStatusDropDown");
				BindStatus(applicationStatusDropDownList, candidate.Status);
				applicationStatusDropDownList.Visible = true;
			}

            //FVN-6215 - Referral outcomes RefusedReferral , FoundJobFromOtherSource and NotYetPlaced  should be editable by Talent users so commented the below lines
            ////else if (JobId > 0 && candidate.Status == ApplicationStatusTypes.RefusedReferral || candidate.Status == ApplicationStatusTypes.FoundJobFromOtherSource || candidate.Status == ApplicationStatusTypes.NotYetPlaced)
            ////{
            ////    var applicationStatusDropDownList = (DropDownList)e.Item.FindControl("SearchResultApplicationStatusDropDown");
            ////    applicationStatusDropDownList.Visible = false;

            ////    var applicationStatusLiteral = (Literal)e.Item.FindControl("SearchResultApplcationStatusLiteral");
            ////    applicationStatusLiteral.Visible = true;

            ////    var labelText = string.Empty;

            ////    switch (candidate.Status)
            ////    {
            ////        case ApplicationStatusTypes.RefusedReferral:
            ////            labelText = "Refused referral";
            ////            break;

            ////        case ApplicationStatusTypes.FoundJobFromOtherSource:
            ////            labelText = "Found job from other source";
            ////            break;
            ////        case ApplicationStatusTypes.NotYetPlaced:
            ////            labelText = "Not yet placed";
            ////            break;
            ////    }

            ////    applicationStatusLiteral.Text = CodeLocalise("ApplicationStatus.Text", labelText);
            ////}

			#endregion

            #region Invite Queued Status
            var inviteStatusLiteral = (Literal)e.Item.FindControl("SearchResultInviteesQueuedStatusLiteral");
            inviteStatusLiteral.Text = CodeLocalise("InviteStatusLiteral", "Invitation is pending release of Veteran Priority of Services exclusion");
            if(!candidate.IsVeteran && Tab == Tabs.Invitees && candidate.IsInviteQueued)
                inviteStatusLiteral.Visible = true;

            #endregion

            #region Employer Notes

            var notesTextBox = (TextBox)e.Item.FindControl("NotesTextBox");
			notesTextBox.Text = candidate.EmployerNote;

			var noteDiv = (HtmlGenericControl) e.Item.FindControl("NoteDiv");
			if (candidate.EmployerNote.IsNotNullOrEmpty()) BindNoteAvailableImage(noteDiv);

			#endregion

			#region Register Link buttons Async Callback

			//var searchResultEmailButton = (LinkButton)e.Item.FindControl("SearchResultEmailButton");
			//var searchResultCandidatesLikeThisButton = (LinkButton)e.Item.FindControl("SearchResultCandidatesLikeThisButton");

			//GlobalScriptManager.RegisterAsyncPostBackControl(searchResultEmailButton);
			//GlobalScriptManager.RegisterAsyncPostBackControl(searchResultCandidatesLikeThisButton);

			#endregion

			#region Location

			var locationSpan = (HtmlGenericControl)e.Item.FindControl("LocationSpan");
			locationSpan.Visible = App.Settings.ShowHomeLocationInSearchResults;
				
			#endregion

			#region Years experience

			var yearsExperienceSpan = (HtmlGenericControl)e.Item.FindControl("YearsExperienceSpan");
			yearsExperienceSpan.Visible = !App.Settings.HideYearsOfExpInSearchResults;

			#endregion

			#region Invite creation date

			var dateInviteCreatedSpan = (HtmlGenericControl) e.Item.FindControl("InviteCreatedOnSpan");
			dateInviteCreatedSpan.Visible = (_tab == Tabs.Invitees);
			
			#endregion

			#region NCRC Level

			var ncrcImage = (Image) e.Item.FindControl("NcrcImage");
			BindNcrcImage(ncrcImage, candidate.NcrcLevel, candidate.NcrcLevelId);

			#endregion

		}

		/// <summary>
		/// Handles the ItemDataBound event of the CandidatesLikeThisListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var candidate = (ResumeView)e.Item.DataItem;

			#region Name / Id

			var nameLink = (LinkButton)e.Item.FindControl("CandidatesLikeThisNameLink");
			nameLink.OnClientClick = string.Format("return ResumeLinkButtonClicked(this, {0});", candidate.Id);
			nameLink.Text = candidate.Name;

			#endregion

			#region Flagged Image

      var flagDiv = (HtmlGenericControl)e.Item.FindControl("CandidatesLikeThisFlagDiv");
      BindFlaggedImage(flagDiv, candidate.Flagged);


			#endregion

			#region Veteran Image

			var veteranImage = (Image)e.Item.FindControl("CandidatesLikeThisVeteranImage");
			if (candidate.IsVeteran) BindVeteranImage(veteranImage);

			#endregion

			#region Score

			var scoreImage = (Image)e.Item.FindControl("CandidatesLikeThisScoreImage");
			BindScore(scoreImage, candidate.Score);

			#endregion

			#region Branding

			if (App.Settings.ShowBrandingStatement)
			{
				var brandingLiteral = (Literal)e.Item.FindControl("CandidatesLikeThisBrandingLiteral");
				brandingLiteral.Text = candidate.Branding;
			}

			#endregion

			#region Employment History

			var employmentHistoryLiteral = (Literal)e.Item.FindControl("CandidatesLikeThisEmploymentHistoryLiteral");
			BindEmploymentHistory(employmentHistoryLiteral, candidate.EmploymentHistories, candidate.Branding);

			#endregion

			#region Application Status

			// var applicationStatusDropDownList = (DropDownList)e.Item.FindControl("CandidatesLikeThisApplicationStatusDropDownList");
			// BindStatus(applicationStatusDropDownList, candidate.Status);
			
			#endregion

			#region Employer Notes

			var notesTextBox = (TextBox)e.Item.FindControl("CandidatesLikeThisNotesTextBox");
			notesTextBox.Text = candidate.EmployerNote;

      var noteDiv = (HtmlGenericControl)e.Item.FindControl("CandidatesLikeThisNoteDiv");
      if (candidate.EmployerNote.IsNotNullOrEmpty()) BindNoteAvailableImage(noteDiv);

			#endregion

			#region Highlight Original Candidate

			if (candidate.Id == CandidatesLikeThisPersonId)
			{
				var topTableRow = (TableRow)e.Item.FindControl("CandidatesLikeThisItemTopTableRow");
			  topTableRow.CssClass = "topRowHighlighted";
				var bottomTableRow = (TableRow)e.Item.FindControl("CandidatesLikeThisItemBottomTableRow");
			  bottomTableRow.CssClass = "bottomRowHighlighted";
			}

			#endregion

      #region Email

      if ((App.Settings.Theme == FocusThemes.Workforce) || (App.Settings.Theme == FocusThemes.Education && !ApprovedUser))
		  {
		    var emailDiv = (HtmlGenericControl) e.Item.FindControl("CandidatesLikeThisEmailDiv");
		    emailDiv.Visible = ShowEmailIcons;
		  }

		  #endregion

			#region Location

			if (App.Settings.ShowHomeLocationInSearchResults)
			{
				var locationSpan = (HtmlGenericControl) e.Item.FindControl("CandidatesLikeThisLocationSpan");
				locationSpan.Visible = true;
			}

			#endregion

			#region Years experience

			if (App.Settings.HideYearsOfExpInSearchResults)
			{
				var yearsExperienceSpan = (HtmlGenericControl) e.Item.FindControl("CandidatesLikeThisYearsExperienceSpan");
				yearsExperienceSpan.Visible = false;
			}

			#endregion

		  #region Register Link buttons Async Callback

		  //var candidatesLikeThisEmailButton = (LinkButton)e.Item.FindControl("CandidatesLikeThisEmailButton");
		  //var candidatesLikeThisCandidatesLikeThisButton = (LinkButton)e.Item.FindControl("CandidatesLikeThisCandidatesLikeThisButton");

		  //GlobalScriptManager.RegisterAsyncPostBackControl(candidatesLikeThisEmailButton);
		  //GlobalScriptManager.RegisterAsyncPostBackControl(candidatesLikeThisCandidatesLikeThisButton);

		  #endregion

			#region NCRC Level

			var ncrcImage = (Image)e.Item.FindControl("CandidatesLikeThisNcrcImage");
			BindNcrcImage(ncrcImage, candidate.NcrcLevel, null);

			#endregion

		}

		/// <summary>
		/// Handles the Command event of the SearchResultRowAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void SearchResultRowAction_Command(object sender, CommandEventArgs e)
		{
			var rowNo = Convert.ToInt32(e.CommandArgument);
			var id = Convert.ToInt64(SearchResultListView.DataKeys[rowNo]["Id"]);
			var name = SearchResultListView.DataKeys[rowNo]["Name"].ToString();
			var applicationStatus = (ApplicationStatusTypes)Enum.Parse(typeof(ApplicationStatusTypes), SearchResultListView.DataKeys[rowNo]["Status"].ToString());
			var approvalStatus = (ApprovalStatuses)Enum.Parse(typeof(ApprovalStatuses), SearchResultListView.DataKeys[rowNo]["ApplicationApprovalStatus"].ToString());
            this.Page.Title = this.Page.Title + "-" + id;
			ResumeView candidate;

			switch (e.CommandName)
			{
				case "CandidatesLikeThis":
					ShowCandidatesLikeThisModal(id);
					Bind();
					break;

				case "Email":
					
					// Check whether the candidate is an applicant because applicants get a different email panel
					var isApplicant = (applicationStatus != ApplicationStatusTypes.NotApplicable);
                    this.Page.Title = "Email -" + id;
					candidate = CandidatesInList.FirstOrDefault(x => x.Id == id);

					if (App.Settings.Theme == FocusThemes.Education || approvalStatus == ApprovalStatuses.Approved ||
						  approvalStatus == ApprovalStatuses.None || approvalStatus == ApprovalStatuses.OnHold)
					{
            PoolAction.Show(ActionTypes.EmailCandidate, id, name, applicationStatus, candidate, JobId, ActiveJobs);
					}
					else
					{
						ApplicationId = candidate.ApplicationId;
						ApplicationLockVersion = candidate.ApplicationLockVersion;

						var title = string.Empty;
						var details = string.Empty;

						if (approvalStatus == ApprovalStatuses.WaitingApproval || approvalStatus == ApprovalStatuses.Reconsider)
						{
							title = CodeLocalise("TitleResumeReviewInProgress.Text", "Resume review in progress");
							details = CodeLocalise("DetailsResumeReviewInProgress.Text",
								"The job seeker you have chosen to contact has already submitted a referral request to your position which is currently under review by staff. By clicking Confirm Interest, the job seeker will be notified that their referral request has been successful and they will be displayed on your Applicants tab. If you abandon this action, the job seeker's request will remain under review.");
						}
						else if (approvalStatus == ApprovalStatuses.Rejected)
						{
							title = CodeLocalise("TitleResumeReviewAlreadyTakenPlace.Text", "Resume review already taken place");
							details = CodeLocalise("DetailsResumeReviewAlreadyTakenPlace.Text",
								"The job seeker you have chosen to contact has already submitted a referral request to your position which was denied by staff. By clicking Confirm Interest, the job seeker will be notified that the decision regarding their referral request has been reversed and they will be displayed on your Applicants tab. If you abandon this action, the job seeker's request will remain as denied.");
						}

						var closeText = CodeLocalise("CloseText.Text", "Cancel");
						var okText = CodeLocalise("OkText.Text", "Confirm Interest");
						ReferralRequestSent.Show(title, details, closeText, okText: okText, okCommandName: "Confirm");
					

						Bind();
					}
					break;
				
				case "Note":
                    this.Page.Title = "Note -" + id;
					var candidateNotePanel = (Panel) SearchResultListView.Items[rowNo].FindControl("NotePanel");
					if (candidateNotePanel.IsNotNull()) candidateNotePanel.Visible = true;
					break;

				case "SaveNote":
					var notesTextBox = (TextBox)SearchResultListView.Items[rowNo].FindControl("NotesTextBox");
                    this.Page.Title = "Save Note -" + id;
					if (notesTextBox.IsNotNull())
					{
						ServiceClientLocator.CandidateClient(App).SavePersonNote(id, App.User.EmployerId, notesTextBox.TextTrimmed());
						Confirmation.Show(CodeLocalise("SaveNoteConfirmation.Title", "Note saved"),
						                  CodeLocalise("SaveNoteConfirmation.Body", "Note has been saved to candidate."),
						                  CodeLocalise("Global.Close.Text", "Close"));
					}
					Bind();
					break;

				case "CloseNote":
					Bind();
					break;

				case "Preview":
					candidate = CandidatesInList.FirstOrDefault(x => x.Id == id);
          if (candidate.IsNotNull()) ResumePreview.Show(candidate, JobId, ShowEmailIcons, ApprovedUser);
					Bind();
					break;

				case "Flag":
					candidate = CandidatesInList.FirstOrDefault(x => x.Id == id);
                    this.Page.Title = "Flag -" + id;

					if(candidate.IsNotNull()) ServiceClientLocator.CandidateClient(App).ToggleCandidateFlag(candidate.Id);
					Bind();
					break;
			}
		}

		/// <summary>
		/// Handles the OkCommand event of the ReferralRequestSent control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		public void ReferralRequestSent_OkCommand(object sender, CommandEventArgs e)
		{
			var command = e.CommandName;

			if (command == "Confirm")
			{
				var message = ServiceClientLocator.CandidateClient(App).ApproveCandidateReferral(ApplicationId, ApplicationLockVersion, true);

				if (message.Length == 0)
				{
					CandidateApplication = ServiceClientLocator.CandidateClient(App).GetJobSeekerReferral(ApplicationId);
					var emailTemplate = GetApproveCandidateReferralEmail();

					ServiceClientLocator.CandidateClient(App).SendCandidateEmail(CandidateApplication.CandidateId, emailTemplate.Subject, emailTemplate.Body, true);
				}
				else
				{
					Confirmation.Show(CodeLocalise("ReferralApprovedInvalid.Title", "Referral failed"),
																 CodeLocalise("ReferralApprovedInvalid.Body", string.Format("There was a problem approving the referral.<br /><br />{0}", message)),
																 CodeLocalise("CloseModal.Text", "OK"));
				}
			}
		}

		/// <summary>
		/// Gets the approve candidate referral email.
		/// </summary>
		/// <returns></returns>
		private EmailTemplateView GetApproveCandidateReferralEmail()
		{
			var templateValues = new EmailTemplateData
			{
				RecipientName = CandidateApplication.Name,
				JobTitle = CandidateApplication.JobTitle,
				EmployerName = CandidateApplication.EmployerName
			};

			return ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.AutoApprovedJobSeekerReferral, templateValues);
		}

		/// <summary>
		/// Handles the Selected event of the SearchResultDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
		protected void SearchResultDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
		{
		  var candidates = e.ReturnValue as List<ResumeView>;
		  if (candidates.IsNotNull())
		    CandidatesInList = candidates;
		}

		/// <summary>
		/// Handles the Selected event of the CandidatesLikeThisDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
		{
			try
			{
				// Save the list to view state so it can be accessed on post back
				CandidatesInCandidatesLikeThisList = (List<ResumeView>)e.ReturnValue;
			}
			catch { }
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the SearchResultApplicationStatusDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SearchResultApplicationStatusDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var statusDropDown = (DropDownList) sender;
			var listViewDataItem = (ListViewDataItem)statusDropDown.Parent;
			var personId = Convert.ToInt64(SearchResultListView.DataKeys[listViewDataItem.DisplayIndex]["Id"]);

			ServiceClientLocator.CandidateClient(App).UpdateApplicationStatus(personId, JobId, statusDropDown.SelectedValueToEnum<ApplicationStatusTypes>());

			Confirmation.Show(CodeLocalise("UpdateApplicationStatusConfirmation.Title", "Application updated"),
															CodeLocalise("UpdateApplicationStatusConfirmation.Body", "The status of the application has been updated."),
															CodeLocalise("Global.Close.Text", "Close"));
		}

		/// <summary>
		/// Handles the Command event of the CandidatesLikeThisRowAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisRowAction_Command(object sender, CommandEventArgs e)
		{
			var rowNo = Convert.ToInt32(e.CommandArgument);
			var id = Convert.ToInt64(CandidatesLikeThisListView.DataKeys[rowNo]["Id"]);
			var name = CandidatesLikeThisListView.DataKeys[rowNo]["Name"].ToString();
			var applicationStatus = (ApplicationStatusTypes)Enum.Parse(typeof(ApplicationStatusTypes), CandidatesLikeThisListView.DataKeys[rowNo]["Status"].ToString());
			
			switch (e.CommandName)
			{
				case "CandidatesLikeThis":
					ShowCandidatesLikeThisModal(id);
					Bind();
					break;
				case "Email":
          var emailcandidate = CandidatesInCandidatesLikeThisList.FirstOrDefault(x => x.Id == id);
          PoolAction.Show(ActionTypes.EmailCandidate, id, name, applicationStatus, emailcandidate, JobId, ActiveJobs);
					Bind();
					break;
				case "Note":
					ShowCandidatesLikeThisModal(CandidatesLikeThisPersonId, false);
					var candidateNotePanel = (Panel)CandidatesLikeThisListView.Items[rowNo].FindControl("CandidatesLikeThisNotePanel");
					if (candidateNotePanel.IsNotNull()) candidateNotePanel.Visible = true;
					break;
				case "SaveNote":
					var notesTextBox = (TextBox)CandidatesLikeThisListView.Items[rowNo].FindControl("CandidatesLikeThisNotesTextBox");
					if (notesTextBox.IsNotNull())
					{
						ServiceClientLocator.CandidateClient(App).SavePersonNote(id, App.User.EmployerId, notesTextBox.TextTrimmed());
						Confirmation.Show(CodeLocalise("SaveNoteConfirmation.Title", "Note saved"),
															CodeLocalise("SaveNoteConfirmation.Body", "Note has been saved to candidate."),
															CodeLocalise("Global.Close.Text", "Close"));
					}
					ShowCandidatesLikeThisModal(CandidatesLikeThisPersonId, false);
					Bind();
					break;
				case "CloseNote":
					ShowCandidatesLikeThisModal(CandidatesLikeThisPersonId, false);
					Bind();
					break;
				case "Flag":
					var candidate = CandidatesInCandidatesLikeThisList.FirstOrDefault(x => x.Id == id);
					if(candidate.IsNotNull()) ServiceClientLocator.CandidateClient(App).ToggleCandidateFlag(candidate.Id);
					ShowCandidatesLikeThisModal(CandidatesLikeThisPersonId, false);
					Bind();
					break;
			}
		}

		/// <summary>
		/// Handles the Completed event of the PoolAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PoolAction_Completed(object sender, EventArgs e)
		{
			Bind();

			if(CandidatesLikeThisDataSource.SelectParameters.Count > 0)
			{
				CandidatesLikeThisListView.DataBind();
				CandidatesLikeThisModalPopup.Show();
			}
		}

		/// <summary>
		/// Handles the Click event of the CandidatesLikeThisModalCloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisModalCloseButton_Click(object sender, EventArgs e)
		{
			CandidatesLikeThisDataSource.SelectParameters.Clear();
			CandidatesLikeThisListView.DataSource = null;
			CandidatesLikeThisListView.DataBind();
			CandidatesLikeThisModalPopup.Hide();

			Bind();
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the SortByDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SortByDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var poolResultSortByslookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PoolResultSortBys);
			var sortByDropDownSelectedValue = SortByDropDown.SelectedValueToLong();

			if(sortByDropDownSelectedValue > 0)
			{
				foreach (var lookUpItem in poolResultSortByslookUps.Where(lookUpItem => lookUpItem.Id == sortByDropDownSelectedValue))
				{
					SearchResultListView.Sort(lookUpItem.Key, SortDirection.Ascending);
					break;
				}
			}
			else
			{
				SearchResultListView.Sort(string.Empty, SortDirection.Ascending);
			}
			Bind();
		}

		/// <summary>
		/// Handles the Email Candidate Click event of the ResumePreviewModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
		protected void ResumePreviewModalEmailCandidate_Click(object sender, ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs e)
		{
      PoolAction.Show(ActionTypes.EmailCandidate, e.CandidateId, e.CandidateName, e.ApplicationStatus, e.Candidate, JobId, ActiveJobs);
		}

		/// <summary>
		/// Handles the Email Candidate Click event of the ResumePreviewModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
		protected void ResumePreviewModalEmailResumeCandidate_Click(object sender, ResumePreviewModal.ResumePreviewEmailResumeCandidateClickEventArgs e)
		{
			EmailResume.Show(e.ResumeId,true);
		}


		/// <summary>
		/// Handles the Candidates Like Me Click event of the ResumePreview control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ResumePreviewModal.ResumePreviewCandidatesLikeMeEventArgs"/> instance containing the event data.</param>
		protected void ResumePreviewCandidatesLikeMe_Click(object sender, ResumePreviewModal.ResumePreviewCandidatesLikeMeEventArgs e)
		{
			ShowCandidatesLikeThisModal(e.CandidateId);
		}

		/// <summary>
		/// Handles the Toggled event of the ResumePreviewFlag control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ResumePreviewFlag_Toggled(object sender, EventArgs e)
		{
			Bind();
		}

    /// <summary>
    /// Handles the Saved event of the ResumePreviewNote control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ResumePreviewNote_Saved(object sender, EventArgs e)
    {
      Bind();
    }

		/// <summary>
		/// Handles the OnClick event of the InviteMultipleButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		/// <exception cref="System.NotImplementedException"></exception>
		protected void InviteMultipleButton_OnClick(object sender, EventArgs e)
		{
			PoolAction.Show(ActionTypes.EmailCandidates, SearchId, JobId);
			Bind();
		}

		#endregion

		#region List binding methods

		/// <summary>
		/// Clears the candidates from the list.
		/// </summary>
		private void SearchResultListClear()
		{
			SearchResultListView.DataSource = null;
			SearchResultListView.DataBind();
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private static void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = OldApp_RefactorIfFound.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString();

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

    /// <summary>
    /// Binds the flagged image.
    /// </summary>
    /// <param name="flagDiv">The flag div.</param>
    /// <param name="flagged">Whether the candidate is flagged</param>
    private void BindFlaggedImage(HtmlControl flagDiv, bool flagged)
    {
      flagDiv.Attributes["class"] = flagged ? "tooltip left Flagged" : "tooltip left Flag";
    }

		/// <summary>
		/// Binds the veteran image.
		/// </summary>
		/// <param name="veteranImage">The veteran image.</param>
		private void BindVeteranImage(Image veteranImage)
		{
			veteranImage.ToolTip = veteranImage.AlternateText = CodeLocalise("VeteranImage.ToolTip", "Veteran");
			veteranImage.ImageUrl = UrlBuilder.VeteranImage();
			veteranImage.Visible = true;
			// apply tabindex here rather than in the designer as if we apply a TabIndex="0" attribute in the designer, 
			// it is ignored at runtime and not rendered!
			veteranImage.Attributes.Add("tabindex", "0");
		}

		/// <summary>
		/// Binds the NCRC image.
		/// </summary>
		/// <param name="ncrcImage">The NCRC icon image.</param>
		/// <param name="ncrcLevel">The candidate's NCRC level.</param>
		/// <param name="ncrcLevelId">The (lokup) ID for the candidates NCRC level.</param>
		private void BindNcrcImage(Image ncrcImage, NcrcLevelTypes? ncrcLevel, long? ncrcLevelId)
		{
            if (App.Settings.Theme != FocusThemes.Workforce || ((ncrcLevel == NcrcLevelTypes.None || ncrcLevel.IsNull()) && ncrcLevelId.IsNullOrZero()) || !App.Settings.EnableNCRCFeatureGroup) 
        return;

			ncrcImage.Visible = true;
			if (ncrcLevel == NcrcLevelTypes.Bronze || ncrcLevelId == GetLookupId("NCRCLevel.Bronze"))
			{
				ncrcImage.ImageUrl = UrlBuilder.NcrcBronzeIconSmall();
				ncrcImage.AlternateText = ncrcImage.ToolTip = CodeLocalise("Ncrc.Bronze.Tooltip", "Bronze NCRC");
			}
			else if (ncrcLevel == NcrcLevelTypes.Silver || ncrcLevelId == GetLookupId("NCRCLevel.Silver"))
			{
				ncrcImage.ImageUrl = UrlBuilder.NcrcSilverIconSmall();
				ncrcImage.AlternateText = ncrcImage.ToolTip = CodeLocalise("Ncrc.Silver.Tooltip", "Silver NCRC");
			}
			else if (ncrcLevel == NcrcLevelTypes.Gold || ncrcLevelId == GetLookupId("NCRCLevel.Gold"))
			{
				ncrcImage.ImageUrl = UrlBuilder.NcrcGoldIconSmall();
				ncrcImage.AlternateText = ncrcImage.ToolTip = CodeLocalise("Ncrc.Gold.Tooltip", "Gold NCRC");
			}
			else if (ncrcLevel == NcrcLevelTypes.Platinum || ncrcLevelId == GetLookupId("NCRCLevel.Platinum"))
			{
				ncrcImage.ImageUrl = UrlBuilder.NcrcPlatinumIconSmall();
				ncrcImage.AlternateText = ncrcImage.ToolTip = CodeLocalise("Ncrc.Platinum.Tooltip", "Platinum NCRC");
			}
		}

		/// <summary>
		/// Binds the note available image.
		/// </summary>
		/// <param name="noteDiv">The note div.</param>
		private void BindNoteAvailableImage(HtmlControl noteDiv)
		{
		  noteDiv.Attributes["class"] = "tooltip left Noted";
		}

		/// <summary>
		/// Binds the branding.
		/// </summary>
		/// <param name="brandingLabel">The branding label.</param>
		/// <param name="branding">The branding.</param>
    private void BindBranding(Label brandingLabel, string branding)
		{
			brandingLabel.Text = branding;
		}

		/// <summary>
		/// Binds the employment history.
		/// </summary>
		/// <param name="employmentHistoryLabel">The employment history label.</param>
		/// <param name="employmentHistories">The employment histories.</param>
		/// <param name="branding">The branding.</param>
		private void BindEmploymentHistory(Literal employmentHistoryLiteral, IEnumerable<ResumeView.ResumeJobView> employmentHistories, string branding)
		{
			var employmentHistorySummary = new StringBuilder(string.Empty);

			var unknownText = CodeLocalise("Global.Unknown.Text", "Unknown");
			var i = 1;

			foreach (var employmentHistory in employmentHistories)
			{
				if (branding.IsNotNullOrEmpty() && i > 2) break;
				if (branding.IsNullOrEmpty() && i > 3) break;

				employmentHistorySummary.AppendFormat("{0} - {1} ({2}-{3})<br />",
																							employmentHistory.JobTitle.IsNotNullOrEmpty()
																								? employmentHistory.JobTitle
																								: unknownText,
																							employmentHistory.Employer.IsNotNullOrEmpty()
																								? employmentHistory.Employer
																								: unknownText,
																							employmentHistory.YearStart.IsNotNullOrEmpty()
																								? employmentHistory.YearStart
																								: unknownText,
																							employmentHistory.YearEnd.IsNotNullOrEmpty()
																								? employmentHistory.YearEnd
																								: unknownText);
				i++;
			}


			if (employmentHistorySummary.ToString().IsNotNullOrEmpty())
				employmentHistoryLiteral.Text = employmentHistorySummary.ToString().Substring(0, employmentHistorySummary.ToString().Length - 6);
		}

		/// <summary>
		/// Binds the status.
		/// </summary>
		/// <param name="statusDropDownList">The status drop down list.</param>
		/// <param name="status">The status.</param>
		private void BindStatus(DropDownList statusDropDownList, ApplicationStatusTypes status)
		{
			statusDropDownList.Items.Clear();

			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.DidNotApply, "Failed to apply to job"), ApplicationStatusTypes.DidNotApply.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToShow, "Failed to report to interview"), ApplicationStatusTypes.FailedToShow.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToReportToJob, "Failed to report to job"), ApplicationStatusTypes.FailedToReportToJob.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToRespondToInvitation, "Failed to respond to invitation"), ApplicationStatusTypes.FailedToRespondToInvitation.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Hired, "Hired"), ApplicationStatusTypes.Hired.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewDenied, "Interview denied"), ApplicationStatusTypes.InterviewDenied.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewScheduled, "Interview scheduled"), ApplicationStatusTypes.InterviewScheduled.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.JobAlreadyFilled, "Job already filled"), ApplicationStatusTypes.JobAlreadyFilled.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NewApplicant, "New applicant"), ApplicationStatusTypes.NewApplicant.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotHired, "Not hired"), ApplicationStatusTypes.NotHired.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotQualified, "Not qualified"), ApplicationStatusTypes.NotQualified.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Recommended, "Recommended"), ApplicationStatusTypes.Recommended.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise("ApplicationStatusTypes.job", "Refused job"), ApplicationStatusTypes.RefusedOffer.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.RefusedReferral, true), ApplicationStatusTypes.RefusedReferral.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.UnderConsideration, "Under consideration"), ApplicationStatusTypes.UnderConsideration.ToString()));
			
			statusDropDownList.SelectValue(status.ToString());
		}

		#endregion

		/// <summary>
		/// Shows the candidates like this modal.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="logSearch">if set to <c>true</c> [log search].</param>
		private void ShowCandidatesLikeThisModal(long personId, bool logSearch = true)
		{
            this.Page.Title = "CandidatesLikeThis - " + personId;
			CandidatesLikeThisListView.DataSource = null;

			CandidatesLikeThisPersonId = personId;

			CandidatesLikeThisDataSource.SelectParameters.Clear();
			var parameter = new Parameter("personId", TypeCode.String, personId.ToString());
			CandidatesLikeThisDataSource.SelectParameters.Add(parameter);

			var jobIdParameter = new Parameter("jobId", TypeCode.Int64, JobId.ToString());
			CandidatesLikeThisDataSource.SelectParameters.Add(jobIdParameter);

			var logSearchParameter = new Parameter("logSearch", TypeCode.Boolean, logSearch.ToString());
			CandidatesLikeThisDataSource.SelectParameters.Add(logSearchParameter);

			CandidatesLikeThisListView.DataBind();

			CandidatesLikeThisModalPopup.Show();
		}

		/// <summary>
		/// Builds the list tabs.
		/// </summary>
		private void BuildListTabs()
		{
			var linkJobId = (JobId > 0) ? (long?) JobId : null;
			var linkSavedSearchId = (SavedSearchId > 0) ? (long?)SavedSearchId : null;

			AllResumesLink.NavigateUrl = UrlBuilder.TalentPool(Tabs.AllResumes, linkJobId, linkSavedSearchId, SearchSession);
			ApplicantsLink.NavigateUrl = UrlBuilder.TalentPool(Tabs.Applicants, linkJobId, linkSavedSearchId, SearchSession);
			FlaggedResumesLink.NavigateUrl = UrlBuilder.TalentPool(Tabs.FlaggedResumes, linkJobId, linkSavedSearchId, SearchSession);
			InviteesLink.NavigateUrl = UrlBuilder.TalentPool(Tabs.Invitees, linkJobId, linkSavedSearchId, SearchSession);
            
		}

		/// <summary>
		/// Searches the scope to string.
		/// </summary>
		/// <param name="searchScope">The search scope.</param>
		/// <returns></returns>
		private static string SearchScopeToString(CandidateSearchScopeTypes searchScope)
		{
			switch (searchScope)
			{
				case CandidateSearchScopeTypes.Flagged:
					return FlaggedText;
				case CandidateSearchScopeTypes.JobApplicants:
					return ApplicantsText;
				case CandidateSearchScopeTypes.JobInvitees:
					return InviteesText;
				default:
					return OpenText;
			}
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		internal void Bind()
		{
			SearchResultListView.DataBind();
			
			SearchResultListPager.Visible = (SearchResultListPager.TotalRowCount > 0);
			SortByDropDown.Visible = (SearchResultListPager.TotalRowCount > 0 && Tab == Tabs.Applicants && JobId > 0);
			if (SortByDropDown.Visible) SortByDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PoolResultSortBys), null, CodeLocalise("Global.SortBy.TopDefault", "- sort by -"));

			InviteMultipleButton.Visible = (Tab == Tabs.AllResumes && App.Settings.MultipleJobseekerInvitesEnabled && (HighestMatchRating > 0) && (JobId > 0) && ShowEmailIcons);

            this.Page.Title = Tab.ToString();
            if (Page.RouteData.Values.ContainsKey("jobid"))
                this.Page.Title = Tab + "-" + Page.RouteData.Values["jobid"].ToString();
            if (Page.RouteData.Values.ContainsKey("searchid"))
                this.Page.Title = Tab + "-" + Page.RouteData.Values["searchid"].ToString();
            var sessionid = Page.RouteData.Values.ContainsKey("searchsession") ? Page.RouteData.Values["searchsession"].ToString() : "0";
            if (sessionid.Length > 1)
                this.Page.Title = this.Page.Title + " - " + sessionid.Split('-')[0];

     		_controlBound = true;
		}

		public enum Tabs
		{
			AllResumes,
			Applicants,
			FlaggedResumes,
			Invitees
		}

		private void ApplyBranding()
		{
			SearchSummaryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SearchSummaryPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			SearchSummaryPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			CandidatesLikeThisModalCloseButton.ImageUrl = UrlBuilder.ButtonCloseIcon();
		}

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			InviteMultipleButton.Text = CodeLocalise("InviteMultipleButton.Text", "Send Multiple Invites");
		}
	}
}
