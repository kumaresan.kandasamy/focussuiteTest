﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web;
using System.Web.UI.WebControls;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebTalent.Controls
{
	public partial class WelcomeToTalentModal : UserControlBase
	{
		public delegate void CloseCommandHandler(object sender, CommandEventArgs eventArgs);
		public event CloseCommandHandler CloseCommand;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Shows the modal.
		/// </summary>
		/// <param name="top">The top.</param>
		/// <param name="closeCommand">The close command.</param>
		public bool Show(int top = 210, string closeCommand = "")
		{
			if (ShowWelcomeModal())
			{
				WelcomeModal.Y = top;
				WelcomeModal.Show();

				if (closeCommand.IsNotNullOrEmpty())
				{
					CloseIcon.Visible = false;
					CloseButton.Visible = true;
					CloseButton.CommandName = closeCommand;
				}

				return true;
			}

			return false;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			WelcomeContentLiteral.Text = CodeLocalise("WelcomeContentLiteral.Text",
																								@"<b>Welcome to #FOCUSTALENT#</b><br/>
<br/>
Please click “Post a new job” to create a posting.<br/>
<br/>
If you have problems navigating the site, please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a>. 
<br/>
Our business hours are between #SUPPORTHOURSOFBUSINESS#");

			CloseIcon.Src =
				CloseButton.ImageUrl = UrlBuilder.ButtonCloseIcon();

			CloseIcon.Alt =
				CloseButton.AlternateText = HtmlLocalise("Global.Close.Button", "Close");
		}

		/// <summary>
		/// Shows the welcome modal.
		/// </summary>
		/// <returns></returns>
		private bool ShowWelcomeModal()
		{
			try
			{
			  if (Request.Cookies["ShowTalentWelcomeModal"] != null)
					return false;

				var cookie = new HttpCookie("ShowTalentWelcomeModal");
				var now = DateTime.Now;
				cookie.Value = now.ToString();
				cookie.Expires = now.AddDays(1);
				Response.Cookies.Add(cookie);

				return true;
			}
			catch
			{
				return true;
			}
		}

		/// <summary>
		/// Handles the Command event of the CloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Command(object sender, CommandEventArgs eventArgs)
		{
			WelcomeModal.Hide();
			OnCloseCommand(new CommandEventArgs(eventArgs.CommandName, null));
		}

		/// <summary>
		/// Raises the <see cref="OnCloseCommand"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnCloseCommand(CommandEventArgs eventArgs)
		{
			if (CloseCommand != null)
				CloseCommand(this, eventArgs);
		}
	}
}