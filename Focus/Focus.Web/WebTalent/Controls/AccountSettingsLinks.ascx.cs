﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.HtmlControls;

using Focus.Common.Extensions;
using Focus.Web.Code;
using Framework.Core;
using Focus.Common;
using Focus.Core;

#endregion

namespace Focus.Web.WebTalent.Controls
{
  public partial class AccountSettingsLinks : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (App.Settings.DisableTalentAuthentication)
      {
        TalentAuthenticationLinks.Visible = false;
      }
      else
      {
				if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent))
				{
					UpdateLink(TalentChangePasswordLink, UrlBuilder.ChangeLogin());
					UpdateLink(TalentContactInformationLink, UrlBuilder.UpdateContactInformation());
					TalentChangePasswordSSOLinkPlaceholder.Visible = false;
					TalentContactInformationSSOLinkPlaceholder.Visible = false;
				}
				else
				{
					TalentAuthenticationLinks.Visible = false;

					if (App.Settings.SSOChangeUsernamePasswordUrl.IsNotNullOrEmpty())
						UpdateLink(TalentChangePasswordSSOLink, App.Settings.SSOChangeUsernamePasswordUrl);
					else
						TalentChangePasswordSSOLinkPlaceholder.Visible = false;

					if (App.Settings.SSOManageAccountUrl.IsNotNullOrEmpty())
						UpdateLink(TalentContactInformationSSOLink, App.Settings.SSOManageAccountUrl);
					else
						TalentContactInformationSSOLinkPlaceholder.Visible = false;
				}
      }

			if (ServiceClientLocator.EmployeeClient(App).GetEmployeeByPerson(App.User.PersonId.Value).ApprovalStatus.IsNotIn(ApprovalStatuses.None,ApprovalStatuses.Approved))
				TalentManageCompanyLinkPlaceholder.Visible = false;
			else
				UpdateLink(TalentManageCompanyLink, UrlBuilder.ManageCompany(), "javascript:showProcessing();");

      UpdateLink(TalentSavedSearchesLink, UrlBuilder.ManageSavedSearches());
    }

    /// <summary>
    /// Either adds the href to the link, or styles it bold, depending on if it for the active page
    /// </summary>
    /// <param name="link">The hyperlink control on the page</param>
    /// <param name="href">The href parameter for the link</param>
    private void UpdateLink(HtmlAnchor link, string href)
    {
      UpdateLink(link, href, string.Empty);
    }

    /// <summary>
    /// Either adds a href (and onclick event) to the link, or styles it bold, depending on if it for the active page
    /// </summary>
    /// <param name="link">The hyperlink control on the page</param>
    /// <param name="href">The href parameter for the link</param>
    /// <param name="onclick">An onclick event to add to the link</param>
    private void UpdateLink(HtmlAnchor link, string href, string onclick)
    {
      if (Page.Is(href))
      {
        //link.Style.Value = "font-weight:bold;text-decoration:none";
				link.Attributes["class"] = "current-page";
      }
      else
      {
        link.HRef = href;
	      if (onclick.Length > 0)
	      {
		      link.Attributes.Add("onclick", onclick);
					link.Attributes.Add("onkeypress", onclick);
				}
      }
    }
  }
}