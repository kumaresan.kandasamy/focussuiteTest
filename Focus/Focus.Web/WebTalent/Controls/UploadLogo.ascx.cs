﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Drawing;
using System.IO;
using System.Web.UI;

using Framework.Core;
using AjaxControlToolkit;

using Focus.Core;
using Focus.Web.Code;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.WebTalent.Controls
{
	public partial class UploadLogo : UserControlBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets the width of the logo.
		/// </summary>
		public int LogoWidth
		{
			get { return GetViewStateValue("UploadLogo:LogoWidth", 450); }
			set { SetViewStateValue("UploadLogo:LogoWidth", value); }
		}

		/// <summary>
		/// Gets or sets the height of the logo.
		/// </summary>
		public int LogoHeight
		{
			get { return GetViewStateValue("UploadLogo:LogoHeight", 113); }
			set { SetViewStateValue("UploadLogo:LogoHeight", value); }
		}

		/// <summary>
		/// Gets or sets the size of the max file.
		/// </summary>
		public int MaxFileSize
		{
			get { return GetViewStateValue("UploadLogo:MaxFileSize", 100); }
			set { SetViewStateValue("UploadLogo:MaxFileSize", value); }
		}

		/// <summary>
		/// Gets or sets the validation group.
		/// </summary>
		/// <value>The validation group.</value>
		public string ValidationGroup
		{
			get { return GetViewStateValue("UploadLogo:ValidationGroup", string.Empty); }
			set { SetViewStateValue("UploadLogo:ValidationGroup", value); }
		}

		/// <summary>
		/// Gets the name of the logo.
		/// </summary>
		/// <value>The name of the logo.</value>
		public string LogoName
		{
			get { return LogoNameTextBox.TextTrimmed(); }
		}

		/// <summary>
		/// Gets the logo.
		/// </summary>
		/// <value>The logo.</value>
		public byte[] Logo
		{
			get { return App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedLogo); }
		}

		#endregion

		/// <summary>
		/// Clears this control.
		/// </summary>
		public void Clear()
		{
			LogoNameTextBox.Text = "";
			UploadedLogoFileType.Text = "";
			ImageText.Style.Add("display", "none");
			App.RemoveSessionValue(Constants.StateKeys.UploadedLogo);
		}

		/// <summary>
		/// Disables the validation.
		/// </summary>
		public void DisableValidation()
		{
			LogoNameRequired.Enabled = false;
			UploadLogoAsyncFileUploadedLogoFileTypeValidator.Enabled = false;
			UploadLogoAsyncFileUploadedLogoIsValidValidator.Enabled = false;
		}

		/// <summary>
		/// Enables the validation.
		/// </summary>
		public void EnableValidation()
		{
			LogoNameRequired.Enabled = true;
			UploadLogoAsyncFileUploadedLogoFileTypeValidator.Enabled = true;
			UploadLogoAsyncFileUploadedLogoIsValidValidator.Enabled = true;
		}

		/// <summary>
		/// Determines whether this control has file.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this control has file; otherwise, <c>false</c>.
		/// </returns>
		public bool HasFile()
		{
			var fileBytes = App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedLogo);

			return  (fileBytes != null && fileBytes.Length > 0);
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Set the validation group
			if(ValidationGroup.IsNotNullOrEmpty())
			{
				LogoNameRequired.ValidationGroup = ValidationGroup;
				UploadLogoAsyncFileUploadedLogoFileTypeValidator.ValidationGroup = ValidationGroup;
				UploadLogoAsyncFileUploadedLogoIsValidValidator.ValidationGroup = ValidationGroup;
			}

			// Localise some error messages
			LogoNameRequired.ErrorMessage = CodeLocalise("LogoName.Required", "Logo name is required.");
			UploadLogoAsyncFileUploadedLogoFileTypeValidator.ErrorMessage = CodeLocalise("UploadLogoAsyncFileUpload.InvalidFileType", "Logo must be in GIF, JPEG, or PNG format.");
		  UploadLogoAsyncFileUploadedLogoFileTypeValidator.ValidateEmptyText = true;
			UploadLogoAsyncFileUploadedLogoIsValidValidator.ErrorMessage = CodeLocalise("UploadLogoAsyncFileUpload.InvalidFileSize", "Logo has invalid dimensions or is too big.");

			// Hide the spoof textboxes
			UploadedLogoFileType.Style.Add("display", "none");
			UploadedLogoIsValid.Style.Add("display", "none");

			ScriptManager.RegisterClientScriptBlock(this, typeof(UploadLogo), "UploadLogoJavaScript", @"$(document).ready(function ()
			{
				UploadLogoStartup();
			});
	
			function UploadLogoStartup() {"
				//$('.fileupload input:file').customFileInput('" + CodeLocalise("Global.Browse.Text", "Browse") + @"', '" + CodeLocalise("Global.Change.Text", "Change") + @"', '" + CodeLocalise("UploadLogoFeedback.Text", "Upload a logo") + @"');
				+ @"$('.inFieldLabel > label, .inFieldLabelAlt > label').inFieldLabels(); 
			}

			function UploadLogoUploadComplete(sender, args) {
				var options = { type: ""POST"",
					url: """ + UrlBuilder.AjaxService() + @"/IsUploadedLogoValid"",
					contentType: ""application/json; charset=utf-8"",
					dataType: ""json"",
					async: true,
					success: function (response) {
						var results = response.d;
						$('#UploadedLogoIsValid').val(results);
						if (results == '1'){
							$('#UploadedLogoImage').attr('src', '" + UrlBuilder.ViewLogo(Constants.StateKeys.UploadedLogo) + @"');
							$('#UploaderDiv').hide();
							$('#ImageDiv').show();	
							$('#ImageText').show();
						}
					}
				};

				$.ajax(options);

		
			}

			function UploadLogoCheckExtension(sender, args) {
				var filename = args.get_fileName();
				var ext = filename.substring(filename.lastIndexOf(""."") + 1).toLowerCase();

				$('#UploadedLogoFileType').val(ext);

				return (ext == 'png' || ext == 'jpg' || ext == 'gif');
			}

			function UploadLogoAsyncFileUploadLogoFileTypeValidator_Validate(source, args) {
				var fileType = $('#UploadedLogoFileType').val();
				args.IsValid = (fileType == 'png' || fileType == 'jpg' || fileType == 'gif');
			}

			function UploadLogoAsyncFileUploadedLogoIsValidValidator_Validate(source, args) {
				var isValid = ($('#UploadedLogoIsValid').val() == ""1"");
				args.IsValid = isValid;
			}", true);
		}

		#region Ajax Events

		/// <summary>
		/// Handles the UploadedComplete event of the UploadLogoAsyncFileUpload control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="AjaxControlToolkit.AsyncFileUploadEventArgs"/> instance containing the event data.</param>
		public void UploadLogoAsyncFileUpload_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
		{
			var bytes = UploadLogoAsyncFileUpload.FileBytes;

			if (bytes == null || bytes.Length > (MaxFileSize * 1024) || !IsValidDimensions(bytes))
			{
				var img = ScaleImage(bytes, LogoHeight, LogoWidth);
				var imgBytes = ConvertImageToByteArray(img);
				App.SetSessionValue(Constants.StateKeys.UploadedLogo, imgBytes);
				
				if (imgBytes.Length < MaxFileSize * 1024)
				{
					
					ImageText.Text = CodeLocalise("ImageResized.Text",
						                            "The image exceeded the maximum file dimensions/size and has been resized.<br>Please click Save to save the image.");
					App.SetSessionValue(Constants.StateKeys.UploadedLogoIsValid, true);
					ImageText.Style.Add("display", "block");
				}
				else
				{
					ImageText.Text = CodeLocalise("ImageResized.Text",
																				"The image exceeded the maximum file dimensions/size and has been resized but still exceeds maximum file size.<br>Please choose another image.");
				}
			}
			else
			{
				App.SetSessionValue(Constants.StateKeys.UploadedLogoIsValid, true);
				App.SetSessionValue(Constants.StateKeys.UploadedLogo, bytes);
				ImageText.Text = "";
			}

			UploadLogoAsyncFileUpload.ClearAllFilesFromPersistedStore();
		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Determines whether the image has valid dimensions.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <returns>
		/// 	<c>true</c> if the image dimensions are valid; otherwise, <c>false</c>.
		/// </returns>
		private bool IsValidDimensions(byte[] bytes)
		{
			try
			{
				using (var memoryStream = new MemoryStream(bytes))
				{
					using (var img = System.Drawing.Image.FromStream(memoryStream))
					{
						var width = img.Width;
						var height = img.Height;

						return (width <= LogoWidth && height <= LogoHeight);
					}
				}
			}

			catch
			{
				return false;
			}
		}


		/// <summary>
		/// Scales the image.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <param name="maxHeight">Height of the max.</param>
		/// <param name="maxWidth">Width of the max.</param>
		/// <returns></returns>
		private System.Drawing.Image ScaleImage(byte[] bytes, int maxHeight, int maxWidth)
		{

			using (var memoryStream = new MemoryStream(bytes))
			{
				using (var img = System.Drawing.Image.FromStream(memoryStream))
				{
					var ratio = (double)maxHeight / maxWidth;

					var newWidth = (int)(img.Width * ratio);
					var newHeight = (int)(img.Height * ratio);

					var newImage = new Bitmap(newWidth, newHeight);
					using (var g = Graphics.FromImage(newImage))
					{
						g.DrawImage(img, 0, 0, newWidth, newHeight);
					}
					return newImage;
				}
			}
		}

		/// <summary>
		/// Converts the image to byte array.
		/// </summary>
		/// <param name="image">The image.</param>
		/// <returns></returns>
		public byte[] ConvertImageToByteArray(System.Drawing.Image image)
		{
			using (var ms = new MemoryStream())
			{
				image.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
				// or whatever output format you like
				return ms.ToArray();
			}
		}

		#endregion
	}
}