﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Security.Permissions;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.WebTalent
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)] 
  public partial class UpdateCompanyInformation : PageBase
	{
    /// <summary>
    /// Gets or sets the user id.
    /// </summary>
    /// <value>The user id.</value>
    protected long EmployerId
    {
      get { return GetViewStateValue<long>("UpdateCompanyInformation:EmployerId"); }
      set { SetViewStateValue("UpdateCompanyInformation:EmployerId", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        Bind();
        LocaliseUI();

        if (App.User.EmployerId != null) EmployerId = (long) App.User.EmployerId;

        InitialiseControlValues();
      }
    }

    /// <summary>
    /// Handles the Click event of the SaveButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveButton_Click(object sender, EventArgs e)
    {
      var model = GetModel();

      if (model.PrimaryPhoneNumber.IsNull()) model.PrimaryPhoneNumber = new PhoneNumberDto { Id = 0 };

      model.PrimaryPhoneNumber.Number = PhoneTextBox.TextTrimmed();
      model.PrimaryPhoneNumber.PhoneType = PhoneTypeDropDown.SelectedValueToEnum<PhoneTypes>();
      model.PrimaryPhoneNumber.IsPrimary = true;
      model.PrimaryPhoneNumber.Extension = PhoneExtensionTextBox.TextTrimmed();

      if (model.AlternatePhoneNumber1.IsNull()) model.AlternatePhoneNumber1 = new PhoneNumberDto { Id = 0 };

      model.AlternatePhoneNumber1.Number = AlternatePhone1TextBox.TextTrimmed();
      model.AlternatePhoneNumber1.PhoneType = AlternatePhone1TypeDropDown.SelectedValueToEnum<PhoneTypes>();
      model.AlternatePhoneNumber1.IsPrimary = false;

      if (model.AlternatePhoneNumber2.IsNull()) model.AlternatePhoneNumber2 = new PhoneNumberDto { Id = 0 };

      model.AlternatePhoneNumber2.Number = AlternatePhone2TextBox.TextTrimmed();
      model.AlternatePhoneNumber2.PhoneType = AlternatePhone2TypeDropDown.SelectedValueToEnum<PhoneTypes>();
      model.AlternatePhoneNumber2.IsPrimary = false;

      model.EmployerId = EmployerId;
    }

    /// <summary>
    /// Initialises the control values.
    /// </summary>
    private void InitialiseControlValues()
    {
      var model = GetModel();

      NameTextBox.Text = model.Name;
      FEINLabel.Text = model.FEIN;
      IndustrialClassificationTextBox.Text = model.IndustrialClassification;
      
      if (model.PrimaryPhoneNumber.IsNotNull())
      {
        if (model.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) PhoneTextBox.Text = model.PrimaryPhoneNumber.Number;
        if (model.PrimaryPhoneNumber.PhoneType.IsNotNull()) PhoneTypeDropDown.SelectValue(model.PrimaryPhoneNumber.PhoneType.ToString());
        if (model.PrimaryPhoneNumber.Extension.IsNotNullOrEmpty()) PhoneExtensionTextBox.Text = model.PrimaryPhoneNumber.Extension;
      }

      if (model.AlternatePhoneNumber1.IsNotNull())
      {
        if (model.AlternatePhoneNumber1.Number.IsNotNullOrEmpty()) AlternatePhone1TextBox.Text = model.AlternatePhoneNumber1.Number;
        if (model.AlternatePhoneNumber1.PhoneType.IsNotNull()) AlternatePhone1TypeDropDown.SelectValue(model.AlternatePhoneNumber1.PhoneType.ToString());
      }

      if (model.AlternatePhoneNumber2.IsNotNull())
      {
        if (model.AlternatePhoneNumber2.Number.IsNotNullOrEmpty()) AlternatePhone2TextBox.Text = model.AlternatePhoneNumber2.Number;
        if (model.AlternatePhoneNumber2.PhoneType.IsNotNull()) AlternatePhone2TypeDropDown.SelectValue(model.AlternatePhoneNumber2.PhoneType.ToString());
      }
    }

    /// <summary>
    /// Gets the model.
    /// </summary>
    /// <returns></returns>
    private EmployerDetailsModel GetModel()
    {
      return ServiceClientLocator.EmployerClient(App).GetEmployer(EmployerId);
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {

      BindContactTypeDropDown(PhoneTypeDropDown);
      BindContactTypeDropDown(AlternatePhone1TypeDropDown);
      BindContactTypeDropDown(AlternatePhone2TypeDropDown);
    }

    /// <summary>
    /// Binds the contact type drop down.
    /// </summary>
    /// <param name="dropDownList">The drop down list.</param>
    private void BindContactTypeDropDown(DropDownList dropDownList)
    {
      dropDownList.Items.Clear();

      dropDownList.Items.AddEnum(PhoneTypes.Phone, "landline");
      dropDownList.Items.AddEnum(PhoneTypes.Mobile, "mobile");
      dropDownList.Items.AddEnum(PhoneTypes.Fax, "fax");
      dropDownList.Items.AddEnum(PhoneTypes.Other, "other");
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      SaveButton.Text = CodeLocalise("Global.Save.Label", "Save");
      PhoneRequired.ErrorMessage = CodeLocalise("ContactPhone.RequiredErrorMessage", "Phone number is required");
     
    }
  }
}