﻿<%@ Page Title="Update Contact Information" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateContactInformation.aspx.cs" Inherits="Focus.Web.WebTalent.UpdateContactInformation" %>

<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebTalent/Controls/TabNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="SettingsLinks" Src="Controls/AccountSettingsLinks.ascx" %>
<%@ Register TagPrefix="uc" TagName="ContactInformation" Src="~/Code/Controls/User/ContactInformation.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<uc:SettingsLinks ID="SettingLinksMain" runat="server" />

	<h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>
	
	<table width="100%" role="presentation">
		<tr>
			<td>
				<asp:button ID="SaveButton" runat="server" CssClass="button2 right" OnClick="SaveButton_Click" ValidationGroup="ContactInformation" />
				<p><b><%= HtmlLocalise("UpdateContactInformation.Label", "Update my contact information")%></b> <%= HtmlRequiredFieldsInstruction() %></p>
			</td>	
		</tr>
	</table>
	
	<uc:ContactInformation ID="ContactInformationMain" runat="server" />

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />

</asp:Content>


