﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JobUpload.aspx.cs" Inherits="Focus.Web.WebTalent.JobUpload" %>

<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebTalent/Controls/JobsNavigation.ascx" tagname="JobsNavigation" tagprefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagPrefix="uc" TagName="ConfirmationModal" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%# ResolveUrl("~/Assets/Scripts/jquery-ui-1.9.2.custom.new.min.js") %>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<%# ResolveUrl("~/Assets/Css/jquery-ui-1.9.2.custom.css") %>" />
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:JobsNavigation ID="PageNavigation" runat="server" />
	
  <asp:PlaceHolder runat="server" ID="InitialControls">
    <p>
      <asp:Button runat="server" ID="DownloadTemplateButton" class="button3" CausesValidation="False" OnClick="DownloadTemplateButton_OnClick"/>
    </p>
    <p>
        <div style="display: inline-block">
          <asp:Label runat="server" ID="HelpLabel" style="display:inline-block"></asp:Label>
        </div>
        <div id="HelpToolTip" clientidmode="Static" style="display:inline-block">
			    <%= HtmlTooltipster("tooltipWithArrow", "Help.Tooltip", @"Files of CSV and XLS are suported, but note that any text formatting in the description field will be not retained in the CSV format.")%>
        </div>
    </p>
    <p>
      <asp:FileUpload runat="server" ID="UploadFile" title="Upload File" style="width: 355px" />&nbsp;
      <br/>
      <asp:RequiredFieldValidator runat="server" ID="UploadFileRequiredValidator" ControlToValidate="UploadFile" ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
      <focus:InsensitiveRegularExpressionValidator ID="UploadFileTypeValidator" runat="server" ControlToValidate="UploadFile"
			  CssClass="error" ValidationExpression="(.*)(\.)((csv)|(xls)|(xlsx))$"
			  SetFocusOnError="True" Display="Dynamic"></focus:InsensitiveRegularExpressionValidator>
    </p>
    <br />
    <p>
      <asp:Literal runat="server" ID="SelectEmployerLiteral"></asp:Literal>&nbsp;
      <asp:DropDownList runat="server" ID="BusinessUnitDropdown"/>
      <br />
      <asp:RequiredFieldValidator runat="server" ID="BusinessUnitDropdownValidator" ControlToValidate="BusinessUnitDropdown" ValidateEmptyText="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
    </p>
    <p>
			  <asp:CheckBox ID="MeetsMinimumWageRequirementCheckBox" runat="server"/>
        <asp:Literal runat="server" ID="MeetsMinimumWageRequirementCheckBoxText"></asp:Literal>
        <br />
        <focus:CheckBoxCustomValidator runat="server" ID="MeetsMinimumWageRequirementValidator" ControlToValidate="MeetsMinimumWageRequirementCheckBox" ClientValidationFunction="JobUpload_ValidateMeetsMinimumWageRequirement" ValidateEmptyText="True" EnableClientScript="True" SetFocusOnError="True" CssClass="error" Display="Dynamic" />
    </p>
  </asp:PlaceHolder>
  
  <asp:PlaceHolder runat="server" ID="FileSummaryHeaderPlaceHolder" Visible="False">
    <p>
      <asp:Label runat="server" ID="FileNameHeader" style="font-weight: bold" />&nbsp;
      <asp:Label runat="server" ID="UploadFileName"></asp:Label>
    </p>
    <p>
      <asp:Label runat="server" ID="EmployerNameHeader" style="font-weight: bold" />&nbsp;
      <asp:Label runat="server" ID="EmployerName"></asp:Label>
    </p>
  </asp:PlaceHolder>
  
  <asp:PlaceHolder runat="server" ID="ProgressBarPlaceHolder" Visible="False">
    <asp:HiddenField runat="server" ID="ProcessingComplete" value="0" />
    <br />
    <p><em><asp:Literal runat="server" ID="FileProcessingLiteral"></asp:Literal></em></p>
    <script type="text/javascript" >
      var JobUpload_CheckProgress_Url;
      var JobUpload_CheckProgress_UploadFileId;

      function JobUpload_CheckProgress(url, uploadFileId) {
        if (JobUpload_CheckProgress_Url == null) {
          JobUpload_CheckProgress_Url = url;
          JobUpload_CheckProgress_UploadFileId = uploadFileId;
        }

        var options = {
          type: "POST",
          url: JobUpload_CheckProgress_Url,
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          data: '{"uploadFileId":' + JobUpload_CheckProgress_UploadFileId + '}',
          async: true,
          success: function (response) {
            var results = response.d;
            if (results == "1") {
              JobUpload_ProcessingComplete();
            } else {
              window.setTimeout(JobUpload_CheckProgress, 5000);
            }
          }
        };

        $.ajax(options);
      }
    </script>
  </asp:PlaceHolder>

  <asp:PlaceHolder runat="server" ID="ValidationResultsPlaceHolder" Visible="False">

    <asp:PlaceHolder runat="server" ID="UploadStatisticsPlaceHolder">
      <table>
        <thead>
          <tr>
            <th>&nbsp;</th>
            <th><asp:Label runat="server" ID="UploadStatisticsHeader" /></th>
          </tr>
        </thead>
        <tbody>
          <tr id="ActiveJobsRow" runat="server">
            <td><asp:Label runat="server" ID="ActiveJobsHeader"></asp:Label></td>
            <th><asp:Label runat="server" ID="ActiveJobsTotal"></asp:Label></th>
            <td>&nbsp;</td>
          </tr>
          <tr id="YellowWordsRow" runat="server">
            <td><asp:Label runat="server" ID="YellowWordsHeader"></asp:Label></td>
            <th><asp:Label runat="server" ID="YellowWordsTotal"></asp:Label></th>
            <td>&nbsp;</td>
          </tr>
          <tr id="RedWordsRow" runat="server">
            <td><asp:Label runat="server" ID="RedWordsHeader"></asp:Label></td>
            <th><asp:Label runat="server" ID="RedWordsTotal"></asp:Label></th>
            <td>&nbsp;</td>
          </tr>
          <tr id="CreditChecksRequiredRow" runat="server">
            <td><asp:Label runat="server" ID="CreditChecksRequiredHeader"></asp:Label></td>
            <th><asp:Label runat="server" ID="CreditChecksRequiredTotal"></asp:Label></th>
            <td>&nbsp;</td>
          </tr>
          <tr id="CriminalChecksRequiredRow" runat="server">
            <td><asp:Label runat="server" ID="CriminalChecksRequiredHeader"></asp:Label></td>
            <th><asp:Label runat="server" ID="CriminalChecksRequiredTotal"></asp:Label></th>
            <td><asp:LinkButton runat="server" ID="CriminalBackgroundDisclaimerLink" OnClick="CriminalBackgroundDisclaimerLink_OnClick" ></asp:LinkButton></td>
          </tr>
          <tr id="JobsInErrorRow" runat="server" class="error">
            <td><asp:Label runat="server" ID="JobsInErrorHeader"></asp:Label><hr /></td>
            <th><asp:Label runat="server" ID="JobsInErrorTotal"></asp:Label><hr /></th>
            <td>&nbsp;</td>
          </tr>
          <tr id="TotalJobRows" runat="server">
            <td style="text-align: right"><asp:Label runat="server" ID="TotalJobRowsHeader"></asp:Label></td>
            <th><asp:Label runat="server" ID="TotalJobRowsTotal"></asp:Label></th>
            <td>
            <div style="display: inline-block">
              <asp:LinkButton runat="server" ID="ExceptionLogButton" OnClick="ExceptionLogButton_OnClick"></asp:LinkButton>
            </div>
            <div id="ExceptionLogToolTip" clientidmode="Static" style="display:inline-block">
			        <%= HtmlTooltipster("tooltipWithArrow", "ExceptionLog.Tooltip", @"Records in the upload file which will not become active jobs will be reported in this exception log")%>
            </div>
            </td>
          </tr>
        </tbody>
      </table>
      <br />
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="UploadDisclaimerPlaceHolder" Visible="False">
      <br />
      <p class="messageSystem">
        <asp:Image runat="server" ID="DisclaimerImage" AlternateText="Disclaimer Image"/>
        <asp:Literal runat="server" ID="UploadDisclaimerText"></asp:Literal>
      </p>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="NoResultsPlaceHolder" Visible="False">
      <br />
      <p>
        <asp:Literal runat="server" ID="NoResultsLabel"></asp:Literal>
      </p>
    </asp:PlaceHolder>
  </asp:PlaceHolder>
  <br />
  <p>
    <asp:Button runat="server" ID="CancelButton" class="button2" CausesValidation="False" OnClick="CancelButton_OnClick"/>
    <asp:Button runat="server" ID="UploadButton" class="button2" OnClick="UploadButton_OnClick"/>
    <asp:Button runat="server" ID="CommitButton" class="button2" OnClick="CommitButton_OnClick" Visible="False"/>
  </p>
  
  <uc:ConfirmationModal ID="UploadConfirmationModal" runat="server" OnCloseCommand="UploadConfirmationModal_OnCloseCommand"/>

  <script type="text/javascript" >
    function JobUpload_ValidateMeetsMinimumWageRequirement(oSrc, args) {
      args.IsValid = $("#<%=MeetsMinimumWageRequirementCheckBox.ClientID%>").is(":checked");
    }

  </script>
</asp:Content>