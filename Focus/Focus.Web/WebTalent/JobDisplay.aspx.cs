﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Focus.Web.WebTalent
{
	public partial class JobDisplay : PageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if ("netscape|gecko|firefox|opera".IndexOf(Request.Browser.Browser.ToLower()) >= 0)
				ClientTarget = "Uplevel";
			Response.Write(App.GetSessionValue<string>("Career:DisplayPosting"));
		}
	}
}