﻿<%@ Page Title="Change Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangeLogin.aspx.cs" Inherits="Focus.Web.WebTalent.ChangeLogin" %>

<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="SettingsLinks" Src="Controls/AccountSettingsLinks.ascx" %>
<%@ Register Src="~/Code/Controls/User/SecurityQuestions.ascx" TagName="SecurityQuestions" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<uc:SettingsLinks ID="SettingLinksMain" runat="server" />

	<h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>

	<table role="presentation" width="100%" id="PasswordSaveTable" runat="server">
		<tr>
			<td>
				<asp:button ID="ChangeLoginButton" runat="server" ValidationGroup="ChangeLogin" CssClass="button2 right" OnClick="ChangeLoginButton_Click" />
				<p><b><%= HtmlLocalise("ChangeLogin.Label", "Change login")%></b> <%= HtmlRequiredFieldsInstruction() %></p>
			</td>	
		</tr>
	</table>

	<asp:PlaceHolder runat="server" ID="PasswordInputPlaceHolder">
		<table role="presentation">
			<tr>
				<td width="175"><%= HtmlRequiredLabel(CurrentPasswordTextbox, "CurrentPassword.Label", "Current password")%></td>
				<td>
					<asp:Textbox ID="CurrentPasswordTextbox" TextMode="Password" runat="server" ClientIDMode="Static" MaxLength="20" />
					<asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeLogin" />
				</td>
			</tr>
			<tr>
				<td><%= HtmlRequiredLabel(NewPasswordTextbox, "NewPassword.Label", "New password")%></td>
				<td>
					<asp:Textbox ID="NewPasswordTextbox" TextMode="Password" runat="server" ClientIDMode="Static" MaxLength="20" />
					<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPasswordTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeLogin" />
					<asp:RegularExpressionValidator ID="NewPasswordRegEx" runat="server" ControlToValidate="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeLogin" />
				</td>
                <td class="instructions"><%= HtmlLocalise("PasswordInstructions", " Password is case-sensitive.")%></td>
			</tr>
			<tr>
				<td><%= HtmlRequiredLabel(ConfirmNewPasswordTextbox, "ConfirmNewPassword.Label", "Retype new password")%></td>
				<td>
					<asp:Textbox ID="ConfirmNewPasswordTextbox" TextMode="Password" runat="server" ClientIDMode="Static" MaxLength="20" />
					<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPasswordTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeLogin" />
					<asp:RegularExpressionValidator ID="ConfirmNewPasswordRegEx" runat="server" ControlToValidate="ConfirmNewPasswordTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeLogin" />
					<asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToValidate="ConfirmNewPasswordTextbox" ControlToCompare="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeLogin" />
				</td>
			</tr>
		</table>
		<p><asp:CustomValidator ID="ChangeLoginValidator" runat="server" CssClass="error" /></p>
		<br/>
		<hr/>
		<br/>
	</asp:PlaceHolder>

	<table role="presentation" width="100%" id="UserNameSaveTable" runat="server">
		<tr>
			<td>
				<asp:button ID="ChangeUsernameButton" runat="server" ValidationGroup="ChangeUsername" CssClass="button2 right" OnClick="ChangeUsernameButton_Click" />
				<p><b><%= HtmlLocalise("ChangeUsername.Label", "Change username")%></b> <%= HtmlRequiredFieldsInstruction() %></p>
			</td>	
		</tr>
	</table>
	
	<asp:PlaceHolder runat="server" ID="UserNameInputPlaceHolder">
		<p><%= HtmlLabel("ChangeUsername.Warning", "Please note: if you change your username, you will need to reauthenticate your email address.") %></p>
		<table role="presentation">
			<tr>
				<td width="175"><%= HtmlRequiredLabel(CurrentUsernameTextbox, "CurrentUsername.Label", "Current username")%></td>
				<td>
					<asp:Textbox ID="CurrentUsernameTextbox" runat="server" ReadOnly="true" ClientIDMode="Static" MaxLength="100" />
					<asp:RequiredFieldValidator ID="CurrentUsernameRequired" runat="server" ControlToValidate="CurrentUsernameTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeUsername" />
				</td>
			</tr>
			<tr>
				<td><%= HtmlRequiredLabel(NewUsernameTextbox, "NewUsername.Label", "New username")%></td>
				<td>
					<asp:Textbox ID="NewUsernameTextbox" runat="server" ClientIDMode="Static" MaxLength="100" />
					<asp:RequiredFieldValidator ID="NewUsernameRequired" runat="server" ControlToValidate="NewUsernameTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeUsername" />
					<asp:RegularExpressionValidator ID="NewUserNameRegEx" runat="server" ControlToValidate="NewUserNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeUsername" />
				</td>
			</tr>
			<tr>
				<td><%= HtmlRequiredLabel(ConfirmNewUsernameTextbox, "ConfirmNewUsername.Label", "Retype new username")%></td>
				<td>
					<asp:Textbox ID="ConfirmNewUsernameTextbox" runat="server" ClientIDMode="Static" MaxLength="100" />
					<asp:RequiredFieldValidator ID="ConfirmNewUsernameRequired" runat="server" ControlToValidate="ConfirmNewUsernameTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeUsername" />
					<asp:CompareValidator ID="NewUsernameCompare" runat="server" ControlToValidate="ConfirmNewUsernameTextbox" ControlToCompare="NewUsernameTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeUsername" />
				</td>
			</tr>
		</table>
		<p><asp:CustomValidator ID="ChangeUsernameValidator" runat="server" CssClass="error" /></p>
	</asp:PlaceHolder>

	<asp:PlaceHolder runat="server" ID="SecurityQuestionsPlaceHolder">
		<br/>
		<hr/>
		<br/>
		<table role="presentation" width="100%" id="SecurityQuestionsTable" runat="server">
			<tr>
				<td>
					<p><b><%= HtmlLocalise( "SecurityQuestions.Label", "Change Security Questions" )%></b> <%= HtmlRequiredFieldsInstruction() %></p>
				</td>	
			</tr>
		</table>
	
		<uc:SecurityQuestions ID="SecurityQuestionsPanel" runat="server" PageMode="Update" />
	</asp:PlaceHolder>

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
</asp:Content>
