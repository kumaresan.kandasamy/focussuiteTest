﻿<%@ Page Title="Jobs" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Jobs.aspx.cs" Inherits="Focus.Web.WebTalent.Jobs" %>
<%@ Import Namespace="Focus.Core.Models" %>

<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobActionModal.ascx" tagname="JobActionModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/MessagesDisplay.ascx" tagname="MessagesDisplay" tagprefix="uc" %>
<%@ Register src="~/WebTalent/Controls/WelcomeToTalentModal.ascx" tagname="WelcomeToTalentModal" tagprefix="uc" %>
<%@ Register src="~/WebTalent/Controls/JobsNavigation.ascx" tagname="JobsNavigation" tagprefix="uc" %>
<%@ Register Src="~/Code/Controls/User/SecurityQuestionsModal.ascx" TagName="SecurityQuestionsModal" TagPrefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript" >
    $(document).ready(function () {
      Sys.WebForms.PageRequestManager.getInstance().add_endRequest(BindTooltips);
    });
  </script>
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:JobsNavigation ID="PageNavigation" runat="server" />

	<uc:MessagesDisplay ID="Messages" runat="server" Module="Talent" />
  <div style="float: right" id="BusinessUnitFilterWrapper">
    <focus:LocalisedLabel runat="server" ID="BusinessUnitFilterLabel" LocalisationKey="BusinessUnitFilterLabel.Text" DefaultText="Show #EMPLOYMENTTYPES#:LOWER for:" />
    <asp:DropDownList runat="server" ID="BusinessUnitFilter" title="Business Unit Filter" OnSelectedIndexChanged="BusinessUnitFilter_OnSelectedIndexChanged" AutoPostBack="True" />
  </div>
	<ul class="navTab localNavTab">
		<li runat="server" id="ActiveTab" enableviewstate="false">
      <div class="helpMessage">
        <div class="tooltipImage toolTipNew" id="ActiveToolTip" runat="server">
    		  <asp:HyperLink runat="server" id="ActiveTabHyperLink" />
        </div>
      </div>
		</li>
		<li runat="server" id="OnHoldTab" enableviewstate="false">
		  <div class="helpMessage">
        <div class="tooltipImage toolTipNew" id="OnHoldToolTip" runat="server">
    		  <asp:HyperLink runat="server" id="OnHoldTabHyperLink" />
        </div>
      </div>
		</li>
		<li runat="server" id="ClosedTab" enableviewstate="false">
		  <div class="helpMessage">
        <div class="tooltipImage toolTipNew" id="ClosedToolTip" runat="server">
    		  <asp:HyperLink runat="server" id="ClosedTabHyperLink" />
        </div>
      </div>
		</li>
		<li runat="server" id="DraftTab" enableviewstate="false">
		  <div class="helpMessage">
        <div class="tooltipImage toolTipNew" id="DraftToolTip" runat="server">
    		  <asp:HyperLink runat="server" id="DraftTabHyperLink" />
        </div>
      </div>
		</li>
	</ul>

	<div id="tabbedContent">
		<asp:UpdatePanel ID="JobsListUpdatePanel" runat="server" >
			<ContentTemplate>
				<asp:ListView ID="JobsListView" runat="server" DataSourceID="JobsDataSource" OnItemDataBound="JobsListView_ItemDataBound" ItemPlaceholderID="JobsListViewPlaceHolder">			
					<LayoutTemplate>
						<div style="width:100%;">
							<uc:Pager ID="JobsListPager" runat="server" PagedControlID="JobsListView" PageSize="10" />
						</div>	
						<table role="presentation" id="JobListTable" class="genericTable">
							<asp:PlaceHolder ID="JobsListViewPlaceHolder" runat="server" />
						</table>
					</LayoutTemplate>	
					<ItemTemplate>											
						<tr class="topRow" id="topRow-<%#((JobViewModel)Container.DataItem).Id%>">
							<td class="column1" style="min-width: 160px">
								<span><%# ((JobViewModel)Container.DataItem).JobTitle%></span>
								<input type="hidden" name="JobTitle-<%#((JobViewModel)Container.DataItem).Id%>" value="<%# ((JobViewModel)Container.DataItem).JobTitle %>" />
								<input type="hidden" name="JobType-<%#((JobViewModel)Container.DataItem).Id%>" value="<%# ((JobViewModel)Container.DataItem).JobType %>" />
								<input type="hidden" name="NumberOfOpenings-<%#((JobViewModel)Container.DataItem).Id%>" value="<%# ((JobViewModel)Container.DataItem).NumberOfOpenings %>" />
							</td>
							<td class="column2">
							    <span>
							        <asp:Panel runat="server" ID="BusinessUnitNameRow">
							            <strong><%# ((JobViewModel)Container.DataItem).BusinessUnitName %></strong>
                          <asp:Label runat="server" ID="JobLocationLabel" CssClass="joblocation"></asp:Label>
                        </asp:Panel>
                      <div><%# FormatDates(((JobViewModel)Container.DataItem))%></div>
                      <asp:Label runat="server" ID="JobStatusLabel"></asp:Label>
							    </span>
							</td>
							<td class="column3">
								<span>
									<asp:Button ID="ViewApplicantsButton" runat="server" CssClass="button3" UseSubmitBehavior="False" CommandName="ViewApplications" 
															CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" OnCommand="ViewButton_Command" />
									<asp:Button ID="ViewJobButton" runat="server" CssClass="button3" UseSubmitBehavior="False" CommandName="OpenJob" 
															CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" OnCommand="ViewButton_Command" />
								</span>
							</td>
						</tr>
						<tr class="bottomRow" id="bottomRow-<%#((JobViewModel)Container.DataItem).Id%>">
							<td colspan="3">
								<span>
								  <div class="tooltip left Refresh">
										<asp:LinkButton ID="RefreshJobButton" runat="server" OnCommand="JobRowAction_Command" aria-label="Refresh"
																		CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" CommandName="RefreshJob">
											<div class="tooltipImage toolTipNew"  title="<%= HtmlLocalise("RefreshJobToolTip", "Refresh")%>"></div>
										</asp:LinkButton>
									</div>
									<div class="tooltip left CloseJob">
										<asp:LinkButton ID="CloseJobButton"  runat="server" OnCommand="JobRowAction_Command" aria-label="Close"
																		CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" CommandName="CloseJob">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("CloseJobToolTip", "Close")%>"></div>
										</asp:LinkButton>
									</div>
									<div class="tooltip left HoldJob">
										<asp:LinkButton ID="HoldJobButton" runat="server" OnCommand="JobRowAction_Command" aria-label="Hold Job"
																		CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" CommandName="HoldJob">											
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("HoldJobToolTip", "Hold")%>"></div>
										</asp:LinkButton>
									</div>
									<div class="tooltip left DuplicateJob">
										<asp:LinkButton ID="DuplicateJobButton"  runat="server" OnCommand="JobRowAction_Command" aria-label="Duplicate Job"
																		CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" CommandName="DuplicateJob">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("DuplicateJobToolTip", "Duplicate")%>"></div>
										</asp:LinkButton>
									</div>

									<div class="tooltip left Reactivate">
										<asp:LinkButton ID="ReactivateJobButton" runat="server" OnCommand="JobRowAction_Command" aria-label="Reactivate Job"
																		CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" CommandName="ReactivateJob">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ReactivateJobToolTip", "Reactivate")%>"></div>
										</asp:LinkButton>
									</div>

									<div class="tooltip left EditJob">
										<asp:LinkButton ID="EditJobButton"  runat="server" OnCommand="JobRowAction_Command" aria-label="Edit Job"
																		CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" CommandName="EditJob">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("EditJobToolTip", "Edit")%>"></div>
										</asp:LinkButton>
									</div>

									<div class="tooltip left DeleteJob">
										<asp:LinkButton ID="DeleteJobButton" runat="server" OnCommand="JobRowAction_Command" aria-label="Delete Job"
																		CommandArgument="<%#((JobViewModel)Container.DataItem).Id%>" CommandName="DeleteJob">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("DeleteJobToolTip", "Delete")%>"></div> 
										</asp:LinkButton>
									</div>

								</span>
							</td>
						</tr>
					</ItemTemplate>
					<EmptyDataTemplate>
						<div style="vertical-align: top; min-height:100%;">
							<br />
							<br />
							<%= NoJobsHelpText %>	
						</div>
					</EmptyDataTemplate>
				</asp:ListView>
				<asp:ObjectDataSource ID="JobsDataSource" runat="server" TypeName="Focus.Web.WebTalent.Jobs" EnablePaging="true" 
															SelectMethod="GetJobs" SelectCountMethod="GetJobsCount" SortParameterName="orderBy"
                              OnSelecting="JobsDataSource_OnSelecting" />
															
				<%-- Modals --%>
				<uc:JobActionModal ID="JobAction" runat="server" OnCompleted="JobAction_Completed"/>
				<uc:ConfirmationModal ID="Confirmation" runat="server" Height="175px" Width="300px" OnOkCommand="Confirmation_OkCommand" />	
				<uc:WelcomeToTalentModal ID="WelcomeToTalent" runat="server" OnCloseCommand="WelcomeToTalent_OnCloseCommand" />			
				<uc:SecurityQuestionsModal ID="SecurityQuestionsModal" runat="server" PageMode="Register" />

			</ContentTemplate>
			
			</asp:UpdatePanel>	
			
	</div>

</asp:Content>
