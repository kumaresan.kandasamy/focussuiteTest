﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion


#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Controllers.WebTalent;
using Focus.Web.WebTalent.Controls;
using Framework.Core;
using Focus.Common.DiffPlex;

#endregion

namespace Focus.Web.WebTalent
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)]
	public partial class Job : PageBase
	{
		private long _jobId;
		private JobDetailsView _model;
		private JobPrevDataDto _previousData;
		private List<JobLocationDto> _jobLocations;
		private static char[] _diffDelimeters;

		private static char[] DiffDelimeters
		{
			get { return _diffDelimeters ?? (_diffDelimeters = new[] { ' ', '\n', ',', '/', '*', '(', ')' }); }
		}

		/// <summary>
		/// Gets or sets the model.
		/// </summary>
		protected JobDetailsView Model
		{
			get { return _model ?? (_model = GetViewStateValue("Job:Model", new JobDetailsView())); }
			set { _model = value; }
		}

		/// <summary>
		/// Gets or sets the model.
		/// </summary>
		protected JobPrevDataDto PreviousData
		{
			get { return _previousData ?? (_previousData = GetViewStateValue( "Job:PreviousData", new JobPrevDataDto() )); }
			set { _previousData = value; }
		}

		/// <summary>
		/// Gets or sets the locations.
		/// </summary>
		protected List<JobLocationDto> JobLocations
		{
			get { return _jobLocations ?? (_jobLocations = GetViewStateValue("Job:JobLocations", new List<JobLocationDto>())); }
			set { _jobLocations = value; }
		}

		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected JobController PageController { get { return PageControllerBaseProperty as JobController; } }

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		protected override IPageController RegisterPageController()
		{
			return new JobController(App);
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Get the job Id
			if (Page.RouteData.Values.ContainsKey("id"))
				long.TryParse(Page.RouteData.Values["id"].ToString(), out _jobId);

			if (_jobId == 0)
				throw new Exception(FormatError(ErrorTypes.InvalidJob, "Invalid job id"));

			if (App.User.EmployerId == null)
				throw new Exception(FormatError(ErrorTypes.InvalidEmployer, "Invalid employer id"));

      if (App.User.EmployeeId == null)
				throw new Exception(FormatError(ErrorTypes.InvalidEmployer, "Invalid employee id"));

		  if (!IsPostBack)
		  {
				LocaliseUI();
				Model = ServiceClientLocator.JobClient( App ).GetJobDetails( _jobId, true );
				PreviousData = ServiceClientLocator.JobClient( App ).GetJobPrevData( _jobId );

		    if (Model.EmployerId != App.User.EmployerId)
		      throw new Exception("Invalid access to job");

        JobLocations = ServiceClientLocator.JobClient(App).GetJobLocations(_jobId);
        ActivityList.JobId = _jobId;
        ActivityList.JobType = Model.JobType;
				ShowHideDifferencePane();
			}

			// Localise
			Page.Title = Model.JobType.IsInternship()
				? CodeLocalise("Page.Internship.Title", "Internship: {0}", Model.JobId)
				: CodeLocalise("Page.Job.Title", "Job: {0}", Model.JobId);

			PreviewJobButton.Text = CodeLocalise("PreviewJobButton.Text", "Preview");
			MatchJobButton.Text = CodeLocalise("MatchJobButton.Text", "View matches from the database");

			PostJobButton.Text = Model.JobType.IsInternship()
				? CodeLocalise("PostInternshipButton.Text", "Post internship")
				: CodeLocalise("PostJobButton.Text", "Post job");

      JobActivityLogPlaceHolder.Visible = ActivityList.Visible = App.Settings.DisplayJobActivityLogInTalent;
		}

		private void ShowHideDifferencePane()
		{
			DifferencePane.Visible = false;
			if (App.Settings.JobProofingEnabled)
			{
				if (PreviousData.JobId == Model.JobId)
				{
					var jobLocations = JobLocations.Aggregate(string.Empty, (current, jobLocation) => current + (jobLocation.Location.FormatForDisplayAsHtml() + "<br>"));

					var sb = new StringBuilder();
					GetDifferences(sb, PreviousData.ClosingDate, Model.ClosingDate, JobClosingDateLabel.Text);
					GetDifferences(sb, PreviousData.NumOpenings, Model.NumberOfOpenings, NumberOfOpeningsLabel.Text);
					GetDifferences(sb, PreviousData.Locations, jobLocations, JobLocationsLabel.Text);
					GetDifferences(sb, PreviousData.Description, Model.JobDescription, JobDescriptionLabel.Text);
					GetDifferences(sb, PreviousData.JobRequirements, Model.JobRequirements, JobRequirementsLabel.Text);
					GetDifferences(sb, PreviousData.JobSalaryBenefits, Model.JobSalaryBenefits, JobSalaryBenefitsLabel.Text);
					GetDifferences(sb, PreviousData.RecruitmentInformation, Model.JobRecruitmentInformation, JobRecruitmentInformationLabel.Text);
					Differences.Text = sb.ToString();
				}
			}
			TalentJobDetailTableCols.Controls.Clear();
			TalentJobDetailTableCols.InnerHtml = DifferencePane.Visible
				? "<col style='width:76%'/><col style='width:24%'/>" 
				: "<col style='width:100%'/>";
		}

		private static string PrepareStringForDiff(string str)
		{
			return str.Replace( "\r\n", "\n" ).Replace( "<br/>", "\n" ).Replace( "</br>", "\n" ).Replace( "<br>", "\n" ).Replace( "<br />", "\n" );
		}

		private void GetDifferences( StringBuilder sb, DateTime? oldDate, DateTime? newDate, string label )
		{
			var oldDateText = oldDate.HasValue ? oldDate.Value.ToString( "MMM d, yyyy" ) : string.Empty;
			var newDateText = newDate.HasValue ? newDate.Value.ToString( "MMM d, yyyy" ) : string.Empty;
			GetDifferences( sb, oldDateText, newDateText, label );
		}

		private void GetDifferences( StringBuilder sb, int? oldValue, int? newValue, string label )
		{
			var oldDateText = oldValue.HasValue ? oldValue.Value.ToString() : string.Empty;
			var newDateText = newValue.HasValue ? newValue.Value.ToString() : string.Empty;
			GetDifferences( sb, oldDateText, newDateText, label );
		}

		private void GetDifferences( StringBuilder sb, string oldText, string newText, string label )
		{
			if (oldText == newText)
			{
				return;
			}
			oldText = PrepareStringForDiff(oldText);
			newText = PrepareStringForDiff(newText);
			DifferencePane.Visible = true;
			sb.Append( "<h4>" );
			sb.Append( label );
			sb.Append( "</h4><ul>" );
			var diffTool = new Differ();
			var diffs = diffTool.CreateWordDiffs(oldText, newText, true, false, DiffDelimeters);
			var deletionCount = 0;
			var insertionCount = 0;
			var changeCount = 0;
			foreach (var diffBlock in diffs.DiffBlocks)
			{
				if (diffBlock.InsertCountB > 0)
				{
					if (diffBlock.DeleteCountA > 0)
					{
						changeCount++;
					}
					else
					{
						insertionCount++;
					}
				}
				else
				{
					deletionCount++;
				}
			}
			if (deletionCount > 0)
			{
				sb.AppendFormat("<li>{0} deletions</li>", deletionCount);
			}
			if (insertionCount > 0)
			{
				sb.AppendFormat("<li>{0} insertions</li>", insertionCount);
			}
			if (changeCount > 0)
			{
				sb.AppendFormat("<li>{0} amendments</li>", changeCount);
			}
			sb.Append( "</ul>" );
		}

		private static string ShowDifferencesText( string oldText, string newText )
		{
			var diffTool = new Differ();
			oldText = PrepareStringForDiff( oldText );
			newText = PrepareStringForDiff( newText );
			var diffs = diffTool.CreateWordDiffs( oldText, newText, true, false, DiffDelimeters );
			return diffs.BuildDiffText().Replace( "\n", "\r\n" );
		}

		private string ShowTextWithDifferences(DateTime? oldDate, DateTime? newDate)
		{
			var oldDateText = oldDate.HasValue ? oldDate.Value.ToString( "MMM d, yyyy" ) : CodeLocalise( "JobClosingDate.NotDefined", "Not defined" );
			var newDateText = newDate.HasValue ? newDate.Value.ToString( "MMM d, yyyy" ) : CodeLocalise( "JobClosingDate.NotDefined", "Not defined" );
			return ShowTextWithDifferences(oldDateText, newDateText);
		}

		private string ShowTextWithDifferences( int? oldValue, int? newValue )
		{
			var oldDateText = oldValue.HasValue ? oldValue.Value.ToString() : CodeLocalise( "NumberOfOpenings.NotDefined", "Not defined" );
			var newDateText = newValue.HasValue ? newValue.Value.ToString() : CodeLocalise( "NumberOfOpenings.NotDefined", "Not defined" );
			return ShowTextWithDifferences( oldDateText, newDateText );
		}

		private string ShowTextWithDifferences(string oldValue, string newValue)
		{
			var text = new StringBuilder();
			if (App.Settings.JobProofingEnabled)
			{

				if (PreviousData.JobId != Model.JobId)
				{
					text.Append(newValue);
				}
				else
				{
					if (oldValue.IsNotNullOrEmpty())
					{
						text.Append(newValue.IsNotNullOrEmpty()
							? ShowDifferencesText(oldValue, newValue)
							: ShowDifferencesText(oldValue, string.Empty));
					}
					else
					{
						text.Append(newValue.IsNotNullOrEmpty()
							? ShowDifferencesText(string.Empty, newValue)
							: string.Empty);
					}
				}
			}
			else
			{
				text.Append(newValue);
			}
			return text.ToString();
		}

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			// Bind the job
			Bind();

			// Localise text
			LocaliseUI();
			ApplyTheme();

			// Persist the Model
			SetViewStateValue("Job:Model", _model);
			SetViewStateValue("Job:JobLocations", _jobLocations);
			SetViewStateValue("Job:PreviousData", _previousData);
		}

		/// <summary>
		/// Localises text on the page
		/// </summary>
		private void LocaliseUI()
		{
			NumberOfOpeningsLabel.Text = HtmlLocalise( "NumberOfOpenings.Label", "Number of openings" );
			JobRequirementsLabel.Text = HtmlLocalise("JobRequirements.Label", "Job requirements");
			JobSalaryBenefitsLabel.Text = HtmlLocalise("JobSalaryBenefits.Label", "Job salary and benefits");
			JobRecruitmentInformationLabel.Text = HtmlLocalise("JobRecruitmentInformation.Label", "Job recruitment information");
			if( Model.JobType.IsInternship() )
			{
				JobDetailsLabel.Text = HtmlLocalise("InternshipDetails.Header", "Internship details");
				JobClosingDateLabel.Text = HtmlLocalise("InternshipClosingDate.Label", "Internship closing date");
				JobIdLabel.Text = HtmlLocalise("InternshipReference.Label", "Internship ID");
				JobLocationsLabel.Text = HtmlLocalise("InternshipLocation.Label", "Internship Locations");
				JobDescriptionLabel.Text = HtmlLocalise("InternshipDescription.Label", "Internship Description");
				ForeignLabourCertificationLabel.Text = HtmlLocalise("IntenshipForeignLabourCertification.Label", "Internship condition");
        JobActivityLogHeaderLabel.Text = HtmlLocalise("JobOrderActivityLog.Label", "Internship Activity Log");
			}
			else
			{
				JobDetailsLabel.Text = HtmlLocalise("JobOrderDetails.Header", "#EMPLOYMENTTYPE# details");
				JobClosingDateLabel.Text = HtmlLocalise("JobClosingDate.Label", "Job closing date");
				JobIdLabel.Text = HtmlLocalise("JobReference.Label", "Job ID");
				JobLocationsLabel.Text = HtmlLocalise("JobLocation.Label", "Job locations");
				JobDescriptionLabel.Text = HtmlLocalise("JobDescription.Label", "Job description");
				ForeignLabourCertificationLabel.Text = HtmlLocalise("JobForeignLabourCertification.Label", "Job condition");
				JobActivityLogHeaderLabel.Text = HtmlLocalise("JobOrderActivityLog.Label", "#EMPLOYMENTTYPE# Activity Log");
      }
		}

		/// <summary>
		/// Hides/Shows controls based on the theme
		/// </summary>
		private void ApplyTheme()
		{
			
		}

		private static string BuildJobConditionsText(string foreignLabourCertificationDetails, string courtOrderedAffirmativeActionDetails, string federalContractorDetails)
		{
			string jobConditions = null;
			if (foreignLabourCertificationDetails.IsNotNullOrEmpty() || courtOrderedAffirmativeActionDetails.IsNotNullOrEmpty() || federalContractorDetails.IsNotNullOrEmpty())
			{
				jobConditions = foreignLabourCertificationDetails;

				if (jobConditions.IsNotNullOrEmpty() && courtOrderedAffirmativeActionDetails.IsNotNullOrEmpty())
					jobConditions += ", ";

				jobConditions += courtOrderedAffirmativeActionDetails;

				if (jobConditions.IsNotNullOrEmpty() && federalContractorDetails.IsNotNullOrEmpty())
					jobConditions += ", ";

				jobConditions += federalContractorDetails;
			}
			return jobConditions;
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			JobTitle.Text = Model.JobTitle;
			StatusDetails.Text = FormatStatusDetails();

			JobClosingDate.Text = ShowTextWithDifferences(PreviousData.ClosingDate, Model.ClosingDate);

			switch (Model.JobType)
			{
				case JobTypes.Job:
					PositionDetails.Text = CodeLocalise("PaidPosition.Text", "paid position");
					break;

				case JobTypes.InternshipPaid:
					PositionDetails.Text = CodeLocalise("PaidInternship.Text", "paid internship");
					break;

				case JobTypes.InternshipUnpaid:
					PositionDetails.Text = CodeLocalise("UnpaidInternship.Text", "unpaid internship");
					break;
			}

			JobReference.Text = Model.JobId.ToString();
			var prevJobConditions = BuildJobConditionsText(PreviousData.ForeignLabourCertificationDetails, PreviousData.CourtOrderedAffirmativeActionDetails, PreviousData.FederalContractorDetails);
			var jobConditions = BuildJobConditionsText(Model.ForeignLabourCertificationDetails, Model.CourtOrderedAffirmativeActionDetails, Model.FederalContractorDetails);
			if( prevJobConditions != null )
			{
				ForeignLabourCertification.Text = ShowTextWithDifferences(prevJobConditions, jobConditions ?? string.Empty);
			}
			else
			{
				if( jobConditions != null )
				{
					ForeignLabourCertification.Text = ShowTextWithDifferences( string.Empty, jobConditions );
				}
				else
				{
					ForeignLabourCertificationLabel.Visible = ForeignLabourCertification.Visible = SemiColonLiteral.Visible = false;
				}
			}

			if (Model.JobStatus == JobStatuses.Active && Model.VeteranPriorityEndDate.HasValue && Model.VeteranPriorityEndDate >= DateTime.Now)
			{
				switch (Model.ExtendVeteranPriority.GetValueOrDefault(ExtendVeteranPriorityOfServiceTypes.None))
				{
					case ExtendVeteranPriorityOfServiceTypes.None:
						VeteranPriorityLiteral.Text = CodeLocalise("VeteranPriorityLiteral.Auto", "This posting is currently available to veteran job seekers only until {0} {1}", Model.VeteranPriorityEndDate.Value.ToString("MMM d, yyyy"), Model.VeteranPriorityEndDate.Value.ToShortTimeString());
						break;
					case ExtendVeteranPriorityOfServiceTypes.ExtendUntil:
						VeteranPriorityLiteral.Text = CodeLocalise("VeteranPriorityLiteral.ExtendUntil", "This posting is currently available to veteran job seekers only until {0}", Model.VeteranPriorityEndDate.Value.ToString("MMM d, yyyy"));
						break;
					case ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting:
						VeteranPriorityLiteral.Text = CodeLocalise("VeteranPriorityLiteral.ExtendForLifeOfPosting", "This posting is currently available to veteran job seekers only until {0}", Model.ClosingDate.GetValueOrDefault().ToString("MMM d, yyyy"));
						break;
				}
			}
			else
			{
				VeteranPriorityRow.Visible = false;
			}

			NumberOfOpenings.Text = ShowTextWithDifferences( PreviousData.NumOpenings, Model.NumberOfOpenings );
			JobDescription.Text = ShowTextWithDifferences( PreviousData.Description, Model.JobDescription ).FormatForDisplayAsHtml();

			var locationsText = JobLocations.Aggregate( string.Empty, ( current, jobLocation ) => string.Format( "{0}{1}<br>", current, jobLocation.Location.FormatForDisplayAsHtml() ) );
			Locations.Text = ShowTextWithDifferences( PreviousData.Locations, locationsText ).FormatForDisplayAsHtml();

			#region Requirements/Details/Salary And Benefits/Recruitment Information

			JobRequirements.Text = ShowTextWithDifferences( PreviousData.JobRequirements, Model.JobRequirements ).FormatForDisplayAsHtml();
			JobDetails.Text = ShowTextWithDifferences( PreviousData.JobDetails, Model.JobDetails ).FormatForDisplayAsHtml();
			JobSalaryBenefits.Text = ShowTextWithDifferences( PreviousData.JobSalaryBenefits, Model.JobSalaryBenefits ).FormatForDisplayAsHtml();
			JobRecruitmentInformation.Text = ShowTextWithDifferences( PreviousData.RecruitmentInformation, Model.JobRecruitmentInformation ).FormatForDisplayAsHtml();

			JobRequirementsHeaderRow.Visible = JobRequirementsRow.Visible = Model.JobRequirements.IsNotNullOrEmpty() || PreviousData.JobRequirements.IsNotNullOrEmpty();
			JobDetailsHeaderRow.Visible = JobDetailsRow.Visible = Model.JobDetails.IsNotNullOrEmpty();
			JobSalaryBenefitsHeaderRow.Visible = JobSalaryBenefitsRow.Visible = Model.JobSalaryBenefits.IsNotNullOrEmpty();
			JobRecruitmentInformationHeaderRow.Visible = JobRecruitmentInformationRow.Visible = Model.JobRecruitmentInformation.IsNotNullOrEmpty();

			#endregion

			var talentPoolSearch = Utilities.ShowTalentPoolSearch();

			HoldJobPanel.Visible = Model.JobStatus.IsNotIn(JobStatuses.OnHold, JobStatuses.Draft, JobStatuses.AwaitingEmployerApproval, JobStatuses.Closed);
			ReactivateJobPanel.Visible = (Model.ApprovalStatus != ApprovalStatuses.Rejected && Model.ApprovalStatus != ApprovalStatuses.WaitingApproval && Model.JobStatus == JobStatuses.OnHold);
			CloseJobPanel.Visible = Model.JobStatus.IsNotIn(JobStatuses.Draft, JobStatuses.AwaitingEmployerApproval, JobStatuses.Closed);
			RefreshJobPanel.Visible = Model.JobStatus == JobStatuses.Active || (Model.JobStatus == JobStatuses.Closed && Model.AutoClosed && Model.ClosingDate.HasValue && (DateTime.Now - (DateTime)Model.ClosingDate).TotalDays <= 30);
			DuplicateJobPanel.Visible = (Model.JobStatus != JobStatuses.Draft);

			MatchJobButton.Visible = (Model.JobStatus != JobStatuses.Draft) && talentPoolSearch;
			DeleteJobPanel.Visible = (Model.JobStatus == JobStatuses.Draft);
			EditJobPanel.Visible = (Model.JobStatus != JobStatuses.Closed);

			PostJobButton.Visible = (Model.JobStatus == JobStatuses.Draft && Model.ApprovalStatus == ApprovalStatuses.Approved);

      if (Model.HiringManagerId != App.User.EmployeeId)
      {
        HoldJobPanel.Visible =
          ReactivateJobPanel.Visible =
          CloseJobPanel.Visible =
          RefreshJobPanel.Visible =
          EditJobPanel.Visible =
          DeleteJobPanel.Visible =
					PreviewJobButton.Visible = 
          MatchJobButton.Visible = false;
      }
		}

		/// <summary>
		/// Formats the status details.
		/// </summary>
		/// <returns></returns>
		private string FormatStatusDetails()
		{
			var applicationCount = Model.VeteranPriorityEndDate.GetValueOrDefault(DateTime.MinValue) > DateTime.Now
															 ? Model.VeteranApplicationCount
															 : Model.ApplicationCount;

			if (applicationCount == 0)
				return Model.JobType.IsInternship()
					? CodeLocalise("InternshipStatusDetails.0.Label", "There are no applicants for this internship.")
					: CodeLocalise("JobStatusDetails.0.Label", "There are no applicants for this job.");

			var applicantsLink = String.Format(@"<a id=""applicant"" href=""{0}"">{1}</a>", UrlBuilder.TalentPool(PoolResult.Tabs.Applicants, _jobId), applicationCount == 1 ? CodeLocalise("Applicant.Label", "applicant") : CodeLocalise("Applicants.Label", "applicants"));
		    if (Model.HiringManagerId != App.User.EmployeeId)
                applicantsLink = String.Format(@applicationCount == 1 ? CodeLocalise("Applicant.Label", "applicant") : CodeLocalise("Applicants.Label", "applicants"));

			return Model.JobType.IsInternship()
				? string.Format(applicationCount == 1 ? CodeLocalise("InternshipStatusDetails.1.Label", "There is {0} {1} for this internship.") : CodeLocalise("StatusDetails.N.Label", "There are {0} {1} for this internship."), applicationCount, applicantsLink)
				: string.Format(applicationCount == 1 ? CodeLocalise("JobStatusDetails.1.Label", "There is {0} {1} for this job.") : CodeLocalise("StatusDetails.N.Label", "There are {0} {1} for this job."), applicationCount, applicantsLink);
		}

		/// <summary>
		/// Handles the Command event of the JobAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void JobAction_Command(object sender, CommandEventArgs e)
		{
			var jobType = Model.JobType.IsInternship() ? CodeLocalise("Global.Internship.Text", "internship") : CodeLocalise("Global.Job.Text", "job");

			switch (e.CommandName)
			{
				case "RefreshJob":
					// Refresh the posted date
          JobAction.Show(ActionTypes.RefreshJob, Model.JobId, Model.JobTitle, Model.JobType, numberOfOpenings: Model.NumberOfOpenings);
					break;

				case "CloseJob":
					// Close - Shows the close job modal
					JobAction.Show(ActionTypes.CloseJob, Model.JobId, Model.JobTitle, Model.JobType);
					break;

				case "HoldJob":
					// Hold – moves job to hold tab and shows system status message
					Confirmation.Show(CodeLocalise("HoldConfirmation.Title", "Hold Posting"),
														CodeLocalise("HoldConfirmation.Body", "Are you sure you want to hold {0}?", Model.JobTitle),
														CodeLocalise("Global.Cancel.Text", "Cancel"),
														okText: CodeLocalise("HoldConfirmation.Ok.Text", "Hold posting"), okCommandName: "Hold", okCommandArgument: Model.JobId.ToString());
					break;

				case "DuplicateJob":
					// Duplicate – creates a new job at the top of the list.
					Handle(PageController.QueryDuplicateJob(_jobId, Model.JobTitle));
					break;

				case "DeleteJob":
					Confirmation.Show(CodeLocalise("DeleteConfirmation.Title", "Delete {0}", jobType.ToTitleCase()),
									CodeLocalise("DeleteConfirmation.Body", "Are you sure you want to delete the {0} {1}?", jobType, Model.JobTitle),
									CodeLocalise("Global.Cancel.Text", "Cancel"),
									okText: CodeLocalise("DeleteConfirmation.Ok.Text", "Delete {0}", jobType.ToTitleCase()),
									height: 75,
									okCommandName: "Delete",
									okCommandArgument: Model.JobId.ToString(CultureInfo.InvariantCulture));
					break;

				case "ReactivateJob":
					// Reactivate – moves the job to the Active tab and triggers a status message about the reactivation.
          JobAction.Show(ActionTypes.ReactivateJob, Model.JobId, Model.JobTitle, Model.JobType);
					break;

				case "EditJob":
					// Edit the job using the job wizard
					Response.Redirect(UrlBuilder.JobWizard(Model.JobId));
					break;

				case "MatchJob":
					// Match the job against the database of candidates
					Response.Redirect(UrlBuilder.TalentPool(jobId: Model.JobId));
					break;

				case "PreviewJob":
					var job = ServiceClientLocator.JobClient(App).GetJob(Model.JobId);
					job.PostingHtml = job.PostingHtml.Replace("#POSTINGFOOTER#", "");
					Handle(PageController.PreviewPosting(job, true));
					break;

				case "PostJob":
					//Get the full model and pass it to the preview modal
					Handle(PageController.PreviewPosting(ServiceClientLocator.JobClient(App).GetJob(Model.JobId), allowPost: true));
					break;
			}
		}

		/// <summary>
		/// Handles the Completed event of the JobAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobAction_Completed(object sender, EventArgs e)
		{
      Response.Redirect(UrlBuilder.JobView(_jobId));
		}

		/// <summary>
		/// Handles the OkCommand event of the Confirmation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_OkCommand(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Hold":
					// Do the Holding
					ServiceClientLocator.JobClient(App).HoldJob(Model.JobId);
          Response.Redirect(UrlBuilder.JobView(_jobId));
					break;

				case "Delete":
					// Do the Deletion
					ServiceClientLocator.JobClient(App).DeleteJob(Model.JobId);
					Response.Redirect(UrlBuilder.TalentJobs());
					break;
			}
		}
	}
}