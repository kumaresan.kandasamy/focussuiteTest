﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;
using System.Web.Security;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Web.Code;
using Framework.Logging;
using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebTalent
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)] 
	public partial class UpdateContactInformation : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// TODO: There is a very similar page to this in Assist (ContactInformation.aspx).  Need to investigate whether a common control can be created

      if (App.Settings.DisableTalentAuthentication)
        throw new Exception("Not authorised");

			if (!IsPostBack)
			{
				LocaliseUI();
				ContactInformationMain.Bind(App.User.UserId);
			}
		}

		/// <summary>
		/// Handles the Click event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Click(object sender, EventArgs e)
		{
			try
			{
				var model = ContactInformationMain.Unbind();

				bool failedCensorshipCheck;
				var userData = ServiceClientLocator.AccountClient(App).UpdateContactDetails(model, true, out failedCensorshipCheck);

				// If we're updating our own user then reset authentication cookie with the details
				if (userData.Id == App.User.UserId)
				{
					var cookie = FormsAuthentication.GetAuthCookie(model.PersonDetails.FirstName, false);

					// Store UserData inside the Forms Ticket with all the attributes in sync with the web.config
					var newticket = new FormsAuthenticationTicket(1, model.PersonDetails.FirstName, DateTime.Now,
																												DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false,
																												App.User.SerializeUserContext());

					// Encrypt the ticket and store it in the cookie
					cookie.Value = FormsAuthentication.Encrypt(newticket);

					Context.Response.Cookies.Set(cookie);
				}

				if (failedCensorshipCheck)
				{
					ConfirmationModal.Show(CodeLocalise("CensorshipCheck.Title", "Contact details change requires approval"),
																 CodeLocalise("CensorshipCheck.Body", "Our staff will review your account amendment request and get back to you shortly. In the meantime, any active jobs will retain existing details. Once approved, these job listings can be amended by re-posting."),
																 CodeLocalise("CloseModal.Text", "Return to account settings"));
				}
				else
				{
					ConfirmationModal.Show(CodeLocalise("SuccessfulUpdate.Title", "Contact details changed"),
																 CodeLocalise("SuccessfulUpdate.Body", "You have successfully changed your contact details."),
																 CodeLocalise("CloseModal.Text", "Return to account settings"));
				}
			}
			catch (Exception ex)
			{

				ConfirmationModal.Show(CodeLocalise("ContactUpdateError.Title", "Contact update failed"),
						CodeLocalise("ContactUpdateError.Body", ex.Message),
						CodeLocalise("CloseModal.Text", "OK"));
				
			}
			
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SaveButton.Text = CodeLocalise("Global.Save.Label", "Save");
		}
	}
}