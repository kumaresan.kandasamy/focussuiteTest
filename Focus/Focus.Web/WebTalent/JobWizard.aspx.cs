﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;

using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebTalent
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)] 
	public partial class JobWizard : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				long currentJobId = 0;
				if (Page.RouteData.Values.ContainsKey("id"))
					long.TryParse(Page.RouteData.Values["id"].ToString(), out currentJobId);

				Wizard.JobId = currentJobId;
				Wizard.HiringManagerId = (App.User.EmployeeId.HasValue) ? App.User.EmployeeId.Value : 0;
			}

			Page.Title = CodeLocalise("Page.Title", "Job wizard");
		  Navigation.DisablePostJobButton = true;
		}
	}
}