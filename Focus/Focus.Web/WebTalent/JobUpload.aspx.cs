﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Upload;
using Focus.Core.Models.Validation;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebTalent
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)] 
  public partial class JobUpload : PageBase
  {
    private long UploadFileId
    {
      get { return GetViewStateValue<long>("JobUpload:UploadFileId"); }
      set { SetViewStateValue("JobUpload:UploadFileId", value); }
    }

    /// <summary>
    /// Fires when the page is loading
    /// </summary>
    /// <param name="sender">Page raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Utilities.ShowJobUpload(App.User.EmployeeId))
        Response.RedirectToRoute("Default");

      if (!IsPostBack)
      {
        LocaliseUI();
        BindControls();
      }
      else
      {
        if (ProcessingComplete.Value == "1")
          ShowResultsPanel();
      }

      Page.Title = CodeLocalise("Page.Title", "Upload mulitple #POSTINGTYPES#");
      Navigation.DisableUploadJobButton = true;
    }

    /// <summary>
    /// Localises the controls on the page
    /// </summary>
    private void LocaliseUI()
    {
      DownloadTemplateButton.Text = CodeLocalise("DownloadTemplateButton.Text", "Download template");
      HelpLabel.Text = HtmlLocalise("HelpLiteral.Text", "Please ensure the file has a header row, and at least one job record");
			SelectEmployerLiteral.Text = HtmlRequiredLabel(BusinessUnitDropdown, "SelectEmployerLabel.Text", "#BUSINESS#/Business Unit:");

      UploadFileRequiredValidator.Text = CodeLocalise("UploadFileRequiredValidator.Text", "An upload file must be selected");
      UploadFileTypeValidator.Text = CodeLocalise("UploadFileTypeValidator.Text", "File must either be .csv, .xls or .xlsx file");
			BusinessUnitDropdownValidator.Text = CodeLocalise("BusinessUnitDropdownValidator.Text", "An #BUSINESS#/Business Unit must be selected");
      MeetsMinimumWageRequirementCheckBoxText.Text = HtmlRequiredLabel(MeetsMinimumWageRequirementCheckBox, "MeetsMinimumWageRequirementCheckBox.Text", "I confirm that all the job postings in this file meet the states minimum-wage requirement");
      MeetsMinimumWageRequirementValidator.ErrorMessage = CodeLocalise("MeetsMinimumWageRequirementValidator.ErrorMessage", "All job postings in this file must meet the states minimum-wage requirement");

      FileProcessingLiteral.Text = HtmlLocalise("FileProcessingLiteral.Text", "The file is being validated...");

      FileNameHeader.Text = HtmlLocalise("FileName.Title", "File name:");
			EmployerNameHeader.Text = HtmlLocalise("Employer.Title", "#BUSINESS#/Business Unit:");

      UploadStatisticsHeader.Text = HtmlLocalise("UploadStatistics.Text", "Upload statistics");
      ActiveJobsHeader.Text = HtmlLocalise("ActiveJobs.Text", "Active #POSTINGTYPES#:LOWER");
      YellowWordsHeader.Text = HtmlLocalise("YellowWords.Text", "#POSTINGTYPES# pending approval because of yellow words");
      RedWordsHeader.Text = HtmlLocalise("RedWords.Text", "Draft #POSTINGTYPES#:LOWER with red words");
      CriminalBackgroundDisclaimerLink.Text = HtmlLocalise("CriminalBackgroundDisclaimerLink.Text", "Legal notices");
			CreditChecksRequiredHeader.Text = HtmlLocalise( "CreditChecksRequired.Text", "#POSTINGTYPES# that require credit checks" );
			CriminalChecksRequiredHeader.Text = HtmlLocalise( "CriminalChecksRequired.Text", "#POSTINGTYPES#  that require criminal background checks" );
      JobsInErrorHeader.Text = HtmlLocalise("JobsInError.Text", "#POSTINGTYPES# not created/in error");
      TotalJobRowsHeader.Text = HtmlLocalise("TotalJobRows.Text", "Total number of #POSTINGTYPES#:LOWER in file");

      NoResultsLabel.Text = HtmlLocalise("NoResultsLabel.Text", "No #POSTINGTYPES#:LOWER found in the file");

      CancelButton.Text = CodeLocalise("CancelButton.Text", "Cancel");
      UploadButton.Text = CodeLocalise("UploadButton.Text", "Upload");
      CommitButton.Text = CodeLocalise("CommitButton.Text", "Upload");
      ExceptionLogButton.Text = CodeLocalise("ExceptionLogButton.Text", "Error log...");

      UploadDisclaimerText.Text = HtmlLocalise("UploadDisclaimer.Text", "By clicking 'upload',  you confirm that you have reviewed the legal notices and that the postings in this file that require background checks on applicants can be posted as-is");
    }

    /// <summary>
    /// Binds controls on the page to data sources
    /// </summary>
    private void BindControls()
    {
      var businessUnits = ServiceClientLocator.EmployeeClient(App).GetEmployeesBusinessUnits(App.User.EmployeeId.GetValueOrDefault(0));

      BusinessUnitDropdown.DataSource = businessUnits;
      BusinessUnitDropdown.DataValueField = "BusinessUnitId";
      BusinessUnitDropdown.DataTextField = "BusinessUnitName";
      BusinessUnitDropdown.DataBind();
			BusinessUnitDropdown.Items.AddLocalisedTopDefault("Global.Company.TopDefault", "- select a #BUSINESS#:LOWER -");

      var unitCount = BusinessUnitDropdown.Items.Count;
      if (unitCount > 1)
      {
        if (unitCount == 2)
        {
          BusinessUnitDropdown.SelectedIndex = 1;
        }
        else
        {
          var primaryUnit = businessUnits.FirstOrDefault(bu => bu.IsDefault);
          if (primaryUnit.IsNotNull())
            BusinessUnitDropdown.SelectValue(primaryUnit.BusinessUnitId.ToString());
        }
      }
    }

    /// <summary>
    /// Handles when the download template button is clicked
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void DownloadTemplateButton_OnClick(object sender, EventArgs e)
    {
      var template = ServiceClientLocator.CoreClient(App).GetCSVTemplateForSchema(ValidationSchema.JobUpload);

      Response.Clear();
      Response.AddHeader("Content-Type", "text/csv");
      Response.AddHeader("Content-Disposition", string.Format("attachment; filename=JobUploadTemplate.csv; size={0}", template.Length));
      Response.Write(template);
      Response.End();
    }

    /// <summary>
    /// Handles when the cancel button is clicked
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void CancelButton_OnClick(object sender, EventArgs e)
    {
      RedirectToDashboard();
    }

    /// <summary>
    /// Handles when the upload button is clicked
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void UploadButton_OnClick(object sender, EventArgs e)
    {
      var uploadFileBytes = UploadFile.FileBytes;

      var fileType = UploadFile.FileName.EndsWith(".csv", StringComparison.OrdinalIgnoreCase)
                       ? UploadFileType.CSV
                       : UploadFileType.Excel;

      var customInfo = new Dictionary<string, string>
      {
        {"EmployeeId", App.User.EmployeeId.ToString()},
        {"BusinessUnitId", BusinessUnitDropdown.SelectedValue}
      };

      var uploadFileId = ServiceClientLocator.CoreClient(App).ValidateUploadFile(UploadFile.FileName, ValidationSchema.JobUpload, uploadFileBytes, fileType, customInfo, true);

      UploadFileId = uploadFileId;

      ShowProcessingPanel();
    }

    /// <summary>
    /// Handles when the exception log button is clicked
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void ExceptionLogButton_OnClick(object sender, EventArgs e)
    {
      var fieldNameLookup  = new Dictionary<int, string>();

      var schema = ServiceClientLocator.CoreClient(App).GetValidationSchema(ValidationSchema.JobUpload);
      var root = schema.Root;

      if (root.IsNotNull())
      {
        var fieldElements = root.Descendants("field");
        var index = 1;
        foreach (var field in fieldElements)
        {
          fieldNameLookup.Add(index, field.Attribute("displayname").Value);
          index++;
        }
      }

      var results = ServiceClientLocator.CoreClient(App).GetUploadFileDetails(UploadFileId, true);
      var output = new StringBuilder();

      output.Append(CodeLocalise("RowNumber.Text", "Row number"));
      output.Append(",");
      output.Append(CodeLocalise("ReasonForException.Text", "Reason for exception"));
      output.Append(",");
      output.Append(CodeLocalise("JobTitle.Text", "Job title"));
      output.Append(",");
      output.Append(CodeLocalise("FieldName.Text", "Field in exception"));
      output.Append(",");
      output.AppendLine(CodeLocalise("Exception.Text", "Exception details"));

      results.ForEach(row =>
      {
        var fieldValidation = (List<FieldValidationResult>)row.FieldValidation.Deserialize(typeof (List<FieldValidationResult>));
        var recordData = (JobUploadRecord)row.FieldData.Deserialize(typeof(JobUploadRecord));

        if (row.ValidationMessage.IsNotNullOrEmpty())
        {
          output.Append(row.RecordNumber);
          output.Append(",");
          output.Append(CodeLocalise(ValidationRecordStatus.SchemaFailure));
          output.Append(",");
          output.Append(recordData.JobTitle);
          output.Append(",,");
          if (row.ValidationMessage.Contains(","))
          {
            output.Append("\"");
            output.Append(row.ValidationMessage);
            output.AppendLine("\"");
          }
          else
          {
            output.AppendLine(row.ValidationMessage);  
          }
        }

        foreach (var invalidField in fieldValidation.Where(fieldResult => fieldResult.Status != ValidationRecordStatus.Success))
        {
          output.Append(row.RecordNumber);
          output.Append(",");
          
          var statuses = Enum.GetValues(typeof(ValidationRecordStatus))
                             .Cast<ValidationRecordStatus>()
                             .Where(f => f != ValidationRecordStatus.Success && (f & invalidField.Status) == f)
                             .ToList();

          if (statuses.Count > 1)
            output.Append("\"");
          output.Append(string.Join(",", statuses.Select(status => CodeLocalise(status))));
          if (statuses.Count > 1)
            output.Append("\"");

          output.Append(",");
          output.Append(recordData.JobTitle);
          output.Append(",");
          output.Append(fieldNameLookup[invalidField.FieldNumber]);
          output.Append(",");
          if (invalidField.Message.Contains(","))
          {
            output.Append("\"");
            output.Append(invalidField.Message);
            output.AppendLine("\"");
          }
          else
          {
            output.AppendLine(invalidField.Message);
          }
        }
      });

      Response.Clear();
      Response.AddHeader("Content-Type", "text/csv");
      Response.AddHeader("Content-Disposition", string.Format("attachment; filename=JobUploadExceptionLog.csv; size={0}", output.Length));
      Response.Write(output.ToString());
      Response.End();
    }

    /// <summary>
    /// Handles when the commit button is clicked
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void CommitButton_OnClick(object sender, EventArgs e)
    {
      ServiceClientLocator.CoreClient(App).CommitUploadFile(UploadFileId);

      UploadConfirmationModal.Show(CodeLocalise("UploadConfirmation.Title", "Thank you for uploading your #POSTINGTYPE#:LOWER postings"),
                                   CodeLocalise("UploadConfirmation.Body", "The process may take a few minutes.<br /><br />However, once it is completed, you may access your uploaded jobs at any time from your home page, where you can make changes (including refreshing a posting date or changing an expiration date), put a job on hold, or close it."),
                                   CodeLocalise("UploadConfirmation.Close", "Return to dashboard"),
                                   "Dashboard");
    }

    /// <summary>
    /// Handles the OnCloseCommand event of the Confirmation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void UploadConfirmationModal_OnCloseCommand(object sender, CommandEventArgs eventargs)
    {
      RedirectToDashboard();
    }

    /// <summary>
    /// Shows the processing panel
    /// </summary>
    private void ShowProcessingPanel()
    {
      InitialControls.Visible = UploadButton.Visible = false;
      FileSummaryHeaderPlaceHolder.Visible = true;

      UploadFileName.Text = UploadFile.FileName;
      EmployerName.Text = BusinessUnitDropdown.SelectedItem.Text;

      var postBackOptions  = new PostBackOptions(this)
      {
        AutoPostBack = false,
        RequiresJavaScriptProtocol = true,
        PerformValidation = false
      };

      // Add the client-side script to the post the page back
      var js = string.Format(@"
        $(document).ready(function () {{ 
          $('#UpdateProgressIndicator').show(); 
          JobUpload_CheckProgress('{0}/GetUploadFileProcessingState', {1}); 
        }});

        function JobUpload_ProcessingComplete() {{
          $('#{2}').val('1');
          {3};
        }}", 
           UrlBuilder.AjaxService(),
           UploadFileId,
           ProcessingComplete.ClientID,
           Page.ClientScript.GetPostBackEventReference(postBackOptions));

      Page.ClientScript.RegisterClientScriptBlock(GetType(), "ProcessingComplete", js, true);

      ProgressBarPlaceHolder.Visible = true;
    }

    /// <summary>
    /// Shows the result panel
    /// </summary>
    private void ShowResultsPanel()
    {
      ValidationResultsPlaceHolder.Visible = CommitButton.Visible = true;
      ProgressBarPlaceHolder.Visible = false;

      ProcessingComplete.Value = "2";

      var results = ServiceClientLocator.CoreClient(App).GetUploadFileDetails(UploadFileId, false);

      var totalRecords = results.Count;
      if (totalRecords == 0)
      {
        UploadStatisticsPlaceHolder.Visible = CommitButton.Visible = false;
        NoResultsPlaceHolder.Visible = true;
      }
      else
      {
        var successRecords = ShowTotalRow(ActiveJobsRow, ActiveJobsTotal, results, ValidationRecordStatus.Success);
        ShowTotalRow(YellowWordsRow, YellowWordsTotal, results, ValidationRecordStatus.YellowWords);
        ShowTotalRow(RedWordsRow, RedWordsTotal, results, ValidationRecordStatus.RedWords);

				var criminalTotal = ShowCriminalAndCreditChecksRequiredTotalRows( CreditChecksRequiredRow, CriminalChecksRequiredRow, CreditChecksRequiredTotal, CriminalChecksRequiredTotal, results );

        var jobsInErrorTotal = ShowTotalRow(JobsInErrorRow, JobsInErrorTotal, results, ValidationRecordStatus.SchemaFailure);

        TotalJobRowsTotal.Text = totalRecords.ToString(CultureInfo.CurrentUICulture);

        UploadDisclaimerPlaceHolder.Visible = (criminalTotal > 0);
        DisclaimerImage.ImageUrl = UrlBuilder.InformationPanelIcon();

        ExceptionLogButton.Visible = successRecords < totalRecords;

        if (jobsInErrorTotal == totalRecords)
          CommitButton.Visible = false;
      }
    }

		/// <summary>
		/// Show hide sections for Credit Check and Criminal background check required counts
		/// </summary>
		/// <param name="creditChecksRequiredRow">table row for credit check required total</param>
		/// <param name="criminalChecksRequiredRow">table row for criminal background check required total</param>
		/// <param name="creditChecksRequiredTotalLabel">label for credit check required total</param>
		/// <param name="criminalChecksRequiredTotalLabel">label for criminal background check required total</param>
		/// <param name="results"></param>
	  private static int ShowCriminalAndCreditChecksRequiredTotalRows( Control creditChecksRequiredRow, Control criminalChecksRequiredRow, ITextControl creditChecksRequiredTotalLabel, ITextControl criminalChecksRequiredTotalLabel, IEnumerable<UploadRecordDto> results )
		{
			var creditChecksRequiredTotal = 0;
			var criminalChecksRequiredTotal = 0;
			foreach (var uploadRecord in results.Select(record => (JobUploadRecord)record.FieldData.Deserialize(typeof(JobUploadRecord))))
			{
				if (uploadRecord.ApplicantCreditCheck ?? false)
				{
					creditChecksRequiredTotal++;
				}
				if( uploadRecord.ApplicantCriminalCheck ?? false )
				{
					criminalChecksRequiredTotal++;
				}
			}

			creditChecksRequiredRow.Visible = (creditChecksRequiredTotal > 0);
			creditChecksRequiredTotalLabel.Text = creditChecksRequiredTotal.ToString( CultureInfo.CurrentUICulture );

			criminalChecksRequiredRow.Visible = (criminalChecksRequiredTotal > 0);
			criminalChecksRequiredTotalLabel.Text = criminalChecksRequiredTotal.ToString( CultureInfo.CurrentUICulture );
			return criminalChecksRequiredTotal;
		}

    /// <summary>
    /// Shows a row in the result table for a certain status
    /// </summary>
    /// <param name="row">The table row.</param>
    /// <param name="label">The header label.</param>
    /// <param name="results">Validation results</param>
    /// <param name="status">The status to count</param>
    private static int ShowTotalRow(Control row, ITextControl label, IEnumerable<UploadRecordDto> results, ValidationRecordStatus status)
    {
      var total = results.Count(record => status == ValidationRecordStatus.Success 
                                                      ? (record.Status == status) 
                                                      : ((record.Status & status) == status));

      row.Visible = (total > 0);
      label.Text = total.ToString(CultureInfo.CurrentUICulture);

      return total;
    }

    /// <summary>
    /// Redirects the user to the dashboard
    /// </summary>
    private void RedirectToDashboard()
    {
      Response.Redirect(UrlBuilder.TalentJobs());
    }

    /// <summary>
    /// Open Legal Notices
    /// </summary>
    protected void CriminalBackgroundDisclaimerLink_OnClick(object sender, EventArgs e)
    {
      var category = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories).Where(x => x.Key.Equals("DocumentCategories.LegalNotice")).Select(x => x.Id).FirstOrDefault());
      string legalNoticesUrl = UrlBuilder.Notice(Uri.EscapeDataString(HtmlLocalise("LegalNotice.Title", "Legal Notices")), category, App.Settings.Module == FocusModules.Assist ? DocumentFocusModules.Assist : DocumentFocusModules.Talent);
      ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", string.Format("window.open('{0}');", legalNoticesUrl), true);

    }
  }
}