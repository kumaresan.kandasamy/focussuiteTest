﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Help.aspx.cs" Inherits="Focus.Web.WebTalent.Help" %>

<%@ Register src="~/WebTalent/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<h1><asp:Literal ID="HelpTitle" runat="server" /></h1>
	<br />
	<div>
		<asp:Literal ID="HelpContent" runat="server" />		
	</div>
</asp:Content>
