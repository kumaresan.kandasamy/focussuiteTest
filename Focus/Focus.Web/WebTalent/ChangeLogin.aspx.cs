﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;
using Focus.Core;
using Focus.Common;
using Framework.Core;

#endregion

namespace Focus.Web.WebTalent
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.TalentUser)] 
	public partial class ChangeLogin : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            if (App.Settings.DisableTalentAuthentication)
                throw new Exception("Not authorised");

            if (IsPostBack) return;
            LocaliseUI();
            NewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
            ConfirmNewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
            NewUserNameRegEx.ValidationExpression = App.Settings.UserNameRegExPattern;
            NewUserNameRegEx.Enabled = NewUserNameRegEx.ValidationExpression.IsNotNullOrEmpty();

            if (App.User.IsShadowingUser)
            {
                CurrentPasswordTextbox.Attributes["value"] = "********";
            }

            if (App.User.UserName.IsNotNull())
                CurrentUsernameTextbox.Text = App.User.UserName;
            if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent)
                ApplySSOTheme();

            SecurityQuestionsPlaceHolder.Visible = SecurityQuestionsTable.Visible = App.Settings.EnableTalentPasswordSecurity;

            if (App.Settings.EnableTalentPasswordSecurity)
                SecurityQuestionsPanel.Bind();
        }

		/// <summary>
		/// Handles the Click event of the ChangeLoginButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ChangeLoginButton_Click(object sender, EventArgs e)
		{
			var currentPassword = (CurrentPasswordTextbox.Text ?? string.Empty).Trim();
			var newPassword = (NewPasswordTextbox.Text ?? string.Empty).Trim();

			try
			{

				if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent))
				{
					ServiceClientLocator.AccountClient(App).ChangePassword(currentPassword, newPassword);
				}
				else
				{
					if (App.Settings.SSOManageAccountUrl.IsNotNullOrEmpty())
						Response.Redirect(App.Settings.SSOManageAccountUrl);
				}

				ConfirmationModal.Show(CodeLocalise("PasswordSuccessfulChange.Title", "Password Changed"),
						CodeLocalise("PasswordSuccessfulChange.Body", "You have successfully changed your password."),
						CodeLocalise("CloseModal.Text", "Return to account settings"));
			}
			catch (Exception ex)
			{
				ChangeLoginValidator.IsValid = false;
				ChangeLoginValidator.ErrorMessage = ex.Message;
			}
		}

		/// <summary>
		/// Handles the Click event of the ChangeUsernameButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ChangeUsernameButton_Click(object sender, EventArgs e)
		{
			var currentUsername = (CurrentUsernameTextbox.Text ?? string.Empty).Trim();
			var newUsername = (NewUsernameTextbox.Text ?? string.Empty).Trim();

			try
			{
				if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent))
				{
					ServiceClientLocator.AccountClient(App).ChangeUsername(currentUsername, newUsername);
                    //save changes to App.User context
                    App.User.UserName = newUsername;
                    App.User.Save();
				}
				else
				{
					if (App.Settings.SSOManageAccountUrl.IsNotNullOrEmpty())
						Response.Redirect(App.Settings.SSOManageAccountUrl);
				}

                //display changed username in the current username textbox after saving.
                CurrentUsernameTextbox.Text = newUsername;
                NewUsernameTextbox.Text =
                     ConfirmNewUsernameTextbox.Text = "";

				ConfirmationModal.Show(CodeLocalise("UsernameSuccessfulChange.Title", "Username Changed"),
																 CodeLocalise("UsernameSuccessfulChange.Body", "You have successfully changed your username."),
																 CodeLocalise("CloseModal.Text", "Return to account settings"));
			}
			catch (Exception ex)
			{
				ChangeUsernameValidator.IsValid = false;
				ChangeUsernameValidator.ErrorMessage = ex.Message; 
			}
		}

		/// <summary>
		/// Handles the Click event of the ChangeUsernameButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SecurityQuestionsButton_Click( object sender, EventArgs e )
		{
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForTalent)
			{
				ChangeLoginButton.Text = CodeLocalise("Global.Save.Label", "Change password");
				ChangeUsernameButton.Text = CodeLocalise("Global.Save.Label", "Change username");
			}
			else
				ChangeLoginButton.Text = ChangeUsernameButton.Text = CodeLocalise("Global.Save.Label", "Save");

			CurrentPasswordRequired.ErrorMessage = CodeLocalise("CurrentPasswordRequired.ErrorMessage", "Current password required");
			NewPasswordRequired.ErrorMessage = CodeLocalise("NewPasswordRequired.ErrorMessage", "New password required");
			NewPasswordRegEx.ErrorMessage = CodeLocalise("NewPasswordRegEx.RegExErrorMessage", "New password must be between 6 and 20 characters and contain at least one number");
			ConfirmNewPasswordRequired.ErrorMessage = CodeLocalise("ConfirmNewPasswordRequired.ErrorMessage", "Retype new password required");
			ConfirmNewPasswordRegEx.ErrorMessage = CodeLocalise("ConfirmNewPasswordRegEx.RegExErrorMessage", "Confirmed new password must be between 6 and 20 characters and contain at least one number");
			NewPasswordCompare.ErrorMessage = CodeLocalise("NewPasswordCompare.ErrorMessage", "New passwords must match");
			CurrentUsernameRequired.ErrorMessage = CodeLocalise("CurrentUsernameRequired.ErrorMessage", "Current username required");
			NewUsernameRequired.ErrorMessage = CodeLocalise("NewUsernameRequired.ErrorMessage", "New username required");
			ConfirmNewUsernameRequired.ErrorMessage = CodeLocalise("ConfirmNewUsernameRequired.ErrorMessage", "Retype new username required");
			NewUsernameCompare.ErrorMessage = CodeLocalise("NewUsernameCompare.ErrorMessage", "New usernames must match");
			NewUserNameRegEx.ErrorMessage = CodeLocalise("NewUserName.RegExErrorMessage", "Username is not in a valid email format");
		}

		/// <summary>
		/// Applies the O auth theme.
		/// </summary>
		// ReSharper disable once InconsistentNaming
		private void ApplySSOTheme()
		{
			UserNameInputPlaceHolder.Visible = false;
			PasswordInputPlaceHolder.Visible = false;

			PasswordSaveTable.Visible = UserNameSaveTable.Visible = App.Settings.SSOManageAccountUrl.IsNotNullOrEmpty();
		}
	}
}