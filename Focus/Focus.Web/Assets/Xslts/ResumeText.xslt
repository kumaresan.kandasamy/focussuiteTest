﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="text" encoding="UTF-8" />

	<xsl:template match="*">
		<!-- Simply recurse over the children. -->
		<xsl:apply-templates />
	</xsl:template>

</xsl:stylesheet>