﻿function ScrollableSkillsAccordion_UpdateSummary() {
	var skillsAccordion = this;

	var summaryListWrapper = $('#' + skillsAccordion.SummaryListId);
	var summaryList = $(summaryListWrapper).children('table');
	var hiddenIdsField = $('input[id="' + skillsAccordion.HiddenIdsFieldId + '"]');
	var hiddenNamesField = $('input[id="' + skillsAccordion.HiddenNamesFieldId + '"]');

	$(summaryList).children().remove();
	$(hiddenIdsField).val('');
	$(hiddenNamesField).val('');

	$(summaryListWrapper).hide();

	var selectedSkills = $('#' + skillsAccordion.AccordionId + ' :checked').not('input[value="all"]');
	if ($(selectedSkills).length > 0) {
		$(summaryListWrapper).show();
		$(selectedSkills).map(function () {
			$(summaryList).append('<tr><td class="column1">' + $(this).next('label').html() + '</td><td class="column2"><input type="hidden" value="' + $(this).val() + '" /><img src="' + skillsAccordion.DeleteIconUrl + '" style="position:relative; top:2px;" /></td></tr>');
			$(summaryList).stop(true, true).effect('highlight', { color: '#FCE8C2' }, 2000);
		});

		var skillIds = $(selectedSkills).map(function () {
			return $(this).val();
		}).get().join(',');
		$('input[id="' + skillsAccordion.HiddenIdsFieldId + '"]').val(skillIds);

		var skillNames = $(selectedSkills).map(function () {
			return $("label[for='" + $(this).attr("id") + "']").text();
		}).get().join(',');
		$('input[id="' + skillsAccordion.HiddenNamesFieldId + '"]').val(skillNames);
	}
}

function ScrollableSkillsAccordion_SetupCheckBoxes() {
	var skillsAccordion = this;
	skillsAccordion.UpdateSummary();
	$('#' + skillsAccordion.AccordionId + ' input:checkbox').on('click',null, function () {
		if ($(this).val() === 'all') {
			var checkAll = $(this);
			$(checkAll).parents('ul').find('input:checkbox').not('input:checkbox[value="all"]').each(function () {
				$(this).prop('checked', $(checkAll).is(':checked'));
			});
		}
		else {
			var allChecked = ($(this).parents('ul').find('input:checkbox').not('input:checkbox[value="all"]').length == $(this).parents('ul').find('input:checkbox:checked').not('input:checkbox[value="all"]').length);
			$(this).parents('ul').find('input:checkbox[value="all"]').prop('checked', allChecked);
		}
		skillsAccordion.UpdateSummary();
	});
	$('#' + skillsAccordion.SummaryListId + ' img').on('click', null, function() {
		skillsAccordion.RemoveSkill($(this).prev('input:hidden').val());
	});
}

function ScrollableSkillsAccordion_RemoveSkill(skillId) {
	var skillsAccordion = this;
	var skillCheckbox = $('#' + skillsAccordion.AccordionId + ' input:checkbox[value="' + skillId + '"]');
	if ($(skillCheckbox).exists()) {
		$(skillCheckbox).prop('checked', false);
		$(skillCheckbox).parents('ul').find('input[value="all"]').prop('checked', false);
		skillsAccordion.UpdateSummary();
	}
}

function ScrollableSkillsAccordion(accordionId, summaryListId, hiddenIdsFieldId, hiddenNamesFieldId, deleteIconUrl) {
	this.AccordionId = accordionId;
	this.SummaryListId = summaryListId;
	this.HiddenIdsFieldId = hiddenIdsFieldId;
	this.HiddenNamesFieldId = hiddenNamesFieldId;
	this.DeleteIconUrl = deleteIconUrl;

	this.SetUpCheckBoxes = ScrollableSkillsAccordion_SetupCheckBoxes;
	this.UpdateSummary = ScrollableSkillsAccordion_UpdateSummary;
	this.RemoveSkill = ScrollableSkillsAccordion_RemoveSkill;
}