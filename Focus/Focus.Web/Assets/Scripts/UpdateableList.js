﻿function RemoveListItem(item) {
	// Get deleted text
	var text = item.closest('td').prev('td').text();
	// Get table container
	var table = item.closest('table');
	// Get hidden field
	var hiddenField = table.next('input[type="hidden"]');
	//Get array of data in hidden field
	var values = hiddenField.val().split('|*|');
	// Remove deleted text
	values = jQuery.grep(values, function (value) {
		return value != text;
	});
	// Repopulate hidden field with chosen data
	hiddenField.val(values.join("|*|"));

	// Rebuild table
	var tableId = table.attr('id');
	$("#" + tableId + " tr").remove();

	for (var i = 0; i < values.length; i++) {
		BuildTable(tableId, values[i]);
	}

}

function BuildTable(tableId, text) {
	$("#" + tableId).find('tbody')
                .append($('<tr>')
                        .append($('<td>')
                        .attr('width', '100%')
                        .text(text)
                            ).append($('<td>').attr('class', 'deleteItem'))
                );
}

function AddListItem(textBoxId, hiddenfieldId, tableid, validationGroup) {

	if (validationGroup != null) {
		if (!Page_ClientValidate(validationGroup)) {
			return false;
		}
	}
	
	// Get currently stored values
	var hiddenField = $('#' + hiddenfieldId).val();

	// Get the new text then clear text box
	var newText = $('#' + textBoxId).val().trim();
	$('#' + textBoxId).val('');

	if (newText == '')
		return;

	// Build array of current values
	var list = hiddenField.split('|*|');

	// If new text already entered then stop
	if (list != null && jQuery.inArray(newText, list) != -1)
		return;

	// Build delimited string and assign to hidden field
	if (hiddenField.length > 0)
		hiddenField += '|*|';
	hiddenField += newText;
	$('#' + hiddenfieldId).val(hiddenField);

	// Build table and clear text box
	BuildTable(tableid, newText);
}

function BuildTableFromScratch(hiddenfieldId, tableid) {
	//alert($('#' + hiddenfieldId).val());
	if ($('#' + hiddenfieldId).length == 0 || $('#' + hiddenfieldId).val() == '')
		return;

	// Build array of current values
	var list = $('#' + hiddenfieldId).val().split('|*|');
	// Loop through array and build display
	for (var i = 0; i < list.length; i++) {
		BuildTable(tableid, list[i]);
	}
}