﻿/*! Copyright © 2000-2014 Burning Glass International Inc.
*
* Proprietary and Confidential
*
* All rights are reserved. Reproduction or transmission in whole or in part, in
* any form or by any means, electronic, mechanical or otherwise, is prohibited
* without the prior written consent of Burning Glass International Inc. */

/* Dependencies: 
*
*   jQuery
*   KnockoutJS
*		Microsoft ASP.NET clientscript validation
*/

/*jshint forin:true, noarg:true, noempty:true, eqeqeq:true, bitwise:true, undef:true, unused:false, browser:true, devel:true, jquery:true, esnext:true, es3:true, maxerr:50, maxcomplexity:6 */
/*global ko, Page_ClientValidate */

(function (focusSuite) {
    focusSuite.assistmanageactivityservice = function () {
        /// <summary>Defines the assistActivities namespace in the focusSuite namespace.</summary>

        var assistActivitiesItem = function (data, self) {
            /// <summary>Defines a model representing a data item in the UpdateableCLientListList control.</summary>
            /// <param name="data">The data to deserialise into the model.</param>
            data = data || {};

            this.Id = data.Id;

            this.ActivityCategoryId = data.ActivityCategoryId;
            /// <field type="ko.observable(String)">The category.</field>
            this.CategoryName = self.categories()[self.categoryIds.indexOf(data.CategoryLocalisationKey)].name;

            /// <field type="ko.observable(String)">The activity name.</field>
            this.ActivityName = ko.observable(data.ActivityName);

            /// <field type="ko.observable(Boolean)">Specifies whether the item is visible.</field>
            this.ActivityVisible = ko.observable(data.ActivityVisible);

            /// <field type="ko.observable(Boolean)">Specifies whether the item is editable.</field>
            this.Editable = data.Editable;

            this.ActivityType = data.ActivityType;

            this.DirtyFlag = new ko.dirtyFlag(this);
        };
        var categoriesItem = function (data) {
            var self = this;
            data = data || {};

            self.name = ko.observable(data.CategoryName);
        };
        var assistActivityManageViewModel = function (getActivitiesUrl, saveActivitiesUrl, okText) {
            /// <summary>The viewmodel for the UpdateableClientList control.</summary>
            /// <param name="initConfig" type="Object">The initial configuration for the control.</param>
            var self = this;

            self.getActivitiesUrl = getActivitiesUrl;
            self.saveActivitiesUrl = saveActivitiesUrl;

            self.okText = okText;

            self.activityTypeSelected = ko.observable("0");

            self.recordsPerPage = ko.observable("10");

            self.pageNumber = ko.observable("1");

            self.activities = ko.observableArray([]);

            self.changedActivities = ko.computed(function () {
                return ko.utils.arrayFilter(self.activities(), function (item) {
                    return item.DirtyFlag.isDirty();
                });
            }, this);

            self.categoryIds = [];

            self.categories = ko.observableArray([]);

            self.saveMessage = ko.observable("");

            self.filteredActivities = ko.computed(function () {
                return ko.utils.arrayFilter(self.activities(), function (item) {
                    return item.ActivityType == self.activityTypeSelected();
                });
            });

            self.totalPages = ko.computed(function () {
                return Math.ceil(self.filteredActivities().length / self.recordsPerPage());
            });

            self.previousClass = ko.computed(function () {
                if (self.pageNumber() > 1)
                    return "pager-button";
                else
                    return "aspNetDisabled pager-button-disabled";
            });

            self.previousHref = ko.computed(function () {
                if (self.pageNumber() > 1)
                    return "#";
                else return null;
            });

            self.nextClass = ko.computed(function () {
                if (self.pageNumber() < self.totalPages())
                    return "pager-button";
                else
                    return "aspNetDisabled pager-button-disabled";
            });

            self.nextHref = ko.computed(function () {
                if (self.pageNumber() < self.totalPages())
                    return "#";
                else
                    return null;
            });

            self.pages = ko.computed(function () {
                var tempPages = [];
                var tempTotalPages = self.totalPages();
                var i = 0;
                while (i < tempTotalPages) {
                    tempPages.push(i + 1);
                    i++;
                }
                return tempPages;
            });

            self.pagedFilteredActivities = ko.computed(function () {
                return self.filteredActivities().slice((parseInt(self.pageNumber()) - 1) * parseInt(self.recordsPerPage()), parseInt(self.pageNumber()) * parseInt(self.recordsPerPage()));
            });

            $.ajaxSetup({ cache: false });
            $.getJSON(self.getActivitiesUrl, function (data) {
                for (var i = 0; i < data.d.length; i++) {
                    var index = self.categoryIds.indexOf(data.d[i].CategoryLocalisationKey);
                    if (index == -1) {
                        self.categoryIds.push(data.d[i].CategoryLocalisationKey);
                        self.categories.push(new categoriesItem(data.d[i]));
                    }
                }
                self.activities(ko.utils.arrayMap(data.d, function (item) { return new assistActivitiesItem(item, self); }));
                self.activities.sort(function (left, right) { return left.CategoryName() == right.CategoryName() ? 0 : (left.CategoryName() < right.CategoryName() ? -1 : 1) });
            });

            self.sortCategoryAsc = function () {
                self.activities.sort(function (left, right) { return left.CategoryName() == right.CategoryName() ? 0 : (left.CategoryName() < right.CategoryName() ? -1 : 1) })
            };

            self.sortCategoryDesc = function () {
                self.activities.sort(function (left, right) { return left.CategoryName() == right.CategoryName() ? 0 : (left.CategoryName() > right.CategoryName() ? -1 : 1) })
            };

            self.sortActivityAsc = function () {
                self.activities.sort(function (left, right) { return left.ActivityName() == right.ActivityName() ? 0 : (left.ActivityName() < right.ActivityName() ? -1 : 1) })
            };

            self.sortActivityDesc = function () {
                self.activities.sort(function (left, right) { return left.ActivityName() == right.ActivityName() ? 0 : (left.ActivityName() > right.ActivityName() ? -1 : 1) })
            };

            self.previousPage = function () {
                var currentPage = parseInt(self.pageNumber());
                var prevPage = currentPage - 1;
                self.pageNumber(prevPage);
            };

            self.nextPage = function () {
                var currentPage = parseInt(self.pageNumber());
                var nextPage = currentPage + 1;
                self.pageNumber(nextPage);
            };

            self.save = function () {
                $.ajax({
                    url: self.saveActivitiesUrl,
                    type: "POST",
                    cache: false,
                    data: ko.toJSON({ data: self.changedActivities() }),
                    contentType: "application/json",
                    dataType: "json",
                    success: function (response) {
                        self.saveMessage(response.d);
                        $find('ConfirmationPopupBehavior').show();
                    }
                });
                ko.utils.arrayForEach(this.changedActivities(), function (item) {
                    item.DirtyFlag.reset();
                });
            };
        };
        var bindAssistManageActivityServiceViewModel = function (controlContainer, getActivitiesUrl, saveActivitiesUrl, okText) {
            /// <summary>Initialises the bindings.</summary>
            var container = document.getElementById(controlContainer);
            if (!ko.dataFor(container))
                ko.applyBindings(new assistActivityManageViewModel(getActivitiesUrl, saveActivitiesUrl, okText), container);
               };

        return {
            BindAssistManageActivityServiceViewModel: bindAssistManageActivityServiceViewModel
        };
    } ();

})(window.focusSuite = window.focusSuite || {});