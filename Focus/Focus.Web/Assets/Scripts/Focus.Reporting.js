﻿function Report_ShowAdditionalOffices(link) {
  link.hide();
  link.next("span").show();
}

function Report_HideAdditionalOffices(link) {
  link.parent().hide();
  link.parent().prev("a").show();
}