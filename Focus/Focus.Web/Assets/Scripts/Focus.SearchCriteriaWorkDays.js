﻿(function (focusSuite) {
  focusSuite.searchCriteriaWorkDays = function () {
    var bindClicks = function (id) {
      var table = $("#" + id);

      var firstBox = table.find(":checkbox:first");
      var otherBoxes = table.find(":checkbox:not(:first)");

      firstBox.on("click", function () {
        otherBoxes.prop("checked", $(this).is(":checked"));
      });

      otherBoxes.on("click", function () {
        firstBox.prop("checked", otherBoxes.filter(":checked").length == otherBoxes.length);
      });
    };

    var bindSearchCriteriaWorkDaysOnClick = function (weekdaysCheckBoxListId, weekendCheckBoxList) {
      bindClicks(weekdaysCheckBoxListId);
      bindClicks(weekendCheckBoxList);
    };

    return {
      BindSearchCriteriaWorkDaysOnClick: bindSearchCriteriaWorkDaysOnClick
    };
  } ();
})(window.focusSuite = window.focusSuite || {});