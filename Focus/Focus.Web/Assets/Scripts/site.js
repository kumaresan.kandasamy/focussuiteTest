﻿// Startup JS
$(document).ready(function () {

  // if a user selects a presaved username from the login panel text box, remove the password label
  $('.modal #UserNameTextBox').bind('input propertychange', function () {
    setTimeout(
	  function () {
	    if ($(".modal #PasswordTextBox").val().length > 0) {
	      $(".modal #PasswordTextBox").prev().hide();
	    }
	  }, 100);
  });

  //$("select").selectBox();

  // Adding additional CSS classes for wizard progress bar
  $(".progressBar .active").parent().prev().addClass("activePrevious");
  if ($(".progressBar .active").parent().next().children("span").hasClass("visited")) {
    $(".progressBar .active").addClass("nextVisited");
  }
  $(".visited").each(function (index) {
    if (!$(this).parent().next().children("span").hasClass("visited") && !$(this).parent().next().children("span").hasClass("active")) {
      $(this).addClass("nextNotVisited");
    }
    if ($(this).parent().next().hasClass("activePrevious")) {
      $(this).addClass("nextActivePrevious");
    }
    if ($(this).parent().hasClass("activePrevious") && $(this).parent().next().children("span").hasClass("active")) {
      $(this).addClass("nextActive");
    }
  });

  // Add or log contact hiding
  $("#AddOrLogRadio1").click(function (e) {
    $("#addContactContent").addClass("show");
    $("#logContactContent").removeClass("show");
    e.preventDefault();
  });
  $("#AddOrLogRadio2").click(function (e) {
    $("#addContactContent").removeClass("show");
    $("#logContactContent").addClass("show");
    e.preventDefault();
  });

  // Add note hiding
  $("#addNote").click(function (e) {
    $(".addNoteContent").each(function (index) {
      $(this).toggleClass("show");
    });
    var c = $(this).contents().unwrap();
    e.preventDefault();
  });

  // others link boxes
  $(".othersLink").each(function (index) {
    var linkId = $(this).attr('class').split(" ")[1];
    var numberOfLinks = $("." + linkId).size() - 1;

    //var n = $(this).text().match(/\d+/g);
    var stringWithAmountOfLinks = $(this).text().replace("0", numberOfLinks);

    $(this).text(stringWithAmountOfLinks);
  });
  $(".othersLink").click(function (e) {
    var linkId = $(this).attr('class').split(" ")[1];
    $("." + linkId).toggleClass("show");

    var numberOfLinks = $("." + linkId).size() - 1;

    if ($(this).hasClass("show")) {
      var togglePlusMinus = $(this).text().replace("+", "-");
      $(this).text(togglePlusMinus);
    }
    else {
      var togglePlusMinus = $(this).text().replace("-", "+");
      $(this).text(togglePlusMinus);
    }
    e.preventDefault();
  });

  // Apply jQuery to infield labels
  $.InFieldLabels.defaultOptions.pollDuration = 300;
  $(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();

  // new style accordions
  SetupAccordionNew();

  //Device Jquery
  if ($('body').css('min-width') == '0px') {
    $('.table th .AscButton').parent('.table th td').css({
      "padding": "10px 0 10px 10px",
      "width": "15px"
    });
    //Anchor tag sorting arrows fix
    $('.table th a.AscButton').css({
      "float": "left",
      "display": "inherit",
      "position": "static",
      "bottom": "0px",
      "padding-bottom": "10px"
    });
    $('.table th a.DescButton').css({
      "float": "left",
      "display": "inherit",
      "position": "static",
      "right": "0px",
      "top": "0px"
    });
    //Input sorting arrows fix
    $('.table th .orderingcontrols .AscButton').css({
      "bottom": "10px"
    });
    $('.table th .orderingcontrols .DescButton').css({
      "top": "10px"
    });

    $('.topRow .column2 span').each(function () {
      var height = $(this).height();
      $(this).parents('tr.topRow').find('td.column1 span, td.column3 span').css('height', height);
    });

    /* Assist job seekers drill down > job seeker activity log action dropdownlist fix */
    $('#MainContent_JobSeekerActivityList_ActionDropDownValidator').next('br').attr('style', 'display:none');
    $("#MainContent_Wizard_Step7_InterviewContactPreferencesPanel table tr td:contains('Other or specific instructions')").each(function () {

      $(this).html('Other or <br /> specific instructions');
    });
  }
  //end of device JS

  /* Add alt text where they're missing */
  $("img:not([alt])").attr("alt", "");
  /* Add a href to Pager.controls so that they are accessibility compliant */
  $("[id$='PreviousButton']:not([href])").attr("href", "#");
  $("[id$='NextButton']:not([href])").attr("href", "#");

  $("[id$='SelectorAllCheckBoxes']:not([title])").attr("title", "Selector all check boxes");

  BindTooltips();

  if (!Modernizr.generatedcontent) {
    $(".requiredData").prepend("<img src=\"../Assets/Images/asteriskRequired.gif\" />");
  }
});

function BindTooltips() {
  $('.toolTipNew:not(.tooltipstered)').tooltipster({
    maxWidth: 180,
    theme: '.tooltipster-career',
    contentAsHTML: true
  });
}

function OpenLocalisePopup(localiseKey, applicationPath) {
  var url = applicationPath + "/LocalisePopup.aspx?localiseKey=" + localiseKey;
  //var oArgs = window.showModalDialog(url, null, "dialogHeight: 400px;dialogWidth: 800px; scroll:no; status:no;");
  window.open(url, "", "width=800,height=400", "scrollbars: 0");
  //if (oArgs && oArgs.val && oArgs.val.length > 0) {
  //alert(oArgs.val);
  window.location = window.location;
  //}
}

function SetupAccordionNew() {
  $('.accordionContentNew').hide();
  $('.accordionNew>.accordionSectionNew').each(function () {
    var accordionSection = $(this);
    $(this).undelegate('.accordionTitleNew', 'click');
    $(this).delegate('.accordionTitleNew', 'click', function () {
      $(accordionSection).children('.accordionContentNew').slideToggle(400, function () {
        $(this).parent('.accordionSectionNew').toggleClass('accordionExpandedNew');
        if (!($(this).is(':hidden'))) {
          $(accordionSection).siblings('.accordionSectionNew').children('.accordionContentNew').each(function () {
            $(this).slideUp(400, function () {
              $(this).parent('.accordionSectionNew').removeClass('accordionExpandedNew');
            });
          });
        }
      });
    });
  });
}

// Disable backspace causing a back event
$(document).keydown(function (e) {
  var nodeName = e.target.nodeName.toLowerCase();

  if (e.which === 8) {
    if ((nodeName === 'input' && (e.target.type === 'text' || e.target.type === 'password')) || nodeName === 'textarea') {
      // do nothing 
    }
    else
      e.preventDefault();
  }
});

// Keyword / Statements functions

var storeKeywords = "";

function AddSelectedKeywordsOrStatements(keywordsId, statementsId, targetControlId, keywordsHeadingMatch) {
  if (targetControlId == null || targetControlId.length == 0)
    return;

  if ($get(statementsId) != null) {
    var statementsCheckBoxList = $get(statementsId);
    var statementList = statementsCheckBoxList.getElementsByTagName('INPUT');

    for (var i = 0; i < statementList.length; i++)
      if (statementList[i].checked && statementList[i].disabled == false)
        if ($get(targetControlId).value.indexOf(statementList[i].parentNode.getElementsByTagName('label')[0].innerHTML) < 0) {
          $("#" + targetControlId).keydown();
          var currentValue = $get(targetControlId).value;
          if (currentValue.substring(currentValue.length - 1) == "\n") {
            $get(targetControlId).value += "* " + statementList[i].parentNode.getElementsByTagName('label')[0].innerHTML;
          } else {
            $get(targetControlId).value += "\n* " + statementList[i].parentNode.getElementsByTagName('label')[0].innerHTML;
          }
          statementList[i].disabled = true;
        }
  }

  var newline = "\r\n";
  //Check IE 9 or greater
  if (Modernizr.canvas) newline = "\n";
  var keywordsHeading = newline + keywordsHeadingMatch + " ";
  var selectedText = "";


  if ($get(keywordsId) != null) {
    var keywordsCheckBoxList = $get(keywordsId);
    var keywordList = keywordsCheckBoxList.getElementsByTagName('INPUT');

    for (var i = 0; i < keywordList.length; i++) {
      if (keywordList[i].checked && keywordList[i].disabled == false) {
        if ($get(targetControlId).value.indexOf(": " + keywordList[i].parentNode.getElementsByTagName('label')[0].innerHTML + ",") < 0 &&
							$get(targetControlId).value.indexOf(", " + keywordList[i].parentNode.getElementsByTagName('label')[0].innerHTML + ",") < 0 &&
							$get(targetControlId).value.indexOf(": " + keywordList[i].parentNode.getElementsByTagName('label')[0].innerHTML + ".") < 0 &&
							$get(targetControlId).value.indexOf(", " + keywordList[i].parentNode.getElementsByTagName('label')[0].innerHTML + ".") < 0) {

          selectedText = keywordList[i].parentNode.getElementsByTagName('label')[0].innerHTML;
          if ($get(targetControlId).value.indexOf(keywordsHeadingMatch) != -1) {
            var job_description = $get(targetControlId).value;
            var after_data = job_description.substring(job_description.indexOf(keywordsHeadingMatch), job_description.length);

            if (storeKeywords == "") {
              storeKeywords = TrimText(after_data.split(newline)[0]);
              if ((storeKeywords != "" || storeKeywords != '') && storeKeywords.substring(storeKeywords.length - 1, storeKeywords.length) == ".")
                storeKeywords = storeKeywords.substring(0, storeKeywords.length - 1);
            }
            else if (storeKeywords != "" && (TrimText(storeKeywords) + ".") != TrimText(after_data.split(newline)[0])) {
              var updatedKeyWords = TrimText(after_data.split(newline)[0]);
              storeKeywords = updatedKeyWords.substring(0, updatedKeyWords.length - 1);
            }

            $("#" + targetControlId).keydown();
            selectedText = storeKeywords + ", " + selectedText;
            $get(targetControlId).value = $get(targetControlId).value.replace(newline + TrimText(storeKeywords) + ".", "");
            $get(targetControlId).value += newline + TrimText(selectedText) + ".";

            storeKeywords = selectedText.substring(0, selectedText.length);
          }
          else {
            $("#" + targetControlId).keydown();
            selectedText = keywordsHeading + selectedText + ".";
            $get(targetControlId).value += selectedText;
            storeKeywords = selectedText.substring(0, selectedText.length - 1);
          }
        }

        keywordList[i].disabled = true;
      }
    }
  }

  return false;
}

// Similar Jobs

var selectedTextAreaText;

//function GetSelectectTextAreaText() 
//{
//	var range = $(this).getSelection();
//	selectedTextAreaText = range.text;
//}

function hasClass(elem, classSelector) {
  return (elem.className).indexOf(classSelector) > -1;
}
function GetSelectedText(classSelector) {
  var selText = "";
  if (window.getSelection) {  // all browsers, except IE before version 9
    if (document.activeElement && (document.activeElement.tagName.toLowerCase() == "textarea") && ((classSelector) ? hasClass(document.activeElement, classSelector) : true)) {
      var text = document.activeElement.value;
      selText = text.substring(document.activeElement.selectionStart,
                                              document.activeElement.selectionEnd);
    }
  }
  else {
    if (document.selection.createRange) {       // Internet Explorer
      var range = document.selection.createRange();
      selText = range.text;
    }
  }
  if (selText !== "") {
    selectedTextAreaText = selText;
  }
}

function AddSelectectTextAreaText(targetControlId, onAddEventJavascriptMethod, errorLabelId) {
  var errorLabel = null;
  if (errorLabelId != null && errorLabelId.length > 0)
    errorLabel = $("#" + errorLabelId);

  var textToAdd = TrimText(selectedTextAreaText);
  if (textToAdd == null) {
    if (errorLabel != null)
      errorLabel.show();

    return false;
  }

  if (errorLabel != null)
    errorLabel.hide();

  if (targetControlId == null || targetControlId.length == 0 || typeof selectedTextAreaText == "undefined")
    return false;

  $("#" + targetControlId).keydown();
  $("#" + targetControlId).val($("#" + targetControlId).val() + "\r\n" + textToAdd);
  selectedTextAreaText = "";
  if (onAddEventJavascriptMethod && onAddEventJavascriptMethod.length > 0) eval(onAddEventJavascriptMethod);

  return false;
}

// Helper Functions

function SetEndOfText(control) {
  if (control.createTextRange) {
    var fieldRange = control.createTextRange();

    fieldRange.moveStart('character', control.value.length);
    fieldRange.collapse();
    fieldRange.select();
  }
}

function TrimText(text) {
  if (!text || typeof text != 'string') return null;
  return text.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ').replace(/[\n]+$/, '');
}

function showProcessing() {
  $("#UpdateProgressIndicator").show();
  setTimeout('$("#ImageProgress").attr("src", $("#ImageProgress").attr("src")).focus();', 50);
}

// Load cascading drop down
function LoadCascadingDropDownList(parentSelectedValue, dropDownListClientId, serviceMethodUrl, category, topDefaultText, loadingText, dropDownListSelectedIndex) {
  var select = $('#' + dropDownListClientId);

  var selectOptions;
  if (select.prop) {
    selectOptions = select.prop('options');
  }
  else {
    selectOptions = select.attr('options');
  }
  $('option', select).remove();

  if (dropDownListSelectedIndex == null) {
    dropDownListSelectedIndex = -1;
  }

  if (topDefaultText && topDefaultText.length > 0) {
    if (dropDownListSelectedIndex == -1) {
      selectOptions[selectOptions.length] = new Option(topDefaultText, '', false, true);
    }
    else {
      selectOptions[selectOptions.length] = new Option(topDefaultText, '', false, false);
    }
  }

  selectOptions[selectOptions.length] = new Option(loadingText, loadingText, false, true);

  if (parentSelectedValue.length > 0) {
    var ajaxOptions = {
      type: "POST",
      url: serviceMethodUrl,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      async: true,
      data: '{"knownCategoryValues": "' + parentSelectedValue + '", "category": "' + category + '"}',
      success: function (response) {
        var results = response.d;
        if (results && results.length > 0) {
          $('option', select).remove();
          $.each(results, function (index, option) {
            if (dropDownListSelectedIndex != -1 && (dropDownListSelectedIndex - 1) == index) {
              selectOptions[selectOptions.length] = new Option(option.name, option.value, false, true);
            }
            else {
              selectOptions[selectOptions.length] = new Option(option.name, option.value, false, false);
            }
          });
          //					if (spanBox) {
          //						spanBox.text(select.find(':selected').text());
          //					}
        }
      }
    };

    $.ajax(ajaxOptions);
  }
  else {
    //		if (spanBox) {
    //			spanBox.text(select.find(':selected').text());
    //		}
  }
}

function UpdateStyledDropDown(dropDownCtrl, selectedValue) {
  if ($(dropDownCtrl).exists()) {
    if (selectedValue != null)
      $(dropDownCtrl).children('option[value="' + selectedValue + '"]').prop('selected', true);
    else
      $(dropDownCtrl).children('option:first').prop('selected', true);

    $(dropDownCtrl).next().find(':first-child').text($(dropDownCtrl).children('option:selected').text()).parent().addClass('changed');
  }
}

// helper method for checking whether an element exists in the DOM
$.fn.exists = function () {
  return jQuery(this).length > 0;
};

function formatCurrency(usFormat) {
  var sign, cents;
  if (usFormat === '' || typeof usFormat === 'undefined')
    return '';
  usFormat = usFormat.toString().replace(/\$|\,/g, '');
  if (isNaN(usFormat))
    usFormat = '0';
  sign = (usFormat === (usFormat = Math.abs(usFormat)));
  usFormat = Math.floor(usFormat * 100 + 0.50000000001);
  cents = usFormat % 100;
  usFormat = Math.floor(usFormat / 100).toString();
  if (cents < 10)
    cents = '0' + cents;
  for (var i = 0; i < Math.floor((usFormat.length - (1 + i)) / 3); i++)
    usFormat = usFormat.substring(0, usFormat.length - (4 * i + 3)) + ',' + usFormat.substring(usFormat.length - (4 * i + 3));
  if (usFormat === '0' && cents === '000')
    return '0.00';
  else
    return (((sign) ? '' : '-') + usFormat + '.' + cents);
}

function GetAgeForDate(birth, now) {
  if (now == null)
    now = new Date();

  var currentYear = now.getFullYear();
  var yearDiff = currentYear - birth.getFullYear();
  var birthdayThisYear = new Date(currentYear, birth.getMonth(), birth.getDate());

  return (now >= birthdayThisYear)
        ? yearDiff
        : yearDiff - 1;
}

function scrollContainerIntoViewIfNeeded(elementId, containerId) {
  var element = $("#" + elementId);
  var container = $("#" + containerId);

  var docViewTop = container.scrollTop();
  var docViewBottom = docViewTop + container.height();

  var elemTop = element.offset().top;
  var elemBottom = elemTop + element.height();

  var isScrolled = ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));

  if (!isScrolled) {
    if (element.height() > container.height()) {
      container.scrollTop(elemTop);
    } else {
      container.scrollTop(elemTop - (container.height() - element.height()));
    }
  }
}

function PopulateAddressFieldsForPostalCode(serviceUrl, postalCode, stateDropDownList, countyDropDownList, countyCascadingDropDownList, countySelectText, cityTextBox) {
  var options = {
    type: "POST",
    url: serviceUrl + "/GetStateCityCountyAndCountiesForPostalCode",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: true,
    data: '{"postalCode": "' + postalCode + '"}',
    success: function (response) {
      var results = response.d;

      if (results && results.StateId) {
        $("#" + stateDropDownList).val(results.StateId);

        if (results.Counties && results.Counties.length > 0) {
          $("#" + countyDropDownList).html('');
          $('<option>' + countySelectText + '</option>').appendTo("#" + countyDropDownList);

          var text = '';
          for (i = 0; i < results.Counties.length; i++) {
            $('<option value="' + results.Counties[i].value + '">' + results.Counties[i].name + '</option>').appendTo("#" + countyDropDownList);
            if (results.Counties[i].value == results.CountyId)
              text = results.Counties[i].name;
          }

          if (results.CountyId) {
            $("#" + countyDropDownList).val(results.CountyId);
            var dropdown = $("[id*='" + countyCascadingDropDownList + "']");
            if (dropdown != null)
              dropdown.val(results.CountyId);
          }

          if (results.CityName) {
            $("#" + cityTextBox).val(results.CityName);
          } else {
            $("#" + cityTextBox).val("");
          }

          $("#" + countyDropDownList).removeAttr('disabled');
        }
      } else {
        $("#" + stateDropDownList).val('');
        $("#" + countyDropDownList).html('');
        $('<option>' + countySelectText + '</option>').appendTo("#" + countyDropDownList);
        $("#" + countyDropDownList).val('');
        $("#" + countyDropDownList).attr('disabled', '');
        $find(countyCascadingDropDownList).set_SelectedValue('', '');
        $("#" + cityTextBox).val("");
      }
    }
  };
  $.ajax(options);
}

function PopulateJobAddressFieldsForPostalCode(serviceUrl, postalCode, stateDropDownList, cityTextBox) {
  var options = { type: "POST",
    url: serviceUrl + "/GetStateCityCountyAndCountiesForPostalCode",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: true,
    data: '{"postalCode": "' + postalCode + '"}',
    success: function (response) {
      var results = response.d;

      if (results && results.StateId) {
        $("#" + stateDropDownList).val(results.StateId);
        if (results.CityName) {
          $("#" + cityTextBox).val(results.CityName);
        } else {
          $("#" + cityTextBox).val("");
        }
      }
      else {
        $("#" + stateDropDownList).val('');
        $("#" + cityTextBox).val("");
      }
    }
  };

  $.ajax(options);
}


// global variables
var allCollapsiblePanelBehaviors, expandCollapseButton, expandAllText, collapseAllText;

function toggleExpandCollapseButtonState(allPanelsExpanded) {
  /// <summary>Toggles the Expand All / Collapse All button state.</summary>
  if (allPanelsExpanded) {
    expandCollapseButton.val(collapseAllText).removeClass('is-collapsed');
  } else {
    expandCollapseButton.val(expandAllText).addClass('is-collapsed');
  }
}

/// <summary>Event handler fired when each collapsible panel collapses.</summary>
function collapsiblePanelCollapsed() {
  // At least one panel has been collapsed so set the control button to 'Expand all'
  toggleExpandCollapseButtonState(false);
}

/// <summary>Event handler fired when each collapsible panel expands.</summary>
function collapsiblePanelExpanded() {
  var allPanelsExpanded = true;

  // Check if any of the panels are collapsed
  $.each(allCollapsiblePanelBehaviors, function (index, behavior) {
    if (behavior.get_Collapsed()) {
      allPanelsExpanded = false;
      return;
    }
  });
  // if ALL panels are expanded, set the control button to 'Collapse all'. Check all scenarios as this function is also called on page load
  toggleExpandCollapseButtonState(allPanelsExpanded);
}

function getAllCollapsiblePanelBehaviors() {
  /// <summary>
  /// Gets a list of all collapsible panel behaviours on the page, assigns them as an array to the global allCollapsiblePanelBehaviors variable 
  /// and binds their expandComplete and collapseComplete events.
  /// </summary>
  allCollapsiblePanelBehaviors = $.grep(window.Sys.Application.getComponents(), function (behavior) {
    return typeof behavior.get_name !== 'undefined' && behavior.get_name() === 'CollapsiblePanelBehavior';
  });
  $.each(allCollapsiblePanelBehaviors, function (index, behavior) {
    behavior.add_expandComplete(collapsiblePanelExpanded);
    behavior.add_collapseComplete(collapsiblePanelCollapsed);
  });
}

function bindExpandCollapseAllPanels(controlButton, nestedGroups, expandText, collapseText) {
  /// <summary>Binds the Expand / Collapse All button.</summary>
  /// <param name="controlButton" type="String" mayBeNull="false">The (client) ID of the Expand / Collapse All button.</param>
  /// <param name="nestedGroups" type="Array" mayBeNull="true">An array of nested panel groups.</param>
  /// <param name="expandText" type="String" mayBeNull="true">The "Expand All" text to display on the button.</param>
  /// <param name="collapseText" type="String" mayBeNull="true">The "Collapse All" text to display on the button.</param>
  expandAllText = expandText || 'Expand all'; // in case no value passed in for expandText
  collapseAllText = collapseText || 'Collapse all'; // in case no value passed in for collapseText
  expandCollapseButton = $('#' + controlButton);
  nestedGroups = nestedGroups || [];

  // just a helper function to expand or collapse a panel
  // if doCollapse is true, the panel will be collapsed, if false, it will be expanded
  function expandCollapsePanel(panelBehavior, doCollapse) {
    if (doCollapse) {
      if (panelBehavior !== null && !panelBehavior.get_Collapsed()) {
        panelBehavior.collapsePanel();
      }
    }
    else {
      if (panelBehavior !== null && panelBehavior.get_Collapsed()) {
        panelBehavior.expandPanel();
      }
    }
  }

  // populate the global allCollapsiblePanelBehaviors variable
  getAllCollapsiblePanelBehaviors();

  // Set Text and Class
  collapsiblePanelExpanded();

  // remove all collapsible panels that aren't part of any nested groups that have been passed into the function
  // first normalise nested groups into one big long list of behaviour ids of all panels that are in all nested groups
  var panelsInNestedGroups = $.map(nestedGroups, function (nestedGroup) {
    var panelsInGroup = [nestedGroup.outerPanel];
    $.each(nestedGroup.innerPanels, function (index, innerPanel) {
      panelsInGroup.push(innerPanel);
    });
    return panelsInGroup;
  });

  // now create an array of all collapsible panel extenders that aren't part of any nested groups
  // basically take the allCollapsiblePanelBehaviors array and remove each id in the panelsInNestedGroups array from it
  var panelsNotInNestedGroups = $.map(allCollapsiblePanelBehaviors, function (behavior) {
    if ($.inArray(behavior.get_id(), panelsInNestedGroups) == -1) {
      return behavior;
    }
  });

  // OK, now we have a list of all panels that are part of a nested group, and a list of those that aren't...

  // ... so now we can bind the click event of the 'expand all/collapse all' button
  expandCollapseButton.on('click', null, function () {
    if (!$(this).hasClass('is-collapsed')) {
      // the button is currently set to 'Collapse All', so we need to collapse all panels
      // nothing fancy here, we can just call collapsePanel method on any collapsible panel on the page that isn't currently collapsed
      $.each(allCollapsiblePanelBehaviors, function (index, behavior) {
        expandCollapsePanel(behavior, true);
        $('#' + behavior._collapseControlID).removeClass('on');
      });
      // finally change the state of the button to set its text to 'expand all' and add the class 'is-collapsed'
      toggleExpandCollapseButtonState(true);
    } else {
      // the button is currently set to 'Expand all' so now we have to get clever
      // for any collapsible panel on the page that's NOT part of a nested group, we just check if it's collapsed, and if so, call expandPanel method. Simples.
      $.each(panelsNotInNestedGroups, function (index, behavior) {
        expandCollapsePanel(behavior, false);
        $('#' + behavior._collapseControlID).addClass('on');
      });
      // now the science part... dealing with nested panels
      // start by looping through the array of nested groups
      $.each(nestedGroups, function (index, nestedGroup) {
        var outerPanel = $find(nestedGroup.outerPanel);
        // declare an event handler that the outer panel of the nested group will fire when it has expanded
        function outerPanelExpandComplete() {
          // loop through the array of inner panels for the nested group, and if they're collapsed, expand them
          $.each(nestedGroup.innerPanels, function (innerIndex, innerPanelBehavior) {
            expandCollapsePanel($find(innerPanelBehavior), false);
          });
          // and after we've expanded all the inner panels, remove the expandComplete event handler from the outer panel
          // if we didn't do this, everytime the user expanded the outer panel (by clicking on it rather than using the expand all button)
          // this callback function would fire and all it's inner panels would expand
          outerPanel.remove_expandComplete(outerPanelExpandComplete);
        }
        // anyway, back to our loop...
        // if the outer panel of the nested group is collapsed, bind its expandComplete event to  the event handler above, then expand it
        if (outerPanel.get_Collapsed()) {
          outerPanel.add_expandComplete(outerPanelExpandComplete);
          expandCollapsePanel(outerPanel, false);
        }
        else {
          $.each(nestedGroup.innerPanels, function (innerIndex, innerPanelBehavior) {
            expandCollapsePanel($find(innerPanelBehavior), false);
          });
        }
      });
      // and finally change the state of the button to set its text to 'collapse all' and remove the class 'is-collapsed'
      toggleExpandCollapseButtonState(false);
    }
    return false;
  });
}

function equaliseCareerPathItems() {
  /// <summary>Equalises the height of the boxes in the career path chart (to the height of the tallest box).</summary>
  setTimeout(function () {
    var heightsArray = [];
    $('.careerPathChart .box td').height('auto').each(function () {
      heightsArray.push($(this).height());
    });
    $('.careerPathChart .box td').height(Math.max.apply(Math, heightsArray));
  }, 10);
}

function isAppleMobile() {
  /// <summary>Detects whether the user is using an Apple Mobile (iOS) device.</summary>
  if (navigator && navigator.userAgent && navigator.userAgent != null) {
    var strUserAgent = navigator.userAgent.toLowerCase();
    var arrMatches = strUserAgent.match(/(iphone|ipod|ipad)/);
    if (arrMatches != null)
      return true;
  }
  return false;
}

function setButtonFocus(buttonSelector) {
  /// <summary>Gives the specified button focus. Includes a check for iOS devices as iOS 8 has introduced a bug when setting focus on a button.</summary>
  /// <param name="buttonSelector">The jQuery selector for the button.</param>
  if (!isAppleMobile()) {
    $(buttonSelector).focus();
  }
}

function accordionLayoutPostbackFix() {
  /// <summary>Adds the "on" class to a singleAccordionTitle if its associated content panel is visible, ensuring the correct visual styling (bottom border) is applied.</summary>
  // FVN-3128
  $('.singleAccordionTitle').not('.excludeFromAccordion').each(function (index, titlePanel) {
    if ($(titlePanel).next('.singleAccordionContentWrapper').find('.singleAccordionContent').is(':visible')) {
      $(titlePanel).addClass('on');
    }
  });
}

function fixAutoCompleteExtender(sender, args) {
  /// <summary>Fixes the position of the ACT AutoCompleteExtender for tablets.</summary>
  var extender = sender._popupBehavior._element;
  var inputControlId = sender._element;
  if (Modernizr.touch) {
    extender.style.top = (parseInt($(inputControlId).offset().top + 26)).toString() + 'px';
    extender.style.left = (parseInt($(inputControlId).offset().left)).toString() + 'px';
  }
}

function autotab(original, destination) {
  if (original.getAttribute && original.value.length == original.getAttribute("maxlength"))
    document.getElementById(destination).focus();
}
