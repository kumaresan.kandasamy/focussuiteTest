﻿/*! Copyright © 2000-2014 Burning Glass International Inc.
*
* Proprietary and Confidential
*
* All rights are reserved. Reproduction or transmission in whole or in part, in
* any form or by any means, electronic, mechanical or otherwise, is prohibited
* without the prior written consent of Burning Glass International Inc. */

/* Dependencies: 
*
*   jQuery
*   KnockoutJS
*		Microsoft ASP.NET clientscript validation
*/

/*jshint forin:true, noarg:true, noempty:true, eqeqeq:true, bitwise:true, undef:true, unused:false, browser:true, devel:true, jquery:true, esnext:true, es3:true, maxerr:50, maxcomplexity:6 */
/*global ko, Page_ClientValidate */

(function (focusSuite) {

  focusSuite.updateableClientList = function () {
    /// <summary>Defines the updateableClientList namespace in the focusSuite namespace.</summary>

    var updateableClientListItem = function (data) {
      /// <summary>Defines a model representing a data item in the UpdateableCLientListList control.</summary>
      /// <param name="data">The data to deserialise into the model.</param>
      var self = this;
      data = data || {};

      /// <field type="ko.observable(String)">The value of the item.</field>
      self.value = ko.observable(data.value);
      /// <field type="ko.observable(String)">The text to display for the item.</field>
      self.label = ko.observable(data.label);
      /// <field type="ko.observable(Boolean)">Specifies whether the item is the default.</field>
      self.isDefault = ko.observable(data.isDefault);
      self.isNonAddedItem = false;
    };

    var updateableClientListViewModel = function (initConfig) {
      /// <summary>The viewmodel for the UpdateableClientList control.</summary>
      /// <param name="initConfig" type="Object">The initial configuration for the control.</param>
      var self = this;
      initConfig = initConfig || {};

      /// <field type="String">Flag specifying whether the control is in "DropDownList" or "TextBox" mode.</field>
      self.mode = initConfig.mode;
      /// <field type="ko.observable(Boolean)">Flag specifying whether the default item selection is enabled.</field>
      self.enableDefaultSelection = ko.observable(initConfig.enableDefaultSelection);
      /// <field type="ko.observableArray()" elementType="updateableClientListItem">The data items in the dropdown list when running in DropDownList mode.</field>
      self.selectableItems = ko.observableArray(ko.utils.arrayMap(initConfig.selectableItems, function (item) {
        return new updateableClientListItem(item);
      }));
      /// <field type="ko.observable(String)">Contains the new item the user has entered / selected.</field>
      // self.newListItem = ko.observable(initConfig.maxItems <= 1 ? initConfig.initialData : '');
      self.newListItem = ko.observable(new updateableClientListItem(null));
      /// <field type="ko.observableArray()" elementType="updateableClientListItem">An array of the selected items.</field>
      self.listItems = ko.observableArray([]);
      /// <field type="ko.computed(String)">A derived field containing the values of the selected items, joined with as a string.</field>
      self.listItemsHiddenField = ko.computed(function () {
        return ko.toJSON(self.listItems());
      });
      self.removeListItem = function (item) {
        /// <summary>Handles a click on the remove icon within each selected item.</summary>
        self.listItems.remove(item);
        if (self.mode !== 'TextBox' && self.mode !== 'AutoCompleteTextBox') {
          // if in DropDownListMode, add the removed item back into the items in the dropdown list and re-sort alphabetically
          // set isDefault to false before adding back into the dropdown, otherwise if the user re-adds this item, it will be set as default
          item.isDefault(false);
          self.selectableItems.push(item);
          self.selectableItems.sort(function (a, b) {
            return (a.label() > b.label()) ? 1 : ((b.label() > a.label()) ? -1 : 0);
          });
        }
      };
      self.validItemSelected = function () {
        /// <summary>Basic validation check to see if a valid items is being added to the list.</summary>
        // first make sure we actually have a new list item
        if (typeof self.newListItem() === 'undefined' || self.newListItem() === null)
          return false;
        // next make sure that it has a value
        if (typeof self.newListItem().value() === 'undefined' || self.newListItem().value() === null || self.newListItem().value() === '') {
          self.newListItem(new updateableClientListItem(null));
          return false;
        }
        // finally make sure that an item with the same value hasn't already been selected
        var matches = $.grep(self.listItems(), function (item) {
          return item.value() === self.newListItem().value();
        });
        if (matches.length > 0) {
          self.newListItem(new updateableClientListItem(null));
          return false;
        }
        return true;
      };
      self.addListItem = function () {
        /// <summary>Handles a click on the add item button.</summary>
        if (!self.validItemSelected())
          return false;

        if (self.mode === 'TextBox' || self.mode === 'AutoCompleteTextBox') {
          // if in TextBox mode, perform any validation specified then create a new updateableClientListItem based on the entered value and add it to the listItems array
          if (typeof initConfig.validationGroup !== 'undefined' && initConfig.validationGroup !== null) {
            if (!Page_ClientValidate(initConfig.validationGroup)) {
              return false;
            }
          }

          if (self.mode === 'TextBox') {
            self.listItems.push(new updateableClientListItem({
              label: self.newListItem().value(),
              value: self.newListItem().value()
            }));
          } else {
            self.listItems.push(self.newListItem());
          }
        } else {
          // if in DropDownList mode, remove the selected item from the dropdown list and move it into the listItems array
          var selectedItem = ko.utils.arrayFirst(self.selectableItems(), function (item) {
            return item.value() === self.newListItem().value();
          });
          self.selectableItems.remove(selectedItem);
          self.listItems.push(selectedItem);
        }

        self.newListItem(new updateableClientListItem(null));
      };
      self.toggleDefaultItem = function (item) {
        /// <summary>Toggles the default item in the selected items list.</summary>
        /// <param name="item" type="updateableClientListItem">The selected item we wish to toggle.</param>
        if (item.isDefault() === true) {
          item.isDefault(false);
        } else {
          ko.utils.arrayForEach(self.listItems(), function (selectedItem) {
            selectedItem.isDefault(false);
          });
          item.isDefault(true);
        }
      };
      /// <field type="ko.computed(String)">Derived field specifying the text to display on the "Add item" button.</field>
      self.addButtonText = ko.computed(function () {
        return self.listItems().length > 0 ? initConfig.addAnotherItemText : initConfig.addItemText;
      });
      /// <field type="ko.computed(Boolean)">Derived field specifying whether the "Add item" button should be disabled.</field>
      self.addButtonDisabled = ko.computed(function () {
        return (typeof initConfig.maxItems !== 'undefined' && initConfig.maxItems !== null && self.listItems().length >= initConfig.maxItems);
      });
      /// <field type="ko.computed(String)">Derived field specifying the class to apply to the "Add item" button based on whether it is currently disabled.</field>
      self.addButtonClass = ko.computed(function () {
        return self.addButtonDisabled() ? initConfig.buttonDisabledClass : initConfig.buttonClass;
      });
      /// <field type="ko.observable(String)">The "prompt text" to display in the dropdown list eg. "Please select..."</field>
      self.dropDownListCaption = ko.observable(initConfig.dropDownListCaption);
      /// <field type="String">The tooltip text to display for the delete icon.</field>
      self.removeIconToolTip = initConfig.deleteItemToolTip;

      (function () {
        /// <summary>Pre-populate the selected items in the list on page load as necessary.</summary>
        if (typeof (initConfig.initialItems) === 'undefined' || initConfig.initialItems === null || initConfig.initialItems.length === 0 || initConfig.maxItems <= 0) {
          self.listItems([]);
        } else if (initConfig.maxItems == 1) {
          self.listItems([]);
          self.newListItem(new updateableClientListItem(initConfig.initialItems[0]));
        } else {
          var initialItems = initConfig.initialItems;
          if (initConfig.initialItems.length == 1 && initConfig.initialItems[0].isNonAddedItem) {
            self.listItems([]);
            self.newListItem(new updateableClientListItem(initConfig.initialItems[0]));
          } else if (self.mode !== 'DropDownList') {
            self.listItems(ko.utils.arrayMap(initialItems, function (item) {
              return new updateableClientListItem(item);
            }));
          } else {
            var matches = [];
            ko.utils.arrayForEach(self.selectableItems(), function (selectableItem) {
              ko.utils.arrayForEach(initialItems, function (initialItem) {
                if (initialItem.value === selectableItem.value()) {
                  selectableItem.isDefault(initialItem.isDefault); // make sure default state is persisted
                  matches.push(selectableItem);
                }
              });
            });
            self.selectableItems.removeAll(matches);
            self.listItems(matches);
          }
        }
      })();
    };

    var bindUpdateableClientListViewModel = function (initConfig, controlContainer) {
      /// <summary>Initialises the bindings.</summary>
      var container = document.getElementById(controlContainer);
      if (!ko.dataFor(container))
        ko.applyBindings(new updateableClientListViewModel(initConfig), container);
    };

    var autoCompleteItemSelected = function (hiddenSelectedClientId, item) {
      /// <summary>Handles an item being selecetd in the auto complete extender.</summary>
      var context = ko.contextFor(document.getElementById(hiddenSelectedClientId));
      context.$root.newListItem(new updateableClientListItem({
        value: item.id,
        label: item.label
      }));
    };

    return {
      BindUpdateableClientListViewModel: bindUpdateableClientListViewModel,
      AutoCompleteItemSelected: autoCompleteItemSelected
    };
  } ();

})(window.focusSuite = window.focusSuite || {});

/*
* Custom Knockout binding for displaying a dynamic tooltip using the jQuery Tooltipster plugin.
*/
ko.bindingHandlers.tooltipster = {
	init: function(element, valueAccessor) {
		$(element).tooltipster();
		$(element).tooltipster('content', ko.unwrap(valueAccessor()));
	},
	update: function(element, valueAccessor) {
		$(element).tooltipster('content', ko.unwrap(valueAccessor()));
	}
};

/*
* Custom Knockout binding for controlling the behaviour of the default icon/button.
*/
ko.bindingHandlers.dynamicDefaultIcon = {
	init: function(element, valueAccessor, allBindingsAccessor) {
		var allBindings = ko.unwrap(allBindingsAccessor());
		var options = allBindings.dynamicDefaultIconOptions || {};

		$(element).attr('src', options.setAsDefaultIcon).tooltipster();
		$(element).tooltipster('content', options.setAsDefaultToolTip);
	},
	update: function(element, valueAccessor, allBindingsAccessor) {
		var isDefault = ko.unwrap(valueAccessor());
		var allBindings = ko.unwrap(allBindingsAccessor());
		var options = allBindings.dynamicDefaultIconOptions || {};

		$(element)
		.attr('src', isDefault ? options.defaultIcon : options.setAsDefaultIcon)
		.tooltipster('content', isDefault ? options.defaultToolTip : options.setAsDefaultToolTip);
	}
};