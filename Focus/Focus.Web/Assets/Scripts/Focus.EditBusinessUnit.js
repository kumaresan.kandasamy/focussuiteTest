﻿/*! Copyright © 2000-2015 Burning Glass International Inc.
*
* Proprietary and Confidential
*
* All rights are reserved. Reproduction or transmission in whole or in part, in
* any form or by any means, electronic, mechanical or otherwise, is prohibited
* without the prior written consent of Burning Glass International Inc. */

/* Dependencies: 
*
*   jQuery
*/

/*jshint forin:true, noarg:true, noempty:true, eqeqeq:true, bitwise:true, undef:true, unused:false, browser:true, devel:true, jquery:true, esnext:true, es3:true, maxerr:50, maxcomplexity:6 */

(function (focusSuite) {

  focusSuite.editBusinessUnit = function () {
    /// <summary>Defines the editBusinessUnit namespace in the focusSuite namespace.</summary>

    var controlClientId,
		    phoneTextBox,
		    alternatePhoneTextBox,
		    alternatePhone2TextBox,
		    postcodeZipTextBox,
		    ajaxServiceUrl,
		    stateDropdownList,
		    countyDropdownList,
		    countyDropdownListCascadingDropdown,
		    countyDefaultText,
		    townCityTextBox,
		    companyDescriptionTextBox,
		    countyDropdownListCascadingDropdownBehaviour,
    // ReSharper disable once InconsistentNaming
		    stateZZ,
    // ReSharper disable once InconsistentNaming
		    countyOutsideUS,
    // ReSharper disable once InconsistentNaming
		    countryUS,
				extendedPostalCodeMaskPattern,
		    countryDropdownList;

    var init = function (initData) {
      initData = initData || {};

      controlClientId = initData.ControlClientId;
      phoneTextBox = initData.PhoneTextBox;
      alternatePhoneTextBox = initData.AlternatePhoneTextBox;
      alternatePhone2TextBox = initData.AlternatePhone2TextBox;
      postcodeZipTextBox = initData.PostcodeZipTextBox;
      ajaxServiceUrl = initData.AjaxServiceUrl;
      stateDropdownList = initData.StateDropdownList;
      countyDropdownList = initData.CountyDropdownList;
      countyDropdownListCascadingDropdown = initData.CountyDropdownListCascadingDropdown;
      countyDefaultText = initData.CountyDefaultText;
      townCityTextBox = initData.TownCityTextBox;
      companyDescriptionTextBox = initData.CompanyDescriptionTextBox;
      countyDropdownListCascadingDropdownBehaviour = initData.CountyDropdownListCascadingDropdownBehaviour;
      stateZZ = initData.StateZZ;
      countyOutsideUS = initData.CountyOutsideUS;
      countryUS = initData.CountryUS;
      extendedPostalCodeMaskPattern = initData.ExtendedPostalCodeMaskPattern;
      countryDropdownList = initData.CountryDropDownList;

      var prm = Sys.WebForms.PageRequestManager.getInstance();

      prm.add_pageLoaded(function () {
        $('.inFieldLabel > label, .inFieldLabelAlt > label').inFieldLabels();

        $('#' + phoneTextBox).mask("(999) 999-9999", { placeholder: " " });
        $('#' + alternatePhoneTextBox).mask("(999) 999-9999", { placeholder: " " });
        $('#' + alternatePhone2TextBox).mask("(999) 999-9999", { placeholder: " " });
        $('#' + postcodeZipTextBox).mask(extendedPostalCodeMaskPattern, { placeholder: " " });
        $('#' + postcodeZipTextBox).change(function () {
          PopulateAddressFieldsForPostalCode(
					 ajaxServiceUrl,
					 $(this).val(),
					 stateDropdownList,
					 countyDropdownList,
					 countyDropdownListCascadingDropdown,
					 countyDefaultText,
					 townCityTextBox);
        });

        $('#' + companyDescriptionTextBox).attr('maxlength', '2000');

        var behavior = $find(countyDropdownListCascadingDropdownBehaviour);
        if (behavior != null) {
          behavior.add_populated(function () {
            updateCountyDropDown($('#' + countyDropdownList), $('#' + stateDropdownList));
          });
        }
      });


    };

    // ReSharper disable once InconsistentNaming
    var doesNAICSExist = function (source, args) {
      var exists;

      $.ajax({
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: ajaxServiceUrl + '/CheckNaicsExists', //+ arguments.Value,
        dataType: 'json',
        data: '{"naicsText": "' + args.Value + '", "childrenOnly": true}',
        async: false,
        success: function (result) {
          exists = (result.d);
        }
      });

      args.IsValid = exists;
    }

    var updateCountyDropDown = function (countyDropDown, stateDropDown) {
      if (countyDropDown.children('option:selected').val() != '')
        countyDropDown.next().find(':first-child').text(countyDropDown.children('option:selected').text()).parent().addClass('changed');
      else
        setCounty(stateDropDown.val() == stateZZ);

      UpdateStyledDropDown(countyDropDown, stateDropDown.val());
    }

    var updateStateCountry = function () {
      var countyValue = $('#' + countyDropdownList).val();
      var stateValue = $('#' + stateDropdownList).val();

      if (countyValue == '<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>') {
        if (stateValue != stateZZ)
          setState(true);
        setCountry(true);
      }
    }

    var updateStateCounty = function () {
      var countryValue = $('#' + countryDropdownList).val();
      var stateDropdown = $('#' + stateDropdownList);
      var stateValue = stateDropdown.val();

      if (countryValue == countryUS) {
        if (stateValue == stateZZ) {
          setState(false);

          countyInvokeCountyCascadeDropdown();
        }
      }
      else {
        if (stateValue != stateZZ) {
          setState(true);

          countyInvokeCountyCascadeDropdown();
        }
      }
    }

    var updateCountry = function () {
      var stateValue = $('#' + stateDropdownList).val();
      setCountry(stateValue == stateZZ);
    }

    // ReSharper disable once InconsistentNaming
    var setState = function (outsideUS) {
      var stateDropDown = $('#' + stateDropdownList);

      if (outsideUS)
        $(stateDropDown).children('option[value="' + stateZZ + '"]').prop('selected', true);
      else
        $(stateDropDown).children().first().prop('selected', true);

      $(stateDropDown).next().find(':first-child').text($(stateDropDown).children('option:selected').text()).parent().addClass('changed');
    }

    // ReSharper disable once InconsistentNaming
    var setCountry = function (outsideUS) {
      var countryDropDown = $('#' + countryDropdownList);

      if (outsideUS)
        $(countryDropDown).children().first().prop('selected', true);
      else
        $(countryDropDown).children('option[value="' + countryUS + '"]').prop('selected', true);

      $(countryDropDown).next().find(':first-child').text($(countryDropDown).children('option:selected').text()).parent().addClass('changed');
    }

    // ReSharper disable once InconsistentNaming
    var setCounty = function (outsideUS) {
      var countyDropDown = $('#' + countyDropdownList);

      if (outsideUS)
        $(countyDropDown).children('option[value="' + countyOutsideUS + '"]').prop('selected', true);
      else
        $(countyDropDown).children().first().prop('selected', true);

      $(countyDropDown).next().find(':first-child').text($(countyDropDown).children('option:selected').text()).parent().addClass('changed');
    }

    var countyInvokeCountyCascadeDropdown = function () {
      $get(countyDropdownList)._behaviors[0]._onParentChange(null, null);
    }

    return {
      init: init,
      doesNAICSExist: doesNAICSExist,
      updateStateCountry: updateStateCountry,
      updateStateCounty: updateStateCounty,
      updateCountry: updateCountry
    };
  } ();

})(window.focusSuite = window.focusSuite || {});