﻿$(document).ready(function () {
  accordionMenuSetupClickEvents();
});

function accordionMenuSetupClickEvents() {
  // *** The following handles a first style of multiple accordion menus ***
  $('.multipleAccordionTitle').on('click', null, function () {
    var isOn = $(this).hasClass("on");

    // Remove the on class from all buttons
    $('.multipleAccordionTitle').removeClass('on');

    // No matter what we close all open slides
    $('.accordionContent').hide();

    // If the next slide wasn't open then open it 
    if ($(this).nextAll(".accordionContent").is(':hidden') == true) {
      // Add the on class to the button
      $(this).addClass('on');

      // Open the slide
      $(this).next('.accordionContent').toggle();
    }

    // if clicking on an already open accordion - close it
    if (isOn) {
      $(this).removeClass("on");
      $(this).next('.accordionContent').toggle();
    }
  });

  // *** The following handles a second style of multiple accordion menus ***
  $('.multipleAccordionTitle2').on('click', null, function () {
    var isOn = $(this).hasClass("on");

    // Remove the on class from all buttons
    $('.multipleAccordionTitle2').removeClass('on');

    // No matter what we close all open slides
    $('.accordionContent2').hide();

    // If the next slide wasn't open then open it 
    if ($(this).nextAll(".accordionContent2").is(':hidden') == true) {
      // Add the on class to the button
      $(this).addClass('on');

      // Open the slide
      $(this).next('.accordionContent2').toggle();
    }

    // if clicking on an already open accordion - close it
    if (isOn) {
      $(this).removeClass("on");
      $(this).next('.accordionContent2').toggle();
    }
  });

  // Adds the .over class from the stylesheet on mouseover - currently not being used
  $('.multipleAccordionTitle').on('mouseover', null, function () {
    $(this).addClass('over');

    // On mouseout remove the over class
  }).on('mouseout', null, function () {
    $(this).removeClass('over');
  });

  $('.accordionContent').hide();
  $('.accordionContent2').hide();
  $('.open').show();

  // *** The following handles single accordions ***
  $('.singleAccordionTitle').not('.excludeFromAccordion').on('click', null, function () {
    // Toggle the 'on' class
    if ($(this).hasClass('on'))
      $(this).removeClass('on');
    else
      $(this).addClass('on');

    $(this).next('.singleAccordionContent').toggle();

    // Toggle on class for content wrapper for Wisconsin styling
	  if (focusSuite.brandIdentifier === "Wisconsin") {
			var singleAccordianContentWrapper = $(this).next('div').find('.singleAccordionContentWrapper');
			if (singleAccordianContentWrapper.length == 0) {
	  		singleAccordianContentWrapper = $(this).parent().parent('tr').next().find('.singleAccordionContentWrapper');
			}
			if (singleAccordianContentWrapper.length == 0) {
				singleAccordianContentWrapper = $(this).closest('.singleAccordionContentWrapper');
			}
			 singleAccordianContentWrapper.toggleClass('on');
	  }

	});

	$('.advancedSearchAccordionTitle').on('click', null, function() {
		if (focusSuite.brandIdentifier === "Wisconsin") {
			$(this).toggleClass('on');
		}
	});

  $('.open').show();

  window.setTimeout(accordionMenuChildPanelsNotHidden, 100);
}

// Temp fix for IE8 for CollapsiblePanelExtender
// CollapsiblePanelExtender wraps a div around the child panel, and sets the "display" property on this new DIV
// In IE8, for some reason, the child panel is set to be hidden too, but this is not updated when the CollapsiblePanelExtender is expanded
function accordionMenuChildPanelsNotHidden() {
	$('.singleAccordionTitle').not('.excludeFromAccordion').each(function () {
		var nextDiv = $(this).next();
		if (nextDiv.attr("class") == "" && nextDiv.attr("id") == "") {
			var childDiv = nextDiv.children(".accordionContent");
			if (childDiv.exists()) {
				nextDiv.css("height", "auto");
				childDiv.css("display", "block");
			}
		}
	});
}