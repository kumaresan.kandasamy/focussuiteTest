﻿ko.oneTimeDirtyFlag = function (root) {
  var _initialized;

  //one-time dirty flag that gives up its dependencies on first change
  var result = ko.computed(function () {
    if (!_initialized) {
      //just for subscriptions
      ko.toJS(root);

      //next time return true and avoid ko.toJS
      _initialized = true;

      //on initialization this flag is not dirty
      return false;
    }

    //on subsequent changes, flag is now dirty
    return true;
  });

  return result;
};

ko.dirtyFlag = function (root, isInitiallyDirty) {
  var result = function () { },
        _initialState = ko.observable(ko.toJSON(root)),
        _isInitiallyDirty = ko.observable(isInitiallyDirty);

  result.isDirty = ko.computed(function () {
    return _isInitiallyDirty() || _initialState() !== ko.toJSON(root);
  });

  result.reset = function () {
    _initialState(ko.toJSON(root));
    _isInitiallyDirty(false);
  };

  return result;
};