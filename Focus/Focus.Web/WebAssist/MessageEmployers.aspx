﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MessageEmployers.aspx.cs" Inherits="Focus.Web.WebAssist.MessageEmployers" %>

<%@ Import Namespace="Focus.Core.Views" %>
<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/AssistEmployersNavigation.ascx" TagName="AssistEmployersNavigation" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="CriteriaBuilder" Src="~/WebAssist/Controls/CriteriaBuilder.ascx" %>
<%@ Register Src="~/WebReporting/Controls/CriteriaLocation.ascx" TagName="CriteriaLocation" TagPrefix="uc" %>
<%@ Register Src="~/WebReporting/Controls/CriteriaDateRange.ascx" TagName="CriteriaDateRange" TagPrefix="uc" %>
<%@ Register Src="~/WebReporting/Controls/CriteriaJobCharacteristics.ascx" TagName="CriteriaJobCharacteristics" TagPrefix="uc" %>
<%@ Register Src="~/WebReporting/Controls/CriteriaEmployerCharacteristics.ascx" TagName="CriteriaEmployerCharacteristics" TagPrefix="uc" %>
<%@ Register Src="~/WebReporting/Controls/CriteriaCompliance.ascx" TagName="CriteriaCompliance" TagPrefix="uc" %>
<%@ Register Src="~/WebReporting/Controls/CriteriaEmployerActivity.ascx" TagName="CriteriaEmployerActivity" TagPrefix="uc" %>
<%@ Register Src="~/WebReporting/Controls/ReportCriteriaDisplay.ascx" TagName="ReportCriteriaDisplay" TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%# ResolveUrl("~/Assets/Scripts/Focus.Reporting.js") %>" type="text/javascript"></script>
	<script src="<%# ResolveUrl("~/Assets/Scripts/UpdateableList.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:AssistEmployersNavigation ID="AssistEmployersNavigationMain" runat="server" />
	<div class="grid100">
		<div class="reportgrid80">
			<h1>
				<%= HtmlLocalise("PageTitle.Text", "Send messages")%></h1>
		</div>
		<div class="reportgrid17" style="text-align: right">
			<asp:Button ID="FindButton" runat="server" Text="Find" class="button3" OnClick="FindButton_Clicked" />
		</div>
	</div>
	<asp:Panel runat="server" ID="CriteriaSelectionPanel">
		<div class="grid100">
			<div class="reportgrid33">
				<div class="reportgrid95">
					<p>
						<asp:Literal runat="server" ID="Column1Header" /></p>
					<uc:CriteriaJobCharacteristics runat="server" ID="CriteriaJobCharacteristics" />
					<uc:CriteriaEmployerCharacteristics runat="server" ID="CriteriaEmployerCharacteristics" />
					<uc:CriteriaCompliance runat="server" ID="CriteriaCompliance" Visible="False" />
				</div>
			</div>
			<div class="reportgrid33">
				<div class="reportgrid95">
					<p>
						<asp:Literal runat="server" ID="Column2Header" /></p>
					<uc:CriteriaEmployerActivity ID="CriteriaEmployerActivity" runat="server" />
				</div>
			</div>
			<div class="reportgrid33">
				<div class="reportgrid95">
					<p>
						<asp:Literal runat="server" ID="Column3Header" /></p>
					<uc:CriteriaLocation runat="server" ID="CriteriaLocation" />
					<uc:CriteriaDateRange runat="server" ID="CriteriaDateRange" />
				</div>
			</div>
		</div>
		<hr />
	</asp:Panel>
	<uc:ReportCriteriaDisplay runat="server" ID="CriteriaDisplay" Visible="False" ShowButtons="True" OnActionRequested="ReportCriteriaDisplay_ActionRequested" />
	<div>
		<table role="presentation" style="width: 100%;">
			<tr>
				<td>
					<h3>
						<%= HtmlLocalise("Employers.Title", "#BUSINESSES#")%></h3>
				</td>
				<td style="text-align: right;">
					<div id="EmployeeButtonsSection" class="opacity">
						<div id="EmployeeButtonsOverlay" class="overlay">
						</div>
						<asp:Button ID="ComposeMessageButton" runat="server" CssClass="button1" UseSubmitBehavior="False" OnClientClick="MessageEmployers_GoToComposeMessage();return false;" />&nbsp;&nbsp;
					</div>
				</td>
			</tr>
		</table>
		<div style="float: left; width: 380px" id="BasicCriteriaPanel" runat="server" visible="False">
			<uc:CriteriaBuilder ID="EmployerCriteriaBuilder" runat="server" OnCommandButtonClicked="EmployerCriteriaBuilderCommandButton_Clicked" />
		</div>
		<div style="float: <%=BasicCriteriaPanel.Visible ? "right" : "left"%>">
			<i>
				<asp:Literal ID="PageInstructions" runat="server" /></i>
			<div id="PagerDiv" style="width: 100%" class="PagerDiv" runat="server" visible="False">
				<uc:Pager ID="SearchResultListPager" runat="server" PagedControlId="EmployeesList" PageSize="10" DisplayRecordCount="True" />
				<%=HtmlLocalise("ItemsSelected.Header", "Contact(s) selected: ") %>
				<span id="MessageEmployers_SelectedCountLabel" runat="server" clientidmode="Static">0</span>
			</div>
			<asp:ListView ID="EmployeesList" runat="server" ItemPlaceholderID="SearchResultPlaceHolder" OnItemDataBound="EmployeesList_OnItemDataBound" DataSourceID="SearchResultDataSource" DataKeyNames="Id, BusinessUnitId" Visible="False" OnLayoutCreated="EmployeesList_LayoutCreated">
				<LayoutTemplate>
					<table style="width: 100%;">
						<thead class="tableHeader">
							<tr>
								<td style="width: 20px;">
									<asp:CheckBox ID="MessageEmployers_HeaderSelectorCheckBox" runat="server" ClientIDMode="Static" />
								</td>
								<td style="width: 380px;">
									<asp:Literal runat="server" ID="EmployerNameHeaderLabel" />
								</td>
								<td style="width: 380px;">
									<focus:LocalisedLabel ID="EmployeeListHeader2" runat="server" LocalisationKey="EmployeeListHeaderContact.Text" DefaultText="Contact name" />
								</td>
							</tr>
						</thead>
						<tbody class="tableBody">
							<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
						</tbody>
					</table>
				</LayoutTemplate>
				<ItemTemplate>
					<tr>
						<td style="width: 20px;">
							<asp:CheckBox ID="SelectorCheckBox" runat="server" ClientIDMode="Static" CssClass="SelectorCheckBox" />
						</td>
						<td style="width: 380px;">
							<%# ((EmployeeSearchResultView)Container.DataItem).BusinessUnitName%>
						</td>
						<td style="width: 380px;">
							<%# ((EmployeeSearchResultView)Container.DataItem).LastName%>,
							<%# ((EmployeeSearchResultView)Container.DataItem).FirstName%>
						</td>
					</tr>
				</ItemTemplate>
				<EmptyDataTemplate>
					<tr>
						<td style="text-align: left;" colspan="4">
							<br />
							<%= HtmlLocalise("EmployeeList.NoResult", "No matches")%>
					</tr>
				</EmptyDataTemplate>
			</asp:ListView>
			<asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.WebAssist.MessageEmployers" EnablePaging="true" SelectMethod="GetEmployees" SelectCountMethod="GetEmployeesCount" SortParameterName="orderBy" OnSelecting="SearchResultDataSource_OnSelecting"></asp:ObjectDataSource>
			<asp:CustomValidator ID="EmployeesListValidator" runat="server" ClientValidationFunction="validateEmployeesList" ValidationGroup="MessageEmployer" ValidateEmptyText="True" CssClass="error" />
			<br />
			<hr />
			<br />
			<asp:HiddenField runat="server" ID="SelectedEmployeeIds" />
			<asp:HiddenField runat="server" ID="SelectedBusinessUnitIds" />
			<div id="ComposeMessageSection" class="opacity">
				<div id="ComposeMessageOverlay" class="overlay">
				</div>
				<h3>
					<%= HtmlLocalise("ComposeMessage.Title", "Compose message")%></h3>
				<asp:UpdatePanel ID="ComposeMessageUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<table role="presentation">
							<tr>
								<td>
									<%=HtmlLabel("MessageFormat.Label", "Message format") %>
								</td>
								<td>
									<asp:RadioButtonList ID="MessageFormatRadioButtonList" RepeatDirection="Horizontal" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="MessageFormatRadioButtonList_SelectedIndexChanged" AutoPostBack="true" role="presentation" />
								</td>
							</tr>
						</table>
						<p>
							<focus:LocalisedLabel runat="server" ID="SavedMessagesDropDownLabel" AssociatedControlID="SavedMessagesDropDown" LocalisationKey="SavedMessages" DefaultText="Saved messages" CssClass="sr-only"/>
							<asp:DropDownList ID="SavedMessagesDropDown" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="SavedMessagesDropDown_SelectedIndexChanged" />
						</p>
						<p>
							<%= HtmlInFieldLabel("SavedMessageNameTextBox", "SavedMessageName.InlineLabel", "name your saved message", 500)%>
							<asp:TextBox ID="SavedMessageNameTextBox" runat="server" Width="500" MaxLength="100" ClientIDMode="Static" />
							<asp:RequiredFieldValidator ID="SavedMessageNameRequired" runat="server" ControlToValidate="SavedMessageNameTextBox" ValidationGroup="SaveMessageTexts" CssClass="error" />
						</p>
						<p>
							<%= HtmlInFieldLabel("MessageTextbox", "Message.InlineLabel", "add new message here", 500)%>
							<asp:TextBox ID="MessageTextbox" runat="server" Width="500" MaxLength="1000" ClientIDMode="Static" TextMode="MultiLine" Rows="5" />
							<asp:RequiredFieldValidator ID="MessageRequired" runat="server" ControlToValidate="MessageTextbox" ValidationGroup="MessageEmployer" CssClass="error" />
							<asp:RequiredFieldValidator ID="MessageRequiredSaveText" runat="server" ControlToValidate="MessageTextbox" ValidationGroup="SaveMessageTexts" CssClass="error" />
						</p>
						<p>
							<asp:CheckBox runat="server" ID="EmailMeCopyOfMessageCheckBox" Checked="True" />&nbsp;
							<focus:LocalisedLabel runat="server" ID="EmailMeCopyOfMessageLabel" DefaultText="email me a copy of this message" AssociatedControlID="EmailMeCopyOfMessageCheckBox" />
						</p>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="SavedMessagesDropDown" EventName="SelectedIndexChanged" />
					</Triggers>
				</asp:UpdatePanel>
				<p>
					<asp:Button ID="SendMessageButton" runat="server" CssClass="button2" OnClick="SendMessageButton_Clicked" ValidationGroup="MessageEmployer" />
					<asp:Button ID="SaveMessageTextButton" runat="server" CssClass="button3" OnClick="SaveMessageTextButton_Clicked" ValidationGroup="SaveMessageTexts" />
				</p>
			</div>
		</div>
		<div style="clear: both">
		</div>
	</div>
	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
	<script type="text/javascript">
		$(document).ready(function () {
			$('td').delegate('.deleteItem', 'click', function (e) {
				RemoveListItem($(this));
				e.stopPropagation();
			});
		});

		var MessageEmployers_allSelectorCheckBoxes = $('.SelectorCheckBox > input:checkbox');

		var MessageEmployers_selectedEmployeesField = $("#<%=SelectedEmployeeIds.ClientID %>");
		var MessageEmployers_selectedEmployeesArray = new Array();
        
		var MessageEmployers_selectedBusinessUnitsField = $("#<%=SelectedBusinessUnitIds.ClientID %>");
		var MessageEmployers_selectedBusinessUnitsArray = new Array();

		function pageLoad() {
		    MessageEmployers_allSelectorCheckBoxes.click(function () { MessageEmployers_checkBoxHandler($(this)); });
		    $("#MessageEmployers_HeaderSelectorCheckBox").attr("Title", "Select All Employers");
			$("#MessageEmployers_HeaderSelectorCheckBox").click(function () { MessageEmployers_HeaderCheckbox($(this)); });
			$(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();

			if (MessageEmployers_selectedEmployeesField.val().length > 0) {
				MessageEmployers_selectedEmployeesArray = MessageEmployers_selectedEmployeesField.val().split(",");
				MessageEmployers_allSelectorCheckBoxes.each(function () {
					var seekerId = $(this).parent().attr("data-value");
					if ($.inArray(seekerId, MessageEmployers_selectedEmployeesArray) >= 0) {
						$(this).prop("checked", true);
					}
				});
			}

			if (MessageEmployers_selectedBusinessUnitsField.val().length > 0) {
				MessageEmployers_selectedBusinessUnitsArray = MessageEmployers_selectedBusinessUnitsField.val().split(",");
			}

			MessageEmployers_checkBoxHandler();
		}

		function MessageEmployers_HeaderCheckbox(oSrc) {
			if (oSrc.is(":checked")) {
				MessageEmployers_allSelectorCheckBoxes.each(function () {
					var oChk = $(this);
					oChk.prop("checked", true);
					MessageEmployers_AddToSelected(oChk.parent().attr("data-value"), oChk.parent().attr("data-businessunitid"));
				});
			} else {
				MessageEmployers_allSelectorCheckBoxes.each(function () {
					var oChk = $(this);
					oChk.prop("checked", false);
					MessageEmployers_RemoveFromSelected(oChk.parent().attr("data-value"), oChk.parent().attr("data-businessunitid"));
				});
			}

			MessageEmployers_ProcessSelected();
		}

		function validateEmployeesList(oSrc, args) {
			args.IsValid = (MessageEmployers_selectedEmployeesArray.length > 0);
		}

		function MessageEmployers_checkBoxHandler(oChk) {
			if (oChk != null) {
				var val = oChk.parent().attr("data-value");
				var empId = oChk.parent().attr("data-businessunitid");
				if (oChk.is(":checked")) {
					MessageEmployers_AddToSelected(val, empId);
				} else {
					MessageEmployers_RemoveFromSelected(val, empId);
				}
			}

			var selectedPageCount = 0;
			MessageEmployers_allSelectorCheckBoxes.each(function () {
				if ($(this).is(":checked"))
					selectedPageCount++;
			});
			$("#MessageEmployers_HeaderSelectorCheckBox").prop("checked", selectedPageCount == MessageEmployers_allSelectorCheckBoxes.length);

			MessageEmployers_ProcessSelected();
		}

		function MessageEmployers_ProcessSelected() {
			$("#MessageEmployers_SelectedCountLabel").html(MessageEmployers_selectedEmployeesArray.length);

			if (MessageEmployers_selectedEmployeesArray.length > 0)
				showComposeMessage();
			else
				hideComposeMessage();
		}

		function MessageEmployers_AddToSelected(value, businessUnitId) {
			if ($.inArray(value, MessageEmployers_selectedEmployeesArray) < 0) {
				MessageEmployers_selectedEmployeesArray.push(value);
				MessageEmployers_selectedEmployeesField.val(MessageEmployers_selectedEmployeesArray.join(","));
			}

			if ($.inArray(businessUnitId, MessageEmployers_selectedBusinessUnitsArray) < 0) {
				MessageEmployers_selectedBusinessUnitsArray.push(businessUnitId);
				MessageEmployers_selectedBusinessUnitsField.val(MessageEmployers_selectedBusinessUnitsArray.join(","));
			}
		}

		function MessageEmployers_RemoveFromSelected(value, businessUnitId) {
			var index = $.inArray(value, MessageEmployers_selectedEmployeesArray);
			if (index >= 0) {
				MessageEmployers_selectedEmployeesArray.splice(index, 1);
				MessageEmployers_selectedEmployeesField.val(MessageEmployers_selectedEmployeesArray.join(","));
			}

			index = $.inArray(businessUnitId, MessageEmployers_selectedBusinessUnitsArray);
			if (index >= 0) {
				MessageEmployers_selectedBusinessUnitsArray.splice(index, 1);
				MessageEmployers_selectedBusinessUnitsField.val(MessageEmployers_selectedBusinessUnitsArray.join(","));
			}
		}

		function showComposeMessage() {
			$('#ComposeMessageSection').removeClass('opacity');
			$('#ComposeMessageOverlay').removeClass('overlay');

			$('#EmployeeButtonsSection').removeClass('opacity');
			$('#EmployeeButtonsOverlay').removeClass('overlay');
		}

		function hideComposeMessage() {
			$('#ComposeMessageSection').addClass('opacity');
			$('#ComposeMessageOverlay').addClass('overlay');

			$('#EmployeeButtonsSection').addClass('opacity');
			$('#EmployeeButtonsOverlay').addClass('overlay');
		}

		function MessageEmployers_GoToComposeMessage() {
			$("#<%=MessageTextbox.ClientID%>").focus();
			$("html, body").scrollTop($(document).height());
		}
	</script>
</asp:Content>
