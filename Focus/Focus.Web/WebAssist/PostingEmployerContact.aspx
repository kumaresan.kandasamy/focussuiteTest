﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PostingEmployerContact.aspx.cs" Inherits="Focus.Web.WebAssist.PostingEmployerContact" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistEmployersNavigation.ascx" tagname="AssistEmployersNavigation" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigation1" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:AssistEmployersNavigation ID="AssistEmployersNavigationMain" runat="server" />
	<br/>
	<asp:HyperLink runat="server" ID="ReturnToJobDevelopmentLink" />
	<br/>
	<br/>
	<h1><asp:Literal ID="TitleLiteral" runat="server" /></h1>
	<table>
		<tr style="min-height:30px">
			<td colspan="3"><b><focus:LocalisedLabel runat="server" ID="PostingContactDetails" RenderOuterSpan="False" LocalisationKey="PostingContactDetails.Label" DefaultText="Contact details from job posting"/></b></td>
		</tr>
		<tr style="min-height:30px">
			<td><focus:LocalisedLabel runat="server" ID="EmployerLabel" RenderOuterSpan="False" LocalisationKey="Employer.Label" DefaultText="Employer"/></td>
			<td style="width:25px"/>
			<td><asp:Literal ID="EmployerLiteral" runat="server" /></td>
		</tr>
		<tr style="min-height:30px">
			<td><focus:LocalisedLabel runat="server" ID="HiringManagerLabel" RenderOuterSpan="False" LocalisationKey="HiringManager.Label" DefaultText="Hiring Manager"/></td>
			<td/>
			<td><asp:Literal ID="ContactLiteral" runat="server" /></td>
		</tr>
		<tr style="min-height:30px">
			<td><focus:LocalisedLabel runat="server" ID="TelephoneLabel" RenderOuterSpan="False" LocalisationKey="Telephone.Label" DefaultText="Phone Number"/></td>
			<td/>
			<td><asp:Literal ID="TelephoneLiteral" runat="server" /></td>
		</tr>
		<tr style="min-height:30px">
			<td><focus:LocalisedLabel runat="server" ID="EmailLabel" RenderOuterSpan="False" LocalisationKey="Email.Label" DefaultText="Email"/></td>
			<td/>
			<td><asp:Literal ID="EmailLiteral" runat="server" /></td>
		</tr>
		<tr style="min-height:30px">
			<td><focus:LocalisedLabel runat="server" ID="AddressLabel" RenderOuterSpan="False" LocalisationKey="Address.Label" DefaultText="Address"/></td>
			<td/>
			<td><asp:Literal ID="AddressLiteral" runat="server" /></td>
		</tr>
	</table>
</asp:Content>
