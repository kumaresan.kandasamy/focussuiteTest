﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SavedSearches.aspx.cs" Inherits="Focus.Web.WebAssist.SavedSearches" %>
<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>
<%@ Register src="~/Code/Controls/User/SavedSearchesList.ascx" tagname="SavedSearchesList" tagprefix="uc" %>
<%@ Register Src="Controls/AccountSettingsLinks.ascx" TagPrefix="uc" TagName="SettingsLinks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<uc:SettingsLinks ID="SettingLinksMain" runat="server" />

	<h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>
  <uc:SavedSearchesList ID="SavedSearchesList" runat="server" />	
	
</asp:Content>
