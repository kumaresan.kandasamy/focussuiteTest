﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Xml;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Common.Extensions;
using Focus.Core.Views;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	public partial class JobMatching : PageBase
	{
		#region Page Properties
		
		protected string JobId = "";
		protected string FromUrl = "";
    protected string InitialText = "";
    protected string ROnet = "";
		
		#endregion

		public ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue<ExplorerCriteria>("JobModal:ReportCriteria"); }
			set { SetViewStateValue("JobModal:ReportCriteria", value); }
		}

    public ExplorerJobReportView JobReport
		{
      get { return GetViewStateValue<ExplorerJobReportView>("JobModal:JobReport"); }
			set { SetViewStateValue("JobModal:JobReport", value); }
		}

		public List<LightboxBreadcrumb> Breadcrumbs
		{
			get { return GetViewStateValue("JobModal:Breadcrumb", new List<LightboxBreadcrumb>()); }
			set { SetViewStateValue("JobModal:Breadcrumb", value); }
		}

		public CareerPathDirections CareerPathDirection
		{
			get { return GetViewStateValue<CareerPathDirections>("JobModal:CareerPathDirection"); }
			set { SetViewStateValue("JobModal:CareerPathDirection", value); }
		}

		public JobCareerPathModel CareerPaths
		{
			get { return GetViewStateValue<JobCareerPathModel>("JobModal:CareerPaths"); }
			set { SetViewStateValue("JobModal:CareerPaths", value); }
		}

		public JobDegreeCertificationModel DegreeCertifications
		{
			get { return GetViewStateValue<JobDegreeCertificationModel>("JobModal:DegreeCertifications"); }
			set { SetViewStateValue("JobModal:DegreeCertifications", value); }
		}

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if(!App.User.IsAuthenticated)
				Response.RedirectToRoute("Login");

			base.OnInit(e);
		}

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      JobId = Page.RouteData.Values["jobid"] as string;
			FromUrl = Page.RouteData.Values["fromURL"] as string;

			var jobSeekerId = (Page.RouteData.Values["jobseekerid"].IsNotNull()) ? Page.RouteData.Values["jobseekerid"].ToString() : "";

			if (jobSeekerId == "0" || jobSeekerId == "")
				InitialText = App.Settings.Theme == FocusThemes.Education
											? "People who have been placed into the <b>{0}</b> job typically have the following characteristics.  Please check to ensure that you did not exclude one or more of these skills from your resume."
											: "People who have placed into the <b>{0}</b> job typically have the following characteristics.";

			if (jobSeekerId != "0" && jobSeekerId != "")
				InitialText = App.Settings.Theme == FocusThemes.Education
											? "People who have been placed into the <b>{0}</b> job typically have the following characteristics.  Please check to ensure that you did not exclude one or more of these skills from your resume."
											: "People who have placed into the <b>{0}</b> job typically have the following characteristics. We've identified gaps in your job seeker's resume and skill set, and provided you with recommendations that you can discuss with them.";
			
			BackHyperLink.NavigateUrl = GetRouteUrl(FromUrl, null);

			OpenAccordion("CommonSkills");
			if (!IsPostBack)
			{
			  ApplyTheme();
				LocaliseUI();
				BindControls();

				var expJobId = GetExplorerJobId(JobId);
				if (expJobId != 0)
					LoadMatches(expJobId);
				else
				{
					panSkillGap.Visible = EducationRequirementsPlaceHolder.Visible = ExperienceRequiredPlaceHolder.Visible = panCareerPath.Visible = panSkillTraining.Visible = panDegreeRequired.Visible = false;
					lblSkillGapInfo.Visible = lblEducationLevelInfo.Visible = lblCertificationInfo.Visible = lblExperienceLevelInfo.Visible = lblCareerPathInfo.Visible = lblSkillTrainingInfo.Visible = lblDegreeRequiredInfo.Visible = true;
					lblSkillGapInfo.Text = lblEducationLevelInfo.Text = lblExperienceLevelInfo.Text = lblCareerPathInfo.Text = lblSkillTrainingInfo.Text = lblDegreeRequiredInfo.Text = "We're still collecting data. We will have more feedback later.";
					lblCertificationInfo.Text = "There are no common certifications or licenses for this type of job.";
				}
				LoadTypicalResumes(JobId, false);
			}
			if (IsPostBack)
        RegisterClientStartupScript("JobMatchingScriptAssist");

	    TypicalResumesTitle.Visible = false;
	    TypicalResumesContent.Visible = false;
		}
    
    /// <summary>
    /// Opens the accordion.
    /// </summary>
    /// <param name="tabName">Name of the tab.</param>
		private void OpenAccordion(string tabName)
		{
			//Clear the existing on/open class
			CommonSkillsTitle.Attributes.Add("class", "multipleAccordionTitle");
			CommonSkillsContent.Attributes.Add("class", "accordionContent");
			CommonCareerPathTitle.Attributes.Add("class", "multipleAccordionTitle");
			CommonCareerPathContent.Attributes.Add("class", "accordionContent");

			switch (tabName)
			{
				case "CommonSkills":
					CommonSkillsTitle.Attributes.Add("class", "multipleAccordionTitle on");
					CommonSkillsContent.Attributes.Add("class", "accordionContent open");
					break;

				case "CommonCareerPath":
					CommonCareerPathTitle.Attributes.Add("class", "multipleAccordionTitle on");
					CommonCareerPathContent.Attributes.Add("class", "accordionContent open");
					break;

				default:
					CommonSkillsTitle.Attributes.Add("class", "multipleAccordionTitle on");
					CommonSkillsContent.Attributes.Add("class", "accordionContent open");
					break;
			}
		}

    /// <summary>
    /// Loads the matches.
    /// </summary>
    /// <param name="expJobId">The exp job id.</param>
		private void LoadMatches(long expJobId)
    {
      if (ROnet.IsNotNullOrEmpty())
      {
        var job = ServiceClientLocator.ExplorerClient(App).GetJobFromROnet(ROnet);
        if (job.IsNotNull())
          lblJobMatchInfo.Text = string.Format(InitialText, job.Name);
      }
			LoadCommonSkills(expJobId);
			LoadEducationLevel(expJobId);
			LoadCertificationsAndLicenses(expJobId);
			LoadExperienceLevel(expJobId);
			LoadCareerPath(expJobId);
			LoadSkillGap(expJobId);
		}

    /// <summary>
    /// Sets visibility based on the theme
    /// </summary>
    private void ApplyTheme()
    {
      var visible = (App.Settings.Theme == FocusThemes.Workforce);

      PotentialGapsTitle.Visible = visible;
      PotentialGapsContent.Visible = visible;
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			CareerPathFromLinkButton.Text = CodeLocalise("WherePeopleGoFromHere.LinkText", "Where people go from here");
			CareerPathToLinkButton.Text = CodeLocalise("HowDidOtherPeopleGetHere.LinkText", "How did other people get here");
		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
		private void BindControls()
		{
			CareerPathFromLinkButton.CommandArgument = CareerPathDirections.FromThisJob.ToString();
			CareerPathFromLinkButton.CommandName = "LoadCareerPaths";
			CareerPathToLinkButton.CommandArgument = CareerPathDirections.ToThisJob.ToString();
			CareerPathToLinkButton.CommandName = "LoadCareerPaths";
		}

    /// <summary>
    /// Gets the explorer job id.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    public long GetExplorerJobId(string jobId)
    {
      long expJobId = 0;
      try
      {
        var xmlposting = App.GetSessionValue<string>("Career:XMLPosting");
        if (xmlposting.IsNotNull())
        {
          var xDoc = new XmlDocument { PreserveWhitespace = true };
          xDoc.LoadXml(xmlposting);

          var criteria = new ExplorerCriteria();

          // KNK 2012.12.12
          // Focus/Career™ formated postings contain job title and onet already classified in different tags
          // job title => //special/jobtitle
          // onet code => //special/cf008

          var jobDetail = "Job id: " + jobId;
          var postingBGTOcc = string.Empty;
          if (xDoc.SelectSingleNode("//special/jobtitle") != null && xDoc.SelectSingleNode("//special/jobtitle").InnerText.Trim().IsNotNullOrEmpty())
          {
            lblJobMatchInfo.Text = string.Format(InitialText, xDoc.SelectSingleNode("//special/jobtitle").InnerText.Trim());
            criteria.CareerAreaJob = xDoc.SelectSingleNode("//special/jobtitle").InnerText.Trim();
          }
          else if (xDoc.SelectSingleNode("//title") != null)
          {
            lblJobMatchInfo.Text = string.Format(InitialText, xDoc.SelectSingleNode("//title").InnerText.Trim());
            criteria.CareerAreaJob = xDoc.SelectSingleNode("//title").InnerText.Trim();
          }

          ROnet = null;
          var oNet = string.Empty;
          if (xDoc.SelectSingleNode("//special/cf031") != null && xDoc.SelectSingleNode("//special/cf031").InnerText.Trim().IsNotNullOrEmpty())
          {
            postingBGTOcc = xDoc.SelectSingleNode("//special/cf031").InnerText.Trim().Split(',')[0];
          }
          else if (xDoc.SelectSingleNode("//special/cf008") != null && xDoc.SelectSingleNode("//special/cf008").InnerText.Trim().IsNotNullOrEmpty())
          {
            oNet = xDoc.SelectSingleNode("//special/cf008").InnerText.Trim().Split(',')[0];
          }
          else if (xDoc.SelectSingleNode("//title") != null && xDoc.SelectSingleNode("//title").Attributes["onet"] != null && xDoc.SelectSingleNode("//title").Attributes["onet"].Value.IsNotNullOrEmpty())
          {
            oNet = xDoc.SelectSingleNode("//title").Attributes["onet"].Value.Trim();
          }


          ROnet = ServiceClientLocator.PostingClient(App).GetPostingBGTOccs(jobId);
          
					if (ROnet.IsNull())
          {
            // Ensure ROnet is numeric
						ROnet = postingBGTOcc.IsNotNullOrEmpty() ? postingBGTOcc.FormatToOnetPattern() : GetROnet(oNet.FormatToOnetPattern());
          }


          if (ROnet.IsNotNullOrEmpty())
          {
            expJobId = ServiceClientLocator.ExplorerClient(App).GetJobIdFromROnet(ROnet);

            //check for expJobId, if not send an email.
            if (expJobId.IsNull() || expJobId == 0)
            {
              jobDetail += string.Format("<br />Job title: {0} <br />BGTOcc: {1}", criteria.CareerAreaJob, ROnet);
              SendEmail(jobDetail, "Explorer type job not found");
            }

            criteria.CareerAreaJobId = expJobId;
            ReportCriteria = criteria;

            var breadcrumbs = new List<LightboxBreadcrumb>();
            Breadcrumbs = breadcrumbs;
          }
          else
          {
            jobDetail += string.Format("<br />Job title: {0} <br />Onet: {1}", criteria.CareerAreaJob, oNet.FormatToOnetPattern());
            SendEmail(jobDetail, "BGTOCC not found");
          }
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }

      return expJobId;
    }

    /// <summary>
    /// Gets the r onet from the onet
    /// </summary>
    /// <param name="Onets">The onets.</param>
    /// <returns></returns>
    private string GetROnet(string Onets)
    {
      //Sometimes we get a | delimited list of onets so convert to list
      var oNetsList = Onets.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
      // Get best fit ROnet
      var bestFitConversion = (from oNet in oNetsList select ServiceClientLocator.OccupationClient(App).ConvertOnetToROnet(oNet) into bestFit where bestFit.IsNotNull() select bestFit.ROnetCode).FirstOrDefault();

      return bestFitConversion;
    }

    /// <summary>
    /// Sends the email.
    /// </summary>
    /// <param name="jobDetail">The job detail.</param>
    /// <param name="errorMessage">The error message.</param>
		private void SendEmail(string jobDetail, string errorMessage)
		{
			if (jobDetail.IsNotNullOrEmpty())
			{
				try
				{
					var fromEmailAddress = App.Settings.MaskingAddress;
					var subject = String.Format("{0} - {1}", App.Settings.Application, errorMessage);
					var mailBody = String.Format("<font face='arial'>This message was sent to you by : {0}<br /></font>{1}<br />User: {2}", fromEmailAddress.Trim(), jobDetail, (App.User.IsAuthenticated) ? App.User.EmailAddress : "Anonymous");
					
					ServiceClientLocator.CoreClient(App).SendEmail(App.Settings.OnErrorEmail, subject, mailBody, true);
					
				}
				catch (ApplicationException ex)
				{
					MasterPage.ShowError(AlertTypes.Error, ex.Message);
				}
			}
		}

    /// <summary>
    /// Loads the common skills.
    /// </summary>
    /// <param name="jobId">The job id.</param>
		private void LoadCommonSkills(long jobId)
		{
			try
			{
				//AKV: Use this section to display skill gap icons in common skills
				#region Possible Skill Gap Icons
				//Resume Model = App.GetSessionValue<Resume>("Career:Resume");
				//if (Model.IsNull())
				//{
				//  UserContext usercontext = App.GetSessionValue<UserContext>("Career:UserContext");
				//  Model = ServiceClientLocator.ResumeClient(App).GetDefaultResume(usercontext.UserId, true);
				//  App.SetSessionValue<Resume>("Career:Resume", Model);
				//}

				//MyResumeModel expResumeModel = new MyResumeModel
				//{
				//  PersonResume = new Data.Core.DataTransferObjects.PersonResumeDto()
				//};

				//if (Model.IsNotNull() && Model.ResumeContent.IsNotNull() && Model.ResumeContent.Skills.IsNotNull())
				//{
				//  expResumeModel.PersonResume.ResumeSkills = (Model.ResumeContent.Skills.Skills.Count > 0 ? Model.ResumeContent.Skills.Skills.CareerSerialize() : null);
				//  App.SetSessionValue<MyResumeModel>("Explorer:ResumeModel", expResumeModel);
				//}
				#endregion

				SkillList.JobId = jobId;
				SkillList.BindList(true, JobSkillTypes.Demanded, SkillTypes.Specialized);
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Loads the education level.
    /// </summary>
    /// <param name="jobId">The job id.</param>
		private void LoadEducationLevel(long jobId)
		{
			try
			{
				EducationRequirementsPlaceHolder.Visible = true;

				var educationRequirements = ServiceClientLocator.ExplorerClient(App).GetJobEducationRequirements(jobId);
				if (educationRequirements.Count > 0)
				{
					var educationRequirement =
						educationRequirements.SingleOrDefault(
							x => x.EducationRequirementType == EducationRequirementTypes.HighSchoolTechnicalTraining);

					HighSchoolTechnicalTrainingLiteral.Text =
						String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
													educationRequirement == null
														? "0"
														: Math.Round(educationRequirement.RequirementPercentile, 0).ToString());

					educationRequirement =
						educationRequirements.SingleOrDefault(
							x => x.EducationRequirementType == EducationRequirementTypes.AssociatesDegree);

					AssociatesDegreeLiteral.Text =
						String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
													educationRequirement == null
														? "0"
														: Math.Round(educationRequirement.RequirementPercentile, 0).ToString());

					educationRequirement =
						educationRequirements.SingleOrDefault(
							x => x.EducationRequirementType == EducationRequirementTypes.BachelorsDegree);

					BachelorsDegreeLiteral.Text =
						String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
													educationRequirement == null
														? "0"
														: Math.Round(educationRequirement.RequirementPercentile, 0).ToString());

					educationRequirement =
						educationRequirements.SingleOrDefault(
							x => x.EducationRequirementType == EducationRequirementTypes.GraduateProfessionalDegree);

					GraduateProfessionalDegreeLiteral.Text =
						String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
													educationRequirement == null
														? "0"
														: Math.Round(educationRequirement.RequirementPercentile, 0).ToString());
				}
				else
				{
					EducationRequirementsPlaceHolder.Visible = false;
					lblEducationLevelInfo.Visible = true;
					lblEducationLevelInfo.Text = "We're still collecting data. We will have more feedback later.";
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Loads the certifications and licenses.
    /// </summary>
    /// <param name="jobId">The job id.</param>
		private void LoadCertificationsAndLicenses(long jobId)
		{
			try
			{
				var degreeCertifications = ServiceClientLocator.ExplorerClient(App).GetJobDegreeCertifications(ReportCriteria);
				DegreeCertifications = degreeCertifications;

				if (degreeCertifications != null && degreeCertifications.JobCertifications.Count > 0)
				{
					CertificationRepeater.DataSource =
							degreeCertifications.JobCertifications.OrderByDescending(
								x => x.DemandPercentile).Take(5).ToList();
					CertificationRepeater.DataBind();

					if (degreeCertifications.JobDegrees.Count > 0)
						LoadDegreeRequired(degreeCertifications.JobDegrees);
					else
					{
						panDegreeRequired.Visible = false;
						lblDegreeRequiredInfo.Visible = true;
						lblDegreeRequiredInfo.Text = "We're still collecting data. We will have more feedback later.";
					}
				}
				else
				{
					lblCertificationInfo.Visible = true;
					lblCertificationInfo.Text = "There are no common certifications or licenses for this type of job.";

					panDegreeRequired.Visible = false;
					lblDegreeRequiredInfo.Visible = true;
					lblDegreeRequiredInfo.Text = "We're still collecting data. We will have more feedback later.";
				}

			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Loads the degree required.
    /// </summary>
    /// <param name="jobDegrees">The job degrees.</param>
		private void LoadDegreeRequired(List<DegreeDto> jobDegrees)
		{
			DegreeRepeater.DataSource = jobDegrees;
			DegreeRepeater.DataBind();
		}

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void DegreeRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var degree = (DegreeDto)e.Item.DataItem;

				var degreeCertifications = DegreeCertifications;
				var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");
				degreeEducationLevelRepeater.DataSource = degreeCertifications.DegreeEducationLevels(degree.Id.Value);
				degreeEducationLevelRepeater.DataBind();
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeEducationLevelRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void DegreeEducationLevelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var degreeEducationLevel = (DegreeEducationLevelDto)e.Item.DataItem;

				var DegreeEducationLevel = (Label)e.Item.FindControl("lblDegreeEducationLevel");
				switch (degreeEducationLevel.EducationLevel)
				{
					case ExplorerEducationLevels.Certificate:
						DegreeEducationLevel.Text = CodeLocalise(degreeEducationLevel.EducationLevel.ToString(), "Certificate");
						break;

					case ExplorerEducationLevels.Associate:
						DegreeEducationLevel.Text = CodeLocalise(degreeEducationLevel.EducationLevel.ToString(), "Associate");
						break;

					case ExplorerEducationLevels.Bachelor:
						DegreeEducationLevel.Text = CodeLocalise(degreeEducationLevel.EducationLevel.ToString(), "Bachelor");
						break;

					case ExplorerEducationLevels.PostBachelorCertificate:
						DegreeEducationLevel.Text = CodeLocalise(degreeEducationLevel.EducationLevel.ToString(), "Post-Baccalaureate Certificate");
						break;

					case ExplorerEducationLevels.GraduateProfessional:
						DegreeEducationLevel.Text = CodeLocalise(degreeEducationLevel.EducationLevel.ToString(), "Graduate or Professional");
						break;

					case ExplorerEducationLevels.PostMastersCertificate:
						DegreeEducationLevel.Text = CodeLocalise(degreeEducationLevel.EducationLevel.ToString(), "Post-Master's Certificate");
						break;

					case ExplorerEducationLevels.Doctorate:
						DegreeEducationLevel.Text = CodeLocalise(degreeEducationLevel.EducationLevel.ToString(), "Doctorate");
						break;
				}
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the CertificationRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void CertificationRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			// Add number before item
			var rankLiteral = (Literal)e.Item.FindControl("CertificationRankLiteral");
			rankLiteral.Text = (e.Item.ItemIndex + 1).ToString();
		}

    /// <summary>
    /// Loads the experience level.
    /// </summary>
    /// <param name="jobId">The job id.</param>
		private void LoadExperienceLevel(long jobId)
		{
			try
			{
				ExperienceRequiredPlaceHolder.Visible = true;

				var experienceLevels = ServiceClientLocator.ExplorerClient(App).GetJobExperienceLevels(jobId);

				if (experienceLevels.Count > 0)
				{
					var experienceLevel = experienceLevels.SingleOrDefault(x => x.ExperienceLevel == ExperienceLevels.LessThanTwoYears);

					LessThanTwoLiteral.Text =
						String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
													experienceLevel == null ? "0" : Math.Round(experienceLevel.ExperiencePercentile, 0).ToString());

					experienceLevel = experienceLevels.SingleOrDefault(x => x.ExperienceLevel == ExperienceLevels.TwoToFiveYears);

					TwoToFiveLiteral.Text =
						String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
													experienceLevel == null ? "0" : Math.Round(experienceLevel.ExperiencePercentile, 0).ToString());

					experienceLevel = experienceLevels.SingleOrDefault(x => x.ExperienceLevel == ExperienceLevels.FiveToEightYears);

					FiveToEightLiteral.Text =
						String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
													experienceLevel == null ? "0" : Math.Round(experienceLevel.ExperiencePercentile, 0).ToString());

					experienceLevel = experienceLevels.SingleOrDefault(x => x.ExperienceLevel == ExperienceLevels.EightPlusYears);

					EightPlusLiteral.Text =
						String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
													experienceLevel == null ? "0" : Math.Round(experienceLevel.ExperiencePercentile, 0).ToString());
				}
				else
				{
					ExperienceRequiredPlaceHolder.Visible = false;
					lblExperienceLevelInfo.Visible = true;
					lblExperienceLevelInfo.Text = "We're still collecting data. We will have more feedback later.";
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Loads the career path.
    /// </summary>
    /// <param name="jobId">The job id.</param>
		private void LoadCareerPath(long jobId)
		{
			try
			{
				CareerPathDirection = CareerPathDirections.FromThisJob;
				CareerPaths = ServiceClientLocator.ExplorerClient(App).GetJobCareerPaths(jobId);
				BindCareerPathSecondLevel();
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Binds the career path second level.
    /// </summary>
		private void BindCareerPathSecondLevel()
		{
			var careerPaths = CareerPaths;

			var careerPathDirection = CareerPathDirection;

			if (careerPathDirection == CareerPathDirections.FromThisJob)
			{
				CareerPathFromLinkButton.Enabled = false;
				CareerPathToLinkButton.Enabled = true;
			}
			else
			{
				CareerPathFromLinkButton.Enabled = true;
				CareerPathToLinkButton.Enabled = false;
			}

			if (
				(careerPathDirection == CareerPathDirections.FromThisJob && careerPaths.CareerPathTo.Count == 0)
				||
				(careerPathDirection == CareerPathDirections.ToThisJob && careerPaths.CareerPathFrom.Count == 0))
			{
				//CareerPathFromLinkButton.Enabled = false;
				//CareerPathToLinkButton.Enabled = false;

				CareerPathPanel.Visible = false;
				NoCareerPathLabel.Visible = true;
				NoCareerPathLabel.Text = CodeLocalise("NoCareerPath.Text", "No career path information available");
			}
			else
			{
				CareerPathPanel.Visible = true;
				NoCareerPathLabel.Visible = false;

				var careerPath = careerPathDirection == CareerPathDirections.FromThisJob
													? careerPaths.CareerPathTo
													: careerPaths.CareerPathFrom;

				CareerPathSecondLevelLeftPanel.Visible =
					CareerPathSecondLevelMiddleLeftPanel.Visible =
					CareerPathSecondLevelMiddleRightPanel.Visible = CareerPathSecondLevelRightPanel.Visible = true;

				CareerPathTopLevelPanel.CssClass = careerPathDirection == CareerPathDirections.FromThisJob
																						? "careerpathChartItem top topArrowDown"
																						: "careerpathChartItem top topArrowUp";

				CareerPathSecondLevelLeftPanel.CssClass = "careerpathChartItem left";
				CareerPathSecondLevelMiddleLeftPanel.CssClass = "careerpathChartItem";
				CareerPathSecondLevelMiddleRightPanel.CssClass = "careerpathChartItem";
				CareerPathSecondLevelRightPanel.CssClass = "careerpathChartItem right";

				CareerPathSecondLevelMiddleLeftArrowLineLiteral.Text = "<span class=\"arrowLineMiddle\"></span>";

				CareerPathSecondLevelLeftLinkButton.Enabled =
					CareerPathSecondLevelMiddleLeftLinkButton.Enabled =
					CareerPathSecondLevelMiddleRightLinkButton.Enabled = CareerPathSecondLevelRightLinkButton.Enabled = false;

				CareerPathJobNameLabel.Text = ReportCriteria.CareerAreaJob;
				switch (careerPath.Count)
				{
					case 1:
						CareerPathSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems1 centre";

						CareerPathSecondLevelLeftPanel.Visible = false;

						CareerPathSecondLevelMiddleLeftArrowLineLiteral.Text = String.Empty;

						CareerPathSecondLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();

						CareerPathSecondLevelMiddleLeftLinkButton.Text = careerPath[0].Job.Name;
						CareerPathSecondLevelMiddleLeftLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
						CareerPathSecondLevelMiddleLeftLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelMiddleLeftLinkButton.Enabled = (careerPath[0].CareerPathTo != null &&
						//                                                     careerPath[0].CareerPathTo.Count > 0);

						CareerPathSecondLevelMiddleRightPanel.Visible = false;

						CareerPathSecondLevelRightPanel.Visible = false;

						break;

					case 2:
						CareerPathSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems2 centre";
            CareerPathSecondLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
            CareerPathSecondLevelLeftLinkButton.Text = careerPath[0].Job.Name;
						CareerPathSecondLevelLeftLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
						CareerPathSecondLevelLeftLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelLeftLinkButton.Enabled = (careerPath[0].CareerPathTo != null && careerPath[0].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleLeftPanel.Visible = false;
            CareerPathSecondLevelMiddleRightPanel.Visible = false;
            CareerPathSecondLevelRightJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
            CareerPathSecondLevelRightLinkButton.Text = careerPath[1].Job.Name;
						CareerPathSecondLevelRightLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
						CareerPathSecondLevelRightLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelRightLinkButton.Enabled = (careerPath[1].CareerPathTo != null && careerPath[1].CareerPathTo.Count > 0);
            break;

					case 3:
						CareerPathSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems3 centre";
            CareerPathSecondLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
            CareerPathSecondLevelLeftLinkButton.Text = careerPath[0].Job.Name;
						CareerPathSecondLevelLeftLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
						CareerPathSecondLevelLeftLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelLeftLinkButton.Enabled = (careerPath[0].CareerPathTo != null && careerPath[0].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
            CareerPathSecondLevelMiddleLeftLinkButton.Text = careerPath[1].Job.Name;
						CareerPathSecondLevelMiddleLeftLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
						CareerPathSecondLevelMiddleLeftLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelMiddleLeftLinkButton.Enabled = (careerPath[1].CareerPathTo != null && careerPath[1].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleRightPanel.Visible = false;
            CareerPathSecondLevelRightJobLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
            CareerPathSecondLevelRightLinkButton.Text = careerPath[2].Job.Name;
						CareerPathSecondLevelRightLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
						CareerPathSecondLevelRightLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelRightLinkButton.Enabled = (careerPath[2].CareerPathTo != null && careerPath[2].CareerPathTo.Count > 0);
            break;

					case 4:
						CareerPathSecondLevelPanel.CssClass = "careerPathRowWrap";
            CareerPathSecondLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
            CareerPathSecondLevelLeftLinkButton.Text = careerPath[0].Job.Name;
						CareerPathSecondLevelLeftLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
						CareerPathSecondLevelLeftLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelLeftLinkButton.Enabled = (careerPath[0].CareerPathTo != null && careerPath[0].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
            CareerPathSecondLevelMiddleLeftLinkButton.Text = careerPath[1].Job.Name;
						CareerPathSecondLevelMiddleLeftLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
						CareerPathSecondLevelMiddleLeftLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelMiddleLeftLinkButton.Enabled = (careerPath[1].CareerPathTo != null && careerPath[1].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleRightJobLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
            CareerPathSecondLevelMiddleRightLinkButton.Text = careerPath[2].Job.Name;
						CareerPathSecondLevelMiddleRightLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
						CareerPathSecondLevelMiddleRightLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelMiddleRightLinkButton.Enabled = (careerPath[2].CareerPathTo != null && careerPath[2].CareerPathTo.Count > 0);
            CareerPathSecondLevelRightJobLinkButton.CommandArgument = careerPath[3].Job.Id.ToString();
            CareerPathSecondLevelRightLinkButton.Text = careerPath[3].Job.Name;
						CareerPathSecondLevelRightLinkButton.CommandArgument = careerPath[3].Job.Id.ToString();
						CareerPathSecondLevelRightLinkButton.CommandName = "ViewCareerPath";
						//CareerPathSecondLevelRightLinkButton.Enabled = (careerPath[3].CareerPathTo != null &&  careerPath[3].CareerPathTo.Count > 0);
            break;
				}

				CareerPathThirdLevelLeftPanel.Visible = false;
				CareerPathThirdLevelMiddleLeftPanel.Visible = false;
				CareerPathThirdLevelMiddleRightPanel.Visible = false;
				CareerPathThirdLevelRightPanel.Visible = false;
			}
		}

    /// <summary>
    /// Handles the Command event of the CareerPathLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void CareerPathLinkButton_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "LoadCareerPaths")
			{
				OpenAccordion("CommonCareerPath");
				CareerPathDirection = (CareerPathDirections)Enum.Parse(typeof(CareerPathDirections), (string)e.CommandArgument);
				BindCareerPathSecondLevel();
			}
		}

    /// <summary>
    /// Handles the Command event of the SecondLevelLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void SecondLevelLinkButton_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "ViewCareerPath")
			{
				OpenAccordion("CommonCareerPath");

				long jobId;
				if (long.TryParse(e.CommandArgument.ToString(), out jobId))
				{
					CareerPathSecondLevelLeftPanel.CssClass = "careerpathChartItem left";
					CareerPathSecondLevelMiddleLeftPanel.CssClass = "careerpathChartItem";
					CareerPathSecondLevelMiddleRightPanel.CssClass = "careerpathChartItem";
					CareerPathSecondLevelRightPanel.CssClass = "careerpathChartItem right";

					var secondLevelLinkButton = (LinkButton)sender;
					secondLevelLinkButton.Enabled = false;

					if (secondLevelLinkButton.Parent is Panel)
					{
						var careerPathDirection = CareerPathDirection;

						var parentPanel = (Panel)secondLevelLinkButton.Parent;
						parentPanel.CssClass += careerPathDirection == CareerPathDirections.FromThisJob ? " highlight bottomArrowDown" : " highlight bottomArrowUp";
					}

					BindCareerPathThirdLevel(jobId, secondLevelLinkButton.ID);
				}
			}
		}

    /// <summary>
    /// Binds the career path third level.
    /// </summary>
    /// <param name="secondLevelJobId">The second level job id.</param>
    /// <param name="secondLevelJobLinkButtonId">The second level job link button id.</param>
		private void BindCareerPathThirdLevel(long secondLevelJobId, string secondLevelJobLinkButtonId)
		{
			var careerPaths = CareerPaths;

			var careerPathDirection = CareerPathDirection;

			var careerPathModel = careerPathDirection == CareerPathDirections.FromThisJob
															? careerPaths.CareerPathTo.SingleOrDefault(x => x.Job.Id == secondLevelJobId)
															: careerPaths.CareerPathFrom.SingleOrDefault(x => x.Job.Id == secondLevelJobId);

			var careerPath = careerPathDirection == CareerPathDirections.FromThisJob
												? careerPathModel.CareerPathTo
												: careerPathModel.CareerPathFrom;

			CareerPathThirdLevelLeftPanel.Visible =
				CareerPathThirdLevelMiddleLeftPanel.Visible =
				CareerPathThirdLevelMiddleRightPanel.Visible = CareerPathThirdLevelRightPanel.Visible = false;

			CareerPathThirdLevelMiddleLeftArrowLineLiteral.Text = "<span class=\"arrowLineMiddle\"></span>";

			SetCareerPathThirdLevelPanelCssClass(secondLevelJobLinkButtonId, careerPath.Count);

			switch (careerPath.Count)
			{
				case 1:
					CareerPathThirdLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
          CareerPathThirdLevelMiddleLeftArrowLineLiteral.Text = String.Empty;
          CareerPathThirdLevelMiddleLeftPanel.Visible = true;
					CareerPathThirdLevelMiddleLeftJobNameLabel.Text = careerPath[0].Job.Name;
          break;

				case 2:
					CareerPathThirdLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
          CareerPathThirdLevelLeftPanel.Visible = true;
					CareerPathThirdLevelLeftJobNameLabel.Text = careerPath[0].Job.Name;
          CareerPathThirdLevelRightJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
          CareerPathThirdLevelRightPanel.Visible = true;
					CareerPathThirdLevelRightJobNameLabel.Text = careerPath[1].Job.Name;
          break;

				case 3:
					CareerPathThirdLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
          CareerPathThirdLevelLeftPanel.Visible = true;
					CareerPathThirdLevelLeftJobNameLabel.Text = careerPath[0].Job.Name;
          CareerPathThirdLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
          CareerPathThirdLevelMiddleLeftPanel.Visible = true;
					CareerPathThirdLevelMiddleLeftJobNameLabel.Text = careerPath[1].Job.Name;
          CareerPathThirdLevelRightJobLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
          CareerPathThirdLevelRightPanel.Visible = true;
					CareerPathThirdLevelRightJobNameLabel.Text = careerPath[2].Job.Name;
          break;

				case 4:
					CareerPathThirdLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
          CareerPathThirdLevelLeftPanel.Visible = true;
					CareerPathThirdLevelLeftJobNameLabel.Text = careerPath[0].Job.Name;
          CareerPathThirdLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
          CareerPathThirdLevelMiddleLeftPanel.Visible = true;
					CareerPathThirdLevelMiddleLeftJobNameLabel.Text = careerPath[1].Job.Name;
          CareerPathThirdLevelMiddleRightJobLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
          CareerPathThirdLevelMiddleRightPanel.Visible = true;
					CareerPathThirdLevelMiddleRightJobNameLabel.Text = careerPath[2].Job.Name;
          CareerPathThirdLevelRightJobLinkButton.CommandArgument = careerPath[3].Job.Id.ToString();
          CareerPathThirdLevelRightPanel.Visible = true;
					CareerPathThirdLevelRightJobNameLabel.Text = careerPath[3].Job.Name;
          break;
			}
		}

    /// <summary>
    /// Sets the career path third level panel CSS class.
    /// </summary>
    /// <param name="secondLevelJobLinkButtonId">The second level job link button id.</param>
    /// <param name="thirdLevelCareerPathCount">The third level career path count.</param>
		private void SetCareerPathThirdLevelPanelCssClass(string secondLevelJobLinkButtonId, int thirdLevelCareerPathCount)
		{
			var careerPaths = CareerPaths;
			var careerPathDirection = CareerPathDirection;

			var secondLevelCareerPath = careerPathDirection == CareerPathDirections.FromThisJob
																		? careerPaths.CareerPathTo
																		: careerPaths.CareerPathFrom;

			var secondLevelCareerPathCount = secondLevelCareerPath.Count;

			var clickPosition = secondLevelJobLinkButtonId.Replace("CareerPathSecondLevel", String.Empty);
			clickPosition = clickPosition.Replace("LinkButton", String.Empty);

			var cssClass = "careerPathRowWrap";

			if (thirdLevelCareerPathCount < 4)
			{
				if (secondLevelCareerPathCount == 1)
				{
					cssClass = String.Format("careerPathRowWrap noOfItems{0} centre", thirdLevelCareerPathCount.ToString());
				}
				else
				{
					cssClass = String.Format("careerPathRowWrap second{0}{1}third{2}", secondLevelCareerPathCount, clickPosition,
																	 thirdLevelCareerPathCount);
				}
			}

			CareerPathThirdLevelPanel.CssClass = cssClass;
		}

    /// <summary>
    /// Handles the ItemClicked event of the ModalList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
		{
			try
			{
				switch (e.CommandName)
				{
					case "ViewJob":
						long itemId;
						if (long.TryParse(e.CommandArgument.ToString(), out itemId))
						{
							var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.Job, LinkId = (long)ReportCriteria.CareerAreaJobId, LinkText = ReportCriteria.CareerAreaJob, Criteria = ReportCriteria };
							var breadcrumbs = Breadcrumbs;
							breadcrumbs.Add(breadcrumb);

							ReportCriteria.CareerAreaJobId = itemId;
							ReportCriteria.CareerAreaJob = "";

							if (ReportCriteria.StateAreaId <= 0)
							{
								var state = new long?();

								if (App.UserData.HasProfile && App.UserData.Profile.PostalAddress.IsNotNull() && App.UserData.Profile.PostalAddress.StateId.IsNotNull())
									state = App.UserData.Profile.PostalAddress.StateId;

								ReportCriteria.StateAreaId = ServiceClientLocator.ExplorerClient(App).GetStates().Where(x => x.StateCode == state.ToString() && x.AreaSortOrder == 0).Select(x => x.Id).FirstOrDefault() ?? 505; // For all states StateAreaId is 505.
							}

							var jobReport = ServiceClientLocator.ExplorerClient(App).GetJobReport(ReportCriteria);
							JobReport = jobReport;
              ReportCriteria.CareerAreaJobId = jobReport.ReportData.JobId;
              ReportCriteria.CareerAreaJob = jobReport.ReportData.Name;
							Breadcrumbs = breadcrumbs;

							lblJobMatchInfo.Text = string.Format(InitialText, ReportCriteria.CareerAreaJob.Trim());

							LoadMatches(itemId);
							LoadTypicalResumes(itemId.ToString(), true);
						}
						break;
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Loads the typical resumes.
    /// </summary>
    /// <param name="lensPostingId">The lens posting id.</param>
    /// <param name="isExplorerJob">if set to <c>true</c> [is explorer job].</param>
		private void LoadTypicalResumes(string lensPostingId, bool isExplorerJob)
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					var typicalResumes = new List<ResumeInfo>();

					if (!isExplorerJob)
						typicalResumes = ServiceClientLocator.PostingClient(App).GetTypicalResumes(lensPostingId, null);
					else if (JobReport.IsNotNull())
					{
            var jSkills = ServiceClientLocator.ExplorerClient(App).GetJobSkills(JobReport.ReportData.JobId).Select(x => x.SkillName).ToList();

						var posting = new PostingEnvelope
						{
							PostingInfo = new PostingInfo
							{
								Title = JobReport.ReportData.Name,
                Description = JobReport.ReportData.DegreeIntroStatement,
								Skills = jSkills
							}
						};

						typicalResumes = ServiceClientLocator.PostingClient(App).GetTypicalResumes("", posting);
					}

					if (typicalResumes.IsNotNull() && typicalResumes.Count > 0)
					{
						TypicalResumesRepeater.DataSource = typicalResumes;
						TypicalResumesRepeater.DataBind();
					}
					else
					{
						panTypicalResumes.Visible = false;
						lblTypicalResumeInfo.Visible = true;
						lblTypicalResumeInfo.Text = "We're still collecting data. We will have more feedback later.";
					}
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the TypicalResumesRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void TypicalResumesRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			var rResume = (ResumeInfo)e.Item.DataItem;

			var currentJob = (Label)e.Item.FindControl("lblCurrentJob");
			currentJob.Text = rResume.CurrentJob.Trim().IsNotNullOrEmpty() ? rResume.CurrentJob.Trim() : "--";

			var previousJob = (Label)e.Item.FindControl("lblPreviousJob");
			previousJob.Text = rResume.PreviousJob.Trim().IsNotNullOrEmpty() ? rResume.PreviousJob.Trim() : "--";

			var yearOfExp = (Label)e.Item.FindControl("lblYearOfExp");
			yearOfExp.Text = rResume.YearsOfExperience.ToString();

			var degreeLevel = (Label)e.Item.FindControl("lblDegreeLevel");
			degreeLevel.Text = rResume.DegreeLevel.Trim().IsNotNullOrEmpty() ? rResume.DegreeLevel.Trim() : "--";

			var skills = (Label)e.Item.FindControl("lblSkills");
			if (rResume.Skills.Count > 0 && string.Join(",", rResume.Skills.ToArray()).Trim().IsNotNullOrEmpty())
				skills.Text = string.Join(",", rResume.Skills.ToArray());
			if (rResume.Certificates.Count > 0 && string.Join(",", rResume.Certificates.ToArray()).Trim().IsNotNullOrEmpty())
			{
				if (skills.Text.Trim().IsNotNullOrEmpty())
					skills.Text += ", " + string.Join(",", rResume.Certificates.ToArray());
				else
					skills.Text += string.Join(",", rResume.Certificates.ToArray());
			}

			if (!skills.Text.Trim().IsNotNullOrEmpty())
				skills.Text = "--";
		}

    /// <summary>
    /// Loads the skill gap.
    /// </summary>
    /// <param name="jobId">The job id.</param>
		private void LoadSkillGap(long jobId)
		{
			try
			{
				var model = App.GetSessionValue<ResumeModel>("Career:Resume");
				if (model.IsNull() || (model.ResumeMetaInfo.IsNotNull() && model.ResumeMetaInfo.IsDefault.HasValue && (bool)model.ResumeMetaInfo.IsDefault))
				{
					model = ServiceClientLocator.ResumeClient(App).GetDefaultResume();
					App.SetSessionValue("Career:Resume", model);
				}

				var skillGap = new List<string>();
				var jSkills = ServiceClientLocator.ExplorerClient(App).GetJobSkills(jobId).Select(x => x.SkillName).ToList();

				if (model.IsNotNull()
					&& model.ResumeContent.IsNotNull()
					&& model.ResumeContent.Skills.IsNotNull()
					&& model.ResumeContent.Skills.Skills.IsNotNull()
					&& model.ResumeContent.Skills.Skills.Count > 0)
				{
					foreach (var skill in jSkills)
					{
						if (!model.ResumeContent.Skills.Skills.Contains(skill, StringComparer.OrdinalIgnoreCase))
							skillGap.Add(skill);
					}
				}
				else
					skillGap = jSkills.ToList();

				if (skillGap.Count > 0)
				{
					SkillGapRepeater.DataSource = skillGap;
					SkillGapRepeater.DataBind();
				}
				else
				{
					panSkillTraining.Visible = false;
					lblSkillTrainingInfo.Visible = true;
					lblSkillTrainingInfo.Text = "We're still collecting data. We will have more feedback later.";
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the SkillGapRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void SkillGapRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			var gSkills = e.Item.DataItem.ToString();

			var skillGap = (Label)e.Item.FindControl("lblSkillGap");
			skillGap.Text = gSkills;
		}
	}
}