﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.Core;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.Views;
using Focus.Common.Extensions;
using Focus.Common;

using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekersAdministrator)]
	public partial class ManageJobSeekerLists : PageBase
	{
		private JobSeekerCriteria _findCriteria;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("Page.Title", "Manage #CANDIDATETYPE# Lists"); // Replaced plural: seekers

			if(!IsPostBack)
			{
				LocaliseUI();
			  ApplyTheme();
				BindListsDropDown();

				SsnSelector.MaxSize = !App.Settings.ShowAssistMultipleJSSearchButtons 
					? 1 
					: App.Settings.MaxSearchableIDs;

				//SsnSelector.ScrollLimit = App.Settings.AssistMultipleJSSearchCriteriaRowsToDisplay;
			}
		}

		#region Events

		/// <summary>
		/// Handles the Clicked event of the FindButton_ control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FindButton_Clicked(object sender, EventArgs e)
		{
			UnbindSearch();
			BindJobSeekersList();
		}

		/// <summary>
		/// Handles the Clicked event of the AddJobSeekersButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddJobSeekersButton_Clicked(object sender, EventArgs e)
		{
			var details = GetSelectedJobSeekerFromList(JobSeekersList);
			JobSeekerList.Show(details);
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the ListsDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ListsDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			BindListMembersList();
		}

		/// <summary>
		/// Handles the Clicked event of the DeleteListMembersButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DeleteListMembersButton_Clicked(object sender, EventArgs e)
		{
			var listId = ListsDropDown.SelectedValueToLong();
			if (listId == 0) return;

			ActionConfirmation.Show(CodeLocalise("DeleteListMembers.Title", "Delete members"),
															CodeLocalise("DeleteListMembers.Body", "Are you sure you want to delete the list members?"),
															CodeLocalise("Global.Cancel.Text", "Cancel"),
															okText: CodeLocalise("DeleteListMembers.Ok.Delete list", "Delete members"), okCommandName: "DeleteListMembers", okCommandArgument: listId.ToString(), height: 50);
		}

		/// <summary>
		/// Handles the Clicked event of the DeleteListButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DeleteListButton_Clicked(object sender, EventArgs e)
		{
			var listId = ListsDropDown.SelectedValueToLong();
			if (listId == 0) return;

			ActionConfirmation.Show(CodeLocalise("DeleteList.Title", "Delete list"),
														CodeLocalise("DeleteList.Body", "Are you sure you want to delete the list?"),
														CodeLocalise("Global.Cancel.Text", "Cancel"),
														okText: CodeLocalise("DeleteList.Ok.Delete list", "Delete list"), okCommandName: "DeleteList", okCommandArgument: listId.ToString(), height: 50);
		}

		/// <summary>
		/// Handles the OkCommand event of the JobSeekerList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobSeekerList_OkCommand(object sender, EventArgs e)
		{
			BindListsDropDown();
			BindListMembersList();
		}

		/// <summary>
		/// Handles the OkCommand event of the ActionConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ActionConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			var listId = GetListId(e.CommandArgument);

			switch (e.CommandName)
			{
				case "DeleteListMembers":
					DeleteListMembers(listId);
					break;
				case "DeleteList":
					DeleteList(listId);
					break;
			}
		}

		#endregion

		#region Bind / Unbind Methods

		/// <summary>
		/// Gets the employees.
		/// </summary>
		/// <returns></returns>
		private void BindJobSeekersList()
		{
			var jobSeekers = ServiceClientLocator.CandidateClient(App).SearchJobSeekers(_findCriteria);
			
			JobSeekersList.DataSource = jobSeekers;
			JobSeekersList.DataBind();

			// Set visibility of other controls
			JobSeekersTable.Visible = true;
      ResultCount.Text = jobSeekers.TotalCount > jobSeekers.Count
        ? CodeLocalise("ResultCountExtra.Text", "{0} found. (Only first {1} shown)", jobSeekers.TotalCount.ToString(CultureInfo.InvariantCulture), _findCriteria.PageSize.ToString(CultureInfo.InvariantCulture)) 
        : CodeLocalise("ResultCount.Text", "{0} found", jobSeekers.Count.ToString(CultureInfo.InvariantCulture));
      
      AddJobSeekersButton.Visible = (jobSeekers.Count > 0);
			PageInstructions.Visible = false;
		}

		/// <summary>
		/// Binds the list members list.
		/// </summary>
		private void BindListMembersList()
		{
			var listId = ListsDropDown.SelectedValueToLong();

			var listMembers = ServiceClientLocator.CandidateClient(App).GetJobSeekersForList(listId);
			ListMembersList.DataSource = listMembers;
			ListMembersList.DataBind();

			// Set visibility of other controls
			ListMembersTable.Visible = ListMembersList.Visible = DeleteListButton.Visible = (listId > 0);
			DeleteListMembersButton.Visible = (listMembers.Count > 0);
		}

		/// <summary>
		/// Binds the lists list.
		/// </summary>
		private void BindListsDropDown()
		{
			ListsDropDown.Items.Clear();

			ListsDropDown.DataSource = ServiceClientLocator.CandidateClient(App).GetJobSeekerLists(ListTypes.AssistJobSeeker);
			ListsDropDown.DataValueField = "Id";
			ListsDropDown.DataTextField = "Text";
			ListsDropDown.DataBind();

			ListsDropDown.Items.AddLocalisedTopDefault("ListsDropDown.TopDefault", "- select list -");
		}
		
		/// <summary>
		/// Unbinds the search.
		/// </summary>
		private void UnbindSearch()
		{
		  _findCriteria = new JobSeekerCriteria
		  {
		    PageSize = 200
		  };

			if (FirstNameTextBox.TextTrimmed().IsNotNullOrEmpty())
				_findCriteria.Firstname = FirstNameTextBox.TextTrimmed();

			if (LastNameTextBox.TextTrimmed().IsNotNullOrEmpty())
				_findCriteria.Lastname = LastNameTextBox.TextTrimmed();

      //var ssnList = SsnSelector.ItemList.IsNotNullOrEmpty() ? SsnSelector.ItemList.ToList().Select(ssn => ssn.Replace("-", "")).ToList() : null;
			var ssnList = SsnSelector.SelectedItems.IsNotNullOrEmpty()
				? SsnSelector.SelectedItems.Select(ssn => ssn.Value.Replace("-", "")).ToList()
				: null;

      if (ssnList.IsNotNullOrEmpty())
        _findCriteria.SsnList = ssnList;

			if (EmailAddressTextBox.TextTrimmed().IsNotNullOrEmpty())
				_findCriteria.EmailAddress = EmailAddressTextBox.TextTrimmed();

			if (PhoneNumberTextBox.TextTrimmed().IsNotNullOrEmpty())
				_findCriteria.PhoneNumber = PhoneNumberTextBox.TextTrimmed();
		}

		#endregion

		/// <summary>
		/// Gets the selected job seeker from list.
		/// </summary>
		/// <param name="list">The list.</param>
		/// <returns></returns>
		private static JobSeekerView[] GetSelectedJobSeekerFromList(ListView list)
		{
			var dataItems = (from item in list.Items
											 let radio = (CheckBox)item.FindControl("SelectorCheckBox")
											 where radio.Checked
											 select item).ToList();

			return dataItems.Select(dataItem => new JobSeekerView
																						{
                                              PersonId = Convert.ToInt64(list.DataKeys[dataItem.DataItemIndex]["PersonId"]),
																							FirstName = list.DataKeys[dataItem.DataItemIndex]["FirstName"].ToString(),
																							LastName = list.DataKeys[dataItem.DataItemIndex]["LastName"].ToString(),
																							EmailAddress = list.DataKeys[dataItem.DataItemIndex]["EmailAddress"].ToString()
																						}).ToArray();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FindButton.Text = CodeLocalise("Global.Find.Text.NoEdit", "Find");
			AddJobSeekersButton.Text = CodeLocalise("AddJobSeekersButton.Text", "Add #CANDIDATETYPE#:LOWER(s) to list"); // Replaced plural: seekers
      JobSeekersListValidator.ErrorMessage = CodeLocalise("JobSeekersListValidator.ErrorMessage", "At least one #CANDIDATETYPE#:LOWER must be selected");
			ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} found", "0");
			DeleteListMembersButton.Text = CodeLocalise("DeleteListMembersButton.Text", "Delete members");
			DeleteListButton.Text = CodeLocalise("DeleteListButton.Text", "Delete list");
			ListMembersListValidator.ErrorMessage = CodeLocalise("ListMembersListValidator.ErrorMessage", "At least one member must be selected");
			PageInstructions.Text = CodeLocalise("PageInstructions.Text", "Find list members at left. Results will appear here.");
		}

    /// <summary>
    /// Show/hide controls based on the theme
    /// </summary>
    private void ApplyTheme()
    {
      SSNPlaceHolder.Visible = App.Settings.Theme == FocusThemes.Workforce;
      NoSSNPlaceHolder.Visible = !SSNPlaceHolder.Visible;
    }

		/// <summary>
		/// Gets the list id.
		/// </summary>
		/// <param name="commandArgument">The command argument.</param>
		/// <returns></returns>
		private static long GetListId(object commandArgument)
		{
			long listId;
			long.TryParse(commandArgument.ToString(), out listId);

			return listId;
		}

		/// <summary>
		/// Deletes the list members.
		/// </summary>
		/// <param name="listId">The list id.</param>
		private void DeleteListMembers(long listId)
		{
			var listMembersToRemove = GetSelectedJobSeekerFromList(ListMembersList);

			ServiceClientLocator.CandidateClient(App).RemoveJobSeekersFromList(listId, listMembersToRemove);

			BindListMembersList();
			
			Confirmation.Show(CodeLocalise("DeleteListMembersConfirmation.Title", "List deleted"),
												CodeLocalise("DeleteListMembersConfirmation.Body", "The members have been deleted from the list."),
												CodeLocalise("CloseModal.Text", "OK"), height: 50);
		}

		/// <summary>
		/// Deletes the list.
		/// </summary>
		/// <param name="listId">The list id.</param>
		private void DeleteList(long listId)
		{
			ServiceClientLocator.CandidateClient(App).DeleteJobSeekerList(listId);
			
			BindListsDropDown();
			BindListMembersList();

			Confirmation.Show(CodeLocalise("DeleteListConfirmation.Title", "List deleted"),
												CodeLocalise("DeleteListConfirmation.Body", "The list has been deleted."),
												CodeLocalise("CloseModal.Text", "OK"), height: 50);
		}
	}
}