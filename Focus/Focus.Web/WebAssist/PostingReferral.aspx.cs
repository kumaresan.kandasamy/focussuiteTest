﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using Focus.Core.EmailTemplate;
using Focus.Web.Code;
using Framework.Core;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Common.Code;
using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistPostingApprover)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistPostingApprovalViewer)]
	public partial class PostingReferral : PageBase
	{
		private string[] RedWords
		{
			get { return GetViewStateValue<string[]>("PostingReferral:RedWords"); }
			set { SetViewStateValue("PostingReferral:RedWords", value); }
		}

		private string[] YellowWords
		{
			get { return GetViewStateValue<string[]>("PostingReferral:YellowWords"); }
			set { SetViewStateValue("PostingReferral:YellowWords", value); }
		}

		private int LockVersion
		{
			get { return GetViewStateValue<int>("PostingReferral:LockVersion"); }
			set { SetViewStateValue("PostingReferral:LockVersion", value); }
		}

		private JobDto _job
		{
			get { return GetViewStateValue<JobDto>("PostingReferral:Job"); }
			set { SetViewStateValue("PostingReferral:Job", value); }
		}

		private UserTypes UserType
		{
			get { return GetViewStateValue<UserTypes>("PostingReferral:UserType"); }
			set { SetViewStateValue("PostingReferral:UserType", value); }
		}

		private long _jobId;
		private bool _postingFilterApplied;
		private const string FilteredURL = "?filtered=true";

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Get the Posting Id
			if (Page.RouteData.Values.ContainsKey("id"))
				long.TryParse(Page.RouteData.Values["id"].ToString(), out _jobId);

            Page.Title = (_jobId==0)?CodeLocalise("Page.Title", "Approval Queues-Posting Referral"):CodeLocalise("Page.Title", "Approval Queues-Posting Referral -"+_jobId);

			if (_jobId == 0)
			{
				throw new Exception(FormatError(ErrorTypes.InvalidJob, "Invalid job id"));
			}

			if (Request.QueryString["filtered"] != null) _postingFilterApplied = Convert.ToBoolean(Request.QueryString["filtered"]);

			if (!IsPostBack)
			{
				LocaliseUI();
				ApplyBranding();

				_job = ServiceClientLocator.JobClient(App).GetJob(_jobId);
				UserType = _job.AwaitingApprovalActionedBy.IsNotNullOrZero()
										 ? ServiceClientLocator.AccountClient(App).GetUserType(_job.AwaitingApprovalActionedBy.Value)
					           : UserTypes.Talent;

				if (_job.IsNull())
				{
					EditJobButton.Visible =
						ApproveButton.Visible =
							DenyButton.Visible =
								PostingReferralHeaderLiteral.Visible =
									StatusCellPlaceHolder.Visible = false;

					JobWarningMessageLabel.Text = HtmlLocalise("JobWarningMessageLabel.NotExists.Text",
						"This job has been deleted and is no longer available");
					JobWarningMessageLabel.Visible = true;

					return;
				}

				if (_job.ApprovalStatus.IsIn(ApprovalStatuses.None, ApprovalStatuses.Approved))
				{
					Response.Redirect(UrlBuilder.PostingReferrals());
				}

				LockVersion = _job.LockVersion;

				if (_job.RedProfanityWords.IsNotNullOrEmpty())
					RedWords = _job.RedProfanityWords.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
						.Distinct()
						.OrderByDescending(s => s.Length)
						.ToArray();

				if (_job.YellowProfanityWords.IsNotNullOrEmpty())
					YellowWords = _job.YellowProfanityWords.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
						.Distinct()
						.OrderByDescending(s => s.Length)
						.ToArray();

				PostingNameLiteral.Text = CodeLocalise("PostingName.Text", "{0} (ID #{1})", _job.JobTitle, _job.Id);

				var postingHtml = GetPostingWithProfanitiesHighlighted(_job.PostingHtml, RedWords, YellowWords); 

				//Replace these tags or this will cause accessability errors on the page
				postingHtml = postingHtml.Replace("<html>", "");
				postingHtml = postingHtml.Replace("</html>", "");
				postingHtml = postingHtml.Replace("<body>", "");
				postingHtml = postingHtml.Replace("</body>", "");

				postingHtml = Utilities.InsertPostingFooter(postingHtml, _job.CriminalBackgroundExclusionRequired, _job.CreditCheckRequired);

				PostingLiteral.Text = postingHtml.AddAltTagWithValueForImg("Posting referral image");

				#region Check if job needs the minimum age reason approval, then display the message in 'Status' panel

				var approvalReasons = new List<string>();

				var approvalDefault = UserType == UserTypes.Talent
					                      ? App.Settings.ApprovalDefaultForCustomerCreatedJobs
					                      : App.Settings.ApprovalDefaultForStaffCreatedJobs;

				JobApprovalCheck[] approvalChecks;
				if (approvalDefault == JobApprovalOptions.SpecifiedApproval)
				{
					approvalChecks = UserType == UserTypes.Talent
						                 ? App.Settings.ApprovalChecksForCustomerCreatedJobs
														 : App.Settings.ApprovalChecksForStaffCreatedJobs;
				}
				else
				{
					approvalChecks = new JobApprovalCheck[0];
				}

				if (approvalChecks.Contains(JobApprovalCheck.SpecialRequirements))
				{
					var count = ServiceClientLocator.JobClient(App).GetJobSpecialRequirementsCount(_jobId);
					if (count > 0)
					{
						approvalReasons.Add(CodeLocalise("SpecialRequirements.Text", "This posting contains special requirements"));
					}
				}

				if (approvalDefault == JobApprovalOptions.ApproveAll)
				{
					approvalReasons.Add(CodeLocalise("ApproveAll.Text", "All job postings require staff approval"));
				}

				if (_job.CriminalBackgroundExclusionRequired.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CriminalBackground))
				{
					approvalReasons.Add(CodeLocalise("CriminalBackgroundExclusionRequired.Text", "This posting requires a criminal background check to be completed on applicants"));
				}

				if (_job.CreditCheckRequired.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CreditCheck))
				{
					approvalReasons.Add(CodeLocalise("CreditCheckRequired.Text", "This posting requires a credit background check to be completed on applicants"));
				}

				//Minimum age reason 4  = 'other'
				if (_job.MinimumAgeReasonValue.HasValue && _job.MinimumAgeReasonValue.Value == 4)
				{
					var minimumAgeReasonTypeLocalised = CodeLocalise("Posting.MinimumAgeReasonType.Fifth", "Other", _job.MinimumAgeReasonValue);

					var minimumAgeReason = (_job.MinimumAgeReason.IsNotNullOrEmpty())
						? string.Format(" ({0}) {1}", minimumAgeReasonTypeLocalised, _job.MinimumAgeReason)
						: string.Empty;

					approvalReasons.Add(CodeLocalise("MinimumAgeReasonApproval.Text", @"Minimum age reason requires approval.  Reason: {0}", minimumAgeReason));
				}

				#endregion

				#region Check if job has inappropriate words that need approval, then display the message in 'Status' panel

				var yellowWords = (_job.YellowProfanityWords.IsNotNullOrEmpty())
					? _job.YellowProfanityWords.Split(',')
					: new string[0];
				var redwords = (_job.RedProfanityWords.IsNotNullOrEmpty()) ? _job.RedProfanityWords.Split(',') : new string[0];

				var inappropriateWords = yellowWords.Concat(redwords).ToArray();

				switch (inappropriateWords.Length)
				{
					case 0:
						InappropriateWordsSummaryLiteral.Text = CodeLocalise("NoInappropriateWords.Text",
							@"There are no inappropriate words/phrases in this posting.");
						break;
					case 1:
						InappropriateWordsSummaryLiteral.Text = CodeLocalise("OneInappropriateWords.Text",
							@"We have flagged 1 potentially inappropriate word/phrase in this posting:");
						break;
					default:
						InappropriateWordsSummaryLiteral.Text = CodeLocalise("InappropriateWords.Text",
							@"We have flagged {0} potentially inappropriate words/phrases in this posting:", inappropriateWords.Length);
						break;
				}

				#endregion

				InappropriateWordsList.DataSource = inappropriateWords;
				InappropriateWordsList.DataBind();

				#region Check if job needs the home-base reason approval, then display the message in 'Status' panel

				if (_job.SuitableForHomeWorker.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.HomeBased))
                    approvalReasons.Add(CodeLocalise("HomeBasedApproval.Text", @"Home-based – Home-based/remote jobs require approval"));

				#endregion

				#region Check if job needs the no fixed location reason approval, then display the message in 'Status' panel

				if (App.Settings.NoFixedLocation && _job.JobLocationType == JobLocationTypes.NoFixedLocation && approvalChecks.Contains(JobApprovalCheck.HomeBased))
                    approvalReasons.Add(CodeLocalise("NoFixedLocation.Text", @"Remote job – Home-based/remote jobs require approval"));

				#endregion

				BindApprovalStatusSection(_job, approvalReasons, approvalChecks);

				if (approvalReasons.Any())
				{
					ApprovalConditionsDatalist.DataSource = approvalReasons;
					ApprovalConditionsDatalist.DataBind();
				}
				else
				{
					ApprovalConditionsPlaceHolder.Visible = false;
				}

				InitialiseApprovalButtons(_job);
			}

			SetReturnUrl();
		}

		private string GetEmail(long jobId)
		{
			var referralEmail =
				ServiceClientLocator.CoreClient(App)
					.GetReferralEmails(null, null, ApprovalStatuses.Rejected, null, jobId)
					.FirstOrDefault();
			return referralEmail.IsNotNull() ? referralEmail.EmailText : string.Empty;
		}

		/// <summary>
		/// Handles the Clicked event of the ApproveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ApproveButton_Clicked(object sender, EventArgs e)
		{
			ApprovePostingReferralModal.Show(_jobId, LockVersion, _postingFilterApplied);
		}

		/// <summary>
		/// Handles the Clicked event of the DenyButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DenyButton_Clicked(object sender, EventArgs e)
		{
			DenyPostingReferral.Show(_jobId, LockVersion, _postingFilterApplied);
		}

		/// <summary>
		/// Handles the Clicked event of the EditJobButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EditJobButton_Clicked(object sender, EventArgs e)
		{
			// Check the job hasn't changed in the mean time
			var job = ServiceClientLocator.JobClient(App).GetJob(_jobId);
			if (job.LockVersion != LockVersion)
			{
				ConfirmationModal.Show(CodeLocalise("UserUpdated.Title", "Already Updated"),
				                       CodeLocalise("UserUpdated.Body", "Another user has recently updated this record. Please exit the record and return to it to see the changes made."),
				                       CodeLocalise("CloseModal.Text", "OK"));
			}
			else
			{
				var wizardUrl = UrlBuilder.AssistJobWizard(_jobId, "approval", _postingFilterApplied ? FilteredURL : null);

				Response.Redirect(wizardUrl);
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EditJobButton.Text = CodeLocalise("EditLinkButton.Text", "Edit job");
			ApproveButton.Text = CodeLocalise("ApproveButton.Text", "Approve posting");
			DenyButton.Text = CodeLocalise("DenyButton.Text", "Deny posting");
			ReturnLink.Text = CodeLocalise("ReturnToList.LinkText", "Return to main page");

			PostingReferralHeaderLiteral.Text = HtmlLocalise("PostingReferral.Header", "Approve job posting:");
            ApprovalConditionsDatalist.Attributes.Add("role", "presentation");
            InappropriateWordsList.Attributes.Add("role", "presentation");
            SpecialConditionsList.Attributes.Add("role", "presentation");
		}

		/// <summary>
		/// Gets the posting with profanities highlighted.
		/// </summary>
		/// <param name="posting">The posting.</param>
		/// <param name="redProfanityWords">The red profanity words.</param>
		/// <param name="yellowProfanityWords">The yellow profanity words.</param>
		/// <returns></returns>
		private string GetPostingWithProfanitiesHighlighted(string posting, string[] redProfanityWords,
			string[] yellowProfanityWords)
		{
			var ignoreList = new List<string>();

			posting = Regex.Replace(posting, @"<span data-nocensorship=""true"">([^<]*)<\/span>", match =>
			{
				ignoreList.Add(match.Value);
				return string.Format(@"<span data-placeholder=""{0}""></span>", ignoreList.Count.ToString(CultureInfo.InvariantCulture));
			});

			if (redProfanityWords.IsNotNullOrEmpty())
			{
				//Highlight red words
				posting = CommonUtilities.WordReplace( posting, redProfanityWords, @"<span class=""redWord"">", "</span>" );
			}

			if (yellowProfanityWords.IsNotNullOrEmpty())
			{
				//Highlight yellow words
				posting = CommonUtilities.WordReplace( posting, yellowProfanityWords, @"<span class=""yellowWord"">", "</span>" );
			}

			var ignoreIndex = 1;
			ignoreList.ForEach(match =>
			{
				var placeHolder = string.Format(@"<span data-placeholder=""{0}""></span>", ignoreIndex.ToString(CultureInfo.InvariantCulture));
				posting = posting.Replace(placeHolder, match);
				ignoreIndex++;
			});

			return posting;
		}

		/// <summary>
		/// Displays the approval status.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="approvalReasons">Populate with any reason for approval</param>
		/// <param name="approvalChecks">What reasons to check</param>
		private void BindApprovalStatusSection(JobDto job, List<string> approvalReasons, JobApprovalCheck[] approvalChecks)
		{
			AccountApprovalStatusLabel.Text = CodeLocalise("NewRequest.Text", "New request");
		  CurrentApprovalStatus.Value = job.ApprovalStatus.ToString();
			if (job.ApprovalStatus == ApprovalStatuses.Rejected)
			{
        EmailHeaderText.Text = CodeLocalise("CopyOfEmailHeader.Rejected.Text", "Copy of email notification sent");
				AccountApprovalStatusLabel.Text = CodeLocalise("DeniedRequest.Text", "Denied");
			}
      else if (job.ApprovalStatus == ApprovalStatuses.Reconsider)
      {
        EmailHeaderText.Text = CodeLocalise("CopyOfEmailHeader.Rejected.Text", "Copy of email notification sent (including denial reason)");
        PostingReferralHeaderLiteral.Text = CodeLocalise("PostingReferralHeaderLiteral.Reconsider.Text", "Approve job posting for a reconsider posting:");
        AccountApprovalStatusLabel.Text = CodeLocalise("ReconsiderRequest.Text", "Reconsider");
        ReconsiderOnDetailsLiteral.Text = CodeLocalise("ReconsiderOnDetailsLiteral.Text", "Set to Reconsider on {0}", job.AwaitingApprovalOn);
      }

      if (job.ApprovalStatus.IsIn(ApprovalStatuses.Rejected, ApprovalStatuses.Reconsider) && job.ClosedBy.IsNotNull())
      {
        var user = ServiceClientLocator.AccountClient(App).GetUserDetails(job.ClosedBy.Value);
        AccountApprovalStatusDetails.Text = CodeLocalise("DeniedDetails.Text", "Request Denied by {0} {1} on {2}",
          user.PersonDetails.FirstName, user.PersonDetails.LastName, job.ClosedOn.ToString());

        var emailText = GetEmail(job.Id.Value);

        if (emailText.IsNullOrEmpty())
          EmailPlaceHolder.Visible = false;
        else
          HoldEmailTextBox.Text = emailText;
      }
      else
      {
        EmailPlaceHolder.Visible = false;
        AccountApprovalStatusDetailsPlaceHolder.Visible = false;
      }

      ReconsiderOnPlaceHolder.Visible = ReconsiderReasonPlaceholder.Visible = (job.ApprovalStatus == ApprovalStatuses.Reconsider);

			string approvalText;

			#region Check if job has special conditions that need approval, then display the message in 'Status' panel

			SpecialConditionsLiteral.Text = CodeLocalise("JobConditions.Text", @"This posting has the following job condition:");

			var specialConditionsStringBuilder = new StringBuilder();

			if (job.CourtOrderedAffirmativeAction.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CourtOrdered))
			{
				approvalText = CodeLocalise("CourtOrderedAffirmativeAction.Text", "Court-ordered affirmative action");
				specialConditionsStringBuilder.Append(approvalText);
				approvalReasons.Add(approvalText);
			}

			if (job.FederalContractor.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.FederalContractor))
			{
				if (specialConditionsStringBuilder.Length > 0)
					specialConditionsStringBuilder.Append(",");

				approvalText = CodeLocalise("FederalContractor.Text", "Federal contractor");
				specialConditionsStringBuilder.Append(approvalText);
				approvalReasons.Add(approvalText);
			}

			if (job.ForeignLabourCertificationH2A.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2A))
			{
				if (specialConditionsStringBuilder.Length > 0)
					specialConditionsStringBuilder.Append(",");

				approvalText = CodeLocalise("ForeignLabourCertificationH2A.Text", "Foreign labor certification H2A agriculture");
				specialConditionsStringBuilder.Append(approvalText);
				approvalReasons.Add(approvalText);
			}

			if (job.ForeignLabourCertificationH2B.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2B))
			{
				if (specialConditionsStringBuilder.Length > 0)
					specialConditionsStringBuilder.Append(",");

				approvalText = CodeLocalise("ForeignLabourCertificationH2B.Text", "Foreign labor certification H2B agriculture");
				specialConditionsStringBuilder.Append(approvalText);
				approvalReasons.Add(approvalText);
			}

			if (job.ForeignLabourCertificationOther.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborOther))
			{
				if (specialConditionsStringBuilder.Length > 0)
					specialConditionsStringBuilder.Append(",");

				approvalText = CodeLocalise("ForeignLabourCertificationOther.Text", "Foreign labor certification other");
				specialConditionsStringBuilder.Append(approvalText);
				approvalReasons.Add(approvalText);
			}

			if (job.IsCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionBased))
			{
				if (specialConditionsStringBuilder.Length > 0)
					specialConditionsStringBuilder.Append(",");

				approvalText = CodeLocalise("IsCommissionBased.Text", "This is a commission-based position");
				specialConditionsStringBuilder.Append(approvalText);
				approvalReasons.Add(approvalText);
			}

			if (job.IsSalaryAndCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionSalary))
			{
				if (specialConditionsStringBuilder.Length > 0)
					specialConditionsStringBuilder.Append(",");

				approvalText = CodeLocalise("IsSalaryAndCommissionBased.Text", "This is a salary + commission-based position");
				specialConditionsStringBuilder.Append(approvalText);
				approvalReasons.Add(approvalText);
			}

			var specialConditionsString = specialConditionsStringBuilder.ToString();

			var specialConditions = (specialConditionsString.IsNotNull()) ? specialConditionsString.Split(',') : new string[0];

			SpecialConditionsList.DataSource = specialConditions;
			SpecialConditionsList.DataBind();

			SpecialRequirementsContainer.Visible =
				SpecialConditionsLiteral.Visible = SpecialConditionsList.Visible = specialConditionsString.IsNotNullOrEmpty();

			#endregion
		}

		/// <summary>
		/// Initialises the approval buttons.
		/// </summary>
		/// <param name="job">The job.</param>
		private void InitialiseApprovalButtons(JobDto job)
		{
			// Get user's roles
			var buttonsVisible = true;
			var roles = ServiceClientLocator.AccountClient(App).GetUsersRoles(App.User.UserId);

			if (job.CourtOrderedAffirmativeAction.IsNotNull() && (bool) job.CourtOrderedAffirmativeAction &&
			    roles.All(r => r.Key != Constants.RoleKeys.AssistCourtOrderedAffirmativeActionApprover))
				buttonsVisible = false;

			if (job.FederalContractor.IsNotNull() && (bool) job.FederalContractor &&
			    roles.All(r => r.Key != Constants.RoleKeys.AssistFederalContractorApprover))
				buttonsVisible = false;

			if (job.ForeignLabourCertificationH2A.IsNotNull() && (bool) job.ForeignLabourCertificationH2A &&
			    roles.All(r => r.Key != Constants.RoleKeys.AssistForeignLabourH2AAgApprover))
				buttonsVisible = false;

			if (job.ForeignLabourCertificationH2B.IsNotNull() && (bool) job.ForeignLabourCertificationH2B &&
			    roles.All(r => r.Key != Constants.RoleKeys.AssistForeignLabourH2BNonAgApprover))
				buttonsVisible = false;

			if (job.ForeignLabourCertificationOther.IsNotNull() && (bool) job.ForeignLabourCertificationOther &&
			    roles.All(r => r.Key != Constants.RoleKeys.AssistForeignLabourOtherApprover))
				buttonsVisible = false;

			if (job.IsCommissionBased.IsNotNull() && (bool) job.IsCommissionBased &&
			    roles.All(r => r.Key != Constants.RoleKeys.AssistCommissionOnlyApprover))
				buttonsVisible = false;

			if (job.IsSalaryAndCommissionBased.IsNotNull() && (bool) job.IsSalaryAndCommissionBased &&
			    roles.All(r => r.Key != Constants.RoleKeys.AssistSalaryAndCommissionApprover))
				buttonsVisible = false;

			if (App.Settings.Theme == FocusThemes.Workforce && job.SuitableForHomeWorker.HasValue &&
			    (bool) job.SuitableForHomeWorker && roles.All(r => r.Key != Constants.RoleKeys.AssistHomeBasedApprover))
				buttonsVisible = false;

			ApproveButton.Visible = DenyButton.Visible = buttonsVisible;
			EditJobButton.Visible = buttonsVisible & App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);

      if (job.ClosingOn < DateTime.Now.Date)
        ApproveButton.Visible = false;

      if (job.ApprovalStatus == ApprovalStatuses.Rejected)
		  {
		    DenyButton.Visible = false;
		  }

      ApprovalButtonsCell.Visible = App.User.IsInRole(Constants.RoleKeys.AssistPostingApprover);
		}

		/// <summary>
		/// Applies the branding.
		/// </summary>
		private void ApplyBranding()
		{
			AccountRequestStatusHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			AccountRequestStatusCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			AccountRequestStatusCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			RequestInformationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			RequestInformationCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			RequestInformationCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		protected void lnkReturn_Click(object sender, EventArgs e)
		{
			var returnPage = UrlBuilder.PostingReferrals();

			if (_postingFilterApplied && !returnPage.Contains("filtered="))
			{
				Response.Redirect(returnPage + "?filtered=true");
			}
			else
			{
				Response.Redirect(returnPage);
			}
		}
	}
}
