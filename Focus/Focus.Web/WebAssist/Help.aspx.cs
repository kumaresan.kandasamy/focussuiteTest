﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;

#endregion

namespace Focus.Web.WebAssist
{
	public partial class Help : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{			
			if (!IsPostBack)
			{
				var helpType = HelpTypes.Help;
				Page.Title = CodeLocalise("Page.Title.Help", "Help");
				var url = Request.Url.PathAndQuery;

				if (url.Contains("/privacy"))
				{
					helpType = HelpTypes.PrivacyAndSecurity;
					Page.Title = CodeLocalise("Page.Title.Privacy", "Privacy and security");
				}
				else if (url.Contains("/faqs"))
				{
					helpType = HelpTypes.FrequentlyAskedQuestions;
					Page.Title = CodeLocalise("Page.Title.FAQs", "FAQs");
				}
				else if (url.Contains("/terms"))
				{
					helpType = HelpTypes.TermsOfUse;
					Page.Title = CodeLocalise("Page.Title.TermsOfUse", "Terms of use");
				}

				HelpTitle.Text = CodeLocalise("Global.HelpTitle-" + helpType + ".Text", "Help - " + helpType);
				HelpContent.Text = CodeLocalise("Global.HelpContent-" + helpType + ".Text", "** ASSIST **" + helpType);
			}
		}		
	}
}