﻿#region Copyright © 2000 - 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion


#region Using Directives
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common.Helpers;
using Focus.Core;
using Focus.Web.Code;
using Focus.Web.Core;
using Focus.Web.Code.Controls.User;


#endregion



namespace Focus.Web.WebAssist
{
    public partial class ManageAcitivityService : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = CodeLocalise("Page.Title", "Manage Activities / Services");
            if (!Page.IsPostBack)
            {
                BindActivityType();
                Localise();
            }
        }
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptInclude(this, GetType(), "Knockout", ResolveUrl("~/Assets/Scripts/knockout-3.1.0.js"));
            ScriptManager.RegisterClientScriptInclude(this, GetType(), "KnockoutDirtyFlag", ResolveUrl("~/Assets/Scripts/knockout.dirtyflag.js"));
            ScriptManager.RegisterClientScriptInclude(this, GetType(), "ManageActivityServiceInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.AssistManageActivitiesServices.js"));
            ScriptManager.RegisterStartupScript
              (this,
              GetType(),
                      this.ID + ":ManageActivityServiceInit", string.Format(@"focusSuite.assistmanageactivityservice.BindAssistManageActivityServiceViewModel('{0}','{1}','{2}','{3}');", ManageActivityList.ClientID, UrlBuilder.GetAllActivities(), UrlBuilder.SaveActivities(), "OK"), true);
           
        }

        #region
        private void BindActivityType()
        {
            ManageTypeDropdown.Items.Clear();
            if(App.Settings.EnableHiringManagerActivitiesServices)
                ManageTypeDropdown.Items.AddEnumWithIntValue(ActivityType.Employer, "Hiring Manager");
            if(App.Settings.EnableJobSeekerActivitiesServices)
                ManageTypeDropdown.Items.AddEnumWithIntValue(ActivityType.JobSeeker, "Job seeker");
            ManageTypeDropdown.SelectedIndex = 0;
                    
        
        }
        private void Localise()
        {
            

            ManageCategorySortAsc.ImageUrl = UrlBuilder.CategorySortAscImage();
            ManageCategorySortDesc.ImageUrl = UrlBuilder.CategorySortDescImage();
            ManageActivitySortAsc.ImageUrl = UrlBuilder.ActivitySortAscImage();
            ManageActivitySortDesc.ImageUrl = UrlBuilder.CategorySortDescImage();

                   


        }
        #endregion

        
    }
}