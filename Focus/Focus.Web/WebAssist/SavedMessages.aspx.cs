﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core;
using Focus.Common.Extensions;
using Focus.Common;

using Framework.Core;

using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
	public partial class SavedMessages : PageBase
	{
		private List<SavedMessageTextDto> _savedMessageTexts;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("Page.Title", "Manage #FOCUSASSIST# Messages");

			if (!IsPostBack)
			{
				LocaliseUI();
				BindMessageAudienceRadioButtonList();
				MessageAudienceRadioButtonList.SelectedIndex = 0;
				BindSavedMessagesDropDown(MessageAudiences.JobSeeker);
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Clicked(object sender, EventArgs e)
		{
			var savedMessageId = SavedMessagesDropDown.SelectedValueToLong();

			if (savedMessageId == 0) throw new Exception("Message Id required");

      var messageTexts =  new List<SavedMessageTextDto>();
      messageTexts.Add(new SavedMessageTextDto
                        {
                          SavedMessageId = savedMessageId,
                          LocalisationId = 0,
                          Text = MessageTextbox.TextTrimmed()
                        });

			var audience = MessageAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();
			
			var message = new SavedMessageDto
											{
												Id = savedMessageId,
												Audience = audience,
												Name = SavedMessagesDropDown.Items[SavedMessagesDropDown.SelectedIndex].Text,
												UserId = App.User.UserId
											};

			ServiceClientLocator.CoreClient(App).SaveMessageTexts(message, messageTexts);

			ConfirmationModal.Show(CodeLocalise("MessageTextsSavedConfirmation.Title", "Message texts saved"),
															 CodeLocalise("MessageTextsSavedConfirmation.Body", "The message texts have been saved."),
															 CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Handles the Clicked event of the DeleteButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DeleteButton_Clicked(object sender, EventArgs e)
		{
			var savedMessageId = SavedMessagesDropDown.SelectedValueToLong();

			if (savedMessageId == 0) throw new Exception("Message Id required");

			DeleteConfirmationModal.Show(CodeLocalise("DeleteConfirmation.Title", "Delete Saved Message"),
														CodeLocalise("DeleteConfirmation.Body", "Are you sure you want to delete {0}?", SavedMessagesDropDown.Items[SavedMessagesDropDown.SelectedIndex].Text),
														CodeLocalise("Global.Cancel.Text", "Cancel"),
														okText: CodeLocalise("DeleteConfirmation.Ok.Text", "Delete saved message"), okCommandName: "Delete", okCommandArgument: savedMessageId.ToString());
		}

		/// <summary>
		/// Handles the OkCommand event of the DeleteConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void DeleteConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			var messageId = GetSavedMessageId(e.CommandArgument);
			var audience = MessageAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();

			ServiceClientLocator.CoreClient(App).DeleteSavedMessage(messageId);

			// Clear controls and rebind the saved messages drop down
			_savedMessageTexts = null;
			MessageTextbox.Text = "";
			BindSavedMessagesDropDown(audience);
			SavedMessagesDropDown.SelectedIndex = 0;
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the SavedMessagesDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SavedMessagesDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			_savedMessageTexts = (SavedMessagesDropDown.SelectedValueToLong() > 0) ? ServiceClientLocator.CoreClient(App).GetSavedMessageTexts(SavedMessagesDropDown.SelectedValueToLong()) : null;

			if (_savedMessageTexts.IsNotNullOrEmpty())
			{
				var defaultLocalisation = ServiceClientLocator.CoreClient(App).GetDefaultCulture();

				MessageTextbox.Text = _savedMessageTexts.Where(x => x.LocalisationId == defaultLocalisation.Id).FirstOrDefault().Text;
			}
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the MessageAudienceRadioButtonList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void MessageAudienceRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
		{
			var audience = MessageAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();
			BindSavedMessagesDropDown(audience);
			_savedMessageTexts = null;
			MessageTextbox.Text = "";
		}

		/// <summary>
		/// Binds the saved messages drop down.
		/// </summary>
		/// <param name="audience">The audience.</param>
		private void BindSavedMessagesDropDown(MessageAudiences audience)
		{
			SavedMessagesDropDown.DataSource = ServiceClientLocator.CoreClient(App).GetSavedMessages(audience);
			SavedMessagesDropDown.DataValueField = "Id";
			SavedMessagesDropDown.DataTextField = "Name";
			SavedMessagesDropDown.DataBind();
			SavedMessagesDropDown.Items.AddLocalisedTopDefault("SavedMessagesDropDownList.TopDefault", "- select saved message -");
		}

		/// <summary>
		/// Binds the message audience radio button list.
		/// </summary>
		private void BindMessageAudienceRadioButtonList()
		{
			MessageAudienceRadioButtonList.Items.Clear();

			MessageAudienceRadioButtonList.Items.AddEnum(MessageAudiences.JobSeeker, "job seeker");

			if (App.Settings.TalentModulePresent)
			{
				MessageAudienceRadioButtonList.Items.AddEnum(MessageAudiences.Employer, "#BUSINESS#:LOWER");
			}

			MessageAudienceRadioButtonList.SelectedIndex = 0;
		}

		/// <summary>
		/// Gets the saved message id.
		/// </summary>
		/// <param name="commandArgument">The command argument.</param>
		/// <returns></returns>
		private static long GetSavedMessageId(object commandArgument)
		{
			long messageId;
			long.TryParse(commandArgument.ToString(), out messageId);

			return messageId;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SaveButton.Text = CodeLocalise("Global.Save.Label", "Save");
			MessageRequired.ErrorMessage = CodeLocalise("MessageRequired.ErrorMessage", "Message text is required");
			SavedMessageRequired.ErrorMessage = CodeLocalise("MessageRequired.ErrorMessage", "Select a saved message to update");
			DeleteButton.Text = CodeLocalise("Global.Delete.Label", "Delete");
			SavedMessageRequiredDelete.ErrorMessage = CodeLocalise("MessageRequiredDelete.ErrorMessage", "Select a saved message to delete");
		}
	}
}