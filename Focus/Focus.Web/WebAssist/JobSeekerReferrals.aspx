﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JobSeekerReferrals.aspx.cs" Inherits="Focus.Web.WebAssist.JobSeekerReferrals" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/ApprovalQueuesNavigation.ascx" tagname="ApprovalQueuesNavigation" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register TagPrefix="uc" TagName="ApprovalQueueFilter" Src="~/WebAssist/Controls/ApprovalQueueFilter.ascx" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

	<uc:ApprovalQueuesNavigation ID="ApprovalQueuesNavigationMain" runat="server" />
	
	<h1><%= HtmlLocalise("JobSeekerReferrals.Header", "Approve job seeker referral requests")%></h1>
	
	<table role="presentation" style="width:100%;" id="FilterTable">
    <tr>
			<td style="width:50px;">
				<%= HtmlLabel(StatusDropDown,"Status.Text", "Status")%>&nbsp;
      </td>
      <td style="width:150px">
         <asp:DropDownList runat="server" ID="StatusDropDown" />
			</td>
			<td><uc:ApprovalQueueFilter ID="ApprovalQueueFilter" runat="server"/></td>
			<td style="text-align: right">
        <asp:Button ID="FilterButton" runat="server" class="button3" OnClick="FilterButton_Clicked" />
      </td>
		</tr>
	</table>
	<table role="presentation" id="ResultCountSection" style="width:100%;" >
		<tr>
			<td><asp:literal ID="ResultCount" runat="server" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:right; white-space: nowrap;"><uc:Pager ID="ReferralListPager" runat="server" PagedControlID="ReferralList" PageSize="25"	/></td>
		</tr>
	</table>
	<asp:ListView ID="ReferralList" runat="server" ItemPlaceholderID="ReferralPlaceHolder" DataSourceID="ReferralDataSource" OnItemDataBound="ReferralList_OnItemDataBound" OnSorting="ReferralList_Sorting" OnLayoutCreated="ReferralList_LayoutCreated">
		<LayoutTemplate>
				<table style="width:100%;" class="table" id="ReferralListTable">
				<thead>
					<tr>
						<th style="width:10%;white-space:nowrap" data-cell="star match">
							<asp:Literal runat="server" ID="StarMatch"></asp:Literal>
							<div class="orderingcontrols">
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="StarMatchSortAscButton" runat="server" CommandName="Sort" CommandArgument="applicationscore asc" CssClass="AscButton" AlternateText="Sort Ascending" />
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>"  ID="StarMatchSortDescButton" runat="server" CommandName="Sort" CommandArgument="applicationscore desc" CssClass="DescButton" AlternateText="Sort Descending" />
							</div>
						</th>
						<th style="width:20%;white-space:nowrap" data-cell="job title/Company">
							<asp:Literal runat="server" ID="JobTitleCompanyHeader"></asp:Literal>
							<div class="orderingcontrols">
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="JobTitleSortAscButton" runat="server" CommandName="Sort" CommandArgument="jobtitle asc, employername asc" CssClass="AscButton" AlternateText="Sort Ascending" />
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>"  ID="JobTitleSortDescButton" runat="server" CommandName="Sort" CommandArgument="jobtitle desc, employername desc" CssClass="DescButton" AlternateText="Sort Descending" />
							</div>
						</th>
						<th style="width:18%;white-space:nowrap" data-cell="posting location">
							<asp:Literal runat="server" ID="PostingLocationHeader"></asp:Literal>
							<div class="orderingcontrols">
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="PostingLocationSortAscButton" runat="server" CommandName="Sort" CommandArgument="joblocationtown asc, joblocationstate asc" CssClass="AscButton" AlternateText="Sort Ascending" />
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>"  ID="PostingLocationSortDescButton" runat="server" CommandName="Sort" CommandArgument="joblocationtown desc, joblocationstate desc" CssClass="DescButton" AlternateText="Sort Descending" />
							</div>
						</th>
						<th style="width:18%;white-space:nowrap" data-cell="job seeker name">
							<asp:Literal runat="server" ID="JobSeekerNameHeader"></asp:Literal>
							<div class="orderingcontrols">
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="JobSeekerNameSortAscButton" runat="server" CommandName="Sort" CommandArgument="name asc" CssClass="AscButton" AlternateText="Sort Ascending" />
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>"  ID="JobSeekerNameSortDescButton" runat="server" CommandName="Sort" CommandArgument="name desc" CssClass="DescButton" AlternateText="Sort Descending" />
							</div>
						</th>
            <th style="width:14%;white-space:nowrap" data-cell="job seeker location">
							<asp:Literal runat="server" ID="JobSeekerLocationHeader"></asp:Literal>
							<div class="orderingcontrols">
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="JobSeekerLocationSortAscButton" runat="server" CommandName="Sort" CommandArgument="jobseekertown asc, jobseekerstate asc" CssClass="AscButton" AlternateText="Sort Ascending" />
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>"  ID="JobSeekerLocationSortDescButton" runat="server" CommandName="Sort" CommandArgument="jobseekertown desc, jobseekerstate desc" CssClass="DescButton" AlternateText="Sort Descending" />
							</div>
						</th>          
						<th style="width:10%;white-space:nowrap" data-cell="request date">
							<asp:Literal runat="server" ID="RequestDateHeader"></asp:Literal>
							<div class="orderingcontrols">
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="RequestDateSortAscButton" runat="server" CommandName="Sort" CommandArgument="referraldate asc" CssClass="AscButton" AlternateText="Sort Ascending" />
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>"  ID="RequestDateSortDescButton" runat="server" CommandName="Sort" CommandArgument="referraldate desc" CssClass="DescButton" AlternateText="Sort Descending" />
							</div>
						</th>
						<th style="width:10%;white-space:nowrap" data-cell="time in queue">
							<asp:Literal runat="server" ID="TimeInQueueHeader"></asp:Literal>
							<div class="orderingcontrols">
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="TimeInQueueSortAscButton" runat="server" CommandName="Sort" CommandArgument="timeinqueue asc" CssClass="AscButton" AlternateText="Sort Ascending" />
								<asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>"  ID="TimeInQueueSortDescButton" runat="server" CommandName="Sort" CommandArgument="timeinqueue desc" CssClass="DescButton" AlternateText="Sort Descending" />
							</div>
						</th>
					</tr>
				</thead>
				<tbody>
					<asp:PlaceHolder ID="ReferralPlaceHolder" runat="server" />
				</tbody>	
			</table>
			<%--<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBody" id="ReferralListTable">
				<asp:PlaceHolder ID="ReferralPlaceHolder" runat="server" />
			</table>--%>
		</LayoutTemplate>
		<ItemTemplate>
			<tr style="vertical-align:top;">
        <td style="width:10%;" data-cell="star match"><asp:image ID="SearchResultScoreImage" runat="server" AlternateText="Search result score" Width="80" Height="16"/></td>
				<td style="width:26%;" data-cell="job title/Company">
				  <a href="<%# UrlBuilder.JobSeekerReferral(((JobSeekerReferralAllStatusViewDto)Container.DataItem).Id)%>?filtered=true"><%# ((JobSeekerReferralAllStatusViewDto)Container.DataItem).JobTitle%>, <%# ((JobSeekerReferralAllStatusViewDto)Container.DataItem).EmployerName%></a><asp:image ID="ForeignLabourCertificationImage" runat="server" Visible="false" CssClass="jobSeekerReferralForeignLabour" AlternateText="Foreign Labour Certification Image"/>
        </td>
        <td style="width:16%;" data-cell="posting location"><asp:Label runat="server" ID="PostingLocationLabel"></asp:Label></td>
				<td style="width:24%;" data-cell="job seeker name"><%# ((JobSeekerReferralAllStatusViewDto)Container.DataItem).Name%><asp:image ID="VeteranImage" runat="server" Visible="false" CssClass="jobSeekerReferralVeteran" Height="15" Width="17" AlternateText="Veteran"/></td>        
				<td style="width:16%;" data-cell="Job seeker location"><asp:Label runat="server" ID="JobseekerLocationLabel"></asp:Label></td>
        <td style="width:12%;" data-cell="request date"><%# ((JobSeekerReferralAllStatusViewDto)Container.DataItem).ReferralDate.ToString("MMM d, yyyy")%></td>
				<td style="width:12%;" data-cell="time in queue"><%# ((JobSeekerReferralAllStatusViewDto)Container.DataItem).TimeInQueue%> <%= HtmlLocalise("Days.Label", "day(s)")%></td>
			</tr>	
		</ItemTemplate>
	</asp:ListView>

	<asp:ObjectDataSource ID="ReferralDataSource" runat="server" TypeName="Focus.Web.WebAssist.JobSeekerReferrals" EnablePaging="true" 
										SelectMethod="GetReferrals" OnSelecting="ReferralList_Selecting" SelectCountMethod="GetReferralsCount" SortParameterName="orderBy">
	</asp:ObjectDataSource>
</asp:Content>
