﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CaseManagementErrors.aspx.cs" Inherits="Focus.Web.WebAssist.CaseManagementErrors" %>

<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="ManageAssistNavigation" Src="~/WebAssist/Controls/ManageAssistNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx"  %>
<%@ Register TagPrefix="uc" TagName="ViewCaseManagementError" Src="~/WebAssist/Controls/ViewCaseManagementError.ascx"  %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%# ResolveUrl("~/Assets/Scripts/jquery-ui-1.9.2.custom.new.min.js") %>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<%# ResolveUrl("~/Assets/Css/jquery-ui-1.9.2.custom.css") %>" />
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />
  <uc:ConfirmationModal ID="Confirmation" runat="server" />
  
  <h1><%= HtmlLocalise("Header.Text", "Case Management Error Administration")%></h1>
 <br />
  <table style="border-collapse: collapse;" role="presentation">
    <tr>
      <td>
        <%= HtmlLabel(ActionTypeDropDownList,"ActionType.Label", "Action Type")%>
        &nbsp;
      </td>
      <td>
        <asp:DropDownList ID="ActionTypeDropDownList" runat="server" CausesValidation="false" />
      </td>
      <td>
        &nbsp;
        <%= HtmlLocalise("Date.Label", "Date Submitted From")%>
        &nbsp;
      </td>
      <td>
        <asp:TextBox runat="server" ID="DateFromTextBox" title="Date From TextBox" MaxLength="100"></asp:TextBox>
        <%= HtmlLocalise("To.Label", "To")%>
        <asp:TextBox runat="server"  title="Date To TextBox" ID="DateToTextBox" MaxLength="100"></asp:TextBox>
      </td>
      <td>
        <asp:Button ID="FilterButton" runat="server" class="button3" OnClick="FilterButton_Clicked" Text="Go" />
      </td>
    </tr>
    <tr>
           <td>
        <%= HtmlLabel(MessageTypeDropDownList,"Message.Label", "Message Type")%>
        &nbsp;
      </td>
      <td>
        <asp:DropDownList ID="MessageTypeDropDownList" runat="server" CausesValidation="false" />
        &nbsp;
      </td>
       <td style="height: 30px">
         &nbsp;
        <%= HtmlLocalise("ErrorDescription.Label", "Error Description")%>
        &nbsp;
      </td>
      <td>
        <asp:TextBox runat="server" ID="ErrorDescriptionTextbox" MaxLength="300" Title="Error Description"></asp:TextBox>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
    </tr>
    <act:CalendarExtender ID="DateFromCalendarExtender" runat="server" TargetControlID="DateFromTextBox" CssClass="cal_Theme1" />
    <act:CalendarExtender ID="DateToCalendarExtender" runat="server" TargetControlID="DateToTextBox" CssClass="cal_Theme1"/>
  </table>
  <div style="width:100%">
	  <div style="float:left">
	    <strong><asp:literal ID="ResultCount" runat="server" /></strong>
    </div>
    <div style="float:right">
		  <uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="ErrorList" PageSize="10" OnDataPaged="SearchResultListPager_OnDataPaged" />
    </div>
  </div>

  <asp:ListView ID="ErrorList" runat="server" DataKeyNames="Id,DateSubmitted,MessageType,ErrorDescription" DataSourceID="ErrorsDataSource" ItemPlaceholderID="SearchResultPlaceHolder" OnItemDataBound="ErrorList_ItemDataBound" OnSorting="ErrorList_Sorting" OnLayoutCreated="ErrorList_LayoutCreated">
		<LayoutTemplate>	
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table" id="ErrorTable">
				<thead>
				<tr>
				  <th>
				    <table role="presentation">
				      <tr>
				        <td rowspan="3" height="16px" style="white-space: nowrap"><asp:Literal runat="server" ID="IdHeader"></asp:Literal></td>
				      </tr>
				    </table>
				  </th>
					<th>
						<table role ="presentation">
							<tr>
								<td rowspan="2" height="16px" style="white-space:nowrap"><asp:Literal runat="server" ID="DateHeader" /></td>
								<td height="8px" style="white-space:nowrap">
									<asp:ImageButton ID="DateAscSortButton" runat="server" CommandName="Sort" CommandArgument="DateSubmitted Asc" CssClass="AscButton" AlternateText="Sort Ascending"/>
                  <asp:ImageButton ID="DateDescSortButton" runat="server" CommandName="Sort" CommandArgument="DateSubmitted Desc" CssClass="DescButton" AlternateText="Sort Descending"/>
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role ="presentation">
							<tr>
								<td rowspan="2" height="16px" style="white-space:nowrap"><asp:Literal runat="server" ID="MessageTypeHeader" /></td>
								<td height="8px" style="white-space:nowrap">
									<asp:ImageButton ID="MessageTypeAscSortButton" runat="server" CommandName="Sort" CommandArgument="MessageType Asc" CssClass="AscButton"  AlternateText="Sort Ascending"/>
                  <asp:ImageButton ID="MessageTypeDescSortButton" runat="server" CommandName="Sort" CommandArgument="MessageType Desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role ="presentation">
							<tr>
								<td rowspan="2" height="16px" style="white-space:nowrap"><asp:Literal runat="server" ID="DescriptionHeader" /></td>
								<td height="8px" style="white-space:nowrap">
									<asp:ImageButton ID="DescriptionAscSortButton" runat="server" CommandName="Sort" CommandArgument="ErrorDescription Asc" CssClass="AscButton"  AlternateText="Sort Ascending"/>
                  <asp:ImageButton ID="DescriptionDescSortButton" runat="server" CommandName="Sort" CommandArgument="ErrorDescription Desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2" height="16px" style="white-space:nowrap"><asp:Literal runat="server" ID="ActionsHeader" /></td>
								<td height="8px">&nbsp;</td>
							</tr>
						</table>
					</th>
				</tr>
				</thead>
				<tbody>
				<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
				</tbody>
			</table>	
		</LayoutTemplate>
		<ItemTemplate>
			<tr>
			  <td><asp:Label runat="server" ID="IdLabel"></asp:Label></td>
				<td>
          <asp:Label runat="server" ID="DateLabel"/>
				</td>
				<td><asp:Label ID="MessageTypeLabel" runat="server" /></td>			
				<td><asp:Label ID="DescriptionLabel" runat="server" /></td>	
        <td>
          <table role="presentation">
						<tr>
							<td><asp:Button runat="server" ID="RequestDetailsButton" CssClass="button2" OnCommand="RequestDetailsButton_OnCommand" /></td>
							<td><asp:Button runat="server" ID="ItemButton" CssClass="button2" OnCommand="ItemButton_OnCommand"  /></td>
						</tr>
            <tr>
              <td><asp:Button runat="server" ID="ResendButton" CssClass="button2" OnCommand="ResendButton_OnCommand" /></td>
              <td><asp:Button runat="server" ID="IgnoreButton" CssClass="button2" OnCommand="IgnoreButton_OnCommand"  /></td>
            </tr>
          </table>
        </td>		
			</tr>	
		</ItemTemplate>
	</asp:ListView>

	<uc:ViewCaseManagementError runat="server" ID="ViewCaseManagementErrorModal" />

	<asp:ObjectDataSource ID="ErrorsDataSource" runat="server" TypeName="Focus.Web.WebAssist.CaseManagementErrors" EnablePaging="true" 
														SelectMethod="GetErrors" OnSelecting="ErrorsDataSource_Selecting" SelectCountMethod="GetErrorCount" SortParameterName="orderBy">
  </asp:ObjectDataSource>
</asp:Content>
