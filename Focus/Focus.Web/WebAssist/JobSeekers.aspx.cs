﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employer;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Focus.Web.WebAssist.Controls;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekersReadOnly)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekersAdministrator)]
	public partial class JobSeekers : PageBase
	{
		#region Private Properties

		private int _jobSeekersCount;
		private bool _reApplyFilter;

		private JobSeekerCriteria JobSeekerCriteria
		{
			get { return GetViewStateValue<JobSeekerCriteria>("JobSeekers:JobSeekerCriteria"); }
			set { SetViewStateValue("JobSeekers:JobSeekerCriteria", value); }
		}

		private ActionTypes? SelectedAction
		{
			get { return GetViewStateValue<ActionTypes>("JobSeekers:SelectedAction"); }
			set { SetViewStateValue("JobSeekers:SelectedAction", value); }
		}

		private List<JobSeekerView> SelectedJobSeekers
		{
			get { return GetViewStateValue<List<JobSeekerView>>("JobSeekers:SelectedJobSeekers"); }
			set { SetViewStateValue("JobSeekers:SelectedJobSeekers", value); }
		}

		#endregion

		protected string DateErrorMessage = "";
		protected string DOBMinLimitErrorMessage = "";
		protected string DOBMaxLimitErrorMessage = "";
		protected string DateOfBirthRequiredMessage = "";

    protected string ActionDropDownRequiredErrorMessage = "";
		protected string SingleCheckboxErrorMessage = "";
		protected string MultipleCheckboxErrorMessage = "";
		protected string CloseJobSeekerWindowErrorMessage = "";

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (StatusDropDown.SelectedValue.IsNotNullOrEmpty() && StatusDropDown.SelectedValue.ToLower() == "inactive")
				BindActionDropDown(false);
			else if (StatusDropDown.SelectedValue.IsNotNullOrEmpty() && StatusDropDown.SelectedValue.ToLower() == "active")
				BindActionDropDown(true);
			else if (StatusDropDown.SelectedValue.IsNullOrEmpty())
				BindActionDropDown(null);
			else if (StatusDropDown.SelectedValue.IsNotNullOrEmpty() && StatusDropDown.SelectedValue.ToLower() == "blocked")
				BindActionDropDown(null, showUnblockOption: true);
			else
			{
				BindActionDropDown(null);
			}
			try
			{
				ActionDropDown.SelectedValue = SelectedAction.ToString();
	
			}
			catch (Exception)
			{
				ActionDropDown.SelectedIndex = 0;
                ActivityAssigner.Visible = false;
			}
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("Page.Title", "Assist #CANDIDATETYPE#"); // Replaced plural: Job Seekers

			SetMessages();

			if (!IsPostBack)
			{
				// Re-set search criteria if returning from a page following a filtered result
				if (Request.QueryString["filtered"] != null)
				{
					_reApplyFilter = Convert.ToBoolean(Request.QueryString["filtered"]);
                    JobSeekerCriteria = App.GetSessionValue("Career:JobSeekerSearchCriteria", new JobSeekerCriteria());

					// Set the Pager control to the previous Listview page and pagesize
					SearchResultListPager.ReturnToPage(Convert.ToInt32(JobSeekerCriteria.PageIndex), Convert.ToInt32(JobSeekerCriteria.PageSize));
				}

				SetReturnUrl(); // To allow return from JobSeekerProfile
				LocaliseUI();
				ApplyBranding();
				SetVisibility();
				ApplyTheme();

				BindStaticDropDowns();
				BindSearchTextFields();

				RunSearch();

				OfficeFilterRow.Visible = App.Settings.OfficesEnabled;
				Messages.Visible = !App.Settings.TalentModulePresent;
                SSNPlaceHolder.Visible = SsnVisibilityEnabled.Visible= App.Settings.EnableSsnFeatureGroup;
                DateOfBirthSearch.Visible = App.Settings.EnableDobFeatureGroup;
				_reApplyFilter = false;
			}

			CreateNewJobSeekerButton.Visible = LocaliseCreateNewJobSeekerButton.Visible = CanCreateJobSeekers();

			if (!App.Settings.ShowAssistMultipleJSSearchButtons)
				UCLSSN.MaxSize = UCLExternalId.MaxSize = UCLPersonId.MaxSize = 1;
			else
				UCLSSN.MaxSize = UCLExternalId.MaxSize = UCLPersonId.MaxSize = App.Settings.MaxSearchableIDs;

			UCLExternalId.ValidatorRegex = App.Settings.ClientSpecificExternalJobseekerIDRegex;
			UCLExternalId.ValidatorRegexErrorMessage = CodeLocalise("UCLExternalIdRegexValidator.Text", "External ID format is invalid");

			BuildValidMultiActionsArray();

            //Accessibility 508 compliance
            var selectorAllCheckBoxes = (CheckBox)JobSeekersListView.FindControl("SelectorAllCheckBoxes");
            if (selectorAllCheckBoxes.IsNotNull())
                selectorAllCheckBoxes.InputAttributes.Add("Title", "Select All CheckBoxes");

            //Enable/Disable controls for config setting EnableJSActivityBackdatingDateSelection

            if (!App.Settings.EnableJSActivityBackdatingDateSelection)
            {
                var ssnbutton = UCLSSN.FindControl("ItemButton");
                ssnbutton.Visible = false;
                var externalidbutton = UCLExternalId.FindControl("ItemButton");
                externalidbutton.Visible = false;
                var personidbutton = UCLPersonId.FindControl("ItemButton");
                personidbutton.Visible = false;
            }
		}

		#region Get Current Application Settings 

		/// <summary>
		/// Determines whether [is assist jobseekers administrator].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is assist jobseekers administrator]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistJobseekersAdministrator()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAdministrator);
		}

		/// <summary>
		/// Determines whether the Assist user can create job seekers
		/// </summary>
		/// <returns>True if they can return job seekers, false otherwise</returns>
		protected bool CanCreateJobSeekers()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersCreateNewAccount) && !App.Settings.SSOEnabledForCareer;
		}

		/// <summary>
		/// Determines whether [is school type two year].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is school type two year]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsSchoolTypeTwoYear()
		{
			return App.Settings.SchoolType.Equals(SchoolTypes.TwoYear);
		}

		#endregion

		#region Button Events

		/// <summary>
		/// Handles the Clicked event of the CreateNewJobSeekerButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CreateNewJobSeekerButton_Clicked(object sender, EventArgs e)
		{
			RegisterJobSeeker.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the RunActivityReportButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void RunActivityReportButton_Clicked(object sender, EventArgs e)
		{
			StudentActivityReportCriteriaModal.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the FindButton_ control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FindButton_Clicked(object sender, EventArgs e)
		{
			var rows = RunSearch();
			if (rows == 0)
				CheckExternalSystem();

			// Save the Search Criteria to a session object so that the user's selections are retained if returning from Job Seeker Profile
			App.SetSessionValue("Career:JobSeekerSearchCriteria", JobSeekerCriteria);
		}

		protected void ClearButton_Clicked( object sender, EventArgs e )
		{
			Response.Redirect( UrlBuilder.AssistJobSeekers() );
		}

		/// <summary>
		/// Handles the Clicked event of the ActionButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ActionButton_Clicked(object sender, EventArgs e)
		{
			// Hide the sub action controls
			SubActionDropDown.Visible = SubActionButton.Visible = false;
			ActivityAssigner.Visible = false;

			SelectedAction = ActionDropDown.SelectedValueToEnum<ActionTypes>();

			var jobSeekersSelected = SelectedJobSeekers = GetSelectedJobSeekers();
			if (jobSeekersSelected.Count == 0)
			{
				ShowErrors(ErrorTypes.NoCandidatesSelected);
				return;
			}

			switch (SelectedAction)
			{
				case ActionTypes.AccessCandidateAccount:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					var singleSignOnKey = ServiceClientLocator.AuthenticationClient(App).CreateCandidateSingleSignOn(jobSeekersSelected[0].PersonId, null);
					var blockedPopupMessage = CodeLocalise("AccessCandidatePopupBlocked.Error.Text",
						"Unable to Access #CANDIDATETYPE# account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.");

					var script = string.Format("$(document).ready(function (){{JobSeekers_CareerPopup = showPopup('{0}{1}','{2}');}});", App.Settings.CareerApplicationPath, UrlBuilder.SingleSignOn(singleSignOnKey, false), blockedPopupMessage);
					ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AccessCandidate", script, true);
					ClearActionControls();
					break;

				case ActionTypes.AssignActivityToCandidate:
					PrepareAssignActivity();
					break;

				case ActionTypes.EmailCandidate:
					ActivityAssigner.Visible = false;
					var candidateIds = (from j in jobSeekersSelected select j.PersonId).ToList();
					EmailCandidate.Show(candidateIds);
					ClearActionControls();
					break;

				case ActionTypes.FindJobsForSeeker:
					MultipleJobSeekerSelectedTest(jobSeekersSelected);
					break;

				case ActionTypes.AddNote:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					NotesAndReminders.Show(jobSeekersSelected[0].PersonId, EntityTypes.JobSeeker, !IsAssistJobseekersAdministrator());
					ClearActionControls();
					break;

				case ActionTypes.AssignCandidateToStaff:
					PrepareAssignJobSeeker();
					ClearActionControls();
					break;

				case ActionTypes.AssignJobSeekerToStaff:
					PrepareAssignJobSeekerToStaffMember();
					break;

				case ActionTypes.AddCandidateToList:
					ActivityAssigner.Visible = false;
					JobSeekerList.Show(jobSeekersSelected.ToArray());
					ClearActionControls();
					break;

				case ActionTypes.UpdateCandidateStatus:
					break;

				case ActionTypes.MarkCandidateIssuesResolved:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					IssueResolutionModal.Show(jobSeekersSelected[0].PersonId);
					ClearActionControls();
					break;

				case ActionTypes.RequestFollowUp:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					RequestFollowUpModal.Show(jobSeekersSelected[0].PersonId, EntityTypes.JobSeeker);
					ClearActionControls();
					break;

				case ActionTypes.JobSearch:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					Response.Redirect(UrlBuilder.AssistNewSearchJobPostings(jobSeekersSelected[0].PersonId));
					ClearActionControls();
					break;

				case ActionTypes.ChangeJobSeekerSsn:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					EditSsnModal.Show(jobSeekersSelected[0].PersonId);
					ClearActionControls();
					break;

				case ActionTypes.AssignJobSeekerToMyself:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					var assistUserOffices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { PersonId = App.User.PersonId });
					var jobSeekerOffices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { PersonId = jobSeekersSelected[0].PersonId });

					var jsOfficeIds = jobSeekerOffices.Select(x => x.Id).ToList();

					if (assistUserOffices.Any(x => jsOfficeIds.Contains(x.Id)))
					{
						ServiceClientLocator.CandidateClient(App).AssignToStaffMember(new List<long> { jobSeekersSelected[0].PersonId }, Convert.ToInt64(App.User.PersonId));

						Confirmation.Show(CodeLocalise("AssignJobSeekerToStaffMemberConfirmation.Title", "#CANDIDATETYPE# assigned"),
							CodeLocalise("AssignJobSeekerToStaffMemberConfirmation.Body", "The selected #CANDIDATETYPE#:LOWER has been successfully assigned to yourself."),
							CodeLocalise("CloseModal.Text", "OK"));

						ClearActionControls();
						HideSubActions();
					}
					else
					{
						ShowErrors(ErrorTypes.JobSeekerNotInOffice);
					}

					break;

				case ActionTypes.BlockUnblockJobSeeker:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;

					var selectedJobSeeker = jobSeekersSelected[0];
					if (selectedJobSeeker.Blocked)
					{
						Confirmation.Show(CodeLocalise("UnblockJobSeeker.Title", "Unblock #CANDIDATETYPE#:LOWER account"),
							CodeLocalise("UnblockJobSeeker.Body", "By confirming this action, you will enable the selected #CANDIDATETYPE#:LOWER's access to #FOCUSCAREER#"),
							CodeLocalise("Global.Close", "Close"),
							"Unblock",
							okText: CodeLocalise("Global.OK", "OK"),
							okCommandName: "Unblock",
							okCommandArgument: selectedJobSeeker.PersonId.ToString(CultureInfo.InvariantCulture));
					}
					else
					{
						Confirmation.SetTextBoxDetails(CodeLocalise("BlockReason.HeaderText", "Reason"), 
							5, 
							true,
							CodeLocalise("BlockReason.ValidationMessage", "Reason is required"),
							"BlockedJobSeeker");

						Confirmation.Show(CodeLocalise("BlockJobSeeker.Title", "Block #CANDIDATETYPE#:LOWER account"),
							CodeLocalise("BlockJobSeeker.Body", "By confirming this action, you will disable the selected #CANDIDATETYPE#:LOWER's access to #FOCUSCAREER#.<br /><br />Please enter your reason for blocking their account access"),
							CodeLocalise("Global.Close", "Close"),
							"Block",
							okText: CodeLocalise("Global.OK", "OK"),
							okCommandName: "Block",
							okCommandArgument: selectedJobSeeker.PersonId.ToString(CultureInfo.InvariantCulture));
					}

					break;

				case ActionTypes.InactivateReactivateJobSeeker:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;

					var jobSeeker = jobSeekersSelected[0];
					if (jobSeeker.Enabled)
					{
						Confirmation.Show(CodeLocalise("InactivateJobSeeker.Title", "Inactivate #CANDIDATETYPE#:LOWER account"),
							CodeLocalise("InactivateJobSeeker.Body", "By confirming this action the #CANDIDATETYPE#:LOWER’s resume will no longer appear in #FOCUSTALENT# searches and their account will be marked as inactive."),
							CodeLocalise("Global.Close", "Close"),
							"Inactivate",
							okText: CodeLocalise("Global.OK", "OK"),
							okCommandName: "Inactivate",
							okCommandArgument: jobSeeker.PersonId.ToString(CultureInfo.InvariantCulture));
					}
					else
					{
						var reactivateSingleSignOnKey = ServiceClientLocator.AuthenticationClient(App).CreateCandidateSingleSignOn(jobSeekersSelected[0].PersonId, null);
						var reactivateBlockedPopupMessage = CodeLocalise("ReactivateCandidatePopupBlocked.Error.Text",
							"Unable to reactivate #CANDIDATETYPE# account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.");

						var reactivateScript = string.Format("$(document).ready(function (){{JobSeekers_CareerPopup = showPopup('{0}{1}','{2}');}});", App.Settings.CareerApplicationPath, UrlBuilder.SingleSignOnWithReactivation(reactivateSingleSignOnKey, false), reactivateBlockedPopupMessage);
						ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ReactivateCandidate", reactivateScript, true);
					}
					break;

				case ActionTypes.OpenAccountInAsset:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;

					ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", string.Format("window.open('{0}');", App.Settings.AssetUrl), true);
					break;
				case ActionTypes.DeactivateJobSeeker:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					var jobSeeekrsToDeactivate = jobSeekersSelected[0];
					Confirmation.Show(CodeLocalise("DeactivateJobSeeker.Title", "Deactivate #CANDIDATETYPE#:LOWER account"),
						CodeLocalise("DeactivateJobSeeker.Body", "By confirming this action, you will deactivate the selected #CANDIDATETYPE#:LOWER's access to #FOCUSCAREER#. This action is not reversible."),
						CodeLocalise("Global.Close", "Close"),
						"Deactivate",
						okText: CodeLocalise("Global.OK", "OK"),
						okCommandName: "Deactivate",
						okCommandArgument: jobSeeekrsToDeactivate.PersonId.ToString(CultureInfo.InvariantCulture));

					break;

			}
			if( !SubActionDropDown.Visible && !ActivityAssigner.Visible )
			{
				SelectedAction = ActionDropDown.Items[0].ValueToEnum<ActionTypes>();
				ActionDropDown.SelectedIndex = 0;
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SubActionButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SubActionButton_Clicked(object sender, EventArgs e)
		{
			switch (SelectedAction)
			{
				case ActionTypes.AssignCandidateToStaff:
				{
					var adminId = SubActionDropDown.SelectedValueToLong();

					ServiceClientLocator.CandidateClient(App).AssignJobSeekerToAdmin(adminId, SelectedJobSeekers[0]);

					Confirmation.Show(CodeLocalise("AssignJobSeekerToAdminConfirmation.Title", "#CANDIDATETYPE# assigned"),
						CodeLocalise("AssignJobSeekerToAdminConfirmation.Body", "The #CANDIDATETYPE# has been assigned to the selected staff member."),
						CodeLocalise("CloseModal.Text", "OK"));

					ClearActionControls();
					HideSubActions();
					break;
				}

				case ActionTypes.AssignJobSeekerToStaff:
				{
					var staffId = SubActionDropDown.SelectedValueToLong();

					ServiceClientLocator.CandidateClient(App).AssignToStaffMember(GetSelectedJobSeekers().Select(x => x.PersonId).ToList(), staffId);

					Confirmation.Show(CodeLocalise("AssignJobSeekerToStaffMemberConfirmation.Title", "#CANDIDATETYPE# assigned"),
						CodeLocalise("AssignJobSeekerToStaffMemberConfirmation.Body", "The #CANDIDATETYPE# has been assigned to {0}", SubActionDropDown.SelectedItem.Text),
						CodeLocalise("CloseModal.Text", "OK"));

					ClearActionControls();
					HideSubActions();
					break;
				}

				case ActionTypes.AssignActivityToCandidate:
					break;
			}
		}

		/// <summary>
		/// Handles the OnOkCommand event of the Confirmation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_OnOkCommand(object sender, CommandEventArgs eventargs)
		{
			var success = false;

			switch (eventargs.CommandName)
			{
				case "Block":
					success = ServiceClientLocator.AccountClient(App).BlockPerson(eventargs.CommandArgument.AsLong(), false, Confirmation.ConfirmationText);
					Confirmation.Show(CodeLocalise("BlockJobSeeker.Title", "Block #CANDIDATETYPE#:LOWER account"),
						success
							? CodeLocalise("BlockJobSeekerSuccess.Body", "#CANDIDATETYPE# successfully blocked")
							: CodeLocalise("BlockJobSeekerFailure.Body", "#CANDIDATETYPE# is already blocked"),
						CodeLocalise("Global.Close", "Close"));
					break;

				case "Unblock":
					success = ServiceClientLocator.AccountClient(App).UnblockPerson(eventargs.CommandArgument.AsLong());
					Confirmation.Show(CodeLocalise("UnblockJobSeeker.Title", "Unblock #CANDIDATETYPE#:LOWER account"),
						success
							? CodeLocalise("UnblockJobSeekerSuccess.Body", "#CANDIDATETYPE# successfully unblocked")
							: CodeLocalise("UnblockJobSeekerFailure.Body", "#CANDIDATETYPE# is already unblocked"),
						CodeLocalise("Global.Close", "Close"));
					break;
				case "Deactivate":
					success = ServiceClientLocator.AccountClient(App).BlockPerson(eventargs.CommandArgument.AsLong(), true);
					Confirmation.Show(CodeLocalise("DeactivateJobSeekerSuccess.Title", "Deactivate #CANDIDATETYPE#:LOWER account"),
						CodeLocalise("DeactivateJobSeekerSuccess.Body", "#CANDIDATETYPE# successfully deactivated"),
						CodeLocalise("Global.Close", "Close"));
					break;

				case "Inactivate":
					success = ServiceClientLocator.AccountClient(App).InactivateJobSeeker(eventargs.CommandArgument.AsLong(), AccountDisabledReason.InactivatedByStaffRequest);
					Confirmation.Show(CodeLocalise("InactivateJobSeekerSuccess.Title", "Inactivate #CANDIDATETYPE#:LOWER account"),
						CodeLocalise("InactivateJobSeekerSuccess.Body", "#CANDIDATETYPE# successfully inactivated"),
						CodeLocalise("Global.Close", "Close"));
					break;
			}

			if (success)
				BindJobSeekersList();

			ClearActionControls();
		}

		/// <summary>
		/// Handles the OnCloseCommand event of the Confirmation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_OnCloseCommand(object sender, CommandEventArgs eventargs)
		{
			ClearActionControls();
		}

		#endregion

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the employees.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<JobSeekerDashboardModel> GetJobSeekers(JobSeekerCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_jobSeekersCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;

			var jobSeekers = ServiceClientLocator.CandidateClient(App).GetCandidatesAndIssuesPagedList(criteria);
			_jobSeekersCount = jobSeekers.TotalCount;

			return jobSeekers;
		}

		/// <summary>
		/// Gets the job seeker activity count.
		/// </summary>
		/// <returns></returns>
		public int GetJobSeekersCount()
		{
			return _jobSeekersCount;
		}

		/// <summary>
		/// Gets the job seekers count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetJobSeekersCount(JobSeekerCriteria criteria)
		{
			return _jobSeekersCount;
		}

		/// <summary>
		/// Handles the Selecting event of the SearchResultDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (!IsPostBack)
			{
				if (App.Settings.OfficesEnabled && !_reApplyFilter)
					JobSeekerCriteria.OfficeIds = GetAllUserOfficeIds();
			}
			e.InputParameters["criteria"] = JobSeekerCriteria;
            App.SetSessionValue("Career:JobSeekerSearchCriteria", JobSeekerCriteria);
		}

		#endregion

		#region Job seeker list events

		/// <summary>
		/// Handles the LayoutCreated event of the JobSeekersListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void JobSeekersListView_LayoutCreated(object sender, EventArgs e)
		{
			#region Set labels

			JobSeekersListView.FindControl("SelectorAllCheckBoxes").Visible = IsAssistJobseekersAdministrator();
			((Literal)JobSeekersListView.FindControl("NameHeaderLiteral")).Text = CodeLocalise("JobSeekerName.ColumnTitle", "Name");
			((Literal)JobSeekersListView.FindControl("ExternalIdLiteral")).Text = CodeLocalise("JobSeekerName.ColumnTitle", "#CANDIDATETYPE# ID");
			((Literal)JobSeekersListView.FindControl("LocationHeaderLiteral")).Text = CodeLocalise("JobSeekerLocation.ColumnTitle", "Location");
			((Literal)JobSeekersListView.FindControl("LastActivityDateHeaderLiteral")).Text = CodeLocalise("JobSeekerName.LastActivity", "Last activity date");
			((Literal)JobSeekersListView.FindControl("IssuesHeaderLiteral")).Text = CodeLocalise("JobSeekerIssues.ColumnTitle", "Issues");

			#endregion
		}

		/// <summary>
		/// Handles the Sorting event of the JobOrdersList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void JobSeekerListView_Sorting(object sender, ListViewSortEventArgs e)
		{
			JobSeekerCriteria.OrderBy = e.SortExpression;
			FormatJobSeekerListSortingImages();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the JobSeekerListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void JobSeekerListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var candidateIssues = (JobSeekerDashboardModel)e.Item.DataItem;
			var checkBox = (CheckBox)e.Item.FindControl("SelectorCheckBox");

			checkBox.Visible = e.Item.FindControl("SelectorCheckBoxLabel").Visible = IsAssistJobseekersAdministrator();

			//// TODO: Check whether to remove one of these when retrieving Ids
			checkBox.Attributes.Add("value", candidateIssues.Id.ToString());
			checkBox.InputAttributes.Add("value", candidateIssues.Id.ToString());

			#region Name

			var nameLink = (HyperLink)e.Item.FindControl("JobSeekerNameLink");
			nameLink.Text = string.Concat(candidateIssues.LastName, ", ", candidateIssues.FirstName, " ", candidateIssues.MiddleInitial).Trim();
			nameLink.NavigateUrl = UrlBuilder.AssistJobSeekerProfile(candidateIssues.Id.GetValueOrDefault(0)) +
			                       ((!Page.IsPostBack) ?
				                       "?filtered=false" :
				                       "?filtered=true");

			#endregion

			#region Veteran Image

			var veteranImage = (Image)e.Item.FindControl("VeteranImage");
			if (candidateIssues.IsVeteran.GetValueOrDefault(false)) BindVeteranImage(veteranImage);

			#endregion

			#region Veteran Image

			var migrantFarmWorkerImage = (Image)e.Item.FindControl("MSFWImage");
			if (candidateIssues.MigrantSeasonalFarmWorkerVerified.GetValueOrDefault(false)) BindMSFWVerifiedImage(migrantFarmWorkerImage);

			#endregion

			#region Blocked Image

			var blockedImage = (Image)e.Item.FindControl("BlockedImage");
			BindBlockedImage(blockedImage, candidateIssues.Blocked, candidateIssues.BlockedReason);

			#endregion

			#region Underage Image

			var underageImage = (Image)e.Item.FindControl("UnderageImage");
			BindUnderageImage(underageImage, candidateIssues.DateOfBirth);

			#endregion

			#region Inactive Image

			var inactiveImage = (Image)e.Item.FindControl("InactiveImage");
			BindInactiveImage(inactiveImage, candidateIssues.Enabled);

			#endregion

			# region Location

			var locationLiteral = (Literal)e.Item.FindControl("CandidateLocationLiteral");

			var stateId = candidateIssues.StateId;
			var town = candidateIssues.TownCity;

			if (town.IsNotNullOrEmpty())
				locationLiteral.Text = town;

			if (stateId.HasValue)
			{
				var stateLookUp =
					ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).SingleOrDefault(x => x.Id == stateId);

				if (stateLookUp != null)
				{
					locationLiteral.Text = locationLiteral.Text.IsNotNullOrEmpty()
						? string.Concat(locationLiteral.Text, ", ", stateLookUp.Text)
						: stateLookUp.Text;
				}
			}

			if (locationLiteral.Text.IsNullOrEmpty())
				locationLiteral.Text = CodeLocalise("Global.NotRecorded", "Not recorded");

			#endregion

			#region Issues

			var issues = BuildIssueList(candidateIssues);
			var issuesLabel = (Label)e.Item.FindControl("IssuesLabel");
			var showAdditionalIssuesLink = (HyperLink)e.Item.FindControl("ShowAdditionalIssuesLink");
			if (issues.Count < 4)
			{
				issuesLabel.Text = string.Join(", ", issues);
			}
			else
			{
				const string comma = ", ";
				issuesLabel.Text = string.Concat(string.Join(comma, issues.Take(2)), comma);
				var additionalIssuesLiteral = (Literal) e.Item.FindControl("AdditionalIssuesLiteral");
				additionalIssuesLiteral.Text = string.Join(", ", issues.Skip(2));

				showAdditionalIssuesLink.Visible = true;
				showAdditionalIssuesLink.Text = CodeLocalise("AdditionalIssuesLink.Text", "+ {0} more", (issues.Count - 2));
				showAdditionalIssuesLink.Attributes.Add("onclick",
					string.Format("ShowAdditionalIssues({0});",
						candidateIssues.Id.ToString()));
				showAdditionalIssuesLink.Attributes.Add("onkeypress",
					string.Format("ShowAdditionalIssues({0});",
						candidateIssues.Id.ToString()));

			}

			#endregion
		}

		#endregion

		#region Modal Events

		/// <summary>
		/// Handles the Resolved event of the Issues control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Issues_Updated(object sender, EventArgs e)
		{
			RunSearch();
		}

		/// <summary>
		/// Handlers the JobSeekerRegistered event of the RegisterJobSeeker modal
		/// </summary>
		/// <param name="sender">The modal raising the event</param>
		/// <param name="e">Standard event arguments</param>
		protected void RegisterJobSeeker_OnJobSeekerRegistered(object sender, EventArgs e)
		{
			RunSearch();
			FindJobSeekerUpdatePanel.Update();
		}

		#endregion

		#region Bind / Unbind Methods

		/// <summary>
		/// Binds text boxes if re-applying filter
		/// </summary>
		private void BindSearchTextFields()
		{
			var expandPanel = false;

			if (!_reApplyFilter)
			{
				return;
			}

			FirstNameTextBox.Text = JobSeekerCriteria.Firstname;
			LastNameTextBox.Text = JobSeekerCriteria.Lastname;
			EmailAddressTextBox.Text = JobSeekerCriteria.EmailAddress;
			UsernameTextBox.Text = JobSeekerCriteria.Username;

			if (JobSeekerCriteria.ExternalSeekerIdList.IsNotNullOrEmpty())
			{
				var selectedExternalIds =
					JobSeekerCriteria.ExternalSeekerIdList.Where(id => id.IsNotNullOrEmpty())
						.Select(id => new UpdateableClientListItem
						{
							Value = id,
							Label = id,
							IsDefault = false
						}).ToList();

				if (selectedExternalIds.Any())
				{
					UCLExternalId.SetSelectedItems(selectedExternalIds);
					expandPanel = true;
				}
			}

			if (JobSeekerCriteria.SsnList.IsNotNullOrEmpty())
			{
				var selectedSsnIds = JobSeekerCriteria.SsnList.Where(id => id.IsNotNullOrEmpty()).Select(id => new UpdateableClientListItem
				{
					Value = id,
					Label = id,
					IsDefault = false
				}).ToList();

				if (selectedSsnIds.Any())
				{
					UCLSSN.SetSelectedItems(selectedSsnIds);
					expandPanel = true;
				}
			}

			if (JobSeekerCriteria.PersonIds.IsNotNullOrEmpty())
			{
				var selectedPersonIds = JobSeekerCriteria.PersonIds.Where(id => id.ToString().IsNotNullOrEmpty() && id != 0).Select(id => new UpdateableClientListItem
				{
					Value = id.ToString(),
					Label = id.ToString(),
					IsDefault = false
				}).ToList();

				if (selectedPersonIds.Any())
				{
					UCLPersonId.SetSelectedItems(selectedPersonIds);
					expandPanel = true;
				}
			}

			if (JobSeekerCriteria.DateOfBirth.IsNotNull())
			{
				DateOfBirthTextBox.Text = JobSeekerCriteria.DateOfBirth.Value.ToString(CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern);
				expandPanel = true;
			}

			// Expand the Job Seeker Search Panel if any of the fields have values 
			if (JobSeekerCriteria.Firstname.IsNotNullOrEmpty() || JobSeekerCriteria.Lastname.IsNotNullOrEmpty() ||
			    JobSeekerCriteria.EmailAddress.IsNotNullOrEmpty() || JobSeekerCriteria.Username.IsNotNullOrEmpty() || expandPanel)
			{
				JobSeekerSearchPanelExtender.Collapsed = !expandPanel;
			}
		}

		/// <summary>
		/// Binds the static drop downs.
		/// </summary>
		private void BindStaticDropDowns()
		{
			BindStatusDropDown();
			//BindActionDropDown();

			BindJobSeekerFilterTypeDropDown();
			BindIssuesForCandidate(JobSeekerIssuesDropDown);
			WorkforceSearchPanel.Visible = true;
			WorkforceJobSeekerSearchPanel.Visible = true;
			WorkforceModalPanels.Visible = true;
		}

		/// <summary>
		/// Binds the job seekers list.
		/// </summary>
		private void BindJobSeekersList()
		{
			JobSeekersListView.DataBind();

			// Set visibility of other controls
			JobSeekersListView.Visible = true;
			SearchResultListPager.Visible = (SearchResultListPager.TotalRowCount > 0);

			if (IsAssistJobseekersAdministrator())
			{
				ActionDropDown.Visible = ActionGoButton.Visible = ActionLabel.Visible = (SearchResultListPager.TotalRowCount > 0);
				ActionDropDown.SelectedIndex = 0;
				SubActionDropDown.Visible = SubActionButton.Visible = false;
			}
		}

		/// <summary>
		/// Binds the status drop down.
		/// </summary>
		private void BindStatusDropDown()
		{
			StatusDropDown.Items.Clear();
			StatusDropDown.Items.Add(new ListItem(CodeLocalise("StatusDropDown.AllStatuses", "All statuses")));
			StatusDropDown.Items.Add(new ListItem(CodeLocalise("StatusDropDown.Active", "Active")));
			StatusDropDown.Items.Add(new ListItem(CodeLocalise("StatusDropDown.Inactive", "Inactive")));
			StatusDropDown.Items.Add(new ListItem(CodeLocalise("StatusDropDown.Blocked", "Blocked")));
			
			StatusDropDown.SelectedIndex = 1; // Keep Active as the default value

			if (_reApplyFilter)
			{

				if (JobSeekerCriteria.JobSeekerUserStatus.IsNotNull())
				{
					StatusDropDown.SelectedIndex = JobSeekerCriteria.JobSeekerUserStatus.Value ? 0 : 1;
				}
				else if ((App.Settings.Theme == FocusThemes.Workforce) &&
				         JobSeekerCriteria.JobSeekerBlockedStatus.IsNotNull() && JobSeekerCriteria.JobSeekerBlockedStatus.Value)
				{
					StatusDropDown.SelectedIndex = 2;
				}

			}
		}

		/// <summary>
		/// Set the OfficeFilterModel for the Office Filter control.
		/// </summary>
		private void RebindOfficeFilter()
		{
			OfficeFilter.OfficeFilterModel = JobSeekerCriteria.OfficeFilterModel;
		}

		/// <summary>
		/// Binds the veteran drop down.
		/// </summary>
		private void BindJobSeekerFilterTypeDropDown()
		{
			JobSeekerFilterTypeDropDown.Items.Clear();
			JobSeekerFilterTypeDropDown.Items.AddLocalisedTopDefault("JobSeekerFilterTypeDropDown.AllJobSeekers.TopDefault", "All #CANDIDATETYPES#:LOWER");
            JobSeekerFilterTypeDropDown.Items.Add(new ListItem(CodeLocalise(JobSeekerFilterType.MSFWVerified, "Verified MSFW #CANDIDATETYPES#:LOWER"), JobSeekerFilterType.MSFWVerified.ToString()));
			JobSeekerFilterTypeDropDown.Items.Add(new ListItem(CodeLocalise(JobSeekerFilterType.NonVeteran, "Non-veteran #CANDIDATETYPES#:LOWER"), JobSeekerFilterType.NonVeteran.ToString()));
			JobSeekerFilterTypeDropDown.Items.Add(new ListItem(CodeLocalise(JobSeekerFilterType.Veteran, "Veteran #CANDIDATETYPES#:LOWER"), JobSeekerFilterType.Veteran.ToString()));
            JobSeekerFilterTypeDropDown.Items.Add(new ListItem(CodeLocalise(JobSeekerFilterType.UnderAge, "Under-age #CANDIDATETYPES#:LOWER"), JobSeekerFilterType.UnderAge.ToString()));

			if (_reApplyFilter) JobSeekerFilterTypeDropDown.SelectedValue = JobSeekerCriteria.JobSeekerFilterType.ToString();
		}

		/// <summary>
		/// Binds the action drop down.
		/// </summary>
		private void BindActionDropDown(bool? showInactiveOption = true, bool showUnblockOption = false)
		{
			ActionDropDown.Items.Clear();

			ActionDropDown.Items.AddLocalisedTopDefault("ActionDropDown.TopDefault", "- select action -");
			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AccessCandidateAccount, "Access #CANDIDATETYPE#:LOWER account"), ActionTypes.AccessCandidateAccount.ToString()));
			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.RequestFollowUp, "Add follow-up"), ActionTypes.RequestFollowUp.ToString()));
			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AddCandidateToList, "Add #CANDIDATETYPE#:LOWER(s) to list"), ActionTypes.AddCandidateToList.ToString()));
			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AddNote, "Add note or reminder"), ActionTypes.AddNote.ToString()));

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				if (!App.Settings.ShowAssetAccess && App.Settings.EnableJSActivityBackdatingDateSelection)
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignActivityToCandidate, "Assign activities/services"), ActionTypes.AssignActivityToCandidate.ToString()));
                if(App.Settings.EnableSsnFeatureGroup){
				if (App.Settings.AssistShowChangeSSNActivityInWF)
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.ChangeJobSeekerSsn, "Change #CANDIDATETYPE#:LOWER's SSN"), ActionTypes.ChangeJobSeekerSsn.ToString()));}
			}

			if (App.Settings.OfficesEnabled)
			{
				var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(Convert.ToInt64(App.User.UserId));
				if (userDetails.PersonDetails.Manager.IsNotNull() && Convert.ToBoolean(userDetails.PersonDetails.Manager))
				{
					//This ddl item has been hidden for now as it is not currently needed however this item is for when assist is integrated with a client's back end system so may be needed in future
					//ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignCandidateToStaff, "Assign #CANDIDATETYPE# to staff"), ActionTypes.AssignCandidateToStaff.ToString()));
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignJobSeekerToStaff, "Assign #CANDIDATETYPE#:LOWER(s) to staff"), ActionTypes.AssignJobSeekerToStaff.ToString()));
				}
				else
				{
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignJobSeekerToMyself, "Assign #CANDIDATETYPE#:LOWER to myself"), ActionTypes.AssignJobSeekerToMyself.ToString()));
				}
			}

			if (App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAccountBlocker))
			{
				if (showUnblockOption && App.Settings.Theme == FocusThemes.Workforce)
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.BlockUnblockJobSeeker, "Unblock #CANDIDATETYPE#:LOWER"), ActionTypes.BlockUnblockJobSeeker.ToString()));
				else
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.BlockUnblockJobSeeker, "Block/Unblock #CANDIDATETYPE#:LOWER"), ActionTypes.BlockUnblockJobSeeker.ToString()));

				if (App.Settings.SSOEnableDeactivateJobSeeker)
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.DeactivateJobSeeker, "Deactivate #CANDIDATETYPE#:LOWER"), ActionTypes.DeactivateJobSeeker.ToString()));
			}

			if (App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersSendMessages))
				ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.EmailCandidate, "Email #CANDIDATETYPE#:LOWER(s)"), ActionTypes.EmailCandidate.ToString()));

			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.JobSearch, "Find job for #CANDIDATETYPE#:LOWER"), ActionTypes.JobSearch.ToString()));

			if (App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAccountInactivator))
			{
				#region Text explanation

				// This is the requirement (FVN-3331) for the text:
				// When filtered on "All statuses" or "Blocked", the action name should be "Inactivate/Reactivate job seeker". 
				// When filtered on "Active", the action name should be "Inactivate job seeker"
				// When filtered on "Inactive", the action name should be "Reactivate job seeker".

				// because there are 3 versions I have changed the showUnblockOption parameter from a bool to a nullable bool to accommodate the 3 versions.

				#endregion

				if (showInactiveOption.IsNull())
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.InactivateReactivateJobSeeker, "Inactivate/Reactivate #CANDIDATETYPE#:LOWER"), ActionTypes.InactivateReactivateJobSeeker.ToString()));
				else if (showInactiveOption.IsNotNull() && (bool)showInactiveOption)
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.InactivateReactivateJobSeeker, "Inactivate #CANDIDATETYPE#:LOWER"), ActionTypes.InactivateReactivateJobSeeker.ToString()));
				else if (showInactiveOption.IsNotNull() && !(bool)showInactiveOption)
					ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.InactivateReactivateJobSeeker, "Reactivate #CANDIDATETYPE#:LOWER"), ActionTypes.InactivateReactivateJobSeeker.ToString()));
			}

			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.MarkCandidateIssuesResolved, "Mark as resolved"), ActionTypes.MarkCandidateIssuesResolved.ToString()));

			if (App.Settings.ShowAssetAccess)
				ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.OpenAccountInAsset, "Open account in Asset"), ActionTypes.OpenAccountInAsset.ToString()));
		}

		/// <summary>
		/// Binds the sub action drop down to staff.
		/// </summary>
		private void BindSubActionDropDownToStaff()
		{
			#region Bind sub action to staff

			SubActionDropDown.Items.Clear();

			SubActionDropDown.DataSource = ServiceClientLocator.AccountClient(App).GetAssistUsers();
			SubActionDropDown.DataValueField = "Id";
			SubActionDropDown.DataTextField = "Name";
			SubActionDropDown.DataBind();

			#endregion
		}

		/// <summary>
		/// Binds the sub action drop down job seeker to staff.
		/// </summary>
		private void BindSubActionDropDownJobSeekerToStaff()
		{
			#region Bind sub action to staff

			#region Get the Offices the manager handles work for

			var stateId = ServiceClientLocator.EmployerClient(App).GetManagerState(Convert.ToInt64(App.User.PersonId));

			var officeIds = stateId.IsNull()
				? ServiceClientLocator.EmployerClient(App).GetOfficesPersonManages(Convert.ToInt64(App.User.PersonId)).Select(x => x.Id).ToList()
				: ServiceClientLocator.EmployerClient(App).GetOfficesByState(Convert.ToInt64(stateId)).Select(x => x.Id).ToList();

			#endregion

			SubActionDropDown.Items.Clear();

			var criteria = new StaffMemberCriteria
			{
				OfficeIds = officeIds, 
				Enabled = true,
				Blocked = false,
				FetchOption = CriteriaBase.FetchOptions.List
			};
			var staffMembers = ServiceClientLocator.EmployerClient(App).GetOfficeStaffMembersList(criteria);
			var staffQuery = staffMembers.Select(s => new { s.Id, DisplayText = s.LastName + ", " + s.FirstName });

			SubActionDropDown.DataSource = staffQuery;
			SubActionDropDown.DataValueField = "Id";
			SubActionDropDown.DataTextField = "DisplayText";
			SubActionDropDown.DataBind();
			SubActionDropDown.Items.AddLocalisedTopDefault("StaffSubActionDropDown.TopDefault", "- select staff member name -");

			#endregion
		}

		/// <summary>
		/// Unbinds the search.
		/// </summary>
		private void UnbindSearch()
		{
			if (JobSeekerCriteria == null)
				JobSeekerCriteria = new JobSeekerCriteria();

			JobSeekerCriteria.Firstname = FirstNameTextBox.TextTrimmed();
			JobSeekerCriteria.Lastname = LastNameTextBox.TextTrimmed();
			JobSeekerCriteria.Username = UsernameTextBox.TextTrimmed();
			JobSeekerCriteria.EmailAddress = EmailAddressTextBox.TextTrimmed();

			DateTime dateOfBirth;
			JobSeekerCriteria.DateOfBirth = DateTime.TryParse(DateOfBirthTextBox.Text, out dateOfBirth) ? dateOfBirth : (DateTime?)null;

			var tempSsnList = UCLSSN.SelectedItems;
			var ssnList = tempSsnList.IsNotNullOrEmpty() ? tempSsnList.ToList().Select(ssn => ssn.Value.Replace("-", "")).ToList() : null;
			JobSeekerCriteria.SsnList = ssnList;

			var externalSeekerIdList = UCLExternalId.SelectedItems;
			JobSeekerCriteria.ExternalSeekerIdList = externalSeekerIdList.IsNotNullOrEmpty() ? externalSeekerIdList.Select(x => x.Value).ToList() : null;

			var tempPersonIdList = UCLPersonId.SelectedItems;
			var personIdList = tempPersonIdList.IsNotNullOrEmpty() ? UCLPersonId.SelectedItems.Select(x => long.Parse(x.Value)).ToList() : null;
			JobSeekerCriteria.PersonIds = personIdList;

			// Show/Hide the Search Panels if reapplying filter
			if (_reApplyFilter) SetSearchPanels();

			if (App.Settings.OfficesEnabled)
			{
				var officeFilterModel = _reApplyFilter && JobSeekerCriteria.OfficeFilterModel.IsNotNull() ? JobSeekerCriteria.OfficeFilterModel : OfficeFilter.Unbind();

				if (officeFilterModel.OfficeId.IsNotNull() && officeFilterModel.OfficeId > 0)
				{
					JobSeekerCriteria.OfficeIds = new List<long?> { officeFilterModel.OfficeId };
				}
				else if (officeFilterModel.OfficeGroup == OfficeGroup.MyOffices)
				{
					JobSeekerCriteria.OfficeIds = GetAllUserOfficeIds();
				}
				else if (officeFilterModel.OfficeGroup == OfficeGroup.StatewideOffices)
				{
					JobSeekerCriteria.OfficeIds = null;
				}

				JobSeekerCriteria.StaffMemberId = officeFilterModel.StaffMemberId;

				if (officeFilterModel.AssignmentType.IsNotNull())
				{
					if (officeFilterModel.AssignmentType == AssignmentType.MyAssignments)
						JobSeekerCriteria.StaffMemberId = Convert.ToInt64(App.User.PersonId);
					else
						JobSeekerCriteria.StaffMemberId = null;
				}

				JobSeekerCriteria.OfficeFilterModel = officeFilterModel;

			}

			SetJobSeekerSearchCriteria();

		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Sets the job seeker search criteria.
		/// </summary>
		private void SetJobSeekerSearchCriteria()
		{
			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				JobSeekerCriteria.JobSeekerUserStatus = null;
				JobSeekerCriteria.JobSeekerBlockedStatus = null;

				switch (StatusDropDown.SelectedIndex)
				{
					case 1:
						JobSeekerCriteria.JobSeekerUserStatus = true;
						break;
					case 2:
						JobSeekerCriteria.JobSeekerUserStatus = false;
						break;
					case 3:
						JobSeekerCriteria.JobSeekerBlockedStatus = true;
						break;
				}
			}
			else
			{
				JobSeekerCriteria.JobSeekerUserStatus = StatusDropDown.SelectedIndex == 0;
			}

			JobSeekerCriteria.JobSeekerGroupedIssueSearch = null;
			JobSeekerCriteria.JobSeekerIssue = null;

			switch (JobSeekerIssuesDropDown.SelectedIndex)
			{
				case 1:
					JobSeekerCriteria.JobSeekerGroupedIssueSearch = true;
					break;

				case 2:
					JobSeekerCriteria.JobSeekerGroupedIssueSearch = false;
					break;

				default:
					if (JobSeekerIssuesDropDown.SelectedIndex > 2)
						JobSeekerCriteria.JobSeekerIssue = JobSeekerIssuesDropDown.SelectedValueToEnum<CandidateIssueType>();
					break;
			}

			JobSeekerCriteria.JobSeekerFilterType = JobSeekerFilterTypeDropDown.SelectedValueToEnum<JobSeekerFilterType>();
	
			switch (App.Settings.CareerExplorerModule)
			{
				case FocusModules.CareerExplorer:
					JobSeekerCriteria.UserType = (int)(UserTypes.Career | UserTypes.Explorer);
					break;

				case FocusModules.Career:
					JobSeekerCriteria.UserType = (int)UserTypes.Career;
					break;

				case FocusModules.Explorer:
					JobSeekerCriteria.UserType = (int)UserTypes.Explorer;
					break;

				default:
					JobSeekerCriteria.UserType = (int)(UserTypes.Career | UserTypes.Explorer);
					break;
			}
		}

		/// <summary>
		/// Show or Hide the search panel criteria depending on whether any of the child controls contain values.
		/// </summary>
		private void SetSearchPanels()
		{
			if (JobSeekerCriteria.Firstname.IsNotNullOrEmpty() || JobSeekerCriteria.Lastname.IsNotNullOrEmpty() ||
			    JobSeekerCriteria.Username.IsNotNullOrEmpty() || JobSeekerCriteria.EmailAddress.IsNotNullOrEmpty() ||
			    JobSeekerCriteria.DateOfBirth.IsNotNull() || JobSeekerCriteria.SsnList.IsNotNullOrEmpty() ||
			    JobSeekerCriteria.ExternalSeekerIdList.IsNotNullOrEmpty() || JobSeekerCriteria.PersonIds.IsNotNullOrEmpty())
			{
				JobSeekerSearchPanelExtender.Collapsed = false;
			}
		}

		/// <summary>
		/// Multiples the job seeker selected test.
		/// </summary>
		/// <param name="selectedCandidates">The selected candidates.</param>
		/// <returns></returns>
		private bool MultipleJobSeekerSelectedTest(List<JobSeekerView> selectedCandidates)
		{
			if (selectedCandidates.Count > 1) ShowErrors(ErrorTypes.MultipleCandidatesSelected);
			return selectedCandidates.Count > 1;
		}

		/// <summary>
		/// Shows the errors.
		/// </summary>
		/// <param name="error">The error.</param>
		private void ShowErrors(ErrorTypes error)
		{
			switch (error)
			{
				case ErrorTypes.NoCandidatesSelected:
					CandidateActionDropDownValidator.ErrorMessage = SingleCheckboxErrorMessage;
					CandidateActionDropDownValidator.IsValid = false;
					break;

				case ErrorTypes.MultipleCandidatesSelected:
					CandidateActionDropDownValidator.ErrorMessage = MultipleCheckboxErrorMessage;
					CandidateActionDropDownValidator.IsValid = false;
					break;

				case ErrorTypes.JobSeekerNotInOffice:
					CandidateActionDropDownValidator.ErrorMessage = HtmlLocalise("JobSeekerNotInOffice.Error.Text", "Selected #CANDIDATETYPE#:LOWER is not in your assigned offices");
					CandidateActionDropDownValidator.IsValid = false;
					break;
			}
		}

		/// <summary>
		/// Binds the veteran image.
		/// </summary>
		/// <param name="veteranImage">The veteran image.</param>
		private void BindVeteranImage(Image veteranImage)
		{
			veteranImage.ToolTip = veteranImage.AlternateText = CodeLocalise("VeteranImage.ToolTip", "Veteran");
			veteranImage.ImageUrl = UrlBuilder.VeteranImage();
			veteranImage.Visible = true;
		}

		/// <summary>
		/// Binds the veteran image.
		/// </summary>
		/// <param name="migrantFarmWorkerImage">The veteran image.</param>
		private void BindMSFWVerifiedImage(Image migrantFarmWorkerImage)
		{
			migrantFarmWorkerImage.ToolTip = migrantFarmWorkerImage.AlternateText = CodeLocalise("MigrantFarmWorkerImage.ToolTip", "Verified as MSFW");
			migrantFarmWorkerImage.ImageUrl = UrlBuilder.MigrantFarmWorkerImage();
			migrantFarmWorkerImage.Visible = true;
		}

		/// <summary>
		/// Binds the blocked image.
		/// </summary>
		/// <param name="blockedImage">The veteran image.</param>
		/// <param name="isBlocked">Whether the person is blocked.</param>
		/// <param name="blockedReason"></param>
		private void BindBlockedImage(Image blockedImage, bool isBlocked, BlockedReason? blockedReason)
		{
			if (isBlocked)
			{
				blockedImage.ToolTip = blockedImage.AlternateText = CodeLocalise("BlockedImage.ToolTip", "Blocked");
				switch (blockedReason ?? BlockedReason.Unknown)
				{
					case BlockedReason.Unknown:
						blockedImage.ImageUrl = UrlBuilder.JobSeekerBlocked();
						blockedImage.ToolTip = blockedImage.AlternateText = CodeLocalise( "BlockedImage.ToolTip", "Blocked" );
						break;
					case BlockedReason.FailedPasswordReset:
						blockedImage.ImageUrl = UrlBuilder.UserBlockedFailedPasswordReset();
						blockedImage.ToolTip = blockedImage.AlternateText = CodeLocalise("BlockedFailedPasswordResetImage.ToolTip", "Blocked (Failed password reset)");
						break;
					case BlockedReason.AlienRegistrationDateExpired:
						blockedImage.ImageUrl = UrlBuilder.JobSeekerBlocked();
						blockedImage.ToolTip = blockedImage.AlternateText = CodeLocalise( "BlockedFailedPasswordResetImage.ToolTip", "Blocked (Alien registration date expired)" );
						break;
				}
				blockedImage.Visible = true;
			}
			else
			{
				blockedImage.Visible = false;
			}
		}

		/// <summary>
		/// Binds the inactive image.
		/// </summary>
		/// <param name="inactiveImage">The inactive image.</param>
		/// <param name="isEnabled">if set to <c>true</c> [is enabled].</param>
		private void BindInactiveImage(Image inactiveImage, bool isEnabled)
		{
			if (!isEnabled)
			{
				inactiveImage.ToolTip = inactiveImage.AlternateText = CodeLocalise("InactiveImage.ToolTip", "Inactive");
				inactiveImage.ImageUrl = UrlBuilder.JobSeekerInactive();
				inactiveImage.Visible = true;
			}
			else
			{
				inactiveImage.Visible = false;
			}
		}

		/// <summary>
		/// Binds the under-age jobseeker image.
		/// </summary>
		/// <param name="underageImage">The under-age jobseeker image.</param>
		/// <param name="dateOfBirth">The jobseeker's date of birth.</param>
		private void BindUnderageImage(Image underageImage, DateTime? dateOfBirth)
		{
			if (App.Settings.UnderAgeJobSeekerRestrictionThreshold <= 0 || !dateOfBirth.HasValue) return;
			var now = DateTime.Today;
			var age = now.Year - dateOfBirth.Value.Year;
			if (dateOfBirth.Value > now.AddYears(-age)) age--;

			if (age >= App.Settings.UnderAgeJobSeekerRestrictionThreshold) return;
			underageImage.Visible = true;
			underageImage.ToolTip = underageImage.AlternateText = CodeLocalise("Underage.Tooltip", "Under-age job seeker");
		}

		/// <summary>
		/// Builds the issue list.
		/// </summary>
		/// <param name="issues">The issues.</param>
		/// <returns></returns>
		private List<string> BuildIssueList(JobSeekerDashboardModel issues)
		{
			var issueList = new List<string>();

			if (issues.NotClickingOnLeadsTriggered.GetValueOrDefault() && App.Settings.NotClickingLeadsEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotClickingLeads, "#CANDIDATETYPE# not clicking on leads"));

			if (issues.NotReportingToInterviewTriggered.GetValueOrDefault() && App.Settings.NotReportingForInterviewEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotReportingForInterviews, "#CANDIDATETYPE# not reporting for interview"));

			if (issues.NotRespondingToEmployerInvitesTriggered.GetValueOrDefault() && App.Settings.NotRespondingToEmployerInvitesEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotRespondingToEmployerInvites, "#CANDIDATETYPE# not responding to #BUSINESS#:LOWER invitations"));

			if (issues.NotSearchingJobsTriggered.GetValueOrDefault() && App.Settings.NotSearchingJobsEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotSearchingJobs, "#CANDIDATETYPE# not searching jobs"));

			if (issues.NoLoginTriggered.GetValueOrDefault() && App.Settings.NotLoggingInEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotLoggingIn, "#CANDIDATETYPE# not signing in"));

			if (issues.JobOfferRejectionTriggered.GetValueOrDefault() && App.Settings.RejectingJobOffersEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.RejectingJobOffers, "#CANDIDATETYPE# rejecting job offers"));

			if (issues.PostingLowQualityResumeTriggered.GetValueOrDefault() && App.Settings.PostingLowQualityResumeEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.PoorQualityResume, "Posting poor-quality resume"));

			if (issues.MigrantSeasonalFarmWorkerTriggered && App.Settings.JobIssueFlagPotentialMSFW)
				issueList.Add(CodeLocalise(CandidateIssueType.PotentialMSFW, "Potential MSFW"));

			if (issues.FollowUpRequested.GetValueOrDefault() && App.Settings.FollowUpEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.RequiringFollowUp, "Requiring follow-up"));

			if (issues.ShowingLowQualityMatchesTriggered.GetValueOrDefault() && App.Settings.ShowingLowQualityMatchesEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.ShowingLowQualityMatches, "Showing low-quality matches"));

			if (issues.PostHireFollowUpTriggered.GetValueOrDefault() && App.Settings.PostHireFollowUpEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.SuggestingPostHireFollowUp, "Suggesting post-hire follow-up"));

			if (issues.InappropriateEmailAddress.GetValueOrDefault() && App.Settings.InappropriateEmailAddressCheckEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.InappropriateEmailAddress, "Inappropriate email address"));

			if (issues.GivingPositiveFeedback.GetValueOrDefault() && App.Settings.PositiveFeedbackCheckEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.GivingPositiveFeedback, "Giving positive survey feedback"));

			if (issues.GivingNegativeFeedback.GetValueOrDefault() && App.Settings.NegativeFeedbackCheckEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.GivingNegativeFeedback, "Giving negative survey feedback"));

			if (issues.ExpiredAlienCertification.GetValueOrDefault() && App.Settings.ExpiredAlienCertificationEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.ExpiredAlienCertification, "#CANDIDATETYPE# with expired alien registration"));

			return issueList;
		}

		/// <summary>
		/// Gets the selected job seeker id.
		/// </summary>
		/// <returns></returns>
		private List<JobSeekerView> GetSelectedJobSeekers()
		{
			return (from item in JobSeekersListView.Items
				let checkBox = (CheckBox)item.FindControl("SelectorCheckBox")
				let viewItem = JobSeekersListView.DataKeys[item.DisplayIndex]
				where checkBox.Checked
				select new JobSeekerView
				{
					PersonId = viewItem["Id"].AsLong(),
					FirstName = viewItem["FirstName"].ToString(),
					LastName = viewItem["LastName"].ToString(),
					MiddleInitial = viewItem["MiddleInitial"] == null ? string.Empty : viewItem["MiddleInitial"].ToString(),
					Blocked = viewItem["Blocked"].AsBoolean(),
					Enabled = viewItem["Enabled"].AsBoolean()
				}).ToList();
		}

		/// <summary>
		/// Deselects the jobseekers.
		/// </summary>
		private void DeselectJobseekers()
		{
            //var checkBoxes = from item in JobSeekersListView.Items
            //    let checkBox = (CheckBox)item.FindControl("SelectorCheckBox")
            //    where checkBox.Checked
            //    select checkBox;

			foreach (var checkBox in JobSeekersListView.Items.Select(item => (CheckBox)item.FindControl("SelectorCheckBox")).Where(control => control.Checked == true))
				checkBox.Checked = false;
		}

		/// <summary>
		/// Prepares the assign employer to staff action.
		/// </summary>
		private void PrepareAssignJobSeeker()
		{
			BindSubActionDropDownToStaff();
			SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignToStaffSubActionDropDownRequired.ErrorMessage", "Staff member required");
			ActivityAssigner.Visible = false;
			SubActionDropDown.Visible = SubActionButton.Visible = true;
		}

		private void PrepareAssignJobSeekerToStaffMember()
		{
			BindSubActionDropDownJobSeekerToStaff();
			SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignJobSeekerToStaffSubActionDropDownRequired.ErrorMessage", "Staff member required");
			ActivityAssigner.Visible = false;
			SubActionDropDown.Visible = SubActionButton.Visible = true;
		}

		/// <summary>
		/// Prepares the assign activity.
		/// </summary>
		private void PrepareAssignActivity()
		{
			SubActionDropDown.Visible = SubActionButton.Visible = false;
			ActivityAssigner.Visible = true;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FindButton.Text = CodeLocalise("Global.Go.Button", "Go");
			ClearButton.Text = CodeLocalise("Global.Clear.Button", "Clear");
			ExternalIdLabel.Text = App.Settings.IntegrationClient.IsIn(IntegrationClient.Mock,
				IntegrationClient.Standalone)
				? CodeLocalise("ExternalIdLabel.Text", "External seeker ID")
				: CodeLocalise("ProviderId.Text", "{0} ID", CodeLocalise(App.Settings.IntegrationClient));
			SSNLabel.Text = CodeLocalise("SSNLabel.Text", "SSN");
			PersonIdLabel.Text = CodeLocalise("PersonIdLabel.Text", "Focus ID");

			ActionLabel.Text = CodeLocalise("ActionLabel.Text", "Action");
			ActionGoButton.Text = CodeLocalise("Global.Go.Button", "Go");
      // ActionDropDownRequired.ErrorMessage = ActionDropDownRequiredErrorMessage; // SingleCheckboxErrorMessage;
		  CandidateActionDropDownValidator.ErrorMessage = ActionDropDownRequiredErrorMessage;
			SubActionButton.Text = CodeLocalise("SubActionButton.Button", "Assign");
			CreateNewJobSeekerButton.Text = CodeLocalise("CreateNewJobSeekerButton.Button", "Create new #CANDIDATETYPE#:LOWER account");
			RunActivityReportButton.Text = CodeLocalise("ActivityReportButton.Text", "Run activity report");
			UsernameLabel.Text = CodeLocalise("JobSeekerUsername.Label", "Username");

			if (!App.Settings.OfficesEnabled)
			{
				JobSeekerStatusLabel.Style["Width"] = "60px";
				JobSeekerStatusCell.Style["Width"] = "320px";
			}
		}

		/// <summary>
		/// Sets some localised message texts
		/// </summary>
		private void SetMessages()
		{
			DateErrorMessage = CodeLocalise("Date.ErrorMessage", "Valid date is required");
			DateOfBirthRequiredMessage = CodeLocalise("DateOfBirthRequired.ErrorMessage", "Date of birth is required");
			DOBMinLimitErrorMessage = CodeLocalise("DateOfBirthMin.ErrorMessage", "You must be at least 14 years of age.");
      DOBMaxLimitErrorMessage = CodeLocalise("DateOfBirthMax.ErrorMessage", "You must not be more than 99 years of age.");

      SingleCheckboxErrorMessage = HtmlLocalise("SingleCheckbox.Error.Text", "You must select a record to run this action");
      ActionDropDownRequiredErrorMessage = CodeLocalise("ActionDropDownRequired.ErrorMessage", "Action required");
			MultipleCheckboxErrorMessage = HtmlLocalise("MultipleCheckbox.Error.Text", "You can only select one record at a time to run this action");
			CloseJobSeekerWindowErrorMessage = HtmlLocalise("CloseJobSeekerWindow.Error.Text", "You can only have one job seeker window open at a time");
		}

		/// <summary>
		/// Builds a javascript array of actions supporting multiple selections
		/// </summary>
		protected void BuildValidMultiActionsArray()
		{
			var validMultiActions = new List<ActionTypes>
			{
				ActionTypes.AddCandidateToList,
				ActionTypes.AssignActivityToCandidate,
				ActionTypes.AssignJobSeekerToStaff,
				ActionTypes.EmailCandidate
			};
			var arrayList = string.Join(",", validMultiActions.Select(a => string.Concat("'", a.ToString(), "'")));
			var script = string.Format("var JobSeekers_ValidMultiActions = new Array({0});", arrayList);
			Page.ClientScript.RegisterClientScriptBlock(GetType(), "JobSeekers_ValidMultiActions", script, true);
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			RunActivityReportButton.Visible = App.User.IsInRole(Constants.RoleKeys.AssistStudentActivityReport);
		}

		/// <summary>
		/// Applies the branding.
		/// </summary>
		private void ApplyBranding()
		{
			JobSeekerSearchHeaderImage.ImageUrl = UrlBuilder.ActivityOffLeftImage();
			JobSeekerSearchPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			JobSeekerSearchPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}

		/// <summary>
		/// Sets the visibilty of controls that depend on the theme
		/// </summary>
		private void SetVisibility()
		{
			FindButton.Visible = true;

			SSNPlaceHolder.Visible = SSNLabelCell.Visible = LastNameEmptyCell.Visible = DateOfBirthSearch.Visible = PersonIdPanel.Visible = true;
			ExternalIdPanel.Visible = true;
			UsernameLabel.Visible = UsernameTextBox.Visible = !(App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist);
		}

		/// <summary>
		/// Runs the search.
		/// </summary>
		private int RunSearch()
		{
			int totalRows;

			if (!_reApplyFilter)
				SearchResultListPager.ReturnToFirstPage();

			UnbindSearch();

			if (_reApplyFilter)
				RebindOfficeFilter();

			BindJobSeekersList();
			SelectedAction = null;

			totalRows = SearchResultListPager.TotalRowCount;
			if (SearchResultListPager.TotalRowCount > 0)
				FormatJobSeekerListSortingImages();

			return totalRows;
		}

		/// <summary>
		/// If searching for a single SSN or External Id, and no job seekers are found, check with EKOS and show register job seeker modal if found
		/// </summary>
		protected void CheckExternalSystem()
		{
			if (JobSeekerCriteria.IsNotNull() && App.Settings.CheckExternalSystemOnSeekerSearch)
			{
				var ssn = JobSeekerCriteria.SsnList.IsNotNullOrEmpty() && JobSeekerCriteria.SsnList.Count == 1
					? JobSeekerCriteria.SsnList[0]
					: JobSeekerCriteria.Ssn;

				var externalId = JobSeekerCriteria.ExternalSeekerIdList.IsNotNullOrEmpty() && JobSeekerCriteria.ExternalSeekerIdList.Count == 1
					? JobSeekerCriteria.ExternalSeekerIdList[0]
					: JobSeekerCriteria.ExternalId;

				ValidatedUserView validatedJobSeeker = null;

				if (ssn.IsNotNullOrEmpty() && externalId.IsNullOrEmpty())
				{
					validatedJobSeeker = ServiceClientLocator.AuthenticationClient(App).ValidateExternalAuthenticationBySsn(ssn);
					if (validatedJobSeeker.IsNotNull())
						validatedJobSeeker.SocialSecurityNumber = ssn;
				}
				else if (ssn.IsNullOrEmpty() && externalId.IsNotNullOrEmpty())
				{
					validatedJobSeeker = ServiceClientLocator.AuthenticationClient(App).ValidateExternalAuthenticationByExternalId(externalId);
				}

				if (validatedJobSeeker.IsNotNull())
					RegisterJobSeeker.Show(validatedJobSeeker);
			}
		}

		/// <summary>
		/// Formats the Job order list sorting images.
		/// </summary>
		private void FormatJobSeekerListSortingImages()
		{
			#region Format images

			// Reset image URLs
			((ImageButton)JobSeekersListView.FindControl("NameSortAscButton")).ImageUrl =
				((ImageButton)JobSeekersListView.FindControl("LocationNameSortAscButton")).ImageUrl =
					((ImageButton)JobSeekersListView.FindControl("LastActivityDateSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)JobSeekersListView.FindControl("NameSortDescButton")).ImageUrl =
				((ImageButton)JobSeekersListView.FindControl("LocationNameSortDescButton")).ImageUrl =
					((ImageButton)JobSeekersListView.FindControl("LastActivityDateSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton)JobSeekersListView.FindControl("NameSortAscButton")).Enabled =
				((ImageButton)JobSeekersListView.FindControl("LocationNameSortAscButton")).Enabled =
					((ImageButton)JobSeekersListView.FindControl("LastActivityDateSortAscButton")).Enabled =
						((ImageButton)JobSeekersListView.FindControl("NameSortDescButton")).Enabled =
							((ImageButton)JobSeekersListView.FindControl("LocationNameSortDescButton")).Enabled =
								((ImageButton)JobSeekersListView.FindControl("LastActivityDateSortDescButton")).Enabled = true;

			if (JobSeekerCriteria.OrderBy.IsNotNullOrEmpty())
			{
				switch (JobSeekerCriteria.OrderBy.ToLower())
				{
					case Constants.SortOrders.JobSeekerNameAsc:
						((ImageButton)JobSeekersListView.FindControl("NameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)JobSeekersListView.FindControl("NameSortAscButton")).Enabled = false;
						break;
					case Constants.SortOrders.JobSeekerNameDesc:
						((ImageButton)JobSeekersListView.FindControl("NameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
						((ImageButton)JobSeekersListView.FindControl("NameSortDescButton")).Enabled = false;
						break;
					case Constants.SortOrders.LocationAsc:
						((ImageButton)JobSeekersListView.FindControl("LocationNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)JobSeekersListView.FindControl("LocationNameSortAscButton")).Enabled = false;
						break;
					case Constants.SortOrders.LocationDesc:
						((ImageButton)JobSeekersListView.FindControl("LocationNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
						((ImageButton)JobSeekersListView.FindControl("LocationNameSortDescButton")).Enabled = false;
						break;
					case Constants.SortOrders.LastActivityAsc:
						((ImageButton)JobSeekersListView.FindControl("LastActivityDateSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)JobSeekersListView.FindControl("LastActivityDateSortAscButton")).Enabled = false;
						break;
					case Constants.SortOrders.LastActivityDesc:
						((ImageButton)JobSeekersListView.FindControl("LastActivityDateSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
						((ImageButton)JobSeekersListView.FindControl("LastActivityDateSortDescButton")).Enabled = false;
						break;
					default:
						((ImageButton)JobSeekersListView.FindControl("NameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)JobSeekersListView.FindControl("NameSortAscButton")).Enabled = false;
						break;
				}
			}

			#endregion
		}

		/// <summary>
		/// Clears the action controls.
		/// </summary>
		private void ClearActionControls()
		{
			DeselectJobseekers();
			//ActionDropDown.SelectedValue = "";
		}

		/// <summary>
		/// Hides sub action controls
		/// </summary>
		private void HideSubActions()
		{
			ActivityAssigner.Visible = false;
			SubActionDropDown.Visible = SubActionButton.Visible = false;
			SelectedAction = ActionDropDown.Items[0].ValueToEnum<ActionTypes>();
			ActionDropDown.SelectedIndex = 0;
		}

		#endregion

		#region CascadingDropDown Update Job Seeker

		/// <summary>
		/// Activities the assignment_ activity selected.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The e.</param>
		protected void ActivityAssignment_ActivitySelected(object sender, ActivityAssignment.ActivitySelectedArgs e)
		{
			var activityId = e.SelectedActivityId;

			var jobSeekersSelected = SelectedJobSeekers = GetSelectedJobSeekers();

			if (jobSeekersSelected.Count == 0)
			{
				ShowErrors(ErrorTypes.NoCandidatesSelected);
				return;
			}

			bool activityStatus=ServiceClientLocator.CandidateClient(App).AssignActivityToMultipleUsers(activityId, jobSeekersSelected.ToList(), e.ActionedDate);

            if(activityStatus)
			Confirmation.Show(CodeLocalise("AssignActivityConfirmation.Title", "Activity/Service assigned successfully"),
				CodeLocalise("AssignActivityConfirmation.Body", "The selected activity/service has been assigned successfully"),
				CodeLocalise("CloseModal.Text", "OK"));
            else
                Confirmation.Show("Validation Error",
                "Staff does not have permission to perform this action",
                CodeLocalise("CloseModal.Text", "OK"));

			ClearActionControls();
			//HideSubActions();
		}

		#endregion

		#region Helper Method - Binding Issues Drop Down

		/// <summary>
		/// Binds the issues for candidate, depending on the Focus Theme.
		/// </summary>
		/// <param name="searchIssuesDropDown">The search issues drop down.</param>
		private void BindIssuesForCandidate(DropDownList searchIssuesDropDown)
		{
			searchIssuesDropDown.Items.Clear();
			searchIssuesDropDown.Items.Add(CodeLocalise("searchIssuesDropDown.WithAndWithoutIssues.NoEdit", "With and without issues"));
			searchIssuesDropDown.Items.Add(CodeLocalise("searchIssuesDropDown.WithAnyIssues.NoEdit", "With any issues"));
			searchIssuesDropDown.Items.Add(CodeLocalise("searchIssuesDropDown.WithNoIssues.NoEdit", "With no issues"));

			if (App.Settings.PositiveFeedbackCheckEnabled)
                searchIssuesDropDown.Items.AddEnum(CandidateIssueType.GivingPositiveFeedback, "Giving positive survey response");

			if (App.Settings.NegativeFeedbackCheckEnabled)
                searchIssuesDropDown.Items.AddEnum(CandidateIssueType.GivingNegativeFeedback, "Giving negative survey response");

			if (App.Settings.InappropriateEmailAddressCheckEnabled)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.InappropriateEmailAddress, "Inappropriate email address");

			if (App.Settings.NotClickingLeadsEnabled)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.NotClickingLeads, "#CANDIDATETYPE# not clicking on leads");

			if (App.Settings.NotReportingForInterviewEnabled && App.Settings.TalentModulePresent)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.NotReportingForInterviews, "#CANDIDATETYPE# not reporting for interview");

			if (App.Settings.NotRespondingToEmployerInvitesEnabled && App.Settings.TalentModulePresent)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.NotRespondingToEmployerInvites, "#CANDIDATETYPE# not responding to #BUSINESS#:LOWER invitations");

			if (App.Settings.NotSearchingJobsEnabled)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.NotSearchingJobs, "#CANDIDATETYPE# not searching for jobs");

			if (App.Settings.NotLoggingInEnabled)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.NotLoggingIn, "#CANDIDATETYPE# not signing in");

			if (App.Settings.RejectingJobOffersEnabled && App.Settings.TalentModulePresent)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.RejectingJobOffers, "#CANDIDATETYPE# rejecting job offers");

			if (App.Settings.PostingLowQualityResumeEnabled)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.PoorQualityResume, "Posting poor-quality resume");

			if (App.Settings.JobIssueFlagPotentialMSFW)
                searchIssuesDropDown.Items.AddEnum(CandidateIssueType.PotentialMSFW, "Potential MSFW job seekers");

			if (App.Settings.FollowUpEnabled)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.RequiringFollowUp, "Requiring follow-up");

			if (App.Settings.ShowingLowQualityMatchesEnabled)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.ShowingLowQualityMatches, "Showing low-quality matches");

			if (App.Settings.PostHireFollowUpEnabled && App.Settings.TalentModulePresent)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.SuggestingPostHireFollowUp, "Suggesting post-hire follow-up");

			if (App.Settings.ExpiredAlienCertificationEnabled && App.Settings.Theme == FocusThemes.Workforce)
				searchIssuesDropDown.Items.AddEnum(CandidateIssueType.ExpiredAlienCertification, "#CANDIDATETYPE# with expired alien registration");

			if (_reApplyFilter)
			{
				if (JobSeekerCriteria.JobSeekerGroupedIssueSearch == true) searchIssuesDropDown.SelectedIndex = 1;
				else if (JobSeekerCriteria.JobSeekerGroupedIssueSearch == false) searchIssuesDropDown.SelectedIndex = 2;
				else
				{
					searchIssuesDropDown.SelectedValue = JobSeekerCriteria.JobSeekerIssue.ToString();
				}
			}
			else
			{
				// Set default to be job seekers with issues triggered
				searchIssuesDropDown.SelectedIndex = 0;
			}
		}

		#endregion

		/// <summary>
		/// Returns all the office Ids related to a user
		/// </summary>
		/// <returns>A list of office Ids</returns>
		private List<long?> GetAllUserOfficeIds()
		{
			return ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { PersonId = Convert.ToInt64(App.User.PersonId) })
				.Select(x => x.Id)
				.ToList();
		}
	}
}
