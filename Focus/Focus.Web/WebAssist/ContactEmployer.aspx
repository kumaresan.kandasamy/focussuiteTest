﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContactEmployer.aspx.cs" Inherits="Focus.Web.WebAssist.ContactEmployer" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistEmployersNavigation.ascx" tagname="AssistEmployersNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/NotesAndRemindersModal.ascx" tagname="NotesAndRemindersModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
	  .ContactEmployerLabel {
	    float: left;
	    width: 150px;
	    margin-top: 8px;
	  }
	  .ContactEmployerField {
	    float: left;
	    margin-top: 8px;
	  }
	</style>
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigation1" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

	<table style="width:100%;" role="presentation">
		<tr>
			<td style="vertical-align:top;">
			  <uc:AssistEmployersNavigation ID="AssistEmployersNavigationMain" runat="server" />
			</td>
			<td style="vertical-align:top; text-align:right;">
			  <asp:Button ID="LogContactButton" runat="server" class="button1" onclick="LogContactButton_Clicked" CausesValidation="false"/>
			  <asp:Button ID="SendEmailButton" runat="server" class="button1" onclick="SendEmailButton_Clicked" ValidationGroup="Email" style="display:none" />
			</td>
		</tr>
		<tr>
			<td colspan="2" style="vertical-align:top;">
				<p class="pageNav"> 
				  <a href="<%=UrlBuilder.JobDevelopment()%>"><%= HtmlLocalise("JobDevelopment.LinkText", "return to #BUSINESS# Development")%></a>
				</p>
			</td>
		</tr>
  </table>
  
  <h1><asp:Literal ID="CandidateNameLiteral" runat="server"></asp:Literal></h1>

  <div class="ContactEmployerLabel">
    <%=HtmlLocalise("Employer.Literal", "#BUSINESS#")%>
  </div>
  <div class="ContactEmployerField">
    <asp:Literal runat="server" ID="EmployerNameLiteral"></asp:Literal>
  </div>
  <div style="clear:both"></div>
  
  <div class="ContactEmployerLabel">
    <%=HtmlLocalise("HiringManager.Literal", "Hiring Manager")%>
  </div>
  <div class="ContactEmployerField">
    <asp:DropDownList runat="server" ID="HiringManagersDropDown" AutoPostBack="True" OnSelectedIndexChanged="HiringManagersDropDown_OnSelectedIndexChanged"/>
    <asp:RequiredFieldValidator ID="HiringManagersDropDownValidator" runat="server" ControlToValidate="HiringManagersDropDown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="HiringManager" />
    <asp:RequiredFieldValidator ID="HiringManagersDropDownEmailValidator" runat="server" ControlToValidate="HiringManagersDropDown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Email" />
  </div>
  <div style="clear:both"></div>
  
  <div class="ContactEmployerLabel">
    <%=HtmlLocalise("Contact.Literal", "Contact")%>
  </div>
  <div class="ContactEmployerField">
    <asp:RadioButton runat="server" ID="TelephoneRadioButton" GroupName="Contact" Checked="True"/>
    <asp:Literal runat="server" ID="TelephoneNumberLiteral"></asp:Literal>
    <br />
    <asp:RadioButton runat="server" ID="EmailRadioButton" GroupName="Contact"/>
  </div>
  <div style="clear:both"></div>

  <asp:Panel runat="server" ID="EmailPanel" Style="display: none" Visible="False">
      <div class="ContactEmployerLabel">
        <%=HtmlLocalise("Subject.Literal", "Subject")%>
      </div>
      <div class="ContactEmployerField">
        <asp:TextBox runat="server" ID="EmailSubjectTextBox" style="width:350px"></asp:TextBox>
			  <asp:RequiredFieldValidator ID="EmailSubjectTextBoxValidator" runat="server" ControlToValidate="EmailSubjectTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Email" />
      </div>
      <div style="clear:both"></div>
      <br />
      <asp:TextBox runat="server" ID="EmailBodyTextBox" TextMode="MultiLine" style="width:500px" Rows="9" ValidationGroup="Email"></asp:TextBox>
			<asp:RequiredFieldValidator ID="EmailBodyTextBoxValidator" runat="server" ControlToValidate="EmailBodyTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Email" />
    <p>
      <asp:CheckBox runat="server" ID="AttachResumeCheckBox"/>
      <br />
      <asp:CheckBox runat="server" ID="AnonymiseResumeCheckBox"/>
    </p>
  </asp:Panel>
  
  <br />
  <p>
    <%=HtmlLocalise("AddNote.Label", "You may also add a note about this contact.")%>
  </p>
  <p>
    <asp:Button runat="server" ID="AddNoteButton" OnClick="AddNoteButton_Clicked" CssClass="button3" ValidationGroup="HiringManager"/>
  </p>
  
  <uc:NotesAndRemindersModal ID="NotesAndReminders" runat="server" />
  <uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />

  <script type="text/javascript">
    Sys.Application.add_load(ContactEmployer_PageLoad);
    
    function ContactEmployer_PageLoad() {
      var attachResumeBox = $("#<%=AttachResumeCheckBox.ClientID%>");
      
      if (attachResumeBox.length > 0) {
        ContactEmployer_AttachResumeCheckBox_Clicked(attachResumeBox);
        
        attachResumeBox.click(function () {
          ContactEmployer_AttachResumeCheckBox_Clicked($(this));
        });
      }
      
      ContactEmployer_ContactMethod();

      $("#<%=TelephoneRadioButton.ClientID%>").click(ContactEmployer_ContactMethod);
      $("#<%=EmailRadioButton.ClientID%>").click(ContactEmployer_ContactMethod);
    }

    function ContactEmployer_ContactMethod() {
      var emailPanel = $("#<%=EmailPanel.ClientID%>");
      var emailButton = $("#<%=SendEmailButton.ClientID%>");
      var logButton = $("#<%=LogContactButton.ClientID%>");

      if ($("#<%=EmailRadioButton.ClientID%>").is(":checked")) {
        emailPanel.show();
        emailButton.show();
        logButton.hide();
      } else {
        emailPanel.hide();
        emailButton.hide();
        logButton.show();
      }
    }

    function ContactEmployer_AttachResumeCheckBox_Clicked (sender) {
      var isDisabled = !(sender.is(":checked"));
      $("#<%=AnonymiseResumeCheckBox.ClientID %>")[0].disabled = isDisabled;
    }
  </script>
</asp:Content>
