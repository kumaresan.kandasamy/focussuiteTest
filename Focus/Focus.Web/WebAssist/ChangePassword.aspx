﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Focus.Web.WebAssist.ChangePassword" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register Src="Controls/AccountSettingsLinks.ascx" TagPrefix="uc" TagName="SettingsLinks" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:SettingsLinks ID="SettingLinksMain" runat="server" />

	<h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>

	<table width="100%" role="presentation">
		<tr>
			<td>
				<asp:button ID="ChangePasswordButton" runat="server" ValidationGroup="ChangePassword" CssClass="button2 right" OnKeyPress="ChangePasswordButton_Click" OnClick="ChangePasswordButton_Click" />
				<p><b><%= HtmlLocalise("ChangePassword.Label", "Change password")%></b> <%= HtmlRequiredFieldsInstruction() %></p>
			</td>	
		</tr>
	</table>
	
	<asp:PlaceHolder runat="server" ID="PasswordTextBoxesPlaceHolder" >
		<p><%= HtmlLocalise("ChangePasswordInstructions.Label", "Note: Passwords are case-sensitive.")%></p>

		<table role="presentation">
			<tr>
				<td width="155"><%= HtmlRequiredLabel(CurrentPasswordTextbox, "CurrentPassword.Label", "Current password")%></td>
				<td>
					<asp:Textbox ID="CurrentPasswordTextbox" TextMode="Password" runat="server" ClientIDMode="Static" MaxLength="20" autocomplete="off" />
					<asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPasswordTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="ChangePassword" />
				</td>
			</tr>
			<tr>
				<td><%= HtmlRequiredLabel(NewPasswordTextbox, "NewPassword.Label", "New password")%></td>
				<td>
					<asp:Textbox ID="NewPasswordTextbox" TextMode="Password" runat="server" ClientIDMode="Static" MaxLength="20" autocomplete="off" />
					<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPasswordTextbox" SetFocusOnError="true" CssClass="error" ValidationGroup="ChangePassword" />
					<asp:RegularExpressionValidator ID="NewPasswordRegEx" runat="server" ControlToValidate="NewPasswordTextBox" SetFocusOnError="true" Display="None" CssClass="error" ValidationGroup="ChangePassword" />
				</td>
			</tr>
			<tr>
				<td><%= HtmlRequiredLabel(ConfirmNewPasswordTextbox, "ConfirmNewPassword.Label", "Retype new password")%></td>
				<td>
					<asp:Textbox ID="ConfirmNewPasswordTextbox" TextMode="Password" runat="server" ClientIDMode="Static" MaxLength="20" autocomplete="off" />
					<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPasswordTextbox" SetFocusOnError="true" CssClass="error" ValidationGroup="ChangePassword" />
					<asp:RegularExpressionValidator ID="ConfirmNewPasswordRegEx" runat="server" ControlToValidate="ConfirmNewPasswordTextbox" SetFocusOnError="true" CssClass="error" ValidationGroup="ChangePassword" />
					<asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToValidate="ConfirmNewPasswordTextbox" ControlToCompare="NewPasswordTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="ChangePassword" />
				</td>
			</tr>
		</table>
		<p><asp:CustomValidator ID="ChangePasswordValidator" runat="server" CssClass="error" /></p>
	</asp:PlaceHolder>

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />

</asp:Content>
