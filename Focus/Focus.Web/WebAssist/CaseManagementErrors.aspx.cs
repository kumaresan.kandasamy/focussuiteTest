﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.CaseManagement;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Services.Messages;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistAccountAdministrator)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistSystemAdministrator)]
	public partial class CaseManagementErrors : PageBase
  {
    #region Properties

    private int _errorCount;
    private const string DefaultSearch = "DateSubmitted Desc";

    private CaseManagementCriteria ErrorCriteria
    {
      get { return GetViewStateValue<CaseManagementCriteria>("CaseManagementErrors:ErrorsCriteria"); }
      set { SetViewStateValue("CaseManagementErrors:ErrorsCriteria", value); }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

      Page.Title = CodeLocalise("Page.Title", "Case Management Errors");

      if (!App.Settings.EnableAssistErrorHandling)
      {
        Response.RedirectToRoute("default");
        return;
      }

      if (!Page.IsPostBack)
      {
        DateFromTextBox.Text = DateTime.Now.AddYears(-1).ToShortDateString();
        DateToTextBox.Text = DateTime.Now.ToShortDateString();
        Bind();
        BindDropdownList();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the ErrorList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void ErrorList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var messageView = (FailedIntegrationMessageDto) e.Item.DataItem;
      var itemButton = (Button) e.Item.FindControl("ItemButton");
      itemButton.CommandArgument = messageView.Id.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture);
      var integrationRequestMessage = new JavaScriptSerializer().Deserialize<IntegrationRequestMessage>(messageView.Request);
      long employerId;
      long jobSeekerId;
      long jobId;
      GetFocusIdFromMessage(integrationRequestMessage, out employerId, out jobSeekerId, out jobId);
      var integrationPoint = integrationRequestMessage.IntegrationPoint;
      switch (integrationPoint)
      {
        case IntegrationPoint.AssignAdminToEmployer:
        case IntegrationPoint.AssignEmployerActivity:
        case IntegrationPoint.SaveEmployer:
					itemButton.Text = CodeLocalise("ItemButton.EmployerText", "View #BUSINESS#");
          itemButton.Visible = employerId > 0;
          break;
        case IntegrationPoint.AssignAdminToJobSeeker:
        case IntegrationPoint.ReferJobSeeker:
        case IntegrationPoint.SaveJobMatches:
        case IntegrationPoint.SaveJobSeeker:
          itemButton.Text = CodeLocalise("ItemButton.JobSeekerText", "View Job Seeker");
          itemButton.Visible = jobSeekerId > 0;
          break;
        case IntegrationPoint.SaveJob:
        case IntegrationPoint.UpdateJobStatus:
          itemButton.Text = CodeLocalise("ItemButton.JobText", "View Job");
          itemButton.Visible = jobId > 0;
          break;
        default:
          itemButton.Visible = false;
          break;
      }

      var resendButton = (Button) e.Item.FindControl("ResendButton");
      resendButton.CommandArgument = messageView.Id.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture);
      resendButton.Text = CodeLocalise("ResendButton.Text", "Resend");

			var requestDetailsButton = (Button)e.Item.FindControl("RequestDetailsButton");
			requestDetailsButton.CommandArgument = messageView.Id.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture);
			requestDetailsButton.Text = CodeLocalise("RequestDetailsButton.Text", "View Messages");

      var ignoreButton = (Button) e.Item.FindControl("IgnoreButton");
      ignoreButton.CommandArgument = messageView.Id.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture);
      ignoreButton.Text = CodeLocalise("IgnoreButton.Text", "Ignore");

      var dateLabel = (Label) e.Item.FindControl("DateLabel");
      dateLabel.Text =
        string.Concat(messageView.DateSubmitted.ToString(CultureInfo.CurrentUICulture.DateTimeFormat.ShortDatePattern),
          " ", messageView.DateSubmitted.ToString(CultureInfo.CurrentUICulture.DateTimeFormat.ShortTimePattern));

      var messageTypeLabel = (Label) e.Item.FindControl("MessageTypeLabel");
      messageTypeLabel.Text = messageView.MessageType;

      var descriptionLabel = (Label) e.Item.FindControl("DescriptionLabel");
      descriptionLabel.Text = messageView.ErrorDescription;

      var idLabel = (Label) e.Item.FindControl("IdLabel");
      idLabel.Text = messageView.Id.ToString();
    }

    /// <summary>
    /// Handles the LayoutCreated event of the ErrorList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ErrorList_LayoutCreated(object sender, EventArgs e)
    {
      // Set table headings
      ((Literal) ErrorList.FindControl("IdHeader")).Text = CodeLocalise("IdHeader.Text", "Id");
      ((Literal) ErrorList.FindControl("DateHeader")).Text = CodeLocalise("DateHeader.Text", "Date submitted");
      ((Literal) ErrorList.FindControl("MessageTypeHeader")).Text = CodeLocalise("MessageTypeHeader.Text",
        "Message type");
      ((Literal) ErrorList.FindControl("DescriptionHeader")).Text = CodeLocalise("DescriptionHeader.Text",
        "Error description");
      ((Literal) ErrorList.FindControl("ActionsHeader")).Text = CodeLocalise("ActionsHeader.Text", "Next action");
    }

    /// <summary>
    /// Handles the Sorting event of the ErrorList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
    protected void ErrorList_Sorting(object sender, ListViewSortEventArgs e)
    {
      ErrorCriteria.OrderBy = e.SortExpression;
      FormatErrorListSortingImages();
    }

    /// <summary>
    /// Handles the Selecting event of the ErrorsDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void ErrorsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      if (ErrorCriteria.IsNull())
      {
	      UnbindSearch();
      }

      e.InputParameters["criteria"] = ErrorCriteria;
    }

    /// <summary>
    /// Fires when the page has changed
    /// </summary>
    /// <param name="o">Pager control</param>
    /// <param name="e">Page event arguments</param>
    protected void SearchResultListPager_OnDataPaged(object o, Pager.DataPagedEventArgs e)
    {

    }
    
    protected void RequestDetailsButton_OnCommand(object sender, CommandEventArgs e)
    {
			var id = long.Parse(e.CommandArgument.ToString());
			ViewCaseManagementErrorModal.Show(id);
    }

    /// <summary>
    /// Resends the message
    /// </summary>
    /// <param name="sender">The button raising the request</param>
    /// <param name="e">The command arguments</param>
    protected void ResendButton_OnCommand(object sender, CommandEventArgs e)
    {
      var id = long.Parse(e.CommandArgument.ToString());
      var success = ServiceClientLocator.CoreClient(App).ResendCaseManagementMessage(id);

      var title = (success)
        ? HtmlLocalise("CommandConfirmationModal.ResendSuccess.Body", "Error resent")
        : HtmlLocalise("CommandConfirmationModal.ResendFailure.Body", "Problem resending error");

      var body = (success)
        ? HtmlLocalise("CommandConfirmationModal.ResendSuccess.Body",
          "Your error has been resent. Please allow a few minutes for processing")
        : HtmlLocalise("CommandConfirmationModal.ResendFailure.Body",
          "There was a problem resending the error. Please try again later");

      Confirmation.Show(title, body, CodeLocalise("Global.OK", "OK"), height: 100);
      Bind(success);
    }

    /// <summary>
    /// Ignores the message
    /// </summary>
    /// <param name="sender">The button raising the request</param>
    /// <param name="e">The command arguments</param>
    protected void IgnoreButton_OnCommand(object sender, CommandEventArgs e)
    {
      var id = long.Parse(e.CommandArgument.ToString());
      var success = ServiceClientLocator.CoreClient(App).IgnoreCaseManagementMessage(id);

      var title = (success)
        ? HtmlLocalise("CommandConfirmationModal.IgnoreSuccess.Body", "Error ignored")
        : HtmlLocalise("CommandConfirmationModal.IgnoreFailure.Body", "Problem ignoring error");

      var body = (success)
        ? HtmlLocalise("CommandConfirmationModal.IgnoreSuccess.Body", "Your error has been marked as ignored")
        : HtmlLocalise("CommandConfirmationModal.IgnoreFailure.Body", "The record has already been marked as ignored");

      Confirmation.Show(title, body, CodeLocalise("Global.OK", "OK"), height: 100);
      Bind(success);
    }

    /// <summary>
    /// Gets the Errors.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="startRowIndex">Start index of the row.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <returns></returns>
    public List<FailedIntegrationMessageDto> GetErrors(CaseManagementCriteria criteria, string orderBy,
      int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int) Math.Floor((double) startRowIndex/maximumRows));

      criteria.PageSize = maximumRows;
      criteria.PageIndex = pageIndex;
			if (criteria.OrderBy.IsNullOrEmpty())
				criteria.OrderBy = orderBy;
	    if (criteria.OrderBy.IsNullOrEmpty())
		    criteria.OrderBy = DefaultSearch;

      var errors = ServiceClientLocator.CoreClient(App).GetCaseManagementErrorsPagedList(criteria);
      _errorCount = errors.TotalCount;

      return errors;
    }

    /// <summary>
    /// Gets the error count.
    /// </summary>
    /// <returns></returns>
    public int GetErrorCount()
    {
      return _errorCount;
    }

    /// <summary>
    /// Gets the error count.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public int GetErrorCount(CaseManagementCriteria criteria)
    {
      return _errorCount;
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    /// <param name="adjustPage">Check if the remaining item has just been removed from the page</param>
    private void Bind(bool adjustPage = false)
    {
      if (adjustPage)
      {
        if ((SearchResultListPager.CurrentPage - 1)*SearchResultListPager.PageSize + 1 == SearchResultListPager.TotalRowCount)
          SearchResultListPager.ReturnToFirstPage();
      }

      ErrorList.DataBind();
      ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} results found",
        SearchResultListPager.TotalRowCount.ToString(CultureInfo.CurrentUICulture));

      if (SearchResultListPager.TotalRowCount > 0)
      {
        SearchResultListPager.Visible = true;
        FormatErrorListSortingImages();
      }
      else
        SearchResultListPager.Visible = false;

  }

  /// <summary>
    /// Formats the error list sorting images.
    /// </summary>
    private void FormatErrorListSortingImages()
    {
      var buttonNames = new Dictionary<string, string>
      {
        { "DateSubmitted Asc", "DateAscSortButton" },
        { "DateSubmitted Desc", "DateDescSortButton" },
        { "MessageType Asc", "MessageTypeAscSortButton" },
        { "MessageType Desc", "MessageTypeDescSortButton" },
        { "ErrorDescription Asc", "DescriptionAscSortButton" },
        { "ErrorDescription Desc", "DescriptionDescSortButton" },
      };

      var imageButtons = buttonNames.Values.ToDictionary(
        buttonName => buttonName,
        buttonName => (ImageButton)ErrorList.FindControl(buttonName));

      foreach (var imageButton in imageButtons.Values)
      {
        imageButton.Enabled = true;
        imageButton.ImageUrl = imageButton.ID.EndsWith("AscSortButton", StringComparison.OrdinalIgnoreCase)
          ? UrlBuilder.CategorySortAscImage()
          : UrlBuilder.CategorySortDescImage();
      }

      var orderBy = ErrorCriteria.OrderBy.IsNotNullOrEmpty() ? ErrorCriteria.OrderBy : DefaultSearch;
      var sortedButton = imageButtons[buttonNames[orderBy]];

      sortedButton.Enabled = false;
      sortedButton.ImageUrl = sortedButton.ID.EndsWith("AscSortButton", StringComparison.OrdinalIgnoreCase)
        ? UrlBuilder.CategorySortedAscImage()
        : UrlBuilder.CategorySortedDescImage();
    }

    protected void ItemButton_OnCommand(object sender, CommandEventArgs e)
    {
      var id = long.Parse(e.CommandArgument.ToString());
      var error = ServiceClientLocator.CoreClient(App).GetCaseManagementError(id);
      var integrationRequestMessage = new JavaScriptSerializer().Deserialize<IntegrationRequestMessage>(error.Request);
      long employerId;
      long jobSeekerId;
      long jobId;
      GetFocusIdFromMessage(integrationRequestMessage, out employerId, out jobSeekerId, out jobId);

	    if (employerId > 0)
	    {
		    var businessUnit = ServiceClientLocator.EmployerClient(App).GetPreferredBusinessUnit(employerId);
		    if (businessUnit.IsNull())
			    businessUnit = ServiceClientLocator.EmployerClient(App).GetBusinessUnits(employerId).First();

		    Response.Redirect(UrlBuilder.AssistEmployerProfile(businessUnit.Id.GetValueOrDefault(0)), true);
	    }

	    if (jobSeekerId > 0)
        Response.Redirect(UrlBuilder.AssistJobSeekerProfile(jobSeekerId), true);

      if (jobId > 0)
        Response.Redirect(UrlBuilder.AssistJobView(jobId), true);
    }

    private static void GetFocusIdFromMessage(IntegrationRequestMessage integrationRequestMessage, out long employerId, out long jobSeekerId, out long jobId)
    {
      var integrationPoint = integrationRequestMessage.IntegrationPoint;
      employerId = 0;
      jobSeekerId = 0;
      jobId = 0;
      switch (integrationPoint)
      {
        // open the employer in assist
        case IntegrationPoint.AssignAdminToEmployer:
          long tmp = 0;
          if (long.TryParse(integrationRequestMessage.AssignAdminToEmployerRequest.EmployerId, out tmp))
            employerId = tmp;
          break;
        case IntegrationPoint.AssignEmployerActivity:
          if (long.TryParse(integrationRequestMessage.AssignEmployerActivityRequest.EmployerId, out tmp))
            employerId = tmp;
          break;
        case IntegrationPoint.SaveEmployer:
          employerId = integrationRequestMessage.SaveEmployerRequest.EmployerId;
          break;
        // open the job seeker in assist
        case IntegrationPoint.AssignAdminToJobSeeker:
          if (long.TryParse(integrationRequestMessage.AssignAdminToJobSeekerRequest.JobSeekerId, out tmp))
            jobSeekerId = tmp;
          break;
        case IntegrationPoint.ReferJobSeeker:
          jobSeekerId = integrationRequestMessage.ReferJobSeekerRequest.PersonId.GetValueOrDefault(0);
          break;
        case IntegrationPoint.SaveJobSeeker:
          jobSeekerId = integrationRequestMessage.SaveJobSeekerRequest.PersonId;
          break;
        case IntegrationPoint.SaveJobMatches:
          jobSeekerId = integrationRequestMessage.SaveJobMatchesRequest.JobSeekerId;
          break;
        // open the job in assist
        case IntegrationPoint.SaveJob:
          jobId = integrationRequestMessage.SaveJobRequest.JobId;
          break;
        case IntegrationPoint.UpdateJobStatus:
          if (long.TryParse(integrationRequestMessage.UpdateJobStatusRequest.JobId, out tmp))
            jobId = tmp;
          break;
      }
    }

    private void BindDropdownList()
    {
      MessageTypeDropDownList.Items.Clear();

      MessageTypeDropDownList.Items.AddLocalisedTopDefault("All.Text", "All");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.None, "None");
			MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.AssignAdminToEmployer, "Assign Admin to #BUSINESS#");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.AssignAdminToJobSeeker, "Assign Admin to Job Seeker");
			MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.AssignEmployerActivity, "Assign #BUSINESS# Activity");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.AssignJobSeekerActivity, "Assign Job Seeker Activity");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.AuthenticateJobSeeker, "Authenticate Job Seeker");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.ChangePassword, "Change Password");
			MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.GetEmployer, "Get #BUSINESS#");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.GetJob, "Get Job");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.GetJobSeeker, "Get Job Seeker");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.GetStaff, "Get Staff");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.IsUIClaimant, "Is UI Claimant");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.JobSeekerLogin, "Job Seeker Login");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.LogAction, "Log Action");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.SaveEmployee, "Save Employee");
			MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.SaveEmployer, "Save #BUSINESS#");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.SaveJob, "Save Job");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.SaveJobMatches, "Save Job Matches");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.SaveJobSeeker, "Save Job Seeker");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.ReferJobSeeker, "Refer Job Seeker");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.RefreshJobSeeker, "Refresh Job Seeker");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.UpdateEmployeeStatus, "Update Employee Status");
      MessageTypeDropDownList.Items.AddEnum(IntegrationPoint.UpdateJobStatus, "Update Job Status");

      ActionTypeDropDownList.Items.Clear();
      ActionTypeDropDownList.Items.AddLocalisedTopDefault("All.Text", "All");
      ActionTypeDropDownList.Items.Add(new ListItem(CodeLocalise("Ignored.Text", "Ignore"),"1"));
      ActionTypeDropDownList.Items.Add(new ListItem(CodeLocalise("Pending.Text", "Pending"),"2"));
      ActionTypeDropDownList.Items.Add(new ListItem(CodeLocalise("Resent.Text", "Resent"),"3"));
    }

    protected void FilterButton_Clicked(object sender, EventArgs e)
    {
      SearchResultListPager.ReturnToFirstPage();
      UnbindSearch();
      Bind();
    
    }

    private void UnbindSearch()
    {
      DateTime dateFrom;
      DateTime.TryParse(DateFromTextBox.Text, out dateFrom);
      
      DateTime dateTo;
      DateTime.TryParse(DateToTextBox.Text, out dateTo);

      ErrorCriteria = new CaseManagementCriteria
      {
        ErrorDescription = ErrorDescriptionTextbox.Text.IsNotNullOrEmpty() ? ErrorDescriptionTextbox.Text : null,
        MessageType = MessageTypeDropDownList.SelectedValue.IsNotNullOrEmpty() ? MessageTypeDropDownList.SelectedValueToEnum<IntegrationPoint>() : (IntegrationPoint?)null,
        IncludeIgnored = ActionTypeDropDownList.SelectedValue.IsNotNullOrEmpty() && ActionTypeDropDownList.SelectedValue == "1",
        Pending = ActionTypeDropDownList.SelectedValue.IsNotNullOrEmpty() && ActionTypeDropDownList.SelectedValue == "2",
        Resent = ActionTypeDropDownList.SelectedValue.IsNotNullOrEmpty() && ActionTypeDropDownList.SelectedValue == "3",
        DateSubmittedFrom = dateFrom,
        DateSubmittedTo = dateTo.AddDays(1).AddMinutes(-1)
      };
    }
  }
 }
