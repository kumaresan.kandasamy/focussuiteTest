﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Framework.Core;


#endregion

namespace Focus.Web.WebAssist
{
	public partial class JobPosting : PageBase
	{
		#region Page Properties

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

			var jobSeekerId = (Page.RouteData.Values["jobseekerid"].IsNotNull())
				? Page.RouteData.Values["jobseekerid"].ToString()
				: "";

			long jobSeekerPersonId = 0;

			if (jobSeekerId.IsNotNullOrEmpty())
				long.TryParse(jobSeekerId, out jobSeekerPersonId);

			var page = Page.Request.QueryString["page"] ?? "1";

			JobPostingDetails.CurrentPageNumber = Convert.ToInt32(page.IsNotNullOrEmpty() ? "0" : page);
			JobPostingDetails.JobseekerId = jobSeekerPersonId;
		}
	}
}
