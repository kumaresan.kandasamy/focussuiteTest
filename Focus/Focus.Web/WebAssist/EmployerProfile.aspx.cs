﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.BusinessUnit;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.Employee;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Core.Models;
using Focus.Web.WebAssist.Controls;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
	public partial class EmployerProfile : PageBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets the business unit id.
		/// </summary>
		private long BusinessUnitId { get; set; }

		private long EmployerId { get; set; }

		private int _employeesCount;
		private int _referralsCount;
    private bool _isAssistPage;
    private bool _jobPostingFilterApplied;

	  private EmployeeCriteria EmployeeCriteria
		{
			get { return GetViewStateValue<EmployeeCriteria>("ListHiringManagers:EmployeeCriteria"); }
			set { SetViewStateValue("ListHiringManagers:EmployeeCriteria", value); }
		}

		internal BusinessUnitProfileModel BusinessUnitProfileModel
		{
			get { return GetViewStateValue<BusinessUnitProfileModel>("BusinessUnitProfile:BusinessUnitProfileModel"); }
			set { SetViewStateValue("BusinessUnitProfile:BusinessUnitProfileModel", value); }
		}

		private ApplicationStatusLogViewCriteria ApplicationStatusLogViewCriteria
		{
			get { return GetViewStateValue<ApplicationStatusLogViewCriteria>("ListReferralSummary:ApplicationStatusLogViewCriteria"); }
			set { SetViewStateValue("ListReferralSummary:ApplicationStatusLogViewCriteria", value); }
		}

		private bool DefaultOfficeRemoved
		{
			get { return GetViewStateValue<bool>("BusinessUnitProfile:DefaultOfficeRemoved"); }
			set { SetViewStateValue("BusinessUnitProfile:DefaultOfficeRemoved", value); }
		}

		private string ReturnUrl
		{
			get { return GetViewStateValue<string>("EmployerProfile:ReturnUrl"); }
			set { SetViewStateValue("EmployerProfile:ReturnUrl", value);}
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			this.Page.Title = CodeLocalise("Page.Title", "#BUSINESS# Profile");

			_isAssistPage = Page.Is(UrlBuilder.AssistEmployerProfileForCompareOnly());

            if (Request.QueryString["filtered"] != null)
            {
                _jobPostingFilterApplied = Convert.ToBoolean(Request.QueryString["filtered"]);
                this.Page.Title = this.Page.Title+ " - filtered";
            }

			if (Page.RouteData.Values.ContainsKey("employerid"))
			{
				long id;
				Int64.TryParse(Page.RouteData.Values["employerid"].ToString(), out id);
				
				BusinessUnitId = id;
				EmployerId = ServiceClientLocator.EmployerClient(App).GetBusinessUnit(BusinessUnitId).EmployerId;
                this.Page.Title = string.Format("{0} - {1}-{2}", this.Page.Title, EmployerId, BusinessUnitId); 

			}

      if (!Page.IsPostBack)
			{
				SetReturnUrl();

				GetBusinessUnitProfileData();
				PopulateEmployerProfileData();
				BindReturnUrl();
				BindReferralTimeSpanDropDown();
				BindReferralOrderStatusDropDown();
				BindReferralStatusFilterDropDown();
				AssignedOffices.ContextId = EmployerId;
				AssignedOffices.Bind();

				EmployerActivityList.BusinessUnitId = BusinessUnitId;

				EmployerJobSeekerPlacements.BusinessUnitId = BusinessUnitId;
				
        NotesReminders.EntityId = BusinessUnitId;
        NotesReminders.EntityType = EntityTypes.BusinessUnit;
        NotesReminders.ReadOnly = (!App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator));
        NotesReminders.Bind();

				LocaliseUI();

				ApplyBranding();

				HideOfficeRow();

				EditFEINButton.Visible = App.Settings.CanEditFEIN && App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
				ReplaceParentButton.Visible = ( BusinessUnitProfileModel.BusinessUnit.IsPrimary && (ServiceClientLocator.EmployerClient(App).GetBusinessUnits(EmployerId).Count() > 1) && !App.User.IsInRole(Constants.RoleKeys.AssistEmployersReadOnly) );
			}
		}

		/// <summary>
		/// Handles the Click event of the lnkReturn control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void lnkReturn_Click(object sender, EventArgs e)
		{
			var returnUrl = UseReturnUrl();

		  if (_isAssistPage && returnUrl.IsNotNullOrEmpty())
		  {
		    if (_jobPostingFilterApplied && !returnUrl.Contains("filtered="))
		    {
		      Response.Redirect(returnUrl + "?filtered=true");
		    }
		    else
		    {
		      Response.Redirect(returnUrl);
		    }
		  }
		  else
		  {
		    Response.Redirect(Page.RouteData.Values.ContainsKey("session")
		      ? UrlBuilder.ReportingSpecific("employer", Page.RouteData.Values["session"].ToString())
		      : UrlBuilder.ReportingSpecific("employer"));
		  }
		}

		/// <summary>
		/// Binds the return URL.
		/// </summary>
		private void BindReturnUrl()
		{
			var returnUrl = GetReturnUrl();
			ReturnUrl = returnUrl;

      if (returnUrl.Contains("joborderdashboard"))
				lnkReturn.Text = CodeLocalise("ReturnToJobOrderDashboard", "return to #EMPLOYMENTTYPE#:LOWER dashboard");

			else if (returnUrl.Contains("jobdevelopment") || returnUrl.Contains("businessdevelopment"))
				lnkReturn.Text = CodeLocalise("ReturnToJobDevelopment", "return to #BUSINESS# development");

      else if (returnUrl.Contains("employers"))
	      lnkReturn.Text = CodeLocalise("ReturnToEmployers", App.Settings.Theme == FocusThemes.Workforce ? "return to Find a #BUSINESS#:LOWER account" : "return to Find an #BUSINESS#:LOWER account");

      else if (returnUrl.Contains("/employer/referral/") || returnUrl.Contains("/hiringmanager/referral/") || returnUrl.Contains("/businessunit/referral/"))
				lnkReturn.Text = CodeLocalise("ReturnToEmployerReferral", "return to #BUSINESS#:LOWER account request");

			else if (ReturnUrl.Contains("employerprofile"))
				lnkReturn.Text = CodeLocalise("ReturnToEmployerProfile", "return to #BUSINESS#:LOWER profile");

      else
        lnkReturn.Text = CodeLocalise("ReturnToReport", "Return to Report");
		}
		
		/// <summary>
		/// Gets the employer profile data.
		/// </summary>
		private void GetBusinessUnitProfileData()
		{
			BusinessUnitProfileModel = ServiceClientLocator.EmployerClient(App).GetBusinessUnitProfile(BusinessUnitId);
            this.Page.Title = this.Page.Title + " - " + BusinessUnitProfileModel.BusinessUnit.Name;
		}

		public List<BusinessUnitLinkedCompanyDto> GetLinkedCompanies(string orderBy, long businessUnitId)
		{
			var criteria = new BusinessUnitLinkedCompanyCriteria
			{
				OrderBy = orderBy.IsNotNullOrEmpty() ? orderBy : Constants.SortOrders.BusinessUnitNameAsc,
				BusinessUnitId = businessUnitId
			};
			var linkedCompanies = ServiceClientLocator.EmployerClient(App).GetBusinessUnitLinkedCompanies(criteria);
			return linkedCompanies;
		}

		/// <summary>
		/// Populates the employer profile data.
		/// </summary>
		private void PopulateEmployerProfileData()
		{
			#region Populate Contact Information

			var sb = new StringBuilder();

			var businessUnit = BusinessUnitProfileModel.BusinessUnit;

			lblCompanyNameTitle.Text = businessUnit.Name;

			if (businessUnit != null)
			{
        foreach (var businessUnitAddress in BusinessUnitProfileModel.BusinessUnitAddresses)
        {
          if (App.Settings.Theme == FocusThemes.Workforce)
          {
            sb.Append(CodeLocalise("LegalName.Label", "<strong>Business legal name:</strong> "));
            sb.Append(businessUnit.LegalName);
            sb.Append("<br />");
            sb.Append(CodeLocalise("DoingBusinessAs.Label", "<strong>Doing business as:</strong> "));
          }
          sb.Append(businessUnit.Name);
					sb.Append("<br/>").Append(businessUnitAddress.Line1);
					if (businessUnitAddress.Line2.IsNotNullOrEmpty())
						sb.Append("<br/>").Append(businessUnitAddress.Line2);
					if (businessUnitAddress.Line3.IsNotNullOrEmpty())
						sb.Append("<br/>").Append(businessUnitAddress.Line3);
					sb.Append("<br/>").Append(businessUnitAddress.TownCity).Append(", ").Append(businessUnitAddress.PostcodeZip).Append("<br/>");
				}

				lblContactDetailsAddress.Text = sb.ToString();	

				sb = new StringBuilder();

				if (businessUnit.PrimaryPhone.IsNotNullOrEmpty())
				{
					sb.Append("<br/>").Append(businessUnit.PrimaryPhoneType).Append(": ").Append(Regex.Replace(businessUnit.PrimaryPhone, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
					if (businessUnit.PrimaryPhoneExtension.IsNotNullOrEmpty())
						sb.Append(" (ext ").Append(businessUnit.PrimaryPhoneExtension + ")");
					if (businessUnit.AlternatePhone1.IsNotNullOrEmpty())
						sb.Append("<br/>").Append(businessUnit.AlternatePhone1Type).Append(": ").Append(Regex.Replace(businessUnit.AlternatePhone1, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
					if (businessUnit.AlternatePhone2.IsNotNullOrEmpty())
						sb.Append("<br/>").Append(businessUnit.AlternatePhone2Type).Append(": ").Append(Regex.Replace(businessUnit.AlternatePhone2, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
				}

				lblContactDetailsPhone.Text = sb.ToString();

				if (App.Settings.Theme == FocusThemes.Workforce)
					imgIsParentCompany.Visible = businessUnit.IsPrimary;				

				if (BusinessUnitProfileModel.LinkedCompanies != null)
				{
					LinkedCompaniesListView.DataSource = BusinessUnitProfileModel.LinkedCompanies;
					LinkedCompaniesListView.DataBind();
				}
			}

			#endregion

			lblNoOfTalentLogins7Days.Text = BusinessUnitProfileModel.Logins7Days.ToString();
			lblTimeSinceLastLogin.Text =TimeSince(BusinessUnitProfileModel.LastLogin) + "(" + BusinessUnitProfileModel.LastLoginEmail + ")";
			
			#region Activity Summary Labels

			lblNoOfJobOrdersOpen7Days.Text = BusinessUnitProfileModel.NoOfJobOrdersOpen7Days.ToString();
			lblNoOfJobOrdersOpen30days.Text = BusinessUnitProfileModel.NoOfJobOrdersOpen30Days.ToString();
			lblNoOfJobOrdersEditedByStaff7days.Text = BusinessUnitProfileModel.NoOfOrdersEditedByStaff30Days.ToString();
			lblNoOfJobOrdersEditedByStaff30days.Text = BusinessUnitProfileModel.NoOfOrdersEditedByStaff30Days.ToString();
			lblNoOfResumesReferred7days.Text = BusinessUnitProfileModel.NoOfResumesReferred7Days.ToString();
			lblNoOfResumesReferred30days.Text = BusinessUnitProfileModel.NoOfResumesReferred30Days.ToString();
			lblNoOfJobSeekersInterviewed7days.Text = BusinessUnitProfileModel.NoOfJobSeekersInterviewed7Days.ToString();
			lblNoOfJobSeekersInterviewed30days.Text = BusinessUnitProfileModel.NoOfJobSeekersInterviewed30Days.ToString();
			lblNoOfJobSeekersRejected7days.Text = BusinessUnitProfileModel.NoOfJobSeekersRejected7Days.ToString();
			lblNoOfJobSeekersRejected30days.Text = BusinessUnitProfileModel.NoOfJobSeekersRejected30Days.ToString();
			lblNoOfJobSeekersHired7days.Text = BusinessUnitProfileModel.NoOfJobSeekersHired7Days.ToString();
			lblNoOfJobSeekersHired30days.Text = BusinessUnitProfileModel.NoOfJobSeekersHired30Days.ToString();

			#endregion
		}

		/// <summary>
		/// Calculates the time since a date
		/// </summary>
		/// <param name="actionTime">The action time.</param>
		/// <returns></returns>
		private string TimeSince(DateTime? actionTime)
		{
			if (actionTime.GetValueOrDefault() == DateTime.MinValue) return String.Empty;

			var currentDateTime = DateTime.Now;
			var timeSince = currentDateTime.Subtract(actionTime.GetValueOrDefault());
			return timeSince.Days + " " + CodeLocalise("Days", "days") + " " + timeSince.Hours + " " +
						 CodeLocalise("Hours", "hours") + " " + timeSince.Minutes + " " + CodeLocalise("Minutes", "minutes");
		}
		
		#region LoginsPanelData Methods

		/// <summary>
		/// Handles the DataBound event of the ItemsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void HiringManagersList_DataBound(object sender, ListViewItemEventArgs e)
		{
			var hiringAreas = ServiceClientLocator.EmployeeClient(App).GetHiringAreas(((EmployeeSearchResultView) e.Item.DataItem).Id);
			var sb = new StringBuilder();

			if (hiringAreas.Count < 6)
			{
				foreach (var hiringArea in hiringAreas)
					sb.Append(hiringArea + "<br/>");
			}
			else
			{
				for (var i = 0; i < 4; i++)
					sb.Append(hiringAreas[i] + "<br/>");
				
				var sb2 = new StringBuilder();
				for (var i = 4; i < hiringAreas.Count; i++)
					sb2.Append(hiringAreas[i] + "<br/>");

				const string html = @"<span class=""tooltip helpMessage""><span class=""tooltipImage toolTipNew"" title=""{1}"">+ {0} others</span></span>";
				
				sb.Append(String.Format(html, hiringAreas.Count - 4, sb2));
				
			}

      var url = _isAssistPage
                  ? UrlBuilder.AssistHiringManagerProfile(((EmployeeSearchResultView) e.Item.DataItem).EmployeeBusinessUnitId)
                  : String.Format(UrlBuilder.ReportingHiringManagerProfile(Page.RouteData.Values["session"].ToString()), ((EmployeeSearchResultView)e.Item.DataItem).EmployeeBusinessUnitId);

			((Literal)e.Item.FindControl("HiringAreas")).Text = sb.ToString();
			((Literal)e.Item.FindControl("HiringManagerLink")).Text = String.Format("<a href=\"{0}\">{1} {2} {3}</a>", url, ((EmployeeSearchResultView)e.Item.DataItem).FirstName, ((EmployeeSearchResultView)e.Item.DataItem).LastName, ((EmployeeSearchResultView)e.Item.DataItem).Suffix);

			var phoneNumber = Regex.Replace(((EmployeeSearchResultView)e.Item.DataItem).PhoneNumber, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat);
			if (((EmployeeSearchResultView)e.Item.DataItem).Extension.IsNotNullOrEmpty())
				phoneNumber = phoneNumber + " (ext " + ((EmployeeSearchResultView) e.Item.DataItem).Extension + ")";

			((Literal)e.Item.FindControl("PhoneNumber")).Text = phoneNumber;

		}

		/// <summary>
		/// Gets the hiring managers.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<EmployeeSearchResultView> GetHiringManagers(EmployeeCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_employeesCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;

			var employees = ServiceClientLocator.EmployeeClient(App).SearchEmployees(criteria);
			_employeesCount = employees.TotalCount;

			return employees;
		}

		/// <summary>
		/// Handles the Selecting event of the HiringManagersDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void HiringManagersDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (EmployeeCriteria.IsNull())
			{
				EmployeeCriteria = new EmployeeCriteria
				{
					OrderBy = Constants.SortOrders.ContactNameAsc,
					BusinessUnitId = BusinessUnitId
				};
			}

			e.InputParameters["criteria"] = EmployeeCriteria;
		}

		/// <summary>
		/// Gets the hiring managers count.
		/// </summary>
		/// <returns></returns>
		protected int GetHiringManagersCount()
		{
			return _employeesCount;
		}

		/// <summary>
		/// Gets the hiring managers count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetHiringManagersCount(EmployeeCriteria criteria)
		{
			return _employeesCount;
		}

		/// <summary>
		/// Handles the LayoutCreated event of the HiringManagersList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void HiringManagersList_LayoutCreated(object sender, EventArgs e)
		{
			#region Set labels

			((Literal)HiringManagersList.FindControl("EmployeeNameHeader")).Text = CodeLocalise("EmployeeNameHeader.Text", "NAME");
			((Literal)HiringManagersList.FindControl("TitleHeader")).Text = CodeLocalise("TitleHeader.Text", "TITLE");
			((Literal)HiringManagersList.FindControl("HiringAreasHeader")).Text = CodeLocalise("HiringAreasHeader.Text", "HIRING AREAS");
			((Literal)HiringManagersList.FindControl("EmailAddressHeader")).Text = CodeLocalise("EmailAddressHeader.Text", "EMAIL ADDRESS");
			((Literal)HiringManagersList.FindControl("PhoneNumberHeader")).Text = CodeLocalise("PhoneNumberHeader.Text", "PHONE NUMBER");
			((Literal)HiringManagersList.FindControl("LastLoginHeader")).Text = CodeLocalise("LastLoginHeader.Text", "LAST SIGN-IN");

			#endregion
		}

		/// <summary>
		/// Handles the Sorting event of the HiringManagersList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void HiringManagersList_Sorting(object sender, ListViewSortEventArgs e)
		{
			EmployeeCriteria.OrderBy = e.SortExpression;
			FormatHiringManagersListSortingImages();
		}

		/// <summary>
		/// Formats the recent matches list sorting images.
		/// </summary>
		private void FormatHiringManagersListSortingImages()
		{
			// Reset image URLs
			((ImageButton)HiringManagersList.FindControl("EmployeeNameAscButton")).ImageUrl =
				((ImageButton)HiringManagersList.FindControl("TitleAscButton")).ImageUrl =
				((ImageButton)HiringManagersList.FindControl("EmailAddressAscButton")).ImageUrl =
				((ImageButton)HiringManagersList.FindControl("PhoneNumberAscButton")).ImageUrl =
				((ImageButton)HiringManagersList.FindControl("LastLoginAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)HiringManagersList.FindControl("EmployeeNameDescButton")).ImageUrl =
				((ImageButton)HiringManagersList.FindControl("TitleDescButton")).ImageUrl =
				((ImageButton)HiringManagersList.FindControl("EmailAddressDescButton")).ImageUrl =
				((ImageButton)HiringManagersList.FindControl("PhoneNumberDescButton")).ImageUrl =
				((ImageButton)HiringManagersList.FindControl("LastLoginDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();
			
			// Reenable the buttons
			((ImageButton)HiringManagersList.FindControl("EmployeeNameAscButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("TitleAscButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("EmailAddressAscButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("PhoneNumberAscButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("LastLoginAscButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("EmployeeNameDescButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("TitleDescButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("EmailAddressDescButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("PhoneNumberDescButton")).Enabled =
				((ImageButton)HiringManagersList.FindControl("LastLoginDescButton")).Enabled = true;	

			switch (EmployeeCriteria.OrderBy.ToLower())
			{
				case Constants.SortOrders.ContactNameAsc:
					((ImageButton)HiringManagersList.FindControl("EmployeeNameAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)HiringManagersList.FindControl("EmployeeNameAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.ContactNameDesc:
					((ImageButton)HiringManagersList.FindControl("EmployeeNameDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)HiringManagersList.FindControl("EmployeeNameDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.TitleAsc:
					((ImageButton)HiringManagersList.FindControl("TitleAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)HiringManagersList.FindControl("TitleAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.TitleDesc:
					((ImageButton)HiringManagersList.FindControl("TitleDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)HiringManagersList.FindControl("TitleDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.HiringAreasAsc:
					((ImageButton)HiringManagersList.FindControl("HiringAreasAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)HiringManagersList.FindControl("HiringAreasAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.HiringAreasDesc:
					((ImageButton)HiringManagersList.FindControl("HiringAreasDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)HiringManagersList.FindControl("HiringAreasDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.EmailAddressAsc:
					((ImageButton)HiringManagersList.FindControl("EmailAddressAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)HiringManagersList.FindControl("EmailAddressAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.EmailAddressDesc:
					((ImageButton)HiringManagersList.FindControl("EmailAddressDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)HiringManagersList.FindControl("EmailAddressDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.PhoneNumberAsc:
					((ImageButton)HiringManagersList.FindControl("PhoneNumberAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)HiringManagersList.FindControl("PhoneNumberAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.PhoneNumberDesc:
					((ImageButton)HiringManagersList.FindControl("PhoneNumberDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)HiringManagersList.FindControl("PhoneNumberDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.LastLoginAsc:
					((ImageButton)HiringManagersList.FindControl("LastLoginAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)HiringManagersList.FindControl("LastLoginAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.LastLoginDesc:
					((ImageButton)HiringManagersList.FindControl("LastLoginDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)HiringManagersList.FindControl("LastLoginDescButton")).Enabled = false;
					break;
			}
		}

		#endregion
		
    #region ReferralSummaryPanelData Methods

    /// <summary>
		/// Handles the DataBound event of the ItemsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ReferralSummaryList_DataBound(object sender, ListViewItemEventArgs e)
		{
			var applicationStatusLog = (EmployerJobReferralListItem)e.Item.DataItem;
					
			var scoreImage = (Image)e.Item.FindControl("ReferralScoreImage");
			BindScore(scoreImage, applicationStatusLog.MostRecentReferral.CandidateApplicationScore);
					
			var employmentHistoryLiteral = (Literal)e.Item.FindControl("ReferralLastEmploymentLiteral");
			employmentHistoryLiteral.Text = GetEmploymentHistory(applicationStatusLog.MostRecentReferral);

			var applicationStatusDropDownList = (DropDownList)e.Item.FindControl("ApplicationStatusDropDown");

      var candidateApplication = applicationStatusLog.MostRecentReferral;

      ApplicationStatusTypes candidateApplicationStatus;
      Enum.TryParse(applicationStatusLog.MostRecentReferral.CandidateApplicationStatus.ToString(), true, out candidateApplicationStatus);

      BindStatus(applicationStatusDropDownList, candidateApplicationStatus);

      var applicationStatusLabel = (Label)e.Item.FindControl("ApplicationStatusLabel");

            applicationStatusLabel.Text = HtmlLocalise("ApplicationStatus.Label","Application status");

      if (candidateApplication.VeteranPriorityEndDate.IsNotNull() && candidateApplication.VeteranPriorityEndDate > DateTime.Now && !candidateApplication.CandidateIsVeteran.GetValueOrDefault(false))
      {
        applicationStatusDropDownList.Visible = false;
        applicationStatusLabel.Text = HtmlLocalise("ApplicationStatusLabel.QueuedForRelease", "Queued for release");
      }
      else if (candidateApplication.CandidateApprovalStatus.IsNotIn(ApprovalStatuses.Approved, ApprovalStatuses.None))
      {
        applicationStatusDropDownList.Visible = false;
        if (candidateApplication.CandidateApprovalStatus == ApprovalStatuses.WaitingApproval)
          applicationStatusLabel.Text = HtmlLocalise("ApplicationStatusLabel.Pending", "{0} (Pending)", CodeLocalise(candidateApplicationStatus));
      }
      else
      {
        applicationStatusLabel.Visible = false;
      }
					
			var repeater = (Repeater)e.Item.FindControl("AdditionalReferralsRepeater");
			repeater.DataSource = applicationStatusLog.OtherReferrals;
			repeater.DataBind();
		}

		/// <summary>
		/// Handles the DataBound event of the AdditionalReferralsRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void AdditionalReferralsRepeater_DataBound(object sender, RepeaterItemEventArgs e)
		{
			var candidateApplication = (ApplicationStatusLogViewDto)e.Item.DataItem;
			var scoreImage = (Image)e.Item.FindControl("AdditionalRatingImage");
			BindScore(scoreImage, candidateApplication.CandidateApplicationScore);
			
			var employmentHistoryLiteral = (Literal)e.Item.FindControl("ReferralLastEmploymentLiteral");

			employmentHistoryLiteral.Text = GetEmploymentHistory(candidateApplication);
			
			var applicationStatusDropDownList = (DropDownList)e.Item.FindControl("ApplicationStatusDropDown");

      ApplicationStatusTypes candidateApplicationStatus;
      Enum.TryParse(candidateApplication.CandidateApplicationStatus.ToString(), true, out candidateApplicationStatus);
			BindStatus(applicationStatusDropDownList, candidateApplicationStatus);

      var applicationStatusLabel = (Label)e.Item.FindControl("ApplicationStatusLabel");

      applicationStatusLabel.Text = HtmlLocalise("ApplicationStatus.Label", "Application status");

      if (candidateApplication.VeteranPriorityEndDate.IsNotNull() && candidateApplication.VeteranPriorityEndDate > DateTime.Now && !candidateApplication.CandidateIsVeteran.GetValueOrDefault(false))
      {
        applicationStatusDropDownList.Visible = false;
        applicationStatusLabel.Text = HtmlLocalise("ApplicationStatusLabel.QueuedForRelease", "Queued for release");
        applicationStatusLabel.Visible = true;
      }
      else if (candidateApplication.CandidateApprovalStatus.IsNotIn(ApprovalStatuses.Approved, ApprovalStatuses.None))
      {
        applicationStatusDropDownList.Visible = false;
        if (candidateApplication.CandidateApprovalStatus == ApprovalStatuses.WaitingApproval)
        {
            applicationStatusLabel.Text = HtmlLocalise("ApplicationStatusLabel.Pending", "{0} (Pending)", CodeLocalise(candidateApplicationStatus));
            applicationStatusLabel.Visible = true;
        }
      }
      else
      {
        applicationStatusLabel.Visible = false;
      }
		}

		/// <summary>
		/// Binds the status.
		/// </summary>
		/// <param name="statusDropDownList">The status drop down list.</param>
		/// <param name="status">The status.</param>
		private void BindStatus(DropDownList statusDropDownList, ApplicationStatusTypes status)
		{
			statusDropDownList.Items.Clear();

			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToReportToJob), ApplicationStatusTypes.FailedToReportToJob.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToRespondToInvitation), ApplicationStatusTypes.FailedToRespondToInvitation.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToShow), ApplicationStatusTypes.FailedToShow.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FoundJobFromOtherSource), ApplicationStatusTypes.FoundJobFromOtherSource.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Hired), ApplicationStatusTypes.Hired.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewDenied), ApplicationStatusTypes.InterviewDenied.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewScheduled), ApplicationStatusTypes.InterviewScheduled.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.JobAlreadyFilled), ApplicationStatusTypes.JobAlreadyFilled.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NewApplicant), ApplicationStatusTypes.NewApplicant.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotHired), ApplicationStatusTypes.NotHired.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotQualified), ApplicationStatusTypes.NotQualified.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotYetPlaced), ApplicationStatusTypes.NotYetPlaced.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Recommended), ApplicationStatusTypes.Recommended.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise("ApplicationStatusTypes.RefusedOffer", "Refused job"), ApplicationStatusTypes.RefusedOffer.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.RefusedReferral), ApplicationStatusTypes.RefusedReferral.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.UnderConsideration), ApplicationStatusTypes.UnderConsideration.ToString()));

			statusDropDownList.SelectValue(status.ToString());
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the ApplicationStatusDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ApplicationStatusDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var statusDropDown = (DropDownList)sender;
			
			var	item = statusDropDown.NamingContainer;

			var candidateApplicationId = Convert.ToInt64(((HiddenField)item.FindControl("CandidateApplicationIdHiddenField")).Value);
			
			ServiceClientLocator.CandidateClient(App).UpdateApplicationStatus(candidateApplicationId, statusDropDown.SelectedValueToEnum<ApplicationStatusTypes>());

			Confirmation.Show(CodeLocalise("UpdateApplicationStatusConfirmation.Title", "Application updated"),
															CodeLocalise("UpdateApplicationStatusConfirmation.Body", "The status of the application has been updated."),
															CodeLocalise("Global.Close.Text", "Close"));
		}

		/// <summary>
		/// Gets the employment history.
		/// </summary>
		/// <param name="dto">The dto.</param>
		/// <returns></returns>
		public string GetEmploymentHistory(ApplicationStatusLogViewDto dto)
		{
			var unknownText = CodeLocalise("Global.Unknown.Text", "Unknown");

			if (dto.LatestJobTitle.IsNotNull() || dto.LatestJobEmployer.IsNotNull() || dto.LatestJobStartYear.IsNotNull() || dto.LatestJobEndYear.IsNotNull())
				return String.Format("{0} - {1} ({2}-{3})", dto.LatestJobTitle.IsNotNullOrEmpty()
																											? dto.LatestJobTitle
																											: unknownText,
																											dto.LatestJobEmployer.IsNotNullOrEmpty()
																												? dto.LatestJobEmployer
																												: unknownText,
																											dto.LatestJobStartYear.IsNotNullOrEmpty()
																												? dto.LatestJobStartYear
																												: unknownText,
																											dto.LatestJobEndYear.IsNotNullOrEmpty()
																												? dto.LatestJobEndYear
																												: unknownText);

			return String.Empty;
		}

		/// <summary>
		/// Gets the referral summary.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<EmployerJobReferralListItem> GetReferralSummary(ApplicationStatusLogViewCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_referralsCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;

			var referrals = ServiceClientLocator.CandidateClient(App).GetApplicationStatusLogViewsGrouped(criteria);
			_referralsCount = referrals.TotalCount;

		return referrals;
		}

		/// <summary>
		/// Handles the Selecting event of the HiringManagersDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void ReferralSummaryDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (ApplicationStatusLogViewCriteria.IsNull())
			{
				ApplicationStatusLogViewCriteria = new ApplicationStatusLogViewCriteria
				{
					BusinessUnitId = BusinessUnitId,
					DaysSinceStatusChange = 30,
					JobStatus = JobStatuses.Active,
					ApprovalStatuses = new List<ApprovalStatuses> { ApprovalStatuses.None, ApprovalStatuses.Approved }
				};
			}

			e.InputParameters["criteria"] = ApplicationStatusLogViewCriteria;
		}

		/// <summary>
		/// Gets the hiring managers count.
		/// </summary>
		/// <returns></returns>
		protected int GetReferralSummaryCount()
		{
			return _referralsCount;
		}

		/// <summary>
		/// Gets the hiring managers count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetReferralSummaryCount(ApplicationStatusLogViewCriteria criteria)
		{
			return _referralsCount;
		}

		/// <summary>
		/// Handles the LayoutCreated event of the HiringManagersList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ReferralSummaryList_LayoutCreated(object sender, EventArgs e)
		{
			((Literal)ReferralSummaryList.FindControl("JobOrderHeader")).Text = CodeLocalise("JobOrderHeader.Text", "#EMPLOYMENTTYPE#");
			((Literal)ReferralSummaryList.FindControl("DateHeader")).Text = CodeLocalise("DateHeader.Text", "Date");
			((Literal)ReferralSummaryList.FindControl("ReferralsHeader")).Text = CodeLocalise("ReferralsHeader.Text", "Referrals");
			((Literal)ReferralSummaryList.FindControl("RatingHeader")).Text = CodeLocalise("EmailAddressHeader.Text", "Rating");
			((Literal)ReferralSummaryList.FindControl("NameHeader")).Text = CodeLocalise("NameHeader.Text", "Name");
			((Literal)ReferralSummaryList.FindControl("RecentEmploymentHeader")).Text = CodeLocalise("RecentEmploymentHeader.Text", "Recent Employment");
			((Literal)ReferralSummaryList.FindControl("YearsOfExperienceHeader")).Text = CodeLocalise("YearsOfExperienceHeader.Text", "Years of Experience");
			((Literal)ReferralSummaryList.FindControl("ReferralOutcomeHeader")).Text = CodeLocalise("ReferralOutcomeHeader.Text", "Referral Outcome");
		}

		/// <summary>
		/// Handles the Sorting event of the HiringManagersList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void ReferralSummaryList_Sorting(object sender, ListViewSortEventArgs e)
		{
			ApplicationStatusLogViewCriteria.OrderBy = e.SortExpression;
			FormatReferralSummaryListSortingImages();
		}

		/// <summary>
		/// Formats the recent matches list sorting images.
		/// </summary>
		private void FormatReferralSummaryListSortingImages()
		{
			if (!ReferralSummaryPager.Visible) return;

			// Reset image URLs
			((ImageButton)ReferralSummaryList.FindControl("JobOrderAscButton")).ImageUrl =
				((ImageButton)ReferralSummaryList.FindControl("DateAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)ReferralSummaryList.FindControl("JobOrderDescButton")).ImageUrl =
				((ImageButton)ReferralSummaryList.FindControl("DateDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton)ReferralSummaryList.FindControl("JobOrderAscButton")).Enabled =
				((ImageButton)ReferralSummaryList.FindControl("DateAscButton")).Enabled =
				((ImageButton)ReferralSummaryList.FindControl("JobOrderDescButton")).Enabled =
				((ImageButton)ReferralSummaryList.FindControl("DateDescButton")).Enabled = true;

			if (ApplicationStatusLogViewCriteria.OrderBy.IsNullOrEmpty()) return;

			switch (ApplicationStatusLogViewCriteria.OrderBy.ToLower())
			{
				case Constants.SortOrders.JobTitleAsc:
					((ImageButton)ReferralSummaryList.FindControl("JobOrderAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)ReferralSummaryList.FindControl("JobOrderAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.JobTitleDesc:
					((ImageButton)ReferralSummaryList.FindControl("JobOrderDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)ReferralSummaryList.FindControl("JobOrderDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.DateReceivedAsc:
					((ImageButton)ReferralSummaryList.FindControl("DateAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)ReferralSummaryList.FindControl("DateAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.DateReceivedDesc:
					((ImageButton)ReferralSummaryList.FindControl("DateDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)ReferralSummaryList.FindControl("DateDescButton")).Enabled = false;
					break;
			}
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private static void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = OldApp_RefactorIfFound.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString();

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CandidateTypePlacedAtEmployerLiteral.Text = CodeLocalise("JobSeekersPlacedAtThisEmployer.Label", "#CANDIDATETYPES# Placed at this #BUSINESS#").ToTitleCase();
			imgIsParentCompany.ImageUrl = UrlBuilder.EmployerParentIconLarge();
			imgIsParentCompany.AlternateText = imgIsParentCompany.ToolTip = CodeLocalise("EmployerParent.Hint", "Parent #BUSINESS#:LOWER");
			EditFEINButton.Text = CodeLocalise("EditFEINButton.Text", "Edit FEIN");
			ReplaceParentButton.Text = CodeLocalise("ReplaceParentButton.Text", "Replace parent");
			ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");
		}

		private void ApplyBranding()
		{
			ContactInformationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ContactInformationCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ContactInformationCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			LinkedCompaniesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LinkedCompaniesCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			LinkedCompaniesCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			LoginsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LoginsCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			LoginsCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			if (HiringManagersList.FindControl("EmployeeNameDescButton") != null) ((ImageButton)HiringManagersList.FindControl("EmployeeNameDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
			if (HiringManagersList.FindControl("EmployeeNameAscButton") != null) ((ImageButton)HiringManagersList.FindControl("EmployeeNameAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();

			if (HiringManagersList.FindControl("TitleAscButton") != null) ((ImageButton)HiringManagersList.FindControl("TitleAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			if (HiringManagersList.FindControl("TitleDescButton") != null) ((ImageButton)HiringManagersList.FindControl("TitleDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			if (HiringManagersList.FindControl("EmailAddressAscButton") != null) ((ImageButton)HiringManagersList.FindControl("EmailAddressAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			if (HiringManagersList.FindControl("EmailAddressDescButton") != null) ((ImageButton)HiringManagersList.FindControl("EmailAddressDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			if (HiringManagersList.FindControl("PhoneNumberAscButton") != null) ((ImageButton)HiringManagersList.FindControl("PhoneNumberAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			if (HiringManagersList.FindControl("PhoneNumberDescButton") != null) ((ImageButton)HiringManagersList.FindControl("PhoneNumberDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			if (HiringManagersList.FindControl("LastLoginAscButton") != null) ((ImageButton)HiringManagersList.FindControl("LastLoginAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			if (HiringManagersList.FindControl("LastLoginDescButton") != null) ((ImageButton)HiringManagersList.FindControl("LastLoginDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
			
			ActivitySummaryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ActivitySummaryCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ActivitySummaryCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ActivityLogHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ActivityLogCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ActivityLogCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ReferralSummaryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ReferralSummaryCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ReferralSummaryCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			if (ReferralSummaryList.FindControl("JobOrderAscButton") !=null) ((ImageButton)ReferralSummaryList.FindControl("JobOrderAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			if (ReferralSummaryList.FindControl("JobOrderDescButton") != null) ((ImageButton)ReferralSummaryList.FindControl("JobOrderDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			if (ReferralSummaryList.FindControl("DateAscButton") != null) ((ImageButton)ReferralSummaryList.FindControl("DateAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			if (ReferralSummaryList.FindControl("DateDescButton") != null) ((ImageButton)ReferralSummaryList.FindControl("DateDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			JobSeekersPlacedAtThisEmployerHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobSeekersPlacedAtThisEmployerCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobSeekersPlacedAtThisEmployerCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			NotesAndRemindersHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			NotesAndRemindersCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			NotesAndRemindersCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			AssignedOfficeHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			AssignedOfficeCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			AssignedOfficeCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Hides the office row.
		/// </summary>
		private void HideOfficeRow()
		{
			OfficeRow.Visible = App.Settings.OfficesEnabled;
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the ReferralTimeSpanDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ReferralTimeSpanDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			ApplicationStatusLogViewCriteria.DaysSinceStatusChange = (ReferralTimeSpanDropDown.SelectedValue == "all") ? (int?)null : Convert.ToInt32(ReferralTimeSpanDropDown.SelectedValue);
			ReferralSummaryPager.ReturnToFirstPage();
			BindJobReferralSummaryList();
			FormatReferralSummaryListSortingImages();
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the ReferralOrderStatusDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ReferralOrderStatusDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			ApplicationStatusLogViewCriteria.JobStatus = (ReferralOrderStatusDropDown.SelectedValue == "all") ? (JobStatuses?)null : ReferralOrderStatusDropDown.SelectedValueToEnum<JobStatuses>();
			ReferralSummaryPager.ReturnToFirstPage();
			BindJobReferralSummaryList();
			FormatReferralSummaryListSortingImages();
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the ReferralStatusFilterDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ReferralStatusFilterDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			ApplicationStatusLogViewCriteria.ApplicationStatus = ReferralStatusFilterDropDown.SelectedValue.Equals("all", StringComparison.OrdinalIgnoreCase) ? (ApplicationStatusTypes?)null : ReferralStatusFilterDropDown.SelectedValueToEnum(ApplicationStatusTypes.NotApplicable);
			ReferralSummaryPager.ReturnToFirstPage();
			BindJobReferralSummaryList();
			FormatReferralSummaryListSortingImages();
		}

		/// <summary>
		/// Binds the job referral summary list.
		/// </summary>
		protected void BindJobReferralSummaryList()
		{
			ReferralSummaryList.DataBind();

			// Set visibility of other controls
			ReferralSummaryPager.Visible = (ReferralSummaryPager.TotalRowCount > 0);
		}

		/// <summary>
		/// Handles the OnClick event of the EditFEINButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void EditFEINButton_OnClick(object sender, EventArgs e)
		{
			EditFEINModal.Show(BusinessUnitId, BusinessUnitProfileModel.EmployerLockVersion);
		}

		/// <summary>
		/// Handles the FEINChanged event of the EditFEINModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void EditFEINModal_FEINChanged(object sender, EventArgs e)
		{
			SetReturnUrl(ReturnUrl);
			Response.Redirect(UrlBuilder.AssistEmployerProfile(BusinessUnitId));
		}

		/// <summary>
		/// Handles the ParentReplaced event of the ReplaceParentModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void ReplaceParentModal_ParentReplaced(object sender, EventArgs e)
		{
			SetReturnUrl(ReturnUrl);
			Response.Redirect(UrlBuilder.AssistEmployerProfile(BusinessUnitId));
		}

		/// <summary>
		/// Binds the referral time span drop down.
		/// </summary>
		protected void BindReferralTimeSpanDropDown()
		{
			ReferralTimeSpanDropDown.Items.Clear();

			ReferralTimeSpanDropDown.Items.Add(new ListItem(CodeLocalise("TheLastDay.Text", "the last day"), "1"));
			ReferralTimeSpanDropDown.Items.Add(new ListItem(CodeLocalise("Last7Days.Text", "last 7 days"), "7"));
			ReferralTimeSpanDropDown.Items.Add(new ListItem(CodeLocalise("Last14Days.Text", "last 14 days"), "14"));
			ReferralTimeSpanDropDown.Items.Add(new ListItem(CodeLocalise("Last30Days.Text", "last 30 days"), "30"));
			ReferralTimeSpanDropDown.Items.Add(new ListItem(CodeLocalise("Last60Days.Text", "last 60 days"), "60"));
			ReferralTimeSpanDropDown.Items.Add(new ListItem(CodeLocalise("Last90Days.Text", "last 90 days"), "90"));
			ReferralTimeSpanDropDown.Items.Add(new ListItem(CodeLocalise("Last90Days.Text", "last 180 days"), "180"));
			ReferralTimeSpanDropDown.Items.Add(new ListItem(CodeLocalise("AllDays.Text", "all"), "all"));

			ReferralTimeSpanDropDown.SelectedIndex = 3;
		}

		/// <summary>
		/// Binds the referral order status drop down.
		/// </summary>
		protected void BindReferralOrderStatusDropDown()
		{
			ReferralOrderStatusDropDown.Items.Clear();

			ReferralOrderStatusDropDown.Items.Add(new ListItem(CodeLocalise("AllOrderStatuses.Text", "all orders"), "all"));
			ReferralOrderStatusDropDown.Items.Add(new ListItem(CodeLocalise(JobStatuses.Active, "Active"), JobStatuses.Active.ToString()));
			ReferralOrderStatusDropDown.Items.Add(new ListItem(CodeLocalise(JobStatuses.OnHold, "On hold"), JobStatuses.OnHold.ToString()));
			ReferralOrderStatusDropDown.Items.Add(new ListItem(CodeLocalise(JobStatuses.Closed, "Closed"), JobStatuses.Closed.ToString()));

			ReferralOrderStatusDropDown.SelectedIndex = 1;
		}

		/// <summary>
		/// Binds the referral status filter drop down.
		/// </summary>
		protected void BindReferralStatusFilterDropDown()
		{
			ReferralStatusFilterDropDown.Items.Clear();

			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise("AllReferrals.Text", "All referrals"), "All"));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.DidNotApply, "Did not apply"), ApplicationStatusTypes.DidNotApply.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToReportToJob), ApplicationStatusTypes.FailedToReportToJob.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToRespondToInvitation), ApplicationStatusTypes.FailedToRespondToInvitation.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToShow, "Failed to show"), ApplicationStatusTypes.FailedToShow.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FoundJobFromOtherSource), ApplicationStatusTypes.FoundJobFromOtherSource.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Hired, "Hired"), ApplicationStatusTypes.Hired.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewDenied, "Interview denied"), ApplicationStatusTypes.InterviewDenied.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewScheduled, "Interview scheduled"), ApplicationStatusTypes.InterviewScheduled.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.JobAlreadyFilled), ApplicationStatusTypes.JobAlreadyFilled.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NewApplicant, "New applicant"), ApplicationStatusTypes.NewApplicant.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotHired, "Not hired"), ApplicationStatusTypes.NotHired.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotQualified), ApplicationStatusTypes.NotQualified.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotYetPlaced), ApplicationStatusTypes.NotYetPlaced.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Recommended, "Recommended"), ApplicationStatusTypes.Recommended.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise("ApplicationStatusTypes.RefusedOffer", "Refused job"), ApplicationStatusTypes.RefusedOffer.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.RefusedReferral), ApplicationStatusTypes.RefusedReferral.ToString()));
			ReferralStatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.UnderConsideration, "Under consideration"), ApplicationStatusTypes.UnderConsideration.ToString()));

			ReferralStatusFilterDropDown.SelectedIndex = 0;
		}

		#endregion

		#region Assigned offices panel methods

		/// <summary>
		/// Handles the Click event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveButtonClick(object sender, EventArgs e)
		{
			var employerOffices = new List<EmployerOfficeMapperDto>();
			var assignedOffices = AssignedOffices.AssignedOfficeList;

			if (assignedOffices.Count > 0)
			{
				employerOffices = (from AssignedOfficeModel item in assignedOffices
											select new EmployerOfficeMapperDto()
											{
												OfficeId = item.OfficeId.GetValueOrDefault(),
												EmployerId = EmployerId
											}).ToList();
			}
			else
			{
				AssignedOffices.Invalidate(HtmlLocalise("OfficeExistsValidator.Error", "An office must be assigned"));
				return;
			}

			ServiceClientLocator.EmployerClient(App).UpdateEmployerAssignedOffice(employerOffices, EmployerId);

			// If the default office has been automatically removed and offices assigned then update the offices with the zip of the employer so that future employers with that zip can be auto allocated
			if (DefaultOfficeRemoved)
			{
				var officeIds = (from l in assignedOffices
												 select l.OfficeId.GetValueOrDefault()).ToList();

				var postcodeZip = new List<string>{ServiceClientLocator.EmployerClient(App).GetEmployer(EmployerId).PrimaryAddress.PostcodeZip};

				ServiceClientLocator.EmployerClient(App).UpdateOfficeAssignedZips(postcodeZip, officeIds);
			}

			Confirmation.Show(CodeLocalise("EmployerOfficesUpdatedConfirmation.Title", "#BUSINESS# offices updated"),
															 CodeLocalise("EmployerOfficesUpdatedConfirmation.Body", "The offices for the #BUSINESS#:LOWER have been updated."),
															 CodeLocalise("CloseModal.Text", "OK"));
		}

		#endregion


		#region Linked Companies panel methods

		private int hiringManagerIndex = 1;

		protected void LinkedCompaniesListView_OnLayoutCreated(object sender, EventArgs e)
		{
			((Literal)LinkedCompaniesListView.FindControl("CompanyNameHeader")).Text = CodeLocalise("CompanyNameHeader.Text", "#BUSINESS# Name");
			((Literal)LinkedCompaniesListView.FindControl("LocationHeader")).Text = CodeLocalise("LocationHeader.Text", "Location");
			((Literal)LinkedCompaniesListView.FindControl("HiringManagersHeader")).Text = CodeLocalise("HiringManagersHeader.Text", "Hiring Managers");
			FormatLinkedCompaniesListSortingImages(Constants.SortOrders.BusinessUnitNameAsc);
		}

		protected void LinkedCompaniesListView_OnItemDataBound(object sender, ListViewItemEventArgs e)
		{
			hiringManagerIndex = 1;
			var linkedBusinessUnit = (BusinessUnitLinkedCompanyDto) e.Item.DataItem;

			var hiringManagersRepeater = e.Item.FindControl("HiringManagersRepeater") as Repeater;
			var hiringManagers = linkedBusinessUnit.HiringManagers;
			hiringManagersRepeater.DataSource = hiringManagers;
			hiringManagersRepeater.DataBind();
			if (hiringManagers.Count > App.Settings.NumberOfHiringManagersOnEmployerProfile)
			{
				var moreHiringManagersLink = (HtmlAnchor) e.Item.FindControl("MoreHiringManagersLink");
				moreHiringManagersLink.InnerText = string.Format("+{0} more",
					hiringManagers.Count - App.Settings.NumberOfHiringManagersOnEmployerProfile);
				moreHiringManagersLink.Visible = true;
			}

			var linkedCompanyLocation = e.Item.FindControl("LinkedCompanyLocation") as Literal;
			var linkedCompanyAddress = linkedBusinessUnit.BusinessUnitAddress;
			linkedCompanyLocation.Text = string.Format("{0}, {1}", linkedCompanyAddress.TownCity,
				ServiceClientLocator.CoreClient(App)
					.GetLookup(LookupTypes.States)
					.Where(x => x.Id == linkedCompanyAddress.StateId)
					.Select(x => x.Text)
					.SingleOrDefault());

			var businessUnitLink = e.Item.FindControl("BusinessUnitLink") as HyperLink;
			businessUnitLink.Text = linkedBusinessUnit.BusinessUnit.Name;
			businessUnitLink.NavigateUrl =
				UrlBuilder.AssistEmployerProfile(linkedBusinessUnit.BusinessUnit.Id.GetValueOrDefault());

			if (linkedBusinessUnit.BusinessUnit.IsPrimary)
			{
				var parentCompanyIcon = e.Item.FindControl("ParentCompanyIcon") as Image;
				parentCompanyIcon.AlternateText = parentCompanyIcon.ToolTip = CodeLocalise("EmployerParent.Hint", "Parent #BUSINESS#:LOWER");
				parentCompanyIcon.Visible = true;
			}
		}

		protected void HiringManagersRepeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
			{
				var hiringManagerLink = (HyperLink)e.Item.FindControl("HiringManagerLink");
				var hiringManager = (HiringManagerSummaryDto) e.Item.DataItem;
				hiringManagerLink.NavigateUrl = UrlBuilder.AssistHiringManagerProfile(hiringManager.EmployeeBusinessUnitId);
				if (hiringManagerIndex > App.Settings.NumberOfHiringManagersOnEmployerProfile)
				{
					hiringManagerLink.Style.Add("display", "none");
				}
				hiringManagerIndex++;
			}
		}

		protected void LinkedCompaniesListView_OnSorting(object sender, ListViewSortEventArgs e)
		{
			FormatLinkedCompaniesListSortingImages(e.SortExpression);
		}

		/// <summary>
		/// Formats the linked companies list sorting images.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		private void FormatLinkedCompaniesListSortingImages(string orderBy)
		{
			#region Format images

			// Reset image URLs
			((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)LinkedCompaniesListView.FindControl("LocationSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();
			((ImageButton)LinkedCompaniesListView.FindControl("LocationSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortAscButton")).Enabled =
				((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortDescButton")).Enabled =
				((ImageButton)LinkedCompaniesListView.FindControl("LocationSortAscButton")).Enabled =
				((ImageButton)LinkedCompaniesListView.FindControl("LocationSortDescButton")).Enabled = true;

			switch (orderBy)
			{
				case Constants.SortOrders.BusinessUnitNameAsc:
					((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortAscButton")).Enabled = false;
					((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					break;
				case Constants.SortOrders.BusinessUnitNameDesc:
					((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortDescButton")).Enabled = false;
					((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					break;
				case Constants.SortOrders.LocationAsc:
					((ImageButton)LinkedCompaniesListView.FindControl("LocationSortAscButton")).Enabled = false;
					((ImageButton)LinkedCompaniesListView.FindControl("LocationSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					break;
				case Constants.SortOrders.LocationDesc:
					((ImageButton)LinkedCompaniesListView.FindControl("LocationSortDescButton")).Enabled = false;
					((ImageButton)LinkedCompaniesListView.FindControl("LocationSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					break;
				default:
					((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortAscButton")).Enabled = false;
					((ImageButton)LinkedCompaniesListView.FindControl("CompanyNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					break;
			}

			#endregion
		}

		#endregion

		protected void ReplaceParentButton_OnClick(object sender, EventArgs e)
		{
			ReplaceParentModal.Show(EmployerId, BusinessUnitProfileModel.BusinessUnit.Name, BusinessUnitId);
		}
	}
}