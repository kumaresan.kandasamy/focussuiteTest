﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;
using System.Web.Security;
using Focus.Common;
using Focus.Core;
using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
	public partial class ContactInformation : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (App.Settings.DisableAssistAuthentication)
        throw new Exception("Not authorised");

			Page.Title = CodeLocalise("Page.Title", "Update contact information");

			if (!IsPostBack)
			{
				LocaliseUI();
				ContactInformationMain.Bind(App.User.UserId);

				SaveButton.Visible =(App.Settings.Module != FocusModules.Assist || !App.Settings.ReadOnlyContactInfo || App.User.IsInRole(Constants.RoleKeys.AssistAccountAdministrator));
			}
		}

		/// <summary>
		/// Handles the Click event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Click(object sender, EventArgs e)
		{
			var model = ContactInformationMain.Unbind();

			var userData = ServiceClientLocator.AccountClient(App).UpdateContactDetails(model);

			// If we're updating our own user then reset authentication cookie with the details
			if (userData.Id == App.User.UserId)
			{
				var cookie = FormsAuthentication.GetAuthCookie(model.PersonDetails.FirstName, true);

				// Store UserData inside the Forms Ticket with all the attributes in sync with the web.config
				var newticket = new FormsAuthenticationTicket(1, model.PersonDetails.FirstName, DateTime.Now,
																											DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false,
																											App.User.SerializeUserContext());

				// Encrypt the ticket and store it in the cookie
				cookie.Value = FormsAuthentication.Encrypt(newticket);

				Context.Response.Cookies.Set(cookie);
			}

			ConfirmationModal.Show(CodeLocalise("SuccessfulUpdate.Title", "Contact details changed"),
															 CodeLocalise("SuccessfulUpdate.Body", "You have successfully changed your contact details."),
															 CodeLocalise("CloseModal.Text", "Return to account settings"));
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SaveButton.Text = CodeLocalise("Global.Save.Label", "Save");
		}
	}
}