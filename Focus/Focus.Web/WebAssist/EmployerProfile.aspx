﻿<%@ Page  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="EmployerProfile.aspx.cs" Inherits="Focus.Web.WebAssist.EmployerProfile"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Import Namespace="Focus.Core.Messages.EmployerService" %>
<%@ Import Namespace="Focus.Core.Views" %>
<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>
<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="NotesAndReminders" Src="~/WebAssist/Controls/NotesAndReminders.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register Src="~/WebAssist/Controls/EmployerActivityList.ascx" TagName="EmployerActivityList" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/EmployerJobSeekerPlacements.ascx" TagName="EmployerJobSeekerPlacements" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/AssignedOffices.ascx" TagName="AssignedOffices" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/EditFEINModal.ascx" TagName="EditFEINModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ReplaceParentModal.ascx" TagName="ReplaceParentModal" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(BindTooltips);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width: 100%;" role="presentation">
        <tr>
            <td colspan="2">
                <asp:LinkButton ID="lnkReturn" runat="server" OnClick="lnkReturn_Click" CausesValidation="False"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <h1>
                    <%= HtmlLocalise("EmployerProfile", "#BUSINESS# profile")%>:
                    <asp:Label runat="server" ID="lblCompanyNameTitle"></asp:Label>
                    <asp:Image runat="server" ID="imgIsParentCompany" ClientIDMode="Static" Visible="false"
                        AlternateText="Parent Company Image" Width="18" Height="22"/></h1>
            </td>
            <td style="text-align: right; vertical-align: top;">
                <asp:Button ID="ReplaceParentButton" ValidationGroup="BUSelectionRequired" runat="server" CssClass="button1" ClientIDMode="Static" CausesValidation="False" OnClick="ReplaceParentButton_OnClick" />
                <asp:Button runat="server" ID="EditFEINButton" CssClass="button3" ClientIDMode="Static" OnClick="EditFEINButton_OnClick" />
								<asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3" style="float:right" ClientIDMode="Static"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="HorizontalRule">
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 100%;" role="presentation">
        <tr>
            <td colspan="2">
                <asp:Panel ID="ContactInformationHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image ID="ContactInformationHeaderImage" runat="server" alt="." Height="22" Width="22"/>
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("CompanyInformation.Label", "#BUSINESS# Information")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel ID="ContactInformationPanel" CssClass="singleAccordionContent" ClientIDMode="Static" runat="server">
                    <asp:Label runat="server" ID="lblContactDetailsAddress"></asp:Label>
										<asp:Label runat="server" ID="lblContactDetailsPhone"></asp:Label>
									</asp:Panel>
								</div>
                <act:CollapsiblePanelExtender ID="ContactInformationCollapsiblePanelExtender" runat="server"
                    TargetControlID="ContactInformationPanel" ExpandControlID="ContactInformationHeaderPanel"
                    CollapseControlID="ContactInformationHeaderPanel" ImageControlID="ContactInformationHeaderImage"
                    SuppressPostBack="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="LinkedCompaniesHeaderPanel" CssClass="singleAccordionTitle">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image runat="server" ID="LinkedCompaniesHeaderImage" alt="." Height="22" Width="22" />&nbsp;&nbsp;<span
                                    class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("LinkedCompanies.Label", "Linked #BUSINESSES#")%>
                                    </a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel runat="server" ID="LinkedCompaniesPanel" CssClass="singleAccordionContent" ClientIDMode="Static">
                    <asp:ListView runat="server" ID="LinkedCompaniesListView" DataSourceID="LinkedCompaniesListViewDataSource"
                        ItemPlaceholderID="LinkedCompaniesResults" OnLayoutCreated="LinkedCompaniesListView_OnLayoutCreated"
                      OnItemDataBound="LinkedCompaniesListView_OnItemDataBound" OnSorting="LinkedCompaniesListView_OnSorting">
                      <LayoutTemplate>
                          <table class="table" style="width: 100%;" role="presentation">
                              <thead>
                                  <tr>
                                      <td class="tableHead">
                                          <asp:Literal runat="server" ID="CompanyNameHeader"></asp:Literal>
                                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="CompanyNameSortAscButton"
                                              CssClass="AscButton" runat="server" CommandName="Sort" CommandArgument="businessunitname asc"
                                              AlternateText="Sort company name ascending button" />
                                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="CompanyNameSortDescButton"
                                              CssClass="DescButton" runat="server" CommandName="Sort" CommandArgument="businessunitname desc"
                                              AlternateText="Sort company name descending button" />
                                      </td>
                                      <td class="tableHead">
                                          <asp:Literal runat="server" ID="LocationHeader"></asp:Literal>
                                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LocationSortAscButton"
                                              CssClass="AscButton" runat="server" CommandName="Sort" CommandArgument="location asc"
                                              AlternateText="Sort location ascending button" />
                                          <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="LocationSortDescButton"
                                              CssClass="DescButton" runat="server" CommandName="Sort" CommandArgument="location desc"
                                              AlternateText="Sort location descending button" />
                                      </td>
                                      <td class="tableHead">
                                          <asp:Literal runat="server" ID="HiringManagersHeader"></asp:Literal>
                                      </td>
                                  </tr>
                              </thead>
                              <tbody>
                                  <asp:PlaceHolder runat="server" ID="LinkedCompaniesResults"></asp:PlaceHolder>
                              </tbody>
                          </table>
                      </LayoutTemplate>
                      <ItemTemplate>
                          <tr>
                              <td>
                                  <asp:HyperLink runat="server" ID="BusinessUnitLink" CssClass="hiringManagersLink"></asp:HyperLink><asp:Image runat="server" ID="ParentCompanyIcon" ImageUrl="<%# UrlBuilder.EmployerParentIcon() %>" Visible="false" AlternateText="Parent Company Icon" />
                              </td>
                              <td>
                                  <asp:Literal runat="server" ID="LinkedCompanyLocation"></asp:Literal>
                              </td>
                              <td>
                                  <asp:Repeater runat="server" ID="HiringManagersRepeater" OnItemDataBound="HiringManagersRepeater_OnItemDataBound">
                                      <ItemTemplate>
                                          <asp:HyperLink runat="server" ID="HiringManagerLink" CssClass="hiringManagersLink"><%# ((HiringManagerSummaryDto)Container.DataItem).FirstName %>&nbsp;<%# ((HiringManagerSummaryDto)Container.DataItem).LastName %></asp:HyperLink>
                                      </ItemTemplate>
                                  </asp:Repeater>
                                  <a runat="server" id="MoreHiringManagersLink" href="#" visible="False" class="hiringManagersLink"
                                      onclick="showAllHiringManagers(this); return false;"></a>
                              </td>
                          </tr>
                      </ItemTemplate>
                      <EmptyDataTemplate>
                          <table style="width: 100%;" role="presentation">
                              <tr>
                                  <td>
                                      <%= HtmlLocalise("NoMatches.Text", "No linked #BUSINESSES#:LOWER.")%>
                                  </td>
                              </tr>
                          </table>
                      </EmptyDataTemplate>
                  </asp:ListView>
										<asp:ObjectDataSource ID="LinkedCompaniesListViewDataSource" runat="server" TypeName="Focus.Web.WebAssist.EmployerProfile"
                      EnablePaging="false" SelectMethod="GetLinkedCompanies" SortParameterName="orderBy">
                      <SelectParameters>
                          <asp:RouteParameter Name="businessUnitId" Type="Int64" RouteKey="employerid" />
                      </SelectParameters>
                  </asp:ObjectDataSource>
									</asp:Panel>
								</div>
                
                <act:CollapsiblePanelExtender runat="server" ID="LinkedCompaniesCollapsiblePanelExtender"
                    TargetControlID="LinkedCompaniesPanel" ExpandControlID="LinkedCompaniesHeaderPanel"
                    CollapseControlID="LinkedCompaniesHeaderPanel" ImageControlID="LinkedCompaniesHeaderImage"
                    SuppressPostBack="True" Collapsed="True" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="LoginsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image ID="LoginsHeaderImage" runat="server" alt="." Height="22" Width="22"/>
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Signins.Label", "Sign-ins")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel ID="LoginsPanel" CssClass="singleAccordionContent" runat="server">
                    <table role="presentation">
                        <tr>
                            <td colspan="2">
                                <%= HtmlLocalise("NoOfTalentLogins7Days", "Number of #FOCUSTALENT# sign-ins for all #BUSINESS#:LOWER hiring managers in the past 7 days")%>:
                                <asp:Label runat="server" ID="lblNoOfTalentLogins7Days"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <%= HtmlLocalise("TimeSinceLastLogin", "Time since last sign-in")%>:
                                <asp:Label runat="server" ID="lblTimeSinceLastLogin"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <uc:Pager ID="SearchResultListPager" runat="server" PagedControlId="HiringManagersList"
                        PageSize="10" />
                    <asp:ListView ID="HiringManagersList" runat="server" OnItemDataBound="HiringManagersList_DataBound"
                        ItemPlaceholderID="SearchResultPlaceHolder" DataSourceID="HiringManagersDataSource"
                        DataKeyNames="Id,LastLogin" OnLayoutCreated="HiringManagersList_LayoutCreated"
                        OnSorting="HiringManagersList_Sorting">
                        <LayoutTemplate>
                            <table style="width: 100%;" role="presentation">
                                <tr>
                                    <td class="tableHead">
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="EmployeeNameHeader" />
                                                </td>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmployeeNameAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="contactname asc" alt="Sort employee ascending button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmployeeNameDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="contactname desc" alt="Sort employee descending button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tableHead">
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="TitleHeader" />
                                                </td>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="TitleAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="title asc" alt="Sort title ascending button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="TitleDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="title desc" alt="Sort title descending button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tableHead">
                                        <table role="presentation">
                                            <tr>
                                                <td style="height: 16px;">
                                                    <asp:Literal runat="server" ID="HiringAreasHeader" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tableHead">
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="EmailAddressHeader" />
                                                </td>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmailAddressAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="emailaddress asc" alt="Sort email ascending button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmailAddressDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="emailaddress desc" alt="Sort email descending button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tableHead">
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="PhoneNumberHeader" />
                                                </td>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="PhoneNumberAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="phonenumber asc" alt="Sort phone number ascending button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="PhoneNumberDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="phonenumber desc" alt="Sort phone number descending button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td class="tableHead">
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="LastLoginHeader" />
                                                </td>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LastLoginAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="lastlogin asc" alt="Sort last login ascending button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 8px;">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="LastLoginDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="lastlogin desc" alt="Sort last login descending button" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <hr />
                                    </td>
                                </tr>
                                <asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="width: 15%; vertical-align: top;">
                                    <asp:Literal runat="server" ID="HiringManagerLink"></asp:Literal>
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <%# ((EmployeeSearchResultView)Container.DataItem).JobTitle %>
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <asp:Literal runat="server" ID="HiringAreas"></asp:Literal>
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <%# ((EmployeeSearchResultView)Container.DataItem).EmailAddress %>
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <asp:Literal ID="PhoneNumber" runat="server" />
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <%# DataBinder.Eval(Container.DataItem, "LastLogin", "{0:MMM d yyyy, h:mm}")%><%# (DataBinder.Eval(Container.DataItem, "LastLogin", "{0:tt}").ToLower())%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                    <asp:ObjectDataSource ID="HiringManagersDataSource" runat="server" TypeName="Focus.Web.WebAssist.EmployerProfile"
                        EnablePaging="true" SelectMethod="GetHiringManagers" SelectCountMethod="GetHiringManagersCount"
                        SortParameterName="orderBy" OnSelecting="HiringManagersDataSource_Selecting">
                    </asp:ObjectDataSource>
									</asp:Panel>
								</div>
                
                <act:CollapsiblePanelExtender ID="LoginsCollapsiblePanelExtender" runat="server"
                    TargetControlID="LoginsPanel" ExpandControlID="LoginsHeaderPanel" CollapseControlID="LoginsHeaderPanel"
                    Collapsed="true" ImageControlID="LoginsHeaderImage" SuppressPostBack="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="ActivitySummaryHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image ID="ActivitySummaryHeaderImage" runat="server" alt="." Height="22" Width="22"/>
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ActivitySummary.Label", "Activity Summary")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel ID="ActivitySummaryPanel" CssClass="singleAccordionContent" runat="server">
                    <table role="presentation">
                      <tr>
                          <td colspan="2">
															<input type="radio" name="rdoDaysFilter" ID="rdoDaysFilter7days" value="7days" checked="checked"/>
                              <label for="rdoDaysFilter7days"><%=HtmlLabel("Last7Days", "Last 7 days") %></label>
                              <input type="radio" name="rdoDaysFilter" ID="rdoDaysFilter30days" value="30days"/>
                              <label for="rdoDaysFilter30days"><%=HtmlLabel("Last30Days", "Last 30 days") %></label>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <%= HtmlLocalise("NoOfJobOrdersOpen", "Number of #EMPLOYMENTTYPE# open")%>: <span
                                  id="NoOfJobOrdersOpen7days">
                                  <asp:Label runat="server" ID="lblNoOfJobOrdersOpen7Days"></asp:Label></span><span
                                      id="NoOfJobOrdersOpen30days" style="display: none;"><asp:Label runat="server" ID="lblNoOfJobOrdersOpen30days"></asp:Label></span>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <%= HtmlLocalise("NoOfJobOrdersEditedByStaff", "Number of #EMPLOYMENTTYPE# edited by staff")%>:
                              <span id="NoOfJobOrdersEditedByStaff7days">
                                  <asp:Label runat="server" ID="lblNoOfJobOrdersEditedByStaff7days"></asp:Label></span><span
                                      id="NoOfJobOrdersEditedByStaff30days" style="display: none;"><asp:Label runat="server"
                                          ID="lblNoOfJobOrdersEditedByStaff30days"></asp:Label></span>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <%= HtmlLocalise("NoOfResumesReferred", "Number of resumes referred")%>: <span id="NoOfResumesReferred7days">
                                  <asp:Label runat="server" ID="lblNoOfResumesReferred7days"></asp:Label></span><span
                                      id="NoOfResumesReferred30days" style="display: none;"><asp:Label runat="server" ID="lblNoOfResumesReferred30days"></asp:Label></span>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <%= HtmlLocalise("NoOfJobSeekersInterviewed", "Number of #CANDIDATETYPE# interviewed")%>:
                              <span id="NoOfJobSeekersInterviewed7days">
                                  <asp:Label runat="server" ID="lblNoOfJobSeekersInterviewed7days"></asp:Label></span><span
                                      id="NoOfJobSeekersInterviewed30days" style="display: none;"><asp:Label runat="server"
                                          ID="lblNoOfJobSeekersInterviewed30days"></asp:Label></span>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <%= HtmlLocalise("NoOfJobSeekersRejected", "Number of #CANDIDATETYPE# rejected")%>:
                              <span id="NoOfJobSeekersRejected7days">
                                  <asp:Label runat="server" ID="lblNoOfJobSeekersRejected7days"></asp:Label></span><span
                                      id="NoOfJobSeekersRejected30days" style="display: none;"><asp:Label runat="server"
                                          ID="lblNoOfJobSeekersRejected30days"></asp:Label></span>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <%= HtmlLocalise("NoOfJobSeekersHired", "Number of #CANDIDATETYPE# hired")%>: <span
                                  id="NoOfJobSeekersHired7days">
                                  <asp:Label runat="server" ID="lblNoOfJobSeekersHired7days"></asp:Label></span><span
                                      id="NoOfJobSeekersHired30days" style="display: none;"><asp:Label runat="server" ID="lblNoOfJobSeekersHired30days"></asp:Label></span>
                          </td>
                      </tr>
                    </table>
									</asp:Panel>
								</div>
                
                <act:CollapsiblePanelExtender ID="ActivitySummaryCollapsiblePanelExtender" runat="server"
                    TargetControlID="ActivitySummaryPanel" ExpandControlID="ActivitySummaryHeaderPanel"
                    CollapseControlID="ActivitySummaryHeaderPanel" Collapsed="true" ImageControlID="ActivitySummaryHeaderImage"
                    SuppressPostBack="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="ActivityLogHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image ID="ActivityLogHeaderImage" runat="server" alt="." Height="22" Width="22"/>
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ActivityLog.Label", "Activity Log")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel ID="ActivityLogPanel" CssClass="singleAccordionContent" runat="server">
                    <uc:EmployerActivityList ID="EmployerActivityList" runat="server" />
									</asp:Panel>
								</div>
                <act:CollapsiblePanelExtender ID="ActivityLogCollapsiblePanelExtender" runat="server"
                    TargetControlID="ActivityLogPanel" ExpandControlID="ActivityLogHeaderPanel" CollapseControlID="ActivityLogHeaderPanel"
                    Collapsed="true" ImageControlID="ActivityLogHeaderImage" SuppressPostBack="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="ReferralSummaryHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image ID="ReferralSummaryHeaderImage" runat="server" alt="." Height="22" Width="22"/>
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ReferralSummary.Label", "Referral Summary")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel ID="ReferralSummaryPanel" CssClass="singleAccordionContent" runat="server">
                    <asp:UpdatePanel runat="server" ID="ReferralSummaryUpdatePanel">
                        <ContentTemplate>
                            <%=HtmlLabel(ReferralTimeSpanDropDown,"ShowResultsFor.Text", "Show results for")%>
                            &nbsp;
                            <asp:DropDownList runat="server" ID="ReferralTimeSpanDropDown" OnSelectedIndexChanged="ReferralTimeSpanDropDown_SelectedIndexChanged"
                                AutoPostBack="True" />
                            &nbsp;&nbsp;
                            <%=HtmlLabel(ReferralOrderStatusDropDown,"OrderStatus.Text","Order status") %>
                            &nbsp;
                            <asp:DropDownList runat="server" ID="ReferralOrderStatusDropDown" OnSelectedIndexChanged="ReferralOrderStatusDropDown_SelectedIndexChanged"
                                AutoPostBack="True" />
                            &nbsp;&nbsp;
                            <%=HtmlLabel(ReferralStatusFilterDropDown,"Display.Text", "Display")%>
                            &nbsp;
                            <asp:DropDownList runat="server" ID="ReferralStatusFilterDropDown" OnSelectedIndexChanged="ReferralStatusFilterDropDown_SelectedIndexChanged"
                                AutoPostBack="True" />
                            <uc:Pager align="right" ID="ReferralSummaryPager" runat="server" PagedControlId="ReferralSummaryList"
                                PageSize="10" DisplayRecordCount="True" />
                            <asp:ListView ID="ReferralSummaryList" runat="server" OnItemDataBound="ReferralSummaryList_DataBound"
                                ItemPlaceholderID="SearchResultPlaceHolder" DataSourceID="ReferralSummaryDataSource"
                                DataKeyNames="Id" OnLayoutCreated="ReferralSummaryList_LayoutCreated" OnSorting="ReferralSummaryList_Sorting">
                                <LayoutTemplate>
                                    <table style="width: 100%;" role="presentation">
                                        <tr>
                                            <td class="tableHead" style="width: 15%;">
                                                <table role="presentation">
                                                    <tr>
                                                        <td rowspan="2" style="height: 16px;">
                                                            <asp:Literal runat="server" ID="JobOrderHeader" />
                                                        </td>
                                                        <td style="height: 8px;">
                                                            <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobOrderAscButton"
                                                                runat="server" CommandName="Sort" CommandArgument="jobtitle asc" CssClass="AscButton"
                                                                AlternateText="Sort job order ascending button" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px;">
                                                            <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="JobOrderDescButton"
                                                                runat="server" CommandName="Sort" CommandArgument="jobtitle desc" CssClass="DescButton"
                                                                AlternateText="Sort job order descending button" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tableHead" style="width: 10%;">
                                                <table role="presentation">
                                                    <tr>
                                                        <td rowspan="2" style="height: 16px;">
                                                            <asp:Literal runat="server" ID="DateHeader" />
                                                        </td>
                                                        <td style="height: 8px;">
                                                            <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="DateAscButton"
                                                                runat="server" CommandName="Sort" CommandArgument="datereceived asc" CssClass="AscButton"
                                                                AlternateText="Sort date ascending button" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 8px;">
                                                            <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="DateDescButton"
                                                                runat="server" CommandName="Sort" CommandArgument="datereceived desc" CssClass="DescButton"
                                                                AlternateText="Sort date descending button" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tableHead" style="width: 10%;">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="height: 16px;">
                                                            <asp:Literal runat="server" ID="ReferralsHeader" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tableHead" style="width: 10%;">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="height: 16px;">
                                                            <asp:Literal runat="server" ID="RatingHeader" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tableHead" style="width: 15%;">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="height: 16px;">
                                                            <asp:Literal runat="server" ID="NameHeader" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tableHead" style="width: 20%;">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="height: 16px;">
                                                            <asp:Literal runat="server" ID="RecentEmploymentHeader" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tableHead" style="width: 10%;">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="height: 16px; text-align: center;">
                                                            <asp:Literal runat="server" ID="YearsOfExperienceHeader" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td class="tableHead" style="width: 10%;">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="height: 16px; text-align: center;">
                                                            <asp:Literal runat="server" ID="ReferralOutcomeHeader" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <hr />
                                            </td>
                                        </tr>
                                        <asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="vertical-align: top;">
                                            <%# ((EmployerJobReferralListItem)Container.DataItem).MostRecentReferral.JobTitle%>
                                        </td>
                                        <td style="vertical-align: top;">
                                            <%# ((EmployerJobReferralListItem)Container.DataItem).MostRecentReferral.ActionedOn.ToString("MMM dd, yyyy")%>
                                        </td>
                                        <td style="vertical-align: top;">
                                            <span class="Show<%# ((EmployerJobReferralListItem)Container.DataItem).Id %>"><a
                                                href="javascript:void(0)" onclick="ShowRepeater(<%# ((EmployerJobReferralListItem)Container.DataItem).Id%>, <%# ((EmployerJobReferralListItem)Container.DataItem).NumReferrals%>)">
                                                Show referrals</a></span> <span class="Repeater Hide<%# ((EmployerJobReferralListItem)Container.DataItem).Id %>">
                                                    <a href="javascript:void(0)" onclick="HideRepeater(<%# ((EmployerJobReferralListItem)Container.DataItem).Id%>)">
                                                        Hide referrals</a></span>
                                        </td>
                                        <td style="vertical-align: top;">
                                            <span class="Repeater Repeater<%# ((EmployerJobReferralListItem)Container.DataItem).Id%>">
                                                <asp:Image ID="ReferralScoreImage" runat="server" alt="Referral Score Image" Height="16" Width="80"/></span>
                                        </td>
                                        <td style="vertical-align: top;" class="Repeater Repeater<%# ((EmployerJobReferralListItem)Container.DataItem).Id%>">
                                            <%# ((EmployerJobReferralListItem)Container.DataItem).MostRecentReferral.CandidateFirstName%>
                                            <%# ((EmployerJobReferralListItem)Container.DataItem).MostRecentReferral.CandidateLastName%>
                                        </td>
                                        <td style="vertical-align: top;" class="Repeater Repeater<%# ((EmployerJobReferralListItem)Container.DataItem).Id%>">
                                            <asp:Literal ID="ReferralLastEmploymentLiteral" runat="server" />
                                        </td>
                                        <td style="vertical-align: top; text-align: center;" class="Repeater Repeater<%# ((EmployerJobReferralListItem)Container.DataItem).Id%>">
                                            <%#((EmployerJobReferralListItem)Container.DataItem).MostRecentReferral.CandidateYearsExperience%>
                                        </td>
                                        <td class="Repeater Repeater<%# ((EmployerJobReferralListItem)Container.DataItem).Id%>">
	                                        <focus:LocalisedLabel runat="server" ID="ApplicationStatusDropDownLabel" AssociatedControlID="ApplicationStatusDropDown" LocalisationKey="ApplicationStatus.Label" DefaultText="Application status" CssClass="sr-only"/>
                                            <asp:DropDownList runat="server" ID="ApplicationStatusDropDown" OnSelectedIndexChanged="ApplicationStatusDropDown_SelectedIndexChanged"
                                                AutoPostBack="true" />
                                            <asp:Label runat="server" ID="ApplicationStatusLabel"></asp:Label>
                                        </td>
                                    </tr>
																		<tr>
																			<td>
																				<asp:HiddenField runat="server" ID="CandidateApplicationIdHiddenField" Value="<%# ((EmployerJobReferralListItem)Container.DataItem).MostRecentReferral.CandidateApplicationId%>" />		
																			</td>
																		</tr>
                                    <asp:Repeater runat="server" ID="AdditionalReferralsRepeater" OnItemDataBound="AdditionalReferralsRepeater_DataBound">
                                        <ItemTemplate>
                                            <tr class="Repeater Repeater<%# ((ApplicationStatusLogViewDto)Container.DataItem).JobId %>">
																							<td style="vertical-align: top;">
																									&nbsp;
																							</td>
																							<td style="vertical-align: top;">
																									&nbsp;
																							</td>
																							<td style="vertical-align: top;">
																									&nbsp;
																							</td>
                                                <td>
                                                    <asp:Image ID="AdditionalRatingImage" runat="server" alt="Additional rating image" Height="16" Width="80"/>
                                                </td>
                                                <td>
                                                    <%# ((ApplicationStatusLogViewDto)Container.DataItem).CandidateFirstName%>
                                                    <%# ((ApplicationStatusLogViewDto)Container.DataItem).CandidateLastName%>
                                                </td>
                                                <td>
                                                    <asp:Literal ID="ReferralLastEmploymentLiteral" runat="server" />
                                                </td>
                                                <td style="text-align: center;">
                                                    <%#((ApplicationStatusLogViewDto)Container.DataItem).CandidateYearsExperience%>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ApplicationStatusDropDown" title="Application Status DropDown" OnSelectedIndexChanged="ApplicationStatusDropDown_SelectedIndexChanged"
                                                        AutoPostBack="true" />
                                                    <asp:Label runat="server" ID="ApplicationStatusLabel" Visible="False"></asp:Label>
                                                </td>
                                            </tr>
                                            <asp:HiddenField runat="server" ID="CandidateApplicationIdHiddenField" Value="<%# ((ApplicationStatusLogViewDto)Container.DataItem).CandidateApplicationId %>" />
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                    <table style="width: 100%;" role="presentation">
                                        <tr>
                                            <td>
                                                <%= HtmlLocalise("NoMatches.Text", "No referrals.") %>
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:ListView>
                            <asp:ObjectDataSource ID="ReferralSummaryDataSource" runat="server" TypeName="Focus.Web.WebAssist.EmployerProfile"
                                EnablePaging="true" SelectMethod="GetReferralSummary" SelectCountMethod="GetReferralSummaryCount"
                                SortParameterName="orderBy" OnSelecting="ReferralSummaryDataSource_Selecting">
                            </asp:ObjectDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
									</asp:Panel>
								</div>
                
                <act:CollapsiblePanelExtender ID="ReferralSummaryCollapsiblePanelExtender" runat="server"
                    TargetControlID="ReferralSummaryPanel" ExpandControlID="ReferralSummaryHeaderPanel"
                    CollapseControlID="ReferralSummaryHeaderPanel" Collapsed="true" ImageControlID="ReferralSummaryHeaderImage"
                    SuppressPostBack="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="JobSeekersPlacedAtThisEmployerHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image ID="JobSeekersPlacedAtThisEmployerHeaderImage" runat="server" alt="." Height="22" Width="22"/>
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><asp:Literal
                                    runat="server" ID="CandidateTypePlacedAtEmployerLiteral"></asp:Literal></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel ID="JobSeekersPlacedAtThisEmployerPanel" CssClass="singleAccordionContent" runat="server">
										<uc:EmployerJobSeekerPlacements ID="EmployerJobSeekerPlacements" runat="server" />
									</asp:Panel>
								</div>
                <act:CollapsiblePanelExtender ID="JobSeekersPlacedAtThisEmployerCollapsiblePanelExtender"
                    runat="server" TargetControlID="JobSeekersPlacedAtThisEmployerPanel" ExpandControlID="JobSeekersPlacedAtThisEmployerHeaderPanel"
                    CollapseControlID="JobSeekersPlacedAtThisEmployerHeaderPanel" Collapsed="true"
                    ImageControlID="JobSeekersPlacedAtThisEmployerHeaderImage" SuppressPostBack="true" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="NotesAndRemindersHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image ID="NotesAndRemindersHeaderImage" runat="server" alt="." Height="22" Width="22"/>
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("NotesAndReminders.Label", "Notes and Reminders")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel ID="NotesAndRemindersPanel" CssClass="singleAccordionContent" runat="server">
										<uc:NotesAndReminders ID="NotesReminders" runat="server" />
									</asp:Panel>
								</div>
                <act:CollapsiblePanelExtender ID="NotesAndRemindersCollapsiblePanelExtender" runat="server"
                    TargetControlID="NotesAndRemindersPanel" ExpandControlID="NotesAndRemindersHeaderPanel"
                    CollapseControlID="NotesAndRemindersHeaderPanel" Collapsed="true" ImageControlID="NotesAndRemindersHeaderImage"
                    SuppressPostBack="true" />
            </td>
        </tr>
        <tr runat="server" id="OfficeRow">
            <td colspan="2">
                <asp:Panel ID="AssignedOfficeHeaderPanel" runat="server" CssClass="singleAccordionTitle"
                    Style="margin-bottom: 20px !important;">
                    <table class="accordionTable" role="presentation">
                        <tr>
                            <td>
                                <asp:Image ID="AssignedOfficeHeaderImage" runat="server" alt="." Height="22" Width="22"/>
                                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AssignedOffice.Label", "Assigned Office")%></a></span>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
								<div class="singleAccordionContentWrapper">
									<asp:Panel ID="AssignedOfficePanel" CssClass="singleAccordionContent" runat="server">
										<uc:AssignedOffices ID="AssignedOffices" runat="server" OnSaveAssignedOfficesClick="SaveButtonClick" AdministrationType="Employer" />
									</asp:Panel>
								</div>
                <act:CollapsiblePanelExtender ID="AssignedOfficeCollapsiblePanelExtender" runat="server"
                    TargetControlID="AssignedOfficePanel" ExpandControlID="AssignedOfficeHeaderPanel"
                    CollapseControlID="AssignedOfficeHeaderPanel" Collapsed="true" ImageControlID="AssignedOfficeHeaderImage"
                    SuppressPostBack="true" BehaviorID="AssignedOfficeCollapsiblePanel" />
            </td>
        </tr>
    </table>
    <uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />
    <uc:EditFEINModal ID="EditFEINModal" runat="server" OnFEINChanged="EditFEINModal_FEINChanged" />
    <uc:ReplaceParentModal ID="ReplaceParentModal" runat="server" OnParentReplaced="ReplaceParentModal_ParentReplaced" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[name="rdoDaysFilter"]').change(function () {
                if ($(this).val() == '30days') {
                    $('span[id$="7days"]').hide();
                    $('span[id$="30days"]').show();
                }
                else {
                    $('span[id$="7days"]').show();
                    $('span[id$="30days"]').hide();
                }
                  });

            $('.Repeater').hide();
           });


        Sys.Application.add_load(EmployerProfile_PageLoad);

        function EmployerProfile_PageLoad() {
          var button = $("#<%= ExpandCollapseAllButton.ClientID %>");
          if (button.length > 0)
          	bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>', null); // bind the magic

          accordionLayoutPostbackFix(); // FVN-3128
        }

        function showPopup(url, blockedPopupMessage) {
            var popup = window.open(url, "_blank");
            if (!popup)
                alert(blockedPopupMessage);
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            $('.Repeater').hide();

        });

        function ShowRepeater(id, rowspan) {
            var repeater = '.Repeater' + id;
            $(repeater).show();
            var showLink = '.Show' + id;
            $(showLink).hide();
            var hideLink = '.Hide' + id;
            $(hideLink).show();
        }

        function HideRepeater(id) {
            var repeater = '.Repeater' + id;
            $(repeater).hide();
            var showLink = '.Show' + id;
            $(showLink).show();
            var hideLink = '.Hide' + id;
            $(hideLink).hide();
        }

        function showAllHiringManagers(src) {
            $(src).siblings('.hiringManagersLink').show();
            $(src).hide();
        }

    </script>
</asp:Content>
