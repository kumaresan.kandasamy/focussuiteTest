﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageAnnouncements.aspx.cs" Inherits="Focus.Web.WebAssist.ManageAnnouncements" %>



<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/ManageAssistNavigation.ascx" tagname="ManageAssistNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/CreateAnnouncement.ascx" tagname="CreateAnnouncement" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/SavedAnnouncements.ascx" tagname="SavedAnnouncements" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/RecentAnnouncements.ascx" tagname="RecentAnnouncements" tagprefix="uc" %>



<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%= ResolveUrl("~/Assets/tinymce/tinymce.min.js") %>" type="text/javascript"></script>
	<script type="text/javascript">
		tinymce.init({
			encoding: "xml",
			selector: ".mcetiny",
			theme: "modern",
			menubar: "file edit insert view",
			plugins: "link preview",
			toolbar: "undo redo |  bold italic | link image | preview image",
			setup: function (editor) {
				editor.on("SaveContent", function (i) {
					i.content = i.content.replace(/&#39/g, "&apos");
				});
			}
		});
	</script>

</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />

	<h1><%= HtmlLocalise("ManageAnnouncements.Title", "Home page announcements")%></h1>

	<p class="pageNav">
		<asp:LinkButton ID="NewAnnouncementLink" runat="server" Visible="false" OnClick="NewAnnouncementLink_Clicked"><%= HtmlLocalise("NewAnnouncement.LinkText", "New announcement")%></asp:LinkButton>
		<strong><asp:Literal ID="NewAnnouncementLinkDisplayOnly" runat="server" /></strong>
		&nbsp;|&nbsp;
		<asp:LinkButton ID="SavedAnnouncementsLink" runat="server" OnClick="SavedAnnouncementsLink_Clicked"><%= HtmlLocalise("SavedAnnouncements.LinkText", "Saved announcements")%></asp:LinkButton>
		<strong><asp:Literal ID="SavedAnnouncementsLinkDisplayOnly" runat="server" Visible="false" /></strong>
		&nbsp;|&nbsp;
		<asp:LinkButton ID="RecentAnnouncementsLink" runat="server" OnClick="RecentAnnouncementsLink_Clicked"><%= HtmlLocalise("RecentAnnouncementsLink.LinkText", "Recently posted announcements")%></asp:LinkButton>
		<strong><asp:Literal ID="RecentAnnouncementsLinkDisplayOnly" runat="server" Visible="false" /></strong>
	</p>

	<uc:CreateAnnouncement ID="CreateAnnouncement" runat="server" />
	<uc:SavedAnnouncements ID="SavedAnnouncements" runat="server" Visible="false"/>
	<uc:RecentAnnouncements ID="RecentAnnouncements" runat="server" Visible="false"/>
</asp:Content>
