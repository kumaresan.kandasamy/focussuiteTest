﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Feedback.aspx.cs" Inherits="Focus.Web.WebAssist.Feedback" %>

<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ManageAssistNavigation.ascx" TagName="ManageAssistNavigation"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />
    <asp:UpdatePanel ID="FeedbackUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table style="width: 100%;" role="presentation">
                <tr>
                    <td>
                        <h1>
                            <%= HtmlLocalise("Feedback.Title", "Contact us") %></h1>
                    </td>
                    <td>
                        <asp:Button ID="SendEmailButton" runat="server" class="button3 right" OnClick="SendEmailButton_Clicked" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table role="presentation">
                            <tbody>
                                <tr>
                                    <td rowspan="7" style="vertical-align: top; width: 140px;">
                                        <%= HtmlLabel("ReasonForContact","Reason for contact") %>
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="CommentQuestionRadio" GroupName="ReasonForContactGroup" TextAlign="Right"
                                            runat="server" Checked="True" OnCheckedChanged="CommentQuestionRadio_CheckedChanged"
                                            AutoPostBack="True" CausesValidation="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="JobWebsiteSuggestionRadio" GroupName="ReasonForContactGroup"
                                            TextAlign="Right" runat="server" OnCheckedChanged="CommentQuestionRadio_CheckedChanged"
                                            AutoPostBack="True" CausesValidation="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="IssueRadioButton" GroupName="ReasonForContactGroup" TextAlign="Right"
                                            runat="server" OnCheckedChanged="IssueRadioButton_CheckedChanged" AutoPostBack="True"
                                            CausesValidation="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
	                                    <focus:LocalisedLabel runat="server" ID="IssueDropDownLabel" AssociatedControlID="IssueDropDown" LocalisationKey="Issues" DefaultText="Issues" CssClass="sr-only"/>
                                        <asp:DropDownList runat="server" ID="IssueDropDown" Enabled="False" AutoPostBack="True"
                                            OnSelectedIndexChanged="IssueDropDown_SelectedIndexChanged" />
                                        <asp:Button ID="IssueButton" runat="server" CssClass="button1" OnClick="IssueButton_Clicked" />
                                        <asp:RequiredFieldValidator ID="IssueDropDownRequired" runat="server" ControlToValidate="IssueDropDown"
                                            CssClass="error" Enabled="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
	                                    <focus:LocalisedLabel runat="server" ID="SubIssueDropDownLabel" AssociatedControlID="SubIssueDropDown" Visible="false" LocalisationKey=" SubIssues" DefaultText="Sub issues" CssClass="sr-only"/>
                                        <asp:DropDownList runat="server" ID="SubIssueDropDown" Visible="false" ClientIDMode="Static"
                                            OnSelectedIndexChanged="SubIssueDropDown_SelectedIndexChanged" />
                                        <asp:RequiredFieldValidator ID="SubIssueDropDownRequired" runat="server" ControlToValidate="SubIssueDropDown"
                                            CssClass="error" Enabled="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <asp:Label runat="server" ID="MessageLabel"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
	                                    <focus:LocalisedLabel runat="server" ID="DetailsTextBoxLabel" AssociatedControlID="DetailsTextBox" LocalisationKey="Details" DefaultText="Details" CssClass="sr-only"/>
                                        <asp:TextBox ID="DetailsTextBox" runat="server" TextMode="MultiLine" Rows="5" Width="400"
                                            style="overflow-y: scroll" />
                                        <asp:RequiredFieldValidator ID="DetailsRequired" runat="server" ControlToValidate="DetailsTextBox"
                                            CssClass="error" Enabled="False"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            <%-- Confirmation Modal --%>
            <uc:ConfirmationModal ID="Confirmation" runat="server" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="CommentQuestionRadio" />
            <asp:AsyncPostBackTrigger ControlID="IssueRadioButton" />
            <asp:AsyncPostBackTrigger ControlID="JobWebsiteSuggestionRadio" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
