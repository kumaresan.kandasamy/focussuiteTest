﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	public partial class RegisterEmployer : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		/// <summary>
		/// Handles the OnRegistrationCompleted event of the Registration control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.EmployerRegistration.RegistrationCompletedEventArgs"/> instance containing the event data.</param>
		protected void Registration_OnRegistrationCompleted(object sender, EmployerRegistration.RegistrationCompletedEventArgs e)
		{
			Confirmation.Show(
				CodeLocalise("SuccessfulSubmitValidFEIN.Title", "Thank you for registering this #BUSINESS#:LOWER with #FOCUSTALENT#"),
				CodeLocalise("SuccessfulSubmitValidFEIN.Body", "Since this registration does not require additional staff review, it will not go to the Approve #BUSINESS# Account queue. You may post jobs immediately on this #BUSINESS#:LOWER's behalf."),
				CodeLocalise("Global.ViewDashboard.Text.NoEdit", "View Dashboard"), 
				closeLink: UrlBuilder.JobOrderDashboard(),
				okText: CodeLocalise("CreateJob.Text.NoEdit", "Create Job"), 
				okCommandName: "CreateJob",
				okCommandArgument: e.EmployeeId.ToString());
		}

		/// <summary>
		/// Handles the OkCommand event of the Confirmation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_OkCommand(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "CreateJob":
					if (e.CommandArgument.IsNotNull())
					{
						Response.Redirect(String.Format("{0}?managerId={1}", UrlBuilder.AssistJobWizard(), e.CommandArgument));
					}
					break;
			}
		}
	}
}