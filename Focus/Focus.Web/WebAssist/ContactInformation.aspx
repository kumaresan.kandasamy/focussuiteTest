﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ContactInformation.aspx.cs" Inherits="Focus.Web.WebAssist.ContactInformation" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register Src="Controls/AccountSettingsLinks.ascx" TagPrefix="uc" TagName="SettingsLinks" %>
<%@ Register TagPrefix="uc" TagName="ContactInformation" Src="~/Code/Controls/User/ContactInformation.ascx" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:SettingsLinks ID="SettingLinksMain" runat="server" />

	<h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>

	<table style="width:100%;" role="presentation" >
		<tr>
			<td>
				<asp:button ID="SaveButton" runat="server" CssClass="button2 right" OnClick="SaveButton_Click" ValidationGroup="ContactInformation" />
				<p><b><%= HtmlLocalise("UpdateContactInformation.Label", "Update my contact information")%></b> <%= HtmlRequiredFieldsInstruction() %></p>
			</td>	
		</tr>
	</table>

	<uc:ContactInformation ID="ContactInformationMain" runat="server" />

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
</asp:Content>
