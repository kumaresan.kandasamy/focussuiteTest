﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageDocuments.aspx.cs" Inherits="Focus.Web.WebAssist.ManageDocuments" %>

<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Import Namespace="Focus.Core.Models.Assist" %>
<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/DocumentModal.ascx" TagName="DocumentModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ManageAssistNavigation" Src="~/WebAssist/Controls/ManageAssistNavigation.ascx" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
  <uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />
	<h1>
		<%= HtmlLocalise("ManageDocuments.Header", "Manage Documents")%></h1>
	<div style="text-align: right; margin-bottom: 10px;">
		<asp:Button runat="server" ID="AddButton" OnClick="AddButton_Clicked" CssClass="button1" />
	</div>
	<div style="display: block;">
		<asp:Repeater ID="DocumentGroupRepeater" runat="server" OnItemDataBound="DocumentGroupRepeater_ItemDataBound">
			<ItemTemplate>
				<asp:Panel ID="DocumentsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
					<table class="accordionTable" role="presentation">
						<tr class="multipleAccordionTitle">
							<td>
								<asp:Image ID="DocumentsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="." Width="22" Height="22" />
								&nbsp;&nbsp;
                <span class="collapsiblePanelHeaderLabel cpHeaderControl">
                  <a href="#"><asp:Label runat="server" ID="GroupLabel"></asp:Label></a>
                </span>
							</td>
						</tr>
					</table>
				</asp:Panel>
					<asp:Panel ID="DocumentsPanel" runat="server">
						<div class="singleAccordionContentWrapper">
							<div class="singleAccordionContent">
								<asp:Repeater ID="DocumentRepeater" runat="server" OnItemDataBound="DocumentRepeater_ItemDataBound" OnItemCommand="DocumentRepeater_ItemCommand">
									<ItemTemplate>
										<div style="display: block; padding-bottom: 10px;">
											<div style="display: inline-block; padding-right: 15px;">
												<%# ((DocumentDto)Container.DataItem).Title%>
											</div>
											<div style="display: inline-block;">
												<asp:ImageButton ID="EditDocumentImageButton" runat="server" ImageUrl="<%# UrlBuilder.EditIconSmall() %>" CommandArgument="<%#((DocumentDto)Container.DataItem).Id %>" CommandName="EditDocument" />
											</div>
											<div style="display: inline-block;">
												<asp:ImageButton ID="DeleteDocumentImageButton" runat="server" ImageUrl="<%# UrlBuilder.DeleteIconSmall() %>" CommandArgument="<%#((DocumentDto)Container.DataItem).Id %>" CommandName="DeleteDocument" />
											</div>
										</div>
									</ItemTemplate>
								</asp:Repeater>
							</div>
						</div>
					</asp:Panel>
					<act:CollapsiblePanelExtender ID="DocumentsPanelExtender" runat="server" TargetControlID="DocumentsPanel"
						ExpandControlID="DocumentsHeaderPanel" CollapseControlID="DocumentsHeaderPanel"
						Collapsed="false" ImageControlID="DocumentsHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
						ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
			</ItemTemplate>
		</asp:Repeater>
	</div>
	<uc:DocumentModal ID="DocumentModal" runat="server" OnCompleted="DocumentModal_Completed"/>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" OnOkCommand="ConfirmationModal_OkCommand" />
	
</asp:Content>
