﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="JobSeekerProfile.aspx.cs" Inherits="Focus.Web.WebAssist.JobSeekerProfile"
    MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="Focus.Core.Views" %>
<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>
<%@ Register Src="~/Code/Controls/User/ResumePreviewModal.ascx" TagName="ResumePreviewModal"
    TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/UpdateResumeModal.ascx" TagName="UpdateResumeModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/JobSeekerActivityList.ascx" TagName="JobSeekerActivityList"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="NotesAndReminders" Src="~/WebAssist/Controls/NotesAndReminders.ascx" %>
<%@ Register Src="~/WebAssist/Controls/CandidatesLikeThisModal.ascx" TagName="CandidatesLikeThisModal"
    TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/EmailJobSeekerModal.ascx" TagName="EmailCandidateModal"
    TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/AssignedOffices.ascx" TagName="AssignedOffices"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<table style="width: 100%;" role="presentation">
		<tr>
			<td colspan="2"><asp:LinkButton ID="lnkReturn" runat="server" OnClick="lnkReturn_Click" CausesValidation="False"></asp:LinkButton></td>
		</tr>
		<tr>
			<td colspan="2"><br /></td>
		</tr>
		<tr id="JobSeekerProfileTitleRow">
			<td><h2><%= HtmlLocalise("JobSeekerProfile.Title", "#CANDIDATETYPE# profile:")%> <asp:Label runat="server" ID="lblProfileTitle"></asp:Label> <asp:Image runat="server" ID="UnderageImage" Visible="False" CssClass="tooltipImage toolTipNew" AlternateText="Underage Image"/></h2></td>
			<td style="text-align: right; vertical-align: top;"><asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3" style="float:right" ClientIDMode="Static"/></td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="HorizontalRule"></div>
			</td>
		</tr>
	</table>
	<table style="width: 100%;" role="presentation">
		<tr>
			<td>
				<table style="width: 100%;" role="presentation">
					<tr Id="ProfilePanelRow" runat="server">
						<td colspan="2">
							<asp:Panel ID="JobSeekerProfileHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
								<table role="presentation" class="accordionTable">
										<tr>
											<td>
												<asp:Image ID="JobSeekerProfileHeaderImage" runat="server" alt="."/>
												&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobSeekerProfile.Label", "Profile")%></a></span></td>
										</tr>
									</table>
							</asp:Panel>
							<div class="singleAccordionContentWrapper">
								<asp:Panel ID="JobSeekerProfilePanel" CssClass="singleAccordionContent" runat="server">
									<table role="presentation">
										<tr runat="server" Id="UnemploymentInfoRow">
											<td colspan="2">
												<b><%= HtmlLocalise("Unemployment", "Unemployment")%></b>:&nbsp;<asp:Label runat="server" ID="lblUnemployment"></asp:Label>
											</td>
										</tr>
										<tr runat="server" ID="VeteranInfoRow">
											<td style="vertical-align: top;">
												<b><%= HtmlLocalise("Veteran", "Veteran")%>:</b>&nbsp;
											</td>
											<td>
												<asp:Label runat="server" ID="lblVeteran"></asp:Label>
											</td>
										</tr>
										<tr runat="server" ID="OtherHistoryRow" Visible="False">
											<td style="vertical-align:top;">
											</td>
											<td>
												<b><%= HtmlLocalise("VeteranOther", "Previous history")%>:</b> <a href="#" id="ShowOtherHistoryLink" runat="server"></a><a href="#" id="HideOtherHistoryLink" runat="server" style="display:none"></a>
												<br />
												<asp:Label runat="server" ID="lblOtherHistory" style="display:none"></asp:Label>
												<br />
											</td>
										</tr>
										<tr runat="server" id="DisabiltyInfoRow" style="vertical-align: top;" >
                                            <td>
                                                <b><%= HtmlLocalise("Disabled", "Disabled")%>:</b>
                                            </td>
                                            <td><asp:Label runat="server" ID="lblDisabled"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="ServiceConnectedDisabiltyInfoRow">
                                            <td>
                                                <b><%= HtmlLocalise("ServiceConnectedDisability", "Service-connected disability")%></b></td><td><asp:Label
                                                    runat="server" ID="lblServiceConnectedDisability"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="HouselessWithoutShelterInfoRow"  style="vertical-align: top;">
                                        <td>
												<b><%= HtmlLocalise("HousingIssue", "Housing Issues")%>:</b>&nbsp;</td>
											<td><asp:Label runat="server" ID="lblHouseWithoutShelter"></asp:Label>
											</td>
                                        </tr>
                                        <tr runat="server" id="ExOffenderInfoRow"  style="vertical-align: top;">
                                        <td>
												<b><%= HtmlLocalise("LegalIssue", "Legal Issue")%>:</b>&nbsp;</td>
											<td><asp:Label runat="server" ID="lblExoffender"></asp:Label>
											</td>
                                        </tr>

										<tr runat="server" ID="MSFWInfoRow">
											<td colspan="2">
                                                <focus:LocalisedLabel runat="server" Id="MSFWVerifiedLabel" LocalisationKey="MSFW.Label" RenderOuterSpan="True" 
                                                    DefaultText="Have you done farm or food processing work during the past year?" CssClass="dataInputLabel"></focus:LocalisedLabel>
												<br />
												<asp:RadioButtonList ID="MSFWVerifiedOptions" runat="server" CssClass="dataInputHorizontalRadio" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="MSFWVerifiedOptions_OnSelectedIndexChanged" >
												</asp:RadioButtonList>
												<asp:CheckBoxList ID="MSFWQuestions" runat="server" CssClass="dataInputVerticalRadio" RepeatDirection="Vertical"></asp:CheckBoxList>
											</td>
										</tr>
                                        <tr id="CultureInfoRow" runat="server"  style="vertical-align: top;">
                                            <td><b><%= HtmlLocalise("LanguageCulturalIssues", "Language or Cultural Issues")%>:</b></td>
											<td><asp:Label runat="server" ID="LanguageCulturalIssues"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="IncomeInfoRow" runat="server"  style="vertical-align: top;">
                                            <td><b><%= HtmlLocalise("IncomeIssues", "Income Issues")%>:</b></td>
											<td><asp:Label runat="server" ID="IncomeIssues"></asp:Label>
                                            </td>
                                        </tr>
										</table>
								</asp:Panel>  
							</div>
							<act:CollapsiblePanelExtender ID="JobSeekerProfileCollapsiblePanelExtender" runat="server" TargetControlID="JobSeekerProfilePanel" ExpandControlID="JobSeekerProfileHeaderPanel" 
																						CollapseControlID="JobSeekerProfileHeaderPanel" ImageControlID="JobSeekerProfileHeaderImage"
																						SuppressPostBack="true" />
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<asp:Panel ID="ContactInformationHeaderPanel" runat="server" CssClass="singleAccordionTitle">
								<table role="presentation" class="accordionTable">
										<tr>
											<td>
												<asp:Image ID="ContactInformationHeaderImage" runat="server" alt="."/>
												&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ContactInformation.Label", "Contact Information")%></a></span></td>
										</tr>
									</table>
							</asp:Panel>
							<div class="singleAccordionContentWrapper">
								<asp:Panel ID="ContactInformationPanel" CssClass="singleAccordionContent" runat="server">
									<table role="presentation">
										<tr>
											<td colspan="2">
												<asp:Label runat="server" ID="lblAddress1"></asp:Label>
												<br />
												<asp:Label runat="server" ID="lblTownCity"></asp:Label> 
												<asp:Label runat="server" ID="lblState"></asp:Label> 
												<asp:Label runat="server" ID="lblZipCode"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("PhoneNumber", "Phone number")%>: <asp:Label runat="server" ID="lblPhoneNumber"></asp:Label></td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("Email", "email")%>: <asp:Label runat="server" ID="lblEmail"></asp:Label></td>
										</tr>
									</table>
								</asp:Panel>
							</div>
							<act:CollapsiblePanelExtender ID="ContactInformationCollapsiblePanelExtender" runat="server" TargetControlID="ContactInformationPanel" ExpandControlID="ContactInformationHeaderPanel" 
																						CollapseControlID="ContactInformationHeaderPanel" ImageControlID="ContactInformationHeaderImage"
																						SuppressPostBack="true" />
						</td>
					</tr>
					<tr>   
						<td colspan="2">
							<asp:Panel ID="JobSeekerResumeHeaderPanel" runat="server" CssClass="singleAccordionTitle">
								<table role="presentation" class="accordionTable">
										<tr>
											<td>
												<asp:Image ID="JobSeekerResumeHeaderImage" runat="server"  alt="."/>
												&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobSeekerResume.Label", "Resume")%></a></span></td>
										</tr>
									</table>
							</asp:Panel>
							<div class="singleAccordionContentWrapper">
								<asp:Panel ID="JobSeekerResumePanel" runat="server" CssClass="singleAccordionContent">
									<p>
										<asp:Label runat="server" ID="NoResumeLabel"></asp:Label>
									</p>
									<table role="presentation" id="JobSeekerResumeTable" runat="server" style="width:100%">
										<tr>
											<td style="width:33%;"><%= HtmlLocalise("ResumeLength", "Resume length")%>: <asp:Label runat="server" ID="lblResumeLength"></asp:Label></td>
											<td style="width:33%"><%= App.Settings.HideEnrollmentStatus ? "" : (HtmlLocalise("EnrollmentStatus", "Enrollment Status") + ":")%> <asp:Label runat="server" ID="lblEnrollmentStatus"></asp:Label></td>
											<td style="width:33%"><asp:Button runat="server" ID="UpdateResumeModalAction" class="button3" OnClick="UpdateResume_Click" CausesValidation="False"/></td>
										</tr>
										<tr>
											<td><%= HtmlLocalise("SkillsCodedFromResume", "Number of skills coded from resume")%>: <asp:Label runat="server" ID="lblSkillsCodedFromResume"></asp:Label></td>
											<td><%= HtmlLocalise("EducationLevel", "Education Level")%>: <asp:Label runat="server" ID="lblEducationLevel"></asp:Label></td>
											<td></td>
										</tr>
										<tr>
											<td><%= HtmlLocalise("NumberOfResumes", "Number of resumes")%>: <asp:Label runat="server" ID="lblNumberOfResumes"></asp:Label></td>
											<td><%= HtmlLocalise("EmploymentStatus", "Employment Status")%>: <asp:Label runat="server" ID="lblEmploymentStatus"></asp:Label></td>
											<td></td>
										</tr>
										<tr>
											<td id="searchableResumeCell" runat="server">
												<%= HtmlLocalise("ResumeIsSearchable", "Resume is searchable") %>: <asp:Label runat="server" ID="lblResumeIsSearchable"></asp:Label>
											</td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td>
												<%= HtmlLocalise("WillingToWorkOvertime", "Willing to work overtime")%>: <asp:Label runat="server" ID="lblWillingToWorkOvertime"></asp:Label>
											</td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td>
												<%= HtmlLocalise("WillingToRelocate", "Willing to relocate")%>: <asp:Label runat="server" ID="lblWillingToRelocate"></asp:Label>
											</td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td>
												<%= HtmlLocalise("WillingToRelocate", "Number of incomplete resumes")%>: <asp:Label runat="server" ID="lblNumberOfIncompleteResumes"></asp:Label>
											</td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td colspan="3">
												<br/>
											</td>
										</tr>
										<tr>
											<td>
												<asp:Literal runat="server" ID="ViewJobSeekerResumeLiteral"/>
											</td>
										</tr>
										<tr>
											<td style="white-space:nowrap">
												<asp:Repeater runat="server" ID="JobSeekerResumeRepeater" >
													<HeaderTemplate><ul></HeaderTemplate>
														<ItemTemplate>
															<li>
																<asp:LinkButton runat="server" ID="ResumeLinkButton" Text="<%# ((ResumeSummary)Container.DataItem).ResumeName %>" 
																								OnCommand="ResumeLinkButton_Command" CommandArgument="<%# ((ResumeSummary)Container.DataItem).ResumeID %>" />  (<%# ((ResumeSummary)Container.DataItem).ResumeDate.ToString("MMM d") %>)
															</li>
														</ItemTemplate>
													<FooterTemplate></ul></FooterTemplate>
												</asp:Repeater>
											</td>
											<td colspan="2"></td>
										</tr>
									</table>
								</asp:Panel>
							</div>
							<act:CollapsiblePanelExtender ID="JobSeekerResumeCollapsiblePanelExtender" runat="server" TargetControlID="JobSeekerResumePanel" ExpandControlID="JobSeekerResumeHeaderPanel" 
																						CollapseControlID="JobSeekerResumeHeaderPanel" Collapsed="true" ImageControlID="JobSeekerResumeHeaderImage"
																						SuppressPostBack="true"  />
						</td>
					</tr>
					<tr>   
						<td colspan="2">
							<asp:Panel ID="ActivitySummaryHeaderPanel" runat="server" CssClass="singleAccordionTitle">
								<table role="presentation" class="accordionTable">
										<tr>
											<td>
												<asp:Image ID="ActivitySummaryHeaderImage" runat="server" alt="."/>
												&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ActivitySummary.Label", "Activity Summary")%></a></span></td>
										</tr>
									</table>
							</asp:Panel>
							<div class="singleAccordionContentWrapper">
								<asp:Panel ID="ActivitySummaryPanel" CssClass="singleAccordionContent" runat="server">
									<asp:UpdatePanel runat="server" ID="ActivitySummaryUpdatePanel">
										<ContentTemplate>
											<p>
												<%= HtmlLocalise("DaysSinceRegistration", "Days since registration")%>: <asp:Label runat="server" ID="lblDaysSinceRegistration"></asp:Label>
												<br />
												<%= HtmlLocalise("TimeSinceLastLogin", "Time since last sign-in")%>: <asp:Label runat="server" ID="lblTimeSinceLastLogin"></asp:Label>
												<br />
												<%= HtmlLocalise("HighScoreMatches", "Current 3-5 star matches")%>: <asp:Label runat="server" ID="lblHighScoreMatches"></asp:Label>
												<br />
											</p>
											<%=HtmlLabel(DaysFilterDropDown,"ShowActivity.Text", "Show activity")%>&nbsp;<asp:DropDownList runat="server" ID="DaysFilterDropDown" Width="200"/>
											<asp:Button runat="server" ID="DaysDropDownButton" class="button1" OnClick="DaysDropDownButton_Click" CausesValidation="False"/>
											<table role="presentation">
											<tr>
												<td style="width:15%" colspan="2"><%= HtmlLocalise("FocusCareerSignins", "#FOCUSCAREER# sign-ins")%>: <asp:Label runat="server" ID="lblFocusCareerSignins"></asp:Label></td>
												<td style="width:15%" colspan="2"><asp:Label runat="server" ID="lblMatchesViewed"></asp:Label></td>
											</tr>
											<tr>
												<td colspan="2"><%= HtmlLocalise("ResumeEdited", "Resume edited")%>: <asp:Label runat="server" ID="lblResumeEdited"></asp:Label></td>
												<td colspan="2"><%= HtmlLocalise("ReferralsRequested", (CurrentTheme == FocusThemes.Education) ? "Application approval requests" : "Referrals requested")%>: <asp:Label runat="server" ID="lblReferralsRequested"></asp:Label></td>
											</tr>
											<tr>
												<td colspan="2"><%= HtmlLocalise("OnlineResumeHelpUsed", "Online resume help used")%>: <asp:Label runat="server" ID="lblOnlineResumeHelpUsed"></asp:Label></td>
												<td colspan="2"><%= HtmlLocalise("SelfReferrals", "Self-referrals")%>: <asp:Label runat="server" ID="lblSelfReferrals"></asp:Label></td>
											</tr>
											<tr>
												<td colspan="2"><%= HtmlLocalise("JobSearchesPerformed", "Job searches performed")%>: <asp:Label runat="server" ID="lblJobSearchesPerformed"></asp:Label></td>
												<td colspan="2" id="staffReferralsCell" runat="server"><%= HtmlLocalise("StaffReferrals", "Staff referrals")%>: <asp:Label runat="server" ID="lblStaffReferrals"></asp:Label></td>
											</tr>
											<tr>
												<td colspan="2"><%= HtmlLocalise("JobSearchesSavedForJobAlerts", "Job searches saved for job alerts")%>: <asp:Label runat="server" ID="lblJobSearchesSavedForJobAlerts"></asp:Label></td>
												<td colspan="2" id="totalReferralsCell" runat="server"><%= HtmlLocalise("TotalReferrals", "Total referrals")%>: <asp:Label runat="server" ID="lblTotalReferrals"></asp:Label></td>
											</tr>
											<tr>
												<td colspan="2" id="highGrowthSectorsCell" runat="server"><%= HtmlLocalise("JobSearchesByHighGrowthSectors", "Job searches by high-growth sectors")%>: <asp:Label runat="server" ID="lblJobSearchesByHighGrowthSectors"></asp:Label></td>
												<td colspan="2"></td>
											</tr>
										</table>
									</ContentTemplate>
								</asp:UpdatePanel>
							</asp:Panel>
						</div>
						<act:CollapsiblePanelExtender ID="ActivitySummaryCollapsiblePanelExtender" runat="server" TargetControlID="ActivitySummaryPanel" ExpandControlID="ActivitySummaryHeaderPanel" 
																			CollapseControlID="ActivitySummaryHeaderPanel" Collapsed="true" ImageControlID="ActivitySummaryHeaderImage"
																			SuppressPostBack="true" />
						</td>
					</tr>
					<tr id="referralOutcomesRow" runat="server">   
						<td colspan="2">
							<asp:Panel ID="ReferralOutcomesHeaderPanel" runat="server" CssClass="singleAccordionTitle">
								<table role="presentation" class="accordionTable">
									<tr>
										<td>
											<asp:Image ID="ReferralOutcomesHeaderImage" runat="server" alt="."/>
											&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ReferralOutcomes.Label", "Referral Outcomes")%></a></span></td>
									</tr>
								</table>
							</asp:Panel>
							<div class="singleAccordionContentWrapper">
								<asp:Panel ID="ReferralOutcomesPanel" CssClass="singleAccordionContent" runat="server">
									<table role="presentation" class="cellsWithPadding">
										<tr>
											<td colspan="2" >
												<%= HtmlLocalise("ApplicantsHired", "Applicants hired")%>: <asp:Label runat="server" ID="lblApplicantsHired"></asp:Label>
											</td>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsFailedToRespondToInvitation", "Applicants who failed to respond to invitation")%>: <asp:Label runat="server" ID="lblApplicantsFailedToRespondToInvitation"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsInterviewed", "Applicants interviewed/pending interviews")%>: <asp:Label runat="server" ID="lblApplicantsInterviewed"></asp:Label>
											</td>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsFoundJobFromOtherSource", "Applicants who found a job from other source")%>: <asp:Label runat="server" ID="lblApplicantsFoundJobFromOtherSource"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsJobAlreadyFilled", "Applicants job already filled")%>: <asp:Label runat="server" ID="lblApplicantsJobAlreadyFilled"></asp:Label>
											</td>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsRefused", "Applicants who refused jobs")%>: <asp:Label runat="server" ID="lblApplicantsRefused"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsNotYetPlaced", "Applicants not yet placed")%>: <asp:Label runat="server" ID="lblApplicantsNotYetPlaced"></asp:Label>
											</td>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsRefusedReferral", "Applicants who refused referral")%>: <asp:Label runat="server" ID="lblApplicantsRefusedReferral"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsRejected", "Applicants rejected")%>: <asp:Label runat="server" ID="lblApplicantsRejected"></asp:Label>
											</td>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsNotQualified", "Applicants who were not qualified")%>: <asp:Label runat="server" ID="lblApplicantsNotQualified"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsUnderConsideration", "Applicants under consideration")%>: <asp:Label runat="server" ID="lblApplicantsUnderConsideration"></asp:Label>
											</td>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsInterviewDenied", "Interviews denied")%>: <asp:Label runat="server" ID="lblApplicantsInterviewDenied"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsDidNotApply", "Applicants who did not apply")%>: <asp:Label runat="server" ID="lblApplicantsDidNotApply"></asp:Label>
											</td>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsNewApplicant", "New applicants")%>: <asp:Label runat="server" ID="lblApplicantsNewApplicant"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsFailedToShow", "Applicants who failed to show")%>: <asp:Label runat="server" ID="lblApplicantsFailedToShow"></asp:Label>
											</td>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsRecommended", "Recommended applicants")%>: <asp:Label runat="server" ID="lblApplicantsRecommended"></asp:Label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<%= HtmlLocalise("ApplicantsFailedToReportToJob", "Applicants who failed to report to job")%>: <asp:Label runat="server" ID="lblApplicantsFailedToReportToJob"></asp:Label>
											</td>
											<td colspan="2">
												&nbsp;
											</td>
										</tr>
									</table>
								</asp:Panel>
							</div>
						<act:CollapsiblePanelExtender ID="ReferralOutcomesHeaderCollapsiblePanelExtender" runat="server" TargetControlID="ReferralOutcomesPanel" ExpandControlID="ReferralOutcomesHeaderPanel" 
																			CollapseControlID="ReferralOutcomesHeaderPanel" Collapsed="true" ImageControlID="ReferralOutcomesHeaderImage"
																			SuppressPostBack="true" />
						</td>
					</tr>
					<tr id="surveyResponsesRow" runat="server">   
						<td colspan="2">
							<asp:Panel ID="SurveyResponsesHeaderPanel" runat="server" CssClass="singleAccordionTitle">
								<table role="presentation" class="accordionTable">
										<tr>
											<td>
												<asp:Image ID="SurveyResponsesHeaderImage" runat="server" alt="."/>
												&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("SurveyResponses.Label", "Survey Responses")%></a></span></td>
										</tr>
									</table>
							</asp:Panel>
							<div class="singleAccordionContentWrapper">
								<asp:Panel ID="SurveyResponsesPanel" CssClass="singleAccordionContent" runat="server">
									<table role="presentation">
										<tr>
											<td colspan="2"><%= HtmlLocalise("MatchQualityDissatisfied", "Very dissatisfied with match quality")%>: <asp:Label runat="server" ID="lblMatchQualityDissatisfied"></asp:Label></td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("MatchQualitySomwwhatDissatisfied", "Somewhat dissatisfied with match quality")%>: <asp:Label runat="server" ID="lblMatchQualitySomewhatDissatisfied"></asp:Label></td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("MatchQualitySomewhatSatisfied", "Somewhat satisfied with match quality")%>: <asp:Label runat="server" ID="lblMatchQualitySomewhatSatisfied"></asp:Label></td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("MatchQualitySatisfied", "Very satisfied with match quality")%>: <asp:Label runat="server" ID="lblMatchQualitySatisfied"></asp:Label></td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("NotReceiveUnexpectedMatches", "Did not receive unexpected matches")%>: <asp:Label runat="server" ID="lblNotReceiveUnexpectedMatches"></asp:Label></td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("ReceivedUnexpectedResults", "Received unexpected results")%>: <asp:Label runat="server" ID="lblReceivedUnexpectedResults"></asp:Label></td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("NotEmployerInvitationsReceive", "No #BUSINESS#:LOWER invitations were received")%>: <asp:Label runat="server" ID="lblNotEmployerInvitationsReceive"></asp:Label></td>
										</tr>
										<tr>
											<td colspan="2"><%= HtmlLocalise("ReceivedEmployerInvitations", "Received #BUSINESS#:LOWER invitations")%>: <asp:Label runat="server" ID="lblReceivedEmployerInvitations"></asp:Label></td>
										</tr>
									</table>
								</asp:Panel>
							</div>
						<act:CollapsiblePanelExtender ID="SurveyResponsesCollapsiblePanelExtender" runat="server" TargetControlID="SurveyResponsesPanel" ExpandControlID="SurveyResponsesHeaderPanel" 
																			CollapseControlID="SurveyResponsesHeaderPanel" Collapsed="true" ImageControlID="SurveyResponsesHeaderImage"
																			SuppressPostBack="true" />
						</td>
					</tr>
					<tr>   
			<td colspan="2">
				<asp:Panel ID="ActivityLogHeaderPanel" runat="server" CssClass="singleAccordionTitle">
					<table role="presentation" class="accordionTable">
							<tr>
								<td>
									<asp:Image ID="ActivityLogHeaderImage" runat="server" alt="."/>
									&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ActivityLog.Label", "#CANDIDATETYPE#:TITLE Activity Log")%></a></span></td>
							</tr>
						</table>
				</asp:Panel>
				<div class="singleAccordionContentWrapper">
					<asp:Panel ID="ActivityLogPanel" CssClass="singleAccordionContent" runat="server">
						<uc:JobSeekerActivityList ID="JobSeekerActivityList" runat="server" OnJobSeekerActivityListEmailCandidateClick="JobSeekerActivityListEmailCandidate_Click"/>	
					</asp:Panel>
				</div>
				<act:CollapsiblePanelExtender ID="ActivityLogCollapsiblePanelExtender" runat="server" TargetControlID="ActivityLogPanel" ExpandControlID="ActivityLogHeaderPanel" 
																			CollapseControlID="ActivityLogHeaderPanel" Collapsed="true" ImageControlID="ActivityLogHeaderImage"
																			SuppressPostBack="true" />
			</td>
		</tr>

					<tr>   
						<td colspan="2">
							<asp:Panel ID="NotesAndRemindersHeaderPanel" runat="server" CssClass="singleAccordionTitle">
								<table role="presentation" class="accordionTable">
										<tr>
											<td>
												<asp:Image ID="NotesAndRemindersHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" alt="."/>
												&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("NotesAndReminders.Label", "Notes and Reminders")%></a></span></td>
										</tr>
									</table>
							</asp:Panel>
							<div class="singleAccordionContentWrapper">
								<asp:Panel ID="NotesAndRemindersPanel" CssClass="singleAccordionContent" runat="server">
									<uc:NotesAndReminders ID="NotesReminders" runat="server" />
								</asp:Panel>
							</div>
							<act:CollapsiblePanelExtender ID="NotesAndRemindersCollapsiblePanelExtender" runat="server" TargetControlID="NotesAndRemindersPanel" ExpandControlID="NotesAndRemindersHeaderPanel" 
																						CollapseControlID="NotesAndRemindersHeaderPanel" Collapsed="true" ImageControlID="NotesAndRemindersHeaderImage"
																						CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																						SuppressPostBack="true" BehaviorID="NotesAndRemindersCollapsiblePanel"/>
						</td>
					</tr>

					<tr runat="server" ID="OfficeRow">   
						<td colspan="2">
							<asp:Panel ID="AssignedOfficesHeaderPanel" runat="server" CssClass="singleAccordionTitle" style="margin-bottom: 20px !important;">
								<table role="presentation" class="accordionTable">
										<tr>
											<td>
												<asp:Image ID="AssignedOfficesHeaderImage" runat="server" alt="."/>
												&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AssignedOffices.Label", "Assigned Offices")%></a></span></td>
										</tr>
									</table>
							</asp:Panel>
							<div class="singleAccordionContentWrapper">
								<asp:Panel ID="AssignedOfficesPanel" CssClass="singleAccordionContent" runat="server">
									<uc:AssignedOffices ID="AssignedOffices" runat="server" OnSaveAssignedOfficesClick="SaveAssignedOffices" AdministrationType="JobSeeker" />
								</asp:Panel>
							</div>
							<act:CollapsiblePanelExtender ID="AssignedOfficesCollapsiblePanelExtender" runat="server" TargetControlID="AssignedOfficesPanel" ExpandControlID="AssignedOfficesHeaderPanel" 
																						CollapseControlID="AssignedOfficesHeaderPanel" Collapsed="true" ImageControlID="AssignedOfficesHeaderImage"
																						SuppressPostBack="true" BehaviorID="AssignedOfficeCollapsiblePanel"/>
						</td>
					</tr>

				</table>
			</td>
  
		</tr>
	</table>
	<uc:ResumePreviewModal ID="ShowResume" runat="server" OnResumePreviewEmailCandidateClick="ResumePreviewModalEmailCandidate_Click" OnResumePreviewCandidatesLikeMeClick="ResumePreviewCandidatesLikeMe_Click" />
	<uc:CandidatesLikeThisModal ID="CandidatesLikeThis" runat="server" OnCandidatesLikeThisEmailCandidateClick="CandidatesLikeThisModalEmailCandidate_Click"/>
	<uc:EmailCandidateModal ID="EmailCandidate" runat="server" OnCompleted="EmailCandidate_Completed" />
  <uc:UpdateResumeModal ID="UpdateResume" runat="server" OnUpdated="UpdateResume_OnUpdated" />
	<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
	
	<script type="text/javascript">

		Sys.Application.add_load(JobSeekerProfile_PageLoad);

		function JobSeekerProfile_PageLoad() {
			var button = $("#<%= ExpandCollapseAllButton.ClientID %>");
			if (button.length > 0)
				bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>', null); // bind the magic

				accordionLayoutPostbackFix(); // FVN-3128
		}
		 
</script>

</asp:Content>

