﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.Services;
using System.Web.UI.WebControls;

using Focus.Core;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.Criteria.Report;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Web.Code;
using Focus.Web.WebAssist.Controls;

using Framework.Core;

using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekersAdministrator)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekersSendMessages)]
  public partial class MessageJobSeekers : PageBase
	{
		#region Properties
 
		private int _jobSeekersCount;
    private List<SavedMessageTextDto> _savedMessageTexts;

    private JobSeekersReportCriteria JobSeekerCriteria
    {
      get { return GetViewStateValue<JobSeekersReportCriteria>("MessageJobSeekers:JobSeekerCriteria"); }
      set { SetViewStateValue("MessageJobSeekers:JobSeekerCriteria", value); }
    }

    private JobSeekerCriteria BasicCriteria
    {
      get { return GetViewStateValue<JobSeekerCriteria>("MessageJobSeekers:BasicCriteria"); }
      set { SetViewStateValue("MessageJobSeekers:BasicCriteria", value); }
    }

    private bool UseCriteria
    {
      get { return GetViewStateValue<bool>("MessageJobSeekers:UseCriteria"); }
      set { SetViewStateValue("MessageJobSeekers:UseCriteria", value); }
    }

    private const int JobSeekersLimit = 500;

		#endregion

		/// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      Page.Title = CodeLocalise("Page.Title", "Message #CANDIDATETYPE#"); //Replaced plural: Job Seekers
      LocaliseUI();

      if (!IsPostBack)
      {
        ApplyTheme();

        BindListsList();

	      if (App.Settings.Theme == FocusThemes.Workforce)
		      BindActionDropDown();
	      else
	      {
					BindMessageFormatRadioButtonList();
					SendMessage.Bind(GetAction());
	      }
					
        CriteriaLocation.BindCriteria(null, ReportType.JobSeeker);
      }

			if (App.Settings.Theme == FocusThemes.Workforce)
				MessageModal.Completed += SendMessageAction_Completed;
			else
				SendMessage.Completed += SendMessageAction_Completed;

			CriteriaJobSeekerCharacteristics.Visible = !App.Settings.HideJSCharacteristicsFromMessages;
    }

		#region Events

		/// <summary>
		/// Handles the SelectedIndexChanged event of the MessageFormatRadioButtonList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void MessageFormatRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
		{
			SendMessage.Bind(GetAction());
		}

		/// <summary>
		/// Handles the Clicked event of the LookupJobButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void LookupJobButton_Clicked(object sender, EventArgs e)
		{
			JobLookup.Show();
		}

		/// <summary>
		/// Handles the OkCommand event of the JobSeekerList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobSeekerList_OkCommand(object sender, EventArgs e)
		{
			BindListsList();
		}

		/// <summary>
		/// Handles the OkCommand event of the JobLookup control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.JobLookupEventArgs"/> instance containing the event data.</param>
		protected void JobLookup_OkCommand(object sender, JobLookupEventArgs e)
		{
			if (App.Settings.Theme == FocusThemes.Workforce)
				MessageModal.Show(ActionTypes.SendCandidateEmails, GetSelectedJobSeekerIds(), e.Url);
			else
				SendMessage.Bind(GetAction(), GetSelectedJobSeekerIds(), e.Url);
		}

		/// <summary>
		/// Handles the Clicked event of the FindButton_ control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FindButton_Clicked(object sender, EventArgs e)
		{
			UnbindSearch();
			BindJobSeekersList(true);
			FindButton.Visible = false;

      if (App.Settings.ReportingEnabled)
		  {
        CriteriaSelectionPanel.Visible = false;
        
        CriteriaDisplay.Visible = true;
		    CriteriaDisplay.SetCriteriaPanelCollapsedState(false);
		    CriteriaDisplay.ShowCriteria(JobSeekerCriteria);
		  }
		}


    /// <summary>
    /// Handles the ActionRequested event of the ReportCriteriaDisplay control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ReportCriteriaDisplay_ActionRequested(object sender, CommandEventArgs eventargs)
    {
      switch (eventargs.CommandName)
      {
        case "EditCriteria":
          if (App.Settings.ReportingEnabled)
          {
            CriteriaSelectionPanel.Visible = true;
            CriteriaDisplay.Visible = false;
	          FindButton.Visible = true;
          }
          break;

        case "NewReport":
          Response.Redirect(UrlBuilder.MessageJobSeekers());
          break;
      }
    }

		/// <summary>
		/// Handles the Clicked event of the AddJobSeekersButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddJobSeekersButton_Clicked(object sender, EventArgs e)
		{
			var details = GetSelectedJobSeekersDetails();
			JobSeekerList.Show(details);
		}

		/// <summary>
		/// Handles the Clicked event of the SelectListButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SelectListButton_Clicked(object sender, EventArgs e)
		{
      CriteriaDisplay.SetCriteriaPanelCollapsedState(true);

			BindJobSeekersList(false);
		}

		/// <summary>
		/// Handles the Clicked event of the DeleteJobSeekersButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void DeleteJobSeekersFromListButton_Clicked(object sender, EventArgs e)
		{
			var listSelectedValue = ListsDropDownList.SelectedValue.Split(':');

			if (listSelectedValue.Length == 2)
			{
				var id = long.Parse(listSelectedValue[1]);

				ActionConfirmation.Show(CodeLocalise("DeleteListMembers.Title", "Delete #CANDIDATETYPES#:LOWER from list"),
					CodeLocalise("DeleteListMembers.Body", "Are you sure you want to delete the selected #CANDIDATETYPES#:LOWER from the list?"),
					CodeLocalise("Global.Cancel.Text", "Cancel"),
					okText: CodeLocalise("DeleteListMembers.Ok.Delete list", "Delete #CANDIDATETYPES#:LOWER"), okCommandName: "DeleteListMembers", okCommandArgument: id.ToString(), height: 50);
			}
		}

		/// <summary>
		/// Handles the Clicked event of the DeleteListButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void DeleteListButton_Clicked(object sender, EventArgs e)
		{
			var listSelectedValue = ListsDropDownList.SelectedValue.Split(':');

			if (listSelectedValue.Length == 2)
			{
				var id = long.Parse(listSelectedValue[1]);

				ActionConfirmation.Show(CodeLocalise("DeleteList.Title", "Delete list"),
					CodeLocalise("DeleteList.Body", "Are you sure you want to delete the selected list?"),
					CodeLocalise("Global.Cancel.Text", "Cancel"),
					okText: CodeLocalise("DeleteList.Ok.Delete list", "Delete list"), okCommandName: "DeleteList", okCommandArgument: id.ToString(), height: 50);
			}
		}

		/// <summary>
		/// Handles the OkCommand event of the ActionConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void ActionConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			long listId;
			long.TryParse(e.CommandArgument.ToString(), out listId);

			switch (e.CommandName)
			{
				case "DeleteListMembers":
					DeleteListMembers(listId);
					break;

				case "DeleteList":
					DeleteList(listId);
					break;
			}
		}

		/// <summary>
		/// Handles the Clicked event of the ActionGoButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void ActionGoButton_Clicked(object sender, EventArgs e)
		{
		  var action = ActionDropDown.SelectedValueToEnum(ActionTypes.NoAction);

			switch (action)
			{
				case ActionTypes.SendCandidateEmails:
				case ActionTypes.CreateCandidateHomepageAlerts:
					ActivityDiv.Visible = false;
					MessageModal.Show(action, GetSelectedJobSeekerIds());
					break;

				case ActionTypes.AssignActivityToCandidate:
					PrepareAssignActivity();
					break;

				case ActionTypes.LookupJobForJobSeeker:
					ActivityDiv.Visible = false;
					JobLookup.Show();
					break;
			}
	  }

		/// <summary>
		/// Handles the Completed event of the SendMessageAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="SendMessages.SendMessageEventArgs"/> instance containing the event data.</param>
		protected void SendMessageAction_Completed(object sender, SendMessages.SendMessageEventArgs e)
		{
			if (e.Title.IsNotNullOrEmpty() && e.Message.IsNotNullOrEmpty())
			{
				ConfirmationModal.Show(e.Title,
															 e.Message,
															 CodeLocalise("CloseModal.Text", "OK"));
			}
		}

		/// <summary>
		/// Activities the assignment_ activity selected.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The e.</param>
		protected void ActivityAssignment_ActivitySelected(object sender, ActivityAssignment.ActivitySelectedArgs e)
		{
			var activityId = e.SelectedActivityId;

			var jobSeekersSelected = GetSelectedJobSeekersDetails();

			bool activityStatus=ServiceClientLocator.CandidateClient(App).AssignActivityToMultipleUsers(activityId, jobSeekersSelected.ToList(), e.ActionedDate);
            if (activityStatus)
			Confirmation.Show(CodeLocalise("AssignActivityConfirmation.Title", "Activity/Service assigned successfully"),
				CodeLocalise("AssignActivityConfirmation.Body", "The selected activity/service has been assigned successfully"),
				CodeLocalise("CloseModal.Text", "OK"));
            else
                Confirmation.Show("Validation Error",
                "Staff does not have permission to perform this action",
                CodeLocalise("CloseModal.Text", "OK"));


			ClearActionControls();
			//HideSubActions();
		}

	  #endregion

    #region DataSource Methods

    /// <summary>
    /// Handles the OnDataBound event of the JobSeekersList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void JobSeekersList_OnDataBound(object sender, EventArgs e)
    {
      if (JobSeekersList.Items.IsNullOrEmpty())
        return; // No data so empty data template is displayed

			var officeCell = JobSeekersList.FindControl("OfficeHeaderCell");

			if (officeCell.IsNotNull())
				officeCell.Visible = App.Settings.OfficesEnabled;
    }

    /// <summary>
    /// Handles the OnItemDataBound event of the JobSeekersList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void JobSeekersList_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var checkBox = (CheckBox)e.Item.FindControl("SelectorCheckBox");
      checkBox.Attributes.Add("data-value", ((JobSeekerView) e.Item.DataItem).PersonId.ToString(CultureInfo.InvariantCulture));

      var jobSeekerItem = (JobSeekerView)e.Item.DataItem;

      if (App.Settings.OfficesEnabled)
        ((Literal) e.Item.FindControl("OfficeLiteral")).Text = jobSeekerItem.Offices;
      else
        e.Item.FindControl("OfficeCell").Visible = false;
    }

    /// <summary>
    /// Handles the OnSelecting event of the SearchResultDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void SearchResultDataSource_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["reportCriteria"] = null;
      e.InputParameters["basicCriteria"] = null;

      if (UseCriteria)
      {
        if (App.Settings.ReportingEnabled)
          e.InputParameters["reportCriteria"] = JobSeekerCriteria;
        else
          e.InputParameters["basicCriteria"] = BasicCriteria;
      }

      e.InputParameters["listSelectedValue"] = UseCriteria ? string.Empty : ListsDropDownList.SelectedValue;
    }

    /// <summary>
    /// Gets the job seekers.
    /// </summary>
    /// <param name="reportCriteria">The criteria for reporting search.</param>
    /// <param name="basicCriteria">The criteria for basic search.</param>
    /// <param name="listSelectedValue">The value of the selected list drop down</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="startRowIndex">Start index of the row.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <returns>A list of job seekers</returns>
    public List<JobSeekerView> GetJobSeekers(JobSeekersReportCriteria reportCriteria, JobSeekerCriteria basicCriteria, string listSelectedValue, string orderBy, int startRowIndex, int maximumRows)
    {
      var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

      List<JobSeekerView> jobSeekers = null;
      _jobSeekersCount = 0;

      if (reportCriteria.IsNotNull())
      {
        jobSeekers = new List<JobSeekerView>();

				reportCriteria.SubscribedOnly = App.Settings.UnsubscribeFromJSMessages;

        reportCriteria.PageSize = maximumRows;
        reportCriteria.PageIndex = pageIndex;

        var jobSeekersFromReport = ServiceClientLocator.ReportClient(App).GetJobSeekersReport(reportCriteria);
        jobSeekersFromReport.ForEach(seeker => jobSeekers.Add(new JobSeekerView
        {
          PersonId = seeker.FocusPersonId,
          FirstName = seeker.FirstName,
          LastName = seeker.LastName,
          EmailAddress = seeker.EmailAddress,
          Offices = seeker.Office.Replace("||", ", ")
        }));

        _jobSeekersCount = jobSeekersFromReport.TotalCount;
      }
      else if (basicCriteria.IsNotNull())
      {
        basicCriteria.PageSize = maximumRows;
        basicCriteria.PageIndex = pageIndex;

        var pagedJobSeekers = ServiceClientLocator.CandidateClient(App).SearchJobSeekers(basicCriteria);
        jobSeekers = pagedJobSeekers;
        _jobSeekersCount = pagedJobSeekers.TotalCount;
      }
      else if (listSelectedValue.IsNotNullOrEmpty())
      {
        var listsOrSavedSearch = listSelectedValue.Split(':');

        if (listsOrSavedSearch.Length == 2)
        {
          var listsOrSavedSearchType = (ListOrSavedSearchType)Enum.Parse(typeof(ListOrSavedSearchType), listsOrSavedSearch[0]);
          var id = long.Parse(listsOrSavedSearch[1]);

          if (listsOrSavedSearchType == ListOrSavedSearchType.List)
          {
            var pagedJobSeekers = ServiceClientLocator.CandidateClient(App).GetJobSeekersForPagedList(id, pageIndex, maximumRows);
            _jobSeekersCount = pagedJobSeekers.TotalCount;
            jobSeekers = pagedJobSeekers;
          }
        }
      }

      return jobSeekers;
    }

    /// <summary>
    /// Gets the job seekers count.
    /// </summary>
    /// <returns></returns>
    public int GetJobSeekersCount()
    {
      return _jobSeekersCount;
    }

	  /// <summary>
	  /// Gets the job seekers count.
	  /// </summary>
    /// <param name="reportCriteria">The criteria for reporting search.</param>
    /// <param name="basicCriteria">The criteria for basic search.</param>
    /// <param name="listSelectedValue">The value of the selected list drop down</param>
	  /// <returns></returns>
    public int GetJobSeekersCount(JobSeekersReportCriteria reportCriteria, JobSeekerCriteria basicCriteria, string listSelectedValue)
    {
      return _jobSeekersCount;
    }

    #endregion

    #region Bind / Unbind Methods

    /// <summary>
		/// Binds the lists list.
		/// </summary>
		private void BindListsList()
		{
			ListsDropDownList.Items.Clear();

			var listsOrSavedSearches = new List<ListOrSavedSearch>();

			listsOrSavedSearches.AddRange(ServiceClientLocator.CandidateClient(App).GetJobSeekerLists(ListTypes.AssistJobSeeker).Select(list => new ListOrSavedSearch { Id = string.Concat(ListOrSavedSearchType.List, ":", list.Id), Text = list.Text }));

			//listsOrSavedSearches.AddRange(ServiceClientLocator.SearchClient(App).GetCandidateSavedSearch().Select(savedSearch => new ListOrSavedSearch { Id = string.Concat(ListOrSavedSearchType.SavedSearch + ":" + savedSearch.Id), Text = savedSearch.Name }));

			ListsDropDownList.DataSource = listsOrSavedSearches;
			ListsDropDownList.DataValueField = "Id";
			ListsDropDownList.DataTextField = "Text";
			ListsDropDownList.DataBind();

			ListsDropDownList.Items.AddLocalisedTopDefault("JobSeekerListsDropDown.TopDefault.NoEdit", "- select list -");
		}

		/// <summary>
		/// Gets the employees.
		/// </summary>
		/// <returns></returns>
		private void BindJobSeekersList(bool useCriteria)
		{
		  UseCriteria = useCriteria;
			JobSeekersList.DataBind();

      SelectedJobSeekerIds.Value = "";

			// Set visibility of other controls
		  JobSeekersList.Visible = true;
			PageInstructions.Visible = false;
		  PagerDiv.Visible = true;
			SelectedCountDiv.Visible = true;

		}

		/// <summary>
		/// Binds the action drop down.
		/// </summary>
	  private void BindActionDropDown()
	  {
			ActionDropDown.Items.Clear();
          if(App.Settings.EnableJSActivityBackdatingDateSelection)
              ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignActivityToCandidate, "Assign activities/services"), ActionTypes.AssignActivityToCandidate.ToString()));
			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.SendCandidateEmails, "Email recipients"), ActionTypes.SendCandidateEmails.ToString()));
			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.LookupJobForJobSeeker, "Look up job for selected #CANDIDATETYPES#:LOWER"), ActionTypes.LookupJobForJobSeeker.ToString()));
			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.CreateCandidateHomepageAlerts, "Post to homepage"), ActionTypes.CreateCandidateHomepageAlerts.ToString()));
			
			ActionDropDown.Items.AddLocalisedTopDefault("ActionDropDown.TopDefault", "- select action -");
	  }

		/// <summary>
		/// Binds the message format radio button list.
		/// </summary>
		private void BindMessageFormatRadioButtonList()
		{
			MessageFormatRadioButtonList.Items.Clear();

			MessageFormatRadioButtonList.Items.AddEnum(MessageFormats.Email, "email");
			MessageFormatRadioButtonList.Items.AddEnum(MessageFormats.HomepageAlert, "post to home page");

			MessageFormatRadioButtonList.SelectedIndex = 0;
		}

		/// <summary>
		/// Unbinds the search.
		/// </summary>
		private void UnbindSearch()
		{
      if (App.Settings.ReportingEnabled)
		  {
		    JobSeekerCriteria = new JobSeekersReportCriteria
		    {
		      DisplayType = ReportDisplayType.Table,
		      JobSeekerStatusInfo = CriteriaJobSeekerStatus.Unbind(),
		      VeteranInfo = CriteriaVeteranStatus.Unbind(),
		      CharacteristicsInfo = CriteriaJobSeekerCharacteristics.Unbind(),
		      QualificationsInfo = CriteriaJobSeekerQualifications.Unbind(),
		      DateRangeInfo = CriteriaDateRange.UnBind(),
		      SalaryInfo = CriteriaOccupationAndSalary.UnbindSalary(),
		      OccupationInfo = CriteriaOccupationAndSalary.UnbindOccupations(),
		      KeywordInfo = CriteriaKeywordAndContext.Unbind(),
		      ResumeInfo = CriteriaResume.Unbind(),
					IndividualCharacteristicsInfo = CriteriaIndividualJobSeekerCharacteristics.Unbind(),
		      JobSeekerActivitySummaryInfo = CriteriaJobSeekerActivitySummary.Unbind(),
		      JobSeekerActivityLogInfo = CriteriaJobSeekerActivityLog.Unbind(),
		      LocationInfo = CriteriaLocation.Unbind(),
		      JobSeekerReferralOutcomesInfo = CriteriaReferralsOutcome.Unbind(),
		      ChartGroupInfo = null,
		      PageSize = JobSeekersLimit,
		      ZeroesHandling = ReportZeroesHandling.ExcludeAnyZeroes
		    };
		  }
		  else
		  {
        BasicCriteria = new JobSeekerCriteria
		    {
		      Firstname = FirstNameTextBox.TextTrimmed(),
		      Lastname = LastNameTextBox.TextTrimmed(),
		      EmailAddress = EmailAddressTextBox.TextTrimmed(),
		      PhoneNumber = PhoneNumberTextBox.TextTrimmed()
		    };
		  }
		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Gets the action.
		/// </summary>
		/// <returns></returns>
	  private ActionTypes GetAction()
	  {
			var selectedMessageFormat = MessageFormatRadioButtonList.SelectedValueToEnum<MessageFormats>();

			var action = ActionTypes.NoAction;

			switch (selectedMessageFormat)
			{
				case MessageFormats.Email:
					action = ActionTypes.SendCandidateEmails;
					break;
				case MessageFormats.HomepageAlert:
					action = ActionTypes.CreateCandidateHomepageAlerts;
					break;
			}

		  return action;
	  }

		/// <summary>
		/// Gets the selected job seeker ids.
		/// </summary>
		/// <returns></returns>
		private List<long> GetSelectedJobSeekerIds()
		{
			return SelectedJobSeekerIds.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(id => id.AsLong()).ToList();
		}

		/// <summary>
		/// Gets the selected job seekers details.
		/// </summary>
		/// <returns></returns>
		private JobSeekerView[] GetSelectedJobSeekersDetails()
		{
			var indexes = (from item in JobSeekersList.Items
											 let check = (CheckBox)item.FindControl("SelectorCheckBox")
											 where check.Checked
											 select item.DataItemIndex).ToList();

			var tempIds = new List<int>();
			var pageSize = SearchResultListPager.PageSize;
			var page = SearchResultListPager.CurrentPage;

			foreach (var index in indexes)
			{
				if (index >= pageSize)
				{
					tempIds.Add(index - pageSize * (page - 1));
				}
				else
				{
					tempIds.Add(index);
				}
			}

			return tempIds.Select(id => new JobSeekerView
			{
				PersonId = Convert.ToInt64(JobSeekersList.DataKeys[id]["PersonId"]),
				FirstName = JobSeekersList.DataKeys[id]["FirstName"].ToString(),
				LastName = JobSeekersList.DataKeys[id]["LastName"].ToString(),
				EmailAddress = JobSeekersList.DataKeys[id]["EmailAddress"].ToString()
			}).ToArray();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ComposeMessageButton.Text = CodeLocalise("ComposeMessageButton.Text.NoEdit", "Compose message");

      CriteriaDisplay.ButtonText = CodeLocalise("CriteriaDisplay.ButtonText", "search");

      Column1Header.Text = CodeLocalise("Column1Header.JobSeeker.Text", "Which job seekers are you looking for?");
      Column2Header.Text = CodeLocalise("Column2Header.JobSeeker.Text", "What have the job seekers been doing?");
      Column3Header.Text = CodeLocalise("Column3Header.JobSeeker.Text", "Where are the job seekers located, and what's the overall time period for this search?");

			FindButton.Text = BasicFindButton.Text = CodeLocalise("Global.Find.Text.NoEdit", "Find");
			AddJobSeekersButton.Text = CodeLocalise("AddJobSeekersButton.Text.NoEdit", "Add #CANDIDATETYPE#:LOWER to list"); //Replaced plural: seekers
			LookupJobButton.Text = CodeLocalise("LookupJobButton.Text.NoEdit", "Look up job for selected #CANDIDATETYPE#:LOWER"); //Replaced plural: seekers

			PageInstructions.Text = CodeLocalise("PageInstructions.Text", "Select message recipients from a list or saved search, or find them at left. Results will appear here.");
			SelectListButton.Text = CodeLocalise("SelectListButton.Text.NoEdit", "Select");

			JobSeekersListValidator.ErrorMessage = ListMembersListValidator.ErrorMessage = CodeLocalise("JobSeekersListValidator.ErrorMessage", "At least one #CANDIDATETYPE#:LOWER must be selected");

		  ListsDropDownRequiredValidator.ErrorMessage = CodeLocalise("ListsDropDownRequiredValidator.ErrorMessage", "Please select a list");

			DeleteJobSeekersFromListButton.Text = CodeLocalise("DeleteJobSeekersFromListButton.Text", "Delete #CANDIDATETYPES#:LOWER");
			DeleteListButton.Text = CodeLocalise("DeleteListButton.Text", "Delete list");

			ActionGoButton.Text = CodeLocalise("GoButton.Text", "Go");

			ActionRequired.ErrorMessage = CodeLocalise("ActionRequired.ErrorMessage", "An action is required");
		}

    /// <summary>
    /// Hides/Shows controls depending on settings
    /// </summary>
    private void ApplyTheme()
    {
      if (App.Settings.ReportingEnabled)
      {
        CriteriaSelectionPanel.Visible = true;
	      FindButton.Visible = true;
        BasicSelectionPanel.Visible = false;
      }
      else
      {
        CriteriaSelectionPanel.Visible = false;
				FindButton.Visible = false;
				BasicSelectionPanel.Visible = true;
      }

			MessageModal.Visible = DeleteListButton.Visible = DeleteJobSeekersFromListButton.Visible = JobSeekerSelectedPlaceHolder.Visible = ActionDiv.Visible = App.Settings.Theme == FocusThemes.Workforce;

			SeparatorPlaceHolder.Visible = ComposeMessagePlaceHolder.Visible = ComposeMessageButtonPlaceHolder.Visible = LookupJobButtonPlaceHolder.Visible = App.Settings.Theme == FocusThemes.Education;

	    SendMessagesTitleLabel.DefaultText = CodeLocalise("PageTitle.Text", App.Settings.Theme == FocusThemes.Workforce ? "Manage #CANDIDATETYPE# communication" : "Send Messages");
    }

		/// <summary>
		/// Deletes the list.
		/// </summary>
		/// <param name="listId">The list identifier.</param>
		private void DeleteList(long listId)
		{
			ServiceClientLocator.CandidateClient(App).DeleteJobSeekerList(listId);

			CriteriaDisplay.OnActionRequested(new CommandEventArgs("NewReport", null));

			Confirmation.Show(CodeLocalise("DeleteListConfirmation.Title", "List deleted"),
												CodeLocalise("DeleteListConfirmation.Body", "The list has been deleted."),
												CodeLocalise("CloseModal.Text", "OK"), height: 50);
		}

		/// <summary>
		/// Deletes the list members.
		/// </summary>
		/// <param name="listId">The list identifier.</param>
		private void DeleteListMembers(long listId)
		{
			var listMembersToRemove = GetSelectedJobSeekerFromList(JobSeekersList);

			ServiceClientLocator.CandidateClient(App).RemoveJobSeekersFromList(listId, listMembersToRemove);

			BindJobSeekersList(false);

			Confirmation.Show(CodeLocalise("DeleteListMembersConfirmation.Title", "List deleted"),
												CodeLocalise("DeleteListMembersConfirmation.Body", "The members have been deleted from the list."),
												CodeLocalise("CloseModal.Text", "OK"), height: 50);
		}

		/// <summary>
		/// Gets the selected job seeker from list.
		/// </summary>
		/// <param name="list">The list.</param>
		/// <returns></returns>
		private static JobSeekerView[] GetSelectedJobSeekerFromList(ListView list)
		{
			var dataItems = (from item in list.Items
											 let radio = (CheckBox)item.FindControl("SelectorCheckBox")
											 where radio.Checked
											 select item).ToList();

			return dataItems.Select(dataItem => new JobSeekerView
			{
				PersonId = Convert.ToInt64(list.DataKeys[dataItem.DataItemIndex]["PersonId"]),
				FirstName = list.DataKeys[dataItem.DataItemIndex]["FirstName"].ToString(),
				LastName = list.DataKeys[dataItem.DataItemIndex]["LastName"].ToString(),
				EmailAddress = list.DataKeys[dataItem.DataItemIndex]["EmailAddress"].ToString()
			}).ToArray();
		}

		/// <summary>
		/// Clears the action controls.
		/// </summary>
		private void ClearActionControls()
		{
			DeselectJobseekers();
			//ActionDropDown.SelectedValue = "";
		}

		/// <summary>
		/// Hides sub action controls
		/// </summary>
		private void HideSubActions()
		{
			ActivityDiv.Visible = false;
		}

		/// <summary>
		/// Deselects the jobseekers.
		/// </summary>
		private void DeselectJobseekers()
		{
            //var checkBoxes = from item in JobSeekersList.Items
            //                                 let checkBox = (CheckBox)item.FindControl("SelectorCheckBox")
            //                                 where checkBox.Checked
            //                                 select checkBox;

            foreach (var checkBox in JobSeekersList.Items.Select(item => (CheckBox)item.FindControl("SelectorCheckBox")).Where(control => control.Checked == true))
				checkBox.Checked = false;
		}

		/// <summary>
		/// Prepares the assign activity.
		/// </summary>
		private void PrepareAssignActivity()
		{
			ActivityDiv.Visible = true;
		}

		#endregion

		#region List / Saved Search Helper Class

		private class ListOrSavedSearch
		{
			public string Id { get; set; }
			public string Text { get; set; }
		}

		private enum ListOrSavedSearchType
		{
			List,
			SavedSearch
		}

		#endregion
  }
}
