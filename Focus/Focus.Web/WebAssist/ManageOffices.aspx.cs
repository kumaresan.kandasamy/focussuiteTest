﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Focus.Web.WebAssist.Controls;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistOfficesCreate)]
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistOfficesListStaff)]
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistOfficesSetDefault)]
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistOfficesViewEdit)]
  public partial class ManageOffices : PageBase
	{
		#region Properties

		private int _officeCount;

		private OfficeCriteria OfficeCriteria
		{
			get { return GetViewStateValue<OfficeCriteria>("ManageOffices:OfficeCriteria"); }
			set { SetViewStateValue("ManageOffices:OfficeCriteria", value); }
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      Page.Title = CodeLocalise("Page.Title", "Manage Offices");

      if (!App.Settings.OfficesEnabled)
        Response.RedirectToRoute("default");

			if (!IsPostBack)
			{
				BindFilterDropDown();
				LocaliseUI();
			  ApplySettings();

				Bind();
			}
		}

    /// <summary>
    /// Fires when the Create Office button is clicked to show the office modal
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void CreateOfficeButton_Clicked(object sender, EventArgs e)
    {
      OfficeModal.Show();
    }

    /// <summary>
    /// Fires when the Update Default Office button is clickedl
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard event argument</param>
    protected void UpdateDefaultOfficeButton_Clicked(object sender, EventArgs e)
    {
      UpdateDefaultOfficeModal.Show();
    }

		/// <summary>
		/// Handles the Selecting event of the OfficesDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void OfficesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (OfficeCriteria.IsNull())
				OfficeCriteria = new OfficeCriteria { OrderBy = "officename asc" };

			if (OfficeFilterDropDown.SelectedValue ==  OfficeListFilter.Active.ToString() )
			{
				OfficeCriteria.InActive = false;
			}
			else if (OfficeFilterDropDown.SelectedValue == OfficeListFilter.Inactive.ToString())
			{
				OfficeCriteria.InActive = true;
			}
			else
			{
				OfficeCriteria.InActive = null;
			}

			e.InputParameters["criteria"] = OfficeCriteria;
		}

		/// <summary>
		/// Handles the Change event of the OfficeFilterDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void OfficeFilterDropDown_Change(object sender, EventArgs e)
		{
      SearchResultListPager.ReturnToFirstPage();

			Bind();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the OfficeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void OfficeList_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var stateIdHiddenField = (HiddenField)e.Item.FindControl("StateIdHiddenField");
					
			if (stateIdHiddenField.IsNotNull())
			{
				var stateId = Convert.ToInt64(stateIdHiddenField.Value);

				var stateName = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Id == stateId).Select(x => x.Text).FirstOrDefault();

				var stateNameLabel = (Label)e.Item.FindControl("StateName");
				if (stateNameLabel.IsNotNull())
				{
					stateNameLabel.Text = stateName;
				}
			}

		  var linkActive = App.User.IsInRole(Constants.RoleKeys.AssistOfficesViewEdit);

      var officeDto = (OfficeDto)e.Item.DataItem;

      var officeNameLabel = (Label) e.Item.FindControl("OfficeNameLabel");
		  if (officeNameLabel.IsNotNull())
		  {
        officeNameLabel.Text = officeDto.OfficeName;
		    officeNameLabel.Visible = !linkActive;
		  }

		  var officeNameLink = (LinkButton)e.Item.FindControl("OfficeLink");
      if (officeNameLink.IsNotNull())
      {
        officeNameLink.Text = officeDto.OfficeName;
        officeNameLink.CommandArgument = officeDto.Id.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture);
        officeNameLink.Visible = linkActive;
      }

			var defaultType = officeDto.DefaultType;
			if (defaultType.HasFlag(OfficeDefaultType.All))
			{
				ShowDefaultOfficeIcon((Image) e.Item.FindControl("DefaultOfficeGeneralIcon"));
			}
			else
			{
				if (defaultType.HasFlag(OfficeDefaultType.Employer))
				{
					ShowDefaultOfficeIcon((Image) e.Item.FindControl("DefaultOfficeEmployerIcon"));
				}
				if (defaultType.HasFlag(OfficeDefaultType.JobOrder))
				{
					ShowDefaultOfficeIcon((Image) e.Item.FindControl("DefaultOfficeJobOrderIcon"));
				}
				if (defaultType.HasFlag(OfficeDefaultType.JobSeeker))
				{
					ShowDefaultOfficeIcon((Image) e.Item.FindControl("DefaultOfficeJobSeekerIcon"));
				}
			}

		}

		/// <summary>
		/// Finds and enables a default office icon.
		/// </summary>
		/// <param name="defaultOfficeIcon">The default office icon.</param>
	  private static void ShowDefaultOfficeIcon(Image defaultOfficeIcon)
	  {
		  if (!defaultOfficeIcon.IsNotNull()) return;
		  defaultOfficeIcon.Visible = true;
	  }

    /// <summary>
    /// Fires when the office link is click to show the view/edit office modal
    /// </summary>
    /// <param name="sender">Link button raising the event</param>
    /// <param name="e">Command arguments for the link</param>
    protected void OfficeLink_OnCommand(object sender, CommandEventArgs e)
    {
      var officeId = long.Parse(e.CommandArgument.ToString());
      OfficeModal.Show(officeId);
    }

		/// <summary>
		/// Handles the Completed event of the OfficeAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void OfficeAction_Completed(object sender, OfficeModal.OfficeModalEventArgs e)
		{
      if (e.ActionType.IsIn(ActionTypes.ReactivateOffice, ActionTypes.DeactivateOffice))
      {
        // If on the last page, which only has one item (prior to the update), the paging needs to be reset because the last page will be going missing!
        var numberOfPages = (int) Math.Ceiling ((double)SearchResultListPager.TotalRowCount / SearchResultListPager.PageSize);

        if (SearchResultListPager.TotalRowCount % SearchResultListPager.PageSize == 1 && SearchResultListPager.CurrentPage == numberOfPages)
          SearchResultListPager.ReturnToFirstPage();
      }

      RefreshPanels();
		}

    /// <summary>
    /// Fires when the default office is updated
    /// </summary>
    /// <param name="sender">Default Office Update modal</param>
    /// <param name="eventargs">Standard event args</param>
    protected void UpdateDefaultOfficeModal_OnUpdated(object sender, EventArgs eventargs)
    {
      RefreshPanels();
    }

		/// <summary>
		/// Handles the LayoutCreated event of the OfficeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void OfficeList_LayoutCreated(object sender, EventArgs e)
		{
			// Set table headings
			((Literal)OfficeList.FindControl("OfficeNameHeader")).Text = CodeLocalise("OfficeNameHeader.Text", "Office name");
			((Literal)OfficeList.FindControl("CityHeader")).Text = CodeLocalise("CityHeader.Text", "City");
			((Literal)OfficeList.FindControl("StateNameHeader")).Text = CodeLocalise("StateNameHeader.Text", "State");
      ((Literal)OfficeList.FindControl("ZipHeader")).Text = CodeLocalise("ZipHeader.Text", "Zip code");
      ((Literal)OfficeList.FindControl("OfficeManagerMailboxHeader")).Text = CodeLocalise("OfficeManagerMailboxHeader.Text", "Office manager mailbox");
    }

    /// <summary>
    /// Handles the Sorting event of the OfficeList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
    protected void OfficeList_Sorting(object sender, ListViewSortEventArgs e)
    {
      OfficeCriteria.OrderBy = e.SortExpression;
      FormatOfficeListSortingImages();
    }

    /// <summary>
    /// Binds and refreshes the list
    /// </summary>
    /// <param name="doBind">Whether to re-bind the data.</param>
    private void RefreshPanels(bool doBind = true)
    {
      if (doBind)
        Bind();

      PagerUpdatePanel.Update();
      OfficeListPanel.Update();
    }

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			OfficeList.DataBind();
			ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} results found", SearchResultListPager.TotalRowCount.ToString(CultureInfo.CurrentUICulture));

			if (SearchResultListPager.TotalRowCount > 0)
				FormatOfficeListSortingImages();
		}

		/// <summary>
		/// Binds the filter drop down.
		/// </summary>
		private void BindFilterDropDown()
		{
			OfficeFilterDropDown.Items.Clear();
			OfficeFilterDropDown.Items.Add(new ListItem(CodeLocalise(OfficeListFilter.None, "- all -"), OfficeListFilter.None.ToString()));
			OfficeFilterDropDown.Items.Add(new ListItem(CodeLocalise(OfficeListFilter.Active, "Active offices"), OfficeListFilter.Active.ToString()));
			OfficeFilterDropDown.Items.Add(new ListItem(CodeLocalise(OfficeListFilter.Inactive, "Inactive offices"), OfficeListFilter.Inactive.ToString()));
			OfficeFilterDropDown.SelectedIndex = 1;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
      CreateOfficeButton.Text = CodeLocalise(ActionTypes.CreateOffice, "Create office");
      UpdateDefaultOfficeButton.Text = CodeLocalise(ActionTypes.CreateOffice, "Update default office");
		}

    /// <summary>
    /// Applies any settings to the page
    /// </summary>
    private void ApplySettings()
    {
      CreateOfficeButton.Visible = App.User.IsInRole(Constants.RoleKeys.AssistOfficesCreate);
      UpdateDefaultOfficeButton.Visible = App.User.IsInRole(Constants.RoleKeys.AssistOfficesSetDefault);
    }

		/// <summary>
		/// Gets the offices.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<OfficeDto> GetOffices(OfficeCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;

			var offices = ServiceClientLocator.EmployerClient(App).GetOfficesPagedList(criteria);
			_officeCount = offices.TotalCount;

			return offices;
		}

		/// <summary>
		/// Gets the office count.
		/// </summary>
		/// <returns></returns>
		public int GetOfficeCount()
		{
			return _officeCount;
		}

		/// <summary>
		/// Gets the office count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetOfficeCount(OfficeCriteria criteria)
		{
			return _officeCount;
		}

    /// <summary>
    /// Formats the offcie list sorting images.
    /// </summary>
    private void FormatOfficeListSortingImages()
    {
      var buttonNames = new List<string>
      {
        "OfficeNameAscSortButton",
        "OfficeNameDescSortButton",
        "TownCityAscSortButton",
        "TownCityDescSortButton",
        "StateIdAscSortButton",
        "StateIdDescSortButton",
        "PostcodeZipAscSortButton",
        "PostcodeZipDescSortButton",
        "OfficeManagerMailboxAscSortButton",
        "OfficeManagerMailboxDescSortButton"
      };

      var imageButtons = buttonNames.ToDictionary(
        buttonName => buttonName.ToLowerInvariant(), 
        buttonName => (ImageButton) OfficeList.FindControl(buttonName));

      foreach(var imageButton in imageButtons.Values)
      {
        imageButton.Enabled = true;
        imageButton.ImageUrl = imageButton.ID.EndsWith("AscSortButton", StringComparison.OrdinalIgnoreCase) 
          ? UrlBuilder.CategorySortAscImage()
          : UrlBuilder.CategorySortDescImage();
      }

      var orderBy = (OfficeCriteria.OrderBy ?? "OfficeName Asc").Replace(" ", "").ToLowerInvariant();
      var sortedButtonId = string.Concat(orderBy, "sortbutton");
      var sortedButton = imageButtons[sortedButtonId];

      sortedButton.Enabled = false;
      sortedButton.ImageUrl = sortedButton.ID.EndsWith("AscSortButton", StringComparison.OrdinalIgnoreCase)
        ? UrlBuilder.CategorySortedAscImage()
        : UrlBuilder.CategorySortedDescImage();
    }

    /// <summary>
    /// Fires when the page has changed
    /// </summary>
    /// <param name="o">Pager control</param>
    /// <param name="e">Page event arguments</param>
    protected void SearchResultListPager_OnDataPaged(object o, Pager.DataPagedEventArgs e)
    {
      RefreshPanels(false);
    }
	}
}