﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;
using AjaxControlToolkit;
using Focus.Common;
using Focus.Core;
using Focus.Web.Code;
using Framework.Core;
using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
	public partial class JobWizard : PageBase
	{
		private long _jobId
		{
			get { return GetViewStateValue<long>("JobWizard:JobId"); }
			set { SetViewStateValue("JobWizard:JobId", value); }
		}

    public bool FromApproval { get; set; }
		private const string FilteredURL = "?filtered=true";

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			var source = Page.RouteData.Values["source"];
			if (source != null)
				FromApproval = (string.Compare(source.ToString(), "approval", StringComparison.OrdinalIgnoreCase) == 0);
			if (!IsPostBack)
			{
				long currentJobId = 0;
				if (Page.RouteData.Values.ContainsKey("id"))
					long.TryParse(Page.RouteData.Values["id"].ToString(), out currentJobId);

				Wizard.JobId = _jobId = currentJobId;

				if(_jobId > 0)
					ServiceClientLocator.JobClient(App).SaveBeforeImageOfJob(currentJobId);

				long hiringManagerId = 0;
				var managerId = Request.QueryString["managerId"];
				if (managerId.IsNotNullOrEmpty())
					long.TryParse(managerId, out hiringManagerId);

				Wizard.HiringManagerId = hiringManagerId;

				if (FromApproval)
				{
					var filtered = Request.QueryString["filtered"];
					ReturnLink.NavigateUrl = filtered.IsNotNullOrEmpty() ? UrlBuilder.PostingReferral(_jobId).AddToEndOfString(FilteredURL) : UrlBuilder.PostingReferral(_jobId);
					ReturnLink.Text = HtmlLocalise("PostingReferralLink.Text", "return to job posting approval");
				}
				else
				{
					ReturnLink.NavigateUrl = UrlBuilder.JobOrderDashboard();
					ReturnLink.Text = HtmlLocalise("JobOrderDashboardLink.Text", "return to #EMPLOYMENTTYPE# Dashboard");
				}
			}

            Page.Title = (_jobId > 0) ? CodeLocalise("Page.Title", "Job wizard - " + _jobId) : CodeLocalise("Page.Title", "Job wizard" ); 
			ViewNotesButton.Text = CodeLocalise("ViewNotesButton.Text", "View / add notes and reminders");
			ViewNotesButton.Visible = (_jobId > 0);
		}

		/// <summary>
		/// Handles the Click event of the ViewNotesButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ViewNotesButton_Click(object sender, EventArgs e)
		{
			NotesAndReminders.Show(_jobId, EntityTypes.Job);
		}
	}
}