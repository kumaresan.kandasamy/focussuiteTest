﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="JobWizardEducation.aspx.cs" Inherits="Focus.Web.WebAssist.JobWizardEducation" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistEmployersNavigation.ascx" tagname="AssistEmployersNavigation" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/Education/JobWizardEducation.ascx" tagname="JobWizardEducation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/NotesAndRemindersModal.ascx" tagname="NotesAndRemindersModal" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%# ResolveUrl("~/Assets/Scripts/jquery-ui-1.9.2.custom.new.min.js") %>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<%# ResolveUrl("~/Assets/Css/jquery-ui-1.9.2.custom.css") %>" />
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />	
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:AssistEmployersNavigation ID="AssistEmployersNavigationMain" runat="server" />
	<table style="width:100%;">
		<tr>
			<td><a href="<%= UrlBuilder.JobOrderDashboard() %>"><%= HtmlLocalise("JobOrderDashboardLink.Text", "return to #EMPLOYMENTTYPE# Dashboard")%></a></td>
			<td style="text-align:right;">
				<asp:UpdatePanel ID="ViewNotesUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<asp:Button ID="ViewNotesButton" runat="server" SkinID="Button3" OnClick="ViewNotesButton_Click" CausesValidation="false" UseSubmitBehavior="False" />
						<uc:NotesAndRemindersModal ID="NotesAndReminders" runat="server" />	
					</ContentTemplate>
				</asp:UpdatePanel>
			</td>
		</tr>
	</table>
	<uc:JobWizardEducation ID="Wizard" runat="server" />

	
</asp:Content>
