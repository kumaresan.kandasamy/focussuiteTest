﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageJobSeekerLists.aspx.cs" Inherits="Focus.Web.WebAssist.ManageJobSeekerLists" %>
<%@ Import Namespace="Focus.Core.Views" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistJobSeekersNavigation.ascx" tagname="AssistJobSeekersNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/JobSeekerListModal.ascx" tagname="JobSeekerListModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/UpdateableClientList.ascx" tagName="UpdateableClientList" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	
	<uc:AssistJobSeekersNavigation ID="AssistJobSeekersNavigationMain" runat="server" />

	<h1><%= HtmlLocalise("PageTitle.Text", "Manage lists")%></h1>

	<div>
		<div style="float: left; min-width: 200px; padding: 10px;">
			<table role="presentation">
				<tr>
					<td style="width:150px;"><%= HtmlLabel(FirstNameTextBox,"JobSeekerFirstName.Label", "First name") %></td>
					<td><asp:TextBox runat="server" ID="FirstNameTextBox" Width="200" /></td>					
					<td style="width:10px;"></td>
					<td></td>
				</tr>
				<tr>
					<td><%= HtmlLabel(LastNameTextBox,"JobSeekerLastName.Label", "Last name") %></td>
					<td><asp:TextBox runat="server" ID="LastNameTextBox" Width="200" /></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><%= HtmlLabel(EmailAddressTextBox,"JobSeekerEmailAddress.Label", "Email address")%></td>
					<td><asp:TextBox runat="server" ID="EmailAddressTextBox" Width="200" /></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><%= HtmlLabel(PhoneNumberTextBox,"JobSeekerPhoneNumber.Label", "Phone number")%></td>
					<td><asp:TextBox runat="server" ID="PhoneNumberTextBox" Width="200" /></td>
					<td></td>
					<td></td>
				</tr>
				<tr style="vertical-align: top">
          <asp:PlaceHolder runat="server" ID="SSNPlaceHolder">
					  <td><%= HtmlLabel("JobSeekerSsn.Label", "SSN") %></td>
					  <td>
              <uc:UpdateableClientList ID="SsnSelector" runat="server" AllowNonAdded="True" InputMask="9?99-99-9999" MaskPromptCharacter="_" ValidatorRegex="^\d{3}-\d{2}-\d{4}$" ValidatorRegexErrorMessage="SSN must consist of 9 numbers" SelectionBoxTitle="Selected SSNs"/>
					  </td>
          </asp:PlaceHolder>
          <asp:PlaceHolder runat="server" ID="NoSSNPlaceHolder">
					  <td></td>
					  <td></td>
          </asp:PlaceHolder>
					<td></td>
					<td><asp:Button ID="FindButton" runat="server" class="button3" OnClick="FindButton_Clicked" /></td>
				</tr>
			</table>
		</div>
		<div style="padding: 10px; float: left; min-width: 600px">
			<table style="width:100%;" role="presentation" >
				<tr>
					<td>
						<table role="presentation">
							<tr>
								<td><h3 style="padding-bottom:0;"><%= HtmlLocalise("JobSeekers.Title", "#CANDIDATETYPE#")%></h3></td> <%-- Replaced plural: Job Seekers --%>
								<td>&nbsp;&nbsp;<asp:literal ID="ResultCount" runat="server" /></td>
							</tr>
						</table>
					</td>
					<td><asp:Button ID="AddJobSeekersButton" runat="server" CssClass="button2 right" OnClick="AddJobSeekersButton_Clicked" ValidationGroup="JobSeekerList" Visible="False" /></td>
				</tr>
			</table>
			<i><asp:literal ID="PageInstructions" runat="server" /></i>			
			<table id="JobSeekersTable" runat="server" Visible="false" style="width: 600px;" role="presentation">
				<tr>
					<td style="width:20px;">&nbsp;</td>
					<td style="width:400px;"><%= HtmlLocalise("JobSeekerName.ColumnTitle", "Name")%></td>
					<td style="width:170px;"><%= HtmlLocalise("PhoneNumber.ColumnTitle", "Phone number")%></td>
				</tr>

			</table>
			<asp:ListView ID="JobSeekersList" runat="server" ItemPlaceholderID="JobSeekerPlaceHolder" DataKeyNames="PersonId,FirstName,LastName,PhoneNumber,EmailAddress">
				<LayoutTemplate>
					<div style="height: 125px; overflow-y: scroll; width: 620px;">
						<table style="width:600px;" role="presentation">
							<asp:PlaceHolder ID="JobSeekerPlaceHolder" runat="server" />
						</table>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<tr>
						<td style="width:20px;"><asp:Checkbox ID="SelectorCheckBox" runat="server" ClientIDMode="Static" CssClass="JobSeekerSelectorCheckBox"/></td>
						<td style="width:400px;"><%# ((JobSeekerView)Container.DataItem).LastName%>, <%# ((JobSeekerView)Container.DataItem).FirstName%></td>
						<td style="width:170px;"><%# ((JobSeekerView)Container.DataItem).PhoneNumber%></td>
					</tr>	
				</ItemTemplate>
				<EmptyDataTemplate>
					<table style="width:600px;" role="presentation">
						<tr>
							<td style="text-align:left;">
								<br />
								<br />
								<%= HtmlLocalise("JobSeekersList.NoResult", "No matches")%></td>
						</tr>
					</table>
				</EmptyDataTemplate>
			</asp:ListView>
			<br/>
			
			<asp:CustomValidator ID="JobSeekersListValidator" runat="server" ClientValidationFunction="validateJobSeekersList"
													 ValidationGroup="JobSeekerList" ValidateEmptyText="True" CssClass="error"/>
			<br/>
			<hr />
			<br/>
			<h3><%= HtmlLabel(ListsDropDown,"JobSeekerLists.Title", "View, edit, and delete lists")%></h3>
			<br/>
			<asp:DropDownList ID="ListsDropDown" runat="server" OnSelectedIndexChanged="ListsDropDown_SelectedIndexChanged" AutoPostBack="True" CausesValidation="False" />
			<br/><br/>
			<table id="ListMembersTable" runat="server" Visible="false" class="tableHeader" style="width: 600px;" role="presentation">
				<tr>
					<td style="width:20px;">&nbsp;</td>
					<td style="width:400px;"><%= HtmlLocalise("JobSeekerName.ColumnTitle", "Name")%></td>
					<td style="width:170px;"><%= HtmlLocalise("PhoneNumber.ColumnTitle", "Phone number")%></td>
				</tr>
			</table>
			<asp:ListView ID="ListMembersList" runat="server" ItemPlaceholderID="ListMemberPlaceHolder" DataKeyNames="PersonId,FirstName,LastName,PhoneNumber,EmailAddress">
				<LayoutTemplate>
					<div style="height: 125px; overflow-y: scroll; width: 620px;">
						<table style="width:600px;" class="tableBody" role="presentation">
							<asp:PlaceHolder ID="ListMemberPlaceHolder" runat="server" />
						</table>
					</div>
				</LayoutTemplate>
				<ItemTemplate>
					<tr>
						<td style="width:20px;"><asp:Checkbox ID="SelectorCheckBox" runat="server" ClientIDMode="Static" CssClass="ListMemberSelectorCheckBox"/></td>
						<td style="width:400px;"><%# ((JobSeekerView)Container.DataItem).LastName%>, <%# ((JobSeekerView)Container.DataItem).FirstName%></td>
						<td style="width:170px;"><%# ((JobSeekerView)Container.DataItem).PhoneNumber%></td>
					</tr>	
				</ItemTemplate>
				<EmptyDataTemplate>
					<table style="width:600px;" role="presentation">
						<tr>
							<td style="text-align:left;">
								<%= HtmlLocalise("ListMembersList.NoResult", "No members")%></td>
						</tr>
					</table>
				</EmptyDataTemplate>
			</asp:ListView>
			<br/>
			<asp:Button ID="DeleteListMembersButton" runat="server" CssClass="button2" ValidationGroup="ListMemberList" OnClick="DeleteListMembersButton_Clicked" Visible="False" />
			<asp:Button ID="DeleteListButton" runat="server" CssClass="button2" CausesValidation="False" OnClick="DeleteListButton_Clicked" Visible="False" />

			<asp:CustomValidator ID="ListMembersListValidator" runat="server" ClientValidationFunction="validateListMembersList"
													 ValidationGroup="ListMemberList" ValidateEmptyText="True" CssClass="error"/>
		</div>
	</div>

	<%-- Modals --%>
	<uc:JobSeekerListModal ID="JobSeekerList" runat="server" OnOkCommand="JobSeekerList_OkCommand" />
	<uc:ConfirmationModal ID="Confirmation" runat="server" />
	<uc:ConfirmationModal ID="ActionConfirmation" runat="server" OnOkCommand="ActionConfirmationModal_OkCommand" />

	<script type="text/javascript">
		function validateJobSeekersList(oSrc, args) {
			args.IsValid = checkAtLeastOneJobSeekerSelected();
		}

		function checkAtLeastOneJobSeekerSelected() {
			var result = false;
			$('.JobSeekerSelectorCheckBox > input:checkbox').each(function () { if (this.checked) result = true; });

			return result;
		}

		function validateListMembersList(oSrc, args) {
			args.IsValid = checkAtLeastOneListMemberSelected();
		}

		function checkAtLeastOneListMemberSelected() {
			var result = false;
			$('.ListMemberSelectorCheckBox > input:checkbox').each(function () { if (this.checked) result = true; });

			return result;
		}
		
	</script>
			
</asp:Content>

