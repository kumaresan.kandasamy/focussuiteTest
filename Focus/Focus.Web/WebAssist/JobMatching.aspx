﻿<%@ Page Title="Job matching" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="JobMatching.aspx.cs" Inherits="Focus.Web.WebAssist.JobMatching" %>
<%@ Register Src="~/WebAssist/Controls/JobSkillList.ascx" TagName="JobSkillList" TagPrefix="uc" %>  
<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>  
<%@ Register TagPrefix="uc" TagName="AssistJobSeekersNavigation" Src="~/WebAssist/Controls/AssistJobSeekersNavigation.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel ID="UpdatePanelJobMatching" runat="server">
			<table style="width:100%;" role="presentation">
			  <tr>
					<td colspan="3">
						<uc:AssistJobSeekersNavigation ID="AssistJobSeekersNavigationMain" runat="server" />
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:HyperLink ID="BackHyperLink" runat="server">
														<%= HtmlLocalise("JobResults.Label", "&#171; Job results")%>
						</asp:HyperLink>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div style="height: 10px;">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<h1>
							<%= HtmlLocalise("Heading.Label", "How strong is this match?")%></h1>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div style="height: 10px;">
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:Label ID="lblJobMatchInfo" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div style="height: 15px;">
						</div>
					</td>
				</tr>
				
				<tr>
					<td colspan="3">
						<table style="width:100%;" class="FocusCareer numberedAccordion" role="presentation">
							<tr id="CommonSkillsTitle" runat="server" class="multipleAccordionTitle">
					<td style="width:10;" class="column1">
						<%= HtmlLocalise("Header1Number.Label", "1")%>
					</td>
					<td class="column2">
						<a href="#"><%= HtmlLocalise("Header1.Label", "Common skills")%></a>
					</td>
					<td class="column3" style="vertical-align: top; text-align: right;">
						<a class="show" href="#">
							<%= HtmlLocalise("Show1.Label", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide1.Label", "HIDE")%></a>
					</td>
				</tr>
				<tr id="CommonSkillsContent" runat="server" class="accordionContent">
					<td>
					</td>
					<td>
						<br />
						<asp:Panel ID="panSkillGap" runat="server">
							<uc:JobSkillList ID="SkillList" runat="server" DisplaySkillGap="false" DisableLinks="True" />
							<span class="instructionalText">
								<%= HtmlLocalise("Highlightd.Label", "Skills specifically called for in this job posting are highlighted.")%>
							</span>
							<br />
							</asp:Panel>
						<asp:Label ID="lblSkillGapInfo" runat="server" Visible="false"></asp:Label>
					</td>
					<td>
					</td>
				</tr>
				<tr id="EducationCertificatesLicensesTitle" runat="server" class="multipleAccordionTitle">
					<td class="column1">
						<%= HtmlLocalise("HeaderNumber2.Label", "2")%>
					</td>
					<td class="column2">
						<a href="#"><%= HtmlLocalise("Header2.Label", "Education, certificates, and licenses")%></a>
					</td>
					<td class="column3" style="vertical-align: top; text-align:right;">
						<a class="show" href="#">
							<%= HtmlLocalise("Show2.Label", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide2.Label", "HIDE")%></a>
					</td>
				</tr>
				<tr id="EducationCertificatesLicensesContent" runat="server" class="accordionContent">
					<td>
					</td>
					<td>
						<br />
						<table role="presentation">
							<tr>
								<td style="vertical-align: top; width: 60%;">
									<div>
										<strong>
											<%= HtmlLocalise("EducationLevel.Label", "Education level")%>
										</strong>
									</div>
									<br />
									<asp:PlaceHolder ID="EducationRequirementsPlaceHolder" runat="server">
										<table class="horizontalBarChart">
											<tr>
												<td class="column1" style="width:155px;">
													<%= HtmlLocalise("HighSchoolTechnicalTraining.BarChartLabel", "High school/technical training")%>
												</td>
												<td class="column2" rowspan="5">
												</td>
												<td class="column3">
													<asp:Literal ID="HighSchoolTechnicalTrainingLiteral" runat="server" />
												</td>
											</tr>
											<tr>
												<td class="column1">
													<%= HtmlLocalise("AssociatesDegree.BarChartLabel", "Associates degree")%>
												</td>
												<td class="column3">
													<asp:Literal ID="AssociatesDegreeLiteral" runat="server" />
												</td>
											</tr>
											<tr>
												<td class="column1">
													<%= HtmlLocalise("BachelorsDegree.BarChartLabel", "Bachelors degree")%>
												</td>
												<td class="column3">
													<asp:Literal ID="BachelorsDegreeLiteral" runat="server" />
												</td>
											</tr>
											<tr>
												<td class="column1">
													<%= HtmlLocalise("GraduateProfessionalDegree.BarChartLabel", "Graduate/professional degree")%>
												</td>
												<td class="column3">
													<asp:Literal ID="GraduateProfessionalDegreeLiteral" runat="server" />
												</td>
											</tr>
										</table>
										<br />
									</asp:PlaceHolder>
									<asp:Label ID="lblEducationLevelInfo" runat="server" Visible="false"></asp:Label>
								</td>
								<td style="vertical-align: top; width: 40%; padding-left:25px;">
									<div>
										<strong>
											<%= HtmlLocalise("CertificationsLevel.Label", "Certifications and licenses")%>
										</strong>
									</div>
									<br />
									<asp:Label ID="lblCertificationInfo" runat="server" Visible="false"></asp:Label>
									<table>
										<asp:Repeater ID="CertificationRepeater" runat="server" OnItemDataBound="CertificationRepeater_ItemDataBound">
											<ItemTemplate>
												<tr>
													<td>
														<asp:Literal ID="CertificationRankLiteral" runat="server" />.
														<%# Eval("CertificationName")%>
													</td>
												</tr>
											</ItemTemplate>
										</asp:Repeater>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td>
					</td>
				</tr>
				<tr id="LevelOfExperienceTitle" runat="server" class="multipleAccordionTitle">
					<td class="column1">
						<%= HtmlLocalise("HeaderNumber3.Label", "3")%>
					</td>
					<td class="column2">
						<a href="#"><%= HtmlLocalise("Header3.Label", "Level of experience")%></a>
					</td>
					<td class="column3" style="vertical-align: top; text-align:right;">
						<a class="show" href="#">
							<%= HtmlLocalise("Show3.Label", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide3.Label", "HIDE")%></a>
					</td>
				</tr>
				<tr id="LevelOfExperienceContent" runat="server" class="accordionContent">
					<td>
					</td>
					<td>
						<br />
						<table>
							<tr>
								<td>
									<strong>
										<%= HtmlLocalise("ExperienceLevels.Label", "Experience levels of successful candidates")%></strong>
								</td>
							</tr>
							<tr>
								<td>
									<asp:PlaceHolder ID="ExperienceRequiredPlaceHolder" runat="server">
										<table class="horizontalBarChart">
											<tr>
												<td class="column1" style="width:155px;">
													<%= HtmlLocalise("LessThanTwoYears.BarChartLabel", "Less than 2 years")%>
												</td>
												<td class="column2" rowspan="5">
												</td>
												<td class="column3">
													<asp:Literal ID="LessThanTwoLiteral" runat="server" />
												</td>
											</tr>
											<tr>
												<td class="column1">
													<%= HtmlLocalise("TwoToFiveYears.BarChartLabel", "2-5 years")%>
												</td>
												<td class="column3">
													<asp:Literal ID="TwoToFiveLiteral" runat="server" />
												</td>
											</tr>
											<tr>
												<td class="column1">
													<%= HtmlLocalise("FiveToEightYears.BarChartLabel", "5-8 years")%>
												</td>
												<td class="column3">
													<asp:Literal ID="FiveToEightLiteral" runat="server" />
												</td>
											</tr>
											<tr class="last">
												<td class="column1">
													<%= HtmlLocalise("EightPlusYears.BarChartLabel", "8+ years")%>
												</td>
												<td class="column3">
													<asp:Literal ID="EightPlusLiteral" runat="server" />
												</td>
											</tr>
										</table>
									</asp:PlaceHolder>
									<asp:Label ID="lblExperienceLevelInfo" runat="server" Visible="false"></asp:Label>
								</td>
							</tr>
						</table>
					</td>
					<td>
					</td>
				</tr>
				<tr id="CommonCareerPathTitle" clientidmode="Static" runat="server" class="multipleAccordionTitle">
					<td class="column1">
						<%= HtmlLocalise("HeaderTitle4.Label", "4")%>
					</td>
					<td class="column2">
						<a href="#"><%= HtmlLocalise("Header4.Label", "Common career paths")%></a>
					</td>
					<td class="column3" style="vertical-align: top; text-align:right;">
						<a class="show" href="#">
							<%= HtmlLocalise("Show4.Label", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide4.Label", "HIDE")%></a>
					</td>
				</tr>
				<tr id="CommonCareerPathContent" runat="server" class="accordionContent">
					<td>
					</td>
					<td>
						<asp:Panel ID="panCareerPath" runat="server">
							<p>
								<%= HtmlLocalise("CommonCareerPathsIntro.Text", "Click on &quot;How did other people get here&quot; to see what individuals in this occupation were doing before they took this job. Click on &quot;Where people go from here&quot; to see common next steps.")%></p>
							<asp:UpdatePanel ID="JobCareerPathUpdatePanel" runat="server" UpdateMode="Conditional">
								<ContentTemplate>
									<table>
										<tr>
											<td style="width:50px">
												<strong>
													<%= HtmlLocalise("Global.Show.Text", "Show")%>:</strong>
											</td>
											<td>
												<asp:LinkButton ID="CareerPathFromLinkButton" runat="server" OnCommand="CareerPathLinkButton_Command" />&nbsp;
												<asp:LinkButton ID="CareerPathToLinkButton" runat="server" OnCommand="CareerPathLinkButton_Command" />
											</td>
										</tr>
									</table>
									<table style="width:100%">
										<tr>
											<td style="text-align:center;" >
												<asp:Label ID="NoCareerPathLabel" runat="server" />
												<table>
													<tr>
														<td>
															<asp:Panel ID="CareerPathPanel" runat="server" CssClass="careerPathChart">
																<!-- First Row -->
																<asp:Panel ID="CareerPathTopLevelPanel" runat="server" CssClass="careerpathChartItem top topArrowDown">
																	<table class="box">
																		<tr>
																			<td style="text-align:center; vertical-align:middle;">
																				<asp:Label ID="CareerPathJobNameLabel" runat="server" />
																			</td>
																		</tr>
																	</table>
																	<span class="arrowLineTop"></span>
																</asp:Panel>
																<div class="clear">
																</div>
																<!-- Second Row -->
																<asp:Panel ID="CareerPathSecondLevelPanel" runat="server" CssClass="careerPathRowWrap">
																	<asp:Panel ID="CareerPathSecondLevelLeftPanel" runat="server" CssClass="careerpathChartItem left">
																		<span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
																		<asp:LinkButton ID="CareerPathSecondLevelLeftJobLinkButton" runat="server" CommandName="ViewJob"
																			CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
																		<table class="box">
																			<tr>
																				<td style="text-align:center; vertical-align:middle;">
																					<asp:LinkButton ID="CareerPathSecondLevelLeftLinkButton"  text="Career Path Second Level Left LinkButton" runat="server" OnCommand="SecondLevelLinkButton_Command" />
																				</td>
																			</tr>
																		</table>
																		<span class="BottomArrow"></span>
																	</asp:Panel>
																	<asp:Panel ID="CareerPathSecondLevelMiddleLeftPanel" runat="server" CssClass="careerpathChartItem">
																		<asp:Literal ID="CareerPathSecondLevelMiddleLeftArrowLineLiteral" runat="server" /><span
																			class="arrowLineBottom"></span>
																		<asp:LinkButton ID="CareerPathSecondLevelMiddleLeftJobLinkButton" runat="server"
																			CommandName="ViewJob" CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
																		<table class="box">
																			<tr>
																				<td style="text-align:center; vertical-align:middle;">
																					<asp:LinkButton ID="CareerPathSecondLevelMiddleLeftLinkButton" Text="Career Path Second Level Middle Left LinkButton" runat="server" OnCommand="SecondLevelLinkButton_Command" />
																				</td>
																			</tr>
																		</table>
																		<span class="BottomArrow"></span>
																	</asp:Panel>
																	<asp:Panel ID="CareerPathSecondLevelMiddleRightPanel" runat="server" CssClass="careerpathChartItem">
																		<span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
																		<asp:LinkButton ID="CareerPathSecondLevelMiddleRightJobLinkButton" runat="server"
																			CommandName="ViewJob" CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
																		<table class="box">
																			<tr>
																				<td style="text-align:center; vertical-align:middle;">
																					<asp:LinkButton ID="CareerPathSecondLevelMiddleRightLinkButton" runat="server" OnCommand="SecondLevelLinkButton_Command" />
																				</td>
																			</tr>
																		</table>
																		<span class="BottomArrow"></span>
																	</asp:Panel>
																	<asp:Panel ID="CareerPathSecondLevelRightPanel" runat="server" CssClass="careerpathChartItem right">
																		<span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
																		<asp:LinkButton ID="CareerPathSecondLevelRightJobLinkButton" runat="server" CommandName="ViewJob"
																			CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
																		<table class="box">
																			<tr>
																				<td style="text-align:center; vertical-align:middle;">
																					<asp:LinkButton ID="CareerPathSecondLevelRightLinkButton" runat="server" OnCommand="SecondLevelLinkButton_Command" />
																				</td>
																			</tr>
																		</table>
																		<span class="BottomArrow"></span>
																	</asp:Panel>
																</asp:Panel>
																<!-- Third Row -->
																<asp:Panel ID="CareerPathThirdLevelPanel" runat="server" CssClass="careerPathRowWrap">
																	<asp:Panel ID="CareerPathThirdLevelLeftPanel" runat="server" CssClass="careerpathChartItem left">
																		<span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
																		<asp:LinkButton ID="CareerPathThirdLevelLeftJobLinkButton" runat="server" CommandName="ViewJob"
																			CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
																		<table class="box">
																			<tr>
																				<td style="text-align:center; vertical-align:middle;">
																					<asp:Label ID="CareerPathThirdLevelLeftJobNameLabel" runat="server" />
																				</td>
																			</tr>
																		</table>
																	</asp:Panel>
																	<asp:Panel ID="CareerPathThirdLevelMiddleLeftPanel" runat="server" CssClass="careerpathChartItem">
																		<asp:Literal ID="CareerPathThirdLevelMiddleLeftArrowLineLiteral" runat="server" /><span
																			class="arrowLineBottom"></span>
																		<asp:LinkButton ID="CareerPathThirdLevelMiddleLeftJobLinkButton" runat="server" CommandName="ViewJob"
																			CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
																		<table class="box">
																			<tr>
																				<td style="text-align:center; vertical-align:middle;">
																					<asp:Label ID="CareerPathThirdLevelMiddleLeftJobNameLabel" runat="server" />
																				</td>
																			</tr>
																		</table>
																	</asp:Panel>
																	<asp:Panel ID="CareerPathThirdLevelMiddleRightPanel" runat="server" CssClass="careerpathChartItem center">
																		<span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
																		<asp:LinkButton ID="CareerPathThirdLevelMiddleRightJobLinkButton" runat="server"
																			CommandName="ViewJob" CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
																		<table class="box">
																			<tr>
																				<td style="text-align:center; vertical-align:middle;">
																					<asp:Label ID="CareerPathThirdLevelMiddleRightJobNameLabel" runat="server" />
																				</td>
																			</tr>
																		</table>
																	</asp:Panel>
																	<asp:Panel ID="CareerPathThirdLevelRightPanel" runat="server" CssClass="careerpathChartItem right">
																		<span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
																		<asp:LinkButton ID="CareerPathThirdLevelRightJobLinkButton" runat="server" CommandName="ViewJob"
																			CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
																		<table class="box">
																			<tr>
																				<td style="text-align:center; vertical-align:middle;">
																					<asp:Label ID="CareerPathThirdLevelRightJobNameLabel" runat="server" />
																				</td>
																			</tr>
																		</table>
																	</asp:Panel>
																</asp:Panel>
															</asp:Panel>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</ContentTemplate>
							</asp:UpdatePanel>
						</asp:Panel>
						<asp:Label ID="lblCareerPathInfo" runat="server" Visible="false"></asp:Label>
					</td>
					<td>
					</td>
				</tr>
				<tr id="TypicalResumesTitle" runat="server" class="multipleAccordionTitle">
					<td class="column1">
						<%= HtmlLocalise("HeaderTitle5.Label", "5")%>
					</td>
					<td class="column2">
						<a href="#"><%= HtmlLocalise("Header5.Label", "Typical resumes")%></a>
					</td>
					<td class="column3" style="vertical-align: top; text-align:right;">
						<a class="show" href="#">
							<%= HtmlLocalise("Show5.Label", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide5.Label", "HIDE")%></a>
					</td>
				</tr>
				<tr id="TypicalResumesContent" runat="server" class="accordionContent">
					<td>
					</td>
					<td>
						<asp:Panel ID="panTypicalResumes" runat="server">
							<div>
								<strong>
									<%= HtmlLocalise("TypicalResumes.Label", "Typical resumes of people who placed into similar jobs")%>
								</strong>
							</div>
							<div style="height: 10px">
							</div>
							<table style="width:100%" class="genericTable withHeader">
								<tr>
									<th>
										<%= HtmlLocalise("GenericTH1.Label", "JOB TITLE")%>
									</th>
									<th>
										JOB TITLE<br />
										PRIOR TO PLACEMENT
									</th>
									<th>
										<%= HtmlLocalise("GenericTH2.Label", "YEARS EXPERIENCE<br />ON PLACEMENT")%>
									</th>
									<th>
										<%= HtmlLocalise("GenericTH3.Label", "DEGREE LEVEL")%>
									</th>
									<th>
										<%= HtmlLocalise("GenericTH4.Label", "SKILLS &amp; QUALIFICATIONS")%>
									</th>
								</tr>
								<asp:Repeater ID="TypicalResumesRepeater" runat="server" OnItemDataBound="TypicalResumesRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td>
												<asp:Label ID="lblCurrentJob" runat="server"></asp:Label>
											</td>
											<td>
												<asp:Label ID="lblPreviousJob" runat="server"></asp:Label>
											</td>
											<td>
												<asp:Label ID="lblYearOfExp" runat="server"></asp:Label>
											</td>
											<td>
												<asp:Label ID="lblDegreeLevel" runat="server"></asp:Label>
											</td>
											<td>
												<asp:Label ID="lblSkills" runat="server"></asp:Label>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</asp:Panel>
						<asp:Label ID="lblTypicalResumeInfo" runat="server" Visible="false"></asp:Label>
					</td>
					<td>
					</td>
				</tr>
				<tr id="PotentialGapsTitle" runat="server" class="multipleAccordionTitle">
					<td class="column1">
						<%= HtmlLocalise("HeaderTitle6.Label", "5")%>
					</td>
					<td class="column2">
						<a href="#"><%= HtmlLocalise("Header6.Label", "Potential gaps")%></a>
					</td>
					<td class="column3" style="vertical-align: top; text-align:right;">
						<a class="show" href="#">
							<%= HtmlLocalise("Show6.Label", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide6.Label", "HIDE")%></a>
					</td>
				</tr>
				<tr id="PotentialGapsContent" runat="server" class="accordionContent">
					<td>
					</td>
					<td>
						<div>
							<%= HtmlLocalise("PossibleSteps.Label", "Based on the gaps we found between your skills and experience and the skills and experience of people who have placed into jobs like these, here are possible next steps for you:")%></div>
						<div style="height: 10px;">
						</div>
						<div>
							<strong>
								<%= HtmlLocalise("SkillsLabel4.Label", "Skills")%>
							</strong>
						</div>
						<asp:Panel ID="panSkillTraining" runat="server">
							<div>
								<%= HtmlLocalise("AdditionalTraining.Label", "Additional training in these skills:")%></div>
							<div style="height: 10px;">
							</div>
							<asp:Repeater ID="SkillGapRepeater" runat="server" OnItemDataBound="SkillGapRepeater_ItemDataBound">
								<HeaderTemplate><ul id="triple" style="width: 100%; margin-bottom: 30px;"></HeaderTemplate>
									<ItemTemplate>
										<li style="line-height: 1.5em; float: left; padding-right:30px;">
											<asp:Label ID="lblSkillGap" runat="server"></asp:Label>
										</li>
									</ItemTemplate>
									<FooterTemplate></ul></FooterTemplate>
								</asp:Repeater>
								<br class="clear"/>
						</asp:Panel>
						<asp:Label ID="lblSkillTrainingInfo" runat="server" Visible="false"></asp:Label>
						<div style="height: 5px;">
						</div>
						<div>
							<strong>
								<%= HtmlLocalise("EducationLabel2.Label", "Education")%>
							</strong>
						</div>
						<asp:Panel ID="panDegreeRequired" runat="server">
							<ul>
								<asp:Repeater ID="DegreeRepeater" runat="server" OnItemDataBound="DegreeRepeater_ItemDataBound">
									<ItemTemplate>
										<li>
											<%# Eval("Name") %>&nbsp;
											<ul>
												<asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound">
													<ItemTemplate>
														<li>
															<asp:Label ID="lblDegreeEducationLevel" CssClass="modalLink" runat="server" />&nbsp;
														</li>
													</ItemTemplate>
												</asp:Repeater>
											</ul>
										</li>
									</ItemTemplate>
								</asp:Repeater>
							</ul>
						</asp:Panel>
						<asp:Label ID="lblDegreeRequiredInfo" runat="server" Visible="false"></asp:Label>
					  </td>
					<td>
					</td>
				</tr>
						</table>
					</td>
				</tr>
			</table>
	</asp:Panel>
	<script type="text/javascript">
		Sys.Application.add_load(function () {
			equaliseCareerPathItems();
			$('#CommonCareerPathTitle, #CommonCareerPathTitle .show').click(equaliseCareerPathItems);
		});
	</script>
</asp:Content>
