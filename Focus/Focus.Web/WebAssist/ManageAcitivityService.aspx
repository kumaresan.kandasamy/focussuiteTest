﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageAcitivityService.aspx.cs"
    MasterPageFile="~/Site.Master" Inherits="Focus.Web.WebAssist.ManageAcitivityService" %>

<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ManageAssistNavigation" Src="~/WebAssist/Controls/ManageAssistNavigation.ascx" %>
<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/Localise.ascx" TagName="Localise" TagPrefix="uc" %>


<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    
    <style type="text/css">
    select {
  border: 1px solid #90c8c1 !important;
  background: #ffffff !important;
  background: -moz-linear-gradient(top, white 1%, #dfe9e7 100%) !important;
  background: -webkit-linear-gradient(top, white 1%, #dfe9e7 100%) !important;
  background: -o-linear-gradient(top, white 1%, #dfe9e7 100%) !important;
  background: -ms-linear-gradient(top, white 1%, #dfe9e7 100%) !important;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#dfe9e7', GradientType=0) !important;
  /*background: -webkit-gradient(linear, center top, center bottom, from(#ffffff), to(#dfe9e7)) !important;*/
  color: #226c67 !important;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  -ms-border-radius: 3px;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
  border-radius: 3px;
  font-weight: bold;
  height: 26px; }
    
</style>

</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />
    
    <div runat="server" id="ManageActivityList">
        <table style="width: 100%;" role="presentation">
            <tr>
                <td colspan="2">
                    <div width="100px">
                        <h1>
                            Manage Activities/Services</h1>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    Activity type:
                    <asp:DropDownList runat="server" ID="ManageTypeDropdown" title="Type dropdown" data-bind="value: activityTypeSelected "
                        Visible="true" />
                    Display
                    <select data-bind="value: recordsPerPage" title="records per page">
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    records per page
                </td>
                <td style="width: 100%; text-align: center">
                    <span data-bind="text: filteredActivities().length + ' records found'" ></span>
                </td>
                <td style="white-space: nowrap;">
                    Page :
                </td>
                <td style="white-space: nowrap;" class="extended-pager-table">
                    <a data-bind="click: previousPage, attr: {href: previousHref}, css: previousClass">&#x00ab;
                        Previous</a>
                </td>
                <td style="white-space: nowrap;" class="extended-pager-table">
                    <select data-bind="options: pages, value: pageNumber" title="xxx" ></select>
                </td>
                <td style="white-space: nowrap;" class="extended-pager-table">
                    <span data-bind="text: ' of ' + totalPages()" ></span>
                </td>
                <td style="white-space: nowrap;" class="extended-pager-table">
                    <a data-bind="click: nextPage, attr: {href: nextHref}, css: nextClass">Next &#x00bb;</a></td>
                    <td class="extended-pager-table">
                        <input type="button" class="button3" data-bind="click: save" value="Save" />
                    </td>
                
            </tr>
        </table>
        <table style="width: 100%;" class="table">
            <thead>
                <tr>
                    <th id="ManageCategoryTableHeader">
                        <table role="presentation">
                            <tr>
                                <td rowspan="2">
                                    <asp:Literal runat="server" ID="ManageCategoryHeader">Category</asp:Literal>
                                </td>
                                <td>
                                    <asp:ImageButton runat="server" ID="ManageCategorySortAsc" CssClass="AscButton"  AlternateText="sort up based on category" data-bind="click: sortCategoryAsc" />
                                    <asp:ImageButton runat="server" ID="ManageCategorySortDesc" CssClass="DescButton" AlternateText="sort down based on category"
                                        data-bind="click: sortCategoryDesc" />
                                </td>
                            </tr>
                        </table>
                    </th>
                    <th id="ManageActivityTableHeader">
                        <table role="presentation">
                            <tr>
                                <td rowspan="2">
                                    <asp:Literal runat="server" ID="ManageActivityHeader">Activity/Service</asp:Literal>
                                </td>
                                <td>
                                    <asp:ImageButton runat="server" ID="ManageActivitySortAsc" CssClass="AscButton" AlternateText="sort up based on activity" data-bind="click: sortActivityAsc" />
                                    <asp:ImageButton runat="server" ID="ManageActivitySortDesc" CssClass="DescButton" AlternateText="sort down based on activity"
                                        data-bind="click: sortActivityDesc" />
                                </td>
                            </tr>
                        </table>
                    </th>
                    <th id="ManageEnabledTableHeader">
                        <asp:Literal runat="server" ID="ManageEnabledHeader">Enable/Disable</asp:Literal>
                    </th>
                </tr>
            </thead>
            <tbody data-bind="foreach: pagedFilteredActivities">
                <tr>
                    <td headers="ManageCategoryTableHeader">
                        <span runat="server" data-bind="text: CategoryName, visible: !Editable" />
                        <input type="text" runat="server" data-bind="value: CategoryName, valueUpdate: 'keyup', visible: Editable" title="category name"/>
                    </td>
                    <td headers="ManageActivityTableHeader">
                        <span runat="server" data-bind="text: ActivityName, visible: !Editable" />
                        <input type="text" runat="server" data-bind="value: ActivityName, visible: Editable" title="activity name" />
                    </td>
                    <td headers="ManageEnabledTableHeader">
                        <input type="checkbox" data-bind="checked: ActivityVisible" title="activity visible" />
                    </td>
                </tr>
            </tbody>
        </table>
        <asp:HiddenField ID="ModalDummyTarget" runat="server" />
        <act:ModalPopupExtender ID="ConfirmationPopup" runat="server" ClientIDMode="Static"
            TargetControlID="ModalDummyTarget" PopupControlID="ModalPanel" PopupDragHandleControlID="ModalPanel"
            RepositionMode="RepositionOnWindowResizeAndScroll" BackgroundCssClass="modalBackground"
            BehaviorID="ConfirmationPopupBehavior" />
        <asp:Panel ID="ModalPanel" runat="server" CssClass="modal conditionallyScrolledModal"
            Style="display: none;">
            <table>
                <tr>
                    <th style="vertical-align: top;">
                        <asp:Literal ID="ManageTitle" runat="server">Save activities</asp:Literal>
                    </th>
                </tr>
                <tr>
                    <td style="vertical-align: top;">
                        <span runat="server" data-bind="text: saveMessage" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <input type="button" runat="server" title="button" data-bind="value: okText" class="button1" onclick="$find('ConfirmationPopupBehavior').hide();;return false;" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
