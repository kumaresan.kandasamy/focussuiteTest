﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Permissions;
using System.Text;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Job;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistPostingApprover)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistPostingApprovalViewer)]
	public partial class PostingReferrals : PageBase
	{
		#region Properties

		private JobPostingReferralCriteria Criteria
		{
			get { return GetViewStateValue<JobPostingReferralCriteria>("PostingReferrals:Criteria"); }
			set { SetViewStateValue("PostingReferrals:Criteria", value); }
		}

		private bool ApprovalQueueFilterVisible()
		{
			return App.Settings.Theme == FocusThemes.Workforce;
		}

		private int _referralCount;
		private bool _reApplyFilter;

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Localise();

			if (!IsPostBack)
			{

				// Re-set search criteria if returning from a page following a filtered result
				if (Request.QueryString["filtered"] != null)
				{
					_reApplyFilter = Convert.ToBoolean(Request.QueryString["filtered"]);
					Criteria = App.GetSessionValue("PostingReferrals:Criteria", new JobPostingReferralCriteria());

					// Set the Pager control to the previous Listview page and pagesize
					ReferralListPager.ReturnToPage(Convert.ToInt32(Criteria.PageIndex), Convert.ToInt32(Criteria.PageSize));
				}

				BindStatusDropDown();
				BindReferralList();

				ApprovalQueueFilter.Visible = ApprovalQueueFilterVisible();
				ApprovalQueueFilter.Initialise(ApprovalQueueFilterType.Job, _reApplyFilter);
			}
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the referrals.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<JobPostingReferralViewDto> GetReferrals(JobPostingReferralCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;
			if (criteria.OrderBy.IsNull())
				criteria.OrderBy = orderBy;

			var referrals = ServiceClientLocator.JobClient(App).GetJobPostingReferrals(criteria);
			_referralCount = referrals.TotalCount;

			return referrals;
		}

		/// <summary>
		/// Gets the referrals count.
		/// </summary>
		/// <returns></returns>
		public int GetReferralsCount()
		{
			return _referralCount;
		}

		/// <summary>
		/// Gets the referrals count.
		/// </summary>
		/// <returns></returns>
		public int GetReferralsCount(JobPostingReferralCriteria criteria)
		{
			return _referralCount;
		}

		#endregion

		/// <summary>
		/// Binds the referral list.
		/// </summary>
		private void BindReferralList()
    {
			ReferralList.DataBind();

			ResultCount.Text = ReferralListPager.TotalRowCount == 1
				? CodeLocalise("ResultCount1.Text", "1 posting found", ReferralListPager.TotalRowCount.ToString(CultureInfo.CurrentUICulture))
				: CodeLocalise("ResultCount.Text", "{0} postings found", ReferralListPager.TotalRowCount.ToString(CultureInfo.CurrentUICulture));

			ReferralListPager.Visible = ReferralList.Visible = (ReferralListPager.TotalRowCount > 0);

			if (ReferralListPager.TotalRowCount > 0)
			{
				((Literal)ReferralList.FindControl("JobTitleHeader")).Text = HtmlLocalise("JobTitle.ColumnTitle", "#POSTINGTYPE# title");
				((Literal)ReferralList.FindControl("EmployerNameHeader")).Text = HtmlLocalise("EmployerName.ColumnTitle", "#BUSINESS# name");
				((Literal)ReferralList.FindControl("PostingDateHeader")).Text = HtmlLocalise("PostingDate.ColumnTitle", "Posting date");
				((Literal)ReferralList.FindControl("TimeInQueueHeader")).Text = HtmlLocalise("TimeInQueue.ColumnTitle", "Time in queue");

			  FormatSortingImages();
			}
		}

    /// <summary>
    /// Formats the sorting images
    /// </summary>
    private void FormatSortingImages()
    {
      var orderBy = "";
      if (Criteria.OrderBy.IsNotNullOrEmpty())
        orderBy = Criteria.OrderBy.ToLower();

      FormatSortButton("JobTitleSortAscButton", "jobtitle asc", orderBy);
      FormatSortButton("JobTitleSortDescButton", "jobtitle desc", orderBy);
      FormatSortButton("EmployerNameAscButton", "employername asc", orderBy);
      FormatSortButton("EmployerNameDescButton", "employername desc", orderBy);
      FormatSortButton("AwaitingApprovalOnAscButton", "awaitingapprovalon asc", orderBy);
      FormatSortButton("AwaitingApprovalOnDescButton", "awaitingapprovalon desc", orderBy);
      FormatSortButton("TimeInQueueAscButton", "timeinqueue asc", orderBy);
      FormatSortButton("TimeInQueueDescButton", "timeinqueue desc", orderBy);
    }

    /// <summary>
    /// Formats a sorting button
    /// </summary>
    /// <param name="buttonId">The id of the button</param>
    /// <param name="buttonSort">The "order by" for the button</param>
    /// <param name="currentSort">The current "order by" for the list</param>
    private void FormatSortButton(string buttonId, string buttonSort, string currentSort)
    {
      var button = (ImageButton)ReferralList.FindControl(buttonId);
      button.Enabled = (currentSort != buttonSort);
      if (buttonSort.EndsWith("asc", true, CultureInfo.InvariantCulture))
        button.ImageUrl = button.Enabled ? UrlBuilder.ActivityOnSortAscImage() : UrlBuilder.ActivityListSortAscImage();
      else
        button.ImageUrl = button.Enabled ? UrlBuilder.ActivityOnSortDescImage() : UrlBuilder.ActivityListSortDescImage();
    }

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			Page.Title = CodeLocalise("Page.Title", "Approval Queues-Posting Referrals");
			FilterButton.Text = CodeLocalise("FilterButton.Text.NoEdit", "Go");
		}

		/// <summary>
		/// Binds the status drop down.
		/// </summary>
		private void BindStatusDropDown()
		{
			StatusDropDown.Items.Clear();

      StatusDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.WaitingApproval, "New requests"), ApprovalStatuses.WaitingApproval.ToString()));
      StatusDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.Reconsider, "Reconsider"), ApprovalStatuses.Reconsider.ToString()));
			StatusDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.Rejected, "Denied requests"), ApprovalStatuses.Rejected.ToString()));

			StatusDropDown.SelectedIndex = 0;

			if (_reApplyFilter && Criteria.IsNotNull() && Criteria.ApprovalStatus.IsNotNull())
				StatusDropDown.SelectedValue = Criteria.ApprovalStatus.ToString();
		}

		/// <summary>
		/// Unbinds the search.
		/// </summary>
		private void UnbindSearch()
		{
			if (ApprovalQueueFilterVisible())
			{
				var filterModel = ApprovalQueueFilter.Unbind();

				Criteria = new JobPostingReferralCriteria
				{
					OfficeIds = filterModel.OfficeId.HasValue ? new List<long?> { filterModel.OfficeId } : null,
					JobType = filterModel.JobType,
					OfficeId = filterModel.OfficeId,
					StaffMemberId = filterModel.StaffMemberId,
					ApprovalStatus = (ApprovalStatuses)Enum.Parse(typeof(ApprovalStatuses), StatusDropDown.SelectedValue, true),
				};
			}
			else
			{
				Criteria = new JobPostingReferralCriteria
				{
					ApprovalStatus = (ApprovalStatuses)Enum.Parse(typeof(ApprovalStatuses), StatusDropDown.SelectedValue, true),
				};
			}
		}

		/// <summary>
		/// Handles the Clicked event of the FilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void FilterButton_Clicked(object sender, EventArgs e)
		{
			if (!_reApplyFilter)
      ReferralListPager.ReturnToFirstPage();

			UnbindSearch();

			BindReferralList();

			App.SetSessionValue("PostingReferrals:Criteria", Criteria);
		}

		protected void ReferralDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (Criteria.IsNull())
			{
				if (!_reApplyFilter)
				{
					if (ApprovalQueueFilterVisible())
					{
						var filterModel = ApprovalQueueFilter.Unbind();
						Criteria = new JobPostingReferralCriteria
						{
							ApprovalStatus = ApprovalStatuses.WaitingApproval,
							OfficeId = filterModel.OfficeId,
							OfficeIds = filterModel.OfficeId.HasValue ? new List<long?> {filterModel.OfficeId} : null,
						};
					}
					else
					{
						Criteria = new JobPostingReferralCriteria
						{
							ApprovalStatus = ApprovalStatuses.WaitingApproval
						};
					}
				}
			}

			e.InputParameters["criteria"] = Criteria;
			App.SetSessionValue("PostingReferrals:Criteria", Criteria);
		}

    /// <summary>
    /// Handles the Sorting event of the ReferralList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ListViewSortEventArgs"/> instance containing the event data.</param>
    protected void ReferralList_Sorting(object sender, ListViewSortEventArgs e)
    {
      #region Job order list sorting

      Criteria.OrderBy = e.SortExpression;
      FormatSortingImages();

      #endregion
    }
	}
}