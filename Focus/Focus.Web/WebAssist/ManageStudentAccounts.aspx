﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageStudentAccounts.aspx.cs" Inherits="Focus.Web.WebAssist.ManageStudentAccounts" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/ManageAssistNavigation.ascx" tagname="ManageAssistNavigation" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />
	<div>
		<asp:Button ID="SendEmailButton" runat="server" class="button3 right" OnClick="SendEmailButton_Clicked" CausesValidation="true" />
		<div style="float: left; width: 50%;">
			<div>
				<h1><%= HtmlLocalise("Header.Text", "Invite users")%></h1>
			
			</div>
			<div >
				<asp:RadioButton runat="server" ID="SingleUserRadioButton" GroupName="InviteUserGroup" TextAlign="Right" Checked="True" CausesValidation="False" ClientIDMode="Static"/>
			</div>
		  <div >
				<span class="inFieldLabel"><label for="EmailTextBox" style="width: 275px"><%= HtmlLocalise("EmailText.InlineLabel", "Email address")%></label></span>
				<asp:TextBox runat="server" ID="EmailTextBox" Width="275" ClientIDMode="Static"/>
				<asp:RequiredFieldValidator ID="EmailTextBoxRequired" runat="server" ControlToValidate="EmailTextBox" CssClass="error"  SetFocusOnError="true" ClientIDMode="Static" Enabled="false" />
			</div>
			<br />
			<div>
				<asp:RadioButton runat="server" ID="MultipleUserRadioButton" GroupName="InviteUserGroup" TextAlign="Right" CausesValidation="False" ClientIDMode="Static"/>
			</div>
			<div id="ReminderSection" class="opacity" runat="server">
				<div id="ReminderOverlay" class="opacity" runat="server"></div>
				<div>
          <asp:FileUpload ID="FileUpload" runat="server" CssClass="fileupload" ClientIDMode="Static"/>
					<asp:RequiredFieldValidator ID="FileUploadRequired" runat="server" ControlToValidate="FileUpload"	CssClass="error" SetFocusOnError="false" ClientIDMode="Static" Enabled="false" Display="Dynamic" />
					<asp:CustomValidator ID="FileUploadCustomValidator" ValidateEmptyText="false" runat="server" ControlToValidate="FileUpload" CssClass="error" SetFocusOnError="false" ClientIDMode="Static" Enabled="false" ClientValidationFunction="validateFileUpload" Display="Dynamic"/>
				</div>
			</div>		
		</div>
    <br />
		<div style="float: right; vertical-align:top; width: 45%; display:none;" class="subItemsPanel" id="StatusDiv" runat="server">
			<div style="font-weight: bold">
				<focus:LocalisedLabel runat="server" ID="StatusTitleLabel" LocalisationKey="StatusTitle.Label" DefaultText="Status" />
			</div>
			<div>
				<focus:LocalisedLabel runat="server" ID="StatusLabel" LocalisationKey="Status.Label"/>
			</div>
		</div>
	</div>

<script type="text/javascript">
  $(document).ready(function () {
    $('.fileupload').customFileInput('Browse', 'Change', 'Upload a file');

    $("#<%= MultipleUserRadioButton.ClientID %>").change(function () {
      if ($(this).is(":checked")) {
        $("#<%=EmailTextBoxRequired.ClientID %>").css({ "visibility": "hidden" });
        $("#<%=EmailTextBox.ClientID %>").val("");
        $("#<%=EmailTextBox.ClientID %>").blur();
      }
      ManageStudentAccounts_HandleRadioButton(false);
    });

    $("#<%= SingleUserRadioButton.ClientID %>").change(function () {
      if ($(this).is(":checked")) {
        var upload = $("#<%=FileUpload.ClientID %>");
        upload.wrap('<form>').closest('form').get(0).reset();
        upload.unwrap();
        upload.prev("span.customfile-feedback").text("Upload a file");

        $("#<%=FileUploadRequired.ClientID %>").css({ "visibility": "hidden" });
        $("#<%=FileUploadCustomValidator.ClientID %>").css({ "visibility": "hidden" });
      }
      ManageStudentAccounts_HandleRadioButton(true);
    });

    ManageStudentAccounts_HandleRadioButton($("#SingleUserRadioButton").is(":checked"));
  });
  
  function ManageStudentAccounts_HandleRadioButton(isEmailTextBox) {
    $("#<%= ReminderSection.ClientID %>").removeClass("opacity");
    $("#<%= ReminderOverlay.ClientID %>").removeClass("overlay");

    $("#<%=EmailTextBox.ClientID%>").attr('disabled', !isEmailTextBox);
    $("#<%=FileUpload.ClientID %>").attr('disabled', isEmailTextBox);
    document.getElementById("<%=EmailTextBoxRequired.ClientID %>").enabled = isEmailTextBox;
    document.getElementById("<%=FileUploadRequired.ClientID %>").enabled = !isEmailTextBox;
    document.getElementById("<%=FileUploadCustomValidator.ClientID %>").enabled = !isEmailTextBox;
   }

   function validateFileUpload(src, args) {
	  var isValid = true;
		var fileUpload = $("#FileUpload");
   	var fileName = fileUpload.val();
	  var fileExtension = fileName.substr((fileName.lastIndexOf('.') + 1));
   	
	  if ($.inArray(fileExtension, ['csv', 'txt']) == -1) {
	  	isValid = false;
		}

	  args.IsValid = isValid;
   }
</script>
</asp:Content>


