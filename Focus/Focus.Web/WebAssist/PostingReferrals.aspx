﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PostingReferrals.aspx.cs"
Inherits="Focus.Web.WebAssist.PostingReferrals" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/ApprovalQueuesNavigation.ascx" tagname="ApprovalQueuesNavigation" tagprefix="uc"
%>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register src="~/WebAssist/Controls/ApprovalQueueFilter.ascx" tagname="ApprovalQueueFilter" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:ApprovalQueuesNavigation ID="ApprovalQueuesNavigationMain" runat="server" />
	<h1>
		<%= HtmlLocalise( "PostingReferrals.Header", "Approve job postings")%>
	</h1>
	<table role="presentation" style="width:100%;">
		<tr>
			<td style="width:50px;">
				<%= HtmlLabel(StatusDropDown,"Status.Text", "Status")%>&nbsp;</td>
			<td style="width:150px">
				<asp:DropDownList runat="server" ID="StatusDropDown" />
			</td>
			<td>
				<uc:ApprovalQueueFilter ID="ApprovalQueueFilter" runat="server" />
			</td>
			<td style="text-align: right">
				<asp:Button ID="FilterButton" runat="server" class="button3" OnClick="FilterButton_Clicked" />
			</td>
		</tr>
	</table>
	<table role="presentation" style="width:100%;">
		<tr>
			<td style="width:50%;">
				<asp:literal ID="ResultCount" runat="server" />
			</td>
			<td style="text-align:right; white-space: nowrap;">
				<uc:Pager ID="ReferralListPager" runat="server" PagedControlID="ReferralList" PageSize="25" />
			</td>
		</tr>
	</table>
	<asp:ListView ID="ReferralList" runat="server" ItemPlaceholderID="ReferralPlaceHolder" DataSourceID="ReferralDataSource" OnSorting="ReferralList_Sorting">
		<LayoutTemplate>
			<table ID="ReferralListTable" class="table" style="width: 100%;">
				<tr>
					<th style="width:25%;">
						<table role="presentation">
							<tr>
								<td rowspan="2" style="height: 16px;">
									<asp:Literal ID="JobTitleHeader" runat="server" />
								</td>
								<td style="height: 8px;">
									<asp:ImageButton ID="JobTitleSortAscButton" runat="server" CommandName="Sort"
									CommandArgument="jobtitle asc" CssClass="AscButton" AlternateText="Sort Ascending" />
									<asp:ImageButton ID="JobTitleSortDescButton" runat="server" CommandName="Sort"
									CommandArgument="jobtitle desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
					<th style="width:25%;">
					<table role="presentation">
							<tr>
								<td rowspan="2" style="height: 16px;">
									<asp:Literal ID="EmployerNameHeader" runat="server" />
								</td>
								<td style="height: 8px;">
									<asp:ImageButton ID="EmployerNameAscButton" runat="server" CommandName="Sort"
									CommandArgument="EmployerName asc" CssClass="AscButton" AlternateText="Sort Ascending" />
									<asp:ImageButton ID="EmployerNameDescButton" runat="server" CommandName="Sort"
									CommandArgument="EmployerName desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
					<th style="width:25%;">
					<table role="presentation">
							<tr>
								<td rowspan="2" style="height: 16px;">
									<asp:Literal ID="PostingDateHeader" runat="server" />
								</td>
								<td style="height: 8px;">
									<asp:ImageButton ID="AwaitingApprovalOnAscButton" runat="server" CommandName="Sort"
									CommandArgument="AwaitingApprovalOn asc" CssClass="AscButton" AlternateText="Sort Ascending" />
									<asp:ImageButton ID="AwaitingApprovalOnDescButton" runat="server" CommandName="Sort"
									CommandArgument="AwaitingApprovalOn desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
					<th style="width:25%;">
					<table role="presentation">
							<tr>
								<td rowspan="2" style="height: 16px;">
									<asp:Literal ID="TimeInQueueHeader" runat="server" />
								</td>
								<td style="height: 8px;">
									<asp:ImageButton ID="TimeInQueueAscButton" runat="server" CommandName="Sort"
									CommandArgument="TimeInQueue asc" CssClass="AscButton" AlternateText="Sort Ascending" />
									<asp:ImageButton ID="TimeInQueueDescButton" runat="server" CommandName="Sort"
									CommandArgument="TimeInQueue desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
				</tr>
				<asp:PlaceHolder ID="ReferralPlaceHolder" runat="server" />
			</table>
		</LayoutTemplate>
		<ItemTemplate>
			<tr style="vertical-align:top;">
				<td style="width:25%;">
					<a href="<%# UrlBuilder.PostingReferral(((JobPostingReferralViewDto)Container.DataItem).Id) %>?filtered=true"><%# ((JobPostingReferralViewDto)Container.DataItem).JobTitle %></a>
				</td>
				<td style="width:25%;">
					<%# ((JobPostingReferralViewDto)Container.DataItem).EmployerName%>
				</td>
				<td style="width:25%;">
					<%# ((JobPostingReferralViewDto)Container.DataItem).AwaitingApprovalOn.GetValueOrDefault().ToString( "MMM d, yyyy")%>
				</td>
				<td style="width:25%;">
					<%# ((JobPostingReferralViewDto)Container.DataItem).TimeInQueue%>
						<%= HtmlLocalise( "Days.Label", "day(s)") %>
				</td>
			</tr>
		</ItemTemplate>
	</asp:ListView>
	<asp:ObjectDataSource ID="ReferralDataSource" runat="server" TypeName="Focus.Web.WebAssist.PostingReferrals" EnablePaging="true"
	SelectMethod="GetReferrals" SelectCountMethod="GetReferralsCount" OnSelecting="ReferralDataSource_Selecting" SortParameterName="orderBy"></asp:ObjectDataSource>
</asp:Content>