﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core.Criteria.Employee;
using Focus.Core.Criteria.Report;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Core;
using Focus.Web.Code;
using Focus.Web.WebAssist.Controls;
using Framework.Core;

using Constants = Focus.Core.Constants;
using Focus.Services.Messages;

using Focus.Services.Helpers;

#endregion


namespace Focus.Web.WebAssist
{
    [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
    [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersSendMessages)]
    public partial class MessageEmployers : PageBase
    {
        private int _employeesCount;
        private List<SavedMessageTextDto> _savedMessageTexts;

        private EmployersReportCriteria EmployerCriteria
        {
            get { return GetViewStateValue<EmployersReportCriteria>("MessageEmployers:EmployerCriteria"); }
            set { SetViewStateValue("MessageEmployers:EmployerCriteria", value); }
        }

        private EmployeeCriteria BasicCriteria
        {
            get { return GetViewStateValue<EmployeeCriteria>("MessageEmployers:BasicCriteria"); }
            set { SetViewStateValue("MessageEmployers:BasicCriteria", value); }
        }

        private EmailTemplateDto EmailTemplate
        {
            get { return GetViewStateValue<EmailTemplateDto>("MessageJobSeekers:EmailTemplate"); }
            set { SetViewStateValue("MessageJobSeekers:EmailTemplate", value); }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = CodeLocalise("Page.Title", "Assist #BUSINESSES#-MesssageEmployers");
            LocaliseUI();

            if (!IsPostBack)
            {
                ApplyTheme();

                _savedMessageTexts = null;
                GetEmailTemplate();
                BindSavedMessagesDropDown();
                BindMessageFormatRadioButtonList();
                CriteriaLocation.BindCriteria(null, ReportType.Employer);

                // For initial realease just show Employer Characterisitics
                EmployerCriteriaBuilder.CriteriaBuildersToShow = CriteriaBuilder.Criterias.EmployerCharacteristics;

                ResetComposeMessageForm();
            }
        }

        /// <summary>
        /// Handles the Clicked event of the NewReportButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void NewSearchButton_Clicked(object sender, EventArgs e)
        {
            Response.Redirect(UrlBuilder.MessageEmployers());
        }

        /// <summary>
        /// Handles the Clicked event of the EmployerCriteriaBuilderCommandButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.CriteriaBuilder.CommandButtonClickedEventArgs"/> instance containing the event data.</param>
        protected void EmployerCriteriaBuilderCommandButton_Clicked(object sender, CriteriaBuilder.CommandButtonClickedEventArgs e)
        {
            // run the search
            BasicCriteria = new EmployeeCriteria
            {
                Location = e.LocationFilter,
                JobCharacteristics = e.JobCharacteristicsFilter,
                EmployerCharacteristics = e.EmployerCharacteristicsFilter,
                Compliance = e.ComplianceFilter,
                PageSize = App.Settings.EmployeeSearchMaximumRecordsToReturn,
                ListSize = App.Settings.EmployeeSearchMaximumRecordsToReturn
            };

            BindEmployeesList();

            SavedMessageNameTextBox.Text = string.Empty;
            MessageTextbox.Text = string.Empty;
        }

        /// <summary>
        /// Handles the Clicked event of the FindButton_ control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void FindButton_Clicked(object sender, EventArgs e)
        {
            EmployerCriteria = new EmployersReportCriteria
            {
                SalaryInfo = CriteriaJobCharacteristics.UnbindSalary(),
                JobOrderCharacteristicsInfo = CriteriaJobCharacteristics.UnbindCharacteristics(),
                KeywordInfo = CriteriaJobCharacteristics.UnbindKeyWords(),
                EmployerCharacteristicsInfo = CriteriaEmployerCharacteristics.Unbind(),
                JobOrderComplianceInfo = CriteriaCompliance.Unbind(),
                EmployersEmployerActivityInfo = CriteriaEmployerActivity.Unbind(),
                DateRangeInfo = CriteriaDateRange.UnBind(),
                LocationInfo = CriteriaLocation.Unbind(),
                DisplayType = ReportDisplayType.Table,
                ChartGroupInfo = null,
                PageSize = int.MaxValue,
                ZeroesHandling = ReportZeroesHandling.ExcludeAnyZeroes
            };

            BindEmployeesList();

            SavedMessageNameTextBox.Text = string.Empty;
            MessageTextbox.Text = string.Empty;

            CriteriaSelectionPanel.Visible = false;
            CriteriaDisplay.Visible = true;
            CriteriaDisplay.SetCriteriaPanelCollapsedState(false);
            CriteriaDisplay.ShowCriteria(EmployerCriteria);
        }

        /// <summary>
        /// Handles the ActionRequested event of the ReportCriteriaDisplay control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
        protected void ReportCriteriaDisplay_ActionRequested(object sender, CommandEventArgs eventargs)
        {
            switch (eventargs.CommandName)
            {
                case "EditCriteria":
                    if (App.Settings.ReportingEnabled)
                    {
                        CriteriaSelectionPanel.Visible = true;
                        CriteriaDisplay.Visible = false;
                    }
                    break;

                case "NewReport":
                    Response.Redirect(UrlBuilder.MessageEmployers());
                    break;
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the MessageFormatRadioButtonList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void MessageFormatRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reset saved messages dropdown
            SavedMessagesDropDown.SelectedIndex = 0;
        }

        /// <summary>
        /// Handles the ItemDataBound event of the CultureSpecificMessagesRepeater control.
        /// </summary>
        /// <param name="source">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
        protected void CultureSpecificMessagesRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
        {
            // Do nothing if this isn't a Item or AlternateItem
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
                return;

            var localisationIdHidden = (HiddenField)e.Item.FindControl("LocalisationIdHidden");
            localisationIdHidden.Value = ((LocalisationDto)e.Item.DataItem).Id.GetValueOrDefault().ToString(CultureInfo.InvariantCulture);

            var cultureNameLabel = (Label)e.Item.FindControl("CultureNameLabel");
            cultureNameLabel.Text = ((LocalisationDto)e.Item.DataItem).Culture;

            var cultureSpecificMessageTextbox = (TextBox)e.Item.FindControl("CultureSpecificMessageTextbox");

            var cultureSpecificMessageInlineLabel = (Label)e.Item.FindControl("CultureSpecificMessageInlineLabel");
            cultureSpecificMessageInlineLabel.Text = CodeLocalise("CultureSpecificMesssage.InlineLabel", "enter message for the {0} culture", ((LocalisationDto)e.Item.DataItem).Culture);
            cultureSpecificMessageInlineLabel.AssociatedControlID = cultureSpecificMessageTextbox.ID;

            if (_savedMessageTexts.IsNotNullOrEmpty())
            {
                var savedMessageText = _savedMessageTexts.FirstOrDefault(x => x.LocalisationId == ((LocalisationDto)e.Item.DataItem).Id.GetValueOrDefault());
                if (savedMessageText != null) cultureSpecificMessageTextbox.Text = savedMessageText.Text;
            }
        }

        /// <summary>
        /// Handles the Clicked event of the SendMessageButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SendMessageButton_Clicked(object sender, EventArgs e)
        {
            var selectedMessageFormat = MessageFormatRadioButtonList.SelectedValueToEnum<MessageFormats>();

            switch (selectedMessageFormat)
            {
                case MessageFormats.Email:
                    SendEmailAlerts();
                    break;
                case MessageFormats.HomepageAlert:
                    SendHomepageAlerts();
                    break;
            }

            ConfirmationModal.Show(CodeLocalise("MessageSentConfirmation.Title", "Message sent"),
                                                             CodeLocalise("MessageSentConfirmation.Body", "Your message has been sent to the selected users."),
                                                             CodeLocalise("CloseModal.Text", "OK"));
        }

        /// <summary>
        /// Handles the Clicked event of the SaveMessageTextButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SaveMessageTextButton_Clicked(object sender, EventArgs e)
        {
            var messageTexts = new List<SavedMessageTextDto>
			{
				new SavedMessageTextDto
				{
					SavedMessageId = 0,
					LocalisationId = 0,
					Text = MessageTextbox.TextTrimmed()
				}
			};

            // Add the default message text, set the LocalisationId to zero and the service will replace it with the correct one

            var message = new SavedMessageDto
                            {
                                Audience = MessageAudiences.Employer,
                                Name = SavedMessageNameTextBox.TextTrimmed(),
                                UserId = App.User.UserId
                            };

            ServiceClientLocator.CoreClient(App).SaveMessageTexts(message, messageTexts);

            // Rebind SavedMessagesDropDown to show new value
            BindSavedMessagesDropDown();

            ConfirmationModal.Show(CodeLocalise("MessageTextsSavedConfirmation.Title", "Message texts saved"),
                                                             CodeLocalise("MessageTextsSavedConfirmation.Body", "The message texts have been saved."),
                                                             CodeLocalise("CloseModal.Text", "OK"));
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the SavedMessagesDropDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SavedMessagesDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            _savedMessageTexts = (SavedMessagesDropDown.SelectedValueToLong() > 0) ? ServiceClientLocator.CoreClient(App).GetSavedMessageTexts(SavedMessagesDropDown.SelectedValueToLong()) : null;

            if (_savedMessageTexts.IsNotNullOrEmpty())
            {
                var defaultLocalisation = ServiceClientLocator.CoreClient(App).GetDefaultCulture();

                var savedMessageTextDto = _savedMessageTexts.FirstOrDefault(x => x.LocalisationId == defaultLocalisation.Id);
                if (savedMessageTextDto != null)
                    MessageTextbox.Text = savedMessageTextDto.Text;
            }
        }

        protected void EmployeesList_LayoutCreated(object sender, EventArgs e)
        {
            // Set table headings
            ((Literal)EmployeesList.FindControl("EmployerNameHeaderLabel")).Text = CodeLocalise("EmployerName.Header", "#BUSINESS# Name");
        }

        #region DataSource Methods

        /// <summary>
        /// Handles the OnItemDataBound event of the EmployeesList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
        protected void EmployeesList_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var checkBox = (CheckBox)e.Item.FindControl("SelectorCheckBox");
            checkBox.Attributes.Add("data-value", ((EmployeeSearchResultView)e.Item.DataItem).Id.ToString(CultureInfo.InvariantCulture));
            checkBox.Attributes.Add("data-businessunitid", ((EmployeeSearchResultView)e.Item.DataItem).BusinessUnitId.ToString(CultureInfo.InvariantCulture));
            checkBox.Attributes.Add("Title", ((EmployeeSearchResultView)e.Item.DataItem).EmployerName);
        }

        /// <summary>
        /// Handles the OnSelecting event of the SearchResultDataSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
        protected void SearchResultDataSource_OnSelecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["reportCriteria"] = null;
            e.InputParameters["basicCriteria"] = null;

            if (App.Settings.ReportingEnabled)
                e.InputParameters["reportCriteria"] = EmployerCriteria;
            else
                e.InputParameters["basicCriteria"] = BasicCriteria;
        }

        /// <summary>
        /// Gets the employees.
        /// </summary>
        /// <param name="reportCriteria">The criteria for reporting search.</param>
        /// <param name="basicCriteria">The criteria for basic search.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="startRowIndex">Start index of the row.</param>
        /// <param name="maximumRows">The maximum rows.</param>
        /// <returns>A list of employees</returns>
        public IEnumerable<EmployeeSearchResultView> GetEmployees(EmployersReportCriteria reportCriteria, EmployeeCriteria basicCriteria, string orderBy, int startRowIndex, int maximumRows)
        {
            var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

            if (reportCriteria.IsNotNull())
            {
                var employers = ServiceClientLocator.ReportClient(App).GetEmployersReport(reportCriteria);
                var businessUnitIds = employers.Select(employer => employer.FocusBusinessUnitId).ToList();
                var employees = ServiceClientLocator.EmployeeClient(App).GetEmployeesForBusinessUnits(businessUnitIds, pageIndex, maximumRows, App.Settings.UnsubscribeFromHMMessages);

                _employeesCount = employees.TotalCount;

                return employees;
            }
            else
            {
                basicCriteria.PageIndex = pageIndex;
                basicCriteria.PageSize = maximumRows;

                var employees = ServiceClientLocator.EmployeeClient(App).SearchEmployees(basicCriteria);

                _employeesCount = employees.TotalCount;

                return employees;
            }
        }

        /// <summary>
        /// Gets the employees count.
        /// </summary>
        /// <returns></returns>
        public int GetEmployeesCount()
        {
            return _employeesCount;
        }

        /// <summary>
        /// Gets the employees count.
        /// </summary>
        /// <param name="reportCriteria">The criteria for reporting search.</param>
        /// <param name="basicCriteria">The criteria for basic search.</param>
        /// <returns></returns>
        public int GetEmployeesCount(EmployersReportCriteria reportCriteria, EmployeeCriteria basicCriteria)
        {
            return _employeesCount;
        }

        #endregion

        /// <summary>
        /// Gets the email template
        /// </summary>
        private void GetEmailTemplate()
        {
            EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplate(EmailTemplateTypes.SendMessageEmployer);
        }

        /// <summary>
        /// Binds the employees list.
        /// </summary>
        private void BindEmployeesList()
        {
            EmployeesList.DataBind();

            SelectedEmployeeIds.Value = "";
            SelectedBusinessUnitIds.Value = "";

            // Set visibility of other controls
            EmployeesList.Visible = true;
            PagerDiv.Visible = true;
            PageInstructions.Visible = false;
        }

        /// <summary>
        /// Binds the saved messages list.
        /// </summary>
        private void BindSavedMessagesDropDown()
        {
            SavedMessagesDropDown.DataSource = ServiceClientLocator.CoreClient(App).GetSavedMessages(MessageAudiences.Employer);
            SavedMessagesDropDown.DataValueField = "Id";
            SavedMessagesDropDown.DataTextField = "Name";
            SavedMessagesDropDown.DataBind();
            SavedMessagesDropDown.Items.AddLocalisedTopDefault("SavedMessagesDropDownList.TopDefault", "- select saved message -");
        }

        /// <summary>
        /// Binds the message format radio button list.
        /// </summary>
        private void BindMessageFormatRadioButtonList()
        {
            MessageFormatRadioButtonList.Items.Clear();

            MessageFormatRadioButtonList.Items.AddEnum(MessageFormats.Email, "email");
            MessageFormatRadioButtonList.Items.AddEnum(MessageFormats.HomepageAlert, "post to home page");

            MessageFormatRadioButtonList.SelectedIndex = 0;
        }

        /// <summary>
        /// Sends the homepage alerts.
        /// </summary>
        private void SendHomepageAlerts()
        {
            var businessUnits = GetSelectedBusinessUnitIds();

            var homePageAlerts = businessUnits.Select(businessUnitId => new BusinessUnitHomepageAlertView
            {
                BusinessUnitId = businessUnitId,
                ExpiryDate = DateTime.Now.AddDays(7),
                IsSystemAlert = true,
                Message = MessageTextbox.TextTrimmed()
            }).ToList();

            ServiceClientLocator.EmployerClient(App).CreateHomepageAlert(homePageAlerts);
        }

        /// <summary>
        /// Sends the email alerts.
        /// </summary>
        private void SendEmailAlerts()
        {
            var count = 0;
            var sendCopy = EmailMeCopyOfMessageCheckBox.Checked;

            var employees = GetSelectedEmployeeIds();
            var messageBody = MessageTextbox.TextTrimmed();

            var unsubscribeUrl = string.Empty;

            var footerType = EmailFooterTypes.None;

            if (App.Settings.UnsubscribeFromHMMessages)
            {
                unsubscribeUrl = string.Concat(App.Settings.TalentApplicationPath, UrlBuilder.UnsubscribeFromHmEmails(false));
                footerType = EmailFooterTypes.HiringManagerUnsubscribe;
            }


            var EmployeesEmails = new List<Focus.Core.Views.EmployeeEmailView>();
            var messageSubject = CodeLocalise("EmployerAlertEmail.Subject", "Focus Alert");
            var senderEmail = App.Settings.UseOfficeAddressToSendMessages
                                      ? ServiceClientLocator.EmployerClient(App).GetHiringManagerOfficeEmail(Convert.ToInt64(App.User.PersonId))
                            : ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplate);
            var employeeIdList = new List<long>();
            foreach (var employee in employees)
            {
                //var EmployeeEmail = new EmployeeEmailView();
                //EmployeeEmail.employeeId = employee;
                //EmployeeEmail.Subject = messageSubject;
                //EmployeeEmail.Body = messageBody;
                //EmployeeEmail.SenderAddress = senderEmail;
                //EmployeeEmail.BccRequestor = count == 0 && sendCopy;

                //EmployeesEmails.Add(EmployeeEmail);

                employeeIdList.Add(employee);
                //ServiceClientLocator.EmployeeClient(App).EmailEmployee(employee, messageSubject, messageBody, senderAddress: senderEmail, sendCopy: count == 0 && sendCopy, bccSelf: count == 0 && sendCopy, footerType: footerType, footerUrl: unsubscribeUrl.IsNotNullOrEmpty() ? unsubscribeUrl : null);
                count++;
            }
            //Blast mail 
            ServiceClientLocator.EmployeeClient(App).EmailEmployees(employeeIdList, messageSubject, messageBody, senderAddress: senderEmail, sendCopy: count == 0 && sendCopy, bccSelf: count == 0 && sendCopy, footerType: footerType, footerUrl: unsubscribeUrl.IsNotNullOrEmpty() ? unsubscribeUrl : null);

            //send blast mail for employers

            //var valueTobeSendForMessageBus = new SendBlastEmailMessages()
            //{
            //    EmployeesEmails = EmployeesEmails,
            //    FooterType = footerType,
            //    SendCopy = count == 0 && sendCopy,
            //    FooterUrl = unsubscribeUrl.IsNotNullOrEmpty() ? unsubscribeUrl : null,
            //    IsEmployeeEmail=true
            //};

            //Helpers.Messaging.Publish(valueTobeSendForMessageBus, null, true);

        }

        /// <summary>
        /// Gets the selected employer ids.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<long> GetSelectedBusinessUnitIds()
        {
            return SelectedBusinessUnitIds.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(id => id.AsLong());
        }

        /// <summary>
        /// Gets the selected employee ids.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<long> GetSelectedEmployeeIds()
        {
            return SelectedEmployeeIds.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(id => id.AsLong());
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            EmployerCriteriaBuilder.CommandButtonText = CodeLocalise("SearchEmployersButton.Text", "Find");

            ComposeMessageButton.Text = CodeLocalise("ComposeMessageButton.Text", "Compose message");
            CriteriaDisplay.ButtonText = CodeLocalise("CriteriaDisplay.ButtonText", "search");

            Column1Header.Text = CodeLocalise("Column1Header.Employer.Text", "Which #BUSINESSES#:LOWER are you looking for?");
            Column2Header.Text = CodeLocalise("Column2Header.Employer.Text", "View the #BUSINESS#:LOWER activity");
            Column3Header.Text = CodeLocalise("Column3Header.Employer.Text", "Where are the #BUSINESSES#:LOWER located, and what's the overall time period for this search?");

            PageInstructions.Text = CodeLocalise("PageInstructions.Text", "Find message recipients using filters left. Results will appear here.");
            SendMessageButton.Text = CodeLocalise("SendMessageButton.Text", "Send message");
            SaveMessageTextButton.Text = CodeLocalise("SaveMessageTextButton.Text", "Save text to my messages");
            MessageRequired.ErrorMessage = MessageRequiredSaveText.ErrorMessage = CodeLocalise("MessageRequired.ErrorMessage", "Message text is required");
            EmployeesListValidator.ErrorMessage = CodeLocalise("EmployeesListValidator.ErrorMessage", "At least one employee must be selected");
            SavedMessageNameRequired.ErrorMessage = CodeLocalise("SavedMessageNameRequired.ErrorMessage", "Message name is required");
        }

        /// <summary>
        /// Hides/Shows controls depending on settings
        /// </summary>
        private void ApplyTheme()
        {
            if (App.Settings.ReportingEnabled)
            {
                CriteriaSelectionPanel.Visible = true;
                EmployerCriteriaBuilder.Visible = false;
                BasicCriteriaPanel.Visible = false;
            }
            else
            {
                CriteriaSelectionPanel.Visible = false;
                EmployerCriteriaBuilder.Visible = true;
                BasicCriteriaPanel.Visible = true;
            }
        }

        /// <summary>
        /// Resets the form
        /// </summary>
        private void ResetComposeMessageForm()
        {
            SavedMessageNameTextBox.ResetText();
        }
    }
}