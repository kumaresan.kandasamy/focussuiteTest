﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Document;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Assist;
using Focus.Web.Code;
using Focus.Web.WebAssist.Controls;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistManageDocuments)]
	public partial class ManageDocuments  : PageBase
	{
		#region Properties

		private List<GroupedDocumentsModel> _documentGroups { get; set; }

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			Title = CodeLocalise("Page.Title", "Manage Documents");

			if (!IsPostBack)
			{
				LocaliseUI();

				BindList();	
			}
		}

		private void LocaliseUI()
		{
			AddButton.Text = CodeLocalise("AddButton.Text", "Add Document");
		}

		/// <summary>
		/// Binds the list.
		/// </summary>
		private void BindList()
		{
			_documentGroups = ServiceClientLocator.CoreClient(App).GetGroupedDocuments(new DocumentCriteria());

			if (_documentGroups.IsNullOrEmpty()) return;

			DocumentGroupRepeater.DataSource = _documentGroups;
			DocumentGroupRepeater.DataBind();
		}

		/// <summary>
		/// Deletes the document.
		/// </summary>
		/// <param name="documentId">The document id.</param>
		private void DeleteDocument(long documentId)
		{
			ServiceClientLocator.CoreClient(App).DeleteDocument(documentId);
		}

		/// <summary>
		/// Handles the ItemDataBound event of the DocumentGroupRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RepeaterItemEventArgs" /> instance containing the event data.</param>
		protected void DocumentGroupRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var group = e.Item.DataItem as GroupedDocumentsModel;

				if (group.IsNotNull())
				{
					var groupLabel = e.Item.FindControl("GroupLabel") as Label;

					if (groupLabel.IsNotNull())
						groupLabel.Text = group.GroupName;
				}

				var documentRepeater = e.Item.FindControl("DocumentRepeater") as Repeater;

				if (documentRepeater.IsNull()) return;

				documentRepeater.DataSource = group.Documents;
				documentRepeater.DataBind();
			}
		}

		/// <summary>
		/// Handles the ItemDataBound event of the DocumentRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RepeaterItemEventArgs" /> instance containing the event data.</param>
		protected void DocumentRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var deleteButton = e.Item.FindControl("DeleteDocumentImageButton") as ImageButton;

				if (deleteButton.IsNotNull())
				{
					deleteButton.ToolTip = CodeLocalise("DocumentDeleteButton.ToolTip", "Delete document record");
					deleteButton.AlternateText = CodeLocalise("DocumentDeleteButton.AlternativeText", "Delete");
				}

				var editButton = e.Item.FindControl("EditDocumentImageButton") as ImageButton;

				if (editButton.IsNotNull())
				{
					editButton.ToolTip = CodeLocalise("DocumentEditButton.ToolTip", "Edit document record");
					editButton.AlternateText = CodeLocalise("DocumentEditButton.AlternativeText", "Edit");
				}
			}
		}

		/// <summary>
		/// Handles the ItemCommand event of the DocumentRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewCommandEventArgs" /> instance containing the event data.</param>
		protected void DocumentRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			var documentId = Convert.ToInt64(e.CommandArgument);

			switch (e.CommandName)
			{
				case "EditDocument":
					DocumentModal.Show(documentId);
					break;
				case "DeleteDocument":
					ConfirmationModal.Show(
					CodeLocalise("DeleteDocumentModal.Title", "Delete Document"),
					CodeLocalise("DeleteDocumentModal.Body", "If you are sure you want to delete this document, please select OK"),
					CodeLocalise("Global.Cancel.Text", "Cancel"),
					okText: CodeLocalise("Confirmation.Ok.Text", "OK"),
					okCommandName: "DeleteDocument",
					okCommandArgument: documentId.ToString(CultureInfo.InvariantCulture));
					break;
			}
		}

		/// <summary>
		/// Handles the Clicked event of the AddButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void AddButton_Clicked(object sender, EventArgs e)
		{
			DocumentModal.Show();
		}

		/// <summary>
		/// Handles the OkCommand event of the ConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
		protected void ConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			long documentId;
			long.TryParse(e.CommandArgument.ToString(), out documentId);

			switch (e.CommandName)
			{
				case "DeleteDocument":
					DeleteDocument(documentId);
					BindList();
					break;
			}
		}

		/// <summary>
		/// Handles the Completed event of the DocumentModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
		protected void DocumentModal_Completed(object sender)
		{
			BindList();
		}
	}
}