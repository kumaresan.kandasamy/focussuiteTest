﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Employee;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion


namespace Focus.Web.WebAssist
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersReadOnly)]
  public partial class ContactEmployer : PageBase
	{
    /// <summary>
    /// Gets or sets the Candidate.
    /// </summary>
    protected CandidateViewDto Candidate
    {
      get { return GetViewStateValue<CandidateViewDto>("ContactEmployer:Candidate"); }
      set { SetViewStateValue("ContactEmployer:Candidate", value); }
    }

    /// <summary>
    /// Gets or sets the Recent Placement.
    /// </summary>
    protected CandidateViewDto RecentPlacement
    {
      get { return GetViewStateValue<CandidateViewDto>("ContactEmployer:RecentPlacement"); }
      set { SetViewStateValue("ContactEmployer:RecentPlacement", value); }
    }

    /// <summary>
    /// Gets or sets the Employer.
    /// </summary>
    protected BusinessUnitProfileModel BusinessUnit
    {
      get { return GetViewStateValue<BusinessUnitProfileModel>("ContactEmployer:BusinessUnit"); }
      set { SetViewStateValue("ContactEmployer:BusinessUnit", value); }
    }

    /// <summary>
    /// Gets or sets the Hiring Manager.
    /// </summary>
    protected EmployeeSearchResultView HiringManager
    {
      get { return GetViewStateValue<EmployeeSearchResultView>("ContactEmployer:HiringManager"); }
      set { SetViewStateValue("ContactEmployer:HiringManager", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
			Page.Title = CodeLocalise("Page.Title", "Email #BUSINESS#:LOWER(es)");

      if (!IsPostBack)
      {
        var businessUnitId = long.Parse(Page.RouteData.Values["id"].ToString());
        var personId = long.Parse(Page.RouteData.Values["personid"].ToString());
        var otherPersonId = long.Parse(Page.RouteData.Values["otherpersonid"].ToString());

        Candidate = ServiceClientLocator.CandidateClient(App).GetCandidate(personId);
        BusinessUnit = ServiceClientLocator.EmployerClient(App).GetBusinessUnitProfile(businessUnitId);
        if (otherPersonId != 0)
          RecentPlacement = ServiceClientLocator.CandidateClient(App).GetCandidate(otherPersonId);

        LocaliseUI();
        Bind();
      }
    }

    /// <summary>
    /// Handles the OnSelectedIndexChanged event of the HiringManagersDropDown drop-down.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void HiringManagersDropDown_OnSelectedIndexChanged(object sender, EventArgs e)
    {
      var hiringManagerId = HiringManagersDropDown.SelectedValueToLong();
      var hiringManager = hiringManagerId > 0
                            ? ServiceClientLocator.EmployeeClient(App).SearchEmployees(new EmployeeCriteria { Id = hiringManagerId }).FirstOrDefault()
                            : null;

      SelectHiringManager(hiringManager);
    }

    /// <summary>
    /// Handles the Clicked event of the Log Action button.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see LogActionButton_Clicked="System.EventArgs"/> instance containing the event data.</param>
    protected void LogContactButton_Clicked(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the Clicked event of the Send Email button.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see LogActionButton_Clicked="System.EventArgs"/> instance containing the event data.</param>
    protected void SendEmailButton_Clicked(object sender, EventArgs e)
    {
      if (HiringManager.IsNotNull())
        SendEmail();
      else
        HiringManagersDropDownEmailValidator.IsValid = false;
    }

    /// <summary>
    /// Handles the Clicked event of the Add Note button.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see LogActionButton_Clicked="System.EventArgs"/> instance containing the event data.</param>
    protected void AddNoteButton_Clicked(object sender, EventArgs e)
    {
      if (HiringManager.IsNotNull())
        NotesAndReminders.Show(HiringManager.Id, EntityTypes.Employee, (!App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator)));
      else
        HiringManagersDropDownValidator.IsValid = false;
    }

    /// <summary>
    /// Localises the controls on the page
    /// </summary>
    private void LocaliseUI()
    {
      LogContactButton.Text = HtmlLocalise("LogAction.Button", "Log contact");
      SendEmailButton.Text = HtmlLocalise("SendEmail.Button", "Send email");
      AddNoteButton.Text = HtmlLocalise("AddNote.Button", "Add note/reminder");
      TelephoneRadioButton.Text = HtmlLocalise("Telephone.RadioButton", "telephone number");
      EmailRadioButton.Text = HtmlLocalise("Email.RadioButton", "email");
      AttachResumeCheckBox.Text = HtmlLocalise("AttachResume.CheckBox", "attach resume");
      AnonymiseResumeCheckBox.Text = HtmlLocalise("AnonymiseResume.CheckBox", "anonymize resume");

      HiringManagersDropDownValidator.ErrorMessage = HiringManagersDropDownEmailValidator.ErrorMessage = 
        HtmlLocalise("EmailTextBox.Required", "Please select a hiring manager");

      EmailSubjectTextBoxValidator.ErrorMessage = HtmlLocalise("EmailTextBox.Required", "Please enter the email subect");
      EmailBodyTextBoxValidator.ErrorMessage = HtmlLocalise("EmailTextBox.Required", "Please enter the email text");
    }

    /// <summary>
    /// Populates the controls on the page to information from data sources
    /// </summary>
    private void Bind()
    {
			CandidateNameLiteral.Text = HtmlLocalise("ContactInfo.Label", "Contact #BUSINESS#:LOWER about {0} {1}", Candidate.FirstName, Candidate.LastName);
      EmployerNameLiteral.Text = BusinessUnit.BusinessUnit.Name;
      
      BindHiringManagers();
    }

    /// <summary>
    /// Builds the Hiring Manager dropdown
    /// </summary>
    private void BindHiringManagers()
    {
      var criteria = new EmployeeCriteria
      {
        BusinessUnitId = BusinessUnit.BusinessUnit.Id.GetValueOrDefault(0)
      };

      var managers = ServiceClientLocator.EmployeeClient(App).SearchEmployees(criteria);
      foreach (var manager in managers)
      {
        HiringManagersDropDown.Items.Add(new ListItem(string.Concat(manager.FirstName, " ", manager.LastName), manager.Id.ToString(CultureInfo.InvariantCulture)));
      }

      if (managers.Count > 1)
        HiringManagersDropDown.Items.AddLocalisedTopDefault("HiringManager.TopDefault", "- select hiring manager -");
      else
        SelectHiringManager(managers[0]);
    }

    /// <summary>
    /// Gets the selected hiring manager and displays their phone number
    /// </summary>
    /// <param name="hiringManager"></param>
    private void SelectHiringManager(EmployeeSearchResultView hiringManager)
    {
      HiringManager = hiringManager;

      if (hiringManager.IsNotNull())
      {
        var phoneNumber = hiringManager.PhoneNumber ?? HtmlLocalise("PhoneUnknown", "(number not known)");
        TelephoneNumberLiteral.Text = string.Concat(": ", phoneNumber);

        var emailTemplateData = new EmailTemplateData
        {
          RecipientName = string.Concat(hiringManager.FirstName, " ", hiringManager.LastName),
          MatchingCandidates = string.Concat(Candidate.FirstName, " ", Candidate.LastName),
          EmployeeName = RecentPlacement.IsNull() ? string.Empty : string.Concat(RecentPlacement.FirstName, " ", RecentPlacement.LastName),
          SenderName = string.Concat(App.User.FirstName, " ", App.User.LastName)
        };
        var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.ContactEmployerAboutCandidate, emailTemplateData);
        EmailSubjectTextBox.Text = emailTemplate.Subject;
        EmailBodyTextBox.Text = emailTemplate.Body;

        EmailPanel.Visible = true;
        HiringManagersDropDownValidator.Visible = false;
        HiringManagersDropDownEmailValidator.Visible = false;
      }
      else
      {
        TelephoneNumberLiteral.Text = string.Empty;

        EmailPanel.Visible = false;
        HiringManagersDropDownValidator.Visible = true;
        HiringManagersDropDownEmailValidator.Visible = true;
      }
    }

    /// <summary>
    /// Sends the email
    /// </summary>
    private void SendEmail()
    {
      if (AttachResumeCheckBox.Checked)
      {
        var resume = ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(Candidate.Id.GetValueOrDefault(0), !AnonymiseResumeCheckBox.Checked);
        var resumeBytes = Encoding.UTF8.GetBytes(resume);
        var resumeName = AnonymiseResumeCheckBox.Checked 
          ? "Resume.html" 
          : HtmlLocalise("ResumeName.Text", "{0}{1}Resume.html", Candidate.FirstName, Candidate.LastName);

        ServiceClientLocator.EmployeeClient(App).EmailEmployeeWithAttachment(HiringManager.Id, EmailSubjectTextBox.Text.Trim(), EmailBodyTextBox.Text.Trim(), resumeBytes, resumeName);
      }
      else
      {
				ServiceClientLocator.EmployeeClient(App).EmailEmployee(HiringManager.Id, EmailSubjectTextBox.Text.Trim(), EmailBodyTextBox.Text.Trim(), senderAddress: ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.ContactEmployerAboutCandidate));
      }

			Confirmation.Show(CodeLocalise("EmailEmployeeConfirmation.Title", "Contact #BUSINESS#:LOWER"),
                      CodeLocalise("EmailEmployeeConfirmation.Body", "Thank you. We have sent an email on your behalf."),
                      CodeLocalise("Global.Ok.Text", "OK"));
    }
  }
}