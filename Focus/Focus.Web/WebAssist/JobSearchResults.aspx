﻿<%@ Page Title="Job search results" Language="C#" MasterPageFile="~/Site.Master"
	AutoEventWireup="True" CodeBehind="JobSearchResults.aspx.cs" Inherits="Focus.Web.WebAssist.JobSearchResults" %>
<%@ Import Namespace="Focus.Core.Models.Career" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistJobSeekersNavigation.ascx" tagname="AssistJobSeekersNavigation" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="SearchResults" Src="~/Code/Controls/User/JobPostingSearchResults.ascx" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
  <script type="text/javascript">
  	$(document).ready(function () {
  		Sys.WebForms.PageRequestManager.getInstance().add_endRequest(BindTooltips);
  	});
  </script>
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
     <div><uc:AssistJobSeekersNavigation ID="AssistJobSeekersNavigationMain" runat="server" /></div>
			<br />
      <div><uc:SearchResults ID="Results" runat="server" /></div>
</asp:Content>