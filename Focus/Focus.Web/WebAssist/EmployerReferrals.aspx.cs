﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Permissions;
using System.Web.UI.WebControls;

using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Focus.Web.Controllers.Code.Controls.User;
using Focus.Web.ViewModels;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployerAccountApprover)]
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployerAccountApprovalViewer)]
  public partial class EmployerReferrals : PageBase
  // TODO: complete controller for EmployerReferrals
  {

    private EmployerAccountReferralCriteria EmployerAccountReferralCriteria
    {
			get { return GetViewStateValue<EmployerAccountReferralCriteria>("ListApprovalRequests:EmployerAccountReferralCriteria"); }
      set { SetViewStateValue("ListApprovalRequests:EmployerAccountReferralCriteria", value); }
    }

    private int _referralCount;
	  private bool _reApplyFilter;

    private EmployerReferralsViewModel _model;

    protected EmployerReferralsController PageController { get { return PageControllerBaseProperty as EmployerReferralsController; } }

    /// <summary>
    /// Registers the page controller.
    /// </summary>
    /// <returns></returns>
    protected override IPageController RegisterPageController()
    {
      return new EmployerReferralsController(App);
    }

    protected EmployerReferralsViewModel Model
    {
      get { return _model ?? (_model = (GetViewStateValue("ListApprovalRequests:Model", new EmployerReferralsViewModel()))); }
      set { _model = value; }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      Localise();

      if (!IsPostBack)
      {
				// Re-set search criteria if returning from a page following a filtered result
				if (Request.QueryString["filtered"] != null)
				{
					_reApplyFilter = Convert.ToBoolean(Request.QueryString["filtered"]);
					EmployerAccountReferralCriteria = App.GetSessionValue("ListApprovalRequests:EmployerAccountReferralCriteria", new EmployerAccountReferralCriteria());

					// Set the Pager control to the previous Listview page and pagesize
					ReferralListPager.ReturnToPage(Convert.ToInt32(EmployerAccountReferralCriteria.PageIndex), Convert.ToInt32(EmployerAccountReferralCriteria.PageSize));
				}

        BindReferralList();
        BindPendingFilterDropDown();
	      ApprovalQueueFilter.Initialise(ApprovalQueueFilterType.Employer, _reApplyFilter);

			 SetReturnUrl();
      }
    }

    /// <summary>
    /// Gets the referrals.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="startRowIndex">Start index of the row.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <returns></returns>
    public List<EmployerAccountReferralViewDto> GetReferrals(EmployerAccountReferralCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
      var result = PageController.GetViewModel(criteria, orderBy, startRowIndex, maximumRows);

      var viewModelResult = result as ViewModelResult;

      if (viewModelResult != null)
        Model = viewModelResult.ViewModel as EmployerReferralsViewModel;
      else
        _referralCount = 0;

      if (Model != null)
      {
        _referralCount = Model.EmployerAccountReferrals.TotalCount;
        return Model.EmployerAccountReferrals;
      }

      return null;
    }

    /// <summary>
    /// Gets the referrals count.
    /// </summary>
    /// <returns></returns>
    public int GetReferralsCount()
    {
      return _referralCount;
    }

    /// <summary>
    /// Gets the referrals count.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public int GetReferralsCount(EmployerAccountReferralCriteria criteria)
    {
      return _referralCount;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
			Page.Title = CodeLocalise("Page.Title", " Approve #BUSINESS#:LOWER account requests");
      FilterButton.Text = CodeLocalise("FilterButton.Text", "Go");
    }

    /// <summary>
    /// Binds the referral list.
    /// </summary>
    private void BindReferralList()
    {
      ReferralList.DataBind();

      ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} requests found", ReferralListPager.TotalRowCount.ToString());
      ReferralListPager.Visible = ReferralList.Visible = (ReferralListPager.TotalRowCount > 0);

			if (ReferralListPager.TotalRowCount > 0)
			{
				((Literal)ReferralList.FindControl("BusinessUnitNameHeader")).Text = HtmlLocalise("EmployerName.ColumnTitle", "#BUSINESS# name");
				((Literal)ReferralList.FindControl("ContactNameHeader")).Text = HtmlLocalise("ContactName.ColumnTitle", "Contact name");
				((Literal)ReferralList.FindControl("AccountCreationDateHeader")).Text = HtmlLocalise("AccountCreationDate.ColumnTitle", "Account creation date");
				((Literal)ReferralList.FindControl("TimeInQueueHeader")).Text = HtmlLocalise("TimeInQueue.ColumnTitle", "Time in queue");

			  FormatSortingImages();
			}
    }

    /// <summary>
    /// Formats the sorting images
    /// </summary>
    private void FormatSortingImages()
    {
      var orderBy = "";
      if (EmployerAccountReferralCriteria.OrderBy.IsNotNullOrEmpty())
        orderBy = EmployerAccountReferralCriteria.OrderBy.ToLower();

      FormatSortButton("BusinessUnitNameAscButton", "businessunitname asc", orderBy);
      FormatSortButton("BusinessUnitNameDescButton", "businessunitname desc", orderBy);
      FormatSortButton("ContactNameAscButton", "employeefullname asc", orderBy);
      FormatSortButton("ContactNameDescButton", "employeefullname desc", orderBy);
      FormatSortButton("AccountCreationDateAscButton", "accountcreationdate asc", orderBy);
      FormatSortButton("AccountCreationDateDescButton", "accountcreationdate desc", orderBy);
			FormatSortButton("TimeInQueueAscButton", "awaitingapprovaldate desc", orderBy, true);
			FormatSortButton("TimeInQueueDescButton", "awaitingapprovaldate asc", orderBy, true);
    }

    /// <summary>
    /// Formats a sorting button
    /// </summary>
    /// <param name="buttonId">The id of the button</param>
    /// <param name="buttonSort">The "order by" for the button</param>
    /// <param name="currentSort">The current "order by" for the list</param>
		/// <param name="isReversed">Whether the sort order is actually being done in the opposite direction (sorting on time in queue actually sorts on approval date in db)</param>
		private void FormatSortButton(string buttonId, string buttonSort, string currentSort, bool isReversed = false)
    {
      var button = (ImageButton)ReferralList.FindControl(buttonId);
      button.Enabled = (currentSort != buttonSort);
			if (buttonSort.EndsWith(isReversed ? "desc" : "asc", true, CultureInfo.InvariantCulture))
        button.ImageUrl = button.Enabled ? UrlBuilder.ActivityOnSortAscImage() : UrlBuilder.ActivityListSortAscImage();
      else
        button.ImageUrl = button.Enabled ? UrlBuilder.ActivityOnSortDescImage() : UrlBuilder.ActivityListSortDescImage();
    }

    /// <summary>
    /// Unbinds the search.
    /// </summary>
    private void UnbindSearch()
    {
      var filterModel = ApprovalQueueFilter.Unbind();

      EmployerAccountReferralCriteria = new EmployerAccountReferralCriteria
        {
          ApprovalStatus = (ApprovalStatuses)Enum.Parse(typeof(ApprovalStatuses), PendingFilterDropDown.SelectedValue, true),
          EmployerType = filterModel.EmployerType,
          OfficeIds = filterModel.OfficeId.IsNotNull() ? new List<long?> { filterModel.OfficeId } : null
        };
    }

    /// <summary>
    /// Binds the pending filter drop down.
    /// </summary>
    private void BindPendingFilterDropDown()
    {
      PendingFilterDropDown.Items.Clear();

      PendingFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.WaitingApproval, "New requests"), ApprovalStatuses.WaitingApproval.ToString()));
      PendingFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.OnHold, "On Hold requests"), ApprovalStatuses.OnHold.ToString()));
      PendingFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.Rejected, "Denied requests"), ApprovalStatuses.Rejected.ToString()));

      PendingFilterDropDown.SelectedIndex = 0;

	    if (_reApplyFilter)
		    PendingFilterDropDown.SelectedValue = EmployerAccountReferralCriteria.ApprovalStatus.ToString();
    }

    /// <summary>
    /// Handles the Selecting event of the ReferralDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void ReferralDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      if (EmployerAccountReferralCriteria.IsNull())
      {
	      if (!_reApplyFilter)
	      {
		      var filterModel = ApprovalQueueFilter.Unbind();
		      EmployerAccountReferralCriteria = new EmployerAccountReferralCriteria
		      {
			      ApprovalStatus = ApprovalStatuses.WaitingApproval,
			      OfficeIds = filterModel.OfficeId.HasValue ? new List<long?> {filterModel.OfficeId} : null,
		      };
	      }
      }

      e.InputParameters["criteria"] = EmployerAccountReferralCriteria;
			App.SetSessionValue("ListApprovalRequests:EmployerAccountReferralCriteria", EmployerAccountReferralCriteria);
    }

    /// <summary>
    /// Handles the ItemDataBound event of the ReferralList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void ReferralList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var accountReferralView = (EmployerAccountReferralViewDto)e.Item.DataItem;

      var image = (Image)e.Item.FindControl("FlagImage");

      image.Visible = (((ApprovalStatuses)accountReferralView.EmployeeApprovalStatus == ApprovalStatuses.OnHold && accountReferralView.TypeOfApproval == (int)ApprovalType.HiringManager) ||
                       ((ApprovalStatuses)accountReferralView.EmployerApprovalStatus == ApprovalStatuses.OnHold && accountReferralView.TypeOfApproval == (int)ApprovalType.Employer) ||
                       ((ApprovalStatuses)accountReferralView.BusinessUnitApprovalStatus == ApprovalStatuses.OnHold && accountReferralView.TypeOfApproval == (int)ApprovalType.BusinessUnit));

	    var employerLink =  (HyperLink)e.Item.FindControl("EmployerNameLink");
	    employerLink.NavigateUrl = string.Format("{0}{1}",UrlBuilder.EmployerReferral(((EmployerAccountReferralViewDto)e.Item.DataItem).EmployerId), "?filtered=true");
	    employerLink.Visible = ((EmployerAccountReferralViewDto) e.Item.DataItem).TypeOfApproval == (int)ApprovalType.Employer;

			var businessUnitLink = (HyperLink)e.Item.FindControl("BusinessUnitNameLink");
	    businessUnitLink.NavigateUrl = string.Format("{0}{1}",UrlBuilder.BusinessUnitReferral(((EmployerAccountReferralViewDto) e.Item.DataItem).BusinessUnitId), "?filtered=true");
		  businessUnitLink.Visible = ((EmployerAccountReferralViewDto) e.Item.DataItem).TypeOfApproval == (int) ApprovalType.BusinessUnit;

			var hiringManagerLink = (HyperLink)e.Item.FindControl("HiringManagerLink");
	    hiringManagerLink.NavigateUrl = string.Format("{0}{1}",UrlBuilder.HiringManagerReferral(((EmployerAccountReferralViewDto) e.Item.DataItem).EmployeeId), "?filtered=true");
	    hiringManagerLink.Visible = ((EmployerAccountReferralViewDto)e.Item.DataItem).TypeOfApproval == (int) ApprovalType.HiringManager;
    }

    /// <summary>
    /// Handles the Sorting event of the ReferralList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ListViewSortEventArgs"/> instance containing the event data.</param>
    protected void ReferralList_Sorting(object sender, ListViewSortEventArgs e)
    {
      #region Job order list sorting

      EmployerAccountReferralCriteria.OrderBy = e.SortExpression;
      FormatSortingImages();

      #endregion
    }

    /// <summary>
    /// Handles the Clicked event of the FilterButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void FilterButton_Clicked(object sender, EventArgs e)
    {
      ReferralListPager.ReturnToFirstPage();
      UnbindSearch();
      BindReferralList();

			App.SetSessionValue("ListApprovalRequests:EmployerAccountReferralCriteria", EmployerAccountReferralCriteria);
    }
  }
}