﻿<%@ Page Title="Job posting" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True" CodeBehind="JobPosting.aspx.cs" Inherits="Focus.Web.WebAssist.JobPosting" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistJobSeekersNavigation.ascx" tagname="AssistJobSeekersNavigation" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobPostingDetails.ascx" tagPrefix="uc" tagName="JobPostingDetails" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
			    
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel ID="UpdatePanelJobPosting" runat="server" UpdateMode="Always">
		<ContentTemplate>
		    <uc:AssistJobSeekersNavigation ID="AssistJobSeekersNavigationMain" runat="server" />
				<div>
					<uc:JobPostingDetails ID="JobPostingDetails" runat="server" />
							
				</div>
		</ContentTemplate>
         
	</asp:UpdatePanel>

</asp:Content>
