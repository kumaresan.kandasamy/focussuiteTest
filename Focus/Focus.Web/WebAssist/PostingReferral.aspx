﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="PostingReferral.aspx.cs" Inherits="Focus.Web.WebAssist.PostingReferral" %>

<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ApprovePostingReferralModal.ascx" TagName="ApprovePostingReferralModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/DenyPostingReferralModal.ascx" TagName="DenyPostingReferralModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content id="HeaderContent" contentplaceholderid="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content id="MainContent" contentplaceholderid="MainContent" runat="server">
    <p class="pageNav">
     		<asp:LinkButton ID="ReturnLink" runat="server" OnClick="lnkReturn_Click" CausesValidation="False"></asp:LinkButton>
    </p>
    <h1>
        <asp:literal id="PostingReferralHeaderLiteral" runat="server" /><asp:literal id="PostingNameLiteral"
            runat="server" /></h1>
    <div style="width: 100%">
        <asp:Panel id="AccountRequestStatusHeaderPanel" runat="server" cssclass="singleAccordionTitle on">
            <table class="accordionTable" role="presentation">
                <tr>
                    <td>
                        <asp:image id="AccountRequestStatusHeaderImage" runat="server" AlternateText="." Width="22" Height="22"/>
                        &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("RequestStatus.Label", "Request status")%></a></span>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" id="AccountRequestStatusPanel">
            <br />
            <div style="float: left" id="StatusDiv">
                <asp:Placeholder id="StatusCellPlaceHolder" runat="server"><strong>
                    <%= HtmlLocalise("PostingApprovalStatus.Title", "Posting approval status")%>:</strong>
                    <asp:label id="AccountApprovalStatusLabel" runat="server"></asp:label><br /><br/>

                    <asp:PlaceHolder runat="server" ID="ReconsiderReasonPlaceholder">
                        <%=HtmlLocalise("ReconsiderReason.Text", "Reason: Posting was denied but hiring manager has requested posting be reconsidered")%><br/><br/>
                    </asp:PlaceHolder>
                    <asp:Placeholder id="AccountApprovalStatusDetailsPlaceHolder" runat="server">
                        <asp:label runat="server" id="AccountApprovalStatusDetails"></asp:label><br /><br/>
                    </asp:Placeholder>
                    <asp:PlaceHolder runat="server" ID="ReconsiderOnPlaceHolder">
                        <asp:label runat="server" id="ReconsiderOnDetailsLiteral"></asp:label><br /><br/>
                    </asp:PlaceHolder>
										
										<strong><%= HtmlLocalise("ReasonForApproval.Title", "Reason for approval")%>:</strong>
										<br />
										
										<asp:PlaceHolder runat="server" ID="ApprovalConditionsPlaceHolder">
											<ul>
													<asp:Datalist id="ApprovalConditionsDatalist" runat="server" autogeneratecolumns="false">
															<ItemTemplate>
																	<li><%# (string)Container.DataItem %></li>
															</ItemTemplate>
													</asp:Datalist>
											</ul>
											<br />
										</asp:PlaceHolder>

                    <asp:literal id="InappropriateWordsSummaryLiteral" runat="server" />
										<br />
                    <ul>
                        <asp:Datalist id="InappropriateWordsList" runat="server" autogeneratecolumns="false">
                            <ItemTemplate>
                                <li><%# (string)Container.DataItem %></li>
                            </ItemTemplate>
                        </asp:Datalist>
                    </ul>
                </asp:Placeholder>

                <asp:HiddenField runat="server" ID="CurrentApprovalStatus" ClientIDMode="Static"/>
                <br />
            </div>
            <div style="float: right">
                <asp:label runat="server" cssclass="error" id="JobWarningMessageLabel" visible="False">
                </asp:label>
                <asp:Panel runat="server" id="ApprovalButtonsCell">
                    <div style="width: 100%; text-align: right">
                        <asp:button id="EditJobButton" runat="server" cssclass="button1" onclick="EditJobButton_Clicked" />
                        <asp:button id="DenyButton" runat="server" cssclass="button4" onclick="DenyButton_Clicked" />
                        <asp:button id="ApproveButton" runat="server" cssclass="button2 right" onclick="ApproveButton_Clicked" />
                    </div>
                </asp:Panel>
                <br />
                <div id="SpecialRequirementsContainer" runat="server" class="subItemsPanel" visible="false">
                    <asp:literal id="SpecialConditionsLiteral" runat="server" />
                    <br />
                    <ul>
                        <asp:Datalist id="SpecialConditionsList" runat="server" autogeneratecolumns="false">
                            <itemtemplate>
              <li>
                <%# (string)Container.DataItem %></li>
            </itemtemplate>
                        </asp:Datalist>
                    </ul>
                </div>
                <br />
                <asp:Placeholder runat="server" id="EmailPlaceHolder">
                    <div style="width: 100%; text-align: left">
                        <asp:Literal runat="server" ID="EmailHeaderText"></asp:Literal><br />
                        <asp:textbox id="HoldEmailTextBox" runat="server" width="480px" clientidmode="Static"
                            autocompletetype="Disabled" textmode="MultiLine" rows="10" readonly="True" /><br />
                        <br />
                    </div>
                </asp:Placeholder>
            </div>
            <div style="clear: both">
            </div>
        </asp:Panel>
        <act:CollapsiblePanelExtender ID="AccountRequestStatusCollapsiblePanelExtender" runat="server"
            TargetControlID="AccountRequestStatusPanel" ExpandControlID="AccountRequestStatusHeaderPanel"
            CollapseControlID="AccountRequestStatusHeaderPanel" Collapsed="False" ImageControlID="AccountRequestStatusHeaderImage"
            SuppressPostBack="true" BehaviorID="AccountRequestStatusCollapsiblePanel" />
    </div>
    <div style="width: 100%">
        <asp:Panel id="RequestInformationHeaderPanel" runat="server" cssclass="singleAccordionTitle">
            <table class="accordionTable" role="presentation">
                <tr>
                    <td>
                        <asp:image id="RequestInformationHeaderImage" runat="server" AlternateText="." width="22" Height="22"/>
                        &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("RequestInformation.Label", "Request information")%></a></span>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel id="RequestInformationPanel" runat="server">
            <br />
            <asp:literal id="PostingLiteral" runat="server" />
            <div style="clear: both">
            </div>
        </asp:Panel>
        <act:CollapsiblePanelExtender ID="RequestInformationCollapsiblePanelExtender" runat="server"
            TargetControlID="RequestInformationPanel" ExpandControlID="RequestInformationHeaderPanel"
            CollapseControlID="RequestInformationHeaderPanel" Collapsed="true" ImageControlID="RequestInformationHeaderImage"
            SuppressPostBack="true" BehaviorID="RequestInformationCollapsiblePanel" />
    </div>
    <asp:updatepanel id="ConfirmationUpdatePanel" runat="server" updatemode="Conditional">
        <contenttemplate>
      <uc:ApprovePostingReferralModal ID="ApprovePostingReferralModal" runat="server" />
      <uc:DenyPostingReferralModal ID="DenyPostingReferral" runat="server" />
      <uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
    </contenttemplate>
    </asp:updatepanel>

</asp:Content>