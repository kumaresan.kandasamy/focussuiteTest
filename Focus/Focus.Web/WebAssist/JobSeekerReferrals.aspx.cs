﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Permissions;
using System.Web.UI.WebControls;

using Focus.Core;
using Focus.Common;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekerReferralApprover)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer)]
	public partial class JobSeekerReferrals : PageBase
	{
		private int _referralCount;
		private bool _reApplyFilter;

		private JobSeekerReferralCriteria JobSeekerReferralCriteria
		{
			get { return GetViewStateValue<JobSeekerReferralCriteria>("ListApprovalRequests:JobSeekerReferralCriteria"); }
			set { SetViewStateValue("ListApprovalRequests:JobSeekerReferralCriteria", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Localise();

			if (!IsPostBack)
			{
				// Re-set search criteria if returning from a page following a filtered result
				if (Request.QueryString["filtered"] != null)
				{
					_reApplyFilter = Convert.ToBoolean(Request.QueryString["filtered"]);
					JobSeekerReferralCriteria = App.GetSessionValue("ListApprovalRequests:JobSeekerReferralCriteria", new JobSeekerReferralCriteria());

					// Set the Pager control to the previous Listview page and pagesize
					ReferralListPager.ReturnToPage(Convert.ToInt32(JobSeekerReferralCriteria.PageIndex), Convert.ToInt32(JobSeekerReferralCriteria.PageSize));
				}

				BindReferralList();
				ApprovalQueueFilter.Initialise(ApprovalQueueFilterType.JobSeeker, _reApplyFilter);
				BindStatusDropDown();

				if (ReferralListPager.TotalRowCount > 0)
				{
					FormatSortingImages();
				}
			}

			SetReturnUrl();
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<JobSeekerReferralAllStatusViewDto> GetReferrals(JobSeekerReferralCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
				criteria = new JobSeekerReferralCriteria();

			if (criteria.OrderBy.IsNull())
				criteria.OrderBy = orderBy;
			criteria.PageIndex = pageIndex;
			criteria.PageSize = maximumRows;

			var referrals = ServiceClientLocator.CandidateClient(App).GetJobSeekerReferrals(criteria);

			_referralCount = referrals.TotalCount;

			return referrals;
		}

		/// <summary>
		/// Gets the referrals count.
		/// </summary>
		/// <returns></returns>
		public int GetReferralsCount()
		{
			return _referralCount;
		}

		/// <summary>
		/// Gets the referrals count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetReferralsCount(JobSeekerReferralCriteria criteria)
		{
			return _referralCount;
		}

		/// <summary>
		/// Handles the OnItemDataBound event of the ReferralList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ReferralList_OnItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var candidate = (JobSeekerReferralAllStatusViewDto)e.Item.DataItem;

			var scoreImage = (Image)e.Item.FindControl("SearchResultScoreImage");
			BindScore(scoreImage, candidate.ApplicationScore);

      var postingLocation = (Label)e.Item.FindControl("PostingLocationLabel");
      // postingLocation.Text = string.Join(",", new[] { candidate.Town, candidate.State });
      postingLocation.Text = candidate.PostingLocations;
		  
			var location = (Label)e.Item.FindControl("JobSeekerLocationLabel");
		  location.Text = candidate.JobSeekerAddress;

			var veteranImage = (Image)e.Item.FindControl("VeteranImage");
			if (Convert.ToBoolean(candidate.IsVeteran)) BindVeteranImage(veteranImage);

			//Render image for HSA or HSB postings
			var foreignLabourCertificationImage = (Image)e.Item.FindControl("ForeignLabourCertificationImage");
			if (Convert.ToBoolean(candidate.ForeignLabourCertificationH2A)) BindH2AImage(foreignLabourCertificationImage);
			else if (Convert.ToBoolean(candidate.ForeignLabourCertificationH2B)) BindH2BImage(foreignLabourCertificationImage);

		}

		/// <summary>
		/// Handles the Selecting event of the ReferralList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void ReferralList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (JobSeekerReferralCriteria.IsNull())
			{
				if (!_reApplyFilter)
				{
					var filterModel = ApprovalQueueFilter.Unbind();
					JobSeekerReferralCriteria = new JobSeekerReferralCriteria
					{
						ApprovalStatus = ApprovalStatuses.WaitingApproval,
						OfficeIds = filterModel.OfficeId.HasValue ? new List<long?> { filterModel.OfficeId } : null,
					};
				}
			}
			e.InputParameters["criteria"] = JobSeekerReferralCriteria;
			App.SetSessionValue("ListApprovalRequests:JobSeekerReferralCriteria", JobSeekerReferralCriteria);
		}

		/// <summary>
		/// Handles the Sorting event of the ReferralList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void ReferralList_Sorting(object sender, ListViewSortEventArgs e)
		{
			#region Job order list sorting

			JobSeekerReferralCriteria.OrderBy = e.SortExpression;
			FormatSortingImages();

			#endregion
		}

		/// <summary>
		/// Handles the LayoutCreated event of the ReferralList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void ReferralList_LayoutCreated(object sender, EventArgs e)
		{
			((Literal)ReferralList.FindControl("StarMatch")).Text = CodeLocalise("StarMatch.ColumnTitle", "Star match");
			((Literal)ReferralList.FindControl("JobTitleCompanyHeader")).Text = CodeLocalise("JobTitleCompanyHeader.ColumnTitle", "#POSTINGTYPE# title/#BUSINESS#");
      ((Literal)ReferralList.FindControl("PostingLocationHeader")).Text = CodeLocalise("PostingLocationHeader.ColumnTitle", "Posting Location");
      ((Literal)ReferralList.FindControl("JobSeekerNameHeader")).Text = CodeLocalise("JobSeekerNameHeader.ColumnTitle", "#CANDIDATETYPE# name");
      ((Literal)ReferralList.FindControl("JobSeekerLocationHeader")).Text = CodeLocalise("JobSeekerLocationHeader.ColumnTitle", "Jobseeker location");
			((Literal)ReferralList.FindControl("RequestDateHeader")).Text = CodeLocalise("RequestDateHeader.ColumnTitle", "Request date");
			((Literal)ReferralList.FindControl("TimeInQueueHeader")).Text = CodeLocalise("TimeInQueueHeader.ColumnTitle", "Time in queue");
		}

		#endregion

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			Page.Title = CodeLocalise("Page.Title", "Approval Queues");
			FilterButton.Text = CodeLocalise("FilterButton.Text", "Go");
		}

		/// <summary>
		/// Binds the status drop down.
		/// </summary>
		private void BindStatusDropDown()
		{
			StatusDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.WaitingApproval, "New requests"), ApprovalStatuses.WaitingApproval.ToString()));
			StatusDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.Rejected, "Denied requests"), ApprovalStatuses.Rejected.ToString()));
			StatusDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.OnHold, "On Hold requests"), ApprovalStatuses.OnHold.ToString()));
			StatusDropDown.Items.Add(new ListItem(CodeLocalise(ApprovalStatuses.Reconsider, "Reconsider requests"), ApprovalStatuses.Reconsider.ToString()));

			StatusDropDown.SelectedIndex = 0;
			if (_reApplyFilter && JobSeekerReferralCriteria.ApprovalStatus.IsNotNull())
				StatusDropDown.SelectedValue = JobSeekerReferralCriteria.ApprovalStatus.ToString();
		}

		/// <summary>
		/// Binds the veteran image.
		/// </summary>
		/// <param name="veteranImage">The veteran image.</param>
		private void BindVeteranImage(Image veteranImage)
		{
			veteranImage.ToolTip = veteranImage.AlternateText = CodeLocalise("VeteranImage.ToolTip", "Veteran");
			veteranImage.ImageUrl = UrlBuilder.VeteranImage();
			veteranImage.Visible = true;
		}

		/// <summary>
		/// Binds the foreign labour certification image.
		/// </summary>
		/// <param name="image">The veteran image.</param>
		/// <exception cref="NotImplementedException"></exception>
		private void BindH2AImage(Image image)
		{
			image.ToolTip = image.AlternateText = CodeLocalise("ForeignLabourCertificationImage.ToolTip", "Foreign Labour Certification");
			image.ImageUrl = UrlBuilder.H2AImage();
			image.Visible = true;

		}

		/// <summary>
		/// Binds the foreign labour certification image.
		/// </summary>
		/// <param name="image">The veteran image.</param>
		/// <exception cref="NotImplementedException"></exception>
		private void BindH2BImage(Image image)
		{
			image.ToolTip = image.AlternateText = CodeLocalise("ForeignLabourCertificationImage.ToolTip", "Foreign Labour Certification");
			image.ImageUrl = UrlBuilder.H2BImage();
			image.Visible = true;

		}

		/// <summary>
		/// Binds the referral list.
		/// </summary>
		private void BindReferralList()
		{
			ReferralList.DataBind();

			ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} referrals found", ReferralListPager.TotalRowCount.ToString(CultureInfo.CurrentCulture));

			ReferralListPager.Visible = (ReferralListPager.TotalRowCount > 0);
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = App.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString(CultureInfo.InvariantCulture);

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

		/// <summary>
		/// Unbinds the search.
		/// </summary>
		private void UnbindSearch()
		{
			var filterModel = ApprovalQueueFilter.Unbind();

			JobSeekerReferralCriteria = new JobSeekerReferralCriteria
			{
				ApprovalStatus = (ApprovalStatuses)Enum.Parse(typeof(ApprovalStatuses), StatusDropDown.SelectedValue, true),
				JobSeekerType = filterModel.JobSeekerType.IsNotNull() ? (VeteranFilterTypes?)Enum.Parse(typeof(VeteranFilterTypes), filterModel.JobSeekerType.ToString(), true) : null,
				OfficeIds = filterModel.OfficeId.IsNotNull() ? new List<long?> { filterModel.OfficeId } : null,
				StaffMemberId = filterModel.StaffMemberId
			};
		}

		/// <summary>
		/// Formats the sorting images.
		/// </summary>
		private void FormatSortingImages()
		{
			#region Format images

			// Reset image URLs
			((ImageButton)ReferralList.FindControl("StarMatchSortAscButton")).ImageUrl =
				((ImageButton)ReferralList.FindControl("JobTitleSortAscButton")).ImageUrl =
				((ImageButton)ReferralList.FindControl("JobSeekerNameSortAscButton")).ImageUrl =
        ((ImageButton)ReferralList.FindControl("PostingLocationSortAscButton")).ImageUrl =
        ((ImageButton)ReferralList.FindControl("JobSeekerLocationSortAscButton")).ImageUrl =
				((ImageButton)ReferralList.FindControl("RequestDateSortAscButton")).ImageUrl =
				((ImageButton)ReferralList.FindControl("TimeInQueueSortAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();

			((ImageButton)ReferralList.FindControl("StarMatchSortDescButton")).ImageUrl =
				((ImageButton)ReferralList.FindControl("JobTitleSortDescButton")).ImageUrl =
        ((ImageButton)ReferralList.FindControl("PostingLocationSortDescButton")).ImageUrl =
        ((ImageButton)ReferralList.FindControl("JobSeekerNameSortDescButton")).ImageUrl =
        ((ImageButton)ReferralList.FindControl("JobSeekerLocationSortDescButton")).ImageUrl =
        ((ImageButton)ReferralList.FindControl("RequestDateSortDescButton")).ImageUrl =
        ((ImageButton)ReferralList.FindControl("TimeInQueueSortDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			// Reenable the buttons
			((ImageButton)ReferralList.FindControl("StarMatchSortAscButton")).Enabled =
				((ImageButton)ReferralList.FindControl("JobTitleSortAscButton")).Enabled =
        ((ImageButton)ReferralList.FindControl("PostingLocationSortAscButton")).Enabled =
				((ImageButton)ReferralList.FindControl("JobSeekerNameSortAscButton")).Enabled =
        ((ImageButton)ReferralList.FindControl("JobSeekerLocationSortAscButton")).Enabled =
				((ImageButton)ReferralList.FindControl("RequestDateSortAscButton")).Enabled =
				((ImageButton)ReferralList.FindControl("TimeInQueueSortAscButton")).Enabled =
				((ImageButton)ReferralList.FindControl("StarMatchSortDescButton")).Enabled =
				((ImageButton)ReferralList.FindControl("JobTitleSortDescButton")).Enabled =
        ((ImageButton)ReferralList.FindControl("PostingLocationSortDescButton")).Enabled =
        ((ImageButton)ReferralList.FindControl("JobSeekerNameSortDescButton")).Enabled =
        ((ImageButton)ReferralList.FindControl("JobSeekerLocationSortDescButton")).Enabled =
        ((ImageButton)ReferralList.FindControl("RequestDateSortDescButton")).Enabled =
        ((ImageButton)ReferralList.FindControl("TimeInQueueSortDescButton")).Enabled = true;

			if (JobSeekerReferralCriteria.OrderBy.IsNotNullOrEmpty())
			{
				switch (JobSeekerReferralCriteria.OrderBy.ToLower())
				{
					case "applicationscore asc":
						((ImageButton)ReferralList.FindControl("StarMatchSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)ReferralList.FindControl("StarMatchSortAscButton")).Enabled = false;
						break;
					case "applicationscore desc":
						((ImageButton)ReferralList.FindControl("StarMatchSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
						((ImageButton)ReferralList.FindControl("StarMatchSortDescButton")).Enabled = false;
						break;
					case "jobtitle asc, employername asc":
						((ImageButton)ReferralList.FindControl("JobTitleSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)ReferralList.FindControl("JobTitleSortAscButton")).Enabled = false;
						break;
					case "jobtitle desc, employername desc":
						((ImageButton)ReferralList.FindControl("JobTitleSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
						((ImageButton)ReferralList.FindControl("JobTitleSortDescButton")).Enabled = false;
						break;
          case "joblocationtown asc, joblocationstate asc":
            ((ImageButton)ReferralList.FindControl("PostingLocationSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
            ((ImageButton)ReferralList.FindControl("PostingLocationSortAscButton")).Enabled = false;
            break;
          case "joblocationtown desc, joblocationstate desc":
            ((ImageButton)ReferralList.FindControl("PostingLocationSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
            ((ImageButton)ReferralList.FindControl("PostingLocationSortDescButton")).Enabled = false;
            break;
					case "name asc":
						((ImageButton)ReferralList.FindControl("JobSeekerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)ReferralList.FindControl("JobSeekerNameSortAscButton")).Enabled = false;
						break;
					case "name desc":
            ((ImageButton)ReferralList.FindControl("JobSeekerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
            ((ImageButton)ReferralList.FindControl("JobSeekerNameSortDescButton")).Enabled = false;
						break;
          //case "town asc, state asc":
          //  ((ImageButton)ReferralList.FindControl("LocationSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
          //  ((ImageButton)ReferralList.FindControl("LocationSortAscButton")).Enabled = false;
          //  break;
          //case "town desc, state desc":
          //  ((ImageButton)ReferralList.FindControl("LocationDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
          //  ((ImageButton)ReferralList.FindControl("LocationDescButton")).Enabled = false;
          //  break;
          case "jobseekertown asc, jobseekerstate asc":
            ((ImageButton)ReferralList.FindControl("JobSeekerLocationSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
            ((ImageButton)ReferralList.FindControl("JobSeekerLocationSortAscButton")).Enabled = false;
				    break;
          case "jobseekertown desc, jobseekerstate desc":
            ((ImageButton)ReferralList.FindControl("JobSeekerLocationSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
            ((ImageButton)ReferralList.FindControl("JobSeekerLocationSortDescButton")).Enabled = false;
				    break;
					case "referraldate asc":
						((ImageButton)ReferralList.FindControl("RequestDateSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)ReferralList.FindControl("RequestDateSortAscButton")).Enabled = false;
						break;
					case "referraldate desc":
            ((ImageButton)ReferralList.FindControl("RequestDateSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
            ((ImageButton)ReferralList.FindControl("RequestDateSortDescButton")).Enabled = false;
						break;
					case "timeinqueue asc":
						((ImageButton)ReferralList.FindControl("TimeInQueueSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
						((ImageButton)ReferralList.FindControl("TimeInQueueSortAscButton")).Enabled = false;
						break;
					case "timeinqueue desc":
            ((ImageButton)ReferralList.FindControl("TimeInQueueSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
            ((ImageButton)ReferralList.FindControl("TimeInQueueSortDescButton")).Enabled = false;
						break;
				}
			}

			#endregion
		}

		/// <summary>
		/// Handles the Clicked event of the FilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void FilterButton_Clicked(object sender, EventArgs e)
		{
			if (!_reApplyFilter)
			ReferralListPager.ReturnToFirstPage();

			UnbindSearch();
			BindReferralList();

			if (ReferralListPager.TotalRowCount > 0) FormatSortingImages();

			App.SetSessionValue("ListApprovalRequests:JobSeekerReferralCriteria", JobSeekerReferralCriteria);
		}
	}
}