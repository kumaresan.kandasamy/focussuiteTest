﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="SavedMessages.aspx.cs" Inherits="Focus.Web.WebAssist.SavedMessages" %>

<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register Src="Controls/AccountSettingsLinks.ascx" TagPrefix="uc" TagName="SettingsLinks" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:SettingsLinks ID="SettingLinksMain" runat="server" />
    <h1>
        <%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>
    <table style="width:100%;" role="presentation">
        <tr>
            <td style="width:100%; vertical-align:top;">
                <asp:Button ID="SaveButton" runat="server" CssClass="button2 right" OnClick="SaveButton_Clicked"
                    ValidationGroup="SaveMessageTexts" />
                <p>
                    <b>
                        <%= HtmlLocalise("SavedMessages.Label", "Saved messages")%></b></p>
            </td>
            <td style="vertical-align:top;">
                <asp:Button ID="DeleteButton" runat="server" CssClass="button2 right" OnClick="DeleteButton_Clicked"
                    ValidationGroup="DeleteMessageTexts" />
            </td>
        </tr>
    </table>
    <table role="presentation">
        <tr>
            <td>
                <%=HtmlLabel("MessageAudience.Label", "Show saved messages for") %>
            </td>
            <td>
                <asp:RadioButtonList ID="MessageAudienceRadioButtonList" RepeatDirection="Horizontal"
                    runat="server" ClientIDMode="Static" OnSelectedIndexChanged="MessageAudienceRadioButtonList_SelectedIndexChanged"
                    AutoPostBack="true" role="presentation"/>
            </td>
        </tr>
    </table>
    <p>
	    <focus:LocalisedLabel runat="server" ID="SavedMessagesDropDownLabel" AssociatedControlID="SavedMessagesDropDown" LocalisationKey="SavedMessages" DefaultText="Saved messages" CssClass="sr-only"/>
        <asp:DropDownList ID="SavedMessagesDropDown" runat="server" ClientIDMode="Static"
            AutoPostBack="true" OnSelectedIndexChanged="SavedMessagesDropDown_SelectedIndexChanged"/>
        <asp:RequiredFieldValidator ID="SavedMessageRequired" runat="server" ControlToValidate="SavedMessagesDropDown"
            ValidationGroup="SaveMessageTexts" CssClass="error" />
        <asp:RequiredFieldValidator ID="SavedMessageRequiredDelete" runat="server" ControlToValidate="SavedMessagesDropDown"
            ValidationGroup="DeleteMessageTexts" CssClass="error" />
    </p>
    <p>
	    <focus:LocalisedLabel runat="server" ID="MessageTextboxLabel" AssociatedControlID="MessageTextbox" LocalisationKey="Message" DefaultText="Message" CssClass="sr-only"/>
        <asp:TextBox ID="MessageTextbox" runat="server" Width="500" MaxLength="1000" ClientIDMode="Static"
            TextMode="MultiLine" Rows="5" />
        <asp:RequiredFieldValidator ID="MessageRequired" runat="server" ControlToValidate="MessageTextbox"
            ValidationGroup="SaveMessageTexts" CssClass="error" />
    </p>

    <%-- Confirmation Modal --%>
    <uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
    <uc:ConfirmationModal ID="DeleteConfirmationModal" runat="server" OnOkCommand="DeleteConfirmationModal_OkCommand" />
</asp:Content>
