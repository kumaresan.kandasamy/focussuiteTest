﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="JobOrderDashboard.aspx.cs" Inherits="Focus.Web.WebAssist.JobOrderDashboard" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="Focus.Core.Models" %>
<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/AssistEmployersNavigation.ascx" TagName="AssistEmployersNavigation" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register Src="~/Code/Controls/User/EmailEmployeeModal.ascx" TagName="EmailEmployeeModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagPrefix="uc" TagName="ConfirmationModal" %>
<%@ Register Src="~/Code/Controls/User/ErrorModal.ascx" TagPrefix="uc" TagName="ErrorModal" %>
<%@ Register Src="~/WebAssist/Controls/FindHiringManager.ascx" TagName="FindHiringManager" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/FindEmployer.ascx" TagName="FindEmployer" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/NotesAndRemindersModal.ascx" TagName="NotesAndRemindersModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/OfficeFilter.ascx" TagName="OfficeFilter" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/JobIssueResolutionModal.ascx" TagName="JobIssueResolutionModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ActivityReportCriteriaModal.ascx" TagName="EmployerActivityReportCriteriaModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/RequestFollowUpModal.ascx" TagName="RequestFollowUp" TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        
    </style>
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width: 100%;" role="presentation">
        <tr>
            <td style="vertical-align: top;">
                <uc:AssistEmployersNavigation ID="AssistEmployersNavigationMain" runat="server" />
            </td>
            <td style="vertical-align: top; text-align: right;">
	            <asp:Button ID="ActivityReportButton" runat="server" class="button1" OnClick="RunActivityReportButton_Clicked" CausesValidation="false"/>
                <asp:Literal ID="LocaliseCreateNewEmployerAndJobOrderButton" runat="server" />
                <asp:Button ID="CreateNewEmployerAndJobOrderButton" runat="server" class="button1" OnClick="CreateNewEmployerAndJobOrderButton_Clicked" CausesValidation="false" />
            </td>
        </tr>
    </table>
    <h1>
        <%= HtmlLocalise("Page.Title", "#EMPLOYMENTTYPE# dashboard")%></h1>
    <br />
    <table style="width: 100%;" data-panel="jobordersearch" role="presentation">
        <tr>
            <td colspan="2">
                <table style="width: 100%; border-collapse: collapse;" role="presentation">
                    <tr style="vertical-align: top">
                        <td style="height: 30px;" runat="server" id="JobOrderShowCell">
                            <%= HtmlLabel(JobOrdersFilterDropDown,"JobOrdersShow.Label", "Show")%>
                        </td>
                        <td style="width: 323px">
                            <asp:RequiredFieldValidator ID="JobOrdersDropDownRequired" runat="server" ValidationGroup="Action" ControlToValidate="JobOrdersFilterDropDown" CssClass="error" />
                            <asp:DropDownList ID="JobOrdersFilterDropDown" Width="300" runat="server" CausesValidation="false" />
                            &nbsp;
                            <asp:RequiredFieldValidator ID="JobOrdersFilterSubMenuDropDownRequired" runat="server" ValidationGroup="Action" ControlToValidate="JobOrdersFilterSubMenuDropDown" CssClass="error" />
														<focus:LocalisedLabel runat="server" ID="JobOrdersFilterSubMenuDropDownLabel" AssociatedControlID="JobOrdersFilterSubMenuDropDown" LocalisationKey="JobOrderSubMenu" DefaultText="Job order sub menu" CssClass="sr-only"/>
                            <asp:DropDownList ID="JobOrdersFilterSubMenuDropDown" runat="server" CausesValidation="false"/>
                        </td>
                        <td style="width: 94px;">
                            <%= HtmlLabel(JobOrderIssuesDropDown,"JobOrdersIssues.Label", "Issues")%>
                        </td>
                        <td style="width: 290px">
                            <asp:DropDownList runat="server" ID="JobOrderIssuesDropDown" Width="270" />
                        </td>
                        <td style="width: 60px;" runat="server" id="JobOrderStatusCell">
                            <%= HtmlLabel(JobOrdersStatusDropDown,"JobOrdersStatus.Label", "Status")%>
                        </td>
                        <td style="width: 190px">
                            <asp:RequiredFieldValidator ID="JobOrdersStatusRequired" runat="server" ValidationGroup="Action" ControlToValidate="JobOrdersStatusDropDown" CssClass="error" />
                            <asp:DropDownList ID="JobOrdersStatusDropDown" runat="server" CausesValidation="false" Width="170px" />
                        </td>
                        <td style="text-align: right">
                            <asp:Button ID="JobFilterButton" runat="server" class="button3" OnClick="JobFilterButton_Clicked" ValidationGroup="FindJobOrder" />
                            <asp:Button ID="EducationFilterClearButton" runat="server" class="button3" OnClick="FilterClearButton_Clicked" ValidationGroup="FindJobOrder" />
                        </td>
                    </tr>
                    <tr runat="server" id="OfficeFilterRow">
                        <td colspan="6">
                            <uc:OfficeFilter ID="OfficeFilter" runat="server" />
                        </td>
                        <td style="text-align: right; vertical-align: top;">
                            <asp:Button ID="FilterClearButton" runat="server" class="button3" OnClick="FilterClearButton_Clicked" ValidationGroup="FindJobOrder" />
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="SearchPanelTop">
                    <div class="singleAccordionContentWrapper">
                        <table style="width: 100%;" role="presentation">
                            <tr>
                                <td colspan="3">
                                    <asp:Panel ID="JobOrderSearchHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                                        <table class="accordionTable" role="presentation">
                                            <tr>
                                                <td>
                                                    <asp:Image ID="JobOrderSearchHeaderImage" runat="server" alt="." />
                                                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("FindAJobOrder.Label", "Find a #EMPLOYMENTTYPE#:LOWER")%></a></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="JobOrderSearchPanel" runat="server" CssClass="singleAccordionContent">
                                        <table style="width: 100%;" role="presentation">
                                            <tr>
                                                <td style="width: 850px;">
                                                    <table role="presentation">
                                                        <tr>
                                                            <td style="width: 60px;">
                                                                <%= HtmlLabel(JobIDTextBox,"JobID.Text", "#POSTINGTYPE# ID")%>
                                                            </td>
                                                            <td style="width: 180px;">
                                                                <asp:TextBox runat="server" ID="JobIDTextBox" ClientIDMode="Static" Width="180" MaxLength="19" />
                                                            </td>
                                                            <td style="width: 0;">
                                                            </td>
                                                            <td style="width: 150px;">
                                                                <%= HtmlLabel(EmployerNameTextBox,"EmployerName.Text", "#BUSINESS# name")%>
                                                            </td>
                                                            <td style="width: 108px;">
                                                                <asp:TextBox runat="server" ID="EmployerNameTextBox" Width="180" />
                                                            </td>
                                                            <td style="width: 0;">
                                                            </td>
                                                            <td style="width: 150px;">
                                                                <%= HtmlLabel(HiringManagerFirstNameTextBox,"Global.JobOrderHiringManagerFirstName", "Hiring mgr. first name")%>
                                                            </td>
                                                            <td style="width: 180px;">
                                                                <asp:TextBox runat="server" ID="HiringManagerFirstNameTextBox" Width="180" />
                                                            </td>
                                                            <td style="width: 100px;">
                                                                <%= HtmlLabel("CreationDate.Label", "Creation date") %>&nbsp;
                                                            </td>
                                                            <td style="width: 40px;">
                                                                <i>
                                                                    <%= HtmlLabel("CreationDateFrom.Label", "from") %></i>&nbsp;
                                                            </td>
                                                            <td style="width: 100px;" class="localNavTab">
	                                                            <focus:LocalisedLabel runat="server" ID="FromMonthDropDownListLabel" AssociatedControlID="FromMonthDropDownList" LocalisationKey="MonthFrom" DefaultText="Month from" CssClass="sr-only"/>
                                                                <asp:DropDownList ID="FromMonthDropDownList" runat="server" />
                                                            </td>
                                                            <td style="width: 70px;" class="localNavTab">
	                                                            <focus:LocalisedLabel runat="server" ID="FromYearDropDownListLabel" AssociatedControlID="FromYearDropDownList" LocalisationKey="YearFrom" DefaultText="Year from" CssClass="sr-only"/>
                                                                <asp:DropDownList ID="FromYearDropDownList" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <asp:RegularExpressionValidator ID="JobOrderIDValidator" runat="server" ControlToValidate="JobIDTextbox"
                                                                    CssClass="error" ValidationExpression="\d{1,19}" ValidationGroup="FindJobOrder" Display="Dynamic"></asp:RegularExpressionValidator>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <%= HtmlLabel(JobTitleTextBox,"JobTitle.Text", "#POSTINGTYPE# title")%>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="JobTitleTextBox" ClientIDMode="Static" Width="180" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <%= HtmlLabel(HiringManagerUsernameTextBox,"Global.JobOrderHiringManagerUsername", "Hiring mgr. username")%>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="HiringManagerUsernameTextBox" ClientIDMode="Static" Width="180" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                                <%= HtmlLabel(HiringManagerLastNameTextBox,"Global.JobOrderHiringManagerLastName", "Hiring mgr. last name")%>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox runat="server" ID="HiringManagerLastNameTextBox" ClientIDMode="Static" Width="180" />
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td>
                                                                <i>
                                                                    <%= HtmlLabel("CreationDateTo.Label", "to") %></i>&nbsp;
                                                            </td>
                                                            <td>
	                                                            <focus:LocalisedLabel runat="server" ID="ToMonthDropDownListLabel" AssociatedControlID="ToMonthDropDownList" LocalisationKey="MonthTo" DefaultText="Month to" CssClass="sr-only"/>
                                                                <asp:DropDownList ID="ToMonthDropDownList" runat="server" />
                                                            </td>
                                                            <td>
																																<focus:LocalisedLabel runat="server" ID="ToYearDropDownListLabel" AssociatedControlID="ToYearDropDownList" LocalisationKey="YearTo" DefaultText="YearTo" CssClass="sr-only"/>
                                                                <asp:DropDownList ID="ToYearDropDownList" runat="server" />
                                                            </td>
                                                            <td colspan="2">
                                                                <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="Selected dates are invalid.  Please try again." ClientValidationFunction="ToFromDateCheck" 
																																											Display="Dynamic" CssClass="error" ValidationGroup="FindJobOrder">Selected dates are invalid.  Please try again.</asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <act:CollapsiblePanelExtender ID="JobOrderSearchPanelExtender" runat="server" TargetControlID="JobOrderSearchPanel" ExpandControlID="JobOrderSearchHeaderPanel" 
																											CollapseControlID="JobOrderSearchHeaderPanel" Collapsed="true" ImageControlID="JobOrderSearchHeaderImage" SuppressPostBack="true" />
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table id="JobOrderHeaderTable" runat="server" visible="True" style="width: 100%;" role="presentation">
                    <tr>
                        <td style="width: 40px;" runat="server" id="ActionLabelCell">
                            <%= HtmlLabel(JobOrdersActionDropDown,"WebAssist.JobOrderDashboard.Action.Label", "Action")%>
                        </td>
                        <td style="white-space: nowrap;">
                            &nbsp;
                            <asp:DropDownList runat="server" ID="JobOrdersActionDropDown" Width="350" Visible="true" ClientIDMode="AutoID" />
                            &nbsp;
                            <asp:Button ID="JobOrdersActionButton" runat="server" class="button3" OnClick="JobOrdersActionButton_Clicked"
                                ValidationGroup="JobOrdersAction" />
                        </td>
                        <td style="white-space: nowrap; text-align: right; width: 100%;">
                            <uc:Pager ID="SearchResultListPager" runat="server" PagedControlId="JobOrderList" PageSize="10" DisplayRecordCount="True"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40px;">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                            <asp:RequiredFieldValidator ID="JobOrdersActionDropDownRequired" runat="server" ValidationGroup="JobOrdersAction" ControlToValidate="JobOrdersActionDropDown" CssClass="error" Display="Dynamic" />
                            <asp:CustomValidator ID="JobOrdersActionDropDownValidator" runat="server" CssClass="error" ControlToValidate="JobOrdersActionDropDown"
																									SetFocusOnError="True" ClientValidationFunction="JobOrders_ActionDropDown_Validate" ValidationGroup="JobOrdersAction"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr runat="server" id="SubActionRow" Visible="False">
            <td colspan="2">
                <table id="SubActionTable" runat="server" visible="True" style="width: 100%;" role="presentation">
                    <tr>
                        <td style="width: 40px;">
                        </td>
                        <td>
                            &nbsp;
                            <asp:DropDownList runat="server" ID="SubActionDropDown" Width="350" ClientIDMode="Static" />
                            &nbsp;
                            <asp:Button ID="SubActionButton" ClientIDMode="Static" runat="server" class="button3" OnClick="SubActionButton_Clicked" />
                            <asp:RequiredFieldValidator ID="SubActionDropDownRequired" runat="server" ValidationGroup="SubAction" ControlToValidate="SubActionDropDown" CssClass="error" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 40px;">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                            <asp:CustomValidator ID="SubActionButtonValidator" runat="server" CssClass="error" ControlToValidate="SubActionDropDown" SetFocusOnError="True" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ListView ID="JobOrderList" runat="server" OnItemDataBound="ItemsList_DataBound" ItemPlaceholderID="SearchResultPlaceHolder" DataSourceID="JobsDataSource" 
															DataKeyNames="Id,JobStatus,EmployerId,JobTitle,ClosingOn,EmployeeId,ClosedOn" OnSorting="JobOrderList_Sorting" OnLayoutCreated="JobOrdersList_LayoutCreated">
                    <LayoutTemplate>
                        <table style="width: 100%;" class="table" role="presentation">
                            <tr>
                                <td class="tableHead">
                                    <table role="presentation">
                                        <tr>
                                            <td rowspan="2" style="height: 16px;">
                                                <asp:CheckBox ID="SelectorAllCheckBoxes" ClientIDMode="AutoID" runat="server" CausesValidation="False" OnClick="SelectDeselectAllCheckboxes()" AutoPostBack="true" />
                                            </td>
                                            <td style="height: 8px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 8px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tableHead">
                                    <table role="presentation">
                                        <tr>
                                            <td rowspan="2" style="height: 16px;">
                                                <asp:Literal runat="server" ID="JobTitleHeader" />
                                            </td>
                                            <td style="height: 8px;white-space:nowrap">
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="JobTitleSortAscButton"
                                                    runat="server" CommandName="Sort" CommandArgument="jobtitle asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>" ID="JobTitleSortDescButton"
                                                    runat="server" CommandName="Sort" CommandArgument="jobtitle desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tableHead">
                                    <table role="presentation">
                                        <tr>
                                            <td rowspan="2" style="height: 16px;">
                                                <asp:Literal runat="server" ID="JobIdHeader" />
                                            </td>
                                            <td style="height: 8px;white-space:nowrap">
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="JobIdSortAscButton"
                                                    runat="server" CommandName="Sort" CommandArgument="id asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>" ID="JobIdSortDescButton"
                                                    runat="server" CommandName="Sort" CommandArgument="id desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tableHead">
                                    <table role="presentation">
                                        <tr>
                                            <td rowspan="2" style="height: 16px;">
                                                <asp:Literal runat="server" ID="EmployerNameHeader" />
                                            </td>
                                            <td style="height: 8px;white-space:nowrap">
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="EmployerNameSortAscButton"
                                                    runat="server" CommandName="Sort" CommandArgument="businessunitname asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>" ID="EmployerNameSortDescButton"
                                                    runat="server" CommandName="Sort" CommandArgument="businessunitname desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tableHead">
                                    <table role="presentation">
                                        <tr>
                                            <td rowspan="2" style="height:16px;">
                                                <asp:Literal runat="server" ID="DateHeader" />
                                            </td>
                                            <td style="height:8px;white-space:nowrap">
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="DateSortAscButton"
                                                    runat="server" CommandName="Sort" CommandArgument="date asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>" ID="DateSortDescButton"
                                                    runat="server" CommandName="Sort" CommandArgument="date desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tableHead">
                                    <table role="presentation">
                                        <tr>
                                            <td rowspan="2" style="height:16px;">
                                                <asp:Literal runat="server" ID="ReferralsHeader" />
                                            </td>
                                            <td style="height:8px;white-space:nowrap">
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortAscImage() %>" ID="ReferralSortAscButton"
                                                    runat="server" CommandName="Sort" CommandArgument="referralcount asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                <asp:ImageButton ImageUrl="<%# UrlBuilder.CategorySortDescImage() %>" ID="ReferralSortDescButton"
                                                    runat="server" CommandName="Sort" CommandArgument="referralcount desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="tableHead">
                                    <asp:Literal runat="server" ID="IssuesHeaderLiteral"></asp:Literal>
                                </td>
                            </tr>
                            <asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="width: 1%;">
                                <%if (IsAssistEmployersAdministrator())
                                  {%>
																	<asp:Label ID="SelectorCheckBoxLabel" runat="server" Text="Check box" CssClass="sr-only" AssociatedControlID="SelectorCheckBox" />
                                <asp:CheckBox ID="SelectorCheckBox" runat="server" ClientIDMode="AutoID" CausesValidation="False" />
                                <% } %>
                                <input type="hidden" name="NumberOfOpenings-<%#((JobViewModel)Container.DataItem).Id%>" value="<%# ((JobViewModel)Container.DataItem).NumberOfOpenings %>" />
                            </td>
                            <td style="width:20%;">
                                <a href="<%# UrlBuilder.AssistJobView(((JobViewModel)Container.DataItem).Id) + FilterQueryString %>"><%# ((JobViewModel)Container.DataItem).JobTitle%></a>
                            </td>
                            <td style="width:7%;">
                                <asp:Literal runat="server" ID="JobIdLiteral"></asp:Literal>
                            </td>
                            <td style="width:20%;">
                                <a id="BusinessUnitUrl" runat="server" href=""><%# ((JobViewModel)Container.DataItem).BusinessUnitName%></a>
                            </td>
                            <td style="width:21%;">
                                <%# FormatDates(((JobViewModel)Container.DataItem))%>
                            </td>
                            <td style="width:10%;">
                                <a id="JobReferrals_Url" runat="server" href=""><%# ((JobViewModel)Container.DataItem).ReferralCount%></a>
                            </td>
                            <td style="width:21%;">
                                <asp:Literal runat="server" ID="IssuesLiteral"></asp:Literal>
                                <span id="AdditionalIssuesLink_<%#((JobViewModel)Container.DataItem).Id%>">
                                    <asp:HyperLink runat="server" ID="ShowAdditionalIssuesLink" Visible="False" NavigateUrl="javascript:void(0);"
                                        ClientIDMode="Predictable"></asp:HyperLink>
																</span> 
																<span style="display: none" id="AdditionalIssues_<%#((JobViewModel)Container.DataItem).Id%>">
																	<asp:Literal runat="server" ID="AdditionalIssuesLiteral"></asp:Literal>
                                  <a href="javascript:void(0);" onclick="HideAdditionalIssues(<%#((JobViewModel)Container.DataItem).Id%>);"><%=HtmlLocalise("Hide.text", "Hide") %></a>
																</span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <br />
                        <%=HtmlLocalise("NoJobOrders", "No #POSTINGTYPES#:LOWER found") %></EmptyDataTemplate>
                </asp:ListView>
                <asp:ObjectDataSource ID="JobsDataSource" runat="server" TypeName="Focus.Web.WebAssist.JobOrderDashboard" EnablePaging="true" SelectMethod="GetJobs" 
																			OnSelecting="SearchResultDataSource_Selecting" SelectCountMethod="GetJobsCount" SortParameterName="orderBy"></asp:ObjectDataSource>
                <asp:CustomValidator ID="JobsListValidator" runat="server" ClientValidationFunction="validateEmployeeSelected" ValidationGroup="Action" ValidateEmptyText="True" CssClass="error" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="singleAccordionContentWrapper">
                    <asp:Panel ID="CreateJobOrderHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                        <table class="accordionTable" role="presentation">
                            <tr>
                                <td>
                                    <asp:Image ID="CreateJobOrderHeaderImage" runat="server" alt="." />
                                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("CreateJobOrder.Label", "Create #EMPLOYMENTTYPE#:LOWER")%></a></span>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="CreateJobOrderPanel" runat="server" CssClass="singleAccordionContent">
                        <uc:FindHiringManager ID="FindManager" runat="server" />
                    </asp:Panel>
                    <act:CollapsiblePanelExtender ID="CreateJobOrderCollapsiblePanelExtender" runat="server" TargetControlID="CreateJobOrderPanel" ExpandControlID="CreateJobOrderHeaderPanel"
                        CollapseControlID="CreateJobOrderHeaderPanel" Collapsed="true" ImageControlID="CreateJobOrderHeaderImage" SuppressPostBack="true" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="singleAccordionContentWrapper">
                    <asp:Panel ID="FindEmployerHeaderPanel" runat="server" CssClass="singleAccordionTitle">
                        <table class="accordionTable" role="presentation">
                            <tr>
                                <td>
                                    <asp:Image ID="FindEmployerHeaderImage" runat="server" alt="." />
                                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#" runat="server" id="FindABusinessAccountHRef"><span class="sr-only">&nbsp;Link 1</span></a></span>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="FindEmployerPanel" runat="server" CssClass="singleAccordionContent">
                        <uc:FindEmployer ID="FindEmployer" runat="server" />
                    </asp:Panel>
                    <act:CollapsiblePanelExtender ID="FindAnEmployerPanelCollapsiblePanelExtender" runat="server" TargetControlID="FindEmployerPanel" ExpandControlID="FindEmployerHeaderPanel"
                        CollapseControlID="FindEmployerHeaderPanel" Collapsed="true" ImageControlID="FindEmployerHeaderImage" SuppressPostBack="true" />
                </div>
            </td>
        </tr>
    </table>
    <%-- Modals --%>
    <uc:ConfirmationModal ID="Confirmation" runat="server" />
    <uc:ErrorModal ID="_Error" runat="server" />
    <uc:NotesAndRemindersModal ID="NotesAndReminders" runat="server" />
    <uc:EmailEmployeeModal ID="EmailEmployee" runat="server" />
    <uc:JobIssueResolutionModal ID="JobIssueResolution" runat="server" OnCompleted="JobAction_Completed" />
    <uc:RequestFollowUp ID="RequestFollowUpModal" runat="server" OnFollowUpRequested="JobAction_Completed" />
		<uc:EmployerActivityReportCriteriaModal ID="EmployerActivityReportCriteriaModal" runat="server" ReportType="EmployerActivity" />
    <script type="text/javascript">

      var Business_TalentPopup = null;

        Sys.Application.add_load(function () {
            $("#" + JobOrderDashboard_Variables["FindEmployerPanel"]).parent().css("height", "auto");
        });

        $(document).ready(function () {

            var filter = $("#" + JobOrderDashboard_Variables["JobOrdersFilterDropDown"]);
            JobOrderDashboard_UpdateSubFilter(filter);

            filter.change(function () {
                JobOrderDashboard_UpdateSubFilter($(this));
            });

	        OfficeFilter_Enable(JobOrderDashboard_Variables["OfficeFilter"], $("#" + JobOrderDashboard_Variables["JobOrdersStatusDropDown"]).val() !== "Draft");

            $("#" + JobOrderDashboard_Variables["JobOrdersStatusDropDown"]).change(function () {
                OfficeFilter_Enable(JobOrderDashboard_Variables["OfficeFilter"], $(this).val() !== "Draft");
            });

            $("*[data-panel='jobordersearch'] input:text").keypress(function (event) {
                if (event.keyCode == 13) {
                    $("#<%=JobFilterButton.ClientID %>").click();
                }

                return event.keyCode != 13;
            });
        });

        function JobOrderDashboard_UpdateSubFilter(filter) {
            var subFilter = $("#" + JobOrderDashboard_Variables["JobOrdersFilterSubMenuDropDown"]);
            subFilter.children('option').hide();

            var array = JobOrderDashboard_SubFilter[filter.val()];
            if (array != null && array.length > 0) {
                $.each(subFilter.children('option'), function () {
                    if (array.indexOf(this.value) < 0) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
                subFilter.show();
            } else {
                subFilter.hide();
            }
        }

        function SelectDeselectAllCheckboxes() {

            var checkboxes = $('input:checkbox');
            var checked = $("[id$='SelectorAllCheckBoxes']").is(':checked');
            for (var i = 0; i <= checkboxes.length - 1; i++)
                checkboxes[i].checked = checked;

            return false;
        }

        function HideSubAction() {
            $("#SubActionButton").hide();
            $("#SubActionDropDown").hide();
            return false;
        }

        function showPopup(url, blockedPopupMessage) {
            var popup = window.open(url, "Business_blank");
            if (!popup)
              alert(blockedPopupMessage);

          return popup;
        }

        function validateEmployeeSelected(oSrc, args) {
            args.IsValid = checkEmployeeSelected();
        }

        function checkEmployeeSelected() {
            var radios = $('input:radio');
            var checked = false;

            for (var i = 0; i <= radios.length - 1; i++)
                if (radios[i].checked)
                    checked = true;

            return checked;
        }

        function ToFromDateCheck(sender, args) {
            var fromMonth = $("#<%= FromMonthDropDownList.ClientID  %> :selected").text();
            var fromYear = $("#<%= FromYearDropDownList.ClientID  %> :selected").text();
            var toMonth = $("#<%= ToMonthDropDownList.ClientID  %> :selected").text();
            var toYear = $("#<%= ToYearDropDownList.ClientID  %> :selected").text();


            args.IsValid = false;

            if (fromMonth == 'month' && fromYear != 'year' || fromMonth != 'month' && fromYear == 'year')
                return;

            if (toMonth == 'month' && toYear != 'year' || toMonth != 'month' && toYear == 'year')
                return;

            if (fromMonth == 'month' && fromYear == 'year' || toMonth == 'month' && toYear == 'year') {
                args.IsValid = true;
                return;
            }

            var fromDate = Date.parse(fromMonth + " 1, " + fromYear);

            var toDate = Date.parse(toMonth + " 28, " + toYear);

            args.IsValid = toDate > fromDate;
        }

        // Expands a collapsible panel
        function ExpandCollapsiblePanel(panelName) {
            var collPanel = $find(panelName);
            if (collPanel.get_Collapsed()) {
                collPanel.set_Collapsed(false);
            }
        }

        function ShowAdditionalIssues(id) {
            $('#AdditionalIssues_' + id).show();
            $('#AdditionalIssuesLink_' + id).hide();
        }
        function HideAdditionalIssues(id) {
            $('#AdditionalIssues_' + id).hide();
            $('#AdditionalIssuesLink_' + id).show();
        }

      function JobOrders_ActionDropDown_Validate(sender, args) {

        var action = $("#<%=JobOrdersActionDropDown.ClientID %>").val();

        if (action == "AccessEmployeeAccount" && (Business_TalentPopup != null && !Business_TalentPopup.closed)) {
          $(sender).text("<%=CloseBusinessWindowErrorMessage %>");
          args.IsValid = false;
        }
      }
    </script>
</asp:Content>
