﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Focus.Web.Core.Models;
using Constants = Focus.Core.Constants;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekerReferralApprover)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer)]
	public partial class JobSeekerReferral : PageBase
	{
		protected bool _jobReferralFilterApplied;
		private long _candidateApplicationId;
		private const string FilteredURL = "?filtered=true";
    private JobSeekerReferralAllStatusViewDto _candidateApplication
		{
			get { return GetViewStateValue<JobSeekerReferralAllStatusViewDto>("CandidateReferral:candidateApplication"); }
			set { SetViewStateValue("CandidateReferral:candidateApplication", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("Page.Title", "Approval Queues-Jobseeker");

			if (Request.QueryString["filtered"] != null) _jobReferralFilterApplied = Convert.ToBoolean(Request.QueryString["filtered"]);

			// Get the Candidate Application Id
            if (Page.RouteData.Values.ContainsKey("id"))
            {
                long.TryParse(Page.RouteData.Values["id"].ToString(), out _candidateApplicationId);
                Page.Title = string.Format("{0} - {1}",Page.Title,_candidateApplicationId);
            }

			if (_candidateApplicationId == 0)
				throw new Exception(FormatError(ErrorTypes.CandidateReferralNotFound, "Invalid #CANDIDATETYPE# referral id"));
			
			if (!IsPostBack)
			{
				_candidateApplication = ServiceClientLocator.CandidateClient(App).GetJobSeekerReferral(_candidateApplicationId);
				LocaliseUI();
			  ApplyBranding();
        Bind();

				ApprovalButtonsCell.Visible = App.User.IsInRole(Constants.RoleKeys.AssistJobSeekerReferralApprover);
			}

      SetReturnUrl();
		}

		/// <summary>
		/// Handles the Clicked event of the ApproveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ApproveButton_Clicked(object sender, EventArgs e)
		{
				bool alienRegistrationExpire = false;

				long defaultResumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(_candidateApplication.CandidateId, true);
				var defaultResume = ServiceClientLocator.ResumeClient(App).GetResume(defaultResumeId);

				if (defaultResume.IsNotNull() && defaultResume.ResumeContent.Profile.IsNotNull())
						{
								var userProfile = defaultResume.ResumeContent.Profile;
					
								if (!(userProfile.IsUSCitizen??true))
								{
										if (userProfile.AlienExpires.HasValue)
										{
												if(userProfile.AlienExpires < DateTime.Now)
												{
														alienRegistrationExpire = true;
												}
										}
								}
						}

				// if non US and registraton expired then do not allow posting
			if (alienRegistrationExpire)
			{
				ReferralConfirmationModal.Show(CodeLocalise("ReferralQueued.Title", "Unable to refer"),
					CodeLocalise("ReferralQueued.Body",
						"The selected job seeker has a lapsed alien registration expiry date on their default resume and therefore you are unable to refer them to this posting."),
					CodeLocalise("Global.Close.Text", "Close"));
				ConfirmationUpdatePanel.Update();

			}
			else
			{
				Utilities.ApproveCandidateReferral(_candidateApplication, false, EmailTemplateTypes.ApproveCandidateReferral,
					ConfirmationModal, _jobReferralFilterApplied ? UrlBuilder.JobSeekerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.JobSeekerReferrals());
			}
      
		}

		/// <summary>
		/// Handles the Clicked event of the DenyButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DenyButton_Clicked(object sender, EventArgs e)
		{
			DenyCandidateReferral.Show(_candidateApplicationId, _candidateApplication.LockVersion,_jobReferralFilterApplied);
		}

		/// <summary>
		/// Handles the Clicked event of the HoldButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void HoldButton_Clicked(object sender, EventArgs e)
		{
			HoldCandidateReferralModal.Show(_candidateApplication, _jobReferralFilterApplied);
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ApproveButton.Text = CodeLocalise("ApproveButton.Text", "Approve referral");
			DenyButton.Text = CodeLocalise("DenyButton.Text", "Deny referral");
			HoldButton.Text = CodeLocalise("HoldButton.Text", "Place on hold");

      JobSeekerProfileLink.Text = CodeLocalise("JobSeekerProfileLink.Text", "View #CANDIDATETYPE# profile");
      JobOrderProfileLink.Text = CodeLocalise("JobOrderProfileLink.Text", "View #POSTINGTYPE# profile");
			var returnPage = GetReturnUrl();
			if (returnPage.Contains("pool/referrals"))
			{
				ReturnLink.Text = HtmlLocalise("ReturnToPool.LinkText", "Return to pending referrals");
			}
			else if (returnPage.Contains("assist/jobposting/"))
			{
				ReturnLink.Text = HtmlLocalise("ReturnToPool.LinkText", "Return to job posting");
			}
			else
			{
				ReturnLink.Text = HtmlLocalise("ReturnToList.LinkText", "Return to main page");
			}

		}
    
    /// <summary>
    /// Applies the brading
    /// </summary>
    private void ApplyBranding()
    {
      ReferralRequestStatusHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      ReferralRequestStatusCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      ReferralRequestStatusCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      ResumeAndPostingHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      ResumeAndPostingCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      ResumeAndPostingCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
    }

    /// <summary>
    /// Binds the controls to the data source
    /// </summary>
    private void Bind()
    {
      CandidateNameLiteral.Text = _candidateApplication.Name;

      ReferralReasonLiteral.Text = (_candidateApplication.ApprovalRequiredReason.IsNotNullOrEmpty()) ? _candidateApplication.ApprovalRequiredReason : CodeLocalise("NoReferralReasonGiven.Text", "No referral reason given");
      BindScore(ReferralScoreImage, _candidateApplication.ApplicationScore);

      BindApprovalStatus();

      JobSeekerProfileLink.NavigateUrl = UrlBuilder.AssistJobSeekerProfile(_candidateApplication.CandidateId);
      JobOrderProfileLink.NavigateUrl = UrlBuilder.AssistJobView(_candidateApplication.JobId);

	    var posting = Utilities.InsertPostingFooter(_candidateApplication.Posting, _candidateApplication.JobId);

      //Get the job for this referral to see if it is HSA or HSB Posting
      var jobId = _candidateApplication.JobId;
      var job = ServiceClientLocator.JobClient(App).GetJob(jobId);
			
      
			if (job.IsNotNull())
      {
				SetPostingApprovalStatus(job.JobStatus);

        var imageString = string.Empty;
        if (Convert.ToBoolean(job.ForeignLabourCertificationH2A))
        {
          imageString = string.Format("<img src='{0}' title='{1}' alt='{2}' class='jobSeekerReferralForeignLabour' />", UrlBuilder.H2AImage(), "f", "f");
        }
        else if (Convert.ToBoolean(job.ForeignLabourCertificationH2B))
        {
          imageString = string.Format("<img src='{0}' title='{1}' alt='{2}' class='jobSeekerReferralForeignLabour' />", UrlBuilder.H2BImage(), "f", "f");
        }

        if (!imageString.IsNullOrEmpty())
        {
          //This section of the code is dependant on the </b> tag to insert the image.
          var index = posting.IndexOf("</b>", StringComparison.Ordinal);
          posting = posting.Insert(index + 4, imageString);
        }

				if (job.JobStatus != JobStatuses.Active)
				{
					ApproveButton.Visible = HoldButton.Visible = false;
				}
      }

			if (_candidateApplication.ApprovalStatus.IsIn(ApprovalStatuses.None, ApprovalStatuses.Approved))
			{
				ApproveButton.Visible = HoldButton.Visible = DenyButton.Visible = false;
				ConfirmationModal.Show(CodeLocalise("AlreadyApproved.Title", "Approve referral request"),
															 CodeLocalise("AlreadyApproved.Body", "The referral has already been approved"),
											  			 CodeLocalise("Global.Close.Text", "Close"));
			}

			//Remove html and body tags to prevent accessibility errors (FVN-1983)
			var bodyPosPosting = posting.IndexOf("<body>", StringComparison.CurrentCulture);
			var bodyEndPosPosting = posting.IndexOf("</body>", StringComparison.CurrentCulture);

			posting = posting.Remove(bodyEndPosPosting);
			posting = posting.Remove(0, bodyPosPosting + 6);

      PostingLiteral.Text = posting;

	    var noResumeMessage = CodeLocalise("NoResumeMessage", "This job seeker no longer has a default resume to display");

			var resumeHtml = ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(_candidateApplication.CandidateId, true, includeAge: job.MinimumAge.GetValueOrDefault(0) > 0, noResumeMessage: noResumeMessage, jobId:_candidateApplication.JobId);
			
	    var bodyPosResume = resumeHtml.IndexOf("<body>",StringComparison.CurrentCulture);
			var bodyEndPosResume = resumeHtml.IndexOf("</body>", StringComparison.CurrentCulture);

	    resumeHtml = resumeHtml.Remove(bodyEndPosResume);
			resumeHtml = resumeHtml.Remove(0,bodyPosResume+6);

      ResumeLiteral.Text = resumeHtml;
			if (String.CompareOrdinal(resumeHtml, noResumeMessage) == 0)
			{
				ApproveButton.Visible = false;
			}
    }

    /// <summary>
    /// Displays details of the referral
    /// </summary>
    private void BindApprovalStatus()
    {
      UserDetailsModel user = null;
      if (_candidateApplication.StatusLastChangedBy.IsNotNull())
          user = ServiceClientLocator.AccountClient(App).GetUserDetails(_candidateApplication.StatusLastChangedBy.Value);

      if (_candidateApplication.ApprovalStatus == ApprovalStatuses.Rejected)
      {
        DenyButton.Visible = false;

        ReferralApprovalStatusLabel.Text = CodeLocalise("Denied.Text", "Denied");

				if (_candidateApplication.AutomaticallyDenied) ReferralApprovalStatusDetails.Text = CodeLocalise("ReferralAutoDeniedOn.Text", "Auto denied on {0}", _candidateApplication.StatusLastChangedOn);
        else if (user.IsNotNull())
        {
          ReferralApprovalStatusDetails.Text = CodeLocalise("ReferralDeniedBy.Text", "Referral denied by {0} {1} on {2}",
                                                            user.PersonDetails.FirstName,
                                                            user.PersonDetails.LastName,
                                                            _candidateApplication.StatusLastChangedOn);
        }

        var emails = ServiceClientLocator.CoreClient(App).GetReferralEmails(null, _candidateApplication.Id, ApprovalStatuses.Rejected, 1);
        if (emails.Any())
        {
          EmailPlaceHolder.Visible = true;
          HoldEmailTextBox.Text = emails.Select(email => email.EmailText).First();
        }
      }
      else if (_candidateApplication.ApprovalStatus == ApprovalStatuses.WaitingApproval)
      {
        ReferralApprovalStatusLabel.Text = CodeLocalise("Pending.Text", "Pending");
        ReferralApprovalStatusDetails.Visible = false;
        EmailPlaceHolder.Visible = false;

        if (user.IsNotNull())
        {
          ReferralApprovalStatusDetails.Text = CodeLocalise("PlacedUnderReviewBy.Text", "Placed under review by {0} {1} on {2}",
                                                            user.PersonDetails.FirstName,
                                                            user.PersonDetails.LastName,
                                                            _candidateApplication.StatusLastChangedOn);
        }
        else
        {
          ReferralApprovalStatusDetails.Text = CodeLocalise("PlacedUnderReviewOn.Text", "Placed under review on {0}", _candidateApplication.StatusLastChangedOn);
        }
      }
			else if (_candidateApplication.ApprovalStatus == ApprovalStatuses.OnHold)
			{
				HoldButton.Visible = false;

				ReferralApprovalStatusLabel.Text = CodeLocalise("OnHold.Text", "On-Hold");

				if (_candidateApplication.AutomaticallyOnHold.GetValueOrDefault(false))
				{
					ReferralApprovalStatusDetails.Text = CodeLocalise("ReferralAutoOnHold.Text", "Placed on Auto on-hold on {0}", _candidateApplication.StatusLastChangedOn);
				}
				else if (user.IsNotNull())
				{
					ReferralApprovalStatusDetails.Text = CodeLocalise("ReferralDeniedBy.Text", "Placed on-hold by {0} {1} on {2}",
					                                                  user.PersonDetails.FirstName,
					                                                  user.PersonDetails.LastName,
					                                                  _candidateApplication.StatusLastChangedOn);
				}

				var emails = ServiceClientLocator.CoreClient(App).GetReferralEmails(null, _candidateApplication.Id, ApprovalStatuses.OnHold, 1);
				if (emails.Any())
				{
					EmailPlaceHolder.Visible = true;
					HoldEmailTextBox.Text = emails.Select(email => email.EmailText).First();
				}

				var isAuto = _candidateApplication.AutomaticallyOnHold.GetValueOrDefault(false);
				ApproveButton.Visible = !isAuto;
			}
			else if (_candidateApplication.ApprovalStatus == ApprovalStatuses.Reconsider)
			{
				var statusDetails = string.Empty;

				ReferralApprovalStatusLabel.Text = CodeLocalise("Reconsider.Text", "Reconsider");

				if (user.IsNotNull())
				{
					statusDetails = CodeLocalise("PlacedUnderReviewBy.Text", "Referral denied by {0} {1} on {2}",
						user.PersonDetails.FirstName,
						user.PersonDetails.LastName,
						_candidateApplication.StatusLastChangedOn);
				}

				statusDetails += "<br/>";
				statusDetails += CodeLocalise("ReferralAutoReconsider.Text", "Set to Reconsider on {0}", _candidateApplication.StatusLastChangedOn);

				ReferralApprovalStatusDetails.Text = statusDetails;
				EmailPlaceHolder.Visible = false;
				DenyButton.Visible = true;
				ApproveButton.Visible = true;

				var emails = ServiceClientLocator.CoreClient(App).GetReferralEmails(null, _candidateApplication.Id, ApprovalStatuses.Rejected, 1);

				if (emails.Any())
				{
					EmailPlaceHolder.Visible = true;
					HoldEmailTextBox.Text = emails.Select(email => email.EmailText).First();
				}
			}
    }

    /// <summary>
    /// Binds the score.
    /// </summary>
    /// <param name="scoreImage">The score image.</param>
    /// <param name="score">The score.</param>
    private void BindScore(Image scoreImage, int score)
    {
      var minimumStarScores = App.Settings.StarRatingMinimumScores;

      scoreImage.ToolTip = score.ToString(CultureInfo.InvariantCulture);

      if (score >= minimumStarScores[5])
        scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
      else if (score >= minimumStarScores[4])
        scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
      else if (score >= minimumStarScores[3])
        scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
      else if (score >= minimumStarScores[2])
        scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
      else if (score >= minimumStarScores[1])
        scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
      else
        scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
    }

    /// <summary>
    /// Binds the foreign labour certification image.
    /// </summary>
    /// <param name="image">The veteran image.</param>
    /// <exception cref="NotImplementedException"></exception>
    private void BindH2AImage(Image image)
    {
      image.ToolTip = image.AlternateText = CodeLocalise("ForeignLabourCertificationImage.ToolTip", "Foreign Labour Certification");
      image.ImageUrl = UrlBuilder.H2AImage();
      image.Visible = true;

    }

    /// <summary>
    /// Binds the foreign labour certification image.
    /// </summary>
    /// <param name="image">The veteran image.</param>
    /// <exception cref="NotImplementedException"></exception>
    private void BindH2BImage(Image image)
    {
      image.ToolTip = image.AlternateText = CodeLocalise("ForeignLabourCertificationImage.ToolTip", "Foreign Labour Certification");
      image.ImageUrl = UrlBuilder.H2BImage();
      image.Visible = true;

    }

		//added for hbeing able to understand visibility of action buttons
		private void SetPostingApprovalStatus(JobStatuses jobStatus)
		{

			string postingStatusLabel = "";

			switch (jobStatus)
			{
				case JobStatuses.Active:
					postingStatusLabel = CodeLocalise("Active.Text", "Active");
					break;
				case JobStatuses.AwaitingEmployerApproval:
					postingStatusLabel = CodeLocalise("Pending.Text", "Pending Employer Approval");
					break;
				case JobStatuses.Closed:
					postingStatusLabel = CodeLocalise("Clised.Text", "Closed");
					break;
				case JobStatuses.Draft:
					postingStatusLabel = CodeLocalise("Draft.Text", "Draft");
					break;
				case JobStatuses.OnHold:
					postingStatusLabel = CodeLocalise("OnHold.Text", "On-Hold"); 
					break;
				default:
					postingStatusLabel = CodeLocalise("Active.Text", "Active");
					break;
			}

			PostingApprovalStatusLabel.Text = postingStatusLabel;
		}

		protected void lnkReturn_Click(object sender, EventArgs e)
		{
				var returnPage = GetReturnUrl();
				if (!returnPage.Contains("assist/jobposting/") || !returnPage.Contains("pool/referrals"))
				{
					returnPage = UrlBuilder.JobSeekerReferrals();
				}

        if (_jobReferralFilterApplied && !returnPage.Contains("filtered="))
        {
          Response.Redirect(returnPage + "?filtered=true");
        }
        else
        {
          Response.Redirect(returnPage);
        }
      }
	}
}