﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Focus.Core.Criteria.User;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Settings.Interfaces;
using Focus.Services;
using Focus.Services.Core;
using Focus.Web.Code;
using Focus.Web.Code.Controllers.WebAssist;
using Focus.Web.Core.Models;
using Focus.Web.WebAssist.Controls;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
    [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistAccountAdministrator)]
    [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistStaffView)]
    public partial class ManageStaffAccounts : PageBase
    {
        #region Properties
        private int _staffCount;

        /// <summary>
        /// Gets or sets the staff criteria.
        /// </summary>
        /// <value>The staff criteria.</value>
        protected StaffCriteria StaffCriteria
        {
            get { return GetViewStateValue<StaffCriteria>("ManageStaffAccounts:StaffCriteria"); }
            set { SetViewStateValue("ManageStaffAccounts:StaffCriteria", value); }
        }

        protected ManageStaffController PageController { get { return PageControllerBaseProperty as ManageStaffController; } }

        #endregion

        /// <summary>
        /// Registers the page controller.
        /// </summary>
        /// <returns></returns>
        protected override IPageController RegisterPageController()
        {
            return new ManageStaffController(App);
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = CodeLocalise("Page.Title", "Manage #FOCUSASSIST#-StaffAccounts");
            LocaliseUI();

            if (!IsPostBack)
            {
                BindAccountStatusDropDown();
                if (App.Settings.OfficesEnabled)
                    BindOfficeDropDown(OfficeDropDown);
                else
                    OfficeRow.Visible = false;

                UploadMultipleUsersButton.Visible =
                    CreateAccountButton.Visible = !App.Settings.AuthenticateAssistUserViaClient && App.User.IsInAnyRole(Constants.RoleKeys.AssistAccountAdministrator, Constants.RoleKeys.AssistStaffDetails);

            }
        }


        /// <summary>
        /// Handles the Clicked event of the FindButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        protected void FindButton_Clicked(object sender, EventArgs e)
        {
            SearchResultListPager.ReturnToFirstPage();
            UnbindSearch();
            BindStaffList();

        }

        /// <summary>
        /// Handles the Clicked event of the CreateAccountButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateAccountButton_Clicked(object sender, EventArgs e)
        {
            CreateStaffAccountModal.Show();
        }

        /// <summary>
        /// Handles the Clicked event of the UploadMultipleUsersButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void UploadMultipleUsersButton_Clicked(object sender, EventArgs e)
        {
            UploadMultipleUsersModal.Show();
        }

        #region ObjectDataSource Methods

        /// <summary>
        /// Gets the staff.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="startRowIndex">Start index of the row.</param>
        /// <param name="maximumRows">The maximum rows.</param>
        /// <returns></returns>
        public List<StaffSearchViewDto> GetStaff(StaffCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
        {
            var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

            if (criteria.IsNull())
            {
                _staffCount = 0;
                return null;
            }

            criteria.PageSize = maximumRows;
            criteria.PageIndex = pageIndex;

            var staff = ServiceClientLocator.StaffClient(App).SearchStaff(criteria);
            _staffCount = staff.TotalCount;

            return staff;
        }

        /// <summary>
        /// Gets the staff count.
        /// </summary>
        /// <returns></returns>
        public int GetStaffCount()
        {
            return _staffCount;
        }

        /// <summary>
        /// Gets the staff count.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public int GetStaffCount(StaffCriteria criteria)
        {
            return _staffCount;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the Selecting event of the SearchResultDataSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
        protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["criteria"] = StaffCriteria;
        }

        /// <summary>
        /// Handles the ItemDataBound event of the SearchResultListView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
        protected void SearchResultListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var staffMember = (StaffSearchViewDto)e.Item.DataItem;

            // StaffSearchViewDto Id holds the userid of the staff member
            var staffMembersRoles = ServiceClientLocator.AccountClient(App).GetUsersRoles(staffMember.Id.Value);

            var staffMemberBackdateSettings = ServiceClientLocator.StaffClient(App).GetBackdateSettings(staffMember.PersonId);

            var hasEditRole = App.User.IsInAnyRole(Constants.RoleKeys.AssistAccountAdministrator, Constants.RoleKeys.AssistUpdatePermissions);
            var permissonsPanel = (Panel)e.Item.FindControl("PermissionsPanel");
            permissonsPanel.Enabled = hasEditRole;

            if (!App.User.IsInAnyRole(Constants.RoleKeys.AssistAccountAdministrator, Constants.RoleKeys.AssistStaffResetPassword))
            {
                e.Item.FindControl("ResetPasswordButton").Visible = false;
            }

            // To eliminate object not set to instance of object error
            if (staffMembersRoles.IsNull()) staffMembersRoles = new List<RoleDto>();

            var manageStatusButton = (LinkButton)e.Item.FindControl("ManageStatusButton");
            if (!App.User.IsInAnyRole(Constants.RoleKeys.AssistAccountAdministrator, Constants.RoleKeys.AssistStaffDeactivateActivate))
            {
                manageStatusButton.Visible = false;
            }
            else
            {
                if (!staffMember.Blocked)
                {
                    manageStatusButton.Text = @"Deactivate account";
                    manageStatusButton.CommandName = "DeactivateAccount";
                }

                if (staffMember.Blocked)
                {
                    manageStatusButton.Text = @"Reactivate account";
                    manageStatusButton.CommandName = "ReactivateAccount";
                }
            }

            if (!App.User.IsInAnyRole(Constants.RoleKeys.AssistAccountAdministrator, Constants.RoleKeys.AssistUpdatePermissions))
            {
                e.Item.FindControl("UpdateRolesButton").Visible = false;
            }

            #region Manager

            if (App.Settings.OfficesEnabled)
            {
                var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(staffMember.Id.Value);

                var managerCheckBox = (CheckBox)e.Item.FindControl("ManagerCheckBox");
                managerCheckBox.Text = CodeLocalise("ManagerCheckBox.Label", "Manager");

                if (userDetails.PersonDetails.Manager.IsNotNull())
                    managerCheckBox.Checked = (bool)userDetails.PersonDetails.Manager;

                managerCheckBox.Enabled = hasEditRole;
            }
            else
            {
                var managerCheckBox = (CheckBox)e.Item.FindControl("ManagerCheckBox");
                managerCheckBox.Visible = false;
            }

            #endregion

            #region Manage Job Seeker Accounts
            var hasAccountAdminRole = App.User.IsInAnyRole(Constants.RoleKeys.AssistAccountAdministrator);
            var hasSystemAdminRole = App.User.IsInAnyRole(Constants.RoleKeys.AssistSystemAdministrator);

            var manageJobSeekersReadOnlyCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersReadOnlyCheckBox");
            manageJobSeekersReadOnlyCheckBox.Text = CodeLocalise("ManageJobSeekersReadOnly.Label", "View");
            manageJobSeekersReadOnlyCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekersReadOnly);

            var manageJobSeekersAdministratorCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersAdministratorCheckBox");
            manageJobSeekersAdministratorCheckBox.Text = CodeLocalise("ManageJobSeekersAdministrator.Label", "View & edit");
            manageJobSeekersAdministratorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekersAdministrator);

            if (App.Settings.OfficesEnabled)
            {
                var assignOfficeFromDefaultCheckBox = (CheckBox)e.Item.FindControl("AssignOfficeFromDefaultCheckBox");
                assignOfficeFromDefaultCheckBox.Text = CodeLocalise("AssignOfficeFromDefaultCheckBox.Label", "Assign job seeker from default office");
                assignOfficeFromDefaultCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistAssignJobSeekerOfficeFromDefault);
                //assignOfficeFromDefaultCheckBox.Enabled = (hasSystemAdminRole && !CheckDefaultOrStatewideOffice(e)) ? true : false;

                var reassignOfficeToNewOfficeCheckBox = (CheckBox)e.Item.FindControl("ReassignOfficeToNewOfficeCheckBox");
                reassignOfficeToNewOfficeCheckBox.Text = CodeLocalise("ReassignOfficeToNewOfficeCheckBox.Label", "Reassign job seeker to new office(s)");
                reassignOfficeToNewOfficeCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.ReassignJobSeekerToNewOffice);

                //var manageJobSeekersOfficesAdministratorCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersOfficesAdministratorCheckBox");
                //manageJobSeekersOfficesAdministratorCheckBox.Text = CodeLocalise("ManageJobSeekersOfficesAdministratorCheckBox.Label", "Assign job seeker from default office");
                //manageJobSeekersOfficesAdministratorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekerOfficeAdministrator);
            }
            else
            {
                e.Item.FindControl("JobSeekerAssignFromDefaultOfficeRow").Visible = false;
                e.Item.FindControl("JobSeekerReassignToNewOfficeRow").Visible = false;
            }

            if (App.Settings.Theme == FocusThemes.Education)
            {
                var manageStudentAccountAdministratorCheckBox = (CheckBox)e.Item.FindControl("ManageStudentAccountAdministratorCheckBox");
                manageStudentAccountAdministratorCheckBox.Text = CodeLocalise("ManageStudentAccountAdministrator.Label", "Invite student registrations");
                manageStudentAccountAdministratorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistStudentAccountAdministrator);
            }
            else
                e.Item.FindControl("ManageStudentAccountAdministratorPlaceHolder").Visible = false;

            if (App.Settings.Theme == FocusThemes.Workforce)
            {
                var manageJobSeekersAccountInactivatorCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersAccountInactivatorCheckBox");
                manageJobSeekersAccountInactivatorCheckBox.Text = CodeLocalise("manageJobSeekersAccountDeactivator.Label", "Inactivate & Reactivate accounts");
                manageJobSeekersAccountInactivatorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekersAccountInactivator);
            }
            else
            {
                e.Item.FindControl("ManageJobSeekersAccountInactivatorPlaceHolder").Visible = false;
            }

            var manageJobSeekersAccountBlockerCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersAccountBlockerCheckBox");
            manageJobSeekersAccountBlockerCheckBox.Text = CodeLocalise("ManageJobSeekersAccountBlocker.Label", "Block & unblock accounts");
            manageJobSeekersAccountBlockerCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekersAccountBlocker);


            var manageJobSeekersActionMenuCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersActionMenuCheckBox");
            manageJobSeekersActionMenuCheckBox.Text = CodeLocalise("ManageJobSeekersActionMenu.Label", "Action Menu:  Backdate my entries");
            manageJobSeekersActionMenuCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.BackdateMyEntriesJSActionMenuMaxDays);
            manageJobSeekersActionMenuCheckBox.Enabled = hasAccountAdminRole;


            var manageJobSeekersActivityLogBackdateEntryCheckBox = (CheckBox)e.Item.FindControl("manageJobSeekersActivityLogBackdateEntryCheckBox");
            manageJobSeekersActivityLogBackdateEntryCheckBox.Text = CodeLocalise("ManageJobSeekersActivityLogBackdateEntry.Label", "Activity Log:  Backdate any entry for up to");
            manageJobSeekersActivityLogBackdateEntryCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.BackdateAnyEntryJSActivityLogMaxDays);
            manageJobSeekersActivityLogBackdateEntryCheckBox.Enabled = hasAccountAdminRole;


            var manageJobSeekersActivityLogBackdateEntriesCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersActivityLogBackdateEntriesCheckBox");
            manageJobSeekersActivityLogBackdateEntriesCheckBox.Text = CodeLocalise("ManageJobSeekersActivityLogBackdateEntries.Label", "Activity Log:  Backdate my entries for up to");
            manageJobSeekersActivityLogBackdateEntriesCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.BackdateMyEntriesJSActivityLogMaxDays);
            manageJobSeekersActivityLogBackdateEntriesCheckBox.Enabled = hasAccountAdminRole;


            var manageJobSeekersActivityLogDeleteEntriesCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersActivityLogDeleteEntriesCheckBox");
            manageJobSeekersActivityLogDeleteEntriesCheckBox.Text = CodeLocalise("ManageJobSeekersActivityLogDeleteEntries.Label", "Activity Log:  Delete my entries for up to");
            manageJobSeekersActivityLogDeleteEntriesCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.DeleteMyEntriesJSActivityLogMaxDays);
            manageJobSeekersActivityLogDeleteEntriesCheckBox.Enabled = hasAccountAdminRole;


            var manageJobSeekersActivityLogDeleteEntryCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersActivityLogDeleteEntryCheckBox");
            manageJobSeekersActivityLogDeleteEntryCheckBox.Text = CodeLocalise("ManageJobSeekersActivityLogDeleteEntry.Label", "Activity Log:  Delete any entry for up to");
            manageJobSeekersActivityLogDeleteEntryCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.DeleteAnyEntryJSActivityLogMaxDays);
            manageJobSeekersActivityLogDeleteEntryCheckBox.Enabled = hasAccountAdminRole;


            var manageJobSeekersSendMessagesCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersSendMessagesCheckBox");
            manageJobSeekersSendMessagesCheckBox.Text = CodeLocalise("ManageJobSeekersAccountSendMessagesCheckBox.Label", "Send messages");
            manageJobSeekersSendMessagesCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekersSendMessages);

            var manageJobSeekersCreateNewAccountCheckBox = (CheckBox)e.Item.FindControl("ManageJobSeekersCreateNewAccountCheckBox");
            manageJobSeekersCreateNewAccountCheckBox.Text = CodeLocalise("ManageJobSeekersCreateNewAccountCheckBox.Label", "Create accounts");
            manageJobSeekersCreateNewAccountCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekersCreateNewAccount);

            #endregion

            #region Manage Employer Accounts

            // If there is no Talent integration, hide the panels. No other work needs to be done.
            if (!App.Settings.TalentModulePresent)
            {
                var manageEmployerAccountsHeaderPanel = (Panel)e.Item.FindControl("ManageEmployerAccountsHeaderPanel");
                manageEmployerAccountsHeaderPanel.Visible = App.Settings.TalentModulePresent;
                var manageEmployerAccountsPanel = (Panel)e.Item.FindControl("ManageEmployerAccountsPanel");
                manageEmployerAccountsPanel.Visible = App.Settings.TalentModulePresent;
            }
            else
            {
                var manageEmployerAccountsLabel = (Label)e.Item.FindControl("ManageEmployerAccountsLabel");

                manageEmployerAccountsLabel.Text = CodeLocalise("ManageEmployerAccountsLabel.Text", App.Settings.OfficesEnabled ? "Manage #BUSINESS#:LOWER accounts & job postings" : "Manage #BUSINESS#:LOWER accounts");

                var manageEmployersReadOnlyCheckBox = (CheckBox)e.Item.FindControl("ManageEmployersReadOnlyCheckBox");
                manageEmployersReadOnlyCheckBox.Text = CodeLocalise("ManageEmployersReadOnly.Label", "View");
                manageEmployersReadOnlyCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployersReadOnly);

                var manageEmployersAdministratorCheckBox = (CheckBox)e.Item.FindControl("ManageEmployersAdministratorCheckBox");
                manageEmployersAdministratorCheckBox.Text = CodeLocalise("ManageEmployersAdministrator.Label", "View & edit");
                manageEmployersAdministratorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployersAdministrator);

                var manageEmployersAccountBlockerCheckBox = (CheckBox)e.Item.FindControl("ManageEmployersAccountBlockerCheckBox");
                manageEmployersAccountBlockerCheckBox.Text = CodeLocalise("ManageEmployersAccountBlockerCheckBox.Label", "Block & unblock accounts");
                manageEmployersAccountBlockerCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployersAccountBlocker);

                if (App.Settings.Theme == FocusThemes.Workforce)
                {
                    var manageEmployerOverrideCriminalExclusionsCheckBox = (CheckBox)e.Item.FindControl("ManageEmployerOverrideCriminalExclusionsCheckBox");
                    manageEmployerOverrideCriminalExclusionsCheckBox.Text = CodeLocalise("ManageEmployerOverrideCriminalExclusionsCheckBox.Label", "Override criminal check(TEGL notice)");
                    manageEmployerOverrideCriminalExclusionsCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployerOverrideCriminalExclusions);
                }
                else
                {
                    e.Item.FindControl("ManageEmployerOverrideCriminalExclusionsCheckBox").Visible = false;
                }

                if (App.Settings.ShowCreditCheck)
                {
                    var manageEmployerOverrideCreditCheckCheckBox = (CheckBox)e.Item.FindControl("ManageEmployerOverrideCreditCheckCheckBox");
                    manageEmployerOverrideCreditCheckCheckBox.Text = CodeLocalise("ManageEmployerOverrideCreditCheckCheckBox.Label", "Override credit check(TEGL notice)");
                    manageEmployerOverrideCreditCheckCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployerOverrideCreditCheckRequired);

                }
                else
                {
                    e.Item.FindControl("ManageEmployerOverrideCreditCheckCheckBox").Visible = false;
                }

                if (App.Settings.OfficesEnabled)
                {
                    var assistAssignBusinessOfficeFromDefaultCheckBox = (CheckBox)e.Item.FindControl("AssistAssignBusinessOfficeFromDefaultCheckBox");
                    assistAssignBusinessOfficeFromDefaultCheckBox.Text = CodeLocalise("AssistAssignBusinessOfficeFromDefaultCheckBox.Label", "Assign business account from default office");
                    assistAssignBusinessOfficeFromDefaultCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistAssignBusinessOfficeFromDefault);
                    //assistAssignBusinessOfficeFromDefaultCheckBox.Enabled = (hasSystemAdminRole && !CheckDefaultOrStatewideOffice(e)) ? true : false;

                    var manageEmployerReassignBusinessCheckBox = (CheckBox)e.Item.FindControl("ManageEmployerReassignBusinessCheckBox");
                    manageEmployerReassignBusinessCheckBox.Text = CodeLocalise("ManageEmployerReassignBusiness.Label", "Reassign business account to new office(s)");
                    manageEmployerReassignBusinessCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.ReassignBusinessToNewOffice);

                    var manageEmployerReassignJobPostingCheckBox = (CheckBox)e.Item.FindControl("ManageEmployerReassignJobPostingCheckBox");
                    manageEmployerReassignJobPostingCheckBox.Text = CodeLocalise("ManageEmployerReassignJobPosting.Label", "Reassign job posting to new office(s)");
                    manageEmployerReassignJobPostingCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.ReassignJobPostingToNewOffice);

                }
                else
                {
                    e.Item.FindControl("EmployerAssignFromDefaultOfficeRow").Visible = false;
                    e.Item.FindControl("BusinessReassignToNewOfficeRow").Visible = false;
                    e.Item.FindControl("JobPostingReassignToNewOfficeRow").Visible = false;
                }

                if (App.Settings.Theme == FocusThemes.Workforce)
                {
                    var manageJobDevelopmentCheckBoxCheckBox = (CheckBox)e.Item.FindControl("ManageJobDevelopmentCheckBox");
                    manageJobDevelopmentCheckBoxCheckBox.Text = CodeLocalise("ManageJobDevelopmentCheckBox.Label", "Handle business development");
                    manageJobDevelopmentCheckBoxCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployerJobDevelopmentManager);
                }
                else
                {
                    e.Item.FindControl("ManageJobDevelopmentCheckBox").Visible = false;
                }

                var manageEmployerSendMessagesCheckBox = (CheckBox)e.Item.FindControl("ManageEmployerSendMessagesCheckBox");
                manageEmployerSendMessagesCheckBox.Text = CodeLocalise("ManageEmployerSendMessagesCheckBox.Label", "Send messages");
                manageEmployerSendMessagesCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployersSendMessages);

                var manageEmployerCreateNewAccountCheckBox = (CheckBox)e.Item.FindControl("ManageEmployerCreateNewAccountCheckBox");
                manageEmployerCreateNewAccountCheckBox.Text = CodeLocalise("ManageEmployerCreateNewAccountCheckBox.Label", "Create accounts");
                manageEmployerCreateNewAccountCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployersCreateNewAccount);



            }
            var manageEmployersActionMenuCheckBox = (CheckBox)e.Item.FindControl("ManageEmployersActionMenuCheckBox");
            manageEmployersActionMenuCheckBox.Text = CodeLocalise("ManageEmployersActionMenu.Label", "Action Menu:  Backdate my entries");
            manageEmployersActionMenuCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.BackdateMyEntriesHMActionMenuMaxDays);
            manageEmployersActionMenuCheckBox.Enabled = hasAccountAdminRole;



            var manageEmployersActivityLogBackdateEntryCheckBox = (CheckBox)e.Item.FindControl("ManageEmployersActivityLogBackdateEntryCheckBox");
            manageEmployersActivityLogBackdateEntryCheckBox.Text = CodeLocalise("ManageEmployersActivityLogBackdateEntry.Label", "Activity Log:  Backdate any entry for up to");
            manageEmployersActivityLogBackdateEntryCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.BackdateAnyEntryHMActivityLogMaxDays);
            manageEmployersActivityLogBackdateEntryCheckBox.Enabled = hasAccountAdminRole;


            var manageEmployersActivityLogBackdateEntriesCheckBox = (CheckBox)e.Item.FindControl("ManageEmployersActivityLogBackdateEntriesCheckBox");
            manageEmployersActivityLogBackdateEntriesCheckBox.Text = CodeLocalise("ManageEmployersActivityLogBackdateEntries.Label", "Activity Log:  Backdate my entries for up to");
            manageEmployersActivityLogBackdateEntriesCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.BackdateMyEntriesHMActivityLogMaxDays);
            manageEmployersActivityLogBackdateEntriesCheckBox.Enabled = hasAccountAdminRole;


            var manageEmployersActivityLogDeleteEntriesCheckBox = (CheckBox)e.Item.FindControl("ManageEmployersActivityLogDeleteEntriesCheckBox");
            manageEmployersActivityLogDeleteEntriesCheckBox.Text = CodeLocalise("ManageEmployersActivityLogDeleteEntries.Label", "Activity Log:  Delete my entries for up to");
            manageEmployersActivityLogDeleteEntriesCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.DeleteMyEntriesHMActivityLogMaxDays);
            manageEmployersActivityLogDeleteEntriesCheckBox.Enabled = hasAccountAdminRole;



            var manageEmployersActivityLogDeleteEntryCheckBox = (CheckBox)e.Item.FindControl("ManageEmployersActivityLogDeleteEntryCheckBox");
            manageEmployersActivityLogDeleteEntryCheckBox.Text = CodeLocalise("ManageEmployersActivityLogDeleteEntry.Label", "Activity Log:  Delete any entry for up to");
            manageEmployersActivityLogDeleteEntryCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.DeleteAnyEntryHMActivityLogMaxDays);
            manageEmployersActivityLogDeleteEntryCheckBox.Enabled = hasAccountAdminRole;

            #endregion

            #region Manage Approval Queues

            // If there is no Talent integration, hide the panels. No other work needs to be done.
            if (!App.Settings.TalentModulePresent)
            {
                var manageApprovalQueuesHeaderPanel = (Panel)e.Item.FindControl("ManageApprovalQueuesHeaderPanel");
                manageApprovalQueuesHeaderPanel.Visible = App.Settings.TalentModulePresent;
                var manageApprovalQueuesPanel = (Panel)e.Item.FindControl("ManageApprovalQueuesPanel");
                manageApprovalQueuesPanel.Visible = App.Settings.TalentModulePresent;
            }
            else
            {
                #region referrals

                var referralViewCheckBox = (CheckBox)e.Item.FindControl("ReferralViewCheckBox");
                var referralViewEditCheckBox = (CheckBox)e.Item.FindControl("ReferralViewEditCheckBox");
                var referralViewHeaderLabel = (Literal)e.Item.FindControl("ReferralRequestsLabel");

                if (App.Settings.JobSeekerReferralsEnabled)
                {
                    referralViewHeaderLabel.Text = CodeLocalise("ReferralRequests.Label", "Referral requests");
                    referralViewCheckBox.Text = CodeLocalise("ReferralView.Label", "View");
                    referralViewEditCheckBox.Text = CodeLocalise("ReferralViewEdit.Label", "View & edit");
                }
                else
                    referralViewCheckBox.Visible = referralViewEditCheckBox.Visible = referralViewHeaderLabel.Visible = false;

                referralViewCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer);
                referralViewEditCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobSeekerReferralApprover);


                #endregion

                #region Employer Accounts

                var employerAccountsViewCheckBox = (CheckBox)e.Item.FindControl("EmployerAccountsViewCheckBox");
                employerAccountsViewCheckBox.Text = CodeLocalise("EmployerAccountsView.Label", "View");
                employerAccountsViewCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployerAccountApprovalViewer);

                var employerAccountsViewEditCheckBox = (CheckBox)e.Item.FindControl("EmployerAccountsViewEditCheckBox");
                employerAccountsViewEditCheckBox.Text = CodeLocalise("EmployerAccountsViewEdit.Label", "View & edit");
                employerAccountsViewEditCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistEmployerAccountApprover);

                #endregion

                #region Job Postings

                var postingViewCheckBox = (CheckBox)e.Item.FindControl("JobPostingsViewCheckBox");
                postingViewCheckBox.Text = CodeLocalise("JobPostingsView.Label", "View");
                postingViewCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistPostingApprovalViewer);

                var postingViewEditCheckBox = (CheckBox)e.Item.FindControl("JobPostingsViewEditCheckBox");
                postingViewEditCheckBox.Text = CodeLocalise("JobPostingsViewEdit.Label", "View & edit");
                postingViewEditCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistPostingApprover);

                #endregion

                #region Special job order approval

                if (App.Settings.Theme == FocusThemes.Workforce)
                {
                    var handleSpecialOrdersPlaceHolder = (PlaceHolder)e.Item.FindControl("HandleSpecialOrdersPlaceHolder");
                    handleSpecialOrdersPlaceHolder.Visible = true;

                    var courtOrderedAffrimativeActionCheckBox = (CheckBox)e.Item.FindControl("CourtOrderedAffrimativeActionCheckBox");
                    courtOrderedAffrimativeActionCheckBox.Text = CodeLocalise("CourtOrderedAffrimativeAction.Label", "Court-ordered affirmative action");
                    courtOrderedAffrimativeActionCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistCourtOrderedAffirmativeActionApprover);
                    courtOrderedAffrimativeActionCheckBox.Enabled = postingViewEditCheckBox.Checked;
                    courtOrderedAffrimativeActionCheckBox.Visible = !App.Settings.HideJobConditions;

                    var federalContractorCheckBox = (CheckBox)e.Item.FindControl("FederalContractorCheckBox");
                    federalContractorCheckBox.Text = CodeLocalise("FederalContractor.Label", "Federal contractor");
                    federalContractorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistFederalContractorApprover);
                    federalContractorCheckBox.Enabled = postingViewEditCheckBox.Checked;
                    federalContractorCheckBox.Visible = !App.Settings.HideJobConditions;

                    var foreignLabourH2AAgCheckBox = (CheckBox)e.Item.FindControl("ForeignLabourH2AAgCheckBox");
                    foreignLabourH2AAgCheckBox.Text = CodeLocalise("ForeignLabourH2AAg.Label", "Foreign labor (H2A - ag)");
                    foreignLabourH2AAgCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistForeignLabourH2AAgApprover);
                    foreignLabourH2AAgCheckBox.Enabled = postingViewEditCheckBox.Checked;
                    foreignLabourH2AAgCheckBox.Visible = !App.Settings.HideJobConditions;

                    var foreignLabourH2BNonAgCheckBox = (CheckBox)e.Item.FindControl("ForeignLabourH2BNonAgCheckBox");
                    foreignLabourH2BNonAgCheckBox.Text = CodeLocalise("ForeignLabourH2BNonAg.Label", "Foreign labor (H2B - non-ag)");
                    foreignLabourH2BNonAgCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistForeignLabourH2BNonAgApprover);
                    foreignLabourH2BNonAgCheckBox.Enabled = postingViewEditCheckBox.Checked;
                    foreignLabourH2BNonAgCheckBox.Visible = !App.Settings.HideJobConditions;

                    var foreignLabourOtherCheckBox = (CheckBox)e.Item.FindControl("ForeignLabourOtherCheckBox");
                    foreignLabourOtherCheckBox.Text = CodeLocalise("ForeignLabourOther.Label", "Foreign labor (other)");
                    foreignLabourOtherCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistForeignLabourOtherApprover);
                    foreignLabourOtherCheckBox.Enabled = postingViewEditCheckBox.Checked;
                    foreignLabourOtherCheckBox.Visible = !App.Settings.HideJobConditions;

                    var homeBasedCheckBox = (CheckBox)e.Item.FindControl("HomeBasedCheckBox");
                    homeBasedCheckBox.Text = CodeLocalise("HomeBased.Label", "Home-based");
                    homeBasedCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistHomeBasedApprover);
                    homeBasedCheckBox.Enabled = postingViewEditCheckBox.Checked;
                    homeBasedCheckBox.Visible = (App.Settings.Theme == FocusThemes.Workforce);

                    var commissionOnlyCheckBox = (CheckBox)e.Item.FindControl("CommissionOnlyCheckBox");
                    commissionOnlyCheckBox.Text = CodeLocalise("CommissionOnly.Label", "Commission-based only");
                    commissionOnlyCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistCommissionOnlyApprover);
                    commissionOnlyCheckBox.Enabled = postingViewEditCheckBox.Checked;

                    var salaryAndCommissionCheckBox = (CheckBox)e.Item.FindControl("SalaryAndCommissionCheckBox");
                    salaryAndCommissionCheckBox.Text = CodeLocalise("SalaryAndCommission.Label", "Commission + salary-based");
                    salaryAndCommissionCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistSalaryAndCommissionApprover);
                    salaryAndCommissionCheckBox.Enabled = postingViewEditCheckBox.Checked;

                    var allConditionsCheckBox = (CheckBox)e.Item.FindControl("AllConditionsCheckBox");
                    allConditionsCheckBox.Text = CodeLocalise("AllConditions.Label", "All job conditions");
                    allConditionsCheckBox.Enabled = postingViewEditCheckBox.Checked;

                    if (postingViewEditCheckBox.Checked)
                    {
                        allConditionsCheckBox.Checked =
                          (commissionOnlyCheckBox.Checked && commissionOnlyCheckBox.Visible)
                          && (salaryAndCommissionCheckBox.Checked && salaryAndCommissionCheckBox.Visible)
                          && (courtOrderedAffrimativeActionCheckBox.Checked && courtOrderedAffrimativeActionCheckBox.Visible)
                          && (federalContractorCheckBox.Checked && federalContractorCheckBox.Visible)
                          && (foreignLabourH2AAgCheckBox.Checked && foreignLabourH2AAgCheckBox.Visible)
                          && (foreignLabourH2BNonAgCheckBox.Checked && foreignLabourH2BNonAgCheckBox.Visible)
                          && (foreignLabourOtherCheckBox.Checked && foreignLabourOtherCheckBox.Visible)
                          && (homeBasedCheckBox.Checked && homeBasedCheckBox.Visible);
                    }
                }
            }

                #endregion

            #endregion

            #region Manage Focus/Assist

            var accountAdministratorCheckBox = (CheckBox)e.Item.FindControl("AccountAdministratorCheckBox");
            accountAdministratorCheckBox.Text = CodeLocalise("AccountAdministrator.Label", "Account admin");
            accountAdministratorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistAccountAdministrator);

            var broadcastAdministratorCheckBox = (CheckBox)e.Item.FindControl("BroadcastAdministratorCheckBox");
            broadcastAdministratorCheckBox.Text = CodeLocalise("BroadcastAdministrator.Label", "Broadcasts");
            broadcastAdministratorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistBroadcastsAdministrator);

            var systemAdministratorCheckBox = (CheckBox)e.Item.FindControl("SystemAdministratorCheckBox");
            systemAdministratorCheckBox.Text = CodeLocalise("SystemAdministrator.Label", "System admin");
            systemAdministratorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistSystemAdministrator);

            if (!App.User.IsInRole(Constants.RoleKeys.AssistAccountAdministrator))
                systemAdministratorCheckBox.Visible = false;
            if (App.Settings.Theme == FocusThemes.Workforce && App.User.IsInRole(Constants.RoleKeys.AssistAccountAdministrator))
            {
                var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(staffMember.Id.Value);

                var lverCheckBox = (CheckBox)e.Item.FindControl("LverCheckBox");
                lverCheckBox.Text = CodeLocalise("LverCheckBox.Label", "LVER");
                lverCheckBox.Checked = userDetails.PersonDetails.LocalVeteranEmploymentRepresentative.GetValueOrDefault(false);

                var dvopCheckBox = (CheckBox)e.Item.FindControl("DvopCheckBox");
                dvopCheckBox.Text = CodeLocalise("DvopCheckBox.Label", "DVOP");
                dvopCheckBox.Checked = userDetails.PersonDetails.DisabledVeteransOutreachProgramSpecialist.GetValueOrDefault(false);
            }
            else
            {
                e.Item.FindControl("LverCheckBox").Visible = false;
                e.Item.FindControl("DvopCheckBox").Visible = false;
            }

            #endregion

            #region Handle work for these offices

            if (App.Settings.OfficesEnabled)
            {
                var officeRadioButtonList = (RadioButtonList)e.Item.FindControl("OfficeRadioButtonList");
                officeRadioButtonList.Items.Add(new ListItem(CodeLocalise("OfficeRadioButtonList.Statewide", "Statewide"), "1"));
                officeRadioButtonList.Items.Add(new ListItem(CodeLocalise("OfficeRadioButtonList.Office", "Handles work for these offices"), "2"));

                var personOfficesControl = (AssignedOffices)e.Item.FindControl("AssignedOffices");
                BindPersonOffices(personOfficesControl, staffMember.PersonId, e.Item);
            }
            else
            {
                var officePlaceHolder = (PlaceHolder)e.Item.FindControl("OfficesPlaceHolder");
                officePlaceHolder.Visible = false;
            }

            #endregion

            #region Manage Job Orders

            //if (App.Settings.OfficesEnabled)
            //{
            //    var manageJobOrdersOfficesAdministratorCheckBox = (CheckBox)e.Item.FindControl("ManageJobOrdersOfficesAdministratorCheckBox");
            //    manageJobOrdersOfficesAdministratorCheckBox.Text = CodeLocalise("ManageJobOrdersOfficesAdministratorCheckBox.Label", "Administer office assignment - job postings");
            //    manageJobOrdersOfficesAdministratorCheckBox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistJobOrderOfficeAdministrator);
            //}
            //else
            //{
            //    e.Item.FindControl("ManageJobOrdersOfficesAdministratorCheckBox").Visible = false;
            //}


            #endregion

            #region Localise Link Buttons

            var resetPasswordButton = (LinkButton)e.Item.FindControl("ResetPasswordButton");

            if ((App.Settings.AuthenticateAssistUserViaClient && !App.Settings.ClientAllowsPasswordReset) || App.Settings.DisableAssistAuthentication)
                resetPasswordButton.Visible = false;
            else
                resetPasswordButton.Text = CodeLocalise("ResetPasswordButton.Text", "Reset password");

            var manageStatusBtn = (LinkButton)e.Item.FindControl("ManageStatusButton");
            if (App.Settings.DisableAssistAuthentication)
                manageStatusBtn.Visible = false;

            var updateRolesButton = (LinkButton)e.Item.FindControl("UpdateRolesButton");
            updateRolesButton.Text = CodeLocalise("UpdateRolesButton.Text", "Update permissions");

            var editStaffButton = (LinkButton)e.Item.FindControl("EditStaffButton");

            if (App.Settings.SamlEnabled || App.Settings.SamlEnabledForAssist || !App.User.IsInAnyRole(Constants.RoleKeys.AssistAccountAdministrator, Constants.RoleKeys.AssistStaffDetails))
            {
                editStaffButton.Visible = false;
            }
            else
            {
                editStaffButton.Text = CodeLocalise("EditStaffAccount.Text", "Edit Staff Account");
            }

            var accountActionSeparatorEdit = (Literal)e.Item.FindControl("AccountActionSeparatorEdit");
            accountActionSeparatorEdit.Visible = editStaffButton.Visible && (resetPasswordButton.Visible || manageStatusBtn.Visible || updateRolesButton.Visible);

            var accountActionSeparatorReset = (Literal)e.Item.FindControl("AccountActionSeparatorReset");
            accountActionSeparatorReset.Visible = resetPasswordButton.Visible && (manageStatusBtn.Visible || updateRolesButton.Visible);

            var accountActionSeparatorManage = (Literal)e.Item.FindControl("AccountActionSeparatorManage");
            accountActionSeparatorManage.Visible = manageStatusBtn.Visible && updateRolesButton.Visible;

            #endregion

            #region Manage offices

            if (App.Settings.OfficesEnabled)
            {
                var checkbox = (CheckBox)e.Item.FindControl("CreateOfficesCheckBox");
                checkbox.Text = CodeLocalise("CreateOfficesCheckBox.Label", "Create offices");
                checkbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistOfficesCreate);

                checkbox = (CheckBox)e.Item.FindControl("ActivateDeactivateCheckbox");
                checkbox.Text = CodeLocalise("ActivateDeactivateCheckbox.Label", "Activate/Deactivate offices");
                checkbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistOfficesActivateDeactivate);

                checkbox = (CheckBox)e.Item.FindControl("ListStaffCheckBox");
                checkbox.Text = CodeLocalise("ListStaffCheckBox.Label", "List staff");
                checkbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistOfficesListStaff);

                checkbox = (CheckBox)e.Item.FindControl("SetDefaultOfficeCheckBox");
                checkbox.Text = CodeLocalise("SetDefaultOfficeCheckBox.Label", "Set default office");
                checkbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistOfficesSetDefault);

                checkbox = (CheckBox)e.Item.FindControl("ViewEditOfficesCheckBox");
                checkbox.Text = CodeLocalise("ViewEditOfficesCheckBox.Label", "View/edit offices");
                checkbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistOfficesViewEdit);
            }
            else
            {
                var manageOfficesPlaceHolder = (PlaceHolder)e.Item.FindControl("ManageOfficesPlaceHolder");
                manageOfficesPlaceHolder.Visible = false;
            }

            #endregion

            #region Manage Reports

            if (App.Settings.ReportingEnabled)
            {
                BindCheckBox(staffMembersRoles, e, "JobSeekerReportsCheckBox", CodeLocalise("Global.ViewAndExport", "View &amp; Export"), Constants.RoleKeys.AssistJobSeekerReports);
                BindCheckBox(staffMembersRoles, e, "JobOrderReportsCheckBox", CodeLocalise("Global.ViewAndExport", "View &amp; Export"), Constants.RoleKeys.AssistJobOrderReports);
                BindCheckBox(staffMembersRoles, e, "EmployerReportsCheckBox", CodeLocalise("Global.ViewAndExport", "View &amp; Export"), Constants.RoleKeys.AssistEmployerReports);

                BindCheckBox(staffMembersRoles, e, "JobSeekerReportsViewOnlyCheckBox", CodeLocalise("Global.View", "View"), Constants.RoleKeys.AssistJobSeekerReportsViewOnly);
                BindCheckBox(staffMembersRoles, e, "JobOrderReportsViewOnlyCheckBox", CodeLocalise("Global.View", "View"), Constants.RoleKeys.AssistJobOrderReportsViewOnly);
                BindCheckBox(staffMembersRoles, e, "EmployerReportsViewOnlyCheckBox", CodeLocalise("Global.View", "View"), Constants.RoleKeys.AssistEmployerReportsViewOnly);
            }
            else
            {
                e.Item.FindControl("ManageReportsPlaceHolder").Visible = false;
            }

            #endregion

            #region Manage Staff

            var manageStaffCheckbox = (CheckBox)e.Item.FindControl("ManageStaffViewCheckBox");
            manageStaffCheckbox.Text = CodeLocalise("ManageStaffViewCheckBox.Label", "View");
            manageStaffCheckbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistStaffView);
            manageStaffCheckbox.Enabled = !accountAdministratorCheckBox.Checked;

            manageStaffCheckbox = (CheckBox)e.Item.FindControl("ManageStaffResetPasswordCheckBox");
            manageStaffCheckbox.Text = CodeLocalise("ManageStaffResetPasswordCheckBox.Label", "Reset password");
            manageStaffCheckbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistStaffResetPassword);
            manageStaffCheckbox.Enabled = !accountAdministratorCheckBox.Checked;

            manageStaffCheckbox = (CheckBox)e.Item.FindControl("ManageStaffActivateCheckBox");
            manageStaffCheckbox.Text = CodeLocalise("ManageStaffActivateCheckBox.Label", "Activate/deactivate accounts");
            manageStaffCheckbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistStaffDeactivateActivate);
            manageStaffCheckbox.Enabled = !accountAdministratorCheckBox.Checked;

            manageStaffCheckbox = (CheckBox)e.Item.FindControl("ManageStaffDetailsCheckBox");
            manageStaffCheckbox.Text = CodeLocalise("ManageStaffDetailsCheckBox.Label", "Staff details");
            manageStaffCheckbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistStaffDetails);
            manageStaffCheckbox.Enabled = !accountAdministratorCheckBox.Checked;

            manageStaffCheckbox = (CheckBox)e.Item.FindControl("ManageUpdatePermissionsCheckbox");
            manageStaffCheckbox.Text = CodeLocalise("ManageUpdatePermissionsCheckbox.Label", "Update Permissions");
            manageStaffCheckbox.Checked = staffMembersRoles.Any(r => r.Key == Constants.RoleKeys.AssistUpdatePermissions);
            manageStaffCheckbox.Enabled = !accountAdministratorCheckBox.Checked;

            #endregion

            #region TextBoxBackdateDelete

            var manageJobSeekersActivityLogBackdateEntryTextBox = (TextBox)e.Item.FindControl("ManageJobSeekersActivityLogBackdateEntryTextBox");
            manageJobSeekersActivityLogBackdateEntryTextBox.Text = staffMemberBackdateSettings.IsNotNull() ? (staffMemberBackdateSettings.BackdateAnyEntryJSActivityLogMaxDays.IsNotNull() ? staffMemberBackdateSettings.BackdateAnyEntryJSActivityLogMaxDays.ToString() : "0") : "0";
            manageJobSeekersActivityLogBackdateEntryTextBox.Enabled = (manageJobSeekersActivityLogBackdateEntryCheckBox.Checked && manageJobSeekersActivityLogBackdateEntryCheckBox.Enabled) ? true : false;

            var manageJobSeekersActivityLogBackdateEntriesTextBox = (TextBox)e.Item.FindControl("ManageJobSeekersActivityLogBackdateEntriesTextBox");
            manageJobSeekersActivityLogBackdateEntriesTextBox.Text = staffMemberBackdateSettings.IsNotNull() ? (staffMemberBackdateSettings.BackdateMyEntriesJSActivityLogMaxDays.IsNotNull() ? staffMemberBackdateSettings.BackdateMyEntriesJSActivityLogMaxDays.ToString() : "0") : "0";
            manageJobSeekersActivityLogBackdateEntriesTextBox.Enabled = (manageJobSeekersActivityLogBackdateEntriesCheckBox.Checked && manageJobSeekersActivityLogBackdateEntriesCheckBox.Enabled) ? true : false;

            var manageJobSeekersActivityLogDeleteEntryTextBox = (TextBox)e.Item.FindControl("ManageJobSeekersActivityLogDeleteEntryTextBox");
            manageJobSeekersActivityLogDeleteEntryTextBox.Text = staffMemberBackdateSettings.IsNotNull() ? (staffMemberBackdateSettings.DeleteAnyEntryJSActivityLogMaxDays.IsNotNull() ? staffMemberBackdateSettings.DeleteAnyEntryJSActivityLogMaxDays.ToString() : "0") : "0";
            manageJobSeekersActivityLogDeleteEntryTextBox.Enabled = (manageJobSeekersActivityLogDeleteEntryCheckBox.Checked && manageJobSeekersActivityLogDeleteEntryCheckBox.Enabled) ? true : false;


            var manageJobSeekersActivityLogDeleteEntriesTextBox = (TextBox)e.Item.FindControl("ManageJobSeekersActivityLogDeleteEntriesTextBox");
            manageJobSeekersActivityLogDeleteEntriesTextBox.Text = staffMemberBackdateSettings.IsNotNull() ? (staffMemberBackdateSettings.DeleteMyEntriesJSActivityLogMaxDays.IsNotNull() ? staffMemberBackdateSettings.DeleteMyEntriesJSActivityLogMaxDays.ToString() : "0") : "0";
            manageJobSeekersActivityLogDeleteEntriesTextBox.Enabled = (manageJobSeekersActivityLogDeleteEntriesCheckBox.Checked && manageJobSeekersActivityLogDeleteEntriesCheckBox.Enabled) ? true : false;

            var manageEmployersActivityLogBackdateEntryTextBox = (TextBox)e.Item.FindControl("ManageEmployersActivityLogBackdateEntryTextBox");
            manageEmployersActivityLogBackdateEntryTextBox.Text = staffMemberBackdateSettings.IsNotNull() ? (staffMemberBackdateSettings.BackdateAnyEntryHMActivityLogMaxDays.IsNotNull() ? staffMemberBackdateSettings.BackdateAnyEntryHMActivityLogMaxDays.ToString() : "0") : "0";
            manageEmployersActivityLogBackdateEntryTextBox.Enabled = (manageEmployersActivityLogBackdateEntryCheckBox.Checked && manageEmployersActivityLogBackdateEntryCheckBox.Enabled) ? true : false;

            var manageEmployersActivityLogBackdateEntriesTextBox = (TextBox)e.Item.FindControl("ManageEmployersActivityLogBackdateEntriesTextBox");
            manageEmployersActivityLogBackdateEntriesTextBox.Text = staffMemberBackdateSettings.IsNotNull() ? (staffMemberBackdateSettings.BackdateMyEntriesHMActivityLogMaxDays.IsNotNull() ? staffMemberBackdateSettings.BackdateMyEntriesHMActivityLogMaxDays.ToString() : "0") : "0";
            manageEmployersActivityLogBackdateEntriesTextBox.Enabled = (manageEmployersActivityLogBackdateEntriesCheckBox.Checked && manageEmployersActivityLogBackdateEntriesCheckBox.Enabled) ? true : false;

            var manageEmployersActivityLogDeleteEntryTextBox = (TextBox)e.Item.FindControl("ManageEmployersActivityLogDeleteEntryTextBox");
            manageEmployersActivityLogDeleteEntryTextBox.Text = staffMemberBackdateSettings.IsNotNull() ? (staffMemberBackdateSettings.DeleteAnyEntryHMActivityLogMaxDays.IsNotNull() ? staffMemberBackdateSettings.DeleteAnyEntryHMActivityLogMaxDays.ToString() : "0") : "0";
            manageEmployersActivityLogDeleteEntryTextBox.Enabled = (manageEmployersActivityLogDeleteEntryCheckBox.Checked && manageEmployersActivityLogDeleteEntryCheckBox.Enabled) ? true : false;

            var manageEmployersActivityLogDeleteEntriesTextBox = (TextBox)e.Item.FindControl("ManageEmployersActivityLogDeleteEntriesTextBox");
            manageEmployersActivityLogDeleteEntriesTextBox.Text = staffMemberBackdateSettings.IsNotNull() ? (staffMemberBackdateSettings.DeleteMyEntriesHMActivityLogMaxDays.IsNotNull() ? staffMemberBackdateSettings.DeleteMyEntriesHMActivityLogMaxDays.ToString() : "0") : "0";
            manageEmployersActivityLogDeleteEntriesTextBox.Enabled = (manageEmployersActivityLogDeleteEntriesCheckBox.Checked && manageEmployersActivityLogDeleteEntriesCheckBox.Enabled) ? true : false;

            #endregion
        }

        /// <summary>
        /// Checks/Unchecks a checkbox on the grid
        /// </summary>
        /// <param name="staffMembersRoles">The staff members roles.</param>
        /// <param name="e">The event data.</param>
        /// <param name="checkboxId">The checkbox id.</param>
        /// <param name="text">The text.</param>
        /// <param name="key">The role key.</param>
        private void BindCheckBox(IEnumerable<RoleDto> staffMembersRoles, ListViewItemEventArgs e, string checkboxId, string text, string key)
        {
            var checkbox = (CheckBox)e.Item.FindControl(checkboxId);
            checkbox.Text = text;
            checkbox.Checked = staffMembersRoles.Any(r => r.Key == key);
        }

        /// <summary>
        /// Handles the ItemCommand event of the StaffList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewCommandEventArgs"/> instance containing the event data.</param>
        protected void StaffList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            string[] ids = { "", "" };
            var staffUserId = Convert.ToInt64(0);
            var staffPersonId = Convert.ToInt64(0);

            if (e.CommandName == "UpdateRoles")
                ids = e.CommandArgument.ToString().Split(new[] { ',' });
            else if (e.CommandName == "DeactivateAccount" || e.CommandName == "ReactivateAccount")
                staffPersonId = GetStaffPersonId(e.CommandArgument);
            else
                staffUserId = GetStaffUserId(e.CommandArgument);



            switch (e.CommandName)
            {
                case "UpdateRoles":

                    if (UpdateUserRoles(ids, e.Item))
                        BindStaffList();
                    break;

                case "ResetPassword":
                    ActionConfirmationModal.Show(CodeLocalise("ResetPassword.Title", "Reset password"),
                                                        CodeLocalise("ResetPassword.Body", "Reset this user's password and automatically email them a link they can use to change their password?"),
                                                        CodeLocalise("Global.Cancel.Text", "Cancel"),
                                                        okText: CodeLocalise("ResetPassword.Ok.Text", "Reset password"), okCommandName: "ResetPassword", okCommandArgument: staffUserId.ToString());
                    break;

                case "EditStaffAccount":
                    EditStaffAccountModal.Show(staffUserId);
                    break;

                case "DeactivateAccount":
                    DeactivateAccount(staffPersonId);
                    break;

                case "ReactivateAccount":
                    ReactivateAccount(staffPersonId);
                    break;

                case "AddWorkplace":
                    AddPersonOffice(e.Item);
                    break;
            }
        }

        /// <summary>
        /// Handles the OkCommand event of the ResetPasswordModal control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        protected void ActionConfirmationModal_OkCommand(object sender, CommandEventArgs e)
        {
            var userId = GetStaffUserId(e.CommandArgument);

            switch (e.CommandName)
            {
                case "ResetPassword":
                    ResetPassword(Convert.ToInt64(e.CommandArgument));
                    break;
            }

            BindStaffList();
        }

        #endregion

        #region Bind / Unbind Methods

        /// <summary>
        /// Binds the staff list.
        /// </summary>
        private void BindStaffList()
        {
            StaffList.DataBind();

            // Set visibility of other controls
            StaffSearchHeaderTable.Visible = true;
            SearchResultListPager.Visible = (SearchResultListPager.TotalRowCount > 0);
            ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} results found", SearchResultListPager.TotalRowCount.ToString());
        }

        /// <summary>
        /// Unbinds the search.
        /// </summary>
        private void UnbindSearch()
        {
            StaffCriteria = new StaffCriteria();

            if (FirstNameTextBox.TextTrimmed().IsNotNullOrEmpty())
                StaffCriteria.Firstname = FirstNameTextBox.TextTrimmed();

            if (LastNameTextBox.TextTrimmed().IsNotNullOrEmpty())
                StaffCriteria.Lastname = LastNameTextBox.TextTrimmed();

            if (EmailAddressTextBox.TextTrimmed().IsNotNullOrEmpty())
                StaffCriteria.EmailAddress = EmailAddressTextBox.TextTrimmed();

            if (OfficeDropDown.SelectedIndex > 0)
                StaffCriteria.OfficeId = OfficeDropDown.SelectedValueToNullableLong();

            if (AccountStatusDropDown.SelectedValueToEnum<AccountStatusFilter>() == AccountStatusFilter.Active)
                StaffCriteria.Blocked = false;

            if (AccountStatusDropDown.SelectedValueToEnum<AccountStatusFilter>() == AccountStatusFilter.Inactive)
                StaffCriteria.Blocked = true;
        }

        /// <summary>
        /// Binds the office drop down.
        /// </summary>
        /// <param name="officeDropDown">The office drop down.</param>
        private void BindOfficeDropDown(DropDownList officeDropDown)
        {
            officeDropDown.Items.Clear();

            officeDropDown.DataSource = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { InActive = false });
            officeDropDown.DataValueField = "Id";
            officeDropDown.DataTextField = "OfficeName";
            officeDropDown.DataBind();

            officeDropDown.Items.AddLocalisedTopDefault("OfficeDropDownList.TopDefault.NoEdit", "- select an office -");
        }

        /// <summary>
        /// Binds the account status drop down.
        /// </summary>
        private void BindAccountStatusDropDown()
        {
            AccountStatusDropDown.Items.Clear();
            AccountStatusDropDown.Items.AddEnum(AccountStatusFilter.Active);
            AccountStatusDropDown.Items.AddEnum(AccountStatusFilter.Inactive);
            AccountStatusDropDown.Items.AddEnum(AccountStatusFilter.Both);

            AccountStatusDropDown.SelectValue(AccountStatusFilter.Active.ToString());
        }


        /// <summary>
        /// Binds the person offices.
        /// </summary>
        /// <param name="personOfficesControl">The assigned offices user control.</param>
        /// <param name="personId">The person id.</param>
        /// <param name="item">The item.</param>
        private void BindPersonOffices(AssignedOffices personOfficesControl, long personId, ListViewItem item)
        {
            var isStatewide = ServiceClientLocator.EmployerClient(App).IsPersonStatewide(personId);
            personOfficesControl.ContextId = personId;
            personOfficesControl.Bind();

            if (!isStatewide)
            {
                #region Select offices radio button

                var officeRadioButtonList = (RadioButtonList)item.FindControl("OfficeRadioButtonList");
                officeRadioButtonList.SelectedValue = "2";

                #endregion
            }
            else
            {
                #region Bind current offices info

                var issuesLiteral = (Literal)item.FindControl("CurrentOfficeListLiteral");
                issuesLiteral.Text = CodeLocalise("CurrentOfficeListLiteral.Statewide", "Statewide");

                #endregion

                #region Select statewide radiobutton

                var officeRadioButtonList = (RadioButtonList)item.FindControl("OfficeRadioButtonList");
                officeRadioButtonList.SelectedValue = "1";

                #endregion

                #region Hide office controls

                var officesRow = item.FindControl("OfficesRow") as HtmlTableRow;
                officesRow.Attributes.Add("style", "display:none");

                #endregion
            }
        }

        protected void PersonOfficesControlOnAssignedOfficesDataBinding(object sender, EventArgs eventArgs)
        {
            var personOfficesControl = (AssignedOffices)sender;
            var personOffices = personOfficesControl.AssignedOfficeList;
            if (personOffices.Count > 0)
            {
                var issuesLiteral = (Literal)personOfficesControl.NamingContainer.FindControl("CurrentOfficeListLiteral");
                issuesLiteral.Text = string.Join(", ", personOffices.Select(x => x.OfficeName).Take(3));

                if (personOffices.Count > 3)
                {
                    var showAdditionalOfficesLink = (HyperLink)personOfficesControl.NamingContainer.FindControl("ShowAdditionalOfficesLink");
                    showAdditionalOfficesLink.Visible = true;
                    showAdditionalOfficesLink.Text = CodeLocalise("ShowAdditionalOfficesLink.Text", " + {0} more",
                                                                                                                (personOffices.Count - 3));

                }
            }
        }

        #endregion

        /// <summary>
        /// Gets the staff user id.
        /// </summary>
        /// <param name="commandArgument">The command argument.</param>
        /// <returns></returns>
        private static long GetStaffUserId(object commandArgument)
        {
            long staffUserId;
            long.TryParse(commandArgument.ToString(), out staffUserId);

            return staffUserId;
        }

        /// <summary>
        /// Gets the staff person id.
        /// </summary>
        /// <param name="commandArgument">The command argument.</param>
        /// <returns></returns>
        private static long GetStaffPersonId(object commandArgument)
        {
            long staffPersonId;
            long.TryParse(commandArgument.ToString(), out staffPersonId);

            return staffPersonId;
        }

        /// <summary>
        /// Updates the user roles.
        /// </summary>
        /// <param name="ids">The user id.</param>
        /// <param name="updatedSearchItem">The updated search item.</param>
        /// <remarks>A boolean indicating success, or a validation failure</remarks>
        private bool UpdateUserRoles(string[] ids, ListViewItem updatedSearchItem)
        {


            #region BackdateDeletedeclaration
            var manageJobSeekersActionMenuCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersActionMenuCheckBox");
            var manageJobSeekersActivityLogBackdateEntryCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersActivityLogBackdateEntryCheckBox");
            var manageJobSeekersActivityLogBackdateEntryTextBox = (TextBox)updatedSearchItem.FindControl("ManageJobSeekersActivityLogBackdateEntryTextBox");
            var manageJobSeekersActivityLogBackdateEntriesCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersActivityLogBackdateEntriesCheckBox");
            var manageJobSeekersActivityLogBackdateEntriesTextBox = (TextBox)updatedSearchItem.FindControl("ManageJobSeekersActivityLogBackdateEntriesTextBox");
            var manageJobSeekersActivityLogDeleteEntryCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersActivityLogDeleteEntryCheckBox");
            var manageJobSeekersActivityLogDeleteEntryTextBox = (TextBox)updatedSearchItem.FindControl("ManageJobSeekersActivityLogDeleteEntryTextBox");
            var manageJobSeekersActivityLogDeleteEntriesCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersActivityLogDeleteEntriesCheckBox");
            var manageJobSeekersActivityLogDeleteEntriesTextBox = (TextBox)updatedSearchItem.FindControl("ManageJobSeekersActivityLogDeleteEntriesTextBox");
            var manageEmployersActionMenuCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployersActionMenuCheckBox");
            var manageEmployersActivityLogBackdateEntryCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployersActivityLogBackdateEntryCheckBox");
            var manageEmployersActivityLogBackdateEntryTextBox = (TextBox)updatedSearchItem.FindControl("ManageEmployersActivityLogBackdateEntryTextBox");
            var manageEmployersActivityLogBackdateEntriesCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployersActivityLogBackdateEntriesCheckBox");
            var manageEmployersActivityLogBackdateEntriesTextBox = (TextBox)updatedSearchItem.FindControl("ManageEmployersActivityLogBackdateEntriesTextBox");
            var manageEmployersActivityLogDeleteEntryCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployersActivityLogDeleteEntryCheckBox");
            var manageEmployersActivityLogDeleteEntryTextBox = (TextBox)updatedSearchItem.FindControl("ManageEmployersActivityLogDeleteEntryTextBox");
            var manageEmployersActivityLogDeleteEntriesCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployersActivityLogDeleteEntriesCheckBox");
            var manageEmployersActivityLogDeleteEntriesTextBox = (TextBox)updatedSearchItem.FindControl("ManageEmployersActivityLogDeleteEntriesTextBox");
            var officeRadioButtonList = (RadioButtonList)updatedSearchItem.FindControl("OfficeRadioButtonList");
            var managerCheckBox = (CheckBox)updatedSearchItem.FindControl("ManagerCheckBox");
            var manageSeekerAccountsPanelExtender = (CollapsiblePanelExtender)updatedSearchItem.FindControl("ManageSeekerAccountsPanelExtender");
            var manageOfficesPanelExtender = (CollapsiblePanelExtender)updatedSearchItem.FindControl("ManageOfficesPanelExtender");
            var manageStaffPanelExtender = (CollapsiblePanelExtender)updatedSearchItem.FindControl("ManageStaffPanelExtender");
            var manageEmployerAccountsPanelExtender = (CollapsiblePanelExtender)updatedSearchItem.FindControl("ManageEmployerAccountsPanelExtender");
            var manageJobSeekersActivityLogBackdateEntryCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageJobSeekersActivityLogBackdateEntryCustomValidator");
            var manageJobSeekersActivityLogDeleteEntryCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageJobSeekersActivityLogDeleteEntryCustomValidator");
            var manageEmployersActivityLogBackdateEntryCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageEmployersActivityLogBackdateEntryCustomValidator");
            var manageEmployersActivityLogDeleteEntryCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageEmployersActivityLogDeleteEntryCustomValidator");
            var manageEmployersActivityLogDeleteEntriesCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageEmployersActivityLogDeleteEntriesCustomValidator");
            var manageEmployersActivityLogBackdateEntriesCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageEmployersActivityLogBackdateEntriesCustomValidator");
            var manageJobSeekersActivityLogDeleteEntriesCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageJobSeekersActivityLogDeleteEntriesCustomValidator");
            var manageJobSeekersActivityLogBackdateEntriesCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageJobSeekersActivityLogBackdateEntriesCustomValidator");


            var assistAssignBusinessOfficeFromDefaultCheckBox = (CheckBox)updatedSearchItem.FindControl("AssistAssignBusinessOfficeFromDefaultCheckBox");
            var assignOfficeFromDefaultCheckBox = (CheckBox)updatedSearchItem.FindControl("AssignOfficeFromDefaultCheckBox");
            var systemAdministratorCheckBox = (CheckBox)updatedSearchItem.FindControl("SystemAdministratorCheckBox");
            var accountAdministratorCheckBox = (CheckBox)updatedSearchItem.FindControl("AccountAdministratorCheckBox");

            var assistAssignBusinessOfficeFromDefaultCustomValidator = (CustomValidator)updatedSearchItem.FindControl("AssistAssignBusinessOfficeFromDefaultCustomValidator");
            var assignOfficeFromDefaultCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("AssignOfficeFromDefaultCheckBoxCustomValidator");

            var createOfficesCheckBox = (CheckBox)updatedSearchItem.FindControl("CreateOfficesCheckBox");
            var activateDeactivateCheckbox = (CheckBox)updatedSearchItem.FindControl("ActivateDeactivateCheckbox");
            var listStaffCheckBox = (CheckBox)updatedSearchItem.FindControl("ListStaffCheckBox");
            var setDefaultOfficeCheckBox = (CheckBox)updatedSearchItem.FindControl("SetDefaultOfficeCheckBox");
            var viewEditOfficesCheckBox = (CheckBox)updatedSearchItem.FindControl("ViewEditOfficesCheckBox");
            var manageStaffViewCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageStaffViewCheckBox");
            var manageStaffResetPasswordCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageStaffResetPasswordCheckBox");
            var manageStaffActivateCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageStaffActivateCheckBox");
            var manageStaffDetailsCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageStaffDetailsCheckBox");
            var manageUpdatePermissionsCheckbox = (CheckBox)updatedSearchItem.FindControl("ManageUpdatePermissionsCheckbox");

            var createOfficesCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("CreateOfficesCheckBoxCustomValidator");
            var activateDeactivateCheckboxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ActivateDeactivateCheckboxCustomValidator");
            var listStaffCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ListStaffCheckBoxCustomValidator");
            var setDefaultOfficeCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("SetDefaultOfficeCheckBoxCustomValidator");
            var viewEditOfficesCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ViewEditOfficesCheckBoxCustomValidator");
            var manageStaffViewCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageStaffViewCheckBoxCustomValidator");
            var manageStaffResetPasswordCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageStaffResetPasswordCheckBoxCustomValidator");
            var manageStaffActivateCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageStaffActivateCheckBoxCustomValidator");
            var manageStaffDetailsCheckBoxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageStaffDetailsCheckBoxCustomValidator");
            var manageUpdatePermissionsCheckboxCustomValidator = (CustomValidator)updatedSearchItem.FindControl("ManageUpdatePermissionsCheckboxCustomValidator");

            #endregion

            #region statewide privilege and Non-Manager Validation
            //employer backdate
            if (manageEmployersActivityLogBackdateEntryCheckBox.Checked)
            {
                if (officeRadioButtonList.SelectedValue == "1" || managerCheckBox.Checked)
                {
                    //FVN-7830
                    if (manageEmployersActivityLogBackdateEntryTextBox.TextTrimmed().Equals("0"))
                    {
                        manageEmployersActivityLogBackdateEntryCustomValidator.IsValid = false;
                        manageEmployersActivityLogBackdateEntryCustomValidator.Text = HtmlLocalise("manageEmployersActivityLogBackdateEntryCustomValidator.Error", "Maximum number of days is required");
                        manageEmployerAccountsPanelExtender.Collapsed = false;
                        manageEmployerAccountsPanelExtender.ClientState = "false";
                    }
                    else
                    {
                        manageEmployersActivityLogBackdateEntryCustomValidator.IsValid = true;
                    }
                }
                else
                {
                    manageEmployersActivityLogBackdateEntryCustomValidator.IsValid = false;
                    manageEmployersActivityLogBackdateEntryCustomValidator.Text = HtmlLocalise("manageEmployersActivityLogBackdateEntryCustomValidator.Error", "To hold this permission, the user must have statewide privileges OR hold a manager role for at least one office.");
                    manageEmployerAccountsPanelExtender.Collapsed = false;
                    manageEmployerAccountsPanelExtender.ClientState = "false";

                }
            }

            //employer Delete
            if (manageEmployersActivityLogDeleteEntryCheckBox.Checked)
            {
                if (officeRadioButtonList.SelectedValue == "1" || managerCheckBox.Checked)
                {
                    //FVN-7830
                    if (manageEmployersActivityLogDeleteEntryTextBox.TextTrimmed().Equals("0"))
                    {
                        manageEmployersActivityLogDeleteEntryCustomValidator.IsValid = false;
                        manageEmployersActivityLogDeleteEntryCustomValidator.Text = HtmlLocalise("manageEmployersActivityLogDeleteEntryCustomValidator.Error", "Maximum number of days is required.");
                        manageEmployerAccountsPanelExtender.Collapsed = false;
                        manageEmployerAccountsPanelExtender.ClientState = "false";
                    }
                    else
                    {
                        manageEmployersActivityLogDeleteEntryCustomValidator.IsValid = true;
                    }
                }
                else
                {
                    manageEmployersActivityLogDeleteEntryCustomValidator.IsValid = false;
                    manageEmployersActivityLogDeleteEntryCustomValidator.Text = HtmlLocalise("manageEmployersActivityLogDeleteEntryCustomValidator.Error", "To hold this permission, the user must have statewide privileges OR hold a manager role for at least one office.");
                    manageEmployerAccountsPanelExtender.Collapsed = false;
                    manageEmployerAccountsPanelExtender.ClientState = "false";

                }
            }

            //Jobseeker backdate
            if (manageJobSeekersActivityLogBackdateEntryCheckBox.Checked)
            {
                if (officeRadioButtonList.SelectedValue == "1" || managerCheckBox.Checked)
                {
                    //FVN-7830
                    if (manageJobSeekersActivityLogBackdateEntryTextBox.TextTrimmed().Equals("0"))
                    {
                        manageJobSeekersActivityLogBackdateEntryCustomValidator.IsValid = false;
                        manageJobSeekersActivityLogBackdateEntryCustomValidator.Text = HtmlLocalise("manageJobSeekersActivityLogBackdateEntryCustomValidator.Error", "Maximum number of days is required.");
                        manageSeekerAccountsPanelExtender.Collapsed = false;
                        manageSeekerAccountsPanelExtender.ClientState = "false";
                    }
                    else
                    {
                        manageJobSeekersActivityLogBackdateEntryCustomValidator.IsValid = true;
                    }

                }
                else
                {
                    manageJobSeekersActivityLogBackdateEntryCustomValidator.IsValid = false;
                    manageJobSeekersActivityLogBackdateEntryCustomValidator.Text = HtmlLocalise("manageJobSeekersActivityLogBackdateEntryCustomValidator.Error", "To hold this permission, the user must have statewide privileges OR hold a manager role for at least one office.");
                    manageSeekerAccountsPanelExtender.Collapsed = false;
                    manageSeekerAccountsPanelExtender.ClientState = "false";

                }

            }

            //Jobseeker Delete
            if (manageJobSeekersActivityLogDeleteEntryCheckBox.Checked)
            {
                if (officeRadioButtonList.SelectedValue == "1" || managerCheckBox.Checked)
                {
                    //FVN-7830
                    if (manageJobSeekersActivityLogDeleteEntryTextBox.TextTrimmed().Equals("0"))
                    {
                        manageJobSeekersActivityLogDeleteEntryCustomValidator.IsValid = false;
                        manageJobSeekersActivityLogDeleteEntryCustomValidator.Text = HtmlLocalise("manageJobSeekersActivityLogDeleteEntryCustomValidator.Error", "Maximum number of days is required.");
                        manageSeekerAccountsPanelExtender.Collapsed = false;
                        manageSeekerAccountsPanelExtender.ClientState = "false";
                    }
                    else
                    {
                        manageJobSeekersActivityLogDeleteEntryCustomValidator.IsValid = true;
                    }
                }
                else
                {
                    manageJobSeekersActivityLogDeleteEntryCustomValidator.IsValid = false;
                    manageJobSeekersActivityLogDeleteEntryCustomValidator.Text = HtmlLocalise("manageJobSeekersActivityLogDeleteEntryCustomValidator.Error", "To hold this permission, the user must have statewide privileges OR hold a manager role for at least one office.");
                    manageSeekerAccountsPanelExtender.Collapsed = false;
                    manageSeekerAccountsPanelExtender.ClientState = "false";

                }

            }


            #endregion
            #region Validate Backdate/Delete textBoxes value
            if (manageEmployersActivityLogDeleteEntriesCheckBox.Checked && manageEmployersActivityLogDeleteEntriesTextBox.TextTrimmed().Equals("0"))
            {
                manageEmployersActivityLogDeleteEntriesCustomValidator.IsValid = false;
                manageEmployersActivityLogDeleteEntriesCustomValidator.Text = HtmlLocalise("manageEmployersActivityLogDeleteEntriesCustomValidator.Error", "Maximum number of days is required.");
                manageEmployerAccountsPanelExtender.Collapsed = false;
                manageEmployerAccountsPanelExtender.ClientState = "false";
            }
            if (manageEmployersActivityLogBackdateEntriesCheckBox.Checked && manageEmployersActivityLogBackdateEntriesTextBox.TextTrimmed().Equals("0"))
            {
                manageEmployersActivityLogBackdateEntriesCustomValidator.IsValid = false;
                manageEmployersActivityLogBackdateEntriesCustomValidator.Text = HtmlLocalise("manageEmployersActivityLogBackdateEntriesCustomValidator.Error", "Maximum number of days is required.");
                manageEmployerAccountsPanelExtender.Collapsed = false;
                manageEmployerAccountsPanelExtender.ClientState = "false";
            }
            if (manageJobSeekersActivityLogBackdateEntriesCheckBox.Checked && manageJobSeekersActivityLogBackdateEntriesTextBox.TextTrimmed().Equals("0"))
            {
                manageJobSeekersActivityLogBackdateEntriesCustomValidator.IsValid = false;
                manageJobSeekersActivityLogBackdateEntriesCustomValidator.Text = HtmlLocalise("manageJobSeekersActivityLogBackdateEntriesCustomValidator.Error", "Maximum number of days is required.");
                manageSeekerAccountsPanelExtender.Collapsed = false;
                manageSeekerAccountsPanelExtender.ClientState = "false";
            }
            if (manageJobSeekersActivityLogDeleteEntriesCheckBox.Checked && manageJobSeekersActivityLogDeleteEntriesTextBox.TextTrimmed().Equals("0"))
            {
                manageJobSeekersActivityLogDeleteEntriesCustomValidator.IsValid = false;
                manageJobSeekersActivityLogDeleteEntriesCustomValidator.Text = HtmlLocalise("manageJobSeekersActivityLogDeleteEntriesCustomValidator.Error", "Maximum number of days is required.");
                manageSeekerAccountsPanelExtender.Collapsed = false;
                manageSeekerAccountsPanelExtender.ClientState = "false";
            }
            #endregion

            #region ValidateAnyEntryandMyEntry
            if (manageEmployersActivityLogDeleteEntryCheckBox.Checked && manageEmployersActivityLogDeleteEntriesCheckBox.Checked)
            {
                manageEmployersActivityLogDeleteEntriesCustomValidator.IsValid = false;
                manageEmployersActivityLogDeleteEntriesCustomValidator.Text = HtmlLocalise("manageEmployersActivityLogDeleteEntriesCustomValidator.Error", "Select delete permission for either “my entries” or “any entry,” but not both.");
                manageEmployerAccountsPanelExtender.Collapsed = false;
                manageEmployerAccountsPanelExtender.ClientState = "false";
            }
            if (manageEmployersActivityLogBackdateEntriesCheckBox.Checked && manageEmployersActivityLogBackdateEntryCheckBox.Checked)
            {
                manageEmployersActivityLogBackdateEntriesCustomValidator.IsValid = false;
                manageEmployersActivityLogBackdateEntriesCustomValidator.Text = HtmlLocalise("manageEmployersActivityLogBackdateEntriesCustomValidator.Error", "Select backdate permission for either “my entries” or “any entry,” but not both.");
                manageEmployerAccountsPanelExtender.Collapsed = false;
                manageEmployerAccountsPanelExtender.ClientState = "false";
            }
            if (manageJobSeekersActivityLogBackdateEntryCheckBox.Checked && manageJobSeekersActivityLogBackdateEntriesCheckBox.Checked)
            {
                manageJobSeekersActivityLogBackdateEntriesCustomValidator.IsValid = false;
                manageJobSeekersActivityLogBackdateEntriesCustomValidator.Text = HtmlLocalise("manageJobSeekersActivityLogBackdateEntriesCustomValidator.Error", "Select backdate permission for either “my entries” or “any entry,” but not both.");
                manageSeekerAccountsPanelExtender.Collapsed = false;
                manageSeekerAccountsPanelExtender.ClientState = "false";
            }
            if (manageJobSeekersActivityLogDeleteEntriesCheckBox.Checked && manageJobSeekersActivityLogDeleteEntryCheckBox.Checked)
            {
                manageJobSeekersActivityLogDeleteEntriesCustomValidator.IsValid = false;
                manageJobSeekersActivityLogDeleteEntriesCustomValidator.Text = HtmlLocalise("manageJobSeekersActivityLogDeleteEntriesCustomValidator.Error", "Select delete permission for either “my entries” or “any entry,” but not both.");
                manageSeekerAccountsPanelExtender.Collapsed = false;
                manageSeekerAccountsPanelExtender.ClientState = "false";
            }
            #endregion

            #region statewide/dedault office privilege for AssignOfficeFromDefault


            //Manage job seeker accounts
            if (assignOfficeFromDefaultCheckBox.Checked)
            {
                if (CheckDefaultOrStatewideOffice(updatedSearchItem) && systemAdministratorCheckBox.Checked)
                {
                    assignOfficeFromDefaultCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    assignOfficeFromDefaultCheckBoxCustomValidator.IsValid = false;
                    assignOfficeFromDefaultCheckBoxCustomValidator.Text = HtmlLocalise("AssignOfficeFromDefaultCheckBoxCustomValidator.Error", "Only System Administrators assigned to the Default office may have this permission.");
                    manageSeekerAccountsPanelExtender.Collapsed = false;
                    manageSeekerAccountsPanelExtender.ClientState = "false";
                }
            }

            //Manage business accounts & job postings
            if (assistAssignBusinessOfficeFromDefaultCheckBox.Checked)
            {
                if (CheckDefaultOrStatewideOffice(updatedSearchItem) && systemAdministratorCheckBox.Checked)
                {
                    assistAssignBusinessOfficeFromDefaultCustomValidator.IsValid = true;
                }
                else
                {
                    assistAssignBusinessOfficeFromDefaultCustomValidator.IsValid = false;
                    assistAssignBusinessOfficeFromDefaultCustomValidator.Text = HtmlLocalise("AssistAssignBusinessOfficeFromDefaultCustomValidator.Error", "Only System Administrators assigned to the Default office may have this permission.");
                    manageEmployerAccountsPanelExtender.Collapsed = false;
                    manageEmployerAccountsPanelExtender.ClientState = "false";
                }
            }

            #endregion

            #region validate Account Admin/System Admin privilege
            //Manage staff View
            if (manageStaffViewCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    manageStaffViewCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    manageStaffViewCheckBoxCustomValidator.IsValid = false;
                    manageStaffViewCheckBoxCustomValidator.Text = HtmlLocalise("ManageStaffViewCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageStaffPanelExtender.Collapsed = false;
                    manageStaffPanelExtender.ClientState = "false";

                }
            }

            //Manage staff - Reset password
            if (manageStaffResetPasswordCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    manageStaffResetPasswordCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    manageStaffResetPasswordCheckBoxCustomValidator.IsValid = false;
                    manageStaffResetPasswordCheckBoxCustomValidator.Text = HtmlLocalise("ManageStaffResetPasswordCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageStaffPanelExtender.Collapsed = false;
                    manageStaffPanelExtender.ClientState = "false";

                }
            }

            //Manage staff - Activate/deactivate accounts
            if (manageStaffActivateCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    manageStaffActivateCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    manageStaffActivateCheckBoxCustomValidator.IsValid = false;
                    manageStaffActivateCheckBoxCustomValidator.Text = HtmlLocalise("ManageStaffActivateCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageStaffPanelExtender.Collapsed = false;
                    manageStaffPanelExtender.ClientState = "false";

                }
            }

            //Manage staff - Activate/deactivate accounts
            if (manageStaffActivateCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    manageStaffActivateCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    manageStaffActivateCheckBoxCustomValidator.IsValid = false;
                    manageStaffActivateCheckBoxCustomValidator.Text = HtmlLocalise("ManageStaffActivateCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageStaffPanelExtender.Collapsed = false;
                    manageStaffPanelExtender.ClientState = "false";

                }
            }

            //Manage staff - Staff details
            if (manageStaffDetailsCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    manageStaffDetailsCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    manageStaffDetailsCheckBoxCustomValidator.IsValid = false;
                    manageStaffDetailsCheckBoxCustomValidator.Text = HtmlLocalise("ManageStaffDetailsCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageStaffPanelExtender.Collapsed = false;
                    manageStaffPanelExtender.ClientState = "false";

                }
            }

            //Manage staff - Update Permissions
            if (manageUpdatePermissionsCheckbox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    manageUpdatePermissionsCheckboxCustomValidator.IsValid = true;
                }
                else
                {
                    manageUpdatePermissionsCheckboxCustomValidator.IsValid = false;
                    manageUpdatePermissionsCheckboxCustomValidator.Text = HtmlLocalise("ManageUpdatePermissionsCheckboxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageStaffPanelExtender.Collapsed = false;
                    manageStaffPanelExtender.ClientState = "false";

                }
            }


            // Manage offices - Activate/Deactivate offices
            if (activateDeactivateCheckbox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    activateDeactivateCheckboxCustomValidator.IsValid = true;
                }
                else
                {
                    activateDeactivateCheckboxCustomValidator.IsValid = false;
                    activateDeactivateCheckboxCustomValidator.Text = HtmlLocalise("ActivateDeactivateCheckboxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageOfficesPanelExtender.Collapsed = false;
                    manageOfficesPanelExtender.ClientState = "false";

                }
            }

            // Manage offices - Create offices
            if (createOfficesCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    createOfficesCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    createOfficesCheckBoxCustomValidator.IsValid = false;
                    createOfficesCheckBoxCustomValidator.Text = HtmlLocalise("CreateOfficesCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageOfficesPanelExtender.Collapsed = false;
                    manageOfficesPanelExtender.ClientState = "false";

                }
            }

            // Manage offices - List staff
            if (listStaffCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    listStaffCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    listStaffCheckBoxCustomValidator.IsValid = false;
                    listStaffCheckBoxCustomValidator.Text = HtmlLocalise("ListStaffCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageOfficesPanelExtender.Collapsed = false;
                    manageOfficesPanelExtender.ClientState = "false";

                }
            }

            // Manage offices - Set default office
            if (setDefaultOfficeCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    setDefaultOfficeCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    setDefaultOfficeCheckBoxCustomValidator.IsValid = false;
                    setDefaultOfficeCheckBoxCustomValidator.Text = HtmlLocalise("SetDefaultOfficeCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageOfficesPanelExtender.Collapsed = false;
                    manageOfficesPanelExtender.ClientState = "false";

                }
            }

            // Manage offices - View/edit offices
            if (viewEditOfficesCheckBox.Checked)
            {
                if (accountAdministratorCheckBox.Checked || systemAdministratorCheckBox.Checked)
                {
                    viewEditOfficesCheckBoxCustomValidator.IsValid = true;
                }
                else
                {
                    viewEditOfficesCheckBoxCustomValidator.IsValid = false;
                    viewEditOfficesCheckBoxCustomValidator.Text = HtmlLocalise("ViewEditOfficesCheckBoxCustomValidator.Error", "This function is restricted to Account and System Administrators only.");
                    manageOfficesPanelExtender.Collapsed = false;
                    manageOfficesPanelExtender.ClientState = "false";

                }
            }

            #endregion

            if (!ValidateUserOffices(updatedSearchItem) || !manageJobSeekersActivityLogDeleteEntryCustomValidator.IsValid || !manageJobSeekersActivityLogBackdateEntryCustomValidator.IsValid || !manageEmployersActivityLogBackdateEntryCustomValidator.IsValid || !manageEmployersActivityLogDeleteEntryCustomValidator.IsValid || !manageEmployersActivityLogDeleteEntriesCustomValidator.IsValid || !manageJobSeekersActivityLogDeleteEntriesCustomValidator.IsValid
                || !manageJobSeekersActivityLogBackdateEntriesCustomValidator.IsValid || !manageEmployersActivityLogBackdateEntriesCustomValidator.IsValid
                || !assignOfficeFromDefaultCheckBoxCustomValidator.IsValid || !assistAssignBusinessOfficeFromDefaultCustomValidator.IsValid
                || !createOfficesCheckBoxCustomValidator.IsValid || !activateDeactivateCheckboxCustomValidator.IsValid || !listStaffCheckBoxCustomValidator.IsValid || !setDefaultOfficeCheckBoxCustomValidator.IsValid || !viewEditOfficesCheckBoxCustomValidator.IsValid || !manageStaffViewCheckBoxCustomValidator.IsValid || !manageStaffResetPasswordCheckBoxCustomValidator.IsValid || !manageStaffActivateCheckBoxCustomValidator.IsValid || !manageStaffDetailsCheckBoxCustomValidator.IsValid || !manageUpdatePermissionsCheckboxCustomValidator.IsValid)

            { return false; }


            var userId = Convert.ToInt64(ids[0]);

            var staffMembersRoles = ServiceClientLocator.AccountClient(App).GetUsersRoles(userId);
            var personId = Convert.ToInt64(ids[1]);

            #region Manage Job Seekers

            var manageJobSeekersReadOnlyCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersReadOnlyCheckBox");
            staffMembersRoles = UnbindCheckBox(manageJobSeekersReadOnlyCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekersReadOnly);

            var manageJobSeekersAdministratorCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersAdministratorCheckBox");
            staffMembersRoles = UnbindCheckBox(manageJobSeekersAdministratorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekersAdministrator);


            staffMembersRoles = UnbindCheckBox(manageJobSeekersActionMenuCheckBox, staffMembersRoles, Constants.RoleKeys.BackdateMyEntriesJSActionMenuMaxDays);


            staffMembersRoles = UnbindCheckBox(manageJobSeekersActivityLogBackdateEntryCheckBox, staffMembersRoles, Constants.RoleKeys.BackdateAnyEntryJSActivityLogMaxDays);


            staffMembersRoles = UnbindCheckBox(manageJobSeekersActivityLogBackdateEntriesCheckBox, staffMembersRoles, Constants.RoleKeys.BackdateMyEntriesJSActivityLogMaxDays);


            staffMembersRoles = UnbindCheckBox(manageJobSeekersActivityLogDeleteEntriesCheckBox, staffMembersRoles, Constants.RoleKeys.DeleteMyEntriesJSActivityLogMaxDays);


            staffMembersRoles = UnbindCheckBox(manageJobSeekersActivityLogDeleteEntryCheckBox, staffMembersRoles, Constants.RoleKeys.DeleteAnyEntryJSActivityLogMaxDays);

            //var manageJobSeekersOfficesAdministratorCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersOfficesAdministratorCheckBox");
            //staffMembersRoles = UnbindCheckBox(manageJobSeekersOfficesAdministratorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekerOfficeAdministrator);

            var manageJobSeekersAccountInactivatorCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersAccountInactivatorCheckBox");
            staffMembersRoles = UnbindCheckBox(manageJobSeekersAccountInactivatorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekersAccountInactivator);

            var manageJobSeekersAccountBlockerCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersAccountBlockerCheckBox");
            staffMembersRoles = UnbindCheckBox(manageJobSeekersAccountBlockerCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekersAccountBlocker);

            var manageJobSeekersSendMessagesCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersSendMessagesCheckBox");
            staffMembersRoles = UnbindCheckBox(manageJobSeekersSendMessagesCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekersSendMessages);

            var manageJobSeekersCreateNewAccountCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobSeekersCreateNewAccountCheckBox");
            staffMembersRoles = UnbindCheckBox(manageJobSeekersCreateNewAccountCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekersCreateNewAccount);


            staffMembersRoles = UnbindCheckBox(assignOfficeFromDefaultCheckBox, staffMembersRoles, Constants.RoleKeys.AssistAssignJobSeekerOfficeFromDefault);

            var reassignOfficeToNewOfficeCheckBox = (CheckBox)updatedSearchItem.FindControl("ReassignOfficeToNewOfficeCheckBox");
            staffMembersRoles = UnbindCheckBox(reassignOfficeToNewOfficeCheckBox, staffMembersRoles, Constants.RoleKeys.ReassignJobSeekerToNewOffice);

            #endregion

            #region Backdate Activities Setting

            var staffBackdateSettings = new PersonActivityUpdateSettingsDto();

            staffBackdateSettings.PersonId = personId;

            #region Job Seeker Backdate Max Days Settings

            if (manageJobSeekersActivityLogBackdateEntryCheckBox.Checked)
                staffBackdateSettings.BackdateAnyEntryJSActivityLogMaxDays = manageJobSeekersActivityLogBackdateEntryTextBox.Text.IsNotNullOrEmpty() ? Convert.ToInt64(manageJobSeekersActivityLogBackdateEntryTextBox.Text) : 0;

            if (manageJobSeekersActivityLogBackdateEntriesCheckBox.Checked)
                staffBackdateSettings.BackdateMyEntriesJSActivityLogMaxDays = manageJobSeekersActivityLogBackdateEntriesTextBox.Text.IsNotNullOrEmpty() ? Convert.ToInt64(manageJobSeekersActivityLogBackdateEntriesTextBox.Text) : 0;

            if (manageJobSeekersActivityLogDeleteEntryCheckBox.Checked)
                staffBackdateSettings.DeleteAnyEntryJSActivityLogMaxDays = manageJobSeekersActivityLogDeleteEntryTextBox.Text.IsNotNullOrEmpty() ? Convert.ToInt64(manageJobSeekersActivityLogDeleteEntryTextBox.Text) : 0;

            if (manageJobSeekersActivityLogDeleteEntriesCheckBox.Checked)
                staffBackdateSettings.DeleteMyEntriesJSActivityLogMaxDays = manageJobSeekersActivityLogDeleteEntriesTextBox.Text.IsNotNullOrEmpty() ? Convert.ToInt64(manageJobSeekersActivityLogDeleteEntriesTextBox.Text) : 0;
            #endregion


            #region Hiring Manager Backdate Max Days Settings

            if (manageEmployersActivityLogBackdateEntryCheckBox.Checked)
                staffBackdateSettings.BackdateAnyEntryHMActivityLogMaxDays = manageEmployersActivityLogBackdateEntryTextBox.Text.IsNotNullOrEmpty() ? Convert.ToInt64(manageEmployersActivityLogBackdateEntryTextBox.Text) : 0;

            if (manageEmployersActivityLogBackdateEntriesCheckBox.Checked)
                staffBackdateSettings.BackdateMyEntriesHMActivityLogMaxDays = manageEmployersActivityLogBackdateEntriesTextBox.Text.IsNotNullOrEmpty() ? Convert.ToInt64(manageEmployersActivityLogBackdateEntriesTextBox.Text) : 0;

            if (manageEmployersActivityLogDeleteEntriesCheckBox.Checked)
                staffBackdateSettings.DeleteMyEntriesHMActivityLogMaxDays = manageEmployersActivityLogDeleteEntriesTextBox.Text.IsNotNullOrEmpty() ? Convert.ToInt64(manageEmployersActivityLogDeleteEntriesTextBox.Text) : 0;

            if (manageEmployersActivityLogDeleteEntryCheckBox.Checked)
                staffBackdateSettings.DeleteAnyEntryHMActivityLogMaxDays = manageEmployersActivityLogDeleteEntryTextBox.Text.IsNotNullOrEmpty() ? Convert.ToInt64(manageEmployersActivityLogDeleteEntryTextBox.Text) : 0;
            #endregion

            ServiceClientLocator.StaffClient(App).UpdateStaffBackdateSettings(staffBackdateSettings);
            #endregion

            #region Manage Employers

            var manageEmployersReadOnlyCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployersReadOnlyCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployersReadOnlyCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployersReadOnly);

            var manageEmployersAdministratorCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployersAdministratorCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployersAdministratorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployersAdministrator);

            var manageEmployersAccountBlockerCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployersAccountBlockerCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployersAccountBlockerCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployersAccountBlocker);


            staffMembersRoles = UnbindCheckBox(manageEmployersActionMenuCheckBox, staffMembersRoles, Constants.RoleKeys.BackdateMyEntriesHMActionMenuMaxDays);


            staffMembersRoles = UnbindCheckBox(manageEmployersActivityLogBackdateEntryCheckBox, staffMembersRoles, Constants.RoleKeys.BackdateAnyEntryHMActivityLogMaxDays);


            staffMembersRoles = UnbindCheckBox(manageEmployersActivityLogBackdateEntriesCheckBox, staffMembersRoles, Constants.RoleKeys.BackdateMyEntriesHMActivityLogMaxDays);


            staffMembersRoles = UnbindCheckBox(manageEmployersActivityLogDeleteEntriesCheckBox, staffMembersRoles, Constants.RoleKeys.DeleteMyEntriesHMActivityLogMaxDays);


            staffMembersRoles = UnbindCheckBox(manageEmployersActivityLogDeleteEntryCheckBox, staffMembersRoles, Constants.RoleKeys.DeleteAnyEntryHMActivityLogMaxDays);

            var manageEmployerOverrideCriminalExclusionsCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployerOverrideCriminalExclusionsCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployerOverrideCriminalExclusionsCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployerOverrideCriminalExclusions);


            //staffMembersRoles = UnbindCheckBox(manageEmployersOfficesAdministratorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployerOfficeAdministrator);

            var manageJobDevelopmentCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobDevelopmentCheckBox");
            staffMembersRoles = UnbindCheckBox(manageJobDevelopmentCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployerJobDevelopmentManager);

            var manageEmployerSendMessagesCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployerSendMessagesCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployerSendMessagesCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployersSendMessages);

            var manageEmployerCreateNewAccountCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployerCreateNewAccountCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployerCreateNewAccountCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployersCreateNewAccount);

            var manageEmployerOverrideCreditCheckCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployerOverrideCreditCheckCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployerOverrideCreditCheckCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployerOverrideCreditCheckRequired);

            var manageEmployerReassignBusinessCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployerReassignBusinessCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployerReassignBusinessCheckBox, staffMembersRoles, Constants.RoleKeys.ReassignBusinessToNewOffice);

            //var assistAssignBusinessOfficeFromDefaultCheckBox = (CheckBox)updatedSearchItem.FindControl("AssistAssignBusinessOfficeFromDefaultCheckBox");
            staffMembersRoles = UnbindCheckBox(assistAssignBusinessOfficeFromDefaultCheckBox, staffMembersRoles, Constants.RoleKeys.AssistAssignBusinessOfficeFromDefault);

            var manageEmployerReassignJobPostingCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageEmployerReassignJobPostingCheckBox");
            staffMembersRoles = UnbindCheckBox(manageEmployerReassignJobPostingCheckBox, staffMembersRoles, Constants.RoleKeys.ReassignJobPostingToNewOffice);

            #endregion

            #region Manage Approval Queues

            var jobPostingsViewEditCheckBox = (CheckBox)updatedSearchItem.FindControl("JobPostingsViewEditCheckBox");
            staffMembersRoles = UnbindCheckBox(jobPostingsViewEditCheckBox, staffMembersRoles, Constants.RoleKeys.AssistPostingApprover);

            var jobPostingsViewCheckBox = (CheckBox)updatedSearchItem.FindControl("JobPostingsViewCheckBox");
            staffMembersRoles = UnbindCheckBox(jobPostingsViewCheckBox, staffMembersRoles, Constants.RoleKeys.AssistPostingApprovalViewer);

            var employerAccountsViewEditCheckBox = (CheckBox)updatedSearchItem.FindControl("EmployerAccountsViewEditCheckBox");
            staffMembersRoles = UnbindCheckBox(employerAccountsViewEditCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployerAccountApprover);

            var employerAccountsViewCheckBox = (CheckBox)updatedSearchItem.FindControl("EmployerAccountsViewCheckBox");
            staffMembersRoles = UnbindCheckBox(employerAccountsViewCheckBox, staffMembersRoles, Constants.RoleKeys.AssistEmployerAccountApprovalViewer);

            var referralViewEditCheckBox = (CheckBox)updatedSearchItem.FindControl("ReferralViewEditCheckBox");
            staffMembersRoles = UnbindCheckBox(referralViewEditCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekerReferralApprover);

            var referralViewCheckBox = (CheckBox)updatedSearchItem.FindControl("ReferralViewCheckBox");
            staffMembersRoles = UnbindCheckBox(referralViewCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer);

            if (App.Settings.Theme == FocusThemes.Workforce)
            {
                #region Special job orders

                var courtOrderedAffrimativeActionCheckBox = (CheckBox)updatedSearchItem.FindControl("CourtOrderedAffrimativeActionCheckBox");
                staffMembersRoles = UnbindCheckBox(courtOrderedAffrimativeActionCheckBox, staffMembersRoles, Constants.RoleKeys.AssistCourtOrderedAffirmativeActionApprover);

                var federalContractorCheckBox = (CheckBox)updatedSearchItem.FindControl("FederalContractorCheckBox");
                staffMembersRoles = UnbindCheckBox(federalContractorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistFederalContractorApprover);

                var foreignLabourH2AAgCheckBox = (CheckBox)updatedSearchItem.FindControl("ForeignLabourH2AAgCheckBox");
                staffMembersRoles = UnbindCheckBox(foreignLabourH2AAgCheckBox, staffMembersRoles, Constants.RoleKeys.AssistForeignLabourH2AAgApprover);

                var foreignLabourH2BNonAgCheckBox = (CheckBox)updatedSearchItem.FindControl("ForeignLabourH2BNonAgCheckBox");
                staffMembersRoles = UnbindCheckBox(foreignLabourH2BNonAgCheckBox, staffMembersRoles, Constants.RoleKeys.AssistForeignLabourH2BNonAgApprover);

                var foreignLabourOtherCheckBox = (CheckBox)updatedSearchItem.FindControl("ForeignLabourOtherCheckBox");
                staffMembersRoles = UnbindCheckBox(foreignLabourOtherCheckBox, staffMembersRoles, Constants.RoleKeys.AssistForeignLabourOtherApprover);

                var homeBasedCheckBox = (CheckBox)updatedSearchItem.FindControl("HomeBasedCheckBox");
                staffMembersRoles = UnbindCheckBox(homeBasedCheckBox, staffMembersRoles, Constants.RoleKeys.AssistHomeBasedApprover);

                var commissionOnlyCheckBox = (CheckBox)updatedSearchItem.FindControl("CommissionOnlyCheckBox");
                staffMembersRoles = UnbindCheckBox(commissionOnlyCheckBox, staffMembersRoles, Constants.RoleKeys.AssistCommissionOnlyApprover);

                var salaryAndCommissionCheckBox = (CheckBox)updatedSearchItem.FindControl("SalaryAndCommissionCheckBox");
                staffMembersRoles = UnbindCheckBox(salaryAndCommissionCheckBox, staffMembersRoles, Constants.RoleKeys.AssistSalaryAndCommissionApprover);

                #endregion
            }

            #endregion

            #region Manage Focus/Assist


            staffMembersRoles = UnbindCheckBox(accountAdministratorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistAccountAdministrator);

            var broadcastAdministratorCheckBox = (CheckBox)updatedSearchItem.FindControl("BroadcastAdministratorCheckBox");
            staffMembersRoles = UnbindCheckBox(broadcastAdministratorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistBroadcastsAdministrator);


            staffMembersRoles = UnbindCheckBox(systemAdministratorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistSystemAdministrator);

            #endregion

            #region Mange Job Orders

            //var manageJobOrdersOfficesAdministratorCheckBox = (CheckBox)updatedSearchItem.FindControl("ManageJobOrdersOfficesAdministratorCheckBox");
            //staffMembersRoles = UnbindCheckBox(manageJobOrdersOfficesAdministratorCheckBox, staffMembersRoles, Constants.RoleKeys.AssistJobOrderOfficeAdministrator);

            #endregion

            #region Offices Administration

            if (App.Settings.Theme == FocusThemes.Education)
            {
                var manageStudentRegistrationAdministratorCheckBox =
                  (CheckBox)updatedSearchItem.FindControl("ManageStudentAccountAdministratorCheckBox");
                staffMembersRoles = UnbindCheckBox(manageStudentRegistrationAdministratorCheckBox, staffMembersRoles,
                                                   Constants.RoleKeys.AssistStudentAccountAdministrator);
            }

            #endregion

            #region Manage Offices

            var checkbox = (CheckBox)updatedSearchItem.FindControl("ActivateDeactivateCheckbox");
            staffMembersRoles = UnbindCheckBox(checkbox, staffMembersRoles, Constants.RoleKeys.AssistOfficesActivateDeactivate);

            checkbox = (CheckBox)updatedSearchItem.FindControl("CreateOfficesCheckBox");
            staffMembersRoles = UnbindCheckBox(checkbox, staffMembersRoles, Constants.RoleKeys.AssistOfficesCreate);

            checkbox = (CheckBox)updatedSearchItem.FindControl("ListStaffCheckBox");
            staffMembersRoles = UnbindCheckBox(checkbox, staffMembersRoles, Constants.RoleKeys.AssistOfficesListStaff);

            checkbox = (CheckBox)updatedSearchItem.FindControl("SetDefaultOfficeCheckBox");
            staffMembersRoles = UnbindCheckBox(checkbox, staffMembersRoles, Constants.RoleKeys.AssistOfficesSetDefault);

            checkbox = (CheckBox)updatedSearchItem.FindControl("ViewEditOfficesCheckBox");
            staffMembersRoles = UnbindCheckBox(checkbox, staffMembersRoles, Constants.RoleKeys.AssistOfficesViewEdit);

            #endregion

            #region Manage Reports

            var reportsCheckbox = (CheckBox)updatedSearchItem.FindControl("JobSeekerReportsCheckBox");
            staffMembersRoles = UnbindCheckBox(reportsCheckbox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekerReports);

            reportsCheckbox = (CheckBox)updatedSearchItem.FindControl("JobOrderReportsCheckBox");
            staffMembersRoles = UnbindCheckBox(reportsCheckbox, staffMembersRoles, Constants.RoleKeys.AssistJobOrderReports);

            reportsCheckbox = (CheckBox)updatedSearchItem.FindControl("EmployerReportsCheckBox");
            staffMembersRoles = UnbindCheckBox(reportsCheckbox, staffMembersRoles, Constants.RoleKeys.AssistEmployerReports);

            reportsCheckbox = (CheckBox)updatedSearchItem.FindControl("JobSeekerReportsViewOnlyCheckBox");
            staffMembersRoles = UnbindCheckBox(reportsCheckbox, staffMembersRoles, Constants.RoleKeys.AssistJobSeekerReportsViewOnly);

            reportsCheckbox = (CheckBox)updatedSearchItem.FindControl("JobOrderReportsViewOnlyCheckBox");
            staffMembersRoles = UnbindCheckBox(reportsCheckbox, staffMembersRoles, Constants.RoleKeys.AssistJobOrderReportsViewOnly);

            reportsCheckbox = (CheckBox)updatedSearchItem.FindControl("EmployerReportsViewOnlyCheckBox");
            staffMembersRoles = UnbindCheckBox(reportsCheckbox, staffMembersRoles, Constants.RoleKeys.AssistEmployerReportsViewOnly);

            #endregion

            #region Manage Staff

            var staffCheckbox = (CheckBox)updatedSearchItem.FindControl("ManageStaffViewCheckBox");
            staffMembersRoles = UnbindStaffCheckBox(accountAdministratorCheckBox, staffCheckbox, staffMembersRoles, Constants.RoleKeys.AssistStaffView);

            staffCheckbox = (CheckBox)updatedSearchItem.FindControl("ManageStaffResetPasswordCheckBox");
            staffMembersRoles = UnbindStaffCheckBox(accountAdministratorCheckBox, staffCheckbox, staffMembersRoles, Constants.RoleKeys.AssistStaffResetPassword);

            staffCheckbox = (CheckBox)updatedSearchItem.FindControl("ManageStaffActivateCheckBox");
            staffMembersRoles = UnbindStaffCheckBox(accountAdministratorCheckBox, staffCheckbox, staffMembersRoles, Constants.RoleKeys.AssistStaffDeactivateActivate);

            staffCheckbox = (CheckBox)updatedSearchItem.FindControl("ManageStaffDetailsCheckBox");
            staffMembersRoles = UnbindStaffCheckBox(accountAdministratorCheckBox, staffCheckbox, staffMembersRoles, Constants.RoleKeys.AssistStaffDetails);

            staffCheckbox = (CheckBox)updatedSearchItem.FindControl("ManageUpdatePermissionsCheckbox");
            staffMembersRoles = UnbindStaffCheckBox(accountAdministratorCheckBox, staffCheckbox, staffMembersRoles, Constants.RoleKeys.AssistUpdatePermissions);

            //to-do updatepermission=true, isDefult or statewide
            //var manageUpdatePermissionsCheckbox = (CheckBox)updatedSearchItem.FindControl("ManageUpdatePermissionsCheckbox");

            //if (manageUpdatePermissionsCheckbox.Checked && !CheckDefaultOrStatewideOffice(updatedSearchItem))
            //{
            //    manageUpdatePermissionsCustomValidator.IsValid = false;
            //    manageUpdatePermissionsCustomValidator.Text = HtmlLocalise("manageUpdatePermissionsCustomValidator.Error", "Only Default office staff may have this permission.");
            //    manageStaffPanelExtender.Collapsed = false;
            //    manageStaffPanelExtender.ClientState = "false";
            //}

            #endregion
            //if (!manageUpdatePermissionsCustomValidator.IsValid)
            //{ return false; }


            ServiceClientLocator.AccountClient(App).UpdateUsersRoles(userId, staffMembersRoles);

            UpdateUserOffices(ids, updatedSearchItem);




            ServiceClientLocator.AccountClient(App).UpdateManagerFlag(personId, managerCheckBox.Checked);

            var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(userId);

            var lverCheckBox = (CheckBox)updatedSearchItem.FindControl("LverCheckBox");
            userDetails.PersonDetails.LocalVeteranEmploymentRepresentative = lverCheckBox.Checked;

            var dvopCheckBox = (CheckBox)updatedSearchItem.FindControl("DvopCheckBox");
            userDetails.PersonDetails.DisabledVeteransOutreachProgramSpecialist = dvopCheckBox.Checked;

            ServiceClientLocator.AccountClient(App).UpdateAssistUser(userDetails);

            if (userId == App.User.UserId)
            {
                App.UpdateRoles(userId);
                if (!App.User.IsInRole(Constants.RoleKeys.AssistAccountAdministrator))
                    Response.Redirect(UrlBuilder.Default(App.User, App));
            }

            Confirmation.Show(CodeLocalise("StaffRolesUpdatedConfirmation.Title", "Staff roles updated"),
                                                               CodeLocalise("StaffRolesUpdatedConfirmation.Body", "The roles for the staff account have been updated."),
                                                               CodeLocalise("CloseModal.Text", "OK"));

            return true;
        }

        /// <summary>
        /// Validate at least one office has been selected
        /// </summary>
        /// <param name="updatedSearchItem">The current staff member row</param>
        /// <returns>true if valid, false if not</returns>
        private bool ValidateUserOffices(ListViewItem updatedSearchItem)
        {
            if (!App.Settings.OfficesEnabled)
                return true;

            var officeRadioButtonList = (RadioButtonList)updatedSearchItem.FindControl("OfficeRadioButtonList");
            var workLevel = officeRadioButtonList.SelectedValue.ToInt();

            if (workLevel == 1)
                return true;

            if (workLevel != 2)
            {
                var officeListValidator = (CustomValidator)updatedSearchItem.FindControl("OfficeListValidator");
                officeListValidator.IsValid = false;
                officeListValidator.Text = HtmlLocalise("OfficeListValidator.Error", "Please select an option");

                ExpandOfficeControl(updatedSearchItem);

                return false;
            }

            var personOfficesList = (AssignedOffices)updatedSearchItem.FindControl("AssignedOffices");
            if (personOfficesList.AssignedOfficeList.Count > 0)
            {
                return true;
            }
            else
            {
                personOfficesList.Invalidate(CodeLocalise("OfficeExistsValidator.Error", "Please select at least one office"));
            }

            ExpandOfficeControl(updatedSearchItem);

            return false;
        }

        /// <summary>
        /// Expands the office control in the case of a validation error
        /// </summary>
        /// <param name="updatedSearchItem">The updated search item.</param>
        private void ExpandOfficeControl(ListViewItem updatedSearchItem)
        {
            var panelExtender = (CollapsiblePanelExtender)updatedSearchItem.FindControl("PersonOfficesPanelExtender");
            panelExtender.Collapsed = false;
            panelExtender.ClientState = "false";
        }

        /// <summary>
        /// Updates the user offices.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="updatedSearchItem">The updated search item.</param>
        private void UpdateUserOffices(string[] ids, ListViewItem updatedSearchItem)
        {
            var personId = Convert.ToInt64(ids[1]);

            var personOffices = new List<PersonOfficeMapperDto>();
            var officeRadioButtonList = (RadioButtonList)updatedSearchItem.FindControl("OfficeRadioButtonList");
            var personOfficesList = (AssignedOffices)updatedSearchItem.FindControl("AssignedOffices");

            var workLevel = officeRadioButtonList.SelectedValue.ToInt();

            if (workLevel == 1)
            {
                var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Key == App.Settings.DefaultStateKey);
                personOffices.Add(new PersonOfficeMapperDto { PersonId = personId, StateId = lookup.Id });
            }
            else
            {
                // If there's nothing in the list box then everything is to be deleted 
                if (personOfficesList.AssignedOfficeList.Count > 0)
                    personOffices = (from AssignedOfficeModel item in personOfficesList.AssignedOfficeList
                                     select new PersonOfficeMapperDto()
                                     {
                                         OfficeId = item.OfficeId,
                                         PersonId = personId
                                     }).ToList();
            }

            ServiceClientLocator.EmployerClient(App).SavePersonOffices(personOffices, personId);
            if (App.User.PersonId == personId)
                App.GetOfficesForUser(personId, true);

            Confirmation.Show(CodeLocalise("StaffOfficeUpdatedConfirmation.Title", "Staff offices updated"),
                                                             CodeLocalise("StaffOfficesUpdatedConfirmation.Body", "The offices for the staff account have been updated."),
                                                             CodeLocalise("CloseModal.Text", "OK"));
        }

        /// <summary>
        /// Updates the user offices.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="updatedSearchItem">The updated search item.</param>
        private bool CheckDefaultOrStatewideOffice(ListViewItem updatedSearchItem)
        {


            var personOffices = new List<PersonOfficeMapperDto>();
            var officeRadioButtonList = (RadioButtonList)updatedSearchItem.FindControl("OfficeRadioButtonList");
            var personOfficesList = (AssignedOffices)updatedSearchItem.FindControl("AssignedOffices");
            bool officeFlag = false;
            var workLevel = officeRadioButtonList.SelectedValue.ToInt();

            if (workLevel == 1)
            {
                officeFlag = true;
            }
            else
            {
                if (personOfficesList.AssignedOfficeList.Count > 0)
                {
                    foreach (var AssignedOffice in personOfficesList.AssignedOfficeList)
                    {
                        long officeId = AssignedOffice.OfficeId.IsNotNull() ? AssignedOffice.OfficeId.GetValueOrDefault() : 0;
                        if (officeId != 0)
                        {
                            var office = ServiceClientLocator.EmployerClient(App).GetOffice(officeId);
                            if (office.DefaultType != OfficeDefaultType.None)
                            {
                                officeFlag = true;
                                break;
                            }
                        }
                    }
                }

            }
            return officeFlag;

        }

        private bool CheckDefaultOrStatewideOffice(ListViewItemEventArgs e)
        {
            bool officeFlag = false;
            var officeRadioButtonList = (RadioButtonList)e.Item.FindControl("OfficeRadioButtonList");
            var workLevel = officeRadioButtonList.SelectedValue.ToInt();

            if (workLevel == 1)
            {
                officeFlag = true;
            }
            else
            {
                var personOfficesList = (AssignedOffices)e.Item.FindControl("AssignedOffices");
                if (personOfficesList.AssignedOfficeList.Count > 0)
                {
                    foreach (var AssignedOffice in personOfficesList.AssignedOfficeList)
                    {
                        long officeId = AssignedOffice.OfficeId.IsNotNull() ? AssignedOffice.OfficeId.GetValueOrDefault() : 0;
                        if (officeId != 0)
                        {
                            var office = ServiceClientLocator.EmployerClient(App).GetOffice(officeId);
                            if (office.DefaultType != OfficeDefaultType.None)
                            {
                                officeFlag = true;
                                break;
                            }
                        }
                    }
                }
            }

            return officeFlag;
        }


        /// <summary>
        /// Unbinds the check box.
        /// </summary>
        /// <param name="checkBox">The check box.</param>
        /// <param name="currentRoles">The current roles.</param>
        /// <param name="roleKey">The role key.</param>
        /// <returns></returns>
        private List<RoleDto> UnbindCheckBox(CheckBox checkBox, List<RoleDto> currentRoles, string roleKey)
        {
            var hasRole = currentRoles.Any(r => r.Key == roleKey);

            // If nothing has changed return current roles
            if (hasRole == checkBox.Checked) return currentRoles;

            // Add new role
            if (checkBox.Checked)
            {
                currentRoles.Add(new RoleDto { Key = roleKey });
            }
            else
            {
                // If code has got this far we are removing the role
                var rolesToBeRemoved = currentRoles.Where(x => x.Key == roleKey).ToList();
                rolesToBeRemoved.ForEach(x => currentRoles.Remove(x));
            }

            return currentRoles;
        }

        /// <summary>
        /// Unbinds the staff check box.
        /// </summary>
        /// <param name="adminCheckBox">The check box for account administrators.</param>
        /// <param name="checkBox">The check box.</param>
        /// <param name="currentRoles">The current roles.</param>
        /// <param name="roleKey">The role key.</param>
        /// <returns></returns>
        private List<RoleDto> UnbindStaffCheckBox(CheckBox adminCheckBox, CheckBox checkBox, List<RoleDto> currentRoles, string roleKey)
        {
            var hasRole = currentRoles.Any(r => r.Key == roleKey);

            // If nothing has changed return current roles
            if (hasRole == (checkBox.Checked || adminCheckBox.Checked)) return currentRoles;

            // Add new role
            if (adminCheckBox.Checked || checkBox.Checked)
            {
                currentRoles.Add(new RoleDto { Key = roleKey });
            }
            else
            {
                // If code has got this far we are removing the role
                var rolesToBeRemoved = currentRoles.Where(x => x.Key == roleKey).ToList();
                rolesToBeRemoved.ForEach(x => currentRoles.Remove(x));
            }

            return currentRoles;
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="userId">The user id.</param>
        private void ResetPassword(long userId)
        {
            ServiceClientLocator.AccountClient(App).ResetUsersPassword(userId, string.Format("{0}{1}", App.Settings.AssistApplicationPath, UrlBuilder.ResetPassword(false)));

            Confirmation.Show(CodeLocalise("ResetPasswordConfirmation.Title", "Reset password sent"),
                                                             CodeLocalise("ResetPasswordConfirmation.Body", "An email has been sent to the user with a link they can use to change their password."),
                                                             CodeLocalise("CloseModal.Text", "OK"));
        }

        /// <summary>
        /// Deactivates the account.
        /// </summary>
        /// <param name="personId">The user id.</param>
        private void DeactivateAccount(long personId)
        {
            var result = PageController.DeactivateAccount(personId);
            BindStaffList();
            Handle(result);
        }




        /// <summary>
        /// Reactivates the account.
        /// </summary>
        /// <param name="personId">The user id.</param>
        private void ReactivateAccount(long personId)
        {
            var result = PageController.ReactivateAccount(personId);
            BindStaffList();
            Handle(result);
        }

        /// <summary>
        /// Adds the person office.
        /// </summary>
        /// <param name="item">The item.</param>
        private void AddPersonOffice(ListViewItem item)
        {
            var assignedOfficesControl = item.FindControl("AssignedOffices") as AssignedOffices;
            assignedOfficesControl.AddOffice();
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            FindButton.Text = CodeLocalise("FindButton.Text.NoEdit", "Find");
            CreateAccountButton.Text = CodeLocalise("CreateAccountButton.Text.NoEdit", "Create new staff account");
            UploadMultipleUsersButton.Text = CodeLocalise("UploadMultipleUsersButton.Text.NoEdit", "Upload multiple users");

        }


    }
}
