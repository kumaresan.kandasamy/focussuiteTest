﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
  public partial class JobWizardEducation : PageBase
	{
    private long _jobId
    {
      get { return GetViewStateValue<long>("JobWizard:JobId"); }
      set { SetViewStateValue("JobWizard:JobId", value); }
    }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        long currentJobId = 0;
        if (Page.RouteData.Values.ContainsKey("id"))
          long.TryParse(Page.RouteData.Values["id"].ToString(), out currentJobId);

        Wizard.JobId = _jobId = currentJobId;

        long hiringManagerId = 0;
        var managerId = Request.QueryString["managerId"];
        if (managerId.IsNotNullOrEmpty())
          long.TryParse(managerId, out hiringManagerId);

        Wizard.HiringManagerId = hiringManagerId;
      }

      Page.Title = CodeLocalise("Page.Title", "Job wizard");
      ViewNotesButton.Text = CodeLocalise("ViewNotesButton.Text", "View / add notes and reminders");
      ViewNotesButton.Visible = (_jobId > 0);
    }

    /// <summary>
    /// Handles the Click event of the ViewNotesButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ViewNotesButton_Click(object sender, EventArgs e)
    {
      NotesAndReminders.Show(_jobId, EntityTypes.Job);
    }
  }
}