﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;
using Focus.Core;

#endregion

namespace Focus.Web.WebAssist
{
    [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistSystemAdministrator)]
    public partial class ManageAssist : PageBase
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = CodeLocalise("Page.Title", "Manage #FOCUSASSIST#");

            // Hide the Talent links if no Talent integration is present.
            TalentActivitiesPlaceHolder.Visible = App.Settings.TalentModulePresent;

            // Hide the Localise link depending on the role
            LocaliseLocaliseLink.Visible =
              LocaliseLink.Visible =
        LocaliseLinkDisplayOnly.Visible =
        LocalisePlaceHolder.Visible = App.User.IsInRole(Constants.RoleKeys.SystemAdmin);

            if (!IsPostBack)
            {

                LocaliseUI();
                if (App.Settings.TalentModulePresent)
                {
                    TalentDefaults.Visible = true;
                    TalentDefaults.Bind();
                }
                else
                {
                    CareerDefaultsLink.Visible = false;
                    CareerDefaultsLinkDisplayOnly.Visible = CareerDefaults.Visible = true;

                    CareerDefaults.Visible = true;
                    CareerDefaults.Bind();
                }
            }
        }

        /// <summary>
        /// Handles the Clicked event of the CareerDefaultsLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CareerDefaultsLink_Clicked(object sender, EventArgs e)
        {
            ResetNavigation();
            CareerDefaultsLink.Visible = false;
            CareerDefaultsLinkDisplayOnly.Visible = CareerDefaults.Visible = true;
            CareerDefaults.Bind();
        }

        /// <summary>
        /// Handles the Clicked event of the CareerMessagesLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void CareerMessagesLink_Clicked(object sender, EventArgs e)
        {
            ResetNavigation();
            CareerMessagesLink.Visible = false;
            CareerMessagesLinkDisplayOnly.Visible = CareerMessages.Visible = true;
            CareerMessages.Bind();
        }

        /// <summary>
        /// Handles the Clicked event of the TalentDefaultsLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        protected void TalentDefaultsLink_Clicked(object sender, EventArgs e)
        {
            ResetNavigation();
            TalentDefaultsLink.Visible = false;
            TalentDefaultsLinkDisplayOnly.Visible = TalentDefaults.Visible = true;
            TalentDefaults.Bind();
        }

        /// <summary>
        /// Handles the Clicked event of the TalentMessagesLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void TalentMessagesLink_Clicked(object sender, EventArgs e)
        {
            ResetNavigation();
            TalentMessagesLink.Visible = false;
            TalentMessagesLinkDisplayOnly.Visible = TalentMessages.Visible = true;
            TalentMessages.Bind();
        }

        /// <summary>
        /// Handles the Clicked event of the AssistDefaultsLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AssistDefaultsLink_Clicked(object sender, EventArgs e)
        {
            ResetNavigation();
            AssistDefaultsLink.Visible = false;
            AssistDefaultsLinkDisplayOnly.Visible = AssistDefaults.Visible = true;
            AssistDefaults.Bind();
        }

        /// <summary>
        /// Handles the Clicked event of the AssistMessagesLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void AssistMessagesLink_Clicked(object sender, EventArgs e)
        {
            ResetNavigation();
            AssistMessagesLink.Visible = false;
            AssistMessagesLinkDisplayOnly.Visible = AssistMessages.Visible = true;
            AssistMessages.Bind();
        }


        /// <summary>
        /// Handles the Clicked event of the LocaliseLogin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void LocaliseLoginLink_Clicked(object sender, EventArgs e)
        {
            ResetNavigation();
            LocaliseLink.Visible = false;
            LocaliseLinkDisplayOnly.Visible = Localise.Visible = true;
        }




        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            if (App.User.IsInRole(Constants.RoleKeys.SystemAdmin))
            {

                LocaliseAssistDefaultsLink.Text = EditLocalisationLink("AssistDefaults.LinkText");
                LocaliseAssistMessagesLink.Text = EditLocalisationLink("AssistMessages.LinkText");
                LocaliseCareerDefaultsLink.Text = EditLocalisationLink("CareerDefaults.LinkText");
                LocaliseCareerMessagesLink.Text = EditLocalisationLink("CareerMessages.LinkText");
                LocaliseTalentDefaultsLink.Text = EditLocalisationLink("TalentDefaults.LinkText");
                LocaliseTalentMessagesLink.Text = EditLocalisationLink("TalentMessages.LinkText");
                LocaliseLinkDisplayOnly.Text = CodeLocalise("LocaliseLogin.LinkText", "Localise");
                LocaliseLink.Text = EditLocalisationLink("Localise.LinkText");
            }

            CareerDefaultsLinkDisplayOnly.Text = CodeLocalise("CareerDefaults.LinkText", "#FOCUSCAREER# settings");
            CareerMessagesLinkDisplayOnly.Text = CodeLocalise("CareerMessages.LinkText", "#FOCUSCAREER# messages");
            TalentDefaultsLink.Text = TalentDefaultsLinkDisplayOnly.Text = CodeLocalise("TalentDefaults.LinkText", "#FOCUSTALENT# settings");
            TalentMessagesLinkDisplayOnly.Text = CodeLocalise("TalentMessages.LinkText", "#FOCUSTALENT# messages");
            AssistDefaultsLinkDisplayOnly.Text = CodeLocalise("AssistDefaults.LinkText", "#FOCUSASSIST# settings");
            AssistMessagesLinkDisplayOnly.Text = CodeLocalise("AssistMessages.LinkText", "#FOCUSASSIST# messages");

        }

        /// <summary>
        /// Resets the navigation.
        /// </summary>
        private void ResetNavigation()
        {
            CareerDefaultsLink.Visible =
          CareerMessagesLink.Visible =
            TalentDefaultsLink.Visible =
            TalentMessagesLink.Visible =
            AssistDefaultsLink.Visible =
            AssistMessagesLink.Visible =
    LocaliseLink.Visible =
     true;

            CareerDefaultsLinkDisplayOnly.Visible =
          CareerMessagesLinkDisplayOnly.Visible =
            TalentDefaultsLinkDisplayOnly.Visible =
            TalentMessagesLinkDisplayOnly.Visible =
            AssistDefaultsLinkDisplayOnly.Visible =
            AssistMessagesLinkDisplayOnly.Visible =
    LocaliseLinkDisplayOnly.Visible =
    false;

            CareerDefaults.Visible =
            CareerMessages.Visible =
            TalentDefaults.Visible =
            TalentMessages.Visible =
            AssistDefaults.Visible =
            AssistMessages.Visible =
    Localise.Visible =
     false;

        }
    }
}