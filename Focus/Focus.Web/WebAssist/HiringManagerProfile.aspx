﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HiringManagerProfile.aspx.cs" Inherits="Focus.Web.WebAssist.HiringManagerProfile" %>
<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>
<%@ Register src="~/WebAssist/Controls/HiringManagerActivityList.ascx" tagName="HMActivityList" tagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="NotesAndReminders" Src="~/WebAssist/Controls/NotesAndReminders.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
  <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
<table style="width:100%;" role="presentation">
	<tr>
    <td colspan="2"><asp:LinkButton ID="lnkReturn" runat="server" OnClick="lnkReturn_Click" CausesValidation="False"></asp:LinkButton></td>
  </tr>
  <tr>
    <td colspan="2"><br /></td>
  </tr>
  <tr>
    <td><h1><%= HtmlLocalise("HiringManagerProfile", "Hiring manager profile")%>: <asp:Label runat="server" ID="lblHiringManagerName"></asp:Label></h1></td>
    <td style="text-align:right; vertical-align:top;"><asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3" style="float:right" ClientIDMode="Static"/></td>
  </tr>
  <tr>
    <td colspan="2">
      <div class="HorizontalRule"></div>
    </td>
  </tr>
</table>

<table style="width:100%;" role="presentation">
	<tr>   
    <td colspan="2">
			<asp:Panel ID="ContactInformationHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
				<table role="presentation" class="accordionTable">
						<tr>
							<td>
								<asp:Image ID="ContactInformationHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
								&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ContactInformation.Label", "Contact Information")%></a></span></td>
						</tr>
					</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="ContactInformationPanel" CssClass="singleAccordionContent" runat="server">
					<table role="presentation">
						<tr>
							<td>
								<asp:Label runat="server" ID="lblContactDetails"></asp:Label>
								<asp:Label runat="server" ID="lblContactDetailsPhone"></asp:Label>
							</td>
							<td style="vertical-align: top;">
								<asp:LinkButton ID="AccessEmployerAccountButton" runat="server" OnClientClick="return AccessEmployerAccountButton_Validate()" OnClick="AccessEmployerAccountButton_Click" CausesValidation="True" ValidationGroup="LinkButtonValidation"><%= HtmlLocalise("AccessEmployersAccount.Label.", "Access #BUSINESS#:LOWER's #FOCUSTALENT# account")%></asp:LinkButton>
								<br/>
								<asp:Label runat="server" ID="ValidationErrorLabel" CssClass="error" ClientIDMode="Static" style="display: none"></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="ContactInformationCollapsiblePanelExtender" runat="server" TargetControlID="ContactInformationPanel" ExpandControlID="ContactInformationHeaderPanel" 
																		CollapseControlID="ContactInformationHeaderPanel" ImageControlID="ContactInformationHeaderImage"
																		SuppressPostBack="true" />
		</td>
	</tr>
	<tr>   
    <td colspan="2">
		    <asp:Panel ID="LoginsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
			    <table role="presentation" class="accordionTable">
					    <tr>
						    <td>
							    <asp:Image ID="LoginsHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
							    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Signins.Label", "Sign-ins")%></a></span></td>
					    </tr>
				    </table>
		    </asp:Panel>
				<div class="singleAccordionContentWrapper">
					<asp:Panel ID="LoginsPanel" CssClass="singleAccordionContent" runat="server">
						<table role="presentation">
							<tr>
								<td colspan="2">
									<%= HtmlLocalise("DaysSinceRegistration", "Days since registration")%>: <asp:Label runat="server" ID="lblDaysSinceRegistration"></asp:Label>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<%= HtmlLocalise("SigninsSinceRegistraion", "Sign-ins since registration")%>: <asp:Label runat="server" ID="lblLoginsSinceRegistration"></asp:Label>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<%= HtmlLocalise("Signins7Days", "Sign-ins in past 7 days")%>: <asp:Label runat="server" ID="lblLogins7Days"></asp:Label>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<%= HtmlLocalise("TimeSinceLastSignin", "Time since last sign-in")%>: <asp:Label runat="server" ID="lblTimeSinceLastLogin"></asp:Label>
								</td>
							</tr>
						</table>	
					</asp:Panel>
				</div>
		    <act:CollapsiblePanelExtender ID="LoginsCollapsiblePanelExtender" runat="server" TargetControlID="LoginsPanel" ExpandControlID="LoginsHeaderPanel" 
																	    CollapseControlID="LoginsHeaderPanel" Collapsed="true" ImageControlID="LoginsHeaderImage"
																	    SuppressPostBack="true" />
	    </td>
	</tr>
	<tr>   
    <td colspan="2">
			<asp:Panel ID="ActivitySummaryHeaderPanel" runat="server" CssClass="singleAccordionTitle">
				<table role="presentation" class="accordionTable">
						<tr>
							<td>
								<asp:Image ID="ActivitySummaryHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
								&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ActivitySummary.Label", "Activity Summary")%></a></span></td>
						</tr>
					</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="ActivitySummaryPanel" CssClass="singleAccordionContent" runat="server" >
					<table role="presentation">
						<tr>
							<td colspan="2">
								<input type="radio" name="rdoDaysFilter" ID="rdoDaysFilter7days" value="7days" checked="checked"/>
								<label for="rdoDaysFilter7days"><%=HtmlLabel("Last7Days", "Last 7 days") %></label>
								<input type="radio" name="rdoDaysFilter" ID="rdoDaysFilter30days" value="30days"/>
								<label for="rdoDaysFilter30days"><%=HtmlLabel("Last30Days", "Last 30 days") %></label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("Applicants", "Applicants")%>: <span id="Applicants7days"><asp:Label runat="server" ID="Applicants7daysLabel"></asp:Label></span><span id="Applicants30days" style="display: none;"><asp:Label runat="server" ID="Applicants30daysLabel"></asp:Label></span>
							</td>
						</tr>
						<%--<tr> Commented as these count not available
							<td colspan="2">
								<%= HtmlLocalise("Matches", "Matches")%>: <span id="Matches7days"><asp:Label runat="server" ID="Matches7daysLabel"></asp:Label></span><span id="Matches30days" style="display: none;"><asp:Label runat="server" ID="Matches30daysLabel"></asp:Label></span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ThreeFiveStarMatches", "3-5–star matches")%>: <span id="ThreeFiveStarMatches7days"><asp:Label runat="server" ID="ThreeFiveStarMatches7daysLabel"></asp:Label></span><span id="ThreeFiveStarMatches30days" style="display: none;"><asp:Label runat="server" ID="ThreeFiveStarMatches30daysLabel"></asp:Label></span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ThreeFiveStarMatchesNotView", "3-5–star matches employer did not view")%>: <span id="ThreeFiveStarMatchesNotView7days"><asp:Label runat="server" ID="ThreeFiveStarMatchesNotView7daysLabel"></asp:Label></span><span id="ThreeFiveStarMatchesNotView30days" style="display: none;"><asp:Label runat="server" ID="ThreeFiveStarMatchesNotView30daysLabel"></asp:Label></span>
							</td>
						</tr>--%>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("InvitationsToSeekers", "Invitations sent to #CANDIDATETYPE#")%>: <span id="InvitationsToSeekers7days"><asp:Label runat="server" ID="InvitationsToSeekers7daysLabel"></asp:Label></span><span id="InvitationsToSeekers30days" style="display: none;"><asp:Label runat="server" ID="InvitationsToSeekers30daysLabel"></asp:Label></span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("InvitedSeekersViewedJobs", "Invited #CANDIDATETYPE# who viewed jobs")%>: <span id="InvitedSeekersViewedJobs7days"><asp:Label runat="server" ID="InvitedSeekersViewedJobs7daysLabel"></asp:Label></span><span id="InvitedSeekersViewedJobs30days" style="display: none;"><asp:Label runat="server" ID="InvitedSeekersViewedJobs30daysLabel"></asp:Label></span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("InvitedSeekersNotViewJob", "Invited #CANDIDATETYPE# who did not view jobs")%>: <span id="InvitedSeekersNotViewJob7days"><asp:Label runat="server" ID="InvitedSeekersNotViewJob7daysLabel"></asp:Label></span><span id="InvitedSeekersNotViewJob30days" style="display: none;"><asp:Label runat="server" ID="InvitedSeekersNotViewJob30daysLabel"></asp:Label></span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("InvitedSeekersClickedHowToApply", "Invited #CANDIDATETYPE# who clicked \"How to Apply\"")%>: <span id="InvitedSeekersClickedHowToApply7days"><asp:Label runat="server" ID="InvitedSeekersClickedHowToApply7daysLabel"></asp:Label></span><span id="InvitedSeekersClickedHowToApply30days" style="display: none;"><asp:Label runat="server" ID="InvitedSeekersClickedHowToApply30daysLabel"></asp:Label></span>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("InvitedSeekersNotClickHowToApply", "Invited #CANDIDATETYPE# who did not click \"How to Apply\"")%>: <span id="InvitedSeekersNotClickHowToApply7days"><asp:Label runat="server" ID="InvitedSeekersNotClickHowToApply7daysLabel"></asp:Label></span><span id="InvitedSeekersNotClickHowToApply30days" style="display: none;"><asp:Label runat="server" ID="InvitedSeekersNotClickHowToApply30daysLabel"></asp:Label></span>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</div>
			  
			<act:CollapsiblePanelExtender ID="ActivitySummaryCollapsiblePanelExtender" runat="server" TargetControlID="ActivitySummaryPanel" ExpandControlID="ActivitySummaryHeaderPanel" 
																		CollapseControlID="ActivitySummaryHeaderPanel" Collapsed="true" ImageControlID="ActivitySummaryHeaderImage"
																		SuppressPostBack="true" />
		</td>
	</tr>
	<tr>   
    <td colspan="2">
			<asp:Panel ID="ReferralOutcomesHeaderPanel" runat="server" CssClass="singleAccordionTitle">
				<table role="presentation" class="accordionTable">
						<tr>
							<td>
								<asp:Image ID="ReferralOutcomesHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
								&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ReferralOutcomes.Label", "Referral Outcomes")%></a></span></td>
						</tr>
					</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="ReferralOutcomesPanel" CssClass="singleAccordionContent" runat="server">
					<table role="presentation" class="cellsWithPadding">
						<tr>
							<td colspan="2" >
								<%= HtmlLocalise("ApplicantsHired", "Applicants hired")%>: <asp:Label runat="server" ID="lblApplicantsHired"></asp:Label>
							</td>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsFailedToRespondToInvitation", "Applicants who failed to respond to invitation")%>: <asp:Label runat="server" ID="lblApplicantsFailedToRespondToInvitation"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsInterviewed", "Applicants interviewed/pending interviews")%>: <asp:Label runat="server" ID="lblApplicantsInterviewed"></asp:Label>
							</td>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsFoundJobFromOtherSource", "Applicants who found a job from other source")%>: <asp:Label runat="server" ID="lblApplicantsFoundJobFromOtherSource"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsJobAlreadyFilled", "Applicants job already filled")%>: <asp:Label runat="server" ID="lblApplicantsJobAlreadyFilled"></asp:Label>
							</td>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsRefused", "Applicants who refused jobs")%>: <asp:Label runat="server" ID="lblApplicantsRefused"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsNotYetPlaced", "Applicants not yet placed")%>: <asp:Label runat="server" ID="lblApplicantsNotYetPlaced"></asp:Label>
							</td>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsRefusedReferral", "Applicants who refused referral")%>: <asp:Label runat="server" ID="lblApplicantsRefusedReferral"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsRejected", "Applicants rejected")%>: <asp:Label runat="server" ID="lblApplicantsRejected"></asp:Label>
							</td>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsNotQualified", "Applicants who were not qualified")%>: <asp:Label runat="server" ID="lblApplicantsNotQualified"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsUnderConsideration", "Applicants under consideration")%>: <asp:Label runat="server" ID="lblApplicantsUnderConsideration"></asp:Label>
							</td>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsInterviewDenied", "Interviews denied")%>: <asp:Label runat="server" ID="lblApplicantsInterviewDenied"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsFailedToShow", "Applicants who failed to show")%>: <asp:Label runat="server" ID="lblApplicantsFailedToShow"></asp:Label>
							</td>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsNewApplicant", "New applicants")%>: <asp:Label runat="server" ID="lblApplicantsNewApplicant"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsDidNotApply", "Applicants who did not apply")%>: <asp:Label runat="server" ID="lblApplicantsDidNotApply"></asp:Label>
							</td>
							<td colspan="2">
								<%= HtmlLocalise("ApplicantsRecommended", "Recommended applicants")%>: <asp:Label runat="server" ID="lblApplicantsRecommended"></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</div>
			  
			<act:CollapsiblePanelExtender ID="ReferralOutcomesCollapsiblePanelExtender" runat="server" TargetControlID="ReferralOutcomesPanel" ExpandControlID="ReferralOutcomesHeaderPanel" 
																		CollapseControlID="ReferralOutcomesHeaderPanel" Collapsed="true" ImageControlID="ReferralOutcomesHeaderImage"
																		SuppressPostBack="true" />
		</td>
	</tr>
	<tr>   
    <td colspan="2">
			<asp:Panel ID="SurveyResponsesHeaderPanel" runat="server" CssClass="singleAccordionTitle">
				<table role="presentation" class="accordionTable">
						<tr>
							<td>
								<asp:Image ID="SurveyResponsesHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
								&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("SurveyResponses.Label", "Survey Responses")%></a></span></td>
						</tr>
					</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="SurveyResponsesPanel" CssClass="singleAccordionContent" runat="server">
					<table role="presentation">
					 <tr>
							<td colspan="2">
								<%= HtmlLocalise("SurveyVerySatisfied", "Very satisfied with resume quality")%>: <asp:Label runat="server" ID="lblSurveyVerySatisfied"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("SurveySomewhatSatisfied", "Somewhat satisfied with resume quality")%>: <asp:Label runat="server" ID="lblSurveySomewhatSatisfied"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("SurveySomewhatDissatified", "Somewhat dissatified with resume quality")%>: <asp:Label runat="server" ID="lblSurveySomewhatDissatified"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("SurveyVeryDissatified", "Very dissatified with resume quality")%>: <asp:Label runat="server" ID="lblSurveyVeryDissatified"></asp:Label>
							</td>
						</tr>
					
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("SurveyDidNotInterview", "Did not interview applicants")%>: <asp:Label runat="server" ID="lblSurveyDidNotInterview"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("SurveyInterviewed", "Interviewed applicants")%>: <asp:Label runat="server" ID="lblSurveyInterviewed"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("SurveyDidNotHire", "Did not hire applicants")%>: <asp:Label runat="server" ID="lblSurveyDidNotHire"></asp:Label>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<%= HtmlLocalise("SurveyHired", "Hired applicants")%>: <asp:Label runat="server" ID="lblSurveyHired"></asp:Label>
							</td>
						</tr>
					</table>
				</asp:Panel>
			</div>     	
			
			<act:CollapsiblePanelExtender ID="SurveyResponsesCollapsiblePanelExtender" runat="server" TargetControlID="SurveyResponsesPanel" ExpandControlID="SurveyResponsesHeaderPanel" 
																		CollapseControlID="SurveyResponsesHeaderPanel" Collapsed="true" ImageControlID="SurveyResponsesHeaderImage"
																		SuppressPostBack="true" />
		</td>
	</tr>
	<tr>   
    <td colspan="2">
			<asp:Panel ID="ActivityLogHeaderPanel" runat="server" CssClass="singleAccordionTitle">
				<table role="presentation" class="accordionTable">
						<tr>
							<td>
								<asp:Image ID="ActivityLogHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
								&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ActivityLog.Label", "Hiring Manager Activity Log")%></a></span></td>
						</tr>
					</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="ActivityLogPanel" CssClass="singleAccordionContent" runat="server">
				 <uc:HMActivityList ID="HMActivityList" runat="server" />
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="ActivityLogCollapsiblePanelExtender" runat="server" TargetControlID="ActivityLogPanel" ExpandControlID="ActivityLogHeaderPanel" 
																		CollapseControlID="ActivityLogHeaderPanel" Collapsed="true" ImageControlID="ActivityLogHeaderImage"
																		SuppressPostBack="true" />
		</td>
	</tr>
	<tr>   
    <td colspan="2">
			<asp:Panel ID="NotesAndRemindersHeaderPanel" runat="server" CssClass="singleAccordionTitle">
				<table role="presentation" class="accordionTable">
						<tr>
							<td>
								<asp:Image ID="NotesAndRemindersHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
								&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("NotesAndReminders.Label", "Notes and Reminders")%></a></span></td>
						</tr>
					</table>
			</asp:Panel>
			<div class="singleAccordionContentWrapper">
				<asp:Panel ID="NotesAndRemindersPanel" CssClass="singleAccordionContent" runat="server">
					<uc:NotesAndReminders ID="NotesReminders" runat="server" />
				</asp:Panel>
			</div>
			<act:CollapsiblePanelExtender ID="NotesAndRemindersCollapsiblePanelExtender" runat="server" TargetControlID="NotesAndRemindersPanel" ExpandControlID="NotesAndRemindersHeaderPanel" 
																		CollapseControlID="NotesAndRemindersHeaderPanel" Collapsed="true" ImageControlID="NotesAndRemindersHeaderImage"
																		SuppressPostBack="true" BehaviorID="NotesAndRemindersCollapsiblePanel"/>
		</td>
	</tr>
    
</table>
<%-- <uc:EmailEmployeeModal ID="EmailEmployee" runat="server" />--%>
 <uc:ConfirmationModal ID="ConfirmationModal" runat="server" />		

		<script type="text/javascript">

			var Business_TalentPopup = null;

			Sys.Application.add_load(HiringManagerProfile_PageLoad);

			function HiringManagerProfile_PageLoad() {
				var button = $("#<%= ExpandCollapseAllButton.ClientID %>");
				if (button.length > 0)
					bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>', null); // bind the magic

				accordionLayoutPostbackFix(); // FVN-3128
			}

			function showPopup(url, blockedPopupMessage) {
				var popup = window.open(url, "Business_blank");
				if (!popup)
					alert(blockedPopupMessage);

				return popup;
			}

			function jumpToNotesReminders() {
				$find("NotesAndRemindersCollapsiblePanel")._doOpen();
				setTimeout(function () {
					$(window).scrollTop($("#<%= NotesAndRemindersHeaderPanel.ClientID %>").offset().top);
				}, 500);
				return false;
			}

			$('input[name="rdoDaysFilter"]').change(function () {
				if ($(this).val() == '30days') {
					$('span[id$="7days"]').hide();
					$('span[id$="30days"]').show();
				}
				else {
					$('span[id$="7days"]').show();
					$('span[id$="30days"]').hide();
				}
			});

			function AccessEmployerAccountButton_Validate() {

				if (Business_TalentPopup != null && !Business_TalentPopup.closed) {
					$("#ValidationErrorLabel").text("<%=CloseBusinessWindowErrorMessage %>").show();
					return false;
				} else {
					$("#ValidationErrorLabel").hide();
					return true;
				}
			}

	</script>

</asp:Content>


