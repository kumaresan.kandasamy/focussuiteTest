﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;

using Focus.Common;
using Focus.Core.Models.Career;
using Focus.Web.Code;
using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistJobSeekersAdministrator)]
  public partial class SearchJobPostings : PageBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      string title = CodeLocalise("Page.Title", "Search Job Postings");

      if (Page.RouteData.Values.ContainsKey("newsearch"))
        App.SetSessionValue<SearchCriteria>("Career:SearchCriteria",null);

      if (Page.RouteData.Values.ContainsKey("jobseekerid") && !IsPostBack)
      {
        long id;
        Int64.TryParse(Page.RouteData.Values["jobseekerid"].ToString(), out id);
				SearchPostings.JobSeekerId = id;
      }

      if (Page.RouteData.Values.ContainsKey("jobseekerid") && Page.RouteData.Values.ContainsKey("newsearch"))
          title = title + " - " + Page.RouteData.Values["jobseekerid"].ToString() + " - " + Page.RouteData.Values["newsearch"].ToString();

      Page.Title = title;

      SearchPostings.SearchJobPostingsClick += OnSearchJobPostingsClick;
      SearchPostings.Visible = true;
    }

    /// <summary>
    /// Called when [search job postings click].
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    public void OnSearchJobPostingsClick(object sender, EventArgs e)
    {
			if (SearchPostings.JobSeekerId > 0)
				ServiceClientLocator.CandidateClient(App).RegisterFindJobsForSeeker(SearchPostings.JobSeekerId);

			var sExp = SearchPostings.BuildSearchExpression();
      App.SetSessionValue("Career:SearchCriteria", sExp);

			//TODO: need new combined results page
			Response.Redirect(UrlBuilder.AssistJobSearchResultsPage(SearchPostings.JobSeekerId, 1));
    }
  }
}