﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="EmployerReferrals.aspx.cs" Inherits="Focus.Web.WebAssist.EmployerReferrals" %>

<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Import Namespace="Framework.Core" %>
<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ApprovalQueuesNavigation.ascx" TagName="ApprovalQueuesNavigation"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register Src="~/WebAssist/Controls/ApprovalQueueFilter.ascx" TagName="ApprovalQueueFilter"
    TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
  <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
  <uc:ApprovalQueuesNavigation ID="ApprovalQueuesNavigationMain" runat="server" />
  <h1>
    <%= HtmlLocalise("EmployerReferrals.Header", "Approve #BUSINESS#:LOWER account requests")%>
  </h1>
  <table style="width:100%;" role="presentation">
    <tr>
      <td style="width: 50px;">
        <%= HtmlLabel(PendingFilterDropDown, "Status.Text", "Status")%>&nbsp;</td>
      <td style="width: 150px">
        <asp:DropDownList runat="server" ID="PendingFilterDropDown" />
      </td>
      <td>
        <uc:ApprovalQueueFilter ID="ApprovalQueueFilter" runat="server" />
      </td>
      <td style="text-align: right">
        <asp:Button ID="FilterButton" runat="server" class="button3" OnClick="FilterButton_Clicked" />
      </td>
    </tr>
  </table>
  <table style="width:100%;" role="presentation">
    <tr>
      <td style="width:50%;">
        <asp:Literal ID="ResultCount" runat="server" />
      </td>
      <td style="text-align:right; white-space: nowrap;">
        <uc:Pager ID="ReferralListPager" runat="server" PagedControlId="ReferralList" PageSize="25" />
      </td>
    </tr>
  </table>
  <asp:ListView ID="ReferralList" runat="server" ItemPlaceholderID="ReferralPlaceHolder" DataSourceID="ReferralDataSource"
  OnItemDataBound="ReferralList_ItemDataBound" OnSorting="ReferralList_Sorting">
    <LayoutTemplate>
      <table style="width:100%;" class="table" id="EmployerReferralListTable">
        <tr>
          <th style="width:25%;">
            <table role="presentation">
              <tr>
                <td rowspan="2" style="height: 16px;">
                  <asp:Literal ID="BusinessUnitNameHeader" runat="server" />
                </td>
                <td style="height: 8px;">
                  <asp:ImageButton ID="BusinessUnitNameAscButton" runat="server" CommandName="Sort" CommandArgument="BusinessUnitName asc"
                  CssClass="AscButton" AlternateText="Sort Ascending" />
                  <asp:ImageButton ID="BusinessUnitNameDescButton" runat="server" CommandName="Sort" CommandArgument="BusinessUnitName desc"
                  CssClass="DescButton" AlternateText="Sort Descending" />
                </td>
              </tr>
            </table>
          </th>
          <th style="width:25%;">
            <table role="presentation">
              <tr>
                <td rowspan="2" style="height: 16px;">
                  <asp:Literal ID="ContactNameHeader" runat="server" />
                </td>
                <td style="height: 8px;">
                  <asp:ImageButton ID="ContactNameAscButton" runat="server" CommandName="Sort" CommandArgument="EmployeeFullName asc"
                  CssClass="AscButton" AlternateText="Sort Ascending" />
                  <asp:ImageButton ID="ContactNameDescButton" runat="server" CommandName="Sort" CommandArgument="EmployeeFullName desc"
                  CssClass="DescButton" AlternateText="Sort Descending" />
                </td>
              </tr>
            </table>
          </th>
          <th style="width:25%;">
            <table role="presentation">
              <tr>
                <td rowspan="2" style="height: 16px;">
                  <asp:Literal ID="AccountCreationDateHeader" runat="server" />
                </td>
                <td style="height: 8px;">
                  <asp:ImageButton ID="AccountCreationDateAscButton" runat="server" CommandName="Sort" CommandArgument="AccountCreationDate asc"
                  CssClass="AscButton" AlternateText="Sort Ascending" />
                  <asp:ImageButton ID="AccountCreationDateDescButton" runat="server" CommandName="Sort" CommandArgument="AccountCreationDate desc"
                  CssClass="DescButton" AlternateText="Sort Descending" />
                </td>
              </tr>
            </table>
          </th>
          <th style="width:25%;">
            <table role="presentation">
              <tr>
                <td rowspan="2" style="height: 16px;">
                  <asp:Literal ID="TimeInQueueHeader" runat="server" />
                </td>
                <td style="height: 8px;">
                  <asp:ImageButton ID="TimeInQueueAscButton" runat="server" CommandName="Sort" CommandArgument="AwaitingApprovalDate desc"
                  CssClass="AscButton" AlternateText="Sort Ascending" />
                  <asp:ImageButton ID="TimeInQueueDescButton" runat="server" CommandName="Sort" CommandArgument="AwaitingApprovalDate asc"
                  CssClass="DescButton" AlternateText="Sort Descending" />
                </td>
              </tr>
            </table>
          </th>
        </tr>
        <asp:PlaceHolder ID="ReferralPlaceHolder" runat="server" />
      </table>
    </LayoutTemplate>
    <ItemTemplate>
      <tr style="vertical-align:top;">
        <td style="width:25%;">
          <asp:Image ID="FlagImage" runat="server" ImageUrl="<%# UrlBuilder.FlaggedCandidateImage() %>" AlternateText="FlagImage"
          />
          <asp:Image ID="primaryImage" runat="server" ImageUrl="<%# UrlBuilder.EmployerParentIcon() %>" Visible="<%# ((EmployerAccountReferralViewDto) Container.DataItem).EmployerIsPrimary %>"
          AlternateText="Primary Image" />
	        <asp:HyperLink id="EmployerNameLink" runat="server"> <%# ((EmployerAccountReferralViewDto)Container.DataItem).BusinessUnitName%></asp:HyperLink>
          <asp:HyperLink id="BusinessUnitNameLink" runat="server" ><%# ((EmployerAccountReferralViewDto)Container.DataItem).BusinessUnitName%></asp:HyperLink>
          <asp:Literal id="BusinessUnitLabel" runat="server" visible="<%# ((EmployerAccountReferralViewDto) Container.DataItem).TypeOfApproval == (int)ApprovalType.HiringManager %>"
          Text="<%# ((EmployerAccountReferralViewDto)Container.DataItem).BusinessUnitName%>"></asp:Literal>
        </td>
        <td style="width:25%;">
          <asp:HyperLink id="HiringManagerLink" runat="server">
                        <%# ((EmployerAccountReferralViewDto)Container.DataItem).EmployeeFirstName%>
                        <%# ((EmployerAccountReferralViewDto)Container.DataItem).EmployeeLastName%>
												<%# ((EmployerAccountReferralViewDto)Container.DataItem).Suffix %></asp:HyperLink>
          <asp:Literal id="HiringManagerLabel" runat="server" visible="<%# ((EmployerAccountReferralViewDto) Container.DataItem).TypeOfApproval != (int)ApprovalType.HiringManager %>"
          Text='<%# String.Format("{0} {1} {2}",((EmployerAccountReferralViewDto)Container.DataItem).EmployeeFirstName,((EmployerAccountReferralViewDto)Container.DataItem).EmployeeLastName,((EmployerAccountReferralViewDto)Container.DataItem).Suffix)%>'></asp:Literal>
        </td>
        <td style="width:25%;">
          <%# ((EmployerAccountReferralViewDto)Container.DataItem).AccountCreationDate.ToString( "MMM d, yyyy")%>
        </td>
        <td style="width:25%;">
          <%# ((EmployerAccountReferralViewDto)Container.DataItem).AwaitingApprovalDate.GetBusinessDays(DateTime.Now)%>
            <%= HtmlLocalise( "Days.Label", "day(s)")%>
        </td>
      </tr>
    </ItemTemplate>
  </asp:ListView>
  <asp:ObjectDataSource ID="ReferralDataSource" runat="server" TypeName="Focus.Web.WebAssist.EmployerReferrals" EnablePaging="true"
  SelectMethod="GetReferrals" SelectCountMethod="GetReferralsCount" SortParameterName="orderBy" OnSelecting="ReferralDataSource_Selecting"></asp:ObjectDataSource>
</asp:Content>