﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageOffices.aspx.cs" Inherits="Focus.Web.WebAssist.ManageOffices" %>
<%@ Import Namespace="System.Security.Policy" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>

<%@ Register TagPrefix="uc" TagName="TabNavigation" Src="~/WebAssist/Controls/TabNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="ManageAssistNavigation" Src="~/WebAssist/Controls/ManageAssistNavigation.ascx" %>
<%@ Register TagPrefix="uc" TagName="Pager" Src="~/Code/Controls/User/Pager.ascx" %>
<%@ Register tagPrefix="uc" tagName="OfficeModal" src="~/WebAssist/Controls/OfficeModal.ascx" %>
<%@ Register tagPrefix="uc" tagName="UpdateDefaultOfficeModal" src="~/WebAssist/Controls/UpdateDefaultOfficeModal.ascx" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%# ResolveUrl("~/Assets/Scripts/jquery-ui-1.9.2.custom.new.min.js") %>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<%# ResolveUrl("~/Assets/Css/jquery-ui-1.9.2.custom.css") %>" />
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />

	<table role="presentation" ID="OfficeHeaderTable" runat="server" Visible="True" style="width:100%;" >
	  <tr>
  		<td colspan="2">
				<h1><%= HtmlLocalise("Header.Text", "Manage Offices")%></h1>
			</td>
      <td style="text-align: right;">
         <asp:Button ID="UpdateDefaultOfficeButton" runat="server" class="button1" OnClick="UpdateDefaultOfficeButton_Clicked"/>
         <asp:Button ID="CreateOfficeButton" runat="server" class="button1" OnClick="CreateOfficeButton_Clicked"/>
      </td>
	  </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
    </tr>
		<tr style="vertical-align: top">
			<td style="width:40px;"><%= HtmlLabel(OfficeFilterDropDown,"WebAssist.ManageOffices.Show.Label", "Show")%></td>
			<td style="width:600px;">
				&nbsp;&nbsp;<asp:DropDownList runat="server" ID="OfficeFilterDropDown" AutoPostBack="true" OnSelectedIndexChanged="OfficeFilterDropDown_Change" Width="350" Visible = true ClientIDMode="Static" CssClass="tooltip"/>
			</td>
      <td style="white-space: nowrap">
				<div style="width:100%; vertical-align: middle">
				  <asp:UpdatePanel ID="PagerUpdatePanel" runat="server" ClientIDMode="Static" UpdateMode="Conditional">
	          <ContentTemplate>
	            <div style="float:left">
	              <strong><asp:literal ID="ResultCount" runat="server" /></strong>
              </div>
              <div style="float:right">
					      <uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="OfficeList" PageSize="10" OnDataPaged="SearchResultListPager_OnDataPaged" />
              </div>
            </ContentTemplate>
          </asp:UpdatePanel>
				</div>
			</td>
		</tr>
	</table>

	<asp:UpdatePanel ID="OfficeListPanel" runat="server" ClientIDMode="Static" UpdateMode="Conditional">
	<ContentTemplate>
	<asp:ListView ID="OfficeList" runat="server" DataKeyNames="Id,OfficeName,TownCity,StateId,PostcodeZip,OfficeManagerMailbox" DataSourceID="OfficesDataSource" ItemPlaceholderID="SearchResultPlaceHolder" OnItemDataBound="OfficeList_ItemDataBound" OnSorting="OfficeList_Sorting" OnLayoutCreated="OfficeList_LayoutCreated">
		<LayoutTemplate>	
			<table style="width:100%;" class="table" id="OfficeList">
				<thead>
				<tr>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="OfficeNameHeader" /></td>
								<td style="height:8px;">
									<asp:ImageButton ID="OfficeNameAscSortButton" runat="server" CommandName="Sort" CommandArgument="OfficeName Asc" CssClass="AscButton" AlternateText="Sort Ascending"/>
                  <asp:ImageButton ID="OfficeNameDescSortButton" runat="server" CommandName="Sort" CommandArgument="OfficeName Desc" CssClass="DescButton" AlternateText="Sort Descending"/>
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="CityHeader" /></td>
								<td style="height:8px;">
									<asp:ImageButton ID="TownCityAscSortButton" runat="server" CommandName="Sort" CommandArgument="TownCity Asc" CssClass="AscButton"  AlternateText="Sort Ascending"/>
                  <asp:ImageButton ID="TownCityDescSortButton" runat="server" CommandName="Sort" CommandArgument="TownCity Desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="StateNameHeader" /></td>
								<td style="height:8px;"><asp:ImageButton ID="StateIdAscSortButton" runat="server" CommandName="Sort" CommandArgument="StateId Asc" CssClass="AscButton"  AlternateText="Sort Ascending"/>
                <asp:ImageButton ID="StateIdDescSortButton" runat="server" CommandName="Sort" CommandArgument="StateId Desc" CssClass="DescButton" AlternateText="Sort Descending" /></td>
							</tr>
						</table>
					</th>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="ZipHeader" /></td>
								<td style="height:8px;">
									<asp:ImageButton ID="PostcodeZipAscSortButton" runat="server" CommandName="Sort" CommandArgument="PostcodeZip Asc" CssClass="AscButton"  AlternateText="Sort Ascending"/>
                  <asp:ImageButton ID="PostcodeZipDescSortButton" runat="server" CommandName="Sort" CommandArgument="PostcodeZip Desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="OfficeManagerMailboxHeader" /></td>
								<td style="height:8px;">
									<asp:ImageButton ID="OfficeManagerMailboxAscSortButton" runat="server" CommandName="Sort" CommandArgument="OfficeManagerMailbox Asc" CssClass="AscButton"  AlternateText="Sort Ascending"/>
                  <asp:ImageButton ID="OfficeManagerMailboxDescSortButton" runat="server" CommandName="Sort" CommandArgument="OfficeManagerMailbox Desc" CssClass="DescButton" AlternateText="Sort Descending" />
								</td>
							</tr>
						</table>
					</th>
				</tr>
				</thead>
				<tbody>
				<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
				</tbody>
			</table>	
		</LayoutTemplate>
		<ItemTemplate>
			<tr>
				<asp:HiddenField runat="server" ID="IdHiddenField" ClientIDMode="AutoId" Value="<%# ((OfficeDto)Container.DataItem).Id%>"/>
				<asp:HiddenField runat="server" ID="StateIdHiddenField" ClientIDMode="AutoId" Value="<%# ((OfficeDto)Container.DataItem).StateId%>"/>
				<td>
				  <asp:LinkButton runat="server" ID="OfficeLink" CommandName="Profile" OnCommand="OfficeLink_OnCommand" Visible="True"></asp:LinkButton>
				  <asp:Label runat="server" ID="OfficeNameLabel" Visible="False"></asp:Label>
					<asp:Image ID="DefaultOfficeGeneralIcon" runat="server" Visible="False" CssClass="defaultOfficeListIcon" ImageUrl="<%# UrlBuilder.DefaultOfficeGeneralIconSmall() %>" ToolTip='<%# HtmlLocalise("DefaultOfficeGeneral.ToolTip", "This is the default office for all unassigned records") %>' AlternateText='<%# HtmlLocalise("DefaultOfficeGeneral.ToolTip", "This is the default office for all unassigned records") %>'/>
					<asp:Image runat="server" ID="DefaultOfficeJobOrderIcon" Visible="False" CssClass="defaultOfficeListIcon" ImageUrl="<%# UrlBuilder.DefaultOfficeJobOrderIconSmall() %>" ToolTip='<%# HtmlLocalise("DefaultOfficeJobOrder.ToolTip", "This is the default office for all unassigned #EMPLOYMENTTYPES#:LOWER") %>' AlternateText='<%# HtmlLocalise("DefaultOfficeJobOrder.ToolTip", "This is the default office for all unassigned #EMPLOYMENTTYPES#:LOWER") %>'/>
					<asp:Image runat="server" ID="DefaultOfficeJobSeekerIcon" Visible="False" CssClass="defaultOfficeListIcon" ImageUrl="<%# UrlBuilder.DefaultOfficeJobSeekerIconSmall() %>" ToolTip='<%# HtmlLocalise("DefaultOfficeJobSeeker.ToolTip", "This is the default office for all unassigned job seekers") %>' AlternateText='<%# HtmlLocalise("DefaultOfficeJobSeeker.ToolTip", "This is the default office for all unassigned job seekers") %>'/>
					<asp:Image runat="server" ID="DefaultOfficeEmployerIcon" Visible="False" CssClass="defaultOfficeListIcon" ImageUrl="<%# UrlBuilder.DefaultOfficeEmployerIconSmall() %>" ToolTip='<%# HtmlLocalise("DefaultOfficeEmployer.ToolTip", "This is the default office for all unassigned #BUSINESSES#:LOWER") %>' AlternateText='<%# HtmlLocalise("DefaultOfficeEmployer.ToolTip", "This is the default office for all unassigned #BUSINESSES#:LOWER") %>'/>
				</td>
				<td><asp:Label runat="server" Text='<%#((OfficeDto)Container.DataItem).TownCity%>'/></td>
				<td><asp:Label runat="server" ID="StateName"/></td>
				<td><asp:Label ID="PostcodeZipItemLabel" runat="server" Text='<%#((OfficeDto)Container.DataItem).PostcodeZip%>'/></td>			
				<td><asp:Label ID="OfficeManagerMailboxItemLabel" runat="server" Text='<%#((OfficeDto)Container.DataItem).OfficeManagerMailbox%>'/></td>			
			</tr>	
		</ItemTemplate>
	</asp:ListView>

	<asp:ObjectDataSource ID="OfficesDataSource" runat="server" TypeName="Focus.Web.WebAssist.ManageOffices" EnablePaging="true" 
														SelectMethod="GetOffices" OnSelecting="OfficesDataSource_Selecting" SelectCountMethod="GetOfficeCount" SortParameterName="orderBy">
  </asp:ObjectDataSource>
	<uc:OfficeModal ID="OfficeModal" runat="server" OnCompleted="OfficeAction_Completed"/>	
	<uc:UpdateDefaultOfficeModal ID="UpdateDefaultOfficeModal" runat="server" OnUpdated="UpdateDefaultOfficeModal_OnUpdated" />	
	</ContentTemplate>
</asp:UpdatePanel>
	<script type="text/javascript" >

		function fnCheckUnCheck(objId) {
			var grd = document.getElementById("OfficeListPanel");

			var rdoArray = grd.getElementsByTagName("input");

			for (i = 0; i <= rdoArray.length - 1; i++) {
				if (rdoArray[i].type == 'radio') {
					if (rdoArray[i].id != objId) {
						rdoArray[i].checked = false;
					}
				}
			}
		}
	</script>
</asp:Content>


