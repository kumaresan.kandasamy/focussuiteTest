﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.Web.WebAssist {
    
    
    public partial class SearchJobPostings {
        
        /// <summary>
        /// TabNavigationMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.WebAssist.Controls.TabNavigation TabNavigationMain;
        
        /// <summary>
        /// UpdatePanelJobSearchCriteria control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanelJobSearchCriteria;
        
        /// <summary>
        /// AssistJobSeekersNavigationMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.WebAssist.Controls.AssistJobSeekersNavigation AssistJobSeekersNavigationMain;
        
        /// <summary>
        /// SearchPostings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.Code.Controls.User.SearchJobPostings SearchPostings;
        
        /// <summary>
        /// SavedSearch control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Web.WebAssist.Controls.SavedSearch SavedSearch;
    }
}
