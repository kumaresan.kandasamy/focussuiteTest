﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf.PdfDocument;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employer;
using Focus.Core.Criteria.Job;
using Focus.Core.Models;
using Focus.Core.Models.Assist;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersReadOnly)]
  public partial class JobOrderDashboard : PageBase
  {
		private JobListFilter? _jobFilter;
		private string _altEmployeeApproved, _altEmployeeRejected, _altEmployeeAwaitingApproval;
		private int _page;
		private int _pageSize;
		private string _problemWithDatesDetails, _activeDetails, _onHoldDetails, _closedDetails, _draftDetails, _jobDateFormat, _jobDateDefaultValue;
		private List<int> _ids;
		private long _employeeId;
		private int _jobsCount;
    private bool _reApplyFilter;
		protected string CloseBusinessWindowErrorMessage = "";

    #region Properties
		
    internal List<int> RowIds
    {
      get { return GetViewStateValue<List<int>>("JobOrderDashboard:RowIds"); }
      set { SetViewStateValue("JobOrderDashboard:RowIds", value); }
    }
    
    private JobStatuses? SelectedStatus
    {
      get { return GetViewStateValue<JobStatuses?>("JobOrderDashboard:SelectedStatus"); }
      set { SetViewStateValue("JobOrderDashboard:SelectedStatus", value); }
    }
    
    private ActionTypes? SelectedAction
    {
      get { return GetViewStateValue<ActionTypes>("Employers:SelectedAction"); }
      set { SetViewStateValue("Employers:SelectedAction", value); }
    }

		private JobCriteria JobCriteria
    {
      get { return GetViewStateValue<JobCriteria>("JobOrderDashboard:JobCriteria"); }
      set { SetViewStateValue("JobOrderDashboard:JobCriteria", value); }
    }

    public string FilterQueryString
    {
      get 
      {
        return "?filtered=true";
      }
    }

    #endregion

    #region Page load and pre render

    protected void Page_Load(object sender, EventArgs e)
    {
			Page.Title = CodeLocalise("Page.Title", "#EMPLOYMENTTYPE#  dashboard");
      _problemWithDatesDetails = CodeLocalise("JobRow.ProblemWithDates.Label", "Problem displaying the dates!");

			_jobDateFormat = "{0:" + CodeLocalise("JobRow.Date.Format", "MMM dd, yyyy") +"}";
			_jobDateDefaultValue = CodeLocalise("JobRow.Date.DefaultValue", "NA");
      _activeDetails = CodeLocalise("JobRow.Active.Label", "Posted {0}, expires {1}");
      _onHoldDetails = CodeLocalise("JobRow.OnHold.Label", "Posted {0}, on hold since {1}");
      _closedDetails = CodeLocalise("JobRow.Closed.Label", "Posted {0}, closed on {1}");
      _draftDetails = CodeLocalise("JobRow.Expires.Label", "Draft last saved on {0}");

      _altEmployeeApproved = CodeLocalise("EmployeeApproved.Hint", "Employee approved");
      _altEmployeeRejected = CodeLocalise("EmployeeRejected.Hint", "Employee rejected");
      _altEmployeeAwaitingApproval = CodeLocalise("EmployeeAwaitingApproval", "Employee awaiting approval");

			CloseBusinessWindowErrorMessage = CodeLocalise("CloseBusinessWindow.Error.Text", "You can only have one #BUSINESS#:LOWER window open at a time");

      if (!IsPostBack)
      {
        // Re-set search criteria if returning from a page following a filtered result
        if (Request.QueryString["filtered"] != null)
        {
            Page.Title = CodeLocalise("Page.Title", "#EMPLOYMENTTYPE#  dashboard - filtered");
          _reApplyFilter = Convert.ToBoolean(Request.QueryString["filtered"]);
          JobCriteria = App.GetSessionValue("JobOrderDashboard:SearchCriteria", new JobCriteria());

          // Set the Pager control to the previous Listview page and pagesize
          SearchResultListPager.ReturnToPage(Convert.ToInt32(JobCriteria.PageIndex), Convert.ToInt32(JobCriteria.PageSize));
        }
        else 
        {
          // Clear down Find Employer / Business session variables which may have been set in a different page
          App.RemoveSessionValue("FindEmployer:SearchCriteria");
        }

				SetReturnUrl();
        LocaliseUI();
	      ApplyBranding();
	      ApplyTheme();
        BindJobOrdersList();
        BindCreationDates();
        BindJobOrderFilters();
        BindActionDropDown();
        BindJobIssuesDropDown();

	      OfficeFilterRow.Visible = App.Settings.OfficesEnabled;

        if (_reApplyFilter)
        {
          RebindOfficeFilter();
          BindSearchTextFields();
        }

        JobOrderShowCell.Style.Add("width", OfficeFilterRow.Visible ? "120px" : "60px");

        if (SearchResultListPager.TotalRowCount > 0) FormatJobOrderListSortingImages();

        _reApplyFilter = false;
      }
      else
      {
        PopulateJobOrdersFilterSubMenu(JobOrdersFilterDropDown.SelectedValueToEnum<JobListFilter>(), false);
      }

      CreateNewEmployerAndJobOrderButton.Visible = LocaliseCreateNewEmployerAndJobOrderButton.Visible = CanCreateEmployers();
      JobOrdersActionDropDown.Attributes.Add("OnChange", "HideSubAction()");

      JavaScriptHelpers();
    }

    #endregion

    #region Data binding / sorting / object datasource methods

    /// <summary>
    /// Binds the creation dates.
    /// </summary>
    private void BindCreationDates()
    {
      #region Bind creation dates

      FromMonthDropDownList.Items.Clear(); FromYearDropDownList.Items.Clear();
      ToMonthDropDownList.Items.Clear(); ToYearDropDownList.Items.Clear();

      var months = new[] { CodeLocalise("Global.January.Label", "January"), CodeLocalise("Global.February.Label", "February"), CodeLocalise("Global.March.Label", "March"),
													 CodeLocalise("Global.April.Label", "April"), CodeLocalise("Global.May.Label", "May"), CodeLocalise("Global.June.Label", "June"),
													 CodeLocalise("Global.July.Label", "July"), CodeLocalise("Global.August.Label", "August"), CodeLocalise("Global.September.Label", "September"),
													 CodeLocalise("Global.October.Label", "October"), CodeLocalise("Global.November.Label", "November"), CodeLocalise("Global.December.Label", "December")};

      for (var i = 0; i < 12; i++)
      {
        FromMonthDropDownList.Items.Add(new ListItem(months[i], (i + 1).ToString()));
        ToMonthDropDownList.Items.Add(new ListItem(months[i], (i + 1).ToString()));
      }

      for (var i = 2005; i <= DateTime.Now.Year; i++)
      {
        FromYearDropDownList.Items.Add(new ListItem((i).ToString(), (i).ToString()));
        ToYearDropDownList.Items.Add(new ListItem((i).ToString(), (i).ToString()));
      }

      FromMonthDropDownList.Items.AddLocalisedTopDefault("Month.TopDefault.NoEdit", "month");
      FromYearDropDownList.Items.AddLocalisedTopDefault("Year.TopDefault.NoEdit", "year");

      ToMonthDropDownList.Items.AddLocalisedTopDefault("Month.TopDefault.NoEdit", "month");
      ToYearDropDownList.Items.AddLocalisedTopDefault("Year.TopDefault.NoEdit", "year");

      #endregion
    }

    /// <summary>
    /// Populates the job orders filter sub menu.
    /// </summary>
    /// <param name="jobListFilter">The job list filter.</param>
    /// <param name="rebuild">Whether to rebuild the drop-down</param>
    protected void PopulateJobOrdersFilterSubMenu(JobListFilter? jobListFilter, bool rebuild = true)
    {
      JobOrdersFilterSubMenuDropDown.Style["display"] = "none";

      if (rebuild)
      {
        JobOrdersFilterSubMenuDropDown.Items.Clear();

        JobOrdersFilterSubMenuDropDown.Items.AddEnum(JobListFilter.ForeignLaborJobsAll, "Foreign labor certification all");
        JobOrdersFilterSubMenuDropDown.Items.AddEnum(JobListFilter.ForeignLaborJobsH2A, "Foreign labor certification H2A");
        JobOrdersFilterSubMenuDropDown.Items.AddEnum(JobListFilter.ForeignLaborJobsH2B, "Foreign labor certification H2B");
        JobOrdersFilterSubMenuDropDown.Items.AddEnum(JobListFilter.ForeignLaborJobsOther, "Foreign labor certification Other");
      }

      foreach (ListItem item in JobOrdersFilterSubMenuDropDown.Items)
      {
        item.Attributes["style"] = "display:none";
      }

      switch (jobListFilter)
      {
        case JobListFilter.ForeignLaborJobs:
        case JobListFilter.ForeignLaborJobsAll:
        case JobListFilter.ForeignLaborJobsH2A:
        case JobListFilter.ForeignLaborJobsH2B:
        case JobListFilter.ForeignLaborJobsOther:
          JobOrdersFilterSubMenuDropDown.Style["display"] = "block";
          break;
      }

      const string js = @"var JobOrderDashboard_SubFilter = new Array(); 
        JobOrderDashboard_SubFilter['ForeignLaborJobs'] = ['ForeignLaborJobsAll', 'ForeignLaborJobsH2A', 'ForeignLaborJobsH2B', 'ForeignLaborJobsOther'];";

      ClientScript.RegisterClientScriptBlock(GetType(), "JobOrderDashboard_SubFilter", js, true);
    }

		/// <summary>
		/// Binds the sub action drop down posting to staff.
		/// </summary>
    private void BindSubActionDropDownPostingToStaff()
    {
      #region Bind sub action to staff

			#region Get the Offices the manager handles work for

			var stateId = ServiceClientLocator.EmployerClient(App).GetManagerState(Convert.ToInt64(App.User.PersonId));

			var officeIds = new List<long?>();

			if (stateId.IsNull())
				officeIds = ServiceClientLocator.EmployerClient(App).GetOfficesPersonManages(Convert.ToInt64(App.User.PersonId)).Select(x => x.Id).ToList();
			else
				officeIds = ServiceClientLocator.EmployerClient(App).GetOfficesByState(Convert.ToInt64(stateId)).Select(x => x.Id).ToList();

			#endregion

      SubActionDropDown.Items.Clear();

			var criteria = new StaffMemberCriteria
			{
				OfficeIds = officeIds,
				Enabled = true,
				Blocked = false,
				FetchOption = CriteriaBase.FetchOptions.List
			};
			var staffMembers = ServiceClientLocator.EmployerClient(App).GetOfficeStaffMembersList(criteria);
			var staffQuery = staffMembers.Select(s => new { s.Id, DisplayText = s.LastName + ", " + s.FirstName });

			SubActionDropDown.DataSource = staffQuery;
      SubActionDropDown.DataValueField = "Id";
			SubActionDropDown.DataTextField = "DisplayText";
      SubActionDropDown.DataBind();
      SubActionDropDown.Items.AddLocalisedTopDefault("PostingStaffSubActionDropDown.TopDefault", "- select staff member name -");

      #endregion
    }

		/// <summary>
		/// Binds the sub action drop down to staff.
		/// </summary>
		private void BindSubActionDropDownToStaff()
		{
			#region Bind sub action to staff

			SubActionDropDown.Items.Clear();

			SubActionDropDown.DataSource = ServiceClientLocator.AccountClient(App).GetAssistUsers();
			SubActionDropDown.DataValueField = "Id";
			SubActionDropDown.DataTextField = "Name";
			SubActionDropDown.DataBind();
			SubActionDropDown.Items.AddLocalisedTopDefault("StaffSubActionDropDown.TopDefault", "- select staff member name -");

			#endregion
		}

    /// <summary>
    /// Binds the sub action drop down to job status.
    /// </summary>
    private void BindSubActionDropDownToJobStatus()
    {
      #region Bind sub action to job status

      SubActionDropDown.Items.Clear();

			if (JobCriteria.JobStatus != JobStatuses.Closed)
			{
				if (JobCriteria.JobStatus != JobStatuses.Active)
					SubActionDropDown.Items.AddEnum(JobStatuses.Active);

				SubActionDropDown.Items.AddEnum(JobStatuses.Closed);

				if (JobCriteria.JobStatus != JobStatuses.OnHold)
					SubActionDropDown.Items.AddEnum(JobStatuses.OnHold);
			}
			else
			{
				SubActionDropDown.Items.AddEnum(JobStatuses.Active);
			}


	    SubActionDropDown.Items.AddLocalisedTopDefault("StatusSubActionDropDown.TopDefault", "- select status -");

      #endregion
    }

    /// <summary>
    /// Binds the job orders list.
    /// </summary>
    private void BindJobOrdersList()
    {
      JobOrderList.DataBind();

			JobOrderHeaderTable.Visible = SubActionTable.Visible = JobOrderList.Items.Count > 0;
    }

	  /// <summary>
    /// Handles the DataBound event of the ItemsList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void ItemsList_DataBound  (object sender, ListViewItemEventArgs e)
    {
			var job = (JobViewModel)e.Item.DataItem;

		  var jobIdLiteral = (Literal) e.Item.FindControl("JobIdLiteral");
		  jobIdLiteral.Text = job.Id.ToString();

      var url = (HtmlAnchor)e.Item.FindControl("JobReferrals_Url");   
     
      if (job.ReferralCount > 0)
        url.HRef = UrlBuilder.AssistPoolReferrals(job.Id.GetValueOrDefault(0)) + FilterQueryString;
      else
        url.Disabled = true;

			url = (HtmlAnchor)e.Item.FindControl("BusinessUnitUrl");
	    url.HRef = UrlBuilder.AssistEmployerProfile(job.BusinessUnitId.GetValueOrDefault(0)) + FilterQueryString;

      var issues = BuildIssueList(job.Issues);
      var issuesLiteral = (Literal)e.Item.FindControl("IssuesLiteral");
      var showAdditionalIssuesLink = (HyperLink)e.Item.FindControl("ShowAdditionalIssuesLink");
      if (issues.Count < 3)
      {
        issuesLiteral.Text = string.Join(", ", issues);
      }
      else
      {
        issuesLiteral.Text = string.Join(", ", issues.Take(1)) + ", ";
        var additionalIssuesLiteral = (Literal)e.Item.FindControl("AdditionalIssuesLiteral");
        additionalIssuesLiteral.Text = string.Join(", ", issues.Skip(1));

        showAdditionalIssuesLink.Visible = true;
        showAdditionalIssuesLink.Text = CodeLocalise("AdditionalIssuesLink.Text", "+ {0} more", (issues.Count - 1));
        showAdditionalIssuesLink.Attributes.Add("onclick",
                                                string.Format("ShowAdditionalIssues({0});",
                                                              job.Id.ToString()));
				showAdditionalIssuesLink.Attributes.Add("onkeypress",
																				string.Format("ShowAdditionalIssues({0});",
																											job.Id.ToString()));
      }
	    
	    if (!RowIds.IsNotNullOrEmpty()) return;
      if (!RowIds.Contains(e.Item.DataItemIndex)) return;
      
			var checkBox = (CheckBox) e.Item.FindControl("SelectorCheckBox");
      checkBox.Checked = true;
    }

    /// <summary>
    /// Binds the job filter drop down.
    /// </summary>
    private void BindJobOrderFilters()
    {
      #region BindJobOrderFilters
			
			switch (App.Settings.Theme)
			{
				case FocusThemes.Education:
					JobOrdersFilterDropDown.Items.AddEnum(JobListFilter.JobsAndInternships, "Jobs and internships");
					JobOrdersFilterDropDown.Items.AddEnum(JobListFilter.JobOrders, "Job orders");
					JobOrdersFilterDropDown.Items.AddEnum(JobListFilter.Internships, "Internships");
					break;

				default:
			    JobOrdersFilterDropDown.Items.Add(CodeLocalise("JobOrdersFilterDropDown.Default.NoEdit", "All #EMPLOYMENTTYPES#:LOWER"));
					JobOrdersFilterDropDown.Items.AddEnum(JobListFilter.NewJobs, "New #EMPLOYMENTTYPES#:LOWER (in the last 3 days)");
					if (!App.Settings.HideJobConditions)
					{
						JobOrdersFilterDropDown.Items.AddEnum(JobListFilter.CourtOrderedAffirmativeActionJobs, "Court ordered affirmative action #EMPLOYMENTTYPES#:LOWER");
						JobOrdersFilterDropDown.Items.AddEnum(JobListFilter.FederalContractorJobs, "Federal contractor #EMPLOYMENTTYPES#:LOWER");
						JobOrdersFilterDropDown.Items.AddEnum(JobListFilter.ForeignLaborJobs, "Foreign labor #EMPLOYMENTTYPES#:LOWER");
					}
					break;
			}

			JobOrdersStatusDropDown.Items.Add(new ListItem(CodeLocalise("JobOrdersStatusDropDown.AllStatuses", "All statuses"), ""));
     	JobOrdersStatusDropDown.Items.AddEnum(JobStatuses.Active, "Active #EMPLOYMENTTYPE# ");
			JobOrdersStatusDropDown.Items.AddEnum(JobStatuses.Closed, "Closed #EMPLOYMENTTYPE# ");
      JobOrdersStatusDropDown.Items.AddEnum(JobStatuses.Draft, "Draft #EMPLOYMENTTYPE# ");
      JobOrdersStatusDropDown.Items.AddEnum(JobStatuses.OnHold, "On hold #EMPLOYMENTTYPE# ");
      JobOrdersStatusDropDown.Width = App.Settings.Theme == FocusThemes.Workforce ? new Unit("170px") : new Unit("225px");
	    JobOrdersStatusDropDown.SelectedIndex = 1; // Keep active as the default value


      PopulateJobOrdersFilterSubMenu(JobFilter);

      if (_reApplyFilter)
      {
        JobOrdersStatusDropDown.SelectedValue = JobCriteria.JobStatus.ToString();
        SetJobOrdersAndSubMenuDropDownFilterDropDowns();
      }
      else
      {
        switch (JobFilter)
        {
          case JobListFilter.ForeignLaborJobsAll:
          case JobListFilter.ForeignLaborJobsH2A:
          case JobListFilter.ForeignLaborJobsH2B:
          case JobListFilter.ForeignLaborJobsOther:
            JobOrdersFilterSubMenuDropDown.SelectedValue = JobFilter.ToString();
            JobOrdersFilterDropDown.SelectedValue = JobListFilter.ForeignLaborJobs.ToString();
            break;

          default:
            JobOrdersFilterDropDown.SelectedValue = JobFilter.ToString();
            break;
        }
      }

      #endregion
    }

    /// <summary>
    /// Handles the Sorting event of the JobOrdersList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
    protected void JobOrderList_Sorting(object sender, ListViewSortEventArgs e)
    {
      #region Job order list sorting

      if (e.SortExpression.Contains("date"))
      {
        switch (JobCriteria.JobStatus)
        {
          case JobStatuses.Active:
            e.SortExpression = "postedon " + e.SortExpression.Substring(e.SortExpression.Length - 4, 4);
            break;

          case JobStatuses.Closed:
            e.SortExpression = "closedon " + e.SortExpression.Substring(e.SortExpression.Length - 4, 4);
            break;

          case JobStatuses.OnHold:
            e.SortExpression = "heldon " + e.SortExpression.Substring(e.SortExpression.Length - 4, 4);
            break;

          default:
            e.SortExpression = "updatedon " + e.SortExpression.Substring(e.SortExpression.Length - 4, 4);
            break;
        }
      }
      JobCriteria.OrderBy = e.SortExpression;
      FormatJobOrderListSortingImages();

      #endregion
    }

    /// <summary>
    /// Unbinds the search.
    /// </summary>
    private void UnbindSearch()
    {
      if (JobCriteria == null)
        JobCriteria = new JobCriteria();

	    if (JobOrdersStatusDropDown.SelectedIndex != 0)
	    {
		    SelectedStatus = JobOrdersStatusDropDown.SelectedValueToEnum<JobStatuses>();
	    }
	    else
	    {
		    SelectedStatus = null;
	    }

			JobCriteria.JobStatus = SelectedStatus;

			if (JobOrderIssuesDropDown.SelectedIndex > 1) // Default is now with AND without issues so we don't need to filter on it
				JobCriteria.JobIssueFilter = JobOrderIssuesDropDown.SelectedValueToEnum<JobIssuesFilter>();
			else
				JobCriteria.JobIssueFilter = null;

      if (JobIDTextBox.TextTrimmed().IsNotNull())
        JobCriteria.JobId = JobIDTextBox.TextTrimmed().ToLong();

      if (JobTitleTextBox.TextTrimmed().IsNotNull())
        JobCriteria.JobTitle = JobTitleTextBox.TextTrimmed().NullIfEmpty();

      if (EmployerNameTextBox.TextTrimmed().IsNotNull())
        JobCriteria.BusinessUnitName = EmployerNameTextBox.TextTrimmed().NullIfEmpty();

      if (FromMonthDropDownList.SelectedIndex > 0 && FromYearDropDownList.SelectedIndex > 0)
      {
        JobCriteria.CreatedFrom = GetDate(FromMonthDropDownList, FromYearDropDownList);
      }

      if (ToMonthDropDownList.SelectedIndex > 0 && ToYearDropDownList.SelectedIndex > 0)
      {
        JobCriteria.CreatedTo = GetDate(ToMonthDropDownList, ToYearDropDownList, true);
      }

      if (HiringManagerUsernameTextBox.TextTrimmed().IsNotNull())
        JobCriteria.HiringManagerUsername = HiringManagerUsernameTextBox.TextTrimmed().NullIfEmpty();

			if (HiringManagerFirstNameTextBox.TextTrimmed().IsNotNull())
        JobCriteria.HiringManagerFirstName = HiringManagerFirstNameTextBox.TextTrimmed().NullIfEmpty();

			if (HiringManagerLastNameTextBox.TextTrimmed().IsNotNull())
        JobCriteria.HiringManagerLastName = HiringManagerLastNameTextBox.TextTrimmed().NullIfEmpty();

      var filter = JobOrdersFilterDropDown.SelectedValueToEnum<JobListFilter>();
      JobCriteria.JobListFilter = filter;      

      switch (filter)
      {
        case JobListFilter.ForeignLaborJobs:
          filter = JobOrdersFilterSubMenuDropDown.SelectedValueToEnum<JobListFilter>();
          break;
      }

      switch (filter)
      {
        case JobListFilter.ForeignLaborJobsAll:
          JobCriteria.ForeignLaborJobsAll = true;
          break;

        case JobListFilter.ForeignLaborJobsH2A:
          JobCriteria.ForeignLaborJobsH2A = true;
          break;

        case JobListFilter.ForeignLaborJobsH2B:
          JobCriteria.ForeignLaborJobsH2B = true;
          break;

        case JobListFilter.ForeignLaborJobsOther:
          JobCriteria.ForeignLaborJobsOther = true;
          break;

        case JobListFilter.NewJobs:
          var postedOn = DateTime.Now.AddDays(-3);
          JobCriteria.PostedOn = Convert.ToDateTime(String.Format("{0}-{1}-{2} 00:00:00.000", postedOn.Year, postedOn.Month, postedOn.Day));
          break;

        case JobListFilter.FederalContractorJobs:
          JobCriteria.FederalContractor = true;
          break;

        case JobListFilter.CourtOrderedAffirmativeActionJobs:
          JobCriteria.CourtOrderedAffirmativeAction = true;
          break;

				case JobListFilter.JobsAndInternships:
		      JobCriteria.JobType = JobTypes.Job | JobTypes.InternshipPaid | JobTypes.InternshipUnpaid;
					break;

				case JobListFilter.JobOrders:
					JobCriteria.JobType = JobTypes.Job;
					break;

				case JobListFilter.Internships:
					JobCriteria.JobType = JobTypes.InternshipPaid | JobTypes.InternshipUnpaid;
					break;

				case JobListFilter.None:
		      JobCriteria.ForeignLaborJobsAll =
			      JobCriteria.ForeignLaborJobsH2A =
			      JobCriteria.ForeignLaborJobsH2B =
			      JobCriteria.ForeignLaborJobsOther =
			      JobCriteria.FederalContractor =
			      JobCriteria.CourtOrderedAffirmativeAction = null;
		      JobCriteria.JobType = null;
		      JobCriteria.PostedOn = DateTime.MinValue;
		      break;
      }

			if (App.Settings.OfficesEnabled && OfficeFilter.FilterEnabled())
			{
        var officeFilterModel = _reApplyFilter && JobCriteria.OfficeFilterModel.IsNotNull() ? JobCriteria.OfficeFilterModel : OfficeFilter.Unbind();

				if (officeFilterModel.OfficeId.IsNotNull() && officeFilterModel.OfficeId > 0)
				{
					JobCriteria.OfficeIds = new List<long?> { officeFilterModel.OfficeId };
				}
				else if (officeFilterModel.OfficeGroup == OfficeGroup.MyOffices)
				{
					JobCriteria.OfficeIds = GetAllUserOfficeIds();
				}
				else if(officeFilterModel.OfficeGroup == OfficeGroup.StatewideOffices)
				{
					JobCriteria.OfficeIds = null;
				}

				JobCriteria.StaffMemberId = officeFilterModel.StaffMemberId;

				if (officeFilterModel.AssignmentType.IsNotNull())
				{
					if (officeFilterModel.AssignmentType == AssignmentType.MyAssignments)
						JobCriteria.StaffMemberId = Convert.ToInt64(App.User.PersonId);
					else
						JobCriteria.StaffMemberId = null;
				}

        JobCriteria.OfficeFilterModel = officeFilterModel;

			}
			else
			{
				JobCriteria.OfficeIds = null;
				JobCriteria.StaffMemberId = null;
			}
    }

    /// <summary>
    /// Binds the action drop down.
    /// </summary>
    private void BindActionDropDown()
    {
      #region Bind action drop down

      JobOrdersActionDropDown.Items.Clear();
      JobOrdersActionDropDown.Items.AddLocalisedTopDefault("ActionDropDown.TopDefault.NoEdit", "- select action -");

	    if (!IsAssistEmployersAdministrator())
	    {
				ActionLabelCell.Visible = JobOrdersActionDropDown.Visible = JobOrdersActionButton.Visible = false;
				return;
	    }

			JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AccessEmployeeAccount, "Access #BUSINESS#:LOWER account"), ActionTypes.AccessEmployeeAccount.ToString()));
      JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.RequestFollowUp, "Add follow-up"), ActionTypes.RequestFollowUp.ToString()));
      JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AddNote, "Add note or reminder"), ActionTypes.AddNote.ToString()));

      var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(Convert.ToInt64(App.User.UserId));
      if (App.Settings.OfficesEnabled && userDetails.PersonDetails.Manager.IsNotNull() && Convert.ToBoolean(userDetails.PersonDetails.Manager))
      {
        JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignJobOrderToStaff, "Assign #POSTINGTYPES#:LOWER to staff"), ActionTypes.AssignJobOrderToStaff.ToString()));
      }

			if (App.User.IsInRole(Constants.RoleKeys.AssistEmployersSendMessages))
				JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.EmailEmployee, "Email #BUSINESS#:LOWER(es)"), ActionTypes.EmailEmployee.ToString()));

      JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.MatchJob, "Find #CANDIDATETYPES#:LOWER for job"), ActionTypes.MatchJob.ToString()));
      JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.MarkJobIssuesResolved, "Mark as resolved"), ActionTypes.MarkJobIssuesResolved.ToString()));
      JobOrdersActionDropDown.SelectedIndex = 0;

      #endregion
    }

    /// <summary>
    /// Binds the issues drop down.
    /// </summary>
    private void BindJobIssuesDropDown()
    {
      JobOrderIssuesDropDown.Items.Clear();
      JobOrderIssuesDropDown.Items.AddLocalisedTopDefault("ActionDropDown.TopDefault", "- select -");
			JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.Both, "With and without issues");
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.Any, "With any issues");
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.None, "With no issues");
			if (App.Settings.JobIssueFlagLowMinimumStarMatches)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.LowQualityMatches, "Low number of high-quality matches");
			if (App.Settings.JobIssueFlagLowReferrals)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.LowReferralActivity, "Low referral activity");
			if (App.Settings.JobIssueFlagNotClickingReferrals)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.NotViewingReferrals, "Not clicking on referred applicants");
			if (App.Settings.JobIssueFlagSearchingNotInviting)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.SearchNotInviting, "Searching but not inviting");
			if (App.Settings.JobIssueFlagClosingDateRefreshed)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.ClosingDateRefreshed, "Refreshing closing date");
			if (App.Settings.JobIssueFlagJobClosedEarly)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.EarlyJobClosing, "Pending closing date");
			if (App.Settings.JobIssueFlagJobAfterHiring)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.PostHireFollowUp, "Suggesting post-hire follow-up");
			if (App.Settings.JobIssueFlagNegativeFeedback)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.NegativeSurveyResponse, "Giving negative survey responses");
			if (App.Settings.JobIssueFlagPositiveFeedback)
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.PositiveSurveyResponse, "Giving positive survey responses");
      JobOrderIssuesDropDown.Items.AddEnum(JobIssuesFilter.FollowUpRequested, "Requiring follow-up");

      if (_reApplyFilter)
      {
        JobOrderIssuesDropDown.SelectedValue = JobCriteria.JobIssueFilter.ToString();
      }
      else
      {
        JobOrderIssuesDropDown.SelectedIndex = 1;
      }
      
    }

    /// <summary>
    /// Set the OfficeFilterModel for the Office Filter control.
    /// </summary>
    private void RebindOfficeFilter()
    {
      OfficeFilter.OfficeFilterModel = JobCriteria.OfficeFilterModel;
    }		
    
    /// <summary>
    /// Binds text boxes if re-applying filter
    /// </summary>
    private void BindSearchTextFields()
    {
      JobIDTextBox.Text = JobCriteria.JobId.ToString();
      JobTitleTextBox.Text = JobCriteria.JobTitle;
      EmployerNameTextBox.Text = JobCriteria.BusinessUnitName;
      HiringManagerFirstNameTextBox.Text = JobCriteria.HiringManagerFirstName;
      HiringManagerLastNameTextBox.Text = JobCriteria.HiringManagerLastName;
      HiringManagerUsernameTextBox.Text = JobCriteria.HiringManagerUsername;

      if (JobCriteria.CreatedFrom != null)
      {
        FromMonthDropDownList.SelectedValue = JobCriteria.CreatedFrom.Value.Month.ToString();
        FromYearDropDownList.SelectedValue = JobCriteria.CreatedFrom.Value.Year.ToString();
      }

      if (JobCriteria.CreatedTo != null)
      {
        ToMonthDropDownList.SelectedValue = JobCriteria.CreatedTo.Value.Month.ToString();
        ToYearDropDownList.SelectedValue = JobCriteria.CreatedTo.Value.Year.ToString();
      }

      // Expand the Job Order Search Panel if any of the fields have values
      if (JobCriteria.CreatedFrom != null || JobCriteria.CreatedTo != null ||
          JobCriteria.JobId.ToString().IsNotNullOrEmpty() || JobCriteria.JobTitle.IsNotNullOrEmpty() ||
          JobCriteria.BusinessUnitName.IsNotNullOrEmpty() || JobCriteria.HiringManagerFirstName.IsNotNullOrEmpty() ||
          JobCriteria.HiringManagerLastName.IsNotNullOrEmpty() || JobCriteria.HiringManagerUsername.IsNotNullOrEmpty())
      {
        JobOrderSearchPanelExtender.Collapsed = false;
      }

    }

    #endregion

    #region LocaliseUI

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      SubActionButton.Text = CodeLocalise("JobOrdersSubActionButton.Text", "Go");
      JobFilterButton.ToolTip =  JobFilterButton.Text = CodeLocalise("JobFilterButton.Text.NoEdit.NoEdit", "Go");

			FilterClearButton.Text = CodeLocalise( "FilterResetButton.Text", "Clear" );
			FilterClearButton.ToolTip = CodeLocalise( "FilterResetButton.TootTip", "Clear all filters back to default values" );

			EducationFilterClearButton.Text = CodeLocalise( "FilterResetButton.Text", "Clear" );
			EducationFilterClearButton.ToolTip = CodeLocalise( "FilterResetButton.TootTip", "Clear all filters back to default values" );

			JobOrdersActionButton.Text = CodeLocalise("JobOrdersActionButton.Text.NoEdit", "Go");
      LocaliseCreateNewEmployerAndJobOrderButton.Text = EditLocalisationLink("CreateNewEmployerAndJobOrderButton.Text.NoEdit");
			CreateNewEmployerAndJobOrderButton.ToolTip = CreateNewEmployerAndJobOrderButton.Text = CodeLocalise("CreateNewEmployerAndJobOrderButton.Text.NoEdit", "Create new #BUSINESS#:LOWER & #EMPLOYMENTTYPE#:LOWER");
			JobOrderIDValidator.ErrorMessage = CodeLocalise("JobOrderIDValidator.ErrorMessage", "#EMPLOYMENTTYPE# ID must be numeric.");
      JobOrdersActionDropDownRequired.ErrorMessage = CodeLocalise("ActionDropDownRequired.ErrorMessage", "Action required");
			ActivityReportButton.ToolTip = ActivityReportButton.Text = CodeLocalise("ActivityReportButton.Text", "Run activity report");

			FindABusinessAccountHRef.InnerText = App.Settings.Theme == FocusThemes.Workforce ? CodeLocalise("FindAnEmployer.Label", "Find a #BUSINESS#:LOWER account") : CodeLocalise("FindAnEmployer.Label", "Find an #BUSINESS#:LOWER account");
        //Accessibility 508 Compliance
        var selectorAllCheckBoxes = (CheckBox)JobOrderList.FindControl("SelectorAllCheckBoxes");
        if (selectorAllCheckBoxes.IsNotNull())
            selectorAllCheckBoxes.InputAttributes.Add("Title", "Select All CheckBoxes");
    }

    /// <summary>
    /// Registers some variables to be used in JavaScript
    /// </summary>
    private void JavaScriptHelpers()
    {
      var variables = new NameValueCollection
		  {
        {"OfficeFilter", OfficeFilter.ClientID},
        {"JobOrdersFilterDropDown", JobOrdersFilterDropDown.ClientID},
        {"JobOrdersFilterSubMenuDropDown", JobOrdersFilterSubMenuDropDown.ClientID},
		    {"JobOrdersStatusDropDown", JobOrdersStatusDropDown.ClientID},
        {"FindEmployerPanel", FindEmployerPanel.ClientID}
      };

      RegisterCodeValuesJson("JobOrderDashboard_Variables", "JobOrderDashboard_Variables", variables);
    }

    #endregion

    #region Events

    /// <summary>
    /// Handles the Completed event of the JobAction control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void JobAction_Completed(object sender, EventArgs e)
    {
      JobOrderList.DataBind();
    }

    /// <summary>
    /// Handles the Selecting event of the SearchResultDataSource control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      if (JobCriteria.IsNull())
      {
        if (!_reApplyFilter)
        {
          var officeIds =
            ServiceClientLocator.EmployerClient(App)
              .GetOfficesList(new OfficeCriteria {PersonId = Convert.ToInt64(App.User.PersonId)})
              .Select(x => x.Id)
              .ToList();
          JobCriteria = new JobCriteria
          {
            JobStatus = JobStatuses.Active,
            OfficeIds = officeIds,
            OrderBy = "postedon desc"
          };
        }
      }

      e.InputParameters["criteria"] = JobCriteria;
      App.SetSessionValue("JobOrderDashboard:SearchCriteria", JobCriteria);
    }

    /// <summary>
    /// Handles the Clicked event of the CreateNewEmployerAndJobOrderButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CreateNewEmployerAndJobOrderButton_Clicked(object sender, EventArgs e)
    {
      Response.Redirect(UrlBuilder.AssistRegisterEmployer());
    }

		/// <summary>
		/// Handles the Clicked event of the RunActivityReportButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void RunActivityReportButton_Clicked(object sender, EventArgs e)
		{
			EmployerActivityReportCriteriaModal.Show();
		}

    /// <summary>
    /// Handles the Clicked event of the ActionButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void JobOrdersActionButton_Clicked(object sender, EventArgs e)
    {
      #region Job orders action

			// Hide the sub action controls
	    SubActionRow.Visible = false;
			SelectedAction = JobOrdersActionDropDown.SelectedValueToEnum<ActionTypes>();

      _ids = GetSelectedIds();
      RowIds = _ids;

      if (_ids.Count == 0)
      {
        ShowError(ErrorTypes.NoJobSelected);
        return;
      }

      switch (SelectedAction)
      {
        case ActionTypes.AccessEmployeeAccount:
          if (_ids.Count == 1)
          {
            _employeeId = GetSelectedValue(_ids[0], "EmployeeId");
            var singleSignOnKey = ServiceClientLocator.AuthenticationClient(App).CreateEmployeeSingleSignOn(_employeeId, null);

	          var blockedPopupMessage = CodeLocalise("AccessEmployeePopupBlocked.Error.Text", "Unable to access #BUSINESS#:LOWER account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.");

	          var script = string.Format("$(document).ready(function (){{Business_TalentPopup = showPopup('{0}{1}','{2}');}});", App.Settings.TalentApplicationPath, UrlBuilder.SingleSignOn(singleSignOnKey, false), blockedPopupMessage);

            Page.ClientScript.RegisterStartupScript(GetType(), "AccessEmployee", script, true);
            RowIds = null;

						JobOrdersActionDropDown.SelectedValue = "";
          }
          else
          {
            ShowError(ErrorTypes.MultipleJobsSelected);
          }
          break;

        case ActionTypes.EmailEmployee:
          var employeeIds = new List<long>();

          foreach (var id in _ids)
          {
            _employeeId = GetSelectedValue(id, "EmployeeId");

            if (!employeeIds.Contains(_employeeId))
              employeeIds.Add(_employeeId);
          }

          EmailEmployee.Visible = true;
          EmailEmployee.Show(employeeIds);
          RowIds = null;
					JobOrdersActionDropDown.SelectedValue = "";

          break;

        case ActionTypes.AssignEmployerToStaff:
          PrepareAssignEmployer();
          break;

				case ActionTypes.AssignJobOrderToStaff:
      		PrepareAssignPosting();
      		break;

        case ActionTypes.MatchJob:
          if (_ids.Count == 1)
          {
            var jobId = GetSelectedValue(_ids[0], "Id");
            Response.Redirect(UrlBuilder.AssistPool(TalentPoolListTypes.MatchesForThisJob, jobId));
						JobOrdersActionDropDown.SelectedValue = "";
          }
          else
          {
            ShowError(ErrorTypes.MultipleJobsSelected);
          }
          break;

        case ActionTypes.AddNote:
          if (_ids.Count == 1)
          {
            var jobId = GetSelectedValue(_ids[0], "Id");
            NotesAndReminders.Show(jobId, EntityTypes.Job, !IsAssistEmployersAdministrator());
            RowIds = null;
						JobOrdersActionDropDown.SelectedValue = "";
          }
          else
          {
            ShowError(ErrorTypes.MultipleJobsSelected);
          }
          break;
          case ActionTypes.MarkJobIssuesResolved:
          if (_ids.Count == 1)
          {
            JobIssueResolution.Show(GetSelectedValue(_ids[0], "Id"), GetSelectedStringValue(_ids[0], "JobTitle"));
            RowIds = null;
            JobOrdersActionDropDown.SelectedIndex = 0;
          }
          else
          {
            ShowError(ErrorTypes.MultipleJobsSelected);
          }
          break;

				case ActionTypes.RequestFollowUp:
					if (_ids.Count == 1)
					{
						var jobId = GetSelectedValue(_ids[0], "Id");
						RequestFollowUpModal.Show(jobId, EntityTypes.Job);
            RowIds = null;
						JobOrdersActionDropDown.SelectedValue = "";
					}
					else
          {
            ShowError(ErrorTypes.MultipleJobsSelected);
          }
          break;

      }

      BindJobOrdersList();

      #endregion
    }

    /// <summary>
    /// Handles the Clicked event of the SubActionButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SubActionButton_Clicked(object sender, EventArgs e)
    {
      
      #region Job orders sub action

      _ids = GetSelectedIds();
      RowIds = _ids;

      if (_ids.Count == 0)
      {
        ShowError(ErrorTypes.SubActionNoneSelected);
        return;
      }

      SelectedAction = JobOrdersActionDropDown.SelectedValueToEnum<ActionTypes>();

      if (SubActionDropDown.SelectedIndex == 0)
      {
				if (SelectedAction == ActionTypes.AssignEmployerToStaff || SelectedAction == ActionTypes.AssignJobOrderToStaff)
          ShowError(ErrorTypes.SubActionStaff);

        return;
      }
      
      switch (SelectedAction)
      {
        case ActionTypes.AssignEmployerToStaff:
          var adminId = SubActionDropDown.SelectedValueToLong();

          foreach (var id in _ids)
          {
						var selectedJobId = GetSelectedValue(id, "EmployeeId");
						ServiceClientLocator.EmployerClient(App).AssignEmployerToAdmin(adminId, selectedJobId);
          }

					Confirmation.Show(CodeLocalise("AssignEmployerConfirmation.Title", "#BUSINESS#(es) assigned to staff"),
														CodeLocalise("AssignEmployeeConfirmation.Body", "Hiring manager(s) assigned to the staff member."),
                            CodeLocalise("CloseModal.Text", "OK"));

          break;

				case ActionTypes.AssignJobOrderToStaff:
					var staffId = SubActionDropDown.SelectedValueToLong();
      		
					ServiceClientLocator.JobClient(App).AssignJobToStaffMember(GetSelectedJobIds(_ids), staffId);

					var staffName = SubActionDropDown.SelectedItem.Text;

					Confirmation.Show(CodeLocalise("AssignJobOrderConfirmation.Title", "#EMPLOYMENTTYPE#(s) assigned to staff"),
														CodeLocalise("AssignJobOrderConfirmation.Body", "#EMPLOYMENTTYPE#(s) assigned to {0}", staffName),
														CodeLocalise("CloseModal.Text", "OK"));

					break;
      }

      RowIds = null;
      BindJobOrdersList();
	    SubActionRow.Visible = false;

      #endregion
    }

	  protected void FilterClearButton_Clicked(object sender, EventArgs e)
	  {
			Response.Redirect( UrlBuilder.JobOrderDashboard() );
		}

	  /// <summary>
    /// Handles the Clicked event of the JobFilterButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void JobFilterButton_Clicked(object sender, EventArgs e)
    {
      var originalOrder = JobCriteria.IsNotNull() ? JobCriteria.OrderBy : "postedon desc";
      var originalStatus = JobCriteria.IsNotNull() ? JobCriteria.JobStatus : JobStatuses.Active;

      if (!_reApplyFilter)
	      SearchResultListPager.ReturnToFirstPage();

			SubActionRow.Visible = false;

	    if (_reApplyFilter) RebindOfficeFilter();

	    RowIds = null;
	    UnbindSearch();
	    BindActionDropDown();

      if (JobCriteria.JobStatus == originalStatus && !originalOrder.Contains("date"))
      {
        JobCriteria.OrderBy = originalOrder;
      }
      else
      {
        switch (JobCriteria.JobStatus)
        {
          case JobStatuses.Active:
            JobCriteria.OrderBy = "postedon desc";
            break;

          case JobStatuses.Closed:
            JobCriteria.OrderBy = "closedon desc";
            break;

          case JobStatuses.OnHold:
            JobCriteria.OrderBy = "heldon desc";
            break;

          default:
            JobCriteria.OrderBy = "updatedon desc";
            break;
        }
      }
      
      BindJobOrdersList();
      
      // Set the sort images
      if (SearchResultListPager.TotalRowCount > 0) FormatJobOrderListSortingImages();

      // Save the Search Criteria to a session object so that the user's selections are retained if returning from Job Seeker Profile
      App.SetSessionValue("JobOrderDashboard:SearchCriteria", JobCriteria);
      App.SetSessionValue("JobOrderDashboard:SearchCriteriaSet", true);

    }

    #endregion

    #region Errors

    /// <summary>
    /// Shows the error modal.
    /// </summary>
    private void ShowError(ErrorTypes errorType)
    {
      switch (errorType)
      {
        #region error messages

        case ErrorTypes.NoJobSelected:
          JobOrdersActionDropDownValidator.ErrorMessage = HtmlLocalise("SingleCheckbox.Error.Text",
                                                                       "You must select a record to run this action");
          JobOrdersActionDropDownValidator.IsValid = false;
          break;

        case ErrorTypes.MultipleJobsSelected:

          JobOrdersActionDropDownValidator.ErrorMessage = HtmlLocalise("MultipleCheckbox.Error.Text",
                                                                       "You can only select one record at a time to run this action");
          JobOrdersActionDropDownValidator.IsValid = false;
          break;

        case ErrorTypes.SubActionNoneSelected:
          SubActionButtonValidator.ErrorMessage = HtmlLocalise("MultipleCheckbox.Error.Text",
                                                                       "You must select a record to run this action");
          SubActionButtonValidator.IsValid = false;
          break;

        case ErrorTypes.SubActionMultipleSelected:

          SubActionButtonValidator.ErrorMessage = HtmlLocalise("MultipleCheckbox.Error.Text",
                                                                       "You can only select one record at a time to run this action");
          SubActionButtonValidator.IsValid = false;
          break;

        case ErrorTypes.SubActionStatus:

          SubActionButtonValidator.ErrorMessage = HtmlLocalise("SubActionStatus.Error.Text",
                                                                       "Please select a status");
          SubActionButtonValidator.IsValid = false;
          break;

        case ErrorTypes.SubActionActivity:

          SubActionButtonValidator.ErrorMessage = HtmlLocalise("SubActionActivity.Error.Text",
                                                                       "Please select an activity");
          SubActionButtonValidator.IsValid = false;
          break;

        case ErrorTypes.SubActionStaff:

          SubActionButtonValidator.ErrorMessage = HtmlLocalise("SubActionStaff.Error.Text",
                                                                       "Please select a staff member");
          SubActionButtonValidator.IsValid = false;
          break;

				case ErrorTypes.JobClosedMoreThan30DaysAgo:

		      SubActionButtonValidator.ErrorMessage = HtmlLocalise("JobClosedMoreThan30DaysAgo.Error.Text",
		                                                           "The selected job must have been closed in the last 30 days to run this action");
		      SubActionButtonValidator.IsValid = false;
		      break;

		      #endregion
      }
    }

    #endregion

    #region Formatting

    /// <summary>
    /// Handles the LayoutCreated event of the JobOrdersList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void JobOrdersList_LayoutCreated(object sender, EventArgs e)
    {
      #region Set labels

			JobOrderList.FindControl("SelectorAllCheckBoxes").Visible = IsAssistEmployersAdministrator();
			((Literal)JobOrderList.FindControl("JobIdHeader")).Text = CodeLocalise("JobIdHeader.Text", "#POSTINGTYPE# Id");
			((Literal)JobOrderList.FindControl("JobTitleHeader")).Text = CodeLocalise("JobNameHeader.Text", "#POSTINGTYPE# title");
			((Literal)JobOrderList.FindControl("EmployerNameHeader")).Text = CodeLocalise("EmployerNameHeader.Text", "#BUSINESS# name");
      ((Literal)JobOrderList.FindControl("DateHeader")).Text = CodeLocalise("DateHeader.Text", "Date");
      ((Literal)JobOrderList.FindControl("ReferralsHeader")).Text = CodeLocalise("ReferralsHeader.Text", "Pending<br />referrals");
      ((Literal)JobOrderList.FindControl("IssuesHeaderLiteral")).Text = CodeLocalise("IssuesHeaderLiteral.Text", "Issues");
	    
	    #endregion

	    ApplyBranding();
    }

    /// <summary>
    /// Formats the posted and expiry dates.
    /// </summary>
    /// <param name="job">The job view.</param>
    /// <returns></returns>
		protected string FormatDates(JobViewModel job)
    {
      #region Format dates

      var formattedDates = _problemWithDatesDetails;

      switch (job.JobStatus)
      {
        case JobStatuses.Active:
					formattedDates = String.Format(_activeDetails, job.PostedOn.FormatDateTime(_jobDateFormat, _jobDateDefaultValue), job.ClosingOn.FormatDateTime(_jobDateFormat, _jobDateDefaultValue));
          break;

        case JobStatuses.OnHold:
					formattedDates = String.Format(_onHoldDetails, job.PostedOn.FormatDateTime(_jobDateFormat, _jobDateDefaultValue), job.HeldOn.FormatDateTime(_jobDateFormat, _jobDateDefaultValue));
          break;

        case JobStatuses.Closed:
					formattedDates = String.Format(_closedDetails, job.PostedOn.FormatDateTime(_jobDateFormat, _jobDateDefaultValue), job.ClosedOn.FormatDateTime(_jobDateFormat, _jobDateDefaultValue));
          break;

        case JobStatuses.Draft:
					formattedDates = String.Format(_draftDetails, job.UpdatedOn.FormatDateTime(_jobDateFormat));
          break;
      }

      return formattedDates;

      #endregion
    }

    /// <summary>
    /// Formats the Job order list sorting images.
    /// </summary>
    private void FormatJobOrderListSortingImages()
    {
      #region Format images

      // Reset image URLs
      ((ImageButton)JobOrderList.FindControl("JobTitleSortAscButton")).ImageUrl =
				((ImageButton)JobOrderList.FindControl("JobIdSortAscButton")).ImageUrl =
        ((ImageButton)JobOrderList.FindControl("EmployerNameSortAscButton")).ImageUrl =
				((ImageButton)JobOrderList.FindControl("DateSortAscButton")).ImageUrl =
        ((ImageButton)JobOrderList.FindControl("ReferralSortAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
      ((ImageButton)JobOrderList.FindControl("JobTitleSortDescButton")).ImageUrl =
				((ImageButton)JobOrderList.FindControl("JobIdSortDescButton")).ImageUrl =
        ((ImageButton)JobOrderList.FindControl("EmployerNameSortDescButton")).ImageUrl =
				((ImageButton)JobOrderList.FindControl("DateSortDescButton")).ImageUrl =
				((ImageButton)JobOrderList.FindControl("ReferralSortDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

      // Reenable the buttons
      ((ImageButton)JobOrderList.FindControl("JobTitleSortAscButton")).Enabled =
				((ImageButton)JobOrderList.FindControl("JobIdSortAscButton")).Enabled =
        ((ImageButton)JobOrderList.FindControl("EmployerNameSortAscButton")).Enabled =
				((ImageButton)JobOrderList.FindControl("DateSortAscButton")).Enabled =
        ((ImageButton)JobOrderList.FindControl("ReferralSortAscButton")).Enabled =
				((ImageButton)JobOrderList.FindControl("JobTitleSortDescButton")).Enabled =
				((ImageButton)JobOrderList.FindControl("JobIdSortDescButton")).Enabled =
				((ImageButton)JobOrderList.FindControl("EmployerNameSortDescButton")).Enabled =
				((ImageButton)JobOrderList.FindControl("DateSortDescButton")).Enabled =
        ((ImageButton)JobOrderList.FindControl("ReferralSortDescButton")).Enabled = true;

      switch (JobCriteria.OrderBy.ToLower())
      {
				case "jobtitle asc":
					((ImageButton)JobOrderList.FindControl("JobTitleSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)JobOrderList.FindControl("JobTitleSortAscButton")).Enabled = false;
					break;
				case "jobtitle desc":
					((ImageButton)JobOrderList.FindControl("JobTitleSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage(); ;
					((ImageButton)JobOrderList.FindControl("JobTitleSortDescButton")).Enabled = false;
					break;
				case "id asc":
					((ImageButton)JobOrderList.FindControl("JobIdSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)JobOrderList.FindControl("JobIdSortAscButton")).Enabled = false;
					break;
				case "id desc":
					((ImageButton)JobOrderList.FindControl("JobIdSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage(); ;
					((ImageButton)JobOrderList.FindControl("JobIdSortDescButton")).Enabled = false;
					break;
				case "businessunitname asc":
          ((ImageButton)JobOrderList.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();;
          ((ImageButton)JobOrderList.FindControl("EmployerNameSortAscButton")).Enabled = false;
          break;
        case "businessunitname desc":
          ((ImageButton)JobOrderList.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();;
          ((ImageButton)JobOrderList.FindControl("EmployerNameSortDescButton")).Enabled = false;
          break;
				case "postedon  asc":
        case "closedon  asc":
        case "heldon  asc":
        case "updatedon  asc":
          ((ImageButton)JobOrderList.FindControl("DateSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();;
          ((ImageButton)JobOrderList.FindControl("DateSortAscButton")).Enabled = false;
          break;
        case "postedon desc":
        case "closedon desc":
        case "heldon desc":
        case "updatedon desc":
          ((ImageButton)JobOrderList.FindControl("DateSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();;
          ((ImageButton)JobOrderList.FindControl("DateSortDescButton")).Enabled = false;
          break;
        case "referralcount asc":
          ((ImageButton)JobOrderList.FindControl("ReferralSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();;
          ((ImageButton)JobOrderList.FindControl("ReferralSortAscButton")).Enabled = false;
          break;
        case "referralcount desc":
          ((ImageButton)JobOrderList.FindControl("ReferralSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();;
          ((ImageButton)JobOrderList.FindControl("ReferralSortDescButton")).Enabled = false;
          break;
      }

      #endregion
    }

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			var isEducation = App.Settings.Theme == FocusThemes.Education;
			ActivityReportButton.Visible = App.Settings.ShowTalentActivityReport;
			EducationFilterClearButton.Visible = isEducation;
			FilterClearButton.Visible = !isEducation;
		}

    #endregion

    #region Other methods

    /// <summary>
    /// Gets the employee status image.
    /// </summary>
    /// <param name="approvalStatuses">The approval statuses.</param>
    /// <returns></returns>
    protected string GetEmployeeStatusImage(ApprovalStatuses approvalStatuses)
    {
      string img, tooltip;

      switch (approvalStatuses)
      {
        case ApprovalStatuses.Approved:
					img = UrlBuilder.EmployerApprovedIcon();
          tooltip = _altEmployeeApproved;
          break;

        case ApprovalStatuses.Rejected:
					img = UrlBuilder.EmployerRejectedIcon();
          tooltip = _altEmployeeRejected;
          break;

        default:
					img = UrlBuilder.EmployerNewIcon();
          tooltip = _altEmployeeAwaitingApproval;
          break;
      }

      return string.Format("<img src='{0}' alt='{1}' title='{1}' />", img, tooltip);
    }

    /// <summary>
    /// Gets the selected checkbox id.
    /// </summary>
    /// <returns></returns>
    private List<int> GetSelectedIds()
    {
      _page = SearchResultListPager.CurrentPage;
      _pageSize = SearchResultListPager.PageSize;

     var items = (from item in JobOrderList.Items
                      let checkbox = (CheckBox) item.FindControl("SelectorCheckBox")
                      where checkbox.Checked
                      select item);

      var listItems = items.Select(item => item.DataItemIndex).ToList();
      return listItems;
    }

    /// <summary>
    /// Prepares the assign employer to staff action.
    /// </summary>
    private void PrepareAssignEmployer()
    {
      BindSubActionDropDownToStaff();
      SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignToStaffSubActionDropDownRequired.ErrorMessage", "Staff member required");
			SubActionButton.Text = CodeLocalise("EmployerSubActionButton.Text", "Assign");
			SubActionRow.Visible = true;
    }

		/// <summary>
		/// Prepares the assign employer to staff action.
		/// </summary>
		private void PrepareAssignPosting()
		{
			BindSubActionDropDownPostingToStaff();
			SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignPostingToStaffSubActionDropDownRequired.ErrorMessage", "Staff member required");
			SubActionButton.Text = CodeLocalise("PostingSubActionButton.Text", "Assign");
			SubActionRow.Visible = true;
		}

    /// <summary>
    /// Prepares the job statuses.
    /// </summary>
    private void PrepareJobStatuses()
    {
      BindSubActionDropDownToJobStatus();
      SubActionDropDownRequired.ErrorMessage = CodeLocalise("ChangeJobStatusSubActionDropDownRequired.ErrorMessage", "Status is required");
			SubActionButton.Text = CodeLocalise("JobOrdersSubActionButton.Text", "Go");
			SubActionRow.Visible = true;
    }

    /// <summary>
    /// Gets the selected value.
    /// </summary>
    /// <returns></returns>
    private long GetSelectedValue(int rowIndex, string type)
    {
      if (rowIndex >= _pageSize)
        rowIndex = rowIndex - _pageSize*(_page - 1);
     
      return long.Parse(JobOrderList.DataKeys[rowIndex][type].ToString());
    }

    /// <summary>
    /// Gets the selected value.
    /// </summary>
    /// <returns></returns>
    private string GetSelectedStringValue(int rowIndex, string type)
    {
      if (rowIndex >= _pageSize)
        rowIndex = rowIndex - _pageSize * (_page - 1);

      return JobOrderList.DataKeys[rowIndex][type].ToString();
    }

    /// <summary>
    /// Gets the selected date.
    /// </summary>
    /// <returns></returns>
	  private DateTime GetSelectedDateValue(int rowIndex, string type)
	  {
		  return DateTime.Parse(GetSelectedStringValue(rowIndex, type));
	  }

    /// <summary>
    /// Gets the selected jobIds.
    /// </summary>
    /// <returns></returns>
    private List<long> GetSelectedJobIds(IEnumerable<int> ids )
    {
      var tempIds = new List<int>();
      
      foreach (var id in ids)
      {
        if (id >= _pageSize)
        {
          tempIds.Add(id - _pageSize*(_page - 1));
        }
        else
        {
          tempIds.Add(id);
        }
      }

      return tempIds.Select(id =>
                              {
                                var dataKey = JobOrderList.DataKeys[id];
                                return dataKey != null ? long.Parse(dataKey["Id"].ToString()) : 0;
                              }).ToList();
    }

    /// <summary>
    /// Determines whether [is assist employers administrator].
    /// </summary>
    /// <returns>
    /// 	<c>true</c> if [is assist employers administrator]; otherwise, <c>false</c>.
    /// </returns>
    protected bool IsAssistEmployersAdministrator()
    {
      return App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
    }

    /// <summary>
    /// Determines whether the Assist user can create employers
    /// </summary>
    /// <returns>True if they can return employers, false otherwise</returns>
    protected bool CanCreateEmployers()
    {
      return App.User.IsInRole(Constants.RoleKeys.AssistEmployersCreateNewAccount);
    }

    /// <summary>
    /// Gets the jobs.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="startRowIndex">Start index of the row.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <returns></returns>
		public List<JobViewModel> GetJobs(JobCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
    {
     var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

     criteria.PageSize = maximumRows;
     criteria.PageIndex = pageIndex;

     var jobs = ServiceClientLocator.JobClient(App).GetJobs(criteria);
       _jobsCount = jobs.TotalCount;
      return jobs;
    }

    /// <summary>
    /// Gets the jobs count.
    /// </summary>
    /// <returns></returns>
    public int GetJobsCount()
    {
      return _jobsCount;
    }

    /// <summary>
    /// Gets the jobs count.
    /// </summary>
    /// <returns></returns>
    public int GetJobsCount(JobCriteria criteria)
    {
      return _jobsCount;
    }
  
    /// <summary>
    /// Gets the job filter.
    /// </summary>
    /// <value>The job filter.</value>
    private JobListFilter JobFilter
    {
      get
      {
        if (_jobFilter == null)
        {
          _jobFilter = JobListFilter.None;

          var filterValue = Page.RouteData.Values["filter"];
          var filter =  filterValue.IsNotNull() ? filterValue.ToString() : string.Empty;

          if (filter.IsNotNullOrEmpty())
          {
            JobListFilter jobFilter;
            Enum.TryParse(filter, true, out jobFilter);
            _jobFilter = jobFilter;
          }
        }

        return _jobFilter.Value;
      }
    }

    /// <summary>
    /// Gets the date.
    /// </summary>
    /// <param name="monthList">The month drop down list.</param>
    /// <param name="yearList">The year drop down list.</param>
    /// <param name="isLastDay">if set to <c>true</c> [is last day].</param>
    /// <returns></returns>
    private static DateTime? GetDate(ListControl monthList, ListControl yearList, bool isLastDay = false)
    {
      #region Get date
      DateTime? result = null;

      int month, year;

      int.TryParse(monthList.SelectedValue, out month);
      int.TryParse(yearList.SelectedValue, out year);

      if (month > 0 && month < 13)
      {
        result = new DateTime(year, month, 1);
        if (isLastDay) result = (result.Value.AddMonths(1).AddDays(-1));
      }

      return result;
      #endregion
    }

	  /// <summary>
	  /// Returns all the office Ids related to a user
	  /// </summary>
	  /// <returns>A list of office Ids</returns>
	  private List<long?> GetAllUserOfficeIds()
	  {
		  return ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { PersonId = Convert.ToInt64(App.User.PersonId) })
																							  .Select(x => x.Id)
																							  .ToList();
	  }

    /// <summary>
    /// Builds the issue list.
    /// </summary>
    /// <param name="issues">The issues.</param>
    /// <returns></returns>
    private List<string> BuildIssueList(JobIssuesModel issues)
   {
     var issueList = new List<string>();
     if (issues.IsNull()) return issueList;

      if (issues.JobIssues.IsNotNull())
      {
        var issueRecord = issues.JobIssues;
        if (issueRecord.LowQualityMatches)
          issueList.Add(CodeLocalise(JobIssuesFilter.LowQualityMatches, "Low number of high quality matches"));
        if (issueRecord.LowReferralActivity)
          issueList.Add(CodeLocalise(JobIssuesFilter.LowReferralActivity, "Low referral activity"));
        if (issueRecord.NotViewingReferrals)
          issueList.Add(CodeLocalise(JobIssuesFilter.NotViewingReferrals, "Not clicking on referred applicants"));
        if (issueRecord.SearchingButNotInviting)
          issueList.Add(CodeLocalise(JobIssuesFilter.SearchNotInviting, "Searching but not inviting"));
        if (issueRecord.ClosingDateRefreshed)
          issueList.Add(CodeLocalise(JobIssuesFilter.ClosingDateRefreshed, "Refreshing closing date"));
        if (issueRecord.EarlyJobClosing)
          issueList.Add(CodeLocalise(JobIssuesFilter.EarlyJobClosing, "Pending closing date"));
        if (issueRecord.NegativeSurveyResponse)
          issueList.Add(CodeLocalise(JobIssuesFilter.NegativeSurveyResponse, "Giving negative survey responses"));
        if (issueRecord.PositiveSurveyResponse)
          issueList.Add(CodeLocalise(JobIssuesFilter.PositiveSurveyResponse, "Giving positive survey responses"));
        if (issueRecord.FollowUpRequested)
          issueList.Add(CodeLocalise(JobIssuesFilter.FollowUpRequested, "Requiring follow-up"));
      }

      if (issues.PendingPostHireFollowUps.IsNotNullOrEmpty())
      {
        issues.PendingPostHireFollowUps.ForEach(
          x =>
          issueList.Add(CodeLocalise(JobIssuesFilter.PostHireFollowUp.ToString(),
                                     "Suggesting post-hire follow-up for {0} {1}", x.CandidateFirstName,
                                     x.CandidateLastName)));
      }

      for (var index = 1; index < issueList.Count; index++)
        issueList[index] = issueList[index].ToLower(CultureInfo.CurrentUICulture);

     return issueList;
   }

    /// <summary>
    /// Apply client-specific branding as required.
    /// </summary>
	  private void ApplyBranding()
	  {
		  JobOrderSearchHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
		  JobOrderSearchPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		  JobOrderSearchPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		  CreateJobOrderHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
		  CreateJobOrderCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		  CreateJobOrderCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		  FindEmployerHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
		  FindAnEmployerPanelCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		  FindAnEmployerPanelCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
	  }

    /// <summary>
    /// Populate the Job Filter and Sub Menu.
    /// </summary>
    private void SetJobOrdersAndSubMenuDropDownFilterDropDowns()
    {
      var filter = JobCriteria.JobListFilter.ToString();

      // Job Order Filter
      JobOrdersFilterDropDown.SelectedValue = filter;

      // Foreign Labor Jobs 
      if (filter == JobListFilter.ForeignLaborJobs.ToString())
      {
        JobOrdersFilterDropDown.SelectedValue = JobListFilter.ForeignLaborJobs.ToString();

        if (JobCriteria.ForeignLaborJobsAll == true)
        {
          JobOrdersFilterSubMenuDropDown.SelectedValue = JobListFilter.ForeignLaborJobsAll.ToString();
        }
        else if (JobCriteria.ForeignLaborJobsH2A == true)
        {
          JobOrdersFilterSubMenuDropDown.SelectedValue = JobListFilter.ForeignLaborJobsH2A.ToString();
        }
        else if (JobCriteria.ForeignLaborJobsH2B == true)
        {
          JobOrdersFilterSubMenuDropDown.SelectedValue = JobListFilter.ForeignLaborJobsH2B.ToString();
        }
        else if (JobCriteria.ForeignLaborJobsOther == true)
        {
          JobOrdersFilterSubMenuDropDown.SelectedValue = JobListFilter.ForeignLaborJobsOther.ToString();
        }
      }

    }

    #endregion
  }
}