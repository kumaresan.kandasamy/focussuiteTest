﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ManageStaffAccounts.aspx.cs" Inherits="Focus.Web.WebAssist.ManageStaffAccounts"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ManageAssistNavigation.ascx" TagName="ManageAssistNavigation"
    TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/CreateStaffAccountModal.ascx" TagName="CreateStaffAccountModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/EditStaffAccountModal.ascx" TagName="EditStaffAccountModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/UploadMultipleUsersModal.ascx" TagName="UploadMultipleUsersModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/AssignedOffices.ascx" TagName="AssignedOffices"
    TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />
    <h2>
        <%= HtmlLocalise("Header.Text", "Locate account")%></h2>
    <table role="presentation">
        <tr>
            <td style="width: 150px;">
                <%= HtmlLabel(FirstNameTextBox,"FirstName.Label", "First name") %>
            </td>
            <td>
                <asp:TextBox runat="server" ID="FirstNameTextBox" Width="200" MaxLength="50" />
            </td>
            <td style="width: 10px;" />
            <td />
        </tr>
        <tr>
            <td>
                <%= HtmlLabel(LastNameTextBox,"LastName.Label", "Last name") %>
            </td>
            <td>
                <asp:TextBox runat="server" ID="LastNameTextBox" Width="200" MaxLength="50" />
            </td>
            <td />
            <td />
        </tr>
        <tr>
            <td>
                <%= HtmlLabel(EmailAddressTextBox,"EmailAddress.Label", "Email address")%>
            </td>
            <td>
                <asp:TextBox runat="server" ID="EmailAddressTextBox" Width="200" MaxLength="255" />
            </td>
            <td />
            <td />
        </tr>
        <tr id="AccountStatusRow" runat="server">
            <td>
                <%= HtmlLabel(AccountStatusDropDown,"AccountStatus.Label", "Account status")%>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="AccountStatusDropDown" Width="200" />
            </td>
            <td />
            <td />
        </tr>
        <tr id="OfficeRow" runat="server">
            <td>
                <%= HtmlLabel(OfficeDropDown,"Office.Label", "Office")%>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="OfficeDropDown" Width="200" />
            </td>
            <td />
            <td />
        </tr>
        <tr>
            <td />
            <td />
            <td />
            <td style="vertical-align: top;">
                <asp:Button ID="FindButton" runat="server" class="button3" OnClick="FindButton_Clicked" />
                <asp:Button ID="CreateAccountButton" runat="server" class="button3" OnClick="CreateAccountButton_Clicked" />
                <asp:Button ID="UploadMultipleUsersButton" runat="server" class="button3" OnClick="UploadMultipleUsersButton_Clicked" />
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="SearchResultsPanel">
        <table role="presentation" id="StaffSearchHeaderTable" runat="server" visible="false"
            style="width: 100%;">
            <tr>
                <td>
                    <strong>
                        <%= HtmlLocalise("SearchResults.Title", "Results")%></strong>&nbsp;&nbsp;<asp:Literal
                            ID="ResultCount" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    <uc:Pager ID="SearchResultListPager" runat="server" PagedControlId="StaffList" PageSize="10" />
                </td>
            </tr>
        </table>
        <asp:ListView ID="StaffList" runat="server" DataSourceID="SearchResultDataSource"
            OnItemDataBound="SearchResultListView_ItemDataBound" OnItemCommand="StaffList_ItemCommand">
            <LayoutTemplate>
                <table role="presentation" class="genericTable">
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class="topRow">
                    <td class="column1">
                        <span><strong>
                            <%# ((StaffSearchViewDto)Container.DataItem).LastName %>,
                            <%# ((StaffSearchViewDto)Container.DataItem).FirstName  %></strong></span>
                    </td>
                    <td>
                        <span>
                            <%# ((StaffSearchViewDto)Container.DataItem).EmailAddress %></span>
                    </td>
                    <td class="column3">
                        <span>
                            <asp:LinkButton ID="EditStaffButton" runat="server" CommandArgument="<%#((StaffSearchViewDto)Container.DataItem).Id %>"
                                CommandName="EditStaffAccount" />
                            <asp:Literal ID="AccountActionSeparatorEdit" runat="server"> | </asp:Literal>
                            <asp:LinkButton ID="ResetPasswordButton" runat="server" CommandArgument="<%#((StaffSearchViewDto)Container.DataItem).Id %>"
                                CommandName="ResetPassword" />
                            <asp:Literal ID="AccountActionSeparatorReset" runat="server"> | </asp:Literal>
                            <asp:LinkButton ID="ManageStatusButton" runat="server" CommandArgument="<%#((StaffSearchViewDto)Container.DataItem).PersonId %>" />
                            <asp:Literal ID="AccountActionSeparatorManage" runat="server"> | </asp:Literal>
                            <asp:LinkButton ID="UpdateRolesButton" runat="server" CommandArgument='<%#((StaffSearchViewDto)Container.DataItem).Id + "," + ((StaffSearchViewDto)Container.DataItem).PersonId %>'
                                CommandName="UpdateRoles" ValidationGroup="BackdateDeleteActivities" />
                            <br />
                        </span>
                    </td>
                </tr>
                <tr class="bottomRow">
                    <td colspan="4">
                        <span>
                            <asp:Panel runat="server" ID="PermissionsPanel">
                                <table role="presentation" style="width: 100%;">
                                    <tr>
                                        <td colspan="3">
                                            <br />
                                        </td>
                                    </tr>
                                    <asp:PlaceHolder runat="server" ID="OfficesPlaceHolder">
                                        <tr style="vertical-align: top;">
                                            <td colspan="3">
                                                <asp:Panel ID="HandleWorkHeaderPanel" runat="server">
                                                    <table role="presentation" class="accordionTable">
                                                        <tr>
                                                            <td style="width: 100%;">
                                                                <asp:Image ID="PersonOfficesHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                                                    AlternateText="Person Offices Header Image" />
                                                                &nbsp;&nbsp;<%= HtmlLocalise("PersonOffices.Label", "Handles work for these offices")%>&nbsp;&nbsp;
                                                                <i>
                                                                    <%= HtmlLocalise("CurrentOffices.Label", "Current offices:  ")%>
                                                                    <asp:Literal runat="server" ID="CurrentOfficeListLiteral" />
                                                                    <asp:HyperLink runat="server" ID="ShowAdditionalOfficesLink" Visible="False" NavigateUrl="javascript:void(0);"></asp:HyperLink></i>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="HandleWorkPanel" runat="server">
                                                    <table role="presentation">
                                                        <tr>
                                                            <td>
                                                                <table role="presentation">
                                                                    <tr id="radioButtonRow">
                                                                        <td colspan="4">
                                                                            <asp:RadioButtonList ID="OfficeRadioButtonList" runat="server" RepeatDirection="Vertical"
                                                                                CssClass="RadioButtonList" onclick="HideShowOfficeControls(this);" />
                                                                            <asp:CustomValidator runat="server" ID="OfficeListValidator" CssClass="error" Display="Dynamic"
                                                                                Style="border: none; min-height: 0px; display: inline-block; padding-top: 20px" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="OfficesRow" runat="server">
                                                                        <td>
                                                                            <uc:AssignedOffices ID="AssignedOffices" runat="server" AdministrationType="StaffAccount"
                                                                                AddButtonCommandName="AddWorkplace" ReturnNoOfficesForStatewide="True" OnAssignedOfficesDataBinding="PersonOfficesControlOnAssignedOfficesDataBinding" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <act:CollapsiblePanelExtender ID="PersonOfficesPanelExtender" runat="server" TargetControlID="HandleWorkPanel"
                                                    ExpandControlID="HandleWorkHeaderPanel" CollapseControlID="HandleWorkHeaderPanel"
                                                    Collapsed="true" ImageControlID="PersonOfficesHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
                                                    ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <br />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <tr style="vertical-align: top;">
                                        <td style="width: 34%;">
                                            <asp:Panel ID="ManageSeekerAccountsHeaderPanel" runat="server">
                                                <table role="presentation" class="accordionTable">
                                                    <tr>
                                                        <td>
                                                            <asp:Image ID="ManageSeekerAccountsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                                                AlternateText="Manage Seeker Accounts Header Image" />
                                                            &nbsp;&nbsp;<%= HtmlLocalise("Global.ManageSeekerAccounts", "Manage job seeker accounts")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="ManageSeekerAccountsPanel" runat="server">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="width: 30px;" rowspan="17">
                                                        </td>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersReadOnlyCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersAdministratorCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersActionMenuCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersActivityLogBackdateEntryCheckBox" runat="server"
                                                                onclick="CheckBox_clicked(this)" TextAlign="Right" />&nbsp;
                                                            <asp:TextBox runat="server" ID="ManageJobSeekersActivityLogBackdateEntryTextBox"
                                                                MaxLength="3" Width="27" />&nbsp; days
                                                            <ajaxtoolkit:FilteredTextBoxExtender ID="ManageJobSeekersActivityLogBackdateEntryextender"
                                                                runat="server" Enabled="True" TargetControlID="ManageJobSeekersActivityLogBackdateEntryTextBox"
                                                                FilterType="Numbers" />
                                                            <asp:RequiredFieldValidator ID="ManageJobSeekersActivityLogBackdateEntryValidator"
                                                                runat="server" ErrorMessage="Maximum number  of days is required" SetFocusOnError="true"
                                                                ControlToValidate="ManageJobSeekersActivityLogBackdateEntryTextBox" CssClass="error"
                                                                Display="Dynamic" ValidationGroup="BackdateDeleteActivities" ValidateEmptyText="true"
                                                                Style="border: none; min-height: 0px; display: inline-block;" />
                                                            <asp:CustomValidator runat="server" ID="ManageJobSeekersActivityLogBackdateEntryCustomValidator"
                                                                CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
                                                                padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersActivityLogBackdateEntriesCheckBox" runat="server"
                                                                onclick="CheckBox_clicked(this)" TextAlign="Right" />&nbsp;
                                                            <asp:TextBox runat="server" ID="ManageJobSeekersActivityLogBackdateEntriesTextBox"
                                                                MaxLength="3" Width="27" />&nbsp; days
                                                            <ajaxtoolkit:FilteredTextBoxExtender ID="ManageJobSeekersActivityLogBackdateEntriesextender"
                                                                runat="server" Enabled="True" TargetControlID="ManageJobSeekersActivityLogBackdateEntriesTextBox"
                                                                FilterType="Numbers" />
                                                            <asp:RequiredFieldValidator ID="ManageJobSeekersActivityLogBackdateEntriesValidator"
                                                                runat="server" ErrorMessage="Maximum number  of days is required" SetFocusOnError="true"
                                                                ControlToValidate="ManageJobSeekersActivityLogBackdateEntriesTextBox" CssClass="error"
                                                                Display="Dynamic" ValidationGroup="BackdateDeleteActivities" ValidateEmptyText="true"
                                                                Style="border: none; min-height: 0px; display: inline-block;" />
                                                            <asp:CustomValidator runat="server" ID="ManageJobSeekersActivityLogBackdateEntriesCustomValidator"
                                                                SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersActivityLogDeleteEntryCheckBox" runat="server"
                                                                onclick="CheckBox_clicked(this)" TextAlign="Right" />&nbsp;
                                                            <asp:TextBox runat="server" ID="ManageJobSeekersActivityLogDeleteEntryTextBox" MaxLength="3"
                                                                Width="27" />&nbsp; days
                                                            <ajaxtoolkit:FilteredTextBoxExtender ID="ManageJobSeekersActivityLogDeleteEntryTextBoxextender"
                                                                runat="server" Enabled="True" TargetControlID="ManageJobSeekersActivityLogDeleteEntryTextBox"
                                                                FilterType="Numbers" />
                                                            <asp:RequiredFieldValidator ID="ManageJobSeekersActivityLogDeleteEntryValidator"
                                                                runat="server" ErrorMessage="Maximum number  of days is required" SetFocusOnError="true"
                                                                ControlToValidate="ManageJobSeekersActivityLogDeleteEntryTextBox" CssClass="error"
                                                                Display="Dynamic" ValidationGroup="BackdateDeleteActivities" ValidateEmptyText="true"
                                                                Style="border: none; min-height: 0px; display: inline-block;" />
                                                            <asp:CustomValidator runat="server" ID="ManageJobSeekersActivityLogDeleteEntryCustomValidator"
                                                                CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
                                                                padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersActivityLogDeleteEntriesCheckBox" runat="server"
                                                                onclick="CheckBox_clicked(this)" TextAlign="Right" />&nbsp;
                                                            <asp:TextBox runat="server" ID="ManageJobSeekersActivityLogDeleteEntriesTextBox"
                                                                MaxLength="3" Width="27" />&nbsp; days
                                                            <ajaxtoolkit:FilteredTextBoxExtender ID="ManageJobSeekersActivityLogDeleteEntriesTextBoxextender"
                                                                runat="server" Enabled="True" TargetControlID="ManageJobSeekersActivityLogDeleteEntriesTextBox"
                                                                FilterType="Numbers" />
                                                            <asp:RequiredFieldValidator ID="ManageJobSeekersActivityLogDeleteEntriesValidator"
                                                                runat="server" ErrorMessage="Maximum number of days is required" SetFocusOnError="true"
                                                                ControlToValidate="ManageJobSeekersActivityLogDeleteEntriesTextBox" CssClass="error"
                                                                Display="Dynamic" ValidationGroup="BackdateDeleteActivities" ValidateEmptyText="true"
                                                                Style="border: none; min-height: 0px; display: inline-block;" />
                                                            <asp:CustomValidator runat="server" ID="ManageJobSeekersActivityLogDeleteEntriesCustomValidator"
                                                                SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr id="JobSeekerAssignFromDefaultOfficeRow" runat="server">
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="AssignOfficeFromDefaultCheckBox" runat="server" TextAlign="Right" />
                                                            <asp:CustomValidator runat="server" ID="AssignOfficeFromDefaultCheckBoxCustomValidator"
                                                                SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersAccountBlockerCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersCreateNewAccountCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <asp:PlaceHolder runat="server" ID="ManageJobSeekersAccountInactivatorPlaceHolder">
                                                        <tr>
                                                            <td style="padding-top: 7px">
                                                                <asp:CheckBox ID="ManageJobSeekersAccountInactivatorCheckBox" runat="server" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                    </asp:PlaceHolder>
                                                    <tr id="JobSeekerReassignToNewOfficeRow" runat="server">
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ReassignOfficeToNewOfficeCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageJobSeekersSendMessagesCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <asp:PlaceHolder runat="server" ID="ManageStudentAccountAdministratorPlaceHolder">
                                                        <tr>
                                                            <td style="padding-top: 7px">
                                                                <asp:CheckBox runat="server" ID="ManageStudentAccountAdministratorCheckBox" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                    </asp:PlaceHolder>
                                                </table>
                                            </asp:Panel>
                                            <act:CollapsiblePanelExtender ID="ManageSeekerAccountsPanelExtender" runat="server"
                                                TargetControlID="ManageSeekerAccountsPanel" ExpandControlID="ManageSeekerAccountsHeaderPanel"
                                                CollapseControlID="ManageSeekerAccountsHeaderPanel" Collapsed="true" ImageControlID="ManageSeekerAccountsHeaderImage"
                                                CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
                                                SuppressPostBack="true" />
                                        </td>
                                        <td style="width: 35%;">
                                            <asp:Panel ID="ManageEmployerAccountsHeaderPanel" runat="server">
                                                <table role="presentation" class="accordionTable">
                                                    <tr>
                                                        <td>
                                                            <asp:Image ID="ManageEmployerAccountsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                                                AlternateText="Manage Employer Accounts Header Image" />
                                                            &nbsp;&nbsp;<asp:Label runat="server" ID="ManageEmployerAccountsLabel" Style="border-bottom: none;
                                                                border-left: none; border-right: none; padding-left: 0px; display: inline" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="ManageEmployerAccountsPanel" runat="server">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="width: 30px;" rowspan="17" />
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployersReadOnlyCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployersAdministratorCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployersActionMenuCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployersActivityLogBackdateEntryCheckBox" runat="server"
                                                                onclick="CheckBox_clicked(this)" TextAlign="Right" />&nbsp;
                                                            <asp:TextBox runat="server" ID="ManageEmployersActivityLogBackdateEntryTextBox" MaxLength="3"
                                                                Width="27" />&nbsp; days
                                                            <ajaxtoolkit:FilteredTextBoxExtender ID="ManageEmployersActivityLogBackdateEntryTextBoxextender"
                                                                runat="server" Enabled="True" TargetControlID="ManageEmployersActivityLogBackdateEntryTextBox"
                                                                FilterType="Numbers" />
                                                            <asp:RequiredFieldValidator ID="ManageEmployersActivityLogBackdateEntryValidator"
                                                                runat="server" ErrorMessage="Maximum number  of days is required" SetFocusOnError="true"
                                                                ControlToValidate="ManageEmployersActivityLogBackdateEntryTextBox" CssClass="error"
                                                                Display="Dynamic" ValidationGroup="BackdateDeleteActivities" ValidateEmptyText="true"
                                                                Style="border: none; min-height: 0px; display: inline-block;" />
                                                            <asp:CustomValidator runat="server" ID="ManageEmployersActivityLogBackdateEntryCustomValidator"
                                                                SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployersActivityLogBackdateEntriesCheckBox" runat="server"
                                                                onclick="CheckBox_clicked(this)" TextAlign="Right" />&nbsp;
                                                            <asp:TextBox runat="server" ID="ManageEmployersActivityLogBackdateEntriesTextBox"
                                                                MaxLength="3" Width="27" />&nbsp; days
                                                            <ajaxtoolkit:FilteredTextBoxExtender ID="ManageEmployersActivityLogBackdateEntriesTextBoxextender"
                                                                runat="server" Enabled="True" TargetControlID="ManageEmployersActivityLogBackdateEntriesTextBox"
                                                                FilterType="Numbers" />
                                                            <asp:RequiredFieldValidator ID="ManageEmployersActivityLogBackdateEntriesValidator"
                                                                runat="server" ErrorMessage="Maximum number  of days is required" SetFocusOnError="true"
                                                                ControlToValidate="ManageEmployersActivityLogBackdateEntriesTextBox" CssClass="error"
                                                                ValidationGroup="BackdateDeleteActivities" Display="Dynamic" ValidateEmptyText="true"
                                                                Style="border: none; min-height: 0px; display: inline-block;" />
                                                            <asp:CustomValidator runat="server" ID="ManageEmployersActivityLogBackdateEntriesCustomValidator"
                                                                SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployersActivityLogDeleteEntryCheckBox" runat="server" TextAlign="Right"
                                                                onclick="CheckBox_clicked(this)" />&nbsp;
                                                            <asp:TextBox runat="server" ID="ManageEmployersActivityLogDeleteEntryTextBox" MaxLength="3"
                                                                Width="27" />&nbsp; days<br />
                                                            <ajaxtoolkit:FilteredTextBoxExtender ID="ManageEmployersActivityLogDeleteEntryTextBoxextender"
                                                                runat="server" Enabled="True" TargetControlID="ManageEmployersActivityLogDeleteEntryTextBox"
                                                                FilterType="Numbers" />
                                                            <asp:RequiredFieldValidator ID="ManageEmployersActivityLogDeleteEntryValidator" runat="server"
                                                                ErrorMessage="Maximum number  of days is required" SetFocusOnError="true" ControlToValidate="ManageEmployersActivityLogDeleteEntryTextBox"
                                                                CssClass="error" Display="Dynamic" ValidationGroup="BackdateDeleteActivities"
                                                                ValidateEmptyText="true" Style="border: none; min-height: 0px; display: inline-block;" />
                                                            <asp:CustomValidator runat="server" ID="ManageEmployersActivityLogDeleteEntryCustomValidator"
                                                                SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployersActivityLogDeleteEntriesCheckBox" runat="server"
                                                                onclick="CheckBox_clicked(this)" TextAlign="Right" />&nbsp;
                                                            <asp:TextBox runat="server" ID="ManageEmployersActivityLogDeleteEntriesTextBox" MaxLength="3"
                                                                Width="27" />&nbsp; days
                                                            <ajaxtoolkit:FilteredTextBoxExtender ID="ManageEmployersActivityLogDeleteEntriesTextBoxextender"
                                                                runat="server" Enabled="True" TargetControlID="ManageEmployersActivityLogDeleteEntriesTextBox"
                                                                FilterType="Numbers" />
                                                            <asp:RequiredFieldValidator ID="ManageEmployersActivityLogDeleteEntriesValidator"
                                                                runat="server" ErrorMessage="Maximum number  of days is required" SetFocusOnError="true"
                                                                ControlToValidate="ManageEmployersActivityLogDeleteEntriesTextBox" CssClass="error"
                                                                Display="Dynamic" ValidationGroup="BackdateDeleteActivities" ValidateEmptyText="true"
                                                                Style="border: none; min-height: 0px; display: inline-block;" />
                                                            <asp:CustomValidator runat="server" ID="ManageEmployersActivityLogDeleteEntriesCustomValidator"
                                                                SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr id="EmployerAssignFromDefaultOfficeRow" runat="server">
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox runat="server" ID="AssistAssignBusinessOfficeFromDefaultCheckBox" TextAlign="Right" />
                                                            <asp:CustomValidator runat="server" ID="AssistAssignBusinessOfficeFromDefaultCustomValidator"
                                                                SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <%--<tr id="JobOrderOfficeAdminRow" runat="server">
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox runat="server" ID="ManageJobOrdersOfficesAdministratorCheckBox" TextAlign="Right" />
                                                        </td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployersAccountBlockerCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployerCreateNewAccountCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr id="JobDevelopmentRow" runat="server">
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox runat="server" ID="ManageJobDevelopmentCheckBox" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployerOverrideCreditCheckCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployerOverrideCriminalExclusionsCheckBox" runat="server"
                                                                TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr id="BusinessReassignToNewOfficeRow" runat="server">
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployerReassignBusinessCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr id="JobPostingReassignToNewOfficeRow" runat="server">
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployerReassignJobPostingCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 7px">
                                                            <asp:CheckBox ID="ManageEmployerSendMessagesCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <act:CollapsiblePanelExtender ID="ManageEmployerAccountsPanelExtender" runat="server"
                                                TargetControlID="ManageEmployerAccountsPanel" ExpandControlID="ManageEmployerAccountsHeaderPanel"
                                                CollapseControlID="ManageEmployerAccountsHeaderPanel" Collapsed="true" ImageControlID="ManageEmployerAccountsHeaderImage"
                                                CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
                                                SuppressPostBack="true" />
                                        </td>
                                        <td style="width: 34%;">
                                            <asp:Panel ID="ManageApprovalQueuesHeaderPanel" runat="server">
                                                <table role="presentation" class="accordionTable">
                                                    <tr>
                                                        <td>
                                                            <asp:Image ID="ManageApprovalQueuesHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                                                AlternateText="Manage Approval Queues Header Image" />
                                                            &nbsp;&nbsp;<%= HtmlLocalise("ManageApprovalQueues.Label", "Manage approval queues")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="ManageApprovalQueuesPanel" runat="server">
                                                <table role="presentation">
                                                    <tr style="padding-top: 100px">
                                                        <td style="width: 30px;" rowspan="21">
                                                        </td>
                                                        <td>
                                                            <asp:Literal ID="ReferralRequestsLabel" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="ReferralViewCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="ReferralViewEditCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr style="padding-top: 100px">
                                                        <td>
                                                            <br />
                                                            <%= HtmlLocalise("EmployerAccounts.Label", "#BUSINESS# accounts")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="EmployerAccountsViewCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="EmployerAccountsViewEditCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr style="padding-top: 100px">
                                                        <td>
                                                            <br />
                                                            <%= HtmlLocalise("JobPostings.Label", "Job postings")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="JobPostingsViewCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="JobPostingsViewEditCheckBox" runat="server" TextAlign="Right" onclick="PostingApproverCheckBoxChanged(this)" />
                                                        </td>
                                                    </tr>
                                                    <asp:PlaceHolder runat="server" ID="HandleSpecialOrdersPlaceHolder" Visible="False">
                                                        <tr style="padding-top: 100px">
                                                            <td>
                                                                <br />
                                                                <%= HtmlLocalise("HandleSpecialJobOrders.Label", "Handle these special job conditions:")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="AllConditionsCheckBox" runat="server" TextAlign="Right" Onclick="AllConditionsCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="CommissionOnlyCheckBox" runat="server" TextAlign="Right" onclick="ConditionCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="SalaryAndCommissionCheckBox" runat="server" TextAlign="Right" onclick="ConditionCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="CourtOrderedAffrimativeActionCheckBox" runat="server" TextAlign="Right"
                                                                    onclick="ConditionCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="FederalContractorCheckBox" runat="server" TextAlign="Right" onclick="ConditionCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="ForeignLabourH2AAgCheckBox" runat="server" TextAlign="Right" onclick="ConditionCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="ForeignLabourH2BNonAgCheckBox" runat="server" TextAlign="Right"
                                                                    onclick="ConditionCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="ForeignLabourOtherCheckBox" runat="server" TextAlign="Right" onclick="ConditionCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="HomeBasedCheckBox" runat="server" TextAlign="Right" onclick="ConditionCheckBoxChanged(this)" />
                                                            </td>
                                                        </tr>
                                                    </asp:PlaceHolder>
                                                </table>
                                            </asp:Panel>
                                            <act:CollapsiblePanelExtender ID="ManageApprovalQueuesPanelExtender" runat="server"
                                                TargetControlID="ManageApprovalQueuesPanel" ExpandControlID="ManageApprovalQueuesHeaderPanel"
                                                CollapseControlID="ManageApprovalQueuesHeaderPanel" Collapsed="true" ImageControlID="ManageApprovalQueuesHeaderImage"
                                                CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
                                                SuppressPostBack="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr style="vertical-align: top;">
                                        <td>
                                            <asp:Panel ID="ManageFocusHeaderPanel" runat="server">
                                                <table role="presentation" class="accordionTable">
                                                    <tr>
                                                        <td>
                                                            <asp:Image ID="ManageFocusHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                                                AlternateText="Manage Focus Header Image" />
                                                            &nbsp;&nbsp;<%= HtmlLocalise("ManageFocus.Label", "Manage Assist Roles")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="ManageFocusPanel" runat="server">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="width: 30px;" rowspan="7">
                                                        </td>
                                                        <td>
                                                            <i>
                                                                <%= HtmlLocalise("ManageFocus.Instructions", "Only account administrators can create other account administrators or system administrators. Only system administrators may set system defaults.")%></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="AccountAdministratorCheckBox" runat="server" TextAlign="Right"
                                                                onclick="AccountAdminCheckBoxChanged(this)" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="BroadcastAdministratorCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="DvopCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="LverCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="ManagerCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="SystemAdministratorCheckBox" runat="server" TextAlign="Right" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <act:CollapsiblePanelExtender ID="ManageFocusPanelExtender" runat="server" TargetControlID="ManageFocusPanel"
                                                ExpandControlID="ManageFocusHeaderPanel" CollapseControlID="ManageFocusHeaderPanel"
                                                Collapsed="true" ImageControlID="ManageFocusHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
                                                ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
                                        </td>
                                        <td>
                                            <asp:PlaceHolder runat="server" ID="ManageReportsPlaceHolder">
                                                <asp:Panel ID="ManageReportsHeaderPanel" runat="server">
                                                    <table role="presentation" class="accordionTable">
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="ManageReportsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                                                    AlternateText="Manage Reports Header Image" />
                                                                &nbsp;&nbsp;<%= HtmlLocalise("ManageReports.Label", "Manage Reports")%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="ManageReportsPanel" runat="server">
                                                    <table role="presentation">
                                                        <tr>
                                                            <td style="width: 30px;" rowspan="12">
                                                            </td>
                                                            <td id="JobSeekerReportsHeaderCell" runat="server">
                                                                <%= HtmlLocalise("JobSeekerReportsCheckBox.Label", "#CANDIDATETYPE# reports")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="JobSeekerReportsViewOnlyCheckBox" runat="server" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="JobSeekerReportsCheckBox" runat="server" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="JobOrderReportsHeaderCell" runat="server">
                                                                <%= HtmlLocalise("JobOrderReportsCheckBox.Label", "#EMPLOYMENTTYPE# reports")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="JobOrderReportsViewOnlyCheckBox" runat="server" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="JobOrderReportsCheckBox" runat="server" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td id="EmployerReportsHeaderCell" runat="server">
                                                                <%= HtmlLocalise("EmployerReportsCheckBox.Label", "#BUSINESS#/hiring manager reports")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="EmployerReportsViewOnlyCheckBox" runat="server" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="EmployerReportsCheckBox" runat="server" TextAlign="Right" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <act:CollapsiblePanelExtender ID="ManageReportsPanelExtender" runat="server" TargetControlID="ManageReportsPanel"
                                                    ExpandControlID="ManageReportsHeaderPanel" CollapseControlID="ManageReportsHeaderPanel"
                                                    Collapsed="true" ImageControlID="ManageReportsHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
                                                    ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
                                            </asp:PlaceHolder>
                                        </td>
                                        <td>
                                            <asp:PlaceHolder runat="server" ID="ManageOfficesPlaceHolder">
                                                <asp:Panel ID="ManageOfficesHeaderPanel" runat="server">
                                                    <table role="presentation" class="accordionTable">
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="ManageOfficesHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                                                    AlternateText="Manage Offices Header Image" />
                                                                &nbsp;&nbsp;<%= HtmlLocalise("ManageOffices.Label", "Manage offices")%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="ManageOfficesPanel" runat="server">
                                                    <table role="presentation">
                                                        <tr>
                                                            <td style="width: 30px" rowspan="5">
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox runat="server" ID="ActivateDeactivateCheckbox" TextAlign="Right" />
                                                                <asp:CustomValidator runat="server" ID="ActivateDeactivateCheckboxCustomValidator"
                                                                    SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                    min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="CreateOfficesCheckBox" runat="server" TextAlign="Right" />
                                                                <asp:CustomValidator runat="server" ID="CreateOfficesCheckBoxCustomValidator" SetFocusOnError="true"
                                                                    CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
                                                                    padding-top: 5px; margin-top: -5px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="ListStaffCheckBox" runat="server" TextAlign="Right" />
                                                                <asp:CustomValidator runat="server" ID="ListStaffCheckBoxCustomValidator" SetFocusOnError="true"
                                                                    CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
                                                                    padding-top: 5px; margin-top: -5px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="SetDefaultOfficeCheckBox" runat="server" TextAlign="Right" />
                                                                <asp:CustomValidator runat="server" ID="SetDefaultOfficeCheckBoxCustomValidator"
                                                                    SetFocusOnError="true" CssClass="error" Display="Dynamic" Style="border: none;
                                                                    min-height: 0px; display: inline-block; padding-top: 5px; margin-top: -5px;" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="ViewEditOfficesCheckBox" runat="server" TextAlign="Right" />
                                                                <asp:CustomValidator runat="server" ID="ViewEditOfficesCheckBoxCustomValidator" SetFocusOnError="true"
                                                                    CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
                                                                    padding-top: 5px; margin-top: -5px;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <act:CollapsiblePanelExtender ID="ManageOfficesPanelExtender" runat="server" TargetControlID="ManageOfficesPanel"
                                                    ExpandControlID="ManageOfficesHeaderPanel" CollapseControlID="ManageOfficesHeaderPanel"
                                                    Collapsed="true" ImageControlID="ManageOfficesHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
                                                    ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
                                            </asp:PlaceHolder>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="ManageStaffHeaderPanel" runat="server">
                                                <table role="presentation" class="accordionTable">
                                                    <tr>
                                                        <td>
                                                            <asp:Image ID="ManageStaffHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                                                                AlternateText="Manage Staff Header Image" />
                                                            &nbsp;&nbsp;<%= HtmlLocalise("ManageFocus.Label", "Manage Staff")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="ManageStaffPanel" runat="server">
                                                <table role="presentation">
                                                    <tr>
                                                        <td style="width: 30px;" rowspan="5">
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="ManageStaffViewCheckBox" runat="server" TextAlign="Right" />
                                                            <asp:CustomValidator runat="server" ID="ManageStaffViewCheckBoxCustomValidator" SetFocusOnError="true"
                                                                                 CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
		padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="ManageStaffResetPasswordCheckBox" runat="server" TextAlign="Right" />
                                                            <asp:CustomValidator runat="server" ID="ManageStaffResetPasswordCheckBoxCustomValidator" SetFocusOnError="true"
                                                                                 CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
		padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="ManageStaffActivateCheckBox" runat="server" TextAlign="Right" />
                                                            <asp:CustomValidator runat="server" ID="ManageStaffActivateCheckBoxCustomValidator" SetFocusOnError="true"
                                                                                 CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
		padding-top: 5px; margin-top: -5px;" />

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="ManageStaffDetailsCheckBox" runat="server" TextAlign="Right" />
                                                            <asp:CustomValidator runat="server" ID="ManageStaffDetailsCheckBoxCustomValidator" SetFocusOnError="true"
                                                                                 CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
		padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="ManageUpdatePermissionsCheckbox" runat="server" TextAlign="Right" />
                                                            <asp:CustomValidator runat="server" ID="ManageUpdatePermissionsCheckboxCustomValidator" SetFocusOnError="true"
                                                                CssClass="error" Display="Dynamic" Style="border: none; min-height: 0px; display: inline-block;
                                                                padding-top: 5px; margin-top: -5px;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <act:CollapsiblePanelExtender ID="ManageStaffPanelExtender" runat="server" TargetControlID="ManageStaffPanel"
                                                ExpandControlID="ManageStaffHeaderPanel" CollapseControlID="ManageStaffHeaderPanel"
                                                Collapsed="true" ImageControlID="ManageStaffHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
                                                ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
                                        </td>
                                        <td colspan="2">
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </span>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </asp:Panel>
    <asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.WebAssist.ManageStaffAccounts"
        EnablePaging="true" SelectMethod="GetStaff" SelectCountMethod="GetStaffCount"
        SortParameterName="orderBy" OnSelecting="SearchResultDataSource_Selecting" />
    <%-- Confirmation Modal --%>
    <uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
    <uc:ConfirmationModal ID="ActionConfirmationModal" runat="server" OnOkCommand="ActionConfirmationModal_OkCommand" />
    <uc:CreateStaffAccountModal ID="CreateStaffAccountModal" runat="server" />
    <uc:EditStaffAccountModal ID="EditStaffAccountModal" runat="server" />
    <uc:UploadMultipleUsersModal ID="UploadMultipleUsersModal" runat="server" />
    <script type="text/javascript">

        Sys.Application.add_load(ManageStaffAccounts_PageLoad);
        Sys.Application.add_load(bind);


        function ManageStaffAccounts_PageLoad() {
            $(".RadioButtonList").each(function () {
                HideShowOfficeControls(this);

            });

        }

        function bind() {

            var array = $('*[id*=ManageJobSeekersActivityLogBackdateEntriesCheckBox]').length;
            for (var i = 0; i < array; i++) {
                //job seeker
                var ManageJobSeekersActivityLogBackdateEntryCheckBox = $('*[id*=ManageJobSeekersActivityLogBackdateEntryCheckBox_' + i + ']').prop('checked');
                var ManageJobSeekersActivityLogBackdateEntryCheckBoxDisabled = $('*[id*=ManageJobSeekersActivityLogBackdateEntryCheckBox_' + i + ']').prop('disabled');
                var ManageJobSeekersActivityLogBackdateEntryValidator = $('*[id*=ManageJobSeekersActivityLogBackdateEntryValidator_' + i + ']');
                if (ManageJobSeekersActivityLogBackdateEntryCheckBox && !ManageJobSeekersActivityLogBackdateEntryCheckBoxDisabled) {
                    $('*[id*=ManageJobSeekersActivityLogBackdateEntryTextBox_' + i + ']').prop('disabled', false);
                    ValidatorEnable(ManageJobSeekersActivityLogBackdateEntryValidator[0], true);
                }
                else {
                    $('*[id*=ManageJobSeekersActivityLogBackdateEntryTextBox_' + i + ']').prop('disabled', true);
                    ValidatorEnable(ManageJobSeekersActivityLogBackdateEntryValidator[0], false);
                }


                var ManageJobSeekersActivityLogBackdateEntries = $('*[id*=ManageJobSeekersActivityLogBackdateEntriesCheckBox_' + i + ']').prop('checked');
                var ManageJobSeekersActivityLogBackdateEntriesDisabled = $('*[id*=ManageJobSeekersActivityLogBackdateEntriesCheckBox_' + i + ']').prop('disabled');
                var ManageJobSeekersActivityLogBackdateEntriesValidator = $('*[id*=ManageJobSeekersActivityLogBackdateEntriesValidator_' + i + ']');
                if (ManageJobSeekersActivityLogBackdateEntries && !ManageJobSeekersActivityLogBackdateEntriesDisabled) {
                    $('*[id*=ManageJobSeekersActivityLogBackdateEntriesTextBox_' + i + ']').prop('disabled', false);
                    ValidatorEnable(ManageJobSeekersActivityLogBackdateEntriesValidator[0], true);
                }
                else {
                    $('*[id*=ManageJobSeekersActivityLogBackdateEntriesTextBox_' + i + ']').prop('disabled', true);
                    ValidatorEnable(ManageJobSeekersActivityLogBackdateEntriesValidator[0], false);

                }

                var ManageJobSeekersActivityLogDeleteEntryCheckBox = $('*[id*=ManageJobSeekersActivityLogDeleteEntryCheckBox_' + i + ']').prop('checked');
                var ManageJobSeekersActivityLogDeleteEntryCheckBoxDisabled = $('*[id*=ManageJobSeekersActivityLogDeleteEntryCheckBox_' + i + ']').prop('disabled');
                var ManageJobSeekersActivityLogDeleteEntryValidator = $('*[id*=ManageJobSeekersActivityLogDeleteEntryValidator_' + i + ']');
                if (ManageJobSeekersActivityLogDeleteEntryCheckBox && !ManageJobSeekersActivityLogDeleteEntryCheckBoxDisabled) {
                    $('*[id*=ManageJobSeekersActivityLogDeleteEntryTextBox_' + i + ']').prop('disabled', false);
                    ValidatorEnable(ManageJobSeekersActivityLogDeleteEntryValidator[0], true);
                }
                else {
                    $('*[id*=ManageJobSeekersActivityLogDeleteEntryTextBox_' + i + ']').prop('disabled', true);
                    ValidatorEnable(ManageJobSeekersActivityLogDeleteEntryValidator[0], false);

                }

                var ManageJobSeekersActivityLogDeleteEntriesCheckBox = $('*[id*=ManageJobSeekersActivityLogDeleteEntriesCheckBox_' + i + ']').prop('checked');
                var ManageJobSeekersActivityLogDeleteEntriesCheckBoxDisabled = $('*[id*=ManageJobSeekersActivityLogDeleteEntriesCheckBox_' + i + ']').prop('disabled');
                var ManageJobSeekersActivityLogDeleteEntriesValidator = $('*[id*=ManageJobSeekersActivityLogDeleteEntriesValidator_' + i + ']');
                if (ManageJobSeekersActivityLogDeleteEntriesCheckBox && !ManageJobSeekersActivityLogDeleteEntriesCheckBoxDisabled) {
                    $('*[id*=ManageJobSeekersActivityLogDeleteEntriesTextBox_' + i + ']').prop('disabled', false);
                    ValidatorEnable(ManageJobSeekersActivityLogDeleteEntriesValidator[0], true);
                }
                else {
                    $('*[id*=ManageJobSeekersActivityLogDeleteEntriesTextBox_' + i + ']').prop('disabled', true);
                    ValidatorEnable(ManageJobSeekersActivityLogDeleteEntriesValidator[0], false);
                }

                //employer

                var ManageEmployersActivityLogBackdateEntryCheckBox = $('*[id*=ManageEmployersActivityLogBackdateEntryCheckBox_' + i + ']').prop('checked');
                var ManageEmployersActivityLogBackdateEntryCheckBoxDisabled = $('*[id*=ManageEmployersActivityLogBackdateEntryCheckBox_' + i + ']').prop('disabled');
                var ManageEmployersActivityLogBackdateEntryValidator = $('*[id*=ManageEmployersActivityLogBackdateEntryValidator_' + i + ']');
                if (ManageEmployersActivityLogBackdateEntryCheckBox && !ManageEmployersActivityLogBackdateEntryCheckBoxDisabled) {
                    $('*[id*=ManageEmployersActivityLogBackdateEntryTextBox_' + i + ']').prop('disabled', false);
                    ValidatorEnable(ManageEmployersActivityLogBackdateEntryValidator[0], true);
                }
                else {
                    $('*[id*=ManageEmployersActivityLogBackdateEntryTextBox_' + i + ']').prop('disabled', true);
                    ValidatorEnable(ManageEmployersActivityLogBackdateEntryValidator[0], false);
                }


                var ManageEmployersActivityLogBackdateEntriesCheckBox = $('*[id*=ManageEmployersActivityLogBackdateEntriesCheckBox_' + i + ']').prop('checked');
                var ManageEmployersActivityLogBackdateEntriesCheckBoxDisabled = $('*[id*=ManageEmployersActivityLogBackdateEntriesCheckBox_' + i + ']').prop('disabled');
                var ManageEmployersActivityLogBackdateEntriesValidator = $('*[id*=ManageEmployersActivityLogBackdateEntriesValidator_' + i + ']');
                if (ManageEmployersActivityLogBackdateEntriesCheckBox && !ManageEmployersActivityLogBackdateEntriesCheckBoxDisabled) {
                    $('*[id*=ManageEmployersActivityLogBackdateEntriesTextBox_' + i + ']').prop('disabled', false);
                    ValidatorEnable(ManageEmployersActivityLogBackdateEntriesValidator[0], true);
                }
                else {

                    $('*[id*=ManageEmployersActivityLogBackdateEntriesTextBox_' + i + ']').prop('disabled', true);
                    ValidatorEnable(ManageEmployersActivityLogBackdateEntriesValidator[0], false);
                }

                var ManageEmployersActivityLogDeleteEntryCheckBox = $('*[id*=ManageEmployersActivityLogDeleteEntryCheckBox_' + i + ']').prop('checked');
                var ManageEmployersActivityLogDeleteEntryCheckBoxDisabled = $('*[id*=ManageEmployersActivityLogDeleteEntryCheckBox_' + i + ']').prop('disabled');
                var ManageEmployersActivityLogDeleteEntryValidator = $('*[id*=ManageEmployersActivityLogDeleteEntryValidator_' + i + ']');
                if (ManageEmployersActivityLogDeleteEntryCheckBox && !ManageEmployersActivityLogDeleteEntryCheckBoxDisabled) {
                    $('*[id*=ManageEmployersActivityLogDeleteEntryTextBox_' + i + ']').prop('disabled', false);
                    ValidatorEnable(ManageEmployersActivityLogDeleteEntryValidator[0], true);

                }
                else {
                    $('*[id*=ManageEmployersActivityLogDeleteEntryTextBox_' + i + ']').prop('disabled', true);
                    ValidatorEnable(ManageEmployersActivityLogDeleteEntryValidator[0], false);
                }

                var ManageEmployersActivityLogDeleteEntriesCheckBox = $('*[id*=ManageEmployersActivityLogDeleteEntriesCheckBox_' + i + ']').prop('checked');
                var ManageEmployersActivityLogDeleteEntriesCheckBoxDisabled = $('*[id*=ManageEmployersActivityLogDeleteEntriesCheckBox_' + i + ']').prop('disabled');
                var ManageEmployersActivityLogDeleteEntriesValidator = $('*[id*=ManageEmployersActivityLogDeleteEntriesValidator_' + i + ']');
                if (ManageEmployersActivityLogDeleteEntriesCheckBox && !ManageEmployersActivityLogDeleteEntriesCheckBoxDisabled) {
                    $('*[id*=ManageEmployersActivityLogDeleteEntriesTextBox_' + i + ']').prop('disabled', false);
                    ValidatorEnable(ManageEmployersActivityLogDeleteEntriesValidator[0], true);
                }
                else {
                    $('*[id*=ManageEmployersActivityLogDeleteEntriesTextBox_' + i + ']').prop('disabled', true);
                    ValidatorEnable(ManageEmployersActivityLogDeleteEntriesValidator[0], false);
                }
            }

        }




        function HideShowOfficeControls(wrapperElement) {
            var val = $(wrapperElement).find('input:checked').val();

            var officesDiv = $(wrapperElement).parents('#radioButtonRow').next();
            if (val != "2") {
                officesDiv.hide();
            }
            else {
                officesDiv.show();
            }
        }

        var ManageStaffAccounts_ConditionsCheckBoxes = {};

        function ManageStaffAccounts_GetConditionsCheckBoxes(index) {
            if (ManageStaffAccounts_ConditionsCheckBoxes[index] == null) {
                ManageStaffAccounts_ConditionsCheckBoxes[index] =
		    {
		        "AllConditionsCheckBox": $('[id*=AllConditionsCheckBox_' + index + ']'),
		        "CourtOrderedAffrimativeActionCheckBox": $('[id*=CourtOrderedAffrimativeActionCheckBox_' + index + ']'),
		        "FederalContractorCheckBox": $('[id*=FederalContractorCheckBox_' + index + ']'),
		        "ForeignLabourH2AAgCheckBox": $('[id*=ForeignLabourH2AAgCheckBox_' + index + ']'),
		        "ForeignLabourH2BNonAgCheckBox": $('[id*=ForeignLabourH2BNonAgCheckBox_' + index + ']'),
		        "ForeignLabourOtherCheckBox": $('[id*=ForeignLabourOtherCheckBox_' + index + ']'),
		        "NonForeignLabourCheckBox": $('[id*=NonForeignLabourCheckBox_' + index + ']'),
		        "CommissionOnlyCheckBox": $('[id*=CommissionOnlyCheckBox_' + index + ']'),
		        "SalaryAndCommissionCheckBox": $('[id*=SalaryAndCommissionCheckBox_' + index + ']'),
		        "HomeBasedCheckBox": $('[id*=HomeBasedCheckBox_' + index + ']')
		    };
            }

            return ManageStaffAccounts_ConditionsCheckBoxes[index];
        }

        function PostingApproverCheckBoxChanged(src, args) {
            var controlId = src.id;
            var index = GetRepeaterIndex(controlId);
            var checkBoxes = ManageStaffAccounts_GetConditionsCheckBoxes(index);

            checkBoxes["AllConditionsCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["CourtOrderedAffrimativeActionCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["FederalContractorCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["ForeignLabourH2AAgCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["ForeignLabourH2BNonAgCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["ForeignLabourOtherCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["NonForeignLabourCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["CommissionOnlyCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["SalaryAndCommissionCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));
            checkBoxes["HomeBasedCheckBox"].prop('disabled', !$('#' + controlId).prop('checked'));

            if (!$('#' + controlId).prop('checked')) {
                checkBoxes["AllConditionsCheckBox"].prop('checked', false);
                checkBoxes["CourtOrderedAffrimativeActionCheckBox"].prop('checked', false);
                checkBoxes["FederalContractorCheckBox"].prop('checked', false);
                checkBoxes["ForeignLabourH2AAgCheckBox"].prop('checked', false);
                checkBoxes["ForeignLabourH2BNonAgCheckBox"].prop('checked', false);
                checkBoxes["ForeignLabourOtherCheckBox"].prop('checked', false);
                checkBoxes["NonForeignLabourCheckBox"].prop('checked', false);
                checkBoxes["CommissionOnlyCheckBox"].prop('checked', false);
                checkBoxes["SalaryAndCommissionCheckBox"].prop('checked', false);
                checkBoxes["HomeBasedCheckBox"].prop('checked', false);
            }
        }

        function ConditionCheckBoxChanged(src, args) {
            var controlId = src.id;
            var index = GetRepeaterIndex(controlId);
            var checkBoxes = ManageStaffAccounts_GetConditionsCheckBoxes(index);

            if (!$(src).is(":checked")) {
                checkBoxes["AllConditionsCheckBox"].prop('checked', false);
            } else {
                var allChecked = true;
                for (var boxKey in checkBoxes) {
                    if (boxKey != "AllConditionsCheckBox" && checkBoxes[boxKey].length > 0 && !checkBoxes[boxKey].is(":checked")) {
                        allChecked = false;
                    }
                }
                checkBoxes["AllConditionsCheckBox"].prop('checked', allChecked);
            }
        }

        function AllConditionsCheckBoxChanged(src, args) {
            var controlId = src.id;
            var index = GetRepeaterIndex(controlId);
            var checkBoxes = ManageStaffAccounts_GetConditionsCheckBoxes(index);

            var checked = $(src).is(":checked");

            checkBoxes["CourtOrderedAffrimativeActionCheckBox"].prop('checked', checked);
            checkBoxes["FederalContractorCheckBox"].prop('checked', checked);
            checkBoxes["ForeignLabourH2AAgCheckBox"].prop('checked', checked);
            checkBoxes["ForeignLabourH2BNonAgCheckBox"].prop('checked', checked);
            checkBoxes["ForeignLabourOtherCheckBox"].prop('checked', checked);
            checkBoxes["NonForeignLabourCheckBox"].prop('checked', checked);
            checkBoxes["CommissionOnlyCheckBox"].prop('checked', checked);
            checkBoxes["SalaryAndCommissionCheckBox"].prop('checked', checked);
            checkBoxes["HomeBasedCheckBox"].prop('checked', checked);
        }

        function AccountAdminCheckBoxChanged(src, args) {
            var controlId = src.id;
            var index = GetRepeaterIndex(controlId);

            $('[id*=ManageStaffViewCheckBox_' + index + ']').prop('disabled', $('#' + controlId).prop('checked'));
            $('[id*=ManageStaffResetPasswordCheckBox_' + index + ']').prop('disabled', $('#' + controlId).prop('checked'));
            $('[id*=ManageStaffActivateCheckBox_' + index + ']').prop('disabled', $('#' + controlId).prop('checked'));
            $('[id*=ManageStaffDetailsCheckBox_' + index + ']').prop('disabled', $('#' + controlId).prop('checked'));
            $('[id*=ManageUpdatePermissionsCheckbox_' + index + ']').prop('disabled', $('#' + controlId).prop('checked'));

            if ($('#' + controlId).prop('checked')) {
                $('[id*=ManageStaffViewCheckBox_' + index + ']').prop('checked', true);
                $('[id*=ManageStaffResetPasswordCheckBox_' + index + ']').prop('checked', true);
                $('[id*=ManageStaffActivateCheckBox_' + index + ']').prop('checked', true);
                $('[id*=ManageStaffDetailsCheckBox_' + index + ']').prop('checked', true);
                $('[id*=ManageUpdatePermissionsCheckbox_' + index + ']').prop('checked', true);
            }
        }

        function GetRepeaterIndex(srcCtrlId) {
            var indexIdentifier = srcCtrlId.lastIndexOf('_') + 1;
            return srcCtrlId.substr(indexIdentifier, srcCtrlId.length - indexIdentifier);
        }



        function CheckBox_clicked(src, args) {

            var checked = $(src).is(":checked");
            var textboxId = src.id.replace("CheckBox", "TextBox");
            var textbox = $('#' + textboxId);
            var validatorId = src.id.replace("CheckBox", "Validator");

            if (checked) {

                $(textbox).prop("disabled", false);
                ValidatorEnable($("#" + validatorId)[0], checked);


            }
            else {

                $(textbox).prop("disabled", true);
                ValidatorEnable($("#" + validatorId)[0], checked);

            }

        }

        
       
    </script>
</asp:Content>
