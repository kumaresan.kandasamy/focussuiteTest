﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Web.Code;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistStudentAccountAdministrator)]
  public partial class ManageStudentAccounts : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (App.Settings.Theme != FocusThemes.Education)
        Response.Redirect(UrlBuilder.Default(App.User, App));

			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
      SingleUserRadioButton.Text = CodeLocalise("SingleUserRadioButton.Text", "Invite a #CANDIDATETYPE#:LOWER to register");
      MultipleUserRadioButton.Text = CodeLocalise("MultipleUserRadioButton.Text", "Invite multiple #CANDIDATETYPES#:LOWER to register");
			SendEmailButton.Text = CodeLocalise("SendEmailButton.Text", "Invite #CANDIDATETYPES#:LOWER");
			EmailTextBoxRequired.ErrorMessage = CodeLocalise("EmailTextBoxRequired.ErrorMessage", "Email address is required");
			FileUploadRequired.ErrorMessage = CodeLocalise("FileUploadRequired.ErrorMessage", "A file of user email addresses must be uploaded");
			FileUploadCustomValidator.ErrorMessage = CodeLocalise("FileUploadCustomValidator.ErrorMessage", "File is of unsupported type. File must be csv or txt and under 2Mb");
		}

		/// <summary>
		/// Handles the Clicked event of the SendEmailButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SendEmailButton_Clicked(object sender, EventArgs e)
		{
			var email = new List<string>();
			var validFile = true;

			// Get email address(es)
		  if (SingleUserRadioButton.Checked)
		  {
		    email.Add(EmailTextBox.TextTrimmed());
		  }
		  else
		  {
		    EmailTextBox.Text = string.Empty;
			  validFile = ValidateFileUpload();
		    
				if(validFile)
					email.AddRange(ExtractEmails());
			}

			var statusMessage = "";

			if (validFile)
			{
				#region Get the user type for career module

				var userType = UserTypes.Career | UserTypes.Explorer;

				if (App.Settings.CareerExplorerModule == FocusModules.Explorer)
					userType = UserTypes.Explorer;

				if (App.Settings.CareerExplorerModule == FocusModules.Career)
					userType = UserTypes.Career;

				#endregion

				// Validate email address(es)
				var emailValidationModel = ServiceClientLocator.AccountClient(App).ValidateEmail(email, userType);

				if (emailValidationModel.InvalidEmailFormat.IsNotNullOrEmpty() || emailValidationModel.MultipleEmailError.IsNotNullOrEmpty() || emailValidationModel.DuplicateEmailError.IsNotNullOrEmpty()
				    || emailValidationModel.UsernameInUse.IsNotNullOrEmpty())
				{
					statusMessage = BuildStatusErrorMessage(emailValidationModel, email.Count);
				}
				else
				{
					// Send emails
					var resetPasswordUrl = string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.ResetPassword(false));

					var pinRegistrationUrl = "";
					if (App.Settings.Theme == FocusThemes.Education)
						pinRegistrationUrl = string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.PinRegistration(false));

					ServiceClientLocator.AccountClient(App).SendRegistrationPinEmail(emailValidationModel.ValidEmail, pinRegistrationUrl, resetPasswordUrl, App.Settings.CareerExplorerModule);

					if (emailValidationModel.ValidEmail.Count() == 1)
					{
						statusMessage = CodeLocalise("EmailSent.Text", "{0} email address found.  Registration invitation has been queued for processing", emailValidationModel.ValidEmail.Count());
					}
					else
					{
						statusMessage = CodeLocalise("EmailSent.Text", "{0} email addresses found.  Registration invitations have been queued for processing", emailValidationModel.ValidEmail.Count());
					}
				}
			}
			else
			{
				// Invalid file
				statusMessage = CodeLocalise("InvalidFile.Text", "File is of unsupported type. File must be csv or txt and under 2Mb");
			}

			StatusDiv.Attributes.Add("style", "float: right; vertical-align:top; width: 45%; display:inline-block");
			StatusLabel.DefaultText = statusMessage;
		}

		/// <summary>
		/// Validates the file upload.
		/// </summary>
		/// <returns></returns>
	  private bool ValidateFileUpload()
	  {
		  if (FileUpload.FileName.IsNull())
			  return false;

			var fileType = Path.GetExtension(FileUpload.FileName);
		  if (!(fileType == ".csv" || fileType == ".txt"))
			  return false;

			return FileUpload.PostedFile.ContentLength <= 2000000;
		}

		/// <summary>
		/// Extracts the emails.
		/// </summary>
		/// <returns></returns>
		private List<string> ExtractEmails()
		{
			var emails = new List<string>();

			using (var reader = new StreamReader(FileUpload.PostedFile.InputStream))
			{
				while (!reader.EndOfStream)
				{
					var email = reader.ReadLine();

					if (email.IsNotNullOrEmpty())
						emails.Add(email);
				}
			}

			return emails;
		}

		/// <summary>
		/// Builds the status error message.
		/// </summary>
		/// <param name="emailValidationModel">The email validation model.</param>
		/// <param name="uploadCount">The upload count.</param>
		/// <returns></returns>
		private string BuildStatusErrorMessage(EmailValidationModel emailValidationModel, int uploadCount)
		{
			var errorMessage = new StringBuilder();

			// Display validation errors
			if (emailValidationModel.InvalidEmailFormat.IsNotNullOrEmpty() && uploadCount == 1 && emailValidationModel.InvalidEmailFormat.Count() == 1)
			{
				#region Build error message for the email address having invalid email format

				errorMessage.Append(CodeLocalise("InvalidEmailFormatSingle.ErrorMessage", "Uploaded email address has an invalid email format.  Please correct it and try again."));

				#endregion
			}
			else if (emailValidationModel.InvalidEmailFormat.IsNotNullOrEmpty() && uploadCount == emailValidationModel.InvalidEmailFormat.Count())
			{
				#region Build error message for All email addresses having invalid email formats

				errorMessage.Append(CodeLocalise("InvalidEmailFormatAll.ErrorMessage", "All records have an invalid email format.  Please correct these records and try again."));

				#endregion
			}

			if (emailValidationModel.MultipleEmailError.IsNotNullOrEmpty() && uploadCount == 1 && emailValidationModel.MultipleEmailError.Count() == 1)
			{
				#region Build error message for the email address having multiple email addresses on one row

        errorMessage.Append(CodeLocalise("MultipleEmailErrorSingle.ErrorMessage", "Uploaded record has multiple email addresses.  Please correct it and try again."));

				#endregion
			}
			else if (emailValidationModel.MultipleEmailError.IsNotNullOrEmpty() && uploadCount == emailValidationModel.MultipleEmailError.Count())
			{
				#region Build error message for having multiple email addresses on one row

				errorMessage.Append(CodeLocalise("MultipleEmailErrorAll.ErrorMessage", "All records have been identified as having multiple email addresses.  Please correct these records and try again."));

				#endregion
			}

			if (emailValidationModel.UsernameInUse.IsNotNullOrEmpty() && uploadCount == 1 && emailValidationModel.UsernameInUse.Count() == 1)
			{
				#region Build error message for the email address already being used as a username

				errorMessage.Append(CodeLocalise("UsernameInUseFormatSingle.ErrorMessage", "This email address is already in use.  Please enter an alternative one to continue."));

				#endregion
			}
			else if (emailValidationModel.UsernameInUse.IsNotNullOrEmpty() && uploadCount == emailValidationModel.UsernameInUse.Count())
			{
				#region Build error message for All email addresses being used as a username

				errorMessage.Append(CodeLocalise("UsernameInUseFormatAll.ErrorMessage", "All email addresses are already in use.  Please enter alternative ones to continue."));

				#endregion
			}

			if ((emailValidationModel.InvalidEmailFormat.IsNotNullOrEmpty() && uploadCount > emailValidationModel.InvalidEmailFormat.Count())
				|| (emailValidationModel.MultipleEmailError.IsNotNullOrEmpty() && uploadCount > emailValidationModel.MultipleEmailError.Count())
				|| (emailValidationModel.DuplicateEmailError.IsNotNullOrEmpty() && uploadCount > emailValidationModel.DuplicateEmailError.Count())
				|| (emailValidationModel.UsernameInUse.IsNotNullOrEmpty() && uploadCount > emailValidationModel.UsernameInUse.Count()))
      {
				#region Build error message for having invalid email formats

				if (emailValidationModel.InvalidEmailFormat.IsNotNullOrEmpty())
				{
					if (emailValidationModel.InvalidEmailFormat.Count() == 1)
					{
						errorMessage.Append(CodeLocalise("InvalidEmailFormatOne.ErrorMessage",
                        "{0} records found.<br/>The following record has an invalid email format.  Please correct it and try again.", uploadCount, emailValidationModel.InvalidEmailFormat.Count()));
					}
					else
					{
						errorMessage.Append(CodeLocalise("InvalidEmailFormatSome.ErrorMessage",
                        "{0} records found.<br/>The following {1} records have an invalid email format.  Please correct these records and try again.", uploadCount, emailValidationModel.InvalidEmailFormat.Count()));
					}

					errorMessage.Append("<br/><br/>");

					foreach (var invalidEmail in emailValidationModel.InvalidEmailFormat)
					{
						errorMessage.Append(invalidEmail + "<br/>");
					}
				}

				#endregion

				#region Build error message for having multiple email addresses on one row

				if (emailValidationModel.MultipleEmailError.IsNotNullOrEmpty())
				{
					if (errorMessage.Length <= 0)
            errorMessage.Append(CodeLocalise("MultipleEmailErrorHeader.ErrorMessage", "{0} records found.<br/>", uploadCount));
					else
						errorMessage.Append("<br/>");

					if (emailValidationModel.MultipleEmailError.Count() == 1)
					{
						errorMessage.Append(CodeLocalise("MultipleEmailErrorOne.ErrorMessage",
																						"The following record has been identified as having multiple email addresses.  Please correct it and try again.",
																						uploadCount, emailValidationModel.MultipleEmailError.Count()));
					}
					else
					{
						errorMessage.Append(CodeLocalise("MultipleEmailErrorSome.ErrorMessage",
																						"The following {1} records have been identified as having multiple email addresses.  Please correct these records and try again.",
																						uploadCount, emailValidationModel.MultipleEmailError.Count()));
					}
						
					errorMessage.Append("<br/><br/>");

					foreach (var multipleEmail in emailValidationModel.MultipleEmailError)
					{
						errorMessage.Append(multipleEmail + "<br/>");
					}
				}

				#endregion

        #region Build error message for having Duplicate email addresses on one row

        if (emailValidationModel.DuplicateEmailError.IsNotNullOrEmpty())
        {
          if (errorMessage.Length <= 0)
            errorMessage.Append(CodeLocalise("DuplicateEmailErrorHeader.ErrorMessage", "{0} records found.<br/>", uploadCount));
          else
            errorMessage.Append("<br/>");

          if (emailValidationModel.DuplicateEmailError.Count() == 1)
          {
            errorMessage.Append(CodeLocalise("DuplicateEmailErrorOne.ErrorMessage",
                                            "The following record has been identified as being a duplicate email address.  Please correct it and try again."));
          }
          else
          {
            errorMessage.Append(CodeLocalise("DuplicateEmailErrorSome.ErrorMessage",
                                            "The following {0} records have been identified as being duplicate email addresses.  Please correct these records and try again.",
                                            emailValidationModel.DuplicateEmailError.Count()));
          }

          errorMessage.Append("<br/><br/>");

          foreach (var DuplicateEmail in emailValidationModel.DuplicateEmailError)
          {
            errorMessage.Append(DuplicateEmail + "<br/>");
          }
        }

        #endregion

				#region Build error message for having email already being used as a username

				if (emailValidationModel.UsernameInUse.IsNotNullOrEmpty())
				{
					if (emailValidationModel.UsernameInUse.Count() == 1)
					{
						errorMessage.Append(CodeLocalise("UsernameInUseFormatOne.ErrorMessage",
												"{0} records found.<br/>The following record has an email address that is already in use.  Please enter an alternative one to continue.", uploadCount, emailValidationModel.UsernameInUse.Count()));
					}
					else
					{
						errorMessage.Append(CodeLocalise("UsernameInUseFormatSome.ErrorMessage",
												"{0} records found.<br/>The following {1} records have an email address already in use.  Please enter alternative ones to continue.", uploadCount, emailValidationModel.UsernameInUse.Count()));
					}

					errorMessage.Append("<br/><br/>");

					foreach (var invalidEmail in emailValidationModel.UsernameInUse)
					{
						errorMessage.Append(invalidEmail + "<br/>");
					}
				}

				#endregion
			}

			return errorMessage.ToString();
		}
	}
}
