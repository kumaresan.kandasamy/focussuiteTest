﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;
using Focus.Core;
using Focus.Web.Code;
using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersReadOnly)]
	public partial class Employers : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("Page.Title", "Assist #BUSINESSES#");

			if (!IsPostBack)
			{
			  LocaliseUI();
				SetReturnUrl();
			}

      CreateNewEmployerButton.Visible = CanCreateEmployers();
		}

		/// <summary>
		/// Handles the Clicked event of the CreateNewEmployerButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CreateNewEmployerButton_Clicked(object sender, EventArgs e)
		{
			Response.Redirect(UrlBuilder.AssistRegisterEmployer());
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
			CreateNewEmployerButton.Text = CodeLocalise("CreateNewEmployerButton.Text", "Create new #BUSINESS#:LOWER account");

	    if (App.Settings.Theme == FocusThemes.Workforce)
	    {
				FindAnEmployerTitleLabel.Text = CodeLocalise("Global.AssistEmployers.LinkText", "Find a #BUSINESS#:LOWER account");
	    }
			else if (App.Settings.Theme == FocusThemes.Education)
			{
				FindAnEmployerTitleLabel.Text = CodeLocalise("Global.AssistEmployers.LinkText", "Find an #BUSINESS#:LOWER account"); // Because in this instance the placeholder will be "employer"
			}
    }

    /// <summary>
    /// Determines whether the Assist user can create employers
    /// </summary>
    /// <returns>True if they can return employers, false otherwise</returns>
    protected bool CanCreateEmployers()
    {
      return App.User.IsInRole(Constants.RoleKeys.AssistEmployersCreateNewAccount);
    }
	}
}