﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;
using Focus.Core;
using Focus.Common;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
	public partial class ChangePassword : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (App.Settings.DisableAssistAuthentication)
        throw new Exception("Not authorised");

			Page.Title = CodeLocalise("Page.Title", "Change password");

			if (!IsPostBack)
			{
				LocaliseUI();
				NewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
				ConfirmNewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;

				if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist) ApplySSOTheme();
			}
		}

		/// <summary>
		/// Handles the Click event of the ChangePasswordButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ChangePasswordButton_Click(object sender, EventArgs e)
		{
			if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist))
			{
				var currentPassword = (CurrentPasswordTextbox.Text ?? string.Empty).Trim();
				var newPassword = (NewPasswordTextbox.Text ?? string.Empty).Trim();

				try
				{
					ServiceClientLocator.AccountClient(App).ChangePassword(currentPassword, newPassword);

          ConfirmationModal.Show(CodeLocalise("PasswordSuccessfulChange.Title", "Password Changed"),
                CodeLocalise("PasswordSuccessfulChange.Body", "You have successfully changed your password."),
                CodeLocalise("CloseModal.Text", "Return to account settings"));
				}
				catch (Exception ex)
				{
					ChangePasswordValidator.IsValid = false;
					ChangePasswordValidator.ErrorMessage = ex.Message;
				}
			}
			else
			{
				if (App.Settings.SSOManageAccountUrl.IsNotNullOrEmpty())
					Response.Redirect(App.Settings.SSOManageAccountUrl);
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist)
				ChangePasswordButton.Text = CodeLocalise("Global.Save.Label", "Change password");
			else
				ChangePasswordButton.Text = CodeLocalise("Global.Save.Label", "Save");

			CurrentPasswordRequired.ErrorMessage = CodeLocalise("CurrentPasswordRequired.ErrorMessage", "Current password required");
			NewPasswordRequired.ErrorMessage = CodeLocalise("NewPasswordRequired.ErrorMessage", "New password required");
			NewPasswordRegEx.ErrorMessage = CodeLocalise("NewPasswordRegEx.ErrorMessage", "New password must be between 6 and 20 characters and contain at least one number");
			ConfirmNewPasswordRequired.ErrorMessage = CodeLocalise("ConfirmNewPasswordRequired.ErrorMessage", "Retype new password required");
			ConfirmNewPasswordRegEx.ErrorMessage = CodeLocalise("ConfirmNewPasswordRegEx.RegExErrorMessage", "Confirmed new password must be between 6 and 20 characters and contain at least one number");
			NewPasswordCompare.ErrorMessage = CodeLocalise("NewPasswordCompare.ErrorMessage", "New passwords must match");
		}

		private void ApplySSOTheme()
		{
			PasswordTextBoxesPlaceHolder.Visible = false;

			if (App.Settings.SSOManageAccountUrl.IsNullOrEmpty())
				ChangePasswordButton.Visible = false;
		}
	}
}