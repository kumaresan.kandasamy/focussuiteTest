﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployerReferral.aspx.cs" Inherits="Focus.Web.WebAssist.EmployerReferral" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register src="~/WebAssist/Controls/DenyEmployerReferralModal.ascx" tagname="DenyEmployerReferralModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/HoldEmployerReferralModal.ascx" tagname="HoldEmployerReferralModal" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="EditCompanyModal" Src="~/Code/Controls/User/EditCompanyModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="UpdateContactInformationModal" Src="~/Code/Controls/User/UpdateContactInformationModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="NotesAndReminders" Src="~/WebAssist/Controls/NotesAndReminders.ascx" %>
<%@ Register src="~/WebAssist/Controls/EditFEINModal.ascx" tagName="EditFEINModal" tagPrefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<p class="pageNav">
		<asp:LinkButton ID="ReturnLink" runat="server" OnClick="lnkReturn_Click" CausesValidation="False"></asp:LinkButton>
</p>

	<h2><asp:Literal ID="ApproveAccountRequestLabel" runat="server" />&nbsp;<asp:Literal ID="EmployerName2Literal" runat="server" /></h2>

  <div style="width:100%">
    <asp:Panel ID="AccountRequestStatusHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
  	  <table class="accordionTable" role="presentation">
			  <tr>
				  <td>
					  <asp:Image ID="AccountRequestStatusHeaderImage" runat="server" AlternateText="."/>
					  &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AccountRequestStatus.Label", "Account request status")%></a></span></td>
			  </tr>
		  </table>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="AccountRequestStatusPanel">
      <br />
      <div style="float:left">
			  <div>
				  <strong><%= HtmlLocalise("Status.Title", "Account approval status")%>:</strong>
			    <asp:Label ID="AccountApprovalStatusLabel" runat="server"></asp:Label>
          <asp:Label runat="server" ID="AccountApprovalStatusDetails"></asp:Label>
          <span style="width:100%; text-align: right; right:500px; padding-left:500px; float:right"><asp:PlaceHolder runat="server" ID="ApprovalButtonsCell">
            <asp:button ID="HoldButton" runat="server" CssClass="button5" OnClick="HoldButton_Clicked"/>
			      <asp:button ID="DenyButton" runat="server" CssClass="button5" OnClick="DenyButton_Clicked"/>
			      <asp:button ID="ApproveButton" runat="server" CssClass="button1" OnClick="ApproveButton_Clicked"/>
          <br />
        </asp:PlaceHolder></span>
			  </div>
        <br />
			  <asp:Literal ID="InappropriateWordsSummaryLiteral" runat="server" /><br />
			  <ul>
				  <asp:DataList ID="InappropriateWordsList" runat="server" AutoGenerateColumns="false">
					  <ItemTemplate>
						  <li><%# Container.DataItem %></li>
					  </ItemTemplate>
				  </asp:DataList>
			  </ul>
			  <br/>
			  <asp:Literal ID="PendingPostingsLiteral" runat="server" />
			  <br />
			  <ul>
				  <asp:DataList ID="PendingPostingsList" runat="server" AutoGenerateColumns="false" OnItemCommand="Item_Command" DataKeyField="JobId">
					  <ItemTemplate>
						  <li><asp:LinkButton ID="PendingPostingLink" runat="server"><%# Eval("JobTitle") %></asp:LinkButton>
                          <span class="content"><asp:Label runat="server" ID="JobIssuesLiteral"><%# Eval("Issues") %></asp:Label></span></li>
					  </ItemTemplate>
				  </asp:DataList>
			  </ul>
        <br />
      </div>
      <div style="float:right">
        
        
        <asp:PlaceHolder runat="server" ID="EmailPlaceHolder">
          <div style="width:100%; text-align: left">
            <%=HtmlLocalise("CopyOfEmailHeader.Text", "Copy of email notification sent") %><br />
            <asp:TextBox ID="HoldEmailTextBox" runat="server" Width="480px" ClientIDMode="Static" AutoCompleteType="Disabled" TextMode="MultiLine" Rows="10" ReadOnly="True" /><br />
            <br />
          </div>
        </asp:PlaceHolder>
        <br/>
      </div>
      <div style="clear: both"></div>
    </asp:Panel>
    
    <act:CollapsiblePanelExtender ID="AccountRequestStatusCollapsiblePanelExtender" runat="server" TargetControlID="AccountRequestStatusPanel" ExpandControlID="AccountRequestStatusHeaderPanel" 
															    CollapseControlID="AccountRequestStatusHeaderPanel" Collapsed="False" ImageControlID="AccountRequestStatusHeaderImage"
															    SuppressPostBack="true" BehaviorID="AccountRequestStatusCollapsiblePanel"/>
  </div>

  <div style="width:100%">
    <asp:Panel ID="CompanyInformationHeaderPanel" runat="server" CssClass="singleAccordionTitle">
  	  <table class="accordionTable" role="presentation">
			  <tr>
				  <td>
					  <asp:Image ID="CompanyInformationHeaderImage" runat="server" AlternateText="."/>
					  &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("CompanyInformation.Label", "#BUSINESS# information")%></a></span></td>
			  </tr>
		  </table>
    </asp:Panel>

    <asp:Panel ID="CompanyInformationPanel" runat="server">
      <br />
      <div style="float:left">
        <asp:PlaceHolder runat="server" ID="LegalNamePlaceHolder">
          <strong><asp:Literal runat="server" ID="LegalNameTitleLiteral" /></strong> <asp:Label runat="server" ID="LegalNameLabel" /><br />
          <strong><asp:Literal runat="server" ID="EmployerNameTitleLiteral" /></strong> <asp:Label runat="server" ID="EmployerNameLabel" />
        </asp:PlaceHolder>
        <strong><asp:Literal ID="EmployerNameLiteral" runat="server" /></strong><br />
        <asp:Literal ID="EmployerAddress1Literal" runat="server" /><br />
        <asp:Literal ID="EmployerAddress2Literal" runat="server" /><br />
        <%= HtmlLabel("EmployerPublicTransportAvailable.Label", "Public transport accessible")%>: <asp:Literal ID="EmployerPublicTransportAvailableLiteral" runat="server" /><br />
        <br /><%= HtmlLabel("EmployerUrl.Label", "URL")%>: <asp:Literal ID="EmployerUrlLiteral" runat="server" /><br />
        <asp:PlaceHolder id="EmployerPhoneRow" runat="server">
			    <%= HtmlLabel("EmployerPhoneNumber1.Label", "Phone number")%>: <asp:Literal ID="EmployerPhoneNumberLiteral" runat="server" /> - <asp:Literal ID="EmployerPhoneTypeLiteral" runat="server" /><br />
		    </asp:PlaceHolder><br/>
		    <asp:PlaceHolder id="EmployerPhoneType1Row" runat="server">
			    <%= HtmlLabel("EmployerAltPhone1.Label", "Alternate phone number 1")%>: <asp:Literal ID="EmployerAltPhone1Literal" runat="server" /> - <asp:Literal ID="EmployerAltPhoneType1Literal" runat="server" /><br />
		    </asp:PlaceHolder>
		    <asp:PlaceHolder id="EmployerPhoneType2Row" runat="server">
			    <%= HtmlLabel("EmployerAltPhone1.Label", "Alternate phone number 2")%>: <asp:Literal ID="EmployerAltPhone2Literal" runat="server" /> - <asp:Literal ID="EmployerAltPhoneType2Literal" runat="server" /><br />
		    </asp:PlaceHolder>
		    <br />
		    <asp:PlaceHolder runat="server" id="FEINTableRow">
			    <%= HtmlLabel("EmployerFEIN.Label", "FEIN #")%>: <asp:Literal ID="EmployerFEINLiteral" runat="server" /><br />
		    </asp:PlaceHolder>
		    <asp:PlaceHolder runat="server" ID="SEINTableRow">
			    <%= HtmlLabel("EmployerSEIN.Label", "SEIN #")%>: <asp:Literal ID="EmployerSEINLiteral" runat="server" /><br />
        </asp:PlaceHolder>
		    <asp:PlaceHolder runat="server" ID="IndustryClassificationHolder">
		      <br />
		      <strong><%= HtmlLabel("EmployerIndustryClassification.Label", "Industry classiﬁcation")%></strong>: <asp:Literal ID="EmployerIndustryClassificationLiteral" runat="server" /><br />
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="PrimaryNamePlaceHolder">
          <br /><strong><%= HtmlLabel("PrimaryCompany.Label", "Primary #BUSINESS#:LOWER")%>:</strong><br />
		      <asp:Literal ID="PrimaryCompanyLiteral" runat="server" /><br />
        </asp:PlaceHolder>
        <br />
        <asp:Literal runat="server" ID="DescriptionTitleLiteral"></asp:Literal>
        <p>
          <asp:Literal runat="server" ID="BusinessDescriptionLiteral"></asp:Literal>
        </p>
      </div>
      <div style="float:right">
        <asp:HyperLink runat="server" ID="EmployerProfileLink" CssClass="button1 linkbuttonInline"></asp:HyperLink>
        <asp:Button runat="server" ID="EditFEINButton" CssClass="button3" ClientIDMode="Static" OnClick="EditFEINButton_OnClick" />
				<asp:Button runat="server" ID="EditButton" CssClass="button2" OnClick="EditButton_Clicked"/><br />
      </div>
      <div style="clear: both"></div>
    </asp:Panel>  

    <act:CollapsiblePanelExtender ID="CompanyInformationCollapsiblePanelExtender" runat="server" TargetControlID="CompanyInformationPanel" ExpandControlID="CompanyInformationHeaderPanel" 
															    CollapseControlID="CompanyInformationHeaderPanel" Collapsed="true" ImageControlID="CompanyInformationHeaderImage"
															    SuppressPostBack="true" BehaviorID="CompanyInformationCollapsiblePanel"/>
  </div>

  <div style="width:100%">
    <asp:Panel ID="EmployerNotesAndRemindersHeaderPanel" runat="server" CssClass="singleAccordionTitle">
  	  <table class="accordionTable" role="presentation">
			  <tr>
				  <td>
					  <asp:Image ID="EmployerNotesAndRemindersHeaderImage" runat="server" AlternateText="."/>
					  &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("EmployerNotesAndReminders.Label", "#BUSINESS# notes and reminders")%></a></span></td>
			  </tr>
		  </table>
    </asp:Panel>

    <asp:Panel ID="EmployerNotesAndRemindersPanel" runat="server">
	    <uc:NotesAndReminders ID="EmployerNotesReminders" runat="server" />
    </asp:Panel>  

    <act:CollapsiblePanelExtender ID="EmployerNotesAndRemindersCollapsiblePanelExtender" runat="server" TargetControlID="EmployerNotesAndRemindersPanel" ExpandControlID="EmployerNotesAndRemindersHeaderPanel" 
															    CollapseControlID="EmployerNotesAndRemindersHeaderPanel" Collapsed="true" ImageControlID="EmployerNotesAndRemindersHeaderImage"
															    SuppressPostBack="true" BehaviorID="EmployerNotesAndRemindersCollapsiblePanel"/>
  </div>
  
  <div style="width:100%">
    <asp:Panel ID="ContactInformationHeaderPanel" runat="server" CssClass="singleAccordionTitle">
  	  <table class="accordionTable" role="presentation">
			  <tr>
				  <td>
					  <asp:Image ID="ContactInformationHeaderImage" runat="server" AlternateText="."/>
					  &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ContactInformation.Label", "Hiring contact information")%></a></span></td>
			  </tr>
		  </table>
    </asp:Panel>

    <asp:Panel ID="ContactInformationPanel" runat="server">
      <br />
      <div style="float:left">
        <strong><%= HtmlLabel("Employee.Label", "Contact")%>:</strong><br/>
        <asp:Literal ID="EmployeeNameLiteral" runat="server" /><br/>
        <asp:Literal ID="EmployeeAddress1Literal" runat="server" /><br/>
        <asp:Literal ID="EmployeeAddress2Literal" runat="server" /><br/>
        <br />
		    <%= HtmlLabel("EmployeeEmailAddress.Label", "Email")%>: <asp:Literal ID="EmployeeEmailAddressLiteral" runat="server" /><br/>
      
		    <asp:PlaceHolder id="EmployeePhoneRow" runat="server">
			    <%= HtmlLabel("EmployeePhoneNumber1.Label", "Phone number")%>: <asp:Literal ID="EmployeePhoneNumberLiteral" runat="server" /> - <asp:Literal ID="EmployeePhoneTypeLiteral" runat="server" /><br />
		    </asp:PlaceHolder>
		    <asp:PlaceHolder id="EmployeePhoneType1Row" runat="server">
			    <%= HtmlLabel("EmployeeAltPhone1.Label", "Alternate phone number 1")%>: <asp:Literal ID="EmployeeAltPhone1Literal" runat="server" /> - <asp:Literal ID="EmployeeAltPhoneType1Literal" runat="server" /><br />
		    </asp:PlaceHolder>
		    <asp:PlaceHolder id="EmployeePhoneType2Row" runat="server">
			    <%= HtmlLabel("EmployeeAltPhone1.Label", "Alternate phone number 2")%>: <asp:Literal ID="EmployeeAltPhone2Literal" runat="server" /> - <asp:Literal ID="EmployeeAltPhoneType2Literal" runat="server" /><br />
		    </asp:PlaceHolder>
        <br />
      </div>
      <div style="float:right">
        <asp:Button runat="server" ID="EditContactButton" CssClass="button2" OnClick="EditContactButton_Clicked"/>
      </div>
      <div style="clear: both"></div>
    </asp:Panel>  

    <act:CollapsiblePanelExtender ID="ContactInformationCollapsiblePanelExtender" runat="server" TargetControlID="ContactInformationPanel" ExpandControlID="ContactInformationHeaderPanel" 
															    CollapseControlID="ContactInformationHeaderPanel" Collapsed="true" ImageControlID="ContactInformationHeaderImage"
															    SuppressPostBack="true" BehaviorID="ContactInformationCollapsiblePanel"/>
  </div>

  <div style="width:100%">
    <asp:Panel ID="HiringContactNotesAndRemindersHeaderPanel" runat="server" CssClass="singleAccordionTitle">
  	  <table class="accordionTable" role="presentation">
			  <tr>
				  <td>
					  <asp:Image ID="HiringContactNotesAndRemindersHeaderImage" runat="server" AlternateText="."/>
					  &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("HiringContactNotesAndReminders.Label", "Hiring contact notes and reminders")%></a></span></td>
			  </tr>
		  </table>
    </asp:Panel>

    <asp:Panel ID="HiringContactNotesAndRemindersPanel" runat="server">
	    <uc:NotesAndReminders ID="HiringContactNotesReminders" runat="server" />
    </asp:Panel>  

    <act:CollapsiblePanelExtender ID="HiringContactNotesAndRemindersCollapsiblePanelExtender" runat="server" TargetControlID="HiringContactNotesAndRemindersPanel" ExpandControlID="HiringContactNotesAndRemindersHeaderPanel" 
															    CollapseControlID="HiringContactNotesAndRemindersHeaderPanel" Collapsed="true" ImageControlID="HiringContactNotesAndRemindersHeaderImage"
															    SuppressPostBack="true" BehaviorID="HiringContactNotesAndRemindersCollapsiblePanel"/>
  </div>
	
	<asp:UpdatePanel ID="ConfirmationUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<uc:ConfirmationModal ID="Confirmation" runat="server" />
			</ContentTemplate>	
	</asp:UpdatePanel>

	<uc:DenyEmployerReferralModal ID="DenyEmployerReferral" runat="server" />
	<uc:HoldEmployerReferralModal ID="HoldEmployerReferral" runat="server" />
  <uc:EditCompanyModal ID="EditCompany" runat="server" Visible="False" OnCompanyUpdated="EditCompanyModal_EmployerUpdated" OnBusinessUnitUpdated="EditCompanyModal_BusinessUnitUpdated" CloseWindowOnSave="True" />
	<uc:UpdateContactInformationModal ID="UpdateContactInformationModal" Visible="False" runat="server" OnContactUpdated="EditContactModal_ContactUpdated"/>
	<uc:EditFEINModal ID="EditFEINModal" runat="server" OnFEINChanged="EditFEINModal_FEINChanged" />	
</asp:Content>
