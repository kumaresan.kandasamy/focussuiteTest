﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JobSeekerReferral.aspx.cs" Inherits="Focus.Web.WebAssist.JobSeekerReferral" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register src="~/WebAssist/Controls/DenyCandidateReferralModal.ascx" tagname="DenyCandidateReferralModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/HoldCandidateReferralModal.ascx" tagname="HoldCandidateReferralModal" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<p class="pageNav">
		<asp:LinkButton ID="ReturnLink" runat="server" OnClick="lnkReturn_Click" CausesValidation="False"></asp:LinkButton>
	</p>

 
  <h1>
    <%= HtmlLocalise("JobSeekerReferral.Header", "Approve referral request:")%>&nbsp;
    <asp:Literal ID="CandidateNameLiteral" runat="server" />
  </h1>

  <div style="width:100%">
    <asp:Panel ID="ReferralRequestStatusHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
  	  <table class="accordionTable" role="presentation">
			  <tr>
				  <td>
					  <asp:Image ID="ReferralRequestStatusHeaderImage" runat="server" AlternateText="Referral Request Status Header Image" />&nbsp;&nbsp;
            <span class="collapsiblePanelHeaderLabel cpHeaderControl">
              <a href="#"><%= HtmlLocalise("ReferralRequestStatus.Label", "Referral request status")%></a>
            </span>
          </td>
			  </tr>
		  </table>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="ReferralRequestStatusPanel">
      <br />
      <div style="float:left">
			  <div>
				  <strong><%= HtmlLocalise("Status.Title", "Referral request status")%>:</strong>
			    <asp:Label ID="ReferralApprovalStatusLabel" runat="server"></asp:Label>
			    <br />
          <asp:Label runat="server" ID="ReferralApprovalStatusDetails"></asp:Label>
          <br /><br />
				  <strong><%= HtmlLocalise("StarMatch.Title", "Referral star match")%>:</strong>
			    <asp:image ID="ReferralScoreImage" runat="server" AlternateText="Referral Score Image" />
			    <br />
				  <strong><%= HtmlLocalise("ReferralReason.Title", "Reason for referral")%>:</strong>
          <asp:Literal ID="ReferralReasonLiteral" runat="server" />
			    <br />
			    <br />
   		  </div>
      </div>

      <div style="float:right">
        <asp:PlaceHolder runat="server" ID="ApprovalButtonsCell">
          <div style="width:100%; text-align: right">
						<asp:button ID="HoldButton" runat="server" CssClass="button4" OnClick="HoldButton_Clicked"/>
						<asp:button ID="DenyButton" runat="server" CssClass="button4" OnClick="DenyButton_Clicked"/>
						<asp:button ID="ApproveButton" runat="server" CssClass="button2 right" OnClick="ApproveButton_Clicked"/>
          </div>
          <br />
          <br />
        </asp:PlaceHolder>
        
        <asp:PlaceHolder runat="server" ID="EmailPlaceHolder" Visible="False">
          <div style="width:100%; text-align: left">
            <%=HtmlLocalise("CopyOfEmailHeader.Text", "Copy of email notification sent") %><br />
            <asp:TextBox ID="HoldEmailTextBox" runat="server" Width="480px" ClientIDMode="Static" AutoCompleteType="Disabled" TextMode="MultiLine" Rows="10" ReadOnly="True" /><br />
            <br />
          </div>
        </asp:PlaceHolder>
        <br/>
      </div>

      <div style="clear: both"></div>
    </asp:Panel>
    
		<asp:UpdatePanel runat="server" ID="ConfirmationUpdatePanel" UpdateMode="Conditional">
			<ContentTemplate>
					<uc:ConfirmationModal ID="ReferralConfirmationModal" runat="server" Width="300px" Height="200px" />
			</ContentTemplate>  
		</asp:UpdatePanel>

    <act:CollapsiblePanelExtender ID="ReferralRequestStatusCollapsiblePanelExtender" runat="server" TargetControlID="ReferralRequestStatusPanel" ExpandControlID="ReferralRequestStatusHeaderPanel" 
															    CollapseControlID="ReferralRequestStatusHeaderPanel" Collapsed="False" ImageControlID="ReferralRequestStatusHeaderImage"
															    SuppressPostBack="true" BehaviorID="ReferralRequestStatusCollapsiblePanel"/>
  </div>
  
  <div style="width:100%">
    <asp:Panel ID="ResumeAndPostingHeaderPanel" runat="server" CssClass="singleAccordionTitle on">
  	  <table class="accordionTable" role="presentation">
			  <tr>
				  <td>
					  <asp:Image ID="ResumeAndPostingHeaderImage" runat="server" AlternateText="Resume And Posting Header Image" />&nbsp;&nbsp;
            <span class="collapsiblePanelHeaderLabel cpHeaderControl">
              <a href="#"><%= HtmlLocalise("ResumeAndPosting.Label", "Referral request information")%></a>
            </span>
          </td>
			  </tr>
		  </table>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="ResumeAndPostingPanel">
      <br />
      <table style="width: 100%;" role="presentation">
		    <tr>
			    <td style="width:49%">
			      <div style="float:left">
			        <h2><%= HtmlLocalise("Resume.Header", "Resume")%></h2>
            </div>
            <div style="float:right">
              <asp:HyperLink runat="server" ID="JobSeekerProfileLink" CssClass="button1 linkbutton"></asp:HyperLink>
            </div>
			    </td>
          <td style="width:2%">&nbsp;</td>
			    <td style="width:49%">
			      <div style="float:left">
			        <h2><%= HtmlLocalise("JobPosting.Header", "#POSTINGTYPE# Posting")%></h2>
            </div>
            <div style="float:right">
              <asp:HyperLink runat="server" ID="JobOrderProfileLink" CssClass="button1 linkbutton"></asp:HyperLink>
            </div>
			    </td>
		    </tr>

				 <tr>
			    <td style="width:49%"></td>
          <td style="width:2%">&nbsp;</td>
			    <td style="width:49%">
			      <div style="float:left">
			        <asp:Label ID="PostingApprovalStatusLabel" runat="server"></asp:Label>
            </div>
			    </td>
		    </tr>

		    <tr>
			    <td>
				    <div style="overflow:auto;height:400px;width:100%">
					    <asp:Literal ID="ResumeLiteral" runat="server" />
				    </div>
			    </td>
          <td>&nbsp;</td>
			    <td>
						
			      <asp:image ID="ForeignLabourCertificationImage" runat="server" Visible="false" CssClass="jobSeekerReferralForeignLabour" AlternateText="Foreign Labour Certification Image"/>
				    <div style="overflow:auto;height:400px;width:100%">
					    <asp:Literal ID="PostingLiteral" runat="server" />
				    </div>
			    </td>
		    </tr>
	    </table>
    </asp:Panel>
    
    <act:CollapsiblePanelExtender ID="ResumeAndPostingCollapsiblePanelExtender" runat="server" TargetControlID="ResumeAndPostingPanel" ExpandControlID="ResumeAndPostingHeaderPanel" 
															    CollapseControlID="ResumeAndPostingHeaderPanel" Collapsed="False" ImageControlID="ResumeAndPostingHeaderImage"
															    SuppressPostBack="true" BehaviorID="ResumeAndPostingCollapsiblePanel"/>
  </div>

	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
	<uc:DenyCandidateReferralModal ID="DenyCandidateReferral" runat="server" />
	<uc:HoldCandidateReferralModal ID="HoldCandidateReferralModal" runat="server" />
</asp:Content>
