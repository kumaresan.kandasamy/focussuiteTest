﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Employers.aspx.cs" Inherits="Focus.Web.WebAssist.Employers" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistEmployersNavigation.ascx" tagname="AssistEmployersNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/FindEmployer.ascx" tagname="FindEmployer" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<table style="width:100%;" role="presentation">
		<tr>
			<td style="vertical-align: top;">
				<uc:AssistEmployersNavigation ID="AssistEmployersNavigationMain" runat="server" />
			</td>
			<td style="vertical-align: top; text-align:right;"><asp:Button ID="CreateNewEmployerButton" runat="server" class="button1" OnClick="CreateNewEmployerButton_Clicked" CausesValidation="false" /></td>
		</tr>
	</table>

  <h1><asp:Label runat="server" ID="FindAnEmployerTitleLabel"/></h1>
	<uc:FindEmployer ID="FindEmployer" runat="server" />

</asp:Content>