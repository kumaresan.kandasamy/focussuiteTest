﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Controllers.WebAssist;
using Focus.Web.Core.Models;
using Focus.Web.WebAssist.Controls;
using Framework.Core;
using Focus.Common.Code;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersReadOnly)]
	public partial class Job : PageBase
	{
		#region Properties

		protected JobViewModel JobModel
		{
			get { return App.GetSessionValue("JobView:Model", new JobViewModel()); }
			set { App.SetSessionValue("JobView:Model", value); }
		}

		private ActionTypes? SelectedAction
		{
			get { return GetViewStateValue<ActionTypes>("Employers:SelectedAction"); }
			set { SetViewStateValue("Employers:SelectedAction", value); }
		}

		/// <summary>
		/// Gets or sets the model.
		/// </summary>
		protected JobDetailsView Model
		{
			get { return _model ?? (_model = GetViewStateValue("Job:Model", new JobDetailsView())); }
			set { SetViewStateValue("Job:Model", value); }
		}
		private long _jobId;
		private JobDetailsView _model;
		private UserDetailsModel _userModel;
		private string _assistUserDetails;
		private string _businessUnitName;
		private JobActivityView _jobActivitySummary;
		private string _jobOrderText;
    protected bool _jobPostingFilterApplied;
		protected string CloseBusinessWindowErrorMessage = "";

		private string BlockedPopUpMessage
		{
			get
			{
				return CodeLocalise("AccessEmployeePopupBlocked.Error.Text",
														"Unable to access #BUSINESS#:LOWER account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.");
			}
		}

		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected JobController PageController { get { return PageControllerBaseProperty as JobController; } }

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		protected override IPageController RegisterPageController()
		{
			return new JobController(App);
		}

		/// <summary>
		/// Gets or sets the user model.
		/// </summary>
		/// <value>The user model.</value>
		protected UserDetailsModel UserModel
		{
			get { return _userModel ?? (_userModel = GetViewStateValue("Job:UserModel", new UserDetailsModel())); }
			set { _userModel = value; }
		}

		/// <summary>
		/// Gets the text as to whether this a job or internship
		/// </summary>
		/// <value>The user model.</value>
		protected string JobOrderText
		{
			get
			{
				if (_jobOrderText.IsNull())
					_jobOrderText = (App.Settings.Theme == FocusThemes.Workforce || JobModel.JobType == JobTypes.Job) ? CodeLocalise("JobOrder.Label", "#EMPLOYMENTTYPE#") : CodeLocalise("Internship.Label", "Internship");
				return _jobOrderText;
			}
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            if (Request.QueryString["filtered"] != null)
            {
                _jobPostingFilterApplied = Convert.ToBoolean(Request.QueryString["filtered"]);

            }
            
      // Get the job Id
			if (Page.RouteData.Values.ContainsKey("id"))
				long.TryParse(Page.RouteData.Values["id"].ToString(), out _jobId);

			if (_jobId == 0)
				throw new Exception(FormatError(ErrorTypes.InvalidJob, "Invalid job id"));
			
            
            Page.Title = (_jobId > 0) ? CodeLocalise("Page.Title", "Job View - " + _jobId) : CodeLocalise("Page.Title", "Job View");
            if (Request.QueryString["filtered"] != null) 
                Page.Title = (_jobId > 0) ?CodeLocalise("Page.Title", "Job View - filtered -"+_jobId):CodeLocalise("Page.Title", "Job View - filtered");

			CloseBusinessWindowErrorMessage = CodeLocalise("CloseBusinessWindow.Error.Text", "You can only have one #BUSINESS#:LOWER window open at a time");

			if (!IsPostBack)
			{
				CreditCheckRequiredRow.Visible = App.Settings.ShowCreditCheck;

				JobModel = new JobViewModel { JobId = _jobId, JobOrderActivitySortColumn = "date", JobOrderActivitySortDirection = "desc" };
				RetrieveJobData();
				NotesReminders.EntityId = _jobId;
				NotesReminders.EntityType = EntityTypes.Job;
				NotesReminders.ReadOnly = (!App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator));
				NotesReminders.Bind();
				ReferralSummaryList.JobId = _jobId;
				ActivityList.JobId = _jobId;
			  ActivityList.JobType = JobModel.JobType.GetValueOrDefault();

				Localise();
				ApplyBranding();
				BindData();
				HidePanels();

				CriminalRecordExclusionCheckBox.Enabled = App.User.IsInRole(Constants.RoleKeys.AssistEmployerOverrideCriminalExclusions);
				CreditCheckRequiredCheckBox.Enabled = App.User.IsInRole(Constants.RoleKeys.AssistEmployerOverrideCreditCheckRequired);

				AssignedOffices.ContextId = _jobId;
				AssignedOffices.Bind();
			}

			JobOrdersActionDropDown.Attributes.Add("OnChange", "HideSubAction()");
		}

		/// <summary>
		/// Handles the Pre-Render event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event (The Pag).</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{

			HoldJobButtonCell.Visible = !((JobModel.JobStatus == JobStatuses.OnHold) || (JobModel.JobStatus == JobStatuses.Draft) || (JobModel.JobStatus == JobStatuses.Closed));
			ReactivateJobButtonCell.Visible = (Model.ApprovalStatus != ApprovalStatuses.Rejected && Model.ApprovalStatus != ApprovalStatuses.WaitingApproval && Model.JobStatus == JobStatuses.OnHold);
			CloseJobButtonCell.Visible = (JobModel.JobStatus != JobStatuses.Draft) && (JobModel.JobStatus != JobStatuses.Closed);
            DeleteButtonCell.Visible = (JobModel.JobStatus == JobStatuses.Draft) && App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
            EditJobButtonCell.Visible = (JobModel.JobStatus != JobStatuses.Closed) && App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
			RefreshJobButtonCell.Visible = JobModel.JobStatus == JobStatuses.Active || (JobModel.JobStatus == JobStatuses.Closed && JobModel.AutoClosed && JobModel.JobClosingOn.HasValue && (DateTime.Now - (DateTime)JobModel.JobClosingOn).TotalDays <= 30);
			DuplicateJobButtonCell.Visible = (JobModel.JobStatus != JobStatuses.Draft);
			ViewMatchesButtonCell.Visible = (JobModel.JobStatus != JobStatuses.Draft);

            ViewJobAsEmployerLinkButton.Visible =
            AccessEmployersAccountLinkButton.Visible = App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
		}

		/// <summary>
		/// Retrieves the job data.
		/// </summary>
		private void RetrieveJobData()
		{
			Model = ServiceClientLocator.JobClient(App).GetJobDetails(_jobId, true);

			UserModel = new UserDetailsModel();

			if (Model.HiringManagerId.HasValue)
			{
				var employee = ServiceClientLocator.EmployeeClient(App).GetEmployee(Model.HiringManagerId.GetValueOrDefault());
				var userId = ServiceClientLocator.AccountClient(App).GetUserDetails(personId: employee.PersonId).UserId;
				UserModel = ServiceClientLocator.AccountClient(App).GetUserDetails(userId);
			}

			// If job was posted by Assist User, need to retrieve assist user details.
			if (Model.PostedBy != null && Model.PostedBy != Model.HiringManagerId)
			{
				var assistUser = ServiceClientLocator.AccountClient(App).GetUserDetails(Model.PostedBy.GetValueOrDefault());
				_assistUserDetails = String.Format("{0} {1} {2}{3} ", assistUser.PersonDetails.FirstName, assistUser.PersonDetails.LastName, assistUser.PersonDetails.JobTitle.IsNotNullOrEmpty() ? String.Format("({0}) ", assistUser.PersonDetails.JobTitle) : "", CodeLocalise("OnBehalfOf", "on behalf of"));
			}

			// Get job client data
			var job = ServiceClientLocator.JobClient(App).GetJobView(_jobId);
			_businessUnitName = job.BusinessUnitName;

			_jobActivitySummary = ServiceClientLocator.JobClient(App).GetJobActivitySummary(_jobId);

			// Set as temp as JobModel in session was not binding
			var temp = JobModel;
			// Persist employee id for futher actions on the page.
			temp.EmployeeId = job.EmployeeId;
			temp.EmployerId = job.EmployerId;
			temp.JobStatus = job.JobStatus;
			temp.JobTitle = job.JobTitle;
			temp.JobType = job.JobType;
			temp.JobClosedOn = job.ClosedOn;
			temp.JobClosingOn = job.ClosingOn;
            		temp.AutoClosed = Model.AutoClosed;

			JobModel = temp;
		}

		/// <summary>
		/// Binds the data to the controls
		/// </summary>
		private void BindData()
		{
			#region Title

			JobViewTitleHeader.Text = String.Format("{0}:	{1}, {2}", CodeLocalise("Title.Text", "{0} profile", JobOrderText), Model.JobTitle, _businessUnitName);

			#endregion

			#region Applicant Status

			Status1.Text = String.Format("{0} {1} ", Model.ApplicationCount == 1 ? CodeLocalise("ThereIs", "There is") : CodeLocalise("ThereAre", "There are"), Model.ApplicationCount);

			ApplicantsLink.Text = Model.ApplicationCount == 1 ? CodeLocalise("Applicant", "applicant") : CodeLocalise("Applicants", "applicants");
            ApplicantsLink.NavigateUrl = App.User.IsInRole(Constants.RoleKeys.AssistEmployersReadOnly) ? "" : UrlBuilder.AssistPool(TalentPoolListTypes.Applicants, Model.JobId);

			Status2.Text = String.Format(" {0}  {1} ", CodeLocalise("And", "and"), Model.RecommendedCount);

			RecommendedLink.Text = CodeLocalise("Recommended", "recommended");
            RecommendedLink.NavigateUrl = App.User.IsInRole(Constants.RoleKeys.AssistEmployersReadOnly) ? "" : UrlBuilder.AssistPool(TalentPoolListTypes.MatchesForThisJob, Model.JobId);

			Status3.Text = String.Format(" {0} {1}",
				Model.RecommendedCount == 1 ? CodeLocalise("Candidate", "candidate") : CodeLocalise("Candidates", "candidates"),
				Model.JobType.IsInternship() ? CodeLocalise("ForThisInternship", "for this internship") : CodeLocalise("ForThisJob", "for this job"));

			#endregion

			#region Job order details

			if (UserModel.PersonDetails == null)
			{
				lblPostedBy.Text = CodeLocalise("PostedByName.NotDefined", "Not defined");
				lblEmployerTelephone.Text = CodeLocalise("PostedByContactDetails.NotDefined", "Not defined");
				lblEmployerEmail.Text = CodeLocalise("PostedByEmail.NotDefined", "Not defined");
			}
			else
			{
				lblPostedBy.Text = _assistUserDetails + UserModel.PersonDetails.FirstName + @" " + UserModel.PersonDetails.LastName + (UserModel.PersonDetails.JobTitle.IsNotNullOrEmpty() ? " (" + UserModel.PersonDetails.JobTitle + ")" : "");
				lblEmployerTelephone.Text = UserModel.PrimaryPhoneNumber != null ? Regex.Replace(UserModel.PrimaryPhoneNumber.Number, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat) : CodeLocalise("NotDefined", "not defined");
				lblEmployerEmail.Text = UserModel.PersonDetails.EmailAddress;
			}

			lblPostedDate.Text = (Model.PostedOn.HasValue ? CodeLocalise("JobPostedOnDate.Format", "{0:MMM d, yyyy}", Model.PostedOn.Value) : CodeLocalise("JobPostedOnDate.NotDefined", "Not posted"));
			FirstPostedTextLabel.Text = String.Format("{0} {1}", JobOrderText, CodeLocalise("FirstPosted.Label", "first posted"));

			lblTimeSinceCreation.Text = (Model.CreatedOn.HasValue ? TimeSince(Model.CreatedOn) : CodeLocalise("JobCreatedOnDate.NotDefined", "Not defined"));
			TimeSinceTextLabel.Text = String.Format("{0} {1} {2}", HtmlLocalise("TimeSince.Label", "Time since"), JobOrderText.ToLower(), CodeLocalise("Creation.Label", "creation"));

			lblLastEdited.Text = (Model.LastPostedOn.HasValue ? CodeLocalise("JobEditedDate.Format", "{0:MMM d, yyyy}", Model.LastPostedOn.Value) : CodeLocalise("JobPostedDate.NotDefined", "never"));
			LastEditedTextLabel.Text = String.Format("{0} {1}", JobOrderText, CodeLocalise("LastEdited.Label", "last edited"));

			lblLastRefreshed.Text = (Model.LastRefreshedOn.HasValue ? CodeLocalise("JobRefreshedDate.Format", "{0:MMM d, yyyy}", Model.LastRefreshedOn.Value) : CodeLocalise("JobRefreshedDate.NotDefined", "never"));
			LastRefreshedTextLabel.Text = String.Format("{0} {1}", JobOrderText, CodeLocalise("LastRefreshed.Label", "last refreshed"));

			lblTimeSinceLastEdit.Text = (Model.LastPostedOn.HasValue ? TimeSince(Model.LastPostedOn) : CodeLocalise("NotApplicable", "n/a"));

			lblTimeSinceLastRefresh.Text = (Model.LastRefreshedOn.HasValue ? TimeSince(Model.LastRefreshedOn) : CodeLocalise("NotApplicable", "n/a"));

			if (Model.ClosedOn.IsNotNull() || (Model.ClosingDate.HasValue && Model.ClosingDate.Value < DateTime.Now))
			{
				var closeDate = Model.ClosedOn.HasValue ? Model.ClosedOn : Model.ClosingDate;

				lblClosingDate.Text = CodeLocalise("JobClosedDate.Format", "{0:MMM d, yyyy}", closeDate);
				ClosingDateTextLabel.Text = String.Format("{0} {1}", JobOrderText, CodeLocalise("ClosedDate.Label", "closed date"));

				TimeUntilClosingPlaceHolder.Visible = false;
			}
			else
			{
				lblClosingDate.Text = (Model.ClosingDate.HasValue ? CodeLocalise("JobClosingDate.Format", "{0:MMM d, yyyy}", Model.ClosingDate.Value) : CodeLocalise("JobClosingDate.NotDefined", "Not defined"));
				ClosingDateTextLabel.Text = String.Format("{0} {1}", JobOrderText, CodeLocalise("ClosingDate.Label", "closing date"));

				TimeUntilClosingLabel.Text = HtmlLocalise("TimeUntilClosing.Label", "Time until closing date");
				lblTimeUntilClosing.Text = (Model.ClosingDate.HasValue ? TimeUntil(Model.ClosingDate) : CodeLocalise("JobClosingDate.NotDefined", "Not defined"));

				TimeUntilClosingPlaceHolder.Visible = true;
			}

			if (Model.JobStatus == JobStatuses.Active && Model.VeteranPriorityEndDate.HasValue && Model.VeteranPriorityEndDate >= DateTime.Now)
			{
				switch (Model.ExtendVeteranPriority.GetValueOrDefault(ExtendVeteranPriorityOfServiceTypes.None))
				{
					case ExtendVeteranPriorityOfServiceTypes.None:
						VeteranPriorityLiteral.Text = CodeLocalise("VeteranPriorityLiteral.Auto", "This posting is currently available to veteran job seekers only until {0} {1}", Model.VeteranPriorityEndDate.Value.ToString("MMM d, yyyy"), Model.VeteranPriorityEndDate.Value.ToShortTimeString());
						break;
					case ExtendVeteranPriorityOfServiceTypes.ExtendUntil:
						VeteranPriorityLiteral.Text = CodeLocalise("VeteranPriorityLiteral.ExtendUntil", "This posting is currently available to veteran job seekers only until {0}", Model.VeteranPriorityEndDate.Value.ToString("MMM d, yyyy"));
						break;
					case ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting:
						VeteranPriorityLiteral.Text = CodeLocalise("VeteranPriorityLiteral.ExtendForLifeOfPosting", "This posting is currently available to veteran job seekers only until {0}", Model.ClosingDate.GetValueOrDefault().ToString("MMM d, yyyy"));
						break;
				}
			}
			else
			{
				VeteranPriorityRow.Visible = false;
			}
			#endregion

			#region Description

            if (App.User.IsInRole(Constants.RoleKeys.AssistEmployersReadOnly))
            {
                JobActionRow.Visible = false;
                EmailEmployeeLinkButton.Visible = false;
            }

			JobReference.Text = Model.JobId.ToString(CultureInfo.CurrentCulture);
			JobOrderSuppressed.Text = Model.ScreeningPreferences == ScreeningPreferences.JobSeekersMustBeScreened ? CodeLocalise("Yes.Label", "Yes") : CodeLocalise("No.Label", "No");

			NumberOfOpenings.Text = (Model.NumberOfOpenings > 0
																 ? Model.NumberOfOpenings.ToString()
																 : CodeLocalise("NumberOfOpenings.NotDefined", "Not defined"));

			// Populate Method of Application
			var methodsOfApplication = new List<string>();

			var contactMethods = Model.InterviewContactPreferences;

			if ((contactMethods & ContactMethods.Email) == ContactMethods.Email)
				methodsOfApplication.Add(CodeLocalise("EmailResume", "Email resume to specified address"));

			if ((contactMethods & ContactMethods.Online) == ContactMethods.Online)
				methodsOfApplication.Add(CodeLocalise("ApplyOnline", "Apply online through job URL"));

			if ((contactMethods & ContactMethods.Mail) == ContactMethods.Mail)
				methodsOfApplication.Add(CodeLocalise("EmailResume", "Mail resume to specified address"));

			if ((contactMethods & ContactMethods.Fax) == ContactMethods.Fax)
				methodsOfApplication.Add(CodeLocalise("FaxResume", "Fax resume to specified number"));

			if ((contactMethods & ContactMethods.Telephone) == ContactMethods.Telephone)
				methodsOfApplication.Add(CodeLocalise("Telephone", "Call for an appointment"));

			if ((contactMethods & ContactMethods.InPerson) == ContactMethods.InPerson)
				methodsOfApplication.Add(CodeLocalise("InPerson", "Send seeker(s) for in-person interview"));

			if ((contactMethods & ContactMethods.FocusTalent) == ContactMethods.FocusTalent)
				methodsOfApplication.Add(CodeLocalise("FocusTalent", "Through #FOCUSTALENT# Account"));

			if ((contactMethods & ContactMethods.FocusCareer) == ContactMethods.FocusCareer)
				methodsOfApplication.Add(CodeLocalise("FocusCareer", "Through #FOCUSCAREER# Account"));

			//if (methodsOfApplication.Count == 0)
			//{
			//  ApplicationMethod.Text = CodeLocalise("NoneSpecified", "none specified");
			//}
			//else
			//{
			//  foreach (var method in methodsOfApplication)
			//  {
			//    ApplicationMethod.Text += method + @", ";
			//  }
			//  ApplicationMethod.Text = ApplicationMethod.Text.Substring(0, ApplicationMethod.Text.Length - 2);
			//}

			PositionDetails.Text = (string.IsNullOrEmpty(Model.PositionDetails) ? CodeLocalise("PositionDetails.NotDefined", "Not defined") : Model.PositionDetails);
            CareerJobReference.Text = (string.IsNullOrEmpty(Model.ExternalId) ? CodeLocalise("CareerJobReference.NotDefined", "Unknown") : Model.ExternalId);
			JobStatus.Text = Model.JobStatus.ToString();
			Confidential.Text = Model.IsConfidential ? CodeLocalise("Yes.Label", "Yes") : CodeLocalise("No.Label", "No");

			CriminalRecordExclusionCheckBox.Visible = Model.HasCheckedCriminalRecordExclusion;

			if (Model.HasCheckedCriminalRecordExclusion)
				CriminalRecordExclusionCheckBox.Checked = Model.CriminalBackgroundExclusionRequired.IsNotNull() && (bool)Model.CriminalBackgroundExclusionRequired;

            if(App.Settings.ShowCreditCheck)
                CreditCheckRequiredCheckBox.Checked = Model.CreditCheckRequired.GetValueOrDefault();

			if (Model.ForeignLabourCertificationDetails.IsNotNullOrEmpty())
			{
				JobConditionsLabel.Text = string.Concat(CodeLocalise("JobConditions.Label", "Job conditions"), ": ");
				ForeignLabourCertification.Text = Model.ForeignLabourCertificationDetails;
			}

			#endregion

			#region Requirements/Details/Salary And Benefits/Recruitment Information

			JobRequirements.Text = Model.JobRequirements;
			JobDetails.Text = Model.JobDetails; ;
			JobSalaryBenefits.Text = Model.JobSalaryBenefits;
			JobRecruitmentInformation.Text = Model.JobRecruitmentInformation;

			JobRequirementsHeaderRow.Visible = JobRequirementsRow.Visible = Model.JobRequirements.IsNotNullOrEmpty();
			JobDetailsHeaderRow.Visible = JobDetailsRow.Visible = Model.JobDetails.IsNotNullOrEmpty();
			JobSalaryBenefitsHeaderRow.Visible = JobSalaryBenefitsRow.Visible = Model.JobSalaryBenefits.IsNotNullOrEmpty();
			JobRecruitmentInformationHeaderRow.Visible = JobRecruitmentInformationRow.Visible = Model.JobRecruitmentInformation.IsNotNullOrEmpty();

			#endregion

			#region ActivitySummary

			//StarMatches.Text = _jobActivitySummary.StarMatches.ToString();
			//StarApplicants.Text = _jobActivitySummary.StarDidNotView.ToString();
			//StarReferrals.Text = _jobActivitySummary.StarReferrals.ToString();
			//StarApplicants.Text = _jobActivitySummary.StarApplicants.ToString();

			StaffReferrals.Text = _jobActivitySummary.StaffReferrals.ToString(CultureInfo.CurrentCulture);
			SelfReferrals.Text = _jobActivitySummary.SelfReferrals.ToString(CultureInfo.CurrentCulture);
			TotalReferrals.Text = (_jobActivitySummary.StaffReferrals + _jobActivitySummary.SelfReferrals).ToString(CultureInfo.CurrentCulture);
			EmployerInvitationsSent.Text = _jobActivitySummary.EmployerInvitationsSent.ToString(CultureInfo.CurrentCulture);
			SeekersViewed.Text = _jobActivitySummary.SeekersViewed.ToString(CultureInfo.CurrentCulture);
			SeekersDidNotView.Text = _jobActivitySummary.SeekersDidNotView.ToString(CultureInfo.CurrentCulture);
			SeekersClicked.Text = _jobActivitySummary.SeekersClicked.ToString(CultureInfo.CurrentCulture);
			SeekersDidNotClick.Text = _jobActivitySummary.SeekersDidNotClick.ToString(CultureInfo.CurrentCulture);
			ApplicantsInterviewed.Text = _jobActivitySummary.ApplicantsInterviewed.ToString(CultureInfo.CurrentCulture);
			ApplicantsFailedToShow.Text = _jobActivitySummary.ApplicantsFailedToShow.ToString(CultureInfo.CurrentCulture);
			ApplicantsDenied.Text = _jobActivitySummary.ApplicantsDenied.ToString(CultureInfo.CurrentCulture);
			ApplicantsHired.Text = _jobActivitySummary.ApplicantsHired.ToString(CultureInfo.CurrentCulture);
			ApplicantsRejected.Text = _jobActivitySummary.ApplicantsRejected.ToString(CultureInfo.CurrentCulture);
			JobOffersRefused.Text = _jobActivitySummary.JobOffersRefused.ToString(CultureInfo.CurrentCulture);

			ApplicantsDidNotApply.Text = _jobActivitySummary.ApplicantDidNotApply.ToString(CultureInfo.CurrentCulture);

			#endregion

			BindActionDropDown();
			//BindSubActionDropDownToStaff();
		}

		/// <summary>
		/// Localises this page.
		/// </summary>
		private void Localise()
		{
			var returnPage = GetReturnUrl();
			if (returnPage.Contains("referral"))
      {
        ReturnLink.Text = CodeLocalise("ReturnToReport", "return to referral request");
        ReturnLink.NavigateUrl = returnPage;
      }
      else if (returnPage.Contains("joborderdashboard") || !Page.RouteData.Values.ContainsKey("session"))
			{
        ReturnLink.Text = CodeLocalise("JobView.ReturnToJobOrderDashboard", "return to #EMPLOYMENTTYPE# dashboard");
        if (_jobPostingFilterApplied)
        {
          ReturnLink.NavigateUrl = UrlBuilder.JobOrderDashboard() + "?filtered=true";
        }
        else
        {
          ReturnLink.NavigateUrl = UrlBuilder.JobOrderDashboard();
        }
			}
      else 
      {
        ReturnLink.Text = CodeLocalise("JobView.ReturnToReport", "return to report");
        ReturnLink.NavigateUrl = UrlBuilder.ReportingSpecific("joborder", Page.RouteData.Values["session"].ToString());
      }

		  PrintButton.Text = CodeLocalise("Global.Print.Text", "Print");

			EditLinkButton.NavigateUrl = UrlBuilder.AssistJobWizard(JobModel.JobId);
            EditLinkLabel.Text = CodeLocalise("EditJobToolTip", "Edit");

            RefreshJobLabel.Text = CodeLocalise("RefreshJobToolTip", "Refresh");
            CloseJobLabel.Text = CodeLocalise("CloseJobToolTip", "Close");
            HoldJobLabel.Text = CodeLocalise("HoldJobToolTip", "Hold");
            ReactivateJobLabel.Text = CodeLocalise("ReativateJobToolTip", "Reactivate");
            DuplicateJobLabel.Text = CodeLocalise("DuplicateJobToolTip", "Duplicate");
            DeleteJobLabel.Text = CodeLocalise("DeleeteJobToolTip", "Delete");

						EmailEmployeeLinkButton.Text = CodeLocalise("EmailEmployer", "Email #BUSINESS#:LOWER");

						AccessEmployersAccountLinkButton.Text = CodeLocalise("AccessEmployersAccount", "Access #BUSINESS#:LOWER's account");

			ViewJobAsEmployerLinkButton.Text = String.Format("{0} {1}", CodeLocalise("Access.Label", "Access"), JobOrderText.ToLower());
			PreviewJobOrderLinkButton.Text = String.Format("{0} {1}", CodeLocalise("Preview.Label", "Preview"), JobOrderText.ToLower());

			ViewMatchesButton.Text = CodeLocalise("MatchJobButton.Text", "View matches from the database");

			SubActionButton.Text = CodeLocalise("JobOrdersSubActionButton.Text", "Go");
			JobOrdersActionButton.Text = CodeLocalise("JobOrdersActionButton.Text", "Go");

			SuppressedLabel.Text = String.Format("{0} {1}", JobOrderText, CodeLocalise("Suppressed.Label", "suppressed"));
			JobOrderLabel.Text = String.Format("{0} {1}", JobOrderText, CodeLocalise("Details.Label", "Details")).ToTitleCase();

			ActivityLogLabel.Text = String.Format("{0} {1}", JobOrderText, HtmlLocalise("ActivityLog.Label", "Activity Log")).ToTitleCase();

			CriminalRecordExclusionCheckBox.Text = CodeLocalise("CriminalCheck", "Criminal background exclusion required");

			CreditCheckRequiredCheckBox.Text = CodeLocalise("CreditCheckRequiredCheckBox.Text", "Credit background check required");

      ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");
		}

		private void ApplyBranding()
		{
			ApplicantStatusHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ApplicationStatusPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ApplicationStatusPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			JobOrderDetailsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobOrderDetailsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobOrderDetailsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			DescriptionPanelImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			DescriptionPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			DescriptionPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ActivitySummaryPanelImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ActivitySummaryPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ActivitySummaryPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			JobOrderActivityPanelImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobOrderActivityPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobOrderActivityPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ReferralSummaryImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ReferralSummaryPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ReferralSummaryPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			NotesRemindersPanelImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			NotesRemindersPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			NotesRemindersPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			AssignedOfficeHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			AssignedOfficeCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			AssignedOfficeCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Hides various panels.
		/// </summary>
		private void HidePanels()
		{
		  ApplicationStatusPlaceHolder.Visible = Model.JobStatus != JobStatuses.Draft;

      OfficePlaceHolder.Visible = Model.JobStatus != JobStatuses.Draft && App.Settings.OfficesEnabled;
		}

		/// <summary>
		/// Handles the Click event of the EmailEmployeeLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmailEmployeeLinkButton_Click(object sender, EventArgs e)
		{
			EmailEmployee.Show(JobModel.EmployeeId.GetValueOrDefault());
		}

		/// <summary>
		/// Handles the Click event of the SaveButton in the AssignedOffices user control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveAssignedOffices(object sender, EventArgs e)
		{
			var jobOffices = new List<JobOfficeMapperDto>();
			var assignedOffices = AssignedOffices.AssignedOfficeList;

			// If there's nothing in the list box then everything is to be deleted 
			if (assignedOffices.Count > 0)
			{
				jobOffices = (from AssignedOfficeModel item in assignedOffices
											select new JobOfficeMapperDto
											{
												OfficeId = item.OfficeId.GetValueOrDefault(),
												JobId = _jobId
											}).ToList();
			}
			else
			{
				AssignedOffices.Invalidate(CodeLocalise("OfficeExistsValidator.Error", "An office must be assigned"));
				return;
			}

			ServiceClientLocator.JobClient(App).UpdateJobAssignedOffice(jobOffices, _jobId);

			if (AssignedOffices.DefaultOfficeRemoved)
			{
				var officeIds = (from l in assignedOffices
												 select l.OfficeId.GetValueOrDefault()).ToList();

				var locations = ServiceClientLocator.JobClient(App).GetJobLocations(JobModel.JobId).Select(x => x.Location).ToList();
				var jobLocationZips = new List<string>();

				// Get a list of the Zips the job is located at
				if (locations.IsNotNullOrEmpty() && locations.Count > 0)
				{
					const string pattern = @"(?<=\().+?(?=\))";
					jobLocationZips.AddRange(locations.Select(location => Regex.Match(location, pattern).Value));
				}

				ServiceClientLocator.EmployerClient(App).UpdateOfficeAssignedZips(jobLocationZips, officeIds);
			}

			Confirmation.Show(CodeLocalise("JobOfficesUpdatedConfirmation.Title", "Job offices updated"),
															 CodeLocalise("JobOfficesUpdatedConfirmation.Body", "The offices for the job have been updated."),
															 CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Determines whether [is assist employers administrator].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is assist employers administrator]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistEmployersAdministrator()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
		}

		/// <summary>
		/// Handles the CheckChanged event of the CriminalRecordExclusionCheckBox control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void CriminalRecordExclusionCheckBox_CheckChanged(object sender, EventArgs e)
		{
			ServiceClientLocator.JobClient(App).UpdateCriminalBackgroundExclusionRequired(JobModel.JobId, CriminalRecordExclusionCheckBox.Checked);
		}

		/// <summary>
		/// Handles the CheckChanged event of the CreditCheckRequiredCheckBox control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void CreditCheckRequiredCheckBox_CheckChanged(object sender, EventArgs e)
		{
			ServiceClientLocator.JobClient(App).UpdateCreditCheckRequired(JobModel.JobId, CreditCheckRequiredCheckBox.Checked);
		}

		/// <summary>
		/// Binds the sub action drop down to staff.
		/// </summary>
		private void BindSubActionDropDownToStaff()
		{
			SubActionDropDown.Items.Clear();

			SubActionDropDown.DataSource = ServiceClientLocator.AccountClient(App).GetAssistUsers();
			SubActionDropDown.DataValueField = "Id";
			SubActionDropDown.DataTextField = "Name";
			SubActionDropDown.DataBind();

			SubActionDropDown.Items.AddLocalisedTopDefault("StaffSubActionDropDown.TopDefault", "- select staff member name -");
		}

		/// <summary>
		/// Binds the action drop down.
		/// </summary>
		private void BindActionDropDown()
		{
			JobOrdersActionDropDown.Items.Clear();

			JobOrdersActionDropDown.Items.AddLocalisedTopDefault("ActionDropDown.TopDefault", "- select action -");

			if (IsAssistEmployersAdministrator())
			{
				JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AccessEmployeeAccount, "Access #BUSINESS#:LOWER account"),ActionTypes.AccessEmployeeAccount.ToString()));
				JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.EmailEmployee, "Email #BUSINESS#:LOWER"),ActionTypes.EmailEmployee.ToString()));
				JobOrdersActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.MatchJob, "Find #CANDIDATETYPE# for job"), ActionTypes.MatchJob.ToString()));

				JobOrdersActionDropDown.SelectedIndex = 0;
			}
			else
			{
				ActionLabelCell.Visible = JobOrdersActionDropDown.Visible = JobOrdersActionButton.Visible = false;
			}
		}

		/// <summary>
		/// Handles the Click event of the AccessEmployersAccountLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AccessEmployersAccountLinkButton_Click(object sender, EventArgs e)
		{
			var script = string.Format("$(document).ready(function (){{Business_TalentPopup = showPopup('{0}{1}','{2}');}});",
																 App.Settings.TalentApplicationPath, UrlBuilder.SingleSignOn(GetSingleSignOnKey(), false),
																 BlockedPopUpMessage);

			Page.ClientScript.RegisterStartupScript(GetType(), "AccessEmployee", script, true);
		}

		/// <summary>
		/// Handles the Click event of the ApplicantsViewOfJobButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ApplicantsViewOfJobButton_Click(object sender, EventArgs e)
		{

		}

		/// <summary>
		/// Handles the Click event of the ViewJobAsEmployerLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ViewJobAsEmployerLinkButton_Click(object sender, EventArgs e)
		{
			var script = string.Format("$(document).ready(function (){{Business_TalentPopup = showPopup('{0}{1}','{2}');}});",
																 App.Settings.TalentApplicationPath,
																 UrlBuilder.ViewJobAsEmployee(GetSingleSignOnKey(), JobModel.JobId, false), BlockedPopUpMessage);
			Page.ClientScript.RegisterStartupScript(GetType(), "AccessJobEmployee", script, true);
		}

		/// <summary>
		/// Handles the Click event of the PreviewJobOrderLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PreviewJobOrderLinkButton_Click(object sender, EventArgs e)
		{
			var job = ServiceClientLocator.JobClient(App).GetJob(JobModel.JobId);
			Handle(PageController.PreviewPosting(job, showPrint: true));
		}

		/// <summary>
		/// Handles the Click event of the CloseJobLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseJobLinkButton_Click(object sender, EventArgs e)
		{
			JobAction.Show(ActionTypes.CloseJob, _jobId, JobModel.JobTitle, JobModel.JobType.GetValueOrDefault(JobTypes.Job));
		}

		/// <summary>
		/// Handles the Click event of the HoldJobLinkButton button to put an active job on hold.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void HoldJobLinkButton_Click(object sender, EventArgs e)
		{
			Confirmation.Show(CodeLocalise("HoldConfirmation.Title", "Hold Posting"),
												CodeLocalise("HoldConfirmation.Body", "Are you sure you want to hold {0}?", JobModel.JobTitle),
												CodeLocalise("Global.Cancel.Text", "Cancel"),
												okText: CodeLocalise("HoldConfirmation.Ok.Text", "Hold posting"), okCommandName: "Hold", okCommandArgument: _jobId.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Handles the Click event of the ReactivateJobLinkButton button to put an re-activate a job on hold.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ReactivateJobLinkButton_Click(object sender, EventArgs e)
		{
			JobAction.Show(ActionTypes.ReactivateJob, _jobId, JobModel.JobTitle, JobModel.JobType.GetValueOrDefault(JobTypes.Job));
		}

		/// <summary>
		/// Handles the Click event of the RefreshJobLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void RefreshJobLinkButton_Click(object sender, EventArgs e)
		{
			JobAction.Show(ActionTypes.RefreshJob, _jobId, JobModel.JobTitle, JobModel.JobType.GetValueOrDefault(JobTypes.Job), numberOfOpenings: Model.NumberOfOpenings);
		}

		/// <summary>
		/// Handles the Click event of the DuplicateJobLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DuplicateJobLinkButton_Click(object sender, EventArgs e)
		{
			Handle(PageController.QueryDuplicateJob(_jobId, JobModel.JobTitle));
		}

		//
		/// <summary>
		/// Handles the Click event of the DeleteJobLinkButton control to show a confirmation prior to deleting the job
		/// </summary>
		/// <param name="sender">The button raising of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DeleteJobLinkButton_Click(object sender, EventArgs e)
		{
			Confirmation.Show(CodeLocalise("DeleteConfirmation.Title", "Delete Job"),
												CodeLocalise("DeleteConfirmation.Body", "Are you sure you want to delete the job {0}?", JobModel.JobTitle),
												CodeLocalise("Global.Cancel.Text", "Cancel"),
												okText: CodeLocalise("DeleteConfirmation.Ok.Text", "Delete Job"),
												height: 75,
												okCommandName: "Delete",
												okCommandArgument: _jobId.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Handles the Click event of the ViewMatchesButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ViewMatchesButton_Click(object sender, EventArgs e)
		{
			Response.Redirect(UrlBuilder.AssistPool(TalentPoolListTypes.MatchesForThisJob, JobModel.JobId));
		}

		/// <summary>
		/// Handles the Click event of the PrintButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PrintButton_Click(object sender, EventArgs e)
		{
			Page.ClientScript.RegisterStartupScript(GetType(), "outputwindow",
																							"window.open('" + UrlBuilder.AssistPrintJob(JobModel.JobId) +
																							"', '','status=0, toolbar=0, menubar=0, height=600, width=600, location=0, resizable=1, scrollbars=1');",
																							true);
		}

		/// <summary>
		/// Gets the single sign on key.
		/// </summary>
		/// <returns></returns>
		private Guid GetSingleSignOnKey()
		{
			return ServiceClientLocator.AuthenticationClient(App).CreateEmployeeSingleSignOn(JobModel.EmployeeId.GetValueOrDefault(), null);
		}

		/// <summary>
		/// Handles the Clicked event of the ActionButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobOrdersActionButton_Clicked(object sender, EventArgs e)
		{
			// Hide the sub action controls
			SubActionDropDown.Visible = SubActionButton.Visible = false;

			if (JobModel.EmployeeId.HasValue)
			{
				SelectedAction = JobOrdersActionDropDown.SelectedValueToEnum<ActionTypes>();

				switch (SelectedAction)
				{
					case ActionTypes.AccessEmployeeAccount:

						var singleSignOnKey = ServiceClientLocator.AuthenticationClient(App).CreateEmployeeSingleSignOn(JobModel.EmployeeId.GetValueOrDefault(), null);

						var script = string.Format("$(document).ready(function (){{Business_TalentPopup = showPopup('{0}{1}','{2}');}});",
																			 App.Settings.TalentApplicationPath, UrlBuilder.SingleSignOn(singleSignOnKey, false),
																			 BlockedPopUpMessage);

						Page.ClientScript.RegisterStartupScript(GetType(), "AccessEmployee", script, true);

						break;

					case ActionTypes.EmailEmployee:
						EmailEmployee.Show(JobModel.EmployeeId.GetValueOrDefault());
						break;

					case ActionTypes.AssignEmployerToStaff:
						PrepareAssignEmployer();
						break;

					case ActionTypes.MatchJob:
						Response.Redirect(UrlBuilder.AssistPool(TalentPoolListTypes.MatchesForThisJob, _jobId));
						break;
				}
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SubActionButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SubActionButton_Clicked(object sender, EventArgs e)
		{
			SelectedAction = JobOrdersActionDropDown.SelectedValueToEnum<ActionTypes>();

			if (SubActionDropDown.SelectedIndex == 0)
			{
				#region sub action error

				SubActionButtonValidator.IsValid = false;

				if (SelectedAction == ActionTypes.AssignEmployerToStaff)
					SubActionButtonValidator.ErrorMessage = HtmlLocalise("SubActionStatus.Error.Text",
																															 "Please select a staff member");

				#endregion

				return;
			}

			switch (SelectedAction)
			{
				case ActionTypes.AssignEmployerToStaff:

					#region assign to staff

					var adminId = SubActionDropDown.SelectedValueToLong();

					ServiceClientLocator.EmployerClient(App).AssignEmployerToAdmin(adminId, JobModel.EmployeeId ?? 0);

					Confirmation.Show(CodeLocalise("AssignEmployerConfirmation.Title", "#BUSINESS# assigned to staff"),
														CodeLocalise("AssignEmployerConfirmation.Body",
																				 "#BUSINESS# assigned to the staff member."),
														CodeLocalise("CloseModal.Text", "OK"));

					#endregion

					break;
			}

			SubActionDropDown.Visible = false;
			SubActionButton.Visible = false;
			JobOrdersActionDropDown.SelectedIndex = 0;
		}

		/// <summary>
		/// Prepares the assign employer to staff action.
		/// </summary>
		private void PrepareAssignEmployer()
		{
			BindSubActionDropDownToStaff();
			SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignToStaffSubActionDropDownRequired.ErrorMessage",
																														"Staff member required");
			SubActionButton.Text = CodeLocalise("JobOrdersSubActionButton.Text", "Assign");
			SubActionDropDown.Visible = SubActionButton.Visible = true;
		}

		/// <summary>
		/// Binds the sub action drop down to job status.
		/// </summary>
		private void BindSubActionDropDownToJobStatus()
		{
			#region Bind sub action to job status

			SubActionDropDown.Items.Clear();
			if (JobModel.JobStatus != JobStatuses.Active)
				SubActionDropDown.Items.AddEnum(JobStatuses.Active);
			SubActionDropDown.Items.AddEnum(JobStatuses.Closed);
			if (JobModel.JobStatus != JobStatuses.OnHold)
				SubActionDropDown.Items.AddEnum(JobStatuses.OnHold);
			SubActionDropDown.Items.AddLocalisedTopDefault("StatusSubActionDropDown.TopDefault", "- select status -");

			#endregion
		}

		/// <summary>
		/// Prepares the job statuses.
		/// </summary>
		private void PrepareJobStatuses()
		{
			BindSubActionDropDownToJobStatus();
			SubActionDropDownRequired.ErrorMessage = CodeLocalise("ChangeJobStatusSubActionDropDownRequired.ErrorMessage",
																														"Status is required");
			SubActionButton.Text = CodeLocalise("JobOrdersSubActionButton.Text", "Go");
			SubActionDropDown.Visible = SubActionButton.Visible = true;
		}

		/*
		/// <summary>
		/// Binds the sub action drop down to activities.
		/// </summary>
		private void BindSubActionDropDownToActivities()
		{
			SubActionDropDown.Visible = true;
			SubActionDropDown.Items.Clear();

			SubActionDropDown.DataSource = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmployeeActivities);
			SubActionDropDown.DataValueField = "Id";
			SubActionDropDown.DataTextField = "Text";
			SubActionDropDown.DataBind();

			SubActionDropDown.Items.AddLocalisedTopDefault("ActivitiesSubActionDropDown.TopDefault", "- select activity -");
		}
		*/

		/// <summary>
		/// Handles the Completed event of the JobAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobAction_Completed(object sender, EventArgs e)
		{
			Response.Redirect(UrlBuilder.AssistJobView(_jobId));
		}

		/// <summary>
		/// Handles the OkCommand event of the Confirmation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_OkCommand(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Hold":
					// Do the Holding
					ServiceClientLocator.JobClient(App).HoldJob(_jobId);
					JobModel.JobStatus = JobStatuses.OnHold;
          Response.Redirect(UrlBuilder.AssistJobView(_jobId));
					break;

				case "Delete":
					ServiceClientLocator.JobClient(App).DeleteJob(_jobId);
					Response.Redirect(UrlBuilder.JobOrderDashboard());
					break;
			}
		}

		/// <summary>
		/// Times the since.
		/// </summary>
		/// <param name="actionTime">The action time.</param>
		/// <returns></returns>
		private string TimeSince(DateTime? actionTime)
		{
			if (actionTime.GetValueOrDefault() == DateTime.MinValue) return String.Empty;

			var currentDateTime = DateTime.Now;
			var timeSince = currentDateTime.Subtract(actionTime.GetValueOrDefault());
			return timeSince.Days + " " + CodeLocalise("Days", "days") + " " + timeSince.Hours + " " +
						 CodeLocalise("Hours", "hours") + " " + timeSince.Minutes + " " + CodeLocalise("Minutes", "minutes");
		}

		/// <summary>
		/// Times the until.
		/// </summary>
		/// <param name="actionTime">The action time.</param>
		/// <returns></returns>
		private string TimeUntil(DateTime? actionTime)
		{
			if (actionTime.GetValueOrDefault() == DateTime.MinValue) return String.Empty;

			var currentDateTime = DateTime.Now;
			var timeUntil = actionTime.GetValueOrDefault().Subtract(currentDateTime);
			return timeUntil.Days + " " + CodeLocalise("Days", "days") + " " + timeUntil.Hours + " " +
						 CodeLocalise("Hours", "hours") + " " + timeUntil.Minutes + " " + CodeLocalise("Minutes", "minutes");
		}
	}
}
