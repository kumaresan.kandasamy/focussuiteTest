﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.WebAssist
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
  public partial class EmailAlerts : PageBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      Page.Title = CodeLocalise("Page.Title", "Email Alerts");

      if (!IsPostBack)
      {
        LocaliseControls();
        InitialiseControlValues();
      }
    }

    /// <summary>
    /// Handles the Click event of the Save Button control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SetAlertsButton_Click(object sender, EventArgs e)
    {
      var userAlert = GetAlertTable();

      userAlert.UserId = App.User.UserId;

      userAlert.Frequency = AlertFrequencyDropDown.SelectedValue.ToCharArray()[0];
      userAlert.NewReferralRequests = NewReferralRequestsBox.Checked;
      userAlert.EmployerAccountRequests = EmployerAccountRequestsBox.Checked;
      userAlert.NewJobPostings = NewJobPostingsBox.Checked;

      ServiceClientLocator.AccountClient(App).UpdateUserAlert(userAlert);

      ConfirmationModal.Show(CodeLocalise("SuccessfulUpdate.Title", "Email alert details changed"),
                               CodeLocalise("SuccessfulUpdate.Body", "You have successfully changed your email alert details."),
                               CodeLocalise("CloseModal.Text", "Return to account settings"));
    }

    /// <summary>
    /// Localises the Controls on the page.
    /// </summary>
    private void LocaliseControls()
    {
      SetAlertsButton.Text = CodeLocalise("Global.Save.Label", "Save");
      NewReferralRequestsBox.Text = CodeLocalise("NewReferralRequests.Text", "New Referral Requests");
			EmployerAccountRequestsBox.Text = CodeLocalise("EmployerAccountRequests.Text", "#BUSINESS# Account Requests");
      NewJobPostingsBox.Text = CodeLocalise("NewJobPostings.Text", "New Job Postings");

      AlertFrequencyDropDown.Items.Add(new ListItem("Daily", "D"));
      AlertFrequencyDropDown.Items.Add(new ListItem("Weekly", "W"));
    }

    /// <summary>
    /// Initialises the control values.
    /// </summary>
    private void InitialiseControlValues()
    {
      var userAlert = GetAlertTable();

       NewReferralRequestsBox.Checked = userAlert.NewReferralRequests;
       EmployerAccountRequestsBox.Checked = userAlert.EmployerAccountRequests;
       NewJobPostingsBox.Checked = userAlert.NewJobPostings;

      AlertFrequencyDropDown.SelectedValue = userAlert.Frequency.ToString();
    }

    /// <summary>
    /// Gets the user alert record from the database
    /// </summary>
    /// <returns>The User Alert</returns>
    private UserAlertDto GetAlertTable()
    {
      return ServiceClientLocator.AccountClient(App).GetUserAlert(App.User.UserId);
    }
  }
}