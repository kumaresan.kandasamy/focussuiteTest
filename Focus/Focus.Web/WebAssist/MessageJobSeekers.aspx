﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MessageJobSeekers.aspx.cs" Inherits="Focus.Web.WebAssist.MessageJobSeekers" MaintainScrollPositionOnPostback="true"%>
<%@ Import Namespace="Focus.Core.Views" %>

<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/JobSeekerListModal.ascx" tagname="JobSeekerListModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistJobSeekersNavigation.ascx" tagname="AssistJobSeekersNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/JobLookupModal.ascx" tagname="JobLookupModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/SendMessageModal.ascx" tagname="MessageModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/SendMessages.ascx" tagname="SendMessage" tagprefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ActivityAssignment.ascx" TagName="ActivityAssignment" TagPrefix="uc" %>

<%@ Register Src="~/WebReporting/Controls/CriteriaJobSeekerStatus.ascx" TagName="CriteriaJobSeekerStatus" TagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaVeteranStatus.ascx" tagName="CriteriaVeteranStatus" tagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaJobSeekerCharacteristics.ascx" tagName="CriteriaJobSeekerCharacteristics" tagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaJobSeekerQualifications.ascx" tagName="CriteriaJobSeekerQualifications" tagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaOccupationAndSalary.ascx" tagName="CriteriaOccupationAndSalary" tagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaKeywordAndContext.ascx" tagName="CriteriaKeywordAndContext" tagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaIndividualJobSeekerCharacteristics.ascx" tagName="CriteriaIndividualJobSeekerCharacteristics" tagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaResume.ascx" tagName="CriteriaResume" tagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaJobSeekerActivitySummary.ascx" tagName="CriteriaJobSeekerActivitySummary" tagPrefix="uc" %>
<%@ Register src="~/WebReporting/Controls/CriteriaJobSeekerActivityLog.ascx" tagName="CriteriaJobSeekerActivityLog" tagPrefix="uc"%>
<%@ Register src="~/WebReporting/Controls/CriteriaReferralsOutcome.ascx" tagName="CriteriaReferralsOutcome" tagPrefix="uc"%>
<%@ Register src="~/WebReporting/Controls/CriteriaLocation.ascx" tagName="CriteriaLocation" tagPrefix="uc" %>
<%@ Register Src="~/WebReporting/Controls/CriteriaDateRange.ascx" TagName="CriteriaDateRange" TagPrefix="uc" %>

<%@ Register src="~/WebReporting/Controls/ReportCriteriaDisplay.ascx" tagName="ReportCriteriaDisplay" tagPrefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%# ResolveUrl("~/Assets/Scripts/Focus.Reporting.js") %>" type="text/javascript"></script>
		<script src="<%# ResolveUrl("~/Assets/Scripts/UpdateableList.js") %>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

	<uc:AssistJobSeekersNavigation ID="AssistJobSeekersNavigationMain" runat="server" />

	<div class="grid100">
		<div class="reportgrid80">
			<h1><focus:LocalisedLabel runat="server" ID="SendMessagesTitleLabel" DefaultText="Send messages" LocalisationKey="PageTitle.Text"/></h1>
		</div>
    <div class="reportgrid17" style="text-align: right">
      <asp:Button ID="FindButton" runat="server" Text="Find" class="button3" OnClick="FindButton_Clicked" ValidationGroup="SendMessagesGroup" CausesValidation="True"/>
    </div>
	</div>
  
  <asp:Panel runat="server" ID="BasicSelectionPanel" Visible="False">
    <div style="float:left">
    	<table role="presentation">
				<tr>
					<td style="width:150px; white-space:nowrap;"><%= HtmlLabel("JobSeekerFirstName.Label", "First name") %></td>
					<td><asp:TextBox runat="server" ID="FirstNameTextBox" Width="200" /></td>					
				</tr>
				<tr>
					<td style="white-space:nowrap;"><%= HtmlLabel("JobSeekerLastName.Label", "Last name") %></td>
					<td><asp:TextBox runat="server" ID="LastNameTextBox" Width="200" /></td>
				</tr>
        <tr>
					<td style="white-space:nowrap;"><%= HtmlLabel("JobSeekerEmailAddress.Label", "Email address")%></td>
					<td><asp:TextBox runat="server" ID="EmailAddressTextBox" Width="200" /></td>
				</tr>
				<tr>
					<td style="white-space:nowrap;"><%= HtmlLabel("JobSeekerPhoneNumber.Label", "Phone number")%></td>
					<td><asp:TextBox runat="server" ID="PhoneNumberTextBox" Width="200" /></td>
				</tr>
			</table>
    </div>
    <div style="float: right">
      <asp:Button ID="BasicFindButton" runat="server" Text="Find" class="button3" OnClick="FindButton_Clicked" />
    </div>
    <div style="clear: both"></div>
  </asp:Panel>
  
  <asp:Panel runat="server" ID="CriteriaSelectionPanel">
     <div class="grid100">
        <div class="reportgrid33">
            <div class="reportgrid95">
                <p><asp:Literal runat="server" ID="Column1Header" /></p>
                <uc:CriteriaJobSeekerStatus ID="CriteriaJobSeekerStatus" runat="server" />
                <uc:CriteriaVeteranStatus ID="CriteriaVeteranStatus" runat="server" />
                <uc:CriteriaJobSeekerCharacteristics ID="CriteriaJobSeekerCharacteristics" runat="server" />
                <uc:CriteriaJobSeekerQualifications ID="CriteriaJobSeekerQualifications" runat="server" />
                <uc:CriteriaOccupationAndSalary ID="CriteriaOccupationAndSalary" runat="server" />
                <uc:CriteriaKeywordAndContext ID="CriteriaKeywordAndContext" runat="server" />
                <uc:CriteriaResume runat="server" ID="CriteriaResume" Visible="False" />
								<uc:CriteriaIndividualJobSeekerCharacteristics runat="server" ID="CriteriaIndividualJobSeekerCharacteristics"/>
            </div>
        </div>
        <div class="reportgrid33">
            <div class="reportgrid95">
                <p><asp:Literal runat="server" ID="Column2Header" /></p>
                <uc:CriteriaJobSeekerActivitySummary runat="server" ID="CriteriaJobSeekerActivitySummary" ShowJobSeekerAccountsCritria="False" />
                <uc:CriteriaJobSeekerActivityLog runat="server" ID="CriteriaJobSeekerActivityLog" ShowActivitiesAndServices="True" />
                <uc:CriteriaReferralsOutcome runat="server" ID="CriteriaReferralsOutcome" />
            </div>
        </div>
        <div class="reportgrid33">
            <div class="reportgrid95">
                <p><asp:Literal runat="server" ID="Column3Header" /></p>
                <uc:CriteriaLocation runat="server" ID="CriteriaLocation" ShowCounty="True"/>
                <uc:CriteriaDateRange runat="server" ID="CriteriaDateRange" />
            </div>
        </div>
    </div>
  </asp:Panel>
  
  <uc:ReportCriteriaDisplay runat="server" ID="CriteriaDisplay" Visible="False" ShowButtons="True" OnActionRequested="ReportCriteriaDisplay_ActionRequested" />
  
		
			<table role="presentation" style="width:100%;">
				<tr>
					<td><h3><%= HtmlLocalise("JobSeekers.Title", "#CANDIDATETYPE#")%></h3></td>
					<td style="text-align:right;">
					  <div id="JobSeekerButtonsSection" class="opacity">
				    <div id="JobSeekerButtonsOverlay" class="overlay"></div>
							<asp:PlaceHolder runat="server" ID="ComposeMessageButtonPlaceHolder"><asp:Button ID="ComposeMessageButton" runat="server" CssClass="button1" UseSubmitBehavior="False" OnClientClick="MessageJobSeekers_GoToComposeMessage();return false;" />&nbsp;&nbsp;</asp:PlaceHolder>
					    <asp:Button ID="AddJobSeekersButton" runat="server" CssClass="button2" OnClick="AddJobSeekersButton_Clicked" />
							<asp:PlaceHolder runat="server" ID="LookupJobButtonPlaceHolder">&nbsp;&nbsp;<asp:Button ID="LookupJobButton" runat="server" CssClass="button2" OnClick="LookupJobButton_Clicked" /> </asp:PlaceHolder>
            </div>
					</td>
				</tr>
			</table>
			
			<i><asp:literal ID="PageInstructions" runat="server" /></i><br />
			<br />
			<div>
				<focus:LocalisedLabel runat="server" ID="ListsDropDownListLabel" AssociatedControlID="ListsDropDownList" LocalisationKey="Messages" DefaultText="Messages" CssClass="sr-only"/>
				<asp:DropDownList ID="ListsDropDownList" runat="server" Width="350px" />&nbsp;&nbsp;
				<asp:Button ID="SelectListButton" runat="server" CssClass="button3" OnClick="SelectListButton_Clicked" ValidationGroup="ListsDropDownList" />
				<asp:Button ID="DeleteJobSeekersFromListButton" runat="server" CssClass="button3" OnClick="DeleteJobSeekersFromListButton_Clicked" ValidationGroup="DeleteJobSeekersFromList" />
				<asp:Button ID="DeleteListButton" runat="server" CssClass="button3" OnClick="DeleteListButton_Clicked" ValidationGroup="ListsDropDownList" />
			</div>
			<asp:RequiredFieldValidator ID="ListsDropDownRequiredValidator" runat="server" ControlToValidate="ListsDropDownList" ValidationGroup="ListsDropDownList" CssClass="error block" />
			<asp:PlaceHolder runat="server" ID="JobSeekerSelectedPlaceHolder">
				<asp:CustomValidator ID="ListMembersListValidator" runat="server" ClientValidationFunction="validateJobSeekersList" ValidationGroup="DeleteJobSeekersFromList" ValidateEmptyText="True" CssClass="error"/>
			</asp:PlaceHolder>
			
			<div>
				<span runat="server" ID="ActionDiv" Visible="False">
					<div id="ActionDropDownSection" class="opacity" >
						<div id="ActionDropDownOverlay" class="overlay"></div>
						<span><focus:LocalisedLabel ID="ActionLabel" runat="server" DefaultText="Action" LocalisationKey="ActionLabel.Text" Width="70px"/></span>
						<span>
							<asp:DropDownList runat="server" ID="ActionDropDown" Title="Action"/>
							<asp:Button runat="server" ID="ActionGoButton" CssClass="button3" OnClick="ActionGoButton_Clicked" CausesValidation="True" ValidationGroup="ActionGroup"/>
						</span>
					</div>
				</span>
			</div>
			<div id="ActivitySection" class="opacity" >
				<div id="ActivityOverlay" class="overlay"></div>
				<div style="padding-left: 74px; margin-top: 5px;" runat="server" ID="ActivityDiv" Visible="False">
					<uc:ActivityAssignment ID="ActivityAssigner" runat="server" ActivityType="JobSeeker" OnActivitySelected="ActivityAssignment_ActivitySelected" />
				</div>
			</div>
			<asp:RequiredFieldValidator runat="server" ID="ActionRequired" ControlToValidate="ActionDropDown" CssClass="error block" ValidationGroup="ActionGroup"/>
			<div id="SelectedCountDiv" runat="server" Visible="False" class="floatLeft">
				<%=HtmlLocalise("ItemsSelected.Header", "#CANDIDATETYPE#(s) selected: ") %>
				<span id="MessageJobSeekers_SelectedCountLabel" runat="server" clientidmode="Static">0</span>
			</div>
			<div class="floatRight" runat="server" ID="PagerDiv" Visible="False">
				<uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="JobSeekersList" PageSize="10" DisplayRecordCount="True" />
			</div>					
			<asp:ListView ID="JobSeekersList" runat="server" OnDataBound="JobSeekersList_OnDataBound" OnItemDataBound="JobSeekersList_OnItemDataBound" ItemPlaceholderID="JobSeekerPlaceHolder" DataSourceID="SearchResultDataSource" DataKeyNames="PersonId,FirstName,LastName,EmailAddress" Visible="False">
				<LayoutTemplate>
					<table role="presentation" style="width:100%;">
						<thead class="tableHeader">
				        <tr>
					        <td style="width:20px;"><asp:Checkbox ID="MessageJobSeekers_HeaderSelectorCheckBox" runat="server" ClientIDMode="Static" /></td>
					        <td style="width:220px;"><focus:LocalisedLabel id="NameHeaderLiteral" runat="server" DefaultText="Name" LocalisationKey="NameHeaderLiteral.Text"></focus:LocalisedLabel></td>
					        <td style="width:380px;" id="OfficeHeaderCell" runat="server"><focus:LocalisedLabel id="OfficeHeaderLiteral" runat="server" DefaultText="Office" LocalisationKey="OfficeHeaderLiteral.Text"></focus:LocalisedLabel></td>
					        <td><focus:LocalisedLabel id="EmailHeaderLiteral" runat="server" DefaultText="Email address" LocalisationKey="EmailHeaderLiteral.Text"></focus:LocalisedLabel></td>
				        </tr>
						</thead>
            <tbody class="tableBody">
						  <asp:PlaceHolder ID="JobSeekerPlaceHolder" runat="server" />
            </tbody>
					</table>
				</LayoutTemplate>
				<ItemTemplate>
					<tr>
						<td style="width:20px;"><asp:Checkbox ID="SelectorCheckBox" runat="server" ClientIDMode="Static" CssClass="SelectorCheckBox"/></td>
						<td style="width:220px;"><%# ((JobSeekerView)Container.DataItem).LastName%>, <%# ((JobSeekerView)Container.DataItem).FirstName%></td>
						<td style="width:380px;" runat="server" id="OfficeCell">
						  <asp:Literal id="OfficeLiteral" runat="server"></asp:Literal>
						</td>
						<td><%# ((JobSeekerView)Container.DataItem).EmailAddress%></td>
					</tr>	
				</ItemTemplate>
				<EmptyDataTemplate>
						<tr>
							<td style="text-align:left;" colspan="4">
								<br />
								<%= HtmlLocalise("JobSeekersList.NoResult", "No matches")%></td>
						</tr>
				</EmptyDataTemplate>
			</asp:ListView>
		  <asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.WebAssist.MessageJobSeekers"
          EnablePaging="true" SelectMethod="GetJobSeekers" SelectCountMethod="GetJobSeekersCount"
          SortParameterName="orderBy" OnSelecting="SearchResultDataSource_OnSelecting"></asp:ObjectDataSource>

			<asp:CustomValidator ID="JobSeekersListValidator" runat="server" ClientValidationFunction="validateJobSeekersList" ValidationGroup="MessageJobSeekers" ValidateEmptyText="True" CssClass="error"/>
			<br/>
      <asp:PlaceHolder runat="server" ID="SeparatorPlaceHolder"><hr/></asp:PlaceHolder>
      <br/>
      <asp:HiddenField runat="server" ID="SelectedJobSeekerIds"/>
			<asp:PlaceHolder runat="server" ID="ComposeMessagePlaceHolder">
				<div id="ComposeMessageSection" class="opacity" >
					<div id="ComposeMessageOverlay" class="overlay"></div>
					<h3><%= HtmlLocalise("ComposeMessage.Title", "Compose message")%></h3>
					<table role="presentation">
						<tr>
							<td><%=HtmlLabel("MessageFormat.Label", "Message format") %></td>
							<td><asp:RadioButtonList ID="MessageFormatRadioButtonList" RepeatDirection="Horizontal" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="MessageFormatRadioButtonList_SelectedIndexChanged" AutoPostBack="true" role="presentation"  CellSpacing="15"/></td>
						</tr>
					</table>
					<br />
					<uc:SendMessage runat="server" ID="SendMessage"/>
				</div>	
			</asp:PlaceHolder>	

	<%-- Modals --%>
	<uc:JobSeekerListModal ID="JobSeekerList" runat="server" OnOkCommand="JobSeekerList_OkCommand" />
	<uc:JobLookupModal ID="JobLookup" runat="server" OnOkCommand="JobLookup_OkCommand" />
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
	<uc:ConfirmationModal ID="ActionConfirmation" runat="server" OnOkCommand="ActionConfirmationModal_OkCommand" />
	<uc:ConfirmationModal ID="Confirmation" runat="server"/>
	<uc:MessageModal ID="MessageModal" runat="server"/>

	<script type="text/javascript">
	  $(document).ready(function() {
	    $('td').delegate('.deleteItem', 'click', function(e) {
	      RemoveListItem($(this));
	      e.stopPropagation();
	    });
	  });

	  var MessageJobSeekers_allSelectorCheckBoxes = $('.SelectorCheckBox > input:checkbox');
	  var MessageJobSeekers_selectedJobSeekersField = $("#<%=SelectedJobSeekerIds.ClientID %>");
	  var MessageJobSeekers_selectedJobSeekersArray = new Array();
	  
	  function pageLoad()
	  {
	    MessageJobSeekers_allSelectorCheckBoxes.click(function () { MessageJobSeekers_checkBoxHandler($(this)); });
	    $("#MessageJobSeekers_HeaderSelectorCheckBox").click(function () { MessageJobSeekers_HeaderCheckbox($(this)); });
	    $(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();

	    if (MessageJobSeekers_selectedJobSeekersField.val().length > 0) {
	      MessageJobSeekers_selectedJobSeekersArray = MessageJobSeekers_selectedJobSeekersField.val().split(",");
	      MessageJobSeekers_allSelectorCheckBoxes.each(function () {
	        var seekerId = $(this).parent().attr("data-value");
	        if ($.inArray(seekerId, MessageJobSeekers_selectedJobSeekersArray) >= 0) {
	          $(this).prop("checked", true);
	        }
	      });
	    }

	       MessageJobSeekers_checkBoxHandler();

		  $('#<%=ActionDropDown.ClientID %>').on('change', function() {
				if ($(this).val() !== '<%=ActionTypes.AssignActivityToCandidate %>') {
					$('#<%= ActivityDiv.ClientID %>').hide();
				} 
		  });
	  }

	  function MessageJobSeekers_HeaderCheckbox(oSrc) {
	    if (oSrc.is(":checked")) {
	      MessageJobSeekers_allSelectorCheckBoxes.each(function () {
	        var oChk = $(this);
	        oChk.prop("checked", true);
	        MessageJobSeekers_AddToSelected(oChk.parent().attr("data-value"));
	      });
	    } else {
	      MessageJobSeekers_allSelectorCheckBoxes.each(function () {
	        var oChk = $(this);
	        oChk.prop("checked", false);
	        MessageJobSeekers_RemoveFromSelected(oChk.parent().attr("data-value"));
	      });
	    }

	    StoreJobSeekerIds();

	    MessageJobSeekers_ProcessSelected();
	  }

	  function validateJobSeekersList(oSrc, args)
	  {
	    args.IsValid = (MessageJobSeekers_selectedJobSeekersArray.length > 0);
	  }

	  function MessageJobSeekers_checkBoxHandler(oChk) {
	    if (oChk != null) {
	      var val = oChk.parent().attr("data-value");
	      if (oChk.is(":checked")) {
	        MessageJobSeekers_AddToSelected(val);
	      } else {
	        MessageJobSeekers_RemoveFromSelected(val);
				}
			}

	    var selectedPageCount = 0;
	    MessageJobSeekers_allSelectorCheckBoxes.each(function() {
	      if ($(this).is(":checked"))
	        selectedPageCount++;
	    });
	     $("#MessageJobSeekers_HeaderSelectorCheckBox").prop("checked", selectedPageCount == MessageJobSeekers_allSelectorCheckBoxes.length);

		  StoreJobSeekerIds();
	     
	     MessageJobSeekers_ProcessSelected();
	   }

		function StoreJobSeekerIds() {
			// Set the Jobseeker Ids to a session variable so they can be accessed by the send message control when used on the page (EDU) rather than the modal (WF)
			var jsonIds = {
				ids: MessageJobSeekers_selectedJobSeekersField.val().split(",")
			};
			$.ajax({
				type: "POST",
				url: '<%= UrlBuilder.AjaxService() %>/SetJobSeekerIds',
				data: JSON.stringify(jsonIds),
				contentType: "application/json; charset=utf-8",
				dataType: "json"
			});
		}

	 function MessageJobSeekers_ProcessSelected() {
	  $("#MessageJobSeekers_SelectedCountLabel").html(MessageJobSeekers_selectedJobSeekersArray.length);

	  if (MessageJobSeekers_selectedJobSeekersArray.length > 0)
	  	enableButtons();
	  else
	  	disableButtons();
	  }

	  function MessageJobSeekers_AddToSelected(value) {
	    if ($.inArray(value, MessageJobSeekers_selectedJobSeekersArray) < 0) {
	      MessageJobSeekers_selectedJobSeekersArray.push(value);
	      MessageJobSeekers_selectedJobSeekersField.val(MessageJobSeekers_selectedJobSeekersArray.join(","));
	    }
	  }

	  function MessageJobSeekers_RemoveFromSelected(value) {
	    var index = $.inArray(value, MessageJobSeekers_selectedJobSeekersArray);
	    if (index >= 0) {
	      MessageJobSeekers_selectedJobSeekersArray.splice(index, 1);
	      MessageJobSeekers_selectedJobSeekersField.val(MessageJobSeekers_selectedJobSeekersArray.join(","));
	    }
	  }

	  function enableButtons() {
	  	$('#ComposeMessageSection').removeClass('opacity');
	  	$('#ComposeMessageOverlay').removeClass('overlay');

	  	$('#JobSeekerButtonsSection').removeClass('opacity');
	  	$('#JobSeekerButtonsOverlay').removeClass('overlay');

	  	$('#ActionDropDownSection').removeClass('opacity');
	  	$('#ActionDropDownOverlay').removeClass('overlay');

	  	$('#ActivitySection').removeClass('opacity');
	  	$('#ActivityOverlay').removeClass('overlay');
	  }

	  function disableButtons() {
	  	$('#ComposeMessageSection').addClass('opacity');
	  	$('#ComposeMessageOverlay').addClass('overlay');

	  	$('#JobSeekerButtonsSection').addClass('opacity');
	  	$('#JobSeekerButtonsOverlay').addClass('overlay');

	  	$('#ActionDropDownSection').addClass('opacity');
	  	$('#ActionDropDownOverlay').addClass('overlay');

	  	$('#ActivitySection').addClass('opacity');
	  	$('#ActivityOverlay').addClass('overlay');
	  }

	  function MessageJobSeekers_GoToComposeMessage() {
	  	$("html, body").scrollTop($(document).height());
	  }
	</script>

</asp:Content>
