﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Focus.Web.WebAssist.Dashboard" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/MessagesDisplay.ascx" tagname="MessagesDisplay" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/DashboardReferralsReport.ascx" tagname="DashboardReferralsReport" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/DashboardDailyStatistics.ascx" tagname="DashboardDailyStatistics" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/WelcomeToAssistModal.ascx" tagname="WelcomeToAssistModal" tagprefix="uc" %>

<%@ Register src="~/Code/Controls/User/Reporting/SavedReports.ascx" tagname="DashboardReports" tagprefix="uc1" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">

	<uc:MessagesDisplay ID="Messages" runat="server" Module="Assist" />

	<uc:DashboardReferralsReport ID="ReferralsReport" runat="server" />
	
	<uc:DashboardDailyStatistics ID="DailyStatistics" runat="server" />

	<uc1:DashboardReports ID="DashboardReports" runat="server" DashboardReports="true" AllowDelete="False" ShowShortHeader="True" />

	<%-- Modals --%>
	<uc:WelcomeToAssistModal ID="WelcomeToAssist" runat="server" />	

</asp:Content>