﻿  <%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" maintainScrollPositionOnPostBack="true" CodeBehind="Pool.aspx.cs" Inherits="Focus.Web.WebAssist.Pool" %>
<%@ Import Namespace="Focus.Core.Views" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistEmployersNavigation.ascx" tagname="AssistEmployersNavigation" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register Src="~/Code/Controls/User/EmailJobSeekerModal.ascx" TagName="EmailCandidateModal" TagPrefix="uc" %>
<%@ Register src="~/WebAssist/Controls/NotesAndRemindersModal.ascx" tagname="NotesAndRemindersModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ResumePreviewModal.ascx" tagname="ResumePreviewModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/CandidateSearchCriteriaSummary.ascx" tagname="CandidateSearchCriteriaSummary" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/CandidatesLikeThisModal.ascx" tagname="CandidatesLikeThisModal" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="SaveSearchActionModal" Src="~/Code/Controls/User/SaveSearchActionModal.ascx" %>
<%@ Register src="~/WebAssist/Controls/CompareResumesModal.ascx" tagname="CompareResumesModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/SearchCriteria/ResumeSearchAdditionalCriteria.ascx" tagname="AdditionalCriteria" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/JobApplicationRequirementsModal.ascx" tagname="JobApplicationRequirementsModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ReferralRequestSentModal.ascx" tagName="ReferralRequestSentModal" tagPrefix="uc" %>
<%@ Register src="~/Code/Controls/User/EmailDocumentModal.ascx" tagname="EmailResumeModal" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<style type="text/css">
  </style>
	<script src="<%# ResolveUrl("~/Assets/Scripts/jquery-ui-1.9.2.custom.new.min.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
  <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <uc:AssistEmployersNavigation ID="AssistEmployersNavigationMain" runat="server" />
  <asp:HyperLink runat="server" ID="ReturnToJobOrderDashboardLink"></asp:HyperLink> <br/>
  <div id="JobDetails" runat="server">
  <table style="width:100%;" role="presentation">
		<tr>
			<td style="width:100%;"><h1><asp:HyperLink runat="server" ID="ReturnToJobOrderLink"></asp:HyperLink></h1></td>
			<td style="width:100%;">
				<asp:UpdatePanel ID="ViewNotesUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<asp:Button ID="ViewNotesButton" runat="server" SkinID="Button3" OnClick="ViewNotesButton_Click" CausesValidation="false" UseSubmitBehavior="False" />
						<uc:NotesAndRemindersModal ID="NotesAndReminders" runat="server" OnCompleted="NotesAndReminders_Completed" />	
					</ContentTemplate>
				</asp:UpdatePanel>
			</td>
		</tr>
	</table>
  <h3><asp:Literal runat="server" ID="JobEmployerLabel"></asp:Literal></h3>
  </div>

<asp:Panel ID="SearchPanelTop" DefaultButton="KeywordSearchButton" runat="server">
  <table style="width:99%" role="presentation">
    <tr>
      <td>
	      <table role="presentation">
		      <tr>
			      <td>
				      <strong><%= HtmlLocalise("SearchCriteriaTitle.Label", "Search resumes")%></strong>
				      &nbsp;
				      <asp:Button ID="SaveSearchButton" runat="server"  class="button3" ClientIDMode="Static" onclick="SaveSearchButton_Click"/>
				      &nbsp;
				      <asp:Label ID="SaveSearchErrorLabel" runat="server" CssClass="error" />
			      </td>
		      </tr>
				</table>
		    <div style="width:100%; margin-top:2px; margin-bottom:10px ;float:left">
					<div style="display: inline-block">
						<asp:Literal ID="SearchLabel" runat="server" />
						<asp:DropDownList runat="server" ID="JobTypeDropDownList" ClientIDMode="Static" />
						&nbsp;<%= HtmlLocalise("Keywords.Text", "with keyword(s)")%>
					</div>
					<div style="display: inline-block">
						<%= HtmlInFieldLabel("KeywordsTextBox", "Keywords.InlineLabel", "type keywords", 250)%>
						<asp:TextBox runat="server" ID="KeywordsTextBox" ClientIDMode="Static" Width="250"/>
					</div>
					<%= HtmlLabel(KeywordContextDropDown,"In.Text", "in")%>
					<asp:DropDownList runat="server" ID="KeywordContextDropDown" ClientIDMode="Static" Title="Keywords in"/>
					<%= HtmlLabel(KeywordScopeDropDown,"Within.Text", "within")%>
					<asp:DropDownList runat="server" ID="KeywordScopeDropDown" ClientIDMode="Static" Enabled="False" Title="Keywords within"/>
					<asp:Button ID="KeywordSearchButton" runat="server" class="button3" onclick="Search_Click" ClientIDMode="Static" />
					&nbsp;<asp:RequiredFieldValidator ID="KeywordRequired" ControlToValidate="KeywordsTextBox" runat="server" ValidationGroup="PoolSearchKeywordSearch" CssClass="error" Display="Dynamic"/>
					<div style="float: right">
						<asp:Button ID="ClearSearchButton" runat="server" class="button3"  onclick="ClearSearchButton_Click" ClientIDMode="Static" />
					</div>
				</div>
	    </td>
    </tr>
  </table>
</asp:Panel>

<asp:Panel ID="AdvancedSearchHeaderPanel" runat="server" CssClass="singleAccordionTitle " Visible="True">
	<asp:Image ID="AdvancedSearchHeaderImage" runat="server" alt="Advanced Search Image" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" Height="22" Width="22" />
	&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Search.Label", "Search options")%></a></span></asp:Panel>
<asp:Panel ID="AdvancedSearchPanel" runat="server">
	<div id="SearchPanelMiddle">
		<table role="presentation">
			<tr>
				<td style="height: 21px"><%= HtmlLabel("SampleResumeSearch.Label", "Search by sample resume")%></td>
				<td class="column2" style="height: 21px">
				</td>
			</tr>
			<tr>
				<td class="column1">
					<table role="presentation">
						<tr >
							<td style="width: 275px;"><asp:FileUpload ID="ResumeFileUpload"  title="Resume FileUpload" runat="server" CssClass="fileupload" ClientIDMode="Static" /></td>
							<td style="padding-left: 3px"><%= HtmlTooltipster("tooltipWithArrow", "ResumeFileUpload.Tooltip", @"Upload a resume and click 'go' to begin your search. You may narrow your search by selecting other criteria in Advanced search options.")%></td>	
							<td><asp:RegularExpressionValidator ID="ResumeFileUploadRegularEx" runat="server" ControlToValidate="ResumeFileUpload" CssClass="error" ValidationGroup="PoolSearchResumeSearch" /></td>
						</tr>
					</table>
				</td>
				<td class="column2">
				</td>
			</tr>
			<tr>
				<td class="column1">
					<div id="UploadedResumeDiv" style="display:none;">
						<asp:Label id="UploadedResumeLabel" runat="server" ClientIDMode="Static"></asp:Label>
						<asp:Button ID="SampleResumeButton" runat="server" class="button3" ClientIDMode="Static"  onclick="SampleResumeButton_Click" />
						<asp:Button ID="ClearResumeButton" runat="server" class="button3" ClientIDMode="Static" OnClick="ClearResumeButton_OnClick" CausesValidation="False" />
						<br />
					</div>
				</td>
				<td class="column2">
				</td>
			</tr>
			<tr id="SavedSearchLabelRow">
				<td class="column1"><%= HtmlLabel(SavedSearchDropDown,"SavedSearch.Label", "Search by saved query")%></td>
				<td class="column2"></td>
			</tr>
			<tr id="SavedSearchRow">
				<td class="column1">
					<asp:DropDownList runat="server" ID="SavedSearchDropDown" Width="250" ClientIDMode="Static" />
					<asp:Button ID="SavedSearchButton" runat="server" class="button3" ClientIDMode="Static" ValidationGroup="PoolSavedSearch" onclick="SavedSearchButton_Click" />
					<asp:RequiredFieldValidator ID="SavedSearchRequired" ControlToValidate="SavedSearchDropDown" runat="server" ValidationGroup="PoolSavedSearch" CssClass="error" />
				</td>
				<td class="column2">
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2"><strong><%= HtmlLocalise("AdvancedSearch.Label", "Advanced search options")%></strong></td>
			</tr>
		</table>
		<uc:AdditionalCriteria runat="server" ID="AdditionalCriteria" ValidationGroup="PoolSearchAdvancedSearch" />
		<table role="presentation">
			<tr>
				<td colspan="2"><asp:Button ID="ApplyAdvancedSearchButton" runat="server" class="button3 right" ClientIDMode="Static" ValidationGroup="PoolSearchAdvancedSearch" OnClick="Search_Click" />
					</td>
			</tr>
		</table>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="AdvancedSearchPanelExtender" runat="server" 
														TargetControlID="AdvancedSearchPanel" 
														ExpandControlID="AdvancedSearchHeaderPanel" 
														CollapseControlID="AdvancedSearchHeaderPanel" 
														Collapsed="true" 
														ImageControlID="AdvancedSearchHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true" BehaviorID="AdvancedSearchPanelBehavior" />
<div id="SearchPanelBottom">
</div>
<div class="clear">
</div>

<asp:Panel ID="SearchSummarySectionPanel" runat="server" Visible="false">
	<asp:Panel ID="SearchSummaryHeaderPanel" runat="server" CssClass="singleAccordionTitle">
		<asp:Image ID="SearchSummaryHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="Search summary image"/>
		&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("SearchSummary.Label", "Search summary")%></a></span></asp:Panel>
	<asp:Panel ID="SearchSummaryPanel" runat="server">
		<%= HtmlLocalise("SearchSummary.Text", "The following criteria was applied in the search:")%><br />
		<uc:CandidateSearchCriteriaSummary ID="SearchCriteriaSummary" runat="server" />
	</asp:Panel>
	<act:CollapsiblePanelExtender ID="SearchSummaryPanelExtender" runat="server" 
																				TargetControlID="SearchSummaryPanel" 
																				ExpandControlID="SearchSummaryHeaderPanel" 
																				CollapseControlID="SearchSummaryHeaderPanel" 
																				Collapsed="false" 
																				ImageControlID="SearchSummaryHeaderImage" 
																				CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
																				ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																				SuppressPostBack="true" />
</asp:Panel>

<ul class="navTab localNavTab">
	<li id="TabAllResumes" runat="server" ><asp:HyperLink ID="AllResumesLink" runat="server"><%= HtmlLocalise("AllResumesTab.Label", "All Resumes")%></asp:HyperLink></li>
  <li id="TabMatchesForThisJob" runat="server" ><asp:HyperLink ID="MatchesForThisJobLink" runat="server"><%= HtmlLocalise("MatchesForThisJob.Label", "Recommended Matches")%></asp:HyperLink></li>
	<li id="TabApplicants" runat="server" ><asp:HyperLink ID="ApplicantsLink" runat="server"><%= HtmlLocalise("ApplicantsTab.Label", "Referred Applicants")%></asp:HyperLink></li>
  <li id="TabReferrals" runat="server" ><asp:HyperLink ID="ReferralsLink" runat="server"><%= HtmlLocalise("Referrals.Label", "Pending Referrals")%></asp:HyperLink></li>
  <li id="TabInvitees" runat="server"><asp:HyperLink ID="InviteesLink" runat="server"><%= HtmlLocalise("Invitees.Label","Invitees") %></asp:HyperLink></li>
</ul>
<div id="tabbedContent">
  <asp:UpdatePanel ID="SearchResultUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
	<div style="width:100%;">
		<uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="SearchResultListView" PageSize="10" DisplayRecordCount="True" />
	</div>
	<asp:ListView ID="SearchResultListView" DataSourceID="SearchResultDataSource" runat="server" OnItemDataBound="SearchResultListView_ItemDataBound" OnDataBound="SearchResultListView_DataBound"
								DataKeyNames="Id,Name,Status,Score,Flagged,IsVeteran,YearsExperience" ItemPlaceholderID="SearchResultPlaceHolder" Visible="True">
		<LayoutTemplate>
			<table role="presentation" class="genericTable">
				<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
			</table>
		</LayoutTemplate>
		<ItemTemplate>
			<tr class="topRow" data-row="<%# ((ResumeView)Container.DataItem).ApplicationApprovalStatus.ToString().ToLower() %> candidate">
				<td class="column1" style="width: 120px;"><span><asp:image ID="SearchResultScoreImage" runat="server" AlternateText="Search result score" Width="80" Height="16"/></span></td>
				<td class="column2">
					<span>
						<asp:LinkButton ID="SearchResultNameLink" runat="server" OnCommand="SearchResultRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="Preview" />
						<asp:image ID="FlaggedImage" runat="server" Visible="false" AlternateText="Flagged image"/>
						<asp:image ID="VeteranImage" runat="server" Visible="false" Height="15" Width="17" AlternateText="Veteran"/>
					    <br/>
					    <asp:Literal ID="SearchResultInviteesQueuedStatusLiteral" runat="server" Visible="false" />
					</span>
				</td>
				<td class="column2">
					<span><asp:literal ID="SearchResultEmploymentHistoryLiteral" runat="server" /><asp:Literal runat="server" ID="SimilarToMessage" Visible="False" /></span>
				</td>
			  <td class="column2" >
				  <div>
					  <div style="float: right;">
				    <table role="presentation">
				      <tr>
				        <td style="width: 70px;"> <%# HtmlLocalise("SearchResultList.Experience.Text", "{0} years", ((ResumeView)Container.DataItem).YearsExperience)%></td>
                <td>
										<asp:Button ID="ReferButton" CommandName="Refer" CommandArgument="<%# Container.DisplayIndex %>" Visible="false" runat="server" data-action="REFER" ClientIDMode="AutoID"  CssClass="button1" OnCommand="ReferButton_Click" />
										<asp:Button runat="server" ID="ViewReferralRequest" CssClass="button1" CommandName="ViewReferral" OnClick="ViewReferralRequest_OnClick" data-action="VIEW REFERRAL REQUEST" Visible="False" ></asp:Button>
                </td>
				      </tr>
				    </table>
						</div>
					</div>
       </td>
				<td class="column3">
					<span>
						<asp:DropDownList runat="server" ID="SearchResultApplicationStatusDropDown" title="Search Result Application Status DropDown" Visible="false" OnSelectedIndexChanged="SearchResultApplicationStatusDropDown_SelectedIndexChanged" AutoPostBack="true"/>
            <asp:Literal runat="server" ID="QueuedForVeteranLiteral" Visible="False"></asp:Literal>
					</span>
				</td>
			</tr>
			<tr class="middleRow">
				<td class="column1" colspan="6">
					<div>
									<asp:LinkButton ID="SimilarToLinkButton" runat="server" Visible="false" OnCommand="CompareResumesLinkButton_Command" CommandName="CompareResumes"/>
					</div>
				</td>
			</tr>
			<tr class="bottomRow">
				<td class="column2" colspan="5" style="height:57px;">
					<span>
						<table role="presentation" style="width:100%">
							<tr>
								<td>
									<div class="tooltip left Email">
										<asp:LinkButton ID="SearchResultEmailButton" runat="server" OnCommand="SearchResultRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="Email" aria-label="Email" data-action="EMAIL">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Email.ToolTip", "Email")%>"></div>
										</asp:LinkButton>
									</div>
									
									<div id="CandidateNotesDiv" runat="server" class="tooltip left Note">
										<asp:LinkButton ID="SearchResultNoteLinkButton" runat="server" OnCommand="SearchResultRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="Note" aria-label="Note">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("Note.ToolTip", "Add/Edit Note")%>"></div>
										</asp:LinkButton>
									</div>

									<div class="tooltip left MoreCandidatesLikeThis">
										<asp:LinkButton ID="SearchResultCandidatesLikeThisButton" runat="server" OnCommand="SearchResultRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="CandidatesLikeThis" aria-label="Candidates Like This">
											<div class="tooltipImage toolTipNew" title="<%# HtmlLocalise("CandidatesLikeThis.ToolTip", "Candidates Like This")%>"></div>
										</asp:LinkButton>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<asp:panel ID="NotePanel" runat="server" Visible="false" Width="100%" >
										<br/>
										<table role="presentation" style="width:100%">
											<tr>
												<td><asp:TextBox ID="NotesTextBox" Width="99%" MaxLength="1000" runat="server" /></td>
												<td style="width:20px; text-align:center">
														<asp:LinkButton ID="SaveNoteLinkButton" runat="server" OnCommand="SearchResultRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="SaveNote">
															<img src="<%= UrlBuilder.SaveIcon() %>" alt="<%= HtmlLocalise("SaveNote.Text", "Save note") %>" style="width:16px" />
														</asp:LinkButton>
												</td>
												<td style="width:20px">
													<asp:LinkButton ID="CloseNoteLinkButton" runat="server" OnCommand="SearchResultRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="CloseNote">
														<img src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" />
													</asp:LinkButton>
												</td>
											</tr>
										</table>
										<br/>
									</asp:panel>
								</td>
							</tr>
						</table>
					</span>
				</td>
			</tr>
		</ItemTemplate>
		<EmptyDataTemplate>
			<p>
				<b><%= HtmlLocalise("NoResults.Text", "No matching candidates found") %></b>
			</p>
		</EmptyDataTemplate>
	</asp:ListView>
  <uc:JobApplicationRequirementsModal ID="ApplicationRequirementsModal" runat="server" OnReferred="ApplicationRequirementsModal_Referred" />
	<uc:ReferralRequestSentModal ID="AutoApprovalModal" runat="server" OnAutoApproved="AutoApprovalModal_OnAutoApproved" />
  </ContentTemplate>
  </asp:UpdatePanel>

	<asp:Panel ID="InstructionsPanel" runat="server" Visible="False">
		<p>
			<i><%= HtmlLocalise("Instructions.Text", "Search resumes in #FOCUSASSIST# with any of the following options. Your results will appear below.")%></i>
		</p>
		<a class="button7">
			<strong><%= HtmlLocalise("KeywordSearchButton.Header", "Keyword and advanced search") %></strong>&nbsp; 
			<%= HtmlLocalise("KeywordSearchButton.Text", "search hundreds of criteria to narrow in on candidates")%>
		</a> 
		<a class="button7">
			<strong><%= HtmlLocalise("OpenJobSearchButton.Header", "Search based on your open jobs")%></strong>&nbsp; 
			<%= HtmlLocalise("OpenJobSearchButton.Text", "#FOCUSASSIST# will suggest people for your job")%>
		</a> 
		<a class="button7">
			<strong><%= HtmlLocalise("ResumeSearchButton.Header", "Search based on a sample resume")%></strong>&nbsp; 
			<%= HtmlLocalise("ResumeSearchButton.Text", "Lose someone fabulous? Describe that individual and let #FOCUSASSIST# find your next fabulous employee.")%>
		</a> 
		<a class="button7">
			<strong><%= HtmlLocalise("SavedSearchButton.Header", "Search based on a saved search")%></strong>&nbsp; 
			<%= HtmlLocalise("SavedSearchButton.Text", "Find people who match a previously saved search")%>
		</a>	
	</asp:Panel>
		
	<asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.WebAssist.Pool" EnablePaging="true" SelectMethod="GetCandidates" SelectCountMethod="GetCandidateCount" 
												SortParameterName="orderBy" OnSelected="SearchResultDataSource_Selected" OnSelecting="SearchResultDataSource_Selecting">
	</asp:ObjectDataSource>
</div>


<uc:ResumePreviewModal ID="ResumePreview" runat="server" OnResumePreviewEmailResumeCandidateClick="ResumePreviewModalEmailResumeCandidate_Click" OnResumePreviewEmailCandidateClick="ResumePreviewModalEmailCandidate_Click" OnResumePreviewCandidatesLikeMeClick="ResumePreviewCandidatesLikeMe_Click" />
<uc:CandidatesLikeThisModal ID="CandidatesLikeThis" runat="server" OnCandidatesLikeThisEmailCandidateClick="CandidatesLikeThisModalEmailCandidate_Click" />
<uc:EmailCandidateModal ID="EmailCandidate" runat="server" OnCompleted="EmailCandidate_Completed" />
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
<uc:SaveSearchActionModal ID="SaveSearchAction" runat="server" OnCompleted="SaveSearchAction_Completed"/>
<uc:CompareResumesModal ID="CompareResumes" runat="server" OnCompleted="CompareResumes_Completed"/>
<uc:EmailResumeModal ID="EmailResume" runat="server"/>
<asp:UpdatePanel runat="server" ID="ConfirmationUpdatePanel" UpdateMode="Conditional">
<ContentTemplate>
    <uc:ConfirmationModal ID="ReferralConfirmationModal" runat="server" Width="300px" Height="200px" />
</ContentTemplate>  
</asp:UpdatePanel>

<script type="text/javascript">
	$(document).ready(function () {

			$('.fileupload').show();
		  $('.fileupload').customFileInput('<%= HtmlLocalise("Global.Browse.Text", "Browse")%>', '<%= HtmlLocalise("Global.Change.Text", "Change")%>', '<%= HtmlLocalise("UploadResumeFeedback.Text", "Upload a resume")%>');
    });
   $('.button7').click(function () 
	 {
   	$find('AdvancedSearchPanelBehavior')._doOpen();
  });
</script>
</asp:Content>
