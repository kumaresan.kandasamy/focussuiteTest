﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using Focus.Web.Core.Models;
using Framework.Core;

using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
	public partial class HiringManagerProfile : PageBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets the employee business unit id.
		/// </summary>
		private long EmployeeBusinessUnitId { get; set; }

		internal HiringManagerProfileModel HiringManagerProfileModel
		{
			get { return GetViewStateValue<HiringManagerProfileModel>("HiringManagerProfile:HiringManagerProfile"); }
			set { SetViewStateValue("HiringManagerProfile:HiringManagerProfile", value); }
		}

		internal EmployeeBusinessUnitCriteria EmployeeBusinessUnitCriteria
		{
			get { return GetViewStateValue<EmployeeBusinessUnitCriteria>("ListEmployeeBusinessUnitCriteria:EmployeeBusinessUnitCriteria"); }
			set { SetViewStateValue("ListEmployeeBusinessUnitCriteria:EmployeeBusinessUnitCriteria", value); }
		}

		protected string CloseBusinessWindowErrorMessage = "";
		private bool _filterApplied;

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			CloseBusinessWindowErrorMessage = CodeLocalise("CloseBusinessWindow.Error.Text", "You can only have one #BUSINESS#:LOWER window open at a time");
			
			if (Request.QueryString["filtered"] != null) 
				_filterApplied = Convert.ToBoolean(Request.QueryString["filtered"]);

			if (!Page.IsPostBack)
			{
				if (Page.RouteData.Values.ContainsKey("employeebusinessunitid"))
				{
					long id;
					Int64.TryParse(Page.RouteData.Values["employeebusinessunitid"].ToString(), out id);
					EmployeeBusinessUnitId = id;
					GetStaffProfileData();
					PopulateStaffData();

					HMActivityList.EmployeeBusinessUnitId = EmployeeBusinessUnitId;
					HMActivityList.BusinessUnitId = HiringManagerProfileModel.BusinessUnitId;
					HMActivityList.EmployeeId = HiringManagerProfileModel.EmployeeId;

					NotesReminders.EntityId = HiringManagerProfileModel.EmployeeId;
					NotesReminders.EntityType = EntityTypes.Employee;
					NotesReminders.ReadOnly = (!App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator));
					NotesReminders.Bind();
                    Page.Title = CodeLocalise("Page.Title", "Hiring Manager Profile - "+ HiringManagerProfileModel.BusinessUnitName +"-"+HiringManagerProfileModel.EmployeeId);
				}

				Localise();
				ApplyBranding();
			}
		}

		/// <summary>
		/// Gets the staff profile data.
		/// </summary>
		private void GetStaffProfileData()
		{
			HiringManagerProfileModel = ServiceClientLocator.EmployeeClient(App).GetHiringManagerProfile(EmployeeBusinessUnitId);
		}

		/// <summary>
		/// Populates the staff data.
		/// </summary>
		private void PopulateStaffData()
		{
			// Info for Contact Information Panel
			var sb = new StringBuilder();

			sb.Append(HiringManagerProfileModel.EmployeeName + "<br/>");
			sb.Append(HiringManagerProfileModel.BusinessUnitName + "<br/>");

			var address = (from add in HiringManagerProfileModel.BusinessUnitAddresses
										 where add.IsPrimary
										 select add).First();

			if (address != null)
			{
				sb.Append(address.Line1);
				if (address.Line2.IsNotNullOrEmpty())
					sb.Append("<br/>" + address.Line2);
				if (address.Line3.IsNotNullOrEmpty())
					sb.Append("<br/>" + address.Line3);
				sb.Append("<br/>" + address.TownCity + ", " + address.PostcodeZip + "<br/>");
			}

			lblContactDetails.Text = sb.ToString();

			sb = new StringBuilder();

			if (HiringManagerProfileModel.EmployeePhoneNumber.IsNotNullOrEmpty() && HiringManagerProfileModel.EmployeePhoneNumber.Count > 0)
			{
				var primaryPhone = HiringManagerProfileModel.EmployeePhoneNumber.FirstOrDefault(x => x.IsPrimary);
				sb.Append("<br/>").Append(primaryPhone.PhoneType + ": " + Regex.Replace(primaryPhone.Number, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));

				if (primaryPhone.Extension.IsNotNullOrEmpty())
					sb.Append(" (ext " + primaryPhone.Extension + ")");

				var altPhones = HiringManagerProfileModel.EmployeePhoneNumber.Where(x => !x.IsPrimary).ToList();

				if (altPhones.IsNotNullOrEmpty() && altPhones.Any())
				{
					foreach (var altPhone in altPhones)
					{
						sb.Append("<br/>").Append(altPhone.PhoneType).Append(": ").Append(Regex.Replace(altPhone.Number, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
					}
				}
			}

			sb.Append("<br/>email: " + HiringManagerProfileModel.EmployeeEmail);

			// Contact Information
			lblHiringManagerName.Text = String.Format("{0} / {1}", HiringManagerProfileModel.EmployeeName, HiringManagerProfileModel.BusinessUnitName);
			lblContactDetailsPhone.Text = sb.ToString();

			// Logins
			lblDaysSinceRegistration.Text = HiringManagerProfileModel.DaysSinceRegistration.ToString();
			lblLoginsSinceRegistration.Text = HiringManagerProfileModel.LoginsSinceRegistration.ToString();
			lblLogins7Days.Text = HiringManagerProfileModel.Logins7Days.ToString();
			lblTimeSinceLastLogin.Text = TimeSince(HiringManagerProfileModel.LastLogin);

			#region Activity Summary

			Applicants7daysLabel.Text = HiringManagerProfileModel.ApplicantsSeven.ToString();
			//TODO: Matches7daysLabel.Text = HiringManagerProfileModel.MatchesSeven.ToString();
			//TODO: ThreeFiveStarMatches7daysLabel.Text = HiringManagerProfileModel.ThreeFiveStar.ToString();
			//TODO: ThreeFiveStarMatchesNotView7daysLabel.Text = HiringManagerProfileModel.ThreeFiveStarNotViewed.ToString();
			InvitationsToSeekers7daysLabel.Text = HiringManagerProfileModel.InvitationsSentSeven.ToString();
			InvitedSeekersViewedJobs7daysLabel.Text = HiringManagerProfileModel.InvitedSeekersViewedJobSeven.ToString();
			InvitedSeekersNotViewJob7daysLabel.Text = HiringManagerProfileModel.InvitedSeekersNotViewJobSeven.ToString();
			InvitedSeekersClickedHowToApply7daysLabel.Text = HiringManagerProfileModel.InvitedSeekersClickedHowToApplySeven.ToString();
			InvitedSeekersNotClickHowToApply7daysLabel.Text = HiringManagerProfileModel.InvitedSeekersNotClickHowToApplySeven.ToString();

			Applicants30daysLabel.Text = HiringManagerProfileModel.ApplicantsThirty.ToString();
			//TODO: Matches30daysLabel.Text = HiringManagerProfileModel.MatchesThirty.ToString();
			//TODO: ThreeFiveStarMatches30daysLabel.Text = HiringManagerProfileModel.ThreeFiveStar.ToString();
			//TODO: ThreeFiveStarMatchesNotView30daysLabel.Text = HiringManagerProfileModel.ThreeFiveStarNotViewed.ToString();
			InvitationsToSeekers30daysLabel.Text = HiringManagerProfileModel.InvitationsSentThirty.ToString();
			InvitedSeekersViewedJobs30daysLabel.Text = HiringManagerProfileModel.InvitedSeekersViewedJobThirty.ToString();
			InvitedSeekersNotViewJob30daysLabel.Text = HiringManagerProfileModel.InvitedSeekersNotViewJobThirty.ToString();
			InvitedSeekersClickedHowToApply30daysLabel.Text = HiringManagerProfileModel.InvitedSeekersClickedHowToApplyThirty.ToString();
			InvitedSeekersNotClickHowToApply30daysLabel.Text = HiringManagerProfileModel.InvitedSeekersNotClickHowToApplyThirty.ToString();

			#endregion

			//Referral Outcomes
			lblApplicantsInterviewed.Text = HiringManagerProfileModel.ApplicantsInterviewed.ToString();
			lblApplicantsFailedToShow.Text = HiringManagerProfileModel.ApplicantsFailedToShow.ToString();
			lblApplicantsInterviewDenied.Text = HiringManagerProfileModel.ApplicantsInterviewDenied.ToString();
			lblApplicantsHired.Text = HiringManagerProfileModel.ApplicantsHired.ToString();
			lblApplicantsRejected.Text = HiringManagerProfileModel.ApplicantsRejected.ToString();
			lblApplicantsRefused.Text = HiringManagerProfileModel.ApplicantsRefused.ToString();
			lblApplicantsDidNotApply.Text = HiringManagerProfileModel.ApplicantsDidNotApply.ToString();
			lblApplicantsFailedToRespondToInvitation.Text = HiringManagerProfileModel.ApplicantsFailedToRespondToInvitation.ToString();
			lblApplicantsFoundJobFromOtherSource.Text = HiringManagerProfileModel.ApplicantsFoundJobFromOtherSource.ToString();
			lblApplicantsJobAlreadyFilled.Text = HiringManagerProfileModel.ApplicantsJobAlreadyFilled.ToString();
			lblApplicantsNewApplicant.Text = HiringManagerProfileModel.ApplicantsNewApplicant.ToString();
			lblApplicantsNotQualified.Text = HiringManagerProfileModel.ApplicantsNotQualified.ToString();
			lblApplicantsNotYetPlaced.Text = HiringManagerProfileModel.ApplicantsNotYetPlaced.ToString();
			lblApplicantsRecommended.Text = HiringManagerProfileModel.ApplicantsRecommended.ToString();
			lblApplicantsRefusedReferral.Text = HiringManagerProfileModel.ApplicantsRefusedReferral.ToString();
			lblApplicantsUnderConsideration.Text = HiringManagerProfileModel.ApplicantsUnderConsideration.ToString();

			//Survey Responses
			lblSurveyVeryDissatified.Text = HiringManagerProfileModel.SurveyVeryDissatified.ToString();
			lblSurveyVerySatisfied.Text = HiringManagerProfileModel.SurveyVerySatisfied.ToString();
			lblSurveySomewhatDissatified.Text = HiringManagerProfileModel.SurveySomewhatDissatified.ToString();
			lblSurveySomewhatSatisfied.Text = HiringManagerProfileModel.SurveySomewhatSatisfied.ToString();
			lblSurveyDidNotHire.Text = HiringManagerProfileModel.SurveyDidNotHire.ToString();
			lblSurveyHired.Text = HiringManagerProfileModel.SurveyHired.ToString();
			lblSurveyDidNotInterview.Text = HiringManagerProfileModel.SurveyDidNotInterview.ToString();
			lblSurveyInterviewed.Text = HiringManagerProfileModel.SurveyInterviewed.ToString();
		}


		/// <summary>
		/// Determines whether [is assist employers administrator].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is assist employers administrator]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistEmployersAdministrator()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
		}

		/// <summary>
		/// Calculates the time since a date
		/// </summary>
		/// <param name="actionTime">The action time.</param>
		/// <returns></returns>
		private string TimeSince(DateTime? actionTime)
		{
			if (!actionTime.HasValue) return String.Empty;

			var currentDateTime = DateTime.Now;
			var timeSince = currentDateTime.Subtract(actionTime.GetValueOrDefault());
			return timeSince.Days + " " + CodeLocalise("Days", "days") + " " + timeSince.Hours + " " +
									 CodeLocalise("Hours", "hours") + " " + timeSince.Minutes + " " + CodeLocalise("Minutes", "minutes");
		}

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			var returnUrl = GetReturnUrl();

			if (returnUrl.Contains("joborderdashboard"))
				lnkReturn.Text = CodeLocalise("ReturnToJobOrderDashboard", "return to #EMPLOYMENTTYPE#:LOWER dashboard");
			else if (returnUrl.Contains("employers"))
			{
				if (App.Settings.Theme == FocusThemes.Workforce)
					lnkReturn.Text = CodeLocalise("ReturnToEmployers", "return to Find a #BUSINESS#:LOWER account");
				else
					lnkReturn.Text = CodeLocalise("ReturnToEmployers", "return to Find an #BUSINESS#:LOWER account");
			}
			else
				lnkReturn.Text = CodeLocalise("ReturnToEmployerProfile", "return to #BUSINESS# profile");

      ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");

		}

		private void ApplyBranding()
		{
			ContactInformationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ContactInformationCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ContactInformationCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			LoginsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LoginsCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			LoginsCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ActivitySummaryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ActivitySummaryCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ActivitySummaryCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ReferralOutcomesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ReferralOutcomesCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ReferralOutcomesCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			SurveyResponsesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SurveyResponsesCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			SurveyResponsesCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ActivityLogHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ActivityLogCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ActivityLogCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			NotesAndRemindersHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			NotesAndRemindersCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			NotesAndRemindersCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Handles the Click event of the lnkReturn control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void lnkReturn_Click(object sender, EventArgs e)
		{
			var returnPage = UseReturnUrl();
			if (_filterApplied && !returnPage.Contains("filtered="))
			{
				Response.Redirect(string.Concat(returnPage, "?filtered=true"));
			}
			else
			{
				Response.Redirect(returnPage);
			}
		}

		/// <summary>
		/// Handles the Click event of the AccessEmployerAccountButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void AccessEmployerAccountButton_Click(object sender, EventArgs e)
        {
    		var singleSignOnKey = ServiceClientLocator.AuthenticationClient(App).CreateEmployeeSingleSignOn(HiringManagerProfileModel.EmployeeId, null);
			var blockedPopupMessage = CodeLocalise("AccessEmployeePopupBlocked.Error.Text", "Unable to access #BUSINESS#:LOWER account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.");
			var script = string.Format("$(document).ready(function (){{Business_TalentPopup = showPopup('{0}{1}','{2}');}});", App.Settings.TalentApplicationPath, UrlBuilder.SingleSignOn(singleSignOnKey, false), blockedPopupMessage);
			ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AccessEmployeeAccount", script, true);
		}
	}
}