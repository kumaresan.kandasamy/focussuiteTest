﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerActivityList.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EmployerActivityList" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>

<table style="width: 100%;" role="presentation">
	<tr style="float: left">
		<td>
			<%=HtmlLabel(DaysFilterDropDown,"ShowResultsFor.Text", "Show results for")%>
			&nbsp;
			<asp:DropDownList runat="server" ID="DaysFilterDropDown" OnSelectedIndexChanged="DaysFilterDropDown_SelectedIndexChanged" AutoPostBack="True" />
			&nbsp;&nbsp;
			<%=HtmlLabel(OrderByFilterDropDown,"InLabel.Text", "in")%>
			&nbsp;
			<asp:DropDownList runat="server" ID="OrderByFilterDropDown" OnSelectedIndexChanged="OrderByFilterDropDown_SelectedIndexChanged" AutoPostBack="True" />
			&nbsp;&nbsp;
			<%=HtmlLocalise("OrderLabel.Text", "order")%>
			&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td>
			<asp:LinkButton runat="server" ID="DownloadCsvLinkButton" OnClick="DownloadCsvLinkButton_Click" ><%= HtmlLocalise("DownloadCsvFile.Text", "Download this log (CSV file)")%></asp:LinkButton>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
</table>			
<asp:UpdatePanel runat="server" ID="ActivityLogUpdatePanel">
	<ContentTemplate>
		<uc:Pager ID="ActivityLogPager" runat="server" PagedControlID="ActivityLogList" PageSize="10"  />
		<asp:ListView ID="ActivityLogList" runat="server" OnItemDataBound="ActivityLogList_ItemDataBound" ItemPlaceholderID="ActionSearchPlaceHolder" DataSourceID="ActivityLogDataSource"
									DataKeyNames="BuId,ActionedOn"  OnLayoutCreated="ActivityLogList_LayoutCreated" OnSorting="ActivityLogList_Sorting">

			<LayoutTemplate>
				<table style="width: 100%;" role="presentation">
					<tr>
						<td>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height: 16px;"><asp:Literal runat="server" ID="ActionDateTimeHeader" /></td>
									<td style="height: 8px;"><asp:ImageButton ID="ActionDateTimeAscButton" runat="server" CommandName="Sort" CommandArgument="activitydate asc" CssClass="AscButton" AlternateText="Sort Ascending"/></td>
								</tr>
								<tr>
									<td style="height: 8px;"><asp:ImageButton ID="ActionDateTimeDescButton" runat="server"  CssClass="DescButton" CommandName="Sort" CommandArgument="activitydate desc"  AlternateText="Sort Descending"/></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height: 16px;"><asp:Literal runat="server" ID="UsernameHeader" /></td>
									<td style="height: 8px;"><asp:ImageButton ID="UsernameAscButton" runat="server" CommandName="Sort" CommandArgument="activityusername asc" CssClass="AscButton" AlternateText="Sort Ascending"/></td>
								</tr>
								<tr>
									<td style="height: 8px;"><asp:ImageButton ID="UsernameDescButton" runat="server" CssClass="DescButton" CommandName="Sort" CommandArgument="activityusername desc"  AlternateText="Sort Descending"/></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height: 16px;"><asp:Literal runat="server" ID="ActionHeader" /></td>
									<td style="height: 8px;"><asp:ImageButton ID="ActionAscButton" runat="server" CommandName="Sort" CommandArgument="activityaction asc" CssClass="AscButton" AlternateText="Sort Ascending"/></td>
								</tr>
								<tr>
									<td style="height: 8px;"><asp:ImageButton ID="ActionDescButton" runat="server" CssClass="DescButton" CommandName="Sort" CommandArgument="activityaction desc"  AlternateText="Sort Descending"/></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="6"><hr /></td>
					</tr>
					<asp:PlaceHolder ID="ActionSearchPlaceHolder" runat="server" />
				</table>
			</LayoutTemplate>
			<ItemTemplate>
				<tr>
					<td style="width:15%"><asp:Literal runat="server" ID="ActionDate" Text="<%# ((BusinessUnitActivityViewDto)Container.DataItem).ActionedOn%>"></asp:Literal></td> 
					<td style="width:15%"><asp:Literal runat="server" ID="Username" Text="<%# ((BusinessUnitActivityViewDto)Container.DataItem).UserName%>"></asp:Literal></td>
					<td style="width:20%"><asp:Literal runat="server" ID="ActionDescription"></asp:Literal></td>
				</tr>
			</ItemTemplate>
			<EmptyDataTemplate><br/><%=HtmlLocalise("NoEmployerActivity", "No activity recorded") %></EmptyDataTemplate>
		</asp:ListView>

  	<asp:ObjectDataSource ID="ActivityLogDataSource" OnSelecting="ActivityLogDataSource_Selecting" runat="server" EnablePaging="true" 
													SelectMethod="GetActivityLog" SelectCountMethod="GetActivityLogCount" SortParameterName="orderBy" TypeName="Focus.Web.WebAssist.Controls.EmployerActivityList">
		</asp:ObjectDataSource>
			
	</ContentTemplate>
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="DaysFilterDropDown" EventName="SelectedIndexChanged" />
		<asp:AsyncPostBackTrigger ControlID="OrderByFilterDropDown" EventName="SelectedIndexChanged" />
	</Triggers>
</asp:UpdatePanel>
