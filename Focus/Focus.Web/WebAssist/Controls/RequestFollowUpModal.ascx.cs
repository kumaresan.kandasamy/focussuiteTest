﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.WebAssist.Controls
{
  public partial class RequestFollowUpModal : UserControlBase
	{

    #region Properties

    protected long Id
    {
      get { return GetViewStateValue<long>("RequestFollowUpModal:Id"); }
      set { SetViewStateValue("RequestFollowUpModal:Id", value); }
    }

		// The entity type will be EntityTypes.Job or EntityTypes.JobSeeker
	  protected EntityTypes CallingEntity
	  {
			get { return GetViewStateValue<EntityTypes>("RequestFollowUpModal:CallingEntity"); }
			set { SetViewStateValue("RequestFollowUpModal:CallingEntity", value); }
	  }

    #endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
      }

      NoteValidator.ValidationGroup = SaveButton.ValidationGroup = string.Concat(ClientID, "_RequestFollowUpModalValidation");
      RequestFollowUpModalPopup.BehaviorID = string.Concat(ClientID, "_RequestFollowUpModalBehaviour");
    }

	  /// <summary>
	  /// Shows the specified person id.
	  /// </summary>
	  /// <param name="id">The entity id.</param>
	  /// <param name="entityType">The entity type. </param>
	  public void Show(long id, EntityTypes entityType)
    {
      NoteTextBox.Text = string.Empty;
			Id = id;
		  CallingEntity = entityType;
      RequestFollowUpModalPopup.Show();
    }

    /// <summary>
    /// Handles the Clicked event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void CancelButton_Clicked(object sender, EventArgs e)
    {
      RequestFollowUpModalPopup.Hide();
    }

    /// <summary>
    /// Handles the Clicked event of the SaveButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void SaveButton_Clicked(object sender, EventArgs e)
    {
	    // Save the note
	    switch (CallingEntity)
	    {
		    case EntityTypes.JobSeeker:
			    ServiceClientLocator.CoreClient(App).SaveNoteReminder(
				    new NoteReminderDto
					    {
						    CreatedBy = App.User.UserId,
						    NoteReminderType = NoteReminderTypes.Note,
						    Text = NoteTextBox.Text,
						    EntityId = Id,
						    EntityType = EntityTypes.JobSeeker,
						    NoteType = NoteTypes.FollowUpRequest
					    }, null);

					// Flag candidate for follow up.
			    ServiceClientLocator.CandidateClient(App).FlagCandidateForFollowUp(Id);
			    OnIssuesResolved(new EventArgs());
			    RequestFollowUpModalPopup.Hide();

			    Confirmation.Show(CodeLocalise("FollowUpRequested.Header", "Follow-up added"),
			                      CodeLocalise("FollowUpRequested.Text",
                                         "The designated #CANDIDATETYPE#:LOWER(s) has/have been flagged for your follow-up."),
			                      CodeLocalise("Ok.Text", "OK"));
			    break;

		    case EntityTypes.Job:
			    ServiceClientLocator.CoreClient(App).SaveNoteReminder(
				    new NoteReminderDto
					    {
						    CreatedBy = App.User.UserId,
						    NoteReminderType = NoteReminderTypes.Note,
						    Text = NoteTextBox.Text,
						    EntityId = Id,
						    EntityType = EntityTypes.Job,
						    NoteType = NoteTypes.FollowUpRequest
					    }, null);

			    ServiceClientLocator.JobClient(App).FlagJobForFollowUp(Id);
			    OnIssuesResolved(new EventArgs());
			    RequestFollowUpModalPopup.Hide();
			    
					Confirmation.Show(CodeLocalise("FollowUpRequested.Header", "Follow-up added"),
			                      CodeLocalise("FollowUpRequested.Text",
			                                   "The selection has been flagged as requiring follow-up."),
			                      CodeLocalise("Ok.Text", "OK"));
					break;
	    }
    }

	  /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      SaveButton.Text = CodeLocalise("IssueResolutionModal.SaveButton", "Save");
      CancelButton.Text = CodeLocalise("IssueResoluti1112onModal.Cancel", "Cancel");
      NoteValidator.ErrorMessage = CodeLocalise("NoteValidationError.Text", "Please enter a follow-up reason");
      ModalTitle.Text = CodeLocalise("IssueResolutionModal.Title", "Add follow-up");
    }

    #region Page event

    public event RequestForFollowUpHandler FollowUpRequested;

    /// <summary>
    /// Raises the <see cref="E:IssuesResolved" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    public void OnIssuesResolved(EventArgs e)
    {
      var handler = FollowUpRequested;
      if (handler != null) handler(this, e);
    }

    public delegate void RequestForFollowUpHandler(object o, EventArgs e);

    #endregion
  }
}