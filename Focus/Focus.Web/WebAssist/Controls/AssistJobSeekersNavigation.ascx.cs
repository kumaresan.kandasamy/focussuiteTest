﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Focus.Web.Code;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class AssistJobSeekersNavigation : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		/// <summary>
		/// Determines whether [is assist job seekers administrator].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is assist job seekers administrator]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistJobSeekersAdministrator()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAdministrator);
		}

		/// <summary>
		/// Determines whether [has assist job seekers send messages role].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [has assist job seekers send messages role]; otherwise, <c>false</c>.
		/// </returns>
		protected bool HasAssistJobSeekersSendMessagesRole()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersSendMessages);
		}

		/// <summary>
		/// Determines whether [is message job seekers page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is message job seekers page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsMessageJobSeekersPage()
		{
			return (Page.Is(UrlBuilder.MessageJobSeekers()));
		}

		/// <summary>
		/// Determines whether [is manage job seeker lists page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is manage job seeker lists page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsManageJobSeekerListsPage()
		{
			return (Page.Is(UrlBuilder.ManageJobSeekerLists()));
		}

		/// <summary>
		/// Determines whether [is search job postings page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is search job postings page]; otherwise, <c>false</c>.
		/// </returns>
		protected  bool IsSearchJobPostingsPage()
		{
      return (Page.Is(UrlBuilder.AssistSearchJobPostings()) || Page.Is(UrlBuilder.AssistNewSearchJobPostings()) ||
		          Page.Is(UrlBuilder.AssistJobMatching()) ||
		          Page.Is(UrlBuilder.AssistJobPosting()) ||
		          Page.Is(UrlBuilder.AssistJobSearchResults()));
		}

		/// <summary>
		/// Determines whether this instance is workforce.
		/// </summary>
		/// <returns></returns>
		protected bool IsWorkforce()
		{
			return App.Settings.Theme == FocusThemes.Workforce;
		}
	}
}