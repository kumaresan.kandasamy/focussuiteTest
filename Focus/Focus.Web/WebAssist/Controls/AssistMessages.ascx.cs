﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;
using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class AssistMessages : UserControlBase
	{
		private EmailTemplateTypes? _selectedEmailTemplateType
		{
			get { return GetViewStateValue<EmailTemplateTypes?>("AssistMessages:SelectedEmailTemplateType"); }
			set { SetViewStateValue("AssistMessages:SelectedEmailTemplateType", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the EmailTemplateNameDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmailTemplateNameDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (EmailTemplateNameDropDown.SelectedIndex == 0)
			{
				_selectedEmailTemplateType = null;
				EmailTemplateEditor.Bind(new EmailTemplateDto(), new Dictionary<string, string>());
			}
			else
			{
				_selectedEmailTemplateType = EmailTemplateNameDropDown.SelectedValueToEnum<EmailTemplateTypes>();
				var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplate(_selectedEmailTemplateType.Value);
				var messageVariables = GetMessageVariables(_selectedEmailTemplateType.Value);
				EmailTemplateEditor.Bind(emailTemplate, messageVariables);
			}
		}

		/// <summary>
		/// Handles the Click event of the SaveChangesButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveChangesButton_Click(object sender, EventArgs e)
		{
			if (!_selectedEmailTemplateType.HasValue) return;

			var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplate(_selectedEmailTemplateType.Value);

			if (emailTemplate.IsNull()) emailTemplate = new EmailTemplateDto { EmailTemplateType = _selectedEmailTemplateType.Value };

			EmailTemplateEditor.UnBind(ref emailTemplate);

			ServiceClientLocator.CoreClient(App).SaveEmailTemplate(emailTemplate);

			Confirmation.Show(CodeLocalise("AssistMessageUpdatedConfirmation.Title", "Assist message updated"),
															 CodeLocalise("AssistMessageUpdatedConfirmation.Body", "The message has been updated."),
															 CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			BindEmailTemplateNameDropDown();
			EmailTemplateEditor.Bind(new EmailTemplateDto(), new Dictionary<string, string>());
		}

		/// <summary>
		/// Binds the email template name drop down.
		/// </summary>
		private void BindEmailTemplateNameDropDown()
		{
			EmailTemplateNameDropDown.Items.Clear();

			if (!App.Settings.TalentModulePresent)
			{
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.AssistAccountCreation, "Assist account creation");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.CustomMessage, "Custom message");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.JobScreeningRequired,"#CANDIDATETYPE# screening notification");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.AssistCandidateSearchAlert,"#CANDIDATETYPE# search alert");
			}
			else
			{
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.ApprovalQueueAlert, "Approval queue alert");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.ApproveEmployerAccountReferral, "Approve #BUSINESS#:LOWER account referral"); 
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.ApproveCandidateReferral,"Approve #CANDIDATETYPE#:LOWER referral"); 
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.ApprovePostingReferral, "Approve posting referral");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.AssistAccountCreation, "Assist account creation");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.AutoDeniedReferralRequest, "Auto-denied referral request");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.BusinessUnitReminder, "Business unit reminder");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.ContactEmployerAboutCandidate, "Contact #BUSINESS#:LOWER about candidate"); 
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.CustomMessage, "Custom message");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.DenyEmployerAccountReferral, "Deny #BUSINESS#:LOWER account referral");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.DenyCandidateReferral,"Deny #CANDIDATETYPE#:LOWER referral"); 
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.DenyPostingReferral, "Deny posting referral");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.EditPostingReferralConfirmation,"Edit posting referral confirmation"); 
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.EmployeeReminder, "Employee reminder");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.HoldEmployerAccountReferral, "Hold #BUSINESS#:LOWER account referral");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.HoldCandidateReferralRequest, "Hold referral request notification");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.JobReminder, "#POSTINGTYPE# reminder");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.JobSeekerReminder, "#CANDIDATETYPE# reminder"); 
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.JobScreeningRequired,"#CANDIDATETYPE# screening notification");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.AssistCandidateSearchAlert,"#CANDIDATETYPE# search alert");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.AutoDeniedExpiredPosting, "#CANDIDATETYPE# referral auto-denied on expired posting"); 
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.RecommendJobSeeker, "Recommend #CANDIDATETYPE#:LOWER to apply for job");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.PendingApprovePostingRequest, "Pending approve posting request");
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.StaffReferralNotification, "Staff referral notification to #CANDIDATETYPE#");

			}
			EmailTemplateNameDropDown.Items.AddLocalisedTopDefault("EmailTemplateName.TopDefault.NoEdit", "- select message -");
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SaveChangesButton.Text = CodeLocalise("SaveChangesButton.Text.NoEdit", "Save changes");
		}

		/// <summary>
		/// Gets the message variables.
		/// </summary>
		/// <param name="emailTemplateType">Type of the email template.</param>
		/// <returns></returns>
		private Dictionary<string, string> GetMessageVariables(EmailTemplateTypes emailTemplateType)
		{
			var dictionary = new Dictionary<string, string>();

			switch (emailTemplateType)
			{
				case EmailTemplateTypes.ApproveCandidateReferral:
					dictionary.Add(Constants.PlaceHolders.ApplicationInstructions, CodeLocalise("ApplicationInstructions.Text", "Application instructions"));
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;

				case EmailTemplateTypes.ApproveEmployerAccountReferral:
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;

				case EmailTemplateTypes.ApprovePostingReferral:
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "#POSTINGTYPE# link url"));
					break;

				case EmailTemplateTypes.AssistCandidateSearchAlert:
					dictionary.Add(Constants.PlaceHolders.MatchedCandidates, CodeLocalise("MatchedCandidates.Text", "Matched #CANDIDATETYPE#"));
					dictionary.Add(Constants.PlaceHolders.SavedSearchName, CodeLocalise("SavedSearchName.Text", "Saved search name"));
					dictionary.Add(Constants.PlaceHolders.FocusTalentUrl, CodeLocalise("FocusTalentUrl.Text", "#FOCUSTALENT# home page url"));
					break;

				case EmailTemplateTypes.DenyCandidateReferral:
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					dictionary.Add(Constants.PlaceHolders.FocusCareerUrl, CodeLocalise("FocusCareerUrl.Text", "#FOCUSCAREER# url"));
					break;

				case EmailTemplateTypes.DenyEmployerAccountReferral:
					dictionary.Add(Constants.PlaceHolders.FreeText, CodeLocalise("DenialReasonsFreeText.Text", "Denial reasons free text"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS# name"));
					break;

				case EmailTemplateTypes.HoldEmployerAccountReferral:
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS# name"));
					break;

				case EmailTemplateTypes.HoldCandidateReferralRequest:
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS# name"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					break;

				case EmailTemplateTypes.DenyPostingReferral:
					dictionary.Add(Constants.PlaceHolders.FreeText, CodeLocalise("DenialReasonsFreeText.Text", "Denial reasons free text"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					dictionary.Add(Constants.PlaceHolders.FocusTalentUrl, CodeLocalise("FocusTalentUrl.Text", "#FOCUSTALENT# home page url"));
					break;

				case EmailTemplateTypes.EditPostingReferralConfirmation:
					dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "#POSTINGTYPE# link url"));
					dictionary.Add(Constants.PlaceHolders.JobPosting, CodeLocalise("JobPosting.Text", "#POSTINGTYPE# posting"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					dictionary.Add(Constants.PlaceHolders.FocusTalentUrl, CodeLocalise("FocusTalentUrl.Text", "#FOCUSTALENT# home page url"));
					break;

				case EmailTemplateTypes.JobReminder:
					dictionary.Add(Constants.PlaceHolders.JobId, CodeLocalise("JobId.Text", "#POSTINGTYPE# id"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.Reminder, CodeLocalise("Reminder.Text", "Reminder"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;

				case EmailTemplateTypes.BusinessUnitReminder:
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
					dictionary.Add(Constants.PlaceHolders.FocusAssist, App.Settings.FocusAssistApplicationName);
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.Reminder, CodeLocalise("Reminder.Text", "Reminder"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;

				case EmailTemplateTypes.EmployeeReminder:
				case EmailTemplateTypes.JobSeekerReminder:
					dictionary.Add(Constants.PlaceHolders.FocusAssist, App.Settings.FocusAssistApplicationName);
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.Reminder, CodeLocalise("Reminder.Text", "Reminder"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;

				case EmailTemplateTypes.ContactEmployerAboutCandidate:
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.EmployeeName, CodeLocalise("EmployeeName.Text", "Employee's name"));
					dictionary.Add(Constants.PlaceHolders.MatchedCandidates, CodeLocalise("MatchedCandidates.Text", "Candidate's name"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;

				case EmailTemplateTypes.AssistAccountCreation:
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.FocusAssist, App.Settings.FocusAssistApplicationName);
					dictionary.Add(Constants.PlaceHolders.Username, CodeLocalise("Username.Text", "Username"));
					dictionary.Add(Constants.PlaceHolders.Password, CodeLocalise("Password.Text", "Password"));
					dictionary.Add(Constants.PlaceHolders.Url, CodeLocalise("Url.Text", "Url"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;

				case EmailTemplateTypes.JobScreeningRequired:
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "#POSTINGTYPE# link url"));
					dictionary.Add(Constants.PlaceHolders.FocusAssist, CodeLocalise("FocusAssist.Text", "#FOCUSASSIST#"));
			        break;

				case EmailTemplateTypes.RecommendJobSeeker:
							dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
						dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
						dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "#POSTINGTYPE# link url"));
						dictionary.Add(Constants.PlaceHolders.FocusAssist, CodeLocalise("FocusAssist.Text", "#FOCUSASSIST#"));
						dictionary.Add(Constants.PlaceHolders.FocusCareer, CodeLocalise("FocusCareer.Text", "#FOCUSCAREER#"));
						dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
						break;

				case EmailTemplateTypes.PendingApprovePostingRequest:
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderEmailAddress, CodeLocalise("SenderEmailAddress.Text", "Sender's email address"));
					dictionary.Add(Constants.PlaceHolders.SenderPhoneNumber, CodeLocalise("SenderPhoneNumber.Text", "Sender's phone number"));
					dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "#POSTINGTYPE# link url"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;

				case EmailTemplateTypes.StaffReferralNotification:
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "#POSTINGTYPE# link url"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;
			}

			return dictionary;
		}
	}
}