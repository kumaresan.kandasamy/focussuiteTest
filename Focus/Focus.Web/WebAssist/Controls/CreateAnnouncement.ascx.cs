﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Common.Extensions;
using Focus.Common;

using Framework.Core;
using System.Web;
using System.Web.UI.WebControls;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class CreateAnnouncement : UserControlBase
	{
		private List<SavedMessageTextDto> _savedMessageTexts;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the AnnouncementAudienceRadioButtonList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AnnouncementAudienceRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
		{
			var audience = AnnouncementAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();
			BindSavedAnnouncementDropDown(audience);
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the SavedAnnouncementsDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SavedAnnouncementsDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			
			AnnouncementTextbox.Text = "";
			SavedAnnouncementNameTextBox.Text = "";

			_savedMessageTexts = (SavedAnnouncementsDropDown.SelectedValueToLong() > 0) ? ServiceClientLocator.CoreClient(App).GetSavedMessageTexts(SavedAnnouncementsDropDown.SelectedValueToLong()) : null;

			if (_savedMessageTexts.IsNotNullOrEmpty())
			{
				var audience = AnnouncementAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();
				var defaultLocalisation = ServiceClientLocator.CoreClient(App).GetDefaultCulture();
				var savedMessageText = _savedMessageTexts.FirstOrDefault(x => x.LocalisationId == defaultLocalisation.Id);
				
				if (!savedMessageText.IsNull())
				{
					var savedMessage = ServiceClientLocator.CoreClient(App)
					.GetSavedMessages(audience)
					.FirstOrDefault(a => a.Id ==  savedMessageText.SavedMessageId);

				AnnouncementTextbox.Text = savedMessageText.Text;
				SavedAnnouncementNameTextBox.Text = savedMessage.IsNull()?"":savedMessage.Name;

				}

			}
		}

		/// <summary>
		/// Handles the Clicked event of the SendAnnouncementButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
		protected void SendAnnouncementButton_Clicked(object sender, EventArgs e)
		{
			var expiryDate = DateTime.Now.AddDays(7);
			var audience = AnnouncementAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();

			var text = Regex.Replace(HttpUtility.HtmlDecode(AnnouncementTextbox.TextTrimmed()), "<p>|</p>", "");

			if (audience == MessageAudiences.AllJobSeekers)
			{
				var homepageAlert = new JobSeekerHomepageAlertView
				                    	{
																Message = text,
				                    		AlertForAllCandidates = true,
				                    		ExpiryDate = expiryDate,
				                    		IsSystemAlert = true
				                    	};

				ServiceClientLocator.CandidateClient(App).CreateHomepageAlert(homepageAlert);
			}
			else if (audience == MessageAudiences.AllTalentUsers)
			{
				var homepageAlert = new HiringManagerHomepageAlertView
				{
					Message = text,
					ExpiryDate = expiryDate,
					IsSystemAlert = true
				};

				ServiceClientLocator.EmployeeClient(App).CreateHomepageAlert(homepageAlert);
			}
			else if (audience == MessageAudiences.AllAssistUsers)
			{
				var homepageAlert = new StaffHomepageAlertView
				{
					Message = text,
					AlertForAllStaff = true,
					ExpiryDate = expiryDate,
					IsSystemAlert = true
				};

				ServiceClientLocator.StaffClient(App).CreateHomepageAlert(homepageAlert);
			}

			BindSavedAnnouncementDropDown(audience);

			Confirmation.Show(CodeLocalise("AnnouncementSentConfirmation.Title", "Announcement sent"),
															 CodeLocalise("AnnouncementSentConfirmation.Body", "Your announcement has been sent to the selected audience."),
															 CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Handles the Clicked event of the SaveAnnouncementButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveAnnouncementButton_Clicked(object sender, EventArgs e)
		{
			var messageTexts = new List<SavedMessageTextDto>();

			var audience = AnnouncementAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();

			// Add the default message text, set the LocalisationId to zero and the service will replace it with the correct one
			messageTexts.Add(new SavedMessageTextDto
										{
											SavedMessageId = 0,
											LocalisationId = 0,
											Text =  HttpUtility.HtmlDecode(AnnouncementTextbox.TextTrimmed())
										});

			var message = new SavedMessageDto
											{
												Audience = audience,
												Name = SavedAnnouncementNameTextBox.TextTrimmed(),
												UserId = App.User.UserId
											};

			ServiceClientLocator.CoreClient(App).SaveMessageTexts(message, messageTexts);

			// Rebind SavedAnnouncementDropDown to show new value
			BindSavedAnnouncementDropDown(audience);

			
			Confirmation.Show(CodeLocalise("AnnouncementSavedConfirmation.Title", "Announcement saved"),
															 CodeLocalise("AnnouncementSavedConfirmation.Body", "The announcement has been saved."),
															 CodeLocalise("CloseModal.Text", "OK"));
		}

		#region Bind and Unbind methods

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			AnnouncementTextbox.Text = "";
			SavedAnnouncementNameTextBox.Text = "";
			BindAnnouncementAudienceRadioButtonList();
			AnnouncementAudienceRadioButtonList.SelectedIndex = 0;
			BindSavedAnnouncementDropDown(MessageAudiences.AllJobSeekers);
			
		}

		/// <summary>
		/// Binds the announcement audience radio button list.
		/// </summary>
		private void BindAnnouncementAudienceRadioButtonList()
		{
			AnnouncementAudienceRadioButtonList.Items.Clear();

      AnnouncementAudienceRadioButtonList.Items.AddEnum(MessageAudiences.AllJobSeekers, HtmlLocalise("Audience.Jobseeker.Label", "#CANDIDATETYPES#"));
      if (App.Settings.TalentModulePresent)
				AnnouncementAudienceRadioButtonList.Items.AddEnum(MessageAudiences.AllTalentUsers, HtmlLocalise("Audience.Employers.Label", "Hiring Managers"));
      AnnouncementAudienceRadioButtonList.Items.AddEnum(MessageAudiences.AllAssistUsers, HtmlLocalise("Audience.Staff.Label", "Staff members"));

			AnnouncementAudienceRadioButtonList.SelectedIndex = 0;
		}

		/// <summary>
		/// Binds the saved announcement drop down.
		/// </summary>
		/// <param name="audience">The audience.</param>
		private void BindSavedAnnouncementDropDown(MessageAudiences audience)
		{
			SavedAnnouncementsDropDown.DataSource = ServiceClientLocator.CoreClient(App).GetSavedMessages(audience);
			SavedAnnouncementsDropDown.DataValueField = "Id";
			SavedAnnouncementsDropDown.DataTextField = "Name";
			SavedAnnouncementsDropDown.DataBind();
			SavedAnnouncementsDropDown.Items.AddLocalisedTopDefault("SavedMessagesDropDownList.TopDefault.NoEdit", "New announcement");
			AnnouncementTextbox.Text = "";
			SavedAnnouncementNameTextBox.Text = "";
		}

		#endregion

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SendAnnouncementButton.Text = CodeLocalise("SendAnnouncementButton.Text.NoEdit", "Send");
			SaveAnnouncementButton.Text = CodeLocalise("Save.Label.NoEdit", "Save announcement");
			AnnouncementRequired.ErrorMessage = AnnouncementRequiredSave.ErrorMessage = CodeLocalise("AnnouncementRequired.ErrorMessage", "Announcement text is required");
			SavedAnnouncementNameRequired.ErrorMessage = CodeLocalise("SavedAnnouncementNameRequired.ErrorMessage", "Announcement name is required");
		}
	}
}