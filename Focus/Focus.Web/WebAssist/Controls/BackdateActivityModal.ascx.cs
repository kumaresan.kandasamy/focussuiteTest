﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Common;
using Focus.Core;
#endregion

namespace Focus.Web.WebAssist.Controls
{
    public partial class BackdateActivityModal : UserControlBase
    {
        private static long ActivityId;
        private static string ActivityTypeId;
        private ActivityType ActivityUserType;

        private static long BLUserId;
        private static string BLActionType;
        private static DateTime BLActionedOn;

        public string NewServiceDateRequiredErrorMessage, NewServiceDateTextErrorMessage, NewServiceDateValidate;

        public long BackdateMaxDays;

        public void LocaliseUI()
        {
            NewServiceDateRequiredErrorMessage = CodeLocalise("NewServiceDateRequiredErrorMessage.ErrorMessage", "Please enter a date.");
            NewServiceDateTextErrorMessage = CodeLocalise("NewServiceDateTextErrorMessage.ErrorMessage", "Please enter valid date.");

            NewServiceDateValidate = CodeLocalise("NewServiceDateValidate.ErrorMessage", "The backdate entry cannot be a date in the future.");
        }

        protected void BackdateButton_Click(object sender, EventArgs e)
        {
            bool backdateFlag = false;

            //Jobseekeer Validation
            if (BLActionType.Equals(ActionTypes.AssignActivityToCandidate.ToString()))
            {
                var currentUserRoles = ServiceClientLocator.AccountClient(App).GetUsersRoles(App.User.UserId);
                var dbStaffActivityUpdateSettings = ServiceClientLocator.StaffClient(App).GetBackdateSettings(App.User.PersonId ?? 0);

                backdateFlag = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.BackdateAnyEntryJSActivityLogMaxDays)) != null ? true : false;
                if (backdateFlag)
                    backdateFlag = dbStaffActivityUpdateSettings.BackdateAnyEntryJSActivityLogMaxDays != null ? (BLActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.BackdateAnyEntryJSActivityLogMaxDays))) : false;
                if (!backdateFlag && (BLUserId == App.User.UserId))
                {
                    backdateFlag = currentUserRoles.Where(x => x.Key.Contains(Constants.RoleKeys.BackdateMyEntriesJSActivityLogMaxDays)) != null ? true : false;
                    if (backdateFlag)
                        backdateFlag = dbStaffActivityUpdateSettings.BackdateMyEntriesJSActivityLogMaxDays != null ? (BLActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.BackdateMyEntriesJSActivityLogMaxDays))) : false;
                }
            }

            //Hiring Manager Validation
            if (!backdateFlag)
                if (BLActionType.Equals(ActionTypes.AssignActivityToEmployee.ToString()))
                {
                    var currentUserRoles = ServiceClientLocator.AccountClient(App).GetUsersRoles(App.User.UserId);
                    var dbStaffActivityUpdateSettings = ServiceClientLocator.StaffClient(App).GetBackdateSettings(App.User.PersonId ?? 0);

                    backdateFlag = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.BackdateAnyEntryHMActivityLogMaxDays)) != null ? true : false;
                    if (backdateFlag)
                        backdateFlag = dbStaffActivityUpdateSettings.BackdateAnyEntryHMActivityLogMaxDays != null ? (BLActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.BackdateAnyEntryHMActivityLogMaxDays))) : false;
                    if (!backdateFlag && (BLUserId == App.User.UserId))
                    {
                        backdateFlag = currentUserRoles.Where(x => x.Key.Contains(Constants.RoleKeys.BackdateMyEntriesHMActivityLogMaxDays)) != null ? true : false;
                        if (backdateFlag)
                            backdateFlag = dbStaffActivityUpdateSettings.BackdateMyEntriesHMActivityLogMaxDays != null ? (BLActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.BackdateMyEntriesHMActivityLogMaxDays))) : false;
                    }
                }

            if (backdateFlag)
            {
                if (!string.Equals(NewServiceTextBox.Text, EmptyDateString) && Page.IsValid)
                {

                    var activityDate = DateTime.Parse(NewServiceTextBox.Text);

                    ServiceClientLocator.CoreClient(App).BackdateActionEvent(ActivityId, activityDate, ActivityTypeId);
                    TriggerActivityBackdated(new ActivityBackdatedEventArgs(true));
                    NewServiceTextBox.Text = String.Empty;
                    BackdateModalPopup.Hide();
                }
            }
            //redirect to the Activity log page to avoid "double submit problem"
            Response.Redirect(Request.RawUrl);
        }

        protected void BackdateCancelButton_Click(object sender, EventArgs e)
        {
            NewServiceTextBox.Text = string.Empty;
            BackdateModalPopup.Hide();
        }

        public void show(long activityId, DateTime actionEventDate, ActivityType activityType, string activityTypeId, long lUserId, string lActionType, DateTime lActionedOn)
        {
            ActivityId = activityId;
            ActivityTypeId = activityTypeId;
            ActivityUserType = activityType;
            BLUserId = lUserId;
            BLActionType = lActionType;
            BLActionedOn = lActionedOn;
            LocaliseUI();

            CurrentServiceDateLabel.DefaultText = actionEventDate.ToString("MM/dd/yyyy");

            //Clear the date in the New Service Date textbox.
            NewServiceTextBox.Text = String.Empty;
            BackdateModalPopup.Show();
        }

        #region Activity Backdate Event Handler
        public event ActivityBackdateHandler ActivityBackdated;
        public delegate void ActivityBackdateHandler(object sender, ActivityBackdatedEventArgs eventargs);

        protected virtual void TriggerActivityBackdated(ActivityBackdatedEventArgs args)
        {
            if (ActivityBackdated != null)
                ActivityBackdated(this, args);
        }

        public class ActivityBackdatedEventArgs : EventArgs
        {
            public readonly bool isBackdated;

            public ActivityBackdatedEventArgs(bool backdated)
            {
                isBackdated = backdated;
            }
        }
        #endregion
    }
}
