﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Common.Extensions;
using Focus.Common;

#endregion


namespace Focus.Web.WebAssist.Controls
{
	public partial class JobLookupModal : UserControlBase
	{
		public delegate void OkCommandHandler(object sender, JobLookupEventArgs eventArgs);
		public event OkCommandHandler OkCommand;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				BindCreationDates();				
			}
		}

		/// <summary>
		/// Shows the specified job seeker search view.
		/// </summary>
		/// <param name="top">The top.</param>
		public void Show(int top = 210)
		{
			ModalPopup.Y = top;
			ResetAndShowModal();
		}

		#region Events

		/// <summary>
		/// Handles the Clicked event of the GoButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void GoButton_Clicked(object sender, EventArgs e)
		{
			var jobTitle = JobTitleTextBox.TextTrimmed();
			var employerName = EmployerNameTextBox.TextTrimmed();
			var fromDate = GetDate(FromMonthDropDownList, FromYearDropDownList);
			var toDate = GetDate(ToMonthDropDownList, ToYearDropDownList, true);

			BindJobs(ServiceClientLocator.JobClient(App).LookupJob(jobTitle, employerName, fromDate, toDate, JobStatuses.Active));
		}

		/// <summary>
		/// Handles the Clicked event of the GoJobIdButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void GoJobIdButton_Clicked(object sender, EventArgs e)
		{
			int jobId;
			int.TryParse(JobIdTextBox.TextTrimmed(), out jobId);

			BindJobs(ServiceClientLocator.JobClient(App).LookupJob(jobId, JobStatuses.Active));
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Handles the Clicked event of the OkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OkButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();

			var dataItem = (from item in JobsList.Items
											let radio = (HtmlInputRadioButton)item.FindControl("SelectorRadioButton")
											where radio.Checked
											select item).FirstOrDefault();

			var jobId = long.Parse(JobsList.DataKeys[dataItem.DataItemIndex]["JobId"].ToString());
			var jobTitle = JobsList.DataKeys[dataItem.DataItemIndex]["JobTitle"].ToString();
      var url = UrlBuilder.CareerInviteToJobPosting(ServiceClientLocator.JobClient(App).GetJobLensPostingId(jobId));

			OnOkCommand(new JobLookupEventArgs(jobId, jobTitle, url));
		}

		/// <summary>
		/// Handles the Clicked event of the SearchAgainLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SearchAgainLinkButton_Clicked(object sender, EventArgs e)
		{
			ResetAndShowModal();
		}

		/// <summary>
		/// Raises the <see cref="E:OkCommand"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="Focus.Web.WebAssist.Controls.JobLookupEventArgs"/> instance containing the event data.</param>
		protected virtual void OnOkCommand(JobLookupEventArgs eventArgs)
		{
			if (OkCommand != null)
				OkCommand(this, eventArgs);
		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Binds the creation dates.
		/// </summary>
		private void BindCreationDates()
		{
			FromMonthDropDownList.Items.Clear(); FromYearDropDownList.Items.Clear();
			ToMonthDropDownList.Items.Clear(); ToYearDropDownList.Items.Clear();

			var months = new[] { CodeLocalise("Global.January.Label", "January"), CodeLocalise("Global.February.Label", "February"), CodeLocalise("Global.March.Label", "March"),
													 CodeLocalise("Global.April.Label", "April"), CodeLocalise("Global.May.Label", "May"), CodeLocalise("Global.June.Label", "June"),
													 CodeLocalise("Global.July.Label", "July"), CodeLocalise("Global.August.Label", "August"), CodeLocalise("Global.September.Label", "September"),
													 CodeLocalise("Global.October.Label", "October"), CodeLocalise("Global.November.Label", "November"), CodeLocalise("Global.December.Label", "December")};

			for (var i = 0; i < 12; i++)
			{
				FromMonthDropDownList.Items.Add(new ListItem(months[i], (i + 1).ToString()));
				ToMonthDropDownList.Items.Add(new ListItem(months[i], (i + 1).ToString()));
			}

			for (var i = 2005; i <= DateTime.Now.Year; i++)
			{
				FromYearDropDownList.Items.Add(new ListItem((i).ToString(), (i).ToString()));
				ToYearDropDownList.Items.Add(new ListItem((i).ToString(), (i).ToString()));
			}

			FromMonthDropDownList.Items.AddLocalisedTopDefault("Month.TopDefault", "month");
			FromYearDropDownList.Items.AddLocalisedTopDefault("Year.TopDefault", "year");

			ToMonthDropDownList.Items.AddLocalisedTopDefault("Month.TopDefault", "month");
			ToYearDropDownList.Items.AddLocalisedTopDefault("Year.TopDefault", "year");
		
		}

		/// <summary>
		/// Binds the jobs.
		/// </summary>
		/// <param name="jobs">The jobs.</param>
		private void BindJobs(List<JobLookupView> jobs)
		{
			JobsList.DataSource = jobs;
			JobsList.DataBind();

			SearchPanel.Visible = false;
			SearchResultsPanel.Visible = true;

			ResultCount.Text = (jobs.Count > 0 ? CodeLocalise("ResultCount.Text", "{0} found", jobs.Count.ToString()) : "");
      // Don't show OK button if no jobs are available
      OkButton.Visible = jobs.Count > 0;

			ModalPopup.Show();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SearchCancelButton.Text = SearchResultsCancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			GoButton.Text = GoJobIdButton.Text = CodeLocalise("Global.Go.Button", "Go");
      OkButton.Text = CodeLocalise("OkButton.Text", "Contact #CANDIDATETYPE#:LOWER(s) about this job");

			SearchAgainLinkButton.Text = CodeLocalise("SearchAgainLinkButton.Text", "Search for another job");

			JobsListValidator.ErrorMessage = CodeLocalise("JobsListValidator.ErrorMessage", "A job must be selected") + "<br/>";
			SearchTypeOneValidator.ErrorMessage = CodeLocalise("SearchTypeOneValidator.ErrorMessage", "A job title, #BUSINESS#:LOWER or creation date must be supplied") + "<br/>";
			SearchTypeOneFromToValidator.ErrorMessage = CodeLocalise("SearchTypeOneFromToValidator.ErrorMessage", "The creation from and to dates are invalid") + "<br/>";
			SearchTypeTwoValidator.ErrorMessage = CodeLocalise("SearchTypeTwoValidator.ErrorMessage", "A numeric job id must be supplied") + "<br/>";
		}

		/// <summary>
		/// Gets the date.
		/// </summary>
		/// <param name="monthList">The month drop down list.</param>
		/// <param name="yearList">The year drop down list.</param>
		/// <param name="isLastDay">if set to <c>true</c> [is last day].</param>
		/// <returns></returns>
		private static DateTime? GetDate(ListControl monthList, ListControl yearList, bool isLastDay = false)
		{
			DateTime? result = null;

			int month, year;

			int.TryParse(monthList.SelectedValue, out month);
			int.TryParse(yearList.SelectedValue, out year);

			if (month > 0 && month < 13)
			{
				result = new DateTime(year, month, 1);
				if (isLastDay) result = (result.Value.AddMonths(1).AddDays(-1));
			}

			return result;
		}

		/// <summary>
		/// Resets the and show modal.
		/// </summary>
		private void ResetAndShowModal()
		{
			SearchPanel.Visible = true;
			SearchResultsPanel.Visible = false;

			JobTitleTextBox.Text = EmployerNameTextBox.Text = JobIdTextBox.Text = "";
			FromMonthDropDownList.SelectedIndex = FromYearDropDownList.SelectedIndex = 0;
			ToMonthDropDownList.SelectedIndex = ToYearDropDownList.SelectedIndex = 0;

			ModalPopup.Show();
		}

		#endregion
	}

	#region Job Lookup Event Args

	public class JobLookupEventArgs : EventArgs
	{
		public long JobId { get; set; }
		public string JobTitle { get; set; }
		public string Url { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="JobLookupEventArgs"/> class.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="url">The URL.</param>
		public JobLookupEventArgs(long jobId, string jobTitle, string url)
		{
			JobId = jobId;
			JobTitle = jobTitle;
			Url = url;
		}
	}

	#endregion
}