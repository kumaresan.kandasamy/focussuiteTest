﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Web.Code;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class TabNavigation : UserControlBase
	{
		private long _jobSeekerId = 0;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsDashboardPage()) TabDashboard.Attributes.Add("class", "active");
			if (IsJobSeekersPage()) TabJobSeekers.Attributes.Add("class", "active");
			if (IsEmployersPage()) TabEmployers.Attributes.Add("class", "active");
			if (IsQueuesPage()) TabApprovalQueues.Attributes.Add("class", "active");
			if (IsManagePage()) TabManage.Attributes.Add("class", "active");
      if (IsReportingPage()) TabReporting.Attributes.Add("class", "active");

			// If the user does not have rights to an area hide the tab
			if (!App.User.IsAssistUserAndCanViewJobSeekers()) TabJobSeekers.Visible = false;
			if (!App.User.IsAssistUserAndCanViewEmployers() || !App.Settings.TalentModulePresent) TabEmployers.Visible = false;
			if (!App.User.IsAssistUserAndCanViewQueues()) TabApprovalQueues.Visible = false;

			TabReporting.Visible = App.Settings.ReportingEnabled && App.User.IsAssistUserAndCanViewReports();

			// If the instance of Focus Suite does not contain Focus Talent, hide the relevant tabs and make the Student tab the default
			TabDashboard.Visible = App.Settings.TalentModulePresent;

		}

		/// <summary>
		/// Determines whether [is dashboard page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is dashboard page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsDashboardPage()
		{
			return Page.Is(UrlBuilder.AssistDashboard()) || Page.Is(UrlBuilder.AssistDashboardWithAction(string.Empty));
		}

		/// <summary>
		/// Determines whether [is job seekers page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is job seekers page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsJobSeekersPage()
		{
			if (Page.RouteData.Values.ContainsKey("jobseekerid") && !IsPostBack)
			{
				long id;
				Int64.TryParse(Page.RouteData.Values["jobseekerid"].ToString(), out id);
				_jobSeekerId = id;
			}
			return (Page.Is(UrlBuilder.AssistJobSeekers()) || Page.Is(UrlBuilder.MessageJobSeekers()) || Page.Is(UrlBuilder.ManageJobSeekerLists()) ||
							Page.Is(UrlBuilder.AssistJobSeekerProfileForCompareOnly()) || Page.Is((UrlBuilder.AssistNewSearchJobPostings(_jobSeekerId))) || 
							Page.Is(UrlBuilder.AssistJobSearchResults(_jobSeekerId)));
		}

		/// <summary>
		/// Determines whether [is employers page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is employers page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsEmployersPage()
		{
      return (Page.Is(UrlBuilder.AssistEmployers()) || Page.Is(UrlBuilder.MessageEmployers()) || Page.Is(UrlBuilder.JobOrderDashboard()) || Page.Is(UrlBuilder.JobDevelopment()) ||
							Page.Is(UrlBuilder.AssistJobView()) || Page.Is(UrlBuilder.AssistJobWizard()) || Page.Is(UrlBuilder.AssistRegisterEmployer()) || Page.Is(UrlBuilder.AssistPoolForPathCompareOnly()) ||
              Page.Is(UrlBuilder.AssistEmployerProfileForCompareOnly()) || Page.Is(UrlBuilder.AssistHiringManagerProfileForCompareOnly()) || Page.Is(UrlBuilder.PostingEmployerContact()));
		}

		/// <summary>
		/// Determines whether [is queues page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is queues page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsQueuesPage()
		{
			return (Page.Is(UrlBuilder.JobSeekerReferrals()) || Page.Is(UrlBuilder.EmployerReferrals()) || Page.Is(UrlBuilder.PostingReferrals()) || Page.Is(UrlBuilder.JobSeekerReferral(null)) || 
							Page.Is(UrlBuilder.EmployerReferral(null)) || Page.Is(UrlBuilder.PostingReferral(null)));
		}

		/// <summary>
		/// Determines whether [is manage page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is manage page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsManagePage()
		{
      return (Page.Is(UrlBuilder.ManageSystemDefaults()) 
				   || Page.Is(UrlBuilder.ManageStaffAccounts()) 
					 || Page.Is(UrlBuilder.ManageAnnouncements())
					 || Page.Is(UrlBuilder.AssistFeedback())
					 || Page.Is(UrlBuilder.CaseManagementErrors())
					 || Page.Is(UrlBuilder.ManageDocuments()));
		}
		
    /// <summary>
    /// Determines whether [is reporting page].
    /// </summary>
    /// <returns>
    /// 	<c>true</c> if [is reporting page]; otherwise, <c>false</c>.
    /// </returns>
    private bool IsReportingPage()
    {
      return Page.Is(UrlBuilder.ReportingForPathCompareOnly());
    }
	}
}