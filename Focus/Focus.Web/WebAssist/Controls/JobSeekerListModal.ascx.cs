﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.Core;
using Focus.Core.Views;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class JobSeekerListModal : UserControlBase
	{
		private const long AddNewValue = -1;
    private const long NoListValue = -2;

		#region Candidate Details

		/// <summary>
		/// Gets or sets the candidate search id.
		/// </summary>
		/// <value>The candidate search id.</value>
		protected JobSeekerView[] JobSeekers
		{
			get { return GetViewStateValue<JobSeekerView[]>("JobSeekerListModal:JobSeeker"); }
			set { SetViewStateValue("JobSeekerListModal:JobSeeker", value); }
		}

		#endregion

		public delegate void OkCommandHandler(object sender, EventArgs eventArgs);
		public event OkCommandHandler OkCommand;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				BindListsList();
			}
		}

		/// <summary>
		/// Raises the <see cref="E:OkCommand"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void OnOkCommand(EventArgs eventArgs)
		{
			if (OkCommand != null)
				OkCommand(this, eventArgs);
		}

		/// <summary>
		/// Shows the specified job seeker search view.
		/// </summary>
		/// <param name="jobSeekers">The job seekers.</param>
		public void Show(JobSeekerView[] jobSeekers)
		{
			JobSeekers = jobSeekers;

			BuildTitleAndButton();

            // KRP - 16.May.2017 
            // FVN-4613 - Rebind the lists as we might have deleted one
            BindListsList();
            
            // Reset inputs
		  NewListTextBox.Text = string.Empty;
		  ListsDropDownList.SelectedIndex = 0;
			
			ModalPopup.Show();
		}

    /// <summary>
    /// Builds the title and button which depend on number of job seekers
    /// </summary>
	  public void BuildTitleAndButton()
	  {
      if (JobSeekers.Length == 1)
      {
        TitleLabel.Text = CodeLocalise("Title.Text", "Add '{0} {1}' to list", JobSeekers[0].FirstName, JobSeekers[0].LastName);
        AddButton.Text = CodeLocalise("AddButton.Text", "Add #CANDIDATETYPE#:LOWER to list");
      }
      else
      {
        TitleLabel.Text = CodeLocalise("Title.Text", "Add {0} #CANDIDATETYPES#:LOWER to list", JobSeekers.Length);
        AddButton.Text = CodeLocalise("AddButton.Text", "Add #CANDIDATETYPES#:LOWER to list");
      }
	  }

    /// <summary>
    /// Sets the job seeker view.
    /// </summary>
    /// <param name="jobSeekers">The job seekers.</param>
    public void SetJobSeekerView(JobSeekerView[] jobSeekers)
    {
      JobSeekers = jobSeekers;
    }

		/// <summary>
		/// Handles the Clicked event of the AddButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddButton_Clicked(object sender, EventArgs e)
		{
			var listId = ListsDropDownList.SelectedValueToLong(AddNewValue);
			string addError;
      if (listId == NoListValue)
      {
        ListsDropDownListRequiredValidator.IsValid = false;
        return;
      }

			if (listId == AddNewValue)
			{
				var newListName = NewListTextBox.TextTrimmed();
				addError = ServiceClientLocator.CandidateClient(App).AddJobSeekersToList(ListTypes.AssistJobSeeker, newListName, JobSeekers);
			}
			else
				addError = ServiceClientLocator.CandidateClient(App).AddJobSeekersToList(ListTypes.AssistJobSeeker, listId, JobSeekers);

			ModalPopup.Hide();

			if (addError.IsNullOrEmpty())
			{
        if (JobSeekers.Length > 1)
        {
          Confirmation.Show(CodeLocalise("JobSeekersAddedToList.Title", "#CANDIDATETYPES# added to list"),
                            CodeLocalise("JobSeekersAddedToList.Body", "Thank you. The {0} #CANDIDATETYPES#:LOWER were added to the designated list.", JobSeekers.Length.ToString(CultureInfo.CurrentUICulture)),
                            CodeLocalise("Global.Ok.Text", "OK"));
        }
        else
        {
          Confirmation.Show(CodeLocalise("JobSeekerAddedToList.Title", "#CANDIDATETYPE# added to list"),
                            CodeLocalise("JobSeekerAddedToList.Body", "Thank you. '{0} {1}' was added to the designated list.", JobSeekers[0].FirstName, JobSeekers[0].LastName),
														CodeLocalise("Global.Ok.Text", "OK"));
        }

				// Rebind the Lists as we might have added a new one
				BindListsList();
			}
			else
        Confirmation.Show(CodeLocalise("JobSeekerAddedToListError.Title", "Unable to add #CANDIDATETYPE#:LOWER to List"),
                          CodeLocalise("JobSeekerAddedToListError.Body", "Unfortunately we were unable to add the #CANDIDATETYPE#:LOWER to the list for the following reason(s):<br/><br/>{0}", addError),
													CodeLocalise("Global.Ok.Text", "OK"));

			OnOkCommand(new EventArgs());
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Binds the lists list.
		/// </summary>
		private void BindListsList()
		{
			ListsDropDownList.Items.Clear();

			ListsDropDownList.DataSource = ServiceClientLocator.CandidateClient(App).GetJobSeekerLists(ListTypes.AssistJobSeeker);
			ListsDropDownList.DataValueField = "Id";
			ListsDropDownList.DataTextField = "Text";
			ListsDropDownList.DataBind();

      ListsDropDownList.Items.AddLocalisedTopDefault("JobSeekerListsDropDown.TopDefault", "- select list -", NoListValue.ToString());
			ListsDropDownList.Items.Add(new ListItem(CodeLocalise("JobSeekerListsDropDown.NewList", "New list"), AddNewValue.ToString()));
      ListsDropDownListRequiredValidator.InitialValue = NoListValue.ToString();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
		  NewListTextBoxValidator.ErrorMessage = CodeLocalise("NewListTextBoxValidator.ErrorMessage", "List name is mandatory");
      ListsDropDownListRequiredValidator.ErrorMessage = CodeLocalise("ListsDropDownListRequiredValidator.ErrorMessage", "Job seeker list not specified");
		}
	}
}
