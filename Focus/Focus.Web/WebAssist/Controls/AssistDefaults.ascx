﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssistDefaults.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.AssistDefaults" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ImageUploader.ascx" tagname="ImageUploader" tagprefix="uc" %>

<div style="width:100%"><asp:Button ID="SaveChangesTopButton" runat="server" Text="Save changes" class="button1 right" OnClick="SaveChangesButton_Click" /></div>
<br />
<br />


<asp:Panel ID="JobDevelopmentFlagsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="JobDevelopmentFlagsHeaderImage" runat="server" AlternateText="." />
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobDevelopmentFlags.Label", "Business Development defaults")%></a></span>
				</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="RecentPlacementFlagsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table style="width:100%;" role="presentation">
				<tr>
					<td><asp:CheckBox runat="server" id="RecentPlacementCheckBox" TextAlign="Right" Checked="True" Enabled="False" />
						&nbsp;&nbsp;
						<%= HtmlLabel("RecenPlacementsFlag.Label", "Recent placements to Matched #CANDIDATETYPE#")%>
					</td> 
				</tr>
				<tr>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<%= HtmlLabel("RecenPlacementsDropDown.Label", "Show matches to placements made in the last ")%>
						&nbsp;&nbsp;	
						<asp:DropDownList runat="server" ID="PlacementTimeframeDropDown" />
						&nbsp;&nbsp;
						<%= HtmlLabel("RecenPlacementsDropDownNext.Label", " days")%>
					</td>
				</tr>
				<tr>
					<td><br /></td>
				</tr>
			</table>	
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="JobDevelopmentCollapsiblePanelExtender" runat="server" TargetControlID="RecentPlacementFlagsPanel" ExpandControlID="JobDevelopmentFlagsHeaderPanel" 
																CollapseControlID="JobDevelopmentFlagsHeaderPanel" Collapsed="true" ImageControlID="JobDevelopmentFlagsHeaderImage" 
																SuppressPostBack="true" />
<asp:Panel ID="ColourDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="ColourDefaultsHeaderImage" runat="server" AlternateText="Color defaults" Width="22" Height="22"/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ColourDefaults.Label", "Color defaults")%></a></span>
				</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="ColourDefaultsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table role="presentation">
				<tr>
					<td><%= HtmlLabel("LightColour.Label", "Light color")%></td>
					<td>
						<asp:Textbox ID="LightColourTextBox" runat="server" ClientIDMode="Static" MaxLength="6" Width="100" />
						<asp:RequiredFieldValidator runat="server" ID="LightColourValidator" ValidationGroup="AssistDefaults" SetFocusOnError="true" Display="Dynamic" CssClass="error" ControlToValidate="LightColourTextBox" />		
					</td>
				</tr>
				<tr>
					<td><%= HtmlLabel("DarkColour.Label", "Dark color")%></td>
					<td>
						<asp:Textbox ID="DarkColourTextBox" runat="server" ClientIDMode="Static" MaxLength="6" Width="100" />
						<asp:RequiredFieldValidator runat="server" ID="DarkColourValidator" ValidationGroup="AssistDefaults" SetFocusOnError="true" Display="Dynamic" CssClass="error" ControlToValidate="DarkColourTextBox" />		
					</td>
				</tr>
				<tr>
					<td></td>
					<td><asp:Button ID="ResetColoursButton" runat="server" class="button3" OnClick="ResetColoursButton_Click" /></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
			</table>	
		</div>
	</div>		
</asp:Panel>
<act:CollapsiblePanelExtender ID="ColourDefaultsPanelExtender" runat="server" TargetControlID="ColourDefaultsPanel" ExpandControlID="ColourDefaultsHeaderPanel" 
																CollapseControlID="ColourDefaultsHeaderPanel" Collapsed="true" ImageControlID="ColourDefaultsHeaderImage" 
																SuppressPostBack="true" />
<asp:PlaceHolder runat="server" ID="JobOrderDefaultsPlaceHolder">
<asp:Panel runat="server" ID="JobOrderDefaultsHeaderPanel" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="JobOrderDefaultsHeaderImage" runat="server" AlternateText="." />
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobOrderDefaults.Label", "#EMPLOYMENTTYPE# flag defaults")%></a></span></td>
		</tr>
	</table>
 </asp:Panel>
 
 <asp:Panel ID="JobOrderDefaultsPanel" runat="server">
	 <div class="singleAccordionContentWrapper">
			<div class="singleAccordionContent">
				<div style="float:left;width:250px;">
					<%=HtmlLocalise("JobOrderDefaultsHeader.Label", "Enable these #EMPLOYMENTTYPE#:LOWER flags")%>
				</div>
				<div style="float:left;">
					<table role="presentation">
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagLowMatchesCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagLowMatchesLabel" AssociatedControlID="FlagLowMatchesCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagLowMatchesCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagLowMatches.Label1", "Flag if #EMPLOYMENTTYPE#:LOWER has fewer than")%>
								<asp:TextBox runat="server" ID="FlagLowMatchesCountTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagLowMatches.Label2", "matches at")%>
								<asp:TextBox runat="server" ID="FlagLowMatchesMinimumStarsTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagLowMatches.Label3", "stars or above in")%>
								<asp:DropDownList runat="server" ID="FlagLowMatchesPeriodDaysDropDown" MaxLength="4" width="45"></asp:DropDownList>
								<%=HtmlLocalise("FlagLowMatches.Label3", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagLowMatchesCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagLowMatchesCountValidator" runat="server" ControlToValidate="FlagLowMatchesCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagLowMatchesCountRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagLowMatchesCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagLowMatchesCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagLowMatchesMinimumStarsValidator" runat="server" ControlToValidate="FlagLowMatchesMinimumStarsTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagLowMatchesMinimumStarsRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagLowMatchesMinimumStarsTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagLowReferralsCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagLowReferralsLabel" AssociatedControlID="FlagLowReferralsCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagLowReferralsCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagLowReferrals.Label1", "Flag if #EMPLOYMENTTYPE#:LOWER has fewer than")%>
								<asp:TextBox runat="server" ID="FlagLowReferralsCountTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagLowMatches.Label2", "referrals or referral requests in")%>
								<asp:TextBox runat="server" ID="FlagLowReferralsPeriodDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagLowMatches.Label3", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagLowReferralsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagLowReferralsCountValidator" runat="server" ControlToValidate="FlagLowReferralsCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagLowReferralsCountRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagLowReferralsCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagLowReferralsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagLowReferralsPeriodDaysValidator" runat="server" ControlToValidate="FlagLowReferralsPeriodDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagLowReferralsPeriodDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagLowReferralsPeriodDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagNotClickingReferralsCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagNotClickingReferralsLabel" AssociatedControlID="FlagNotClickingReferralsCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagNotClickingReferralsCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagNotClickingReferrals.Label1", "Flag #EMPLOYMENTTYPE#:LOWER if hiring contact has not clicked on referred applicants after")%>
								<asp:TextBox runat="server" ID="FlagNotClickingReferralsPeriodDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagNotClickingReferrals.Label2", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagNotClickingReferralsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagNotClickingReferralsPeriodDaysValidator" runat="server" ControlToValidate="FlagNotClickingReferralsPeriodDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagNotClickingReferralsPeriodDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagNotClickingReferralsPeriodDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagSearchWithoutInviteCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagSearchWithoutInviteLabel" AssociatedControlID="FlagSearchWithoutInviteCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagSearchWithoutInviteCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagSearchWithoutInvite.Label1", "Flag #EMPLOYMENTTYPE#:LOWER if hiring contact performs")%>
								<asp:TextBox runat="server" ID="FlagSearchWithoutInviteTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSearchWithoutInvite.Label2", "or more searches against a job, but does not send invitations to any job seekers in")%>
								<asp:TextBox runat="server" ID="FlagSearchWithoutInviteDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSearchWithoutInvite.Label3", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagSearchWithoutInviteCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSearchWithoutInviteValidator" runat="server" ControlToValidate="FlagSearchWithoutInviteTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSearchWithoutInviteRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSearchWithoutInviteTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagSearchWithoutInviteCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSearchWithoutInviteDaysValidator" runat="server" ControlToValidate="FlagSearchWithoutInviteDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSearchWithoutInviteDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSearchWithoutInviteDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagJobClosingDateRefreshedCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagJobClosingDateRefreshedLabel" AssociatedControlID="FlagJobClosingDateRefreshedCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagJobClosingDateRefreshedCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagJobClosingDateRefreshed.Label1", "Flag #EMPLOYMENTTYPE#:LOWER if the hiring contact refreshes the job closing date")%>
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagJobClosedEarlyCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagJobClosedEarlyLabel" AssociatedControlID="FlagJobClosedEarlyCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagJobClosedEarlyCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagJobClosedEarly.Label1", "Flag #EMPLOYMENTTYPE#:LOWER if hiring contact closes the job post")%>
								<asp:TextBox runat="server" ID="FlagJobClosedEarlyTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagJobClosedEarly.Label2", "days before job post is set to expire")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagJobClosedEarlyTextBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagJobClosedEarlyValidator" runat="server" ControlToValidate="FlagJobClosedEarlyTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagJobClosedEarlyRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagJobClosedEarlyTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagJobAfterHiringPeriodDaysCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagJobAfterHiringPeriodDaysLabel" AssociatedControlID="FlagJobAfterHiringPeriodDaysCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagJobAfterHiringPeriodDaysCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagJobAfterHiringPeriodDays.Label1", "Flag #EMPLOYMENTTYPE#:LOWER")%>
								<asp:TextBox runat="server" ID="FlagJobAfterHiringPeriodDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagJobAfterHiringPeriodDays.Label2", "days after a referral outcome on posting has been set to hired")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagJobAfterHiringPeriodDaysCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagJobAfterHiringPeriodDaysValidator" runat="server" ControlToValidate="FlagJobAfterHiringPeriodDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagJobAfterHiringPeriodDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagJobAfterHiringPeriodDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagContactNegativeFeedbackCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagContactNegativeFeedbackLabel" AssociatedControlID="FlagContactNegativeFeedbackCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagContactNegativeFeedbackCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagContactNegativeFeedback.Label1", "Flag when hiring contact responds to the customer service questions negatively")%>
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagContactPositiveFeedbackCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagContactPositiveFeedbackLabel" AssociatedControlID="FlagContactPositiveFeedbackCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagContactPositiveFeedbackCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagContactPositiveFeedback.Label1", "Flag when hiring contact responds to the customer service questions positively")%>
							</td>
						</tr>
					</table>
				</div>
				<div style="clear: both;"></div>
				<br />
			</div>
		</div>
  </asp:Panel>
	<act:CollapsiblePanelExtender ID="JobOrderDefaultsPanelExtender" runat="server" TargetControlID="JobOrderDefaultsPanel" ExpandControlID="JobOrderDefaultsHeaderPanel" 
																  CollapseControlID="JobOrderDefaultsHeaderPanel" Collapsed="true" ImageControlID="JobOrderDefaultsHeaderImage" 
																  SuppressPostBack="true" />
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="FlagDefaultPlaceHolder">
  <asp:Panel runat="server" ID="FlagsDefaultsHeaderPanel" CssClass="singleAccordionTitle">
	  <table class="accordionTable" role="presentation" >
		  <tr  class="multipleAccordionTitle">
			  <td>
				  <asp:Image ID="FlagDefaultsHeaderImage" runat="server" AlternateText="." />
					&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("FlagDefaults.Label", "#CANDIDATETYPE# flag defaults")%></a></span></td>
		  </tr>
	  </table>
  </asp:Panel>

  <asp:Panel ID="FlagDefaultsPanel" runat="server">
	  <div class="singleAccordionContentWrapper">
			<div class="singleAccordionContent">
				<div style="float:left;width:250px;">
					<%=HtmlLocalise("FlagDefaultsJobSeekerHeader.Label", "Enable these #CANDIDATETYPE# flags")%>
				</div>
				<div style="float:left;">
					<table role="presentation">
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagSeekerNotLoggedOnCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagSeekerNotLoggedOnLabel" AssociatedControlID="FlagSeekerNotLoggedOnCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagSeekerNotLoggedOnCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagSeekerNotLoggedOnDays.Label1", "Flag if") %>
								<asp:TextBox runat="server" ID="FlagSeekerNotLoggedOnDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotLoggedOnDays.Label2", "days since last login (self-service only)")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotLoggedOnCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotLoggedOnDaysValidator" runat="server" ControlToValidate="FlagSeekerNotLoggedOnDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotLoggedOnDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotLoggedOnDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>

						<tr>
							<td><asp:CheckBox runat="server" ID="FlagSeekerNotClickingLeadsCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagSeekerNotClickingLeadsLabel" AssociatedControlID="FlagSeekerNotClickingLeadsCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagSeekerNotClickingLeadsCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagSeekerNotClickingLeads.FlagSeekerNotClickingLeadsCountTextBoxLabel", "Flag if fewer than")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotClickingLeadsCountTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotClickingLeads.Label2", "leads viewed in")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotClickingLeadsDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotClickingLeads.Label3", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotClickingLeadsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotClickingLeadsCountValidator" runat="server" ControlToValidate="FlagSeekerNotClickingLeadsCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotClickingLeadsCountRegEx" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotClickingLeadsCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotClickingLeadsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotClickingLeadsDaysValidator" runat="server" ControlToValidate="FlagSeekerNotClickingLeadsDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotClickingLeadsDaysRegEx" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotClickingLeadsDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>

						<tr>
							<td><asp:CheckBox runat="server" ID="FlagSeekerNotSearchingJobsCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagSeekerNotSearchingJobsLabel" AssociatedControlID="FlagSeekerNotSearchingJobsCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagSeekerNotSearchingJobsCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagSeekerNotSearchingJobs.LFlagSeekerNotSearchingJobsCountTextBoxabel1", "Flag if fewer than")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotSearchingJobsCountTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotSearchingJobs.Label2", "jobs searched in")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotSearchingJobsDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotSearchingJobs.Label3", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotSearchingJobsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotSearchingJobsCountValidator" runat="server" ControlToValidate="FlagSeekerNotSearchingJobsCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotSearchingJobsCountRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotSearchingJobsCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotSearchingJobsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotSearchingJobsDaysValidator" runat="server" ControlToValidate="FlagSeekerNotSearchingJobsDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotSearchingJobsDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotSearchingJobsDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>

						<tr>
							<td><asp:CheckBox runat="server" ID="FlagSeekerRejectingOffersCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagSeekerRejectingOffersLabel" AssociatedControlID="FlagSeekerRejectingOffersCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagSeekerRejectingOffersCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagSeekerRejectingOffers.Label1", "Flag if")%>
								<asp:TextBox runat="server" ID="FlagSeekerRejectingOffersCountTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerRejectingOffers.Label2", "or more job offers rejected in")%>
								<asp:TextBox runat="server" ID="FlagSeekerRejectingOffersDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerRejectingOffers.Label3", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerRejectingOffersCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerRejectingOffersCountValidator" runat="server" ControlToValidate="FlagSeekerRejectingOffersCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerRejectingOffersCountRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerRejectingOffersCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerRejectingOffersCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerRejectingOffersDaysValidator" runat="server" ControlToValidate="FlagSeekerRejectingOffersDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerRejectingOffersDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerRejectingOffersDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>

						<tr>
							<td><asp:CheckBox runat="server" ID="FlagSeekerNotReportingInterviewsCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagSeekerNotReportingInterviewsLabel" AssociatedControlID="FlagSeekerNotReportingInterviewsCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagSeekerNotReportingInterviewsCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagSeekerNotReportingInterviews.Label1", "Flag if seeker did not report for")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotReportingInterviewsCountTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotReportingInterviews.Label2", "or more interviews in")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotReportingInterviewsDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotReportingInterviews.Label3", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotReportingInterviewsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotReportingInterviewsCountValidator" runat="server" ControlToValidate="FlagSeekerNotReportingInterviewsCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotReportingInterviewsCountRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotReportingInterviewsCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotReportingInterviewsCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotReportingInterviewsDaysValidator" runat="server" ControlToValidate="FlagSeekerNotReportingInterviewsDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotReportingInterviewsDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotReportingInterviewsDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>

						<tr>
							<td><asp:CheckBox runat="server" ID="FlagSeekerNotRespondingInvitesCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagSeekerNotRespondingInvitesLabel" AssociatedControlID="FlagSeekerNotRespondingInvitesCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagSeekerNotRespondingInvitesCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagSeekerNotRespondingInvites.Label1", "Flag if seeker did not respond to")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotRespondingInvitesCountTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotRespondingInvites.Label2", "or more #BUSINESS#:LOWER invitations in")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotRespondingInvitesDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotRespondingInvites.Label3", "days, with a grace period of")%>
								<asp:TextBox runat="server" ID="FlagSeekerNotRespondingInvitesGraceTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagSeekerNotRespondingInvites.Label4", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotRespondingInvitesCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotRespondingInvitesCountValidator" runat="server" ControlToValidate="FlagSeekerNotRespondingInvitesCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotRespondingInvitesCountRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotRespondingInvitesCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotRespondingInvitesCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotRespondingInvitesDaysValidator" runat="server" ControlToValidate="FlagSeekerNotRespondingInvitesDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotRespondingInvitesDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotRespondingInvitesDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagSeekerNotRespondingInvitesCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagSeekerNotRespondingInvitesGraceValidator" runat="server" ControlToValidate="FlagSeekerNotRespondingInvitesGraceTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagSeekerNotRespondingInvitesGraceRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagSeekerNotRespondingInvitesGraceTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
        
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagLowQualityMatchesCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagLowQualityMatchesLabel" AssociatedControlID="FlagLowQualityMatchesCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagLowQualityMatchesCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagLowQualityMatches.Label1", "Flag if fewer than")%>
								<asp:TextBox runat="server" ID="FlagLowQualityMatchesCountTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagLowQualityMatches.Label2", "matches of")%>
								<asp:DropDownList runat="server" ID="FlagLowQualityMatchesDropDown" />
								<%=HtmlLocalise("FlagLowQualityMatches.Label3", "or above in")%>
								<asp:TextBox runat="server" ID="FlagLowQualityMatchesDaysTextBox" MaxLength="4" width="30"></asp:TextBox>
								<%=HtmlLocalise("FlagLowQualityMatches.Label4", "days")%>
							</td>
						</tr>
						<tr data-validators="<%=FlagLowQualityMatchesCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagLowQualityMatchesCountValidator" runat="server" ControlToValidate="FlagLowQualityMatchesCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagLowQualityMatchesCountRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagLowQualityMatchesCountTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>
						<tr data-validators="<%=FlagLowQualityMatchesCheckBox.ClientID%>">
							<td></td>
							<td>
								<asp:RequiredFieldValidator ID="FlagLowQualityMatchesDaysValidator" runat="server" ControlToValidate="FlagLowQualityMatchesDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
								<asp:RegularExpressionValidator ID="FlagLowQualityMatchesDaysRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="FlagLowQualityMatchesDaysTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
							</td>
						</tr>

						<tr>
							<td><asp:CheckBox runat="server" ID="FlagLowQualityResumeCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagLowQualityResumeLabel" AssociatedControlID="FlagLowQualityResumeCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagLowQualityResumeCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagLowQualityResume.Label1", "Flag if resume contains less than")%>
								<asp:TextBox runat="server" ID="FlagLowQualityResumeWordCountTextBox"  MaxLength="4" width="30"/>
								<%=HtmlLocalise("FlagLowQualityResume.Label2", "words and/or less than")%>
								<asp:TextBox runat="server" ID="FlagLowQualityResumeSkillCountTextBox"  MaxLength="4" width="30"/>
								<%=HtmlLocalise("FlagLowQualityResume.Label2", "auto-extracted skills")%>
							</td>
						</tr>
        
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagPostHireFollowUpCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagPostHireFollowUpLabel" AssociatedControlID="FlagPostHireFollowUpCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagPostHireFollowUpCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("FlagPostHireFollowUp.Label1", "Hire reported by #BUSINESS#:LOWER, staff or job seeker")%>
							</td>
						</tr>
        	<asp:PlaceHolder id="InappropriateEmailRow" runat="server">
						<tr>
							<td><asp:CheckBox runat="server" ID="InappropriateEmailCheckBox" /></td>
							<td><asp:Label runat="server" ID="InappropriateEmailLabel" AssociatedControlID="InappropriateEmailCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=InappropriateEmailCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("InappropriateEmail.Label1", "Flag if red or yellow words have been used in job seeker email address")%>
							</td>
						</tr>
						</asp:PlaceHolder>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagPositiveFeedbackCheckbox" /></td>
							<td><asp:Label runat="server" ID="FlagPositiveFeedbackLabel" AssociatedControlID="FlagPositiveFeedbackCheckbox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagPositiveFeedbackCheckbox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("GivingPositiveFeedback.Label1", "Flag when job seeker responds to the customer service questions positively")%>
							</td>
						</tr>
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagNegativeFeedbackCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagNegativeFeedbackLabel" AssociatedControlID="FlagNegativeFeedbackCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagNegativeFeedbackCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("NegativeSurvey.Label1", "Flag when job seeker responds to the customer service questions negatively")%>
							</td>
						</tr>
						<asp:PlaceHolder id="FlagMSFWRow" runat="server">
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagPotentialMSFWCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagPotentialMSFWLabel" AssociatedControlID="FlagPotentialMSFWCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagPotentialMSFWCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise("NegativeMSFW.Label", "Flag if job seeker selects Yes to the question &quot;Have you traveled doing farm or food processing work during the past year which caused you to be away overnight from your regular home?&quot; in their resume")%>
							</td>
						</tr>
						</asp:PlaceHolder>
						<asp:PlaceHolder id="FlagExpiredAlienCertificationRow" runat="server">
						<tr>
							<td><asp:CheckBox runat="server" ID="FlagExpiredAlienCertificationCheckBox" /></td>
							<td><asp:Label runat="server" ID="FlagExpiredAlienCertificationLabel" AssociatedControlID="FlagExpiredAlienCertificationCheckBox"></asp:Label></td>
						</tr>
						<tr data-values="<%=FlagExpiredAlienCertificationCheckBox.ClientID%>">
							<td></td>
							<td>
								<%=HtmlLocalise( "FlagExpiredAlienCertification.Label", "Flag job seekers identified for having an expired alien certification registration date." )%>
							</td>
						</tr>
						</asp:PlaceHolder>
					</table>
				</div>
				<div style="clear: both;"></div>
				<br />	
			</div>
		</div>
  </asp:Panel>
  <act:CollapsiblePanelExtender ID="FlagDefaultsPanelExtender" runat="server" TargetControlID="FlagDefaultsPanel" ExpandControlID="FlagsDefaultsHeaderPanel" 
																  CollapseControlID="FlagsDefaultsHeaderPanel" Collapsed="true" ImageControlID="FlagDefaultsHeaderImage" 
																  SuppressPostBack="true" />
</asp:PlaceHolder>
<asp:PlaceHolder ID="LaborInsightPlaceHolder" runat="server" Visible="False">
  <asp:Panel ID="LaborInsightDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	  <table class="accordionTable" role="presentation">
		  <tr  class="multipleAccordionTitle">
			  <td>
				  <asp:Image ID="LaborInsightDefaultsHeaderImage" runat="server" AlternateText="."/>
					&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("LaborInsightsDefaults.Label", "Labor/Insight staff access default")%></a></span></td>
		  </tr>
	  </table>
  </asp:Panel>
  <asp:Panel runat="server" ID="LaborInsightsDefaultsPanel">
	  <div class="singleAccordionContentWrapper">
			<div class="singleAccordionContent">
				<table role="presentation">
					<tr>
						<td>
							<asp:RadioButton runat="server" ID="LaborInsightAccessYes" GroupName="LaborInsight"/>
							<%= HtmlLocalise("Global.Yes", "Yes")%>
						</td>
					</tr>
					<tr>
						<td>
							<asp:RadioButton runat="server" ID="LaborInsightAccessNo" GroupName="LaborInsight"/>
							<%= HtmlLocalise("Global.No", "No")%>
						</td>
					</tr>
				</table>
			</div>
		</div>
  </asp:Panel>
  <act:CollapsiblePanelExtender ID="LaborInsightDefaultsCollapsiblePanelExtender" runat="server" TargetControlID="LaborInsightsDefaultsPanel" ExpandControlID="LaborInsightDefaultsHeaderPanel" 
																CollapseControlID="LaborInsightDefaultsHeaderPanel" Collapsed="true" ImageControlID="LaborInsightDefaultsHeaderImage" 
																SuppressPostBack="true" />
</asp:PlaceHolder>													
<asp:Panel ID="LogoDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="LogoDefaultsHeaderImage" runat="server" AlternateText="Logo defaults" Width="22" Height="22"/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("LogoDefaults.Label", "Logo defaults")%></a></span></td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="LogoDefaultsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table style="width:100%;" role="presentation">
				<tr>
					<td colspan="2"><%= HtmlLocalise("LogoRequirements.Label", "Logos must be in GIF, JPG, or PNG format at 72dpi. Maximum size 300 pixels wide by 55 pixels high.")%></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
				<tr>
					<td style="width:100px; vertical-align:top;"><asp:Label ID="ApplicationLogoLabel" runat="server"><%= HtmlLabel("ApplicationLogo.Label", "Application logo")%></asp:Label></td>
					<td>
						<uc:ImageUploader ID="AssistApplicationLogoUploader" runat="server" ImageHeight="55" ImageWidth="300" />
					</td>
				</tr>
				<tr>
					<td><%= HtmlLabel("FooterLogo.Label", "Footer logo")%></td>
					<td><asp:CheckBox runat="server" id="AssistFooterLogoCheckBox" TextAlign="Right" /></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
			</table>	
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="LogoDefaultsPanelExtender" runat="server" TargetControlID="LogoDefaultsPanel" ExpandControlID="LogoDefaultsHeaderPanel" 
																CollapseControlID="LogoDefaultsHeaderPanel" Collapsed="true" ImageControlID="LogoDefaultsHeaderImage" 
																SuppressPostBack="true" />	
<asp:PlaceHolder ID="ManageActivitiesPlaceHolder" runat="server" Visible="true">
  <asp:Panel ID="ManageActivitiesHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	  <table class="accordionTable" role="presentation">
		  <tr  class="multipleAccordionTitle">
			  <td>
				  <asp:Image ID="ManageActivitiesHeaderImage" runat="server" AlternateText="."/>
					&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ManageActivitiesServices.Label", "Manage Activities/Services defaults")%></a></span></td>
		  </tr>
	  </table>
  </asp:Panel>
  <asp:Panel runat="server" ID="ManageActivitiesPanel">
  <div class="singleAccordionContentWrapper">
			<div class="singleAccordionContent">
            <table role="presentation">
            <tr>
            <td><asp:CheckBox runat="server" ID="EnableJobSeekerActivitiesCheckBox" /></td>
			<td><asp:Label runat="server" ID="EnableJobSeekerActivitiesLabel" AssociatedControlID="EnableJobSeekerActivitiesCheckBox"></asp:Label></td></tr>
            <tr>
            <td><asp:CheckBox runat="server" ID="EnableHiringManagerActivitiesCheckBox" /></td>
			<td><asp:Label runat="server" ID="EnableHiringManagerActivitiesLabel" AssociatedControlID="EnableHiringManagerActivitiesCheckBox"></asp:Label></td>
            </tr>
            </table>
            </div>
            </div>
  </asp:Panel> 
  <act:CollapsiblePanelExtender ID="ManageActivitiesCollapsiblePanelExtender" runat="server" TargetControlID="ManageActivitiesPanel" ExpandControlID="ManageActivitiesHeaderPanel" 
																CollapseControlID="ManageActivitiesHeaderPanel" Collapsed="true" ImageControlID="ManageActivitiesHeaderImage" 
																SuppressPostBack="true" /> 
  </asp:PlaceHolder>                                                             
                                                                						
<asp:Panel ID="OfficeHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="OfficeHeaderImage" runat="server" AlternateText="."/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("OfficeDefaults.Label", "Manage Offices defaults")%></a></span>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="OfficePanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table role="presentation">
				<tr>
					<td><asp:Checkbox runat="server" id="EnableOfficesCheckBox" /></td>
					 <td><%= HtmlLabel("EnableOfficesCheckBox.Label", "Enable office functionality")%></td>
				</tr>
			</table>		
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="OfficeCollapsiblePanelExtender" runat="server" TargetControlID="OfficePanel" ExpandControlID="OfficeHeaderPanel" 
																CollapseControlID="OfficeHeaderPanel" Collapsed="true" ImageControlID="OfficeHeaderImage" 
																SuppressPostBack="true" />
<asp:PlaceHolder runat="server" ID="ReferralRequestApprovalQueuePlaceHolder">
<asp:Panel ID="ReferralRequestApprovalQueueHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="ReferralRequestApprovalQueueHeaderImage" runat="server" AlternateText="."/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ReferralRequestApprovalQueue.Label", "Referral request approval queue defaults")%></a></span>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="ReferralRequestApprovalQueuePanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table role="presentation">
				<tr>
					<td><asp:RadioButton ID="JobseekersAssignOfficeRadioButton" runat="server" GroupName="AssignedOfficeFilter" />
					<%= HtmlLocalise("JobseekersAssignOffice", "Filter referrals by #CANDIDATETYPE#:LOWER's assigned office")%></td>
				</tr>
				<tr>
					<td><asp:RadioButton ID="JobPostingAssignOfficeRadioButton" runat="server" GroupName="AssignedOfficeFilter" />
					<%= HtmlLocalise("JobPostingsAssignOffice.Label", "Filter referrals by #EMPLOYMENTTYPE#:LOWER's assigned office")%></td>
				</tr>
			</table>	
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="ReferralRequestApprovalQueueExtender" runat="server" TargetControlID="ReferralRequestApprovalQueuePanel" ExpandControlID="ReferralRequestApprovalQueueHeaderPanel" 
																CollapseControlID="ReferralRequestApprovalQueueHeaderPanel" Collapsed="true" ImageControlID="ReferralRequestApprovalQueueHeaderImage" 
																SuppressPostBack="true" />

</asp:PlaceHolder>                                                                                                                              																                                   
<asp:Panel ID="SearchDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="SearchDefaultsHeaderImage" runat="server" AlternateText="."/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("SearchDefaults.Label", "Search defaults")%></a></span></td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="SearchDefaultsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table role="presentation">
				<tr>
					<td><asp:RadioButton ID="JobSeekersWithXStarsRadioButton" runat="server" GroupName="JobSeekers" />
					<%= HtmlLocalise("Stars.MinimumScore", "#CANDIDATETYPE# matching #EMPLOYMENTTYPE# with a minimum score of")%> <asp:DropDownList runat="server" ID="MinimumStarsDropDownList"/> <%= HtmlLocalise("Stars", "stars") %></td>
				</tr>
				<tr>
					<td><asp:RadioButton ID="AllJobSeekersRadioButton" runat="server" GroupName="JobSeekers" />
					<%= HtmlLocalise("All.MinimumScore", "all #CANDIDATETYPE#, regardless of match score")%></td>
				</tr>
			</table>			
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="SearchDefaultsCollapsiblePanelExtender" runat="server" TargetControlID="SearchDefaultsPanel" ExpandControlID="SearchDefaultsHeaderPanel" 
																CollapseControlID="SearchDefaultsHeaderPanel" Collapsed="true" ImageControlID="SearchDefaultsHeaderImage" 
																CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																SuppressPostBack="true" />                           
<asp:PlaceHolder ID="TextDefaultsPlaceHolder" runat="server">
   <asp:Panel ID="TextDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	  <table class="accordionTable" role="presentation">
		  <tr  class="multipleAccordionTitle">
			  <td>
				  <asp:Image ID="TextDefaultsHeaderImage" runat="server" AlternateText="."/>
					&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("TextDefaults.Label", "Text defaults")%></a></span></td>
		  </tr>
	  </table>
  </asp:Panel>
  
  <asp:Panel ID="TextDefaultsPanel" runat="server">
	  <div class="singleAccordionContentWrapper">
			<div class="singleAccordionContent">
				<table role="presentation">
					<tr>
						<th></th>
						<th><%= HtmlLabel("Singular.Label", "Singular")%></th>
						<th><%= HtmlLabel("Plural.Label", "Plural")%></th>
					</tr>
					<tr>
						<td><%= HtmlLabel("CandidateType.Label", "Candidate type")%></td>
						<td>
							<asp:Textbox ID="CandidateTypeTextBox" runat="server" ClientIDMode="Static" MaxLength="25" Width="100" />
							<br />
 							<asp:RequiredFieldValidator ID="CandidateTypeValidator" runat="server" ControlToValidate="CandidateTypeTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
						</td>
						<td>
							<asp:Textbox ID="CandidateTypePluralTextBox" runat="server" ClientIDMode="Static" MaxLength="25" Width="100" />
							<br />
 							<asp:RequiredFieldValidator ID="CandidateTypePluralValidator" runat="server" ControlToValidate="CandidateTypePluralTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
						</td>
					</tr>
          <tr>
						<td><%= HtmlLabel("CandidateType.Label", "Business terminology")%></td>
						<td>
							<asp:Textbox ID="BusinessTerminologyTextBox" runat="server" ClientIDMode="Static" MaxLength="25" Width="100" />
							<br />
 							<asp:RequiredFieldValidator ID="BusinessTerminologyRequired" runat="server" ControlToValidate="BusinessTerminologyTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
						</td>
						<td>
							<asp:Textbox ID="BusinessTerminologyPluralTextBox" runat="server" ClientIDMode="Static" MaxLength="25" Width="100" />
							<br />
 							<asp:RequiredFieldValidator ID="BusinessTerminologyPluralRequired" runat="server" ControlToValidate="BusinessTerminologyPluralTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
						</td>
					</tr>
					<tr>
						<td colspan="2"><br /></td>
					</tr>
				</table>	
			</div>
		</div>		
  </asp:Panel>
  <act:CollapsiblePanelExtender ID="TextDefaultsCollapsiblePanelExtender" runat="server" TargetControlID="TextDefaultsPanel" ExpandControlID="TextDefaultsHeaderPanel" 
																CollapseControlID="TextDefaultsHeaderPanel" Collapsed="true" ImageControlID="TextDefaultsHeaderImage" 
																SuppressPostBack="true" />
</asp:PlaceHolder>





<div style="width:100%"><asp:Button ID="SaveChangesBottomButton" runat="server" class="button1 right" OnClick="SaveChangesButton_Click" /></div>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />

<script type="text/javascript">
  $(document).ready(
		function () {
			$('#LightColourTextBox').jPicker({
				window: {
					position: {
						x: 'screenCenter',
						y: 'center'
					}
				}
			});
			$('#DarkColourTextBox').jPicker({
				window: {
					position: {
						x: 'screenCenter',
						y: 'center'
					}
				}
			});

		  $("#<%=FlagDefaultsPanel.ClientID %>").find("input[type='checkbox']").each(function () {
		    var checkBox = $(this);
		    checkBox.bind("click", function () {
		      AssistDefaults_EnableRow(checkBox);
		    });

		    AssistDefaults_EnableRow(checkBox);
		  });
		}
	);

  function AssistDefaults_EnableRow(checkBox) {
    var checkBoxId = checkBox.attr("id");
    var isChecked = checkBox.is(":checked");
    
    var valuesRow = $("tr[data-values='" + checkBoxId + "']");
    if (isChecked) {
      valuesRow.removeClass("disabledRow");
    } else {
      valuesRow.addClass("disabledRow");
    }
    valuesRow.find("input,select").attr("disabled", !isChecked);
    
    $("tr[data-validators='" + checkBoxId + "']").find("span[id!='']").each(function () {
      ValidatorEnable(this, isChecked);
    });
  }
</script>
