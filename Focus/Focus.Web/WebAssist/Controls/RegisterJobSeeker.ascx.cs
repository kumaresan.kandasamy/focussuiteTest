﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Core.Models;
using Framework.Core;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class RegisterJobSeeker : UserControlBase
	{
		protected const string CreateOwnSecurityQuestionValue = "Own";
		protected string DateErrorMessage = "";
		protected string DOBMinLimitErrorMessage = "";
		protected string DOBMaxLimitErrorMessage = "";
		protected string DateOfBirthRequiredMessage = "";

		protected UserDetailsModel UserModel
		{
			get { return GetViewStateValue<UserDetailsModel>("RegisterJobSeeker:UserDetailsModel"); }
			set { SetViewStateValue("RegisterJobSeeker:UserDetailsModel", value); }
		}

		/// <summary>
		/// Gets or sets the career model.
		/// </summary>
		/// <value>The model.</value>
		protected CareerRegistrationModel CareerModel
		{
			get { return App.GetSessionValue("AssistJobSeekers:RegistrationModel", new CareerRegistrationModel()); }
			set { App.SetSessionValue("AssistJobSeekers:RegistrationModel", value); }
		}

		/// <summary>
		/// Gets or sets the explorer model.
		/// </summary>
		/// <value>The explorer model.</value>
		protected ExplorerRegistrationModel ExplorerModel
		{
			get { return App.GetSessionValue("AssistJobSeekers:RegistrationModel", new ExplorerRegistrationModel()); }
			set { App.SetSessionValue("AssistJobSeekers:RegistrationModel", value); }
		}

		/// <summary>
		/// Gets the Assist User's Office ID
		/// </summary>
		private long? CurrentOfficeId
		{
			get
			{
				var currentOfficeId = App.GetSessionValue<long?>("AssistJobSeekers:CurrentOfficeId");
				if (currentOfficeId.IsNull())
				{
					var officeModel = ServiceClientLocator.EmployerClient(App).GetCurrentOfficeForPerson(App.User.PersonId.GetValueOrDefault(0));
					currentOfficeId = (officeModel.IsNull()) ? 0 : officeModel.OfficeId;
					App.SetSessionValue("AssistJobSeekers:CurrentOfficeId", currentOfficeId);
				}
				return currentOfficeId;
			}
		}

		/// <summary>
		/// Any saved external Id
		/// </summary>
		protected string ExternalId
		{
			get { return GetViewStateValue<string>("RegisterJobSeeker:ExternalId"); }
			set { SetViewStateValue("RegisterJobSeeker:ExternalId", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
			NewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;

			EmailAddressTextbox.MaxLength = ConfirmEmailAddressTextbox.MaxLength = Math.Min(App.Settings.MaximumEmailLength, App.Settings.MaximumUserNameLength);

			meEducationPhoneNumber.Mask = App.Settings.PhoneNumberMaskPattern;

		    UsernameInUseErrorMessage.Text = "";
		    SsnInUseErrorMessage.Text = "";

			if (!IsPostBack)
			{
				BindSecurityQuestionDropDown();
				SetApplicationSpecificRowVisibility();
				if (App.Settings.CareerExplorerModule == FocusModules.Explorer)
					ExplorerModel = new ExplorerRegistrationModel();
				else
					CareerModel = new CareerRegistrationModel();

				ResetForms();

				BindDropdowns();

				ApplyTheme();
			}

			LocaliseUI();
		}

		/// <summary>
		/// Shows the specified person id.
		/// </summary>
		/// <param name="personId">The person id.</param>
		public void Show(long? personId = null)
		{
			ExternalId = null;

			if (personId.IsNotNull())
			{
				BindRegisterModal(Convert.ToInt64(personId));
				RegisterButton.Visible = false;
				HeadingLabel.Text = CodeLocalise("HeadingLabelEdit.Text", "Edit #CANDIDATETYPE#:LOWER account");
				SaveButton.Visible = true;
				AgreeToTermsButton.Visible = false;
				StepHeading.Visible = false;
			}
			else
			{
				RegisterButton.Visible = true;
				SaveButton.Visible = false;
				AgreeToTermsButton.Visible = true;
				StepHeading.Visible = true;
				EducationPlaceHolder1.Visible = false;
				HeadingLabel.Text = CodeLocalise("HeadingLabelNew.Text.NoEdit", "Create new #CANDIDATETYPE#:LOWER account");
			}

			RegisterModalPopup.Show();
		}

		/// <summary>
		/// Shows the modal with details from an external system
		/// </summary>
		/// <param name="validatedJobSeeker">Details of the external job seeker</param>
		public void Show(ValidatedUserView validatedJobSeeker)
		{
			UserNameTextbox.Text = validatedJobSeeker.EmailAddress;

			EmailAddressTextbox.Text = ConfirmEmailAddressTextbox.Text = validatedJobSeeker.EmailAddress;

			if (validatedJobSeeker.PrimaryPhone.IsNotNull())
			{
				if (App.Settings.Theme == FocusThemes.Education)
				{
					EducationPhoneNumberTextBox.Text = validatedJobSeeker.PrimaryPhone;
					EducationPhoneNumberTypeDropDownList.SelectValue(PhoneType.Home.ToString());

					EducationPhoneNumberTextBox.Enabled = validatedJobSeeker.PrimaryPhone.IsNullOrEmpty();
					EducationPhoneNumberTypeDropDownList.Enabled = validatedJobSeeker.PrimaryPhone.IsNullOrEmpty();
				}
				else
				{
					AdditionalInformationPhoneNumberTextBox.Text = validatedJobSeeker.PrimaryPhone;
					AdditionalInformationPhoneNumberTextBox.Enabled = validatedJobSeeker.PrimaryPhone.IsNullOrEmpty();
				}
			}

			AdditionalInformationFirstNameTextBox.Text = validatedJobSeeker.FirstName;
			AdditionalInformationFirstNameTextBox.Enabled = validatedJobSeeker.FirstName.IsNullOrEmpty();

			AdditionalInformationMiddleInitialTextBox.Text = validatedJobSeeker.MiddleInitial;
			AdditionalInformationMiddleInitialTextBox.Enabled = validatedJobSeeker.FirstName.IsNullOrEmpty();

			AdditionalInformationLastNameTextBox.Text = validatedJobSeeker.LastName;
			AdditionalInformationLastNameTextBox.Enabled = validatedJobSeeker.LastName.IsNullOrEmpty();

			if (validatedJobSeeker.SocialSecurityNumber.IsNotNull() && validatedJobSeeker.SocialSecurityNumber.Length == 9)
			{
				SocialSecurityNumberPart1TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart1TextBox.Attributes["value"] = validatedJobSeeker.SocialSecurityNumber.Substring(0, 3);
				SocialSecurityNumberPart2TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart2TextBox.Attributes["value"] = validatedJobSeeker.SocialSecurityNumber.Substring(3, 2);
				SocialSecurityNumberPart3TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart3TextBox.Attributes["value"] = validatedJobSeeker.SocialSecurityNumber.Substring(5, 4);

				SocialSecurityNumberPart1TextBox.Enabled = SocialSecurityNumberPart2TextBox.Enabled = SocialSecurityNumberPart3TextBox.Enabled = false;
				ConfirmSocialSecurityNumberPart1TextBox.Enabled = ConfirmSocialSecurityNumberPart2TextBox.Enabled = ConfirmSocialSecurityNumberPart3TextBox.Enabled = false;
			}

			if (validatedJobSeeker.DateOfBirth.HasValue)
				AdditionalInformationDateOfBirthTextBox.Text = validatedJobSeeker.DateOfBirth.Value.ToString("MM/dd/yyyy");

			AdditionalInformationDateOfBirthTextBox.Enabled = !validatedJobSeeker.DateOfBirth.HasValue;

			AdditionalInformationAddressTextBox.Text = validatedJobSeeker.AddressLine1;
			AdditionalInformationAddressTextBox.Enabled = validatedJobSeeker.AddressLine1.IsNullOrEmpty();

			AdditionalInformationCityTextBox.Text = validatedJobSeeker.AddressTownCity;
			AdditionalInformationCityTextBox.Enabled = validatedJobSeeker.AddressTownCity.IsNullOrEmpty();

			if (validatedJobSeeker.AddressStateId.GetValueOrDefault(0) > 0)
				AdditionalInformationStateDropdown.SelectValue(validatedJobSeeker.AddressStateId.ToString());

			AdditionalInformationStateDropdown.Enabled = validatedJobSeeker.AddressStateId.GetValueOrDefault(0) == 0;

			AdditionalInformationZipTextBox.Text = validatedJobSeeker.AddressPostcodeZip;
			AdditionalInformationZipTextBox.Enabled = validatedJobSeeker.AddressPostcodeZip.IsNullOrEmpty();

			Show();

			ExternalId = validatedJobSeeker.ExternalId;
		}

		/// <summary>
		/// Handles the Click event of the RegisterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void RegisterButton_Click(object sender, EventArgs e)
		{
			if (App.Settings.CareerExplorerModule == FocusModules.Explorer)
				RegistrationStep1Explorer();
			else
				RegistrationStep1Career();

			RegisterModalPopup.Show();
		}

		/// <summary>
		/// Handles the Click event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Click(object sender, EventArgs e)
		{
			UpdateUserDetails();
		}

		/// <summary>
		/// Binds the register modal.
		/// </summary>
		/// <param name="personId">The person id.</param>
		private void BindRegisterModal(long personId)
		{
			UserModel = ServiceClientLocator.AccountClient(App).GetUserDetails(0, personId);

			UserNameTextbox.Text = UserModel.UserDetails.UserName;
			ScreenNameTextbox.Text = UserModel.UserDetails.ScreenName;
			EmailAddressTextbox.Text = UserModel.PersonDetails.EmailAddress;
			if (UserModel.SecurityQuestionId.HasValue)
			{
				SecurityQuestionDropDown.SelectValue(UserModel.SecurityQuestionId.ToString());
				SecurityQuestionTextBox.Text = string.Empty;
			}
			else
			{
				SecurityQuestionDropDown.SelectValue( CreateOwnSecurityQuestionValue );
				SecurityQuestionTextBox.Text = UserModel.SecurityQuestion;
			}
			if (App.Settings.ShowProgramArea)
			{
				DepartmentDropDownList.SelectValue(UserModel.PersonDetails.ProgramAreaId.ToString());
				DegreeDropDownListCascadingDropDown.SelectedValue = UserModel.PersonDetails.DegreeId.ToString();
			}
			else
			{
				AllDegreeDropDownList.SelectedValue = UserModel.PersonDetails.DegreeId.ToString();
			}

			var enrollmentStatus = UserModel.PersonDetails.EnrollmentStatus.ToString();
			if (EnrollmentStatusDropDownList.Items.FindByValue(enrollmentStatus).IsNotNull())
				EnrollmentStatusDropDownList.SelectValue(enrollmentStatus);

			if (App.Settings.Theme == FocusThemes.Education)
			{
				if (App.Settings.ShowCampus)
				{
					var campusId = UserModel.PersonDetails.CampusId.ToString();
					if (CampusDropDownList.Items.FindByValue(campusId).IsNotNull())
						CampusDropDownList.SelectValue(campusId);
				}

				if (UserModel.PrimaryPhoneNumber.IsNotNull())
				{
					EducationPhoneNumberTextBox.Text = UserModel.PrimaryPhoneNumber.Number;

					var phoneType = PhoneType.Home;
					switch (UserModel.PrimaryPhoneNumber.PhoneType)
					{
						case PhoneTypes.Mobile:
							phoneType = PhoneType.Cell;
							break;
						case PhoneTypes.Fax:
							phoneType = PhoneType.Fax;
							break;
					}
					EducationPhoneNumberTypeDropDownList.SelectValue(phoneType.ToString());

					CellphoneProviderDropDown.SelectValue(UserModel.PrimaryPhoneNumber.ProviderId.ToString());
				}
			}

			#region Person/Address details

			AdditionalInformationFirstNameTextBox.Text = UserModel.PersonDetails.FirstName;
			AdditionalInformationMiddleInitialTextBox.Text = UserModel.PersonDetails.MiddleInitial;
			AdditionalInformationLastNameTextBox.Text = UserModel.PersonDetails.LastName;
			AdditionalInformationDateOfBirthTextBox.Text = UserModel.PersonDetails.DateOfBirth.ToString();
			AdditionalInformationAddressTextBox.Text = UserModel.AddressDetails.Line1;
			AdditionalInformationCityTextBox.Text = UserModel.AddressDetails.TownCity;
			AdditionalInformationStateDropdown.SelectValue(UserModel.AddressDetails.StateId.ToString(CultureInfo.InvariantCulture));
			AdditionalInformationZipTextBox.Text = UserModel.AddressDetails.PostcodeZip;

			#endregion
		}

		/// <summary>
		/// Registrations the step1 explorer.
		/// </summary>
		private void RegistrationStep1Explorer()
		{
			ExplorerModel = GetExplorerRegistrationModel();

			// Check to see if the username already exists in the system
			var userExists = ServiceClientLocator.AccountClient(App).CheckUserExists(ExplorerModel, checkLocalOnly: (ExternalId.IsNotNullOrEmpty()));
			if (userExists.Item1)
			{
				RaiseRegistrationError(externalReason: userExists.Item2);
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = false;
			}
			else
			{
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = false;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = true;
			}
		}

		/// <summary>
		/// Registration - Step 1 : Career.
		/// </summary>
		private void RegistrationStep1Career()
		{
			CareerModel = GetCareerRegistrationModel();

			// Check to see if the username already exists in the system
			var userExists = ServiceClientLocator.AccountClient(App).CheckUserExists(CareerModel, checkLocalOnly: (ExternalId.IsNotNullOrEmpty()));
			if (userExists.Item1)
			{
				var errorMessage = "User with the given information already present in the system.";
			    if (userExists.Item2 == ExtenalUserPreExistenceReason.SsnExists)
			    {
			        RaiseRegistrationSsnError(errorMessage, userExists.Item2);
			    }

                if (userExists.Item2 != ExtenalUserPreExistenceReason.SsnExists)
			    {
			        RaiseRegistrationError(errorMessage, userExists.Item2);
			    }

				

				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = false;
			}
			else
			{
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = false;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = true;
			}
		}

		/// <summary>
		/// Updates the user details.
		/// </summary>
		private void UpdateUserDetails()
		{
			UpdateUserDetailsModel();
			CareerModel = GetCareerRegistrationModel();

			var userExists = ServiceClientLocator.AccountClient(App).CheckUserExists(CareerModel, UserModel.UserId, (ExternalId.IsNotNullOrEmpty()));
			if (userExists.Item1)
				RaiseRegistrationError("User with the given information already present in the system.", userExists.Item2);
			else
			{
				ServiceClientLocator.AccountClient(App).UpdateUser(UserModel);

				ResetForms();

				Confirmation.Show(CodeLocalise("UserEditConfirmation.Title", "#CANDIDATETYPE# account updated"),
					CodeLocalise("UserEditConfirmation.Body", "#CANDIDATETYPE# account updated successfully"),
					CodeLocalise("CloseModal.Text", "OK"));
			}
		}

		/// <summary>
		/// Handles the Click event of the CancelRegistration control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void CancelRegistration_Click(object sender, EventArgs e)
		{
			RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
			RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = false;

			ResetForms();

			RegisterModalPopup.Hide();
		}

		/// <summary>
		/// Handles the Click event of the AgreeToTermsButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void AgreeToTermsButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (App.Settings.CareerExplorerModule == FocusModules.Explorer)
					RegisterExplorer();
				else
					RegisterCareer();
			}
			catch (ApplicationException ex)
			{
				RaiseRegistrationError(ex.Message);
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = false;

				return;
			}

			ResetForms();

			OnJobSeekerRegistered(new EventArgs());

			Confirmation.Show(CodeLocalise("CareerRegistrationConfirmation.Title", "#CANDIDATETYPE# registered"),
				CodeLocalise("CareerRegistrationConfirmation.Body", "#CANDIDATETYPE# registration complete."),
				CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Registers the user in career and career/explorer mode.
		/// </summary>
		private void RegisterCareer()
		{
			ServiceClientLocator.AccountClient(App).RegisterCareerUser(CareerModel);

			if (CareerModel.RegistrationError != ErrorTypes.UserNameAlreadyExists) return;

			RaiseRegistrationError();
			RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
			RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = false;
		}

		/// <summary>
		/// Registers the user in Explorer only mode.
		/// </summary>
		private void RegisterExplorer()
		{
			ServiceClientLocator.AccountClient(App).RegisterExplorerUser(ExplorerModel);

			if (ExplorerModel.RegistrationError != ErrorTypes.UserNameAlreadyExists) return;

			RaiseRegistrationError();
			RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
			RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = false;
		}

		/// <summary>
		/// Gets the career registration model.
		/// </summary>
		/// <returns></returns>
		private CareerRegistrationModel GetCareerRegistrationModel()
		{
			// Create model and initialise it with Email Address, Security Question and Answer
			var model = new CareerRegistrationModel
			{
				AccountUserName = EmailAddressTextbox.TextTrimmed(defaultValue: null),
				AccountPassword = NewPasswordTextBox.TextTrimmed(defaultValue: null),
				EmailAddress = EmailAddressTextbox.TextTrimmed(defaultValue: null),
				SecurityQuestion = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue) ? null : SecurityQuestionTextBox.TextTrimmed(),
				SecurityQuestionId = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue) ? SecurityQuestionDropDown.SelectedValueToInt() : (long?)null,
				SecurityAnswer = SecurityAnswerTextBox.TextTrimmed(),
				ModuleToCheck = App.Settings.CareerExplorerModule,
				OverrideOfficeId = CurrentOfficeId,
				ExternalId = ExternalId
			};

			// Establish if this is a registration using SSN
			var ssn = SocialSecurityNumberPart1TextBox.Text + SocialSecurityNumberPart2TextBox.Text + SocialSecurityNumberPart3TextBox.Text;
			ssn = Regex.Replace(ssn, @"[^0-9]", "");

			var isSsn = !(ssn.Length < 9);

			// If SSN registrataion then just use this else set up all the other data items needed for the registration model
			if (isSsn)
				model.SocialSecurityNumber = ssn;

			model.FirstName = AdditionalInformationFirstNameTextBox.TextTrimmed(defaultValue: null);
			model.MiddleName = AdditionalInformationMiddleInitialTextBox.TextTrimmed(defaultValue: null);
			model.LastName = AdditionalInformationLastNameTextBox.TextTrimmed(defaultValue: null);

			model.DateOfBirth = AdditionalInformationDateOfBirthTextBox.TextTrimmed().ToDate();

			if (App.Settings.Theme != FocusThemes.Education)
				model.PrimaryPhone = new Phone
				{
					PhoneNumber = Regex.Replace(AdditionalInformationPhoneNumberTextBox.TextTrimmed(), @"[^0-9]", ""),
					PhoneType = PhoneType.Home
				};
			else
				model.PrimaryPhone = new Phone
				{
					PhoneNumber = Regex.Replace(EducationPhoneNumberTextBox.TextTrimmed(), @"[^0-9]", ""),
					PhoneType = EducationPhoneNumberTypeDropDownList.SelectedValue == "" ? null : (PhoneType?)Enum.Parse(typeof(PhoneType), EducationPhoneNumberTypeDropDownList.SelectedValue),
					Provider = EducationPhoneNumberTypeDropDownList.SelectedValue == "" ? null : CellphoneProviderDropDown.SelectedValue.ToLong()
				};

			model.PostalAddress = new Address
			{
				Street1 = AdditionalInformationAddressTextBox.TextTrimmed(defaultValue: null),
				City = AdditionalInformationCityTextBox.TextTrimmed(defaultValue: null),
				StateId = AdditionalInformationStateDropdown.SelectedIndex != 0 ? AdditionalInformationStateDropdown.SelectedValue.ToLong() : null,
				StateName = AdditionalInformationStateDropdown.SelectedIndex != 0 ? AdditionalInformationStateDropdown.SelectedItem.Text : null,
				Zip = AdditionalInformationZipTextBox.TextTrimmed(defaultValue: null)
			};

			if (App.Settings.Theme == FocusThemes.Education)
			{
				model.ProgramAreaId = DepartmentDropDownList.SelectedValue.ToLong();
				model.DegreeId = App.Settings.ShowProgramArea
					? DegreeDropDownList.SelectedValueToLong()
					: AllDegreeDropDownList.SelectedValueToLong();
				model.EnrollmentStatus = EnrollmentStatusDropDownList.SelectedValue.AsEnum<SchoolStatus>();

				if (App.Settings.ShowCampus)
					model.CampusId = CampusDropDownList.SelectedValueToLong();
			}

			if (App.Settings.DisplayCareerAccountType)
				model.AccountType = SeekerAccountTypeDropDownList.SelectedValue.AsEnum<CareerAccountType>();

			return model;
		}

		/// <summary>
		/// Gets the explorer registration model.
		/// </summary>
		/// <returns></returns>
		private ExplorerRegistrationModel GetExplorerRegistrationModel()
		{
			//TODO: Jane - 23/05/2013 - Creating an explorer user needs to be reviewed since putting in the CE education changes.  Currently the user can enter personal details etc but these don't get saved to the DB
			// Personally I'd like to merge these explorer/career model methods together to a "getRegistrationModel" and "RegisterUser".  I don't see the point in duplicating effort here
			var model = new ExplorerRegistrationModel
			{
				AccountUserName = EmailAddressTextbox.TextTrimmed(defaultValue: null),
				AccountPassword = NewPasswordTextBox.TextTrimmed(defaultValue: null),
				ScreenName = ScreenNameTextbox.TextTrimmed(),
				EmailAddress = EmailAddressTextbox.TextTrimmed(defaultValue: null),
				SecurityQuestion = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue) ? null : SecurityQuestionTextBox.TextTrimmed(),
				SecurityQuestionId = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue) ? SecurityQuestionDropDown.SelectedValueToInt() : (long?)null,
				SecurityAnswer = SecurityAnswerTextBox.TextTrimmed(),
				ModuleToCheck = FocusModules.Explorer
			};

			return model;
		}

		/// <summary>
		/// Updates the user details model that has been put in state when a user is being editted - so that we don't have to go to the db for the user details again
		/// </summary>
		private void UpdateUserDetailsModel()
		{
			#region User details

			UserModel.UserDetails.UserName = UserNameTextbox.TextTrimmed(defaultValue: null);
			UserModel.Password = NewPasswordTextBox.TextTrimmed(defaultValue: null);
			UserModel.SecurityQuestion = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue) ? null : SecurityQuestionTextBox.TextTrimmed();
			UserModel.SecurityQuestionId = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue) ? SecurityQuestionDropDown.SelectedValueToInt() : (long?)null;
			UserModel.SecurityAnswer = SecurityAnswerTextBox.TextTrimmed();

			#endregion

			#region Person details

			UserModel.PersonDetails.EmailAddress = EmailAddressTextbox.TextTrimmed(defaultValue: null);
			UserModel.PersonDetails.FirstName = AdditionalInformationFirstNameTextBox.TextTrimmed(defaultValue: null);
			UserModel.PersonDetails.MiddleInitial = AdditionalInformationMiddleInitialTextBox.TextTrimmed(defaultValue: null);
			UserModel.PersonDetails.LastName = AdditionalInformationLastNameTextBox.TextTrimmed(defaultValue: null);
			UserModel.PersonDetails.DateOfBirth = AdditionalInformationDateOfBirthTextBox.TextTrimmed().ToDate();
			UserModel.PersonDetails.AccountType = App.Settings.DisplayCareerAccountType ? SeekerAccountTypeDropDownList.SelectedValue.AsEnum<CareerAccountType>() : null;
			UserModel.PersonDetails.ProgramAreaId = DepartmentDropDownList.SelectedValue.ToLong();
			UserModel.PersonDetails.DegreeId = App.Settings.ShowProgramArea
				? DegreeDropDownList.SelectedValueToLong()
				: AllDegreeDropDownList.SelectedValueToLong();
			UserModel.PersonDetails.EnrollmentStatus = EnrollmentStatusDropDownList.SelectedValue.AsEnum<SchoolStatus>();

			if (App.Settings.Theme == FocusThemes.Education)
			{
				if (App.Settings.ShowCampus)
					UserModel.PersonDetails.CampusId = CampusDropDownList.SelectedValueToNullableLong();
			}


			#endregion

			#region Cellphone details

			if (App.Settings.Theme == FocusThemes.Education)
			{
				if (UserModel.PrimaryPhoneNumber.IsNull())
					UserModel.PrimaryPhoneNumber = new PhoneNumberDto();

				var phoneType = PhoneTypes.Phone;
				var selectedPhoneType = EducationPhoneNumberTypeDropDownList.SelectedValue;
				if (selectedPhoneType.IsNotNullOrEmpty())
				{
					var tempPhoneType = (PhoneType)Enum.Parse(typeof(PhoneType), selectedPhoneType, true);

					switch (tempPhoneType)
					{
						case PhoneType.Cell:
							phoneType = PhoneTypes.Mobile;
							break;
						case PhoneType.NonUS:
						case PhoneType.Home:
						case PhoneType.Work:
							phoneType = PhoneTypes.Phone;
							break;
						case PhoneType.Fax:
							phoneType = PhoneTypes.Fax;
							break;
						default:
							phoneType = PhoneTypes.Phone;
							break;
					}
				}

				UserModel.PrimaryPhoneNumber.Number = Regex.Replace(EducationPhoneNumberTextBox.TextTrimmed(), @"[^0-9]", "");
				UserModel.PrimaryPhoneNumber.ProviderId = CellphoneProviderDropDown.SelectedValue.ToInt();
				UserModel.PrimaryPhoneNumber.PhoneType = phoneType;
				UserModel.PrimaryPhoneNumber.IsPrimary = true;
			}

			#endregion

			#region Address details

			if (UserModel.AddressDetails.IsNull())
				UserModel.AddressDetails = new PersonAddressDto();

			UserModel.AddressDetails.Line1 = AdditionalInformationAddressTextBox.TextTrimmed(defaultValue: null);
			UserModel.AddressDetails.TownCity = AdditionalInformationCityTextBox.TextTrimmed(defaultValue: null);
			UserModel.AddressDetails.StateId = Convert.ToInt64(AdditionalInformationStateDropdown.SelectedValue);
			UserModel.AddressDetails.PostcodeZip = AdditionalInformationZipTextBox.TextTrimmed(defaultValue: null);

			#endregion
		}

		/// <summary>
		/// Raises the registration error.
		/// </summary>
		/// <param name="errorMessage">The error message.</param>
		/// <param name="externalReason"></param>
		private void RaiseRegistrationError(string errorMessage = null, ExtenalUserPreExistenceReason externalReason = ExtenalUserPreExistenceReason.NotApplicable)
		{
			var error = "Email address is already in use.";

			switch (externalReason)
			{
				case ExtenalUserPreExistenceReason.UsernameExists:
					errorMessage = CodeLocalise("ExternalJobSeekerUserNameExists.Error", "Username is already in use on external system");
					break;
				case ExtenalUserPreExistenceReason.SsnExists:
					errorMessage = CodeLocalise("ExternalJobSeekerSSNExists.Error", "SSN is already in use on external system");
					break;
			}

			if (errorMessage.IsNotNullOrEmpty())
				error = errorMessage;

			UsernameInUseErrorMessage.Text = CodeLocalise("UsernameErrorMessage.Text", error);
		}




	    private void RaiseRegistrationSsnError(string errorMessage = null, ExtenalUserPreExistenceReason externalReason = ExtenalUserPreExistenceReason.SsnExists)
	    {
	        var error = "Ssn is already in use.";

	        switch (externalReason)
	        {
	            case ExtenalUserPreExistenceReason.UsernameExists:
	                errorMessage = CodeLocalise("ExternalJobSeekerUserNameExists.Error", "Username is already in use on external system");
	                break;
	            case ExtenalUserPreExistenceReason.SsnExists:
	                errorMessage = CodeLocalise("ExternalJobSeekerSSNExists.Error", "SSN is already in use on external system");
	                break;
	        }

	        if (errorMessage.IsNotNullOrEmpty())
	            error = errorMessage;

	        SsnInUseErrorMessage.Text = CodeLocalise("SocialSecurityNumberValidator.AlreadyInUse", error);
            //SsnInUseErrorMessage
	    }


		/// <summary>
		/// Binds the security question drop down.
		/// </summary>
		private void BindSecurityQuestionDropDown()
		{
			if (App.Settings.CareerExplorerModule == FocusModules.Explorer)
				BindExplorerDropDown(SecurityQuestionDropDown, ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions), null, CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"));
			else
				SecurityQuestionDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions), null, CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"));

			SecurityQuestionDropDown.Items.Add(new ListItem(CodeLocalise("CreateOwnSecurityQuestion.DropDownValue", "Create own security question"), CreateOwnSecurityQuestionValue));
		}

		/// <summary>
		/// Binds the lookup.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		/// <param name="lookupItems">The lookup items.</param>
		/// <param name="currentValue">The current value.</param>
		/// <param name="topDefault">The top default.</param>
		private static void BindExplorerDropDown(DropDownList dropDownList, IList<LookupItemView> lookupItems, string currentValue, string topDefault)
		{
			dropDownList.DataSource = lookupItems;
			dropDownList.DataValueField = "Id";
			dropDownList.DataTextField = "Text";
			dropDownList.DataBind();

			if (!String.IsNullOrEmpty(currentValue) && dropDownList.Items.FindByValue(currentValue) != null) dropDownList.SelectValue(currentValue);
			if (!String.IsNullOrEmpty(topDefault)) dropDownList.Items.Insert(0, new ListItem(topDefault, string.Empty));
		}

		/// <summary>
		/// Sets the application specific row visibility.
		/// </summary>
		private void SetApplicationSpecificRowVisibility()
		{
			ScreenNameRow.Visible = (App.Settings.CareerExplorerModule == FocusModules.Explorer);
			SSNRow.Visible = SSN2Row.Visible = (App.Settings.CareerExplorerModule != FocusModules.Explorer);
			SeekerAccountTypePlaceholder.Visible = SeekerAccountTypeRequired.Enabled = App.Settings.DisplayCareerAccountType;
		}

		/// <summary>
		/// Handles the CloseCommand event of the ConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		public void ConfirmationModal_CloseCommand(object sender, CommandEventArgs e)
		{
			Response.Redirect(Page.Request.Url.ToString());
		}

		/// <summary>
		/// Binds the dropdowns.
		/// </summary>
		private void BindDropdowns()
		{
			var states = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(lu => !App.Settings.SpecialStateKeys.Contains(lu.Key)).ToList();

			AdditionalInformationStateDropdown.BindLookup(states, states.Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).SingleOrDefault().ToString(CultureInfo.InvariantCulture), CodeLocalise("State.TopDefault", "- select state -"));

			if (App.Settings.Theme == FocusThemes.Education)
			{
				BindEnrollmentStatusDropDown(EnrollmentStatusDropDownList);

				if (App.Settings.ShowCampus)
					Utilities.BindCampusDropDown(CampusDropDownList);

				if (App.Settings.ShowProgramArea)
				{
					BindDepartmentDropDown();
					BindDegreeDropDown();
				}
				else
				{
					BindAllDegreeDropDown();
				}
				BindEducationPhoneNumberTypeDropDown();
				BindCellPhoneProviderDropDown();
			}
			if (App.Settings.DisplayCareerAccountType)
				BindCareerAccountTypeDropdown();
		}

		protected void BindCareerAccountTypeDropdown()
		{
			SeekerAccountTypeDropDownList.Items.Add(App.Settings.Theme == FocusThemes.Workforce
				? new ListItem(CodeLocalise("AccountType.JobSeeker", "#CANDIDATETYPE#"), "0")
				: new ListItem(CodeLocalise("AccountType.JobSeeker", "Job Seeker"), "0"));
			SeekerAccountTypeDropDownList.Items.Add(new ListItem(CodeLocalise("AccountType.Student", "Student"), "1"));
			SeekerAccountTypeDropDownList.Items.AddLocalisedTopDefault("Global.TopDefault", "- select -");
		}

		/// <summary>
		/// Binds the phone type drop down.
		/// </summary>
		private void BindEducationPhoneNumberTypeDropDown()
		{
			EducationPhoneNumberTypeDropDownList.Items.AddLocalisedTopDefault("SelectPhoneType.DefaultText", "- select -");

			EducationPhoneNumberTypeDropDownList.Items.AddEnum(PhoneType.Home, "Home");
			EducationPhoneNumberTypeDropDownList.Items.AddEnum(PhoneType.Work, "Work");
			EducationPhoneNumberTypeDropDownList.Items.AddEnum(PhoneType.Cell, "Cell");
			EducationPhoneNumberTypeDropDownList.Items.AddEnum(PhoneType.Fax, "Fax");
			EducationPhoneNumberTypeDropDownList.Items.AddEnum(PhoneType.NonUS, "Non US");
		}

		/// <summary>
		/// Binds the cell phone provider drop down.
		/// </summary>
		private void BindCellPhoneProviderDropDown()
		{
			CellphoneProviderDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.CellphoneProviders), null, CodeLocalise("CellphoneProviders.TopDefault", "- select cellphone provider -"));
		}

		/// <summary>
		/// Binds the department drop down.
		/// </summary>
		private void BindDepartmentDropDown()
		{
			DepartmentDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreas();
			DepartmentDropDownList.DataTextField = "Name";
			DepartmentDropDownList.DataValueField = "Id";
			DepartmentDropDownList.DataBind();
			DepartmentDropDownList.Items.Insert(0, new ListItem(CodeLocalise("ProgramArea.Undeclared.Text", "Undeclared"), "0"));
			DepartmentDropDownList.Items.AddLocalisedTopDefault("Global.ProgramArea.TopDefault", "- select department -");
		}


		/// <summary>
		/// Binds the study program area degrees drop down.
		/// </summary>
		private void BindDegreeDropDown()
		{
			if (!String.IsNullOrEmpty(DepartmentDropDownList.SelectedValue))
			{
				DegreeDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(DepartmentDropDownList.SelectedValueToLong());
				DegreeDropDownList.DataTextField = "DegreeEducationLevelName";
				DegreeDropDownList.DataValueField = "DegreeEducationLevelId";
				DegreeDropDownList.DataBind();
				DegreeDropDownList.Items.AddLocalisedTopDefault("Global.Degree.AllDegrees", "All Degrees", string.Concat("-", DepartmentDropDownList.SelectedValue));
				DegreeDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

				var selectedDegree = Request.Form[DegreeDropDownList.UniqueID];
				if (selectedDegree.IsNotNullOrEmpty())
				{
					DegreeDropDownList.SelectedValue = selectedDegree;
				}
			}
		}

		/// <summary>
		/// Binds all degrees to the drop-down
		/// </summary>
		private void BindAllDegreeDropDown()
		{
			AllDegreeDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(null);
			AllDegreeDropDownList.DataTextField = "DegreeEducationLevelName";
			AllDegreeDropDownList.DataValueField = "DegreeEducationLevelId";
			AllDegreeDropDownList.DataBind();
			AllDegreeDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			var messages = new NameValueCollection
			{
				{ "PhoneNumberRequired", CodeLocalise("PhoneNumberRequired.ErrorMessage", "Phone number is required") },
				{ "PhoneNumberInvalid", CodeLocalise("PhoneNumberInvalid.ErrorMessage", "U.S. phone number must be 10 digits") }
			};

			SocialSecurityNumberValidator.ErrorMessage = CodeLocalise("SocialSecurityNumberValidator.ErrorMessage", "Invalid social security number");
			ConfirmSocialSecurityNumberValidator.ErrorMessage = CodeLocalise("SocialSecurityNumberValidatorValidator.ErrorMessage", "SSN must match");

			PhoneNumberValidator.ErrorMessage = CodeLocalise("PhoneNumber.ErrorMessage", "U.S. phone number must be 10 digits");

			RegisterCodeValuesJson("RegisterJobSeeker_ValidationMessages", "RegisterJobSeeker_ValidationMessages", messages);

			AdditionalInformationDateOfBirthTextBox.ToolTip = CodeLocalise("ClosingDateTextBox.ToolTip", "Please enter the date of birth as mm/dd/yyyy");
			DateErrorMessage = CodeLocalise("Date.ErrorMessage", "Valid date is required");
			DateOfBirthRequiredMessage = CodeLocalise("DateOfBirthRequired.ErrorMessage", "Date of birth is required");
			DOBMinLimitErrorMessage = CodeLocalise("DateOfBirthMin.ErrorMessage", "You must be at least 14 years of age.");
      DOBMaxLimitErrorMessage = CodeLocalise("DateOfBirthMax.ErrorMessage", "You must not be more than 99 years of age.");

			//AdditionalInformationValidator.ErrorMessage = CodeLocalise("AdditionalInformationValidator.ErrorMessage", "Additional information is required");
			RegisterButton.Text = CodeLocalise("RegisterButton.Text", "Register");

			CancelButtonStep1.Text = CancelButtonStep2.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			AgreeToTermsButton.Text = CodeLocalise("AgreeToTermsButton.Text", "I confirm that I explained these terms to this #CANDIDATETYPE#:LOWER");

			ScreenNameRequired.Text = CodeLocalise("ScreenNameRequired.ErrorMessage", "Screen name is required");
			EmailAddressRequired.Text = CodeLocalise("EmailAddressRequired.ErrorMessage", "Email address is required");
			EmailAddressRegEx.Text = CodeLocalise("EmailAddressRegEx.ErrorMessage", "Email address is not in the correct format");
			ConfirmEmailAddressRequired.Text = CodeLocalise("ConfirmEmailAddressRequired.ErrorMessage", "Confirm email address is required");
			ConfirmEmailAddressCompare.Text = CodeLocalise("ConfirmEmailAddressCompare.ErrorMessage", "Email addresses must match");
			NewPasswordRequired.Text = CodeLocalise("NewPasswordRequired.ErrorMessage", "Password is required");
			NewPasswordRegEx.Text = CodeLocalise("NewPasswordRegEx.ErrorMessage", "Password must be 6-20 characters and contain at least 1 number");
			ConfirmNewPasswordRequired.Text = CodeLocalise("ConfirmNewPasswordRequired.ErrorMessage", "Confirm password is required");
			ConfirmNewPasswordCompare.Text = CodeLocalise("ConfirmNewPasswordCompare.ErrorMessage", "Passwords must match");
			SecurityQuestionCustomValidator.Text = CodeLocalise("SecurityQuestionCustomValidator.ErrorMessage", "Security question is required");
			SecurityQuestionRequired.Text = CodeLocalise("SecurityQuestionRequired.ErrorMessage", "Security question is required");
			SecurityAnswerRequired.Text = CodeLocalise("SecurityAnswerRequired.ErrorMessage", "Security answer is required");
			SaveButton.Text = CodeLocalise("SaveButton.Text", "Save");
			DepartmentRequired.Text = CodeLocalise("DepartmentRequired.Text", "Program of study is required");
			DegreeRequired.Text = CodeLocalise("DegreeRequired.Text", "Degree is required");
			EnrollmentStatusRequired.Text = CodeLocalise("EnrollmentStatusRequired.Text", "Enrollment status is required");
			CampusRequired.Text = CodeLocalise("CampusRequired.Text", "Campus location is required");
			AdditionalInformationFirstNameValidator.Text = CodeLocalise("AdditionalInformationFirstNameValidator.Text",
				"First name is required");
			AdditionalInformationLastNameValidator.Text = CodeLocalise("AdditionalInformationLastNameValidator.Text",
				"Last name is required");
			AdditionalInformationZipValidator.Text = CodeLocalise("AdditionalInformationZipValidator.Text",
				"Zip code is required");
			AdditionalInformationAddressValidator.Text = CodeLocalise("AdditionalInformationAddressValidator.Text",
				"Address is required");
			AdditionalInformationCityValidator.Text = CodeLocalise("AdditionalInformationCityValidator.Text", "City is required");
			AdditionalInformationStateValidator.Text = CodeLocalise("AdditionalInformationStateValidator.Text", "State is required");

			SeekerAccountTypeRequired.ErrorMessage = CodeLocalise("SeekerAccountType.RequiredErrorMessage", "Account type is required");

			EducationPhoneNumberValidator.ErrorMessage = CodeLocalise("EducationPhoneNumber.ErrorMessage", "U.S. phone number must be 10 digits");
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			if (App.Settings.Theme == FocusThemes.Education)
			{
				#region Show/hide controls

				EducationPlaceHolder1.Visible = true;
				EducationInformationPlaceHolder2.Visible = true;
				WorkforceSSNPlaceHolder.Visible = false;
				AdditionalInformationPhoneNumberRow.Visible = false;
				EducationPhoneNumberPlaceHolder.Visible = true;
				EducationAdditionalInfoTitleRow.Visible = true;
				AdditionalInformationDateOfBirthRow.Visible = false;
				RegistrationEmailHelpText.Visible = false;

				CampusPlaceHolder.Visible = App.Settings.ShowCampus;

				#endregion

				DegreeDropDownListCascadingDropDown.LoadingText = CodeLocalise("DegreeDropDownListCascadingDropDown.LoadingText", "[Loading degrees...]");
				DegreeDropDownListCascadingDropDown.PromptText = CodeLocalise("DegreeDropDownListCascadingDropDown.PromptText", "- select degree -");

				if (!App.Settings.ShowProgramArea)
				{
					ProgramOfStudyPlaceHolder.Visible = false;
					DegreeDropCascadingDropDownPlaceHolder.Visible = false;
					AllDegreeDropDownList.Visible = true;
					DegreeRequired.ControlToValidate = "AllDegreeDropDownList";
				}
			}
		}

		/// <summary>
		/// Resets the forms.
		/// </summary>
		private void ResetForms()
		{
			ExternalId = null;

			// Registration
			UserNameTextbox.Text = "";
			ScreenNameTextbox.Text = "";
			EmailAddressTextbox.Text = "";
		    UsernameInUseErrorMessage.Text = "";
		    SsnInUseErrorMessage.Text = "";
			ConfirmEmailAddressTextbox.Text = "";
            SocialSecurityNumberPart1TextBox.Text = "";
			SocialSecurityNumberPart2TextBox.Text = "";
			SocialSecurityNumberPart3TextBox.Text = "";
			ConfirmSocialSecurityNumberPart1TextBox.Text = "";
			ConfirmSocialSecurityNumberPart2TextBox.Text = "";
			ConfirmSocialSecurityNumberPart3TextBox.Text = "";
			SocialSecurityNumberPart1TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart1TextBox.Attributes["value"] = "";
			SocialSecurityNumberPart2TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart2TextBox.Attributes["value"] = "";
			SocialSecurityNumberPart3TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart3TextBox.Attributes["value"] = "";
			SocialSecurityNumberPart1TextBox.Enabled = SocialSecurityNumberPart2TextBox.Enabled = SocialSecurityNumberPart3TextBox.Enabled = true;
			ConfirmSocialSecurityNumberPart1TextBox.Enabled = ConfirmSocialSecurityNumberPart2TextBox.Enabled = ConfirmSocialSecurityNumberPart3TextBox.Enabled = true;
			SecurityQuestionDropDown.SelectedIndex = -1;
			SecurityQuestionTextBox.Text = "";
			SeekerAccountTypeDropDownList.SelectedIndex = -1;
			SecurityAnswerTextBox.Text = "";
			AdditionalInformationFirstNameTextBox.Text = "";
			AdditionalInformationMiddleInitialTextBox.Text = "";
			AdditionalInformationLastNameTextBox.Text = "";
			AdditionalInformationDateOfBirthTextBox.Text = "";
			AdditionalInformationPhoneNumberTextBox.Text = "";
			AdditionalInformationAddressTextBox.Text = "";
			AdditionalInformationCityTextBox.Text = "";
			AdditionalInformationZipTextBox.Text = "";
			UsernameInUseErrorMessage.Text = "";
			DepartmentDropDownList.SelectedIndex = -1;
			AllDegreeDropDownList.SelectedIndex = -1;
			DegreeDropDownListCascadingDropDown.SelectedValue = "";
			EnrollmentStatusDropDownList.SelectedIndex = -1;
			CampusDropDownList.SelectedIndex = -1;
			CellphoneProviderDropDown.SelectedIndex = -1;
			EducationPhoneNumberTextBox.Text = "";


			#region Re-enable controls in case they where disabled whilst importing a job seeker

			if (App.Settings.Theme == FocusThemes.Education)
				EducationPhoneNumberTypeDropDownList.Enabled = true;
			else
				AdditionalInformationPhoneNumberTextBox.Enabled = true;

			AdditionalInformationFirstNameTextBox.Enabled = true;
			AdditionalInformationMiddleInitialTextBox.Enabled = true;
			AdditionalInformationLastNameTextBox.Enabled = true;
			AdditionalInformationDateOfBirthTextBox.Enabled = true;
			AdditionalInformationAddressTextBox.Enabled = true;
			AdditionalInformationCityTextBox.Enabled = true;
			AdditionalInformationStateDropdown.Enabled = true;
			AdditionalInformationZipTextBox.Enabled = true;

			#endregion

			var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Key == App.Settings.DefaultStateKey);
			if (lookup.IsNotNull())
				AdditionalInformationStateDropdown.SelectValue(lookup.Id.ToString(CultureInfo.InvariantCulture));

			RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
			RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panRegisterStep2.Visible = false;
		}

		#region Page event

		public event JobSeekerRegisteredHandler JobSeekerRegistered;

		/// <summary>
		/// Raises the <see cref="JobSeekerRegistered" /> event.
		/// </summary>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		public void OnJobSeekerRegistered(EventArgs e)
		{
			var handler = JobSeekerRegistered;
			if (handler != null)
				handler(this, e);
		}

		public delegate void JobSeekerRegisteredHandler(object sender, EventArgs e);

		#endregion
	}
}
