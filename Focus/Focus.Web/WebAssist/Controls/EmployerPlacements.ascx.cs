﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	
	public partial class EmployerPlacements : UserControlBase
	{

		private int _employerPlacementsViewCount;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

			if (!IsPostBack)
			{
				OfficeFilterRow.Visible = App.Settings.OfficesEnabled;
			}
			LocaliseUI();
		}

		/// <summary>
		/// Handles the Clicked event of the FilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FilterButton_Clicked(object sender, EventArgs e)
		{
			EmployerPlacementsListPager.ReturnToFirstPage();
			EmployerPlacementsListView.DataBind();
			EmployerPlacementsUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the DataPaged event of the EmployerPostingsListPager control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.Pager.DataPagedEventArgs"/> instance containing the event data.</param>
		protected void EmployerPlacementsListPager_DataPaged(object sender, Pager.DataPagedEventArgs e)
		{
			EmployerPlacementsUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the Closed event of the ComparePostingToResumeModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.ComparePostingToResumeModal.ModalClosedEventArgs"/> instance containing the event data.</param>
		protected void ComparePostingToResumeModal_Closed(object sender, ComparePostingToResumeModal.ModalClosedEventArgs e)
		{
			if (e.DataUpdated)
			{
				EmployerPlacementsListView.DataBind();
				EmployerPlacementsUpdatePanel.Update();
			}
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the employer placements.
		/// </summary>
		/// <param name="officeGroup">The office group.</param>
		/// <param name="officeId">The office id.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<EmployerPlacementsView> GetEmployerPlacements(OfficeGroup officeGroup, long? officeId, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			var criteria = new PlacementsCriteria
			               	{
			               		FetchOption = CriteriaBase.FetchOptions.PagedList,
			               		OrderBy = orderBy,
			               		PageIndex = pageIndex,
			               		PageSize = maximumRows,
											
												OfficeGroup = (App.Settings.OfficesEnabled) ? officeGroup : OfficeGroup.StatewideOffices,
												OfficeId = officeId
			               	};

			var employerPostingsPaged = ServiceClientLocator.EmployerClient(App).GetEmployerPlacements(criteria);
			_employerPlacementsViewCount = employerPostingsPaged.TotalCount;

			return employerPostingsPaged;
		}

		/// <summary>
		/// Gets the employer postings count.
		/// </summary>
		/// <returns></returns>
		public int GetEmployerPlacementsCount(OfficeGroup officeGroup, long? officeId)
		{
			return _employerPlacementsViewCount;
		}

		/// <summary>
		/// Handles the Selecting event of the EmployerPostingsDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void EmployerPlacementsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (App.Settings.OfficesEnabled)
			{
				e.InputParameters["officeGroup"] = OfficeFilter.OfficeGroup;
				e.InputParameters["officeId"] = OfficeFilter.OfficeId;
			}
			else
			{
				e.InputParameters["officeGroup"] = OfficeGroup.StatewideOffices;
				e.InputParameters["officeId"] = null;
			}
		}

		#endregion

		#region Employer Postings list events

		/// <summary>
		/// Handles the LayoutCreated event of the EmployerPostingsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmployerPlacementsListView_LayoutCreated(object sender, EventArgs e)
		{
			#region Set labels

			((Literal)EmployerPlacementsListView.FindControl("EmployerNameHeaderLiteral")).Text = CodeLocalise("EmployerNameName.ColumnTitle", "#BUSINESS# Name");
			((Literal)EmployerPlacementsListView.FindControl("EmployeeNameLiteral")).Text = CodeLocalise("EmployeeNameName.ColumnTitle", "Job Seeker Name");
			((Literal)EmployerPlacementsListView.FindControl("JobTitleHeaderLiteral")).Text = CodeLocalise("JobTitle.ColumnTitle", "Job Title");
			((Literal)EmployerPlacementsListView.FindControl("PostingDateHeaderLiteral")).Text = CodeLocalise("PostingDate.LastActivity", "Date Hired");
		
			#endregion

			#region Set sorting images

			var employerNameSortAscButton = (Image)EmployerPlacementsListView.FindControl("EmployerNameSortAscButton");
			var employerNameSortDescButton = (Image)EmployerPlacementsListView.FindControl("EmployerNameSortDescButton");
			var nameSortAscButton = (Image)EmployerPlacementsListView.FindControl("NameSortAscButton");
			var nameSortDescButton = (Image)EmployerPlacementsListView.FindControl("NameSortDescButton");
			var jobTitleSortAscButton = (Image)EmployerPlacementsListView.FindControl("JobTitleSortAscButton");
			var jobTitleSortDescButton = (Image)EmployerPlacementsListView.FindControl("JobTitleSortDescButton");

			employerNameSortAscButton.ImageUrl =
				nameSortAscButton.ImageUrl = jobTitleSortAscButton.ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			employerNameSortDescButton.ImageUrl =
				nameSortDescButton.ImageUrl = jobTitleSortDescButton.ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			#endregion
		}

		/// <summary>
		/// Handles the Sorting event of the EmployerPostingsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void EmployerPlacementsListView_Sorting(object sender, ListViewSortEventArgs e)
		{
		  FormatEmployerPlacementsListViewSortingImages(e.SortExpression);
			EmployerPlacementsUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the EmployerPostingsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerPlacementsListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			
			var employer = (EmployerPlacementsView) e.Item.DataItem;

			var employerPostingsListModels = new List<EmployerPlacementsListModel>();

			employerPostingsListModels.AddRange(employer.Placements.Select((t, i) => new EmployerPlacementsListModel { Placement = t, EmployerFirstPosting = (i == 0), EmployerLastPosting = (i == (employer.Placements.Count - 1)), EmployerPostingCount = employer.Placements.Count }));

			var itemRepeater = (Repeater)e.Item.FindControl("EmployerRepeater");

			if(itemRepeater.IsNotNull())
			{
				itemRepeater.DataSource = employerPostingsListModels;
				itemRepeater.DataBind();
			}
		}

		/// <summary>
		/// Handles the ItemDataBound event of the EmployerRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			var model = (EmployerPlacementsListModel) e.Item.DataItem;

			var row = e.Item.FindControl("EmployerJobRow") as HtmlTableRow;

			if (model.EmployerFirstPosting)
			{

				if (model.EmployerPostingCount > 2)
				{
					var showMoreLink = (HyperLink)e.Item.FindControl("ShowMoreLink");
					showMoreLink.Visible = true;

					showMoreLink.Text = (model.EmployerPostingCount == 2) ? CodeLocalise("showMoreLinkSingluar.Text", "+ 1 other")
																														: CodeLocalise("showMoreLinkPlural.Text", "+ {0} others", model.EmployerPostingCount - 1);
				}

				if (row.IsNotNull())
				{
					if (model.EmployerPostingCount > 2)
						row.Attributes["class"] = "parentListItem highlighted";
					else
						row.Attributes["class"] = "parentListItem";
				}
			}
			else
			{
				if (row.IsNotNull())
				{
					if (model.EmployerPostingCount > 2)
						row.Attributes["class"] = "childListItem highlighted";
					else
						row.Attributes["class"] = "childListItem";
				}
				

				if (model.EmployerLastPosting)
				{
					var showLessLink = (HyperLink)e.Item.FindControl("ShowLessLink");
					showLessLink.Visible = true;
						showLessLink.Text = (model.EmployerPostingCount == 2) ? CodeLocalise("showLessLinkSingluar.Text", "- 1 other")
																														: CodeLocalise("showLessLinkPlural.Text", "- {0} others", model.EmployerPostingCount - 1);
					
				}
			}
		}

		/// <summary>
		/// Handles the Command event of the ViewMatchesLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ViewMatchesLinkButton_Command(object sender, CommandEventArgs e)
		{
			var postingId = e.CommandArgument.ToString().ToLong();

			if (postingId.HasValue)
			{
			}
			//ComparePostingToResumeModal.Show(postingId.Value);
		}

		/// <summary>
		/// Handles the Command event of the ViewPostingLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ViewEmployerLinkButton_Command(object sender, CommandEventArgs e)
		{
			var employerId = e.CommandArgument.ToString().ToLong();

			if (employerId.HasValue)
			{
				Response.Redirect(UrlBuilder.AssistEmployerProfile(employerId.Value));
			}
		
		}

		protected void ViewJobSeekerLinkButton_Command(object sender, CommandEventArgs e)
		{
			var jobSeekerId = e.CommandArgument.ToString().ToLong();

			if (jobSeekerId.HasValue)
			{
				Response.Redirect(UrlBuilder.AssistJobSeekerProfile(jobSeekerId.Value));
			}
		}

		protected void ContactEmployerLinkButton_Command(object sender, CommandEventArgs e)
		{
			var lensPostingId = e.CommandArgument.ToString();

			var postingId = ServiceClientLocator.PostingClient(App).GetPostingId(lensPostingId);

			if (postingId.HasValue)
				Response.Redirect(UrlBuilder.PostingEmployerContact(postingId.Value));
		}
		
		#endregion

		/// <summary>
		/// Formats the employer postings list view sorting images.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		private void FormatEmployerPlacementsListViewSortingImages(string orderBy)
		{
			#region Format images

			// Reset image URLs
			  ((ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortAscButton")).ImageUrl =
				((ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			  ((ImageButton)EmployerPlacementsListView.FindControl("NameSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			  ((ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortDescButton")).ImageUrl =
				((ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();
			  ((ImageButton)EmployerPlacementsListView.FindControl("NameSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			  ((ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortAscButton")).Enabled =
				((ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortAscButton")).Enabled =
				((ImageButton)EmployerPlacementsListView.FindControl("NameSortAscButton")).Enabled =
				((ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortDescButton")).Enabled =
				((ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortDescButton")).Enabled = true;
			  ((ImageButton)EmployerPlacementsListView.FindControl("NameSortDescButton")).Enabled = true;

			switch (orderBy)
			{
				case Constants.SortOrders.EmployerNameAsc:
					((ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.EmployerNameDesc:
					((ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortDescButton")).Enabled = false;
					break;
				case Constants.SortOrders.JobTitleAsc:
					((ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.JobTitleDesc:
					((ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortDescButton")).Enabled = false;
					break;
				case Constants.SortOrders.NameAsc:
					((ImageButton)EmployerPlacementsListView.FindControl("NameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployerPlacementsListView.FindControl("NameSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.NameDesc:
					((ImageButton)EmployerPlacementsListView.FindControl("NameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployerPlacementsListView.FindControl("NameSortDescButton")).Enabled = false;
					break;
			}

			#endregion
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FilterButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
		}

		protected void EmployerPlacementsListView_OnPreRender(object sender, EventArgs e)
		{
			//To meet accessibility FVN-1983
			var control = (ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortAscButton");
			if(control.IsNotNull()){
			control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			control = (ImageButton)EmployerPlacementsListView.FindControl("EmployerNameSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)EmployerPlacementsListView.FindControl("NameSortAscButton");
			control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			control = (ImageButton)EmployerPlacementsListView.FindControl("NameSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortAscButton");
			control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			control = (ImageButton)EmployerPlacementsListView.FindControl("JobTitleSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			}
		}
	}

		public class EmployerPlacementsListModel
		{
			public EmployerPlacementsViewDto Placement { get; set; }
			public bool EmployerFirstPosting { get; set; }
			public bool EmployerLastPosting { get; set; }
			public int EmployerPostingCount { get; set; }
		}
	}
