﻿using System;
using Focus.Core.Criteria.Employee;



namespace Focus.Web.WebAssist.Controls
{
	public partial class ComplianceCriteriaBuilder : CriteriaBuilderControlBase
	{
		private ComplianceCriteria _criteria
		{
			get { return GetViewStateValue<ComplianceCriteria>("ComplianceCriteriaBuilder:Criteria"); }
			set { SetViewStateValue("ComplianceCriteriaBuilder:Criteria", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				_criteria = new ComplianceCriteria();
			}
		}

		#region Public methods

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public ComplianceCriteria Unbind()
		{
			return _criteria;
		}

		/// <summary>
		/// Clears the foreign labour certification jobs filter.
		/// </summary>
		public void ClearForeignLabourCertificationJobsFilter()
		{
			_criteria.ForeignLabourCertificationJobs = null;
		}

		/// <summary>
		/// Clears the foreign labour certification jobs with good matches filter.
		/// </summary>
		public void ClearForeignLabourCertificationJobsWithGoodMatchesFilter()
		{
			_criteria.ForeignLabourCertificationJobsWithGoodMatches = null;
		}

		/// <summary>
		/// Clears the court ordered affirmative action filter.
		/// </summary>
		public void ClearCourtOrderedAffirmativeActionFilter()
		{
			_criteria.CourtOrderedAffirmativeAction = null;
		}

		/// <summary>
		/// Clears the federal contract jobs filter.
		/// </summary>
		public void ClearFederalContractJobsFilter()
		{
			_criteria.FederalContractJobs = null;
		}

		#endregion

		#region Add button clicks

		/// <summary>
		/// Handles the Clicked event of the AddForeignLabourCertificationJobsFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddForeignLabourCertificationJobsFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.ForeignLabourCertificationJobs = true;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																										{
																											DisplayText = CodeLocalise("ForeignLabourCertificationJobs.Text", "Compliant with foregin labour jobs"),
																											OverwriteExistingValue = true,
																											Type = CriteriaElementType.ComplianceForeignLabourCertificationJobs
																										}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddForeignLabourCertificationJobsWithGoodMatchesFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddForeignLabourCertificationJobsWithGoodMatchesFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.ForeignLabourCertificationJobsWithGoodMatches = true;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("ForeignLabourCertificationJobsWithGoodMatches.Text", "Compliant with foregin labour jobs with good matches"),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.ComplianceForeignLabourCertificationJobsWithGoodMatches
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddCourtOrderedAffirmativeActionFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddCourtOrderedAffirmativeActionFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.CourtOrderedAffirmativeAction = true;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("CourtOrderedAffirmativeAction.Text", "Compliant with court ordered affirmative action"),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.ComplianceCourtOrderedAffirmativeAction
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddFederalContractJobsFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddFederalContractJobsFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.FederalContractJobs = true;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("FederalContractJobs.Text", "Compliant with federal contract jobs"),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.ComplianceFederalContractJobs
																									}));
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			AddForeignLabourCertificationJobsFilterButton.Text
				= AddForeignLabourCertificationJobsWithGoodMatchesFilterButton.Text
				= AddCourtOrderedAffirmativeActionFilterButton.Text
				= AddFederalContractJobsFilterButton.Text = CodeLocalise("Global.Add.Text", "Add");
		}

		#endregion
	}
}