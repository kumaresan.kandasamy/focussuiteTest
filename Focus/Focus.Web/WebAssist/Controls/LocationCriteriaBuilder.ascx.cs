﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Core.Criteria.Employee;
using Focus.Web.Code;
using Framework.Core;
using Focus.Common.Extensions;

namespace Focus.Web.WebAssist.Controls
{
	public partial class LocationCriteriaBuilder : CriteriaBuilderControlBase
	{
		private DistanceUnits _distanceUnits
		{
			get { return GetViewStateValue<DistanceUnits>("LocationCriteriaBuilder:DistanceUnits"); }
			set { SetViewStateValue("LocationCriteriaBuilder:DistanceUnits", value); }
		}

		private ReportLocationCriteria _criteria
		{
			get { return GetViewStateValue<ReportLocationCriteria>("LocationCriteriaBuilder:Criteria"); }
			set { SetViewStateValue("LocationCriteriaBuilder:Criteria", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				BindRadiusDistanceDropDown();
				LocaliseUI();
				ApplyBranding();
				_criteria = new ReportLocationCriteria();
			}
		}

		#region Public methods

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public ReportLocationCriteria Unbind()
		{
			return _criteria;
		}

		/// <summary>
		/// Clears the location radius filter.
		/// </summary>
		public void ClearLocationRadiusFilter()
		{
			ClearLocationRadiusControls();
			_criteria.Distance = 0;
			_criteria.PostalCode = null;
		}

		/// <summary>
		/// Clears the county filter.
		/// </summary>
		/// <param name="county">The county.</param>
		public void ClearCountyFilter(string county)
		{
			foreach (var countyFilter in _criteria.Counties.Where(countyFilter => countyFilter == county))
			{
				_criteria.Counties.Remove(countyFilter);
				break;
			}
		}

		/// <summary>
		/// Clears the office filter.
		/// </summary>
		/// <param name="office">The office.</param>
		public void ClearOfficeFilter(string office)
		{
			foreach (var officeFilter in _criteria.Offices.Where(officeFilter => officeFilter == office))
			{
				_criteria.Offices.Remove(officeFilter);
				break;
			}
		}

		/// <summary>
		/// Clears the workforce investment board filter.
		/// </summary>
		/// <param name="workforceInvestmentBoard">The workforce investment board.</param>
		public void ClearWorkforceInvestmentBoardFilter(string workforceInvestmentBoard)
		{
			foreach (var workforceInvestmentBoardFilter in _criteria.WorkforceInvestmentBoards.Where(workforceInvestmentBoardFilter => workforceInvestmentBoardFilter == workforceInvestmentBoard))
			{
				_criteria.WorkforceInvestmentBoards.Remove(workforceInvestmentBoardFilter);
				break;
			}
		}

		#endregion

		#region Add button clicks

		/// <summary>
		/// Handles the Clicked event of the AddRadiusFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddRadiusFilterButton_Clicked(object sender, EventArgs e)
		{
			if (RadiusZIPCodeTextBox.TextTrimmed().IsNullOrEmpty()) return;
			
			_criteria.Distance = RadiusDistanceDropDown.SelectedValueToLong();
			_criteria.DistanceUnits = _distanceUnits;
			_criteria.PostalCode = RadiusZIPCodeTextBox.TextTrimmed();
			ClearLocationRadiusControls();
			
			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
			                                           	{
			                                           		DisplayText = CodeLocalise("LocationRadiusFilter.Text", "{0} {1} from {2}", _criteria.Distance, _criteria.DistanceUnits, _criteria.PostalCode),
			                                           		OverwriteExistingValue = true,
			                                           		Type = CriteriaElementType.LocationRadius
			                                           	}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddCountyFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddCountyFilterButton_Clicked(object sender, EventArgs e)
		{
			if (CountyTextBox.TextTrimmed().IsNullOrEmpty()) return;

			var county = CountyTextBox.TextTrimmed();
			
			if(_criteria.Counties.IsNull()) _criteria.Counties = new List<string>();
			
			_criteria.Counties.Add(county);
			CountyTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
			                                           	{
			                                           		DisplayText = CodeLocalise("CountyFilter.Text", "Within the county of {0}", county),
			                                           		OverwriteExistingValue = false,
			                                           		Type = CriteriaElementType.LocationCounty,
																										Parameters = new List<string> {county}
			                                           	}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddOfficeFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddOfficeFilterButton_Clicked(object sender, EventArgs e)
		{
			if (OfficeTextBox.TextTrimmed().IsNullOrEmpty()) return;

			var office = OfficeTextBox.TextTrimmed();

			if (_criteria.Offices.IsNull()) _criteria.Offices = new List<string>();

			_criteria.Offices.Add(office);
			OfficeTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
			{
				DisplayText = CodeLocalise("OfficeFilter.Text", "Within the {0} office region", office),
				OverwriteExistingValue = false,
				Type = CriteriaElementType.LocationOffice,
				Parameters = new List<string> { office }
			}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddWorkforceInvestmentBoardFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddWorkforceInvestmentBoardFilterButton_Clicked(object sender, EventArgs e)
		{
			if (WorkforceInvestmentBoardTextBox.TextTrimmed().IsNullOrEmpty()) return;

			var workforceInvestmentBoard = WorkforceInvestmentBoardTextBox.TextTrimmed();

			if (_criteria.WorkforceInvestmentBoards.IsNull()) _criteria.WorkforceInvestmentBoards = new List<string>();

			_criteria.WorkforceInvestmentBoards.Add(workforceInvestmentBoard);
			WorkforceInvestmentBoardTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
			{
				DisplayText = CodeLocalise("OfficeFilter.Text", "Within the {0} workforce investment board region", workforceInvestmentBoard),
				OverwriteExistingValue = false,
				Type = CriteriaElementType.LocationWorkforceInvestmentBoard,
				Parameters = new List<string> { workforceInvestmentBoard }
			}));
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Binds the location distance drop down.
		/// </summary>
		private void BindRadiusDistanceDropDown()
		{
			RadiusDistanceDropDown.Items.Clear();

			var distanceUnit = (_distanceUnits.IsNotNull()) ? _distanceUnits : GetDistanceUnits();
			var distanceUnits = CodeLocalise(distanceUnit, "Undefined");
			RadiusDistanceDropDown.Items.Add(new ListItem(string.Format("5 {0}", distanceUnits), "5"));
			RadiusDistanceDropDown.Items.Add(new ListItem(string.Format("10 {0}", distanceUnits), "10"));
			RadiusDistanceDropDown.Items.Add(new ListItem(string.Format("25 {0}", distanceUnits), "25"));
			RadiusDistanceDropDown.Items.Add(new ListItem(string.Format("50 {0}", distanceUnits), "50"));
			RadiusDistanceDropDown.Items.Add(new ListItem(string.Format("100 {0}", distanceUnits), "100"));
		}

		/// <summary>
		/// Clears the location radius controls.
		/// </summary>
		private void ClearLocationRadiusControls()
		{
			BindRadiusDistanceDropDown();
			RadiusZIPCodeTextBox.Text = "";
		}

		/// <summary>
		/// Gets the distance units.
		/// </summary>
		private DistanceUnits GetDistanceUnits()
		{
			var distanceUnits = App.Settings.DistanceUnits;
			_distanceUnits = distanceUnits;
			return distanceUnits;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			AddRadiusFilterButton.Text = AddCountyFilterButton.Text = AddOfficeFilterButton.Text = AddWorkforceInvestmentBoardFilterButton.Text = CodeLocalise("Global.Add.Text", "Add");
		}

		private void ApplyBranding()
		{
			LocationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LocationPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			LocationPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}

		#endregion

	}
}