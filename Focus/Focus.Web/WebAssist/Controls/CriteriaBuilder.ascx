﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CriteriaBuilder.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.CriteriaBuilder" %>
<%@ Import Namespace="Focus.Web.WebAssist.Controls" %>

<%@ Register TagPrefix="uc" TagName="LocationCriteriaBuilder" Src="~/WebAssist/Controls/LocationCriteriaBuilder.ascx" %>
<%@ Register TagPrefix="uc" TagName="JobCharacteristicsCriteriaBuilder" Src="~/WebAssist/Controls/JobCharacteristicsCriteriaBuilder.ascx" %>
<%@ Register TagPrefix="uc" TagName="EmployerCharacteristicsCriteriaBuilder" Src="~/WebAssist/Controls/EmployerCharacteristicsCriteriaBuilder.ascx" %>
<%@ Register TagPrefix="uc" TagName="ComplianceCriteriaBuilder" Src="~/WebAssist/Controls/ComplianceCriteriaBuilder.ascx" %>

<div class="subItemsPanel">
	<h3><%= Title%></h3>
	<table width="100%">
		<tr>
			<td width="150px"><i><%= Instructions%></i></td>
			<td><asp:button ID="CommandButton" runat="server" CssClass="button2 right" OnClick="CommandButton_Click"/></td>
		</tr>
	</table>
	<br/>
	<h4><%= HtmlLocalise("ActiveCriteria.Tiltle", "Active selections") %></h4>
	<asp:UpdatePanel ID="CriteriaBuilderUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:ListView ID="ActiveCriteriaList" runat="server" >
				<LayoutTemplate>
					<table class="deletableListItemTable">
						<tr runat="server" ID="itemPlaceHolder"></tr>
					</table>
				</LayoutTemplate>
				<ItemTemplate>
					<tr>
						<td class="column1"><%# ((CriteriaBuilderControlBase.CriteriaElement)Container.DataItem).DisplayText %>
						</td>
						<td class="column2">
							<asp:ImageButton ID="ItemsListRemoveImageButton" runat="server"  OnCommand="ActiveCriteriaListRemoveButton_Command" CommandArgument="<%# Container.DataItemIndex %>" 
																CommandName="Remove" ImageUrl="<%# UrlBuilder.ButtonDeleteIcon() %>" CausesValidation="False" />
						</td>
					</tr>
				</ItemTemplate>
				<EmptyDataTemplate><%= HtmlLocalise("NoActiveCriteria.Text", "No active selections") %></EmptyDataTemplate>
			</asp:ListView>
			<uc:LocationCriteriaBuilder ID="LocationCriteria"	runat="server" OnCriteriaAdded="CriteriaAdded" />
			<uc:JobCharacteristicsCriteriaBuilder ID="JobCharacteristicsCriteria" runat="server" OnCriteriaAdded="CriteriaAdded" />
			<uc:EmployerCharacteristicsCriteriaBuilder ID="EmployerCharacteristicsCriteria" runat="server" OnCriteriaAdded="CriteriaAdded" />
			<uc:ComplianceCriteriaBuilder ID="ComplianceCriteria" runat="server" OnCriteriaAdded="CriteriaAdded" />
			<script type="text/javascript">
				var prm = Sys.WebForms.PageRequestManager.getInstance();
				prm.add_pageLoaded(criteriaBuilderUpdated);

				function criteriaBuilderUpdated(sender, args) {
					$('.inFieldLabel > label').inFieldLabels();
				}
			</script>
		</ContentTemplate>
	</asp:UpdatePanel>
</div>
<br/>