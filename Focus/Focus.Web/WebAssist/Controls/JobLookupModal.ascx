﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobLookupModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.JobLookupModal" %>
<%@ Import Namespace="Focus.Core.Views" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel"
												PopupDragHandleControlID="ModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal">
	<h2><%= HtmlLocalise("Title.Label", "Look up job for selected #CANDIDATETYPES#:LOWER")%></h2>

	<asp:Panel ID="SearchPanel" runat="server">
		<asp:Panel runat="server" ClientIDMode="Static" ID="ModalPanelHeader">
		<table role="presentation">
			<tr>
				<td style="width:150px;"><%= HtmlLabel(JobTitleTextBox,"JobTitle.Label", "Job title") %></td>
				<td><asp:TextBox runat="server" ID="JobTitleTextBox" Width="200" /></td>	
				<td rowspan="2" style="vertical-align:middle;">
					<asp:Button ID="GoButton" runat="server" CssClass="button3" OnClick="GoButton_Clicked" ValidationGroup="SearchTypeOne" />
				</td>
			</tr>
			<tr>
				<td><%= HtmlLabel(EmployerNameTextBox, "EmployerName.Label", "#BUSINESS# name")%></td>
				<td><asp:TextBox runat="server" ID="EmployerNameTextBox" Width="200" /></td>	
			</tr>
			<tr>
				<td><%= HtmlLabel("CreationDate.Label", "Creation date") %></td>
				<td>
					<table role="presentation">
						<tr>
							<td><i><%= HtmlLabel("CreationDateFrom.Label", "from") %></i>&nbsp;</td>
							<td>
								<focus:LocalisedLabel runat="server" ID="FromMonthDropDownListLabel" AssociatedControlID="FromMonthDropDownList" LocalisationKey="MonthFrom" DefaultText="Month from" CssClass="sr-only"/>
								<asp:DropDownList ID="FromMonthDropDownList" runat="server" />&nbsp;
							</td>
							<td>
								<focus:LocalisedLabel runat="server" ID="FromYearDropDownListLabel" AssociatedControlID="FromYearDropDownList" LocalisationKey="YearFrom" DefaultText="Year from" CssClass="sr-only"/>
								<asp:DropDownList ID="FromYearDropDownList" runat="server" />
							</td>
						</tr>
						<tr>
							<td><i><%= HtmlLabel("CreationDateTo.Label", "to") %></i>&nbsp;</td>
							<td>
								<focus:LocalisedLabel runat="server" ID="ToMonthDropDownListLabel" AssociatedControlID="ToMonthDropDownList" LocalisationKey="MonthTo" DefaultText="Month to" CssClass="sr-only"/>
								<asp:DropDownList ID="ToMonthDropDownList" runat="server" />&nbsp;
							</td>
							<td>
								<focus:LocalisedLabel runat="server" ID="ToYearDropDownListLabel" AssociatedControlID="ToYearDropDownList" LocalisationKey="YearTo" DefaultText="Year to" CssClass="sr-only"/>
								<asp:DropDownList ID="ToYearDropDownList" runat="server" />
							</td>
						</tr>
					</table>				
				</td>	
				<td></td>
			</tr>
			<tr>
				<td colspan="3"><hr /></td>
			</tr>
			<tr>
				<td style="width:150px;"><%= HtmlLabel(JobIdTextBox,"JobId.Label", "Job ID") %></td>
				<td><asp:TextBox runat="server" ID="JobIdTextBox" Width="200" /></td>	
				<td><asp:Button ID="GoJobIdButton" runat="server" CssClass="button3" OnClick="GoJobIdButton_Clicked" ValidationGroup="SearchTypeTwo" /></td>
			</tr>		
		</table>
</asp:Panel>
		<br />
		
		<asp:CustomValidator ID="SearchTypeOneValidator" runat="server" ClientValidationFunction="validateSearchTypeOne"
													 ValidationGroup="SearchTypeOne" ValidateEmptyText="True" CssClass="error" Display="Dynamic"/>
		<asp:CustomValidator ID="SearchTypeOneFromToValidator" runat="server" ClientValidationFunction="validateSearchTypeOneFromTo"
													 ValidationGroup="SearchTypeOne" ValidateEmptyText="True" CssClass="error" Display="Dynamic"/>													 
		<asp:CustomValidator ID="SearchTypeTwoValidator" runat="server" ClientValidationFunction="validateSearchTypeTwo"
													 ValidationGroup="SearchTypeTwo" ValidateEmptyText="True" CssClass="error" Display="Dynamic"/>

		<br />

		<p>
			<asp:Button ID="SearchCancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />	
		</p>
	</asp:Panel>

	<asp:Panel ID="SearchResultsPanel" runat="server" Visible="false">
		<p>
			<asp:LinkButton ID="SearchAgainLinkButton" runat="server" OnClick="SearchAgainLinkButton_Clicked" />
		</p>

		<table ID="JobsSearchHeaderTable" runat="server" Visible="false" style="width:100%;" role="presentation">
			<tr>
				<td width="100%" colspan="2"><strong><%= HtmlLocalise("SearchResults.Title", "#EMPLOYMENTTYPE#")%></strong>&nbsp;&nbsp;<asp:literal ID="ResultCount" runat="server" /></td>				
			</tr>
		</table>
		<table ID="JobsSearchTable" runat="server" Visible="false" style="width:100%;" role="presentation">
			<tr>
				<td style="width:420px;"><%= HtmlLocalise("JobTitle.ColumnTitle", "Job title")%></td>
				<td style="width:170px;"><%= HtmlLocalise("Employer.ColumnTitle", "#BUSINESS#")%></td>
			</tr>
			<tr>
				<td colspan="2"><hr/></td>
			</tr>	
		</table>
		<asp:ListView ID="JobsList" runat="server" ItemPlaceholderID="JobsPlaceHolder" DataKeyNames="JobId,JobTitle">
			<LayoutTemplate>
				<div style="height: 125px; overflow-y: scroll; width: 620px;">
					<table style="width:600px;">
						<asp:PlaceHolder ID="JobsPlaceHolder" runat="server" />
					</table>
				</div>
			</LayoutTemplate>
			<ItemTemplate>
				<tr>
					<td style="width:20px;"><input id="SelectorRadioButton" name="SelectorRadioButtons" type="radio" runat="server" onclick="checkUnCheckListItems(this.id);" /></td>
					<td style="width:400px;"><%# ((JobLookupView)Container.DataItem).JobTitle%></td>
					<td style="width:170px;"><%# ((JobLookupView)Container.DataItem).Employer%></td>
				</tr>	
			</ItemTemplate>
			<EmptyDataTemplate>
				<table style="width:600px;">
					<tr>
						<td style="text-align:left;">
							<br />
							<br />
							<%= HtmlLocalise("JobsList.NoResult", "No matches")%></td>
					</tr>
				</table>
			</EmptyDataTemplate>
		</asp:ListView>

		<br />
		
		<asp:CustomValidator ID="JobsListValidator" runat="server" ClientValidationFunction="validateJobsSelected"
													 ValidationGroup="SelectJobSeekers" ValidateEmptyText="True" CssClass="error"/>

		<br />

		<p>
			<asp:Button ID="SearchResultsCancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
			<asp:Button ID="OkButton" runat="server" CssClass="button3" OnClick="OkButton_Clicked" ValidationGroup="SelectJobSeekers" />		
		</p>
	</asp:Panel>
</asp:Panel>


<script type="text/javascript">

	function checkUnCheckListItems(CheckedRadioId)
	{
	  var radios = $('input[name$="SelectorRadioButtons"]');

		for (i = 0; i <= radios.length - 1; i++)
			if (radios[i].id != CheckedRadioId)
				radios[i].checked = false;
	}

	function validateJobsSelected(src, args)
	{
		args.IsValid = checkJobsSelected();
	}

	function checkJobsSelected()
	{
	  var radios = $('input[name$="SelectorRadioButtons"]');
		var checked = false;

		for (i = 0; i <= radios.length - 1; i++)
			if (radios[i].checked)
				checked = true;

		return checked;
	}

	function validateSearchTypeOne(src, args)
	{
		args.IsValid = true;

		var jobTitleTextBox = $("#<%= JobTitleTextBox.ClientID %>").val();
		var employerNameTextBox = $("#<%= EmployerNameTextBox.ClientID %>").val();
		var fromMonthDropDownList = $("#<%= FromMonthDropDownList.ClientID %>").val();
		var fromYearDropDownList = $("#<%= FromYearDropDownList.ClientID %>").val();
		var toMonthDropDownList = $("#<%= ToMonthDropDownList.ClientID %>").val();
		var toYearDropDownList = $("#<%= ToYearDropDownList.ClientID %>").val();

		if (fromMonthDropDownList != "" && fromYearDropDownList == "") fromMonthDropDownList = "";
		if (fromYearDropDownList != "" && fromMonthDropDownList == "") fromYearDropDownList = "";

		if (toMonthDropDownList != "" && toYearDropDownList == "") toMonthDropDownList = "";
		if (toYearDropDownList != "" && toMonthDropDownList == "") toYearDropDownList = "";

		if ((jobTitleTextBox + employerNameTextBox + fromMonthDropDownList + fromYearDropDownList + toMonthDropDownList + toYearDropDownList) == "")
			args.IsValid = false;
	}

	function validateSearchTypeOneFromTo(src, args)
	{
		args.IsValid = true;

		var fromMonthDropDownList = $("#<%= FromMonthDropDownList.ClientID %>").val();
		var fromYearDropDownList = $("#<%= FromYearDropDownList.ClientID %>").val();
		var toMonthDropDownList = $("#<%= ToMonthDropDownList.ClientID %>").val();
		var toYearDropDownList = $("#<%= ToYearDropDownList.ClientID %>").val();

		if (fromMonthDropDownList != "" && fromYearDropDownList == "") fromMonthDropDownList = "";
		if (fromYearDropDownList != "" && fromMonthDropDownList == "") fromYearDropDownList = "";

		if (toMonthDropDownList != "" && toYearDropDownList == "") toMonthDropDownList = "";
		if (toYearDropDownList != "" && toMonthDropDownList == "") toYearDropDownList = "";

		if ((fromMonthDropDownList + fromYearDropDownList) == "" && (toMonthDropDownList + toYearDropDownList) != "")
		{
			args.IsValid = false;
			return;
		}

		if ((fromMonthDropDownList + fromYearDropDownList) != "" && (toMonthDropDownList + toYearDropDownList) == "")
		{
			args.IsValid = false;
			return;
		}

		if ((fromMonthDropDownList + fromYearDropDownList) != "" && (toMonthDropDownList + toYearDropDownList) != "")
		{
			var fromDate = (parseInt(fromYearDropDownList) * 1000) + parseInt(fromMonthDropDownList);
			var toDate = (parseInt(toYearDropDownList) * 1000) + parseInt(toMonthDropDownList);

			if (fromDate > toDate)
				args.IsValid = false;
		}
	}

	function validateSearchTypeTwo(src, args)
	{
		args.IsValid = true;

		var jobId = $("#<%= JobIdTextBox.ClientID %>").val();

		if (jobId == "")
			args.IsValid = false;
		else
		{
			jobId = parseInt(jobId);

			if (isNaN(jobId) || jobId <= 0)
				args.IsValid = false;
		}
	}

</script>