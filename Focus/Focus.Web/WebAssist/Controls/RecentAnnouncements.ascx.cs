﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core.Views;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class RecentAnnouncements : UserControlBase
	{
		private int _announcementCount;

		private const string DefaultSortOrder = "createdon asc";

		private string SortOrder
		{
			get { return GetViewStateValue("RecentAnnouncements:SortOrder", DefaultSortOrder); }
			set { SetViewStateValue("RecentAnnouncements:SortOrder", value); }
		}

		/// <summary>
		/// Handles the ItemDataBound event of the OfficeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void AnnouncementList_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var listItem = e.Item;
			var messageView = (MessageView)listItem.DataItem;

			var datePostedLiteral = (Literal)listItem.FindControl("DatePostedLiteral");
			datePostedLiteral.Text = messageView.CreatedOn.ToString(CultureInfo.CurrentCulture);

			var postedByLiteral = (Literal)listItem.FindControl("PostedByLiteral");
			postedByLiteral.Text = string.Concat(messageView.CreatedByFirstName, " ", messageView.CreatedByLastName);

			var announcementLiteral = (Literal)listItem.FindControl("AnnouncementLiteral");
			if (messageView.Text.Length > 100)
			{
				announcementLiteral.Text = string.Concat(messageView.Text.TruncateString(97), "...");

				var showMoreLink = (HyperLink) listItem.FindControl("ShowMoreLink");
				showMoreLink.Text = CodeLocalise("ShowMoreLink.Text", "More");
				showMoreLink.Attributes.Add("onclick", string.Format("RecentAnnouncements_ShowFullMessage({0});", messageView.Id.ToString(CultureInfo.InvariantCulture)));
				showMoreLink.Attributes.Add("onkeypress", string.Format("RecentAnnouncements_ShowFullMessage({0});", messageView.Id.ToString(CultureInfo.InvariantCulture)));
				showMoreLink.Visible = true;

				var hideLink = (HyperLink)listItem.FindControl("HideLink");
				hideLink.Text = CodeLocalise("HideLink.Text", "Hide");
				hideLink.Attributes.Add("onclick", string.Format("RecentAnnouncements_HideFullMessage({0});", messageView.Id.ToString(CultureInfo.InvariantCulture)));
				hideLink.Attributes.Add("onkeypress", string.Format("RecentAnnouncements_HideFullMessage({0});", messageView.Id.ToString(CultureInfo.InvariantCulture)));
				hideLink.Visible = true;

				var fullAnnouncementLiteral = (Literal)listItem.FindControl("FullAnnouncementLiteral");
				fullAnnouncementLiteral.Text = messageView.Text;
			}
			else
			{
				announcementLiteral.Text = messageView.Text;
			}

			var displayedToLiteral = (Literal)listItem.FindControl("DisplayedToLiteral");
			displayedToLiteral.Text = CodeLocalise(messageView.Audience, true);

			var removeButton = (Button)e.Item.FindControl("RemoveButton");
			removeButton.CommandArgument = messageView.Id.ToString(CultureInfo.InvariantCulture);
			removeButton.Text = CodeLocalise("RemoveButton.Text", "Remove");
		}

		/// <summary>
		/// Handles the LayoutCreated event of the OfficeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void AnnouncementList_LayoutCreated(object sender, EventArgs e)
		{
			// Set table headings
			((Literal)AnnouncementList.FindControl("DatePostedHeader")).Text = CodeLocalise("DatePostedHeader.Text", "Date posted");
			((Literal)AnnouncementList.FindControl("PostedByHeader")).Text = CodeLocalise("PostedByHeader.Text", "Posted by");
			((Literal)AnnouncementList.FindControl("AnnouncementHeader")).Text = CodeLocalise("AnnouncementHeader.Text", "Announcement");
			((Literal)AnnouncementList.FindControl("DisplayedToHeader")).Text = CodeLocalise("DisplayedToHeader.Text", "Displayed to");
		}

		/// <summary>
		/// Handles the Sorting event of the control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void AnnouncementList_Sorting(object sender, ListViewSortEventArgs e)
		{
			SortOrder = e.SortExpression;
			FormatSortingImages();
		}

		/// <summary>
		/// Resends the message
		/// </summary>
		/// <param name="sender">The button raising the request</param>
		/// <param name="e">The command arguments</param>
		protected void RemoveButton_OnCommand(object sender, CommandEventArgs e)
		{
			var id = long.Parse(e.CommandArgument.ToString());
			ServiceClientLocator.CoreClient(App).ExpireMessage(id);

			Bind();
		}

		/// <summary>
		/// Binds the data
		/// </summary>
		public void Bind()
		{
			AnnouncementList.DataBind();

			FormatSortingImages();
		}

		/// <summary>
		/// Gets the announcements.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		/// <returns></returns>
		public List<MessageView> GetAnnouncements(string orderBy)
		{
			if (orderBy.IsNotNullOrEmpty())
				SortOrder = orderBy;

			var announcements = ServiceClientLocator.CoreClient(App).GetRecentMessages(SortOrder);
			_announcementCount = announcements.Count;

			return announcements;
		}

		/// <summary>
		/// Gets the Announcement count.
		/// </summary>
		/// <returns></returns>
		public int GetAnnouncementCount()
		{
			return _announcementCount;
		}

		/// <summary>
		/// Gets the Announcement count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetAnnouncementCount(int criteria)
		{
			return _announcementCount;
		}

		/// <summary>
		/// Formats the icons for sorting the columns in the header
		/// </summary>
		private void FormatSortingImages()
		{
			var datePostedAscButton = (ImageButton) AnnouncementList.FindControl("DatePostedSortAscButton");

			if (datePostedAscButton.IsNull())
				return;

			var datePostedDescButton = (ImageButton) AnnouncementList.FindControl("DatePostedSortDescButton");
			var postedByAscButton = (ImageButton) AnnouncementList.FindControl("PostedBySortAscButton");
			var postedByDescButton = (ImageButton) AnnouncementList.FindControl("PostedBySortDescButton");
			var announcementAscButton = (ImageButton) AnnouncementList.FindControl("AnnouncementSortAscButton");
			var announcementDescButton = (ImageButton) AnnouncementList.FindControl("AnnouncementSortDescButton");
			var displayedToSortAscButton = (ImageButton) AnnouncementList.FindControl("DisplayedToSortAscButton");
			var displayedToSortDescButton = (ImageButton) AnnouncementList.FindControl("DisplayedToSortDescButton");

			// Reset image URLs
			datePostedAscButton.ImageUrl =
				postedByAscButton.ImageUrl =
				announcementAscButton.ImageUrl =
				displayedToSortAscButton.ImageUrl = UrlBuilder.ActivityOnSortAscImage();

			datePostedDescButton.ImageUrl =
				postedByDescButton.ImageUrl =
				announcementDescButton.ImageUrl =
				displayedToSortDescButton.ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			// Reenable the buttons
			datePostedAscButton.Enabled =
				postedByAscButton.Enabled =
				announcementAscButton.Enabled =
				displayedToSortAscButton.Enabled =
				datePostedDescButton.Enabled =
				postedByDescButton.Enabled =
				announcementDescButton.Enabled =
				displayedToSortDescButton.Enabled = true;

			switch (SortOrder.ToLower())
			{
				case "createdon asc":
					datePostedAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
					datePostedAscButton.Enabled = false;
					break;
				case "createdon desc":
					datePostedDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
					datePostedDescButton.Enabled = false;
					break;
				case "createdbyfirstname asc,createdbylastname asc":
					postedByAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
					postedByAscButton.Enabled = false;
					break;
				case "createdbyfirstname desc,createdbylastname desc":
					postedByDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
					postedByDescButton.Enabled = false;
					break;
				case "text asc":
					announcementAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
					announcementAscButton.Enabled = false;
					break;
				case "text desc":
					announcementDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
					announcementDescButton.Enabled = false;
					break;
				case "audience asc":
					displayedToSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
					displayedToSortAscButton.Enabled = false;
					break;
				case "audience desc":
					displayedToSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
					displayedToSortDescButton.Enabled = false;
					break;
			}
		}
	}
}