﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class SendMessages : UserControlBase
	{
		#region Event delegates

		public delegate void SavedMessageChangedHandler(object sender, EventArgs eventArgs);
		public event SavedMessageChangedHandler SavedMessageChanged;

		public delegate void CompletedHandler(object sender, SendMessageEventArgs eventArgs);
		public event CompletedHandler Completed;

		#endregion

		#region Properties

		public class SendMessageEventArgs : EventArgs
		{
			public string Title { get; set; }
			public string Message { get; set; }
		}

		private ActionTypes ActionType
		{
			get { return GetViewStateValue<ActionTypes>("SendMessages:ActionType"); }
			set { SetViewStateValue("SendMessages:ActionType", value); }
		}

		private List<long> JobSeekerIds
		{
			get { return GetViewStateValue<List<long>>("SendMessages:JobSeekerIds"); }
			set { SetViewStateValue("SendMessages:JobSeekerIds", value); }
		}

		private EmailTemplateDto EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateDto>("SendMessages:EmailTemplate"); }
			set { SetViewStateValue("SendMessages:EmailTemplate", value); }
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				BindSavedMessagesDropDown();
				Localise();
			}
		}

		/// <summary>
		/// Binds the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="jobSeekerIds">The job seeker ids.</param>
		/// <param name="jobUrl">The job URL.</param>
		public void Bind(ActionTypes action, List<long> jobSeekerIds = null, string jobUrl = "")
		{
			ActionType = action;

			JobSeekerIds = jobSeekerIds;

			// Reset saved messages dropdown
			SavedMessagesDropDown.SelectedIndex = 0;

			EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplate(EmailTemplateTypes.SendMessageJobSeeker);

			SubjectRequiredDiv.Visible = SubjectDiv.Visible = EmailCopyDiv.Visible = action != ActionTypes.CreateCandidateHomepageAlerts;

			if (App.Settings.Theme == FocusThemes.Workforce)
				ResetComposeMessageForm();

			if (jobUrl.IsNotNullOrEmpty())
				MessageTextbox.Text = Environment.NewLine + jobUrl;
		}

		#region Events

		/// <summary>
		/// Handles the SelectedIndexChanged event of the SavedMessagesDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SavedMessagesDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var messages = (SavedMessagesDropDown.SelectedValueToLong() > 0) ? ServiceClientLocator.CoreClient(App).GetSavedMessageTexts(SavedMessagesDropDown.SelectedValueToLong()) : null;

			if (messages.IsNotNullOrEmpty())
			{
				var defaultLocalisation = ServiceClientLocator.CoreClient(App).GetDefaultCulture();
				var savedMessageText = messages.FirstOrDefault(x => x.LocalisationId == defaultLocalisation.Id);
				if (savedMessageText != null) MessageTextbox.Text = savedMessageText.Text;
			}
			else
			{
				MessageTextbox.Text = string.Empty;
			}

			OnSavedMessageChanged(new EventArgs());
		}

		/// <summary>
		/// Handles the Clicked event of the SendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendMessageButton_Clicked(object sender, EventArgs e)
		{
			SendMessage();
		}

		/// <summary>
		/// Handles the Clicked event of the SaveMessageTextButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveMessageTextButton_Clicked(object sender, EventArgs e)
		{
			// Add the default message text, set the LocalisationId to zero and the service will replace it with the correct one
			var messageTexts = new List<SavedMessageTextDto>
      {
        new SavedMessageTextDto {SavedMessageId = 0, LocalisationId = 0, Text = MessageTextbox.TextTrimmed()}
      };

			var message = new SavedMessageDto { Audience = MessageAudiences.JobSeeker, Name = SavedMessageNameTextBox.TextTrimmed(), UserId = App.User.UserId };

			ServiceClientLocator.CoreClient(App).SaveMessageTexts(message, messageTexts);

			// Rebind SavedMessagesDropDown to show new value
			BindSavedMessagesDropDown();

			ResetComposeMessageForm();

			OnCompleted(new SendMessageEventArgs
			{
				Title = CodeLocalise("MessageTextsSavedConfirmation.Title", "Message text saved"),
				Message = CodeLocalise("MessageTextsSavedConfirmation.Body", "The message text has been saved.")
			});
		}

		/// <summary>
		/// Raises the <see cref="Completed" /> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompleted(SendMessageEventArgs eventArgs)
		{
			if (Completed != null)
				Completed(this, eventArgs);
		}

		/// <summary>
		/// Raises the <see cref="E:SavedMessageChanged" /> event.
		/// </summary>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected virtual void OnSavedMessageChanged(EventArgs e)
		{
			if (SavedMessageChanged != null)
				SavedMessageChanged(this, e);
		}

		#endregion

		#region Helpers

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			SavedMessageNameRequired.ErrorMessage = CodeLocalise("MessageRequiredSaveText.ErrorMessage", "Please enter a name for your saved message");

			SubjectRequired.ErrorMessage = CodeLocalise("SubjectRequired.ErrorMessage", "Please enter a subject");
			MessageRequired.ErrorMessage = SavedMessageMessageRequired.ErrorMessage = CodeLocalise("MessageRequired.ErrorMessage", "Please enter a message");

			SendMessageButton.Text = CodeLocalise("SendMessageButton.Text.NoEdit", "Send message");
			SaveMessageTextButton.Text = CodeLocalise("SaveMessageTextButton.Text.NoEdit", "Save text to my messages");
		}

		/// <summary>
		/// Sends the message.
		/// </summary>
		private void SendMessage()
		{
			var showConfirmation = true;

			// Only need to get the IDs from session if we're running EDU - otherwise the IDs are passed into the send message modal
			if (App.Settings.Theme == FocusThemes.Education)
				JobSeekerIds = GetJobSeekerIds();

			switch (ActionType)
			{
				case ActionTypes.SendCandidateEmails:
					showConfirmation = SendEmailAlerts();
					break;

				case ActionTypes.CreateCandidateHomepageAlerts:
					SendHomepageAlerts();
					break;
			}

			ResetComposeMessageForm();

			if (showConfirmation)
			{
				OnCompleted(new SendMessageEventArgs
				{
					Title = CodeLocalise("MessageSentConfirmation.Title", "Message sent"),
					Message = CodeLocalise("MessageSentConfirmation.Body", "Your message has been sent to the selected users.")
				});
			}
		}

		/// <summary>
		/// Sends the email alerts.
		/// </summary>
		private bool SendEmailAlerts()
		{
			var messageSubject = SubjectTextBox.TextTrimmed();
			var messageBody = MessageTextbox.TextTrimmed();
			var messageSender = App.Settings.UseOfficeAddressToSendMessages
																? ServiceClientLocator.EmployerClient(App).GetHiringManagerOfficeEmail(Convert.ToInt64(App.User.PersonId))
																: ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplate);

			// Batch up the emails and send in one go
			var emails = JobSeekerIds.Select(jobSeeker => new CandidateEmailView
			{
				PersonId = Convert.ToInt64(jobSeeker),
				Subject = messageSubject,
				Body = messageBody,
				SenderAddress = messageSender,
				BccRequestor = false
			}).ToList();

			var unsubscribeUrl = string.Empty;

			var footerType = EmailFooterTypes.None;

			if (App.Settings.UnsubscribeFromJSMessages)
			{
				unsubscribeUrl = string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.UnsubscribeFromJsEmails(false));
				footerType = EmailFooterTypes.JobSeekerUnsubscribe;
			}

			if (emails.Count > 0)
			{
				var unsubscribedCount = ServiceClientLocator.CandidateClient(App).SendCandidateEmails(emails, true, EmailMeCopyOfMessageCheckBox.Checked, footerType, unsubscribeUrl.IsNotNullOrEmpty() ? unsubscribeUrl : null, App.Settings.UnsubscribeFromJSMessages);

				if (App.Settings.UnsubscribeFromJSMessages && unsubscribedCount > 0)
				{
					if (unsubscribedCount == emails.Count)
					{
						OnCompleted(new SendMessageEventArgs
						{
							Title = CodeLocalise("MessageNotSentConfirmation.Title", "Message not sent"),
							Message = CodeLocalise("MessageNotSentConfirmation.Body", "Your message has not been sent to the selected users, as they have unsubscribed from recieving emails.")
						});
					}
					else
					{
						OnCompleted(new SendMessageEventArgs
						{
							Title = CodeLocalise("MessageSentConfirmation.Title", "Message sent"),
							Message = CodeLocalise("MessageSentConfirmationNotAll.Body", "Your message has been sent to the selected users. Some users may have subsequently unsubscribed and will not receive the email.")
						});
					}
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Sends the homepage alerts.
		/// </summary>
		private void SendHomepageAlerts()
		{
			var expiryDate = DateTime.Now.AddDays(7);

			var homepageAlertText = MessageTextbox.TextTrimmed();

			var jobSeekerAlerts = JobSeekerIds.Select(jobSeeker => new JobSeekerHomepageAlertView
			{
				Message = homepageAlertText,
				AlertForAllCandidates = false,
				CandidateId = jobSeeker,
				ExpiryDate = expiryDate,
				IsSystemAlert = true
			}).ToList();

			if (jobSeekerAlerts.Any())
				ServiceClientLocator.CandidateClient(App).CreateHomepageAlert(jobSeekerAlerts);
		}

		/// <summary>
		/// Resets the compose message form.
		/// </summary>
		private void ResetComposeMessageForm()
		{
			SavedMessagesDropDown.SelectedIndex = 0;
			SavedMessagesDropDown.Items.Reset();
			SavedMessageNameTextBox.ResetText();
			SubjectTextBox.Text = EmailTemplate.Subject;
			MessageTextbox.Text = EmailTemplate.Body;
			EmailMeCopyOfMessageCheckBox.Checked = true;
		}

		/// <summary>
		/// Gets the job seeker ids.
		/// </summary>
		/// <returns></returns>
		private List<long> GetJobSeekerIds()
		{
			if (Session["MessageJobSeekers:JobSeekerIds"].IsNotNull())
			{
				var jobseekerIdList = ((string[])Session["MessageJobSeekers:JobSeekerIds"]).ToList();

				return jobseekerIdList.Select(id => Convert.ToInt64(id)).ToList();
			}

			return null;
		}

		#endregion

		#region Bind Methods

		/// <summary>
		/// Binds the saved messages list.
		/// </summary>
		private void BindSavedMessagesDropDown()
		{
			SavedMessagesDropDown.DataSource = ServiceClientLocator.CoreClient(App).GetSavedMessages(MessageAudiences.JobSeeker);
			SavedMessagesDropDown.DataValueField = "Id";
			SavedMessagesDropDown.DataTextField = "Name";
			SavedMessagesDropDown.DataBind();
			SavedMessagesDropDown.Items.AddLocalisedTopDefault("SavedMessagesDropDownList.TopDefault.NoEdit", "- select saved message -");

			SavedMessagesDropDown.SelectedIndex = 0;
		}

		#endregion
	}
}