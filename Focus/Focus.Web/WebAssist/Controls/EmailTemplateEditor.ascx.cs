﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Common.Extensions;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class EmailTemplateEditor : UserControlBase
	{
		private bool _showSubject = true;
		private bool _showSalutation = true;
		private bool _showSenderOptions = false;
	  
		/// <summary>
		/// Gets or sets a value indicating whether [show subject].
		/// </summary>
		/// <value><c>true</c> if [show subject]; otherwise, <c>false</c>.</value>
		public bool ShowSubject
		{
			get { return _showSubject; }
			set { _showSubject = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [show salutation].
		/// </summary>
		/// <value><c>true</c> if [show salutation]; otherwise, <c>false</c>.</value>
		public bool ShowSalutation
		{
			get { return _showSalutation; }
			set { _showSalutation = value; }
		}

		public bool ShowSenderOptions
		{
			get { return _showSenderOptions; }
			set { _showSenderOptions = value; }
		}

    /// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				LocaliseUI();
				InitialiseSenderEmailSelector();
				SetControlVisibility();
			}
		}

		/// <summary>
		/// Handles the Click event of the AddVariableButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void AddVariableButton_Click(object sender, CommandEventArgs e)
		{
      if (e.CommandArgument.ToString() == "body")
        BodyTextBox.Text = String.Format("{0} {1}", BodyTextBox.Text, EmailBodyVariableDropDown.SelectedValue);
      else if (e.CommandArgument.ToString() == "subject")
        SubjectTextBox.Text = String.Format("{0} {1}", SubjectTextBox.Text, EmailSubjectVariableDropDown.SelectedValue);
		}

		/// <summary>
		/// Binds the specified email template.
		/// </summary>
		/// <param name="emailTemplate">The email template.</param>
    /// <param name="availableEmailTags">The avaiable email tags.</param>
		public void Bind(EmailTemplateDto emailTemplate, Dictionary<string,string> availableEmailTags)
		{
			SubjectTextBox.Text = (emailTemplate.IsNotNull() && emailTemplate.Subject.IsNotNull()) ? emailTemplate.Subject : "";
			SalutationTextBox.Text = (emailTemplate.IsNotNull() && emailTemplate.Subject.IsNotNull()) ? emailTemplate.Salutation : "";
			BodyTextBox.Text = (emailTemplate.IsNotNull() && emailTemplate.Body.IsNotNull()) ? emailTemplate.Body : "";

      EmailVariableRow.Visible = AddSubjectVariableButton.Visible = EmailSubjectVariableDropDown.Visible = (availableEmailTags.IsNotNullOrEmpty());

      if (availableEmailTags.IsNotNullOrEmpty()) BindEmailVariableDropDown(availableEmailTags);

		  if (emailTemplate.IsNotNull() && emailTemplate.SenderEmailType.IsNotNull())
			{
				SenderEmailAddressRadioButtonList.SelectValue(emailTemplate.SenderEmailType.ToString());
				ClientSpecificEmailTextBox.Text = emailTemplate.SenderEmailType == SenderEmailTypes.ClientSpecific ? emailTemplate.ClientSpecificEmailAddress : String.Empty;
			}
			else
			{
				SenderEmailAddressRadioButtonList.SelectValue(SenderEmailTypes.SystemDefualt.ToString());
				ClientSpecificEmailTextBox.Text = String.Empty;
			}

			ShowHideClientSpecificEmailTextBox();
		}

		/// <summary>
		/// Binds the email variable drop down.
		/// </summary>
		/// <param name="avaiableEmailTags">The avaiable email tags.</param>
		private void BindEmailVariableDropDown(Dictionary<string, string> avaiableEmailTags)
    {
      EmailSubjectVariableDropDown.DataSource = avaiableEmailTags;
      EmailSubjectVariableDropDown.DataValueField = "Key";
      EmailSubjectVariableDropDown.DataTextField = "Value";
      EmailSubjectVariableDropDown.DataBind();
      EmailBodyVariableDropDown.DataSource = avaiableEmailTags;
      EmailBodyVariableDropDown.DataValueField = "Key";
      EmailBodyVariableDropDown.DataTextField = "Value";
      EmailBodyVariableDropDown.DataBind();
		}

		/// <summary>
		/// Uns the bind.
		/// </summary>
		/// <param name="emailTemplate">The email template.</param>
		public void UnBind(ref EmailTemplateDto emailTemplate)
		{
			emailTemplate.Subject = SubjectTextBox.TextTrimmed();
			emailTemplate.Salutation = SalutationTextBox.TextTrimmed();
			emailTemplate.Body = BodyTextBox.TextTrimmed();

			var senderEmailType = GetSelectedSenderTypeFromRadioButtons();
			emailTemplate.SenderEmailType = senderEmailType;
			if (senderEmailType == SenderEmailTypes.ClientSpecific)
				emailTemplate.ClientSpecificEmailAddress = ClientSpecificEmailTextBox.TextTrimmed();
			else
			{
				ClientSpecificEmailTextBox.Text = String.Empty;
				emailTemplate.ClientSpecificEmailAddress = String.Empty;
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
      AddBodyVariableButton.Text = AddSubjectVariableButton.Text = CodeLocalise("Global.Add.Text", "Add");

			EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address format is invalid");
			EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
		}

		private void InitialiseSenderEmailSelector()
		{
			var emailSelectorList = new Dictionary<SenderEmailTypes, string>(3)
				{
					{SenderEmailTypes.SystemDefualt, "Allow system default as sender address"},
					{SenderEmailTypes.StaffUser, "Allow staff user as sender address"},
					{SenderEmailTypes.ClientSpecific, "Allow client specific sender address"}
				};

			SenderEmailAddressRadioButtonList.DataTextField = "Value";
			SenderEmailAddressRadioButtonList.DataValueField = "Key";
			SenderEmailAddressRadioButtonList.DataSource = emailSelectorList;
			SenderEmailAddressRadioButtonList.DataBind();
		}

		/// <summary>
		/// Sets the control visibility.
		/// </summary>
		private void SetControlVisibility()
		{
			SubjectRow.Visible = SubjectSpacerRow.Visible = _showSubject;
			SalutationRow.Visible = SalutationSpacerRow.Visible = _showSalutation;
			SenderOptionsRow.Visible = SenderOptionsSpacerRow.Visible = _showSenderOptions;
		}

		/// <summary>
		/// Shows/hides the client specific email text box.
		/// </summary>
		protected void ShowHideClientSpecificEmailTextBox()
		{
			ClientSpecificEmailRow.Visible = ClientSpecificEmailSpacerRow.Visible
				= GetSelectedSenderTypeFromRadioButtons() == SenderEmailTypes.ClientSpecific;
		}

		/// <summary>
		/// Handles the IndexChanged event of the SenderEmailRadioButtonList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SenderEmailRadioButtonList_IndexChanged(object sender, EventArgs e)
		{
			ShowHideClientSpecificEmailTextBox();
		}

		/// <summary>
		/// Gets the selected sender type from radio buttons.
		/// </summary>
		/// <returns></returns>
		internal SenderEmailTypes GetSelectedSenderTypeFromRadioButtons()
		{
			return ((SenderEmailTypes[]) Enum.GetValues(typeof (SenderEmailTypes))).Where(senderEmailType => SenderEmailAddressRadioButtonList.SelectedValue == senderEmailType.ToString()).FirstOrDefault();
		}
	}
}