﻿using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Employee;
using Focus.Web.Code;
using Framework.Core;
using Focus.Common.Extensions;

namespace Focus.Web.WebAssist.Controls
{
	public partial class EmployerCharacteristicsCriteriaBuilder : CriteriaBuilderControlBase
	{
		private EmployerCharacteristicsCriteria _criteria
		{
			get { return GetViewStateValue<EmployerCharacteristicsCriteria>("EmployerCharacteristicsCriteriaBuilder:Criteria"); }
			set { SetViewStateValue("EmployerCharacteristicsCriteriaBuilder:Criteria", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			  ApplyTheme();
				ApplyBranding();

				BindWorkOpportuniesTaxCreditDropDown();
				_criteria = new EmployerCharacteristicsCriteria();

				EmployerCharacteristicsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			}
		}

		#region Public methods

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public EmployerCharacteristicsCriteria Unbind()
		{
			return _criteria;
		}

		/// <summary>
		/// Clears the sector filter.
		/// </summary>
		/// <param name="sector">The sector.</param>
		public void ClearSectorFilter(string sector)
		{
			foreach (var sectorFilter in _criteria.Sectors.Where(sectorFilter => sectorFilter == sector))
			{
				_criteria.Sectors.Remove(sectorFilter);
				break;
			}
		}

		/// <summary>
		/// Clears the work opportunies tax credit filter.
		/// </summary>
		/// <param name="workOpportunitiesTaxCreditCategory">The work opportunities tax credit category.</param>
		public void ClearWorkOpportuniesTaxCreditFilter(WorkOpportunitiesTaxCreditCategories workOpportunitiesTaxCreditCategory)
		{
			_criteria.WorkOpportunitiesTaxCreditInterests = _criteria.WorkOpportunitiesTaxCreditInterests ^ workOpportunitiesTaxCreditCategory;
		}

		/// <summary>
		/// Clears the federal employer identification number filter.
		/// </summary>
		public void ClearFederalEmployerIdentificationNumberFilter()
		{
			_criteria.FederalEmployerIdentificationNumber = null;
		}

		/// <summary>
		/// Clears the name filter.
		/// </summary>
		public void ClearNameFilter()
		{
			_criteria.EmployerName = null;
		}

		/// <summary>
		/// Clears the job title filter.
		/// </summary>
		public void ClearJobTitleFilter()
		{
			_criteria.JobTitle = null;
		}

		/// <summary>
		/// Clears the green job filter.
		/// </summary>
		public void ClearGreenJobFilter()
		{
			_criteria.IsGreenJob = null;
		}

		/// <summary>
		/// Clears the IT employer filter.
		/// </summary>
		public void ClearITEmployerFilter()
		{
			_criteria.IsITEmployer = null;
		}

		/// <summary>
		/// Clears the healthcare employer filter.
		/// </summary>
		public void ClearHealthcareEmployerFilter()
		{
			_criteria.IsHealthcareEmployer = null;
		}

		/// <summary>
		/// Clears the biotech employer filter.
		/// </summary>
		public void ClearBiotechEmployerFilter()
		{
			_criteria.IsBiotechEmployer = null;
		}

		/// <summary>
		/// Clears the account creation date filter.
		/// </summary>
		public void ClearAccountCreationDateFilter()
		{
			_criteria.AccountCreationDate = null;
			AccountCreationDateCriteriaBuilder.ClearCriteria();
		}

		/// <summary>
		/// Clears the screenig assistance filter.
		/// </summary>
		public void ClearScreenigAssistanceFilter()
		{
			_criteria.JobScreeningAssistanceRequested = null;
			// AddScreeningAssistanceFilterButton.Visible = true;
		}

		#endregion

		#region Add button clicks

		/// <summary>
		/// Handles the Clicked event of the AddSectorFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddSectorFilterButton_Clicked(object sender, EventArgs e)
		{
			if (SectorTextBox.TextTrimmed().IsNullOrEmpty()) return;

			var sector = SectorTextBox.TextTrimmed();

			if (_criteria.Sectors.IsNull()) _criteria.Sectors = new List<string>();

			_criteria.Sectors.Add(sector);
			SectorTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("Sector.Text", "#BUSINESS# works in {0} sector", sector),
																										OverwriteExistingValue = false,
																										Type = CriteriaElementType.EmployerCharacteristicsSector,
																										Parameters = new List<string> { sector }
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddWorkOpportuniesTaxCreditFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddWorkOpportuniesTaxCreditFilterButton_Clicked(object sender, EventArgs e)
		{
			var workOpportuniesTaxCredit = WorkOpportuniesTaxCreditDropDown.SelectedValueToEnum<WorkOpportunitiesTaxCreditCategories>();

			if (workOpportuniesTaxCredit.IsNull() || (workOpportuniesTaxCredit == WorkOpportunitiesTaxCreditCategories.None))
				return;

			// Check if the criteria has already been added
			if ((_criteria.WorkOpportunitiesTaxCreditInterests & workOpportuniesTaxCredit) == workOpportuniesTaxCredit)
			{
				WorkOpportuniesTaxCreditDropDown.SelectedIndex = 0;
				return;
			}

			_criteria.WorkOpportunitiesTaxCreditInterests = _criteria.WorkOpportunitiesTaxCreditInterests | workOpportuniesTaxCredit;

			WorkOpportuniesTaxCreditDropDown.SelectedIndex = 0;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("WorkOpportunitiesTaxCreditInterests.Text", "#BUSINESS# is interested in {0}", CodeLocalise(workOpportuniesTaxCredit)),
																										OverwriteExistingValue = false,
																										Type = CriteriaElementType.EmployerCharacteristicsWorkOpportuniesTaxCredit,
																										Parameters = new List<string> { workOpportuniesTaxCredit.ToString() }
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddFederalEmployerIdentificationNumberFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddFederalEmployerIdentificationNumberFilterButton_Clicked(object sender, EventArgs e)
		{
			if (FederalEmployerIdentificationNumberTextBox.TextTrimmed().IsNullOrEmpty()) return;

			_criteria.FederalEmployerIdentificationNumber = FederalEmployerIdentificationNumberTextBox.TextTrimmed();
			
			FederalEmployerIdentificationNumberTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("FederalEmployerIdentificationNumber.Text", "#BUSINESS# FEIN is {0}", _criteria.FederalEmployerIdentificationNumber),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsFederalEmployerIdentificationNumber
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddNameFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddNameFilterButton_Clicked(object sender, EventArgs e)
		{
			if (NameTextBox.TextTrimmed().IsNullOrEmpty()) return;

			_criteria.EmployerName = NameTextBox.TextTrimmed();

			NameTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("Name.Text", "#BUSINESS# name is {0}", _criteria.EmployerName),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsName
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddJobTitleFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddJobTitleFilterButton_Clicked(object sender, EventArgs e)
		{
			if (JobTitleTextBox.TextTrimmed().IsNullOrEmpty()) return;

			_criteria.JobTitle = JobTitleTextBox.TextTrimmed();

			JobTitleTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("JobTitle.Text", "#BUSINESS# has {0} job title", _criteria.JobTitle),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsJobTitle
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddGreenJobFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddGreenJobFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.IsGreenJob = true;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("GreenJob.Text", "#BUSINESS# is flagged as a green job #BUSINESS#:LOWER"),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsGreenJob
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddITEmployerFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddITEmployerFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.IsITEmployer = true;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("ITEmployer.Text", "#BUSINESS# is flagged as an IT #BUSINESS#:LOWER"),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsITEmployer
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddHealthcareEmployerFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddHealthcareEmployerFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.IsHealthcareEmployer = true;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("HealthcareEmployer.Text", "#BUSINESS# is flagged as a healthcare #BUSINESS#:LOWER"),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsHealthcareEmployer
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddBiotechEmployerFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddBiotechEmployerFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.IsBiotechEmployer = true;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("BiotechEmployer.Text", "#BUSINESS# is flagged as a biotech #BUSINESS#:LOWER"),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsBiotechEmployer
																									}));
		}

		/// <summary>
		/// Handles the CriteriaAdded event of the AccountCreationDateCriteriaBuilder control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.CriteriaBuilderControlBase.CriteriaAddedEventArgs"/> instance containing the event data.</param>
		protected void AccountCreationDateCriteriaBuilder_CriteriaAdded(object sender, CriteriaBuilderControlBase.CriteriaAddedEventArgs e)
		{
			_criteria.AccountCreationDate = AccountCreationDateCriteriaBuilder.Unbind();

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("AccountCreationDate.Text", "#BUSINESS#'s account created {0}", e.Criteria.DisplayText),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsAccountCreationDate
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddScreeningAssistanceFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddScreeningAssistanceFilterButton_Clicked(object sender, EventArgs e)
		{
			_criteria.JobScreeningAssistanceRequested = true;

			// AddScreeningAssistanceFilterButton.Visible = false;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("ScreeningAssistance.Text", "#BUSINESS# has requested job screening assistance"),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.EmployerCharacteristicsScreeningAssistance
																									}));
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Binds the work opportunies tax credit drop down.
		/// </summary>
		private void BindWorkOpportuniesTaxCreditDropDown()
		{
			WorkOpportuniesTaxCreditDropDown.Items.Clear();

			WorkOpportuniesTaxCreditDropDown.Items.AddLocalisedTopDefault("WorkOpportuniesTaxCreditDropDown.TopDefault", "- select a WOTC type -");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients, "TANF / K TAP recipients");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents, "EZ and RRC");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.SNAPRecipients, "SNAP recipients");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.Veterans, "Veterans");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation, "Vocational rehabilitation");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients, "SSI recipients");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.ExFelons, "Ex-felons");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.SummerYouth, "Summer youth");
			WorkOpportuniesTaxCreditDropDown.Items.AddEnum(WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient, "Long term family assistance recipients");
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			AddSectorFilterButton.Text = AddWorkOpportuniesTaxCreditFilterButton.Text = AddFederalEmployerIdentificationNumberFilterButton.Text = AddNameFilterButton.Text = AddJobTitleFilterButton.Text = 
				AddGreenJobFilterButton.Text = AddITEmployerFilterButton.Text = AddHealthcareEmployerFilterButton.Text = AddBiotechEmployerFilterButton.Text = AddScreeningAssistanceFilterButton.Text = 
				CodeLocalise("Global.Add.Text", "Add");
		}

    /// <summary>
    /// Hides/shows controls depending on the theme
    /// </summary>
    private void ApplyTheme()
    {
      TargetedSectorPlaceHolder.Visible =
      WorkOpportuniesTaxCreditPlaceHolder.Visible =
      FederalEmployerIdentificationNumberPlaceHolder.Visible =
      FlagsPlaceHolder.Visible =
      ScreeningAssistancePlaceHolder.Visible =
        (App.Settings.Theme != FocusThemes.Education);
    }

		private void ApplyBranding()
		{
			EmployerCharacteristicsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			EmployerCharacteristicsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
		}

		#endregion

	}
}