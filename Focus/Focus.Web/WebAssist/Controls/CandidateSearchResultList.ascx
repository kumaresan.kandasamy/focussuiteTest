﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidateSearchResultList.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.CandidateSearchResultList" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>

<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/EmailJobSeekerModal.ascx" TagName="EmailCandidateModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/JobSeekerListModal.ascx" TagName="JobSeekerListModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagPrefix="uc" TagName="ConfirmationModal" %>
<%@ Register src="~/WebAssist/Controls/NotesAndRemindersModal.ascx" tagname="NotesAndRemindersModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/IssueResolutionModal.ascx" tagname="IssueResolutionModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/RequestFollowUpModal.ascx" tagName="RequestFollowUp" tagPrefix="uc" %>

<asp:UpdatePanel runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<table width="100%" role="presentation">
			<tr>
				<td nowrap="nowrap"><asp:Literal ID="ActionLabel" runat="server" Visible="True" ClientIDMode="Static" /></td>
				<td><asp:DropDownList runat="server" ID="ActionDropDown" Width="250" Visible="True" ClientIDMode="Static" /></td>
				<td><asp:Button ID="ActionGoButton" runat="server" class="button3" Visible="True" ClientIDMode="Static" OnClick="ActionButton_Clicked" ValidationGroup="Action" /></td>
				<td>
					<asp:DropDownList runat="server" ID="SubActionDropDown" Width="225" Visible="false" ClientIDMode="Static" />
				</td>
				<td><asp:Button ID="SubActionButton" runat="server" class="button3" Visible="false" ClientIDMode="Static" ValidationGroup="SubAction" OnClick="SubActionButton_Clicked" /></td>
				<td width="100%" nowrap="nowrap" align="right">
					<uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="CandidateListView" PageSize="10" DisplayRecordCount="True" />
				</td>
			</tr>
			<tr>
				<td colspan="3"><asp:RequiredFieldValidator ID="ActionDropDownRequired" runat="server" ValidationGroup="Action" ControlToValidate="ActionDropDown" CssClass="error" Display="Dynamic" />
					<asp:CustomValidator ID="CandidateActionDropDownValidator" runat="server" CssClass="error" ControlToValidate="ActionDropDown" SetFocusOnError="True" /></td>
				<td colspan="2"><asp:RequiredFieldValidator ID="SubActionDropDownRequired" runat="server" ValidationGroup="SubAction" ControlToValidate="SubActionDropDown" CssClass="error" /></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<br/>
		<asp:ListView runat="server" ID="CandidateListView" ItemPlaceholderID="CandidateSearchResultPlaceHolder" DataSourceID="CandidateSearchResultDataSource" OnLayoutCreated="CandidateListView_LayoutCreated" 
							OnSorting="CandidateListView_Sorting" OnItemDataBound="CandidateListView_ItemDataBound" DataKeyNames="Id, FirstName, LastName, MiddleInitial, Blocked">
		<LayoutTemplate>
			<table style="width: 100%;" class="table" id="JobSeekerResultTable" role="presentation">
				<tr>
					<th width="10">
						<table role="presentation">
							<tr>
								<td rowspan="2">
									<asp:CheckBox ID="SelectorAllCheckBoxes" ClientIDMode="Static" runat="server" CausesValidation="False" OnClick="SelectDeselectAllCheckboxes()" AutoPostBack="False" />
								</td>
								<td>
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2">
									<asp:Literal runat="server" ID="NameHeaderLiteral"></asp:Literal>
								</td>
								<td style="white-space:nowrap">
									<asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NameSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" CommandArgument="jobseekername asc" />
									<asp:ImageButton ImageUrl=<%# UrlBuilder.ActivityOnSortDescImage() %> ID="NameSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" CommandArgument="jobseekername desc" />
								</td>
							</tr>
						</table>
					</th>
          <asp:PlaceHolder ID="StudentIdHeaderPlaceHolder" runat="server">
            <th style="white-space:nowrap">
              <asp:Literal runat="server" ID="StudentIdHeaderLiteral"></asp:Literal>
            </th>
          </asp:PlaceHolder>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2" style="white-space:nowrap">
									<asp:Literal runat="server" ID="StudyProgramHeaderLiteral"></asp:Literal>
								</td>
								<td style="white-space:nowrap">
									<asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="StudyProgramNameSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" CommandArgument="studyprogram asc" />
									<asp:ImageButton ImageUrl=<%# UrlBuilder.ActivityOnSortDescImage() %> ID="StudyProgramNameSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" CommandArgument="studyprogram desc" />
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role="presentation">
							<tr>
								<td rowspan="2" style="white-space:nowrap">
										<asp:Literal runat="server" ID="LastActivityDateHeaderLiteral"></asp:Literal>
								</td>
								<td style="white-space:nowrap">
									<asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LastActivityDateSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" CommandArgument="lastactivity asc" />
									<asp:ImageButton ImageUrl=<%# UrlBuilder.ActivityOnSortDescImage() %> ID="LastActivityDateSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" CommandArgument="lastactivity desc" />
								</td>
							</tr>
						</table>
					</th>
					<th>
							<asp:Literal runat="server" ID="IssuesHeaderLiteral"></asp:Literal>
					</th>
				</tr>
				<asp:PlaceHolder ID="CandidateSearchResultPlaceHolder" runat="server" />
			</table>
		</LayoutTemplate>
		<ItemTemplate>
			<tr>
				<td>
					<asp:CheckBox ID="SelectorCheckBox" GroupName="SelectorCheckBoxes" runat="server" ClientIDMode="Static" CausesValidation="False" AutoPostBack="False" />
				</td>
				<td>
					<asp:HyperLink runat="server" ID="CandidateNameLink"></asp:HyperLink>
          <asp:image ID="BlockedImage" runat="server" Visible="false" />
				</td>
        <asp:PlaceHolder ID="StudentIdPlaceHolder" runat="server">
          <td>
            <asp:Literal runat="server" ID="StudentIdLiteral"></asp:Literal>
          </td>
        </asp:PlaceHolder>
				<td>
					<asp:Literal runat="server" ID="StudyProgramLiteral"></asp:Literal>
				</td>
				<td>
					<%#((StudentAlumniIssueViewDto)Container.DataItem).LastLoggedInOn%>
				</td>
				<td>
					<asp:Literal runat="server" ID="IssuesLiteral"></asp:Literal>
					<span id="AdditionalIssuesLink_<%#((StudentAlumniIssueViewDto)Container.DataItem).Id%>"><asp:HyperLink runat="server" ID="ShowAdditionalIssuesLink" Visible="False" NavigateUrl="javascript:void(0);" ClientIDMode="Predictable"></asp:HyperLink></span>
					<span style="display: none" id="AdditionalIssues_<%#((StudentAlumniIssueViewDto)Container.DataItem).Id%>"><asp:Literal runat="server" ID="AdditionalIssuesLiteral"></asp:Literal>
					<a href="javascript:void(0);" onclick="HideAdditionalIssues(<%#((StudentAlumniIssueViewDto)Container.DataItem).Id%>);"><%=HtmlLocalise("Hide.text", "Hide") %></a></span>
				</td>
			</tr>
		</ItemTemplate>
		<EmptyDataTemplate><br/><%=HtmlLocalise("NoJobSeekers", "No #CANDIDATETYPE# found") %></EmptyDataTemplate>
		</asp:ListView>
		<asp:ObjectDataSource ID="CandidateSearchResultDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.CandidateSearchResultList"	EnablePaging="true" SelectMethod="GetJobSeekers" SelectCountMethod="GetJobSeekersCount"
				SortParameterName="orderBy" OnSelecting="SearchResultDataSource_Selecting">
		</asp:ObjectDataSource>  
    	
		<%-- Modals --%>
		<uc:EmailCandidateModal ID="EmailCandidate" runat="server" />
		<uc:JobSeekerListModal ID="JobSeekerList" runat="server" />
		<uc:ConfirmationModal ID="Confirmation" runat="server" OnOkCommand="Confirmation_OnOkCommand" OnCloseCommand="Confirmation_OnCloseCommand" />
		<uc:NotesAndRemindersModal ID="NotesAndReminders" runat="server" />
		<uc:IssueResolutionModal ID="IssueResolutionModal" runat="server" OnIssuesResolved="Issues_Updated" />
		<uc:RequestFollowUp ID="RequestFollowUpModal" runat="server" OnFollowUpRequested="Issues_Updated" />
    <asp:HiddenField runat="server" ID="AccessCandidateUrl"/>
    <script type="text/javascript">
      function <%=ClientID %>_AccessCandidate() {
        var url = $("#<%=AccessCandidateUrl.ClientID %>").val();
        if (url.length > 0) {
          showPopup(url, '<%=BlockedPopupMessage %>');
          $("#<%=AccessCandidateUrl.ClientID %>").val("");
        }
      }

      $(document).ready(function() {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(<%=ClientID %>_AccessCandidate)
        ;
      });
    </script>
	</ContentTemplate>
</asp:UpdatePanel>