﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerCharacteristicsCriteriaBuilder.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EmployerCharacteristicsCriteriaBuilder" %>

<%@ Register TagPrefix="uc" TagName="DateCriteriaBuilder" Src="~/WebAssist/Controls/DateCriteriaBuilder.ascx" %>

<asp:Panel ID="EmployerCharacteristicsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<asp:Image ID="EmployerCharacteristicsHeaderImage" runat="server" AlternateText="Employer Characteristics Header Image" />
	&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("EmployerCharacteristics.Label", "#BUSINESS# characteristics")%></a></span>
</asp:Panel>
<asp:Panel ID="EmployerCharacteristicsPanel" runat="server" CssClass="singleAccordionContent">
	<table>
	  <asp:PlaceHolder runat="server" ID="TargetedSectorPlaceHolder">
		  <tr>
			  <td colspan="2"><h5><%= HtmlLocalise("Sector.Title", "Targeted sector")%></h5></td>
		  </tr>
		  <tr>
			  <td width="200px">
				  <%= HtmlInFieldLabel("SectorTextBox", "Sector.InlineLabel", "sector type", 150)%>
				  <asp:TextBox runat="server" ID="SectorTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25" />
			  </td>
			  <td><asp:Button ID="AddSectorFilterButton" runat="server" CssClass="button3" OnClick="AddSectorFilterButton_Clicked" ClientIDMode="Static" /></td>
		  </tr>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="WorkOpportuniesTaxCreditPlaceHolder">
		  <tr>
			  <td colspan="2"><h5><%= HtmlLocalise("WorkOpportuniesTaxCredit.Title", "WOTC interest")%></h5></td>
		  </tr>
		  <tr>
			  <td>
				  <asp:DropDownList runat="server" ID="WorkOpportuniesTaxCreditDropDown" ClientIDMode="Static" />
			  </td>
			  <td><asp:Button ID="AddWorkOpportuniesTaxCreditFilterButton" runat="server" CssClass="button3" OnClick="AddWorkOpportuniesTaxCreditFilterButton_Clicked" ClientIDMode="Static" /></td>
		  </tr>
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="FederalEmployerIdentificationNumberPlaceHolder">
		  <tr>
			  <td colspan="2"><h5><%= HtmlLocalise("FederalEmployerIdentificationNumber.Title", "FEIN")%></h5></td>
		  </tr>
		  <tr>
			  <td>
				  <%= HtmlInFieldLabel("FederalEmployerIdentificationNumberTextBox", "FederalEmployerIdentificationNumber.InlineLabel", "fein", 150)%>
				  <asp:TextBox runat="server" ID="FederalEmployerIdentificationNumberTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25" />
			  </td>
			  <td><asp:Button ID="AddFederalEmployerIdentificationNumberFilterButton" runat="server" CssClass="button3" OnClick="AddFederalEmployerIdentificationNumberFilterButton_Clicked" ClientIDMode="Static" /></td>
		  </tr>
    </asp:PlaceHolder>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("Name.Title", "#BUSINESS# name")%></h5></td>
		</tr>
		<tr>
			<td>
				<%= HtmlInFieldLabel("NameTextBox", "NameTextBox.InlineLabel", "#BUSINESS#:LOWER name", 150)%>
				<asp:TextBox runat="server" ID="NameTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25" />
			</td>
			<td><asp:Button ID="AddNameFilterButton" runat="server" CssClass="button3" OnClick="AddNameFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("JobTitle.Title", "Employee job title")%></h5></td>
		</tr>
		<tr>
			<td>
				<%= HtmlInFieldLabel("JobTitleTextBox", "JobTitleTextBox.InlineLabel", "Employee job title", 150)%>
				<asp:TextBox runat="server" ID="JobTitleTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25" />
			</td>
			<td><asp:Button ID="AddJobTitleFilterButton" runat="server" CssClass="button3" OnClick="AddJobTitleFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
    <asp:PlaceHolder runat="server" ID="FlagsPlaceHolder">
		  <tr>
			  <td colspan="2"><h5><%= HtmlLocalise("Flags.Title", "Flags")%></h5></td>
		  </tr>
		  <tr>
			  <td><%= HtmlLabel("GreenJob.Label", "Green job")%></td>
			  <td><asp:Button ID="AddGreenJobFilterButton" runat="server" CssClass="button3" OnClick="AddGreenJobFilterButton_Clicked" ClientIDMode="Static" /></td>
		  </tr>
		  <tr>
			  <td><%= HtmlLabel("ITEmployer.Label", "IT #BUSINESS#:LOWER")%></td>
			  <td><asp:Button ID="AddITEmployerFilterButton" runat="server" CssClass="button3" OnClick="AddITEmployerFilterButton_Clicked" ClientIDMode="Static" /></td>
		  </tr>
		  <tr>
			  <td><%= HtmlLabel("HealthcareEmployer.Label", "Healthcare #BUSINESS#:LOWER")%></td>
			  <td><asp:Button ID="AddHealthcareEmployerFilterButton" runat="server" CssClass="button3" OnClick="AddHealthcareEmployerFilterButton_Clicked" ClientIDMode="Static" /></td>
		  </tr>
		  <tr>
			  <td><%= HtmlLabel("BiotechEmployer.Label", "Biotech #BUSINESS#:LOWER")%></td>
			  <td><asp:Button ID="AddBiotechEmployerFilterButton" runat="server" CssClass="button3" OnClick="AddBiotechEmployerFilterButton_Clicked" ClientIDMode="Static" /></td>
		  </tr>
    </asp:PlaceHolder>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("AccountCreationDate.Title", "Account creation date")%></h5></td>
		</tr>
		<tr>
			<td><%= HtmlLabel("AccountCreationDate.Label", "#BUSINESS#'s account created during selected date range")%></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="2"><uc:DateCriteriaBuilder ID="AccountCreationDateCriteriaBuilder" runat="server" OnCriteriaAdded="AccountCreationDateCriteriaBuilder_CriteriaAdded" /></td>
		</tr>
    <asp:PlaceHolder runat="server" ID="ScreeningAssistancePlaceHolder">
		  <tr>
			  <td colspan="2"><h5><%= HtmlLocalise("ScreeningAssistance.Title", "Assistance request")%></h5></td>
		  </tr>
		  <tr>
			  <td><%= HtmlLabel("ScreeningAssistance.Label", "#BUSINESS# has requested job screening assistance")%></td>
			  <td><asp:Button ID="AddScreeningAssistanceFilterButton" runat="server" CssClass="button3" OnClick="AddScreeningAssistanceFilterButton_Clicked" ClientIDMode="Static" /></td>
		  </tr>
    </asp:PlaceHolder>
	</table>
</asp:Panel>
<act:CollapsiblePanelExtender ID="EmployerCharacteristicsPanelExtender" runat="server" 
														TargetControlID="EmployerCharacteristicsPanel" 
														ExpandControlID="EmployerCharacteristicsHeaderPanel" 
														CollapseControlID="EmployerCharacteristicsHeaderPanel" 
														Collapsed="true" 
														ImageControlID="EmployerCharacteristicsHeaderImage" 
														SuppressPostBack="true"
														BehaviorID="EmployerCharacteristicsPanelBehavior" />