﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobDevelopmentOfficeFilter.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.JobDevelopmentOfficeFilter" %>

<focus:LocalisedLabel runat="server" ID="OfficeGroupDropDownLabel" AssociatedControlID="OfficeGroupDropDown" LocalisationKey="OfficeGroup" DefaultText="Office group" CssClass="sr-only"/>
<asp:DropDownList ID="OfficeGroupDropDown" runat="server" CausesValidation="false" ClientIDMode="AutoID" />
<%= HtmlLabel(OfficeDropDown, "AssignedOffices.Label", "select office")%>
<asp:DropDownList ID="OfficeDropDown" runat="server" ClientIDMode="AutoID" />

<script type="text/javascript">
  Sys.Application.add_load(<%= this.ClientID %>_PageLoad);

  function <%= this.ClientID %>_PageLoad() {

    <%= this.ClientID %>_EnableDisableOffices();

    $("#<%= OfficeGroupDropDown.ClientID%>").on('change', function() {
      <%= this.ClientID %>_EnableDisableOffices();
    });
  }


  function <%= this.ClientID %>_EnableDisableOffices() {
    var officeDropDown = $("#<%= OfficeDropDown.ClientID%>");
		
		if(officeDropDown != null)
		{
			officeDropDown.val('');
			if ($("#<%= OfficeGroupDropDown.ClientID%> option:selected").val() != "MyOffices") {
				officeDropDown.attr('disabled', true);
			}
			else {
				officeDropDown.removeAttr('disabled');
			}
		}
  }


</script>
