﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class JobReferralSummaryList : UserControlBase
	{
		private int _referralsCount;
    private List<ProgramAreaDegreeEducationLevelViewDto> _programAreaDegrees;

		private ApplicationStatusLogViewCriteria ReferralCriteria
		{
			get { return GetViewStateValue<ApplicationStatusLogViewCriteria>("JobReferralSummaryList:Criteria"); }
			set { SetViewStateValue("JobReferralSummaryList:Criteria", value); }
		}

		public long JobId { get; set; }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				if (JobId == 0)
					throw new Exception(FormatError(ErrorTypes.InvalidJob, "Invalid job id"));

				ReferralCriteria = new ApplicationStatusLogViewCriteria
				                   	{
				                   		JobId = JobId,
															OrderBy = Constants.SortOrders.DateReceivedDesc, 
															DaysSinceStatusChange = 30,
                              IgnoreSameStatuses = true
				                   	};

				BindDaysFilterDropDown();
				BindStatusFilterDropDown();
				BindJobReferralSummaryList();
				FormatListSortingImages();
			}
		}

		/// <summary>
		/// Handles the Selecting event of the JobReferralListDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void JobReferralListDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["criteria"] = ReferralCriteria;
		}

		/// <summary>
		/// Handles the LayoutCreated event of the JobReferralList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobReferralList_LayoutCreated(object sender, EventArgs e)
		{
			// Set labels
			((Literal)JobReferralList.FindControl("DateHeader")).Text = CodeLocalise("DateHeader.Text", "Date");
			((Literal)JobReferralList.FindControl("RatingHeader")).Text = CodeLocalise("RatingHeader.Text", "Rating");
			((Literal)JobReferralList.FindControl("NameHeader")).Text = CodeLocalise("NameHeader.Text", "Name");
			((Literal)JobReferralList.FindControl("ReferralOutcomeHeader")).Text = CodeLocalise("ReferralOutcomeHeader.Text", "Referral Outcome");

      if (App.Settings.Theme == FocusThemes.Education)
      {
        var programOfStudyCell = (PlaceHolder)JobReferralList.FindControl("ProgramOfStudyHeaderCell");
        programOfStudyCell.Visible = true;
        ((Literal)JobReferralList.FindControl("ProgramOfStudyHeader")).Text = CodeLocalise("ProgramOfStudyHeader.Text", "Program of Study");

        var completionDateCell = (PlaceHolder)JobReferralList.FindControl("CompletionDateHeaderCell");
        completionDateCell.Visible = true;
        ((Literal)JobReferralList.FindControl("CompletionDateHeader")).Text = CodeLocalise("CompletionDateHeader.Text", "Graduation date");
      }
      else
      {
        var recentEmploymentCell = (PlaceHolder)JobReferralList.FindControl("RecentEmploymentHeaderCell");
        recentEmploymentCell.Visible = true;
        ((Literal)JobReferralList.FindControl("RecentEmploymentHeader")).Text = CodeLocalise("RecentEmploymentHeader.Text", "Recent Employment");

        var experienceCell = (PlaceHolder)JobReferralList.FindControl("ExperienceHeaderCell");
        experienceCell.Visible = true;
        ((Literal)JobReferralList.FindControl("ExperienceHeader")).Text = CodeLocalise("ExperienceHeader.Text", "Years Of Experience");
      }
		}

		/// <summary>
		/// Handles the Sorting event of the JobReferralList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void JobReferralList_Sorting(object sender, ListViewSortEventArgs e)
		{
			ReferralCriteria.OrderBy = e.SortExpression;
			FormatListSortingImages();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the JobReferralList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void JobReferralList_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var applicationStatusLog = (ApplicationStatusLogViewDto)e.Item.DataItem;

			#region Score

			var scoreImage = (Image)e.Item.FindControl("ReferralScoreImage");
			BindScore(scoreImage, applicationStatusLog.CandidateApplicationScore);
			
			#endregion

			#region Recent Employment History

      if (App.Settings.Theme == FocusThemes.Workforce)
      {
        var recentEmploymentCell = (PlaceHolder)e.Item.FindControl("ReferralLastEmploymentBodyCell");
        recentEmploymentCell.Visible = true;

			  var unknownText = CodeLocalise("Global.Unknown.Text", "Unknown");

			  var employmentHistoryLiteral = (Literal)e.Item.FindControl("ReferralLastEmploymentLiteral");
			  if (applicationStatusLog.LatestJobTitle.IsNotNull() || applicationStatusLog.LatestJobEmployer.IsNotNull() || applicationStatusLog.LatestJobStartYear.IsNotNull() || applicationStatusLog.LatestJobEndYear.IsNotNull())
			  {
				  employmentHistoryLiteral.Text = String.Format("{0} - {1} ({2}-{3})",
				                                                applicationStatusLog.LatestJobTitle.IsNotNullOrEmpty()
				                                              	  ? applicationStatusLog.LatestJobTitle
				                                              	  : unknownText,
				                                                applicationStatusLog.LatestJobEmployer.IsNotNullOrEmpty()
				                                              	  ? applicationStatusLog.LatestJobEmployer
				                                              	  : unknownText,
				                                                applicationStatusLog.LatestJobStartYear.IsNotNullOrEmpty()
				                                              	  ? applicationStatusLog.LatestJobStartYear
				                                              	  : unknownText,
				                                                applicationStatusLog.LatestJobEndYear.IsNotNullOrEmpty()
				                                              	  ? applicationStatusLog.LatestJobEndYear
				                                              	  : unknownText);
			  }

        var experienceCell = (PlaceHolder)e.Item.FindControl("ExperienceBodyCell");
        experienceCell.Visible = true;
      }
			
			#endregion

      #region Program of Study

      if (App.Settings.Theme == FocusThemes.Education)
      {
        var programOfStudyCell = (PlaceHolder)e.Item.FindControl("ProgramOfStudyBodyCell");
        programOfStudyCell.Visible = true;

        var areaId = applicationStatusLog.ProgramAreaId;
        var degreeId = applicationStatusLog.DegreeId;

        if (areaId.HasValue || degreeId.HasValue)
        {
          var programOfStudyLiteral = (Literal)e.Item.FindControl("ProgramOfStudyLiteral");
          programOfStudyLiteral.Text = GetUserProgramOfStudy(areaId, degreeId);
        }

        var completionDateCell = (PlaceHolder)e.Item.FindControl("CompletionDateBodyCell");
        completionDateCell.Visible = true;

        var completionDateLiteral = (Literal)e.Item.FindControl("CompletionDateLiteral");
        completionDateLiteral.Text = applicationStatusLog.EducationCompletionDate.HasValue 
                                       ? applicationStatusLog.EducationCompletionDate.Value.ToString("MM/yyyy")
                                       : HtmlLabel("Global.Unknown", "Unknown");
      }

      #endregion

      #region Referral Outcome

      var referralOutcomeLiteral = (Literal)e.Item.FindControl("ReferralOutcomeLiteral");
      
      ApplicationStatusTypes candidateApplicationStatus;

      Enum.TryParse(applicationStatusLog.CandidateApplicationStatus.ToString(CultureInfo.InvariantCulture), true, out candidateApplicationStatus);

      if (applicationStatusLog.VeteranPriorityEndDate.IsNotNull() && applicationStatusLog.VeteranPriorityEndDate > DateTime.Now && !(applicationStatusLog.CandidateIsVeteran ?? false))
          referralOutcomeLiteral.Text = CodeLocalise("ReferralOutcomeLiteral.Text", "Queued for release");
      else
          referralOutcomeLiteral.Text = CodeLocalise(candidateApplicationStatus, true);

			#endregion

			//To meet accessibility FVN-1983
			var control = (ImageButton)JobReferralList.FindControl("DateSortAscButton");
			control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			control = (ImageButton)JobReferralList.FindControl("DateSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("RatingSortAscButton");
			control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			control = (ImageButton)JobReferralList.FindControl("RatingSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("NameSortAscButton");
			control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			control = (ImageButton)JobReferralList.FindControl("NameSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("ProgramOfStudySortAscButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("ProgramOfStudySortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("ExperienceSortAscButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("ExperienceSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("CompletionDateSortAscButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("CompletionDateSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("ReferralOutcomeSortAscButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			control = (ImageButton)JobReferralList.FindControl("ReferralOutcomeSortDescButton");
			control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the DaysFilterDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DaysFilterDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			ReferralCriteria.DaysSinceStatusChange = (DaysFilterDropDown.SelectedValue == "all") ? (int?)null : Convert.ToInt32(DaysFilterDropDown.SelectedValue);
			JobReferralListPager.ReturnToFirstPage();
			BindJobReferralSummaryList();
			FormatListSortingImages();
    }

		/// <summary>
		/// Handles the SelectedIndexChanged event of the StatusFilterDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void StatusFilterDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			ReferralCriteria.ApplicationStatus = (StatusFilterDropDown.SelectedValue.ToLower() == "all") ? (ApplicationStatusTypes?)null : StatusFilterDropDown.SelectedValueToEnum(ApplicationStatusTypes.NotApplicable);
			JobReferralListPager.ReturnToFirstPage();
			BindJobReferralSummaryList();
			FormatListSortingImages();
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the referral summaries.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<ApplicationStatusLogViewDto> GetReferralSummaries(ApplicationStatusLogViewCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_referralsCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;

			var referrals = ServiceClientLocator.CandidateClient(App).GetApplicationStatusLogViews(criteria);
			_referralsCount = referrals.TotalCount;

			return referrals;
		}

		/// <summary>
		/// Gets the referral summaries count.
		/// </summary>
		/// <returns></returns>
		public int GetReferralSummariesCount()
		{
			return _referralsCount;
		}

		/// <summary>
		/// Gets the referral summaries count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetReferralSummariesCount(ApplicationStatusLogViewCriteria criteria)
		{
			return _referralsCount;
		}

		#endregion

		/// <summary>
		/// Binds the job referral summary list.
		/// </summary>
		private void BindJobReferralSummaryList()
		{
			JobReferralList.DataBind();

			// Set visibility of other controls
			JobReferralListPager.Visible = (JobReferralListPager.TotalRowCount > 0);
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private static void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = OldApp_RefactorIfFound.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString(CultureInfo.InvariantCulture);

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

		/// <summary>
		/// Binds the days filter drop down.
		/// </summary>
		private void BindDaysFilterDropDown()
		{
			DaysFilterDropDown.Items.Clear();

			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("TheLastDay.Text", "the last day"), "1"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last7Days.Text", "last 7 days"), "7"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last14Days.Text", "last 14 days"), "14"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last30Days.Text", "last 30 days"), "30"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last60Days.Text", "last 60 days"), "60"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last90Days.Text", "last 90 days"), "90"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last180Days.Text", "last 180 days"), "180"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("AllDays.Text", "all"), "all"));

			DaysFilterDropDown.SelectedIndex = 3;
		}

		/// <summary>
		/// Binds the status filter drop down.
		/// </summary>
		private void BindStatusFilterDropDown()
		{
			StatusFilterDropDown.Items.Clear();

			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise("AllStatuses.Text", "All referral outcomes"), "All"));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise("ApplicationStatusTypes.DidNotApply", "Failed to apply to job"), ApplicationStatusTypes.DidNotApply.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToReportToJob, "Failed to report to job"), ApplicationStatusTypes.FailedToReportToJob.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise("ApplicationStatusTypes.FailedToShow", "Failed to report to interview"), ApplicationStatusTypes.FailedToShow.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FoundJobFromOtherSource, "Found job from other source"), ApplicationStatusTypes.FoundJobFromOtherSource.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Hired, "Hired"), ApplicationStatusTypes.Hired.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewDenied, "Interview denied"), ApplicationStatusTypes.InterviewDenied.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewScheduled, "Interview scheduled"), ApplicationStatusTypes.InterviewScheduled.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.JobAlreadyFilled, "Job already filled"), ApplicationStatusTypes.JobAlreadyFilled.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NewApplicant, "New applicant"), ApplicationStatusTypes.NewApplicant.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotHired, "Not hired"), ApplicationStatusTypes.NotHired.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotQualified, "Not qualified"), ApplicationStatusTypes.NotQualified.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotYetPlaced, "Not yet placed"), ApplicationStatusTypes.NotYetPlaced.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Recommended, "Recommended"), ApplicationStatusTypes.Recommended.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise("ApplicationStatusTypes.RefusedOffer", "Refused job"), ApplicationStatusTypes.RefusedOffer.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.RefusedReferral, "Refused referral"), ApplicationStatusTypes.RefusedReferral.ToString()));
			StatusFilterDropDown.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.UnderConsideration, "Under consideration"), ApplicationStatusTypes.UnderConsideration.ToString()));
			
			StatusFilterDropDown.SelectedIndex = 0;
		}

		/// <summary>
		/// Formats the list sorting images.
		/// </summary>
		private void FormatListSortingImages()
		{
			if (!JobReferralListPager.Visible) return;

			var dateSortAscButton = (ImageButton) JobReferralList.FindControl("DateSortAscButton");
			var dateSortDescButton = (ImageButton)JobReferralList.FindControl("DateSortDescButton");
			var ratingSortAscButton = (ImageButton)JobReferralList.FindControl("RatingSortAscButton");
			var ratingSortDescButton = (ImageButton)JobReferralList.FindControl("RatingSortDescButton");
			var nameSortAscButton = (ImageButton)JobReferralList.FindControl("NameSortAscButton");
			var nameSortDescButton = (ImageButton)JobReferralList.FindControl("NameSortDescButton");
			var experienceSortAscButton = (ImageButton)JobReferralList.FindControl("ExperienceSortAscButton");
			var experienceSortDescButton = (ImageButton)JobReferralList.FindControl("ExperienceSortDescButton");
      var programOfStudySortAscButton = (ImageButton)JobReferralList.FindControl("ProgramOfStudySortAscButton");
      var programOfStudySortDescButton = (ImageButton)JobReferralList.FindControl("ProgramOfStudySortDescButton");
			var referralOutcomeSortAscButton = (ImageButton)JobReferralList.FindControl("ReferralOutcomeSortAscButton");
			var referralOutcomeSortDescButton = (ImageButton)JobReferralList.FindControl("ReferralOutcomeSortDescButton");
      var completionDateSortAscButton = (ImageButton)JobReferralList.FindControl("CompletionDateSortAscButton");
      var completionDateSortDescButton = (ImageButton)JobReferralList.FindControl("CompletionDateSortDescButton");

			// Reset image URLs
			dateSortAscButton.ImageUrl = 
        ratingSortAscButton.ImageUrl = 
        nameSortAscButton.ImageUrl = 
        experienceSortAscButton.ImageUrl = 
        programOfStudySortAscButton.ImageUrl = 
        referralOutcomeSortAscButton.ImageUrl =
        completionDateSortAscButton.ImageUrl = UrlBuilder.CategorySortAscImage();

			dateSortDescButton.ImageUrl = 
        ratingSortDescButton.ImageUrl = 
        nameSortDescButton.ImageUrl = 
        experienceSortDescButton.ImageUrl = 
        programOfStudySortDescButton.ImageUrl =
        referralOutcomeSortDescButton.ImageUrl =
        completionDateSortDescButton.ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			dateSortAscButton.Enabled = 
        ratingSortAscButton.Enabled = 
        nameSortAscButton.Enabled = 
        experienceSortAscButton.Enabled = 
        dateSortDescButton.Enabled = 
        ratingSortDescButton.Enabled = 
        nameSortDescButton.Enabled = 
				experienceSortDescButton.Enabled = 
        programOfStudySortAscButton.Enabled = 
        programOfStudySortDescButton.Enabled = 
        referralOutcomeSortAscButton.Enabled = 
        referralOutcomeSortDescButton.Enabled =
        completionDateSortAscButton.Enabled =
        completionDateSortDescButton.Enabled = true;

			switch(ReferralCriteria.OrderBy.ToLower())
			{
				case Constants.SortOrders.DateReceivedAsc:
					dateSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
					dateSortAscButton.Enabled = false;
					break;

				case Constants.SortOrders.DateReceivedDesc:
					dateSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
					dateSortDescButton.Enabled = false;
					break;

				case Constants.SortOrders.ScoreAsc:
					ratingSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
					ratingSortAscButton.Enabled = false;
					break;

				case Constants.SortOrders.ScoreDesc:
					ratingSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
					ratingSortDescButton.Enabled = false;
					break;

				case Constants.SortOrders.NameAsc:
					nameSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
					nameSortAscButton.Enabled = false;
					break;

				case Constants.SortOrders.NameDesc:
					nameSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
					nameSortDescButton.Enabled = false;
					break;

				case Constants.SortOrders.YearsExperienceAsc:
					experienceSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
					experienceSortAscButton.Enabled = false;
					break;

				case Constants.SortOrders.YearsExperienceDesc:
					experienceSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
					experienceSortDescButton.Enabled = false;
					break;

        case Constants.SortOrders.StudyProgramAsc:
          programOfStudySortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
          programOfStudySortAscButton.Enabled = false;
          break;

        case Constants.SortOrders.StudyProgramDesc:
          programOfStudySortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
          programOfStudySortDescButton.Enabled = false;
          break;

        case Constants.SortOrders.StatusAsc:
          referralOutcomeSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
          referralOutcomeSortAscButton.Enabled = false;
          break;

        case Constants.SortOrders.StatusDesc:
          referralOutcomeSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
          referralOutcomeSortDescButton.Enabled = false;
          break;

        case Constants.SortOrders.EducationCompletionDateAsc:
          completionDateSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
          completionDateSortDescButton.Enabled = false;
          break;

        case Constants.SortOrders.EducationCompletionDateDesc:
          completionDateSortAscButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
          completionDateSortDescButton.Enabled = false;
          break;
			}
		}

    /// <summary>
    /// Gets the user program of study.
    /// </summary>
    /// <returns></returns>
    private string GetUserProgramOfStudy(long? areaId, long? degreeId)
    {
      var programArea = "No program area";
      var programAreaDegree = "No degree found";

      if (_programAreaDegrees.IsNull())
        _programAreaDegrees = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels();

      if (areaId.IsNotNull())
        programArea = (areaId > 0)
          ? _programAreaDegrees.Where(pad => pad.ProgramAreaId == areaId).Select(pad => pad.ProgramAreaName).FirstOrDefault()
          : HtmlLocalise("Global.Undeclared", "Undeclared");

      if (degreeId.IsNotNull())
      {
        if (degreeId > 0)
          programAreaDegree = _programAreaDegrees.Where(pad => pad.DegreeEducationLevelId == degreeId).Select(pad => pad.DegreeEducationLevelName).FirstOrDefault();
        else if (degreeId == 0)
          programAreaDegree = HtmlLocalise("Global.Undeclared", "Undeclared");
        else
          programAreaDegree = HtmlLocalise("Global.AllDegrees", "All degrees");
      }

      return App.Settings.ShowProgramArea && programArea.IsNotNullOrEmpty()
               ? string.Concat(programArea, "/", programAreaDegree)
               : programAreaDegree;
    }
	}
}