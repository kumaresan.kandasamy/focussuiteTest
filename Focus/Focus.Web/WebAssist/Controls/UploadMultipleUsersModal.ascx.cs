﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Text;
using System.Web;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Document;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class UploadMultipleUsersModal : UserControlBase
	{
		//TODO: This needs to be a config setting
		protected string PhoneNumberMask = "(999) 999-9999";

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			
		}

		/// <summary>
		/// Shows this instance.
		/// </summary>
		public void Show()
		{
			LocaliseUI();
			UploadMultipleUsersModalPopup.Show();
		}

		/// <summary>
		/// Handles the Click event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Click(object sender, EventArgs e)
		{
			var errorMessage = new StringBuilder();

			ResetError();

			var modelList = ServiceClientLocator.AccountClient(App).ExportAssistUsers(FileUpload.FileName, FileUpload.FileBytes);

			#region Create export validation error messages

			var errorData = modelList.Where(x => x.RegistrationError != null).ToList();

			if (errorData.IsNotNullOrEmpty())
			{
				foreach (var error in errorData)
				{
					if (error.ErrorText.IsNotNull())
					{
						foreach (var errorText in error.ErrorText)
						{
							errorMessage.Append(CodeLocalise("Fatal.ErrorMessage", errorText));
						}
					}
				}
			}

			errorData = modelList.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.InvalidEmail)).ToList();

			if (errorData.IsNotNullOrEmpty())
			{
				var rows = new StringBuilder();

				foreach (var error in errorData)
				{
					if (rows.IsNotNull() && rows.Length > 0)
						rows.Append(", ");

					rows.Append(modelList.IndexOf(error) + 2);
				}

				if (errorMessage.Length > 0)
					errorMessage.Append("<br/>");

				errorMessage.Append(CodeLocalise("InvalidEmailFormats.ErrorMessage", string.Format("Invalid email address format found on the following rows: {0}", rows)));
			}

			errorData = modelList.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber)).ToList();

			if (errorData.IsNotNullOrEmpty())
			{
				var rows = new StringBuilder();

				foreach (var error in errorData)
				{
					if (rows.IsNotNull() && rows.Length > 0)
						rows.Append(", ");

					rows.Append(modelList.IndexOf(error) + 2);
				}

				if (errorMessage.Length > 0)
					errorMessage.Append("<br/>");

				errorMessage.Append(CodeLocalise("InvalidPhoneFormats.ErrorMessage", string.Format("Invalid phone number format found on the following rows: {0}", rows)));
			}

			errorData = modelList.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.InvalidTitle)).ToList();

			if (errorData.IsNotNullOrEmpty())
			{
				var rows = new StringBuilder();

				foreach (var error in errorData)
				{
					if (rows.IsNotNull() && rows.Length > 0)
						rows.Append(", ");

					rows.Append(modelList.IndexOf(error) + 2);
				}

				if (errorMessage.Length > 0)
					errorMessage.Append("<br/>");

				errorMessage.Append(CodeLocalise("InvalidTitle.ErrorMessage", string.Format("Unsupported title found on the following rows: {0}", rows)));
			}

			errorData = modelList.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.RequiredDataNotFound)).ToList();

			if (errorData.IsNotNullOrEmpty())
			{
				var rows = new StringBuilder();

				foreach (var error in errorData)
				{
					if (rows.IsNotNull() && rows.Length > 0)
						rows.Append(", ");

					rows.Append(modelList.IndexOf(error) + 2);
				}

				if (errorMessage.Length > 0)
					errorMessage.Append("<br/>");

				errorMessage.Append(CodeLocalise("RequiredDataMissing.ErrorMessage", string.Format("One or more mandatory fields are not populated on the following rows: {0}", rows)));
			}

			#endregion

			if (errorMessage.Length > 0)
			{
				var message = errorMessage.ToString();
				StatusLabel.DefaultText = message;
			}
			else
			{
				foreach (var model in modelList)
				{
					model.UserExternalOffice = CodeLocalise("Global.UnassignedOffice.Text", "Unassigned");	
				}

				var processedModels = ServiceClientLocator.AccountClient(App).CreateAssistUsers(modelList);

				errorData = processedModels.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.UserNameAlreadyExists)).ToList();

				if (errorData.IsNotNullOrEmpty())
				{
					var rows = new StringBuilder();

					foreach (var error in errorData)
					{
						if (rows.IsNotNull() && rows.Length > 0)
							rows.Append(", ");

						rows.Append(modelList.IndexOf(error) + 2);

            if (error.AccountUserId.HasValue)
              ServiceClientLocator.AccountClient(App).ResetUsersPassword(error.AccountUserId.Value, string.Format("{0}{1}", App.Settings.AssistApplicationPath, UrlBuilder.ResetPassword(false)));
					}

					if (errorMessage.Length > 0)
						errorMessage.Append("<br/>");

          errorMessage.Append(CodeLocalise("UserNameExists.ErrorMessage", string.Format("A user already exists with the specified username on the following rows. Password reset emails were sent to the users instead: {0}", rows)));
        }

				errorData = processedModels.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.UserAlreadyExists)).ToList();

				if (errorData.IsNotNullOrEmpty())
				{
					var rows = new StringBuilder();

					foreach (var error in errorData)
					{
						if (rows.IsNotNull() && rows.Length > 0)
							rows.Append(", ");

						rows.Append(modelList.IndexOf(error) + 2);
					}

					if (errorMessage.Length > 0)
						errorMessage.Append("<br/>");

					errorMessage.Append(CodeLocalise("UserNameExists.ErrorMessage", string.Format("A user already exists with the personal details on the following rows: {0}", rows)));
				}

				#region Email address mandatory check - this should never be triggered as it should be caught in the file extract validation

				errorData = processedModels.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.PersonEmailAddressMissing)).ToList();

				if (errorData.IsNotNullOrEmpty())
				{
					var rows = new StringBuilder();

					foreach (var error in errorData)
					{
						if (rows.IsNotNull() && rows.Length > 0)
							rows.Append(", ");

						rows.Append(modelList.IndexOf(error) + 2);
					}

					if (errorMessage.Length > 0)
						errorMessage.Append("<br/>");

					errorMessage.Append(CodeLocalise("UserNameExists.ErrorMessage", string.Format("A user already exists with the details on the following rows: {0}", rows)));
				}

				#endregion

				#region External Office mandatory check - this should never be triggered as this is set in the AccountClient method

				errorData = processedModels.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.PersonOfficeMissing)).ToList();

				if (errorData.IsNotNullOrEmpty())
				{
					if (errorMessage.Length > 0)
						errorMessage.Append("<br/>");

					errorMessage.Append(CodeLocalise("UserNameExists.ErrorMessage", string.Format("Account registration requires an External Office.")));
				}

				#endregion

				#region Phone number mandatory check - this should never be triggere as it should be caught in the file extract validation

				errorData = processedModels.Where(x => x.RegistrationError != null && x.RegistrationError.Contains(ErrorTypes.PhoneNumberMissing)).ToList();

				if (errorData.IsNotNullOrEmpty())
				{
					var rows = new StringBuilder();

					foreach (var error in errorData)
					{
						if (rows.IsNotNull() && rows.Length > 0)
							rows.Append(", ");

						rows.Append(modelList.IndexOf(error) + 2);
					}

					if (errorMessage.Length > 0)
						errorMessage.Append("<br/>");

					errorMessage.Append(CodeLocalise("UserNameExists.ErrorMessage", string.Format("Phone number details are missing on the following rows: {0}", rows)));
				}

				#endregion

        if (processedModels.Count == 0)
        {
          StatusLabel.DefaultText = CodeLocalise("MultipleAssistUserUpload.NoUsers",
                                                 "0 users created.<br />The file should consist of a header record followed by at least one user record.");
        }
        else
        {
          StatusLabel.DefaultText = errorMessage.Length > 0
                                      ? CodeLocalise("MultipleAssistUserUpload.Error", errorMessage.ToString())
                                      : CodeLocalise("MultipleAssistUserUpload.ResultText",
                                                     string.Format("{0} users created", processedModels.Count));
        }
			}

			UploadMultipleUsersModalPopup.Show();
		}

		/// <summary>
		/// Handles the Click event of the Download Button to download a copy of the template
		/// </summary>
		/// <param name="sender">The button being clicked.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DownloadButton_Click(object sender, EventArgs e)
		{
			var category = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories, 0, "DocumentCategories.Templates").First();
			var group = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentGroups, category.Id, "DocumentGroups.StaffUpload").First();
			var criteria = new DocumentCriteria
			{
				Category = category.Id,
				Group = group.Id,
				FetchOption = CriteriaBase.FetchOptions.Single
			};

			var document = ServiceClientLocator.CoreClient(App).GetDocuments(criteria).FirstOrDefault();

			if (document.IsNotNull())
			{
				Response.Clear();
				Response.Buffer = true;
				Response.Charset = "";
				Response.Cache.SetCacheability(HttpCacheability.NoCache);
				Response.AddHeader("Content-type", document.MimeType);
				Response.AddHeader("content-disposition", "attachment;filename=" + document.FileName);
				Response.BinaryWrite(document.File);
				Response.Flush();
				Response.End();
			}
		}

		/// <summary>
		/// Handles the Click event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Click(object sender, EventArgs e)
		{
			ResetForm();

			UploadMultipleUsersModalPopup.Hide();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CancelButton.Text = CodeLocalise("Global.Cancel.Label", "Cancel");
			SaveButton.Text = CodeLocalise("Global.Save.Label", "Upload");
			DownloadButton.Text = CodeLocalise("Global.Download.Label", "Download Template");
			FileUploadRequired.ErrorMessage = CodeLocalise("FileSelectionRequired.ErrorMessage", "A file of user details must be selected");
		}

		/// <summary>
		/// Resets the form.
		/// </summary>
		private void ResetForm()
		{
			ResetError();
		}

		/// <summary>
		/// Resets the error.
		/// </summary>
		private void ResetError()
		{
			StatusLabel.DefaultText = string.Empty;
		}
	}
}