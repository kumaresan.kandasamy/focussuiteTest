﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Focus.Web.Core.Models;
using Framework.Core;

using Focus.Common.Models;
using Focus.Common;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Views;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	
	public partial class CandidatesLikeThisModal : UserControlBase
	{
		protected AssistTalentPoolModel Model
		{
			get { return GetViewStateValue("AssistTalentPool:Model", new AssistTalentPoolModel()); }
			set { SetViewStateValue("AssistTalentPool:Model", value); }
		}

		/// <summary>
		/// Gets or sets the candidates like this person id.
		/// </summary>
		/// <value>The candidates like this person id.</value>
		protected long CandidatesLikeThisPersonId
		{
			get { return GetViewStateValue<long>("AssistTalentPool:CandidatesLikeThisPersonId"); }
			set { SetViewStateValue("AssistTalentPool:CandidatesLikeThisPersonId", value); }
		}
		
		/// <summary>
		/// Gets or sets the candidates in candidates like this list.
		/// </summary>
		/// <value>The candidates in candidates like this list.</value>
		private List<ResumeView> CandidatesInCandidatesLikeThisList
		{
			get { return GetViewStateValue<List<ResumeView>>("PoolResult:CandidatesInCandidatesLikeThisList"); }
			set { SetViewStateValue("PoolResult:CandidatesInCandidatesLikeThisList", value); }
		}

		private int CandidatesLikeThisCount { get; set; }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyBranding();
		}

		/// <summary>
		/// Shows the specified person id.
		/// </summary>
		/// <param name="personId">The person id.</param>
		public void Show(long personId)
		{
			CandidatesLikeThisListView.DataSource = null;
			CandidatesLikeThisListView.DataBind();

			CandidatesLikeThisPersonId = personId;

			CandidatesLikeThisDataSource.SelectParameters.Clear();
			var parameter = new Parameter("personId", TypeCode.String, personId.ToString());
			CandidatesLikeThisDataSource.SelectParameters.Add(parameter);

			CandidatesLikeThisListView.DataBind();

			CandidatesLikeThisModalPopup.Show();
		}

		/// <summary>
		/// Gets the candidates like this.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<ResumeView> GetCandidatesLikeThis(string orderBy, int startRowIndex, int maximumRows)
		{
			return GetCandidatesLikeThis(String.Empty, orderBy, startRowIndex, maximumRows);
		}

		/// <summary>
		/// Gets the candidates like this.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<ResumeView> GetCandidatesLikeThis(string personId, string orderBy, int startRowIndex, int maximumRows)
		{
			if (personId.IsNullOrEmpty()) return null;

			CandidateSearchResultModel result;

			try
			{
				result = ServiceClientLocator.SearchClient(App).SearchCandidates(Guid.Empty,
																												new CandidateSearchCriteria
																												{
																													SearchType = CandidateSearchTypes.CandidatesLikeThis,
																													PageSize = 4,
																													CandidatesLikeThisSearchCriteria = new CandidatesLikeThisCriteria { CandidateId = personId }
																												});
			}
			catch
			{
				return null;
			}

			var candidates = result.Candidates;
			CandidatesLikeThisCount = candidates.TotalCount;

			return candidates;
		}

		/// <summary>
		/// Gets the candidates like this count.
		/// </summary>
		/// <returns></returns>
		public int GetCandidatesLikeThisCount()
		{
			return CandidatesLikeThisCount;
		}

		/// <summary>
		/// Gets the candidates like this count.
		/// </summary>
		/// <param name="personId">The pewrson id.</param>
		/// <returns></returns>
		public int GetCandidatesLikeThisCount(string personId)
		{
			return CandidatesLikeThisCount;
		}
		
		/// <summary>
		/// Handles the Selected event of the CandidatesLikeThisDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
		{
			try
			{
				// Save the list to view state so it can be accessed on post back
				CandidatesInCandidatesLikeThisList = (List<ResumeView>)e.ReturnValue;
			}
			catch
			{
			}
		}

		/// <summary>
		/// Handles the ItemDataBound event of the CandidatesLikeThisListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var candidate = (ResumeView)e.Item.DataItem;

			#region Name / Id

			var nameLink = (LinkButton)e.Item.FindControl("CandidatesLikeThisNameLink");
			nameLink.OnClientClick = string.Format("return ResumeLinkButtonClicked(this, {0});", candidate.Id);
			nameLink.Text = candidate.Name;

			#endregion

			#region Flagged Image

			var flaggedImage = (Image)e.Item.FindControl("CandidatesLikeThisFlaggedImage");
			if (candidate.Flagged) BindFlaggedImage(flaggedImage);

			#endregion

			#region Veteran Image

			var veteranImage = (Image)e.Item.FindControl("CandidatesLikeThisVeteranImage");
			if (candidate.IsVeteran) BindVeteranImage(veteranImage);

			#endregion

			#region Score

			var scoreImage = (Image)e.Item.FindControl("CandidatesLikeThisScoreImage");
			BindScore(scoreImage, candidate.Score);

			#endregion

			#region Employment History

			var employmentHistoryLiteral = (Literal)e.Item.FindControl("CandidatesLikeThisEmploymentHistoryLiteral");
			BindEmploymentHistory(employmentHistoryLiteral, candidate.EmploymentHistories);

			#endregion

			#region Employer Notes

			var notesTextBox = (TextBox)e.Item.FindControl("CandidatesLikeThisNotesTextBox");
			notesTextBox.Text = candidate.EmployerNote;

			#endregion

			#region Highlight Original Candidate

			if (candidate.Id == CandidatesLikeThisPersonId)
			{
				var topTableRow = (TableRow)e.Item.FindControl("CandidatesLikeThisItemTopTableRow");
				topTableRow.CssClass = "topRowHighlighted";
				var bottomTableRow = (TableRow)e.Item.FindControl("CandidatesLikeThisItemBottomTableRow");
				bottomTableRow.CssClass = "bottomRowHighlighted";
			}

			#endregion

			#region Register Link buttons Async Callback

			var candidatesLikeThisEmailButton = (LinkButton)e.Item.FindControl("CandidatesLikeThisEmailButton");
			//var candidatesLikeThisCandidatesLikeThisButton = (LinkButton)e.Item.FindControl("CandidatesLikeThisCandidatesLikeThisButton");

			GlobalScriptManager.RegisterAsyncPostBackControl(candidatesLikeThisEmailButton);
			//GlobalScriptManager.RegisterAsyncPostBackControl(candidatesLikeThisCandidatesLikeThisButton);

			#endregion

		}

		/// <summary>
		/// Handles the Click event of the CandidatesLikeThisModalCloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisModalCloseButton_Click(object sender, EventArgs e)
		{
			CandidatesLikeThisDataSource.SelectParameters.Clear();
			CandidatesLikeThisListView.DataSource = null;
			CandidatesLikeThisListView.DataBind();
			CandidatesLikeThisModalPopup.Hide();
		}

		/// <summary>
		/// Binds the flagged image.
		/// </summary>
		/// <param name="flaggedImage">The flagged image.</param>
		private void BindFlaggedImage(Image flaggedImage)
		{
			flaggedImage.ToolTip = CodeLocalise("FlaggedImage.ToolTip", "Flagged");
			flaggedImage.ImageUrl = UrlBuilder.FlaggedCandidateImage();
			flaggedImage.Visible = true;
		}

		/// <summary>
		/// Binds the veteran image.
		/// </summary>
		/// <param name="veteranImage">The veteran image.</param>
		private void BindVeteranImage(Image veteranImage)
		{
			veteranImage.ToolTip = CodeLocalise("VeteranImage.ToolTip", "Veteran");
			veteranImage.ImageUrl = UrlBuilder.VeteranImage();
			veteranImage.Visible = true;
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private static void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = OldApp_RefactorIfFound.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString();

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

		/// <summary>
		/// Binds the employment history.
		/// </summary>
		/// <param name="employmentHistoryLiteral">The employment history literal.</param>
		/// <param name="employmentHistories">The employment histories.</param>
		private void BindEmploymentHistory(Literal employmentHistoryLiteral, IEnumerable<ResumeView.ResumeJobView> employmentHistories)
		{
			var employmentHistorySummary = new StringBuilder(string.Empty);

			var unknownText = CodeLocalise("Global.Unknown.Text", "Unknown");
			var i = 1;

			foreach (var employmentHistory in employmentHistories)
			{
				if (i > 3) break;

				employmentHistorySummary.AppendFormat("{0} - {1} ({2}-{3})<br />",
																							employmentHistory.JobTitle.IsNotNullOrEmpty()
																								? employmentHistory.JobTitle
																								: unknownText,
																							employmentHistory.Employer.IsNotNullOrEmpty()
																								? employmentHistory.Employer
																								: unknownText,
																							employmentHistory.YearStart.IsNotNullOrEmpty()
																								? employmentHistory.YearStart
																								: unknownText,
																							employmentHistory.YearEnd.IsNotNullOrEmpty()
																								? employmentHistory.YearEnd
																								: unknownText);
				i++;
			}


			if (employmentHistorySummary.ToString().IsNotNullOrEmpty())
				employmentHistoryLiteral.Text = employmentHistorySummary.ToString().Substring(0, employmentHistorySummary.ToString().Length - 6);
		}

		/// <summary>
		/// Handles the Command event of the CandidatesLikeThisRowAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisRowAction_Command(object sender, CommandEventArgs e)
		{
			var rowNo = Convert.ToInt32(e.CommandArgument);
			var id = Convert.ToInt64(CandidatesLikeThisListView.DataKeys[rowNo]["Id"]);

			switch (e.CommandName)
			{
				case "CandidatesLikeThis":
					Show(id);
					break;
				case "Email":
					OnCandidatesLikeThisEmailCandidateClick(new CandidatesLikeThisEmailCandidateClickEventArgs(id));
					CandidatesLikeThisModalPopup.Hide();
					break;
			}
		}
		
		#region Events
		
		public event CandidatesLikeThisEmailCandidateClickHandler CandidatesLikeThisEmailCandidateClick;
		
		/// <summary>
		/// Raises the <see cref="E:CandidatesLikeThisEmailCandidateClick"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.CandidatesLikeThisModal.CandidatesLikeThisEmailCandidateClickEventArgs"/> instance containing the event data.</param>
		protected virtual void OnCandidatesLikeThisEmailCandidateClick(CandidatesLikeThisEmailCandidateClickEventArgs e)
		{
			if (CandidatesLikeThisEmailCandidateClick != null)
				CandidatesLikeThisEmailCandidateClick(this, e);
		}
		
		#endregion

		#region Delegates

		public delegate void CandidatesLikeThisEmailCandidateClickHandler(object o, CandidatesLikeThisEmailCandidateClickEventArgs e);
		
		#endregion

		#region EventArgs

		public class CandidatesLikeThisEmailCandidateClickEventArgs : EventArgs
		{
			public readonly long CandidateId;

			public CandidatesLikeThisEmailCandidateClickEventArgs(long candidateId)
			{
				CandidateId = candidateId;
			}
		}

		#endregion


		private void ApplyBranding()
		{
			CandidatesLikeThisModalCloseButton.ImageUrl = UrlBuilder.ButtonCloseIcon();
		}
	}
}
