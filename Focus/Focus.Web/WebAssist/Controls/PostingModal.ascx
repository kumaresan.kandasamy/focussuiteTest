﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PostingModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.PostingModal" %>

<asp:HiddenField ID="PostingModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="PostingModalPopup" runat="server" 
				TargetControlID="PostingModalDummyTarget"
				PopupControlID="PostingModalPanel"
				RepositionMode="RepositionOnWindowResizeAndScroll"
				BackgroundCssClass="modalBackground"
				BehaviorID="PostingModal" />	

<asp:Panel ID="PostingModalPanel" runat="server" CssClass="modal" Style="display:none;width:90%">
	<asp:UpdatePanel ID="PostingModalUpdatePanel" runat="server">
		<ContentTemplate>
			<div style="width: 98%;">
					<iframe id="PostingDetail" title="Posting content" runat="server" width="100%" height="430px" style="border-style:solid;border-width: 1px; padding: 5px;">Posting Detail</iframe>
			</div>
			<br/>
			<div style="width:100%;display:inline-block">
				<asp:Button ID="CloseButton" runat="server" CssClass="button2" OnClick="CloseButton_Clicked" Text="Close"/>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Panel>
