﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchesToOpenPositions.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.MatchesToOpenPositions" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>


<asp:HiddenField ID="OpenPositionsModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="OpenPositionsModalPopup" runat="server" ClientIDMode="Static"
						TargetControlID="OpenPositionsModalDummyTarget"
						PopupControlID="OpenPositionsModalPanel"
						PopupDragHandleControlID="OpenPositionsModalPanel"
						RepositionMode="None"
						BackgroundCssClass="modalBackground" />

<asp:Panel ID="OpenPositionsModalPanel" runat="server" CssClass="modal" ClientIDMode="Static">
    <h1><%= HtmlLocalise("OpenPositionsWindow.Title", "Matches to open opitions") %></h1>
    <table>
        <tr>
            <td>
	        <div style="float:left;">		
                <table width="500px">
			        <tr>
				        <td><strong><%= HtmlLocalise("SuggestedMatchTitle", "Job seeker")%></strong></td>	
			        </tr>
			        <tr>
				        <td width="100%" style="border-style:solid; border-width: 1px; padding: 5px;">
					        <div style="overflow:scroll;height:430px;">
						        <asp:Literal ID="JobSeekerDetailsLiteral" runat="server" />
					        </div>
				        </td>
			        </tr>   
		        </table>
	        </div>
            &nbsp;&nbsp;&nbsp;
	        <div style="float:right;">
		        <table width="500px">
			        <tr>
				        <td><strong><%= HtmlLocalise("PositionTitle.Text", "Position ")%></strong>
                            <asp:DropDownList runat="server" ID="OpenPositionsDropDown" OnSelectedIndexChanged="OpenPositionsDropDown_SelectedIndexChanged" AutoPostBack="True"/>
                            <asp:image ID="PositionScoreImage" runat="server" Height="15px" Width="65px" />
				        </td>
			        </tr>
			        <tr>
				        <td  style="border-style:solid; border-width: 1px; padding: 5px;">
					        <div style="overflow:scroll;height:430px;">
						        <asp:Literal ID="OpenPositionDetailsLiteral" runat="server" />
					        </div>
				        </td>
			        </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="DoNotShowCheckBox" runat="server" /> <%= HtmlLocalise("DoNotShowCheckBox.Text", "Do not show this suggestion again ")%>
                        </td>
                    </tr>
		        </table>
	        </div> 
            </td>
        </tr>
        
        <tr>
            <td>
                <br />
                <asp:Button ID="ContactEmployerButton" runat="server" class="button2" />
                &nbsp;&nbsp;
                <asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
            </td>
        </tr>
    </table>

	<div class="closeIcon">
	    <img src="<%= UrlBuilder.Content("~/Assets/Images/button_x_close_off.png") %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('OpenPositionsModalPopup').hide();" />
	</div>

</asp:Panel>

<%-- Confirmation Modal --%>
<uc:ConfirmationModal ID="Confirmation" runat="server" Height="250px" Width="400px"/>

