﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.DocumentModal" %>

<asp:HiddenField ID="DocumentModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="DocumentModalPopup" runat="server" BehaviorID="OfficeModal"
												TargetControlID="DocumentModalDummyTarget"
												PopupControlID="DocumentModalPanel"
												PopupDragHandleControlID="DocumentModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="DocumentModalPanel" runat="server" CssClass="modal" style="display: none;">
	<asp:Panel runat="server" ClientIDMode="Static" ID="DocumentModalPanelHeader">
		<div style="padding-bottom: 25px;"><h1><asp:Label runat="server" ID="MainTitle"><%= HtmlLocalise("Title.Text", "Manage Document") %></asp:Label></h1></div>
	  <div>
      <div style="display: block; padding-bottom: 10px;">
        <span style="vertical-align: top; width: 15%; display: inline-block;"><%= HtmlRequiredLabel(TitleTextBox, "Title.Label", "Title")%></span>
        <asp:TextBox runat="server" ID="TitleTextBox" Width="80%"></asp:TextBox>
        <asp:RequiredFieldValidator ID="TitleRequired" runat="server" ControlToValidate="TitleTextBox"	CssClass="error" SetFocusOnError="True" ClientIDMode="Static"  ValidationGroup="DocumentUpload"/>
      </div>
      <div style="display: block; padding-bottom: 10px;">
        <span style="vertical-align: top; width: 15%; display: inline-block;"><focus:LocalisedLabel runat="server" ID="DescriptionLabel" AssociatedControlID="DescriptionTextBox" DefaultText="Description" Width="15%"/></span>
        <asp:TextBox runat="server" ID="DescriptionTextBox" TextMode="MultiLine" Rows="5" Width="80%" ></asp:TextBox>
        <asp:RegularExpressionValidator ID="DescriptionRegEx" ControlToValidate="DescriptionTextBox" ErrorMessage="" ValidationExpression="^[\s\S]{0,2000}$" ValidationGroup="DocumentUpload" runat="server" CssClass="error" SetFocusOnError="true" /> 
      </div>
      <div style="display: block; padding-bottom: 10px;">
        <span style="vertical-align: top; width: 15%; display: inline-block;"><%= HtmlRequiredLabel(CategoryDropDown, "Category.Label", "Category")%></span>
        <asp:DropDownList runat="server" ID="CategoryDropDown"></asp:DropDownList>
        <asp:RequiredFieldValidator ID="CategoryRequired" runat="server" ControlToValidate="CategoryDropDown"	CssClass="error" SetFocusOnError="True" ClientIDMode="Static"  ValidationGroup="DocumentUpload"/>
      </div>
      <div style="display: block; padding-bottom: 10px;">
        <span style="vertical-align: top; width: 15%; display: inline-block;"><%= HtmlRequiredLabel(GroupDropdown, "Group.Label", "Group")%></span>
				<focus:AjaxDropDownList runat="server" ID="GroupDropdown"></focus:AjaxDropDownList>
				<act:CascadingDropDown ID="GroupCascader" runat="server" TargetControlID="GroupDropdown" ParentControlID="CategoryDropDown" ServicePath="~/Services/AjaxService.svc" Category="DocumentGroups" ServiceMethod="GetDocumentGroups" />

        <asp:RequiredFieldValidator ID="GroupRequired" runat="server" ControlToValidate="GroupDropdown"	CssClass="error" SetFocusOnError="True" ClientIDMode="Static"  ValidationGroup="DocumentUpload"/>
      </div>  
      <div style="display: block; padding-bottom: 20px;">
        <span style="vertical-align: top; width: 15%; display: inline-block;"><%= HtmlRequiredLabel(ModuleCheckBoxList, "Module.Label", "Module")%></span>
        <div style="display: inline-block;" id="ModulesDiv">
					<asp:CheckBoxList runat="server" ID="ModuleCheckBoxList" role="presentation"></asp:CheckBoxList>
				</div>
        <div >
          <asp:CustomValidator ID="ModuleRequired" runat="server" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="DocumentUpload" ClientValidationFunction="validateModules" ValidateEmptyText="true" />
        </div>
      </div>
      <div style="display: block; padding-bottom: 20px;" id="FileNameDiv" runat="server" ClientIDMode="Static">
        <span style="vertical-align: top; width: 15%; display: inline-block;"><focus:LocalisedLabel runat="server" ID="FileNameLabel" DefaultText="File name:"/></span>
        <asp:Label runat="server" ID="DocumentFileNameLabel"></asp:Label>
      </div>
      <div style="display: block">
        <div runat="server" id="UploadDocumentDiv" ClientIDMode="Static" style="width: 100%;">
	        <focus:LocalisedLabel runat="server" ID="DocumentUploadLabel" AssociatedControlID="DocumentUpload" LocalisationKey="DocumentUpload.Label" DefaultText="Document upload" CssClass="sr-only"/>
          <asp:FileUpload ID="DocumentUpload" runat="server" ClientIDMode="Static" CssClass="fileupload"/>
					<asp:RequiredFieldValidator ID="DocumentUploadRequired" runat="server" ControlToValidate="DocumentUpload"	CssClass="error" SetFocusOnError="false" ClientIDMode="Static" Display="Dynamic" ValidationGroup="DocumentUpload"/>
          <focus:InsensitiveRegularExpressionValidator runat="server" ID="FileUploadRegEx" CssClass="error" ControlToValidate="DocumentUpload" ValidationGroup="DocumentUpload" SetFocusOnError="true" Display="Dynamic"/>
        </div>
				<div style="float: right;">
					<asp:Button runat="server" ID="UploadButton" Text="Upload" CssClass="button3" OnClick="UploadButton_Click" style="float: right;" ValidationGroup="DocumentUpload"/>
				</div>
        <div runat="server" id="EditIconsDiv" ClientIDMode="Static">
          <asp:ImageButton ID="DownloadDocumentImageButton" runat="server" OnClick="DownloadDocument_Click"/>
          <asp:ImageButton ID="ReplaceDocumentImageButton" runat="server" OnClientClick="return ShowUpload();"/>
        </div>
      </div>
  </div>
	</asp:Panel>
	<div>
		<asp:HiddenField runat="server" ID="IdHiddenField" ClientIDMode="Static"/>
	</div>
 	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('<%=DocumentModalPopup.BehaviorID %>').hide(); return false;" /></div>
</asp:Panel>
<script type="text/javascript">

function ShowUpload() {
  $("#UploadDocumentDiv").show();
  $("#EditIconsDiv").hide();
  $("#FileNameDiv").hide();
  ValidatorEnable($("#<%= DocumentUploadRequired.ClientID %>")[0], true);
  return false;
}

function validateModules(src, args) {
  args.IsValid = $('#ModulesDiv input:checkbox').is(':checked');
}
 
</script>