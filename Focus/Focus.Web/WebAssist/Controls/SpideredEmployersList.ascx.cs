﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.Models.Assist;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Focus.Core.Criteria.SpideredPosting;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class SpideredEmployersList : UserControlBase
	{
		private int _spideredEmployersViewCount;

		/// <summary>
		/// Gets or sets the employer filter.
		/// </summary>
		/// <value>The employer filter.</value>
		private string EmployerFilter
		{
			get { return GetViewStateValue<string>("SpideredEmployersList:EmployerFilter"); }
			set { SetViewStateValue("SpideredEmployersList:EmployerFilter", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
		}

		/// <summary>
		/// Handles the Clicked event of the FilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FilterButton_Clicked(object sender, EventArgs e)
		{
			SpideredEmployersListPager.ReturnToFirstPage();
			EmployerFilter = FilterTextBox.TextTrimmed();
			SpideredEmployersListView.DataBind();
			SpideredEmployersListUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the DataPaged event of the SpideredEmployersListPager control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.Pager.DataPagedEventArgs"/> instance containing the event data.</param>
		protected void SpideredEmployersListPager_DataPaged(object sender, Pager.DataPagedEventArgs e)
		{
			SpideredEmployersListUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the Closed event of the ComparePostingToResumeModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.ComparePostingToResumeModal.ModalClosedEventArgs"/> instance containing the event data.</param>
		protected void ComparePostingToResumeModal_Closed(object sender, ComparePostingToResumeModal.ModalClosedEventArgs e)
		{
			if (e.DataUpdated)
			{
				SpideredEmployersListView.DataBind();
				SpideredEmployersListUpdatePanel.Update();
			}
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the spidered employers.
		/// </summary>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<SpideredEmployerModel> GetSpideredEmployers(string employerName, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int) Math.Floor((double) startRowIndex/maximumRows));

			var criteria = new SpideredEmployerCriteria
			               	{
			               		EmployerName = employerName,
			               		MinimumPostingCount = App.Settings.JobDevelopmentSpideredJobListNumberOfPostingsRequired,
			               		FetchOption = CriteriaBase.FetchOptions.PagedList,
			               		OrderBy = orderBy,
			               		PageIndex = pageIndex,
			               		PageSize = maximumRows,
			               		ListSize = App.Settings.JobDevelopmentSpideredJobListMaximumNumberOfEmployers,
			               		PostingAge = App.Settings.JobDevelopmentSpideredJobListPostingAge
			               	};

			if (!App.Settings.NationWideInstance)
				criteria.StateCodes = GetSearchableStateCodes();

			try
			{
				var employersPaged = ServiceClientLocator.EmployerClient(App).GetSpideredEmployers(criteria);
				_spideredEmployersViewCount = employersPaged.TotalCount;

				return employersPaged;
			}
			catch
			{
				_spideredEmployersViewCount = 0;
				return new List<SpideredEmployerModel>{ new SpideredEmployerModel
					{
						EmployerName = "ERROR",
						Id = -1
					} 
				};
			}
		}

		/// <summary>
		/// Gets the spidered employers count.
		/// </summary>
		/// <returns></returns>
		public int GetSpideredEmployersCount()
		{
			return _spideredEmployersViewCount;
		}

		/// <summary>
		/// Gets the spidered employers count.
		/// </summary>
		/// <param name="employerName">Name of the employer.</param>
		/// <returns></returns>
		public int GetSpideredEmployersCount(string employerName)
		{
			return _spideredEmployersViewCount;
		}

		/// <summary>
		/// Handles the Selecting event of the SpideredEmployersDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void SpideredEmployersDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["employerName"] = EmployerFilter;
		}

		#endregion

		#region Spidered Employers list events

		/// <summary>
		/// Handles the LayoutCreated event of the SpideredEmployersListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SpideredEmployersListView_LayoutCreated(object sender, EventArgs e)
		{
			#region Set labels

			((Literal)SpideredEmployersListView.FindControl("EmployerNameHeaderLiteral")).Text = CodeLocalise("EmployerNameName.ColumnTitle", "#BUSINESS# Name");
			((Literal) SpideredEmployersListView.FindControl("JobTitleHeaderLiteral")).Text = CodeLocalise("JobTitle.ColumnTitle", "Job Title");
			((Literal) SpideredEmployersListView.FindControl("PostingDateHeaderLiteral")).Text = CodeLocalise("PostingDate.LastActivity", "Posting Date");
			((Literal) SpideredEmployersListView.FindControl("ActionsHeaderLiteral")).Text = CodeLocalise("ActionsIssues.ColumnTitle", "Actions");

			((ImageButton)SpideredEmployersListView.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			((ImageButton)SpideredEmployersListView.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
			#endregion
		}

		/// <summary>
		/// Handles the Sorting event of the SpideredEmployersListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void SpideredEmployersListView_Sorting(object sender, ListViewSortEventArgs e)
		{
			FormatSpideredEmployersListViewSortingImages(e.SortExpression);
			SpideredEmployersListUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the SpideredEmployersListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void SpideredEmployersListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var item = (SpideredEmployerModel) e.Item.DataItem;
			if (item.EmployerName == "ERROR" && item.Id == -1)
			{
				SpideredEmployersListView.Visible = false;
				ErrorLabel.Visible = true;
			}
			else
			{
				SpideredEmployersListView.Visible = true;
				ErrorLabel.Visible = false;
			}
		}

		/// <summary>
		/// Handles the Command event of the SearchMatchesLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void SearchMatchesLinkButton_Command(object sender, CommandEventArgs e)
		{
			var lensPostingId = e.CommandArgument.ToString();

			if (lensPostingId.IsNotNullOrEmpty())
				ComparePostingToResumeModal.Show(lensPostingId, true);
		}

		/// <summary>
		/// Handles the Command event of the ViewPostingLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ViewPostingLinkButton_Command(object sender, CommandEventArgs e)
		{
			var postingId = e.CommandArgument.ToString();

			if (postingId.IsNotNullOrEmpty())
				PostingModal.Show(postingId);
		}

		/// <summary>
		/// Handles the OnItemCommand event of the SpideredEmployersListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewCommandEventArgs"/> instance containing the event data.</param>
		protected void SpideredEmployersListView_OnItemCommand(object sender, ListViewCommandEventArgs e)
		{
			if (String.Equals(e.CommandName, "GetPostings"))
			{
				var postingRepeater = (Repeater) e.Item.FindControl("PostingsRepeater");

				if (postingRepeater.IsNull())
					return;

				if (postingRepeater.Items.Count == 0)
				{
					var employerName = e.CommandArgument.ToString();

					if (employerName.IsNotNullOrEmpty())
					{
						var criteria = new SpideredPostingCriteria
						               	{
						               		FetchOption = CriteriaBase.FetchOptions.PagedList,
						               		PageIndex = 0,
						               		PageSize = App.Settings.JobDevelopmentSpideredJobListPostingsToDisplay,
						               		EmployerName = employerName,
						               		PostingAge = App.Settings.JobDevelopmentSpideredJobListPostingAge
						               	};

						if (!App.Settings.NationWideInstance)
							criteria.StateCodes = GetSearchableStateCodes();

						var postings = ServiceClientLocator.PostingClient(App).GetSpideredPostings(criteria);

						postingRepeater.DataSource = postings;
						postingRepeater.DataBind();
						postingRepeater.Visible = true;
					}
				}
				else
				{
					postingRepeater.Visible = (!postingRepeater.Visible);
				}

				SpideredEmployersListUpdatePanel.Update();
			}
		}

		/// <summary>
		/// Handles the Command event of the ContactEmployerLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ContactEmployerLinkButton_Command(object sender, CommandEventArgs e)
		{
			var lensPostingId = e.CommandArgument.ToString();

			var postingId = ServiceClientLocator.PostingClient(App).GetPostingId(lensPostingId);

			if (postingId.HasValue)
				Response.Redirect(UrlBuilder.PostingEmployerContact(postingId.Value));
		}

		#endregion

		/// <summary>
		/// Formats the spidered employers list view sorting images.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		private void FormatSpideredEmployersListViewSortingImages(string orderBy)
		{
			#region Format images

			// Reset image URLs
			((ImageButton) SpideredEmployersListView.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton) SpideredEmployersListView.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton) SpideredEmployersListView.FindControl("EmployerNameSortAscButton")).Enabled =
				((ImageButton) SpideredEmployersListView.FindControl("EmployerNameSortDescButton")).Enabled = true;

			switch (orderBy)
			{
				case Constants.SortOrders.EmployerNameAsc:
					((ImageButton) SpideredEmployersListView.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton) SpideredEmployersListView.FindControl("EmployerNameSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.EmployerNameDesc:
					((ImageButton) SpideredEmployersListView.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton) SpideredEmployersListView.FindControl("EmployerNameSortDescButton")).Enabled = false;
					break;
			}

			#endregion
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FilterButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
			ErrorLabel.Text = CodeLocalise("ErrorLabel.Text", "Unable to retrieve employers at this time. Please try again later");
		}

		/// <summary>
		/// Gets the searchable state codes.
		/// </summary>
		/// <returns></returns>
		private List<string> GetSearchableStateCodes()
		{
			return ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => App.Settings.SearchableStateKeys.ToList().Contains(x.Key)).Select(x => x.ExternalId).ToList();
		}
	}
}
