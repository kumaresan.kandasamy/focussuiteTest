﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabNavigation.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.TabNavigation" %>
<ul class="navTab">
  <li ID="TabDashboard" runat="server"><a href="<%= UrlBuilder.AssistDashboard() %>"><%= HtmlLocalise("TabDashboard.Label.NoEdit", "Dashboard") %></a></li>
  <li ID="TabJobSeekers" runat="server"><a href="<%= UrlBuilder.AssistJobSeekers() %>"><%= HtmlLocalise("TabJobSeekers.Label.NoEdit", "Assist #CANDIDATETYPES#") %></a></li><%-- Replaced plural: Job Seekers --%>
	<li ID="TabEmployers" runat="server"><a href="<%= UrlBuilder.JobOrderDashboard() %>"><%= HtmlLocalise("TabEmployers.Label.NoEdit", "Assist #BUSINESSES#")%></a></li>
	<li ID="TabApprovalQueues" runat="server"><a href="<%= UrlBuilder.AssistQueues() %>"><%= HtmlLocalise("TabApprovalQueue.Label.NoEdit", "Approval queues")%></a></li>
  <li id="TabReporting" runat="server"><a href="<%= UrlBuilder.ReportingHome() %>"><%= HtmlLocalise("TabReporting.Label.NoEdit", "Reporting")%></a></li>
	<li ID="TabManage" runat="server"><a href="<%= UrlBuilder.ManageAssist() %>"><%= HtmlLocalise("TabManage.Label.NoEdit", "Manage Focus Suite")%></a></li>
</ul>
