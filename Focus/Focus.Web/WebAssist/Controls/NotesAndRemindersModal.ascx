﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotesAndRemindersModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.NotesAndRemindersModal" %>

<%@ Register src="~/WebAssist/Controls/NotesAndReminders.ascx" tagName="NotesAndReminders" tagPrefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagName="ConfirmationModal" tagPrefix="uc" %>

<asp:HiddenField ID="NotesRemindersModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="NotesRemindersModalPopup" runat="server" BehaviorID="NotesRemindersModal"
												TargetControlID="NotesRemindersModalDummyTarget"
												PopupControlID="NotesRemindersModalPanel"
												PopupDragHandleControlID="NotesRemindersModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="NotesRemindersModalPanel" runat="server" CssClass="modal" style="display:none" >
	<div style="text-align:left">
		<asp:Panel runat="server" ClientIDMode="Static" ID="NotesRemindersModalPanelHeader">
		<h2>Notes and reminders</h2></asp:Panel>
		<div style="overflow-y: auto; overflow-x: hidden; ">
			<uc:NotesAndReminders ID="NotesReminders" OnCompleted="AddNote_Completed" runat="server" />
		</div>
		</div>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('<%=NotesRemindersModalPopup.BehaviorID %>').hide(); return false;" /></div>
</asp:Panel>

<asp:UpdatePanel runat="server" UpdateMode="Always">
	<ContentTemplate>
		<uc:ConfirmationModal ID="NotesRemindersConfirmationModal" runat="server"/>
	</ContentTemplate>
</asp:UpdatePanel>
