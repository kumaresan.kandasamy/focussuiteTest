﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core.Criteria.Employer;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class ListOfficeStaffMembersModal : UserControlBase
	{
		#region Properties

		private long _officeId;

		#endregion


		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Shows the specified office id.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		public void Show(long officeId)
		{
			HeaderLabel.Text = CodeLocalise("HeaderLabel.Text", "Staff members");

			_officeId = officeId;

			var managers = ServiceClientLocator.EmployerClient(App).GetOfficeManagers(officeId);

			var managerHeaderLabel = CodeLocalise("OfficeManagerHeaderLabel.Text", "Manager:  ");
			var stringBuilder = new StringBuilder();

			if (managers.IsNotNull() && managers.Count > 0)
			{
				if (managers.Count > 1)
					managerHeaderLabel = CodeLocalise("OfficeManagerHeaderLabel.Text", "Managers:  ");

				foreach (var manager in managers)
				{
					if (stringBuilder.Length > 0)
						stringBuilder.Append(", ");

					stringBuilder = stringBuilder.Append(manager.FirstName);
					stringBuilder = stringBuilder.Append(" ");
					stringBuilder = stringBuilder.Append(manager.LastName);
				}
			}
			else
			{
				stringBuilder = stringBuilder.Append(CodeLocalise("OfficeManagerLabel.NoManagerText", "None"));
			}


			OfficeManagerHeaderLabel.Text = managerHeaderLabel;
			OfficeManagerLabel.Text = stringBuilder.ToString();

			BindStaffMembersList();

			ListOfficeStaffMembersModalPopup.Show();
		}

		/// <summary>
		/// Binds the staff members list.
		/// </summary>
		private void BindStaffMembersList()
		{
			var criteria = new StaffMemberCriteria { OfficeIds = new List<long?>{_officeId}, OrderBy = "LastName desc" };
			var staff = ServiceClientLocator.EmployerClient(App).GetOfficeStaffMembersPagedList(criteria);
			StaffMemberList.DataSource = staff;
			StaffMemberList.DataBind();

			if (staff.IsNotNull() && staff.Count == 0)
			{
				NoStaffLabel.Text = CodeLocalise("NoStaffLabel.Text", "No-one is currently assigned to handle work for this office");
				NoStaffRow.Visible = true;
			}
			else
			{
				NoStaffRow.Visible = false;
			}
				
			
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			OkButton.Text = CodeLocalise("OkButton.Text", "Ok");
		}

		/// <summary>
		/// Handles the ItemDataBound event of the StaffMemberList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void StaffMemberList_ItemDataBound(object sender, EventArgs e)
		{
			// Set labels
			((Literal)StaffMemberList.FindControl("FirstNameHeader")).Text = CodeLocalise("FirstNameHeader.Text", "First name");
			((Literal)StaffMemberList.FindControl("LastNameHeader")).Text = CodeLocalise("LastNameHeader.Text", "Last name");
		}
	}
}