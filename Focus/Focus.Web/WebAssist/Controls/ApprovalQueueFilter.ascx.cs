﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.Employer;
using Focus.Core.Criteria.Job;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Assist;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class ApprovalQueueFilter : UserControlBase
	{
		private bool LocationsInitialised
		{
			get { return GetViewStateValue<bool>("ApprovalQueueFilter:LocationsInitialised"); }
			set { SetViewStateValue("ApprovalQueueFilter:LocationsInitialised", value); }
		}

		private JobSeekerReferralCriteria JobSeekerReferralCriteria
		{
			get { return App.GetSessionValue<JobSeekerReferralCriteria>("ListApprovalRequests:JobSeekerReferralCriteria"); }
		}

		private EmployerAccountReferralCriteria EmployerAccountReferralCriteria
		{
			get { return App.GetSessionValue<EmployerAccountReferralCriteria>("ListApprovalRequests:EmployerAccountReferralCriteria"); }
		}

		private JobPostingReferralCriteria JobPostingReferralCriteria
		{
			get { return App.GetSessionValue<JobPostingReferralCriteria>("PostingReferrals:Criteria"); }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				OfficePlaceHolder.Visible = App.Settings.OfficesEnabled;
			}
		}

		/// <summary>
		/// Initialises the specified type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="filtered">Is the Search Criteria Filtered</param>
		public void Initialise(ApprovalQueueFilterType type, bool filtered = false)
		{
			if (type == ApprovalQueueFilterType.Employer)
			{
				JobTypeDropDownListLabel.Visible = JobTypeDropDownList.Visible = false;
				EmployerTypeDropDownListLabel.Visible = EmployerTypeDropDownList.Visible = true;
				JobSeekerTypeDropDownListLabel.Visible = JobSeekerTypeDropDownList.Visible = false;
				BindEmployerTypeDropDownList(filtered);
			}
			else if (type == ApprovalQueueFilterType.Job)
			{
				JobTypeDropDownListLabel.Visible = JobTypeDropDownList.Visible = true;
				EmployerTypeDropDownListLabel.Visible = EmployerTypeDropDownList.Visible = false;
				JobSeekerTypeDropDownListLabel.Visible = JobSeekerTypeDropDownList.Visible = false;
				BindJobTypeDropDownList(filtered);
			}
			else if (type == ApprovalQueueFilterType.JobSeeker)
			{
				JobSeekerTypeDropDownListLabel.Visible = JobSeekerTypeDropDownList.Visible = true;
				JobTypeDropDownListLabel.Visible = JobTypeDropDownList.Visible = false;
				EmployerTypeDropDownListLabel.Visible = EmployerTypeDropDownList.Visible = false;
				BindJobSeekerTypeDropDownList(filtered);
			}

			BindLocationDropDownList(type, filtered);
		}

		/// <summary>
		/// Binds the job type drop down list.
		/// </summary>
		private void BindJobTypeDropDownList(bool filtered)
		{
			JobTypeDropDownList.Items.Clear();

			JobTypeDropDownList.Items.AddEnum(JobListFilter.None, CodeLocalise("AllJobs.Text.NoEdit", "All jobs"));
			JobTypeDropDownList.Items.AddEnum(JobListFilter.CommissionOnly, CodeLocalise("CommissionOnly.Text.NoEdit", "Commission only"));
			JobTypeDropDownList.Items.AddEnum(JobListFilter.CourtOrderedAffirmativeActionJobs, CodeLocalise("CourtOrderedAffirmativeActionJobs.Text.NoEdit", "Court-ordered affirmative action"));

			JobTypeDropDownList.Items.AddEnum(JobListFilter.FederalContractorJobs, CodeLocalise("FederalContractorJobs.Text.NoEdit", "Federal contractor"));
			JobTypeDropDownList.Items.AddEnum(JobListFilter.ForeignLaborJobsH2A, CodeLocalise("ForeignLaborJobsH2A.Text.NoEdit", "Foreign labor (H2A-ag)"));
			JobTypeDropDownList.Items.AddEnum(JobListFilter.ForeignLaborJobsH2B, CodeLocalise("ForeignLaborJobsH2B.Text.NoEdit", "Foreign labor (H2B-nonag)"));
			JobTypeDropDownList.Items.AddEnum(JobListFilter.ForeignLaborJobsOther, CodeLocalise("ForeignLaborJobsOther.Text.NoEdit", "Foreign labor (other)"));

			if (App.Settings.Theme == FocusThemes.Workforce && App.User.IsInRole(Constants.RoleKeys.AssistHomeBasedApprover))
				JobTypeDropDownList.Items.AddEnum(JobListFilter.HomeBased, CodeLocalise("HomeBased.Text.NoEdit", "Home-based"));

			if (App.Settings.Theme == FocusThemes.Workforce && App.Settings.NoFixedLocation)
				JobTypeDropDownList.Items.AddEnum(JobListFilter.NoFixedLocation, CodeLocalise("NoFixedLocation.Text.NoEdit", "No fixed location"));

			JobTypeDropDownList.Items.AddEnum(JobListFilter.SalaryAndCommission, CodeLocalise("SalaryAndCommission.Text.NoEdit", "Salary + commission based"));

			JobTypeDropDownList.SelectedValue = JobListFilter.None.ToString();

			if (filtered && JobPostingReferralCriteria.IsNotNull() && JobPostingReferralCriteria.JobType.IsNotNull())
				JobTypeDropDownList.SelectedValue = JobPostingReferralCriteria.JobType.ToString();
		}

		/// <summary>
		/// Binds the employer type drop down list.
		/// </summary>
		private void BindEmployerTypeDropDownList(bool filtered)
		{
			EmployerTypeDropDownList.Items.Clear();

			EmployerTypeDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.OwnershipTypes), null, CodeLocalise("Global.AllEmployers.TopDefault.NoEdit", "All #BUSINESSES#:LOWER"));
			EmployerTypeDropDownList.Items.Add(new ListItem("Out-of-state", "outofstate"));
			EmployerTypeDropDownList.Items.Add(new ListItem("In-state", "instate"));

			if (filtered && EmployerAccountReferralCriteria.IsNotNull() && EmployerAccountReferralCriteria.EmployerType.IsNotNull())
				EmployerTypeDropDownList.SelectedValue = EmployerAccountReferralCriteria.EmployerType;
		}

		/// <summary>
		/// Binds the job seeker type drop down list.
		/// </summary>
		private void BindJobSeekerTypeDropDownList(bool filtered)
		{
			JobSeekerTypeDropDownList.Items.Clear();

			JobSeekerTypeDropDownList.Items.AddLocalisedTopDefault("VeteranDropDown.AllJobSeekers.TopDefault", "All #CANDIDATETYPES#:LOWER");
			JobSeekerTypeDropDownList.Items.Add(new ListItem(CodeLocalise(VeteranFilterTypes.NonVeteran, "Non-veteran #CANDIDATETYPES#:LOWER"), VeteranFilterTypes.NonVeteran.ToString()));
			JobSeekerTypeDropDownList.Items.Add(new ListItem(CodeLocalise(VeteranFilterTypes.Veteran, "Veteran #CANDIDATETYPES#:LOWER"), VeteranFilterTypes.Veteran.ToString()));

			if (filtered && JobSeekerReferralCriteria.IsNotNull() && JobSeekerReferralCriteria.JobSeekerType.IsNotNull())
				JobSeekerTypeDropDownList.SelectedValue = JobSeekerReferralCriteria.JobSeekerType.ToString();
		}

		/// <summary>
		/// Binds the location drop down list.
		/// </summary>
		private void BindLocationDropDownList(ApprovalQueueFilterType type = ApprovalQueueFilterType.JobSeeker, bool filtered = false)
		{
			if (LocationsInitialised)
				return;

			LocationsInitialised = true;

			LocationDropDownList.Items.Clear();

			var isStateWide = ServiceClientLocator.EmployerClient(App).IsPersonStatewide(App.User.PersonId.GetValueOrDefault());

			List<OfficeDto> offices;

			if (App.Settings.ShowAllOfficesReferralRequestQueue && !isStateWide)
			{
				offices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria
				{
					OfficeGroup = OfficeGroup.StatewideOffices,
					FetchOption = CriteriaBase.FetchOptions.List,
					InActive = false
				});
			}
			else
			{
				offices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria
				{
					OfficeGroup = OfficeGroup.MyOffices,
					FetchOption = CriteriaBase.FetchOptions.List,
					InActive = false
				});
			}

			LocationDropDownList.DataSource = offices;
			LocationDropDownList.DataValueField = "Id";
			LocationDropDownList.DataTextField = "OfficeName";
			LocationDropDownList.DataBind();

			if (isStateWide)
				LocationDropDownList.Items.AddLocalisedTopDefault("Statewide.Select.NoEdit", "Statewide");

			if (App.User.PersonId.IsNotNull() && App.Settings.OfficesEnabled)
			{
				var currentOfficeId = Utilities.GetCurrentOffice(App.User.PersonId.Value);
				if (currentOfficeId.IsNotNull())
					LocationDropDownList.SelectedValue = currentOfficeId.ToString();

				if (filtered)
				{
					LocationDropDownList.SelectedIndex = 0;

					switch (type)
					{
							case ApprovalQueueFilterType.JobSeeker:
							if (JobSeekerReferralCriteria.OfficeIds.IsNotNullOrEmpty())
								LocationDropDownList.SelectedValue = JobSeekerReferralCriteria.OfficeIds.FirstOrDefault().Value.ToString();
							break;

							case ApprovalQueueFilterType.Employer:
							if (EmployerAccountReferralCriteria.OfficeIds.IsNotNullOrEmpty())
							LocationDropDownList.SelectedValue = EmployerAccountReferralCriteria.OfficeIds.FirstOrDefault().Value.ToString();
							break;

							case ApprovalQueueFilterType.Job:
							break;
					}
				}
			}
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public ApprovalQueueFilterModel Unbind()
		{
			BindLocationDropDownList();

			return new ApprovalQueueFilterModel
			{
				JobType = JobTypeDropDownList.SelectedValue.IsNotNullOrEmpty() ? JobTypeDropDownList.SelectedValueToEnum<JobListFilter>() : (JobListFilter?)null,
				EmployerType = EmployerTypeDropDownList.SelectedValue.IsNotNullOrEmpty() ? EmployerTypeDropDownList.SelectedValue : null,
				OfficeId = LocationDropDownList.SelectedValue.IsNotNullOrEmpty() ? Convert.ToInt64(LocationDropDownList.SelectedValue) : (long?)null,
				JobSeekerType = JobSeekerTypeDropDownList.SelectedValue.IsNotNullOrEmpty() ? JobSeekerTypeDropDownList.SelectedValueToEnum<VeteranFilterTypes>() : (VeteranFilterTypes?)null,
			};
		}
	}
}