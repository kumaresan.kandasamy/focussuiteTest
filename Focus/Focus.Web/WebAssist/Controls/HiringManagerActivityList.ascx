﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HiringManagerActivityList.ascx.cs"
    Inherits="Focus.Web.WebAssist.Controls.HiringManagerActivityList" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal"
    TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/EmailEmployeeModal.ascx" TagName="EmailEmployeeModal"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ActivityAssignment" Src="~/WebAssist/Controls/ActivityAssignment.ascx" %>
<%@ Register Src="~/WebAssist/Controls/ActivityDeleteModal.ascx" TagName="ActivityDeleteModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/BackdateActivityModal.ascx" TagName="BackdateActivityModal"
    TagPrefix="uc" %>
<asp:UpdatePanel ID="HiringManagerActivityListUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table style="width: 100%;" role="presentation">
            <tr>
                <td style="white-space: nowrap;">
                    <%=HtmlLabel(ServiceDropDown,"Show", "Show")%>
                    <asp:DropDownList runat="server" ID="ServiceDropDown" OnSelectedIndexChanged="ServiceDropDown_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <%=HtmlLabel(DaysBackDropDown,"for", "for")%>
                    <asp:DropDownList runat="server" ID="DaysBackDropDown" OnSelectedIndexChanged="DaysBackDropdown_SelectedIndexChanged"
                        AutoPostBack="True" />
                    <%=HtmlLabel(UserDropDown,"by", "by")%>
                    <asp:DropDownList runat="server" ID="UserDropDown" OnSelectedIndexChanged="UserDropDown_SelectedIndexChanged"
                        AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    <uc:Pager ID="HiringManagerActivityListPager" runat="server" PagedControlId="HiringManagerActivityLogList"
                        DisplayRecordCount="False" PageSize="10" />
                </td>
            </tr>
        </table>
        <asp:ListView runat="server" ID="HiringManagerActivityLogList" ItemPlaceholderID="ListPlaceHolder"
            DataSourceID="HiringManagerActivityListDataSource" OnSorting="HiringManagerActivityLogList_Sorting"
            OnLayoutCreated="HiringManagerActivityLogList_LayoutCreated" OnItemDataBound="HiringManagerActivityLogList_ItemDataBound"
            OnItemCommand="HiringManagerActivityLogList_ItemCommand">
            <LayoutTemplate>
                <table style="width: 100%;" class="table" id="HiringManagerActivityTable" role="presentation">
                    <tr>
                        <td class="tableHead">
                            <table role="presentation">
                                <tr>
                                    <td rowspan="2" style="height: 16px">
                                        <asp:Literal runat="server" ID="UserName" />
                                    </td>
                                    <td style="height: 8px">
                                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="UserNameSortAscButton"
                                            runat="server" CommandName="Sort" CommandArgument="activityusername asc" CssClass="AscButton"
                                            AlternateText="." Width="15" Height="9" />
                                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="UserNameSortDescButton"
                                            runat="server" CommandName="Sort" CommandArgument="activityusername desc" CssClass="DescButton"
                                            AlternateText="." Width="15" Height="9" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tableHead">
                            <table role="presentation">
                                <tr>
                                    <td rowspan="2" style="height: 16px">
                                        <asp:Literal runat="server" ID="ActivityDate" />
                                    </td>
                                    <td style="height: 8px">
                                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ActivityDateSortAscButton"
                                            runat="server" CommandName="Sort" CommandArgument="activitydate asc" AlternateText="."
                                            CssClass="AscButton" Width="15" Height="9" />
                                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ActivityDateSortDescButton"
                                            runat="server" CommandName="Sort" CommandArgument="activitydate desc" AlternateText="."
                                            CssClass="DescButton" Width="15" Height="9" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tableHead" style="text-align: left;">
                            <asp:Literal runat="server" ID="Action" />
                        </td>
                        <td class="tableHead">
                        </td>
                        <td class="tableHead">
                        </td>
                    </tr>
                    <asp:PlaceHolder ID="ListPlaceHolder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td runat="server" id="UsernameDisplay">
                        <%# ((HiringManagerActivityViewDto)Container.DataItem).EmployeeLastName%>,
                        <%# ((HiringManagerActivityViewDto)Container.DataItem).EmployeeFirstName%>
                    </td>
                    <td>
                        <%# ((HiringManagerActivityViewDto)Container.DataItem).ActionedOn%>
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="ActionDescription"></asp:Literal>
                    </td>
                    <td style="padding-right: 10px; padding-left: 10px; white-space: nowrap;">
                        <asp:LinkButton runat="server" ID="HiringManagerBackdateLinkbutton" Text="Backdate"
                            CommandName="backdateActivity" CommandArgument='<%# Eval("Id")+ ", " + Eval("EntityIdAdditional01") + "," + Eval("UserId") + "," + Eval("ActionName") + "," + Eval("ActionedOn") %>' />
                    </td>
                    <td>
                        <asp:LinkButton runat="server" ID="HiringManagerDeleteLinkButton" Text="Delete" CommandName="deleteActivity"
                            CommandArgument='<%# Eval("Id")+ ", " + Eval("EntityIdAdditional01") + "," + Eval("UserId") + "," + Eval("ActionName") + "," + Eval("ActionedOn") %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <br />
                <%=HtmlLocalise("NoJobActivity", "No activity recorded for this hiring manager") %></EmptyDataTemplate>
        </asp:ListView>
        <asp:ObjectDataSource runat="server" ID="HiringManagerActivityListDataSource" TypeName="Focus.Web.WebAssist.Controls.HiringManagerActivityList"
            SortParameterName="orderBy" SelectMethod="GetHiringManagerActivity" SelectCountMethod="GetHiringManagerActivityCount"
            OnSelecting="HiringManagerActivityListDataSource_Selecting" EnablePaging="True">
        </asp:ObjectDataSource>
    </ContentTemplate>
</asp:UpdatePanel>
<br />
<div>
    <%= HtmlLabel(ActionDropDown,"WebAssist.JobOrderDashboard.Action.Label", "Action")%>
    <asp:DropDownList runat="server" ID="ActionDropDown" Width="350" Visible="True" ClientIDMode="Static"
        Style="margin-left: 5px;" />
    <asp:Button ID="ActionGoButton" runat="server" class="button3" Visible="True" ClientIDMode="Static"
        OnClick="ActionButton_Clicked" ValidationGroup="Action" Text="Go" />
    <asp:RequiredFieldValidator ID="ActionDropDownRequired" runat="server" ValidationGroup="Action"
        ControlToValidate="ActionDropDown" CssClass="error" />
</div>
<br />
<div class="offset-1">
    <uc:ActivityAssignment ID="ActivityAssigner" runat="server" ActivityType="Employer"
        Flow="Horizontal" OnActivitySelected="ActivityAssignment_ActivitySelected" Visible="False" />
    <focus:LocalisedLabel runat="server" ID="SubActionDropDownLabel" AssociatedControlID="SubActionDropDown"
        LocalisationKey="SubActionDropDownL.abel" DefaultText="Sub activity" CssClass="sr-only"
        Visible="false" />
    <asp:DropDownList runat="server" ID="SubActionDropDown" Width="225" Visible="false"
        ClientIDMode="Static" />
    <asp:Button ID="SubActionButton" runat="server" class="button3" Visible="false" ClientIDMode="Static"
        ValidationGroup="SubAction" OnClick="SubActionButton_Clicked" Text="Assign" />
    <asp:RequiredFieldValidator ID="SubActionDropDownRequired" runat="server" ValidationGroup="SubAction"
        ControlToValidate="SubActionDropDown" CssClass="error" />
</div>
<uc:ConfirmationModal ID="Confirmation" runat="server" />
<uc:EmailEmployeeModal ID="EmailEmployee" runat="server" />
<uc:ActivityDeleteModal ID="HMDeleteActivity" runat="server" OnActivityDeleted="HMDeleteActivity_ActivityDeleted" />
<uc:BackdateActivityModal ID="HMBackdateActivity" runat="server" OnActivityBackdated="HMBackdateActivity_ActivityBackdated" />
