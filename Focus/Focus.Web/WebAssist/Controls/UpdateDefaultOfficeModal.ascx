﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateDefaultOfficeModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.UpdateDefaultOfficeModal" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="UpdateDefaultOfficeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="UpdateDefaultOfficeModalPopup" runat="server" BehaviorID="UpdateDefaultOfficeModal"
												TargetControlID="UpdateDefaultOfficeModalDummyTarget"
												PopupControlID="UpdateDefaultOfficeModalPanel"
												PopupDragHandleControlID="UpdateDefaultOfficeModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="UpdateDefaultOfficeModalPanel" runat="server" CssClass="modal" style="display: none;">
	<asp:Panel runat="server" ClientIDMode="Static" ID="UpdateDefaultOfficeModalPanelHeader">
	  <div style="width: 480px">
	    <h1><asp:label runat="server" ID="HeaderLabel" /></h1>
    </div>
	</asp:Panel>
	<div style="width: 510px">
	  <asp:ListView ID="UpdateOfficeList" runat="server" ItemPlaceholderID="UpdateOfficePlaceHolder"  OnItemDataBound="UpdateOfficeList_OnItemDataBound">
		  <LayoutTemplate>
		    <table>
		      <asp:PlaceHolder ID="UpdateOfficePlaceHolder" runat="server" />
		    </table>
		  </LayoutTemplate>
	    <ItemTemplate>
	      <tr>
	        <td colspan="2" style="font-weight: bold">
	          <br />
	          <asp:Label runat="server" ID="UpdateOfficeHeaderLabel"></asp:Label>
	        </td>
	      </tr>
        <tr>
          <td style="padding-right: 16px"><asp:Label runat="server" ID="CurrentDefaultOfficeHeaderLabel"></asp:Label></td>
          <td><asp:Label runat="server" ID="CurrentDefaultOfficeNameLabel"></asp:Label></td>
        </tr>
        <tr style="vertical-align: top">
          <td><asp:Label runat="server" ID="NewDefaultOfficeHeaderLabel"></asp:Label></td>
          <td>
            <asp:HiddenField runat="server" ID="DefaultTypeValue"/>
            <asp:DropDownList runat="server" ID="NewDefaultOfficeDropdown"/>
            <br />
          </td>
        </tr>
	    </ItemTemplate>
	  </asp:ListView>
    <p>
      <asp:CustomValidator ID="NewOfficesValidator" runat="server" SetFocusOnError="true" ValidationGroup="UpdateDefaultOffice" ClientValidationFunction="UpdateDefaultOfficeModal_ValidateOffices" CssClass="error" />
    </p>
    <div style="width: 100%; text-align: right">
		  <asp:Button ID="OkButton" runat="server" SkinID="Button1" OnClick="OkButton_Click" ValidationGroup="UpdateDefaultOffice" />
		  <asp:Button ID="CloseButton" runat="server" SkinID="Button1" />
    </div>
	</div>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('<%=UpdateDefaultOfficeModalPopup.BehaviorID %>').hide(); return false;" /></div>
</asp:Panel>

<uc:ConfirmationModal ID="ConfirmationModal" runat="server" Width="300px" ClientIDMode="AutoID" />

<script type="text/javascript">
  function UpdateDefaultOfficeModal_ValidateOffices(source, arguments) {
    var selectedOfficeCount = 0;

    var dropdowns = $("*[data-validationgroup='" + source.validationGroup + "']");
    dropdowns.each(function (index, obj) {
      if ($(obj).val().length > 0)
        selectedOfficeCount++;
    });

    arguments.IsValid = (selectedOfficeCount > 0);
  }
</script>