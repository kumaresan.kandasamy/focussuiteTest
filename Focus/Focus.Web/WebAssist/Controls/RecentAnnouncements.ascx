﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecentAnnouncements.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.RecentAnnouncements" %>
<%@ Import Namespace="Focus.Core.Views" %>

<asp:ListView ID="AnnouncementList" runat="server" DataKeyNames="Id,CreatedOn,CreatedByFirstName,CreatedByLastName,Text,Audience" DataSourceID="AnnouncementsDataSource" ItemPlaceholderID="SearchResultPlaceHolder" OnItemDataBound="AnnouncementList_ItemDataBound" OnSorting="AnnouncementList_Sorting" OnLayoutCreated="AnnouncementList_LayoutCreated">
	<LayoutTemplate>
		<table style="width:100%;" class="table">
		<thead>
		<tr>
			<th style="white-space: nowrap">
				<asp:Literal runat="server" ID="DatePostedHeader" />
        <div class="orderingcontrols">
          <asp:ImageButton ID="DatePostedSortAscButton" runat="server" CommandName="Sort" CommandArgument="CreatedOn asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
          <asp:ImageButton ID="DatePostedSortDescButton" runat="server" CommandName="Sort" CommandArgument="CreatedOn desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
        </div>
			</th>
			<th style="white-space: nowrap">
				<asp:Literal runat="server" ID="PostedByHeader" />
        <div class="orderingcontrols">
          <asp:ImageButton ID="PostedBySortAscButton" runat="server" CommandName="Sort" CommandArgument="CreatedByFirstName asc,CreatedByLastName asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
          <asp:ImageButton ID="PostedBySortDescButton" runat="server" CommandName="Sort" CommandArgument="CreatedByFirstName desc,CreatedByLastName desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
        </div>
			</th>
			<th style="white-space: nowrap">
				<asp:Literal runat="server" ID="AnnouncementHeader" />
        <div class="orderingcontrols">
          <asp:ImageButton ID="AnnouncementSortAscButton" runat="server" CommandName="Sort" CommandArgument="Text asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
          <asp:ImageButton ID="AnnouncementSortDescButton" runat="server" CommandName="Sort" CommandArgument="Text desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
        </div>
			</th>
			<th style="white-space: nowrap">
				<asp:Literal runat="server" ID="DisplayedToHeader" />
        <div class="orderingcontrols">
          <asp:ImageButton ID="DisplayedToSortAscButton" runat="server" CommandName="Sort" CommandArgument="Audience asc" CssClass="AscButton" AlternateText="Sort ascending" Width="15" Height="9"/>
          <asp:ImageButton ID="DisplayedToSortDescButton" runat="server" CommandName="Sort" CommandArgument="Audience desc" CssClass="DescButton" AlternateText="Sort descending" Width="15" Height="9"/>
        </div>
			</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
			<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
		</tbody>
		</table>
	</LayoutTemplate>
	<ItemTemplate>
		<tr>
			<td>
				<asp:Literal ID="DatePostedLiteral" runat="server" />
			</td>
			<td><asp:Literal ID="PostedByLiteral" runat="server" /></td>
			<td>
				<span id="AnnouncementLiteralContainer_<%#((MessageView)Container.DataItem).Id%>">
					<asp:Literal ID="AnnouncementLiteral" runat="server" />
					<asp:HyperLink runat="server" ID="ShowMoreLink" Visible="False" Style="font-weight: bold" NavigateUrl="javascript:void(0);"></asp:HyperLink>
				</span>
				<span id="FullAnnouncementLiteralContainer_<%#((MessageView)Container.DataItem).Id%>" style="display:none">
					<asp:Literal runat="server" ID="FullAnnouncementLiteral"></asp:Literal>
					<asp:HyperLink runat="server" ID="HideLink" Style="font-weight: bold" NavigateUrl="javascript:void(0);"></asp:HyperLink>
				</span>
			</td>
			<td style="max-width: 640px"><asp:Literal ID="DisplayedToLiteral" runat="server" /></td>
			<td><asp:Button runat="server" ID="RemoveButton" CssClass="button2" OnCommand="RemoveButton_OnCommand"/></td>
		</tr>
	</ItemTemplate>
	<EmptyDataTemplate>
		<%=HtmlLocalise("NoRecentAnnouncements", "No recent announcements found")%>
	</EmptyDataTemplate>
</asp:ListView>
<asp:ObjectDataSource ID="AnnouncementsDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.RecentAnnouncements" EnablePaging="False" 
											SelectMethod="GetAnnouncements" SelectCountMethod="GetAnnouncementCount" SortParameterName="orderBy">
</asp:ObjectDataSource>
<script language="javascript" type="text/javascript">
	function RecentAnnouncements_ShowFullMessage(id) {
		$('#FullAnnouncementLiteralContainer_' + id).show();
		$('#AnnouncementLiteralContainer_' + id).hide();
	}
	function RecentAnnouncements_HideFullMessage(id) {
		$('#FullAnnouncementLiteralContainer_' + id).hide();
		$('#AnnouncementLiteralContainer_' + id).show();
	}		
</script>