﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core.Models.Assist;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class ComparePostingToResumeModal : UserControlBase
	{
		private ComparePostingToResumeModalModel _model
		{
			get { return GetViewStateValue<ComparePostingToResumeModalModel>("ComparePostingToResumeModal:Model"); }
			set { SetViewStateValue("ComparePostingToResumeModal:Model", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
                SetModalTitle();
			}
		}

		/// <summary>
		/// Shows the specified comparison.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="doLiveSearch">if set to <c>true</c> [do live search].</param>
		public void Show(string lensPostingId, bool doLiveSearch)
		{
			GetModel(lensPostingId, doLiveSearch);

			DisplayModel(!doLiveSearch);

			ComparePostingToResumeModalPopup.Show();
		}

		#region Events

		/// <summary>
		/// Handles the SelectedIndexChanged event of the ResumesDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ResumesDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			SetSelectedResumesDetails();
			ComparePostingToResumeModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ComparePostingToResumeModalPopup.Hide();

			var matchIgnored = false;
			
			if (_model.IsNotNull() && _model.MatchedPersons.IsNotNullOrEmpty())
			{
				matchIgnored = _model.MatchedPersons.Any(x => x.DoNotShow);
			}

			if (ModalClosed != null)
				ModalClosed(this, new ModalClosedEventArgs(matchIgnored));
		}

		/// <summary>
		/// Handles the Clicked event of the ContactEmployerButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ContactEmployerButton_Clicked(object sender, EventArgs e)
		{
			ComparePostingToResumeModalPopup.Hide();

			var personId = GetSelectedPersonId();

			Response.Redirect(UrlBuilder.PostingEmployerContact(_model.PostingId, personId));
		}

		/// <summary>
		/// Handles the CheckChanged event of the DoNotShowCheckBox control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DoNotShowCheckBox_CheckChanged(object sender, EventArgs e)
		{
			var personId = GetSelectedPersonId();

			if (DoNotShowCheckBox.Checked)
			{
				ServiceClientLocator.CandidateClient(App).IgnorePersonPostingMatch(_model.PostingId, personId);
				_model.MatchedPersons.Single(x => x.Id == personId).DoNotShow = true;
			}

			else
			{
				ServiceClientLocator.CandidateClient(App).AllowPersonPostingMatch(_model.PostingId, personId);
				_model.MatchedPersons.Single(x => x.Id == personId).DoNotShow = false;
			}

			ComparePostingToResumeModalPopup.Show();
		}

		#endregion

		/// <summary>
		/// Displays the model.
		/// </summary>
		/// <param name="showDoNotDisplayMatchAgain">if set to <c>true</c> [show do not display match again].</param>
		private void DisplayModel(bool showDoNotDisplayMatchAgain)
		{
			ResetModal();

			if (_model.IsNotNull())
			{
				SetModalTitle();
				
				if(_model.PostingHtml.IsNotNull())
					App.SetSessionValue("Career:DisplayPosting", _model.PostingHtml.AddAltTagWithValueForImg("Posting images"));

				PostingDetail.Attributes["src"] = GetRouteUrl("AssistJobDisplay", new { jobid = _model.LensPostingId });

				if (_model.MatchedPersons.IsNotNullOrEmpty())
					BindResumesDropDown();
				else
					DisplayNoMatchesMessage();
			}

			DoNotShowCheckBox.Visible = showDoNotDisplayMatchAgain;
		}

		/// <summary>
		/// Displays the no matches message.
		/// </summary>
		private void DisplayNoMatchesMessage()
		{
			ResumesDropDown.Visible = false;
			MatchScoreImage.Visible = false;
			ResumeResultPanel.Visible = false;
			EmpytyResultPanel.Visible = true;
		}

		/// <summary>
		/// Resets the modal control visibility.
		/// </summary>
		private void ResetModal()
		{
			ResumesDropDown.Visible = true;
			MatchScoreImage.Visible = true;
			ResumeResultPanel.Visible = true;
			EmpytyResultPanel.Visible = false;
			DoNotShowCheckBox.Visible = true;
		}

		/// <summary>
		/// Sets the modal title.
		/// </summary>
		private void SetModalTitle()
		{
			if (_model.IsNotNull())
			{
				var jobTitle = (_model.PostingJobTitle.IsNotNullOrEmpty()) ? _model.PostingJobTitle : CodeLocalise("JobTitle.Default", "Posting");

				ModalTitleLiteral.Text =  (_model.MatchedPersons.Count == 1) ? CodeLocalise("ModalTitleLiteral.Text.Singular", "{0} matches 1 resume", jobTitle)
																																			: CodeLocalise("ModalTitleLiteral.Text.Plural", "{0} matches {1} resumes", jobTitle, _model.MatchedPersons.Count);
			}
			else
			{
				ModalTitleLiteral.Text = "job matches 0 resumes";
			}
		}

		/// <summary>
		/// Binds the resumes drop down.
		/// </summary>
		private void BindResumesDropDown()
		{
			if (_model.IsNotNull() && _model.MatchedPersons.IsNotNullOrEmpty())
			{
				ResumesDropDown.DataSource = _model.MatchedPersons;
				ResumesDropDown.DataTextField = "Name";
				ResumesDropDown.DataValueField = "Id";
				ResumesDropDown.DataBind();

				SetSelectedResumesDetails();
			}
		}

		/// <summary>
		/// Sets the selected resumes details.
		/// </summary>
		private void SetSelectedResumesDetails()
		{
			var personId = GetSelectedPersonId();

			if (personId == 0)
				return;

			if (_model.IsNotNull() && _model.MatchedPersons.IsNotNullOrEmpty())
			{
				var matchedPerson = _model.MatchedPersons.Single(x => x.Id == personId);

				BindScore(MatchScoreImage, matchedPerson.MatchScore);

				DoNotShowCheckBox.Checked = matchedPerson.DoNotShow;
			}

			ResumeLiteral.Text = GetPersonDefaultResume(personId);
		}

		/// <summary>
		/// Gets the model.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="doLiveSearch">if set to <c>true</c> [do live search].</param>
		private void GetModel(string lensPostingId, bool doLiveSearch)
		{
			_model = ServiceClientLocator.PostingClient(App).GetComparePostingToResumeModalModel(lensPostingId, doLiveSearch);
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = OldApp_RefactorIfFound.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString();

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();

			scoreImage.Visible = true;
		}

		/// <summary>
		/// Gets the person default resume.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		private string GetPersonDefaultResume(long personId)
		{
			return ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(personId);
		}

		/// <summary>
		/// Gets the selected person id.
		/// </summary>
		/// <returns></returns>
		private long GetSelectedPersonId()
		{
			return ResumesDropDown.SelectedValueToLong();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ContactEmployerButton.Text = CodeLocalise("ContactEmployerButton.Text", "Contact #BUSINESS#:LOWER");
			CancelButton.Text = CodeLocalise("CancelButton.Text", "Cancel");
			DoNotShowCheckBox.Text = CodeLocalise("DoNotShowCheckBox.Text", "Do not show this suggestion again");
		}

		#region Events

		/// <summary>
		/// Occurs when [modal closed].
		/// </summary>
		public event ModalClosedHandler ModalClosed;

		/// <summary>
		/// Raises the <see cref="E:ModalClosed"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.ComparePostingToResumeModal.ModalClosedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnModalClosed(ModalClosedEventArgs e)
		{
			if (ModalClosed != null)
				ModalClosed(this, e);
		}

		#endregion

		#region Delegates

		public delegate void ModalClosedHandler(object o, ModalClosedEventArgs e);

		#endregion

		#region EventArgs

		public class ModalClosedEventArgs : EventArgs
		{
			public bool DataUpdated { get; private set; }

			/// <summary>
			/// Initializes a new instance of the <see cref="ModalClosedEventArgs"/> class.
			/// </summary>
			/// <param name="dataUpdated">if set to <c>true</c> [data updated].</param>
			public ModalClosedEventArgs(bool dataUpdated = false)
			{
				DataUpdated = dataUpdated;
			}
		}

		#endregion
	}
}