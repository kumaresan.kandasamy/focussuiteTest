﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CandidatesLikeThisModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.CandidatesLikeThisModal" %>
<%@ Import Namespace="Focus.Core.Views" %>

<asp:HiddenField ID="CandidatesLikeThisModalDummyTarget" runat="server" />

<act:ModalPopupExtender ID="CandidatesLikeThisModalPopup" runat="server" ClientIDMode="Static"
											TargetControlID="CandidatesLikeThisModalDummyTarget"
											PopupControlID="CandidatesLikeThisModalPanel" 
											PopupDragHandleControlID="CandidatesLikeThisModalHeader"
											RepositionMode="RepositionOnWindowResizeAndScroll"
											BackgroundCssClass="modalBackground" />


<asp:panel ID="CandidatesLikeThisModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display:none;width:80%">
	<div style="width:100%" class="modal-header" id="CandidatesLikeThisModalHeader">
		<div class="right">
			<asp:ImageButton ID="CandidatesLikeThisModalCloseButton" runat="server" ImageUrl="<%# UrlBuilder.ButtonCloseIcon() %>" OnClick="CandidatesLikeThisModalCloseButton_Click" AlternateText="Close button"/>
		</div>
		<div class="left">
			<strong><%= HtmlLocalise("CandidatesLikeThisModal.Title", "More candidates like")%></strong>
		</div>
	</div>
		<asp:ListView ID="CandidatesLikeThisListView" DataSourceID="CandidatesLikeThisDataSource" runat="server" OnItemDataBound="CandidatesLikeThisListView_ItemDataBound" DataKeyNames="Id,Name,Status" ItemPlaceholderID="CandidatesLikeThisPlaceHolder">
		<LayoutTemplate>
			<table role="presentation" class="genericTable">
				<asp:PlaceHolder ID="CandidatesLikeThisPlaceHolder" runat="server" />
			</table>
		</LayoutTemplate>
		<ItemTemplate>
			<asp:TableRow ID="CandidatesLikeThisItemTopTableRow" CssClass="topRow" runat="server">
				<asp:TableCell ID="TableCell1" runat="server" EnableViewState="false" CssClass="column1"><span><asp:image ID="CandidatesLikeThisScoreImage" runat="server" AlternateText="Candidates Like This Score Image" /></span></asp:TableCell>
				<asp:TableCell ID="TableCell2" runat="server" EnableViewState="false" CssClass="column2">
					<span>
						<asp:LinkButton ID="CandidatesLikeThisNameLink" runat="server" />&nbsp;
						<asp:image ID="CandidatesLikeThisFlaggedImage" runat="server" Visible="false" AlternateText="Candidates Like This Flagged Image" />
						<asp:image ID="CandidatesLikeThisVeteranImage" runat="server" Visible="false" AlternateText="Candidates Like This Veteran Image" />
					</span>
				</asp:TableCell><asp:TableCell ID="TableCell3" runat="server" EnableViewState="false" CssClass="column2"><span><asp:literal ID="CandidatesLikeThisEmploymentHistoryLiteral" runat="server" /></span></asp:TableCell><asp:TableCell ID="TableCell4" runat="server" EnableViewState="false" CssClass="column2"><span><%# HtmlLocalise("CandidateList.Experience.Text", "{0} years", ((ResumeView)Container.DataItem).YearsExperience)%></span></asp:TableCell><asp:TableCell ID="TableCell5" runat="server" EnableViewState="false" CssClass="column3">
					<span>
						<asp:DropDownList runat="server" ID="CandidatesLikeThisApplicationStatusDropDownList" Visible="false" />
					</span>
				</asp:TableCell>
				</asp:TableRow>
				<asp:TableRow ID="CandidatesLikeThisItemBottomTableRow" CssClass="bottomRow" runat="server">
				<asp:TableCell ID="TableCell6" runat="server" CssClass="column2" colspan="5">
					<span>
						<table role="presentation" width="100%">
							<tr>
								<td>
									<div class="tooltip left Email">
										<asp:LinkButton ID="CandidatesLikeThisEmailButton" runat="server" OnCommand="CandidatesLikeThisRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="Email">
											<div class="tooltipImage toolTipNew" title="<%# HtmlLocalise("Email.ToolTip", "Email")%>"></div>
										</asp:LinkButton>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<asp:panel ID="CandidatesLikeThisNotePanel" runat="server" Visible="false" Width="100%" >
										<br/>
										<table role="presentation" width="100%">
											<tr>
												<td><asp:TextBox ID="CandidatesLikeThisNotesTextBox" Width="99%" MaxLength="399" runat="server" /></td>
												<td width="20px" align="center">
														<asp:LinkButton ID="CandidatesLikeThisSaveNoteLinkButton" runat="server" OnCommand="CandidatesLikeThisRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="SaveNote">
															<img src="<%= UrlBuilder.SaveIcon() %>" alt="<%= HtmlLocalise("SaveNote.Text", "Save note") %>" width="16px" />
														</asp:LinkButton>
												</td>
												<td width="20px">
													<asp:LinkButton ID="CandidatesLikeThisCloseNoteLinkButton" runat="server" OnCommand="CandidatesLikeThisRowAction_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="CloseNote">
														<img src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" />
													</asp:LinkButton>
												</td>
											</tr>
										</table>
										<br/>
									</asp:panel>
								</td>
							</tr>
						</table>
					</span>
				</asp:TableCell>
			</asp:TableRow>
			<tr id="ResumeViewRow">
				<td colspan="5">
					<div class="ResumeDiv" style="display:none; max-height:250px; overflow:auto; overflow-x:hidden; margin-bottom:10px;"></div>
				</td>
			</tr>
		</ItemTemplate>
		<EmptyDataTemplate>
			<%= HtmlLocalise("CandidatesLikeThisNoResults.Text", "No matching candidates can be found.") %>
		</EmptyDataTemplate>
	</asp:ListView>
	<asp:ObjectDataSource ID="CandidatesLikeThisDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.CandidatesLikeThisModal" EnablePaging="true" SelectMethod="GetCandidatesLikeThis" 
												SelectCountMethod="GetCandidatesLikeThisCount" SortParameterName="orderBy" OnSelected="CandidatesLikeThisDataSource_Selected" />
</asp:panel>

<script type="text/javascript">
	
	function ResumeLinkButtonClicked(lnk, personId) {
		var resumeDiv = $("#" + lnk.id).parents('tr').nextUntil(".topRow", "#ResumeViewRow").find(".ResumeDiv");

		if(resumeDiv.is(':hidden')) {
			$(".ResumeDiv").hide();
		}

		if (resumeDiv.html() == "") {
			GetResumeText(resumeDiv, personId);
			resumeDiv.html("<%= HtmlLocalise("FetchingResume.Text", "Fetching Resume...") %>");
		}

		resumeDiv.toggle();

		return false;
	}

	function GetResumeText(resumeDiv, personId) {
		$.ajax({
			type: "POST",
			contentType: "application/json; charset=utf-8",
			url: "<%= UrlBuilder.AjaxService() %>/GetResumeText",
			dataType: "json",
			data: '{"personId":' + personId + '}',
			success: function (result) {
				resumeDiv.html(result.d);
			},
			error: function () {
				resumeDiv.html("<%= HtmlLocalise("NoResumeFound.Text", "No Resume Found.") %>");
			}
		});
	}
	
</script>

