﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegisterJobSeeker.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.RegisterJobSeeker"  %>
<%@ Import Namespace="Focus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>

<asp:HiddenField ID="RegisterModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="RegisterModalPopup" runat="server"
												BehaviorID="RegisterModal"
												TargetControlID="RegisterModalDummyTarget"
												PopupControlID="RegisterModalPanel"
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:panel id="RegisterModalPanel" runat="server" class="modal" width="900" style="display:none;">
		<table role="presentation">
			<tr>
				<td>
					<asp:Panel ID="panRegisterStep1Heading" runat="server">
						<h2>
							<asp:Label runat="server" ID="HeadingLabel"><%=HtmlLocalise("HeadingLabelNew.Text.NoEdit", "Create new #CANDIDATETYPE#:LOWER account")%></asp:Label>
						</h2>
					</asp:Panel>
					<asp:Panel ID="panRegisterStep2Heading" runat="server" Visible="false">
						<h2>
							<%= HtmlLocalise("Heading1.Label", "Terms and Conditions")%>
						</h2>
					</asp:Panel>
				</td>
				<td class="steppedProcessSubhead" id="StepHeading" runat="server">
					<asp:Panel ID="panRegisterStep1" runat="server">
						<%= HtmlLocalise("steppedProcessSubhead.Label", "<strong>Step 1</strong> of 2")%>
					</asp:Panel>
					<asp:Panel ID="panRegisterStep2" runat="server" Visible="false">
						<%= HtmlLocalise("steppedProcessSubhead.Label", "<strong>Step 2</strong> of 2")%>
					</asp:Panel>
				</td>
			</tr>
		</table>
		<asp:Panel ID="RegisterStep1Panel" runat="server" ScrollBars="Auto" Style="height: 380px;">
			<table role="presentation">
				<tr>
					<td class="instructionalText">
						<%= HtmlRequiredLabel("RedAsterik.Label", "")%>
						<%= HtmlLocalise("instructionalText.Label", "required fields")%>
					</td>
				</tr>
			</table>
			<table style="width:100%;" class="modalRegistrationTable" role="presentation">
				<tr>
					<td colspan="3" class="tableSectionDivider">Account Details</td>
				</tr>
				<asp:PlaceHolder id="EducationPlaceHolder1" runat="server">
					<tr>
						<td>
							<%= HtmlRequiredLabel(UserNameTextbox, "UserName", "User name")%>
						</td>
						<td style="width:300px;" colspan="2">
							<asp:TextBox ID="UserNameTextbox" runat="server" ClientIDMode="Static" Width="300px" TabIndex="1" MaxLength="100" AutoCompleteType="Disabled"/>
							<asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserNameTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						</td>
					</tr>	
				</asp:PlaceHolder>
				<tr id="ScreenNameRow" runat="server">
					<td class="column1">
						<%= HtmlRequiredLabel(ScreenNameTextbox, "ScreenName", "Screen name")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="ScreenNameTextbox" runat="server" ClientIDMode="Static" Width="300px" TabIndex="1" MaxLength="20" AutoCompleteType="Disabled" />
						<asp:RequiredFieldValidator ID="ScreenNameRequired" runat="server" ControlToValidate="ScreenNameTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
					</td>
					<td class="column3"></td>
				</tr>
				<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(EmailAddressTextbox, "EmailAddress", "Email address")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="EmailAddressTextbox" runat="server" ClientIDMode="Static" Width="300px" TabIndex="2" AutoCompleteType="Disabled" onchange="ClearUserNameInUserMsg()"/>
						<br />
						<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						<asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="EmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						<asp:Label runat="server" ID="UsernameInUseErrorMessage" CssClass="error" ClientIDMode="Static" />
					</td>
					<td class="instructionalText column3">
					  <asp:PlaceHolder runat="server" ID="RegistrationEmailHelpText">
						<%= HtmlLocalise("Email.HelpText", "No email account? Use a free service:")%><br />
						<a href="http://webmail.aol.com" target="_blank">
							<%= HtmlLocalise("AOLMail", "AOLMail")%></a>, <a href="http://mail.google.com" target="_blank">
								<%= HtmlLocalise("Gmail", "Gmail")%></a>, <a href="https://login.live.com/" target="_blank">
									<%= HtmlLocalise("Windowslive", "Windows Live")%></a>, <a href="http://mail.yahoo.com"
										target="_blank">
										<%= HtmlLocalise("Yahoo", "Yahoo!")%></a>
            </asp:PlaceHolder>
					</td>
				</tr>
				<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(ConfirmEmailAddressTextbox, "EmailAddressConfirmation", "Re-enter email address")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="ConfirmEmailAddressTextbox" runat="server" ClientIDMode="Static"
							Width="300px" TabIndex="3" AutoCompleteType="Disabled" onchange="ClearUserNameInUserMsg()"/>
							<br />
						<asp:RequiredFieldValidator ID="ConfirmEmailAddressRequired" runat="server" ControlToValidate="ConfirmEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						<asp:CompareValidator ID="ConfirmEmailAddressCompare" runat="server" ControlToValidate="ConfirmEmailAddressTextbox" ControlToCompare="EmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
					</td>
					<td class="instructionalText column3">
					</td>
				</tr>
				<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(NewPasswordTextBox, "NewPassword", "Password")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="NewPasswordTextBox" runat="server" ClientIDMode="Static" Width="300px" TextMode="Password" TabIndex="4" MaxLength="20" autocomplete="off" /><br />
						<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						<asp:RegularExpressionValidator ID="NewPasswordRegEx" runat="server" ControlToValidate="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
					</td>
					<td class="instructionalText column3">
						<%= HtmlLocalise("PasswordInstructions", "6-20 characters; must include at least one number; must not contain spaces; password is case-sensitive.")%>
					</td>
				</tr>
				<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(ConfirmNewPasswordTextBox, "ConfirmNewPassword", "Re-enter password")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="ConfirmNewPasswordTextBox" runat="server" ClientIDMode="Static" Width="300px" TextMode="Password" TabIndex="5" MaxLength="20" autocomplete="off" />
						<br/>
						<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						<asp:CompareValidator ID="ConfirmNewPasswordCompare" runat="server" ControlToValidate="ConfirmNewPasswordTextBox" ControlToCompare="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
					</td>
					<td class="instructionalText column3">
					</td>
				</tr>
				<asp:PlaceHolder runat="server" ID="WorkforceSSNPlaceHolder" visible="true">
					<tr id="SSNRow" runat="server">
						<td style="vertical-align: top;" class="column1">
						  <%= HtmlLabel(SocialSecurityNumberPart1TextBox, "SocialSecurityNumber", "Social Security Number (SSN)")%>
						</td>
						<td style="vertical-align: top;" class="column2">
							<asp:TextBox ID="SocialSecurityNumberPart1TextBox" runat="server" ClientIDMode="Static" title="SsnTextBox1" TabIndex="6" MaxLength="3" size="3" Width="25" onKeyup="autotab(this, 'SocialSecurityNumberPart2TextBox')" TextMode="Password" autocomplete="off" />
							-
							<focus:LocalisedLabel runat="server" ID="SocialSecurityNumberPart2TextBoxLabel" AssociatedControlID="SocialSecurityNumberPart2TextBox" LocalisationKey="SocialSecurityNumber" DefaultText="Social Security Number (SSN) Part 2" CssClass="sr-only"/>
							<asp:TextBox ID="SocialSecurityNumberPart2TextBox" runat="server" ClientIDMode="Static" title="SsnTextBox2" TabIndex="7" MaxLength="2" size="2" Width="15" onKeyup="autotab(this, 'SocialSecurityNumberPart3TextBox')" TextMode="Password" autocomplete="off"/>
							-
							<focus:LocalisedLabel runat="server" ID="SocialSecurityNumberPart3TextBoxLabel" AssociatedControlID="SocialSecurityNumberPart3TextBox" LocalisationKey="SocialSecurityNumber" DefaultText="Social Security Number (SSN) Part 3" CssClass="sr-only"/>
							<asp:TextBox ID="SocialSecurityNumberPart3TextBox" runat="server" ClientIDMode="Static" title="SsnTextBox3" TabIndex="8" MaxLength="4" size="4" Width="30" TextMode="Password" autocomplete="off" />
							<br/>
						    <asp:Label runat="server" ID="SsnInUseErrorMessage" CssClass="error" ClientIDMode="Static" />
							<asp:CustomValidator ID="SocialSecurityNumberValidator" runat="server" ClientValidationFunction="RegisterJobSeeker_ValidateSsn" ValidationGroup="RegisterStep1" ValidateEmptyText="True" CssClass="error" SetFocusOnError="true" Display="Dynamic" />
						</td>
						<td class="instructionalText column3" rowspan="2">
							<%= HtmlLocalise("SocialSecurityNumber.HelpText", "Your #CANDIDATETYPE#:LOWER's SSN will be stored securely. The SSN is used only to match the #CANDIDATETYPE#:LOWER's account to existing accounts and consolidate their records, when appropriate. This may include previous accounts for case-management, Unemployment Insurance, etc.")%>
						</td>
					</tr>
					<tr id="SSN2Row" runat="server">
						<td class="column1">
						  <%= HtmlLabel(ConfirmSocialSecurityNumberPart1TextBox, "ConfirmSocialSecurityNumber", "Re-enter SSN")%>
						</td>
						<td class="column2" colspan="2">
							<asp:TextBox ID="ConfirmSocialSecurityNumberPart1TextBox" runat="server" ClientIDMode="Static" TabIndex="9" MaxLength="3" size="3" Width="25" onKeyup="autotab(this, 'ConfirmSocialSecurityNumberPart2TextBox')" TextMode="Password" autocomplete="off"/>
							-
							<focus:LocalisedLabel runat="server" ID="ConfirmSocialSecurityNumberPart2TextBoxLabel" AssociatedControlID="ConfirmSocialSecurityNumberPart2TextBox" LocalisationKey="ConfirmSocialSecurityNumber" DefaultText="Re-enter SSN Part 2" CssClass="sr-only"/>
							<asp:TextBox ID="ConfirmSocialSecurityNumberPart2TextBox" runat="server" ClientIDMode="Static" TabIndex="10" MaxLength="2" size="2" Width="15" onKeyup="autotab(this, 'ConfirmSocialSecurityNumberPart3TextBox')" TextMode="Password" autocomplete="off" />
							-
							<focus:LocalisedLabel runat="server" ID="ConfirmSocialSecurityNumberPart3TextBoxLabel" AssociatedControlID="ConfirmSocialSecurityNumberPart3TextBox" LocalisationKey="ConfirmSocialSecurityNumber" DefaultText="Re-enter SSN Part 3" CssClass="sr-only"/>
							<asp:TextBox ID="ConfirmSocialSecurityNumberPart3TextBox" runat="server" ClientIDMode="Static" TabIndex="11" MaxLength="4" size="4" Width="30" TextMode="Password" autocomplete="off"/>
							<br/>
							<asp:CustomValidator ID="ConfirmSocialSecurityNumberValidator" runat="server" ClientValidationFunction="RegisterJobSeeker_ValidateConfirmSsn" ValidationGroup="RegisterStep1" ValidateEmptyText="True" CssClass="error" SetFocusOnError="true" Display="Dynamic" />
						</td>
					</tr>
				</asp:PlaceHolder>
				<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(SecurityQuestionDropDown, "SecurityQuestion", "Security question")%>
					</td>
					<td class="column2">
						<div style="position: relative;">
							<asp:DropDownList ID="SecurityQuestionDropDown" runat="server" ClientIDMode="Static"
								TabIndex="12" Width="308" />
						</div>
						<div id="SecurityQuestionDiv" style="display: none; padding-top: 8px;">
							<focus:LocalisedLabel runat="server" ID="SecurityQuestionTextBoxLabel" AssociatedControlID="SecurityQuestionTextBox" LocalisationKey="SecurityQuestion" DefaultText="Security question" CssClass="sr-only"/>
							<asp:TextBox ID="SecurityQuestionTextBox" runat="server" ClientIDMode="Static" Width="300px" TabIndex="13" MaxLength="100" />
							<br/>
							<asp:CustomValidator ID="SecurityQuestionCustomValidator" runat="server" ControlToValidate="SecurityQuestionTextBox" ClientValidationFunction="validateOwnSecurityQuestion" ValidationGroup="RegisterStep1" ValidateEmptyText="True" CssClass="error" SetFocusOnError="true" Display="Dynamic" />
						</div>
						<asp:RequiredFieldValidator ID="SecurityQuestionRequired" runat="server" ControlToValidate="SecurityQuestionDropDown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
					</td>
					<td class="instructionalText column3">
					</td>
				</tr>
				<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(SecurityAnswerTextBox, "SecurityAnswer", "Security answer")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="SecurityAnswerTextBox" runat="server" ClientIDMode="Static" Width="300px"
							TabIndex="14" MaxLength="30" />
						<br/>
						<asp:RequiredFieldValidator ID="SecurityAnswerRequired" runat="server" ControlToValidate="SecurityAnswerTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
					</td>
					<td class="instructionalText column3">
						<%= HtmlLocalise("SecurityAnswerInstructions", "Answer is case-sensitive.")%>
					</td>
				</tr>
        <asp:PlaceHolder runat="server" ID="SeekerAccountTypePlaceholder" Visible="false">
					<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(SeekerAccountTypeDropDownList, "SeekerAccountType.Label", "Account Type")%>
					</td>
					<td class="column2">
						<div style="position: relative;">
							<asp:DropDownList ID="SeekerAccountTypeDropDownList" runat="server" ClientIDMode="Static" TabIndex="15" Width="308px" />
						</div>
            			<asp:RequiredFieldValidator ID="SeekerAccountTypeRequired" runat="server" ControlToValidate="SeekerAccountTypeDropDownList"
							SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
					</td>
				</tr>
        </asp:PlaceHolder>		
				<asp:PlaceHolder id="EducationInformationPlaceHolder2" runat="server" Visible="false">
				  <asp:PlaceHolder runat="server" ID="ProgramOfStudyPlaceHolder">
					  <tr>
						  <td class="column1">
							  <%= HtmlRequiredLabel(DepartmentDropDownList, "EducationInformationDepartment", "Program of Study")%>
						  </td>
						  <td class="column2" colspan="2">
							  <asp:DropDownList ID="DepartmentDropDownList" runat="server" ClientIDMode="Static" Width="308" EnableViewState="true" TabIndex="15"/>
							  <br/>
							  <asp:RequiredFieldValidator ID="DepartmentRequired" runat="server" ControlToValidate="DepartmentDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						  </td>
					  </tr>
          </asp:PlaceHolder>
					<tr>
						<td class="column1">
							<%= HtmlRequiredLabel(DegreeDropDownList, "EducationInformationDegree", "Degree")%>
						</td>
						<td class="column2" colspan="2">
						  <asp:DropDownList runat="server" ID="AllDegreeDropDownList" ClientIDMode="Static" Width="308" TabIndex="16" EnableViewState="true" Visible="False"/>
              <asp:PlaceHolder runat="server" ID="DegreeDropCascadingDropDownPlaceHolder">
  							<focus:AjaxDropdownList ID="DegreeDropDownList" runat="server" ClientIDMode="Static" Width="308" TabIndex="16" EnableViewState="true"/>
							  <act:CascadingDropDown ID="DegreeDropDownListCascadingDropDown" runat="server" BehaviorID="DegreeDropDownListCascadingDropDownBehavior" TargetControlID="DegreeDropDownList" ParentControlID="DepartmentDropDownList" 
															ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetDegreeEducationLevelsByProgramArea" Category="DegreesWithUndeclared"  />
              </asp:PlaceHolder>
							<br/>
							<asp:RequiredFieldValidator ID="DegreeRequired" runat="server" ControlToValidate="DegreeDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						</td>
					</tr>
					<tr>
						<td class="column1">
							<%= HtmlRequiredLabel(EnrollmentStatusDropDownList, "EducationInformationEnrollmentStatus", "Enrollment status")%>
						</td>
						<td class="column2" colspan="2">
							<asp:DropDownList ID="EnrollmentStatusDropDownList" runat="server" ClientIDMode="Static" Width="308" TabIndex="17" EnableViewState="true"/>
							<br/>
							<asp:RequiredFieldValidator ID="EnrollmentStatusRequired" runat="server" ControlToValidate="EnrollmentStatusDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						</td>
					</tr>
          <asp:PlaceHolder runat="server" ID="CampusPlaceHolder">
					  <tr>
						  <td class="column1">
							  <%= HtmlRequiredLabel(CampusDropDownList, "EducationInformationCampus", "Campus location")%>
						  </td>
						  <td class="column2" colspan="2">
							  <asp:DropDownList ID="CampusDropDownList" runat="server" ClientIDMode="Static" Width="308" TabIndex="17" EnableViewState="true"/>
							  <br/>
							  <asp:RequiredFieldValidator ID="CampusRequired" runat="server" ControlToValidate="CampusDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
						  </td>
					  </tr>
          </asp:PlaceHolder>
				</asp:PlaceHolder>
				<asp:PlaceHolder ID="EducationAdditionalInfoTitleRow" runat="server" Visible="False">
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3" class="tableSectionDivider"><%= HtmlLocalise("EducationAdditionalInfoTitleRow.Label", "Personal Details") %></td>
					</tr>
				</asp:PlaceHolder>
				<tr>
					<td>
						<%= HtmlRequiredLabel(AdditionalInformationFirstNameTextBox, "Firstname.Label", "First name")%>
					</td>
					<td colspan="2">
						<table role="presentation">
							<tr>
								<td>
									<asp:TextBox ID="AdditionalInformationFirstNameTextBox" runat="server" ClientIDMode="Static" Width="180px" TabIndex="18" MaxLength="20" />
									<br/>
                  <asp:RequiredFieldValidator runat="server" ID="AdditionalInformationFirstNameValidator" ControlToValidate="AdditionalInformationFirstNameTextBox" Display="Dynamic" CssClass="error" SetFocusOnError="True" ValidationGroup="RegisterStep1Additional"></asp:RequiredFieldValidator>
								</td>
								<td class="secondaryFieldLabel">
	                <%= HtmlLabel(AdditionalInformationMiddleInitialTextBox, "Middleinitial.Label", "Middle initial")%>
 								</td>
								<td>
									<asp:TextBox ID="AdditionalInformationMiddleInitialTextBox" MaxLength="1" runat="server" TabIndex="19" ClientIDMode="Static" Width="30px" />
								</td>
								<td class="secondaryFieldLabel">
									<%= HtmlRequiredLabel(AdditionalInformationLastNameTextBox, "Lastname.Label", "Last name")%>
								</td>
								<td>
									<asp:TextBox ID="AdditionalInformationLastNameTextBox" runat="server" ClientIDMode="Static" TabIndex="20" Width="180px" MaxLength="20" />
									<br/>
									<asp:RequiredFieldValidator runat="server" ID="AdditionalInformationLastNameValidator" ControlToValidate="AdditionalInformationLastNameTextBox" Display="Dynamic" CssClass="error" SetFocusOnError="True" ValidationGroup="RegisterStep1Additional"></asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr runat="server" id="AdditionalInformationPhoneNumberRow" ClientIDMode="Static">
					<td class="column1">
					  <%= HtmlRequiredLabel(AdditionalInformationPhoneNumberTextBox, "AdditionalInformationPhoneNumber", "Phone number")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="AdditionalInformationPhoneNumberTextBox" runat="server" ClientIDMode="Static" TabIndex="21" Width="300" />
						<ajaxtoolkit:MaskedEditExtender ID="meAdditionalInformationPhoneNumber" runat="server" Mask="(999) 999-9999" MaskType="Number" TargetControlID="AdditionalInformationPhoneNumberTextBox" PromptCharacter="_" ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" />
						<asp:CustomValidator ID="PhoneNumberValidator" runat="server" ControlToValidate="AdditionalInformationPhoneNumberTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1Additional" ClientValidationFunction="ValidatePhone" ValidateEmptyText="true" />
					</td>
					<td class="column3"></td>
				</tr>
				<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(AdditionalInformationZipTextBox, "AdditionalInformationZIPcode.Label", "ZIP code")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="AdditionalInformationZipTextBox" runat="server" ClientIDMode="Static" Width="300" TabIndex="22" MaxLength="9" />
						<ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="AdditionalInformationZipTextBox" FilterType="Numbers" />
						<br/>
						<asp:RequiredFieldValidator runat="server" ID="AdditionalInformationZipValidator" ControlToValidate="AdditionalInformationZipTextBox" SetFocusOnError="True" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1Additional"></asp:RequiredFieldValidator>
					</td>
					<td class="column3"></td>
				</tr>
				<tr>
					<td class="column1">
						<%= HtmlRequiredLabel(AdditionalInformationAddressTextBox, "AdditionalInformationAddress", "Address")%>
					</td>
					<td class="column2">
						<asp:TextBox ID="AdditionalInformationAddressTextBox" runat="server" ClientIDMode="Static" Width="300" TabIndex="23" MaxLength="200" />
						<br/>
						<asp:RequiredFieldValidator runat="server" ID="AdditionalInformationAddressValidator" ControlToValidate="AdditionalInformationAddressTextBox" SetFocusOnError="True" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1Additional"></asp:RequiredFieldValidator>
					</td>
					<td class="column3"></td>
				</tr>
				<tr>
  				<td class="column1"><%= HtmlRequiredLabel(AdditionalInformationCityTextBox, "AdditionalInformationCity", "City")%></td>
					<td colspan="2">
						<table role="presentation">
							<tr>
								<td>
									<asp:TextBox ID="AdditionalInformationCityTextBox" runat="server" ClientIDMode="Static" TabIndex="24" Width="180px" MaxLength="100" />
									<br/>
									<asp:RequiredFieldValidator runat="server" ID="AdditionalInformationCityValidator" ControlToValidate="AdditionalInformationCityTextBox" SetFocusOnError="True" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1Additional"></asp:RequiredFieldValidator>
								</td>
								<td class="secondaryFieldLabel"><%= HtmlRequiredLabel(AdditionalInformationStateDropdown, "AdditionalInformationStateDropdown", "State")%></td>
								<td>
									<div style="position: relative;">
										<asp:DropDownList ID="AdditionalInformationStateDropdown" runat="server" Width="285px" TabIndex="25" ClientIDMode="Static">
											<asp:ListItem Text="- select a state -" />
										</asp:DropDownList>
									</div>
									<asp:RequiredFieldValidator runat="server" ID="AdditionalInformationStateValidator" ControlToValidate="AdditionalInformationStateDropdown" SetFocusOnError="True" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1Additional"></asp:RequiredFieldValidator>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr id="AdditionalInformationDateOfBirthRow" runat="server" clientidmode="Static">
					<td class="column1">
					  <%= HtmlRequiredLabel(AdditionalInformationDateOfBirthTextBox, "AdditionalInformationDateOfBirth", "Date of birth")%>
					</td>
					<td colspan="2">
						<table role="presentation">
							<tr>
								<td>
									<asp:TextBox ID="AdditionalInformationDateOfBirthTextBox" runat="server" ClientIDMode="Static" width="90" TabIndex="26" />
									<ajaxtoolkit:MaskedEditExtender ID="meDOBDate" runat="server" Mask="99/99/9999" MaskType="None" TargetControlID="AdditionalInformationDateOfBirthTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_"/>
									<span class="instructionalText">mm/dd/yyyy</span>
								</td>
							</tr>
							<tr>
								<td><asp:CustomValidator ID="DOBValidator" runat="server" ControlToValidate="AdditionalInformationDateOfBirthTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1Additional" ClientValidationFunction="ValidateDOB" ValidateEmptyText="true" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<asp:PlaceHolder runat="server" ID="EducationPhoneNumberPlaceHolder" Visible="False">
				<tr>
						<td class="column1">
							<%= HtmlLocalise("PhoneNumber.Label", "Phone number")%>
						</td>
						<td colspan="2" class="column2">
							<asp:TextBox runat="server" ID="EducationPhoneNumberTextBox" ClientIDMode="Static" Width="180" TabIndex="27"/>
              <asp:DropDownList ID="EducationPhoneNumberTypeDropDownList" runat="server" Width="95px" OnChange="RegisterJobSeeker_PhoneNumberTypeChanged(true);">
						  </asp:DropDownList>
              <ajaxtoolkit:MaskedEditExtender ID="meEducationPhoneNumber" runat="server" MaskType="Number" TargetControlID="EducationPhoneNumberTextBox" PromptCharacter="_" ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false"/>
              <br />
							<asp:CustomValidator ID="EducationPhoneNumberValidator" runat="server" ControlToValidate="EducationPhoneNumberTextBox"
									 SetFocusOnError="true" Display="Dynamic" CssClass="error" ClientValidationFunction="RegisterJobSeeker_ValidateEducationPhoneNumber" ValidateEmptyText="true" />
						</td>
					</tr>
					<tr id="CellphoneProviderRow" style="display: none; vertical-align: top" runat="server">
						<td class="column1">
							<%= HtmlLocalise("CellphoneProvider.Label", "Cellphone provider")%>
						</td>
						<td class="column2">
							<asp:DropDownList runat="server" ID="CellphoneProviderDropDown" ClientIDMode="Static" TabIndex="28" Width="308" EnableViewState="true"/>
						</td>
						<td class="column3"></td>
					</tr>
					</asp:PlaceHolder>
			</table>
			<act:CalendarExtender ID="DateOfBirthCalendarExtender" runat="server" Format="MM/dd/yyyy" TargetControlID="AdditionalInformationDateOfBirthTextBox" CssClass="cal_Theme1" />
		</asp:Panel>
		<asp:Panel ID="panRegisterStep1Button" runat="server">
			<table style="width: 100%;" role="presentation">
				<tr>
					<td>
					</td>
				</tr>
				<tr>
					<td style="text-align:right;">
						<asp:Button ID="CancelButtonStep1" runat="server" ClientIDMode="Static" CssClass="button1" CausesValidation="false" OnClick="CancelRegistration_Click" TabIndex="31" />
						<asp:Button ID="RegisterButton" runat="server" ClientIDMode="Static" OnClick="RegisterButton_Click" CssClass="button1" OnClientClick="return validateAll();" TabIndex="29"/>
						<asp:Button ID="SaveButton" runat="server" ClientIDMode="Static" OnClick="SaveButton_Click" CssClass="button1" OnClientClick="return validateAll();" TabIndex="30"/>
					</td>
				</tr>
			</table>
		</asp:Panel>
		<asp:Panel ID="RegisterStep2Panel" runat="server" Visible="false">
			<table style="width:100%;" role="presentation">
				<tr>
					<td>
						<asp:Panel ID="panConsent" runat="server" BorderWidth="0px" Height="320px" ScrollBars="Auto"
							Style="text-align: justify;">
							<%= HtmlLocalise("Global.TermsOfUseEducation.Text", DefaultText.EducationTermsAndConditions)%>
						</asp:Panel>
					</td>
				</tr>
				<tr>
					<td style="text-align:right;">
						<asp:Panel ID="panNormalUserSection" runat="server">
							<asp:Button ID="CancelButtonStep2" runat="server" ClientIDMode="Static" CssClass="button1" CausesValidation="false" OnClick="CancelRegistration_Click" />
							<asp:Button ID="AgreeToTermsButton" runat="server" ClientIDMode="Static" CssClass="button1" CausesValidation="false" OnClick="AgreeToTermsButton_Click"/>
						</asp:Panel>
					</td>
				</tr>
			</table>
		</asp:Panel>
		</asp:panel>


<uc:ConfirmationModal ID="Confirmation" runat="server" />
<script type="text/javascript">
  $(document).ready(function () {
    $("#AdditionalInformationZipTextBox").mask("<%= OldApp_RefactorIfFound.Settings.PostalCodeMaskPattern %>", { placeholder: " " });
    $("#AdditionalInformationZipTextBox").change(function () { GetStateCountyCityAndCountiesForPostalCode($(this).val(), "AdditionalInformationStateDropdown", "AdditionalInformationCityTextBox", "<%=AdditionalInformationCityValidator.ClientID%>"); });

    $("#SecurityQuestionDropDown").change(function () {
      if ($(this).val() == "<%= CreateOwnSecurityQuestionValue %>") {
        $("#SecurityQuestionDiv").show();
      } else {
        $("#SecurityQuestionDiv").hide();
      }
    });

    $("#EmailAddressTextbox").keyup(function () {
      $("#UsernameInUseErrorMessage").hide();
    });


    if ($("#SecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
      $("#SecurityQuestionDiv").show();
    }
    RegisterJobSeeker_PhoneNumberTypeChanged(false);
  });

function GetStateCountyCityAndCountiesForPostalCode(postalCode, stateDropDownList, cityTextBox, cityValidator) {
  var options = { type: "POST",
    url: "<%= UrlBuilder.AjaxService() %>/GetStateCityCountyAndCountiesForPostalCode",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: true,
    data: '{"postalCode": "' + postalCode + '"}',
    success: function (response) {
      var results = response.d;
      if (results && results.StateId) {
        $("#" + stateDropDownList).val(results.StateId);
        $("#" + stateDropDownList).next().find(':first-child').text($("#" + stateDropDownList + " option:selected").text()).parent().addClass('changed');

        if (results.CityName) {
          $("#" + cityTextBox).val(results.CityName);
        }
      }
      else {
        $("#" + stateDropDownList).val('');
        $("#" + cityTextBox).val('');
      }

      var cityValidatorControl = $("#" + cityValidator)[0];
      var originalFocus = cityValidatorControl.focusOnError;
      cityValidatorControl.focusOnError = "f";
      ValidatorValidate(cityValidatorControl);
      cityValidatorControl.focusOnError = originalFocus;
    }
  };

  $.ajax(options);
}

function RegisterJobSeeker_ValidateSsn(src, args) {
  var ssnPart1 = $("#SocialSecurityNumberPart1TextBox").val();
  var ssnPart2 = $("#SocialSecurityNumberPart2TextBox").val();
  var ssnPart3 = $("#SocialSecurityNumberPart3TextBox").val();
  if (ssnPart1 == "" && ssnPart2 == "" && ssnPart3 == "") {
    args.IsValid = true;
  } else {
    var ssn = ssnPart1 + "-" + ssnPart2 + "-" + ssnPart3;
    var ssnRegExp = /^[0-9]{3}[\- ]?[0-9]{2}[\- ]?[0-9]{4}$/;
    args.IsValid = ssnRegExp.test(ssn);
  }
}

function RegisterJobSeeker_ValidateConfirmSsn(src, args) {
  var ssnPart1 = $("#SocialSecurityNumberPart1TextBox").val();
  var ssnPart2 = $("#SocialSecurityNumberPart2TextBox").val();
  var ssnPart3 = $("#SocialSecurityNumberPart3TextBox").val();
  var confirmSsnPart1 = $("#ConfirmSocialSecurityNumberPart1TextBox").val();
  var confirmSsnPart2 = $("#ConfirmSocialSecurityNumberPart2TextBox").val();
  var confirmSsnPart3 = $("#ConfirmSocialSecurityNumberPart3TextBox").val();

  args.IsValid = ((ssnPart1 == confirmSsnPart1) && (ssnPart2 == confirmSsnPart2) && (ssnPart3 == confirmSsnPart3));
}

	Sys.Application.add_load(RegisterJobSeeker_PageLoad);
	function RegisterJobSeeker_PageLoad(sender, args) {
	  var cdd = $find("DegreeDropDownListCascadingDropDownBehavior");
	  if (cdd != null) {
	    $('#<%=DegreeDropDownList.ClientID%>').data("originalvalue", $('#<%=DegreeDropDownList.ClientID%>').val());
	    cdd.add_populated(RegisterJobSeeker_DegreeDropdown_OnPopulated);
	  }
	}
	function RegisterJobSeeker_DegreeDropdown_OnPopulated() {
	  UpdateStyledDropDown($('#<%=DegreeDropDownList.ClientID%>'));
	  
	  var originalValue = $('#<%=DegreeDropDownList.ClientID%>').data("originalvalue");
	  if (originalValue != null && originalValue != "") {
	    $('#<%=DegreeDropDownList.ClientID%>').val(originalValue);
	    $('#<%=DegreeDropDownList.ClientID%>').data("originalvalue", "");
	  }
	}
	
	function validateOwnSecurityQuestion(src, args) {
		if ($("#SecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
			var ownSecurityQuestion = $("#SecurityQuestionTextBox").val();
			if (ownSecurityQuestion.length > 0) ownSecurityQuestion = TrimText(ownSecurityQuestion);
			args.IsValid = (ownSecurityQuestion != "");
		} else {
			args.IsValid = true;
		}
	}

	function validateAll() {
		var validateAdditional = true;
		var isValid = true;
	  var step1Valid = true;
		for (var i = 0; i < Page_Validators.length; i++) {
			if ((Page_Validators[i].validationGroup === 'RegisterStep1') || (Page_Validators[i].validationGroup === 'RegisterStep1Additional' && validateAdditional)) {
				ValidatorValidate(Page_Validators[i]);
				if (!Page_Validators[i].isvalid) {
				  isValid = false;
				  if (Page_Validators[i].validationGroup === 'RegisterStep1')
				    step1Valid = false;
				}
			}
		}

    if (!isValid && step1Valid && validateAdditional) {
      var panel = $('#<%=RegisterStep1Panel.ClientID %>');
      panel.scrollTop(panel[0].scrollHeight);
    }
		return isValid;
	}

	function ValidateDOB(sender, args) {
	  if (args.Value != "__/__/____") {
	    var dateRegx = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

	    if (dateRegx.test(args.Value) == false) {
	      sender.innerHTML = "<%= DateErrorMessage %>";
	      args.IsValid = false;
	    } else {
	      var birthStrings = args.Value.split("/");
	      var birthDate = new Date(parseInt(birthStrings[2], 10), parseInt(birthStrings[0], 10) - 1, parseInt(birthStrings[1], 10));

	      var age = GetAgeForDate(birthDate);
	      if (age < 14) {
	        sender.innerHTML = "<%= DOBMinLimitErrorMessage %>";
	        args.IsValid = false;
	      } else if (age > 99) {
	        sender.innerHTML = "<%= DOBMaxLimitErrorMessage %>";
	        args.IsValid = false;
	      }
	    }
	  }
	  else {
	    sender.innerHTML = "<%= DateOfBirthRequiredMessage %>";
	    args.IsValid = false;
	  }
	}

	function ValidatePhone(sender, args) {
		if (args.Value != "(___) ___-____") {
		  var phoneRegx = /^\([0-9]\d{2}\)\s?\d{3}\-\d{4}$/;
		  if (phoneRegx.test(args.Value) == false) {
		    args.IsValid = false;
		    sender.innerHTML = RegisterJobSeeker_ValidationMessages["PhoneNumberInvalid"];
		  }
		} else {
		  sender.innerHTML = RegisterJobSeeker_ValidationMessages["PhoneNumberRequired"];
		  args.IsValid = false;
		}
	}
	
	function ClearUserNameInUserMsg() {
//		$("#UsernameInUseErrorMessage").val('');
		document.getElementById('UsernameInUseErrorMessage').innerHTML = '';
	}

	function RegisterJobSeeker_PhoneNumberTypeChanged(scrollIntoView) {
	  var phoneDropDown = $('#<%=EducationPhoneNumberTypeDropDownList.ClientID%>');
	  if (phoneDropDown.length == 0)
	    return;
	  
	  var phoneValue = $(phoneDropDown).val();

	  var providerRow = $("#<%=CellphoneProviderRow.ClientID%>");
	  if (phoneValue == "Cell") {
	    providerRow.show();
	    if (scrollIntoView)
	      scrollContainerIntoViewIfNeeded('<%=CellphoneProviderDropDown.ClientID%>', '<%=RegisterStep1Panel.ClientID %>');
	  }
	  else {
      providerRow.hide();
    }

    var behaviorPhone = $find('<%=meEducationPhoneNumber.ClientID %>');
    if (behaviorPhone == null)
      return;
	  
    if (phoneValue != "NonUS") {
      behaviorPhone.set_Mask("(999) 999-9999");
      behaviorPhone.set_PromptCharacter("_");
      behaviorPhone._convertMask();
      behaviorPhone.set_MaskType(Sys.Extended.UI.MaskedEditType.Number);
    }
    else {
      behaviorPhone.set_Mask("99999999999999999999");
      behaviorPhone.set_PromptCharacter(" ");
      behaviorPhone._convertMask();
      behaviorPhone.set_MaskType(Sys.Extended.UI.MaskedEditType.None);
    }
  }

  function RegisterJobSeeker_ValidateEducationPhoneNumber(sender, args) {
    var phoneDropDown = $('#<%=EducationPhoneNumberTypeDropDownList.ClientID%>');
    var phoneValue = $(phoneDropDown).val();

    if (args.Value.length > 0 && args.Value != "(___) ___-____") {
      if (phoneValue != "" && phoneValue != "NonUS") {
        var phoneRegx = /^\([0-9]\d{2}\)\s?\d{3}\-\d{4}$/;
        if (phoneRegx.test(args.Value) == false) {
          args.IsValid = false;
        }
      }
    }
  }
</script>
