﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Settings.Lens;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class CareerDefaults : UserControlBase
	{
    private string SpideredJobsExpiry
    {
      get { return GetViewStateValue<string>("CareerDefaults:SpideredJobsExpiry"); }
      set { SetViewStateValue("CareerDefaults:SpideredJobsExpiry", value); }
    }
    
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Localise();
				ApplyTheme();
				ApplyBranding();
			}
			if (App.Settings.UseCustomHeaderHtml)
			{
				ColourDefaultsHeaderPanel.Visible = false;
				ColourDefaultsPanel.Visible = false;
				LogoDefaultsHeaderPanel.Visible = false;
				LogoDefaultsPanel.Visible = false;
			}
		}

    /// <summary>
    /// Handles the Click event of the AddSearchStateButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void AddSearchStateButton_Click(object sender, EventArgs e)
    {
      switch (SearchStatesDropDown.SelectedValue)
      {
        case "":
          break;

        case "-1":
          foreach (var item in SearchStatesDropDown.Items.Cast<ListItem>().Where(item => item.Value.Length > 0 && item.Value != @"-1"))
          {
            SearchStateUpdateablePairs.AddItem(new KeyValuePair<string, string>(item.Value, item.Text));
          }
          SearchStatesDropDown.SelectedIndex = 0;
          break;

        default:
          SearchStateUpdateablePairs.AddItem(new KeyValuePair<string, string>(SearchStatesDropDown.SelectedValue, SearchStatesDropDown.SelectedItem.Text));
          SearchStatesDropDown.SelectedIndex = 0;
          break;
      }
    }

		/// <summary>
		/// Handles the Click event of the SaveChangesButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveChangesButton_Click(object sender, EventArgs e)
		{
			if (LightColourTextBox.Text == string.Empty)
			{
				LightColourValidator.IsValid = false;
				return;
			}

			if (DarkColourTextBox.Text == string.Empty)
			{
				DarkColourValidator.IsValid = false;
				return;
			}

      var bytes = App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedImage);

      if (bytes.IsNotNullOrEmpty())
      {
        var applicationImage = new ApplicationImageDto { Image = bytes, Type = ApplicationImageTypes.FocusCareerExplorerHeader };
        ServiceClientLocator.CoreClient(App).SaveApplicationImage(applicationImage);

        CareerExplorerApplicationLogoUploader.Clear();
        CareerExplorerApplicationLogoUploader.SetPreviewImage(UrlBuilder.ApplicationImage(ApplicationImageTypes.FocusCareerExplorerHeader));
      }

			var configurationItems = new List<ConfigurationItemDto>();

      if (App.Settings.Theme == FocusThemes.Education)
      {
        var schoolType = (SchoolTypes)Enum.Parse(typeof(SchoolTypes), SchoolTypeDropDown.SelectedValue, true);

        if (schoolType != App.Settings.SchoolType)
          configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.SchoolType, Value = ((int)schoolType).ToString(CultureInfo.InvariantCulture) });
      }

      var careerExplorerFeatureEmphasis = (CareerExplorerFeatureEmphasis)PlaceEmphasisOnDropDown.SelectedValueToFlagEnum<CareerExplorerFeatureEmphasis>();
      if (careerExplorerFeatureEmphasis != App.Settings.CareerExplorerFeatureEmphasis)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.CareerExplorerFeatureEmphasis, Value = ((int)careerExplorerFeatureEmphasis).ToString() });
      
      if (LightColourTextBox.TextTrimmed() != App.Settings.CareerExplorerLightColour)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.CareerExplorerLightColour, Value = LightColourTextBox.TextTrimmed() });

      if (DarkColourTextBox.TextTrimmed() != App.Settings.CareerExplorerDarkColour)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.CareerExplorerDarkColour, Value = DarkColourTextBox.TextTrimmed() });

      if (CareerExplorerFooterLogoCheckBox.Checked != App.Settings.ShowBurningGlassLogoInCareerExplorer)
        configurationItems.Add(new ConfigurationItemDto{Key = Constants.ConfigurationItemKeys.ShowBurningGlassLogoInCareerExplorer, Value = CareerExplorerFooterLogoCheckBox.Checked.ToString()});

      if (App.Settings.Theme == FocusThemes.Workforce)
      {
        if (DisplaySurveyFrequencyCheckBox.Checked != App.Settings.DisplaySurveyFrequency)
          configurationItems.Add(new ConfigurationItemDto
                                   {
                                     Key = Constants.ConfigurationItemKeys.DisplaySurveyFrequency,
                                     Value = DisplaySurveyFrequencyCheckBox.Checked.ToString()
                                   });

        if (SurveyFrequency.Value.ToInt() != App.Settings.SurveyFrequency)
          configurationItems.Add(new ConfigurationItemDto
                                   {
                                     Key = Constants.ConfigurationItemKeys.SurveyFrequency,
                                     Value = SurveyFrequency.Value
                                   });
      }

      if (ProspectiveStudentsCheckBox.Checked != App.Settings.SupportsProspectiveStudents)
        configurationItems.Add(new ConfigurationItemDto
        {
          Key = Constants.ConfigurationItemKeys.SupportsProspectiveStudents,
          Value = ProspectiveStudentsCheckBox.Checked.ToString()
        });

		  UnbindCareerSearchSettings(configurationItems);
		  UnbindAlertSettings(configurationItems);
		  UnbindJobDisclaimerSettings(configurationItems);

			if (configurationItems.IsNotNullOrEmpty())
			{
				ServiceClientLocator.CoreClient(App).SaveConfigurationItems(configurationItems);
				App.RefreshSettings();
			}

			Confirmation.Show(CodeLocalise("CareerDefaultsUpdatedConfirmation.Title", "Career defaults updated"),
															 CodeLocalise("CareerDefaultsUpdatedConfirmation.Body", "The default settings have been updated."),
															 CodeLocalise("CloseModal.Text", "OK"));

			// This is a bit of a hack really. When the ddl was changed and then the checkbox unticked and save was pressed after save, altho the correct5 value had been saved the previous value was displaying on the ddl
      if (App.Settings.Theme == FocusThemes.Workforce)
			SurveyFrequencyDropDownList.SelectedValue = SurveyFrequency.Value;
		}

    /// <summary>
    /// Handles the Click event of the ResetColoursButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ResetColoursButton_Click(object sender, EventArgs e)
    {
      LightColourTextBox.Text = App.Settings.CareerExplorerLightColour;
      DarkColourTextBox.Text = App.Settings.CareerExplorerDarkColour;
    }
		

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			BindControls();

		  if (App.Settings.Theme == FocusThemes.Education)
		  {
		    SchoolTypeDropDown.SelectValue(App.Settings.SchoolType.ToString());
		  }

      PlaceEmphasisOnDropDown.SelectValue(App.Settings.CareerExplorerFeatureEmphasis.ToString());

		  CareerExplorerApplicationLogoUploader.Clear();
      CareerExplorerApplicationLogoUploader.SetPreviewImage(UrlBuilder.ApplicationImage(ApplicationImageTypes.FocusCareerExplorerHeader));

      LightColourTextBox.Text = App.Settings.CareerExplorerLightColour;
      DarkColourTextBox.Text = App.Settings.CareerExplorerDarkColour;

      CareerExplorerFooterLogoCheckBox.Checked = App.Settings.ShowBurningGlassLogoInCareerExplorer;

      AllJobsMatchRadioButton.Checked = App.Settings.CareerSearchShowAllJobs;
      MinimumStarMatchRadioButton.Checked = !AllJobsMatchRadioButton.Checked;
      MinimumStarMatchDropDown.SelectValue(App.Settings.CareerSearchMinimumStarMatchScore.ToString(CultureInfo.InvariantCulture));

      DefaultSearchRadiusDropDown.SelectedValue = App.Settings.CareerSearchDefaultRadiusId == 0
		                                                ? ""
                                                    : App.Settings.CareerSearchDefaultRadiusId.ToString(CultureInfo.InvariantCulture);

      ZipCodeRadioButton.Checked = App.Settings.CareerSearchCentrePointType == SearchCentrePointOptions.Zipcode;
      SelectedLocationsRadioButton.Checked = App.Settings.CareerSearchCentrePointType == SearchCentrePointOptions.SelectedLocations;

      SearchRadiusDropDown.SelectValue(App.Settings.CareerSearchRadius.ToString(CultureInfo.InvariantCulture));
		  ZipCodeTextBox.Text = App.Settings.CareerSearchZipcode;

		  var states = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).ToDictionary(s => s.Key, s => s.Text);
      SearchStateUpdateablePairs.Items = App.Settings.NearbyStateKeys.Select(k => new KeyValuePair<string, string>(k, states[k])).ToList();

		  var hasSpideredJobs = false;

      var lensServiceConfigs = ServiceClientLocator.CoreClient(App).GetConfigurationItems("TestLensService");
      foreach (var item in lensServiceConfigs)
      {
        var lensService = (LensService) item.Value.DeserializeJson(typeof (LensService));
        if (lensService.IsSpideredJobsLens == true)
        {
          hasSpideredJobs = true;
          SpideredJobsExpiry = lensService.MaximumDaysSearchableOn.HasValue && lensService.MaximumDaysSearchableOn.Value > 0
            ? lensService.MaximumDaysSearchableOn.Value.ToString(CultureInfo.InvariantCulture)
            : string.Empty;
        }
      }

      if (hasSpideredJobs)
		  {
        SpideredJobsExpiryDropDown.SelectValue(SpideredJobsExpiry);
		    SpideredJobsPlaceHolder.Visible = true;
		  }
		  else
		  {
		    SpideredJobsExpiry = "";
		    SpideredJobsPlaceHolder.Visible = false;
		  }

      ResumesSearchableDropDown.SelectValue(App.Settings.CareerSearchResumesSearchable.ToString());

			DefaultPostingAgeDropDownList.SelectValue(App.Settings.JobSearchPostingDate.ToString());

      JobAlertsDropDown.SelectValue(App.Settings.JobAlertsOption.ToString());
      AlertFrequencyDropDown.SelectValue(App.Settings.JobAlertFrequency.ToString());
      EmailOtherJobsCheckBox.Checked = App.Settings.SendOtherJobsEmail;
		  StaffEmailAlertDropDown.SelectValue(App.Settings.StaffEmailAlerts.ToString());

			DisplaySurveyFrequencyCheckBox.Checked = App.Settings.DisplaySurveyFrequency;
			SurveyFrequencyDropDownList.SelectedValue = App.Settings.SurveyFrequency.ToString(CultureInfo.InvariantCulture);

		  ProspectiveStudentsCheckBox.Checked = App.Settings.SupportsProspectiveStudents;

		  VeteranPriorityCheckBox.Checked = App.Settings.VeteranPriorityServiceEnabled;
      VeteranPriorityStarMatchingDropDownList.SelectValue(App.Settings.VeteranPriorityStarMatching.ToString(CultureInfo.InvariantCulture));

      SpideredJobApplicationDisclaimerCheckBox.Checked = App.Settings.SpideredJobDisclaimerEnabled;
      SpideredJobApplicationDisclaimerTextBox.Value = App.Settings.SpideredJobDisclaimerText;
      SearchResultsDisclaimerCheckBox.Checked = App.Settings.SearchResultsDisclaimerEnabled;
      SearchResultsDisclaimerTextBox.Value = App.Settings.SearchResultsDisclaimerText;
		
		}

		/// <summary>
		/// Binds the controls.
		/// </summary>
		private void BindControls()
		{
			BindSearchStatesDropDown();
			BindSearchRadiusDropDown();
			BindResumesSearchableDropDown();
      BindJobAlertsDropDown();
      BindAlertFrequencyDropDown();
			BindCollegeTypeDropDown();
		  BindCareerExplorerFeatureEmphasis();
			BindDefaultPostingAgeDropDown();
		}

		/// <summary>
		/// Binds the search states drop down.
		/// </summary>
		private void BindSearchStatesDropDown()
		{
      SearchStatesDropDown.DataSource = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States);
      SearchStatesDropDown.DataValueField = "Key";
      SearchStatesDropDown.DataTextField = "Text";
      SearchStatesDropDown.DataBind();

      SearchStatesDropDown.Items.Insert(0, new ListItem(CodeLocalise("AllStates.Text.NoEdit", "-- Select state --"), ""));
      SearchStatesDropDown.Items.Insert(1, new ListItem(CodeLocalise("AllStates.Text.NoEdit", "All states"), "-1"));
    }

		/// <summary>
		/// Binds the search radius drop down.
		/// </summary>
		private void BindSearchRadiusDropDown()
		{
      DefaultSearchRadiusDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses), null, CodeLocalise("Radius.TopDefault.NoEdit", "N/A"));

			SearchRadiusDropDown.Items.Clear();

      SearchRadiusDropDown.Items.Add(new ListItem(CodeLocalise("AnyRadius.Text.NoEdit", "any"), "-1"));
      SearchRadiusDropDown.Items.Add(new ListItem(CodeLocalise("50MileRadius.Text.NoEdit", "50 mile"), "50"));
      SearchRadiusDropDown.Items.Add(new ListItem(CodeLocalise("75MileRadius.Text.NoEdit", "75 mile"), "75"));
      SearchRadiusDropDown.Items.Add(new ListItem(CodeLocalise("100MileRadius.Text.NoEdit", "100 mile"), "100"));
		}

		/// <summary>
		/// Binds the resumes searchable drop down.
		/// </summary>
		private void BindResumesSearchableDropDown()
		{
      ResumesSearchableDropDown.Items.Clear();

      ResumesSearchableDropDown.Items.AddEnum(ResumeSearchableOptions.NotApplicable, "N/A");
      ResumesSearchableDropDown.Items.AddEnum(ResumeSearchableOptions.Yes, "Yes");
      ResumesSearchableDropDown.Items.AddEnum(ResumeSearchableOptions.No, "No");
      ResumesSearchableDropDown.Items.AddEnum(ResumeSearchableOptions.JobSeekersOptionYesByDefault, "Job seeker's option (yes by default)");
      ResumesSearchableDropDown.Items.AddEnum(ResumeSearchableOptions.JobSeekersOptionNoByDefault, "Job seeker's option (no by default)");
		}


		/// <summary>
		/// Binds the job alerts drop down.
		/// </summary>
		private void BindJobAlertsDropDown()
		{
      JobAlertsDropDown.Items.Clear();

      JobAlertsDropDown.Items.AddEnum(JobAlertsOptions.On, "On");
      JobAlertsDropDown.Items.AddEnum(JobAlertsOptions.OnButCanUnsubscribe, "On, but user may unsubscribe");
      JobAlertsDropDown.Items.AddEnum(JobAlertsOptions.UserConfiguration, "User configures alert settings");
		}
		/// <summary>
		/// Binds the alert frequency drop down.
		/// </summary>
		private void BindAlertFrequencyDropDown()
		{
      AlertFrequencyDropDown.Items.Clear();

      AlertFrequencyDropDown.Items.AddEnum(AlertFrequencies.Daily, "Daily");
      AlertFrequencyDropDown.Items.AddEnum(AlertFrequencies.Weekly, "Weekly");

      StaffEmailAlertDropDown.Items.Clear();
      StaffEmailAlertDropDown.Items.AddEnum(AlertFrequencies.Never, "None");
      StaffEmailAlertDropDown.Items.AddEnum(AlertFrequencies.Daily, "Daily");
      StaffEmailAlertDropDown.Items.AddEnum(AlertFrequencies.Weekly, "Weekly");
    }

		private void BindCollegeTypeDropDown()
		{
			SchoolTypeDropDown.Items.Clear();

			SchoolTypeDropDown.Items.AddEnum(SchoolTypes.TwoYear, "2");
			SchoolTypeDropDown.Items.AddEnum(SchoolTypes.FourYear, "4");

			SchoolTypeDropDown.Items.AddLocalisedTopDefault("CollegeType.TopDefault", "- Select -");
		}

    private void BindCareerExplorerFeatureEmphasis()
    {
      PlaceEmphasisOnDropDown.Items.Clear();

      PlaceEmphasisOnDropDown.Items.AddEnum(CareerExplorerFeatureEmphasis.Career, "Career features");
      PlaceEmphasisOnDropDown.Items.AddEnum(CareerExplorerFeatureEmphasis.Explorer, "Explorer features");
    }

		/// <summary>
		/// Binds the default job posting date drop down.
		/// </summary>
		private void BindDefaultPostingAgeDropDown()
		{
			DefaultPostingAgeDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges), null, true);
		}

    /// <summary>
    /// Gets the new configuration settings for Career Searches based on the fields entered on the page
    /// </summary>
    /// <param name="configurationItems">A list of configurations items to be saved</param>
    private void UnbindCareerSearchSettings(List<ConfigurationItemDto> configurationItems)
    {
      var updates = new List<ConfigurationItemUpdate>
      {
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.CareerSearchShowAllJobs,
          NewValue = AllJobsMatchRadioButton.Checked.ToString(),
          OldValue = App.Settings.CareerSearchShowAllJobs.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.CareerSearchMinimumStarMatchScore,
          NewValue = MinimumStarMatchDropDown.SelectedValue,
          OldValue = App.Settings.CareerSearchMinimumStarMatchScore.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.CareerSearchDefaultRadiusId,
          NewValue = DefaultSearchRadiusDropDown.SelectedValue.Length == 0 ? "0" : DefaultSearchRadiusDropDown.SelectedValue,
          OldValue = App.Settings.CareerSearchDefaultRadiusId.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.CareerSearchCentrePointType,
          NewValue = (ZipCodeRadioButton.Checked ? (int) SearchCentrePointOptions.Zipcode : (int) SearchCentrePointOptions.SelectedLocations).ToString(CultureInfo.InvariantCulture),
          OldValue = ((int)App.Settings.CareerSearchCentrePointType).ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.CareerSearchRadius,
          NewValue = SearchRadiusDropDown.SelectedValue,
          OldValue = App.Settings.CareerSearchRadius.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.CareerSearchZipcode,
          NewValue = ZipCodeTextBox.Text.Trim(),
          OldValue = App.Settings.CareerSearchZipcode
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NearbyStateKeys,
          NewValue = string.Join(",", SearchStateUpdateablePairs.Items.Select(i => i.Key)),
          OldValue = string.Join(",", App.Settings.NearbyStateKeys)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.CareerSearchResumesSearchable,
          NewValue = ResumesSearchableDropDown.SelectedValueToFlagEnum(ResumeSearchableOptions.JobSeekersOptionYesByDefault).ToString(CultureInfo.InvariantCulture),
          OldValue = ((int)App.Settings.CareerSearchResumesSearchable).ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.VeteranPriorityServiceEnabled,
          NewValue = VeteranPriorityCheckBox.Checked.ToString(),
          OldValue = App.Settings.VeteranPriorityServiceEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.VeteranPriorityStarMatching,
          NewValue = VeteranPriorityStarMatchingDropDownList.SelectedValue,
          OldValue = App.Settings.VeteranPriorityStarMatching.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.JobSearchPostingDate,
          NewValue = DefaultPostingAgeDropDownList.SelectedValue,
          OldValue = App.Settings.JobSearchPostingDate.ToString(CultureInfo.InvariantCulture)
        }
      };

      configurationItems.AddRange(updates.Where(u => u.OldValue != u.NewValue).Select(u => new ConfigurationItemDto
      {
        Key = u.Key,
        Value = u.NewValue
      }));

      if (SpideredJobsPlaceHolder.Visible && SpideredJobsExpiryDropDown.SelectedValue != SpideredJobsExpiry)
      {
        var lensServiceConfigs = ServiceClientLocator.CoreClient(App).GetConfigurationItems("TestLensService");
        var newValue = SpideredJobsExpiryDropDown.SelectedValue;

        foreach (var item in lensServiceConfigs)
        {
          var lensService = (LensService)item.Value.DeserializeJson(typeof(LensService));
          if (lensService.IsSpideredJobsLens == true)
          {
            lensService.MaximumDaysSearchableOn = newValue.Length == 0 ? null : newValue.ToInt();

            configurationItems.Add(new ConfigurationItemDto
            {
              Id = item.Id,
              Value = lensService.SerializeJson()
            });
          }
        }
      }
    }

    /// <summary>
    /// Gets the new configuration settings for alerts based on the fields entered on the page
    /// </summary>
    /// <param name="configurationItems">A list of configurations items to be saved</param>
    private void UnbindAlertSettings(List<ConfigurationItemDto> configurationItems)
    {
      var updates = new List<ConfigurationItemUpdate>
      {
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.JobAlertsOption,
          NewValue = JobAlertsDropDown.SelectedValueToFlagEnum(JobAlertsOptions.UserConfiguration).ToString(CultureInfo.InvariantCulture),
          OldValue = ((int)App.Settings.JobAlertsOption).ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.JobAlertFrequency,
          NewValue = JobAlertsDropDown.SelectedIndex == 2 
                       ? ((int)App.Settings.JobAlertFrequency).ToString(CultureInfo.InvariantCulture)
                       : AlertFrequencyDropDown.SelectedValueToFlagEnum(AlertFrequencies.Weekly).ToString(CultureInfo.InvariantCulture),
          OldValue = ((int)App.Settings.JobAlertFrequency).ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.SendOtherJobsEmail,
          NewValue = EmailOtherJobsCheckBox.Checked.ToString(),
          OldValue = App.Settings.SendOtherJobsEmail.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.StaffEmailAlerts,
          NewValue = StaffEmailAlertDropDown.SelectedValueToFlagEnum(AlertFrequencies.Never).ToString(CultureInfo.InvariantCulture),
          OldValue = ((int)App.Settings.StaffEmailAlerts).ToString(CultureInfo.InvariantCulture)
        }
      };

      configurationItems.AddRange(updates.Where(u => u.OldValue != u.NewValue).Select(u => new ConfigurationItemDto
      {
        Key = u.Key,
        Value = u.NewValue
      }));
    }

    /// <summary>
    /// Gets the new configuration settings for alerts based on the fields entered on the page
    /// </summary>
    /// <param name="configurationItems">A list of configurations items to be saved</param>
    private void UnbindJobDisclaimerSettings(List<ConfigurationItemDto> configurationItems)
    {
      var updates = new List<ConfigurationItemUpdate>
      {
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.SearchResultsDisclaimerEnabled,
          NewValue = SearchResultsDisclaimerCheckBox.Checked.ToString(),
          OldValue = App.Settings.SearchResultsDisclaimerEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.SearchResultsDisclaimerText,
          NewValue = SearchResultsDisclaimerTextBox.Value,
          OldValue = App.Settings.SearchResultsDisclaimerText
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.SpideredJobDisclaimerEnabled,
          NewValue = SpideredJobApplicationDisclaimerCheckBox.Checked.ToString(),
          OldValue = App.Settings.SpideredJobDisclaimerEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.SpideredJobDisclaimerText,
          NewValue = SpideredJobApplicationDisclaimerTextBox.Value,
          OldValue = App.Settings.SpideredJobDisclaimerText
        }
      };

      configurationItems.AddRange(updates.Where(u => u.OldValue != u.NewValue).Select(u => new ConfigurationItemDto
      {
        Key = u.Key,
        Value = u.NewValue
      }));
    }


		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void Localise()
		{
			SaveChangesTopButton.Text = SaveChangesBottomButton.Text = CodeLocalise("SaveChangesButton.Text.NoEdit", "Save changes");
      ResetColoursButton.Text = CodeLocalise("ResetColoursButton.Text.NoEdit", "Reset colors");
      AddSearchStateButton.Text = CodeLocalise("Global.Add.Text.NoEdit", "Add");
      SelectedLocationsRadioButton.Text = CodeLocalise("SelectedLocationsRadioButton.Text", "selected location(s)");
			EmailOtherJobsCheckBox.Text = CodeLocalise("EmailOtherJobsCheckBox.Text", "Send weekly 'other jobs that might interest you' email");
			DisplaySurveyFrequencyCheckBox.Text = CodeLocalise("DisplaySurveyFrequencyCheckBox.Text", "Display Survey Frequency");
      ProspectiveStudentsCheckBox.Text = CodeLocalise("ProspectiveStudentsCheckBox.Text", "Supports prospective students");
			DarkColourValidator.ErrorMessage = CodeLocalise("DarkColour.Required", "Please Select a Color");
			LightColourValidator.ErrorMessage = CodeLocalise("LightColour.Required", "Please Select a Color");
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
		  ProspectiveStudentsPlaceHolder.Visible = App.Settings.Theme == FocusThemes.Education;

			if (App.Settings.Theme == FocusThemes.Education)
			{
        GeneralApplicationDefaultsPanel.Visible = true;
			  SchoolTypePlaceHolder.Visible = true;
				SurveyFrequencyPlaceHolder.Visible = false;
			  VeteranPriorityPlaceHolder.Visible = false;
				Workforce1Placeholder.Visible = false;
			}
			else
			{
			  VeteranPriorityPlaceHolder.Visible = true;
			}
		}

		private void ApplyBranding()
		{
			SearchDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SearchDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			SearchDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			AlertDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			AlertDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			AlertDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			GeneralApplicationDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			GeneralApplicationDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			GeneralApplicationDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      JobDisclaimerDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      JobDisclaimerDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      JobDisclaimerDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ColourDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ColourDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ColourDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			LogoDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LogoDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			LogoDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

    private struct ConfigurationItemUpdate
    {
      public string Key;
      public string OldValue;
      public string NewValue;
    }
	}
}