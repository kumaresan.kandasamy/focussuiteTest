﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BackdateActivityModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.BackdateActivityModal" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="BackdateModalPopup" runat="server"  BehaviorID="BackdateActivityModal"
												TargetControlID="ModalDummyTarget"
												PopupControlID="BackdatePanel" 
												PopupDragHandleControlID="BackdateSelectorPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />
<asp:Panel ID="BackdatePanel" runat="server" CssClass="modal" Width="350px" Height="180px"
        ClientIDMode="Static">
        <div>
            <asp:Panel ID="BackdateSelectorPanel" runat="server" Visible="True">
                <div style="font-style: italic;font-size: 20.5px;">
    
                    Backdate activity/service
                </div>
                <br />
                <div>
                    <focus:LocalisedLabel runat="server" ID="CurrentServiceLabel" AssociatedControlID="CurrentServiceDateLabel"
                        LocalisationKey="CurrentService.Label" DefaultText="Current service date" />
                    <span style="margin-left: 32px">
                        <focus:LocalisedLabel runat="server" ID="CurrentServiceDateLabel" LocalisationKey="CurrentServiceDate.Label" />
                    </span>
                    <br />
                    <br />
                    <focus:LocalisedLabel runat="server" ID="NewServiceLabel" AssociatedControlID="NewServiceTextBox"
                        LocalisationKey="NewService.Label" DefaultText="New service date" />
                    <span style="margin-left: 40px">
                    <span class="dataInputGroupedItemHorizontal">
                        <asp:TextBox ID="NewServiceTextBox" runat="server" ClientIDMode="Static" Width="95px" PlaceHolder="mm/dd/yyyy" />
                        <act:MaskedEditExtender runat="server" ID="NewServiceTextBoxMask" Mask="99/99/9999" MaskType="Date" TargetControlID="NewServiceTextBox" />
                    </span>
                    <br />
                    <asp:CustomValidator ID="NewServiceTextDateValidate" runat="server" ControlToValidate="NewServiceTextBox" CssClass="error" SetFocusOnError="true" ValidationGroup="BackdateActivity" Display="Dynamic"
                        ClientValidationFunction="BackdateValidate" ValidateEmptyText="true" />
                    </span>
                </div>
                <div style="margin-top: 25px;margin-left:160px;">
                <asp:Button ID="BackdateCancelButton" class="button1" runat="server" Text="Cancel" OnClick="BackdateCancelButton_Click" />
                <asp:Button ID="BackdateButton" class="button1" runat="server" Text="Backdate" OnClick="BackdateButton_Click" ValidationGroup="BackdateActivity" />
                </div>
                
            </asp:Panel>
        </div>
    </asp:Panel>

    <script type="text/javascript">
        function BackdateValidate(sender, args) {
            if (args.Value == "__/__/____" || args.Value == "") {
                sender.innerHTML = "<%= NewServiceDateRequiredErrorMessage %>";
                args.IsValid = false;
            }
            else if (args.Value != "__/__/____") {
                var dateRegx = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

                if (dateRegx.test(args.Value) == false) {
                    sender.innerHTML = "<%= NewServiceDateTextErrorMessage %>";
                    args.IsValid = false;
                }
                else if (args.Value != "") {
                    var activityDate = $('#<%= CurrentServiceDateLabel.ClientID%>').get(0).innerHTML;
                    activityDate = new Date(activityDate);
                    var newServiceDate = new Date(args.Value);
                    if (activityDate < newServiceDate) {
                        sender.innerHTML = "<%= NewServiceDateValidate %>";
                        args.IsValid = false;
                    }
                }
            }
        }
    </script>
