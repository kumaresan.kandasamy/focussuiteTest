﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobIssueResolutionModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.JobIssueResolutionModal" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<asp:HiddenField ID="JobIssueResolutionModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="JobIssueResolutionModalPopup" runat="server" BehaviorID="JobIssueResolutionModal"
												TargetControlID="JobIssueResolutionModalDummyTarget"
												PopupControlID="JobIssueResolutionModalPanel"
												PopupDragHandleControlID="JobIssueResolutionModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="JobIssueResolutionModalPanel" runat="server" CssClass="modal" style="display:none;">
    <h2><asp:Literal runat="server" ID="ModalTitle" Text="Job issue resolution"/></h2>
    <%= HtmlRequiredLabel("Issues.Label", "Issues")%><br/>
    <asp:CheckBoxList runat="server" ID="IssueList" ClientIDMode="Static"></asp:CheckBoxList>
    <asp:CheckBoxList runat="server" ID="FollowUpList" ClientIDMode="Static"></asp:CheckBoxList>
    <asp:CustomValidator runat="server" ID="CheckBoxListValidator" ClientValidationFunction="ValidateIssueList" CssClass="error" ValidationGroup="JobIssueResolutionModal"></asp:CustomValidator><br/>
    <%= HtmlRequiredLabel(NoteTextBox, "Notes.Label", "Notes")%><br/>
    <asp:TextBox ID="NoteTextBox" runat="server" TextMode="MultiLine" Rows="5" Width="600"  /><br/>
    <asp:RequiredFieldValidator runat="server" ControlToValidate="NoteTextBox" ID="NoteValidator" CssClass="error"  ValidationGroup="JobIssueResolutionModal"></asp:RequiredFieldValidator><br/>
    <asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" CausesValidation="False" Text="Cancel"/>
	<asp:Button ID="SaveButton" runat="server" CssClass="button4" OnClick="SaveButton_Clicked" ValidationGroup="JobIssueResolutionModal" Text="Save"/>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />
<script type="text/javascript">
    function ValidateIssueList(source, args) {
        args.IsValid = $('input[id^="IssueList"]:checked').length || $('input[id^="FollowUpList"]:checked').length;
    }
</script>