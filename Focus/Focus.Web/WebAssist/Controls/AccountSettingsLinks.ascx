﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountSettingsLinks.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.AccountSettingsLinks" %>
<p class="pageNav">
  <asp:PlaceHolder id="AssistAuthenticationLinks" runat="server">
	  <asp:PlaceHolder runat="server" ID="ChangePasswordPlaceHolder">
    <a id="AssistChangePasswordLink" runat="server"><%= HtmlLocalise("Global.AssistChangePassword.LinkText", "Change password")%></a>
    |
		</asp:PlaceHolder>
    <a id="AssistContactInformationLink" runat="server"><%= HtmlLocalise("Global.AssistContactInformation.LinkText", "Update contact information")%></a>
    |
  </asp:PlaceHolder>
  <a id="AssistSavedMessagesLink" runat="server"><%= HtmlLocalise("Global.AssistSavedMessages.LinkText", "Manage saved messages")%></a>
  |
  <a id="AssistSavedSearchesLink" runat="server"><%= HtmlLocalise("Global.AssistSavedSearches.LinkText", "Manage saved searches")%></a>
  <asp:PlaceHolder id="EmailAlertsHolder" runat="server">
    |
    <a id="AssistEmailAlertsLink" runat="server"><%= HtmlLocalise("Global.AssistEmailAlerts.LinkText", "Email Alerts")%></a>
  </asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="SSOChangePasswordPlaceHolder" Visible="false">
	|
  <a id="SSOAssistChangePasswordLink" runat="server"><%= HtmlLocalise("Global.AssistChangePassword.LinkText", "Change password")%></a>
	</asp:PlaceHolder>
	<asp:PlaceHolder id="SSOAssistContactInformation" runat="server" Visible="false">
	|
  <a id="SSOAssistContactInformationLink" runat="server"><%= HtmlLocalise("Global.AssistContactInformation.LinkText", "Update contact information")%></a>
  </asp:PlaceHolder>
</p>