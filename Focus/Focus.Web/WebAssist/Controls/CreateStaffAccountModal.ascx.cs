﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Controllers.WebAssist.Controls;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class CreateStaffAccountModal : UserControlBase
	{
		//TODO: This needs to be a config setting
		protected string PhoneNumberMask = "(999) 999-9999";

		protected CreateStaffAccountModalContentController PageController { get { return PageControllerBaseProperty as CreateStaffAccountModalContentController; } }

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		public override IPageController RegisterPageController()
		{
			return new CreateStaffAccountModalContentController(App);
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				PhoneNumberMaskedEdit.Mask = App.Settings.PhoneNumberMaskPattern;
				PhoneNumberRegEx.ValidationExpression = App.Settings.PhoneNumberRegExPattern;
			}
		}

		/// <summary>
		/// Shows this instance.
		/// </summary>
		public void Show()
		{
			Bind();
			LocaliseUI();
			CreateStaffAccountModalPopup.Show();
		}

		/// <summary>
		/// Handles the Click event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Click(object sender, EventArgs e)
		{
			ResetError();

			var model = new CreateAssistUserModel
			            	{
											UserEmailAddress = EmailAddressTextBox.TextTrimmed(),
											UserPerson = new PersonDto{FirstName = FirstNameTextBox.TextTrimmed(),
			            															MiddleInitial = InitialTextBox.TextTrimmed(),
			            															LastName = LastNameTextBox.TextTrimmed(),
			            															TitleId = TitleDropDown.SelectedValueToLong(),
			            															JobTitle = JobTitleTextBox.TextTrimmed()},
			            		UserPhone = PhoneNumberTextBox.TextTrimmed(),
								AccountExternalId = ExternalIdTextBox.TextTrimmed()
			            	};

			var controllerResult = PageController.CreateAssistUser(model);

			var errorResult = controllerResult as ErrorResult;

			if (errorResult.IsNotNull() && errorResult.ErrorMessage.IsNotNullOrEmpty())
			{
				ErrorLabel.Text = errorResult.ErrorMessage;
				ErrorLabel.Visible = true;

				CreateStaffAccountModalPopup.Show();

				return;
			}

			if (Handle(controllerResult))
			{
				ResetForm();

				CreateStaffAccountModalPopup.Hide(); 
			}
		}

		/// <summary>
		/// Handles the Click event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Click(object sender, EventArgs e)
		{
			ResetForm();

			CreateStaffAccountModalPopup.Hide();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			TitleDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Titles), null, CodeLocalise("Global.PersonalTitlez.TopDefault", "- select -"));
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CancelButton.Text = CodeLocalise("Global.Cancel.Label", "Cancel");
			SaveButton.Text = CodeLocalise("Global.Save.Label", "Save");

			EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
      EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address format is invalid");
			ConfirmEmailAddressRequired.ErrorMessage = CodeLocalise("ConfirmEmailAddressRequired.ErrorMessage", "Confirm email address is required");
			ConfirmEmailAddressCompare.ErrorMessage = CodeLocalise("ConfirmEmailAddressCompare.ErrorMessage", "Email addresses must match");
			FirstNameRequired.ErrorMessage = CodeLocalise("FirstName.RequiredErrorMessage", "First name is required.");
			LastNameRequiredField.ErrorMessage = CodeLocalise("LastName.RequiredErrorMessage", "Last name is required.");
			PersonalTitleRequired.ErrorMessage = CodeLocalise("PersonalTitle.RequiredErrorMessage", "Title is required.");
			PhoneNumberRequired.ErrorMessage = CodeLocalise("PhoneNumberValidator.ErrorMessage", "Phone number is required");
			PhoneNumberRegEx.ErrorMessage = CodeLocalise("PhoneNumberRegEx.ErrorMessage", "Incorrect phone number format");

			EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
		}

		/// <summary>
		/// Resets the form.
		/// </summary>
		private void ResetForm()
		{
			EmailAddressTextBox.Text = string.Empty;
			ConfirmEmailAddressTextBox.Text = string.Empty;
			FirstNameTextBox.Text = string.Empty;
			InitialTextBox.Text = string.Empty;
			LastNameTextBox.Text = string.Empty;
			TitleDropDown.SelectedIndex = 0;
			JobTitleTextBox.Text = string.Empty;
			ExternalIdTextBox.Text = string.Empty;
			PhoneNumberTextBox.Text = string.Empty;

			ResetError();
		}

		/// <summary>
		/// Resets the error.
		/// </summary>
		private void ResetError()
		{
			ErrorLabel.Text = string.Empty;
			ErrorLabel.Visible = false;
		}
	}
}