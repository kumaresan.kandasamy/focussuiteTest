﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComplianceCriteriaBuilder.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ComplianceCriteriaBuilder" %>

<asp:Panel ID="ComplianceHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<asp:Image ID="ComplianceHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="Compliance Header Image" />
	&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Compliance.Label", "Compliance")%></a></span>
</asp:Panel>
<asp:Panel ID="CompliancePanel" runat="server">
	<table>
		<tr>
			<td width="200px"><%= HtmlLabel("ForeignLabourCertificationJobs.Label", "Foreign labor certification jobs")%></td>
			<td><asp:Button ID="AddForeignLabourCertificationJobsFilterButton" runat="server" CssClass="button3" OnClick="AddForeignLabourCertificationJobsFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
		<tr>
			<td><%= HtmlLabel("ForeignLabourCertificationJobsWithGoodMatches.Label", "Foreign labor certification jobs with good matches")%></td>
			<td><asp:Button ID="AddForeignLabourCertificationJobsWithGoodMatchesFilterButton" runat="server" CssClass="button3" OnClick="AddForeignLabourCertificationJobsWithGoodMatchesFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
		<tr>
			<td><%= HtmlLabel("CourtOrderedAffirmativeAction.Label", "Court-ordered affirmative action")%></td>
			<td><asp:Button ID="AddCourtOrderedAffirmativeActionFilterButton" runat="server" CssClass="button3" OnClick="AddCourtOrderedAffirmativeActionFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
		<tr>
			<td><%= HtmlLabel("FederalContractJobs.Label", "Federal contract jobs")%></td>
			<td><asp:Button ID="AddFederalContractJobsFilterButton" runat="server" CssClass="button3" OnClick="AddFederalContractJobsFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
	</table>
</asp:Panel>
<act:CollapsiblePanelExtender ID="CompliancePanelExtender" runat="server" 
														TargetControlID="CompliancePanel" 
														ExpandControlID="ComplianceHeaderPanel" 
														CollapseControlID="ComplianceHeaderPanel" 
														Collapsed="true" 
														ImageControlID="ComplianceHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="CompliancePanelBehavior" />