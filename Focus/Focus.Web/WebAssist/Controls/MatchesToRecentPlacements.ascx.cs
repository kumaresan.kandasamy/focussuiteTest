﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.BusinessUnit;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersReadOnly)]
	public partial class MatchesToRecentPlacements : UserControlBase
	{
		private int _matchesCount;
		
		// track items in repeater so that last one gets hide link
		private int _numberOfMatchedCandidates;
		private int _bindCtr;

		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		protected JobDto Job
		{
			get { return GetViewStateValue<JobDto>("JobPreviewModal:Job"); }
			set { SetViewStateValue("JobPreviewModal:Job", value); }
		}

		/// <summary>
		/// Gets or sets the employer criteria.
		/// </summary>
		private BusinessUnitCriteria BusinessUnitCriteria
		{
			get { return GetViewStateValue<BusinessUnitCriteria>("MatchesToRecentPlacements:BusinessUnitCriteria"); }
			set { SetViewStateValue("MatchesToRecentPlacements:BusinessUnitCriteria", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				OfficeFilterLabel.Visible = OfficeFilter.Visible = App.Settings.OfficesEnabled;
				LocaliseUI();
				BindRecentMatchesList();
				ApplyBranding();
			}
		}

		/// <summary>
		/// Handles the DataBound event of the ItemsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ItemsList_DataBound(object sender, ListViewItemEventArgs e)
		{
			((LinkButton)e.Item.FindControl("CompareResumesLinkButton")).Text = CodeLocalise("CompareResumes.Text", "Compare resumes");
			((LinkButton)e.Item.FindControl("ContactEmployerLinkButton")).Text = CodeLocalise("ContactEmployer.Text", "Contact #BUSINESS#:LOWER");

			var match = (EmployerRecentlyPlacedView)e.Item.DataItem;
			
			var openPositionsLinkButton = (LinkButton) e.Item.FindControl("OpenPositionsLinkButton");
			var openPositionsBr = (HtmlControl) e.Item.FindControl("OpenPositionsBr");
						
			_numberOfMatchedCandidates = match.Candidates.Count();

			if (_numberOfMatchedCandidates > 0)
			{
				var firstCandidate = match.Candidates[0];

				var scoreImage = (Image) e.Item.FindControl("ReferralScoreImage");
				BindScore(scoreImage, firstCandidate.Score);

				((Literal) e.Item.FindControl("FirstMatch")).Text = match.Candidates[0].CandidateName;

				// Hide or show the "View matches to open positions" link depending 
				// on if there are any to view or not
				if (!firstCandidate.HasMatchesToOpenPositions)
					openPositionsLinkButton.Visible = openPositionsBr.Visible = false;
				else
					openPositionsLinkButton.Text = CodeLocalise("OpenPositions.Text", "View matches to open positions");

				if (_numberOfMatchedCandidates > 1)
				{
					var extraMatchesLink = (Literal) e.Item.FindControl("ExtraMatchesLink");
					extraMatchesLink.Text = String.Format("<a class=\"ShowLink{0}\" href=\"javascript:void(0)\" onclick=\"ShowRepeater({0})\">+ {1} other{2}</a>", match.Id, _numberOfMatchedCandidates - 1, _numberOfMatchedCandidates == 2 ? "" : "s");

					_bindCtr = 0;

					var repeater = (Repeater) e.Item.FindControl("AdditionalMatchesRepeater");
					repeater.DataSource = match.Candidates.GetRange(1, _numberOfMatchedCandidates - 1);
					repeater.DataBind();
				}
			}
			else
				openPositionsLinkButton.Visible = openPositionsBr.Visible = false;
		}

		/// <summary>
		/// Handles the DataBound event of the AdditionalMatchesRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void AdditionalMatchesRepeater_DataBound(object sender, RepeaterItemEventArgs e)
		{
			((LinkButton)e.Item.FindControl("CompareResumesOthersLinkButton")).Text = CodeLocalise("CompareResumes.Text", "Compare resumes");
			((LinkButton)e.Item.FindControl("ContactEmployerOthersLinkButton")).Text = CodeLocalise("ContactEmployer.Text", "Contact #BUSINESS#:LOWER");

			var additionalMatch = (EmployerRecentlyPlacedView.Candidate)e.Item.DataItem;
			
			var openPositionsLinkButton = (LinkButton)e.Item.FindControl("OpenPositionsOthersLinkButton");
			var openPositionsBr = (HtmlControl)e.Item.FindControl("OpenPositionsOthersBr");

			// Hide or show the "View matches to open positions" link depending 
			// on if there are any to view or not
			if (!additionalMatch.HasMatchesToOpenPositions)
				openPositionsLinkButton.Visible = openPositionsBr.Visible = false;
			else
				openPositionsLinkButton.Text = CodeLocalise("OpenPositions.Text", "View matches to open positions");

			var scoreImage = (Image)e.Item.FindControl("AdditionalScoreImage");
			BindScore(scoreImage, additionalMatch.Score);

			((Literal) e.Item.FindControl("AdditionalMatch")).Text = additionalMatch.CandidateName;

			if (++_bindCtr == _numberOfMatchedCandidates -1)
				((Literal)e.Item.FindControl("LessMatchesLink")).Text = String.Format("<a href=\"javascript:void(0)\" onclick=\"HideRepeater({0})\">- hide {1} name{2}</a>", additionalMatch.RecentlyPlacedId, _numberOfMatchedCandidates - 1, _numberOfMatchedCandidates == 2 ? "" : "s");
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private static void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = OldApp_RefactorIfFound.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString();

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
      FilterButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
		}

		/// <summary>
		/// Formats the recent matches list sorting images.
		/// </summary>
		private void FormatRecentMatchesListSortingImages()
		{
			// Reset image URLs
			((ImageButton)RecentMatchesList.FindControl("EmployerNameAscButton")).ImageUrl =
				((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();

			((ImageButton)RecentMatchesList.FindControl("EmployerNameDescButton")).ImageUrl =
				((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton)RecentMatchesList.FindControl("EmployerNameAscButton")).Enabled =
				((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameAscButton")).Enabled =
				((ImageButton)RecentMatchesList.FindControl("EmployerNameDescButton")).Enabled =
				((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameDescButton")).Enabled = true;

			switch (BusinessUnitCriteria.OrderBy.ToLower())
			{
				case Constants.SortOrders.BusinessUnitNameAsc:
					((ImageButton)RecentMatchesList.FindControl("EmployerNameAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)RecentMatchesList.FindControl("EmployerNameAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.BusinessUnitNameDesc:
					((ImageButton)RecentMatchesList.FindControl("EmployerNameDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)RecentMatchesList.FindControl("EmployerNameDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.RecentPlacementNameAsc:
					((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.RecentPlacementNameDesc:
					((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameDescButton")).Enabled = false;
					break;
			}
		}

		/// <summary>
		/// Handles the Sorting event of the RecentMatchesList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void RecentMatchesList_Sorting(object sender, ListViewSortEventArgs e)
		{
			BusinessUnitCriteria.OrderBy = e.SortExpression;
			FormatRecentMatchesListSortingImages();
		}

		/// <summary>
		/// Unbinds the search.
		/// </summary>
		private void UnbindSearch()
		{
			BusinessUnitCriteria = new BusinessUnitCriteria();

			if (EmployerNameTextBox.TextTrimmed().IsNotNullOrEmpty())
				BusinessUnitCriteria.BusinessUnitName = EmployerNameTextBox.TextTrimmed();

			if (App.Settings.OfficesEnabled)
			{
				BusinessUnitCriteria.OfficeGroup = OfficeFilter.OfficeGroup;
				BusinessUnitCriteria.OfficeId = OfficeFilter.OfficeId;
			}
			else
			{
				BusinessUnitCriteria.OfficeGroup = OfficeGroup.StatewideOffices;
				BusinessUnitCriteria.OfficeId = null;
			}
		}

		/// <summary>
		/// Handles the Selecting event of the RecentMatchesDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void RecentMatchesDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (BusinessUnitCriteria.IsNull())
				BusinessUnitCriteria = new BusinessUnitCriteria { OrderBy = Constants.SortOrders.BusinessUnitNameAsc };

			e.InputParameters["criteria"] = BusinessUnitCriteria;
		}

		/// <summary>
		/// Binds the recent matches list.
		/// </summary>
		private void BindRecentMatchesList()
		{
			RecentMatchesList.DataBind();

			// Set visibility of other controls
			RecentMatchesHeaderTable.Visible = RecentMatchesList.Visible = true;
			SearchResultListPager.Visible = (SearchResultListPager.TotalRowCount > 0);

			ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} results found", SearchResultListPager.TotalRowCount.ToString());
		}

		/// <summary>
		/// Handles the Clicked event of the FilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FilterButton_Clicked(object sender, EventArgs e)
		{
			SearchResultListPager.ReturnToFirstPage();
			UnbindSearch();
			BusinessUnitCriteria.OrderBy = Constants.SortOrders.PlacementDateDesc;
			BindRecentMatchesList();

			// Set the sort images
			if (SearchResultListPager.TotalRowCount > 0) FormatRecentMatchesListSortingImages();
		}

		/// <summary>
		/// Handles the LayoutCreated event of the RecentMatchesList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void RecentMatchesList_LayoutCreated(object sender, EventArgs e)
		{
			#region Set labels

			((Literal)RecentMatchesList.FindControl("EmployerNameHeader")).Text = CodeLocalise("EmployerNameHeader.Text", "#BUSINESS#:UPPER");
			((Literal)RecentMatchesList.FindControl("RecentPlacementNameHeader")).Text = CodeLocalise("RecentPlacementNameHeader.Text", "RECENT PLACEMENT");
			((Literal)RecentMatchesList.FindControl("MatchHeader")).Text = CodeLocalise("MatchHeader.Text", "MATCH");
			((Literal)RecentMatchesList.FindControl("MatchToolTip")).Text = HtmlTooltipWithArrow("JobSeekerMatch.Tooltip", @"These are matches to resumes, not to a specific job.");
			((Literal)RecentMatchesList.FindControl("JobSeekerHeader")).Text = CodeLocalise("JobSeekerHeader.Text", "#CANDIDATETYPE#");
			((Literal)RecentMatchesList.FindControl("ActionsHeader")).Text = CodeLocalise("ActionsHeader.Text", "ACTIONS");

			((ImageButton) RecentMatchesList.FindControl("EmployerNameAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			((ImageButton)RecentMatchesList.FindControl("EmployerNameDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
			((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			((ImageButton)RecentMatchesList.FindControl("RecentPlacementNameDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			#endregion
		}

		/// <summary>
		/// Gets the matches count.
		/// </summary>
		/// <returns></returns>
		public int GetMatchesCount()
		{
			return _matchesCount;
		}

		/// <summary>
		/// Gets the matches count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetMatchesCount(BusinessUnitCriteria criteria)
		{
			return _matchesCount;
		}

		/// <summary>
		/// Gets the matches.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<EmployerRecentlyPlacedView> GetMatches(BusinessUnitCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				if(!App.Settings.OfficesEnabled) 
					criteria.OfficeGroup = OfficeGroup.StatewideOffices;

				_matchesCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;

			var matches = ServiceClientLocator.EmployerClient(App).GetEmployerRecentlyPlacedMatches(criteria);
			_matchesCount = matches.TotalCount;

			return matches;
		}

		/// <summary>
		/// Handles the Click event of the CompareResumesLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CompareResumesLinkButton_Command(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
      {
        case "CompareResumes":

		      CompareResumesModal.Details candidatePlacementResumesDetails;
		      if (e.CommandArgument.ToString().Contains(';'))
					{
						var rowsNoArray = e.CommandArgument.ToString().Split(';');
						candidatePlacementResumesDetails = GetPlacementAndJobseekerDetails(Int32.Parse(rowsNoArray[0]), (Int32.Parse(rowsNoArray[1]) + 1));
					}
					else
						candidatePlacementResumesDetails = GetPlacementAndJobseekerDetails(Convert.ToInt32(e.CommandArgument), 0);

					CompareResumesModalUc.Show(candidatePlacementResumesDetails);
				break;

        case "ViewMatchesToOpenPositions":

		      MatchesToOpenPositionsModal.Details candidateResumeDetails;
		      if (e.CommandArgument.ToString().Contains(';'))
					{
						var rowsNoArray = e.CommandArgument.ToString().Split(new[] { ';' });
						candidateResumeDetails = GetCandidateDetails(Int32.Parse(rowsNoArray[0]), (Int32.Parse(rowsNoArray[1]) + 1));
					}
					else
						candidateResumeDetails = GetCandidateDetails(Convert.ToInt32(e.CommandArgument), 0);

					MatchesToOpenPositionsModalUc.Show(candidateResumeDetails);
				break;
      }
    }

		/// <summary>
		/// Gets the placement and jobseeker details.
		/// </summary>
		/// <param name="rowNo1">The row no1.</param>
		/// <param name="rowNo2">The row no2.</param>
		/// <returns></returns>
		private CompareResumesModal.Details GetPlacementAndJobseekerDetails(int rowNo1, int rowNo2)
    {
			var candidates = (List<EmployerRecentlyPlacedView.Candidate>)(RecentMatchesList.DataKeys[rowNo1]["Candidates"]);
		
			return new CompareResumesModal.Details
				      {
								JobId = RecentMatchesList.DataKeys[rowNo1]["JobId"].AsLong(),
								JobTitle = RecentMatchesList.DataKeys[rowNo1]["JobTitle"].ToString(),
								RecentlyPlacedMatchId = candidates[rowNo2].Id,
								RecentPlacementId = long.Parse(RecentMatchesList.DataKeys[rowNo1]["PersonId"].ToString()),
								RecentPlacementName = RecentMatchesList.DataKeys[rowNo1]["CandidateName"].ToString(),
								BusinessUnitName = RecentMatchesList.DataKeys[rowNo1]["BusinessUnitName"].ToString(),
								CandidateName = candidates[rowNo2].CandidateName,
								CandidateId = long.Parse(candidates[rowNo2].PersonId.ToString()),
								Score = candidates[rowNo2].Score
							};
    }

		/// <summary>
		/// Gets the candidate details.
		/// </summary>
		/// <param name="rowNo1">The row no1.</param>
		/// <param name="rowNo2">The row no2.</param>
		/// <returns></returns>
		private MatchesToOpenPositionsModal.Details GetCandidateDetails(int rowNo1, int rowNo2)
		{
			var candidates = (List<EmployerRecentlyPlacedView.Candidate>)(RecentMatchesList.DataKeys[rowNo1]["Candidates"]);
			var employerId = (long)(RecentMatchesList.DataKeys[rowNo1]["EmployerId"]);

			return new MatchesToOpenPositionsModal.Details
			       	{
			       		CandidateName = candidates[rowNo2].CandidateName,
			       		CandidateId = Int32.Parse(candidates[rowNo2].PersonId.ToString()),
			       		EmployerId = employerId,
			       		RecentlyPlacedId = long.Parse(RecentMatchesList.DataKeys[rowNo1]["PersonId"].ToString())
			       	};
		}


		/// <summary>
		/// Handles the Command event of the ContactEmployerLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ContactEmployerLinkButton_Command(object sender, CommandEventArgs e)
		{
		  DataKey dataKey = null;
		  MatchesToOpenPositionsModal.Details candidateResumeDetails = null;

			switch (e.CommandName)
			{
				case "ContactEmployer":
          dataKey = RecentMatchesList.DataKeys[Convert.ToInt32(e.CommandArgument)];
          candidateResumeDetails = GetCandidateDetails(Convert.ToInt32(e.CommandArgument), 0);
					break;

				case "ContactEmployerOther":
					var rowsNoArray = e.CommandArgument.ToString().Split(new[] { ';' });
          dataKey = RecentMatchesList.DataKeys[Convert.ToInt32(rowsNoArray[0])];
          candidateResumeDetails = GetCandidateDetails(Int32.Parse(rowsNoArray[0]), (Int32.Parse(rowsNoArray[1]) + 1));
					break;
			}

      if (dataKey != null)
      {
        GetSelectedJob(dataKey["JobId"].AsLong());
        if (Job.EmployeeId != null)
          Response.Redirect(UrlBuilder.ContactEmployer(Job.BusinessUnitId.GetValueOrDefault(0), candidateResumeDetails.CandidateId, candidateResumeDetails.RecentlyPlacedId));
      }
		}

		/// <summary>
		/// Gets the selected job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		private void GetSelectedJob(long jobId)
		{
			Job = ServiceClientLocator.JobClient(App).GetJob(jobId);
		}

		/// <summary>
		/// Handles the Completed event of the CompareResumesModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Modal_Completed(object sender, EventArgs e)
		{
			RecentMatchesList.DataBind();
		}

		private void ApplyBranding()
		{

		}
		
	}
}