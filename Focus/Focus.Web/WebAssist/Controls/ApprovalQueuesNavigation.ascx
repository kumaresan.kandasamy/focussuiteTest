﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApprovalQueuesNavigation.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ApprovalQueuesNavigation" %>

<p class="pageNav">
	<% 
		if (IsJobSeekerReferralApproverUser() || IsJobSeekerReferralApprovalViewerUser())
		{
			if (!IsJobSeekerReferralsPage())
  		{%>
	    <asp:Literal runat="server" ID="LocaliseAssistJobSeekerReferrals"/>
				<a href="<%=UrlBuilder.JobSeekerReferrals()%>"><%= HtmlLocalise("Global.AssistJobSeekerReferrals.LinkText", "Approve referral requests")%></a>
			<% }
  		else
  		{ %>
				<strong><%= HtmlLocalise("Global.AssistJobSeekerReferrals.LinkText", "Approve referral requests")%></strong>
			<% }
		}
		
		if (App.Settings.TalentModulePresent && (IsEmployerAccountApproverUser() || IsEmployerAccountApprovalViewerUser()))
		{
			if (IsJobSeekerReferralApproverUser() || IsJobSeekerReferralApprovalViewerUser())
			{%>
				&nbsp;|&nbsp;
			<%}
			if (!IsEmployerReferralsPage())
			{%>
      <asp:Literal runat="server" ID="LocaliseAssistEmployerReferrals"/>
				<a href="<%= UrlBuilder.EmployerReferrals() %>"><%= HtmlLocalise("Global.AssistEmployerReferrals.LinkText", "Approve #BUSINESS#:LOWER account requests")%></a>
			<% }
			else
			{ %>
				<strong><%= HtmlLocalise("Global.AssistEmployerReferrals.LinkText", "Approve #BUSINESS#:LOWER account requests")%></strong>
			<% }
		}
		
		if(IsPostingApproverUser() || IsPostingApprovalViewerUser())
		{
			if (IsJobSeekerReferralApproverUser() || IsJobSeekerReferralApprovalViewerUser() || IsEmployerAccountApproverUser() || IsEmployerAccountApprovalViewerUser())
			{%>
				&nbsp;|&nbsp;
			<%}	
			if (!IsPostingReferralsPage())
			{%>
        <asp:Literal runat="server" ID="LocaliseAssistPostingReferrals"/>
				<a href="<%= UrlBuilder.PostingReferrals() %>"><%= HtmlLocalise("Global.AssistPostingReferrals.LinkText", "Approve #POSTINGTYPE# postings")%></a>
			<% }
			else
			{ %>
				<strong><%= HtmlLocalise("Global.AssistPostingReferrals.LinkText", "Approve #POSTINGTYPE# postings")%></strong>
			<% }
		}
	%>
</p>