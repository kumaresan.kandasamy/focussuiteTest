﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Core.Criteria.Employee;

namespace Focus.Web.WebAssist.Controls
{
	public partial class CriteriaBuilder : UserControlBase
	{
		public string Title { get; set; }
		public string Instructions { get; set; }
		public string CommandButtonText { get; set; }
		public Criterias CriteriaBuildersToShow
		{
			get { return GetViewStateValue<Criterias>("CriteriaBuilder:CriteriaBuildersToShow"); }
			set { SetViewStateValue("CriteriaBuilder:CriteriaBuildersToShow", value); }
		}

		private List<CriteriaBuilderControlBase.CriteriaElement> _criteria
		{
			get { return App.GetSessionValue<List<CriteriaBuilderControlBase.CriteriaElement>>("CriteriaBuilder:CriteriaText"); }
			set { App.SetSessionValue("CriteriaBuilder:CriteriaText", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			CommandButton.Text = CommandButtonText;

			if (!IsPostBack)
			{
				SetCriteriaBuilderVisibility();
				Bind();
			  if (_criteria == null)
			    _criteria = new List<CriteriaBuilderControlBase.CriteriaElement>();
			}
		}

		/// <summary>
		/// Handles the Command event of the ActiveCriteriaListRemoveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ActiveCriteriaListRemoveButton_Command(object sender, CommandEventArgs e)
		{
			var criteriaElementIndex = Convert.ToInt32(e.CommandArgument);

			var criteriaElement = _criteria[criteriaElementIndex];
			_criteria.RemoveAt(criteriaElementIndex);

			switch (criteriaElement.Type)
			{
				case CriteriaBuilderControlBase.CriteriaElementType.LocationRadius:
					LocationCriteria.ClearLocationRadiusFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.LocationCounty:
					LocationCriteria.ClearCountyFilter(criteriaElement.Parameters[0]);
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.LocationOffice:
					LocationCriteria.ClearOfficeFilter(criteriaElement.Parameters[0]);
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.LocationWorkforceInvestmentBoard:
					LocationCriteria.ClearWorkforceInvestmentBoardFilter(criteriaElement.Parameters[0]);
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.JobCharacteristicsOccupationGroup:
					JobCharacteristicsCriteria.ClearOccupationGroupFilter(criteriaElement.Parameters[0]);
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.JobCharacteristicsOccupationTitle:
					JobCharacteristicsCriteria.ClearOccupationTitleFilter(criteriaElement.Parameters[0]);
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.JobCharacteristicSalary:
					JobCharacteristicsCriteria.ClearSalaryFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.JobCharacteristicStatus:
					JobCharacteristicsCriteria.ClearJobStatusFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.JobCharacteristicLevelOfEducation:
					JobCharacteristicsCriteria.ClearEducationFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.JobCharacterisiticSearchTerms:
					JobCharacteristicsCriteria.ClearSearchTermFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsSector:
					EmployerCharacteristicsCriteria.ClearSectorFilter(criteriaElement.Parameters[0]);
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsWorkOpportuniesTaxCredit:
					EmployerCharacteristicsCriteria.ClearWorkOpportuniesTaxCreditFilter((WorkOpportunitiesTaxCreditCategories)Enum.Parse(typeof(WorkOpportunitiesTaxCreditCategories), criteriaElement.Parameters[0]));
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsFederalEmployerIdentificationNumber:
					EmployerCharacteristicsCriteria.ClearFederalEmployerIdentificationNumberFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsName:
					EmployerCharacteristicsCriteria.ClearNameFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsJobTitle:
					EmployerCharacteristicsCriteria.ClearJobTitleFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsGreenJob:
					EmployerCharacteristicsCriteria.ClearGreenJobFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsITEmployer:
					EmployerCharacteristicsCriteria.ClearITEmployerFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsHealthcareEmployer:
					EmployerCharacteristicsCriteria.ClearHealthcareEmployerFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsBiotechEmployer:
					EmployerCharacteristicsCriteria.ClearBiotechEmployerFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsAccountCreationDate:
					EmployerCharacteristicsCriteria.ClearAccountCreationDateFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.EmployerCharacteristicsScreeningAssistance:
					EmployerCharacteristicsCriteria.ClearScreenigAssistanceFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.ComplianceForeignLabourCertificationJobs:
					ComplianceCriteria.ClearForeignLabourCertificationJobsFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.ComplianceForeignLabourCertificationJobsWithGoodMatches:
					ComplianceCriteria.ClearForeignLabourCertificationJobsWithGoodMatchesFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.ComplianceCourtOrderedAffirmativeAction:
					ComplianceCriteria.ClearCourtOrderedAffirmativeActionFilter();
					break;

				case CriteriaBuilderControlBase.CriteriaElementType.ComplianceFederalContractJobs:
					ComplianceCriteria.ClearFederalContractJobsFilter();
					break;
			}

			Bind();
		}

		/// <summary>
		/// Handles the Click event of the CommandButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CommandButton_Click(object sender, EventArgs e)
		{
			var locationFilter = ((CriteriaBuildersToShow & Criterias.Location) == Criterias.Location) ? LocationCriteria.Unbind() : null;
			var jobCharacteristicsFilter = ((CriteriaBuildersToShow & Criterias.JobCharacteristics) == Criterias.JobCharacteristics) ? JobCharacteristicsCriteria.Unbind() : null;
			var employerCharacteristicsFilter = ((CriteriaBuildersToShow & Criterias.EmployerCharacteristics) == Criterias.EmployerCharacteristics) ? EmployerCharacteristicsCriteria.Unbind() : null;
			var complianceFilter = ((CriteriaBuildersToShow & Criterias.Compliance) == Criterias.Compliance) ? ComplianceCriteria.Unbind() : null;

			OnCommandButtonClicked(new CommandButtonClickedEventArgs(locationFilter, jobCharacteristicsFilter, employerCharacteristicsFilter, complianceFilter));
		}

		/// <summary>
		/// Criterias the added.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.CriteriaBuilderControlBase.CriteriaAddedEventArgs"/> instance containing the event data.</param>
		protected void CriteriaAdded(object sender, CriteriaBuilderControlBase.CriteriaAddedEventArgs e)
		{
			if(e.Criteria.OverwriteExistingValue)
			{
				foreach (var criteriaElement in _criteria.Where(criteriaElement => criteriaElement.Type == e.Criteria.Type))
				{
					_criteria.Remove(criteriaElement);
					break;
				}
			}
			_criteria.Add(e.Criteria);
			Bind();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			ActiveCriteriaList.DataSource = _criteria;
			ActiveCriteriaList.DataBind();
		}

		/// <summary>
		/// Sets the criteria builder visibility.
		/// </summary>
		private void SetCriteriaBuilderVisibility()
		{
			LocationCriteria.Visible = ((CriteriaBuildersToShow & Criterias.Location) == Criterias.Location);
			JobCharacteristicsCriteria.Visible = ((CriteriaBuildersToShow & Criterias.JobCharacteristics) == Criterias.JobCharacteristics);
			EmployerCharacteristicsCriteria.Visible = ((CriteriaBuildersToShow & Criterias.EmployerCharacteristics) == Criterias.EmployerCharacteristics);
			ComplianceCriteria.Visible = ((CriteriaBuildersToShow & Criterias.Compliance) == Criterias.Compliance);
		}

		[Flags]
		public enum Criterias
		{
			None = 0,
			Location = 1,
			JobCharacteristics = 2,
			EmployerCharacteristics = 4,
			Compliance = 8
		}

		#region Events

		public event CommandButtonClickedHandler CommandButtonClicked;

		/// <summary>
		/// Raises the <see cref="E:PoolSearchRequest"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.WebTalent.Controls.PoolSearch.PoolSearchRequestEventArgs"/> instance containing the event data.</param>
		protected virtual void OnCommandButtonClicked(CommandButtonClickedEventArgs e)
		{
			if (CommandButtonClicked != null)
				CommandButtonClicked(this, e);
		}

		#endregion

		#region Delegates

		public delegate void CommandButtonClickedHandler(object o, CommandButtonClickedEventArgs e);
		
		#endregion

		#region EventArgs

		/// <summary>
		/// 
		/// </summary>
		public class CommandButtonClickedEventArgs : EventArgs
		{
			public readonly ReportLocationCriteria LocationFilter;
			public readonly JobCharacteristicsCriteria JobCharacteristicsFilter;
			public readonly EmployerCharacteristicsCriteria EmployerCharacteristicsFilter;
			public readonly ComplianceCriteria ComplianceFilter;

			public CommandButtonClickedEventArgs(ReportLocationCriteria locationFilter, JobCharacteristicsCriteria jobCharacteristicsFilter, EmployerCharacteristicsCriteria employerCharacteristicsFilter, 
																						ComplianceCriteria complianceFilter)
			{
				LocationFilter = locationFilter;
				JobCharacteristicsFilter = jobCharacteristicsFilter;
				EmployerCharacteristicsFilter = employerCharacteristicsFilter;
				ComplianceFilter = complianceFilter;
			}
		}

		#endregion
	}
}