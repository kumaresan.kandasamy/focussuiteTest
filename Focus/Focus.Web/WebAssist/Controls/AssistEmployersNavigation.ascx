﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssistEmployersNavigation.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.AssistEmployersNavigation" %>
<%@ Import Namespace="Focus" %>

<p class="pageNav">
			
     <%
       if (!(IsJobOrderDashBoardPage() || IsJobWizardPage() || IsTalentPoolPage()))
  	{%>
			<a href="<%=UrlBuilder.JobOrderDashboard()%>"><%= HtmlLocalise("Global.JobOrderDashboard.LinkText", "#EMPLOYMENTTYPE# dashboard")%></a>
		<% }
  	else
    { %>
			<strong><%= HtmlLocalise("Global.JobOrderDashboard.LinkText", "#EMPLOYMENTTYPE# dashboard")%></strong>
	  <% }
      
			if (OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Workforce && IsJobDevelopmentManager())
			{
                if (!IsDevelopmentPage())
                {%>
			    &nbsp;|&nbsp;<a href="<%= UrlBuilder.JobDevelopment() %>"><%= HtmlLocalise("Global.JobDevelopment.LinkText", "Business Development") %></a>
		        <% }
                else
                { %>
			    &nbsp;|&nbsp;<strong><%= HtmlLocalise("Global.JobDevelopment.LinkText", "Business Development")%></strong>
	            <% }
            }
      
		
     if (!IsEmployersPage())
  	{%>
			&nbsp;|&nbsp;<a href="<%=UrlBuilder.AssistEmployers()%>"><asp:Label runat="server" ID="ActiveAssistEmployersLabel"></asp:Label></a>
		<% }
  	else
  	{ %>
			&nbsp;|&nbsp;<strong><asp:Label runat="server" ID="InActiveAssistEmployersLabel"></asp:Label></strong>
		<% }

			if (IsAssistEmployersAdministrator() && HasAssistEmployersSendMessagesRole())
		{
			if (!IsMessageEmployersPage())
			{%>
				&nbsp;|&nbsp;<a href="<%= UrlBuilder.MessageEmployers() %>"><%= HtmlLocalise("Global.MessageEmployers.LinkText", "Send messages")%></a>
			<% }
			else
			{ %>
				&nbsp;|&nbsp;<strong><%= HtmlLocalise("Global.MessageEmployers.LinkText", "Send messages")%></strong>
			<% }
		}
	%>
</p>