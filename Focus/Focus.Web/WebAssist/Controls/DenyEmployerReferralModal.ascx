﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DenyEmployerReferralModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.DenyEmployerReferralModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel" 
												PopupDragHandleControlID="DenyEmployerReferralHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" 
												><Animations><OnShown><ScriptAction Script="BindDenyEmployerReferralOtherReason();" />  </OnShown></Animations></act:ModalPopupExtender>

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Width="600px" Style="display: none">
	<h2 data-modal='title' id="DenyEmployerReferralHeader"><focus:LocalisedLabel runat="server" ID="TitleLabel" /></h2>
  <p><focus:LocalisedLabel runat="server" ID="SummaryLabel" /></p>
	<p>
	  <asp:Literal ID="TopEmailBodyLiteral" runat="server" ClientIDMode="Static" />
    <asp:PlaceHolder runat="server" ID="FreeTextDenialReasonPlaceholder" Visible="False">
	    <%= HtmlInFieldLabel("DenialReasonsTextBox", "DenialReasons.InlineLabel", "type reasons for denial", 600)%>
		    <asp:TextBox ID="DenialReasonsTextBox" runat="server" TextMode="MultiLine" Rows="4" ClientIDMode="Static" Width="450px" />
		    <asp:RequiredFieldValidator ID="DenialReasonsRequired" runat="server" ControlToValidate="DenialReasonsTextBox" 
          SetFocusOnError="true" CssClass="error" ValidationGroup="DenyEmployer" Display="Dynamic" /><br />
    </asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="CheckBoxDenialReasonsPlaceholder" Visible="False">
      <asp:Panel ID="panDenialReasons" runat="server" CssClass="checkbox-panel" ClientIDMode="Static" >
        <asp:CheckBoxList ID="cblDenialReasons" runat="server" CssClass="checkBoxListTable" RepeatDirection="Vertical" ClientIDMode="Static" ></asp:CheckBoxList>
        <asp:CustomValidator ID="DenialReasonsCheckboxRequired" runat="server" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="DenyEmployer" ClientValidationFunction="validateDenialReasons" ValidateEmptyText="true" />
        <%= HtmlInFieldLabel("OtherDenialReasonTextBox", "OtherDenialReason.InlineLabel", "Your reason for denying the posting", 400)%>
        <asp:TextBox runat="server" ID="OtherDenialReasonTextBox" TextMode="MultiLine" Rows="4" Width="550px" Enabled="False" ClientIDMode="Static" />
        <p>
          <asp:RequiredFieldValidator ID="OtherDeniedReasonRequired" runat="server" ControlToValidate="OtherDenialReasonTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="DenyEmployer" Display="Dynamic" Enabled="False"/>
          <asp:CustomValidator runat="server" ID="OtherDenialReasonValidator" ClientValidationFunction="ValidateOtherDenialReasonLength" CssClass="error" ControlToValidate="OtherDenialReasonTextBox" ValidationGroup="DenyEmployer" ErrorMessage="Error" SetFocusOnError="True"/>
        </p>
      </asp:Panel>
    </asp:PlaceHolder>
		<asp:Literal ID="BottomEmailBodyLiteral" runat="server" ClientIDMode="Static" />
	</p>
	<p><asp:CheckBox ID="BCCMeCheckBox" runat="server" /></p>
	<p>
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" CausesValidation="False"/>
		<asp:Button ID="DenyAndSendMessageButton" runat="server" CssClass="button3" OnClick="DenyAndSendMessageButton_Clicked" ValidationGroup="DenyEmployer" />		
	</p>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />

<script type="text/javascript">

	function validateDenialReasons(src, args) {
		args.IsValid = $('#cblDenialReasons input:checkbox').is(':checked');
  }

  function ValidateOtherDenialReasonLength(sender, args) {
    var description = document.getElementById(sender.controltovalidate).value;
    args.IsValid = description.length <= 250;
  }

	function BindDenyEmployerReferralOtherReason() {
		$('#panDenialReasons input:checkbox:last').on('change', function () {
			if ($(this).is(':checked')) {
				$('#OtherDenialReasonTextBox').removeProp('disabled');
				ValidatorEnable($("#<%= OtherDeniedReasonRequired.ClientID %>")[0], true);
			} else {
				ValidatorEnable($("#<%= OtherDeniedReasonRequired.ClientID %>")[0], false);
				$('#OtherDenialReasonTextBox').val('').prop('disabled', 'disabled');
			}
		});

	}

</script>