﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class DocumentModal : UserControlBase
	{
		public delegate void CompletedHandler(object sender);
		public event CompletedHandler Completed;

		public class DocumentModalEventArgs : EventArgs
		{
			public ActionTypes ActionType { get; set; }
		}

		#region Properties

		public long DocumentId
		{
			get { return GetViewStateValue<long>("DocumentModal:DocumentId"); }
			set { SetViewStateValue("DocumentModal:DocumentId", value); }
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();

				BindModulesCheckBoxList();
				BindGroupDropdown();
				BindCategoryDropDown();

				DownloadDocumentImageButton.ImageUrl = UrlBuilder.DownloadIcon();
				ReplaceDocumentImageButton.ImageUrl = UrlBuilder.DeleteIcon();
			}

			var filePattern = string.Join("|", App.Settings.DocumentFileTypes.Split(','));

			FileUploadRegEx.ValidationExpression = @"^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))\.(" + filePattern + @")$";
		}

		/// <summary>
		/// Shows this instance.
		/// </summary>
		public void Show()
		{
			Initialise();

			ShowUpload(true);

			DocumentModalPopup.Show();
		}

		/// <summary>
		/// Shows the specified document id.
		/// </summary>
		/// <param name="documentId">The document id.</param>
		public void Show(long documentId)
		{
			DocumentId = documentId;

			Initialise();

			ShowUpload(false);

			UploadButton.Text = CodeLocalise("UploadButton.Text", "Update");

			var document = ServiceClientLocator.CoreClient(App).GetDocument(documentId);

			Bind(document);
			DocumentModalPopup.Show();
		}

		/// <summary>
		/// Initialises this instance.
		/// </summary>
		private void Initialise()
		{
			IdHiddenField.Value = "";
			TitleTextBox.Text = String.Empty;
			DescriptionTextBox.Text = String.Empty;
			CategoryDropDown.SelectedValue = String.Empty;
			GroupDropdown.Items.Clear();
			GroupDropdown.SelectedValue = String.Empty;

			foreach (ListItem item in ModuleCheckBoxList.Items)
			{
				item.Selected = false;
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			UploadButton.Text = CodeLocalise("UploadButton.Text", "Upload");

			TitleRequired.ErrorMessage = CodeLocalise("TitleRequired.ErrorMessage", "Title is required");
			GroupRequired.ErrorMessage = CodeLocalise("GroupRequired.ErrorMessage", "Group is required");
			CategoryRequired.ErrorMessage = CodeLocalise("CategoryRequired.ErrorMessage", "Category is required");
			DescriptionRegEx.ErrorMessage = CodeLocalise("DescriptionRegEx.ErrorMessage", "Description can only be 2000 characters");
			DocumentUploadRequired.ErrorMessage = CodeLocalise("DocumentUploadRequired.ErrorMessage", "A document must be selected");
			ModuleRequired.ErrorMessage = CodeLocalise("ModuleRequired.ErrorMessage", "A module must be selected");
			FileUploadRegEx.ErrorMessage = CodeLocalise("FileUploadRegEx.ErrorMessage", "Only documents of the type {0} can be uploaded", App.Settings.DocumentFileTypes);

			DownloadDocumentImageButton.AlternateText = DownloadDocumentImageButton.ToolTip = CodeLocalise("DownloadDocumentImageButton.ToolTip", "Download document");
			ReplaceDocumentImageButton.AlternateText =  ReplaceDocumentImageButton.ToolTip = CodeLocalise("ReplaceDocumentImageButton.ToolTip", "Replace document");
		}

		/// <summary>
		/// Shows the upload.
		/// </summary>
		/// <param name="showUpload">if set to <c>true</c> [show upload].</param>
		private void ShowUpload(bool showUpload)
		{
			UploadDocumentDiv.Attributes.Add("style","display:" + (showUpload ? "block" : "none"));
			DocumentUploadRequired.Enabled = showUpload;
			FileNameDiv.Visible = !showUpload;

			EditIconsDiv.Attributes.Add("style","display:" + (showUpload ? "none" : "block"));
		}

		/// <summary>
		/// Binds the specified document.
		/// </summary>
		/// <param name="document">The document.</param>
		private void Bind(DocumentDto document)
		{
			IdHiddenField.Value = document.Id.ToString();

			TitleTextBox.Text = document.Title;
			DescriptionTextBox.Text = document.Description.Replace("<br/>", Environment.NewLine);
			CategoryDropDown.SelectedValue = document.Category.ToString();
			if (document.Category > 0)
			{
				var documentGroup = document.Group.ToString(CultureInfo.InvariantCulture);
				GroupDropdown.SelectedValue = null;
				GroupDropdown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentGroups, document.Category), documentGroup, CodeLocalise("Global.DocumentGroups.TopDefault", "- select group -"));
				GroupCascader.SelectedValue = documentGroup;
			}
			DocumentFileNameLabel.Text = document.FileName;

			foreach (ListItem item in ModuleCheckBoxList.Items)
			{
				var module = (DocumentFocusModules)Enum.Parse(typeof(DocumentFocusModules), item.Value);
				if ((document.Module & module) == module)
					item.Selected = true;
			}
		}

		/// <summary>
		/// Unbinds the document.
		/// </summary>
		/// <returns></returns>
		private DocumentDto UnbindDocument()
		{
			var document = new DocumentDto
			{
				Title = TitleTextBox.TextTrimmed(),
				Description = DescriptionTextBox.TextTrimmed().Replace(Environment.NewLine, "<br/>"),
				File = DocumentUpload.Visible ? DocumentUpload.FileBytes : null,
				Category = Convert.ToInt64(CategoryDropDown.SelectedValue),
				Group = Convert.ToInt64(GroupDropdown.SelectedValue),
				Module = UnbindModules(),
				FileName = DocumentUpload.Visible ? DocumentUpload.FileName : null,
				MimeType = DocumentUpload.PostedFile.ContentType
			};

			if (DocumentId.IsNotNull() && DocumentId > 0)
				document.Id = DocumentId;

			return document;
		}

		/// <summary>
		/// Binds the modules check box list.
		/// </summary>
		private void BindModulesCheckBoxList()
		{
			ModuleCheckBoxList.Items.Clear();

			ModuleCheckBoxList.Items.Add(new ListItem(CodeLocalise("Assist.Label", "#FOCUSASSIST#"), DocumentFocusModules.Assist.ToString()));
			ModuleCheckBoxList.Items.Add(new ListItem(CodeLocalise("Talent.Label", "#FOCUSTALENT#"), DocumentFocusModules.Talent.ToString()));
			ModuleCheckBoxList.Items.Add(new ListItem(CodeLocalise("Career.Label", "#FOCUSCAREER#"), DocumentFocusModules.Career.ToString()));
		}

		/// <summary>
		/// Binds the group dropdown.
		/// </summary>
		private void BindGroupDropdown()
		{
			GroupCascader.PromptText = CodeLocalise("Global.Occupations.TopDefault", "- select group -");
			GroupCascader.LoadingText = CodeLocalise("Global.Occupations.Progress", "[Loading groups ...]");
			GroupCascader.PromptValue = string.Empty;
		}

		/// <summary>
		/// Binds the category drop down.
		/// </summary>
		private void BindCategoryDropDown()
		{
			// At the moment Category and Group are independant dropdowns however if more categories are introduced they should be made into cscading dropdowns
			CategoryDropDown.Items.Clear();

			CategoryDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories), null, CodeLocalise("Global.DocumentCategories.TopDefault", "- select category -"), "");
		}

		/// <summary>
		/// Unbinds the modules.
		/// </summary>
		/// <returns></returns>
		private DocumentFocusModules UnbindModules()
		{
			var selectedModules = (from ListItem item in ModuleCheckBoxList.Items
														 where item.Selected
														 select (DocumentFocusModules)Enum.Parse(typeof(DocumentFocusModules), item.Value)).ToList();

			var modules = selectedModules.Aggregate((current, module) => (current | module));

			return modules;
		}

		/// <summary>
		/// Replaces the document.
		/// </summary>
		private void ReplaceDocument()
		{
			ShowUpload(true);
		}

		/// <summary>
		/// Handles the Click event of the UploadButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void UploadButton_Click(object sender, EventArgs e)
		{
			var document = UnbindDocument();

			var errors = ServiceClientLocator.CoreClient(App).UploadDocument(document);

			//if (errors.IsNotNullOrEmpty() && errors.Count > 0)

			DocumentUpload.Attributes.Add("type", "input");
			DocumentUpload.Attributes.Add("type", "file");

			OnCompleted();
		}

		/// <summary>
		/// Raises the <see cref="Completed" /> event.
		/// </summary>
		protected virtual void OnCompleted()
		{
			if (Completed != null)
				Completed(this);
		}

		/// <summary>
		/// Handles the Click event of the DownloadDocument control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void DownloadDocument_Click(object sender, EventArgs e)
		{
			var document = ServiceClientLocator.CoreClient(App).GetDocument(DocumentId);

			Response.Clear();
			Response.Buffer = true;
			Response.Charset = "";
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
			Response.AddHeader("Content-type", document.MimeType); 
			Response.AddHeader("content-disposition", "attachment;filename=" + document.FileName);
			Response.BinaryWrite(document.File);
			Response.Flush();
			Response.End();
		}

		/// <summary>
		/// Handles the Click event of the DeleteDocument control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void ReplaceDocument_Click(object sender, EventArgs e)
		{
			ReplaceDocument();
		}
	}
}