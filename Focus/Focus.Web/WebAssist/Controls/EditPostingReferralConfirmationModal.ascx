﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditPostingReferralConfirmationModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EditPostingReferralConfirmationModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel"
												PopupDragHandleControlID="ModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ModalPanel" runat="server" CssClass="modal">
	<asp:Panel runat="server" ClientIDMode="Static" ID="ModalPanelHeader">
	<h1><%= HtmlLocalise("Title.Text", "Notification of edited job posting") %></h1>
	<p><%= HtmlLocalise("Summary.Text", "Notify the #BUSINESS#:LOWER of the edits to their posting.")%></p>
	</asp:Panel>
	<div style="overflow-y: scroll; height: 400px; width: 600px;">
		<asp:Literal ID="EmailBodyLiteral" runat="server" ClientIDMode="Static" />
	</div>
	<p><asp:CheckBox ID="BCCMeCheckBox" runat="server" /></p>
	<p>
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
		<asp:Button ID="EditAndSendMessageButton" runat="server" CssClass="button3" OnClick="EditAndSendMessageButton_Clicked" />		
	</p>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />