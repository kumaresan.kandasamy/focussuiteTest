﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardDailyStatistics.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.DashboardDailyStatistics" %>

<asp:PlaceHolder runat="server" ID="DashboardStatisticsPlaceholder">
	<h1><%= HtmlLocalise("DashboardDailyStatistics.Header", "Daily Statistics")%></h1>
	<h4><%= HtmlLocalise("DashboardJobPostings.Header", "Job Postings")%></h4>
	<p><asp:Literal runat="server" ID="TotalActiveSearchableTalentPostingsLiteral"/><%= HtmlLocalise("TotalSearchableTalentPostings.Text", " searchable Talent postings in total")%> &nbsp;&nbsp;|&nbsp;&nbsp;<asp:Literal runat="server" ID="TotalActiveSearchableTalentPostingsAddedYesterdayLiteral"/><%= HtmlLocalise("TotalSearchableTalentPostingsAddedYesterday.Text", " searchable Talent postings added yesterday")%></p>
	<p><asp:Literal runat="server" ID="TotalActiveSpideredPostingsLiteral"/><%= HtmlLocalise("TotalSearchableSpideredPostings.Text", " searchable spidered postings in total")%></p>
	<h4><%= HtmlLocalise("DashboardResumes.Header", "Resumes")%></h4>
	<p><asp:Literal runat="server" ID="TotalSearchableResumesLiteral"/><%= HtmlLocalise("TotalSearchableResume.Text", " searchable resumes in total")%>&nbsp;&nbsp;|&nbsp;&nbsp;<asp:Literal runat="server" ID="TotalSearchableResumesAddedYesterdayLiteral"/><%= HtmlLocalise("SearchableResumeAddedYesterday.Text", " searchable resumes added yesterday")%></p>
</asp:PlaceHolder>