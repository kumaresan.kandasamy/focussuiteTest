﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.Criteria.SpideredPosting;
using Focus.Core.Models.Assist;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class SpideredPostingsWithGoodMatchesList : UserControlBase
	{
		private int _employerPostingsViewCount;

		/// <summary>
		/// Gets or sets the employer filter.
		/// </summary>
		/// <value>The employer filter.</value>
		private string EmployerFilter
		{
			get { return GetViewStateValue<string>("EmployerPostingsList:EmployerFilter"); }
			set { SetViewStateValue("EmployerPostingsList:EmployerFilter", value); }
		}

		/// <summary>
		/// Gets or sets the sort order.
		/// </summary>
		/// <value>The sort order.</value>
		private string SortOrder
		{
			get { return GetViewStateValue<string>("EmployerPostingsList:SortOrder"); }
			set { SetViewStateValue("EmployerPostingsList:SortOrder", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
		}

		/// <summary>
		/// Handles the Clicked event of the FilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FilterButton_Clicked(object sender, EventArgs e)
		{
			EmployerPostingsListPager.ReturnToFirstPage();
			EmployerFilter = FilterTextBox.TextTrimmed();
			EmployerPostingsListView.DataBind();
			EmployerPostingsListUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the DataPaged event of the EmployerPostingsListPager control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.Pager.DataPagedEventArgs"/> instance containing the event data.</param>
		protected void EmployerPostingsListPager_DataPaged(object sender, Pager.DataPagedEventArgs e)
		{
			EmployerPostingsListUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the Closed event of the ComparePostingToResumeModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.ComparePostingToResumeModal.ModalClosedEventArgs"/> instance containing the event data.</param>
		protected void ComparePostingToResumeModal_Closed(object sender, ComparePostingToResumeModal.ModalClosedEventArgs e)
		{
			if (e.DataUpdated)
			{
				EmployerPostingsListView.DataBind();
				EmployerPostingsListUpdatePanel.Update();
			}
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the employer postings.
		/// </summary>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<SpideredEmployerModel> GetEmployerPostings(string employerName, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			var criteria = new SpideredEmployerCriteria
			               	{
			               		EmployerName = employerName,
												FetchOption = CriteriaBase.FetchOptions.PagedList,
			               		OrderBy = orderBy,
			               		PageIndex = pageIndex,
			               		PageSize = maximumRows,
												PostingAge = App.Settings.JobDevelopmentSpideredJobListPostingAge
			               	};

			var employerPostingsPaged = ServiceClientLocator.EmployerClient(App).GetSpideredEmployersWithGoodMatches(criteria);
			_employerPostingsViewCount = employerPostingsPaged.TotalCount;

			return employerPostingsPaged;
		}

		/// <summary>
		/// Gets the employer postings count.
		/// </summary>
		/// <returns></returns>
		public int GetEmployerPostingsCount()
		{
			return _employerPostingsViewCount;
		}

		/// <summary>
		/// Gets the employer postings count.
		/// </summary>
		/// <param name="employerName">Name of the employer.</param>
		public int GetEmployerPostingsCount(string employerName)
		{
			return _employerPostingsViewCount;
		}

		/// <summary>
		/// Handles the Selecting event of the EmployerPostingsDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void EmployerPostingsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["employerName"] = EmployerFilter;
		}

		#endregion

		#region Employer Postings list events

		/// <summary>
		/// Handles the LayoutCreated event of the EmployerPostingsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmployerPostingsListView_LayoutCreated(object sender, EventArgs e)
		{
			#region Set labels

			((Literal)EmployerPostingsListView.FindControl("EmployerNameHeaderLiteral")).Text = CodeLocalise("EmployerNameName.ColumnTitle", "#BUSINESS# Name");
			((Literal)EmployerPostingsListView.FindControl("JobTitleHeaderLiteral")).Text = CodeLocalise("JobTitle.ColumnTitle", "Job Title");
			((Literal)EmployerPostingsListView.FindControl("PostingDateHeaderLiteral")).Text = CodeLocalise("PostingDate.LastActivity", "Posting Date");
			((Literal)EmployerPostingsListView.FindControl("ActionsHeaderLiteral")).Text = CodeLocalise("ActionsIssues.ColumnTitle", "Actions");

			((ImageButton) EmployerPostingsListView.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
			((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
			((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
			#endregion
		}

		/// <summary>
		/// Handles the Sorting event of the EmployerPostingsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void EmployerPostingsListView_Sorting(object sender, ListViewSortEventArgs e)
		{
			SortOrder = e.SortExpression;
			FormatEmployerPostingsListViewSortingImages(e.SortExpression);
			EmployerPostingsListUpdatePanel.Update();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the EmployerPostingsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerPostingsListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var employer = (SpideredEmployerModel)e.Item.DataItem;

			if (employer.IsNotNull() && employer.NumberOfPostings > 1)
			{
				var getPostingLinkButton = (LinkButton)e.Item.FindControl("GetMorePostingsWithGoodMatchesLinkButton");

				if(getPostingLinkButton.IsNotNull())
				{
					getPostingLinkButton.Text = (employer.NumberOfPostings == 2) ? CodeLocalise("GetMorePostingsWithGoodMatchesLinkButtonSingluar.Text", "+ 1 other with good matches")
																														: CodeLocalise("GetMorePostingsWithGoodMatchesLinkButtonPlural.Text", "+ {0} others with good matches", employer.NumberOfPostings - 1);
					getPostingLinkButton.Visible = true;
				}
			
			}
		}

		/// <summary>
		/// Handles the OnItemCommand event of the EmployerPostingsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewCommandEventArgs"/> instance containing the event data.</param>
		protected void EmployerPostingsListView_OnItemCommand(object sender, ListViewCommandEventArgs e)
		{
			if (String.Equals(e.CommandName, "GetPostings"))
			{
				var postingRepeater = (Repeater)e.Item.FindControl("PostingsRepeater");
				var getPostingLinkButton = (LinkButton)e.Item.FindControl("GetMorePostingsWithGoodMatchesLinkButton");

				if (postingRepeater.IsNull())
					return;

				if (postingRepeater.Items.Count == 0)
				{
					var employerName = e.CommandArgument.ToString();

					if (employerName.IsNotNullOrEmpty())
					{
						// Get the Sort Order
						var sortOrder = Constants.SortOrders.PostingDateDesc;

						switch(SortOrder)
						{
							case Constants.SortOrders.PostingDateAsc:
							case Constants.SortOrders.JobTitleAsc:
							case Constants.SortOrders.JobTitleDesc:
								sortOrder = SortOrder;
								break;
						}

						var criteria = new SpideredPostingCriteria
						{
							FetchOption = CriteriaBase.FetchOptions.PagedList,
							PageIndex = 0,
							PageSize = App.Settings.JobDevelopmentSpideredJobListPostingsToDisplay,
							EmployerName = employerName,
							PostingAge = App.Settings.JobDevelopmentSpideredJobListPostingAge,
							OrderBy = sortOrder
						};

						var postings = ServiceClientLocator.PostingClient(App).GetSpideredPostingsWithGoodMatches(criteria);

						// The first posting is already displayed in the UI so remove it
						if (postings.IsNotNullOrEmpty())
							postings.RemoveAt(0);

						postingRepeater.DataSource = postings;
						postingRepeater.DataBind();
						postingRepeater.Visible = true;

						if (getPostingLinkButton.IsNotNull())
							getPostingLinkButton.Text = getPostingLinkButton.Text.Replace("+", "-");
					}
				}
				else
				{
					postingRepeater.Visible = (!postingRepeater.Visible);
					getPostingLinkButton.Text = (getPostingLinkButton.Text.Contains("+")) ? getPostingLinkButton.Text.Replace("+", "-") : getPostingLinkButton.Text.Replace("-", "+");
				}

				EmployerPostingsListUpdatePanel.Update();
			}
		}

		/// <summary>
		/// Handles the Command event of the ViewMatchesLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ViewMatchesLinkButton_Command(object sender, CommandEventArgs e)
		{
			var lensPostingId = e.CommandArgument.ToString();

			if (lensPostingId.IsNotNullOrEmpty())
				ComparePostingToResumeModal.Show(lensPostingId, false);
		}

		/// <summary>
		/// Handles the Command event of the ViewPostingLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ViewPostingLinkButton_Command(object sender, CommandEventArgs e)
		{
			var postingId = e.CommandArgument.ToString();

			if (postingId.IsNotNullOrEmpty())
				PostingModal.Show(postingId);
		}

		/// <summary>
		/// Handles the Command event of the ContactEmployerLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ContactEmployerLinkButton_Command(object sender, CommandEventArgs e)
		{
			var lensPostingId = e.CommandArgument.ToString();

			var postingId = ServiceClientLocator.PostingClient(App).GetPostingId(lensPostingId);

			if (postingId.HasValue)
				Response.Redirect(UrlBuilder.PostingEmployerContact(postingId.Value));
		}
		
		#endregion

		/// <summary>
		/// Formats the employer postings list view sorting images.
		/// </summary>
		/// <param name="orderBy">The order by.</param>
		private void FormatEmployerPostingsListViewSortingImages(string orderBy)
		{
			#region Format images

			// Reset image URLs
			((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortAscButton")).ImageUrl =
				((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortAscButton")).ImageUrl =
				((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortDescButton")).ImageUrl =
				((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortDescButton")).ImageUrl =
				((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortAscButton")).Enabled =
				((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortAscButton")).Enabled =
				((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortAscButton")).Enabled =
				((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortDescButton")).Enabled =
				((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortDescButton")).Enabled =
				((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortDescButton")).Enabled = true;

			switch (orderBy)
			{
				case Constants.SortOrders.EmployerNameAsc:
					((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.EmployerNameDesc:
					((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortDescButton")).Enabled = false;
					break;
				case Constants.SortOrders.JobTitleAsc:
					((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.JobTitleDesc:
					((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployerPostingsListView.FindControl("JobTitleSortDescButton")).Enabled = false;
					break;
				case Constants.SortOrders.PostingDateAsc:
					((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.PostingDateDesc:
					((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployerPostingsListView.FindControl("PostingDateSortDescButton")).Enabled = false;
					break;
			}

			#endregion
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FilterButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
		}

		protected void EmployerPostingsListView_OnPreRender(object sender, EventArgs e)
		{
			//To meet accessibility FVN-1983
			var control = (ImageButton)EmployerPostingsListView.FindControl("EmployerNameSortAscButton");
			if (control.IsNotNull())
			{
				control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
				control = (ImageButton) EmployerPostingsListView.FindControl("EmployerNameSortDescButton");
				control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
				control = (ImageButton) EmployerPostingsListView.FindControl("JobTitleSortAscButton");
				control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
				control = (ImageButton) EmployerPostingsListView.FindControl("JobTitleSortDescButton");
				control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
				control = (ImageButton) EmployerPostingsListView.FindControl("PostingDateSortAscButton");
				control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
				control = (ImageButton) EmployerPostingsListView.FindControl("PostingDateSortDescButton");
				control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			}
		}
	}
}