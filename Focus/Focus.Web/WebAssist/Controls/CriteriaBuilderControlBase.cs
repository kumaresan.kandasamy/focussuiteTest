﻿using System;
using System.Collections.Generic;

namespace Focus.Web.WebAssist.Controls
{
	public class CriteriaBuilderControlBase : UserControlBase
	{
		#region Events

		public event CriteriaAddedHandler CriteriaAdded;

		/// <summary>
		/// Raises the <see cref="E:PoolSearchRequest"/> event.
		/// </summary>
		/// <param name="e">The <see cref="Focus.Web.WebTalent.Controls.PoolSearch.PoolSearchRequestEventArgs"/> instance containing the event data.</param>
		protected virtual void OnCriteriaAdded(CriteriaAddedEventArgs e)
		{
			if (CriteriaAdded != null)
				CriteriaAdded(this, e);
		}

		#endregion

		#region Delegates

		public delegate void CriteriaAddedHandler(object o, CriteriaAddedEventArgs e);

		#endregion

		#region EventArgs

		/// <summary>
		/// 
		/// </summary>
		public class CriteriaAddedEventArgs : EventArgs
		{
			public readonly CriteriaElement Criteria;

			public CriteriaAddedEventArgs(CriteriaElement criteria)
			{
				Criteria = criteria;
			}
		}

		#endregion

		public class CriteriaElement
		{
			public string DisplayText { get; set; }
			public bool OverwriteExistingValue { get; set; }
			public CriteriaElementType Type { get; set; }
			public List<string> Parameters { get; set; }
		}

		public enum CriteriaElementType
		{
			LocationRadius,
			LocationCounty,
			LocationOffice,
			LocationWorkforceInvestmentBoard,
			JobCharacteristicsOccupationGroup,
			JobCharacteristicsOccupationTitle,
			JobCharacteristicSalary,
			JobCharacteristicStatus,
			JobCharacteristicLevelOfEducation,
			JobCharacterisiticSearchTerms,
			EmployerCharacteristicsSector,
			EmployerCharacteristicsWorkOpportuniesTaxCredit,
			EmployerCharacteristicsFederalEmployerIdentificationNumber,
			EmployerCharacteristicsName,
			EmployerCharacteristicsJobTitle,
			EmployerCharacteristicsGreenJob,
			EmployerCharacteristicsITEmployer,
			EmployerCharacteristicsHealthcareEmployer,
			EmployerCharacteristicsBiotechEmployer,
			EmployerCharacteristicsAccountCreationDate,
			EmployerCharacteristicsScreeningAssistance,
			Date,
			ComplianceForeignLabourCertificationJobs,
			ComplianceForeignLabourCertificationJobsWithGoodMatches,
			ComplianceCourtOrderedAffirmativeAction,
			ComplianceFederalContractJobs
		}
	}
}