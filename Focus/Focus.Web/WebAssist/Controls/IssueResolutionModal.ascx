﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IssueResolutionModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.IssueResolutionModal" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>

<asp:HiddenField ID="IssueResolutionModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="IssueResolutionModalPopup" runat="server" BehaviorID="IssueResolutionModal"
												TargetControlID="IssueResolutionModalDummyTarget"
												PopupControlID="IssueResolutionModalPanel"
												PopupDragHandleControlID="IssueResolutionModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="IssueResolutionModalPanel" runat="server" CssClass="modal" style="display:none;">
    <h2><asp:Literal runat="server" ID="ModalTitle" Text="Issue resolution"/></h2>
    <asp:Literal ID=IssueListLit runat="server" /><br />
    <asp:CheckBoxList runat="server" ID="IssueList" ClientIDMode="Static"></asp:CheckBoxList>
    <asp:CustomValidator runat="server" ID="CheckBoxListValidator" ClientValidationFunction="ValidateIssueList" CssClass="error" ValidationGroup="IssueResolutionModal"></asp:CustomValidator><br/>
    <%= HtmlRequiredLabel(NoteTextBox, "Notes.Label", "Notes")%><br/>
    <asp:TextBox ID="NoteTextBox" runat="server" TextMode="MultiLine" Rows="5" Width="600"  /><br/>
    <asp:RequiredFieldValidator runat="server" ControlToValidate="NoteTextBox" ID="NoteValidator" CssClass="error"  ValidationGroup="IssueResolutionModal"></asp:RequiredFieldValidator><br/>
    <asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" CausesValidation="False" />
	<asp:Button ID="SaveButton" runat="server" CssClass="button4" OnClick="SaveButton_Clicked" ValidationGroup="IssueResolutionModal" />
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />
<script type="text/javascript">
    function ValidateIssueList(source, args) {
        args.IsValid = $('input[id^="IssueList"]:checked').length;
    }
</script>