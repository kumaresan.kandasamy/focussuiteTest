﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class CompareResumesModal : UserControlBase
	{
		protected JobDto Job
		{
			get { return GetViewStateValue<JobDto>("JobPreviewModal:Job"); }
			set { SetViewStateValue("JobPreviewModal:Job", value); }
		}

		private Details _details
		{
			get { return GetViewStateValue<Details>("CompareResumesModal:Details"); }
			set { SetViewStateValue("CompareResumesModal:Details", value); }
		}

		public delegate void CompletedHandler(object sender, EventArgs eventArgs);
		public event CompletedHandler Completed;

		public virtual void OnCompleted(EventArgs eventargs)
		{
			if (Completed != null) Completed(this, eventargs);
		}

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (!IsPostBack)
      {
          LocaliseUI();

          //Bind a zero score to this image to prevent the image having an empty src attribute
          BindScore(ReferralScoreImage, 0);

      }
		}

    /// <summary>
    /// Shows the specified details.
    /// </summary>
    /// <param name="details">The details.</param>
		public void Show(Details details)
    {
	    _details = details;

      #region Suggested Match

      BindScore(ReferralScoreImage, Convert.ToInt32(details.Score));
      CandidateNameLiteral.Text = CodeLocalise("CandidateName.Text", "<strong>" + details.CandidateName + "</strong>");
			CandidateResumeDescriptionLiteral.Text = CodeLocalise("CandidateResumeDescription.Text", "" + GetCandidateResume(details.CandidateId));
			
      #endregion

      #region Recent Placement

      JobEmployerTitleLabel.Text = CodeLocalise("JobEmployerTitle", details.JobTitle + ", " + details.BusinessUnitName);
      PlacementDetailsLiteral.Text = CodeLocalise("PlacementName.Text", "<strong>" + details.RecentPlacementName + "</strong>");
			PlacementResumeDescriptionLiteral.Text = CodeLocalise("PlacementResumeDescription.Text", ""  + GetCandidateResume(details.RecentPlacementId));
	    DoNotShowCheckBox.Checked = false;

      #endregion

      CompareResumesModalPopup.Show();
		}

		/// <summary>
		/// Gets the candidate resume.
		/// </summary>
		/// <param name="candidateId">The candidate id.</param>
		/// <returns></returns>
		private string GetCandidateResume(long candidateId)
		{
			return ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(candidateId);
		}
		
		/// <summary>
		/// Sets the job details.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		private void SetJobDetails(long jobId)
		{
			Job = ServiceClientLocator.JobClient(App).GetJob(jobId);
		}

		/// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      //ContactEmployerButton.Text = CodeLocalise("ContactEmployerButton.Text", "Contact employer");
      //CancelButton.Text = CodeLocalise("CancelButton.Text", "Cancel");
    }

    /// <summary>
    /// Handles the Clicked event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CancelButton_Clicked(object sender, EventArgs e)
    {
			CompareResumesModalPopup.Hide();

			if (Completed != null && DoNotShowCheckBox.Checked)
				Completed(this, new EventArgs());
    }

		/// <summary>
		/// Handles the Clicked event of the ContactEmployerButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ContactEmployerButton_Clicked(object sender, EventArgs e)
		{
			CompareResumesModalPopup.Hide();

			if (Completed != null && DoNotShowCheckBox.Checked)
				Completed(this, new EventArgs());

			SetJobDetails(_details.JobId);

      if (Job.EmployeeId != null) 
        Response.Redirect(UrlBuilder.ContactEmployer(Job.BusinessUnitId.GetValueOrDefault(0), _details.CandidateId, _details.RecentPlacementId));
		}

		/// <summary>
    /// Binds the score.
    /// </summary>
    /// <param name="scoreImage">The score image.</param>
    /// <param name="score">The score.</param>
    private void BindScore(Image scoreImage, int score)
    {
      var minimumStarScores = App.Settings.StarRatingMinimumScores;

      scoreImage.ToolTip = score.ToString();

      if (score >= minimumStarScores[5])
        scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
      else if (score >= minimumStarScores[4])
        scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
      else if (score >= minimumStarScores[3])
        scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
      else if (score >= minimumStarScores[2])
        scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
      else if (score >= minimumStarScores[1])
        scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
      else
        scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
    }

		/// <summary>
		/// Handles the CheckChanged event of the DoNotShowCheckBox control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DoNotShowCheckBox_CheckChanged(object sender, EventArgs e)
		{
			if (DoNotShowCheckBox.Checked)
				ServiceClientLocator.EmployerClient(App).IgnoreRecentlyPlacedMatch(_details.RecentlyPlacedMatchId);
			else
				ServiceClientLocator.EmployerClient(App).AllowRecentlyPlacedMatch(_details.RecentlyPlacedMatchId);

			CompareResumesModalPopup.Show();
		}

		#region Helper Class

		[Serializable]
		public class Details
		{
			public long JobId { get; set; }
			public string JobTitle { get; set; }
			public long RecentlyPlacedMatchId { get; set; }
			
			public string BusinessUnitName { get; set; }

			public long RecentPlacementId { get; set; }
			public string RecentPlacementName { get; set; }

			public long CandidateId { get; set; }
			public string CandidateName { get; set; }

			public int Score { get; set; }
		}

		#endregion

	}
}