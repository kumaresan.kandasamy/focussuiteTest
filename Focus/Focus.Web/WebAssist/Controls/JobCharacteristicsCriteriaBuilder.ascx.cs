﻿using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Employee;
using Framework.Core;
using Focus.Common.Extensions;

namespace Focus.Web.WebAssist.Controls
{
	public partial class JobCharacteristicsCriteriaBuilder : CriteriaBuilderControlBase
	{
		private JobCharacteristicsCriteria _criteria
		{
			get { return GetViewStateValue<JobCharacteristicsCriteria>("JobCharacteristicsCriteriaBuilder:Criteria"); }
			set { SetViewStateValue("JobCharacteristicsCriteriaBuilder:Criteria", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				_criteria = new JobCharacteristicsCriteria();
			}
		}

		#region Public methods

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public JobCharacteristicsCriteria Unbind()
		{
			return _criteria;
		}

		/// <summary>
		/// Clears the occupation group filter.
		/// </summary>
		/// <param name="occupationGroup">The occupation group.</param>
		public void ClearOccupationGroupFilter(string occupationGroup)
		{
			foreach (var occupationGroupFilter in _criteria.OccupationGroups.Where(occupationGroupFilter => occupationGroupFilter == occupationGroup))
			{
				_criteria.OccupationGroups.Remove(occupationGroupFilter);
				break;
			}
		}

		/// <summary>
		/// Clears the occupation title filter.
		/// </summary>
		/// <param name="occupationTitle">The occupation title.</param>
		public void ClearOccupationTitleFilter(string occupationTitle)
		{
			foreach (var occupationTitleFilter in _criteria.OccupationTitles.Where(occupationTitleFilter => occupationTitleFilter == occupationTitle))
			{
				_criteria.OccupationTitles.Remove(occupationTitleFilter);
				break;
			}
		}

		/// <summary>
		/// Clears the salary filter.
		/// </summary>
		public void ClearSalaryFilter()
		{
			_criteria.SalaryMinimum = null;
			_criteria.SalaryMaximum = null;
			_criteria.SalaryFrequency = null;
		}

		/// <summary>
		/// Clears the job status filter.
		/// </summary>
		public void ClearJobStatusFilter()
		{
			_criteria.ShowPostedJobs = null;
			_criteria.ShowRegisteredJobs = null;
			_criteria.ShowExpiredJobs = null;
			_criteria.ShowClosedJobs = null;
		}

		/// <summary>
		/// Clears the education filter.
		/// </summary>
		public void ClearEducationFilter()
		{
			_criteria.LevelOfEducation = null;
			_criteria.ShowUnspecifiedEducationJobs = null;
		}

		/// <summary>
		/// Clears the search term filter.
		/// </summary>
		public void ClearSearchTermFilter()
		{
			_criteria.MatchAllSearchTerms = null;
			_criteria.SearchTerms = null;
			_criteria.MatchSearchTermsInJobTitle = null;
		}

		#endregion

		#region Add button clicks

		/// <summary>
		/// Handles the Clicked event of the AddOccupationGroupFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddOccupationGroupFilterButton_Clicked(object sender, EventArgs e)
		{
			if (OccupationGroupTextBox.TextTrimmed().IsNullOrEmpty()) return;

			var occupationGroup = OccupationGroupTextBox.TextTrimmed();

			if (_criteria.OccupationGroups.IsNull()) _criteria.OccupationGroups = new List<string>();

			_criteria.OccupationGroups.Add(occupationGroup);
			OccupationGroupTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
			                                           	{
			                                           		DisplayText = CodeLocalise("OccupationGroup.Text", "Occupation group {0}", occupationGroup),
			                                           		OverwriteExistingValue = false,
			                                           		Type = CriteriaElementType.JobCharacteristicsOccupationGroup,
			                                           		Parameters = new List<string> {occupationGroup}
			                                           	}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddOccupationTitleFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddOccupationTitleFilterButton_Clicked(object sender, EventArgs e)
		{
			if (OccupationTitleTextBox.TextTrimmed().IsNullOrEmpty()) return;

			var occupationTitle = OccupationTitleTextBox.TextTrimmed();

			if (_criteria.OccupationTitles.IsNull()) _criteria.OccupationTitles = new List<string>();

			_criteria.OccupationTitles.Add(occupationTitle);
			OccupationTitleTextBox.Text = "";

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("OccupationTitle.Text", "Occupation title {0}", occupationTitle),
																										OverwriteExistingValue = false,
																										Type = CriteriaElementType.JobCharacteristicsOccupationTitle,
																										Parameters = new List<string> { occupationTitle }
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddSalaryFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddSalaryFilterButton_Clicked(object sender, EventArgs e)
		{
			if (MinimumSalaryTextBox.TextTrimmed().IsNullOrEmpty() || MaximumSalaryTextBox.TextTrimmed().IsNullOrEmpty())
				return;
			
			try
			{
				_criteria.SalaryMinimum = Convert.ToInt32(MinimumSalaryTextBox.TextTrimmed());
				_criteria.SalaryMaximum = Convert.ToInt32(MaximumSalaryTextBox.TextTrimmed());
				_criteria.SalaryFrequency = (YearySalaryRadioButton.Checked) ? Frequencies.Yearly : Frequencies.Hourly;

				MinimumSalaryTextBox.Text = MaximumSalaryTextBox.Text = "";
				HourlySalaryRadioButton.Checked = true;
				YearySalaryRadioButton.Checked = false;

				var displayText = (YearySalaryRadioButton.Checked)
				                  	? CodeLocalise("YearlySalary.Text", "Salary of ${0} to ${1} per year", _criteria.SalaryMinimum, _criteria.SalaryMaximum)
														: CodeLocalise("HourlySalary.Text", "Salary of ${0} to ${1} per hour", _criteria.SalaryMinimum, _criteria.SalaryMaximum);
				
				OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																										{
																											DisplayText = displayText,
																											OverwriteExistingValue = true,
																											Type = CriteriaElementType.JobCharacteristicSalary,
																										}));
			}
			catch{}
		}

		/// <summary>
		/// Handles the Clicked event of the AddJobStatusFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddJobStatusFilterButton_Clicked(object sender, EventArgs e)
		{
			if (!PostedJobsCheckbox.Checked && !RegisteredJobsCheckbox.Checked && !ExpiredJobsCheckbox.Checked && !ClosedJobsCheckbox.Checked)
				return;

			_criteria.ShowPostedJobs = PostedJobsCheckbox.Checked;
			_criteria.ShowRegisteredJobs = RegisteredJobsCheckbox.Checked;
			_criteria.ShowExpiredJobs = ExpiredJobsCheckbox.Checked;
			_criteria.ShowClosedJobs = ClosedJobsCheckbox.Checked;

			var statusesToShow = new List<string>();
			if (PostedJobsCheckbox.Checked) statusesToShow.Add(CodeLocalise("Posted.Text", "Posted"));
			if (RegisteredJobsCheckbox.Checked) statusesToShow.Add(CodeLocalise("Registered.Text", "Registered"));
			if (ExpiredJobsCheckbox.Checked) statusesToShow.Add(CodeLocalise("Expired.Text", "Expired"));
			if (ClosedJobsCheckbox.Checked) statusesToShow.Add(CodeLocalise("Closed.Text", "Closed"));

			var displayText = CodeLocalise("JobStatus.Text", "Job Status {0}", statusesToShow.ToSentence(CodeLocalise("Global.And.Text", "and")));

			PostedJobsCheckbox.Checked = false;
			RegisteredJobsCheckbox.Checked = false;
			ExpiredJobsCheckbox.Checked = false;
			ClosedJobsCheckbox.Checked = false;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = displayText,
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.JobCharacteristicStatus,
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddEducationFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddEducationFilterButton_Clicked(object sender, EventArgs e)
		{
			if (!HighSchoolDiplomaCheckbox.Checked && !AssociateDegreeCheckbox.Checked && !BachelorsDegreeCheckbox.Checked && !GraduateDegreeCheckbox.Checked && !ShowUnspecifiedEducationLevelCheckbox.Checked)
				return;
			
			var educationLevel = EducationLevels.None;
			var educationLevelsToShow = new List<string>();

			if(HighSchoolDiplomaCheckbox.Checked)
			{
				educationLevel = educationLevel | EducationLevels.HighSchoolDiplomaOrEquivalent;
				educationLevelsToShow.Add(CodeLocalise(EducationLevels.HighSchoolDiplomaOrEquivalent, "High school diploma or equivalent"));
			}

			if(AssociateDegreeCheckbox.Checked)
			{
				educationLevel = educationLevel | EducationLevels.AssociatesDegree;
				educationLevelsToShow.Add(CodeLocalise(EducationLevels.AssociatesDegree, "Associate's degree/some college"));
			}

			if (BachelorsDegreeCheckbox.Checked)
			{
				educationLevel = educationLevel | EducationLevels.BachelorsDegree;
				educationLevelsToShow.Add(CodeLocalise(EducationLevels.BachelorsDegree, "Bachelor's degree"));
			}

			if (GraduateDegreeCheckbox.Checked)
			{
				educationLevel = educationLevel | EducationLevels.GraduateDegree;
				educationLevelsToShow.Add(CodeLocalise(EducationLevels.GraduateDegree, "Graduate degree"));
			}

			_criteria.LevelOfEducation = educationLevel;

			_criteria.ShowUnspecifiedEducationJobs = ShowUnspecifiedEducationLevelCheckbox.Checked;
			if (ShowUnspecifiedEducationLevelCheckbox.Checked) educationLevelsToShow.Add(CodeLocalise("ShowUnspecifiedEducationLevelCheckbox.Text", "Unspecified"));

			var displayText = CodeLocalise("Education.Text", "Job required education level(s) {0}", educationLevelsToShow.ToSentence(CodeLocalise("Global.And.Text", "and")));

			HighSchoolDiplomaCheckbox.Checked = false;
			AssociateDegreeCheckbox.Checked = false;
			BachelorsDegreeCheckbox.Checked = false;
			GraduateDegreeCheckbox.Checked = false; 
			ShowUnspecifiedEducationLevelCheckbox.Checked = false;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = displayText,
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.JobCharacteristicLevelOfEducation
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddSearchTermsFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddSearchTermsFilterButton_Clicked(object sender, EventArgs e)
		{
			if (SearchTermsTextBox.TextTrimmed().IsNullOrEmpty())
				return;

			_criteria.MatchAllSearchTerms = MatchAllTermsRadioButton.Checked;

			var delimeters = new char[] {'\n', '\r'};
			_criteria.SearchTerms = SearchTermsTextBox.TextTrimmed().Split(delimeters).ToList();

			_criteria.MatchSearchTermsInJobTitle = SearchTermsInJobTitleCheckbox.Checked;

			var searchTermsText = (_criteria.MatchAllSearchTerms.GetValueOrDefault())
			                      	? _criteria.SearchTerms.ToSentence(CodeLocalise("Global.And.Text", "and"))
			                      	: _criteria.SearchTerms.ToSentence(CodeLocalise("Global.Or.Text", "or"));

			var displayText = (_criteria.MatchSearchTermsInJobTitle.GetValueOrDefault())
			              	? CodeLocalise("MatchSearchTermsInJobTitle.Text", "Job title contains {0}", searchTermsText)
			              	: CodeLocalise("MatchSearchTerms.Text", "Job contains {0}", searchTermsText);

			MatchAllTermsRadioButton.Checked = true;
			MatchSomeTermsRadioButton.Checked = false;
			SearchTermsTextBox.Text = "";
			SearchTermsInJobTitleCheckbox.Checked = false;

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = displayText,
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.JobCharacterisiticSearchTerms
																									}));
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			AddOccupationGroupFilterButton.Text = AddOccupationTitleFilterButton.Text = AddSalaryFilterButton.Text = AddJobStatusFilterButton.Text = AddEducationFilterButton.Text = AddSearchTermsFilterButton.Text = CodeLocalise("Global.Add.Text", "Add");
			HourlySalaryRadioButton.Text = CodeLocalise(Frequencies.Hourly, "Hourly");
			YearySalaryRadioButton.Text = CodeLocalise(Frequencies.Yearly, "Yearly");
			PostedJobsCheckbox.Text = CodeLocalise("PostedJobs.Text", "Posted");
			RegisteredJobsCheckbox.Text = CodeLocalise("RegisteredJobsCheckbox.Text", "Registered");
			ExpiredJobsCheckbox.Text = CodeLocalise("ExpiredJobsCheckbox.Text", "Expired/expiring");
			ClosedJobsCheckbox.Text = CodeLocalise("ClosedJobsCheckbox.Text", "Closed");
			HighSchoolDiplomaCheckbox.Text = CodeLocalise(EducationLevels.HighSchoolDiplomaOrEquivalent, "High school diploma or equivalent");
			AssociateDegreeCheckbox.Text = CodeLocalise(EducationLevels.AssociatesDegree, "Associate's degree/some college");
			BachelorsDegreeCheckbox.Text = CodeLocalise(EducationLevels.BachelorsDegree, "Bachelor's degree");
			GraduateDegreeCheckbox.Text = CodeLocalise(EducationLevels.GraduateDegree, "Graduate degree");
			ShowUnspecifiedEducationLevelCheckbox.Text = CodeLocalise("ShowUnspecifiedEducationLevelCheckbox.Text", "Unspecified");
			MatchAllTermsRadioButton.Text = CodeLocalise("MatchAllTermsRadioButton.Text", "Results use all terms");
			MatchSomeTermsRadioButton.Text = CodeLocalise("MatchSomeTermsRadioButton.Text", "Results use some terms");
			SearchTermsInJobTitleCheckbox.Text = CodeLocalise("SearchTermsInJobTitleOnlyCheckbox.Text", "Keywords must be in job title");
		}

		#endregion

	}
}