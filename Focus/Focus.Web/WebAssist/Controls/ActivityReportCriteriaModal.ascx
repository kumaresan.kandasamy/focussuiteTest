﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivityReportCriteriaModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ActivityReportCriteriaModal" %>
<script src="<%= ResolveUrl("~/Assets/Scripts/moment.min.js") %>" type="text/javascript"></script>

<asp:HiddenField ID="ActivityReportCriteriaModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ActivityReportCriteriaModalPopup" runat="server" BehaviorID="ActivityReportCriteriaModal"
            TargetControlID="ActivityReportCriteriaModalDummyTarget"
            PopupControlID="ActivityReportCriteriaModalPanel"
            PopupDragHandleControlID="ActivityReportCriteriaModalPanel"
            RepositionMode="RepositionOnWindowResizeAndScroll" 
            BackgroundCssClass="modalBackground" />

<asp:Panel ID="ActivityReportCriteriaModalPanel" runat="server" CssClass="modal" style="display:none;" DefaultButton="RunButton">
	<h2>
	  <asp:Literal ID="ActivityReportTitle" runat="server" />
	</h2>
	<asp:PlaceHolder runat="server" ID="CriteriaPlaceholder">
		<div class="dataInputContainer" style="width: 710px">
		<focus:LocalisedLabel runat="server" ID="SpecifyReportingPeriodLabel" DefaultText="Please specify your reporting period" LocalisationKey="ReportingPeriodInstructions.Text"/>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="FromDateLabel" AssociatedControlID="FromDateTextBox" CssClass="dataInputLabel requiredData" DefaultText="From" LocalisationKey="FromDate.Text"/>
			<div class="dataInputField">
				<asp:TextBox ID="FromDateTextBox" runat="server" MaxLength="10" Width="150" />
				<act:CalendarExtender ID="FromDateCalendarExtender" runat="server" TargetControlID="FromDateTextBox" CssClass="cal_Theme1" />
				<act:MaskedEditExtender ID="FromDateMaskedEditExtender" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="FromDateTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_"/>
				<div>
					<div><asp:RequiredFieldValidator ID="FromDateRequired" runat="server" ValidationGroup="ActivityReport" ControlToValidate="FromDateTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" InitialValue="__/__/____"/></div>
					<div><asp:CustomValidator ID="DateValidator" runat="server" ValidationGroup="ActivityReport" ControlToValidate="FromDateTextBox" CssClass="error block" Display="Dynamic" ValidateEmptyText="false" ClientValidationFunction="ValidateReportDates"/></div>
				</div>
			</div>	
			<focus:LocalisedLabel runat="server" ID="ToDateLabel" AssociatedControlID="ToDateTextBox" CssClass="dataInputLabel requiredData" DefaultText="To" LocalisationKey="ToDate" style="margin-left: 30px;"/>
			<div class="dataInputField">
				<asp:TextBox ID="ToDateTextBox" runat="server" MaxLength="10" Width="150" />
				<act:CalendarExtender ID="ToDateCalendarExtender" runat="server" TargetControlID="ToDateTextBox"  CssClass="cal_Theme1"/>
				<act:MaskedEditExtender ID="ToDateMaskedEditExtender" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="ToDateTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_"/>
				<div>
				<div><asp:RequiredFieldValidator ID="ToDateRequired" runat="server" ValidationGroup="ActivityReport" ControlToValidate="ToDateTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" InitialValue="__/__/____"/></div>							
				</div>
			</div>	
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="SendToLabel" AssociatedControlID="SendToTextbox" CssClass="dataInputLabel requiredData" DefaultText="Send report to" LocalisationKey="SendTo"/>
			<div class="dataInputField block">
				<asp:TextBox ID="SendToTextbox" runat="server" TextMode="MultiLine"/>
				<asp:RequiredFieldValidator ID="SendToRequired" runat="server" ValidationGroup="ActivityReport" ControlToValidate="SendToTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			</div>
		</div>
		<p class="instructionalText"><focus:LocalisedLabel runat="server" DefaultText="The report will be sent to the email addresses provided above. Use a comma to separate multiple addresses. Take care not to separate using any other special characters" RenderOuterSpan="False" ID="InstructionLabel" /></p>
		<div><asp:CustomValidator runat="server" ID="EmailValidator" ValidationGroup="ActivityReport" ControlToValidate="SendToTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error"/></div>
		<p style="text-align:right"><asp:Button ID="RunButton" runat="server" CssClass="button3" OnClick="RunButton_Clicked" ValidationGroup="ActivityReport" CausesValidation="True" /></p>
		</div>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" Visible="False" ID="CompletionPlaceholder">
		<div style="width: 550px">
			<p><focus:LocalisedLabel runat="server" DefaultText="Your report may take up to a few minutes to run. When complete, a copy of the report will be emailed to the specified recipients as an attachment." RenderOuterSpan="False" ID="CompletionLabel" /></p>
			<div style="margin-top: 30px">
			<p style="text-align:right"><asp:Button ID="OkButton" runat="server" CssClass="button3" OnClick="OkButton_Clicked" CausesValidation="True" /></p>
			</div>
		</div>
	</asp:PlaceHolder>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('<%=ActivityReportCriteriaModalPopup.BehaviorID %>').hide(); return false;" /></div>
</asp:Panel>

	<script type="text/javascript">

		function ValidateReportDates(src, args) {
			var fromDate = $("#<%= FromDateTextBox.ClientID  %>").val();
			var toDate = $("#<%= ToDateTextBox.ClientID  %>").val();

			// If either haven't been entered then let the RequiredValidators do their job
			if (fromDate == '__/__/____' || fromDate == '' || toDate == "__/__/____" || toDate == '')
				return;

			var fromDateMoment = moment(fromDate, "<%=DataFormat %>", true);
			var toDateMoment = moment(toDate, "<%=DataFormat %>", true);

		  if (!fromDateMoment.isValid()) {
				src.innerHTML = '<%= FromDateInvalidErrorMessage %>';
				args.IsValid = false;
			}
			else if (!toDateMoment.isValid()) {
				src.innerHTML = '<%= ToDateInvalidErrorMessage %>';
				args.IsValid = false;
			}
			else if (fromDateMoment.isAfter(moment())) {
				src.innerHTML = '<%= FromDateInFutureErrorMessage %>';
				args.IsValid = false;
			}
			else if (toDateMoment.isAfter(moment())) {
				src.innerHTML = '<%= ToDateInFutureErrorMessage %>';
				args.IsValid = false;
			}
		  else if (fromDateMoment.isAfter(toDateMoment)) {
		    src.innerHTML = '<%= FromDateAfterToDateErrorMessage %>';
		    args.IsValid = false;
		  }
		  else if (fromDateMoment.year() < 2000) {
		    src.innerHTML = '<%= DateTooOld %>';
		    args.IsValid = false;
		  }
    }
		
	</script>