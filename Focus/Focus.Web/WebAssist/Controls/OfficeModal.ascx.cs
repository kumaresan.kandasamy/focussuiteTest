﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class OfficeModal : UserControlBase
	{
		public delegate void CompletedHandler(object sender, OfficeModalEventArgs eventArgs);
		public event CompletedHandler Completed;

		public class OfficeModalEventArgs : EventArgs
		{
			public ActionTypes ActionType { get; set; }
		}

		public long OfficeId
		{
			get { return GetViewStateValue<long>("OfficeModal:OfficeId"); }
			set { SetViewStateValue("OfficeModal:OfficeId", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				BindDropdowns();
				AssignPostalZipRegEx.ValidationExpression = @"^[a-zA-Z0-9,]+$";  // Only allow letters, numbers and commas
				OfficeNameRegEx.ValidationExpression = @"^[\s\S]{0,40}$";  // Only allow 40 characters entered
				OfficeManagerMailboxRegEx.ValidationExpression =
					BusinessOutreachMailboxRegEx.ValidationExpression = string.Concat("^", App.Settings.EmailAddressRegExPattern, "$");

				ExternalIdRequiredLabelCell.Visible = ExternalIdRequired.Enabled = App.Settings.OfficeExternalIdMandatory;
				ExternalIdLabelCell.Visible = !App.Settings.OfficeExternalIdMandatory;

				StaffMemberList.MaxSize = App.Settings.MaxStaffForOffices;

				ApplyTheme();
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Clicked(object sender, EventArgs e)
		{
			var saved = Save();
			if (saved)
				OnCompleted(new OfficeModalEventArgs { ActionType = ActionTypes.SaveOffice });
			else
				OfficeModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the Deactivate button.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void ListStaffButton_Clicked(object sender, EventArgs e)
		{
			ListOfficeStaffMembersModal.Show(OfficeId);
		}

		/// <summary>
		/// Handles the Clicked event of the Activate button.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void ActivateButton_Clicked(object sender, EventArgs e)
		{
			ConfirmReactivate();
		}

		/// <summary>
		/// Handles the Clicked event of the Deactivate button.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void DeactivateButton_Clicked(object sender, EventArgs e)
		{
			IsOfficeAssigned();
		}

		/// <summary>
		/// Handles the OkCommand event of the DeleteConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void ConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			long officeId;
			long.TryParse(e.CommandArgument.ToString(), out officeId);

			var actionType = ActionTypes.NoAction;

			switch (e.CommandName)
			{
				case "ActivateOffice":
					ActivateOffice(officeId);
					actionType = ActionTypes.ReactivateOffice;
					break;
				case "DeactivateOffice":
					actionType = ActionTypes.DeactivateOffice;
					DeactivateOffice(officeId);
					break;
			}

			if (actionType != ActionTypes.NoAction)
				OnCompleted(new OfficeModalEventArgs { ActionType = actionType });
		}

		/// <summary>
		/// Raises the <see cref="Completed" /> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompleted(OfficeModalEventArgs eventArgs)
		{
			if (Completed != null)
				Completed(this, eventArgs);
		}

		/// <summary>
		/// Shows this instance.
		/// </summary>
		public void Show()
		{
			OfficeId = 0;

			HeaderLabel.Text = CodeLocalise("HeaderLabel.Text", "Create office");

			OfficeStatusPlaceHolder.Visible =
				ActivateButton.Visible =
				DeactivateButton.Visible = false;

			SaveButton.Visible = true;

			Initialise();
			OfficeModalPopup.Show();

			CountryDropDownList.SelectValue(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(
																										x => x.Key == App.Settings.DefaultCountryKey).Select(x => x.Id).SingleOrDefault().
																											ToString());
		}

		/// <summary>
		/// Shows the specified office id.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		public void Show(long officeId)
		{
			OfficeId = officeId;

			var office = ServiceClientLocator.EmployerClient(App).GetOffice(OfficeId);

			HeaderLabel.Text = office.OfficeName;

			Initialise();
			Bind(office);

			OfficeModalPopup.Show();
		}

		/// <summary>
		/// Initialises this instance.
		/// </summary>
		private void Initialise()
		{
			CountyDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.County.TopDefault", "- select county -");
			CountyDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.County.Progress", "[Loading counties ...]");

			IdHiddenField.Value = "";
			OfficeNameTextBox.Text = "";
			ExternalIdTextBox.Text = "";
			Line1TextBox.Text = "";
			Line2TextBox.Text = "";
			PostcodeZipTextBox.Text = "";
			TownCityTextBox.Text = "";
			StateDropDownList.SelectedIndex = 0;
			CountyDropDownList.SelectedIndex = 0;
			CountyDropDownListCascadingDropDown.SelectedValue = "0";
			CountryDropDownList.SelectedIndex = 0;
			AssignPostalZipTextBox.Text = "";
			OfficeManagerMailboxTextBox.Text = "";
			BusinessOutreachMailboxTextBox.Text = "";
			StaffMemberList.Clear();

			StaffMemberList.EnableDefaultSelection = App.Settings.EnableOfficeDefaultAdmin;

			if (App.Settings.EnableOfficeDefaultAdmin)
			{
				AssignOfficeStaffLabel.CssClass = "requiredData";
			}

			StaffMemberList.AutoCompleteServiceMethod = "GetNonStatewideStaffMemberNames";
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			if (App.Settings.Theme == FocusThemes.Education)
				BusinessOutreachRow.Visible = false;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SaveButton.Text = CodeLocalise("SaveButton.Text", "Save");
			ActivateButton.Text = CodeLocalise("ActivateButton.Text", "Activate");
			DeactivateButton.Text = CodeLocalise("ActivateButton.Text", "Deactivate");

			OfficeNameRequired.ErrorMessage = CodeLocalise("OfficeNameRequired.ErrorMessage", "Office name is required");
			Line1Required.ErrorMessage = CodeLocalise("Line1Required.ErrorMessage", "Address line 1 is required");
			PostcodeZipRequired.ErrorMessage = CodeLocalise("PostcodeZipRequired.ErrorMessage", "Zip code is required");
			StateRequired.ErrorMessage = CodeLocalise("StateRequired.ErrorMessage", "State is required");
			AssignPostalZipRegEx.ErrorMessage = CodeLocalise("AssignPostalZipRegEx.ErrorMessage", "Only letters, numbers and commas are allowed");
			OfficeNameRegEx.ErrorMessage = CodeLocalise("OfficeNameRegEx.ErrorMessage", "Office name can only be 40 characters");
			OfficeManagerMailboxRequired.Text = CodeLocalise("OfficeManagerMailboxRequired.ErrorMessage", "Office manager mailbox address is required");
			OfficeManagerMailboxRegEx.Text = CodeLocalise("OfficeManagerMailboxRegEx.ErrorMessage", "Please enter a valid email address");
			BusinessOutreachMailboxRequired.Text = CodeLocalise("BusinessOutreachMailboxRequired.ErrorMessage", "Business Outreach mailbox address is required");
			BusinessOutreachMailboxRegEx.Text = CodeLocalise("BusinessOutreachMailboxRegEx.ErrorMessage", "Please enter a valid email address");
			ExternalIdRequired.ErrorMessage = CodeLocalise("ExternalIdRequired.ErrorMessage", "Office ID is required");
			ExternalIdDuplicate.Text = CodeLocalise("ExternalIdDuplicate.ErrorMessage", "Office ID already exists");
		}

		/// <summary>
		/// Binds the dropdowns.
		/// </summary>
		private void BindDropdowns()
		{
			StateDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), null, CodeLocalise("Global.State.TopDefault", "- select state -"));
			CountryDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), null, CodeLocalise("Global.Country.TopDefault", "- select country -"));
			CountyDropDownList.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Counties), null, CodeLocalise("Global.County.TopDefault", "- select county -"));
		}

		/// <summary>
		/// Binds the specified office.
		/// </summary>
		/// <param name="office">The office.</param>
		private void Bind(OfficeDto office)
		{
			OfficeStatusTextBox.Text = office.InActive ? CodeLocalise("Global.Inactive", "Inactive") : CodeLocalise("Global.Active", "Active");
			OfficeStatusPlaceHolder.Visible = true;

			IdHiddenField.Value = office.Id.ToString();
			OfficeNameTextBox.Text = office.OfficeName;
			Line1TextBox.Text = office.Line1;
			Line2TextBox.Text = office.Line2;
			PostcodeZipTextBox.Text = office.PostcodeZip;
			TownCityTextBox.Text = office.TownCity;
			StateDropDownList.SelectValue(office.StateId.ToString(CultureInfo.InvariantCulture));
			OfficeManagerMailboxTextBox.Text = office.OfficeManagerMailbox;
			BusinessOutreachMailboxTextBox.Text = office.BusinessOutreachMailbox;

			StaffMemberList.Clear();

			var assignedStaff = ServiceClientLocator.EmployerClient(App).GetOfficeStaffMembersList(new StaffMemberCriteria { OfficeIds = new List<long?> { office.Id } });

			var selectedStaff = assignedStaff.Select(staff => new UpdateableClientListItem
			{
				Value = staff.Id.ToString(),
				Label = staff.FirstName + " " + staff.LastName,
				IsDefault = staff.Id == office.DefaultAdministratorId
			}).ToList();

			StaffMemberList.SetSelectedItems(selectedStaff);

			CountyDropDownListCascadingDropDown.PromptText = CodeLocalise("Global.County.TopDefault", "- select county -");
			CountyDropDownListCascadingDropDown.LoadingText = CodeLocalise("Global.County.Progress", "[Loading counties ...]");
			CountyDropDownListCascadingDropDown.PromptValue = string.Empty;

			CountyDropDownList.SelectValue(office.CountyId.ToString());
			CountyDropDownListCascadingDropDown.SelectedValue = office.CountyId.ToString();
			CountryDropDownList.SelectValue(office.CountryId.ToString());
			AssignPostalZipTextBox.Text = office.AssignedPostcodeZip;

			// office default icons
			DefaultOfficeGeneralIcon.Visible = DefaultOfficeEmployerIcon.Visible = DefaultOfficeJobOrderIcon.Visible = DefaultOfficeJobSeekerIcon.Visible = false;

			var defaultType = office.DefaultType;

			if (defaultType.HasFlag(OfficeDefaultType.All))
			{
				ShowDefaultOfficeIcon(DefaultOfficeGeneralIcon, UrlBuilder.DefaultOfficeGeneralIconLarge(), CodeLocalise("DefaultOfficeGeneral.ToolTip", "This is the default office for all unassigned records"));
			}
			else
			{
				if (defaultType.HasFlag(OfficeDefaultType.Employer))
				{
					ShowDefaultOfficeIcon(DefaultOfficeEmployerIcon, UrlBuilder.DefaultOfficeEmployerIconLarge(), CodeLocalise("DefaultOfficeEmployer.ToolTip", "This is the default office for all unassigned #BUSINESSES#:LOWER"));
				}
				if (defaultType.HasFlag(OfficeDefaultType.JobOrder))
				{
					ShowDefaultOfficeIcon(DefaultOfficeJobOrderIcon, UrlBuilder.DefaultOfficeJobOrderIconLarge(), CodeLocalise("DefaultOfficeJobOrder.ToolTip", "This is the default office for all unassigned #EMPLOYMENTTYPES#LOWER"));
				}
				if (defaultType.HasFlag(OfficeDefaultType.JobSeeker))
				{
					ShowDefaultOfficeIcon(DefaultOfficeJobSeekerIcon, UrlBuilder.DefaultOfficeJobSeekerIconLarge(), CodeLocalise("DefaultOfficeJobSeeker.ToolTip", "This is the default office for all unassigned job seekers"));
				}
			}

			ExternalIdTextBox.Text = office.ExternalId;

			OfficeNameTextBox.Enabled =
				OfficeNameTextBox.Enabled =
				Line1TextBox.Enabled =
				Line2TextBox.Enabled =
				PostcodeZipTextBox.Enabled =
				TownCityTextBox.Enabled =
				StateDropDownList.Enabled =
				OfficeManagerMailboxTextBox.Enabled =
				BusinessOutreachMailboxTextBox.Enabled =
				CountyDropDownList.Enabled =
				CountryDropDownList.Enabled =
				AssignPostalZipTextBox.Enabled =
				ExternalIdTextBox.Enabled = App.User.IsInRole(Constants.RoleKeys.AssistOfficesViewEdit);

			SaveButton.Visible = App.User.IsInRole(Constants.RoleKeys.AssistOfficesViewEdit);

			ActivateButton.Visible = office.InActive && App.User.IsInRole(Constants.RoleKeys.AssistOfficesActivateDeactivate);
			DeactivateButton.Visible = !office.InActive && App.User.IsInRole(Constants.RoleKeys.AssistOfficesActivateDeactivate);
		}

		/// <summary>
		/// Finds and enables a default office icon.
		/// </summary>
		/// <param name="defaultOfficeIcon">The default office icon.</param>
		/// <param name="imageUrl">The source URL for the icon.</param>
		/// <param name="tooltip">The tooltip for the icon.</param>
		private static void ShowDefaultOfficeIcon(Image defaultOfficeIcon, string imageUrl, string tooltip)
		{
			if (!defaultOfficeIcon.IsNotNull()) return;
			defaultOfficeIcon.ImageUrl = imageUrl;
			defaultOfficeIcon.AlternateText = defaultOfficeIcon.ToolTip = tooltip;
			defaultOfficeIcon.Visible = true;
		}

		/// <summary>
		/// Saves this instance.
		/// </summary>
		/// <returns>A boolean indicating if it has been saved or not</returns>
		private bool Save()
		{
			if (!ValidateStaffMembers())
				return false;
            
			var staffIds = StaffMemberList.SelectedItems.Select(staff => Convert.ToInt64(staff.Value)).ToList();
			var defaultAdministrator = StaffMemberList.SelectedItems.Where(x => x.IsDefault).Select(x => x.Value).FirstOrDefault().ToLong();

			var office = new OfficeDto
			{
				Id = !String.IsNullOrEmpty(IdHiddenField.Value) ? Convert.ToInt64(IdHiddenField.Value) : (long?)null,
				OfficeName = OfficeNameTextBox.Text,
				Line1 = Line1TextBox.Text,
				Line2 = Line2TextBox.Text,
				PostcodeZip = PostcodeZipTextBox.Text,
				TownCity = TownCityTextBox.Text,
				StateId = Convert.ToInt64(StateDropDownList.SelectedValue),
				CountyId = CountyDropDownList.SelectedValue.IsNotNullOrEmpty() && CountyDropDownList.SelectedValue != "" ? Convert.ToInt64(CountyDropDownList.SelectedValue) : Convert.ToInt64(null),
				CountryId = CountryDropDownList.SelectedValue.IsNotNullOrEmpty() && CountryDropDownList.SelectedValue != "" ? Convert.ToInt64(CountryDropDownList.SelectedValue) : Convert.ToInt64(null),
				AssignedPostcodeZip = AssignPostalZipTextBox.Text,
				OfficeManagerMailbox = OfficeManagerMailboxTextBox.TextTrimmed(),
				BusinessOutreachMailbox = BusinessOutreachMailboxTextBox.TextTrimmed(),
				ExternalId = ExternalIdTextBox.TextTrimmed(),
				DefaultAdministratorId = defaultAdministrator
			};

			var result = ServiceClientLocator.EmployerClient(App).SaveOffice(office);

            if (result == ErrorTypes.Ok)
			{
				#region Save assigned staff

                var personOfficeMapper = (from long staffId in staffIds
                                                               select new PersonOfficeMapperDto()
                                                               {
                                                                   OfficeId = office.Id,
                                                                   PersonId = staffId
                                                               }).ToList() ;

                if (staffIds.Count == 0)
                {
                    personOfficeMapper.Add(new PersonOfficeMapperDto()
                                                               {
                                                                   OfficeId = office.Id,
                                                               });
                }

				// Save assigned staff members
				ServiceClientLocator.EmployerClient(App).AssignOfficeStaff(personOfficeMapper);

                #endregion

				// Update the current user's offices if we've assigned/removed the user to/from this office
				if (staffIds.Any(x => x.Equals(App.User.PersonId)))
					App.GetOfficesForUser(App.User.PersonId, true);

    			return true;
			}

			ExternalIdDuplicate.IsValid = false;
			return false;
		}

		/// <summary>
		/// Validates the staff members.
		/// </summary>
		/// <returns></returns>
		private bool ValidateStaffMembers()
		{
			var pass = true;

			// If EnableOfficeDefaultAdmin = true then perform validation on default administrator
			if (App.Settings.EnableOfficeDefaultAdmin)
			{
				var assignedStaffCount = StaffMemberList.SelectedItems.Count;
				var defaultAdminCount = StaffMemberList.SelectedItems.Count(x => x.IsDefault);

				// Check that staff have been assigned first otherwise "Default administrator is required" doesn't make much sense
				if (assignedStaffCount == 0)
				{
					StaffMemberListValidator.ErrorMessage = CodeLocalise("AtLeastOneStaffMemberDefaultAdmin.ErrorMessage", "At least one assigned staff member/default administrator is required");
					StaffMemberListValidator.IsValid = false;
					pass = false;
				}
				// A default administrator must be assigned
				else if (defaultAdminCount == 0)
				{
					StaffMemberListValidator.ErrorMessage = CodeLocalise("DefaultAdministratorRequired.ErrorMessage", "Default administrator is required");
					StaffMemberListValidator.IsValid = false;
					pass = false;
				}
				// Make sure only one staff member has been chosen as default - this shouldn't be possible with the UpdateableClientList but it's here just in case
				else if (defaultAdminCount > 1)
				{
					StaffMemberListValidator.ErrorMessage = CodeLocalise("OnlyOneDefaultAdministrator.ErrorMessage", "Only one default adminsitrator is permitted");
					StaffMemberListValidator.IsValid = false;
					pass = false;
				}
			}

			return pass;
		}


		/// <summary>
		/// Confirms the reactivate.
		/// </summary>
		/// <returns></returns>
		public void ConfirmReactivate()
		{
			ConfirmationModal.Show(
				CodeLocalise("ReactivateOfficeModal.Title", "Reactivate Office"),
				CodeLocalise("ReactivateOfficeModal.Body", "If you are sure you want to reactivate this office, please select OK"),
				CodeLocalise("Global.Cancel.Text", "Cancel"),
				okText: CodeLocalise("Confirmation.Ok.Text", "OK"),
				okCommandName: "ActivateOffice",
				okCommandArgument: OfficeId.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Activates the office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		private void ActivateOffice(long officeId)
		{
			var activateDeactivateModel = ServiceClientLocator.EmployerClient(App).ActivateDeactivateOffice(officeId, false);

			if (activateDeactivateModel.Error == ErrorTypes.OfficeIsNotInactive)
			{
				UpdateSuccessModal.Show(
					CodeLocalise("UnableToDeactivateOffice.Title", "Unable to activate"),
					CodeLocalise("UnableToDeactivateOffice.ErrorText", "Office is not inactive"),
					CodeLocalise("Global.Title", "OK"));
			}
		}

		/// <summary>
		/// Determines whether [is office assigned] [the specified office id].
		/// </summary>
		/// <returns></returns>
		public void IsOfficeAssigned()
		{
			var assignedModel = ServiceClientLocator.EmployerClient(App).IsOfficeAssigned(OfficeId);

			if (assignedModel.IsAssigned)
			{
				var errorText = new StringBuilder();

				if (assignedModel.Errors.Count > 0)
				{
					foreach (var error in assignedModel.Errors)
					{
						switch (error)
						{
							case ErrorTypes.OfficeHasEmployers:
								if (errorText.Length > 0) errorText.Append("</br>");
								errorText.Append(CodeLocalise("OfficeHasEmployers.ErrorText", "There are #BUSINESSES#:LOWER assigned to this office"));
								break;

							case ErrorTypes.OfficeHasJobOrders:
								if (errorText.Length > 0) errorText.Append("</br>");
								errorText.Append(CodeLocalise("OfficeHasJobOrders.ErrorText", "There are #EMPLOYMENTTYPES#:LOWER assigned to this office"));
								break;

							case ErrorTypes.OfficeHasJobSeekers:
								if (errorText.Length > 0) errorText.Append("</br>");
								errorText.Append(CodeLocalise("OfficeHasJobSeekers.ErrorText", "There are job seekers assigned to this office"));
								break;

							case ErrorTypes.OfficeHasStaffMembers:
								if (errorText.Length > 0) errorText.Append("</br>");
								errorText.Append(CodeLocalise("OfficeHasStaffMembers.ErrorText", "There are staff members assigned to this office"));
								break;

							case ErrorTypes.OfficeHasAssignedZips:
								if (errorText.Length > 0) errorText.Append("</br>");
								errorText.Append(CodeLocalise("OfficeHasStaffMembers.ErrorText", "There are zip codes assigned to this office"));
								break;
						}
					}

					if (errorText.Length > 0) errorText.Append("</br></br>");
					errorText.Append(CodeLocalise("OfficeHasStaffMembers.ErrorText", "Please resolve the above issues before continuing"));

					UpdateSuccessModal.Show(
						CodeLocalise("UnableToDeactivateOffice.Title", "Unable to deactivate"),
						errorText.ToString(),
						CodeLocalise("Global.Title", "OK"));
				}
			}
			else
			{
				ConfirmationModal.Show(
					CodeLocalise("DeactivateOfficeModal.Title", "Deactivate Office"),
					CodeLocalise("DeactivateOfficeModal.Body", "If you are sure you want to deactivate this office, please select OK"),
					CodeLocalise("Global.Cancel.Text", "Cancel"),
					okText: CodeLocalise("Confirmation.Ok.Text", "OK"),
					okCommandName: "DeactivateOffice",
					okCommandArgument: OfficeId.ToString(CultureInfo.InvariantCulture));
			}
		}

		/// <summary>
		/// Deactivates the office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public void DeactivateOffice(long officeId)
		{
			var activateDeactivateModel = ServiceClientLocator.EmployerClient(App).ActivateDeactivateOffice(officeId, true);

			if (activateDeactivateModel.Error == ErrorTypes.OfficeIsNotActive)
			{
				UpdateSuccessModal.Show(
					CodeLocalise("UnableToDeactivateOffice.Title", "Unable to deactivate"),
					CodeLocalise("UnableToDeactivateOffice.ErrorText", "Office is not active"),
					CodeLocalise("Global.Title", "OK"));
			}
		}
	}
}
