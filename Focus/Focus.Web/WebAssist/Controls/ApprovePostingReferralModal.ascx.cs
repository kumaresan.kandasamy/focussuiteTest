﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Focus.Common;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Web.Code;
using Framework.Core;
using Framework.Exceptions;

#endregion


namespace Focus.Web.WebAssist.Controls
{
	public partial class ApprovePostingReferralModal : UserControlBase
	{
		private long _jobId
		{
			get { return GetViewStateValue<long>("DenyPostingReferralModal:JobId"); }
			set { SetViewStateValue("DenyPostingReferralModal:JobId", value); }
		}

		private long _employeeId
		{
			get { return GetViewStateValue<long>("DenyPostingReferralModal:EmployeeId"); }
			set { SetViewStateValue("DenyPostingReferralModal:EmployeeId", value); }
		}

		private EmailTemplateView _emailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>("DenyPostingReferralModal:EmailTemplate"); }
			set { SetViewStateValue("DenyPostingReferralModal:EmailTemplate", value); }
		}

		private int LockVersion
		{
			get { return GetViewStateValue<int>("DenyPostingReferralModal:LockVersion"); }
			set { SetViewStateValue("DenyPostingReferralModal:LockVersion", value); }
		}

		private bool JobPostingFiltered
		{
			get { return GetViewStateValue<bool>("DenyPostingReferralModal:JobPostingFiltered"); }
			set { SetViewStateValue("DenyPostingReferralModal:JobPostingFiltered", value); }
		}

		private const string FilteredUrl = "?filtered=true";

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Shows the specified job id.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="lockVersion">The lock version.</param>
		/// <param name="filtered">Return to filtered list</param>
		public void Show(long jobId, int lockVersion,bool filtered = false)
		{
			_jobId = jobId;
			LockVersion = lockVersion;
			JobPostingFiltered = filtered;

			GetEmailTemplate();

			if (_emailTemplate == null) return;

			EmailBodyLiteral.Text = _emailTemplate.Body.Replace(Environment.NewLine, "<br />");

			ModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the ApproveAndSendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ApproveAndSendMessageButton_Clicked(object sender, EventArgs e)
		{
			try
			{
				var response = ServiceClientLocator.JobClient(App).ApprovePostingReferral(_jobId, LockVersion);

				if (response == ErrorTypes.Ok)
				{
					ServiceClientLocator.EmployeeClient(App).EmailEmployee(_employeeId, _emailTemplate.Subject, _emailTemplate.Body, BCCMeCheckBox.Checked,
					                                                       ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.ApprovePostingReferral));

					ModalPopup.Hide();

					Confirmation.Show(CodeLocalise("ReferralApprovedConfirmation.Title", "Posting approved"),
					                  CodeLocalise("ReferralApprovedConfirmation.Body", "The posting has been approved."),
					                  CodeLocalise("CloseModal.Text", "OK"),
					                  closeLink: JobPostingFiltered ? UrlBuilder.PostingReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.PostingReferrals());
				}
				else
				{
					var message = response == ErrorTypes.EmployeeOnHold
													? CodeLocalise("ReferralEmployeeDenied.Body", "The business who posted this job has been put on hold to access #FOCUSTALENT#. Approval of their posting is not allowed unless the business is approved.")
						              : CodeLocalise("ReferralEmployeeDenied.Body", "The business who posted this job has been denied access to #FOCUSTALENT#. Approval of their posting is not allowed unless the business is approved.");

					Confirmation.Show(CodeLocalise("ReferralEmployeeDenied.Title", "Unable to approve"),
														message,
														CodeLocalise("CloseModal.Text", "OK"));
				}
			}
			catch (ServiceCallException ex)
			{
				Confirmation.Show(CodeLocalise("PostingApprovalError.Title", "Approval failed"),
					CodeLocalise("PostingApprovalError.Body", string.Format("There was a problem approving the referral.<br /><br />{0}", ex.Message)),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink: JobPostingFiltered ? UrlBuilder.PostingReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.PostingReferrals());
			}
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ApproveAndSendMessageButton.Text = CodeLocalise("ApproveAndSendMessageButton.Text.Text", "Email #BUSINESS#:LOWER");
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");
		}


		/// <summary>
		/// Gets the email template.
		/// </summary>
		private void GetEmailTemplate()
		{
			var jobPostingReferral = ServiceClientLocator.JobClient(App).GetJobPostingReferral(_jobId);

			if (!jobPostingReferral.IsNotNull())
			{

				Confirmation.Show(CodeLocalise("PostingApprovalError.Title", "Approval failed"),
					CodeLocalise("PostingApprovalError.Body", string.Format("There was a problem approving the referral.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.")),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink: UrlBuilder.PostingReferrals());

				return;
			}

			if (jobPostingReferral.EmployeeId.HasValue) _employeeId = jobPostingReferral.EmployeeId.Value;
			var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";

			var templateValues = new EmailTemplateData
			{
				RecipientName = String.Format("{0} {1}", jobPostingReferral.EmployeeFirstName, jobPostingReferral.EmployeeLastName),
				JobTitle = jobPostingReferral.JobTitle,
				SenderPhoneNumber =  Regex.Replace(phoneNumber, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat),
				SenderEmailAddress = App.User.EmailAddress,
                SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName),
                JobLinkUrls = new List<string>{string.Concat(App.Settings.TalentApplicationPath, UrlBuilder.JobView(_jobId, false))}
			};

			//Find out if the employer has been approved or not!
			var employer = ServiceClientLocator.EmployerClient(App).GetEmployer(jobPostingReferral.EmployerId);

			if (employer != null && !employer.IsAlreadyApproved)
				//If employer is not approved use this template
				_emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.PendingApprovePostingRequest, templateValues);
			else
				//Else use the standard template.
			_emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.ApprovePostingReferral, templateValues);
		}
	}
}