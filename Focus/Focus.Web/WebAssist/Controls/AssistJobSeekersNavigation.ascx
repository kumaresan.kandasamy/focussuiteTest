﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssistJobSeekersNavigation.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.AssistJobSeekersNavigation" %>
<%@ Import Namespace="Focus" %>

<p class="pageNav">
	<% 
		if (IsMessageJobSeekersPage() || IsManageJobSeekerListsPage() || IsSearchJobPostingsPage())
  	{%>
			<a href="<%=UrlBuilder.AssistJobSeekers()%>"><%= HtmlLocalise("AssistJobSeekers.LinkText", "#CANDIDATETYPE# dashboard")%></a> <%-- Replaced plural: Job Seekers --%>
		<% }
  	else
  	{ %>
			<strong><%= HtmlLocalise("AssistJobSeekers.LinkText", "#CANDIDATETYPE# dashboard")%></strong>
		<% }
		
		if (IsAssistJobSeekersAdministrator())
		{
		  if (OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Workforce)
		  {
		    if (!IsSearchJobPostingsPage())
		    { %>
				&nbsp;|&nbsp;<a href="<%= UrlBuilder.AssistNewSearchJobPostings() %>"><%= HtmlLocalise("SearchJobPostings.LinkText", "Search job postings") %></a>
			<% }
		    else
		    { %>
				&nbsp;|&nbsp;<strong><%= HtmlLocalise("SearchJobPostings.LinkText", "Search job postings") %></strong>
			<% }
		  }
			
			if (HasAssistJobSeekersSendMessagesRole())
			{
				if (!IsMessageJobSeekersPage())
			{%>
				&nbsp;|&nbsp;<a href="<%= UrlBuilder.MessageJobSeekers() %>"><%= HtmlLocalise("MessageJobSeekers.LinkText", IsWorkforce() ? "Manage #CANDIDATETYPE# communication" : "Send messages")%></a>
			<% }
			else
			{ %>
				&nbsp;|&nbsp;<strong><%= HtmlLocalise("MessageJobSeekers.LinkText", IsWorkforce() ? "Manage #CANDIDATETYPE# communication" : "Send messages")%></strong>
			<% }
			}

			if (!IsWorkforce())
			{
				if (!IsManageJobSeekerListsPage())
				{%>
					&nbsp;|&nbsp;<a href="<%= UrlBuilder.ManageJobSeekerLists() %>"><%= HtmlLocalise("ManageJobSeekerLists.LinkText", "Manage lists")%></a>
				<% }
				else
				{ %>
					&nbsp;|&nbsp;<strong><%= HtmlLocalise("ManageJobSeekerLists.LinkText", "Manage lists")%></strong>
				<% }
			}
		}
	%>
</p>