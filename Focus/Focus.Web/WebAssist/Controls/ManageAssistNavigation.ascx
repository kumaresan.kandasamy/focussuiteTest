﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageAssistNavigation.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ManageAssistNavigation" %>

<p class="pageNav">
	<% 
		if (IsSystemAdminUser())
		{
  		if (!IsSystemDefaultsPage())
  		{%>
				<asp:Literal ID="LocaliseManageSystemDefaults" runat="server" /><a href="<%=UrlBuilder.ManageSystemDefaults()%>"><%=HtmlLocalise("Global.ManageAssist.LinkText", "Set system defaults")%></a>&nbsp;|&nbsp;
			<% }
  		else
  		{ %>
				<strong><%=HtmlLocalise("Global.ManageAssist.LinkText", "Set system defaults")%></strong>&nbsp;|&nbsp;
			<% }
		}

        if (IsAccountAdminUser() || IsAssistStaffViewUser())
		{
			if (!IsStaffAccountsPage())
			{%>
				<asp:Literal ID="LocaliseManageStaffAccounts" runat="server" /><a href="<%= UrlBuilder.ManageStaffAccounts() %>"><%= HtmlLocalise("Global.ManageStaffAccounts.LinkText", "Manage staff accounts")%></a>&nbsp;|&nbsp;
			<% }
			else
			{ %>
				<strong><%=HtmlLocalise("Global.ManageStaffAccounts.LinkText", "Manage staff accounts")%></strong>&nbsp;|&nbsp;
			<% }
		}

		if (OfficesEnabled() && UserHasManageOfficePermissions())
		{
			if (!IsOfficePage())
			{%>
				<asp:Literal ID="LocaliseManageOffices" runat="server" /><a href="<%= UrlBuilder.ManageOffices() %>" id="ManageAssist_ManageOfficesLink"><%= HtmlLocalise("Global.ManageOffices.LinkText", "Manage Offices")%></a>&nbsp;|&nbsp;
			<% }
			else
			{ %>
				<strong><%=HtmlLocalise("Global.ManageOffices.LinkText", "Manage Offices")%></strong>&nbsp;|&nbsp;
			<% }
		}
                if (App.Settings.EnableJobSeekerActivitiesServices || App.Settings.EnableHiringManagerActivitiesServices)
                {
                    if (!IsManageActivityService())
                    {%>
            <asp:Literal ID="LocaliseManageActivityService" runat="server" /><a href ="<%= UrlBuilder.ManageActivityServiceDefaults() %>" id="ManageAssit_ManageActivitiesServicesLink"><%= HtmlLocalise("Global.ManageActivitiesServices.LinkText", "Manage Activities/Services")%></a>&nbsp;|&nbsp;         
                     <%
                    
                    }
                    else
                    {
                    %> 
                     <strong><%=HtmlLocalise("Global.ManageActivitiesServices.LinkText", "Manage Activities/Services")%></strong>&nbsp;|&nbsp;
                     <%
                    }
                }

		if (IsSystemOrAccountAdminUser() && App.Settings.EnableAssistErrorHandling)
		{
      if (!IsCaseManagementPage())
			{%>
				<asp:Literal ID="LocaliseCaseManagementErrors" runat="server" /><a href="<%= UrlBuilder.CaseManagementErrors() %>" id="ManageAssist_CaseManagementErrorsLink"><%= HtmlLocalise("Global.CaseManagementErrors.LinkText", "Case management errors")%></a>&nbsp;|&nbsp;
			<% }
			else
			{ %>
				<strong><%=HtmlLocalise("Global.CaseManagementErrors.LinkText", "Case management errors")%></strong>&nbsp;|&nbsp;
			<% }
		}

		if (IsStudentAccountAdministrator() && IsEducationTheme())
    {
			if (!IsStudentAccountsPage() )
			{%>
				<asp:Literal ID="LocaliseManageStudentAccounts" runat="server" /><a href="<%= UrlBuilder.ManageStudentAccounts() %>"><%= HtmlLocalise("Global.ManageStudentAccounts.LinkText", "Manage student accounts")%></a>&nbsp;|&nbsp;
			<% }
			else
			{ %>
				<strong><%=HtmlLocalise("Global.ManageStudentAccounts.LinkText", "Manage student accounts")%></strong>&nbsp;|&nbsp;
			<% }
     }

		if(IsBroadcastsAdminUser())
		{
			if (!IsAnnouncementsPage())
			{%>
				<asp:Literal ID="LocaliseManageAnnouncements" runat="server" /><a href="<%= UrlBuilder.ManageAnnouncements() %>"><%= HtmlLocalise("Global.ManageAnnouncements.LinkText", "Manage home page announcements")%></a>&nbsp;|&nbsp;
			<% }
			else
			{ %>
				<strong><%=HtmlLocalise("Global.ManageAnnouncements.LinkText", "Manage home page announcements")%></strong>&nbsp;|&nbsp;
			<% }
		}

    if (IsAssistManageDocuments())
		{
		  if (!IsDocumentsPage())
		  { %>
			  <asp:Literal ID="LocaliseManageDocuments" runat="server" /><a href="<%= UrlBuilder.ManageDocuments() %>"><%= HtmlLocalise("Global.ManageDocuments.LinkText", "Manage documents") %></a>&nbsp;|&nbsp;
		  <% }
		  else
		  { %>
			  <strong><%= HtmlLocalise("Global.ManageDocuments.LinkText", "Manage documents") %></strong>&nbsp;|&nbsp;
		  <% }
		}

		if (!IsFeedbackPage())
		{%>
			<asp:Literal ID="LocaliseAssistFeedback" runat="server" /><a href="<%=UrlBuilder.AssistFeedback()%>"><%=HtmlLocalise("Global.AssistFeedback.LinkText", "Send feedback")%></a>
		<% }
		else
    { %>
			<strong><%=HtmlLocalise("Global.AssistFeedback.LinkText", "Send feedback")%></strong>
		<% } %>
</p>
