﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Common;
using Focus.Core.Criteria.Employer;

#endregion

namespace Focus.Web.WebAssist.Controls
{
    public partial class AssignedOffices : UserControlBase
    {

        private AssignedOfficeAdministratorType _administrationType = AssignedOfficeAdministratorType.JobSeeker;

        /// <summary>
        /// Gets or sets a flag indicating whether the default office has been removed from the assigned offices list.
        /// </summary>
        public bool DefaultOfficeRemoved
        {
            get { return GetViewStateValue<bool>("AssignedOfficesPanel:DefaultOfficeRemoved"); }
            private set { SetViewStateValue("AssignedOfficesPanel:DefaultOfficeRemoved", value); }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether the default office is assigned initially
        /// </summary>
        public bool DefaultOfficeAssigned
        {
            get { return GetViewStateValue<bool>("AssignedOfficesPanel:DefaultOfficeAssigned"); }
            private set { SetViewStateValue("AssignedOfficesPanel:DefaultOfficeAssigned", value); }
        }

        /// <summary>
        /// Gets or sets the list of assigned offices.
        /// </summary>
        public List<AssignedOfficeModel> AssignedOfficeList
        {
            get
            {
                var list = GetViewStateValue<List<AssignedOfficeModel>>("AssignedOffices:AssignedOfficeList");
                if (list.IsNull())
                {
                    list = new List<AssignedOfficeModel>();
                    SetViewStateValue("AssignedOffices:AssignedOfficeList", list);
                }
                return list;
            }
            private set { SetViewStateValue("AssignedOffices:AssignedOfficeList", value); }
        }

        /// <summary>
        /// Gets or sets the jobseeker / employer ID.
        /// </summary>
        public long ContextId
        {
            get { return GetViewStateValue<long>("AssignedOffices:ContextId"); }
            set { SetViewStateValue("AssignedOffices:ContextId", value); }
        }

        /// <summary>
        /// Gets or sets the entity type we are assigning offices to.
        /// </summary>
        public AssignedOfficeAdministratorType AdministrationType
        {
            get { return _administrationType; }
            set { _administrationType = value; }
        }

        /// <summary>
        /// Gets or sets a flag indicating whether for default offices should be assigned for statewide handlers
        /// </summary>
        public bool ReturnNoOfficesForStatewide { get; set; }

        /// <summary>
        /// Gets or sets the name of the command to assign to the "Add" button.
        /// </summary>
        public string AddButtonCommandName { get; set; }

        public event EventHandler SaveAssignedOfficesClick;

        protected virtual void OnSaveAssignedOfficesClick(EventArgs e)
        {
            var handler = SaveAssignedOfficesClick;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler AssignedOfficesDataBinding;

        protected virtual void OnAssignedOfficesDataBinding(EventArgs e)
        {
            var handler = AssignedOfficesDataBinding;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Handles the page init event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (AddButtonCommandName.IsNotNullOrEmpty())
            {
                AddOfficeButton.CommandName = AddButtonCommandName;
            }
            else
            {
                AddOfficeButton.Click += new EventHandler(AddOfficeButtonClick);
            }
        }

        /// <summary>
        /// Handles the page load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!App.Settings.OfficesEnabled)
                return;

            if (DefaultOfficeAssigned)
            {
                if (App.Settings.OfficesEnabled &&
                    ((AdministrationType == AssignedOfficeAdministratorType.StaffAccount) ||
                     (AdministrationType == AssignedOfficeAdministratorType.JobSeeker && App.User.IsInRole(Constants.RoleKeys.AssistAssignJobSeekerOfficeFromDefault)) ||
                     (AdministrationType == AssignedOfficeAdministratorType.Employer && App.User.IsInRole(Constants.RoleKeys.AssistAssignBusinessOfficeFromDefault)) ||
                     (AdministrationType == AssignedOfficeAdministratorType.JobOrder && App.User.IsInRole(Constants.RoleKeys.ReassignJobPostingToNewOffice))))
                {

                    OfficeDropDownList.Enabled = true;
                    AddOfficeButton.Enabled = true;

                    if (AdministrationType != AssignedOfficeAdministratorType.StaffAccount)
                        SaveButton.Enabled = true;

                }
            }
            else
            {
                if (App.Settings.OfficesEnabled &&
                    ((AdministrationType == AssignedOfficeAdministratorType.StaffAccount) ||
                     (AdministrationType == AssignedOfficeAdministratorType.JobSeeker && App.User.IsInRole(Constants.RoleKeys.ReassignJobSeekerToNewOffice)) ||
                     (AdministrationType == AssignedOfficeAdministratorType.Employer && App.User.IsInRole(Constants.RoleKeys.ReassignBusinessToNewOffice)) ||
                     (AdministrationType == AssignedOfficeAdministratorType.JobOrder && App.User.IsInRole(Constants.RoleKeys.ReassignJobPostingToNewOffice))))
                {
                    OfficeDropDownList.Enabled = true;
                    AddOfficeButton.Enabled = true;

                    if (AdministrationType != AssignedOfficeAdministratorType.StaffAccount)
                        SaveButton.Enabled = true;
                }
            }



            if (AdministrationType == AssignedOfficeAdministratorType.StaffAccount)
            {
                SaveButton.Visible = false;
            }

            Localise();
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void Localise()
        {
            if (!App.Settings.OfficesEnabled) return;
            SaveButton.Text = CodeLocalise("SaveButton.Label.NoEdit", "Update");
            AddOfficeButton.Text = CodeLocalise("AddOfficeButton.Label.NoEdit", "Add");
        }

        /// <summary>
        /// Binds everything(!)
        /// </summary>
        public void Bind()
        {
            DefaultOfficeRemoved = false;

            BindOffices();
            BindAssignedOffices();
        }

        /// <summary>
        /// Binds the assigned offices repeater.
        /// </summary>
        private void BindAssignedOffices()
        {
            OfficesRepeater.DataSource = AssignedOfficeList;
            OfficesRepeater.DataBind();
        }

        /// <summary>
        /// Binds the offices.
        /// </summary>
        private void BindOffices()
        {
            List<OfficeDto> offices;

            var defaultOfficeType = OfficeDefaultType.None;

            switch (AdministrationType)
            {
                case AssignedOfficeAdministratorType.JobSeeker:
                    defaultOfficeType = OfficeDefaultType.JobSeeker;
                    OfficeDropDownList.DataSource = ServiceClientLocator.EmployerClient(App).GetOfficesExcludingDefaultList(new OfficeCriteria { InActive = false, DefaultType = defaultOfficeType });
                    // Get the person's offices and add them to the list
                    offices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { PersonId = ContextId });
                    break;
                case AssignedOfficeAdministratorType.Employer:
                    defaultOfficeType = OfficeDefaultType.Employer;
                    OfficeDropDownList.DataSource = ServiceClientLocator.EmployerClient(App).GetOfficesExcludingDefaultList(new OfficeCriteria { InActive = false, DefaultType = defaultOfficeType });
                    offices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { EmployerId = ContextId });
                    break;
                case AssignedOfficeAdministratorType.StaffAccount:
                    OfficeDropDownList.DataSource = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { InActive = false });
                    offices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { PersonId = ContextId, ReturnNoOfficesForStatewide = ReturnNoOfficesForStatewide });
                    break;
                case AssignedOfficeAdministratorType.JobOrder:
                    defaultOfficeType = OfficeDefaultType.JobOrder;
                    OfficeDropDownList.DataSource = ServiceClientLocator.EmployerClient(App).GetOfficesExcludingDefaultList(new OfficeCriteria { InActive = false, DefaultType = defaultOfficeType });
                    offices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { JobId = ContextId });
                    break;
                default:
                    offices = null;
                    break;
            }
            OfficeDropDownList.DataValueField = "Id";
            OfficeDropDownList.DataTextField = "OfficeName";
            OfficeDropDownList.DataBind();
            OfficeDropDownList.Items.AddLocalisedTopDefault("AssignedOfficeDropDownList.TopDefault.NoEdit", "- select an office -");

            if (!offices.IsNotNullOrEmpty()) return;

            DefaultOfficeAssigned = false;

            if (offices.Count == 1)
            {
                if (defaultOfficeType != OfficeDefaultType.None && (offices[0].DefaultType & defaultOfficeType) == defaultOfficeType)
                    DefaultOfficeAssigned = true;
            }

            foreach (var office in offices)
            {
                AssignedOfficeList.Add(new AssignedOfficeModel
                                                {
                                                    OfficeId = office.Id,
                                                    OfficeName = office.OfficeName
                                                });

                //if (defaultOfficeType != OfficeDefaultType.None && (office.DefaultType & defaultOfficeType) == defaultOfficeType)
                //    DefaultOfficeAssigned = true;
            }
        }

        /// <summary>
        /// Handles the Click event of the AddOfficeButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void AddOfficeButtonClick(object sender, EventArgs e)
        {
            AddOffice();
        }

        /// <summary>
        /// Public method for adding a selected office.
        /// </summary>
        public void AddOffice()
        {
            var officeId = OfficeDropDownList.SelectedValue;
            var officeName = OfficeDropDownList.SelectedItem.Text;

            if (officeId != "")
            {
                var existingItem = AssignedOfficeList.Exists(x => x.OfficeId.ToString() == officeId);

                // Only add the Office if it doesn't already exist in the list
                if (!existingItem)
                {
                    AssignedOfficeList.Add(new AssignedOfficeModel
                    {
                        OfficeId = long.Parse(officeId),
                        OfficeName = officeName
                    });

                    // Automatically remove the default office from the list when an office is assigned
                    if (AdministrationType != AssignedOfficeAdministratorType.StaffAccount)
                    {
                        var type = GetOfficeType();

                        var defaultOfficeId = GetDefaultOfficeId(type);

                        if (defaultOfficeId.IsNotNull() && defaultOfficeId.Value.ToString(CultureInfo.InvariantCulture) != officeId)
                        {
                            var assignedDefaultOffice = AssignedOfficeList.Find(x => x.OfficeId == defaultOfficeId);

                            if (assignedDefaultOffice.IsNotNull())
                            {
                                AssignedOfficeList.Remove(assignedDefaultOffice);

                                DefaultOfficeRemoved = true;
                            }
                        }
                    }

                    BindAssignedOffices();
                }
                else
                {
                    OfficeExistsValidator.IsValid = false;
                    OfficeExistsValidator.Text = HtmlLocalise("OfficeExistsValidator.Error", "{0} has already been selected", officeName);
                }

                OfficeDropDownList.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Handles a click on the save button.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event args.</param>
        protected void SaveButtonClick(object sender, EventArgs e)
        {
            OnSaveAssignedOfficesClick(EventArgs.Empty);
            DefaultOfficeRemoved = false;
        }

        /// <summary>
        /// Handles the ItemDataBound event of the OfficesRepeater control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OfficesRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
            var jobSeekerOffice = (AssignedOfficeModel)e.Item.DataItem;
            var officeNameDiv = (Label)e.Item.FindControl("OfficeName");
            var removeIconButton = (LinkButton)e.Item.FindControl("RemoveOfficeButton");
            var removeIconLabel = (Label)e.Item.FindControl("RemoveOfficeLabel");
            officeNameDiv.Text = jobSeekerOffice.OfficeName;
            removeIconButton.CommandArgument = jobSeekerOffice.OfficeId.ToString();
            removeIconButton.ToolTip = removeIconLabel.Text = CodeLocalise("RemoveOffice.ToolTip", "Remove office");


            if (App.Settings.OfficesEnabled)
            {
                if (DefaultOfficeAssigned)
                {
                    if ((AdministrationType == AssignedOfficeAdministratorType.JobSeeker && !App.User.IsInRole(Constants.RoleKeys.AssistAssignJobSeekerOfficeFromDefault)) ||
                        (AdministrationType == AssignedOfficeAdministratorType.Employer && !App.User.IsInRole(Constants.RoleKeys.AssistAssignBusinessOfficeFromDefault)) ||
                        (AdministrationType == AssignedOfficeAdministratorType.JobOrder && !App.User.IsInRole(Constants.RoleKeys.ReassignJobPostingToNewOffice)))
                    {
                        removeIconButton.Visible = false;
                    }
                    else
                    {
                        removeIconButton.Visible = true;
                    }
                }
                else
                {
                    if ((AdministrationType == AssignedOfficeAdministratorType.JobSeeker && !App.User.IsInRole(Constants.RoleKeys.ReassignJobSeekerToNewOffice)) ||
                        (AdministrationType == AssignedOfficeAdministratorType.Employer && !App.User.IsInRole(Constants.RoleKeys.ReassignBusinessToNewOffice)) ||
                        (AdministrationType == AssignedOfficeAdministratorType.JobOrder && !App.User.IsInRole(Constants.RoleKeys.ReassignJobPostingToNewOffice)))
                    {
                        removeIconButton.Visible = false;
                    }
                    else
                    {
                        removeIconButton.Visible = true;
                    }
                }
                // Enable remove button for manage staff account page
                if (AdministrationType == AssignedOfficeAdministratorType.StaffAccount)
                    removeIconButton.Visible = true;

            }
        }

        /// <summary>
        /// Handles the ItemCommand event of the OfficesRepeater control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OfficesRepeaterItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName != "RemoveOffice") return;
            var selectedOfficeId = Convert.ToInt64(e.CommandArgument);
            RemoveOffice(selectedOfficeId);

        }

        /// <summary>
        /// Public method for removing an office.
        /// </summary>
        /// <param name="officeId"></param>
        public void RemoveOffice(long officeId)
        {
            if (App.Settings.EnableOfficeDefaultAdmin)
            {
                var defaultAdministratorModel = ServiceClientLocator.EmployerClient(App).IsDefaultAdministrator(ContextId, officeId);
                if (defaultAdministratorModel.IsDefaultAdministrator)
                {
                    OfficeExistsValidator.IsValid = false;
                    OfficeExistsValidator.ErrorMessage = CodeLocalise("UserIsDefaultAdministrator.ErrorMessage", "This user is the default administrator for this office. Please replace the default administrator on the office record before carrying out this action");
                    return;
                }
            }

            var type = GetOfficeType();

            if (AdministrationType != AssignedOfficeAdministratorType.StaffAccount && officeId == GetDefaultOfficeId(type))
                DefaultOfficeRemoved = true;

            var assignedDefaultOffice = AssignedOfficeList.Find(x => x.OfficeId == officeId);
            AssignedOfficeList.Remove(assignedDefaultOffice);

            BindAssignedOffices();
        }

        /// <summary>
        /// Handles the databinding event of the offices repeater.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OfficesRepeaterDataBinding(object sender, EventArgs e)
        {
            OnAssignedOfficesDataBinding(EventArgs.Empty);
        }

        /// <summary>
        /// Server validation method for the AssignedOfficeRequired custom validator.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        protected void AssignedOfficeRequiredServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = AssignedOfficeList.Count > 0;
        }

        /// <summary>
        /// Invalidates the OfficeExistsValidator control, displaying a specified message.
        /// </summary>
        /// <param name="errorMessage">The error message to display to the user.</param>
        public void Invalidate(string errorMessage)
        {
            OfficeExistsValidator.IsValid = false;
            OfficeExistsValidator.ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets the id of the default office, if it exists
        /// </summary>
        /// <returns>The id of the default office (NULL if it doesn't exist)</returns>
        private long? GetDefaultOfficeId(OfficeDefaultType type)
        {
            var defaultOffice = ServiceClientLocator.EmployerClient(App).GetDefaultOffice(type);
            return defaultOffice.IsNotNull() ? defaultOffice.Id : null;
        }

        /// <summary>
        /// Gets the default office type
        /// </summary>
        /// <returns>The default office type</returns>
        private OfficeDefaultType GetOfficeType()
        {
            switch (AdministrationType)
            {
                case AssignedOfficeAdministratorType.Employer:
                    return OfficeDefaultType.Employer;
                case AssignedOfficeAdministratorType.JobOrder:
                    return OfficeDefaultType.JobOrder;
                case AssignedOfficeAdministratorType.JobSeeker:
                    return OfficeDefaultType.JobSeeker;
            }

            return OfficeDefaultType.All; ;
        }
    }

    /// <summary>
    /// A model class representing the basic details needed to display an office in the UI.
    /// </summary>
    [Serializable]
    public class AssignedOfficeModel
    {
        /// <summary>
        /// The unique ID of the office.
        /// </summary>
        public long? OfficeId { get; set; }
        /// <summary>
        /// The name of the office.
        /// </summary>
        public string OfficeName { get; set; }
    }

    /// <summary>
    /// An enumeration of possible types of entity to administer.
    /// </summary>
    public enum AssignedOfficeAdministratorType
    {
        /// <summary>
        /// A job seeker.
        /// </summary>
        JobSeeker,
        /// <summary>
        /// An employer.
        /// </summary>
        Employer,
        /// <summary>
        /// A staff account.
        /// </summary>
        StaffAccount,
        /// <summary>
        /// A job order.
        /// </summary>
        JobOrder
    }
}