﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateStaffAccountModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.CreateStaffAccountModal" %>

<asp:HiddenField ID="CreateStaffAccountModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="CreateStaffAccountModalPopup" runat="server"
												BehaviorID="CreateStaffAccountModal"
												TargetControlID="CreateStaffAccountModalDummyTarget"
												PopupControlID="CreateStaffAccountModalPanel"
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:panel id="CreateStaffAccountModalPanel" runat="server" class="modal" style="display:none;">
	<h1>
		<focus:LocalisedLabel runat="server" ID="CreateStaffAccountHeadingLabel" LocalisationKey="CreateStaffAccountHeading.Label" DefaultText="Create new staff account"/>
	</h1>
	<focus:LocalisedLabel runat="server" ID="CreateStaffAccountSubHeadingRequiredLabel" CssClass="requiredDataLegend" LocalisationKey="CreateStaffAccountSubHeadingRequired.Label" DefaultText="required fields"/>
	<div class="createStaffAccount">
		<div>
			<asp:Label runat="server" ID="ErrorLabel" CssClass="error" Visible="false" />
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="EmailAddressLabel" AssociatedControlID="EmailAddressTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="EmailAddress.Label" DefaultText="Email address"  />
			<span class="dataInputField">
				<asp:TextBox ID="EmailAddressTextBox" runat="server" ClientIDMode="Static" MaxLength="100" />
				<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount" />
				<asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount" />
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="ConfirmEmailAddressLocalisedLabel" AssociatedControlID="ConfirmEmailAddressTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="ConfirmEmailAddress.Label" DefaultText="Confirm email address"  />
			<span class="dataInputField">
				<asp:TextBox ID="ConfirmEmailAddressTextBox" runat="server" ClientIDMode="Static" MaxLength="100" />
				<asp:RequiredFieldValidator ID="ConfirmEmailAddressRequired" runat="server" ControlToValidate="ConfirmEmailAddressTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount" />
				<asp:CompareValidator ID="ConfirmEmailAddressCompare" runat="server" ControlToValidate="ConfirmEmailAddressTextBox" ControlToCompare="EmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount" />
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="FirstNameLocalisedLabe1" AssociatedControlID="FirstNameTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="FirstName.Label" DefaultText="First name"  />
			<span class="dataInputField">
				<asp:TextBox ID="FirstNameTextBox" runat="server" ClientIDMode="Static" MaxLength="50" />
				<asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount" />
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="InitialLocalisedLabe3" AssociatedControlID="InitialTextBox" CssClass="dataInputLabel" LocalisationKey="Initial.Label" DefaultText="Middle initial"  />
			<span class="dataInputField">
				<asp:TextBox ID="InitialTextBox" runat="server" ClientIDMode="Static" MaxLength="5" Width="95px" />
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="LastNameLocalisedLabel" AssociatedControlID="LastNameTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="LastName.Label" DefaultText="Last name"  />
			<span class="dataInputField">
				<asp:TextBox ID="LastNameTextBox" runat="server" ClientIDMode="Static" MaxLength="50" />
				<asp:RequiredFieldValidator ID="LastNameRequiredField" runat="server" ControlToValidate="LastNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount" />
			</span>
		</div>
				<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="TitleLocalisedLabel" AssociatedControlID="TitleDropDown" CssClass="dataInputLabel requiredData" LocalisationKey="Title.Label" DefaultText="Title"  />
			<span class="dataInputField">
				<asp:DropDownList ID="TitleDropDown" runat="server" ClientIDMode="Static" Width="100px" />
				<br/>
				<asp:RequiredFieldValidator ID="PersonalTitleRequired" runat="server" ControlToValidate="TitleDropDown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount" />
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="JobTitleLocalisedLabel" AssociatedControlID="JobTitleTextBox" CssClass="dataInputLabel" LocalisationKey="JobTitle.Label" DefaultText="Job title"  />
			<span class="dataInputField">
				<asp:TextBox ID="JobTitleTextBox" runat="server" ClientIDMode="Static" MaxLength="200" />
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="ExternalIdLocalisedLabel" AssociatedControlID="ExternalIdTextBox" CssClass="dataInputLabel" LocalisationKey="ExternalId.Label" DefaultText="External Id"  />
			<span class="dataInputField">
				<asp:TextBox ID="ExternalIdTextBox" runat="server" ClientIDMode="Static" MaxLength="200" />
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="PhoneNumberLocalisedLabel" AssociatedControlID="PhoneNumberTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="PhoneNumber.Label" DefaultText="Phone number"  />
			<span class="dataInputField">
				<asp:TextBox ID="PhoneNumberTextBox" runat="server" ClientIDMode="Static" MaxLength="20" />
				<asp:RequiredFieldValidator ID="PhoneNumberRequired" runat="server" ControlToValidate="PhoneNumberTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount" InitialValue="(___) ___-____"/>
				<act:MaskedEditExtender ID="PhoneNumberMaskedEdit" runat="server" MaskType="None" TargetControlID="PhoneNumberTextBox" PromptCharacter="_" ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" />
				<asp:RegularExpressionValidator ID="PhoneNumberRegEx" runat="server" ControlToValidate="PhoneNumberTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="CreateStaffAccount"/>
			</span>
		</div>
	</div>
	<div class="modalButtons">
		<asp:Button ID="CancelButton" runat="server" class="button1" CausesValidation="false" OnClick="CancelButton_Click" />
		<asp:Button ID="SaveButton" runat="server" class="button1" ValidationGroup="CreateStaffAccount" OnClick="SaveButton_Click" />
	</div>
</asp:panel>

<script type="text/javascript">
	function ValidatePhone(sender, args) {
		if (args.Value != "<%= PhoneNumberMask %>") {
			var phoneRegx = /^\([0-9]\d{2}\)\s?\d{3}\-\d{4}$/;
			if (phoneRegx.test(args.Value) == false) {
				args.IsValid = false;
			}
		}
	}
</script>