﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivityDeleteModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.DeleteModal" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="DeleteModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="DeletePanel" 
												PopupDragHandleControlID="DeleteSelectorPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="DeletePanel" runat="server" CssClass="modal" Width="350px" Height="150px"
        ClientIDMode="Static">
        <div>
            <asp:Panel ID="DeleteSelectorPanel" runat="server" Visible="True">
                <div style="font-style: italic;font-size: 20.5px;">
    
                    Delete Warning
                </div>
                <br />
                <div>
                    <focus:LocalisedLabel runat="server" ID="DeleteLabel" 
                        LocalisationKey="Delete.Label" DefaultText="Are you sure you want to permanently delete this activity/service from the log?" />
                </div>
                <div style="margin-top: 25px;margin-left:120px;">
                <asp:Button ID="DeleteCancelButton"   class="button1" runat="server" Text="Cancel" OnClick="DeleteCancelButton_Click" />
                <span style="margin-left:20px;"><asp:Button ID="DeleteButton"   class="button1" runat="server" Text="Delete" OnClick="DeleteButton_Click" /></span>
                </div>
                
            </asp:Panel>
        </div>
    </asp:Panel>