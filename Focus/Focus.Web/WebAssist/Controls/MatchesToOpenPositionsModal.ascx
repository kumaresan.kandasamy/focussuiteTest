﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchesToOpenPositionsModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.MatchesToOpenPositionsModal" %>

<%@ Register  Src="~/Code/Controls/User/EmailEmployeeModal.ascx"  TagName="EmailEmployeeModal"  TagPrefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="OpenPositionsModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="OpenPositionsModalPopup" runat="server" ClientIDMode="Static"
						TargetControlID="OpenPositionsModalDummyTarget"
						PopupControlID="OpenPositionsModalPanel"
						PopupDragHandleControlID="OpenPositionsModalHeaderPanel"
						RepositionMode="RepositionOnWindowResizeAndScroll"
						BackgroundCssClass="modalBackground" />

<asp:Panel ID="OpenPositionsModalPanel" runat="server" CssClass="modal" ClientIDMode="Static" Style="display:none">
  <asp:Panel runat="server" ID="OpenPositionsModalHeaderPanel">
		<table role="presentation">
					<tr>
						<td>
							<h2><%= HtmlLocalise("OpenPositionsWindow.Title", "Matches to open positions") %></h2>
						</td>
					</tr>
		</table>
	</asp:Panel>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
	<ContentTemplate>
  <table role="presentation">
    <tr>
			<td>
				<table role="presentation">
					<tr>
						<td>
							<div style="float:left; height: 500px; ">		
								<table role="presentation" style="width:500px;">
									<tr style="height: 30px;">
										<td><strong><%= HtmlLocalise("SuggestedMatchTitle", "#CANDIDATETYPE#")%></strong></td>	
										<td>&nbsp;&nbsp;&nbsp;</td>
									</tr>
									<tr>
										<td style="width:100%; border-style:solid; border-width: 1px; padding: 5px;">
											<div style="overflow:scroll;height:430px;">
												<asp:Literal ID="CandidateNameLiteral" runat="server" /><br/>
												<asp:Literal ID="CandidateResumeDescriptionLiteral" runat="server" />
											</div>
										</td>
									</tr>   
								</table>
							</div>
						</td>
						<td>
							<div style="float:right; height: 500px;">
								<table role="presentation" style="width:500px;">
									<tr style="height: 20px;">
										<td>
											<strong><%= HtmlLabel(OpenPositionsDropDown,"PositionTitle.Text", "Position ")%></strong>
											<asp:DropDownList runat="server" ID="OpenPositionsDropDown" OnSelectedIndexChanged="OpenPositionsDropDown_SelectedIndexChanged" AutoPostBack="True" />
											<asp:Image ID="PositionScoreImage" runat="server" Height="15px" Width="65px" Visible="False" AlternateText="Positions score"/>
										</td>
									</tr>
									<tr>
										<td  style="border-style:solid; border-width: 1px; padding: 5px;">
											<div style="overflow:scroll;height:430px;">
												<asp:Literal ID="JobTitleLiteral" runat="server"  />
												<asp:Literal ID="EmployerNameLiteral" runat="server" />
												<asp:Literal ID="JobDescriptionLiteral" runat="server" />
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<br/>
											<asp:Label ID="Label1" runat="server" CssClass="sr-only" AssociatedControlID="DoNotShowCheckBox">Do not show check box</asp:Label>
											<asp:CheckBox ID="DoNotShowCheckBox" runat="server"  OnCheckedChanged="DoNotShowCheckBox_CheckChanged" AutoPostBack="True" /> <%= HtmlLocalise("DoNotShowCheckBox.Text", "Do not show this suggestion again ")%>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
			</td>
    </tr>
    <tr>
      <td>
        <br />
        <asp:Button ID="ContactEmployerButton" runat="server" class="button2"  OnClick="ContactEmployerButton_Clicked" />
        &nbsp;&nbsp;
        <asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
      </td>
    </tr>
  </table>

	<%--<div class="closeIcon">
		<img src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('OpenPositionsModalPopup').hide();" />
	</div>--%>
	</ContentTemplate>
		<Triggers>
      <asp:AsyncPostBackTrigger ControlID="OpenPositionsDropDown" EventName="SelectedIndexChanged" /> 
    </Triggers>
	</asp:UpdatePanel>	
</asp:Panel>

<%-- Confirmation Modal --%>
<uc:EmailEmployeeModal ID="EmailEmployee" runat="server" />
<uc:ConfirmationModal ID="Confirmation" runat="server" Height="250px" Width="400px"/>


