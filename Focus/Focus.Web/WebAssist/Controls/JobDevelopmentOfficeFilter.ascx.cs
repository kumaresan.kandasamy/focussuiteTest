﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Employer;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class JobDevelopmentOfficeFilter : UserControlBase
	{
		/// <summary>
		/// Gets the office group.
		/// </summary>
		/// <value>The office group.</value>
		public OfficeGroup OfficeGroup
		{
			get { return OfficeGroupDropDown.SelectedValueToEnum(OfficeGroup.MyOffices); }
		}

		/// <summary>
		/// Gets the office id.
		/// </summary>
		/// <value>The office id.</value>
		public long? OfficeId
		{
			get { return OfficeDropDown.SelectedValueToNullableLong(); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				BindControl();
			}
		}

		/// <summary>
		/// Binds the control.
		/// </summary>
		private void BindControl()
		{
			BindOfficeGroupDropDown();
			BindOfficeDropDown();
		}

		/// <summary>
		/// Binds the office group drop down.
		/// </summary>
		private void BindOfficeGroupDropDown()
		{
			OfficeGroupDropDown.Items.Clear();

			OfficeGroupDropDown.Items.AddEnum(OfficeGroup.MyOffices, CodeLocalise("MyOffices.Text.TopDefault", "My offices"));
			OfficeGroupDropDown.Items.AddEnum(OfficeGroup.StatewideOffices, CodeLocalise("OfficesStatewide.Text.NoEdit", "Offices statewide"));
		}

		/// <summary>
		/// Binds the office drop down.
		/// </summary>
		private void BindOfficeDropDown()
		{
			OfficeDropDown.Items.Clear();

			var offices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria {OfficeGroup = OfficeGroup.MyOffices});
			OfficeDropDown.DataSource = offices;
			OfficeDropDown.DataValueField = "Id";
			OfficeDropDown.DataTextField = "OfficeName";
			OfficeDropDown.DataBind();
			
			OfficeDropDown.Items.AddLocalisedTopDefault("Offices.All.TopDefault", "- All offices -");
		}
	}
}