﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CareerDefaults.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.CareerDefaults" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register src="~/Code/Controls/User/UpdateablePairs.ascx" tagname="UpdateablePairs" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ImageUploader" Src="~/Code/Controls/User/ImageUploader.ascx" %>

<div style="width:100%"><asp:Button ID="SaveChangesTopButton" runat="server" class="button1 right" OnClick="SaveChangesButton_Click" /></div>
<br />
<br />

<asp:Panel ID="SearchDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="SearchDefaultsHeaderImage" runat="server" AlternateText="."/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("SearchDefaults.Label", "Search defaults")%></a></span>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="SearchDefaultsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table style="width:100%;" role="presentation">
			<tr>
				<td style="width:350px;"></td>
				<td style="width:300px;"></td>
				<td></td>
			</tr>
      <asp:PlaceHolder runat="server" ID="Workforce1Placeholder">
				<tr runat="server" ID="SearchTheseJobsRow">
					<td style="vertical-align: top;"><%= HtmlLabel("SearchTheseJobs.Label", "Set default job matching criteria")%></td>
					<td colspan="2" style="text-align:left;">
						<asp:RadioButton id="MinimumStarMatchRadioButton" GroupName="MinimumStarMatchGroup" runat="server"/> <%= HtmlLocalise("MinimumStarMatch.Text", "jobs matching resume with minimum match score of")%> 
						<asp:DropDownList runat="server" ID="MinimumStarMatchDropDown">
							<asp:ListItem>1</asp:ListItem>
							<asp:ListItem>2</asp:ListItem>
							<asp:ListItem Selected="true">3</asp:ListItem>
							<asp:ListItem>4</asp:ListItem>
							<asp:ListItem>5</asp:ListItem>
						</asp:DropDownList> <%= HtmlLocalise("Stars.Text", "stars")%>
					</td>
				</tr>
				<tr >
					<td></td>
					<td colspan="2">
						<asp:RadioButton id="AllJobsMatchRadioButton" GroupName="MinimumStarMatchGroup" runat="server" Checked="true"/> <%= HtmlLocalise("AllJobsMatch.Text", "all jobs, regardless of match score")%>
					</td>
				</tr>
				<tr>
					<td colspan="3"><br /></td>
				</tr>
				<tr>
					<td><%= HtmlLabel("DefaultSearchRadius.Label", "Default search radius")%></td>
					<td colspan="2">
						<asp:DropDownList runat="server" ID="DefaultSearchRadiusDropDown">
						</asp:DropDownList>
						<%= HtmlLocalise("DefaultSearchRadius.Text", "miles from job seeker's ZIP code") %>
					</td>
				</tr>
				<tr>
					<td colspan="3"><br /></td>
				</tr>
				<tr>
					<td style="vertical-align: top"><%= HtmlLabel("SearchStates.Label", "Display jobs from")%></td>
					<td colspan="2">
						<asp:DropDownList runat="server" ID="SearchStatesDropDown" />
						<asp:Button ID="AddSearchStateButton" runat="server" class="button3" OnClick="AddSearchStateButton_Click" />
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div style="height:100px;width:280px;overflow: auto">
							<uc:UpdateablePairs ID="SearchStateUpdateablePairs" runat="server" RestrictDuplicates="True" MinListSize="1" />
						</div>
					</td>
					<td style="vertical-align: top">
						<asp:RadioButton id="ZipCodeRadioButton" GroupName="SearchCentrePointType" runat="server" TextAlign="Right"/>  
						<asp:DropDownList runat="server" ID="SearchRadiusDropDown" />
						<%= HtmlLocalise("SearchCentrePointType.Label", "radius from this ZIP code")%>
						<asp:TextBox runat="server" ID="ZipCodeTextBox" Width="80" MaxLength="10" />
						<ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="ZipCodeTextBox" FilterType="Numbers" />
						<br />
						<asp:RadioButton id="SelectedLocationsRadioButton" GroupName="SearchCentrePointType" runat="server" TextAlign="Right" Checked="true" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><br /></td>
				</tr>
				<asp:PlaceHolder runat="server" ID="SpideredJobsPlaceHolder">
					<tr>
						<td><%= HtmlLabel("SpideredJobsExpiry.Label", "Expire spidered jobs after")%></td>
						<td colspan="2">
							<asp:DropDownList runat="server" ID="SpideredJobsExpiryDropDown">
								<asp:ListItem Value="" Selected="true">N/A</asp:ListItem>
								<asp:ListItem>10</asp:ListItem>
								<asp:ListItem>15</asp:ListItem>
								<asp:ListItem>20</asp:ListItem>
								<asp:ListItem>25</asp:ListItem>
								<asp:ListItem>30</asp:ListItem>
							</asp:DropDownList>
							<%= HtmlLocalise("Days.Label", "days")%>
						</td>
					</tr>
					<tr>
						<td colspan="3"><br /></td>
					</tr>
				</asp:PlaceHolder>
      </asp:PlaceHolder>
			<tr>
				<td style="vertical-align: top"><%= HtmlLabel("ResumesSearchable.Label", "Resumes searchable by interested #BUSINESSES#:LOWER?")%></td>
				<td colspan="2">
					<asp:DropDownList runat="server" ID="ResumesSearchableDropDown" />
				</td>
			</tr>
			<tr>
				<td colspan="3"><br /></td>
			</tr>
			<asp:PlaceHolder runat="server" ID="VeteranPriorityPlaceHolder">
				<tr>
					<td style="vertical-align: top"><%= HtmlLabel("VeteranPriority.Label", "Veteran Priority Service enabled")%></td>
					<td colspan="2">
						<asp:CheckBox runat="server" ID="VeteranPriorityCheckBox" />
						<br />
						<%=HtmlLabel("VeteranPriorityCheckBox.TextPre", "Veterans must match with at least")%>&nbsp;
						<asp:DropDownList runat="server" ID="VeteranPriorityStarMatchingDropDownList">
							<asp:ListItem>3</asp:ListItem>
							<asp:ListItem>4</asp:ListItem>
							<asp:ListItem>5</asp:ListItem>
						</asp:DropDownList>&nbsp;
						<%=HtmlLabel("VeteranPriorityCheckBox.TextPost", " stars to gain posting exclusivity")%>
					</td>
				</tr>
				<tr>
					<td colspan="3"><br /></td>
				</tr>
			</asp:PlaceHolder>
      <tr>
        <td style="vertical-align: top"><%= HtmlLabel("DefaultPostingAge.Label", "Set default job posting criteria")%></td>
				<td colspan="2">
					<asp:DropDownList runat="server" ID="DefaultPostingAgeDropDownList"/>
				</td>
      </tr>
		</table>	
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="SearchDefaultsPanelExtender" runat="server" TargetControlID="SearchDefaultsPanel" ExpandControlID="SearchDefaultsHeaderPanel" 
																CollapseControlID="SearchDefaultsHeaderPanel" Collapsed="true" ImageControlID="SearchDefaultsHeaderImage" 
																SuppressPostBack="true" />

<asp:PlaceHolder runat="server" ID="AlertDefaultsPlaceHolder" Visible="False">
  <asp:Panel ID="AlertDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	  <table class="accordionTable" role="presentation">
		  <tr  class="multipleAccordionTitle">
			  <td>
				  <asp:Image ID="AlertDefaultsHeaderImage" runat="server"  AlternateText="."/>
					&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AlertDefaults.Label", "Alert defaults")%></a></span>
			  </td>
		  </tr>
	  </table>
  </asp:Panel>
  <asp:Panel ID="AlertDefaultsPanel" runat="server">
	  <div class="singleAccordionContentWrapper">
			<div class="singleAccordionContent">
				<table style="width:100%;" role="presentation">
					<tr>
						<td style="width:150px;"></td>
						<td style="width:280px;"></td>
						<td style="width:150px;"></td>
						<td></td>
					</tr>
					<tr>
						<td><%= HtmlLabel("JobAlerts.Label", "Job alerts")%></td>
						<td>
							<asp:DropDownList runat="server" ID="JobAlertsDropDown" />
						</td>
						<td><%= HtmlLabel("AlertFrequency.Label", "Alert frequency")%></td>
						<td style="text-align: left">
							<asp:DropDownList runat="server" ID="AlertFrequencyDropDown" />
						</td>
					</tr>
					<tr>
						<td colspan="4"><br /></td>
					</tr>
					<tr>
						<td><%= HtmlLabel("EmailOtherJobs.Label", "Email")%></td>
						<td colspan="3">
							<asp:CheckBox runat="server" id="EmailOtherJobsCheckBox" TextAlign="Right" />
						</td>
					</tr>
					<tr>
						<td colspan="4"><br /></td>
					</tr>
					<tr>
						<td><%= HtmlLabel("StaffEmailAlerts.Label", "Staff email alerts")%></td>
						<td colspan="3">
							<asp:DropDownList runat="server" ID="StaffEmailAlertDropDown" />
						</td>
					</tr>
					<tr>
						<td colspan="4"><br /></td>
					</tr>
 				</table>		
			</div>
		</div>
  </asp:Panel>
  <act:CollapsiblePanelExtender ID="AlertDefaultsPanelExtender" runat="server" TargetControlID="AlertDefaultsPanel" ExpandControlID="AlertDefaultsHeaderPanel" 
																  CollapseControlID="AlertDefaultsHeaderPanel" Collapsed="true" ImageControlID="AlertDefaultsHeaderImage" 
																  CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																  SuppressPostBack="true" />
</asp:PlaceHolder>

<asp:Panel ID="GeneralApplicationDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="GeneralApplicationDefaultsHeaderImage" runat="server" AlternateText="."/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("GeneralApplicationDefaults.Label", "General Application defaults")%></a></span>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="GeneralApplicationDefaultsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table role="presentation">
					<asp:PlaceHolder ID="SchoolTypePlaceHolder" Visible="False" runat="server">
				<tr>
					<td>
					<focus:LocalisedLabel runat="server" ID="SchoolType1Label" LocalisationKey="SchoolType1.Label" DefaultText="#SCHOOLNAME# is a " RenderOuterSpan="False"/>
						<asp:DropDownList runat="server" ID="SchoolTypeDropDown" />
						<%= HtmlLocalise("SchoolType2.Label", " year school")%>
					</td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
				</asp:PlaceHolder>
				<tr>
						<td><focus:LocalisedLabel runat="server" ID="PlaceEmphasisOnLabel" LocalisationKey="PlaceEmphasisOn.Label" DefaultText="Place emphasis on" RenderOuterSpan="False"/>&nbsp;
						<asp:DropDownList runat="server" ID="PlaceEmphasisOnDropDown"/>
						</td>
				</tr>
				<asp:PlaceHolder runat="server" ID="SurveyFrequencyPlaceHolder">
					<tr>
						<td><asp:CheckBox runat="server" id="DisplaySurveyFrequencyCheckBox" TextAlign="Right" ClientIDMode="Static"/></td>
					</tr>
					<tr>
						<td><%= HtmlLocalise("DisplaySurveyFrequency.Text", "Display job seeker survey every ")%> 
							<asp:DropDownList runat="server" ID="SurveyFrequencyDropDownList" Enabled="False" ClientIDMode="Static">
								<asp:ListItem>7</asp:ListItem>
								<asp:ListItem>14</asp:ListItem>
								<asp:ListItem Selected="true">30</asp:ListItem>
							</asp:DropDownList> <%= HtmlLocalise("Stars.Text", " days at next log-in")%>
							<asp:HiddenField runat="server" ID="SurveyFrequency"/>
							</td>
					</tr>
					<tr>
						<td colspan="2"><br /></td>
					</tr>
				</asp:PlaceHolder>
				<asp:PlaceHolder runat="server" ID="ProspectiveStudentsPlaceHolder">
					<tr>
						<td><asp:CheckBox runat="server" id="ProspectiveStudentsCheckBox" TextAlign="Right" ClientIDMode="Static"/></td>
					</tr>
					<tr>
						<td colspan="2"><br /></td>
					</tr>
				</asp:PlaceHolder>
			</table>			
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="GeneralApplicationDefaultsPanelExtender" runat="server" TargetControlID="GeneralApplicationDefaultsPanel" ExpandControlID="GeneralApplicationDefaultsHeaderPanel" 
																CollapseControlID="GeneralApplicationDefaultsHeaderPanel" Collapsed="true" ImageControlID="GeneralApplicationDefaultsHeaderImage" 
																CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																SuppressPostBack="true" />
                                  
<asp:Panel ID="JobDisclaimerDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="JobDisclaimerDefaultsHeaderImage" runat="server" AlternateText="."/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobDisclaimerDefaults.Label", "Job Disclaimer defaults")%></a></span>
			</td>
		</tr>		
	</table>
</asp:Panel>
<asp:Panel ID="JobDisclaimerDefaultsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table style="width:100%;" role="presentation">
				<tr>
					<td style="width:250px;"></td>
					<td></td>
				</tr>
				<tr>
					<td><%= HtmlLabel("SearchResultsDisclaimer.Label", "Display Search Results disclaimer")%></td>
					<td><asp:CheckBox runat="server" id="SearchResultsDisclaimerCheckBox" TextAlign="Right" /></td>				
				</tr>
				<tr>
					<td colspan="1"><br /></td>
					<td><textarea runat="server" id="SearchResultsDisclaimerTextBox"  rows="6" cols="25" style="overflow-y:scroll"></textarea></td>
				</tr>
        <tr>
          <td style="width:250px;"></td>
          <td></td>
        </tr>
        <tr>
			    <td><%= HtmlLabel("SpideredJobApplicationDisclaimer.Label", "Spidered job application disclaimer")%></td>
			    <td><asp:CheckBox runat="server" id="SpideredJobApplicationDisclaimerCheckBox" TextAlign="Right" /></td>		
        </tr>
		    <tr>
			    <td colspan="1"><br /></td>
			    <td><textarea runat="server" id="SpideredJobApplicationDisclaimerTextBox" rows="6" cols="25" style="overflow-y:scroll"></textarea></td>
		    </tr>
			</table>	
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="JobDisclaimerDefaultsPanelExtender" runat="server" TargetControlID="JobDisclaimerDefaultsPanel" ExpandControlID="JobDisclaimerDefaultsHeaderPanel" 
																CollapseControlID="JobDisclaimerDefaultsHeaderPanel" Collapsed="true" ImageControlID="JobDisclaimerDefaultsHeaderImage" 
																SuppressPostBack="true" />

<asp:Panel ID="ColourDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="ColourDefaultsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="." Width="22" Height="22"/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ColourDefaults.Label", "Color defaults")%></a></span>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="ColourDefaultsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table role="presentation">
				<tr>
					<td><%= HtmlLabel("LightColour.Label", "Light color")%></td>
					<td>
						<asp:Textbox ID="LightColourTextBox" runat="server" ClientIDMode="Static" MaxLength="6" Width="100" />
						<br /><asp:RequiredFieldValidator runat="server" ID="LightColourValidator" ValidationGroup="CareerDefaults" SetFocusOnError="true" Display="Dynamic" CssClass="error" ControlToValidate="LightColourTextBox" />		
					</td>
				</tr>
				<tr>
					<td><%= HtmlLabel("DarkColour.Label", "Dark color")%></td>
					<td>
						<asp:Textbox ID="DarkColourTextBox" runat="server" ClientIDMode="Static" MaxLength="6" Width="100" />
						<br /><asp:RequiredFieldValidator runat="server" ID="DarkColourValidator" ValidationGroup="CareerDefaults" SetFocusOnError="true" Display="Dynamic" CssClass="error" ControlToValidate="DarkColourTextBox" />		
					</td>
				</tr>
				<tr>
					<td></td>
					<td><asp:Button ID="ResetColoursButton" runat="server" class="button3" OnClick="ResetColoursButton_Click" /></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
			</table>
		</div>
	</div>			
</asp:Panel>
<act:CollapsiblePanelExtender ID="ColourDefaultsPanelExtender" runat="server" TargetControlID="ColourDefaultsPanel" ExpandControlID="ColourDefaultsHeaderPanel" 
																CollapseControlID="ColourDefaultsHeaderPanel" Collapsed="true" ImageControlID="ColourDefaultsHeaderImage" 
																CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																SuppressPostBack="true" />
																
<asp:Panel ID="LogoDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<table class="accordionTable" role="presentation">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="LogoDefaultsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="." Width="22" Height="22"/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("LogoDefaults.Label", "Logo defaults")%></a></span>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="LogoDefaultsPanel" runat="server">
	<div class="singleAccordionContentWrapper">
		<div class="singleAccordionContent">
			<table style="width:100%;" role="presentation">
				<tr>
					<td colspan="2"><i><%= HtmlLocalise("LogoRequirements.Label", "Logos must be in GIF, JPG, or PNG format at 72dpi. Maximum size 300 pixels wide by 70 pixels high.")%></i></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
				<tr>
					<td style="width:100px; vertical-align: top;"><%= HtmlLabel("ApplicationLogo.Label", "Application logo")%></td>
					<td>
						<uc:ImageUploader ID="CareerExplorerApplicationLogoUploader" runat="server" ImageWidth="300" ImageHeight="70" />
					</td>
				</tr>
				<tr>
					<td><%= HtmlLabel("FooterLogo.Label", "Footer logo")%></td>
					<td><asp:CheckBox runat="server" id="CareerExplorerFooterLogoCheckBox" TextAlign="Right" /></td>
				</tr>
				<tr>
					<td colspan="2"><br /></td>
				</tr>
			</table>	
		</div>
	</div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="LogoDefaultsPanelExtender" runat="server" TargetControlID="LogoDefaultsPanel" ExpandControlID="LogoDefaultsHeaderPanel" 
																CollapseControlID="LogoDefaultsHeaderPanel" Collapsed="true" ImageControlID="LogoDefaultsHeaderImage" 
																CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
																SuppressPostBack="true" />


<div style="width:100%"><asp:Button ID="SaveChangesBottomButton" runat="server" class="button1 right" OnClick="SaveChangesButton_Click" /></div>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
<%--<uc:ConfirmationModal ID="DeleteConfirmation" runat="server" OnOkCommand="DeleteConfirmation_OkCommand" />--%>

<script type="text/javascript">
  var CareerDefaults_JobAlertsDropDown;
  var CareerDefaults_AlertFrequencyDropDown;
  var CareerDefaults_VeteranPriorityDropDown;

  $(document).ready(
    function () {
    	CareerDefaults_JobAlertsDropDown = $("#<%=JobAlertsDropDown.ClientID %>");
    	CareerDefaults_AlertFrequencyDropDown = $("#<%=AlertFrequencyDropDown.ClientID %>");
    	CareerDefaults_VeteranPriorityDropDown = $("#<%=VeteranPriorityStarMatchingDropDownList.ClientID %>");

    	CareerDefaults_JobAlertFrequency();
    	CareerDefaults_JobAlertsDropDown.bind("change", CareerDefaults_JobAlertFrequency);

	    CareerDefaults_DisplaySurveyFrequency();
	    CareerDefaults_SurveyFrequencyChange();

	    $('#LightColourTextBox').jPicker({ window: { position: { x: 'screenCenter', y: 'center' }} });
	    $('#DarkColourTextBox').jPicker({ window: { position: { x: 'screenCenter', y: 'center' }} });

    	$("#DisplaySurveyFrequencyCheckBox").change(function () {
    		var displaySurveyFrequency = $("#DisplaySurveyFrequencyCheckBox").is(":checked");
    		$("#SurveyFrequencyDropDownList").prop('disabled', !displaySurveyFrequency);
    	});

    	$("#SurveyFrequencyDropDownList").change(function () {
    		CareerDefaults_SurveyFrequencyChange();
	    });

      if (CareerDefaults_VeteranPriorityDropDown.length > 0) {
        var checkBox = $("#<%=VeteranPriorityCheckBox.ClientID %>");
        $("#<%=VeteranPriorityCheckBox.ClientID %>").click(function () {
          CareerDefaults_VeteranPriorityCheckBoxClicked($(this));
        });
        CareerDefaults_VeteranPriorityCheckBoxClicked(checkBox);
      }

      var cbSearchResults = $("#<%=SearchResultsDisclaimerCheckBox.ClientID %>");
      $("#<%=SearchResultsDisclaimerCheckBox.ClientID %>").click(function () {
        CareerDefaults_SearchResultsDisclaimerCheckBoxChecked($(this));
      });
	CareerDefaults_SearchResultsDisclaimerCheckBoxChecked(cbSearchResults);
      
	var chkSpideredJobApplicationDisclaimerCheckBox = $("#<%=SpideredJobApplicationDisclaimerCheckBox.ClientID %>");
      $("#<%=SpideredJobApplicationDisclaimerCheckBox.ClientID %>").click(function () {
        CareerDefaults_SpideredJobApplicationDisclaimerCheckBoxChecked($(this));
      });
      CareerDefaults_SpideredJobApplicationDisclaimerCheckBoxChecked(chkSpideredJobApplicationDisclaimerCheckBox);

	
    }
  );
	
  function CareerDefaults_JobAlertFrequency() {
    var isDisabled = (CareerDefaults_JobAlertsDropDown.prop("selectedIndex") == 2);
    CareerDefaults_AlertFrequencyDropDown.prop("disabled", isDisabled);
   }

   function CareerDefaults_DisplaySurveyFrequency() {
   	var displaySurveyFrequency = $("#DisplaySurveyFrequencyCheckBox").is(":checked");
   	$("#SurveyFrequencyDropDownList").prop('disabled', !displaySurveyFrequency);
   }

   function CareerDefaults_SurveyFrequencyChange() {

   	var surveyFrequency = $("#SurveyFrequencyDropDownList").val();
	 	var hiddenSurveyFrequency = document.getElementById('<%= SurveyFrequency.ClientID %>');

	 	if (hiddenSurveyFrequency)
	 	{
	 		hiddenSurveyFrequency.value = surveyFrequency;
	 	}
	 }

	 function CareerDefaults_VeteranPriorityCheckBoxClicked(chkBox) {
	   CareerDefaults_VeteranPriorityDropDown.prop("disabled", !chkBox.is(":checked"));
	 }
   

   function CareerDefaults_SpideredJobApplicationDisclaimerCheckBoxChecked(chkBox) 
   {
     $("#<%=SpideredJobApplicationDisclaimerTextBox.ClientID %>").prop("disabled", !chkBox.is(":checked"));  
	}
	
 function CareerDefaults_SearchResultsDisclaimerCheckBoxChecked(chkBox) 
   {
     $("#<%=SearchResultsDisclaimerTextBox.ClientID %>").prop("disabled", !chkBox.is(":checked"));
   }
</script>