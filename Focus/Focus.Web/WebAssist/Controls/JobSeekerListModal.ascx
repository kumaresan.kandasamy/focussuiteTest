﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobSeekerListModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.JobSeekerListModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" BehaviorID="JobSeekerListModalPanel"
												TargetControlID="ModalDummyTarget"
												PopupControlID="JobSeekerListModalPanel"
												PopupDragHandleControlID="JobSeekerListHeaderPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="JobSeekerListModalPanel" runat="server" CssClass="modal" style="display:none;">
	<asp:Panel ID="JobSeekerListHeaderPanel" runat="server" CssClass="modal-header">
		<h2><asp:Label ID="TitleLabel" runat="server" Text="Add candidate to list"/></h2>
	</asp:Panel>
	<p>
		<focus:LocalisedLabel runat="server" ID="ListsDropDownListLabel" AssociatedControlID="ListsDropDownList" LocalisationKey="JobSeekerList" DefaultText="Job seekers" CssClass="sr-only"/>		
		<asp:DropDownList ID="ListsDropDownList" runat="server" Width="350px" />
		<br/><asp:RequiredFieldValidator runat="server" ID="ListsDropDownListRequiredValidator" ValidationGroup="JobSeekerListModal" CssClass="error" Display="Dynamic" ControlToValidate="ListsDropDownList" />		
	</p>
	<div id="newListDiv" style="display:none;">
		<p>
			<span class="inFieldLabel"><%= HtmlLabel(NewListTextBox,"NewList.Label", "enter new list name")%></span>
			<asp:TextBox ID="NewListTextBox" runat="server" Width="347" ClientIDMode="Static" />
		    <br/><asp:CustomValidator runat="server" ID="NewListTextBoxValidator" ValidationGroup="JobSeekerListModal" CssClass="error" Display="Dynamic" ControlToValidate="NewListTextBox" ValidateEmptyText="True" ClientValidationFunction="validateNewList" />
		</p>
	</div>
	<p>
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
		<asp:Button ID="AddButton" runat="server" CssClass="button3" ValidationGroup="JobSeekerListModal" OnClick="AddButton_Clicked" CausesValidation="True" />		
	</p>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />

<script type="text/javascript">

  Sys.Application.add_load(JobSeekerListModal_PageLoad);

  function JobSeekerListModal_PageLoad(sender, args) {
    $('#<%= ListsDropDownList.ClientID %>').change(function () {
      if ($(this).val() == '-1')
        $('#newListDiv').show();
      else
        $('#newListDiv').hide();
    });
  }

function validateNewList(oSrc, args) {
    args.IsValid = true;
    if ($('#<%= ListsDropDownList.ClientID %>').val() == '-1' && $.trim($('#<%= NewListTextBox.ClientID %>').val()).length == 0)
        args.IsValid = false;
}

</script>