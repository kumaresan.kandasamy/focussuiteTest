﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ComparePostingToResumeModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ComparePostingToResumeModal" %>


<asp:HiddenField ID="ComparePostingToResumeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ComparePostingToResumeModalPopup" runat="server" 
				TargetControlID="ComparePostingToResumeModalDummyTarget"
				PopupControlID="ComparePostingToResumeModalPanel"
				PopupDragHandleControlID="ComparePostingToResumeModalHeaderPanel"
				RepositionMode="RepositionOnWindowResizeAndScroll"
				BackgroundCssClass="modalBackground"
				BehaviorID="ComparePostingToResumeModal" />	

<asp:Panel ID="ComparePostingToResumeModalPanel" runat="server" CssClass="modal" Style="display: none; width: 90%">
  <asp:UpdatePanel runat="server" ID="ComparePostingToResumeModalUpdatePanel">
    <ContentTemplate>
      <asp:Panel runat="server" ID="ComparePostingToResumeModalHeaderPanel">
        <table role="presentation">
          <tr>
            <td>
              <h1>
                <asp:Literal ID="ModalTitleLiteral" runat="server" /></h1>
            </td>
          </tr>
        </table>
      </asp:Panel>
      <div style="width: 100%; display: inline-block">
        <div style="width: 48%; float: left;">
          <strong>
            <%= HtmlLocalise("Posting.Title", "Posting")%></strong>
        </div>
        <div style="width: 48%; float: right;">
          <strong>
            <%= HtmlLocalise("ResumeLabel.Text", "Resume")%></strong>&nbsp;
          <asp:DropDownList runat="server" ID="ResumesDropDown" OnSelectedIndexChanged="ResumesDropDown_SelectedIndexChanged"
            AutoPostBack="True" ClientIDMode="AutoID" Title="Resume"/>
          <asp:Image ID="MatchScoreImage" runat="server" Height="15px" Width="65px" Visible="False"
            Style="float: right" />
        </div>
      </div>
      <div style="width: 100%; display: inline-block">
        <div class="ResumeDivDevice" style="width: 48%; float: left;">
          <iframe id="PostingDetail" runat="server" width="100%" height="430px" title="Spidered jobs with good matches content"
            style="border-width: 1px; border-style: solid; padding: 5px;">Posting Detail</iframe>
        </div>
        <div style="width: 48%; float: right;">
          <asp:Panel ID="ResumeResultPanel" runat="server">
            <div style="overflow: scroll; height: 430px; border-style: solid; border-width: 1px;
              padding: 5px;">
              <asp:Literal ID="ResumeLiteral" runat="server" />
            </div>
            <br />
            <asp:CheckBox ID="DoNotShowCheckBox" runat="server" OnCheckedChanged="DoNotShowCheckBox_CheckChanged"
              AutoPostBack="True" ClientIDMode="AutoID" />
					<asp:Label runat="server" CssClass="sr-only" AssociatedControlID="DoNotShowCheckBox">Do not show check box</asp:Label>
          </asp:Panel>
          <asp:Panel ID="EmpytyResultPanel" runat="server" Visible="false">
            <%= HtmlLocalise("NoResumesFound.Text", "No matching resumes found")%>
          </asp:Panel>
        </div>
        <div style="width: 100%; display: inline-block">
				<asp:Button ID="ContactEmployerButton" runat="server" class="button2"  OnClick="ContactEmployerButton_Clicked" />
          &nbsp;&nbsp;
				<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
        </div>
      </div>
      <script type="text/javascript">
        /* Ipad Iframe fix */
        $(function () {
          var iframe = $("#MainContent_SpideredJobWithGoodMatchesList_ComparePostingToResumeModal_PostingDetail");
          iframe.load(function () {
            $("body").height(iframe.height());
          });
        });
      </script>
    </ContentTemplate>
    <Triggers>
    </Triggers>
  </asp:UpdatePanel>

</asp:Panel>