﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadMultipleUsersModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.UploadMultipleUsersModal"  %>

<asp:HiddenField ID="UploadMultipleUsersModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="UploadMultipleUsersModalPopup" runat="server"
												BehaviorID="UploadMultipleUsersModal"
												TargetControlID="UploadMultipleUsersModalDummyTarget"
												PopupControlID="UploadMultipleUsersModalPanel"
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:panel id="UploadMultipleUsersModalPanel" runat="server" class="modal" style="display:none;">
	<h2>
		<focus:LocalisedLabel runat="server" ID="UploadMultipleUsersHeadingLabel" LocalisationKey="UploadMultipleUsersHeading.Label" DefaultText="Upload multiple users"/>
	</h2>
	<focus:LocalisedLabel runat="server" ID="UploadMultipleUsersSubHeadingRequiredLabel" LocalisationKey="UploadMultipleUsersSubHeadingRequired.Label" DefaultText="Please ensure the file has a header row, and at least one staff record"/>
	<div>
		<div>
			<asp:Label runat="server" ID="ErrorLabel" CssClass="error" Visible="false" />
		</div>
		<div>
			<asp:FileUpload ID="FileUpload" title="File Upload" runat="server" ClientIDMode="Static"/>
		</div>
		<div>
			<asp:RequiredFieldValidator ID="FileUploadRequired" runat="server" ControlToValidate="FileUpload"	CssClass="error" SetFocusOnError="false" ClientIDMode="Static"  ValidationGroup="MultipleUpload"/>
		</div>
		<div>
			<focus:LocalisedLabel runat="server" ID="StatusLabel" LocalisationKey="Status.Label" CssClass="error"/>
		</div>
	</div>
	<br/>
	<div >
		<asp:Button ID="CancelButton" runat="server" class="button1" CausesValidation="false" OnClick="CancelButton_Click" />
		<asp:Button ID="DownloadButton" runat="server" class="button1" OnClick="DownloadButton_Click" CausesValidation="False" />
		<asp:Button ID="SaveButton" runat="server" class="button1" OnClick="SaveButton_Click" CausesValidation="True" ValidationGroup="MultipleUpload"/>
	</div>
</asp:panel>
