﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditStaffAccountModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EditStaffAccountModal" %>
<%@ Register TagPrefix="uc" TagName="ContactInformation" Src="~/Code/Controls/User/ContactInformation.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>

<asp:HiddenField ID="EditStaffAccountModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="EditStaffAccountModalPopup" runat="server"
												BehaviorID="EditStaffAccountModal"
												TargetControlID="EditStaffAccountModalDummyTarget"
												PopupControlID="EditStaffAccountModalPanel"
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:panel id="EditStaffAccountModalPanel" runat="server" CssClass="modal" style="display:none; overflow-x:hidden;">
	<h2>
		<focus:LocalisedLabel runat="server" ID="EditStaffAccountHeadingLabel" LocalisationKey="EditStaffAccountHeading.Label" DefaultText="Edit staff account"/>
	</h2>
  <asp:PlaceHolder runat="server" ID="UserNamePlaceHolder">
	  <strong><focus:LocalisedLabel runat="server" ID="ChangeUsernameLabel" CssClass="dataInputLabel" LocalisationKey="ChangeUsername.Label" DefaultText="Change Username" /></strong>
	  <div id="editStaffAccountContainer" class="createStaffAccount">
		  <div class="dataInputRow">
		    <span class="dataInputLabel"></span>
		 	  <span class="dataInputField">
			  <asp:Label runat="server" ID="ErrorLabel" CssClass="error" Visible="false" />
        </span>
      </div>
      <div class="dataInputRow">
			  <focus:LocalisedLabel runat="server" ID="LocalisedLabel1" AssociatedControlID="CurrentUserNameTextBox" CssClass="dataInputLabel" LocalisationKey="CurrentUserName.Label" DefaultText="Current username"  />
			  <span class="dataInputField">
				  <asp:TextBox ID="CurrentUserNameTextBox" runat="server" Width="210px" ClientIDMode="Static" ReadOnly="true" CssClass="disabledControl" />
			  </span>
		  </div>
		  <div class="dataInputRow">
			  <focus:LocalisedLabel runat="server" ID="UserNameLabel" AssociatedControlID="EditUserNameTextBox" CssClass="dataInputLabel" LocalisationKey="Username.Label" DefaultText="New username"  />
			  <span class="dataInputField">
				  <asp:TextBox ID="EditUserNameTextBox" runat="server" ClientIDMode="Static" Width="210px" MaxLength="100" />
          <br />
				  <asp:RegularExpressionValidator ID="UserNameRegEx" runat="server" ControlToValidate="EditUserNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="EditStaffAccount" />
			  </span>
		  </div>
		  <div class="dataInputRow">
			  <focus:LocalisedLabel runat="server" ID="ConfirmUserNameLocalisedLabel" AssociatedControlID="ConfirmUserNameTextBox" CssClass="dataInputLabel" LocalisationKey="ConfirmUserName.Label" DefaultText="Retype new username"  />
			  <span class="dataInputField">
				  <asp:TextBox ID="ConfirmUserNameTextBox" runat="server" Width="210px" ClientIDMode="Static" MaxLength="100"/>
            <br />
				  <asp:CompareValidator ID="ConfirmUserNameCompare" runat="server" ControlToValidate="ConfirmUserNameTextBox" ControlToCompare="EditUserNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="EditStaffAccount" />
			    <asp:CustomValidator ID="CheckUserNameRequired" runat="server" ControlToValidate="EditUserNameTextBox" ClientValidationFunction="validateUsername" SetFocusOnError="true"  Display="Dynamic" CssClass="error" ValidationGroup="EditStaffAccount"/>
        </span>
		  </div>
    </div>
    <hr>
    <br />
  </asp:PlaceHolder>
  <strong><focus:LocalisedLabel runat="server" ID="LocalisedLabel2" CssClass="dataInputLabel" LocalisationKey="ChangeUsername.Label" DefaultText="Update staff contact information" /></strong> <focus:LocalisedLabel runat="server" ID="LocalisedLabel3" CssClass="requiredDataLegend" LocalisationKey="EditStaffAccountSubHeadingRequired.Label" DefaultText="required fields"/>
	
	<uc:ContactInformation ID="ContactInformationMain" runat="server" />
	<div class="modalButtons">
		<asp:Button ID="CancelButton" runat="server" class="button1" CausesValidation="false" OnClick="CancelButton_Click" />
		<asp:Button ID="SaveButton" runat="server" class="button1" ValidationGroup="EditStaffAccount" OnClientClick="return EditStaffAccountModal_Validate();" OnClick="SaveButton_Click" />
	</div>
</asp:panel>

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
  
  
<script type="text/javascript">

  jQuery('#ConfirmUserNameTextBox').on('input', function () {
    if ($('#ConfirmUserNameTextBox').val() != '') {
      $('#MainContent_EditStaffAccountModal_CheckUserNameRequired').hide();
    }
  });

  function validateUsername(oSrc, args) {
    if ($('#ConfirmUserNameTextBox').val() == '' && $('#EditUserNameTextBox').val() != '') {
      args.IsValid = false;
    } else {
      args.IsValid = true;
    }
  }

    function EditStaffAccountModal_Validate() 
    {
      var isValid = Page_ClientValidate('EditStaffAccount');
      if (isValid) {
        isValid = Page_ClientValidate('ContactInformation');
      }

      return isValid;
    }
</script>
