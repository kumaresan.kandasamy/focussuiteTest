﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
  public partial class UpdateDefaultOfficeModal : UserControlBase
  {
    public delegate void UpdatedHandler(object sender, EventArgs eventArgs);
    public event UpdatedHandler Updated;

    private List<OfficeDto> _activeOffices = null;
    private List<OfficeDto> _defaultOffices = null;

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        LocaliseUI();

      UpdateDefaultOfficeModalPopup.CancelControlID = CloseButton.ClientID;
      NewOfficesValidator.ValidationGroup = OkButton.ValidationGroup = string.Concat(ClientID, "_UpdateDefaultOffice");
    }

    /// <summary>
    /// Fires when an item is data bound in the list
    /// </summary>
    /// <param name="sender">ListView control</param>
    /// <param name="e">Item event arguments</param>
    protected void UpdateOfficeList_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var defaultType = (OfficeDefaultType)e.Item.DataItem;

      var label = (Label)e.Item.FindControl("UpdateOfficeHeaderLabel");
      switch (defaultType)
      {
        case OfficeDefaultType.JobSeeker:
          label.Text = CodeLocalise("UpdateOfficeHeaderLabel.JobSeeker", "For unassigned #CANDIDATETYPE#:LOWER records.");
          break;

        case OfficeDefaultType.JobOrder:
          label.Text = CodeLocalise("UpdateOfficeHeaderLabel.JobOrder", "For unassigned #EMPLOYMENTTYPE#:LOWER records.");
          break;

        case OfficeDefaultType.Employer:
					label.Text = CodeLocalise("UpdateOfficeHeaderLabel.Employer", "For unassigned #BUSINESS#:LOWER records.");
          break;

        default:
					label.Text = CodeLocalise("UpdateOfficeHeaderLabel.All", "For unassigned #CANDIDATETYPE#:LOWER, #EMPLOYMENTTYPE#:LOWER and #BUSINESS#:LOWER records.");
          break;
      }

      if (_activeOffices.IsNull())
      {
        var criteria = new OfficeCriteria
        {
          FetchOption = CriteriaBase.FetchOptions.List,
          InActive = false
        };
        _activeOffices = ServiceClientLocator.EmployerClient(App).GetOfficesList(criteria);
      }

      if (_defaultOffices.IsNull())
        _defaultOffices = ServiceClientLocator.EmployerClient(App).GetDefaultOffices(OfficeDefaultType.All);

      var defaultOffice = App.Settings.EnableDefaultOfficePerRecordType
                            ? _defaultOffices.FirstOrDefault(office => (office.DefaultType & defaultType) == defaultType)
                            : _defaultOffices.FirstOrDefault();

      var hiddenType = (HiddenField)e.Item.FindControl("DefaultTypeValue");
      hiddenType.Value = defaultType.ToString();

      label = (Label)e.Item.FindControl("CurrentDefaultOfficeHeaderLabel");
      label.Text = HtmlLocalise("CurrentOffice.Label", "Current default office");

      label = (Label)e.Item.FindControl("CurrentDefaultOfficeNameLabel");
      label.Text = defaultOffice.IsNotNull()
                    ? defaultOffice.OfficeName
                    : HtmlLocalise("Global.NotRecorded.Text", "Not recorded");

      label = (Label)e.Item.FindControl("NewDefaultOfficeHeaderLabel");
      label.Text = HtmlLocalise("NewOffice.Label", "New default office");

      var dropdown = (DropDownList)e.Item.FindControl("NewDefaultOfficeDropdown");
      BindOfficeDropdown(dropdown, _activeOffices, defaultOffice);
    }

    /// <summary>
    /// Fires when the OK button is clicked to update the default office
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void OkButton_Click(object sender, EventArgs e)
    {
      var items = (from item in UpdateOfficeList.Items
                     let hiddenType = (HiddenField) item.FindControl("DefaultTypeValue")
                     let dropdownList = (DropDownList) item.FindControl("NewDefaultOfficeDropdown")
                   where dropdownList.SelectedIndex > 0
                   select new
                   {
                     DefaultType = (OfficeDefaultType)Enum.Parse(typeof(OfficeDefaultType), hiddenType.Value),
                     DefaultOfficeId = dropdownList.SelectedValueToLong()
                   }).ToDictionary(item => item.DefaultType, item => item.DefaultOfficeId);

      SaveDefaultOffices(items);
    }

    /// <summary>
    /// Localises the text on the page
    /// </summary>
    private void LocaliseUI()
    {
      HeaderLabel.Text = CodeLocalise("HeaderLabel.Text", "Update default office");

      NewOfficesValidator.ErrorMessage = App.Settings.EnableDefaultOfficePerRecordType
        ? CodeLocalise("NewOfficesValidator.Multi.ErrorMessage", "Please select at least one new office")
        : CodeLocalise("NewOfficesValidator.One.ErrorMessage", "Please select a new office");

      OkButton.Text = CodeLocalise("Global.Save.Text", "Save");
      CloseButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
    }

    /// <summary>
    /// Shows the modal
    /// </summary>
    public void Show()
    {
      var typesToUpdate = new List<OfficeDefaultType> ();
      if (App.Settings.EnableDefaultOfficePerRecordType)
      {
        typesToUpdate.Add(OfficeDefaultType.JobSeeker);
        typesToUpdate.Add(OfficeDefaultType.JobOrder);
        typesToUpdate.Add(OfficeDefaultType.Employer);
      }
      else
      {
        typesToUpdate.Add(OfficeDefaultType.All);
      }

      UpdateOfficeList.DataSource = typesToUpdate;
      UpdateOfficeList.DataBind();

      UpdateDefaultOfficeModalPopup.Show();
    }

    /// <summary>
    /// Binds a list of offices to a drop-down of a specific type
    /// </summary>
    /// <param name="dropdown">The drop-down</param>
    /// <param name="offices">A list of offices</param>
    /// <param name="defaultOffice">The default office for a specific type</param>
    private void BindOfficeDropdown(ListControl dropdown, IEnumerable<OfficeDto> offices, OfficeDto defaultOffice)
    {
      dropdown.DataSource = defaultOffice.IsNull()
        ? offices
        : offices.Where(office => office.Id != defaultOffice.Id);

      dropdown.DataValueField = "Id";
      dropdown.DataTextField = "OfficeName";
      dropdown.DataBind();
      dropdown.Items.AddLocalisedTopDefault("NewDefaultOfficeDropdown.TopDefault.NoEdit", "- Select -");

      dropdown.Attributes.Add("data-validationgroup", NewOfficesValidator.ValidationGroup);
    }

    /// <summary>
    /// Saves the new default offices
    /// </summary>
    /// <param name="offices">A look-up of offices</param>
    private void SaveDefaultOffices(Dictionary<OfficeDefaultType, long> offices)
    {
      foreach (var defaultType in offices.Keys)
      {
        ServiceClientLocator.EmployerClient(App).UpdateDefaultOffice(offices[defaultType], defaultType);
      }

      if (App.Settings.EnableDefaultOfficePerRecordType)
      {
        var title = (offices.Count == 1)
              ? CodeLocalise("ChangeDefaultSuccessful.Single.Title", "Default office updated")
              : CodeLocalise("ChangeDefaultSuccessful.Multi.Title", "Default offices updated");

        ConfirmationModal.Show(title,
                               CodeLocalise("ChangeDefaultSuccessful.Multi.Body", "Your default office selection has been successfully updated"),
                               CodeLocalise("CloseModal.Text", "OK"));
      }
      else
      {
        ConfirmationModal.Show(CodeLocalise("ChangeDefaultSuccessful.Title", "Default office update"),
                               CodeLocalise("ChangeDefaultSuccessful.Body", "The default office for unassigned records has been updated"),
                               CodeLocalise("CloseModal.Text", "OK"));
      }

      OnUpdated(new EventArgs());
    }

    /// <summary>
    /// Raises the <see cref="Updated" /> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnUpdated(EventArgs eventArgs)
    {
      if (Updated != null)
        Updated(this, eventArgs);
    }

  }
}