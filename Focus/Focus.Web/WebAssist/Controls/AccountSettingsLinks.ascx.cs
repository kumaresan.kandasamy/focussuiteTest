﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.HtmlControls;

using Focus.Common.Extensions;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
  public partial class AccountSettingsLinks : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (App.Settings.DisableAssistAuthentication)
      {
        AssistAuthenticationLinks.Visible = false;
      }
      else
      {
				if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForAssist)
				{
					var passwordLinkVisible = !App.Settings.SSOChangeUsernamePasswordUrl.IsNullOrEmpty();
					SSOChangePasswordPlaceHolder.Visible = passwordLinkVisible;
					if (passwordLinkVisible) UpdateLink(SSOAssistChangePasswordLink, App.Settings.SSOChangeUsernamePasswordUrl);

					var manageAccountLinkVisible = !App.Settings.SSOManageAccountUrl.IsNullOrEmpty();
					SSOAssistContactInformation.Visible = manageAccountLinkVisible;
					if (manageAccountLinkVisible) UpdateLink(SSOAssistContactInformationLink, App.Settings.SSOManageAccountUrl);

					AssistAuthenticationLinks.Visible = false;
				}
				else
				{
					UpdateLink(AssistChangePasswordLink, UrlBuilder.AssistChangePassword());
					UpdateLink(AssistContactInformationLink, UrlBuilder.AssistContactInformation());
				}
      }

      UpdateLink(AssistSavedMessagesLink, UrlBuilder.AssistSavedMessages());
      UpdateLink(AssistSavedSearchesLink, UrlBuilder.AssistSavedSearches());

      if (App.User.IsAssistUserAndCanViewQueues())
        UpdateLink(AssistEmailAlertsLink, UrlBuilder.AssistEmailAlerts()); 
      else
        EmailAlertsHolder.Visible = false;

			if(!App.Settings.TalentModulePresent)
				EmailAlertsHolder.Visible = false;

			UpdateLink(AssistEmailAlertsLink, UrlBuilder.AssistEmailAlerts());
    }

    /// <summary>
    /// Either adds the href to the link, or styles it bold, depending on if it for the active page
    /// </summary>
    /// <param name="link">The hyperlink control on the page</param>
    /// <param name="href">The href parameter for the link</param>
    private void UpdateLink(HtmlAnchor link, string href)
    {
	    if (Page.Is(href))
		    //link.Style.Value = "font-weight:bold;text-decoration:none"; 
		    link.Attributes["class"] = "current-page";
	    else
		    link.HRef = href;
    }
  }
}