﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompareResumesModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.CompareResumesModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register  Src="~/Code/Controls/User/EmailEmployeeModal.ascx"  TagName="EmailEmployeeModal"  TagPrefix="uc" %>


<asp:HiddenField ID="CompareResumesModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="CompareResumesModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="CompareResumesModalDummyTarget"
												PopupControlID="CompareResumesModalPanel"
												PopupDragHandleControlID="CompareResumesModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="CompareResumesModalPanel" runat="server" CssClass="modal" ClientIDMode="Static" Style="display:none">
	<asp:Panel runat="server" ID="CompareResumesModalPanelHeader" ClientIDMode="Static">
    <h2><%= HtmlLocalise("CompareResumesWindow.Title", "Compare Resumes") %></h2></asp:Panel>
    <table role="presentation">
        <tr>
            <td>
	        <%--<div style="float:left;">--%>		
                <table style="width:500px" role="presentation">
			        <tr>
				        <td style="height: 40px"><%= HtmlLocalise("SuggestedMatchTitle", "Suggested match ")%>
                            <asp:image ID="ReferralScoreImage" runat="server" AlternateText="Referral Score Image" Height="16" Width="80"/>
                        </td><td>&nbsp;&nbsp;&nbsp;</td>
	    		    </tr>
		    	    <tr>
			    	    <td style="border-style:solid; border-width: 1px; padding: 5px; width: 100%;">
				    	    <div style="overflow:scroll;height:430px;">
					    	    <h3><asp:Literal ID="CandidateNameLiteral" runat="server" Text="Candidate name"/></h3><br/><br/>
						        <asp:Literal ID="CandidateResumeDescriptionLiteral" runat="server" />
    					    </div>
	    			    </td>
		    	    </tr>   
		        </table>
            </td>
            <td>
	        <%--</div>
	        <div style="float:right;">--%>
		        <table style="width:500px;" role="presentation">
			        <tr>
				        <td style="height: 40px"><%= HtmlLocalise("JobEmployerTitleLabel.Text", "Recent placement: ")%>
                            <asp:Label runat="server" ID="JobEmployerTitleLabel"></asp:Label>
				        </td>
			        </tr>
			        <tr>
				        <td style="border-style:solid; border-width: 1px; padding: 5px; width:100%;">
					        <div style="overflow:scroll;height:430px;">
						        <h3><asp:Literal ID="PlacementDetailsLiteral" runat="server" Text="Placement details"/></h3><br/><br/>
						        <asp:Literal ID="PlacementResumeDescriptionLiteral" runat="server" />
					        </div>
				        </td>
			        </tr>
		        </table>
	        <%--</div>--%> 
            </td>
        </tr>
        <tr>
            <td colspan="2">
	            <asp:CheckBox ID="DoNotShowCheckBox" runat="server" OnCheckedChanged="DoNotShowCheckBox_CheckChanged" AutoPostBack="True" CausesValidation="False" /><%= HtmlLabel(DoNotShowCheckBox,"DoNotShowCheckBox.Text", "Do not show this suggestion again ")%>
			</td>
        </tr>
        <tr>
            <td colspan="2">
                <br />
                <asp:Button ID="ContactEmployerButton" runat="server" class="button2" OnClick="ContactEmployerButton_Clicked" Text="Contact Employer" />
                &nbsp;&nbsp;
                <asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked"  Text="Cancel" />
            </td>
        </tr>
    </table>

<%--	<div class="closeIcon">
	    <img src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('CompareResumesModalPopup').hide();" />
	</div>--%>

</asp:Panel>

<%-- Confirmation Modal --%>
<uc:EmailEmployeeModal ID="EmailEmployee" runat="server" />
<uc:ConfirmationModal ID="Confirmation" runat="server" Height="250px" Width="400px"/>