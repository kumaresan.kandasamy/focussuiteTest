﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationCriteriaBuilder.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.LocationCriteriaBuilder" %>
<asp:Panel ID="LocationHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<asp:Image ID="LocationHeaderImage" runat="server" AlternateText="Location Header Image" />
	&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Location.Label", "Location")%></a></span>
</asp:Panel>
<asp:Panel ID="LocationPanel" runat="server">
	<table>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("County.Title", "County")%></h5></td>
		</tr>
		<tr>
			<td width="200px">
				<%= HtmlInFieldLabel("CountyTextBox", "County.InlineLabel", "county name", 150)%>
				<asp:TextBox runat="server" ID="CountyTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25" />
			</td>
			<td><asp:Button ID="AddCountyFilterButton" runat="server" CssClass="button3" OnClick="AddCountyFilterButton_Clicked" /></td>
		</tr>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("Office.Title", "Office")%></h5></td>
		</tr>
		<tr>
			<td>
				<%= HtmlInFieldLabel("OfficeTextBox", "Office.InlineLabel", "office location", 150)%>
				<asp:TextBox runat="server" ID="OfficeTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25" />
			</td>
			<td><asp:Button ID="AddOfficeFilterButton" runat="server" CssClass="button3" OnClick="AddOfficeFilterButton_Clicked" /></td>
		</tr>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("WorkforceInvestmentBoard.Title", "WIB")%></h5></td>
		</tr>
		<tr>
			<td>
				<%= HtmlInFieldLabel("WorkforceInvestmentBoardTextBox", "WorkforceInvestmentBoard.InlineLabel", "wib location", 150)%>
				<asp:TextBox runat="server" ID="WorkforceInvestmentBoardTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25" />
			</td>
			<td><asp:Button ID="AddWorkforceInvestmentBoardFilterButton" runat="server" CssClass="button3" OnClick="AddWorkforceInvestmentBoardFilterButton_Clicked" /></td>
		</tr>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("Radius.Title", "Radius")%></h5></td>
		</tr>
		<tr>
			<td>
				<table>
					<tr>
						<td><asp:DropDownList runat="server" ID="RadiusDistanceDropDown" ClientIDMode="Static" /></td>
						<td>&nbsp;<%= HtmlLocalise("RadiusFrom.Label", "from")%>&nbsp;</td>
						<td>
							<%= HtmlInFieldLabel("RadiusZIPCodeTextBox", "RadiusZIPCode.InlineLabel", "zip code", 90)%>
							<asp:TextBox runat="server" ID="RadiusZIPCodeTextBox" Text="" Width="100" ClientIDMode="Static" MaxLength="10" />
						</td>
					</tr>
				</table>
			</td>
			<td><asp:Button ID="AddRadiusFilterButton" runat="server" CssClass="button3" OnClick="AddRadiusFilterButton_Clicked" /></td>		
		</tr>
	</table>
</asp:Panel>
<act:CollapsiblePanelExtender ID="LocationPanelExtender" runat="server" 
														TargetControlID="LocationPanel" 
														ExpandControlID="LocationHeaderPanel" 
														CollapseControlID="LocationHeaderPanel" 
														Collapsed="true" 
														ImageControlID="LocationHeaderImage" 
														SuppressPostBack="true"
														BehaviorID="LocationPanelBehavior" />
