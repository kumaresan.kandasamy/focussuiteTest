﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfficeModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.OfficeModal" %>
<%@ Register tagPrefix="uc" tagName="ListOfficeStaffMembersModal" src="~/WebAssist/Controls/ListOfficeStaffMembersModal.ascx" %>
<%@ Register tagName="UpdateableClientList" tagPrefix="focus" src="~/Code/Controls/User/UpdateableClientList.ascx" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Import Namespace="Focus" %>

<asp:HiddenField ID="OfficeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="OfficeModalPopup" runat="server" BehaviorID="OfficeModal"
												TargetControlID="OfficeModalDummyTarget"
												PopupControlID="OfficeModalPanel"
												PopupDragHandleControlID="OfficeModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="OfficeModalPanel" runat="server" CssClass="modal" style="display: none;">
	<asp:Panel runat="server" ClientIDMode="Static" ID="OfficeModalPanelHeader">
	  <div>
		  <div>
	      <h1>
		      <asp:label runat="server" ID="HeaderLabel" Text="Create office"/>
					<asp:Image ID="DefaultOfficeGeneralIcon" runat="server" Visible="False" CssClass="defaultOfficeListIcon" AlternateText="Default Office General Icon" />
					<asp:Image runat="server" ID="DefaultOfficeJobOrderIcon" Visible="False" CssClass="defaultOfficeListIcon" AlternateText="Default Office Job Order Icon" />
					<asp:Image runat="server" ID="DefaultOfficeJobSeekerIcon" Visible="False" CssClass="defaultOfficeListIcon" AlternateText="Default Office JobSeeker Icon"/>
					<asp:Image runat="server" ID="DefaultOfficeEmployerIcon" Visible="False" CssClass="defaultOfficeListIcon" AlternateText="Default Office Employer Icon" />
	      </h1>
	    </div>
			<div style="float:right; text-align: right">
        <asp:Button ID="ActivateButton" runat="server" CssClass="button4" onClick="ActivateButton_Clicked" />
        <asp:Button ID="DeactivateButton" runat="server" CssClass="button4" onClick="DeactivateButton_Clicked" />
        <asp:Button ID="SaveButton" runat="server" CssClass="button4" onClick="SaveButton_Clicked" ValidationGroup="OfficeSave"/>
      </div>
      <div style="clear: both"></div>
    </div>
	</asp:Panel>
	<div>
		<table role="presentation">
		  <asp:PlaceHolder runat="server" ID="OfficeStatusPlaceHolder">
        <tr>
				  <td class="label"><%= HtmlLabel(OfficeStatusTextBox, "OfficeStatus.Label", "Office status")%></td>
          <td colspan="3">
            <asp:textbox Id="OfficeStatusTextBox" runat="server" Enabled="False"></asp:textbox>
          </td>
        </tr>
      </asp:PlaceHolder>
			<tr>
				<td class="label"><%= HtmlRequiredLabel(OfficeNameTextBox, "OfficeName.Label", "Office name")%></td>
				<td colspan="3">
					<asp:TextBox ID="OfficeNameTextBox" runat="server" ClientIDMode="Static"/>
					<asp:RequiredFieldValidator ID="OfficeNameRequired" runat="server" ControlToValidate="OfficeNameTextBox" SetFocusOnError="true" Display="Dynamic" ValidationGroup="OfficeSave" CssClass="error" />
					<asp:RegularExpressionValidator ID="OfficeNameRegEx" runat="server" ControlToValidate="OfficeNameTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OfficeSave"/>
				</td>
			</tr>
			<tr>
				<td class="label" id="ExternalIdRequiredLabelCell" runat="server">
					<%= HtmlRequiredLabel(ExternalIdTextBox, "ExternalIdTextBox.Label", "Office ID")%>
				</td>
				<td class="label" id="ExternalIdLabelCell" runat="server">
					<%= HtmlLabel("ExternalIdTextBox.Label", "Office ID")%>
				</td>
				<td style="width:150px;"><asp:TextBox runat="server" ID="ExternalIdTextBox" ClientIDMode="Static" /></td>
				<td style="width:20px;"><%= HtmlTooltipster("tooltipWithArrow", "ExternalId.Tooltip", @"This is the external office ID used to map case management activities")%>	</td>
				<td>
				  <asp:RequiredFieldValidator ID="ExternalIdRequired" runat="server" ControlToValidate="ExternalIdTextBox" SetFocusOnError="true" ValidationGroup="OfficeSave" CssClass="error" Display="Dynamic" />
				  <asp:CustomValidator ID="ExternalIdDuplicate" runat="server" SetFocusOnError="true" ValidationGroup="OfficeSave" CssClass="error" Display="Dynamic" />
				</td>
			</tr>
			<tr>
				<td class="label"><%= HtmlRequiredLabel(Line1TextBox, "Line1.Label", "Address line 1")%></td>
				<td colspan="3"><asp:TextBox ID="Line1TextBox" runat="server" ClientIDMode="Static"/>
				<asp:RequiredFieldValidator ID="Line1Required" runat="server" ControlToValidate="Line1TextBox" SetFocusOnError="true" ValidationGroup="OfficeSave" CssClass="error" /></td>
			</tr>
			<tr>
				<td class="label"><%= HtmlLabel(Line2TextBox, "Line2.Label", "Address line 2")%></td>
				<td><asp:TextBox ID="Line2TextBox" runat="server" ClientIDMode="Static"/></td>
			</tr>
			<tr>
				<td class="label"><%= HtmlRequiredLabel(PostcodeZipTextBox, "PostcodeZip.Label", "Zip code")%></td>
				<td colspan="3">
					<asp:TextBox ID="PostcodeZipTextBox" runat="server" ClientIDMode="Static"/>
					<asp:RequiredFieldValidator ID="PostcodeZipRequired" runat="server" ControlToValidate="PostcodeZipTextBox" SetFocusOnError="true" ValidationGroup="OfficeSave" CssClass="error" />
				</td>
			</tr>
			<tr>
				<td class="label"><%= HtmlLabel(TownCityTextBox, "TownCity.Label", "City")%></td>
				<td colspan="3">
					<asp:TextBox ID="TownCityTextBox" runat="server" ClientIDMode="Static"/>
				</td>
			</tr>
			<tr>
				<td class="label"><%= HtmlRequiredLabel(StateDropDownList, "State.Label", "State")%></td>
				<td colspan="3">
					<asp:Dropdownlist ID="StateDropDownList" runat="server" ClientIDMode="Static" onchange="UpdateCountry();"/>
					<asp:RequiredFieldValidator ID="StateRequired" runat="server" ControlToValidate="StateDropDownList" SetFocusOnError="true" ValidationGroup="OfficeSave" CssClass="error" />
				</td>
			</tr>
            <tr>
				<td class="label"><%= HtmlLabel(CountyDropDownList, "County.Label", "County")%></td>
				<td colspan="3">
                <span lang="">
					<focus:AjaxDropDownList ID="CountyDropDownList" runat="server" ClientIDMode="Static" onchange="UpdateStateCountry()"/>
					<act:CascadingDropDown ID="CountyDropDownListCascadingDropDown" runat="server" TargetControlID="CountyDropDownList" ParentControlID="StateDropDownList" 
																ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetCounties" Category="Counties" ClientIDMode="Static" BehaviorID="CountyDropDownListCascadingDropDown"/>
				</span>
                </td>
			</tr>
			<tr>
				<td class="label"><%= HtmlLabel(CountryDropDownList, "Country.Label", "Country")%></td>
				<td colspan="3">
					<asp:Dropdownlist ID="CountryDropDownList" runat="server" ClientIDMode="Static" onchange="UpdateStateCounty();"/>
				</td>
			</tr>
			<tr>
				<td class="label" style="white-space:nowrap;"><%= HtmlRequiredLabel(OfficeManagerMailboxTextBox, "OfficeManagerMailbox.Label", "Office manager mailbox")%></td>
				<td colspan="3">
					<asp:TextBox ID="OfficeManagerMailboxTextBox" runat="server" ClientIDMode="Static"/>
					<asp:RequiredFieldValidator ID="OfficeManagerMailboxRequired" runat="server" ControlToValidate="OfficeManagerMailboxTextBox" SetFocusOnError="true" Display="Dynamic" ValidationGroup="OfficeSave" CssClass="error" />
          <asp:RegularExpressionValidator ID="OfficeManagerMailboxRegEx" runat="server" ControlToValidate="OfficeManagerMailboxTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OfficeSave"/>
				</td>
			</tr>
			<tr runat="server" id="BusinessOutreachRow">
				<td class="label" style="white-space:nowrap;"><%= HtmlRequiredLabel(BusinessOutreachMailboxTextBox, "BusinessOutreachMailbox.Label", "Business outreach mailbox")%></td>
				<td colspan="3">
					<asp:TextBox ID="BusinessOutreachMailboxTextBox" runat="server" ClientIDMode="Static"/>
					<asp:RequiredFieldValidator ID="BusinessOutreachMailboxRequired" runat="server" ControlToValidate="BusinessOutreachMailboxTextBox" SetFocusOnError="true" Display="Dynamic" ValidationGroup="OfficeSave" CssClass="error" />
          <asp:RegularExpressionValidator ID="BusinessOutreachMailboxRegEx" runat="server" ControlToValidate="BusinessOutreachMailboxTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OfficeSave"/>
				</td>
			</tr>
			<tr id="AssignOfficeStaffRow" runat="server">
				<td class="label" style="vertical-align:top; white-space:nowrap;"><focus:LocalisedLabel ID="AssignOfficeStaffLabel" runat="server" LocalisationKey="AssignOfficeStaff.Label" DefaultText="Assign office staff" /></td>
				<td colspan="2">
					<focus:UpdateableClientList ID="StaffMemberList" runat="server" SelectionBoxTitle="Assigned staff" InputControlType="AutoCompleteTextBox"/>
				</td>
				<td style="vertical-align: top;">
					<asp:CustomValidator ID="StaffMemberListValidator" runat="server" SetFocusOnError="true" Display="Dynamic" ValidationGroup="OfficeSave" CssClass="error" />
				</td>
			</tr>
			<tr>
				<td class="label"><%= HtmlLabel(AssignPostalZipTextBox, "AssignPostalZip.Label", "Assigned zip codes")%></td>
				<td colspan="3">
					<asp:TextBox ID="AssignPostalZipTextBox" runat="server" TextMode="MultiLine" Width="500" ClientIDMode="Static"/>
				</td>
			</tr>
			<tr><td colspan="4"><asp:RegularExpressionValidator ID="AssignPostalZipRegEx" runat="server" ControlToValidate="AssignPostalZipTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OfficeSave"/></td></tr>
		</table>
		<asp:HiddenField runat="server" ID="IdHiddenField" ClientIDMode="Static"/>
	</div>
 	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('<%=OfficeModalPopup.BehaviorID %>').hide(); return false;" /></div>
</asp:Panel>
<uc:ListOfficeStaffMembersModal ID="ListOfficeStaffMembersModal" runat="server"/>	
<uc:ConfirmationModal ID="ConfirmationModal" runat="server" Width="300px" ClientIDMode="AutoID" OnOkCommand="ConfirmationModal_OkCommand" />
<uc:ConfirmationModal ID="UpdateSuccessModal" runat="server" Width="300px" ClientIDMode="AutoID" />
<script type="text/javascript">
	$(document).ready(
		function () {
			$("#PostcodeZipTextBox").mask("<%= OldApp_RefactorIfFound.Settings.PostalCodeMaskPattern %>", { placeholder: " " });
			$("#PostcodeZipTextBox").change(function () { GetStateCityCountyAndCountiesForPostalCode($(this).val(), "StateDropDownList", "<%= CountyDropDownList.ClientID %>", "<%= CountyDropDownListCascadingDropDown.ClientID %>"); });
		}
	);

	function GetStateCityCountyAndCountiesForPostalCode(postalCode, stateDropDownList, countyDropDownList, countyCascadingDropDownList) {
		var options = { type: "POST",
			url: "<%= UrlBuilder.AjaxService() %>/GetStateCityCountyAndCountiesForPostalCode",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: true,
			data: '{"postalCode": "' + postalCode + '"}',
			success: function (response) {
				var results = response.d;

				if (results && results.StateId) {
					$("#" + stateDropDownList).val(results.StateId);

					if (results.Counties && results.Counties.length > 0) {
						$("#" + countyDropDownList).html('');
						$('<option><%=HtmlLocalise("Global.County.TopDefault", "- select county -") %></option>').appendTo("#" + countyDropDownList);

						var text = '';
						for (i = 0; i < results.Counties.length; i++) {
							$('<option value="' + results.Counties[i].value + '">' + results.Counties[i].name + '</option>').appendTo("#" + countyDropDownList);
							if (results.Counties[i].value == results.CountyId)
								text = results.Counties[i].name;
						}

						if (results.CountyId) {
							$("#" + countyDropDownList).val(results.CountyId);
							$find(countyCascadingDropDownList).set_SelectedValue(results.CountyId, text);
						}

						$("#" + countyDropDownList).removeAttr('disabled');
					}
				}
				else {
					$("#" + stateDropDownList).val('');
					$("#" + countyDropDownList).html('');
					$('<option><%=HtmlLocalise("Global.County.TopDefault", "- select county -") %></option>').appendTo("#" + countyDropDownList);
					$("#" + countyDropDownList).val('');
					$("#" + countyDropDownList).attr('disabled', '');
					$find(countyCascadingDropDownList).set_SelectedValue('', '');
				}
			}
		};

		$.ajax(options);
	}

	Sys.Application.add_load(OfficeModal_PageLoad);
	function OfficeModal_PageLoad(sender, args) {
//		var StateCdd = $find("StateDropDownList");
//		if (StateCdd != null)
//			StateCdd.add_populated(StateDropDownList_OnPopulated);
		
		var countyBehavior = $find('<%=CountyDropDownListCascadingDropDown.BehaviorID %>');
		if (countyBehavior != null) {
				countyBehavior.add_populated(function() {
					OfficeModal_UpdateCountyDropDown($('#CountyDropDownList'), $("#StateDropDownList"));
				});
			}
	}
		
//	function StateDropDownList_OnPopulated() {
//		UpdateStyledDropDown($('#<%=StateDropDownList.ClientID%>'));
//	}

	function OfficeModal_UpdateCountyDropDown(countyDropDown, stateDropDown) {
		if (countyDropDown.children('option:selected').val() != '')
			countyDropDown.next().find(':first-child').text(countyDropDown.children('option:selected').text()).parent().addClass('changed');
		else
			SetCounty(stateDropDown.val() == <%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>);
        
		UpdateStyledDropDown(countyDropDown, stateDropDown.val());
	}
  
	function UpdateStateCountry() {
		var countyValue = $('#CountryDropDownList').val();
		var stateValue = $('#StateDropDownList').val();

		if (countyValue == '<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>') {
			if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>')
				SetState(true);
			SetCountry(true);
		}
	}
	  
	function County_InvokeCountyCascadeDropdown() {
		$get("<%= CountyDropDownList.ClientID %>")._behaviors[0]._onParentChange(null, null);
	}

  function UpdateStateCounty() {
		var countryValue = $('#CountryDropDownList').val();
		var stateDropdown = $('#StateDropDownList');
		var stateValue = stateDropdown.val();

		if (countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
			if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
       	SetState(false);

       	County_InvokeCountyCascadeDropdown();
			}
		}
		else {
			if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
       	SetState(true);

       	County_InvokeCountyCascadeDropdown();
			}
		}
  }
       
	function SetCounty(outsideUS) {
		var countyDropDown = $('#CountyDropDownList');

		if (outsideUS)
			$(countyDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>"]').prop('selected', true);
		else
			$(countyDropDown).children().first().prop('selected', true);
	  
		$(countyDropDown).next().find(':first-child').text($(countyDropDown).children('option:selected').text()).parent().addClass('changed');
	}
			 
	function SetCountry(outsideUS) {
		var countryDropDown = $('#CountryDropDownList');

		if (outsideUS)
			$(countryDropDown).children().first().prop('selected', true);
		else 
			$(countryDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>"]').prop('selected', true);
	  
		$(countryDropDown).next().find(':first-child').text($(countryDropDown).children('option:selected').text()).parent().addClass('changed');
	}

  function SetState(outsideUS) {
		var stateDropDown = $('#StateDropDownList');

		if (outsideUS)
			$(stateDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>"]').prop('selected', true);
		else
			$(stateDropDown).children().first().prop('selected', true);

		$(stateDropDown).next().find(':first-child').text($(stateDropDown).children('option:selected').text()).parent().addClass('changed');
  }
       
	function UpdateCountry() {
		var stateValue = $('#StateDropDownList').val();

		if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
			$("#PostcodeZipTextBox").mask("<%= OldApp_RefactorIfFound.Settings.PostalCodeMaskPattern %>", { placeholder: " " });
		}
		else {
			$("#PostcodeZipTextBox").unmask();
		}

		SetCountry(stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>');
	}

</script>
