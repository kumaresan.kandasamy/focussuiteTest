﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;


#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class NotesAndRemindersModal : UserControlBase
	{
    public delegate void CompletedHandler(object sender, EventArgs eventArgs);
    public event CompletedHandler Completed;

    public virtual void OnCompleted(EventArgs eventargs)
    {
      if (Completed != null) Completed(this, eventargs);
    }

    /// <summary>
    /// Main page load event
    /// </summary>
    /// <param name="sender">Page raising the event</param>
    /// <param name="e">Standard Event Arguments</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      NotesRemindersModalPopup.BehaviorID = string.Concat(ClientID, "_ModalBehaviour");
    }

		/// <summary>
		/// Shows the specified entity id.
		/// </summary>
		/// <param name="entityId">The entity id.</param>
		/// <param name="entityType">Type of the entity.</param>
		/// <param name="readOnly">if set to <c>true</c> [read only].</param>
		public void Show(long entityId, EntityTypes entityType, bool readOnly = false)
		{
			NotesReminders.EntityId = entityId;
			NotesReminders.EntityType = entityType;
			NotesReminders.ReadOnly = readOnly;
			NotesReminders.Bind();
			
			NotesRemindersModalPopup.Show();
		}

		/// <summary>
		/// Handles the Completed event of the AddNote control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddNote_Completed(object sender, EventArgs e)
		{
			NotesRemindersModalPopup.Hide();

			NotesRemindersConfirmationModal.Show(CodeLocalise("Confirmation", "Confirmation"),
																					 CodeLocalise("NoteReminderAdded.Body",
																												"Your {0} has been stored against the selected recipient(s)",
																												NotesReminders.AddNoteRadio.Checked
																													? CodeLocalise("Note", "note")
																													: CodeLocalise("Reminder", "reminder")),
																					 CodeLocalise("CloseModal.Text", "OK"), height: 75);

      OnCompleted(e);
		}
	}
}