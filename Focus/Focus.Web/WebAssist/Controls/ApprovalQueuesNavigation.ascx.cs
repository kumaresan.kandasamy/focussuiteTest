﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Web.Code;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class ApprovalQueuesNavigation : UserControlBase
	{
	  protected void Page_Load(object sender, EventArgs e)
	  {
	    if (App.User.IsInRole(Constants.RoleKeys.SystemAdmin))
	    {
	      LocaliseAssistEmployerReferrals.Text = EditLocalisationLink("Global.AssistEmployerReferrals.LinkText.NoText");
	      LocaliseAssistJobSeekerReferrals.Text = EditLocalisationLink("Global.AssistJobSeekerReferrals.LinkText");
	      LocaliseAssistPostingReferrals.Text = EditLocalisationLink("Global.AssistPostingReferrals.LinkText");
	    }
	  }

	  /// <summary>
		/// Determines whether [is job seeker referral approver user].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is job seeker referral approver user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsJobSeekerReferralApproverUser()
		{
			return (App.User.IsInRole(Constants.RoleKeys.AssistJobSeekerReferralApprover) && (App.Settings.JobSeekerReferralsEnabled));
		}

		/// <summary>
		/// Determines whether [is job seeker referral approval viewer user].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is job seeker referral approval viewer user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsJobSeekerReferralApprovalViewerUser()
		{
			return (App.User.IsInRole(Constants.RoleKeys.AssistJobSeekerReferralApprovalViewer) && (App.Settings.JobSeekerReferralsEnabled));
		}

		/// <summary>
		/// Determines whether [is employer account approver user].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is employer account approver user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsEmployerAccountApproverUser()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistEmployerAccountApprover);
		}

		/// <summary>
		/// Determines whether [is employer account approval viewer user].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is employer account approval viewer user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsEmployerAccountApprovalViewerUser()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistEmployerAccountApprovalViewer);
		}

		/// <summary>
		/// Determines whether [is posting approver user].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is posting approver user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsPostingApproverUser()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistPostingApprover);
		}

		/// <summary>
		/// Determines whether [is posting approval viewer user].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is posting approval viewer user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsPostingApprovalViewerUser()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistPostingApprovalViewer);
		}

		/// <summary>
		/// Determines whether [is job seeker referrals page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is job seeker referrals page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsJobSeekerReferralsPage()
		{
			return (Page.Is(UrlBuilder.JobSeekerReferrals()));
		}

		/// <summary>
		/// Determines whether [is employer referrals page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is employer referrals page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsEmployerReferralsPage()
		{
			return (Page.Is(UrlBuilder.EmployerReferrals()));
		}

		/// <summary>
		/// Determines whether [is posting referrals page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is posting referrals page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsPostingReferralsPage()
		{
			return (Page.Is(UrlBuilder.PostingReferrals()));
		}
	}
}