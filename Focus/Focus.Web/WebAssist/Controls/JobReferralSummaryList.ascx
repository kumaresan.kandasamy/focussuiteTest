﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobReferralSummaryList.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.JobReferralSummaryList" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>

<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>

<div>
  <%= HtmlLocalise("ReferralInstructions.Label", "Reporting outcomes are reported voluntarily by hiring managers and job seekers. If outcomes do not display in the Referral Outcome column, they were not provided by customers.") %>
  <br />
</div>
<asp:UpdatePanel ID="JobReferralSummaryListUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<table style="width:100%;" role="presentation">
			<tr>
				<td>
					<%=HtmlLabel(DaysFilterDropDown,"ShowResultsFor.Text", "Show results for")%>
					&nbsp;
					<asp:DropDownList runat="server" ID="DaysFilterDropDown" OnSelectedIndexChanged="DaysFilterDropDown_SelectedIndexChanged" AutoPostBack="True" />
					&nbsp;&nbsp;
					<%=HtmlLabel(StatusFilterDropDown,"Display.Text", "Display")%>
					&nbsp;
					<asp:DropDownList runat="server" ID="StatusFilterDropDown" OnSelectedIndexChanged="StatusFilterDropDown_SelectedIndexChanged" AutoPostBack="True" />
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
		</table>
		<table ID="JobReferralListHeaderTable" runat="server" style="width: 100%;" role="presentation">
			<tr>
				<td style="text-align:right; white-space:nowrap;"><uc:Pager ID="JobReferralListPager" runat="server" PagedControlID="JobReferralList" PageSize="10" DisplayRecordCount="true" /></td>
			</tr>
		</table>
		<asp:ListView ID="JobReferralList" runat="server" ItemPlaceholderID="ListPlaceHolder" DataSourceID="JobReferralListDataSource" DataKeyNames="Id" OnLayoutCreated="JobReferralList_LayoutCreated" 
									OnSorting="JobReferralList_Sorting" OnItemDataBound="JobReferralList_ItemDataBound" >
			<LayoutTemplate>
				<table style="width:100%;" class="table" role="presentation">
					<tr>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:Literal runat="server" ID="DateHeader" /></td>
									<td>
									  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="DateSortAscButton" runat="server" CommandName="Sort" CommandArgument="datereceived asc" CssClass="AscButton" />
                   <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="DateSortDescButton" runat="server" CommandName="Sort" CommandArgument="datereceived desc" CssClass="DescButton" />
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:Literal runat="server" ID="RatingHeader" /></td>
									<td>
									  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="RatingSortAscButton" runat="server" CommandName="Sort" CommandArgument="score asc" CssClass="AscButton" />
                   <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="RatingSortDescButton" runat="server" CommandName="Sort" CommandArgument="score desc" CssClass="DescButton" />
									</td>
								</tr>
								<tr>
									<td></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:Literal runat="server" ID="NameHeader" /></td>
									<td>
									  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NameSortAscButton" runat="server" CommandName="Sort" CommandArgument="name asc" CssClass="AscButton" />
                   <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NameSortDescButton" runat="server" CommandName="Sort" CommandArgument="name desc" CssClass="DescButton" />
                  </td>
								</tr>
							</table>
						</td>
            <asp:PlaceHolder runat="server" ID="RecentEmploymentHeaderCell" Visible="False">
						<td>
							<table role="presentation">
								<tr>
									<td><asp:Literal runat="server" ID="RecentEmploymentHeader" /></td>
								</tr>
							</table>
						</td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="ProgramOfStudyHeaderCell" Visible="False">
						  <td>
							  <table role="presentation">
								  <tr>
									  <td><asp:Literal runat="server" ID="ProgramOfStudyHeader" /></td>
									  <td>
									    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ProgramOfStudySortAscButton" runat="server" CommandName="Sort" CommandArgument="studyprogram asc" CssClass="AscButton" />
                      <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ProgramOfStudySortDescButton" runat="server" CommandName="Sort" CommandArgument="studyprogram desc" CssClass="DescButton" />
									  </td>
								  </tr>
							  </table>
						  </td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="ExperienceHeaderCell" Visible="False">
						<td>
							<table role="presentation">
								<tr>
									<td><asp:Literal runat="server" ID="ExperienceHeader" /></td>
									<td>
									  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ExperienceSortAscButton" runat="server" CommandName="Sort" CommandArgument="yearsexperience asc" CssClass="AscButton" />
                   <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ExperienceSortDescButton" runat="server" CommandName="Sort" CommandArgument="yearsexperience desc" CssClass="DescButton" />
									</td>
								</tr>
							</table>
						</td>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="CompletionDateHeaderCell" Visible="False">
						<td>
							<table role="presentation">
								<tr>
									<td><asp:Literal runat="server" ID="CompletionDateHeader" /></td>
									<td>
									  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="CompletionDateSortAscButton" runat="server" CommandName="Sort" CommandArgument="educationcompletiondate asc" CssClass="AscButton" />
                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="CompletionDateSortDescButton" runat="server" CommandName="Sort" CommandArgument="educationcompletiondate desc" CssClass="DescButton" />
									</td>
								</tr>
							</table>
						</td>
            </asp:PlaceHolder>
						<td>
							<table role="presentation">
								<tr>
									<td><asp:Literal runat="server" ID="ReferralOutcomeHeader" /></td>
									<td>
									  <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ReferralOutcomeSortAscButton" runat="server" CommandName="Sort" CommandArgument="status asc" CssClass="AscButton" />
                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ReferralOutcomeSortDescButton" runat="server" CommandName="Sort" CommandArgument="status desc" CssClass="DescButton" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<asp:PlaceHolder ID="ListPlaceHolder" runat="server" />
				</table>
			</LayoutTemplate>
			<ItemTemplate>
				<tr>
					<td><%# ((ApplicationStatusLogViewDto)Container.DataItem).ActionedOn.ToString("MMM dd, yyyy")%></td>
					<td><asp:image ID="ReferralScoreImage" runat="server" AlternateText="Referral Score Image"/></td>
					<td><%# ((ApplicationStatusLogViewDto)Container.DataItem).CandidateFirstName%> <%# ((ApplicationStatusLogViewDto)Container.DataItem).CandidateLastName%></td>
          <asp:PlaceHolder runat="server" ID="ReferralLastEmploymentBodyCell" Visible="False">
					  <td><asp:literal ID="ReferralLastEmploymentLiteral" runat="server" /></td>
          </asp:PlaceHolder>
          <asp:PlaceHolder runat="server" ID="ProgramOfStudyBodyCell" Visible="False">
            <td data-cell="programofstudy"><asp:literal ID="ProgramOfStudyLiteral" runat="server" /></td>	
          </asp:PlaceHolder>
          <asp:PlaceHolder runat="server" ID="ExperienceBodyCell" Visible="False">
            <td><%# ((ApplicationStatusLogViewDto)Container.DataItem).CandidateYearsExperience%></td>
          </asp:PlaceHolder>
          <asp:PlaceHolder runat="server" ID="CompletionDateBodyCell" Visible="False">
            <td data-cell="completiondate"><asp:literal ID="CompletionDateLiteral" runat="server" /></td>
          </asp:PlaceHolder>
					<td><asp:literal ID="ReferralOutcomeLiteral" runat="server" /></td>							
				</tr>	
			</ItemTemplate>
			<EmptyDataTemplate>
				<table style="width: 100%;" role="presentation">
					<tr>
						<td data-cell="noreferrals"><%= HtmlLocalise("NoMatches.Text", "No referrals.") %></td>
					</tr>
				</table>
			</EmptyDataTemplate>
		</asp:ListView>

		<asp:ObjectDataSource ID="JobReferralListDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.JobReferralSummaryList" EnablePaging="true" 
														SelectMethod="GetReferralSummaries" SelectCountMethod="GetReferralSummariesCount" SortParameterName="orderBy" OnSelecting="JobReferralListDataSource_Selecting" />
	</ContentTemplate>
</asp:UpdatePanel>
