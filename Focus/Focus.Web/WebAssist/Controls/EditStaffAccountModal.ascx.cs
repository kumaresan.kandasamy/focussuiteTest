﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.Security;

using Focus.Common;
using Focus.Core;
using Focus.Core.Models;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class EditStaffAccountModal : UserControlBase
	{

	  public long StaffId { get; set; }
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		/// <summary>
		/// Shows this instance.
		/// </summary>
		public void Show(long staffId)
		{
			Bind(staffId);
		  StaffId = staffId;
			LocaliseUI();
			EditStaffAccountModalPopup.Show();
		}

		/// <summary>
		/// Handles the Click event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Click(object sender, EventArgs e)
		{
		  var model = ContactInformationMain.Unbind();

		  if (CurrentUserNameTextBox.Text == EditUserNameTextBox.Text)
		  {
        ErrorLabel.Visible = true;
        ErrorLabel.Text = CodeLocalise("UserNameMatchEqual.ErrorMessage", "Username is already used");
        EditStaffAccountModalPopup.Show();
		    return;
		  }
		  model.UserDetails.UserName = EditUserNameTextBox.Text.IsNotNullOrEmpty() ? EditUserNameTextBox.Text : string.Empty;

		  try
		  {
		    var userData = ServiceClientLocator.AccountClient(App).UpdateContactDetails(model);

		    // If we're updating our own user then reset authentication cookie with the details
		    if (userData.Id == App.User.UserId)
		    {
					var cookie = FormsAuthentication.GetAuthCookie(model.PersonDetails.FirstName, true);
		      // Update changed parameters
		      userData.UserName = model.UserDetails.UserName;

		      // Store UserData inside the Forms Ticket with all the attributes in sync with the web.config
		      var newticket = new FormsAuthenticationTicket(1, model.PersonDetails.FirstName, DateTime.Now,
		        DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false,
						App.User.SerializeUserContext());

		      // Encrypt the ticket and store it in the cookie
		      cookie.Value = FormsAuthentication.Encrypt(newticket);

		      Context.Response.Cookies.Set(cookie);
		    }

		    ConfirmationModal.Show(CodeLocalise("SuccessfulStaffUpdate.Title", "Staff Account Details Changed"),
		      CodeLocalise("SuccessfulStaffUpdate.Body",string.Format("You have successfuly changed account details for {0} {1}", model.PersonDetails.FirstName,model.PersonDetails.LastName)),
		      CodeLocalise("CloseModal.Text", "Return to account settings"), "", UrlBuilder.ManageStaffAccounts());
		  }
		  catch (ServiceCallException ex)
		  {
		    switch (ex.Message)
		    {
          case "User already exists":
            ErrorLabel.Text = CodeLocalise("UserNameMatch.ErrorMessage", "Username is already in use by another user");
		        break;
          default:
            ErrorLabel.Text = ex.Message;
            break;
		    }
		    ErrorLabel.Visible = true;

		    EditStaffAccountModalPopup.Show();
		  }
		}

		/// <summary>
		/// Handles the Click event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Click(object sender, EventArgs e)
		{
			EditStaffAccountModalPopup.Hide();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind(long userId)
		{
      var userModel = ContactInformationMain.Bind(userId,"",true,false);
      CurrentUserNameTextBox.Text = userModel.UserDetails.UserName;

      if (App.Settings.ReadOnlyContactInfo && !App.User.IsInRole(Constants.RoleKeys.AssistAccountAdministrator))
		  {
        EditUserNameTextBox.ReadOnly =
        ConfirmUserNameTextBox.ReadOnly = true;
        SaveButton.Visible = false;
		  }

		  UserNamePlaceHolder.Visible = !App.Settings.AuthenticateAssistUserViaClient;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CancelButton.Text = CodeLocalise("Global.Cancel.Label", "Cancel");
			SaveButton.Text = CodeLocalise("Global.Save.Label", "Save");
      ConfirmUserNameCompare.ErrorMessage = CodeLocalise("ConfirmUsernamesCompare.ErrorMessage", "New usernames must match");

      UserNameRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address format is invalid");
      UserNameRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
      CheckUserNameRequired.ErrorMessage = CodeLocalise("ConfirmUsernamesCompare.ErrorMessage", "New usernames must match");
		}
	}
}