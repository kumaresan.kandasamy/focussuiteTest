﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindEmployer.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.FindEmployer" %>
<%@ Import Namespace="Focus.Core.Views" %>
<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>
<%@ Register TagPrefix="uc" TagName="EmailEmployeeModal" Src="~/Code/Controls/User/EmailEmployeeModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="EditCompanyModal" Src="~/Code/Controls/User/EditCompanyModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="ActivityAssignment" Src="~/WebAssist/Controls/ActivityAssignment.ascx" %>
<%@ Register TagPrefix="uc" TagName="UpdateContactInformationModal" Src="~/Code/Controls/User/UpdateContactInformationModal.ascx" %>
<%@ Register TagPrefix="uc" TagName="UpdateableClientList" Src="~/Code/Controls/User/UpdateableClientList.ascx" %>
	
  <div class="findEmployer" id="FindEmployerTable">
	  <div class="findEmployerCriteria1">
			<table role="presentation">
				<tr style="vertical-align: top">
					<td><%= HtmlLabel(FirstNameTextBox, "ContactFirstName.Label", "Hiring mgr. first name")%></td>
					<td><asp:TextBox runat="server" ID="FirstNameTextBox" Width="200" MaxLength="25" /></td>	
      				
					<td><%= HtmlLabel(EmployerNameTextBox, "EmployerName.Label", "#BUSINESS# name")%></td>
					<td><asp:TextBox runat="server" ID="EmployerNameTextBox" Width="200" MaxLength="50" /></td>
					
					<td><focus:LocalisedLabel runat="server" ID="FindEmployerStatusLabel" AssociatedControlID="FindEmployerStatusDropdown" DefaultText="Status"/></td>
					<td><asp:DropDownList runat="server" ID="FindEmployerStatusDropdown"/></td>
				</tr>
				<tr style="vertical-align: top">
					<td><%= HtmlLabel(LastNameTextBox, "ContactLastName.Label", "Hiring mgr. last name")%></td>
					<td><asp:TextBox runat="server" ID="LastNameTextBox" Width="200" MaxLength="25" /></td>

					<td><%= HtmlLabel(ContactEmailTextBox, "ContactEmail.Label", "Hiring mgr. email")%></td>
					<td><asp:TextBox runat="server" ID="ContactEmailTextBox" Width="200" MaxLength="50" /></td>
				</tr>
			</table>
		</div>  

		<asp:PlaceHolder runat="server" ID="FEINPlaceHolder">
      <div class="findEmployerCriteria2">
 	  	  <asp:UpdatePanel ID="FEINUpdatePanel" runat="server" UpdateMode="Conditional">
          <ContentTemplate>
				    <table role="presentation">
					    <tr style="vertical-align: top">
						    <td><%= HtmlLocalise("FederalEmployerIdentificationNumber.Label", "FEIN")%>&nbsp;</td>
						    <td>
							    <uc:UpdateableClientList ID="FEINList" AllowNonAdded="True" InputMask="99-9999999" runat="server" SelectionBoxTitle="Selected FEINs" Width="225px" />
						    </td>
					    </tr>
				    </table>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>
   	</asp:PlaceHolder>
  </div>
  
  <div style="float:right;padding-top: 4px;text-align: right;">
    <asp:Button ID="FindButton" runat="server" class="button3" OnClick="FindButton_Clicked" Text="Find" Tooltip="Find"/>
	  <br />
		<asp:Button ID="ClearButton" runat="server" class="button3" OnClick="ClearButton_Clicked" Tooltip="Clear" style="margin-top: 4px;"  />
  </div>
  
  <div style="clear:both"></div>
  <asp:UpdatePanel ID="FindUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
			<table ID="EmployeeSearchHeaderTable" runat="server" Visible="false" style="width:100%;">
				<tr>
					<td style="width:50%;"><strong><%= HtmlLocalise("SearchResults.Title", "Search results")%></strong>&nbsp;&nbsp;<asp:literal ID="ResultCount" runat="server" /></td>
					<td style="text-align: right; white-space: nowrap;"><uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="EmployeesList" PageSize="10" /></td>
				</tr>
			</table>
			<asp:ListView ID="EmployeesList" runat="server" OnItemDataBound="ItemsList_ItemDataBound" ItemPlaceholderID="SearchResultPlaceHolder" OnLayoutCreated="EmployeesList_LayoutCreated"  OnSorting="EmployeesList_Sorting" DataSourceID="SearchResultDataSource" DataKeyNames="Id, EmployerId, EmployeeApprovalStatus, AccountEnabled, BusinessUnitId, BusinessUnitName, AccountBlocked, IsPreferred">
				<LayoutTemplate>
					<table class="table" style="width:100%;" id="FindEmployerResultsList" >
						<thead>
							<tr>
								<th />
								<th />
								<th>
									<asp:Literal runat="server" ID="ContactNameHeader" />
									<asp:ImageButton CssClass="AscButton" ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ContactNameSortAscButton" runat="server" CommandName="Sort" CommandArgument="contactname asc" />
									<asp:ImageButton CssClass="DescButton" ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ContactNameSortDescButton" runat="server" CommandName="Sort" CommandArgument="contactname desc" />
								</th>

								<th>
									<asp:Literal runat="server" ID="EmployerNameHeader" />
									<asp:ImageButton CssClass="AscButton" ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmployerNameSortAscButton" runat="server" CommandName="Sort" CommandArgument="businessunitname asc" />
									<asp:ImageButton CssClass="DescButton" ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmployerNameSortDescButton" runat="server" CommandName="Sort" CommandArgument="businessunitname desc" />
								</th>
								
								<th>
									<asp:Literal runat="server" ID="PhoneNumberHeader" />
									<asp:ImageButton CssClass="AscButton" ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="PhoneNumberSortAscButton" runat="server" CommandName="Sort" CommandArgument="phonenumber asc" />
									<asp:ImageButton CssClass="DescButton" ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="PhoneNumberSortDescButton" runat="server" CommandName="Sort" CommandArgument="phonenumber desc" />
								</th>
								<th>
									<asp:Literal runat="server" ID="LocationHeader" />
									<asp:ImageButton CssClass="AscButton" ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LocationSortAscButton" runat="server" CommandName="Sort" CommandArgument="location asc" />
									<asp:ImageButton CssClass="DescButton" ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="LocationSortDescButton" runat="server" CommandName="Sort" CommandArgument="location desc" />
								</th>						
							</tr>
							</thead>
						<tbody>
							<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
						</tbody>
					</table>
				</LayoutTemplate>
				<ItemTemplate>
					<tr>
						<td style="width:1%;">
							<%if (IsAssistEmployersAdministrator())
							{%>
								<asp:CheckBox ID="SelectorCheckBox" GroupName="SelectorCheckBoxes" runat="server" ClientIDMode="Static" CausesValidation="False" AutoPostBack="False" />
							<% } %>
						</td>
						<td style="width:1%; text-align:left; white-space:nowrap;">
							<%# GetEmployeeStatusImage((EmployeeSearchResultView)Container.DataItem) %>
						</td>
						<td style="width:23%;">
							<asp:Literal runat="server" ID="HiringManagerLink"/>
						</td>
						<td style="width:28%;">
							<a id="BusinessUnitUrl" runat="server" href=""><%# ((EmployeeSearchResultView)Container.DataItem).BusinessUnitName %></a>
							<%# GetEmployerStatusImages((EmployeeSearchResultView)Container.DataItem)%>
						</td>
						<td style="width:21%;"><asp:Literal id="PhoneNumber" runat="server" /></td>							
						<td style="width:26%;"><%# ((EmployeeSearchResultView)Container.DataItem).Town %>, <%# ((EmployeeSearchResultView)Container.DataItem).State %></td>							
					</tr>	
				</ItemTemplate>
				<EmptyDataTemplate>
					<table ID="Table1" runat="server" Visible="false" style="width:100%;" role="presentation">
						<tr>
							<td style="width:50%;"><strong><%= HtmlLocalise("SearchResults.Title", "Search results")%></strong>&nbsp;&nbsp;<asp:literal ID="Literal1" runat="server" /></td>
							<td style="text-align:right; white-space:nowrap;"><uc:Pager ID="Pager1" runat="server" PagedControlID="EmployeesList" PageSize="10" /></td>
						</tr>
						<tr><td>No Results Found</td></tr>
					</table>
				</EmptyDataTemplate>
			</asp:ListView>
  </ContentTemplate>
  <Triggers>
    <asp:AsyncPostBackTrigger ControlID="FindButton"/> 
    <asp:AsyncPostBackTrigger ControlID="ClearButton"/> 
  </Triggers>
  </asp:UpdatePanel>  

	<asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.FindEmployer" EnablePaging="true" 
										SelectMethod="GetEmployees" SelectCountMethod="GetEmployeesCount" SortParameterName="orderBy" OnSelecting="SearchResultDataSource_Selecting" >
	</asp:ObjectDataSource>

	
	
  <asp:UpdatePanel ID="ActionUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
		<table style="width:100%;" role="presentation">
			<tr>
				<td style="width:15%; vertical-align:middle;"><asp:Label ID="ActionLabel" runat="server" Visible="False" ClientIDMode="Static" AssociatedControlID="ActionDropDown" /></td>
				<td style="width:85%;">
					<br />
					<asp:DropDownList runat="server" ID="ActionDropDown" Width="350" Visible="false" ClientIDMode="Static" />
					<asp:Button ID="ActionGoButton" runat="server" class="button3" Visible="false" ClientIDMode="Static" OnClick="ActionButton_Clicked" OnClientClick="FindEmployer_ResetValues()" ValidationGroup="Action" Text="Go"/>
					<asp:RequiredFieldValidator ID="ActionDropDownRequired" runat="server" ValidationGroup="Action" ControlToValidate="ActionDropDown" CssClass="error"/>
					<asp:CustomValidator ID="MultipleRowValidator" runat="server" CssClass="error" ControlToValidate="ActionDropDown" SetFocusOnError="True" ClientValidationFunction="FindBusiness_ActionDropDown_Validate" ValidationGroup="Action" />
					<asp:CustomValidator ID="EmployeesListValidator" runat="server" ClientValidationFunction="validateRowSelected" ValidationGroup="Action" ValidateEmptyText="True" CssClass="error"/>
					<br />
					<br />
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<uc:ActivityAssignment ID="ActivityAssigner" runat="server" ActivityType="Employer" Flow="Horizontal" OnActivitySelected="ActivityAssignment_ActivitySelected" Visible="False" />
					<asp:DropDownList runat="server" ID="SubActionDropDown" Width="225" Visible="false" ClientIDMode="Static" />
					<asp:Button ID="SubActionButton" runat="server" class="button3" Visible="false" ClientIDMode="Static"  ValidationGroup="SubAction" OnClick="SubActionButton_Clicked" Text="Go"/>
					<asp:RequiredFieldValidator ID="SubActionDropDownRequired" runat="server" ValidationGroup="SubAction" ControlToValidate="SubActionDropDown" CssClass="error" />
				</td>
			</tr>
		</table>
    <%-- Modals --%>
    <uc:EditCompanyModal ID="EditCompany" runat="server" OnBusinessUnitUpdated="EditCompanyModal_BusinessUnitUpdated" ShowConfirmationOnSave="True" CloseWindowOnSave="True" />
    <uc:ConfirmationModal ID="Confirmation" runat="server" />
	  <uc:ConfirmationModal ID="ActionConfirmationModal" runat="server" OnOkCommand="ActionConfirmationModal_OkCommand" />
    <uc:EmailEmployeeModal ID="EmailEmployee" runat="server" />
		<uc:UpdateContactInformationModal ID="UpdateContactInformationModal" Visible="False" runat="server" OnContactUpdated="EditContactModal_ContactUpdated"/>	
    </ContentTemplate>
      <Triggers>
      <asp:AsyncPostBackTrigger ControlID="SubActionButton" EventName="Click"/> 
      <asp:AsyncPostBackTrigger ControlID="ActionGoButton" EventName="Click"/>
      </Triggers>
    </asp:UpdatePanel>

	<script type="text/javascript">

		var FindBusiness_TalentPopup = null;

		Sys.Application.add_load(function () {
			BindTooltips();
		});

		$(document).ready(function () {
			$("#FindEmployerTable input:text").keypress(function (event) {
				if (event.keyCode == 13) {
					$("#<%=FindButton.ClientID %>").click();
				}

				return event.keyCode != 13;
			});
		});

		function validateRowSelected(oSrc, args) {
			args.IsValid = checkRowSelected();
		}

		function checkRowSelected() {
			var checkBoxes = $('input:checkbox');
			var checked = false;

			for (i = 0; i <= checkBoxes.length - 1; i++)
				if (checkBoxes[i].checked)
					checked = true;

			return checked;
		}

		function showPopup(url, blockedPopupMessage) {
			var popup = window.open(url, "Business_blank");
			if (!popup)
				alert(blockedPopupMessage);

			return popup;
		}

		function FindEmployer_ResetValues() {

			//reset activity default category value
			if ($("#MainContent_FindEmployer_ActivityAssigner_ActivityCategoryDropDownList").exists()) {
				$("#MainContent_FindEmployer_ActivityAssigner_ActivityCategoryDropDownList")[0].selectedIndex = 0;
			}

			return true;
		}

		function FindBusiness_ActionDropDown_Validate(sender, args) {

			var action = $("#<%=ActionDropDown.ClientID %>").val();

			if (action == "AccessEmployeeAccount" && (FindBusiness_TalentPopup != null && !FindBusiness_TalentPopup.closed)) {
				$(sender).text("<%=CloseFindBusinessWindowErrorMessage %>");
				args.IsValid = false;
			}
		}
	</script>

 
