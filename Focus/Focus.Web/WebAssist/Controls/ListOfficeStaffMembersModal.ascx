﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListOfficeStaffMembersModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ListOfficeStaffMembersModal" %>
<%@ Import Namespace="Focus.Core.Models" %>

<asp:HiddenField ID="ListOfficeStaffMembersModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ListOfficeStaffMembersModalPopup" runat="server" BehaviorID="ListOfficeStaffMembersModal"
												TargetControlID="ListOfficeStaffMembersModalDummyTarget"
												PopupControlID="ListOfficeStaffMembersModalPanel"
												PopupDragHandleControlID="ListOfficeStaffMembersModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ListOfficeStaffMembersModalPanel" runat="server" CssClass="modal" style="display: none;">
	<asp:Panel runat="server" ClientIDMode="Static" ID="ListOfficeStaffMembersModalPanelHeader">
	<h2><asp:label runat="server" ID="HeaderLabel" Text="Office staff members"/></h2>
	</asp:Panel>
	<div style="width: 710px">
		<table role="presentation">
			<tr>
				<td class="label" style="width: 30px"><asp:Label runat="server" ID="OfficeManagerHeaderLabel"/></td>
				<td><asp:Label runat="server" ID="OfficeManagerLabel" /></td>
			</tr>
			<tr id="NoStaffRow" runat="server" Visible="False">
				<td colspan="2"><asp:Label runat="server" ID="NoStaffLabel" /></td>
			</tr>
		</table>
	</div>
	<asp:Panel ID="StaffMembersListPanel" runat="server" ClientIDMode="Static">
	<ContentTemplate>
	<asp:ListView ID="StaffMemberList" runat="server" DataKeyNames="FirstName, MiddleInitial, LastName" Title="StaffMemberList" ItemPlaceholderID="StaffSearchResultPlaceHolder" OnItemDataBound="StaffMemberList_ItemDataBound" >
		<LayoutTemplate>	
		<div style="height: 125px; overflow-y: scroll; width: 620px;">
			<table style="width:100%;" class="table">
				<tr>
					<th class="th2">
						<table role="presentation">
							<tr>
								<td rowspan="2" height="16px"><asp:Literal runat="server" ID="FirstNameHeader" /></td>
							</tr>
						</table>
					</th>
					<th class="th2">
						<table role="presentation">
							<tr>
								<td rowspan="2" height="16px"><asp:Literal runat="server" ID="LastNameHeader" /></td>
							</tr>
						</table>
					</th>
				</tr>
				<asp:PlaceHolder ID="StaffSearchResultPlaceHolder" runat="server" />
			</table>
			</div>	
		</LayoutTemplate>
		<ItemTemplate>
			<tr>
				<td><asp:Label ID="Label2" runat="server" Text='<%#((PersonModel)Container.DataItem).FirstName%>'/></td>
				<td><asp:Label ID="Label4" runat="server" Text='<%#((PersonModel)Container.DataItem).LastName%>'/></td>
			</tr>	
		</ItemTemplate>
	</asp:ListView>
	</ContentTemplate>
	<div style="width: 710px">
		<asp:Button ID="OkButton" runat="server" CssClass="button4" CausesValidation="False" />
	</div>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('<%=ListOfficeStaffMembersModalPopup.BehaviorID %>').hide(); return false;" /></div>
</asp:Panel>
</asp:Panel>

