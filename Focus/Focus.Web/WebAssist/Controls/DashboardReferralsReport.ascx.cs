﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Focus.Core;
using Focus.Web.Code;

#endregion


namespace Focus.Web.WebAssist.Controls
{
	public partial class DashboardReferralsReport : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LoadReport();
		}

		/// <summary>
		/// Job seeker referrals enabled.
		/// </summary>
		/// <returns></returns>
		protected bool JobSeekerReferralsEnabled()
		{
			return App.Settings.JobSeekerReferralsEnabled;
		}

		/// <summary>
		/// Loads the report.
		/// </summary>
		private void LoadReport()
		{
			var report = ServiceClientLocator.ReportClient(App).GetApprovalQueueStatusReport();

			NewPostingsCountLiteral.Text = report.NewPostingsSinceLastLoginCount.ToString();
			FlaggedPostingsCountLiteral.Text = report.FlaggedPostingsCount.ToString();
			NewEmployerAccountsCountLiteral.Text = report.NewEmployeeAccountsSinceLastLoginCount.ToString();
			OutstandingEmployerAccountsCountLiteral.Text = report.OutstandingEmployeeAccountsCount.ToString();
			NewReferralRequestsCountLiteral.Text = report.NewReferralRequestsSinceLastLoginCount.ToString();
			OutstandingReferralRequestsCountLiteral.Text = report.OutstandingReferralRequestsCount.ToString();

			JobPostingsHeaderLiteral.Text = ( App.User.IsInRole(Constants.RoleKeys.AssistPostingApprover)
																				? EditLocalisationLink("JobPostings.Header.NoEdit") + "<a href='" + UrlBuilder.PostingReferrals() + "'><strong>" + CodeLocalise("JobPostings.Header.NoEdit", "#EMPLOYMENTTYPES#") + "</strong></a>"
																				: "<strong>" + CodeLocalise("JobPostings.Header", "#EMPLOYMENTTYPES#") + "</strong>");

		  NewEmployerAccountsHeaderLiteral.Text = (App.User.IsInRole(Constants.RoleKeys.AssistEmployerAccountApprover)
		                                             ? EditLocalisationLink("NewEmployerAccounts.Header.NoEdit") +
		                                               "<a href='" +
		                                               UrlBuilder.EmployerReferrals() + "'><strong>" +
		                                               CodeLocalise("NewEmployerAccounts.Header.NoEdit",
																																"#BUSINESS# account requests") + "</strong></a>"
		                                             : "<strong>" +
																									 CodeLocalise("NewEmployerAccounts.Header", "#BUSINESS# account requests") +
		                                               "</strong>");


			ReferralRequestsHeaderLiteral.Text = ( App.User.IsInRole(Constants.RoleKeys.AssistJobSeekerReferralApprover)
                                             ? EditLocalisationLink("ReferralRequests.Header.NoEdit") + "<a href='" + UrlBuilder.JobSeekerReferrals() + "'><strong>" + CodeLocalise("ReferralRequests.Header.NoEdit", "Referral requests") + "</strong></a>"
																						 : "<strong>" + CodeLocalise("ReferralRequests.Header", "Referral requests") + "</strong>");			
		}
	}
}