﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;

using Focus.Web.Code;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class AssistEmployersNavigation : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyTheme();
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				InActiveAssistEmployersLabel.Text = ActiveAssistEmployersLabel.Text = CodeLocalise("Global.AssistEmployers.LinkText", "Find a #BUSINESS#:LOWER account");
			}
			else if (App.Settings.Theme == FocusThemes.Education)
			{
				InActiveAssistEmployersLabel.Text = ActiveAssistEmployersLabel.Text = CodeLocalise("Global.AssistEmployers.LinkText", "Find an #BUSINESS#:LOWER account"); // Because in this instance the placeholder will be "employer"
			}
		}

		/// <summary>
		/// Determines whether [is assist employers administrator].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is assist employers administrator]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistEmployersAdministrator()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
		}

		/// <summary>
		/// Determines whether [has assist employers send messages role].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [has assist employers send messages role]; otherwise, <c>false</c>.
		/// </returns>
		protected bool HasAssistEmployersSendMessagesRole()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistEmployersSendMessages);
		}

		/// <summary>
		/// Determines whether [is employers page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is employers page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsEmployersPage()
		{
			// IsEmployers is catching Find employer and Send messages due to route having /employers/ in common	
			return (Page.Is(UrlBuilder.AssistEmployers()) && !Page.Is(UrlBuilder.MessageEmployers()));
		}

		/// <summary>
		/// Determines whether [is development page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is development page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsDevelopmentPage()
		{
			return (Page.Is(UrlBuilder.JobDevelopment()) || Page.Is(UrlBuilder.PostingEmployerContact()));
		}

		/// <summary>
		/// Determines whether [is message employers page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is message employers page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsMessageEmployersPage()
		{
			return (Page.Is(UrlBuilder.MessageEmployers()));
		}

    /// <summary>
    /// Determines whether [is job order dashboard page].
    /// </summary>
    /// <returns>
    /// 	<c>true</c> if [is job order dashboard page]; otherwise, <c>false</c>.
    /// </returns>
    protected bool IsJobOrderDashBoardPage()
    {
      return (Page.Is(UrlBuilder.JobOrderDashboard()));
    }

    /// <summary>
    /// Determines whether [is job development page].
    /// </summary>
    /// <returns>
    /// 	<c>true</c> if [is job development page]; otherwise, <c>false</c>.
    /// </returns>
    protected bool IsJobDevelopmentPage()
    {
      return (Page.Is(UrlBuilder.JobDevelopment()));
    }

		/// <summary>
		/// Determines whether [is job wizard page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is job wizard page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsJobWizardPage()
		{
			return (Page.Is(UrlBuilder.AssistJobWizard()));
		}

        /// <summary>
        /// Determines whether [is talent pool page].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is talent pool page]; otherwise, <c>false</c>.
        /// </returns>
        protected  bool IsTalentPoolPage()
        {
          return Page.Is(UrlBuilder.AssistPoolForPathCompareOnly());
        }

        protected bool IsJobDevelopmentManager()
        {
            return App.User.IsInRole(Constants.RoleKeys.AssistEmployerJobDevelopmentManager);
        }
	}
}