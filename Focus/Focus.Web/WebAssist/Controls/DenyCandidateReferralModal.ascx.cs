﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Web.Code;
using Framework.Core;
using Mindscape.LightSpeed;
using Framework.Exceptions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class DenyCandidateReferralModal : UserControlBase
	{
		private long CandidateApplicationId
		{
			get { return GetViewStateValue<long>("DenyCandidateReferralModal:CandidateApplicationId"); }
			set { SetViewStateValue("DenyCandidateReferralModal:CandidateApplicationId", value); }
		}

		private int ApplicationLockVersion
		{
			get { return GetViewStateValue<int>("DenyCandidateReferralModal:ApplicationLockVersion"); }
			set { SetViewStateValue("DenyCandidateReferralModal:ApplicationLockVersion", value); }
		}

		private EmailTemplateView EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>("DenyCandidateReferralModal:EmailTemplate"); }
			set { SetViewStateValue("DenyCandidateReferralModal:EmailTemplate", value); }
		}

		private bool JobReferralFilterApplied
		{
			get { return GetViewStateValue<bool>("DenyCandidateReferralModal:JobReferralFilterApplied"); }
			set { SetViewStateValue("DenyCandidateReferralModal:JobReferralFilterApplied", value); }
		}

		private const string FilteredURL = "?filtered=true";
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Shows the specified candidate application id.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="filtered"></param>
		public void Show(long candidateApplicationId, int applicationLockVersion, bool filtered = false)
		{
			CandidateApplicationId = candidateApplicationId;
			ApplicationLockVersion = applicationLockVersion;
			JobReferralFilterApplied = filtered;

			GetEmailTemplate();
			DenialReasonsTextBox.Text = string.Empty;

			var regex = new Regex( Constants.PlaceHolders.DenialReasons );
			var emailBodySplit = regex.Split(EmailTemplate.Body);
			TopEmailBodyLiteral.Text = emailBodySplit[0].TrimEnd('\r', '\n').Replace(Environment.NewLine, "<br />");
			BottomEmailBodyLiteral.Text = (emailBodySplit.Length > 1) ? emailBodySplit[1].TrimStart('\r', '\n').Replace(Environment.NewLine, "<br />") : string.Empty;

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				CheckBoxDenialReasonsPlaceholder.Visible = true;
				cblDenialReasons.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobSeekerDenialReasons));
				OtherDenialReasonTextBox.ResetText();
				OtherDenialReasonTextBox.Enabled = false;
			}
			else
			{
				EmailBodyTextBoxPlaceholder.Visible = true;
				CheckBoxDenialReasonsPlaceholder.Visible = false;
			}

			ModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the SendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DenyAndSendMessageButton_Clicked(object sender, EventArgs e)
		{
			string denialReasons;
			List<KeyValuePair<long, string>> denialList = null;
			var coreClient = ServiceClientLocator.CoreClient(App);
			var candidateClient = ServiceClientLocator.CandidateClient(App);

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				var otherId = ServiceClientLocator.CoreClient( App ).GetLookup( LookupTypes.JobSeekerDenialReasons ).Where( x => x.Key.Equals( "JobSeekerDenialReasons.Other" ) ).Select( x => x.Id ).FirstOrDefault();
				var query = (from ListItem checkbox in cblDenialReasons.Items
										 where checkbox.Selected && checkbox.Value.IsNotNullOrEmpty() && checkbox.Value != otherId.ToString()
					select checkbox).ToList();

				#region Create a list of the reasons to be displayed in the email

				var denialReasonList = query.Select(checkBox => checkBox.Text).ToList();

				var otherReason = OtherDenialReasonTextBox.TextTrimmed();

				if (otherReason.IsNotNullOrEmpty())
					denialReasonList.Add(otherReason);

				denialReasons = string.Join(Environment.NewLine, denialReasonList.Select(x => string.Concat("* ", x)));

				#endregion

				#region Create a list of keyvaluepairs of the reason Ids and Other text for saving to the database

				denialList = query.Select(checkBox => new KeyValuePair<long, string>(Convert.ToInt64(checkBox.Value), string.Empty)).ToList();

				if (otherReason.IsNotNullOrEmpty())
				{
					var otherReasonEntry = new KeyValuePair<long, string>(otherId, otherReason);
					denialList.Add(otherReasonEntry);
				}

				#endregion
			}
			else
			{
				denialReasons = DenialReasonsTextBox.TextTrimmed();
			}

			var jobSeekerReferralView = candidateClient.GetJobSeekerReferral(CandidateApplicationId);
			var emailBody = EmailTemplate.Body.Replace(Constants.PlaceHolders.DenialReasons, denialReasons);
			try
			{
				candidateClient.DenyCandidateReferral( CandidateApplicationId, ApplicationLockVersion, denialList );

				candidateClient.SendCandidateEmail(jobSeekerReferralView.CandidateId, EmailTemplate.Subject, emailBody, false, BCCMeCheckBox.Checked);
				coreClient.SaveReferralEmail(EmailTemplate.Subject, emailBody, ApprovalStatuses.Rejected, null, CandidateApplicationId);

				ModalPopup.Hide();

				Confirmation.Show(CodeLocalise("ReferralDeniedConfirmation.Title", "Referral denied"),
					CodeLocalise("ReferralDeniedConfirmation.Body", "The referral has been denied."),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink: JobReferralFilterApplied ? UrlBuilder.JobSeekerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.JobSeekerReferrals());
			}
			catch (ServiceCallException ex)
			{
				Confirmation.Show(CodeLocalise("ReferralDeniedError.Title", "Denial failed"),
					CodeLocalise("ReferralDeniedError.Body", string.Format("There was a problem denying the referral.<br /><br />{0}", ex.Message)),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink: JobReferralFilterApplied ? UrlBuilder.JobSeekerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.JobSeekerReferrals());
			}
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			DenyAndSendMessageButton.Text = CodeLocalise("DenyAndSendMessageButton.Text", "Email #CANDIDATETYPE#");
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");
			DenialReasonsRequired.ErrorMessage = CodeLocalise("DenialReasonsRequired.ErrorMessage", "Denial reasons required");
			DenialReasonsCheckboxRequired.ErrorMessage = CodeLocalise("DenialReasonsCheckboxRequired.ErrorMessage", "Denial reasons required");
			OtherDeniedReasonRequired.ErrorMessage = CodeLocalise("OtherDeniedReasonRequired.ErrorMessage", "Other reason description required");
      OtherDenialReasonValidator.ErrorMessage = CodeLocalise("OtherDeniedReasonValidator.ErrorMessage", "Other reason description must be 250 characters or less");
		}

		/// <summary>
		/// Gets the email template.
		/// </summary>
		private void GetEmailTemplate()
		{
			var jobSeekerReferralView = ServiceClientLocator.CandidateClient(App).GetJobSeekerReferral(CandidateApplicationId);
			var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";
		  var employerName = jobSeekerReferralView.EmployerName;

      if (jobSeekerReferralView.IsNotNull() && jobSeekerReferralView.IsConfidential.Value)
		    employerName = CodeLocalise("Confidential.Label", "[Confidential]");

			var templateValues = new EmailTemplateData
			                     	{
			                     		RecipientName = jobSeekerReferralView.Name,
			                     		JobTitle = jobSeekerReferralView.JobTitle,
			                     		EmployerName = employerName,
															SenderPhoneNumber = Regex.Replace(phoneNumber, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat),
			                     		SenderEmailAddress = ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.DenyCandidateReferral),
			                     		SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName)
			                     	};
			
			EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.DenyCandidateReferral, templateValues);
		}
	}
}