﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class ReplaceParentModal : UserControlBase
	{

    #region Properties

		protected long EmployerId
    {
			get { return GetViewStateValue<long>("ReplaceParentModal:EmployerId"); }
			set { SetViewStateValue("ReplaceParentModal:EmployerId", value); }
    }

		protected string BusinessUnitName
		{
			get { return GetViewStateValue<string>("ReplaceParentModal:BusinessUnitName"); }
			set { SetViewStateValue("ReplaceParentModal:BusinessUnitName", value); }
		}

		protected long? BusinessUnitId
		{
			get { return GetViewStateValue<long?>("ReplaceParentModal:BusinessUnitId"); }
			set { SetViewStateValue("ReplaceParentModal:BusinessUnitId", value);}
		}

		protected string Title;

    #endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
      }
    }

		/// <summary>
		/// Binds the business unit RadioButton list.
		/// </summary>
		/// <param name="employerId">The employer identifier.</param>
		protected void BindBusinessUnitRadioButtonList(long employerId)
		{
			var businessUnits = ServiceClientLocator.EmployerClient(App).GetBusinessUnits(employerId).Where(x => !x.IsPrimary);
			BusinessUnitRadioButtonList.DataSource = businessUnits;
			BusinessUnitRadioButtonList.DataValueField = "Id";
			BusinessUnitRadioButtonList.DataTextField = "Name";
			BusinessUnitRadioButtonList.DataBind();
		}

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		protected void Unbind()
		{
			//TODO: implement unbind
		}


		/// <summary>
		/// Shows the specified employer identifier.
		/// </summary>
		/// <param name="employerId">The employer identifier.</param>
		/// <param name="businessUnitName">Name of the business unit.</param>
		/// <param name="businessUnitId">The business unit identifier.</param>
	  public void Show(long employerId, string businessUnitName, long? businessUnitId)
		{
			Localise();
			EmployerId = employerId;
			BusinessUnitName = businessUnitName;
			BusinessUnitId = businessUnitId;
			BindBusinessUnitRadioButtonList(employerId);
				
			ReplaceParentModalPopup.Show();
    }

    /// <summary>
    /// Handles the Clicked event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void CancelButton_Clicked(object sender, EventArgs e)
    {
			ReplaceParentModalPopup.Hide();
    }

    /// <summary>
    /// Handles the Clicked event of the SaveButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ReplaceButton_Clicked(object sender, EventArgs e)
    {
	    var newPrimaryBU = ServiceClientLocator.EmployerClient(App).GetBusinessUnit(BusinessUnitRadioButtonList.SelectedValueToLong());
			ServiceClientLocator.EmployerClient(App).UpdatePrimaryBusinessUnit(BusinessUnitId, newPrimaryBU.Id);
			
			ReplaceParentModalPopup.Hide();

			Confirmation.Show(CodeLocalise("ReplaceParent.Header", "Parent #BUSINESS#:LOWER replaced"),
												CodeLocalise("ReplaceParent.Text", "<br/>You have successfully replaced  <br/><br/>\"" + BusinessUnitName + "\"<br/>with<br/>\"" + newPrimaryBU.Name + "\""),
												CodeLocalise("Close.Text", "Close"),
												"ParentReplacedConfirmationClosed",
												height:150
												);
    }

	  /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
			ReplaceButton.Text = CodeLocalise("ReplaceButton.Text", "Replace");
			CancelButton.Text = CodeLocalise("CancelButton.Text", "Cancel");
			Title = CodeLocalise("ReplaceParentModal.Title", "Replace Parent #BUSINESS#");
		  BusinessUnitRadioButtonListRequiredValidator.ErrorMessage = CodeLocalise("BusinessUnitRadioButtonListRequiredValidator.ErrorMessage", "A business unit selection is required");
    }

		/// <summary>
		/// Handles the CloseCommand event of the Confirmation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_CloseCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "ParentReplacedConfirmationClosed")
				OnParentReplaced(new EventArgs());
			
		}

		#region Page event

		public event ParentReplacedHandler ParentReplaced;
		
		/// <summary>
		/// Raises the <see cref="E:ParentReplaced" /> event.
		/// </summary>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		public void OnParentReplaced(EventArgs e)
		{
			var handler = ParentReplaced;
			if (handler != null) handler(this, e);
		}

		public delegate void ParentReplacedHandler(object o, EventArgs e);

		#endregion

   
  }
}