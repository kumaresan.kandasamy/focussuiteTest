﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Text.RegularExpressions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
  public partial class EditSsnModal : UserControlBase
  {

    #region Properties

    protected long Id
    {
      get { return GetViewStateValue<long>("EditSsnModal:Id"); }
      set { SetViewStateValue("EditSsnModal:Id", value); }
    }

		protected string SsnRequired
		{
			get { return GetViewStateValue<string>("EditSsnModal:SsnRequired"); }
			set { SetViewStateValue("EditSsnModal:SsnRequired", value); }
		}

		protected string SsnInvalid
		{
			get { return GetViewStateValue<string>("EditSsnModal:SsnInvalid"); }
			set { SetViewStateValue("EditSsnModal:SsnInvalid", value); }
		}

    #endregion

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
      }
    }

    /// <summary>
    /// Shows the SSN for the specified jobseeker id.
    /// </summary>
    /// <param name="id">The id.</param>
    public void Show(long id)
    {
      NewSsnPart1TextBox.Text = NewSsnPart2TextBox.Text = NewSsnPart3TextBox.Text = string.Empty;
      CurrentSsnPart1TextBox.Text = CurrentSsnPart2TextBox.Text = CurrentSsnPart3TextBox.Text = string.Empty;
      ErrorLabel.Visible = false;

      Id = id;
      var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(personId: id);

      if (userDetails.PersonDetails.SocialSecurityNumber.IsNotNullOrEmpty())
      {
        var matches = Regex.Matches(userDetails.PersonDetails.SocialSecurityNumber, @"^([0-9a-zA-Z]{3})[\- ]?([0-9a-zA-Z]{2})[\- ]?([0-9a-zA-Z]{4})$");

        if (matches.IsNotNull() && matches.Count == 1)
        {
          Match match = matches[0];

          CurrentSsnPart1TextBox.Text = match.Groups[1].Value;
          CurrentSsnPart2TextBox.Text = match.Groups[2].Value;
          CurrentSsnPart3TextBox.Text = match.Groups[3].Value;
        }
      }

      EditSsnModalPopup.Show();
    }

    /// <summary>
    /// Handles the Clicked event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void CancelButton_Clicked(object sender, EventArgs e)
    {
      EditSsnModalPopup.Hide();
    }

    /// <summary>
    /// Handles the Clicked event of the SaveButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void SaveButton_Clicked(object sender, EventArgs e)
    {
      ErrorLabel.Visible = false;
      var currentSsn = string.Concat(CurrentSsnPart1TextBox.Text, CurrentSsnPart2TextBox.Text, CurrentSsnPart3TextBox.Text);
      currentSsn = Regex.Replace(currentSsn, "[^0-9a-zA-Z]+", "");

      var newSsn = string.Concat(NewSsnPart1TextBox.Text, NewSsnPart2TextBox.Text, NewSsnPart3TextBox.Text);
      newSsn = Regex.Replace(newSsn, "[^0-9]+", "");

      var model = new UpdateSsnModel { PersonId = Convert.ToInt64(Id), CurrentSsn = currentSsn, NewSsn = newSsn };
      if (ServiceClientLocator.AccountClient(App).CheckSocialSecurityNumberInUse(newSsn))
      {
        model.Error = ErrorTypes.SsnAlreadyExists;
        model.ErrorText = string.Format("A user with SSN '{0}' already exists.", newSsn);
      }
      else
      {
        ServiceClientLocator.AccountClient(App).UpdateSsn(model);
      }

      if (model.Error.IsNotNull() && model.Error != ErrorTypes.Ok)
      {
        ErrorLabel.Text = CodeLocalise("ErrorLabel.Text", model.ErrorText);
        ErrorLabel.Visible = true;
        EditSsnModalPopup.Show();
      }
      else
      {
        EditSsnModalPopup.Hide();
        Confirmation.Show(CodeLocalise("EditSsn.Header", "SSN changed"),
                          CodeLocalise("EditSsn.Text", "The SSN has been changed."),
                          CodeLocalise("Ok.Text", "OK"));
      }
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      SaveButton.Text = CodeLocalise("IssueResolutionModal.SaveButton", "Save");
      CancelButton.Text = CodeLocalise("IssueResoluti1112onModal.Cancel", "Cancel");
      ModalTitle.Text = CodeLocalise("UpdateSsnModal.Title", "Change SSN");
			SsnInvalid = CodeLocalise("SocialSecurityNumberValidator.Error", "SSN must consist of 9 numbers");
	    SsnRequired = CodeLocalise("SocialSecurityNumberRequired.Error", "SSN is required");
    }

    #region Page event

    public event RequestForFollowUpHandler FollowUpRequested;

    /// <summary>
    /// Raises the <see cref="FollowUpRequested" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    public void OnIssuesResolved(EventArgs e)
    {
      var handler = FollowUpRequested;
      if (handler != null) handler(this, e);
    }

    public delegate void RequestForFollowUpHandler(object o, EventArgs e);

    #endregion
  }
}