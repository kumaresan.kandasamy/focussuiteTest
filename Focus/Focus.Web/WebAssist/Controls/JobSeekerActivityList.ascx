﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobSeekerActivityList.ascx.cs"
    Inherits="Focus.Web.WebAssist.Controls.JobSeekerActivityList" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ActivityAssignment" Src="~/WebAssist/Controls/ActivityAssignment.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<%@ Register Src="~/WebAssist/Controls/JobSeekerListModal.ascx" TagName="JobSeekerListModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/EditSsnModal.ascx" TagName="EditSsnModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ActivityDeleteModal.ascx" TagName="ActivityDeleteModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/BackdateActivityModal.ascx" TagName="BackdateActivityModal"
    TagPrefix="uc" %>
<asp:UpdatePanel ID="JobSeekerActivityListUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table role="presentation" style="width: 100%;">
            <tr>
                <td style="white-space: nowrap;">
                    <%=HtmlLabel(ServiceDropDown,"Show", "Show")%>
                    <asp:DropDownList runat="server" ID="ServiceDropDown" OnSelectedIndexChanged="ServiceDropDown_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <%=HtmlLabel(DaysBackDropDown,"for", "for")%>
                    <asp:DropDownList runat="server" ID="DaysBackDropDown" OnSelectedIndexChanged="DaysBackDropdown_SelectedIndexChanged"
                        AutoPostBack="True" />
                    <%=HtmlLabel(UserDropDown,"by", "by")%>
                    <asp:DropDownList runat="server" ID="UserDropDown" OnSelectedIndexChanged="UserDropDown_SelectedIndexChanged"
                        AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td style="white-space: nowrap;">
                    <uc:Pager ID="JobSeekerActivityListPager" runat="server" PagedControlId="JobSeekerActivityLogList"
                        DisplayRecordCount="False" PageSize="10" />
                </td>
            </tr>
        </table>
        <asp:ListView runat="server" ID="JobSeekerActivityLogList" ItemPlaceholderID="ListPlaceHolder"
            DataSourceID="JobSeekerActivityListDataSource" OnSorting="JobSeekerActivityLogList_Sorting"
            OnItemCommand="JobSeekerActivityLogList_ItemCommand" OnLayoutCreated="JobSeekerActivityLogList_LayoutCreated"
            OnItemDataBound="JobSeekerActivityLogList_ItemDataBound">
            <LayoutTemplate>
                <table style="width: 100%;" class="table" id="JobSeekerActivityTable" role="presentation">
                    <thead>
                        <td class="tableHead">
                            <table role="presentation">
                                <tr>
                                    <td rowspan="2" style="height: 16px">
                                        <asp:Literal runat="server" ID="UserName" />
                                    </td>
                                    <td style="height: 8px">
                                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="UserNameSortAscButton"
                                            runat="server" CommandName="Sort" CommandArgument="activityusername asc" CssClass="AscButton"
                                            AlternateText="." Width="15" Height="9" />
                                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="UserNameSortDescButton"
                                            runat="server" CommandName="Sort" CommandArgument="activityusername desc" CssClass="DescButton"
                                            AlternateText="." Width="15" Height="9" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="tableHead">
                            <table role="presentation">
                                <tr>
                                    <td rowspan="2" style="height: 16px;">
                                        <asp:Literal runat="server" ID="ActivityDate" />
                                    </td>
                                    <td style="height: 8px;">
                                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ActivityDateSortAscButton"
                                            runat="server" CommandName="Sort" CommandArgument="activitydate asc" CssClass="AscButton"
                                            AlternateText="." Width="15" Height="9" />
                                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ActivityDateSortDescButton"
                                            runat="server" CommandName="Sort" CommandArgument="activitydate desc" CssClass="DescButton"
                                            AlternateText="." Width="15" Height="9" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                       <td class="tableHead" style="text-align: left;">
                            <asp:Literal runat="server" ID="Action" />
                        </td>
                       <td class="tableHead">
                        </td>
                        <td class="tableHead">
                        </td>
                    </thead>
                    <asp:PlaceHolder ID="ListPlaceHolder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td runat="server" id="UsernameDisplay" style="padding-right: 15px; white-space: nowrap">
                        <%# ((JobSeekerActivityActionViewDto)Container.DataItem).UserName%>
                    </td>
                    <td runat="server" id="ActionDate" style="padding-right: 15px; white-space: nowrap">
                        <%# ((JobSeekerActivityActionViewDto)Container.DataItem).ActionedOn%>
                    </td>
                    <td runat="server" id="ActionDescriptionRow">
                        <asp:Literal runat="server" ID="ActionDescription"></asp:Literal>
                        <asp:Literal runat="server" ID="TooltipDetails"></asp:Literal>
                    </td>
                    <td style="padding-right: 10px; padding-left: 10px; white-space: nowrap;">
                        <asp:LinkButton runat="server" ID="JobSeekerBackdateLinkbutton" Text="Backdate" CommandName="backdateActivity"
                            CommandArgument='<%# Eval("ID") + ", " + Eval("EntityIdAdditional01") + "," + Eval("UserId") + "," + Eval("ActionType") + "," + Eval("ActionedOn") %>'  />
                    </td>
                    <td>
                        <asp:LinkButton runat="server" ID="JobSeekerDeleteLinkButton" Text="Delete" CommandName="deleteActivity"
                            CommandArgument='<%# Eval("ID") + "," + Eval("EntityIdAdditional01") + "," + Eval("UserId") + "," + Eval("ActionType") + "," + Eval("ActionedOn") %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
                <br />
                <%=HtmlLocalise("NoJobActivity", "No activity recorded for this job seeker") %></EmptyDataTemplate>
        </asp:ListView>
        <asp:ObjectDataSource runat="server" ID="JobSeekerActivityListDataSource" TypeName="Focus.Web.WebAssist.Controls.JobSeekerActivityList"
            SortParameterName="orderBy" SelectMethod="GetJobSeekerActivity" SelectCountMethod="GetJobSeekerActivityCount"
            OnSelecting="JobSeekerActivityListDataSource_Selecting" EnablePaging="True">
        </asp:ObjectDataSource>
    </ContentTemplate>
</asp:UpdatePanel>
<br />
<asp:PlaceHolder ID="ActionPlaceHolder" runat="server">
    <div>
        <%= HtmlLabel(ActionDropDown,"WebAssist.JobOrderDashboard.Action.Label", "Action")%>
        <asp:DropDownList runat="server" ID="ActionDropDown" Width="350" Visible="True" ClientIDMode="Static"
            Style="margin-left: 5px;" />
        <asp:Button ID="ActionGoButton" runat="server" class="button3" Visible="True" ClientIDMode="Static"
            OnClick="ActionButton_Clicked" OnClientClick="JobSeekerActivityList_ResetValues()"
            ValidationGroup="Action" ClientValidationFunction="JobSeekerActivityList_ActionDropDown_Validate" />
        <asp:RequiredFieldValidator ID="ActionDropDownRequired" runat="server" ValidationGroup="Action"
            ControlToValidate="ActionDropDown" CssClass="error" />
        <asp:CustomValidator ID="ActionDropDownValidator" runat="server" CssClass="error"
            ControlToValidate="ActionDropDown" SetFocusOnError="True" />
    </div>
    <br />
    <div class="offset-1">
        <uc:ActivityAssignment ID="ActivityAssigner" runat="server" ActivityType="JobSeeker"
            Flow="Horizontal" OnActivitySelected="ActivityAssignment_ActivitySelected" Visible="False" />
        <asp:DropDownList runat="server" ID="SubActionDropDown" Width="225" Visible="false"
            ClientIDMode="Static" />
        <asp:Button ID="SubActionButton" runat="server" class="button3" Visible="false" ClientIDMode="Static"
            ValidationGroup="SubAction" OnClick="SubActionButton_Clicked" />
        <asp:RequiredFieldValidator ID="SubActionDropDownRequired" runat="server" ValidationGroup="SubAction"
            ControlToValidate="SubActionDropDown" CssClass="error" />
    </div>
</asp:PlaceHolder>
<uc:ConfirmationModal ID="Confirmation" runat="server" OnOkCommand="Confirmation_OnOkCommand" />
<uc:JobSeekerListModal ID="JobSeekerList" runat="server" />
<uc:EditSsnModal ID="EditSsnModal" runat="server" />
<uc:ActivityDeleteModal ID="DeleteActivity" runat="server" OnActivityDeleted="DeleteActivity_ActivityDeleted" />
<uc:BackdateActivityModal ID="BackdateActivity" runat="server" OnActivityBackdated="BacdateActivity_ActivityBackdated" />
<script type="text/javascript">
    var JobSeekers_CareerPopup = null;

    function JobSeekerActivityList_ResetValues() {

        //reset activity default category value
        if ($("#MainContent_JobSeekerActivityList_ActivityAssigner_ActivityCategoryDropDownList").exists()) {
            $("#MainContent_JobSeekerActivityList_ActivityAssigner_ActivityCategoryDropDownList")[0].selectedIndex = 0;
        }

        return true;
    }

    function JobSeekerActivityList_ActionDropDown_Validate() {
        if (action == "AccessCandidateAccount" && (JobSeekers_CareerPopup != null && !JobSeekers_CareerPopup.closed)) {
            $(sender).text("<%=CloseJobSeekerWindowErrorMessage %>");
            args.IsValid = false;
        }
    }

    function showPopup(url, blockedPopupMessage) {
        var popup = window.open(url, "JobSeekerProfile_blank");
        if (!popup)
            alert(blockedPopupMessage);

        return popup;
    }
</script>
