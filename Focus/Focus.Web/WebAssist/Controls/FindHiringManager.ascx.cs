﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Employee;
using Focus.Core.Views;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class FindHiringManager : UserControlBase
	{
		private int _employeesCount;
		private string _altEmployeeApproved, _altEmployeeRejected, _altEmployeeAwaitingApproval;

		private EmployeeCriteria EmployeeCriteria
		{
			get { return GetViewStateValue<EmployeeCriteria>("FindHiringManager:EmployeeCriteria"); }
			set { SetViewStateValue("FindHiringManager:EmployeeCriteria", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			_altEmployeeApproved = CodeLocalise("EmployeeApproved.Hint", "Employee approved");
			_altEmployeeRejected = CodeLocalise("EmployeeRejected.Hint", "Employee rejected");
			_altEmployeeAwaitingApproval = CodeLocalise("EmployeeAwaitingApproval", "Employee awaiting approval");

			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Handles the Click event of the FindButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FindButton_Click(object sender, EventArgs e)
		{
			SearchResultListPager.ReturnToFirstPage();
			UnbindSearch();
			EmployeeCriteria.OrderBy = Constants.SortOrders.BusinessUnitNameAsc;
			BindEmployeesList();

			// Set the sort images
			if (SearchResultListPager.TotalRowCount > 0) FormatEmployeesListSortingImages();
		}

		/// <summary>
		/// Handles the Click event of the CreateJobButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CreateJobButton_Click(object sender, EventArgs e)
		{
			var employeeId = GetSelectedEmployeeId();
		  Response.Redirect(String.Format("{0}?managerId={1}",UrlBuilder.AssistJobWizard(), employeeId));
		}

		/// <summary>
		/// Handles the Click event of the CreateEmployerButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CreateEmployerButton_Click(object sender, EventArgs e)
		{
			Response.Redirect(UrlBuilder.AssistRegisterEmployer());
		}

		/// <summary>
		/// Gets the employee status image.
		/// </summary>
		/// <param name="approvalStatuses">The approval statuses.</param>
		/// <returns></returns>
		protected string GetEmployeeStatusImage(ApprovalStatuses approvalStatuses, bool accountBlocked, BlockedReason? blockedReason)
		{
			string img, tooltip;

			switch (approvalStatuses)
			{
				case ApprovalStatuses.Approved:
					img = UrlBuilder.EmployerApprovedIcon();
					tooltip = _altEmployeeApproved;
					break;

				case ApprovalStatuses.Rejected:
					img = UrlBuilder.EmployerRejectedIcon();
					tooltip = _altEmployeeRejected;
					break;

				default:
					img = UrlBuilder.EmployerNewIcon();
					tooltip = _altEmployeeAwaitingApproval;
					break;
			}

            var imagesString = new StringBuilder();
			imagesString.AppendFormat("<img src='{0}' alt='{1}' title='{1}' />", img, tooltip);

            if (accountBlocked)
            {
                switch (blockedReason)
                {
                    case null:
                    case BlockedReason.Unknown:
                        imagesString.AppendFormat("<img src='{0}' alt='{1}'  title='{1}' />", UrlBuilder.BlockedImage(), CodeLocalise("BlockedImage.ToolTip", "Blocked"));
                        break;
                    case BlockedReason.FailedPasswordReset:
                        imagesString.AppendFormat("<img src='{0}' alt='{1}'  title='{1}' />", UrlBuilder.UserBlockedFailedPasswordReset(), CodeLocalise("BlockedImage.ToolTip", "Blocked - Failed Security"));
                        break;
                    case BlockedReason.AlienRegistrationDateExpired:
                        imagesString.AppendFormat("<img src='{0}' alt='{1}'  title='{1}' />", UrlBuilder.UserBlockedFailedPasswordReset(), CodeLocalise("BlockedImage.ToolTip", "Blocked - Alien registration date expired"));
                        break;
                }
            }

			return imagesString.ToString();
		}

		/// <summary>
		/// Handles the Selecting event of the SearchResultDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["criteria"] = EmployeeCriteria;
		}

		/// <summary>
		/// Handles the LayoutCreated event of the EmployeesList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmployeesList_LayoutCreated(object sender, EventArgs e)
		{
			// Set labels
			((Literal)EmployeesList.FindControl("EmployerNameHeader")).Text = CodeLocalise("EmployerNameHeader.Text", "#BUSINESS# name");
			((Literal)EmployeesList.FindControl("ContactNameHeader")).Text = CodeLocalise("ContactNameHeader.Text", "Contact name");
			((Literal)EmployeesList.FindControl("PhoneNumberHeader")).Text = CodeLocalise("PhoneNumberHeader.Text", "Phone number");
			((Literal)EmployeesList.FindControl("LocationHeader")).Text = CodeLocalise("LocationHeader.Text", "Location");
		}

		/// <summary>
		/// Handles the Sorting event of the EmployeesList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void EmployeesList_Sorting(object sender, ListViewSortEventArgs e)
		{
			EmployeeCriteria.OrderBy = e.SortExpression;
			FormatEmployeesListSortingImages();
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the employees.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<EmployeeSearchResultView> GetEmployees(EmployeeCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_employeesCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;

			var employees = ServiceClientLocator.EmployeeClient(App).SearchEmployees(criteria);
			_employeesCount = employees.TotalCount;

			return employees;
		}

		/// <summary>
		/// Gets the employees count.
		/// </summary>
		/// <returns></returns>
		public int GetEmployeesCount()
		{
			return _employeesCount;
		}

		/// <summary>
		/// Gets the employees count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetEmployeesCount(EmployeeCriteria criteria)
		{
			return _employeesCount;
		}

		#endregion

		/// <summary>
		/// Unbinds the search.
		/// </summary>
		private void UnbindSearch()
		{
			EmployeeCriteria = new EmployeeCriteria();

			if (FirstNameTextBox.TextTrimmed().IsNotNullOrEmpty())
				EmployeeCriteria.Firstname = FirstNameTextBox.TextTrimmed();

			if (LastNameTextBox.TextTrimmed().IsNotNullOrEmpty())
				EmployeeCriteria.Lastname = LastNameTextBox.TextTrimmed();

			if (EmployerNameTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				var employerCharacteristics = new EmployerCharacteristicsCriteria();

				if (EmployerNameTextBox.TextTrimmed().IsNotNullOrEmpty())
					employerCharacteristics.EmployerName = EmployerNameTextBox.TextTrimmed();

				EmployeeCriteria.EmployerCharacteristics = employerCharacteristics;
			}

			if (EmailAddressTextBox.TextTrimmed().IsNotNullOrEmpty())
				EmployeeCriteria.EmailAddress = EmailAddressTextBox.TextTrimmed();
		}

		/// <summary>
		/// Binds the employees list.
		/// </summary>
		private void BindEmployeesList()
		{
			EmployeesList.DataBind();

			// Set visibility of other controls
			EmployeeSearchHeaderTable.Visible = EmployeesList.Visible = true;
			CreateEmployerButton.Visible = App.User.IsInRole(Constants.RoleKeys.AssistEmployersCreateNewAccount);
			SearchResultListPager.Visible = CreateJobButton.Visible = (SearchResultListPager.TotalRowCount > 0);

			ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} results found", SearchResultListPager.TotalRowCount.ToString());
		}

		/// <summary>
		/// Gets the selected employee id.
		/// </summary>
		/// <returns></returns>
		private long GetSelectedEmployeeId()
		{
			long employeeId;
			long.TryParse(Request.Form["SelectorRadio"], out employeeId);

			return employeeId;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FindButton.Text = CodeLocalise("CreateJobFindButton.Text.NoEdit", "Find");
			CreateEmployerButton.Text = CodeLocalise("CreateEmployerButton.Text", "Create new #BUSINESS#:LOWER & #EMPLOYMENTTYPE#:LOWER");
			CreateJobButton.Text = CodeLocalise("CreateJobButton.Text", "Create #EMPLOYMENTTYPE#:LOWER");
			EmployeesListValidator.ErrorMessage = CodeLocalise("EmployeesListValidator.ErrorMessage", "You must select a #BUSINESS#:LOWER record to run this action");
		}

		/// <summary>
		/// Formats the employees list sorting images.
		/// </summary>
		private void FormatEmployeesListSortingImages()
		{
			// Reset image URLs
			((ImageButton)EmployeesList.FindControl("EmployerNameSortAscButton")).ImageUrl =
				((ImageButton)EmployeesList.FindControl("ContactNameSortAscButton")).ImageUrl =
				((ImageButton)EmployeesList.FindControl("PhoneNumberSortAscButton")).ImageUrl =
				((ImageButton)EmployeesList.FindControl("LocationSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)EmployeesList.FindControl("EmployerNameSortDescButton")).ImageUrl =
				((ImageButton)EmployeesList.FindControl("ContactNameSortDescButton")).ImageUrl =
				((ImageButton)EmployeesList.FindControl("PhoneNumberSortDescButton")).ImageUrl =
				((ImageButton)EmployeesList.FindControl("LocationSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton)EmployeesList.FindControl("EmployerNameSortAscButton")).Enabled =
				((ImageButton)EmployeesList.FindControl("ContactNameSortAscButton")).Enabled =
				((ImageButton)EmployeesList.FindControl("PhoneNumberSortAscButton")).Enabled =
				((ImageButton)EmployeesList.FindControl("LocationSortAscButton")).Enabled =
				((ImageButton)EmployeesList.FindControl("EmployerNameSortDescButton")).Enabled =
				((ImageButton)EmployeesList.FindControl("ContactNameSortDescButton")).Enabled =
				((ImageButton)EmployeesList.FindControl("PhoneNumberSortDescButton")).Enabled =
				((ImageButton)EmployeesList.FindControl("LocationSortDescButton")).Enabled = true;

			switch(EmployeeCriteria.OrderBy.ToLower())
			{
				case Constants.SortOrders.BusinessUnitNameAsc:
					((ImageButton)EmployeesList.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton) EmployeesList.FindControl("EmployerNameSortAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.BusinessUnitNameDesc:
					((ImageButton)EmployeesList.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployeesList.FindControl("EmployerNameSortDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.ContactNameAsc:
					((ImageButton)EmployeesList.FindControl("ContactNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployeesList.FindControl("ContactNameSortAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.ContactNameDesc:
					((ImageButton)EmployeesList.FindControl("ContactNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployeesList.FindControl("ContactNameSortDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.PhoneNumberAsc:
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.PhoneNumberDesc:
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.LocationAsc:
					((ImageButton)EmployeesList.FindControl("LocationSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployeesList.FindControl("LocationSortAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.LocationDesc:
					((ImageButton)EmployeesList.FindControl("LocationSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployeesList.FindControl("LocationSortDescButton")).Enabled = false;
					break;
			}
		}

		/// <summary>
		/// Handles the DataBound event of the ItemsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ItemsList_DataBound(object sender, ListViewItemEventArgs e)
		{
			if (ScriptManager.GetCurrent(Page).IsNotNull())
			{
				var phoneNumber = Regex.Replace(((EmployeeSearchResultView)e.Item.DataItem).PhoneNumber, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat);
				if (((EmployeeSearchResultView)e.Item.DataItem).Extension.IsNotNullOrEmpty())
					phoneNumber = phoneNumber + " (ext " + ((EmployeeSearchResultView)e.Item.DataItem).Extension + ")";

				((Literal)e.Item.FindControl("PhoneNumber")).Text = phoneNumber;

			}
		}
	}
}