﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web.Services;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Common.ServiceClients;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
    public partial class MatchesToOpenPositions : UserControlBase
    {
      /// <summary>
      /// Handles the Load event of the Page control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      protected void Page_Load(object sender, EventArgs e)
      {
          if (!IsPostBack)
          {
              LocaliseUI();
          }
      }

      /// <summary>
      /// Shows the specified details.
      /// </summary>
      /// <param name="details">The details.</param>
      public void Show(object[] details)
      {
          #region Job Seeker

          BindScore(PositionScoreImage, Convert.ToInt32(details[4]));
          JobSeekerDetailsLiteral.Text = CodeLocalise("CadidateName.Text", "<strong>" + details[3] + "</strong>");

          #endregion

          #region Open Positions

          //JobEmployerTitleLabel.Text = CodeLocalise("JobSeekerName", details[0] + ", " + details[2]);
          OpenPositionDetailsLiteral.Text = CodeLocalise("PositionName.Text", "<strong>" + details[1].ToString() + "</strong>");

          #endregion

          OpenPositionsModalPopup.Show();
      }

      /// <summary>
      /// Localises the UI.
      /// </summary>
      private void LocaliseUI()
      {
          ContactEmployerButton.Text = CodeLocalise("ContactEmployerButton.Text", "Contact employer");
          CancelButton.Text = CodeLocalise("CancelButton.Text", "Cancel");
      }

			public void OpenPositionsDropDown_SelectedIndexChanged(object sender, EventArgs e)
			{
					
			}

      /// <summary>
      /// Handles the Clicked event of the CancelButton control.
      /// </summary>
      /// <param name="sender">The source of the event.</param>
      /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
      protected void CancelButton_Clicked(object sender, EventArgs e)
      {
          OpenPositionsModalPopup.Hide();
      }

      /// <summary>
      /// Binds the score.
      /// </summary>
      /// <param name="scoreImage">The score image.</param>
      /// <param name="score">The score.</param>
      private void BindScore(Image scoreImage, int score)
      {
          var minimumStarScores = App.Settings.StarRatingMinimumScores;

          scoreImage.ToolTip = score.ToString();

          if (score >= minimumStarScores[5])
              scoreImage.ImageUrl = UrlBuilder.Content("~/Assets/Images/starsfive.gif");
          else if (score >= minimumStarScores[4])
              scoreImage.ImageUrl = UrlBuilder.Content("~/Assets/Images/starsfour.gif");
          else if (score >= minimumStarScores[3])
              scoreImage.ImageUrl = UrlBuilder.Content("~/Assets/Images/starsthree.gif");
          else if (score >= minimumStarScores[2])
              scoreImage.ImageUrl = UrlBuilder.Content("~/Assets/Images/starstwo.gif");
          else if (score >= minimumStarScores[1])
              scoreImage.ImageUrl = UrlBuilder.Content("~/Assets/Images/starsone.gif");
          else
              scoreImage.ImageUrl = UrlBuilder.Content("~/Assets/Images/starszero.gif");
      }

			#region Helper Class

			public class Details
			{
				public string JobTitle { get; set; }
				public string EmployerName { get; set; }
				public string CandidateName { get; set; }
				public int Score { get; set; }
			}

			#endregion
    }
}