﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerJobSeekerPlacements.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EmployerJobSeekerPlacements" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>

<asp:UpdatePanel runat="server" ID="JobSeekersPlacedAtThisEmployerUpdatePanel">
	<ContentTemplate>
			<uc:Pager ID="PlacementsPager" runat="server" PagedControlID="PlacementsList" PageSize="10" />				
			<asp:ListView ID="PlacementsList" runat="server" ItemPlaceholderID="PlacementsPlaceHolder" DataSourceID="PlacementsDataSource"
					          DataKeyNames="BuID, JobSeekerId"  OnLayoutCreated="PlacementsList_LayoutCreated" OnSorting="PlacementsList_Sorting">
			<LayoutTemplate>
				<table style="width:100%;" role="presentation">
					<tr>
						<td>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="PlacementNameHeader" /></td>
									<td style="height:8px;"><asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="PlacementNameAscButton" runat="server" CommandName="Sort" CommandArgument="placementname asc" /></td>
								</tr>
								<tr>
									<td style="height:8px;"><asp:ImageButton ImageUrl=<%# UrlBuilder.ActivityOnSortDescImage() %> ID="PlacementNameDescButton" runat="server" CommandName="Sort" CommandArgument="placementname desc" /></td>
								</tr>
							</table>
						</td>
						<td>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="JobTitleHeader" /></td>
									<td style="height:8px;"><asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="JobTitleAscButton" runat="server" CommandName="Sort" CommandArgument="placementjobtitle asc" /></td>
								</tr>
								<tr>
									<td style="height:8px;"><asp:ImageButton ImageUrl=<%# UrlBuilder.ActivityOnSortDescImage() %> ID="JobTitleDescButton" runat="server" CommandName="Sort" CommandArgument="placementjobtitle desc" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="6"><hr /></td>
					</tr>
					<asp:PlaceHolder ID="PlacementsPlaceHolder" runat="server" />
				</table>
			</LayoutTemplate>
			<ItemTemplate>
				<tr>
					<td style="width:15%; vertical-align: top;"><asp:LinkButton runat="server" ID="PlacementNameLinkButton" OnCommand="PlacementNameLinkButton_Command" CommandArgument="<%# ((BusinessUnitPlacementsViewDto)Container.DataItem).JobSeekerId %>" CommandName="JobSeekerProfile"  >
										                    <%# ((BusinessUnitPlacementsViewDto)Container.DataItem).LastName%>, <%# ((BusinessUnitPlacementsViewDto)Container.DataItem).FirstName%> </asp:LinkButton>
					</td>
					<td style="width:15%; vertical-align: top;"><%# ((BusinessUnitPlacementsViewDto)Container.DataItem).JobTitle%></td>
				</tr>
			</ItemTemplate>
			<EmptyDataTemplate><br/><%=HtmlLocalise("NoPlacementActivity", "No placements recorded") %></EmptyDataTemplate>
		</asp:ListView>
		<asp:ObjectDataSource ID="PlacementsDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.EmployerJobSeekerPlacements" EnablePaging="true" 
							            SelectMethod="GetEmployerPlacements" SelectCountMethod="GetEmployerPlacementCount" SortParameterName="orderBy" OnSelecting="PlacementsDataSource_Selecting">
		</asp:ObjectDataSource>
	</ContentTemplate>
</asp:UpdatePanel>
