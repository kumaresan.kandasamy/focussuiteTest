﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerPlacements.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EmployerPlacements" %>
<%@ Import Namespace="Focus.Web.WebAssist.Controls" %>

<%@ Register  Src="~/WebAssist/Controls/JobDevelopmentOfficeFilter.ascx"  TagName="JobDevelopmentOfficeFilter"  TagPrefix="uc" %>
<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>

<asp:UpdatePanel ID="EmployerPlacementsUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
	<ContentTemplate>
	<div class="FilterAndPagerDiv">
	<div class="FilterDiv">
		<table role="presentation">
		<tr runat="server" id="OfficeFilterRow">
       <td colspan="2" ><strong><%= HtmlLabel("Filter.Label", "Filter") %></strong></td>
			 <td><uc:JobDevelopmentOfficeFilter ID="OfficeFilter" runat="server" /></td>
			 <td><asp:Button ID="FilterButton" runat="server" class="button1" CausesValidation="False" OnClick="FilterButton_Clicked" /></td>
		</tr>
		</table>
		</div>
		<div class="PagerDiv">
				<uc:Pager ID="EmployerPlacementsListPager" runat="server" PagedControlID="EmployerPlacementsListView" PageSize="10" DisplayRecordCount="true" OnDataPaged="EmployerPlacementsListPager_DataPaged" />
		</div>
	</div>
	<div class="MainListDiv">
			<asp:ListView runat="server" ID="EmployerPlacementsListView" ItemPlaceholderID="EmployerPlacementsPlaceHolder" DataSourceID="EmployerPlacementsDataSource" OnLayoutCreated="EmployerPlacementsListView_LayoutCreated" 
									OnSorting="EmployerPlacementsListView_Sorting" OnItemDataBound="EmployerPlacementsListView_ItemDataBound" OnPreRender="EmployerPlacementsListView_OnPreRender">
			
				<LayoutTemplate>
					 <table style="width:100%;" class="table">
							<thead>
								<th>
									<table role="presentation">
										<tr>
                      <td rowspan="2">
                         <asp:Literal runat="server" ID="EmployerNameHeaderLiteral" />
                      </td>
                      <td>
                        <asp:ImageButton ID="EmployerNameSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" CommandArgument="employername asc"  />
                        <asp:ImageButton ID="EmployerNameSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" CommandArgument="employername desc" />
                      </td>
                    </tr>
									</table>
								</th>
								<th>
									<table role="presentation">
										<tr>
                      <td rowspan="2">
                         <asp:Literal runat="server" ID="EmployeeNameLiteral" />
                      </td>
                      <td>
                        <asp:ImageButton ID="NameSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" CommandArgument="name asc" />
                        <asp:ImageButton ID="NameSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" CommandArgument="name desc" />
                      </td>
                    </tr>
									</table>
								</th>
								<th>
									<table role="presentation">
											<tr>
                      <td rowspan="2">
                         <asp:Literal runat="server" ID="JobTitleHeaderLiteral" />
                      </td>
                      <td>
                        <asp:ImageButton ID="JobTitleSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" CommandArgument="jobtitle asc" />
                        <asp:ImageButton ID="JobTitleSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" CommandArgument="jobtitle desc" />
                      </td>
                    </tr>
									</table>
								</th>
								<th>
									<asp:Literal runat="server" ID="PostingDateHeaderLiteral" />
                </th>
							</thead>
							<asp:PlaceHolder ID="EmployerPlacementsPlaceHolder" runat="server" />
					 </table>
				</LayoutTemplate>
				<ItemTemplate>
					<asp:Repeater ID="EmployerRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
						<ItemTemplate>
							<tr id="EmployerJobRow" runat="server" ClientIDMode="AutoID" data-val='<%# Server.HtmlEncode(((EmployerPlacementsListModel)Container.DataItem).Placement.EmployerName) %>'>
								<td>
									<asp:LinkButton ID="ViewEmployer" runat="server" OnCommand="ViewEmployerLinkButton_Command" CommandArgument='<%# ((EmployerPlacementsListModel)Container.DataItem).Placement.BusinessUnitId %>' CommandName="ViewEmployer" ClientIDMode="AutoID"><%#((EmployerPlacementsListModel)Container.DataItem).Placement.EmployerName%></asp:LinkButton><br/>
								</td>
								<td>
									<asp:LinkButton ID="ViewEmployee" runat="server" OnCommand="ViewJobSeekerLinkButton_Command" CommandArgument='<%# ((EmployerPlacementsListModel)Container.DataItem).Placement.JobSeekerId %>' CommandName="ViewPosting" ClientIDMode="AutoID"><%#((EmployerPlacementsListModel)Container.DataItem).Placement.Name%></asp:LinkButton><br/>
									<asp:HyperLink runat="server" ID="ShowMoreLink" Visible="false" href="#" onClick="javascript:EmployerPlacements_showMore(this);return false;" CssClass="showMoreLink"/>
									<asp:HyperLink runat="server" ID="ShowLessLink" Visible="false" href="#" onClick="javascript:EmployerPlacements_showLess(this);return false;" />
								</td>
								<td>
									<%#((EmployerPlacementsListModel)Container.DataItem).Placement.JobTitle%>
								</td>
								<td>
									<%# (((EmployerPlacementsListModel)Container.DataItem).Placement.StartDate.HasValue ? ((EmployerPlacementsListModel)Container.DataItem).Placement.StartDate.Value.ToString("MMM dd, yyyy") : string.Format("{0}", "")) %>
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
					
				</ItemTemplate>
			</asp:ListView>
			<asp:ObjectDataSource ID="EmployerPlacementsDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.EmployerPlacements" EnablePaging="true" SelectMethod="GetEmployerPlacements" 
														SelectCountMethod="GetEmployerPlacementsCount" SortParameterName="orderBy" OnSelecting="EmployerPlacementsDataSource_Selecting" />
		</div>
    <script type="text/javascript">
      function EmployerPlacements_showMore(cell) {
        var parentRow = $(cell).closest("tr");
        var empName = parentRow.attr("data-val");
        $("tr.childListItem[data-val='" + empName + "']").show();
        $(cell).hide();
      }

      function EmployerPlacements_showLess(cell) {
        var parentRow = $(cell).closest("tr");
        var empName = parentRow.attr("data-val");
        $("tr.childListItem[data-val='" + empName + "']").hide();
        $("tr.parentListItem[data-val='" + empName + "']").find(".showMoreLink").show();
      }
    </script>
</ContentTemplate>
	<Triggers>
	</Triggers>
</asp:UpdatePanel>