﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
  public partial class JobIssueResolutionModal : UserControlBase
	{

    #region Properties

    private long JobId
    {
      get { return GetViewStateValue<long>("JobIssueResolutionModal:JobId"); }
      set { SetViewStateValue("JobIssueResolutionModal:JobId", value); }
    }

    private string JobName
    {
      get { return GetViewStateValue<string>("JobIssueResolutionModal:JobName"); }
      set { SetViewStateValue("JobIssueResolutionModal:JobName", value); }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack) Localise();
    }

    /// <summary>
    /// Shows the current control and binds the lists
    /// </summary>
    /// <param name="jobId">The job Id</param>
    /// <param name="jobName">Name of the job.</param>
    public void Show (long jobId, string jobName)
    {
      JobId = jobId;
      JobName = jobName;
      Reset();

      ModalTitle.Text = CodeLocalise("JobIssueResolutionModal.Title", "Resolve issues for {0}", jobName);

      Bind();

      if (IssueList.Items.Count == 0 && FollowUpList.Items.Count == 0)// User is trying to resolve issues for a job which has none
        Confirmation.Show(CodeLocalise("IssueResolution.NoneFound.Header", "Issue resolution"),
                          CodeLocalise("IssueResolution.NoneFound.Text", "No issues to resolve for {0}", jobName),
                          CodeLocalise("Global.Ok", "Ok"));
      else
        JobIssueResolutionModalPopup.Show();
    }

    #region private methods

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      var issues = ServiceClientLocator.JobClient(App).GetJobIssues(JobId);
      BindIssues(issues.JobIssues);
      BindPostHireFollowUps(issues.PendingPostHireFollowUps);
    }

    /// <summary>
    /// Binds the issues.
    /// </summary>
    /// <param name="issueRecord">The issue record.</param>
    private void BindIssues(JobIssuesDto issueRecord)
    {
      IssueList.Items.Clear();
      if (issueRecord.IsNull()) return;

      if (issueRecord.LowQualityMatches)
        IssueList.Items.Add(
          new ListItem(CodeLocalise(JobIssuesFilter.LowQualityMatches, "Low number of high quality matches"),
                       JobIssuesFilter.LowQualityMatches.ToString()));
      if (issueRecord.LowReferralActivity)
        IssueList.Items.Add(new ListItem(CodeLocalise(JobIssuesFilter.LowReferralActivity, "Low referral activity"),
                                         JobIssuesFilter.LowReferralActivity.ToString()));
      if (issueRecord.NotViewingReferrals)
        IssueList.Items.Add(
          new ListItem(CodeLocalise(JobIssuesFilter.NotViewingReferrals, "Not clicking on referred applicants"),
                       JobIssuesFilter.NotViewingReferrals.ToString()));
      if (issueRecord.SearchingButNotInviting)
        IssueList.Items.Add(new ListItem(CodeLocalise(JobIssuesFilter.SearchNotInviting, "Searching but not inviting"),
                                         JobIssuesFilter.SearchNotInviting.ToString()));
      if (issueRecord.ClosingDateRefreshed)
        IssueList.Items.Add(new ListItem(CodeLocalise(JobIssuesFilter.ClosingDateRefreshed, "Refreshing closing date"),
                                         JobIssuesFilter.ClosingDateRefreshed.ToString()));
      if (issueRecord.EarlyJobClosing)
        IssueList.Items.Add(new ListItem(CodeLocalise(JobIssuesFilter.EarlyJobClosing, "Pending closing date"),
                                         JobIssuesFilter.EarlyJobClosing.ToString()));
      if (issueRecord.NegativeSurveyResponse)
        IssueList.Items.Add(
          new ListItem(CodeLocalise(JobIssuesFilter.NegativeSurveyResponse, "Giving negative survey responses"),
                       JobIssuesFilter.NegativeSurveyResponse.ToString()));
      if (issueRecord.PositiveSurveyResponse)
        IssueList.Items.Add(
          new ListItem(CodeLocalise(JobIssuesFilter.PositiveSurveyResponse, "Giving positive survey responses"),
                       JobIssuesFilter.PositiveSurveyResponse.ToString()));
      if (issueRecord.FollowUpRequested)
        IssueList.Items.Add(new ListItem(CodeLocalise(JobIssuesFilter.FollowUpRequested, "Requiring follow-up"),
                                         JobIssuesFilter.FollowUpRequested.ToString()));
    }

    /// <summary>
    /// Binds the post hire follow up issues
    /// </summary>
    /// <param name="followUps">The follow ups.</param>
    private void BindPostHireFollowUps(List<ApplicationViewDto> followUps)
    {
      followUps.ForEach(
        x =>
        FollowUpList.Items.Add(
          new ListItem(
            CodeLocalise(JobIssuesFilter.PostHireFollowUp.ToString(), "Suggesting post-hire follow-up for {0} {1}",
                         x.CandidateFirstName, x.CandidateLastName), x.Id.ToString())));
    }

    private void Reset()
    {
      // Empty lists
      IssueList.Items.Clear();
      FollowUpList.Items.Clear();

      // Empty text box
      NoteTextBox.Text = string.Empty;
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      SaveButton.Text = CodeLocalise("JobIssueResolutionModal.SaveButton", "Save");
      CancelButton.Text = CodeLocalise("JobIssueResolutionModal.Cancel", "Cancel");
      NoteValidator.ErrorMessage = CodeLocalise("NoteValidationError.Text", "Please enter a note");
      CheckBoxListValidator.ErrorMessage = CodeLocalise("CheckBoxListValidationError.Text", "Pleae select at least one issue to resolve");
    }

    #endregion

    #region Events

    /// <summary>
    /// Handles the Clicked event of the SaveButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void SaveButton_Clicked(object sender, EventArgs e)
    {
      // Unbind issues resolved
      var issuesResolved =
        IssueList.Items.Cast<ListItem>().Where(x => x.Selected).Select(
          x => (JobIssuesFilter) Enum.Parse(typeof (JobIssuesFilter), x.Value)).ToList();
      
      // Unbind follow up issues resolved
      var followUpsResolved =
        FollowUpList.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value.ToLong().GetValueOrDefault()).
          ToList();

      // Save issues resolved
      ServiceClientLocator.JobClient(App).ResolveJobIssues(JobId, issuesResolved, followUpsResolved);

      // Save the note
      ServiceClientLocator.CoreClient(App).SaveNoteReminder(
        new NoteReminderDto
        {
          CreatedBy = App.User.UserId,
          NoteReminderType = NoteReminderTypes.Note,
          Text = NoteTextBox.Text,
          EntityId = JobId,
          EntityType = EntityTypes.Job,
          NoteType = NoteTypes.ResolvedIssues
        }, null);

      // Hde the modal
      JobIssueResolutionModalPopup.Hide();

      // Show the confirmation window
      Confirmation.Show(CodeLocalise("IssueResolution.IssuesResolved.Header", "Issue resolution"),
                          CodeLocalise("IssueResolution.IssuesResolved.Text", "Issues resolved for job {0}", JobName),
                          CodeLocalise("Global.Ok", "Ok"));

      // Trigger on completed event to allow control parent to refresh if required
      OnCompleted(new EventArgs());
    }

    /// <summary>
    /// Handles the Clicked event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void CancelButton_Clicked(object sender, EventArgs e)
    {
      JobIssueResolutionModalPopup.Hide();
    }

    #endregion

    #region Event handlers

    /// <summary>
    /// Raises the <see cref="OnCompleted"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected virtual void OnCompleted(EventArgs eventArgs)
    {
      if (Completed != null)
        Completed(this, eventArgs);
    }

    public delegate void CompletedHandler(object sender, EventArgs eventArgs);
    public event CompletedHandler Completed;

    #endregion
  }
}