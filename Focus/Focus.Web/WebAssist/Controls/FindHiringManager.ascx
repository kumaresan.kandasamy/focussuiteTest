﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FindHiringManager.ascx.cs"
    Inherits="Focus.Web.WebAssist.Controls.FindHiringManager" %>
<%@ Import Namespace="Focus.Core.Views" %>
<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<asp:UpdatePanel ID="FindHiringManagerUpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table style="width: 100%;" role="presentation">
            <tr>
                <td style="width: 40%; vertical-align: top">
                    <table role="presentation">
                        <tr>
                            <td style="width: 150px;">
                                <%= HtmlLabel(EmployerNameTextBox, "EmployerName.Label", "#BUSINESS# name")%>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="EmployerNameTextBox" Width="200" MaxLength="25" />
                            </td>
                            <td style="width: 10px;">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel(FirstNameTextBox,"HiringManagerFirstName.Label", "Hiring mgr. first name")%>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="FirstNameTextBox" Width="200" MaxLength="25" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel(LastNameTextBox,"HiringManagerLastName.Label", "Hiring mgr. last name")%>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="LastNameTextBox" Width="200" MaxLength="25" ClientIDMode="Static" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLabel(EmailAddressTextBox,"HiringManagerEmailAddress.Label", "Hiring mgr. email")%>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="EmailAddressTextBox" Width="200" MaxLength="50" ClientIDMode="Static" />
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td style="vertical-align: top;">
                                <asp:Button ID="FindButton" runat="server" class="button3" OnClick="FindButton_Click" Text="Find"/>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 60%; vertical-align: top;">
                    <table id="EmployeeSearchHeaderTable" runat="server" visible="false" style="width: 100%;"
                        role="presentation">
                        <tr>
                            <td style="width: 50%;">
                                <strong>
                                    <%= HtmlLocalise("SearchResults.Title", "Search results")%></strong>&nbsp;&nbsp;<asp:Literal
                                        ID="ResultCount" runat="server" />
                            </td>
                            <td style="text-align: right; white-space: nowrap;">
                                <uc:Pager ID="SearchResultListPager" runat="server" PagedControlId="EmployeesList"
                                    PageSize="10" />
                            </td>
                        </tr>
                    </table>
                    <asp:ListView ID="EmployeesList" runat="server" OnItemDataBound="ItemsList_DataBound"
                        ItemPlaceholderID="SearchResultPlaceHolder" DataSourceID="SearchResultDataSource"
                        DataKeyNames="Id" Visible="false" OnLayoutCreated="EmployeesList_LayoutCreated"
                        OnSorting="EmployeesList_Sorting">
                        <LayoutTemplate>
                            <table style="width: 100%;" class="table">
                                <tr>
                                    <th />
                                    <th />
                                    <th>
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="EmployerNameHeader" />
                                                </td>
                                                <td style="height: 8px;white-space: nowrap">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmployerNameSortAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="businessunitname asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmployerNameSortDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="businessunitname desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                                </td>
                                            </tr>
                                        </table>
                                    </th>
                                    <th>
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="ContactNameHeader" />
                                                </td>
                                                <td style="height: 8px;white-space: nowrap">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="ContactNameSortAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="contactname asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="ContactNameSortDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="contactname desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                                </td>
                                            </tr>
                                        </table>
                                    </th>
                                    <th>
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="PhoneNumberHeader" />
                                                </td>
                                                <td style="height: 8px;white-space: nowrap">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="PhoneNumberSortAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="phonenumber asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="PhoneNumberSortDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="phonenumber desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                                </td>
                                            </tr>
                                        </table>
                                    </th>
                                    <th>
                                        <table role="presentation">
                                            <tr>
                                                <td rowspan="2" style="height: 16px;">
                                                    <asp:Literal runat="server" ID="LocationHeader" />
                                                </td>
                                                <td style="height: 8px;white-space: nowrap">
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LocationSortAscButton"
                                                        runat="server" CommandName="Sort" CommandArgument="location asc" CssClass="AscButton" AlternateText="Sort Ascending" />
                                                    <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="LocationSortDescButton"
                                                        runat="server" CommandName="Sort" CommandArgument="location desc" CssClass="DescButton" AlternateText="Sort Descending" />
                                                </td>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <hr />
                                    </td>
                                </tr>
                                <asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td style="width: 3%;">
                                    <input type="radio" name="SelectorRadio" value="<%# ((EmployeeSearchResultView)Container.DataItem).Id %>" />
                                </td>
                                <td style="width: 6%; text-align: center;">
                                    <%# GetEmployeeStatusImage(((EmployeeSearchResultView)Container.DataItem).EmployeeApprovalStatus,((EmployeeSearchResultView)Container.DataItem).AccountBlocked, ((EmployeeSearchResultView)Container.DataItem).AccountBlockedReason) %>
                                </td>
                                <td style="width: 26%;">
                                    <%# ((EmployeeSearchResultView)Container.DataItem).BusinessUnitName %>
                                </td>
                                <td style="width: 20%;">
                                    <%# ((EmployeeSearchResultView)Container.DataItem).LastName %>,
                                    <%# ((EmployeeSearchResultView)Container.DataItem).FirstName %>
                                </td>
                                <td style="width: 21%;">
                                    <asp:Literal ID="PhoneNumber" runat="server" />
                                </td>
                                <td style="width: 26%;">
                                    <%# ((EmployeeSearchResultView)Container.DataItem).Town %>,
                                    <%# ((EmployeeSearchResultView)Container.DataItem).State %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <table style="width: 100%;" role="presentation">
                                <tr>
                                    <td>
                                        <%= HtmlLocalise("NoMatches.Text", "We could not find a matching #BUSINESS#:LOWER. You will need to create a new #BUSINESS#:LOWER account before you can create a #EMPLOYMENTTYPE#.")%>
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:ListView>
                    <asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.FindHiringManager"
                        EnablePaging="true" SelectMethod="GetEmployees" SelectCountMethod="GetEmployeesCount"
                        SortParameterName="orderBy" OnSelecting="SearchResultDataSource_Selecting"></asp:ObjectDataSource>
                    <asp:CustomValidator ID="EmployeesListValidator" runat="server" ClientValidationFunction="validateHiringManagerSelected"
                        ValidationGroup="CreateJob" ValidateEmptyText="True" CssClass="error" />
                    <br />
                    <p style="text-align: right">
                        <asp:Button ID="CreateEmployerButton" runat="server" class="button3" Visible="false"
                            ClientIDMode="Static" OnClick="CreateEmployerButton_Click" CausesValidation="false" Text="Create #BUSINESS#:LOWER"/>&nbsp;&nbsp;
                        <asp:Button ID="CreateJobButton" runat="server" class="button3" Visible="false" ClientIDMode="Static"
                            OnClick="CreateJobButton_Click" ValidationGroup="CreateJob" Text="Create job"/>
                    </p>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">

    function validateHiringManagerSelected(oSrc, args) {
        args.IsValid = checkHiringManagerSelected();
    }

    function checkHiringManagerSelected() {
        return $("input[name='SelectorRadio']:checked").length > 0;
    }

</script>
