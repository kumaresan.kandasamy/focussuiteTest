﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CareerMessages.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.CareerMessages" %>

<%@ Register src="~/WebAssist/Controls/EmailTemplateEditor.ascx" tagname="EmailTemplateEditor" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<table width="100%" role="presentation">
  <tr>
    <td style="width:150px"></td>
    <td></td>
  </tr>
	<tr>
    <td colspan="2">
			<table style="width:100%" role="presentation">
				<tr>
					<td><strong><%= HtmlLabel("CareerMessage.Title", "#FOCUSCAREER# default messages")%></strong></td>
					<td><asp:Button ID="SaveChangesButton" runat="server" class="button1 right" onclick="SaveChangesButton_Click" /></td>	
				</tr>
			</table>
		</td>
	</tr>
  <tr>
    <td colspan="2"><br /></td>
  </tr>
  <tr>
		<td><%= HtmlLabel("EmailTemplateName.Label", "Select message")%></td>
    <td style="vertical-align: top;">
      <asp:DropDownList runat="server" ID="EmailTemplateNameDropDown" AutoPostBack="True" OnSelectedIndexChanged="EmailTemplateNameDropDown_SelectedIndexChanged"  />
    </td>
  </tr>
  <asp:PlaceHolder runat="server" ID="JobSeekerInactivityPanel" Visible="False">
    <tr>
      <td></td>
      <td>
        <%=HtmlLocalise("SendJobSeekerWarning.Label", "Send job seeker inactivity warnings")%>&nbsp;
        <asp:RadioButton runat="server" ID="JobSeekerInactivityEmailYes" GroupName="JobSeekerInactivityEmail"/><%=HtmlLocalise("Global.Yes", "Yes") %>&nbsp;
        <asp:RadioButton runat="server" ID="JobSeekerInactivityEmailNo" GroupName="JobSeekerInactivityEmail"/><%=HtmlLocalise("Global.No", "No") %>
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <%=HtmlLocalise("JobSeekerEmailLimit1.Label", "Send email after")%>&nbsp;
        <asp:TextBox runat="server" ID="JobSeekerEmailLimitTextBox" MaxLength="3" Width="24" />&nbsp;
        <%=HtmlLocalise("JobSeekerEmailLimit2.Label", "days of inactivity")%>
        <br />
        <asp:RequiredFieldValidator ID="JobSeekerEmailLimitValidator" runat="server" ControlToValidate="JobSeekerEmailLimitTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        <asp:RegularExpressionValidator ID="JobSeekerEmailLimitRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="JobSeekerEmailLimitTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
      </td>
    </tr>
    <tr>
      <td></td>
      <td>
        <%=HtmlLocalise("JobSeekerEmailGrace1.Label", "Job seekers must respond within")%>&nbsp;
        <asp:TextBox runat="server" ID="JobSeekerEmailGraceTextBox" MaxLength="3" Width="24" />&nbsp;
        <%=HtmlLocalise("JobSeekerEmailGrace2.Label", "days")%>
        <br />
        <asp:RequiredFieldValidator ID="JobSeekerEmailGraceValidator" runat="server" ControlToValidate="JobSeekerEmailGraceTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
        <asp:RegularExpressionValidator ID="JobSeekerEmailGraceRegex" runat="server" ValidationExpression="[0-9]*[1-9]+[0-9]*" ControlToValidate="JobSeekerEmailGraceTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
      </td>
    </tr>
  </asp:PlaceHolder>
  <tr>
    <td colspan="2"><br /></td>
  </tr>
</table>

<uc:EmailTemplateEditor ID="EmailTemplateEditor" runat="server" />
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />

<script type="text/javascript">
  $(document).ready(function () {
    CareerMessages_JobSeekerInactivityFields();

    $("#<%=JobSeekerInactivityEmailYes.ClientID%>").click(CareerMessages_JobSeekerInactivityFields);
    $("#<%=JobSeekerInactivityEmailNo.ClientID%>").click(CareerMessages_JobSeekerInactivityFields);
  });

  function CareerMessages_JobSeekerInactivityFields() {
    var sendEmail = $("#<%=JobSeekerInactivityEmailYes.ClientID%>").is(":checked"); ;
    CareerMessages_DisableCheckBox("<%=JobSeekerEmailLimitTextBox.ClientID%>", "<%=JobSeekerEmailLimitValidator.ClientID%>", "<%=JobSeekerEmailLimitRegex.ClientID%>", sendEmail);
    CareerMessages_DisableCheckBox("<%=JobSeekerEmailGraceTextBox.ClientID%>", "<%=JobSeekerEmailGraceValidator.ClientID%>", "<%=JobSeekerEmailGraceRegex.ClientID%>", sendEmail);
  }
  
  function CareerMessages_DisableCheckBox(checkBoxId, requiredValidatorId, regexValidatorId, enabled) {
    var checkBox = $("#" + checkBoxId);

    if (checkBox.length > 0) {
      checkBox.prop("disabled", !enabled);

      ValidatorEnable($("#" + requiredValidatorId)[0], enabled);
      ValidatorEnable($("#" + regexValidatorId)[0], enabled);

      var parentRow = checkBox.closest("tr");
      if (enabled) {
        parentRow.removeClass("disabledRow");
      } else {
        parentRow.addClass("disabledRow");
      }
    }
  }
</script>