﻿

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SavedAnnouncements.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.SavedAnnouncements" %>

<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>

<p>
	<table>
		<tr>
			<td><%=HtmlLabel("AnnouncementAudience.Label", "Show announcements for") %></td>
			<td>
				<asp:RadioButtonList ID="AnnouncementAudienceRadioButtonList" RepeatDirection="Horizontal" runat="server" ClientIDMode="Static" OnSelectedIndexChanged="AnnouncementAudienceRadioButtonList_SelectedIndexChanged" 
																AutoPostBack="true" />
			</td>
		</tr>
	</table>
</p>
<p>
	<asp:DropDownList ID="SavedAnnouncementsDropDown" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="SavedAnnouncementsDropDown_SelectedIndexChanged" Width="210" />
	<asp:RequiredFieldValidator ID="SavedAnnouncementRequired" runat="server" ControlToValidate="SavedAnnouncementsDropDown" ValidationGroup="SavedAnnouncements" CssClass="error" />
	<asp:RequiredFieldValidator ID="SavedAnnouncementRequiredDelete" runat="server" ControlToValidate="SavedAnnouncementsDropDown" ValidationGroup="SavedAnnouncementsDelete" CssClass="error" />
</p>
<p>
	<asp:TextBox ID="AnnouncementTextbox" CssClass="mcetiny" runat="server" Width="500" MaxLength="1000" Rows="5"></asp:TextBox>
	<asp:RequiredFieldValidator ID="AnnouncementRequiredSave" runat="server" ControlToValidate="AnnouncementTextbox" ValidationGroup="SavedAnnouncements" CssClass="error" />
</p>
			
<asp:Repeater ID="CultureSpecificAnnouncementsRepeater" runat="server" OnItemDataBound="CultureSpecificAnnouncementsRepeater_ItemDataBound">
	<HeaderTemplate><p><%= HtmlLabel("CultureSpecificAnnouncementsr.Instructions", "The above text will be shown to any #BUSINESS#:LOWER who does not have one of the following cultures. Please leave the text box empty if you would like #BUSINESSES#:LOWER that use that culture to see the default text.")%></p></HeaderTemplate>
	<ItemTemplate>
		<p>				
			<asp:Label ID="CultureNameLabel" runat="server" /><br/>
			<span class="inFieldLabel"><asp:Label ID="CultureSpecificAnnouncementInlineLabel" runat="server" Width="500" /></span>
			<asp:TextBox ID="CultureSpecificAnnouncementTextbox" CssClass="mcetiny" runat="server" Width="500" MaxLength="1000"></asp:TextBox>
			<asp:HiddenField ID="LocalisationIdHidden" runat="server" />
		</p>
	</ItemTemplate>
</asp:Repeater>
							
<p>
	<asp:Button ID="SaveAnnouncementButton" runat="server" CssClass="button2" OnClick="SaveAnnouncementButton_Clicked"  ValidationGroup="SavedAnnouncements" OnClientClick="tinyMCE.triggerSave(false,true);" />
	<asp:button ID="DeleteButton" runat="server" CssClass="button3" OnClick="DeleteButton_Clicked" ValidationGroup="SavedAnnouncementsDelete" />
</p>

<%-- Confirmation Modal --%>
<uc:ConfirmationModal ID="Confirmation" runat="server" />
<uc:ConfirmationModal ID="DeleteConfirmationModal" runat="server" OnOkCommand="DeleteConfirmationModal_OkCommand" />