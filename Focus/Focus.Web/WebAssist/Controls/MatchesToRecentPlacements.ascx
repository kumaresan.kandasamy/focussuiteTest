﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MatchesToRecentPlacements.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.MatchesToRecentPlacements" %>
<%@ Import Namespace="Focus.Core.Views" %>

<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/CompareResumesModal.ascx" tagname="CompareResumesModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/MatchesToOpenPositionsModal.ascx" tagname="MatchesToOpenPositionsModal" tagprefix="uc" %>
<%@ Register  Src="~/Code/Controls/User/EmailEmployeeModal.ascx"  TagName="EmailEmployeeModal"  TagPrefix="uc" %>
<%@ Register  Src="~/WebAssist/Controls/JobDevelopmentOfficeFilter.ascx"  TagName="JobDevelopmentOfficeFilter"  TagPrefix="uc" %>


<asp:UpdatePanel runat="server" ID="RecentPlacementMatchesUpdatePanel" UpdateMode="Always" >
	<ContentTemplate>
		<table style="width:100%;" role="presentation">
			<tr>
				<td style="width:40px;" ><strong><%= HtmlLabel("Filter.Label", "Filter") %></strong></td>
				<td>
					<span class="inFieldLabel"><%= HtmlLabel(EmployerNameTextBox,"EmployerName.Label", "#BUSINESS#:LOWER name")%></span>
					<asp:TextBox runat="server" ID="EmployerNameTextBox" Width="200" MaxLength="25" ClientIDMode="Static" />
					<focus:LocalisedLabel ID="OfficeFilterLabel" runat="server" LocalisationKey="OfficeFilterLabel.Text" DefaultText="in" RenderOuterSpan="False" />
					<uc:JobDevelopmentOfficeFilter ID="OfficeFilter" runat="server" />
					&nbsp;<asp:Button ID="FilterButton" runat="server" class="button1" onclick="FilterButton_Clicked" Text="Go"/>			
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right">
					<table id="RecentMatchesHeaderTable" runat="server" Visible="false" style="width:100%;border-collapse:collapse;" role="presentation">
						<tr>
							<td style="width:100%;" ><strong><%= HtmlLocalise("SearchResults.Title", "Search results")%></strong>&nbsp;&nbsp;<asp:literal ID="ResultCount" runat="server" />&nbsp;</td>
							<td style="text-align:right; white-space: nowrap;"><uc:Pager ID="SearchResultListPager" runat="server" PagedControlID="RecentMatchesList" PageSize="10" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<asp:ListView ID="RecentMatchesList" runat="server" OnItemDataBound="ItemsList_DataBound" ItemPlaceholderID="SearchResultPlaceHolder" DataSourceID="RecentMatchesDataSource"
						DataKeyNames="Id,BusinessUnitName,BusinessUnitId,JobId,JobTitle,PersonId,CandidateName,Candidates,EmployerId"  OnLayoutCreated="RecentMatchesList_LayoutCreated" OnSorting="RecentMatchesList_Sorting">
			<LayoutTemplate>
				<table style="width:100%;" class="table">
					<tr>
						<th>
							<table role="presentation">
								<tr>
									<td rowspan="2"><asp:Literal runat="server" ID="EmployerNameHeader" /></td>
									<td>
									  <asp:ImageButton ID="EmployerNameAscButton" runat="server" CommandName="Sort" CommandArgument="businessunitname asc" CssClass="AscButton" />
                   <asp:ImageButton ID="EmployerNameDescButton" runat="server" CommandName="Sort" CommandArgument="businessunitname desc" CssClass="DescButton" />
									</td>
								</tr>
							</table>
						</th>
						<th>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="RecentPlacementNameHeader" /></td>
									<td style="height:8px;">
									  <asp:ImageButton  ID="RecentPlacementNameAscButton" runat="server" CommandName="Sort" CommandArgument="recentplacementname asc" CssClass="AscButton" />
                   <asp:ImageButton  ID="RecentPlacementNameDescButton" runat="server" CommandName="Sort" CommandArgument="recentplacementname desc" CssClass="DescButton" />
									</td>
								</tr>
							</table>
						</th>
						<th>
							<table role="presentation">
								<tr>
									<td style="height:16px;"><asp:Literal runat="server" ID="MatchHeader" /></td>
                                    <td style="height:16px; padding-left:5px"><asp:literal runat="server" ID="MatchToolTip" /></td>
								</tr>
							</table>
						</th>
						<th>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="JobSeekerHeader" /></td>
								</tr>
							</table>
						</th>
						<th>
							<table role="presentation">
								<tr>
									<td rowspan="2" style="height:16px;"><asp:Literal runat="server" ID="ActionsHeader" /></td>
								</tr>
							</table>
						</th>
					</tr>
					<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
				</table>
			</LayoutTemplate>
			<ItemTemplate>
				<tr>
					<td style="width:20%;"><%# ((EmployerRecentlyPlacedView)Container.DataItem).BusinessUnitName%></td>
					<td style="width:20%;"><%# ((EmployerRecentlyPlacedView)Container.DataItem).CandidateName%><br/><%# ((EmployerRecentlyPlacedView)Container.DataItem).JobTitle%></td>
					<td style="width:20%;"><asp:image ID="ReferralScoreImage" runat="server" AlternateText="Referral Score Image" /></td>							
					<td style="width:20%;"><asp:Literal runat="server" ID="FirstMatch"></asp:Literal><br/><asp:Literal runat="server" ID="ExtraMatchesLink" ></asp:Literal></td>
					<td style="width:20%; padding: 3px 0px;">
						<asp:LinkButton ID="CompareResumesLinkButton" runat="server" OnCommand="CompareResumesLinkButton_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="CompareResumes"></asp:LinkButton><br/>
						<asp:LinkButton ID="OpenPositionsLinkButton" runat="server"  OnCommand="CompareResumesLinkButton_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="ViewMatchesToOpenPositions"></asp:LinkButton><span ID="OpenPositionsBr" runat="server"><br /></span>
						<asp:LinkButton ID="ContactEmployerLinkButton" runat="server" OnCommand="ContactEmployerLinkButton_Command" CommandArgument="<%# Container.DisplayIndex %>" CommandName="ContactEmployer" ></asp:LinkButton>
					</td>
				</tr>
				<asp:Repeater runat="server" ID="AdditionalMatchesRepeater" OnItemDataBound="AdditionalMatchesRepeater_DataBound" >
					<ItemTemplate>
						<tr visible="false" class="Repeater Repeater<%# ((EmployerRecentlyPlacedView.Candidate)Container.DataItem).RecentlyPlacedId %>" >
							<td style="width:20%;"></td>
							<td style="width:20%;"></td>
							<td style="width:20%;"><asp:image ID="AdditionalScoreImage" runat="server" AlternateText="Additional Score Image" /></td>							
							<td style="width:20%;"><asp:Literal runat="server" ID="AdditionalMatch"></asp:Literal><br/><asp:Literal runat="server" ID="LessMatchesLink"></asp:Literal></td>
							<td style="width:20%; padding: 3px 0px;">
								<asp:LinkButton ID="CompareResumesOthersLinkButton" runat="server" OnCommand="CompareResumesLinkButton_Command" CommandArgument='<%# ((ListViewItem)Container.Parent.Parent).DisplayIndex + ";" + Container.ItemIndex %>' CommandName="CompareResumes"></asp:LinkButton><br/>
								<asp:LinkButton ID="OpenPositionsOthersLinkButton" runat="server" OnCommand="CompareResumesLinkButton_Command" CommandArgument='<%# ((ListViewItem)Container.Parent.Parent).DisplayIndex + ";" + Container.ItemIndex %>' CommandName="ViewMatchesToOpenPositions"></asp:LinkButton><span ID="OpenPositionsOthersBr" runat="server"><br /></span>
								<asp:LinkButton ID="ContactEmployerOthersLinkButton" runat="server" OnCommand="ContactEmployerLinkButton_Command" CommandArgument='<%# ((ListViewItem)Container.Parent.Parent).DisplayIndex + ";" + Container.ItemIndex %>' CommandName="ContactEmployerOther" ></asp:LinkButton>
							</td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</ItemTemplate>
		</asp:ListView>

  	<asp:ObjectDataSource ID="RecentMatchesDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.MatchesToRecentPlacements" EnablePaging="true" 
													SelectMethod="GetMatches" SelectCountMethod="GetMatchesCount" SortParameterName="orderBy" OnSelecting="RecentMatchesDataSource_Selecting">
    </asp:ObjectDataSource>
					
		<uc:CompareResumesModal ID="CompareResumesModalUc" runat="server" OnCompleted="Modal_Completed"/>
		
	</ContentTemplate>
		<Triggers>
      <asp:AsyncPostBackTrigger ControlID="MatchesToOpenPositionsModalUc"  /> 
    </Triggers>
	</asp:UpdatePanel>	
	<uc:MatchesToOpenPositionsModal ID="MatchesToOpenPositionsModalUc" runat="server" OnCompleted="Modal_Completed"/>
	<uc:EmailEmployeeModal ID="EmailEmployee" runat="server" />

		<script type="text/javascript" >

			$(document).ready(function () 
			{
				$('.Repeater').hide();
			});
			
			var prm = Sys.WebForms.PageRequestManager.getInstance();

			prm.add_endRequest(function () 
			{
				$('.Repeater').hide();
				$(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();
			});

			function ShowRepeater(id) 
			{
				var repeater = '.Repeater' + id;
				$(repeater).show();

				var showLink = '.ShowLink' + id;
				$(showLink).hide();
			}

			function HideRepeater(id) 
			{
				var repeater = '.Repeater' + id;
				$(repeater).hide();

				var showLink = '.ShowLink' + id;
				$(showLink).show();
			}

		</script>
	


	
	
	
	
	