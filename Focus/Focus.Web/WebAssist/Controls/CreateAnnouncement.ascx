﻿
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateAnnouncement.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.CreateAnnouncement" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<div style="margin-bottom:5px;">

    <table role="presentation">
        <tr>
            <td>
                <%=HtmlLabel("AnnouncementAudience.Label", "Create announcements for") %>
            </td>
            <td>
                <asp:RadioButtonList ID="AnnouncementAudienceRadioButtonList" RepeatDirection="Horizontal"
                    runat="server" ClientIDMode="Static" OnSelectedIndexChanged="AnnouncementAudienceRadioButtonList_SelectedIndexChanged"
                    AutoPostBack="true" role="presentation" />
            </td>
        </tr>
    </table>
		<focus:LocalisedLabel runat="server" ID="SavedAnnouncementsDropDownLabel" AssociatedControlID="SavedAnnouncementsDropDown" LocalisationKey="SavedAnnouncementsDropDown.Label" DefaultText="Saved announcements" CssClass="sr-only"/>
    <asp:DropDownList ID="SavedAnnouncementsDropDown" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="SavedAnnouncementsDropDown_SelectedIndexChanged" />
</div>
<p>
    <%= HtmlInFieldLabel("SavedAnnouncementNameTextBox", "SavedAnnouncementName.InlineLabel", "name your saved announcement", 500)%>
    <asp:TextBox ID="SavedAnnouncementNameTextBox" runat="server" Width="500" MaxLength="100" ClientIDMode="Static" />
    <asp:RequiredFieldValidator ID="SavedAnnouncementNameRequired" runat="server" ControlToValidate="SavedAnnouncementNameTextBox"
        ValidationGroup="SaveAnnouncement" CssClass="error" />
</p>
<p>
    <asp:TextBox ID="AnnouncementTextbox" runat="server" Width="500" MaxLength="1000" CssClass="mcetiny" Title="Announcement Text"></asp:TextBox>
    
    <asp:RequiredFieldValidator ID="AnnouncementRequired" runat="server" ControlToValidate="AnnouncementTextbox" ValidationGroup="CreateAnnouncement" CssClass="error" />
    <asp:RequiredFieldValidator ID="AnnouncementRequiredSave" runat="server" ControlToValidate="AnnouncementTextbox" ValidationGroup="SaveAnnouncement" CssClass="error" />
</p>
<p>
    <asp:Button ID="SendAnnouncementButton" runat="server" CssClass="button2" OnClick="SendAnnouncementButton_Clicked"
        ValidationGroup="CreateAnnouncement" OnClientClick="tinyMCE.triggerSave(false,true);" />
    <asp:Button ID="SaveAnnouncementButton" runat="server" CssClass="button3" OnClick="SaveAnnouncementButton_Clicked"
        ValidationGroup="SaveAnnouncement" OnClientClick="tinyMCE.triggerSave(false,true);" />
</p>
<%-- Confirmation Modal --%>
<uc:ConfirmationModal ID="Confirmation" runat="server" />
