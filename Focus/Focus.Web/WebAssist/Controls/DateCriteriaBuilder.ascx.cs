﻿using System;
using Focus.Core;
using Focus.Core.Criteria.Common;
using Framework.Core;
using Focus.Common.Extensions;

namespace Focus.Web.WebAssist.Controls
{
	public partial class DateCriteriaBuilder : CriteriaBuilderControlBase
	{
		private DateCriteria _criteria
		{
			get { return GetViewStateValue<DateCriteria>("DateCriteriaBuilder:Criteria"); }
			set { SetViewStateValue("DateCriteriaBuilder:Criteria", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				BindDateWithinDropDown();
				_criteria = new DateCriteria();
			}
		}

		#region Public methods

		/// <summary>
		/// Unbinds this instance.
		/// </summary>
		/// <returns></returns>
		public DateCriteria Unbind()
		{
			return _criteria;
		}

		/// <summary>
		/// Clears the criteria.
		/// </summary>
		public void ClearCriteria()
		{
			_criteria.DateWithinRange = null;
			_criteria.FromDate = null;
			_criteria.ToDate = null;
		}

		#endregion

		#region Add button clicks

		/// <summary>
		/// Handles the Clicked event of the AddDateWithinFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddDateWithinFilterButton_Clicked(object sender, EventArgs e)
		{
			if (DateWithinDropDown.SelectedIndex == 0)
				return;

			_criteria.DateWithinRange = DateWithinDropDown.SelectedValueToEnum<DateWithinRanges>();
			_criteria.FromDate = null;
			_criteria.ToDate = null;

			ClearControls();

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
			                                           	{
			                                           		DisplayText = CodeLocalise("DateWithin.Text", "within the {0}", CodeLocalise(_criteria.DateWithinRange)),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.Date
			                                           	}));
			                	
		}

		/// <summary>
		/// Handles the Clicked event of the AddDateFromFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddDateFromFilterButton_Clicked(object sender, EventArgs e)
		{
			if (DateFromTextBox.TextTrimmed().IsNullOrEmpty())
				return;

			_criteria.DateWithinRange = null;
			_criteria.FromDate = DateFromTextBox.TextTrimmed().ToDate();
			_criteria.ToDate = null;

			ClearControls();

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("DateFrom.Text", "from {0}", _criteria.FromDate.GetValueOrDefault().ToShortDateString()),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.Date
																									}));
		}

		/// <summary>
		/// Handles the Clicked event of the AddDateRangeFilterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddDateRangeFilterButton_Clicked(object sender, EventArgs e)
		{
			if (DateRangeFromTextBox.TextTrimmed().IsNullOrEmpty() || DateRangeToTextBox.TextTrimmed().IsNullOrEmpty())
				return;

			_criteria.DateWithinRange = null;
			_criteria.FromDate = DateRangeFromTextBox.TextTrimmed().ToDate();
			_criteria.ToDate = DateRangeToTextBox.TextTrimmed().ToDate();

			ClearControls();

			OnCriteriaAdded(new CriteriaAddedEventArgs(new CriteriaElement
																									{
																										DisplayText = CodeLocalise("DateRange.Text", "between {0} and {1} ", _criteria.FromDate.GetValueOrDefault().ToShortDateString(), _criteria.ToDate.GetValueOrDefault().ToShortDateString()),
																										OverwriteExistingValue = true,
																										Type = CriteriaElementType.Date
																									}));
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Binds the date within drop down.
		/// </summary>
		private void BindDateWithinDropDown()
		{
			DateWithinDropDown.Items.Clear();

			DateWithinDropDown.Items.AddLocalisedTopDefault("DateWithinDropDown.TopDefault", "- select range -");
			DateWithinDropDown.Items.AddEnum(DateWithinRanges.LastThreeDays, "last 3 days");
			DateWithinDropDown.Items.AddEnum(DateWithinRanges.LastSevenDays, "last 7 days");
			DateWithinDropDown.Items.AddEnum(DateWithinRanges.LastThirtyDays, "last 30 days");
			DateWithinDropDown.Items.AddEnum(DateWithinRanges.LastSixtyDays, "last 60 days");
			DateWithinDropDown.Items.AddEnum(DateWithinRanges.LastNinetyDays, "last 90 days");
		}

		/// <summary>
		/// Clears the controls.
		/// </summary>
		private void ClearControls()
		{
			DateWithinDropDown.SelectedIndex = 0;
			DateFromTextBox.Text = DateRangeFromTextBox.Text = DateRangeToTextBox.Text = "";
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			AddDateWithinFilterButton.Text = AddDateFromFilterButton.Text = AddDateRangeFilterButton.Text = CodeLocalise("Global.Add.Text", "Add");
		}

		#endregion

	}
}