﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SavedSearch.ascx.cs"
	Inherits="Focus.Web.WebAssist.Controls.SavedSearch" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
	TagPrefix="uc" %>
<tr>
	<td colspan="3">
		<table role="presentation" style="width:100%;" class="savedSearchTop">
			<tr>
				<td>
					<h1>
						<%= HtmlLocalise("Heading1.Label", "Saved Search: ")%>
						<asp:Label ID="lblSearchName" runat="server" Text=""></asp:Label>
					</h1>
					<asp:HiddenField ID="hdnResumeAssociation" runat="server" ClientIDMode="Static" />
				</td>
				<td class="instructionalText">
					<asp:Label ID="lblLastSaved" runat="server" />
				</td>
				<td>
					<asp:LinkButton ID="lnkDeleteSearch" runat="server" OnClick="lnkDeleteSaveSearch_Click"><%= HtmlLocalise("DeleteThisSearch.Label", "Delete this search")%></asp:LinkButton>
					<asp:LinkButton ID="lnkUnsubscribeAlert" runat="server" OnClick="lnkDeleteSaveSearch_Click"><%= HtmlLocalise("UnsubscribeThisAlert.Label", "Delete and unsubscribe this alert")%></asp:LinkButton>
				</td>
				<td style="width:100%; text-align:right;>
					<asp:Button ID="btnSaveSearch" Text="Save &#187;" runat="server" class="buttonLevel2"
						ValidationGroup="SavedSearch" OnClick="btnSaveSearch_Click" />
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="3">
		<div class="horizontalRule">
		</div>
	</td>
</tr>
<tr>
	<td colspan="3">
		<table role="presentation" class="savedSearchMiddle" role="presentation">
			<tr>
				<td>
				  <asp:CheckBox runat="server" ClientIDMode="Static" ID="EmailSavedSearchCheckbox" onClick="EmailAlertSelected(this);" />
				</td>
				<td>
					<asp:DropDownList ID="ddlAlertFrequency" runat="server" Width="200px" ClientIDMode="Static">
					</asp:DropDownList>
				</td>
				<td>
					<%= HtmlLocalise("as.Label", "as")%>
				</td>
				<td>
					<asp:DropDownList ID="ddlAlertFormat" runat="server" Width="200px" ClientIDMode="Static">
					</asp:DropDownList>
				</td>
				<td>
					<%= HtmlLocalise("at.Label", "at")%>
				</td>
				<td>
					<%= HtmlInFieldLabel("txtEmail", "EmailTextBox.Label", "Email address", 280)%>
					<asp:TextBox ID="txtEmail" runat="server" Width="174px" ClientIDMode="Static" />
				</td>
				<td>
					<%= HtmlLocalise("JobsMatching.Label", "when jobs matching this are found.")%>
				</td>
				<td>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<asp:CustomValidator ID="AlertFrequencyValidator" runat="server" ControlToValidate="ddlAlertFrequency"
						ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error"
						ValidationGroup="SavedSearch" ClientValidationFunction="ValidateAlertFrequency"
						ValidateEmptyText="true" />
				</td>
				<td>
				</td>
				<td>
					<asp:CustomValidator ID="AlertFormatValidator" runat="server" ControlToValidate="ddlAlertFormat"
						ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error"
						ValidationGroup="SavedSearch" ClientValidationFunction="ValidateAlertFormat" ValidateEmptyText="true" />
				</td>
				<td>
				</td>
				<td>
					<asp:CustomValidator ID="AlertEmailValidator" runat="server" ControlToValidate="txtEmail"
						ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error"
						ValidationGroup="SavedSearch" ClientValidationFunction="ValidateAlertEmail" ValidateEmptyText="true" />
					<asp:RegularExpressionValidator ID="valEmailAddress" runat="server" ControlToValidate="txtEmail"
						CssClass="error" Display="Dynamic" SetFocusOnError="True" ValidationExpression="^([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
						ValidationGroup="SavedSearch"></asp:RegularExpressionValidator>
				</td>
				<td>
				</td>
				<td>
				</td>
			</tr>
			<%--			<tr>
				<td colspan="8">
					<%= HtmlLocalise("ResumeSearch.Label", "Search based on this resume:")%>
					<asp:DropDownList ID="ddlResumeNames" runat="server" Width="300px">
					</asp:DropDownList>
				</td>
			</tr>--%>
		</table>
	</td>
</tr>
<tr>
	<td colspan="3">
		<div class="horizontalRule">
		</div>
	</td>
</tr>
<tr>
	<td colspan="3">
		<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
	</td>
</tr>
<script type="text/javascript">

	Sys.Application.add_load(function() {
		EmailAlertSelected(document.getElementById('EmailSavedSearchCheckbox'));
	});
  
	function EmailAlertSelected(sender) 
	{
		var frequency = document.getElementById("<%=ddlAlertFrequency.ClientID %>");
		var format = document.getElementById("<%=ddlAlertFormat.ClientID %>");
		var email = document.getElementById("<%=txtEmail.ClientID %>");

		frequency.disabled = format.disabled = email.disabled = !sender.checked;

		if (!sender.checked)
		{
			document.getElementById("<%=AlertEmailValidator.ClientID %>").style.display = 'none';
			document.getElementById("<%=AlertFrequencyValidator.ClientID %>").style.display = 'none';
			document.getElementById("<%=AlertFormatValidator.ClientID %>").style.display = 'none';

			//email.value = "";
		}
	}

	function ValidateAlertFrequency(sender, args) 
	{
		var frequency = document.getElementById("<%=ddlAlertFrequency.ClientID %>");
		var emailRequired = document.getElementById("<%=EmailSavedSearchCheckbox.ClientID %>");

		if (frequency.selectedIndex == 0) 
		{
			if (emailRequired.checked) 
			{
				sender.innerHTML = "<%= AlertFrequencyRequired %>";
				args.IsValid = false;
			}
		}
	}

	function ValidateAlertFormat(sender, args) 
	{
		var format = document.getElementById("<%=ddlAlertFormat.ClientID %>");
		var emailRequired = document.getElementById("<%=EmailSavedSearchCheckbox.ClientID %>");

		if (format.selectedIndex == 0) 
		{
			if (emailRequired.checked) 
			{
				sender.innerHTML = "<%= AlertFormatRequired %>";
				args.IsValid = false;
			}
		}
	}

	function ValidateAlertEmail(sender, args) 
	{
		var email = document.getElementById("<%=txtEmail.ClientID %>");
		var emailRequired = document.getElementById("<%=EmailSavedSearchCheckbox.ClientID %>");

		if (email.value == "") 
		{
			if (emailRequired.checked) 
			{
				sender.innerHTML = "<%= AlertEmailRequired %>";
				args.IsValid = false;
			}
		}
	}
</script>
