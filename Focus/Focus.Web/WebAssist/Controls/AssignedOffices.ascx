﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignedOffices.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.AssignedOffices" %>

<div>
	<focus:LocalisedLabel runat="server" ID="OfficeDropDownListLabel" AssociatedControlID="OfficeDropDownList" LocalisationKey="OfficeDropDownList.Label" DefaultText="Offices" CssClass="sr-only"/>
	<asp:DropDownList ID="OfficeDropDownList" runat="server" Enabled="False" />
	<asp:Button ID="AddOfficeButton" runat="server" CssClass="button3" Enabled="False"/>
	<asp:Button runat="server" ID="SaveButton" CssClass="button1" OnClick="SaveButtonClick" Enabled="False"/>
</div>
<div><asp:CustomValidator runat="server" ID="OfficeExistsValidator" CssClass="error" Display="Dynamic" style="border: none; min-height: 0px;	display:inline-block; "/></div>
<div>
	<asp:Repeater runat="server" ID="OfficesRepeater" OnItemDataBound="OfficesRepeaterItemDataBound" OnItemCommand="OfficesRepeaterItemCommand" OnDataBinding="OfficesRepeaterDataBinding">
		<ItemTemplate>
			<div class="office-list-item">
				<asp:Label ID="OfficeName" runat="server"></asp:Label>
				<asp:LinkButton ID="RemoveOfficeButton" runat="server" CssClass="toolTipNew" CommandName="RemoveOffice"><asp:Label ID="RemoveOfficeLabel" runat="server" CssClass="sr-only" /></asp:LinkButton>
			</div>
		</ItemTemplate>
	</asp:Repeater>
</div>
<div><asp:CustomValidator runat="server" ID="AssignedOfficeRequired" ValidationGroup="AssignedOfficeUpdate" CssClass="error" ClientValidationFunction="AssignedOfficeRequiredValidate" OnServerValidate="AssignedOfficeRequiredServerValidate"></asp:CustomValidator></div>
<script type="text/javascript">
	function AssignedOfficeRequiredValidate(src, args) {
		var listItems = $('#<%= OfficesRepeater.ClientID %> .office-list-item').length;
		args.IsValid = listItems > 0;
	}
</script>