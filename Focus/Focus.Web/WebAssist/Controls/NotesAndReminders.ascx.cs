﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.NoteReminder;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Core.Models;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class NotesAndReminders : UserControlBase
	{
		private int _notesRemindersCount;
	  private UserDetailsModel _hiringManager;
	  private BusinessUnitDto _businessUnit;
    private PersonDto _jobSeeker;

		public delegate void CompletedHandler(object sender, EventArgs eventArgs);
		public event CompletedHandler Completed;

		public virtual void OnCompleted(EventArgs eventargs)
		{
			if (Completed != null) Completed(this, eventargs);
		}

		public long EntityId
		{
			get { return GetViewStateValue<long>("NotesAndReminders:EntityId"); }
			set { SetViewStateValue("NotesAndReminders:EntityId", value); }
		}

		public EntityTypes EntityType
		{
			get { return GetViewStateValue<EntityTypes>("NotesAndReminders:EntityType"); }
			set { SetViewStateValue("NotesAndReminders:EntityType", value); }
		}

		public bool ReadOnly
		{
			get { return GetViewStateValue<bool>("NotesAndReminders:ReadOnly"); }
			set { SetViewStateValue("NotesAndReminders:ReadOnly", value); }
		}

	  private bool ShowNoteType
	  {
      get { return EntityType.IsIn(EntityTypes.JobSeeker, EntityTypes.Job); }
	  }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				LocaliseUI();
				NoteTypeDropDownLabel.Visible = NoteTypeDropDown.Visible;
			}

			ReminderDateGreaterThanTodayCompareValidatior.ValueToCompare = DateTime.Today.AddDays(-1).ToShortDateString();
      ReminderDateRequired.ValidationGroup 
        = ReminderDateGreaterThanTodayCompareValidatior.ValidationGroup
        = NoteRequired.ValidationGroup
        = AddNoteButton.ValidationGroup
        = string.Concat(ClientID, "_ValidationGroup");
		}
		
		/// <summary>
		/// Handles the Click event of the AddNoteButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AddNoteButton_Click(object sender, EventArgs e)
		{
			if (AddNoteRadio.Checked)
				AddNote();
			else
				AddReminders();

			// Fires event to be picked up by parent modal (when control is in a modal) to hide this control and show confirmation dialog
			if (Completed!=null)
        Completed(this, new EventArgs());

			// Shows confirmaion dialog when used only as a control
			// Could be removed if all places where this control is used handle the above event and show confirmation
			// This Show method is repeated in NotesAndRemindersModal.ascx.cs
			NotesRemindersConfirmationModal.Show(CodeLocalise("Confirmation", "Confirmation"),
																					 CodeLocalise("NoteReminderAdded.Body",
																												"Your {0} has been stored against the selected recipient(s)",
																												AddNoteRadio.Checked
																													? CodeLocalise("Note", "note")
																													: CodeLocalise("Reminder", "reminder")),
																					 CodeLocalise("CloseModal.Text", "OK"), height: 75);
			
			ResetForm();
			BindNotesRemindersList();
		}

		/// <summary>
		/// Handles the Selecting event of the NotesRemindersDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void NotesRemindersDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			var listType = NoteReminderDropDown.SelectedValueToEnum<NoteReminderTypes>();
		  NoteTypes? noteType = null;
      if (ShowNoteType && listType == NoteReminderTypes.Note)
      {
        if (NoteTypeDropDown.SelectedIndex > 0)
          noteType = NoteTypeDropDown.SelectedValueToEnum<NoteTypes>();
      }

		  e.InputParameters["criteria"] = new NoteReminderCriteria{ EntityId = EntityId, NoteReminderType = listType, EntityType = EntityType, NoteType = noteType};
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the NoteReminderDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void NoteReminderDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			NotesRemindersListPager.ReturnToFirstPage();
			BindNotesRemindersList();
      NoteTypeDropDownLabel.Visible = NoteTypeDropDown.Visible = ShowNoteType &&
		                             NoteReminderDropDown.SelectedValueToEnum<NoteReminderTypes>() == NoteReminderTypes.Note;
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the notes reminders.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<NoteReminderViewDto> GetNotesReminders(NoteReminderCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_notesRemindersCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;
			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			var notesReminders = ServiceClientLocator.CoreClient(App).GetNotesReminders(criteria);
			_notesRemindersCount = notesReminders.TotalCount;

			return notesReminders;
		}

		/// <summary>
		/// Gets the notes reminders count.
		/// </summary>
		/// <returns></returns>
		public int GetNotesRemindersCount()
		{
			return _notesRemindersCount;
		}

		/// <summary>
		/// Gets the employees count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetNotesRemindersCount(NoteReminderCriteria criteria)
		{
			return _notesRemindersCount;
		}

		#endregion

		#region Binding methods

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			if (EntityType.IsNull())
				throw new Exception("No entity type specified");

			if (!ReadOnly)
			{
				_hiringManager = null;

				switch (EntityType)
				{
					case EntityTypes.Job:
						var job = ServiceClientLocator.JobClient(App).GetJobView(EntityId);


						if (job.IsNull())
							throw new Exception("Invalid job id");

						if (job.EmployeeId.HasValue)
							_hiringManager = ServiceClientLocator.EmployeeClient(App).GetUserDetails(job.EmployeeId.Value);

				    BindNotesForDropDown(job);

						break;
          case EntityTypes.BusinessUnit:
            _businessUnit = ServiceClientLocator.EmployerClient(App).GetBusinessUnit(EntityId);

            if (_businessUnit.IsNull())
              throw new Exception("Invalid business unit id");

				    break;
            case EntityTypes.Employee:
				    _hiringManager = ServiceClientLocator.EmployeeClient(App).GetUserDetails(EntityId);
				    break;
          case EntityTypes.JobSeeker:
            _jobSeeker = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(EntityId);
				    break;
					default:
						throw new Exception(String.Format("Notes and reminders not available for {0} entity type", EntityType.ToString()));
				}

				BindReminderTypeRadioButtonList();
				//BindNoteForDropDown(job, employee);
				BindReminderForDropDown();
			}

      // Only show Note Type drop down for Job Seeker notes
		  NoteTypeDropDownLabel.Visible =  NoteTypeDropDown.Visible = ShowNoteType;
      if (NoteTypeDropDown.Visible)
        BindNoteTypeDropDown();

			BindNotesRemindersList();
			BindNoteReminderDropDown();

			AddNotePanel.Visible = (!ReadOnly);
      NoteForDropDownLabel.Visible = ForLiteral.Visible = NoteForDropDown.Visible = !ReadOnly && EntityType == EntityTypes.Job;
		}

    /// <summary>
    /// Binds the notes for drop down. (Only used for jobs)
    /// </summary>
    /// <param name="job">The job.</param>
    private void BindNotesForDropDown(JobViewDto job)
    {
      NoteForDropDown.Items.Clear();
      ForLiteral.Text = CodeLocalise("for", "for");
      NoteForDropDown.Items.Add(new ListItem(job.JobTitle, job.Id.ToString()));
      NoteForDropDown.Items.Add(new ListItem(job.BusinessUnitName, job.BusinessUnitId.ToString()));
      if (_hiringManager.IsNotNull())
        NoteForDropDown.Items.Add(new ListItem(_hiringManager.PersonDetails.FirstName + " " + _hiringManager.PersonDetails.LastName, job.EmployeeId.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture)));
    }

		/// <summary>
		/// Binds the reminder type radio button list.
		/// </summary>
		private void BindReminderTypeRadioButtonList()
		{
			ReminderTypeRadioButtonList.Items.Clear();
			ReminderTypeRadioButtonList.Items.Add(new ListItem(CodeLocalise("EmailRadioButton.Text", "Email"), ReminderMedia.Email.ToString()));
			ReminderTypeRadioButtonList.Items.Add(new ListItem(CodeLocalise("DashboardMessageRadioButton.Text", "Post to dashboard on"), ReminderMedia.DashboardMessage.ToString()));
			ReminderTypeRadioButtonList.Items[0].Selected = true;
		}

		/// <summary>
		/// Binds the reminder for drop down.
		/// </summary>
		private void BindReminderForDropDown()
		{
			ReminderForDropDown.Items.Clear();
			ReminderForDropDown.Items.Add(new ListItem(CodeLocalise("ReminderForMyself.Text.NoEdit", "myself"), App.User.UserId.ToString()));
      switch (EntityType)
      {
        case EntityTypes.Job:
        case EntityTypes.Employee:
          if (_hiringManager.IsNotNull())
          {
            ReminderForDropDown.Items.Add(
              new ListItem(
                String.Format("{0}, {1}", _hiringManager.PersonDetails.LastName, _hiringManager.PersonDetails.FirstName),
                _hiringManager.UserId.ToString()));
            ReminderForDropDown.Items.Add(
              new ListItem(
                String.Format("myself and {0}, {1}", _hiringManager.PersonDetails.LastName,
                              _hiringManager.PersonDetails.FirstName),
                string.Format("{0}#{1}", App.User.UserId, _hiringManager.UserId)));
          }
          break;

        case EntityTypes.BusinessUnit:
          ReminderForDropDown.Items.Add(
            new ListItem(
              _businessUnit.Name,
              _businessUnit.Id.ToString()));
            ReminderForDropDown.Items.Add(
              new ListItem(
                String.Format("myself and {0}", _businessUnit.Name),
                string.Format("{0}#{1}", App.User.UserId, _businessUnit.Id)));
          break;

				case EntityTypes.JobSeeker:
					ReminderForDropDown.Items.Add(
						new ListItem(
              String.Format("{0} {1}", _jobSeeker.FirstName, _jobSeeker.LastName), _jobSeeker.Id.ToString()));
					ReminderForDropDown.Items.Add(
						new ListItem(
              String.Format("myself and {0} {1}", _jobSeeker.FirstName, _jobSeeker.LastName),
              string.Format("{0}#{1}", App.User.UserId, _jobSeeker.Id)));
					break;
      }

		  SetDefaultReminderOption();
		}

    /// <summary>
    /// Sets the default reminder option.
    /// </summary>
    private void SetDefaultReminderOption()
    {
      // Hiring manager for employee notes and reminders, myself for all others
      ReminderForDropDown.SelectedIndex = EntityType == EntityTypes.Employee ? 1 : 0;
    }

	  /// <summary>
		/// Binds the notes reminders list.
		/// </summary>
		private void BindNotesRemindersList()
		{
			NotesRemindersList.DataBind();

			// Set visibility of other controls
			NotesRemindersListPager.Visible = (NotesRemindersListPager.TotalRowCount > 0);
		}

		/// <summary>
		/// Binds the note reminder drop down.
		/// </summary>
		private void BindNoteReminderDropDown()
		{
			NoteReminderDropDown.Items.Clear();
			NoteReminderDropDown.Items.AddEnum(NoteReminderTypes.Note, "Notes");
			NoteReminderDropDown.Items.AddEnum(NoteReminderTypes.Reminder, "Reminders");
		}

    /// <summary>
    /// Binds the note type drop down.
    /// </summary>
    private void BindNoteTypeDropDown()
    {
      NoteTypeDropDown.Items.Clear();
      NoteTypeDropDown.Items.Add(new ListItem(CodeLocalise("All.Text.NoEdit", "All")));
      NoteTypeDropDown.Items.AddEnum(NoteTypes.ResolvedIssues, "Resolved issues");
      NoteTypeDropDown.Items.AddEnum(NoteTypes.FollowUpRequest, "Follow-ups");
    }

		#endregion

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			AddNoteButton.Text = CodeLocalise("AddNoteButton.Text.NoEdit", "Add note or reminder");
			ReminderDateRequired.ErrorMessage = CodeLocalise("ReminderDateRequired.ErrorMessage", "Reminder date required");
			ReminderDateGreaterThanTodayCompareValidatior.ErrorMessage = CodeLocalise("ReminderDateGreaterThanTodayCompareValidatior.ErrorMessage", "Date must not be in the past");
			NoteRequired.ErrorMessage = CodeLocalise("NoteRequired.ErrorMessage", "Note or reminder required");
		  ReminderDateTextBox.ToolTip = CodeLocalise("ReminderDateTextBox.ToolTip", "Please enter the reminder date as mm/dd/yyyy");
		}

		/// <summary>
		/// Resets the form.
		/// </summary>
		private void ResetForm()
		{
			AddNoteRadio.Checked = true;
			AddReminderRadio.Checked = false;
			ReminderTypeRadioButtonList.SelectedIndex = 0;
      SetDefaultReminderOption();
			ReminderDateTextBox.Text = string.Empty;
			NoteTextBox.Text = string.Empty;
      if (EntityType == EntityTypes.Job)
        NoteForDropDown.SelectedIndex = 0;
		}

		/// <summary>
		/// Adds the note.
		/// </summary>
		private void AddNote()
		{
			var note = new NoteReminderDto
			{
				Text = NoteTextBox.TextTrimmed(),
				CreatedBy = App.User.UserId,
				NoteReminderType = NoteReminderTypes.Note
			};
      if (EntityType == EntityTypes.Job)
      {
        switch (NoteForDropDown.SelectedIndex)
        {
          case 0:
            note.EntityType = EntityTypes.Job;
            break;
          case 1:
            note.EntityType = EntityTypes.BusinessUnit;
            break;
          case 2:
            note.EntityType = EntityTypes.Employee;
            break;
        }
        note.EntityId = NoteForDropDown.SelectedValue.ToLong().Value;
      }
      else
      {
        note.EntityId = EntityId;
        note.EntityType = EntityType;
      }

			ServiceClientLocator.CoreClient(App).SaveNoteReminder(note, null);
		}

		/// <summary>
		/// Adds the reminders.
		/// </summary>
		private void AddReminders()
		{
			var userIds = ReminderForDropDown.SelectedValue.Split('#');

		  var reminder = new NoteReminderDto
		                   {
		                     Text = NoteTextBox.TextTrimmed(),
		                     EntityId = EntityId,
                         EntityType = EntityType,
		                     CreatedBy = App.User.UserId,
		                     NoteReminderType = NoteReminderTypes.Reminder,
		                     ReminderVia = ReminderTypeRadioButtonList.SelectedValueToEnum<ReminderMedia>(),
		                     ReminderDue = ReminderDateTextBox.TextTrimmed().ToDate()
		                   };
      var recipients = new List<NoteReminderRecipientDto>();
      switch (EntityType)
      {
        case EntityTypes.Job:
        case EntityTypes.Employee:
          recipients = userIds.Select(userId => new NoteReminderRecipientDto()
                                                  {
                                                    RecipientEntityId = Convert.ToInt64(userId),
                                                    RecipientEntityType = EntityTypes.User
                                                  }).ToList();
          break;

        case EntityTypes.JobSeeker:
          // Overly complicated but ready for other options in the future (which are commented out)
          recipients.Add(new NoteReminderRecipientDto
          {
            RecipientEntityId = userIds[0].ToLong().GetValueOrDefault(),
            RecipientEntityType = ReminderForDropDown.SelectedIndex == 1? EntityTypes.JobSeeker : EntityTypes.User
          });

          if (ReminderForDropDown.SelectedIndex == 2)
          {
            recipients.Add(new NoteReminderRecipientDto
            {
              RecipientEntityId = userIds[1].ToLong().GetValueOrDefault(),
              RecipientEntityType = EntityTypes.JobSeeker
            });
          }
          break;

        case EntityTypes.BusinessUnit:
          if (ReminderForDropDown.SelectedIndex == 1)
            recipients.Add(new NoteReminderRecipientDto
                             {
                               RecipientEntityId = userIds[0].ToLong().GetValueOrDefault(),
                               RecipientEntityType = EntityTypes.BusinessUnit
                             });
          else
          {
            recipients.Add(new NoteReminderRecipientDto
                             {
                               RecipientEntityId = userIds[0].ToLong().GetValueOrDefault(),
                               RecipientEntityType = EntityTypes.User
                             });
            if (ReminderForDropDown.SelectedIndex == 2)
              recipients.Add(new NoteReminderRecipientDto
                               {
                                 RecipientEntityId = userIds[1].ToLong().GetValueOrDefault(),
                                 RecipientEntityType = EntityTypes.BusinessUnit
                               });
          }
          break;
      }

		  var reminderId = ServiceClientLocator.CoreClient(App).SaveNoteReminder(reminder, recipients).Id;

      if (reminder.ReminderDue <= DateTime.Now && reminderId.HasValue)
        ServiceClientLocator.CoreClient(App).SendReminder(reminderId.Value);
		}
	}
}