﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendMessageModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.SendMessageModal" %>
<%@ Register src="~/WebAssist/Controls/SendMessages.ascx" tagname="SendMessage" tagprefix="uc" %>

<asp:HiddenField ID="SendMessageModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="SendMessageModalPopup" runat="server"
												BehaviorID="SendMessageModal"
												TargetControlID="SendMessageModalDummyTarget"
												PopupControlID="SendMessageModalPanel"
												BackgroundCssClass="modalBackground" 
												RepositionMode="RepositionOnWindowResizeAndScroll" />
												
<asp:panel id="SendMessageModalPanel" runat="server" class="modal" style="display:none;">
	<h1>
		<focus:LocalisedLabel runat="server" ID="SendMessageHeadingLabel" LocalisationKey="SendMessageHeadingLabel.Text" DefaultText="Email recipients"/>
	</h1>
	<div>
		<uc:SendMessage ID="SendMessage" runat="server" />
	</div>
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('<%=SendMessageModalPopup.BehaviorID %>').hide(); return false;" /></div>
</asp:panel>