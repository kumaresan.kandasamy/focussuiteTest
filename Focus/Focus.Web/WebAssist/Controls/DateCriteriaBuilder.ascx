﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateCriteriaBuilder.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.DateCriteriaBuilder" %>

<table>
	<tr>
		<td><%= HtmlLabel("DateWithin.Label", "Show results within")%></td>
		<td></td>
	</tr>
	<tr>
		<td width="200px"><asp:DropDownList runat="server" ID="DateWithinDropDown" ClientIDMode="Static" /></td>
		<td><asp:Button ID="AddDateWithinFilterButton" runat="server" CssClass="button3" ClientIDMode="Static" OnClick="AddDateWithinFilterButton_Clicked" /></td>
	</tr>
	<tr>
		<td><%= HtmlLabel("DateFrom.Label", "Show results from")%></td>
		<td></td>
	</tr>
	<tr>
		<td>
			<asp:TextBox ID="DateFromTextBox" runat="server"  ClientIDMode="Static" />
			<act:CalendarExtender ID="DateFromCalendarExtender" runat="server" TargetControlID="DateFromTextBox" CssClass="cal_Theme1" />
		</td>
		<td><asp:Button ID="AddDateFromFilterButton" runat="server" CssClass="button3" ClientIDMode="Static" OnClick="AddDateFromFilterButton_Clicked" /></td>
	</tr>
	<tr>
		<td><%= HtmlLabel("DateFrom.Label", "Show results from")%></td>
		<td></td>
	</tr>
	<tr>
		<td>
			<asp:TextBox ID="DateRangeFromTextBox" runat="server"  ClientIDMode="Static" />
			<act:CalendarExtender ID="DateRangeFromCalendarExtender" runat="server" TargetControlID="DateRangeFromTextBox" CssClass="cal_Theme1"/>
		</td>
		<td></td>
	</tr>
	<tr>
		<td><%= HtmlLabel("DateTo.Label", "to")%></td>
		<td></td>
	</tr>
	<tr>
		<td>
			<asp:TextBox ID="DateRangeToTextBox" runat="server"  ClientIDMode="Static" />
			<act:CalendarExtender ID="DateRangeToCalendarExtender" runat="server" TargetControlID="DateRangeToTextBox" CssClass="cal_Theme1"/>
		</td>
		<td><asp:Button ID="AddDateRangeFilterButton" runat="server" CssClass="button3" ClientIDMode="Static" OnClick="AddDateRangeFilterButton_Clicked" /></td>
	</tr>
</table>