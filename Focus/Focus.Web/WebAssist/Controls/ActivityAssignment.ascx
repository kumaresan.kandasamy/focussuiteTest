﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivityAssignment.ascx.cs"
  Inherits="Focus.Web.WebAssist.Controls.ActivityAssignment" %>
<table style="border-collapse: collapse;" role="presentation">
  <tr>
    <td>
	    <focus:LocalisedLabel runat="server" ID="ActivityCategoryDropDownListLabel" AssociatedControlID="ActivityCategoryDropDownList" LocalisationKey="ActivityCategory" DefaultText="Activity category" CssClass="sr-only"/>
      <asp:DropDownList runat="server" ID="ActivityCategoryDropDownList"></asp:DropDownList>
      <asp:Literal runat="server" ID="linebreaker"><br/></asp:Literal>
      <focus:AjaxDropDownList runat="server" ID="ActivityDropDownList"></focus:AjaxDropDownList>
      <act:CascadingDropDown ID="ActivityCascader" runat="server" TargetControlID="ActivityDropDownList" ParentControlID="ActivityCategoryDropDownList" ServicePath="~/Services/AjaxService.svc" Category="Activities" ServiceMethod="GetActivities" />
			<focus:LocalisedLabel runat="server" ID="ActivityDateTextBoxLable" AssociatedControlID="ActivityDateTextBox" LocalisationKey="ActivityDate" DefaultText="Activity Date" CssClass="sr-only" />
      <asp:TextBox ID="ActivityDateTextBox" runat="server" ClientIDMode="Static" />
      <act:CalendarExtender ID="ActivityDateCalendarExtender" runat="server" TargetControlID="ActivityDateTextBox" CssClass="cal_Theme1" />
      <asp:Button ID="SelectActivityButton" runat="server" CssClass="button3" CausesValidation="True" ValidationGroup="Activities" OnClick="SelectActivityButton_Click" Text="Go"/>
    </td>
  </tr>
  <tr>
    <td>
      <asp:RequiredFieldValidator runat="server" ID="ActivityRequiredValidator" ControlToValidate="ActivityDropDownList" CssClass="error" SetFocusOnError="True" ValidationGroup="Activities" Display="Dynamic"></asp:RequiredFieldValidator>
      <asp:RequiredFieldValidator  ID="ActivityDateIsNotEmpty" runat="server" ControlToValidate="ActivityDateTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error"  ValidationGroup="Activities"/>
      <asp:CompareValidator ID="ActivityDateCompareValidator" runat="server" ControlToValidate="ActivityDateTextBox" Type="Date" Operator="LessThanEqual" CssClass="error" Display="Dynamic" ValidationGroup="Activities" />
    </td>
  </tr>
</table>
