﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WelcomeToAssistModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.WelcomeToAssistModal" %>

<asp:HiddenField ID="WelcomeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="WelcomeModal" runat="server" BehaviorID="WelcomeModal"
												TargetControlID="WelcomeModalDummyTarget"
												PopupControlID="WelcomePanel" 
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground"
												CancelControlID="ReturnHomeButton" />

<asp:Panel ID="WelcomePanel" runat="server" CssClass="modal" Width="550px" ClientIDMode="Static" Style="display:none;" >
	<div>
		<asp:Panel runat="server" ID="WelcomeContentPanel" Visible="False">
			<asp:Literal ID="WelcomeContentLiteral" runat="server" />
		</asp:Panel>
		<asp:Panel ID="OfficeSelectorPanel" runat="server" Visible="False">
			<strong id="OfficeSelectorTitle" runat="server"><%= HtmlLocalise("OfficeSelectorTitle.Text", "Welcome to #FOCUSASSIST#") %></strong>
			<br/><%= HtmlLabel(OfficeDropDownList,"OfficeConfirmation.Text", "Please confirm which office you are currently working at?") %>
			<div style="margin-top:20px;">
				<asp:DropDownList ID="OfficeDropDownList" runat="server" ValidationGroup="OfficeValidation" />
				<asp:Button ID="SaveCurrentOfficeButton" CausesValidation="True" ValidationGroup="OfficeValidation" class="button1" runat="server" Text="Save" OnClick="SaveCurrentOfficeClick" />
				<asp:RequiredFieldValidator CssClass="error" ControlToValidate="OfficeDropDownList" ValidationGroup="OfficeValidation" ErrorMessage="Please select a valid office from the list." InitialValue="" runat="server"></asp:RequiredFieldValidator>
			</div>
		</asp:Panel>
	</div>
	<div class="closeIcon" id="closeIcon" runat="server"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.TextButton", "Close") %>" onclick="$find('WelcomeModal').hide(); return false;" /></div>
</asp:Panel>
