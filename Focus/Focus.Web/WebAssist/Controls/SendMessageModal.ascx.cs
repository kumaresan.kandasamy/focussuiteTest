﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class SendMessageModal : UserControlBase
	{
		public delegate void CompletedHandler(object sender, SendMessages.SendMessageEventArgs eventArgs);
		public event CompletedHandler Completed;

		private ActionTypes ActionType
		{
			get { return GetViewStateValue<ActionTypes>("SendMessageModal:ActionType"); }
			set { SetViewStateValue("SendMessageModal:ActionType", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			SendMessage.SavedMessageChanged += SendMessage_SavedMessageChanged;
			SendMessage.Completed += SendMessage_Completed;
		}

		/// <summary>
		/// Shows the specified action.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="jobSeekerIds">The job seeker ids.</param>
		/// <param name="jobUrl">The job URL.</param>
		public void Show(ActionTypes action, List<long> jobSeekerIds, string jobUrl = "")
		{
			ActionType = action;

			Localise();

			SendMessage.Bind(action, jobSeekerIds, jobUrl);

			SendMessageModalPopup.Show();
		}

		/// <summary>
		/// Handles the SavedMessageChanged event of the SendMessage control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SendMessage_SavedMessageChanged(object sender, EventArgs e)
		{
			Localise(); // Re-establish the correct modal heading

			SendMessageModalPopup.Show();
		}

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			if (ActionType == ActionTypes.SendCandidateEmails)
			{
				SendMessageHeadingLabel.DefaultText = CodeLocalise("SendMessageHeadingLabel.Text", "Email recipients");
			}
			else if (ActionType == ActionTypes.CreateCandidateHomepageAlerts)
			{
				SendMessageHeadingLabel.DefaultText = CodeLocalise("SendMessageHeadingLabel.Text", "Post to homepage");
			}
		}

		/// <summary>
		/// Handles the Completed event of the SendMessage control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventArgs">The <see cref="SendMessages.SendMessageEventArgs"/> instance containing the event data.</param>
		protected void SendMessage_Completed(object sender, SendMessages.SendMessageEventArgs eventArgs)
		{
			if (Completed != null)
				Completed(this, eventArgs);
		}
	}
}