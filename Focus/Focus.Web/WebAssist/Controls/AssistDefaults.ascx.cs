﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;

using Framework.Core;

using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class AssistDefaults : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
			  ApplyTheme();
				LocaliseUI();
				ApplyBranding();
				BindPlacementTimeframeDropDown();
				BindMinimumStarsDropDown();
			  BindLowQualityMatchesDropDown();
				BindFlagLowMatchesPeriodDaysDropDown();
			}

			if (App.Settings.UseCustomHeaderHtml)
			{
				ColourDefaultsHeaderPanel.Visible = false;
				ColourDefaultsPanel.Visible = false;
				LogoDefaultsHeaderPanel.Visible = false;
				LogoDefaultsPanel.Visible = false;
			}

			ReferralRequestApprovalQueuePlaceHolder.Visible = App.Settings.OfficesEnabled;
		}

		/// <summary>
		/// Handles the Click event of the SaveChangesButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveChangesButton_Click(object sender, EventArgs e)
		{
			if (LightColourTextBox.Text == string.Empty)
			{
				LightColourValidator.IsValid = false;
				return;
			}

			if (DarkColourTextBox.Text == string.Empty)
			{
				DarkColourValidator.IsValid = false;
				return;
			}

			var bytes = App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedImage);

			if (bytes.IsNotNullOrEmpty())
			{
				var applicationImage = new ApplicationImageDto {Image = bytes, Type = ApplicationImageTypes.FocusAssistHeader};
				ServiceClientLocator.CoreClient(App).SaveApplicationImage(applicationImage);

				AssistApplicationLogoUploader.Clear();
				AssistApplicationLogoUploader.SetPreviewImage(UrlBuilder.ApplicationImage(ApplicationImageTypes.FocusAssistHeader));
			}

			var configurationItems = new List<ConfigurationItemDto>();

			if (AssistFooterLogoCheckBox.Checked != App.Settings.ShowBurningGlassLogoInAssist)
				configurationItems.Add(new ConfigurationItemDto
				{
					Key = Constants.ConfigurationItemKeys.ShowBurningGlassLogoInAssist,
					Value = AssistFooterLogoCheckBox.Checked.ToString()
				});

			if (LightColourTextBox.TextTrimmed() != App.Settings.AssistLightColour)
				configurationItems.Add(new ConfigurationItemDto
				{
					Key = Constants.ConfigurationItemKeys.AssistLightColour,
					Value = LightColourTextBox.TextTrimmed()
				});

			if (DarkColourTextBox.TextTrimmed() != App.Settings.AssistDarkColour)
				configurationItems.Add(new ConfigurationItemDto
				{
					Key = Constants.ConfigurationItemKeys.AssistDarkColour,
					Value = DarkColourTextBox.TextTrimmed()
				});

			if (PlacementTimeframeDropDown.SelectedItem.Text.ToInt() != App.Settings.RecentPlacementTimeframe)
				configurationItems.Add(new ConfigurationItemDto
				{
					Key = Constants.ConfigurationItemKeys.RecentPlacementTimeframe,
					Value = PlacementTimeframeDropDown.SelectedValue
				});

			var minimumStars = JobSeekersWithXStarsRadioButton.Checked ? MinimumStarsDropDownList.SelectedItem.Text.ToInt() : 0;
			if (minimumStars != App.Settings.RecommendedMatchesMinimumStars)
				configurationItems.Add(new ConfigurationItemDto
				{
					Key = Constants.ConfigurationItemKeys.RecommendedMatchesMinimumStars,
					Value = minimumStars.ToString()
				});

			if (FlagDefaultPlaceHolder.Visible)
				UnbindFlagDefaultSettings(configurationItems);

			if (LaborInsightPlaceHolder.Visible)
				UnbindLaborInsightSettings(configurationItems);

			if (JobOrderDefaultsPlaceHolder.Visible)
				UnbindJobOrderDefaultSettings(configurationItems);

            if (EnableJobSeekerActivitiesCheckBox.Checked != App.Settings.EnableJobSeekerActivitiesServices)
                configurationItems.Add(new ConfigurationItemDto
                {
                    Key = Constants.ConfigurationItemKeys.EnableJobSeekerActivitiesServices,
                    Value = EnableJobSeekerActivitiesCheckBox.Checked.ToString()
                });

            if (EnableHiringManagerActivitiesCheckBox.Checked != App.Settings.EnableHiringManagerActivitiesServices)
                configurationItems.Add(new ConfigurationItemDto
                {
                    Key = Constants.ConfigurationItemKeys.EnableHiringManagerActivitiesServices,
                    Value = EnableHiringManagerActivitiesCheckBox.Checked.ToString()
                });

			if (EnableOfficesCheckBox.Checked != App.Settings.OfficesEnabled)
				configurationItems.Add(new ConfigurationItemDto
				{
					Key = Constants.ConfigurationItemKeys.OfficesEnabled,
					Value = EnableOfficesCheckBox.Checked.ToString()
				});

			if (JobPostingAssignOfficeRadioButton.Checked != App.Settings.ReferralRequestApprovalQueueOfficeFilterType)
				configurationItems.Add(new ConfigurationItemDto
				{
					Key = Constants.ConfigurationItemKeys.ReferralRequestApprovalQueueOfficeFilterType,
					Value = JobPostingAssignOfficeRadioButton.Checked.ToString()
				});

			if (configurationItems.IsNotNullOrEmpty())
			{
				ServiceClientLocator.CoreClient(App).SaveConfigurationItems(configurationItems);
				App.RefreshSettings();
			}

			var localisationItems = new List<LocalisationItemDto>();

			var localisationItem = CheckLocalisationItem(CandidateTypeTextBox, "Global.CandidateType",
				HtmlLocalise("Global.CandidateType", "#CANDIDATETYPE#"));
			if (localisationItem != null)
				localisationItems.Add(localisationItem);

			localisationItem = CheckLocalisationItem(CandidateTypePluralTextBox, "Global.CandidateTypes",
				HtmlLocalise("Global.CandidateTypes", "#CANDIDATETYPES#"));
			if (localisationItem != null)
				localisationItems.Add(localisationItem);

			localisationItem = CheckLocalisationItem(BusinessTerminologyTextBox, "Global.Business",
				HtmlLocalise("Global.Business", "#BUSINESS#"));
			if (localisationItem != null)
				localisationItems.Add(localisationItem);

			localisationItem = CheckLocalisationItem(BusinessTerminologyPluralTextBox, "Global.Businesses",
				HtmlLocalise("Global.Businesses", "#BUSINESSES#"));
			if (localisationItem != null)
				localisationItems.Add(localisationItem);

			if (localisationItems.Any())
			{
				ServiceClientLocator.CoreClient(App).SaveLocalisationItems(localisationItems);
				ResetLocaliser(true);
			}

			ReferralRequestApprovalQueuePlaceHolder.Visible = App.Settings.OfficesEnabled;

			Confirmation.Show(CodeLocalise("AssistDefaultsUpdatedConfirmation.Title", "Assist defaults updated"),
				CodeLocalise("AssistDefaultsUpdatedConfirmation.Body", "The default settings have been updated."),
				CodeLocalise("CloseModal.Text", "OK"));

		}

		/// <summary>
		/// Checks if a localisation item has been changed
		/// </summary>
		/// <param name="box">The text box holding the new value</param>
		/// <param name="key">The key of the localisation item</param>
		/// <param name="oldText">The original text</param>
		/// <param name="byTheme"></param>
		/// <returns>A localisation record if a change has been made</returns>
		private LocalisationItemDto CheckLocalisationItem(TextBox box, string key, string oldText, bool byTheme = true)
    {
	    var fullKey = key;

			if (byTheme)
				fullKey = string.Concat(key, ".", App.Settings.Theme.ToString());

      var newText = box.Text.Trim();

      if (newText != oldText)
      {
        return new LocalisationItemDto
        {
          Key = fullKey,
          Value = newText
        };
      }

      return null;
    }

		/// <summary>
		/// Handles the Click event of the ResetColoursButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ResetColoursButton_Click(object sender, EventArgs e)
		{
			LightColourTextBox.Text = App.Settings.AssistLightColour;
			DarkColourTextBox.Text = App.Settings.AssistDarkColour;
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			// Clear the session values and load the current image into the preview
			AssistApplicationLogoUploader.Clear();
			AssistApplicationLogoUploader.SetPreviewImage(UrlBuilder.ApplicationImage(ApplicationImageTypes.FocusAssistHeader));

			LightColourTextBox.Text = App.Settings.AssistLightColour;
			DarkColourTextBox.Text = App.Settings.AssistDarkColour;

			AssistFooterLogoCheckBox.Checked = App.Settings.ShowBurningGlassLogoInAssist;

      PlacementTimeframeDropDown.SelectValue(App.Settings.RecentPlacementTimeframe.ToString(CultureInfo.InvariantCulture));

      FlagSeekerNotLoggedOnCheckBox.Checked = App.Settings.NotLoggingInEnabled;
      FlagSeekerNotLoggedOnDaysTextBox.Text = App.Settings.NotLoggingInDaysThreshold.ToString(CultureInfo.CurrentCulture);

      FlagSeekerNotClickingLeadsCheckBox.Checked = App.Settings.NotClickingLeadsEnabled;
			FlagSeekerNotClickingLeadsCountTextBox.Text = App.Settings.NotClickingLeadsCountThreshold.ToString(CultureInfo.CurrentCulture);
			FlagSeekerNotClickingLeadsDaysTextBox.Text = App.Settings.NotClickingLeadsDaysThreshold.ToString(CultureInfo.CurrentCulture);

		  FlagSeekerNotSearchingJobsCheckBox.Checked = App.Settings.NotSearchingJobsEnabled;
      FlagSeekerNotSearchingJobsCountTextBox.Text = App.Settings.NotSearchingJobsCountThreshold.ToString(CultureInfo.CurrentCulture);
      FlagSeekerNotSearchingJobsDaysTextBox.Text = App.Settings.NotSearchingJobsDaysThreshold.ToString(CultureInfo.CurrentCulture);

      FlagSeekerRejectingOffersCheckBox.Checked = App.Settings.RejectingJobOffersEnabled;
      FlagSeekerRejectingOffersCountTextBox.Text = App.Settings.RejectingJobOffersCountThreshold.ToString(CultureInfo.CurrentCulture);
      FlagSeekerRejectingOffersDaysTextBox.Text = App.Settings.RejectingJobOffersDaysThreshold.ToString(CultureInfo.CurrentCulture);

      FlagSeekerNotReportingInterviewsCheckBox.Checked = App.Settings.NotReportingForInterviewEnabled;
      FlagSeekerNotReportingInterviewsCountTextBox.Text = App.Settings.NotReportingForInterviewCountThreshold.ToString(CultureInfo.CurrentCulture);
      FlagSeekerNotReportingInterviewsDaysTextBox.Text = App.Settings.NotReportingForInterviewDaysThreshold.ToString(CultureInfo.CurrentCulture);

      FlagSeekerNotRespondingInvitesCheckBox.Checked = App.Settings.NotRespondingToEmployerInvitesEnabled;
      FlagSeekerNotRespondingInvitesCountTextBox.Text = App.Settings.NotRespondingToEmployerInvitesCountThreshold.ToString(CultureInfo.CurrentCulture);
      FlagSeekerNotRespondingInvitesDaysTextBox.Text = App.Settings.NotRespondingToEmployerInvitesDaysThreshold.ToString(CultureInfo.CurrentCulture);
      FlagSeekerNotRespondingInvitesGraceTextBox.Text = App.Settings.NotRespondingToEmployerInvitesGracePeriodDays.ToString(CultureInfo.CurrentCulture);

      FlagLowQualityMatchesCheckBox.Checked = App.Settings.ShowingLowQualityMatchesEnabled;
      FlagLowQualityMatchesCountTextBox.Text = App.Settings.ShowingLowQualityMatchesCountThreshold.ToString(CultureInfo.CurrentCulture);
      FlagLowQualityMatchesDropDown.SelectedIndex = App.Settings.ShowingLowQualityMatchesMinStarThreshold - 1;
      FlagLowQualityMatchesDaysTextBox.Text = App.Settings.ShowingLowQualityMatchesDaysThreshold.ToString(CultureInfo.CurrentCulture);

		  FlagLowQualityResumeCheckBox.Checked = App.Settings.PostingLowQualityResumeEnabled;
			FlagLowQualityResumeWordCountTextBox.Text = App.Settings.PostingLowQualityResumeWordCountThreshold.ToString(CultureInfo.CurrentCulture);
			FlagLowQualityResumeSkillCountTextBox.Text = App.Settings.PostingLowQualityResumeSkillCountThreshold.ToString(CultureInfo.CurrentCulture);
		  
		  FlagPostHireFollowUpCheckBox.Checked = App.Settings.PostHireFollowUpEnabled;

			FlagExpiredAlienCertificationCheckBox.Checked = App.Settings.ExpiredAlienCertificationEnabled;
			FlagPositiveFeedbackCheckbox.Checked = App.Settings.PositiveFeedbackCheckEnabled;
			FlagNegativeFeedbackCheckBox.Checked = App.Settings.NegativeFeedbackCheckEnabled;

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				FlagPotentialMSFWCheckBox.Checked = App.Settings.JobIssueFlagPotentialMSFW;
				InappropriateEmailCheckBox.Checked = App.Settings.InappropriateEmailAddressCheckEnabled;
			}
		  
			// Bind job order issues settings.
			FlagLowMatchesCheckBox.Checked = App.Settings.JobIssueFlagLowMinimumStarMatches;
			FlagLowMatchesCountTextBox.Text = App.Settings.JobIssueMinimumMatchCountThreshold.ToString(CultureInfo.CurrentCulture);
			FlagLowMatchesMinimumStarsTextBox.Text = App.Settings.JobIssueMatchMinimumStarsThreshold.ToString(CultureInfo.CurrentCulture);
			
			FlagLowReferralsCheckBox.Checked = App.Settings.JobIssueFlagLowReferrals;
			FlagLowReferralsCountTextBox.Text = App.Settings.JobIssueMinimumReferralsCountThreshold.ToString(CultureInfo.CurrentCulture);
			FlagLowReferralsPeriodDaysTextBox.Text = App.Settings.JobIssueReferralCountDaysThreshold.ToString(CultureInfo.CurrentCulture);

			FlagNotClickingReferralsCheckBox.Checked = App.Settings.JobIssueFlagNotClickingReferrals;
			FlagNotClickingReferralsPeriodDaysTextBox.Text = App.Settings.JobIssueNotClickingReferralsDaysThreshold.ToString(CultureInfo.CurrentCulture);

			FlagSearchWithoutInviteCheckBox.Checked = App.Settings.JobIssueFlagSearchingNotInviting;
			FlagSearchWithoutInviteTextBox.Text = App.Settings.JobIssueSearchesWithoutInviteThreshold.ToString(CultureInfo.CurrentCulture);
			FlagSearchWithoutInviteDaysTextBox.Text = App.Settings.JobIssueSearchesWithoutInviteDaysThreshold.ToString(CultureInfo.CurrentCulture);

			FlagJobClosingDateRefreshedCheckBox.Checked = App.Settings.JobIssueFlagClosingDateRefreshed;

			FlagJobClosedEarlyCheckBox.Checked = App.Settings.JobIssueFlagJobClosedEarly;
			FlagJobClosedEarlyTextBox.Text = App.Settings.JobIssuePostClosedEarlyDaysThreshold.ToString(CultureInfo.CurrentCulture);

			FlagJobAfterHiringPeriodDaysCheckBox.Checked = App.Settings.JobIssueFlagJobAfterHiring;
			FlagJobAfterHiringPeriodDaysTextBox.Text = App.Settings.JobIssueHiredDaysElapsedThreshold.ToString(CultureInfo.CurrentCulture);

			FlagContactNegativeFeedbackCheckBox.Checked = App.Settings.JobIssueFlagNegativeFeedback;
			FlagContactPositiveFeedbackCheckBox.Checked = App.Settings.JobIssueFlagPositiveFeedback;

		  if (App.Settings.LaborInsightAccess)
		    LaborInsightAccessYes.Checked = true;
		  else
		    LaborInsightAccessNo.Checked = true;

			CandidateTypeTextBox.Text = HtmlLocalise("Global.CandidateType", "#CANDIDATETYPE#");
			CandidateTypePluralTextBox.Text = HtmlLocalise("Global.CandidateTypes", "#CANDIDATETYPES#");

			BusinessTerminologyTextBox.Text = HtmlLocalise("Global.Business", "#BUSINESS#");
			BusinessTerminologyPluralTextBox.Text = HtmlLocalise("Global.Businesses", "#BUSINESSES#");

            EnableJobSeekerActivitiesCheckBox.Checked = App.Settings.EnableJobSeekerActivitiesServices;
            EnableHiringManagerActivitiesCheckBox.Checked = App.Settings.EnableHiringManagerActivitiesServices;

			EnableOfficesCheckBox.Checked = App.Settings.OfficesEnabled;

			if (App.Settings.ReferralRequestApprovalQueueOfficeFilterType)
				JobPostingAssignOfficeRadioButton.Checked = true;
			else
				JobseekersAssignOfficeRadioButton.Checked = true;
    }

    /// <summary>
    /// Sets visibility of controls based on the theme
    /// </summary>
    private void ApplyTheme()
    {
      var visibility = (App.Settings.Theme == FocusThemes.Workforce);

      FlagDefaultPlaceHolder.Visible = visibility;
      LaborInsightPlaceHolder.Visible = visibility;

      TextDefaultsPlaceHolder.Visible = !Visible;

			FlagExpiredAlienCertificationRow.Visible = visibility;
      JobOrderDefaultsPlaceHolder.Visible = App.Settings.TalentModulePresent;
    }

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
      FlagSeekerNotLoggedOnLabel.Text = HtmlLocalise("FlagSeekerNotLoggedOn.Label", "Seeker not logging in");
      FlagSeekerNotLoggedOnDaysValidator.Text = HtmlLocalise("FlagSeekerNotLoggedOnDays.Required", "Please enter a number of days");
      FlagSeekerNotLoggedOnDaysRegex.Text = HtmlLocalise("FlagSeekerNotLoggedOnDays.Range", "Please enter a valid number of days");

      FlagSeekerNotClickingLeadsLabel.Text = HtmlLocalise("FlagSeekerNotClickingLeads.Label", "Seeker not clicking on leads");

      FlagSeekerNotSearchingJobsLabel.Text = HtmlLocalise("FlagSeekerNotSearchingJobs.Label", "Seeker not searching jobs");
      FlagSeekerNotSearchingJobsCountValidator.Text = HtmlLocalise("FlagSeekerNotSearchingJobsCount.Required", "Please enter a number of jobs");
      FlagSeekerNotSearchingJobsCountRegex.Text = HtmlLocalise("FlagSeekerNotSearchingJobsCount.Range", "Please enter a valid number of jobs");
      FlagSeekerNotSearchingJobsDaysValidator.Text = HtmlLocalise("FlagSeekerNotSearchingJobsDays.Required", "Please enter a number of days");
      FlagSeekerNotSearchingJobsDaysRegex.Text = HtmlLocalise("FlagSeekerNotSearchingJobsDays.Range", "Please enter a valid number of days");

      FlagSeekerRejectingOffersLabel.Text = HtmlLocalise("FlagSeekerRejectingOffers.Label", "Seeker rejecting job offers");
      FlagSeekerRejectingOffersCountValidator.Text = HtmlLocalise("FlagSeekerRejectingOffersCount.Required", "Please enter a number of offers");
      FlagSeekerRejectingOffersCountRegex.Text = HtmlLocalise("FlagSeekerRejectingOffersCount.Range", "Please enter a valid number of offers");
      FlagSeekerRejectingOffersDaysValidator.Text = HtmlLocalise("FlagSeekerRejectingOffersDays.Required", "Please enter a number of days");
      FlagSeekerRejectingOffersDaysRegex.Text = HtmlLocalise("FlagSeekerRejectingOffersDays.Range", "Please enter a valid number of days");

      FlagSeekerNotReportingInterviewsLabel.Text = HtmlLocalise("FlagSeekerNotReportingInterviews.Label", "Seeker not reporting for interviews");
      FlagSeekerNotReportingInterviewsCountValidator.Text = HtmlLocalise("FlagSeekerNotReportingInterviewsCount.Required", "Please enter a number of interviews");
      FlagSeekerNotReportingInterviewsCountRegex.Text = HtmlLocalise("FlagSeekerNotReportingInterviewsCount.Range", "Please enter a valid number of interviews");
      FlagSeekerNotReportingInterviewsDaysValidator.Text = HtmlLocalise("FlagSeekerNotReportingInterviewsDays.Required", "Please enter a number of days");
      FlagSeekerNotReportingInterviewsDaysRegex.Text = HtmlLocalise("FlagSeekerNotReportingInterviewsDays.Range", "Please enter a valid number of days");
      
      FlagSeekerNotRespondingInvitesLabel.Text = HtmlLocalise("FlagSeekerNotRespondingInvites.Label", "Seeker not responding to #BUSINESS#:LOWER invitations");
      FlagSeekerNotRespondingInvitesCountValidator.Text = HtmlLocalise("FlagSeekerNotRespondingInvitesCount.Required", "Please enter a number of invites");
      FlagSeekerNotRespondingInvitesCountRegex.Text = HtmlLocalise("FlagSeekerNotRespondingInvitesCount.Range", "Please enter a valid number of invites");
      FlagSeekerNotRespondingInvitesDaysValidator.Text = HtmlLocalise("FlagSeekerNotRespondingInvitesDays.Required", "Please enter a number of days");
      FlagSeekerNotRespondingInvitesDaysRegex.Text = HtmlLocalise("FlagSeekerNotRespondingInvitesDays.Range", "Please enter a valid number of days");
      FlagSeekerNotRespondingInvitesGraceValidator.Text = HtmlLocalise("FlagSeekerNotRespondingInvitesGrace.Required", "Please enter a grace period");
      FlagSeekerNotRespondingInvitesGraceRegex.Text = HtmlLocalise("FlagSeekerNotRespondingInvitesGrace.Range", "Please enter a valid grace period");

      FlagLowQualityMatchesLabel.Text = HtmlLocalise("FlagLowQualityMatches", "Showing low-quality matches");
      FlagLowQualityMatchesCountValidator.Text = HtmlLocalise("FlagLowQualityMatchesCount.Required", "Please enter a number of matches");
      FlagLowQualityMatchesCountRegex.Text = HtmlLocalise("FlagLowQualityMatchesCount.Range", "Please enter a valid number of matches");
      FlagLowQualityMatchesDaysValidator.Text = HtmlLocalise("FlagLowQualityMatchesDays.Required", "Please enter a number of days");
      FlagLowQualityMatchesDaysRegex.Text = HtmlLocalise("FlagLowQualityMatchesDays.Range", "Please enter a valid number of days");

      FlagLowQualityResumeLabel.Text = HtmlLocalise("FlagLowQualityResume", "Posting poor-quality resume");

      FlagPostHireFollowUpLabel.Text = HtmlLocalise("FlagPostHireFollowUp.Label", "Suggesting post-hire follow-up");

		  InappropriateEmailLabel.Text = HtmlLocalise("InappropriateEmailLabel", "Inappropriate Email Address");
			FlagPositiveFeedbackLabel.Text = HtmlLocalise("FlagPositiveFeedbackLabel", "Giving positive survey responses");
			FlagNegativeFeedbackLabel.Text = HtmlLocalise("FlagNegativeFeedbackLabel", "Giving negative survey responses");
      FlagPotentialMSFWLabel.Text = HtmlLocalise("FlagPotentialMSFWLabel", "Seeker is potential MSFW");
			FlagExpiredAlienCertificationLabel.Text = HtmlLocalise( "FlagExpiredAlienCertificationLabel", "#CANDIDATETYPE# with expired alien certification registration date" );

			// Job order issues
			FlagLowMatchesLabel.Text = HtmlLocalise("FlagLowMatches.Label", "Low number of high quality matches");
			FlagLowMatchesCountValidator.Text = HtmlLocalise("FlagLowMatchesCount.Required", "Please enter a minimum number of matches");
			FlagLowMatchesCountRegex.Text = HtmlLocalise("FlagLowMatchesCount.Range", "Please enter a valid number of matches");
			FlagLowMatchesMinimumStarsValidator.Text = HtmlLocalise("FlagLowMatchesMinimumStars.Required", "Please enter minimum stars for match");
			FlagLowMatchesMinimumStarsRegex.Text = HtmlLocalise("FlagLowMatchesMinimumStars.Range", "Please enter a valid number of minimum stars for match");
			
			FlagLowReferralsLabel.Text = HtmlLocalise("FlagLowReferrals.Label", "Low referral activity");
			FlagLowReferralsCountValidator.Text = HtmlLocalise("FlagLowReferralsCount.Required", "Please enter a minimum number of referrals");
			FlagLowReferralsCountRegex.Text = HtmlLocalise("FlagLowReferralsCount.Range", "Please enter a valid minimum number of referrals");
			FlagLowReferralsPeriodDaysValidator.Text = HtmlLocalise("FlagLowReferralsPeriodDays.Required", "Please enter a number of days");
			FlagLowReferralsPeriodDaysRegex.Text = HtmlLocalise("FlagLowReferralsPeriodDays.Range", "Please enter a valid number of days");

			FlagNotClickingReferralsLabel.Text = HtmlLocalise("FlagNotClickingReferrals.Label", "Not clicking on referred applicants");
			FlagNotClickingReferralsPeriodDaysValidator.Text = HtmlLocalise("FlagNotClickingReferralsPeriodDays.Required", "Please enter a number of days");
			FlagNotClickingReferralsPeriodDaysRegex.Text = HtmlLocalise("FlagNotClickingReferralsPeriodDays.Range", "Please enter a valid number of days");

			FlagSearchWithoutInviteLabel.Text = HtmlLocalise("FlagSearchWithoutInvite.Label", "Searching but not inviting");
			FlagSearchWithoutInviteValidator.Text = HtmlLocalise("FlagSearchWithoutInvite.Required", "Please enter a minimum number of searches");
			FlagSearchWithoutInviteRegex.Text = HtmlLocalise("FlagSearchWithoutInvite.Range", "Please enter a valid minimum number of searches");
			FlagSearchWithoutInviteDaysValidator.Text = HtmlLocalise("FlagSearchWithoutInviteDays.Required", "Please enter a number of days");
			FlagSearchWithoutInviteDaysRegex.Text = HtmlLocalise("FlagSearchWithoutInviteDays.Range", "Please enter a valid number of days");

			FlagJobClosingDateRefreshedLabel.Text = HtmlLocalise("FlagJobClosingDateRefreshed.Label", "Refreshing closing date");

			FlagJobClosedEarlyLabel.Text = HtmlLocalise("FlagJobClosedEarly.Label", "Pending closing date");
			FlagJobClosedEarlyValidator.Text = HtmlLocalise("FlagJobClosedEarly.Required", "Please enter a number of days");
			FlagJobClosedEarlyRegex.Text = HtmlLocalise("FlagJobClosedEarly.Range", "Please enter a valid number of days");

			FlagJobAfterHiringPeriodDaysLabel.Text = HtmlLocalise("FlagJobAfterHiringPeriodDays.Label", "Suggesting post-hire follow-up");
			FlagJobAfterHiringPeriodDaysValidator.Text = HtmlLocalise("FlagJobAfterHiringPeriodDays.Required", "Please enter a number of days");
			FlagJobAfterHiringPeriodDaysRegex.Text = HtmlLocalise("FlagJobAfterHiringPeriodDays.Range", "Please enter a valid number of days");

			FlagContactNegativeFeedbackLabel.Text = HtmlLocalise("FlagContactNegativeFeedback.Label", "Giving negative survey responses");

			FlagContactPositiveFeedbackLabel.Text = HtmlLocalise("FlagContactPositiveFeedback.Label", "Giving positive survey responses");

      SaveChangesTopButton.Text = SaveChangesBottomButton.Text = CodeLocalise("SaveChangesButton.Text.NoEdit", "Save changes");
			ResetColoursButton.Text = CodeLocalise("ResetColoursButton.Text.NoEdit", "Reset colors");
			AssistFooterLogoCheckBox.Text = CodeLocalise("FooterLogoCheckBox.Text", "Display Burning Glass logo in footers");
			if (App.Settings.Theme == FocusThemes.Education)
				JobDevelopmentFlagsHeaderPanel.Visible = RecentPlacementFlagsPanel.Visible = false;

      CandidateTypeValidator.Text = CandidateTypePluralValidator.Text = HtmlLocalise("CandidateType.Required", "Please enter some text");
			DarkColourValidator.ErrorMessage = CodeLocalise("DarkColour.Required", "Please Select a Color");
			LightColourValidator.ErrorMessage = CodeLocalise("LightColour.Required", "Please Select a Color");

			BusinessTerminologyRequired.Text = BusinessTerminologyPluralRequired.Text = HtmlLocalise("BusinessTerminology.Required", "Please enter some text");
            
            //Manage Activities defaults
            EnableHiringManagerActivitiesLabel.Text = HtmlLocalise("EnableHiringManagerActivities.Label", "Enable Hiring Manager Activities/Services features.");
            EnableJobSeekerActivitiesLabel.Text = HtmlLocalise("EnableJobSeekerActivities.Label", "Enable Job Seeker Activities/Services features. ");
		}

		private void ApplyBranding()
		{
			FlagDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			FlagDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			FlagDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			JobOrderDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobOrderDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobOrderDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ColourDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ColourDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ColourDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			LogoDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LogoDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			LogoDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			JobDevelopmentFlagsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			JobDevelopmentCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			JobDevelopmentCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			SearchDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SearchDefaultsCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			SearchDefaultsCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

            ManageActivitiesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
            ManageActivitiesCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
            ManageActivitiesCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			LaborInsightDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			LaborInsightDefaultsCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			LaborInsightDefaultsCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			TextDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			TextDefaultsCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			TextDefaultsCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			OfficeHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			OfficeCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			OfficeCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ReferralRequestApprovalQueueHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ReferralRequestApprovalQueueExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ReferralRequestApprovalQueueExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Binds the timeframe date within the drop down.
		/// </summary>
		private void BindPlacementTimeframeDropDown()
		{
			PlacementTimeframeDropDown.Items.Clear();
			PlacementTimeframeDropDown.Items.Add("7");
			PlacementTimeframeDropDown.Items.Add("14");
			PlacementTimeframeDropDown.Items.Add("30");
			PlacementTimeframeDropDown.Items.Add("60");
			PlacementTimeframeDropDown.Items.Add("90");
			PlacementTimeframeDropDown.Items.Add("120");
		}

		/// <summary>
		/// Binds the timeframe date within the drop down.
		/// </summary>
		/// 
		private void BindFlagLowMatchesPeriodDaysDropDown()
		{
			var days = new[] {"3", "5", "10", "15"};
			FlagLowMatchesPeriodDaysDropDown.Items.Add(App.Settings.JobIssueMatchDaysThreshold.ToString(CultureInfo.CurrentCulture));

			foreach (var day in days.Where(day => !day.Equals(App.Settings.JobIssueMatchDaysThreshold.ToString(CultureInfo.CurrentCulture))))
			{
				FlagLowMatchesPeriodDaysDropDown.Items.Add(day);
			}
		}

		/// <summary>
		/// Binds the minimum stars drop down.
		/// </summary>
		private void BindMinimumStarsDropDown()
		{
			MinimumStarsDropDownList.Items.Clear();
			MinimumStarsDropDownList.Items.Add("5");
			MinimumStarsDropDownList.Items.Add("4");
			MinimumStarsDropDownList.Items.Add("3");
			MinimumStarsDropDownList.Items.Add("2");
			MinimumStarsDropDownList.Items.Add("1");

			var minimumStars = App.Settings.RecommendedMatchesMinimumStars;
			if (minimumStars == 0)
			{
				AllJobSeekersRadioButton.Checked = true;
				MinimumStarsDropDownList.SelectedIndex = 0;
			}
			else
			{
				JobSeekersWithXStarsRadioButton.Checked = true;
				MinimumStarsDropDownList.SelectedIndex = 5 - minimumStars;
			}
		}

    /// <summary>
    /// Binds the low quality matches drop down
    /// </summary>
    private void BindLowQualityMatchesDropDown()
    {
      //FlagLowQualityMatchesDropDown
      for (var minStars = 1; minStars < 5; minStars++)
        FlagLowQualityMatchesDropDown.Items.Add(new ListItem(HtmlLocalise("Stars.NoEdit", "{0} stars", minStars), minStars.ToString(CultureInfo.InvariantCulture)));
    }

    /// <summary>
    /// Gets the new configuration settings based on the fields entered on the page
    /// </summary>
    /// <param name="configurationItems">A list of configurations items to be saved</param>
    private void UnbindFlagDefaultSettings(List<ConfigurationItemDto> configurationItems)
    {
      var updates = new List<ConfigurationItemUpdate>
      {
				#region Not Logging In

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotLoggingInEnabled,
          NewValue = FlagSeekerNotLoggedOnCheckBox.Checked.ToString(),
          OldValue = App.Settings.NotLoggingInEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotLoggingInDaysThreshold,
          NewValue = GetNewValue(FlagSeekerNotLoggedOnDaysTextBox, FlagSeekerNotLoggedOnCheckBox, App.Settings.NotLoggingInDaysThreshold),
          OldValue = App.Settings.NotLoggingInDaysThreshold.ToString(CultureInfo.InvariantCulture)
        },

				#endregion

				#region Not Clicking Leads

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotClickingLeadsEnabled,
          NewValue = FlagSeekerNotClickingLeadsCheckBox.Checked.ToString(),
          OldValue = App.Settings.NotClickingLeadsEnabled.ToString()
        },
				new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotClickingLeadsCountThreshold,
          NewValue = GetNewValue(FlagSeekerNotClickingLeadsCountTextBox, FlagSeekerNotClickingLeadsCheckBox, App.Settings.NotClickingLeadsCountThreshold),
          OldValue = App.Settings.NotClickingLeadsCountThreshold.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotClickingLeadsDaysThreshold,
          NewValue = GetNewValue(FlagSeekerNotClickingLeadsDaysTextBox, FlagSeekerNotClickingLeadsCheckBox, App.Settings.NotClickingLeadsDaysThreshold),
          OldValue = App.Settings.NotClickingLeadsDaysThreshold.ToString(CultureInfo.InvariantCulture)
        },

				#endregion

				#region Not Searching Jobs

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotSearchingJobsEnabled,
          NewValue = FlagSeekerNotSearchingJobsCheckBox.Checked.ToString(),
          OldValue = App.Settings.NotSearchingJobsEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotSearchingJobsCountThreshold,
          NewValue = GetNewValue(FlagSeekerNotSearchingJobsCountTextBox, FlagSeekerNotSearchingJobsCheckBox, App.Settings.NotSearchingJobsCountThreshold),
          OldValue = App.Settings.NotSearchingJobsCountThreshold.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotSearchingJobsDaysThreshold,
          NewValue = GetNewValue(FlagSeekerNotSearchingJobsDaysTextBox, FlagSeekerNotSearchingJobsCheckBox, App.Settings.NotSearchingJobsDaysThreshold),
          OldValue = App.Settings.NotSearchingJobsDaysThreshold.ToString(CultureInfo.InvariantCulture)
        },

				#endregion
        
				#region Rejecting Job Offers

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.RejectingJobOffersEnabled,
          NewValue = FlagSeekerRejectingOffersCheckBox.Checked.ToString(),
          OldValue = App.Settings.RejectingJobOffersEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.RejectingJobOffersCountThreshold,
          NewValue = GetNewValue(FlagSeekerRejectingOffersCountTextBox, FlagSeekerRejectingOffersCheckBox, App.Settings.RejectingJobOffersCountThreshold),
          OldValue = App.Settings.RejectingJobOffersCountThreshold.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.RejectingJobOffersDaysThreshold,
          NewValue = GetNewValue(FlagSeekerRejectingOffersDaysTextBox, FlagSeekerRejectingOffersCheckBox, App.Settings.RejectingJobOffersDaysThreshold),
          OldValue = App.Settings.RejectingJobOffersDaysThreshold.ToString(CultureInfo.InvariantCulture)
        },

				#endregion

				#region Rejecting Not Reporting for Interviews

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotReportingForInterviewEnabled,
          NewValue = FlagSeekerNotReportingInterviewsCheckBox.Checked.ToString(),
          OldValue = App.Settings.NotReportingForInterviewEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotReportingForInterviewCountThreshold,
          NewValue = GetNewValue(FlagSeekerNotReportingInterviewsCountTextBox, FlagSeekerNotReportingInterviewsCheckBox, App.Settings.NotReportingForInterviewCountThreshold),
          OldValue = App.Settings.NotReportingForInterviewCountThreshold.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotReportingForInterviewDaysThreshold,
          NewValue = GetNewValue(FlagSeekerNotReportingInterviewsDaysTextBox, FlagSeekerNotReportingInterviewsCheckBox, App.Settings.NotReportingForInterviewDaysThreshold),
          OldValue = App.Settings.NotReportingForInterviewDaysThreshold.ToString(CultureInfo.InvariantCulture)
        },

				#endregion

				#region Rejecting Not Responding to Employer Invites

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotRespondingToEmployerInvitesEnabled,
          NewValue = FlagSeekerNotRespondingInvitesCheckBox.Checked.ToString(),
          OldValue = App.Settings.NotRespondingToEmployerInvitesEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotRespondingToEmployerInvitesCountThreshold,
          NewValue = GetNewValue(FlagSeekerNotRespondingInvitesCountTextBox, FlagSeekerNotRespondingInvitesCheckBox, App.Settings.NotRespondingToEmployerInvitesCountThreshold),
          OldValue = App.Settings.NotRespondingToEmployerInvitesCountThreshold.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotRespondingToEmployerInvitesDaysThreshold,
          NewValue = GetNewValue(FlagSeekerNotRespondingInvitesDaysTextBox, FlagSeekerNotRespondingInvitesCheckBox, App.Settings.NotRespondingToEmployerInvitesDaysThreshold),
          OldValue = App.Settings.NotRespondingToEmployerInvitesDaysThreshold.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NotRespondingToEmployerInvitesGracePeriodDays,
          NewValue = GetNewValue(FlagSeekerNotRespondingInvitesGraceTextBox, FlagSeekerNotRespondingInvitesCheckBox, App.Settings.NotRespondingToEmployerInvitesGracePeriodDays),
          OldValue = App.Settings.NotRespondingToEmployerInvitesGracePeriodDays.ToString(CultureInfo.InvariantCulture),
        },

				#endregion

				#region Low Quality Matches

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.ShowingLowQualityMatchesEnabled,
          NewValue = FlagLowQualityMatchesCheckBox.Checked.ToString(),
          OldValue = App.Settings.ShowingLowQualityMatchesEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.ShowingLowQualityMatchesCountThreshold,
          NewValue = GetNewValue(FlagLowQualityMatchesCountTextBox, FlagLowQualityMatchesCheckBox, App.Settings.ShowingLowQualityMatchesCountThreshold),
          OldValue = App.Settings.ShowingLowQualityMatchesCountThreshold.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.ShowingLowQualityMatchesMinStarThreshold,
          NewValue = GetNewValue(FlagLowQualityMatchesDropDown, FlagLowQualityMatchesCheckBox, App.Settings.ShowingLowQualityMatchesMinStarThreshold),
          OldValue = App.Settings.ShowingLowQualityMatchesMinStarThreshold.ToString(CultureInfo.InvariantCulture)
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.ShowingLowQualityMatchesDaysThreshold,
          NewValue = GetNewValue(FlagLowQualityMatchesDaysTextBox, FlagLowQualityMatchesCheckBox, App.Settings.ShowingLowQualityMatchesDaysThreshold),
          OldValue = App.Settings.ShowingLowQualityMatchesDaysThreshold.ToString(CultureInfo.InvariantCulture)
        },

				#endregion

				#region Low Quality Resumes

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.PostingLowQualityResumeEnabled,
          NewValue = FlagLowQualityResumeCheckBox.Checked.ToString(),
          OldValue = App.Settings.PostingLowQualityResumeEnabled.ToString()
        },
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.PostingLowQualityResumeWordCountThreshold,
          NewValue = GetNewValue(FlagLowQualityResumeWordCountTextBox, FlagLowQualityResumeCheckBox, App.Settings.PostingLowQualityResumeWordCountThreshold),
          OldValue = App.Settings.PostingLowQualityResumeWordCountThreshold.ToString(CultureInfo.InvariantCulture)
        },
				new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.PostingLowQualityResumeSkillCountThreshold,
          NewValue = GetNewValue(FlagLowQualityResumeSkillCountTextBox, FlagLowQualityResumeCheckBox, App.Settings.PostingLowQualityResumeSkillCountThreshold),
          OldValue = App.Settings.PostingLowQualityResumeSkillCountThreshold.ToString(CultureInfo.InvariantCulture)
        },

				#endregion

				#region Post-Hire follow up

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.PostHireFollowUpEnabled,
          NewValue = FlagPostHireFollowUpCheckBox.Checked.ToString(),
          OldValue = App.Settings.PostHireFollowUpEnabled.ToString()
        },

				#endregion

        #region Inappropriate Email

        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.InappropriateEmailAddressCheckEnabled,
          NewValue = InappropriateEmailCheckBox.Checked.ToString(),
          OldValue = App.Settings.InappropriateEmailAddressCheckEnabled.ToString()
        },

        #endregion

				#region Negative feedback
				
				new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.NegativeFeedbackCheckEnabled,
          NewValue = FlagNegativeFeedbackCheckBox.Checked.ToString(),
          OldValue = App.Settings.NegativeFeedbackCheckEnabled.ToString()
        },

				#endregion

				#region Positive feedback

				new ConfigurationItemUpdate
				{
					Key = Constants.ConfigurationItemKeys.PositiveFeedbackCheckEnabled,
					NewValue = FlagPositiveFeedbackCheckbox.Checked.ToString(),
					OldValue = App.Settings.PositiveFeedbackCheckEnabled.ToString()
				},

				#endregion

				#region Candidate Issues

				new ConfigurationItemUpdate
				{
					Key = Constants.ConfigurationItemKeys.ExpiredAlienCertificationEnabled,
					NewValue = FlagExpiredAlienCertificationCheckBox.Checked.ToString(),
					OldValue = App.Settings.ExpiredAlienCertificationEnabled.ToString()
				},

				#endregion

        #region MSFW

				new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.JobIssueFlagPotentialMSFW,
          NewValue = FlagPotentialMSFWCheckBox.Checked.ToString(),
          OldValue = App.Settings.JobIssueFlagPotentialMSFW.ToString()
        }

        #endregion
				
      };

      configurationItems.AddRange(updates.Where(u => u.OldValue != u.NewValue).Select(u => new ConfigurationItemDto
      {
        Key = u.Key,
        Value = u.NewValue
      }));
    }

		/// <summary>
    /// Gets the new job order issue configuration settings based on the fields entered on the page
    /// </summary>
    /// <param name="configurationItems">A list of configurations items to be saved</param>
		private void UnbindJobOrderDefaultSettings(List<ConfigurationItemDto> configurationItems)
		{
			var updates = new List<ConfigurationItemUpdate>
			{

				#region Low matches

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueFlagLowMinimumStarMatches,
						NewValue = FlagLowMatchesCheckBox.Checked.ToString(),
						OldValue = App.Settings.JobIssueFlagLowMinimumStarMatches.ToString()
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueMinimumMatchCountThreshold,
						NewValue =
							GetNewValue(FlagLowMatchesCountTextBox, FlagLowMatchesCheckBox,
													App.Settings.JobIssueMinimumMatchCountThreshold),
						OldValue = App.Settings.JobIssueMinimumMatchCountThreshold.ToString(CultureInfo.InvariantCulture)
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueMatchMinimumStarsThreshold,
						NewValue =
							GetNewValue(FlagLowMatchesMinimumStarsTextBox, FlagLowMatchesCheckBox,
													App.Settings.JobIssueMatchMinimumStarsThreshold),
						OldValue = App.Settings.JobIssueMatchMinimumStarsThreshold.ToString(CultureInfo.InvariantCulture)
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueMatchDaysThreshold,
						NewValue =
							GetNewValue(FlagLowMatchesPeriodDaysDropDown, FlagLowMatchesCheckBox,
													App.Settings.JobIssueMatchDaysThreshold),
						OldValue = App.Settings.JobIssueMatchDaysThreshold.ToString(CultureInfo.InvariantCulture)
					},

				#endregion

				#region Low referrals

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueFlagLowReferrals,
						NewValue = FlagLowReferralsCheckBox.Checked.ToString(),
						OldValue = App.Settings.JobIssueFlagLowReferrals.ToString()
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueMinimumReferralsCountThreshold,
						NewValue =
							GetNewValue(FlagLowReferralsCountTextBox, FlagLowReferralsCheckBox,
													App.Settings.JobIssueMinimumReferralsCountThreshold),
						OldValue =
							App.Settings.JobIssueMinimumReferralsCountThreshold.ToString(CultureInfo.InvariantCulture)
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueReferralCountDaysThreshold,
						NewValue =
							GetNewValue(FlagLowReferralsPeriodDaysTextBox, FlagLowReferralsCheckBox,
													App.Settings.JobIssueReferralCountDaysThreshold),
						OldValue = App.Settings.JobIssueReferralCountDaysThreshold.ToString(CultureInfo.InvariantCulture)
					},

				#endregion

				#region Not clicking referrals

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueFlagNotClickingReferrals,
						NewValue = FlagNotClickingReferralsCheckBox.Checked.ToString(),
						OldValue = App.Settings.JobIssueFlagNotClickingReferrals.ToString()
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueNotClickingReferralsDaysThreshold,
						NewValue =
							GetNewValue(FlagNotClickingReferralsPeriodDaysTextBox, FlagNotClickingReferralsCheckBox,
													App.Settings.JobIssueNotClickingReferralsDaysThreshold),
						OldValue =
							App.Settings.JobIssueNotClickingReferralsDaysThreshold.ToString(CultureInfo.InvariantCulture)
					},

				#endregion

				#region Searching not inviting
 
				new ConfigurationItemUpdate
					{
							Key = Constants.ConfigurationItemKeys.JobIssueFlagSearchingNotInviting,
							NewValue = FlagSearchWithoutInviteCheckBox.Checked.ToString(),
							OldValue = App.Settings.JobIssueFlagSearchingNotInviting.ToString()
						},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueSearchesWithoutInviteThreshold,
						NewValue =
							GetNewValue(FlagSearchWithoutInviteTextBox, FlagSearchWithoutInviteCheckBox,
													App.Settings.JobIssueSearchesWithoutInviteThreshold),
						OldValue =
							App.Settings.JobIssueSearchesWithoutInviteThreshold.ToString(CultureInfo.InvariantCulture)
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueSearchesWithoutInviteDaysThreshold,
						NewValue = GetNewValue(FlagSearchWithoutInviteDaysTextBox, FlagSearchWithoutInviteCheckBox, App.Settings.JobIssueSearchesWithoutInviteDaysThreshold),
						OldValue = App.Settings.JobIssueSearchesWithoutInviteDaysThreshold.ToString(CultureInfo.InvariantCulture)
					},

				#endregion
				
				#region Job closing date refreshed

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueFlagClosingDateRefreshed,
						NewValue = FlagJobClosingDateRefreshedCheckBox.Checked.ToString(),
						OldValue = App.Settings.JobIssueFlagClosingDateRefreshed.ToString()
					},

				#endregion

				#region Job closed early

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueFlagJobClosedEarly,
						NewValue = FlagJobClosedEarlyCheckBox.Checked.ToString(),
						OldValue = App.Settings.JobIssueFlagJobClosedEarly.ToString(CultureInfo.InvariantCulture)
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssuePostClosedEarlyDaysThreshold,
						NewValue =
							GetNewValue(FlagJobClosedEarlyTextBox, FlagJobClosedEarlyCheckBox,
													App.Settings.JobIssuePostClosedEarlyDaysThreshold),
						OldValue = App.Settings.JobIssuePostClosedEarlyDaysThreshold.ToString(CultureInfo.InvariantCulture)
					},

				#endregion

				#region Post hiring follow up

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueFlagJobAfterHiring,
						NewValue = FlagJobAfterHiringPeriodDaysCheckBox.Checked.ToString(),
						OldValue = App.Settings.JobIssueFlagJobAfterHiring.ToString()
					},

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueHiredDaysElapsedThreshold,
						NewValue =
							GetNewValue(FlagJobAfterHiringPeriodDaysTextBox, FlagJobAfterHiringPeriodDaysCheckBox,
													App.Settings.JobIssueHiredDaysElapsedThreshold),
						OldValue = App.Settings.JobIssueHiredDaysElapsedThreshold.ToString(CultureInfo.InvariantCulture)
					},

				#endregion

				#region Negative feedback

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueFlagNegativeFeedback,
						NewValue = FlagContactNegativeFeedbackCheckBox.Checked.ToString(),
						OldValue = App.Settings.JobIssueFlagNegativeFeedback.ToString()
					},

				#endregion

				#region Positive feedback

				new ConfigurationItemUpdate
					{
						Key = Constants.ConfigurationItemKeys.JobIssueFlagPositiveFeedback,
						NewValue = FlagContactPositiveFeedbackCheckBox.Checked.ToString(),
						OldValue = App.Settings.JobIssueFlagPositiveFeedback.ToString(CultureInfo.InvariantCulture)
					}

				#endregion

			};

			configurationItems.AddRange(updates.Where(u => u.OldValue != u.NewValue).Select(u => new ConfigurationItemDto
			{
				Key = u.Key,
				Value = u.NewValue
			}));

		}

    /// <summary>
    /// Gets the new configuration settings for Labor Insight access
    /// </summary>
    /// <param name="configurationItems">A list of configurations items to be saved</param>
    private void UnbindLaborInsightSettings(List<ConfigurationItemDto> configurationItems)
    {
      var updates = new List<ConfigurationItemUpdate>
      {
        // Not Logging In
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.LaborInsightAccess,
          NewValue = LaborInsightAccessYes.Checked.ToString(),
          OldValue = App.Settings.LaborInsightAccess.ToString()
        }
      };

      configurationItems.AddRange(updates.Where(u => u.OldValue != u.NewValue).Select(u => new ConfigurationItemDto
      {
        Key = u.Key,
        Value = u.NewValue
      }));
    }

    /// <summary>
    /// Gets the value entered in a text box
    /// </summary>
    /// <param name="textBox">The text box</param>
    /// <param name="associatedCheckBox">The check box associated with the drop-down</param>
    /// <param name="oldValue">The original value of the text box</param>
    /// <returns>The new value of the text box</returns>
    private string GetNewValue(TextBox textBox, CheckBox associatedCheckBox, int oldValue)
    {
      textBox.Text = textBox.Text.Trim();
      if (!associatedCheckBox.Checked)
        textBox.Text = oldValue.ToString(CultureInfo.InvariantCulture);

      return int.Parse(textBox.Text).ToString(CultureInfo.InvariantCulture);
    }

	  /// <summary>
	  /// Gets the value entered in a drop down
	  /// </summary>
	  /// <param name="dropDown">The drop down</param>
	  /// <param name="associatedCheckBox">The check box associated with the drop-down</param>
	  /// <param name="oldValue">The original value of the drop down</param>
	  /// <returns>The new value of the drop down</returns>
	  private string GetNewValue(DropDownList dropDown, CheckBox associatedCheckBox, int oldValue)
    {
      if (!associatedCheckBox.Checked)
        dropDown.SelectedValue = oldValue.ToString(CultureInfo.InvariantCulture);

      return dropDown.SelectedValue;
    }


    private struct ConfigurationItemUpdate
    {
      public string Key;
      public string OldValue;
      public string NewValue;
    }
	}
}
