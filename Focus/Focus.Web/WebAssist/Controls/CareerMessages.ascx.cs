﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Framework.Core;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class CareerMessages : UserControlBase
	{
    private EmailTemplateTypes? SelectedEmailTemplateType
    {
      get { return GetViewStateValue<EmailTemplateTypes?>("CareerMessages:SelectedEmailTemplateType"); }
      set { SetViewStateValue("CareerMessages:SelectedEmailTemplateType", value); }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        LocaliseUI();
      }
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    public void Bind()
    {
      BindEmailTemplateNameDropDown();
      EmailTemplateEditor.Bind(new EmailTemplateDto(), new Dictionary<string, string>());

      JobSeekerInactivityEmailYes.Checked = App.Settings.JobSeekerSendInactivityEmail;
      JobSeekerInactivityEmailNo.Checked = !App.Settings.JobSeekerSendInactivityEmail;
      JobSeekerEmailLimitTextBox.Text = App.Settings.JobSeekerInactivityEmailLimit.ToString(CultureInfo.CurrentUICulture);
      JobSeekerEmailGraceTextBox.Text = App.Settings.JobSeekerInactivityEmailGrace.ToString(CultureInfo.CurrentUICulture);

      JobSeekerInactivityPanel.Visible = false;
    }

    /// <summary>
    /// Handles the SelectedIndexChanged event of the EmailTemplateNameDropDown control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void EmailTemplateNameDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (EmailTemplateNameDropDown.SelectedIndex == 0)
      {
        SelectedEmailTemplateType = null;
        EmailTemplateEditor.Bind(new EmailTemplateDto(), new Dictionary<string, string>());
      }
      else
      {
        SelectedEmailTemplateType = EmailTemplateNameDropDown.SelectedValueToEnum<EmailTemplateTypes>();
        var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplate(SelectedEmailTemplateType.Value);
        var messageVariables = GetMessageVariables(SelectedEmailTemplateType.Value);
        EmailTemplateEditor.Bind(emailTemplate, messageVariables);
      }

      JobSeekerInactivityPanel.Visible = (SelectedEmailTemplateType == EmailTemplateTypes.CareerJobSeekerInactivity);
    }

    /// <summary>
    /// Handles the Click event of the SaveChangesButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveChangesButton_Click(object sender, EventArgs e)
    {
      if (!SelectedEmailTemplateType.HasValue) return;

      var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplate(SelectedEmailTemplateType.Value);

      if (emailTemplate.IsNull()) emailTemplate = new EmailTemplateDto { EmailTemplateType = SelectedEmailTemplateType.Value };

      EmailTemplateEditor.UnBind(ref emailTemplate);

      ServiceClientLocator.CoreClient(App).SaveEmailTemplate(emailTemplate);

      if (JobSeekerInactivityPanel.Visible)
      {
        var configurationItems = new List<ConfigurationItemDto>();
        
        UnbindJobSeekerInactivitySettings(configurationItems);

	      if (configurationItems.IsNotNullOrEmpty())
	      {
		      ServiceClientLocator.CoreClient(App).SaveConfigurationItems(configurationItems);
		      App.RefreshSettings();
	      }

	      if (!JobSeekerInactivityEmailYes.Checked)
        {
          JobSeekerEmailLimitTextBox.Text = App.Settings.JobSeekerInactivityEmailLimit.ToString(CultureInfo.CurrentUICulture);
          JobSeekerEmailGraceTextBox.Text = App.Settings.JobSeekerInactivityEmailGrace.ToString(CultureInfo.CurrentUICulture);
        }
      }

      Confirmation.Show(CodeLocalise("CareerMessageUpdatedConfirmation.Title", "#FOCUSCAREER# message updated"),
                               CodeLocalise("CareerMessageUpdatedConfirmation.Body", "The message has been updated."),
                               CodeLocalise("CloseModal.Text", "OK"));
    }

    /// <summary>
    /// Binds the email template name drop down.
    /// </summary>
    private void BindEmailTemplateNameDropDown()
    {
      EmailTemplateNameDropDown.Items.Clear();

			if (App.Settings.Theme == FocusThemes.Workforce)
        EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.CareerJobSeekerInactivity, "#FOCUSCAREER# job seeker inactivity");

      EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.CareerPasswordReset, "#FOCUSCAREER# password reset");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.NewApplicantNotification, "New Applicant Notification");

	    if (App.Settings.TalentModulePresent)
				EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.TalentPostingApplicationtViaEmail, "#FOCUSTALENT# posting application via email");

			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.JobSeekerNoticeOfOnHoldJob, "Job Seeker Notice of On-hold Job");

	    if (App.Settings.Theme == FocusThemes.Workforce)
	    {
		    EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.VeteranPriorityPosting, "Veteran priority of service job alert");
		    EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.ExpiredAlienRegistration, "Expired Alien Registration Notification");
	    }

	    EmailTemplateNameDropDown.Items.AddLocalisedTopDefault("EmailTemplateName.TopDefault.NoEdit", "- select message -");
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      SaveChangesButton.Text = CodeLocalise("SaveChangesButton.Text.NoEdit", "Save changes");

      JobSeekerEmailLimitValidator.Text = JobSeekerEmailGraceValidator.Text = CodeLocalise("JobSeekerDays.Required", "Please enter a number of days");
      JobSeekerEmailLimitRegex.Text = JobSeekerEmailGraceRegex.Text = CodeLocalise("JobSeekerDays.Range", "Please enter a valid number of days");
    }

    /// <summary>
    /// Gets the message variables.
    /// </summary>
    /// <param name="emailTemplateType">Type of the email template.</param>
    /// <returns></returns>
    private Dictionary<string, string> GetMessageVariables(EmailTemplateTypes emailTemplateType)
    {
      var dictionary = new Dictionary<string, string>();

      switch (emailTemplateType)
      {
        case EmailTemplateTypes.CareerPasswordReset:
          dictionary.Add(Constants.PlaceHolders.FocusCareerUrl, CodeLocalise("FocusCareerURL.Text", "#FOCUSCAREER# url"));
          dictionary.Add(Constants.PlaceHolders.FocusCareer, CodeLocalise("FocusCareer.Text", "#FOCUSCAREER#"));
          dictionary.Add(Constants.PlaceHolders.Url, CodeLocalise("ResetPassword.Text", "Reset password URL"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
          break;

        case EmailTemplateTypes.CareerJobSeekerInactivity:
          dictionary.Add(Constants.PlaceHolders.FocusCareerUrl, CodeLocalise("FocusCareerURL.Text", "#FOCUSCAREER# url"));
          dictionary.Add(Constants.PlaceHolders.FocusCareer, CodeLocalise("FocusCareer.Text", "#FOCUSCAREER#"));
          dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
          break;

        case EmailTemplateTypes.CareerAccountRegistration:
          dictionary.Add(Constants.PlaceHolders.FocusCareer, CodeLocalise("FocusCareer.Text", "#FOCUSCAREER#"));
          dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
          dictionary.Add(Constants.PlaceHolders.PinNumber, CodeLocalise("PinNumber.Text", "Pin number"));
          dictionary.Add(Constants.PlaceHolders.AuthenticateUrl, CodeLocalise("AuthenticateUrl.Text", "Authentication url"));
          break;

        case EmailTemplateTypes.TalentPostingApplicationtViaEmail:
          dictionary.Add(Constants.PlaceHolders.FocusCareer, CodeLocalise("FocusCareer.Text", "#FOCUSCAREER#"));
          dictionary.Add(Constants.PlaceHolders.FocusTalent, CodeLocalise("FocusTalent.Text", "#FOCUSTALENT#"));
          dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "Job link"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "Job title"));
          dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
          break;

        case EmailTemplateTypes.VeteranPriorityPosting:
          dictionary.Add(Constants.PlaceHolders.FocusCareer, CodeLocalise("FocusCareer.Text", "#FOCUSCAREER#"));
          dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "Job link"));
          break;

				case EmailTemplateTypes.NewApplicantNotification:
					dictionary.Add(Constants.PlaceHolders.JobId, CodeLocalise("JobId.Text", "#POSTINGTYPE# id"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.FocusTalent, CodeLocalise("FocusTalent.Text", "#FOCUSTALENT#"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;
      }

      return dictionary;
    }

    /// <summary>
    /// Gets the new configuration settings for alerts based on the fields entered on the page
    /// </summary>
    /// <param name="configurationItems">A list of configurations items to be saved</param>
    private void UnbindJobSeekerInactivitySettings(List<ConfigurationItemDto> configurationItems)
    {
      var updates = new List<ConfigurationItemUpdate>
      {
        new ConfigurationItemUpdate
        {
          Key = Constants.ConfigurationItemKeys.JobSeekerSendInactivityEmail,
          NewValue = JobSeekerInactivityEmailYes.Checked.ToString(),
          OldValue = App.Settings.JobSeekerSendInactivityEmail.ToString()
        }
      };

      if (JobSeekerInactivityEmailYes.Checked)
      {
        updates.Add(
          new ConfigurationItemUpdate
          {
            Key = Constants.ConfigurationItemKeys.JobSeekerInactivityEmailLimit,
            NewValue = JobSeekerEmailLimitTextBox.Text.ToInt().ToString(),
            OldValue = App.Settings.JobSeekerInactivityEmailLimit.ToString(CultureInfo.InvariantCulture)
          });

        updates.Add(
          new ConfigurationItemUpdate
          {
            Key = Constants.ConfigurationItemKeys.JobSeekerInactivityEmailGrace,
            NewValue = JobSeekerEmailGraceTextBox.Text.ToInt().ToString(),
            OldValue = App.Settings.JobSeekerInactivityEmailGrace.ToString(CultureInfo.InvariantCulture)
          });
      }

      configurationItems.AddRange(updates.Where(u => u.OldValue != u.NewValue).Select(u => new ConfigurationItemDto
      {
        Key = u.Key,
        Value = u.NewValue
      }));
    }

    private struct ConfigurationItemUpdate
    {
      public string Key;
      public string OldValue;
      public string NewValue;
    }
	}
}