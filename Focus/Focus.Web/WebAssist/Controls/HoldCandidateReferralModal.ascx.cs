﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Web.Code;

using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class HoldCandidateReferralModal : UserControlBase
	{
		private EmailTemplateView EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>("HoldCandidateReferralModal:EmailTemplate"); }
			set { SetViewStateValue("HoldCandidateReferralModal:EmailTemplate", value); }
		}

		private long ApplicationId
		{
			get { return GetViewStateValue<long>("HoldCandidateReferralModal:ApplicationId"); }
			set { SetViewStateValue("HoldCandidateReferralModal:ApplicationId", value); }
		}

		private long CandidateId
		{
			get { return GetViewStateValue<long>("HoldCandidateReferralModal:CandidateId"); }
			set { SetViewStateValue("HoldCandidateReferralModal:CandidateId", value); }
		}

		private int LockVersion
		{
			get { return GetViewStateValue<int>("HoldCandidateReferralModal:LockVersion"); }
			set { SetViewStateValue("HoldCandidateReferralModal:LockVersion", value); }
		}

		private bool JobReferralFilterApplied
		{
			get { return GetViewStateValue<bool>("HoldCandidateReferralModal:JobReferralFilterApplied"); }
			set { SetViewStateValue("HoldCandidateReferralModal:JobReferralFilterApplied", value); }
		}

		private const string FilteredURL = "?filtered=true";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void HoldAndSendMessageButton_Clicked(object sender, EventArgs e)
		{
			var coreClient = ServiceClientLocator.CoreClient(App);
			var candidateClient = ServiceClientLocator.CandidateClient(App);

			try
			{
				candidateClient.HoldCandidateReferral(ApplicationId, ApplicationOnHoldReasons.PutOnHoldByStaff, LockVersion);
			}
			catch (ServiceCallException ex)
			{
				Confirmation.Show(CodeLocalise("ReferralDeniedError.Title", "Hold failed"),
					CodeLocalise("ReferralDeniedError.Body", string.Format("There was a problem putting the referral on hold.<br /><br />{0}", ex.Message)),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink: JobReferralFilterApplied ? UrlBuilder.JobSeekerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.JobSeekerReferrals());

				return;
			}

			var senderAddress = coreClient.GetSenderEmailAddressForTemplate(EmailTemplateTypes.HoldCandidateReferralRequest);
			var emailSubject = EmailSubjectTextBox.Text;
			var emailBody = EmailBodyTextBox.TextTrimmed();
			candidateClient.SendCandidateEmail(CandidateId, emailSubject, emailBody, false, BCCMeCheckBox.Checked, senderAddress: senderAddress);
			coreClient.SaveReferralEmail(emailSubject, emailBody, ApprovalStatuses.OnHold, applicationId: ApplicationId);

			Confirmation.Show(CodeLocalise("ReferralDeniedConfirmation.Title", "Referral put on hold"),
												CodeLocalise("ReferralDeniedConfirmation.Body", "The referral has been put on hold."),
												CodeLocalise("CloseModal.Text", "OK"),
												closeLink: JobReferralFilterApplied ? UrlBuilder.JobSeekerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.JobSeekerReferrals());
		}

		/// <summary>
		/// Shows the specified application.
		/// </summary>
		/// <param name="candidateApplication">The candidate application.</param>
		/// <param name="filtered">Return back to filtered list</param>
		public void Show(JobSeekerReferralAllStatusViewDto candidateApplication,bool filtered = false)
		{
			LockVersion = candidateApplication.LockVersion;
			ApplicationId = candidateApplication.Id.GetValueOrDefault();
			CandidateId = candidateApplication.CandidateId;
			JobReferralFilterApplied = filtered;

			GetEmailTemplate(candidateApplication);

			var emailBody = EmailTemplate.Body.Split(new[] {'\n'}, 2);

			SalutationTextBox.Text = emailBody[0];

			EmailSubjectTextBox.Text = EmailTemplate.Subject.Trim();
			EmailBodyTextBox.Text = emailBody[1].Trim();

			ModalPopup.Show();
		}

		private void LocaliseUI()
		{
			TitleLiteral.Text = HtmlLocalise("TitleLiteral.Text", "Notification of on-hold referral request");
			SubTextLiteral.Text = HtmlLocalise("SubTextLiteral.Text", "Notify the job seeker that their referral request has been placed on hold.");
			EmailSubjectLiteral.Text = HtmlLocalise("EmailSubjectLiteral.Text", "Subject");
			EmailBodyRequired.ErrorMessage = CodeLocalise("EmailBodyRequired.ErrorMessage", "Email body required");
			HoldAndSendMessageButton.Text = CodeLocalise("DenyAndSendMessageButton.Text", "Email job seeker");
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");
		}

		/// <summary>
		/// Builds the email body.
		/// </summary>
		/// <returns></returns>
		private void GetEmailTemplate(JobSeekerReferralAllStatusViewDto candidateApplication)
		{
			var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) 
				? userDetails.PrimaryPhoneNumber.Number 
				: "N/A";

			var templateValues = new EmailTemplateData
			{
				RecipientName = candidateApplication.Name,
				SenderPhoneNumber = phoneNumber,
				SenderEmailAddress = App.User.EmailAddress,
				SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName),
				EmployerName = candidateApplication.EmployerName,
				JobTitle = candidateApplication.JobTitle
			};

			EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.HoldCandidateReferralRequest, templateValues);
		}
	}
}