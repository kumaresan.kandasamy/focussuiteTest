﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SpideredEmployersList.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.SpideredEmployersList" %>
<%@ Import Namespace="Focus.Core.Models.Assist" %>

<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/ComparePostingToResumeModal.ascx" tagName="ComparePostingToResumeModal" tagPrefix="uc" %>
<%@ Register src="~/WebAssist/Controls/PostingModal.ascx" tagName="PostingModal" tagPrefix="uc" %>

<asp:UpdatePanel ID="SpideredEmployersListUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
	<ContentTemplate>
		<div class="FilterAndPagerDiv">
			<div class="FilterDiv">
				<table role="presentation">
					<tr>
						<td style="width:40;"><focus:LocalisedLabel runat="server" ID="FilterLabel" LocalisationKey="Filter.Label" DefaultText="Filter" AssociatedControlID="FilterTextBox" CssClass="bold"/></td>
						<td>
							<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="FilterTextBoxInFieldLabel" AssociatedControlID="FilterTextBox" LocalisationKey="FilterTextBox.InFiledLabel" DefaultText="#BUSINESS#:LOWER name" Width="200"/></span>
							<asp:TextBox ID="FilterTextBox" runat="server" MaxLength="100" />
							<asp:Button ID="FilterButton" runat="server" class="button1" CausesValidation="False" OnClick="FilterButton_Clicked" />
						</td>
					</tr>
				</table>
			</div>
			<div class="PagerDiv">
				<uc:Pager ID="SpideredEmployersListPager" runat="server" PagedControlID="SpideredEmployersListView" PageSize="10" DisplayRecordCount="true" OnDataPaged="SpideredEmployersListPager_DataPaged" />
			</div>
		</div>
		<div class="MainListDiv">
			<asp:ListView runat="server" ID="SpideredEmployersListView" ItemPlaceholderID="SpideredEmployersPlaceHolder" DataSourceID="SpideredEmployersDataSource" OnLayoutCreated="SpideredEmployersListView_LayoutCreated" 
									OnSorting="SpideredEmployersListView_Sorting" OnItemDataBound="SpideredEmployersListView_ItemDataBound" OnItemCommand="SpideredEmployersListView_OnItemCommand">
				<LayoutTemplate>
					 <table style="width:100%;" class="table">
							<tr>
								<th style="width:35%;">
									<table role="presentation">
										<tr>
                      <td rowspan="2">
                         <asp:Literal runat="server" ID="EmployerNameHeaderLiteral" />
                      </td>
                      <td>
	                      
                        <asp:ImageButton ID="EmployerNameSortAscButton" CssClass="AscButton"
                            runat="server" CommandName="Sort" CommandArgument="employername asc" AlternateText="Sort Ascending" />
                           <asp:ImageButton ID="EmployerNameSortDescButton" CssClass="DescButton"
                            runat="server" CommandName="Sort" CommandArgument="employername desc" AlternateText="Sort Descending" />
                      </td>
                    </tr>
									</table>
								</th>
								<th style="width:35%;">
									<asp:Literal runat="server" ID="JobTitleHeaderLiteral" />
                </th>
								<th style="width:10%;">
									<asp:Literal runat="server" ID="PostingDateHeaderLiteral" />
								</th>
								<th style="width:20%;">
									<asp:Literal runat="server" ID="ActionsHeaderLiteral" />
                </th>
							</tr>
							<asp:PlaceHolder ID="SpideredEmployersPlaceHolder" runat="server" />
					 </table>
				</LayoutTemplate>
				<ItemTemplate>
					<tr id="EmployerRow" runat="server" ClientIDMode="AutoID">
						<td>
							<%# Eval("EmployerName") %>
						</td>
						<td>
							<asp:LinkButton ID="GetPostingsLinkButton" runat="server" CommandArgument='<%# Eval("EmployerName") %>' CommandName="GetPostings" ClientIDMode="AutoID"><%# Eval("NumberOfPostings")%> jobs</asp:LinkButton>
						</td>
						<td></td>
						<td></td>
					</tr>
					<asp:Repeater ID="PostingsRepeater" runat="server">
						<ItemTemplate>
							<tr id="PostingRow" runat="server" ClientIDMode="Static">
								<td></td>
								<td style="vertical-align:top;">
									<asp:LinkButton ID="ViewPostingLinkButton" runat="server" OnCommand="ViewPostingLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="ViewPosting" ClientIDMode="AutoID"><%# Eval("JobTitle") %></asp:LinkButton><br/>
								</td>
								<td style="vertical-align:top;">
									<%# ((SpideredPostingModel)Container.DataItem).PostingDate.ToString("MMM dd, yyyy") %>
								</td>
								<td>
									<asp:Panel ID="ViewMatchesPanel" runat="server">
										<asp:LinkButton ID="SearchMatchesLinkButton" runat="server" OnCommand="SearchMatchesLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="SearchMatches" ClientIDMode="AutoID"><%= HtmlLocalise("SearchMatches.Text", "Search For Matches")%></asp:LinkButton>
									</asp:Panel>
									<asp:LinkButton ID="ContactEmployerLinkButton" runat="server" OnCommand="ContactEmployerLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="ContactEmployer" ClientIDMode="AutoID"><%= HtmlLocalise("ContactEmployer.Text", "Contact Employer")%></asp:LinkButton>
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</ItemTemplate>
			</asp:ListView>
			<asp:ObjectDataSource ID="SpideredEmployersDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.SpideredEmployersList" EnablePaging="true" SelectMethod="GetSpideredEmployers" 
														SelectCountMethod="GetSpideredEmployersCount" SortParameterName="orderBy" OnSelecting="SpideredEmployersDataSource_Selecting" />
		</div>

		<uc:ComparePostingToResumeModal ID="ComparePostingToResumeModal" runat="server" OnModalClosed="ComparePostingToResumeModal_Closed" />
		<uc:PostingModal ID="PostingModal" runat="server" />

		<script type="text/javascript">

		var prm = Sys.WebForms.PageRequestManager.getInstance();

		prm.add_endRequest(function ()
		{
			// Apply jQuery to infield labels
			$(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();
		});
		
		</script>
	</ContentTemplate>
	<Triggers>
	</Triggers>
</asp:UpdatePanel>
<asp:Label runat="server" id="ErrorLabel" cssClass="error" Visible="False"></asp:Label>
