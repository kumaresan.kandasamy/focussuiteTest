﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewCaseManagementError.ascx.cs"
	Inherits="Focus.Web.WebAssist.Controls.ViewCaseManagementError" %>
<asp:HiddenField ID="ViewCaseManagementErrorModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ViewCaseManagementErrorModalPopup" runat="server" ClientIDMode="Static"
	TargetControlID="ViewCaseManagementErrorModalDummyTarget" PopupControlID="ViewCaseManagementErrorPanel"
	RepositionMode="RepositionOnWindowResizeAndScroll" BackgroundCssClass="modalBackground" />
<asp:Panel ID="ViewCaseManagementErrorPanel" TabIndex="-1" runat="server" CssClass="modal"
	Style="display: none; width: 80%">
	<div class="closeIcon"><input type="image" src="<%= UrlBuilder.ButtonCloseIcon() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" onclick="$find('<%=ViewCaseManagementErrorModalPopup.BehaviorID %>').hide(); return false;" /></div>
	<table id="RequestResponseMessagesTable" class="table" width="100%">
		<thead>
			<tr>
				<th width="45%">
					<focus:LocalisedLabel ID="LocalisedLabel1" runat="server" DefaultText="Request sent" />
				</th>
				<th width="45%">
					<focus:LocalisedLabel ID="LocalisedLabel2" runat="server" DefaultText="Response received" />
				</th>
				<th width="10%">
				</th>
			</tr>
		</thead>
		<tbody data-bind="if: requestResponses().length == 0">
			<tr>
				<td colspan="3" style="padding: 10px;">
					<focus:LocalisedLabel ID="LocalisedLabel5" runat="server" DefaultText="No messages were logged for this error." />
				</td>
			</tr>
		</tbody>
		<tbody data-bind="foreach: requestResponses">
			<tr>
				<td>
					<textarea disabled style="resize: none;" data-bind="text: fullRequest, attr: { rows: rows}" title="Request"></textarea>
				</td>
				<td>
					<textarea disabled style="resize: none;" data-bind="text: fullResponse, attr: { rows: rows}" title="Response"></textarea>
				</td>
				<td>
					<button class="button2" data-bind="click: ExpandCollapse, visible: expanded() != true">
						<focus:LocalisedLabel ID="LocalisedLabel3" runat="server" DefaultText="Expand" />
					</button>
					<button class="button2" data-bind="click: ExpandCollapse, visible: expanded">
						<focus:LocalisedLabel ID="LocalisedLabel4" runat="server" DefaultText="Collapse" />
					</button>
				</td>
			</tr>
		</tbody>
	</table>
</asp:Panel>
<script type="text/javascript">
		var requestResponseModel = function (data) {
			var self = this;

			self.expanded = ko.observable(false);

			self.rows = ko.observable(5);

			try {
				self.fullRequest = vkbeautify.json(data.RequestMessage);
			}
			catch (err) {
				self.fullRequest = vkbeautify.xml(data.RequestMessage);
			}

			try {
				self.fullResponse = vkbeautify.json(data.ResponseMessage);
			}
			catch(err) {
				self.fullResponse = vkbeautify.xml(data.ResponseMessage);
			}

			self.ExpandCollapse = function () {
				if (self.expanded())
				{
					self.rows(5);
					self.expanded(false);
				}
				else
				{
					self.rows(20);
					self.expanded(true);
				}
			}
		}

		var viewModel = function () {
			var self = this;

			self.requestResponses = ko.observableArray(ko.utils.arrayMap(<%= RequestResponseData %>, function(item) {
				return new requestResponseModel(item);
			}));
		}

		ko.applyBindings(new viewModel(), document.getElementById('RequestResponseMessagesTable'));
</script>
