﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Core;
using Framework.Core;

using Focus.Web.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Services.Core;
using Framework.Exceptions;


#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class HoldEmployerReferralModal : UserControlBase
	{

    private ApprovalType ApprovalType
    {
      get { return GetViewStateValue<ApprovalType>("HoldEmployerReferralModal:ApprovalType"); }
      set { SetViewStateValue("HoldEmployerReferralModal:ApprovalType", value); }
    }

		private long EmployeeId
		{
			get { return GetViewStateValue<long>("HoldEmployerReferralModal:EmployeeId"); }
			set { SetViewStateValue("HoldEmployerReferralModal:EmployeeId", value); }
		}

		private EmailTemplateView EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>( "HoldEmployerReferralModal:EmailTemplate" ); }
			set { SetViewStateValue( "HoldEmployerReferralModal:EmailTemplate", value ); }
		}

    private long EmployerId
		{
			get { return GetViewStateValue<long>("HoldEmployerReferralModal:EmployerId"); }
			set { SetViewStateValue("HoldEmployerReferralModal:EmployerId", value); }
		}

    private long BusinessUnitId
		{
			get { return GetViewStateValue<long>("HoldEmployerReferralModal:BusinessUnitId"); }
			set { SetViewStateValue("HoldEmployerReferralModal:BusinessUnitId", value); }
		}

		private string LockVersion
		{
			get { return GetViewStateValue<string>("HoldEmployerReferralModal:LockVersion"); }
			set { SetViewStateValue("HoldEmployerReferralModal:LockVersion", value); }
		}

		private bool EmployerFiltered
		{
			get { return GetViewStateValue<bool>("HoldEmployerReferralModal:EmployerFiltered"); }
			set { SetViewStateValue("HoldEmployerReferralModal:EmployerFiltered", value); }
		}

		private const string FilteredUrl = "?filtered=true";

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Shows the specified employee id.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="approvalType"></param>
		/// <param name="employerId"></param>
		/// <param name="businessUnitId"></param>
		/// <param name="lockVersion"></param>
		/// <param name="filtered">Return filtered list</param>
		public void Show(long employeeId, ApprovalType approvalType, long employerId, long businessUnitId, string lockVersion,bool filtered = false)
		{
			EmployeeId = employeeId;
			ApprovalType = approvalType;
			BusinessUnitId = businessUnitId;
			EmployerId = employerId;
			LockVersion = lockVersion;
			EmployerFiltered = filtered;

			var id = EmployeeId;
			switch (ApprovalType)
			{
				case ApprovalType.Employer:
					id = EmployerId;
					break;
				case ApprovalType.BusinessUnit:
					id = BusinessUnitId;
					break;
			}

			GetEmailTemplate(id);

			if (EmailTemplate == null) return;

			var regex = new Regex(Constants.PlaceHolders.HoldReasons);
			var emailBodySplit = regex.Split(EmailTemplate.Body);

			TopEmailBodyLiteral.Text = emailBodySplit[0].TrimEnd('\r', '\n').Replace(Environment.NewLine, "<br />");
			BottomEmailBodyLiteral.Text = (emailBodySplit.Length > 1) ? emailBodySplit[1].TrimStart('\r', '\n').Replace(Environment.NewLine, "<br />") : string.Empty;

			BCCMeCheckBox.Checked = false; // default to unchecked in case the user has previously opened the modal and cancelled

			cblOnHoldReasons.BindLookup(ApprovalType == ApprovalType.HiringManager
				? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.HiringManagerOnHoldReasons)
				: ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmployerOnHoldReasons));

			EmailSubjectTextBox.Text = EmailTemplate.Subject.Trim();

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				CheckBoxOnHoldReasonsPlaceholder.Visible = true;
				//replaced lookup type de LookupTypes DenialReasons to lookup types OnHoldReasons
				cblOnHoldReasons.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(ApprovalType == ApprovalType.HiringManager ? LookupTypes.HiringManagerOnHoldReasons : LookupTypes.EmployerOnHoldReasons));
				OtherOnHoldReasonTextBox.ResetText();
				OtherOnHoldReasonTextBox.Enabled = false;
			}
			else
			{
				OnHoldReasonsTextBox.ResetText();
				FreeTextOnHoldReasonPlaceholder.Visible = true;
				CheckBoxOnHoldReasonsPlaceholder.Visible = false;
			}

			LocaliseUI();

			ModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the SendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void HoldAndSendMessageButton_Clicked(object sender, EventArgs e)
		{
			try
			{
				var Id = EmployeeId;

				switch (ApprovalType)
				{
					case ApprovalType.Employer:
						Id = EmployerId;
						break;
					case ApprovalType.BusinessUnit:
						Id = BusinessUnitId;
						break;
				}
				var employeeClient = ServiceClientLocator.EmployeeClient(App);
				var coreClient = ServiceClientLocator.CoreClient(App);

				string onHoldReasons;
				var holdList = new List<KeyValuePair<long, string>>();

				if (App.Settings.Theme == FocusThemes.Workforce)
				{
					var otherId = ApprovalType == ApprovalType.HiringManager
						? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.HiringManagerOnHoldReasons).Where(x => x.Key.Equals("HiringManagerOnHoldReasons.OtherReason")).Select(x => x.Id).FirstOrDefault()
						: ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmployerOnHoldReasons).Where(x => x.Key.Equals("EmployerOnHoldReasons.Other")).Select(x => x.Id).FirstOrDefault();

					var query = (from ListItem checkbox in cblOnHoldReasons.Items
						where checkbox.Selected && checkbox.Value.IsNotNullOrEmpty() && checkbox.Value != otherId.ToString()
						select checkbox).ToList();

					#region Create a list of the reasons to be displayed in the email

					var holdReasonList = query.Select(checkBox => checkBox.Text).ToList();

					var otherReason = OtherOnHoldReasonTextBox.TextTrimmed();

					if (otherReason.IsNotNullOrEmpty())
						holdReasonList.Add(otherReason);

					onHoldReasons = string.Join(Environment.NewLine, holdReasonList.Select(x => string.Concat("* ", x)));

					#endregion

					#region Create a list of keyvaluepairs of the reason Ids and Other text for saving to the database

					holdList = query.Select(checkBox => new KeyValuePair<long, string>(Convert.ToInt64(checkBox.Value), string.Empty)).ToList();

					if (otherReason.IsNotNullOrEmpty())
					{
						var otherReasonEntry = new KeyValuePair<long, string>(otherId, otherReason);
						holdList.Add(otherReasonEntry);
					}

					#endregion
				}
				else
				{
					onHoldReasons = OnHoldReasonsTextBox.TextTrimmed();
				}

				employeeClient.HoldEmployerAccountReferral( EmployeeId, BusinessUnitId, LockVersion, ApprovalType, App.Settings.Theme == FocusThemes.Workforce ? holdList : null );
				var employerReferralView = employeeClient.GetEmployerAccountReferral(Id);

				GetEmailTemplate(Id);

				if (EmailTemplate == null) return;

				var senderAddress = coreClient.GetSenderEmailAddressForTemplate(EmailTemplateTypes.HoldEmployerAccountReferral);
				var emailBody = EmailTemplate.Body.Replace(Constants.PlaceHolders.HoldReasons, onHoldReasons);

				employeeClient.EmailEmployee(employerReferralView.EmployeeId, EmailSubjectTextBox.TextTrimmed(), emailBody, BCCMeCheckBox.Checked, senderAddress);

				coreClient.SaveReferralEmail(EmailSubjectTextBox.TextTrimmed(), emailBody, ApprovalStatuses.OnHold, employerReferralView.EmployeeId);

				ModalPopup.Hide();

				if (ApprovalType == ApprovalType.HiringManager)
				{
					Confirmation.Show(CodeLocalise("ReferralHeldConfirmationHiringManager.Title", "Hiring Manager account place on hold"),
						CodeLocalise("ReferralHeldConfirmationHiringManager.Body", "The hiring manager account has been placed on hold."),
						CodeLocalise("CloseModal.Text", "OK"),
						closeLink: EmployerFiltered ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.EmployerReferrals());
				}
				else
				{
					Confirmation.Show(CodeLocalise("ReferralHeldConfirmation.Title", "#BUSINESS# account place on hold"),
						CodeLocalise("ReferralHeldConfirmation.Body", "The #BUSINESS#:LOWER account has been placed on hold."),
						CodeLocalise("CloseModal.Text", "OK"),
						closeLink: EmployerFiltered ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.EmployerReferrals());
				}
			}
			catch (ServiceCallException ex)
			{
				var message = (ApprovalType == ApprovalType.HiringManager)
												? CodeLocalise("ReferralHoldExceptionHiringManager.Body", "There was a problem putting the hiring manager on hold.<br /><br />{0}", ex.Message)
												: CodeLocalise("ReferralHolddExceptionEmployer.Body", "There was a problem putting the #BUSINESS#:LOWER on hold.<br /><br />{0}", ex.Message);

				Confirmation.Show(CodeLocalise("ReferralOnHoldError.Title", "Hold failed"),
													message,
													CodeLocalise("CloseModal.Text", "OK"),
													closeLink: EmployerFiltered ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.EmployerReferrals());
			}
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			HoldAndSendMessageButton.Text = ApprovalType == ApprovalType.HiringManager ? CodeLocalise("DenyAndSendHMMessageButton.Text", "Email hiring manager") : CodeLocalise("DenyAndSendMessageButton.Text", "Email #BUSINESS#:LOWER");
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				OnHoldReasonsCheckboxRequired.ErrorMessage = CodeLocalise("HoldReasonsRequired.ErrorMessage", "On hold reasons required");
				OtherOnHoldReasonRequired.ErrorMessage = CodeLocalise("OtherOnHoldReasonRequired.ErrorMessage", "Other reason description required");
			}
			else
			{
				OnHoldReasonsRequired.ErrorMessage = CodeLocalise("HoldReasonsRequired.ErrorMessage", "On hold reasons required");
			}

			NotificationTitleLabel.DefaultText = ApprovalType == ApprovalType.HiringManager ? CodeLocalise("NotificationHMLabel.Text", "Notification of Pending Hiring Manager Account Request") : CodeLocalise("NotificationLabel.Text", "Notification of Pending #BUSINESS# Account Request");
			NotificationSubTitleLabel.DefaultText = ApprovalType == ApprovalType.HiringManager ? CodeLocalise("NotificationHMSubTitleLabel.Text", "Notify the hiring manager their account request is pending.") : CodeLocalise("NotificationSubTitleLabel.Text", "Notify the #BUSINESS#:LOWER their account request is pending.");
		}

		/// <summary>
		/// Builds the email body.
		/// </summary>
		/// <returns></returns>
    private void GetEmailTemplate(long employeeId)
    {
      var employerReferralView = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(employeeId);
			if (!employerReferralView.IsNotNull())
			{
				var message = (ApprovalType == ApprovalType.HiringManager)
												? CodeLocalise("ReferralOnHoldErrorHiringManager.Body", "There was a problem putting the hiring manager on hold.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.")
												: CodeLocalise("ReferralOnHoldErrorEmployer.Body", "There was a problem putting the #BUSINESS#:LOWER on hold.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.");

				Confirmation.Show(CodeLocalise("ReferralOnHoldError.Title", "Hold failed"),
													message,
													CodeLocalise("CloseModal.Text", "OK"),
													closeLink: EmployerFiltered ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.EmployerReferrals());
				return;
			}
      var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
      var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";

      var templateValues = new EmailTemplateData
      {
        RecipientName = String.Format("{0} {1}", employerReferralView.EmployeeFirstName, employerReferralView.EmployeeLastName),
        SenderPhoneNumber = phoneNumber,
        SenderEmailAddress = App.User.EmailAddress,
        SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName)
      };

      EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.HoldEmployerAccountReferral, templateValues);
    }
	}
}