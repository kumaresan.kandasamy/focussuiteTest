﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Focus.Common.Code;
using Focus.Common.Code.ControllerResults;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Employee;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Focus.Web.Controllers.Code.Controls.User;
using Focus.Web.ViewModels;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersReadOnly)]
	public partial class FindEmployer : UserControlBase
	{
		public override IPageController RegisterPageController()
		{
			return new FindEmployerController(App);
		}

		protected FindEmployerController PageController { get { return PageControllerBaseProperty as FindEmployerController; } }

		private int _employeesCount;
		private bool _reApplyFindEmployeeFilter;
		private string _altEmployeeApproved, _altEmployeeRejected, _altEmployeeAwaitingApproval, _employeeOnHold;
		private string _altEmployerPreferred, _altEmployerParent;
		protected string CloseFindBusinessWindowErrorMessage = "";

		private EmployeeCriteria EmployeeCriteria
		{
			get { return GetViewStateValue<EmployeeCriteria>("Employers:EmployeeCriteria"); }
			set { SetViewStateValue("Employers:EmployeeCriteria", value); }
		}

		private ActionTypes? SelectedAction
		{
			get { return GetViewStateValue<ActionTypes>("Employers:SelectedAction"); }
			set { SetViewStateValue("Employers:SelectedAction", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			_altEmployeeApproved = CodeLocalise("EmployeeApproved.Hint", "Account approved");
			_altEmployeeRejected = CodeLocalise("EmployeeRejected.Hint", "Account rejected");
			_altEmployeeAwaitingApproval = CodeLocalise("EmployeeAwaitingApproval", "Account awaiting approval");
			_employeeOnHold = CodeLocalise("EmployeeAwaitingApproval", "Account on-hold");
			_altEmployerPreferred = CodeLocalise("EmployerPreferred", "#BUSINESS# preferred");
			_altEmployerParent = CodeLocalise("EmployerParent.Hint", "Parent #BUSINESS#:LOWER");

			if (!IsPostBack)
			{
				// Re-set search criteria if returning from a page following a filtered result
				if (Request.QueryString["filtered"] != null)
				{
					if (App.GetSessionValue("FindEmployer:SearchCriteriaApplied", false))
					{
						EmployeeCriteria = App.GetSessionValue("FindEmployer:SearchCriteria", new EmployeeCriteria());
						_reApplyFindEmployeeFilter = true;

						SearchResultListPager.ReturnToPage(Convert.ToInt32(EmployeeCriteria.PageIndex), Convert.ToInt32(EmployeeCriteria.PageSize));

						BindEmployerSearchTextFields();
						EmployersSearch();
					}
				}
				else
				{
					App.RemoveSessionValue("FindEmployer:SearchCriteriaApplied");
				}

				FindEmployerStatusDropdown.Visible = FindEmployerStatusLabel.Visible = App.Settings.Theme == FocusThemes.Workforce;

				if (App.Settings.Theme == FocusThemes.Workforce)
					BindStatusDropdown();

				LocaliseUI();
				FEINList.MaxSize = App.Settings.FindEmployerFEINsAllowed;
			}
		}

		/// <summary>
		/// Handles the DataBound event of the ItemsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ItemsList_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var checkBox = (CheckBox)e.Item.FindControl("SelectorCheckBox");

			var scriptManager = ScriptManager.GetCurrent(Page);

			if (scriptManager.IsNotNull())
			{
				scriptManager.RegisterAsyncPostBackControl(checkBox);

				var employee = (EmployeeSearchResultView)e.Item.DataItem;

				var url = (HtmlAnchor)e.Item.FindControl("BusinessUnitUrl");
				url.HRef = UrlBuilder.AssistEmployerProfile(employee.BusinessUnitId) + "?filtered=true";

				var urlString = UrlBuilder.AssistHiringManagerProfile(((EmployeeSearchResultView)e.Item.DataItem).EmployeeBusinessUnitId) + "?filtered=true";

				((Literal)e.Item.FindControl("HiringManagerLink")).Text = String.Format("<a href=\"{0}\">{1}, {2}</a>", urlString, ((EmployeeSearchResultView)e.Item.DataItem).LastName, ((EmployeeSearchResultView)e.Item.DataItem).FirstName);

				var phoneNumber = Regex.Replace(((EmployeeSearchResultView)e.Item.DataItem).PhoneNumber, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat);
				if (((EmployeeSearchResultView)e.Item.DataItem).Extension.IsNotNullOrEmpty())
					phoneNumber = phoneNumber + " (ext " + ((EmployeeSearchResultView)e.Item.DataItem).Extension + ")";

				((Literal)e.Item.FindControl("PhoneNumber")).Text = phoneNumber;
			}
		}

		/// <summary>
		/// Handles Find.
		/// </summary>
		private void EmployersSearch()
		{
			if (!_reApplyFindEmployeeFilter)
			{
				SearchResultListPager.ReturnToFirstPage();
				UnbindSearch();
			}

			BindEmployeesList();

			SelectedAction = null;
			BindActionDropDown();

			ActionDropDown.Visible = ActionGoButton.Visible = ActionLabel.Visible = SearchResultListPager.Visible = (SearchResultListPager.TotalRowCount > 0);
			ActivityAssigner.Visible = false;

			ActionUpdatePanel.Update();
			if (FEINPlaceHolder.Visible)
				FEINUpdatePanel.Update();

			// Set the sort images
			if (SearchResultListPager.TotalRowCount > 0) FormatEmployeesListSortingImages();
		}

		/// <summary>
		/// Handles the Clicked event of the FindButton_ control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void FindButton_Clicked(object sender, EventArgs e)
		{
			EmployersSearch();
		}

		/// <summary>
		/// Fires when the ClearButton is clicked
		/// </summary>
		/// <param name="sender">The button being clicked</param>
		/// <param name="e">Standard event arguments</param>
		protected void ClearButton_Clicked(object sender, EventArgs e)
		{
			var url = Request.Url.GetLeftPart(UriPartial.Path);
			var hasQueryString = false;
			foreach (var qs in Request.QueryString.AllKeys)
			{
				if (!qs.Equals("filtered"))
				{
					url = string.Concat(url, hasQueryString ? "&" : "?", Request.QueryString[qs]);
					hasQueryString = true;
				}
			}
			Response.Redirect(url);
		}

		/// <summary>
		/// Handles the Clicked event of the ActionButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ActionButton_Clicked(object sender, EventArgs e)
		{
			// Hide the sub action controls
			ActivityAssigner.Visible = SubActionDropDown.Visible = SubActionButton.Visible = false;

			SelectedAction = ActionDropDown.SelectedValueToEnum<ActionTypes>();

			var employeesSelected = GetSelectedEmployees();
			var businessUnitId = GetSelectedBusinessUnitId();

			if (employeesSelected.IsNullOrEmpty() || employeesSelected.Count == 0) return;

			switch (SelectedAction)
			{
				case ActionTypes.AccessEmployeeAccount:
					if (AreMultipleRowsSelected(employeesSelected)) return;

					var singleSignOnKey = ServiceClientLocator.AuthenticationClient(App).CreateEmployeeSingleSignOn(employeesSelected[0], null);
					var blockedPopupMessage = CodeLocalise("AccessEmployeePopupBlocked.Error.Text", "Unable to access #BUSINESS#:LOWER account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.");
					var script = string.Format("$(document).ready(function (){{FindBusiness_TalentPopup = showPopup('{0}{1}','{2}');}});", App.Settings.TalentApplicationPath, UrlBuilder.SingleSignOn(singleSignOnKey, false), blockedPopupMessage);

					ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AccessEmployeeAccount", script, true);
					break;

				case ActionTypes.ViewEmployerProfile:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					Response.Redirect(UrlBuilder.AssistEmployerProfile(GetSelectedBusinessUnitId()));
					break;

				case ActionTypes.EmailEmployee:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					ActivityAssigner.Visible = false;
					EmailEmployee.Show(employeesSelected[0]);
					break;

				case ActionTypes.AssignEmployerToStaff:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					PrepareAssignEmployer();
					break;

				case ActionTypes.AssignActivityToEmployee:
					PrepareAssignActivity();
					break;

				case ActionTypes.MarkAsPreferredEmployer:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					var markResult = PageController.MarkAsPreferredConfirmation(businessUnitId);
					Handle(markResult);
					ActionComplete();
					break;

				case ActionTypes.UnmarkAsPreferredEmployer:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					var unmarkResult = PageController.UnmarkAsPreferredConfirmation(businessUnitId);
					Handle(unmarkResult);
					ActionComplete();
					break;

				case ActionTypes.BlockEmployeeAccount:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					ActionConfirmationModal.Show(CodeLocalise("BlockEmployeeAccount.Title", "Block account"),
														CodeLocalise("BlockEmployeeAccount.Body", "Please confirm you would like to block this user's account."),
														CodeLocalise("Global.Cancel.Text", "Cancel"),
																												okText: CodeLocalise("BlockEmployeeAccount.Ok.Text", "Block account"), okCommandName: "BlockEmployeeAccount", okCommandArgument: employeesSelected[0].ToString(CultureInfo.InvariantCulture));
					break;

				case ActionTypes.UnblockEmployeeAccount:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					ActionConfirmationModal.Show(CodeLocalise("UnblockEmployeeAccount.Title", "Unblock account"),
														CodeLocalise("UnblockEmployeeAccount.Body", "Please confirm you would like to unblock this user's account."),
														CodeLocalise("Global.Cancel.Text", "Cancel"),
																												okText: CodeLocalise("UnblockEmployeeAccount.Ok.Text", "Unblock account"), okCommandName: "UnblockEmployeeAccount", okCommandArgument: employeesSelected[0].ToString(CultureInfo.InvariantCulture));
					break;

				case ActionTypes.BlockAllEmployerEmployeesAccount:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					ActionConfirmationModal.Show(CodeLocalise("BlockAllEmployerEmployeesAccount.Title", "Block all #BUSINESS#:LOWER's accounts"),
																							CodeLocalise("BlockAllEmployerEmployeesAccount.Body", "Please confirm you would like to block all accounts for this #BUSINESS#:LOWER."),
									CodeLocalise("Global.Cancel.Text", "Cancel"),
																							okText: CodeLocalise("BlockAllEmployerEmployeesAccount.Ok.Text", "Block accounts"), okCommandName: "BlockAllEmployerEmployeesAccount", okCommandArgument: businessUnitId.ToString(CultureInfo.InvariantCulture));
					break;

				case ActionTypes.UnblockAllEmployerEmployeesAccount:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					ActionConfirmationModal.Show(CodeLocalise("UnblockAllEmployerEmployeesAccount.Title", "Unblock all #BUSINESS#:LOWER's accounts"),
																							CodeLocalise("UnblockAllEmployerEmployeesAccount.Body", "Please confirm you would like to unblock all accounts for this #BUSINESS#:LOWER."),
									CodeLocalise("Global.Cancel.Text", "Cancel"),
																							okText: CodeLocalise("UnblockAllEmployerEmployeesAccount.Ok.Text", "Unblock accounts"), okCommandName: "UnblockAllEmployerEmployeesAccount", okCommandArgument: businessUnitId.ToString(CultureInfo.InvariantCulture));
					break;

				case ActionTypes.ApproveEmployeeReferral:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					ActionConfirmationModal.Show(CodeLocalise("ApproveEmployeeReferral.Title", "Approve rejected #BUSINESS#:LOWER"),
																																	 CodeLocalise("UnblockAllEmployerEmployeesAccount.Body", "Please confirm you would like to approve this rejected #BUSINESS#:LOWER."),
														 CodeLocalise("Global.Cancel.Text", "Cancel"),
																																	 okText: CodeLocalise("ApproveEmployeeReferral.Ok.Text", "Approve"), okCommandName: "ApproveEmployeeReferral", okCommandArgument: employeesSelected[0].ToString(CultureInfo.InvariantCulture));
					break;

				case ActionTypes.EditCompanyAddress:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					ActivityAssigner.Visible = false;
					var employerId = GetSelectedEmployerId();
					EditCompany.Show(employerId, businessUnitId);
					break;

				case ActionTypes.EditHiringManager:
					if (AreMultipleRowsSelected(employeesSelected)) return;
					UpdateContactInformationModal.Visible = true;
					UpdateContactInformationModal.Show(GetSelectedHiringManagerUserId(), GetSelectedBusinessUnitName());
					break;
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SubActionButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SubActionButton_Clicked(object sender, EventArgs e)
		{
			switch (SelectedAction)
			{
				case ActionTypes.AssignEmployerToStaff:
					var adminId = SubActionDropDown.SelectedValueToLong();
					var employeeId = GetSelectedEmployeeId();

					ServiceClientLocator.EmployerClient(App).AssignEmployerToAdmin(adminId, employeeId);

					Confirmation.Show(CodeLocalise("AssignEmployerConfirmation.Title", "#BUSINESS# assigned to staff"),
																									CodeLocalise("AssignEmployerConfirmation.Body", "The #BUSINESS#:LOWER was assigned to the staff member."),
										CodeLocalise("CloseModal.Text", "OK"));
					break;
			}
		}

		/// <summary>
		/// Handles the OkCommand event of the ActionConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ActionConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			long employeeId;
			long businessUnitId;

			switch (e.CommandName)
			{
				case "BlockEmployeeAccount":
					long.TryParse(e.CommandArgument.ToString(), out employeeId);
					if (employeeId > 0)
					{
						ServiceClientLocator.EmployeeClient(App).BlockAccount(employeeId);
						Confirmation.Show(CodeLocalise("BlockEmployeeAccountConfirmation.Title", "Account blocked"),
																				 CodeLocalise("BlockEmployeeAccountConfirmation.Body", "The user's account has been blocked."),
																				 CodeLocalise("CloseModal.Text", "OK"));
					}
					break;

				case "UnblockEmployeeAccount":
					long.TryParse(e.CommandArgument.ToString(), out employeeId);
					if (employeeId > 0)
					{
						ServiceClientLocator.EmployeeClient(App).UnblockAccount(employeeId);
						Confirmation.Show(CodeLocalise("UnblockEmployeeAccountConfirmation.Title", "Account unblocked"),
																				 CodeLocalise("UnblockEmployeeAccountConfirmation.Body", "The user's account has been unblocked."),
																				 CodeLocalise("CloseModal.Text", "OK"));
					}
					break;

				case "BlockAllEmployerEmployeesAccount":
					long.TryParse(e.CommandArgument.ToString(), out businessUnitId);
					if (businessUnitId > 0)
					{
						ServiceClientLocator.EmployerClient(App).BlockEmployeesAccounts(businessUnitId);
						Confirmation.Show(CodeLocalise("BlockAllEmployerEmployeesAccountConfirmation.Title", "Accounts blocked"),
																																								 CodeLocalise("BlockAllEmployerEmployeesAccountConfirmation.Body", "All user accounts for the #BUSINESS#:LOWER have been blocked."),
																				 CodeLocalise("CloseModal.Text", "OK"));
					}
					break;

				case "UnblockAllEmployerEmployeesAccount":
					long.TryParse(e.CommandArgument.ToString(), out businessUnitId);
					if (businessUnitId > 0)
					{
						ServiceClientLocator.EmployerClient(App).UnblockEmployeesAccounts(businessUnitId);
						Confirmation.Show(CodeLocalise("UnblockAllEmployerEmployeesAccountConfirmation.Title", "Accounts unblocked"),
																																								 CodeLocalise("UnblockAllEmployerEmployeesAccountConfirmation.Body", "All user accounts for the #BUSINESS#:LOWER have been unblocked."),
																				 CodeLocalise("CloseModal.Text", "OK"));
					}
					break;
			}

			ActionComplete();
		}

		/// <summary>
		/// Fires when the selected action has been completed
		/// </summary>
		/// <param name="doBind">Whether to bind employers list</param>
		public void ActionComplete(bool doBind = true)
		{
			SelectedAction = null;
			//ActivityAssigner.Visible = false;
			//ActionDropDown.SelectedIndex = 0;
			ActionUpdatePanel.Update();

			if (doBind)
			{
				BindEmployeesList();
				ClearSelectedRadioButtons();
				FindUpdatePanel.Update();
			}
		}

		/// <summary>
		/// Determines whether [is assist employers administrator].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is assist employers administrator]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistEmployersAdministrator()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
		}

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the employees.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<EmployeeSearchResultView> GetEmployees(EmployeeCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var result = PageController.GetEmployees(criteria, orderBy, startRowIndex, maximumRows);
			if (Handle(result))
				return null;

			var viewModelResult = (ViewModelResult)result;
			var employeesModel = (FindEmployerViewModel)viewModelResult.ViewModel;

			_employeesCount = employeesModel.EmployeesCount;

			return employeesModel.Employees;
		}

		/// <summary>
		/// Gets the employees count.
		/// </summary>
		/// <returns></returns>
		public int GetEmployeesCount()
		{
			return _employeesCount;
		}

		/// <summary>
		/// Gets the employees count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetEmployeesCount(EmployeeCriteria criteria)
		{
			return _employeesCount;
		}

		/// <summary>
		/// Gets the employee status image.
		/// </summary>
		/// <param name="employeeItem">The data row item holding the employee view.</param>
		/// <returns></returns>
		protected string GetEmployeeStatusImage(EmployeeSearchResultView employeeItem)
		{
			var approvalStatuses = employeeItem.EmployeeApprovalStatus;

			string img, tooltip;

			switch (approvalStatuses)
			{
				case ApprovalStatuses.Approved:
				case ApprovalStatuses.None:
					img = UrlBuilder.EmployerApprovedIcon();
					tooltip = _altEmployeeApproved;
					break;

				case ApprovalStatuses.Rejected:
					img = UrlBuilder.EmployerRejectedIcon();
					tooltip = _altEmployeeRejected;
					break;

				case ApprovalStatuses.OnHold:
					img = UrlBuilder.EmployerOnHoldIcon();
					tooltip = _employeeOnHold;
					break;

				case ApprovalStatuses.WaitingApproval:
					img = UrlBuilder.EmployerNewIcon();
					tooltip = _altEmployeeAwaitingApproval;
					break;

				default:
					img = UrlBuilder.EmployerNewIcon();
					tooltip = _altEmployeeAwaitingApproval;
					break;
			}

			var imagesString = new StringBuilder();
			imagesString.AppendFormat("<img src='{0}' alt='{1}' title='{1}' class='toolTipNew' />", img, tooltip);

			if (employeeItem.AccountBlocked)
			{
				switch (employeeItem.AccountBlockedReason)
				{
					case null:
					case BlockedReason.Unknown:
						imagesString.AppendFormat("<img src='{0}' alt='{1}'  title='{1}' class='toolTipNew' />", UrlBuilder.BlockedImage(), CodeLocalise("BlockedImage.ToolTip", "Blocked"));
						break;
					case BlockedReason.FailedPasswordReset:
						imagesString.AppendFormat("<img src='{0}' alt='{1}'  title='{1}' class='toolTipNew' />", UrlBuilder.UserBlockedFailedPasswordReset(), CodeLocalise("BlockedFailedSecurityImage.ToolTip", "Blocked - Failed Security"));
						break;
					case BlockedReason.AlienRegistrationDateExpired:
						imagesString.AppendFormat("<img src='{0}' alt='{1}'  title='{1}' class='toolTipNew' />", UrlBuilder.UserBlockedFailedPasswordReset(), CodeLocalise("BlockedFailedSecurityImage.ToolTip", "Blocked - Alien registration date expired"));
						break;
				}
			}

			return imagesString.ToString();
		}

		/// <summary>
		/// Gets the employer status images.
		/// </summary>
		/// <param name="employeeItem">The employee item.</param>
		/// <returns></returns>
		protected string GetEmployerStatusImages(EmployeeSearchResultView employeeItem)
		{
			var imagesString = new StringBuilder();

			if (employeeItem.IsPreferred && App.Settings.EnablePreferredEmployers)
				imagesString.AppendFormat("<img src='{0}' alt='{1}'  title='{1}' class='toolTipNew' />", UrlBuilder.EmployerPreferredIcon(), _altEmployerPreferred);

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				if (employeeItem.IsPrimary)
					imagesString.AppendFormat("<img src='{0}' alt='{1}'  title='{1}' class='toolTipNew' />", UrlBuilder.EmployerParentIcon(),
							_altEmployerParent);
			}

			return imagesString.ToString();
		}

		/// <summary>
		/// Handles the Selecting event of the SearchResultDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["criteria"] = EmployeeCriteria;
			App.SetSessionValue("FindEmployer:SearchCriteria", EmployeeCriteria);
			App.SetSessionValue("FindEmployer:SearchCriteriaApplied", EmployeeCriteria != null);
		}

		#endregion


		/// <summary>
		/// Binds the employees list.
		/// </summary>
		private void BindEmployeesList()
		{
			EmployeesList.DataBind();

			// Set visibility of other controls
			EmployeeSearchHeaderTable.Visible = true;

			ResultCount.Text = CodeLocalise("ResultCount.Text", "{0} results found", SearchResultListPager.TotalRowCount.ToString(CultureInfo.InvariantCulture));
		}

		/// <summary>
		/// Binds the action drop down.
		/// </summary>
		private void BindActionDropDown()
		{
			var actionDropDownData = new List<ListItem>
	                                    {
	                                        new ListItem(CodeLocalise(ActionTypes.AccessEmployeeAccount, "Access #BUSINESS#:LOWER account"), ActionTypes.AccessEmployeeAccount.ToString())
	                                    };
			if (App.Settings.Theme == FocusThemes.Workforce && App.Settings.EnableHiringManagerActivityBackdatingDateSelection)
                actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.AssignActivityToEmployee, "Assign activities/services"), ActionTypes.AssignActivityToEmployee.ToString()));

			if (IsAssistEmployersAdministrator())
			{
				SubActionDropDown.Visible = SubActionButton.Visible = ActivityAssigner.Visible = false;
				actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.EditCompanyAddress, "Edit #BUSINESS#:LOWER contact information"), ActionTypes.EditCompanyAddress.ToString()));

				ActionDropDown.Visible = ActionGoButton.Visible = ActionLabel.Visible = true;
			}

			actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.EditHiringManager, "Edit hiring manager contact information"), ActionTypes.EditHiringManager.ToString()));

			actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.EmailEmployee, "Email #BUSINESS#:LOWER"), ActionTypes.EmailEmployee.ToString()));

			if (App.Settings.IntegrationClient != IntegrationClient.Standalone)
				actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.AssignEmployerToStaff, "Assign #BUSINESS#:LOWER to staff"), ActionTypes.AssignEmployerToStaff.ToString()));

			if (App.Settings.EnablePreferredEmployers)
				actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.MarkAsPreferredEmployer, "Mark as preferred #BUSINESS#:LOWER"), ActionTypes.MarkAsPreferredEmployer.ToString()));

			if (App.User.IsInRole(Constants.RoleKeys.AssistEmployersAccountBlocker) && !App.Settings.DisableTalentAuthentication)
			{
				if (EmployeeCriteria.StatusTypes.IsIn(FindEmployerStatusTypes.Active, FindEmployerStatusTypes.All, FindEmployerStatusTypes.Unapproved))
				{
					actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.BlockAllEmployerEmployeesAccount, "Block all users for this #BUSINESS#:LOWER"), ActionTypes.BlockAllEmployerEmployeesAccount.ToString()));
					actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.BlockEmployeeAccount, "Block this user"), ActionTypes.BlockEmployeeAccount.ToString()));
				}

				if (EmployeeCriteria.StatusTypes.IsIn(FindEmployerStatusTypes.Blocked, FindEmployerStatusTypes.All, FindEmployerStatusTypes.Unapproved))
				{
					actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.UnblockAllEmployerEmployeesAccount, "Unblock all users for this #BUSINESS#:LOWER"), ActionTypes.UnblockAllEmployerEmployeesAccount.ToString()));
					actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.UnblockEmployeeAccount, "Unblock this user"), ActionTypes.UnblockEmployeeAccount.ToString()));
				}
					
			}

			if (App.Settings.EnablePreferredEmployers)
				actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.UnmarkAsPreferredEmployer, "Unmark as preferred #BUSINESS#:LOWER"), ActionTypes.UnmarkAsPreferredEmployer.ToString()));

			actionDropDownData.Add(new ListItem(CodeLocalise(ActionTypes.ViewEmployerProfile, "View #BUSINESS#:LOWER profile"), ActionTypes.ViewEmployerProfile.ToString()));

			ActionDropDown.BindListItem(actionDropDownData.OrderBy(item => item.Text), "- select action -", "ActionDropDown.TopDefault");
		}

		/// <summary> 
		/// Binds the sub action drop down to staff.
		/// </summary>
		private void BindSubActionDropDownToStaff()
		{
			SubActionDropDown.Items.Clear();

			SubActionDropDown.DataSource = ServiceClientLocator.AccountClient(App).GetAssistUsers();
			SubActionDropDown.DataValueField = "Id";
			SubActionDropDown.DataTextField = "Name";
			SubActionDropDown.DataBind();

			SubActionDropDown.Items.AddLocalisedTopDefault("StaffSubActionDropDown.TopDefault", "- select staff member name -");
		}

		/// <summary>
		/// Binds the status dropdown.
		/// </summary>
		private void BindStatusDropdown()
		{
			FindEmployerStatusDropdown.Items.Clear();

			FindEmployerStatusDropdown.Items.AddEnum(FindEmployerStatusTypes.Active, CodeLocalise("FindEmployerStatusTypes.Active", "Active"));
			FindEmployerStatusDropdown.Items.AddEnum(FindEmployerStatusTypes.Blocked, CodeLocalise("FindEmployerStatusTypes.Blocked", "Blocked"));
			FindEmployerStatusDropdown.Items.AddEnum(FindEmployerStatusTypes.Unapproved, CodeLocalise("FindEmployerStatusTypes.Unapproved", "Unapproved"));

			FindEmployerStatusDropdown.Items.AddLocalisedTopDefault("FindEmployerStatusDropdown.TopDefault", "- select -");
		}

		/// <summary>
		/// Binds text boxes if re-applying filter
		/// </summary>
		private void BindEmployerSearchTextFields()
		{
			bool expandPanel = App.GetSessionValue("FindEmployer:SearchCriteriaApplied", false);

			if (EmployeeCriteria.Firstname.IsNotNullOrEmpty())
			{
				FirstNameTextBox.Text = EmployeeCriteria.Firstname;
				expandPanel = true;
			}

			if (EmployeeCriteria.Lastname.IsNotNullOrEmpty())
			{
				LastNameTextBox.Text = EmployeeCriteria.Lastname;
				expandPanel = true;
			}

			if (EmployeeCriteria.EmailAddress.IsNotNullOrEmpty())
			{
				ContactEmailTextBox.Text = EmployeeCriteria.EmailAddress;
				expandPanel = true;
			}

			if (EmployeeCriteria.EmployerCharacteristics.IsNotNull())
			{
				if (EmployeeCriteria.EmployerCharacteristics.EmployerName.IsNotNullOrEmpty())
				{
					EmployerNameTextBox.Text = EmployeeCriteria.EmployerCharacteristics.EmployerName;
					expandPanel = true;
				}

				if (EmployeeCriteria.EmployerCharacteristics.FederalEmployerIdentificationNumbers.IsNotNullOrEmpty())
				{
					var selectedExternalIds = EmployeeCriteria.EmployerCharacteristics.FederalEmployerIdentificationNumbers.Where(id => id.IsNotNullOrEmpty()).Select(id => new UpdateableClientListItem
					{
						Value = id,
						Label = id,
						IsDefault = false
					}).ToList();

					if (selectedExternalIds.Any())
					{
						FEINList.SetSelectedItems(selectedExternalIds);
						expandPanel = true;
					}
				}

			}

			// Expand the Find an Employer Search Panel if it exists within the parent page and any of the fields have values
			if (Parent.FindControl("FindAnEmployerPanelCollapsiblePanelExtender") != null)
			{
				var panelExtender = (CollapsiblePanelExtender)Parent.FindControl("FindAnEmployerPanelCollapsiblePanelExtender");
				panelExtender.Collapsed = !expandPanel;
			}

		}

		/// <summary>
		/// Unbinds the search.
		/// </summary>
		private void UnbindSearch()
		{
			var employeeCriteria = new EmployeeCriteria();

			if (FirstNameTextBox.TextTrimmed().IsNotNullOrEmpty())
				employeeCriteria.Firstname = FirstNameTextBox.TextTrimmed();

			if (LastNameTextBox.TextTrimmed().IsNotNullOrEmpty())
				employeeCriteria.Lastname = LastNameTextBox.TextTrimmed();

			var feins = FEINList.SelectedItems.IsNull()
										? new List<string>()
										: FEINList.SelectedItems.Select(x => x.Value).ToList();

			if (EmployerNameTextBox.TextTrimmed().IsNotNullOrEmpty() || feins.IsNotNullOrEmpty())
			{
				var employerCharacteristics = new EmployerCharacteristicsCriteria();

				if (EmployerNameTextBox.TextTrimmed().IsNotNullOrEmpty())
					employerCharacteristics.EmployerName = EmployerNameTextBox.TextTrimmed();

				if (feins.IsNotNullOrEmpty())
					employerCharacteristics.FederalEmployerIdentificationNumbers = feins;

				employeeCriteria.EmployerCharacteristics = employerCharacteristics;
			}

			if (ContactEmailTextBox.TextTrimmed().IsNotNullOrEmpty())
				employeeCriteria.EmailAddress = ContactEmailTextBox.TextTrimmed();

			if (FindEmployerStatusDropdown.SelectedIndex != 0)
				employeeCriteria.StatusTypes = FindEmployerStatusDropdown.SelectedValueToEnum<FindEmployerStatusTypes>();

			var employeesModel = new FindEmployerViewModel {Criteria = employeeCriteria};

			EmployeeCriteria = employeesModel.Criteria;
		}

		/// <summary>
		/// Gets the selected employee id.
		/// </summary>
		/// <returns></returns>
		private long GetSelectedEmployeeId()
		{
			return (from item in EmployeesList.Items let checkBox = (CheckBox)item.FindControl("SelectorCheckBox") where checkBox.Checked select item)
			 .Select(item => long.Parse(EmployeesList.DataKeys[item.DisplayIndex]["Id"].ToString()))
			 .FirstOrDefault();
		}

		/// <summary>
		/// Gets the selected employer id.
		/// </summary>
		/// <returns></returns>
		private long GetSelectedEmployerId()
		{
			return (from item in EmployeesList.Items let checkBox = (CheckBox)item.FindControl("SelectorCheckBox") where checkBox.Checked select item)
			 .Select(item => long.Parse(EmployeesList.DataKeys[item.DisplayIndex]["EmployerId"].ToString()))
			 .FirstOrDefault();
		}

		/// <summary>
		/// Gets the selected employees.
		/// </summary>
		/// <returns></returns>
		private List<long> GetSelectedEmployees()
		{
			return (from item in EmployeesList.Items
							let checkBox = (CheckBox)item.FindControl("SelectorCheckBox")
							where checkBox.Checked
							select item).Select(
													item =>
													EmployeesList.DataKeys[item.DisplayIndex]["Id"].AsLong()).ToList();
		}

		/// <summary>
		/// Gets the selected business unit id.
		/// </summary>
		/// <returns></returns>
		private long GetSelectedBusinessUnitId()
		{
			return (from item in EmployeesList.Items let checkBox = (CheckBox)item.FindControl("SelectorCheckBox") where checkBox.Checked select item)
			 .Select(item => long.Parse(EmployeesList.DataKeys[item.DisplayIndex]["BusinessUnitId"].ToString()))
			 .FirstOrDefault();
		}

		/// <summary>
		/// Gets the name of the selected business unit.
		/// </summary>
		/// <returns></returns>
		private string GetSelectedBusinessUnitName()
		{
			return (from item in EmployeesList.Items let checkBox = (CheckBox)item.FindControl("SelectorCheckBox") where checkBox.Checked select item)
			 .Select(item => EmployeesList.DataKeys[item.DisplayIndex]["BusinessUnitName"].ToString())
			 .FirstOrDefault();
		}

		/// <summary>
		/// Gets the selected hiring manager user id.
		/// </summary>
		/// <returns></returns>
		private long GetSelectedHiringManagerUserId()
		{
			var employeeId = (from item in EmployeesList.Items let checkBox = (CheckBox)item.FindControl("SelectorCheckBox") where checkBox.Checked select item)
								 .Select(item => long.Parse(EmployeesList.DataKeys[item.DisplayIndex]["Id"].ToString()))
								 .FirstOrDefault();

			var employee = ServiceClientLocator.EmployeeClient(App).GetEmployee(employeeId);

			return ServiceClientLocator.AccountClient(App).GetUserDetails(0, employee.PersonId).UserId;
		}

		/// <summary>
		/// Prepares the assign employer to staff action.
		/// </summary>
		private void PrepareAssignEmployer()
		{
			ActivityAssigner.Visible = false;
			BindSubActionDropDownToStaff();
			SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignToStaffSubActionDropDownRequired.ErrorMessage", "Staff member required");
			SubActionDropDown.Visible = SubActionButton.Visible = true;
		}

		/// <summary>
		/// Clears all selected radio buttons.
		/// </summary>
		private void ClearSelectedRadioButtons()
		{
			foreach (var checkBox in EmployeesList.Items.Select(item => (CheckBox)item.FindControl("SelectorCheckBox")))
			{
				checkBox.Checked = false;
			}
		}

		/// <summary>
		/// Prepares the assign activity action.
		/// </summary>
		private void PrepareAssignActivity()
		{
			SubActionDropDown.Items.Clear();
			SubActionDropDown.Visible = SubActionButton.Visible = false;
			ActivityAssigner.Visible = true;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FindButton.Text = CodeLocalise("Global.Find.Text.NoEdit", "Find");
			ClearButton.Text = CodeLocalise("Global.Clear.Text.NoEdit", "Clear");
			ActionLabel.Text = CodeLocalise("ActionLabel.Text", "Action");
			ActionGoButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
			ActionDropDownRequired.ErrorMessage = CodeLocalise("ActionDropDownRequired.ErrorMessage", "Action required");
			EmployeesListValidator.ErrorMessage = CodeLocalise("EmployerListValidator.ErrorMessage", "Select a record before running this action");
			SubActionButton.Text = CodeLocalise("SubActionButton.Text", "Assign");

			FEINPlaceHolder.Visible = App.Settings.FindEmployerFEINsAllowed > 0;

			CloseFindBusinessWindowErrorMessage = CodeLocalise("CloseFindBusinessWindow.Error.Text", "You can only have one #BUSINESS#:LOWER window open at a time");

            //Enable Activities/Services when EnableHiringManagerActivityBackdatingDateSelection is enabled

            var button = FEINList.FindControl("ItemButton");
            if (!App.Settings.EnableHiringManagerActivityBackdatingDateSelection)
                button.Visible = false;
		}

		/// <summary>
		/// Formats the employees list sorting images.
		/// </summary>
		private void FormatEmployeesListSortingImages()
		{
			// Reset image URLs
			((ImageButton)EmployeesList.FindControl("EmployerNameSortAscButton")).ImageUrl =
					((ImageButton)EmployeesList.FindControl("ContactNameSortAscButton")).ImageUrl =
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortAscButton")).ImageUrl =
					((ImageButton)EmployeesList.FindControl("LocationSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)EmployeesList.FindControl("EmployerNameSortDescButton")).ImageUrl =
					((ImageButton)EmployeesList.FindControl("ContactNameSortDescButton")).ImageUrl =
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortDescButton")).ImageUrl =
					((ImageButton)EmployeesList.FindControl("LocationSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton)EmployeesList.FindControl("EmployerNameSortAscButton")).Enabled =
					((ImageButton)EmployeesList.FindControl("ContactNameSortAscButton")).Enabled =
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortAscButton")).Enabled =
					((ImageButton)EmployeesList.FindControl("LocationSortAscButton")).Enabled =
					((ImageButton)EmployeesList.FindControl("EmployerNameSortDescButton")).Enabled =
					((ImageButton)EmployeesList.FindControl("ContactNameSortDescButton")).Enabled =
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortDescButton")).Enabled =
					((ImageButton)EmployeesList.FindControl("LocationSortDescButton")).Enabled = true;

			switch (EmployeeCriteria.OrderBy.ToLower())
			{
				case Constants.SortOrders.EmployerNameAsc:
					((ImageButton)EmployeesList.FindControl("EmployerNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployeesList.FindControl("EmployerNameSortAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.EmployerNameDesc:
					((ImageButton)EmployeesList.FindControl("EmployerNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployeesList.FindControl("EmployerNameSortDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.ContactNameAsc:
					((ImageButton)EmployeesList.FindControl("ContactNameSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployeesList.FindControl("ContactNameSortAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.ContactNameDesc:
					((ImageButton)EmployeesList.FindControl("ContactNameSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployeesList.FindControl("ContactNameSortDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.PhoneNumberAsc:
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.PhoneNumberDesc:
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployeesList.FindControl("PhoneNumberSortDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.LocationAsc:
					((ImageButton)EmployeesList.FindControl("LocationSortAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)EmployeesList.FindControl("LocationSortAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.LocationDesc:
					((ImageButton)EmployeesList.FindControl("LocationSortDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)EmployeesList.FindControl("LocationSortDescButton")).Enabled = false;
					break;
			}
		}

		/// <summary>
		/// Ares the multiple rows selected.
		/// </summary>
		/// <param name="selectedEmployeeIds">The selected employee ids.</param>
		/// <returns></returns>
		private bool AreMultipleRowsSelected(List<long> selectedEmployeeIds)
		{
			if (selectedEmployeeIds.Count > 1)
			{
				MultipleRowValidator.ErrorMessage = HtmlLocalise("MultipleCheckbox.Error.Text", "This action does not support multiple account selections");
				MultipleRowValidator.IsValid = false;
			}

			return selectedEmployeeIds.Count > 1;
		}

		#region Assign activity to employer

		/// <summary>
		/// Handles the UpdateEmployee event of the SaveSelectedActivity control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ActivityAssignment.ActivitySelectedArgs" /> instance containing the event data.</param>
		protected void ActivityAssignment_ActivitySelected(object sender, ActivityAssignment.ActivitySelectedArgs e)
		{
			var activityId = e.SelectedActivityId;
			var employeesSelected = GetSelectedEmployees();
            bool activityStatus = true;

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				if (employeesSelected.IsNullOrEmpty() || employeesSelected.Count == 0) return;
                activityStatus=ServiceClientLocator.EmployeeClient(App).AssignActivityToMultipleEmployees(activityId, employeesSelected, e.ActionedDate);
			}
			else
			{
				if (employeesSelected.IsNullOrEmpty() || employeesSelected.Count == 0 || AreMultipleRowsSelected(employeesSelected) || e.SelectedActivityId.IsNull()) return;
                activityStatus=ServiceClientLocator.EmployeeClient(App).AssignActivity(activityId, employeesSelected[0], e.ActionedDate);
			}
            if (activityStatus)
            {
                Confirmation.Show(CodeLocalise("AssignActivityConfirmation.Title", "Activity/Service assigned successfully"),
                                                                                        CodeLocalise("AssignActivityConfirmation.Body", "The selected activity/service has been assigned successfully"),
                                                                                        CodeLocalise("CloseModal.Text", "OK"));

                ActionComplete();
            }
            else 
            {
                Confirmation.Show("Validation Error", "Staff does not have permission to perform this action", CodeLocalise("CloseModal.Text", "OK"));
            }

		}

		#endregion

		/// <summary>
		/// Handles the BusinessUnitUpdated event of the EditCompanyModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.EditCompanyModal.BusinessUnitUpdatedEventArgs"/> instance containing the event data.</param>
		protected void EditCompanyModal_BusinessUnitUpdated(object sender, EditCompanyModal.BusinessUnitUpdatedEventArgs e)
		{
			var currentIndex = (from item in EmployeesList.Items let checkBox = (CheckBox)item.FindControl("SelectorCheckBox") where checkBox.Checked select item).Select(item => item.DisplayIndex).FirstOrDefault();
      var employeeId = (long)EmployeesList.DataKeys[currentIndex]["Id"];

			BindEmployeesList();
			SearchResultListPager.Visible = (SearchResultListPager.TotalRowCount > 0);
			FindUpdatePanel.Update();

      var checkItem = EmployeesList.Items.FirstOrDefault(item => (long)(EmployeesList.DataKeys[item.DisplayIndex]["Id"]) == employeeId);
      if (!checkItem.IsNull())
      {
        ((CheckBox)EmployeesList.Items[checkItem.DisplayIndex].FindControl("SelectorCheckBox")).Checked = true;
      }
      else
      {
	      ActionDropDown.Visible = ActionGoButton.Visible = ActionLabel.Visible = false;
				ActionUpdatePanel.Update();
      }
		}

		/// <summary>
		/// Handles the ContactUpdated event of the EditContactModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Code.Controls.User.UpdateContactInformationModal.ContactUpdatedEventArgs"/> instance containing the event data.</param>
		protected void EditContactModal_ContactUpdated(object sender, UpdateContactInformationModal.ContactUpdatedEventArgs e)
		{
			var currentIndex = (from item in EmployeesList.Items let checkBox = (CheckBox)item.FindControl("SelectorCheckBox") where checkBox.Checked select item).Select(item => item.DisplayIndex).FirstOrDefault();
			var employeeId = (long)EmployeesList.DataKeys[currentIndex]["Id"];

			BindEmployeesList();
			FindUpdatePanel.Update();

			var checkItem = EmployeesList.Items.FirstOrDefault(item => (long)(EmployeesList.DataKeys[item.DisplayIndex]["Id"]) == employeeId);

			if (checkItem.IsNull())
			{
				ActionComplete(false);
			}
			else
			{
				((CheckBox)EmployeesList.Items[checkItem.DisplayIndex].FindControl("SelectorCheckBox")).Checked = true;
			}

			Confirmation.Show(CodeLocalise("ReferralEditEmployerConfirmation.Title", "Hiring manager amended"),
														CodeLocalise("ReferralEditEmployerConfirmation.Body", "The hiring manager has been amended."),
														CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Handles the LayoutCreated event of the EmployeesList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmployeesList_LayoutCreated(object sender, EventArgs e)
		{
			// Set labels
			((Literal)EmployeesList.FindControl("EmployerNameHeader")).Text = CodeLocalise("EmployerNameHeader.Text", "#BUSINESS# name");
			((Literal)EmployeesList.FindControl("ContactNameHeader")).Text = CodeLocalise("ContactNameHeader.Text", "Contact name");
			((Literal)EmployeesList.FindControl("PhoneNumberHeader")).Text = CodeLocalise("PhoneNumberHeader.Text", "Phone number");
			((Literal)EmployeesList.FindControl("LocationHeader")).Text = CodeLocalise("LocationHeader.Text", "Location");
		}

		/// <summary>
		/// Handles the Sorting event of the EmployeesList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void EmployeesList_Sorting(object sender, ListViewSortEventArgs e)
		{
			EmployeeCriteria.OrderBy = e.SortExpression;
			FormatEmployeesListSortingImages();
		}
	}
}
