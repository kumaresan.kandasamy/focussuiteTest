﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailTemplateEditor.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EmailTemplateEditor" %>

<asp:Table ID="EditorTable" runat="server" Width="100%">
	<asp:TableRow>
		<asp:TableCell  Width="150" />
		<asp:TableCell />
	</asp:TableRow>
	<asp:TableRow ID="SenderOptionsRow" runat="server" Visible="False">
		<asp:TableCell><%= HtmlLabel("SenderEmailAddress.Label", "Set Sender address")%></asp:TableCell>
		<asp:TableCell>
			<asp:RadioButtonList OnSelectedIndexChanged="SenderEmailRadioButtonList_IndexChanged" AutoPostBack="True" ID="SenderEmailAddressRadioButtonList" runat="server"/>
		</asp:TableCell>
  </asp:TableRow>
	<asp:TableRow ID="SenderOptionsSpacerRow" runat="server" Visible="False">
		<asp:TableCell ColumnSpan="2"><br/></asp:TableCell>
	</asp:TableRow>
	<asp:TableRow ID="ClientSpecificEmailRow" runat="server" Visible="False">
		<asp:TableCell>Please enter email address</asp:TableCell>
		<asp:TableCell>
			<asp:TextBox runat="server" ID="ClientSpecificEmailTextBox" Width="400" MaxLength="200"></asp:TextBox>
			<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="ClientSpecificEmailTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
			<asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="ClientSpecificEmailTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error"/>
		</asp:TableCell>
  </asp:TableRow>
	<asp:TableRow ID="ClientSpecificEmailSpacerRow" runat="server" Visible="False">
		<asp:TableCell ColumnSpan="2"><br/></asp:TableCell>
	</asp:TableRow>
	<asp:TableRow ID="SubjectRow" runat="server">
		<asp:TableCell><%= HtmlLabel("Subject.Label", "Subject") %></asp:TableCell>
		<asp:TableCell><asp:Textbox ID="SubjectTextBox" runat="server" Width="400" MaxLength="1000" ClientIDMode="Static" />&nbsp;
        <asp:DropDownList runat="server" ID="EmailSubjectVariableDropDown" />&nbsp;<asp:Button ID="AddSubjectVariableButton" CommandArgument="subject" runat="server" class="button3" OnCommand="AddVariableButton_Click" /></asp:TableCell>
	</asp:TableRow>
	<asp:TableRow ID="SubjectSpacerRow" runat="server">
		<asp:TableCell ColumnSpan="2"><br/></asp:TableCell>
	</asp:TableRow>
	<asp:TableRow ID="SalutationRow" runat="server">
		<asp:TableCell><%= HtmlLabel("Salutation.Label", "Salutation") %></asp:TableCell>
		<asp:TableCell><asp:Textbox ID="SalutationTextBox" runat="server" Width="200" MaxLength="100" ClientIDMode="Static" /></asp:TableCell>
	</asp:TableRow>
	<asp:TableRow ID="SalutationSpacerRow" runat="server">
		<asp:TableCell ColumnSpan="2"><br/></asp:TableCell>
	</asp:TableRow>
	<asp:TableRow>
		<asp:TableCell VerticalAlign="Top"><%= HtmlLabel("Body.Label", "Body") %></asp:TableCell>
		<asp:TableCell><asp:Textbox ID="BodyTextBox" runat="server" TextMode="MultiLine" Rows="10" tyle="overflow-y:scroll" ClientIDMode="Static" /></asp:TableCell>
	</asp:TableRow>
	<asp:TableRow ID="EmailVariableRow" runat="server" Visible="False">
		<asp:TableCell></asp:TableCell>
		<asp:TableCell>
			<asp:DropDownList runat="server" ID="EmailBodyVariableDropDown" />&nbsp;
      <asp:Button ID="AddBodyVariableButton" CommandArgument="body" runat="server" class="button3" OnCommand="AddVariableButton_Click" />
		</asp:TableCell>
	</asp:TableRow>
</asp:Table>