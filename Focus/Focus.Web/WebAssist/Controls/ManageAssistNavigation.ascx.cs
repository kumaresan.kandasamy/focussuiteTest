﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Web.Code;
using Focus.Common.Extensions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class ManageAssistNavigation : UserControlBase
	{
	  /// <summary>
	  /// Handles the Load event of the Page control.
	  /// </summary>
	  /// <param name="sender">The source of the event.</param>
	  /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void Page_Load(object sender, EventArgs e)
	  {
	    if (App.User.IsInRole(Constants.RoleKeys.SystemAdmin))
	    {
	      LocaliseAssistFeedback.Text = EditLocalisationLink("Global.AssistFeedback.LinkText");
	      LocaliseManageAnnouncements.Text = EditLocalisationLink("Global.ManageAnnouncements.LinkText");
	      LocaliseManageOffices.Text = EditLocalisationLink("Global.ManageOffices.LinkText");
	      LocaliseManageStaffAccounts.Text = EditLocalisationLink("Global.ManageStaffAccounts.LinkText");
	      LocaliseManageStudentAccounts.Text = EditLocalisationLink("Global.ManageStudentAccounts.LinkText");
	      LocaliseManageSystemDefaults.Text = EditLocalisationLink("Global.ManageAssist.LinkText");
          LocaliseManageOffices.Text = EditLocalisationLink("Global.ManageOffices.LinkText");
          LocaliseManageActivityService.Text = EditLocalisationLink("Global.ManageActivitiesServices.LinkText");
	    }
	  }

	  /// <summary>
		/// Determines whether [is system admin user].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is system admin user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsSystemAdminUser()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistSystemAdministrator);
		}

		/// <summary>
		/// Determines whether [is account admin user].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is account admin user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAccountAdminUser()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistAccountAdministrator);
		}

		/// <summary>
		/// Determines whether use is a System Admin or an Account Admin
		/// </summary>
		/// <returns></returns>
		protected bool IsSystemOrAccountAdminUser()
		{
			return App.User.IsInAnyRole(Constants.RoleKeys.AssistSystemAdministrator, Constants.RoleKeys.AssistAccountAdministrator);
		}

		/// <summary>
		/// Determines whether [is broadcasts admin user].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is broadcasts admin user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsBroadcastsAdminUser()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistBroadcastsAdministrator);
		}

    /// <summary>
    /// Determines whether [is student registration administrator].
    /// </summary>
    /// <returns>
    ///   <c>true</c> if [is student registration administrator]; otherwise, <c>false</c>.
    /// </returns>
    protected bool IsStudentAccountAdministrator()
    {
      return App.User.IsInRole(Constants.RoleKeys.AssistStudentAccountAdministrator);
    }

		/// <summary>
		/// Determines whether [is assist manage documents].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is assist manage documents]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistManageDocuments()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistManageDocuments);
		}
		
		/// <summary>
		/// Determines whether [is assist staff view user].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is assist staff view user]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistStaffViewUser()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistStaffView);
		}

		/// <summary>
		/// Determines whether [is system defaults page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is system defaults page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsSystemDefaultsPage()
		{
			return (Page.Is(UrlBuilder.ManageSystemDefaults()));
		}


        protected bool IsManageActivityService()
        {
            return (Page.Is(UrlBuilder.ManageActivityServiceDefaults()));
        }

		/// <summary>
		/// Determines whether [is staff accounts page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is staff accounts page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsStaffAccountsPage()
		{
			return (Page.Is(UrlBuilder.ManageStaffAccounts()));
		}

		protected bool IsOfficePage()
		{
			return (Page.Is(UrlBuilder.ManageOffices()));
		}

    /// <summary>
    /// Determines if the current page is the case management page
    /// </summary>
    /// <returns>True if it is the case management page</returns>
    protected bool IsCaseManagementPage()
    {
      return (Page.Is(UrlBuilder.CaseManagementErrors()));
    }

		/// <summary>
		/// Users the has manage office permissions.
		/// </summary>
		/// <returns></returns>
		protected bool UserHasManageOfficePermissions()
		{
			return (App.User.IsInRole(Constants.RoleKeys.AssistOfficesCreate) ||
			        App.User.IsInRole(Constants.RoleKeys.AssistOfficesListStaff) ||
			        App.User.IsInRole(Constants.RoleKeys.AssistOfficesSetDefault) ||
			        App.User.IsInRole(Constants.RoleKeys.AssistOfficesViewEdit));
		}

		/// <summary>
		/// Determines whether [is student accounts page].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is student accounts page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsStudentAccountsPage()
		{
			return (Page.Is(UrlBuilder.ManageStudentAccounts()));
		}

		/// <summary>
		/// Determines whether [is messages page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is messages page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAnnouncementsPage()
		{
			return (Page.Is(UrlBuilder.ManageAnnouncements()));
		}

		/// <summary>
		/// Determines whether [is documents page].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is documents page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsDocumentsPage()
		{
			return (Page.Is(UrlBuilder.ManageDocuments()));
		}

		/// <summary>
		/// Determines whether [is feedback page].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is feedback page]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsFeedbackPage()
		{
			return (Page.Is(UrlBuilder.AssistFeedback()));
		}

		/// <summary>
		/// Determines whether [is education theme].
		/// </summary>
		/// <returns>
		///   <c>true</c> if this instance is education; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsEducationTheme()
		{
			return (App.Settings.Theme == FocusThemes.Education);
		}

		/// <summary>
		/// Officeses the enabled.
		/// </summary>
		/// <returns></returns>
		protected bool OfficesEnabled()
		{
			return (App.Settings.OfficesEnabled);
		}
	}
}
