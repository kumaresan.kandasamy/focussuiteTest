﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobCharacteristicsCriteriaBuilder.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.JobCharacteristicsCriteriaBuilder" %>

<asp:Panel ID="JobCharacteristicsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
	<asp:Image ID="JobCharacteristicsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>" AlternateText="Job Characteristics Header Image" />
	&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobCharacteristics.Label", "#POSTINGTYPE# characteristics")%></a></span>
</asp:Panel>
<asp:Panel ID="JobCharacteristicsPanel" runat="server">
	<table>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("OccupationGroup.Title", "Occupation group")%></h5></td>
		</tr>
		<tr>
			<td style="width:200;">
				<%= HtmlInFieldLabel("OccupationGroupTextBox", "OccupationGroup.InlineLabel", "o'net title", 150)%>
				<asp:TextBox runat="server" ID="OccupationGroupTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25" />
			</td>
			<td><asp:Button ID="AddOccupationGroupFilterButton" runat="server" CssClass="button3" OnClick="AddOccupationGroupFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("OccupationTitle.Title", "Detailed occupation")%></h5></td>
		</tr>
		<tr>
			<td>
				<%= HtmlInFieldLabel("OccupationTitleTextBox", "OccupationTitle.InlineLabel", "o'net title", 150)%>
				<asp:TextBox runat="server" ID="OccupationTitleTextBox" Text="" Width="150" ClientIDMode="Static" MaxLength="25"  />
			</td>
			<td><asp:Button ID="AddOccupationTitleFilterButton" runat="server" CssClass="button3" OnClick="AddOccupationTitleFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("Salary.Title", "Salary")%></h5></td>
		</tr>
		<tr>
			<td colspan="2">
				<asp:RadioButton ID="HourlySalaryRadioButton" GroupName="SalaryFrequecy" runat="server" Checked="true" ClientIDMode="Static" />
				<asp:RadioButton ID="YearySalaryRadioButton" GroupName="SalaryFrequecy" runat="server" ClientIDMode="Static" />
			</td>
		</tr>
		<tr>
			<td>
				<table>
					<tr>
						<td><%= HtmlLocalise("Global.SystemCurrency.Symbol", "$")%></td>
						<td>
							<%= HtmlInFieldLabel("MinimumSalaryTextBox", "MinimumSalary.InlineLabel", "low", 75)%>
							<asp:TextBox runat="server" ID="MinimumSalaryTextBox" Text="" Width="75" ClientIDMode="Static" MaxLength="7" />
						</td>
						<td><%= HtmlLocalise("SalaryTo.Text", "to")%> <%= HtmlLocalise("Global.SystemCurrency.Symbol", "$")%></td>
						<td>
							<%= HtmlInFieldLabel("MaximumSalaryTextBox", "MaximumSalary.InlineLabel", "high", 75)%>
							<asp:TextBox runat="server" ID="MaximumSalaryTextBox" Text="" Width="75" ClientIDMode="Static" MaxLength="7" />
						</td>	
					</tr>
				</table>
			</td>
			<td><asp:Button ID="AddSalaryFilterButton" runat="server" CssClass="button3" OnClick="AddSalaryFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>	
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("JobStatus.Title", "Show these #POSTINGTYPE#")%></h5></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="PostedJobsCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="RegisteredJobsCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="ExpiredJobsCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="ClosedJobsCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td><asp:Button ID="AddJobStatusFilterButton" runat="server" CssClass="button3" OnClick="AddJobStatusFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("Education.Title", "Level of education")%></h5></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="HighSchoolDiplomaCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="AssociateDegreeCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="BachelorsDegreeCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="GraduateDegreeCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td></td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="ShowUnspecifiedEducationLevelCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td><asp:Button ID="AddEducationFilterButton" runat="server" CssClass="button3" OnClick="AddEducationFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>
		<tr>
			<td colspan="2"><h5><%= HtmlLocalise("SearchTerms.Title", "Show #POSTINGTYPE# with these terms")%></h5></td>
		</tr>
		<tr>
			<td><asp:RadioButton ID="MatchAllTermsRadioButton" GroupName="SearchTermsFilterType" runat="server" Checked="true" ClientIDMode="Static" /></td>
			<td></td>
		</tr>	
		<tr>
			<td><asp:RadioButton ID="MatchSomeTermsRadioButton" GroupName="SearchTermsFilterType" runat="server" ClientIDMode="Static" /></td>
			<td></td>
		</tr>
		<tr>
			<td colspan="2">
				<%= HtmlInFieldLabel("SearchTermsTextBox", "SearchTerms.InlineLabel", "one term per line", 200)%>
				<asp:Textbox ID="SearchTermsTextBox" runat="server" TextMode="MultiLine" Rows="3" Width="200" ClientIDMode="Static" />
			</td>
		</tr>
		<tr>
			<td><asp:Checkbox ID="SearchTermsInJobTitleCheckbox" runat="server" ClientIDMode="Static" /></td>
			<td><asp:Button ID="AddSearchTermsFilterButton" runat="server" CssClass="button3" OnClick="AddSearchTermsFilterButton_Clicked" ClientIDMode="Static" /></td>
		</tr>	
	</table>
</asp:Panel>
<act:CollapsiblePanelExtender ID="JobCharacteristicsPanelExtender" runat="server" 
														TargetControlID="JobCharacteristicsPanel" 
														ExpandControlID="JobCharacteristicsHeaderPanel" 
														CollapseControlID="JobCharacteristicsHeaderPanel" 
														Collapsed="true" 
														ImageControlID="JobCharacteristicsHeaderImage" 
														CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
														ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
														SuppressPostBack="true"
														BehaviorID="JobCharacteristicsPanelBehavior" />