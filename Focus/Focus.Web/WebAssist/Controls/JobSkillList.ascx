<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobSkillList.ascx.cs" Inherits=" Focus.Web.WebAssist.Controls.JobSkillList" %>

<asp:UpdatePanel ID="SkillsListUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
	<ContentTemplate>
		<table style="width:100%;" role="presentation">
			<tr>
				<td style="width:50px;"><strong><%= HtmlLocalise("Global.Show.Text", "Show")%>:</strong></td>
				<td>
					<asp:LinkButton ID="SpecializedSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command" />&nbsp; 
					<asp:LinkButton ID="SoftwareSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command" />&nbsp; 
					<asp:LinkButton ID="FoundationSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command" /><br />
					<asp:LinkButton ID="HeldSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command" />
				</td>
			</tr>
			<tr>
				<td><div style="height:10px;" ></div></td>
			</tr>
		</table>
		<asp:PlaceHolder ID="DemandedSkillsPlaceHolder" runat="server">
			<table style="width:100%;" class="genericTable withHeader">
				<tr>
					<th id="SkillTypeTableCell" runat="server">
						<table role="presentation">
							<tr>
								<td>
									<asp:Label ID="SkillTypeLabel" runat="server" />&nbsp;
								</td>
								<td style="width:70px;vertical-align: top; padding-top: 3px;">
									<div class="tooltip">
										<div class="tooltipWithArrow helpMessage">
											<div class="tooltipImage toolTipNew" title="<%= SkillTypeTooltipText %>"></div>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</th>
				</tr>
				<tr>
					<td style="vertical-align: top; text-align: left;">
					  <asp:Label runat="server" ID="NoSkillsLabel"></asp:Label>
            <asp:PlaceHolder ID="SkillsColumnOnePlaceHolder" runat="server">
						  <table style="width:100%;" role="presentation">
							  <asp:Repeater ID="SkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								  <ItemTemplate>
									  <tr>
										  <td style="vertical-align: top;">
											  <strong>
											    <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											  </strong>
										  </td>
									  </tr>			
								  </ItemTemplate>
							  </asp:Repeater>
						  </table>
            </asp:PlaceHolder>
					</td>
					<asp:PlaceHolder ID="SkillsColumnTwoPlaceHolder" runat="server">
					<td style="vertical-align: top; width:50%; text-align: left;">
						<table style="width:100%;" role="presentation">
							<asp:Repeater ID="SkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td style="vertical-align: top;">
											<strong>
												<asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											</strong>
										</td>
									</tr>			
								</ItemTemplate>
							</asp:Repeater>
						</table>
					</td>
					</asp:PlaceHolder>
				</tr>
			</table>
		</asp:PlaceHolder>
		<asp:PlaceHolder ID="HeldSkillsPlaceHolder" runat="server">
			<table style="width:100%;" class="genericTable withHeader" role="presentation">
				<tr>
					<th>
						<table role="presentation">
							<tr>
								<td>
									<%= HtmlLocalise("OtherSpecializedSkills.Text", "SPECIALIZED SKILLS") %>&nbsp;
								</td>
								<td style="width:70px;vertical-align: top; padding-top: 3px;">
									<div class="tooltip">
										<div class="tooltipWithArrow helpMessage">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("SpecializedSkills.TooltipText", "Successful candidates for this occupation have shown these other skills on their resumes.")%>"></div>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</th>
					<th>
						<table role="presentation">
							<tr>
								<td>
									<%= HtmlLocalise("OtherSoftwareSkills.Text", "SOFTWARE SKILLS") %>&nbsp;
								</td>
								<td style="width:70px;vertical-align: top; padding-top: 3px;">
									<div class="tooltip">
										<div class="tooltipWithArrow helpMessage">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("SoftwareSkills.TooltipText", "Successful candidates for this occupation have shown these other skills on their resumes.")%>"></div>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</th>
				</tr>
        <tr id="NoHeldSkillsRow" runat="server">
          <td colspan="2">
            <asp:Label runat="server" ID="NoHeldSkillsLabel"></asp:Label>
          </td>
        </tr>
				<tr id="HeldSkillsRow" runat="server">
					<td style="vertical-align: top; text-align: left;">
						<table style="width:100%;" role="presentation">
							<asp:Repeater ID="HeldSpecializedSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td style="vertical-align: top;">
											<strong>
												<asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											</strong>
										</td>
									</tr>			
								</ItemTemplate>
							</asp:Repeater>
						</table>						
					</td>
					<td style="vertical-align: top; text-align:left;">
						<table style="width:100%;" role="presentation">
							<asp:Repeater ID="HeldSoftwareSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td style="vertical-align: top;">
											<strong>
												<asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											</strong>
										</td>
									</tr>			
								</ItemTemplate>
							</asp:Repeater>
						</table>						
					</td>
				</tr>
			</table>			
		</asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="HeldFoundationSkillHolder">
            <table style="width:100%;" class="genericTable withHeader">
				<tr>
					<th>
						<table>
							<tr>
								<td>
									<%= HtmlLocalise("OtherFoundationSkills.Text", "FOUNDATION SKILLS") %>&nbsp;
								</td>
								<%--<td style="vertical-align: top;">					
									<img src="<%=ResolveUrl("~/Assets/Images/icon_help.png")%>" width="18" height="17" class="toolTipHover" alt="" />
									<div class="tooltip">
										<div class="tooltipWithArrow helpMessage">
											<div class="tooltipPopup">
												<div class="tooltipPopupInner">
												<div class="arrow"></div>
            							<%= HtmlLocalise("FoundationSkills.TooltipText", "Successful candidates for this occupation have shown these other skills on their resumes.")%>
												</div>
											</div> 
										</div>
									</div>
								</td>--%>
								<td style="width:70px;vertical-align: top; padding-top: 3px;">
									<div class="tooltip">
										<div class="tooltipWithArrow helpMessage">
											<div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("FoundationSkills.TooltipText", "Successful candidates for this occupation have shown these other skills on their resumes.")%>"></div>
										</div>
									</div>
								</td>
							</tr>
						</table>
					</th></tr>
				<tr>
					<td style="vertical-align: top; text-align:left;">
						<table style="width:100%;" role="presentation">
							<asp:Repeater ID="HeldFoundationSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td style="vertical-align: top;">
											<strong>
												<asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											</strong>
										</td>
									</tr>			
								</ItemTemplate>
							</asp:Repeater>
						</table>						
					</td>
				</tr>
			</table>
        </asp:PlaceHolder>
	</ContentTemplate>
</asp:UpdatePanel>