﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TalentDefaults.ascx.cs"
    Inherits="Focus.Web.WebAssist.Controls.TalentDefaults" %>
<%@ Register Src="~/Code/Controls/User/ImageUploader.ascx" TagName="ImageUploader"
    TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagName="ConfirmationModal"
    TagPrefix="uc" %>
<div style="width: 100%; text-align: right">
    <asp:Literal runat="server" ID="LocaliseSaveChangesTopButton" /><asp:Button ID="SaveChangesTopButton"
        runat="server" Text="Save changes" class="button1" OnClick="SaveChangesButton_Click" /></div>
<br />
<asp:Panel ID="ApprovalDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table class="accordionTable" role="presentation">
        <tr class="multipleAccordionTitle">
            <td>
                <asp:Image ID="ApprovalDefaultsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                    AlternateText="Approval defaults" Width="22" Height="22"/>
                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ApprovalDefaults.Label", "Approval defaults")%></a></span>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="ApprovalDefaultsPanel" runat="server">
    <div class="singleAccordionContentWrapper">
        <div class="singleAccordionContent">
            <table style="width: 100%;" role="presentation">
                <tr>
                    <td style="width: 350px;">
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= HtmlLabel(PreferenceDefaultsEmployerRadioButton, "PreferenceDefaultsEmployer.Label", "#BUSINESS# preferences override system defaults")%>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="PreferenceDefaultsEmployerRadioButton" GroupName="PreferenceDefaults"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= HtmlLabel(PreferenceDefaultsSystemRadioButton, "PreferenceDefaultsSystem.Label", "System defaults override #BUSINESS#:LOWER preferences")%>
                    </td>
                    <td>
                        <asp:RadioButton runat="server" ID="PreferenceDefaultsSystemRadioButton" GroupName="PreferenceDefaults" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr id="NewEmployerApprovalRow" runat="server">
                    <td>
                        <%= HtmlLabel(NewEmployerAccountDropDownList, "NewEmployerApproval.Label", "New #BUSINESS#:LOWER account")%>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="NewEmployerAccountDropDownList" onchange="NewEmployerChange();" />
                    </td>
                </tr>
                <tr id="HiringManagerSearchApproval">
                    <td>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="HiringManagerSearchApprovalCheckBox" TextAlign="Right" /><%= HtmlLabel(HiringManagerSearchApprovalCheckBox,"HiringManagerSearchApproval.Label", "Disable Talent Pool Searching")%>
                    </td>
                </tr>
                <%--<tr id="NewBusinessUnitAccountRow" runat="server">
                    <td>
                        <%= HtmlLabel(NewBusinessUnitAccountDropDownList,"NewBusinessUnitAccount.Label", "New business unit account")%>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="NewBusinessUnitAccountDropDownList" onchange="NewBusinessChange();" />
                    </td>
                </tr>
                <tr id="NewBusinessUnitSearchRow" style="display: none">
                    <td>
                    </td>
                    <td>
                        <asp:CheckBox ID="NewBusinessUnitSearchCheckBox" runat="server" />
                        <%= HtmlLabel(NewBusinessUnitSearchCheckBox,"DisableTalentPoolSearching.Text", "Disable Talent Pool searching")%>
                    </td>
                </tr>
                <tr id="NewHirningManagerRow" runat="server">
                    <td>
                        <%= HtmlLabel(NewHiringManagerAccountDropDownList,"NewHiringManagerAccount.Label", "New hiring manager account")%>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="NewHiringManagerAccountDropDownList" onchange="NewHiringChange();" />
                    </td>
                </tr>
                <tr id="NewHiringManagerSearchRow" style="display: none">
                    <td>
                    </td>
                    <td>
                        <asp:CheckBox ID="NewHiringManagerSearchCheckBox" runat="server" />
                        <%= HtmlLabel(NewHiringManagerSearchCheckBox,"DisableTalentPoolSearching.Text", "Disable Talent Pool searching")%>
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        <%= HtmlLabel(NewEmployerCensorshipFilteringCheckBox, "NewEmployerCensorshipFiltering.Label", "Activate new #BUSINESS#:LOWER censorship filtering")%>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="NewEmployerCensorshipFilteringCheckBox" />
                    </td>
                </tr>
                <tr id="ResumeReferralApprovals" runat="server">
                    <td>
                        <%= HtmlLabel("ResumeReferralApprovals.Label", "Screening Preference defaults")%>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ResumeReferralApprovalsDropDownList" />
                    </td>
                </tr>
                <tr id="ScreenPreferencesRow" runat="server">
                    <td>
                        <%= HtmlLabel("ScreeningPrefrencesDefault.Label", "Screening Preference Default")%>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ScreeningPrefrencesDefaultDropDownList" />
                    </td>
                </tr>
            </table>
						<asp:PlaceHolder runat="server" ID="JobApprovalOptionsPlaceHolder">
						<table>
							<tr>
								<th style="width: 350px;"></th>
								<th><%= HtmlLabel("CustomerCreatedJobs.Label", "Customer-created job postings")%></th>
								<th><%= HtmlLabel("StaffCreatedJobs.Label", "Staff-created job postings")%></th>
							</tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:CheckBox runat="server" ID="ActivateYellowWordFilteringCheckBox" />
                                    <%= HtmlLabel(ActivateYellowWordFilteringCheckBox,"ActiveYellowWordFiltering.Label", "Enable yellow-word filter")%>
                                </td>
                            </tr>
							<tr>
								<td><%= HtmlLabel("ApprovalDefaultsForJobPostings.Label", "Approval defaults for job postings")%></td>
								<td>
										<asp:DropDownList runat="server" ID="ApprovalDefaultsForCustomerJobsDropDownList" />
								</td>
								<td>
										<asp:DropDownList runat="server" ID="ApprovalDefaultsForStaffJobsDropDownList" />
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top"><%= HtmlLabel("JobRequirementsForApproval.Label", "Send jobs with these requirements for approval")%></td>
								<td>
									<asp:CheckBoxList runat="server" ID="ApprovalCheckCustomerJobsCheckBoxList" />
								</td>
								<td>
									<asp:CheckBoxList runat="server" ID="ApprovalCheckStaffJobsCheckBoxList" />
								</td>
							</tr>
						</table>
						</asp:PlaceHolder>
        </div>
    </div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="ApprovalDefaultsPanelExtender" runat="server" TargetControlID="ApprovalDefaultsPanel"
    ExpandControlID="ApprovalDefaultsHeaderPanel" CollapseControlID="ApprovalDefaultsHeaderPanel"
    Collapsed="true" ImageControlID="ApprovalDefaultsHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
    ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
<asp:Panel ID="ColourDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table class="accordionTable" role="presentation">
        <tr class="multipleAccordionTitle">
            <td>
                <asp:Image ID="ColourDefaultsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                    alt="." Width="22" Height="22"/>
                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ColourDefaults.Label", "Color defaults")%></a></span>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="ColourDefaultsPanel" runat="server">
    <div class="singleAccordionContentWrapper">
        <div class="singleAccordionContent">
            <table role="presentation">
                <tr>
                    <td>
                        <%= HtmlLabel(LightColourTextBox,"LightColour.Label", "Light color")%>
                    </td>
                    <td>
                        <asp:TextBox ID="LightColourTextBox" runat="server" ClientIDMode="Static" MaxLength="6"
                            Width="100" />
                        <asp:RequiredFieldValidator runat="server" ID="LightColourValidator" ValidationGroup="TalentDefaults"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ControlToValidate="LightColourTextBox" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= HtmlLabel(DarkColourTextBox,"DarkColour.Label", "Dark color")%>
                    </td>
                    <td>
                        <asp:TextBox ID="DarkColourTextBox" runat="server" ClientIDMode="Static" MaxLength="6"
                            Width="100" />
                        <asp:RequiredFieldValidator runat="server" ID="DarkColourValidator" ValidationGroup="TalentDefaults"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ControlToValidate="DarkColourTextBox" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="LocaliseResetColoursButton"></asp:Literal><asp:Button
                            ID="ResetColoursButton" runat="server" class="button3" OnClick="ResetColoursButton_Click" Tooltip="Reset colors"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="ColourDefaultsPanelExtender" runat="server" TargetControlID="ColourDefaultsPanel"
    ExpandControlID="ColourDefaultsHeaderPanel" CollapseControlID="ColourDefaultsHeaderPanel"
    Collapsed="true" ImageControlID="ColourDefaultsHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
    ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
<asp:PlaceHolder ID="JobOrderSettingsPlaceHolder" runat="server">
    <asp:Panel ID="JobOrderSettingsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
        <table class="accordionTable" role="presentation">
            <tr class="multipleAccordionTitle">
                <td>
                    <asp:Image ID="JobOrderSettingsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                        AlternateText="Job order settings" Width="22" Height="22"/>
                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("JobOrderSettings.Label", "#EMPLOYMENTTYPE# management defaults")%></a></span>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="JobOrderSettingsPanel" runat="server">
        <div class="singleAccordionContentWrapper">
            <div class="singleAccordionContent">
                <table style="width: 100%;" role="presentation">
                    <asp:PlaceHolder runat="server" ID="PreScreeningPlaceHolder" Visible="False">
                        <tr>
                            <td style="width: 350px;">
                                <asp:Label runat="server" ID="PreScreeningLabel"></asp:Label>
                            </td>
                            <td>
                                <fieldset>
                                <legend>Pre screening</legend>
                                    <asp:RadioButton ID="PreScreeningYesRadioButton" runat="server" GroupName="PreScreening" />
                                    <label for="<%=PreScreeningYesRadioButton.ClientID %>">
                                        <%=HtmlLabel("Yes.Label", "Yes")%></label>
                                    <asp:RadioButton ID="PreScreeningNoRadioButton" runat="server" GroupName="PreScreening" />
                                    <label for="<%=PreScreeningNoRadioButton.ClientID %>">
                                        <%=HtmlLabel("No.Label", "No")%></label>
                                </fieldset>
                            </td>
                        </tr>
                    </asp:PlaceHolder>
										<tr>
												<td>
														<%= HtmlLabel(RequirementsPreferenceDefaultDropdownList, "RequirementsPreferenceDefault.Label", "Requirements preference default")%>
												</td>
												<td>
														<asp:DropDownList runat="server" ID="RequirementsPreferenceDefaultDropdownList" />
												</td>
										</tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <act:CollapsiblePanelExtender ID="JobOrderSettingsPanelPanelExtender" runat="server"
        TargetControlID="JobOrderSettingsPanel" ExpandControlID="JobOrderSettingsHeaderPanel"
        CollapseControlID="JobOrderSettingsHeaderPanel" Collapsed="true" ImageControlID="JobOrderSettingsHeaderImage"
        CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
        SuppressPostBack="true" />
</asp:PlaceHolder>
<asp:Panel ID="LogoDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table class="accordionTable" role="presentation">
        <tr class="multipleAccordionTitle">
            <td>
                <asp:Image ID="LogoDefaultsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                    alt="." Width="22" Height="22"/>
                &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("LogoDefaults.Label", "Logo defaults")%></a></span>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="LogoDefaultsPanel" runat="server">
    <div class="singleAccordionContentWrapper">
        <div class="singleAccordionContent">
            <table style="width: 100%;" role="presentation">
                <tr>
                    <td colspan="2">
                        <%= HtmlLocalise("LogoRequirements.Label", "Logos must be in GIF, JPG, or PNG format at 72dpi. Maximum size 300 pixels wide by 55 pixels high.")%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px; vertical-align: top;">
                        <%= HtmlLabel("ApplicationLogo.Label", "Application logo")%>
                    </td>
                    <td>
                        <uc:ImageUploader ID="TalentApplicationLogoUploader" runat="server" ImageWidth="300"
                            ImageHeight="55" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%= HtmlLabel(TalentFooterLogoCheckBox,"FooterLogo.Label", "Footer logo")%>
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="TalentFooterLogoCheckBox" TextAlign="Right" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<act:CollapsiblePanelExtender ID="LogoDefaultsPanelExtender" runat="server" TargetControlID="LogoDefaultsPanel"
    ExpandControlID="LogoDefaultsHeaderPanel" CollapseControlID="LogoDefaultsHeaderPanel"
    Collapsed="true" ImageControlID="LogoDefaultsHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>"
    ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
<asp:PlaceHolder runat="server" ID="SharingDefaultsPlaceHolder" Visible="False">
    <asp:Panel ID="SharingDefaultsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
        <table class="accordionTable" role="presentation">
            <tr class="multipleAccordionTitle">
                <td>
                    <asp:Image ID="SharingDefaultsHeaderImage" runat="server" ImageUrl="<%# UrlBuilder.OpenAccordionImage() %>"
                        AlternateText="Sharing defaults" Height="22" Width="22"/>
                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("SharingDefaults.Label", "Sharing defaults")%></a></span>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="SharingDefaultsPanel" runat="server">
        <div class="singleAccordionContentWrapper">
            <div class="singleAccordionContent">
                <table role="presentation">
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="SharingDefaultsEmployerUsersSameFEINCheckBox" />
                        </td>
                        <td>
                            <%= HtmlLabel(SharingDefaultsEmployerUsersSameFEINCheckBox, "SharingDefaultsEmployerUsers.Label", "#BUSINESS# users at the same FEIN may share job postings")%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <act:CollapsiblePanelExtender ID="SharingDefaultsCollapsiblePanelExtender" runat="server"
        TargetControlID="SharingDefaultsPanel" ExpandControlID="SharingDefaultsHeaderPanel"
        CollapseControlID="SharingDefaultsHeaderPanel" Collapsed="true" ImageControlID="SharingDefaultsHeaderImage"
        CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>"
        SuppressPostBack="true" />
</asp:PlaceHolder>

<div style="width: 100%">
    <asp:Button ID="SaveChangesBottomButton" runat="server" class="button1 right" OnClick="SaveChangesButton_Click" /></div>
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
<script type="text/javascript">
    $(document).ready(
		function () {
		    NewEmployerChange();
		    var dropdown = $("#" + TalentDefaults_ClientSideVars.ApprovalDefaultsForCustomerJobsDropDownListClientId);
		    if (dropdown.exists()) {
		        var checkBoxList = $("#" + TalentDefaults_ClientSideVars.ApprovalCheckCustomerJobsCheckBoxListClientId);
		        dropdown.change(function () {
		            TalentDefaults_ApprovalDropDownChange($(this), $("#" + TalentDefaults_ClientSideVars.ApprovalCheckCustomerJobsCheckBoxListClientId));
		        });
		        TalentDefaults_ApprovalDropDownChange(dropdown, checkBoxList);

		        dropdown = $("#" + TalentDefaults_ClientSideVars.ApprovalDefaultsForStaffJobsDropDownListClientId);
		        checkBoxList = $("#" + TalentDefaults_ClientSideVars.ApprovalCheckStaffJobsCheckBoxListClientId);
		        dropdown.change(function () {
		            TalentDefaults_StaffApprovalDropDownChange($(this), $("#" + TalentDefaults_ClientSideVars.ApprovalCheckStaffJobsCheckBoxListClientId));
		        });
		        TalentDefaults_StaffApprovalDropDownChange(dropdown, checkBoxList);
		    }

		    $('#LightColourTextBox').jPicker({
		        window: {
		            position: {
		                x: 'screenCenter',
		                y: 'center'
		            }
		        }
		    });
		    $('#DarkColourTextBox').jPicker({
		        window: {
		            position: {
		                x: 'screenCenter',
		                y: 'center'
		            }
		        }
		    });
		}
	);

	function NewEmployerChange() {
		if ($('#<%=NewEmployerAccountDropDownList.ClientID %>').val() == '<%= ApprovalSystemAvailable %>') {
			$('#HiringManagerSearchApproval').show();
		} else {
            $('#HiringManagerSearchApproval').hide();
        }
        
	}

	function TalentDefaults_ApprovalDropDownChange(dropdown, checkBoxList) {
	    checkBoxList.find("input:checkbox").prop("disabled", dropdown.val() != "SpecifiedApproval");
	}

	function TalentDefaults_StaffApprovalDropDownChange(dropdown, checkBoxList) {
	    checkBoxList.find("input:checkbox").prop("disabled", dropdown.val() != "SpecifiedApproval");
	    if (dropdown.val() == "NoApproval") {
	        $('[id*=ActivateYellowWordFilteringCheckBox]').prop('checked', false);
	        $('[id*=ActivateYellowWordFilteringCheckBox]').prop('disabled', true);

	    }
	    else
    	    $('[id*=ActivateYellowWordFilteringCheckBox]').prop('disabled', false);

	}
</script>
