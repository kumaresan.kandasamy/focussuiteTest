﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HoldEmployerReferralModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.HoldEmployerReferralModal" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget" PopupControlID="ModalPanel" PopupDragHandleControlID="ModalPanelHeader" RepositionMode="RepositionOnWindowResizeAndScroll" BackgroundCssClass="modalBackground">
	<Animations><OnShown><ScriptAction Script="HoldEmployerReferralModal_BindOnHoldOtherReason();" />  </OnShown></Animations>
</act:ModalPopupExtender>
<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:none;width:650px;">
	<asp:Panel runat="server" ID="ModalPanelHeader">
		<h2 data-modal='title'><focus:LocalisedLabel runat="server" ID="NotificationTitleLabel" /></h2>
		<p><focus:LocalisedLabel runat="server" ID="NotificationSubTitleLabel"/></p>
		<p><%= HtmlLabel(EmailSubjectTextBox,"EmailSubject", "Email subject")%></p>
		<asp:TextBox runat="server" Width="400px" ID="EmailSubjectTextBox" AutoCompleteType="Disabled" ></asp:TextBox>
	</asp:Panel>
	<p>
		<asp:Literal ID="TopEmailBodyLiteral" runat="server" />
    <asp:PlaceHolder runat="server" ID="FreeTextOnHoldReasonPlaceholder" Visible="False">
	    <%= HtmlInFieldLabel("OnHoldReasonsTextBox", "OnHoldReasons.InlineLabel", "Your reason for putting this referral on hold", 600)%>
		    <asp:TextBox ID="OnHoldReasonsTextBox" runat="server" TextMode="MultiLine" Rows="4" ClientIDMode="Static" Width="450px" />
		    <asp:RequiredFieldValidator ID="OnHoldReasonsRequired" runat="server" ControlToValidate="OnHoldReasonsTextBox" 
          SetFocusOnError="true" CssClass="error" ValidationGroup="HoldEmployer" Display="Dynamic" /><br />
    </asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="CheckBoxOnHoldReasonsPlaceholder">
			<asp:Panel ID="panOnHoldReasons" runat="server" CssClass="checkbox-panel" >
				<asp:CheckBoxList ID="cblOnHoldReasons" runat="server" CssClass="checkBoxListTable" RepeatDirection="Vertical"></asp:CheckBoxList>
				<asp:CustomValidator ID="OnHoldReasonsCheckboxRequired" runat="server" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="HoldEmployer" ClientValidationFunction="HoldEmployerReferralModal_ValidateOnHoldReasons" ValidateEmptyText="true" />
				<%= HtmlInFieldLabel(OtherOnHoldReasonTextBox.ClientID, "OtherOnHoldReason.InlineLabel", "Your reason for putting this referral on hold", 400 )%>
				<asp:TextBox runat="server" ID="OtherOnHoldReasonTextBox" TextMode="MultiLine" Rows="4" Width="550px" Enabled="False" />
				<asp:RequiredFieldValidator ID="OtherOnHoldReasonRequired" runat="server" ControlToValidate="OtherOnHoldReasonTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="HoldEmployer" Display="Dynamic" Enabled="False" />
			</asp:Panel>
		</asp:PlaceHolder>
		<asp:Literal ID="BottomEmailBodyLiteral" runat="server" />
	</p>
	<asp:PlaceHolder runat="server" ID="AdditionalHoldMessage" Visible="False">
		<%= HtmlLocalise("AdditionalHoldMessage", "Please note this #BUSINESS#:LOWER has multiple employees who are awaiting account approval therefore this notification will be sent to them also.")%>
	</asp:PlaceHolder>	
	<p><asp:CheckBox id="BCCMeCheckBox" runat="server" /></p>
	<p>
		<asp:Button ID="CancelButton" runat="server" class="button4" />
		<asp:Button ID="HoldAndSendMessageButton" runat="server" CssClass="button3" OnClick="HoldAndSendMessageButton_Clicked" ValidationGroup="HoldEmployer"/>
	</p>
</asp:Panel>
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
<script type="text/javascript">
	function HoldEmployerReferralModal_ValidateOnHoldReasons(src, args) {
		args.IsValid = $('#<%=cblOnHoldReasons.ClientID %> input:checkbox').is(':checked');
	}

	function HoldEmployerReferralModal_BindOnHoldOtherReason() {
		$('#<%=cblOnHoldReasons.ClientID %> input:checkbox:last').on('change', function ()
		{
			if ($(this).is(':checked'))
			{
				$('#<%=OtherOnHoldReasonTextBox.ClientID %>').removeProp('disabled');
				ValidatorEnable($("#<%= OtherOnHoldReasonRequired.ClientID %>")[0], true);
			}
			else
			{
				ValidatorEnable($("#<%= OtherOnHoldReasonRequired.ClientID %>")[0], false);
				$('#<%=OtherOnHoldReasonTextBox.ClientID %>').val('').prop('disabled', 'disabled');
			}
		});
	}
</script>
