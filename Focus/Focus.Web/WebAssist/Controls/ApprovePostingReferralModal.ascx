﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApprovePostingReferralModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ApprovePostingReferralModal" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel" 
												PopupDragHandleControlID="ModalPanelHeader"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal">
	<asp:Panel runat="server" ID="ModalPanelHeader">
	<h2 data-modal='title'><%= HtmlLocalise("Title.Text", "Approve posting")%></h2>
	<p><%= HtmlLocalise("Summary.Text", "Notify the employer the posting has been approved and posted.") %></p>
	</asp:Panel>
	<div style="overflow: scroll; height: 200px; width: 450px;">
		<asp:Literal ID="EmailBodyLiteral" runat="server" ClientIDMode="Static" />
	</div>
	<p><asp:CheckBox ID="BCCMeCheckBox" runat="server" /></p>
	<p>
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" />
		<asp:Button ID="ApproveAndSendMessageButton" runat="server" CssClass="button3" OnClick="ApproveAndSendMessageButton_Clicked" />		
	</p>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />