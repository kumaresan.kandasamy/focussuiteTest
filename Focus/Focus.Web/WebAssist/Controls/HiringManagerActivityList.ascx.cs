﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Focus.Web.Code;
using Focus.Web.Core.Models;
using Framework.Core;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Criteria.Employer;
using System.Linq;

#endregion

namespace Focus.Web.WebAssist.Controls
{
    public partial class HiringManagerActivityList : UserControlBase
    {
        #region Properties and Criteria

        private int _activityCount;
        private long _assignActivityId;

        /// <summary>
        /// Gets or sets the business unit criteria.
        /// </summary>
        /// <value>The business unit criteria.</value>
        private EmployeeBusinessUnitCriteria EmployeeBusinessUnitCriteria
        {
            get { return GetViewStateValue<EmployeeBusinessUnitCriteria>("ListEmployeeBusinessUnitActivity:EmployeeBusinessUnitCriteria"); }
            set { SetViewStateValue("ListEmployeeBusinessUnitActivity:EmployeeBusinessUnitCriteria", value); }
        }

        /// <summary>
        /// Gets or sets the selected action.
        /// </summary>
        /// <value>The selected action.</value>
        private ActionTypes? SelectedAction
        {
            get { return GetViewStateValue<ActionTypes>("JobSeekers:SelectedAction"); }
            set { SetViewStateValue("JobSeekers:SelectedAction", value); }
        }

        /// <summary>
        /// Gets or sets the hiring manager profile model.
        /// </summary>
        /// <value>The hiring manager profile model.</value>
        internal HiringManagerProfileModel HiringManagerProfileModel
        {
            get { return GetViewStateValue<HiringManagerProfileModel>("HiringManagerProfile:HiringManagerProfile"); }
            set { SetViewStateValue("HiringManagerProfile:HiringManagerProfile", value); }
        }

        /// <summary>
        /// Gets or sets the job id.
        /// </summary>
        public long EmployeeBusinessUnitId { get; set; }

        /// <summary>
        /// Gets or sets the employee id.
        /// </summary>
        public long EmployeeId
        {
            get { return GetViewStateValue<long>("HiringManagerProfile:EmployeeId"); }
            set { SetViewStateValue("HiringManagerProfile:EmployeeId", value); }
        }

        /// <summary>
        /// Gets or sets the business unit id.
        /// </summary>
        /// <value>The business unit id.</value>
        public long BusinessUnitId { get; set; }

        private static PersonActivityUpdateSettingsDto StaffHMBacdateSetting;

        #endregion

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            StaffHMBacdateSetting = ServiceClientLocator.StaffClient(App).GetBackdateSettings(App.User.PersonId ?? 0);

            if (!IsPostBack)
            {
                if (EmployeeBusinessUnitId == 0)
                    throw new Exception(FormatError(ErrorTypes.EmployeeNotFound, "Invalid business unit employee id"));
                EmployeeBusinessUnitCriteria = new EmployeeBusinessUnitCriteria
                {
                    Id = EmployeeBusinessUnitId,
                    EmployeeId = EmployeeId,
                    BusinessUnitId = BusinessUnitId,
                    DaysBack = 30,
                    OrderBy = Constants.SortOrders.DateReceivedDesc,
                    FetchOption = CriteriaBase.FetchOptions.PagedList
                };

                BindDaysBackDropdown();
                BindServiceDropDown();
                BindUserDropDown();
                BindHiringManagerActivityList();
                BindActionDropDown();
                FormatSortingElements();
                LocaliseUI();
            }
        }

        #region Localise UI

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            ActionGoButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
            ActionDropDownRequired.ErrorMessage = CodeLocalise("ActionDropDownRequired.ErrorMessage", "Action required");
            SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignToStaffSubActionDropDownRequired.ErrorMessage", "Staff member required");
            SubActionButton.Text = CodeLocalise("SubActionButton.Text", "Assign");
        }

        #endregion

        #region Data retrieval

        /// <summary>
        /// Gets the hiring manager activity.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="startRowIndex">Start index of the row.</param>
        /// <param name="maximumRows">The maximum rows.</param>
        /// <returns></returns>
        public List<HiringManagerActivityViewDto> GetHiringManagerActivity(EmployeeBusinessUnitCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
        {
            var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

            if (criteria.IsNull())
            {
                _activityCount = 0;
                return null;
            }

            criteria.PageSize = maximumRows;
            criteria.PageIndex = pageIndex;

            var activites = ServiceClientLocator.EmployeeClient(App).GetHiringManagerActivities(criteria);
            _activityCount = activites.TotalCount;
            return activites;
        }

        /// <summary>
        /// Gets the hiring manager activity count.
        /// </summary>
        /// <returns></returns>
        public int GetHiringManagerActivityCount()
        {
            return _activityCount;
        }

        /// <summary>
        /// Gets the hiring manager activity count.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public int GetHiringManagerActivityCount(EmployeeBusinessUnitCriteria criteria)
        {
            return _activityCount;
        }

        #endregion

        #region Formatting

        /// <summary>
        /// Formats the sorting elements.
        /// </summary>
        private void FormatSortingElements()
        {

            if (!HiringManagerActivityListPager.Visible) return;

            var userNameSortAscButton = (ImageButton)HiringManagerActivityLogList.FindControl("UserNameSortAscButton");
            var userNameSortDescButton = (ImageButton)HiringManagerActivityLogList.FindControl("UserNameSortDescButton");
            var activityDateSortAscButton = (ImageButton)HiringManagerActivityLogList.FindControl("ActivityDateSortAscButton");
            var activityDateSortDescButton = (ImageButton)HiringManagerActivityLogList.FindControl("ActivityDateSortDescButton");

            userNameSortAscButton.ImageUrl = activityDateSortAscButton.ImageUrl = UrlBuilder.CategorySortAscImage();
            userNameSortDescButton.ImageUrl = activityDateSortDescButton.ImageUrl = UrlBuilder.CategorySortDescImage();

            userNameSortAscButton.Enabled =
                activityDateSortAscButton.Enabled = userNameSortDescButton.Enabled = activityDateSortDescButton.Enabled = true;

            switch (EmployeeBusinessUnitCriteria.OrderBy)
            {
                case Constants.SortOrders.ActivityUsernameAsc:
                    userNameSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
                    userNameSortAscButton.Enabled = false;
                    break;

                case Constants.SortOrders.ActivityUsernameDesc:
                    userNameSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
                    userNameSortDescButton.Enabled = false;
                    break;

                case Constants.SortOrders.ActivityDateAsc:
                    activityDateSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
                    activityDateSortAscButton.Enabled = false;
                    break;

                default:
                    activityDateSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
                    activityDateSortDescButton.Enabled = false;
                    break;
            }
        }

        #endregion

        #region Bind Controls

        /// <summary>
        /// Binds the job activity list.
        /// </summary>
        private void BindHiringManagerActivityList()
        {
            HiringManagerActivityLogList.DataBind();

            // Set visibility of other controls
            HiringManagerActivityListPager.Visible = (HiringManagerActivityListPager.TotalRowCount > 0);
        }

        /// <summary>
        /// Binds the service drop down.
        /// </summary>
        private void BindServiceDropDown()
        {
            ServiceDropDown.Items.Clear();

            ServiceDropDown.Items.Add(new ListItem(CodeLocalise("ServiceDropDown.AllActivities", "All activities")));

            if (App.Settings.Theme == FocusThemes.Workforce && App.Settings.EnableHiringManagerActivityBackdatingDateSelection)
                ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignActivityToEmployee, "Assigned activities/services"), ActionTypes.AssignActivityToEmployee.ToString()));

            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.BlockEmployeeAccount, "Account blocked"), ActionTypes.BlockEmployeeAccount.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.UnblockEmployeeAccount, "Account unblocked"), ActionTypes.UnblockEmployeeAccount.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.BlockAllEmployerEmployeesAccount, "All users in FEIN blocked"), ActionTypes.BlockAllEmployerEmployeesAccount.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.UnblockAllEmployerEmployeesAccount, "All users in FEIN unblocked"), ActionTypes.UnblockAllEmployerEmployeesAccount.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.CloseJob, "#EMPLOYMENTTYPES# closed"), ActionTypes.CloseJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.CreateJob, "#EMPLOYMENTTYPES# drafted"), ActionTypes.CreateJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.SaveJob, "#EMPLOYMENTTYPES# edited"), ActionTypes.SaveJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.HoldJob, "#EMPLOYMENTTYPES# on hold"), ActionTypes.HoldJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.RefreshJob, "#EMPLOYMENTTYPES# refreshed"), ActionTypes.RefreshJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.ReactivateJob, "#EMPLOYMENTTYPES# reactivated"), ActionTypes.ReactivateJob.ToString()));
            ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.PostJob, "#EMPLOYMENTTYPES# posted"), ActionTypes.PostJob.ToString()));
        }

        /// <summary>
        /// Binds the days back dropdown.
        /// </summary>
        private void BindDaysBackDropdown()
        {
            DaysBackDropDown.Items.Clear();
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.TheLastDay", "the last day"), "1"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The7Days", "last 7 days"), "7"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The7Days", "last 14 days"), "14"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The30Days", "last 30 days"), "30"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The60Days", "last 60 days"), "60"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The90Days", "last 90 days"), "90"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The180Days", "last 180 days"), "180"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.AllDays.NoEdit", "all"), "0"));
            DaysBackDropDown.SelectedIndex = 3;
        }

        /// <summary>
        /// Binds the user drop down.
        /// </summary>
        private void BindUserDropDown()
        {
            UserDropDown.Items.Clear();
            UserDropDown.Items.Add(new ListItem(CodeLocalise("HiringManagerUserDropDown.AllUsers", "All users")));
            var users = ServiceClientLocator.EmployeeClient(App).GetEmployeeBusinessUnitActivityUsers(EmployeeBusinessUnitId);
            if (users.IsNullOrEmpty())
                return;

            foreach (var user in users)
            {
                UserDropDown.Items.Add(new ListItem(user.EmployeeLastName + ", " + user.EmployeeFirstName, user.UserId.ToString()));
            }
        }

        /// <summary>
        /// Binds the action description.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="literal">The control.</param>
        private void BindActionDescription(HiringManagerActivityViewDto action, Literal literal)
        {
            ActionTypes actionType;
            if (!Enum.TryParse(action.ActionName, out actionType)) return;

            var textUnknown = "** " + CodeLocalise("Global.Unknown.Label", "Unknown") + " **";
            var text = "";

            switch (actionType)
            {
                case ActionTypes.CreateJob:
                    text = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# created");
                    break;

                case ActionTypes.PostJob:
                    text = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# posted");
                    break;

                case ActionTypes.SaveJob:
                    text = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# edited");
                    break;

                case ActionTypes.RefreshJob:
                    text = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# refreshed");
                    break;

                case ActionTypes.ReactivateJob:
                    text = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# reactivated");
                    break;

                case ActionTypes.ApprovePostingReferral:
                    text = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# approved");
                    break;

                case ActionTypes.HoldJob:
                    text = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# on hold");
                    break;

                case ActionTypes.CloseJob:
                    text = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# closed");
                    break;

                case ActionTypes.UpdateApplicationStatusToRecommended:
                    text = CodeLocalise("UpdateApplicationStatusToRecommended", "Referral outcome updated to 'Recommended'");
                    break;

                case ActionTypes.UpdateApplicationStatusToFailedToShow:
                    text = CodeLocalise("UpdateApplicationStatusToFailedToShow", "Referral outcome updated to 'Failed to show'");
                    break;

                case ActionTypes.UpdateApplicationStatusToHired:
                    text = CodeLocalise("UpdateApplicationStatusToHired", "Referral outcome updated to 'Hired'");
                    break;

                case ActionTypes.UpdateApplicationStatusToInterviewScheduled:
                    text = CodeLocalise("UpdateApplicationStatusToInterviewScheduled", "Referral outcome updated to 'Interview scheduled'");
                    break;

                case ActionTypes.UpdateApplicationStatusToNewApplicant:
                    text = CodeLocalise("UpdateApplicationStatusToNewApplicant", "Referral outcome updated to 'New applicant'");
                    break;

                case ActionTypes.UpdateApplicationStatusToNotApplicable:
                    text = CodeLocalise("UpdateApplicationStatusToNotApplicable", "Referral outcome updated to 'Not applicable'");
                    break;

                case ActionTypes.UpdateApplicationStatusToOfferMade:
                    text = CodeLocalise("UpdateApplicationStatusToOfferMade", "Referral outcome updated to 'Offer made'");
                    break;

                case ActionTypes.UpdateApplicationStatusToNotHired:
                    text = CodeLocalise("UpdateApplicationStatusToNotHired", "Referral outcome updated to 'Not hired'");
                    break;

                case ActionTypes.UpdateApplicationStatusToUnderConsideration:
                    text = CodeLocalise("UpdateApplicationStatusToUnderConsideration", "Referral outcome updated to 'Under consideration'");
                    break;

                case ActionTypes.UpdateApplicationStatusToDidNotApply:
                    text = CodeLocalise("UpdateApplicationStatusToDidNotApply", "Referral outcome updated to 'Did not apply'");
                    break;

                case ActionTypes.UpdateApplicationStatusToInterviewDenied:
                    text = CodeLocalise("UpdateApplicationStatusToInterviewDenied", "Referral outcome updated to 'Interview denied'");
                    break;

                case ActionTypes.UpdateApplicationStatusToRefusedOffer:
                    text = CodeLocalise("UpdateApplicationStatusToRefusedJob", "Referral outcome updated to 'Refused job'");
                    break;

                case ActionTypes.UpdateApplicationStatusToSelfReferred:
                    text = CodeLocalise("UpdateApplicationStatusToSelfReferred", "Referral outcome updated to 'Self referred'");
                    break;

                case ActionTypes.EmailEmployee:
                    text = CodeLocalise("EmailEmployee", "Email sent to #BUSINESS#:LOWER");
                    break;

                case ActionTypes.AssignEmployeeToStaff:
                    text = String.Format("{0} ({1})", CodeLocalise("AssignEmployerToStaff", "#BUSINESS# assigned to staff "), action.AdditionalName);
                    break;

                case ActionTypes.AssignActivityToEmployee:
                    var activity = ServiceClientLocator.CoreClient(App).GetActivity(action.EntityIdAdditional01.GetValueOrDefault());
                    text = activity.IsNull() ? String.Format("{0} ({1})", CodeLocalise("AssignActivity", "Assign activity"), CodeLocalise("UnknownActivity", "Unknown activity")) : String.Format("{0} ({1}/{2})", CodeLocalise("AssignActivity", "Assign activity"), CodeLocalise(activity.CategoryLocalisationKey, activity.CategoryName), CodeLocalise(activity.ActivityLocalisationKey, activity.ActivityName));
                    break;

                case ActionTypes.BlockEmployeeAccount:
                    text = CodeLocalise("BlockEmployeeAccount", "Account blocked");
                    break;

                case ActionTypes.BlockAllEmployerEmployeesAccount:
                    text = CodeLocalise("BlockAllEmployerEmployeesAccount", "All users in FEIN blocked");
                    break;

                case ActionTypes.UnblockEmployeeAccount:
                    text = CodeLocalise("UnblockEmployeeAccount", "Account unblocked");
                    break;

                case ActionTypes.UnblockAllEmployerEmployeesAccount:
                    text = CodeLocalise("UnblockAllEmployerEmployeesAccount", "All users in FEIN unblocked");
                    break;
            }

            literal.Text = text.IsNotNullOrEmpty() ? text : textUnknown;

            // Referral outcomes will return JobSeekerName
            if (action.AdditionalName.IsNotNullOrEmpty() && action.JobId.IsNotNull())
                literal.Text += String.Format(" ({0}, {1})", action.AdditionalName, action.JobTitle);

            // Job Actions will return JobId and title
            else if (action.JobId != null)
                literal.Text += String.Format(" ({0}, ID {1})", action.JobTitle, action.JobId);
        }

        /// <summary>
        /// Binds the action drop down.
        /// </summary>
        private void BindActionDropDown()
        {
            ActionDropDown.Items.Clear();
            ActionDropDown.Items.AddLocalisedTopDefault("ActionDropDown.TopDefault", "- select action -");

            if (App.Settings.EnableHiringManagerActivityBackdatingDateSelection)
                ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignActivityToEmployee, "Assign activities/services"), ActionTypes.AssignActivityToEmployee.ToString()));
            ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.EmailEmployee, "Contact #BUSINESS#"), ActionTypes.EmailEmployee.ToString()));

            //TODO: this has just been commented out for now as Helen just wanted the item hidden in case the requirements changed again.  However, when removed this item and the related methods can be removed (23/09/2013)
            //if (App.Settings.OfficesEnabled)
            //  ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignEmployerToStaff, "Assign Employer to staff"), ActionTypes.AssignEmployerToStaff.ToString()));

            ActionDropDown.SelectedIndex = 0;
        }

        /// <summary>
        /// Binds the sub action drop down to employer.
        /// </summary>
        private void BindSubActionDropDownToEmployer()
        {
            SubActionDropDown.Items.Clear();

            SubActionDropDown.DataSource = ServiceClientLocator.AccountClient(App).GetAssistUsers();
            SubActionDropDown.DataValueField = "Id";
            SubActionDropDown.DataTextField = "Name";
            SubActionDropDown.DataBind();

            SubActionDropDown.Items.AddLocalisedTopDefault("EmployerSubActionDropDown.TopDefault", "- select staff member name -");
        }

        #endregion

        # region Events

        /// <summary>
        /// Handles the Selecting event of the HiringManagerActivityListDataSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
        protected void HiringManagerActivityListDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["criteria"] = EmployeeBusinessUnitCriteria;
        }

        /// <summary>
        /// Handles the Sorting event of the HiringManagerActivityLogList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
        protected void HiringManagerActivityLogList_Sorting(object sender, ListViewSortEventArgs e)
        {
            EmployeeBusinessUnitCriteria.OrderBy = e.SortExpression;
            FormatSortingElements();
        }

        /// <summary>
        /// Handles the LayoutCreated event of the HiringManagerActivityLogList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void HiringManagerActivityLogList_LayoutCreated(object sender, EventArgs e)
        {
            // Set labels
            ((Literal)HiringManagerActivityLogList.FindControl("UserName")).Text = CodeLocalise("UserName.Text", "USER NAME");
            ((Literal)HiringManagerActivityLogList.FindControl("ActivityDate")).Text = CodeLocalise("ActivityDate.Text", "DATE");
            ((Literal)HiringManagerActivityLogList.FindControl("Action")).Text = CodeLocalise("Action.Text", "ACTIVITY/SERVICE");
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the DaysBackDropdown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void DaysBackDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            EmployeeBusinessUnitCriteria.DaysBack = Convert.ToInt32(DaysBackDropDown.SelectedValue);
            HiringManagerActivityListPager.ReturnToFirstPage();
            BindHiringManagerActivityList();
            FormatSortingElements();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the UserDropDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void UserDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UserDropDown.SelectedIndex == 0)
                EmployeeBusinessUnitCriteria.UserId = null;
            else
                EmployeeBusinessUnitCriteria.UserId = Convert.ToInt64(UserDropDown.SelectedValue);

            HiringManagerActivityListPager.ReturnToFirstPage();
            BindHiringManagerActivityList();
            FormatSortingElements();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ServiceDropDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ServiceDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ServiceDropDown.SelectedIndex == 0)
                EmployeeBusinessUnitCriteria.Action = null;
            else
            {
                ActionTypes actionType;
                Enum.TryParse(ServiceDropDown.SelectedValue, out actionType);
                EmployeeBusinessUnitCriteria.Action = actionType;
            }

            HiringManagerActivityListPager.ReturnToFirstPage();
            BindHiringManagerActivityList();
            FormatSortingElements();
        }

        /// <summary>
        /// Handles the ItemDataBound event of the HiringManagerActivityLogList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
        protected void HiringManagerActivityLogList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var action = (HiringManagerActivityViewDto)e.Item.DataItem;
            var literal = (Literal)e.Item.FindControl("ActionDescription");

            BindActionDescription(action, literal);

            var backdateButton = (LinkButton)e.Item.FindControl("HiringManagerBackdateLinkbutton");
            var deleteButton = (LinkButton)e.Item.FindControl("HiringManagerDeleteLinkButton");

            var dbStaffHMBacdateSetting = ServiceClientLocator.StaffClient(App).GetBackdateSettings(App.User.PersonId ?? 0);

            #region Toggle Backdate/Delete LinkButtons
            //check whether Activity Backdate/Delete settings exist for the staff and Activity Backdate/Delete is Enable for Job seeker
            if (dbStaffHMBacdateSetting.IsNotNull() && App.Settings.EnableHiringManagerActivityBackdatingDateSelection && action.ActionName.Equals(ActionTypes.AssignActivityToEmployee.ToString()))
            {
                var currentUserRoles = ServiceClientLocator.AccountClient(App).GetUsersRoles(App.User.UserId);

                //set visibility if Staff has permissions to Backdate/Delete Any Activity.
                //backdateButton.Visible = App.User.IsInRole(Constants.RoleKeys.BackdateAnyEntryHMActivityLogMaxDays);
                backdateButton.Visible = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.BackdateAnyEntryHMActivityLogMaxDays)).IsNotNull() ? true : false;
                if (backdateButton.Visible)
                    backdateButton.Visible = dbStaffHMBacdateSetting.BackdateAnyEntryHMActivityLogMaxDays.IsNotNull() ? (action.ActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffHMBacdateSetting.BackdateAnyEntryHMActivityLogMaxDays))) : false;

                //deleteButton.Visible = App.User.IsInRole(Constants.RoleKeys.DeleteAnyEntryHMActivityLogMaxDays);
                deleteButton.Visible = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.DeleteAnyEntryHMActivityLogMaxDays)).IsNotNull() ? true : false;
                if (deleteButton.Visible)
                    deleteButton.Visible = dbStaffHMBacdateSetting.DeleteAnyEntryHMActivityLogMaxDays.IsNotNull() ? (action.ActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffHMBacdateSetting.DeleteAnyEntryHMActivityLogMaxDays))) : false;

                var staffPersonDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId, App.User.PersonId ?? 0);
                //check whether the current staff is a manager before setting visibility to activities.
                if (staffPersonDetails.PersonDetails.Manager.IsNotNull() && (bool)staffPersonDetails.PersonDetails.Manager && !ServiceClientLocator.EmployerClient(App).IsPersonStatewide(App.User.PersonId ?? 0))
                {
                    //get the list of offices current staff is mapped with
                    var officesAssigned = staffPersonDetails.PersonOffices.Select(x => x.OfficeId).Cast<long>().ToList();
                    //get the list of offices activity staff is mapped with
                    var activityStaffUser = ServiceClientLocator.AccountClient(App).GetUserDetails(action.UserId);
                    if (!ServiceClientLocator.EmployerClient(App).IsPersonStatewide(activityStaffUser.PersonDetails.Id ?? 0))
                    {
                        var activityUserOffices = activityStaffUser.PersonOffices.Select(o => o.OfficeId).Cast<long>().ToList();
                        if (backdateButton.Visible)
                            backdateButton.Visible = officesAssigned.Intersect<long>(activityUserOffices).Any();
                        if (deleteButton.Visible)
                            deleteButton.Visible = officesAssigned.Intersect<long>(activityUserOffices).Any();
                    }
                }

                //check if the activity is staff's own before setting visibility for Backdate/Delete my entries
                if (action.UserId == App.User.UserId)
                {
                    //set visibility if staff has permission only to update his own Activity.
                    if (!backdateButton.Visible)
                    {
                        //backdateButton.Visible = App.User.IsInRole(Constants.RoleKeys.BackdateMyEntriesHMActivityLogMaxDays);
                        backdateButton.Visible = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.BackdateMyEntriesHMActivityLogMaxDays)).IsNotNull() ? true : false;
                        if (backdateButton.Visible)
                            backdateButton.Visible = dbStaffHMBacdateSetting.BackdateMyEntriesHMActivityLogMaxDays.IsNotNull() ? (action.ActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffHMBacdateSetting.BackdateMyEntriesHMActivityLogMaxDays))) : false;
                    }
                    if (!deleteButton.Visible)
                    {
                        //deleteButton.Visible = App.User.IsInRole(Constants.RoleKeys.DeleteMyEntriesHMActivityLogMaxDays);
                        deleteButton.Visible = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.DeleteMyEntriesHMActivityLogMaxDays)).IsNotNull() ? true : false;
                        if (deleteButton.Visible)
                            deleteButton.Visible = dbStaffHMBacdateSetting.DeleteMyEntriesHMActivityLogMaxDays.IsNotNull() ? (action.ActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffHMBacdateSetting.DeleteMyEntriesHMActivityLogMaxDays))) : false;
                    }
                }
            }
            else
                backdateButton.Visible = deleteButton.Visible = false;
            #endregion
        }

        /// <summary>
        /// Handles the Clicked event of the ActionButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ActionButton_Clicked(object sender, EventArgs e)
        {
            SelectedAction = ActionDropDown.SelectedValueToEnum<ActionTypes>();

            switch (SelectedAction)
            {
                case ActionTypes.AssignActivityToEmployee:
                    ActivityAssigner.Visible = true;
                    SubActionDropDownLabel.Visible = SubActionDropDown.Visible = SubActionButton.Visible = false;
                    break;

                case ActionTypes.EmailEmployee:
                    ActivityAssigner.Visible = SubActionDropDown.Visible = SubActionButton.Visible = false;
                    EmailEmployee.Show(EmployeeId);
                    break;

                case ActionTypes.AssignEmployerToStaff:
                    ActivityAssigner.Visible = false;
                    BindSubActionDropDownToEmployer();
                    SubActionDropDownLabel.Visible = SubActionDropDown.Visible = SubActionButton.Visible = true;
                    break;
            }
        }

        /// <summary>
        /// Handles the Clicked event of the SubActionButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SubActionButton_Clicked(object sender, EventArgs e)
        {
            switch (SelectedAction)
            {
                case ActionTypes.AssignEmployerToStaff:
                    {
                        var adminId = SubActionDropDown.SelectedValueToLong();
                        var employeeId = EmployeeId;

                        ServiceClientLocator.EmployerClient(App).AssignEmployerToAdmin(adminId, employeeId);

                        Confirmation.Show(CodeLocalise("AssignEmployerToAdminConfirmation.Title", "#BUSINESS# assigned"),
                                                                                    CodeLocalise("AssignEmployerToAdminConfirmation.Body", "The #BUSINESS#:LOWER was assigned to the selected admin."),
                                                                                    CodeLocalise("CloseModal.Text", "OK"));

                        break;
                    }
            }
        }

        #region CascadingDropDown Save Selected Activity

        /// <summary>
        /// Handles the UpdateEmployee event of the SaveSelectedActivity control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Focus.Web.Code.Controls.User.CascadingDropDownSelector.SelectedActivityEventArgs"/> instance containing the event data.</param>
        protected void ActivityAssignment_ActivitySelected(object sender, ActivityAssignment.ActivitySelectedArgs e)
        {
            if (EmployeeBusinessUnitCriteria.EmployeeId != null && e.SelectedActivityId.IsNotNull())
            {
                var employeeId = EmployeeBusinessUnitCriteria.EmployeeId.Value;
                _assignActivityId = e.SelectedActivityId;

                bool activityStatus = ServiceClientLocator.EmployeeClient(App).AssignActivity(_assignActivityId, employeeId, e.ActionedDate);

                if (activityStatus)
                {

                    HiringManagerActivityLogList.DataBind();
                    Confirmation.Show(CodeLocalise("AssignActivityConfirmation.Title", "Activity service assigned to #BUSINESS#:LOWER"),
                                                            CodeLocalise("AssignActivityConfirmation.Body", "The activity service was assigned to the #BUSINESS#:LOWER."),
                                                            CodeLocalise("CloseModal.Text", "OK"));
                }
                else
                {
                    Confirmation.Show("Validation Error", "Staff does not have permission to perform this action", CodeLocalise("CloseModal.Text", "OK"));
                }
            }
        }

        #endregion

        #endregion

        /// <summary>
        /// Handles the select event of the items in list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void HiringManagerActivityLogList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            var activityId = commandArgs[0];
            if (commandArgs.Length > 1)
            {
                var activityTypeId = !commandArgs[1].ToString().Equals(null) ? commandArgs[1] : null;

                var dlUserId = !commandArgs[2].ToString().Equals(null) ? commandArgs[2] : null;
                var dlActionType = !commandArgs[3].ToString().Equals(null) ? commandArgs[3] : null;
                var dlActionedOn = !commandArgs[4].ToString().Equals(null) ? commandArgs[4] : null;

                var activityActionEventDate = ServiceClientLocator.CoreClient(App).GetActionEventDate(Convert.ToInt64(activityId));

                switch (e.CommandName)
                {
                    case "backdateActivity":
                        HMBackdateActivity.show(Convert.ToInt64(activityId), activityActionEventDate, ActivityType.Employer, activityTypeId, Convert.ToInt64(dlUserId), dlActionType, Convert.ToDateTime(dlActionedOn));
                        break;
                    case "deleteActivity":
                        HMDeleteActivity.show(Convert.ToInt64(activityId), activityTypeId, Convert.ToInt64(dlUserId), dlActionType, Convert.ToDateTime(dlActionedOn));
                        break;
                }
            }
        }

        protected virtual void HMBackdateActivity_ActivityBackdated(object sender, BackdateActivityModal.ActivityBackdatedEventArgs e)
        {
            if (e.isBackdated)
                BindHiringManagerActivityList();
        }

        protected virtual void HMDeleteActivity_ActivityDeleted(object sender, DeleteModal.DeleteActivityEventArgs e)
        {
            if (e.isDeleted)
                BindHiringManagerActivityList();
        }
    }
}
