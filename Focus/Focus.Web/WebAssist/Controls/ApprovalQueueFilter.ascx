﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApprovalQueueFilter.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ApprovalQueueFilter" %>


<table style="border-collapse:collapse;" role="presentation">
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
				<%= HtmlLocalise("Show.Label", "Show")%>
			&nbsp;
		</td>
		<td>
			<focus:LocalisedLabel runat="server" ID="JobTypeDropDownListLabel" AssociatedControlID="JobTypeDropDownList" LocalisationKey="JobType" DefaultText="Job type" CssClass="sr-only"/>
			<asp:DropDownList ID="JobTypeDropDownList" Visible="true" runat="server" CausesValidation="false" />
		</td>
		<td>
			<focus:LocalisedLabel runat="server" ID="EmployerTypeDropDownListLabel" AssociatedControlID="EmployerTypeDropDownList" LocalisationKey="Employer type" DefaultText="#BUSINESS# type" CssClass="sr-only" Visible="False"/>
			<asp:DropDownList ID="EmployerTypeDropDownList" Visible="false" runat="server" CausesValidation="false" />
		</td>
		<td>
			<focus:LocalisedLabel runat="server" ID="JobSeekerTypeDropDownListLabel" AssociatedControlID="JobSeekerTypeDropDownList" LocalisationKey="JobSeekerType" DefaultText="Job seeker type" CssClass="sr-only" Visible="False"/>
			<asp:DropDownList ID="JobSeekerTypeDropDownList" Visible="false" runat="server" CausesValidation="false" />
		</td>
		<asp:PlaceHolder runat="server" ID="OfficePlaceHolder">
			<td>
				&nbsp; 
					<%= HtmlLabel(LocationDropDownList,"ForThisOffice.Label", "for this office")%>
				&nbsp;
			</td>
			<td><asp:DropDownList ID="LocationDropDownList" Visible="true" runat="server" CausesValidation="false" /></td>
		</asp:PlaceHolder>
	</tr>
	<tr><td>&nbsp;</td></tr>
</table>