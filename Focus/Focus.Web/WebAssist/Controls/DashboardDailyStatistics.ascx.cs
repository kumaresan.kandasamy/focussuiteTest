﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Common;
using Focus.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class DashboardDailyStatistics : UserControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			PopulateStatistics();
		}

		/// <summary>
		/// Populates the statistics.
		/// </summary>
		private void PopulateStatistics()
		{
			var statisticTypes = new List<StatisticType>
				                     {
					                     StatisticType.TotalSearchableResumes,
					                     StatisticType.TotalSearchableResumesAddedYesterday,
					                     StatisticType.TotalActiveTalentPostings,
					                     StatisticType.TotalActiveTalentPostingsAddedYesterday,
					                     StatisticType.TotalActiveSpideredJobs
				                     };

			var statistics = ServiceClientLocator.ReportClient(App).GetStatistics(statisticTypes, DateTime.Now);

			if (statistics.Count > 0)
			{
				foreach (var stat in statistics)
				{
					switch (stat.Key)
					{
						case StatisticType.TotalSearchableResumes:
							TotalSearchableResumesLiteral.Text = stat.Value;
							break;

						case StatisticType.TotalSearchableResumesAddedYesterday:
							TotalSearchableResumesAddedYesterdayLiteral.Text = stat.Value;
							break;

						case StatisticType.TotalActiveTalentPostings:
							TotalActiveSearchableTalentPostingsLiteral.Text = stat.Value;
							break;

						case StatisticType.TotalActiveTalentPostingsAddedYesterday:
							TotalActiveSearchableTalentPostingsAddedYesterdayLiteral.Text = stat.Value;
							break;

						case StatisticType.TotalActiveSpideredJobs:
							TotalActiveSpideredPostingsLiteral.Text = stat.Value;
							break;
					}
				}
			}
			else if (statistics.Count == 0)
			{
				DashboardStatisticsPlaceholder.Visible = false;
			}
			
		}
	}
}