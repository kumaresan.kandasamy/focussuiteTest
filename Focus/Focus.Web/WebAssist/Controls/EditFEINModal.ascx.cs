﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core.Models.Assist;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class EditFEINModal : UserControlBase
	{
		protected string Title;

		private long BusinessUnitId
		{
			get { return GetViewStateValue<long>("EditFEINModal:BusinessUnitId"); }
			set { SetViewStateValue("EditFEINModal:BusinessUnitId", value); }
		}

		private string CurrentFEIN
		{
			get { return GetViewStateValue<string>("EditFEINModal:CurrentFEIN"); }
			set { SetViewStateValue("EditFEINModal:CurrentFEIN", value); }
		}

		private string NewFEIN
		{
			get { return GetViewStateValue<string>("EditFEINModal:NewFEIN"); }
			set { SetViewStateValue("EditFEINModal:NewFEIN", value); }
		}

	  private FEINChangeActions FEINAction
	  {
      get { return GetViewStateValue<FEINChangeActions>("EditFEINModal:FEINAction"); }
      set { SetViewStateValue("EditFEINModal:FEINAction", value); }
    }

		protected int? LockVersion
		{
			get { return GetViewStateValue<int?>("EditFEINModal:LockVersion"); }
			set { SetViewStateValue("EditFEINModal:LockVersion", value); }
		}

		/// <summary>
		/// Shows the specified business unit identifier.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="lockVersion">The lock version of the employer</param>
		/// <exception cref="System.Exception">Invalid Business Unit Id.</exception>
		public void Show(long businessUnitId, int? lockVersion = null)
		{
			if(businessUnitId < 1)
				throw new Exception("Invalid Business Unit Id.");

			BusinessUnitId = businessUnitId;
			LockVersion = lockVersion;

			GetCurrentFEIN(businessUnitId);
			Bind();

			LocaliseUI();
			EditFEINModalPopup.Show();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			// Clear any previously entered values
			NewFEINTextbox.Text = ConfirmFEINTextbox.Text = string.Empty;

			if (!IsAutoAssignedFEIN())
			{
				CurrentFEINValue.Text = CurrentFEIN;
				NewFEINCompare.ValueToCompare = CurrentFEIN;
			}
			else
			{
				NewFEINCompare.Enabled = false;
			}
		}

		/// <summary>
		/// Determines whether [is automatic assigned FEIN].
		/// </summary>
		/// <returns></returns>
		private bool IsAutoAssignedFEIN()
		{
			// Auto assigned FEINs are GUIDs
			if (CurrentFEIN.IsNullOrEmpty())
				return false;
				
			var guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");

			return guidRegEx.IsMatch(CurrentFEIN);
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			Title = CodeLocalise("ModalTitle.Text", "Edit FEIN");
			SaveButton.Text = CodeLocalise("Global.Save.Label", "Save");
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			NewFEINRequired.ErrorMessage = CodeLocalise("NewFEINRequired.ErrorMessage", "New FEIN is required");
			ConfirmFEINRequired.ErrorMessage = CodeLocalise("ConfirmFEINRequired.ErrorMessage", "Confirm FEIN is required");
			ConfirmFEINCompare.ErrorMessage = CodeLocalise("ConfirmFEINCompare.ErrorMessage", "FEINs must match");
			NewFEINCompare.ErrorMessage = CodeLocalise("NewFEINCompare.ErrorMessage", "New FEIN is the same as the current FEIN");
		}

		/// <summary>
		/// Gets the current business unit's FEIN.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		private void GetCurrentFEIN(long businessUnitId)
		{
			CurrentFEIN = ServiceClientLocator.EmployerClient(App).GetFein(businessUnitId);
		}

		/// <summary>
		/// Updates the fein.
		/// </summary>
		private void UpdateFEIN()
		{
			try
			{
				var action = ServiceClientLocator.EmployerClient(App).SaveFEINChange(BusinessUnitId, NewFEIN, LockVersion);

				var confirmationText = (action == FEINChangeActions.PrimaryBusinessUnitNewFEINSiblings) ? CodeLocalise("EditFEINConfirmation.PrimaryBusinessUnitExistingFEINSiblings.Text", "FEIN has been updated successfully on the selected company and it's linked business units")
																																																		: CodeLocalise("EditFEINConfirmation.Default.Text", "FEIN has been updated successfully");

				Confirmation.Show(CodeLocalise("EditFEINConfirmation.Title", "FEIN updated"), confirmationText, CodeLocalise("Global.Close.Text", "Close"), "FEINUpdatedConfirmationClosed");
			}
			catch (Exception ex)
			{
				Confirmation.Show( CodeLocalise( "EditFEINError.Title", "FEIN update failed" ),
													CodeLocalise("EditFEINError.Body", "There was a problem updating the FEIN. Click OK to refresh the page and try again<br /><br />{0}", ex.Message),
													CodeLocalise("CloseModal.Text", "OK"),
													"FEINUpdatedConfirmationClosed");
			}
		}

		/// <summary>
		/// Shows the confirm.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="newEmployerName">New name of the employer.</param>
		private void ShowConfirm(FEINChangeActions action, string newEmployerName)
		{
			Confirmation.Show(CodeLocalise("EditFEINConfirm.Title", "Are you sure?"), GetConfirmText(action, newEmployerName), CodeLocalise("Global.Cancel.Text", "Cancel"), okText: CodeLocalise("Global.OK.Text", "OK"), okCommandName: "ConfirmFEINUpdate");
		}

		/// <summary>
		/// Gets the confirm text.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="newEmployerName">New name of the employer.</param>
		/// <returns></returns>
		private string GetConfirmText(FEINChangeActions action, string newEmployerName)
		{
			switch (action)
			{
				case FEINChangeActions.PrimaryBusinessUnitNewFEINSiblings:
					return CodeLocalise("EditFEINConfirm.PrimaryBusinessUnitNewFEINSiblings.Text", "Please note when you click OK, all business units linked to this #BUSINESS#:LOWER will be updated also");
				case FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings:
					return CodeLocalise("EditFEINConfirm.PrimaryBusinessUnitExistingFEINNoSiblings.Text", @"The FEIN you have entered is already in use by the parent #BUSINESS#:LOWER ""{0}"". When you click OK, your selected #BUSINESS#:LOWER will become a linked business unit of ""{0}"". Do you wish to proceed?", newEmployerName ?? "");
				case FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings:
					return CodeLocalise("EditFEINConfirm.PrimaryBusinessUnitExistingFEINSiblings.Text", @"The FEIN you have entered is already in use by the parent #BUSINESS#:LOWER ""{0}"". When you click OK, your selected #BUSINESS#:LOWER and it's linked business units will become business units of ""{0}"". Do you wish to proceed?", newEmployerName ?? "");
				case FEINChangeActions.NonPrimaryBusinessUnitExistingFEIN:
					return CodeLocalise("EditFEINConfirm.NonPrimaryBusinessUnitExistingFEIN.Text", @"The FEIN you have entered is already in use by the parent #BUSINESS#:LOWER ""{0}"". When you click OK, your selected #BUSINESS#:LOWER will become a business unit of ""{0}"". Do you wish to proceed?", newEmployerName ?? "");
				case FEINChangeActions.NonPrimaryBusinessUnitNewFEIN:
					return CodeLocalise("EditFEINConfirm.NonPrimaryBusinessUnitNewFEIN.Text", "The FEIN you have entered is not currently in use. When you click OK, the details of your selected business unit will be used to create a new parent #BUSINESS#:LOWER. Do you wish to proceed?");
			}

			return "";
		}

		private void ShowValidationMessage(FEINChangeValidationMessages validationMessage, string businessUnitName)
		{
			Confirmation.Show(CodeLocalise("ValidationMessage.Title", "Update prohibited"), GetValidationMessage(validationMessage, businessUnitName), CodeLocalise("Global.Close.Text", "Close"));
		}

		private string GetValidationMessage(FEINChangeValidationMessages validationMessage, string businessUnitName)
		{
			switch (validationMessage)
			{
				case FEINChangeValidationMessages.FEINNotChanged:
					return CodeLocalise("FEINNotChanged.Text", "The FEIN is the same as the business unit's current FEIN");
				case FEINChangeValidationMessages.EmployeeAssignedToSiblingBusinessUnit:
					return CodeLocalise("EmployeeAssignedToSiblingBusinessUnit.Text", @"This update will move ""{0}"" to a new #BUSINESS#:LOWER hierarchy. However one or more hiring contacts recruiting for ""{0}"" belong to other #BUSINESSES#:LOWER in the current hierarchy and therefore cannot belong to two hierarchies. To run this action, please dis-associate the hiring managers that should not be moved first, then re-run this action", businessUnitName);
			}

			return "";
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			EditFEINModalPopup.Hide();
		}

		/// <summary>
		/// Handles the Clicked event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Clicked(object sender, EventArgs e)
		{
			NewFEIN = NewFEINTextbox.TextTrimmed();

			var result = ServiceClientLocator.EmployerClient(App).ValidateFEINChange(BusinessUnitId, NewFEIN);

      FEINAction = FEINChangeActions.None;

			if (result.IsValid)
			{
        FEINAction = result.ActionToBeCarriedOut;

				if (result.ActionToBeCarriedOut == FEINChangeActions.PrimaryBusinessUnitNewFEINNoSiblings)
				{
					UpdateFEIN();
				}
				else
				{
					ShowConfirm(result.ActionToBeCarriedOut, result.NewEmployerName);
				}
			
				EditFEINModalPopup.Hide();
			}
			else
			{
				ShowValidationMessage(result.ValidationMessage, result.BusinessUnitName);
			}
		}

		/// <summary>
		/// Handles the OkCommand event of the Confirmation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_OkCommand(object sender, CommandEventArgs e)
		{
			UpdateFEIN();
		}

		/// <summary>
		/// Handles the CloseCommand event of the Confirmation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_CloseCommand(object sender, CommandEventArgs e)
		{
			if(e.CommandName == "FEINUpdatedConfirmationClosed")
        OnFEINChanged(new FEINChangedEventArgs(FEINAction));
		}

		#region Page event

		public event FEINChangedHandler FEINChanged;

		/// <summary>
		/// Raises the <see cref="FEINChanged" /> event.
		/// </summary>
    /// <param name="e">The <see cref="FEINChangedEventArgs"/> instance containing the event data.</param>
    public void OnFEINChanged(FEINChangedEventArgs e)
		{
			var handler = FEINChanged;
			if (handler != null) 
        handler(this, e);
		}

    public delegate void FEINChangedHandler(object o, FEINChangedEventArgs e);

		#endregion
	}

  public class FEINChangedEventArgs : EventArgs
  {
    public FEINChangeActions Action { get; set; }

    public FEINChangedEventArgs(FEINChangeActions action)
    {
      Action = action;
    }
  }
}