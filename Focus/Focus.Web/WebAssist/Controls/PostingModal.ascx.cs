﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Focus.Common.Extensions;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class PostingModal : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Shows the specified posting id.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		public void Show(string  lensPostingId)
		{
			DisplayPosting(lensPostingId);

			PostingModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the CloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Clicked(object sender, EventArgs e)
		{
			PostingModalPopup.Hide();
		}
		
		/// <summary>
		/// Displays the model.
		/// </summary>
		private void DisplayPosting(string lensPostingId)
		{
			var posting = ServiceClientLocator.PostingClient(App).GetJobPosting(lensPostingId);
			if (posting.IsNotNull())
			{
				if (posting.PostingHtml.IsNotNull())
					App.SetSessionValue("Career:DisplayPosting", posting.PostingHtml.AddAltTagWithValueForImg("Posting images"));

				PostingDetail.Attributes["src"] = GetRouteUrl("AssistJobDisplay", new { jobid = lensPostingId });
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CloseButton.Text = CodeLocalise("CloseButton.Text", "Close");
		}
	}
}