﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendMessages.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.SendMessages" %>

<div>
	<div class="marginBottom5">
		<focus:LocalisedLabel runat="server" ID="SavedMessagesDropDownLabel" AssociatedControlID="SavedMessagesDropDown" LocalisationKey="SavedMessages" DefaultText="Saved messages" CssClass="sr-only"/>
		<asp:DropDownList ID="SavedMessagesDropDown" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="SavedMessagesDropDown_SelectedIndexChanged" />
	</div>
	<div>
		<%= HtmlInFieldLabel(SavedMessageNameTextBox.ClientID, "SavedMessageName.InlineLabel", "name your saved message", 500)%>
		<asp:Textbox ID="SavedMessageNameTextBox" runat="server" Width="500" MaxLength="100" />
	</div>
	<div class="marginBottom5">
		<asp:RequiredFieldValidator ID="SavedMessageNameRequired" runat="server" ControlToValidate="SavedMessageNameTextBox" ValidationGroup="SaveMessageTexts" CssClass="error" />
	</div>
</div>     
<div runat="server" id="SubjectDiv" class="marginBottom5">
	<span class="width100"><%=HtmlLabel(SubjectTextBox,"SubjectTextBox.Label", "Subject")%></span>
	<span class="width20"></span>
	<span><asp:Textbox ID="SubjectTextBox" runat="server" Width="450px" MaxLength="100" /></span>
</div>
<div runat="server" ID="SubjectRequiredDiv">
	<asp:RequiredFieldValidator runat="server" ID="SubjectRequired" ValidationGroup="MessageJobSeeker" ControlToValidate="SubjectTextBox" Display="Dynamic" CssClass="error"/>
</div>
<div>
	<%= HtmlInFieldLabel(MessageTextbox.ClientID, "MessageTextbox.InlineLabel", "add new message here", 500)%>
	<asp:Textbox ID="MessageTextbox" runat="server" Width="500" MaxLength="1000" TextMode="MultiLine" Rows="5" />
</div>
<div>
	<span><asp:RequiredFieldValidator ID="MessageRequired" runat="server" ControlToValidate="MessageTextbox" ValidationGroup="MessageJobSeeker" Display="Dynamic" CssClass="error" /></span>
	<span><asp:RequiredFieldValidator ID="SavedMessageMessageRequired" runat="server" ControlToValidate="MessageTextbox" ValidationGroup="SaveMessageTexts" CssClass="error" /></span>
</div>	
<div class="marginBottom20" runat="server" ID="EmailCopyDiv">
	<span><asp:CheckBox runat="server" ID="EmailMeCopyOfMessageCheckBox" Checked="True" /></span>&nbsp;
	<span><focus:LocalisedLabel runat="server" ID="EmailMeCopyOfMessageLabel" DefaultText="email me a copy of this message" AssociatedControlID="EmailMeCopyOfMessageCheckBox"/></span>
</div>
<div>
	<asp:Button ID="SendMessageButton" runat="server" CssClass="button2" OnClick="SendMessageButton_Clicked" ValidationGroup="MessageJobSeeker" />
	<asp:Button ID="SaveMessageTextButton" runat="server" CssClass="button3" OnClick="SaveMessageTextButton_Clicked"  ValidationGroup="SaveMessageTexts" />
</div>	