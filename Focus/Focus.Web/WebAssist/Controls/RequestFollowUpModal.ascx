﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RequestFollowUpModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.RequestFollowUpModal" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<asp:HiddenField ID="RequestFollowUpModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="RequestFollowUpModalPopup" runat="server" BehaviorID="RequestFollowUpModal"
												TargetControlID="RequestFollowUpModalDummyTarget"
												PopupControlID="RequestFollowUpModalPanel"
												PopupDragHandleControlID="RequestFollowUpModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="RequestFollowUpModalPanel" runat="server" CssClass="modal" style="display:none;">
    <h2><asp:Literal runat="server" ID="ModalTitle" /></h2>
    <%= HtmlRequiredLabel(NoteTextBox, "Notes.Label", "Notes")%>:<br/>
    <asp:TextBox ID="NoteTextBox" runat="server" TextMode="MultiLine" Rows="5" Width="600"  /><br/>
    <asp:RequiredFieldValidator runat="server" ControlToValidate="NoteTextBox" ID="NoteValidator" CssClass="error" ValidationGroup="RequestFollowUp"></asp:RequiredFieldValidator><br/>
    <asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" CausesValidation="False" Text="Cancel"/>
	<asp:Button ID="SaveButton" runat="server" CssClass="button4" OnClick="SaveButton_Clicked" CausesValidation="True" ValidationGroup="RequestFollowUp" Text="Save"/>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />