﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Core;
using Framework.Core;

using Focus.Web.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Framework.Exceptions;


#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class DenyEmployerReferralModal : UserControlBase
	{

		private ApprovalType _approvalType
		{
			get { return GetViewStateValue<ApprovalType>("DenyEmployerReferralModal:ApprovalType"); }
			set { SetViewStateValue("DenyEmployerReferralModal:ApprovalType", value); }
		}

		private long _employeeId
		{
			get { return GetViewStateValue<long>("DenyEmployerReferralModal:EmployeeId"); }
			set { SetViewStateValue("DenyEmployerReferralModal:EmployeeId", value); }
		}

		private EmailTemplateView EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>("DenyEmployerReferralModal:EmailTemplate"); }
			set { SetViewStateValue("DenyEmployerReferralModal:EmailTemplate", value); }
		}

		protected long _employerId
		{
			get { return GetViewStateValue<long>("DenyEmployerReferralModal:EmployerId"); }
			set { SetViewStateValue("DenyEmployerReferralModal:EmployerId", value); }
		}

		protected long _businessUnitId
		{
			get { return GetViewStateValue<long>("DenyEmployerReferralModal:BusinessUnitId"); }
			set { SetViewStateValue("DenyEmployerReferralModal:BusinessUnitId", value); }
		}

		protected string LockVersion
		{
			get { return GetViewStateValue<string>("DenyEmployerReferralModal:LockVersion"); }
			set { SetViewStateValue("DenyEmployerReferralModal:LockVersion", value); }
		}

		private bool EmployerFiltered
		{
			get { return GetViewStateValue<bool>("DenyEmployerReferralModal:EmployerFiltered"); }
			set { SetViewStateValue("DenyEmployerReferralModal:EmployerFiltered", value); }
		}

		private const string FilteredUrl = "?filtered=true";
		private bool isApprovedAlready = false;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Shows the specified employee id.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="approvalType"></param>
		/// <param name="employerId"></param>
		/// <param name="businessunitId"></param>
		/// <param name="lockVersion"></param>
		public void Show(long employeeId,ApprovalType approvalType,long employerId, long businessunitId, string lockVersion,bool filtered = false)
		{
			_employeeId = employeeId;
			_approvalType = approvalType;
			_businessUnitId = businessunitId;
			_employerId = employerId;
			LockVersion = lockVersion;
			EmployerFiltered = filtered;

			var id = _employeeId;
			switch (_approvalType)
			{
				case ApprovalType.Employer:
					id = _employerId;
					break;
				case ApprovalType.BusinessUnit:
					id = _businessUnitId;
					break;
			}

			GetEmailTemplate(id);

			if (EmailTemplate == null) return;

      // KRP 09.May.2017 - Check if the employer is approved
      if (!isApprovedAlready)
      {
			var regex = new Regex(Constants.PlaceHolders.DenialReasons);
			var emailBodySplit = regex.Split(EmailTemplate.Body);
			
			TopEmailBodyLiteral.Text = emailBodySplit[0].TrimEnd('\r', '\n').Replace(Environment.NewLine, "<br />");
			BottomEmailBodyLiteral.Text = (emailBodySplit.Length > 1) ? emailBodySplit[1].TrimStart('\r', '\n').Replace(Environment.NewLine, "<br />") : string.Empty;
      }
			BCCMeCheckBox.Checked = false; // default to unchecked in case the user has previously opened the modal and cancelled

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				CheckBoxDenialReasonsPlaceholder.Visible = true;
				cblDenialReasons.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(_approvalType == ApprovalType.HiringManager ? LookupTypes.HiringManagerDenialReasons : LookupTypes.EmployerDenialReasons));
				OtherDenialReasonTextBox.ResetText();
				OtherDenialReasonTextBox.Enabled = false;
			}
			else
			{
				FreeTextDenialReasonPlaceholder.Visible = true;
				CheckBoxDenialReasonsPlaceholder.Visible = false;
			}

			LocaliseUI();

			ModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the SendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DenyAndSendMessageButton_Clicked(object sender, EventArgs e)
		{
			try
			{
				var id = _employeeId;

				switch (_approvalType)
				{
					case ApprovalType.Employer:
						id = _employerId;
						break;
					case ApprovalType.BusinessUnit:
						id = _businessUnitId;
						break;
				}

      /* KRP 09.May.2017 - Check if the employer is approved
      If approved, redirect to employer referral page*/
      if (isApprovedAlready)
      {
          ModalPopup.Hide();
          Response.Redirect(UrlBuilder.EmployerReferrals());
      }

				string denialReasons;
				var denialList = new List<KeyValuePair<long, string>>();

				if (App.Settings.Theme == FocusThemes.Workforce)
				{
					var otherId = ServiceClientLocator.CoreClient(App).GetLookup(_approvalType == ApprovalType.HiringManager ? LookupTypes.HiringManagerDenialReasons : LookupTypes.EmployerDenialReasons).Where(x => x.Key.Equals(_approvalType == ApprovalType.HiringManager ? "HiringManagerDenialReasons.OtherReason" : "EmployerDenialReasons.OtherReason")).Select(x => x.Id).FirstOrDefault();

					var query = (from ListItem checkbox in cblDenialReasons.Items
											 where checkbox.Selected && checkbox.Value.IsNotNullOrEmpty() && checkbox.Value != otherId.ToString()
											 select checkbox).ToList();

					#region Create a list of the reasons to be displayed in the email

					var denialReasonList = query.Select(checkBox => checkBox.Text).ToList();

					var otherReason = OtherDenialReasonTextBox.TextTrimmed();

					if (otherReason.IsNotNullOrEmpty())
						denialReasonList.Add(otherReason);

					denialReasons = string.Join(Environment.NewLine, denialReasonList.Select(x => string.Concat("* ", x)));

					#endregion

					#region Create a list of keyvaluepairs of the reason Ids and Other text for saving to the database

					denialList = query.Select(checkBox => new KeyValuePair<long, string>(Convert.ToInt64(checkBox.Value), string.Empty)).ToList();

					if (otherReason.IsNotNullOrEmpty())
					{
						var otherReasonEntry = new KeyValuePair<long, string>(otherId, otherReason);
						denialList.Add(otherReasonEntry);
					}

					#endregion
				}
				else
				{
					denialReasons = DenialReasonsTextBox.TextTrimmed();
				}

				ServiceClientLocator.EmployeeClient(App).DenyEmployerAccountReferral(_employeeId, _businessUnitId, LockVersion, _approvalType, App.Settings.Theme == FocusThemes.Workforce ? denialList : null);

				GetEmailTemplate(id);

				if (EmailTemplate == null) return;

				var employerReferralView = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(id);
				var emailBody = EmailTemplate.Body.Replace(Constants.PlaceHolders.DenialReasons, denialReasons);
				var senderAddress = ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.DenyEmployerAccountReferral);

				ServiceClientLocator.EmployeeClient(App).EmailEmployee(employerReferralView.EmployeeId, EmailTemplate.Subject, emailBody, BCCMeCheckBox.Checked, senderAddress);
				ServiceClientLocator.CoreClient(App).SaveReferralEmail(EmailTemplate.Subject, emailBody, ApprovalStatuses.Rejected, employerReferralView.EmployeeId);

				ModalPopup.Hide();

				if (_approvalType == ApprovalType.HiringManager)
				{
					Confirmation.Show(CodeLocalise("ReferralDeniedConfirmationHiringManager.Title", "Hiring manager account denied"),
					CodeLocalise("ReferralDeniedConfirmationHiringManager.Body", "The hiring manager account has been denied."),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink: EmployerFiltered ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.EmployerReferrals());
				}
				else
				{
					Confirmation.Show(CodeLocalise("ReferralDeniedConfirmation.Title", "#BUSINESS# account denied"),
					CodeLocalise("ReferralDeniedConfirmation.Body", "The #BUSINESS#:LOWER account has been denied."),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink:  EmployerFiltered ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.EmployerReferrals());
				}
			}
			catch (ServiceCallException ex)
			{
				var message = (_approvalType == ApprovalType.HiringManager)
					              ? CodeLocalise("ReferralDeniedExceptionHiringManager.Body", "There was a problem denying the hiring manager.<br /><br />{0}", ex.Message)
												: CodeLocalise("ReferralDeniedExceptionEmployer.Body", "There was a problem denying the #BUSINESS#:LOWER.<br /><br />{0}", ex.Message);

				Confirmation.Show(CodeLocalise("ReferralDeniedError.Title", "Denial failed"),
													message,
													CodeLocalise("CloseModal.Text", "OK"),
													closeLink: EmployerFiltered ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.EmployerReferrals());
			}
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			DenyAndSendMessageButton.Text = _approvalType == ApprovalType.HiringManager ? CodeLocalise("DenyAndSendMessageHiringManagerButton.Text", "Email hiring manager") : CodeLocalise("DenyAndSendMessageEmployerButton.Text", "Email #BUSINESS#:LOWER");
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");
			DenialReasonsRequired.ErrorMessage = CodeLocalise("DenialReasonsRequired.ErrorMessage", "Denial reasons required");
			DenialReasonsCheckboxRequired.ErrorMessage = CodeLocalise("DenialReasonsCheckboxRequired.ErrorMessage", "Denial reasons required");
			OtherDeniedReasonRequired.ErrorMessage = CodeLocalise("OtherDeniedReasonRequired.ErrorMessage", "Other reason description required");
			SummaryLabel.DefaultText = _approvalType == ApprovalType.HiringManager ? CodeLocalise("HiringManagerSummary.Text", "Notify the hiring manager their account request has been denied.") : CodeLocalise("EmployerSummary.Text", "Notify the #BUSINESS#:LOWER their account request has been denied.");
			TitleLabel.DefaultText = _approvalType == ApprovalType.HiringManager ? CodeLocalise("HiringManagerTitle.Text", "Notification of denied hiring manager account request") : CodeLocalise("EmployerTitle.Text", "Notification of denied #BUSINESS#:LOWER account request");
      OtherDenialReasonValidator.ErrorMessage = CodeLocalise("OtherDenialReasonValidator.ErrorMessage", "Other reason description must be 250 characters or less");
		}

		/// <summary>
		/// Builds the email body.
		/// </summary>
		/// <returns></returns>
		private void GetEmailTemplate(long employeeId)
		{
			var employerReferralView = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(employeeId);
			      /* KRP 09.May.2017 - Check if the employer is approved
      If approved, redirect to employer referral page*/
      if (isApprovedAlready)
      {
          ModalPopup.Hide();
          Response.Redirect(UrlBuilder.EmployerReferrals());
      }
			if (!employerReferralView.IsNotNull())
			{
				var message = (_approvalType == ApprovalType.HiringManager)
					              ? CodeLocalise("ReferralDeniedErrorHiringManager.Body", "There was a problem denying the hiring manager.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.")
												: CodeLocalise("ReferralDeniedErrorEmployer.Body", "There was a problem denying the #BUSINESS#:LOWER.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.");

				Confirmation.Show(CodeLocalise("ReferralOnHoldError.Title", "Hold failed"),
													message,
													CodeLocalise("CloseModal.Text", "OK"),
													closeLink: UrlBuilder.EmployerReferrals());
				return;
			}
			
			var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";

			var templateValues = new EmailTemplateData
			{
				RecipientName = String.Format("{0} {1}", employerReferralView.EmployeeFirstName, employerReferralView.EmployeeLastName),
				FreeText = Constants.PlaceHolders.DenialReasons,
				SenderPhoneNumber = Regex.Replace(phoneNumber, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat),
				SenderEmailAddress = App.User.EmailAddress,
				SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName)
			};

			EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.DenyEmployerAccountReferral, templateValues);
		}
	}
}
