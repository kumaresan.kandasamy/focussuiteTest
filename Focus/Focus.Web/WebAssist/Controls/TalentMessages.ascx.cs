﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

using Focus.Core;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core.DataTransferObjects.FocusCore;

using Framework.Core;

using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class TalentMessages : UserControlBase
	{
		private EmailTemplateTypes? _selectedEmailTemplateType
		{
			get { return GetViewStateValue<EmailTemplateTypes?>("TalentMessages:SelectedEmailTemplateType"); }
			set { SetViewStateValue("TalentMessages:SelectedEmailTemplateType", value); }
		}

	/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			BindEmailTemplateNameDropDown();
			EmailTemplateEditor.Bind(new EmailTemplateDto(), new Dictionary<string, string>());
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the EmailTemplateNameDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmailTemplateNameDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (EmailTemplateNameDropDown.SelectedIndex == 0)
			{
				_selectedEmailTemplateType = null;
				EmailTemplateEditor.Bind(new EmailTemplateDto(), new Dictionary<string, string>());
			}
			else
			{
				_selectedEmailTemplateType = EmailTemplateNameDropDown.SelectedValueToEnum<EmailTemplateTypes>();
				var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplate(_selectedEmailTemplateType.Value);
				var messageVariables = GetMessageVariables(_selectedEmailTemplateType.Value);
				EmailTemplateEditor.Bind(emailTemplate, messageVariables);	
			}
		}

		/// <summary>
		/// Handles the Click event of the SaveChangesButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveChangesButton_Click(object sender, EventArgs e)
		{
			if (!_selectedEmailTemplateType.HasValue) return;

			var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplate(_selectedEmailTemplateType.Value);

			if (emailTemplate.IsNull()) emailTemplate = new EmailTemplateDto { EmailTemplateType = _selectedEmailTemplateType.Value };

			EmailTemplateEditor.UnBind(ref emailTemplate);

			ServiceClientLocator.CoreClient(App).SaveEmailTemplate(emailTemplate);

			Confirmation.Show(CodeLocalise("TalentMessageUpdatedConfirmation.Title", "Talent message updated"),
															 CodeLocalise("TalentMessageUpdatedConfirmation.Body", "The message has been updated."),
															 CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Binds the email template name drop down.
		/// </summary>
		private void BindEmailTemplateNameDropDown()
		{
			EmailTemplateNameDropDown.Items.Clear();

			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.AcknowledgeApplication, "Acknowledge application");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.AutoApprovedJobSeekerReferral, "Auto-approved Job Seeker Referrals");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.TalentCandidateSearchAlert, "#CANDIDATETYPE# search alert");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.CustomMessage, "Custom message");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.EmployerNotificationJobExpiration, "#BUSINESS# notification of closed job post");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.EmployerNotificationPendingJobExpiration, "#BUSINESS# notification of pending job expiration");
      if (App.Settings.Theme != FocusThemes.Education)
        EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.HiringFromTaxCreditProgramNotification, "Hiring from tax credit program notification");
      EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer, "Invitation to apply for #POSTINGTYPE#:LOWER through #FOCUSCAREER#");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.TalentPasswordReset, "#FOCUSTALENT# password reset");
			EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.RejectApplication, "Rejection");

      if (App.Settings.PreScreeningServiceRequest && App.Settings.Theme == FocusThemes.Workforce)
        EmailTemplateNameDropDown.Items.AddEnum(EmailTemplateTypes.PreScreeningServiceRequest, "Pre-screening service information request");

			EmailTemplateNameDropDown.Items.AddLocalisedTopDefault("EmailTemplateName.TopDefault.NoEdit", "- select message -");
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SaveChangesButton.Text = CodeLocalise("SaveChangesButton.Text.NoEdit", "Save changes");
		}

		/// <summary>
		/// Gets the message variables.
		/// </summary>
		/// <param name="emailTemplateType">Type of the email template.</param>
		/// <returns></returns>
		private Dictionary<string, string> GetMessageVariables(EmailTemplateTypes emailTemplateType)
		{
			var dictionary = new Dictionary<string, string>();

			switch(emailTemplateType)
			{
				case EmailTemplateTypes.AutoApprovedJobSeekerReferral:
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
					dictionary.Add(Constants.PlaceHolders.FocusCareer, CodeLocalise("FocusCareer.Text", "#FOCUSCAREER#"));
					dictionary.Add(Constants.PlaceHolders.FocusAssist, CodeLocalise("FocusAssist.Text", "#FOCUSASSIST#"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "Job title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					break;

				case EmailTemplateTypes.CustomMessage:
					dictionary.Add(Constants.PlaceHolders.FreeText, CodeLocalise("FreeText.Text", "Free text area"));
					break;

				case EmailTemplateTypes.TalentCandidateSearchAlert:
					dictionary.Add(Constants.PlaceHolders.MatchedCandidates, CodeLocalise("MatchedCandidates.Text", "Matched #CANDIDATETYPE#"));
					dictionary.Add(Constants.PlaceHolders.SavedSearchName, CodeLocalise("SavedSearchName.Text", "Saved search name"));
					break;

        case EmailTemplateTypes.HiringFromTaxCreditProgramNotification:
          dictionary.Add(Constants.PlaceHolders.HiringManagerName, CodeLocalise("HiringManagerName.Text", "Hiring manager name"));
          dictionary.Add(Constants.PlaceHolders.HiringManagerPhoneNumber, CodeLocalise("HiringManagerPhoneNumber.Text", "Hiring manager phone number"));
          dictionary.Add(Constants.PlaceHolders.HiringManagerEmail, CodeLocalise("HiringManagerEmail.Text", "Hiring manager email"));
          dictionary.Add(Constants.PlaceHolders.JobPostingDate, CodeLocalise("JobPostingDate.Text", "#POSTINGTYPE# posting date"));
          dictionary.Add(Constants.PlaceHolders.WOTC, CodeLocalise("WOTC.Text", "WOTC"));
			    break;

        case EmailTemplateTypes.EmployerNotificationJobExpiration:
          dictionary.Add(Constants.PlaceHolders.JobId, CodeLocalise("JobId.Text", "#POSTINGTYPE# id"));
          dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "#POSTINGTYPE# link url"));
          dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
			    break;

				case EmailTemplateTypes.EmployerNotificationPendingJobExpiration:
					dictionary.Add(Constants.PlaceHolders.JobId, CodeLocalise("JobId.Text", "#POSTINGTYPE# id"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.DaysLeftToJobExpiry, CodeLocalise("DaysLeftToJobExpiry.Text", "Days left to job expiry"));
					break;

				case EmailTemplateTypes.TalentPasswordReset:
					dictionary.Add(Constants.PlaceHolders.FocusTalent, CodeLocalise("FocusTalent.Text", "#FOCUSTALENT#"));
					dictionary.Add(Constants.PlaceHolders.Url, CodeLocalise("PasswordResetUrl.Text", "#URL#"));
					dictionary.Add(Constants.PlaceHolders.FocusExplorer, CodeLocalise("FocusCareer.Text", "#FOCUSEXPLORER#"));
					break;

        case EmailTemplateTypes.PreScreeningServiceRequest:
          dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
          dictionary.Add(Constants.PlaceHolders.HiringManagerName, CodeLocalise("HiringManagerName.Text", "Hiring manager name"));
          dictionary.Add(Constants.PlaceHolders.JobId, CodeLocalise("JobId.Text", "#POSTINGTYPE# id"));
          dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
          dictionary.Add(Constants.PlaceHolders.ExpiryDate, CodeLocalise("ExpiryDate.Text", "Expiry date"));
          dictionary.Add(Constants.PlaceHolders.OfficeName, CodeLocalise("OfficeName.Text", "Office Name"));
          break;

				default:
					dictionary.Add(Constants.PlaceHolders.EmployerName, CodeLocalise("EmployerName.Text", "#BUSINESS#'s name"));
					dictionary.Add(Constants.PlaceHolders.JobTitle, CodeLocalise("JobTitle.Text", "#POSTINGTYPE# title"));
					
          if (emailTemplateType == EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer)
						dictionary.Add(Constants.PlaceHolders.JobLinkUrl, CodeLocalise("JobLinkUrl.Text", "#POSTINGTYPE# link url"));
					
            dictionary.Add(Constants.PlaceHolders.RecipientName, CodeLocalise("RecipientName.Text", "Recipient's name"));
					dictionary.Add(Constants.PlaceHolders.SenderName, CodeLocalise("SenderName.Text", "Sender's name"));
					break;
			}
			
			return dictionary;
		}
	}
}
