﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Core.Criteria;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Core;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.BusinessUnit;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class EmployerJobSeekerPlacements : UserControlBase
	{
		#region Properties

		private int _placementsCount;

		/// <summary>
		/// Gets or sets the session id.
		/// </summary>
		private string SessionId { get; set; }
		
		private BusinessUnitCriteria BusinessUnitCriteria
		{
			get { return GetViewStateValue<BusinessUnitCriteria>("ListBusinessUnitActivity:BusinessUnitCriteria"); }
			set { SetViewStateValue("ListBusinessUnitActivity:BusinessUnitCriteria", value); }
		}

		/// <summary>
		/// Gets or sets the business unit id.
		/// </summary>
		public long BusinessUnitId { get; set; }

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Page.RouteData.Values.ContainsKey("session"))
			{
        var sessionid = Page.RouteData.Values["session"].ToString();

				SessionId = sessionid;
			}
			if (!Page.IsPostBack)
			{
				ApplyBranding();
			}
		}

		#region PlacementsList Events
		
		/// <summary>
		/// Handles the LayoutCreated event of the PlacementsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PlacementsList_LayoutCreated(object sender, EventArgs e)
		{
			((Literal)PlacementsList.FindControl("PlacementNameHeader")).Text = CodeLocalise("PlacementNameHeader.Text", "NAME");
			((Literal)PlacementsList.FindControl("JobTitleHeader")).Text = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(CodeLocalise("JobTitleHeader.Text", "#POSTINGTYPE# TITLE")).ToUpperInvariant();
		}


		/// <summary>
		/// Handles the Sorting event of the PlacementsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void PlacementsList_Sorting(object sender, ListViewSortEventArgs e)
		{
			BusinessUnitCriteria.OrderBy = e.SortExpression;
			FormatPlacementsListSortingImages();
		}

		#endregion

		#region PlacementsDataSource Events

		/// <summary>
		/// Handles the Selecting event of the PlacementsDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void PlacementsDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (BusinessUnitCriteria.IsNull())
			{
				BusinessUnitCriteria = new BusinessUnitCriteria { OrderBy = Constants.SortOrders.PlacementNameDesc, BusinessUnitId = BusinessUnitId };
			}

			e.InputParameters["criteria"] = BusinessUnitCriteria;
		}

		/// <summary>
		/// Gets the employer placement count.
		/// </summary>
		/// <returns></returns>
		public int GetEmployerPlacementCount()
		{
			return _placementsCount;
		}

		/// <summary>
		/// Gets the employer placement count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetEmployerPlacementCount(BusinessUnitCriteria criteria)
		{
			return _placementsCount;
		}

		/// <summary>
		/// Gets the employer placements.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
    public List<BusinessUnitPlacementsViewDto> GetEmployerPlacements(BusinessUnitCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_placementsCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;
			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			var placements = ServiceClientLocator.EmployerClient(App).GetBusinessUnitPlacements(criteria);
			_placementsCount = placements.TotalCount;

			return placements;
		}

		#endregion

		#region Formatting Sorting Images

		/// <summary>
		/// Formats the placements list sorting images.
		/// </summary>
		private void FormatPlacementsListSortingImages()
		{
			if (!PlacementsPager.Visible) return;

			#region ResetEnable Placements Image Buttons

			((ImageButton)PlacementsList.FindControl("PlacementNameAscButton")).ImageUrl =
			((ImageButton)PlacementsList.FindControl("JobTitleAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)PlacementsList.FindControl("PlacementNameDescButton")).ImageUrl =
			((ImageButton)PlacementsList.FindControl("JobTitleDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			((ImageButton)PlacementsList.FindControl("PlacementNameDescButton")).Enabled =
			((ImageButton)PlacementsList.FindControl("PlacementNameAscButton")).Enabled =
			((ImageButton)PlacementsList.FindControl("JobTitleDescButton")).Enabled =
			((ImageButton)PlacementsList.FindControl("JobTitleAscButton")).Enabled = true;

			#endregion

			switch (BusinessUnitCriteria.OrderBy.ToLower())
			{
				case Constants.SortOrders.PlacementNameAsc:
					((ImageButton)PlacementsList.FindControl("PlacementNameAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)PlacementsList.FindControl("PlacementNameAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.PlacementNameDesc:
					((ImageButton)PlacementsList.FindControl("PlacementNameDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)PlacementsList.FindControl("PlacementNameDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.PlacementJobTitleAsc:
					((ImageButton)PlacementsList.FindControl("JobTitleAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)PlacementsList.FindControl("JobTitleAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.PlacementJobTitleDesc:
					((ImageButton)PlacementsList.FindControl("JobTitleDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)PlacementsList.FindControl("JobTitleDescButton")).Enabled = false;
					break;
			}
		}

		#endregion

		#region PlacementNameLinkButton Command

		/// <summary>
		/// Handles the Command event of the PlacementNameLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void PlacementNameLinkButton_Command(object sender, CommandEventArgs e)
		{
			// The job seeker id is set to a known id as the reporting page will change the uploading of jobseekers from a different repository
			var jobseekerId = Int32.Parse(e.CommandArgument.ToString());
			var sessionId = SessionId;
		  var url = Page.Is(UrlBuilder.AssistEmployerProfileForCompareOnly())
                  ? UrlBuilder.AssistJobSeekerProfile(jobseekerId)
                  : UrlBuilder.ReportingJobSeekerProfile(jobseekerId, sessionId);
      Response.Redirect(url);
		}

		#endregion

		private void ApplyBranding()
		{

			if (PlacementsList.FindControl("PlacementNameAscButton") != null)
			{
				var control = (ImageButton) PlacementsList.FindControl("PlacementNameAscButton");
				control.ImageUrl = UrlBuilder.ActivityOnSortAscImage();
				control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			}

			if (PlacementsList.FindControl("PlacementNameDescButton") != null)
			{
				var control =(ImageButton)PlacementsList.FindControl("PlacementNameDescButton");
				control.ImageUrl = UrlBuilder.ActivityOnSortDescImage();
				control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			}

			if (PlacementsList.FindControl("JobTitleAscButton") != null)
			{
				var control = (ImageButton) PlacementsList.FindControl("JobTitleAscButton");
				control.ImageUrl = UrlBuilder.ActivityOnSortAscImage();
				control.AlternateText = CodeLocalise("Global.SortAscending", "Sort ascending");
			}
			if (PlacementsList.FindControl("JobTitleDescButton") != null)
			{
				var control = (ImageButton) PlacementsList.FindControl("JobTitleDescButton");
				control.ImageUrl = UrlBuilder.ActivityOnSortDescImage();
				control.AlternateText = CodeLocalise("Global.SortDescending", "Sort descending");
			}

		}
	}
}