﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Focus.Web.Controllers.WebAssist;
using Constants = Focus.Core.Constants;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
  public partial class TalentDefaults : UserControlBase
  {
    protected TalentSettingsController PageController { get { return PageControllerBaseProperty as TalentSettingsController; } }

    /// <summary>
    /// Registers the page controller.
    /// </summary>
    /// <returns></returns>
    public override IPageController RegisterPageController()
    {
      return new TalentSettingsController(App);
    }

    private string _approvalString = TalentApprovalOptions.ApprovalSystemAvailable.ToString();
    /// <summary>
    /// Enum to Compare
    /// </summary>
    protected string ApprovalSystemAvailable
    {
      get { return _approvalString; }
      set { _approvalString = value; }
    }
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        ApplyTheme();
        LocaliseUI();
        ApplyBranding();
        BindResumeReferralApprovalsDropDown();
        BindNewEmployerApprovalsDropDownList();
        //BindNewBusinessUnitAccountDropdownList();
        //BindNewHiringManagerAccountDropdownList();
	      BindRequirementsPreferenceDefaultDropdownList();
        BindScreenPreferenceDefaultDropDown();
				BindJobApprovalOptions();
      }

      if (App.Settings.UseCustomHeaderHtml)
      {
        ColourDefaultsHeaderPanel.Visible = false;
        ColourDefaultsPanel.Visible = false;
        LogoDefaultsHeaderPanel.Visible = false;
        LogoDefaultsPanel.Visible = false;
      }

      RegisterJavascript();
      if (ApprovalCheckStaffJobsCheckBoxList != null)
          ApprovalCheckStaffJobsCheckBoxList.Attributes.Add("role", "presentation");
      if (ApprovalCheckCustomerJobsCheckBoxList != null)
          ApprovalCheckCustomerJobsCheckBoxList.Attributes.Add("role", "presentation");
    }

	  protected void Page_PreRender(object source, EventArgs e)
	  {
		  var nvc = new NameValueCollection
			{
				{"ApprovalDefaultsForCustomerJobsDropDownListClientId", ApprovalDefaultsForCustomerJobsDropDownList.ClientID },
				{"ApprovalDefaultsForStaffJobsDropDownListClientId", ApprovalDefaultsForStaffJobsDropDownList.ClientID },
				{"ApprovalCheckCustomerJobsCheckBoxListClientId", ApprovalCheckCustomerJobsCheckBoxList.ClientID },
				{"ApprovalCheckStaffJobsCheckBoxListClientId", ApprovalCheckStaffJobsCheckBoxList.ClientID },
			};
			RegisterCodeValuesJson("TalentDefaults_ClientSideVars", "TalentDefaults_ClientSideVars", nvc);
	  }

	  /// <summary>
    /// Handles the Click event of the SaveChangesButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveChangesButton_Click(object sender, EventArgs e)
    {
      if (LightColourTextBox.Text == string.Empty)
      {
        LightColourValidator.IsValid = false;
        return;
      }

      if (DarkColourTextBox.Text == string.Empty)
      {
        DarkColourValidator.IsValid = false;
        return;
      }

      var bytes = App.GetSessionValue<byte[]>(Constants.StateKeys.UploadedImage);

      if (bytes.IsNotNullOrEmpty())
      {
        PageController.SaveLogo(bytes);

        TalentApplicationLogoUploader.Clear();
        TalentApplicationLogoUploader.SetPreviewImage(UrlBuilder.ApplicationImage(ApplicationImageTypes.FocusTalentHeader));
      }

      var configurationItems = new List<ConfigurationItemDto>();

      var newEmployerApproval = NewEmployerAccountDropDownList.SelectedValueToFlagEnum(TalentApprovalOptions.ApprovalSystemAvailable);
      if (newEmployerApproval != (int)App.Settings.NewEmployerApproval)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.NewEmployerApproval, Value = newEmployerApproval.ToString(CultureInfo.InvariantCulture) });

      var newBusinessUnitApproval = NewEmployerAccountDropDownList.SelectedValueToFlagEnum(TalentApprovalOptions.ApprovalSystemAvailable);
      if (newBusinessUnitApproval != (int)App.Settings.NewBusinessUnitApproval)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.NewBusinessUnitApproval, Value = newBusinessUnitApproval.ToString(CultureInfo.InvariantCulture) });

      var newHiringManagerApproval = NewEmployerAccountDropDownList.SelectedValueToFlagEnum(TalentApprovalOptions.ApprovalSystemAvailable);
      if (newHiringManagerApproval != (int)App.Settings.NewHiringManagerApproval)
          configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.NewHiringManagerApproval, Value = newHiringManagerApproval.ToString(CultureInfo.InvariantCulture)});

      if (NewEmployerCensorshipFilteringCheckBox.Checked != App.Settings.NewEmployerCensorshipFiltering)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.NewEmployerCensorshipFiltering, Value = NewEmployerCensorshipFilteringCheckBox.Checked.ToString() });

      if (TalentFooterLogoCheckBox.Checked != App.Settings.ShowBurningGlassLogoInTalent)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.ShowBurningGlassLogoInTalent, Value = TalentFooterLogoCheckBox.Checked.ToString() });

      if (PreferenceDefaultsEmployerRadioButton.Checked != App.Settings.EmployerPreferencesOverideSystemDefaults)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.EmployerPreferencesOverideSystemDefaults, Value = PreferenceDefaultsEmployerRadioButton.Checked.ToString() });

      if (LightColourTextBox.TextTrimmed() != App.Settings.AssistLightColour)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.TalentLightColour, Value = LightColourTextBox.TextTrimmed() });

      if (DarkColourTextBox.TextTrimmed() != App.Settings.AssistDarkColour)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.TalentDarkColour, Value = DarkColourTextBox.TextTrimmed() });

      if (ScreeningPrefrencesDefaultDropDownList.SelectedValue.ToInt() != (int)App.Settings.ScreeningPrefExclusiveToMinStarMatchDefault)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.ScreeningPrefExclusiveToMinStarMatchDefault, Value = ScreeningPrefrencesDefaultDropDownList.SelectedValue });

      if (ResumeReferralApprovalsDropDownList.SelectedValue.ToInt() != App.Settings.ResumeReferralApprovalsMinimumStars)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.ResumeReferralApprovalsMinimumStars, Value = ResumeReferralApprovalsDropDownList.SelectedValue });

			#region Job Approval options

			configurationItems.AddRange(UnbindJobApprovalDropdownList(ApprovalDefaultsForCustomerJobsDropDownList, App.Settings.ApprovalDefaultForCustomerCreatedJobs, Constants.ConfigurationItemKeys.ApprovalDefaultForCustomerCreatedJobs));
			configurationItems.AddRange(UnbindJobApprovalDropdownList(ApprovalDefaultsForStaffJobsDropDownList, App.Settings.ApprovalDefaultForStaffCreatedJobs, Constants.ConfigurationItemKeys.ApprovalDefaultForStaffCreatedJobs));

			configurationItems.AddRange(UnbindJobApprovalCheckList(ApprovalCheckCustomerJobsCheckBoxList, App.Settings.ApprovalChecksForCustomerCreatedJobs, Constants.ConfigurationItemKeys.ApprovalChecksForCustomerCreatedJobs));
			configurationItems.AddRange(UnbindJobApprovalCheckList(ApprovalCheckStaffJobsCheckBoxList, App.Settings.ApprovalChecksForStaffCreatedJobs, Constants.ConfigurationItemKeys.ApprovalChecksForStaffCreatedJobs));

			#endregion

			if (SharingDefaultsEmployerUsersSameFEINCheckBox.Checked != App.Settings.SharingJobsForSameFEIN)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.SharingJobsForSameFEIN, Value = SharingDefaultsEmployerUsersSameFEINCheckBox.Checked.ToString() });

      // We are checking against the negated version of TalentPoolSearch setting as that is how it is saved as detailed below.
      if (HiringManagerSearchApprovalCheckBox.Checked != !App.Settings.TalentPoolSearch)
        // As the UI reads 'Disable Talent Pool Search' and the Config setting is TalentPoolSearch, we want to save the opposite of the Checkbox.Checked value.
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.TalentPoolSearch, Value = (!HiringManagerSearchApprovalCheckBox.Checked).ToString() });

      if (HiringManagerSearchApprovalCheckBox.Checked != !App.Settings.HiringManagerTalentPoolSearch)
          configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.HiringManagerTalentPoolSearch, Value = (!HiringManagerSearchApprovalCheckBox.Checked).ToString() });

      if (HiringManagerSearchApprovalCheckBox.Checked != !App.Settings.BusinessUnitTalentPoolSearch)
          configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.BusinessUnitTalentPoolSearch, Value = (!HiringManagerSearchApprovalCheckBox.Checked).ToString() });

			var requirementsPreferences = RequirementsPreferenceDefaultDropdownList.SelectedValueToEnum(RequirementsPreference.AllowAll);
			if (requirementsPreferences != App.Settings.RequirementsPreferenceDefault)
				configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.RequirementsPreferenceDefault, Value = ((int)requirementsPreferences).ToString(CultureInfo.InvariantCulture) });

      if (PreScreeningYesRadioButton.Checked != App.Settings.PreScreeningServiceRequest)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.PreScreeningServiceRequest, Value = PreScreeningYesRadioButton.Checked.ToString() });

    if(ActivateYellowWordFilteringCheckBox.Checked != App.Settings.ActivateYellowWordFilteringForStaffCreatedJobs)
        configurationItems.Add(new ConfigurationItemDto { Key = Constants.ConfigurationItemKeys.ActivateYellowWordFilteringForStaffCreatedJobs, Value = ActivateYellowWordFilteringCheckBox.Checked.ToString()});

      var result = PageController.SaveTalentSettings(configurationItems);
      Handle(result);
    }

		/// <summary>
		/// Unbinds the setting from an approval dropdown
		/// </summary>
		/// <param name="dropDownList">The approval drop-down list</param>
		/// <param name="currentOption">The current app setting</param>
		/// <param name="configKey">The configuration item key</param>
		/// <returns>A list of configuration items to add</returns>
		private IEnumerable<ConfigurationItemDto> UnbindJobApprovalDropdownList(DropDownList dropDownList, JobApprovalOptions currentOption, string configKey)
		{
			if (dropDownList.SelectedValue != currentOption.ToString())
				yield return new ConfigurationItemDto { Key = configKey, Value = dropDownList.SelectedValue };
		}

		/// <summary>
		/// Unbinds the setting from an approval check box list
		/// </summary>
		/// <param name="checkBoxList">The approval check box list</param>
		/// <param name="currentChecks">The current app setting value</param>
		/// <param name="configKey">The configuration item key</param>
		/// <returns>A list of configuration items to add</returns>
		private IEnumerable<ConfigurationItemDto> UnbindJobApprovalCheckList(CheckBoxList checkBoxList, JobApprovalCheck[] currentChecks, string configKey)
		{
			var unbindCheckBoxes = checkBoxList.Items.Cast<ListItem>()
																							 .Where(li => li.Selected && li.Enabled)
																							 .Select(li => (JobApprovalCheck) Enum.Parse(typeof(JobApprovalCheck), li.Value, true))
																							 .ToArray();

			if (unbindCheckBoxes.Length != currentChecks.Length || unbindCheckBoxes.Any(cb => !currentChecks.Contains(cb)))
				yield return new ConfigurationItemDto { Key = configKey, Value = unbindCheckBoxes.Any() ? string.Join(",", unbindCheckBoxes.Select(cb => cb.ToString())) : "None" };
		}

    /// <summary>
    /// Handles the Click event of the ResetColoursButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ResetColoursButton_Click(object sender, EventArgs e)
    {
      LightColourTextBox.Text = App.Settings.TalentLightColour;
      DarkColourTextBox.Text = App.Settings.TalentDarkColour;
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    public void Bind()
    {
      NewEmployerCensorshipFilteringCheckBox.Checked = App.Settings.NewEmployerCensorshipFiltering;

      TalentApplicationLogoUploader.Clear();
      TalentApplicationLogoUploader.SetPreviewImage(UrlBuilder.ApplicationImage(ApplicationImageTypes.FocusTalentHeader));

      LightColourTextBox.Text = App.Settings.TalentLightColour;
      DarkColourTextBox.Text = App.Settings.TalentDarkColour;

      TalentFooterLogoCheckBox.Checked = App.Settings.ShowBurningGlassLogoInTalent;

      SharingDefaultsEmployerUsersSameFEINCheckBox.Checked = App.Settings.SharingJobsForSameFEIN;

      HiringManagerSearchApprovalCheckBox.Checked = !App.Settings.TalentPoolSearch;
      //NewBusinessUnitSearchCheckBox.Checked = !App.Settings.BusinessUnitTalentPoolSearch;
      //NewHiringManagerSearchCheckBox.Checked = !App.Settings.HiringManagerTalentPoolSearch;

      ScreenPreferencesRow.Visible = App.Settings.ScreeningPrefExclusiveToMinStarMatch;
      ResumeReferralApprovals.Visible = (App.Settings.Theme == FocusThemes.Workforce &&
                                         App.Settings.ScreeningPrefBelowStarMatchApproval);

      if (App.Settings.EmployerPreferencesOverideSystemDefaults)
        PreferenceDefaultsEmployerRadioButton.Checked = true;
      else
        PreferenceDefaultsSystemRadioButton.Checked = true;

      if (App.Settings.PreScreeningServiceRequest)
        PreScreeningYesRadioButton.Checked = true;
      else
        PreScreeningNoRadioButton.Checked = true;

      //NewBusinessUnitAccountRow.Visible = App.Settings.DisplayFEIN != ControlDisplayType.Hidden;
      ActivateYellowWordFilteringCheckBox.Checked = App.Settings.ActivateYellowWordFilteringForStaffCreatedJobs;
    }

    /// <summary>
    /// Sets visibility of controls based on the theme
    /// </summary>
    private void ApplyTheme()
    {
	    PreScreeningPlaceHolder.Visible = false;
      SharingDefaultsPlaceHolder.Visible = true;
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      LocaliseSaveChangesTopButton.Text = EditLocalisationLink("SaveChangesButton.Text.NoEdit");
      SaveChangesTopButton.Text =
        SaveChangesBottomButton.Text = CodeLocalise("SaveChangesButton.Text.NoEdit", "Save changes");
      LocaliseResetColoursButton.Text = EditLocalisationLink("ResetColoursButton.Text.NoEdit");
      ResetColoursButton.Text = CodeLocalise("ResetColoursButton.Text.NoEdit", "Reset colors");
      TalentFooterLogoCheckBox.Text = CodeLocalise("FooterLogoCheckBox.Text", "Display Burning Glass logo in footers");
      DarkColourValidator.ErrorMessage = CodeLocalise("DarkColour.Required", "Please Select a Color");
      LightColourValidator.ErrorMessage = CodeLocalise("LightColour.Required", "Please Select a Color");

      PreScreeningLabel.Text = CodeLocalise("PreScreening.Label", "Show services request?");

      LightColourTextBox.ToolTip = CodeLocalise("LightColour.Label", "Light color");
      DarkColourTextBox.ToolTip = CodeLocalise("DarkColour.Label", "Dark color");

      ResumeReferralApprovalsDropDownList.ToolTip = CodeLocalise("ResumeReferralApprovals.Label", "Screening Preference defaults");
      ScreeningPrefrencesDefaultDropDownList.ToolTip = CodeLocalise("ScreeningPrefrencesDefault.Label", "Screening Preference Default");

			ApprovalDefaultsForCustomerJobsDropDownList.ToolTip = CodeLocalise("ApprovalDefaultsForCustomerJobsDropDownList.ToolTip", "Approval defaults for customer-created job postings");
			ApprovalDefaultsForStaffJobsDropDownList.ToolTip = CodeLocalise("ApprovalDefaultsForStaffJobsDropDownList.ToolTip", "Approval defaults for staff-created job postings");
		}

    private void ApplyBranding()
    {
      ApprovalDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      ApprovalDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      ApprovalDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      ColourDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      ColourDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      ColourDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      JobOrderSettingsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      JobOrderSettingsPanelPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      JobOrderSettingsPanelPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      LogoDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      LogoDefaultsPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      LogoDefaultsPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      SharingDefaultsHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      SharingDefaultsCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      SharingDefaultsCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
    }

    /// <summary>
    /// Binds the resume referral approvals drop down.
    /// </summary>
    private void BindResumeReferralApprovalsDropDown()
    {
      ResumeReferralApprovalsDropDownList.Items.Clear();
      ResumeReferralApprovalsDropDownList.Items.Add(new ListItem(CodeLocalise("StaffOnlyReferral.NoEdit", "Only staff may refer job seekers"), "-1"));
      ResumeReferralApprovalsDropDownList.Items.Add(new ListItem(CodeLocalise("SelfReferralAny.NoEdit", "Self referral for any job"), "0"));
      if (App.Settings.ScreeningPrefBelowStarMatchApproval)
      {
        ResumeReferralApprovalsDropDownList.Items.Add(new ListItem(CodeLocalise("SelfReferralRequires3+StarMatch.NoEdit", "Allow 3+ star matches to apply without approval"), "3"));
        ResumeReferralApprovalsDropDownList.Items.Add(new ListItem(CodeLocalise("SelfReferralRequires4+StarMatch.NoEdit", "Allow 4+ star matches to apply without approval"), "4"));
        ResumeReferralApprovalsDropDownList.Items.Add(new ListItem(CodeLocalise("SelfReferralRequires4+StarMatch.NoEdit", "Allow 5+ star matches to apply without approval"), "5"));
      }
      ResumeReferralApprovalsDropDownList.SelectValue((App.Settings.ResumeReferralApprovalsMinimumStars).ToString(CultureInfo.InvariantCulture));
    }

    private void BindScreenPreferenceDefaultDropDown()
    {
      ScreeningPrefrencesDefaultDropDownList.Items.Clear();
      ScreeningPrefrencesDefaultDropDownList.Items.Add(new ListItem(CodeLocalise("SelfReferralRequires3+StarMatch.NoEdit", "Allow only 3+ star matches to apply"), ((int)ScreeningPreferences.JobSeekersMustHave3StarMatchToApply).ToString()));
      ScreeningPrefrencesDefaultDropDownList.Items.Add(new ListItem(CodeLocalise("SelfReferralRequires4+StarMatch.NoEdit", "Allow only 4+ star matches to apply"), ((int)ScreeningPreferences.JobSeekersMustHave4StarMatchToApply).ToString()));
      ScreeningPrefrencesDefaultDropDownList.Items.Add(new ListItem(CodeLocalise("SelfReferralRequires4+StarMatch.NoEdit", "Allow only 5 star matches to apply"), ((int)ScreeningPreferences.JobSeekersMustHave5StarMatchToApply).ToString()));

      ScreeningPrefrencesDefaultDropDownList.SelectValue(((int)App.Settings.ScreeningPrefExclusiveToMinStarMatchDefault).ToString(CultureInfo.InvariantCulture));
    }

		/// <summary>
		/// Binds the options for job approvals
		/// </summary>
		private void BindJobApprovalOptions()
		{
			ApprovalDefaultsForCustomerJobsDropDownList.Items.Clear();
			ApprovalDefaultsForCustomerJobsDropDownList.Items.AddEnum(JobApprovalOptions.NoApproval, CodeLocalise("CustomerJobsNoStaffApproval.NoEdit", "No staff approval necessary"));
			ApprovalDefaultsForCustomerJobsDropDownList.Items.AddEnum(JobApprovalOptions.ApproveAll, CodeLocalise("CustomerJobsStaffApproveAll.NoEdit", "All job postings require staff approval"));
			ApprovalDefaultsForCustomerJobsDropDownList.Items.AddEnum(JobApprovalOptions.SpecifiedApproval, CodeLocalise("CustomerJobsSpecifiedApproval.NoEdit", "Only specified job postings require staff approval"));

			ApprovalDefaultsForCustomerJobsDropDownList.SelectValue(App.Settings.ApprovalDefaultForCustomerCreatedJobs.ToString());

			ApprovalDefaultsForStaffJobsDropDownList.Items.Clear();
			ApprovalDefaultsForStaffJobsDropDownList.Items.AddEnum(JobApprovalOptions.NoApproval, CodeLocalise("StaffJobsNoStaffApproval.NoEdit", "No queue approval necessary"));
			ApprovalDefaultsForStaffJobsDropDownList.Items.AddEnum(JobApprovalOptions.ApproveAll, CodeLocalise("StaffJobsStaffApproveAll.NoEdit", "All job postings require queue approval"));
			ApprovalDefaultsForStaffJobsDropDownList.Items.AddEnum(JobApprovalOptions.SpecifiedApproval, CodeLocalise("StaffJobsSpecifiedApproval.NoEdit", "Only specified job postings require queue approval"));

			ApprovalDefaultsForStaffJobsDropDownList.SelectValue(App.Settings.ApprovalDefaultForStaffCreatedJobs.ToString());

			foreach (var check in Enum.GetValues(typeof (JobApprovalCheck)).Cast<JobApprovalCheck>())
			{
				AddJobApprovalCheckToList(ApprovalCheckStaffJobsCheckBoxList, App.Settings.ApprovalChecksForStaffCreatedJobs, check);
				AddJobApprovalCheckToList(ApprovalCheckCustomerJobsCheckBoxList, App.Settings.ApprovalChecksForCustomerCreatedJobs, check);
			}
		}

		/// <summary>
		/// Add a job approval check to a check box list
		/// </summary>
		/// <param name="list">The check box list</param>
		/// <param name="currentChecks">The current app settings</param>
		/// <param name="check">The check to add</param>
		private void AddJobApprovalCheckToList(CheckBoxList list, IEnumerable<JobApprovalCheck> currentChecks, JobApprovalCheck check)
		{
			var checkName = check.ToString();
            //var isMinimumAgeForOther = check == JobApprovalCheck.MinimumAgeForOther;
			var listItem = new ListItem(CodeLocalise(check, true), checkName)
			{
				Selected = currentChecks.Contains(check)
			};
			list.Items.Add(listItem);
		}

    /// <summary>
    /// Binds the job posting approval drop down
    /// </summary>
    private void BindNewEmployerApprovalsDropDownList()
    {
      NewEmployerAccountDropDownList.Items.Clear();
      NewEmployerAccountDropDownList.Items.AddEnum(TalentApprovalOptions.NoApproval);
      NewEmployerAccountDropDownList.Items.AddEnum(TalentApprovalOptions.ApprovalSystemAvailable);
      NewEmployerAccountDropDownList.Items.AddEnum(TalentApprovalOptions.ApprovalSystemNotAvailable);

      NewEmployerAccountDropDownList.SelectValue(App.Settings.NewEmployerApproval.ToString());
    }

    //private void BindNewBusinessUnitAccountDropdownList()
    //{
    //  NewBusinessUnitAccountDropDownList.Items.Clear();
    //  NewBusinessUnitAccountDropDownList.Items.AddEnum(TalentApprovalOptions.NoApproval);
    //  NewBusinessUnitAccountDropDownList.Items.AddEnum(TalentApprovalOptions.ApprovalSystemAvailable);
    //  NewBusinessUnitAccountDropDownList.Items.AddEnum(TalentApprovalOptions.ApprovalSystemNotAvailable);

    //  NewBusinessUnitAccountDropDownList.SelectValue(App.Settings.NewBusinessUnitApproval.ToString());
    //}

    //private void BindNewHiringManagerAccountDropdownList()
    //{
    //  NewHiringManagerAccountDropDownList.Items.Clear();
    //  NewHiringManagerAccountDropDownList.Items.AddEnum(TalentApprovalOptions.NoApproval);
    //  NewHiringManagerAccountDropDownList.Items.AddEnum(TalentApprovalOptions.ApprovalSystemAvailable);
    //  NewHiringManagerAccountDropDownList.Items.AddEnum(TalentApprovalOptions.ApprovalSystemNotAvailable);

    //  NewHiringManagerAccountDropDownList.SelectValue(App.Settings.NewHiringManagerApproval.ToString());
    //}

		/// <summary>
		/// Binds the requirements preferences drop-down with values from the enumeration
		/// </summary>
		private void BindRequirementsPreferenceDefaultDropdownList()
		{
			RequirementsPreferenceDefaultDropdownList.Items.Clear();
			RequirementsPreferenceDefaultDropdownList.Items.AddEnum(RequirementsPreference.None);
			RequirementsPreferenceDefaultDropdownList.Items.AddEnum(RequirementsPreference.CriteriaMetOnly);
			RequirementsPreferenceDefaultDropdownList.Items.AddEnum(RequirementsPreference.AllowAll);

			RequirementsPreferenceDefaultDropdownList.SelectValue(App.Settings.RequirementsPreferenceDefault.ToString());
		}

    /// <summary>
    /// Registers the javascript.
    /// </summary>
    private void RegisterJavascript()
    {
      var js = @"function pageLoad() {
	$(document).ready(function()
	{
		validateNewEmployerApproval();
		$(""#" + NewEmployerAccountDropDownList.ClientID + @""").change(validateNewEmployerApproval)
	});
}

	function validateNewEmployerApproval()
	{
		var newEmployersApprovalOption = {
                NoApproval: '" + TalentApprovalOptions.NoApproval + @"',
                ApprovalSystemAvailable: '" + TalentApprovalOptions.ApprovalSystemAvailable + @"',
                ApprovalSystemNotAvailable: '" + TalentApprovalOptions.ApprovalSystemNotAvailable + @"'
            };

			$(""#" + NewEmployerCensorshipFilteringCheckBox.ClientID + @""").prop('disabled', $(""#" + NewEmployerAccountDropDownList.ClientID + @""").val() === newEmployersApprovalOption.NoApproval);
 
	}";

      ScriptManager.RegisterClientScriptBlock(this, typeof(TalentDefaults), "TalentDefaultsJavaScript", js, true);
    }
  }
}