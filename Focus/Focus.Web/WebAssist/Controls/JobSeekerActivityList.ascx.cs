﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employer;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Core;
using Focus.Web.Code;

#endregion

namespace Focus.Web.WebAssist.Controls
{
    public partial class JobSeekerActivityList : UserControlBase
    {
        #region Properties

        private int _activityCount;
        protected string CloseJobSeekerWindowErrorMessage = "";

        private JobSeekerCriteria JobSeekerCriteria
        {
            get { return GetViewStateValue<JobSeekerCriteria>("ActivitySummaryPanel:JobSeekerCriteria"); }
            set { SetViewStateValue("ActivitySummaryPanel:JobSeekerCriteria", value); }
        }

        private ActionTypes? SelectedAction
        {
            get { return GetViewStateValue<ActionTypes>("JobSeekers:SelectedAction"); }
            set { SetViewStateValue("JobSeekers:SelectedAction", value); }
        }

        internal JobSeekerProfileModel JobSeekerProfileModel
        {
            get { return GetViewStateValue<JobSeekerProfileModel>("JobSeekerProfile:JobSeekerProfileModel"); }
            set { SetViewStateValue("JobSeekerProfile:JobSeekerProfileModel", value); }
        }

        internal long JobSeekerId
        {
            get { return GetViewStateValue<long>("JobSeekerProfile:JobSeekerId"); }
            set { SetViewStateValue("JobSeekerProfile:JobSeekerId", value); }
        }

        private static PersonActivityUpdateSettingsDto staffActivityUpdateSettings;

        #endregion

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (JobSeekerId == 0)
                    throw new Exception(FormatError(ErrorTypes.JobSeekerListNotFound, "Invalid #CANDIDATETYPE# id"));

                staffActivityUpdateSettings = ServiceClientLocator.StaffClient(App).GetBackdateSettings(App.User.PersonId ?? 0);

                JobSeekerCriteria = new JobSeekerCriteria
                {
                    JobSeekerId = JobSeekerId,
                    DaysBack = 30,
                    OrderBy = Constants.SortOrders.DateReceivedDesc,
                    FetchOption = CriteriaBase.FetchOptions.PagedList,
                };

                BindDaysBackDropdown();
                BindServiceDropDown();
                BindUserDropDown();
                BindJobSeekerActivityList();
                BindActionDropDown();
                LocaliseUI();
            }

            CloseJobSeekerWindowErrorMessage = CodeLocalise("CloseJobSeekerWindow.Error.Text", "You can only have one job seeker window open at a time");
        }

        #region Formatting

        /// <summary>
        /// Formats the sorting elements.
        /// </summary>
        private void FormatSortingElements()
        {

            if (!JobSeekerActivityListPager.Visible) return;

            var userNameSortAscButton = (ImageButton)JobSeekerActivityLogList.FindControl("UserNameSortAscButton");
            var userNameSortDescButton = (ImageButton)JobSeekerActivityLogList.FindControl("UserNameSortDescButton");
            var activityDateSortAscButton = (ImageButton)JobSeekerActivityLogList.FindControl("ActivityDateSortAscButton");
            var activityDateSortDescButton = (ImageButton)JobSeekerActivityLogList.FindControl("ActivityDateSortDescButton");

            userNameSortAscButton.ImageUrl = activityDateSortAscButton.ImageUrl = UrlBuilder.CategorySortAscImage();
            userNameSortDescButton.ImageUrl = activityDateSortDescButton.ImageUrl = UrlBuilder.CategorySortDescImage();

            userNameSortAscButton.Enabled =
                activityDateSortAscButton.Enabled = userNameSortDescButton.Enabled = activityDateSortDescButton.Enabled = true;

            switch (JobSeekerCriteria.OrderBy)
            {
                case Constants.SortOrders.ActivityUsernameAsc:
                    userNameSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
                    userNameSortAscButton.Enabled = false;
                    break;

                case Constants.SortOrders.ActivityUsernameDesc:
                    userNameSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
                    userNameSortDescButton.Enabled = false;
                    break;

                case Constants.SortOrders.ActivityDateAsc:
                    activityDateSortAscButton.ImageUrl = UrlBuilder.ActivityListSortAscImage();
                    activityDateSortAscButton.Enabled = false;
                    break;

                default:
                    activityDateSortDescButton.ImageUrl = UrlBuilder.ActivityListSortDescImage();
                    activityDateSortDescButton.Enabled = false;
                    break;
            }
        }

        #endregion

        #region Localise UI

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            ActionGoButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
            ActionDropDownRequired.ErrorMessage = CodeLocalise("ActionDropDownRequired.ErrorMessage", "Action required");
            SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignToStaffSubActionDropDownRequired.ErrorMessage", "Staff member required");
            SubActionButton.Text = CodeLocalise("SubActionButton.Text", "Assign");
        }

        #endregion

        #region Bind data

        /// <summary>
        /// Binds the list
        /// </summary>
        /// <param name="updatePanel">Whether to do an update on the UpdatePanel</param>
        public void Bind(bool updatePanel = false)
        {
            BindJobSeekerActivityList();
            if (updatePanel)
                JobSeekerActivityListUpdatePanel.Update();
        }

        /// <summary>
        /// Binds the job seeker activity list.
        /// </summary>
        private void BindJobSeekerActivityList()
        {
            JobSeekerActivityLogList.DataBind();

            // Set visibility of other controls
            JobSeekerActivityListPager.Visible = (JobSeekerActivityListPager.TotalRowCount > 0);
            FormatSortingElements();
        }

        /// <summary>
        /// Binds the service drop down.
        /// </summary>
        private void BindServiceDropDown()
        {
            var listItems = new List<ListItem>();

            ServiceDropDown.Items.Clear();

            // Generic items
            listItems.Add(new ListItem(CodeLocalise(ActionTypes.InviteJobSeekerToApply, "Job recommendation sent"), ActionTypes.InviteJobSeekerToApply.ToString()));
            listItems.Add(new ListItem(CodeLocalise(ActionTypes.CreateNewResume, "Added new resume"), ActionTypes.CreateNewResume.ToString()));
            listItems.Add(new ListItem(CodeLocalise(ActionTypes.MarkCandidateIssuesResolved, "Resolved Issues"), ActionTypes.MarkCandidateIssuesResolved.ToString()));
            listItems.Add(new ListItem(CodeLocalise(ActionTypes.AutoResolvedIssue, "Auto-resolved Issues"), ActionTypes.AutoResolvedIssue.ToString()));
            listItems.Add(new ListItem(CodeLocalise(ActionTypes.ExternalReferral, "Self-referred (External)"), ActionTypes.ExternalReferral.ToString()));
            listItems.Add(new ListItem(CodeLocalise(ActionTypes.ExternalStaffReferral, "Staff-referral (External)"), ActionTypes.ExternalStaffReferral.ToString()));
            listItems.Add(new ListItem(CodeLocalise(ActionTypes.UnblockUser, "Unblocked account"), ActionTypes.UnblockUser.ToString()));
            listItems.Add(new ListItem(CodeLocalise(ActionTypes.SaveResume, "Updated resume"), ActionTypes.SaveResume.ToString()));

            if (App.Settings.Theme == FocusThemes.Workforce)
            {
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.ActivateAccount, "#CANDIDATETYPE# account reactivated"), ActionTypes.ActivateAccount.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.InactivateAccount, "#CANDIDATETYPE# account inactivated"), ActionTypes.InactivateAccount.ToString()));
                if (App.Settings.EnableJSActivityBackdatingDateSelection)
                {
                    listItems.Add(new ListItem(CodeLocalise(ActionTypes.AssignActivityToCandidate, "Assigned activities/services"), ActionTypes.AssignActivityToCandidate.ToString()));
                }
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.AutoApprovedReferralBypass, "Auto-approved referral request"), ActionTypes.AutoApprovedReferralBypass.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.ReapplyReferralRequest, "#CANDIDATETYPE# reapply requests"), ActionTypes.ReapplyReferralRequest.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.UpdateJobSeekerAssignedStaffMember, "Assigned to staff member"), ActionTypes.UpdateJobSeekerAssignedStaffMember.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.ChangeJobSeekerSsn, "SSN updated"), ActionTypes.ChangeJobSeekerSsn.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.UnsubscribeEmail, "Email unsubscribe requests"), ActionTypes.UnsubscribeEmail.ToString()));
            }

            if (App.Settings.TalentModulePresent)
            {
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.CreateCandidateApplication, "Staff-referral (regular)"), ActionTypes.CreateCandidateApplication.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.HoldCandidateReferral, "Referral request put on hold"), ActionTypes.HoldCandidateReferral.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.UpdateReferralStatusToAutoOnHold, "Referral request put on auto-hold"), ActionTypes.UpdateReferralStatusToAutoOnHold.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.InviteJobSeekerToApplyThroughTalent, "Invitation to apply sent"), ActionTypes.InviteJobSeekerToApplyThroughTalent.ToString()));
                // This item is set to catch all actions of type UpdateApplicationStatusTo
                listItems.Add(new ListItem(CodeLocalise("ReferralOutcomeUpdated.NoEdit", "Referral outcome updated"), ActionTypes.UpdateApplicationStatusToNotApplicable.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.ReferralRequest, "Referral request"), ActionTypes.ReferralRequest.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.ApproveCandidateReferral, "Referral request approved"), ActionTypes.ApproveCandidateReferral.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.DenyCandidateReferral, "Referral request denied"), ActionTypes.DenyCandidateReferral.ToString()));
                listItems.Add(new ListItem(CodeLocalise(ActionTypes.SelfReferral, "Self-referred (Regular)"), ActionTypes.SelfReferral.ToString()));
                // See FVN-3139
                // listItems.Add(new ListItem(CodeLocalise(ActionTypes.StaffReferralDeprecated, "Staff-referral (Deprecated)"), ActionTypes.StaffReferralDeprecated.ToString()));

                if (App.Settings.Theme == FocusThemes.Education)
                    listItems.Add(new ListItem(CodeLocalise(ActionTypes.SendContactRequest, "Contact request sent"), ActionTypes.SendContactRequest.ToString()));
            }

            listItems.Add(new ListItem(CodeLocalise(ActionTypes.BlockUser, "Blocked account"), ActionTypes.BlockUser.ToString()));
            if (App.Settings.ShowStaffAssistedLMI)
            {
                ServiceDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.StaffAssistedLMI, "Staff assisted LMI"), ActionTypes.StaffAssistedLMI.ToString()));
            }

            // Sort Alphabetically
            foreach (var listItem in listItems.OrderBy(o => o.Text))
            {
                ServiceDropDown.Items.Add(listItem);
            }

            ServiceDropDown.Items.Insert(0, new ListItem(CodeLocalise("ServiceDropDown.AllServices.NoEdit", "all activities")));


        }

        /// <summary>
        /// Binds the days back dropdown.
        /// </summary>
        private void BindDaysBackDropdown()
        {
            DaysBackDropDown.Items.Clear();
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.TheLastDay.NoEdit", "the last day"), "1"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The7Days.NoEdit", "last 7 days"), "7"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The14Days.NoEdit", "last 14 days"), "14"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The30Days.NoEdit", "last 30 days"), "30"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The60Days.NoEdit", "last 60 days"), "60"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The90Days.NoEdit", "last 90 days"), "90"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.The180Days.NoEdit", "last 180 days"), "180"));
            DaysBackDropDown.Items.Add(new ListItem(CodeLocalise("DaysBackDropdown.AllDays.NoEdit", "all"), "0"));
            DaysBackDropDown.SelectedIndex = 3;
        }

        /// <summary>
        /// Binds the user drop down.
        /// </summary>
        private void BindUserDropDown()
        {
            UserDropDown.Items.Clear();
            UserDropDown.Items.Add(new ListItem(CodeLocalise("JobSeekerUserDropDown.AllUsers.NoEdit", "all users")));
            var users = ServiceClientLocator.CandidateClient(App).GetJobSeekerActivityUsers(JobSeekerId);
            if (users.IsNullOrEmpty())
                return;

            foreach (var user in users)
                UserDropDown.Items.Add(new ListItem(user.UserName, user.Id.ToString()));
        }

        /// <summary>
        /// Binds the action description.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="literal">The control.</param>
        /// <param name="tooltip">A tooltip control.</param>
        private void BindActionDescription(JobSeekerActivityActionViewDto action, Literal literal, Literal tooltip)
        {
            var text = "** " + CodeLocalise("Global.Unknown.Label", "Unknown") + " **";
            var additionalDetails = string.Empty;
            var toolTipDetails = string.Empty;

            ActionTypes actionType;
            if (!Enum.TryParse(action.ActionType, out actionType))
            {
                literal.Text = text;
                return;
            }

            switch (actionType)
            {
                case ActionTypes.AssignActivityToCandidate:
                    var activity = ServiceClientLocator.CoreClient(App).GetActivity(action.EntityIdAdditional01.GetValueOrDefault());

                    text = activity.IsNull()
                        ? String.Format("{0} ({1})", CodeLocalise("AssignActivity", "Assign activity"), CodeLocalise("UnknownActivity", "Unknown activity"))
                        : String.Format("{0} ({1}/{2})", CodeLocalise("AssignActivity", "Assign activity"), CodeLocalise(activity.CategoryLocalisationKey, activity.CategoryName), CodeLocalise(activity.ActivityLocalisationKey, activity.ActivityName));

                    break;

                case ActionTypes.SaveResume:
                    text = CodeLocalise("UpdatedResume", "Updated resume");
                    break;

                case ActionTypes.CreateNewResume:
                    text = CodeLocalise("AddedNewResume", "Added new resume");
                    break;

                case ActionTypes.SelfReferral:
                    text = CodeLocalise("SelfReferral", "Self-referral (regular)");
                    break;

                case ActionTypes.ReferralRequest:
                    text = CodeLocalise("ReferralRequest", "Referral request");
                    break;

                case ActionTypes.ApproveCandidateReferral:
                    text = CodeLocalise("ApproveCandidateReferral", "Referral request approved");
                    break;

                case ActionTypes.DenyCandidateReferral:
                    text = CodeLocalise("DenyCandidateReferral", "Referral request denied");
                    break;

                case ActionTypes.InviteJobSeekerToApply:
                    text = action.EntityIdAdditional02 == 1
                        ? CodeLocalise("InvitationToApplyQueued", "Job recommendation is queued")
                        : CodeLocalise("InvitationToApply", "Job recommendation sent");
                    break;

                case ActionTypes.InviteJobSeekerToApplyThroughTalent:
                    text = action.EntityIdAdditional02 == 1
                        ? CodeLocalise("InvitationToApplyQueued", "Invitation to apply is queued")
                        : CodeLocalise("InvitationToApply", "Invitation to apply sent");
                    break;

                case ActionTypes.UpdateApplicationStatusToRecommended:
                    text = CodeLocalise("UpdateApplicationStatusToRecommended", "Referral outcome updated to 'Recommended'");
                    break;

                case ActionTypes.UpdateApplicationStatusToFailedToShow:
                    text = CodeLocalise("UpdateApplicationStatusToFailedToShow", "Referral outcome updated to 'Failed to show'");
                    break;

                case ActionTypes.UpdateApplicationStatusToHired:
                    text = CodeLocalise("UpdateApplicationStatusToHired", "Referral outcome updated to 'Hired'");
                    break;

                case ActionTypes.UpdateApplicationStatusToInterviewScheduled:
                    text = CodeLocalise("UpdateApplicationStatusToInterviewScheduled", "Referral outcome updated to 'Interview scheduled'");
                    break;

                case ActionTypes.UpdateApplicationStatusToNewApplicant:
                    text = CodeLocalise("UpdateApplicationStatusToNewApplicant", "Referral outcome updated to 'New applicant'");
                    break;

                case ActionTypes.UpdateApplicationStatusToNotApplicable:
                    text = CodeLocalise("UpdateApplicationStatusToNotApplicable", "Referral outcome updated to 'Not applicable'");
                    break;

                case ActionTypes.UpdateApplicationStatusToOfferMade:
                    text = CodeLocalise("UpdateApplicationStatusToOfferMade", "Referral outcome updated to 'Offer made'");
                    break;

                case ActionTypes.UpdateApplicationStatusToNotHired:
                    text = CodeLocalise("UpdateApplicationStatusToNotHired", "Referral outcome updated to 'Not hired'");
                    break;

                case ActionTypes.UpdateReferralStatusToAutoDenied:
                    text = CodeLocalise("UpdateReferralStatusToAutoDenied", "Referral outcome updated to 'auto-denied'");
                    break;

                case ActionTypes.UpdateApplicationStatusToUnderConsideration:
                    text = CodeLocalise("UpdateApplicationStatusToUnderConsideration", "Referral outcome updated to 'Under consideration'");
                    break;

                case ActionTypes.UpdateApplicationStatusToDidNotApply:
                    text = CodeLocalise("UpdateApplicationStatusToDidNotApply", "Referral outcome updated to 'Did not apply'");
                    break;

                case ActionTypes.UpdateApplicationStatusToInterviewDenied:
                    text = CodeLocalise("UpdateApplicationStatusToInterviewDenied", "Referral outcome updated to 'Interview denied'");
                    break;

                case ActionTypes.UpdateApplicationStatusToRefusedOffer:
                    text = CodeLocalise("UpdateApplicationStatusToRefusedJob", "Referral outcome updated to 'Refused job'");
                    break;

                case ActionTypes.UpdateApplicationStatusToSelfReferred:
                    text = CodeLocalise("UpdateApplicationStatusToSelfReferred", "Referral outcome updated to 'Self referred'");
                    break;

                case ActionTypes.ExternalReferral:
                    text = CodeLocalise("ExternalReferral", "Self-referral (external)");
                    break;

                case ActionTypes.ExternalStaffReferral:
                    text = CodeLocalise("ExternalReferral", "Staff-referral (external)");
                    break;

                case ActionTypes.UpdateApplicationStatusToRefusedReferral:
                    text = CodeLocalise("UpdateApplicationStatusToRefusedReferral", "Referral outcome updated to 'Refused referral'");
                    break;

                case ActionTypes.UpdateApplicationStatusToNotYetPlaced:
                    text = CodeLocalise("UpdateApplicationStatusToNotYetPlaced", "Referral outcome updated to 'Not Yet Placed'");
                    break;

                case ActionTypes.UpdateApplicationStatusToNotQualified:
                    text = CodeLocalise("UpdateApplicationStatusToNotQualified", "Referral outcome updated to 'Not Qualified'");
                    break;

                case ActionTypes.UpdateApplicationStatusToJobAlreadyFilled:
                    text = CodeLocalise("UpdateApplicationStatusToJobAlreadyFilled", "Referral outcome updated to 'Job already filled'");
                    break;

                case ActionTypes.UpdateApplicationStatusToFoundJobFromOtherSource:
                    text = CodeLocalise("UpdateApplicationStatusToFoundJobFromOtherSource", "Referral outcome updated to 'Job found from other source'");
                    break;

                case ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation:
                    text = CodeLocalise("UpdateApplicationStatusToFailedToRespondToInvitation", "Referral outcome updated to 'Failed to respond to invitation'");
                    break;

                case ActionTypes.UpdateApplicationStatusToFailedToReportToJob:
                    text = CodeLocalise("UpdateApplicationStatusToFailedToReportToJob", "Referral outcome updated to 'Failed to report to job'");
                    break;
                // See FVN-3139
                // case ActionTypes.StaffReferralDeprecated:
                //   text = CodeLocalise("StaffReferralDeprecated", "Staff-referral (Deprecated)");
                //   break;

                case ActionTypes.CreateCandidateApplication:
                    text = CodeLocalise("CreateCandidateApplication", "Staff-referral (regular)");
                    break;

                case ActionTypes.WaivedRequirements:
                    text = CodeLocalise("WaivedRequirements", "Waived requirements");
                    additionalDetails = action.AdditionalDetails;
                    break;

                case ActionTypes.ChangeJobSeekerSsn:
                    text = CodeLocalise(ActionTypes.ChangeJobSeekerSsn, "SSN updated");
                    break;

                case ActionTypes.UpdateJobSeekerAssignedStaffMember:
                    if (action.EntityIdAdditional02.IsNotNull())
                    {
                        var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(personId: Convert.ToInt64(action.EntityIdAdditional02));
                        var assignedStaffMember = userDetails.PersonDetails.FirstName + " " + userDetails.PersonDetails.LastName;
                        text = CodeLocalise("AssignJobSeekerToMyself", string.Format("Assigned to {0}", assignedStaffMember));
                    }
                    break;

                case ActionTypes.UnsubscribeEmail:
                    UnsubscribeTypes unsubscribeTypes;
                    Enum.TryParse(action.EntityIdAdditional02.ToString(), true, out unsubscribeTypes);
                    if (unsubscribeTypes == UnsubscribeTypes.VeteranPriorityEmails)
                        text = CodeLocalise("UnsubscribeVeteranEmails", "Unsubscribed from veteran priority job alerts");
                    if (unsubscribeTypes == UnsubscribeTypes.BlastEmails)
                        text = CodeLocalise("UnsubscribeBlastEmail", "Unsubscribed from blast emails");
                    break;
                case ActionTypes.BlockUser:
                    if (action.EntityIdAdditional02.IsNotNullOrZero())
                    {
                        var blockedReason = (BlockedReason)(action.EntityIdAdditional02);
                        var blockedReasonText = CodeLocalise(blockedReason, true);
                        text = CodeLocalise("AutoBlocked.Text", "Auto blocked; {0}", blockedReasonText);
                    }
                    else
                    {
                        text = CodeLocalise("BlockUser.ActionDescription", "Administrative block placed on account");
                        toolTipDetails = action.AdditionalDetails;
                    }
                    break;

                case ActionTypes.UnblockUser:
                    text = CodeLocalise("UnblockUser", "Unblocked account");
                    break;

                case ActionTypes.AutoResolvedIssue:
                    var autoResolvedIssueText = "Unknown";
                    CandidateIssueType candidateAutoResolvedIssue;
                    if (Enum.TryParse(action.AdditionalDetails, true, out candidateAutoResolvedIssue))
                        autoResolvedIssueText = CodeLocalise(candidateAutoResolvedIssue, true);

                    text = CodeLocalise("AutoResolvedIssue", "Issue auto-resolved (flagged for {0})", autoResolvedIssueText.ToLowerInvariant());
                    break;

                case ActionTypes.MarkCandidateIssuesResolved:
                    var issueText = "Unknown";
                    CandidateIssueType candidateIssue;
                    if (Enum.TryParse(action.AdditionalDetails, true, out candidateIssue))
                        issueText = CodeLocalise(candidateIssue, true);

                    text = CodeLocalise("MarkCandidateIssuesResolved", "Issue resolved (flagged for {0})", issueText.ToLowerInvariant());
                    break;

                case ActionTypes.ReapplyReferralRequest:
                    text = CodeLocalise(actionType + ".ActionDescription", "#CANDIDATETYPE# reapply request");
                    break;

                case ActionTypes.AutoApprovedReferralBypass:
                    text = CodeLocalise(actionType + ".ActionDescription", "Auto-approved referral request");
                    break;

                case ActionTypes.StaffAssistedLMI:
                    text = CodeLocalise("StaffAssistedLMI", "Staff assisted LMI");
                    break;

                case ActionTypes.HoldCandidateReferral:
                    text = CodeLocalise("HoldCandidateReferral.ActionDescription", "Referral request put on hold");
                    break;

                case ActionTypes.UpdateReferralStatusToAutoOnHold:
                    text = CodeLocalise("UpdateReferralStatusToAutoOnHold.ActionDescription", "Referral request put on auto-hold");
                    break;

                case ActionTypes.InactivateAccount:
                    text = CodeLocalise("InactivateAccount.ActionDescription", "#CANDIDATETYPE# account inactivated");
                    break;

                case ActionTypes.ActivateAccount:
                    text = CodeLocalise("ActivateAccount.ActionDescription", "#CANDIDATETYPE# account reactivated");
                    break;

                case ActionTypes.SendContactRequest:
                    text = CodeLocalise("SendContactRequest.ActionDescription", "Contact request sent");
                    break;
            }

            if (action.JobTitle != null)
            {
                if (additionalDetails.Length > 0)
                    additionalDetails = string.Concat(": ", additionalDetails);

                var pattern = text.EndsWith(")") ? "{0}, {1}, {2}{3}, {4}" : "{0} ({1}, {2}{3}, {4})";

                text = String.Format(pattern,
                         text,
                         action.JobTitle,
                         action.BusinessUnitName.Length == 0 ? CodeLocalise("DefaultEmployerName.Text", "Kentucky Employer") : action.BusinessUnitName,
                         additionalDetails,
                                         action.JobId);
            }
            else
            {
                if (additionalDetails.IsNotNullOrEmpty())
                    additionalDetails = string.Concat(" (", additionalDetails, ")");

                text = string.Concat(text, additionalDetails);
            }

            literal.Text = text;

            if (toolTipDetails.IsNotNullOrEmpty())
            {
                tooltip.Text = HtmlTooltipster("tooltipWithArrow inlineTooltip", "", toolTipDetails.Replace("\n", "<br />"), "moreInfo");
            }
        }

        /// <summary>
        /// Binds the action drop down.
        /// </summary>
        private void BindActionDropDown()
        {
            ActionDropDown.Items.Clear();

            ActionDropDown.Items.AddLocalisedTopDefault("ActionDropDown.TopDefault.NoEdit", "- select action -");

            if (!App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAdministrator))
            {
                ActionPlaceHolder.Visible = false;
                return;
            }

            if (App.Settings.Theme != FocusThemes.Education)
            {
                if (!App.Settings.ShowAssetAccess && App.Settings.EnableJSActivityBackdatingDateSelection)
                    ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignActivityToCandidate, "Assign activities/services"), ActionTypes.AssignActivityToCandidate.ToString()));
                if (App.Settings.EnableSsnFeatureGroup)
                {
                    if (App.Settings.AssistShowChangeSSNActivityInWF)
                        ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.ChangeJobSeekerSsn, "Change #CANDIDATETYPE#:LOWER's SSN"), ActionTypes.ChangeJobSeekerSsn.ToString()));
                }
            }

            // TODO:
            // ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.FindJobsForSeeker, "Find jobs for #CANDIDATETYPE#"), ActionTypes.FindJobsForSeeker.ToString()));

            ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AddCandidateToList, "Add #CANDIDATETYPE# to list"), ActionTypes.AddCandidateToList.ToString()));

            if (App.Settings.OfficesEnabled)
            {
                var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(Convert.ToInt64(App.User.UserId));
                if (userDetails.PersonDetails.Manager.IsNotNull() && Convert.ToBoolean(userDetails.PersonDetails.Manager))
                {
                    ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignJobSeekerToStaff, "Assign #CANDIDATETYPE# to staff"), ActionTypes.AssignJobSeekerToStaff.ToString()));
                }
                else
                {
                    ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignJobSeekerToMyself, "Assign #CANDIDATETYPE#:LOWER to myself"), ActionTypes.AssignJobSeekerToMyself.ToString()));
                }
            }

            ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.EmailCandidate, "Email #CANDIDATETYPE#"), ActionTypes.EmailCandidate.ToString()));

            if (App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAccountInactivator))
            {
                if (JobSeekerProfileModel.JobSeekerProfileView.Enabled)
                    ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.InactivateReactivateJobSeeker, "Inactivate #CANDIDATETYPE#:LOWER"), ActionTypes.InactivateReactivateJobSeeker.ToString()));
                else if (!JobSeekerProfileModel.JobSeekerProfileView.Enabled)
                    ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.InactivateReactivateJobSeeker, "Reactivate #CANDIDATETYPE#:LOWER"), ActionTypes.InactivateReactivateJobSeeker.ToString()));
            }


            if (App.Settings.ShowAssetAccess)
                ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.OpenAccountInAsset, "Open account in Asset"), ActionTypes.OpenAccountInAsset.ToString()));

            ActionDropDown.SelectedIndex = 0;
        }

        #endregion

        # region Events

        /// <summary>
        /// Handles the SelectedIndexChanged event of the ServiceDropDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ServiceDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ServiceDropDown.SelectedIndex == 0)
                JobSeekerCriteria.Action = null;
            else
            {
                ActionTypes actionType;
                Enum.TryParse(ServiceDropDown.SelectedValue, out actionType);
                JobSeekerCriteria.Action = actionType;
            }

            JobSeekerActivityListPager.ReturnToFirstPage();
            BindJobSeekerActivityList();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the DaysBackDropdown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void DaysBackDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {
            JobSeekerCriteria.DaysBack = Convert.ToInt32(DaysBackDropDown.SelectedValue);
            JobSeekerActivityListPager.ReturnToFirstPage();
            BindJobSeekerActivityList();
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the UserDropDown control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void UserDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UserDropDown.SelectedIndex == 0)
                JobSeekerCriteria.UserId = null;
            else
                JobSeekerCriteria.UserId = Convert.ToInt64(UserDropDown.SelectedValue);

            JobSeekerActivityListPager.ReturnToFirstPage();
            BindJobSeekerActivityList();
        }

        /// <summary>
        /// Handles the Selecting event of the JobSeekerActivityListDataSource control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
        protected void JobSeekerActivityListDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters["criteria"] = JobSeekerCriteria;

            var pager = JobSeekerActivityListPager.PageSizerControl;

            // Fix, for Firefox, because it seems to reset the selected index of the contol (See FVN-320)
            var script = string.Format(@"
        $(document).ready(function() {{
          $('#{0}').prop('selectedIndex', {1});
        }});", pager.ClientID, pager.SelectedIndex);

            Page.ClientScript.RegisterClientScriptBlock(GetType(), "PagerFix", script, true);
        }

        /// <summary>
        /// Handles the Sorting event of the JobSeekerActivityLogList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
        protected void JobSeekerActivityLogList_Sorting(object sender, ListViewSortEventArgs e)
        {
            JobSeekerCriteria.OrderBy = e.SortExpression;
            FormatSortingElements();
        }

        /// <summary>
        /// Handles the LayoutCreated event of the JobSeekerActivityLogList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void JobSeekerActivityLogList_LayoutCreated(object sender, EventArgs e)
        {
            // Set labels
            ((Literal)JobSeekerActivityLogList.FindControl("UserName")).Text = CodeLocalise("UserName.Text", "USER NAME");
            ((Literal)JobSeekerActivityLogList.FindControl("ActivityDate")).Text = CodeLocalise("ActivityDate.Text", "DATE");
            ((Literal)JobSeekerActivityLogList.FindControl("Action")).Text = CodeLocalise("Action.Text", "ACTIVITY/SERVICE");
        }

        /// <summary>
        /// Handles the ItemDataBound event of the JobSeekerActivityLogList control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
        protected void JobSeekerActivityLogList_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var action = (JobSeekerActivityActionViewDto)e.Item.DataItem;
            var literal = (Literal)e.Item.FindControl("ActionDescription");
            var tooltip = (Literal)e.Item.FindControl("TooltipDetails");
            BindActionDescription(action, literal, tooltip);

            var backdateButton = (LinkButton)e.Item.FindControl("JobSeekerBackdateLinkbutton");
            var deleteButton = (LinkButton)e.Item.FindControl("JobSeekerDeleteLinkButton");

            var dbStaffActivityUpdateSettings = ServiceClientLocator.StaffClient(App).GetBackdateSettings(App.User.PersonId ?? 0);

            #region Toggle Backdate/Delete LinkButtons
            //check whether Activity Backdate/Delete settings exist for the staff and Activity Backdate/Delete is Enable for Job seeker
            if (dbStaffActivityUpdateSettings.IsNotNull() && App.Settings.EnableJSActivityBackdatingDateSelection && action.ActionType.Equals(ActionTypes.AssignActivityToCandidate.ToString()))
            {
                var currentUserRoles = ServiceClientLocator.AccountClient(App).GetUsersRoles(App.User.UserId);
                //set visibility if Staff has permissions to Backdate/Delete Any Activity.
                //backdateButton.Visible = App.User.IsInRole(Constants.RoleKeys.BackdateAnyEntryJSActivityLogMaxDays);
                backdateButton.Visible = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.BackdateAnyEntryJSActivityLogMaxDays)).IsNotNull() ? true : false;
                if (backdateButton.Visible)
                    backdateButton.Visible = dbStaffActivityUpdateSettings.BackdateAnyEntryJSActivityLogMaxDays.IsNotNull() ? (action.ActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.BackdateAnyEntryJSActivityLogMaxDays))) : false;

                //deleteButton.Visible = App.User.IsInRole(Constants.RoleKeys.DeleteAnyEntryJSActivityLogMaxDays);
                deleteButton.Visible = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.DeleteAnyEntryJSActivityLogMaxDays)).IsNotNull() ? true : false;
                if (deleteButton.Visible)
                    deleteButton.Visible = dbStaffActivityUpdateSettings.DeleteAnyEntryJSActivityLogMaxDays.IsNotNull() ? (action.ActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.DeleteAnyEntryJSActivityLogMaxDays))) : false;
                //get the staff person details
                var staffPersonDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId, App.User.PersonId ?? 0);

                //check whether the current staff is a manager before setting visibility to activities.
                if (staffPersonDetails.PersonDetails.Manager.IsNotNull() && (bool)staffPersonDetails.PersonDetails.Manager && !ServiceClientLocator.EmployerClient(App).IsPersonStatewide(App.User.PersonId ?? 0))
                {
                    //get the list of offices current staff is mapped with
                    var officesAssigned = staffPersonDetails.PersonOffices.Select(x => x.OfficeId).Cast<long>().ToList();
                    //get the list of offices activity staff is mapped with
                    var activityStaffUser = ServiceClientLocator.AccountClient(App).GetUserDetails(action.UserId);
                    if (!ServiceClientLocator.EmployerClient(App).IsPersonStatewide(activityStaffUser.PersonDetails.Id ?? 0))
                    {
                        var activityUserOffices = activityStaffUser.PersonOffices.Select(o => o.OfficeId).Cast<long>().ToList();
                        if (backdateButton.Visible)
                            backdateButton.Visible = officesAssigned.Intersect<long>(activityUserOffices).Any();
                        if (deleteButton.Visible)
                            deleteButton.Visible = officesAssigned.Intersect<long>(activityUserOffices).Any();
                    }
                }

                //check if the activity is staff's own before setting visibility for Backdate/Delete my entries
                if (action.UserId == App.User.UserId)
                {
                    //set visibility if staff has permission only to update his own Activity.
                    if (!backdateButton.Visible)
                    {
                        //backdateButton.Visible = App.User.IsInRole(Constants.RoleKeys.BackdateMyEntriesJSActivityLogMaxDays);
                        backdateButton.Visible = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.BackdateMyEntriesJSActivityLogMaxDays)).IsNotNull() ? true : false;
                        if (backdateButton.Visible)
                            backdateButton.Visible = dbStaffActivityUpdateSettings.BackdateMyEntriesJSActivityLogMaxDays.IsNotNull() ? (action.ActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.BackdateMyEntriesJSActivityLogMaxDays))) : false;
                    }
                    if (!deleteButton.Visible)
                    {
                        //deleteButton.Visible = App.User.IsInRole(Constants.RoleKeys.DeleteMyEntriesJSActivityLogMaxDays);
                        deleteButton.Visible = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.DeleteMyEntriesJSActivityLogMaxDays)).IsNotNull() ? true : false;
                        if (deleteButton.Visible)
                            deleteButton.Visible = dbStaffActivityUpdateSettings.DeleteMyEntriesJSActivityLogMaxDays.IsNotNull() ? (action.ActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.DeleteMyEntriesJSActivityLogMaxDays))) : false;
                    }
                }
            }
            else
                backdateButton.Visible = deleteButton.Visible = false;
            #endregion
        }

        /// <summary>
        /// Handles the Clicked event of the ActionButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ActionButton_Clicked(object sender, EventArgs e)
        {
            // Hide the sub action controls

            SelectedAction = ActionDropDown.SelectedValueToEnum<ActionTypes>();

            if (JobSeekerCriteria.JobSeekerId.IsNull()) return;

            switch (SelectedAction)
            {
                case ActionTypes.AssignActivityToCandidate:
                    ActivityAssigner.Visible = true;
                    SubActionDropDown.Visible = SubActionButton.Visible = false;
                    break;

                case ActionTypes.EmailCandidate:
                    ActivityAssigner.Visible = SubActionDropDown.Visible = SubActionButton.Visible = false;
                    OnJobSeekerActivityListEmailCandidateClick(new JobSeekerActivityListEmailCandidateClickEventArgs(JobSeekerId));
                    break;

                case ActionTypes.AssignJobSeekerToStaff:
                    ActivityAssigner.Visible = false;
                    BindSubActionDropDownJobSeekerToStaff();
                    SubActionDropDown.Visible = SubActionButton.Visible = true;
                    break;

                case ActionTypes.AddCandidateToList:
                    ActivityAssigner.Visible = SubActionDropDown.Visible = SubActionButton.Visible = false;
                    var details = GetJobSeekerDetails();
                    JobSeekerList.Show(new[] { details });
                    break;

                case ActionTypes.FindJobsForSeeker:
                    ActivityAssigner.Visible = SubActionDropDown.Visible = SubActionButton.Visible = false;
                    // TODO:
                    break;

                case ActionTypes.ChangeJobSeekerSsn:
                    ActivityAssigner.Visible = SubActionDropDown.Visible = SubActionButton.Visible = false;
                    EditSsnModal.Show(Convert.ToInt64(JobSeekerCriteria.JobSeekerId));
                    break;

                case ActionTypes.AssignJobSeekerToMyself:
                    var assistUserOffices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { PersonId = App.User.PersonId });
                    var jobSeekerOffices = ServiceClientLocator.EmployerClient(App).GetOfficesList(new OfficeCriteria { PersonId = JobSeekerId });

                    var jsOfficeIds = jobSeekerOffices.Select(x => x.Id).ToList();

                    if (assistUserOffices.Any(x => jsOfficeIds.Contains(x.Id)))
                    {
                        ServiceClientLocator.CandidateClient(App).AssignToStaffMember(new List<long> { JobSeekerId }, Convert.ToInt64(App.User.PersonId));

                        Confirmation.Show(CodeLocalise("AssignJobSeekerToStaffMemberConfirmation.Title", "#CANDIDATETYPE# assigned"),
                                                            CodeLocalise("AssignJobSeekerToStaffMemberConfirmation.Body", "The selected #CANDIDATETYPE#:LOWER has been successfully assigned to yourself."),
                                                            CodeLocalise("CloseModal.Text", "OK"));
                    }
                    else
                    {
                        ActionDropDownValidator.ErrorMessage = HtmlLocalise("JobSeekerNotInOffice.Error.Text",
                                                                                                                                             "Selected #CANDIDATETYPE#:LOWER is not in your assigned offices");
                        ActionDropDownValidator.IsValid = false;
                    }

                    break;

                case ActionTypes.InactivateReactivateJobSeeker:
                    if (JobSeekerProfileModel.JobSeekerProfileView.Enabled)
                    {
                        Confirmation.Show(CodeLocalise("InactivateJobSeeker.Title", "Inactivate #CANDIDATETYPE#:LOWER account"),
                            CodeLocalise("InactivateJobSeeker.Body", "By confirming this action the #CANDIDATETYPE#:LOWER’s resume will no longer appear in #FOCUSTALENT# searches and their account will be marked as inactive."),
                            CodeLocalise("Global.Close", "Close"),
                            "Inactivate",
                            okText: CodeLocalise("Global.OK", "OK"),
                            okCommandName: "Inactivate",
                            okCommandArgument: JobSeekerProfileModel.JobSeekerProfileView.Id.ToString());
                    }
                    else
                    {
                        var reactivateSingleSignOnKey = ServiceClientLocator.AuthenticationClient(App).CreateCandidateSingleSignOn(Convert.ToInt64(JobSeekerProfileModel.JobSeekerProfileView.Id), null);
                        var reactivateBlockedPopupMessage = CodeLocalise("ReactivateCandidatePopupBlocked.Error.Text",
                            "Unable to reactivate #CANDIDATETYPE# account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.");

                        var reactivateScript = string.Format("$(document).ready(function (){{JobSeekers_CareerPopup = showPopup('{0}{1}','{2}');}});", App.Settings.CareerApplicationPath, UrlBuilder.SingleSignOnWithReactivation(reactivateSingleSignOnKey, false), reactivateBlockedPopupMessage);
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ReactivateCandidate", reactivateScript, true);
                    }

                    break;

                case ActionTypes.OpenAccountInAsset:
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", string.Format("window.open('{0}');", App.Settings.AssetUrl), true);
                    break;
            }
        }


        /// <summary>
        /// Binds the sub action drop down job seeker to staff.
        /// </summary>
        private void BindSubActionDropDownJobSeekerToStaff()
        {
            #region Bind sub action to staff

            #region Get the Offices the manager handles work for

            var stateId = ServiceClientLocator.EmployerClient(App).GetManagerState(Convert.ToInt64(App.User.PersonId));

            var officeIds = stateId.IsNull()
                ? ServiceClientLocator.EmployerClient(App).GetOfficesPersonManages(Convert.ToInt64(App.User.PersonId)).Select(x => x.Id).ToList()
                : ServiceClientLocator.EmployerClient(App).GetOfficesByState(Convert.ToInt64(stateId)).Select(x => x.Id).ToList();

            #endregion

            SubActionDropDown.Items.Clear();

            var criteria = new StaffMemberCriteria
            {
                OfficeIds = officeIds,
                Enabled = true,
                Blocked = false,
                FetchOption = CriteriaBase.FetchOptions.List
            };
            var staffMembers = ServiceClientLocator.EmployerClient(App).GetOfficeStaffMembersList(criteria);
            var staffQuery = staffMembers.Select(s => new { s.Id, DisplayText = s.LastName + ", " + s.FirstName });

            SubActionDropDown.DataSource = staffQuery;
            SubActionDropDown.DataValueField = "Id";
            SubActionDropDown.DataTextField = "DisplayText";
            SubActionDropDown.DataBind();
            SubActionDropDown.Items.AddLocalisedTopDefault("StaffSubActionDropDown.TopDefault", "- select staff member name -");

            #endregion
        }

        /// <summary>
        /// Handles the Clicked event of the SubActionButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SubActionButton_Clicked(object sender, EventArgs e)
        {
            switch (SelectedAction)
            {
                case ActionTypes.AssignJobSeekerToStaff:
                    {
                        var staffId = SubActionDropDown.SelectedValueToLong();
                        var jobSeeker = GetJobSeekerDetails();

                        ServiceClientLocator.CandidateClient(App).AssignToStaffMember(new List<long> { jobSeeker.PersonId }, staffId);

                        Confirmation.Show(CodeLocalise("AssignJobSeekerToAdminConfirmation.Title", "Job seeker assigned"),
                                                                                     CodeLocalise("AssignJobSeekerToAdminConfirmation.Body", "The job seeker was assigned to the selected admin."),
                                                                                     CodeLocalise("CloseModal.Text", "OK"));

                        break;
                    }
            }
        }

        /// <summary>
        /// Handles the OnOkCommand event of the Confirmation control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
        protected void Confirmation_OnOkCommand(object sender, CommandEventArgs eventargs)
        {
            var success = false;

            switch (eventargs.CommandName)
            {
                case "Inactivate":
                    success = ServiceClientLocator.AccountClient(App).InactivateJobSeeker(eventargs.CommandArgument.AsLong(), AccountDisabledReason.InactivatedByStaffRequest);
                    if (success)
                    {
                        JobSeekerProfileModel.JobSeekerProfileView.Enabled = false;
                        Confirmation.Show(CodeLocalise("InactivateJobSeekerSuccess.Title", "Inactivate #CANDIDATETYPE#:LOWER account"),
                                          CodeLocalise("InactivateJobSeekerSuccess.Body", "#CANDIDATETYPE# successfully inactivated"),
                                          CodeLocalise("Global.Close", "Close"));

                        BindActionDropDown();
                    }
                    break;
            }

            if (success)
                BindJobSeekerActivityList();
        }

        #endregion

        /// <summary>
        /// Handles the select event of backdate and delete items in list.
        /// </summary>
        /// <param name="sender">The source of the event</param>
        /// <param name="e">The event.</param>
        protected void JobSeekerActivityLogList_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
            var activityId = commandArgs[0];
            if (commandArgs.Length > 1)
            {
                var activityTypeId = !commandArgs[1].ToString().Equals(null) ? commandArgs[1] : null;

                var dlUserId = !commandArgs[2].ToString().Equals(null) ? commandArgs[2] : null;
                var dlActionType = !commandArgs[3].ToString().Equals(null) ? commandArgs[3] : null;
                var dlActionedOn = !commandArgs[4].ToString().Equals(null) ? commandArgs[4] : null;

                var activityActionEventDate = ServiceClientLocator.CoreClient(App).GetActionEventDate(Convert.ToInt64(activityId));

                switch (e.CommandName)
                {
                    case "backdateActivity":
                        BackdateActivity.show(Convert.ToInt64(activityId), activityActionEventDate, ActivityType.JobSeeker, activityTypeId, Convert.ToInt64(dlUserId), dlActionType, Convert.ToDateTime(dlActionedOn));
                        break;
                    case "deleteActivity":
                        DeleteActivity.show(Convert.ToInt64(activityId), activityTypeId, Convert.ToInt64(dlUserId), dlActionType, Convert.ToDateTime(dlActionedOn));
                        break;
                }
            }
        }

        # region Data retrieval

        /// <summary>
        /// Gets the job seeker activity.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="startRowIndex">Start index of the row.</param>
        /// <param name="maximumRows">The maximum rows.</param>
        /// <returns></returns>
        public List<JobSeekerActivityActionViewDto> GetJobSeekerActivity(JobSeekerCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
        {
            var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

            if (criteria.IsNull())
            {
                _activityCount = 0;
                return null;
            }

            criteria.PageSize = maximumRows;
            criteria.PageIndex = pageIndex;

            var activites = ServiceClientLocator.CandidateClient(App).GetJobSeekerActivities(criteria);

            _activityCount = activites.TotalCount;
            return activites;
        }

        /// <summary>
        /// Gets the job seeker activity count.
        /// </summary>
        /// <returns></returns>
        public int GetJobSeekerActivityCount()
        {
            return _activityCount;
        }

        /// <summary>
        /// Gets the job seeker activity count.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        public int GetJobSeekerActivityCount(JobSeekerCriteria criteria)
        {
            return _activityCount;
        }

        #endregion

        #region CascadingDropDown Update Job Seeker

        /// <summary>
        /// Handles the UpdateJobSeeker event of the SaveSelectedActivity control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.ActivityAssignment.ActivitySelectedArgs" /> instance containing the event data.</param>
        protected void ActivityAssignment_ActivitySelected(object sender, ActivityAssignment.ActivitySelectedArgs e)
        {
            if (JobSeekerCriteria.JobSeekerId != null && e.SelectedActivityId.IsNotNull())
            {
                var activityId = e.SelectedActivityId;

                bool activityStatus = ServiceClientLocator.CandidateClient(App).AssignActivity(activityId, GetJobSeekerDetails(), e.ActionedDate);
                BindJobSeekerActivityList();
                if (activityStatus)
                    Confirmation.Show(CodeLocalise("AssignActivityConfirmation.Title", "Activity service assigned to candidate"),
                                                        CodeLocalise("AssignActivityConfirmation.Body", "The activity service was assigned to the candidate."),
                                                        CodeLocalise("CloseModal.Text", "OK"));
                else
                    Confirmation.Show("Validation Error", "Staff does not have permission to perform this action", CodeLocalise("CloseModal.Text", "OK"));

            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Gets the selected job seeker details.
        /// </summary>
        /// <returns></returns>
        private JobSeekerView GetJobSeekerDetails()
        {
            return new JobSeekerView
            {
                PersonId = JobSeekerProfileModel.JobSeekerProfileView.Id.GetValueOrDefault(0),
                UserName = JobSeekerProfileModel.JobSeekerProfileView.EmailAddress,
                ClientId = JobSeekerProfileModel.JobSeekerProfileView.ClientId,
                FirstName = JobSeekerProfileModel.JobSeekerProfileView.FirstName,
                LastName = JobSeekerProfileModel.JobSeekerProfileView.LastName,
                Ssn = JobSeekerProfileModel.JobSeekerProfileView.SocialSecurityNumber,
                PhoneNumber = JobSeekerProfileModel.JobSeekerProfileView.HomePhone,
                EmailAddress = JobSeekerProfileModel.JobSeekerProfileView.EmailAddress
            };
        }

        #endregion

        #region Events

        public event JobSeekerActivityListEmailCandidateClickHandler JobSeekerActivityListEmailCandidateClick;


        /// <summary>
        /// Raises the <see cref="E:JobSeekerActivityListEmailCandidateClick"/> event.
        /// </summary>
        /// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.JobSeekerActivityList.JobSeekerActivityListEmailCandidateClickEventArgs"/> instance containing the event data.</param>
        protected virtual void OnJobSeekerActivityListEmailCandidateClick(JobSeekerActivityListEmailCandidateClickEventArgs e)
        {
            if (JobSeekerActivityListEmailCandidateClick != null)
                JobSeekerActivityListEmailCandidateClick(this, e);
        }

        protected void BacdateActivity_ActivityBackdated(object sender, BackdateActivityModal.ActivityBackdatedEventArgs e)
        {
            if (e.isBackdated)
                BindJobSeekerActivityList();
        }

        protected virtual void DeleteActivity_ActivityDeleted(object sender, DeleteModal.DeleteActivityEventArgs e)
        {
            if (e.isDeleted)
                BindJobSeekerActivityList();
        }

        #endregion

        #region Delegates

        public delegate void JobSeekerActivityListEmailCandidateClickHandler(object o, JobSeekerActivityListEmailCandidateClickEventArgs e);

        #endregion

        #region EventArgs

        public class JobSeekerActivityListEmailCandidateClickEventArgs : EventArgs
        {
            public readonly long CandidateId;

            public JobSeekerActivityListEmailCandidateClickEventArgs(long candidateId)
            {
                CandidateId = candidateId;
            }
        }

        #endregion

    }
}
