﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Common;
using Focus.Core;
#endregion

namespace Focus.Web.WebAssist.Controls
{
    public partial class DeleteModal : UserControlBase
    {
        private static long ActivityId;

        private static string ActivityTypeId;

        private static long DLUserId;
        private static string DLActionType;
        private static DateTime DLActionedOn;

        
        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            bool deleteFlag = false;
            //JS validation
            if (DLActionType.Equals(ActionTypes.AssignActivityToCandidate.ToString()))
            {
                var currentUserRoles = ServiceClientLocator.AccountClient(App).GetUsersRoles(App.User.UserId);
                var dbStaffActivityUpdateSettings = ServiceClientLocator.StaffClient(App).GetBackdateSettings(App.User.PersonId ?? 0);
                deleteFlag = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.DeleteAnyEntryJSActivityLogMaxDays)) != null ? true : false;
                if (deleteFlag)
                    deleteFlag = dbStaffActivityUpdateSettings.DeleteAnyEntryJSActivityLogMaxDays != null ? (DLActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.DeleteAnyEntryJSActivityLogMaxDays))) : false;
                if (!deleteFlag && (DLUserId == App.User.UserId))
                {
                    deleteFlag = currentUserRoles.Where(x => x.Key.Contains(Constants.RoleKeys.DeleteMyEntriesJSActivityLogMaxDays)) != null ? true : false;
                    if (deleteFlag)
                        deleteFlag = dbStaffActivityUpdateSettings.DeleteMyEntriesJSActivityLogMaxDays != null ? (DLActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.DeleteMyEntriesJSActivityLogMaxDays))) : false;
                }
            }

            //HM validation
            if (!deleteFlag)
                if (DLActionType.Equals(ActionTypes.AssignActivityToEmployee.ToString()))
                {
                    var currentUserRoles = ServiceClientLocator.AccountClient(App).GetUsersRoles(App.User.UserId);
                    var dbStaffActivityUpdateSettings = ServiceClientLocator.StaffClient(App).GetBackdateSettings(App.User.PersonId ?? 0);
                    deleteFlag = currentUserRoles.Where(x => x.Key.Equals(Constants.RoleKeys.DeleteAnyEntryHMActivityLogMaxDays)) != null ? true : false;
                    if (deleteFlag)
                        deleteFlag = dbStaffActivityUpdateSettings.DeleteAnyEntryHMActivityLogMaxDays != null ? (DLActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.DeleteAnyEntryHMActivityLogMaxDays))) : false;
                    if (!deleteFlag && (DLUserId == App.User.UserId))
                    {
                        deleteFlag = currentUserRoles.Where(x => x.Key.Contains(Constants.RoleKeys.DeleteMyEntriesHMActivityLogMaxDays)) != null ? true : false;
                        if (deleteFlag)
                            deleteFlag = dbStaffActivityUpdateSettings.DeleteMyEntriesHMActivityLogMaxDays != null ? (DLActionedOn > DateTime.Now.AddDays(-Convert.ToDouble(dbStaffActivityUpdateSettings.DeleteMyEntriesHMActivityLogMaxDays))) : false;
                    }
                }

            if (deleteFlag)
            {
                bool isDeleted = ServiceClientLocator.CoreClient(App).DeleteActionEvent(ActivityId, ActivityTypeId);
                if (isDeleted)
                    TriggerActivityDeleted(new DeleteActivityEventArgs(isDeleted));
            }
            //redirect to the Activity log page to avoid "double submit problem"
            Response.Redirect(Request.RawUrl);



        }

        protected void DeleteCancelButton_Click(object sender, EventArgs e)
        {
            DeleteModalPopup.Hide();
        }

        public void show(long activityId, string activityTypeId, long lUserId, string lActionType, DateTime lActionedOn)
        {
            ActivityId = activityId;
            ActivityTypeId = activityTypeId;
            DLUserId = lUserId;
            DLActionType = lActionType;
            DLActionedOn = lActionedOn;
            DeleteModalPopup.Show();
        }

        #region Activity Delete Event Handler
        public event DeleteActivityEventHandler ActivityDeleted;
        public delegate void DeleteActivityEventHandler(object sender, DeleteActivityEventArgs args);

        protected virtual void TriggerActivityDeleted(DeleteActivityEventArgs args)
        {
            if (ActivityDeleted != null)
                ActivityDeleted(this, args);
        }

        public class DeleteActivityEventArgs : EventArgs
        {
            public readonly bool isDeleted;

            public DeleteActivityEventArgs(bool deleted)
            {
                isDeleted = deleted;
            }
        }
        #endregion
    }
}
