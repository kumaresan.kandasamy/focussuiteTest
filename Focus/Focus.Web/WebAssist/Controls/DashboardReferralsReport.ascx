﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardReferralsReport.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.DashboardReferralsReport" %>

<table role="presentation">
	<tr>
		<td><h1><%= HtmlLocalise("ApprovalQueueStatus.Header", "Approval queue status")%></h1></td>
	</tr>
	<tr>
		<td><asp:Literal id="JobPostingsHeaderLiteral" runat="server" /></td>
	</tr>
	<tr>
		<td><strong><asp:Literal ID="NewPostingsCountLiteral" runat="server" /></strong>&nbsp;<%= HtmlLocalise("NewPostings.Label", "new posting(s) since last sign in")%>&nbsp;&nbsp;|&nbsp;&nbsp;  
		<strong><asp:Literal ID="FlaggedPostingsCountLiteral" runat="server" /></strong>&nbsp;<%= HtmlLocalise("FlaggedPostings.Label", "ﬂagged posting(s) waiting for review")%><br /><br /></td>
	</tr>
</table>
<table role="presentation">
	<tr>
		<td><asp:Literal id="NewEmployerAccountsHeaderLiteral" runat="server" /></td>
	</tr>
	<tr>
		<td><strong><asp:Literal ID="NewEmployerAccountsCountLiteral" runat="server" /></strong>&nbsp;<%= HtmlLocalise("NewEmployerAccounts.Label", "new account(s) requests since last sign in")%>&nbsp;&nbsp;|&nbsp;&nbsp;  
		<strong><asp:Literal ID="OutstandingEmployerAccountsCountLiteral" runat="server" /></strong>&nbsp;<%= HtmlLocalise("OutstandingEmployerAccounts.Label", "account(s) request waiting more than 1 business day")%><br /><br /></td>
	</tr>
</table>
<%if (JobSeekerReferralsEnabled())
	{%>
		<table role="presentation">
<% }
	else
  { %>
		<table style="display: none" role="presentation">
<% } %>
	<tr>
		<td><asp:Literal id="ReferralRequestsHeaderLiteral" runat="server" /></td>
	</tr>
	<tr>
		<td><strong><asp:Literal ID="NewReferralRequestsCountLiteral" runat="server" /></strong>&nbsp;<%= HtmlLocalise("NewReferralRequests.Label", "new request(s) since last sign in")%>&nbsp;&nbsp;|&nbsp;&nbsp;  
		<strong><asp:Literal ID="OutstandingReferralRequestsCountLiteral" runat="server" /></strong>&nbsp;<%= HtmlLocalise("OutstandingReferralRequests.Label", "request(s) waiting more than 1 business day")%><br /><br /></td>
	</tr>
</table>