﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Framework.Core;

using Focus.Core;
using Focus.Web.Code;
using Focus.Common;

#endregion


namespace Focus.Web.WebAssist.Controls
{
	public partial class EditPostingReferralConfirmationModal : UserControlBase
	{
		private long JobId
		{
			get { return GetViewStateValue<long>("EditPostingReferralConfirmationModal:JobId"); }
			set { SetViewStateValue("EditPostingReferralConfirmationModal:JobId", value); }
		}

		private string NewPosting
		{
			get { return GetViewStateValue<string>("EditPostingReferralConfirmationModal:NewPosting"); }
			set { SetViewStateValue("EditPostingReferralConfirmationModal:NewPosting", value); }
		}

		private long EmployeeId
		{
			get { return GetViewStateValue<long>("DenyPostingReferralModal:EmployeeId"); }
			set { SetViewStateValue("DenyPostingReferralModal:EmployeeId", value); }
		}

		private EmailTemplateView EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>("EditPostingReferralConfirmationModal:EmailTemplate"); }
			set { SetViewStateValue("EditPostingReferralConfirmationModal:EmailTemplate", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Shows the specified job id.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="newPosting">The new posting.</param>
		public void Show(long jobId, string newPosting)
		{
			JobId = jobId;
			NewPosting = newPosting;

			GetEmailTemplate();
			EmailBodyLiteral.Text = EmailTemplate.Body.Replace(Environment.NewLine, "<br />");

			ModalPopup.Show();
		}

		/// <summary>
		/// Handles the Clicked event of the EditAndSendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EditAndSendMessageButton_Clicked(object sender, EventArgs e)
		{
			ServiceClientLocator.JobClient(App).EditPostingReferral(JobId, NewPosting);

			// TODO: EditPostingReferralConfirmationModal.ascx~EditAndSendMessageButton_Clicked - Batch job to cancel posting after 30 days if it has not been accepted by employer

			ServiceClientLocator.EmployeeClient(App).EmailEmployee(EmployeeId, EmailTemplate.Subject, EmailTemplate.Body, BCCMeCheckBox.Checked, ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.EditPostingReferralConfirmation));

			ModalPopup.Hide();

			Confirmation.Show(CodeLocalise("ReferralEditedConfirmation.Title", "Posting edited"),
															CodeLocalise("ReferralEditedConfirmation.Body", "The posting has been updated and a copy emailed to the #BUSINESS#:LOWER for acceptance."),
															CodeLocalise("CloseModal.Text", "OK"),
															closeLink: UrlBuilder.PostingReferrals());
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EditAndSendMessageButton.Text = CodeLocalise("EditAndSendMessageButton.Text.Text", "Email #BUSINESS#:LOWER");
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");
		}

		/// <summary>
		/// Gets the email template.
		/// </summary>
		private void GetEmailTemplate()
		{
			var jobPostingReferral = ServiceClientLocator.JobClient(App).GetJobPostingReferral(JobId);
			if (jobPostingReferral.EmployeeId.HasValue) EmployeeId = jobPostingReferral.EmployeeId.Value;
			var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";

		  var jobLinkUrl = string.Concat(App.Settings.TalentApplicationPath, UrlBuilder.JobView(JobId, false));
		  var jobLinkHtml = string.Format("<a href='{0}'>{0}</a>", jobLinkUrl);

			var templateValues = new EmailTemplateData
																{
																	RecipientName = String.Format("{0} {1}", jobPostingReferral.EmployeeFirstName, jobPostingReferral.EmployeeLastName),
																	JobTitle = jobPostingReferral.JobTitle,
																	JobPosting = NewPosting,
																	SenderPhoneNumber = phoneNumber,
																	SenderEmailAddress = App.User.EmailAddress,
																	SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName),
                                  JobLinkUrls = new List<string> { jobLinkHtml }
																};

			EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.EditPostingReferralConfirmation, templateValues);
		}
	}
}