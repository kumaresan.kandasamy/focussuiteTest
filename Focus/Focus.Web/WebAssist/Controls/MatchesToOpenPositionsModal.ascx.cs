﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class MatchesToOpenPositionsModal : UserControlBase
	{
		#region Properties

		/// <summary>
		/// Gets or sets the selected job details.
		/// </summary>
		private MatchedJob JobDetails 
		{
			get { return GetViewStateValue<MatchedJob>("MatchesToOpenPositions:JobDetails"); }
			set { SetViewStateValue("MatchesToOpenPositions:JobDetails", value); } 
		}

		/// <summary>
		/// Gets or sets the job.
		/// </summary>
		private JobDto Job
		{
			get { return GetViewStateValue<JobDto>("JobDetails:Job"); }
			set { SetViewStateValue("JobDetails:Job", value); }
		}

		/// <summary>
		/// Gets or sets the candidate details.
		/// </summary>
		private Details CandidateDetails
		{
			get { return GetViewStateValue<Details>("MatchesToOpenPositions:Details"); }
			set { SetViewStateValue("MatchesToOpenPositions:Details", value); }
		}

		public delegate void CompletedHandler(object sender, EventArgs eventArgs);
		public event CompletedHandler Completed;

		public virtual void OnCompleted(EventArgs eventargs)
		{
			if (Completed != null) Completed(this, eventargs);
		}
		
		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Shows the specified details.
		/// </summary>
		/// <param name="details">The details.</param>
		public void Show(Details details)
		{
			#region Job Seeker

			CandidateDetails = details;

			CandidateNameLiteral.Text = CodeLocalise("CandidateName.Text", "<strong>" + details.CandidateName + "</strong>");
			CandidateResumeDescriptionLiteral.Text = CodeLocalise("CandidateResumeDescription.Text", GetCandidateResume(details.CandidateId));
			
			#endregion

			#region Open Positions

			BindOpenPositionDropDown();
			
			#endregion

			OpenPositionsModalPopup.Show();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ContactEmployerButton.Text = CodeLocalise("ContactEmployerButton.Text", "Contact #BUSINESS#:LOWER");
			CancelButton.Text = CodeLocalise("CancelButton.Text", "Cancel");
		}

		#region Events

		/// <summary>
		/// Handles the SelectedIndexChanged event of the OpenPositionsDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OpenPositionsDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var jobId = Convert.ToInt64(OpenPositionsDropDown.SelectedValue);

			if (jobId > 0)
			{
				SetSelectedJobDetails(jobId);

				JobDescriptionLiteral.Text = Job.PostingHtml;
				BindScore(PositionScoreImage, JobDetails.Score);
				PositionScoreImage.Visible = true;
				DoNotShowCheckBox.Checked = JobDetails.DoNotShow;
			}
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			OpenPositionsModalPopup.Hide();

			if (Completed != null)
				Completed(this, new EventArgs());
		}
		
		/// <summary>
		/// Handles the Clicked event of the ContactEmployerButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ContactEmployerButton_Clicked(object sender, EventArgs e)
		{
			OpenPositionsModalPopup.Hide();

			if (Completed != null)
				Completed(this, new EventArgs());

			if (Job.EmployeeId != null)
        Response.Redirect(UrlBuilder.ContactEmployer(Job.BusinessUnitId.GetValueOrDefault(0), CandidateDetails.CandidateId, CandidateDetails.RecentlyPlacedId));
		}

		/// <summary>
		/// Handles the CheckChanged event of the DoNotShowCheckBox control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DoNotShowCheckBox_CheckChanged(object sender, EventArgs e)
		{
			var jobId = Job.Id.AsLong();
			var postingId = JobDetails.PostingId;
			
			if (DoNotShowCheckBox.Checked)
			{
				ServiceClientLocator.CandidateClient(App).IgnorePersonPostingMatch(postingId, CandidateDetails.CandidateId);
				CandidateDetails.MatchedJobs.Single(job => job.JobId == jobId).DoNotShow = true;
			}
				
			else
			{
				ServiceClientLocator.CandidateClient(App).AllowPersonPostingMatch(postingId, CandidateDetails.CandidateId);
				CandidateDetails.MatchedJobs.Single(job => job.JobId == jobId).DoNotShow = false;
			}
				

			OpenPositionsModalPopup.Show();
		}

		#endregion

		#region Setting/Getting Jobs

		/// <summary>
		/// Sets the selected job details.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		private void SetSelectedJobDetails(long jobId)
		{
			JobDetails = CandidateDetails.MatchedJobs.Where(x => x.JobId == jobId).FirstOrDefault();
			GetSelectedJob(jobId);
		}

		/// <summary>
		/// Gets all the open positions.
		/// </summary>
		/// <returns></returns>
		private List<MatchedJob> GetOpenPositions()
		{
			var jobsList = ServiceClientLocator.CandidateClient(App).GetOpenPositionJobs(CandidateDetails.CandidateId, CandidateDetails.EmployerId);

			CandidateDetails.MatchedJobs = new List<MatchedJob>();
			
			if (jobsList.IsNotNullOrEmpty())
			{
				foreach (var openPositionMatch in jobsList)
				{
					CandidateDetails.MatchedJobs.Add(new MatchedJob
					                                 	{
					                                 		JobId = openPositionMatch.JobId,
					                                 		JobTitle = openPositionMatch.JobTitle,
					                                 		Score = openPositionMatch.JobScore,
					                                 		EmployerName = openPositionMatch.EmployerName,
					                                 		DoNotShow = false,
																							PostingId = openPositionMatch.PostingId
					                                 	});
				}
			}

			return CandidateDetails.MatchedJobs;
		}
		
		/// <summary>
		/// Gets the selected job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		private void GetSelectedJob(long jobId)
		{
			Job = ServiceClientLocator.JobClient(App).GetJob(jobId);
		}

		#endregion

		/// <summary>
		/// Binds the open position drop down.
		/// </summary>
		private void BindOpenPositionDropDown()
		{
			OpenPositionsDropDown.Items.Clear();
			
			var openPostions = GetOpenPositions();
			
			OpenPositionsDropDown.DataSource = openPostions;

			OpenPositionsDropDown.DataValueField = "JobId";
			OpenPositionsDropDown.DataTextField = "JobTitle";
			OpenPositionsDropDown.DataBind();

			if (openPostions.IsNotNullOrEmpty())
			{
				OpenPositionsDropDown.SelectedIndex = 0;
				SetSelectedJobDetails(openPostions[0].JobId);
			}

			OpenPositionsDropDown_SelectedIndexChanged(OpenPositionsDropDown, System.EventArgs.Empty);
		}

		/// <summary>
		/// Gets the candidate resume.
		/// </summary>
		/// <param name="candidateId">The candidate id.</param>
		/// <returns></returns>
		private string GetCandidateResume(long candidateId)
		{
			return ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(candidateId);
		}

		
		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private static void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = OldApp_RefactorIfFound.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString();

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

		#region Helper Class

		[Serializable]
		public class Details
		{
			public long CandidateId { get; set; }
			public string CandidateName { get; set; }
			public List<MatchedJob> MatchedJobs { get; set; }
			public long EmployerId { get; set; }
      public long RecentlyPlacedId { get; set; }
		}

		[Serializable]
		public class MatchedJob
		{
			public long JobId { get; set; }
			public string JobTitle { get; set; }
			public int Score { get; set; }
			public string EmployerName { get; set; }
			public bool DoNotShow { get; set; }
			public long PostingId { get; set; }
		}

		#endregion
		
	}
}