﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Views;
using Focus.Web.Code;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class CandidateSearchResultList : UserControlBase
	{
		#region Private Members

		private List<ProgramAreaDegreeEducationLevelViewDto> _programAreaDegrees;
		private int _jobSeekersCount;
	  private bool _reApplyFilter;

		#endregion

		#region Private Properties
		
		private JobSeekerCriteria JobSeekerCriteria
		{
			get { return GetViewStateValue<JobSeekerCriteria>("JobSeekers:JobSeekerCriteria"); }
			set { SetViewStateValue("JobSeekers:JobSeekerCriteria", value); }
		}

		private ActionTypes? SelectedAction
		{
			get { return GetViewStateValue<ActionTypes>("JobSeekers:SelectedAction"); }
			set { SetViewStateValue("JobSeekers:SelectedAction", value); }
		}


		private List<JobSeekerView> SelectedJobSeekers
		{
			get { return GetViewStateValue<List<JobSeekerView>>("JobSeekers:SelectedJobSeekers"); }
			set { SetViewStateValue("JobSeekers:SelectedJobSeekers", value); }
		}

		#endregion

    #region Protected Members

	  protected string BlockedPopupMessage = "";

    #endregion

    #region Public Properties

    public JobSeekerCriteria CandidateCriteria { get; set; }
		public RegisterJobSeeker RegisterJobseeker { get; set; }

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
			  LocaliseUI();
        BindActionDropDown();
        if (!_reApplyFilter)  RunSearch();
			}

      BlockedPopupMessage = CodeLocalise("AccessCandidatePopupBlocked.Error.Text",
                                         "Unable to Access #CANDIDATETYPE# account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.");
		}

    #region LocaliseUI
    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      ActionLabel.Text = CodeLocalise("ActionLabel.Text", "Action");
      ActionGoButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
      ActionDropDownRequired.ErrorMessage = CodeLocalise("ActionDropDownRequired.ErrorMessage", "Action required");
      SubActionButton.Text = CodeLocalise("SubActionButton.Text", "Assign");
    }

    #endregion

    #region Get Current Application Settings

    /// <summary>
		/// Determines whether [is assist jobseekers administrator].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is assist jobseekers administrator]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistJobseekersAdministrator()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAdministrator);
		}
		
		#endregion

		#region ObjectDataSource Methods

		/// <summary>
		/// Gets the employees.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<StudentAlumniIssueViewDto> GetJobSeekers(JobSeekerCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			if (maximumRows == -1)
				maximumRows = 10;
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_jobSeekersCount = 0; return null;
			}

			if (maximumRows == -1)
				maximumRows = 10;
			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;
		

			var jobSeekers = ServiceClientLocator.CandidateClient(App).GetStudentAlumniIssuesPagedList(criteria);
			_jobSeekersCount = jobSeekers.TotalCount;

			return jobSeekers;
		}

		/// <summary>
		/// Gets the job seeker activity count.
		/// </summary>
		/// <returns></returns>
		public int GetJobSeekersCount()
		{
			return _jobSeekersCount;
		}

		/// <summary>
		/// Gets the job seekers count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetJobSeekersCount(JobSeekerCriteria criteria)
		{
			return _jobSeekersCount;
		}

		/// <summary>
		/// Handles the Selecting event of the SearchResultDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["criteria"] = JobSeekerCriteria;
      
			if (JobSeekerCriteria != null)
				App.SetSessionValue("Career:SearchCriteria", JobSeekerCriteria);
		}

		#endregion

		#region Job seeker list events

		/// <summary>
		/// Handles the LayoutCreated event of the JobSeekersListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void CandidateListView_LayoutCreated(object sender, EventArgs e)
		{
			#region Set labels

			CandidateListView.FindControl("SelectorAllCheckBoxes").Visible = IsAssistJobseekersAdministrator(); 
			((Literal)CandidateListView.FindControl("NameHeaderLiteral")).Text = CodeLocalise("CandidateName.ColumnTitle", "Name");
      ((Literal)CandidateListView.FindControl("StudyProgramHeaderLiteral")).Text = CodeLocalise("StudyProgram.ColumnTitle", "Program Of Study");
			((Literal)CandidateListView.FindControl("LastActivityDateHeaderLiteral")).Text = CodeLocalise("CandidateAtivityDate.LastActivity", "Last activity date");
			((Literal)CandidateListView.FindControl("IssuesHeaderLiteral")).Text = CodeLocalise("CandidateIssues.ColumnTitle", "Issues");

      if (App.Settings.ShowStudentExternalId)
        ((Literal)CandidateListView.FindControl("StudentIdHeaderLiteral")).Text = CodeLocalise("CandidateId.ColumnTitle", "Student ID");
      else
        CandidateListView.FindControl("StudentIdHeaderPlaceHolder").Visible = false;

			#endregion
		}

		/// <summary>
		/// Handles the Sorting event of the JobOrdersList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void CandidateListView_Sorting(object sender, ListViewSortEventArgs e)
		{
			JobSeekerCriteria.OrderBy = e.SortExpression;
			FormatJobSeekerListSortingImages();
		}

		/// <summary>
		/// Formats the Job order list sorting images.
		/// </summary>
		private void FormatJobSeekerListSortingImages()
		{
			#region Format images

			// Reset image URLs
			((ImageButton) CandidateListView.FindControl("NameSortAscButton")).ImageUrl = 
				((ImageButton)CandidateListView.FindControl("StudyProgramNameSortAscButton")).ImageUrl =
				((ImageButton)CandidateListView.FindControl("LastActivityDateSortAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)CandidateListView.FindControl("NameSortDescButton")).ImageUrl =
				((ImageButton)CandidateListView.FindControl("StudyProgramNameSortDescButton")).ImageUrl =
				((ImageButton)CandidateListView.FindControl("LastActivityDateSortDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			// Reenable the buttons
			((ImageButton)CandidateListView.FindControl("NameSortAscButton")).Enabled =
				((ImageButton)CandidateListView.FindControl("StudyProgramNameSortAscButton")).Enabled =
				((ImageButton)CandidateListView.FindControl("LastActivityDateSortAscButton")).Enabled =
				((ImageButton)CandidateListView.FindControl("NameSortDescButton")).Enabled =
				((ImageButton)CandidateListView.FindControl("StudyProgramNameSortDescButton")).Enabled =
				((ImageButton)CandidateListView.FindControl("LastActivityDateSortDescButton")).Enabled = true;

			switch (JobSeekerCriteria.OrderBy ?? Constants.SortOrders.JobSeekerNameAsc)
			{
				case Constants.SortOrders.JobSeekerNameAsc:
					((ImageButton)CandidateListView.FindControl("NameSortAscButton")).ImageUrl = UrlBuilder.ActivitySortAscImage();
					((ImageButton)CandidateListView.FindControl("NameSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.JobSeekerNameDesc:
					((ImageButton)CandidateListView.FindControl("NameSortDescButton")).ImageUrl = UrlBuilder.ActivitySortDescImage();
					((ImageButton)CandidateListView.FindControl("NameSortDescButton")).Enabled = false;
					break;
        case Constants.SortOrders.StudyProgramAsc:
          ((ImageButton)CandidateListView.FindControl("StudyProgramNameSortAscButton")).ImageUrl = UrlBuilder.ActivitySortAscImage();
          ((ImageButton)CandidateListView.FindControl("StudyProgramNameSortAscButton")).Enabled = false;
          break;
        case Constants.SortOrders.StudyProgramDesc:
					((ImageButton)CandidateListView.FindControl("StudyProgramNameSortDescButton")).ImageUrl = UrlBuilder.ActivitySortDescImage();
          ((ImageButton)CandidateListView.FindControl("StudyProgramNameSortDescButton")).Enabled = false;
          break;
				case Constants.SortOrders.LastActivityAsc:
					((ImageButton)CandidateListView.FindControl("LastActivityDateSortAscButton")).ImageUrl = UrlBuilder.ActivitySortAscImage();
					((ImageButton)CandidateListView.FindControl("LastActivityDateSortAscButton")).Enabled = false;
					break;
				case Constants.SortOrders.LastActivityDesc:
					((ImageButton)CandidateListView.FindControl("LastActivityDateSortDescButton")).ImageUrl = UrlBuilder.ActivitySortDescImage();
					((ImageButton)CandidateListView.FindControl("LastActivityDateSortDescButton")).Enabled = false;
					break;
				default:
					((ImageButton)CandidateListView.FindControl("NameSortAscButton")).ImageUrl = UrlBuilder.ActivitySortAscImage();
					((ImageButton)CandidateListView.FindControl("NameSortAscButton")).Enabled = false;
					break;
			}

			#endregion
		}

		/// <summary>
		/// Handles the ItemDataBound event of the JobSeekerListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void CandidateListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var candidateIssues = (StudentAlumniIssueViewDto)e.Item.DataItem;
			var checkBox = (CheckBox)e.Item.FindControl("SelectorCheckBox");

			checkBox.Visible = IsAssistJobseekersAdministrator();

      checkBox.Attributes.Add("value", candidateIssues.Id.ToString());
			checkBox.InputAttributes.Add("value", candidateIssues.Id.ToString());

			#region Name

			var nameLink = (HyperLink)e.Item.FindControl("CandidateNameLink");
      nameLink.Text = string.Concat(candidateIssues.LastName, ", ", candidateIssues.FirstName, " ", candidateIssues.MiddleInitial).Trim();

      nameLink.NavigateUrl = UrlBuilder.AssistJobSeekerProfile((long)candidateIssues.Id) + "?filtered=true";

			#endregion

      #region Blocked Image

      var blockedImage = (Image)e.Item.FindControl("BlockedImage");
      BindBlockedImage(blockedImage, candidateIssues.Blocked);

      #endregion

      #region Student External Id

		  if (App.Settings.ShowStudentExternalId)
		    ((Literal) e.Item.FindControl("StudentIdLiteral")).Text = candidateIssues.ExternalId;
		  else
        e.Item.FindControl("StudentIdPlaceHolder").Visible = false;

      #endregion

      # region Program of Study

      var studyProgramLiteral = (Literal)e.Item.FindControl("StudyProgramLiteral");

      var areaId = candidateIssues.ProgramAreaId;
      var degreeId = candidateIssues.DegreeId;

			if (areaId.HasValue || degreeId.HasValue)
				studyProgramLiteral.Text = GetUserProgramOfStudy(areaId, degreeId);

			#endregion

			#region Issues

			var issues = BuildIssueList(candidateIssues);
			var issuesLiteral = (Literal)e.Item.FindControl("IssuesLiteral");
			var showAdditionalIssuesLink = (HyperLink)e.Item.FindControl("ShowAdditionalIssuesLink");
			if (issues.Count < 4)
			{
				issuesLiteral.Text = string.Join(", ", issues);
			}
			else
			{
				issuesLiteral.Text = string.Join(", ", issues.Take(2)) + @", ";
				var additionalIssuesLiteral = (Literal)e.Item.FindControl("AdditionalIssuesLiteral");
				additionalIssuesLiteral.Text = string.Join(", ", issues.Skip(issues.Count - 2));

				showAdditionalIssuesLink.Visible = true;
				showAdditionalIssuesLink.Text = CodeLocalise("AdditionalIssuesLink.Text", "+ {0} more", (issues.Count - 2));
				showAdditionalIssuesLink.Attributes.Add("onclick",
																								string.Format("ShowAdditionalIssues({0});",
																															candidateIssues.Id.ToString()));
				showAdditionalIssuesLink.Attributes.Add("onkeypress",
																				string.Format("ShowAdditionalIssues({0});",
																											candidateIssues.Id.ToString()));

			}

			#endregion
		}

		#endregion

		#region Modal Events

		/// <summary>
		/// Handles the Resolved event of the Issues control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Issues_Updated(object sender, EventArgs e)
		{
			RunSearch(false);
		}

		#endregion

    #region Binding Action Drop Downs

    /// <summary>
    /// Binds the blocked image.
    /// </summary>
    /// <param name="blockedImage">The veteran image.</param>
    /// <param name="isBlocked">Whether the person is blocked.</param>
    private void BindBlockedImage(Image blockedImage, bool isBlocked)
    {
      if (isBlocked)
      {
        blockedImage.ToolTip = blockedImage.AlternateText = CodeLocalise("BlockedImage.ToolTip", "Blocked");
        blockedImage.ImageUrl = UrlBuilder.BlockedImage();
        blockedImage.Visible = true;
      }
      else
      {
        blockedImage.Visible = false;
      }
    }

    /// <summary>
    /// Binds the action drop down.
    /// </summary>
    private void BindActionDropDown()
    {
      ActionDropDown.Items.Clear();

      ActionDropDown.Items.AddLocalisedTopDefault("ActionDropDown.TopDefault", "- select action -");
      ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AccessCandidateAccount, "Access #CANDIDATETYPE#:LOWER account"), ActionTypes.AccessCandidateAccount.ToString()));
      ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.RequestFollowUp, "Add follow up"), ActionTypes.RequestFollowUp.ToString()));
      ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AddNote, "Add note or reminder"), ActionTypes.AddNote.ToString()));
      ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AddCandidateToList, "Add #CANDIDATETYPE#:LOWER to list"), ActionTypes.AddCandidateToList.ToString()));

      if (App.Settings.OfficesEnabled)
      {
        var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(Convert.ToInt64(App.User.UserId));
        if (userDetails.PersonDetails.Manager.IsNotNull() && Convert.ToBoolean(userDetails.PersonDetails.Manager))
          ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AssignCandidateToStaff, "Assign #CANDIDATETYPE#:LOWER to staff"), ActionTypes.AssignCandidateToStaff.ToString()));
      }

      if (App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAccountBlocker))
        ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.BlockUnblockJobSeeker, "Block/Unblock #CANDIDATETYPE#:LOWER"), ActionTypes.BlockUnblockJobSeeker.ToString()));

      ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.EmailCandidate, "Email #CANDIDATETYPE#:LOWER(s)"), ActionTypes.EmailCandidate.ToString()));
      ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.MarkCandidateIssuesResolved, "Mark as resolved"), ActionTypes.MarkCandidateIssuesResolved.ToString()));
			ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.AutoResolvedIssue, "Mark as auto-resolved"), ActionTypes.AutoResolvedIssue.ToString()));
			//TODO: Jane - 23/05/2013 - For now this edit option has been removed until the edit screen can be revisited
			//if (App.Settings.Theme == FocusThemes.Education)
			//  ActionDropDown.Items.Add(new ListItem(CodeLocalise(ActionTypes.EditStudentAlumniAccount, "Edit student/alumni account"), ActionTypes.EditStudentAlumniAccount.ToString()));
    }

    /// <summary>
    /// Binds the sub action drop down to staff.
    /// </summary>
    private void BindSubActionDropDownToStaff()
    {
      SubActionDropDown.Items.Clear();

      SubActionDropDown.DataSource = ServiceClientLocator.AccountClient(App).GetAssistUsers();
      SubActionDropDown.DataValueField = "Id";
      SubActionDropDown.DataTextField = "Name";
      SubActionDropDown.DataBind();

      SubActionDropDown.Items.AddLocalisedTopDefault("StaffSubActionDropDown.TopDefault", "- select staff member name -");
    }

    #endregion

    /// <summary>
		/// Runs the search.
		/// </summary>
		public int RunSearch(bool refreshCriteria = true, bool reApplyFilter = false)
    {      
      if (reApplyFilter)
      {
        // Set the Pager control to the previous Listview page and pagesize
        SearchResultListPager.ReturnToPage(Convert.ToInt32(CandidateCriteria.PageIndex), Convert.ToInt32(CandidateCriteria.PageSize));
        _reApplyFilter = true;
      }
      else
      {
        SearchResultListPager.ReturnToFirstPage();
      }			  

      if (JobSeekerCriteria.IsNull() || refreshCriteria)
			  JobSeekerCriteria = CandidateCriteria;

      BindJobSeekersList();
			SelectedAction = null;

      var totalRows = SearchResultListPager.TotalRowCount;
			if (SearchResultListPager.TotalRowCount > 0) 
        FormatJobSeekerListSortingImages();

      return totalRows;
		}
    
		/// <summary>
		/// Builds the issue list.
		/// </summary>
		/// <param name="issues">The issues.</param>
		/// <returns></returns>
		private List<string> BuildIssueList(StudentAlumniIssueViewDto issues)
		{
			var issueList = new List<string>();

			if (issues.NoLoginTriggered.GetValueOrDefault() && App.Settings.NotLoggingInEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotLoggingIn, "#CANDIDATETYPE# not logging in"));
			
			if (issues.NotClickingOnLeadsTriggered.GetValueOrDefault() && App.Settings.NotClickingLeadsEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotClickingLeads, "#CANDIDATETYPE# not clicking on leads"));
			
			if (issues.JobOfferRejectionTriggered.GetValueOrDefault() && App.Settings.RejectingJobOffersEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.RejectingJobOffers, "#CANDIDATETYPE# rejecting job offers"));
			
			if (issues.NotReportingToInterviewTriggered.GetValueOrDefault() && App.Settings.NotReportingForInterviewEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotReportingForInterviews, "#CANDIDATETYPE# not reporting for interviews"));
			
			if (issues.NotRespondingToEmployerInvitesTriggered.GetValueOrDefault() && App.Settings.NotRespondingToEmployerInvitesEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotRespondingToEmployerInvites, "#CANDIDATETYPE# not responding to #BUSINESS#:LOWER invitations"));
			
			if (issues.ShowingLowQualityMatchesTriggered.GetValueOrDefault() && App.Settings.ShowingLowQualityMatchesEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.ShowingLowQualityMatches, "Showing low-quality matches"));
			
			if (issues.PostingLowQualityResumeTriggered.GetValueOrDefault() && App.Settings.PostingLowQualityResumeEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.PoorQualityResume, "Posting poor-quality resume"));
			
			if (issues.PostHireFollowUpTriggered.GetValueOrDefault() && App.Settings.PostHireFollowUpEnabled)
        issueList.Add(CodeLocalise(CandidateIssueType.SuggestingPostHireFollowUp, "Suggesting post-hire followup"));
			
			if (issues.FollowUpRequested.GetValueOrDefault() && App.Settings.FollowUpEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.RequiringFollowUp, "Requiring followup"));
			
			if (issues.NotSearchingJobsTriggered.GetValueOrDefault() && App.Settings.NotSearchingJobsEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.NotSearchingJobs, "#CANDIDATETYPE# not searching jobs"));

      if (issues.InappropriateEmailAddress.GetValueOrDefault())
        issueList.Add(CodeLocalise(CandidateIssueType.InappropriateEmailAddress, "Inappropriate email address"));

			if (issues.GivingNegativeFeedback.GetValueOrDefault() && App.Settings.NegativeFeedbackCheckEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.GivingNegativeFeedback, "#CANDIDATETYPE# giving negative feedback"));

			if (issues.GivingPositiveFeedback.GetValueOrDefault() && App.Settings.PositiveFeedbackCheckEnabled)
				issueList.Add(CodeLocalise(CandidateIssueType.GivingPositiveFeedback, "#CANDIDATETYPE# giving positive feedback"));
			
			return issueList;
		}

		/// <summary>
		/// Binds the job seekers list.
		/// </summary>
		private void BindJobSeekersList()
		{
		  CandidateListView.DataBind();

			// Set visibility of other controls
			SearchResultListPager.Visible = (SearchResultListPager.TotalRowCount > 0);

			if (IsAssistJobseekersAdministrator())
			{
				ActionDropDown.Visible =
					ActionGoButton.Visible = ActionLabel.Visible = (SearchResultListPager.TotalRowCount > 0);
				ActionDropDown.SelectedIndex = 0;
				SubActionDropDown.Visible = SubActionButton.Visible = false;
			}
			else
			{
				ActionDropDown.Visible =
					ActionGoButton.Visible = ActionLabel.Visible = false;
			}
		}
    
		#region Select/Deselect Candidate

    /// <summary>
    /// Gets the selected job seeker id.
    /// </summary>
    /// <returns></returns>
    private List<JobSeekerView> GetSelectedJobSeekers()
    {
      return (from item in CandidateListView.Items
              let checkBox = (CheckBox)item.FindControl("SelectorCheckBox")
              let viewItem = CandidateListView.DataKeys[item.DisplayIndex]
              where checkBox.Checked
              select new JobSeekerView
              {
                PersonId = viewItem["Id"].AsLong(),
                FirstName = viewItem["FirstName"].ToString(),
                LastName = viewItem["LastName"].ToString(),
                MiddleInitial = viewItem["MiddleInitial"] == null ? string.Empty : viewItem["MiddleInitial"].ToString(),
                Blocked = viewItem["Blocked"].AsBoolean()
              }).ToList();
    }
    
		/// <summary>
		/// Deselects the jobseekers.
		/// </summary>
		private void DeselectJobseekers()
		{
			var checkBoxes = from item in CandidateListView.Items
											 let checkBox = (CheckBox)item.FindControl("SelectorCheckBox")
											 where checkBox.Checked
											 select checkBox;

			foreach (var checkBox in checkBoxes)
				checkBox.Checked = false;
		}

		#endregion

    #region Actions

    /// <summary>
    /// Handles the Clicked event of the ActionButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ActionButton_Clicked(object sender, EventArgs e)
    {
      // Hide the sub action controls
      SubActionDropDown.Visible = SubActionButton.Visible = false;

      SelectedAction = ActionDropDown.SelectedValueToEnum<ActionTypes>();

      var jobSeekersSelected = SelectedJobSeekers = GetSelectedJobSeekers();
      if (jobSeekersSelected.Count == 0)
      {
        ShowErrors(ErrorTypes.NoCandidatesSelected);
        return;
      }

      switch (SelectedAction)
      {
        case ActionTypes.AccessCandidateAccount:
          if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;

          var singleSignOnKey = ServiceClientLocator.AuthenticationClient(App).CreateCandidateSingleSignOn(jobSeekersSelected[0].PersonId, null);
          AccessCandidateUrl.Value = string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.SingleSignOn(singleSignOnKey, false));

          DeselectJobseekers();
          break;

        case ActionTypes.EmailCandidate:
          var candidateIds = (from j in jobSeekersSelected select j.PersonId).ToList();
          EmailCandidate.Show(candidateIds);
          break;

        case ActionTypes.FindJobsForSeeker:
          if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
          break;

        case ActionTypes.AddNote:
          if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
          NotesAndReminders.Show(jobSeekersSelected[0].PersonId, EntityTypes.JobSeeker, !IsAssistJobseekersAdministrator());
          break;

        case ActionTypes.AssignCandidateToStaff:
          PrepareAssignJobSeeker();
          DeselectJobseekers();
          break;

        case ActionTypes.AddCandidateToList:
          JobSeekerList.Show(jobSeekersSelected.ToArray());
          DeselectJobseekers();
          break;

        case ActionTypes.UpdateCandidateStatus:
          break;

				case ActionTypes.MarkCandidateIssuesResolved:
          if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
          IssueResolutionModal.Show(jobSeekersSelected[0].PersonId);
          break;
				

        case ActionTypes.RequestFollowUp:
          if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
          RequestFollowUpModal.Show(jobSeekersSelected[0].PersonId, EntityTypes.JobSeeker);
          break;

				case ActionTypes.EditStudentAlumniAccount:
					if (MultipleJobSeekerSelectedTest(jobSeekersSelected)) return;
					if (RegisterJobseeker.IsNotNull())
						RegisterJobseeker.Show(jobSeekersSelected[0].PersonId);
					break;

        case ActionTypes.BlockUnblockJobSeeker:
          var selectedJobSeeker = jobSeekersSelected[0];
          if (selectedJobSeeker.Blocked)
          {
            Confirmation.Show(CodeLocalise("BlockJobSeeker.Title", "Unblock #CANDIDATETYPE#:LOWER account"),
															CodeLocalise("BlockJobSeeker.Body", "By confirming this action, you will enable the selected #CANDIDATETYPE#:LOWER's access to #FOCUSCAREER#"),
                              CodeLocalise("Global.Close", "Close"),
                              "Unblock",
                              okText: CodeLocalise("Global.OK", "OK"),
                              okCommandName: "Unblock",
                              okCommandArgument: selectedJobSeeker.PersonId.ToString(CultureInfo.InvariantCulture));
          }
          else
          {
						Confirmation.SetTextBoxDetails(CodeLocalise("BlockReason.HeaderText", "Reason"),
													5,
													true,
													CodeLocalise("BlockReason.ValidationMessage", "Reason is required"),
													"BlockedJobSeeker");

						Confirmation.Show(CodeLocalise("BlockJobSeeker.Title", "Block #CANDIDATETYPE#:LOWER account"),
															CodeLocalise("BlockJobSeeker.Body", "By confirming this action, you will disable the selected #CANDIDATETYPE#:LOWER's access to #FOCUSCAREER#.<br /><br />Please enter your reason for blocking their account access"),
                              CodeLocalise("Global.Close", "Close"),
                              "Unblock",
                              okText: CodeLocalise("Global.OK", "OK"),
                              okCommandName: "Block",
                              okCommandArgument: selectedJobSeeker.PersonId.ToString(CultureInfo.InvariantCulture));
          }
          break;
      }
    }

    /// <summary>
    /// Handles the Clicked event of the SubActionButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SubActionButton_Clicked(object sender, EventArgs e)
    {
      switch (SelectedAction)
      {
        case ActionTypes.AssignCandidateToStaff:
          {
            var adminId = SubActionDropDown.SelectedValueToLong();

            ServiceClientLocator.CandidateClient(App).AssignJobSeekerToAdmin(adminId, SelectedJobSeekers[0]);

            Confirmation.Show(CodeLocalise("AssignJobSeekerToAdminConfirmation.Title", "#CANDIDATETYPE# assigned"),
                              CodeLocalise("AssignJobSeekerToAdminConfirmation.Body", "The #CANDIDATETYPE# has been assigned to the selected staff member."),
                              CodeLocalise("CloseModal.Text", "OK"));

            break;
          }
        
      }
    }

    /// <summary>
    /// Handles the OnOkCommand event of the Confirmation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void Confirmation_OnOkCommand(object sender, CommandEventArgs eventargs)
    {
      var success = false;

      switch (eventargs.CommandName)
      {
        case "Block":
					success = ServiceClientLocator.AccountClient(App).BlockPerson(eventargs.CommandArgument.AsLong(), false, Confirmation.ConfirmationText);
          Confirmation.Show(CodeLocalise("BlockJobSeeker.Title", "Block #CANDIDATETYPE#:LOWER account"),
                            success
                              ? CodeLocalise("BlockJobSeekerSuccess.Body", "#CANDIDATETYPE# successfully blocked")
                              : CodeLocalise("BlockJobSeekerFailure.Body", "#CANDIDATETYPE# is already blocked"),
                            CodeLocalise("Global.Close", "Close"));
          break;

        case "Unblock":
          success = ServiceClientLocator.AccountClient(App).UnblockPerson(eventargs.CommandArgument.AsLong());
          Confirmation.Show(CodeLocalise("UnblockJobSeeker.Title", "Unblock #CANDIDATETYPE#:LOWER account"),
                            success
                              ? CodeLocalise("UnblockJobSeekerSuccess.Body", "#CANDIDATETYPE# successfully unblocked")
                              : CodeLocalise("UnblockJobSeekerFailure.Body", "#CANDIDATETYPE# is already unblocked"),
                            CodeLocalise("Global.Close", "Close"));
          break;
      }

      if (success)
        BindJobSeekersList();

      DeselectJobseekers();
    }

    /// <summary>
    /// Handles the OnCloseCommand event of the Confirmation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void Confirmation_OnCloseCommand(object sender, CommandEventArgs eventargs)
    {
      DeselectJobseekers();
    }

    /// <summary>
    /// Multiples the job seeker selected test.
    /// </summary>
    /// <param name="selectedCandidates">The selected candidates.</param>
    /// <returns></returns>
    private bool MultipleJobSeekerSelectedTest(List<JobSeekerView> selectedCandidates)
    {
      if (selectedCandidates.Count > 1) ShowErrors(ErrorTypes.MultipleCandidatesSelected);
      return selectedCandidates.Count > 1;
    }
    
    /// <summary>
    /// Prepares the assign employer to staff action.
    /// </summary>
    private void PrepareAssignJobSeeker()
    {
      BindSubActionDropDownToStaff();
      SubActionDropDownRequired.ErrorMessage = CodeLocalise("AssignToStaffSubActionDropDownRequired.ErrorMessage", "Staff member required");
      //ActivityAssigner.Visible = false;
      SubActionDropDown.Visible = SubActionButton.Visible = true;
    }
    
    /// <summary>
    /// Shows the errors.
    /// </summary>
    /// <param name="error">The error.</param>
    private void ShowErrors(ErrorTypes error)
    {
      switch (error)
      {
        case ErrorTypes.NoCandidatesSelected:

          CandidateActionDropDownValidator.ErrorMessage = HtmlLocalise("SingleCheckbox.Error.Text",
                                                                       "You must select a record to run this action");
          CandidateActionDropDownValidator.IsValid = false;
          break;

        case ErrorTypes.MultipleCandidatesSelected:

          CandidateActionDropDownValidator.ErrorMessage = HtmlLocalise("MultipleCheckbox.Error.Text",
                                                                       "You can only select one record at a time to run this action");
          CandidateActionDropDownValidator.IsValid = false;
          break;

      }
    }

    #endregion
    
		#region Helper methods

		/// <summary>
		/// Gets the user program of study.
		/// </summary>
		/// <returns></returns>
		private string GetUserProgramOfStudy(long? areaId, long? degreeId)
		{
			var programArea = "No program area";
			var programAreaDegree = "No degree found";

      if (_programAreaDegrees.IsNull())
        _programAreaDegrees = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels();

      if (areaId.IsNotNull())
        programArea = (areaId > 0)
          ? _programAreaDegrees.Where(pad => pad.ProgramAreaId == areaId).Select(pad => pad.ProgramAreaName).FirstOrDefault()
          : HtmlLocalise("Global.Undeclared", "Undeclared");

			if (degreeId.IsNotNull())
			{
        if (degreeId > 0)
          programAreaDegree = _programAreaDegrees.Where(pad => pad.DegreeEducationLevelId == degreeId).Select(pad => pad.DegreeEducationLevelName).FirstOrDefault();
        else if (degreeId == 0)
          programAreaDegree = HtmlLocalise("Global.Undeclared", "Undeclared");
        else
          programAreaDegree = HtmlLocalise("Global.AllDegrees", "All degrees");
			}

      return App.Settings.ShowProgramArea && programArea.IsNotNullOrEmpty()
               ? string.Concat(programArea, "/", programAreaDegree)
               : programAreaDegree;
		}

		#endregion

  }
}