﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TalentMessages.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.TalentMessages" %>

<%@ Register src="~/WebAssist/Controls/EmailTemplateEditor.ascx" tagname="EmailTemplateEditor" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<table style="width:100%;" role="presentation">
  <tr>
    <td style="width:150px;"></td>
    <td></td>
  </tr>
	<tr>
    <td colspan="2">
			<table style="width:100%;" role="presentation">
				<tr>
					<td><strong><%= HtmlLabel("TalentMessage.Title", "#FOCUSTALENT# default messages")%></strong></td>
					<td><asp:Button ID="SaveChangesButton" runat="server" class="button1 right" onclick="SaveChangesButton_Click" /></td>	
				</tr>
			</table>
		</td>
	</tr>
  <tr>
    <td colspan="2"><br /></td>
  </tr>
  <tr>
		<td><%= HtmlLabel("EmailTemplateName.Label", "Select message")%></td>
    <td style="vertical-align: top;">
      <asp:DropDownList runat="server" ID="EmailTemplateNameDropDown" AutoPostBack="True" OnSelectedIndexChanged="EmailTemplateNameDropDown_SelectedIndexChanged"  />
    </td>
  </tr>
  <tr>
    <td colspan="2"><br /></td>
  </tr>
</table>

<uc:EmailTemplateEditor ID="EmailTemplateEditor" runat="server" />
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />