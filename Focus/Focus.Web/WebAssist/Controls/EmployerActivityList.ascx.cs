﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.BusinessUnit;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class EmployerActivityList : UserControlBase
	{
		#region Properties

		private int _activitiesCount;
		
		private BusinessUnitCriteria BusinessUnitCriteria
		{
			get { return GetViewStateValue<BusinessUnitCriteria>("ListBusinessUnitActivity:BusinessUnitCriteria"); }
			set { SetViewStateValue("ListBusinessUnitActivity:BusinessUnitCriteria", value); }
		}

		/// <summary>
		/// Gets or sets the business unit id.
		/// </summary>
		public long BusinessUnitId { get; set; }
		
		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				BusinessUnitCriteria = new BusinessUnitCriteria
				{
					BusinessUnitId = BusinessUnitId,
					DaysBack = 30,
					OrderBy = Constants.SortOrders.ActivityDateDesc,
					FetchOption = CriteriaBase.FetchOptions.PagedList
				};

				BindActivityLogList();
				BindActivityFiltersDropDown();
			}
		}
		
		#region Bind Controls - Filters Dropdown Lists 

		/// <summary>
		/// Binds the activity log list.
		/// </summary>
		private void BindActivityLogList()
		{
			ActivityLogList.DataBind();

			// Set visibility of other controls
			ActivityLogPager.Visible = (ActivityLogPager.TotalRowCount > 0);
		}

		/// <summary>
		/// Binds the action description.
		/// </summary>
		/// <param name="action">The action.</param>
    private string BindActionDescription(BusinessUnitActivityViewDto action)
		{
			string literalText = "";

			ActionTypes actionType;
			if (!Enum.TryParse(action.ActionName, out actionType))
				return literalText;

			switch (actionType)
			{
				case ActionTypes.CreateJob:
					literalText = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# drafted");
					break;

				case ActionTypes.PostJob:
					literalText = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# posted");
					break;

				case ActionTypes.SaveJob:
					literalText = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# edited");
					break;

				case ActionTypes.RefreshJob:
					literalText = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# refreshed");
					break;

				case ActionTypes.ReactivateJob:
					literalText = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# reactivated");
					break;

				case ActionTypes.HoldJob:
					literalText = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# on hold");
					break;

				case ActionTypes.CloseJob:
					literalText = CodeLocalise(actionType + ".ActionDescription", "#EMPLOYMENTTYPE# closed");
					break;
				case ActionTypes.UnblockAllEmployerEmployeesAccount:
					literalText = CodeLocalise(actionType + ".ActionDescription", "All user accounts unblocked");
					break;

				case ActionTypes.BlockAllEmployerEmployeesAccount:
					literalText = CodeLocalise(actionType + ".ActionDescription", "All user accounts blocked");
					break;
			}

			if (actionType != ActionTypes.UnblockAllEmployerEmployeesAccount && actionType != ActionTypes.BlockAllEmployerEmployeesAccount)
				literalText += String.Format(" ({0}, ID{1})", action.JobTitle, action.JobId);

			return literalText;
		}

		/// <summary>
		/// Binds the activity filters drop down.
		/// </summary>
		private void BindActivityFiltersDropDown()
		{
			#region Days Filter

			DaysFilterDropDown.Items.Clear();

			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("TheLastDay.Text", "the last day"), "1"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last7Days.Text", "last 7 days"), "7"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last7Days.Text", "last 14 days"), "14"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last30Days.Text", "last 30 days"), "30"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last60Days.Text", "last 60 days"), "60"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last90Days.Text", "last 90 days"), "90"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last90Days.Text", "last 180 days"), "180"));
			DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("AllDays.Text", "all"), "0"));

			DaysFilterDropDown.SelectedIndex = 3;

			#endregion

			#region Order By Filter

			OrderByFilterDropDown.Items.Clear();

			OrderByFilterDropDown.Items.Add(new ListItem(CodeLocalise("Ascending.Text", "Ascending"), "activitydate asc"));
			OrderByFilterDropDown.Items.Add(new ListItem(CodeLocalise("Descending.Text", "Descending"), "activitydate desc"));

			OrderByFilterDropDown.SelectedIndex = 1;

			#endregion
		}

		#region Drop Downs Events

		/// <summary>
		/// Handles the SelectedIndexChanged event of the DaysFilterDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DaysFilterDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			BusinessUnitCriteria.DaysBack = Convert.ToInt32(DaysFilterDropDown.SelectedValue);
			ActivityLogPager.ReturnToFirstPage();
			BindActivityLogList();
			FormatActivityLogListSortingImages();
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the OrderByFilterDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void OrderByFilterDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			BusinessUnitCriteria.OrderBy = OrderByFilterDropDown.SelectedValue;
			ActivityLogPager.ReturnToFirstPage();
			BindActivityLogList();
			FormatActivityLogListSortingImages();
		}

		#endregion
		
		#endregion

		#region ActivityLogList Events 

		/// <summary>
		/// Handles the DataBound event of the ActivityLogList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ActivityLogList_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
      var action = (BusinessUnitActivityViewDto)e.Item.DataItem;
			var literal = (Literal)e.Item.FindControl("ActionDescription");

			literal.Text = BindActionDescription(action);
		}

		/// <summary>
		/// Handles the LayoutCreated event of the ActivityLogList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ActivityLogList_LayoutCreated(object sender, EventArgs e)
		{
			((Literal)ActivityLogList.FindControl("ActionDateTimeHeader")).Text = CodeLocalise("ActionDateTimeHeader.Text", "ACTION DATE/TIME");
			((Literal)ActivityLogList.FindControl("UsernameHeader")).Text = CodeLocalise("UsernameHeader.Text", "USERNAME");
			((Literal)ActivityLogList.FindControl("ActionHeader")).Text = CodeLocalise("ActionHeader.Text", "ACTION");

			ApplyBranding();
		}

		/// <summary>
		/// Handles the Sorting event of the ActivityLogList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void ActivityLogList_Sorting(object sender, ListViewSortEventArgs e)
		{
			BusinessUnitCriteria.OrderBy = e.SortExpression;
			FormatActivityLogListSortingImages();
		}

		#endregion

		#region ActivityLogDataSource Events

		/// <summary>
		/// Activities the log count.
		/// </summary>
		/// <returns></returns>
		public int GetActivityLogCount()
		{
			return _activitiesCount;
		}

		/// <summary>
		/// Activities the log count.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public int GetActivityLogCount(BusinessUnitCriteria criteria)
		{
			return _activitiesCount;
		}

		/// <summary>
		/// Handles the Selecting event of the ActivityLogDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void ActivityLogDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			if (BusinessUnitCriteria.IsNull())
			{
				BusinessUnitCriteria = new BusinessUnitCriteria { OrderBy = Constants.SortOrders.ActivityDateDesc, BusinessUnitId = BusinessUnitId };
			}

			e.InputParameters["criteria"] = BusinessUnitCriteria;
		}

		/// <summary>
		/// Gets the activity log.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
    public PagedList<BusinessUnitActivityViewDto> GetActivityLog(BusinessUnitCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_activitiesCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;
			criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			var activities = ServiceClientLocator.EmployerClient(App).GetBusinessUnitActivitiesAsPaged(criteria);
			_activitiesCount = activities.TotalCount;

			return activities;
		}

		#endregion

		#region Formatting Sorting Images

		/// <summary>
		/// Formats the activity log list sorting images.
		/// </summary>
		private void FormatActivityLogListSortingImages()
		{
			if (!ActivityLogPager.Visible) return;

			#region ResetEnable Activity Log Image Buttons

			((ImageButton)ActivityLogList.FindControl("ActionDateTimeAscButton")).ImageUrl =
			((ImageButton)ActivityLogList.FindControl("UsernameAscButton")).ImageUrl =
			((ImageButton)ActivityLogList.FindControl("ActionAscButton")).ImageUrl = UrlBuilder.CategorySortAscImage();
			((ImageButton)ActivityLogList.FindControl("ActionDateTimeDescButton")).ImageUrl =
			((ImageButton)ActivityLogList.FindControl("UsernameDescButton")).ImageUrl =
			((ImageButton)ActivityLogList.FindControl("ActionDescButton")).ImageUrl = UrlBuilder.CategorySortDescImage();

			((ImageButton)ActivityLogList.FindControl("ActionDateTimeDescButton")).Enabled =
			((ImageButton)ActivityLogList.FindControl("ActionDateTimeAscButton")).Enabled =
			((ImageButton)ActivityLogList.FindControl("UsernameDescButton")).Enabled =
			((ImageButton)ActivityLogList.FindControl("UsernameAscButton")).Enabled =
			((ImageButton)ActivityLogList.FindControl("ActionAscButton")).Enabled =
			((ImageButton)ActivityLogList.FindControl("ActionDescButton")).Enabled = true;

			#endregion

			switch (BusinessUnitCriteria.OrderBy.ToLower())
			{
				case Constants.SortOrders.ActivityDateAsc:
					((ImageButton)ActivityLogList.FindControl("ActionDateTimeAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)ActivityLogList.FindControl("ActionDateTimeAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.ActivityDateDesc:
					((ImageButton)ActivityLogList.FindControl("ActionDateTimeDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)ActivityLogList.FindControl("ActionDateTimeDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.ActivityActionAsc:
					((ImageButton)ActivityLogList.FindControl("ActionAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)ActivityLogList.FindControl("ActionAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.ActivityActionDesc:
					((ImageButton)ActivityLogList.FindControl("ActionDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)ActivityLogList.FindControl("ActionDescButton")).Enabled = false;
					break;

				case Constants.SortOrders.ActivityUsernameAsc:
					((ImageButton)ActivityLogList.FindControl("UsernameAscButton")).ImageUrl = UrlBuilder.ActivityListSortAscImage();
					((ImageButton)ActivityLogList.FindControl("UsernameAscButton")).Enabled = false;
					break;

				case Constants.SortOrders.ActivityUsernameDesc:
					((ImageButton)ActivityLogList.FindControl("UsernameDescButton")).ImageUrl = UrlBuilder.ActivityListSortDescImage();
					((ImageButton)ActivityLogList.FindControl("UsernameDescButton")).Enabled = false;
					break;
			}
		}

		#endregion

		#region Activity Log To CSV
		
		/// <summary>
		/// Gets the activities log as a csv byte array.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		private byte[] GetActivitiesLog(BusinessUnitCriteria criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			if (criteria.IsNull())
			{
				_activitiesCount = 0;
				return null;
			}

			criteria.PageSize = maximumRows;
			criteria.PageIndex = pageIndex;
			criteria.FetchOption = CriteriaBase.FetchOptions.List;

			var activities = ServiceClientLocator.EmployerClient(App).GetBusinessUnitActivitiesAsList(criteria);
			var activityCsv = ""; 

			if (activities.Count > 0)
			{
				var headerCompanyName = CodeLocalise("Csv.HeaderCompanyName", activities.First().BuName);
				var headerColumnsTitles = CodeLocalise("Csv.HeaderColumnTitles", "\nAction Date/Time,Username,Action");

				var query = activities.Select(a => new[]
		                                       {string.Concat("\n",a.ActionedOn.ToString(CultureInfo.InvariantCulture), ","), 
																						 string.Concat(a.UserName, ","), 
																						 string.Concat("\"", BindActionDescription(a), "\"")
                                           });

				var activitiesList = query.SelectMany(a => a.Select(b => b)).ToList();
				activityCsv = headerCompanyName + headerColumnsTitles + string.Join("", activitiesList);
			}
			return new ASCIIEncoding().GetBytes(activityCsv);
		}

		/// <summary>
		/// Handles the Click event of the DownloadCsvLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DownloadCsvLinkButton_Click(object sender, EventArgs e)
		{
			var bytes = GetActivitiesLog(BusinessUnitCriteria, "", 0, 10);

			// Send to screen
			Response.Clear();
			Response.ContentType = "application/octec-stream";
			Response.AppendHeader("Content-Disposition", "attachment; filename=EmployerActivityLog.csv");
			Response.OutputStream.Write(bytes, 0, bytes.Length);
			Response.End();
		}

		#endregion

		/// <summary>
		/// Applies branding.
		/// </summary>
		private void ApplyBranding()
		{
			((ImageButton)ActivityLogList.FindControl("ActionDateTimeDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
			((ImageButton)ActivityLogList.FindControl("ActionDateTimeAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();

			((ImageButton)ActivityLogList.FindControl("UsernameAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			((ImageButton)ActivityLogList.FindControl("UsernameDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();

			((ImageButton)ActivityLogList.FindControl("ActionAscButton")).ImageUrl = UrlBuilder.ActivityOnSortAscImage();
			((ImageButton)ActivityLogList.FindControl("ActionDescButton")).ImageUrl = UrlBuilder.ActivityOnSortDescImage();
		}
	}
}