﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI;

using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;

using Framework.Core;
using Focus.Services.Messages;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

#endregion

namespace Focus.Web.WebAssist.Controls
{

	public partial class ViewCaseManagementError : UserControlBase
	{
		public string RequestResponseData { get; set; }

		protected void Page_PreRender(object sender, EventArgs e)
		{
			ScriptManager.RegisterClientScriptInclude(this, GetType(), "Knockout", ResolveUrl("~/Assets/Scripts/knockout-3.1.0.js"));
			ScriptManager.RegisterClientScriptInclude(this, GetType(), "vkbeautify", ResolveUrl("~/Assets/Scripts/vkbeautify.0.99.00.beta.js"));
		}

		public void Show(long errorId)
		{
			var requestResponseMessages = ServiceClientLocator.CoreClient(App).GetIntegrationRequestResponses(errorId);
			RequestResponseData = JsonConvert.SerializeObject(requestResponseMessages);
			ViewCaseManagementErrorModalPopup.Show();
		}
	}
}
