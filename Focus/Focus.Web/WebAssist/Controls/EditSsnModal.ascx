﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditSsnModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EditSsnModal" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<asp:HiddenField ID="EditSsnModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="EditSsnModalPopup" runat="server" BehaviorID="EditSsnModal"
												TargetControlID="EditSsnModalDummyTarget"
												PopupControlID="EditSsnModalPanel"
												PopupDragHandleControlID="EditSsnModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="EditSsnModalPanel" runat="server" CssClass="modal" style="display:none;">
  <h2><asp:Literal runat="server" ID="ModalTitle" /></h2>
	<p>
		<span style="width: 100px;display:inline-block;"><%= HtmlLabel(CurrentSsnPart1TextBox,"OldSsn.Label", "Current SSN")%>:</span>
		<asp:TextBox ID="CurrentSsnPart1TextBox" runat="server" ClientIDMode="Static"
			MaxLength="3" size="3" Width="25" onKeyup="autotab(this, 'CurrentSsnPart2TextBox')"
			Enabled="False"/>
		-
		<focus:LocalisedLabel runat="server" ID="CurrentSsnPart2TextBoxLabel" AssociatedControlID="CurrentSsnPart2TextBox" LocalisationKey="OldSsn.Label" DefaultText="Current SSN Part2" CssClass="sr-only"/>
		<asp:TextBox ID="CurrentSsnPart2TextBox" runat="server" ClientIDMode="Static"
			MaxLength="2" size="2" Width="17" onKeyup="autotab(this, 'CurrentSsnPart3TextBox')"
		 Enabled="False" />
		-
		<focus:LocalisedLabel runat="server" ID="CurrentSsnPart3TextBoxLabel" AssociatedControlID="CurrentSsnPart3TextBox" LocalisationKey="OldSsn.Label" DefaultText="Current SSN Part3" CssClass="sr-only"/>
		<asp:TextBox ID="CurrentSsnPart3TextBox" runat="server" ClientIDMode="Static"
			MaxLength="4" size="4" Width="30" Enabled="False"/>
	</p>
	<p>
		<span style="width: 100px;display:inline-block;"><%= HtmlRequiredLabel(NewSsnPart1TextBox, "NewSsn.Label", "New SSN")%>:</span>
		<asp:TextBox ID="NewSsnPart1TextBox" runat="server" ClientIDMode="Static"
			MaxLength="3" size="3" Width="25" onKeyup="autotab(this, 'NewSsnPart2TextBox')" />
		-
		<focus:LocalisedLabel runat="server" ID="NewSsnPart2TextBoxLabel" AssociatedControlID="NewSsnPart2TextBox" LocalisationKey="NewSsn.Label" DefaultText="New SSN Part2" CssClass="sr-only"/>
		<asp:TextBox ID="NewSsnPart2TextBox" runat="server" ClientIDMode="Static"
			MaxLength="2" size="2" Width="17" onKeyup="autotab(this, 'NewSsnPart3TextBox')" />
		-
		<focus:LocalisedLabel runat="server" ID="NewSsnPart3TextBoxLabel" AssociatedControlID="NewSsnPart3TextBox" LocalisationKey="NewSsn.Label" DefaultText="New SSN Part3" CssClass="sr-only"/>
		<asp:TextBox ID="NewSsnPart3TextBox" runat="server" ClientIDMode="Static"
			MaxLength="4" size="4" Width="30"  />
	</p>
	<p>
		<asp:CustomValidator ID="SocialSecurityNumberValidator" runat="server" ClientValidationFunction="validateSsn"
			ValidationGroup="EditSsn" ValidateEmptyText="True" CssClass="error" SetFocusOnError="true"
			Display="Dynamic" />
			<asp:Label runat="server" ID="ErrorLabel" CssClass="error" Visible="false" />
	</p>
	<p>
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" CausesValidation="False" />
		<asp:Button ID="SaveButton" runat="server" CssClass="button4" OnClick="SaveButton_Clicked" CausesValidation="True" ValidationGroup="EditSsn" />
	</p>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" />

<script type="text/javascript">
  Sys.Application.add_load(function () {
    $("#<%=NewSsnPart1TextBox.ClientID%>").mask("9?99", { placeholder: '' });
    $("#<%=NewSsnPart2TextBox.ClientID%>").mask("9?9", { placeholder: '' });
    $("#<%=NewSsnPart3TextBox.ClientID%>").mask("9?999", { placeholder: '' });
  });
  
  function validateSsn(src, args) {
    var ssnPart1 = $("#NewSsnPart1TextBox").val();
    var ssnPart2 = $("#NewSsnPart2TextBox").val();
    var ssnPart3 = $("#NewSsnPart3TextBox").val();

    if (ssnPart1 == "" && ssnPart2 == "" && ssnPart3 == "") {
      src.innerHTML = "<%= SsnRequired %>";
      args.IsValid = false;
    } else {
      var ssn = ssnPart1 + "-" + ssnPart2 + "-" + ssnPart3;
      var ssnRegExp = /^[0-9]{3}[\- ]?[0-9]{2}[\- ]?[0-9]{4}$/;
      args.IsValid = ssnRegExp.test(ssn);
      src.innerHTML = "<%= SsnInvalid %>";
    }
  }
</script>