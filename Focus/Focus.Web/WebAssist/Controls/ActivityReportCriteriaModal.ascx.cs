﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;

using Focus.Common;
using Focus.Core;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class ActivityReportCriteriaModal : UserControlBase
	{
		#region Properties

    public AsyncReportType ReportType
    {
      get { return GetViewStateValue<AsyncReportType>("ActivityReportCriteriaModal:ReportType"); }
      set { SetViewStateValue("ActivityReportCriteriaModal:ReportType", value); }
    }

    protected string FromDateInFutureErrorMessage { get; set; }
    protected string FromDateAfterToDateErrorMessage { get; set; }
    protected string ToDateInvalidErrorMessage { get; set; }
    protected string FromDateInvalidErrorMessage { get; set; }
    protected string ToDateInFutureErrorMessage { get; set; }
    protected string DateTooOld { get; set; }

	  protected string DataFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpperInvariant();

		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

    protected void Page_PreRender(object sender, EventArgs e)
    {
      FromDateAfterToDateErrorMessage = CodeLocalise("ToDateFromDateCompare.ErrorMessage", "The From date must be before or on the To date");
      FromDateInFutureErrorMessage = CodeLocalise("FromDateNotInFuture.ErrorMessage", "The From date must not be in the future");
      ToDateInFutureErrorMessage = CodeLocalise("ToDateNotInFuture.ErrorMessage", "The To date must not be in the future");
      FromDateInvalidErrorMessage = CodeLocalise("FromDateInvalid.ErrorMessage", "The From date is invalid");
      ToDateInvalidErrorMessage = CodeLocalise("ToDateInvalid.ErrorMessage", "The To date is invalid");
      DateTooOld = CodeLocalise("DateTooOld.ErrorMessage", "Year must not be before 2000");
    }

		/// <summary>
		/// Shows the specified business unit identifier.
		/// </summary>
		/// <exception cref="System.Exception">Invalid Business Unit Id.</exception>
		public void Show()
		{
			Initialise();
			ActivityReportCriteriaModalPopup.Show();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
      ActivityReportTitle.Text = CodeLocalise(ReportType, true);

			RunButton.Text = CodeLocalise("RunButton.Label", "Run report");
      FromDateRequired.ErrorMessage = CodeLocalise("FromDateRequired.ErrorMessage", "From date is required");
			ToDateRequired.ErrorMessage = CodeLocalise("ToDateRequired.ErrorMessage", "To date is required");
			SendToRequired.ErrorMessage = CodeLocalise("SendToRequired.ErrorMessage", "At least one email address is required");
			OkButton.Text = CodeLocalise("OkButton.Label", "Ok");
		}

		/// <summary>
		/// Initialises this instance.
		/// </summary>
		private void Initialise()
		{
			CompletionPlaceholder.Visible = false;
			CriteriaPlaceholder.Visible = true;
			ToDateTextBox.Text = string.Empty;
			FromDateTextBox.Text = string.Empty;
			SendToTextbox.Text = string.Empty;

			ActivityReportCriteriaModalPanel.Attributes.Remove("style");
			ActivityReportCriteriaModalPanel.Attributes.Add("style", "display:none; min-height: 275px;");
		}

		/// <summary>
		/// Handles the Clicked event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void RunButton_Clicked(object sender, EventArgs e)
		{
			var fromDate = FromDateTextBox.Text.ToDate();
			var toDate = ToDateTextBox.Text.ToDate();

			if (fromDate != null && toDate != null)
			{
				var validation = ServiceClientLocator.ReportClient(App).GenerateActivityReport(ReportType, fromDate.Value, toDate.Value, SendToTextbox.Text);
				if (validation.IsNotNull() && validation.Errors.IsNotNullOrEmpty())
				{
					// Display error if any of the email addresses were invalid.  
					// Other error types can/are returned however these should be captured by the validators so I haven't added further custom validators to display the same errors from the server code
					if (validation.Errors.Any(x => x.Equals(ErrorTypes.InvalidEmail)))
					{
						var invalidEmails = string.Join(", ", validation.InvalidEmails);

						EmailValidator.IsValid = false;
						EmailValidator.Text = string.Concat(HtmlLocalise("InvalidEmail.ErrorMessage", "The following email addresses are invalid, please correct them and try again:<br/><br/>"), invalidEmails);

						ActivityReportCriteriaModalPopup.Show();
					}
				}
				else
				{
					ActivityReportCriteriaModalPanel.Attributes.Remove("style");
					ActivityReportCriteriaModalPanel.Attributes.Add("style", "display:none; min-height: 125px;");

					CompletionPlaceholder.Visible = true;
					CriteriaPlaceholder.Visible = false;

					ActivityReportCriteriaModalPopup.Show();
				}
			}
		}

		/// <summary>
		/// Handles the Clicked event of the OkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void OkButton_Clicked(object sender, EventArgs e)
		{
			ActivityReportCriteriaModalPopup.Hide();
		}
	}
}