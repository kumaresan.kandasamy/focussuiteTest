﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

using Framework.Core;
using System.Web;

#endregion


namespace Focus.Web.WebAssist.Controls
{
	public partial class SavedAnnouncements : UserControlBase
	{
		private List<SavedMessageTextDto> _savedMessageTexts;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the AnnouncementAudienceRadioButtonList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void AnnouncementAudienceRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
		{
			var audience = AnnouncementAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();
			BindSavedAnnouncementDropDown(audience);
			BindCultureSpecificAnnouncementsRepeater(audience);
			_savedMessageTexts = null;
			AnnouncementTextbox.Text = "";
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the SavedAnnouncementsDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SavedAnnouncementsDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			AnnouncementTextbox.Text = "";
			_savedMessageTexts = (SavedAnnouncementsDropDown.SelectedValueToLong() > 0) ? ServiceClientLocator.CoreClient(App).GetSavedMessageTexts(SavedAnnouncementsDropDown.SelectedValueToLong()) : null;

			if (_savedMessageTexts.IsNotNullOrEmpty())
			{
				var defaultLocalisation = ServiceClientLocator.CoreClient(App).GetDefaultCulture();

				AnnouncementTextbox.Text = _savedMessageTexts.Where(x => x.LocalisationId == defaultLocalisation.Id).FirstOrDefault().Text;

				BindCultureSpecificAnnouncementsRepeater(AnnouncementAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>());
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SaveAnnouncementButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveAnnouncementButton_Clicked(object sender, EventArgs e)
		{
			var savedMessageId = SavedAnnouncementsDropDown.SelectedValueToLong();

			if (savedMessageId == 0) throw new Exception("Message Id required");

			var messageTexts = UnbindCultureSpecificAnnouncementsRepeaterToSavedMessageTexts(savedMessageId);

			var audience = AnnouncementAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();

			// Add the default message text, set the LocalisationId to zero and the service will replace it with the correct one
			messageTexts.Add(new SavedMessageTextDto
										{
											SavedMessageId = savedMessageId,
											LocalisationId = 0,
											Text = HttpUtility.HtmlDecode(AnnouncementTextbox.TextTrimmed())
										});

			var message = new SavedMessageDto
										{
											Id = savedMessageId,
											Audience = audience,
											Name = SavedAnnouncementsDropDown.Items[SavedAnnouncementsDropDown.SelectedIndex].Text,
											UserId = App.User.UserId
										};

			ServiceClientLocator.CoreClient(App).SaveMessageTexts(message, messageTexts);

			BindSavedAnnouncementDropDown(audience);
			Confirmation.Show(CodeLocalise("AnnouncementSavedConfirmation.Title", "Announcement saved"),
															 CodeLocalise("AnnouncementSavedConfirmation.Body", "The announcement has been saved."),
															 CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Handles the ItemDataBound event of the CultureSpecificAnnouncementsRepeater control.
		/// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void CultureSpecificAnnouncementsRepeater_ItemDataBound(object source, RepeaterItemEventArgs e)
		{
			// Do nothing if this isn't a Item or AlternateItem
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			var localisationIdHidden = (HiddenField)e.Item.FindControl("LocalisationIdHidden");
			localisationIdHidden.Value = ((LocalisationDto)e.Item.DataItem).Id.GetValueOrDefault().ToString();

			var cultureNameLabel = (Label)e.Item.FindControl("CultureNameLabel");
			cultureNameLabel.Text = ((LocalisationDto)e.Item.DataItem).Culture;

			var cultureSpecificAnnouncementTextbox = (TextBox)e.Item.FindControl("CultureSpecificAnnouncementTextbox");

			var cultureSpecificAnnouncementInlineLabel = (Label)e.Item.FindControl("CultureSpecificAnnouncementInlineLabel");
			cultureSpecificAnnouncementInlineLabel.Text = CodeLocalise("CultureSpecificAnnouncement.InlineLabel", "enter announcement for the {0} culture", ((LocalisationDto)e.Item.DataItem).Culture);
			cultureSpecificAnnouncementInlineLabel.AssociatedControlID = cultureSpecificAnnouncementTextbox.ID;

			if (_savedMessageTexts.IsNotNullOrEmpty())
			{
				var savedMessageText = _savedMessageTexts.Where(x => x.LocalisationId == ((LocalisationDto)e.Item.DataItem).Id.GetValueOrDefault()).FirstOrDefault();
				if (savedMessageText != null) cultureSpecificAnnouncementTextbox.Text = savedMessageText.Text;
			}
		}

		/// <summary>
		/// Handles the Clicked event of the DeleteButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DeleteButton_Clicked(object sender, EventArgs e)
		{
			var savedMessageId = SavedAnnouncementsDropDown.SelectedValueToLong();

			if (savedMessageId == 0) throw new Exception("Message Id required");

			DeleteConfirmationModal.Show(CodeLocalise("DeleteConfirmation.Title", "Delete Saved Announcement"),
														CodeLocalise("DeleteConfirmation.Body", "Are you sure you want to delete {0}?", SavedAnnouncementsDropDown.Items[SavedAnnouncementsDropDown.SelectedIndex].Text),
														CodeLocalise("Global.Cancel.Text", "Cancel"),
														okText: CodeLocalise("DeleteConfirmation.Ok.Text", "Delete saved announcement"), okCommandName: "Delete", okCommandArgument: savedMessageId.ToString());
		}

		/// <summary>
		/// Handles the OkCommand event of the DeleteConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void DeleteConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			var messageId = GetSavedMessageId(e.CommandArgument);
			var audience = AnnouncementAudienceRadioButtonList.SelectedValueToEnum<MessageAudiences>();

			ServiceClientLocator.CoreClient(App).DeleteSavedMessage(messageId);

			// Clear controls and rebind the saved messages drop down
			_savedMessageTexts = null;
			AnnouncementTextbox.Text = "";
			
			BindSavedAnnouncementDropDown(audience);
			SavedAnnouncementsDropDown.SelectedIndex = 0;
			BindCultureSpecificAnnouncementsRepeater(audience);
		}

		#region Bind and Unbind methods

		/// <summary>
		/// Binds this instance.
		/// </summary>
		public void Bind()
		{
			AnnouncementTextbox.Text = "";
			BindAnnouncementAudienceRadioButtonList();
			AnnouncementAudienceRadioButtonList.SelectedIndex = 0;
			BindSavedAnnouncementDropDown(MessageAudiences.AllJobSeekers);
			BindCultureSpecificAnnouncementsRepeater(MessageAudiences.AllJobSeekers);

		}

		/// <summary>
		/// Binds the announcement audience radio button list.
		/// </summary>
		private void BindAnnouncementAudienceRadioButtonList()
		{
			AnnouncementTextbox.Text = "";

			AnnouncementAudienceRadioButtonList.Items.Clear();

			AnnouncementAudienceRadioButtonList.Items.AddEnum(MessageAudiences.AllJobSeekers, HtmlLocalise("Audience.Jobseeker.Label", "#CANDIDATETYPES#"));
			if (App.Settings.TalentModulePresent)
				AnnouncementAudienceRadioButtonList.Items.AddEnum(MessageAudiences.AllTalentUsers, HtmlLocalise("Audience.Employers.Label", "Hiring Managers"));
			AnnouncementAudienceRadioButtonList.Items.AddEnum(MessageAudiences.AllAssistUsers, HtmlLocalise("Audience.Staff.Label", "Staff members"));

			AnnouncementAudienceRadioButtonList.SelectedIndex = 0;
		}

		/// <summary>
		/// Binds the saved announcement drop down.
		/// </summary>
		/// <param name="audience">The audience.</param>
		private void BindSavedAnnouncementDropDown(MessageAudiences audience)
		{
			SavedAnnouncementsDropDown.DataSource = ServiceClientLocator.CoreClient(App).GetSavedMessages(audience);
			SavedAnnouncementsDropDown.DataValueField = "Id";
			SavedAnnouncementsDropDown.DataTextField = "Name";
			SavedAnnouncementsDropDown.DataBind();
			SavedAnnouncementsDropDown.Items.AddLocalisedTopDefault("SavedMessagesDropDownList.TopDefault",
				"- select an announcement -");
			AnnouncementTextbox.Text = "";
			BindCultureSpecificAnnouncementsRepeater(audience);
		}

		/// <summary>
		/// Binds the culture specific announcements repeater.
		/// </summary>
		/// <param name="audience">The audience.</param>
		private void BindCultureSpecificAnnouncementsRepeater(MessageAudiences audience)
		{
			var t = CultureSpecificAnnouncementsRepeater.Items;
			var cultures = (audience != MessageAudiences.AllJobSeekers) ? ServiceClientLocator.CoreClient(App).GetNonDefaultCultures() : new List<LocalisationDto>();

			if (cultures.Count > 0)
			{
				CultureSpecificAnnouncementsRepeater.DataSource = cultures;
				CultureSpecificAnnouncementsRepeater.DataBind();
				CultureSpecificAnnouncementsRepeater.Visible = true;
			}
			else
			{
				CultureSpecificAnnouncementsRepeater.Visible = false;
			}
		}

		/// <summary>
		/// Unbinds the culture specific announcements repeater to saved message texts.
		/// </summary>
		/// <returns></returns>
		private List<SavedMessageTextDto> UnbindCultureSpecificAnnouncementsRepeaterToSavedMessageTexts(long savedMessageId)
		{
			return (from RepeaterItem item in CultureSpecificAnnouncementsRepeater.Items
							let localisationId = Convert.ToInt64(((HiddenField)item.FindControl("LocalisationIdHidden")).Value)
							let cultureSpecificAnnouncementTextbox = (TextBox)item.FindControl("CultureSpecificAnnouncementTextbox")
							where cultureSpecificAnnouncementTextbox.IsNotNull() && cultureSpecificAnnouncementTextbox.TextTrimmed().IsNotNullOrEmpty()
							select new SavedMessageTextDto
							{
								SavedMessageId = savedMessageId,
								LocalisationId = localisationId,
								Text = HttpUtility.HtmlDecode(cultureSpecificAnnouncementTextbox.TextTrimmed())
							}).ToList();
		}

		#endregion

		/// <summary>
		/// Gets the saved message id.
		/// </summary>
		/// <param name="commandArgument">The command argument.</param>
		/// <returns></returns>
		private static long GetSavedMessageId(object commandArgument)
		{
			long messageId;
			long.TryParse(commandArgument.ToString(), out messageId);

			return messageId;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SaveAnnouncementButton.Text = CodeLocalise("Save.Label", "Save announcement");
			AnnouncementRequiredSave.ErrorMessage = CodeLocalise("AnnouncementRequired.ErrorMessage", "Announcement text is required");
			DeleteButton.Text = CodeLocalise("Global.Delete.Label", "Delete");
			SavedAnnouncementRequiredDelete.ErrorMessage = CodeLocalise("AnnouncementRequiredDelete.ErrorMessage", "Select a saved announcement to delete");
			SavedAnnouncementRequired.ErrorMessage = CodeLocalise("AnnouncementRequired.ErrorMessage", "Select a saved announcement to update");
		}
	}
}