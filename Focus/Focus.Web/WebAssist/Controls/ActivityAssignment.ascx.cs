﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Common.Extensions;
using Focus.Common;

#endregion


namespace Focus.Web.WebAssist.Controls
{
    public partial class ActivityAssignment : UserControlBase
    {
        public ActivityType ActivityType { get; set; }

        private FlowDirection _flow;

        public FlowDirection Flow
        {
            get { return _flow; }
            set
            {
                _flow = value;
                linebreaker.Visible = (FlowDirection.Vertical == _flow);
            }
        }

        private static long ActionMenuMaxDays;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Localise();
                Bind();
                linebreaker.Visible = FlowDirection.Vertical == Flow;
            }
        }

        /// <summary>
        /// Binds this instance.
        /// </summary>
        private void Bind()
        {
            ActivityCategoryDropDownList.DataTextField = "Name";
            ActivityCategoryDropDownList.DataValueField = "Id";
            ActivityCategoryDropDownList.DataSource = ServiceClientLocator.CoreClient(App).GetActivityCategories(ActivityType);
            ActivityCategoryDropDownList.DataBind();
            ActivityCategoryDropDownList.Items.Insert(0, CodeLocalise("ActivityCategory.Type", "- Category -"));


            ActivityDateTextBox.Visible =
                ActivityDateCompareValidator.Enabled =
                  ActivityDateIsNotEmpty.Enabled = (ActivityType == ActivityType.JobSeeker) ? App.Settings.EnableJSActivityBackdatingDateSelection && App.User.IsInRole(Constants.RoleKeys.BackdateMyEntriesJSActionMenuMaxDays) : App.Settings.EnableHiringManagerActivityBackdatingDateSelection && App.User.IsInRole(Constants.RoleKeys.BackdateMyEntriesHMActionMenuMaxDays);

            ActivityDateCompareValidator.ValueToCompare = DateTime.Now.ToShortDateString();
            ActivityDateTextBox.Text = DateTime.Now.ToShortDateString();
        }

        /// <summary>
        /// Localises this instance.
        /// </summary>
        private void Localise()
        {
            ActivityCascader.LoadingText = CodeLocalise("ActivityCascader.LoadingText", "Loading activities");
            SelectActivityButton.Text = CodeLocalise("SaveActivity.Button", "Save");
            ActivityRequiredValidator.ErrorMessage = CodeLocalise("ActivityRequired.ErrorMessage", "Please choose an activity");

            if (((ActivityType == ActivityType.JobSeeker) && App.Settings.EnableJSActivityBackdatingDateSelection && App.User.IsInRole(Constants.RoleKeys.BackdateMyEntriesJSActionMenuMaxDays)) || (!(ActivityType == ActivityType.JobSeeker) && App.Settings.EnableHiringManagerActivityBackdatingDateSelection && App.User.IsInRole(Constants.RoleKeys.BackdateMyEntriesHMActionMenuMaxDays)))
                ActivityDateCompareValidator.ErrorMessage = CodeLocalise("ActivityDate.ErrorMessage", "Activity date must be equal to or less than today's date");
        }

        /// <summary>
        /// Handles the Click event of the SelectActivityButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SelectActivityButton_Click(object sender, EventArgs e)
        {
            DateTime? actiondate = null;

            if (((ActivityType == ActivityType.JobSeeker) && App.Settings.EnableJSActivityBackdatingDateSelection && App.User.IsInRole(Constants.RoleKeys.BackdateMyEntriesJSActionMenuMaxDays)) || (!(ActivityType == ActivityType.JobSeeker) && App.Settings.EnableHiringManagerActivityBackdatingDateSelection && App.User.IsInRole(Constants.RoleKeys.BackdateMyEntriesHMActionMenuMaxDays)))
            {
                actiondate = DateTime.Parse(ActivityDateTextBox.Text);
                var currentdate = DateTime.Now.Date;
                //FVN-7828 & FVN-7834
                if (actiondate == currentdate)
                    actiondate = actiondate + DateTime.Now.TimeOfDay;
            }

            FireActivitySelected(new ActivitySelectedArgs(ActivityDropDownList.SelectedValue.AsLong(), actiondate));
            //ActivityCategoryDropDownList.SelectedIndex = 0;
        }

        /// <summary>
        /// Occurs when [selected activity].
        /// </summary>
        public event ActivitySelectedHandler ActivitySelected;

        public delegate void ActivitySelectedHandler(object o, ActivitySelectedArgs e);

        protected virtual void FireActivitySelected(ActivitySelectedArgs e)
        {
            if (ActivitySelected != null)
                ActivitySelected(this, e);
        }

        public class ActivitySelectedArgs : EventArgs
        {
            public readonly long SelectedActivityId;
            public readonly DateTime? ActionedDate;

            public ActivitySelectedArgs(long activity, DateTime? actionedDate)
            {
                SelectedActivityId = activity;
                ActionedDate = actionedDate;
            }
        }

    }
}