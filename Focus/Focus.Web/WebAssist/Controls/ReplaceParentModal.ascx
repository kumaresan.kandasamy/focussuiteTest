﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReplaceParentModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.ReplaceParentModal" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<asp:HiddenField ID="ReplaceParentModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ReplaceParentModalPopup" runat="server" BehaviorID="ReplaceParentModal"
												TargetControlID="ReplaceParentModalDummyTarget"
												PopupControlID="ReplaceParentModalPanel"
												PopupDragHandleControlID="ReplaceParentModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="ReplaceParentModalPanel" runat="server" CssClass="modal" style="display:none; position: absolute">
  <table style="width:500px;">
		<tr>
			<th style="vertical-align: top;" data-modal='title'><%= Title  %></th>
		</tr>
	</table>
	<p>&nbsp;</p>
	<p>
		<focus:LocalisedLabel runat="server" RenderOuterSpan="True" ID="ReplaceParentInstructionsLabel" DefaultText="Select which linked business unit you wish to replace the existing parent #BUSINESS#:LOWER with:" Width="95%"/>
	</p>
	<p>
		<span style="display: block; overflow: auto; height: 100px"> 
			<asp:RadioButtonList runat="server" ID="BusinessUnitRadioButtonList" RepeatDirection="Vertical" RepeatLayout="Flow"/>
		</span>
		<span>
			<asp:RequiredFieldValidator ID="BusinessUnitRadioButtonListRequiredValidator" ValidationGroup="BUSelectionRequired" CssClass="error" ControlToValidate="BusinessUnitRadioButtonList" runat="server"></asp:RequiredFieldValidator>
		</span>
	</p>
	<p>&nbsp;</p>
	<p>
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" CausesValidation="False" />
		<asp:Button ID="ReplaceButton" runat="server" CssClass="button4" OnClick="ReplaceButton_Clicked" ValidationGroup="BUSelectionRequired" CausesValidation="True" />
	</p>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" ClientIDMode="AutoID" OnCloseCommand="Confirmation_CloseCommand"/>
