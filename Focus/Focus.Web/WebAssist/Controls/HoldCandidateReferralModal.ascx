﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HoldCandidateReferralModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.HoldCandidateReferralModal" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget" PopupControlID="ModalPanel" PopupDragHandleControlID="ModalPanelHeader" RepositionMode="RepositionOnWindowResizeAndScroll" BackgroundCssClass="modalBackground">
	<Animations><OnShown><ScriptAction Script="BindOnHoldCandidateReferralOtherReason();" />  </OnShown></Animations>
</act:ModalPopupExtender>
<asp:Panel ID="ModalPanel" runat="server" CssClass="modal" Style="display:none;width:600px;">
	<asp:Panel runat="server" ID="ModalPanelHeader">
		<h1>
			<asp:Literal runat="server" ID="TitleLiteral"></asp:Literal>
		</h1>
	</asp:Panel>
	<p>
		<asp:Literal runat="server" Id="SubTextLiteral" ></asp:Literal>
	</p>
	<p>
		<asp:Literal runat="server" Id="EmailSubjectLiteral"></asp:Literal> : <asp:TextBox runat="server" ID="EmailSubjectTextBox" Width="500px" Enabled="False" Title="Subject"></asp:TextBox>
	</p>
	<p>
		<asp:TextBox runat="server" ID="SalutationTextBox" Enabled="False" Width="250px" Title="Salutation"></asp:TextBox>
	</p>
	<p>
		<asp:TextBox runat="server" ID="EmailBodyTextBox" TextMode="MultiLine" Rows="10" Width="550px" Title="Email body"/>
		<p><asp:RequiredFieldValidator Visible="false" ID="EmailBodyRequired" runat="server" ControlToValidate="EmailBodyTextBox" SetFocusOnError="true" CssClass="error" ValidationGroup="HoldCandidateReferral" Display="Dynamic" Enabled="False" /></p>
	</p>

	<p><asp:CheckBox id="BCCMeCheckBox" runat="server" /></p>
	<p>
		<asp:Button ID="CancelButton" runat="server" class="button4" />
		<asp:Button ID="HoldAndSendMessageButton" runat="server" CssClass="button3" OnClick="HoldAndSendMessageButton_Clicked" ValidationGroup="HoldCandidateReferral"/>
	</p>
</asp:Panel>
<uc:ConfirmationModal ID="Confirmation" runat="server" Width="300px" />
