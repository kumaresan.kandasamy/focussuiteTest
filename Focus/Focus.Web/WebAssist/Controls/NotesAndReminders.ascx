﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NotesAndReminders.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.NotesAndReminders" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<%@ Import Namespace="Framework.Core" %>

<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagName="ConfirmationModal" tagPrefix="uc" %>

<asp:UpdatePanel ID="NotesRemindersUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<div style="width: 710px">
			<asp:Panel ID="AddNotePanel" runat="server">
				<table role="presentation">
					<tr>
						<td><asp:Label ID="AddNoteLabel" runat="server" Text="Add a Note" AssociatedControlID="AddNoteRadio" CssClass="groupLabel" /><asp:RadioButton ID="AddNoteRadio" runat="server" GroupName="actionRadio" Checked="true" /><%= HtmlLocalise("AddNoteRadio.Text", "Add a note")%> <asp:Literal runat="server" ID="ForLiteral"></asp:Literal></td>
						<td>&nbsp;</td>
                        <td>
	                        <focus:LocalisedLabel runat="server" ID="NoteForDropDownLabel" AssociatedControlID="NoteForDropDown" LocalisationKey="Note" DefaultText="Note" CssClass="sr-only"/>
	                        <asp:DropDownList ID="NoteForDropDown" runat="server" />
                        </td>
					</tr>
					<tr>
						<td><asp:Label ID="AddAReminderFor" runat="server" Text="Add a Reminder For" AssociatedControlID="AddReminderRadio" CssClass="groupLabel" /><asp:RadioButton ID="AddReminderRadio" runat="server" GroupName="actionRadio"  /><%= HtmlLocalise("AddReminderRadio.Text", "Add a reminder for")%></td>
						<td>&nbsp;</td>
						<td>
							<focus:LocalisedLabel runat="server" ID="ReminderForDropDownLabel" AssociatedControlID="ReminderForDropDown" LocalisationKey="Reminder" DefaultText="Reminder" CssClass="sr-only"/>
							<asp:DropDownList runat="server" ID="ReminderForDropDown" ClientIDMode="AutoID"/>
						</td>
					</tr>
				</table>

				<div id="ReminderSection" class="opacity" runat="server">
					<div id="ReminderOverlay" class="overlay" runat="server"></div>
					<table class="NotePanelInputs" role="presentation">
						<tr>
							<td><asp:RadioButtonList runat="server" ID="ReminderTypeRadioButtonList" RepeatDirection="Horizontal" CssClass="RadioButtonList reminderRadioButton" role="presentation"/></td>
							<td class="requiredData"><asp:TextBox ID="ReminderDateTextBox" runat="server" /></td>
							<td>
								<asp:RequiredFieldValidator ID="ReminderDateRequired" runat="server" ControlToValidate="ReminderDateTextBox" CssClass="error" Display="Dynamic" ValidationGroup="NotesAndReminders" Enabled="false" />
								<asp:CompareValidator ID="ReminderDateGreaterThanTodayCompareValidatior" runat="server" ControlToValidate="ReminderDateTextBox" Type="Date" Operator="GreaterThan" CssClass="error" Display="Dynamic" 
																			ValidationGroup="NotesAndReminders" Enabled="false" />
							</td>
						</tr>
					</table>
					<act:CalendarExtender ID="ReminderDateCalendarExtender" runat="server" BehaviorID="ReminderDateCalendar" TargetControlID="ReminderDateTextBox" CssClass="cal_Theme1" />
				</div>
				<br />
				<%= HtmlInFieldLabel(NoteTextBox.ClientID, "NoteTextBox.Label", "Type note or reminder here.", 600, "inFieldLabelAlt")%>
				<asp:TextBox ID="NoteTextBox" runat="server" TextMode="MultiLine" Rows="5" Width="675"  />
				<asp:RequiredFieldValidator ID="NoteRequired" runat="server" ControlToValidate="NoteTextBox" CssClass="error" Display="Dynamic" ValidationGroup="NotesAndReminders" />
				<br />
				<br />
				<asp:Button ID="AddNoteButton" runat="server" CssClass="button2" OnClick="AddNoteButton_Click" ValidationGroup="NotesAndReminders" Text="Add note or reminder"/>
				<br />
				<br />
			</asp:Panel>
				
			<div ID="NotesRemindersHeaderTable" runat="server">
			<%= HtmlLocalise("JobOrdersShow.Label", "Show")%>
				<focus:LocalisedLabel runat="server" ID="NoteReminderDropDownLabel" AssociatedControlID="NoteReminderDropDown" LocalisationKey="NoteReminder" DefaultText="Note reminder" CssClass="sr-only"/>
				<asp:DropDownList runat="server" ID="NoteReminderDropDown" Width="110px" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="NoteReminderDropDown_SelectedIndexChanged"/>&nbsp;
        <focus:LocalisedLabel runat="server" ID="NoteTypeDropDownLabel" AssociatedControlID="NoteTypeDropDown" LocalisationKey="NoteType" DefaultText="Note type" CssClass="sr-only"/>
				<asp:DropDownList runat="server" ID="NoteTypeDropDown" AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="NoteReminderDropDown_SelectedIndexChanged" ></asp:DropDownList>&nbsp;
			  <br />
        <uc:Pager ID="NotesRemindersListPager" runat="server" DisplayRecordCount="True" PagedControlID="NotesRemindersList" PageSize="10" />
			</div>
			<br />
			<asp:ListView ID="NotesRemindersList" runat="server" ItemPlaceholderID="NotesRemindersPlaceHolder" DataSourceID="NotesRemindersDataSource" >
				<LayoutTemplate>
					<table style="padding-right: 5px; width: 100%;" role="presentation">
						<tr>
							<td><asp:PlaceHolder ID="NotesRemindersPlaceHolder" runat="server" /></td>
						</tr>
					</table>
				</LayoutTemplate>	
				<ItemTemplate>
				<p>
					<i><%# ((NoteReminderViewDto)Container.DataItem).CreatedByFirstname %> <%# ((NoteReminderViewDto)Container.DataItem).CreatedByLastname %> - <%# ((NoteReminderViewDto)Container.DataItem).CreatedOn.ToString("MMMM dd, yyyy") %></i>
					<br />
					<%# ((NoteReminderViewDto)Container.DataItem).Text.HtmlEncode() %>
				</p>
				</ItemTemplate>
			</asp:ListView>
			
			<asp:ObjectDataSource ID="NotesRemindersDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.NotesAndReminders" EnablePaging="true" SelectMethod="GetNotesReminders" 
														SelectCountMethod="GetNotesRemindersCount" SortParameterName="orderBy" OnSelecting="NotesRemindersDataSource_Selecting" />
		</div>
		<uc:ConfirmationModal ID="NotesRemindersConfirmationModal" runat="server"/>
	</ContentTemplate>
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="NoteReminderDropDown" EventName="SelectedIndexChanged" />
	</Triggers>
</asp:UpdatePanel>


<script type="text/javascript">
	$(document).ready(function () {
		bindJavascript();
	});
	

	 Sys.Application.add_load(NotesAndReminders_PageLoad);

	 function NotesAndReminders_PageLoad(sender, args) {
	 	DisableOverlayControls();
	 }

	 var prm = Sys.WebForms.PageRequestManager.getInstance();

	prm.add_endRequest(function () {
		bindJavascript();
		// Apply jQuery to infield labels
		$(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();
	});
	
	function bindJavascript() {
		$("#<%= AddReminderRadio.ClientID %>").change(function () {
		  NotesAndReminders_ReminderOverlay(false);
		});

		$("#<%= AddNoteRadio.ClientID %>").change(function () {
		  NotesAndReminders_ReminderOverlay(true);
        });
if ($("#<%= AddNotePanel.ClientID %>").is(":visible")) {
	  NotesAndReminders_ReminderOverlay($("#<%= AddNoteRadio.ClientID %>").is(":checked"));
	}}
	
  function NotesAndReminders_ReminderOverlay(isNote) {
    if (isNote) {
      $("#<%= ReminderSection.ClientID %>").addClass("opacity");
      $("#<%= ReminderOverlay.ClientID %>").addClass("overlay");

      DisableOverlayControls();

      ValidatorEnable($("#<%= ReminderDateRequired.ClientID %>")[0], false);
      ValidatorEnable($("#<%= ReminderDateGreaterThanTodayCompareValidatior.ClientID %>")[0], false);
    } else {
      $("#<%= ReminderSection.ClientID %>").removeClass("opacity");
      $("#<%= ReminderOverlay.ClientID %>").removeClass("overlay");

      var overlayInputs = $(".NotePanelInputs :input");
      overlayInputs.prop('disabled', false);
      $('#calendarimage').show();

      ValidatorEnable($("#<%= ReminderDateRequired.ClientID %>")[0], true);
      ValidatorEnable($("#<%= ReminderDateGreaterThanTodayCompareValidatior.ClientID %>")[0], true);
      $("#<%= ReminderDateRequired.ClientID %>").hide();
    }
  }

	function DisableOverlayControls() {
		var overlayInputs = $(".NotePanelInputs :input");
		overlayInputs.prop('disabled', true);
		$('#calendarimage').hide();
    }

</script>
