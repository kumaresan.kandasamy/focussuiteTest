﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditFEINModal.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.EditFEINModal" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Code/Controls/User/ConfirmationModal.ascx" %>
<asp:HiddenField ID="EditFEINModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="EditFEINModalPopup" runat="server" BehaviorID="EditFEINModal"
												TargetControlID="EditFEINModalDummyTarget"
												PopupControlID="EditFEINModalPanel"
												PopupDragHandleControlID="EditFEINModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll" 
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="EditFEINModalPanel" runat="server" CssClass="modal" style="display:none;" DefaultButton="SaveButton">
	<table style="width:500px; border: 0;">
		<tr>
			<th style="vertical-align:top;" data-modal='title'><%= Title %></th>
		</tr>
	</table>
	<p style="margin-left:4px;">
		<focus:LocalisedLabel runat="server" DefaultText="Current FEIN" RenderOuterSpan="True" ID="CurrentFEINLabel" Width="20%" />
		<asp:Label ID="CurrentFEINValue" runat="server" />
	</p>
	<p style="margin-left:4px;">
		<focus:LocalisedLabel runat="server" AssociatedControlID="NewFEINTextbox" DefaultText="New FEIN" RenderOuterSpan="True" ID="NewFEINLabel" Width="20%" />
		<asp:TextBox ID="NewFEINTextbox" runat="server" MaxLength="10" TabIndex="1" Width="20%" />
		<asp:RequiredFieldValidator ID="NewFEINRequired" runat="server" ControlToValidate="NewFEINTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="EditFEIN" />
		<asp:CompareValidator ID="NewFEINCompare" runat="server" ControlToValidate="NewFEINTextbox" ValueToCompare="" Type="String" Operator="NotEqual" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="EditFEIN" />
	</p>
	<p style="margin-left:4px;">
		<focus:LocalisedLabel runat="server" AssociatedControlID="ConfirmFEINTextbox" DefaultText="Confirm FEIN" RenderOuterSpan="True" ID="ConfirmFEINLabel" Width="20%" />
		<asp:TextBox ID="ConfirmFEINTextbox" runat="server" MaxLength="10" TabIndex="2" Width="20%" />
		<asp:RequiredFieldValidator ID="ConfirmFEINRequired" runat="server" ControlToValidate="ConfirmFEINTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="EditFEIN" />
		<asp:CompareValidator ID="ConfirmFEINCompare" runat="server" ControlToValidate="ConfirmFEINTextbox" ControlToCompare="NewFEINTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="EditFEIN" />
	</p>
	<p style="text-align: right">
		<asp:Button ID="CancelButton" runat="server" CssClass="button4" OnClick="CancelButton_Clicked" CausesValidation="False" TabIndex="3" />
		<asp:Button ID="SaveButton" runat="server" CssClass="button3" OnClick="SaveButton_Clicked" CausesValidation="True" ValidationGroup="EditFEIN" TabIndex="4" />
	</p>
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" ClientIDMode="AutoID" OnOkCommand="Confirmation_OkCommand" OnCloseCommand="Confirmation_CloseCommand" />

<script type="text/javascript">
	Sys.Application.add_load(EditFEIN_PageLoad);

	function EditFEIN_PageLoad() {
		$("#<%= NewFEINTextbox.ClientID  %>").mask("99-9999999", { placeholder: " " });
		$("#<%= ConfirmFEINTextbox.ClientID  %>").mask("99-9999999", { placeholder: " " });
	}
</script>