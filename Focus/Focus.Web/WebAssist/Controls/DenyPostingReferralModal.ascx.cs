﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class DenyPostingReferralModal : UserControlBase
	{
		private long JobId
		{
			get { return GetViewStateValue<long>("DenyPostingReferralModal:JobId"); }
			set { SetViewStateValue("DenyPostingReferralModal:JobId", value); }
		}

		private long EmployeeId
		{
			get { return GetViewStateValue<long>("DenyPostingReferralModal:EmployeeId"); }
			set { SetViewStateValue("DenyPostingReferralModal:EmployeeId", value); }
		}

		private EmailTemplateView EmailTemplate
		{
			get { return GetViewStateValue<EmailTemplateView>("DenyPostingReferralModal:EmailTemplate"); }
			set { SetViewStateValue("DenyPostingReferralModal:EmailTemplate", value); }
		}

		private int LockVersion
		{
			get { return GetViewStateValue<int>("DenyPostingReferralModal:LockVersion"); }
			set { SetViewStateValue("DenyPostingReferralModal:LockVersion", value); }
		}

		private bool JobPostingFiltered
		{
			get { return GetViewStateValue<bool>("DenyPostingReferralModal:JobPostingFiltered"); }
			set { SetViewStateValue("DenyPostingReferralModal:JobPostingFiltered", value); }
		}

		private const string DenialReasonsPlaceHolder = "#DENIALREASONS#";
		private const string FilteredUrl = "?filtered=true";
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}
		}

		/// <summary>
		/// Shows the specified job id.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="filtered">Return to filtered list</param>
		public void Show(long jobId, int lockVersion, bool filtered = false)
		{
			JobId = jobId;
			LockVersion = lockVersion;
			JobPostingFiltered = filtered;

			GetEmailTemplate();

			if (EmailTemplate.IsNotNull())
			{
				var regex = new Regex(DenialReasonsPlaceHolder);
				var emailBodySplit = regex.Split(EmailTemplate.Body.Replace("\n", "<br />").Replace("\r", ""));
				TopEmailBodyLiteral.Text = emailBodySplit[0];
				BottomEmailBodyLiteral.Text = (emailBodySplit.Length > 1) ? emailBodySplit[1] : String.Empty;

				if (App.Settings.Theme == FocusThemes.Workforce)
				{
					CheckBoxDenialReasonsPlaceholder.Visible = true;
					cblDenialReasons.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobDenialReasons));
				}
				else
				{
					FreeTextDenialReasonPlaceholder.Visible = true;
				}

				ModalPopup.Show();
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SendMessageButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DenyAndSendMessageButton_Clicked(object sender, EventArgs e)
		{
			try
			{
				string denialReasons;
				var denialList = new List<KeyValuePair<long, string>>();

				if (App.Settings.Theme == FocusThemes.Workforce)
				{
					var otherId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobDenialReasons).Where(x => x.Key.Equals("JobDenialReasons.OtherReason")).Select(x => x.Id).FirstOrDefault();

					var query = (from ListItem checkbox in cblDenialReasons.Items
											 where checkbox.Selected && checkbox.Value.IsNotNullOrEmpty() && checkbox.Value != otherId.ToString()
											 select checkbox).ToList();

					// Create a list of the reasons to be displayed in the email

					var denialReasonList = query.Select(checkBox => checkBox.Text).ToList();

					var otherReason = OtherDenialReasonTextBox.TextTrimmed();

					if (otherReason.IsNotNullOrEmpty())
						denialReasonList.Add(otherReason);

					denialReasons = string.Join(Environment.NewLine, denialReasonList.Select(x => string.Concat("* ", x)));

					// Create a list of keyvaluepairs of the reason Ids and Other text for saving to the database

					denialList = query.Select(checkBox => new KeyValuePair<long, string>(Convert.ToInt64(checkBox.Value), string.Empty)).ToList();

					if (otherReason.IsNotNullOrEmpty())
					{
						var otherReasonEntry = new KeyValuePair<long, string>(otherId, otherReason);
						denialList.Add(otherReasonEntry);
					}

				}
				else
				{
					denialReasons = DenialReasonsTextBox.TextTrimmed();
				}

				ServiceClientLocator.JobClient(App).DenyPostingReferral(JobId, LockVersion, App.Settings.Theme == FocusThemes.Workforce ? denialList : null);

				var emailBody = EmailTemplate.Body.Replace(DenialReasonsPlaceHolder, denialReasons);
				ServiceClientLocator.EmployeeClient(App).EmailEmployee(EmployeeId, EmailTemplate.Subject, emailBody, BCCMeCheckBox.Checked, ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.DenyPostingReferral));
				ServiceClientLocator.CoreClient(App).SaveReferralEmail(EmailTemplate.Subject, emailBody, ApprovalStatuses.Rejected, null, null, JobId);

				ModalPopup.Hide();

				Confirmation.Show(CodeLocalise("ReferralDeniedConfirmation.Title", "Posting denied"),
																 CodeLocalise("ReferralDeniedConfirmation.Body", "The posting has been denied."),
																 CodeLocalise("CloseModal.Text", "OK"),
																 closeLink: JobPostingFiltered ? UrlBuilder.PostingReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.PostingReferrals());
			}
			catch (ServiceCallException ex)
			{
				Confirmation.Show(CodeLocalise("PostingDeniedError.Title", "Denial failed"),
					CodeLocalise("ReferralDeniedError.Body", string.Format("There was a problem denying the referral.<br /><br />{0}", ex.Message)),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink: JobPostingFiltered ? UrlBuilder.PostingReferrals().AddToEndOfString(FilteredUrl) : UrlBuilder.PostingReferrals());
			}
		}

		/// <summary>
		/// Handles the Clicked event of the CancelButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Clicked(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			DenyAndSendMessageButton.Text = CodeLocalise("DenyAndSendMessageButton.Text", "Deny posting");
			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			BCCMeCheckBox.Text = CodeLocalise("BCCMeCheckBox.Text", "email me a copy of this message");
			DenialReasonsRequired.ErrorMessage = CodeLocalise("DenialReasonsRequired.ErrorMessage", "Denial reasons required");
			OtherDeniedReasonRequired.ErrorMessage = CodeLocalise("DenialReasonsRequired.ErrorMessage", "Other reason description required");
			DenialReasonsCheckboxRequired.ErrorMessage = CodeLocalise("DenialReasonsCheckboxRequired.ErrorMessage", "Denial reasons required");
      OtherDenialReasonValidator.ErrorMessage = CodeLocalise("OtherDenialReasonValidator.ErrorMessage", "Other reason description must be 250 characters or less");
		}

		/// <summary>
		/// Builds the email body.
		/// </summary>
		/// <returns></returns>
		private void GetEmailTemplate()
		{
			var jobPostingReferral = ServiceClientLocator.JobClient(App).GetJobPostingReferral(JobId);

			if (!jobPostingReferral.IsNotNull())
			{
				Confirmation.Show(CodeLocalise("PostingApprovalError.Title", "Denial failed"),
					CodeLocalise("PostingApprovalError.Body", string.Format("There was a problem denying the referral.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.")),
					CodeLocalise("CloseModal.Text", "OK"),
					closeLink: UrlBuilder.PostingReferrals());

				return;
			}

			DenialHelpText.Text = jobPostingReferral.JobStatus == JobStatuses.OnHold
				? HtmlLocalise("SummaryOnHold.Text", "This will<ul><li>remove the posting from the approval queue and put it in the #BUSINESS#:LOWER's on-hold tab in #FOCUSTALENT#, and</li><li>email the #BUSINESS#:LOWER alerting them their posting was denied.</li></ul>")
				: HtmlLocalise("Summary.Text", "This will<ul><li>remove the posting from the approval queue and put it in the #BUSINESS#:LOWER's draft tab in #FOCUSTALENT#, and</li><li>email the #BUSINESS#:LOWER alerting them their posting was denied.</li></ul>");

			if (jobPostingReferral.EmployeeId.HasValue) EmployeeId = jobPostingReferral.EmployeeId.Value;
			var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";

			var templateValues = new EmailTemplateData
			{
				RecipientName = String.Format("{0} {1}", jobPostingReferral.EmployeeFirstName, jobPostingReferral.EmployeeLastName),
				JobTitle = jobPostingReferral.JobTitle,
				JobStatus = CodeLocalise(jobPostingReferral.JobStatus, true),
				FreeText = DenialReasonsPlaceHolder,
				SenderPhoneNumber = Regex.Replace(phoneNumber, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat),
				SenderEmailAddress = App.User.EmailAddress,
				SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName)
			};

			EmailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.DenyPostingReferral, templateValues);
		}
	}
}