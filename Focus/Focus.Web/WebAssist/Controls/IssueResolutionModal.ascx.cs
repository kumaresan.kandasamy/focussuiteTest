﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.WebAssist.Controls
{
  public partial class IssueResolutionModal : UserControlBase
	{
    #region Properties

    private long PersonId
    {
      get { return GetViewStateValue<long>("IssueResolutionModal:PersonId"); }
      set { SetViewStateValue("IssueResolutionModal:PersonId", value); }
    }

    #endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        Localise();
      }

      CheckBoxListValidator.ValidationGroup = NoteValidator.ValidationGroup = SaveButton.ValidationGroup = string.Concat(ClientID, "_IssueResolutionModalValidation");
      IssueResolutionModalPopup.BehaviorID = string.Concat(ClientID, "_IssueResolutionModalBehaviour");
    }

    #region Public methods

    /// <summary>
    /// Shows the specified person id.
    /// </summary>
    /// <param name="personId">The person id.</param>
    public void Show(long personId)
    {
      PersonId = personId;
      NoteTextBox.Text = string.Empty;
      Bind();
      IssueResolutionModalPopup.Show();
    }

    #endregion

    #region Control events

    /// <summary>
    /// Handles the Clicked event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void CancelButton_Clicked(object sender, EventArgs e)
    {
      IssueResolutionModalPopup.Hide();
    }

    /// <summary>
    /// Handles the Clicked event of the SaveButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void SaveButton_Clicked(object sender, EventArgs e)
    {
      // Get the resolved items
      var resolvedIssues = (from ListItem item in IssueList.Items
                                                where item.Selected
                                                select  (CandidateIssueType) Enum.Parse(typeof(CandidateIssueType), item.Value)).ToList();
      // Save the note
      ServiceClientLocator.CoreClient(App).SaveNoteReminder(
        new NoteReminderDto
          {
            CreatedBy = App.User.UserId,
            NoteReminderType = NoteReminderTypes.Note,
            Text = NoteTextBox.Text,
            EntityId = PersonId,
            EntityType = EntityTypes.JobSeeker,
            NoteType = NoteTypes.ResolvedIssues
          }, null);

      // Resolve the issues
      ServiceClientLocator.CandidateClient(App).ResolveCandidateIssues(PersonId, resolvedIssues);

      // Trigger issues resolved event
      OnIssuesResolved(new EventArgs());

      IssueResolutionModalPopup.Hide();
      if (resolvedIssues.Count == 1)
      {
        Confirmation.Show(
          CodeLocalise("IssueResolved.Header", "Issue resolved"),
          CodeLocalise("IssueResolved.Text", "The designated issue has been marked as resolved"),
          CodeLocalise("Ok.Text", "OK"));
      }
      else
      {
        Confirmation.Show(
          CodeLocalise("IssuesResolved.Header", "Issues resolved"),
          CodeLocalise("IssuesResolved.Text", "The designated issues have been marked as resolved"),
          CodeLocalise("Ok.Text", "OK"));
      }
    }

    #endregion

    #region Private methods

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      // Get candidate issues
      var issues = ServiceClientLocator.CandidateClient(App).GetCandidateAndIssues(PersonId);

      // Update modal title
      ModalTitle.Text = CodeLocalise("IssueResolutionModal.Title", "Resolve Issues For {0} {1}", issues.FirstName,
                                     issues.LastName);
      // Reset issue list
      IssueList.Items.Clear();

      // Display issues
      if (issues.InappropriateEmailAddress.GetValueOrDefault())
        IssueList.Items.AddEnum(CandidateIssueType.InappropriateEmailAddress, "Inappropriate email address");

      if (issues.NoLoginTriggered && App.Settings.NotLoggingInEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.NotLoggingIn, "Seeker not logging in");

      if (issues.NotClickingOnLeadsTriggered && App.Settings.NotClickingLeadsEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.NotClickingLeads, "Seeker not clicking on leads");

      if (issues.JobOfferRejectionTriggered && App.Settings.RejectingJobOffersEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.RejectingJobOffers, "Seeker rejecting job offers");

      if (issues.NotReportingToInterviewTriggered && App.Settings.NotReportingForInterviewEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.NotReportingForInterviews, "Seeker not reporting for interviews");

      if (issues.NotRespondingToEmployerInvitesTriggered && App.Settings.NotRespondingToEmployerInvitesEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.NotRespondingToEmployerInvites, "Seeker not responding to #BUSINESS#:LOWER invitations");

      if (issues.ShowingLowQualityMatchesTriggered && App.Settings.ShowingLowQualityMatchesEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.ShowingLowQualityMatches, "Showing low-quality matches");

      if (issues.PostingLowQualityResumeTriggered && App.Settings.PostingLowQualityResumeEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.PoorQualityResume, "Posting poor-quality resume");

      if (issues.PostHireFollowUpTriggered && App.Settings.PostHireFollowUpEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.SuggestingPostHireFollowUp, "Suggesting post-hire follow-up");

      if (issues.FollowUpRequested && App.Settings.FollowUpEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.RequiringFollowUp, "Requiring follow-up");

      if (issues.NotSearchingJobsTriggered && App.Settings.NotSearchingJobsEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.NotSearchingJobs, "Seeker not searching jobs");

      if (issues.GivingPositiveFeedback.GetValueOrDefault() && App.Settings.PositiveFeedbackCheckEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.GivingPositiveFeedback, "Giving positive survey feedback");

      if (issues.GivingNegativeFeedback.GetValueOrDefault() && App.Settings.NegativeFeedbackCheckEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.GivingNegativeFeedback, "Giving negative survey feedback");

      if (issues.GivingNegativeFeedback.GetValueOrDefault() && App.Settings.NegativeFeedbackCheckEnabled)
        IssueList.Items.AddEnum(CandidateIssueType.GivingNegativeFeedback, "Giving negative survey feedback");

			if( issues.HasExpiredAlienCertificationRegistration.GetValueOrDefault() && App.Settings.ExpiredAlienCertificationEnabled )
				IssueList.Items.AddEnum( CandidateIssueType.ExpiredAlienCertification, "Seeker with expired alien registration" );

      if (IssueList.Items.Count > 0)
				IssueListLit.Text = HtmlRequiredLabel(IssueList, "Issues.Label", "Issues");

    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      SaveButton.Text = CodeLocalise("IssueResolutionModal.SaveButton", "Save");
      CancelButton.Text = CodeLocalise("IssueResolutionModal.Cancel", "Cancel");
      NoteValidator.ErrorMessage = CodeLocalise("NoteValidationError.Text", "Please enter a note");
      CheckBoxListValidator.ErrorMessage = CodeLocalise("IssueCheckBoxListValidationError.Text", "Please select at least one issue to resolve");
    }

    #endregion

    #region Page event

    public event IssuesResolvedHandler IssuesResolved;

    /// <summary>
    /// Raises the <see cref="E:IssuesResolved" /> event.
    /// </summary>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    public void OnIssuesResolved(EventArgs e)
    {
      var handler = IssuesResolved;
      if (handler != null) handler(this, e);
    }

    public delegate void IssuesResolvedHandler(object o, EventArgs e);

    #endregion

  }
}