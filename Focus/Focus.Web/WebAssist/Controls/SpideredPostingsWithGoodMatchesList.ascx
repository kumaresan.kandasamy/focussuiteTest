﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SpideredPostingsWithGoodMatchesList.ascx.cs" Inherits="Focus.Web.WebAssist.Controls.SpideredPostingsWithGoodMatchesList" %>

<%@ Register src="~/Code/Controls/User/Pager.ascx" tagname="Pager" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/ComparePostingToResumeModal.ascx" tagName="ComparePostingToResumeModal" tagPrefix="uc" %>
<%@ Register src="~/WebAssist/Controls/PostingModal.ascx" tagName="PostingModal" tagPrefix="uc" %>

<asp:UpdatePanel ID="EmployerPostingsListUpdatePanel" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False">
	<ContentTemplate>
		<div class="FilterAndPagerDiv">
			<div class="FilterDiv">
				<table role="presentation">
					<tr>
						<td  style="width:40"><focus:LocalisedLabel runat="server" ID="FilterLabel" LocalisationKey="Filter.Label" DefaultText="Filter" AssociatedControlID="FilterTextBox" CssClass="bold"/></td>
						<td>
							<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="FilterTextBoxInFieldLabel" AssociatedControlID="FilterTextBox" LocalisationKey="FilterTextBox.InFiledLabel" DefaultText="#BUSINESS#:LOWER name" Width="200"/></span>
							<asp:TextBox ID="FilterTextBox" runat="server" MaxLength="100" />
							<asp:Button ID="FilterButton" runat="server" class="button1" CausesValidation="False" OnClick="FilterButton_Clicked" />
						</td>
					</tr>
				</table>
			</div>
			<div class="PagerDiv">
				<uc:Pager ID="EmployerPostingsListPager" runat="server" PagedControlID="EmployerPostingsListView" PageSize="10" DisplayRecordCount="true" OnDataPaged="EmployerPostingsListPager_DataPaged" />
			</div>
		</div>
		<div class="MainListDiv">
			<asp:ListView runat="server" ID="EmployerPostingsListView" ItemPlaceholderID="EmployerPostingsPlaceHolder" DataSourceID="EmployerPostingsDataSource" OnLayoutCreated="EmployerPostingsListView_LayoutCreated" 
									OnSorting="EmployerPostingsListView_Sorting" OnItemDataBound="EmployerPostingsListView_ItemDataBound"  OnItemCommand="EmployerPostingsListView_OnItemCommand" OnPreRender="EmployerPostingsListView_OnPreRender">
			
				<LayoutTemplate>
					 <table style="width:100%;" class="table">
							<tr>
								<th>
									<table role="presentation">
										<tr>
                      <td rowspan="2">
                         <asp:Literal runat="server" ID="EmployerNameHeaderLiteral" />
                      </td>
                      <td>
	                      
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="EmployerNameSortAscButton" CssClass="AscButton"
                            runat="server" CommandName="Sort" CommandArgument="employername asc" AlternateText="Sort Ascending" />
                           <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="EmployerNameSortDescButton" CssClass="DescButton"
                            runat="server" CommandName="Sort" CommandArgument="employername desc" AlternateText="Sort Descending" />
                      </td>
                    </tr>
									</table>
								</th>
								<th>
									<table role="presentation">
										<tr>
                      <td rowspan="2">
                         <asp:Literal runat="server" ID="JobTitleHeaderLiteral" />
                      </td>
                      <td>
                        <asp:ImageButton ID="JobTitleSortAscButton" CssClass="AscButton"
                            runat="server" CommandName="Sort" CommandArgument="jobtitle asc" />
                           <asp:ImageButton ID="JobTitleSortDescButton" CssClass="DescButton"
                            runat="server" CommandName="Sort" CommandArgument="jobtitle desc" />
                      </td>
                    </tr>
									</table>
								</th>
								<th>
									<table role="presentation">
										<tr>
                      <td rowspan="2">
                         <asp:Literal runat="server" ID="PostingDateHeaderLiteral" />
                      </td>
                      <td>
                        <asp:ImageButton ID="PostingDateSortAscButton" CssClass="AscButton"
                            runat="server" CommandName="Sort" CommandArgument="postingdate asc" />
                           <asp:ImageButton ID="PostingDateSortDescButton" CssClass="DescButton"
                            runat="server" CommandName="Sort" CommandArgument="postingdate desc" />
                      </td>
                    </tr>
									</table>
								</th>
								<th>
									<asp:Literal runat="server" ID="ActionsHeaderLiteral" />
                </th>
							</tr>
							<asp:PlaceHolder ID="EmployerPostingsPlaceHolder" runat="server" />
					 </table>
				</LayoutTemplate>
				<ItemTemplate>
					<tr id="EmployerRow" runat="server" ClientIDMode="AutoID">
						<td>
							<%# Eval("EmployerName") %>
						</td>
						<td>
							<asp:LinkButton ID="ViewPostingLinkButton" runat="server" OnCommand="ViewPostingLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="ViewPosting" ClientIDMode="AutoID"><%# Eval("JobTitle") %></asp:LinkButton><br/>
							<asp:LinkButton ID="GetMorePostingsWithGoodMatchesLinkButton" runat="server" CommandArgument='<%# Eval("EmployerName") %>' CommandName="GetPostings" ClientIDMode="AutoID" Visible="false" />
						</td>
						<td>
							<%#((Focus.Core.Models.Assist.SpideredEmployerModel)Container.DataItem).PostingDate.ToString("MMM dd, yyyy")%>
						</td>
						<td>
							<asp:Panel ID="ViewMatchesPanel" runat="server">
								<asp:LinkButton ID="ViewMatchesLinkButton" runat="server" OnCommand="ViewMatchesLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="ViewMatches" ClientIDMode="AutoID"><%= HtmlLocalise("ViewMatches.Text", "View Matches")%></asp:LinkButton>
							</asp:Panel>
							<asp:LinkButton ID="ContactEmployerLinkButton" runat="server" OnCommand="ContactEmployerLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="ContactEmployer" ClientIDMode="AutoID"><%= HtmlLocalise("ContactEmployer.Text", "Contact #BUSINESS#")%></asp:LinkButton>
						</td>
					</tr>
					<asp:Repeater ID="PostingsRepeater" runat="server">
						<ItemTemplate>
							<tr id="EmployerJobRow" runat="server" ClientIDMode="Static">
								<td>
								</td>
								<td>
									<asp:LinkButton ID="ViewPostingLinkButton" runat="server" OnCommand="ViewPostingLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="ViewPosting" ClientIDMode="AutoID"><%# Eval("JobTitle") %></asp:LinkButton><br/>
								</td>
								<td>
									<%#((Focus.Core.Models.Assist.SpideredPostingModel)Container.DataItem).PostingDate.ToString("MMM dd, yyyy")%>
								</td>
								<td>
									<asp:Panel ID="ViewMatchesPanel" runat="server">
										<asp:LinkButton ID="ViewMatchesLinkButton" runat="server" OnCommand="ViewMatchesLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="ViewMatches" ClientIDMode="AutoID"><%= HtmlLocalise("ViewMatches.Text", "View Matches")%></asp:LinkButton>
									</asp:Panel>
									<asp:LinkButton ID="ContactEmployerLinkButton" runat="server" OnCommand="ContactEmployerLinkButton_Command" CommandArgument='<%# Eval("LensPostingId") %>' CommandName="ContactEmployer" ClientIDMode="AutoID"><%= HtmlLocalise("ContactEmployer.Text", "Contact #BUSINESS#")%></asp:LinkButton>
								</td>
							</tr>
						</ItemTemplate>
					</asp:Repeater>
				</ItemTemplate>
			</asp:ListView>
			<asp:ObjectDataSource ID="EmployerPostingsDataSource" runat="server" TypeName="Focus.Web.WebAssist.Controls.SpideredPostingsWithGoodMatchesList" EnablePaging="true" SelectMethod="GetEmployerPostings" 
														SelectCountMethod="GetEmployerPostingsCount" SortParameterName="orderBy" OnSelecting="EmployerPostingsDataSource_Selecting" />
		</div>

		<uc:ComparePostingToResumeModal ID="ComparePostingToResumeModal" runat="server" OnModalClosed="ComparePostingToResumeModal_Closed" />
		<uc:PostingModal ID="PostingModal" runat="server" />

		<script type="text/javascript">

			var prm = Sys.WebForms.PageRequestManager.getInstance();

			prm.add_endRequest(function () {
				// Apply jQuery to infield labels
				$(".inFieldLabel > label, .inFieldLabelAlt > label").inFieldLabels();
			});
			
		</script>
	</ContentTemplate>
	<Triggers>
	</Triggers>
</asp:UpdatePanel>