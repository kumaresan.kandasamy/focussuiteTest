﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist.Controls
{
	public partial class WelcomeToAssistModal : UserControlBase
	{
		private bool ShowWelcomeMessage
		{
			get { return GetViewStateValue<bool>("WelcomeToAssistModal:ShowWelcomeMessage"); }
			set { SetViewStateValue("WelcomeToAssistModal:ShowWelcomeMessage", value); }
		}

		private bool ShowCurrentOfficeSelector
		{
			get { return GetViewStateValue<bool>("WelcomeToAssistModal:ShowCurrentOfficeSelector"); }
			set { SetViewStateValue("WelcomeToAssistModal:ShowCurrentOfficeSelector", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;

			if (App.Settings.OfficesEnabled)
			{
				SetSectionVisibility();

				if (ShowWelcomeMessage)
				{
					LocaliseUI();
					WelcomeContentPanel.Visible = true;
				}

				if (ShowCurrentOfficeSelector)
				{
					BindOffices();
					closeIcon.Visible = false;
					OfficeSelectorPanel.Visible = true;
				}

				if (ShowCurrentOfficeSelector && ShowWelcomeMessage)
				{
					OfficeSelectorTitle.Visible = false;
					OfficeSelectorPanel.Style.Add("margin-top", "20px");
				}

				// If welcome message is shown, don't show for another 24 hours
				if (ShowWelcomeMessage)
				{
          var cookie = new HttpCookie(string.Concat("WelcomeModal-", FormsAuthentication.FormsCookieName));
					var now = DateTime.Now;
					cookie.Value = Constants.WelcomeModalStateKeys.DoNotShow;
					cookie.Expires = now.AddDays(1);
					Response.Cookies.Add(cookie);
				}
				else if (ShowCurrentOfficeSelector)
				{
          App.SetCookieValue(string.Concat("WelcomeModal-", FormsAuthentication.FormsCookieName), Constants.WelcomeModalStateKeys.DoNotShow);
				}

				Show();
			}
			else
			{
				WelcomeModal.Hide();
			}
		}

		private void BindOffices()
		{
			// Clear items from list and add a default selection
			OfficeDropDownList.Items.Clear();

			if (App.User.PersonId == null) return;

			var offices = ServiceClientLocator.EmployerClient(App).GetOfficesPersonManages(App.User.PersonId.Value, true);
			var currentOfficeForPerson = ServiceClientLocator.EmployerClient(App).GetCurrentOfficeForPerson(App.User.PersonId.Value);

			OfficeDropDownList.DataSource = offices;
			OfficeDropDownList.DataValueField = "Id";
			OfficeDropDownList.DataTextField = "OfficeName";
			OfficeDropDownList.DataBind();
			OfficeDropDownList.Items.AddLocalisedTopDefault("Offices.Select.NoEdit", "- select -");

			if (currentOfficeForPerson != null && currentOfficeForPerson.StartTime.Date == DateTime.Today)
				OfficeDropDownList.SelectValue(currentOfficeForPerson.OfficeId.ToString());
		}

		/// <summary>
		/// Shows the modal.
		/// </summary>
		/// <param name="top">The top.</param>
		private void Show(int top = 210)
		{
			if (!ShowModal()) return;
			WelcomeModal.Y = top;
			WelcomeModal.Show();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			var welcomeBody = App.Settings.Theme == FocusThemes.Workforce
			                  	? @"<b>Welcome to #FOCUSASSIST#</b><br/>
                              <br/>
															Please click the appropriate tab to assist customers.
                              Your security permissions control your access to data views, Approval queues, Reports and various features in this application.
                              If you do not have security permissions for the features that you need for your work, please consult your manager to discuss access
															changes before contacting Security or Support.<br/>
                              <br/>
                              If you have problems navigating the site, please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a>. 
                              Our business hours are #SUPPORTHOURSOFBUSINESS#"
													: @"<b>Welcome to #FOCUSASSIST#</b><br/>
                              <br />
                              Please click the appropriate tab to assist customers.
                              Your security permissions control your access to data views, Approval queues, Reports and various features in this application.
                              If you do not have security permissions for the features that you need for your work, please consult your manager to discuss access
															changes before contacting Security or Support.<br/>
                              <br/>
                              If you have problems navigating the site, please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a>. 
                              Our business hours are #SUPPORTHOURSOFBUSINESS#";

			WelcomeContentLiteral.Text = CodeLocalise("WelcomeContentLiteral.Text", welcomeBody);
		}

		private bool ShowModal()
		{
			return (ShowCurrentOfficeSelector || ShowWelcomeMessage);
		}


		private void SetSectionVisibility()
		{
      var visibility = App.GetCookieValue(string.Concat("WelcomeModal-", FormsAuthentication.FormsCookieName), Constants.WelcomeModalStateKeys.ShowModal);
			ShowWelcomeMessage = (visibility == Constants.WelcomeModalStateKeys.ShowModal);
			ShowCurrentOfficeSelector = (visibility != Constants.WelcomeModalStateKeys.DoNotShow && OfficesAvailable());
		}

		private bool OfficesAvailable()
		{
			if (App.User.PersonId == null)
			{
				return false;
			}

			try
			{
				var offices = ServiceClientLocator.EmployerClient(App).GetOfficesPersonManages(App.User.PersonId.Value);
				// Save Current Office if user has only one
				if(offices.IsNotNullOrEmpty() && offices.Count == 1)
					ServiceClientLocator.EmployerClient(App).SaveCurrentOfficeForPerson(offices.FirstOrDefault().Id.Value, App.User.PersonId.Value);

				return offices != null && offices.Count > 1;
			}
			catch (Exception)
			{
				return false;
			}
		}

		protected void SaveCurrentOfficeClick(object sender, EventArgs e)
		{
			if (App.User.PersonId != null)
				ServiceClientLocator.EmployerClient(App).SaveCurrentOfficeForPerson(OfficeDropDownList.SelectedValueToLong(), App.User.PersonId.Value);
		}
	}
}