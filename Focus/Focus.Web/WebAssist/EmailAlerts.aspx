﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmailAlerts.aspx.cs" Inherits="Focus.Web.WebAssist.EmailAlerts" %>

<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagPrefix="uc" TagName="TabNavigation" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagPrefix="uc" TagName="ConfirmationModal"  %>
<%@ Register Src="Controls/AccountSettingsLinks.ascx" TagPrefix="uc" TagName="SettingsLinks" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <uc:SettingsLinks ID="SettingLinksMain" runat="server" />

  <h1><%= HtmlLocalise("Global.AccountSettings.Header", "Account settings")%></h1>
    
  <div style="width:100%">
  <asp:button ID="SetAlertsButton" runat="server"  CssClass="button2 right" OnClick="SetAlertsButton_Click" ValidationGroup="CheckBoxes" />
  <p style="font-weight:bold"><%= HtmlLocalise("EmailAlerts.Label", "Email Alerts")%></p>
  </div>

  <table role="presentation">
	<tr>
	    <td style="width:150px">
	        <%= HtmlLabel("SendEmailAlertsFor.Label", "Send email alerts for")%>
	    </td>
		<td>
			<asp:CheckBox ID="NewReferralRequestsBox" runat="server" ClientIDMode="Static" />
            &nbsp;
	    </td>
		<td>
			<asp:CheckBox ID="EmployerAccountRequestsBox" runat="server" ClientIDMode="Static" />
            &nbsp;
	    </td>
		<td>
			<asp:CheckBox ID="NewJobPostingsBox" runat="server" ClientIDMode="Static" />
            &nbsp;
		</td>
	</tr>
    <tr>
        <td colspan="3">
            &nbsp;
        </td>
    </tr>
	<tr>
		<td><%= HtmlLabel(AlertFrequencyDropDown,"Frequency.Label", "Frequency")%></td>
        <td colspan="3">
            <asp:DropDownList ID="AlertFrequencyDropDown" runat="server" ClientIDMode="Static"  />
        </td>
	</tr>
    </table>
    
	<%-- Confirmation Modal --%>
	<uc:confirmationmodal ID="ConfirmationModal" runat="server" />
    
  <script type="text/javascript">
    var checkBoxes = null;
    var dropDown = null;

    function CheckBoxClick() 
    {
      var checked = false;
      for (var index = 0; index < 3 && !checked; index++) 
      {
        if (checkBoxes[index].checked) 
        {
          checked = true;
        }
      }

      if (checked) 
      {
        dropDown.disabled = false;
      }
      else 
      {
        dropDown.disabled = true;
      }
    }

    $(document).ready
    (
		  function () 
		  {
		    dropDown = document.getElementById('<%= AlertFrequencyDropDown.ClientID %>');
		    
		    checkBoxes = new Array();
		    checkBoxes.push(document.getElementById('<%= NewReferralRequestsBox.ClientID %>'));
		    checkBoxes.push(document.getElementById('<%= EmployerAccountRequestsBox.ClientID %>'));
		    checkBoxes.push(document.getElementById('<%= NewJobPostingsBox.ClientID %>'));

		    for (var index = 0; index < 3; index++) 
		    {
		      $(checkBoxes[index]).click(CheckBoxClick);
		    }
		  }
    );
  </script> 

</asp:Content>
