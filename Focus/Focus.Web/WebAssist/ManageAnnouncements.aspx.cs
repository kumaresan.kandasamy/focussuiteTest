﻿using System;
using System.Security.Permissions;
using Focus.Core;
using Focus.Services;

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistBroadcastsAdministrator)]
	public partial class ManageAnnouncements : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("Page.Title", "Manage #FOCUSASSIST#-Announcements");

			if (!IsPostBack)
			{
				LocaliseUI();
				CreateAnnouncement.Bind();
			}
		}

		/// <summary>
		/// Handles the Clicked event of the NewAnnouncementLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void NewAnnouncementLink_Clicked(object sender, EventArgs e)
		{
			ResetNavigation();
			NewAnnouncementLink.Visible = false;
			NewAnnouncementLinkDisplayOnly.Visible = CreateAnnouncement.Visible = true;
			CreateAnnouncement.Bind();
		}

		/// <summary>
		/// Handles the Clicked event of the SavedAnnouncementsLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SavedAnnouncementsLink_Clicked(object sender, EventArgs e)
		{
			ResetNavigation();
			SavedAnnouncementsLink.Visible = false;
			SavedAnnouncementsLinkDisplayOnly.Visible = SavedAnnouncements.Visible = true;
			SavedAnnouncements.Bind();
		}

		/// <summary>
		/// Handles the Clicked event of the RecentAnnouncementsLink control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void RecentAnnouncementsLink_Clicked(object sender, EventArgs e)
		{
			ResetNavigation();
			RecentAnnouncementsLink.Visible = false;
			RecentAnnouncementsLinkDisplayOnly.Visible = RecentAnnouncements.Visible = true;
			RecentAnnouncements.Bind();
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			NewAnnouncementLinkDisplayOnly.Text = CodeLocalise("NewAnnouncement.LinkText", "New announcement");
			SavedAnnouncementsLinkDisplayOnly.Text = CodeLocalise("SavedAnnouncements.LinkText", "Saved announcements");
			RecentAnnouncementsLinkDisplayOnly.Text = CodeLocalise("RecentAnnouncements.LinkText", "Recently posted announcements");
		}

		/// <summary>
		/// Resets the navigation.
		/// </summary>
		private void ResetNavigation()
		{
			NewAnnouncementLink.Visible = SavedAnnouncementsLink.Visible = RecentAnnouncementsLink.Visible = true;

			NewAnnouncementLinkDisplayOnly.Visible = SavedAnnouncementsLinkDisplayOnly.Visible = RecentAnnouncementsLinkDisplayOnly.Visible = false;

			CreateAnnouncement.Visible = SavedAnnouncements.Visible = RecentAnnouncements.Visible = false;
		}
	}
}