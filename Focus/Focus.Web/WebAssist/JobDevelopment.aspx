﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="JobDevelopment.aspx.cs" Inherits="Focus.Web.WebAssist.JobDevelopment" %>

<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/AssistEmployersNavigation.ascx" TagName="AssistEmployersNavigation"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/MatchesToRecentPlacements.ascx" TagName="RecentPlacements"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/SpideredEmployersList.ascx" TagName="SpideredEmployersList"
    TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/SpideredPostingsWithGoodMatchesList.ascx"
    TagName="SpideredPostingsWithGoodMatchesList" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/EmployerPlacements.ascx" TagName="EmployerPlacements"
    TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .FilterAndPagerDiv
        {
            width: 100%;
            display: inline-block;
        }
        .FilterDiv
        {
            float: left;
        }
        .FilterDiv .bold
        {
            font-weight: bold;
        }
        .FilterDiv input[type='text']
        {
            width: 200px;
        }
        .PagerDiv
        {
            float: right;
        }
        .MainListDiv
        {
            width: 100%;
            margin-bottom: 20px;
        }
        #EmployerJobRow td
        {
            vertical-align: top;
        }
        #OfficeFilterRow
        {
            margin-top: 5px;
        }
        .childListItem
        {
            display: none;
        }
<%--
    UIS20180530: FVN-6085 To fix the 508 compalience. Also, found this property is not used anywhere.       
     .highlighted td
        {
            background-color: #dcdce7;
        }--%>
    </style>
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigation ID="TabNavigation1" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="AutoID">
    <table style="width: 100%;" role="presentation">
        <tr>
            <td style="vertical-align: top;">
                <uc:AssistEmployersNavigation ID="AssistEmployersNavigationMain" runat="server" />
            </td>
            <td style="vertical-align: top; text-align: right;">
                <asp:Button ID="CreateNewEmployerAndJobOrderButton" runat="server" class="button1"
                    OnClick="CreateNewEmployerAndJobOrderButton_Clicked" CausesValidation="false" />
            </td>
        </tr>
    </table>
    <br />
    <act:Accordion ID="AccordionCtrl" runat="server" CssClass="accordionTable" HeaderCssClass="singleAccordionTitle excludeFromAccordion off"
        HeaderSelectedCssClass="singleAccordionTitle excludeFromAccordion on" ContentCssClass="singleAccordionContent"
        FadeTransitions="false" FramesPerSecond="40" TransitionDuration="150" AutoSize="None"
        RequireOpenedPane="false" SuppressHeaderPostbacks="true" SelectedIndex="0">
        <Panes>
            <act:AccordionPane ID="AccordionPane1" runat="server">
                <Header>
                    <span class="plusMinusImage"></span>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a
                        href="#"><%= HtmlLocalise("RecentMatches.Label", "Matches to Recent Placements")%></a></span>
                </Header>
                <Content>
                    <asp:Panel ID="RecentMatchesPanel" runat="server">
                        <uc:RecentPlacements ID="RecentPlacementsUc" runat="server" />
                        <br />
                    </asp:Panel>
                </Content>
            </act:AccordionPane>
            <act:AccordionPane ID="AccordionPane2" runat="server">
                <Header>
                    <span class="plusMinusImage"></span>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a
                        href="#"><%= HtmlLocalise("SpideredJobs.Label", "Spidered jobs with good matches")%></a></span>
                </Header>
                <Content>
                    <asp:Panel ID="SpideredJobsPanel" runat="server">
                        <uc:SpideredPostingsWithGoodMatchesList ID="SpideredJobWithGoodMatchesList" runat="server" />
                    </asp:Panel>
                </Content>
            </act:AccordionPane>
            <act:AccordionPane ID="AccordionPane3" runat="server">
                <Header>
                    <span class="plusMinusImage"></span>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a
                        href="#"><%= HtmlLocalise("EmployerJobs.Label", "#BUSINESSES# with many spidered jobs")%></a></span>
                </Header>
                <Content>
                    <asp:Panel ID="EmployerSpideredJobsPanel" runat="server">
                        <uc:SpideredEmployersList ID="SpideredEmployersList" runat="server" />
                    </asp:Panel>
                </Content>
            </act:AccordionPane>
            <act:AccordionPane ID="AccordionPane4" runat="server">
                <Header>
                    <span class="plusMinusImage"></span>&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a
                        href="#"><%= HtmlLocalise("EmployerPlacements.Label", "#BUSINESSES# with many placements")%></a></span>
                </Header>
                <Content>
                    <asp:Panel ID="EmployerPlacementsPanel" runat="server">
                        <uc:EmployerPlacements ID="EmployerPlacementsList" runat="server" />
                    </asp:Panel>
                </Content>
            </act:AccordionPane>
        </Panes>
    </act:Accordion>
</asp:Content>
