﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Code;
using Focus.Core;
using Focus.Core.Settings;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Models.Assist;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Focus.Web.Controllers.WebAssist;
using Focus.Web.WebAssist.Controls;
using Constants = Focus.Core.Constants;

using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployerAccountApprover)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployerAccountApprovalViewer)]
	public partial class EmployerReferral : PageBase
	{
		#region Properties
		
		protected ApprovalType EmployerApprovalType
		{
			get { return GetViewStateValue<ApprovalType>("EmployerReferral:EmployerApprovalType"); }
			set { SetViewStateValue("EmployerReferral:EmployerApprovalType", value); }
		}

		private ApprovalStatuses ApprovalStatus
		{
			get { return GetViewStateValue<ApprovalStatuses>("EmployerReferral:ApprovalStatus"); }
			set { SetViewStateValue("EmployerReferral:ApprovalStatus", value); }
		}

		protected long EntityId
		{
			get { return GetViewStateValue<long>("EmployerReferral:EntityId"); }
			set { SetViewStateValue("EmployerReferral:EntityId", value); }
		}

		protected string LockVersion
		{
			get { return GetViewStateValue<string>("EmployerReferral:LockVersion"); }
			set { SetViewStateValue("EmployerReferral:LockVersion", value); }
		}

		protected long EmployeeId
		{
			get { return GetViewStateValue<long>("EmployerReferral:EmployeeId"); }
			set { SetViewStateValue("EmployerReferral:EmployeeId", value); }
		}
		
		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		/// <value>The user id.</value>
		protected long EmployerId
		{
			get { return GetViewStateValue<long>("EmployerReferral:EmployerId"); }
			set { SetViewStateValue("EmployerReferral:EmployerId", value); }
		}

		protected long BusinessUnitId
		{
			get { return GetViewStateValue<long>("EmployerReferral:BusinessUnitId"); }
			set { SetViewStateValue("EmployerReferral:BusinessUnitId", value); }
		}

		protected List<string> RedProfanityWords
		{
			get { return GetViewStateValue<List<string>>("EmployerReferral:RedProfanityWords"); }
			set { SetViewStateValue("EmployerReferral:RedProfanityWords", value); }
		}

		protected List<string> YellowProfanityWords
		{
			get { return GetViewStateValue<List<string>>("EmployerReferral:YellowProfanityWords"); }
			set { SetViewStateValue("EmployerReferral:YellowProfanityWords", value); }
		}

		protected string BusinessUnitName
		{
			get { return GetViewStateValue<string>("EmployerReferral:BusinessUnitName"); }
			set { SetViewStateValue("EmployerReferral:BusinessUnitName", value); }
		}

		private bool _employerFilterApplied;

		private const string FilteredURL = "?filtered=true";

		/// <summary>
		/// Gets the page controller.
		/// </summary>
		/// <value>
		/// The page controller.
		/// </value>
		protected EmployerReferralController PageController
		{
			get { return PageControllerBaseProperty as EmployerReferralController; }
		}

		/// <summary>
		/// Registers the page controller.
		/// </summary>
		/// <returns></returns>
		protected override IPageController RegisterPageController()
		{
			return new EmployerReferralController(App);
		}

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("Page.Title", "Approve #BUSINESS#:LOWER account request");

			if (Request.QueryString["filtered"] != null) _employerFilterApplied = Convert.ToBoolean(Request.QueryString["filtered"]);

			if (!IsPostBack)
			{
				if (Page.RouteData.Values.ContainsKey("id"))
				{
					// Get the Employee Id
					long id;
					long.TryParse(Page.RouteData.Values["id"].ToString(), out id);

					var url = Request.Url.AbsoluteUri;

					if (url.Contains("employer"))
						EmployerApprovalType = ApprovalType.Employer;

					if (url.Contains("businessunit"))
						EmployerApprovalType = ApprovalType.BusinessUnit;

					if (url.Contains("hiringmanager"))
						EmployerApprovalType = ApprovalType.HiringManager;

					EntityId = id;
                    Page.Title = CodeLocalise("Page.Title", "Approve #BUSINESS#:LOWER account request-"+EntityId );
				}

				ApplyTheme();
				ApplyBranding();
				LocaliseUI();
				Bind();

				if (App.GetSessionValue<string>("EmployerReferral:Confirmation") == "OK")
				{
					Confirmation.Show(CodeLocalise("ReferralEditEmployerConfirmation.Title", "#BUSINESS# account amended"),
														CodeLocalise("ReferralEditEmployerConfirmation.Body", "The #BUSINESS#:LOWER account has been amended."),
														CodeLocalise("CloseModal.Text", "OK"));

					App.RemoveSessionValue("EmployerReferral:Confirmation");
				}
				else if (App.GetSessionValue<string>("EmployerReferralContact:Confirmation") == "OK")
				{
					Confirmation.Show(CodeLocalise("ReferralEditEmployerConfirmation.Title", "Hiring manager amended"),
														CodeLocalise("ReferralEditEmployerConfirmation.Body", "The hiring manager has been amended."),
														CodeLocalise("CloseModal.Text", "OK"));

					App.RemoveSessionValue("EmployerReferralContact:Confirmation");
				}

				ConfirmationUpdatePanel.Update();

				ApprovalButtonsCell.Visible = App.User.IsInRole(Constants.RoleKeys.AssistEmployerAccountApprover);

				SetReturnUrl();
			}
		}

		/// <summary>
		/// Handles the Clicked event of the ApproveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ApproveButton_Clicked(object sender, EventArgs e)
		{
			var id = EntityId;
			switch (EmployerApprovalType)
			{
				case ApprovalType.Employer:
					id = EmployerId;
					break;
				case ApprovalType.BusinessUnit:
					id = BusinessUnitId;
					break;
			}

			try
			{
				// Need to get the view before we approve as when the status is approved the record is removed from the database view
				var employerReferralView = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(id);

				var response = ServiceClientLocator.EmployeeClient(App).ApproveEmployerAccountReferral(EmployeeId, BusinessUnitId, LockVersion, EmployerApprovalType);
				switch (response)
				{
					case ApproveReferralResponseType.ParentCompanyLinkedToBusinessUnitNotApproved:
						Confirmation.Show(CodeLocalise("ReferralDeniedBusinessUnit.Title", "Unable to approve"),
															CodeLocalise("ReferralDeniedBusinessUnit.Body", "The parent #BUSINESS#:LOWER linked to this business unit has not yet been approved. Please review the parent #BUSINESS#:LOWER account request prior to taking action on this request."),
															CodeLocalise("CloseModal.Text", "OK"),
															closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
						ConfirmationUpdatePanel.Update();
						return;
					case ApproveReferralResponseType.ParentCompanyLinkedToHiringManagerNotApproved:
						Confirmation.Show(CodeLocalise("ReferralDeniedHiringManager.Title", "Unable to approve hiring manager"),
															CodeLocalise("ReferralDeniedHiringManager.Body", "The #BUSINESS#:LOWER linked to this account request has not yet been approved. Please review the #BUSINESS#:LOWER account request prior to approving this request."),
															CodeLocalise("CloseModal.Text", "OK"),
															closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
						ConfirmationUpdatePanel.Update();
						return;
				}


				if (employerReferralView.IsNotNull())
				{

					var emailTemplate = GetApproveEmployerReferralEmail(employerReferralView);
					var senderAddress = ServiceClientLocator.CoreClient(App).GetSenderEmailAddressForTemplate(EmailTemplateTypes.ApproveEmployerAccountReferral);

					ServiceClientLocator.EmployeeClient(App).EmailEmployee(employerReferralView.EmployeeId, emailTemplate.Subject, emailTemplate.Body, false, senderAddress);

					ServiceClientLocator.CoreClient(App).SaveReferralEmail(emailTemplate.Subject, emailTemplate.Body, ApprovalStatuses.OnHold, employerReferralView.EmployeeId);
				}
				if (EmployerApprovalType == ApprovalType.HiringManager)
				{
					Confirmation.Show(CodeLocalise("ReferralApprovedConfirmationHiringManager.Title", "Hiring manager account approved"),
														CodeLocalise("ReferralApprovedConfirmationHiringManager.Body", "The hiring manager account has been approved."),
														CodeLocalise("CloseModal.Text", "OK"),
														closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
				}
				else
				{
					Confirmation.Show(CodeLocalise("ReferralApprovedConfirmation.Title", "#BUSINESS# account approved"),
														CodeLocalise("ReferralApprovedConfirmation.Body", "The #BUSINESS#:LOWER account has been approved."),
														CodeLocalise("CloseModal.Text", "OK"),
														closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
				}
				ConfirmationUpdatePanel.Update();
			}
			catch (ServiceCallException ex)
			{
				var message = (EmployerApprovalType == ApprovalType.HiringManager)
												? CodeLocalise("ReferralApprovalExceptionHiringManager.Body", "There was a problem approving the hiring manager.<br /><br />{0}", ex.Message)
												: CodeLocalise("ReferralApprovalExceptionExceptionEmployer.Body", "There was a problem approving the #BUSINESS#:LOWER.<br /><br />{0}", ex.Message);

				Confirmation.Show(CodeLocalise("ReferralApprovalError.Title", "Approval failed"),
													message,
													CodeLocalise("CloseModal.Text", "OK"),
													closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());

				ConfirmationUpdatePanel.Update();
			}
		}

		/// <summary>
		/// Handles the Clicked event of the DenyButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void DenyButton_Clicked(object sender, EventArgs e)
		{
			DenyEmployerReferral.Show(EmployeeId, EmployerApprovalType, EmployerId, BusinessUnitId, LockVersion, _employerFilterApplied);
		}

		/// <summary>
		/// Handles the Clicked event of the HoldButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void HoldButton_Clicked(object sender, EventArgs e)
		{
			HoldEmployerReferral.Show(EmployeeId, EmployerApprovalType, EmployerId, BusinessUnitId, LockVersion, _employerFilterApplied);
		}

		/// <summary>
		/// Handles the Clicked event of the EditButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EditButton_Clicked(object sender, EventArgs e)
		{
			var id = EntityId;
			switch (EmployerApprovalType)
			{
				case ApprovalType.Employer:
					id = EmployerId;
					break;
				case ApprovalType.BusinessUnit:
					id = BusinessUnitId;
					break;
			}

			//Can this record still be located
			var employerReferralView = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(id);

			var employer = ServiceClientLocator.EmployerClient(App).GetEmployer(EmployerId);
			
			//if the record is not null then carry out the requested action
			//Other wise this record has had it's status changed by another user and therefore further edits are invalid
			if (employer.IsNotNull())
			{
				
				EditCompany.Visible = true;

				EditCompany.Show(EmployerId, BusinessUnitId, employer.LockVersion);
			}
			else
			{

				var message = CodeLocalise("ApprovalStatusChanged.EditError", "There was a problem editing the record.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.");

				Confirmation.Show(CodeLocalise("EditButton.Text", "Edit #BUSINESS#"),
													message,
													CodeLocalise("CloseModal.Text", "OK"),
													closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
			}
		}

		/// <summary>
		/// Handles the Clicked event of the EditContactButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void EditContactButton_Clicked(object sender, EventArgs e)
		{
			var id = EntityId;
			switch (EmployerApprovalType)
			{
				case ApprovalType.Employer:
					id = EmployerId;
					break;
				case ApprovalType.BusinessUnit:
					id = BusinessUnitId;
					break;
			}

			//Can this record still be located
			var employerReferralView = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(id);

			//if the record is not null then carry out the requested action
			//Other wise this record has had it's status changed by another user and therefore further edits are invalid
			if (employerReferralView.IsNotNull() && LockVersion == employerReferralView.AccountLockVersion)
			{
				var employee = ServiceClientLocator.EmployeeClient(App).GetEmployee(EmployeeId);
				var user = ServiceClientLocator.AccountClient(App).GetUserDetails(0, employee.PersonId);
				
				string lockVersion = !String.IsNullOrEmpty(employerReferralView.AccountLockVersion)
					? (employerReferralView.AccountLockVersion.Split('.')[2] ?? "0")
					: "0";
				UpdateContactInformationModal.Show(user.UserId, BusinessUnitName, Convert.ToInt32(lockVersion));
				UpdateContactInformationModal.Visible = true;
			}
			else
			{

				var message = CodeLocalise("ApprovalStatusChanged.EditError", "There was a problem editing the record.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.");

				Confirmation.Show(CodeLocalise("EditButton.Text", "Edit hiring contact"),
													message,
													CodeLocalise("CloseModal.Text", "OK"),
													closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
			}

		}

		/// <summary>
		/// Handles the OnClick event of the EditFEINButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void EditFEINButton_OnClick(object sender, EventArgs e)
		{

			var id = EntityId;
			switch (EmployerApprovalType)
			{
				case ApprovalType.Employer:
					id = EmployerId;
					break;
				case ApprovalType.BusinessUnit:
					id = BusinessUnitId;
					break;
			}

			//Can this record still be located
			var employerReferralView = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(id);

			//if the record is not null then carry out the requested action
			//Other wise this record has had it's status changed by another user and therefore further edits are invalid
			if (employerReferralView.IsNotNull() && LockVersion == employerReferralView.AccountLockVersion)
			{
				var lockVersion = int.Parse(LockVersion.Split(new char[] { '.' })[0]);
				EditFEINModal.Show(BusinessUnitId, lockVersion);
			}
			else
			{

				var message = CodeLocalise("ApprovalStatusChanged.EditError", "There was a problem editing the record.<br /><br />Another user has recently updated this record. Please exit the record and return to it to see the changes made.");

					Confirmation.Show(CodeLocalise("ModalTitle.Text", "Edit FEIN"),
														message,
														CodeLocalise("CloseModal.Text", "OK"),
														closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
			}
		}

		/// <summary>
		/// Handles the FEINChanged event of the EditFEINModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void EditFEINModal_FEINChanged(object sender, FEINChangedEventArgs e)
		{
			switch (e.Action)
			{
				case FEINChangeActions.NonPrimaryBusinessUnitExistingFEIN:
				case FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings:
				case FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings:
					Response.Redirect(UrlBuilder.EmployerReferral(EmployeeId));
					break;
				default:
					RedirectToSelf();
					break;
			}
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			var referral = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(EntityId);
			if (referral.IsNull())
			{
				Response.Redirect(_employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
				return;
			}

			LockVersion = referral.AccountLockVersion;

			BindNotesReminders(referral);

			EmployeeId = referral.EmployeeId;
			EmployerId = referral.EmployerId;
			BusinessUnitId = referral.BusinessUnitId;

			var descriptions = ServiceClientLocator.EmployerClient(App).GetBusinessUnitDescriptions(BusinessUnitId);
			var inappropriateWords = GetInappropriateWords(referral, descriptions);

			EmployerProfileLink.NavigateUrl = UrlBuilder.AssistEmployerProfile(BusinessUnitId);

			BusinessUnitName = referral.BusinessUnitName;

			if (referral.EmployerName.IsNotNullOrEmpty())
				EmployerNameLiteral.Text = EmployerName2Literal.Text = PrimaryCompanyLiteral.Text = GetTextWithProfanitiesHighlighted(referral.BusinessUnitName);

			if (LegalNamePlaceHolder.Visible)
			{
				LegalNameLabel.Text = referral.BusinessUnitLegalName;
				EmployerNameLabel.Text = EmployerNameLiteral.Text;
			}

			if (referral.EmployerAddressLine1.IsNotNullOrEmpty()) EmployerAddress1Literal.Text = GetTextWithProfanitiesHighlighted(referral.EmployerAddressLine1 + (referral.EmployerAddressLine2.IsNotNullOrEmpty() ? "<br/>" + referral.EmployerAddressLine2 : ""));
			EmployerAddress2Literal.Text = GetTextWithProfanitiesHighlighted(BuildAddressLine2(referral.EmployerAddressTownCity, referral.EmployerAddressStateId, referral.EmployerAddressPostcodeZip));
			EmployerPublicTransportAvailableLiteral.Text = referral.EmployerPublicTransitAccessible ? CodeLocalise("Global.Yes", "Yes") : CodeLocalise("Global.No", "No");

			if (referral.PrimaryPhone.IsNotNullOrEmpty())
			{
				EmployerPhoneNumberLiteral.Text = GetTextWithProfanitiesHighlighted(Regex.Replace(referral.PrimaryPhone, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
				EmployerPhoneTypeLiteral.Text = referral.PrimaryPhoneType;
			}
			else
			{
				EmployerPhoneRow.Visible = false;
			}

			if (referral.AlternatePhone1.IsNotNullOrEmpty())
			{
				EmployerAltPhone1Literal.Text = GetTextWithProfanitiesHighlighted(Regex.Replace(referral.AlternatePhone1, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
				EmployerAltPhoneType1Literal.Text = referral.AlternatePhone1Type;
			}
			else
			{
				EmployerPhoneType1Row.Visible = false;
			}

			if (referral.AlternatePhone2.IsNotNullOrEmpty())
			{
				EmployerAltPhone2Literal.Text = GetTextWithProfanitiesHighlighted(Regex.Replace(referral.AlternatePhone2, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
				EmployerAltPhoneType2Literal.Text = referral.AlternatePhone2Type;
			}
			else
			{
				EmployerPhoneType2Row.Visible = false;
			}
			if (referral.EmployerUrl.IsNotNullOrEmpty()) EmployerUrlLiteral.Text = GetTextWithProfanitiesHighlighted(referral.EmployerUrl);

			if (referral.EmployerFederalEmployerIdentificationNumber.IsNotNullOrEmpty())
				ShowFEIN(referral.EmployerFederalEmployerIdentificationNumber);

			if (referral.EmployerStateEmployerIdentificationNumber.IsNotNullOrEmpty()) EmployerSEINLiteral.Text = GetTextWithProfanitiesHighlighted(referral.EmployerStateEmployerIdentificationNumber);
			if (referral.EmployerIndustrialClassification.IsNotNullOrEmpty()) EmployerIndustryClassificationLiteral.Text = GetTextWithProfanitiesHighlighted(referral.EmployerIndustrialClassification);


			#region Contact

			EmployeeNameLiteral.Text = GetTextWithProfanitiesHighlighted(BuildFullName(referral.EmployeeFirstName, referral.EmployeeLastName, referral.Suffix));
			if (referral.EmployeeAddressLine1.IsNotNullOrEmpty()) EmployeeAddress1Literal.Text = GetTextWithProfanitiesHighlighted(referral.EmployeeAddressLine1 + (referral.EmployeeAddressLine2.IsNotNullOrEmpty() ? "<br/>" + referral.EmployeeAddressLine2 : ""));
			EmployeeAddress2Literal.Text = GetTextWithProfanitiesHighlighted(BuildAddressLine2(referral.EmployeeAddressTownCity, referral.EmployeeAddressStateId, referral.EmployeeAddressPostcodeZip));
			if (referral.EmployeeEmailAddress.IsNotNullOrEmpty()) EmployeeEmailAddressLiteral.Text = GetTextWithProfanitiesHighlighted(referral.EmployeeEmailAddress);

			var employee = ServiceClientLocator.EmployeeClient(App).GetUserDetails(referral.EmployeeId);

			if (employee.PrimaryPhoneNumber.IsNotNull())
			{
				EmployeePhoneNumberLiteral.Text = GetTextWithProfanitiesHighlighted(Regex.Replace(employee.PrimaryPhoneNumber.Number, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
				EmployeePhoneTypeLiteral.Text = CodeLocalise(employee.PrimaryPhoneNumber.PhoneType);
			}
			else
			{
				EmployeePhoneRow.Visible = false;
			}

			if (employee.AlternatePhoneNumber1.IsNotNull())
			{
				EmployeeAltPhone1Literal.Text = GetTextWithProfanitiesHighlighted(Regex.Replace(employee.AlternatePhoneNumber1.Number, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
				EmployeeAltPhoneType1Literal.Text = CodeLocalise(employee.AlternatePhoneNumber1.PhoneType);
			}
			else
			{
				EmployeePhoneType1Row.Visible = false;
			}

			if (employee.AlternatePhoneNumber2.IsNotNull())
			{
				EmployeeAltPhone2Literal.Text = GetTextWithProfanitiesHighlighted(Regex.Replace(employee.AlternatePhoneNumber2.Number, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat));
				EmployeeAltPhoneType2Literal.Text = CodeLocalise(employee.AlternatePhoneNumber2.PhoneType);
			}
			else
			{
				EmployeePhoneType2Row.Visible = false;
			}

			#endregion

			#region Business unit

			if (referral.BusinessUnitName.IsNotNullOrEmpty())
				EmployerNameLiteral.Text = EmployerName2Literal.Text = GetTextWithProfanitiesHighlighted(referral.BusinessUnitName);

			var otherDescriptions = descriptions.Where(desc => !desc.IsPrimary).ToList();
			var mainDescription = descriptions.FirstOrDefault(desc => desc.IsPrimary);

			var mainText = mainDescription.IsNull() ? string.Empty : mainDescription.Description;
			var mainCensored = GetTextWithProfanitiesHighlighted(mainText, RedProfanityWords, YellowProfanityWords);

			var descriptionText = new StringBuilder();
			if (otherDescriptions.IsNullOrEmpty())
			{
				descriptionText.Append(mainCensored);
			}
			else
			{
				descriptionText.Append("<ul>");
				descriptionText.Append("<li>").Append(mainCensored).Append("</li>");
				otherDescriptions.ForEach(desc =>
				{
					var censored = GetTextWithProfanitiesHighlighted(desc.Description, RedProfanityWords, YellowProfanityWords);
					descriptionText.Append("<li>").Append(censored).Append("</li>");
				});
                descriptionText.Append("</ul>");
			}

			BusinessDescriptionLiteral.Text = descriptionText.ToString().Replace("\n", "<br />");

			#endregion

			BindApprovalStatus(referral);
			BindInappropriateWords(inappropriateWords);
			BindPendingPostings();
		}

		/// <summary>
		/// Shows the FEIN for the company
		/// </summary>
		/// <param name="fein">The FEIN</param>
		private void ShowFEIN(string fein)
		{
			var feinRegEx = new Regex(App.Settings.FEINRegExPattern);

			EmployerFEINLiteral.Text = feinRegEx.IsMatch(fein)
																	 ? GetTextWithProfanitiesHighlighted(fein)
																	 : CodeLocalise("EmployerFEINLiteral.Text", "No FEIN stored for this #BUSINESS#:LOWER");
		}

		/// <summary>
		/// Handles the Command event of the PendingPostingsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.DataListCommandEventArgs"/> instance containing the event data.</param>
		protected void Item_Command(object sender, DataListCommandEventArgs e)
		{
			long jobId;
			long.TryParse(PendingPostingsList.DataKeys[e.Item.ItemIndex].ToString(), out jobId);
			var job = ServiceClientLocator.JobClient(App).GetJob(jobId);
			Handle(PageController.PreviewPosting(job));
		}

		/// <summary>
		/// Displays details of the account referral
		/// </summary>
		/// <param name="referral"></param>
		private void BindApprovalStatus(EmployerAccountReferralViewDto referral)
		{
			ApprovalStatus = GetApprovalStatus(referral);

			if (ApprovalStatus == ApprovalStatuses.OnHold || ApprovalStatus == ApprovalStatuses.Rejected)
			{
				//HoldButton.Visible = false;

				AccountApprovalStatusLabel.Text = HtmlLocalise("NewRequest.Text", ApprovalStatus == ApprovalStatuses.OnHold ? "On Hold" : "Denied");

				if (referral.ApprovalStatusChangedBy.IsNotNull())
				{
					var user = ServiceClientLocator.AccountClient(App).GetUserDetails(referral.ApprovalStatusChangedBy.Value);

					AccountApprovalStatusDetails.Text = string.Format(ApprovalStatus == ApprovalStatuses.OnHold ? "Placed on hold by {0} {1} on {2}" : "Denied by {0} {1} on {2}",
																														user.PersonDetails.FirstName,
																														user.PersonDetails.LastName,
																														referral.ApprovalStatusChangedOn);
				}

				var emailText = GetEmail(referral);

				if (emailText.IsNullOrEmpty())
					EmailPlaceHolder.Visible = false;
				else
					HoldEmailTextBox.Text = emailText;
			}
			else
			{
				AccountApprovalStatusLabel.Text = HtmlLocalise("NewRequest.Text", "New request");
				AccountApprovalStatusDetails.Visible = false;
				EmailPlaceHolder.Visible = false;
			}

			if (ApprovalStatus == ApprovalStatuses.Rejected)
			{ 
				DenyButton.Visible = false; 
			}
			else if (ApprovalStatus == ApprovalStatuses.OnHold)
			{
				HoldButton.Visible = false;
			}
		}

		/// <summary>
		/// Gets the approval status of a referral (which can be for employer, business unit or employee)
		/// </summary>
		/// <param name="referral">The referral</param>
		/// <returns>The approval status</returns>
		private ApprovalStatuses GetApprovalStatus(EmployerAccountReferralViewDto referral)
		{
			var approvalStatus = ApprovalStatuses.None;
			switch (referral.TypeOfApproval)
			{
				case (int)ApprovalType.Employer:
					approvalStatus = (ApprovalStatuses)referral.EmployerApprovalStatus;
					break;
				case (int)ApprovalType.BusinessUnit:
					approvalStatus = (ApprovalStatuses)referral.BusinessUnitApprovalStatus;
					break;
				case (int)ApprovalType.HiringManager:
					approvalStatus = (ApprovalStatuses)referral.EmployeeApprovalStatus;
					break;
			}

			return approvalStatus;
		}

		/// <summary>
		/// Gets the email sent to place the employee on hold
		/// </summary>
		/// <returns></returns>
		private string GetEmail(EmployerAccountReferralViewDto referral)
		{
			var emails = ServiceClientLocator.CoreClient(App).GetReferralEmails(referral.EmployeeId, null, (ApprovalStatuses)referral.EmployeeApprovalStatus, 1);

			return emails.Select(email => email.EmailText).FirstOrDefault();
		}

		/// <summary>
		/// Binds the pending postings.
		/// </summary>
		public void BindPendingPostings()
		{
            var approvalChecks = App.Settings.ApprovalChecksForCustomerCreatedJobs;
			var pendingPostings = ServiceClientLocator.JobClient(App).GetEmployeesPendingPostings(EmployeeId);
            List<PendingJobIssuesModel> JobList = new List<PendingJobIssuesModel>();
            foreach (var posting in pendingPostings)
            {
                string issues = null;
                if ((posting.CourtOrderedAffirmativeAction.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CourtOrdered))
                                || (posting.FederalContractor.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.FederalContractor))
                                || (posting.ForeignLabourCertificationH2A.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2A))
                                || (posting.ForeignLabourCertificationH2B.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2B))
                                || (posting.ForeignLabourCertificationOther.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborOther))
                                || (posting.IsCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionBased))
                                || (posting.IsSalaryAndCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionSalary))
                                || (posting.SuitableForHomeWorker.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.HomeBased))
                                || (posting.JobLocationType == JobLocationTypes.NoFixedLocation && approvalChecks.Contains(JobApprovalCheck.HomeBased)) // No Fixed Location is linked to HomeBased job, so share same config
                                || (posting.CreditCheckRequired.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CreditCheck))
                                || (posting.JobSpecialRequirement.IsNotNullOrEmpty() && approvalChecks.Contains(JobApprovalCheck.SpecialRequirements))
                                || (posting.HasCheckedCriminalRecordExclusion.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CriminalBackground)) || posting.RedProfanityWords.IsNotNullOrEmpty() || posting.YellowProfanityWords.IsNotNullOrEmpty())
                {
                    if (posting.RedProfanityWords.IsNotNullOrEmpty())
                        issues += " - Job has red-word issues";
                    if (posting.YellowProfanityWords.IsNotNullOrEmpty())
                        issues += " - Job has yellow-word issues";
                    if (posting.CourtOrderedAffirmativeAction.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CourtOrdered))
                        issues += " - Court-ordered affirmative action jobs require approval";
                    if (posting.FederalContractor.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.FederalContractor))
                        issues += " - Federal contractor jobs require approval";
                    if (posting.ForeignLabourCertificationH2A.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2A))
                        issues += " - Foreign labor (H2A) jobs require approval";
                    if (posting.ForeignLabourCertificationH2B.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2B))
                        issues += " - Foreign labor (H2B) jobs require approval";
                    if (posting.ForeignLabourCertificationOther.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborOther))
                        issues += " -  Foreign labor (Other) jobs require approval";
                    if (posting.IsSalaryAndCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionSalary))
                        issues += " - Commission + salary jobs require approval";
                    if (posting.IsCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionBased))
                        issues += " - Commission-based jobs require approval";
                    if (posting.JobLocationType == JobLocationTypes.NoFixedLocation && approvalChecks.Contains(JobApprovalCheck.HomeBased))
                        issues += " -  Remote job – Home-based/remote jobs require approval";
                    if (posting.CreditCheckRequired.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CreditCheck))
                        issues += " - Credit background check jobs require  approval";
                    if (posting.HasCheckedCriminalRecordExclusion.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CriminalBackground))
                        issues += " -  Criminal background check jobs require  approval";
                    if (posting.MinimumAgeReasonValue.GetValueOrDefault(0) == 4)
                        issues += " - Minimum age other requirement jobs require  approval";
                    if (posting.JobSpecialRequirement.IsNotNullOrEmpty() && approvalChecks.Contains(JobApprovalCheck.SpecialRequirements))
                        issues += " - Special requirement jobs require approval";
                    if (posting.SuitableForHomeWorker.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.HomeBased))
                        issues += " - Home-based job - Home-based/remote jobs require approval";
                }

                else
                    issues += " - No job posting issues found";

                JobList.Add(new PendingJobIssuesModel(posting.Id, posting.JobTitle, issues));
            }

			PendingPostingsList.DataSource = JobList;
			PendingPostingsList.DataBind();

			if (pendingPostings.IsNull() || !pendingPostings.Any())
				PendingPostingsLiteral.Text = CodeLocalise("NoPendingPostingsLiteral.Text", "No pending postings available");
			else
				PendingPostingsLiteral.Text = CodeLocalise("PendingPostingsLiteral.Text", "Pending postings");
		}

		/// <summary>
		/// Gets the inappropriate words.
		/// </summary>
		/// <param name="referral">The referral.</param>
		/// <param name="descriptions">The business unit descriptions.</param>
		/// <returns></returns>
		private List<string> GetInappropriateWords(EmployerAccountReferralViewDto referral, List<BusinessUnitDescriptionDto> descriptions)
		{
			var inappropriateWords = new List<string>();

			var redWords = new StringBuilder(referral.RedProfanityWords ?? string.Empty);
			if (referral.TypeOfApproval == 2 && referral.BusinessUnitRedProfanityWords.IsNotNullOrEmpty())
				redWords.Append(",").Append(referral.BusinessUnitRedProfanityWords);

			descriptions.ForEach(desc =>
			{
				if (desc.RedProfanityWords.IsNotNullOrEmpty())
					redWords.Append(",").Append(desc.RedProfanityWords);
			});

			RedProfanityWords = GetCensorshipList(redWords.ToString());
			inappropriateWords.AddRange(RedProfanityWords);

			var yellowWords = new StringBuilder(referral.YellowProfanityWords ?? string.Empty);
			if (referral.TypeOfApproval == 2 && referral.BusinessUnitYellowProfanityWords.IsNotNullOrEmpty())
				yellowWords.Append(",").Append(referral.BusinessUnitYellowProfanityWords);

			descriptions.ForEach(desc =>
			{
				if (desc.YellowProfanityWords.IsNotNullOrEmpty())
					yellowWords.Append(",").Append(desc.YellowProfanityWords);
			});

			YellowProfanityWords = GetCensorshipList(yellowWords.ToString());
			inappropriateWords.AddRange(YellowProfanityWords);

			return inappropriateWords;
		}

		/// <summary>
		/// Binds the inappropriate words.
		/// </summary>
		/// <param name="inappropriateWords">The inappropriate words.</param>
		private void BindInappropriateWords(List<string> inappropriateWords)
		{
			switch (inappropriateWords.Count)
			{
				case 0:
					InappropriateWordsSummaryLiteral.Text = CodeLocalise("NoInappropriateWords.Text", @"There are no inappropriate words/phrases in this account request.");
					InappropriateWordsList.Visible = false;
					break;
				case 1:
					InappropriateWordsSummaryLiteral.Text = CodeLocalise("OneInappropriateWords.Text", @"We have flagged 1 potentially inappropriate word/phrase in this account request:");
					break;
				default:
					InappropriateWordsSummaryLiteral.Text = CodeLocalise("InappropriateWords.Text", @"We have flagged {0} potentially inappropriate words/phrases in this account request:", inappropriateWords.Count);
					break;
			}

			InappropriateWordsList.DataSource = inappropriateWords.Distinct().OrderBy(w => w);
			InappropriateWordsList.DataBind();
		}

		/// <summary>
		/// Converts a string of censored words to a list
		/// </summary>
		/// <param name="words">A comma-delimited list of words</param>
		/// <param name="inappropriateWords">A list of existing inappropriate words.</param>
		/// <returns>
		/// A list of words
		/// </returns>
		private List<string> GetCensorshipList(string words, List<string> inappropriateWords = null)
		{
			if (words.IsNullOrEmpty())
				return new List<string>();

			var wordList = words.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
													.Distinct()
													.OrderByDescending(s => s.Length)
													.ToList();

			if (inappropriateWords.IsNotNull() && wordList.IsNotNullOrEmpty())
				inappropriateWords.AddRange(wordList);

			return wordList;
		}

		/// <summary>
		/// Binds the Hiring Managers Notes/Reminders
		/// </summary>
		/// <param name="referral">The referral</param>
		private void BindNotesReminders(EmployerAccountReferralViewDto referral)
		{
			EmployerNotesReminders.EntityId = referral.BusinessUnitId;
			EmployerNotesReminders.EntityType = EntityTypes.BusinessUnit;
			EmployerNotesReminders.ReadOnly = (!App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator));
			EmployerNotesReminders.Bind();

			HiringContactNotesReminders.EntityId = referral.EmployeeId;
			HiringContactNotesReminders.EntityType = EntityTypes.Employee;
			HiringContactNotesReminders.ReadOnly = (!App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator));
			HiringContactNotesReminders.Bind();
		}

		/// <summary>
		/// Hides/Shows controls depending on the theme
		/// </summary>
		private void ApplyTheme()
		{
			IndustryClassificationHolder.Visible = App.Settings.Theme == FocusThemes.Workforce;

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
					EmployerNameLiteral.Visible =
					PrimaryNamePlaceHolder.Visible =
					DescriptionTitleLiteral.Visible = false;
			}
			else
			{
				LegalNamePlaceHolder.Visible = false;
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EmployerNameTitleLiteral.Text = CodeLocalise("EmployerNameTitleLiteral.Text", "#BUSINESS# name:");
			LegalNameTitleLiteral.Text = CodeLocalise("LegalNameTitleLiteral.Text", "#BUSINESS# legal name:");
			ApproveButton.Text = EmployerApprovalType == ApprovalType.HiringManager ? CodeLocalise("ApproveHiringManagerButton.Text", "Approve hiring manager") : CodeLocalise("ApproveButton.Text", "Approve #BUSINESS#:LOWER");
			DenyButton.Text = EmployerApprovalType == ApprovalType.HiringManager ? CodeLocalise("ApproveHiringManagerButton.Text", "Deny hiring manager") : CodeLocalise("DenyButton.Text", "Deny #BUSINESS#:LOWER");
			ApproveAccountRequestLabel.Text = EmployerApprovalType == ApprovalType.HiringManager ? CodeLocalise("EmployerReferral.Header", "Approve hiring manager account request:") : CodeLocalise("EmployerReferral.Header", "Approve #BUSINESS#:LOWER account request:");
			EditButton.Text = CodeLocalise("EditButton.Text", "Edit #BUSINESS#:LOWER");
			EditContactButton.Text = CodeLocalise("EditButton.Text", "Edit hiring contact");
			HoldButton.Text = CodeLocalise("HoldButton.Text", "Place on hold");
			DescriptionTitleLiteral.Text = HtmlLabel("BusinessDescription.Label", "Description:");
            InappropriateWordsList.Attributes.Add("role", "presentation");
            PendingPostingsList.Attributes.Add("role", "presentation");

			EmployerProfileLink.Text = CodeLocalise("EmployerProfileLink.Text", "View #BUSINESS#:LOWER profile");

			if (!IsAssistEmployersAdministrator())
			{
				EditButton.Visible = false;
			}

			EditFEINButton.Text = CodeLocalise("EditFEINButton.Text", "Edit FEIN");
			EditFEINButton.Visible = App.Settings.CanEditFEIN && App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);

			ReturnLink.Text = HtmlLocalise("ReturnToList.LinkText", "Return to main page");

			if (App.Settings.Theme == FocusThemes.Education)
			{
				FEINTableRow.Visible = SEINTableRow.Visible = false;
			}
		}

		/// <summary>
		/// Applies branding
		/// </summary>
		private void ApplyBranding()
		{
			HiringContactNotesAndRemindersHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			HiringContactNotesAndRemindersCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			HiringContactNotesAndRemindersCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			EmployerNotesAndRemindersHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			EmployerNotesAndRemindersCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			EmployerNotesAndRemindersCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			CompanyInformationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			CompanyInformationCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			CompanyInformationCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			ContactInformationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			ContactInformationCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			ContactInformationCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

			AccountRequestStatusHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			AccountRequestStatusCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			AccountRequestStatusCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Builds the address line2.
		/// </summary>
		/// <param name="town">The town.</param>
		/// <param name="stateId">The state id.</param>
		/// <param name="postcode">The postcode.</param>
		/// <returns></returns>
		private string BuildAddressLine2(string town, long? stateId, string postcode)
		{
			var address = new StringBuilder("");
			if (town.IsNotNullOrEmpty()) address.Append(town);

			if (town.IsNotNullOrEmpty() && (stateId.IsNotNull() && stateId > 0)) address.Append(", ");

			if (stateId.IsNotNull() && stateId > 0) address.Append(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Id == stateId).Select(x => x.Text).SingleOrDefault());

			if (postcode.IsNotNullOrEmpty()) address.AppendFormat(" {0}", postcode);

			return address.ToString().Trim();
		}

		/// <summary>
		/// Builds the full name.
		/// </summary>
		/// <param name="firstName">The first name.</param>
		/// <param name="lastName">The last name.</param>
		/// <returns></returns>
		private static string BuildFullName(string firstName, string lastName, string suffix)
		{
			return String.Format("{0} {1} {2}", firstName, lastName, suffix).Trim();
		}

		/// <summary>
		/// Gets the approve employer referral email.
		/// </summary>
		/// <returns></returns>
		private EmailTemplateView GetApproveEmployerReferralEmail(EmployerAccountReferralViewDto employerReferralView)
		{
			var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
			var phoneNumber = (userDetails.IsNotNull() && userDetails.PrimaryPhoneNumber.IsNotNull() && userDetails.PrimaryPhoneNumber.Number.IsNotNullOrEmpty()) ? userDetails.PrimaryPhoneNumber.Number : "N/A";

			var templateValues = new EmailTemplateData
			{
				RecipientName = String.Format("{0} {1}", employerReferralView.EmployeeFirstName, employerReferralView.EmployeeLastName),
				SenderPhoneNumber = Regex.Replace(phoneNumber, App.Settings.PhoneNumberStrictRegExPattern, App.Settings.PhoneNumberFormat),
				SenderEmailAddress = App.User.EmailAddress,
				SenderName = String.Format("{0} {1}", App.User.FirstName, App.User.LastName)
			};

			return ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.ApproveEmployerAccountReferral, templateValues);
		}

		/// <summary>
		/// Gets the text with profanities highlighted.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="redWords">The red words.</param>
		/// <param name="yellowWords">The yellow words.</param>
		/// <returns></returns>
		private string GetTextWithProfanitiesHighlighted(string text, List<string> redWords = null, List<string> yellowWords = null)
		{
			if (redWords.IsNull())
				redWords = RedProfanityWords;

			if (yellowWords.IsNull())
				yellowWords = YellowProfanityWords;

			if (redWords.IsNotNull() && redWords.Count > 0)
				//Highlight red words
				text = CommonUtilities.WordReplace(text, redWords, @"<font style=""BACKGROUND-COLOR: red"">", "</font>");

			if (yellowWords.IsNotNull() && yellowWords.Count > 0)
				//Highlight yellow words
				text = CommonUtilities.WordReplace(text, yellowWords, @"<font style=""BACKGROUND-COLOR: yellow"">", "</font>");

			return text;
		}

		/// <summary>
		/// Determines whether [is assist employers administrator].
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if [is assist employers administrator]; otherwise, <c>false</c>.
		/// </returns>
		protected bool IsAssistEmployersAdministrator()
		{
			return App.User.IsInRole(Constants.RoleKeys.AssistEmployersAdministrator);
		}

		/// <summary>
		/// Handles the EmployerUpdated event of the EditCompanyModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.EditCompanyModal.BusinessUnitUpdatedEventArgs"/> instance containing the event data.</param>
		protected void EditCompanyModal_EmployerUpdated(object sender, EditCompanyModal.CompanyUpdatedEventArgs e)
		{
			var model = e.Model;

			if (e.Model.IsNotNull())
			{
				ShowFEIN(model.FEIN);
				EmployerSEINLiteral.Text = model.StateEmployerIdentificationNumber;
				EmployerNameLiteral.Text = EmployerName2Literal.Text = model.Name;
			}
		}

		/// <summary>
		/// Handles the BusinessUnitUpdated event of the EditCompanyModal control.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void EditCompanyModal_BusinessUnitUpdated(object sender, EditCompanyModal.BusinessUnitUpdatedEventArgs e)
		{
			var model = e.Model;
			if (e.Model.IsNotNull())
			{
				EmployerAddress1Literal.Text = model.BusinessUnitAddress.Line1;
				EmployerAddress2Literal.Text = BuildAddressLine2(model.BusinessUnitAddress.TownCity,
																												 model.BusinessUnitAddress.StateId,
																												 model.BusinessUnitAddress.PostcodeZip);
				EmployerPhoneNumberLiteral.Text = model.BusinessUnit.PrimaryPhone;
				EmployerUrlLiteral.Text = model.BusinessUnit.Url;
				EmployerIndustryClassificationLiteral.Text = model.BusinessUnit.IndustrialClassification;
			}

			App.SetSessionValue("EmployerReferral:Confirmation", "OK");
			RedirectToSelf();
		}

		protected void EditContactModal_ContactUpdated(object sender, UpdateContactInformationModal.ContactUpdatedEventArgs e)
		{
			App.SetSessionValue("EmployerReferralContact:Confirmation", "OK");
			RedirectToSelf();
		}

		/// <summary>
		/// Redirects to the same page to refresh all the details on the page
		/// </summary>
		private void RedirectToSelf()
		{
			var referral = ServiceClientLocator.EmployeeClient(App).GetEmployerAccountReferral(EntityId);

			var referralChanged = false;

			// Check if referral is stick active
			if (referral.IsNull())
			{
				referralChanged = true;
			}
			else
			{
				var currentApprovalStatus = GetApprovalStatus(referral);
				if (currentApprovalStatus != ApprovalStatus)
				{
					referralChanged = true;
				}
			}

			if (referralChanged)
			{
				if (App.GetSessionValue<string>("EmployerReferral:Confirmation") == "OK")
				{
					var message = referral.IsNull()
													? CodeLocalise("ReferralEditEmployerConfirmationApproved.Body", "The #BUSINESS#:LOWER account has been amended, although the #BUSINESS#:LOWER has been approved in the meantime.")
													: CodeLocalise("ReferralEditEmployerConfirmationApproved.Body", "The #BUSINESS#:LOWER account has been amended, although the approval status has been changed in the meantime.");

					Confirmation.Show(CodeLocalise("ReferralEditEmployerConfirmation.Title", "#BUSINESS# account amended"),
														message,
														CodeLocalise("CloseModal.Text", "OK"),
														closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
					ConfirmationUpdatePanel.Update();

					App.RemoveSessionValue("EmployerReferral:Confirmation");
				}
				else if (App.GetSessionValue<string>("EmployerReferralContact:Confirmation") == "OK")
				{
					var message = referral.IsNull()
													? CodeLocalise("ReferralEditEmployerConfirmationApproved.Body", "The hiring manager has been amended, although the #BUSINESS#:LOWER has been approved in the meantime")
													: CodeLocalise("ReferralEditEmployerConfirmationApproved.Body", "The hiring manager has been amended, although the approval status has been changed in the meantime.");

					Confirmation.Show(CodeLocalise("ReferralEditEmployerConfirmation.Title", "Hiring manager amended"),
														message,
														CodeLocalise("CloseModal.Text", "OK"),
														closeLink: _employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
					ConfirmationUpdatePanel.Update();

					App.RemoveSessionValue("EmployerReferralContact:Confirmation");
				}
				else
				{
					Response.Redirect(_employerFilterApplied ? UrlBuilder.EmployerReferrals().AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferrals());
				}

				return;
			}
			//Return to saved criteria if possible
			switch (EmployerApprovalType)
			{
				case ApprovalType.Employer:
					Response.Redirect(_employerFilterApplied ? UrlBuilder.EmployerReferral(EntityId).AddToEndOfString(FilteredURL) : UrlBuilder.EmployerReferral(EntityId));
					break;
				case ApprovalType.BusinessUnit:
					Response.Redirect(_employerFilterApplied ? UrlBuilder.BusinessUnitReferral(EntityId).AddToEndOfString(FilteredURL) : UrlBuilder.BusinessUnitReferral(EntityId));
					break;
				case ApprovalType.HiringManager:
					Response.Redirect(_employerFilterApplied ? UrlBuilder.HiringManagerReferral(EntityId).AddToEndOfString(FilteredURL) : UrlBuilder.HiringManagerReferral(EntityId));
					break;
			}
		}

		protected void lnkReturn_Click(object sender, EventArgs e)
		{
			var returnPage = UrlBuilder.EmployerReferrals();

			if (_employerFilterApplied && !returnPage.Contains("filtered="))
			{
				Response.Redirect(returnPage.AddToEndOfString(FilteredURL));
			}
			else
			{
				Response.Redirect(returnPage);
			}
		}

	}

    public class PendingJobIssuesModel
    {
       public long? JobId { get; set; }
       public string JobTitle { get; set; }
       public string Issues { get; set; }

        public PendingJobIssuesModel(long? id, string jobTitle, string issues)
        {
            JobId = id;
            JobTitle = jobTitle;
            Issues = issues;
        }

    }
}
