﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Security.Permissions;

using Focus.Core;
using Focus.Web.Code;
using AjaxControlToolkit;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersAdministrator)]
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistEmployersReadOnly)]
  public partial class JobDevelopment : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      Page.Title = CodeLocalise("Page.Title", "Business development");

			if (!IsPostBack)
			{
			  SetReturnUrl();
				LocaliseUI();
			}

			if (IsPostBack)
				RegisterClientStartupScript("AssistToolTip");
     
		}

		/// <summary>
		/// Handles the Clicked event of the CreateNewEmployerAndJobOrderButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CreateNewEmployerAndJobOrderButton_Clicked(object sender, EventArgs e)
		{
			Response.Redirect(UrlBuilder.AssistRegisterEmployer());
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			#region Localise

			CreateNewEmployerAndJobOrderButton.Text = CodeLocalise("CreateNewEmployerAndJobOrderButton.Text", "Create new #BUSINESS#:LOWER & #EMPLOYMENTTYPE#:LOWER");

			#endregion
		}
	}
}