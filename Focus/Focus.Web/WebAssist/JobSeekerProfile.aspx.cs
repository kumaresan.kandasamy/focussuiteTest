﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Focus.Web.Core.Models;
using Focus.Web.WebAssist.Controls;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
  [PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
  public partial class JobSeekerProfile : PageBase
	{
    #region Properties

    protected bool _isAssistPage;
    protected bool _jobSeekerFilterApplied;

    internal long JobSeekerId
    {
      get { return GetViewStateValue<long>("JobSeekerProfile:JobSeekerId"); }
      set { SetViewStateValue("JobSeekerProfile:JobSeekerId", value); }
    }

    protected JobSeekerProfileModel JobSeekerProfileModel
    {
      get { return GetViewStateValue<JobSeekerProfileModel>("JobSeekerProfile:JobSeekerProfileModel"); }
      set { SetViewStateValue("JobSeekerProfile:JobSeekerProfileModel", value); }
    }

    private JobSeekerCriteria JobSeekerCriteria
    {
      get { return GetViewStateValue<JobSeekerCriteria>("ActivitySummaryPanel:JobSeekerCriteria"); }
      set { SetViewStateValue("ActivitySummaryPanel:JobSeekerCriteria", value); }
    }

    private JobSeekerActivityView JobSeekerActivityView
    {
      get { return GetViewStateValue<JobSeekerActivityView>("ActivitySummaryPanel:JobSeekerActivityView"); }
      set { SetViewStateValue("ActivitySummaryPanel:JobSeekerActivityView", value); }
    }


    #endregion

    protected FocusThemes CurrentTheme
    {
      get { return App.Settings.Theme; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       Page.Title = CodeLocalise("Page.Title", "Job Seeker Profile");

      _isAssistPage = Page.Is(UrlBuilder.AssistJobSeekerProfileForCompareOnly());

      if (Request.QueryString["filtered"] != null) _jobSeekerFilterApplied = Convert.ToBoolean(Request.QueryString["filtered"]);

      // Set the visibility of the items dependent on Focus Talent integration
      lblResumeIsSearchable.Visible =
      searchableResumeCell.Visible =
      referralOutcomesRow.Visible =
      surveyResponsesRow.Visible = App.Settings.TalentModulePresent;

      staffReferralsCell.Visible =
      totalReferralsCell.Visible =
      highGrowthSectorsCell.Visible = (App.Settings.TalentModulePresent && CurrentTheme == FocusThemes.Workforce);

      if (!Page.IsPostBack)
      {
        if (Page.RouteData.Values.ContainsKey("jobseekerid"))
        {
          long id;
          Int64.TryParse(Page.RouteData.Values["jobseekerid"].ToString(), out id);
          JobSeekerId = id;
          BindMSFWControls();
          GetJobSeekerProfileData();
          PopulateJobSeekerProfileData();
          PopulateActivitySummaryPanel();
          BindControls();
          NotesReminders.EntityId = JobSeekerId;
          NotesReminders.EntityType = EntityTypes.JobSeeker;
          NotesReminders.ReadOnly = (!App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersAdministrator));
          NotesReminders.Bind();
          JobSeekerActivityList.JobSeekerId = JobSeekerId;
          JobSeekerActivityList.JobSeekerProfileModel = JobSeekerProfileModel;
          AssignedOffices.ContextId = JobSeekerId;
          AssignedOffices.Bind();

					Page.Title = string.Format("{0} - {1}",CodeLocalise("Page.Title", "Job Seeker Profile"),id);
        }
        Localise();
        ApplyBranding();
        HideOfficeRow();
      }
    }

    /// <summary>
    /// Gets the job seeker profile data.
    /// </summary>
    private void GetJobSeekerProfileData()
    {
      //List<CandidateNoteView> notes;
			JobSeekerProfileModel = ServiceClientLocator.CandidateClient(App).GetJobSeekerProfile(JobSeekerId);

      if (JobSeekerProfileModel.JobSeekerProfileView.DefaultResumeId.HasValue)
      {
        var defaultResumeHtml = ServiceClientLocator.CandidateClient(App).GetResumeByIdAsHtml(JobSeekerProfileModel.JobSeekerProfileView.DefaultResumeId.Value);
        if (defaultResumeHtml.IsNotNull())
        {
          // Remove any style tags
          var tagMatch = new Regex(@"<style[^>]*>[\s\S]*</style>");
          defaultResumeHtml = tagMatch.Replace(defaultResumeHtml, "");

          // Replace remaining tags with an empty string so they are not considered in count
          tagMatch = new Regex("<[^>]+>");
          defaultResumeHtml = tagMatch.Replace(defaultResumeHtml, "");

          JobSeekerProfileModel.ResumeLength = Regex.Matches(defaultResumeHtml, @"\S+").Count;
        }
      }
    }

    #region PopulateJobSeekerProfile And Localise

		/// <summary>
		/// Binds the under-age jobseeker image.
		/// </summary>
		/// <param name="dateOfBirth">The jobseeker's date of birth.</param>
		private void BindUnderageImage(DateTime? dateOfBirth)
		{
			if (App.Settings.UnderAgeJobSeekerRestrictionThreshold <= 0 || !dateOfBirth.HasValue) return;
			var now = DateTime.Today;
			var age = now.Year - dateOfBirth.Value.Year;
			if (dateOfBirth.Value > now.AddYears(-age)) age--;

			if (age >= App.Settings.UnderAgeJobSeekerRestrictionThreshold) return;
			UnderageImage.Visible = true;
			UnderageImage.ImageUrl = UrlBuilder.UnderAgeJobSeekerIconSmall();
			UnderageImage.ToolTip = UnderageImage.AlternateText = CodeLocalise("Underage.Tooltip", "Under-age job seeker");
		}

    /// <summary>
    /// Populates the job seeker profile data.
    /// </summary>
    private void PopulateJobSeekerProfileData()
    {
      // Title
      var seekerView = JobSeekerProfileModel.JobSeekerProfileView;
      
      var profileTitle = new StringBuilder();
      profileTitle.Append(seekerView.FirstName);
      
      if (seekerView.MiddleInitial.IsNotNullOrEmpty())
        profileTitle.Append(" ").Append(seekerView.MiddleInitial);
      
      profileTitle.Append(" ").Append(seekerView.LastName);

      if (CurrentTheme == FocusThemes.Workforce)
        profileTitle.Append(", Focus ID: ").Append(seekerView.Id);
      
      lblProfileTitle.Text = profileTitle.ToString();

			BindUnderageImage(seekerView.DateOfBirth);

      // Profile

      // Unemployment Info
      if (JobSeekerProfileModel.UnemploymentInfo == string.Empty)
        UnemploymentInfoRow.Visible = false;
      else
        lblUnemployment.Text = JobSeekerProfileModel.UnemploymentInfo;

      // Veteran Info
      var veteranInfo = JobSeekerProfileModel.VeteranInfo;

        if (veteranInfo.IsNotNull())
        {
            bool jobseekerIsVeteran = veteranInfo.IsVeteran ?? false;

            if (!jobseekerIsVeteran == true)
            {
                VeteranInfoRow.Visible = ServiceConnectedDisabiltyInfoRow.Visible = false;
            }

            else
            {
                var sb = new StringBuilder();

                var historyCount = 0;
                foreach (var history in veteranInfo.History)
                {
                    historyCount++;

                    if (historyCount > 1)
                        sb.Append("<br />");

                    if (history.VeteranStartDate != null && history.VeteranEndDate != null)
                        sb.Append(String.Format("{0} - {1}<br/>",
                            ((DateTime) history.VeteranStartDate).ToString("MMM d, yyyy"),
                            ((DateTime) history.VeteranEndDate).ToString("MMM d, yyyy")));

                    // Not sure if veteran era is getting populated from career UI, so removing this section from display
                    if (history.VeteranEra != null)
                        sb.Append(String.Format("{0}: {1}<br/>", CodeLocalise("VeteranEra", "Type of veteran"),
                            CodeLocalise(history.VeteranEra)));

                    if (history.IsCampaignVeteran != null)
                        sb.Append(String.Format("{0}: {1}<br/>", CodeLocalise("CampaignVeteran", "Campaign veteran"),
                            (bool) history.IsCampaignVeteran ? CodeLocalise("Yes", "yes") : CodeLocalise("No", "no")));

                    if (history.UnitOrAffiliation.IsNotNullOrEmpty())
                        sb.Append(String.Format("{0}: {1}<br/>",
                            CodeLocalise("UnitOrAffiliation", "Unit or affiliation"), history.UnitOrAffiliation));

                    if (history.RankId != null)
                        sb.Append(String.Format("{0}: {1}<br/>", CodeLocalise("Rank", "Rank"),
                            ServiceClientLocator.CoreClient(App)
                                .GetLookup(LookupTypes.MilitaryRanks)
                                .Where(x => x.Id == history.RankId)
                                .Select(x => x.Text)
                                .SingleOrDefault()));

                    if (history.MilitaryBranchOfServiceId != null)
                        sb.Append(String.Format("{0}: {1}<br/>",
                            CodeLocalise("MilitaryBranchOfService", "Military branch of service"),
                            ServiceClientLocator.CoreClient(App)
                                .GetLookup(LookupTypes.MilitaryBranchesOfService)
                                .Where(x => x.Id == history.MilitaryBranchOfServiceId)
                                .Select(x => x.Text)
                                .SingleOrDefault()));

                    if (history.MilitaryDischargeId != null)
                        sb.Append(String.Format("{0}: {1}<br/>", CodeLocalise("MilitaryDischarge", "Military discharge"),
                            ServiceClientLocator.CoreClient(App)
                                .GetLookup(LookupTypes.MilitaryDischargeTypes)
                                .Where(x => x.Id == history.MilitaryDischargeId)
                                .Select(x => x.Text)
                                .SingleOrDefault()));

                    if (history.MilitaryOccupation.IsNotNullOrEmpty())
                        sb.Append(String.Format("{0}: {1}<br/>",
                            CodeLocalise("MilitaryOccupationCode", "Military occupation code"),
                            history.MilitaryOccupation));

                    if (history.TransitionType != null)
                        sb.Append(String.Format("{0}: {1}<br/>", CodeLocalise("TransitionType", "Transition type"),
                            history.TransitionType));

                    if (history.VeteranDisabilityStatus.IsNotNull() && historyCount > 1)
                    {
                        sb.Append(CodeLocalise(history.VeteranDisabilityStatus));
                        sb.Append("<br />");
                    }

                    if (historyCount == 1)
                    {
                        sb.Append("<br />");
                        lblVeteran.Text = sb.ToString();
                        sb = new StringBuilder();
                    }
                }

                if (historyCount > 1 && App.Settings.AllowDateBoundMilitaryService)
                {
                    OtherHistoryRow.Visible = true;
                    lblOtherHistory.Text = sb.ToString();

                    ShowOtherHistoryLink.Attributes.Add("onclick",
                        string.Format("$('#{0}').show();$(this).hide();$('#{1}').show();return false;",
                            lblOtherHistory.ClientID, HideOtherHistoryLink.ClientID));
                    ShowOtherHistoryLink.Attributes.Add("onkeypress",
                        string.Format("$('#{0}').show();$(this).hide();$('#{1}').show();return false;",
                            lblOtherHistory.ClientID, HideOtherHistoryLink.ClientID));

                    HideOtherHistoryLink.Attributes.Add("onclick",
                        string.Format("$('#{0}').hide();$(this).hide();$('#{1}').show();return false;",
                            lblOtherHistory.ClientID, ShowOtherHistoryLink.ClientID));
                    HideOtherHistoryLink.Attributes.Add("onkeypress",
                        string.Format("$('#{0}').hide();$(this).hide();$('#{1}').show();return false;",
                            lblOtherHistory.ClientID, ShowOtherHistoryLink.ClientID));

                }

                // Service connected diability
                var firstHistory = veteranInfo.History[0];
                if (firstHistory.VeteranDisabilityStatus.IsNull() ||
                    firstHistory.VeteranDisabilityStatus == VeteranDisabilityStatus.NotDisabled)
                    ServiceConnectedDisabiltyInfoRow.Visible = false;
                else
                    lblServiceConnectedDisability.Text = firstHistory.VeteranDisabilityStatus ==
                                                         VeteranDisabilityStatus.Disabled
                        ? CodeLocalise(VeteranDisabilityStatus.Disabled, "Disabled (Up to 20%)")
                        : CodeLocalise(VeteranDisabilityStatus.SpecialDisabled, "Special Disabled (30% or more)");
            }
        }

        else
            VeteranInfoRow.Visible = ServiceConnectedDisabiltyInfoRow.Visible = false;

        // Disability
        if (JobSeekerProfileModel.DisabilityStatus != DisabilityStatus.Disabled)
            DisabiltyInfoRow.Visible = false;
        else
        {
            if (JobSeekerProfileModel.DisabilityCategoryIds.IsNotNull())
            {
                var disabledList = JobSeekerProfileModel.DisabilityCategoryIds;
                foreach (var id in disabledList)
                {
                    var category =
                        ServiceClientLocator.CoreClient(App)
                            .GetLookup(LookupTypes.DisabilityCategories)
                            .Where(x => x.Id == id)
                            .ToList();
                    lblDisabled.Text += category[0].Text + "<br/>";
                }
            }
            else
            {
                lblDisabled.Text = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DisabilityCategories).Where(x => x.Id == JobSeekerProfileModel.DisabilityCategoryId).Select(x => x.Text).SingleOrDefault();

            }
        }
    

      // Migrant / Seasonal Farm Worker
      if (!JobSeekerProfileModel.IsMSFW.GetValueOrDefault(false))
      {
        MSFWInfoRow.Visible = false;  
      }
      else
      {
        var verified = JobSeekerProfileModel.JobSeekerProfileView.MigrantSeasonalFarmWorkerVerified;
        if (verified.HasValue)
          MSFWVerifiedOptions.SelectedIndex = verified.Value ? 0 : 1;

        if (!App.Settings.PotentialMSFWQuestionsEnabled)
            MSFWQuestions.Visible = false;
        else
        {
            foreach (ListItem msfwBox in MSFWQuestions.Items)
            {
                msfwBox.Enabled = !verified.GetValueOrDefault();

                if (JobSeekerProfileModel.MSFWQuestions.IsNotNullOrEmpty())
                {
                    var msfwBoxValue = msfwBox.Value.ToLong();
                    if (msfwBoxValue.HasValue)
                        msfwBox.Selected = JobSeekerProfileModel.MSFWQuestions.Contains(msfwBoxValue.Value);
                }
            }
        } 

      }

      //HousingInfo
      if (JobSeekerProfileModel.HomelessWithoutShelter || JobSeekerProfileModel.HomelessWithShelter || JobSeekerProfileModel.RunawayYouth)
      {
          HouselessWithoutShelterInfoRow.Visible = true;
          
          if (JobSeekerProfileModel.HomelessWithoutShelter)
              lblHouseWithoutShelter.Text = "Homeless without shelter <br/>";
          if (JobSeekerProfileModel.HomelessWithShelter)
              lblHouseWithoutShelter.Text += "Homeless with shelter <br/>";
          if (JobSeekerProfileModel.RunawayYouth)
              lblHouseWithoutShelter.Text += "RunawayYouth <br/>";
      }

      else
          HouselessWithoutShelterInfoRow.Visible = false;


      //Legal Info
      if (JobSeekerProfileModel.ExOffender)
      {
          ExOffenderInfoRow.Visible = true;
          lblExoffender.Text = "Ex-offender <br/>";
      }
      else
          ExOffenderInfoRow.Visible = false;

      //Language Or Cultural Issues
      #region

      if (JobSeekerProfileModel.PreferredLanguage.IsNotNull() )
      {

          if (!JobSeekerProfileModel.PreferredLanguage.Equals("Not disclosed"))
          {
              
              LanguageCulturalIssues.Text = "\r\n" + "Preferred Language: " + JobSeekerProfileModel.PreferredLanguage + "<br/>";
          }
      }
          if (JobSeekerProfileModel.LowLevelLiteracy)
          {
              
              LanguageCulturalIssues.Text += "Low Level of Literacy / Basic Skills Deficiencies:" + "<br/>";
              if (JobSeekerProfileModel.NativeLanguage.IsNotNull())
                  LanguageCulturalIssues.Text += "&nbsp;&nbsp;a. Native Language: " + JobSeekerProfileModel.NativeLanguage + "<br/>";

              if (JobSeekerProfileModel.CommonLanguage.IsNotNull())
                  LanguageCulturalIssues.Text += "&nbsp;&nbsp;b. Common Language: " + JobSeekerProfileModel.CommonLanguage + "<br/>";

          }
          if (JobSeekerProfileModel.CulturalBarriers)
          {
              
              LanguageCulturalIssues.Text += "Cultural barriers";
          }

          if (!LanguageCulturalIssues.Text.IsNullOrWhitespace())
          {
              CultureInfoRow.Visible = true;
          }
          else
          {
              CultureInfoRow.Visible = false;
          }

      #endregion

     //Income issues
      #region
      if (JobSeekerProfileModel.NoOfDependents.IsNotNull())
      {
          if (!JobSeekerProfileModel.NoOfDependents.Equals("Not disclosed"))
              IncomeIssues.Text = "Number of dependents - " + JobSeekerProfileModel.NoOfDependents + "<br/>";
      }
      if (JobSeekerProfileModel.EstMonthlyIncome.IsNotNull())
      {
          if (!JobSeekerProfileModel.EstMonthlyIncome.Equals("Not disclosed"))
              IncomeIssues.Text += "Estimated monthly income - $" + JobSeekerProfileModel.EstMonthlyIncome + "<br/>";
      }
    if (JobSeekerProfileModel.DisplacedHomemaker)
        IncomeIssues.Text += "Unemployed Homemaker <br />";
    if (JobSeekerProfileModel.SingleParent)
        IncomeIssues.Text += "Single parent <br/>";
    if (JobSeekerProfileModel.LowIncomeStatus)
        IncomeIssues.Text += "Low income status <br/>";
    if (!IncomeIssues.Text.IsNullOrWhitespace())
        IncomeInfoRow.Visible = true;
    else
        IncomeInfoRow.Visible = false;
      #endregion

      if (!UnemploymentInfoRow.Visible && !VeteranInfoRow.Visible && !DisabiltyInfoRow.Visible && !MSFWInfoRow.Visible && !CultureInfoRow.Visible && !IncomeInfoRow.Visible && !ExOffenderInfoRow.Visible && !HouselessWithoutShelterInfoRow.Visible)
      {
        ProfilePanelRow.Visible = false;
        ContactInformationCollapsiblePanelExtender.Collapsed = false;
      }

      if (!VeteranInfoRow.Visible)
      {
        ContactInformationCollapsiblePanelExtender.Collapsed = false;
        ContactInformationHeaderPanel.CssClass = "singleAccordionTitle on";
      }
      else
      {
        ContactInformationCollapsiblePanelExtender.Collapsed = true;
        ContactInformationHeaderPanel.CssClass = "singleAccordionTitle";
      }

      // Contact information
      if (ProfileContainsContactAddressData(JobSeekerProfileModel.JobSeekerProfileView))
      {
        lblAddress1.Text = JobSeekerProfileModel.JobSeekerProfileView.AddressLine1;
        lblTownCity.Text = string.Concat(NullAlternative(JobSeekerProfileModel.JobSeekerProfileView.TownCity, CodeLocalise("NotAvailable", "Not available")), ", ");
				lblState.Text = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Id == JobSeekerProfileModel.JobSeekerProfileView.StateId).Select(x => x.ExternalId).SingleOrDefault();
				lblZipCode.Text = JobSeekerProfileModel.JobSeekerProfileView.PostcodeZip;
      }
      else
      {
        lblTownCity.Text = CodeLocalise("AddressNotAvailable", "Address not available");
      }

      var phone = JobSeekerProfileModel.PrimaryPhone;
      if (phone.IsNull() || phone.PhoneNumber.IsNullOrEmpty())
      {
        lblPhoneNumber.Text = CodeLocalise("NotAvailable", "Not available");
      }
      else
      {
        var phoneNumber = (phone.PhoneType == PhoneType.NonUS) ? phone.PhoneNumber : phone.PhoneNumber.FormatPhoneNumber("(000) 000-0000");
        lblPhoneNumber.Text = string.Format("{0} ({1})", phoneNumber, CodeLocalise(phone.PhoneType));
      }

      lblEmail.Text = NullAlternative(JobSeekerProfileModel.JobSeekerProfileView.EmailAddress);

      // Resume
      if (JobSeekerProfileModel.JobSeekerProfileView.DefaultResumeId.HasValue)
      {
        lblResumeLength.Text = String.Format("{0} words", JobSeekerProfileModel.ResumeLength);
        lblSkillsCodedFromResume.Text = JobSeekerProfileModel.SkillsCodedFromResume.ToString(CultureInfo.InvariantCulture);
        lblNumberOfResumes.Text = JobSeekerProfileModel.NumberOfResumes.ToString(CultureInfo.InvariantCulture);
        lblResumeIsSearchable.Text = JobSeekerProfileModel.ResumeIsSearchable.HasValue
                                                                     ? (JobSeekerProfileModel.ResumeIsSearchable.Value ? "yes" : "no")
                                   : CodeLocalise("NotSelected", "Not selected");
        lblWillingToWorkOvertime.Text = JobSeekerProfileModel.WillingToWorkOvertime.HasValue
                                                                        ? (JobSeekerProfileModel.WillingToWorkOvertime.Value ? "yes" : "no")
                                                                        : CodeLocalise("NotSelected", "Not selected");
        lblWillingToRelocate.Text = JobSeekerProfileModel.WillingToRelocate.HasValue
                                                                        ? (JobSeekerProfileModel.WillingToRelocate.Value ? "yes" : "no")
                                                                        : CodeLocalise("NotSelected", "Not selected");
        lblNumberOfIncompleteResumes.Text = JobSeekerProfileModel.NumberOfIncompleteResumes.ToString(CultureInfo.InvariantCulture);

        NoResumeLabel.Visible = false;

        lblEmploymentStatus.Text = JobSeekerProfileModel.EmploymentStatus != 0 ? CodeLocalise(JobSeekerProfileModel.EmploymentStatus) : CodeLocalise("NotSelected", "Not selected");
        lblEnrollmentStatus.Text = JobSeekerProfileModel.EnrollmentStatus.IsNotNull() ? CodeLocalise(JobSeekerProfileModel.EnrollmentStatus) : CodeLocalise("NotSelected", "Not selected");
        lblEnrollmentStatus.Visible = !App.Settings.HideEnrollmentStatus;
        lblEducationLevel.Text = JobSeekerProfileModel.EducationLevel.IsNotNull() ? CodeLocalise(JobSeekerProfileModel.EducationLevel) : CodeLocalise("NotSelected", "Not selected");

        JobSeekerResumeTable.Visible = true;
        UpdateResumeModalAction.Visible = App.Settings.Theme == FocusThemes.Workforce && JobSeekerProfileModel.NumberOfIncompleteResumes < JobSeekerProfileModel.NumberOfResumes;
      }
      else
      {
        NoResumeLabel.Visible = true;
        JobSeekerResumeTable.Visible = false;
        UpdateResumeModalAction.Visible = false;
      }

      //Referral Outcomes
      lblApplicantsInterviewed.Text = JobSeekerProfileModel.ApplicantsInterviewed.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsFailedToShow.Text = JobSeekerProfileModel.ApplicantsFailedToShow.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsInterviewDenied.Text = JobSeekerProfileModel.ApplicantsInterviewDenied.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsHired.Text = JobSeekerProfileModel.ApplicantsHired.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsRejected.Text = JobSeekerProfileModel.ApplicantsRejected.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsRefused.Text = JobSeekerProfileModel.ApplicantsRefused.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsDidNotApply.Text = JobSeekerProfileModel.ApplicantsDidNotApply.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsFailedToRespondToInvitation.Text = JobSeekerProfileModel.ApplicantsFailedToRespondToInvitation.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsFailedToReportToJob.Text = JobSeekerProfileModel.ApplicantsFailedToReportToJob.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsFoundJobFromOtherSource.Text = JobSeekerProfileModel.ApplicantsFoundJobFromOtherSource.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsJobAlreadyFilled.Text = JobSeekerProfileModel.ApplicantsJobAlreadyFilled.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsNewApplicant.Text = JobSeekerProfileModel.ApplicantsNewApplicant.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsNotQualified.Text = JobSeekerProfileModel.ApplicantsNotQualified.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsNotYetPlaced.Text = JobSeekerProfileModel.ApplicantsNotYetPlaced.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsRecommended.Text = JobSeekerProfileModel.ApplicantsRecommended.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsRefusedReferral.Text = JobSeekerProfileModel.ApplicantsRefusedReferral.ToString(CultureInfo.CurrentUICulture);
      lblApplicantsUnderConsideration.Text = JobSeekerProfileModel.ApplicantsUnderConsideration.ToString(CultureInfo.CurrentUICulture);

      lblMatchQualityDissatisfied.Text = String.Format("{0} time{1}", JobSeekerProfileModel.MatchQualityDissatisfied, JobSeekerProfileModel.MatchQualityDissatisfied == 1 ? "" : "s");
      lblMatchQualitySatisfied.Text = String.Format("{0} time{1}", JobSeekerProfileModel.MatchQualitySatisfied, JobSeekerProfileModel.MatchQualitySatisfied == 1 ? "" : "s");
      lblMatchQualitySomewhatDissatisfied.Text = String.Format("{0} time{1}", JobSeekerProfileModel.MatchQualitySomewhatDissatisfied, JobSeekerProfileModel.MatchQualitySomewhatDissatisfied == 1 ? "" : "s");
      lblMatchQualitySomewhatSatisfied.Text = String.Format("{0} time{1}", JobSeekerProfileModel.MatchQualitySomewhatSatisfied, JobSeekerProfileModel.MatchQualitySomewhatSatisfied == 1 ? "" : "s");
      lblReceivedUnexpectedResults.Text = String.Format("{0} time{1}", JobSeekerProfileModel.ReceivedUnexpectedResults, JobSeekerProfileModel.ReceivedUnexpectedResults == 1 ? "" : "s");
      lblNotReceiveUnexpectedMatches.Text = String.Format("{0} time{1}", JobSeekerProfileModel.NotReceiveUnexpectedMatches, JobSeekerProfileModel.NotReceiveUnexpectedMatches == 1 ? "" : "s");
      lblNotEmployerInvitationsReceive.Text = String.Format("{0} time{1}", JobSeekerProfileModel.NotEmployerInvitationsReceive, JobSeekerProfileModel.NotEmployerInvitationsReceive == 1 ? "" : "s");
      lblReceivedEmployerInvitations.Text = String.Format("{0} time{1}", JobSeekerProfileModel.ReceivedEmployerInvitations, JobSeekerProfileModel.ReceivedEmployerInvitations == 1 ? "" : "s");
    }

    /// <summary>
    /// Localises this instance.
    /// </summary>
    private void Localise()
    {
      DaysDropDownButton.Text = CodeLocalise("Button.Go", "Go");
      NoResumeLabel.Text = HtmlLocalise("NoResume.Label", "No resume stored");

      ShowOtherHistoryLink.InnerText = "Show";
      HideOtherHistoryLink.InnerText = "Hide";

      var returnPage = GetReturnUrl();

      if (returnPage.Contains("employer"))
				lnkReturn.Text = CodeLocalise("ReturnToEmployerProfile", "return to #BUSINESS# profile");
			else if (returnPage.Contains("jobdevelopment") | returnPage.Contains("businessdevelopment"))
				lnkReturn.Text = CodeLocalise("ReturnToJobDevelopment", "return to #BUSINESS# development");
      else if (returnPage.Contains("reporting"))
        lnkReturn.Text = CodeLocalise("ReturnToReport", "return to Report");
      else if (returnPage.Contains("referral"))
        lnkReturn.Text = CodeLocalise("ReturnToReport", "return to referral request");
      else
        lnkReturn.Text = CodeLocalise("ReturnToAssistJobSeekers", "return to #CANDIDATETYPE#:LOWER dashboard");

      var viewJobSeekerResumeText = CodeLocalise("ViewJobSeekerResume", "View #CANDIDATETYPE# resume").ToLower();
      viewJobSeekerResumeText = char.ToUpper(viewJobSeekerResumeText[0]) + viewJobSeekerResumeText.Substring(1);

      ViewJobSeekerResumeLiteral.Text = viewJobSeekerResumeText;
      UpdateResumeModalAction.Text = CodeLocalise("UpdateResume.Label", "Update information");

      ExpandCollapseAllButton.Text = HtmlLocalise("ExpandAllButton.Text", "Expand all");
    }

    private void ApplyBranding()
    {
      ContactInformationHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      ContactInformationCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      ContactInformationCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      JobSeekerProfileHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      JobSeekerProfileCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      JobSeekerProfileCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      ActivitySummaryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      ActivitySummaryCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      ActivitySummaryCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      JobSeekerResumeHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      JobSeekerResumeCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      JobSeekerResumeCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      ReferralOutcomesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      ReferralOutcomesHeaderCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      ReferralOutcomesHeaderCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      SurveyResponsesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      SurveyResponsesCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      SurveyResponsesCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      ActivityLogHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      ActivityLogCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      ActivityLogCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      NotesAndRemindersHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      NotesAndRemindersCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      NotesAndRemindersCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();

      AssignedOfficesHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
      AssignedOfficesCollapsiblePanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
      AssignedOfficesCollapsiblePanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
    }

    /// <summary>
    /// Hides the office panel.
    /// </summary>
    private void HideOfficeRow()
    {
      OfficeRow.Visible = App.Settings.OfficesEnabled;
    }

    /// <summary>
    /// Handles the Click event of the lnkReturn control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    public void lnkReturn_Click(object sender, EventArgs e)
    {
      var returnPage = UseReturnUrl();

      if (returnPage.IsNullOrEmpty())
        Response.RedirectToRoute("AssistJobSeekers");
      else if ((returnPage.Contains("employer") || returnPage.Contains("referral") || _isAssistPage) && returnPage.IsNotNullOrEmpty())
      {
        if (_jobSeekerFilterApplied && !returnPage.Contains("filtered="))
        {
          Response.Redirect(returnPage + "?filtered=true");
        }
        else
        {
          Response.Redirect(returnPage);
        }
      }
      else
        Response.Redirect(Page.RouteData.Values.ContainsKey("session")
          ? UrlBuilder.ReportingSpecific("jobseeker", Page.RouteData.Values["session"].ToString())
          : UrlBuilder.ReportingSpecific("jobseeker"));
    }

    #endregion

    /// <summary>
    /// Binds MSFW Controls
    /// </summary>
    private void BindMSFWControls()
    {
      MSFWQuestions.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MSFWQuestions), null);
      MSFWVerifiedOptions.Items.Add(new ListItem(CodeLocalise("MSFW.Verified", "Verified as MSFW"), "yes"));
      MSFWVerifiedOptions.Items.Add(new ListItem(CodeLocalise("MSFW.NotVerified", "Verified as not MSFW"), "no"));
      MSFWVerifiedOptions.Attributes.Add("role", "presentation");
      MSFWQuestions.Attributes.Add("role", "presentation");
    }

     /// <summary>
    /// Binds the controls.
    /// </summary>
    private void BindControls()
    {
      //BindReturnUrl();
      BindDaysFiltersDropDown();
      BindRepeater();
      UpdateResumeModalAction.Visible = !(App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersReadOnly));
    }

    /// <summary>
    /// Binds the repeater.
    /// </summary>
    private void BindRepeater()
    {
			var resumeList = ServiceClientLocator.CandidateClient(App).GetResumes(JobSeekerId);

      JobSeekerResumeRepeater.DataSource = resumeList;
      JobSeekerResumeRepeater.DataBind();
    }

    /// <summary>
    /// Handles the Click event of the DaysDropDownButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void DaysDropDownButton_Click(object sender, EventArgs e)
    {
      JobSeekerCriteria.DaysBack = Convert.ToInt32(DaysFilterDropDown.SelectedValue);
      PopulateActivitySummaryPanel();
    }

    /// <summary>
    /// Handles the Click event of the SaveButton in the AssignedOffices user control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void SaveAssignedOffices(object sender, EventArgs e)
    {
      var personOffices = new List<PersonOfficeMapperDto>();
      var assignedOfficeList = AssignedOffices.AssignedOfficeList;

      // If there's nothing in the list box then everything is to be deleted 
      if (assignedOfficeList.Count > 0)
      {
        personOffices = (from AssignedOfficeModel item in assignedOfficeList
                         select new PersonOfficeMapperDto()
                         {
                           OfficeId = item.OfficeId,
                           PersonId = JobSeekerId
                         }).ToList();
      }
      else
      {
        AssignedOffices.Invalidate(HtmlLocalise("OfficeExistsValidator.Error", "An office must be assigned"));
        return;
      }

			ServiceClientLocator.EmployerClient(App).SavePersonOffices(personOffices, JobSeekerId);

      // If the default office has been automatically removed and offices assigned then update the offices with the zip of the Job Seeker so that future job seekers with that zip can be auto allocated
      if (AssignedOffices.DefaultOfficeRemoved)
      {
        var officeIds = (from l in assignedOfficeList
                         select l.OfficeId.GetValueOrDefault()).ToList();

        var postcodeZip = new List<string> { JobSeekerProfileModel.JobSeekerProfileView.PostcodeZip };

				ServiceClientLocator.EmployerClient(App).UpdateOfficeAssignedZips(postcodeZip, officeIds);
      }

      Confirmation.Show(CodeLocalise("JobSeekerOfficesUpdatedConfirmation.Title", "Job seeker offices updated"),
                                                       CodeLocalise("JobSeekerOfficesUpdatedConfirmation.Body", "The offices for the job seeker account have been updated."),
                                                       CodeLocalise("CloseModal.Text", "OK"));
    }

    /// <summary>
    /// Populates the activity summary panel.
    /// </summary>
    private void PopulateActivitySummaryPanel()
    {
      if (JobSeekerCriteria == null)
        JobSeekerCriteria = new JobSeekerCriteria { DaysBack = 0, JobSeekerId = JobSeekerId };

      var getExtraFields = CurrentTheme == FocusThemes.Workforce;
      JobSeekerActivityView = ServiceClientLocator.CandidateClient(App).GetJobSeekerActivityView(JobSeekerCriteria, getExtraFields, getExtraFields);

      lblDaysSinceRegistration.Text = JobSeekerActivityView.DaysSinceRegistration.ToString(CultureInfo.CurrentCulture);
      lblFocusCareerSignins.Text = JobSeekerActivityView.FocusCareerSignins.ToString(CultureInfo.CurrentCulture);
      lblTimeSinceLastLogin.Text = TimeSince(JobSeekerActivityView.LastLogin);
      lblResumeEdited.Text = JobSeekerProfileModel.JobSeekerProfileView.DefaultResumeId.HasValue
    ? String.Format("{0} time{1}", JobSeekerActivityView.ResumeEdited, JobSeekerActivityView.ResumeEdited == 1 ? "" : "s")
    : HtmlLocalise("Not.Applicable", "NA");
      lblOnlineResumeHelpUsed.Text = String.Format("{0} time{1}", JobSeekerActivityView.OnlineResumeHelpUsed, JobSeekerActivityView.OnlineResumeHelpUsed == 1 ? "" : "s");
      lblHighScoreMatches.Text = JobSeekerActivityView.HighScoreMatches.ToString(CultureInfo.CurrentCulture);

      if (App.Settings.Theme == FocusThemes.Workforce) lblMatchesViewed.Text = CodeLocalise("MatchesViewed", "Matches viewed") + @":";
      else lblMatchesViewed.Text = CodeLocalise("JobPostingsViewed", "Job postings viewed") + @": ";
      lblMatchesViewed.Text += JobSeekerActivityView.MatchesViewed.ToString(CultureInfo.CurrentCulture);

      lblReferralsRequested.Text = JobSeekerActivityView.ReferralsRequested.ToString(CultureInfo.CurrentCulture);
      lblStaffReferrals.Text = JobSeekerActivityView.StaffReferrals.ToString(CultureInfo.CurrentCulture);
      lblSelfReferrals.Text = JobSeekerActivityView.SelfReferrals.ToString(CultureInfo.CurrentCulture);
      lblTotalReferrals.Text = (JobSeekerActivityView.SelfReferrals + JobSeekerActivityView.StaffReferrals).ToString(CultureInfo.CurrentCulture);
      lblJobSearchesPerformed.Text = JobSeekerActivityView.JobSearchesPerformed.ToString(CultureInfo.CurrentCulture);
      lblJobSearchesByHighGrowthSectors.Text = JobSeekerActivityView.JobSearchesByHighGrowthSectors.ToString(CultureInfo.CurrentCulture);
      lblJobSearchesSavedForJobAlerts.Text = JobSeekerActivityView.JobSearchesSavedForJobAlerts.ToString(CultureInfo.CurrentCulture);
    }

    /// <summary>
    /// Binds the days filters drop down.
    /// </summary>
    private void BindDaysFiltersDropDown()
    {
      DaysFilterDropDown.Items.Clear();

      DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last7Days.Text", "for last 7 days"), "7"));
      DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last30Days.Text", "for last 30 days"), "30"));
      DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last60Days.Text", "for last 60 days"), "60"));
      DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("Last90Days.Text", "for last 180 days"), "180"));
      DaysFilterDropDown.Items.Add(new ListItem(CodeLocalise("SinceRegistration.Text", "since registration"), "0"));

      DaysFilterDropDown.SelectedIndex = 4;
    }

    /// <summary>
    /// Handles the Command event of the ResumeLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void ResumeLinkButton_Command(object sender, CommandEventArgs e)
    {
      var resumeId = long.Parse(e.CommandArgument.ToString());

			App.ClearPageError();

			ResumeView candidate = null;

			try
			{
				candidate = ServiceClientLocator.CandidateClient(App).GetResumeById(resumeId);
			}
			catch (ArgumentNullException ex)
			{
				App.SetPageError(ex.Message);
			}
			
      ShowResume.Show(resumeId, candidate, !(App.User.IsInRole(Constants.RoleKeys.AssistJobSeekersReadOnly)));
    }

    /// <summary>
    /// Handles the Email Candidate Click event of the ResumePreviewModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
    protected void ResumePreviewModalEmailCandidate_Click(object sender, ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs e)
    {
      EmailCandidate.Show(e.CandidateId);
    }


    /// <summary>
    /// Handles the Click event of the ResumePreviewCandidatesLikeMe control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Web.Code.Controls.User.ResumePreviewModal.ResumePreviewCandidatesLikeMeEventArgs"/> instance containing the event data.</param>
    protected void ResumePreviewCandidatesLikeMe_Click(object sender, ResumePreviewModal.ResumePreviewCandidatesLikeMeEventArgs e)
    {
      CandidatesLikeThis.Show(e.CandidateId);
    }

    #region Candidates Like This private methods

    /// <summary>
    /// Handles the Email Candidate Click event of the ResumePreviewModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
    protected void CandidatesLikeThisModalEmailCandidate_Click(object sender, CandidatesLikeThisModal.CandidatesLikeThisEmailCandidateClickEventArgs e)
    {
      EmailCandidate.Show(e.CandidateId);
    }

    /// <summary>
    /// Handles the Click event of the JobSeekerActivityListEmailCandidate control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Web.WebAssist.Controls.JobSeekerActivityList.JobSeekerActivityListEmailCandidateClickEventArgs"/> instance containing the event data.</param>
    protected void JobSeekerActivityListEmailCandidate_Click(object sender, JobSeekerActivityList.JobSeekerActivityListEmailCandidateClickEventArgs e)
    {
      EmailCandidate.Show(e.CandidateId);
    }

    #endregion

    #region Helper Methods

    /// <summary>
    /// Alternatve Null text
    /// </summary>
    /// <param name="text">The text.</param>
    /// <param name="alternativeIfNull">The alternative if null.</param>
    /// <returns></returns>
    private string NullAlternative(string text, string alternativeIfNull = "-")
    {
      return string.IsNullOrEmpty(text) ? alternativeIfNull : text;
    }

    /// <summary>
    /// Calculates the time since a date
    /// </summary>
    /// <param name="actionTime">The action time.</param>
    /// <returns></returns>
    private string TimeSince(DateTime? actionTime)
    {
      if (!actionTime.HasValue) return String.Empty;

      var currentDateTime = DateTime.Now;
      var timeSince = currentDateTime.Subtract(actionTime.GetValueOrDefault());
      return timeSince.Days + " " + CodeLocalise("Days", "days") + " " + timeSince.Hours + " " +
                   CodeLocalise("Hours", "hours") + " " + timeSince.Minutes + " " + CodeLocalise("Minutes", "minutes");
    }

    /// <summary>
    /// Checks whether the job seeker profile contains any contact address data.
    /// </summary>
    /// <param name="profile">The job seeker profile.</param>
    /// <returns>true if the profile contains any contact address data, else false.</returns>
    private static bool ProfileContainsContactAddressData(JobSeekerProfileViewDto profile)
    {
      return profile.AddressLine1.IsNotNullOrEmpty() || profile.TownCity.IsNotNullOrEmpty() || profile.StateId.HasValue ||
             profile.CountryId.HasValue;
    }

    #endregion

    /// <summary>
    /// Handles the Completed event of the EmailCandidate control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void EmailCandidate_Completed(object sender, EmailJobSeekerCompletedEventArgs e)
    {
      if (CandidatesLikeThis.CandidatesLikeThisDataSource.SelectParameters.Count > 0)
      {
        CandidatesLikeThis.CandidatesLikeThisListView.DataBind();
        CandidatesLikeThis.CandidatesLikeThisModalPopup.Show();
      }
    }

    /// <summary>
    /// Fires when the update resume button is clicked to show the Update Resume modal
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard Event Args</param>
    protected void UpdateResume_Click(object sender, EventArgs e)
    {
      if (JobSeekerProfileModel.JobSeekerProfileView.DefaultResumeId.HasValue)
      {
        UpdateResume.DefaultResumeId = JobSeekerProfileModel.JobSeekerProfileView.DefaultResumeId.Value;
        UpdateResume.Show(JobSeekerProfileModel.EmploymentStatus, JobSeekerProfileModel.EducationLevel, JobSeekerProfileModel.EnrollmentStatus);
      }
    }

    /// <summary>
    /// Fires when the Update Resume modal has completed and updated at least one resume
    /// </summary>
    /// <param name="sender">The modal</param>
    /// <param name="eventargs">Arguments indicating which options were selected</param>
    protected void UpdateResume_OnUpdated(object sender, UpdateResumeEventArgs eventargs)
    {
      JobSeekerProfileModel.EmploymentStatus = eventargs.EmploymentStatus;
      JobSeekerProfileModel.EducationLevel = eventargs.EducationLevel;
      JobSeekerProfileModel.EnrollmentStatus = eventargs.SchoolStatus;

      lblEmploymentStatus.Text = eventargs.EmploymentStatus != 0 ? CodeLocalise(eventargs.EmploymentStatus) : CodeLocalise("NotSelected", "Not selected");
      lblEnrollmentStatus.Text = eventargs.SchoolStatus.IsNotNull() ? CodeLocalise(eventargs.SchoolStatus) : CodeLocalise("NotSelected", "Not selected");
      lblEducationLevel.Text = eventargs.EducationLevel.IsNotNull() ? CodeLocalise(eventargs.EducationLevel) : CodeLocalise("NotSelected", "Not selected");
    }

    /// <summary>
    /// Verifies the MSFW Status
    /// </summary>
    /// <param name="sender">Radio buttons raising the event</param>
    /// <param name="e">Standard event arguments</param>
    protected void MSFWVerifiedOptions_OnSelectedIndexChanged(object sender, EventArgs e)
    {
      var questions = new List<long>();

      var isVerified = MSFWVerifiedOptions.SelectedValue == "yes";
      if (App.Settings.PotentialMSFWQuestionsEnabled)
      {
          foreach (ListItem item in MSFWQuestions.Items)
          {
              item.Enabled = !isVerified;
              if (!isVerified)
                  item.Selected = false;
              else if (item.Selected)
                  questions.Add(item.Value.ToLong().GetValueOrDefault(0));
          } 
      }
      ServiceClientLocator.CandidateClient(App).VerifyMSFWStatus(JobSeekerProfileModel.JobSeekerProfileView.Id.GetValueOrDefault(), isVerified, questions);

			JobSeekerActivityList.Bind(true);
    }
	}
}
