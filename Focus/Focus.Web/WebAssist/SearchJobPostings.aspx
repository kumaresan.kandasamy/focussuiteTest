﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchJobPostings.aspx.cs" Inherits="Focus.Web.WebAssist.SearchJobPostings" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistJobSeekersNavigation.ascx" tagname="AssistJobSeekersNavigation" tagprefix="uc" %>
<%@ Register Src="~/Code/Controls/User/SearchJobPostings.ascx" TagName="SearchPostings" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/SavedSearch.ascx" TagName="SavedSearch" TagPrefix="uc" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
  <asp:UpdatePanel ID="UpdatePanelJobSearchCriteria" runat="server" UpdateMode="Always">
		<ContentTemplate>
      <div><uc:AssistJobSeekersNavigation ID="AssistJobSeekersNavigationMain" runat="server" /></div>
			<table style="width:100%;" role="presentation">
				<%--SEARCH JOB POSTINGS--%>
				<uc:SearchPostings ID="SearchPostings" runat="server" Visible="false" />
				<%--SAVED SEARCH--%>
				<uc:SavedSearch ID="SavedSearch" runat="server" Visible="false" />
			</table>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

