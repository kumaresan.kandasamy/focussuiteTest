﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using Focus.Core;
using Focus.Common.Extensions;
using Focus.Common;
using Constants = Focus.Core.Constants;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
	public partial class Feedback : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = CodeLocalise("Page.Title", "Manage #FOCUSASSIST#-Feedback");

			if (!IsPostBack)
			{
				LocaliseUI();
				BindIssueDropDown();
			}
		}

		/// <summary>
		/// Handles the CheckedChanged event of the IssueRadioButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void IssueRadioButton_CheckedChanged(object sender, EventArgs e)
		{
			DetailsRequired.Enabled = false;
      DetailsTextBox.Text = "";

			IssueDropDown.Enabled = IssueRadioButton.Checked;
			IssueDropDown.SelectedIndex = 0;
		}

		/// <summary>
		/// Handles the Clicked event of the SendEmailButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendEmailButton_Clicked(object sender, EventArgs e)
		{
      if (IssueRadioButton.Checked)
      {
          if (IssueDropDown.SelectedIndex == 0)
          {
              IssueDropDownRequired.Enabled = true;
              IssueDropDownRequired.IsValid = false;
              return;
          }
          if (SubIssueDropDown.SelectedIndex == 0 || SubIssueDropDown.SelectedIndex == -1)
          {
              SubIssueDropDownRequired.Enabled = true;
              SubIssueDropDownRequired.IsValid = false;
              return;
          }
      }
            
      if (String.IsNullOrEmpty(DetailsTextBox.Text))
      {
          DetailsRequired.Enabled = true;
          DetailsRequired.IsValid = false;
          return;
      }
          
			// Not localising strings here as we don't know the language of the recipient
			var subject = new StringBuilder("Feedback from ");
			subject.AppendFormat("{0} : ", App.Settings.Application);

			if (CommentQuestionRadio.Checked) 
				subject.Append("Comment");
			else if (JobWebsiteSuggestionRadio.Checked) 
				subject.Append("Suggested job source website");
			else
			{
				subject.AppendFormat(IssueDropDown.SelectedItem.Text + ": ");
				subject.AppendFormat(SubIssueDropDown.SelectedItem.Text);
			}
			
			ServiceClientLocator.CoreClient(App).SendFeedback(subject.ToString(), DetailsTextBox.TextTrimmed());

			Confirmation.Show(CodeLocalise("FeedbackConfirmation.Title", "Feedback confirmation"),
												CodeLocalise("FeedbackConfirmation.Body", "Thank you for your feedback, it has been gratefully received."),
												CodeLocalise("CloseModal.Text", "OK"));
		}

		/// <summary>
		/// Binds the issue drop down.
		/// </summary>
		private void BindIssueDropDown()
		{
			var feedbackIssues = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.FeedbackIssues).OrderBy(x => x.Text).ToList();
			
			IssueDropDown.Items.Clear();
			IssueDropDown.DataSource = !App.Settings.TalentModulePresent ? feedbackIssues.Where(x => x.Key != LookupTypes.FeedbackIssues.ToString() + ".EmployerIssues") : feedbackIssues;
			IssueDropDown.DataValueField = "Id";
			IssueDropDown.DataTextField = "Text";
			IssueDropDown.DataBind();
			IssueDropDown.Items.AddLocalisedTopDefault("IssueType.TopDefault.NoEdit", "- select issue -");
			IssueButton.Enabled = false;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SendEmailButton.Text = CodeLocalise("SendEmailButton.Text.NoEdit", "Send email");
			CommentQuestionRadio.Text = CodeLocalise("CommentQuestionRadio.Text", "I have a comment or question");
			IssueRadioButton.Text = CodeLocalise("IssueRadioButton.Text", "I am reporting an issue");
			IssueButton.Text = CodeLocalise("IssueButton.Text.NoEdit", "Go");
			JobWebsiteSuggestionRadio.Text = CodeLocalise("JobWebsiteSuggestionRadio.Text", "I would like to suggest a website to source jobs from");
			IssueDropDownRequired.ErrorMessage = CodeLocalise("IssueDropDownRequired.ErrorMessage", "Issue type required");
			DetailsRequired.ErrorMessage = CodeLocalise("DetailsRequired.ErrorMessage", "Details of message required");
			SubIssueDropDownRequired.ErrorMessage = CodeLocalise("SubIssueDropDownRequired.ErrorMessage", "Sub-Issue type required");
		  MessageLabel.Text = CodeLocalise("MessageLabel.Text", "Please enter your comments in the text box below");
		}


		/// <summary>
		/// Handles the Clicked event of the IssueButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void IssueButton_Clicked(object sender, EventArgs e)
		{
			//Temporary Settings for Having No Talent Module these options are now hidden
			var talentSettings = new List<string>{"StaffIssues.CantAccessACustomerAccount","StaffIssues.CantCreateACustomerAccount","StaffIssues.CantAssignCustomersToStaff","StaffIssues.CantEditSaveChangesToACustomerRecord","StaffIssues.CantFindACustomerRecord",
			"StaffIssues.CantReferJobSeekers","StaffIssues.FiltersProfanityCriminalBackground","StaffIssues.QueueEmployerAccountApprovals","StaffIssues.QueueJobPostApprovals","StaffIssues.QueueReferralRequestsApprovals"};
			// Hide the sub action controls
      SubIssueDropDownRequired.Enabled = DetailsRequired.Enabled = false;
          
      var selectedAction = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.FeedbackIssues).Where(x => x.Id == IssueDropDown.SelectedValueToLong()).Select(x => x.Key.Substring(15)).SingleOrDefault();
      switch (selectedAction)
			{
        case "GeneralIssues":
					var generalIssues = !App.Settings.TalentModulePresent ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.GeneralIssues).Where(x => !talentSettings.Contains(x.Key)).OrderBy(x => x.Text).ToList() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.GeneralIssues).OrderBy(x => x.Text).ToList();
					SubIssueDropDown.BindLookup(generalIssues, null, CodeLocalise("Global.IssueType.TopDefault", "- select issue -")); 
				break;

				case "JobSeekerIssues":
				var jobseekerIssues = !App.Settings.TalentModulePresent ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobSeekerIssues).Where(x => !talentSettings.Contains(x.Key)).OrderBy(x => x.Text).ToList() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobSeekerIssues).OrderBy(x => x.Text).ToList();
				SubIssueDropDown.BindLookup(jobseekerIssues, null, CodeLocalise("Global.IssueType.TopDefault", "- select issue -")); 
				break;

				case "EmployerIssues":
					var employerIssues = !App.Settings.TalentModulePresent ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmployerIssues).Where(x => !talentSettings.Contains(x.Key)).OrderBy(x => x.Text).ToList() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmployerIssues).OrderBy(x => x.Text).ToList();
					SubIssueDropDown.BindLookup(employerIssues, null, CodeLocalise("Global.IssueType.TopDefault", "- select issue -")); 
				break;

				case "CareerExplorationIssues":
					var careerIssues = !App.Settings.TalentModulePresent ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.CareerExplorationIssues).Where(x => !talentSettings.Contains(x.Key)).OrderBy(x => x.Text).ToList() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.CareerExplorationIssues).OrderBy(x => x.Text).ToList();
					SubIssueDropDown.BindLookup(careerIssues, null, CodeLocalise("Global.IssueType.TopDefault", "- select issue -")); 
				break;

				case "StaffIssues":
					var staffIssues = !App.Settings.TalentModulePresent ? ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.StaffIssues).Where(x => !talentSettings.Contains(x.Key)).OrderBy(x => x.Text).ToList() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.StaffIssues).OrderBy(x => x.Text).ToList();
					SubIssueDropDown.BindLookup(staffIssues, null, CodeLocalise("Global.IssueType.TopDefault", "- select issue -")); 
				break;
			}

      SubIssueDropDown.Visible = SubIssueDropDown.Enabled = SubIssueDropDownLabel.Visible = true;
		}



		/// <summary>
		/// Handles the CheckedChanged event of the CommentQuestionRadio control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CommentQuestionRadio_CheckedChanged(object sender, EventArgs e)
		{
			DetailsRequired.Enabled = true;
		  DetailsTextBox.Text = "";

      IssueDropDown.Enabled = IssueDropDownRequired.Enabled = SubIssueDropDown.Visible = IssueButton.Enabled = IssueRadioButton.Checked;
		  IssueDropDown.SelectedIndex = 0;
		}

    /// <summary>
    /// Handles the SelectedIndexChanged event of the SubIssueDropDown control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SubIssueDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        SubIssueDropDownRequired.Enabled = SubIssueDropDown.SelectedIndex == 0;
        DetailsRequired.Enabled = false;
    }

    /// <summary>
    /// Handles the SelectedIndexChanged event of the IssueDropDown control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void IssueDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        SubIssueDropDownRequired.Enabled = DetailsRequired.Enabled = false;
            
        if (IssueDropDown.SelectedIndex == 0)
        {
            IssueButton.Enabled = false;
            IssueDropDownRequired.IsValid = false;
            IssueDropDownRequired.Enabled = true;
        }
        else
        {
            IssueButton.Enabled = true;
            IssueDropDownRequired.Enabled = false;
        }
        SubIssueDropDown.Items.Clear();
        SubIssueDropDown.Visible = false;
    }
	}
}
