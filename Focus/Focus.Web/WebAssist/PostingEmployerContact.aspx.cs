﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Assist;
using Focus.Web.Code;
using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	public partial class PostingEmployerContact : PageBase
	{
		private PostingEmployerContactModel _model;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if(!IsPostBack)
			{
				LocaliseUI();
				ReturnToJobDevelopmentLink.NavigateUrl = UrlBuilder.JobDevelopment();
				GetPageModel();
				BindModel();
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ReturnToJobDevelopmentLink.Text = CodeLocalise("ReturnToJobDevelopmentLink.Text", "return to #BUSINESS# development");
		}

		/// <summary>
		/// Gets the page model.
		/// </summary>
		private void GetPageModel()
		{
			long postingId = 0;
			long? personId = null;

			if (Page.RouteData.Values.ContainsKey("id"))
				long.TryParse(Page.RouteData.Values["id"].ToString(), out postingId);

			if (postingId == 0)
				throw new Exception(FormatError(ErrorTypes.PostingNotFound, "Invalid posting id"));

			if (Page.RouteData.Values.ContainsKey("personid"))
			{
				long personIdInQuerystring;
				long.TryParse(Page.RouteData.Values["personid"].ToString(), out personIdInQuerystring);

				if (personIdInQuerystring > 0)
					personId = personIdInQuerystring;
			}

			_model = ServiceClientLocator.PostingClient(App).GetPostingEmployerContactModel(postingId, personId);
		}

		/// <summary>
		/// Binds the model.
		/// </summary>
		private void BindModel()
		{
			var unknownText = CodeLocalise("Global.Unknown.Text", "Unknown");
			TitleLiteral.Text = (_model.JobSeekerName.IsNullOrEmpty()) ? CodeLocalise("TitleLiteralNoResume.Text", "Contact #BUSINESS#:LOWER") : CodeLocalise("TitleLiteralWithResume.Text", "Contact #BUSINESS#:LOWER about {0}", _model.JobSeekerName);
			EmployerLiteral.Text = (_model.PostingContactDetails.IsNotNull() && _model.PostingContactDetails.EmployerName.IsNotNullOrEmpty()) ? _model.PostingContactDetails.EmployerName : unknownText;
			ContactLiteral.Text = (_model.PostingContactDetails.IsNotNull() && _model.PostingContactDetails.ContactName.IsNotNullOrEmpty()) ? _model.PostingContactDetails.ContactName : unknownText;
			TelephoneLiteral.Text = (_model.PostingContactDetails.IsNotNull() && _model.PostingContactDetails.TelephoneNumber.IsNotNullOrEmpty()) ? _model.PostingContactDetails.TelephoneNumber : unknownText;
			EmailLiteral.Text = (_model.PostingContactDetails.IsNotNull() && _model.PostingContactDetails.EmailAddress.IsNotNullOrEmpty()) ? _model.PostingContactDetails.EmailAddress : unknownText;
			AddressLiteral.Text = (_model.PostingContactDetails.IsNotNull() && _model.PostingContactDetails.Address.IsNotNull() && (_model.PostingContactDetails.Address.TownCity.IsNotNullOrEmpty() ||
			                                                                                                                        _model.PostingContactDetails.Address.State.IsNotNullOrEmpty() ||
			                                                                                                                        _model.PostingContactDetails.Address.PostcodeZip.IsNotNullOrEmpty()))
			                      	? _model.PostingContactDetails.Address.ToString()
			                      	: unknownText;
		}
	}
}