﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageAssist.aspx.cs" Inherits="Focus.Web.WebAssist.ManageAssist" %>

<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/ManageAssistNavigation.ascx" tagname="ManageAssistNavigation" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/CareerDefaults.ascx" tagname="CareerDefaults" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/CareerMessages.ascx" tagname="CareerMessages" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/TalentDefaults.ascx" tagname="TalentDefaults" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/TalentMessages.ascx" tagname="TalentMessages" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistDefaults.ascx" tagname="AssistDefaults" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssistMessages.ascx" tagname="AssistMessages" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/Localise.ascx" tagName="Localise" tagPrefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:ManageAssistNavigation ID="ManageAssistNavigationMain" runat="server" />

	<h1><%= HtmlLocalise("SystemDefaults.Header", "System defaults")%></h1>

	<p class="pageNav">
		<asp:PlaceHolder ID="TalentActivitiesPlaceHolder" runat="server">
		  <asp:Literal ID="LocaliseTalentDefaultsLink" runat="server" />
			<asp:LinkButton ID="TalentDefaultsLink" runat="server" Visible="false"  OnClick="TalentDefaultsLink_Clicked" CausesValidation="False" />
			<strong><asp:Literal ID="TalentDefaultsLinkDisplayOnly" runat="server" /></strong>
			&nbsp;|&nbsp;
      <asp:Literal ID="LocaliseTalentMessagesLink" runat="server" />
			<asp:LinkButton ID="TalentMessagesLink" runat="server" OnClick="TalentMessagesLink_Clicked" CausesValidation="False"><%= HtmlLocalise("TalentMessages.LinkText", "#FOCUSTALENT# messages")%></asp:LinkButton>
			<strong><asp:Literal ID="TalentMessagesLinkDisplayOnly" runat="server" Visible="false" /></strong>
			&nbsp;|&nbsp;
		</asp:PlaceHolder>
    <asp:Literal ID="LocaliseCareerDefaultsLink" runat="server" />
		<asp:LinkButton ID="CareerDefaultsLink" runat="server" OnClick="CareerDefaultsLink_Clicked" CausesValidation="False"><%= HtmlLocalise("CareerDefaults.LinkText", "#FOCUSCAREER# settings")%></asp:LinkButton>
		<strong><asp:Literal ID="CareerDefaultsLinkDisplayOnly" runat="server" Visible="false" /></strong>
		&nbsp;|&nbsp;
     <asp:Literal ID="LocaliseCareerMessagesLink" runat="server" />
		<asp:LinkButton ID="CareerMessagesLink" runat="server" OnClick="CareerMessagesLink_Clicked" CausesValidation="False"><%= HtmlLocalise("CareerMessages.LinkText", "#FOCUSCAREER# messages")%></asp:LinkButton>
		<strong><asp:Literal ID="CareerMessagesLinkDisplayOnly" runat="server" Visible="false" /></strong>
		&nbsp;|&nbsp;
    <asp:Literal ID="LocaliseAssistDefaultsLink" runat="server" />
		<asp:LinkButton ID="AssistDefaultsLink" runat="server" OnClick="AssistDefaultsLink_Clicked" CausesValidation="False"><%= HtmlLocalise("AssistDefaults.LinkText", "#FOCUSASSIST# settings")%></asp:LinkButton>
		<strong><asp:Literal ID="AssistDefaultsLinkDisplayOnly" runat="server" Visible="false" /></strong>
		&nbsp;|&nbsp;
    <asp:Literal ID="LocaliseAssistMessagesLink" runat="server" />
		<asp:LinkButton ID="AssistMessagesLink" runat="server" OnClick="AssistMessagesLink_Clicked" CausesValidation="False"><%= HtmlLocalise("AssistMessages.LinkText", "#FOCUSASSIST# messages")%></asp:LinkButton>
		<strong><asp:Literal ID="AssistMessagesLinkDisplayOnly" runat="server" Visible="false" /></strong>
    <asp:PlaceHolder runat="server" ID="LocalisePlaceHolder">
      &nbsp;|&nbsp;
      <asp:Literal ID="LocaliseLocaliseLink" runat="server" />
		  <asp:LinkButton ID="LocaliseLink" runat="server" OnClick="LocaliseLoginLink_Clicked" CausesValidation="False"><%= HtmlLocalise("Localise.LinkText", "Localise")%></asp:LinkButton>
		  <strong><asp:Literal ID="LocaliseLinkDisplayOnly" runat="server" Visible="false" /></strong>
    </asp:PlaceHolder>
   </p>

	<uc:TalentDefaults ID="TalentDefaults" runat="server" Visible="false" />
	<uc:TalentMessages ID="TalentMessages" runat="server" Visible="false" />
	<uc:CareerDefaults ID="CareerDefaults" runat="server" Visible="false" />
	<uc:CareerMessages ID="CareerMessages" runat="server" Visible="false" />
	<uc:AssistDefaults ID="AssistDefaults" runat="server" Visible="false" />
	<uc:AssistMessages ID="AssistMessages" runat="server" Visible="false" />
  	<uc:Localise ID="Localise"	runat="server" Visible="False" />
</asp:Content>

