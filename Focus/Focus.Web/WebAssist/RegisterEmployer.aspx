﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegisterEmployer.aspx.cs" Inherits="Focus.Web.WebAssist.RegisterEmployer" %>

<%@ Register src="~/Code/Controls/User/EmployerRegistration.ascx" tagname="EmployerRegistration" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="Navigation" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:EmployerRegistration ID="Registration" runat="server" OnRegistrationCompleted="Registration_OnRegistrationCompleted" />

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="Confirmation" runat="server" Height="250px" Width="400px" OnOkCommand="Confirmation_OkCommand" />
</asp:Content>
