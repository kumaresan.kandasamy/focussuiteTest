﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Job.aspx.cs" Inherits="Focus.Web.WebAssist.Job" MaintainScrollPositionOnPostback="true" %>
<%@ Register src="~/WebAssist/Controls/TabNavigation.ascx" tagname="TabNavigation" tagprefix="uc" %>
<%@ Register src="~/Code/Controls/User/EmailEmployeeModal.ascx" tagName="EmailEmployeeModal" tagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/JobActionModal.ascx" TagPrefix="uc" TagName="JobActionModal"%>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagprefix="uc" tagname="ConfirmationModal"  %>
<%@ Register TagPrefix="uc" TagName="NotesAndReminders" Src="~/WebAssist/Controls/NotesAndReminders.ascx" %>
<%@ Register TagPrefix="uc" TagName="JobReferralSummaryList" Src="~/WebAssist/Controls/JobReferralSummaryList.ascx" %>
<%@ Register src="~/Code/Controls/User/JobActivityList.ascx" tagName="JobActivityList" tagPrefix="uc" %>
<%@ Register src="~/WebAssist/Controls/AssignedOffices.ascx" tagName="AssignedOffices" tagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
  <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<div id="JobOrderJobView">
  <asp:HyperLink runat="server" ID="ReturnLink"></asp:HyperLink> 
  <br/>
    <table role="presentation" id="JobOrderTable" style="width:100%;">
    <tr>
      <td><h1><asp:Literal runat="server" ID="JobViewTitleHeader"></asp:Literal></h1></td>
      <td><h2><span id="JobId" class="right"><%= HtmlLocalise("JobReference.Label", "Job ID")%>: <asp:Literal ID="JobReference" runat="server" /></span></h2></td>
      <td><asp:Button runat="server" ID="PrintButton" CssClass="button2 right" OnClick="PrintButton_Click"/></td>
    </tr>
    <tr>
      <td></td>
      <td><h2><span id="CareerJobId" class="right">(<%= HtmlLocalise("CareerJobReference.Label", "External Job ID")%>: <asp:Literal ID="CareerJobReference" runat="server" />)</span></h2></td>
      <td><asp:Button ID="ExpandCollapseAllButton" runat="server" class="button3" style="float:right" ClientIDMode="Static"/></td>
    </tr>
  </table>
  
  <asp:PlaceHolder runat="server" ID="ApplicationStatusPlaceHolder">
    <asp:Panel ID="ApplicantStatusHeaderPanel" runat="server" CssClass="singleAccordionTitle">
      <table role="presentation" class="accordionTable">
		  <tr  class="multipleAccordionTitle">
			  <td>
				<asp:Image ID="ApplicantStatusHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
				  &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ApplicantStatus.Label", "Applicant Status")%></a></span></td>
		  </tr>
	  </table>
    </asp:Panel>
	  <div class="singleAccordionContentWrapper">
		  <asp:Panel runat="server" ID="ApplicationStatusPanel" CssClass="singleAccordionContent collapsiblePanelFix">
			  <asp:Literal runat="server" ID="Status1" /><asp:HyperLink runat="server" ID="ApplicantsLink" /><asp:Literal runat="server" ID="Status2"/><asp:HyperLink runat="server" ID="RecommendedLink" /><asp:Literal runat="server" ID="Status3" />
		  </asp:Panel>
	   </div>
    <act:CollapsiblePanelExtender runat="server" ID="ApplicationStatusPanelExtender" TargetControlID="ApplicationStatusPanel" ExpandControlID="ApplicantStatusHeaderPanel" CollapseControlID="ApplicantStatusHeaderPanel" Collapsed="False" ImageControlID="ApplicantStatusHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
  </asp:PlaceHolder>

	<asp:Panel ID="JobOrderDetailsHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table role="presentation" class="accordionTable">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="JobOrderDetailsHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><asp:HyperLink runat="server" ID="JobOrderLabel" href="#" /></span></td>
		</tr>
	</table>
  </asp:Panel>
	<div class="singleAccordionContentWrapper">
		<asp:Panel runat="server" ID="JobOrderDetailsPanel" CssClass="singleAccordionContent">
			<table role="presentation" style="width:100%;">
				<tr>
					<td style="width:60%"><%= HtmlLocalise("PostedBy.Label", "Posted by")%>: <asp:Label runat="server" ID="lblPostedBy"></asp:Label></td>
					<td style="width:40%"><asp:Label runat="server" ID="LastEditedTextLabel"></asp:Label>: <asp:Label runat="server" ID="lblLastEdited"></asp:Label></td>
				</tr>
				<tr>
					<td><%= HtmlLocalise("EmployerTelephone.Label", "#BUSINESS# telephone")%>: <asp:Label runat="server" ID="lblEmployerTelephone"></asp:Label></td>
					<td><%= HtmlLocalise("TimeSinceLastEdit.Label", "Time since last edit")%>: <asp:Label runat="server" ID="lblTimeSinceLastEdit"></asp:Label></td>
				</tr>
				<tr>
					<td><%= HtmlLocalise("EmployerEmail.Label", "#BUSINESS# email")%>: <asp:Label runat="server" ID="lblEmployerEmail"></asp:Label></td>
					<td><asp:Label runat="server" ID="LastRefreshedTextLabel"></asp:Label>: <asp:Label runat="server" ID="lblLastRefreshed"></asp:Label></td>
				</tr>
				<tr>
					<td><asp:Label runat="server" ID="FirstPostedTextLabel"></asp:Label>: <asp:Label runat="server" ID="lblPostedDate"></asp:Label></td>
					<td><%= HtmlLocalise("TimeSinceLastRefresh.Label", "Time since last refresh")%>: <asp:Label runat="server" ID="lblTimeSinceLastRefresh"></asp:Label></td>
				</tr>
				<tr>
					<td><asp:Label runat="server" ID="TimeSinceTextLabel"></asp:Label>: <asp:Label runat="server" ID="lblTimeSinceCreation"></asp:Label></td>
					<td>
					  <asp:Label runat="server" ID="ClosingDateTextLabel"></asp:Label>: <asp:Label runat="server" ID="lblClosingDate"></asp:Label>
					</td>
				</tr>
				<tr>
					<td><asp:CheckBox ID="CriminalRecordExclusionCheckBox" runat="server" TextAlign="Right" ClientIDMode="Static" OnCheckedChanged="CriminalRecordExclusionCheckBox_CheckChanged" AutoPostBack="True"/></td>
					<td>
					  <asp:PlaceHolder runat="server" ID="TimeUntilClosingPlaceHolder">
					    <asp:Label runat="server" ID="TimeUntilClosingLabel"></asp:Label>:
              <asp:Label runat="server" ID="lblTimeUntilClosing"></asp:Label>
            </asp:PlaceHolder>
					</td>
				</tr>
				<tr id="CreditCheckRequiredRow" runat="server">
					<td><asp:CheckBox ID="CreditCheckRequiredCheckBox" runat="server" TextAlign="Right" ClientIDMode="Static" OnCheckedChanged="CreditCheckRequiredCheckBox_CheckChanged" AutoPostBack="True"/></td>
					<td></td>
				</tr>
				<tr>
					<td><asp:Label runat="server" ID="JobConditionsLabel"></asp:Label><asp:Literal ID="ForeignLabourCertification" runat="server"/></td>
					<td></td>
				</tr>
				<tr id="VeteranPriorityRow" runat="server">
					<td colspan="2">
						<br />
						<asp:Literal ID="VeteranPriorityLiteral" runat="server" EnableViewState="false" />
					</td>
				</tr>
			</table>
		</asp:Panel>
	</div>
  <act:CollapsiblePanelExtender runat="server" ID="JobOrderDetailsPanelExtender" TargetControlID="JobOrderDetailsPanel" ExpandControlID="JobOrderDetailsHeaderPanel" CollapseControlID="JobOrderDetailsHeaderPanel" Collapsed="True" ImageControlID="JobOrderDetailsHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />


  <asp:Panel ID="DescriptionHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table role="presentation" class="accordionTable" style="width:100%;">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="DescriptionPanelImage" runat="server" AlternateText="." Height="22" Width="22"/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("Description", "Description")%></a></span></td>
     </tr>
	</table>
  </asp:Panel>
	<div class="singleAccordionContentWrapper">
		<asp:Panel runat="server" ID="DescriptionPanel" CssClass="singleAccordionContent">
			 <table role="presentation" style="width:100%;">
				<tr>
					<td style="width:50%">
						<%= HtmlLocalise("PreviewPrint.Label", "")%>&nbsp;
						<asp:LinkButton runat="server" ID="ApplicantsViewOfJobButton" OnClick="ApplicantsViewOfJobButton_Click" Visible="false"/>
					</td>
					<td style="width:50%; text-align:right;">
						<table role="presentation">
						 <tr id="JobActionRow" runat="server">
							<td runat="server" ID="EditJobButtonCell"><div class="tooltip right EditJob"><asp:HyperLink runat="server" ID="EditLinkButton" onmouseout="restoreTitle(this,'Edit');" onmouseover="removeTitle(this);" onblur="javascript:;" onfocus="javascript:;" ><div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("EditJobToolTip", "Edit")%>"></div><asp:Label ID="EditLinkLabel" runat="server" CssClass="sr-only" /></asp:HyperLink></div></td>
							<td runat="server" ID="RefreshJobButtonCell"><div class="tooltip right Refresh"><asp:LinkButton runat="server" ID="RefreshJobButton" OnClick="RefreshJobLinkButton_Click" onmouseout="restoreTitle(this,'Refresh');" onmouseover="removeTitle(this);" onblur="javascript:;" onfocus="javascript:;" ><div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("RefreshJobToolTip", "Refresh")%>"></div><asp:Label ID="RefreshJobLabel" runat="server" CssClass="sr-only"/></asp:LinkButton></div></td>
							<td runat="server" ID="CloseJobButtonCell"><div class="tooltip right CloseJob"><asp:LinkButton runat="server" ID="CloseJobButton" OnClick="CloseJobLinkButton_Click"  onmouseout="restoreTitle(this,'Close Job');" onmouseover="removeTitle(this);" onblur="javascript:;" onfocus="javascript:;" ><div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("CloseJobToolTip", "Close")%>"></div><asp:Label ID="CloseJobLabel" runat="server" CssClass="sr-only"/></asp:LinkButton></div></td>
							<td runat="server" ID="HoldJobButtonCell"><div class="tooltip right HoldJob"><asp:LinkButton runat="server" ID="HoldJobLinkButton" OnClick="HoldJobLinkButton_Click"  onmouseout="restoreTitle(this,'Hold Job');" onmouseover="removeTitle(this);" onblur="javascript:;" onfocus="javascript:;" ><div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("HoldJobToolTip", "Hold")%>"></div><asp:Label ID="HoldJobLabel" runat="server" CssClass="sr-only" /></asp:LinkButton></div></td>
							<td runat="server" ID="ReactivateJobButtonCell"><div class="tooltip right Reactivate"><asp:LinkButton runat="server" ID="ReactivateJobLinkButton" OnClick="ReactivateJobLinkButton_Click" onmouseout="restoreTitle(this,'Reactivate Job');" onmouseover="removeTitle(this);" onblur="javascript:;" onfocus="javascript:;" ><div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("ReactivateJobToolTip", "Reactivate")%>"></div><asp:Label ID="ReactivateJobLabel" runat="server" CssClass="sr-only"/></asp:LinkButton></div></td>
							<td runat="server" ID="DuplicateJobButtonCell"><div class="tooltip right DuplicateJob"><asp:LinkButton runat="server" ID="DuplicateJobButton" OnClick="DuplicateJobLinkButton_Click" onmouseout="restoreTitle(this,'Duplicate Job');" onmouseover="removeTitle(this);" onblur="javascript:;" onfocus="javascript:;" ><div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("DuplicateJobToolTip", "Duplicate")%>"></div><asp:Label ID="DuplicateJobLabel" runat="server" CssClass="sr-only" /></asp:LinkButton></div></td>
							<td runat="server" ID="DeleteButtonCell"><div class="tooltip right DeleteJob"><asp:LinkButton runat="server" ID="DeleteJobButton" OnClick="DeleteJobLinkButton_Click" onmouseout="restoreTitle(this,'Delete Job');" onmouseover="removeTitle(this);" onblur="javascript:;" onfocus="javascript:;" ><div class="tooltipImage toolTipNew" title="<%= HtmlLocalise("DeleteJobToolTip", "Delete")%>"></div><asp:Label ID="DeleteJobLabel" runat="server" CssClass="sr-only" /></asp:LinkButton></div></td>
							<td runat="server" ID="ViewMatchesButtonCell"><asp:Button ID="ViewMatchesButton" runat="server" CssClass="button3 right" OnClick="ViewMatchesButton_Click" OnClientClick="javascript:showProcessing();" /></td>
						</tr>
						</table>
					</td>
				</tr>
			</table>
			<table role="presentation" style="width:100%;">
				<tr>
					<td style="width:50%"></td>
					<td style="width:50%">
						<asp:LinkButton runat="server" ID="ViewJobAsEmployerLinkButton" OnClick="ViewJobAsEmployerLinkButton_Click" CausesValidation="True" OnClientClick="return ViewJobAsEmployerLinkButton_Validate()" ValidationGroup="LinkButtonValidation"></asp:LinkButton>
						<br/>
						<asp:Label runat="server" ID="ViewJobAsEmployerValidationErrorLabel" CssClass="error" ClientIDMode="Static" style="display: none"></asp:Label>
					</td>
				</tr>
				<tr>
					<td><asp:Label runat="server" ID="SuppressedLabel"></asp:Label>: <asp:Literal runat="server" ID="JobOrderSuppressed"></asp:Literal></td>
					<td><asp:LinkButton runat="server" ID="PreviewJobOrderLinkButton" OnClick="PreviewJobOrderLinkButton_Click"></asp:LinkButton></td>
				</tr>
				<tr>
				 <td><%= HtmlLocalise("NumberOfOpenings.Label", "Number of openings")%>: <asp:Literal ID="NumberOfOpenings" runat="server" /></td>
				 <td>
					 <asp:LinkButton runat="server" ID="AccessEmployersAccountLinkButton" onclick="AccessEmployersAccountLinkButton_Click" CausesValidation="True" OnClientClick="return AccessEmployersAccountLinkButton_Validate()" ValidationGroup="LinkButtonValidation"></asp:LinkButton>
					 <br/>
					 <asp:Label runat="server" ID="AccessEmployersAccountValidationErrorLabel" CssClass="error" ClientIDMode="Static" style="display: none"></asp:Label>
				 </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><asp:LinkButton runat="server" ID="EmailEmployeeLinkButton" onclick="EmailEmployeeLinkButton_Click"></asp:LinkButton></td>
				</tr>
				<tr>
					<td><%= HtmlLocalise("PositionDetails.Label", "Position type")%>: <asp:Literal ID="PositionDetails" runat="server" /></td>
				 </tr>
				<tr>
					<td><%= HtmlLocalise("JobStatus.Label", "Job Status")%>: <asp:Literal ID="JobStatus" runat="server" /></td>
				</tr>
				<tr>
					<td colspan="4"><%= HtmlLocalise("Confidential.Label", "Confidential")%>: <asp:Literal ID="Confidential" runat="server" /></td>
				</tr>
			</table><br/>
			<div><p><asp:Literal ID="JobDescription" runat="server" /></p></div>
		 <table role="presentation" style="width:100%;">
				<tr id="JobRequirementsHeaderRow" runat="server">
					<td><strong><%= HtmlLocalise("JobRequirements.Label", "Job requirements")%></strong></td>
				</tr>
				<tr  id="JobRequirementsRow" runat="server">
					<td>
							<div><p><asp:Literal ID="JobRequirements" runat="server" /></p></div>
						</td>
				</tr>
				<tr id="JobDetailsHeaderRow" runat="server">
					<td><strong><%= HtmlLocalise("JobDetails.Label", "Job details")%></strong></td>
				</tr>
				<tr id="JobDetailsRow" runat="server">
					<td>
							<div><p><asp:Literal ID="JobDetails" runat="server" /></p></div>
						</td>
				</tr>
				<tr id="JobSalaryBenefitsHeaderRow" runat="server">
					<td><strong><%= HtmlLocalise("JobSalaryBenefits.Label", "Job salary and benefits")%></strong></td>
				</tr>
				<tr id="JobSalaryBenefitsRow" runat="server">
					<td>
							<div><p><asp:Literal ID="JobSalaryBenefits" runat="server" /></p></div>
						</td>
				</tr>
				<tr id="JobRecruitmentInformationHeaderRow" runat="server">
					<td>
					<strong><%= HtmlLocalise("JobRecruitmentInformation.Label", "Job recruitment information")%></strong></td>
				</tr>
				<tr id="JobRecruitmentInformationRow" runat="server">
					<td>
							<div><p><asp:Literal ID="JobRecruitmentInformation" runat="server" /></p></div>
						</td>
				</tr>
			</table>
		</asp:Panel>
	</div>
  <act:CollapsiblePanelExtender runat="server" ID="DescriptionPanelExtender" TargetControlID="DescriptionPanel" ExpandControlID="DescriptionHeaderPanel" CollapseControlID="DescriptionHeaderPanel" Collapsed="True" ImageControlID="DescriptionPanelImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
  
	
	 <asp:Panel ID="ActivitySummaryHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table role="presentation" class="accordionTable" style="width:100%;">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="ActivitySummaryPanelImage" runat="server" AlternateText="." Height="22" Width="22" />
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ActivitySummary", "Activity Summary")%></a></span></td>
		</tr>
	</table>
  </asp:Panel>
	<div class="singleAccordionContentWrapper">
  <asp:Panel runat="server" ID="ActivitySummaryPanel" CssClass="singleAccordionContent">
    <table role="presentation" style="width:100%;">
			<tr>
				<td><%= HtmlLocalise("SeekersViewed.Label", "Invited #CANDIDATETYPE# who viewed the job")%>: <asp:Literal ID="SeekersViewed" runat="server" /></td>	
				<td><%= HtmlLocalise("ApplicantsInterviewed.Label", "Applicants interviewed/pending interviews") %>: <asp:Literal runat="server" ID="ApplicantsInterviewed"></asp:Literal></td>
	    </tr>
			<tr>
				<td><%= HtmlLocalise("SeekersDidNotView.Label", "Invited #CANDIDATETYPE# who did not view the job")%>: <asp:Literal ID="SeekersDidNotView" runat="server" /></td>
       <td><%= HtmlLocalise("ApplicantsFailedToShow.Label", "Applicants who failed to show") %>: <asp:Literal runat="server" ID="ApplicantsFailedToShow"></asp:Literal></td>
	    </tr>
			<tr>
				<td><%= HtmlLocalise("SeekersClicked.Label", "Invited #CANDIDATETYPE# who clicked \"How to Apply\"")%>: <asp:Literal ID="SeekersClicked" runat="server" /></td>
				<td><%= HtmlLocalise("ApplicantsDenied.Label", "Applicants denied interviews") %>: <asp:Literal runat="server" ID="ApplicantsDenied"></asp:Literal></td>
	    </tr>
			<tr>
				<td><%= HtmlLocalise("SeekersDidNotClick.Label", "Invited #CANDIDATETYPE# who did not click \"How to Apply\"")%>: <asp:Literal ID="SeekersDidNotClick" runat="server" /></td>
				<td><%= HtmlLocalise("ApplicantsHired.Label", "Applicants hired") %>: <asp:Literal runat="server" ID="ApplicantsHired"></asp:Literal></td>
       </tr>
			<tr>
				<td><%= HtmlLocalise("StaffReferrals.Label", "Staff referrals")%>: <asp:Literal ID="StaffReferrals" runat="server" /></td>
				<td><%= HtmlLocalise("ApplicantsRejected.Label", "Applicants rejected") %>: <asp:Literal runat="server" ID="ApplicantsRejected"></asp:Literal></td>
			</tr>
			<tr>
				<td><%= HtmlLocalise("SelfReferrals.Label", "Self-referrals")%>: <asp:Literal ID="SelfReferrals" runat="server" /></td>
				<td><%= HtmlLocalise("JobOffersRefused.Label", "Job offers refused") %>: <asp:Literal runat="server" ID="JobOffersRefused"></asp:Literal></td>
			</tr>
			<tr>
				<td><%= HtmlLocalise("TotalReferrals.Label", "Total referrals")%>: <asp:Literal ID="TotalReferrals" runat="server" /></td>
				<td><%= HtmlLocalise("ApplicantsDidNotApply.Label", "Applicant did not apply") %>: <asp:Literal runat="server" ID="ApplicantsDidNotApply"></asp:Literal></td>
			</tr>
			<tr>
				<td><%= HtmlLocalise("EmployerInvitationsSent.Label", "#BUSINESS# invitations sent")%>: <asp:Literal ID="EmployerInvitationsSent" runat="server" /></td>
			</tr>
		<%--	
			<tr>
		    <td width="50%"><%= HtmlLocalise("TotalMatches.Label", "Total matches")%>: <asp:Literal ID="TotalMatches" runat="server" /></td>
				<td width="50%"></td>
	    </tr>
			<tr>
				 <td><%= HtmlLocalise("35starMatches.Label", "3-5-star matches") %>: <asp:Literal runat="server" ID="StarMatches"></asp:Literal></td>
			</tr>
			<tr>
				 <td><%= HtmlLocalise("35starApplicants.Label", "3-5-star applicants")%>: <asp:Literal ID="StarApplicants" runat="server" /></td>
			</tr>
			<tr>
				<td><%= HtmlLocalise("35starDidNotView.Text", "3-5-star applicants employer did not view") %>: <asp:Literal runat="server" ID="StarDidNotView"></asp:Literal></td>
			</tr>
			<tr>
				<td><%= HtmlLocalise("35starReferrals.Label", "3-5-star referrals")%>: <asp:Literal ID="StarReferrals" runat="server" /></td>
			</tr>--%>
    </table>
  </asp:Panel>
	</div>
  <act:CollapsiblePanelExtender runat="server" ID="ActivitySummaryPanelExtender" TargetControlID="ActivitySummaryPanel" ExpandControlID="ActivitySummaryHeaderPanel" Collapsed="True" CollapseControlID="ActivitySummaryHeaderPanel" ImageControlID="ActivitySummaryPanelImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
  

  <asp:Panel ID="JobOrderActivityHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table role="presentation" class="accordionTable">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="JobOrderActivityPanelImage" runat="server" AlternateText="." Height="22" Width="22" />
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><asp:HyperLink runat="server" ID="ActivityLogLabel" href="#" /></span></td>
		</tr>
	</table>
  </asp:Panel>
	<div class="singleAccordionContentWrapper">
  <asp:Panel runat="server" ID="JobOrderActivityPanel" CssClass="singleAccordionContent">
    <uc:JobActivityList ID="ActivityList" runat="server" />
    <table role="presentation">
      <tr>
      <td runat="server" ID="ActionLabelCell"><%= HtmlLabel(JobOrdersActionDropDown,"WebAssist.JobOrderDashboard.Action.Label", "Action")%></td>
       <td style="text-align:left;">
        &nbsp;
				<asp:DropDownList runat="server" ID="JobOrdersActionDropDown" Width="350" Visible="true" ClientIDMode="Static" ValidationGroup="ActionValidationGroup"/>&nbsp;
        <asp:Button ID="JobOrdersActionButton" runat="server" class="button1" OnClientClick="javascript:return jobOrdersActionButtonClicked();" OnClick="JobOrdersActionButton_Clicked" ValidationGroup="ActionValidationGroup"/>
			</td>
    </tr>
		<tr>
			<td style="width:40px">&nbsp;</td>
      <td>
          &nbsp;
					<asp:CustomValidator ID="ActionValidator" runat="server" CssClass="error" ControlToValidate="JobOrdersActionDropDown" SetFocusOnError="True" ClientValidationFunction="JobOrdersActionButton_Validate"  ValidationGroup="ActionValidationGroup"/>
				</td>
      <td colspan="2">&nbsp;</td>
		</tr>
  </table>
  <table role="presentation">
    <tr>
			<td style="width:40px"></td>
      <td>
        &nbsp;
					<asp:DropDownList runat="server" ID="SubActionDropDown" Width="350" Visible="false" ClientIDMode="Static"/>&nbsp;
          <asp:Button ID="SubActionButton" Visible="false" runat="server" ClientIDMode="static" CssClass="button1" OnClick="SubActionButton_Clicked"/>
					<asp:RequiredFieldValidator ID="SubActionDropDownRequired" runat="server" ValidationGroup="SubAction" ControlToValidate="SubActionDropDown" CssClass="error" />
				</td>
    </tr>
    <tr>
			<td style="width:40px">&nbsp;</td>
      <td>
          &nbsp;
					<asp:CustomValidator ID="SubActionButtonValidator" runat="server" CssClass="error" ControlToValidate="SubActionDropDown" SetFocusOnError="True" />
				</td>
      <td colspan="2">&nbsp;</td>
		</tr>
  </table>  
  </asp:Panel>
	</div>
  <act:CollapsiblePanelExtender runat="server" ID="JobOrderActivityPanelExtender" TargetControlID="JobOrderActivityPanel" ExpandControlID="JobOrderActivityHeaderPanel" CollapseControlID="JobOrderActivityHeaderPanel" Collapsed="True" ImageControlID="JobOrderActivityPanelImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
  

  <asp:Panel ID="ReferralSummaryHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table role="presentation" class="accordionTable">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="ReferralSummaryImage" runat="server" AlternateText="." Height="22" Width="22"/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("ReferralSummary.Label", "Referral Summary")%></a></span></td>
		</tr>
	</table>
  </asp:Panel>
	<div class="singleAccordionContentWrapper">
  <asp:Panel runat="server" ID="ReferralSummaryPanel" CssClass="singleAccordionContent">
    <uc:JobReferralSummaryList ID="ReferralSummaryList" runat="server" />
  </asp:Panel>
	</div>
  <act:CollapsiblePanelExtender runat="server" ID="ReferralSummaryPanelExtender" TargetControlID="ReferralSummaryPanel" ExpandControlID="ReferralSummaryHeaderPanel" CollapseControlID="ReferralSummaryHeaderPanel" Collapsed="True" ImageControlID="ReferralSummaryImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" />
  

  <asp:Panel ID="NotesRemindersHeaderPanel" runat="server" CssClass="singleAccordionTitle">
    <table role="presentation" class="accordionTable">
		<tr  class="multipleAccordionTitle">
			<td>
				<asp:Image ID="NotesRemindersPanelImage" runat="server" AlternateText="." Height="22" Width="22"/>
				&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("NotesAndReminders.Label", "Notes and Reminders")%></a></span></td>
		</tr>
	</table>
  </asp:Panel>
	<div class="singleAccordionContentWrapper">
  <asp:Panel runat="server" ID="NotesRemindersPanel" CssClass="singleAccordionContent">
		<uc:NotesAndReminders ID="NotesReminders" runat="server" />
  </asp:Panel>
	</div>
  <act:CollapsiblePanelExtender runat="server" ID="NotesRemindersPanelExtender" TargetControlID="NotesRemindersPanel" ExpandControlID="NotesRemindersHeaderPanel" CollapseControlID="NotesRemindersHeaderPanel" 
																Collapsed="True" ImageControlID="NotesRemindersPanelImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" 
																BehaviorID="NotesRemindersCollapsiblePanel"/>

	<asp:PlaceHolder runat="server" ID="OfficePlaceHolder">
		<asp:Panel ID="AssignedOfficeHeaderPanel" runat="server" CssClass="singleAccordionTitle" style="margin-bottom: 20px !important;">
			<table role="presentation" class="accordionTable">
					<tr>
						<td>
							<asp:Image ID="AssignedOfficeHeaderImage" runat="server" AlternateText="." Height="22" Width="22"/>
							&nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("AssignedOffice.Label", "Assigned Office")%></a></span></td>
					</tr>
				</table>
		</asp:Panel>
		<div class="singleAccordionContentWrapper">
		<asp:Panel ID="AssignedOfficePanel" runat="server" CssClass="singleAccordionContent">
			<uc:AssignedOffices ID="AssignedOffices" runat="server" OnSaveAssignedOfficesClick="SaveAssignedOffices" AdministrationType="JobOrder" />
		</asp:Panel>
		</div>
		<act:CollapsiblePanelExtender ID="AssignedOfficeCollapsiblePanelExtender" runat="server" TargetControlID="AssignedOfficePanel" ExpandControlID="AssignedOfficeHeaderPanel" 
																	CollapseControlID="AssignedOfficeHeaderPanel" Collapsed="true" ImageControlID="AssignedOfficeHeaderImage"
																	SuppressPostBack="true" BehaviorID="AssignedOfficeCollapsiblePanel"/>
	</asp:PlaceHolder>
	</div>
  <uc:EmailEmployeeModal ID="EmailEmployee" runat="server" />
  <uc:ConfirmationModal ID="Confirmation" runat="server" Height="175px" Width="300px" OnOkCommand="Confirmation_OkCommand"/>
   <uc:JobActionModal ID="JobAction" OnCompleted="JobAction_Completed" runat="server"/>

  <script type="text/javascript" >

  	var Business_TalentPopup = null;

		Sys.Application.add_load(Job_PageLoad);

		function Job_PageLoad() {
			var button = $("#<%= ExpandCollapseAllButton.ClientID %>");
			if (button.length > 0)
				bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>', null); // bind the magic
		}

	  function showPopup(url, blockedPopupMessage) {
		  var popup = window.open(url, "Business_blank");
		  if (!popup)
			  alert(blockedPopupMessage);

		  return popup;
	  }

	  function HideSubAction() {

		  $("#SubActionButton").hide();
		  $("#SubActionDropDown").hide();
		  return false;
	  }

	  //JAWS Compatibility
	  function removeTitle(link) {
		  $(link).removeAttr("title");
	  }

	  function restoreTitle(link, title) {
		  $(link).attr("title", title);
	  }


	  function jumpToNotesReminders() {
		  $find("NotesRemindersCollapsiblePanel")._doOpen();
		  setTimeout(function () {
			  $(window).scrollTop($("#<%= NotesRemindersHeaderPanel.ClientID %>").offset().top);
		  }, 500);
		  return false;
	  }

	  function jobOrdersActionButtonClicked() {
		  if ($("#<%= JobOrdersActionDropDown.ClientID %>").val().toLowerCase() == 'addnote') {
			  return jumpToNotesReminders();
		  }
		  else {
			  return true;
		  }
	  }

	  function JobOrdersActionButton_Validate(sender, args) {

		  var action = $("#<%=JobOrdersActionDropDown.ClientID %>").val();

		  if (action == "AccessEmployeeAccount" && (Business_TalentPopup != null && !Business_TalentPopup.closed)) {
			  $(sender).text("<%=CloseBusinessWindowErrorMessage %>");
			  args.IsValid = false;
		  }
	  }

	  function ViewJobAsEmployerLinkButton_Validate() {

	  	if (Business_TalentPopup != null && !Business_TalentPopup.closed) {
	  		$("#ViewJobAsEmployerValidationErrorLabel").text("<%=CloseBusinessWindowErrorMessage %>").show();
	  		return false;
	  	} else {
	  		$("#ViewJobAsEmployerValidationErrorLabel").hide();
	  		return true;
	  	}
	  }

	  function AccessEmployersAccountLinkButton_Validate() {

	  	if (Business_TalentPopup != null && !Business_TalentPopup.closed) {
	  		$("#AccessEmployersAccountValidationErrorLabel").text("<%=CloseBusinessWindowErrorMessage %>").show();
	  		return false;
	  	} else {
	  		$("#AccessEmployersAccountValidationErrorLabel").hide();
	  		return true;
	  	}
	  }
  </script>
</asp:Content>

