﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
  CodeBehind="JobSeekers.aspx.cs" Inherits="Focus.Web.WebAssist.JobSeekers" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="Focus.Core.Models" %>
<%@ Register Src="~/WebAssist/Controls/TabNavigation.ascx" TagName="TabNavigation" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/AssistJobSeekersNavigation.ascx" TagName="AssistJobSeekersNavigation" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/EmailJobSeekerModal.ascx" TagName="EmailCandidateModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/JobSeekerListModal.ascx" TagName="JobSeekerListModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/ConfirmationModal.ascx" TagPrefix="uc" TagName="ConfirmationModal" %>
<%@ Register Src="~/WebAssist/Controls/RegisterJobSeeker.ascx" TagName="RegisterJobSeekerModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/NotesAndRemindersModal.ascx" TagName="NotesAndRemindersModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/IssueResolutionModal.ascx" TagName="IssueResolutionModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/RequestFollowUpModal.ascx" TagName="RequestFollowUp" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ActivityAssignment.ascx" TagName="ActivityAssignment" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/ActivityReportCriteriaModal.ascx" TagName="StudentActivityReportCriteriaModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/OfficeFilter.ascx" TagName="OfficeFilter" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/MessagesDisplay.ascx" TagName="MessagesDisplay" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/WelcomeToAssistModal.ascx" TagName="WelcomeToAssistModal" TagPrefix="uc" %>
<%@ Register Src="~/WebAssist/Controls/EditSsnModal.ascx" TagName="EditSsnModal" TagPrefix="uc" %>
<%@ Register Src="~/Code/Controls/User/UpdateableClientList.ascx" TagName="UpdateableClientList" TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
  <script src="<%# ResolveUrl("~/Assets/Scripts/UpdateableList.js") %>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
  <uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
  <table style="width: 100%;" role="presentation">
    <tr>
      <td style="vertical-align: top;">
        <uc:AssistJobSeekersNavigation ID="AssistJobSeekersNavigationMain" runat="server" />
      </td>
      <td style="vertical-align: top; text-align: right;">
        <asp:Button ID="RunActivityReportButton" runat="server" class="button1" OnKeyPress="RunActivityReportButton_Clicked" OnClick="RunActivityReportButton_Clicked"
          CausesValidation="false" />
        <asp:Literal ID="LocaliseCreateNewJobSeekerButton" runat="server" /><asp:Button ID="CreateNewJobSeekerButton"
          runat="server" Text="" class="button1" OnClick="CreateNewJobSeekerButton_Clicked" OnKeyPress="CreateNewJobSeekerButton_Clicked"/>
      </td>
    </tr>
  </table>
  <uc:MessagesDisplay ID="Messages" runat="server" Module="Assist" />
  <h1>
    <%= HtmlLocalise("JobSeekerDashboard.Title", "#CANDIDATETYPE# dashboard")%></h1>
  <br />
  <table style="border-collapse: collapse; width: 100%" data-panel="jobseekersearch" role="presentation">
    <tr style="vertical-align: top">
      <td style="white-space: nowrap;">
        <asp:Panel ID="WorkforceSearchPanel" runat="server" Visible="False">
          <div style="height: 28px;">
            <div style="width: 118px; display: inline-block">
                <%= HtmlLocalise("JobSeekerDashboard.Show", "Show")%>
            </div>
            <div style="width: 322px; display: inline-block;">
	            <focus:LocalisedLabel runat="server" ID="JobSeekerFilterTypeDropDownLabel" AssociatedControlID="JobSeekerFilterTypeDropDown" LocalisationKey="JobSeekerFilter" DefaultText="Job seeker filter" CssClass="sr-only"/>
              <asp:DropDownList runat="server" ID="JobSeekerFilterTypeDropDown" Width="200" ClientIDMode="AutoID"/>
            </div>
            <div style="width: 90px; display: inline-block;">
                <%= HtmlLabel(JobSeekerIssuesDropDown,"JobSeekerDashboard.Issues", "Issues")%>
            </div>
            <div style="width: 370px; display: inline-block;">
              <asp:DropDownList runat="server" ID="JobSeekerIssuesDropDown" Visible="True" Width="350"/>
            </div>
            <div id="JobSeekerStatusLabel" runat="server" style="width: 75px; display: inline-block;">
              <%= HtmlLabel(StatusDropDown,"JobSeekerDashboard.Status","Status")%></div>
            <div id="JobSeekerStatusCell" runat="server" style="width: 170px; display: inline-block;">
              <asp:DropDownList runat="server" ID="StatusDropDown" Visible="True" Width="150" />
            </div>
          </div>
        </asp:Panel>
      </td>
			<td>
				<asp:Button runat="server" Text="Go" class="button3" OnClick="FindButton_Clicked" OnKeyPress="FindButton_Clicked" ID="FindButton" ValidationGroup="SearchGroup" />				
			</td>
    </tr>
    <tr runat="server" id="OfficeFilterRow">
      <td>
        <uc:OfficeFilter ID="OfficeFilter" runat="server" />
      </td>
			<td style="vertical-align: top; white-space: nowrap;">
				<asp:Button ID="ClearButton" runat="server" Text="Clear" class="button3" OnClick="ClearButton_Clicked" OnKeyPress="ClearButton_Clicked" CausesValidation="False" />				
			</td>
    </tr>
  </table>
  <br />
  <div>
    <div id="SearchPanelTop" data-panel="jobseekersearch">
      <table style="width: 100%;" role="presentation">
        <tr>
          <td style="padding-top: 6px; padding-bottom: 4px;">
            <asp:Panel ID="JobSeekerSearchHeaderPanel" runat="server" CssClass="singleAccordionTitle">
              <table class="accordionTable" role="presentation">
                <tr>
                  <td>
                    <asp:Image ID="JobSeekerSearchHeaderImage" runat="server" alt="." Width="22" Height="22" />
                    &nbsp;&nbsp;<span class="collapsiblePanelHeaderLabel cpHeaderControl"><a href="#"><%= HtmlLocalise("FindAJobSeeker.Label", "Find a #CANDIDATETYPE#:LOWER")%></a></span>
                  </td>
                </tr>
              </table>
            </asp:Panel>
          </td>
        </tr>
        <tr>
          <td>
            <div class="singleAccordionContentWrapper">
              <asp:Panel ID="JobSeekerSearchPanel" runat="server" CssClass="singleAccordionContent collapsiblePanelFix" Style="display: none" >
                <table style="width: 100%;" role="presentation">
                  <tr style="vertical-align: top">
                    <td style="white-space: nowrap;">
                      <%= HtmlLabel(FirstNameTextBox,"JobSeekerFirstName.Label", "First name") %>
                    </td>
                    <td style="padding-bottom: 15px">
                      <asp:TextBox runat="server" ID="FirstNameTextBox" Width="200" ClientIDMode="AutoID"/>
                    </td>
                    <asp:PlaceHolder runat="server" ID="DateOfBirthSearch">
                      <td style="white-space: nowrap;">
                        <%= HtmlLabel(DateOfBirthTextBox,"DateOfBirth.Label", "Date of birth")%>
                        <span class="instructionalText">mm/dd/yyyy</span>
                      </td>
                      <td style="white-space: nowrap;">
                        <table role="presentation">
                          <tr>
                            <td>
                              <asp:TextBox ID="DateOfBirthTextBox" runat="server" />
                              <act:MaskedEditExtender ID="meDOB" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="DateOfBirthTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_" />
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <asp:CustomValidator ID="DOBValidator" runat="server" ControlToValidate="DateOfBirthTextBox" SetFocusOnError="true" Display="Dynamic" 
																										CssClass="error" ClientValidationFunction="ValidateDateOfBirth" ValidateEmptyText="true" ValidationGroup="SearchGroup" />
                            </td>
                          </tr>
                        </table>
                      </td>
                      <td style="width: 10px;">
                      </td>
                    </asp:PlaceHolder>
                    <td style="white-space: nowrap;">
                      <%= HtmlLabel(EmailAddressTextBox,"JobSeekerEmailAddress.Label", "Resume email")%>
                    </td>
                    <td>
                      <asp:TextBox runat="server" ID="EmailAddressTextBox" Width="200" />
                    </td>
                    <td style="width: 10px;">
                    </td>
                  </tr>
                  <tr style="vertical-align: top">
                    <td style="white-space: nowrap;">
                      <%= HtmlLabel(LastNameTextBox,"JobSeekerLastName.Label", "Last name") %>
                    </td>
                    <td style="padding-bottom: 15px">
                      <asp:TextBox runat="server" ID="LastNameTextBox" Width="200" />
                    </td>
                    <td style="white-space: nowrap;">
                      <asp:Label runat="server" AssociatedControlID="UsernameTextBox" ID="UsernameLabel"></asp:Label>
                    </td>
                    <td style="padding-bottom: 15px">
                      <asp:TextBox runat="server" ID="UsernameTextBox" Width="200" />
                    </td>
                    <td style="width: 10px;">
                    </td>
                  </tr>
                  <tr style="vertical-align: top">
                    <asp:PlaceHolder ID="SSNPlaceHolder" runat="server">
                      <td style="width: 10px;" id="LastNameEmptyCell" runat="server">
                        <asp:Label runat="server" ID="SSNLabel"></asp:Label>
                      </td>
                      <td style="white-space: nowrap;" id="SSNLabelCell" runat="server">
                        <uc:UpdateableClientList ID="UCLSSN" runat="server" AllowNonAdded="True" InputMask="9?99-99-9999" MaskPromptCharacter="_" ValidatorRegex="^\d{3}-\d{2}-\d{4}$" 
																									ValidatorRegexErrorMessage="SSN must consist of 9 numbers" ValidationGroup="SearchGroup" SelectionBoxTitle="Selected SSNs" />
                      </td>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="ExternalIdPanel">
                      <td style="width: 10px; white-space: nowrap;">
                        <asp:Label runat="server" ID="ExternalIdLabel"></asp:Label>
                      </td>
                      <td style="white-space: nowrap;" id="ExternalIdLabelCell" runat="server">
                        <uc:UpdateableClientList ID="UCLExternalId" AllowNonAdded="True" runat="server" SelectionBoxTitle="Selected External IDs" />
                      </td>
                      <td id="SsnVisibilityEnabled" runat="server" style="width: 10px;">
                      </td>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="PersonIdPanel">
                      <td style="white-space: nowrap; width: 10px;">
                        <asp:Label runat="server" ID="PersonIdLabel"></asp:Label>
                      </td>
                      <td style="white-space: nowrap;" id="PersonIdLabelCell" runat="server">
                        <uc:UpdateableClientList ID="UCLPersonId" AllowNonAdded="True" runat="server" ValidatorCompareValue="9223372036854775807" ValidatorCompareErrorMessage="Focus ID must be less than 9,223,372,036,854,775,807"
                          ValidatorCompareType="Double" ValidatorCompareOperator="LessThan" ValidationGroup="SearchGroup" InputMask="9?999999999999999999" MaskPromptCharacter="" />
                      </td>
                      <td style="width: 10px;">
                      </td>
                    </asp:PlaceHolder>
                  </tr>
                </table>
                <act:CalendarExtender ID="DateOfBirthCalendarExtender" runat="server" TargetControlID="DateOfBirthTextBox" CssClass="cal_Theme1" />
                <div class="clear">
                  &nbsp;</div>
              </asp:Panel>
            </div>
          </td>
        </tr>
      </table>
      <act:CollapsiblePanelExtender ID="JobSeekerSearchPanelExtender" runat="server" TargetControlID="JobSeekerSearchPanel" ExpandControlID="JobSeekerSearchHeaderPanel" CollapseControlID="JobSeekerSearchHeaderPanel" Collapsed="True" 
					ImageControlID="JobSeekerSearchHeaderImage" CollapsedImage="<%# UrlBuilder.ActivityOffLeftImage() %>" ExpandedImage="<%# UrlBuilder.ExpandedAccordionImage() %>" SuppressPostBack="true" BehaviorID="JobSeekerSearchPanelBehaviour" />
    </div>
    <br />
    <asp:Panel ID="WorkforceJobSeekerSearchPanel" runat="server" Visible="False">
      <asp:UpdatePanel ID="FindJobSeekerUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
          <table style="width: 100%;" role="presentation">
            <tr>
              <td style="white-space: nowrap; width: 120px !important;">
                <asp:Literal ID="ActionLabel" runat="server" Visible="False" ClientIDMode="Static" />
              </td>
              <td style="width: 350px;">
							<focus:LocalisedLabel runat="server" ID="ActionDropDownLabel" AssociatedControlID="ActionDropDown" LocalisationKey="Action" DefaultText="Action" CssClass="sr-only"/>
                <asp:DropDownList runat="server" ID="ActionDropDown" Width="350" Visible="false" ClientIDMode="Static" />
              </td>
              <td style="width: 20px; padding-left: 3px; padding-right: 14px;">
                <asp:Button ID="ActionGoButton" runat="server" class="button3" Visible="false" ClientIDMode="Static" OnKeyPress="ActionButton_Clicked" OnClick="ActionButton_Clicked" OnClientClick="return JobSeekers_ActionButton_Validate(this);" ValidationGroup="Action" />
              </td>
              <td style="width: 100%; white-space: nowrap; text-align: right;">
                <uc:Pager ID="SearchResultListPager" runat="server" PagedControlId="JobSeekersListView" PageSize="10" DisplayRecordCount="True" />
              </td>
            </tr>
            <tr>
              <td>
              </td>
              <td colspan="3">
                <uc:ActivityAssignment ID="ActivityAssigner" runat="server" ActivityType="JobSeeker" OnActivitySelected="ActivityAssignment_ActivitySelected" Visible="False" />
                <asp:DropDownList runat="server" ID="SubActionDropDown" Width="225" Visible="false" ClientIDMode="Static" />
                <asp:Button ID="SubActionButton" runat="server" class="button3" Visible="false" ClientIDMode="Static" ValidationGroup="SubAction" OnKeyPress="SubActionButton_Clicked" OnClick="SubActionButton_Clicked" />
              </td>
            </tr>
            <tr>
              <td>
              </td>
              <td colspan="3">
                <asp:CustomValidator ID="CandidateActionDropDownValidator" runat="server" CssClass="error" ControlToValidate="ActionDropDown" ClientValidationFunction="JobSeekers_ActionDropDown_Validate" ValidationGroup="Action" SetFocusOnError="True" ValidateEmptyText="true"/>
                <asp:RequiredFieldValidator ID="SubActionDropDownRequired" runat="server" ValidationGroup="SubAction" ControlToValidate="SubActionDropDown" CssClass="error" />
              </td>
            </tr>
          </table>
          <br />
          <asp:ListView runat="server" ID="JobSeekersListView" ItemPlaceholderID="SearchResultPlaceHolder" DataSourceID="SearchResultDataSource" OnLayoutCreated="JobSeekersListView_LayoutCreated"
						            OnSorting="JobSeekerListView_Sorting" OnItemDataBound="JobSeekerListView_ItemDataBound" DataKeyNames="Id, FirstName, LastName, MiddleInitial, Blocked, Enabled">
            <LayoutTemplate>
              <table class="table table-fixed" id="JobSeekerResultTable">
                <thead>
                  <tr>
                    <th style="width: 10px;">
                      <table role="presentation">
                        <tr>
                          <td rowspan="2">
                            <asp:CheckBox ID="SelectorAllCheckBoxes" ClientIDMode="AutoID" runat="server" CausesValidation="False" OnKeyPress="SelectDeselectAllCheckboxes()" OnClick="SelectDeselectAllCheckboxes()" AutoPostBack="False" />
                          </td>
                          <td>
                          </td>
                        </tr>
                      </table>
                    </th>
                    <th style="white-space: nowrap">
                      <asp:Literal runat="server" ID="NameHeaderLiteral"></asp:Literal>
                      <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="NameSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" 
																					CommandArgument="jobseekername asc" AlternateText="Sort Ascending" Width="15" Height="9"/>
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="NameSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" 
																					CommandArgument="jobseekername desc" AlternateText="Sort Descending" Width="15" Height="9" />
                      </div>
                    </th>
                    <th style="white-space: nowrap">
                      <asp:Literal runat="server" ID="ExternalIdLiteral"></asp:Literal>
                    </th>
                    <th style="white-space: nowrap">
                      <asp:Literal runat="server" ID="LocationHeaderLiteral"></asp:Literal>
                      <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LocationNameSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" 
																					CommandArgument="location asc" AlternateText="Sort Ascending" Width="15" Height="9"/>
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="LocationNameSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" 
																					CommandArgument="location desc" AlternateText="Sort Descending" Width="15" Height="9"/>
                      </div>
                    </th>
                    <th style="white-space: nowrap">
                      <asp:Literal runat="server" ID="LastActivityDateHeaderLiteral"></asp:Literal>
                      <div class="orderingcontrols">
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortAscImage() %>" ID="LastActivityDateSortAscButton" CssClass="AscButton" runat="server" CommandName="Sort" 
																					CommandArgument="lastactivity asc" AlternateText="Sort Ascending" Width="15" Height="9"/>
                        <asp:ImageButton ImageUrl="<%# UrlBuilder.ActivityOnSortDescImage() %>" ID="LastActivityDateSortDescButton" CssClass="DescButton" runat="server" CommandName="Sort" 
																					CommandArgument="lastactivity desc" AlternateText="Sort Descending" Width="15" Height="9"/>
                      </div>
                    </th>
                    <th style="white-space: nowrap">
                      <asp:Literal runat="server" ID="IssuesHeaderLiteral"></asp:Literal>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
                </tbody>
              </table>
            </LayoutTemplate>
            <ItemTemplate>
              <tr>
                <td>
                  <asp:Label ID="SelectorCheckBoxLabel" runat="server" Text="Jobseeker check box" CssClass="groupLabel" AssociatedControlID="SelectorCheckBox" />
									<asp:CheckBox ID="SelectorCheckBox" ValidationGroup="SelectorCheckBoxes" runat="server" ClientIDMode="AutoID" CausesValidation="False" AutoPostBack="False" />
                </td>
                <td class="force-wrap">
                  <asp:HyperLink runat="server" ID="JobSeekerNameLink"></asp:HyperLink>
                  <asp:Image ID="VeteranImage" runat="server" Visible="false" CssClass="tooltipImage toolTipNew" Height="15" Width="17" AlternateText="Veteran"/>
                  <asp:Image ID="MSFWImage" runat="server" Visible="false" CssClass="tooltipImage toolTipNew" AlternateText="Migrant or Seasonal Farm Worker" />
                  <asp:Image ID="InactiveImage" runat="server" Visible="false" CssClass="tooltipImage toolTipNew" AlternateText="Inactive" />
                  <asp:Image ID="BlockedImage" runat="server" Visible="false" CssClass="tooltipImage toolTipNew" AlternateText="Blocked Image" />
                  <asp:Image runat="server" ID="UnderageImage" Visible="false" ImageUrl="<%# UrlBuilder.UnderAgeJobSeekerIconSmall() %>" CssClass="tooltipImage toolTipNew" AlternateText="Under age" />
                </td>
                <td>
                  <%# ((JobSeekerDashboardModel)Container.DataItem).ExternalId%>
                </td>
                <td>
                  <asp:Literal runat="server" ID="CandidateLocationLiteral"></asp:Literal>
                </td>
                <td>
                  <%#((JobSeekerDashboardModel)Container.DataItem).LastLoggedInOn%>
                </td>
                <td>
                  <asp:Label runat="server" ID="IssuesLabel"></asp:Label>
                  <span id="AdditionalIssuesLink_<%#((JobSeekerDashboardModel)Container.DataItem).Id%>">
                    <asp:HyperLink runat="server" ID="ShowAdditionalIssuesLink" Visible="False" NavigateUrl="javascript:void(0);" ClientIDMode="Predictable"></asp:HyperLink>
									</span> 
									<span style="display: none" id="AdditionalIssues_<%#((JobSeekerDashboardModel)Container.DataItem).Id%>">
                    <asp:Literal runat="server" ID="AdditionalIssuesLiteral"></asp:Literal>
                    <a href="javascript:void(0);" OnKeyPress="HideAdditionalIssues(<%#((JobSeekerDashboardModel)Container.DataItem).Id%>)" OnClick="HideAdditionalIssues(<%#((JobSeekerDashboardModel)Container.DataItem).Id%>);"><%=HtmlLocalise("Hide.text", "Hide") %></a>
									</span>
                </td>
              </tr>
            </ItemTemplate>
            <EmptyDataTemplate>
              <br />
              <%=HtmlLocalise("NoJobSeekers", "No #CANDIDATETYPES#:LOWER found") %></EmptyDataTemplate>
          </asp:ListView>
          <%-- Modals --%>
          <asp:PlaceHolder runat="server" ID="WorkforceModalPanels">
            <uc:EmailCandidateModal ID="EmailCandidate" runat="server" />
            <uc:JobSeekerListModal ID="JobSeekerList" runat="server" />
            <uc:ConfirmationModal ID="Confirmation" runat="server" OnOkCommand="Confirmation_OnOkCommand"
              OnCloseCommand="Confirmation_OnCloseCommand" />
            <uc:NotesAndRemindersModal ID="NotesAndReminders" runat="server" />
            <uc:IssueResolutionModal ID="IssueResolutionModal" runat="server" OnIssuesResolved="Issues_Updated" />
            <uc:RequestFollowUp ID="RequestFollowUpModal" runat="server" OnFollowUpRequested="Issues_Updated" />
            <uc:EditSsnModal ID="EditSsnModal" runat="server" />
          </asp:PlaceHolder>
        </ContentTemplate>
      </asp:UpdatePanel>
    </asp:Panel>
  </div>
  <asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.Web.WebAssist.JobSeekers"
    EnablePaging="true" SelectMethod="GetJobSeekers" SelectCountMethod="GetJobSeekersCount"
    SortParameterName="orderBy" OnSelecting="SearchResultDataSource_Selecting"></asp:ObjectDataSource>
  <uc:StudentActivityReportCriteriaModal ID="StudentActivityReportCriteriaModal" runat="server" ReportType="StudentActivity" />
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <uc:RegisterJobSeekerModal ID="RegisterJobSeeker" runat="server" OnJobSeekerRegistered="RegisterJobSeeker_OnJobSeekerRegistered" />
    </ContentTemplate>
  </asp:UpdatePanel>
  <%-- Hidden Field --%>
  <uc:WelcomeToAssistModal ID="WelcomeToAssist" runat="server" />
  <script type="text/javascript">

	  var JobSeekers_CareerPopup = null;
	  var JobSeekers_ActionButtonClicked = false;
	  var ActionInactiveOption = null;

	  function JobSeekers_ActionButton_Validate(sender) {

		  //reset activity default category value
		  if ($("#MainContent_ActivityAssigner_ActivityCategoryDropDownList").exists()) {
			  $("#MainContent_ActivityAssigner_ActivityCategoryDropDownList")[0].selectedIndex = 0;
		  } 

		  JobSeekers_ActionButtonClicked = true;
		  var valid = Page_ClientValidate("Action");
		  JobSeekers_ActionButtonClicked = false;
		  return valid;
	  }

	  function JobSeekers_ActionDropDown_Validate(sender, args) {

		  // Validate only if the Action Button has been clicked
		  if (!JobSeekers_ActionButtonClicked)
			  return;

		  var jobSeekerCount = JobSeekers_CountSelected();
		  var action = $("#<%=ActionDropDown.ClientID %>").val();

		  if (action === "") {
			  // Ensure an Action has been selected
			  $(sender).text("<%= ActionDropDownRequiredErrorMessage %>");
			  args.IsValid = false;
			  return;
		  } else {
			  // Ensure a job seeker has been selected
			  if (jobSeekerCount === 0) {
				  $(sender).text("<%= SingleCheckboxErrorMessage %>");
				  args.IsValid = false;
				  return;
			  }
			  // Ensure that only a single selection has been made for actions valid only for a single job seeker
			  if (jobSeekerCount > 1 && $.inArray(action, JobSeekers_ValidMultiActions) < 0) {
				  $(sender).text("<%= MultipleCheckboxErrorMessage %>");
				  args.IsValid = false;
				  return;
			  }
		  }

		  if ((action == "AccessCandidateAccount" || action === "InactivateReactivateJobSeeker") && (JobSeekers_CareerPopup != null && !JobSeekers_CareerPopup.closed)) {
			  $(sender).text("<%=CloseJobSeekerWindowErrorMessage %>");
			  args.IsValid = false;
		  }
	  }

	  function JobSeekers_CountSelected() {
		  var checkBoxes = $('#JobSeekerResultTable').find('input:checkbox');
		  var checked = 0;

		  for (var i = 0; i <= checkBoxes.length - 1; i++)
			  if (checkBoxes[i].checked)
				  checked++;

		  return checked;
	  }

	  function JobSeekers_ClearDropdown(dropdown, callChange) {
		  if (dropdown.length > 0) {
			  dropdown.prop("selectedIndex", 0);
			  if (callChange)
				  dropdown.change();
		  }
	  }

	  function SelectDeselectAllCheckboxes() {

		  var checkboxes = $('input:checkbox');
		  var checked = $("[id$='SelectorAllCheckBoxes']").is(':checked');
		  for (var i = 0; i <= checkboxes.length - 1; i++)
			  checkboxes[i].checked = checked;

		  return false;
	  }

	  function ShowAdditionalIssues(id) {
		  $('#AdditionalIssues_' + id).show();
		  $('#AdditionalIssuesLink_' + id).hide();
	  }
	  function HideAdditionalIssues(id) {
		  $('#AdditionalIssues_' + id).hide();
		  $('#AdditionalIssuesLink_' + id).show();
	  }

	  function showPopup(url, blockedPopupMessage) {
		  var popup = window.open(url, "JobSeekers_blank");
		  if (!popup)
			  alert(blockedPopupMessage);

		  return popup;
	  }

	  $(document).ready(function () {
		  $('td').delegate('.deleteItem', 'click', function (e) {
			  RemoveListItem($(this));
			  e.stopPropagation();
		  });

		  $("*[data-panel='jobseekersearch'] input:text").keypress(function (event) {
			  if (event.keyCode == 13) {
				  $("#<%=FindButton.ClientID %>").click();
			  }

			  return event.keyCode != 13;
		  });
	  });

	  $(document).ready(function () {
		  $("[ids$='SelectorAllCheckBoxes']").attr("title", "Select all checkboxes");

	   
		  if ($("#MainContent_StatusDropDown").val() === 'Inactive') {
			  $('select#ActionDropDown').find('option').each(function() {
				  if ($(this).val() !== null && $(this).val().toLowerCase() === 'inactivatejobseeker') {
					  ActionInactiveOption = $(this);
					  $(this).remove();
				  }
			  });
		  }
	  });

	  function ValidateDateOfBirth(sender, args) {
		  if (args.Value != "__/__/____") {
			  var dateRegx = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

			  if (dateRegx.test(args.Value) == false) {
				  sender.innerHTML = "<%= DateErrorMessage %>";
				  args.IsValid = false;
			  } else {

				  var birthStrings = args.Value.split("/");
				  var birthDate = new Date(parseInt(birthStrings[2], 10), parseInt(birthStrings[0], 10) - 1, parseInt(birthStrings[1], 10));

				  var age = GetAgeForDate(birthDate);
				  if (age < 14) {
					  sender.innerHTML = "<%= DOBMinLimitErrorMessage %>";
					  args.IsValid = false;
                    } else if (age > 99) {
					  sender.innerHTML = "<%= DOBMaxLimitErrorMessage %>";
					  args.IsValid = false;
				  }
			  }
		  }
	  }

	  $("#MainContent_StatusDropDown").change(function () {
		  if (this.value !== null && this.value.toLowerCase() === 'inactive') {
			  $('select#ActionDropDown').find('option').each(function () {
				  if ($(this).val() !== null && $(this).val().toLowerCase() === 'inactivatejobseeker') {
					  ActionInactiveOption = $(this);
					  $(this).remove();
				  }
			  });
		  }
		  else {
			  if (ActionInactiveOption !== null) {
				  $("#ActionDropDown option:last").before(ActionInactiveOption);
			  }
		  }

	  });




  </script>
</asp:Content>
