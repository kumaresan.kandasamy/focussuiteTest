﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Views;
using Focus.Services.Core.Extensions;
using Focus.Web.Code;
using Focus.Web.Code.Controls.User;
using Focus.Web.Code.Controls.User.SearchCriteria;
using Focus.Web.WebAssist.Controls;

using Framework.Core;

#endregion

namespace Focus.Web.WebAssist
{
	[PrincipalPermission(SecurityAction.Demand, Role = Constants.RoleKeys.AssistUser)]
	public partial class Pool : PageBase
	{
		#region Page properties

		/// <summary>
		/// Gets or sets the model.
		/// </summary>
		/// <value>The model.</value>
		protected AssistTalentPoolModel Model
		{
			get { return GetViewStateValue("AssistTalentPool:Model", new AssistTalentPoolModel()); }
			set { SetViewStateValue("AssistTalentPool:Model", value); }
		}

		/// <summary>
		/// Gets or sets the candidates in list.
		/// </summary>
		/// <value>The candidates in list.</value>
		private List<ResumeView> CandidatesInList
		{
			get { return GetViewStateValue<List<ResumeView>>("AssistTalentPool:CandidatesInList"); }
			set { SetViewStateValue("AssistTalentPool:CandidatesInList", value); }
		}

		/// <summary>
		/// Gets or sets the _search criteria.
		/// </summary>
		/// <value>The _search criteria.</value>
		public CandidateSearchCriteria SearchCriteria
		{
			get { return App.GetSessionValue<CandidateSearchCriteria>(Constants.StateKeys.CandidateSearchCriteria + Model.SearchCriteriaId); }
			set { App.SetSessionValue(Constants.StateKeys.CandidateSearchCriteria + Model.SearchCriteriaId, value); }
		}

		/// <summary>
		/// Gets or sets the resume filename.
		/// </summary>
		/// <value>The resume filename.</value>
		private string ResumeFilename
		{
			get { return GetViewStateValue<string>("PoolSearch:ResumeFilename"); }
			set { SetViewStateValue("PoolSearch:ResumeFilename", value); }
		}

		/// <summary>
		/// Gets or sets the content of the resume.
		/// </summary>
		/// <value>The content of the resume.</value>
		private byte[] ResumeContent
		{
			get { return GetViewStateValue<byte[]>("PoolSearch:ResumeContent"); }
			set { SetViewStateValue("PoolSearch:ResumeContent", value); }
		}

		/// <summary>
		/// Gets or sets the saved search id.
		/// </summary>
		/// <value>The saved search id.</value>
		private long SavedSearchId
		{
			get { return GetViewStateValue<long>("PoolSearch:SavedSearchId"); }
			set { SetViewStateValue("PoolSearch:SavedSearchId", value); }
		}

		private static Guid _searchId;
		private int CandidateCount { get; set; }
    private bool _jobPostingFilterApplied;

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (Request.QueryString["filtered"] != null) _jobPostingFilterApplied = Convert.ToBoolean(Request.QueryString["filtered"]);

			Page.Title = CodeLocalise("Page.Title", "Job Pool");

			if (!IsPostBack)
			{
				Model = new AssistTalentPoolModel { SearchCriteria = { GetNotesAndRemindersCount = true, MinimumScore = App.Settings.StarRatingMinimumScores[App.Settings.RecommendedMatchesMinimumStars] } };
				long jobId = 0;

				// Get job id
				if (Page.RouteData.Values.ContainsKey("jobid"))
				{
					long.TryParse(Page.RouteData.Values["jobid"].ToString(), out jobId);
					Model.JobId = jobId;
					SearchCriteria = Model.SearchCriteria;
				}

				// Get current list type
				if (Page.RouteData.Values.ContainsKey("tab"))
				{
					TalentPoolListTypes listTypes;

					if (!Enum.TryParse(Page.RouteData.Values["tab"].ToString(), true, out listTypes) || Model.JobId == 0)
						listTypes = TalentPoolListTypes.AllResumes;

					Model.CurrentTab = listTypes;
				}
				else
				{
					Model.CurrentTab = TalentPoolListTypes.AllResumes;
				}

				// Bind data and populate list
				//LocaliseAndBind();

				ApplyBranding();

				// Check to see if we have search criteria stored in session
				if (Page.RouteData.Values.ContainsKey("searchid"))
				{
					Guid urlSearchId;
					if (Guid.TryParse(Page.RouteData.Values["searchid"].ToString(), out urlSearchId))
					{
						Model.SearchCriteriaId = urlSearchId;
						var criteria = SearchCriteria;
						if (criteria.IsNotNull())
						{
							Model.SearchCriteria = criteria;
							// Reset job id
							Model.JobId = jobId;
							LoadSearchCriteria();
							SearchSummarySectionPanel.Visible = true;
						}
						else
						{
							Model.SearchCriteriaId = Guid.Empty;
						}
					}
					else
					{
						long savedSearchId;
						if (long.TryParse(Page.RouteData.Values["searchid"].ToString(), out savedSearchId))
						{
							SavedSearchId = savedSearchId;
							var savedSearchView = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearch(SavedSearchId);
							Model.SearchCriteria = savedSearchView.Criteria;
							Model.SearchCriteriaId = Guid.NewGuid();
							LoadSearchCriteria();
							SearchSummarySectionPanel.Visible = true;

						}
						else
						{
							Model.SearchCriteriaId = Guid.Empty;
						}
					}
				}

                LocaliseAndBind();

				if (App.Settings.Theme == FocusThemes.Education)
					BindJobTypeDropDownList();
				else
					JobTypeDropDownList.Visible = false;

				// Set default Job Type
				if (Model.JobId > 0 && App.Settings.Theme == FocusThemes.Education && Model.CurrentTab == TalentPoolListTypes.MatchesForThisJob)
				{
					SetDefaultJobTypes();
					SearchSummarySectionPanel.Visible = true;
				}

				BindJobData();
				BindAllTabPlaceHolder();
				BindSavedSearchDropDown();

				BindCandidateList();



				if (SavedSearchId > 0)
					SavedSearchDropDown.SelectedValue = SavedSearchId.ToString();
                //Page.Title = "All resumes - " + SavedSearchId;

			}
			else
			{
				InstructionsPanel.Visible = false;
				SearchResultListView.Visible = SearchSummarySectionPanel.Visible = SearchResultListPager.Visible = true;
				SaveSearchErrorLabel.Text = "";
			}

			ViewNotesButton.Visible = (Model.JobId > 0);
		}

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			BindTabLinks();
			ResumeFileUploadRegularEx.ValidationExpression = App.Settings.ResumeUploadAllowedFileRegExPattern;
			RegisterJavascript();
		}

		#region binding and localisation

		/// <summary>
		/// Retrieves the job data.
		/// </summary>
		private void BindJobData()
		{
			if (Model.JobId == 0)
			{
				//ReturnToJobOrderLink.Visible = JobEmployerLabel.Visible = false;
                JobDetails.Visible = false;
				return;
			}

			var job = ServiceClientLocator.JobClient(App).GetJobView(Model.JobId);
			ReturnToJobOrderLink.NavigateUrl = UrlBuilder.AssistJobView(Model.JobId);
			ReturnToJobOrderLink.Text = CodeLocalise("TalentTalentPool.JobTitle", "{0} #{1}", job.JobTitle, Model.JobId.ToString());
			JobEmployerLabel.Text = job.BusinessUnitName;

			Model.JobTitle = job.JobTitle;
			Model.EmployerName = job.BusinessUnitName;
			Model.JobStatus = job.JobStatus;
		  Model.ExtendVeteranPriority = job.ExtendVeteranPriority;
		  Model.VeteranPriorityEndDate = job.VeteranPriorityEndDate;

		}

		/// <summary>
		/// Binds the candidate list.
		/// </summary>
		private void BindCandidateList()
		{
			SearchResultListView.DataBind();

			SearchCriteriaSummary.UpdateSearchCriteria(Model.SearchCriteria);
		}

		/// <summary>
		/// Binds all tab empty search place holder.
		/// </summary>
		private void BindAllTabPlaceHolder()
		{
			if (Model.SearchCriteriaId == Guid.Empty && Model.CurrentTab == TalentPoolListTypes.AllResumes)
			{
				InstructionsPanel.Visible = true;
				SearchResultListView.Visible = SearchSummarySectionPanel.Visible = SearchResultListPager.Visible = false;
			}
		}

		/// <summary>
		/// Binds the additional criteria.
		/// </summary>
		/// <param name="model">The model.</param>
		private void BindAdditionalCriteria(ResumeSearchAdditionalCriteria.Model model)
		{
			AdditionalCriteria.Bind(model);
		}

		/// <summary>
		/// Binds the job type drop down list.
		/// </summary>
		private void BindJobTypeDropDownList()
		{
			JobTypeDropDownList.Items.AddEnum(JobTypes.Job, "jobs");
			JobTypeDropDownList.Items.AddEnum(JobTypes.InternshipPaid | JobTypes.InternshipUnpaid, "internships");
		}

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void LocaliseAndBind()
		{
			ReturnToJobOrderDashboardLink.Text = CodeLocalise("JobView.ReturnToJobOrderDashboard",
				"return to #EMPLOYMENTTYPE#  dashboard");
			ReturnToJobOrderDashboardLink.NavigateUrl = _jobPostingFilterApplied ? UrlBuilder.JobOrderDashboard() + "?filtered=true" : UrlBuilder.JobOrderDashboard();

			// Highlight correct tab
			TabAllResumes.Attributes["class"] = (Model.CurrentTab == TalentPoolListTypes.AllResumes ? "active" : "");
			TabApplicants.Attributes["class"] = (Model.CurrentTab == TalentPoolListTypes.Applicants ? "active" : "");
			TabMatchesForThisJob.Attributes["class"] = (Model.CurrentTab == TalentPoolListTypes.MatchesForThisJob ? "active" : "");
			TabReferrals.Attributes["class"] = (Model.CurrentTab == TalentPoolListTypes.Referrals ? "active" : "");
            TabInvitees.Attributes["class"] = (Model.CurrentTab == TalentPoolListTypes.Invitees ? "active" : "");

			TabApplicants.Visible = TabMatchesForThisJob.Visible = TabReferrals.Visible = (Model.JobId > 0);
            if (Model.CurrentTab == TalentPoolListTypes.Applicants) this.Page.Title = (Model.JobId > 0) ? "Applicants -" + Model.JobId : "Applicants";
            if (Model.CurrentTab == TalentPoolListTypes.AllResumes) this.Page.Title = (Model.JobId > 0) ? "All Resumes -" + Model.JobId : "All Resumes";
            if (Model.CurrentTab == TalentPoolListTypes.MatchesForThisJob) this.Page.Title = (Model.JobId > 0) ? "Matches For This Job -" + Model.JobId : "Matches For This Job";
            if (SavedSearchId > 0) this.Page.Title = "All Resumes - " + SavedSearchId;
            if (Model.CurrentTab == TalentPoolListTypes.Referrals)
            {
                if (Model.JobId > 0)
                    this.Page.Title = _jobPostingFilterApplied ? "Referrals -" + Model.JobId + " - filtered" : "Referrals -" + Model.JobId;
                else
                    this.Page.Title = "Referrals";
            }
                
            if (Model.CurrentTab == TalentPoolListTypes.Invitees) this.Page.Title = (Model.JobId > 0) ? "Invitees -" + Model.JobId : "Invitees";

			SaveSearchButton.Text = CodeLocalise("SaveSearch.Label.NoEdit", "Save this search and notify me of new talent");
			SampleResumeButton.ToolTip = KeywordSearchButton.Text = SampleResumeButton.Text = SavedSearchButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go");
			KeywordRequired.ErrorMessage = CodeLocalise("KeywordRequired.ErrorMessage", "Enter some keywords");
			ClearSearchButton.Text = CodeLocalise("ClearSearch.Text.NoEdit", "Clear search");
			ResumeFileUploadRegularEx.ErrorMessage = CodeLocalise("SampleResumeSearch.RegExErrorMessage", "Invalid resume format");
			ClearResumeButton.ToolTip = ClearResumeButton.Text = CodeLocalise("ClearResume.Text", "Clear resume");
			SavedSearchRequired.ErrorMessage = CodeLocalise("SavedSearch.RequiredError", "Select a saved search");
			ApplyAdvancedSearchButton.ToolTip = ApplyAdvancedSearchButton.Text = CodeLocalise("ApplyAdvancedSearchButton.Text", "Apply advanced search");
			ViewNotesButton.Text = CodeLocalise("ViewNotesButton.Text", "View / add notes and reminders");
			SearchLabel.Text = (App.Settings.Theme == FocusThemes.Education) ? CodeLocalise("SearchLabel.Text.Education", "Search for #CANDIDATETYPES#:LOWER looking for") : CodeLocalise("SearchLabel.Text", "Search");

			BindKeywordContextDropDown();
			BindKeywordScopeDropDown();

			AdditionalCriteria.BindControls();

    	}

		private void ApplyBranding()
		{
			SearchSummaryHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			SearchSummaryPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
			SearchSummaryPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();

			AdvancedSearchHeaderImage.ImageUrl = UrlBuilder.OpenAccordionImage();
			AdvancedSearchPanelExtender.CollapsedImage = UrlBuilder.ActivityOffLeftImage();
			AdvancedSearchPanelExtender.ExpandedImage = UrlBuilder.ExpandedAccordionImage();
		}

		/// <summary>
		/// Binds the tab links.
		/// </summary>
		private void BindTabLinks()
		{
			var linkJobId = (Model.JobId > 0) ? (long?)Model.JobId : null;
			var searchId = "";

			if (Model.SearchCriteriaId != Guid.Empty)
				searchId = Model.SearchCriteriaId.ToString();

			AllResumesLink.NavigateUrl = GetTabUrl(TalentPoolListTypes.AllResumes, linkJobId, searchId);
			ApplicantsLink.NavigateUrl = GetTabUrl(TalentPoolListTypes.Applicants, linkJobId, searchId);
			MatchesForThisJobLink.NavigateUrl = GetTabUrl(TalentPoolListTypes.MatchesForThisJob, linkJobId, searchId);
			ReferralsLink.NavigateUrl = GetTabUrl(TalentPoolListTypes.Referrals, linkJobId, searchId);
            InviteesLink.NavigateUrl = GetTabUrl(TalentPoolListTypes.Invitees, linkJobId, searchId);
		}

		/// <summary>
		/// Gets the URL for navigating to another tab
		/// </summary>
		/// <param name="tab">The tab to which to navigate.</param>
		/// <param name="linkJobId">The job id.</param>
		/// <param name="searchId">The search id.</param>
		/// <returns></returns>
		private string GetTabUrl(TalentPoolListTypes tab, long? linkJobId, string searchId)
		{
			return Model.CurrentTab == tab
				? UrlBuilder.AssistPool(tab, linkJobId, searchId)
				: UrlBuilder.AssistPool(tab, linkJobId);
		}

		/// <summary>
		/// Binds the keyword context drop down.
		/// </summary>
		private void BindKeywordContextDropDown()
		{
			KeywordContextDropDown.Items.AddEnum(KeywordContexts.FullResume, "Full resume");
			KeywordContextDropDown.Items.AddEnum(KeywordContexts.JobTitle, "Job title");
			KeywordContextDropDown.Items.AddEnum(KeywordContexts.EmployerName, "#BUSINESS# name");
			KeywordContextDropDown.Items.AddEnum(KeywordContexts.Education, "Education");
		}

		/// <summary>
		/// Binds the keyword scope drop down.
		/// </summary>
		private void BindKeywordScopeDropDown()
		{
			KeywordScopeDropDown.Items.AddEnum(KeywordScopes.EntireWorkHistory, "Entire work history");
			KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastJob, "Last job");
			KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastTwoJobs, "Last two jobs");
			KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastThreeJobs, "Last three jobs");
			// Commented out below as it would require a new filter to be apllied in Lens
			//KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastYear, "Last year");
			//KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastThreeYears, "Last three years");
			//KeywordScopeDropDown.Items.AddEnum(KeywordScopes.LastFiveYears, "Last five years");
		}

		//private void GetAndBuildSearchCriteria()
		//{
		//  var programsOfStudy = ServiceClientLocator.JobClient(App).GetJobProgramsOfStudy(Model.JobId);
		//  var educationCriteria = new CandidateSearchEducationCriteria();

		//  if (programsOfStudy.Any())
		//  {
		//    educationCriteria.ProgramsOfStudy = new List<KeyValuePair<string, string>>();
		//    educationCriteria.MonthsUntilExpectedCompletion = 6;
		//    foreach (var programOfStudy in programsOfStudy)
		//    {
		//      educationCriteria.ProgramsOfStudy.Add(new KeyValuePair<string, string>(programOfStudy.Id.ToString(),
		//                                                                             programOfStudy.ProgramOfStudy));
		//    }

		//    Model.SearchCriteria.AdditionalCriteria.EducationCriteria = educationCriteria;

		//    var additionalCriteriaModel = new ResumeSearchAdditionalCriteria.Model
		//                                    {
		//                                      AdditionalCriteria =
		//                                        new CandidateSearchAdditionalCriteria
		//                                          {EducationCriteria = educationCriteria}
		//                                    };


		//    BindAdditionalCriteria(additionalCriteriaModel);
		//  }
		//}

		/// <summary>
		/// Loads the search criteria.
		/// </summary>
		private void LoadSearchCriteria()
		{
			ClearSearchCriteria();

			var criteria = Model.SearchCriteria;

			if (criteria.IsNull()) return;

			AdvancedSearchPanelExtender.Collapsed = false;

			#region Resume Search

			if (criteria.ResumeCriteria.IsNotNull())
			{
				if (criteria.ResumeCriteria.ResumeFilename.IsNotNullOrEmpty())
				{
					ResumeFilename = criteria.ResumeCriteria.ResumeFilename;
					UploadedResumeLabel.Text = string.Format("{0} {1}", CodeLocalise("SampleResumeSearch.FileUploaded.Label", "Basing search on"), ResumeFilename);
				}

				if (criteria.ResumeCriteria.Resume.IsNotNullOrEmpty())
					ResumeContent = criteria.ResumeCriteria.Resume;
			}

			#endregion region

			#region Keyword Search

			if (criteria.KeywordCriteria.IsNotNull())
			{
				if (criteria.KeywordCriteria.Keywords.IsNotNullOrEmpty())
					KeywordsTextBox.Text = criteria.KeywordCriteria.Keywords;

				if (criteria.KeywordCriteria.Context.IsNotNull())
					KeywordContextDropDown.SelectValue(criteria.KeywordCriteria.Context.ToString());

				if (criteria.KeywordCriteria.Scope.IsNotNull())
					KeywordScopeDropDown.SelectValue(criteria.KeywordCriteria.Scope.ToString());
			}

			#endregion

			#region Additonal Search

			if (criteria.AdditionalCriteria.IsNotNull())
			{
				if (criteria.AdditionalCriteria.IsNotNull() && criteria.AdditionalCriteria.JobTypesConsidered.IsNotNull())
				{
					JobTypeDropDownList.SelectValue(criteria.AdditionalCriteria.JobTypesConsidered.ToString());
				}

				var additionalCriteriaModel = new ResumeSearchAdditionalCriteria.Model();

				if (criteria.AdditionalCriteria.IsNotNull() || criteria.CandidateId.IsNotNull() || criteria.MinimumScore.IsNotNull())
				{
					additionalCriteriaModel.AdditionalCriteria = (criteria.AdditionalCriteria.IsNotNull()) ? criteria.AdditionalCriteria : new CandidateSearchAdditionalCriteria();
					additionalCriteriaModel.CandidateId = criteria.CandidateId;
					additionalCriteriaModel.MinimumStarRating = criteria.MinimumScore.ToStarRating(App.Settings.StarRatingMinimumScores);
				}
				else
				{
					additionalCriteriaModel.AdditionalCriteria = new CandidateSearchAdditionalCriteria();
				}

				BindAdditionalCriteria(additionalCriteriaModel);
			}

			#endregion

		}

		#region Clear Search Methods

		/// <summary>
		/// Clears the search criteria.
		/// </summary>
		private void ClearSearchCriteria()
		{
			KeywordsTextBox.Text = string.Empty;
			KeywordContextDropDown.SelectedIndex = 0;
			KeywordScopeDropDown.SelectedIndex = 0;
			AdditionalCriteria.Clear();
			ClearResume();
		}

		private void ClearResume()
		{
			ResumeContent = null;
			ResumeFilename = "";
			UploadedResumeLabel.Text = "";
		}

		#endregion

		#region Save search

		/// <summary>
		/// Handles the Click event of the SavedSearchButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SavedSearchButton_Click(object sender, EventArgs e)
		{
			if (SavedSearchDropDown.SelectedValueToLong() == 0) return;

			var savedSearchView = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearch(SavedSearchDropDown.SelectedValueToLong());
			Model.SearchId = Guid.Empty;
			Model.SearchCriteriaId = Guid.NewGuid();
			Model.SearchCriteria = savedSearchView.Criteria;
			if (Model.SearchCriteria.JobDetailsCriteria.IsNull())
				Model.SearchCriteria.JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = Model.JobId };

			SearchResultListPager.ReturnToFirstPage();

			BindCandidateList();
			LoadSearchCriteria();
			SearchSummarySectionPanel.Visible = true;
		}

		/// <summary>
		/// Handles the Click event of the SaveSearchButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveSearchButton_Click(object sender, EventArgs e)
		{
			SearchCriteria = null; // new CandidateSearchCriteria();

			SavedSearchId = SavedSearchDropDown.SelectedIndex > 0 ? SavedSearchDropDown.SelectedValueToLong() : 0;

			if (SavedSearchId == 0)
			{
				var savedSearchCount = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearchView();
				if (savedSearchCount.Count >= App.Settings.MaximumSavedSearches)
				{
					Confirmation.Show(CodeLocalise("SavedSearchesMaximumExceeded.Title", "Saved searches exceeded"),
						CodeLocalise("SavedSearchesMaximumExceeded.Body",
							"Saving this search exceeds the maximum number of searches you are allowed.<br><br>Please go to your Manage Saved Searches page via Account Settings to delete any unwanted searches before continuing."),
						CodeLocalise("Global.Close.Text", "Close"));
					return;
				}
			}

			// We need to work out if this is a search based on an Resume, Job or Keywords
			if (ResumeFilename.IsNotNullOrEmpty() || ResumeFileUpload.HasFile)
			{
				GetResumeSearchDetails();

				var additionalCriteriamodel = UnbindAdditionalCriteria();

				SearchCriteria = new CandidateSearchCriteria
				{
					SearchType = CandidateSearchTypes.ByResume,
					ResumeCriteria = UnbindResumeCriteria(),
					AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
					MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0,
					GetNotesAndRemindersCount = true
				};
			}
			else if (KeywordsTextBox.TextTrimmed().IsNotNullOrEmpty())
			{
				var additionalCriteriamodel = UnbindAdditionalCriteria();

				SearchCriteria = new CandidateSearchCriteria
				{
					SearchType = CandidateSearchTypes.ByKeywords,
					KeywordCriteria = UnbindKeywordSearchCriteria(),
					AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
					MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0,
					GetNotesAndRemindersCount = true
				};
			}
			else
			{
				SaveSearchErrorLabel.Text = CodeLocalise("SaveSearch.ErrorMessage", "Please enter search criteria before saving a search.");
				OnCompleted(e);
				return;
			}

			if (SavedSearchId > 0)
			{
				SaveSearchAction.Show(SavedSearchId, SearchCriteria);
				SavedSearchDropDown.SelectedValue = SavedSearchId.ToString();
			}
			else
			{
				if (SearchCriteria.IsNotNull())
					SaveSearchAction.Show(SearchCriteria);
			}

		}

		/// <summary>
		/// Handles the Completed event of the PoolSearchAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveSearchAction_Completed(object sender, EventArgs e)
		{
			BindSavedSearchDropDown();
			if (SavedSearchId > 0)
				SavedSearchDropDown.SelectedValue = SavedSearchId.ToString();
			OnCompleted(e);
		}

		/// <summary>
		/// Handles the Completed event of the CompareResumes control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CompareResumes_Completed(object sender, EventArgs e)
		{
			Model.SearchId = Guid.Empty;
			SearchResultListView.DataBind();
		}

		/// <summary>
		/// Raises the <see cref="E:Completed"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCompleted(EventArgs eventArgs)
		{
			if (Completed != null)
				Completed(this, eventArgs);
		}

		public event SaveSearchActionModal.CompletedHandler Completed;

		/// <summary>
		/// Binds the save search drop down.
		/// </summary>
		private void BindSavedSearchDropDown()
		{
			SavedSearchDropDown.DataSource = ServiceClientLocator.SearchClient(App).GetCandidateSavedSearchView();
			SavedSearchDropDown.DataValueField = "Id";
			SavedSearchDropDown.DataTextField = "Name";
			SavedSearchDropDown.DataBind();

			var defaultItem = new ListItem(CodeLocalise("SavedSearch.TopDefault", "- select saved search  -"), string.Empty);
			SavedSearchDropDown.Items.Insert(0, defaultItem);
		}

		#endregion

		/// <summary>
		/// Registers the javascript.
		/// </summary>
		private void RegisterJavascript()
		{
			// Build the JS
			var js = @"function pageLoad() {
	        $(document).ready(function()
	        {
		        // Apply jQuery to infield labels
		        $("".inFieldLabel > label"").inFieldLabels();
	        });
        }

        $(document).ready(function()
        {
						ShowHideCriteria();

						$(""#" + JobTypeDropDownList.ClientID + @""").change(function() {
							ShowHideCriteria();
						});

						$(""#" + ResumeFileUpload.ClientID + @""").change(function () {

		        var filename = $(""#" + ResumeFileUpload.ClientID + @""").val().replace(/(c:\\)*fakepath\\/i, '');
		        $(""#UploadedResumeDiv"").show();
		        $(""#" + UploadedResumeLabel.ClientID + @""").text(""" +
			         CodeLocalise("SampleResumeSearch.FileUploaded.Label", "Basing search on") + @" "" + filename);
	        });

	        if($(""#" + UploadedResumeLabel.ClientID + @""").text() != ''){
		        $(""#UploadedResumeDiv"").show();
	        }

	        $(""#" + KeywordContextDropDown.ClientID + @""").change(function () {
		        if ($(""#" +
			         KeywordContextDropDown.ClientID + @""").val() == """ + KeywordContexts.JobTitle + @"""
				        || $(""#" + KeywordContextDropDown.ClientID + @""").val() == """ + KeywordContexts.EmployerName + @""") {
			        $(""#" +
			         KeywordScopeDropDown.ClientID + @""").removeAttr('disabled');
		        }
		        else {
			        $(""#" + KeywordScopeDropDown.ClientID + @""").attr('disabled', 'disabled');
			        $(""#" + KeywordScopeDropDown.ClientID + @""").val(""" + KeywordScopes.EntireWorkHistory + @""");
		        }	
	        });
	
	        if ($(""#" +
			         KeywordContextDropDown.ClientID + @""").val() == """ + KeywordContexts.JobTitle + @"""
				        || $(""#" + KeywordContextDropDown.ClientID + @""").val() == """ + KeywordContexts.EmployerName + @""") {
			        $(""#" +
			         KeywordScopeDropDown.ClientID + @""").removeAttr('disabled');
		        }
		        else {
			        $(""#" + KeywordScopeDropDown.ClientID + @""").attr('disabled', 'disabled');
			        $(""#" + KeywordScopeDropDown.ClientID + @""").val(""" + KeywordScopes.EntireWorkHistory + @""");
		        }
		
	        // Validation
	        $(""#" +
			         KeywordSearchButton.ClientID + @""").click(function() {
		        var valid = validateGroup(""PoolSearchKeywordSearch"");// && validateGroup(""PoolSearchAdvancedSearch"");
		        if(!valid) return false;
		        showProcessing();	
	        });

	        $(""#" + SampleResumeButton.ClientID + @""").click(function() {
		        var valid = validateGroup(""PoolSearchResumeSearch"") && validateGroup(""PoolSearchAdvancedSearch"");	
		        if(!valid) return false;
		        showProcessing();
	        });

	        $(""#" + SaveSearchButton.ClientID + @""").click(function() {
		        return validateGroup(""PoolSearchResumeSearch"") && validateGroup(""PoolSearchAdvancedSearch"");
	        });

	        $(""#" + ApplyAdvancedSearchButton.ClientID + @""").click(function() {
		        var valid = validateGroup(""PoolSearchAdvancedSearch"");
		        if(!valid) return false;
		        showProcessing();
	        });

	        $(""#" + SavedSearchButton.ClientID +
			         @""").click(function() {
		        var valid = validateGroup(""PoolSavedSearch"");
		        if(!valid) return false;
		        showProcessing();
	        });

	        // Apply jQuery to infield labels
	        $("".inFieldLabel > label"").inFieldLabels();
        });

        function validateGroup(groupName)
        {
	        var isValid = false;
		        try{
			        isValid = Page_ClientValidate(groupName);
		        } catch(e){}
		        return isValid;
        }

				function ShowHideCriteria(){
					var theme = '" + App.Settings.Theme + @"';
					if(theme == '" + FocusThemes.Education + @"') {					
						if ($(""#" + JobTypeDropDownList.ClientID + @""").prop(""selectedIndex"") == 0) {
							HideSkillsCriteria();
							ShowExpectedCompletionSection();
							ShowEnrollmentStatusDropDown();
							ShowAvailabilityCriteria();
							ShowScoreCriteria();
							HideGradePointAverageCriteria();
							SetEducationCriteriaTitle(""" + CodeLocalise("EducationCriteria.AlternativeTitle", "Expected Completion Date & Enrollment Status") + @""");
						} else {
							ShowSkillsCriteria();
							HideExpectedCompletionSection();
							ShowEnrollmentStatusDropDown();
							HideAvailabilityCriteria();
							HideScoreCriteria();
							ShowGradePointAverageCriteria();
							SetEducationCriteriaTitle(""" + CodeLocalise("EducationCriteria.Title", "Education") + @""");
						}
					}
				}
";

			ScriptManager.RegisterClientScriptBlock(this, typeof(Pool), "PoolSearchJavaScript", js, true);
		}

		private void SetDefaultJobTypes()
		{
			// Get job data
			var job = ServiceClientLocator.JobClient(App).GetJob(Model.JobId);

			// No job, throw error
			if (job.IsNull())
				throw new Exception("Invalid Job Id");

			JobTypes jobTypesConsidered;

			// Get job type
			switch (job.JobType)
			{
				case JobTypes.InternshipPaid:
				case JobTypes.InternshipUnpaid:
					jobTypesConsidered = JobTypes.InternshipPaid | JobTypes.InternshipUnpaid;
					break;
				default:
					jobTypesConsidered = JobTypes.Job;
					break;
			}

			// Create new search criteria object
			if (SearchCriteria.IsNull())
				SearchCriteria = new CandidateSearchCriteria();

			if (SearchCriteria.AdditionalCriteria.IsNull())
				SearchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria();

			// Set job tyep criteria
			if (SearchCriteria.AdditionalCriteria.JobTypesConsidered.IsNull() || SearchCriteria.AdditionalCriteria.JobTypesConsidered == JobTypes.None)
			{
				SearchCriteria.AdditionalCriteria.JobTypesConsidered = jobTypesConsidered;
			}

			if (SearchCriteria.AdditionalCriteria.EducationCriteria.IsNull())
			{
				SearchCriteria.AdditionalCriteria.EducationCriteria = new CandidateSearchEducationCriteria()
				{
					IncludeAlumni = true
				};
			}
			// Set expected completion date if this 
			if (jobTypesConsidered == JobTypes.Job)
			{
				SearchCriteria.AdditionalCriteria.EducationCriteria.MonthsUntilExpectedCompletion = 6;
			}
			// Set enrollment status if this is an internship which requires a student to be enrolled
			if ((job.JobType == JobTypes.InternshipPaid || job.JobType == JobTypes.InternshipUnpaid) && job.StudentEnrolled.GetValueOrDefault())
			{
				if (job.MinimumCollegeYears.HasValue && job.MinimumCollegeYears > 0)
				{
					if (App.Settings.SchoolType == SchoolTypes.FourYear)
						SearchCriteria.AdditionalCriteria.EducationCriteria.EnrollmentStatus = SchoolStatus.Sophomore;
					else
						SearchCriteria.AdditionalCriteria.EducationCriteria.EnrollmentStatus = SchoolStatus.SophomoreOrAbove;
				}
			}

			// Get programs of study for current job
			var programsOfStudy = ServiceClientLocator.JobClient(App).GetJobProgramsOfStudy(Model.JobId);

			// If we have some then we need to update the controls
			if (programsOfStudy.IsNotNullOrEmpty())
			{
				SearchCriteria.AdditionalCriteria.EducationCriteria.ProgramsOfStudy = new List<KeyValuePairSerializable<string, string>>();

				foreach (var programOfStudy in programsOfStudy)
				{
					SearchCriteria.AdditionalCriteria.EducationCriteria.ProgramsOfStudy.Add(
						new KeyValuePairSerializable<string, string>(programOfStudy.DegreeEducationLevelId.ToString(),
							programOfStudy.ProgramOfStudy));
				}
			}

			var additionalCriteriaModel = new ResumeSearchAdditionalCriteria.Model
			{
				AdditionalCriteria =
					new CandidateSearchAdditionalCriteria { EducationCriteria = SearchCriteria.AdditionalCriteria.EducationCriteria }
			};

			BindAdditionalCriteria(additionalCriteriaModel);

			Model.SearchCriteria = SearchCriteria;
			JobTypeDropDownList.SelectValue(SearchCriteria.AdditionalCriteria.JobTypesConsidered.ToString());
		}

		#endregion

		#region events

		#region Results events

		/// <summary>
		/// Gets the candidates.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="startRowIndex">Start index of the row.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <returns></returns>
		public List<ResumeView> GetCandidates(AssistTalentPoolModel criteria, string orderBy, int startRowIndex, int maximumRows)
		{
			var pageIndex = ((int)Math.Floor((double)startRowIndex / maximumRows));

			PagedList<ResumeView> candidates = null;

			criteria.SearchCriteria.PageIndex = pageIndex;
			criteria.SearchCriteria.PageSize = maximumRows;
			criteria.SearchCriteria.OrderBy = orderBy;

			switch (criteria.CurrentTab)
			{
				case TalentPoolListTypes.MatchesForThisJob:
				{
					criteria.SearchCriteria.ScopeType = CandidateSearchScopeTypes.Open;
					criteria.SearchCriteria.SearchType = CandidateSearchTypes.ByJobDescription;
					criteria.SearchCriteria.GetRecentlyMatchedInfo = true;

					var result = ServiceClientLocator.SearchClient(App).SearchCandidates(criteria.SearchId, criteria.SearchCriteria);
					candidates = result.Candidates;

					var applications = ServiceClientLocator.CandidateClient(App).GetApplication(candidates.Select(c => c.Id).ToList(), criteria.JobId);

					foreach (var candidate in candidates)
					{
						candidate.ApplicationApprovalStatus = applications[candidate.Id].ApprovalStatus;
					}
					_searchId = result.SearchId;
				}
					break;
				case TalentPoolListTypes.AllResumes:
				{
					criteria.SearchCriteria.ScopeType = CandidateSearchScopeTypes.Open;
					criteria.SearchCriteria.JobDetailsCriteria = new CandidateSearchJobDetailsCriteria();

					var result = ServiceClientLocator.SearchClient(App).SearchCandidates(criteria.SearchId, criteria.SearchCriteria);
					candidates = result.Candidates;

					var applications = ServiceClientLocator.CandidateClient(App).GetApplication(candidates.Select(c => c.Id).ToList(), criteria.JobId);

					foreach (var candidate in candidates)
					{
						candidate.ApplicationApprovalStatus = applications[candidate.Id].ApprovalStatus;
						candidate.ApplicationId = applications[candidate.Id].Id ?? Convert.ToInt64(applications[candidate.Id].Id);
					}

					_searchId = result.SearchId;
				}
					break;
				case TalentPoolListTypes.Applicants:
					if (criteria.SearchCriteriaId == Guid.Empty)
					{
						candidates = ServiceClientLocator.CandidateClient(App).GetApplicantResumeViews(new ResumeViewCriteria
						{
							JobId = criteria.JobId,
							ApprovalStatus = ApprovalStatuses.Approved,
							GetNotesAndRemindersCount = true,
							PageIndex = pageIndex,
							PageSize = maximumRows,
							OrderBy = orderBy
						});
					}
					else
					{
						criteria.SearchCriteria.JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = criteria.JobId };
						criteria.SearchCriteria.ScopeType = CandidateSearchScopeTypes.JobApplicants;

						var result = ServiceClientLocator.SearchClient(App).SearchCandidates(criteria.SearchId, criteria.SearchCriteria);
						candidates = result.Candidates;
						_searchId = result.SearchId;
					}
					break;
				case TalentPoolListTypes.Referrals:
					if (criteria.SearchCriteriaId == Guid.Empty)
					{
						candidates = ServiceClientLocator.CandidateClient(App).GetApplicantResumeViews(new ResumeViewCriteria
						{
							JobId = criteria.JobId,
							ApprovalStatusList = new List<ApprovalStatuses>() { ApprovalStatuses.WaitingApproval, ApprovalStatuses.Reconsider },
							GetNotesAndRemindersCount = true,
							PageIndex = pageIndex,
							PageSize = maximumRows,
							OrderBy = orderBy
						});

					}
					else
					{
						criteria.SearchCriteria.JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = criteria.JobId };
						criteria.SearchCriteria.ScopeType = CandidateSearchScopeTypes.JobApplicantsAwaitingApproval;

						var result = ServiceClientLocator.SearchClient(App).SearchCandidates(criteria.SearchId, criteria.SearchCriteria);
						candidates = result.Candidates;
						_searchId = result.SearchId;
					}

					break;

                case TalentPoolListTypes.Invitees:
                    candidates = ServiceClientLocator.CandidateClient(App).GetInviteesForJob(new InviteesResumeViewCriteria
                    {
                        JobId = criteria.JobId,
                        PageIndex = pageIndex
                    });
                    break;
			}

			CandidateCount = (candidates.IsNotNull()) ? candidates.TotalCount : 0;

			return candidates;
		}



		/// <summary>
		/// Handles the ItemDataBound event of the SearchResultListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void SearchResultListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var candidate = (ResumeView)e.Item.DataItem;

			#region Name / Id

			var nameLink = (LinkButton)e.Item.FindControl("SearchResultNameLink");
			nameLink.Text = candidate.Name != "" ? candidate.Name.NormaliseSpace() : CodeLocalise("Unknown.Text", "Unknown");

			#endregion

			#region Flagged Image

			var flaggedImage = (Image)e.Item.FindControl("FlaggedImage");
			if (candidate.Flagged) BindFlaggedImage(flaggedImage);

			#endregion

			#region Veteran Image

			var veteranImage = (Image)e.Item.FindControl("VeteranImage");
			if (candidate.IsVeteran) BindVeteranImage(veteranImage);

			#endregion

		    #region Invite Queued Status
		    var inviteStatusLiteral = (Literal)e.Item.FindControl("SearchResultInviteesQueuedStatusLiteral");
		    inviteStatusLiteral.Text = CodeLocalise("InviteStatusLiteral", "Invitation is pending release of Veteran Priority of Services exclusion");
            if (!candidate.IsVeteran && (Model.CurrentTab == TalentPoolListTypes.Invitees) && candidate.IsInviteQueued)
		        inviteStatusLiteral.Visible = true;

		    #endregion

			#region Similar To

			var linkButton = (LinkButton)e.Item.FindControl("SimilarToLinkButton");

			if (candidate.RecentlyPlacedPersonId != null && Model.CurrentTab == TalentPoolListTypes.MatchesForThisJob)
			{
				linkButton.Text = String.Format("<em>{0}<br/>{1}</em>", CodeLocalise("SimilarTo", "Similar to"), candidate.RecentlyPlacedCandidateName);
				linkButton.CommandArgument = string.Format("{0};{1};{2};{3};{4};{5};{6};{7}", candidate.RecentlyPlacedJobTitle, candidate.RecentlyPlacedPersonId, candidate.RecentlyPlacedCandidateName, candidate.RecentlyPlaceBusinessUnitName, candidate.Name, candidate.Id, candidate.RecentlyPlacedScore, candidate.RecentlyPlacedMatchId);
				linkButton.Visible = true;
			}

			var similarToMessage = (Literal)e.Item.FindControl("SimilarToMessage");

			if (candidate.HasRecentlyPlacedMatch && Model.CurrentTab == TalentPoolListTypes.MatchesForThisJob)
			{
				similarToMessage.Text = String.Format("<br/><em>{0}</em>", CodeLocalise("SimilarToMessage", "similar to others who have placed into jobs like this"));
				similarToMessage.Visible = true;
			}


			#endregion

			#region Score

			var scoreImage = (Image)e.Item.FindControl("SearchResultScoreImage");
			BindScore(scoreImage, candidate.Score);
			if (Model.CurrentTab == TalentPoolListTypes.FlaggedResumes) scoreImage.ToolTip = CodeLocalise("NoScoringPossible.Text", "No scoring possible");

			#endregion

			#region Employment History

			var employmentHistoryLiteral = (Literal)e.Item.FindControl("SearchResultEmploymentHistoryLiteral");
			BindEmploymentHistory(employmentHistoryLiteral, candidate.EmploymentHistories);

			#endregion

			#region Application Status

			if (Model.JobId > 0 && candidate.Status != ApplicationStatusTypes.NotApplicable && candidate.Status != ApplicationStatusTypes.SelfReferred && candidate.ApplicationApprovalStatus == ApprovalStatuses.Approved &&
			    Model.CurrentTab == TalentPoolListTypes.Applicants)
			{
				var applicationStatusDropDownList = (DropDownList)e.Item.FindControl("SearchResultApplicationStatusDropDown");
				BindStatus(applicationStatusDropDownList, candidate.Status);

				if (!candidate.IsVeteran && candidate.ApplicationVeteranPriorityEndDate.IsNotNull() && candidate.ApplicationVeteranPriorityEndDate > DateTime.Now)
				{
					var queuedForVeteranLiteral = (Literal)e.Item.FindControl("QueuedForVeteranLiteral");
					queuedForVeteranLiteral.Text = HtmlLocalise("QueuedForVeteranLiteral.Text", "Queued for release");
					queuedForVeteranLiteral.Visible = true;
				}
				else
				{
					applicationStatusDropDownList.Visible = true;
				}
			}

			if (Model.CurrentTab.IsIn(TalentPoolListTypes.Referrals, TalentPoolListTypes.MatchesForThisJob, TalentPoolListTypes.AllResumes) && Model.JobId > 0 && (candidate.ApplicationApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.Reconsider, ApprovalStatuses.Rejected)))
			{
				var viewReferralButton = (Button)e.Item.FindControl("ViewReferralRequest");
				viewReferralButton.Text = HtmlLocalise("ViewReferralRequest.Text", "View referral request");
				viewReferralButton.CommandArgument = candidate.ApplicationId.ToString(CultureInfo.InvariantCulture);
				viewReferralButton.Visible = true;
			}

			#endregion

			#region Referral Status

			if (App.Settings.Theme != FocusThemes.Education && Model.JobId > 0
			    && Model.JobStatus.IsNotIn(JobStatuses.Closed, JobStatuses.Draft, JobStatuses.AwaitingEmployerApproval)
			    && candidate.Status != ApplicationStatusTypes.NewApplicant && candidate.ApplicationApprovalStatus.IsNotIn(ApprovalStatuses.Approved, ApprovalStatuses.Reconsider, ApprovalStatuses.WaitingApproval, ApprovalStatuses.Rejected) && Model.CurrentTab.IsIn(TalentPoolListTypes.MatchesForThisJob, TalentPoolListTypes.AllResumes))
			{
				var referButton = (Button)e.Item.FindControl("ReferButton");
				referButton.Visible = true;
				referButton.Text = CodeLocalise("ReferButton.Label", "Refer");
			}

			#endregion

			#region Employer Notes

			var notesTextBox = (TextBox)e.Item.FindControl("NotesTextBox");
			notesTextBox.Text = candidate.EmployerNote;

			#endregion

			#region Assist User Notes

			var notesDiv = (HtmlGenericControl)e.Item.FindControl("CandidateNotesDiv");
			notesDiv.Attributes["class"] = candidate.NotesAndRemindersCount > 0 ? "tooltip left Noted" : "tooltip left Note";

			#endregion

			#region Email Icon

			var searchResultEmailButton = (LinkButton)e.Item.FindControl("SearchResultEmailButton");
			searchResultEmailButton.Visible = Model.JobId > 0 && Model.JobStatus != JobStatuses.Closed && Model.JobStatus != JobStatuses.OnHold && Model.JobStatus != JobStatuses.Draft;

			#endregion
		}

		/// <summary>
		/// Handles the DataBound event of the SearchResultListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void SearchResultListView_DataBound(object sender, EventArgs e)
		{
			Model.SearchId = _searchId;
			SearchCriteria = Model.SearchCriteria;
		}

		/// <summary>
		/// Handles the Selected event of the SearchResultDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
		protected void SearchResultDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
		{
			var list = e.ReturnValue as List<ResumeView>;

			// Save the list to view state so it can be accessed on post back
			if (list.IsNotNull())
				CandidatesInList = list;
		}

		/// <summary>
		/// Handles the Selecting event of the AssistanceSearchResultDataSource control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void SearchResultDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["criteria"] = Model;
		}

		/// <summary>
		/// Handles the Command event of the SearchResultRowAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void SearchResultRowAction_Command(object sender, CommandEventArgs e)
		{
			var rowNo = Convert.ToInt32(e.CommandArgument);
			var id = Convert.ToInt64(SearchResultListView.DataKeys[rowNo]["Id"]);

			switch (e.CommandName)
			{
				case "CandidatesLikeThis":
					CandidatesLikeThis.Show(id);
					break;
				case "Email":
					var applicationStatus = (ApplicationStatusTypes)Enum.Parse(typeof(ApplicationStatusTypes), SearchResultListView.DataKeys[rowNo]["Status"].ToString());
					var name = SearchResultListView.DataKeys[rowNo]["Name"].ToString();
			    bool isVeteran = (bool) SearchResultListView.DataKeys[rowNo]["IsVeteran"];

          // Job available only to veterans for the life of posting
          if (Model.ExtendVeteranPriority.IsNotNull() && (!isVeteran && (Model.ExtendVeteranPriority.Equals(ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting))))
          {
            Confirmation.Show(CodeLocalise("RecommendVeteranPosting.Title", "Posting exclusive to veterans"),
            CodeLocalise("RecommendVeteranPosting.Body",
              "This job is set to exclude non-veteran invitations or recommendations for the lifetime of the posting. You cannot invite or recommend that this job seeker apply."),
              CodeLocalise("CloseModal.Text", "OK"));
            return;
          }

					var autoApprovalAvailable = AutoApprovalModal.Show(id, Model.JobId, name);
					if (!autoApprovalAvailable)
					{
						if (Model.CurrentTab.IsIn(TalentPoolListTypes.AllResumes, TalentPoolListTypes.MatchesForThisJob) && applicationStatus == ApplicationStatusTypes.NotApplicable)
						{
							var lensPostingId = ServiceClientLocator.JobClient(App).GetJobLensPostingId(Model.JobId);

							var post = (lensPostingId.IsNotNullOrEmpty()) ? ServiceClientLocator.PostingClient(App).GetJobPosting(lensPostingId) : null;

							if (post.IsNotNull() && post.PostingXml.IsNotNullOrEmpty())
							{
								var xDoc = new XmlDocument { PreserveWhitespace = true };
								xDoc.LoadXml(post.PostingXml);

                                //FVN-5596 - commenting the below lines based on Gail's comments to ensure the email has the URL to apply for the job via Focus Career when 'apply online'contact preference is chosen
                                //var navigateUrl = string.Empty;

                                //var node = xDoc.SelectSingleNode("//joburl");
                                //if (node.IsNotNull())
                                //{
                                //    if (xDoc.SelectSingleNode("//joburl") != null && !String.IsNullOrEmpty(node.InnerText))
                                //        navigateUrl = node.InnerText;
                                //}

								var score = Convert.ToInt32(SearchResultListView.DataKeys[rowNo]["Score"]);

                                var jobClient = ServiceClientLocator.JobClient(App);
                                var isExistingApplicant = ServiceClientLocator.CandidateClient(App).IsCandidateExistingJobInviteeOrApplicant(Model.JobId, id);

                                if (isExistingApplicant.Exists && isExistingApplicant.IsApplicationPendingApproval)
                                {
                                    var application = ServiceClientLocator.CandidateClient(App).GetApplication(id, Model.JobId);

                                    var candidateName = (application.ApplicationStatus != ApplicationStatusTypes.NotApplicable &&
                                                            application.ApplicationStatus != ApplicationStatusTypes.SelfReferred &&
                                                            name.IsNotNullOrEmpty() && Model.JobId != 0)
                                        ? name
                                        : CodeLocalise("TheJobSeeker.Text", "the job seeker");


                                    Confirmation.Show(CodeLocalise("PendingApplication.Title", "Pending application"),
                                        CodeLocalise("PendingApplicationApproved.Body", "An application has already been received from " + candidateName + " and was waiting to be approved."),
                                        CodeLocalise("Global.Close.Text", "Close"));
                                    return;
                                }
                                else if (isExistingApplicant.Exists && applicationStatus == ApplicationStatusTypes.NotApplicable)
                                {
                                    MasterPage.ShowError(AlertTypes.Info,"This job seeker is already an invitee or applicant to this job");
                                    return;
                                }
                                else if (ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(id, true) == 0)
                                {
                                    MasterPage.ShowError(AlertTypes.Error, "This job seeker currently does not have a completed resume");
                                    return;
                                }
                                else
                                {

                                    EmailCandidate.Show(EmailTemplateTypes.RecommendJobSeeker, id, Model.JobId, lensPostingId, Model.EmployerName, Model.JobTitle,
                 Model.VeteranPriorityEndDate, Model.ExtendVeteranPriority, "", true, score, isVeteran);
                                }
							}
						}
						else
						{
							EmailCandidate.Show(id, Model.JobId);
						}
					}

					break;
				case "Preview":
					var candidate = CandidatesInList.FirstOrDefault(x => x.Id == id);
					if (candidate.IsNotNull()) ResumePreview.Show(candidate, Model.JobId);
					break;
				case "Note":
					if (id.IsNotNull()) NotesAndReminders.Show(id, EntityTypes.JobSeeker);
					break;
				case "SaveNote":
					//var notesTextBox = (TextBox)SearchResultListView.Items[rowNo].FindControl("NotesTextBox");
					//if (notesTextBox.IsNotNull())
					//{
					//  ServiceClientLocator.CandidateClient(App).SaveCandidateNote(id, App.User.UserId, notesTextBox.TextTrimmed());
					//  Confirmation.Show(CodeLocalise("SaveNoteConfirmation.Title", "Note saved"),
					//                    CodeLocalise("SaveNoteConfirmation.Body", "Note has been saved to candidate."),
					//                    CodeLocalise("Global.Close.Text", "Close"));
					//}
					//SearchResultListView.DataBind();
					break;
				case "CloseNote":
					SearchResultListView.DataBind();
					break;
			}
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the SearchResultApplicationStatusDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SearchResultApplicationStatusDropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			var statusDropDown = (DropDownList)sender;
			var listViewDataItem = (ListViewDataItem)statusDropDown.Parent;
			var personId = Convert.ToInt64(SearchResultListView.DataKeys[listViewDataItem.DisplayIndex]["Id"]);

			ServiceClientLocator.CandidateClient(App).UpdateApplicationStatus(personId, Model.JobId, statusDropDown.SelectedValueToEnum<ApplicationStatusTypes>());

			Confirmation.Show(CodeLocalise("UpdateApplicationStatusConfirmation.Title", "Application updated"),
				CodeLocalise("UpdateApplicationStatusConfirmation.Body", "The status of the application has been updated."),
				CodeLocalise("Global.Close.Text", "Close"));
		}

		/// <summary>
		/// Handles the Click event of the ViewNotesButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ViewNotesButton_Click(object sender, EventArgs e)
		{
			NotesAndReminders.Show(Model.JobId, EntityTypes.Job);
		}

		#endregion

		#region search events

		/// <summary>
		/// Handles the Click event of the ApplyAdvancedSearch and keyword search controls.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Search_Click(object sender, EventArgs e)
		{
			// Set new guid so new search session is used
			SearchResultListPager.ReturnToFirstPage();
			Model.SearchId = Guid.Empty;
			Model.SearchCriteriaId = Guid.NewGuid();

			var candidateId = AdditionalCriteria.GetCandidateId();

			var additionalCriteriamodel = UnbindAdditionalCriteria();

			Model.SearchCriteria = new CandidateSearchCriteria
			{
				CandidateId = candidateId,
				SearchType = (Model.JobId == 0 || Model.CurrentTab == TalentPoolListTypes.AllResumes)
					? CandidateSearchTypes.OpenSearch
					: CandidateSearchTypes.ByJobDescription,
				KeywordCriteria = UnbindKeywordSearchCriteria(),
				AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
				MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0,
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = Model.JobId },
				GetNotesAndRemindersCount = true
			};
			//}

			BindCandidateList();
			SearchSummarySectionPanel.Visible = true;
		}

		#endregion

		#region Resume preview events

		/// <summary>
		/// Handles the Email Candidate Click event of the ResumePreviewModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
		protected void ResumePreviewModalEmailCandidate_Click(object sender, ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs e)
		{
			var autoApprovalAvailable = AutoApprovalModal.Show(e.CandidateId, Model.JobId);
			if (!autoApprovalAvailable)
				EmailCandidate.Show(e.CandidateId, Model.JobId);
		}



		/// <summary>
		/// Handles the Email Candidate Click event of the ResumePreviewModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Web.Code.Controls.User.ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
		protected void ResumePreviewModalEmailResumeCandidate_Click(object sender, ResumePreviewModal.ResumePreviewEmailResumeCandidateClickEventArgs e)
		{
			EmailResume.Show(e.ResumeId, true);
		}

		/// <summary>
		/// Handles the Email Candidate Click event of the ResumePreviewModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ResumePreviewModal.ResumePreviewEmailCandidateClickEventArgs"/> instance containing the event data.</param>
		protected void CandidatesLikeThisModalEmailCandidate_Click(object sender, CandidatesLikeThisModal.CandidatesLikeThisEmailCandidateClickEventArgs e)
		{
			var autoApprovalAvailable = AutoApprovalModal.Show(e.CandidateId, Model.JobId);
			if (!autoApprovalAvailable)
				EmailCandidate.Show(e.CandidateId, Model.JobId);
		}

		/// <summary>
		/// Handles the Candidates Like Me Click event of the ResumePreview control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ResumePreviewModal.ResumePreviewCandidatesLikeMeEventArgs"/> instance containing the event data.</param>
		protected void ResumePreviewCandidatesLikeMe_Click(object sender, ResumePreviewModal.ResumePreviewCandidatesLikeMeEventArgs e)
		{
			CandidatesLikeThis.Show(e.CandidateId);
		}

		#endregion

		#region Resume upload event

		/// <summary>
		/// Handles the Click event of the SampleResumeButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SampleResumeButton_Click(object sender, EventArgs e)
		{
			//Clear other search controls
			KeywordsTextBox.Text = "";
			KeywordContextDropDown.SelectedIndex = 0;
			KeywordScopeDropDown.SelectedIndex = 0;
			SavedSearchDropDown.SelectedIndex = 0;

			GetResumeSearchDetails();

			// Set new guid so new search session is used
			Model.SearchId = Guid.Empty;
			Model.SearchCriteriaId = Guid.NewGuid();

			var additionalCriteriamodel = UnbindAdditionalCriteria();

			Model.SearchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.ByResume,
				ResumeCriteria = UnbindResumeCriteria(),
				AdditionalCriteria = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.AdditionalCriteria : null,
				MinimumScore = (additionalCriteriamodel.IsNotNull()) ? additionalCriteriamodel.MinimumStarRating.ToStarRatingMinimumScore(App.Settings.StarRatingMinimumScores) : 0,
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = Model.JobId },
				GetNotesAndRemindersCount = true
			};


			SearchCriteria = Model.SearchCriteria;

			Response.Redirect(UrlBuilder.AssistPool(TalentPoolListTypes.AllResumes, Model.JobId, Model.SearchCriteriaId.ToString()));
		}

		#endregion

		#region Clear search event

		/// <summary>
		/// Clear keyword and advance search criteria 
		/// </summary>
		/// <param name="sender">The source of the event</param>
		/// <param name="e">The system event</param>
		protected void ClearSearchButton_Click(object sender, EventArgs e)
		{
			ClearSearchCriteria();
			Response.Redirect(UrlBuilder.AssistPool(Model.CurrentTab, Model.JobId));
		}

		#endregion

		#region Refer candidate events

		/// <summary>
		/// Handles the Click event of the ReferButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ReferButton_Click(object sender, CommandEventArgs e)
		{
			int rowId;
			int.TryParse(e.CommandArgument.ToString(), out rowId);
			bool alienRegistrationExpire = false;

			var row = SearchResultListView.DataKeys[rowId];
			if (row != null)
			{
				var personId = Convert.ToInt64(row["Id"]);
				var score = Convert.ToInt32(row["Score"]);
				var name = row["Name"].ToString();
                this.Page.Title = "Refer - " + personId;

				long defaultResumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(personId, true);
				var defaultResume = ServiceClientLocator.ResumeClient(App).GetResume(defaultResumeId);

				if (defaultResume.IsNotNull() && defaultResume.ResumeContent.Profile.IsNotNull())
				{
					var userProfile = defaultResume.ResumeContent.Profile;

					if (!(userProfile.IsUSCitizen ?? true))
					{
						if (userProfile.AlienExpires.HasValue)
						{
							if (userProfile.AlienExpires < DateTime.Now)
							{
								alienRegistrationExpire = true;
							}
						}
					}
				}

				// if non US and registraton expired then do not allow posting
				if (alienRegistrationExpire)
				{
					ReferralConfirmationModal.Show(CodeLocalise("ReferralQueued.Title", "Unable to refer"),
						CodeLocalise("ReferralQueued.Body", "The selected job seeker has a lapsed alien registration expiry date on their default resume and therefore you are unable to refer them to this posting."),
						CodeLocalise("Global.Close.Text", "Close"));
					ConfirmationUpdatePanel.Update();

				}
				else
				{
					var autoApprovalAvailable = AutoApprovalModal.Show(personId, Model.JobId, name);

					if (!autoApprovalAvailable)
						ApplicationRequirementsModal.Show(Model.JobId, personId, score);
				}
			}
		}

		#endregion

		/// <summary>
		/// Handles the Completed event of the EmailCandidate control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void EmailCandidate_Completed(object sender, EmailJobSeekerCompletedEventArgs e)
		{
			if (CandidatesLikeThis.CandidatesLikeThisDataSource.SelectParameters.Count > 0)
			{
				CandidatesLikeThis.CandidatesLikeThisListView.DataBind();
				CandidatesLikeThis.CandidatesLikeThisModalPopup.Show();
			}

			if (e.EmailTemplateType == EmailTemplateTypes.RecommendJobSeeker)
			{
        ServiceClientLocator.CandidateClient(App).RegisterJobPostingOfInterestSent(EmailCandidate.LensPostingId, EmailCandidate.CandidateIds.First(),
          EmailCandidate.Score, App.User.UserId, true, e.QueueRecommendation);
			}
		}

		/// <summary>
		/// Handles the Completed event of the NotesAndReminders control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void NotesAndReminders_Completed(object sender, EventArgs e)
		{
			SearchResultListView.DataBind();
			SearchResultUpdatePanel.Update();
		}

		protected void ApplicationRequirementsModal_Referred(object sender, JobApplicationReferredEventArgs e)
		{
			var isQueued = ServiceClientLocator.CandidateClient(App).ReferCandidate(e.PersonId, Model.JobId, e.Score, e.WaivedRequirments, false);
            if (isQueued)
            {
                var job = ServiceClientLocator.JobClient(App).GetJob(Model.JobId);
                Utilities.SendEmailIfJobContactFocusTalent(e.JobId, e.PersonId, job.VeteranPriorityEndDate);
                Utilities.SendResumeIfJobContactByEmail(e.JobId, e.PersonId, job.VeteranPriorityEndDate);
            }
            else 
            {
                Utilities.SendEmailIfJobContactFocusTalent(e.JobId, e.PersonId);
                Utilities.SendResumeIfJobContactByEmail(e.JobId, e.PersonId);
            }

			if (isQueued)
			{
				ReferralConfirmationModal.Show(CodeLocalise("ReferralQueued.Title", "Referral queued"),
					CodeLocalise("ReferralQueued.Body", "Staff referral queued for release.<br /><br />This posting currently has veteran priority. Your referral will be released once the priority has lapsed."),
					CodeLocalise("Global.Close.Text", "Close"));
				ConfirmationUpdatePanel.Update();
			}

			BindCandidateList();
		}

		/// <summary>
		/// Fires when a referral has been auto-approved
		/// </summary>
		/// <param name="sender">Auto-approval modal</param>
		/// <param name="eventargs">Standard event arguments</param>
		protected void AutoApprovalModal_OnAutoApproved(object sender, EventArgs eventargs)
		{
			BindCandidateList();
		}

		#endregion

		#region Results public/private methods

		/// <summary>
		/// Unbinds the keyword search criteria.
		/// </summary>
		/// <returns></returns>
		private CandidateSearchKeywordCriteria UnbindKeywordSearchCriteria()
		{
			#region Keywords

			return (KeywordsTextBox.TextTrimmed().IsNotNullOrEmpty())
				? new CandidateSearchKeywordCriteria
				{
					Keywords = KeywordsTextBox.TextTrimmed(),
					Context = KeywordContextDropDown.SelectedValueToEnum<KeywordContexts>(),
					Scope = KeywordScopeDropDown.SelectedValueToEnum<KeywordScopes>()
				}
				: null;

			#endregion
		}

		/// <summary>
		/// Unbinds the additional criteria.
		/// </summary>
		/// <returns></returns>
		private ResumeSearchAdditionalCriteria.Model UnbindAdditionalCriteria()
		{
			var jobTypesConsidered = (App.Settings.Theme == FocusThemes.Education) ? (JobTypes)JobTypeDropDownList.SelectedValueToFlagEnum<JobTypes>() : JobTypes.All;

			var criteria = AdditionalCriteria.Unbind(jobTypesConsidered);

			if (App.Settings.Theme == FocusThemes.Education)
				criteria.AdditionalCriteria.JobTypesConsidered = jobTypesConsidered;

			return criteria;
		}

		/// <summary>
		/// Unbinds the resume criteria.
		/// </summary>
		/// <returns></returns>
		private CandidateSearchResumeCriteria UnbindResumeCriteria()
		{
			var resumeCriteria = new CandidateSearchResumeCriteria();

			if (ResumeFilename.IsNotNullOrEmpty())
			{
				resumeCriteria = new CandidateSearchResumeCriteria
				{
					Resume = ResumeContent,
					ResumeFileExtension = Path.GetExtension(ResumeFilename),
					ResumeFilename = ResumeFilename
				};
			}

			return resumeCriteria;
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="scoreImage">The score image.</param>
		/// <param name="score">The score.</param>
		private static void BindScore(Image scoreImage, int score)
		{
			var minimumStarScores = OldApp_RefactorIfFound.Settings.StarRatingMinimumScores;

			scoreImage.ToolTip = score.ToString(CultureInfo.InvariantCulture);

			if (score >= minimumStarScores[5])
				scoreImage.ImageUrl = UrlBuilder.AssistFiveStarRatingImage();
			else if (score >= minimumStarScores[4])
				scoreImage.ImageUrl = UrlBuilder.AssistFourStarRatingImage();
			else if (score >= minimumStarScores[3])
				scoreImage.ImageUrl = UrlBuilder.AssistThreeStarRatingImage();
			else if (score >= minimumStarScores[2])
				scoreImage.ImageUrl = UrlBuilder.AssistTwoStarRatingImage();
			else if (score >= minimumStarScores[1])
				scoreImage.ImageUrl = UrlBuilder.AssistOneStarRatingImage();
			else
				scoreImage.ImageUrl = UrlBuilder.AssistZeroStarRatingImage();
		}

		/// <summary>
		/// Binds the flagged image.
		/// </summary>
		/// <param name="flaggedImage">The flagged image.</param>
		private void BindFlaggedImage(Image flaggedImage)
		{
			flaggedImage.ToolTip = flaggedImage.AlternateText = CodeLocalise("FlaggedImage.ToolTip", "Flagged");
			flaggedImage.ImageUrl = UrlBuilder.FlaggedCandidateImage();
			flaggedImage.Visible = true;
			// apply tabindex here rather than in the designer as if we apply a TabIndex="0" attribute in the designer, 
			// it is ignored at runtime and not rendered!
			flaggedImage.Attributes.Add("tabindex", "0");
		}

		/// <summary>
		/// Binds the veteran image.
		/// </summary>
		/// <param name="veteranImage">The veteran image.</param>
		private void BindVeteranImage(Image veteranImage)
		{
			veteranImage.ToolTip = veteranImage.AlternateText = CodeLocalise("VeteranImage.ToolTip", "Veteran");
			veteranImage.ImageUrl = UrlBuilder.VeteranImage();
			veteranImage.Visible = true;
			// apply tabindex here rather than in the designer as if we apply a TabIndex="0" attribute in the designer, 
			// it is ignored at runtime and not rendered!
			veteranImage.Attributes.Add("tabindex", "0");
		}

		/// <summary>
		/// Binds the employment history.
		/// </summary>
		/// <param name="employmentHistoryLiteral">The employment history literal.</param>
		/// <param name="employmentHistories">The employment histories.</param>
		private void BindEmploymentHistory(Literal employmentHistoryLiteral, IEnumerable<ResumeView.ResumeJobView> employmentHistories)
		{
			var employmentHistorySummary = new StringBuilder(string.Empty);

			var unknownText = CodeLocalise("Global.Unknown.Text", "Unknown");
			var i = 1;

			foreach (var employmentHistory in employmentHistories)
			{
				if (i > 3) break;

				employmentHistorySummary.AppendFormat("{0} - {1} ({2}-{3})<br />",
					employmentHistory.JobTitle.IsNotNullOrEmpty()
						? employmentHistory.JobTitle
						: unknownText,
					employmentHistory.Employer.IsNotNullOrEmpty()
						? employmentHistory.Employer
						: unknownText,
					employmentHistory.YearStart.IsNotNullOrEmpty()
						? employmentHistory.YearStart
						: unknownText,
					employmentHistory.YearEnd.IsNotNullOrEmpty()
						? employmentHistory.YearEnd
						: unknownText);
				i++;
			}


			if (employmentHistorySummary.ToString().IsNotNullOrEmpty())
				employmentHistoryLiteral.Text = employmentHistorySummary.ToString().Substring(0, employmentHistorySummary.ToString().Length - 6);
		}

		/// <summary>
		/// Binds the status.
		/// </summary>
		/// <param name="statusDropDownList">The status drop down list.</param>
		/// <param name="status">The status.</param>
		private void BindStatus(DropDownList statusDropDownList, ApplicationStatusTypes status)
		{
			statusDropDownList.Items.Clear();

			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.DidNotApply), ApplicationStatusTypes.DidNotApply.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToShow), ApplicationStatusTypes.FailedToShow.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToReportToJob), ApplicationStatusTypes.FailedToReportToJob.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToRespondToInvitation), ApplicationStatusTypes.FailedToRespondToInvitation.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FoundJobFromOtherSource), ApplicationStatusTypes.FoundJobFromOtherSource.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Hired), ApplicationStatusTypes.Hired.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewDenied), ApplicationStatusTypes.InterviewDenied.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewScheduled), ApplicationStatusTypes.InterviewScheduled.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.JobAlreadyFilled), ApplicationStatusTypes.JobAlreadyFilled.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NewApplicant), ApplicationStatusTypes.NewApplicant.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotHired), ApplicationStatusTypes.NotHired.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotQualified), ApplicationStatusTypes.NotQualified.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotYetPlaced), ApplicationStatusTypes.NotYetPlaced.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Recommended), ApplicationStatusTypes.Recommended.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise("ApplicationStatusTypes.RefusedOffer", "Refused job"), ApplicationStatusTypes.RefusedOffer.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.RefusedReferral), ApplicationStatusTypes.RefusedReferral.ToString()));
			statusDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.UnderConsideration), ApplicationStatusTypes.UnderConsideration.ToString()));

			statusDropDownList.SelectValue(status.ToString());
		}

		/// <summary>
		/// Gets the candidate count.
		/// </summary>
		/// <returns></returns>
		public int GetCandidateCount(AssistTalentPoolModel criteria)
		{
			return CandidateCount;
		}

		/// <summary>
		/// Gets the candidate count.
		/// </summary>
		/// <returns></returns>
		public int GetCandidateCount()
		{
			return CandidateCount;
		}

		#endregion

		#region Upload resume private methods

		/// <summary>
		/// Gets the resume search details.
		/// </summary>
		private void GetResumeSearchDetails()
		{
			if (ResumeFileUpload.HasFile)
			{
				ResumeContent = ResumeFileUpload.FileBytes;
				ResumeFilename = ResumeFileUpload.FileName;
				UploadedResumeLabel.Text = string.Format("{0} {1}", CodeLocalise("SampleResumeSearch.FileUploaded.Label", "Basing search on"), ResumeFilename);
			}
		}

		#endregion

		/// <summary>
		/// Formats the posted and expiry dates.
		/// </summary>
		/// <param name="candidate">The candidate.</param>
		/// <returns></returns>
		protected string FormatCommandArgument(ResumeView candidate)
		{
			var formattedCommandArgument = string.Format("{0},{1}", candidate.Id, candidate.Name);
			return formattedCommandArgument;
		}

		/// <summary>
		/// Handles the Click event of the CompareResumesLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CompareResumesLinkButton_Command(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "CompareResumes":
					var commandArguments = e.CommandArgument.ToString().Split(';');
					var candidatePlacementResumesDetails = new CompareResumesModal.Details
					{
						JobId = Model.JobId,
						JobTitle = commandArguments[0],
						RecentPlacementId = long.Parse(commandArguments[1]),
						RecentPlacementName = commandArguments[2],
						BusinessUnitName = commandArguments[3],
						CandidateName = commandArguments[4],
						CandidateId = long.Parse(commandArguments[5]),
						Score = int.Parse(commandArguments[6]),
						RecentlyPlacedMatchId = int.Parse(commandArguments[7])
					};

					CompareResumes.Show(candidatePlacementResumesDetails);
					break;

			}
		}

		/// <summary>
		/// Navigate the Referral Request page when the "View Referral Request" button is clicked
		/// </summary>
		/// <param name="sender">The button raising the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>

		protected void ViewReferralRequest_OnClick(object sender, EventArgs e)
		{
			var button = (Button)sender;
			var referralId = button.CommandArgument.AsLong();

			SetReturnUrl();

			Response.Redirect(UrlBuilder.JobSeekerReferral(referralId));
		}

		/// <summary>
		/// Clears the selected resume
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void ClearResumeButton_OnClick(object sender, EventArgs e)
		{
			ClearResume();
		}
	}
}
