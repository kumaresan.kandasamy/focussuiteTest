﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="list-test.aspx.cs" Inherits="Focus.Web.list_test" %>
<%@ Register tagName="UpdateableClientList" tagPrefix="focus" src="~/Code/Controls/User/UpdateableClientList.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
	<script src="<%# ResolveUrl("~/Assets/Scripts/jquery-ui-1.9.2.custom.new.min.js") %>" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="<%# ResolveUrl("~/Assets/Css/jquery-ui-1.9.2.custom.css") %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<%-- Uncomment to test in a partial postback scenario --%>
	<asp:UpdatePanel runat="server" ID="TestUpdatePanel">
		<ContentTemplate>
			<div style="float: left; border: 1px solid #999; padding: 15px; margin-right: 25px; width: 400px;">
				<h2>TextBox Mode</h2>
				<focus:UpdateableClientList ID="TextBoxClientList" runat="server" InputControlType="TextBox" />
			</div>
			<div style="float: left; border: 1px solid #999; padding: 15px; margin-right: 25px; width: 400px;">
				<h2>DropDownList Mode</h2>
				<focus:UpdateableClientList ID="DropdownClientList" runat="server" InputControlType="DropDownList" EnableDefaultSelection="true" />
			</div>
			<div style="float: left; border: 1px solid #999; padding: 15px; width: 400px;">
				<h2>AutoComplete Mode</h2>
				<focus:UpdateableClientList ID="AutoCompleteClientList" runat="server" InputControlType="AutoCompleteTextBox" AutoCompleteServiceMethod="GetStaffMemberNames" EnableDefaultSelection="true" />
			</div>
			<div style="clear: both; margin-top: 25px;">
				<asp:Button runat="server" ID="PostbackButton" OnClick="PostbackButton_OnClick" Text="Do a postback" CssClass="button1"/>
				<asp:Button runat="server" ID="ClearButton" OnClick="ClearButton_OnClick" Text="Clear the controls (serverside)" CssClass="button1"/>
				<asp:Button runat="server" ID="ReinitButton" OnClick="ReinitButton_OnClick" Text="Reinitialise (serverside)" CssClass="button1"/>
			</div>
			<asp:Panel ID="PostbackResultsPanel" Visible="False" style="margin-top: 25px;" runat="server">
				<p>Values posted back from TextBox list: <asp:Literal runat="server" ID="TextBoxPostbackList"></asp:Literal></p>
				<p>Values posted back from DropDownList list: <asp:Literal runat="server" ID="DropDownListPostbackList"></asp:Literal></p>
				<p>Values posted back from AutoComplete list: <asp:Literal runat="server" ID="AutoCompletePostbackList"></asp:Literal></p>
			</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
