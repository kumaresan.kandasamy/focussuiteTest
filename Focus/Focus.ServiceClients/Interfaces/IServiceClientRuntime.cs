﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core.Settings.Interfaces;
using Focus.Web.Core.Models;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface IServiceClientRuntime
	{
		/// <summary>
		/// Resets the data.
		/// </summary>
		void ResetData();

		/// <summary>
		/// Starts this instance.
		/// </summary>
		void Start();

		/// <summary>
		/// Shutdowns the App.
		/// </summary>
		void Shutdown();

		/// <summary>
		/// Begins the request.
		/// </summary>
		void BeginRequest();

		/// <summary>
		/// Ends the request.
		/// </summary>
		void EndRequest();

		/// <summary>
		/// Gets or sets the settings.
		/// </summary>
		/// <value>
		/// The settings.
		/// </value>
		IAppSettings Settings { get; set; }

		/// <summary>
		/// Starts session.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <returns></returns>
		/// <value>
		/// The session id.
		/// </value>
		Guid StartSession(AppContextModel context);

		/// <summary>
		/// Refreshes the settings.
		/// </summary>
		void RefreshSettings();

		/// <summary>
		/// Gets an account client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IAccountClient AccountClient(AppContextModel appContext);

		/// <summary>
		/// Gets an annotation client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IAnnotationClient AnnotationClient(AppContextModel appContext);

		/// <summary>
		/// Gets an authentication client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IAuthenticationClient AuthenticationClient(AppContextModel appContext);

		/// <summary>
		/// Gets a candidate client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		ICandidateClient CandidateClient(AppContextModel appContext);

		/// <summary>
		/// Gets a core client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		ICoreClient CoreClient(AppContextModel appContext);

		/// <summary>
		/// Gets an employee client.
		/// </summary>
		//// <param name="appContext">The context model.</param>
		IEmployeeClient EmployeeClient(AppContextModel appContext);

		/// <summary>
		/// Gets an employer client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IEmployerClient EmployerClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets an explorer client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IExplorerClient ExplorerClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets a job client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IJobClient JobClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets an occupation client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IOccupationClient OccupationClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets an organization client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IOrganizationClient OrganizationClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets a posting client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IPostingClient PostingClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets a processor client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IProcessorClient ProcessorClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets a report client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IReportClient ReportClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets a resume client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IResumeClient ResumeClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets a search client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		ISearchClient SearchClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets a staff client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IStaffClient StaffClient(AppContextModel appContext);
		
		/// <summary>
		/// Gets an utility client.
		/// </summary>
		/// <param name="appContext">The context model.</param>
		IUtilityClient UtilityClient(AppContextModel appContext);
	}
}
