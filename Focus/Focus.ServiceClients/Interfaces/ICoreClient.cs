﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.Activity;
using Focus.Core.Criteria.CaseManagement;
using Focus.Core.Criteria.Document;
using Focus.Core.Criteria.EducationInternship;
using Focus.Core.Criteria.NoteReminder;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.EmailTemplate;
using Focus.Core.Models.Assist;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Views;
using LocalisationDto = Focus.Core.DataTransferObjects.FocusCore.LocalisationDto;

using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.Interfaces
{
    public interface ICoreClient
    {
        /// <summary>
        /// Gets the version info.
        /// </summary>
        /// <returns></returns>
        string GetVersionInfo();

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <returns></returns>
        IAppSettings GetConfiguration();

        /// <summary>
        /// Gets the Configuration Items for an optional key 
        /// </summary>
        /// <param name="key">An optional key</param>
        /// <returns>A list of configuration items</returns>
        List<ConfigurationItemView> GetConfigurationItems(string key = "");

        /// <summary>
        /// Saves the configuration items.
        /// </summary>
        /// <param name="configurationItems">The configuration items.</param>
        bool SaveConfigurationItems(List<ConfigurationItemDto> configurationItems);

        /// <summary>
        /// Gets the localisation dictionary.
        /// </summary>
        /// <returns></returns>
        LocalisationDictionary GetLocalisationDictionary();

        /// <summary>
        /// Saves changes to localisation items.
        /// </summary>
        /// <param name="items">The localisation items to update</param>
        /// <param name="culture">An optional culture</param>
        /// <returns>The updated localisation dictionary</returns>
        LocalisationDictionary SaveLocalisationItems(List<LocalisationItemDto> items, string culture = Constants.DefaultCulture);

        /// <summary>
        /// Saves the default localisation items.
        /// </summary>
        /// <param name="items">The items.</param>
        void SaveDefaultLocalisationItems(List<KeyValuePairSerializable<string, string>> items);

        /// <summary>
        /// Gets the licenses and certificates.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        List<CertificateLicenseView> GetLicencesAndCertifications(string name, int listSize);

        /// <summary>
        /// Gets the licenses.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        List<CertificateLicenseView> GetLicenses(string name, int listSize);

        /// <summary>
        /// Gets the certificates.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        List<CertificateLicenseView> GetCertificates(string name, int listSize);

        /// <summary>
        /// Gets the languages.
        /// </summary>
        /// <param name="language">The language like.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        List<LookupItemView> GetLanguages(string language, int listSize);

        /// <summary>
        /// Gets the lookup.
        /// </summary>
        /// <param name="lookupType">Type of the lookup.</param>
        /// <param name="parentId">Parent id</param>
        /// <param name="key">The key.</param>
        /// <param name="prefix">An optional prefix</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        IList<LookupItemView> GetLookup(LookupTypes lookupType, long parentId = 0, string key = null, string prefix = null);

        /// <summary>
        /// Gets the generic job titles.
        /// </summary>
        /// <param name="jobTitle">The job title.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <returns></returns>
        List<string> GetGenericJobTitles(string jobTitle, int listSize);

        /// <summary>
        /// Gets the naics.
        /// </summary>
        /// <param name="naicsPrefix">Prefix of the naics.</param>
        /// <param name="listSize">Size of the list.</param>
        /// <param name="childrenOnly">Whether to get only the child NAICS</param>
        /// <returns></returns>
        List<NaicsView> GetNaics(string naicsPrefix, int listSize, bool childrenOnly = false);

        /// <summary>
        /// Gets the state and county for postal code.
        /// </summary>
        /// <param name="postalCode">The postal code.</param>
        /// <param name="stateLookupId">The state lookup id.</param>
        /// <param name="countyLookupId">The county lookup id.</param>
        /// <param name="cityName">The city name</param>
        void GetStateCityAndCountyForPostalCode(string postalCode, out long stateLookupId, out long countyLookupId, out string cityName);

        /// <summary>
        /// Establishes the location.
        /// </summary>
        /// <param name="postalCode">The postal code.</param>
        /// <param name="cityName">Name of the city.</param>
        /// <param name="stateName">Name of the state.</param>
        /// <returns></returns>
        PostalCodeViewDto EstablishLocation(string postalCode, string cityName, string stateName);

        /// <summary>
        /// Gets the email template preview.
        /// </summary>
        /// <param name="emailTemplateType">Type of the email template.</param>
        /// <param name="templateValues">The template values.</param>
        /// <returns></returns>
        EmailTemplateView GetEmailTemplatePreview(EmailTemplateTypes emailTemplateType, EmailTemplateData templateValues);

        /// <summary>
        /// Gets the sender email address for template.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        string GetSenderEmailAddressForTemplate(EmailTemplateTypes type);

        /// <summary>
        /// Gets the sender email address for template.
        /// </summary>
        /// <param name="template">The type.</param>
        /// <returns></returns>
        string GetSenderEmailAddressForTemplate(EmailTemplateDto template);

        /// <summary>
        /// Gets the email template.
        /// </summary>
        /// <param name="emailTemplateType">Type of the email template.</param>
        /// <returns></returns>
        EmailTemplateDto GetEmailTemplate(EmailTemplateTypes emailTemplateType);

        /// <summary>
        /// Gets the localisation item.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        LocalisationDictionaryEntry GetLocalisationItem(string key);

        /// <summary>
        /// Saves the email template.
        /// </summary>
        /// <param name="emailTemplate">The email template.</param>
        void SaveEmailTemplate(EmailTemplateDto emailTemplate);

        /// <summary>
        /// Gets the emails sent regarding referrals
        /// </summary>
        /// <param name="employeeId">The id of the employee.</param>
        /// <param name="applicationId">The id of the application.</param>
        /// <param name="approvalStatus">The approval status relating to the email</param>
        /// <param name="emailsRequired">The number of emails required.</param>
        /// <param name="jobId">the job posting id</param>
        /// <returns>A list of emails (body text only) sent</returns>
        List<ReferralEmailDto> GetReferralEmails(long? employeeId = null, long? applicationId = null, ApprovalStatuses? approvalStatus = null, int? emailsRequired = null, long? jobId = null);

        /// <summary>
        /// Saves an email regarding a referral.
        /// </summary>
        /// <param name="emailSubject">The email subject.</param>
        /// <param name="emailBody">The email body.</param>
        /// <param name="approvalStatus">The approval status (if relevant).</param>
        /// <param name="employeeId">The employee id.</param>
        /// <param name="applicationId">The application id.</param>
        /// <param name="JobId">the Job posting Id</param>
        /// <exception cref="ServiceCallException"></exception>
        void SaveReferralEmail(string emailSubject, string emailBody, ApprovalStatuses approvalStatus, long? employeeId = null, long? applicationId = null, long? JobId = null);

        /// <summary>
        /// Profiles the event.
        /// </summary>
        /// <param name="server">The server.</param>
        /// <param name="inTime">The in time.</param>
        /// <param name="outTime">The out time.</param>
        /// <param name="milliseconds">The milliseconds.</param>
        /// <param name="declaringType">Type of the declaring.</param>
        /// <param name="method">The method.</param>
        /// <param name="parameters">The parameters.</param>
        void ProfileEvent(string server, DateTime inTime, DateTime outTime, long milliseconds, string declaringType, string method, string parameters);

        /// <summary>
        /// Gets the messages.
        /// </summary>
        /// <param name="module">The module to get the messages for</param>
        /// <returns></returns>
        List<MessageView> GetMessages(FocusModules module);

        /// <summary>
        /// Gets recent messages.
        /// </summary>
        /// <param name="orderBy">The order of the messages</param>
        /// <returns></returns>
        List<MessageView> GetRecentMessages(string orderBy);

        /// <summary>
        /// Expires a messages.
        /// </summary>
        /// <param name="messageId">The id of the message to expire</param>
        /// <returns></returns>
        void ExpireMessage(long messageId);

        /// <summary>
        /// Dismisses the message.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        void DismissMessage(long messageId);

        /// <summary>
        /// Creates the homepage alert.
        /// </summary>
        /// <param name="homepageAlert">The homepage alert.</param>
        void CreateHomepageAlert(HomepageAlertView homepageAlert);

        /// <summary>
        /// Creates the homepage alert.
        /// </summary>
        /// <param name="homepageAlerts">The homepage alerts.</param>
        void CreateHomepageAlert(List<HomepageAlertView> homepageAlerts);

        /// <summary>
        /// Deletes the saved message.
        /// </summary>
        /// <param name="savedMessageId">The saved message id.</param>
        void DeleteSavedMessage(long savedMessageId);

        /// <summary>
        /// Gets the non default cultures.
        /// </summary>
        /// <returns></returns>
        List<LocalisationDto> GetNonDefaultCultures();

        /// <summary>
        /// Gets the default culture.
        /// </summary>
        /// <returns></returns>
        LocalisationDto GetDefaultCulture();

        /// <summary>
        /// Saves the message texts.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="messageTexts">The message texts.</param>
        void SaveMessageTexts(SavedMessageDto message, List<SavedMessageTextDto> messageTexts);

        /// <summary>
        /// Gets the saved messages.
        /// </summary>
        /// <param name="audience">The audience.</param>
        /// <returns></returns>
        List<SavedMessageDto> GetSavedMessages(MessageAudiences audience);

        /// <summary>
        /// Gets the saved message texts.
        /// </summary>
        /// <param name="savedMessageId">The saved message id.</param>
        /// <returns></returns>
        List<SavedMessageTextDto> GetSavedMessageTexts(long savedMessageId);

        /// <summary>
        /// Gets the application image.
        /// </summary>
        /// <param name="imageType">Type of the image.</param>
        /// <returns></returns>
        ApplicationImageDto GetApplicationImage(ApplicationImageTypes imageType);

        /// <summary>
        /// Saves the application image.
        /// </summary>
        /// <param name="applicationImage">The application image.</param>
        /// <returns></returns>
        void SaveApplicationImage(ApplicationImageDto applicationImage);

        /// <summary>
        /// Sends the feedback.
        /// </summary>
        /// <param name="subject">The subject.</param>
        /// <param name="detail">The detail.</param>
        void SendFeedback(string subject, string detail);

        /// <summary>
        /// Saves the note reminder.
        /// </summary>
        /// <param name="noteReminder">The note reminder.</param>
        /// <param name="recipients">Reminder recipients</param>
        NoteReminderDto SaveNoteReminder(NoteReminderDto noteReminder, List<NoteReminderRecipientDto> recipients);

        /// <summary>
        /// Gets the notes and reminders.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        PagedList<NoteReminderViewDto> GetNotesReminders(NoteReminderCriteria criteria);

        /// <summary>
        /// Gets the notes and reminders.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        List<NoteReminderViewDto> GetAllNotesReminders(NoteReminderCriteria criteria);

        /// <summary>
        /// Sends the reminder.
        /// </summary>
        /// <param name="noteReminderId">The note reminder id.</param>
        void SendReminder(long noteReminderId);

        /// <summary>
        /// Gets the industry classification lookup.
        /// </summary>
        /// <param name="parentId">The parent id.</param>
        /// <param name="codeLength">Length of the code.</param>
        /// <returns></returns>
        Dictionary<string, string> GetIndustryClassificationLookup(long? parentId, int? codeLength = null);

        /// <summary>
        /// Gets the industry classification.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        NAICSDto GetIndustryClassification(long id);

        /// <summary>
        /// Gets the industry classification.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <returns></returns>
        NAICSDto GetIndustryClassification(string code);

        /// <summary>
        /// Gets the action event.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        DateTime GetActionEventDate(long id);

        ///<summary>
        /// Gets the last action event date.
        /// </summary>
        /// <param name="id">The Personid.</param>
        /// <returns></returns>
        bool CheckLastAction(long? id);

        /// <summary>
        /// Back dates the Action event.
        /// </summary>
        /// <param name="id">The Id.</param>
        /// <param name="backToDate">The date to be backed to.</param>
        void BackdateActionEvent(long id, DateTime backToDate, string activityTypeId);

        /// <summary>
        /// Deletes the Action Event.
        /// </summary>
        /// <param name="id">The Id.</param>
        /// <returns></returns>
        bool DeleteActionEvent(long id, string activityTypeId);

        /// <summary>
        /// Gets the activity categories.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        List<ActivityCategoryDto> GetActivityCategories(ActivityType type);

        /// <summary>
        /// Saves changes to an activity category
        /// </summary>
        /// <param name="type">The type of the category (Employer or Job Seeker)</param>
        /// <param name="name">The name of the category</param>
        /// <param name="activityCategoryId">The Id if this is an existing category</param>
        /// <returns></returns>
        bool SaveActivityCategory(ActivityType type, string name, long? activityCategoryId = null);

        /// <summary>
        /// Deletes an activity category
        /// </summary>
        /// <param name="activityCategoryId">The Id of the category</param>
        /// <returns></returns>
        bool DeleteActivityCategory(long activityCategoryId);

        /// <summary>
        /// Gets the activities.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        PagedList<ActivityViewDto> GetActivitiesPaged(ActivityCriteria criteria);

        /// <summary>
        /// Gets the activities look up.
        /// </summary>
        /// <param name="categoryId">The category id.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        Dictionary<long, string> GetActivitiesLookUp(long categoryId);

        /// <summary>
        /// Gets all activities.
        /// </summary>
        /// <returns></returns>
        ActivityViewDto[] GetAllActivities();

        /// <summary>
        /// Gets the education internship categories.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        List<EducationInternshipCategoryDto> GetEducationInternshipCategories();

        /// <summary>
        /// Gets the education internship categories.
        /// </summary>
        /// <param name="includeSkills">Specifies whether to include each category's skills.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        List<EducationInternshipCategoryDto> GetEducationInternshipCategories(bool includeSkills);

        /// <summary>
        /// Gets the education internship statements.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        List<EducationInternshipStatementDto> GetEducationInternshipStatements(EducationInternshipStatementCriteria criteria);

        /// <summary>
        /// Gets the activity.
        /// </summary>
        /// <param name="activityId">The activity id.</param>
        /// <returns></returns>
        ActivityViewDto GetActivity(long activityId);

        /// <summary>
        /// Saves the activities.
        /// </summary>
        /// <param name="activities">The activities.</param>
        /// <exception cref="ServiceCallException"></exception>
        string SaveActivities(List<ActivityViewDto> activities);

        /// <summary>
        /// Saves changes to an activity
        /// </summary>
        /// <param name="activityCategoryId">The id of the category</param>
        /// <param name="name">The name of the activity</param>
        /// <param name="externalId">The external id.</param>
        /// <param name="visible">Whether the activity is visible.</param>
        /// <param name="activityId">The Id if this is an existing category.</param>
        /// <returns></returns>
        bool SaveActivity(long activityCategoryId, string name, long externalId, bool visible, long? activityId = null);

        /// <summary>
        /// Saves an activity
        /// </summary>
        /// <param name="activityId">The Id of the activity</param>
        /// <returns></returns>
        bool DeleteActivity(long activityId);

        /// <summary>
        /// Saves the self service activity.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="actingOnBehalfOfUserId">The userId of the person who the action is being carried out on.</param>
        /// <param name="actingOnBehalfOfPersonId">The person of the person who the action is being carried out on.</param>
        /// <exception cref="ServiceCallException"></exception>
        void SaveSelfService(ActionTypes action, long? actingOnBehalfOfUserId = null, long? actingOnBehalfOfPersonId = null);

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="isHtml">if set to <c>true</c> [is HTML].</param>
        /// <param name="attachment">The attachment.</param>
        /// <param name="attachmentName">Name of the attachment.</param>
        /// <param name="detectUrl">Whether to detect URLs in the body and convert to hyperlinks</param>
        /// <exception cref="ServiceCallException"></exception>
        void SendEmail(string to, string subject, string body, bool isHtml = false, byte[] attachment = null, string attachmentName = null, bool detectUrl = false, DateTime? sendByFutureDate = null);

        void SendEmailFromTemplate(string to, EmailTemplateTypes type, EmailTemplateData data, bool isHtml = false, byte[] attachment = null, string attachmentName = null, bool detectUrl = false, string cc = null, string bcc = null, bool sendAsync = false);

        /// <summary>
        /// Gets the job name from R onet.
        /// </summary>
        /// <param name="roNet">The ro net.</param>
        /// <returns></returns>
        /// <exception cref="ServiceCallException"></exception>
        string GetJobNameFromROnet(string roNet);

        string GetCareerAreaName(long careerAreaId);
        T GetSessionValue<T>(string key, T defaultValue = default(T), IEnumerable<Type> knownTypes = null);
        T SetSessionValue<T>(string key, T value, IEnumerable<Type> knownTypes = null);
        void RemoveSessionValue(string key);
        void ClearSession();

        #region Upload File Services

        /// <summary>
        /// Gets the validation schema
        /// </summary>
        /// <param name="schemaType">The schema type</param>
        /// <returns>The response containing the csv template</returns>
        XDocument GetValidationSchema(ValidationSchema schemaType);

        /// <summary>
        /// Gets the csv template for a specific schema type
        /// </summary>
        /// <param name="schemaType">The schema type</param>
        /// <returns>The response containing the csv template</returns>
        string GetCSVTemplateForSchema(ValidationSchema schemaType);

        /// <summary>
        /// Validates for a file against a specific schema type
        /// </summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="schemaType">The schema type</param>
        /// <param name="fileBytes">The file bytes.</param>
        /// <param name="fileType">The type of the file.</param>
        /// <param name="customInfo">Information specific to the upload.</param>
        /// <param name="ignoreHeader">Whether to ignore the header record.</param>
        /// <returns>The upload file id</returns>
        long ValidateUploadFile(string fileName, ValidationSchema schemaType, byte[] fileBytes, UploadFileType fileType, Dictionary<string, string> customInfo, bool ignoreHeader);

        /// <summary>
        /// Gets the processing of an upload file
        /// </summary>
        /// <param name="uploadFileId">The file id</param>
        /// <returns>The processing status of the file</returns>
        UploadFileProcessingState GetUploadFileProcessingState(long uploadFileId);

        /// <summary>
        /// Gets the details of an upload file
        /// </summary>
        /// <param name="uploadFileId">The file id</param>
        /// <param name="exceptionsOnly">Whether to only return rows in exception</param>
        /// <param name="basicDataOnly">Whether to only return basic data only</param>
        /// <returns>
        /// The validation status of the file
        /// </returns>
        /// <exception cref="ServiceCallException"></exception>
        List<UploadRecordDto> GetUploadFileDetails(long uploadFileId, bool exceptionsOnly, bool basicDataOnly = false);

        /// <summary>
        /// Commits the upload file after validation
        /// </summary>
        /// <param name="uploadFileId">The file id</param>
        void CommitUploadFile(long uploadFileId);

        #endregion

        #region Integration

        /// <summary>
        /// Gets the case management records.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns>A paged list of records</returns>
        PagedList<FailedIntegrationMessageDto> GetCaseManagementErrorsPagedList(CaseManagementCriteria criteria);

        /// <summary>
        /// Gets the case management error.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The record with the specified identifier</returns>
        FailedIntegrationMessageDto GetCaseManagementError(long id);

        /// <summary>
        /// Ignores a case manangement record
        /// </summary>
        /// <param name="id">The id of the record to ignore</param>
        /// <returns>A boolean indicating success or failure</returns>
        bool IgnoreCaseManagementMessage(long id);

        /// <summary>
        /// Resends a case manangement record
        /// </summary>
        /// <param name="id">The id of the record to ignore</param>
        /// <returns>A boolean indicating success or failure</returns>
        bool ResendCaseManagementMessage(long id);

        List<IntegrationRequestResponseMessagesDto> GetIntegrationRequestResponses(long caseManagementErrorId);

        #endregion

        #region Documents

        List<DocumentDto> GetDocuments(DocumentCriteria criteria);

        List<GroupedDocumentsModel> GetGroupedDocuments(DocumentCriteria criteria);

        List<ErrorTypes> UploadDocument(DocumentDto document);

        DocumentDto GetDocument(long id);

        void DeleteDocument(long id);

        #endregion
    }
}
