﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core.Models.Career;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.Interfaces
{
	public interface IOrganizationClient
	{
		/// <summary>
		/// Sets the do not display.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool SetDoNotDisplay(string lensPostingId);

    /// <summary>
    /// Applies for job.
    /// </summary>
    /// <param name="lensPostingId">The lens posting id.</param>
    /// <param name="resumeId">The resume id.</param>
    /// <param name="isEligible">if set to <c>true</c> [is eligible].</param>
    /// <param name="score">The score.</param>
    /// <param name="message">The message.</param>
    /// <param name="reviewApplication">if set to <c>true</c> [review application].</param>
    /// <param name="waivedRequirements">The waived requirements.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
		bool ApplyForJob(string lensPostingId, long resumeId, bool isEligible, int score = 0, string message = "", bool reviewApplication = false, List<string> waivedRequirements = null);

		/// <summary>
		/// Gets the application list.
		/// </summary>
		/// <param name="applicationCount">The application count.</param>
		/// <param name="fromDate">From date.</param>
		/// <param name="toDate">To date.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<ReferralPosting> GetApplicationList(int applicationCount, DateTime fromDate, DateTime toDate);

		/// <summary>
		/// Gets the match score.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		int GetMatchScore(long resumeId, string lensPostingId);

		/// <summary>
		/// Gets the job information.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PostingEnvelope GetJobInformation(string jobId);
	}
}
