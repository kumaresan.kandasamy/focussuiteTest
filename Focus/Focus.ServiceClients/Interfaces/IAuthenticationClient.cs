﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using BgtSSO;
using Focus.Core;
using Focus.Core.Messages.AuthenticationService;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion


namespace Focus.ServiceClients.Interfaces
{
	public interface IAuthenticationClient
	{
		/// <summary>
		/// Validates the user's Session 
		/// </summary>
		ErrorTypes ValidateSession();

		/// <summary>
		/// StartSession must be the first call to the service for each user. 
		/// This is irrespective of whether user is logging in or not.
		/// </summary>
		/// <returns>Unique security token that is valid for the duration of the session.</returns>
		Guid StartSession();

		/// <summary>
		/// Logs in the specified user to career/explorer
		/// </summary>
		/// <param name="userName">The userName.</param>
		/// <param name="password">The password.</param>
		/// <param name="integrationUser">The module to login to.</param>
		/// <param name="validationUrl">The validation URL.</param>
		/// <returns></returns>
		LoggedInUserModel LogInToCareer(string userName, string password, string validationUrl, out ValidatedUserView integrationUser);

		/// <summary>
		/// Logs in the specified user.
		/// </summary>
		/// <param name="userName">The userName.</param>
		/// <param name="password">The password.</param>
		/// <param name="moduleToLoginTo">The module to login to.</param>
		/// <param name="validationUrl">The validation URL.</param>
		/// <param name="allowResetSessionId">If set to true, the session id can be reset if it is invalid.</param>
		/// <returns></returns>
		LoggedInUserModel LogIn(string userName, string password, FocusModules moduleToLoginTo, string validationUrl = "", bool allowResetSessionId = false);

		LoggedInUserModel LogIn(ISSOProfile profile, FocusModules moduleToLoginTo);

		/// <summary>
		/// Gets the App.UserData data containing career data on resumes and skills
		/// </summary>
		UserDataModel GetCareerUserData();

		/// <summary>
		/// Logs the out.
		/// </summary>
		void LogOut();
		
		/// <summary>
		/// Processes the forgotten password.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="changePasswordUrl">The change password URL.</param>
		/// <param name="module">The Focus module in use</param>
		/// <param name="monthOfBirth"></param>
		/// <param name="pinRegistrationUrl">The pin registration URL (for Education, in case of user not yet being registered)</param>
		/// <param name="securityQuestion"></param>
		/// <param name="securityQuestionId"></param>
		/// <param name="securityAnswer"></param>
		/// <param name="dayOfBirth"></param>
		/// <exception cref="ServiceCallException"></exception>
		int? ProcessForgottenPassword(string userName, string emailAddress, string changePasswordUrl, FocusModules module, string securityQuestion = null, long? securityQuestionId = null, string securityAnswer = null, int dayOfBirth = 0, int monthOfBirth = 0, string pinRegistrationUrl = "");

		/// <summary>
		/// Creates the employee single sign on.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="userId"> </param>
		/// <returns></returns>
		Guid CreateEmployeeSingleSignOn(long employeeId, long? userId);

		/// <summary>
		/// Creates the employee single sign on.
		/// </summary>
		/// <param name="userName">Name of the user.</param>
		/// <returns></returns>
		Guid CreateEmployeeSingleSignOn(string userName);

		/// <summary>
		/// Creates the candidate single sign on.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		Guid CreateCandidateSingleSignOn(long personId, long? userId);

		/// <summary>
		/// Validates whether (encrypted) user details exist in an external system
		/// </summary>
		/// <param name="authenticationDetails">A dictionary of name/value pairs needed for validation</param>
		/// <returns>Details of the external user</returns>
		ValidatedUserView ValidateExternalAuthentication(Dictionary<string, string> authenticationDetails);

		/// <summary>
		/// Validates whether user details exist in an external system
		/// </summary>
		/// <param name="socialSecurityNumber">The users Social Security number</param>
		/// <param name="checkFocus">Whether to check for existence in Focus first</param>
		/// <returns>
		/// Details of the external user
		/// </returns>
		ValidatedUserView ValidateExternalAuthenticationBySsn(string socialSecurityNumber, bool checkFocus = true);

		/// <summary>
		/// Validates whether user details exist in an external system
		/// </summary>
		/// <param name="externalId">The user's External Id</param>
		/// <param name="checkFocus">Whether to check for existence in Focus first</param>
		/// <returns>Details of the external user</returns>
		ValidatedUserView ValidateExternalAuthenticationByExternalId(string externalId, bool checkFocus = true);

		/// <summary>
		/// Validates the single sign on.
		/// </summary>
		/// <param name="singleSignOnKey">The single sign on key.</param>
		/// <returns></returns>
		LoggedInUserModel ValidateSingleSignOn(Guid singleSignOnKey);

		/// <summary>
		/// Validates the password reset validation key.
		/// </summary>
		/// <param name="validationKey">The validation key.</param>
		/// <param name="error">The error (should validation fail)</param>
		/// <returns></returns>
		long? ValidatePasswordResetValidationKey(string validationKey, out ErrorTypes error);

		/// <summary>
		/// Validates the pin registration details.
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		bool ValidatePinRegistration(PinRegistrationModel model);

		/// <summary>
		/// Gets the security questions.
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="module">The module.</param>
		/// <returns></returns>
		SecurityQuestionResponse GetSecurityQuestions(string emailAddress, string userName, FocusModules module);

		/// <summary>
		/// Processes the security questions.
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="securityAnswer1">The security answer1.</param>
		/// <param name="securityQuestionIndex1"></param>
		/// <param name="securityAnswer2">The security answer2.</param>
		/// <param name="securityQuestionIndex2"></param>
		/// <param name="module">The module.</param>
		/// <param name="resetPasswordUrl"></param>
		/// <returns></returns>
		int? ProcessSecurityQuestions(string emailAddress, string userName,
																	int securityQuestionIndex1, string securityAnswer1,
																	int securityQuestionIndex2, string securityAnswer2,
																	FocusModules module, string resetPasswordUrl);
	}
}
