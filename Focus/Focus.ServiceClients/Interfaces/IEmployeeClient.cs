﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employee;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface IEmployeeClient
	{
		/// <summary>
		/// Gets the employee.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		EmployeeDto GetEmployee(long employeeId);

		/// <summary>
		/// Gets the employee by person.
		/// </summary>
		/// <param name="personId">The employee id.</param>
		/// <returns></returns>
		EmployeeDto GetEmployeeByPerson(long personId);

		/// <summary>
		/// Gets the other employees.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		List<long> GetSameEmployerEmployees(long employeeId);

		/// <summary>
		/// Gets the hiring areas.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		List<string> GetHiringAreas(long employeeId);

		/// <summary>
		/// Gets all employees for a list of employer ids
		/// </summary>
		/// <param name="employerIds">The list of employer ids</param>
		/// <param name="pageIndex">The index of the page required.</param>
		/// <param name="pageSize">The size of each page.</param>
		/// <returns>A list of employees</returns>
		PagedList<EmployeeSearchResultView> GetEmployeesForEmployers(List<long> employerIds, int pageIndex, int pageSize);

		/// <summary>
		/// Gets all employees for a list of businuess unit ids
		/// </summary>
		/// <param name="businessUnitIds">The list of business units ids</param>
		/// <param name="pageIndex">The index of the page required.</param>
		/// <param name="pageSize">The size of each page.</param>
		/// <param name="subscribedOnly">if set to <c>true</c> [subscribed only].</param>
		/// <returns>
		/// A list of employees
		/// </returns>
		PagedList<EmployeeSearchResultView> GetEmployeesForBusinessUnits(List<long> businessUnitIds, int pageIndex, int pageSize, bool subscribedOnly = false);

		/// <summary>
		/// Gets all employees for a list of businuess unit ids
		/// </summary>
		/// <param name="businessUnitIds">The list of business units ids</param>
		/// <returns>A list of employees</returns>
		List<EmployeeSearchResultView> GetEmployeesForBusinessUnits(List<long> businessUnitIds);

		/// <summary>
		/// Gets count of all employees for a list of businuess unit ids
		/// </summary>
		/// <param name="businessUnitIds">The list of business units ids</param>
		/// <returns>A count of employees</returns>
		long GetEmployeeCountForBusinessUnits(List<long> businessUnitIds);

		/// <summary>
		/// Searches the employees.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<EmployeeSearchResultView> SearchEmployees(EmployeeCriteria criteria);

		/// <summary>
		/// Emails the employee.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		/// <param name="bccSelf">if set to <c>true</c> [BCC self].</param>
		/// <param name="senderAddress">Sender address</param>
		/// <param name="sendCopy">if set to <c>true</c> [send copy].</param>
		/// <exception cref="ServiceCallException"></exception>
		void EmailEmployee(long employeeId, string emailSubject, string emailBody, bool bccSelf = false, string senderAddress = null, bool sendCopy = false, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null);

        /// <summary>
        /// Emails the employee with an attachment
        /// </summary>
        /// <param name="employeeId">The employee id.</param>
        /// <param name="emailSubject">The email subject.</param>
        /// <param name="emailBody">The email body.</param>
        /// <param name="attachment">The attachment data</param>
        /// <param name="attachmentName">The name of the attachment</param>
        /// <param name="bccSelf">if set to <c>true</c> [BCC self].</param>
        void EmailEmployees(List<long> employeeIds, string emailSubject, string emailBody, bool bccSelf = false, string senderAddress = null, bool sendCopy = false, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null);

		/// <summary>
		/// Emails the employee with an attachment
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		/// <param name="attachment">The attachment data</param>
		/// <param name="attachmentName">The name of the attachment</param>
		/// <param name="bccSelf">if set to <c>true</c> [BCC self].</param>
		void EmailEmployeeWithAttachment(long employeeId, string emailSubject, string emailBody, byte[] attachment, string attachmentName, bool bccSelf = false);

		/// <summary>
		/// Gets the employer account referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<EmployerAccountReferralViewDto> GetEmployerAccountReferrals(EmployerAccountReferralCriteria criteria);

		/// <summary>
		/// Gets the employer account referral.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		EmployerAccountReferralViewDto GetEmployerAccountReferral(long employeeId);

		/// <summary>
		/// Approves the employer account referral.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessunitId"></param>
		/// <param name="lockVersion">The lock version.</param>
		/// <param name="employerApprovalType">employer approval type</param>
		ApproveReferralResponseType ApproveEmployerAccountReferral(long employeeId, long businessunitId, string lockVersion, ApprovalType employerApprovalType = ApprovalType.Employer);

		/// <summary>
		/// Denies the employer account referral.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessunitId">The businessunit id.</param>
		/// <param name="lockVersion">The lock version.</param>
		/// <param name="employerApprovalType">Type of the employer approval.</param>
		/// <param name="approvalDeniedReasons">The approval denied reasons.</param>
		void DenyEmployerAccountReferral(long employeeId, long businessunitId, string lockVersion, ApprovalType employerApprovalType = ApprovalType.Employer, List<KeyValuePair<long, string>> approvalDeniedReasons = null);

		/// <summary>
		/// Holds the employer account referral.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessunitId">The Business Id</param>
		/// <param name="lockVersion">The Lock Version</param>
		/// <param name="employerApprovalType">Approval Type (Employer, Business Unit or Hiring Manager)</param>
		/// <param name="approvalHoldReasons">List of hold reasons</param>
		void HoldEmployerAccountReferral( long employeeId, long businessunitId, string lockVersion, ApprovalType employerApprovalType = ApprovalType.Employer, List<KeyValuePair<long, string>> approvalHoldReasons = null );

		/// <summary>
		/// Assigns the activity.
		/// </summary>
		/// <param name="activityId">The activity id.</param>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="actionedDate">The actioned date.</param>
		bool AssignActivity(long activityId, long employeeId,DateTime? actionedDate = null);

		/// <summary>
		/// Assigns the activity to multiple employees.
		/// </summary>
		/// <param name="activityId">The activity id.</param>
		/// <param name="employeeIds">The employee ids.</param>
		/// <param name="actionedDate">the back date if specified</param>
		/// <exception cref="ServiceCallException"></exception>
		bool AssignActivityToMultipleEmployees(long activityId, List<long> employeeIds,DateTime? actionedDate = null);

		/// <summary>
		/// Blocks the account.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		void BlockAccount(long employeeId);

		/// <summary>
		/// Unblocks the account.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		void UnblockAccount(long employeeId);

		/// <summary>
		/// Gets the user details.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		UserDetailsModel GetUserDetails(long employeeId);

		/// <summary>
		/// Gets the employees business units.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		List<EmployeeBusinessUnitView> GetEmployeesBusinessUnits(long employeeId);

		/// <summary>
		/// Assigns the employee to business unit.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		void AssignEmployeeToBusinessUnit(long employeeId, long businessUnitId);

		/// <summary>
		/// Sets the business unit as default.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <param name="businessUnitId">The business unit id.</param>
		void SetBusinessUnitAsDefault(long employeeId, long businessUnitId);

		/// <summary>
		/// Gets the hiring manager profile.
		/// </summary>
		/// <param name="employeebusinessunitId">The employeebusinessunit id.</param>
		/// <returns></returns>
		HiringManagerProfileModel GetHiringManagerProfile(long employeebusinessunitId);

		/// <summary>
		/// Gets the hiring manager activities.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<HiringManagerActivityViewDto> GetHiringManagerActivities(EmployeeBusinessUnitCriteria criteria);

		/// <summary>
		/// Gets the employee business unit activity users.
		/// </summary>
		/// <param name="employeeBuId">The bu id.</param>
		/// <returns></returns>
		List<HiringManagerActivityViewDto> GetEmployeeBusinessUnitActivityUsers(long employeeBuId);

		/// <summary>
		/// Creates the homepage alert.
		/// </summary>
		/// <param name="homepageAlert">The homepage alert.</param>
		void CreateHomepageAlert(HiringManagerHomepageAlertView homepageAlert);
	}
}
