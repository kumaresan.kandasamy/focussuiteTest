﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models.Career;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.Interfaces
{
	public interface IOccupationClient
	{
		/// <summary>
		/// Gets the resume builder questions.
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<Question> ResumeBuilderQuestions(string onetCode);

		/// <summary>
		/// Gets the statements
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <param name="tense">The tense of the statement i.e. past or present.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<string> Statements(string onetCode, StatementTense tense = StatementTense.Present);

		/// <summary>
		/// Gets the onet skills
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<string> OnetSkills(string onetCode);

		/// <summary>
		/// MOCs the job titles.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="socCount">The soc count.</param>
		/// <returns></returns>
		List<OnetCode> MOCJobTitles(string jobTitle, int socCount);

		/// <summary>
		/// MOCs for a given occupation
		/// </summary>
		/// <param name="militaryOccupationId">The soc count.</param>
		/// <returns></returns>
		List<OnetCode> GetMocOccupationCodes(long militaryOccupationId);

		List<OnetCode> GenericJobTitles(string jobTitle, int socCount);

		/// <summary>
		/// Gets the military occupation job titles with a specific prefix
		/// </summary>
		/// <param name="jobTitlePrefix">The job title on which to look for a match as a prefix.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns>A list of military job titles</returns>
		List<MilitaryOccupationJobTitleViewDto> GetMilitaryOccupationJobTitles(string jobTitlePrefix, int listSize);

		/// <summary>
		/// Gets the military occupation job titles.
		/// </summary>
		/// <param name="jobTitle">The job title on which to look for a match.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <param name="fullTitleSearch">If set to true look for text anywhere in title. If false, check the prefix ony</param>
		/// <returns>A list of military job titles</returns>
		List<MilitaryOccupationJobTitleViewDto> GetMilitaryOccupationJobTitles(string jobTitle, int listSize, bool fullTitleSearch);

		/// <summary>
		/// Gets the job title for a specific military occupation
		/// </summary>
		/// <param name="militaryOccupationId">The Id of the military occupation</param>
		/// <returns>The job title for the military occupation</returns>
		MilitaryOccupationJobTitleViewDto GetMilitaryOccupationJobTitle(long militaryOccupationId);

		/// <summary>
		/// Converts the onet to r onet.
		/// </summary>
		/// <param name="onet">The onet.</param>
		/// <returns></returns>
		OnetToROnetConversionViewDto ConvertOnetToROnet(string onet);
	}
}
