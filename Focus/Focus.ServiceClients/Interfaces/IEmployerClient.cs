﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria.BusinessUnit;
using Focus.Core.Criteria.Employer;
using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Focus.Core.Models.Assist;
using Focus.Core.Views;
using Focus.Web.Core.Models;

using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.Interfaces
{
	public interface IEmployerClient
	{
		/// <summary>
		/// Gets the employer jobs.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <param name="jobTypes">An optional list of job types</param>
		/// <returns></returns>
		List<SelectView> GetEmployerJobs(long employerId, List<JobTypes> jobTypes = null);

		/// <summary>
		/// Gets the business units.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		List<BusinessUnitDto> GetBusinessUnits(long employerId);


    /// <summary>
    /// Gets the business units.
    /// </summary>
    /// <param name="employerId">The employer id.</param>
    /// <param name="employeeIds">A lookup of employee ids.</param>
    /// <returns></returns>
    List<BusinessUnitDto> GetBusinessUnitsWithEmployeeIds(long employerId, out ILookup<long, long> employeeIds);

		/// <summary>
		/// Gets the business unit.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		BusinessUnitDto GetBusinessUnit(long businessUnitId);

		/// <summary>
		/// Gets the preferred business unit for an employer.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		BusinessUnitDto GetPreferredBusinessUnit(long employerId);

		/// <summary>
		/// Gets the business unit profile.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		BusinessUnitProfileModel GetBusinessUnitProfile(long businessUnitId);

		/// <summary>
		/// Gets the linked companies for a business unit.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		List<BusinessUnitLinkedCompanyDto> GetBusinessUnitLinkedCompanies(BusinessUnitLinkedCompanyCriteria criteria);

		/// <summary>
		/// Saves the business unit.
		/// </summary>
		/// <param name="businessUnit">The business unit.</param>
		/// <returns></returns>
		BusinessUnitDto SaveBusinessUnit(BusinessUnitDto businessUnit);

    /// <summary>
    /// Saves the business unit model.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <param name="checkCensorshipBusinessUnit">Whether to try and check censorship main business unit record</param>
    /// <param name="checkCensorshipDescription">Whether to try and check censorship business unit description</param>
    /// <param name="failedCensorshipCheck">Whether censorship check failed</param>
    /// <returns></returns>
    BusinessUnitModel SaveBusinessUnitModel(BusinessUnitModel model, bool checkCensorshipBusinessUnit, bool checkCensorshipDescription, out bool failedCensorshipCheck);

		/// <summary>
		/// Gets the business unit description.
		/// </summary>
		/// <param name="businessUnitDescriptionId">The business unit description id.</param>
		/// <returns></returns>
		BusinessUnitDescriptionDto GetBusinessUnitDescription(long businessUnitDescriptionId);

		/// <summary>
		/// Gets the primary business unit description.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		BusinessUnitDescriptionDto GetPrimaryBusinessUnitDescription(long businessUnitId);

		/// <summary>
		/// Saves the business unit description.
		/// </summary>
		/// <param name="description">The description.</param>
		/// <returns></returns>
		BusinessUnitDescriptionDto SaveBusinessUnitDescription(BusinessUnitDescriptionDto description);

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		ApprovalStatuses GetEmployerApprovalStatus(long employerId);

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		EmployerDetailsModel GetEmployer(long employerId);

		/// <summary>
		/// Gets the employer's legal name.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		string GetEmployerLegalName(long employerId);

		/// <summary>
		/// Updates the company details.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns>User context for updating</returns>
		IUserContext SaveEmployerDetails(EmployerDetailsModel model);

		/// <summary>
		/// Gets the employer placements.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<EmployerPlacementsView> GetEmployerPlacements(PlacementsCriteria criteria);

		/// <summary>
		/// Gets the business unit descriptions.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		List<BusinessUnitDescriptionDto> GetBusinessUnitDescriptions(long businessUnitId);

		/// <summary>
		/// Gets the business unit descriptions associated with job.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		List<BusinessUnitDescriptionDto> GetBusinessUnitDescriptionsAssociatedWithJob(long businessUnitId);

		/// <summary>
		/// Saves the business unit descriptions.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="descriptions">The descriptions.</param>
    /// <param name="checkCensorship">Whether to try and check censorship business unit description</param>
    bool SaveBusinessUnitDescriptions(long businessUnitId, List<BusinessUnitDescriptionDto> descriptions, bool checkCensorship);

		/// <summary>
		/// Gets the business unit logo.
		/// </summary>
		/// <param name="businessUnitLogoId">The business unit logo id.</param>
		/// <returns></returns>
		BusinessUnitLogoDto GetBusinessUnitLogo(long businessUnitLogoId);

		/// <summary>
		/// Saves the business unit logo.
		/// </summary>
		/// <param name="businessUnitLogo">The business unit logo.</param>
		/// <returns></returns>
		long SaveBusinessUnitLogo(BusinessUnitLogoDto businessUnitLogo);

		/// <summary>
		/// Gets the business unit logos.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		List<BusinessUnitLogoDto> GetBusinessUnitLogos(long businessUnitId);

		/// <summary>
		/// Gets the business unit logos associated with job.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		List<BusinessUnitLogoDto> GetBusinessUnitLogosAssociatedWithJob(long businessUnitId);

		/// <summary>
		/// Saves the business unit logos.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <param name="logos">The logos.</param>
		List<BusinessUnitLogoDto> SaveBusinessUnitLogos(long businessUnitId, List<BusinessUnitLogoDto> logos);

		/// <summary>
		/// Gets the primary employer address.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		EmployerAddressDto GetPrimaryEmployerAddress(long employerId);

		/// <summary>
		/// Gets the primary business unit address.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <returns></returns>
		BusinessUnitAddressDto GetPrimaryBusinessUnitAddress(long businessUnitId);

		/// <summary>
		/// Saves the business unit address.
		/// </summary>
		/// <param name="businessUnitAddress">The business Unit Address.</param>
		/// <returns></returns>
		BusinessUnitAddressDto SaveBusinessUnitAddress(BusinessUnitAddressDto businessUnitAddress);

		/// <summary>
		/// Gets the job titles.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		List<string> GetJobTitles(long employerId, string jobTitle, int count);

		/// <summary>
		/// Assigns the employer to admin. REFERS TO HIRING MANAGER (EmployeeId)
		/// </summary>
		/// <param name="adminId">The admin id.</param>
		/// <param name="employeeId">The employee id.</param>
		bool AssignEmployerToAdmin(long adminId, long employeeId);

		/// <summary>
		/// Checks the NAICS exists.
		/// </summary>
		/// <param name="naics">NAICs code.</param>
    /// <param name="childrenOnly">Whether to check "child" NAICS only (i.e. exluce NAICS where ParentId = 0)</param>
    /// <returns></returns>
    bool CheckNAICSExists(string naics, bool childrenOnly = false);

		/// <summary>
		/// Blocks the employees accounts.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void BlockEmployeesAccounts(long businessUnitId);

		/// <summary>
		/// Unblocks the employees accounts.
		/// </summary>
		/// <param name="businessUnitId">The business unit id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void UnblockEmployeesAccounts(long businessUnitId);

		/// <summary>
		/// Saves the business unit model.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		BusinessUnitModel SaveBusinessUnitModel(BusinessUnitModel model);

		/// <summary>
		/// Gets the business unit activities as list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		List<BusinessUnitActivityViewDto> GetBusinessUnitActivitiesAsList(BusinessUnitCriteria criteria);

		/// <summary>
		/// Gets the business unit activities as paged.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<BusinessUnitActivityViewDto> GetBusinessUnitActivitiesAsPaged(BusinessUnitCriteria criteria);

		/// <summary>
		/// Gets the business unit placements.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<BusinessUnitPlacementsViewDto> GetBusinessUnitPlacements(BusinessUnitCriteria criteria);

		/// <summary>
		/// Gets the business unit placements.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<EmployerRecentlyPlacedView> GetEmployerRecentlyPlacedMatches(BusinessUnitCriteria criteria);

		/// <summary>
		/// Ignores the recommended match.
		/// </summary>
		/// <param name="recentlyPlacedMatchId">The recently placed match id.</param>
		void IgnoreRecentlyPlacedMatch(long recentlyPlacedMatchId);

		/// <summary>
		/// Allows the recommended match.
		/// </summary>
		/// <param name="recentlyPlacedMatchId">The recently placed match id.</param>
		void AllowRecentlyPlacedMatch(long recentlyPlacedMatchId);

		/// <summary>
		/// Searches the employers.
		/// </summary>
		/// <param name="postCode">The post code.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<BusinessUnitSearchViewDto> SearchBusinessUnits(string postCode);

		/// <summary>
		/// Gets the office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		OfficeDto GetOffice(long officeId);

    /// <summary>
    /// Gets the default office.
    /// </summary>
    /// <param name="defaultType">The default type.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    OfficeDto GetDefaultOffice(OfficeDefaultType defaultType);

    /// <summary>
    /// Gets default offices.
    /// </summary>
    /// <param name="defaultType">The default type.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    List<OfficeDto> GetDefaultOffices(OfficeDefaultType defaultTypes);

		/// <summary>
		/// Gets the offices.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<OfficeDto> GetOfficesPagedList(OfficeCriteria criteria);

		/// <summary>
		/// Gets the offices list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<OfficeDto> GetOfficesList(OfficeCriteria criteria);

		/// <summary>
		/// Gets the offices excluding default list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<OfficeDto> GetOfficesExcludingDefaultList(OfficeCriteria criteria);

		/// <summary>
		/// Saves the office.
		/// </summary>
		/// <param name="office">The office.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		ErrorTypes SaveOffice(OfficeDto office);

		/// <summary>
		/// Gets the office staff members paged list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<PersonModel> GetOfficeStaffMembersPagedList(StaffMemberCriteria criteria);

		/// <summary>
		/// Gets the office staff members list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<PersonModel> GetOfficeStaffMembersList(StaffMemberCriteria criteria);

		/// <summary>
		/// Gets the office managers.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<PersonModel> GetOfficeManagers(long officeId);

		/// <summary>
		/// Gets the staff member count.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		int GetStaffMemberCount(long officeId);

		/// <summary>
		/// Determines whether [has assigned postcodes] [the specified office id].
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns>
		///   <c>true</c> if [has assigned postcodes] [the specified office id]; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		bool HasAssignedPostcodes(long officeId);

		/// <summary>
		/// Gets the offices person manages.
		/// </summary>
		/// <param name="personId">The person id.</param>
    /// <param name="activeOnly">Whether to get only active offices</param>
    /// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
    List<OfficeDto> GetOfficesPersonManages(long personId, bool activeOnly = false);

		/// <summary>
		/// Saves the person offices.
		/// </summary>
		/// <param name="personOffices">The person offices.</param>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<PersonOfficeMapperDto> SavePersonOffices(List<PersonOfficeMapperDto> personOffices, long personId);


		/// <summary>
		/// Saves the person offices.
		/// </summary>
		/// <param name="personOffices">The person offices.</param>
		/// <param name="personIds">The person ids.</param>
		/// <returns></returns>
		List<PersonOfficeMapperDto> AssignOfficeStaff(List<PersonOfficeMapperDto> personOffices);

		/// <summary>
		/// Determines whether [is person statewide] [the specified person id].
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns>
		///   <c>true</c> if [is person statewide] [the specified person id]; otherwise, <c>false</c>.
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		bool IsPersonStatewide(long personId);

		/// <summary>
		/// Offices the has employers.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool OfficeHasEmployers(long officeId);

		/// <summary>
		/// Updates the employer assigned office.
		/// </summary>
		/// <param name="employerOffices">The employer offices.</param>
		/// <param name="employerId">The employer id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void UpdateEmployerAssignedOffice(List<EmployerOfficeMapperDto> employerOffices, long employerId);

		/// <summary>
		/// Updates the default office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void UpdateDefaultOffice(long officeId);

    /// <summary>
    /// Updates the default office for a specific type
    /// </summary>
    /// <param name="officeId">The office id.</param>
    /// <param name="officeType">Type of the office.</param>
    /// <exception cref="ServiceCallException"></exception>
    void UpdateDefaultOffice(long officeId, OfficeDefaultType officeType);

		/// <summary>
		/// Offices has job seekers.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool OfficeHasJobSeekers(long officeId);

		/// <summary>
		/// Offices the has jobs.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool OfficeHasJobs(long officeId);

		/// <summary>
		/// Gets the state of the offices by.
		/// </summary>
		/// <param name="stateId">The state id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<OfficeDto> GetOfficesByState(long stateId);

		/// <summary>
		/// Gets the state of the manager.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		long? GetManagerState(long personId);

		/// <summary>
		/// Updates the office assigned zips.
		/// </summary>
		/// <param name="zip">The zip.</param>
		/// <param name="officeIds">The office ids.</param>
		/// <exception cref="ServiceCallException"></exception>
		void UpdateOfficeAssignedZips(List<string> zip, List<long> officeIds);

		/// <summary>
		/// Gets the spidered employer.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<SpideredEmployerModel> GetSpideredEmployers(SpideredEmployerCriteria criteria);

		/// <summary>
		/// Gets the spidered employers with good matches.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<SpideredEmployerModel> GetSpideredEmployersWithGoodMatches(SpideredEmployerCriteria criteria);

		/// <summary>
		/// Saves the current office for person.
		/// </summary>
		/// <param name="currentOfficeId">The current office id.</param>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		bool SaveCurrentOfficeForPerson(long currentOfficeId, long personId);

		/// <summary>
		/// Gets the current office for person.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		CurrentOfficeModel GetCurrentOfficeForPerson(long personId);

		/// <summary>
		/// Determines whether [is office assigned] [the specified office id].
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		OfficeAssignedModel IsOfficeAssigned(long officeId);

		/// <summary>
		/// Activates the deactivate office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <param name="deactivate">if set to <c>true</c> [deactivate].</param>
		/// <returns></returns>
		ActivateDeactivateOfficeModel ActivateDeactivateOffice(long officeId, bool deactivate);

		/// <summary>
		/// Updates the Preferres status of an employer
		/// </summary>
		/// <param name="businessUnitId">The office id.</param>
		/// <param name="isPreferred">if set to <c>true</c> [deactivate].</param>
		/// <returns></returns>
		bool SetEmployerPreferredStatus(long businessUnitId, bool isPreferred);

		/// <summary>
		/// Gets the employer account types.
		/// </summary>
		/// <param name="lensPostingIds">The lens posting ids.</param>
		/// <returns></returns>
		Dictionary<string, long?> GetEmployerAccountTypes(List<string> lensPostingIds);

		/// <summary>
		/// Gets the fein.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <returns></returns>
		string GetFein(long businessUnitId);

		/// <summary>
		/// Validates the fein change.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="newFein">The new fein.</param>
		/// <returns></returns>
		ValidateFEINChangeModel ValidateFEINChange(long businessUnitId, string newFein);

		/// <summary>
		/// Saves the fein change.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="newFein">The new fein.</param>
		/// <param name="lockVersion">The lock version of the employer</param>
		/// <returns></returns>
		FEINChangeActions SaveFEINChange(long businessUnitId, string newFein, int? lockVersion = null);

		/// <summary>
		/// Updates the primary business unit.
		/// </summary>
		/// <param name="currentPrimaryBusinessUnitId">The current primary business unit id.</param>
		/// <param name="newPrimaryBusinessUnitId">The new primary business unit id.</param>
		void UpdatePrimaryBusinessUnit(long? currentPrimaryBusinessUnitId, long? newPrimaryBusinessUnitId);

		/// <summary>
		/// Gets the hiring manager office email.
		/// </summary>
		/// <param name="hiringManagerId">The hiring manager id.</param>
		string GetHiringManagerOfficeEmail(long hiringManagerId);

		/// <summary>
		/// Determines whether [is default administrator] [the specified person id].
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		OfficeDefaultAdministratorModel IsDefaultAdministrator(long personId, long? officeId = null);

		/// <summary>
		/// Gets the staff members.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <param name="statewide">The statewide.</param>
		/// <returns></returns>
    List<PersonModel> GetStaffMembers(string name, int listSize, bool? statewide = null);

		/// <summary>
		/// Creates the homepage alert.
		/// </summary>
		/// <param name="homepageAlerts">The homepage alerts.</param>
		void CreateHomepageAlert(List<BusinessUnitHomepageAlertView> homepageAlerts);

	}
}
