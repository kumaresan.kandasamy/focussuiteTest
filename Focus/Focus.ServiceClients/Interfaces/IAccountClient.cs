﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Exceptions;
using Focus.Core.Messages.AccountService;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface IAccountClient
	{
		/// <summary>
		/// Blocks a person
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="deactivateJobseeker">if set to <c>true</c> [deactivate jobseeker].</param>
		/// <param name="reasonText">The reason text.</param>
		/// <returns>
		/// A boolean indicating success or failure (Failure means the account was already blocked)
		/// </returns>
		bool BlockPerson(long personId, bool deactivateJobseeker = false, string reasonText = "");

		/// <summary>
		/// Unblocks a person
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns>A boolean indicating success or failure (Failure means the account was already unblocked)</returns>
		bool UnblockPerson(long personId);

		/// <summary>
		/// Changes the password.
		/// </summary>
		/// <param name="currentPassword">The current password.</param>
		/// <param name="newPassword">The new password.</param>
		bool ChangePassword(string currentPassword, string newPassword);

		/// <summary>
		/// Changes the name of the user.
		/// </summary>
		/// <param name="currentUsername">The current username.</param>
		/// <param name="newUsername">The new username.</param>
		bool ChangeUsername(string currentUsername, string newUsername);

		/// <summary>
		/// Checks if an email address is in use by another user
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="personId">The ID of the person who requires the email address</param>
		bool CheckEmailAddress(string emailAddress, long? personId = null);

		/// <summary>
		/// Changes the email address of the user.
		/// </summary>
		/// <param name="currentEmailAddress">The current email address.</param>
		/// <param name="newEmailAddress">The new email address.</param>
		bool ChangeEmailAddress(string currentEmailAddress, string newEmailAddress);

		/// <summary>
		/// Checks the user exists.
		/// </summary>
		/// <param name="registrationModel">The registration model.</param>
		/// <param name="existingUserId">The id of the existing user if amending a user</param>
    /// <param name="checkLocalOnly">Whether to only check the local database, or also any external client</param>
    /// <returns>A boolean indicating whether the user exists or not</returns>
		/// <exception cref="ServiceCallException"></exception>
    Tuple<bool, ExtenalUserPreExistenceReason> CheckUserExists(RegistrationModel registrationModel, long? existingUserId = null, bool checkLocalOnly = false);

    /// <summary>
    /// Checks the social security number in use.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
	  bool CheckSocialSecurityNumberInUse(string socialSecurityNumber);

		/// <summary>
		/// Registers the Talent user.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <param name="initialApprovalStatus">Override the initial approval status.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		TalentRegistrationModel RegisterTalentUser(TalentRegistrationModel model, ApprovalStatuses? initialApprovalStatus = null);

		/// <summary>
		/// Registers the career user.
		/// </summary>
		/// <param name="model">The career registration model.</param>
		/// <exception cref="ServiceCallException"></exception>
		void RegisterCareerUser(CareerRegistrationModel model);

		/// <summary>
		/// Registers the explorer user.
		/// </summary>
		/// <param name="model">The model.</param>
		void RegisterExplorerUser(ExplorerRegistrationModel model);

		/// <summary>
		/// Finds the name of the users by.
		/// </summary>
		/// <param name="firstName">The first name.</param>
		/// <param name="lastName">The last name.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		PagedList<UserView> FindUsersByName(string firstName, string lastName, int pageIndex, int pageSize);

		/// <summary>
		/// Finds the users by email.
		/// </summary>
		/// <param name="emailAddress">The email address.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		PagedList<UserView> FindUsersByEmail(string emailAddress, int pageIndex, int pageSize);

		/// <summary>
		/// Finds the users by office.
		/// </summary>
		/// <param name="externalOffice">The external office.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		PagedList<UserView> FindUsersByOffice(string externalOffice, int pageIndex, int pageSize);

		/// <summary>
		/// Finds the user by external identifier.
		/// </summary>
		/// <param name="externalId">The external identifier.</param>
		/// <returns></returns>
		UserView FindUserByExternalId(string externalId);

		/// <summary>
		/// Gets the user type of a user
		/// </summary>
		/// <param name="userId">The id of the user</param>
		/// <returns>The user type</returns>
		UserTypes GetUserType(long userId);

    /// <summary>
    /// Gets the user details.
    /// </summary>
    /// <param name="userId">The user id.</param>
    /// <param name="personId">The person id.</param>
    /// <param name="getSecurityAnswer">Whether to get the decrypted security answer.</param>
    /// <returns></returns>
		UserDetailsModel GetUserDetails(long userId = 0, long personId = 0, bool getSecurityAnswer = false);

    /// <summary>
    /// Updates the contact details.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <returns>
    /// User context for updating
    /// </returns>
    UserDto UpdateContactDetails(UserDetailsModel model);

    /// <summary>
    /// Updates the contact details.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <param name="checkCensorship">Whether to check the censorship</param>
    /// <param name="failedCensorshipCheck">Whether the update details failed censorship checks</param>
    /// <returns>
    /// User context for updating
    /// </returns>
		UserDto UpdateContactDetails(UserDetailsModel model, bool checkCensorship, out bool failedCensorshipCheck);

		/// <summary>
		/// Gets the job seeker's SSN
		/// </summary>
		/// <param name="personId">The Id of the job seeker</param>
		/// <returns>The job seeker's SSN</returns>
		string GetSSN(long personId);

		/// <summary>
		/// Updates the SSN.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PersonDto UpdateSsn(UpdateSsnModel model);

		/// <summary>
		/// Updates the user.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		long UpdateUser(UserDetailsModel model);

		/// <summary>
		/// Validates the employer.
		/// </summary>
		/// <param name="federalEmployerIdentificationNumber">The federal employer identification number.</param>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		ValidatedEmployerView ValidateEmployer(string federalEmployerIdentificationNumber, long? employerId = null);

		/// <summary>
		/// Gets the assist user.
		/// </summary>
		/// <returns></returns>
		List<UserLookup> GetAssistUsers();

		/// <summary>
		/// Gets the users roles.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		List<RoleDto> GetUsersRoles(long userId);

		/// <summary>
		/// Gets the roles for user.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		List<string> GetRolesForUser(long userId);

		/// <summary>
		/// Updates the users roles.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="usersRoles">The users roles.</param>
		void UpdateUsersRoles(long userId, List<RoleDto> usersRoles);

		/// <summary>
		/// Resets the users password. Generates new password and sends out reset password email.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="changePasswordUrl">The change password URL.</param>
		void ResetUsersPassword(long userId, string changePasswordUrl);

		/// <summary>
		/// Deactivates the account.
		/// </summary>
		/// <param name="userId">The user id.</param>
		void DeactivateAccount(long userId);

		/// <summary>
		/// Activates the account.
		/// </summary>
		/// <param name="userId">The user id.</param>
		void ActivateAccount(long userId);

		/// <summary>
		/// Sends the activation email.
		/// </summary>
		/// <param name="name">The user's name.</param>
		/// <param name="email">The email.</param>
		/// <param name="validationUrl">The validation URL.</param>
		void SendActivationEmail(string name, string email, string validationUrl);

		/// <summary>
		/// Sends the activation email.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="validationUrl">The validation URL.</param>
		void SendActivationEmail(string username, string validationUrl);

		/// <summary>
		/// Resets the password.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <param name="newPassword">The new password.</param>
		/// <param name="validationKey">The validation key.</param>
		/// <param name="pinUrl">The pin URL.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		AccountModel ResetPassword(long? userId = null, string newPassword = null, string validationKey = null, string pinUrl = null);

		/// <summary>
		/// Updates the screen name and email address.
		/// </summary>
		/// <param name="screenName">Name of the screen.</param>
		/// <param name="emailAddress">The email address.</param>
		void UpdateScreenNameAndEmailAddress(string screenName, string emailAddress);

		/// <summary>
		/// Changes the security question.
		/// </summary>
		/// <param name="question">The question.</param>
		/// <param name="questionId">The question Id or null if user defined question</param>
		/// <param name="answer">The answer.</param>
		/// <param name="questionIndex"></param>
		bool ChangeSecurityQuestion( string question, long? questionId, string answer, int questionIndex = 1 );

		/// <summary>
		/// Changes the security questions.
		/// </summary>
		/// <param name="questions">The question.</param>
		bool ChangeSecurityQuestions(List<UserSecurityQuestionDto> questions);

		/// <summary>
		/// Gets the user alert settings.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		UserAlertDto GetUserAlert(long userId);

		/// <summary>
		/// Updates the user's alert settings.
		/// </summary>
		/// <param name="userAlert">The user's alert settings.</param>
		/// <returns>A User Alert record</returns>
		UserAlertDto UpdateUserAlert(UserAlertDto userAlert);

		/// <summary>
		/// Changes the migrated status.
		/// </summary>
		/// <param name="isMigrated">if set to <c>true</c> [is migrated].</param>
		/// <returns></returns>
		bool ChangeMigratedStatus(bool isMigrated);

		/// <summary>
		/// Changes the consent status.
		/// </summary>
		/// <param name="isConsentGiven">if set to <c>true</c> [is consent given].</param>
		/// <returns></returns>
		bool ChangeConsentStatus(bool isConsentGiven);

		/// <summary>
		/// Changes the program of study.
		/// </summary>
		/// <param name="newStudyProgramAreaId">The new study program area id.</param>
		/// <param name="newStudyProgramDegreeId">The new study program degree id.</param>
		/// <returns></returns>
		bool ChangeProgramOfStudy(long? newStudyProgramAreaId, long? newStudyProgramDegreeId);

		/// <summary>
		/// Changes the enrollment status.
		/// </summary>
		/// <param name="enrollmentStatus">The enrollment status.</param>
		/// <returns></returns>
		bool ChangeEnrollmentStatus(SchoolStatus? enrollmentStatus);

		/// <summary>
		/// Changes the campus for a student.
		/// </summary>
		/// <param name="campusId">The campus location.</param>
		/// <returns></returns>
		bool ChangeCampus(long campusId);

		/// <summary>
		/// Changes the phone number of a person
		/// </summary>
		/// <param name="phoneNumber">The phone number.</param>
		/// <returns>A boolean indicating success</returns>
		bool ChangePhoneNumber(PhoneNumberDto phoneNumber);

		/// <summary>
		/// Validates the email.
		/// </summary>
		/// <param name="emails">The emails.</param>
		/// <param name="userType">Type of the user.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		EmailValidationModel ValidateEmail(List<string> emails, UserTypes userType);

		/// <summary>
		/// Sends the registration pin email.
		/// </summary>
		/// <param name="email">The email.</param>
		/// <param name="pinRegistrationUrl">The pin registration URL.</param>
		/// <param name="resetPasswordUrl">The reset password URL.</param>
		/// <param name="targetModule">The target module.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool SendRegistrationPinEmail(List<string> email, string pinRegistrationUrl, string resetPasswordUrl, FocusModules targetModule);

		/// <summary>
		/// Updates the manager flag.
		/// </summary>
		/// <param name="manager">if set to <c>true</c> [manager].</param>
		/// <exception cref="ServiceCallException"></exception>
		void UpdateManagerFlag(long personId, bool manager);

		/// <summary>
		/// Creates the assist user.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <exception cref="ServiceCallException"></exception>
		void CreateAssistUser(CreateAssistUserModel model);

		/// <summary>
		/// Creates multiple assist users.
		/// </summary>
		/// <param name="models">The models.</param>
		/// <exception cref="ServiceCallException"></exception>
		List<CreateAssistUserModel> CreateAssistUsers(List<CreateAssistUserModel> models);

		/// <summary>
		/// Updates the last surveyed date.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void UpdateLastSurveyedDate(long personId);

		/// <summary>
		/// Exports the assist users.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="file">The file.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<CreateAssistUserModel> ExportAssistUsers(string fileName, byte[] file);

    /// <summary>
    /// Imports the job seeker.
    /// </summary>
    /// <param name="externalId">The external identifier.</param>
    /// <returns></returns>
    string ImportJobSeeker(string externalId);

    /// <summary>
    /// Imports the employer and artifacts.
    /// </summary>
	  string ImportEmployerAndArtifacts(string externalId);

		/// <summary>
		/// Unsubscribes the user from emails.
		/// </summary>
		/// <param name="encryptedPersonId">The encrypted person id.</param>
		/// <param name="encryptionId">The encryption id.</param>
		/// <param name="urlEncoded">if set to <c>true</c> [URL encoded].</param>
		/// <returns>
		/// ErrorType
		/// </returns>
		ErrorTypes UnsubscribeJobseekerFromEmails(string encryptedPersonId, long encryptionId, bool urlEncoded = false);

		/// <summary>
		/// Unsubscribes the hiring manager from emails.
		/// </summary>
		/// <param name="encryptedPersonId">The encrypted person id.</param>
		/// <param name="encryptionId">The encryption id.</param>
		/// <param name="urlEncoded">if set to <c>true</c> [URL encoded].</param>
		/// <returns></returns>
		ErrorTypes UnsubscribeHiringManagerFromEmails(string encryptedPersonId, long encryptionId, bool urlEncoded = false);

		/// <summary>
		/// Inactivates the job seeker.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="inactivateReason">The inactivate reason.</param>
		/// <returns></returns>
		bool InactivateJobSeeker(long personId, AccountDisabledReason inactivateReason);

    /// <summary>
    /// Updates the assist user.
    /// </summary>
    /// <param name="userDetails">The user details.</param>
    /// <returns></returns>
	  UserDto UpdateAssistUser(UserDetailsModel userDetails);

		/// <summary>
		/// Get Security Questions for user
		/// </summary>
		/// <param name="questionIndex">The specific security question index - pass 0 to get all of them</param>
		/// <returns></returns>
		UserSecurityQuestionDto[] GetUserSecurityQuestion(int questionIndex = 0);

		/// <summary>
		/// Whether the logged on user has security questions
		/// </summary>
		/// <param name="userId">The id of the user</param>
		/// <returns></returns>
		bool HasUserSecurityQuestions(long userId);

		/// <summary>
		/// Updates the career account type
		/// </summary>
		/// <param name="accountType">Career Account Type - Job Seeker or Student</param>
		/// <returns></returns>
		bool ChangeCareerAccountType(CareerAccountType accountType);
	}
}
