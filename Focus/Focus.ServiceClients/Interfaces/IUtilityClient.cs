﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Document;
using Focus.Core.DataTransferObjects.FocusCore;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.Interfaces
{
	public interface IUtilityClient
	{
		/// <summary>
		/// Gets the plain document.
		/// </summary>
		/// <param name="docType">Type of the doc.</param>
		/// <param name="plainDocument">The plain document.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		string GetPlainDocument(DocumentType docType, string plainDocument);

		/// <summary>
		/// Gets the tagged document.
		/// </summary>
		/// <param name="docType">Type of the doc.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="binaryData">The binary data.</param>
		/// <param name="canonize">if set to <c>true</c> [canonize].</param>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		string GetTaggedDocument(DocumentType docType, string fileExtension, byte[] binaryData, bool canonize, out string html);
	}
}
