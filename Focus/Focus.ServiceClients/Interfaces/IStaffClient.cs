﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.User;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Web.Core.Models;

#endregion



namespace Focus.ServiceClients.Interfaces
{
    public interface IStaffClient
    {
        /// <summary>
        /// Searches the staff.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        PagedList<StaffSearchViewDto> SearchStaff(StaffCriteria criteria);

        /// <summary>
        /// Gets the assist user activities as paged.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        PagedList<AssistUserActivityViewDto> GetAssistUserActivitiesAsPaged(StaffCriteria criteria);

        /// <summary>
        /// Gets the assist user activities as list.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        List<AssistUserActivityViewDto> GetAssistUserActivitiesAsList(StaffCriteria criteria);

        /// <summary>
        /// Gets the assist user employers assisted.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        PagedList<AssistUserEmployersAssistedViewDto> GetAssistUserEmployersAssisted(StaffCriteria criteria);

        /// <summary>
        /// Gets the assist user job seekers assisted.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        PagedList<AssistUserJobSeekersAssistedViewDto> GetAssistUserJobSeekersAssisted(StaffCriteria criteria);

        /// <summary>
        /// Gets the staff profile.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <returns></returns>
        StaffProfileModel GetStaffProfile(long userId);

        /// <summary>
        /// Creates the homepage alert.
        /// </summary>
        /// <param name="homepageAlert">The homepage alert.</param>
        void CreateHomepageAlert(StaffHomepageAlertView homepageAlert);

        PersonActivityUpdateSettingsDto GetBackdateSettings(long personId);

        void UpdateStaffBackdateSettings(PersonActivityUpdateSettingsDto staffBackdateSettings);
    }
}
