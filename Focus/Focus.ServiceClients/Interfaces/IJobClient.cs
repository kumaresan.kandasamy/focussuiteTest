﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Job;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.JobService;
using Focus.Core.Models;
using Focus.Core.Models.Assist;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Framework.Exceptions;

#endregion

namespace Focus.ServiceClients.Interfaces
{
	public interface IJobClient
	{
		/// <summary>
		/// Gets the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobDto GetJob(long jobId);

		/// <summary>
		/// Flags a job for follow up.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		void FlagJobForFollowUp(long jobId);

		/// <summary>
		/// Saves the job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <returns></returns>
		JobDto SaveJob(JobDto job);

		/// <summary>
		/// Gets the job view.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobViewDto GetJobView(long jobId);

		/// <summary>
		/// Saves the job certificates.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobCertificates">The job certificates.</param>
		void SaveJobCertificates(long jobId, List<JobCertificateDto> jobCertificates);

		/// <summary>
		/// Saves the job languages.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobLanguages">The job languages.</param>
		void SaveJobLanguages(long jobId, List<JobLanguageDto> jobLanguages);

		/// <summary>
		/// Saves the job licences.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobLicences">The job licences.</param>
		void SaveJobLicences(long jobId, List<JobLicenceDto> jobLicences);

		/// <summary>
		/// Saves the job special requirements request.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobSpecialRequirements">The job special requirements.</param>
		void SaveJobSpecialRequirements(long jobId, List<JobSpecialRequirementDto> jobSpecialRequirements);

		/// <summary>
		/// Saves the job driving licence endorsements.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="jobDrivingLicenceEndorsements">The job driving licence endorsements.</param>
		/// <exception cref="ServiceCallException"></exception>
		void SaveJobDrivingLicenceEndorsements(long jobId, List<JobDrivingLicenceEndorsementDto> jobDrivingLicenceEndorsements);

		/// <summary>
		/// Saves the job locations.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobLocations">The job locations.</param>
		string SaveJobLocations(long jobId, List<JobLocationDto> jobLocations);

		/// <summary>
		/// Saves the job programs of study.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobProgramsOfStudy">The job programs of study.</param>
		void SaveJobProgramsOfStudy(long jobId, List<JobProgramOfStudyDto> jobProgramsOfStudy);

		/// <summary>
		/// Gets the job details.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="getRecommendedCount">if set to <c>true</c> [get recommended count].</param>
		/// <returns></returns>
		JobDetailsView GetJobDetails(long jobId, bool getRecommendedCount = false);

		/// <summary>
		/// Gets the previous job details.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobPrevDataDto GetJobPrevData( long jobId );

		/// <summary>
		/// Gets the job activity summary.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobActivityView GetJobActivitySummary(long jobId);

		/// <summary>
		/// Gets the jobs as a List.
		/// </summary>
		/// <param name="jobStatus">The job status.</param>
		/// <returns></returns>
		List<JobViewDto> GetJobs(JobStatuses jobStatus);

		/// <summary>
		/// Gets the jobs available to talent user by employee ID
		/// </summary>
		/// <param name="jobStatus">The job status.</param>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		List<JobViewDto> GetJobs(JobStatuses jobStatus, long? employeeId);

		/// <summary>
		/// Gets the jobs as a PagedList.
		/// </summary>
		/// <param name="jobStatus">The job status.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="maximumRows">The maximum rows.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="talentDraftJobs">The talent draft jobs flag.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<JobViewModel> GetJobs(JobStatuses jobStatus, int pageIndex, int maximumRows, string orderBy, bool talentDraftJobs = false);

	  /// <summary>
	  /// Gets the jobs for a business unit as a PagedList.
	  /// </summary>
	  /// <param name="jobStatus">The job status.</param>
	  /// <param name="businessUnitId">The business unit identifier.</param>
    /// <param name="pageIndex">Index of the page.</param>
	  /// <param name="maximumRows">The maximum rows.</param>
	  /// <param name="orderBy">The order by.</param>
	  /// <param name="talentDraftJobs">The talent draft jobs flag.</param>
	  /// <returns></returns>
	  /// <exception cref="ServiceCallException"></exception>
    PagedList<JobViewModel> GetJobsForBusinessUnit(JobStatuses jobStatus, long businessUnitId, int pageIndex, int maximumRows, string orderBy, bool talentDraftJobs = false);

	  /// <summary>
	  /// Gets the jobs for an employer as a PagedList.
	  /// </summary>
	  /// <param name="jobStatus">The job status.</param>
	  /// <param name="employerId">The employer identifier.</param>
	  /// <param name="pageIndex">Index of the page.</param>
	  /// <param name="maximumRows">The maximum rows.</param>
	  /// <param name="orderBy">The order by.</param>
	  /// <param name="talentDraftJobs">The talent draft jobs flag.</param>
	  /// <returns></returns>
	  /// <exception cref="ServiceCallException"></exception>
	  PagedList<JobViewModel> GetJobsForEmployer(JobStatuses jobStatus, long employerId, int pageIndex, int maximumRows, string orderBy, bool talentDraftJobs = false);
    
    /// <summary>
    /// Gets the jobs as a PagedList.
    /// </summary>
    /// <param name="jobStatus">The job status.</param>
    /// <param name="businessUnitId">The business unit identifier.</param>
    /// <param name="employerId">The employer identifier.</param>
    /// <param name="allEmployeesForBusinessUnit">Whether to get all jobs for the business unit (if specified)</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="maximumRows">The maximum rows.</param>
    /// <param name="orderBy">The order by.</param>
    /// <param name="talentDraftJobs">The talent draft jobs flag.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    PagedList<JobViewModel> GetJobs(JobStatuses jobStatus, long businessUnitId, long employerId, bool allEmployeesForBusinessUnit, int pageIndex, int maximumRows, string orderBy, bool talentDraftJobs = false);

		/// <summary>
		/// Gets the jobs as a PagedList.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<JobViewModel> GetJobs(JobCriteria criteria);

		/// <summary>
		/// Gets the job description.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		string GetJobDescription(long jobId);

		/// <summary>
		/// Gets the job tasks.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="scope">The job task scope (job or resume).</param>
		/// <returns>A list of job tasks matching the criteria</returns>
		List<JobTaskDetailsView> GetJobTasks(long onetId, JobTaskScopes? scope = null);

		/// <summary>
		/// Gets all job locations.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		List<JobLocationDto> GetAllJobLocations(long employerId);

		/// <summary>
		/// Gets the job locations by job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobLocationDto> GetJobLocations(long jobId);

		/// <summary>
		/// Gets the job locations for a business unit.
		/// </summary>
		/// <param name="jobId">The business unit id.</param>
		/// <returns></returns>
		List<JobLocationDto> GetAllJobLocationsForBusinessUnit(long jobId);

		/// <summary>
		/// Gets the job programs of study.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobProgramOfStudyDto> GetJobProgramsOfStudy(long jobId);

		/// <summary>
		/// Gets the job certificates.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobCertificateDto> GetJobCertificates(long jobId);

		/// <summary>
		/// Gets the job languages.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobLanguageDto> GetJobLanguages(long jobId);

		/// <summary>
		/// Gets the job licences.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobLicenceDto> GetJobLicences(long jobId);

		/// <summary>
		/// Gets the job special requirements.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobSpecialRequirementDto> GetJobSpecialRequirements(long jobId);

		/// <summary>
		/// Gets the count job special requirements.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns>The count</returns>
		int GetJobSpecialRequirementsCount(long jobId);

		/// <summary>
		/// Gets the job driving licence endorsements.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<JobDrivingLicenceEndorsementDto> GetJobDrivingLicenceEndorsements(long jobId);

		/// <summary>
		/// Gets the similar jobs.
		/// </summary>
		/// <param name="search">The search.</param>
		/// <returns></returns>
		List<SimilarJobView> GetSimilarJobs(string search, JobTypes jobType);

		/// <summary>
		/// Posts the job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="automaticApproval">Whether to automatically approve the posting</param>
		/// <param name="contentRating">The content rating.</param>
		/// <param name="inappropiateWords">The inappropiate words.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		JobDto PostJob(JobDto job, bool automaticApproval, out ContentRatings contentRating, out List<string> inappropiateWords, out bool specialConditions);

		/// <summary>
		/// Holds the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="approvalStatus">An optional approval status.</param>
		void HoldJob(long jobId, ApprovalStatuses? approvalStatus = null);

		/// <summary>
		/// Sets the job back to draft
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="approvalStatus">An optional approval status.</param>
		void SetJobToDraft(long jobId, ApprovalStatuses? approvalStatus = null);

		/// <summary>
		/// Deletes the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		void DeleteJob(long jobId);

		/// <summary>
		/// Duplicates the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		long DuplicateJob(long jobId);

		/// <summary>
		/// Refreshes the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="newExpirationDate">The new expiration date.</param>
		/// <param name="postingSurvey">The posting survey.</param>
		void RefreshJob(long jobId, DateTime newExpirationDate, PostingSurveyDto postingSurvey, int? newNumberOfOpenings = null);

		/// <summary>
		/// Reactivates the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="newExpirationDate">The new expiration date.</param>
		void ReactivateJob(long jobId, DateTime newExpirationDate);

		/// <summary>
		/// Closes the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="postingSurvey">The posting survey.</param>
		void CloseJob(long jobId, PostingSurveyDto postingSurvey);

		/// <summary>
		/// Gets the job posting referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<JobPostingReferralViewDto> GetJobPostingReferrals(JobPostingReferralCriteria criteria);

		/// <summary>
		/// Gets the job posting referral.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobPostingReferralViewDto GetJobPostingReferral(long jobId);

		/// <summary>
		/// Approves the posting referral.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		ErrorTypes ApprovePostingReferral(long jobId, int lockVersion);

		/// <summary>
		/// Denies the posting referral.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="referralDeniedReasons">the Denial Reasons for a job</param>
		void DenyPostingReferral(long jobId, int lockVersion, List<KeyValuePair<long, string>> referralDeniedReasons = null);

		/// <summary>
		/// Edits the posting referral.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="posting">The posting.</param>
		void EditPostingReferral(long jobId, string posting);

		/// <summary>
		/// Gets the employees pending postings.
		/// </summary>
		/// <param name="employeeId">The employee id.</param>
		/// <returns></returns>
		IEnumerable<JobViewDto> GetEmployeesPendingPostings(long employeeId);

		/// <summary>
		/// Looks up a job by job title, employer name and / or from / to dates.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="employerName">Name of the employer.</param>
		/// <param name="fromDate">From date.</param>
		/// <param name="toDate">To date.</param>
		/// <param name="jobStatus">The job status.</param>
		/// <returns></returns>
		/// 
		List<JobLookupView> LookupJob(string jobTitle, string employerName, DateTime? fromDate, DateTime? toDate, JobStatuses? jobStatus);

		/// <summary>
		/// Looks up a job by job id.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobStatus">The job status.</param>
		/// <returns></returns>
		List<JobLookupView> LookupJob(long jobId, JobStatuses? jobStatus);

		/// <summary>
		/// Gets the lens posting ID for a posted job URL by Id.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		string GetJobLensPostingId( long jobId );

		/// <summary>
		/// Gets the lens posting ID for a posted job URL by external Id (this may be an internal id, so this will be checked as well).
		/// </summary>
		/// <param name="checkId">The internal or external job id.</param>
		/// <param name="checkVeteranPriorityDate">Whether to check the VPS date.</param>
		/// <param name="jobStatuses">A list of acceptable job statuses</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		string GetJobLensPostingId(string checkId, bool checkVeteranPriorityDate, List<JobStatuses> jobStatuses = null);

		/// <summary>
		/// Gets the job address.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobAddressDto GetJobAddress(long jobId);

		/// <summary>
		/// Gets the job address.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobStatuses GetJobStatus(long jobId);

		/// <summary>
		/// Gets the job dates.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobDatesView GetJobDates(long jobId);

		/// <summary>
		/// Saves the job address.
		/// </summary>
		/// <param name="jobAddress">The job address.</param>
		JobAddressDto SaveJobAddress(JobAddressDto jobAddress);

		/// <summary>
		/// Gets the application instructions.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		string GetApplicationInstructions(long jobId);

		/// <summary>
		/// Gets the job applicants.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<ApplicationViewDto> GetJobApplicants(long jobId);

		/// <summary>
		/// Generates the posting.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="jobLocations">The job locations.</param>
		/// <param name="jobLanguages">The job languages.</param>
		/// <param name="jobCertificates">The job certificates.</param>
		/// <param name="jobLicences">The job licences.</param>
		/// <param name="jobSpecialRequirements">The job special requirements.</param>
		/// <param name="jobProgramsOfStudy">The job programs of study.</param>
		/// <param name="jobDrivingLicenceEndorsements">The job driving licence endorsements.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		string GeneratePosting(JobDto job, List<JobLocationDto> jobLocations, List<JobLanguageDto> jobLanguages, List<JobCertificateDto> jobCertificates, List<JobLicenceDto> jobLicences,
			List<JobSpecialRequirementDto> jobSpecialRequirements, List<JobProgramOfStudyDto> jobProgramsOfStudy, List<JobDrivingLicenceEndorsementDto> jobDrivingLicenceEndorsements);

		/// <summary>
		/// Gets the job activities for a given job
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<JobActionEventViewDto> GetJobActivities(JobActivityCriteria criteria);

		/// <summary>
		/// Prints the job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <returns></returns>
		byte[] PrintJob(long jobId, out string fileName);

		/// <summary>
		/// Gets the job activity users.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobActionEventViewDto> GetJobActivityUsers(long jobId);

		/// <summary>
		/// Checks the censorship for the job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="censorshipType">Type of the censorship.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<string> CheckCensorship(JobDto job, CensorshipType censorshipType);

		/// <summary>
		/// Saves the invite to apply.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="candidatePersonId">The candidate person id.</param>
		/// <param name="score">The job matching score.</param>
    /// <param name="queueInvitation">Whether the invitation should be queued.</param>
		/// <exception cref="ServiceCallException"></exception>
    void SaveInviteToApply(string lensPostingId, long jobId, long candidatePersonId, int score, bool queueInvitation);

		/// <summary>
		/// Updates the invite to apply.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="candidatePersonId">The candidate person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		bool UpdateInviteToApply(string lensPostingId, long candidatePersonId);

		/// <summary>
		/// Saves the education internship skills.
		/// </summary>
		/// <exception cref="ServiceCallException"></exception>
		void SaveEducationInternshipSkills(long jobId, List<JobEducationInternshipSkillDto> skills);

		/// <summary>
		/// Gets the education internship skills.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobEducationInternshipSkillDto> GetEducationInternshipSkills(long jobId);

		/// <summary>
		/// Updates the job assigned office.
		/// </summary>
		/// <param name="jobOffices">The job offices.</param>
		/// <param name="jobId">The job id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void UpdateJobAssignedOffice(List<JobOfficeMapperDto> jobOffices, long jobId);

		/// <summary>
		/// Assigns the job to staff member.
		/// </summary>
		/// <param name="jobIds">The job ids.</param>
		/// <param name="personId">The person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void AssignJobToStaffMember(List<long> jobIds, long personId);

		/// <summary>
		/// Updates the criminal background exclusion required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="criminalBackgroundExclusionRequired">if set to <c>true</c> [criminal background exclusion required].</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		JobDto UpdateCriminalBackgroundExclusionRequired(long jobId, bool criminalBackgroundExclusionRequired);

		/// <summary>
		/// Gets the criminal background exclusion required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool GetCriminalBackgroundExclusionRequired(long jobId);

		/// <summary>
		/// Gets the job's issues.
		/// </summary>
		/// <param name="jobId">The job Id.</param>
		/// <returns></returns>
		JobIssuesModel GetJobIssues(long jobId);

		/// <summary>
		/// Resolves the job's issues.
		/// </summary>
		/// <param name="jobId">The job unique identifier.</param>
		/// <param name="issuesResolved">The issues resolved.</param>
		/// <param name="postHireFollowUpsResolved">The post hire follow ups resolved.</param>
		/// <param name="isAutoResolution">Whether this issue is being automatically resolved</param>
		/// <exception cref="ServiceCallException"></exception>
		void ResolveJobIssues(long jobId, List<JobIssuesFilter> issuesResolved, List<long> postHireFollowUpsResolved, bool isAutoResolution = false);

		/// <summary>
		/// Invites the jobseekers to apply.
		/// </summary>
		/// <param name="personIdsWithScores">The person ids of the jobseekers to invite, along with matching score.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobLink">The job link.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="senderEmail">The sender email.</param>
		/// <exception cref="ServiceCallException"></exception>
		void InviteJobseekersToApply(Dictionary<long, int> personIdsWithScores, long jobId, string jobLink, string lensPostingId, string senderEmail);

    /// <summary>
    /// Imports the job order.
    /// </summary>
    /// <param name="externalJobId">The external job identifier.</param>
	  void ImportJobOrder(string externalJobId);

		/// <summary>
		/// Gets the job credit check required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		bool GetJobCreditCheckRequired(long jobId);

		/// <summary>
		/// Updates the credit check required.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="creditCheckRequired">if set to <c>true</c> [credit check required].</param>
		/// <returns></returns>
		JobDto UpdateCreditCheckRequired(long jobId, bool creditCheckRequired);

		/// <summary>
		/// Looks for job by external Id
		/// </summary>
		/// <param name="jobStatus"></param>
		/// <param name="externalId"></param>
		/// <returns></returns>
		JobDto GetJob(JobStatuses jobStatus, string externalId);

		/// <summary>
		/// Gets the hiring manager person name email for job.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <returns></returns>
		PersonNameEmail GetHiringManagerPersonNameEmailForJob(long jobId);

		/// <summary>
		/// Save details from the specified job into JobPrevData for later proofing
		/// </summary>
		/// <param name="jobId"></param>
		void SaveBeforeImageOfJob(long jobId);
	}
}
