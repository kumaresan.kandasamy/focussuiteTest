﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Framework.Exceptions;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface IResumeClient
	{
		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		ResumeModel GetResume(long? resumeId);

		/// <summary>
		/// Gets the resume names.
		/// </summary>
		/// <returns></returns>
		Dictionary<string, string> GetResumeNames();

		/// <summary>
		/// Gets the number of resumes
		/// </summary>
		/// <returns></returns>
		int GetResumeCount();

		/// <summary>
		/// Gets the resume info.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		ResumeEnvelope GetResumeInfo(long? resumeId);

		/// <summary>
		/// Gets info on all resumes for a user
		/// </summary>
		/// <param name="personId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<ResumeEnvelope> GetAllResumeInfo(long? personId = null);

		/// <summary>
		/// Gets the original uploaded resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="mimeType">The mimeType of the original resume.</param>
		/// <param name="htmlResume">The HTML version of the resume.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		byte[] GetOriginalUploadedResume(long resumeId, out string fileName, out string mimeType, out string htmlResume);

		/// <summary>
		/// Gets the HTML formatted resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		string GetHTMLFormattedResume(long resumeId);

		/// <summary>
		/// Re-Regisers the default resume for a person.
		/// </summary>
		/// <param name="personId">The id of the person (if set to null, it will assume the id of the currently logged on person.</param>
		void RegisterDefaultResume(long? personId = null);

		/// <summary>
		/// Saves the resume.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <param name="resumeNameUpdate">if set to <c>true</c> [resume name update].</param>
		/// <param name="personId">when set overrides resume personId</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		ResumeModel SaveResume(ResumeModel resume, bool resumeNameUpdate = false, long? personId = null);

		/// <summary>
		/// Saves the resume document.
		/// </summary>
		/// <param name="resumeDocument">The resume document.</param>
		/// <returns></returns>
		bool SaveResumeDocument(ResumeDocumentDto resumeDocument);

		/// <summary>
		/// Deletes the resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="newDefaultId">The id of the resume to set as the new default.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool DeleteResume(long resumeId, long? newDefaultId = null);

		/// <summary>
		/// Gets the snap shot.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <param name="escapeText">Whether to escape (HTML Encode) the summary</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		string GetSnapShot(ResumeModel resume, bool escapeText);

		/// <summary>
		/// Gets the tagged resume.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <param name="canonize">if set to <c>true</c> [canonize].</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		string GetTaggedResume(ResumeModel resume, bool canonize = false);

		/// <summary>
		/// Gets the tagged resume.
		/// </summary>
		/// <param name="resumeBytes">The resume bytes.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <returns></returns>
		string GetTaggedResume(byte[] resumeBytes, string fileExtension);

		/// <summary>
		/// Gets the HTML resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		string GetHTMLResume(long resumeId);

		/// <summary>
		/// Gets the resume object.
		/// </summary>
		/// <param name="taggedResume">The tagged resume.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		ResumeModel GetResumeObject(string taggedResume);

		/// <summary>
		/// Gets the distinct jobs.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		Dictionary<string, string> GetDistinctJobs(long resumeId);

		/// <summary>
		/// Sets the default resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool SetDefaultResume(long resumeId);

		/// <summary>
		/// Gets the default resume Id.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		long GetDefaultResumeId();

	  /// <summary>
	  /// Gets the default resume Id for a person
	  /// </summary>
	  /// <param name="personId">The id of person whose default resume id is required</param>
	  /// <param name="completedResumesOnly">Whether to only get completed resumes</param>
	  /// <returns></returns>
	  /// <exception cref="ServiceCallException"></exception>
	  long GetDefaultResumeId(long personId, bool completedResumesOnly = false);

		/// <summary>
		/// Gets the default resume.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		ResumeModel GetDefaultResume();

		/// <summary>
		/// Gets the count of resumes recently viewed by Employees.
		/// </summary>
		/// <returns></returns>
		int GetCountOfResumesRecentlyViewedByEmployees();

    /// <summary>
    /// Gets the resume template.
    /// </summary>
    /// <returns></returns>
	  ResumeModel GetResumeTemplate();
	}
}
