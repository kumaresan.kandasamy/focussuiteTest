﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Framework.Messaging;

#endregion

namespace Focus.ServiceClients.Interfaces
{
	public interface IProcessorClient
	{
		/// <summary>
		/// Enqueues the batch process.
		/// </summary>
		/// <param name="identifier">The identifier.</param>
		/// <returns></returns>
		string EnqueueBatchProcess(string identifier);

		/// <summary>
		/// Schedules the batch process.
		/// </summary>
		/// <param name="process">The process.</param>
		/// <param name="schedule">The schedule.</param>
		/// <returns></returns>
		string EnqueueBatchProcess(BatchProcesses process, MessageSchedulePlan schedule);

	  /// <summary>
	  /// Enqueues the import process.
	  /// </summary>
	  /// <param name="recordType">The type of import to perform</param>
	  /// <returns>A message indicating success or failure</returns>
	  string EnqueueImport(IntegrationImportRecordType recordType);

		/// <summary>
		/// Enqueues a request to populate the report database for specified entity ids
		/// </summary>
		/// <param name="type">The type of report table</param>
		/// <param name="idList">An optional list of ids if only specific entities are needed</param>
		/// <returns>A success message</returns>
		string EnqueueReportPopulation(ReportEntityType type, List<long> idList);

		/// <summary>
		/// Enqueues a request to populate the report database for all entities of a given type
		/// </summary>
		/// <param name="type">The type of report table</param>
		/// <param name="resetAll">Whether to delete all the entities first</param>
		/// <returns>A success message</returns>
		string EnqueueReportPopulation(ReportEntityType type, bool resetAll);

		string ResetCache();

		/// <summary>
		/// Sends reset password emails to imported users.
		/// </summary>
		/// <param name="emailsToSend">The number of emails to send.</param>
		/// <param name="emailRecordType">Type of the email record.</param>
		/// <param name="url">The reset password URL.</param>
		/// <returns>The number of emails actually sent</returns>
		int SendEmailsToImportedUsers(int emailsToSend, MigrationEmailRecordType emailRecordType, string url);

		/// <summary>
		/// Encrypts a field in the database
		/// </summary>
		/// <returns>A boolean indicating if the field was already encrypted</returns>
		bool EncryptField();

		/// <summary>
		/// Encrypts a field in the database
		/// </summary>
		/// <returns>A boolean indicating if the field was already encrypted</returns>
		bool DecryptField();
	}
}
