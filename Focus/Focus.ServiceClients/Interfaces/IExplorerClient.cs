﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Messages.ExplorerService;
using Focus.Core.Models;
using Focus.Core.Views;
using Framework.Exceptions;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface IExplorerClient
	{
		/// <summary>
		/// Gets the ping.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		PingResponse Ping(string name);

		/// <summary>
		/// Gets the state areas.
		/// </summary>
		/// <returns></returns>
		List<StateAreaViewDto> GetStateAreas();

		/// <summary>
		/// Gets a state area.
		/// </summary>
		/// <param name="stateAreaId"></param>
		/// <returns></returns>
		StateAreaViewDto GetStateArea(long stateAreaId);

		/// <summary>
		/// Gets the state area name.
		/// </summary>
		/// <param name="stateAreaId">The state area id.</param>
		/// <returns></returns>
		string GetStateAreaName(long stateAreaId);

		/// <summary>
		/// Gets the state area default.
		/// </summary>
		/// <returns></returns>
		StateAreaViewDto GetStateAreaDefault();

		/// <summary>
		/// Gets the All States/All Areas entry.
		/// </summary>
		/// <returns>The DTO object for the 'All States' record</returns>
		StateAreaViewDto GetAllStatesAllAreasEntry();

		/// <summary>
		/// Gets the states.
		/// </summary>
		/// <returns></returns>
		List<StateAreaViewDto> GetStates();

		/// <summary>
		/// Gets the areas.
		/// </summary>
		/// <param name="stateAreaId">The state id.</param>
		/// <returns></returns>
		List<StateAreaViewDto> GetAreas(long stateAreaId);

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="searchTerm">The search term.</param>
		/// <returns></returns>
		List<JobDto> GetJobs(long stateAreaId, string searchTerm);

		/// <summary>
		/// Gets the employers.
		/// </summary>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="searchTerm">The search term.</param>
		/// <returns></returns>
		List<EmployerDto> GetEmployers(long stateAreaId, string searchTerm);

		/// <summary>
		/// Gets the degree education levels.
		/// </summary>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="searchTerm">The search term.</param>
		/// <param name="searchClientDataOnly">A flag indicating whether to search client data only</param>
		/// <param name="detailedDegreeLevel">The detailed degree level.</param>
		/// <returns></returns>
		List<DegreeAliasViewDto> GetDegreeAliases(long stateAreaId, string searchTerm, bool? searchClientDataOnly, DetailedDegreeLevels detailedDegreeLevel, int count = -1);

		/// <summary>
		/// Gets a career area.
		/// </summary>
		/// <param name="careerAreaId">The id of the career area to get.</param>
		/// <returns>The career area dto</returns>
		/// <exception cref="ServiceCallException"></exception>
		CareerAreaDto GetCareerArea(long careerAreaId);

		/// <summary>
		/// Gets the career areas.
		/// </summary>
		/// <returns></returns>
		List<CareerAreaDto> GetCareerAreas();

		List<CareerAreaJobView> GetCareerAreaJobs(long careerAreaId, bool filterJobs = false);

		/// <summary>
		/// Gets total jobs by career area
		/// </summary>
		/// <param name="criteria">Criteria to restrict jobs being entered</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns>A list of career areas and job totals.</returns>
		List<CareerAreaJobTotalView> GetCareerAreaJobTotals(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the job career areas.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<long> GetJobCareerAreas(long jobId);

		/// <summary>
		/// Gets the job title.
		/// </summary>
		/// <param name="jobTitleId">The job title id.</param>
		/// <returns></returns>
		JobTitleDto GetJobTitle(long jobTitleId);

		/// <summary>
		/// Gets the job titles.
		/// </summary>
		/// <param name="searchTerm">The search term.</param>
		/// <returns></returns>
		List<JobTitleDto> GetJobTitles(string searchTerm);

		/// <summary>
		/// Gets the skill categories.
		/// </summary>
		/// <param name="parentSkillCategoryId">The parent skill category id.</param>
		/// <returns></returns>
		List<SkillCategoryDto> GetSkillCategories(long? parentSkillCategoryId);

		/// <summary>
		/// Gets the skills category skills.
		/// </summary>
		/// <returns></returns>
		List<SkillCategorySkillViewDto> GetSkillCategorySkills();

		/// <summary>
		/// Gets the skills.
		/// </summary>
		/// <param name="skillCategoryId">The skill category id.</param>
		/// <returns></returns>
		List<SkillDto> GetSkills(long? skillCategoryId);

		/// <summary>
		/// Gets skills associated with jobs
		/// </summary>
		/// <returns>A list of skills with jobs</returns>
		List<SkillsForJobsViewDto> GetSkillsForJobs(int maxRank, string term, int count = 50);

		/// <summary>
		/// Gets the site search results.
		/// </summary>
		/// <param name="searchTerm">The search term.</param>
		/// <returns></returns>
		List<SiteSearchResultModel> GetSiteSearchResults(string searchTerm);

		/// <summary>
		/// Gets the job report.
		/// </summary>
		/// <param name="criteria">The criteria on which to search.</param>
		/// <returns></returns>
		ExplorerJobReportView GetJobReport(ExplorerCriteria criteria);

		/// <summary>
		/// Gets the job report.
		/// </summary>
		/// <param name="criteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <param name="refreshMilitaryOccupations">Whether to add a list of military occupation codes to the criteria based on the military occupation ID</param>
		/// <returns></returns>
		List<ExplorerJobReportView> GetJobReport(ExplorerCriteria criteria, int listSize, bool refreshMilitaryOccupations = true);

		/// <summary>
		/// Gets the similar job report.
		/// </summary>
		/// <param name="criteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<ExplorerJobReportView> GetSimilarJobReport(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the job employers
		/// </summary>
		/// <param name="criteria">The criteria on which to search.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<EmployerJobViewDto> GetJobEmployers(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the job skills.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobSkillViewDto> GetJobSkills(long jobId);

		/// <summary>
		/// Gets the job degree certifications.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		JobDegreeCertificationModel GetJobDegreeCertifications(ExplorerCriteria criteria);

		/// <summary>
		/// Gets the job degree certifications.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		JobDegreeCertificationModel GetJobDegreeReportCertifications(ExplorerCriteria criteria);

		/// <summary>
		/// Gets the career paths.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		JobCareerPathModel GetJobCareerPaths(long jobId);

		/// <summary>
		/// Gets the career paths.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="stateAreaId">An id of the state area should job report details (i.e Hiring Demand) be required</param>
		/// <returns></returns>
		JobCareerPathModel GetJobCareerPaths(long jobId, long? stateAreaId);

		/// <summary>
		/// Gets the employer report.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<EmployerJobViewDto> GetEmployerReport(ExplorerCriteria reportCriteria, int listSize);

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		EmployerDto GetEmployer(long employerId);

		/// <summary>
		/// Gets the employer jobs.
		/// </summary>
		/// <param name="criteria">The criteria on which to search.</param>
		/// <returns></returns>
		List<ExplorerEmployerJobReportView> GetEmployerJobs(ExplorerCriteria criteria);

		/// <summary>
		/// Gets the employer skills.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<SkillModel> GetEmployerSkills(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the school's departments.
		/// </summary>
		/// <returns>A list of school departments</returns>
		List<ProgramAreaDto> GetProgramAreas();

		/// <summary>
		/// Gets the school's departments.
		/// </summary>
		/// <returns>A list of school departments</returns>
		ProgramAreaDto GetProgramArea(long programAreaId);

		/// <summary>
		/// Gets the degrees for a department
		/// </summary>
		/// <returns>A list of school departments</returns>
		DegreeDto GetDegree(long? degreeId);

		/// <summary>
		/// Gets the degrees for a department
		/// </summary>
		/// <param name="programAreaId">Whether to restrict to a specific program area</param>
		/// <returns>A list of school departments</returns>
		List<ProgramAreaDegreeViewDto> GetProgramAreaDegrees(long? programAreaId);

		/// <summary>
		/// Gets the degrees for a department
		/// </summary>
		/// <returns>A list of school departments</returns>
		List<ProgramAreaDegreeViewDto> GetProgramAreaDegrees();

		/// <summary>
		/// Gets the degree education levels for a department
		/// </summary>
		/// <param name="programAreaId">Whether to restrict to a specific program area</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="degreeLevel">The level of degree</param>
		/// <param name="checkDegreeLevelByJob">Whether to check Degree Level criteria by looking at JobDegreeLevel records</param>
		/// <returns>
		/// A list of degree education levels
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		List<ProgramAreaDegreeEducationLevelViewDto> GetProgramAreaDegreeEducationLevels(long? programAreaId, ExplorerCriteria criteria = null, DetailedDegreeLevels? degreeLevel = null, bool? checkDegreeLevelByJob = null);

		List<ProgramAreaDegreeEducationLevelViewDto> GetROnetProgramAreaDegreeEducationLevels(long ronetId);

		/// <summary>
		/// Gets the degree education levels for a department
		/// </summary>
		/// <returns>A list of degree education levels</returns>
		List<ProgramAreaDegreeEducationLevelViewDto> GetProgramAreaDegreeEducationLevels();

		/// <summary>
		/// Gets the degree education level report.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="degreeLevel">The degree level.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<JobDegreeEducationLevelReportViewDto> GetDegreeEducationLevelReport(ExplorerCriteria reportCriteria, DetailedDegreeLevels degreeLevel, int listSize);

		/// <summary>
		/// Gets the degree education level.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <returns></returns>
		DegreeEducationLevelViewDto GetDegreeEducationLevel(long degreeEducationLevelId);

		/// <summary>
		/// Gets the degree education level job report.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <param name="jobDegreeTierLevel">The job degree tier level.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<ExplorerJobReportView> GetDegreeEducationLevelJobReport(ExplorerCriteria criteria, int listSize, int jobDegreeTierLevel);

		/// <summary>
		/// Get the degree education level skills.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<SkillModel> GetDegreeEducationLevelSkills(long degreeEducationLevelId, long stateAreaId, int listSize);

		/// <summary>
		/// Gets the degree education level employers.
		/// </summary>
		/// <param name="criteria">The criteria on which to search.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<EmployerJobViewDto> GetDegreeEducationLevelEmployers(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the degree education level study places.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <param name="stateAreaId">The state area id.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<StudyPlaceDegreeEducationLevelViewDto> GetDegreeEducationLevelStudyPlaces(long degreeEducationLevelId, long stateAreaId, int listSize);

		/// <summary>
		/// Gets the degree education level exts.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <returns></returns>
		List<DegreeEducationLevelExtDto> GetDegreeEducationLevelExts(long degreeEducationLevelId);

		/// <summary>
		/// Gets the skill report.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<SkillModel> GetSkillReport(ExplorerCriteria reportCriteria, int listSize);

		/// <summary>
		/// Gets the skill.
		/// </summary>
		/// <param name="skillId">The skill id.</param>
		/// <returns></returns>
		SkillDto GetSkill(long skillId);

		/// <summary>
		/// Gets the skill job report
		/// </summary>
		/// <param name="criteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <param name="maximumRank">The maximum rank allowed for the jobskill</param>
		/// <returns></returns>
		List<ExplorerJobReportView> GetSkillJobReport(ExplorerCriteria criteria, int listSize, int? maximumRank = 20);

		/// <summary>
		/// Gets the skill employers.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<EmployerJobViewDto> GetSkillEmployers(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the job job titles.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobTitleDto> GetJobJobTitles(long jobId);

		/// <summary>
		/// Gets the job education requirements.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobEducationRequirementDto> GetJobEducationRequirements(long jobId);

		/// <summary>
		/// Gets the job experience levels.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		List<JobExperienceLevelDto> GetJobExperienceLevels(long jobId);

		/// <summary>
		/// Sends an email.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="emailAddresses">The email addresses.</param>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		/// <param name="bookmarkUrl">The bookmark url.</param>
		void SendEmail(ExplorerCriteria reportCriteria, string emailAddresses, string emailSubject, string emailBody, string bookmarkUrl);

		/// <summary>
		/// Sends a help email.
		/// </summary>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		void SendHelpEmail(string emailSubject, string emailBody);

		/// <summary>
		/// Gets my account model.
		/// </summary>
		/// <returns></returns>
		MyAccountModel GetMyAccountModel();

		/// <summary>
		/// Gets the my resume model.
		/// </summary>
		/// <returns></returns>
		MyResumeModel GetMyResumeModel();

		/// <summary>
		/// Gets the internship report.
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<InternshipReportViewDto> GetInternshipReport(ExplorerCriteria reportCriteria, int listSize);

		/// <summary>
		/// Gets the employer internships.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		List<InternshipReportViewDto> GetEmployerInternships(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the employer internships.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="listSize">The number of records to return</param>
		/// <returns></returns>
		List<InternshipReportViewDto> GetInternshipsBySkill(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the internship.
		/// </summary>
		/// <param name="internshipCategoryId">The internship category id.</param>
		/// <returns></returns>
		InternshipCategoryDto GetInternship(long internshipCategoryId);

		/// <summary>
		/// Gets the internship skills.
		/// </summary>
		/// <param name="internshipCategoryId">The internship category id.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<SkillModel> GetInternshipSkills(long internshipCategoryId, int listSize);

		/// <summary>
		/// Gets the internship employers.
		/// </summary>
		/// <param name="criteria">The criteria on which to search.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<EmployerJobViewDto> GetInternshipEmployers(ExplorerCriteria criteria, int listSize);

		/// <summary>
		/// Gets the internship jobs.
		/// </summary>
		/// <param name="internshipCategoryId">The internship category id.</param>
		/// <param name="listSize">The list size.</param>
		/// <returns></returns>
		List<InternshipJobTitleDto> GetInternshipJobs(long internshipCategoryId, int listSize);

		/// <summary>
		/// Gets the internship categories.
		/// </summary>
		/// <returns></returns>
		List<InternshipCategoryDto> GetInternshipCategories();

		/// <summary>
		/// Gets the internship categories.
		/// </summary>
		/// <param name="internshipCategoryIds">The internship category ids.</param>
		/// <returns></returns>
		List<InternshipCategoryDto> GetInternshipCategories(List<long> internshipCategoryIds);

		/// <summary>
		/// Uploads the resume.
		/// </summary>
		/// <param name="resumeModel">The resume model.</param>
		/// <returns></returns>
		MyResumeModel UploadResume(ExplorerResumeModel resumeModel);

		/// <summary>
		/// Gets the jobs related to a job title
		/// </summary>
		/// <param name="jobTitleId">The Id of the Job Title</param>
		/// <returns>A list of related jobs</returns>
		List<JobDto> GetRelatedJobs(long jobTitleId);

		/// <summary>
		/// Gets the jobs related to a  military occupation
		/// </summary>
		/// <param name="militaryOccupationId">The Id of the military occupation</param>
		/// <returns> list of related jobs</returns>
		List<JobDto> GetJobsForMilitaryOccupation(long militaryOccupationId);

		/// <summary>
		/// Gets the job id from R onet.
		/// </summary>
		/// <param name="ronet">The ronet.</param>
		/// <returns></returns>
		long GetJobIdFromROnet(string ronet);

		/// <summary>
		/// Gets the Explorer job from ronet.
		/// </summary>
		/// <param name="ROnet">The ronet.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		JobDto GetJobFromROnet(string ROnet);

		/// <summary>
		/// Gets the R onet from resume.
		/// </summary>
		/// <param name="resumexml">The resumexml.</param>
		/// <returns></returns>
		string GetROnetFromResume(string resumexml);

		/// <summary>
		/// Converts the aplhanumeric Ronet to numeric Ronet.
		/// </summary>
		/// <param name="alphaROnet">The numeric R onet.</param>
		/// <returns></returns>
		List<OnetDetailsView> GetOnetFromROnet(string alphaROnet);

		/// <summary>
		/// Gets the R onets from job.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		string GetROnetsFromJob(long jobId);

		/// <summary>
		/// Gets the SOCs for onet.
		/// </summary>
		/// <param name="onetId">The Id of the Onet.</param>
		/// <returns>A list of SOCs</returns>
		List<SOCDto> GetSOCForOnet(long onetId);

		/// <summary>
		/// Gets the job introduction.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		string GetJobIntroduction(long jobId);

		/// <summary>
		/// Users the logged in.
		/// </summary>
		/// <param name="authenticated">if set to <c>true</c> [authenticated].</param>
		/// <returns></returns>
		bool UserLoggedIn(bool authenticated);
	}
}
