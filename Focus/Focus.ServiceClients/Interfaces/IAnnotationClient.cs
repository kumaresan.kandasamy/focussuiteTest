﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Framework.Exceptions;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface IAnnotationClient
	{
		/// <summary>
		/// Gets a bookmark.
		/// </summary>
		/// <param name="bookmarkId">The bookmark id.</param>
		/// <returns></returns>
		BookmarkDto GetBookmark(long bookmarkId);

		/// <summary>
		/// Gets my bookmarks
		/// </summary>
		/// <returns></returns>
		List<BookmarkDto> GetMyBookmarks(BookmarkTypes? bookmarkType = null, string bookmarkName = null);

		/// <summary>
		/// Creates a bookmark.
		/// </summary>
		/// <param name="name">The name</param>
		/// <param name="type">The type</param>
		/// <param name="explorerCriteria">The bookmark criteria for explorer</param>
		bool CreateBookmark(string name, BookmarkTypes type, ExplorerCriteria explorerCriteria);

		/// <summary>
		/// Creates a bookmark.
		/// </summary>
		/// <param name="name">The name</param>
		/// <param name="type">The type</param>
		/// <param name="criteria">The bookmark criteria</param>
		bool CreateBookmark(string name, BookmarkTypes type, string criteria = "");

		/// <summary>
		/// Deletes a bookmark.
		/// </summary>
		/// <param name="bookmarkId">The bookmark id.</param>
		void DeleteBookmark(long bookmarkId);

		/// <summary>
		/// Saves the search.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="criteria"></param>
		/// <param name="type"></param>
		/// <param name="frequency"></param>
		/// <param name="format"></param>
		/// <param name="toEmailAddress"></param>
		/// <param name="status"></param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		/// (searchName, searchCriteria.ToString(), frequency, format,
		bool SaveSearch(long id, string name, SearchCriteria criteria, SavedSearchTypes type, EmailAlertFrequencies frequency, EmailFormats format, string toEmailAddress, AlertStatus status, bool emailRequired = false);

		/// <summary>
		/// Gets the saved search.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<SearchDetails> GetSavedSearches(SavedSearchTypes type);

		/// <summary>
		/// Deletes the save search.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool DeleteSaveSearch(long id);
	}
}
