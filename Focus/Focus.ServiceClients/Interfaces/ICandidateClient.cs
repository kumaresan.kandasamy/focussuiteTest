﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Candidate;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface ICandidateClient
	{
		/// <summary>
		/// Emails the candidate.
		/// </summary>
		/// <param name="personId">The person id of the candidate.</param>
		/// <param name="emailSubject">The email subject.</param>
		/// <param name="emailBody">The email body.</param>
		/// <param name="detectUrl">Whether to detect URLs</param>
		/// <param name="bccSender">if set to <c>true</c> [BCC sender].</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="isInviteToApply">if set to <c>true</c> [is invite to apply].</param>
		/// <param name="candidateName">Name of the candidate.</param>
		/// <param name="senderAddress">Sender address</param>
    /// <param name="ContactRequest">Whether this is a Contact Request Message</param>
    void SendCandidateEmail(long personId, string emailSubject, string emailBody, bool detectUrl, bool bccSender = false, long jobId = 0, bool isInviteToApply = false, string candidateName = "", string senderAddress = null, bool ContactRequest = false);

    /// <summary>
    /// Sends the candidate emails.
    /// </summary>
    /// <param name="emails">The emails.</param>
    /// <param name="detectUrl">Whether to detect URLs</param>
    /// <param name="sendCopy">if set to <c>true</c> [send copy].</param>
    /// <param name="footerType">Type of the footer.</param>
    /// <param name="footerUrl">The footer URL.</param>
    /// <param name="checkUnsubscribed">Whether to check if the job seeker is unsubscribed</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    int SendCandidateEmails(List<CandidateEmailView> emails, bool detectUrl, bool sendCopy = false, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null, bool checkUnsubscribed = false);

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="returnFullResume">if set to <c>true</c> to return the full resume.</param>
		/// <param name="logView">Whether to log the fact the resume has been viewed</param>
		/// <param name="includeAge">Whether to display the age in the resume</param>
		/// <param name="noResumeMessage">A message to show if no resume is found</param>
		/// <returns></returns>
		string GetResumeAsHtml(long personId, bool returnFullResume = false, bool logView = true, bool includeAge = false, string noResumeMessage = null, long jobId = 0);

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="logView">Whether to log an action to record the resume has been viewed.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		string GetResumeByIdAsHtml(long resumeId, bool logView = true);

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="personId"></param>
		/// <param name="jobId">The job id.</param>
		/// <param name="returnFullResume">if set to <c>true</c> to return the full resume.</param>
		/// <returns></returns>
		ResumeView GetResume(long personId, bool returnFullResume = false);

		/// <summary>
		/// Gets the resume by id.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <returns></returns>
		ResumeView GetResumeById(long resumeId);

		/// <summary>
		/// Gets the resumes.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		List<ResumeSummary> GetResumes(long personId);

		/// <summary>
		/// Gets the job seeker referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<JobSeekerReferralAllStatusViewDto> GetJobSeekerReferrals(JobSeekerReferralCriteria criteria);

		/// <summary>
		/// Gets the job seeker referrals.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<JobSeekerView> SearchJobSeekers(JobSeekerCriteria criteria);

		/// <summary>
		/// Gets the job seeker referral.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <returns></returns>
		JobSeekerReferralAllStatusViewDto GetJobSeekerReferral(long candidateApplicationId);

		/// <summary>
		/// Approves the candidate referral.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="autoApproval">if set to <c>true</c> [auto approval].</param>
		/// <returns>
		/// A message if there was a problem approving it, blank otherwise
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		string ApproveCandidateReferral(long candidateApplicationId, int lockVersion, bool autoApproval = false);

		/// <summary>
		/// Denies the candidate referral.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="applicationLockVersion"></param>
		/// <param name="referralDeniedReasons">List of reasons why the referral has been denied</param>
		void DenyCandidateReferral( long candidateApplicationId, int applicationLockVersion, List<KeyValuePair<long, string>> referralDeniedReasons = null );

		/// <summary>
		/// Puts the candidate referral on hold
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="onHoldReason">The reason the referral was put on hold.</param>
		/// <param name="applicationLockVersion">The application lock version.</param>
		/// <exception cref="ServiceCallException"></exception>
		void HoldCandidateReferral(long candidateApplicationId, ApplicationOnHoldReasons onHoldReason, int applicationLockVersion);

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="activityId">The activity id.</param>
		/// <param name="jobSeeker">The job seeker.</param>
		/// <param name="actionedDate">the action date</param>
		/// <param name="isSelfAssigned">Whether the job seeker is assigning themselves</param>
		bool AssignActivity(long activityId, JobSeekerView jobSeeker, DateTime? actionedDate = null, bool isSelfAssigned = false);

	  /// <summary>
	  /// Assigns the activity to multiple users.
	  /// </summary>
	  /// <param name="activityId">The activity id.</param>
	  /// <param name="jobSeekers">The job seekers.</param>
	  /// <param name="actionedDate">the action date</param>
	  /// <exception cref="ServiceCallException"></exception>
	  bool AssignActivityToMultipleUsers(long activityId, List<JobSeekerView> jobSeekers,DateTime? actionedDate =null);

		/// <summary>
		/// Gets the job seeker lists.
		/// </summary>
		/// <param name="listType">Type of the list.</param>
		/// <returns></returns>
		List<SelectView> GetJobSeekerLists(ListTypes listType);

		/// <summary>
		/// Gets the job seeker referrals.
		/// </summary>
		/// <param name="listId">The list id.</param>
		/// <returns></returns>
		List<JobSeekerView> GetJobSeekersForList(long listId);

		/// <summary>
		/// Gets the job seeker referrals.
		/// </summary>
		/// <param name="listId">The list id.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<JobSeekerView> GetJobSeekersForPagedList(long listId, int pageIndex, int pageSize);

		/// <summary>
		/// Adds the job seeker to list.
		/// </summary>
		/// <param name="listType">Type of the list.</param>
		/// <param name="newListName">New name of the list.</param>
		/// <param name="jobSeekers">The job seekers.</param>
		/// <returns></returns>
		string AddJobSeekersToList(ListTypes listType, string newListName, JobSeekerView[] jobSeekers);

		/// <summary>
		/// Adds the job seeker to list.
		/// </summary>
		/// <param name="listType">Type of the list.</param>
		/// <param name="listId">The list id.</param>
		/// <param name="jobSeekers">The job seekers.</param>
		/// <returns></returns>
		string AddJobSeekersToList(ListTypes listType, long listId, JobSeekerView[] jobSeekers);

		/// <summary>
		/// Removes the job seekers from list.
		/// </summary>
		/// <param name="listId">The list id.</param>
		/// <param name="jobSeekers">The job seekers.</param>
		void RemoveJobSeekersFromList(long listId, JobSeekerView[] jobSeekers);

		/// <summary>
		/// Deletes the job seeker list.
		/// </summary>
		/// <param name="listId">The list id.</param>
		void DeleteJobSeekerList(long listId);

		/// <summary>
		/// Assigns the job seeker to admin.
		/// </summary>
		/// <param name="adminId">The admin id.</param>
		/// <param name="jobSeeker">The job seeker.</param>
		/// <returns></returns>
		string AssignJobSeekerToAdmin(long adminId, JobSeekerView jobSeeker);

		/// <summary>
		/// Gets the candidate candidate system defaults.
		/// </summary>
		/// <returns></returns>
		CandidateSystemDefaultsView GetCandidateCandidateSystemDefaults();

		/// <summary>
		/// Updates the candidate system defaults.
		/// </summary>
		/// <param name="systemDefaults">The system defaults.</param>
		void UpdateCandidateSystemDefaults(CandidateSystemDefaultsView systemDefaults);

		/// <summary>
		/// Creates the homepage alert.
		/// </summary>
		/// <param name="homepageAlert">The homepage alert.</param>
		void CreateHomepageAlert(JobSeekerHomepageAlertView homepageAlert);

		/// <summary>
		/// Creates the homepage alert.
		/// </summary>
		/// <param name="homepageAlerts">The homepage alerts.</param>
		void CreateHomepageAlert(List<JobSeekerHomepageAlertView> homepageAlerts);

		/// <summary>
		/// Gets the person note.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		PersonNoteDto GetPersonNote(long personId, long? employerId);

		/// <summary>
		/// Saves the note.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <param name="note">The note.</param>
		void SavePersonNote(long personId, long? employerId, string note);

		/// <summary>
		/// Resfers the candidate.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="score">The score.</param>
		/// <param name="waivedRequirements">The waived requirements.</param>
		/// <param name="staffReferral">if set to <c>true</c> [staff referral].</param>
		/// <returns>Whether the application was queued due to being a veteran job (and job seeker is not a veteran)</returns>
		/// <exception cref="ServiceCallException"></exception>
		bool ReferCandidate(long personId, long jobId, int score, List<string> waivedRequirements, bool staffReferral);

		/// <summary>
		/// Toggles the candidate flag.
		/// </summary>
		/// <param name="personId">The person id.</param>
		void ToggleCandidateFlag(long personId);

		/// <summary>
		/// Updates the application status.
		/// </summary>
		/// <param name="personId">The candidate external id.</param>
		/// <param name="jobId">The job id.</param>
		/// <param name="newStatus">The new status.</param>
		void UpdateApplicationStatus(long personId, long jobId, ApplicationStatusTypes newStatus);

		/// <summary>
		/// Updates the application status.
		/// </summary>
		/// <param name="candidateApplicationId">The candidate application id.</param>
		/// <param name="newStatus">The new status.</param>
		void UpdateApplicationStatus(long candidateApplicationId, ApplicationStatusTypes newStatus);

		/// <summary>
		/// Gets the flagged candidates.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<ResumeView> GetFlaggedCandidates(FlaggedCandidateCriteria criteria);

		/// <summary>
		/// Gets the applicant resume views.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<ResumeView> GetApplicantResumeViews(ResumeViewCriteria criteria);

		/// <summary>
		/// Gets the invitees for job.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns>PagedList&lt;ResumeView&gt;.</returns>
		PagedList<ResumeView> GetInviteesForJob(InviteesResumeViewCriteria criteria);

		/// <summary>
		/// Determines whether [is candidate existing job invitee] [the specified job identifier].
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="candidateId">The candidate identifier.</param>
		/// <returns></returns>
		ExistingApplicant IsCandidateExistingJobInviteeOrApplicant(long jobId, long candidateId);

		/// <summary>
		/// Gets the application.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		ApplicationDto GetApplication(long personId, long jobId);

    /// <summary>
    /// Gets the application.
    /// </summary>
    /// <param name="personId">The person id.</param>
    /// <param name="postingId">The posting id.</param>
    /// <returns></returns>
	  ApplicationDto GetApplicationForPosting(long personId, long postingId);

		/// <summary>
		/// Gets the applications for a list of people
		/// </summary>
		/// <param name="personIds">The list of person ids.</param>
		/// <param name="jobId">The job id.</param>
		/// <returns></returns>
		Dictionary<long, ApplicationDto> GetApplication(List<long> personIds, long jobId);

		/// <summary>
		/// Gets the application views for a candidate
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="referralsRequired">The number required.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<ApplicationViewDto> GetApplicationView(long personId, int? referralsRequired = null);

		/// <summary>
		/// Gets the application status log views.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<ApplicationStatusLogViewDto> GetApplicationStatusLogViews(ApplicationStatusLogViewCriteria criteria);

		/// <summary>
		/// Gets the application status log views grouped.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<EmployerJobReferralListItem> GetApplicationStatusLogViewsGrouped(ApplicationStatusLogViewCriteria criteria);

		/// <summary>
		/// Gets the job seeker activity view.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="getStaffReferrals">Whether to populate the Staff Referrals field</param>
		/// <param name="getHighGrowthSectors">Whether to populate the Staff Referrals field</param>
		/// <returns></returns>
		JobSeekerActivityView GetJobSeekerActivityView(JobSeekerCriteria criteria, bool getStaffReferrals = true, bool getHighGrowthSectors = true);

		/// <summary>
		/// Gets the job seeker profile.
		/// </summary>
		/// <param name="jobSeekerId">The job seeker id.</param>
		/// <param name="showContactDetails"></param>
		/// <returns></returns>
		JobSeekerProfileModel GetJobSeekerProfile(long jobSeekerId, bool showContactDetails = false);

		/// <summary>
		/// Gets the job seeker activities.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<JobSeekerActivityActionViewDto> GetJobSeekerActivities(JobSeekerCriteria criteria);

		/// <summary>
		/// Gets the job seeker activity users.
		/// </summary>
		/// <param name="jobSeekerId">The job seeker id.</param>
		/// <returns></returns>
		List<UserView> GetJobSeekerActivityUsers(long jobSeekerId);

		PagedList<JobSeekerDashboardModel> GetCandidatesAndIssuesPagedList(JobSeekerCriteria criteria);

		/// <summary>
		/// Gets the candidate and issues.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		CandidateIssueViewDto GetCandidateAndIssues(long personId);

		/// <summary>
		/// Gets the student alumni issues paged list.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<StudentAlumniIssueViewDto> GetStudentAlumniIssuesPagedList(JobSeekerCriteria criteria);

		/// <summary>
		/// Gets the candidate.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		CandidateViewDto GetCandidate(long personId);

		/// <summary>
		/// Gets the candidate.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PersonDto GetCandidatePerson(long personId);

		/// <summary>
		/// Flags the candidate for follow up.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void FlagCandidateForFollowUp(long personId);

		/// <summary>
		/// Resolves the candidate issues.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="issuesResolved">The issues resolved.</param>
		/// <exception cref="ServiceCallException"></exception>
		void ResolveCandidateIssues(long personId, List<CandidateIssueType> issuesResolved, bool autoresolve = false);

		/// <summary>
		/// Gets the open position jobs.
		/// </summary>
		/// <param name="candidateId">The candidate id.</param>
		/// <param name="employerId">The employer id.</param>
		/// <returns></returns>
		List<OpenPositionMatchesViewDto> GetOpenPositionJobs(long candidateId, long employerId);

		/// <summary>
		/// Ignores person to posting match.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="personId">The person id.</param>
		void IgnorePersonPostingMatch(long postingId, long personId);

		/// <summary>
		/// Allows the person to posting match.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="personId">The person id.</param>
		void AllowPersonPostingMatch(long postingId, long personId);

		/// <summary>
		/// Registers action when a user runs a job search for a job seeker
		/// </summary>
		/// <param name="candidatePersonId">The candidate person id.</param>
		void RegisterFindJobsForSeeker(long candidatePersonId);

		/// <summary>
		/// Registers action when job seeker click how to apply.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="candidatePersonId">The candidate person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void RegisterHowToApply(string lensPostingId, long candidatePersonId);

		/// <summary>
		/// Registers the self referral.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="candidatePersonId">The candidate person id.</param>
		/// <param name="referralActionTypes">action type defaults to ExternalReferral</param>
		/// <returns>
		/// A boolean indicated if the action
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		bool RegisterSelfReferral(long postingId, long candidatePersonId, int referralScore, ActionTypes referralActionTypes = ActionTypes.ExternalReferral);

		/// <summary>
		/// Shows the not what youre looking for.
		/// </summary>
		/// <param name="personId">The person id.</param>
		void ShowNotWhatYoureLookingFor(long personId);

		/// <summary>
		/// Views the job details.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		void ViewJobDetails(long? personId, string lensPostingId);

		/// <summary>
		/// Assigns to staff member.
		/// </summary>
		/// <param name="jobSeekerIds">The job seeker ids.</param>
		/// <param name="personId">The person id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void AssignToStaffMember(List<long> jobSeekerIds, long personId);

	  /// <summary>
	  /// Registers the job posting of interest sent.
	  /// </summary>
	  /// <param name="postingId">The lens posting id.</param>
	  /// <param name="personId">The person unique identifier.</param>
	  /// <param name="score">The matching score.</param>
    /// <param name="createdBy">Current user id</param>
	  /// <param name="isRecommendation">Whether this is a job recommendation from Assist</param>
	  /// <param name="queueRecommendation">Whether the job recommendation should be queued</param>
	  void RegisterJobPostingOfInterestSent(String postingId, long personId, int score, long createdBy, bool isRecommendation = true, bool queueRecommendation = false);

		/// <summary>
		/// Gets the postings of interest received.
		/// </summary>
		/// <param name="numberOfDays">The number of days worth of jobs to get.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<JobPostingOfInterestSentViewDto> GetPostingsOfInterestReceived(int numberOfDays);

		/// <summary>
		/// Gets a postings of interest received for a specific job.
		/// </summary>
		/// <param name="lensPostingId">The number of days worth of jobs to get.</param>
		/// <returns></returns>
		JobPostingOfInterestSentViewDto GetPostingsOfInterestReceived(string lensPostingId);

		/// <summary>
		/// Marks the application viewed.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="candidateId">The candidate id.</param>
		/// <exception cref="ServiceCallException"></exception>
		void MarkApplicationViewed(long jobId, long candidateId);

		/// <summary>
		/// Gets the new applicant referral view paged list.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="excludeVeteranPriorityJobs">Whether to exclude veteran priority jobs if job seeker is not a veteran</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<NewApplicantReferralViewDto> GetNewApplicantReferralViewPagedList(long personId, bool excludeVeteranPriorityJobs = false);

		/// <summary>
		/// Gets the latest survey results for a job seeker.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		JobSeekerSurveyDto GetLatestJobSeekerSurvey(long? personId);

		/// <summary>
		/// Saves the job seeker survey.
		/// </summary>
		/// <param name="survey">The survey.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		JobSeekerSurveyDto SaveJobSeekerSurvey(JobSeekerSurveyDto survey);

		/// <summary>
		/// Gets the referral view list.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="numberOfReferrals">The number of referrals.</param>
		/// <param name="postingId">Restrict to a single posting.</param>
		/// <param name="dayLimit">.</param>
		/// <param name="ignoreVeteranPriorityService">Whether to ignore the veteran priority service.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<ReferralViewDto> GetReferralViewList(long personId, int? numberOfReferrals, long? postingId = null, int? dayLimit = null, bool ignoreVeteranPriorityService = false);

		/// <summary>
		/// Gets the campuses.
		/// </summary>
		/// <returns>A list of all campuses</returns>
		/// <exception cref="ServiceCallException"></exception>
		List<CampusDto> GetCampuses();

		/// <summary>
		/// Gets a specific campus.
		/// </summary>
		/// <param name="campusId">The id of the campus required.</param>
		/// <returns>
		/// The campus
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		CampusDto GetCampus(long campusId);

    /// <summary>
    /// Gets the UI claimant status.
    /// </summary>
    /// <param name="personId">The person id.</param>
    /// <param name="checkExternalRepository">Whether to check the external repository.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
		bool GetUiClaimantStatus(long personId, bool checkExternalRepository = true);

    /// <summary>
    /// Sets the MSFW verified status.
    /// </summary>
    /// <param name="personId">The Id of person</param>
    /// <param name="isVerified">Whether the person is verified as MSFW</param>
    /// <param name="questions">Any questions to be set.</param>
	  void VerifyMSFWStatus(long personId, bool isVerified, List<long> questions = null);

		/// <summary>
		/// Checks if the candidate has been invited to apply for specified job
		/// </summary>
		/// <param name="jobId"></param>
		/// <param name="personId"></param>
		/// <returns></returns>
		bool IsCandidateInvitedForThisJob(long jobId, long personId);
	}

	public class ExistingApplicant
	{
		public bool Exists { get; set; }
		public bool IsApplicationPendingApproval { get; set; }
	}
}
