﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.DataTransferObjects.Report;
using Focus.Core.Models;
using Focus.Core.Models.Report;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface IReportClient
	{
		/// <summary>
		/// Gets the approval queue status report.
		/// </summary>
		/// <returns></returns>
		ApprovalQueueStatusReportView GetApprovalQueueStatusReport();

		/// <summary>
		/// Gets the supply demand report.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
    PagedList<SupplyDemandReportView> GetSupplyDemandReport(SupplyDemandReportCriteria criteria);

    /// <summary>
    /// Gets the supply demand as a datatable.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="supplyDemand">The supply demand.</param>
    /// <returns>
    /// A datatable of supply demand
    /// </returns>
    /// <exception cref="ServiceCallException"></exception>
    ReportDataTableModel GetSupplyDemandDataTable(SupplyDemandReportCriteria criteria, List<SupplyDemandReportView> supplyDemand = null);

    /// <summary>
    /// Gets the job seekers report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    /// <exception cref="ServiceCallException"></exception>
    PagedList<JobSeekersReportView> GetJobSeekersReport(JobSeekersReportCriteria criteria);

		/// <summary>
		/// Gets the job seekers as a datatable.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="jobSeekers">An optional list of job seekers if already read. If left as null, the report is re-run based on the criteria</param>
		/// <returns>A datatable of job seekers</returns>
		ReportDataTableModel GetJobSeekersDataTable(JobSeekersReportCriteria criteria, List<JobSeekersReportView> jobSeekers = null);

		/// <summary>
		/// Gets the job seekers as an export file
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="reportName">The name of the data table required</param>
		/// <param name="exportType">Whether to export as Excel or a PDF</param>
		/// <param name="jobSeekers">An optional list of job seekers if already read. If left as null, the report is re-run based on the criteria</param>
		/// <returns>An export file of job seekers</returns>
		byte[] GetJobSeekersExportBytes(JobSeekersReportCriteria criteria, string reportName, ReportExportType exportType, List<JobSeekersReportView> jobSeekers = null);

		/// <summary>
		/// Gets the job orders report.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<JobOrdersReportView> GetJobOrdersReport(JobOrdersReportCriteria criteria);

		/// <summary>
		/// Gets the job seekers as a datatable.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="jobOrders">The job orders.</param>
		/// <returns>
		/// A datatable of job seekers
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		ReportDataTableModel GetJobOrdersDataTable(JobOrdersReportCriteria criteria, List<JobOrdersReportView> jobOrders = null);

    /// <summary>
		/// Gets the job orders as an export file
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="reportName">The name of the data table required</param>
		/// <param name="exportType">Whether to export as Excel or a PDF</param>
		/// <param name="jobOrders">An optional list of job orders if already read. If left as null, the report is re-run based on the criteria</param>
		/// <returns>An export file of job orders</returns>
		byte[] GetJobOrdersExportBytes(JobOrdersReportCriteria criteria, string reportName, ReportExportType exportType, List<JobOrdersReportView> jobOrders = null);

		/// <summary>
		/// Gets the employers report.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PagedList<EmployersReportView> GetEmployersReport(EmployersReportCriteria criteria);

		/// <summary>
		/// Gets the employers as a datatable.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="employers">The employers.</param>
		/// <returns>
		/// A datatable of employers
		/// </returns>
		/// <exception cref="ServiceCallException"></exception>
		ReportDataTableModel GetEmployersDataTable(EmployersReportCriteria criteria, List<EmployersReportView> employers = null);

		/// <summary>
		/// Gets the employers as an export file
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="reportName">The name of the data table required</param>
		/// <param name="exportType">Whether to export as Excel or a PDF</param>
		/// <param name="employers">An optional list of employers if already read. If left as null, the report is re-run based on the criteria</param>
		/// <returns>An export file of employers</returns>
		byte[] GetEmployersExportBytes(EmployersReportCriteria criteria, string reportName, ReportExportType exportType, List<EmployersReportView> employers = null);

		/// <summary>
		/// Runs a report and returns an export file
		/// </summary>
		/// <param name="criteria">The criteria to run the report.</param>
		/// <param name="reportName">The name of the data table required</param>
		/// <param name="exportType">Whether to export as Excel or a PDF</param>
		/// <returns>The export file of job seekers</returns>
		byte[] GetReportExportBytes(IReportCriteria criteria, string reportName, ReportExportType exportType);

		/// <summary>
		/// Converts a data table for a report into a file for export
		/// </summary>
		/// <param name="reportData">The report data table</param>
		/// <param name="criteriaDisplay">A list of criteria to display in the export file</param>
		/// <param name="reportName">The name of the data table required</param>
		/// <param name="exportType">Whether to export as Excel or a PDF</param>
		/// <returns>The export file of job seekers</returns>
		byte[] ConvertDataTableToExportBytes(ReportDataTableModel reportData, List<string> criteriaDisplay, string reportName, ReportExportType exportType);

		/// <summary>
		/// Saves the report.
		/// </summary>
		/// <param name="reportName">Name of the report.</param>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <param name="displayOnDashboard">Add to dashboard flag </param>
		void SaveReport(string reportName, IReportCriteria reportCriteria, bool displayOnDashboard);

		/// <summary>
		/// Saves the report to a session state
		/// </summary>
		/// <param name="reportCriteria">The report criteria.</param>
		long SaveReportToSession(IReportCriteria reportCriteria);

		/// <summary>
		/// Gets the saved report
		/// </summary>
		/// <param name="reportId">The id for the saved report</param>
		IReportCriteria GetSavedReport(long reportId);

		/// <summary>
		/// Gets the report from session state
		/// </summary>
		/// <param name="reportId">The id for the saved report</param>
		IReportCriteria GetSavedReportFromSession(long reportId);

		/// <summary>
		/// Gets all saved reports for a user
		/// </summary>
		/// <param name="dashboardReports">Whether to get only dashboard report, non-dashboard reports, or both</param>
		List<SavedReportDto> GetSavedReportList(bool? dashboardReports = null);

		/// <summary>
		/// Deletes a report
		/// </summary>
		/// <param name="reportId">The id of the report to delete</param>
		void DeleteReport(long reportId);

		/// <summary>
		/// Gets the reporting look ups.
		/// </summary>
		/// <param name="lookUp">The look up.</param>
		/// <param name="searchString">The search string.</param>
		/// <returns>A list of matching string for the lookup type</returns>
		string[] GetReportLookUpData(ReportLookUpType lookUp, string searchString);

		/// <summary>
		/// Gets the reporting offices
		/// </summary>
		/// <param name="reportType">The type of report.</param>
		/// <returns>A list of offices for the type</returns>
		List<ReportOffice> GetReportOfficeLookup(ReportType reportType);

		/// <summary>
		/// Gets the statistics.
		/// </summary>
		/// <param name="statisticTypes">The statistic types.</param>
		/// <param name="statisticDate">The statistic date.</param>
		/// <returns></returns>
		List<KeyValuePair<StatisticType, string>> GetStatistics(List<StatisticType> statisticTypes, DateTime statisticDate);

    /// <summary>
    /// Generates the employer activity report.
    /// </summary>
    /// <param name="reportType">Type of the report.</param>
    /// <param name="fromDate">From date.</param>
    /// <param name="toDate">To date.</param>
    /// <param name="recipients">The recipients.</param>
    /// <returns></returns>
		ActivityReportValidationModel GenerateActivityReport(AsyncReportType reportType, DateTime fromDate, DateTime toDate, string recipients);
	}
}
