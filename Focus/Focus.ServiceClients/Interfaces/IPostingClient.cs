﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.SpideredPosting;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Assist;
using Focus.Core.Models.Career;
using Framework.Exceptions;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface IPostingClient
	{
		/// <summary>
		/// Gets the job posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		PostingEnvelope GetJobPosting(string lensPostingId, bool logViewAction = false);

		/// <summary>
		/// Closes the job posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool CloseJobPosting(string lensPostingId);

		/// <summary>
		/// Gets the typical resumes.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="posting">The posting.</param>
		/// <returns></returns>
		List<ResumeInfo> GetTypicalResumes(string lensPostingId, PostingEnvelope posting);

		/// <summary>
		/// Gets the recently viewed postings.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<ViewedPostingModel> GetViewedPostings();

		/// <summary>
		/// Adds the viewed posting.
		/// </summary>
		/// <param name="viewedPosting">The viewed posting.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		bool AddViewedPosting(ViewedPostingDto viewedPosting);

		/// <summary>
		/// Gets the job id from posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		long? GetJobIdFromPosting(string lensPostingId);

		/// <summary>
		/// Gets the compare posting to resume modal model.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="doLiveSearch">if set to <c>true</c> [do live search].</param>
		/// <returns></returns>
		ComparePostingToResumeModalModel GetComparePostingToResumeModalModel(string lensPostingId, bool doLiveSearch);

		/// <summary>
		/// Gets the posting employer contact model.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		PostingEmployerContactModel GetPostingEmployerContactModel(long postingId, long? personId);

		/// <summary>
		/// Gets the spidered postings.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<SpideredPostingModel> GetSpideredPostings(SpideredPostingCriteria criteria);

		/// <summary>
		/// Gets the spidered postings with good matches.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<SpideredPostingModel> GetSpideredPostingsWithGoodMatches(SpideredPostingCriteria criteria);

		/// <summary>
		/// Gets the posting id. If posting not in database retrieves it from document store
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <returns></returns>
		long? GetPostingId(string lensPostingId);

		/// <summary>
		/// Gets the posting BGT occ code.
		/// </summary>
		/// <param name="lensPostingId">The lens posting identifier.</param>
		/// <returns></returns>
		string GetPostingBGTOccs(string lensPostingId);
	}
}
