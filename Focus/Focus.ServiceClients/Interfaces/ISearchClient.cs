﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Criteria.SavedSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Web.Core.Models;
using Framework.Exceptions;

#endregion



namespace Focus.ServiceClients.Interfaces
{
	public interface ISearchClient
	{
		/// <summary>
		/// Searches the candidates.
		/// </summary>
		/// <param name="searchId">The search id.</param>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <returns></returns>
		CandidateSearchResultModel SearchCandidates(Guid searchId, CandidateSearchCriteria searchCriteria);

		/// <summary>
		/// Gets the candidate saved search.
		/// </summary>
		/// <param name="searchId">The search id.</param>
		/// <returns></returns>
		CandidateSavedSearchView GetCandidateSavedSearch(long searchId);

		/// <summary>
		/// Gets the candidate saved search.
		/// </summary>
		/// <returns></returns>
		PagedList<SavedSearchDto> GetCandidateSavedSearch(SavedSearchCriteria criteria);

		/// <summary>
		/// Deletes candidate saved search.
		/// </summary>
		/// <returns></returns>
		void DeleteCandidateSavedSearch(long searchId);

		/// <summary>
		/// Gets the candidate saved search view.
		/// </summary>
		/// <returns></returns>
		List<SavedSearchView> GetCandidateSavedSearchView();

		/// <summary>
		/// Saves the candidate search.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <param name="id">The id.</param>
		/// <param name="name">The name.</param>
		/// <param name="alertEmailRequired">if set to <c>true</c> [alert email required].</param>
		/// <param name="alertEmailFrequency">The alert email frequency.</param>
		/// <param name="alertEmailFormat">The alert email format.</param>
		/// <param name="alertEmailAddress">The alert email address.</param>
		void SaveCandidateSearch(CandidateSearchCriteria searchCriteria, long id, string name, bool alertEmailRequired,
			EmailAlertFrequencies alertEmailFrequency, EmailFormats alertEmailFormat, string alertEmailAddress);

		/// <summary>
		/// Extracts the job description.
		/// </summary>
		/// <param name="binaryFileData">The binary file data.</param>
		/// <param name="fileName">Name of the file.</param>
		/// <param name="contentType">Type of the content.</param>
		/// <returns></returns>
		string ExtractJobDescription(byte[] binaryFileData, string fileName, string contentType);

		/// <summary>
		/// Clarifies the text.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		List<string> ClarifyText(string text, int minScore, int maxSearchCount, string type);

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="jobTasksOnly">Whether to only get onets with job tasks available.</param>
		/// <param name="listSize">The number to get.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<OnetDetailsView> GetOnets(bool jobTasksOnly, int? listSize = null);

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="jobFamilyId">The job family id.</param>
		/// <returns></returns>
		List<OnetDetailsView> GetOnets(long jobFamilyId);

		/// <summary>
		/// Gets the R onet code by id.
		/// </summary>
		/// <param name="ronetId">The ronet id.</param>
		/// <returns></returns>
		string GetROnetCodeById(long ronetId);

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		List<OnetDetailsView> GetOnets(string jobTitle, int listSize);

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		List<ROnetDetailsView> GetROnets(string jobTitle, int listSize);

		/// <summary>
		/// Gets the onet for an ronet.
		/// </summary>
		/// <returns></returns>
		long? GetROnetOnet(long rOnetId);

		/// <summary>
		/// Gets the ronetId for an ronet code.
		/// </summary>
		/// <returns></returns>
		long? GetROnetByCode(string ronetCode);

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		List<OnetDetailsView> GetOnets(long onetId, string jobTitle, int listSize);

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		List<ROnetDetailsView> GetROnets(long onetId, string jobTitle, int listSize);

		/// <summary>
		/// Gets the onet.
		/// </summary>
		/// <param name="onetId"></param>
		/// <returns></returns>
		long? GetOnet(long onetId);

		/// <summary>
		/// Gets the onet for an ronet.
		/// </summary>
		/// <returns></returns>
		long? GetOnetByROnet(long rOnetId);

		/// <summary>
		/// Gets the onet for an ronet.
		/// </summary>
		/// <returns></returns>
		long? GetROnetByOnet(long onetId);

		/// <summary>
		/// Gets the search results.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <param name="count">The count.</param>
		/// <param name="manualSearch">if set to <c>true</c> [manual search].</param>
		/// <returns></returns>
		List<PostingSearchResultView> GetSearchResults(SearchCriteria searchCriteria, int? count = null, bool manualSearch = true);

		/// <summary>
		/// Gets the more jobs like this.
		/// </summary>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <returns></returns>
		List<PostingSearchResultView> GetMoreJobsLikeThis(SearchCriteria searchCriteria);

		/// <summary>
		/// Gets the onet.
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <returns></returns>
		List<string> GetOnetKeywords(string onetCode);

		/// <summary>
		/// Gets the onet tasks.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <returns></returns>
		List<OnetTaskView> GetOnetTasks(long onetId);

		/// <summary>
		/// Gets the resume search session.
		/// </summary>
		/// <param name="searchId">The search identifier.</param>
		/// <param name="starRating">The star rating.</param>
		/// <returns></returns>
		/// <exception cref="ServiceCallException"></exception>
		List<ResumeView> GetResumeSearchSession(Guid searchId, int? starRating);
	}
}
