﻿






CREATE VIEW [dbo].[Data.Application.AssistUserActivityView]
AS

SELECT	
	ActionEvent.Id AS Id,
	[User].Id AS UserId,
	ActionType.Name AS ActionName,
	ActionEvent.ActionedOn
FROM dbo.[Data.Application.User] AS [User] WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK) ON [User].Id = ActionEvent.UserId 
	INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id
WHERE    ((ActionType.Name = 'PostJob') OR
                      (ActionType.Name = 'CreateCandidateApplication') OR
                      (ActionType.Name = 'CloseJob') OR
                      (ActionType.Name = 'RefreshJob') OR
                      (ActionType.Name = 'ReactivateJob') OR
                      (ActionType.Name = 'CreateJob') OR                     
                      (ActionType.Name = 'EditJob') OR                     
                      (ActionType.Name = 'HoldJob'))






