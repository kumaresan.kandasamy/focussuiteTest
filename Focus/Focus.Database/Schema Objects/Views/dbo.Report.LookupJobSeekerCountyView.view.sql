﻿


CREATE VIEW [dbo].[Report.LookupJobSeekerCountyView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.County
	FROM
	(
		SELECT DISTINCT
			County
		FROM 
			[Report.JobSeeker]
	) Data

