﻿
CREATE VIEW [dbo].[Data.Application.JobSeekerProfileView]
AS
	SELECT	
		Person.[Id],
		Person.FirstName,
		Person.MiddleInitial, 
		Person.LastName, 
		Person.DateOfBirth, 
		SocialSecurityNumber, 
		Person.EmailAddress, 
		[Resume].AddressLine1,
		[Resume].TownCity, 
		[Resume].StateId,
		[Resume].CountryId,
		[Resume].PostcodeZip, 
		[Resume].HomePhone,
		[Resume].[Id] AS DefaultResumeId,
		[Resume].[IsSearchable] as IsSearchable,
		[User].CreatedOn,
		[User].ExternalId AS ClientId,
		Person.EnrollmentStatus,
		[User].Id AS UserId,
		Person.MigrantSeasonalFarmWorkerVerified,
		[User].[Enabled]
	FROM 
		[dbo].[Data.Application.Person] AS Person WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.User] AS [User] 
		ON [User].PersonId = Person.Id
	LEFT OUTER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) 
		ON [Resume].PersonId = Person.Id 
		AND [Resume].IsDefault = 1
