﻿

CREATE VIEW [dbo].[Data.Application.OpenPositionMatchesView]
AS
SELECT 
	 Cast(Row_Number() OVER (ORDER BY j.Id) AS bigint) AS Id,
	 j.Id AS JobId,
	 j.JobTitle, 
	 pp.Id AS OpenPositionMatchId,
	 pp.Score AS JobScore, 
	 pp.PersonId AS CandidateId,
	 p.FirstName,
	 p.LastName,	 
	 bu.Id AS BusinessUnitId,
	 bu.Name AS EmployerName,
	 po.Id AS PostingId,
	 j.EmployerId AS EmployerId
FROM  
	dbo.[Data.Application.Job] AS j WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Posting] AS po WITH (NOLOCK) ON j.Id = po.JobId
	INNER JOIN dbo.[Data.Application.PersonPostingMatch] AS pp WITH (NOLOCK) ON pp.PostingId = po.Id
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON p.Id = pp.PersonId
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS bu  WITH (NOLOCK) ON bu.Id = j.BusinessUnitId
WHERE 
	j.JobStatus = 1



