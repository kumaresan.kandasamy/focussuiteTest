﻿






CREATE VIEW [dbo].[Library.JobTaskView]
AS

SELECT
	jt.[Id] AS [Id],
	jtli.[PrimaryValue] AS Prompt,
	jtli.[SecondaryValue] AS Response,
	jt.[JobTaskType] AS JobTaskType,
	jt.[OnetId],
	li.Culture AS Culture,
	jt.Scope AS Scope,
	jt.[Certificate] AS [Certificate],
	lo.OnetCode
FROM
	[Library.JobTask] jt WITH (NOLOCK) 
	INNER JOIN [Library.JobTaskLocalisationItem] jtli WITH (NOLOCK) ON jt.[Key] = jtli.[Key]
	INNER JOIN [Library.Localisation] li WITH (NOLOCK) ON jtli.[LocalisationId] = li.[Id]
	INNER JOIN [Library.Onet] lo WITH (NOLOCK) ON jt.OnetId = lo.Id




