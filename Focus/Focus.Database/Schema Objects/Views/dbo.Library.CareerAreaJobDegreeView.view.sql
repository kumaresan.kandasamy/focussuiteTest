﻿create view [Library.CareerAreaJobDegreeView]
as
select
	NEWID() as Id,
	ca.Id as CareerAreaId,
	jca.JobId as JobId,
	del.Id as DegreeEducationLevelId,
	del.EducationLevel as EducationLevel,
	d.Id as DegreeId,
	d.IsClientData as IsClientData,
	d.ClientDataTag as ClientDataTag
from
	[Library.CareerArea] ca
inner join [Library.JobCareerArea] jca
	on ca.Id = jca.CareerAreaId
inner join [Library.JobDegreeEducationLevel] jdel
	on jca.JobId = jdel.JobId
inner join [Library.DegreeEducationLevel] del
	on jdel.DegreeEducationLevelId = del.Id
inner join [Library.Degree] d
	on del.DegreeId = d.Id