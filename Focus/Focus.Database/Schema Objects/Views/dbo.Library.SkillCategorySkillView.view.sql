﻿
CREATE VIEW [dbo].[Library.SkillCategorySkillView]
AS
SELECT
	SkillCategorySkill.Id,
	SkillCategorySkill.SkillCategoryId,
	SkillCategory.Name AS SkillCategoryName,
	SkillCategory.ParentId AS SkillCategoryParentId,
	SkillCategorySkill.SkillId,
	Skill.SkillType,
	Skill.Name AS SkillName
FROM
	[Library.SkillCategorySkill] AS SkillCategorySkill WITH (NOLOCK)
	INNER JOIN [Library.SkillCategory] AS SkillCategory WITH (NOLOCK) ON SkillCategorySkill.SkillCategoryId = SkillCategory.Id
	INNER JOIN [Library.Skill] AS Skill WITH (NOLOCK) ON SkillCategorySkill.SkillId = Skill.Id

