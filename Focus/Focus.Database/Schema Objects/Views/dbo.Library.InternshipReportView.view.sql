﻿
CREATE VIEW [dbo].[Library.InternshipReportView]
AS
SELECT
	InternshipReport.Id,
	InternshipReport.InternshipCategoryId,
	InternshipCategory.Name,
	InternshipReport.StateAreaId,
	InternshipReport.NumberAvailable
FROM
	[Library.InternshipReport] AS InternshipReport WITH (NOLOCK)
	INNER JOIN [Library.InternshipCategory] AS InternshipCategory WITH (NOLOCK) ON InternshipReport.InternshipCategoryId = InternshipCategory.Id

