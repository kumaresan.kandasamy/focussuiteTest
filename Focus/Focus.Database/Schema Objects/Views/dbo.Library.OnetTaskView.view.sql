﻿
CREATE VIEW [dbo].[Library.OnetTaskView]
AS
SELECT 
	ot.Id,
	ot.OnetId,
	ot.[Key] AS TaskKey,
	oli.PrimaryValue AS Task,
	oli.SecondaryValue AS TaskPastTense,
	l.Culture AS Culture
FROM  
	dbo.[Library.OnetTask] AS ot WITH (NOLOCK)
	INNER JOIN  dbo.[Library.OnetLocalisationItem] AS oli WITH (NOLOCK) ON ot.[Key] = oli.[Key] 
	INNER JOIN [Library.Localisation] AS l WITH (NOLOCK) ON oli.LocalisationId = l.Id
	
