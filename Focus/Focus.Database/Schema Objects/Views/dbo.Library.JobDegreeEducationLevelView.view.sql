﻿


CREATE VIEW [dbo].[Library.JobDegreeEducationLevelView]  
AS  
SELECT  
 JobDegreeEducationLevel.Id,  
 JobDegreeEducationLevel.JobId,  
 JobDegreeEducationLevel.DegreeEducationLevelId,  
 JobDegreeEducationLevel.ExcludeFromJob,  
 DegreeEducationLevel.EducationLevel,  
 DegreeEducationLevel.Name AS DegreeEducationLevelName,  
 DegreeEducationLevel.DegreeId,  
 Degree.Name AS DegreeName,  
 Degree.IsDegreeArea,  
 DegreeEducationLevel.DegreesAwarded,  
 Degree.IsClientData,  
 Degree.ClientDataTag,
 JobDegreeEducationLevel.Tier,
 Job.ROnet
FROM  
 [Library.JobDegreeEducationLevel] AS JobDegreeEducationLevel WITH (NOLOCK)  
 INNER JOIN [Library.DegreeEducationLevel] AS DegreeEducationLevel WITH (NOLOCK) ON JobDegreeEducationLevel.DegreeEducationLevelId = DegreeEducationLevel.Id  
 INNER JOIN [Library.Degree] AS Degree WITH (NOLOCK) ON DegreeEducationLevel.DegreeId = Degree.Id
 INNER JOIN [Library.Job] AS Job WITH (NOLOCK) ON JobDegreeEducationLevel.JobId = Job.Id


