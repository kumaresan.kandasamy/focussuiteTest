﻿


CREATE VIEW [dbo].[Library.StudyPlaceDegreeEducationLevelView]
AS
SELECT
	StudyPlaceDegreeEducationLevel.Id,
	StudyPlace.Id AS StudyPlaceId,
	StudyPlace.Name AS StudyPlaceName,
	StudyPlace.StateAreaId,
	DegreeEducationLevel.Id AS DegreeEducationLevelId,
	DegreeEducationLevel.Name AS DegreeEducationLevelName,
	DegreeEducationLevel.DegreeId,
	Degree.Name AS DegreeName,
	Degree.IsClientData,
	Degree.ClientDataTag
FROM
	[Library.StudyPlaceDegreeEducationLevel] AS StudyPlaceDegreeEducationLevel WITH (NOLOCK)
	INNER JOIN [Library.StudyPlace] AS StudyPlace WITH (NOLOCK) ON StudyPlaceDegreeEducationLevel.StudyPlaceId = StudyPlace.Id
	INNER JOIN [Library.DegreeEducationLevel] AS DegreeEducationLevel WITH (NOLOCK) ON StudyPlaceDegreeEducationLevel.DegreeEducationLevelId = DegreeEducationLevel.Id
	INNER JOIN [Library.Degree] AS Degree WITH (NOLOCK) ON DegreeEducationLevel.DegreeId = Degree.Id



