﻿

CREATE VIEW  [dbo].[Data.Application.BusinessUnitSearchView] as

select
bu.Id,
bu.Name as BusinessUnitName,
bua.Line1 as AddressLine1,
bua.PostcodeZip,
e.Id as EmployerId
from
	[data.application.businessunitaddress] bua
inner join [data.application.businessunit] bu on bua.businessunitid = bu.Id
inner join [data.application.employer] e on bu.EmployerId = e.Id
where e.ApprovalStatus <> 3

