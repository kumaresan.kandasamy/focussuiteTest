﻿

CREATE VIEW [dbo].[Library.JobDegreeEducationLevelReportView]
AS
SELECT
	JobDegreeEducationLevelReport.Id,
	JobDegreeEducationLevelReport.StateAreaId,
	JobDegreeEducationLevelReport.JobDegreeEducationLevelId,
	JobDegreeEducationLevelReport.HiringDemand,
	JobDegreeEducationLevelReport.DegreesAwarded,
	JobDegreeEducationLevel.JobId,
	JobDegreeEducationLevel.DegreeEducationLevelId,
	JobDegreeEducationLevel.ExcludeFromJob,
	DegreeEducationLevel.EducationLevel,
	DegreeEducationLevel.Name AS DegreeEducationLevelName,
	DegreeEducationLevel.DegreeId,
	DegreeEducationLevel.ExcludeFromReport,
	Degree.Name AS DegreeName,
	Degree.IsDegreeArea,
	Degree.IsClientData,
	Degree.ClientDataTag
FROM
	[Library.JobDegreeEducationLevelReport] AS JobDegreeEducationLevelReport WITH (NOLOCK)
	INNER JOIN [Library.JobDegreeEducationLevel] AS JobDegreeEducationLevel WITH (NOLOCK) ON JobDegreeEducationLevelReport.JobDegreeEducationLevelId = JobDegreeEducationLevel.Id
	INNER JOIN [Library.DegreeEducationLevel] AS DegreeEducationLevel WITH (NOLOCK) ON JobDegreeEducationLevel.DegreeEducationLevelId = DegreeEducationLevel.Id
	INNER JOIN [Library.Degree] AS Degree WITH (NOLOCK) ON DegreeEducationLevel.DegreeId = Degree.Id
	



