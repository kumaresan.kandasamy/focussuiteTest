﻿
 CREATE VIEW [dbo].[Library.InternshipSkillView]  
AS  
SELECT  
 InternshipSkill.Id,  
 InternshipSkill.InternshipCategoryId,  
 InternshipSkill.SkillId,  
 Skill.SkillType,  
 Skill.Name AS SkillName,  
 InternshipSkill.[Rank],
 InternshipCategory.Name AS InternshipCategoryName  
FROM  
 [Library.InternshipSkill] AS InternshipSkill WITH (NOLOCK)  
 INNER JOIN [Library.Skill] AS Skill WITH (NOLOCK) ON InternshipSkill.SkillId = Skill.Id
 INNER JOIN [Library.InternshipCategory] AS InternshipCategory WITH (NOLOCK) ON InternshipSkill.InternshipCategoryId = InternshipCategory.Id
