﻿


CREATE VIEW [dbo].[Report.LookupJobOrderTitleView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.JobTitle
	FROM
	(
		SELECT DISTINCT
			JobTitle
		FROM 
			[Report.JobOrder]
	) Data

