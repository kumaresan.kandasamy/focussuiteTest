﻿
CREATE VIEW [dbo].[Library.JobJobTitleSkillView]
AS
SELECT DISTINCT
	JobJobTitle.JobTitleId AS Id,
	JobSkill.SkillId
FROM
	[Library.JobJobTitle] AS JobJobTitle WITH (NOLOCK)
	INNER JOIN [Library.JobSkill] AS JobSkill WITH (NOLOCK) ON JobJobTitle.JobId = JobSkill.JobId
WHERE
	JobSkill.JobSkillType = 1

