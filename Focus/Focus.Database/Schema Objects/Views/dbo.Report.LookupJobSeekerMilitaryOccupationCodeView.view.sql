﻿


CREATE VIEW [dbo].[Report.LookupJobSeekerMilitaryOccupationCodeView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.MilitaryOccupationCode
	FROM
	(
		SELECT DISTINCT
			[Description] AS MilitaryOccupationCode
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 7
	) Data

