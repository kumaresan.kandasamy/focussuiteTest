﻿
CREATE VIEW [dbo].[Library.JobReportView]
AS
SELECT 
	JobReport.Id,
	JobReport.JobId,
	Job.Name,
	Job.DegreeIntroStatement,
	JobReport.StateAreaId,
	JobReport.HiringTrend,
	JobReport.HiringTrendPercentile,
	JobReport.HiringTrendSubLevel, 
	JobReport.HiringDemand,
	JobReport.GrowthTrend,
	JobReport.GrowthPercentile,
	JobReport.SalaryTrend,
	JobReport.SalaryTrendPercentile,
	JobReport.SalaryTrendAverage,
	JobReport.SalaryTrendMin,
	JobReport.SalaryTrendMax,
	JobReport.SalaryTrendRealtime,
	JobReport.SalaryTrendRealtimeAverage,
	JobReport.SalaryTrendRealtimeMin,
	JobReport.SalaryTrendRealtimeMax,
	Job.ROnet,
	Job.StarterJob
FROM
	[Library.JobReport] AS JobReport WITH (NOLOCK)
	INNER JOIN [Library.Job] AS Job WITH (NOLOCK) ON JobReport.JobId = Job.Id
