﻿


CREATE VIEW [dbo].[Config.LookupItemsView]
AS
	
SELECT 
	ci.Id AS Id, 
	ci.[Key] AS [Key],
	cg.[Key] AS LookupType, 
	li.Value AS Value, 
	cgi.DisplayOrder AS DisplayOrder, 
	ISNULL(cip.Id,0) AS ParentID, 
	cip.[Key] AS ParentKey,
	l.Culture AS Culture,
	ci.ExternalId AS ExternalId,
	ci.CustomFilterId AS CustomFilterId
FROM 
	[Config.CodeGroup] cg WITH (NOLOCK)
	INNER JOIN [Config.CodeGroupItem] cgi WITH (NOLOCK) ON cg.Id = cgi.CodeGroupId 
	INNER JOIN [Config.CodeItem] ci WITH (NOLOCK) ON cgi.CodeItemId = ci.Id
	INNER JOIN [Config.LocalisationItem] li WITH (NOLOCK) ON ci.[Key] = li.[Key]
	INNER JOIN [Config.Localisation] l WITH (NOLOCK) ON li.LocalisationId = l.id
	LEFT OUTER JOIN [Config.CodeItem] AS cip WITH (NOLOCK) ON ci.ParentKey = cip.[Key]




