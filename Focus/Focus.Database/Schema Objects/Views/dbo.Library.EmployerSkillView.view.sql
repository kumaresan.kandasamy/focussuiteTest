﻿
CREATE VIEW [dbo].[Library.EmployerSkillView]
AS
SELECT
	EmployerSkill.Id,
	EmployerSkill.EmployerId,
	EmployerSkill.SkillId,
	Skill.SkillType,
	Skill.Name AS SkillName,
	EmployerSkill.Postings
FROM
	[Library.EmployerSkill] AS EmployerSkill WITH (NOLOCK)
	INNER JOIN [Library.Skill] AS Skill WITH (NOLOCK) ON EmployerSkill.SkillId = Skill.Id

