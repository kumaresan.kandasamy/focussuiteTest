﻿





CREATE VIEW [dbo].[Data.Application.CandidateNoteView]
AS

SELECT   
 PersonNote.Id AS Id,  
 PersonNote.Note AS Note,  
 PersonNote.EmployerId AS EmployerId,  
 PersonNote.PersonId AS CandidateId,  
 PersonNote.DateAdded AS NoteAddedDateTime,
 Employer.Name AS EmployerName  
FROM   
 [Data.Application.PersonNote] AS PersonNote WITH (NOLOCK)  
 INNER JOIN [Data.Application.Employer] AS Employer WITH (NOLOCK) ON PersonNote.EmployerID = Employer.ID








