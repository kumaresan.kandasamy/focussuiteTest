﻿
CREATE view  [dbo].[Data.Application.EmployerSearchView] as

select
e.Id,
e.Name as CompanyName,
ea.Line1 as AddressLine1,
ea.PostcodeZip
from
	[data.application.employeraddress] ea
inner join [data.application.employer] e on ea.employerid = e.Id
where e.ApprovalStatus <> 3
