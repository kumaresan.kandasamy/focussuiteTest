﻿
CREATE VIEW [dbo].[Library.SkillReportView]
AS
SELECT
	SkillReport.Id,
	SkillReport.SkillId,
	Skill.Name AS SkillName,
	Skill.SkillType,
	SkillReport.StateAreaId,
	SkillReport.Frequency
FROM
	[Library.SkillReport] AS SkillReport WITH (NOLOCK)
	INNER JOIN [Library.Skill] AS Skill WITH (NOLOCK) ON SkillReport.SkillId = Skill.Id

