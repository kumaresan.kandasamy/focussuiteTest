﻿





  
  
CREATE VIEW [dbo].[Data.Application.ViewedResumeView]  
AS  
  
SELECT 
	ActionEvent.Id,
	ActionEvent.ActionedOn,
	[Resume].Id AS ResumeId,
	[Resume].PersonId AS ResumePersonId,
	ActionEvent.EntityIdAdditional01 AS ViewerUserId,
	[User].UserType AS ViewerUserType
FROM [Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK)
	INNER JOIN [Data.Core.ActionType] AS ActionType WITH (NOLOCK)ON ActionEvent.ActionTypeId = ActionType.Id
	INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON ActionEvent.EntityId = [Resume].Id
	INNER JOIN [Data.Application.User] AS [User] WITH (NOLOCK) ON ActionEvent.EntityIdAdditional01 = [User].Id
WHERE ActionType.Name = 'ViewResume'





