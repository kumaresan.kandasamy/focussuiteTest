﻿

CREATE VIEW [dbo].[Library.ProgramAreaDegreeView]
AS
SELECT
	PAD.Id AS Id,
	PA.Id AS ProgramAreaId,
	PA.Name AS ProgramAreaName,
	D.Id AS DegreeId,
	D.Name AS DegreeName,
	D.IsClientData,
	D.ClientDataTag	
FROM
	dbo.[Library.ProgramArea] PA
	INNER JOIN dbo.[Library.ProgramAreaDegree] PAD ON PAD.ProgramAreaId = PA.Id
	INNER JOIN dbo.[Library.Degree] D ON D.Id = PAD.DegreeId



