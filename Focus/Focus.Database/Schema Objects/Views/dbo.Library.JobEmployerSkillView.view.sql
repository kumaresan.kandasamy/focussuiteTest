﻿

CREATE VIEW [dbo].[Library.JobEmployerSkillView]
AS
SELECT
	JobEmployerSkill.Id,
	JobEmployerSkill.JobId,
	JobEmployerSkill.EmployerId,
	JobEmployerSkill.SkillId,
	Skill.SkillType,
	Skill.Name AS SkillName,
	JobEmployerSkill.SkillCount
FROM
	[Library.JobEmployerSkill] AS JobEmployerSkill WITH (NOLOCK)
	INNER JOIN [Library.Skill] AS Skill WITH (NOLOCK) ON JobEmployerSkill.SkillId = Skill.Id


