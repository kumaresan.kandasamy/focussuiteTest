﻿
CREATE VIEW [dbo].[Library.JobStateAreaView]
AS
SELECT DISTINCT
	Job.Id,
	Job.Name,
	JobReport.StateAreaId
FROM
	[Library.JobReport] AS JobReport WITH (NOLOCK)
	INNER JOIN [Library.Job] AS Job WITH (NOLOCK) ON JobReport.JobId = Job.Id

