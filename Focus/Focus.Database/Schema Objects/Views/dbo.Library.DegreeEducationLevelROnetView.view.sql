﻿


CREATE VIEW [dbo].[Library.DegreeEducationLevelROnetView]
AS

SELECT 
	ROW_NUMBER() OVER (ORDER BY JobDegreeEducationLevel.DegreeEducationLevelId) AS Id, 
	JobDegreeEducationLevel.DegreeEducationLevelId,
	Job.ROnet,
	ProgramAreaDegree.ProgramAreaId
FROM
	[Library.JobDegreeEducationLevel] AS JobDegreeEducationLevel
	INNER JOIN [Library.Job] AS Job ON JobDegreeEducationLevel.JobId = Job.Id
	INNER JOIN [Library.DegreeEducationLevel] AS DegreeEducationLevel ON JobDegreeEducationLevel.DegreeEducationLevelId = DegreeEducationLevel.Id
	LEFT OUTER JOIN [Library.ProgramAreaDegree] AS ProgramAreaDegree ON DegreeEducationLevel.DegreeId = ProgramAreaDegree.DegreeId
WHERE 
	JobDegreeEducationLevel.ExcludeFromJob = 0 



