﻿CREATE VIEW [Library.SkillsForJobsView]
AS
SELECT
	Skill.Id,
	Skill.[Name],
	Skill.SkillType,
	MIN(JobSkill.[Rank]) AS [Rank],
	MAX(JobSkill.DemandPercentile) AS DemandPercentile
FROM
	dbo.[Library.Skill] Skill WITH (NOLOCK)
INNER JOIN dbo.[Library.JobSkill] JobSkill WITH (NOLOCK)
	ON Skill.Id = JobSkill.SkillId
GROUP BY
	Skill.Id,
	Skill.[Name],
	Skill.SkillType