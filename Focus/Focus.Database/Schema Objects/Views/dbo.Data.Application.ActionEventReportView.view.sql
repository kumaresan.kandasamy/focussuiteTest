﻿
CREATE VIEW [Data.Application.ActionEventReportView] 
AS
	SELECT
		AE.Id,
		AE.ActionTypeId,
		AE.ActionedOn,
		U.Id AS UserId,
		U.PersonId,
		U.ExternalId,
		U.UserType,
		O.OfficeId,
		O.OfficeName
	FROM
		[Data.Core.ActionEvent] AE
	INNER JOIN [Data.Application.User] U
		ON U.Id = AE.UserId
	OUTER APPLY
	(
		SELECT TOP 1
			O.Id AS OfficeId,
			O.OfficeName
		FROM
			[Data.Application.PersonsCurrentOffice] PCO
		INNER JOIN [Data.Application.Office] O
			ON O.Id = PCO.OfficeId
		WHERE
			PCO.PersonId = U.PersonId
			AND PCO.StartTime <= AE.ActionedOn
		ORDER BY
			PCO.StartTime DESC
	) O
