﻿
CREATE VIEW [dbo].[Library.JobCareerAreaSkillView]
AS
SELECT DISTINCT
	JobCareerArea.CareerAreaId AS Id,
	JobSkill.SkillId
FROM
	[Library.JobCareerArea] AS JobCareerArea WITH (NOLOCK)
	INNER JOIN [Library.JobSkill] AS JobSkill WITH (NOLOCK) ON JobCareerArea.JobId = JobSkill.JobId
WHERE
	JobSkill.JobSkillType = 1

