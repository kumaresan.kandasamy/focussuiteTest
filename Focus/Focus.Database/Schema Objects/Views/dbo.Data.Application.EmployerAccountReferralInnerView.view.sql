﻿CREATE VIEW dbo.[Data.Application.EmployerAccountReferralInnerView]
AS
WITH EmployersAndEmployees
AS
(
	SELECT					Employer.Id AS EmployerId,
									Employer.ApprovalStatus AS EmployerApprovalStatus,
									ROW_NUMBER() OVER (PARTITION BY Employer.Id ORDER BY Employee.Id) AS EmployerEmployeeNumber,
									ROW_NUMBER() OVER (PARTITION BY BusinessUnit.Id ORDER BY Employee.Id) AS BusinessUnitEmployeeNumber,
									BusinessUnit.ApprovalStatus As BusinessUnitApprovalStatus,
									EmployeeBusinessUnit.[Default] AS BusinessUnitIsDefault,
									Employee.ApprovalStatus AS EmployeeApprovalStatus,
									Employee.Id AS EmployeeId,
									BusinessUnit.Id AS BusinessUnitId,
									BusinessUnit.IsPrimary AS EmployerIsPrimary
	FROM						dbo.[Data.Application.Employer] AS Employer WITH (NOLOCK)
	INNER JOIN			dbo.[Data.Application.Employee] AS Employee WITH (NOLOCK) ON Employer.Id = Employee.EmployerId
	INNER JOIN			dbo.[Data.Application.EmployeeBusinessUnit] AS EmployeeBusinessUnit WITH (NOLOCK) ON Employee.Id = EmployeeBusinessUnit.EmployeeId
	INNER JOIN			dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.BusinessUnitId = BusinessUnit.Id
)
SELECT	EmployersAndEmployees.EmployerId AS Id,
				EmployersAndEmployees.EmployerId,
				EmployersAndEmployees.EmployerApprovalStatus,
				EmployersAndEmployees.BusinessUnitApprovalStatus,
				EmployersAndEmployees.BusinessUnitIsDefault,
				EmployersAndEmployees.EmployeeId,
				EmployersAndEmployees.EmployerIsPrimary,
				EmployersAndEmployees.EmployeeApprovalStatus,
				EmployersAndEmployees.BusinessUnitId,
				1 AS TypeOfApproval
FROM		EmployersAndEmployees
WHERE		EmployersAndEmployees.EmployerApprovalStatus IN (1, 4, 3)
AND			EmployersAndEmployees.EmployerEmployeeNumber = 1

UNION

SELECT	EmployersAndEmployees.EmployeeId AS Id,
				EmployersAndEmployees.EmployerId,
				EmployersAndEmployees.EmployerApprovalStatus,
				EmployersAndEmployees.BusinessUnitApprovalStatus,
				EmployersAndEmployees.BusinessUnitIsDefault,
				EmployersAndEmployees.EmployeeId,
				EmployersAndEmployees.EmployerIsPrimary,
				EmployersAndEmployees.EmployeeApprovalStatus,
				EmployersAndEmployees.BusinessUnitId,
				3 AS TypeOfApproval
FROM		EmployersAndEmployees
WHERE		EmployersAndEmployees.EmployeeApprovalStatus IN (1, 4, 3)
AND
(
	(EmployersAndEmployees.BusinessUnitEmployeeNumber > 1 AND EmployersAndEmployees.EmployerEmployeeNumber > 1)
	OR
	(EmployersAndEmployees.EmployerApprovalStatus IN (0, 2) AND EmployersAndEmployees.BusinessUnitApprovalStatus  IN (0, 2) AND EmployersAndEmployees.BusinessUnitIsDefault = 1)
)
AND NOT EXISTS
(
	SELECT			1
	FROM				[Data.Application.EmployeeBusinessUnit] EBU
	INNER JOIN	[Data.Application.BusinessUnit] BU ON BU.Id = EBU.BusinessUnitId
	WHERE				EBU.EmployeeId = EmployersAndEmployees.EmployeeId
	AND					BU.Id <> EmployersAndEmployees.BusinessUnitId
	AND					BU.ApprovalStatus IN (1, 3, 4)
)

UNION

SELECT	EmployersAndEmployees.BusinessUnitId AS Id,
				EmployersAndEmployees.EmployerId,
				EmployersAndEmployees.EmployerApprovalStatus,
				EmployersAndEmployees.BusinessUnitApprovalStatus,
				EmployersAndEmployees.BusinessUnitIsDefault,
				EmployersAndEmployees.EmployeeId,
				EmployersAndEmployees.EmployerIsPrimary,
				EmployersAndEmployees.EmployeeApprovalStatus,
				EmployersAndEmployees.BusinessUnitId,
				2 AS TypeOfApproval
FROM		EmployersAndEmployees
WHERE		EmployersAndEmployees.BusinessUnitApprovalStatus IN (1, 4, 3)
AND			(EmployersAndEmployees.EmployerIsPrimary = 0 OR EmployersAndEmployees.EmployerApprovalStatus IN (0, 2))
AND			EmployersAndEmployees.BusinessUnitEmployeeNumber = 1