﻿
CREATE VIEW [dbo].[Data.Application.RecentlyPlacedPersonMatchView]  
AS  
  
SELECT   
 rpm.Id,  
 rpm.PersonId AS 'MatchedPersonId',  
 matched.FirstName + ' ' + matched.LastName AS MatchedCandidateName,  
 rp.Id AS 'RecentlyPlacedId',  
 rp.PersonId AS 'PlacedPersonId',  
 placed.FirstName + ' ' + placed.LastName AS PlacedCandidateName,  
 rpm.Score,   
 bu.Id AS 'BusinessUnitId',  
 bu.Name AS 'BusinessUnitName',  
 e.Id AS 'EmployerId',  
 e.Name AS 'EmployerName',   
 rp.PlacementDate,  
 rp.JobId,  
 j.JobTitle 
FROM  
 [Data.Application.RecentlyPlacedMatch] rpm WITH (NOLOCK)  
 INNER JOIN [Data.Application.RecentlyPlaced] rp WITH (NOLOCK) ON rp.Id = rpm.RecentlyPlacedId  
 INNER JOIN [Data.Application.Job] j WITH (NOLOCK) ON j.Id = rp.JobId
  INNER JOIN [Data.Application.Person] matched WITH (NOLOCK) ON matched.Id = rpm.PersonId  
 INNER JOIN [Data.Application.Person] placed WITH (NOLOCK) ON placed.Id = rp.PersonId  
 INNER JOIN [Data.Application.BusinessUnit] bu WITH (NOLOCK) ON bu.id = j.BusinessUnitId   
 INNER JOIN [Data.Application.Employer] e WITH (NOLOCK) ON e.Id = j.EmployerId
 

  









