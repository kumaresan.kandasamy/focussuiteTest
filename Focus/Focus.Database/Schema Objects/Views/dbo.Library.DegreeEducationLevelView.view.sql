﻿
CREATE VIEW [dbo].[Library.DegreeEducationLevelView]  
AS  
SELECT  
 DegreeEducationLevel.Id,  
 DegreeEducationLevel.DegreeId,  
 Degree.Name AS DegreeName,  
 Degree.IsDegreeArea,  
 DegreeEducationLevel.EducationLevel,  
 DegreeEducationLevel.Name AS DegreeEducationLevelName,  
 Degree.IsClientData,  
 Degree.ClientDataTag,
 Degree.Tier 
FROM  
 [Library.DegreeEducationLevel] AS DegreeEducationLevel WITH (NOLOCK)  
 INNER JOIN [Library.Degree] AS Degree WITH (NOLOCK) ON DegreeEducationLevel.DegreeId = Degree.Id
