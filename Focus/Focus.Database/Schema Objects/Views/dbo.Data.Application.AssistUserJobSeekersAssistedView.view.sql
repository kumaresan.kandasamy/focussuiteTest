﻿






CREATE VIEW [dbo].[Data.Application.AssistUserJobSeekersAssistedView]
AS

SELECT	
	ActionEvent.Id AS Id,
	[User].Id AS UserId,
	Person.FirstName + ' ' + Person.LastName AS Name,
	ActionType.Name AS ActionName,
	ActionEvent.ActionedOn

FROM  dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].Id = ActionEvent.UserId

	-- needed for ActionName
	INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id

	-- Assumption that created events will have UserId as EntityId
	LEFT JOIN dbo.[Data.Application.User] AS UserAction WITH (NOLOCK) ON UserAction.Id = ActionEvent.EntityId
	LEFT JOIN dbo.[Data.Application.Person] AS Person  WITH (NOLOCK) ON Person.Id = UserAction.PersonId

WHERE    ((ActionType.Name = 'SelfReferral') OR
                      (ActionType.Name = 'UpdateResume') OR                      
                      (ActionType.Name = 'EmailResume') OR
                      (ActionType.Name = 'DownloadResume') OR
                      (ActionType.Name = 'SaveJobAlert'))







