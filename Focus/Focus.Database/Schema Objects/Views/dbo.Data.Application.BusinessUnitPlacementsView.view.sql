﻿







CREATE VIEW [dbo].[Data.Application.BusinessUnitPlacementsView]
AS
SELECT     
	Cast(Row_Number() OVER (ORDER BY BusinessUnit.Id) AS bigint) AS Id,
	BusinessUnit.Id AS BuId, 
	[Resume].PersonId AS JobSeekerId, 
	BusinessUnit.Name AS BuName, 
	Job.JobTitle, 
	[Application].ApplicationStatus, 
    [Resume].FirstName, 
    [Resume].LastName
FROM         
	dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON BusinessUnit.Id = Job.BusinessUnitId
	INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON Job.Id = Posting.JobId
	INNER JOIN dbo.[Data.Application.Application] AS [Application] WITH (NOLOCK) ON Posting.Id = [Application].PostingId 
	INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
WHERE     
	([Application].ApplicationStatus = 12)







