﻿



CREATE VIEW [dbo].[Library.OnetWordView]
AS

SELECT 
	ow.Id AS 'Id',
	ow.Frequency AS 'Frequency',
	ow.OnetId AS 'OnetId',
	ow.OnetRingId AS 'OnetRingId',
	ow.OnetSoc AS 'OnetSoc',
	ow.Stem AS 'Stem',
	ow.Word AS 'Word',
	o.JobFamilyId AS 'JobFamilyId',
	o.JobZone AS 'JobZone',
	o.[Key] AS 'OnetKey',
	o.OnetCode AS OnetCode 
FROM 
	[Library.OnetWord] ow WITH (NOLOCK)
	INNER JOIN  [Library.Onet] o WITH (NOLOCK) ON ow.OnetId = o.Id



