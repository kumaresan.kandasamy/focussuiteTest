﻿CREATE VIEW [dbo].[Data.Application.StaffProfile]
AS
SELECT 
	[User].Id, 
	Person.FirstName, 
	Person.LastName,
	Person.EmailAddress,
	Person.ExternalOffice,
	[User].[Enabled],
	pn.Number as PhoneNumber,
	pn1.Number as FaxNumber,
	pa.TownCity,
	pa.StateId
FROM  
	dbo.[Data.Application.Person] AS Person WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId
	LEFT JOIN dbo.[Data.Application.PersonAddress] pa WITH (NOLOCK) ON Person.Id = pa.PersonId
	LEFT JOIN dbo.[Data.Application.PhoneNumber] pn WITH (NOLOCK) ON Person.Id = pn.PersonId and pn.PhoneType = 0
	LEFT JOIN dbo.[Data.Application.PhoneNumber] pn1 WITH (NOLOCK) ON Person.Id = pn1.PersonId and pn.PhoneType = 4