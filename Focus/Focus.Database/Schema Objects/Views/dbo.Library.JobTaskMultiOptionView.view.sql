﻿




CREATE VIEW [dbo].[Library.JobTaskMultiOptionView]
AS

SELECT 
	jtmo.[Id],
	jt.[Id] AS JobTaskId,
	jtli.[PrimaryValue] AS Prompt,
	jt.OnetId,
	l.Culture AS Culture
FROM
	[Library.JobTask] jt WITH (NOLOCK)
	INNER JOIN [Library.JobTaskMultiOption] jtmo WITH (NOLOCK) ON jt.[Id] = jtmo.[JobTaskId]
	INNER JOIN [Library.JobTaskLocalisationItem] jtli WITH (NOLOCK) ON jtmo.[Key] = jtli.[Key]
	INNER JOIN [Library.Localisation] l WITH (NOLOCK) ON jtli.LocalisationId = l.id




