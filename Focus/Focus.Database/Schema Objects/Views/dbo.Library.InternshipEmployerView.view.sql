﻿

CREATE VIEW [dbo].[Library.InternshipEmployerView]    
AS    
SELECT    
 InternshipEmployer.Id,    
 InternshipEmployer.InternshipCategoryId,    
 InternshipEmployer.EmployerId,    
 Employer.Name,    
 InternshipEmployer.Frequency,  
 InternshipEmployer.StateAreaId,
 InternshipCategory.Name AS InternshipCategoryName    
FROM    
 [Library.InternshipEmployer] AS InternshipEmployer WITH (NOLOCK)    
 INNER JOIN [Library.Employer] AS Employer WITH (NOLOCK) ON InternshipEmployer.EmployerId = Employer.Id 
 INNER JOIN [Library.InternshipCategory] AS InternshipCategory WITH (NOLOCK) ON InternshipEmployer.InternshipCategoryId = InternshipCategory.Id
