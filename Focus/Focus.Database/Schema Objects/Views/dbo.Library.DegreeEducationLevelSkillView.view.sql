﻿
CREATE VIEW [dbo].[Library.DegreeEducationLevelSkillView]
AS
SELECT
	JobDegreeEducationLevel.DegreeEducationLevelId AS Id,
	JobReport.StateAreaId,
	JobSkill.SkillId,
	Skill.SkillType,
	Skill.Name AS SkillName,
	SUM(JobReport.HiringDemand * (JobSkill.DemandPercentile / 100)) AS DemandPercentile
FROM
	[Library.JobDegreeEducationLevel] AS JobDegreeEducationLevel WITH (NOLOCK)
	INNER JOIN [Library.JobSkill] AS JobSkill WITH (NOLOCK) ON JobDegreeEducationLevel.JobId = JobSkill.JobId
	INNER JOIN [Library.JobReport] AS JobReport WITH (NOLOCK) ON JobDegreeEducationLevel.JobId = JobReport.JobId
	INNER JOIN [Library.Skill] AS Skill WITH (NOLOCK) ON JobSkill.SkillId = Skill.Id
WHERE
	JobSkill.JobSkillType = 1
	AND JobSkill.[Rank] <= 20
GROUP BY
	JobDegreeEducationLevel.DegreeEducationLevelId,
	JobReport.StateAreaId,
	JobSkill.SkillId,
	Skill.SkillType,
	Skill.Name

