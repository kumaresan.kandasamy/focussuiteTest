﻿






CREATE VIEW [dbo].[Data.Application.CandidateDidNotClickHowToApplyView]
AS
SELECT	
	i.Id,
	i.JobId as JobId, 
	i.PersonId as PersonId,  
	i.CreatedOn as ActionedOn,
	j.EmployeeId AS EmployeeId
			 
FROM [Data.Application.InviteToApply] i WITH (NOLOCK)
	INNER JOIN [Data.Application.Job] j WITH (NOLOCK) on j.Id = i.JobId
 
WHERE NOT EXISTS
		(SELECT	ae.EntityId AS JobId, 
				ae.EntityIdAdditional01 AS PersonId, 
				ae.EntityIdAdditional02 AS UserId,
				ae.ActionedOn, 
				j.JobTitle, 
				j.EmployeeId AS EmployeeId
		FROM dbo.[Data.Core.ActionEvent] ae WITH (NOLOCK)
			INNER JOIN dbo.[Data.Core.ActionType] at WITH (NOLOCK) ON ae.ActionTypeId = at.Id
			INNER JOIN dbo.[Data.Application.Job] j WITH (NOLOCK) ON j.Id = ae.EntityId 
			INNER JOIN dbo.[Data.Application.User] u WITH (NOLOCK) ON u.Id = ae.UserId 
			WHERE at.Name = 'RegisterHowToApply' AND  i.JobId = j.Id AND i.PersonId = PersonId)
							




