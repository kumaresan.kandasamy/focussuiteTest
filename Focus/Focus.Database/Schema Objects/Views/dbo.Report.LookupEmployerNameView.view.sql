﻿


CREATE VIEW [dbo].[Report.LookupEmployerNameView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.Name
	FROM
	(
		SELECT DISTINCT
			Name
		FROM 
			[Report.Employer]
	) Data

