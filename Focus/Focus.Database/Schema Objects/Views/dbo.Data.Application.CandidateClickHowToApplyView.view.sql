﻿






CREATE VIEW [dbo].[Data.Application.CandidateClickHowToApplyView]
AS
SELECT DISTINCT 
	Cast(Row_Number() OVER (ORDER BY j.Id) AS bigint) AS Id,
	j.Id AS JobId,
	ae.EntityIdAdditional01 AS PersonId, 
	ae.EntityIdAdditional02 AS UserId,
	MAX(ae.ActionedOn) AS ActionedOn, 
    MAX(j.JobTitle) AS jobTitle,
    MAX(j.EmployeeId) AS EmployeeId
            
FROM dbo.[Data.Core.ActionEvent] ae WITH (NOLOCK)
	INNER JOIN dbo.[Data.Core.ActionType] at WITH (NOLOCK) ON ae.ActionTypeId = at.Id 
	INNER JOIN dbo.[Data.Application.Job] j WITH (NOLOCK) ON j.Id = ae.EntityId 
	INNER JOIN dbo.[Data.Application.User] u WITH (NOLOCK) ON u.Id = ae.EntityIdAdditional02
    INNER JOIN dbo.[Data.Application.InviteToApply] i WITH (NOLOCK) ON (i.PersonId = ae.EntityIdAdditional01 AND i.JobId = ae.EntityId) 
            
WHERE (at.Name = 'RegisterHowToApply' AND i.Viewed = 1)

GROUP BY ae.EntityIdAdditional01, j.Id, ae.EntityIdAdditional02








