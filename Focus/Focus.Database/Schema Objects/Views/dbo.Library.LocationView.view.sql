﻿
CREATE VIEW [dbo].[Library.LocationView]
AS
SELECT
	CONVERT(varchar,[State].Id) + '|' AS Id,
	[State].Id AS StateId,
	[State].Name As StateName,
	NULL AS AreaId,
	NULL As AreaName,
	[State].Name AS LocationName
FROM
	[Library.State] AS [State] WITH (NOLOCK)
UNION
SELECT
	CONVERT(varchar,[State].Id) + '|' + CONVERT(varchar,Area.Id),
	[State].Id,
	[State].Name,
	Area.Id,
	Area.Name,
	Area.Name + ', ' + [State].Code
FROM
	[Library.State] AS [State] WITH (NOLOCK)
	INNER JOIN [Library.StateArea] AS StateArea WITH (NOLOCK) ON State.Id = StateArea.StateId
	INNER JOIN [Library.Area] AS Area WITH (NOLOCK) ON [StateArea].AreaId = Area.Id

