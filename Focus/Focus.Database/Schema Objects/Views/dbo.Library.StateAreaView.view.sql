﻿
CREATE VIEW [dbo].[Library.StateAreaView]  
AS  
SELECT 
	StateArea.Id, 
	StateArea.StateId,
	[State].Name AS StateName,
	[State].Code AS StateCode,
	[State].SortOrder AS StateSortOrder,   
    StateArea.AreaId,
    Area.Name AS AreaName,
    Area.SortOrder AS AreaSortOrder,
    CASE	WHEN [State].SortOrder = 0 AND Area.SortOrder = 0 THEN [State].Name 
			WHEN [State].SortOrder <> 0 AND Area.SortOrder = 0 THEN [State].Name + ', ' + Area.Name 
			ELSE Area.Name + ', ' + [State].Code
			END AS StateAreaName,
	StateArea.AreaCode,   
    StateArea.IsDefault  
FROM  
	dbo.[Library.StateArea] AS StateArea WITH (NOLOCK) 
	INNER JOIN dbo.[Library.State] AS [State] WITH (NOLOCK) ON StateArea.StateId = [State].Id 
	INNER JOIN dbo.[Library.Area] AS Area WITH (NOLOCK) ON StateArea.AreaId = Area.Id  
WHERE 
	(StateArea.Display = 1)  
