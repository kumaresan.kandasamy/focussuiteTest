﻿
CREATE VIEW [dbo].[Library.DegreeEducationLevelStateAreaView]
AS
SELECT DISTINCT
	DegreeEducationLevel.Id,
	DegreeEducationLevel.Name AS DegreeEducationLevelName,
	DegreeEducationLevel.EducationLevel,
	Degree.Id AS DegreeId,
	Degree.Name AS DegreeName,
	Degree.IsDegreeArea,
	Degree.IsClientData,
	Degree.ClientDataTag,
	JobDegreeEducationLevelReport.StateAreaId
FROM
	[Library.DegreeEducationLevel] AS DegreeEducationLevel WITH (NOLOCK)
	INNER JOIN [Library.Degree] AS Degree WITH (NOLOCK) ON DegreeEducationLevel.DegreeId = Degree.Id
	INNER JOIN [Library.JobDegreeEducationLevel] AS JobDegreeEducationLevel WITH (NOLOCK) ON DegreeEducationLevel.Id = JobDegreeEducationLevel.DegreeEducationLevelId
	INNER JOIN [Library.JobDegreeEducationLevelReport] AS JobDegreeEducationLevelReport WITH (NOLOCK) ON JobDegreeEducationLevel.Id = JobDegreeEducationLevelReport.JobDegreeEducationLevelId

