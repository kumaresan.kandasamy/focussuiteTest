﻿

CREATE VIEW [dbo].[Data.Application.BusinessUnitUser] as
SELECT 
	NEWID() AS Id, 
	ebu.BusinessUnitId, 
	u.Id as UserId, 
	u.[Enabled] 
FROM [Data.Application.EmployeeBusinessUnit] ebu WITH (NOLOCK)
	INNER JOIN [Data.Application.Employee] e WITH (NOLOCK) on ebu.EmployeeId = e.Id 
	INNER JOIN [Data.Application.User] u WITH (NOLOCK) on e.PersonId = u.PersonId

