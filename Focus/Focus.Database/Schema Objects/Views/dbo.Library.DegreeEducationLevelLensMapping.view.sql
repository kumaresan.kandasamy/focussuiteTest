﻿

CREATE VIEW [dbo].[Library.DegreeEducationLevelLensMapping]
AS

SELECT   
	dm.Id,  
	dm.LensId,  
	d.RcipCode,  
	del.Id AS DegreeEducationLevelId,  
	pad.ProgramAreaId AS ProgramAreaId  
FROM 
	[Library.DegreeEducationLevelMapping] dm WITH(NOLOCK)  
INNER JOIN [Library.DegreeEducationLevel] del WITH(NOLOCK) 
	ON del.Id = dm.DegreeEducationLevelId  
INNER JOIN [Library.Degree] d WITH(NOLOCK)
	ON d.Id = del.DegreeId
LEFT OUTER JOIN [Library.ProgramAreaDegree] AS pad WITH(NOLOCK) 
	ON del.DegreeId = pad.DegreeId  

