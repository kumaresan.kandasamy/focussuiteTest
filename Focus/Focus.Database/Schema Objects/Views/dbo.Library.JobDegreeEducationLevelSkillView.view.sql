﻿
CREATE VIEW [dbo].[Library.JobDegreeEducationLevelSkillView]
AS
SELECT DISTINCT
	JobDegreeEducationLevel.DegreeEducationLevelId AS Id,
	JobSkill.SkillId
FROM
	[Library.JobDegreeEducationLevel] AS JobDegreeEducationLevel WITH (NOLOCK)
	INNER JOIN [Library.JobSkill] AS JobSkill WITH (NOLOCK) ON JobDegreeEducationLevel.JobId = JobSkill.JobId
WHERE
	JobSkill.JobSkillType = 1

