﻿
CREATE view [dbo].[Config.ActivityView] as  
SELECT 
	a.Id,
	ac.Id as ActivityCategoryId,
	ac.LocalisationKey as CategoryLocalisationKey,
	ac.Name as CategoryName,
	ac.visible as CategoryVisible,
	ac.Administrable as CategoryAdministrable,  
	ac.ActivityType,
	a.LocalisationKey as ActivityLocalisationKey,
	a.Name as ActivityName,
	a.visible as ActivityVisible,
	a.Administrable as ActivityAdministrable,
	a.SortOrder,
	a.ExternalId as ActivityExternalId,
	ac.Editable
FROM
	[Config.ActivityCategory] ac WITH (NOLOCK) 
	INNER JOIN [Config.Activity] a WITH (NOLOCK) on ac.id = a.activitycategoryid
