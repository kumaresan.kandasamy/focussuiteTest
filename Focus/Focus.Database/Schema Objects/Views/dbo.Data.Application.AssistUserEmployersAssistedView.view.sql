﻿




CREATE VIEW [dbo].[Data.Application.AssistUserEmployersAssistedView]
AS

SELECT	
	Cast(Row_Number() OVER (ORDER BY ActionEvent.Id) AS bigint) AS Id,
	[User].Id AS UserId,
	ISNULL(PersonJob.FirstName + ' ' + PersonJob.LastName,ISNULL(PersonAction.FirstName + ' ' + PersonAction.LastName, ISNULL(PersonSearch.FirstName + ' ' + PersonSearch.LastName, ISNULL(PersonCompany .FirstName + ' ' + PersonCompany.LastName, ISNULL(PersonLogo .FirstName + ' ' + PersonLogo.LastName, PersonDesc .FirstName + ' ' + PersonDesc.LastName))))) AS Name,
	ISNULL(BUDesc.Name,ISNULL(BULogo.Name ,ISNULL(BUAction.Name, ISNULL(BUJob.Name, 'All registered Business Units')))) AS BusinessUnitName,
	ActionType.Name AS ActionName,
	ActionEvent.ActionedOn,
	Job.JobTitle,
	SavedSearch.Name AS SavedSearchName,			
	ActionEvent.AdditionalDetails

FROM dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].Id = ActionEvent.UserId

	-- needed for ActionName
	INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id

	-- Job actions have JobId as EntityId - Also Invite Candidate To Apply
	LEFT OUTER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Job.Id	= ActionEvent.EntityId
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnit] AS BUJob WITH (NOLOCK) ON BUJob.Id = Job.BusinessUnitId
	LEFT OUTER JOIN dbo.[Data.Application.Employee] AS EmployeeJob WITH (NOLOCK) ON EmployeeJob.Id = Job.EmployeeId
	LEFT OUTER JOIN dbo.[Data.Application.Person] AS PersonJob WITH (NOLOCK) ON PersonJob.Id = EmployeeJob.PersonId

	--Save BusinessUnitLogo has BusinessUnitLogo as EntityId
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnitLogo] AS BusinessUnitLogo WITH (NOLOCK) ON BusinessUnitLogo.Id = ActionEvent.EntityId
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnit] AS BULogo WITH (NOLOCK) ON BULogo.Id = BusinessUnitLogo.BusinessUnitId
	LEFT OUTER JOIN dbo.[Data.Application.User] AS UserLogo WITH (NOLOCK) ON UserLogo.Id = EntityIdAdditional01
	LEFT OUTER JOIN dbo.[Data.Application.Person] AS PersonLogo WITH (NOLOCK) ON PersonLogo.Id = UserLogo.PersonId

	--Save BusinessUnitDescription has BusinessUnitDescription as EntityId
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnitDescription] AS BusinessUnitDescription WITH (NOLOCK) ON BusinessUnitDescription.Id = ActionEvent.EntityId
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnit] AS BUDesc WITH (NOLOCK) ON BUDesc.Id = BusinessUnitDescription.BusinessUnitId
	LEFT OUTER JOIN dbo.[Data.Application.User] AS UserDesc WITH (NOLOCK) ON UserDesc.Id = ActionEvent.EntityIdAdditional01
	LEFT OUTER JOIN dbo.[Data.Application.Person] AS PersonDesc WITH (NOLOCK) ON PersonDesc.Id = UserDesc.PersonId

	-- Save company info Action has BUId as EntityId and UserId as Additional
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnit] AS BUAction WITH (NOLOCK) ON BUAction.Id = ActionEvent.EntityId
	LEFT OUTER JOIN dbo.[Data.Application.User] AS UserCompany WITH (NOLOCK) ON UserCompany.Id = ActionEvent.EntityIdAdditional01
	LEFT OUTER JOIN dbo.[Data.Application.Person] AS PersonCompany WITH (NOLOCK) ON PersonCompany.Id = UserCompany.PersonId

	-- SaveContactInfo Action and SendCandidateEmails has UserId as EntityId
	LEFT OUTER JOIN dbo.[Data.Application.User] AS UserAction WITH (NOLOCK) ON UserAction.Id = ActionEvent.EntityId
	LEFT OUTER JOIN dbo.[Data.Application.Person] AS PersonAction WITH (NOLOCK) ON PersonAction.Id = UserAction.PersonId

	-- Save Talent Alert has SavedSearchId as EntityId
	LEFT OUTER JOIN dbo.[Data.Application.SavedSearch] AS SavedSearch WITH (NOLOCK) ON SavedSearch.Id = ActionEvent.EntityId
	LEFT OUTER JOIN dbo.[Data.Application.SavedSearchUser] AS SavedSearchUser WITH (NOLOCK) ON SavedSearch.Id = SavedSearchUser.SavedSearchId
	LEFT OUTER JOIN dbo.[Data.Application.User] AS UserSearch WITH (NOLOCK) ON UserSearch.Id = SavedSearchUser.UserId
	LEFT OUTER JOIN dbo.[Data.Application.Person] AS PersonSearch WITH (NOLOCK) ON PersonSearch.Id = UserSearch.PersonId


WHERE    ((ActionType.Name = 'PostJob') OR
                      (ActionType.Name = 'SaveJob') OR
                      (ActionType.Name = 'RefreshJob') OR
                      (ActionType.Name = 'ReactivateJob') OR
                      (ActionType.Name = 'CloseJob') OR
                      (ActionType.Name = 'HoldJob') OR                     
                      (ActionType.Name = 'DuplicateJob') OR   
                      (ActionType.Name = 'UpdateUser') OR     
                      (ActionType.Name = 'SaveBusinessUnit') OR   
                      (ActionType.Name = 'SaveBusinessUnitLogo') OR   
                      (ActionType.Name = 'SaveBusinessUnitDescription') OR                       
                      (ActionType.Name = 'SaveCandidateSavedSearch') OR
                      (ActionType.Name = 'InviteJobSeekerToApply') OR
                      (ActionType.Name = 'SendCandidateEmails'))







