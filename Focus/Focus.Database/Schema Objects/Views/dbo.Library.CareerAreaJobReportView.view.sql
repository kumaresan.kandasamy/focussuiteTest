﻿
CREATE VIEW [dbo].[Library.CareerAreaJobReportView]

AS

SELECT 
	NEWID() AS Id,
	CA.Id AS CareerAreaId,
	JR.StateAreaId,
	JR.JobId,
	JR.HiringDemand
FROM 
	[Library.CareerArea] CA
INNER JOIN [Library.JobCareerArea] JCA
	ON JCA.CareerAreaId = CA.Id
INNER JOIN [Library.JobReport] JR
	ON JR.JobId = JCA.JobId
