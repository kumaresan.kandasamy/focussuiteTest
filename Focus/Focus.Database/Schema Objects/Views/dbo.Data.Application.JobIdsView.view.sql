﻿
CREATE VIEW [dbo].[Data.Application.JobIdsView]
AS
SELECT
	j.Id,
	j.ExternalId,
	jp.LensPostingId,
	j.JobStatus,
	j.VeteranPriorityEndDate
FROM
	[Data.Application.Job] j
INNER JOIN [Data.Application.Posting] jp 
	ON jp.JobId = j.Id

