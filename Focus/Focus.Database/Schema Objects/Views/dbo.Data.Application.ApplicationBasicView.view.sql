﻿
CREATE VIEW [dbo].[Data.Application.ApplicationBasicView]
AS

SELECT
	[Application].Id AS Id,
	[Application].ApplicationStatus AS CandidateApplicationStatus,
	[Application].ApplicationScore AS CandidateApplicationScore,
	[Application].StatusLastChangedOn AS ApplicationStatusLastChangedOn,
	Posting.JobId,
	[Resume].PersonId AS CandidateId
FROM 
	[Data.Application.Application] AS [Application] WITH (NOLOCK)
INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) 
	ON [Application].PostingId = Posting.Id
INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) 
	ON [Application].ResumeId = [Resume].Id
WHERE	
	Posting.JobId IS NOT NULL
