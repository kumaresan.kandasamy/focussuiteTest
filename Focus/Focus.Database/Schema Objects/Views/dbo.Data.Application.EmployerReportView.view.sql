﻿CREATE VIEW [dbo].[Data.Application.EmployerReportView]

AS
	SELECT 
		BU.Id,
		BU.[Name],
		BUA.CountyId,
		BUA.StateId,
		BUA.PostcodeZip,
		E.FederalEmployerIdentificationNumber,
		E.Id AS EmployerId,
		BU.AccountTypeId,
		BU.NoOfEmployees
	FROM
		[Data.Application.BusinessUnit] BU
	INNER JOIN [Data.Application.Employer] E
		ON E.Id = BU.EmployerId
	LEFT OUTER JOIN [Data.Application.BusinessUnitAddress] BUA
		ON BUA.BusinessUnitId = BU.Id
		AND BUA.IsPrimary = 1


