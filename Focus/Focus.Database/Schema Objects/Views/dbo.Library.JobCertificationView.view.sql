﻿
CREATE VIEW [dbo].[Library.JobCertificationView]
AS
SELECT
	JobCertification.Id,
	JobCertification.JobId,
	JobCertification.CertificationId,
	Certification.Name AS CertificationName,
	JobCertification.DemandPercentile
FROM
	[Library.JobCertification] AS JobCertification WITH (NOLOCK)
	INNER JOIN [Library.Certification] AS Certification WITH (NOLOCK) ON JobCertification.CertificationId = Certification.Id

