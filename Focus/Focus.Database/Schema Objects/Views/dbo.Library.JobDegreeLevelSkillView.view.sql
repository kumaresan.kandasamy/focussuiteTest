﻿

CREATE VIEW [dbo].[Library.JobDegreeLevelSkillView]
AS
SELECT DISTINCT
	CAST(1 AS bigint) AS Id,
	JobDegreeLevel.DegreeLevel,
	JobSkill.SkillId
FROM
	[Library.JobDegreeLevel] AS JobDegreeLevel WITH (NOLOCK)
	INNER JOIN [Library.JobSkill] AS JobSkill WITH (NOLOCK) ON JobDegreeLevel.JobId = JobSkill.JobId
WHERE
	JobSkill.JobSkillType = 1


