﻿





CREATE VIEW [dbo].[Data.Application.NoteReminderView]
AS

SELECT
	NoteReminder.Id,
	NoteReminder.NoteReminderType,
	NoteReminder.EntityId,
	[Text],
	Person.FirstName AS CreatedByFirstname,
	Person.LastName AS CreatedByLastname,
	NoteReminder.CreatedOn,
	NoteReminder.EntityType,
	NoteReminder.NoteType
FROM
	[Data.Application.NoteReminder] AS NoteReminder WITH (NOLOCK)
	INNER JOIN [Data.Application.User] AS [User] WITH (NOLOCK) ON NoteReminder.CreatedBy = [User].Id
	INNER JOIN [Data.Application.Person] AS Person WITH (NOLOCK) ON [User].PersonId = Person.Id




