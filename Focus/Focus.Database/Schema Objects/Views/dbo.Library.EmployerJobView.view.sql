﻿

CREATE VIEW [dbo].[Library.EmployerJobView]
AS
SELECT
 EmployerJob.Id,  
 Employer.Id AS EmployerId,  
 Employer.Name AS EmployerName,  
 Job.Id AS JobId,  
 Job.Name AS JobName,  
 Job.ROnet AS ROnet,   
 EmployerJob.StateAreaId,  
 EmployerJob.Positions,
 Job.StarterJob
FROM
	[Library.Employer] AS Employer WITH (NOLOCK)
	INNER JOIN [Library.EmployerJob] AS EmployerJob WITH (NOLOCK) ON Employer.Id = EmployerJob.EmployerId
	INNER JOIN [Library.Job] AS Job WITH (NOLOCK) ON EmployerJob.JobId = Job.Id


