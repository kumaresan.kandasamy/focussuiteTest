﻿
CREATE VIEW [dbo].[Library.JobSkillView]
AS
SELECT
	JobSkill.Id AS Id,
	JobSkill.JobId,
	JobSkill.JobSkillType,
	JobSkill.SkillId,
	Skill.SkillType,
	Skill.Name AS SkillName,
	JobSkill.DemandPercentile,
	JobSkill.[Rank]
FROM
	[Library.JobSkill] AS JobSkill WITH (NOLOCK)
	INNER JOIN [Library.Skill] AS Skill WITH (NOLOCK) ON JobSkill.SkillId = Skill.Id

