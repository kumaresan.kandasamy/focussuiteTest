﻿


CREATE VIEW [dbo].[Report.LookupJobSeekerInternshipView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.[Description]
	FROM
	(
		SELECT DISTINCT
			[Description] 
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 2
	) Data

