﻿


CREATE VIEW [dbo].[Report.LookupJobSeekerMilitaryOccupationTitleView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.MilitaryOccupationTitle
	FROM
	(
		SELECT DISTINCT
			[Description] AS MilitaryOccupationTitle
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 8
	) Data

