﻿



CREATE VIEW [dbo].[Library.DegreeAliasView] AS
SELECT 
	da.id, 
	da.AliasExtended, 
	da.Alias, 
	da.EducationLevel, 
	da.DegreeId, 
	da.DegreeEducationLevelId, 
	d.IsClientData, 
	d.ClientDataTag 
FROM 
	[Library.DegreeAlias] da WITH (NOLOCK)
	INNER JOIN [Library.Degree] d WITH (NOLOCK) ON da.degreeId = d.Id




