﻿
CREATE VIEW [dbo].[Data.Application.JobIssuesView]
AS
SELECT
	ji.Id,
	ji.JobId,
	ji.LowQualityMatches,
	ji.LowReferralActivity,
	ji.NotViewingReferrals,
	ji.SearchingButNotInviting,
	ji.ClosingDateRefreshed,
	ji.EarlyJobClosing,
	ji.NegativeSurveyResponse,
	ji.PositiveSurveyResponse,
	ji.FollowUpRequested,
	ISNULL(av.PostHireFollowUpCount, 0) AS PostHireFollowUpCount
FROM
	[Data.Application.JobIssues] ji
LEFT JOIN 
	(SELECT 
		p.JobId as JobId,
		COUNT(1) AS PostHireFollowUpCount
	FROM
		[Data.Application.Application] a
	INNER JOIN [Data.Application.Posting] p
		ON a.PostingId = p.Id
	WHERE
		a.PostHireFollowUpStatus = 0
	GROUP BY p.JobId) av
 on ji.JobId = av.jobid
	
