﻿CREATE VIEW [dbo].[Config.LaborInsightMappingView]
AS
	
SELECT 
	NEWID() AS Id,
	ci.Id AS CodeGroupItemId,	
	cg.[Key] AS CodeGroupKey,
	lim.LaborInsightId,
	lim.LaborInsightValue
FROM
	[Config.CodeGroup] cg WITH (NOLOCK)
	INNER JOIN [Config.CodeGroupItem] cgi WITH (NOLOCK) ON cg.Id = cgi.CodeGroupId 
	INNER JOIN [Config.CodeItem] ci WITH (NOLOCK) ON cgi.CodeItemId = ci.Id
	INNER JOIN [Config.LaborInsightMapping] lim WITH (NOLOCK) ON lim.CodeGroupKey = cg.[Key] AND lim.CodeItemKey = ci.[Key]