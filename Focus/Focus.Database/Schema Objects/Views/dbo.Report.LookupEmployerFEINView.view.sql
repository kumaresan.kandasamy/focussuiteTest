﻿


CREATE VIEW [dbo].[Report.LookupEmployerFEINView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.FederalEmployerIdentificationNumber
	FROM
	(
		SELECT DISTINCT
			FederalEmployerIdentificationNumber
		FROM 
			[Report.Employer]
	) Data

