﻿

CREATE VIEW [dbo].[Data.Application.CandidateView]
AS
SELECT	Person.[Id],
		Person.FirstName, 
		Person.LastName, 
		Person.DateOfBirth, 
		Person.SocialSecurityNumber, 
        Person.EmailAddress, 
        [Resume].AddressLine1,
        [Resume].TownCity, 
        [Resume].StateId,
        [Resume].CountryId,
        [Resume].PostcodeZip, 
        [Resume].HomePhone,
        [Resume].[Id] AS DefaultResumeId,
        [Resume].[IsSearchable] as IsSearchable,
        [Resume].WordCount AS ResumeWordCount,
        [Resume].SkillCount AS ResumeSkillCount,
        [Resume].VeteranPriorityServiceAlertsSubscription,
        [User].Id AS UserId,
        [User].CreatedOn,
        [User].ExternalId AS ClientId,
        [User].[Enabled],
        [User].LoggedInOn AS LastLoggedInOn
FROM  
	[dbo].[Data.Application.Person] AS Person WITH (NOLOCK)
INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) 
	ON [Resume].PersonId = Person.Id 
	AND [Resume].IsDefault = 1
	AND [Resume].StatusId = 1
INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) 
	ON [User].PersonId = Person.Id

