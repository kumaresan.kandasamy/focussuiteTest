﻿

CREATE VIEW [dbo].[Library.Onet17Onet12MappingView]
AS

SELECT
	loom.Id,
	loom.Onet17Code,
	lo.OnetCode AS Onet12Code,
	lo.Id AS OnetId,
	loom.Occupation
FROM
	[Library.Onet17Onet12Mapping] AS loom WITH (NOLOCK)
	INNER JOIN [Library.Onet] AS lo WITH (NOLOCK) ON loom.OnetId = lo.Id
	





