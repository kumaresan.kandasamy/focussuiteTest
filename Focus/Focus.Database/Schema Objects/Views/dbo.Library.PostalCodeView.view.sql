﻿


CREATE VIEW [dbo].[Library.PostalCodeView]
AS
	
SELECT 
	pc.Id, 
	pc.Code,
	pc.Longitude,
	pc.Latitude,
	pc.CountryKey,
	cli.Value AS CountryName,
	pc.StateKey,
	sli.Value AS StateName,
	pc.CountyKey,
	cnli.Value AS CountyName,
	pc.CityName,
	l.Culture AS Culture
FROM 
	[Library.PostalCode] pc WITH (NOLOCK)
	INNER JOIN [Library.LocalisationItem] cli ON pc.CountryKey = cli.[Key]
	INNER JOIN [Library.Localisation] l ON l.id = cli.LocalisationId
	INNER JOIN [Library.LocalisationItem] sli ON pc.StateKey = sli.[Key] AND sli.LocalisationId = cli.LocalisationId 
	LEFT JOIN [Library.LocalisationItem] cnli ON pc.CountyKey = cnli.[Key] AND cnli.LocalisationId = cli.LocalisationId
		


