﻿CREATE VIEW [Data.Application.JobPostingOfInterestSentView]
AS
SELECT
	jp.Id,
	jp.CreatedOn AS SentOn,
	jp.PersonId,
	p.JobTitle,
	p.EmployerName,
	p.LensPostingId,
	jp.Applied,
	COALESCE(P.JobLocation, J.Location, '') AS JobLocation,
	jp.Score,
	j.JobStatus,
	jp.IsRecommendation
FROM
	[Data.Application.JobPostingOfInterestSent] jp
INNER JOIN [Data.Application.Posting] p 
	ON jp.PostingId = p.Id
LEFT JOIN [Data.Application.Job] j 
	ON jp.JobId = j.Id
