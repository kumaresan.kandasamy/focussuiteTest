﻿
CREATE Function [dbo].[CalculateDistance]
    (@Longitude1 Decimal(8,5),
    @Latitude1   Decimal(8,5),
    @Longitude2  Decimal(8,5),
    @Latitude2   Decimal(8,5))
Returns Float
As
Begin
Declare @Temp Float
 
Set @Temp = sin(@Latitude1/57.2957795130823) * sin(@Latitude2/57.2957795130823) + cos(@Latitude1/57.2957795130823) * cos(@Latitude2/57.2957795130823) * cos(@Longitude2/57.2957795130823 - @Longitude1/57.2957795130823)
 
if @Temp > 1
    Set @Temp = 1
Else If @Temp < -1
    Set @Temp = -1
 
Return (3958.75586574 * acos(@Temp) )
 
End
