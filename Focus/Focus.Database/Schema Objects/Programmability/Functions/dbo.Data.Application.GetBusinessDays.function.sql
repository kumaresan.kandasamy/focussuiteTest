﻿
-- ==================================================================
-- Author:		Jim Macbeth
-- Create date: 2012-01-05
-- Description:	Works out the number of business days between 2 dates
-- ==================================================================
CREATE FUNCTION [dbo].[Data.Application.GetBusinessDays]
(
	@startDate datetime,
	@endDate datetime
)
RETURNS int
AS
BEGIN
	DECLARE @days int

	SET @days = (DATEDIFF(dd, @startDate, @endDate) + 1) -(DATEDIFF(wk, @startDate, @endDate) * 2)
																											 -(CASE WHEN DATENAME(dw, @startDate) = 'Sunday' THEN 1 ELSE 0 END) 
																											 -(CASE WHEN DATENAME(dw, @endDate) = 'Saturday' THEN 1 ELSE 0 END)

	RETURN @days
END

