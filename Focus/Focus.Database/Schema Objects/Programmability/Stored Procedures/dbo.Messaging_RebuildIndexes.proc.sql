﻿CREATE PROCEDURE [dbo].[Messaging_RebuildIndexes]

AS
SET NOCOUNT ON

CREATE TABLE #IndexesToReorganise 
(
	IndexName nvarchar(100), 
	TableName nvarchar(100), 
	Processed bit
)
CREATE TABLE #IndexesToRebuild 
(
	IndexName nvarchar(100), 
	TableName nvarchar(100), 
	Processed bit
)

DECLARE @ErrorMessage nvarchar(4000), @ErrorSeverity int;

INSERT INTO #IndexesToReorganise
SELECT 
	name, 
	OBJECT_NAME(a.[object_id]), 
	0
FROM 
	sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS a
JOIN sys.indexes AS b 
	ON a.object_id = b.object_id 
	AND a.index_id = b.index_id
where 
	object_name(a.[object_id]) in ('Messaging.Message', 'Messaging.MessageLog', 'Messaging.MessagePayLoad')
    and avg_fragmentation_in_percent >= 5 and avg_fragmentation_in_percent < 30
    and name is not null

INSERT INTO #IndexesToRebuild
SELECT 
	name, 
	OBJECT_NAME(a.[object_id]), 
	0
FROM 
	sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS a
JOIN sys.indexes AS b 
	ON a.object_id = b.object_id 
	AND a.index_id = b.index_id
where 
	object_name(a.[object_id]) in ('Messaging.Message', 'Messaging.MessageLog', 'Messaging.MessagePayLoad')
    and avg_fragmentation_in_percent >= 30
    and name is not null

DECLARE @IndexName nvarchar(100);
DECLARE @TableName nvarchar(100);
DECLARE @SqlToRun nvarchar(255);

SELECT TOP 1 @IndexName = IndexName, @TableName = TableName FROM #IndexesToReorganise WHERE Processed = 0 AND IndexName IS NOT NULL

WHILE(@IndexName IS NOT NULL)
BEGIN
	SET @SqlToRun = 'ALTER INDEX [' + @IndexName + '] ON [' + @TableName + '] REORGANIZE;';

	BEGIN TRY
		BEGIN TRAN

		RAISERROR('Starting re-organisation of %s', 0, 1, @IndexName) WITH NOWAIT
		
		EXEC sp_executesql @SqlToRun;
		UPDATE #IndexesToReorganise SET Processed = 1 where IndexName = @IndexName;
		
		RAISERROR('%s re-organised', 0, 1, @IndexName) WITH NOWAIT
		RAISERROR('', 0, 1) WITH NOWAIT
		
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION
		
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY();
		RAISERROR(@ErrorMessage, @ErrorSeverity, 1);
		RETURN
	END CATCH

	SET @IndexName = NULL;
	SELECT TOP 1 @IndexName = IndexName, @TableName = TableName FROM #IndexesToReorganise WHERE Processed = 0 AND IndexName IS NOT NULL
END

SELECT @IndexName = IndexName, @TableName = TableName FROM #IndexesToRebuild WHERE Processed = 0 AND IndexName IS NOT NULL

WHILE(@IndexName IS NOT NULL)
BEGIN
	SET @SqlToRun = 'ALTER INDEX [' + @IndexName + '] ON [' + @TableName + '] REBUILD;';

	BEGIN TRY
		BEGIN TRAN

		RAISERROR('Starting re-build of %s', 0, 1, @IndexName) WITH NOWAIT
	
		EXEC sp_executesql @SqlToRun;
		UPDATE #IndexesToRebuild SET Processed = 1 where IndexName = @IndexName;
		
		RAISERROR('%s re-built', 0, 1, @IndexName) WITH NOWAIT
		RAISERROR('', 0, 1) WITH NOWAIT
		
		COMMIT TRAN
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION
		
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY();
		RAISERROR(@ErrorMessage, @ErrorSeverity, 1);
		RETURN
	END CATCH
	
	SET @IndexName = NULL;
	SELECT TOP 1 @IndexName = IndexName, @TableName = TableName FROM #IndexesToRebuild WHERE Processed = 0 AND IndexName IS NOT NULL
END
