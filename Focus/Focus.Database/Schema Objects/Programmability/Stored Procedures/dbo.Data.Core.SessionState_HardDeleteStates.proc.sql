﻿CREATE PROCEDURE [dbo].[Data.Core.SessionState_HardDeleteStates] 
	@BatchSize INT = 500
AS
BEGIN
	DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @ErrorSeverity INT
    DECLARE @ErrorState INT
    
	DECLARE @SessionStateIds TABLE
	(
		Id BIGINT PRIMARY KEY
	)
	
	DECLARE @Done BIT
	SET @Done = 0

	WHILE @Done = 0
	BEGIN
		INSERT INTO @SessionStateIds
		(
			Id
		)
		SELECT TOP (@BatchSize)
			SS.Id
		FROM
			[Data.Core.SessionState] SS
		WHERE
			SS.DeletedOn IS NOT NULL

		IF @@ROWCOUNT > 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				DELETE
				  SS
				FROM
					[Data.Core.SessionState] SS
				INNER JOIN @SessionStateIds TS
					ON TS.Id = SS.Id
					
				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION
				END
				
				SELECT 
					@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE()

				RAISERROR (@ErrorMessage, @ErrorSeverity,@ErrorState)
				
				RETURN
			END	CATCH
			
			DELETE FROM @SessionStateIds
		END
		ELSE
		BEGIN
			SET @Done = 1
		END
	END
END