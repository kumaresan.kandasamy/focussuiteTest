﻿CREATE PROCEDURE [dbo].[PageState_ExpirePageState] 
	@RemoveDays INT = NULL,
	@RemoveDate DATETIME = NULL,
	@BatchSize INT = 500
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @ErrorSeverity INT
    DECLARE @ErrorState INT
    
    IF @RemoveDays IS NOT NULL AND @RemoveDate IS NOT NULL
    BEGIN
		RAISERROR('Only one of @RemoveDays or @RemoveDate must be specified', 16, 1)
		RETURN
    END    
        
    IF @RemoveDays IS NULL AND @RemoveDate IS NULL
    BEGIN
		SET @RemoveDays = 1

		SELECT @RemoveDays = CAST(Value AS INT)
		FROM [Config.ConfigurationItem]
		WHERE [Key] = 'PageStateRentionDays'
    END 
    
	IF @RemoveDate IS NULL
	BEGIN
		SET @RemoveDate = CAST(DATEADD(DAY, 0 - @RemoveDays, GETUTCDATE()) AS DATE)
	END    
	
	DECLARE @PageStateIds TABLE
	(
		Id UNIQUEIDENTIFIER PRIMARY KEY
	)

	DECLARE @Done BIT
	SET @Done = 0

	WHILE @Done = 0
	BEGIN
		INSERT INTO @PageStateIds
		(
			Id
		)
		SELECT TOP (@BatchSize)
			PS.Id
		FROM
			[Data.Core.PageState] PS
		WHERE
			PS.LastAccessed < @RemoveDate	
		
		IF @@ROWCOUNT > 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				DELETE
					PS
				FROM
					[Data.Core.PageState] PS
				INNER JOIN @PageStateIds TPS
					ON TPS.Id = PS.Id
				
				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION
				END
				
				SELECT 
					@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE()

				RAISERROR (@ErrorMessage, @ErrorSeverity,@ErrorState)
				
				RETURN
			END	CATCH
				
			DELETE FROM @PageStateIds
		END
		ELSE
		BEGIN
			SET @Done = 1
		END
	END
END
GO
