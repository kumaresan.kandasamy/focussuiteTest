﻿CREATE PROCEDURE [dbo].[Log.LogEvent_ExpireLogEvents] 
	@RemoveDays INT = NULL,
	@RemoveDate DATETIME = NULL,
	@BatchSize INT = 500
AS
BEGIN
	SET NOCOUNT ON
	
    DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @ErrorSeverity INT
    DECLARE @ErrorState INT
    
    IF @RemoveDays IS NOT NULL AND @RemoveDate IS NOT NULL
    BEGIN
		RAISERROR('Only one of @RemoveDays or @RemoveDate must be specified', 16, 1)
		RETURN
    END    
        
    IF @RemoveDays IS NULL AND @RemoveDate IS NULL
    BEGIN
		SET @RemoveDays = 30

		SELECT @RemoveDays = CAST(Value AS INT)
		FROM [Config.ConfigurationItem]
		WHERE [Key] = 'LogEventRetentionDays'
    END 
    
	IF @RemoveDate IS NULL
	BEGIN
		SET @RemoveDate = CAST(DATEADD(DAY, 0 - @RemoveDays, GETDATE()) AS DATE)
	END
	
	DECLARE @LogEventIds TABLE
	(
		Id BIGINT PRIMARY KEY
	)

	DECLARE @Done BIT
	SET @Done = 0

	WHILE @Done = 0
	BEGIN
		INSERT INTO @LogEventIds
		(
			Id
		)
		SELECT TOP (@BatchSize)
			LE.Id
		FROM
			[Log.LogEvent] LE
		WHERE
			LE.EventDate < @RemoveDate	
		
		IF @@ROWCOUNT > 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				DELETE
					LE
				FROM
					[Log.LogEvent] LE
				INNER JOIN @LogEventIds TLE
					ON TLE.Id = LE.Id
					
				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION
				END
				
				SELECT 
					@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE()

				RAISERROR (@ErrorMessage, @ErrorSeverity,@ErrorState)
				
				RETURN
			END	CATCH
				
			DELETE FROM @LogEventIds
		END
		ELSE
		BEGIN
			SET @Done = 1
		END
	END
END