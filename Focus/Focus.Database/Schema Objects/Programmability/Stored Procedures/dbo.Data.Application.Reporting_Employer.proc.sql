﻿


CREATE PROCEDURE [dbo].[Data.Application.Reporting_Employer]
	@DateFrom	DATETIME,
	@DateTo	DATETIME,
	@County	NVARCHAR(MAX),
	@Office NVARCHAR(MAX),
	@WIB NVARCHAR(MAX),
	@Radius INT,
	@ZIP NVARCHAR(20),
	@OccupationGroup NVARCHAR(MAX),
	@DetailedOccupation NVARCHAR(MAX),
	@SalaryFrequency INT,
	@MinSalary	DECIMAL,
	@MaxSalary	DECIMAL,
	@JobStatus	INT,
	@EducationLevel	INT,
	@SearchTerms NVARCHAR(MAX),
	@SearchType INT,
	@KeyWordsInJobTitle BIT,
	@TargetedSectors NVARCHAR(MAX), -- To do
	@WOTCInterests NVARCHAR(MAX),
	@FEIN NVARCHAR(10),
	@EmployerName NVARCHAR(50),
	@JobTitle NVARCHAR(50),
	@Flags INT,
	@SearchAccountCreationDate BIT,
	@SearchAssistanceRequested BIT,
	@Compliance INT,
	@SuppressJobOrder BIT,
	@SelfService BIT,
	@StaffAssisted BIT,
	@Spidered BIT,
	@PageNumber	INT,
	@PageSize	INT,
	@SortOrder	VARCHAR(20)

AS
DECLARE @JobCount TABLE
(
	EmployerId int,
	JobCount int
)
DECLARE @Counties TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @Offices TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @WIBs TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @ONetOccupationGroups TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @ONetDetailedOccupations TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @TargetedSector TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @WOTCInterest TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @KeywordSearchTerms TABLE
(		
	Id int,
	Value nvarchar(100),
	Processed bit default 0
)
DECLARE @Zips TABLE
(
	ZipCode varchar(10)
)
CREATE TABLE #JobCounter
(
	Id INT, 
	EmployerName NVARCHAR(50),
	ContactFullName NVARCHAR(100), 
	City NVARCHAR(100), 
	AccountCreationDate DATETIME,
	JobId INT,
	JobCount INT DEFAULT(0)
)
CREATE TABLE #KeyWordEmployer
(
	Id	bigint
)

DECLARE @KeyWordSearchSql nvarchar(max)
DECLARE @CurrentKeyWord nvarchar(max)
DECLARE @CurrentKeyWordId nvarchar(max)
DECLARE @ProcessKeyWords BIT
DECLARE @SearchOperator nvarchar(4)
set @ProcessKeyWords = 0
set @KeyWordSearchSql= ''

if @SearchType = 1
	set @SearchOperator = 'and'
else if @SearchType = 2 or @KeyWordsInJobTitle = 1
	set @SearchOperator = 'or'


INSERT INTO @KeywordSearchTerms
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@SearchTerms,''), '|');

INSERT INTO @ONetDetailedOccupations
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@DetailedOccupation, ''), '|');


INSERT INTO @ONetOccupationGroups
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@OccupationGroup, ''), '|');
		
INSERT INTO @Counties
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@County, ''), '|');
	
INSERT INTO @Offices
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@Office, ''), '|');
	
INSERT INTO @WIBs
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@WIB, ''), '|');


INSERT INTO @TargetedSector
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@TargetedSectors, ''), '|');
	
INSERT INTO @WOTCInterest
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@WOTCInterests, ''), '|');


if (rtrim(ltrim(@ZIP)) <> '')
begin
	insert into @Zips
	select
		z2.ZipCode
	from
		[Data.Report.ZipCodes] z1
	cross join [Data.Report.ZipCodes] z2
	where
		z1.zipcode = rtrim(ltrim(@ZIP))
	and dbo.CalculateDistance(z1.Longitude, z1.Latitude, z2.Longitude, z2.Latitude) < @Radius
end
else
begin
	insert into @Zips
	select null
end

if exists (select 1 from @KeywordSearchTerms where Value is not null)
begin
	set @ProcessKeyWords = 1
	while exists (select 1 from @KeywordSearchTerms where Processed = 0)
	begin
		select top 1 @CurrentKeyWordId = Id, @CurrentKeyWord = Value from @KeywordSearchTerms where Processed = 0
		
		if LEN(@KeyWordSearchSql) > 0
			set @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '
		else
			set @KeyWordSearchSql = ' '	
		
		if @KeyWordsInJobTitle = 1 -- We only need to worry about job title and not the others
			set @KeyWordSearchSql = @KeyWordSearchSql + 'jo.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
		else
		begin
			set @KeyWordSearchSql = @KeyWordSearchSql + '(jo.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
			set @KeyWordSearchSql = @KeyWordSearchSql + ' or bu.Name like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
			set @KeyWordSearchSql = @KeyWordSearchSql + ' or jo.Description like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'')'
		end
						
		update @KeywordSearchTerms set Processed = 1 where @CurrentKeyWordId = Id and @CurrentKeyWord = Value
	end
	set @KeyWordSearchSql = 'insert into #KeyWordEmployer (id) select distinct e.id from [Data.Application.Employee] e left join [Data.Application.Job] jo on e.Id = jo.EmployeeId where ' + @KeyWordSearchSql
	execute sp_executesql @KeyWordSearchSql;
end
else
begin
	insert into #KeyWordEmployer (Id) Values (NULL)
end;
	
INSERT INTO #JobCounter
(
	Id, 
	EmployerName,
	ContactFullName, 
	City, 
	AccountCreationDate,
	JobId
)
select DISTINCT
	bu.Id, 
	bu.Name,
	p.FirstName + ' ' + p.LastName, 
	bua.TownCity + ', ' +  CAST(bua.CountyId AS nvarchar(10)), --stli.Value, 
	u.CreatedOn,
	jo.Id
	
--from BusinessUnit bu
--inner join EmployeeBusinessUnit ebu
--	on ebu.BusinessUnitId = bu.Id
--inner join Employee e
--	on e.Id = ebu.EmployeeId

-- from Employee e	
--inner join EmployeeBusinessUnit ebu
--	on e.Id = ebu.EmployeeId
--inner join BusinessUnit bu
--	on ebu.BusinessUnitId = bu.Id

from [Data.Application.EmployeeBusinessUnit] ebu
inner join [Data.Application.Employee] e
on e.Id = ebu.EmployeeId
inner join [Data.Application.BusinessUnit] bu
on bu.Id = ebu.BusinessUnitId
		
left join [Data.Application.Employer] emp
	on emp.Id = e.EmployerId
	
left join [Data.Application.Job] jo on e.Id = jo.EmployeeId
	
left join [Data.Application.Person] p	on e.PersonId = p.Id
	
left join [Data.Application.BusinessUnitAddress] bua on bu.Id = bua.BusinessUnitId
	
left join [Data.Application.User] u	on p.Id = u.PersonId

--left join CodeItem coci	on coci.Id = bua.CountyId
--left join LocalisationItem coli	on coli.[Key] = coci.[Key]
	
--left join CodeItem sfci	on sfci.Id = jo.SalaryFrequencyId
--left join LocalisationItem sfli	on sfli.[Key] = sfci.[Key]
	
--left join CodeItem stci	on stci.Id = bua.StateId
--left join LocalisationItem stli	on stli.[Key] = stci.[Key]
		
--left join Onet o on  o.Id = jo.OnetId
--left join CodeItem jfci	on jfci.[Id] = o.JobFamilyId
--left join LocalisationItem jfli	on jfli.[Key] = jfci.[key]	
	
--left join @Counties joc on coli.Value LIKE '%' + joc.Value + '%' OR joc.Value IS NULL
left join @Offices joo on bua.TownCity LIKE '%' + joo.Value + '%' OR joo.Value IS NULL
left join @Zips z on bua.PostcodeZip = z.ZipCode or z.ZipCode IS NULL
left join #KeyWordEmployer kst on kst.Id = e.Id or @ProcessKeyWords = 0
left join @WOTCInterest wi on jo.WorkOpportunitiesTaxCreditHires LIKE '%' + wi.Value + '%' OR wi.Value IS NULL
--left join @ONetOccupationGroups oog	on jfli.Value LIKE '%' + oog.Value + '%' OR oog.Value IS NULL
--left join @ONetDetailedOccupations odo on o.[Key] LIKE '%' + odo.Value + '%' OR odo.Value IS NULL

WHERE
	(ISNULL(@SalaryFrequency,0) = 0
	OR
		(jo.SalaryFrequencyId = @SalaryFrequency
		AND	jo.MinSalary < @MaxSalary
		AND jo.MaxSalary > @MinSalary
		)
	)
and (@JobStatus = 0 OR jo.JobStatus & @JobStatus <> 0)
and (@EducationLevel = 0 OR jo.MinimumEducationLevel & @EducationLevel <> 0)
and ISNULL(emp.FederalEmployerIdentificationNumber, '') LIKE '%' + ISNULL(@FEIN, '') + '%'
and ISNULL(bu.Name, '') LIKE '%' + ISNULL(@EmployerName,'') + '%'
and ISNULL(jo.JobTitle, '') LIKE '%' + ISNULL(@JobTitle,'') + '%'
--and (@Flags = 0 OR jo.JobFlag & @Flags <> 0)
and ((jo.ScreeningPreferences > 0 and @SearchAssistanceRequested = 1) or @SearchAssistanceRequested = 0)
--and (@Compliance = 0 OR jo.Compliance & @Compliance <> 0)
--and ((jo.SuppressJobOrder = 1 and @SuppressJobOrder = 1) or @SuppressJobOrder = 0)
--and ((jo.IsSelfService = 1 and @SelfService = 1) or @SelfService = 0)
--and ((jo.IsStaffAssisted = 1 and @StaffAssisted = 1) or @StaffAssisted = 0)
--and ((jo.Spidered = 1 and @Spidered = 1) or @Spidered = 0)
and ((@SearchAccountCreationDate = 1 and u.CreatedOn between @DateFrom and @DateTo) or @SearchAccountCreationDate = 0)

	
INSERT INTO @JobCount
(
	EmployerId,
	JobCount
)
SELECT
	jc.Id,
	COUNT(1)
FROM
	#JobCounter jc
WHERE
	jc.JobId IS NOT NULL
GROUP BY
	jc.Id;


with joborders as
(
	select
		CASE lower(@SortOrder)
			when 'employer_asc' then ROW_NUMBER()OVER (ORDER BY joi.EmployerName ASC, joi.Id ASC)
			when 'contact_asc' then ROW_NUMBER()OVER (ORDER BY joi.ContactFullName ASC, joi.Id ASC)
			when 'city_asc' then ROW_NUMBER()OVER (ORDER BY joi.City ASC, joi.Id ASC)
			when 'listings_asc' then ROW_NUMBER()OVER (ORDER BY joi.Listings ASC, joi.Id ASC)
			when 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY joi.AccountCreationDate ASC, joi.Id ASC)
			when 'accountcreation_desc' then ROW_NUMBER()OVER (ORDER BY joi.AccountCreationDate DESC, joi.Id ASC)
			when 'listings_desc' then ROW_NUMBER()OVER (ORDER BY joi.Listings DESC, joi.Id ASC)
			when 'city_desc' then ROW_NUMBER()OVER (ORDER BY joi.City DESC, joi.Id ASC)
			when 'contact_desc' then ROW_NUMBER()OVER (ORDER BY joi.ContactFullName DESC, joi.Id ASC)
			when 'employer_desc' then ROW_NUMBER()OVER (ORDER BY joi.EmployerName DESC, joi.Id ASC)
			else ROW_NUMBER()OVER (ORDER BY joi.EmployerName ASC, joi.Id ASC)
		END as pagingId,
		CASE @SortOrder
			when 'employer_asc' then ROW_NUMBER()OVER (ORDER BY joi.EmployerName DESC, joi.Id DESC)
			when 'contact_asc' then ROW_NUMBER()OVER (ORDER BY joi.ContactFullName DESC, joi.Id DESC)
			when 'city_asc' then ROW_NUMBER()OVER (ORDER BY joi.City DESC, joi.Id DESC)
			when 'listings_asc' then ROW_NUMBER()OVER (ORDER BY joi.Listings DESC, joi.Id DESC)
			when 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY joi.AccountCreationDate DESC, joi.Id DESC)
			when 'accountcreation_desc' then ROW_NUMBER()OVER (ORDER BY joi.AccountCreationDate ASC, joi.Id DESC)
			when 'listings_desc' then ROW_NUMBER()OVER (ORDER BY joi.Listings ASC, joi.Id DESC)
			when 'city_desc' then ROW_NUMBER()OVER (ORDER BY joi.City ASC, joi.Id DESC)
			when 'contact_desc' then ROW_NUMBER()OVER (ORDER BY joi.ContactFullName ASC, joi.Id DESC)
			when 'employer_desc' then ROW_NUMBER()OVER (ORDER BY joi.EmployerName ASC, joi.Id DESC)
			else ROW_NUMBER()OVER (ORDER BY joi.EmployerName DESC, joi.Id DESC)
		END as pagingRevId,
		joi.Id,
		joi.EmployerName, 
		joi.ContactFullName, 
		joi.City, 
		joi.AccountCreationDate,
		joi.Listings
	from
	(
		SELECT DISTINCT
			jc.Id,
			jc.EmployerName, 
			jc.ContactFullName, 
			jc.City, 
			jc.AccountCreationDate,
			ISNULL(jco.JobCount,0) AS Listings
		FROM
			#JobCounter jc
		left join @JobCount jco on jc.Id =  jco.EmployerId
	) joi	
)
select
	jo.Id,
	jo.pagingId,
	jo.pagingRevId,
	jo.EmployerName,
	jo.ContactFullName, 
	jo.City, 
	jo.Listings,
	jo.AccountCreationDate
from
	joborders jo
where
	pagingId between  ((@PageNumber - 1) * @PageSize) + 1 and (@PageNumber * @PageSize)
order by
	pagingId asc










