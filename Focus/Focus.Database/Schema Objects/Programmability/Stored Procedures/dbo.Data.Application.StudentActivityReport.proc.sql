﻿
CREATE PROCEDURE [dbo].[Data.Application.StudentActivityReport]
	@StartDate DATETIME,
	@EndDate DATETIME
AS
BEGIN

DECLARE @CalculatedTotals TABLE
(
	Row BIGINT,
	SinceInception INT, 
	ThisPeriod INT
)

DECLARE @FinalResult TABLE 
(
	Row BIGINT,
	Activity VARCHAR(250), 
	SinceInception INT, 
	ThisPeriod INT
)

--------------------------
-- REGISTRATION Queries --
--------------------------

INSERT INTO @CalculatedTotals (Row, SinceInception, ThisPeriod)	
SELECT 
	1,
	COUNT(Id),
	NULL
FROM 
	dbo.[Data.Application.RegistrationPin]

INSERT INTO @CalculatedTotals (Row, SinceInception, ThisPeriod)	
SELECT 
	2, 
	COUNT(Id),
	COUNT(CASE WHEN CreatedOn >= @StartDate THEN 1 END)
FROM 
	dbo.[Data.Application.User]
WHERE 
	UserType IN (4, 8, 12)
	AND Pin IS NOT NULL
	AND CreatedOn < @EndDate

--------------------
-- RESUME Queries --
--------------------

;WITH Resumes (PersonId, ActionType, FirstCompletedDate) AS
(
	SELECT
		R.PersonId,
		T.Name,
		MIN(AE.ActionedOn)
	FROM
		[Data.Core.ActionEvent] AE
	INNER JOIN [Data.Core.ActionType] T
		ON T.Id = AE.ActionTypeId
	INNER JOIN [Data.Application.Resume] R
		ON R.Id = AE.EntityId
	WHERE 
		T.Name IN ('CompleteResume', 'SearchableResume')
		AND AE.ActionedOn < @EndDate
	GROUP BY
		R.PersonId,
		T.Name
)
INSERT INTO @CalculatedTotals (Row, SinceInception, ThisPeriod)				
SELECT
	CASE R.ActionType
		WHEN 'CompleteResume' THEN 3
		ELSE 4
	END,
	COUNT(1),
	COUNT(CASE WHEN FirstCompletedDate >= @StartDate THEN 1 END)
FROM
	Resumes R
GROUP BY
	R.ActionType

--------------------------------------------------------
-- ACTIONS (Searches, Visits, Views and Apply Clicks) --
--------------------------------------------------------

INSERT INTO @CalculatedTotals (Row, SinceInception, ThisPeriod)				
SELECT 
	5,
	COUNT(1),
	COUNT(CASE WHEN AE.ActionedOn >= @StartDate THEN 1 END)
FROM 
	[dbo].[Data.Core.ActionEvent] AE
INNER JOIN [dbo].[Data.Core.ActionType] T 
	ON T.Id = AE.ActionTypeId
INNER JOIN [dbo].[Data.Application.User] U 
	ON U.Id = AE.EntityId
WHERE 
	T.Name = 'LogIn'
	AND U.UserType IN (4, 8, 12)
	AND AE.ActionedOn < @EndDate

INSERT INTO @CalculatedTotals (Row, SinceInception, ThisPeriod)				
SELECT 
	CASE T.Name
		WHEN 'RunManualJobSearch' THEN 6
		WHEN 'JobSearch' THEN 7
		ELSE 9
	END,
	COUNT(1),
	COUNT(CASE WHEN AE.ActionedOn >= @StartDate THEN 1 END)
FROM 
	[dbo].[Data.Core.ActionEvent] AE
INNER JOIN [dbo].[Data.Core.ActionType] T 
	ON T.Id = AE.ActionTypeId
INNER JOIN [dbo].[Data.Application.Person] P
	ON P.Id = AE.EntityId
INNER JOIN [dbo].[Data.Application.User] U
	ON U.PersonId = P.Id
WHERE 
	T.Name IN ('RunManualJobSearch', 'JobSearch', 'ViewJobDetails')
	AND U.UserType IN (4, 8, 12)
	AND AE.ActionedOn < @EndDate
GROUP BY
	T.Name

INSERT INTO @CalculatedTotals (Row, SinceInception, ThisPeriod)				
SELECT 
	10,
	COUNT(1),
	COUNT(CASE WHEN AE.ActionedOn >= @StartDate THEN 1 END)
FROM 
	[dbo].[Data.Core.ActionEvent] AE
INNER JOIN [dbo].[Data.Core.ActionType] T 
	ON T.Id = AE.ActionTypeId
INNER JOIN [dbo].[Data.Application.Person] P
	ON P.Id = AE.EntityIdAdditional01
INNER JOIN [dbo].[Data.Application.User] U
	ON U.PersonId = P.Id
WHERE 
	T.Name IN ('RegisterHowToApply', 'RegisterHowToApplySpidered')
	AND U.UserType IN (4, 8, 12)
	AND AE.ActionedOn < @EndDate
 
--------------------
-- SAVED SEARCHES --
--------------------

INSERT INTO @CalculatedTotals (Row, SinceInception, ThisPeriod)				
SELECT
	8,
	COUNT(1),
	COUNT(CASE WHEN SS.CreatedOn >= @StartDate THEN 1 END)
FROM
	[Data.Application.SavedSearch] SS 
INNER JOIN [Data.Application.SavedSearchUser] SSU
	ON SSU.SavedSearchId = SS.Id 
INNER JOIN [Data.Application.User] AS U 
	ON U.Id = SSU.UserId
WHERE 
	U.UserType IN (4, 8, 12)
	AND SS.CreatedOn < @EndDate  
	AND SS.[Type] = 2
	
-------------
-- RESULTS --
-------------

INSERT INTO @FinalResult (Row, Activity, SinceInception, ThisPeriod)
VALUES
	(1, 'Invitations sent', 0, NULL),
	(2, 'Students registered', 0, 0),
	(3, 'Number of students with completed resumes', 0, 0),
	(4, 'Number of students with searchable resume', 0, 0),
	(5, 'Number of visits (logins)', 0, 0),
	(6, 'Number of searches run (Manual)', 0, 0),
	(7, 'Number of searches run (All)', 0, 0),
	(8, 'Number of saved searches', 0, 0),
	(9, 'Number of jobs viewed', 0, 0),
	(10, 'Number of "Apply" clicks', 0, 0)

UPDATE 
	FR
SET
	SinceInception = ISNULL(CT.SinceInception, 0),
	ThisPeriod = CASE FR.Row WHEN 1 THEN NULL ELSE ISNULL(CT.ThisPeriod, 0) END
FROM
	@FinalResult FR
INNER JOIN @CalculatedTotals CT
	ON CT.Row = FR.Row

SELECT 
	Row AS Id,
	[Activity], 
	SinceInception, 
	ThisPeriod 
FROM 
	@FinalResult
ORDER BY
	Row
END
GO
