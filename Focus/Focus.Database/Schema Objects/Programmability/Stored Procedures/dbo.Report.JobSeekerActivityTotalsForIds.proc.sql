﻿
-- =============================================
-- Author:		Tim Case
-- Create date: 21 October 2013
-- Description:	Get report totals for job seeker actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerActivityTotalsForIds]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@Category NVARCHAR(500),
	@Activity NVARCHAR(500) = NULL,
	@JobSeekerIds XML
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #JobSeekerIds
	(
		Id BIGINT
	)
	
	INSERT INTO #JobSeekerIds ( Id )
    SELECT t.value('.', 'bigint')
    FROM @JobSeekerIds.nodes('//id') AS x(t) 
	
	SELECT
		JSA.JobSeekerId AS Id,
		SUM(JSA.AssignmentsMade) AS AssignmentsMade
	FROM 
		[Report.JobSeekerActivityAssignment] JSA
	INNER JOIN #JobSeekerIds JSI
		ON JSA.JobSeekerId = JSI.Id
	WHERE 
		JSA.AssignDate BETWEEN @FromDate AND @ToDate
		AND JSA.ActivityCategory = @Category
		AND JSA.Activity = ISNULL(@Activity, JSA.Activity)
	GROUP BY
		JSA.JobSeekerId
END
