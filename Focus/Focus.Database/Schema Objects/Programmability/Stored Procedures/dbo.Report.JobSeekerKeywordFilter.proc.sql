﻿

-- =============================================
-- Author:		Tim Case
-- Create date: 13 September 2013
-- Description:	Filter job seekers by keyword
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerKeywordFilter]
	@Keywords XML,
	@TypesToCheck XML,
	@SearchAll BIT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #Keywords
	(
		Keyword NVARCHAR(500)
	)
	
	DECLARE @Types TABLE
	(
		KeywordType INT
	)
	
	DECLARE @KeywordsToCheck INT
	
	INSERT INTO #KeyWords ( Keyword )
    SELECT '%' + t.value('.', 'varchar(500)') + '%'
    FROM @Keywords.nodes('//keyword') AS x(t) 
    
  SET @KeywordsToCheck = CASE @SearchAll WHEN 1 THEN @@ROWCOUNT ELSE 1 END

	INSERT INTO @Types ( KeywordType )
    SELECT t.value('.', 'int')
    FROM @TypesToCheck.nodes('//type') AS x(t) 

	SELECT 
		JSD.JobSeekerId AS Id
	FROM
		[Report.JobSeekerData] JSD
	INNER JOIN #Keywords K
		ON JSD.[Description] LIKE K.Keyword
	WHERE
		JSD.DataType IN (SELECT KeywordType FROM @Types)
	GROUP BY
		JSD.JobSeekerId
	HAVING
		COUNT(DISTINCT K.Keyword) >= @KeywordsToCheck
END
