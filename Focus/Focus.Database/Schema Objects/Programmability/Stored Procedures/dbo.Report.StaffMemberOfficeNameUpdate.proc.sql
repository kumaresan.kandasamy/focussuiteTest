﻿CREATE PROCEDURE [dbo].[Report.StaffMemberOfficeNameUpdate] 
	@OfficeId bigint
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
	 RSM
	SET 
	 Office = STUFF(
	  (SELECT 
		'||' + SMO.OfficeName
	   FROM 
		[Report.StaffMember] RSM2
	   INNER JOIN [Report.StaffMemberOffice] SMO
		ON SMO.StaffMemberId = RSM2.Id
	   WHERE
		RSM2.Id = RSM.Id
	   ORDER BY
		SMO.OfficeName
	   FOR XML PATH('')
	  ), 1, 2, '')
	FROM
	 [Report.StaffMember] RSM
	WHERE
	 EXISTS (SELECT 1 FROM [Report.StaffMemberOffice] SMO2 WHERE SMO2.StaffMemberId = RSM.Id AND SMO2.OfficeId = @OfficeId)
END
