﻿


CREATE PROCEDURE [dbo].[PageState_SavePageState] (@id UNIQUEIDENTIFIER, @value IMAGE, @timeout INT = 20) AS
	SET NOCOUNT ON

	IF @id IS NULL 
	BEGIN
		RETURN -1
	END 
		ELSE IF @timeout < 1 
			BEGIN
				RETURN -2
			END 
			ELSE IF @value IS NULL 
				BEGIN
					RETURN -3
				END

	IF EXISTS(SELECT * FROM dbo.[Data.Core.PageState] WHERE Id = @id) 
	BEGIN  
		UPDATE dbo.[Data.Core.PageState]
			SET LastAccessed = GETUTCDATE(),
			Value = @value
		WHERE 
			Id = @id
	END ELSE 
		BEGIN
			INSERT INTO dbo.[Data.Core.PageState] (Id, Value, LastAccessed, [Timeout]) 
			VALUES (@id, @value, GETUTCDATE(), @timeout)
		END

	RETURN 0






