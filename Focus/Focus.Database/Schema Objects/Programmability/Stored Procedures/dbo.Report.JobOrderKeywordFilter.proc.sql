﻿

-- =============================================
-- Author:		Tim Case
-- Create date: 26 September 2013
-- Description:	Filter job Orders by keyword
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobOrderKeywordFilter]
	@Keywords XML,
	@CheckTitle BIT,
	@CheckDescription BIT,
	@CheckEmployer BIT,
	@SearchAll BIT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #Keywords
	(
		KeywordId INT IDENTITY(1, 1),
		TitleKeyword NVARCHAR(500),
		DescriptionKeyword NVARCHAR(500),
		EmployerKeyword NVARCHAR(500)
	)
	
	DECLARE @KeywordsToCheck INT
	
	INSERT INTO #KeyWords ( TitleKeyword, DescriptionKeyword, EmployerKeyword )
    SELECT
		CASE @CheckTitle WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckDescription WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckEmployer WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END
    FROM @Keywords.nodes('//keyword') AS x(t) 
    
	SET @KeywordsToCheck = CASE @SearchAll WHEN 1 THEN @@ROWCOUNT ELSE 1 END

	SELECT 
		J.Id
	FROM 
		[Report.JobOrder] J
	INNER JOIN [Report.Employer] E
		ON E.Id = J.EmployerId
	INNER JOIN #Keywords K
		ON J.JobTitle LIKE K.TitleKeyword
		OR J.[Description] LIKE K.DescriptionKeyword
		OR E.Name LIKE K.EmployerKeyword
	GROUP BY 
		J.Id
	HAVING
		COUNT(DISTINCT K.KeywordId) >= @KeywordsToCheck
END
