﻿








-- ==============================================================================================
-- Author:		Andy Jones
-- Create date: 11 September 2013
-- Description:	Get Person Matches For A Recently Placed Person Ordered By Business Unit name asc
-- ==============================================================================================
CREATE PROCEDURE [dbo].[Data_Application_GetRecentlyPlacedPersonMatchesOrderByBusinessUnitNameAsc]
	@UserId		bigint,
	@BusinessUnitId bigint = null,
	@BusinessUnitName nvarchar(200) = null,
	@FilterByOffices bit = 1,
	@OfficeId bigint = null,
	@MatchCount int = 6,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
SET NOCOUNT ON;

DECLARE @FirstRec int
DECLARE @LastRec int

DECLARE @Matches AS TABLE
(
	Id bigint,
	MatchedPersonId bigint,
	MatchedCandidateName nvarchar(101),
	RecentlyPlacedId bigint,
	PlacedPersonId bigint,
	PlacedCandidateName nvarchar(101),
	Score int,
	BusinessUnitId bigint,
	BusinessUnitName nvarchar(200),
	EmployerId bigint,
	EmployerName nvarchar(200),
	PlacementDate datetime,
	JobId bigint,
	JobTitle nvarchar(200),
	HasMatchesToOpenPositions bit
)

DECLARE @UsersIgnoreMatches AS TABLE
(
	RecentlyPlacedMatchId bigint
) 

DECLARE @RankedMatches AS TABLE
(
	RecentlyPlacedMatchId bigint,
	[Rank] int
)

DECLARE @RecentlyPlacedMatch AS TABLE
(
	TableId int IDENTITY PRIMARY KEY,
	RecentlyPlacedId bigint
)


-- Get the matches based on the filters
INSERT INTO @Matches
SELECT 
	rpm.Id,
	rpm.PersonId,
	null,
	rpm.RecentlyPlacedId,
	rp.PersonId,
	null,
	rpm.Score,
	bu.Id,
	bu.Name,
	j.EmployerId,
	null,
	rp.PlacementDate,  
	j.Id,
	null,
	null
FROM
	[Data.Application.RecentlyPlacedMatch] rpm WITH (NOLOCK)
	INNER JOIN [Data.Application.RecentlyPlaced] rp WITH (NOLOCK) ON rp.Id = rpm.RecentlyPlacedId 
	INNER JOIN [Data.Application.Job] j WITH (NOLOCK) ON j.Id = rp.JobId
	INNER JOIN [Data.Application.BusinessUnit] bu WITH (NOLOCK) ON bu.id = j.BusinessUnitId
WHERE
	(bu.Id = @BusinessUnitId OR @BusinessUnitId IS NULL)
	AND (bu.Name LIKE '%' + @BusinessUnitName + '%' OR @BusinessUnitName is null)
	
--Filter by Offices
DECLARE @OfficeEmployer AS TABLE
(
	EmployerId bigint
) 
	
IF @OfficeId is not null
BEGIN
	INSERT INTO @OfficeEmployer
	SELECT 
		EmployerId 
	FROM 
		[Data.Application.EmployerOfficeMapper] WITH (NOLOCK)
	WHERE OfficeId = @OfficeId
END

IF @FilterByOffices = 1 AND @OfficeId is null
BEGIN
	INSERT INTO @OfficeEmployer
	SELECT DISTINCT eom.EmployerId
	FROM 
		[Data.Application.EmployerOfficeMapper] eom WITH (NOLOCK)
		INNER JOIN [Data.Application.PersonOfficeMapper] pom WITH (NOLOCK) ON eom.OfficeId = pom.OfficeId
		INNER JOIN [Data.Application.User] u WITH (NOLOCK) ON pom.PersonId = u.PersonId
	WHERE u.Id = @UserId
END

IF @FilterByOffices = 1 OR @OfficeId is not null
BEGIN
	DELETE FROM @Matches 
	WHERE EmployerId NOT IN (SELECT EmployerId FROM @OfficeEmployer) 
END
		
-- Get the users matches to ignore
INSERT INTO @UsersIgnoreMatches
SELECT
	m.Id
FROM 
	[Data.Application.RecentlyPlacedMatchesToIgnore] rpmti WITH (NOLOCK)
	INNER JOIN @Matches m ON rpmti.PersonId = m.MatchedPersonId AND rpmti.RecentlyPlacedId = m.RecentlyPlacedId
WHERE
	rpmti.UserId = @UserId
	
-- Remove the ignore matches
DELETE FROM @Matches
WHERE
	Id IN (SELECT RecentlyPlacedMatchId FROM @UsersIgnoreMatches)
 
-- Rank the matches per recently placed
INSERT INTO @RankedMatches
SELECT
	Id,
	RANK() OVER (PARTiTION BY RecentlyPlacedId ORDER BY Score DESC, Id DESC) AS Rank 
FROM 
	@Matches
	
-- Remove RankedMatches above the match count required
DELETE FROM @RankedMatches
WHERE
	[RANK] > @MatchCount
	
-- Remove Matches not in RankedMatches
DELETE FROM @Matches
WHERE
	Id NOT IN (SELECT RecentlyPlacedMatchId FROM @RankedMatches)
	
-- Populate the RecentlyPlacedIds for paging
INSERT INTO @RecentlyPlacedMatch 
SELECT 
	RecentlyPlacedId
FROM 
	@Matches
GROUP BY 
	RecentlyPlacedId, BusinessUnitName
ORDER BY 
	BusinessUnitName
	
SELECT @RowCount = @@ROWCOUNT	

-- Delete RecentlyPlacedMatch not in paged data
DELETE @RecentlyPlacedMatch
WHERE 
	NOT (TableId > @FirstRec AND TableId < @LastRec)
	
-- Delete Matches not in paged data
DELETE @Matches 
WHERE
	NOT RecentlyPlacedId IN (SELECT RecentlyPlacedId FROM  @RecentlyPlacedMatch) 
	

-- Get rest of the data
DECLARE @PersonPostingMatchesByEmployer AS TABLE
(
	Id bigint,
	PersonId bigint,
	EmployerId bigint,
	PostingId bigint
)

DECLARE @UsersPersonPostingMatchesToIgnore AS TABLE
(
	PersonPostingMatchId bigint
)

DECLARE @PersonPostingMatchesGroupedByRecentPlacementMatchId AS TABLE
(
	RecentPlacementMatchId bigint,
	HasJobMatches bit	
)

-- Only get the matches for Persons and Employers in current result set
INSERT INTO @PersonPostingMatchesByEmployer
SELECT
	ppm.Id,
	ppm.PersonId,
	j.EmployerId,
	ppm.PostingId
FROM
	[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) 
	INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON ppm.PostingId = p.Id
	INNER JOIN [Data.Application.Job] j WITH (NOLOCK) ON p.JobId = j.Id
WHERE 
	ppm.PersonId IN (SELECT MatchedPersonId FROM @Matches)
	AND j.EmployerId IN (SELECT EmployerId FROM @Matches)

-- Get the users person posting matches to ignore
INSERT INTO @UsersPersonPostingMatchesToIgnore
SELECT
	ppme.Id
FROM 
	[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
	INNER JOIN @PersonPostingMatchesByEmployer ppme ON ppmti.PersonId = ppme.PersonId AND ppmti.PostingId = ppme.PostingId
WHERE
	ppmti.UserId = @UserId	
	
-- Delete ignored person posting matches
DELETE FROM @PersonPostingMatchesByEmployer
WHERE
	Id IN (SELECT PersonPostingMatchId FROM @UsersPersonPostingMatchesToIgnore)

-- Work out the match counts
INSERT INTO @PersonPostingMatchesGroupedByRecentPlacementMatchId
SELECT
	m.Id,
	CASE  
		WHEN COUNT(ppme.Id) > 0 THEN CAST(1 AS bit)  
		ELSE CAST(0 AS bit)  
	END
FROM 
	@Matches m
	LEFT OUTER JOIN @PersonPostingMatchesByEmployer ppme ON m.MatchedPersonId = ppme.PersonId AND m.EmployerId = ppme.EmployerId
GROUP BY 
	m.Id
	
-- Populate the HasMatchesToOpenJobs flag
UPDATE @Matches
SET
	HasMatchesToOpenPositions = ppmg.HasJobMatches
FROM 
	@Matches m
	INNER JOIN @PersonPostingMatchesGroupedByRecentPlacementMatchId ppmg ON m.Id = ppmg.RecentPlacementMatchId
	
-- Get Matched Candidate Name, Placed Candidate Name, Employer Name and Job Title
UPDATE @Matches
SET
	MatchedCandidateName = mp.FirstName + ' ' + mp.LastName,
	PlacedCandidateName = pp.FirstName + ' ' + pp.LastName,
	EmployerName = e.Name,
	JobTitle = j.JobTitle
FROM 
	@Matches m
	INNER JOIN [Data.Application.Person] mp WITH (NOLOCK) ON mp.Id = m.MatchedPersonId
	INNER JOIN [Data.Application.Person] pp WITH (NOLOCK) ON pp.Id = m.PlacedPersonId 
	INNER JOIN [Data.Application.Job] j WITH (NOLOCK) ON j.Id = m.JobId
	INNER JOIN [Data.Application.Employer] e WITH (NOLOCK) ON e.Id = m.EmployerId

-- Return ordered results
SELECT * FROM @Matches
ORDER BY
	BusinessUnitName, RecentlyPlacedId, Score DESC	
	
END

