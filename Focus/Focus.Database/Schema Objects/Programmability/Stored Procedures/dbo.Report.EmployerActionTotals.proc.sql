﻿

-- =============================================
-- Author:		Tim Case
-- Create date: 27 September 2013
-- Description:	Get report totals for employer actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.EmployerActionTotals]
	@FromDate DATETIME,
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		EmployerId AS Id,
		SUM(JobOrdersEdited) AS JobOrdersEdited,
		SUM(JobSeekersInterviewed) AS JobSeekersInterviewed,
		SUM(JobSeekersHired) AS JobSeekersHired,
		SUM(JobOrdersCreated) AS JobOrdersCreated,
		SUM(JobOrdersPosted) AS JobOrdersPosted,
		SUM(JobOrdersPutOnHold) AS JobOrdersPutOnHold,
		SUM(JobOrdersRefreshed) AS JobOrdersRefreshed,
		SUM(JobOrdersClosed) AS JobOrdersClosed,
		SUM(InvitationsSent) AS InvitationsSent,
		SUM(JobSeekersNotHired) AS JobSeekersNotHired,
		SUM(SelfReferrals) AS SelfReferrals,
		SUM(StaffReferrals) AS StaffReferrals
	FROM 
		[Report.EmployerAction]
	WHERE 
		ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		EmployerId
END
