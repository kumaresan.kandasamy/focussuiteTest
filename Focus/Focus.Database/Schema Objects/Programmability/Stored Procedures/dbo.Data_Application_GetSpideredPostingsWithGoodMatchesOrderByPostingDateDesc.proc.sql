﻿


-- =============================================
-- Author:		Andy Jones
-- Create date: 28 November 2013
-- Description:	Gets spidered postings with good matches ordered by posting date Desc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		PostingDate datetime
	) 
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
		AND p.EmployerName = @EmployerName
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.CreatedOn AS PostingDate
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	ORDER BY
		p.CreatedOn DESC, p.JobTitle
	 		
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Matches not in paged data
	DELETE @Postings
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)
	
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		PostingDate
	FROM
		@Postings
		
END



