﻿
-- =============================================
-- Author:		Tim Case
-- Create date: 21 October 2013
-- Description:	Get report totals for job seeker actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerActivityTotals]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@Category NVARCHAR(500),
	@Activity NVARCHAR(500) = NULL
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		JSA.JobSeekerId AS Id,
		SUM(JSA.AssignmentsMade) AS AssignmentsMade
	FROM 
		[Report.JobSeekerActivityAssignment] JSA
	WHERE 
		JSA.AssignDate BETWEEN @FromDate AND @ToDate
		AND JSA.ActivityCategory = @Category
		AND JSA.Activity = ISNULL(@Activity, JSA.Activity)
	GROUP BY
		JSA.JobSeekerId
END
