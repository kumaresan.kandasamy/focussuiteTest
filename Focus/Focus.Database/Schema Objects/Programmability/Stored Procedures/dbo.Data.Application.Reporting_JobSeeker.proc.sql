﻿


CREATE PROCEDURE [dbo].[Data.Application.Reporting_JobSeeker]
	@EducationLevel	INT,
	@CertAndLicenses NVARCHAR(MAX) = NULL,
	@Skills NVARCHAR(MAX),
	@OccupationGroup NVARCHAR(MAX),
	@DetailedOccupation NVARCHAR(MAX), 
	@IndustryGroup NVARCHAR(MAX), -- To do (Awaiting DB structure)
	@DetailedIndustry NVARCHAR(MAX), -- To do (Awaiting DB structure)
	@SalaryFrequency INT,
	@MinSalary	DECIMAL,
	@MaxSalary	DECIMAL,
	@County	NVARCHAR(MAX),
	@Office NVARCHAR(MAX),
	@WIB NVARCHAR(MAX),
	@Radius INT,
	@ZIP NVARCHAR(20),
	@DateFrom DATETIME,
	@DateTo	DATETIME,
	@VeteranDefinition INT,
	@VeteranType INT,
	@VeteranEmploymentStatus INT,
	@VeteranTransactionType INT,
	@VeteranMilitaryDischarge INT,
	@VeteranBranchOfService INT,
	@ClaimantStatus INT,
	@ClientStatus INT,
	@Services NVARCHAR(MAX),
	@ResultsStatus INT,
	@SearchJobTitle BIT,
	@SearchJobDescription BIT,
	@SearchEmployerName BIT,
	@SearchTerms NVARCHAR(MAX),
	@SearchType INT,
	@PageNumber	INT,
	@PageSize	INT,
	@SortOrder	VARCHAR(20)
AS


BEGIN

DECLARE @CertificationLicenseChosen TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @SkillsChosen TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @ServicesChosen TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @KeywordSearchTerms TABLE
(		
	Id int,
	Value nvarchar(100),
	Processed bit default 0
)
DECLARE @ONetOccupationGroups TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @ONetDetailedOccupations TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @ONetIndustryGroup TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @ONetDetailedIndustry TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @Counties TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @Offices TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @WIBs TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @Zips TABLE
(
	ZipCode varchar(10)
)
CREATE TABLE #KeyWordJobSeekers
(
	Id	bigint
)

DECLARE @KeyWordSearchSql nvarchar(max)
DECLARE @CurrentKeyWord nvarchar(max)
DECLARE @CurrentKeyWordId nvarchar(max)
DECLARE @ProcessKeyWords BIT
DECLARE @SearchOperator nvarchar(4)
set @ProcessKeyWords = 0
set @KeyWordSearchSql= ''

if @SearchType = 1
	set @SearchOperator = 'and'
else if @SearchType = 2
	set @SearchOperator = 'or'	

INSERT INTO @CertificationLicenseChosen
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@CertAndLicenses,''), '|');

INSERT INTO @SkillsChosen
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@Skills,''), '|');

INSERT INTO @ServicesChosen
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@Services,''), '|');

INSERT INTO @KeywordSearchTerms
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@SearchTerms,''), '|');
	
INSERT INTO @ONetDetailedIndustry
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@DetailedIndustry, ''), '|');
	
INSERT INTO @ONetDetailedOccupations
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@DetailedOccupation, ''), '|');
	
INSERT INTO @ONetIndustryGroup
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@IndustryGroup, ''), '|');
	
INSERT INTO @ONetOccupationGroups
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@OccupationGroup, ''), '|');
		
INSERT INTO @Counties
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@County, ''), '|');
	
INSERT INTO @Offices
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@Office, ''), '|');
	
INSERT INTO @WIBs
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@WIB, ''), '|');
	
if (rtrim(ltrim(@ZIP)) <> '')
begin
	insert into @Zips
	select
		z2.ZipCode
	from
		[Data.Report.ZipCodes] z1
	cross join [Data.Report.ZipCodes] z2
	where
		z1.zipcode = rtrim(ltrim(@ZIP))
	and dbo.CalculateDistance(z1.Longitude, z1.Latitude, z2.Longitude, z2.Latitude) < @Radius
end
else
begin
	insert into @Zips
	select null
end
	

if (@SearchEmployerName = 1 or @SearchJobDescription = 1 or @SearchJobTitle = 1) and @SearchType <> 0 and exists (select 1 from @KeywordSearchTerms where Value is not null)
begin
	set @ProcessKeyWords = 1
	while exists (select 1 from @KeywordSearchTerms where Processed = 0)
	begin
		select top 1 @CurrentKeyWordId = Id, @CurrentKeyWord = Value from @KeywordSearchTerms where Processed = 0
		if @SearchEmployerName = 1
		begin
			if LEN(@KeyWordSearchSql) > 0
				set @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '	
		
			set @KeyWordSearchSql = @KeyWordSearchSql + 'candv.EmployerName like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
		end
		
		if @SearchJobTitle = 1
		begin	
			if LEN(@KeyWordSearchSql) > 0
				set @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '
		
			set @KeyWordSearchSql = @KeyWordSearchSql + 'candv.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
		end
		
		if @SearchJobDescription = 1
		begin
			if LEN(@KeyWordSearchSql) > 0
				set @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '
		
			set @KeyWordSearchSql = @KeyWordSearchSql + 'candv.JobDescription like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
		end
					
		update @KeywordSearchTerms set Processed = 1 where @CurrentKeyWordId = Id and @CurrentKeyWord = Value
	end
	set @KeyWordSearchSql = 'insert into #KeyWordJobSeekers (id) select distinct id from [Data.Application.JobSeekerReportView] candv where ' + @KeyWordSearchSql
	print @KeyWordSearchSql
	execute sp_executesql @KeyWordSearchSql;
end
else
begin
	insert into #KeyWordJobSeekers (Id) Values (NULL)
end;

with candidates as(
	select
		CASE lower(@SortOrder)
			when 'name_asc' then ROW_NUMBER()OVER (ORDER BY cand.FirstName ASC, cand.LastName ASC, cand.Id ASC)
			when 'email_asc' then ROW_NUMBER()OVER (ORDER BY cand.EmailAddress ASC, cand.Id ASC)
			when 'clientstatus_asc' then ROW_NUMBER()OVER (ORDER BY cand.ClientStatus ASC, cand.Id ASC)
			when 'city_asc' then ROW_NUMBER()OVER (ORDER BY cand.TownCity ASC, cand.Id ASC)
			when 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY cand.WeeksSinceAccountCreated ASC, cand.Id ASC)
			when 'lastjobtitle_asc' then ROW_NUMBER()OVER (ORDER BY cand.LastJobTitle ASC, cand.Id ASC)
			when 'name_desc' then ROW_NUMBER()OVER (ORDER BY cand.FirstName DESC, cand.LastName DESC, cand.Id ASC)
			when 'email_desc' then ROW_NUMBER()OVER (ORDER BY cand.EmailAddress DESC, cand.Id ASC)
			when 'clientstatus_desc' then ROW_NUMBER()OVER (ORDER BY cand.ClientStatus DESC, cand.Id ASC)
			when 'city_desc' then ROW_NUMBER()OVER (ORDER BY cand.TownCity DESC, cand.Id ASC)
			when 'accountcreation_desc' then ROW_NUMBER()OVER (ORDER BY cand.WeeksSinceAccountCreated DESC, cand.Id ASC)
			when 'lastjobtitle_desc' then ROW_NUMBER()OVER (ORDER BY cand.LastJobTitle DESC, cand.Id ASC)
			else ROW_NUMBER()OVER (ORDER BY cand.FirstName ASC, cand.LastName ASC, cand.Id ASC)
		END as pagingId,
		CASE @SortOrder
			when 'name_asc' then ROW_NUMBER()OVER (ORDER BY cand.FirstName DESC, cand.LastName DESC, cand.Id DESC)
			when 'email_asc' then ROW_NUMBER()OVER (ORDER BY cand.EmailAddress DESC, cand.Id DESC)
			when 'clientstatus_asc' then ROW_NUMBER()OVER (ORDER BY cand.ClientStatus DESC, cand.Id DESC)
			when 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY cand.WeeksSinceAccountCreated DESC, cand.Id DESC)
			when 'city_asc' then ROW_NUMBER()OVER (ORDER BY cand.TownCity DESC, cand.Id DESC)
			when 'lastjobtitle_asc' then ROW_NUMBER()OVER (ORDER BY cand.LastJobTitle DESC, cand.Id DESC)
			when 'name_desc' then ROW_NUMBER()OVER (ORDER BY cand.FirstName ASC, cand.LastName ASC, cand.Id DESC)
			when 'email_desc' then ROW_NUMBER()OVER (ORDER BY cand.EmailAddress ASC, cand.Id DESC)
			when 'clientstatus_desc' then ROW_NUMBER()OVER (ORDER BY cand.ClientStatus ASC, cand.Id DESC)
			when 'city_desc' then ROW_NUMBER()OVER (ORDER BY cand.TownCity ASC, cand.Id DESC)
			when 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY cand.WeeksSinceAccountCreated ASC, cand.Id DESC)
			when 'lastjobtitle_desc' then ROW_NUMBER()OVER (ORDER BY cand.LastJobTitle ASC, cand.Id DESC)
			else ROW_NUMBER()OVER (ORDER BY cand.FirstName DESC, cand.LastName DESC, cand.Id DESC)
		END as pagingRevId, 
		cand.Id,
		cand.FirstName + ' ' + cand.LastName AS FullName,
		cand.FirstName,
		cand.LastName,
		cand.EmailAddress,
		cand.ClientStatus,
		cand.TownCity,
		cand.WeeksSinceAccountCreated,
		cand.LastJobTitle,
		cand.LastEmployer,
		cand.EducationLevel
	from
	(
		SELECT DISTINCT
			p.Id,
			ISNULL(p.FirstName, 'unknown') AS FirstName,
			ISNULL(p.LastName, 'unknown')AS LastName,
			ISNULL(p.EmailAddress, 'unknown') AS EmailAddress,
			'TODO' AS ClientStatus,
			ISNULL (r.TownCity, 'unknown') AS TownCity,
			DATEDIFF (WEEK, u.CreatedOn, GETDATE()) AS WeeksSinceAccountCreated,
			'TODO' AS LastJobTitle,
			'TODO' AS LastEmployer,
			r.HighestEducationLevel AS EducationLevel
			
			FROM [Data.Application.Person] p

		INNER JOIN [Data.Application.Resume] r ON r.PersonId = p.Id AND r.IsDefault = 1
		INNER JOIN [Data.Application.User] u ON u.PersonId = p.Id		
		INNER JOIN #KeyWordJobSeekers kstEmp on p.Id = kstEmp.Id or @ProcessKeyWords = 0
		INNER JOIN  @Zips z	on r.PostCodeZip = z.ZipCode or z.ZipCOde IS NULL
		INNER JOIN @SkillsChosen scands on r.ResumeSkills LIKE '%' + scands.Value + '%' OR scands.Value IS NULL		
	
		--inner join @CertificationLicenseChosen scandcl on r.CertificationLicense LIKE '%' + scandcl.Value + '%' OR scandcl.Value IS NULL
		--inner join @ServicesChosen scandr	on r.ServiceReceived LIKE '%' + scandr.Value + '%' OR scandr.Value IS NULL
		--inner join @Counties c	on r.County LIKE '%' + c.Value + '%' OR c.Value IS NULL
		--inner join @Offices o on r.Office LIKE '%' + o.Value + '%' OR o.Value IS NULL
		--inner join @WIBs w on r.WIBLocation LIKE '%' + w.Value + '%' OR w.Value IS NULL
				
		--left join Onet o on  o.Id = r.PrimaryOnet
		--left join CodeItem jfci	on jfci.[Id] = o.JobFamilyId
		--left join LocalisationItem jfli	on jfli.[Key] = jfci.[key]
		--left join @ONetOccupationGroups oog	on jfli.Value LIKE '%' + oog.Value + '%' OR oog.Value IS NULL
		--left join @ONetDetailedOccupations odo on o.[Key] LIKE '%' + odo.Value + '%' OR odo.Value IS NULL
			
		where
			(u.CreatedOn BETWEEN @DateFrom AND @DateTo)
		and (r.HighestEducationLevel & @EducationLevel <> 0 or @EducationLevel = 0)
		--and (r.VeteranDefinition & @VeteranDefinition <>0 or @VeteranDefinition = 0)
		--and (r.VeteranType & @VeteranType <> 0 or @VeteranType = 0)
		--and (r.VeteranEmploymentStatus & @VeteranEmploymentStatus <> 0 or @VeteranEmploymentStatus = 0)
		--and (r.VeteranTransactionType & @VeteranTransactionType <> 0 or @VeteranTransactionType = 0)
		--and (r.VeteranMilitaryDischarge & @VeteranMilitaryDischarge <> 0 or @VeteranMilitaryDischarge = 0)
		--and (r.VeteranBranchOfService & @VeteranBranchOfService <> 0 or @VeteranBranchOfService = 0)
		--and (r.ApplicationResult & @ResultsStatus <> 0 or @ResultsStatus = 0)
		--and (ISNULL(@SalaryFrequency,0) = 0
		--	OR
		--		(cv.SalaryFrequency = @SalaryFrequency
		--		AND	cv.MinSalary < @MaxSalary
		--		AND cv.MaxSalary > @MinSalary
		--		)
		--	)		
	) cand)

select
	cand.Id,
	cand.pagingId,
	cand.pagingRevId,
	cand.FullName,
	cand.FirstName,
	cand.LastName,
	cand.EmailAddress,
	cand.ClientStatus,
	cand.TownCity,
	cand.WeeksSinceAccountCreated,
	cand.LastJobTitle,
	cand.LastEmployer,
	cand.EducationLevel
from
	candidates cand
where
	pagingId between  ((@PageNumber - 1) * @PageSize) + 1 and (@PageNumber * @PageSize)
order by
	pagingId asc
END




