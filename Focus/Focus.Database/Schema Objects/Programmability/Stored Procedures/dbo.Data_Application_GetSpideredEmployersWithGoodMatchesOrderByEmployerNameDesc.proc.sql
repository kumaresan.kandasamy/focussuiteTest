﻿



-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Employer Name Desc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    CREATE TABLE #PersonPostingMatchToIgnore
    (
		PersonPostingMatchId bigint
	)
      
    CREATE TABLE #PersonPostingMatch
	(
		Id bigint,
        PostingId bigint,
        PersonId bigint
	)
      
    CREATE TABLE #PostingPersonMatchCount
    (
		PostingId bigint,
        MatchCount int
    )
      
    CREATE TABLE #Posting
    (
		Id bigint,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime
    )       
    
    CREATE TABLE #Employer
    (
		EmployerName nvarchar(255),
        PostingCount int
	)
      
    CREATE TABLE #RankedEmployer
    (
		Id int IDENTITY PRIMARY KEY,
        LensPostingId nvarchar(150),
        JobTitle nvarchar(255),
        EmployerName nvarchar(255),
        PostingDate datetime,
        NumberOfPostings int
	)      
    
    DECLARE @FirstRec int
    DECLARE @LastRec int
            
    SET @FirstRec = (@PageNumber - 1) * @PageSize
    SET @LastRec = (@PageNumber * @PageSize + 1)
      
    -- Populate the Postings
    INSERT INTO #Posting
	SELECT 
		p.Id,
        p.LensPostingId,
        p.JobTitle,
        p.EmployerName,
        p.CreatedOn
    FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
	WHERE 
		CreatedOn > @MinimumPostingDate
		AND OriginId = '999'
		AND (EmployerName LIKE '%' + IsNull(@EmployerName, '') + '%')
      
       		
	-- Populate the matches to ignore for this user
    INSERT #PersonPostingMatchToIgnore
    SELECT 
		ppm.Id
    FROM
		[Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK)
        INNER JOIN [Data.Application.PersonPostingMatch] ppm WITH (NOLOCK) ON ppmti.PostingId = ppm.PostingId AND ppmti.PersonId = ppm.PersonId
    WHERE 
        EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = ppmti.PostingId)
        AND ppmti.UserId = @UserID
            
    -- Populate the matches
    INSERT INTO #PersonPostingMatch
    SELECT 
        Id,
        PostingId,
        PersonId
    FROM 
		[Data.Application.PersonPostingMatch] WITH (NOLOCK)
    WHERE
		EXISTS (SELECT Id FROM #Posting WHERE #Posting.Id = PostingId)
            
    -- Remove ignore matches from matches
    DELETE FROM #PersonPostingMatch
    WHERE
		EXISTS (SELECT PersonPostingMatchId FROM #PersonPostingMatchToIgnore WHERE #PersonPostingMatchToIgnore.PersonPostingMatchId = Id)
      
    -- Populate the match counts
    INSERT INTO #PostingPersonMatchCount
    SELECT 
		PostingId, 
        COUNT(Id) 
    FROM 
        #PersonPostingMatch
    GROUP BY
        PostingId
            
    --Populate the employer
    INSERT INTO #Employer
    SELECT 
		p.EmployerName,
        COUNT(Id)
    FROM
		#Posting p
        INNER JOIN #PostingPersonMatchCount ppmc ON p.Id = ppmc.PostingId
	GROUP BY 
		EmployerName
    HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
		
	SELECT @RowCount = @@ROWCOUNT
		
	-- Delete postings with no matches
	DELETE FROM #Posting 
	WHERE 
		NOT EXISTS (SELECT PostingId FROM #PostingPersonMatchCount WHERE PostingId = Id);
	
	-- Remove non-top postings based on order
	WITH OrderedPostings AS
	(
		SELECT  
			Id,
			RANK() OVER (PARTITION BY EmployerName ORDER BY PostingDate DESC, JobTitle, Id) AS [Rank]
		FROM 
			#Posting 
 	) 
	DELETE FROM #Posting
	WHERE
		NOT EXISTS (SELECT Id FROM OrderedPostings op WHERE op.[Rank] = 1 AND op.Id = #Posting.Id)
		
	-- Rank the employer based on the sort order
    INSERT INTO #RankedEmployer
    SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		#Employer e
	INNER JOIN
		#Posting p ON e.EmployerName = p.EmployerName
	ORDER BY
		e.EmployerName DESC, e.PostingCount DESC
            
    SELECT 
		*
    FROM 
		#RankedEmployer
    WHERE
		Id > @FirstRec AND Id < @LastRec
		
END




