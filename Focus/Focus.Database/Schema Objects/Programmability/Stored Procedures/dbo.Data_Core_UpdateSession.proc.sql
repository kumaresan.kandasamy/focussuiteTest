﻿


CREATE PROCEDURE [dbo].[Data_Core_UpdateSession] 
	@SessionId	uniqueidentifier,
	@UserId		bigint,
	@LastRequestOn datetime,
	@CheckUser bit
AS
IF(@CheckUser = 0)
BEGIN
	UPDATE [Data.Core.Session]
	SET UserId = @UserId, LastRequestOn = @LastRequestOn
	WHERE Id = @SessionId 
END ELSE BEGIN
	UPDATE [Data.Core.Session]
	SET LastRequestOn = @LastRequestOn
	WHERE Id = @SessionId AND UserId = @UserId
END




