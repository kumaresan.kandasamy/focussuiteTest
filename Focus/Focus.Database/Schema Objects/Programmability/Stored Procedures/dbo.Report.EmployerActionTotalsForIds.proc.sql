﻿

-- =============================================
-- Author:		Tim Case
-- Create date: 04 October 2013
-- Description:	Get report totals for employer actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.EmployerActionTotalsForIds]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@EmployerIds XML
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #EmployerIds
	(
		Id BIGINT
	)
	
	INSERT INTO #EmployerIds ( Id )
    SELECT t.value('.', 'bigint')
    FROM @EmployerIds.nodes('//id') AS x(t) 

	SELECT
		EA.EmployerId AS Id,
		SUM(EA.JobOrdersEdited) AS JobOrdersEdited,
		SUM(EA.JobSeekersInterviewed) AS JobSeekersInterviewed,
		SUM(EA.JobSeekersHired) AS JobSeekersHired,
		SUM(EA.JobOrdersCreated) AS JobOrdersCreated, 
		SUM(EA.JobOrdersPosted) AS JobOrdersPosted, 
		SUM(EA.JobOrdersPutOnHold) AS JobOrdersPutOnHold, 
		SUM(EA.JobOrdersRefreshed) AS JobOrdersRefreshed, 
		SUM(EA.JobOrdersClosed) AS JobOrdersClosed, 
		SUM(EA.InvitationsSent) AS InvitationsSent, 
		SUM(EA.JobSeekersNotHired) AS JobSeekersNotHired, 
		SUM(EA.SelfReferrals) AS SelfReferrals, 
		SUM(EA.StaffReferrals) AS StaffReferrals
	FROM 
		[Report.EmployerAction] EA
	INNER JOIN #EmployerIds EI
		ON EA.EmployerId = EI.Id
	WHERE 
		EA.ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		EA.EmployerId
END
