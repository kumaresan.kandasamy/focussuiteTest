﻿CREATE PROCEDURE [dbo].[Report.JobSeekerOfficeNameUpdate] 
	@OfficeId bigint
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
	 RJS
	SET 
	 Office = STUFF(
	  (SELECT 
		'||' + JSO.OfficeName
	   FROM 
		[Report.JobSeeker] RJS2
	   INNER JOIN [Report.JobSeekerOffice] JSO
		ON JSO.JobSeekerId = RJS2.Id
	   WHERE
		RJS2.Id = RJS.Id
	   ORDER BY
		JSO.OfficeName
	   FOR XML PATH('')
	  ), 1, 2, '')
	FROM
	 [Report.JobSeeker] RJS
	WHERE
	 EXISTS (SELECT 1 FROM [Report.JobSeekerOffice] JSO2 WHERE JSO2.JobSeekerId = RJS.Id AND JSO2.OfficeId = @OfficeId)
	 
END
