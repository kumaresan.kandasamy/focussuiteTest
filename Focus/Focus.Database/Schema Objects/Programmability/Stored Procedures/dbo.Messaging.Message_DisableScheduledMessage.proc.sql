﻿CREATE PROCEDURE [dbo].[Messaging.Message_DisableScheduledMessage] 
	@MessageTypeName NVARCHAR(100)
AS
BEGIN
	DECLARE @MessageTypeId UNIQUEIDENTIFIER

	SELECT @MessageTypeId = Id 
	FROM [Messaging.MessageType] 
	WHERE TypeName LIKE 'Focus.Services.Messages.' + @MessageTypeName + '%'

	IF @MessageTypeId IS NULL
	BEGIN
		RAISERROR('MessageType not found', 16, 1)
	END

	UPDATE 
		[Messaging.MessageType]
	SET
		SchedulePlan = NULL
	WHERE
		Id = @MessageTypeId

	UPDATE
		[Messaging.Message]
	SET
		[Status] = 4	
	WHERE
		[Status] = 1
		AND MessageTypeId = @MessageTypeId
END