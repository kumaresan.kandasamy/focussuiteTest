﻿CREATE PROCEDURE [dbo].[Report.JobOrderOfficeNameUpdate] 
	@OfficeId bigint
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
	 RJO
	SET 
	 Office = STUFF(
	  (SELECT 
		'||' + JOO.OfficeName
	   FROM 
		[Report.JobOrder] RJO2
	   INNER JOIN [Report.JobOrderOffice] JOO
		ON JOO.JobOrderId = RJO2.Id
	   WHERE
		RJO2.Id = RJO.Id
	   ORDER BY
		JOO.OfficeName
	   FOR XML PATH('')
	  ), 1, 2, '')
	FROM
	 [Report.JobOrder] RJO
	WHERE
	 EXISTS (SELECT 1 FROM [Report.JobOrderOffice] JOO2 WHERE JOO2.JobOrderId = RJO.Id AND JOO2.OfficeId = @OfficeId)
	 
END
