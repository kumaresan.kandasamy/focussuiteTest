﻿


CREATE PROCEDURE [dbo].[PageState_LoadPageState] (@id UNIQUEIDENTIFIER) AS
	SET NOCOUNT ON

	DECLARE @textPtr VARBINARY(16)
  DECLARE @length INT

	UPDATE dbo.[Data.Core.PageState]
		SET LastAccessed = GETUTCDATE(),
				@textPtr = TEXTPTR(Value),
				@length = DATALENGTH(Value)
	WHERE 
		Id = @id

	IF @length IS NOT NULL BEGIN
		SELECT @length AS Length
			READTEXT dbo.[Data.Core.PageState].Value @textPtr 0 @length
	END

	RETURN 0



