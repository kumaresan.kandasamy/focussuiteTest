﻿CREATE TYPE [dbo].[GuidTableType] AS TABLE
(
	[Guid] uniqueidentifier NOT NULL,
	PRIMARY KEY CLUSTERED ([Guid] ASC) WITH (IGNORE_DUP_KEY = OFF)
)