﻿CREATE TYPE [dbo].[ConfigTableType] AS  TABLE
(
    [Key]          NVARCHAR (200)  NOT NULL,
    [Value]        NVARCHAR (2000) NULL,
    [InternalOnly] BIT             NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC)
);

