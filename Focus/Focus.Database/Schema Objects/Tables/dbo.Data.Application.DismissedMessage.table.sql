﻿CREATE TABLE [dbo].[Data.Application.DismissedMessage] (
    [Id]        BIGINT NOT NULL,
    [UserId]    BIGINT NOT NULL,
    [MessageId] BIGINT NOT NULL
);

