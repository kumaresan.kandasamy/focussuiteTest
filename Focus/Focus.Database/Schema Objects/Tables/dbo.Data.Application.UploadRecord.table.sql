﻿CREATE TABLE [dbo].[Data.Application.UploadRecord] (
    [Id]                BIGINT         NOT NULL,
    [UploadFileId]      BIGINT         NOT NULL,
    [RecordNumber]      INT            NOT NULL,
    [Status]            INT            NOT NULL,
    [ValidationMessage] NVARCHAR (250) NOT NULL,
    [FieldValidation]   NVARCHAR (MAX) NOT NULL,
    [FieldData]         NVARCHAR (MAX) NOT NULL
);

