﻿CREATE TABLE [dbo].[Messaging.MessageLog] (
    [Id]        UNIQUEIDENTIFIER NOT NULL,
    [Timestamp] DATETIME         NOT NULL,
    [Details]   NVARCHAR (MAX)   NOT NULL,
    [MessageId] UNIQUEIDENTIFIER NOT NULL
);

