﻿CREATE TABLE [dbo].[Report.Statistic] (
    [Id]            INT            NOT NULL,
    [StatisticType] INT            NOT NULL,
    [Value]         NVARCHAR (MAX) NOT NULL,
    [CreatedOn]     DATETIME       NOT NULL
);

