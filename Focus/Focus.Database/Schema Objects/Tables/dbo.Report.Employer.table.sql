﻿CREATE TABLE [dbo].[Report.Employer] (
    [Id]                                  BIGINT         NOT NULL,
    [Name]                                NVARCHAR (400) NOT NULL,
    [FederalEmployerIdentificationNumber] NVARCHAR (72)  NOT NULL,
    [StateId]                             BIGINT         NULL,
    [PostalCode]                          NVARCHAR (40)  NULL,
    [Office]                              NVARCHAR (400) NULL,
    [CreationDateTime]                    DATETIME       NULL,
    [CountyId]                            BIGINT         NULL,
    [FocusBusinessUnitId]                 BIGINT         NOT NULL,
    [State]                               NVARCHAR (400) NULL,
    [County]                              NVARCHAR (400) NULL,
    [LatitudeRadians]                     FLOAT          NOT NULL,
    [LongitudeRadians]                    FLOAT          NOT NULL,
    [CreatedOn]                           DATETIME       NOT NULL,
    [UpdatedOn]                           DATETIME       NOT NULL,
    [FocusEmployerId]                     BIGINT         NULL,
    [NoOfEmployees]                       BIGINT         NULL,
    [AccountTypeId]                       BIGINT         NULL
);

