﻿CREATE TABLE [dbo].[Data.Application.JobLicence] (
    [Id]      BIGINT         NOT NULL,
    [Licence] NVARCHAR (MAX) NOT NULL,
    [JobId]   BIGINT         NOT NULL
);

