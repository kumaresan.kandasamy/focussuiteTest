﻿CREATE TABLE [dbo].[Data.Application.Application]
(
	[Id]                     BIGINT         NOT NULL,
	[ApprovalStatus]         INT            NOT NULL,
	[ApprovalRequiredReason] NVARCHAR (255) NULL,
	[ApplicationStatus]      INT            NOT NULL,
	[ApplicationScore]       INT            NOT NULL,
	[StatusLastChangedOn]    DATETIME       NULL,
	[CreatedOn]              DATETIME       NOT NULL,
	[UpdatedOn]              DATETIME       NOT NULL,
	[ResumeId]               BIGINT         NOT NULL,
	[PostingId]              BIGINT         NOT NULL,
	[Viewed]                 BIT            NOT NULL,
	[PostHireFollowUpStatus] INT            NULL,
	[AutomaticallyApproved]  BIT            NULL,
	[StatusLastChangedBy]    BIGINT         NULL,
	[AutomaticallyDenied]    BIT            NOT NULL,
	[PreviousApprovalStatus] INT            NULL,
	[LockVersion]            INT            NOT NULL,
	[AutomaticallyOnHold]		 BIT						NULL
);

