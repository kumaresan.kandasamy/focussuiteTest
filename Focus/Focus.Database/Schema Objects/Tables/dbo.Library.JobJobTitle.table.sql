﻿CREATE TABLE [dbo].[Library.JobJobTitle] (
    [Id]               BIGINT           IDENTITY (1, 1) NOT NULL,
    [DemandPercentile] DECIMAL (24, 18) NOT NULL,
    [JobId]            BIGINT           NOT NULL,
    [JobTitleId]       BIGINT           NOT NULL
);

