﻿CREATE TABLE [dbo].[Library.ProgramAreaDegree] (
    [Id]            BIGINT IDENTITY (1, 1) NOT NULL,
    [DegreeId]      BIGINT NULL,
    [ProgramAreaId] BIGINT NULL
);

