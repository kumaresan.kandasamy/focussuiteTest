﻿CREATE TABLE [dbo].[Library.JobCareerArea] (
    [Id]           BIGINT IDENTITY (1, 1) NOT NULL,
    [JobId]        BIGINT NOT NULL,
    [CareerAreaId] BIGINT NOT NULL
);

