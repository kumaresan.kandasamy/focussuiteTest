﻿CREATE TABLE [dbo].[Data.Application.Employee]
(
	[Id]                      BIGINT         NOT NULL,
	[ApprovalStatus]          INT            NOT NULL,
	[StaffUserId]             BIGINT         NULL,
	[EmployerId]              BIGINT         NOT NULL,
	[PersonId]                BIGINT         NOT NULL,
	[RedProfanityWords]       NVARCHAR (MAX) NULL,
	[YellowProfanityWords]    NVARCHAR (MAX) NULL,
	[ApprovalStatusChangedBy] BIGINT         NULL,
	[ApprovalStatusChangedOn] DATETIME       NULL,
	[ApprovalStatusReason]    INT            NOT NULL,
	[AwaitingApprovalDate]    DATETIME       NULL,
	[LockVersion]             INT            NOT NULL
);

