﻿CREATE TABLE [dbo].[Library.DegreeAlias] (
    [Id]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [AliasExtended]          NVARCHAR (400) NOT NULL,
    [EducationLevel]         INT            NOT NULL,
    [Alias]                  NVARCHAR (MAX) NOT NULL,
    [DegreeId]               BIGINT         NOT NULL,
    [DegreeEducationLevelId] BIGINT         NOT NULL
);

