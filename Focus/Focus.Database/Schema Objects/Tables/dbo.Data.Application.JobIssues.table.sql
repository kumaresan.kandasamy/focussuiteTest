﻿CREATE TABLE [dbo].[Data.Application.JobIssues] (
    [Id]                                  BIGINT   NOT NULL,
    [JobId]                               BIGINT   NOT NULL,
    [LowQualityMatches]                   BIT      NOT NULL,
    [LowQualityMatchesResolvedDate]       DATETIME NULL,
    [LowReferralActivity]                 BIT      NOT NULL,
    [LowReferralActivityResolvedDate]     DATETIME NULL,
    [NotViewingReferrals]                 BIT      NOT NULL,
    [NotViewingReferralsResolvedDate]     DATETIME NULL,
    [SearchingButNotInviting]             BIT      NOT NULL,
    [SearchingButNotInvitingResolvedDate] DATETIME NULL,
    [ClosingDateRefreshed]                BIT      NOT NULL,
    [EarlyJobClosing]                     BIT      NOT NULL,
    [NegativeSurveyResponse]              BIT      NOT NULL,
    [PositiveSurveyResponse]              BIT      NOT NULL,
    [FollowUpRequested]                   BIT      NOT NULL
);

