﻿CREATE TABLE [dbo].[Data.Core.ActionEvent] (
    [Id]                   BIGINT           NOT NULL,
    [SessionId]            UNIQUEIDENTIFIER NOT NULL,
    [RequestId]            UNIQUEIDENTIFIER NOT NULL,
    [UserId]               BIGINT           NOT NULL,
    [ActionedOn]           DATETIME         NOT NULL,
    [EntityId]             BIGINT           NULL,
    [EntityIdAdditional01] BIGINT           NULL,
    [EntityIdAdditional02] BIGINT           NULL,
    [AdditionalDetails]    NVARCHAR (MAX)   NULL,
    [ActionTypeId]         BIGINT           NOT NULL,
    [EntityTypeId]         BIGINT           NULL,
    [CreatedOn]            DATETIME         NOT NULL
);

