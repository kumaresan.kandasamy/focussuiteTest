﻿CREATE TABLE [dbo].[Library.EducationInternshipSkill] (
    [Id]                            BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]                          NVARCHAR (MAX) NOT NULL,
    [LocalisationId]                BIGINT         NOT NULL,
    [EducationInternshipCategoryId] BIGINT         NOT NULL
);

