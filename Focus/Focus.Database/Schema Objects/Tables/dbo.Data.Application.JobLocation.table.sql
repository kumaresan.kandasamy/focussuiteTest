﻿CREATE TABLE [dbo].[Data.Application.JobLocation] (
    [Id]                        BIGINT         NOT NULL,
    [Location]                  NVARCHAR (MAX) NOT NULL,
    [IsPublicTransitAccessible] BIT            NOT NULL,
    [JobId]                     BIGINT         NOT NULL,
    [AddressLine1]              NVARCHAR (127) NULL,
    [City]                      NVARCHAR (127) NULL,
    [State]                     NVARCHAR (127) NULL,
    [Zip]                       NVARCHAR (10)  NULL,
    [County]                    NVARCHAR (127) NULL,
		CreatedOn										DATETIME			 NOT NULL CONSTRAINT [DF_Data.Application.JobLocation_CreatedOn] DEFAULT('01 January 1900'),
		NewLocation									BIT
);

