﻿CREATE TABLE [dbo].[Data.Application.EmployerAddress]
(
	[Id]                      bigint         NOT NULL,
	[Line1]                   nvarchar (200) NULL,
	[Line2]                   nvarchar (200) NULL,
	[Line3]                   nvarchar (200) NULL,
	[TownCity]                nvarchar (100) NOT NULL,
	[CountyId]                bigint         NULL,
	[PostcodeZip]             nvarchar (20)  NOT NULL,
	[StateId]                 bigint         NOT NULL,
	[CountryId]               bigint         NOT NULL,
	[IsPrimary]               bit            NOT NULL,
	[PublicTransitAccessible] bit            NOT NULL,
	[EmployerId]              bigint         NOT NULL
);

