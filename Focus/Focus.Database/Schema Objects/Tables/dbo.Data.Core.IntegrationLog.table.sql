﻿CREATE TABLE [dbo].[Data.Core.IntegrationLog] (
    [Id]               BIGINT           IDENTITY (1, 1) NOT NULL,
    [IntegrationPoint] INT              NOT NULL,
    [RequestededBy]    BIGINT           NOT NULL,
    [RequestedOn]      DATETIME         NOT NULL,
    [RequestStart]     DATETIME         NOT NULL,
    [RequestEnd]       DATETIME         NOT NULL,
    [Outcome]          INT              NOT NULL,
    [CreatedOn]        DATETIME         NOT NULL,
    [UpdatedOn]        DATETIME         NOT NULL,
    [RequestId]        UNIQUEIDENTIFIER NULL
);

