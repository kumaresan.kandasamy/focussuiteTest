﻿CREATE TABLE [dbo].[Data.Migration.JobOrderRequest] (
    [Id]               BIGINT         NOT NULL,
    [SourceId]         BIGINT         NOT NULL,
    [FocusId]          BIGINT         NOT NULL,
    [Request]          NVARCHAR (MAX) NOT NULL,
    [Status]           INT            NOT NULL,
    [ExternalId]       NVARCHAR (200) NOT NULL,
    [RecordType]       INT            NOT NULL,
    [Message]          NVARCHAR (MAX) NOT NULL,
    [ProcessedBy]      DATETIME       NULL,
    [ParentExternalId] NVARCHAR (200) NULL,
    [BatchNumber]      INT            NULL,
    [IsUpdate]         BIT            NOT NULL,
    [EmailRequired]    BIT            NOT NULL
);

