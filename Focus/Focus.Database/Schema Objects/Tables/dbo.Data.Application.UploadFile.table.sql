﻿CREATE TABLE [dbo].[Data.Application.UploadFile] (
    [Id]                BIGINT          NOT NULL,
    [UserId]            BIGINT          NOT NULL,
    [SchemaType]        INT             NOT NULL,
    [Status]            INT             NOT NULL,
    [RecordTypeName]    NVARCHAR (900)  NULL,
    [CreatedOn]         DATETIME        NOT NULL,
    [UpdatedOn]         DATETIME        NULL,
    [ProcessingState]   INT             NOT NULL,
    [FileName]          NVARCHAR (250)  NOT NULL,
    [FileType]          INT             NOT NULL,
    [CustomInformation] NVARCHAR (1000) NULL,
    [FileData]          VARBINARY (MAX) NULL
);

