﻿CREATE TABLE [dbo].[Library.DegreeEducationLevelMapping] (
    [Id]                     BIGINT IDENTITY (1, 1) NOT NULL,
    [DegreeEducationLevelId] BIGINT NOT NULL,
    [LensId]                 INT    NOT NULL
);

