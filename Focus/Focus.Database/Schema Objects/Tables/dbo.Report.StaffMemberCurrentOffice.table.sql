﻿CREATE TABLE [dbo].[Report.StaffMemberCurrentOffice] (
    [Id]            BIGINT         NOT NULL,
    [OfficeName]    NVARCHAR (100) NOT NULL,
    [OfficeId]      BIGINT         NULL,
    [StartDate]     DATETIME       NULL,
    [StaffMemberId] BIGINT         NOT NULL
);

