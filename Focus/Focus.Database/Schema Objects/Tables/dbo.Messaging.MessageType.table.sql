﻿CREATE TABLE [dbo].[Messaging.MessageType] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [TypeName]     NVARCHAR (450)   NOT NULL,
    [SchedulePlan] NVARCHAR (500)   NULL
);

