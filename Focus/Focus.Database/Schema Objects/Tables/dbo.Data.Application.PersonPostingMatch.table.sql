﻿CREATE TABLE [dbo].[Data.Application.PersonPostingMatch] (
    [Id]        BIGINT   NOT NULL,
    [Score]     INT      NOT NULL,
    [CreatedOn] DATETIME NOT NULL,
    [PersonId]  BIGINT   NOT NULL,
    [PostingId] BIGINT   NOT NULL
);

