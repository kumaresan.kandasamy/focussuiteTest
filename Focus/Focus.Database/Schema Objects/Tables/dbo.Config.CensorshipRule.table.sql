﻿CREATE TABLE [dbo].[Config.CensorshipRule] (
    [Id]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Type]      INT            NOT NULL,
    [Level]     INT            NOT NULL,
    [Phrase]    NVARCHAR (200) NOT NULL,
    [WhiteList] NVARCHAR (MAX) NULL
);

