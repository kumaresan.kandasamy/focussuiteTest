﻿CREATE TABLE [dbo].[Library.Onet] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [OnetCode]    NVARCHAR (10)  NOT NULL,
    [Key]         NVARCHAR (200) NOT NULL,
    [JobFamilyId] BIGINT         NOT NULL,
    [JobZone]     INT            NOT NULL
);

