﻿CREATE TABLE [dbo].[Library.MilitaryOccupation] (
    [Id]                        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Code]                      NVARCHAR (10)  NOT NULL,
    [Key]                       NVARCHAR (200) NOT NULL,
    [BranchOfServiceId]         BIGINT         NOT NULL,
    [IsCommissioned]            BIT            NOT NULL,
    [MilitaryOccupationGroupId] BIGINT         NOT NULL
);

