﻿CREATE TABLE [dbo].[Data.Application.Encryption] (
    [Id]           BIGINT         NOT NULL,
    [EntityTypeId] INT            NOT NULL,
    [EntityId]     BIGINT         NOT NULL,
    [Vector]       NVARCHAR (MAX) NOT NULL,
    [TargetTypeId] INT            NOT NULL
);

