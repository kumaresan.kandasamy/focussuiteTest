﻿CREATE TABLE [dbo].[Data.Core.IntegrationImportRecord] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [RecordType]      INT              NOT NULL,
    [ExternalId]      NVARCHAR (255)   NOT NULL,
    [Status]          INT              NOT NULL,
    [ImportStartTime] DATETIME         NULL,
    [ImportEndTime]   DATETIME         NULL,
    [Details]         NVARCHAR (2000)  NULL,
    [CreatedOn]       DATETIME         NOT NULL,
    [RequestId]       UNIQUEIDENTIFIER NULL,
    [FocusId]         BIGINT           NULL
);

