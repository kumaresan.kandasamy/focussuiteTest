﻿CREATE TABLE [dbo].[Library.JobReport] (
    [Id]                         BIGINT           IDENTITY (1, 1) NOT NULL,
    [HiringDemand]               INT              NOT NULL,
    [SalaryTrend]                NVARCHAR (10)    NOT NULL,
    [SalaryTrendPercentile]      DECIMAL (24, 18) NOT NULL,
    [SalaryTrendAverage]         DECIMAL (24, 18) NOT NULL,
    [LocalisationId]             BIGINT           NOT NULL,
    [GrowthTrend]                NVARCHAR (50)    NOT NULL,
    [GrowthPercentile]           DECIMAL (24, 18) NOT NULL,
    [HiringTrend]                NVARCHAR (50)    NOT NULL,
    [HiringTrendPercentile]      DECIMAL (24, 18) NOT NULL,
    [SalaryTrendMin]             INT              NOT NULL,
    [SalaryTrendMax]             INT              NOT NULL,
    [SalaryTrendRealtime]        NVARCHAR (10)    NULL,
    [SalaryTrendRealtimeAverage] INT              NULL,
    [SalaryTrendRealtimeMin]     INT              NULL,
    [SalaryTrendRealtimeMax]     INT              NULL,
    [JobId]                      BIGINT           NOT NULL,
    [StateAreaId]                BIGINT           NOT NULL,
    [HiringTrendSubLevel]        NVARCHAR (4)     NOT NULL
);

