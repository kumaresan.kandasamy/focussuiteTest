﻿CREATE TABLE [dbo].[Library.PostalCode] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Code]           NVARCHAR (8)   NOT NULL,
    [Longitude]      FLOAT          NOT NULL,
    [Latitude]       FLOAT          NOT NULL,
    [CountryKey]     NVARCHAR (200) NOT NULL,
    [StateKey]       NVARCHAR (200) NOT NULL,
    [CountyKey]      NVARCHAR (200) NULL,
    [CityName]       NVARCHAR (50)  NULL,
    [OfficeKey]      NVARCHAR (200) NOT NULL,
    [WibLocationKey] NVARCHAR (200) NOT NULL,
    [MSAId]          NVARCHAR (36)  NULL
);

