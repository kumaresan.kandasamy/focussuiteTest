﻿CREATE TABLE [dbo].[Report.JobSeekerJobHistory] (
    [Id]                  BIGINT         NOT NULL,
    [EmployerName]        NVARCHAR (MAX) NOT NULL,
    [EmploymentStartDate] DATETIME       NULL,
    [EmploymentEndDate]   DATETIME       NULL,
    [JobTitle]            NVARCHAR (MAX) NOT NULL,
    [JobSeekerId]         BIGINT         NOT NULL,
    [EmployerCity]        NVARCHAR (MAX) NULL,
    [EmployerState]       NVARCHAR (MAX) NULL
);

