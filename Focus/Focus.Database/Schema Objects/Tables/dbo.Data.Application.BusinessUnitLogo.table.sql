﻿CREATE TABLE [dbo].[Data.Application.BusinessUnitLogo] (
    [Id]             BIGINT          NOT NULL,
    [Name]           NVARCHAR (200)  NOT NULL,
    [Logo]           VARBINARY (MAX) NOT NULL,
    [BusinessUnitId] BIGINT          NULL
);

