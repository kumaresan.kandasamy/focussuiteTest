﻿CREATE TABLE [dbo].[Data.Application.SavedMessage] (
    [Id]       BIGINT         NOT NULL,
    [Audience] BIGINT         NOT NULL,
    [Name]     NVARCHAR (100) NOT NULL,
    [UserId]   BIGINT         NOT NULL
);

