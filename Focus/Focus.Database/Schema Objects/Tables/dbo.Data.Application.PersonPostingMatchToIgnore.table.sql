﻿CREATE TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore] (
    [Id]        BIGINT   NOT NULL,
    [IgnoredOn] DATETIME NOT NULL,
    [PostingId] BIGINT   NOT NULL,
    [PersonId]  BIGINT   NOT NULL,
    [UserId]    BIGINT   NOT NULL
);

