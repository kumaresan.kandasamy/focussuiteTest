﻿CREATE TABLE [dbo].[Config.ActivityCategory] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [LocalisationKey] NVARCHAR (MAX) NOT NULL,
    [Name]            NVARCHAR (MAX) NOT NULL,
    [Visible]         BIT            NOT NULL,
    [Administrable]   BIT            NOT NULL,
    [ActivityType]    INT            NOT NULL,
    [Editable]        BIT            NOT NULL
);

