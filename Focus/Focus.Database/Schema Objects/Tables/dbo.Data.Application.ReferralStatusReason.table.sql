﻿CREATE TABLE [dbo].[Data.Application.ReferralStatusReason]
(
		[Id]                            bigint          NOT NULL,
		[ReasonId]                      bigint          NOT NULL,
		[OtherReasonText]               nvarchar (250)  NULL,
		[EmployerId]                    bigint          NULL,
		[BusinessUnitId]                bigint          NULL,
    [ApplicationId]                 bigint          NULL,
		[JobId]							            bigint			    NULL,
    [ApprovalStatus]                int             NOT NULL,
		[EntityType]                    int             NOT NULL,
    [EmployeeId]                    bigint          NULL
);