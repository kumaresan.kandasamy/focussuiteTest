﻿CREATE TABLE [dbo].[Report.SavedReport] (
    [Id]                 BIGINT         NOT NULL,
    [ReportType]         INT            NOT NULL,
    [Criteria]           NVARCHAR (MAX) NULL,
    [UserId]             BIGINT         NULL,
    [Name]               NVARCHAR (200) NULL,
    [ReportDate]         DATETIME       NOT NULL,
    [DisplayOnDashboard] BIT            NOT NULL,
    [IsSessionReport]    BIT            NOT NULL,
    [ReportDisplayType]  INT            NULL
);

