﻿CREATE TABLE [dbo].[Library.JobTaskLocalisationItem] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]            NVARCHAR (200) NOT NULL,
    [PrimaryValue]   NVARCHAR (MAX) NOT NULL,
    [SecondaryValue] NVARCHAR (MAX) NULL,
    [LocalisationId] BIGINT         NOT NULL
);

