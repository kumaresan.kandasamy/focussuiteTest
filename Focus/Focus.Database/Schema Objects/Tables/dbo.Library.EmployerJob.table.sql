﻿CREATE TABLE [dbo].[Library.EmployerJob] (
    [Id]          BIGINT IDENTITY (1, 1) NOT NULL,
    [Positions]   INT    NOT NULL,
    [JobId]       BIGINT NOT NULL,
    [EmployerId]  BIGINT NOT NULL,
    [StateAreaId] BIGINT NOT NULL
);

