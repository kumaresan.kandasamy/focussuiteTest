﻿CREATE TABLE [dbo].[Data.Application.PersonPostcodeMapper] (
    [Id]          BIGINT        NOT NULL,
    [PostcodeZip] NVARCHAR (20) NOT NULL,
    [OwnedById]   BIGINT        NULL,
    [PersonId]    BIGINT        NOT NULL
);

