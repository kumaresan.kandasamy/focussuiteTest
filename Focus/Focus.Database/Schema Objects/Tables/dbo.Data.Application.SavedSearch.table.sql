﻿CREATE TABLE [dbo].[Data.Application.SavedSearch] (
    [Id]                    BIGINT         NOT NULL,
    [Name]                  NVARCHAR (200) NOT NULL,
    [SearchCriteria]        NVARCHAR (MAX) NOT NULL,
    [AlertEmailRequired]    BIT            NOT NULL,
    [AlertEmailFrequency]   INT            NOT NULL,
    [AlertEmailFormat]      INT            NOT NULL,
    [AlertEmailAddress]     NVARCHAR (200) NOT NULL,
    [AlertEmailScheduledOn] DATETIME       NULL,
    [Type]                  INT            NULL,
    [CreatedOn]             DATETIME       NOT NULL,
    [UpdatedOn]             DATETIME       NOT NULL
);

