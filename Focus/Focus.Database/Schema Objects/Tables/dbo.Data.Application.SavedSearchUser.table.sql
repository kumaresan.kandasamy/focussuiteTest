﻿CREATE TABLE [dbo].[Data.Application.SavedSearchUser] (
    [Id]            BIGINT NOT NULL,
    [UserId]        BIGINT NOT NULL,
    [SavedSearchId] BIGINT NOT NULL
);

