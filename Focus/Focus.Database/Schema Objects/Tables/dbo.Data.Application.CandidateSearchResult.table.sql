﻿CREATE TABLE [dbo].[Data.Application.CandidateSearchResult] (
    [Id]                       BIGINT NOT NULL,
    [CandidateSearchHistoryId] BIGINT NOT NULL,
    [PersonId]                 BIGINT NOT NULL,
    [Score]                    INT    NOT NULL
);

