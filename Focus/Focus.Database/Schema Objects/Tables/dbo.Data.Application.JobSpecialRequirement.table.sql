﻿CREATE TABLE [dbo].[Data.Application.JobSpecialRequirement] (
    [Id]          BIGINT         NOT NULL,
    [Requirement] NVARCHAR (400) NOT NULL,
    [JobId]       BIGINT         NOT NULL,
    [IsNew]       BIT            NOT NULL
);

