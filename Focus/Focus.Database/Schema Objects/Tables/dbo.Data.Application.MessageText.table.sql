﻿CREATE TABLE [dbo].[Data.Application.MessageText] (
    [Id]             BIGINT         NOT NULL,
    [Text]           NVARCHAR (MAX) NOT NULL,
    [LocalisationId] BIGINT         NOT NULL,
    [MessageId]      BIGINT         NOT NULL
);

