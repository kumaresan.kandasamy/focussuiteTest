﻿CREATE TABLE [dbo].[Library.OnetSkills] (
    [Id]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Skill]     NVARCHAR (MAX) NOT NULL,
    [SkillType] NVARCHAR (MAX) NOT NULL,
    [OnetId]    BIGINT         NOT NULL
);

