﻿CREATE TABLE [dbo].[Report.EmployerAction] (
    [Id]                    BIGINT   NOT NULL,
    [ActionDate]            DATETIME NOT NULL,
    [EmployerId]            BIGINT   NOT NULL,
    [JobOrdersEdited]       INT      NOT NULL,
    [JobSeekersInterviewed] INT      NOT NULL,
    [JobSeekersHired]       INT      NOT NULL,
    [JobOrdersCreated]      INT      NOT NULL,
    [JobOrdersPosted]       INT      NOT NULL,
    [JobOrdersPutOnHold]    INT      NOT NULL,
    [JobOrdersRefreshed]    INT      NOT NULL,
    [JobOrdersClosed]       INT      NOT NULL,
    [InvitationsSent]       INT      NOT NULL,
    [JobSeekersNotHired]    INT      NOT NULL,
    [SelfReferrals]         INT      NOT NULL,
    [StaffReferrals]        INT      NOT NULL
);

