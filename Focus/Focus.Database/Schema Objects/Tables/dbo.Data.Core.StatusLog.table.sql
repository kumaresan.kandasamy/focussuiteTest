﻿CREATE TABLE [dbo].[Data.Core.StatusLog] (
    [Id]             BIGINT   NOT NULL,
    [EntityId]       BIGINT   NOT NULL,
    [OriginalStatus] BIGINT   NULL,
    [NewStatus]      BIGINT   NOT NULL,
    [UserId]         BIGINT   NOT NULL,
    [ActionedOn]     DATETIME NOT NULL,
    [EntityTypeId]   BIGINT   NOT NULL
);

