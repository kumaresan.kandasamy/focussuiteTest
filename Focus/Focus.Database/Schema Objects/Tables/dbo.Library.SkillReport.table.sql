﻿CREATE TABLE [dbo].[Library.SkillReport] (
    [Id]          BIGINT IDENTITY (1, 1) NOT NULL,
    [Frequency]   INT    NOT NULL,
    [SkillId]     BIGINT NOT NULL,
    [StateAreaId] BIGINT NOT NULL
);

