﻿CREATE TABLE [dbo].[Report.StaffMemberOffice] (
    [Id]            BIGINT         NOT NULL,
    [OfficeName]    NVARCHAR (100) NOT NULL,
    [OfficeId]      BIGINT         NULL,
    [AssignedDate]  DATETIME       NULL,
    [StaffMemberId] BIGINT         NOT NULL
);

