﻿CREATE TABLE [dbo].[Report.EmployerOffice] (
    [Id]         BIGINT         NOT NULL,
    [OfficeName] NVARCHAR (100) NOT NULL,
    [OfficeId]   BIGINT         NULL,
    [EmployerId] BIGINT         NOT NULL
);

