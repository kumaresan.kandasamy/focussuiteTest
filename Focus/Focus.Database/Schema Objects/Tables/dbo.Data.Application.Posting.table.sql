﻿CREATE TABLE [dbo].[Data.Application.Posting] (
    [Id]            BIGINT          NOT NULL,
    [LensPostingId] NVARCHAR (150)  NOT NULL,
    [JobTitle]      NVARCHAR (255)  NULL,
    [PostingXml]    NVARCHAR (MAX)  NULL,
    [EmployerName]  NVARCHAR (255)  NULL,
    [Url]           NVARCHAR (2048) NULL,
    [ExternalId]    NVARCHAR (36)   NULL,
    [StatusId]      INT             NOT NULL,
    [OriginId]      BIGINT          NULL,
    [Html]          NVARCHAR (MAX)  NOT NULL,
    [ViewedCount]   INT             NOT NULL,
    [CreatedOn]     DATETIME        NOT NULL,
    [UpdatedOn]     DATETIME        NOT NULL,
    [JobId]         BIGINT          NULL,
		[JobLocation]		NVARCHAR(200)   NULL
);

