﻿CREATE TABLE [dbo].[Library.MilitaryOccupationGroup] (
    [Id]             BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (50) NOT NULL,
    [IsCommissioned] BIT           NOT NULL
);

