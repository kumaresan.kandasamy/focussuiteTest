﻿CREATE TABLE [dbo].[Data.Application.Campus] (
    [Id]          BIGINT         NOT NULL,
    [Name]        NVARCHAR (100) NULL,
    [IsNonCampus] BIT            NOT NULL
);

