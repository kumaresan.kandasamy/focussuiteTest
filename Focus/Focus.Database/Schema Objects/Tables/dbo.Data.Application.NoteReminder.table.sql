﻿CREATE TABLE [dbo].[Data.Application.NoteReminder] (
    [Id]               BIGINT         NOT NULL,
    [NoteReminderType] INT            NOT NULL,
    [Text]             NVARCHAR (MAX) NOT NULL,
    [EntityId]         BIGINT         NOT NULL,
    [EntityType]       INT            NOT NULL,
    [ReminderVia]      INT            NULL,
    [ReminderDue]      DATETIME       NULL,
    [ReminderSentOn]   DATETIME       NULL,
    [CreatedBy]        BIGINT         NOT NULL,
    [NoteType]         INT            NULL,
    [CreatedOn]        DATETIME       NOT NULL
);

