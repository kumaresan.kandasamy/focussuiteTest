﻿CREATE TABLE [dbo].[Data.Application.RecentlyPlaced] (
    [Id]            BIGINT   NOT NULL,
    [PlacementDate] DATETIME NOT NULL,
    [JobId]         BIGINT   NOT NULL,
    [PersonId]      BIGINT   NOT NULL
);

