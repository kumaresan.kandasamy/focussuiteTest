﻿CREATE TABLE [dbo].[Library.JobEmployerSkill] (
    [Id]         BIGINT IDENTITY (1, 1) NOT NULL,
    [SkillCount] INT    NOT NULL,
    [Rank]       INT    NOT NULL,
    [JobId]      BIGINT NOT NULL,
    [EmployerId] BIGINT NOT NULL,
    [SkillId]    BIGINT NOT NULL
);

