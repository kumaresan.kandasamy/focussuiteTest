﻿CREATE TABLE [dbo].[Data.Core.SessionState] (
    [Id]        BIGINT           NOT NULL,
    [Key]       NVARCHAR (100)   NOT NULL,
    [Value]     VARBINARY (MAX)  NOT NULL,
    [TypeOf]    NVARCHAR (200)   NOT NULL,
    [Size]      BIGINT           NOT NULL,
    [SessionId] UNIQUEIDENTIFIER NOT NULL,
    [DeletedOn] DATETIME         NULL
);

