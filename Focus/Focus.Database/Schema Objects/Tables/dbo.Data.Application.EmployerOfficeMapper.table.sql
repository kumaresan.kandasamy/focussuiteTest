﻿CREATE TABLE [dbo].[Data.Application.EmployerOfficeMapper] (
    [Id]               BIGINT NOT NULL,
    [OfficeUnassigned] BIT    NULL,
    [EmployerId]       BIGINT NOT NULL,
    [OfficeId]         BIGINT NOT NULL
);

