﻿CREATE TABLE [dbo].[Library.EducationInternshipStatement] (
    [Id]                         BIGINT         IDENTITY (1, 1) NOT NULL,
    [LocalisationId]             BIGINT         NOT NULL,
    [EducationInternshipSkillId] BIGINT         NOT NULL,
    [PastTenseStatement]         NVARCHAR (200) NOT NULL,
    [PresentTenseStatement]      NVARCHAR (200) NOT NULL
);

