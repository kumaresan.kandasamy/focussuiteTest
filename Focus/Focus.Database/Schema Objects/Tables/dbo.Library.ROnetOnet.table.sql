﻿CREATE TABLE [dbo].[Library.ROnetOnet] (
    [Id]      BIGINT IDENTITY (1, 1) NOT NULL,
    [BestFit] BIT    NOT NULL,
    [OnetId]  BIGINT NOT NULL,
    [ROnetId] BIGINT NOT NULL
);

