﻿CREATE TABLE [dbo].[Library.Skill] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [SkillType]      INT            NOT NULL,
    [Name]           NVARCHAR (100) NOT NULL,
    [LocalisationId] BIGINT         NOT NULL
);

