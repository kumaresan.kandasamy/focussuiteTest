﻿CREATE TABLE [dbo].[Data.Application.PersonNote] (
    [Id]         BIGINT         NOT NULL,
    [Note]       NVARCHAR (MAX) NOT NULL,
    [DateAdded]  DATETIME       NOT NULL,
    [EmployerId] BIGINT         NOT NULL,
    [PersonId]   BIGINT         NOT NULL
);

