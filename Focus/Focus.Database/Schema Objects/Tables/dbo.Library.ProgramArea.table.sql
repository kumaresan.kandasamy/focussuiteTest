﻿CREATE TABLE [dbo].[Library.ProgramArea] (
    [Id]            BIGINT          IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (255)  NOT NULL,
    [Description]   NVARCHAR (1000) NOT NULL,
    [IsClientData]  BIT             NOT NULL,
    [ClientDataTag] NVARCHAR (255)  NULL
);

