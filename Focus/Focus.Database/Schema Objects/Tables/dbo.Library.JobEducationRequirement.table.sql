﻿CREATE TABLE [dbo].[Library.JobEducationRequirement] (
    [Id]                       BIGINT           IDENTITY (1, 1) NOT NULL,
    [EducationRequirementType] INT              NOT NULL,
    [RequirementPercentile]    DECIMAL (24, 18) NOT NULL,
    [JobId]                    BIGINT           NOT NULL
);

