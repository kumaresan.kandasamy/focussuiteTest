﻿CREATE TABLE [dbo].[Library.Degree] (
    [Id]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (200) NOT NULL,
    [RcipCode]      NVARCHAR (10)  NULL,
    [IsDegreeArea]  BIT            NOT NULL,
    [IsClientData]  BIT            NOT NULL,
    [ClientDataTag] NVARCHAR (100) NULL,
    [Tier]          INT            NULL
);

