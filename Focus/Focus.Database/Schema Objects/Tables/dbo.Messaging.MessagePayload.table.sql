﻿CREATE TABLE [dbo].[Messaging.MessagePayload] (
    [Id]   UNIQUEIDENTIFIER NOT NULL,
    [Data] NVARCHAR (MAX)   NOT NULL
);

