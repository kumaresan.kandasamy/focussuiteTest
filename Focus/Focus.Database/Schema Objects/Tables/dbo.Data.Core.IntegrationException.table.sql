﻿CREATE TABLE [dbo].[Data.Core.IntegrationException] (
    [Id]               BIGINT          IDENTITY (1, 1) NOT NULL,
    [Message]          NVARCHAR (1000) NOT NULL,
    [StackTrace]       NVARCHAR (MAX)  NOT NULL,
    [IntegrationLogId] BIGINT          NOT NULL
);

