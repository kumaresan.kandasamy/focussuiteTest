﻿CREATE TABLE [dbo].[Library.InternshipReport] (
    [Id]                   BIGINT IDENTITY (1, 1) NOT NULL,
    [NumberAvailable]      INT    NOT NULL,
    [StateAreaId]          BIGINT NOT NULL,
    [InternshipCategoryId] BIGINT NOT NULL
);

