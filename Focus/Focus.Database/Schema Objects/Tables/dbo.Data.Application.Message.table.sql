﻿CREATE TABLE [dbo].[Data.Application.Message] (
    [Id]             BIGINT   NOT NULL,
    [IsSystemAlert]  BIT      NOT NULL,
    [EmployerId]     BIGINT   NULL,
    [ExpiresOn]      DATETIME NOT NULL,
    [Audience]       BIGINT   NULL,
    [MessageType]    INT      NULL,
    [EntityId]       BIGINT   NULL,
    [UserId]         BIGINT   NULL,
    [BusinessUnitId] BIGINT   NULL,
    [CreatedOn]      DATETIME NOT NULL,
		CreatedBy				 BIGINT   NULL
);

