﻿CREATE TABLE [dbo].[Data.Application.Issues]
(
    [Id]                                         BIGINT   NOT NULL,
    [PersonId]                                   BIGINT   NOT NULL,
    [NoLoginTriggered]                           BIT      NOT NULL,
    [NoLoginResolvedDate]                        DATETIME NULL,
    [JobOfferRejectionCount]                     INT      NOT NULL,
    [JobOfferRejectionResolvedDate]              DATETIME NULL,
    [JobOfferRejectionTriggered]                 BIT      NOT NULL,
    [NotReportingToInterviewCount]               INT      NOT NULL,
    [NotReportingToInterviewResolvedDate]        DATETIME NULL,
    [NotReportingToInterviewTriggered]           BIT      NOT NULL,
    [NotClickingOnLeadsCount]                    INT      NOT NULL,
    [NotClickingOnLeadsResolvedDate]             DATETIME NULL,
    [NotClickingOnLeadsTriggered]                BIT      NOT NULL,
    [NotRespondingToEmployerInvitesCount]        INT      NOT NULL,
    [NotRespondingToEmployerInvitesResolvedDate] DATETIME NULL,
    [NotRespondingToEmployerInvitesTriggered]    BIT      NOT NULL,
    [ShowingLowQualityMatchesCount]              INT      NOT NULL,
    [ShowingLowQualityMatchesResolvedDate]       DATETIME NULL,
    [ShowingLowQualityMatchesTriggered]          BIT      NOT NULL,
    [PostingLowQualityResumeCount]               INT      NOT NULL,
    [PostingLowQualityResumeResolvedDate]        DATETIME NULL,
    [PostingLowQualityResumeTriggered]           BIT      NOT NULL,
    [FollowUpRequested]                          BIT      NOT NULL,
    [PostHireFollowUpTriggered]                  BIT      NOT NULL,
    [PostHireFollowUpResolvedDate]               DATETIME NULL,
    [NotSearchingJobsTriggered]                  BIT      NOT NULL,
    [NotSearchingJobsResolvedDate]               DATETIME NULL,
    [LastLoggedInDateForAlert]                   DATETIME NULL,
    [InappropriateEmailAddress]                  BIT      NULL,
    [GivingPositiveFeedback]                     BIT      NULL,
    [GivingNegativeFeedback]                     BIT      NULL,
    [MigrantSeasonalFarmWorkerTriggered]         BIT      NOT NULL,
		[HasExpiredAlienCertificationRegistration]   BIT      NULL
);