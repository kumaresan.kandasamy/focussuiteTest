﻿CREATE TABLE [dbo].[Data.Core.IntegrationRequestResponse] (
    [Id]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [Url]                NVARCHAR (1000) NOT NULL,
    [RequestSentOn]      DATETIME        NOT NULL,
    [ResponseReceivedOn] DATETIME        NULL,
		[StatusCode]				 INT						 NULL,
		[StatusDescription]	 NVARCHAR(250)	 NULL,
    [IntegrationLogId]   BIGINT          NOT NULL
);

