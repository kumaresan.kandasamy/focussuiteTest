﻿CREATE TABLE [dbo].[Data.Application.PersonsCurrentOffice] (
    [Id]        BIGINT   NOT NULL,
    [StartTime] DATETIME NOT NULL,
    [PersonId]  BIGINT   NOT NULL,
    [OfficeId]  BIGINT   NOT NULL
);

