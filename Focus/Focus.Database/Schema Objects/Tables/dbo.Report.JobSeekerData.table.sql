﻿CREATE TABLE [dbo].[Report.JobSeekerData] (
    [Id]          BIGINT         NOT NULL,
    [Description] NVARCHAR (MAX) NOT NULL,
    [DataType]    INT            NOT NULL,
    [JobSeekerId] BIGINT         NOT NULL
);

