﻿CREATE TABLE [dbo].[Library.InternshipSkill] (
    [Id]                   BIGINT IDENTITY (1, 1) NOT NULL,
    [Rank]                 INT    NOT NULL,
    [SkillId]              BIGINT NOT NULL,
    [InternshipCategoryId] BIGINT NOT NULL
);

