﻿CREATE TABLE [dbo].[Report.JobSeekerMilitaryHistory] (
    [Id]                       BIGINT   NOT NULL,
    [JobSeekerId]              BIGINT   NOT NULL,
    [VeteranType]              INT      NULL,
    [VeteranDisability]        INT      NULL,
    [VeteranTransitionType]    INT      NULL,
    [VeteranMilitaryDischarge] INT      NULL,
    [VeteranBranchOfService]   INT      NULL,
    [CampaignVeteran]          BIT      NULL,
    [MilitaryStartDate]        DATETIME NULL,
    [MilitaryEndDate]          DATETIME NULL
);

