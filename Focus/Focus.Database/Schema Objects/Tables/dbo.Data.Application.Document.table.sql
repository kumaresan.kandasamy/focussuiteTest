﻿CREATE TABLE [dbo].[Data.Application.Document] (
    [Id]          BIGINT          NOT NULL,
    [File]        VARBINARY (MAX) NOT NULL,
    [Title]       NVARCHAR (200)  NOT NULL,
    [Description] NVARCHAR (2000) NULL,
    [Category]    BIGINT          NOT NULL,
    [Group]       BIGINT          NOT NULL,
    [Order]       INT             NOT NULL,
    [Module]      INT             NOT NULL,
    [FileName]    NVARCHAR (200)  NOT NULL,
		[MimeType]    NVARCHAR(100)   NULL
);

