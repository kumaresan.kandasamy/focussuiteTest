﻿CREATE TABLE [dbo].[Data.Application.UserAlert] (
    [Id]                      BIGINT    NOT NULL,
    [UserId]                  BIGINT    NOT NULL,
    [NewReferralRequests]     BIT       NOT NULL,
    [EmployerAccountRequests] BIT       NOT NULL,
    [NewJobPostings]          BIT       NOT NULL,
    [Frequency]               NCHAR (1) NOT NULL,
    [CreatedOn]               DATETIME  NOT NULL,
    [UpdatedOn]               DATETIME  NOT NULL
);

