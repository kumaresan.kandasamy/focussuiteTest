﻿CREATE TABLE [dbo].[Data.Application.PersonList] (
    [Id]       BIGINT         NOT NULL,
    [Name]     NVARCHAR (100) NOT NULL,
    [ListType] INT            NOT NULL,
    [PersonId] BIGINT         NOT NULL
);

