﻿CREATE TABLE [dbo].[Report.JobSeekerMilitaryHistory_NonPurged] 
(
	[Id]												BIGINT NOT NULL,
	[VeteranDisability]					INT NULL,
	[VeteranTransitionType]			INT NULL,
	[VeteranMilitaryDischarge]	INT NULL,
	[VeteranBranchOfService]		INT NULL,
	[CampaignVeteran]						BIT NULL,
	[MilitaryStartDate]					DATETIME NULL,
	[MilitaryEndDate]						DATETIME NULL,
	[JobSeekerId]								BIGINT NULL,
	[CreatedOn]									DATETIME NOT NULL,
	[UpdatedOn]									DATETIME NOT NULL,
	[DeletedOn]									DATETIME NULL,
	[VeteranType]								INT NULL
);


