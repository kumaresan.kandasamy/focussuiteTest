﻿CREATE TABLE [dbo].[Library.OnetWord] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]        NVARCHAR (200) NOT NULL,
    [Word]       NVARCHAR (30)  NOT NULL,
    [Stem]       NVARCHAR (30)  NOT NULL,
    [Frequency]  INT            NULL,
    [OnetSoc]    NVARCHAR (10)  NOT NULL,
    [OnetId]     BIGINT         NOT NULL,
    [OnetRingId] BIGINT         NOT NULL
);

