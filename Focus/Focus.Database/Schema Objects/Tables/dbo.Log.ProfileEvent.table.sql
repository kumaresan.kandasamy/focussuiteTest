﻿CREATE TABLE [dbo].[Log.ProfileEvent] (
    [Id]            BIGINT           NOT NULL,
    [SessionId]     UNIQUEIDENTIFIER NOT NULL,
    [RequestId]     UNIQUEIDENTIFIER NOT NULL,
    [UserId]        BIGINT           NOT NULL,
    [Server]        NVARCHAR (50)    NOT NULL,
    [Application]   NVARCHAR (30)    NOT NULL,
    [InTime]        DATETIME         NOT NULL,
    [OutTime]       DATETIME         NOT NULL,
    [Milliseconds]  BIGINT           NOT NULL,
    [DeclaringType] NVARCHAR (200)   NOT NULL,
    [Method]        NVARCHAR (200)   NOT NULL,
    [Parameters]    NVARCHAR (MAX)   NOT NULL
);

