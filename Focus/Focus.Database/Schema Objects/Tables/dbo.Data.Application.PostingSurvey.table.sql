﻿CREATE TABLE [dbo].[Data.Application.PostingSurvey] (
    [Id]                BIGINT NOT NULL,
    [SurveyType]        INT    NOT NULL,
    [SatisfactionLevel] BIGINT NOT NULL,
    [DidInterview]      BIT    NOT NULL,
    [DidHire]           BIT    NOT NULL,
    [DidOffer]          BIT    NULL,
    [JobId]             BIGINT NOT NULL
);

