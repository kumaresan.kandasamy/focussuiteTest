﻿CREATE TABLE [dbo].[Library.State] (
    [Id]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [CountryId] BIGINT        NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    [Code]      NVARCHAR (2)  NOT NULL,
    [SortOrder] INT           NOT NULL
);

