﻿CREATE TABLE [dbo].[Report.JobOrderOffice] (
    [Id]         BIGINT         NOT NULL,
    [OfficeName] NVARCHAR (100) NOT NULL,
    [OfficeId]   BIGINT         NULL,
    [JobOrderId] BIGINT         NOT NULL
);

