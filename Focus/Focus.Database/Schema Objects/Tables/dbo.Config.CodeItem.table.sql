﻿CREATE TABLE [dbo].[Config.CodeItem] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]            NVARCHAR (200) NOT NULL,
    [IsSystem]       BIT            NOT NULL,
    [ParentKey]      NVARCHAR (200) NULL,
    [ExternalId]     NVARCHAR (36)  NULL,
    [CustomFilterId] INT            NULL
);

