﻿CREATE TABLE [dbo].[Data.Application.ResumeSelectedData] (
    [Id]         BIGINT         NOT NULL,
    [ResumeId]   BIGINT         NOT NULL,
    [LookupType] NVARCHAR (200) NOT NULL,
    [LookupId]   BIGINT         NOT NULL
);

