﻿CREATE TABLE [dbo].[Data.Application.JobPostingOfInterestSent] (
    [Id]        BIGINT   NOT NULL,
    [CreatedOn] DATETIME NOT NULL,
    [PostingId] BIGINT   NOT NULL,
    [JobId]     BIGINT   NULL,
    [PersonId]  BIGINT   NOT NULL,
		[Score]			INT			 NULL,
		[Applied]		BIT			 NOT NULL,
		[IsRecommendation]   BIT NULL
);

