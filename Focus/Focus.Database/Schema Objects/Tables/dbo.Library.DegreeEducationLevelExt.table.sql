﻿CREATE TABLE [dbo].[Library.DegreeEducationLevelExt] (
    [Id]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (200) NOT NULL,
    [Url]                    NVARCHAR (200) NOT NULL,
    [Description]            NVARCHAR (MAX) NULL,
    [DegreeEducationLevelId] BIGINT         NOT NULL
);

