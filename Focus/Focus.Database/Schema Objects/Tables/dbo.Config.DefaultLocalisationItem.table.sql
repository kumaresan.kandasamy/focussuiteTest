﻿CREATE TABLE [dbo].[Config.DefaultLocalisationItem] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]          NVARCHAR (200) NOT NULL,
    [DefaultValue] NVARCHAR (MAX) NOT NULL,
    [CreatedOn]    DATETIME       NOT NULL
);

