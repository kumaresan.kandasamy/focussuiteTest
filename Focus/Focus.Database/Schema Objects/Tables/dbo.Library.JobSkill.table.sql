﻿CREATE TABLE [dbo].[Library.JobSkill] (
    [Id]               BIGINT           IDENTITY (1, 1) NOT NULL,
    [JobSkillType]     INT              NOT NULL,
    [DemandPercentile] DECIMAL (24, 18) NOT NULL,
    [Rank]             INT              NOT NULL,
    [JobId]            BIGINT           NOT NULL,
    [SkillId]          BIGINT           NOT NULL
);

