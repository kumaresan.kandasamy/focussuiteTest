﻿CREATE TABLE [dbo].[Library.JobCareerPathFrom] (
    [Id]        BIGINT IDENTITY (1, 1) NOT NULL,
    [Rank]      INT    NOT NULL,
    [FromJobId] BIGINT NOT NULL,
    [ToJobId]   BIGINT NOT NULL
);

