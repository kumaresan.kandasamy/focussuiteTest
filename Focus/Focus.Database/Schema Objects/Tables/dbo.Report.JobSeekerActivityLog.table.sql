﻿CREATE TABLE [dbo].[Report.JobSeekerActivityLog] (
    [Id]              BIGINT         NOT NULL,
    [JobSeekerId]     BIGINT         NOT NULL,
    [ActionTypeName]  NVARCHAR (200) NOT NULL,
    [ActionDate]      DATETIME       NOT NULL,
    [FocusUserId]     BIGINT         NOT NULL,
    [StaffMemberID]   BIGINT         NULL,
    [StaffExternalId] NVARCHAR (255) NULL,
    [OfficeId]        BIGINT         NULL,
    [OfficeName]      NVARCHAR (100) NULL
);

