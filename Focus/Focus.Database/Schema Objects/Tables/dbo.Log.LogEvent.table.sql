﻿CREATE TABLE [dbo].[Log.LogEvent] (
    [Id]            BIGINT           NOT NULL,
    [SessionId]     UNIQUEIDENTIFIER NOT NULL,
    [RequestId]     UNIQUEIDENTIFIER NOT NULL,
    [UserId]        BIGINT           NOT NULL,
    [Server]        NVARCHAR (50)    NOT NULL,
    [Application]   NVARCHAR (30)    NOT NULL,
    [EventDate]     DATETIME         NOT NULL,
    [Severity]      INT              NOT NULL,
    [Message]       NVARCHAR (1024)  NOT NULL,
    [Exception]     NVARCHAR (MAX)   NOT NULL,
    [DeclaringType] NVARCHAR (200)   NOT NULL,
    [Method]        NVARCHAR (200)   NOT NULL,
    [Parameters]    NVARCHAR (MAX)   NOT NULL,
    [LogItems]      NVARCHAR (MAX)   NOT NULL
);

