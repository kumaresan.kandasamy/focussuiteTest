﻿CREATE TABLE [dbo].[Library.JobTaskMultiOption] (
    [Id]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]       NVARCHAR (200) NOT NULL,
    [JobTaskId] BIGINT         NOT NULL
);

