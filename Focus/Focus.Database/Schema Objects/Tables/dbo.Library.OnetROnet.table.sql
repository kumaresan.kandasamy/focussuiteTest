﻿CREATE TABLE [dbo].[Library.OnetROnet] (
    [Id]      BIGINT IDENTITY (1, 1) NOT NULL,
    [OnetId]  BIGINT NOT NULL,
    [ROnetId] BIGINT NOT NULL,
    [BestFit] BIT    NOT NULL
);

