﻿CREATE TABLE [dbo].[Library.JobDegreeLevel] (
    [Id]          BIGINT IDENTITY (1, 1) NOT NULL,
    [DegreeLevel] INT    NOT NULL,
    [JobId]       BIGINT NOT NULL
);

