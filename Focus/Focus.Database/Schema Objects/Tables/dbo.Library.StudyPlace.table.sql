﻿CREATE TABLE [dbo].[Library.StudyPlace] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [StateAreaId] BIGINT         NOT NULL
);

