﻿CREATE TABLE [dbo].[Data.Application.Bookmark] (
    [Id]       BIGINT         NOT NULL,
    [Name]     NVARCHAR (200) NULL,
    [Type]     INT            NOT NULL,
    [Criteria] NVARCHAR (MAX) NULL,
    [UserId]   BIGINT         NULL,
    [UserType] INT            NOT NULL,
    [EntityId] BIGINT         NULL
);

