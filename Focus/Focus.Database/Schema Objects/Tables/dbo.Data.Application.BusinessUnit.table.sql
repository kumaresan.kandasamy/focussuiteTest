﻿CREATE TABLE [dbo].[Data.Application.BusinessUnit]
(
	[Id]                       BIGINT          NOT NULL,
	[Name]                     NVARCHAR (200)  NOT NULL,
	[IsPrimary]                BIT             NOT NULL,
	[Url]                      NVARCHAR (1000) NULL,
	[OwnershipTypeId]          BIGINT          NOT NULL,
	[IndustrialClassification] NVARCHAR (200)  NOT NULL,
	[PrimaryPhone]             NVARCHAR (20)   NOT NULL,
	[PrimaryPhoneExtension]    NVARCHAR (20)   NULL,
	[PrimaryPhoneType]         NVARCHAR (20)   NULL,
	[AlternatePhone1]          NVARCHAR (20)   NULL,
	[AlternatePhone1Type]      NVARCHAR (20)   NULL,
	[AlternatePhone2]          NVARCHAR (20)   NULL,
	[AlternatePhone2Type]      NVARCHAR (20)   NULL,
	[EmployerId]               BIGINT          NOT NULL,
	[IsPreferred]              BIT             NOT NULL,
	[AccountTypeId]            BIGINT          NULL,
	[NoOfEmployees]            BIGINT          NULL,
	[ApprovalStatus]           INT             NOT NULL,
	[ExternalId]               NVARCHAR (50)   NULL,
	[LegalName]                NVARCHAR (200)  NULL,
	[ApprovalStatusReason]     INT             NOT NULL,
	[AwaitingApprovalDate]     DATETIME        NULL,
	[RedProfanityWords]        NVARCHAR (1000) NULL,
	[YellowProfanityWords]     NVARCHAR (1000) NULL,
	[LockVersion]              INT             NOT NULL
);

