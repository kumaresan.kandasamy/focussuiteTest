﻿CREATE TABLE [dbo].[Data.Application.RegistrationPin] (
    [Id]           BIGINT         NOT NULL,
    [Pin]          NVARCHAR (16)  NOT NULL,
    [EmailAddress] NVARCHAR (200) NOT NULL,
		[CreatedOn]    DATETIME       NULL,
		[CreatedBy]    BIGINT         NULL
);

