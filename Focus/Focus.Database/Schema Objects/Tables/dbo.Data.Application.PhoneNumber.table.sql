﻿CREATE TABLE [dbo].[Data.Application.PhoneNumber] (
    [Id]         BIGINT        NOT NULL,
    [Number]     NVARCHAR (30) NOT NULL,
    [PhoneType]  INT           NOT NULL,
    [IsPrimary]  BIT           NOT NULL,
    [Extension]  NVARCHAR (10) NULL,
    [PersonId]   BIGINT        NOT NULL,
    [ProviderId] BIGINT        NULL
);

