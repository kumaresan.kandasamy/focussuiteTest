﻿CREATE TABLE [dbo].[Config.ConfigurationItem] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]          NVARCHAR (200) NOT NULL,
    [Value]        NVARCHAR (MAX) NULL,
    [InternalOnly] BIT            NOT NULL
);

