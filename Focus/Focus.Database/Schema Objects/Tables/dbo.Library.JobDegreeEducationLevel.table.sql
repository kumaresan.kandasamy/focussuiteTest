﻿CREATE TABLE [dbo].[Library.JobDegreeEducationLevel] (
    [Id]                     BIGINT IDENTITY (1, 1) NOT NULL,
    [ExcludeFromJob]         BIT    NOT NULL,
    [Tier]                   INT    NULL,
    [JobId]                  BIGINT NOT NULL,
    [DegreeEducationLevelId] BIGINT NOT NULL
);

