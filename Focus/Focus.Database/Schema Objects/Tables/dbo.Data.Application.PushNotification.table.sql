﻿CREATE TABLE [dbo].[Data.Application.PushNotification] (
    [Id]                      BIGINT         NOT NULL,
    [Title]                   NVARCHAR (MAX) DEFAULT ('') NOT NULL,
    [Text]                    NVARCHAR (MAX) DEFAULT ('') NOT NULL,
    [Sent]                    BIT            DEFAULT ((0)) NOT NULL,
    [PersonMobileDeviceToken] NVARCHAR (MAX) DEFAULT ('') NOT NULL,
    [CreatedOn]               DATETIME       NOT NULL,
    [PersonMobileDeviceId]    BIGINT         DEFAULT ((0)) NOT NULL,
    [Viewed]                  BIT            DEFAULT ((0)) NOT NULL
);



