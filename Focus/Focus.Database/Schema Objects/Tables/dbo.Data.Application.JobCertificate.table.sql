﻿CREATE TABLE [dbo].[Data.Application.JobCertificate] (
    [Id]          BIGINT         NOT NULL,
    [Certificate] NVARCHAR (MAX) NOT NULL,
    [JobId]       BIGINT         NOT NULL
);

