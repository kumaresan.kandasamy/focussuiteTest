﻿	CREATE TABLE [Config.StarRatingMinimumScore]
	(
		[Id] BIGINT NOT NULL IDENTITY(1, 1),
		MinimumScore INT NOT NULL,
		MaximumScore INT NOT NULL,
		StarRating INT NOT NULL
	)	