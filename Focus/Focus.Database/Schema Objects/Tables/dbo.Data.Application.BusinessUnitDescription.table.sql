﻿CREATE TABLE [dbo].[Data.Application.BusinessUnitDescription] (
    [Id]                   BIGINT          NOT NULL,
    [Title]                NVARCHAR (200)  NOT NULL,
    [Description]          NVARCHAR (2000) NOT NULL,
    [IsPrimary]            BIT             NOT NULL,
    [BusinessUnitId]       BIGINT          NOT NULL,
    [RedProfanityWords]    NVARCHAR (1000) NULL,
    [YellowProfanityWords] NVARCHAR (1000) NULL
);

