﻿CREATE TABLE [dbo].[Data.Application.JobPrevData] (
    [Id]                                   BIGINT          NOT NULL,
    [JobId]                                BIGINT          NOT NULL,
    [ClosingDate]                          DATE            NULL,
    [NumOpenings]                          INT             NULL,
    [Locations]                            NVARCHAR (200)  NULL,
    [Description]                          NVARCHAR (MAX)  NULL,
    [JobRequirements]                      NVARCHAR (1000) NULL,
    [JobDetails]                           NVARCHAR (1000) NULL,
    [JobSalaryBenefits]                    NVARCHAR (1000) NULL,
    [RecruitmentInformation]               NVARCHAR (MAX) NULL,
    [ForeignLabourCertificationDetails]    NVARCHAR (200)  NULL,
    [CourtOrderedAffirmativeActionDetails] NVARCHAR (200)  NULL,
    [FederalContractorDetails]             NVARCHAR (200)  NULL
);

