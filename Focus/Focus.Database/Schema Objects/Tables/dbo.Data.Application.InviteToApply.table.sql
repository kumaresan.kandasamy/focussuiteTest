﻿CREATE TABLE [dbo].[Data.Application.InviteToApply] (
    [Id]            BIGINT         NOT NULL,
    [LensPostingId] NVARCHAR (MAX) NOT NULL,
    [Viewed]        BIT            NOT NULL,
    [CreatedBy]     BIGINT         NOT NULL,
    [CreatedOn]     DATETIME       NOT NULL,
    [UpdatedOn]     DATETIME       NOT NULL,
    [JobId]         BIGINT         NOT NULL,
    [PersonId]      BIGINT         NOT NULL
);

