﻿CREATE TABLE [dbo].[Data.Application.EmployeeBusinessUnit] (
    [Id]             BIGINT NOT NULL,
    [Default]        BIT    NOT NULL,
    [BusinessUnitId] BIGINT NOT NULL,
    [EmployeeId]     BIGINT NOT NULL
);

