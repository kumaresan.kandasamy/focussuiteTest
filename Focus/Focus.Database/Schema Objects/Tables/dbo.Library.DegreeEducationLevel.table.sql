﻿CREATE TABLE [dbo].[Library.DegreeEducationLevel] (
    [Id]                BIGINT         IDENTITY (1, 1) NOT NULL,
    [EducationLevel]    INT            NOT NULL,
    [DegreesAwarded]    INT            NOT NULL,
    [Name]              NVARCHAR (200) NULL,
    [ExcludeFromReport] BIT            NOT NULL,
    [DegreeId]          BIGINT         NOT NULL
);

