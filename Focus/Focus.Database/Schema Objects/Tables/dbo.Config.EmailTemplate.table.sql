﻿CREATE TABLE [dbo].[Config.EmailTemplate] (
    [Id]                         BIGINT          IDENTITY (1, 1) NOT NULL,
    [EmailTemplateType]          INT             NOT NULL,
    [Subject]                    NVARCHAR (1000) NOT NULL,
    [Body]                       NVARCHAR (MAX)  NOT NULL,
    [Salutation]                 NVARCHAR (100)  NULL,
    [Recipient]                  NVARCHAR (100)  NULL,
    [SenderEmailType]            INT             NOT NULL,
    [ClientSpecificEmailAddress] NVARCHAR (200)  NULL
);

