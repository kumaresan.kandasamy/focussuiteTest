﻿CREATE TABLE [dbo].[Data.Core.IntegrationRequestResponseMessages] (
    [Id]                           BIGINT         IDENTITY (1, 1) NOT NULL,
    [RequestMessage]               NVARCHAR (MAX) NOT NULL,
    [ResponseMessage]              NVARCHAR (MAX) NULL,
    [IntegrationRequestResponseId] BIGINT         NOT NULL
);

