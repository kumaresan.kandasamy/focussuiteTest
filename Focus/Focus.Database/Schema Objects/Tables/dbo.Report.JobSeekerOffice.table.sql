﻿CREATE TABLE [dbo].[Report.JobSeekerOffice] (
    [Id]           BIGINT         NOT NULL,
    [OfficeName]   NVARCHAR (100) NOT NULL,
    [OfficeId]     BIGINT         NULL,
    [JobSeekerId]  BIGINT         NOT NULL,
    [AssignedDate] DATETIME       NULL
);

