﻿CREATE TABLE [dbo].[Library.StudyPlaceDegreeEducationLevel] (
    [Id]                     BIGINT IDENTITY (1, 1) NOT NULL,
    [StudyPlaceId]           BIGINT NOT NULL,
    [DegreeEducationLevelId] BIGINT NOT NULL
);

