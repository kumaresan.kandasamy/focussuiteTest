﻿CREATE TABLE [dbo].[Data.Application.PersonAddress] (
    [Id]          BIGINT         NOT NULL,
    [Line1]       NVARCHAR (200) NULL,
    [Line2]       NVARCHAR (200) NULL,
    [Line3]       NVARCHAR (200) NULL,
    [TownCity]    NVARCHAR (100) NOT NULL,
    [CountyId]    BIGINT         NULL,
    [PostcodeZip] NVARCHAR (20)  NOT NULL,
    [StateId]     BIGINT         NOT NULL,
    [CountryId]   BIGINT         NOT NULL,
    [IsPrimary]   BIT            NOT NULL,
    [PersonId]    BIGINT         NOT NULL
);

