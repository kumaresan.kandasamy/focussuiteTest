﻿CREATE TABLE [dbo].[Data.Application.JobDrivingLicenceEndorsement] (
    [Id]                          BIGINT NOT NULL,
    [DrivingLicenceEndorsementId] BIGINT NOT NULL,
    [JobId]                       BIGINT NOT NULL
);

