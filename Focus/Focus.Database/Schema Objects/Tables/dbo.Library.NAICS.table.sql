﻿CREATE TABLE [dbo].[Library.NAICS] (
    [Id]       BIGINT         IDENTITY (1, 1) NOT NULL,
    [Code]     NVARCHAR (6)   NOT NULL,
    [Name]     NVARCHAR (200) NOT NULL,
    [ParentId] BIGINT         NOT NULL
);

