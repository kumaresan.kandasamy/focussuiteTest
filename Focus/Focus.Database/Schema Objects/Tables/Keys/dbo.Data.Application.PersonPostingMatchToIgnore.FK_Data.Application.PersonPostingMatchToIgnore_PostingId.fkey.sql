﻿ALTER TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore]
    ADD CONSTRAINT [FK_Data.Application.PersonPostingMatchToIgnore_PostingId] FOREIGN KEY ([PostingId]) REFERENCES [dbo].[Data.Application.Posting] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

