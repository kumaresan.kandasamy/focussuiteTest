﻿ALTER TABLE [dbo].[Library.OnetLocalisationItem]
    ADD CONSTRAINT [FK_Library.OnetLocalisationItem_LocalisationId] FOREIGN KEY ([LocalisationId]) REFERENCES [dbo].[Library.Localisation] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

