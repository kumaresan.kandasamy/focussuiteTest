﻿ALTER TABLE [dbo].[Data.Application.DismissedMessage]
    ADD CONSTRAINT [FK_Data.Application.DismissedMessage_MessageId] FOREIGN KEY ([MessageId]) REFERENCES [dbo].[Data.Application.Message] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

