﻿ALTER TABLE [dbo].[Data.Application.PersonsCurrentOffice]
    ADD CONSTRAINT [FK_Data.Application.PersonsCurrentOffice_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

