﻿ALTER TABLE [dbo].[Data.Application.Application]
    ADD CONSTRAINT [FK_Data.Application.Application_PostingId] FOREIGN KEY ([PostingId]) REFERENCES [dbo].[Data.Application.Posting] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

