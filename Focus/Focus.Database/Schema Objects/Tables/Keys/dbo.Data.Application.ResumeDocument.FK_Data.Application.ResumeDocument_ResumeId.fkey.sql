﻿ALTER TABLE [dbo].[Data.Application.ResumeDocument]
    ADD CONSTRAINT [FK_Data.Application.ResumeDocument_ResumeId] FOREIGN KEY ([ResumeId]) REFERENCES [dbo].[Data.Application.Resume] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

