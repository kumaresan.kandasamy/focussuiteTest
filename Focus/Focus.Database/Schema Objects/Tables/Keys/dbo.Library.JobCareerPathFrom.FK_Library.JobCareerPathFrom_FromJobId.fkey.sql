﻿ALTER TABLE [dbo].[Library.JobCareerPathFrom]
    ADD CONSTRAINT [FK_Library.JobCareerPathFrom_FromJobId] FOREIGN KEY ([FromJobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

