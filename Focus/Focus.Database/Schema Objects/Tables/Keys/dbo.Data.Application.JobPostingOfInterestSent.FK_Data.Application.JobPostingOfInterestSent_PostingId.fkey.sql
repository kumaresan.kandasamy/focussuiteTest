﻿ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent]
    ADD CONSTRAINT [FK_Data.Application.JobPostingOfInterestSent_PostingId] FOREIGN KEY ([PostingId]) REFERENCES [dbo].[Data.Application.Posting] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

