﻿ALTER TABLE [dbo].[Data.Application.PersonAddress]
    ADD CONSTRAINT [FK_Data.Application.PersonAddress_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

