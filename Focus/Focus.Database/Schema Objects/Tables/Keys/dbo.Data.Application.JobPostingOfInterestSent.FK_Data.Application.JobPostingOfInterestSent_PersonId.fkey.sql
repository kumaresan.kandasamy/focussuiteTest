﻿ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent]
    ADD CONSTRAINT [FK_Data.Application.JobPostingOfInterestSent_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

