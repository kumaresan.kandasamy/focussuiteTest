﻿ALTER TABLE [dbo].[Library.StudyPlace]
    ADD CONSTRAINT [FK_Library.StudyPlace_StateAreaId] FOREIGN KEY ([StateAreaId]) REFERENCES [dbo].[Library.StateArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

