﻿ALTER TABLE [dbo].[Library.LocalisationItem]
    ADD CONSTRAINT [FK_Library.LocalisationItem_LocalisationId] FOREIGN KEY ([LocalisationId]) REFERENCES [dbo].[Library.Localisation] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

