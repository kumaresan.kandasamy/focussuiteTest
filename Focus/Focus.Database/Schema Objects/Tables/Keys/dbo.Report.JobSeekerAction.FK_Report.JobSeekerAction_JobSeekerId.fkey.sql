﻿ALTER TABLE [dbo].[Report.JobSeekerAction]
    ADD CONSTRAINT [FK_Report.JobSeekerAction_JobSeekerId] FOREIGN KEY ([JobSeekerId]) REFERENCES [dbo].[Report.JobSeeker] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

