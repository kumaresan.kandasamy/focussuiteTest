﻿ALTER TABLE [dbo].[Data.Application.Job]
    ADD CONSTRAINT [FK_Data.Application.Job_BusinessUnitDescriptionId] FOREIGN KEY ([BusinessUnitDescriptionId]) REFERENCES [dbo].[Data.Application.BusinessUnitDescription] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

