﻿ALTER TABLE [dbo].[Data.Application.JobNote]
    ADD CONSTRAINT [FK_Data.Application.JobNote_NoteId] FOREIGN KEY ([NoteId]) REFERENCES [dbo].[Data.Application.Note] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

