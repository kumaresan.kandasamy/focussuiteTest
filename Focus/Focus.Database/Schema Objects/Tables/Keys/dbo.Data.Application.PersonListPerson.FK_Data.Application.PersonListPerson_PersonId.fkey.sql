﻿ALTER TABLE [dbo].[Data.Application.PersonListPerson]
    ADD CONSTRAINT [FK_Data.Application.PersonListPerson_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

