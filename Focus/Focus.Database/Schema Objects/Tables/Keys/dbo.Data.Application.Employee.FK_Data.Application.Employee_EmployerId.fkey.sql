﻿ALTER TABLE [dbo].[Data.Application.Employee]
    ADD CONSTRAINT [FK_Data.Application.Employee_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Data.Application.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

