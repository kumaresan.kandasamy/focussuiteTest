﻿ALTER TABLE [dbo].[Library.JobCertification]
    ADD CONSTRAINT [FK_Library.JobCertification_CertificationId] FOREIGN KEY ([CertificationId]) REFERENCES [dbo].[Library.Certification] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

