﻿ALTER TABLE [dbo].[Library.InternshipSkill]
    ADD CONSTRAINT [FK_Library.InternshipSkill_InternshipCategoryId] FOREIGN KEY ([InternshipCategoryId]) REFERENCES [dbo].[Library.InternshipCategory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

