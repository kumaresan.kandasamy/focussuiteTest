﻿ALTER TABLE [dbo].[Data.Application.JobPostingOfInterestSent]
    ADD CONSTRAINT [FK_Data.Application.JobPostingOfInterestSent_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

