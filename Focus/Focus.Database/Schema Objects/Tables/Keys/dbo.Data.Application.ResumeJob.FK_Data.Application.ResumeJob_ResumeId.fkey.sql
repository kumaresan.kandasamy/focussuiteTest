﻿ALTER TABLE [dbo].[Data.Application.ResumeJob]
    ADD CONSTRAINT [FK_Data.Application.ResumeJob_ResumeId] FOREIGN KEY ([ResumeId]) REFERENCES [dbo].[Data.Application.Resume] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

