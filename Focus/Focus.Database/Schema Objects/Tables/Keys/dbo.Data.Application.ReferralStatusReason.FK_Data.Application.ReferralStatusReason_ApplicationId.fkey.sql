﻿ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]
    ADD CONSTRAINT [FK_Data.Application.ReferralStatusReason_ApplicationId] FOREIGN KEY ([ApplicationId])	REFERENCES [dbo].[Data.Application.Application] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

