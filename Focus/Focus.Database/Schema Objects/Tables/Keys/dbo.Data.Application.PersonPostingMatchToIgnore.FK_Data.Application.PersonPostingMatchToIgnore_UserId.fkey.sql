﻿ALTER TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore]
    ADD CONSTRAINT [FK_Data.Application.PersonPostingMatchToIgnore_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

