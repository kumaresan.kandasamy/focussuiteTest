﻿ALTER TABLE [dbo].[Data.Application.RecentlyPlaced]
    ADD CONSTRAINT [FK_Data.Application.RecentlyPlaced_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

