﻿ALTER TABLE [dbo].[Library.JobDegreeEducationLevelReport]
    ADD CONSTRAINT [FK_Library.JobDegreeEducationLevelReport_JobDegreeEducationLevelId] FOREIGN KEY ([JobDegreeEducationLevelId]) REFERENCES [dbo].[Library.JobDegreeEducationLevel] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

