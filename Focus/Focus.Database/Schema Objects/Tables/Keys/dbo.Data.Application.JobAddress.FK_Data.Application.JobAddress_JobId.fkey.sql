﻿ALTER TABLE [dbo].[Data.Application.JobAddress]
    ADD CONSTRAINT [FK_Data.Application.JobAddress_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

