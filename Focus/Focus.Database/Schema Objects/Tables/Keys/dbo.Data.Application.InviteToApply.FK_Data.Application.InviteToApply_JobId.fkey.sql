﻿ALTER TABLE [dbo].[Data.Application.InviteToApply]
    ADD CONSTRAINT [FK_Data.Application.InviteToApply_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

