﻿ALTER TABLE [dbo].[Data.Application.PostingSurvey]
    ADD CONSTRAINT [FK_Data.Application.PostingSurvey_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

