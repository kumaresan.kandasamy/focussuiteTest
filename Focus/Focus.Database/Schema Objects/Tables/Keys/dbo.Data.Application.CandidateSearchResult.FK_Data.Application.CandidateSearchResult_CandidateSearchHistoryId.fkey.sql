﻿ALTER TABLE [dbo].[Data.Application.CandidateSearchResult]
    ADD CONSTRAINT [FK_Data.Application.CandidateSearchResult_CandidateSearchHistoryId] FOREIGN KEY ([CandidateSearchHistoryId]) REFERENCES [dbo].[Data.Application.CandidateSearchHistory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

