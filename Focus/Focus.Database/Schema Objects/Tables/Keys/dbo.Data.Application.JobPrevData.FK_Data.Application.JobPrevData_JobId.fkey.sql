﻿ALTER TABLE [dbo].[Data.Application.JobPrevData]
    ADD CONSTRAINT [FK_Data.Application.JobPrevData_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

