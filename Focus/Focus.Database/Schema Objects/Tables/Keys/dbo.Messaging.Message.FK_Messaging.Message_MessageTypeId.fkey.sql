﻿ALTER TABLE [dbo].[Messaging.Message]
    ADD CONSTRAINT [FK_Messaging.Message_MessageTypeId] FOREIGN KEY ([MessageTypeId]) REFERENCES [dbo].[Messaging.MessageType] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

