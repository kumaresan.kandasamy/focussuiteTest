﻿ALTER TABLE [dbo].[Data.Application.RecentlyPlaced]
    ADD CONSTRAINT [FK_Data.Application.RecentlyPlaced_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

