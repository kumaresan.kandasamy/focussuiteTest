﻿ALTER TABLE [dbo].[Report.JobOrderAction]
    ADD CONSTRAINT [FK_Report.JobOrderAction_JobOrderId] FOREIGN KEY ([JobOrderId]) REFERENCES [dbo].[Report.JobOrder] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION NOT FOR REPLICATION;

