﻿ALTER TABLE [dbo].[Messaging.Message]
    ADD CONSTRAINT [FK_Messaging.Message_MessagePayloadId] FOREIGN KEY ([MessagePayloadId]) REFERENCES [dbo].[Messaging.MessagePayload] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

