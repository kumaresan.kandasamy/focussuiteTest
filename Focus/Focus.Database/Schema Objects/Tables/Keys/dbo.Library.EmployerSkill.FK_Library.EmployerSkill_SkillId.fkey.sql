﻿ALTER TABLE [dbo].[Library.EmployerSkill]
    ADD CONSTRAINT [FK_Library.EmployerSkill_SkillId] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Library.Skill] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

