﻿ALTER TABLE [dbo].[Library.SkillCategorySkill]
    ADD CONSTRAINT [FK_Library.SkillCategorySkill_SkillCategoryId] FOREIGN KEY ([SkillCategoryId]) REFERENCES [dbo].[Library.SkillCategory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

