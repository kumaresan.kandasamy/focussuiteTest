﻿ALTER TABLE [dbo].[Data.Application.Job]
    ADD CONSTRAINT [FK_Data.Application.Job_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Data.Application.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

