﻿ALTER TABLE [dbo].[Library.JobExperienceLevel]
    ADD CONSTRAINT [FK_Library.JobExperienceLevel_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

