﻿ALTER TABLE [dbo].[Data.Application.Job]
    ADD CONSTRAINT [FK_Data.Application.Job_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Data.Application.Employee] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

