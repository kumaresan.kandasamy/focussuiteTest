﻿ALTER TABLE [dbo].[Library.InternshipJobTitle]
    ADD CONSTRAINT [FK_Library.InternshipJobTitle_InternshipCategoryId] FOREIGN KEY ([InternshipCategoryId]) REFERENCES [dbo].[Library.InternshipCategory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

