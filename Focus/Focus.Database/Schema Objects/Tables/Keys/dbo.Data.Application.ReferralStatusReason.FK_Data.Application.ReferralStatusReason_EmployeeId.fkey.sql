﻿ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]
    ADD CONSTRAINT [FK_Data.Application.ReferralStatusReason_EmployeeId] FOREIGN KEY ([EmployeeId])	REFERENCES [dbo].[Data.Application.Employee] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

