﻿ALTER TABLE [dbo].[Data.Application.Employee]
    ADD CONSTRAINT [FK_Data.Application.Employee_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

