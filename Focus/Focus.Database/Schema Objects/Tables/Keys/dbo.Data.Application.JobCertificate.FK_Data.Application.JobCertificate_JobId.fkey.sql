﻿ALTER TABLE [dbo].[Data.Application.JobCertificate]
    ADD CONSTRAINT [FK_Data.Application.JobCertificate_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

