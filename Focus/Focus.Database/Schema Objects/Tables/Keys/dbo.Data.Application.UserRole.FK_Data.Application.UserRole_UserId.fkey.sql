﻿ALTER TABLE [dbo].[Data.Application.UserRole]
    ADD CONSTRAINT [FK_Data.Application.UserRole_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

