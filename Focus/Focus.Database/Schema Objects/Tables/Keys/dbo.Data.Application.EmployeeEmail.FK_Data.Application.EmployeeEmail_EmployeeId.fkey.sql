﻿ALTER TABLE [dbo].[Data.Application.EmployeeEmail]
    ADD CONSTRAINT [FK_Data.Application.EmployeeEmail_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Data.Application.Employee] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

