﻿ALTER TABLE [dbo].[Data.Application.PersonListPerson]
    ADD CONSTRAINT [FK_Data.Application.PersonListPerson_PersonListId] FOREIGN KEY ([PersonListId]) REFERENCES [dbo].[Data.Application.PersonList] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

