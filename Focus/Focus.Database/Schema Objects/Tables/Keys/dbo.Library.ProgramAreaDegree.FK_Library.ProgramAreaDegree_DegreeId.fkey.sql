﻿ALTER TABLE [dbo].[Library.ProgramAreaDegree]
    ADD CONSTRAINT [FK_Library.ProgramAreaDegree_DegreeId] FOREIGN KEY ([DegreeId]) REFERENCES [dbo].[Library.Degree] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

