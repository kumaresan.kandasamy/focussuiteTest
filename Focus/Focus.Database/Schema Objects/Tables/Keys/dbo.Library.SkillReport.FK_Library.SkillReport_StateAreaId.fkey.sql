﻿ALTER TABLE [dbo].[Library.SkillReport]
    ADD CONSTRAINT [FK_Library.SkillReport_StateAreaId] FOREIGN KEY ([StateAreaId]) REFERENCES [dbo].[Library.StateArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

