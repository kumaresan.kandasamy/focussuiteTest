﻿ALTER TABLE [dbo].[Library.InternshipReport]
    ADD CONSTRAINT [FK_Library.InternshipReport_InternshipCategoryId] FOREIGN KEY ([InternshipCategoryId]) REFERENCES [dbo].[Library.InternshipCategory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

