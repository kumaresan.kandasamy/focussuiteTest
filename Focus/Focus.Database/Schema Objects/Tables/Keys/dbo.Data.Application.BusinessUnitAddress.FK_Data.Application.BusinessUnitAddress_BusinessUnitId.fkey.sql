﻿ALTER TABLE [dbo].[Data.Application.BusinessUnitAddress]
    ADD CONSTRAINT [FK_Data.Application.BusinessUnitAddress_BusinessUnitId] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[Data.Application.BusinessUnit] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

