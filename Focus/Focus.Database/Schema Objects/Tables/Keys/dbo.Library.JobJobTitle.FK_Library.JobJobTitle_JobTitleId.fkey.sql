﻿ALTER TABLE [dbo].[Library.JobJobTitle]
    ADD CONSTRAINT [FK_Library.JobJobTitle_JobTitleId] FOREIGN KEY ([JobTitleId]) REFERENCES [dbo].[Library.JobTitle] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

