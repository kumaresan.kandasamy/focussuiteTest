﻿ALTER TABLE [dbo].[Data.Application.DismissedMessage]
    ADD CONSTRAINT [FK_Data.Application.DismissedMessage_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

