﻿ALTER TABLE [dbo].[Library.SkillCategorySkill]
    ADD CONSTRAINT [FK_Library.SkillCategorySkill_SkillId] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Library.Skill] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

