﻿ALTER TABLE [dbo].[Data.Application.MessageText]
    ADD CONSTRAINT [FK_Data.Application.MessageText_MessageId] FOREIGN KEY ([MessageId]) REFERENCES [dbo].[Data.Application.Message] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

