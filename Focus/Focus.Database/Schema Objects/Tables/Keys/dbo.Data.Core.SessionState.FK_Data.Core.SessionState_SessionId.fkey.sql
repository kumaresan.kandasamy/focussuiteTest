﻿ALTER TABLE [dbo].[Data.Core.SessionState]
    ADD CONSTRAINT [FK_Data.Core.SessionState_SessionId] FOREIGN KEY ([SessionId]) REFERENCES [dbo].[Data.Core.Session] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

