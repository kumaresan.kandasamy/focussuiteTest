﻿ALTER TABLE [dbo].[Data.Application.ResumeSelectedData]
    ADD CONSTRAINT [FK_Data.Application.ResumeSelectedData_ResumeId] FOREIGN KEY ([ResumeId]) REFERENCES [dbo].[Data.Application.Resume] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

