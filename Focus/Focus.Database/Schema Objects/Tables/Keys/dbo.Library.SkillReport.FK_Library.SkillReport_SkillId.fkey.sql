﻿ALTER TABLE [dbo].[Library.SkillReport]
    ADD CONSTRAINT [FK_Library.SkillReport_SkillId] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Library.Skill] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

