﻿ALTER TABLE [dbo].[Data.Application.ReferralEmail]
    ADD CONSTRAINT [FK_Data.Application.ReferralEmail_ApplicationId] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[Data.Application.Application] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

