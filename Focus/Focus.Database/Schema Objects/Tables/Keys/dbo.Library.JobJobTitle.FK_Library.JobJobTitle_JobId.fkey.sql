﻿ALTER TABLE [dbo].[Library.JobJobTitle]
    ADD CONSTRAINT [FK_Library.JobJobTitle_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

