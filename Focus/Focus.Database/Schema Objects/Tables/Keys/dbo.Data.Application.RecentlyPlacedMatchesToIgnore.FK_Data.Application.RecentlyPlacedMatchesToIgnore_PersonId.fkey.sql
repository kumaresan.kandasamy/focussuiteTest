﻿ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]
    ADD CONSTRAINT [FK_Data.Application.RecentlyPlacedMatchesToIgnore_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

