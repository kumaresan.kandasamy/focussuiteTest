﻿ALTER TABLE [dbo].[Library.JobDegreeEducationLevel]
    ADD CONSTRAINT [FK_Library.JobDegreeEducationLevel_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

