﻿ALTER TABLE [dbo].[Data.Application.EmployeeBusinessUnit]
    ADD CONSTRAINT [FK_Data.Application.EmployeeBusinessUnit_BusinessUnitId] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[Data.Application.BusinessUnit] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

