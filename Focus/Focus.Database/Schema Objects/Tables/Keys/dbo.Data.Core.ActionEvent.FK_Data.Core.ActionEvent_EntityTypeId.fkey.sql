﻿ALTER TABLE [dbo].[Data.Core.ActionEvent]
    ADD CONSTRAINT [FK_Data.Core.ActionEvent_EntityTypeId] FOREIGN KEY ([EntityTypeId]) REFERENCES [dbo].[Data.Core.EntityType] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

