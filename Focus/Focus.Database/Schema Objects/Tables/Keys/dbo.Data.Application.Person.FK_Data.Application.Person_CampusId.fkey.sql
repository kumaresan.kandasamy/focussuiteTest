﻿ALTER TABLE [dbo].[Data.Application.Person]
    ADD CONSTRAINT [FK_Data.Application.Person_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[Data.Application.Campus] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

