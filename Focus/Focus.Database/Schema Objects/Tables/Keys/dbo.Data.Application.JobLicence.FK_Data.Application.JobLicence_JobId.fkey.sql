﻿ALTER TABLE [dbo].[Data.Application.JobLicence]
    ADD CONSTRAINT [FK_Data.Application.JobLicence_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

