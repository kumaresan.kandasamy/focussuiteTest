﻿ALTER TABLE [dbo].[Data.Application.JobSeekerSurvey]
    ADD CONSTRAINT [FK_Data.Application.JobSeekerSurvey_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

