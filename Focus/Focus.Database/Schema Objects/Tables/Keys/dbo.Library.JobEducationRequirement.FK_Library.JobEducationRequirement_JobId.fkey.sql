﻿ALTER TABLE [dbo].[Library.JobEducationRequirement]
    ADD CONSTRAINT [FK_Library.JobEducationRequirement_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

