﻿ALTER TABLE [dbo].[Report.JobSeekerActivityLog]
    ADD CONSTRAINT [FK_Report.JobSeekerActivityLog_JobSeekerId] FOREIGN KEY ([JobSeekerId]) REFERENCES [dbo].[Report.JobSeeker] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

