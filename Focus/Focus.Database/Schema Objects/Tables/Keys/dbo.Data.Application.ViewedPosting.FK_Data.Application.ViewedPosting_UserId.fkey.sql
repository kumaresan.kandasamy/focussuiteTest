﻿ALTER TABLE [dbo].[Data.Application.ViewedPosting]
    ADD CONSTRAINT [FK_Data.Application.ViewedPosting_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

