﻿ALTER TABLE [dbo].[Report.StaffMemberCurrentOffice]
    ADD CONSTRAINT [FK_Report.StaffMemberCurrentOffice_StaffMemberId] FOREIGN KEY ([StaffMemberId]) REFERENCES [dbo].[Report.StaffMember] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

