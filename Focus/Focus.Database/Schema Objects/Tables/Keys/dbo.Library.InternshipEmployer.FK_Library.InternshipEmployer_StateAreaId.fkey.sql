﻿ALTER TABLE [dbo].[Library.InternshipEmployer]
    ADD CONSTRAINT [FK_Library.InternshipEmployer_StateAreaId] FOREIGN KEY ([StateAreaId]) REFERENCES [dbo].[Library.StateArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

