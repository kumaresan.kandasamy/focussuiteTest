﻿ALTER TABLE [dbo].[Config.CodeGroupItem]
    ADD CONSTRAINT [FK_Config.CodeGroupItem_CodeItemId] FOREIGN KEY ([CodeItemId]) REFERENCES [dbo].[Config.CodeItem] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

