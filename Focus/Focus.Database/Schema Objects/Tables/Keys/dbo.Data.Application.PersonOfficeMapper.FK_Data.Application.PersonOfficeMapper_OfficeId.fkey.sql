﻿ALTER TABLE [dbo].[Data.Application.PersonOfficeMapper]
    ADD CONSTRAINT [FK_Data.Application.PersonOfficeMapper_OfficeId] FOREIGN KEY ([OfficeId]) REFERENCES [dbo].[Data.Application.Office] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

