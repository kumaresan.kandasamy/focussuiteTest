﻿ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]
    ADD CONSTRAINT [FK_Data.Application.RecentlyPlacedMatchesToIgnore_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

