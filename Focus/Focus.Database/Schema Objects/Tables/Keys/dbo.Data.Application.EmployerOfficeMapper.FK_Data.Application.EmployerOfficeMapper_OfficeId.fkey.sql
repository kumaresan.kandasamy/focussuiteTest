﻿ALTER TABLE [dbo].[Data.Application.EmployerOfficeMapper]
    ADD CONSTRAINT [FK_Data.Application.EmployerOfficeMapper_OfficeId] FOREIGN KEY ([OfficeId]) REFERENCES [dbo].[Data.Application.Office] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

