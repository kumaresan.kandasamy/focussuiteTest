﻿ALTER TABLE [dbo].[Library.MilitaryOccupationGroupROnet]
    ADD CONSTRAINT [FK_Library.MilitaryOccupationGroupROnet_MilitaryOccupationGroupId] FOREIGN KEY ([MilitaryOccupationGroupId]) REFERENCES [dbo].[Library.MilitaryOccupationGroup] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

