﻿ALTER TABLE [dbo].[Library.JobReport]
    ADD CONSTRAINT [FK_Library.JobReport_StateAreaId] FOREIGN KEY ([StateAreaId]) REFERENCES [dbo].[Library.StateArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

