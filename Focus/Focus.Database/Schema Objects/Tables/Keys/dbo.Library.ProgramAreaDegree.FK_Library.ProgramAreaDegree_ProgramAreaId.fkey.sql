﻿ALTER TABLE [dbo].[Library.ProgramAreaDegree]
    ADD CONSTRAINT [FK_Library.ProgramAreaDegree_ProgramAreaId] FOREIGN KEY ([ProgramAreaId]) REFERENCES [dbo].[Library.ProgramArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

