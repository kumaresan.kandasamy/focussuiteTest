﻿ALTER TABLE [dbo].[Library.EmployerJob]
    ADD CONSTRAINT [FK_Library.EmployerJob_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Library.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

