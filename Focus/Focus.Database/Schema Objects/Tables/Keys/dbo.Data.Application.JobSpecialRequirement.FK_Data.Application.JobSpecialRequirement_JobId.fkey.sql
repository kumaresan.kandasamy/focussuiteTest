﻿ALTER TABLE [dbo].[Data.Application.JobSpecialRequirement]
    ADD CONSTRAINT [FK_Data.Application.JobSpecialRequirement_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

