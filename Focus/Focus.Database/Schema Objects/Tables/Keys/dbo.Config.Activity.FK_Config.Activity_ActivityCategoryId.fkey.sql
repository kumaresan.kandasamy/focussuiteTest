﻿ALTER TABLE [dbo].[Config.Activity]
    ADD CONSTRAINT [FK_Config.Activity_ActivityCategoryId] FOREIGN KEY ([ActivityCategoryId]) REFERENCES [dbo].[Config.ActivityCategory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

