﻿ALTER TABLE [dbo].[Data.Application.ResumeAddIn]
    ADD CONSTRAINT [FK_Data.Application.ResumeAddIn_ResumeId] FOREIGN KEY ([ResumeId]) REFERENCES [dbo].[Data.Application.Resume] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

