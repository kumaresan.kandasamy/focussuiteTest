﻿ALTER TABLE [dbo].[Library.StudyPlaceDegreeEducationLevel]
    ADD CONSTRAINT [FK_Library.StudyPlaceDegreeEducationLevel_StudyPlaceId] FOREIGN KEY ([StudyPlaceId]) REFERENCES [dbo].[Library.StudyPlace] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

