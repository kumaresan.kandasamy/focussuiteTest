﻿ALTER TABLE [dbo].[Data.Core.IntegrationRequestResponseMessages]
    ADD CONSTRAINT [FK_Data.Core.IntegrationRequestResponseMessages_IntegrationRequestResponseId] FOREIGN KEY ([IntegrationRequestResponseId]) REFERENCES [dbo].[Data.Core.IntegrationRequestResponse] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

