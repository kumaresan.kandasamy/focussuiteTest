﻿ALTER TABLE [dbo].[Config.CertificateLicenceLocalisationItem]
    ADD CONSTRAINT [FK_Config.CertificateLicenceLocalisationItem_LocalisationId] FOREIGN KEY ([LocalisationId]) REFERENCES [dbo].[Config.Localisation] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

