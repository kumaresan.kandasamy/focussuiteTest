﻿ALTER TABLE [dbo].[Data.Application.UploadRecord]
    ADD CONSTRAINT [FK_Data.Application.UploadRecord_UploadFileId] FOREIGN KEY ([UploadFileId]) REFERENCES [dbo].[Data.Application.UploadFile] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

