﻿ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore]
    ADD CONSTRAINT [FK_Data.Application.RecentlyPlacedMatchesToIgnore_RecentlyPlacedId] FOREIGN KEY ([RecentlyPlacedId]) REFERENCES [dbo].[Data.Application.RecentlyPlaced] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

