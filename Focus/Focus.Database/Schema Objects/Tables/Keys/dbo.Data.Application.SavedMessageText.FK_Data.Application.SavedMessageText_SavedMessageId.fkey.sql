﻿ALTER TABLE [dbo].[Data.Application.SavedMessageText]
    ADD CONSTRAINT [FK_Data.Application.SavedMessageText_SavedMessageId] FOREIGN KEY ([SavedMessageId]) REFERENCES [dbo].[Data.Application.SavedMessage] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

