﻿ALTER TABLE [dbo].[Report.JobSeekerJobHistory]
    ADD CONSTRAINT [FK_Report.JobSeekerJobHistory_JobSeekerId] FOREIGN KEY ([JobSeekerId]) REFERENCES [dbo].[Report.JobSeeker] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

