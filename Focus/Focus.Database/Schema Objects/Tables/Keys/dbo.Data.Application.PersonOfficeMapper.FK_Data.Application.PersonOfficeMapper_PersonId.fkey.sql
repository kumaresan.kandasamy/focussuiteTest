﻿ALTER TABLE [dbo].[Data.Application.PersonOfficeMapper]
    ADD CONSTRAINT [FK_Data.Application.PersonOfficeMapper_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

