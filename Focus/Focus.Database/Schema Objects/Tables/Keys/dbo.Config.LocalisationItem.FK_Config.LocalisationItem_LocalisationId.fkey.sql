﻿ALTER TABLE [dbo].[Config.LocalisationItem]
    ADD CONSTRAINT [FK_Config.LocalisationItem_LocalisationId] FOREIGN KEY ([LocalisationId]) REFERENCES [dbo].[Config.Localisation] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

