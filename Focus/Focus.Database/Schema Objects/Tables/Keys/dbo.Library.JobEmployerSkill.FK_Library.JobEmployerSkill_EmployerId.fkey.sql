﻿ALTER TABLE [dbo].[Library.JobEmployerSkill]
    ADD CONSTRAINT [FK_Library.JobEmployerSkill_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Library.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

