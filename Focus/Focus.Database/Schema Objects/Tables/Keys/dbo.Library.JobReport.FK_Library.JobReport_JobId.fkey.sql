﻿ALTER TABLE [dbo].[Library.JobReport]
    ADD CONSTRAINT [FK_Library.JobReport_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

