﻿ALTER TABLE [dbo].[Data.Core.IntegrationRequestResponse]
    ADD CONSTRAINT [FK_Data.Core.IntegrationRequestResponse_IntegrationLogId] FOREIGN KEY ([IntegrationLogId]) REFERENCES [dbo].[Data.Core.IntegrationLog] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

