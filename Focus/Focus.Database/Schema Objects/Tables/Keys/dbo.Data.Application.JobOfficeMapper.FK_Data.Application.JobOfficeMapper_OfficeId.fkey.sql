﻿ALTER TABLE [dbo].[Data.Application.JobOfficeMapper]
    ADD CONSTRAINT [FK_Data.Application.JobOfficeMapper_OfficeId] FOREIGN KEY ([OfficeId]) REFERENCES [dbo].[Data.Application.Office] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

