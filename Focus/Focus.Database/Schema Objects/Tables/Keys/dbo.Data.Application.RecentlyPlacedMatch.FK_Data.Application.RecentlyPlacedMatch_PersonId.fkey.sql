﻿ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatch]
    ADD CONSTRAINT [FK_Data.Application.RecentlyPlacedMatch_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

