﻿ALTER TABLE [dbo].[Data.Application.Person]
    ADD CONSTRAINT [FK_Data.Application.Person_AssignedToId] FOREIGN KEY ([AssignedToId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

