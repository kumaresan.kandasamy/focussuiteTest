﻿ALTER TABLE [dbo].[Data.Application.EmployerOfficeMapper]
    ADD CONSTRAINT [FK_Data.Application.EmployerOfficeMapper_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Data.Application.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

