﻿ALTER TABLE [dbo].[Data.Application.InviteToApply]
    ADD CONSTRAINT [FK_Data.Application.InviteToApply_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

