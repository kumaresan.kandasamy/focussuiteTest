﻿ALTER TABLE [dbo].[Data.Application.PhoneNumber]
    ADD CONSTRAINT [FK_Data.Application.PhoneNumber_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

