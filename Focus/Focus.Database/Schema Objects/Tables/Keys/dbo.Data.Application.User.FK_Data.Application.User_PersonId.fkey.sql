﻿ALTER TABLE [dbo].[Data.Application.User]
    ADD CONSTRAINT [FK_Data.Application.User_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

