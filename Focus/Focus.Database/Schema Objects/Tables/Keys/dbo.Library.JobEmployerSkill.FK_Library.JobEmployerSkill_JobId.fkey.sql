﻿ALTER TABLE [dbo].[Library.JobEmployerSkill]
    ADD CONSTRAINT [FK_Library.JobEmployerSkill_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

