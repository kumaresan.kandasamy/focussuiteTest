﻿ALTER TABLE [dbo].[Library.EducationInternshipStatement]
    ADD CONSTRAINT [FK_Library.EducationInternshipStatement_EducationInternshipSkillId] FOREIGN KEY ([EducationInternshipSkillId]) REFERENCES [dbo].[Library.EducationInternshipSkill] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

