﻿ALTER TABLE [dbo].[Data.Application.NoteReminderRecipient]
    ADD CONSTRAINT [FK_Data.Application.NoteReminderRecipient_NoteReminderId] FOREIGN KEY ([NoteReminderId]) REFERENCES [dbo].[Data.Application.NoteReminder] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

