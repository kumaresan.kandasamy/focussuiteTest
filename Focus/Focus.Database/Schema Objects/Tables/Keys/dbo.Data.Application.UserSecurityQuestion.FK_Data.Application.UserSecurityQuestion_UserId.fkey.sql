﻿ALTER TABLE [dbo].[Data.Application.UserSecurityQuestion]
    ADD CONSTRAINT [FK_Data.Application.UserSecurityQuestion_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

