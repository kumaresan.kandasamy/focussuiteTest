﻿ALTER TABLE [dbo].[Library.JobDegreeEducationLevel]
    ADD CONSTRAINT [FK_Library.JobDegreeEducationLevel_DegreeEducationLevelId] FOREIGN KEY ([DegreeEducationLevelId]) REFERENCES [dbo].[Library.DegreeEducationLevel] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

