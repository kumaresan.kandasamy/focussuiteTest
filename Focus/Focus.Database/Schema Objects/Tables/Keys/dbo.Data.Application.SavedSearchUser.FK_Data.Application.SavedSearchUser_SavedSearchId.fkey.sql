﻿ALTER TABLE [dbo].[Data.Application.SavedSearchUser]
    ADD CONSTRAINT [FK_Data.Application.SavedSearchUser_SavedSearchId] FOREIGN KEY ([SavedSearchId]) REFERENCES [dbo].[Data.Application.SavedSearch] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

