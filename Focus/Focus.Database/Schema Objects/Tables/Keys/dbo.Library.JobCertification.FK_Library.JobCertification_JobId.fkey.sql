﻿ALTER TABLE [dbo].[Library.JobCertification]
    ADD CONSTRAINT [FK_Library.JobCertification_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

