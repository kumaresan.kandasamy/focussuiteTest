﻿ALTER TABLE [dbo].[Library.InternshipReport]
    ADD CONSTRAINT [FK_Library.InternshipReport_StateAreaId] FOREIGN KEY ([StateAreaId]) REFERENCES [dbo].[Library.StateArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

