﻿ALTER TABLE [dbo].[Data.Application.PersonNote]
    ADD CONSTRAINT [FK_Data.Application.PersonNote_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

