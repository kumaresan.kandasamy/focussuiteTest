﻿ALTER TABLE [dbo].[Data.Application.ExcludedLensPosting]
    ADD CONSTRAINT [FK_Data.Application.ExcludedLensPosting_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

