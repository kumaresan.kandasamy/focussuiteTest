﻿ALTER TABLE [dbo].[Data.Application.EmployeeJobFamily]
    ADD CONSTRAINT [FK_Data.Application.EmployeeJobFamily_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Data.Application.Employee] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

