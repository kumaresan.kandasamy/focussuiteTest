﻿ALTER TABLE [dbo].[Data.Application.JobIssues]
    ADD CONSTRAINT [FK_Data.Application.JobIssues_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

