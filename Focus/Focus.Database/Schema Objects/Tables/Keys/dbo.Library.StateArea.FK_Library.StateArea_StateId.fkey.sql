﻿ALTER TABLE [dbo].[Library.StateArea]
    ADD CONSTRAINT [FK_Library.StateArea_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[Library.State] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

