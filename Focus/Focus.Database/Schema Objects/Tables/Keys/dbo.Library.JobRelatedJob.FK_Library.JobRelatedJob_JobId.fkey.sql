﻿ALTER TABLE [dbo].[Library.JobRelatedJob]
    ADD CONSTRAINT [FK_Library.JobRelatedJob_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

