﻿ALTER TABLE [dbo].[Data.Application.PersonsCurrentOffice]
    ADD CONSTRAINT [FK_Data.Application.PersonsCurrentOffice_OfficeId] FOREIGN KEY ([OfficeId]) REFERENCES [dbo].[Data.Application.Office] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

