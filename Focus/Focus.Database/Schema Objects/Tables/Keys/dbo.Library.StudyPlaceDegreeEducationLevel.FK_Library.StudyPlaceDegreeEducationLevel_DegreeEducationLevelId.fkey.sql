﻿ALTER TABLE [dbo].[Library.StudyPlaceDegreeEducationLevel]
    ADD CONSTRAINT [FK_Library.StudyPlaceDegreeEducationLevel_DegreeEducationLevelId] FOREIGN KEY ([DegreeEducationLevelId]) REFERENCES [dbo].[Library.DegreeEducationLevel] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

