﻿ALTER TABLE [dbo].[Data.Application.SavedSearchUser]
    ADD CONSTRAINT [FK_Data.Application.SavedSearchUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

