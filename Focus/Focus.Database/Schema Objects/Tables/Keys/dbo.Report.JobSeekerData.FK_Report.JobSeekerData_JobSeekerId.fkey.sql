﻿ALTER TABLE [dbo].[Report.JobSeekerData]
    ADD CONSTRAINT [FK_Report.JobSeekerData_JobSeekerId] FOREIGN KEY ([JobSeekerId]) REFERENCES [dbo].[Report.JobSeeker] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

