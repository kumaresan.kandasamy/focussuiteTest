﻿ALTER TABLE [dbo].[Messaging.MessageLog]
    ADD CONSTRAINT [FK_Messaging.MessageLog_MessageId] FOREIGN KEY ([MessageId]) REFERENCES [dbo].[Messaging.Message] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

