﻿ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]
    ADD CONSTRAINT [FK_Data.Application.ReferralStatusReason_BusinessUnitId] FOREIGN KEY ([BusinessUnitId])	REFERENCES [dbo].[Data.Application.BusinessUnit] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

