﻿ALTER TABLE [dbo].[Library.MilitaryOccupation]
    ADD CONSTRAINT [FK_Library.MilitaryOccupation_MilitaryOccupationGroupId] FOREIGN KEY ([MilitaryOccupationGroupId]) REFERENCES [dbo].[Library.MilitaryOccupationGroup] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

