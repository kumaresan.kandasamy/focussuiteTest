﻿ALTER TABLE [dbo].[Data.Core.IntegrationException]
    ADD CONSTRAINT [FK_Data.Core.IntegrationException_IntegrationLogId] FOREIGN KEY ([IntegrationLogId]) REFERENCES [dbo].[Data.Core.IntegrationLog] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

