﻿ALTER TABLE [dbo].[Library.JobDegreeEducationLevelReport]
    ADD CONSTRAINT [FK_Library.JobDegreeEducationLevelReport_StateAreaId] FOREIGN KEY ([StateAreaId]) REFERENCES [dbo].[Library.StateArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

