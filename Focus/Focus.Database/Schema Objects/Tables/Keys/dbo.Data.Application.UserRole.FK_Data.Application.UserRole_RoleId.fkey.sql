﻿ALTER TABLE [dbo].[Data.Application.UserRole]
    ADD CONSTRAINT [FK_Data.Application.UserRole_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[Data.Application.Role] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

