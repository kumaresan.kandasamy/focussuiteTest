﻿ALTER TABLE [dbo].[Report.EmployerOffice]
    ADD CONSTRAINT [FK_Report.EmployerOffice_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Report.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

