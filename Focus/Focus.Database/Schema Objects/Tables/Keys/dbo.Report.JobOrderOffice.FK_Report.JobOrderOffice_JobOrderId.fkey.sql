﻿ALTER TABLE [dbo].[Report.JobOrderOffice]
    ADD CONSTRAINT [FK_Report.JobOrderOffice_JobOrderId] FOREIGN KEY ([JobOrderId]) REFERENCES [dbo].[Report.JobOrder] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

