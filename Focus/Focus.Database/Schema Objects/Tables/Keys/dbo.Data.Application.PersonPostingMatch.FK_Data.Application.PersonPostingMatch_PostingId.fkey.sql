﻿ALTER TABLE [dbo].[Data.Application.PersonPostingMatch]
    ADD CONSTRAINT [FK_Data.Application.PersonPostingMatch_PostingId] FOREIGN KEY ([PostingId]) REFERENCES [dbo].[Data.Application.Posting] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

