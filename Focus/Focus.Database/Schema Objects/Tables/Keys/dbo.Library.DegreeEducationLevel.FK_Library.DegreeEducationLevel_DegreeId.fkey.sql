﻿ALTER TABLE [dbo].[Library.DegreeEducationLevel]
    ADD CONSTRAINT [FK_Library.DegreeEducationLevel_DegreeId] FOREIGN KEY ([DegreeId]) REFERENCES [dbo].[Library.Degree] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

