﻿ALTER TABLE [dbo].[Data.Application.JobLanguage]
    ADD CONSTRAINT [FK_Data.Application.JobLanguage_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

