﻿ALTER TABLE [dbo].[Config.CodeGroupItem]
    ADD CONSTRAINT [FK_Config.CodeGroupItem_CodeGroupId] FOREIGN KEY ([CodeGroupId]) REFERENCES [dbo].[Config.CodeGroup] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

