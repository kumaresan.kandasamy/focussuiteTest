﻿ALTER TABLE [dbo].[Data.Application.EmployeeBusinessUnit]
    ADD CONSTRAINT [FK_Data.Application.EmployeeBusinessUnit_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Data.Application.Employee] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

