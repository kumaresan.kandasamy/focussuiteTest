ALTER TABLE [dbo].[Data.Application.PersonMobileDevice]
    ADD CONSTRAINT [FK_Data.Application.PersonMobileDevice_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

