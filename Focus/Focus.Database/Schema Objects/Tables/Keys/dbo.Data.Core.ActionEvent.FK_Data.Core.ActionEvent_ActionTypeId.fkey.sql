﻿ALTER TABLE [dbo].[Data.Core.ActionEvent]
    ADD CONSTRAINT [FK_Data.Core.ActionEvent_ActionTypeId] FOREIGN KEY ([ActionTypeId]) REFERENCES [dbo].[Data.Core.ActionType] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

