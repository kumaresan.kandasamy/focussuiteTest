﻿ALTER TABLE [dbo].[Data.Application.JobProgramOfStudy]
    ADD CONSTRAINT [FK_Data.Application.JobProgramOfStudy_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

