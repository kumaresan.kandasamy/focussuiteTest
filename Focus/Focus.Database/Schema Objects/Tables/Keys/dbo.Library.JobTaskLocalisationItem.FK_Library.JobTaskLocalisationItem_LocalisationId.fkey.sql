﻿ALTER TABLE [dbo].[Library.JobTaskLocalisationItem]
    ADD CONSTRAINT [FK_Library.JobTaskLocalisationItem_LocalisationId] FOREIGN KEY ([LocalisationId]) REFERENCES [dbo].[Library.Localisation] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

