﻿ALTER TABLE [dbo].[Library.JobCareerPathTo]
    ADD CONSTRAINT [FK_Library.JobCareerPathTo_CurrentJobId] FOREIGN KEY ([CurrentJobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

