﻿ALTER TABLE [dbo].[Report.EmployerAction]
    ADD CONSTRAINT [FK_Report.EmployerAction_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Report.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

