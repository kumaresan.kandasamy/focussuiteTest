﻿ALTER TABLE [dbo].[Library.JobDegreeLevel]
    ADD CONSTRAINT [FK_Library.JobDegreeLevel_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

