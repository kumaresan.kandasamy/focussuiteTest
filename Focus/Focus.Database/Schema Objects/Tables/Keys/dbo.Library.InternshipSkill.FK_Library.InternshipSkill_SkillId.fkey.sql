﻿ALTER TABLE [dbo].[Library.InternshipSkill]
    ADD CONSTRAINT [FK_Library.InternshipSkill_SkillId] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Library.Skill] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

