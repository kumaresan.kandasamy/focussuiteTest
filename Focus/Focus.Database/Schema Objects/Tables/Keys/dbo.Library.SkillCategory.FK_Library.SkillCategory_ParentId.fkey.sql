﻿ALTER TABLE [dbo].[Library.SkillCategory]
    ADD CONSTRAINT [FK_Library.SkillCategory_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[Library.SkillCategory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

