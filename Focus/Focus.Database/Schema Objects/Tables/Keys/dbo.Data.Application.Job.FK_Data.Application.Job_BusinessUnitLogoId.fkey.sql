﻿ALTER TABLE [dbo].[Data.Application.Job]
    ADD CONSTRAINT [FK_Data.Application.Job_BusinessUnitLogoId] FOREIGN KEY ([BusinessUnitLogoId]) REFERENCES [dbo].[Data.Application.BusinessUnitLogo] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

