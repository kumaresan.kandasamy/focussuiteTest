﻿ALTER TABLE [dbo].[Library.EmployerSkill]
    ADD CONSTRAINT [FK_Library.EmployerSkill_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Library.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

