﻿ALTER TABLE [dbo].[Library.EmployerJob]
    ADD CONSTRAINT [FK_Library.EmployerJob_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

