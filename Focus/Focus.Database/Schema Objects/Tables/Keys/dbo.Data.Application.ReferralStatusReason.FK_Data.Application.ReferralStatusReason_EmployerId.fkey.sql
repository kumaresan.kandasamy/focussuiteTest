﻿ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]
    ADD CONSTRAINT [FK_Data.Application.ReferralStatusReason_EmployerId] FOREIGN KEY ([EmployerId])	REFERENCES [dbo].[Data.Application.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

