﻿ALTER TABLE [dbo].[Library.JobEmployerSkill]
    ADD CONSTRAINT [FK_Library.JobEmployerSkill_SkillId] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Library.Skill] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

