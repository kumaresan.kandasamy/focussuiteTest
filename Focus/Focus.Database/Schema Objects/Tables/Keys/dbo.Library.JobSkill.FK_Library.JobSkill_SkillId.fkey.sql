﻿ALTER TABLE [dbo].[Library.JobSkill]
    ADD CONSTRAINT [FK_Library.JobSkill_SkillId] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[Library.Skill] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

