﻿ALTER TABLE [dbo].[Report.JobSeekerActivityLog]
    ADD CONSTRAINT [FK_Report.JobSeekerActivityLog_StaffMemberID] FOREIGN KEY ([StaffMemberID]) REFERENCES [dbo].[Report.StaffMember] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

