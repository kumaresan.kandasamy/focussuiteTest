﻿ALTER TABLE [dbo].[Data.Application.CandidateSearchHistory]
    ADD CONSTRAINT [FK_Data.Application.CandidateSearchHistory_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

