﻿ALTER TABLE [dbo].[Library.EmployerJob]
    ADD CONSTRAINT [FK_Library.EmployerJob_StateAreaId] FOREIGN KEY ([StateAreaId]) REFERENCES [dbo].[Library.StateArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

