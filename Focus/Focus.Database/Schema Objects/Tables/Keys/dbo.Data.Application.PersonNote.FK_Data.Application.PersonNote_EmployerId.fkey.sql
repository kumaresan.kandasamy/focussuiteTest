﻿ALTER TABLE [dbo].[Data.Application.PersonNote]
    ADD CONSTRAINT [FK_Data.Application.PersonNote_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Data.Application.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

