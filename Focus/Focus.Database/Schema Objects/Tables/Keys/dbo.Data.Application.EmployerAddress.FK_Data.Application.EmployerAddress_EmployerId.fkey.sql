﻿ALTER TABLE [dbo].[Data.Application.EmployerAddress]
    ADD CONSTRAINT [FK_Data.Application.EmployerAddress_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Data.Application.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

