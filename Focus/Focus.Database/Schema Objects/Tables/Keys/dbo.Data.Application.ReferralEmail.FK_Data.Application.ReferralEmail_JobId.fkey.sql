﻿ALTER TABLE [dbo].[Data.Application.ReferralEmail]
    ADD CONSTRAINT [FK_Data.Application.ReferralEmail_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

