﻿ALTER TABLE [dbo].[Data.Application.Posting]
    ADD CONSTRAINT [FK_Data.Application.Posting_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

