﻿ALTER TABLE [dbo].[Library.StateArea]
    ADD CONSTRAINT [FK_Library.StateArea_AreaId] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[Library.Area] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

