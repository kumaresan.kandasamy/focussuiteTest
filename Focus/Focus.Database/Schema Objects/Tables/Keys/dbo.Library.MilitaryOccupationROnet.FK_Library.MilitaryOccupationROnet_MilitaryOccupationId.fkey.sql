﻿ALTER TABLE [dbo].[Library.MilitaryOccupationROnet]
    ADD CONSTRAINT [FK_Library.MilitaryOccupationROnet_MilitaryOccupationId] FOREIGN KEY ([MilitaryOccupationId]) REFERENCES [dbo].[Library.MilitaryOccupation] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

