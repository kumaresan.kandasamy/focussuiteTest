﻿ALTER TABLE [dbo].[Library.DegreeAlias]
    ADD CONSTRAINT [FK_Library.DegreeAlias_DegreeId] FOREIGN KEY ([DegreeId]) REFERENCES [dbo].[Library.Degree] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

