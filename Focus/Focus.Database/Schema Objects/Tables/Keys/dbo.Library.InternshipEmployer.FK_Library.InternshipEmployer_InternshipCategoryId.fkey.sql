﻿ALTER TABLE [dbo].[Library.InternshipEmployer]
    ADD CONSTRAINT [FK_Library.InternshipEmployer_InternshipCategoryId] FOREIGN KEY ([InternshipCategoryId]) REFERENCES [dbo].[Library.InternshipCategory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

