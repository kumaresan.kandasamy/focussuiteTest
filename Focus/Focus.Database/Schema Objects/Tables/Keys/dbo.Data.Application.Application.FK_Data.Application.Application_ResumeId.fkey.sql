﻿ALTER TABLE [dbo].[Data.Application.Application]
    ADD CONSTRAINT [FK_Data.Application.Application_ResumeId] FOREIGN KEY ([ResumeId]) REFERENCES [dbo].[Data.Application.Resume] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

