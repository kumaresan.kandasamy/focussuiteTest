﻿ALTER TABLE [dbo].[Library.JobCareerPathFrom]
    ADD CONSTRAINT [FK_Library.JobCareerPathFrom_ToJobId] FOREIGN KEY ([ToJobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

