﻿ALTER TABLE [dbo].[Library.JobCareerArea]
    ADD CONSTRAINT [FK_Library.JobCareerArea_CareerAreaId] FOREIGN KEY ([CareerAreaId]) REFERENCES [dbo].[Library.CareerArea] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

