﻿ALTER TABLE [dbo].[Data.Application.JobOfficeMapper]
    ADD CONSTRAINT [FK_Data.Application.JobOfficeMapper_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

