﻿ALTER TABLE [dbo].[Data.Application.ResumeJobEducationInternshipSkill]
    ADD CONSTRAINT [FK_Data.Application.ResumeJobEducationInternshipSkill_ResumeJobId] FOREIGN KEY ([ResumeJobId]) REFERENCES [dbo].[Data.Application.ResumeJob] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

