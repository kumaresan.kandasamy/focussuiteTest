﻿ALTER TABLE [dbo].[Library.JobTaskMultiOption]
    ADD CONSTRAINT [FK_Library.JobTaskMultiOption_JobTaskId] FOREIGN KEY ([JobTaskId]) REFERENCES [dbo].[Library.JobTask] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

