﻿ALTER TABLE [dbo].[Library.JobSkill]
    ADD CONSTRAINT [FK_Library.JobSkill_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

