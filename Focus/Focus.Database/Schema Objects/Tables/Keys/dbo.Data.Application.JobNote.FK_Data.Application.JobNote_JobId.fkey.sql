﻿ALTER TABLE [dbo].[Data.Application.JobNote]
    ADD CONSTRAINT [FK_Data.Application.JobNote_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

