﻿ALTER TABLE [dbo].[Report.StaffMemberOffice]
    ADD CONSTRAINT [FK_Report.StaffMemberOffice_StaffMemberId] FOREIGN KEY ([StaffMemberId]) REFERENCES [dbo].[Report.StaffMember] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

