﻿ALTER TABLE [dbo].[Library.EducationInternshipSkill]
    ADD CONSTRAINT [FK_Library.EducationInternshipSkill_EducationInternshipCategoryId] FOREIGN KEY ([EducationInternshipCategoryId]) REFERENCES [dbo].[Library.EducationInternshipCategory] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

