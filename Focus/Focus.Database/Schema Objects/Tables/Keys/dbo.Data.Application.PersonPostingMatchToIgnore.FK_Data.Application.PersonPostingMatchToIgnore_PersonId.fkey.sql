﻿ALTER TABLE [dbo].[Data.Application.PersonPostingMatchToIgnore]
    ADD CONSTRAINT [FK_Data.Application.PersonPostingMatchToIgnore_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

