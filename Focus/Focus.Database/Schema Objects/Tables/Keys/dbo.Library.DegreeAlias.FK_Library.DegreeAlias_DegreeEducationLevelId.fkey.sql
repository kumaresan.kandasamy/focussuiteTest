﻿ALTER TABLE [dbo].[Library.DegreeAlias]
    ADD CONSTRAINT [FK_Library.DegreeAlias_DegreeEducationLevelId] FOREIGN KEY ([DegreeEducationLevelId]) REFERENCES [dbo].[Library.DegreeEducationLevel] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

