﻿ALTER TABLE [dbo].[Library.InternshipEmployer]
    ADD CONSTRAINT [FK_Library.InternshipEmployer_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Library.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

