﻿ALTER TABLE [dbo].[Data.Application.PersonPostcodeMapper]
    ADD CONSTRAINT [FK_Data.Application.PersonPostcodeMapper_OwnedById] FOREIGN KEY ([OwnedById]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

