﻿ALTER TABLE [dbo].[Data.Application.BusinessUnit]
    ADD CONSTRAINT [FK_Data.Application.BusinessUnit_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[Data.Application.Employer] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

