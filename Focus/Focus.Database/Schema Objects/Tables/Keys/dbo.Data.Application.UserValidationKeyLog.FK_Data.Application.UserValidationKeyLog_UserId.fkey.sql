﻿ALTER TABLE [dbo].[Data.Application.UserValidationKeyLog]
    ADD CONSTRAINT [FK_Data.Application.UserValidationKeyLog_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Data.Application.User] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

