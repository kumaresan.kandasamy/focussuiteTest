﻿ALTER TABLE [dbo].[Data.Application.JobSkill]
    ADD CONSTRAINT [FK_Data.Application.JobSkill_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

