﻿ALTER TABLE [dbo].[Data.Application.ReferralEmail]
    ADD CONSTRAINT [FK_Data.Application.ReferralEmail_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [dbo].[Data.Application.Employee] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

