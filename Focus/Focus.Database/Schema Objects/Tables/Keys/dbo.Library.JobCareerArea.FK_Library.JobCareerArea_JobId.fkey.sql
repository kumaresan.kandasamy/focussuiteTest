﻿ALTER TABLE [dbo].[Library.JobCareerArea]
    ADD CONSTRAINT [FK_Library.JobCareerArea_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Library.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

