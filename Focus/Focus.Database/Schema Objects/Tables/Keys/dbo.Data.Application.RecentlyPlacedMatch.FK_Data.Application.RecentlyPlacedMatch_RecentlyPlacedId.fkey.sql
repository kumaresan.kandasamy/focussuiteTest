﻿ALTER TABLE [dbo].[Data.Application.RecentlyPlacedMatch]
    ADD CONSTRAINT [FK_Data.Application.RecentlyPlacedMatch_RecentlyPlacedId] FOREIGN KEY ([RecentlyPlacedId]) REFERENCES [dbo].[Data.Application.RecentlyPlaced] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

