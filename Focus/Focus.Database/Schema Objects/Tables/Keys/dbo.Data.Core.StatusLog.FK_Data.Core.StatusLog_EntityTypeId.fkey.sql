﻿ALTER TABLE [dbo].[Data.Core.StatusLog]
    ADD CONSTRAINT [FK_Data.Core.StatusLog_EntityTypeId] FOREIGN KEY ([EntityTypeId]) REFERENCES [dbo].[Data.Core.EntityType] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

