﻿ALTER TABLE [dbo].[Data.Application.BusinessUnitLogo]
    ADD CONSTRAINT [FK_Data.Application.BusinessUnitLogo_BusinessUnitId] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[Data.Application.BusinessUnit] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

