﻿ALTER TABLE [dbo].[Data.Application.Job]
    ADD CONSTRAINT [FK_Data.Application.Job_BusinessUnitId] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[Data.Application.BusinessUnit] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

