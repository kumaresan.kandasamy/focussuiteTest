﻿ALTER TABLE [dbo].[Data.Application.JobEducationInternshipSkill]
    ADD CONSTRAINT [FK_Data.Application.JobEducationInternshipSkill_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

