﻿ALTER TABLE [dbo].[Data.Application.PersonPostcodeMapper]
    ADD CONSTRAINT [FK_Data.Application.PersonPostcodeMapper_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

