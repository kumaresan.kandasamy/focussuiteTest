﻿ALTER TABLE [dbo].[Report.JobSeekerActivityAssignment]
    ADD CONSTRAINT [FK_Report.JobSeekerActivityAssignment_JobSeekerId] FOREIGN KEY ([JobSeekerId]) REFERENCES [dbo].[Report.JobSeeker] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

