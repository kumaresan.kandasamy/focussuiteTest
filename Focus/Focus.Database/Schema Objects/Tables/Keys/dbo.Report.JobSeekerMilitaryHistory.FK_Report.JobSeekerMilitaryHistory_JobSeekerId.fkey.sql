﻿ALTER TABLE [dbo].[Report.JobSeekerMilitaryHistory]
    ADD CONSTRAINT [FK_Report.JobSeekerMilitaryHistory_JobSeekerId] FOREIGN KEY ([JobSeekerId]) REFERENCES [dbo].[Report.JobSeeker] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

