﻿ALTER TABLE [dbo].[Data.Application.PersonList]
    ADD CONSTRAINT [FK_Data.Application.PersonList_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

