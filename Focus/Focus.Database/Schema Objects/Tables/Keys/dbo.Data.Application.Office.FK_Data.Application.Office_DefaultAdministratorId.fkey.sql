﻿ALTER TABLE [dbo].[Data.Application.Office]
    ADD CONSTRAINT [FK_Data.Application.Office_DefaultAdministratorId] FOREIGN KEY ([DefaultAdministratorId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

