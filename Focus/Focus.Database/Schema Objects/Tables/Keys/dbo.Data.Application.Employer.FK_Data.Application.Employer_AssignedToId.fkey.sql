﻿ALTER TABLE [dbo].[Data.Application.Employer]
    ADD CONSTRAINT [FK_Data.Application.Employer_AssignedToId] FOREIGN KEY ([AssignedToId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

