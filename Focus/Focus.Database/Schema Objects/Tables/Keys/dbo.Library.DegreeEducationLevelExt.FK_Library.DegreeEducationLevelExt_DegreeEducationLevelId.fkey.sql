﻿ALTER TABLE [dbo].[Library.DegreeEducationLevelExt]
    ADD CONSTRAINT [FK_Library.DegreeEducationLevelExt_DegreeEducationLevelId] FOREIGN KEY ([DegreeEducationLevelId]) REFERENCES [dbo].[Library.DegreeEducationLevel] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

