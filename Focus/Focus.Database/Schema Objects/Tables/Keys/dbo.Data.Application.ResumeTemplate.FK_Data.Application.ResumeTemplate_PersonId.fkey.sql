﻿ALTER TABLE [dbo].[Data.Application.ResumeTemplate]
    ADD CONSTRAINT [FK_Data.Application.ResumeTemplate_PersonId] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;

