﻿CREATE TABLE [dbo].[Data.Application.ResumeTemplate] (
    [Id]        BIGINT         NOT NULL,
    [ResumeXml] NVARCHAR (MAX) NOT NULL,
    [CreatedOn] DATETIME       NOT NULL,
    [UpdatedOn] DATETIME       NOT NULL,
    [PersonId]  BIGINT         NOT NULL
);

