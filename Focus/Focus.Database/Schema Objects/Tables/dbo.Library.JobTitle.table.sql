﻿CREATE TABLE [dbo].[Library.JobTitle] (
    [Id]   BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (255) NOT NULL
);

