﻿CREATE TABLE [dbo].[Report.StaffMember] (
    [Id]                  BIGINT          NOT NULL,
    [FirstName]           NVARCHAR (100)  NULL,
    [LastName]            NVARCHAR (100)  NULL,
    [AddressLine1]        NVARCHAR (200)  NULL,
    [CountyId]            BIGINT          NULL,
    [County]              NVARCHAR (400)  NULL,
    [StateId]             BIGINT          NULL,
    [State]               NVARCHAR (400)  NULL,
    [PostalCode]          NVARCHAR (40)   NULL,
    [Office]              NVARCHAR (2000) NULL,
    [EmailAddress]        NVARCHAR (500)  NOT NULL,
    [AccountCreationDate] DATETIME        NOT NULL,
    [LatitudeRadians]     FLOAT           NOT NULL,
    [LongitudeRadians]    FLOAT           NOT NULL,
    [FocusPersonId]       BIGINT          NOT NULL,
    [CreatedOn]           DATETIME        NOT NULL,
    [UpdatedOn]           DATETIME        NOT NULL,
    [Blocked]             BIT             NOT NULL,
    [ExternalStaffID]     NVARCHAR (255)  NULL,
    [Unsubscribed]        BIT             NOT NULL,
    [LocalVeteranEmploymentRepresentative]        BIT            NULL,
    [DisabledVeteransOutreachProgramSpecialist]   BIT            NULL
);

