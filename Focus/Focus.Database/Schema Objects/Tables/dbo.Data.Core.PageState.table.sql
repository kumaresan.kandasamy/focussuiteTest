﻿CREATE TABLE [dbo].[Data.Core.PageState] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Value]        IMAGE            NOT NULL,
    [LastAccessed] DATETIME         NOT NULL,
    [Timeout]      INT              NOT NULL
);

