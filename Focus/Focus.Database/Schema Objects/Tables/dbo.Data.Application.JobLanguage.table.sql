﻿CREATE TABLE [dbo].[Data.Application.JobLanguage]
(
	[Id]                      BIGINT            NOT NULL,
	[Language]                NVARCHAR (MAX)    NOT NULL,
	[JobId]                   BIGINT            NOT NULL,
	[LanguageProficiencyID]   BIGINT            NULL
);



