﻿CREATE TABLE [dbo].[Library.OnetCommodity] (
    [Id]                    BIGINT         IDENTITY (1, 1) NOT NULL,
    [ToolTechnologyType]    INT            NOT NULL,
    [ToolTechnologyExample] NVARCHAR (MAX) NOT NULL,
    [Code]                  NVARCHAR (MAX) NOT NULL,
    [Title]                 NVARCHAR (MAX) NOT NULL,
    [OnetId]                BIGINT         NOT NULL
);

