﻿CREATE TABLE [dbo].[Data.Application.ResumeDocument] (
    [Id]            BIGINT          NOT NULL,
    [FileName]      NVARCHAR (100)  NOT NULL,
    [ContentType]   NVARCHAR (255)  NOT NULL,
    [DocumentBytes] VARBINARY (MAX) NOT NULL,
    [Html]          NVARCHAR (MAX)  NULL,
    [ResumeId]      BIGINT          NOT NULL
);

