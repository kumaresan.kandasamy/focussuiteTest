﻿CREATE TABLE [dbo].[Data.Application.Referral]
(
	Id BIGINT NOT NULL,
	PostingId BIGINT NOT NULL,
	PersonId BIGINT NOT NULL,
	UserId BIGINT NOT NULL,
	ActionerId BIGINT NOT NULL,
	ReferralDate DATETIME NOT NULL,
	ActionTypeId BIGINT NOT NULL,
	ReferralScore INT NULL
)