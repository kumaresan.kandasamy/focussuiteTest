﻿CREATE TABLE [dbo].[Library.ResumeSkill] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (100) NOT NULL,
    [Type]       INT            NOT NULL,
    [Category]   INT            NOT NULL,
    [Stem]       NVARCHAR (100) NOT NULL,
    [NoiseSkill] INT            NOT NULL,
    [Cluster]    INT            NOT NULL
);

