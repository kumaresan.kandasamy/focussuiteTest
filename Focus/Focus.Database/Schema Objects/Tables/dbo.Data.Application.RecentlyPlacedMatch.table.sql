﻿CREATE TABLE [dbo].[Data.Application.RecentlyPlacedMatch] (
    [Id]               BIGINT NOT NULL,
    [Score]            INT    NOT NULL,
    [PersonId]         BIGINT NOT NULL,
    [RecentlyPlacedId] BIGINT NOT NULL
);

