﻿CREATE TABLE [dbo].[Data.Application.Office] (
    [Id]                      BIGINT         NOT NULL,
    [OfficeName]              NVARCHAR (40)  NOT NULL,
    [Line1]                   NVARCHAR (200) NULL,
    [Line2]                   NVARCHAR (200) NULL,
    [TownCity]                NVARCHAR (100) NULL,
    [CountyId]                BIGINT         NULL,
    [StateId]                 BIGINT         NOT NULL,
    [CountryId]               BIGINT         NULL,
    [PostcodeZip]             NVARCHAR (20)  NOT NULL,
    [AssignedPostcodeZip]     NVARCHAR (MAX) NULL,
    [UnassignedDefault]       BIT            NULL,
    [OfficeManagerMailbox]    NVARCHAR (200) NULL,
    [ExternalId]              NVARCHAR (36)  NULL,
    [InActive]                BIT            NOT NULL,
    [BusinessOutreachMailbox] NVARCHAR (200) NULL,
    [DefaultType]             INT            NOT NULL,
    [DefaultAdministratorId]  BIGINT         NULL
);

