﻿CREATE TABLE [dbo].[Library.ROnet] (
    [Id]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [Code]   NVARCHAR (10)  NOT NULL,
    [Key]    NVARCHAR (200) NOT NULL,
    [OnetId] BIGINT         NULL
);

