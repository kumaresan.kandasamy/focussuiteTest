﻿CREATE TABLE [dbo].[Library.LocalisationItem] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]            NVARCHAR (200) NOT NULL,
    [Value]          NVARCHAR (MAX) NOT NULL,
    [LocalisationId] BIGINT         NOT NULL
);

