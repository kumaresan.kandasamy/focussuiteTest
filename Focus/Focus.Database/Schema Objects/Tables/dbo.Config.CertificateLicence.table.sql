﻿CREATE TABLE [dbo].[Config.CertificateLicence] (
    [Id]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]                    NVARCHAR (MAX) NULL,
    [CertificateLicenseType] INT            NOT NULL,
    [OnetParentCode]         NVARCHAR (40)  NOT NULL,
    [IsCertificate]          NVARCHAR (MAX) NOT NULL,
    [IsLicence]              NVARCHAR (MAX) NOT NULL
);

