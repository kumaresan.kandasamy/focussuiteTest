﻿CREATE TABLE [dbo].[Data.Application.UserActionTypeActivity] (
    [Id]                 BIGINT         NOT NULL,
    [ActionTypeId]       INT            NOT NULL,
    [UserId]             BIGINT         NOT NULL,
    [LastActivityDate]   DATETIME       NOT NULL,
    [DeletedOn]          DATETIME       NULL,
    [ActionType]         NVARCHAR (200) NULL,
    [ExternalActivityId] NVARCHAR (200) NULL,
    [ActionerId]         BIGINT         NULL
);



