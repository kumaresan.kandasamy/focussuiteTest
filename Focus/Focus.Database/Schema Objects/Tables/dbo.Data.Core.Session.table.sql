﻿CREATE TABLE [dbo].[Data.Core.Session] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [UserId]        BIGINT           NOT NULL,
    [LastRequestOn] DATETIME         NOT NULL
);

