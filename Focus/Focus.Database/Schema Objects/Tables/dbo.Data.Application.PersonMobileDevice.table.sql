﻿CREATE TABLE [dbo].[Data.Application.PersonMobileDevice] (
    [Id]         BIGINT         NOT NULL,
    [Token]      NVARCHAR (MAX) NOT NULL,
    [ApiKey]     NVARCHAR (MAX) NOT NULL,
    [DeviceType] INT            NOT NULL,
    [CreatedOn]  DATETIME       NOT NULL,
    [DeletedOn]  DATETIME       NULL,
    [PersonId]   BIGINT         NOT NULL
);

