﻿CREATE TABLE [dbo].[Library.SOC] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [SOC2010Code] NVARCHAR (10)  NULL,
    [Key]         NVARCHAR (200) NULL,
    [Title]       NVARCHAR (255) NULL
);

