﻿CREATE TABLE [dbo].[Library.InternshipEmployer] (
    [Id]                   BIGINT IDENTITY (1, 1) NOT NULL,
    [Frequency]            INT    NOT NULL,
    [EmployerId]           BIGINT NOT NULL,
    [StateAreaId]          BIGINT NULL,
    [InternshipCategoryId] BIGINT NOT NULL
);

