﻿CREATE TABLE [dbo].[Report.JobSeekerActivityAssignment] (
    [Id]               BIGINT         NOT NULL,
    [AssignDate]       DATETIME       NOT NULL,
    [AssignmentsMade]  INT            NOT NULL,
    [Activity]         NVARCHAR (400) NOT NULL,
    [ActivityCategory] NVARCHAR (400) NOT NULL,
    [JobSeekerId]      BIGINT         NOT NULL
);

