﻿CREATE TABLE [dbo].[Library.EmployerSkill] (
    [Id]         BIGINT IDENTITY (1, 1) NOT NULL,
    [Postings]   INT    NOT NULL,
    [EmployerId] BIGINT NOT NULL,
    [SkillId]    BIGINT NOT NULL
);

