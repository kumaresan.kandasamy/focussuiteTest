﻿CREATE TABLE [dbo].[Config.LocalisationItem] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [ContextKey]     NVARCHAR (50)  NOT NULL,
    [Key]            NVARCHAR (200) NOT NULL,
    [Value]          NVARCHAR (MAX) NOT NULL,
    [Localised]      BIT            NOT NULL,
    [LocalisationId] BIGINT         NOT NULL
);

