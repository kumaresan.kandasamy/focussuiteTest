﻿CREATE TABLE [dbo].[Library.InternshipJobTitle] (
    [Id]                   BIGINT         IDENTITY (1, 1) NOT NULL,
    [JobTitle]             NVARCHAR (255) NOT NULL,
    [Frequency]            INT            NOT NULL,
    [InternshipCategoryId] BIGINT         NOT NULL
);

