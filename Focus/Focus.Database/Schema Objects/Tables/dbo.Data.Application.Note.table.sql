﻿CREATE TABLE [dbo].[Data.Application.Note] (
    [Id]    BIGINT          NOT NULL,
    [Title] NVARCHAR (200)  NOT NULL,
    [Body]  NVARCHAR (1000) NOT NULL
);

