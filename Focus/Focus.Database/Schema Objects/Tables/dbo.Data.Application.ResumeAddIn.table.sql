﻿CREATE TABLE [dbo].[Data.Application.ResumeAddIn] (
    [Id]                BIGINT         NOT NULL,
    [ResumeId]          BIGINT         NOT NULL,
    [ResumeSectionType] INT            NOT NULL,
    [Description]       NVARCHAR (MAX) NULL
);

