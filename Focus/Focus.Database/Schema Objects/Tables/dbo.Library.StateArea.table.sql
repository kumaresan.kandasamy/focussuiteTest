﻿CREATE TABLE [dbo].[Library.StateArea] (
    [Id]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [AreaCode]  NVARCHAR (MAX) NOT NULL,
    [Display]   BIT            NOT NULL,
    [IsDefault] BIT            NOT NULL,
    [StateId]   BIGINT         NOT NULL,
    [AreaId]    BIGINT         NOT NULL
);

