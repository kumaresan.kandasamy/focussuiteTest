﻿CREATE TABLE [dbo].[Library.JobExperienceLevel] (
    [Id]                   BIGINT           IDENTITY (1, 1) NOT NULL,
    [ExperienceLevel]      INT              NOT NULL,
    [ExperiencePercentile] DECIMAL (24, 18) NOT NULL,
    [JobId]                BIGINT           NOT NULL
);

