﻿CREATE TABLE [dbo].[Library.InternshipCategory] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (100) NOT NULL,
    [LocalisationId] BIGINT         NOT NULL,
    [LensFilterId]   INT            NOT NULL
);

