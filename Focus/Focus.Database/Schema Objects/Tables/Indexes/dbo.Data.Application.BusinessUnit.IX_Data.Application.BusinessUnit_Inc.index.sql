﻿CREATE NONCLUSTERED INDEX [IX_Data.Application.BusinessUnit_Inc]
    ON [dbo].[Data.Application.BusinessUnit]([Id] ASC)
    INCLUDE([AccountTypeId], [PrimaryPhone], [PrimaryPhoneType], [AlternatePhone1], [AlternatePhone1Type], [AlternatePhone2], [AlternatePhone2Type], [AwaitingApprovalDate], [IndustrialClassification], [LegalName], [LockVersion], [Name], [OwnershipTypeId], [RedProfanityWords], [YellowProfanityWords], [Url]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

