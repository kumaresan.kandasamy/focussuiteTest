﻿CREATE NONCLUSTERED INDEX [IX_Data.Migration.UserRequest_RecordType_SecondaryExternalId]
    ON [dbo].[Data.Migration.UserRequest]([RecordType] ASC, [SecondaryExternalId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

