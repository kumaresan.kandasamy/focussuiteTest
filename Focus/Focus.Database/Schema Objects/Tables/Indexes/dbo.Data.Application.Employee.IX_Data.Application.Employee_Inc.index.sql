﻿CREATE NONCLUSTERED INDEX [IX_Data.Application.Employee_Inc]
    ON [dbo].[Data.Application.Employee]([Id] ASC)
    INCLUDE([RedProfanityWords], [YellowProfanityWords], [LockVersion], [ApprovalStatusChangedBy], [ApprovalStatusChangedOn], [PersonId], [AwaitingApprovalDate]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

