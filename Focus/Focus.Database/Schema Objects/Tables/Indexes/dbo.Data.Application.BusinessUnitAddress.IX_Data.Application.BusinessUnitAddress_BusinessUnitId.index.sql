﻿CREATE NONCLUSTERED INDEX [IX_Data.Application.BusinessUnitAddress_BusinessUnitId]
    ON [dbo].[Data.Application.BusinessUnitAddress]([BusinessUnitId] ASC)
    INCLUDE([Line1], [Line2], [TownCity], [PostcodeZip], [StateId], [PublicTransitAccessible], [IsPrimary]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];



