﻿CREATE NONCLUSTERED INDEX [IX_Messaging.Message_Version_MessageTypeId_EntityName_EntityKey_Status]
    ON [dbo].[Messaging.Message]([Version] ASC, [MessageTypeId] ASC, [EntityName] ASC, [EntityKey] ASC, [Status] ASC) WITH (FILLFACTOR = 90, ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];

