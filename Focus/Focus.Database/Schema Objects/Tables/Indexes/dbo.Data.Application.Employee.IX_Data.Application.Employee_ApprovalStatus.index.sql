﻿CREATE NONCLUSTERED INDEX [IX_Data.Application.Employee_ApprovalStatus]
    ON [dbo].[Data.Application.Employee]([ApprovalStatus] ASC)
    INCLUDE([Id], [RedProfanityWords], [YellowProfanityWords], [ApprovalStatusChangedBy], [ApprovalStatusChangedOn], [AwaitingApprovalDate], [LockVersion], [EmployerId], [PersonId]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];



