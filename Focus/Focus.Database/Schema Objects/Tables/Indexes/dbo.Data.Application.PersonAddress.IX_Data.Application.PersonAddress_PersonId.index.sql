﻿CREATE NONCLUSTERED INDEX [IX_Data.Application.PersonAddress_PersonId]
    ON [dbo].[Data.Application.PersonAddress]([PersonId] ASC)
    INCLUDE([Line1], [Line2], [TownCity], [PostcodeZip], [StateId]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];



