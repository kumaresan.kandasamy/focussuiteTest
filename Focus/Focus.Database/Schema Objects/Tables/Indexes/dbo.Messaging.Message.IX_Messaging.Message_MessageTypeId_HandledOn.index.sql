﻿CREATE NONCLUSTERED INDEX [ix_Messaging.Message_MessageTypeId_HandledOn]
ON [dbo].[Messaging.Message] ([MessageTypeId],[HandledOn])
INCLUDE ([Id],[MessagePayloadId])
GO