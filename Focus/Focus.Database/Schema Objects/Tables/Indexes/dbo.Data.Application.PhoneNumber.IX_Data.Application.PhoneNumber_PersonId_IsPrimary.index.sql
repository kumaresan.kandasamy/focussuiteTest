﻿CREATE NONCLUSTERED INDEX [IX_Data.Application.PhoneNumber_PersonId_IsPrimary]
    ON [dbo].[Data.Application.PhoneNumber]([PersonId] ASC, [IsPrimary] ASC)
    INCLUDE([Number], [PhoneType]) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];



