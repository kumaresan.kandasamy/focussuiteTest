﻿CREATE NONCLUSTERED INDEX [IX_Data.Core.Integrationlog_Outcome_RequestedOn]
ON [dbo].[Data.Core.IntegrationLog] ([Outcome],[RequestedOn])
INCLUDE ([Id],[RequestId])
GO

