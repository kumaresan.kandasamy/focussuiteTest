﻿CREATE TABLE [dbo].[Library.CareerArea] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (200) NOT NULL,
    [LocalisationId] BIGINT         NOT NULL
);

