﻿CREATE TABLE [dbo].[Config.LaborInsightMapping]
(
	[Id]                 UNIQUEIDENTIFIER  NOT NULL,
	[LaborInsightId]     NVARCHAR (200)    NOT NULL,
	[LaborInsightValue]  NVARCHAR (200)    NOT NULL,
	[CodeGroupKey]       NVARCHAR (200)    NOT NULL,
	[CodeItemKey]        NVARCHAR (200)    NOT NULL
);

