﻿CREATE TABLE [dbo].[Library.OnetRing] (
    [Id]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (30) NOT NULL,
    [Weighting] INT           NOT NULL,
    [RingMax]   INT           NOT NULL
);

