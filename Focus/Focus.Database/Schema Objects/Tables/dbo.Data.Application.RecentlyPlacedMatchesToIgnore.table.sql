﻿CREATE TABLE [dbo].[Data.Application.RecentlyPlacedMatchesToIgnore] (
    [Id]               BIGINT   NOT NULL,
    [IgnoredOn]        DATETIME NOT NULL,
    [PersonId]         BIGINT   NOT NULL,
    [RecentlyPlacedId] BIGINT   NOT NULL,
    [UserId]           BIGINT   NOT NULL
);

