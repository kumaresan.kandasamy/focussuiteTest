﻿CREATE TABLE [dbo].[Data.Application.JobSeekerSurvey] (
    [Id]                   BIGINT   NOT NULL,
    [SatisfactionLevel]    INT      NOT NULL,
    [UnanticipatedMatches] BIT      NOT NULL,
    [WasInvitedDidApply]   BIT      NOT NULL,
    [PersonId]             BIGINT   NOT NULL,
    [CreatedOn]            DATETIME NOT NULL
);

