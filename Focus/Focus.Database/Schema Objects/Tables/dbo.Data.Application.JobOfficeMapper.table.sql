﻿CREATE TABLE [dbo].[Data.Application.JobOfficeMapper] (
    [Id]               BIGINT NOT NULL,
    [OfficeUnassigned] BIT    NULL,
    [JobId]            BIGINT NOT NULL,
    [OfficeId]         BIGINT NOT NULL
);

