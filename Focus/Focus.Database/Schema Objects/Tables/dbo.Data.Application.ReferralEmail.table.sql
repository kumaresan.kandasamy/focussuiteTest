﻿CREATE TABLE [dbo].[Data.Application.ReferralEmail] (
    [Id]             BIGINT         NOT NULL,
    [PersonId]       BIGINT         NOT NULL,
    [EmailText]      NVARCHAR (MAX) NOT NULL,
    [EmailSentOn]    DATETIME       NOT NULL,
    [EmailSentBy]    BIGINT         NOT NULL,
    [ApprovalStatus] INT            NULL,
    [ApplicationId]  BIGINT         NULL,
    [EmployeeId]     BIGINT         NULL,
    [JobId]          BIGINT         NULL
);



