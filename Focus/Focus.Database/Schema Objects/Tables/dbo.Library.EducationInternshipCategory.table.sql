﻿CREATE TABLE [dbo].[Library.EducationInternshipCategory] (
    [Id]                    BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (100) NOT NULL,
    [LocalisationId]        BIGINT         NOT NULL,
    [CategoryType]          INT            NOT NULL,
    [PastTenseStatement]    NVARCHAR (200) NOT NULL,
    [PresentTenseStatement] NVARCHAR (200) NOT NULL
);

