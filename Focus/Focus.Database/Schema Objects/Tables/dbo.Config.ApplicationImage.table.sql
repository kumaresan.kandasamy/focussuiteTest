﻿CREATE TABLE [dbo].[Config.ApplicationImage] (
    [Id]    BIGINT          IDENTITY (1, 1) NOT NULL,
    [Type]  INT             NOT NULL,
    [Image] VARBINARY (MAX) NOT NULL
);

