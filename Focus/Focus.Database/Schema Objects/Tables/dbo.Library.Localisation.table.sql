﻿CREATE TABLE [dbo].[Library.Localisation] (
    [Id]      BIGINT       IDENTITY (1, 1) NOT NULL,
    [Culture] NVARCHAR (5) NOT NULL
);

