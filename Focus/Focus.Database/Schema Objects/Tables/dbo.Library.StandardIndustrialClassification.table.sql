﻿CREATE TABLE [dbo].[Library.StandardIndustrialClassification] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (200) NOT NULL,
    [SicPrimary]     NVARCHAR (4)   NOT NULL,
    [SicSecondary]   NVARCHAR (4)   NOT NULL,
    [NaicsPrimary]   NVARCHAR (20)  NOT NULL,
    [NaicsSecondary] NVARCHAR (20)  NOT NULL
);

