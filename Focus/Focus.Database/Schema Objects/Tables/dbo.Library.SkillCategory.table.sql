﻿CREATE TABLE [dbo].[Library.SkillCategory] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (255) NULL,
    [LocalisationId] BIGINT         NOT NULL,
    [ParentId]       BIGINT         NULL
);

