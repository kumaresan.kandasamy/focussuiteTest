﻿CREATE TABLE [dbo].[Data.Application.EmployeeEmail] (
    [Id]             BIGINT         NOT NULL,
    [EmployeeId]     BIGINT         NOT NULL,
    [EmailText]      NVARCHAR (MAX) NOT NULL,
    [EmailSentOn]    DATETIME       NOT NULL,
    [EmailSentBy]    BIGINT         NOT NULL,
    [ApprovalStatus] INT            NULL
);

