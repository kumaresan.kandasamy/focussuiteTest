﻿CREATE TABLE [dbo].[Data.Application.FailedIntegrationMessage] (
    [Id]               BIGINT          NOT NULL,
    [DateSubmitted]    DATETIME        NOT NULL,
    [MessageType]      VARCHAR (127)   NOT NULL,
    [ErrorDescription] NVARCHAR (2047) NOT NULL,
    [Request]          NVARCHAR (MAX)  NOT NULL,
    [IsIgnored]        BIT             NOT NULL,
    [DateResent]       DATETIME        NULL,
    IntegrationPoint   INT             NULL
);

