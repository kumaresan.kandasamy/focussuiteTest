﻿CREATE TABLE [dbo].[Library.SkillCategorySkill] (
    [Id]              BIGINT IDENTITY (1, 1) NOT NULL,
    [SkillCategoryId] BIGINT NOT NULL,
    [SkillId]         BIGINT NOT NULL
);

