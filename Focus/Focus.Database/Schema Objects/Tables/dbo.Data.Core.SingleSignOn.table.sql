﻿CREATE TABLE [dbo].[Data.Core.SingleSignOn] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [UserId]           BIGINT           NOT NULL,
    [ActionerId]       BIGINT           NOT NULL,
    [CreatedOn]        DATETIME         NOT NULL,
    [ExternalAdminId]  VARCHAR (20)     NULL,
    [ExternalOfficeId] VARCHAR (20)     NULL,
    [ExternalPassword] VARCHAR (20)     NULL,
    [ExternalUsername] VARCHAR (20)     NULL
);

