﻿CREATE TABLE [dbo].[Data.Application.UserSecurityQuestion]
(
	[Id]                      BIGINT         NOT NULL,
	[UserId]                  BIGINT         NOT NULL,
	[QuestionIndex]           INT            NOT NULL,
	[SecurityQuestion]        NVARCHAR (100) NULL,
	[SecurityAnswerHash]      NVARCHAR (86)  NULL,
	[SecurityAnswerEncrypted] NVARCHAR (250) NULL,
	[SecurityQuestionId]      BIGINT         NULL
);


