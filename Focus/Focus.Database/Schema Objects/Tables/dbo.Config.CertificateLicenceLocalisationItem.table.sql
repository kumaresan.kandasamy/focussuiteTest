﻿CREATE TABLE [dbo].[Config.CertificateLicenceLocalisationItem] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [Key]            NVARCHAR (200) NOT NULL,
    [Value]          NVARCHAR (500) NOT NULL,
    [LocalisationId] BIGINT         NOT NULL
);

