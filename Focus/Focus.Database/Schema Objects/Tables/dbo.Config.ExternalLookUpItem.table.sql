﻿CREATE TABLE [dbo].[Config.ExternalLookUpItem] (
    [Id]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [InternalId]         NVARCHAR (50)  NULL,
    [ExternalId]         NVARCHAR (150) NULL,
    [ExternalLookUpType] INT            NOT NULL,
    [ClientDataTag]      NVARCHAR (50)  NOT NULL,
    [FocusId]            BIGINT         NOT NULL
);

