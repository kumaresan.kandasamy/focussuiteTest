﻿CREATE TABLE [dbo].[Data.Application.ViewedPosting] (
    [Id]            BIGINT         NOT NULL,
    [ViewedOn]      DATETIME       NOT NULL,
    [LensPostingId] NVARCHAR (MAX) NOT NULL,
    [UserId]        BIGINT         NOT NULL
);

