﻿CREATE TABLE [dbo].[Data.Application.UserValidationKeyLog] (
    [Id]            BIGINT        NOT NULL,
    [UserId]        BIGINT        NULL,
    [ValidationKey] NVARCHAR (86) NOT NULL,
    [LogDate]       DATETIME      NOT NULL,
    [Status]        INT           NOT NULL
);

