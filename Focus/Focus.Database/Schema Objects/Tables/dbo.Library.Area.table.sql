﻿CREATE TABLE [dbo].[Library.Area] (
    [Id]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (50) NOT NULL,
    [SortOrder] INT           NOT NULL
);

