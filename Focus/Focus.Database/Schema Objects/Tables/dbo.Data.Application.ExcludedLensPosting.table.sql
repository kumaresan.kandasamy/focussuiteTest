﻿CREATE TABLE [dbo].[Data.Application.ExcludedLensPosting] (
    [Id]            BIGINT         NOT NULL,
    [LensPostingId] NVARCHAR (MAX) NOT NULL,
    [CreatedOn]     DATETIME       NOT NULL,
    [UpdatedOn]     DATETIME       NOT NULL,
    [PersonId]      BIGINT         NOT NULL
);

