﻿CREATE TABLE [dbo].[Config.CodeGroupItem] (
    [Id]           BIGINT IDENTITY (1, 1) NOT NULL,
    [DisplayOrder] INT    NOT NULL,
    [CodeGroupId]  BIGINT NOT NULL,
    [CodeItemId]   BIGINT NOT NULL
);

