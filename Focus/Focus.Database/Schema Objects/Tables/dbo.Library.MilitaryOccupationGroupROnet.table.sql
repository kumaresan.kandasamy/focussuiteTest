﻿CREATE TABLE [dbo].[Library.MilitaryOccupationGroupROnet] (
    [Id]                        BIGINT IDENTITY (1, 1) NOT NULL,
    [ROnetId]                   BIGINT NOT NULL,
    [MilitaryOccupationGroupId] BIGINT NOT NULL
);

