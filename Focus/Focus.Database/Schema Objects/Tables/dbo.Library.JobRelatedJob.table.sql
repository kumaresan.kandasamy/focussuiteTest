﻿CREATE TABLE [dbo].[Library.JobRelatedJob] (
    [Id]           BIGINT IDENTITY (1, 1) NOT NULL,
    [RelatedJobId] BIGINT NOT NULL,
    [Priority]     INT    NOT NULL,
    [JobId]        BIGINT NOT NULL
);

