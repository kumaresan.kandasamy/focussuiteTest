﻿CREATE TABLE [dbo].[Library.JobTask] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [JobTaskType] INT            NOT NULL,
    [Key]         NVARCHAR (200) NOT NULL,
    [Scope]       INT            NOT NULL,
    [Certificate] BIT            NOT NULL,
    [OnetId]      BIGINT         NOT NULL
);

