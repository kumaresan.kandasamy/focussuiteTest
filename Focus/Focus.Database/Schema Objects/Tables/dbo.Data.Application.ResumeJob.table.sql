﻿CREATE TABLE [dbo].[Data.Application.ResumeJob] (
    [Id]        BIGINT           NOT NULL,
    [JobTitle]  NVARCHAR (255)   NULL,
    [Employer]  NVARCHAR (255)   NULL,
    [StartYear] NVARCHAR (50)    NULL,
    [EndYear]   NVARCHAR (50)    NULL,
    [LensId]    UNIQUEIDENTIFIER NULL,
    [ResumeId]  BIGINT           NOT NULL
);

