﻿CREATE TABLE [dbo].[Library.GenericJobTitle] (
    [Id]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [Value] NVARCHAR (MAX) NOT NULL
);

