﻿CREATE TABLE [dbo].[Data.Application.JobProgramOfStudy] (
    [Id]                     BIGINT         NOT NULL,
    [DegreeEducationLevelId] BIGINT         NOT NULL,
    [ProgramOfStudy]         NVARCHAR (MAX) NOT NULL,
    [JobId]                  BIGINT         NOT NULL,
    [Tier]                   INT            NULL
);

