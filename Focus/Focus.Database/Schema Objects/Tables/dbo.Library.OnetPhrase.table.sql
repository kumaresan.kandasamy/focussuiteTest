﻿CREATE TABLE [dbo].[Library.OnetPhrase] (
    [Id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [Phrase]     NVARCHAR (500) NOT NULL,
    [Equivalent] INT            NULL,
    [OnetSoc]    NVARCHAR (10)  NOT NULL,
    [OnetId]     BIGINT         NOT NULL
);

