﻿CREATE TABLE [dbo].[Messaging.Message] (
    [Id]                    UNIQUEIDENTIFIER NOT NULL,
    [Status]                INT              NOT NULL,
    [Priority]              INT              NOT NULL,
    [EnqueuedOn]            DATETIME         NOT NULL,
    [DequeuedOn]            DATETIME         NULL,
    [HandledOn]             DATETIME         NULL,
    [ExecutionMilliseconds] BIGINT           NOT NULL,
    [Failed]                BIT              NOT NULL,
    [MessageTypeId]         UNIQUEIDENTIFIER NOT NULL,
    [MessagePayloadId]      UNIQUEIDENTIFIER NOT NULL,
    [Version]               NVARCHAR (10)    NULL,
    [ProcessOn]             DATETIME         NULL,
    [EntityName]            NVARCHAR (200)   NULL,
    [EntityKey]             BIGINT           NULL
);

