﻿CREATE TABLE [dbo].[Data.Core.ActionType] (
    [Id]           BIGINT         NOT NULL,
    [Name]         NVARCHAR (200) NOT NULL,
    [ReportOn]     BIT            NOT NULL,
    [AssistAction] BIT            NULL
);

