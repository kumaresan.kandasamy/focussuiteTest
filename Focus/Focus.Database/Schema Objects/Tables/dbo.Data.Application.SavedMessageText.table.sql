﻿CREATE TABLE [dbo].[Data.Application.SavedMessageText] (
    [Id]             BIGINT         NOT NULL,
    [LocalisationId] BIGINT         NOT NULL,
    [Text]           NVARCHAR (MAX) NOT NULL,
    [SavedMessageId] BIGINT         NOT NULL
);

