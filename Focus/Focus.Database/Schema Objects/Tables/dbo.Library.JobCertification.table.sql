﻿CREATE TABLE [dbo].[Library.JobCertification] (
    [Id]               BIGINT           IDENTITY (1, 1) NOT NULL,
    [DemandPercentile] DECIMAL (24, 18) NOT NULL,
    [JobId]            BIGINT           NOT NULL,
    [CertificationId]  BIGINT           NOT NULL
);

