﻿CREATE TABLE [dbo].[Library.JobCareerPathTo] (
    [Id]           BIGINT IDENTITY (1, 1) NOT NULL,
    [Rank]         INT    NOT NULL,
    [CurrentJobId] BIGINT NOT NULL,
    [Next1JobId]   BIGINT NOT NULL,
    [Next2JobId]   BIGINT NULL
);

