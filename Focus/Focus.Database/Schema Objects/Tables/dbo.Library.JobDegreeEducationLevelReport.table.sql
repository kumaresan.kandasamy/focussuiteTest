﻿CREATE TABLE [dbo].[Library.JobDegreeEducationLevelReport] (
    [Id]                        BIGINT IDENTITY (1, 1) NOT NULL,
    [HiringDemand]              INT    NOT NULL,
    [DegreesAwarded]            INT    NOT NULL,
    [StateAreaId]               BIGINT NOT NULL,
    [JobDegreeEducationLevelId] BIGINT NOT NULL
);

