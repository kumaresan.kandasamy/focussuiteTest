﻿CREATE TABLE [dbo].[Data.Application.NoteReminderRecipient] (
    [Id]                  BIGINT NOT NULL,
    [RecipientEntityType] INT    NOT NULL,
    [RecipientEntityId]   BIGINT NOT NULL,
    [NoteReminderId]      BIGINT NOT NULL
);

