﻿CREATE TABLE [dbo].[Data.Application.PersonOfficeMapper] (
    [Id]               BIGINT   NOT NULL,
    [StateId]          BIGINT   NULL,
    [PersonId]         BIGINT   NOT NULL,
    [OfficeId]         BIGINT   NULL,
    [OfficeUnassigned] BIT      NULL,
    [CreatedOn]        DATETIME NULL
);

