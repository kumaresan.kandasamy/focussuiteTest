﻿CREATE TABLE [dbo].[Data.Migration.BatchRecord] (
    [Id]          BIGINT NOT NULL,
    [EntityType]  INT    NOT NULL,
    [RecordType]  INT    NOT NULL,
    [BatchNumber] INT    NOT NULL,
    [FirstId]     BIGINT NOT NULL,
    [LastId]      BIGINT NOT NULL
);

