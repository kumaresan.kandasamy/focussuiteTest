﻿CREATE TABLE [dbo].[Data.Application.CandidateSearchHistory] (
    [Id]        BIGINT   NOT NULL,
    [JobId]     BIGINT   NOT NULL,
    [CreatedBy] BIGINT   NOT NULL,
    [CreatedOn] DATETIME NOT NULL
);

