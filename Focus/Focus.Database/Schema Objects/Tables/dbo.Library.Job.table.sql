﻿CREATE TABLE [dbo].[Library.Job] (
    [Id]                   BIGINT          IDENTITY (1, 1) NOT NULL,
    [Name]                 NVARCHAR (200)  NOT NULL,
    [ROnet]                NVARCHAR (20)   NOT NULL,
    [LocalisationId]       BIGINT          NOT NULL,
    [DegreeIntroStatement] NVARCHAR (1000) NULL,
    [Description]          NVARCHAR (MAX)  NULL,
    [StarterJob]           INT             NULL
);

