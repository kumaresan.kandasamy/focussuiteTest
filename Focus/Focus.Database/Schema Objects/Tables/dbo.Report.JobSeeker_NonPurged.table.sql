﻿CREATE TABLE [dbo].[Report.JobSeeker_NonPurged] 
(
	[Id]								BIGINT NOT NULL,
	[JobSeekerReportId] BIGINT NOT NULL DEFAULT 0,
	[DateOfBirth]				DATETIME NULL,
	[Gender]						INT NULL,
	[DisabilityStatus]	INT NULL,
	[DisabilityType]		INT NULL,
	[EthnicHeritage]		INT NULL,
	[Race]							INT NULL,
	[MinimumEducationLevel] INT NULL,
	[EnrollmentStatus]	INT NULL,
	[EmploymentStatus]	INT NULL,
	[Age]								SMALLINT NULL,
	[CreatedOn]					DATETIME NOT NULL,
	[UpdatedOn]					DATETIME NOT NULL,
	[IsCitizen]					BIT NULL,
	[AlienCardExpires]	DATETIME NULL,
	[AlienCardId]				NVARCHAR(MAX) NULL,
	[CountyId]					BIGINT NULL
);

