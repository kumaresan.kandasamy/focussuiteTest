﻿CREATE TABLE [dbo].[Library.OnetSOC] (
    [Id]     BIGINT IDENTITY (1, 1) NOT NULL,
    [OnetId] BIGINT NOT NULL,
    [SOCId]  BIGINT NOT NULL
);

