﻿CREATE TABLE [dbo].[Config.Activity] (
    [Id]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [LocalisationKey]    NVARCHAR (MAX) NOT NULL,
    [Name]               NVARCHAR (MAX) NOT NULL,
    [Visible]            BIT            NOT NULL,
    [Administrable]      BIT            NOT NULL,
    [SortOrder]          INT            NOT NULL,
    [Used]               BIT            NOT NULL,
    [ExternalId]         BIGINT         NOT NULL,
    [ActivityCategoryId] BIGINT         NOT NULL
);

