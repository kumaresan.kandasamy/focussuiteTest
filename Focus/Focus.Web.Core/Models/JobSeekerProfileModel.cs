﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.Web.Core.Models
{
  [Serializable]
  public class JobSeekerProfileModel
  {
		public JobSeekerView JobSeekerView { get; set; }
    public JobSeekerProfileViewDto JobSeekerProfileView { get; set; }

		// Profile
		public string UnemploymentInfo { get; set; }
        public bool HomelessWithoutShelter { get; set; }
        public bool HomelessWithShelter { get; set; }
        public bool RunawayYouth { get; set; }
        public bool ExOffender { get; set; }
		public VeteranInfo VeteranInfo { get; set; }
		public DisabilityStatus DisabilityStatus { get; set; }
        public List<long?> DisabilityCategoryIds { get; set; }
        public long? DisabilityCategoryId { get; set; }

    public bool? IsMSFW { get; set; }
    public bool? MSFWVerified { get; set; }
    public List<long> MSFWQuestions { get; set; }
    public bool LowLevelLiteracy { get; set; }
    public string PreferredLanguage { get; set; }
    public bool CulturalBarriers { get; set; }
    public string CommonLanguage { get; set; }
    public string NativeLanguage { get; set; }
    public string NoOfDependents { get; set; }
    public string EstMonthlyIncome { get; set; }
    public bool DisplacedHomemaker { get; set; }
    public bool SingleParent { get; set; }
    public bool LowIncomeStatus { get; set; }

		// Resume
    public long? ResumeId { get; set; }
    public int ResumeLength { get; set; }
		public int SkillsCodedFromResume { get; set; }
		public int NumberOfResumes { get; set; }
		public bool? ResumeIsSearchable { get; set; }
		public bool? WillingToWorkOvertime { get; set; }
		public bool? WillingToRelocate { get; set; }
		public int NumberOfIncompleteResumes { get; set; }
    public SchoolStatus? EnrollmentStatus { get; set; }
    public EducationLevel? EducationLevel { get; set; }
    public EmploymentStatus? EmploymentStatus { get; set; }

		// Referral outcomes panel
		public int ApplicantsInterviewed { get; set; }
		public int ApplicantsFailedToShow { get; set; }
		public int ApplicantsInterviewDenied { get; set; }
		public int ApplicantsHired { get; set; }
		public int ApplicantsRejected { get; set; }
		public int ApplicantsRefused { get; set; }
		public int ApplicantsDidNotApply { get; set; }
    public int ApplicantsFailedToRespondToInvitation { get; set; }
    public int ApplicantsFailedToReportToJob { get; set; }
    public int ApplicantsFoundJobFromOtherSource { get; set; }
		public int ApplicantsJobAlreadyFilled { get; set; }
		public int ApplicantsNewApplicant { get; set; }
		public int ApplicantsNotQualified { get; set; }
		public int ApplicantsNotYetPlaced { get; set; }
		public int ApplicantsRecommended { get; set; }
		public int ApplicantsRefusedReferral { get; set; }
		public int ApplicantsUnderConsideration { get; set; }

		// Survey responses panel
		public int MatchQualityDissatisfied { get; set; }
		public int MatchQualitySomewhatDissatisfied { get; set; }
		public int MatchQualitySomewhatSatisfied { get; set; }
		public int MatchQualitySatisfied { get; set; }
		public int NotReceiveUnexpectedMatches { get; set; }
		public int ReceivedUnexpectedResults { get; set; }
		public int NotEmployerInvitationsReceive { get; set; }
		public int ReceivedEmployerInvitations { get; set; }

		public List<long?> OfficeIds { get; set; }
    public Phone PrimaryPhone { get; set; }
  }
}
