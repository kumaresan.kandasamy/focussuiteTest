﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

#endregion

namespace Focus.Web.Core.Models
{
	public class EmailValidationModel
	{
		public List<string> ValidEmail { get; set; }
		public List<string> InvalidEmailFormat { get; set; }
    public List<string> MultipleEmailError { get; set; }
    public List<string> DuplicateEmailError { get; set; }
		public List<string> UsernameInUse { get; set; } 
  }
}
