﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.Core.Models
{
  [Serializable]
  public class HiringManagerProfileModel
  {
		public long EmployeeId { get; set; }
		public long EmployerId { get; set; }
		public long BusinessUnitId { get; set; }
		public string EmployeeName { get; set; }
		public string EmployeeEmail { get; set; }
		public List<PhoneNumberDto> EmployeePhoneNumber { get; set; }
		public string BusinessUnitName { get; set; }
    public BusinessUnitDto BusinessUnit { get; set; }
    public List<BusinessUnitAddressDto> BusinessUnitAddresses { get; set; }
    public int DaysSinceRegistration { get; set; }
		public int LoginsSinceRegistration { get; set; }
		public int Logins7Days { get; set; }
		public DateTime? LastLogin { get; set; }
		public int ApplicantsInterviewed { get; set; }
		public int ApplicantsFailedToShow { get; set; }
		public int ApplicantsInterviewDenied { get; set; }
		public int ApplicantsHired { get; set; }
		public int ApplicantsRejected { get; set; }
		public int ApplicantsRefused { get; set; }
		public int ApplicantsDidNotApply { get; set; }
		public int ApplicantsFailedToRespondToInvitation { get; set; }
		public int ApplicantsFoundJobFromOtherSource { get; set; }
		public int ApplicantsJobAlreadyFilled { get; set; }
		public int ApplicantsNewApplicant { get; set; }
		public int ApplicantsNotQualified { get; set; }
		public int ApplicantsNotYetPlaced { get; set; }
		public int ApplicantsRecommended { get; set; }
		public int ApplicantsRefusedReferral { get; set; }
		public int ApplicantsUnderConsideration { get; set; }
		public int SurveyVeryDissatified { get; set; }
	  public int SurveyVerySatisfied { get; set; }
		public int SurveySomewhatDissatified { get; set; }
		public int SurveySomewhatSatisfied { get; set; }
		public int SurveyDidNotInterview { get; set; }
	  public int SurveyInterviewed { get; set; }
	  public int SurveyDidNotHire { get; set; }
		public int SurveyHired { get; set; }
		public int ApplicantsSeven { get; set; }
		public int ApplicantsThirty { get; set; }
		public int MatchesSeven { get; set; }
		public int MatchesThirty { get; set; }
		public int ThreeFiveStar { get; set; }
		public int ThreeFiveStarNotViewed { get; set; }
		public int InvitationsSentSeven { get; set; }
		public int InvitationsSentThirty { get; set; }
		public int InvitedSeekersViewedJobSeven { get; set; }
		public int InvitedSeekersViewedJobThirty { get; set; }
		public int InvitedSeekersNotViewJobSeven { get; set; }
		public int InvitedSeekersNotViewJobThirty { get; set; }
		public int InvitedSeekersClickedHowToApplySeven { get; set; }
		public int InvitedSeekersClickedHowToApplyThirty { get; set; }
		public int InvitedSeekersNotClickHowToApplySeven { get; set; }
		public int InvitedSeekersNotClickHowToApplyThirty { get; set; }
  }
}
