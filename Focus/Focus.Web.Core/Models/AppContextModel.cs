﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core.Models;

#endregion

namespace Focus.Web.Core.Models
{
	public class AppContextModel
	{
		/// <summary>
		/// Gets or sets the client tag.
		/// </summary>
		/// <value>
		/// The client tag.
		/// </value>
		public string ClientTag { get; set; }

		/// <summary>
		/// Gets or sets the session identifier.
		/// </summary>
		/// <value>
		/// The session identifier.
		/// </value>
		public Guid SessionId { get; set; }

		/// <summary>
		/// Gets or sets the request identifier.
		/// </summary>
		/// <value>
		/// The request identifier.
		/// </value>
		public Guid RequestId { get; set; }

		/// <summary>
		/// Gets or sets the user.
		/// </summary>
		/// <value>
		/// The user.
		/// </value>
		public IUserContext User { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="AppContextModel"/> class.
		/// </summary>
		public AppContextModel()
		{
			User = new UserContext();
		}
	}
}
