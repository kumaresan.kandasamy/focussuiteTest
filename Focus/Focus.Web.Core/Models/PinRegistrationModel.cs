﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;

#endregion

namespace Focus.Web.Core.Models
{
	public class PinRegistrationModel
	{
		public string EmailAddress { get; set; }
		public string Pin { get; set; }
		public ErrorTypes RegistrationError { get; set; }
	}
}
