﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Web.Core.Models
{
	public class UserDataModel
	{
		public bool CareerUserDataCheck { get; set; }
		public long? DefaultResumeId { get; set; }
		public ResumeCompletionStatuses DefaultResumeCompletionStatus { get; set; }
		public UserProfile Profile { get; set; }
	}
}
