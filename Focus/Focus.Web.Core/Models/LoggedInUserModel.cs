﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core.Models;

#endregion

namespace Focus.Web.Core.Models
{
	public class LoggedInUserModel
	{
		public UserContext UserContext { get; set; }
		public UserDataModel UserData { get; set; }

    public Guid? ResetSessionId { get; set; }

		public bool AccountReactivated { get; set; }
	}
}
