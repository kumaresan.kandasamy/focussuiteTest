﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.Core.Models
{
	[Serializable]
	public class UserDetailsModel
	{
		public long UserId { get; set; }
		public string Password { get; set; }
		public string SecurityQuestion { get; set; }
		public long? SecurityQuestionId { get; set; }
		public string SecurityAnswer { get; set; }
		public UserDto UserDetails { get; set; }
		public PersonDto PersonDetails { get; set; }
		public PersonAddressDto AddressDetails { get; set; }
		public PhoneNumberDto PrimaryPhoneNumber { get; set; }
		public PhoneNumberDto AlternatePhoneNumber1 { get; set; }
		public PhoneNumberDto AlternatePhoneNumber2 { get; set; }
		public List<PersonOfficeMapperDto> PersonOffices { get; set; }
		public int? LockVersion { get; set; }
	}
}