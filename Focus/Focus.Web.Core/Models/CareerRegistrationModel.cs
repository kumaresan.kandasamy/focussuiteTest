﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.Web.Core.Models
{
  [Serializable]
	public class CareerRegistrationModel : RegistrationModel
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CareerRegistrationModel" /> class.
		/// </summary>
		public CareerRegistrationModel()
		{
			PrimaryPhone = new Phone();
			PostalAddress = new Address();
		}

		public DateTime? DateOfBirth { get; set; }
		public string EmailAddress { get; set; }
		public string SocialSecurityNumber { get; set; }

		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string ScreenName { get; set; }
    public long? SuffixId { get; set; }

		public Phone PrimaryPhone { get; set; }

		public Address PostalAddress { get; set; }

    public CareerAccountType? AccountType { get; set; }

		public string SecurityQuestion { get; set; }
		public long? SecurityQuestionId { get; set; }
		public string SecurityAnswer { get; set; }

		public long? ProgramAreaId { get; set; }
		public long? DegreeId { get; set; }
    public SchoolStatus? EnrollmentStatus { get; set; }

    public long? CampusId { get; set; }

    public string Pin { get; set; }
    public string PinRegisteredEmailAddress { get; set; }

    public long? OverrideOfficeId { get; set; }

    public string ExternalId { get; set; }
	}
}
