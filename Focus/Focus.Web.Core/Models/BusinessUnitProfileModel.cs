﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.Core.Models
{
	[Serializable]
	public class BusinessUnitProfileModel
	{
		public BusinessUnitDto BusinessUnit { get; set; }
    public List<BusinessUnitAddressDto> BusinessUnitAddresses { get; set; }
		public int EmployerLockVersion { get; set; }
		public int Logins7Days { get; set; }
		public DateTime? LastLogin { get; set; }
		public string LastLoginEmail { get; set; }
		public int NoOfJobOrdersOpen7Days { get; set; }
		public int NoOfOrdersEditedByStaff7Days { get; set; }
		public int NoOfResumesReferred7Days { get; set; }
		public int NoOfJobSeekersInterviewed7Days { get; set; }
		public int NoOfJobSeekersRejected7Days { get; set; }
		public int NoOfJobSeekersHired7Days { get; set; }
		public int NoOfJobOrdersOpen30Days { get; set; }
		public int NoOfOrdersEditedByStaff30Days { get; set; }
		public int NoOfResumesReferred30Days { get; set; }
		public int NoOfJobSeekersInterviewed30Days { get; set; }
		public int NoOfJobSeekersRejected30Days { get; set; }
		public int NoOfJobSeekersHired30Days { get; set; }
		public List<long> OfficeIds { get; set; }
		public bool IsPrimary { get; set; }
		public List<BusinessUnitLinkedCompanyDto> LinkedCompanies { get; set; }
	}
}
