﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.Core.Models
{
	[Serializable]
	public class StaffProfileModel
	{
		public PersonDto Person { get; set; }
    public List<PersonAddressDto> PersonAddresses { get; set; }
    public List<PhoneNumberDto> PhoneNumbers { get; set; }
    public int Logins7Days { get; set; }
		public DateTime? LastLogin { get; set; }
		public int ActivityReferrals7Days { get; set; }
		public int ActivityResumesCreated7Days { get; set; }
		public int ActivityResumesEdited7Days { get; set; }
		public int ActivityJobSeekersAssisted7Days { get; set; }
		public int ActivityJobSeekersUnsubscribed7Days { get; set; }
		public int ActivityEmployerAccountsCreated7Days { get; set; }
		public int ActivityEmployerAccountsAssisted7Days { get; set; }
		public int ActivityJobOrdersCreated7Days { get; set; }
		public int ActivityJobOrdersEdited7Days { get; set; }
		public int ActivityReferrals30Days { get; set; }
		public int ActivityResumesCreated30Days { get; set; }
		public int ActivityResumesEdited30Days { get; set; }
		public int ActivityJobSeekersAssisted30Days { get; set; }
		public int ActivityJobSeekersUnsubscribed30Days { get; set; }
		public int ActivityEmployerAccountsCreated30Days { get; set; }
		public int ActivityEmployerAccountsAssisted30Days { get; set; }
		public int ActivityJobOrdersCreated30Days { get; set; }
		public int ActivityJobOrdersEdited30Days { get; set; }
	}
}
