﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.Web.Core.Models
{
  public class EmployerDetailsModel
  {
    public long EmployerId { get; set; }
    public string Name { get; set; }
    public BusinessUnitDescriptionDto Description { get; set; }
    public string FEIN { get; set; }
    public string StateEmployerIdentificationNumber { get; set; }
    public string IndustrialClassification { get; set; }
		public long OwnershipTypeId { get; set; }
		public long? AccountTypeId { get; set; }
    public EmployerAddressDto PrimaryAddress { get; set; }
    public PhoneNumberDto PrimaryPhoneNumber { get; set; }
    public PhoneNumberDto AlternatePhoneNumber1 { get; set; }
    public PhoneNumberDto AlternatePhoneNumber2 { get; set; }
		public bool IsAlreadyApproved { get; set; }
		public int? LockVersion { get; set; }
  }

}