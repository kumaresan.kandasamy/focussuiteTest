﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core.Models;

#endregion

namespace Focus.Web.Core.Models
{
  [Serializable]
	public class ExplorerRegistrationModel : RegistrationModel
	{
		public string ScreenName { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public string EmailAddress { get; set; }
		public string SocialSecurityNumber { get; set; }

		public string FirstName { get; set; }
		public string MiddleInitial { get; set; }
		public string LastName { get; set; }

		public string PhoneNumber { get; set; }

		public string SecurityQuestion { get; set; }
		public long? SecurityQuestionId { get; set; }
		public string SecurityAnswer { get; set; }

		public ExplorerResumeModel ResumeModel { get; set; }

		public string ExternalId { get; set; }
	}
}
