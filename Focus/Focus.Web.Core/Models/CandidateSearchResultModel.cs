﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Core.Views;

#endregion

namespace Focus.Web.Core.Models
{
	public class CandidateSearchResultModel
	{
		/// <summary>
		/// Gets or sets the search id.
		/// </summary>
		/// <value>The search id.</value>
		public Guid SearchId { get; set; }

		/// <summary>
		/// Gets or sets the candidates.
		/// </summary>
		/// <value>The candidates.</value>
		public PagedList<ResumeView> Candidates { get; set; }

		/// <summary>
		/// Gets or sets the highest star rating.
		/// </summary>
		/// <value>
		/// The highest star rating.
		/// </value>
		public int HighestStarRating { get; set; }
	}
}