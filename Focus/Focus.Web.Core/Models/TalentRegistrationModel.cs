﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;

#endregion

namespace Focus.Web.Core.Models
{
	public class TalentRegistrationModel : RegistrationModel
	{
		// User needed for SSO registration
		public UserDto User { get; set; }
		public UserSecurityQuestionDto[] UserSecurityQuestions { get; set; }

		// Contact (Employee) Details
		public PersonDto EmployeePerson { get; set; }
		public PersonAddressDto EmployeeAddress { get; set; }

    public long EmployeeId { get; set; }
		public string EmployeePhone { get; set; }
    public string EmployeePhoneExtension { get; set; }
    public string EmployeePhoneType { get; set; }
		public string EmployeeAlternatePhone1 { get; set; }
    public string EmployeeAlternatePhone1Type { get; set; }
		public string EmployeeAlternatePhone2 { get; set; }
    public string EmployeeAlternatePhone2Type { get; set; }
		public string EmployeeEmailAddress { get; set; }
		public ApprovalStatuses EmployeeApprovalStatus { get; set; }

		// Corporate (Employer) Details
		public EmployerDto Employer { get; set; }
		public string EmployerExternalId { get; set; }
		public string StateEmployerIdentificationNumber { get; set; }
		public EmployerAddressDto EmployerAddress { get; set; }
		public BusinessUnitDto BusinessUnit { get; set; }
		public BusinessUnitDescriptionDto BusinessUnitDescription { get; set; }
    public BusinessUnitAddressDto BusinessUnitAddress { get; set; }

		public UserContext UserContext { get; set; }
	}
}