﻿using System;
using Focus.Installer.Code;
using NUnit.Framework;

namespace Focus.Installer.UnitTests
{
	[TestFixture]
	public class Tests
	{
		[Test]
		public void Test()
		{
			var args = new[] { "-config", "data\\configs\\test.xml", "-releases", "data\\releases.xml", "-log", "verbose"};
			var worker = new Worker(Log);

			worker.Process(args);
		}

		private static void Log(string s,bool fileOnly = false)
		{
			Console.WriteLine(s);
		}
	}
}
