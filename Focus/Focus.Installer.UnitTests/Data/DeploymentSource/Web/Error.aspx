﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="Focus.Web.Error" %>
<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server"></asp:Content>


<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<h2 runat="server" ID="Heading"><%= ErrorHeading %> <%= ErrorInfo %> </h2>
	
	<br />
	<br />
		 
	<%= ErrorStack %>
	<br />
	<br />

	<ul runat="server" ID="HomePageLink">
		<li>
			<a href="<%= UrlBuilder.Default(App.User, App) %>">Click here</a> to return to home page.
		</li>
	</ul>
  

	<%-- Confirmation Modal --%>
	<uc:ConfirmationModal ID="Confirmation" runat="server" OnCloseCommand="Redirect_Click" />

</asp:Content>
