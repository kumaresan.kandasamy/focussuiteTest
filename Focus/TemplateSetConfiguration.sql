﻿use Focus????


BEGIN TRAN

DECLARE @NumberOfConfigInserts BIGINT 
DECLARE @NextId BIGINT 
DECLARE @StartId BIGINT 

SET @NumberOfConfigInserts = 1

UPDATE KeyTable SET NextId = NextId + @NumberOfConfigInserts
SELECT @NextId = NextId FROM KeyTable

SET @StartId = @NextId - @NumberOfConfigInserts

INSERT INTO ConfigurationItem VALUES (@NextId + 1, 'ReportingEnabled', 'True')

UPDATE ConfigurationItem SET VALUE = '' WHERE [Key]= 'TalentApplicationPath'
UPDATE ConfigurationItem SET VALUE = '' WHERE [Key]= 'AssistApplicationPath'
UPDATE ConfigurationItem SET VALUE = 'Focus Talent' WHERE [Key]= 'FocusTalentApplicationName'
UPDATE ConfigurationItem SET VALUE = 'Focus Assist' WHERE [Key]= 'FocusCareerApplicationName'
UPDATE ConfigurationItem SET VALUE = '' WHERE [Key]= 'SupportEmail'
UPDATE ConfigurationItem SET VALUE = 'State.OK' WHERE [Key]= 'DefaultStateKey'
UPDATE ConfigurationItem SET VALUE = '' WHERE [Key]= 'FocusCareerUrl'
UPDATE ConfigurationItem SET VALUE = '<font color='red'><strong>Focus Talent/Assist</strong></font>' WHERE [Key]= 'AppVersion'
UPDATE ConfigurationItem SET VALUE = '' WHERE [Key]= 'FeedbackEmailAddress'


ROLLBACK 
