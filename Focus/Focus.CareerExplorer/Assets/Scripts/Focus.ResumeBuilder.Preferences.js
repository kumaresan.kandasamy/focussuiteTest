﻿// JS validation functions for resume preferences tab
// ==================================================

// initialise variable that will be dynamically populated with code values
var resumePreferencesCodeValues;

function Preferences_PageLoad(sender, args) {
    var moneyText = $('#txtWages');

    if ($(moneyText).exists())
        $(moneyText).val(formatCurrency($(moneyText).val()));

    var indBehavior = $find(resumePreferencesCodeValues.ccdMSABehaviorID);

    if (indBehavior != null) {
        indBehavior.add_populated(function () {
            $('#ddlMSA').next().find(':first-child').text($('#ddlMSA option:selected').text()).parent().addClass('changed');
        });
    }

    var rbArea = $('#rbSearchMSA');
    var rbRadius = $('#rbSearchArea');

    if ($(rbArea).is(':checked')) {
        Hide_Radius_StateMSA(rbArea);
    }
    else if ($(rbRadius).is(':checked')) {
        Hide_Radius_StateMSA(rbRadius);
      }
    CheckforAny();
    ResumeSearchableChanged();
    PostingsInterestedInChanged();
}

function CheckRadius() {
    var rbSearchArea = $('#rbSearchArea');
    var radius = $('#ddlRadius').val();
    var zipCode = $('#txtZip').val();
    if (radius != '' || zipCode.value != '') {
        $(rbSearchArea).prop('checked', true);
        Hide_Radius_StateMSA(rbSearchArea);
    }
}
function CheckMSA() {
    var rbSearchMSA = $('#rbSearchMSA');
    var state = $('#ddlState').val();
    var MSA = $('#ddlMSA').val();
    if (state != '' | MSA != '') {
        $(rbSearchMSA).prop('checked', true);
        Hide_Radius_StateMSA(rbSearchMSA);
    }
}

function AnyClicked() {
    var checkButtonsShift = $('#cblShift');
    if ($(checkButtonsShift).exists()) {
        $(checkButtonsShift).find('input:checkbox').prop('checked', ($('#cbAny').is(':checked')));
    }
}

function CheckforAny() {
    var checkButtonsShift = $('#cblShift');

    if ($(checkButtonsShift).exists()) {
        var allClear = true;
        var allChecked = true;

        $(checkButtonsShift).find('input:checkbox').each(function () {
            if ($(this).is(':checked'))
                allClear = false;
            else
                allChecked = false;
        });
        if (!allClear)
            $('#cbAny').prop('checked', true);
        if (!allChecked)
            $('#cbAny').prop('checked', false);
    }
}

function validatePreferencesPayUnit(sender, args) {
  var payUnitDDL = $('#ddlPayUnit');
  var payUnitValue = $(payUnitDDL).val();
  var money = $('#txtWages').val().replace(/\,/g, '');

  if (money !== "") {
    var decimalMoney = parseFloat(money);
    if ($(payUnitDDL).get(0).selectedIndex === 0) {
      sender.innerHTML = resumePreferencesCodeValues.errorPayUnitRequired;
      args.IsValid = false;
    }
    else if (payUnitValue == resumePreferencesCodeValues.frequencyHourly && (decimalMoney < parseFloat(resumePreferencesCodeValues.minHourlyThreshold) || decimalMoney > parseFloat(resumePreferencesCodeValues.maxHourlyThreshold))) {
      sender.innerHTML = resumePreferencesCodeValues.errorHourlyPayLimit;
      args.IsValid = false;
    }
    else if (payUnitValue == resumePreferencesCodeValues.frequencyDaily && (decimalMoney < parseFloat(resumePreferencesCodeValues.minDailyThreshold) || decimalMoney > parseFloat(resumePreferencesCodeValues.maxDailyThreshold))) {
      sender.innerHTML = resumePreferencesCodeValues.errorDailyPayLimit;
      args.IsValid = false;
    }
    else if (payUnitValue == resumePreferencesCodeValues.frequencyWeekly && (decimalMoney < parseFloat(resumePreferencesCodeValues.minWeeklyThreshold) || decimalMoney > parseFloat(resumePreferencesCodeValues.maxWeeklyThreshold))) {
      sender.innerHTML = resumePreferencesCodeValues.errorWeeklyPayLimit;
      args.IsValid = false;
    }
    else if (payUnitValue == resumePreferencesCodeValues.frequencyMonthly && (decimalMoney < parseFloat(resumePreferencesCodeValues.minMonthlyThreshold) || decimalMoney > parseFloat(resumePreferencesCodeValues.maxMonthlyThreshold))) {
      sender.innerHTML = resumePreferencesCodeValues.errorMonthlyPayLimit;
      args.IsValid = false;
    }
    else if (payUnitValue == resumePreferencesCodeValues.frequencyYearly && (decimalMoney < parseFloat(resumePreferencesCodeValues.minYearlyThreshold) || decimalMoney > parseFloat(resumePreferencesCodeValues.maxYearlyThreshold))) {
      sender.innerHTML = resumePreferencesCodeValues.errorYearlyPayLimit;
      args.IsValid = false;
    }
    }
    else if ($(payUnitDDL).get(0).selectedIndex == 0) {
    	sender.innerHTML = resumePreferencesCodeValues.errorWagesRequired + " & " + resumePreferencesCodeValues.errorPayUnitRequired;
    	args.IsValid = false;
    }
  else {
    if ($(payUnitDDL).get(0).selectedIndex !== 0) {
      sender.innerHTML = resumePreferencesCodeValues.errorWagesRequired;
      args.IsValid = false;
    }
  }
}


function AddAJob_Wages() {
    return Page_ClientValidate('Wages');
}

function validateAllPreferences() {
    var isValid = false;

    try {
    		isValid = Page_ClientValidate("Preferences");
        if (!isValid) return false;
    } catch (e) { }

    return isValid;
}

function validateShift(sender, args) {
    args.IsValid = $('#cblShift input:checkbox').is(':checked');
}

function validateWorkingWeek(sender, args) {
    var internshipChecked = false;
    var otherChecked = false;

    var postingsToSearch = $('#PostingsToSearchCheckboxList input');
    postingsToSearch.each(function () {
        if (($(this).is(':checked') && $(this).next('label').html()) === resumePreferencesCodeValues.labelInternshipCheckbox) {
            internshipChecked = true;
        }
        else if ($(this).is(':checked'))
            otherChecked = true;
    });

    $("#WorkOverTimeValidator").hide();
    args.IsValid = $('#ddlWorkWeek').prop("disabled") || $('#ddlWorkWeek').val().length > 0 || (postingsToSearch.length > 0 && !otherChecked);
}

function Hide_Radius_StateMSA(sender) {
  var rbArea = $('#rbSearchArea');
  var rbMSA = $('#rbSearchMSA');
  var ddlState = $('#ddlState');
  var ddlMSA = $('#ddlMSA');
  var ddlRadius = $('#ddlRadius');
  var txtZipCode = $('#txtZip');
  var cbSearchInMyState = $('#cbSearchInMyState');
  if ($(sender).is($(rbArea))) {
      EnableStyledDropDown(ddlRadius);
      $(txtZipCode).prop('disabled', false);

      $(ddlMSA).empty();
      $(ddlMSA).append($('<option>', {
          value: '',
          text: '- select city -'
      }));

      DisableStyledDropDown(ddlState, true);
      DisableStyledDropDown(ddlMSA, true);
      cbSearchInMyState.prop('disabled', false);
  }
  else {
    EnableStyledDropDown(ddlState);
    EnableStyledDropDown(ddlMSA);
    DisableStyledDropDown(ddlRadius, true);
    $(txtZipCode).prop('disabled', true);
    $(txtZipCode).val('');
    cbSearchInMyState.prop('disabled', true);
    cbSearchInMyState.prop('checked', false);
  }
}

function ValidateRadius(sender, args) {
    var radius = $('#ddlRadius').val();
    var zipCode = $('#txtZip').val().trim();
    args.IsValid = !($("#rbSearchArea").is(':checked')) || (radius.length > 0 && zipCode.length == 5);
}

function ValidateState(sender, args) {
    var state = $('#ddlState')[0].selectedIndex;
    args.IsValid = !($("#rbSearchMSA").is(':checked')) || state > 0;
}

function ValidateLocationPreference(source, args) {
  args.IsValid = $("#rbSearchArea").is(':checked') || $("#rbSearchMSA").is(':checked');
}

function ResumeSearchableChanged() {
    var postingsSelectable = true;
    if ($('#rblResumeSearchable input:checked').val() === 'no') {
        postingsSelectable = false;
        $("#PostingsToSearchValidator").hide();
        $('#rblContactDetailsVisible').prop('disabled', true);
        $('#rblContactDetailsVisible input[value="no"]').prop('checked', true);
    } else {
        $('#rblContactDetailsVisible').prop('disabled', false);
    }
    $('#PostingsToSearchCheckboxList input').each(function () {
        if (!postingsSelectable)
            $(this).prop('checked', false);
        $(this).prop('disabled', !postingsSelectable);
    });
}

function PostingsInterestedInChanged() {
    if ($('#rblResumeSearchable input:checked').val() === 'no')
        return;

    var internshipChecked = false;
    var otherChecked = false;

    $('#PostingsToSearchCheckboxList input').each(function () {
        if (($(this).is(':checked') && $(this).next('label').html()) === resumePreferencesCodeValues.labelInternshipCheckbox) {
            internshipChecked = true;
        }
        else if ($(this).is(':checked'))
            otherChecked = true;
    });

    PreferencesWorkWeekState(internshipChecked && !otherChecked);
}

function ValidatePostingsToSearch(sender, args) {
    var checkBoxCount = 0;
    var checkCount = 0;
    $('#PostingsToSearchCheckboxList input:checkbox:enabled').each(function () {
        checkBoxCount++;
        if ($(this).is(':checked')) {
            checkCount++;
        }
    });

    args.IsValid = checkBoxCount == 0 || checkCount > 0;
}

function validateSkillsAlreadyCapturedBox(sender, args) {
    var checkCount = 0;
    $('#SkillsAccordionDiv input:checkbox').each(function () {
        if ($(this).is(':checked')) {
            checkCount++;
        }
    });

    // ReSharper disable ExpressionIsAlwaysConst
    args.IsValid = (!($("#EducationPreferencesInternshipSkillsDiv").is(":visible")) || $("#SkillsAlreadyCapturedBox").is(':checked') || checkCount > 0);
    // ReSharper restore ExpressionIsAlwaysConst
}

function PreferencesWorkWeekState(disable) {
    if (disable)
        $('#ddlWorkWeek').prop('disabled', true).next(".customStyleSelectBox").addClass('stateDisabled');
    else {
        $('#ddlWorkWeek').prop('disabled', false).next(".customStyleSelectBox").removeClass('stateDisabled');
    }
}