﻿alwaysOnTop.init({
	targetid: 'selectedQuestions',
	orientation: 4,
	position: [50, 325],
	fadeduration: [0, 0],
	frequency: 1,
	hideafter: 0
});

function pageLoad(sender, args) {
	AddToolTipToListItems();
}

function AddToolTipToListItems() {
	var statementsList = '';
	$('#lbSelectedStatements>option').each(function (i, opt) {
		opt.title = opt.text;
		if (statementsList === '')
			statementsList += '#';
		statementsList += opt.text + '^' + opt.value;
	});
	$('#SelectedStatementsHiddenField').val(statementsList);
}

//Take statements from hidden field and fill the list box.
function UpdateSelectedStatementsList() {
	if ($('#SelectedStatementsHiddenField').val().length > 0) {
		var selectedStatements = $('#SelectedStatementsHiddenField').val();
		var list = $('#lbSelectedStatements');
		var arrOuter = selectedStatements.split('#');
		$.each(arrOuter, function (i, arrOuterItem) {
			var arrInner = arrOuterItem.split('^');
			$(list).append($('<option>', {
				value: arrInner[1],
				text: arrInner[0]
			}));
		});
		$(list).children().last().prop('selected', true);
		AddToolTipToListItems();
	}
}

function AddYesNoInStatementList(id, statement) {
	statement = statement.replace(/ˆ/g, '\'');
	var list = $('#lbSelectedStatements');
	var listLength = $(list).children().length;
	var optionExists = $(list).children('option[value="' + id + '"]').exists();

	if (listLength < 12 && !optionExists) {
		AddStatementToList(list, id, (listLength + 1) + ". " + statement);
		AddToolTipToListItems();
		return true;
	}
	else if (!optionExists) {
		$find('StatementSelectionInfoModal').show();
		if ($('#rdbQuestionNo_' + id).exists())
			$('#rdbQuestionNo_' + id).prop('checked', true);
		else if ($('#rdbQuestionSometimes_C_' + id).exists())
			$('#rdbQuestionSometimes_C_' + id).prop('checked', true);
		return true;
	}
}

function RemoveYesNoFromStatementList(id) {
	var list = $('#lbSelectedStatements');
	var listLength = RemoveStatementAndReorder(list, id);
	AddToolTipToListItems();
	return true;
}

function AddRemoveMultiInStatementList(id, statement) {
	statement = statement.replace(/ˆ/g, '\'');
	var list = $('#lbSelectedStatements');
	var listLength = RemoveStatementAndReorder(list, id);

	var chkBoxOption = $('#chkOptions_' + id);
	if (listLength < 12) {
		var arrSelectedOptions = [];
		$(chkBoxOption).find('input:checked').each(function (index, option) {
			arrSelectedOptions.push($(option).next('label').html());
		});
		if (typeof arrSelectedOptions !== 'undefined' && arrSelectedOptions.length > 0) {
			AddStatementToList(list, id, (listLength + 1) + '. ' + statement + ' ' + arrSelectedOptions.join(',') + '.');
			AddToolTipToListItems();
			return true;
		}
	}
	else {
		$find('StatementSelectionInfoModal').show();
		$(chkBoxOption).find('input').prop('checked', false);
		return true;
	}
}

function AddRemoveOpenInStatementList(id, statement, isMyAccomplishment) {
	isMyAccomplishment = typeof isMyAccomplishment !== 'undefined' ? isMyAccomplishment : false;
	statement = statement.replace(/ˆ/g, '\'');
	var txtStatement = $('#txtOpen_' + id);
	var list = $('#lbSelectedStatements');
	var listLength = RemoveStatementAndReorder(list, id);

	if (listLength < 12) {
		if (TrimText($(txtStatement).val()).length > 0) {
			AddStatementToList(list, id, (listLength + 1) + '. ' + statement + ' ' + $(txtStatement).val());
			AddToolTipToListItems();
			return true;
		}
	}
	else {
		if (isMyAccomplishment) {
			$(txtStatement).val('').prop('disabled', true);
			$('#chkMyAccomplishment').prop('checked', false);
		}

		$find('StatementSelectionInfoModal').show();
		$(txtStatement).val('');
		return true;
	}
}

function EnableDisableMyAccomplishment(id, statement) {
	var txtStatement = $('#txtOpen_' + id);
	if ($('#chkMyAccomplishment').is(':checked'))
		$(txtStatement).prop('disabled', false);
	else {
		$(txtStatement).val('').prop('disabled', true);
	}
	AddRemoveOpenInStatementList(id, statement, true);
	return true;
}

function RemoveStatementAndReorder(list, optionId) {
	$(list).children('option[value="' + optionId + '"]').remove();
	var listLength = 0;
	$(list).children('option').each(function (index, option) {
		option.text = (listLength + 1) + option.text.substr(option.text.indexOf('.'), option.text.length);
		listLength++;
	});
	return listLength;
}

function AddStatementToList(list, optionId, optionText) {
	$(list).append($('<option>', {
		text: optionText,
		value: optionId
	}));
	$(list).children('option[value="' + optionId + '"]').prop('selected', true);
}