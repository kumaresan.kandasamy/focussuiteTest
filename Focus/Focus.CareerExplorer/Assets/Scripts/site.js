﻿// Startup JS
$(document).ready(function () {

	// IE10 viewport fix
	if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
		var msViewportStyle = document.createElement('style');
		msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
		document.querySelector('head').appendChild(msViewportStyle);
	}

	// Opening and closing of collapsable accordions
	$('.collapsableAccordionTitle').on('click', function () {
		if ($(this).hasClass('on')) {
			// Add the on class to the button
			$(this).removeClass('on');
			$(this).next('.collapsableAccordionContent').removeClass('open');
			$(this).next('.collapsableAccordionContent').hide();
			collapsiblePanelCollapsed();
		} else {
			$(this).addClass('on');
			$(this).next('.collapsableAccordionContent').addClass('open');
			$(this).next('.collapsableAccordionContent').show();
			collapsiblePanelExpanded();
		}
	});

	//File upload styling script
	$(function () {
		$('.fileUpload').customFileInput();
	});


	// Open overlay panel
	$(".overlayPanelOpenTrigger").click(function () {
		$(".overlayPanelOpen").hide();
		$(this).nextAll(".overlayPanelOpen").show();
	});
	// Close overlay panel
	$(".overlayPanelClosedTrigger").click(function () {
		$(this).closest(".overlayPanelOpen").toggle();
	});

	// Ensure open panel width equals closed panel width
	$(".overlayPanelContainer").each(function () {
		var overlayPanelClosedWidth = $(this).children(".overlayPanelClosed").width();
		var overlayPanelOpenWidth = $(this).children(".overlayPanelOpen").width();

		if (overlayPanelClosedWidth > overlayPanelOpenWidth) {
			$(this).children(".overlayPanelOpen").width(overlayPanelClosedWidth + 5); // +5 for padding
		} else {
			$(this).children(".overlayPanelClosed").width(overlayPanelOpenWidth - 5); // -5 for padding
			if ($(this).hasClass("standardPanel")) {
				$(this).width(overlayPanelOpenWidth + 10);
			}
		}
	});

	$(".SeeMoreLessPanel").each(function () {
		var panel = $(this);
		var text = panel.find(".SeeMoreLessText");
		var moreLink = panel.find(".seeMore");
		var lessLink = panel.find(".seeLess");
		moreLink.click(function () {
			text.show();
			moreLink.hide();
			lessLink.show();
		});
		lessLink.click(function () {
			text.hide();
			moreLink.show();
			lessLink.hide();
		});
	});

	// Calling the custom select dropdown script
	//$('.desktop select:not(.ListBox, .ajax__htmleditor_toolbar_selectbutton)').customSelect();	
	ApplyCustomSelectStyling();

	// Script to handle greying out of disabled drop downs	
	//$('#ddlDisabilityType').next('.customStyleSelectBox').addClass('stateDisabled');


	// Apply jQuery to infield labels
	$(".inFieldLabel > label, .inFieldLabelAlt > label, .inFieldLabelNoBreak > label").inFieldLabels();

	// if users paste content into a text box, the infield label script keeps the label ontop of the pasted content. this code will remove the label
	$("input[type='text']").bind('paste', function (e) {
		var textBox = $(this);
		setTimeout(function () { textBox.blur(); }, 1);
	});

	// initialise in-page messages
	$('.inPageError').each(function () {
		$(this).message({ type: 'error', dismiss: false });
	});
	$('.inPageMessage').each(function () {
		$(this).message({ type: 'info', dismiss: false });
	});

	// collapsible panels
	SetupCollapsiblePanel();

	//Device Jquery
	if ($('body').css('min-width') == '0px') {
		$('.table th .AscButton').parent('.table th td').css({
			"padding": "10px 0 10px 10px",
			"width": "15px"
		});
		$('.table th .AscButton').css({
			"float": "left",
			"display": "inherit",
			"position": "static",
			"bottom": "0px",
			"padding-bottom": "10px"
		});
		$('.table th .DescButton').css({
			"float": "left",
			"display": "inherit",
			"position": "static",
			"right": "0px",
			"top": "0px"
		});
		//$('#JobSearchResultDevice').append('<br/><br/><br/>');
	}
	//BugRel-494 fix
//	var ua = navigator.userAgent.toLowerCase();
//	var isAndroid = ua.indexOf("android") > -1;
//	if (isAndroid) {
//		$('#MainContent_UpdatePanelResumeWizard #RCPNavigation #RCPNavigationTop ul li a').css({
//			'padding-left': '10px', 'padding-right': '10px'});
//		$('#MainContent_UpdatePanelResumeWizard #RCPNavigation #RCPNavigationTop').css({ 'width': '1169px' });
//		$('#MainContent_UpdatePanelResumeWizard #RCPNavigation #RCPNavigationTop ul').css({ 'width': '1170px' });
	//} 
	
	// new style accordions
	SetupAccordionNew();

	BindTooltips();
});

function ApplyCustomSelectStyling(el, options) {
	if (focusSuite.brandIdentifier !== 'Default') return;
	if (typeof el === 'undefined' || el == null) {
	    //$('.desktop select:not(.ListBox, .ajax__htmleditor_toolbar_selectbutton, .form-control)').customSelect(options);
        // KRP - Enabling the custom select style for devices also
        $('select:not(.ListBox, .ajax__htmleditor_toolbar_selectbutton, .form-control)').customSelect(options);
	} else {
		$(el).customSelect(options);
	}
}

function SetupCollapsiblePanel() {
	$('.collapsiblePanel.cpCollapsed>.collapsiblePanelContent').hide();
	$('.collapsiblePanel').each(function () {
	  var panel = $(this);
	  $(this).undelegate('.cpShow, .cpHide, .cpIcon, .cpHeaderControl', 'click');
	  $(this).delegate('.cpShow, .cpHide, .cpIcon, .cpHeaderControl', 'click', function () {
	    var isSingle = false;
	    $('.collapsiblePanel.collapsibleSingle.cpExpanded').each(function () {
	      if (panel.get(0) != this) {
	        OpenHideCollapsiblePanel(this, 1000, true);
	        isSingle = true;
	      }
	    });
	    OpenHideCollapsiblePanel(panel, 1000, isSingle);
	  });
	});
}

function OpenHideCollapsiblePanel(panel, speed, toggleFirst) {
  toggleFirst = toggleFirst == null ? false : toggleFirst;
  
  if (toggleFirst) {
    $(panel).toggleClass('cpCollapsed');
    $(panel).toggleClass('cpExpanded');
  }
  $(panel).children('.collapsiblePanelContent').slideToggle(speed, 'easeInOutExpo', function () {
    if (!toggleFirst) {
      $(panel).toggleClass('cpCollapsed');
      $(panel).toggleClass('cpExpanded');
    }
  });
}

function SetupAccordionNew() {
	$('.accordionContentNew:not(.open)').hide();
	$('.accordionNew>.accordionSectionNew').each(function () {
		var accordionSection = $(this);
		$(this).undelegate('.accordionTitleNew', 'click');
		$(this).delegate('.accordionTitleNew', 'click', function () {
			$(accordionSection).children('.accordionContentNew').slideToggle(400, function () {
				$(this).parent('.accordionSectionNew').toggleClass('accordionExpandedNew');
				if (!($(this).is(':hidden'))) {
					$(accordionSection).siblings('.accordionSectionNew').children('.accordionContentNew').each(function () {
						$(this).slideUp(400, function () {
							$(this).parent('.accordionSectionNew').removeClass('accordionExpandedNew');
						});
					});
				}
			});
		});
	});
}

function BindTooltips() {
	$('.toolTipNew').tooltipster({
		maxWidth: 180,
		theme: '.tooltipster-career',
		contentAsHTML: true
	});
	$('*[data-toggle="tooltip"]').tooltip('destroy').tooltip(); //added to reinitialise Bootstrap tooltips after partial postback
}

// Disable backspace causing a back event
$(document).keydown(function (e) {
	var nodeName = e.target.nodeName.toLowerCase();

	if (e.which === 8) {
		if ((nodeName === 'input' && (e.target.type === 'text' || e.target.type === 'password')) || nodeName === 'textarea') {
			// do nothing 
		}
		else
			e.preventDefault();
	}
});

// Helper Functions
function TrimText(text) {
	if (!text || typeof text != 'string') return null;
	if (text === '') return '';
	return text.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ').replace(/[\n]+$/, '');
}

function ShowPanel(panelId, triggerElementId, hideTriggerElement, validationGroup) {
	var pageIsValid = true;
	if (validationGroup && validationGroup.length > 0) {
		pageIsValid = Page_ClientValidate(validationGroup);
	}
	if (pageIsValid) {
		if (hideTriggerElement) {
			$('#' + triggerElementId).hide();
		}
		$('#' + panelId).show();
	}
}

// Selector
function ShowHideSelectorDropDown(tableRowClientId, parentSelectedIndex) {
	if (parentSelectedIndex > 0) {
		$('#' + tableRowClientId).show();
	}
	else {
		$('#' + tableRowClientId).hide();
	}
}

// Reset Career Area selector
function ResetCareerAreaSelector(careerAreaLabelClientId, careerAreaLabelDefaultText, careerAreaDropDownClientId, jobTitleAutoCompleteTextBoxClientId) {
	var selectorReset = false;

	if ($('#' + careerAreaLabelClientId).text() != careerAreaLabelDefaultText) {
		if ($('#' + careerAreaDropDownClientId).prop('selectedIndex') > 0) {
			$('#' + careerAreaDropDownClientId).prop('selectedIndex', 0);
			var spanBox = $('#' + careerAreaDropDownClientId).next().find(':first-child');
			if (spanBox) {
				spanBox.text(careerAreaLabelDefaultText);
			}
		}

		if ($('#' + jobTitleAutoCompleteTextBoxClientId).val().length > 0) {
			$('#' + jobTitleAutoCompleteTextBoxClientId).val('');
			$('#' + jobTitleAutoCompleteTextBoxClientId).blur();
		}

		$('#' + careerAreaLabelClientId).text(careerAreaLabelDefaultText);

		selectorReset = true;
	}

	return selectorReset;
}

// Reset Education selector
function ResetEducationSelector(educationLabelClientId, educationLabelDefaultText, educationCurrentLabelClientId, educationNonSpecificRadioButtonClientId, educationNonSpecificDropDownListClientId, degreeCertificationAutoCompleteTextBoxClientId, programAreaRadioButtonClientId, programAreaDropDownClientId, programAreaDegreesDropDownClientId) {
	var selectorReset = false;
	if ($('#' + educationLabelClientId).text() != educationLabelDefaultText) {

		if ($('#' + educationNonSpecificDropDownListClientId).prop('selectedIndex') > 0) {
			$('#' + educationNonSpecificDropDownListClientId).prop('selectedIndex', 0);
			var spanBox = $('#' + educationNonSpecificDropDownListClientId).next().find(':first-child');
			if (spanBox) {
				spanBox.text(educationLabelDefaultText);
			}
		}

	  var degreeCertificationAutoCompleteTextBox = $('#' + degreeCertificationAutoCompleteTextBoxClientId);
	  if (degreeCertificationAutoCompleteTextBox.length > 0 && degreeCertificationAutoCompleteTextBox.val().length > 0) {
	    degreeCertificationAutoCompleteTextBox.val('');
	    degreeCertificationAutoCompleteTextBox.blur();
}
	    
        if ($('#' + programAreaRadioButtonClientId).exists()) {
            $('#' + programAreaDropDownClientId).prop('selectedIndex', 0);
            $('#' + programAreaDegreesDropDownClientId).prop('selectedIndex', 0);
            $('#' + programAreaDropDownClientId).change();
        }

        $('#' + educationNonSpecificRadioButtonClientId).prop('checked', true);
		$('#' + educationCurrentLabelClientId).text(educationLabelDefaultText);
		$('#' + educationLabelClientId).text(educationLabelDefaultText);

		selectorReset = true;
	}

	return selectorReset;
}

// Disable modal links
function DisableLightboxModalLinks(lightboxId, message) {
	$(document).ready(function () {
		$('#' + lightboxId).find('.modalLink').each(function () {
			$(this).click(function (event) {
				event.preventDefault();
				alert(message);
			});
		});
	});
}

// Load cascading drop down
function LoadCascadingDropDownList(parentSelectedValue, dropDownListClientId, serviceMethodUrl, category, topDefaultText, loadingText, dropDownListSelectedIndex) {
	var select = $('#' + dropDownListClientId);

	var selectOptions;
	if (select.prop) {
		selectOptions = select.prop('options');
	}
	else {
		selectOptions = select.attr('options');
	}
	$('option', select).remove();

	if (dropDownListSelectedIndex == null) {
		dropDownListSelectedIndex = -1;
	}

	if (topDefaultText && topDefaultText.length > 0) {
		if (dropDownListSelectedIndex == -1) {
			selectOptions[selectOptions.length] = new Option(topDefaultText, '', false, true);
		}
		else {
			selectOptions[selectOptions.length] = new Option(topDefaultText, '', false, false);
		}
	}

	var spanBox = select.next().find(':first-child');
	if (spanBox && loadingText && loadingText.length > 0) {
		spanBox.text(loadingText);
	}
	if (parentSelectedValue.length > 0) {
		var ajaxOptions = {
			type: "POST",
			url: serviceMethodUrl,
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: true,
			data: '{"knownCategoryValues": "' + parentSelectedValue + '", "category": "' + category + '"}',
			success: function (response) {
				var results = response.d;
				if (results && results.length > 0) {
					$.each(results, function (index, option) {
						if (dropDownListSelectedIndex != -1 && (dropDownListSelectedIndex - 1) == index) {
							selectOptions[selectOptions.length] = new Option(option.name, option.value, false, true);
						}
						else {
							selectOptions[selectOptions.length] = new Option(option.name, option.value, false, false);
						}
					});
					if (spanBox) {
						spanBox.text(select.find(':selected').text());
					}
				}
			}
		};

		$.ajax(ajaxOptions);
	}
	else {
		if (spanBox) {
			spanBox.text(select.find(':selected').text());
		}
	}
}

function SiteSearchTerm_Validate(source, args) 
{
  var searchTerm = $('#SearchTermTextBox').val();
  if (!searchTerm) 
    return;

	searchTerm = TrimText(searchTerm);
	if (searchTerm.length > 0 && searchTerm.length < 3) 
	{
		args.IsValid = false;
		return;
	}

	args.IsValid = true;
}

function showProcessing() {
	$("#UpdateProgressIndicator").show();

	setTimeout('$("#ImageProgress").attr("src", $("#ImageProgress").attr("src"));', 50);
}

function formatCurrency(usFormat) {
	var sign, cents;
	if (usFormat === '' || typeof usFormat === 'undefined')
		return '';
	usFormat = usFormat.toString().replace(/\$|\,/g, '');
	if (isNaN(usFormat))
	  usFormat = '0';
	sign = (usFormat == (usFormat = Math.abs(usFormat)));
	usFormat = Math.floor(usFormat * 100 + 0.50000000001);
	cents = usFormat % 100;
	usFormat = Math.floor(usFormat / 100).toString();
	if (cents < 10)
		cents = '0' + cents;
	for (var i = 0; i < Math.floor((usFormat.length - (1 + i)) / 3); i++)
		usFormat = usFormat.substring(0, usFormat.length - (4 * i + 3)) + ',' + usFormat.substring(usFormat.length - (4 * i + 3));
	if (usFormat == '0' && cents == '000')
		return '0.00';
	else
		return (((sign) ? '' : '-') + usFormat + '.' + cents);
}

function GetAgeForDate(birth, now) {
  if (now == null)
    now = new Date();

  var currentYear = now.getFullYear();
  var yearDiff = currentYear - birth.getFullYear();
  var birthdayThisYear = new Date(currentYear, birth.getMonth(), birth.getDate());

  return (now >= birthdayThisYear)
        ? yearDiff
        : yearDiff - 1;
}

function DisableStyledTextBox(textBoxCtrl, defaultMask) {
	if ($(textBoxCtrl).exists()) {
		if (defaultMask != null)
			$(textBoxCtrl).val(defaultMask);
		$(textBoxCtrl).css('background-color', '#F2F2F2').prop('disabled', true);
	}
}

function EnableStyledTextBox(textBoxCtrl) {
	if ($(textBoxCtrl).exists()) {
		$(textBoxCtrl).css('background-color', '#ffffff').prop('disabled', false);
	}
}

function EnableStyledDropDown(dropDownCtrl) {
	if ($(dropDownCtrl).exists()) {
		$(dropDownCtrl).prop('disabled', false);
		$(dropDownCtrl).next().removeClass('stateDisabled');
	}
}

function UpdateStyledDropDown(dropDownCtrl, selectedValue) {
	if ($(dropDownCtrl).exists()) {
		if (selectedValue != null)
			$(dropDownCtrl).children('option[value="' + selectedValue + '"]').prop('selected', true);
		else
			$(dropDownCtrl).children('option:first').prop('selected', true);

		$(dropDownCtrl).next().find(':first-child').text($(dropDownCtrl).children('option:selected').text()).parent().addClass('changed');
	}
}

function DisableStyledDropDown(dropDownCtrl, resetSelectedIndex) {
	if ($(dropDownCtrl).exists()) {
		if (resetSelectedIndex)
			$(dropDownCtrl).children('option:first').prop('selected', true);
		$(dropDownCtrl).prop('disabled', true);
		$(dropDownCtrl).next().find(':first-child').text($(dropDownCtrl).children('option:selected').text()).parent().addClass('changed');
		$(dropDownCtrl).next().addClass('stateDisabled');
	}
}

function textCounter(inputCtrl, labelCtrl) {
	var s = TrimText($(inputCtrl).val());
	if (s != null && s.length > 0) {
		s = s.replace(/(^\s*)|(\s*$)/gi, "");
		s = s.replace(/[ ]{2,}/gi, " ");
		s = s.replace(/\n /, "\n");
		$(labelCtrl).text(s.split(' ').length);
	}
	else
		$(labelCtrl).text('0');
}

/* Master page session timeout script */
var sessionTimeout, urlHome, count, timeoutCounter, timeoutDisplay, urlServiceMethod;

function DisplaySessionTimeout() {
	var timer = $('#divtimer');
	if (timer != null)
		$(timer).hide();
	clearTimeout(timeoutCounter);
	clearTimeout(timeoutDisplay);
	count = 60 * sessionTimeout;
	timeoutDisplay = setTimeout(function () { TimeoutLogout();  }, 1000 * count);
	timeoutCounter = setTimeout(function () { countDown(); }, 1000);
}

function ResetTimer() {
	clearTimeout(timeoutDisplay);
	count = 60 * sessionTimeout;
	timeoutDisplay = setTimeout(function () { TimeoutLogout(); }, 1000 * count);
	HeartBeat();
}

function countDown() {
	var timerOuter = $('#divtimer');
	var timerInner = $('#timer');
	count--;
	if (count <= 50 && count > 0)
		$(timerOuter).show();
	else
		$(timerOuter).hide();
	$(timerInner).html("Your browser session expired after a " + sessionTimeout + " minute period of inactivity. <br/> You will be redirected to ‘Sign in’ page in  00:" + (count > 9 ? count : '0' + count) + (count == 1 ? " second." : " seconds."));
	timeoutCounter = setTimeout(function () { countDown(); }, 1000);
}

function HeartBeat() {
	$.ajax({
		type: "POST",
		url: urlServiceMethod + "/HeartBeat",
		data: '{"value":"abc"}',
		processData: false,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
		}
	});
}

function TimeoutLogout() {
	$.ajax({
		type: "POST",
		url: urlServiceMethod + "/LogOut",
		processData: false,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		beforeSend: function () {
			$("#disableDuringLogout").show();
		},
		success: function (data) {
			location.href = urlHome;
		},
		complete: function () {
			location.href = urlHome;
			$("#disableDuringLogout").show();
		}
	});
}

// helper method for checking whether an element exists in the DOM
$.fn.exists = function() {
	return jQuery(this).length > 0;
};


// global variables
var allCollapsiblePanelBehaviors, expandCollapseButton, expandAllText, collapseAllText;

function toggleExpandCollapseButtonState(collapsed) {
	/// <summary>Toggles the Expand All / Collapse All button state.</summary>
	if (expandCollapseButton.hasClass('is-collapsed') && !collapsed) {
		expandCollapseButton.val(collapseAllText).removeClass('is-collapsed');
	}
	else if (!expandCollapseButton.hasClass('is-collapsed') && collapsed) {
		expandCollapseButton.val(expandAllText).addClass('is-collapsed');
	}
}

function collapsiblePanelCollapsed() {
	/// <summary>Event handler fired when each collapsible panel collapses.</summary>
	var allPanelsCollapsed = true;

	$('.collapsableAccordionTitle').each(function(idx, elem) {
		if ($(this).hasClass('on')) {
			allPanelsCollapsed = false;
		}
	});

	// if all panels are collapsed, set the control button to 'Expand all'
	if (allPanelsCollapsed) {
		toggleExpandCollapseButtonState(true);
	}
}

function collapsiblePanelExpanded() {
	/// <summary>Event handler fired when each collapsible panel expands.</summary>
	var allPanelsExpanded = true;
	$('.collapsableAccordionTitle').each(function (idx, elem) {
		if (!$(this).hasClass('on')) {
			allPanelsExpanded = false;
		}
	});

	// if all panels are expanded, set the control button to 'Collapse all'
	if (allPanelsExpanded) {
		toggleExpandCollapseButtonState(false);
	}
}


function bindExpandCollapseAllPanels(controlButton, expandText, collapseText) {
	/// <summary>Binds the Expand / Collapse All button.</summary>
	/// <param name="controlButton" type="String" mayBeNull="false">The (client) ID of the Expand / Collapse All button.</param>
	/// <param name="expandText" type="String" mayBeNull="true">The "Expand All" text to display on the button.</param>
	/// <param name="collapseText" type="String" mayBeNull="true">The "Collapse All" text to display on the button.</param>
	expandAllText = expandText || 'Expand all'; // in case no value passed in for expandText
	collapseAllText = collapseText || 'Collapse all'; // in case no value passed in for collapseText
	expandCollapseButton = $('#' + controlButton);

	// OK, now we have a list of all panels that are part of a nested group, and a list of those that aren't...

	// ... so now we can bind the click event of the 'expand all/collapse all' button
	 expandCollapseButton.on('click', function () {
     if (!$(this).hasClass('is-collapsed')) {
      // the button is currently set to 'Collapse All', so we need to collapse all panels
      $('.collapsableAccordionTitle').each(function (idx, elem) {
       $(this).removeClass('on');
       $(this).next('.collapsableAccordionContent').removeClass('open').hide();
      });
      // and finally change the state of the button to set its text to 'expand all' and add the class 'is-collapsed'
      toggleExpandCollapseButtonState(true);
     } else {
      // the button is currently set to 'Expand all' so now we have to get clever
      $('.collapsableAccordionTitle').each(function (idx, elem) {
       $(this).addClass('on');
       $(this).next('.collapsableAccordionContent').addClass('open').show();
      });
      // and finally change the state of the button to set its text to 'collapse all' and remove the class 'is-collapsed'
      toggleExpandCollapseButtonState(false);
     }
     
		return false;
	});
}

function OpenLocalisePopup(localiseKey, applicationPath) {
  var url = applicationPath + "/LocalisePopup.aspx?localiseKey=" + localiseKey;

  //var oArgs = window.showModalDialog(url, null, "dialogHeight: 400px;dialogWidth: 800px; scroll:no; status:no;");
  window.open(url, "", "width=800,height=400", "scrollbars: 0");
  if (oArgs && oArgs.val && oArgs.val.length > 0) {
    //alert(oArgs.val);
    window.location = window.location;
  }
}

function equaliseCareerPathItems() {
	/// <summary>Equalises the height of the boxes in the career path chart (to the height of the tallest box).</summary>
	setTimeout(function () {
		var heightsArray = [];
		$('.careerPathChart .box td').height('auto').each(function () {
			heightsArray.push($(this).height());
		});
		$('.careerPathChart .box td').height(Math.max.apply(Math, heightsArray));
}, 10);
}

function isAppleMobile() {
	/// <summary>Detects whether the user is using an Apple Mobile (iOS) device.</summary>
	if (navigator && navigator.userAgent && navigator.userAgent != null) {
		var strUserAgent = navigator.userAgent.toLowerCase();
		var arrMatches = strUserAgent.match(/(iphone|ipod|ipad)/);
		if (arrMatches != null)
			return true;
	}
	return false;
}

function setButtonFocus(buttonSelector) {
	/// <summary>Gives the specified button focus. Includes a check for iOS devices as iOS 8 has introduced a bug when setting focus on a button.</summary>
	/// <param name="buttonSelector">The jQuery selector for the button.</param>
	if (!isAppleMobile()) {
		$(buttonSelector).focus();
	}
}

function fixAutoCompleteExtender(sender, args) {
	/// <summary>Fixes the position of the ACT AutoCompleteExtender for tablets.</summary>
	var extender = sender._popupBehavior._element;
	var inputControlId = sender._element;
	if (Modernizr.touch) {
		extender.style.top = (parseInt($(inputControlId).offset().top + 26)).toString() + 'px';
		extender.style.left = (parseInt($(inputControlId).offset().left)).toString() + 'px';
	}
}

function autotab(original, destination) {
	if (original.getAttribute && original.value.length == original.getAttribute("maxlength"))
		document.getElementById(destination).focus();
}
