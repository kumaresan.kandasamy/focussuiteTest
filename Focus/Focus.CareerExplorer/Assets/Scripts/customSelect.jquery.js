(function ($) {
	$.fn.extend({

		customSelect: function (options) {
			//if (!$.browser.msie || ($.browser.msie && $.browser.version > 6)) {
			// no longer worrying about IE6 as such but just to be safe...
			try {
				if (options === 'refresh') {
					var that = this;
					$(that).each(function () {
						$(this).next('span.customStyleSelectBox').remove();
					});
				}
				return this.each(function () {
					if ($(this).attr('class') === 'ListBox' || $(this).parents('.customStyleSelectBoxIgnore').exists())
						return;
					var currentSelected = $(this).find(':selected');
					// FVN-2735: removed the code below added in FVN-2557 as it was causing issues elsewhere in the system.
					//				  if (currentSelected.parent().attr('customSelect') !== undefined || currentSelected.parent().attr('customSelect') === true)
					//				    return;
					//				  currentSelected.parent().attr('customSelect', 'true');
					if ($(this).next("span.customStyleSelectBox").length > 0)
						return;
					$(this).after('<span class="customStyleSelectBox"><span class="customStyleSelectBoxInner">' + currentSelected.text() + '</span></span>').css({ position: 'absolute', opacity: 0, fontSize: $(this).next().css('font-size') });
					var selectBoxSpan = $(this).next();
					var selectBoxWidth = parseInt($(this).width()) - parseInt(selectBoxSpan.css('padding-left')) - parseInt(selectBoxSpan.css('padding-right'));
					var selectBoxSpanInner = selectBoxSpan.find(':first-child');
					selectBoxSpan.css({ display: 'inline-block' });
					selectBoxSpanInner.css({ width: selectBoxWidth, display: 'inline-block' });
					var selectBoxHeight = parseInt(selectBoxSpan.height()) + parseInt(selectBoxSpan.css('padding-top')) + parseInt(selectBoxSpan.css('padding-bottom'));
					$(this).height(selectBoxHeight).change(function () {
						// selectBoxSpanInner.text($(this).val()).parent().addClass('changed');   This was not ideal
						selectBoxSpanInner.text($(this).find(':selected').text()).parent().addClass('changed');
						// Thanks to Juarez Filho & PaddyMurphy
					});

				});
			}
			catch (ex) {
				return null;
			}
		}
	});
})(jQuery);
