﻿// JS validation functions for resume profile tab
// ==============================================

// initialise variable that will be dynamically populated with code values
var resumeProfileCodeValues;

function Profile_PageLoad(sender, args) {
  $("#rblTap input[type=radio]").change(function () {
    EnableDisableTapDate($(this).val() == "yes");
  });

$('#cbLowLevelLiteracy').click(function () {
      var panLanguageDetail = $('#panLanguageDetail');
      if ($(this).is(":checked")) {
          panLanguageDetail.slideDown(1000, 'easeInOutExpo');
      }
      else {
          panLanguageDetail.slideUp(1000, 'easeInOutExpo');
      }
  });

  toggleDiv();
  ShowHideMilitaryServiceDetail(false);

  EnableDisableTapDate($("#rblTap input[type=radio]:checked").val() == "yes");
  $('#ddlGender, #rblUSCitizen, #DOBTextBox').on('change', toggleDiv);
  $('#DOBTextBox').on('keyup', toggleDiv);

  var permanentResidentCheckBox = $("#rblPermanentResident input[type=radio]");
  permanentResidentCheckBox.change(function () {
    HideShowAlienRegExpiryDate($(this).val() === 'yes');
  });

  HideShowAlienRegExpiryDate($("#rblPermanentResident input[type=radio]:checked").val() === "yes");
}

$(document).ready(function() {
  Profile_PageSetup();
  Sys.WebForms.PageRequestManager.getInstance().add_endRequest(Profile_PageSetup);
});

function Profile_PageSetup() {
  EnableDisableStatus();
  Page_ClientValidate('VeteranType');
}

function HideShowAlienRegExpiryDate(permResidentRefugee) {
  if (focusSuite.theme === "workforce") {
    if (permResidentRefugee && resumeProfileCodeValues.EnableOptionalAlienRegExpiryDate === "1") {
      $('#AlienRegExpiryDiv').hide();
    } else {
      $('#AlienRegExpiryDiv').show();
    }
  }
}

function EnableDisableTapDate(tapAttended) {
  if (focusSuite.theme == "workforce") {
    var customValidator = document.getElementById(resumeProfileCodeValues.TapAttendanceDateValidatorClientId);
    customValidator.enabled = tapAttended;

    if (tapAttended) {
      $("#TapDateDiv").show();
    }
    else {
      $("#TapDateDiv").hide();
      $("#TapAttendanceDateTextBox").val("");
      customValidator.isvalid = true;
      ValidatorUpdateDisplay(customValidator);
    }
  }
  else {
    $("#TapDateDiv").hide();
  }
}

var dtCh = "/";
var minYear = 1900;
var maxYear = 2100;

function isInteger(s) {
  var i;
  for (i = 0; i < s.length; i++) {
    var c = s.charAt(i);
    if (((c < "0") || (c > "9"))) return false;
  }
  return true;
}

function stripCharsInBag(s, bag) {
  var i;
  var returnString = "";
  for (i = 0; i < s.length; i++) {
    var c = s.charAt(i);
    if (bag.indexOf(c) === -1) returnString += c;
  }
  return returnString;
}

function daysInFebruary(year) {
  return (((year % 4 === 0) && ((!(year % 100 === 0)) || (year % 400 === 0))) ? 29 : 28);
}
function daysInMonth(i) {
    if (i === 4 || i === 6 || i === 9 || i === 11) { return 30; }
    else if (i === 2) { return 29; }
    else return 31;
}
function DaysArray(n) {
  for (var i = 1; i <= n; i++) {
    this[i] = 31;
    if (i === 4 || i === 6 || i === 9 || i === 11) {
      this[i] = 30;
    }
    if (i === 2) {
      this[i] = 29;
    }
  }
  return this;
}

function isDate(dtStr) {
  //var daysInMonth = DaysArray(12);
  var pos1 = dtStr.indexOf(dtCh);
  var pos2 = dtStr.indexOf(dtCh, pos1 + 1);
  var strMonth = dtStr.substring(0, pos1);
  var strDay = dtStr.substring(pos1 + 1, pos2);
  var strYear = dtStr.substring(pos2 + 1);
  var month, day, year, strYr;

  strYr = strYear;
  if (strDay.charAt(0) === '0' && strDay.length > 1) strDay = strDay.substring(1);
  if (strMonth.charAt(0) === '0' && strMonth.length > 1) strMonth = strMonth.substring(1);
  for (var i = 1; i <= 3; i++) {
    if (strYr.charAt(0) === '0' && strYr.length > 1) strYr = strYr.substring(1);
  }
  month = parseInt(strMonth);
  day = parseInt(strDay);
  year = parseInt(strYr);
  if (pos1 === -1 || pos2 === -1) {
    if (dtStr === '') {
      return true;
    }
    else {
      return false;
    }
  }
  if (strMonth.length < 1 || month < 1 || month > 12) {
    return false;
  }
if (strDay.length < 1 || day < 1 || day > 31 || (month === 2 && day > daysInFebruary(year)) || day > daysInMonth(month)) {
    return false;
  }
  if (strYear.length != 4 || year === 0 || year < minYear || year > maxYear) {
    return false;
  }
  if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) === false) {
    return false;
  }
  return true;
}
//******************

function CheckTypeofVeteran() {
  var validateProfile = Page_ClientValidate('Profile');
  if (validateProfile) {
    var validateVeteranType = Page_ClientValidate('VeteranType');
    if (validateVeteranType)
      return true;
    else
      return false;
  }
  else
    return false;
}

function ValidateProfileDOB(sender, args) {
  var dateRegx = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

  args.IsValid = true;
  var SDSDdate = MilitaryHistory_GetStartDate(0).val();
  var startdate = new Date(SDSDdate);

  var DOBdate = $("#DOBTextBox").val();
  var birthdate = new Date(DOBdate);
  var rdoMilitaryService = $('[id*=rblMilitaryService]');

  if (SDSDdate != '' && SDSDdate != '__/__/____' && DOBdate != '' && DOBdate != '__/__/____') {
    if (!checkMinimumRange(DOBdate, SDSDdate, parseInt(resumeProfileCodeValues.MilitaryServiceMinimumAge, 10)) && $(rdoMilitaryService).find('input[type="radio"]:checked').val().toLowerCase() == 'yes') {
      sender.innerHTML = resumeProfileCodeValues.errorDOBServiceDutyStartDateLimit;
      args.IsValid = false;
    }
  }
  if (args.Value === '') {
    sender.innerHTML = resumeProfileCodeValues.errorDateOfBirthRequired;
    args.IsValid = false;
  }
else if (dateRegx.test(args.Value) === false || (!moment(DOBdate, "MM/DD/YYYY", true).isValid())) {
    sender.innerHTML = resumeProfileCodeValues.errorDateFormat;
    args.IsValid = false;
  } else {
    var birthStrings = args.Value.split("/");
    var birthDate = new Date(parseInt(birthStrings[2], 10), parseInt(birthStrings[0], 10) - 1, parseInt(birthStrings[1], 10));

    var age = GetAgeForDate(birthDate);
    if (age < parseInt(resumeProfileCodeValues.UnderAgeJobSeekerRestrictionThreshold)) {
      sender.innerHTML = resumeProfileCodeValues.errorDateOfBirthLimitMin;
      args.IsValid = false;
    } else if (age > 99) {
      sender.innerHTML = resumeProfileCodeValues.errorDateOfBirthLimitMax;
      args.IsValid = false;
    }
}
}

function ValidateTapAttendanceDate(sender, args) {
  var rdoMilitary = $('#rblMilitaryService');
  if ($(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
    var dateRegx = /^(0?[1-9]|1[012])(\/|-|\.)(19|20)\d\d$/;

    var attendanceDate = moment(args.Value, "MM/YYYY").startOf('month');
    var three = moment().startOf('month').subtract('years', 3);
    var today = moment();

    if (args.Value === '') {
      sender.innerHTML = resumeProfileCodeValues.errorTapAttendanceDateRequired;
      args.IsValid = false;
    }
    else if (dateRegx.test(args.Value) === false) {
      sender.innerHTML = resumeProfileCodeValues.errorTapAttendanceDateFormat;
      args.IsValid = false;
    }
    else if (!attendanceDate.isAfter(three) && !attendanceDate.isSame(three)) {
      sender.innerHTML = resumeProfileCodeValues.errorTapAttendanceDateTooEarly;
      args.IsValid = false;
    }
    else if (attendanceDate.isAfter(today)) {
      sender.innerHTML = resumeProfileCodeValues.errorTapAttendanceDateTooEarInFutureTooEarly;
      args.IsValid = false;
    }
  }
}

function EnableDisableStatus() {
  var ddlDisabilityType = $('#ddlDisabilityType');
  if ($("#ddlDisabilityStatus").val() == 'Disabled') {
      $('#cblDisabledCategory').show();
      $('#lblDisabilityCategory').show();
  }
  else {
      $('#cblDisabledCategory').hide();
      $('#lblDisabilityCategory').hide();
  }
}
//function validateIncomeTextBox(sender, args) {
//    args.IsValid = true;
//     
//        var familyIncome = $('#IncomeTextBox').val();
//        if (familyIncome !== "" && ($('#IncomeTextBox').val(formatCurrency($('#IncomeTextBox').val()))) != "0.00" ) {
//            
//            familyIncome = $('#IncomeTextBox').val().replace(/\,/g, '');

//            if ($("#ddlDisabilityStatus").val() == 'Disabled') {
//                if ((familyIncome < 850.00 || familyIncome > 29166.50)) {
//                    sender.innerHTML = resumeProfileCodeValues.errorMonthlyIncomeLimit;
//                    args.IsValid = false;
//                }
//            }
//        
//    } 
//}

function validateDisabledCategory(src, args) {
    if ($('#ddlDisabilityStatus').val() === resumeProfileCodeValues.disabilityStatusDisabled)
        args.IsValid = $('#cblDisabledCategory input:checkbox').is(':checked');
}

function validateRace(src, args) {
  args.IsValid = $('#cblRace input:checkbox').is(':checked');
}

function validateEthnicityHeritage(src, args) {
  args.IsValid = $('#rblEthnicityHeritage input:radio').is(':checked');
}

function validateDependants(src,args) {
    var dependants = $("#DependantsTextBox").val();
    if (dependants == "0"|| dependants=="00")
        args.IsValid = false;
}

//function validateMSFWQuestions(src, args) {
//  if ($('#rblMSFW').find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
//    args.IsValid = $('#cblMSFWQuestions input:checkbox').is(':checked');
//  } else {
//    args.IsValid = true;
//  }
//}

function ValidateRunawayYouth(src, args) {

    if ($('#cbHouseIssueYouth').is(":checked")) {
        var DOBdate = $("#DOBTextBox").val();
        var birthDate = new Date(DOBdate);
        var seekerage = GetAgeForDate(birthDate);
        
        if (seekerage > 18)
            args.IsValid = false;
            
    }
    else {
        args.IsValid = true;
        
    }
}


function ValidateHomelessCheckBox(src, args) {
    if ($('#cbHouseIssueHomeless').is(":checked") && $('#cbHouseIssueStaying').is(":checked")) {
        args.IsValid = false;
        
    }
    else {
        args.IsValid = true;
    }
    
}
function validateNativeLanguage(src, args) {
    if ($('#cbLowLevelLiteracy').is(":checked")) {
           if ($('#ddlNativeLanguage').val() == "")
                args.IsValid = false;
            
        
    }

    }
    function validateCommonLanguage(src, args) {
        if ($('#cbLowLevelLiteracy').is(":checked")) {
           
            if ($('#ddlCommonLanguage').val() == "")
                args.IsValid = false;

        }

    }

    function validateEmploye(src, args) {
        if ($('#cbDisplacedHomemaker').is(":checked")) {
            if ($('#ddlEmploymentStatus').val() !== "UnEmployed")
                args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }
    
function ShowHideMilitaryServiceDetail(doValidation) {
  var rdoMilitary = $('#rblMilitaryService');
  var panMilitaryService = $('#panMilitaryService');
  if ($(rdoMilitary).exists()) {
     
    if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
      $(panMilitaryService).slideDown(1000, 'easeInOutExpo');
      if (doValidation) {
      	Page_ClientValidate('VeteranType');
      }
     } else {
     	$(panMilitaryService).slideUp(1000, 'easeInOutExpo');
    }
  }
}

function ShowHideAlienRegistrationDetail() {
  var rdoUSCitizen = $('#rblUSCitizen');
  var panAlienRegistrationDetail = $('#panAlienRegistrationDetail');
  if ($(rdoUSCitizen).exists()) {
    if ($(rdoUSCitizen).find('input[type="radio"]:checked').val().toLowerCase() === 'no') {
      $(panAlienRegistrationDetail).slideDown(1000, 'easeInOutExpo');
    } else {
      $(panAlienRegistrationDetail).slideUp(1000, 'easeInOutExpo');
    }
  }
}

//function ShowHideMSFWDetail() {
//  var rblMSFW = $('#rblMSFW');
//  if ($(rblMSFW).exists()) {
//    var panMSFWDetail = $('#panMSFWDetail');
//    if ($(rblMSFW).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
//      panMSFWDetail.slideDown(1000, 'easeInOutExpo');
//    } else {
//      panMSFWDetail.slideUp(1000, 'easeInOutExpo');
//    }
//  }
//}


function ShowHouseIssueDetail() {
    var panHouseIssueDetail = $('#panHouseIssueDetail');
    panHouseIssueDetail.slideDown(1000, 'easeInOutExpo');
        return false;
    }
function ShowIncomeIssueDetail() {
        var panIncomeIssueDetail = $('#panIncomeIssueDetail');
        panIncomeIssueDetail.slideDown(1000, 'easeInOutExpo');
        return false;
    }
    function ShowLegalIssueDetail() {
        var panLegalIssueDetail = $('#panLegalIssueDetail');
        panLegalIssueDetail.slideDown(1000, 'easeInOutExpo');
        return false;
    }
    function ShowCulturalIssueDetail() {
        var panCulturalIssueDetail = $('#panCulturalIssueDetail');
        panCulturalIssueDetail.slideDown(1000, 'easeInOutExpo');
        return false;
    }
    function HideCulturalIssueDetail() {
        var panCulturalIssueDetail = $('#panCulturalIssueDetail');
        panCulturalIssueDetail.slideUp(1000, 'easeInOutExpo');
        return false;
    }
    function HideLegalIssueDetail() {
        var panLegalIssueDetail = $('#panLegalIssueDetail');
        panLegalIssueDetail.slideUp(1000, 'easeInOutExpo');
        return false;
    }

function HideIncomeIssueDetail() {
        var panIncomeIssueDetail = $('#panIncomeIssueDetail');
        panIncomeIssueDetail.slideUp(1000, 'easeInOutExpo');
        return false;
    }
function HideHouseIssueDetail() {
    var panHouseIssueDetail = $('#panHouseIssueDetail');
    panHouseIssueDetail.slideUp(1000, 'easeInOutExpo');
        return false;
}

function validateAlienRegNo(sender, args) {
  var rdoUSCitizen = $('#rblUSCitizen');
  if ($(rdoUSCitizen).find('input[type="radio"]:checked').val().toLowerCase() === 'no') {
    var RE_Alien = /^[a-zA-Z]([0-9]{8,9})$/;
    if (args.Value.length === 0) {
      sender.innerHTML = resumeProfileCodeValues.errorAlienRegistrationNumberRequired;
      args.IsValid = false;
    }
    else if (!RE_Alien.test(args.Value)) {
      sender.innerHTML = resumeProfileCodeValues.errorAlienRegistrationNumber;
      args.IsValid = false;
    }
  }
}

function validateAlienRegExpire(sender, args) {
	var rdoUSCitizen = $('#rblUSCitizen');
	var rdoPermanentResident = $('#rblPermanentResident');
  //if ($(rdoUSCitizen).find('input[type="radio"]:checked').val().toLowerCase() === 'no') {
	if ($(rdoUSCitizen).find('input[type="radio"]:checked').val().toLowerCase() === 'no'
  && ($(rdoPermanentResident).find('input[type="radio"]:checked').val().toLowerCase() === 'no' || resumeProfileCodeValues.EnableOptionalAlienRegExpiryDate == "0")) {
    var areDate = $("#txtAlienRegExpire").val();
    var expiredate = new Date(areDate);
    var curDate = new Date();
    var dateRegx = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

    if (areDate === '') {
      sender.innerHTML = resumeProfileCodeValues.errorAlienExpirationDateRequired;
      args.IsValid = false;
    }
    else if (dateRegx.test(areDate) === false) {
      sender.innerHTML = resumeProfileCodeValues.errorDateFormat;
      args.IsValid = false;
    }
    else if (expiredate < curDate) {
      sender.innerHTML = resumeProfileCodeValues.errorAlienExpirationDate;
      args.IsValid = false;
    }
  }
}

function CheckNotDisclosed(box) {
  var cblRace = $('#cblRace');
  var notDisclosed = cblRace.find('input').last();
  var anyRaces = cblRace.find('input[id != "' + notDisclosed.attr("id") + '"]');

  var anyChecked = anyRaces.is(':checked');

  if ($(box).attr('id') == notDisclosed.attr('id')) {
    if (notDisclosed.is(':checked') && anyChecked) {
      anyRaces.prop('checked', false);
    }
  } else {
    notDisclosed.prop('checked', !anyChecked);
  }

  // To Update the race custom validator display.
  var raceValidator = $('#RaceValidator');
  if ($(raceValidator).exists()) {
    $(raceValidator).get(0).isvalid = true;
    ValidatorUpdateDisplay($(raceValidator).get(0));
  }
}

//function CheckDisabledNotDisclosed(box) {
//    var cblDisabledCategory = $('#cblDisabledCategory');
//    var notDisclosed = cblDisabledCategory.find('input').last();
//    var anyDisability = cblDisabledCategory.find('input[id != "' + notDisclosed.attr("id") + '"]');

//    var anyChecked = anyDisability.is(':checked');

//    if ($(box).attr('id') == notDisclosed.attr('id')) {
//        if (notDisclosed.is(':checked') && anyChecked) {
//            anyDisability.prop('checked', false);
//        }
//    } else {
//        notDisclosed.prop('checked', !anyChecked);
//    }

//    // To Update the disability custom validator display.
//    var DisabilityValidator = $('#DisabilityValidator');
//    if ($(DisabilityValidator).exists()) {
//        $(DisabilityValidator).get(0).isvalid = true;
//        ValidatorUpdateDisplay($(DisabilityValidator).get(0));
//    }
//}

function ValidateDisabilityNotDisclosedChecked(sender, args) {
    var anyCheckbox = $('#cblDisabledCategory').find('input[id != "' + $('#cblDisabledCategory').find('input').last().attr("id") + '"]');
    if ($('#cblDisabledCategory').find('input').last().is(':checked') && anyCheckbox.is(':checked')) {
        args.IsValid = false;
    }
    else {
        args.IsValid = true;
    }
}

function ValidateEmploymentStatusofUser(sender, args) {
  var empStatus = $('#ddlEmploymentStatus');
  var empStatusValue = $(empStatus).val();
  var militaryEmpStatus = MilitaryHistory_GetMilitaryEmploymentStatus(0);
  if ($("#rblMilitaryService").find('input[type="radio"]:checked').length > 0) {
      var rdoMilitaryService = $("#rblMilitaryService").find('input[type="radio"]:checked').val().toLowerCase();
  }

  //var rdoMilitaryService = $('#rblMilitaryService').find('input[type="radio"]:checked').val().toLowerCase();
  var veteran = MilitaryHistory_GetVeteranType(0);
  var veterantype = $(veteran).val();

  if (resumeProfileCodeValues.isCurrentJob === '1' && empStatusValue == resumeProfileCodeValues.employmentStatusUnemployed) {
    sender.innerHTML = resumeProfileCodeValues.errorEmploymentIsOtherJob;
    args.IsValid = false;
  }

  if ($(empStatus).get(0).selectedIndex === 0) {
    sender.innerHTML = resumeProfileCodeValues.errorEmploymentStatusRequired;
    args.IsValid = false;
  }
  else if (veterantype == resumeProfileCodeValues.veteranEraTransitioningServiceMember && empStatusValue == resumeProfileCodeValues.employmentStatusUnemployed) {
    sender.innerHTML = resumeProfileCodeValues.errorEmploymentStatusNotUnemployed;
    args.IsValid = false;
  }
  else if (rdoMilitaryService === 'yes' && $(militaryEmpStatus).is(':disabled') == false && $(militaryEmpStatus).get(0).selectedIndex != 0 && $(empStatus).get(0).selectedIndex != $(militaryEmpStatus).get(0).selectedIndex) {
    sender.innerHTML = resumeProfileCodeValues.errorEmploymentStatusMismatch;
    args.IsValid = false;
  }
}

function toggleDiv() {
  if (resumeProfileCodeValues.SelectiveServiceRegistration === 'True') {

    var usCitizen = $("#rblUSCitizen").find(":checked").val();
    var dob = $('#DOBTextBox').val();
    var gender = $('#ddlGender').val();

    if (dob != '' && gender != '') {
      if (checkAgeInRange(dob, 18, 26) && gender === 'Male' && usCitizen === 'yes') {
        ValidatorEnable($('#SelectiveServiceRequired').get(0), true);
        $('#SelectiveServiceDiv').show();
      }
      else {
        hideSelectiveService();
      }
    }
    else {
      hideSelectiveService();
    }
  }
  else {
    hideSelectiveService();
  }
}

function hideSelectiveService() {
  var selectiveServiceDiv = $('#SelectiveServiceDiv');
  if (selectiveServiceDiv.exists()) {
    $("table[id$=rblSelectiveService] input:radio:checked").removeAttr("checked");
    ValidatorEnable($('#SelectiveServiceRequired').get(0), false);
    selectiveServiceDiv.hide();
  }
}

function checkAgeInRange(dobString, minAge, maxAge) {
  var dobMoment = moment(dobString, ['MM/DD/YYYY', 'DD/MM/YYYY']);
  var todayMoment = moment();
  var age = todayMoment.diff(dobMoment, 'years');
  return (age >= minAge && age < maxAge);
}

function checkMinimumRange(dobString, compareString, minAge) {
  var dobMoment = moment(dobString, ['MM/DD/YYYY', 'DD/MM/YYYY']);
  var todayMoment = moment(compareString);
  var age = todayMoment.diff(dobMoment, 'years');
  return (age >= minAge);
}


function validateMilitaryService(sender, args)
 {
  var rdoMilitary = $('#rblMilitaryService');
      if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0)
      { args.IsValid = false; }

      else {
          args.IsValid = true;
        }
  
  
 }