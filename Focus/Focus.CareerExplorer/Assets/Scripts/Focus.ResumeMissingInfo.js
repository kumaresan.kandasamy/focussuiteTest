﻿// JS validation functions for resume missing info modal
// =====================================================

// initialise variable that will be dynamically populated with code values
var resumeMissingInfoCodeValues;

$(document).ready(function () {
	Sys.Application.add_load(ResumeMissingInfo_PageLoad);
});

function ResumeMissingInfo_PageLoad(sender, args) {
	var isCounty = resumeMissingInfoCodeValues.isCountyEnabled;
	if (isCounty === 'True') {
		var behavior = $find(resumeMissingInfoCodeValues.ccdCountyBehaviorId);
		if (behavior != null) {
			behavior.add_populated(function () {
				var countyDropDown = $('#ddlCounty');
				var stateDropDown = $("#ddlState"); ;

				if ($('#ddlCounty option:selected').val() != '')
					countyDropDown.next().find(':first-child').text($('#ddlCounty option:selected').text()).parent().addClass('changed');
				else
					SetCounty(stateDropDown.val() === resumeMissingInfoCodeValues.stateCodeOutsideUS);

				UpdateStyledDropDown(countyDropDown, stateDropDown.val());
			});
		}
	}
}

function DriverLicenseChange() {
	var licenseDropDown = $('#DriverLicenseDropDown');
	var license = $(licenseDropDown).val();
	var licenseStateDropDown = $('#DriverLicenseStateDropDown');
	var licenseTypeDropDown = $('#DriverLicenseTypeDropDown');

	if (licenseDropDown.length == 0) {
		return true;
	}

	if (license != 'No' && license != '- select -') {
		if (license === 'Yes') {
			if (!$(licenseStateDropDown).exists() || $(licenseStateDropDown).get(0).selectedIndex === 0) {
				$(licenseStateDropDown).val(resumeMissingInfoCodeValues.userState);
				$(licenseStateDropDown).next().find(':first-child').text($(licenseStateDropDown).children('option:selected').text()).parent().addClass('changed');
			}

			if ($(licenseTypeDropDown).get(0).selectedIndex === 0) {
				$(licenseTypeDropDown).val(resumeMissingInfoCodeValues.drivingLicenseCodeDefault);
				$(licenseTypeDropDown).next().find(':first-child').text($(licenseTypeDropDown).children('option:selected').text()).parent().addClass('changed');

				var displayLicense = $('#HideLicenseCheckBox');
				$(displayLicense).find('input[type="checkbox"]:first').prop('disabled', false);
				$(displayLicense).removeAttr('style');
			}
		}

		if ($(licenseStateDropDown).exists()) {
			EnableStyledDropDown(licenseStateDropDown);
		}

		EnableStyledDropDown(licenseTypeDropDown);
	}
	else {
		DisableStyledDropDown(licenseStateDropDown, true);
		DisableStyledDropDown(licenseTypeDropDown, true);
	}

	DriverLicenseTypeChange();

	return false;
}

//Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
//function EndRequestHandler(sender, args) {
//	if (args.get_error() == undefined) {
//		DriverLicenseChange();
//	}
//}

function ApplyEndorsementsVisualCue(endorsementElement, isEnabled) {
	if ($(endorsementElement).exists()) {
		if (isEnabled)
			$(endorsementElement).removeAttr('style');
		else
			$(endorsementElement).attr('style', 'color:gray;');
	}
}

function DriverLicenseTypeChange() {
	var driverLicense = $('#DriverLicenseTypeDropDown');
	var endorsementTypes = $('#EndorsementCheckBoxList');
	var displayLicense = $('#HideLicenseCheckBox');

	if (driverLicense.length == 0) {
		return true;
	}

	if ($(driverLicense).get(0).selectedIndex != -1) {
		var driverLicenseValue = $(driverLicense).val();
		var arrayOfEndorsementTypes = $(endorsementTypes).find('input');
		var displayLicenseCheck = $(displayLicense).find('input:first');

		if (driverLicenseValue === '0' || $(driverLicense).get(0).selectedIndex === 0 || driverLicenseValue === resumeMissingInfoCodeValues.drivingLicenseCodeClassD) {

			if (driverLicenseValue === resumeMissingInfoCodeValues.drivingLicenseCodeClassD) {
				$(displayLicenseCheck).prop('disabled', false);
				$(displayLicense).removeAttr('style');
			} else {
				$(displayLicenseCheck).prop('checked', false).prop('disabled', true);
				$(displayLicense).attr('style', 'color:gray;');
			}
		} else {
			$(displayLicenseCheck).prop('disabled', false);
			$(displayLicense).removeAttr('style');
		}

		var validEndorsements = licenceLicenceEndorsements[driverLicenseValue];

		if (!validEndorsements)
			validEndorsements = '';

		$(arrayOfEndorsementTypes).each(function () {
			if (validEndorsements.indexOf($(this).val()) != -1) {
				$(this).prop('disabled', false);
				ApplyEndorsementsVisualCue($(this).parent(), true);
				return true;
			}
			$(this).prop('checked', false);
			$(this).prop('disabled', true);
			ApplyEndorsementsVisualCue($(this).parent(), false);
		});
	}
	return false;
}

function ValidateDriverState(src, args) {
	var index = $('#DriverLicenseDropDown').get(0).selectedIndex;
	if (index == 1) {
		args.IsValid = ($('#DriverLicenseStateDropDown').val() != '');
	}
}

function ValidateDriverLicense(src, args) {
	var index = $('#DriverLicenseDropDown').get(0).selectedIndex;
	args.IsValid = (index != 0);
}

function ValidateLicenseType(src, args) {
	var index = $('#DriverLicenseDropDown').get(0).selectedIndex;
	if (index == 1) {
		args.IsValid = ($('#DriverLicenseTypeDropDown').get(0).selectedIndex != 0);
	}
}

function ChangePhoneNoFormat() {
	var phoneType = $('#PhoneNoDropDownList');
	var phoneTextBox = $('#PhoneNoTextBox');
	if ($(phoneType).exists())
		var phonevalue = $(phoneType).val();

	var behaviorPhone = $find('meBehaviourPhoneNo');
	if (phonevalue != 'NonUS') {
		$("#PhoneNoTextBox").mask('(999) 999-9999');
	} else {
    var value = $('#PrimaryContactPhoneNoTextBox').val();
        value = value.replace(/\(|\-|\)|\s/g, '');
        $('#PhoneNoTextBox').unmask();
        $('#PhoneNoTextBox').val(value);		
	}
    $(phoneTextBox).focus();
	ValidatorValidate($('#PhoneNoValidator').get(0));
}

function ValidatePhoneNo(sender, args) {
	var phoneRegx = new RegExp(resumeMissingInfoCodeValues.phoneNumberRegExPattern);
	var phone = $('#PhoneNoDropDownList');
	if ($(phone).exists())
		var phoneValue = $(phone).val();

	if (phoneValue != 'NonUS') {
		if (args.Value === '') {
			sender.innerHTML = sender.errormessage = resumeMissingInfoCodeValues.errorPhoneNumberRequired;
			args.IsValid = false;
		} else if (phoneRegx.test(args.Value) === false) {
			sender.innerHTML = sender.errormessage = resumeMissingInfoCodeValues.errorPhoneNumberFormat;
			args.IsValid = false;
		}
	} else if ($('#PhoneNoTextBox').val() === '                    ') {
		sender.innerHTML = sender.errormessage = resumeMissingInfoCodeValues.errorPhoneNumberRequired;
		args.IsValid = false;
	}
	if (args.IsValid === true && phoneValue === '') {
		sender.innerHTML = sender.errormessage = resumeMissingInfoCodeValues.errorPhoneTypeRequired;
		args.IsValid = false;
	}
}

function UpdateCountry() {
	var stateValue = $('#ddlState').val();
	var zipRequired = $('#ZipRequired');

	if ($(zipRequired).exists()) {
		$(zipRequired).get(0).enabled = (stateValue != resumeMissingInfoCodeValues.stateCodeOutsideUS);
	}

	SetCountry(stateValue === resumeMissingInfoCodeValues.stateCodeOutsideUS);
}

function UpdateStateCounty() {
	var isCounty = resumeMissingInfoCodeValues.isCountyEnabled;
	var countryValue = $('#ddlCountry').val();
	var stateValue = $('#ddlState').val();
	var zipRequired = $('#ZipRequired');

	if (countryValue === resumeMissingInfoCodeValues.countryCodeUS) {
		if (stateValue === resumeMissingInfoCodeValues.stateCodeOutsideUS)
			SetState(false);

		if (isCounty === 'True')
			SetCounty(false);

		if ($(zipRequired).exists())
			$(zipRequired).get(0).enabled = true;
	} else {
		SetState(true);

		if (isCounty === 'True')
			SetCounty(true);

		if ($(zipRequired).exists())
			$(zipRequired).get(0).enabled = false;
	}
}

function UpdateStateCountry() {
	var countyValue = $('#ddlCounty').val();
	var stateValue = $('#ddlState').val();

	if (countyValue === '0') {
		if (stateValue != resumeMissingInfoCodeValues.stateCodeOutsideUS)
			SetState(true);
		SetCountry(true);
	}
}

function SetState(outsideUS) {
	var state = $('#ddlState');

	if (outsideUS) {
		$(state).children('option[value="' + resumeMissingInfoCodeValues.stateCodeOutsideUS + '"]').prop('selected', true);
	}
	else {
		$(state).children('option:first').prop('selected', true);
	}
	$(state).next().find(':first-child').text($(state).children('option:selected').text()).parent().addClass('changed');
}

function SetCounty(outsideUS) {
	var county = $('#ddlCounty');

	if (outsideUS) {
		$(county).children('option[value="0"]').prop('selected', true);
	} else {
		$(county).children('option:first').prop('selected', true);
	}
	$(county).next().find(':first-child').text($(county).children('option:selected').text()).parent().addClass('changed');
}

function SetCountry(outsideUS) {
	var country = $('#ddlCountry');

	if (outsideUS) {
		$(country).children('option:first').prop('selected', true);
	}
	else {
		$(country).children('option[value="' + resumeMissingInfoCodeValues.countryCodeUS + '"]').prop('selected', true);
	}
	$(country).next().find(':first-child').text($(country).children('option:selected').text()).parent().addClass('changed');
}

function ValidateEnrolmentEducationLevel(src, args) {
	var schoolStatus = $('#ddlEnrollmentStatus').val();
	var education = $('#ddlEducationLevel');
	var educationLevel = $(education).val();
	educationLevel = educationLevel.substr(educationLevel.length - 2);

	if ($(education).get(0).selectedIndex === 0) {
		src.innerHTML = src.errormessage = resumeMissingInfoCodeValues.errorEducationLevelRequired;
		args.IsValid = false;
	}
	else if ((schoolStatus === resumeMissingInfoCodeValues.schoolStatusPostHS || schoolStatus === resumeMissingInfoCodeValues.schoolStatusHSGraduate) && educationLevel <= 12) {
		src.innerHTML = src.errormessage = resumeMissingInfoCodeValues.errorEnrollmentStatus;
		args.IsValid = false;
	}
	else if ((schoolStatus === resumeMissingInfoCodeValues.schoolStatusHS || schoolStatus === resumeMissingInfoCodeValues.schoolStatusNotAttendingOrDropOut || schoolStatus === resumeMissingInfoCodeValues.schoolStatusAlternative) && educationLevel > 12) {
		src.innerHTML = src.errormessage = resumeMissingInfoCodeValues.errorEducationLevel;
		args.IsValid = false;
	}
}

function ValidateZipCode(sender, args) {
	var country = $('#ddlCountry');
	var countryValue = ($(country).exists() ? $(country).val() : '');

	if (args.Value.length === 0) {
		sender.innerHTML = sender.errormessage = resumeMissingInfoCodeValues.errorZipCodeRequired;
		args.IsValid = false;
	}
	else if (countryValue === resumeMissingInfoCodeValues.countryCodeUS && (args.Value.length != 9 && args.Value.length != 5)) {
		sender.innerHTML = sender.errormessage = resumeMissingInfoCodeValues.errorZipCodeLimit;
		args.IsValid = false;
	}
}

function UpdateCountyAndTextCounter() {
	var county = $('#ddlCounty');
	if ($(county).exists())
		$(county).next().find(':first-child').text($(county).children('option:selected').text()).parent().addClass('changed');

	textCounter();
}