﻿// JS validation functions for resume add a job tab
// ================================================

// initialise variable that will be dynamically populated with code values
var resumeAddJobCodeValues;

function bindEvents() {
	setEnabled();
	OnDateUpdate(true);
	$('#txtJobTitle').on('blur', function () {
		chkJobTitle();
	});
	$('#ddlRank').on('change', function () {
		setRankHiddenField();
	});
	$('#ddlState').on('change', function () {
		UpdateContactCountryCode();
	});
	$('#ddlCountry').on('change', function () {
		UpdateContactStateCode();
	});
	$('#txtWages').on('keyup', function () {
		setEnabled();
	});
	$('#txtWages').on('blur', function () {
		$(this).val(formatCurrency($(this).val()));
	});
}


$(document).ready(function () {
	$('#ddlEmployer').unbind('click').change(setEmployer);
	$('#txtWages').val(formatCurrency($('#txtWages').val()));
	bindEvents();
});

function AddAJob_Wages() {
	var flag = Page_ClientValidate('Wages');
	return flag;
}

function validateAll() {
	var isValid = false;
	try {
		isValid = Page_ClientValidate("Wages");
		if (!isValid) return false;
	}
	catch (e) { }
	try {
		isValid = Page_ClientValidate("AddAJob");
		if (!isValid) return false;
	}
	catch (e) { }

	return isValid;
}

function validatePayUnit(sender, args) {
	var payUnitDDL = $('#ddlPayUnit');
	var payUnitValue = $(payUnitDDL).val();
	var money = $('#txtWages').val().replace(/\,/g, '');

	if (money !== "") {
		if ($(payUnitDDL).get(0).selectedIndex === 0) {
			sender.innerHTML = resumeAddJobCodeValues.errorPayUnitRequired;
			args.IsValid = false;
		}
		else if (payUnitValue === resumeAddJobCodeValues.frequencyHourly && (money < 4.90 || money > 168.25)) {
			sender.innerHTML = resumeAddJobCodeValues.errorHourlyLimit;
			args.IsValid = false;
		}
		else if (payUnitValue === resumeAddJobCodeValues.frequencyDaily && (money < 39.25 || money > 1346.00)) {
			sender.innerHTML = resumeAddJobCodeValues.errorDailyLimit;
			args.IsValid = false;
		}
		else if (payUnitValue === resumeAddJobCodeValues.frequencyWeekly && (money < 196.00 || money > 6730.75)) {
			sender.innerHTML = resumeAddJobCodeValues.errorWeeklyLimit;
			args.IsValid = false;
		}
		else if (payUnitValue === resumeAddJobCodeValues.frequencyMonthly && (money < 850.00 || money > 29166.50)) {
			sender.innerHTML = resumeAddJobCodeValues.errorMonthlyLimit;
			args.IsValid = false;
		}
		else if (payUnitValue === resumeAddJobCodeValues.frequencyYearly && (money < 10192.00 || money > 350000.00)) {
			sender.innerHTML = resumeAddJobCodeValues.errorYearlyLimit;
			args.IsValid = false;
		}
	}
	else {
		if ($(payUnitDDL).get(0).selectedIndex !== 0) {
			sender.innerHTML = resumeAddJobCodeValues.errorWages;
			args.IsValid = false;
		}
	}
}

function OnDateUpdate(isLoad) {
	var endDateTextBox = $('#EndDateTextBox');
	var inputRowEndDate = $('#inputRowEndDate');
    var ReasonForLeavingDropDown = $('#ddlReasonForLeaving');
	if ($('#chkCurrentlyWorkHere').is(':checked')) {
	    DisableStyledTextBox(endDateTextBox, '');
        //Disable Reason For Leaving Drop Down
        DisableStyledDropDown(ReasonForLeavingDropDown, true);
        if ($('#ddlReasonForLeaving').is(':disabled')) {
            ValidatorEnable($("[id$='ReasonForLeavingRequired']").get(0), false);
        }
		if (isLoad) {
			$(inputRowEndDate).hide();
		}
		else {
			$(inputRowEndDate).slideUp(500);
		}

        var endDateValidator = $('#EndDateValidator');
        $(endDateValidator).get(0).isvalid = true;
        ValidatorUpdateDisplay($(endDateValidator).get(0));
    }
    else {
        $(inputRowEndDate).slideDown(500, function () {
            EnableStyledTextBox(endDateTextBox);
            //Enable Reason For Leaving Drop Down
            EnableStyledDropDown(ReasonForLeavingDropDown);
            if (!isLoad) {
                $("[id$='ReasonForLeavingRequired']").get(0).enabled = true;
            }
        });
    }
}

function ValidateStartDate(sender, args) {
    var datePattern = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

    var ccur_date = new Date();
    var ccur_day = (ccur_date.getDate()).valueOf();
    var ccur_year = (ccur_date.getFullYear()).valueOf();
    var ccur_month = (ccur_date.getMonth()).valueOf() + 1;

    if (args.Value.indexOf("/") > -1) {
        var data = args.Value.valueOf();
        var date = data.split("/");
        var month = date[0];
        var day = date[1];
        var year = date[2];

        if (args.Value === '') {
            sender.innerHTML = resumeAddJobCodeValues.errorStartDateRequired;
            args.IsValid = false;
        }
        else if (datePattern.test(args.Value) === false || (!moment(args.Value, "MM/DD/YYYY", true).isValid())) {
            sender.innerHTML = resumeAddJobCodeValues.errorDateFormat;
            args.IsValid = false;
        }
        else if (year === ccur_year) {
            if (month > (ccur_month + 1)) {
                sender.innerHTML = resumeAddJobCodeValues.errorFutureDate;
                args.IsValid = false;
            }
        }
        else if ((ccur_year - year).valueOf() > 100 || ((ccur_year - year).valueOf() === 100 && month < ccur_month)) {
            sender.innerHTML = resumeAddJobCodeValues.errorYearLimit;
            args.IsValid = false;
        }
        else if (year > ccur_year) {
            sender.innerHTML = resumeAddJobCodeValues.errorDateMax;
            args.IsValid = false;
        }
        else if (ccur_year == year) {
            if (month > ccur_month) {
                sender.innerHTML = resumeAddJobCodeValues.errorDateMax;
                args.IsValid = false;
            }
        }
    }
    else {
        if (args.Value === '') {
            sender.innerHTML = resumeAddJobCodeValues.errorStartDateRequired;
            args.IsValid = false;
        }
    }
}

function ValidateEndDate(sender, args) {
    if (!$('#chkCurrentlyWorkHere').is(':checked')) {
        var datePattern = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

        var ccur_date = new Date();
        var ccur_day = (ccur_date.getDate()).valueOf();
        var ccur_year = (ccur_date.getFullYear()).valueOf();
        var ccur_month = (ccur_date.getMonth()).valueOf() + 1;

        if (args.Value.indexOf("/") > -1) {
            var data = args.Value.valueOf();
            var date = data.split("/");
            var month = date[0];
            var day = date[1];
            var year = date[2];

            if (args.Value === '') {
                sender.innerHTML = resumeAddJobCodeValues.errorEndDateRequired;
                args.IsValid = false;
            }
            else if (datePattern.test(args.Value) === false || (!moment(args.Value, "MM/DD/YYYY", true).isValid())) {
                sender.innerHTML = resumeAddJobCodeValues.errorDateFormat;
                args.IsValid = false;
            }
            else if (year === ccur_year) {
                if (month > (ccur_month + 1)) {
                    sender.innerHTML = resumeAddJobCodeValues.errorFutureDate;
                    args.IsValid = false;
                }
            }
            else if ((ccur_year - year).valueOf() > 100 || ((ccur_year - year).valueOf() === 100 && month < ccur_month)) {
                sender.innerHTML = resumeAddJobCodeValues.errorYearLimit;
                args.IsValid = false;
            }
            else if (year > ccur_year) {
                sender.innerHTML = resumeAddJobCodeValues.errorDateMax;
                args.IsValid = false;
            }
            else if (ccur_year == year) {
                if (month > ccur_month) {
                    sender.innerHTML = resumeAddJobCodeValues.errorDateMax;
                    args.IsValid = false;
                }
            }
        }
        else {
            sender.innerHTML = resumeAddJobCodeValues.errorEndDateRequired;
            args.IsValid = false;
        }
    }
}

function setEnabled() {
	var wages = $('#txtWages').val();
	var payUnit = $('#ddlPayUnit');

	if (wages) {
		EnableStyledDropDown(payUnit);
	}
	else {
		DisableStyledDropDown(payUnit, false);
	}
}

function ValidateJobDate(sender, args) {
    if (!$('#chkCurrentlyWorkHere').is(':checked')) {
        var sDate = $('#StartDateTextBox').val();
        var eDate = $('#EndDateTextBox').val();

        sDate = sDate.split('/');
        var sMonth = sDate[0];
        var sDay = sDate[1];
        var sYear = sDate[2];

        eDate = eDate.split('/');
        var eMonth = eDate[0];
        var eDay = eDate[1];
        var eYear = eDate[2];

        if (sDate !== '' && sDay !== '' && eDate !== '' && eDay !== '' && eMonth !== '' && eYear !== '') {
            if (eYear === sYear && eMonth < sMonth) {
                sender.innerHTML = resumeAddJobCodeValues.errorEndMonth;
                args.IsValid = false;
            }
            else if (eYear < sYear) {
                sender.innerHTML = resumeAddJobCodeValues.errorEndYear;
                args.IsValid = false;
            }
            else if (eYear == sYear && eMonth == sMonth && eDay < sDay) {
                sender.innerHTML = resumeAddJobCodeValues.errorEndDay;
                args.IsValid = false;
            }
        }
    }
}

function UpdateContactCountryCode() {
	var statevalue = $('#ddlState').val();
	var country = $('#ddlCountry');
	var countryvalue = $(country).val();

	if (statevalue === resumeAddJobCodeValues.stateCodeOutsideUS && countryvalue === resumeAddJobCodeValues.countryCodeUS) {
		UpdateStyledDropDown(country);
	}
	if (statevalue != resumeAddJobCodeValues.stateCodeOutsideUS && statevalue != ' ' && countryvalue != resumeAddJobCodeValues.countryCodeUS) {
		UpdateStyledDropDown(country, resumeAddJobCodeValues.countryCodeUS);
	}
}

function UpdateContactStateCode() {
	var state = $('#ddlState');
	var statevalue = $(state).val();
	var countryvalue = $('#ddlCountry').val();

	if (statevalue != resumeAddJobCodeValues.stateCodeOutsideUS && countryvalue != resumeAddJobCodeValues.countryCodeUS) {
		UpdateStyledDropDown(state, resumeAddJobCodeValues.stateCodeOutsideUS);
	}
	if (statevalue === resumeAddJobCodeValues.stateCodeOutsideUS && countryvalue === resumeAddJobCodeValues.countryCodeUS) {
		UpdateStyledDropDown(state);
	}
}

function chkJobTitle() {
	if (firstTitleCheck()) {
		setTitleCheckFalse();
		return;
	}
  var jobTitle = $('#txtJobTitle');
  if (jobTitle.length > 0 && jobTitle.val().length > 0) {
		var txtEmployer = $('#txtEmployer');
		var hideRank = $('#hiderank');
		var branchOfServiceHidden = $('#BranchOfServiceHidden');
		hideRank.val('');
		branchOfServiceHidden.val('');
		disableEmployer(false);
		$.ajax({
			type: 'POST',
			url: resumeAddJobCodeValues.urlAjaxService + '/GetJobEmployer',
			data: '{"jobTitle":"' + $('#txtJobTitle').val() + '"}',
			processData: false,
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			success: function (data) {
				var multipleEmployer = false;
				if (data.d != null) {
					if (data.d === 'SPECIAL_JOB_EMPLOYER') {
						$(txtEmployer).val('Self');
						$(hideRank).hide();
						$('#BranchOfServiceHidden').val('');
						$('#hdnRank').val('');
						$('#txtEmployer').show();
					}
					else if (data.d.length > 0) {
						if (data.d.indexOf('$$') > 0) {
							var boss = data.d.split('$$');
							buildEmployerList(boss);
							multipleEmployer = true;
							$('#txtEmployer').hide();
						}
						else {
							var employerParts = data.d.split('||');
							$(txtEmployer).val(employerParts[1]);
							populateRank(employerParts[0]);
							branchOfServiceHidden.val(employerParts[0]);
							$('#txtEmployer').show();
						}
					}
					else {
						$(hideRank).hide();
						$(txtEmployer).val('');
						$('#txtEmployer').show();
						$('#BranchOfServiceHidden').val('');
						$('#hdnRank').val('');
					}
				}
				else {
					$(hideRank).hide();
					$('#txtEmployer').show();
					$('#BranchOfServiceHidden').val('');
					$('#hdnRank').val('');
				}

				if (!multipleEmployer) {
					$('#ddlEmployer').empty();
					$('#hideemployer').hide();
				}
			}
		});
	}
}

function buildEmployerList(employers) {
	clearEmployer();
	$.each(employers, function (i, employer) {
		var employerParts = employer.split("||");
		$('#ddlEmployer').append($('<option>', {
			value: employerParts[0],
			text: employerParts[1]
		}));
	});
}

function disableEmployer(status) {
	$('#txtEmployer').prop('disabled', status);
}

function populateRank(emp) {
	disableEmployer(true);
	clearRank();
	var ddlRan = $('#ddlRank');
	$.ajax({
		type: 'POST',
		url: resumeAddJobCodeValues.urlAjaxService + '/GetRankList',
		data: '{"branchOfServiceId":' + emp + '}',
		processData: false,
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function (data) {
			var results = data.d;
			$.each(results, function (i, result) {
				var res = result.split('~');
				$(ddlRan).append($('<option>', {
					value: res[0],
					text: res[1]
				}));
			});
			var hiddenvalue = $('#hdnRank').val();
			if (hiddenvalue != null && hiddenvalue !== "") {
				$(ddlRan).val(hiddenvalue);
				$(ddlRan).next().find(':first-child').text($(ddlRan).children('option:selected').text()).parent().addClass('changed');
			}
		}
	});
}

function clearEmployer() {
	$('#hideemployer').show();
	var employerSelect = $('#ddlEmployer');
	$(employerSelect).empty().append('<option value="">- select branch -</option>');
	$(employerSelect).next().find(':first-child').text('- select branch -').parent().addClass('changed');
	$('#txtEmployer').val('');
}

function clearRank() {
	$('#hiderank').show();
	var rankSelect = $('#ddlRank');
	$(rankSelect).empty().append('<option value="none" selected>- select rank -</option>');
	$(rankSelect).next().find(':first-child').text($(rankSelect).children('option:selected').text()).parent().addClass('changed');
}

function setEmployer() {
	$('#hdnRank').val('');
	var selectedValue = $('#ddlEmployer').val();
	var text = $("#ddlEmployer :selected").text() 
	$('#txtEmployer').val(text);

	if (selectedValue === '') {
		disableEmployer(false);
		clearRank();
	}
	else {
		populateRank(selectedValue);
		$('#BranchOfServiceHidden').val(selectedValue);
	}
}

function setRankHiddenField() {
	var selectedValue = $('#ddlRank').val();
	$('#hdnRank').val(selectedValue);
}

function firstTitleCheck () {
	return document.getElementById('firstTitleCheck').value == 'True';
}

function setTitleCheckFalse () {
	$('#firstTitleCheck').val('false');
}