﻿var IE = false, FF = false, CR = false;
var IE_VERSION;

$(document).ready(function () {
	IE = (navigator.appName.indexOf('Microsoft Internet Explorer') != -1);
	if (IE) {
		IE_VERSION = (function () {
			var undef, v = 3, div = document.createElement('div'), all = div.getElementsByTagName('i');
			if ($('html').hasClass('cssgradients'))
				return 10;
			while (div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
				all[0]);
			return v > 4 ? v : undef;
		} ());
	}
	FF = (navigator.appName.indexOf('Netscape') != -1);
	CR = false;
	if (FF && (navigator.appVersion.indexOf('KHTML') != -1)) {
		CR = true;
		FF = false;
	}
});

function AcknowledgeStatementChange() {
	$('#txtJobDescription').effect("highlight", { color: '#FCE8C2' }, 2000);
}

function AddSelectedStatements() {
	var storeKeywords = '';
	var statementsChanged = false;

	var txtJobDescription = $('#txtJobDescription');

	if ($('#chkStatements').exists()) {
		var chkBoxStatements = $('#chkStatements');
		$(chkBoxStatements).children('input:checked').each(function () {
			if (!($(this).is(':disabled'))) {
				if ($(txtJobDescription).val().indexOf($(this).next('label').html()) < 0) {
					$(txtJobDescription).val($(txtJobDescription).val() + '\n* ' + $(this).next('label').html().replace(/\&amp;/g, '&'));
					$(this).prop('disabled', true);
					statementsChanged = true;
				}
			}
		});
	}

	var keywordsHeading = '\r\n* Knowledge sets include: ';
	var selectedText = '';
	var newLine = '\r\n';
	if (FF || CR || (IE && IE_VERSION >= 8))
		newLine = '\n';
	else if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent))
		newLine = '\n';
	if ($('#chkKeywords').exists()) {
		var chkBoxKeywords = $('#chkKeywords');
		$(chkBoxKeywords).children('input:checked').each(function () {
			if (!($(this).is(':disabled'))) {
				storeKeywords = '';
				// TODO use regex instead of the below!
				if ($(txtJobDescription).val().indexOf(': ' + $(this).next('label').html() + ',') < 0 && $(txtJobDescription).val().indexOf(', ' + $(this).next('label').html() + ',') < 0 && $(txtJobDescription).val().indexOf(': ' + $(this).next('label').html() + '.') < 0 && $(txtJobDescription).val().indexOf(', ' + $(this).next('label').html() + '.') < 0) {
					selectedText = $(this).next('label').html().replace(/\&amp;/g, '&');
					var knowledgeSetsText = '* Knowledge sets include:';
					if ($(txtJobDescription).val().indexOf(knowledgeSetsText) != -1) {
						var jobDescription = $(txtJobDescription).val();
						var afterData = jobDescription.substring(jobDescription.indexOf(knowledgeSetsText), jobDescription.length);
						if (storeKeywords === '') {
							storeKeywords = TrimText(afterData.split(newLine)[0]);
							if (storeKeywords != '' && storeKeywords.substring(storeKeywords.length - 1, storeKeywords.length) === '.')
								storeKeywords = storeKeywords.substring(0, storeKeywords.length - 1);
						}
						selectedText = storeKeywords + ', ' + selectedText;
						if ($(txtJobDescription).val().indexOf(newLine + TrimText(storeKeywords) + '.') >= 0)
							$(txtJobDescription).val($(txtJobDescription).val().replace(newLine + TrimText(storeKeywords) + '.', ''));
						else
							$(txtJobDescription).val($(txtJobDescription).val().replace(newLine + TrimText(storeKeywords), ''));

						$(txtJobDescription).val($(txtJobDescription).val() + '\r\n' + TrimText(selectedText) + '.');
					}
					else {
						selectedText = keywordsHeading + selectedText + '.';
						$(txtJobDescription).val($(txtJobDescription).val() + selectedText);
					}
				}
				$(this).prop('disabled', true);
				statementsChanged = true;
			}
		});
	}

	if (statementsChanged)
		AcknowledgeStatementChange();

	return false;
}