﻿var resumeMilitaryHistoryCodeValues;

function MilitaryHistory_PageLoad() {
    var index = 0;
    var found = true;
    while (found) {
        var behavior = $find("ccdRank_" + index);
        if (behavior != null) {
            behavior.add_populated(function (sender) {
                var rowNumber = sender._id.substring(sender._id.lastIndexOf("_") + 1);
                var rank = MilitaryHistory_GetRank(rowNumber);
                rank.next().find(':first-child').text(rank.find('option:selected').text()).parent().addClass('changed');
                rank.next().removeClass('stateDisabled');
            });
            index++;
        } else {
            found = false;
        }
    }
    MilitaryHistory_UpdateStyles();
}

function MilitaryHistory_UpdateStyles() {
    var index = 0;
    var found = true;
    while (found) {
        var veteranType = MilitaryHistory_GetVeteranType(index);
        if (veteranType != null && veteranType.exists()) {
            UpdateVeteranFields(index);
            index++;
        } else {
            found = false;
        }
    }
}

function SetContextKey() {
    //$find(resumeMilitaryHistoryCodeValues.aceMilitaryOccupationTitleClientId).set_contextKey($('ddlBranchOfService').val());
}

function MilitaryHistory_ExtractRowNumber(sender) {
    var senderId = $(sender).attr("id");
    return senderId.substring(senderId.lastIndexOf("_") + 1);
}

function MilitaryHistory_BuildId(clientId, rowNumber) {
    return resumeMilitaryHistoryCodeValues.MilitaryHistoryRepeaterClientId + "_" + clientId + "_" + rowNumber;
}

function MilitaryHistory_GetVeteranType(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ddlVeteranType', rowNumber));
}

function MilitaryHistory_GetServiceDisability(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ddlServiceDisability', rowNumber));
}

function MilitaryHistory_GetServiceDisabilityValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custServiceDisability', rowNumber));
}

function MilitaryHistory_GetMilitaryEmploymentStatus(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ddlMilitaryEmploymentStatus', rowNumber));
}

function MilitaryHistory_GetMilitaryEmploymentStatusValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custMilitaryEmploymentStatus', rowNumber));
}

function MilitaryHistory_GetBranchOfService(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ddlBranchOfService', rowNumber));
}

function MilitaryHistory_GetTransitioningType(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ddlTransitioningType', rowNumber));
}

function MilitaryHistory_GetTransitioningTypeValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custTransitioningType', rowNumber));
}

function MilitaryHistory_GetRank(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ddlRank', rowNumber));
}

function MilitaryHistory_GetStartDate(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('SDSDTextBox', rowNumber));
}

function MilitaryHistory_GetStartDateValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custSDSD', rowNumber));
}

function MilitaryHistory_GetMilitaryOccupationTitle(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('txtMilitaryOccupationTitle', rowNumber));
}

function MilitaryHistory_GetPlannedEndDateLabel(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('lblPlannedEndDate', rowNumber));
}

function MilitaryHistory_GetPlannedEndDate(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('PEDTextBox', rowNumber));
}

function MilitaryHistory_GetPlannedEndDateValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custPED', rowNumber));
}

function MilitaryHistory_GetUnit(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('txtUnit', rowNumber));
}

function MilitaryHistory_GetMilitaryDischarge(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ddlMilitaryDischarge', rowNumber));
}

function MilitaryHistory_GetMilitaryDischargeLabel(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('MilitaryDischargeLabel', rowNumber));
}

function MilitaryHistory_GetMilitaryDischargeValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custMilitaryDischarge', rowNumber));
}

function MilitaryHistory_GetCampaignVeteran(rowNumber) {
    return $('*[data-name="rblCampaignVeteran_' + rowNumber + '"] input');
}

function MilitaryHistory_GetMilitaryEmploymentStatusLabel(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('MilitaryEmploymentStatusLabel', rowNumber));
}

function MilitaryHistory_GetTransitioningTypeLabel(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('TransitioningTypeLabel', rowNumber));
}

function MilitaryHistory_GetServiceDutyStartDateLabel(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ServiceDutyStartDateLabel', rowNumber));
}

function MilitaryHistory_GetConnectedDisabilityLabel(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('ConnectedDisabilityLabel', rowNumber));
}

function MilitaryHistory_GetBranchOfServiceLabel(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('BranchOfServiceLabel', rowNumber));
}

function MilitaryHistory_GetRankLabel(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('RankLabel', rowNumber));
}

function MilitaryHistory_GetServiceBranchValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custServiceBranch', rowNumber));
}

function MilitaryHistory_GetRankValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custRank', rowNumber));
}

function MilitaryHistory_GetVeteranTypeValidator(rowNumber) {
    return $('#' + MilitaryHistory_BuildId('custVeteranType', rowNumber));
}

function ValidateEmploymentStatus(sender, args) {
    var rdoMilitary = $('#rblMilitaryService');
    if ($(rdoMilitary).exists()) {
        var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
        var ddlMilitaryEmploymentStatus = MilitaryHistory_GetMilitaryEmploymentStatus(rowNumber);

        if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
            if (MilitaryHistory_GetVeteranType(rowNumber).val() == resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember) {
                if ($(ddlMilitaryEmploymentStatus).children().first().is(':selected')) {
                    sender.innerHTML = resumeMilitaryHistoryCodeValues.errorMilitaryEmploymentStatusRequired;
                    args.IsValid = false;
                }
                else if ($(ddlMilitaryEmploymentStatus).val() != resumeMilitaryHistoryCodeValues.employmentStatusEmployedTerminationReceived) {
                    sender.innerHTML = resumeMilitaryHistoryCodeValues.errorMilitaryEmploymentStatus;
                    args.IsValid = false;
                }
            }
        }
        else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false; }

    }
    
}

function ValidateServiceBranch(sender, args) 
{
    var rdoMilitary = $('#rblMilitaryService');
    if ($(rdoMilitary).exists()) {
        if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
            var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
            var veteran = MilitaryHistory_GetVeteranType(rowNumber);
            var veterantype = $(veteran).val();
            if (veterantype != '') {
                var serviceBranch = MilitaryHistory_GetBranchOfService(rowNumber);
                if ($(serviceBranch).get(0).selectedIndex == 0) {
                    sender.innerHTML = resumeMilitaryHistoryCodeValues.errorBranchOfServiceRequired;
                    args.IsValid = false;
                }
            }

        }
    }
    else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false }
}

function ValidateRank(sender, args) {
    var rdoMilitary = $('#rblMilitaryService');

        if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
            var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
            var rank = MilitaryHistory_GetRank(rowNumber);
            var rankValue = $(rank).val();
            var serviceBranch = MilitaryHistory_GetBranchOfService(rowNumber);
            if ($(serviceBranch).get(0).selectedIndex != 0) 
                if (rankValue == '')
                    if ($(rank).get(0).selectedIndex == 0) {
                        sender.innerHTML = resumeMilitaryHistoryCodeValues.errorRankRequired;
                        args.IsValid = false;
                    }
        }
   
    else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false; }
}
function ValidateMilitaryDischarge(sender, args) {
    var rdoMilitary = $('#rblMilitaryService');

    if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
        var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
        var veteranType = MilitaryHistory_GetVeteranType(rowNumber).val();
        if (veteranType == resumeMilitaryHistoryCodeValues.veteranEraOtherVet) {
            var militaryDischarge = MilitaryHistory_GetMilitaryDischarge(rowNumber);
            if (militaryDischarge.find('option:first').is(':selected')) {
                args.IsValid = false;
            }
        }

    }
    else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false; }
}

function ValidateTypeofVeteran(sender, args) {
    var rdoMilitary = $('#rblMilitaryService');

    if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
        var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
        var veteran = MilitaryHistory_GetVeteranType(rowNumber);
        var employmentStatus = $('#ddlEmploymentStatus');

        // Nothing Selected
        if ($(veteran).children().first().is(':selected')) {
            sender.innerHTML = resumeMilitaryHistoryCodeValues.errorVeteranTypeRequired;
            args.IsValid = false;
        }

        UpdateVeteranFields(rowNumber)
        employmentStatus.change();

    }
    else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false }
}

function UpdateVeteranFields(rowNumber) {
    var rdoMilitary = $('#rblMilitaryService');

    if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
        var veteran = MilitaryHistory_GetVeteranType(rowNumber);
        var veterantype = $(veteran).val();
        var transition = MilitaryHistory_GetTransitioningType(rowNumber);
        var transitiontype = $(transition).val();
        var employmentStatus = $('#ddlEmploymentStatus');
        var militaryEmploymentStatus = MilitaryHistory_GetMilitaryEmploymentStatus(rowNumber);
        var serviceDutyStart = MilitaryHistory_GetStartDate(rowNumber);
        var serviceDutyEnd = MilitaryHistory_GetPlannedEndDate(rowNumber);
        var serviceDutyEndLabel = MilitaryHistory_GetPlannedEndDateLabel(rowNumber);
        var branchOfService = MilitaryHistory_GetBranchOfService(rowNumber);
        var rank = MilitaryHistory_GetRank(rowNumber);
        var militaryOccupationTitle = MilitaryHistory_GetMilitaryOccupationTitle(rowNumber);
        var unit = MilitaryHistory_GetUnit(rowNumber);
        var serviceDisability = MilitaryHistory_GetServiceDisability(rowNumber);
        var connectedDisabilityLabel = MilitaryHistory_GetConnectedDisabilityLabel(rowNumber);
        var militaryDischarge = MilitaryHistory_GetMilitaryDischarge(rowNumber);
        var militaryDischargeLabel = MilitaryHistory_GetMilitaryDischargeLabel(rowNumber);
        var militaryEmploymentStatusLabel = MilitaryHistory_GetMilitaryEmploymentStatusLabel(rowNumber);
        var transitioningTypeLabel = MilitaryHistory_GetTransitioningTypeLabel(rowNumber);
        var serviceDutyStartDateLabel = MilitaryHistory_GetServiceDutyStartDateLabel(rowNumber);
        var branchOfServiceLabel = MilitaryHistory_GetBranchOfServiceLabel(rowNumber);
        var rankLabel = MilitaryHistory_GetRankLabel(rowNumber);

        // Nothing Selected
        if ($(veteran).children().first().is(':selected')) {
            DisableStyledDropDown(militaryEmploymentStatus, true);
            militaryEmploymentStatusLabel.toggleClass("requiredData", false);

            DisableStyledDropDown(transition, true);
            transitioningTypeLabel.toggleClass("requiredData", false);

            DisableStyledTextBox(serviceDutyStart, '');
            serviceDutyStartDateLabel.toggleClass("requiredData", false);
            DisableStyledTextBox(serviceDutyEnd, '');
            serviceDutyEndLabel.toggleClass("requiredData", false);

            MilitaryHistory_GetCampaignVeteran(rowNumber).prop("disabled", true);
            DisableStyledDropDown(branchOfService, true);
            branchOfServiceLabel.toggleClass("requiredData", false);
            DisableStyledDropDown(rank, true);
            rankLabel.toggleClass("requiredData", false);
            DisableStyledTextBox(militaryOccupationTitle);
            DisableStyledTextBox(unit);
            DisableStyledDropDown(serviceDisability, true);
            connectedDisabilityLabel.toggleClass("requiredData", false);
            DisableStyledDropDown(militaryDischarge, true);
            militaryDischargeLabel.toggleClass("requiredData", false);

            // Turn off all validators other than Type of Veteran
            ValidatorEnable(MilitaryHistory_GetMilitaryEmploymentStatusValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetTransitioningTypeValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetStartDateValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetRankValidator(rowNumber).get(0), false);
        }

        // Other Eligible
        else if (veterantype == resumeMilitaryHistoryCodeValues.veteranEraOtherEligible) {
            $(serviceDutyEndLabel).html('Service duty end date');
            DisableStyledDropDown(militaryEmploymentStatus, true);
            militaryEmploymentStatusLabel.toggleClass("requiredData", false);
            DisableStyledDropDown(transition, true);
            transitioningTypeLabel.toggleClass("requiredData", false);
            DisableStyledTextBox(serviceDutyStart, '');
            serviceDutyStartDateLabel.toggleClass("requiredData", false);
            DisableStyledTextBox(serviceDutyEnd, '');
            serviceDutyEndLabel.toggleClass("requiredData", false);

            ValidatorEnable(MilitaryHistory_GetMilitaryEmploymentStatusValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetTransitioningTypeValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetStartDateValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0), false);

            ValidatorEnable(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0), true);
            MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0).isvalid = true;
            ValidatorUpdateDisplay(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0));

            ValidatorEnable(MilitaryHistory_GetRankValidator(rowNumber).get(0), true);

            MilitaryHistory_GetCampaignVeteran(rowNumber).prop('disabled', true);

            DisableStyledDropDown(serviceDisability, true);
            connectedDisabilityLabel.toggleClass("requiredData", false);
            EnableStyledDropDown(branchOfService);
            branchOfServiceLabel.toggleClass("requiredData", true);
            EnableStyledDropDown(rank);
            rankLabel.toggleClass("requiredData", true);
            EnableStyledTextBox(militaryOccupationTitle);
            EnableStyledTextBox(unit);
            EnableStyledDropDown(militaryDischarge);

            militaryDischargeLabel.toggleClass("requiredData", false);
        }

        // Transitioning Service Member
        else if (veterantype == resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember) {
            $(serviceDutyEndLabel).html('Planned end date');
            EnableStyledDropDown(militaryEmploymentStatus);
            militaryEmploymentStatusLabel.toggleClass("requiredData", true);
            ValidatorEnable(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0), true);
            EnableStyledDropDown(transition);
            transitioningTypeLabel.toggleClass("requiredData", true);

            ValidatorEnable(MilitaryHistory_GetMilitaryEmploymentStatusValidator(rowNumber).get(0), true);

            // militaryEmploymentStatus.find('option[value="EmployedTerminationReceived"]').prop('selected', true);
            // militaryEmploymentStatus.change();

            // if (rowNumber == 0)
            //	employmentStatus.find('option[value="EmployedTerminationReceived"]').prop('selected', true);

            ValidatorEnable(MilitaryHistory_GetTransitioningTypeValidator(rowNumber).get(0), true);
            MilitaryHistory_GetTransitioningTypeValidator(rowNumber).get(0).isvalid = true;
            ValidatorUpdateDisplay(MilitaryHistory_GetTransitioningTypeValidator(rowNumber).get(0));

            ValidatorEnable(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0), true);
            ValidatorEnable(MilitaryHistory_GetRankValidator(rowNumber).get(0), true);

            if (transitiontype == resumeMilitaryHistoryCodeValues.veteranTransitionTypeSpouse) {
                DisableStyledTextBox(serviceDutyStart, '');
                serviceDutyStartDateLabel.toggleClass("requiredData", false);
                DisableStyledTextBox(serviceDutyEnd, '');
                serviceDutyEndLabel.toggleClass("requiredData", false);

                MilitaryHistory_GetCampaignVeteran(rowNumber).prop('disabled', true);

                ValidatorEnable(MilitaryHistory_GetStartDateValidator(rowNumber).get(0), false);
                ValidatorEnable(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0), false);
                ValidatorEnable(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0), false);
                ValidatorEnable(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0), false);
                ValidatorEnable(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0), false);
                ValidatorEnable(MilitaryHistory_GetRankValidator(rowNumber).get(0), false);
            }
            else {
                EnableStyledTextBox(serviceDutyStart);
                serviceDutyStartDateLabel.toggleClass("requiredData", true);
                EnableStyledTextBox(serviceDutyEnd);
                serviceDutyEndLabel.toggleClass("requiredData", true);

                MilitaryHistory_GetCampaignVeteran(rowNumber).removeAttr('disabled');

                EnableStyledDropDown(serviceDisability);
                connectedDisabilityLabel.toggleClass("requiredData", true);

                ValidatorEnable(MilitaryHistory_GetStartDateValidator(rowNumber).get(0), true);
                MilitaryHistory_GetStartDateValidator(rowNumber).get(0).isvalid = true;
                ValidatorUpdateDisplay(MilitaryHistory_GetStartDateValidator(rowNumber).get(0));
                ValidatorEnable(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0), true);
                MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0).isvalid = true;
                ValidatorUpdateDisplay(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0));
                ValidatorEnable(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0), true);
                MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0).isvalid = true;
                ValidatorUpdateDisplay(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0));
                ValidatorEnable(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0), true);
                MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0).isvalid = true;
                ValidatorUpdateDisplay(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0));


                ValidatorEnable(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0), true);
                MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0).isvalid = true;
                ValidatorUpdateDisplay(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0));

                ValidatorEnable(MilitaryHistory_GetRankValidator(rowNumber).get(0), true);
                MilitaryHistory_GetRankValidator(rowNumber).get(0).isvalid = true;
                ValidatorUpdateDisplay(MilitaryHistory_GetRankValidator(rowNumber).get(0));

            }
            EnableStyledDropDown(branchOfService);
            branchOfServiceLabel.toggleClass("requiredData", true);
            EnableStyledDropDown(rank);
            rankLabel.toggleClass("requiredData", true);
            EnableStyledTextBox(militaryOccupationTitle);
            EnableStyledTextBox(unit);
            DisableStyledDropDown(militaryDischarge, true);

            militaryDischargeLabel.toggleClass("requiredData", false);
        }

        // Veteran
        else if (veterantype == resumeMilitaryHistoryCodeValues.veteranEraOtherVet) {
            $(serviceDutyEndLabel).html('Service duty end date');
            DisableStyledDropDown(militaryEmploymentStatus, true);
            militaryEmploymentStatusLabel.toggleClass("requiredData", false);
            DisableStyledDropDown(transition, true);
            transitioningTypeLabel.toggleClass("requiredData", false);
            EnableStyledTextBox(serviceDutyStart);
            serviceDutyStartDateLabel.toggleClass("requiredData", true);
            EnableStyledTextBox(serviceDutyEnd);
            serviceDutyEndLabel.toggleClass("requiredData", true);

            MilitaryHistory_GetCampaignVeteran(rowNumber).removeAttr('disabled');

            EnableStyledDropDown(serviceDisability);
            connectedDisabilityLabel.toggleClass("requiredData", true);

            ValidatorEnable(MilitaryHistory_GetMilitaryEmploymentStatusValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetTransitioningTypeValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetStartDateValidator(rowNumber).get(0), true);
            MilitaryHistory_GetStartDateValidator(rowNumber).get(0).isvalid = true;
            ValidatorUpdateDisplay(MilitaryHistory_GetStartDateValidator(rowNumber).get(0));
            ValidatorEnable(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0), true);
            MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0).isvalid = true;
            ValidatorUpdateDisplay(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0));
            ValidatorEnable(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0), true);
            MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0).isvalid = true;
            ValidatorUpdateDisplay(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0));
            ValidatorEnable(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0), true);
            MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0).isvalid = true;
            ValidatorUpdateDisplay(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0));


            ValidatorEnable(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0), true);
            MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0).isvalid = true;
            ValidatorUpdateDisplay(MilitaryHistory_GetServiceBranchValidator(rowNumber).get(0));

            ValidatorEnable(MilitaryHistory_GetRankValidator(rowNumber).get(0), true);
            MilitaryHistory_GetRankValidator(rowNumber).get(0).isvalid = true;
            ValidatorUpdateDisplay(MilitaryHistory_GetRankValidator(rowNumber).get(0));

            EnableStyledDropDown(branchOfService);
            branchOfServiceLabel.toggleClass("requiredData", true);
            EnableStyledDropDown(rank);
            rankLabel.toggleClass("requiredData", true);
            EnableStyledTextBox(militaryOccupationTitle);
            EnableStyledTextBox(unit);

            EnableStyledDropDown(militaryDischarge);
            militaryDischargeLabel.toggleClass("requiredData", true);
        }
    }


}
function ValidateTransType(sender, args) {
    var rdoMilitary = $('#rblMilitaryService');

    if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
        var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
        var veteran = MilitaryHistory_GetVeteranType(rowNumber);
        var veterantype = $(veteran).val();
        var transition = MilitaryHistory_GetTransitioningType(rowNumber);
        var transitiontype = $(transition).val();
        var serviceDutyStart = MilitaryHistory_GetStartDate(rowNumber);
        var serviceDutyEnd = MilitaryHistory_GetPlannedEndDate(rowNumber);
        var serviceDutyStartDateLabel = MilitaryHistory_GetServiceDutyStartDateLabel(rowNumber);
        var serviceDutyEndDateLabel = MilitaryHistory_GetPlannedEndDateLabel(rowNumber);
        var serviceDisability = MilitaryHistory_GetServiceDisability(rowNumber);
        var connectedDisabilityLabel = MilitaryHistory_GetConnectedDisabilityLabel(rowNumber);
        if (veterantype === resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember) {
            if ($(transition).children().first().is(':selected')) {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorTransitioningTypeRequired;
                args.IsValid = false;
            }
        }
        if (veterantype === resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember && transitiontype === resumeMilitaryHistoryCodeValues.veteranTransitionTypeSpouse) {
            DisableStyledTextBox(serviceDutyStart, '');
            serviceDutyStartDateLabel.toggleClass("requiredData", false);
            DisableStyledTextBox(serviceDutyEnd, '');
            serviceDutyEndDateLabel.toggleClass("requiredData", false);
            MilitaryHistory_GetCampaignVeteran(rowNumber).prop('disabled', true);
            DisableStyledDropDown(serviceDisability, true);
            connectedDisabilityLabel.toggleClass("requiredData", false);

            ValidatorEnable(MilitaryHistory_GetStartDateValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0), false);

            ValidatorEnable(MilitaryHistory_GetStartDateValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0), false);
            ValidatorEnable(MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0), false);
        }
        else if (veterantype === resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember && (transitiontype === resumeMilitaryHistoryCodeValues.veteranTransitionTypeRetirement || transitiontype === resumeMilitaryHistoryCodeValues.veteranTransitionTypeDischarge)) {
            EnableStyledTextBox(serviceDutyStart);
            serviceDutyStartDateLabel.toggleClass("requiredData", true);
            EnableStyledTextBox(serviceDutyEnd);
            serviceDutyEndDateLabel.toggleClass("requiredData", true);
            MilitaryHistory_GetCampaignVeteran(rowNumber).removeAttr('disabled');
            EnableStyledDropDown(serviceDisability);
            connectedDisabilityLabel.toggleClass("requiredData", true);

            MilitaryHistory_GetStartDateValidator(rowNumber).get(0).enabled = true;
            MilitaryHistory_GetPlannedEndDateValidator(rowNumber).get(0).enabled = true;
            MilitaryHistory_GetServiceDisabilityValidator(rowNumber).get(0).enabled = true;

            MilitaryHistory_GetMilitaryDischargeValidator(rowNumber).get(0).enabled = false;
        }
    }
    else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false; }
}

function ValidateStartDateFormat(sender, args) {
    var rdoMilitary = $('#rblMilitaryService');

    if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
        var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
        var serviceDutyStart = MilitaryHistory_GetStartDate(rowNumber);
        var serviceDutyStartDate = $(serviceDutyStart).val();
        var startdate = new Date(serviceDutyStartDate);

        var veterantype = MilitaryHistory_GetVeteranType(rowNumber).val();
        var transitiontype = MilitaryHistory_GetTransitioningType(rowNumber).val();

        var dobDate = $("#DOBTextBox").val();
        var birthdate = new Date(dobDate);

        var curDate = new Date();

        if ((veterantype === resumeMilitaryHistoryCodeValues.veteranEraOtherVet || veterantype === resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember) && transitiontype != resumeMilitaryHistoryCodeValues.veteranTransitionTypeSpouse) {
            if (serviceDutyStartDate === '') {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorServiceDutyStartDateRequired;
                args.IsValid = false;
            }
            else if (isDate(serviceDutyStartDate) === false || (!moment(serviceDutyStartDate, "MM/DD/YYYY", true).isValid())) {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorDateFormat;
                args.IsValid = false;
            }
            else if (startdate > curDate) {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorServiceDutyStartDateLimit;
                args.IsValid = false;
            }
            else if (startdate.value != '' && birthdate.value != '') {
                if (!checkMinimumRange(dobDate, serviceDutyStartDate, parseInt(resumeMilitaryHistoryCodeValues.MilitaryServiceMinimumAge, 10))) {
                    sender.innerHTML = resumeMilitaryHistoryCodeValues.errorServiceDutyStartDateDOBLimit;
                    args.IsValid = false;
                }
            }
        }
    }
    else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false; }
}
function ValidateEndDateFormat(sender, args) {
    var rdoMilitary = $('#rblMilitaryService');

    if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
        var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
        var serviceDutyStartDate = MilitaryHistory_GetStartDate(rowNumber).val();
        var startdate = new Date(serviceDutyStartDate);
        var serviceDutyEnd = MilitaryHistory_GetPlannedEndDate(rowNumber);
        var serviceDutyEndDate = $(serviceDutyEnd).val();
        var enddate = new Date(serviceDutyEndDate);
        var curDate = new Date();

        var veterantype = MilitaryHistory_GetVeteranType(rowNumber).val();
        var transitiontype = MilitaryHistory_GetTransitioningType(rowNumber).val();

        var msPerDay = 24 * 60 * 60 * 1000;
        var diffdays = Math.round((enddate.valueOf() - curDate.valueOf()) / msPerDay);

        if (veterantype === resumeMilitaryHistoryCodeValues.veteranEraOtherVet) {
            if (serviceDutyEndDate === '') {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorServiceDutyEndDateRequired;
                args.IsValid = false;
            }
            else if (isDate(serviceDutyEndDate) === false || (!moment(serviceDutyEndDate, "MM/DD/YYYY", true).isValid())) {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorDateFormat;
                args.IsValid = false;
            }
            else if (enddate > curDate) {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorServiceDutyEndDateLimit;
                args.IsValid = false;
            }
            else if (enddate < startdate) {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorServiceDutyEndDate;
                args.IsValid = false;
            }
        }
        else if (veterantype === resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember && transitiontype != resumeMilitaryHistoryCodeValues.veteranTransitionTypeSpouse) {
            if (serviceDutyEndDate === '') {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorPlannedEndDateRequired;
                args.IsValid = false;
            }
            else if (isDate(serviceDutyEndDate) === false) {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorDateFormat;
                args.IsValid = false;
            }
            else if (enddate < startdate) {
                sender.innerHTML = resumeMilitaryHistoryCodeValues.errorPlannedEndDateLimit;
                args.IsValid = false;
            }
            else if (veterantype === resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember && (transitiontype === resumeMilitaryHistoryCodeValues.veteranTransitionTypeRetirement || transitiontype === resumeMilitaryHistoryCodeValues.veteranTransitionTypeDischarge || transitiontype === resumeMilitaryHistoryCodeValues.veteranTransitionTypeSpouse)) {
                if (enddate < curDate) {
                    sender.innerHTML = resumeMilitaryHistoryCodeValues.errorVEDLimit;
                    args.IsValid = false;

                }
                else if ((diffdays / 365.25) > 1 && transitiontype === resumeMilitaryHistoryCodeValues.veteranTransitionTypeDischarge) {
                    sender.innerHTML = resumeMilitaryHistoryCodeValues.errorDischargingTransitioningVeteranLimit;
                    args.IsValid = false;
                }
                else if ((diffdays / 365.25) > 2 && transitiontype === resumeMilitaryHistoryCodeValues.veteranTransitionTypeRetirement) {
                    sender.innerHTML = resumeMilitaryHistoryCodeValues.errorRetiringTransitioningVeteranLimit;
                    args.IsValid = false;
                }
            }
        }
    }
    else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false; }
}

function ValidateConnectedDisability(sender, args) {
    var rdoMilitary = $('#rblMilitaryService');

    if ($(rdoMilitary).find('input[type="radio"]:checked').length > 0 && $(rdoMilitary).find('input[type="radio"]:checked').val().toLowerCase() === 'yes') {
        var rowNumber = MilitaryHistory_ExtractRowNumber(sender);
        var veteranType = MilitaryHistory_GetVeteranType(rowNumber).val();
        if (veteranType == resumeMilitaryHistoryCodeValues.veteranEraTransitioningServiceMember || veteranType == resumeMilitaryHistoryCodeValues.veteranEraOtherVet) {
            var disability = MilitaryHistory_GetServiceDisability(rowNumber);
            if (disability.find('option:first').is(':selected')) {
                args.IsValid = false;
            }
        }
    }
    else if ($(rdoMilitary).find('input[type="radio"]:checked').length == 0) { args.IsValid = false; }
}
