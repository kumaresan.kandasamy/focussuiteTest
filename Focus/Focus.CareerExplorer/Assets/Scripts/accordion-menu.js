﻿$(document).ready(function () {
  accordionMenuSetupClickEvents();
});

function accordionMenuSetupClickEvents() {
  // *** The following handles a first style of multiple accordion menus ***
  $('.multipleAccordionTitle').on('click', function () {
    var isOn = $(this).hasClass("on");

    // Remove the on class from all buttons
    $('.multipleAccordionTitle').removeClass('on');

    // No matter what we close all open slides
    $('.accordionContent').hide();

    // If the next slide wasn't open then open it 
    if ($(this).nextAll(".accordionContent").is(':hidden') == true) {
      // Add the on class to the button
      $(this).addClass('on');

      // Open the slide
      $(this).next('.accordionContent').toggle();
    }

    // if clicking on an already open accordion - close it
    if (isOn) {
      $(this).removeClass("on");
      $(this).next('.accordionContent').toggle();
    }
  });

  // *** The following handles a second style of multiple accordion menus ***
  $('.multipleAccordionTitle2').on('click', function () {
    var isOn = $(this).hasClass("on");

    // Remove the on class from all buttons
    $('.multipleAccordionTitle2').removeClass('on');

    // No matter what we close all open slides
    $('.accordionContent2').hide();

    // If the next slide wasn't open then open it 
    if ($(this).nextAll(".accordionContent2").is(':hidden') == true) {
      // Add the on class to the button
      $(this).addClass('on');

      // Open the slide
      $(this).next('.accordionContent2').toggle();
    }

    // if clicking on an already open accordion - close it
    if (isOn) {
      $(this).removeClass("on");
      $(this).next('.accordionContent2').toggle();
    }
  });

  // Adds the .over class from the stylesheet on mouseover - currently not being used
  $('.multipleAccordionTitle').on('mouseover', function () {
    $(this).addClass('over');

    // On mouseout remove the over class
  }).on('mouseout', function () {
    $(this).removeClass('over');
  });

  $('.accordionContent').hide();
  $('.accordionContent2').hide();
  $('.open').show();

  // *** The following handles single accordions ***
  $('.singleAccordionTitle').on('click', function () {
    // Toggle the 'on' class
    if ($(this).hasClass('on'))
      $(this).removeClass('on');
    else
      $(this).addClass('on');

    $(this).next('.singleAccordionContent').toggle();
  });

  $('.open').show();  
}