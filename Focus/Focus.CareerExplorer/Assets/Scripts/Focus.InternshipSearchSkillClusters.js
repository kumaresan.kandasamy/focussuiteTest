﻿function InternshipSearchSkillClusters_UpdateSummary() {
	var skillClusters = this;

	var summaryListWrapper = $('#' + skillClusters.SummaryListId);
	var summaryList = $(summaryListWrapper).children('table');
	var hiddenIdsField = $('input[id="' + skillClusters.HiddenIdsFieldId + '"]');
	var hiddenNamesField = $('input[id="' + skillClusters.HiddenNamesFieldId + '"]');

	$(summaryList).children().remove();
	$(hiddenIdsField).val('');
	$(hiddenNamesField).val('');

	$(summaryListWrapper).hide();

	if ($('#' + skillClusters.ClustersListId + ' :checked').length > 0) {
		$(summaryListWrapper).show();
		$('#' + skillClusters.ClustersListId + ' :checked').map(function () {
			$(summaryList).append('<tr><td>' + $(this).next('label').html() + '</td><td class="column2"><input type="hidden" value="' + $(this).val() + '" /><img src="' + skillClusters.DeleteIconUrl + '" style="position:relative; top:2px;" /></td></tr>');
			$(summaryList).stop(true, true).effect('highlight', { color: '#FCE8C2' }, 2000);
		});

		var skillIds = $('#' + skillClusters.ClustersListId + ' :checked').map(function () {
			return $(this).val();
		}).get().join(',');
		$('input[id="' + skillClusters.HiddenIdsFieldId + '"]').val(skillIds);

		var skillNames = $('#' + skillClusters.ClustersListId + ' :checked').map(function () {
			return $("label[for='" + $(this).attr("id") + "']").text();
		}).get().join(',');
		$('input[id="' + skillClusters.HiddenNamesFieldId + '"]').val(skillNames);
	}
}

function InternshipSearchSkillClusters_SetupCheckBoxes() {
	var skillClusters = this;
	skillClusters.UpdateSummary();
	$('#' + skillClusters.ClustersListId + ' input:checkbox').on('click', function () {
		skillClusters.UpdateSummary();
	});
	$('#' + skillClusters.SummaryListId + ' table').on('click', 'img', function () {
		skillClusters.RemoveSkillCluster($(this).prev('input:hidden').val());
	});
}

function InternshipSearchSkillClusters_RemoveSkillCluster(skillClusterId) {
	var skillClusters = this;
	var skillClusterCheckbox = $('#' + skillClusters.ClustersListId + ' input:checkbox[value="' + skillClusterId + '"]');
	if ($(skillClusterCheckbox).exists()) {
		$(skillClusterCheckbox).prop('checked', false);
		skillClusters.UpdateSummary();
	}
}

function InternshipSkillClusters(clustersListId, summaryListId, hiddenIdsFieldId, hiddenNamesFieldId, deleteIconUrl) {
	this.ClustersListId = clustersListId;
	this.SummaryListId = summaryListId;
	this.HiddenIdsFieldId = hiddenIdsFieldId;
	this.HiddenNamesFieldId = hiddenNamesFieldId;
	this.DeleteIconUrl = deleteIconUrl;

	this.SetUpCheckBoxes = InternshipSearchSkillClusters_SetupCheckBoxes;
	this.UpdateSummary = InternshipSearchSkillClusters_UpdateSummary;
	this.RemoveSkillCluster = InternshipSearchSkillClusters_RemoveSkillCluster;
}