﻿/*! Copyright © 2000-2014 Burning Glass International Inc.
*
* Proprietary and Confidential
*
* All rights are reserved. Reproduction or transmission in whole or in part, in
* any form or by any means, electronic, mechanical or otherwise, is prohibited
* without the prior written consent of Burning Glass International Inc. 
*/

(function (focusSuite) {
  focusSuite.searchCriteriaWorkDays = function () {
    var bindClicks = function (id) {
      var table = $("#" + id);
      
      var firstBox = table.find(":checkbox:first");
      var otherBoxes = table.find(":checkbox:not(:first)");

      firstBox.on("click", function () {
        otherBoxes.prop("checked", $(this).is(":checked"));
      });

      otherBoxes.on("click", function () {
        firstBox.prop("checked", otherBoxes.filter(":checked").length == otherBoxes.length);
      });
    };

    var bindSearchCriteriaWorkDaysOnClick = function (weekdaysCheckBoxListId, weekendCheckBoxList) {
      bindClicks(weekdaysCheckBoxListId);
      bindClicks(weekendCheckBoxList);
    };

    return {
      BindSearchCriteriaWorkDaysOnClick: bindSearchCriteriaWorkDaysOnClick
    };
  } ();
})(window.focusSuite = window.focusSuite || {});