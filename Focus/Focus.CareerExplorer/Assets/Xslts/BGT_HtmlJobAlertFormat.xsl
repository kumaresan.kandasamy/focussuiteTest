<!-- 
    ************************************************************
    Copyright(c) 2010, Burning Glass Technologies
    XSL for Job alerts for Focus/Career in specific to BGT customer
    ************************************************************
    Version 1.1
    
    Change Log: 
	07/MAY/2010 RRC  Created the XSL
	12/MAY/2010 RRC - Added cached label in FOCUS URL
	27/MAY/2010 RRC - Added Unsubscribe URL
	
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="html" version="1.0" omit-xml-declaration="no" indent="yes" encoding="iso-8859-1"/>
  <xsl:variable name="customername" select="'BGT'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:template match="/">

    <style type="text/css">
      .font11 { font-family:arial;font-size:11px; }
      .font12 { font-family:arial;font-size:12px; }
      .font13 { font-family:arial;font-size:13px; }
      .font14 { font-family:arial;font-size:14px; }
      .font16 { font-family:arial;font-size:16px; }
      .font18 { font-family:arial;font-size:18px; }
      .font21 { font-family:arial;font-size:21px; }
    </style>
    <html>
      <TABLE border="0" width="100%">
        <TR width="100%">
          <TD width="80%">
            <!--  Main table -->
            <table border="0" width="100%" role="presentation">
              <tr width="100%">
                <td style="background-color:rgb(0,0,139);color:white;font-style:italic" width="100%">
                  <font class="font14">
                    <b>
                      Hi
                      <xsl:value-of select="jobalerts/userinfo/firstname"/>  <xsl:text> </xsl:text>
                      <xsl:value-of select="jobalerts/userinfo/lastname"/>
                    </b>
                  </font>
                </td>
              </tr>
              <tr width="100%">
                <TD width="100%"  style="background-color:rgb(102,154,204);font-style:italic" >
                  <font class="font16">Focus/Career Job Alert</font>
                </TD>
              </tr>
              <tr>
                <TD width="100%" style="background-color:white;" height="1px"/>
              </tr>
              <tr>
                <TD width="100%" style="background-color:rgb(102,154,204);" height="3px"/>
              </tr>
            </table>
          </TD>
          <td width="20%" align="right">
            <img src="http://demo.focus-career.com/images/BurningGlassLogo.gif"  />
          </td>
        </TR>
      </TABLE>
      <table width="100%" role="presentation">
        <xsl:for-each select="jobalerts/posting[@customer=$customername] [position()=1]">
        <TR width="100%">
          <TD width="100%">
            <font style="color:rgb(102,154,204);font-size:23px;font-family:arial;">
              <xsl:text>   </xsl:text>Your job matches from Focus/Career Job Alert
            </font>
          </TD>
        </TR>
        <TR width="100%">
          <TD width="100%" style="background-color:rgb(0,0,139); width=100%; border-width:thick; height:2px;"/>
        </TR>
        <TR>
          <TD width="100%">
            <font class="font16">
              <b>Your resume generated the following matches</b>
            </font>
          </TD>
        </TR>
        </xsl:for-each>
      </table>

      <TABLE>
        <TR>
          <TD width="80%">
            <!--  Repeating row in the Main table-->
            <xsl:for-each select="jobalerts/posting">
              <xsl:choose>
                <xsl:when test="translate(./@customer,$lowercase,$uppercase)=$customername">
                  <TABLE>
                    <TR width="100%">
                      <td>
                        <font class="font14" style="color:rgb(0,0,139);">
                          <b>
                            <xsl:value-of select="jobtitle"/>
                          </b>
                        </font>
                      </td>
                    </TR>
                    <TR>
                      <TD  width="100%">
                        <font class="font13">
                          <xsl:value-of select="employer"/>
                          <xsl:if test='string-length(employer)>0'>
                            <xsl:text>, </xsl:text>
                          </xsl:if>
                          <xsl:value-of select="location"/>
                        </font>
                      </TD>
                    </TR>
                    <TR>
                      <TD width="100%">
                        <font class="font13">
                          Match level:<xsl:text>  </xsl:text>
                        </font>
                        <xsl:choose>
                          <xsl:when test="(score &gt; 0) and  (score &lt; 200)">
                            <img src="http://demo.focus-career.com/images/NoStar.png" />
                          </xsl:when>
                          <xsl:when test="(score &gt; 199) and  score &lt; 400">
                            <img src="http://demo.focus-career.com/images/Blue1Star.png" />

                          </xsl:when>
                          <xsl:when test="(score &gt; 399) and  score &lt; 550">
                            <img src="http://demo.focus-career.com/images/Blue2Star.png" />
     
                          </xsl:when>
                          <xsl:when test="(score &gt; 549) and  score &lt; 700">
                            <img src="http://demo.focus-career.com/images/Blue3Star.png" />

                          </xsl:when>
                          <xsl:when test="(score &gt; 699) and  score &lt; 850">
                            <img src="http://demo.focus-career.com/images/Blue4Star.png" />

                          </xsl:when>
                          <xsl:when test="(score &gt; 849) and  score &lt; 1001">
                            <img src="http://demo.focus-career.com/images/Blue5Star.png" />

                          </xsl:when>
                        </xsl:choose>
                      </TD>
                    </TR>
                    <TR>
                      <TD  width="100%">
                        <font class="font12">
                          <xsl:value-of select="description"/>
                        </font>
                      </TD>
                    </TR>
                    <!--<xsl:if test="string-length(joburl)>0">
                      <TR>
                        <TD width="100%">
                          <a>
                            <xsl:attribute name="href">
                              <xsl:value-of select="joburl"/>
                            </xsl:attribute>
                            <font class="font13">
                              Details of <xsl:text>"</xsl:text><xsl:value-of select="jobtitle"/><xsl:text>" </xsl:text> posted by employer
                            </font>
                          </a>
                        </TD>
                      </TR>
                    </xsl:if>-->
                    <xsl:if test="string-length(focusurl)>0">
                      <TR>
                        <TD width="100%">
                          <a>
                            <xsl:attribute name="href">
                              <xsl:value-of select="focusurl"/>
                            </xsl:attribute>
                            <font class="font13">
                              Copy of <xsl:text>"</xsl:text><xsl:value-of select="jobtitle"/><xsl:text>" </xsl:text> preserved by Focus/Career Job Alert
                            </font>
                          </a>
                          <font class="font13">
                            <xsl:text>(cached)</xsl:text>
                          </font>
                        </TD>
                      </TR>
                    </xsl:if>
                    <TR/>
                    <TR/>
                    <TR/>
                    <TR/>
                  </TABLE>
                </xsl:when>
              </xsl:choose>
            </xsl:for-each>
          </TD>
        </TR>
      </TABLE>

      <br/>
      <footer>
        <table width="100%" role="presentation">
          <tr width="100%">
            <TD width="100%" style="background-color:rgb(0,0,139); width=100%; border-width:thick; height:2px;"/>
          </tr>
        </table>
        <table width="100%" role="presentation">
          <tr>
            <td width="100%" style="text-align:center;">

              <TR width="100%">
                <TD width="100%" style="text-align:center;" >
                  <font class="font11">
                    <xsl:text>You received this email because you signed up for job alerts.</xsl:text>
                  </font>
                </TD>
              </TR>
              <TR width="100%">
                <TD width="100%"  style="text-align:center;" >
                  <font class="font11">
                    <xsl:text>Click </xsl:text>
                    <a>
                      <xsl:attribute name="href">
                        <xsl:value-of select="jobalerts/userinfo/unsubscribeurl"/>
                      </xsl:attribute>
                      <xsl:text>here</xsl:text>
                    </a>
                    <xsl:text> to change settings or unsubscribe job alerts</xsl:text>
                  </font>
                </TD>
              </TR>
            </td>
          </tr>
        </table>
      </footer>
    </html>
  </xsl:template>
</xsl:stylesheet>