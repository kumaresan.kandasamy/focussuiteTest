<?xml version='1.0'?>

<!-- 
	************************************************************
	Copyright 2010, Burning Glass Technologies
	XSL for displaying job postings from Focus/Career
	************************************************************
  19/May/2010 - KNK Created the XSL
  27/May/2011 - RIR updated the XSL
	14/Apr/2015 - JAJ updated the XSL
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="html" version="1.0" omit-xml-declaration="no" indent="yes" encoding="iso-8859-1"/>
	<xsl:variable name="customername" select="'NJ'" />
	<xsl:preserve-space elements="*"/>
	<xsl:template match="/">
		<style type="text/css">
			.font13 { font-family:arial;font-size:13px; }
			.font14 { font-family:arial;font-size:14px; }
			.font16 { font-family:arial;font-size:16px; font-weight:bold; }
			.supfont {font-family:arial;font-size:8px;}
		</style>
		<html>
			<body>

				<TABLE>
					<TR width="100%">
						<td>
							<font class="font14" style="color:rgb(0,0,139);">
								<b>
									<xsl:value-of select="//title"/>
								</b>
								<xsl:if test="(//POSITION_CT) > 1">
									<b>
										- <xsl:value-of select="//POSITION_CT"/> Positions
									</b>

								</xsl:if>
							</font>
						</td>
					</TR>
					<TR width="100%">
						<td>
							<font class="font13">
								<xsl:if test="(//originid) = 7 or (//originid) = 8 or (//originid) = 9 ">
									Kentucky Employer
								</xsl:if>
								<xsl:if test="(//originid) != 7 and (//originid) != 8 and (//originid) != 9 ">
									<xsl:if test="string-length(//company)>0">
										<xsl:value-of select="//company"/>
									</xsl:if>
									<xsl:if test="string-length(//company)=0">
										<xsl:value-of select="//jobemployer"/>
									</xsl:if>
								</xsl:if>
							</font>
						</td>
					</TR>

					<tr/>
					<tr/>
					<xsl:if test="string-length(//duties)>0">
						<tr width="100%">
							<td>

								<font class="font14" style="color:rgb(0,0,139);">
									Job Description, Duties and Responsibilities
								</font>

							</td>
						</tr>
					</xsl:if>
					<tr width="100%">
						<td>
							<font class="font13">
								<xsl:for-each select="//duties/text()[last()]">
									<xsl:call-template name="insertBreaks">
										<xsl:with-param name="pText" select="."/>
									</xsl:call-template>
									<br/>
								</xsl:for-each>
							</font>
						</td>
					</tr>
					<tr/>
					<xsl:if test="string-length(//SAL_MIN)>0">
						<tr width="100%">
							<td>

								<font class="font14" style="color:rgb(0,0,139);">
									Salary Range:
								</font>
								<font class="font13">
									<xsl:value-of select="//SAL_MIN"/> -
									<xsl:value-of select="//SAL_MAX"/>

									<xsl:if test="(//SAL_UNIT_CD) = 1"> /hr</xsl:if>
									<xsl:if test="(//SAL_UNIT_CD) = 2"> /Day</xsl:if>
									<xsl:if test="(//SAL_UNIT_CD) = 3"> /Week</xsl:if>
									<xsl:if test="(//SAL_UNIT_CD) = 4"> /Month</xsl:if>
									<xsl:if test="(//SAL_UNIT_CD) = 5"> /Year</xsl:if>
								</font>
							</td>
						</tr>
					</xsl:if>
					<tr/>

					<xsl:if test="(//BENEFIT401K) = 1 or (//MEDICALLEAVE) = 1 or (//LEAVESHARING) = 1 or (//PROFITSHARING) = 1 or (//DISABILITYINSURANCE) = 1 or (//HEALTHSAVINGS) = 1 or (//LIFEINSURANCE) = 1 or (//VISIONINSURANCE) = 1 or (//RELOCATION) = 1 or (//TUITIONASSISTANCE) = 1 or (//BENEFITSNEGOTIABLE) = 1 or (//OTHER) = 1">

						<tr width="100%">
							<td>
								<font class="font14" style="color:rgb(0,0,139);">
									<br/>Benefits:<br/>
								</font>
							</td>
						</tr>
					</xsl:if>
					<tr width="100%">
						<td>
							<font class="font13">

								<xsl:if test="(//BENEFIT401K) = 1">
									Benefit401K,
								</xsl:if>
								<xsl:if test="(//MEDICALLEAVE) = 1">
									Medical Leave,
								</xsl:if>
								<xsl:if test="(//LEAVESHARING) = 1">
									Leave Sharing,
								</xsl:if>
								<xsl:if test="(//PROFITSHARING) = 1">
									Profit Sharing,
								</xsl:if>
								<xsl:if test="(//DISABILITYINSURANCE) = 1">
									Disability Insurance,
								</xsl:if>
								<xsl:if test="(//HEALTHSAVINGS) = 1">
									Health Savings,
								</xsl:if>
								<xsl:if test="(//LIFEINSURANCE) = 1">
									Life Insurance,
								</xsl:if>
								<xsl:if test="(//VISIONINSURANCE) = 1">
									Vision Insurance,
								</xsl:if>
								<xsl:if test="(//RELOCATION) = 1">
									Relocation,
								</xsl:if>
								<xsl:if test="(//TUITIONASSISTANCE) = 1">
									Tuition Assistance,
								</xsl:if>
								<xsl:if test="(//BENEFITSNEGOTIABLE) = 1">
									Benefits Negotiable
								</xsl:if>
								<xsl:if test="(//OTHER) = 1">
								</xsl:if>
							</font>
						</td>
					</tr>


					<xsl:if test="string-length(//JobDoc/clientjobdata/JOBDOC/JOB_LAST_OPEN_DATE)>0">
						<tr width="100%">
							<td>
								<font class="font14" style="color:rgb(0,0,139);">
									Job closing date:
								</font>
								<font class="font13">
									<xsl:apply-templates select="//JobDoc/clientjobdata/JOBDOC/JOB_LAST_OPEN_DATE"/>
								</font>
							</td>
						</tr>
					</xsl:if>
					<tr/>
					<tr/>
					<tr width="100%">
						<td>
							<xsl:if test="string-length(//experience)>0 or string-length(//degree)>0">
								<font class="font14" style="color:rgb(0,0,139);">
									Requirements:<br/>
								</font>
								<font class="font13">
									<xsl:if test="string-length(//experience)>0">
										Experience: <xsl:value-of select="//experience"/>
									</xsl:if>
									<br/>
									<xsl:if test="string-length(//qualification/required/education/degree)>0">
										Education:
										<xsl:value-of select="//qualification/required/education/degree"/>
										<br/>
									</xsl:if>
									<xsl:if test="string-length(//qualifications/required/education/degree)>0">
										Education:
										<xsl:value-of select="//qualifications/required/education/degree"/>
										<br/>
									</xsl:if>
								</font>
							</xsl:if>

							<xsl:if test="string-length(//certification)>0">
								<font class="font13">
									Licenses &amp; Certifications: <xsl:value-of select="//certification"/>
									<br/>
								</font>
							</xsl:if>
							<xsl:if test="string-length(//AGE_MIN)>0">
								<font class="font13">
									Minimum Age:
									<xsl:value-of select="//AGE_MIN"/>
									<xsl:if test='string-length(//reason) > 0'>
										. <xsl:value-of select="//reason"/>
									</xsl:if>
								</font>

							</xsl:if>
						</td>
					</tr>


					<tr width="100%">
						<td>
							<xsl:if test="(//WORK_MON_FLAG) = 1 or (//WORK_TUE_FLAG) = 1 or (//WORK_WED_FLAG) = 1 or (//WORK_THU_FLAG) = 1 or (//WORK_FRI_FLAG) = 1 or (//WORK_SAT_FLAG) = 1 or (//WORK_SUN_FLAG) = 1 ">

								<font class="font14" style="color:rgb(0,0,139);">
									<br/>Work Days:
								</font>
							</xsl:if>

							<font class="font13">

								<xsl:if test="(//WORK_MON_FLAG) = 1">
									Mon,
								</xsl:if>
								<xsl:if test="(//WORK_TUE_FLAG) = 1">
									Tue,
								</xsl:if>
								<xsl:if test="(//WORK_WED_FLAG) = 1">
									Wed,
								</xsl:if>
								<xsl:if test="(//WORK_THU_FLAG) = 1">
									Thur,
								</xsl:if>
								<xsl:if test="(//WORK_FRI_FLAG) = 1">
									Fri,
								</xsl:if>
								<xsl:if test="(//WORK_SAT_FLAG) = 1">
									Sat,
								</xsl:if>
								<xsl:if test="(//WORK_SUN_FLAG) = 1">
									Sun
								</xsl:if>
							</font>


							<xsl:if test="(//SHIFT_CD) > 0">
								<font class="font14" style="color:rgb(0,0,139);">
									<br/> Work Shift:
								</font>
								<font class="font13">
									<xsl:if test="(//SHIFT_CD) = 1">First (Day)</xsl:if>
									<xsl:if test="(//SHIFT_CD) = 2">Second (Evening)</xsl:if>
									<xsl:if test="(//SHIFT_CD) = 3">Third (Night)</xsl:if>
									<xsl:if test="(//SHIFT_CD) = 4">Rotating</xsl:if>
									<xsl:if test="(//SHIFT_CD) = 5">Split</xsl:if>
									<xsl:if test="(//SHIFT_CD) = 6">Varies</xsl:if>
								</font>
							</xsl:if>

							<xsl:if test="(//OVERTIMEREQUIRED) = 1">

								<font class="font14" style="color:rgb(0,0,139);">
									<br/>Over Time Required:
								</font>

								<font class="font13">
									Yes
								</font>
							</xsl:if>
						</td>
					</tr>

					<xsl:if test="(//ARABIC) = 1 or (//CHINESE) = 1 or (//ENGLISH) = 1 or (//FRENCH) = 1 or (//GERMAN) = 1 or (//ITALIAN) = 1 or (//JAPANESE) = 1 or (//RUSSIAN) = 1 or (//SPANISH) = 1 or (//OTHER) = 1">
						<tr width="100%">
							<td>
								<font class="font14" style="color:rgb(0,0,139);">
									<br/>Languages:
								</font>
							</td>
						</tr>
					</xsl:if>

					<tr width="100%">
						<td>
							<font class="font13">

								<xsl:if test="(//ARABIC) = 1">
									Arabic,
								</xsl:if>
								<xsl:if test="(//CHINESE) = 1">
									Chinese,
								</xsl:if>
								<xsl:if test="(//ENGLISH) = 1">
									English,
								</xsl:if>
								<xsl:if test="(//FRENCH) = 1">
									French,
								</xsl:if>
								<xsl:if test="(//GERMAN) = 1">
									German,
								</xsl:if>
								<xsl:if test="(//ITALIAN) = 1">
									Italian,
								</xsl:if>
								<xsl:if test="(//JAPANESE) = 1">
									Japanese,
								</xsl:if>
								<xsl:if test="(//RUSSIAN) = 1">
									Russian,
								</xsl:if>
								<xsl:if test="(//SPANISH) = 1">
									Spanish
								</xsl:if>
								<xsl:if test="(//OTHER) = 1">

								</xsl:if>

							</font>
						</td>
					</tr>

					<tr/>

				</TABLE>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="JOB_LAST_OPEN_DATE">

		<xsl:variable name="year">
			<xsl:value-of select="substring-before(. ,'-')" />
		</xsl:variable>
		<xsl:variable name="day-temp">
			<xsl:value-of select="substring-after(.,'-')" />
		</xsl:variable>
		<xsl:variable name="mo">
			<xsl:value-of select="substring-before($day-temp,'-')" />
		</xsl:variable>
		<xsl:variable name="day">
			<xsl:value-of select="substring-after($day-temp,'-')" />
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="$day = '1' or $day = '01'">1st</xsl:when>
			<xsl:when test="$day = '2' or $day = '02'">2nd</xsl:when>
			<xsl:when test="$day = '3' or $day = '03'">3rd</xsl:when>
			<xsl:when test="$day = '4' or $day = '04'">4th</xsl:when>
			<xsl:when test="$day = '5' or $day = '05'">5th</xsl:when>
			<xsl:when test="$day = '6' or $day = '06'">6th</xsl:when>
			<xsl:when test="$day = '7' or $day = '07'">7th</xsl:when>
			<xsl:when test="$day = '8' or $day = '08'">8th</xsl:when>
			<xsl:when test="$day = '9' or $day = '09'">9th</xsl:when>
			<xsl:when test="$day = '10'">10th</xsl:when>
			<xsl:when test="$day = '11'">11th</xsl:when>
			<xsl:when test="$day = '12'">12th</xsl:when>
			<xsl:when test="$day = '13'">13th</xsl:when>
			<xsl:when test="$day = '14'">14th</xsl:when>
			<xsl:when test="$day = '15'">15th</xsl:when>
			<xsl:when test="$day = '16'">16th</xsl:when>
			<xsl:when test="$day = '17'">17th</xsl:when>
			<xsl:when test="$day = '18'">18th</xsl:when>
			<xsl:when test="$day = '19'">19th</xsl:when>
			<xsl:when test="$day = '20'">20th</xsl:when>
			<xsl:when test="$day = '21'">21st</xsl:when>
			<xsl:when test="$day = '22'">22nd</xsl:when>
			<xsl:when test="$day = '23'">23rd</xsl:when>
			<xsl:when test="$day = '24'">24th</xsl:when>
			<xsl:when test="$day = '25'">25th</xsl:when>
			<xsl:when test="$day = '26'">26th</xsl:when>
			<xsl:when test="$day = '27'">27th</xsl:when>
			<xsl:when test="$day = '28'">28th</xsl:when>
			<xsl:when test="$day = '29'">29th</xsl:when>
			<xsl:when test="$day = '30'">30th</xsl:when>
			<xsl:when test="$day = '31'">31st</xsl:when>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:choose>
			<xsl:when test="$mo = '1' or $mo = '01'">January</xsl:when>
			<xsl:when test="$mo = '2' or $mo = '02'">February</xsl:when>
			<xsl:when test="$mo = '3' or $mo = '03'">March</xsl:when>
			<xsl:when test="$mo = '4' or $mo = '04'">April</xsl:when>
			<xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
			<xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
			<xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
			<xsl:when test="$mo = '8' or $mo = '08'">August</xsl:when>
			<xsl:when test="$mo = '9' or $mo = '09'">September</xsl:when>
			<xsl:when test="$mo = '10'">October</xsl:when>
			<xsl:when test="$mo = '11'">November</xsl:when>
			<xsl:when test="$mo = '12'">December</xsl:when>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$year"/>

	</xsl:template>

	<xsl:template match="text()" name="insertBreaks">
		<xsl:param name="pText" select="."/>

		<xsl:choose>
			<xsl:when test="not(contains($pText, '&#xA;'))">
				<xsl:copy-of select="$pText"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring-before($pText, '&#xA;')"/>
				<br />
				<xsl:call-template name="insertBreaks">
					<xsl:with-param name="pText" select="substring-after($pText, '&#xA;')"/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
