<!-- 
    ************************************************************
    Copyright(c) 2010, Burning Glass Technologies
    XSL for <> for Focus
    ************************************************************
    Version 1.0
    
    Change Log: 
	  26/JUL/2011 RRC Created the XSL
    31/AUG/2011 RRC do not display employer, if the emploer is "Self" in experience.
    02/SEP/2011 RRC Added  courses, honors, gpa, activities in education.
    14/SEP/2011 AKV Dynamic font size change.
	15/SEP/2011 AKV replacing bulletchar with Unordered list <UL>.
	16/SEP/2011 AKV Default font values are set for fontsize13, fontsize14 and fontsize16 parameters.
	16/SEP/2011 AKV Experience Date issue resolved if it is not in proper format 'to' or '-' appears. 
	20/SEP/2011 AKV Address2 is added in address section.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="html" version="1.0" omit-xml-declaration="no" indent="yes" encoding="iso-8859-1"/>
  <xsl:variable name="bulletchar" select="'&#8226;'"/>
  <xsl:variable name="lineFeed" select="'FC_CRL'"/>
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:param name="fontsize13" select="'13px'"/>
  <xsl:param name="fontsize14" select="'14px'"/>
  <xsl:param name="fontsize16" select="'16px'"/>
  <xsl:template match="/">
    <style type="text/css">
      .font13 { font-family:arial;font-size:<xsl:value-of select="$fontsize13"/>; }
      .font14 { font-family:arial;font-size:<xsl:value-of select="$fontsize14"/>; }
      .font16 { font-family:arial;font-size:<xsl:value-of select="$fontsize16"/>; font-weight:bold; }
      .alignRight{text-align: right; }
    </style>
    <html>
      <body>
        <font class="font13">
          <!--<div class="container">-->
          <xsl:if test="count(//contact)>0">
            <table border="0" width="880">
              <tr>
                <td width="25%">
                  <font class="font16" >

                  </font>
                </td>
                <td width="50%" align="center">
                  <font class="font16">
                    <xsl:value-of select="(//givenname)[1]"/>
                    <xsl:text>   </xsl:text>
                    <xsl:value-of select="(//givenname)[2]"/>
                    <xsl:text>   </xsl:text>
                    <xsl:value-of select="(//surname)[1]"/>
                  </font>

                  <xsl:if test="count((//email)[1])>0">
                    <br/>
                    <font class="font13">
                      <xsl:apply-templates select="(//resume/contact/email)[1]"/>
                    </font >
                  </xsl:if>
                  <xsl:if test="string-length((//phone)[3])>0">
                    <br/>
                    <font class="font13">
                      <xsl:apply-templates select="(//resume/contact/phone)[3]"/>
                    </font >
                  </xsl:if>
                </td>
                <td width="25%" align="right">
                  <font class="font13">
                    <xsl:value-of select="(//resume/contact/address/street)[1]"/>
                    <xsl:if test="string-length((//resume/contact/address)[1]/street[2])>0">
                      <br/>
                      <xsl:value-of select="(//resume/contact/address)[1]/street[2]"/>
                    </xsl:if>
                    <xsl:if test="count((//resume/contact/address/city)[1]) > 0">
                      <br/>
                      <xsl:value-of select="(//resume/contact/address/city)[1]"/>
                    </xsl:if>
                    <xsl:if test="count((//resume/contact/address/town)[1]) > 0">
                      <br/>
                      <xsl:value-of select="(//resume/contact/address/town)[1]"/>
                    </xsl:if>

                    <xsl:if test="count((//resume/contact/address/state)[1]) > 0">
                      <xsl:if test="count((//resume/contact/address)[1]/city[1])>0">
                        <xsl:text>, </xsl:text>
                      </xsl:if>
                      <xsl:if test="(//resume/contact/address)[1]/state[1] != 'ZZ'">
                        <xsl:value-of select="(//resume/contact/address)[1]/state[1]"/>
                      </xsl:if>
                    </xsl:if>

                    <xsl:if test="(//resume/contact/address/country)[1] = 'UK'">
                      <xsl:if test="count((//resume/contact/address/county)[1]) > 0">
                        <xsl:text>, </xsl:text>
                        <xsl:value-of select="(//resume/contact/address/county)[1]"/>
                      </xsl:if>
                    </xsl:if>

                    <xsl:if test="count((//resume/contact/address/postalcode)[1]) > 0">
                      <xsl:text> </xsl:text>
                      <xsl:value-of select="(//resume/contact/address/postalcode)[1]"/>
                    </xsl:if>

                    <xsl:if test="string-length((//phone)[1])>0">
                      <br/>
                      <xsl:apply-templates select="(//resume/contact/phone)[1]"/>
                    </xsl:if>
                    <!--<xsl:if test="string-length((//phone)[2])>0">
                      <br/>
                      <xsl:apply-templates select="(//resume/contact/phone)[2]"/>
                    </xsl:if>-->
                  </font>
                </td>
              </tr>
            </table>

            <table>
              <tr>
                <td></td>
              </tr>
            </table>
          </xsl:if>
          <table width="880" border="0">
            <tr>
              <td>
								<!-- SUMMARY (top of resume)-->
								<xsl:if test="count(//summary/summary)>0">
									<xsl:if test="//summary/summary !=''">
										<table border="0" cellpadding="0" cellspacing="0" width="100%">
											<tr>
												<td width="100%" valign="top">
													<font class="font13">
														<u>
															<b>Summary:</b>
														</u>
													
													</font>
													
												</td>
											</tr>
											<tr>
												<td>
													<table border="0" width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td width="5%"></td>
															<td width="95%" cellpadding="0" cellspacing="0">
																<font class="font13">
																	<div style="text-align: justify;">
																		<xsl:value-of select="//summary/summary"/>
																		<br/>
																	</div>
																</font>
															</td>
														</tr>
													</table>
												</td>
											</tr>

										</table>
									</xsl:if>
								</xsl:if>
								<!-- OBJECTIVE -->
                <xsl:if test="count(//summary/objective)>0">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="100%" valign="top">
                        <font class="font13">
                          <u>
                            <b>Objective:</b>
                          </u>
                        </font>
                      </td>
                    </tr>
                    <tr>
                      <td >
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="5%"></td>
                            <td width="95%" cellpadding="0" cellspacing="0">
                              <font class="font13">
                                <div style="text-align: justify;">
                                  <xsl:value-of select="//summary/objective"/>
                                </div>
                              </font>
                            </td>
                          </tr>
                        </table >
                      </td>
                    </tr>

                  </table>
                </xsl:if>

                <xsl:if test="count(//school) > 0">
                  <br/>
                </xsl:if>

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td width="100%" valign="top">
                      <!-- Education -->
                      <xsl:if test="string-length(//school)>0">
                        <font class="font13">
                          <u>
                            <b>Education:</b>
                          </u>
                        </font>
                      </xsl:if>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <xsl:for-each select="//education">
                        <table border="0" width="100%" cellpadding="0" cellspacing="0">
                          <xsl:for-each select="school">
                            <tr>

                              <td width="80%" valign="top" cellpadding="0" cellspacing="0">
                                <font class="font13">
                                  <xsl:if test="string-length(institution[1]) > 0 ">
                                    <b>
                                      <xsl:apply-templates select="institution[1]"/>
                                    </b>
                                  </xsl:if>

                                  <xsl:if test="count((address/state_fullname)[1]) > 0 and address/state_fullname[1] !='Outside U.S.' ">
                                    <xsl:text>, </xsl:text>
                                    <xsl:value-of select="(address/state_fullname)[1]"/>
                                  </xsl:if>
                                  <xsl:if test="count((address/country)[1]) > 0 and address/country[1] !='US' ">
                                    <xsl:text>,</xsl:text>
                                    <xsl:value-of select="(address/country)[1]"/>
                                  </xsl:if>


                                </font >
                              </td>
                              <td width="20%"  align="right">
                                <font class="font13">
                                  <xsl:if test="string-length(completiondate[1]) > 0 ">
                                    <xsl:apply-templates select="./completiondate"/>
                                  </xsl:if>
                                </font >
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <td width="5%"></td>
                                  <td width="95%" valign="top" cellpadding="0" cellspacing="0">
                                    <font class="font13">
                                      <xsl:text>&#8226; </xsl:text>
                                      <xsl:if test="count(degree)>0">
                                        <xsl:value-of select="degree"/>
                                      </xsl:if>
                                      <xsl:if test="count(major)>0">
                                        <xsl:text> in </xsl:text>
                                        <xsl:value-of select="major"/>
                                      </xsl:if>
                                    </font >
                                  </td>
                                </table >
                              </td>
                              <td> </td>
                            </tr>
                            <tr>
                              <td>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <td width="5%"></td>
                                  <td width="95%" valign="top" cellpadding="0" cellspacing="0">
                                    <font class="font13">
                                      <xsl:if test="string-length(courses[1]) > 0">
                                        <xsl:value-of select="courses[1]"/>
                                      </xsl:if>
                                      <xsl:if test="string-length(courses[1]) > 0 and string-length(honors[1]) > 0">
                                        <xsl:text>, </xsl:text>
                                      </xsl:if>
                                      <xsl:if test="string-length(honors[1]) > 0">
                                        <xsl:value-of select="honors[1]"/>
                                      </xsl:if>
                                      <xsl:if test="(string-length(courses[1]) > 0 or string-length(honors[1]) > 0) and string-length(gpa[1]) > 0 ">
                                        <xsl:text>, </xsl:text>
                                      </xsl:if>
                                      <xsl:if test="string-length(gpa[1]) > 0">
                                        <b>GPA :  </b>
                                        <xsl:value-of select="gpa[1]"/>
                                      </xsl:if>
                                      <xsl:if test="(string-length(courses[1]) > 0 or string-length(honors[1]) > 0 or string-length(gpa[1]) > 0 ) and string-length(activities[1]) > 0  ">
                                        <xsl:text>, </xsl:text>
                                      </xsl:if>
                                      <xsl:if test="string-length(activities[1]) > 0">
                                        <xsl:value-of select="activities[1]"/>
                                      </xsl:if>
                                    </font >
                                  </td>
                                </table >
                              </td>
                              <td> </td>
                            </tr>

                            <tr>
                              <td>
                                <br/>
                              </td>
                            </tr>
                          </xsl:for-each>
                        </table>
                      </xsl:for-each>
                    </td>
                  </tr>
                </table>

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                    <td width="20%" valign="top">
                      <!-- Experience -->
                      <xsl:if test="count(//job) > 0">
                        <font class="font13">
                          <u>
                            <b>Experience:</b>
                          </u>
                        </font>
                      </xsl:if>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <xsl:for-each select="//experience">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">


                          <xsl:for-each select="job">

                            <xsl:if test="@pos !=''">
                              <tr>
                                <td  align="left"  width="75%">
                                  <font class="font13">
                                    <!--<xsl:if test="count(title)>0">
                                        <xsl:value-of select="title"/>
                                      </xsl:if>-->
                                    <xsl:if test="count(employer)>0">
                                      <b>
                                        <xsl:if test="employer != 'Self' and string-length(employer) > 0">
                                          <xsl:value-of select="employer"/>
                                          <xsl:text>, </xsl:text>
                                        </xsl:if>
                                      </b>
                                    </xsl:if>
                                    <xsl:if test="count(address/city[1]) > 0">
                                      <xsl:value-of select="address/city[1]"/>
                                    </xsl:if>
                                    <xsl:if test="count((address)[1]/city[1]) = 0 and count((address)[1]/town[1]) > 0">
                                      <xsl:text>, </xsl:text>
                                      <xsl:value-of select="(address)[1]/town[1]"/>
                                    </xsl:if>
                                    <xsl:if test="count(address/state[1]) > 0">
                                      <xsl:text>,</xsl:text>
                                      <xsl:if test="address/state[1] != 'ZZ'">
                                        <xsl:value-of select="address/state[1]"/>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="count((address)[1]/state[1]) = 0 and count((address)[1]/county[1]) > 0">
                                      <xsl:text>,</xsl:text>
                                      <xsl:value-of select="(address)[1]/county[1]"/>
                                    </xsl:if>
                                    <xsl:if test="(count(./address) > 0 and count(./address/state[1]) = 0 and count(./address/county[1]) = 0  ) and ( count(./address/city[1]) = 0 and count(./address/town[1]) = 0 ) ">
                                      <xsl:text>,</xsl:text>
                                      <xsl:value-of select="./address[1]"/>
                                    </xsl:if>

                                  </font >
                                </td >

                                <td  align="right" width="25%">
                                  <font class="font13">
                                    <xsl:if test="string-length(daterange/start) > 0">
                                      <xsl:if test="string-length(daterange/end) > 0 and (daterange/start = daterange/end)">
                                        <xsl:apply-templates select="daterange/start"/>
                                      </xsl:if>
                                      <xsl:if test="string-length(daterange/end) > 0 and (daterange/start != daterange/end)">
                                        <xsl:apply-templates select="daterange/start"/>
                                        <xsl:variable name="temp1End">
                                          <xsl:value-of select="substring-before(daterange/end ,'/')" />
                                        </xsl:variable>
                                        <xsl:if test="string-length(daterange/end) > 0 and string-length($temp1End) > 0">
                                          <xsl:text> - </xsl:text>
                                        </xsl:if>
                                        <xsl:choose>
                                          <xsl:when test="daterange/end/@currentjob = '1'">
                                            <xsl:text>Present</xsl:text>
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:apply-templates select="daterange/end"/>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </xsl:if>
                                      <xsl:if test="string-length(daterange/end) = '0'">
                                        <xsl:apply-templates select="daterange/start"/>
                                        <!--<xsl:text> - xxxx</xsl:text>-->
                                      </xsl:if>
                                    </xsl:if>
                                  </font >
                                </td>
                              </tr>
                              <tr>
                                <td colspan="2">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td width="5%"></td>
                                      <td width="95%">
                                        <font class="font13">
                                          <div style="text-align: justify;">
                                            <xsl:if test="count(description)>0">
                                              <xsl:variable name="stripedText">
                                                <xsl:value-of select="translate(description,$lineFeed,'FC.CRL')"/>
                                              </xsl:variable>
                                              <ul>
                                                <xsl:call-template name="descriptionsplit">
                                                  <xsl:with-param name="string" select="concat('CRLF',translate($stripedText,'..','.'))"/>
                                                </xsl:call-template>
                                              </ul>
                                            </xsl:if>
                                          </div>
                                        </font>
                                      </td>
                                    </tr >
                                  </table >
                                </td>
                              </tr>
                              <tr>
                                <td/>
                              </tr>
                            </xsl:if>
                          </xsl:for-each>
                          <xsl:for-each select="job">
                            <xsl:if test="string-length(@pos)=0">
                              <tr>
                                <td  align="left"  width="75%">
                                  <font class="font13">
                                    <!--<xsl:if test="count(title)>0">
                                        <xsl:value-of select="title"/>
                                      </xsl:if>-->
                                    <xsl:if test="count(employer)>0">
                                      <b>
                                        <xsl:if test="employer != 'Self' and string-length(employer) > 0">
                                          <xsl:value-of select="employer"/>
                                          <xsl:text>, </xsl:text>
                                        </xsl:if>
                                      </b>
                                    </xsl:if>
                                    <xsl:if test="count(address/city[1]) > 0">
                                      <xsl:value-of select="address/city[1]"/>
                                    </xsl:if>
                                    <xsl:if test="count((address)[1]/city[1]) = 0 and count((address)[1]/town[1]) > 0">
                                      <xsl:text>, </xsl:text>
                                      <xsl:value-of select="(address)[1]/town[1]"/>
                                    </xsl:if>
                                    <xsl:if test="count(address/state[1]) > 0">
                                      <xsl:text>,</xsl:text>
                                      <xsl:if test="address/state[1] != 'ZZ'">
                                        <xsl:value-of select="address/state[1]"/>
                                      </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="count((address)[1]/state[1]) = 0 and count((address)[1]/county[1]) > 0">
                                      <xsl:text>,</xsl:text>
                                      <xsl:value-of select="(address)[1]/county[1]"/>
                                    </xsl:if>
                                    <xsl:if test="(count(./address) > 0 and count(./address/state[1]) = 0 and count(./address/county[1]) = 0  ) and ( count(./address/city[1]) = 0 and count(./address/town[1]) = 0 ) ">
                                      <xsl:text>,</xsl:text>
                                      <xsl:value-of select="./address[1]"/>
                                    </xsl:if>

                                  </font >
                                </td >

                                <td  align="right" width="25%">
                                  <font class="font13">
                                    <xsl:if test="string-length(daterange/start) > 0">
                                      <xsl:if test="string-length(daterange/end) > 0 and (daterange/start = daterange/end)">
                                        <xsl:apply-templates select="daterange/start"/>
                                      </xsl:if>
                                      <xsl:if test="string-length(daterange/end) > 0 and (daterange/start != daterange/end)">
                                        <xsl:apply-templates select="daterange/start"/>
                                        <xsl:variable name="temp2End">
                                          <xsl:value-of select="substring-before(daterange/end ,'/')" />
                                        </xsl:variable>
                                        <xsl:if test="string-length(daterange/end) > 0 and string-length($temp2End) > 0">
                                          <xsl:text> - </xsl:text>
                                        </xsl:if>
                                        <xsl:choose>
                                          <xsl:when test="daterange/end/@currentjob = '1'">
                                            <xsl:text>Present</xsl:text>
                                          </xsl:when>
                                          <xsl:otherwise>
                                            <xsl:apply-templates select="daterange/end"/>
                                          </xsl:otherwise>
                                        </xsl:choose>
                                      </xsl:if>
                                      <xsl:if test="string-length(daterange/end) = '0'">
                                        <xsl:apply-templates select="daterange/start"/>
                                        <!--<xsl:text> - xxxx</xsl:text>-->
                                      </xsl:if>
                                    </xsl:if>
                                  </font >
                                </td>
                              </tr>

                              <tr>
                                <td width="100%" colspan="2">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                      <td width="5%"></td>
                                      <td width="95%">
                                        <font class="font13">
                                          <xsl:if test="count(description)>0">
                                            <br/>
                                          </xsl:if>
                                          <div style="text-align: justify;">
                                            <xsl:if test="count(description)>0">
                                              <xsl:variable name="stripedText">
                                                <xsl:value-of select="translate(description,$lineFeed,'FC.CRL')"/>
                                              </xsl:variable>
                                              <ul>
                                                <xsl:call-template name="descriptionsplit">
                                                  <xsl:with-param name="string" select="concat('CRLF',translate($stripedText,'..','.'))"/>
                                                </xsl:call-template>
                                              </ul>
                                            </xsl:if>
                                          </div>
                                        </font>
                                      </td>
                                    </tr >
                                  </table >
                                </td >
                              </tr>
                              <tr>
                                <td/>
                              </tr>
                            </xsl:if>
                          </xsl:for-each>
                        </table>
                      </xsl:for-each>
                    </td>
                  </tr>
                </table>

                <!-- Veteran experience -->
                <!--<xsl:if test="count(//veteran/rank) > 0 and (//veteran/vet_era) != 3">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="20%" valign="top">
                        <font class="font13">
                          <b>Military Service</b>
                          <br/>

                        </font>
                      </td>

                      <xsl:if test="count(//veteran/rank) > 0">

                        <td width="80%">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                              <td  align="left" width="80%">
                                <font class="font13">
                                  <xsl:if test="string-length(//veteran/rank) > 0 and ((//veteran/rank) != 'Select Rank' and (//veteran/rank) != '0')">
                                    <xsl:value-of select="//veteran/rank"/>
                                  </xsl:if>
                                  <xsl:if test="string-length(//veteran/moc) > 0">
                                    <xsl:if test="string-length(//veteran/rank) > 0 and ((//veteran/rank) != 'Select Rank' and (//veteran/rank) != '0')">
                                      <xsl:text>, </xsl:text>
                                    </xsl:if>
                                    <xsl:if test="string-length(//veteran/mostext) > 0">
                                      <xsl:value-of select="//veteran/mostext"/>
                                    </xsl:if>
                                    <xsl:if test="string-length(//veteran/mostext) = 0">
                                      <xsl:value-of select="//veteran/moc"/>
                                    </xsl:if>
                                  </xsl:if>
                                </font>
                              </td>
                              <td align="right" width="20%">
                                <font class="font13">
                                  <xsl:if test="string-length(//veteran/vet_start_date) > 0">
                                    <xsl:if test="string-length(//veteran/vet_end_date) > 0 and (//veteran/vet_start_date = //veteran/vet_end_date)">
                                      <xsl:apply-templates select="vet_start_date"/>
                                    </xsl:if>
                                    <xsl:if test="string-length(//veteran/vet_end_date) > 0 and (//veteran/vet_start_date != //veteran/vet_end_date)">
                                      <xsl:apply-templates select="//veteran/vet_start_date"/>
                                      <xsl:text> - </xsl:text>
                                      <xsl:apply-templates select="//veteran/vet_end_date"/>

                                    </xsl:if>
                                    <xsl:if test="string-length(//veteran/vet_end_date) = '0'">
                                      <xsl:apply-templates select="//veteran/vet_start_date"/>

                                    </xsl:if>
                                  </xsl:if>
                                </font>
                              </td>
                            </tr>
                            <tr>
                              <td valign="top">
                                <b>
                                  <font class="font13">
                                    <xsl:if test="(//veteran/branch_of_service) = 'USAF' ">
                                      United States Air Force
                                      <br/>
                                    </xsl:if>
                                    <xsl:if test="(//veteran/branch_of_service) = 'USARMY' ">
                                      United States Army
                                      <br/>
                                    </xsl:if>
                                    <xsl:if test="(//veteran/branch_of_service) = 'USCG' ">
                                      United States Coast Guard
                                      <br/>
                                    </xsl:if>
                                    <xsl:if test="(//veteran/branch_of_service) = 'USMC' ">
                                      United States Marine Corps
                                      <br/>
                                    </xsl:if>
                                    <xsl:if test="(//veteran/branch_of_service) = 'USNAVY' ">
                                      United States Navy
                                      <br/>
                                    </xsl:if>

                                  </font>

                                </b>
                              </td>
                            </tr>
                            <tr>
                              <td valign="top">

                                <font class="font13">
                                  <xsl:if test="count(//veteran/unit_affiliation) > 0">
                                    <xsl:value-of select="(//veteran/unit_affiliation)"/>
                                  </xsl:if>
                                </font>

                              </td>
                            </tr>
                          </table>
                        </td>
                      </xsl:if>
                    </tr>

                  </table>
                  <br/>
                </xsl:if>-->



                <!-- Publications -->
                <!--<xsl:if test="count(//professional/publications)>0">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="20%" valign="top">
                        <font class="font13">
                          <b>Publications</b>
                        </font>
                      </td>
                      <td width="80%">
                        <font class="font13">
                          <div style="text-align: justify;">
                            <xsl:value-of select="//professional/publications"/>
                          </div>
                        </font>
                      </td>
                    </tr>
                  </table>
                </xsl:if>-->


                <!-- Intenships (bottom of resume)-->
                <xsl:if test="count(//special/internships) > 0">
                  <br/>
                </xsl:if>
                <xsl:if test="count(//special/internships)>0">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="20%" valign="top">
                        <font class="font13">
                          <u>
                            <b>Internships:</b>
                          </u>
                        </font>
                      </td>
                    </tr>
                    <tr>
                      <td width="80%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td width="5%"></td>
                            <td width="95%">
                              <font class="font13">
                                <div style="text-align: justify;">
                                  <xsl:value-of select="//special/internships"/>
                                </div>
                              </font>
                            </td>
                          </tr >
                        </table >
                      </td>
                    </tr>
                  </table>
                </xsl:if>

                <!-- Skills -->
                <xsl:if test="string-length(//skills/skills) > 0">
                  <br/>
                </xsl:if>
                <xsl:if test="string-length(//skills/skills)>0">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="20%" valign="top">
                        <font class="font13">
                          <u>
                            <b>Skills:</b>
                          </u>
                        </font>
                      </td>
                    </tr>
                    <tr>
                      <td width="80%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td width="5%"></td>
                            <td width="95%">
                              <font class="font13">
                                <div style="text-align: justify;">
                                  <xsl:value-of select="//skills/skills"/>
                                </div>
                              </font>
                            </td>
                          </tr >
                        </table >
                      </td>
                    </tr>
                  </table>
                </xsl:if>


                <xsl:if test="count(//special/volunteeractivities) > 0">
                  <br/>
                </xsl:if>

                <!--Volunteer Activities-->
                <xsl:if test="count(//special/volunteeractivities)>0">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="20%" valign="top">
                        <font class="font13">
                          <u>
                            <b>Volunteer Activities:</b>
                          </u>
                        </font>
                      </td>
                    </tr>
                    <tr>
                      <td width="80%">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                          <tr>
                            <td width="5%"></td>
                            <td width="95%">
                              <font class="font13">
                                <div style="text-align: justify;">
                                  <xsl:value-of select="//special/volunteeractivities"/>
                                </div>
                              </font>
                            </td>
                          </tr >
                        </table >
                      </td>
                    </tr>
                  </table>
                </xsl:if>


                <!--<xsl:if test="count(//references) > 0">
                  <br/>
                </xsl:if>-->

                <!-- References -->
                <!--<xsl:if test="count(//references)>0">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="20%" valign="top">
                        <font class="font13">
                          <b>References</b>
                        </font>
                      </td>
                      <td width="80%">
                        <font class="font13">
                          <div style="text-align: justify;">
                            <xsl:value-of select="//references"/>
                          </div>
                        </font>
                      </td>
                    </tr>
                  </table>
                </xsl:if>-->
              </td>
            </tr>
          </table>
          <!--</div>-->
        </font>
        <br/>
      </body>
    </html>


  </xsl:template>
  <xsl:template match="phone">

    <xsl:if test="(//resume/contact/address/country)[1] = 'US'">
      <xsl:choose>
        <xsl:when test="@type='work'">
          <xsl:text> Work: </xsl:text>
          <xsl:value-of select="format-number(.,'(###)-###-####')"/>
        </xsl:when>
        <xsl:when test="@type='cell'">
          <xsl:text> Cell: </xsl:text>
          <xsl:value-of select="format-number(.,'(###)-###-####')"/>
        </xsl:when>
        <xsl:when test="@type='fax'">
          <xsl:text> Fax: </xsl:text>
          <xsl:value-of select="format-number(.,'(###)-###-####')"/>
        </xsl:when>
        <xsl:when test="@type='pager'">
          <xsl:text> Pager: </xsl:text>
          <xsl:value-of select="format-number(.,'(###)-###-####')"/>
        </xsl:when>
        <xsl:when test="@type='home'">
          <xsl:text> Home: </xsl:text>
          <xsl:value-of select="format-number(.,'(###)-###-####')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text> Non US: </xsl:text>
          <xsl:value-of select="."/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>

    <xsl:if test="(//resume/contact/address/country)[1] != 'US'">
      <xsl:choose>
        <xsl:when test="@type='work'">
          <xsl:value-of select="."/>
          <xsl:text> Work:</xsl:text>
        </xsl:when>
        <xsl:when test="@type='cell'">
          <xsl:value-of select="."/>
          <xsl:text> Cell:</xsl:text>
        </xsl:when>
        <xsl:when test="@type='fax'">
          <xsl:value-of select="."/>
          <xsl:text> Fax:</xsl:text>
        </xsl:when>
        <xsl:when test="@type='pager'">
          <xsl:value-of select="."/>
          <xsl:text> Pager:</xsl:text>
        </xsl:when>
        <xsl:when test="@type='home'">
          <xsl:value-of select="."/>
          <xsl:text> Home:</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="."/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="descriptionsplit">
    <xsl:param name="string"/>
    <xsl:choose>
      <xsl:when test="contains($string,'.')">
        <!--General String :<xsl:value-of select="$string"/><br/>-->
        <xsl:if test="string-length(substring-before($string,'.')) > 0">
          <!--String Before (.) Value:<xsl:variable name="temp" select="substring-before($string, '.')"/><xsl:value-of select="$temp"/> <br/>-->
          <!--<xsl:value-of select="substring($temp,string-length($temp)-1)"/>-->
          <xsl:choose>
            <xsl:when test="normalize-space(substring-before($string, '.')) != 'FC' and starts-with(substring-before($string, '.'),'CRLF') and not(starts-with(substring-before($string,'.'),'CRLFFC')) and not(starts-with(substring-before($string,'.'),'FC'))">
              <li>
                <xsl:variable name="tempString">
                  <xsl:choose>
                    <!-- ******* If the String contains last two charactera as FC then remove it ******** -->
                    <xsl:when test="substring(substring-before($string, '.'),string-length(substring-before($string, '.'))-1) = 'FC'">
                      <xsl:value-of select="translate(substring(substring-before($string, '.'),1,string-length(substring-before($string, '.'))-2),'*:-','')" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="translate(substring-before($string, '.'),'*:-','')"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <xsl:variable name="tempStr">
                  <xsl:value-of select="substring($tempString,5)"/>
                  <xsl:if test="substring(normalize-space($tempString),string-length($tempString)) !='.'">
                    <xsl:text>.</xsl:text>
                  </xsl:if>
                </xsl:variable>
                <!--<br/>: String Last Value :<xsl:value-of select="substring(normalize-space($tempString),string-length($tempString))"/>
									  <br/>:TempString:<xsl:value-of select="$tempStr"/><br/>-->
                <xsl:variable name="strippedString" select="normalize-space($tempStr)"/>
                <!-- ********** Handled for string Starting with Comma ************ -->
                <xsl:choose>
                  <xsl:when test="starts-with($strippedString,',')">
                    <xsl:variable name="commaRemovedString" select="normalize-space(substring($strippedString,2))"/>
                    <xsl:value-of select="concat(translate(substring($commaRemovedString,1,1),$smallcase,$uppercase),substring($commaRemovedString,2))"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="concat(translate(substring($strippedString,1,1),$smallcase,$uppercase),substring($strippedString,2))"/>
                  </xsl:otherwise>
                </xsl:choose>
                <!-- *********** For Handling J.D. sequence dot character ************-->
                <xsl:variable name="testAfterString" select="substring-after($string,'.')"/>
                <xsl:variable name="testBeforeString" select="normalize-space(substring-before($testAfterString,'.'))"/>
                <xsl:if test="not(starts-with($testAfterString,' ')) and string-length(normalize-space($testBeforeString)) > 0 and not(starts-with($testAfterString,'CRLF')) and not(starts-with($testAfterString,'FC'))">
                  <xsl:value-of select="$testBeforeString"/>
                  <xsl:text>.</xsl:text>
                  <!--<br/>: Last Value:<xsl:value-of select="substring($testBeforeString,string-length($testBeforeString))"/>
											  <xsl:if test="substring($testBeforeString,string-length($testBeforeString)) != '.'"><xsl:text>.</xsl:text></xsl:if>
											  <br/>Temp :<xsl:value-of select="$testBeforeString"/>: Not Starts with Space :<xsl:value-of select="not(starts-with($testAfterString,' '))"-->
                </xsl:if>
                <!-- *********************************************************************** -->
              </li>
            </xsl:when>
            <!-- *****************************************************  OtherWise Section *********************************** -->
            <xsl:otherwise>
              <xsl:if test="normalize-space(substring-before($string, '.')) != 'FC'">
                <xsl:if test="not(starts-with(substring-before($string,'.'),'CRLF'))">
                  <!--<br/>Otherwise Section : Input String :<xsl:value-of select="$string"/><br/>-->
                  <li>
                    <!--<xsl:text>Otherwise Section : </xsl:text>-->
                    <xsl:variable name="tempString">
                      <xsl:choose>
                        <xsl:when test="substring(substring-before($string, '.'),string-length(substring-before($string, '.'))-1) = 'FC'">
                          <xsl:value-of select="translate(substring(substring-before($string, '.'),1,string-length(substring-before($string, '.'))-2),'*:-','')" />
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="translate(substring-before($string, '.'),'*:-','')"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>
                    <!--<br/>Before String (.) Value : <xsl:value-of select="$tempString"/><br/>-->
                    <xsl:variable name="tempStr">
                      <xsl:value-of select="$tempString"/>
                      <xsl:text>.</xsl:text>
                    </xsl:variable>
                    <xsl:variable name="strippedString" select="normalize-space($tempStr)"/>
                    <!-- ********** Handled for string Starting with Comma ************ -->
                    <xsl:choose>
                      <xsl:when test="starts-with($strippedString,',')">
                        <xsl:variable name="commaRemovedString" select="normalize-space(substring($strippedString,2))"/>
                        <xsl:value-of select="concat(translate(substring($commaRemovedString,1,1),$smallcase,$uppercase),substring($commaRemovedString,2))"/>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="concat(translate(substring($strippedString,1,1),$smallcase,$uppercase),substring($strippedString,2))"/>
                      </xsl:otherwise>
                    </xsl:choose>
                    <!-- *********************************************************************** -->
                    <!-- *********** For Handling J.D. sequence dot character ************-->
                    <xsl:variable name="testAfterString" select="substring-after($string,'.')"/>
                    <xsl:variable name="testBeforeString" select="normalize-space(substring-before($testAfterString,'.'))"/>
                    <xsl:if test="not(starts-with($testAfterString,' ')) and not(starts-with($testAfterString,'CRLF')) and not(starts-with($testAfterString,'FC')) and string-length(normalize-space($testBeforeString)) > 0">
                      <xsl:value-of select="$testBeforeString"/>
                      <xsl:text>.</xsl:text>
                      <!--: Last Value:<xsl:value-of select="substring($testBeforeString,string-length($testBeforeString))"/>-->
                      <!--<xsl:if test="substring($testBeforeString,string-length($testBeforeString)) != '.'"><xsl:text>.</xsl:text></xsl:if>-->
                      <!--Temp :<xsl:value-of select="$testBeforeString"/>: Not Starts with Space :<xsl:value-of select="not(starts-with($testAfterString,' '))"/>-->
                    </xsl:if>
                    <!-- *********************************************************************** -->
                  </li>
                </xsl:if>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
        <!-- *********** For Handling J.D. sequence dot character ************-->
        <xsl:variable name="testAfterString" select="substring-after($string,'.')"/>
        <xsl:variable name="testBeforeString" select="substring-before($testAfterString,'.')"/>
        <xsl:choose>
          <xsl:when test="not(starts-with($testAfterString,' ')) and string-length(normalize-space($testBeforeString)) > 0 and not(starts-with($testAfterString,'CRLF')) and not(starts-with($testAfterString,'FC'))">
            <!--<br/>When Substring After (.) :<xsl:value-of select="substring-after($testAfterString,'.')"/><br/>-->
            <xsl:call-template name="descriptionsplit">
              <xsl:with-param name="string" select="substring-after($testAfterString,'.')"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <!--<br/>Othr Substring After (.) :<xsl:value-of select="substring-after($string,'.')"/><br/>-->
            <xsl:call-template name="descriptionsplit">
              <xsl:with-param name="string" select="substring-after($string,'.')"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="string-length(substring($string,5)) > 0 and not(starts-with($string,'CRLFFC')) and not(starts-with($string,'FC'))">
          <li>
            <xsl:variable name="tempStr">
              <xsl:if test="starts-with($string,'CRLF')">
                <xsl:value-of select="substring($string,5)"/>
              </xsl:if>
              <xsl:if test="not(starts-with($string,'CRLF'))">
                <xsl:value-of select="$string"/>
              </xsl:if>
            </xsl:variable>
            <xsl:variable name="strippedString" select="normalize-space($tempStr)"/>

            <!-- ********** Handled for string Starting with Comma ************ -->
            <xsl:choose>
              <xsl:when test="starts-with($strippedString,',')">
                <xsl:variable name="commaRemovedString" select="normalize-space(substring($strippedString,2))"/>
                <xsl:value-of select="concat(translate(substring($commaRemovedString,1,1),$smallcase,$uppercase),substring($commaRemovedString,2))"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:variable name="tmpStr" select="concat(translate(substring($strippedString,1,1),$smallcase,$uppercase),substring($strippedString,2))"/>
                <xsl:value-of select="concat($tmpStr,'.')"/>
              </xsl:otherwise>
            </xsl:choose>
          </li>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="start">
    <!--
    <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before(. ,'/')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring-after(.,'/')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$mo = '1' or $mo = '01'">January</xsl:when>
      <xsl:when test="$mo = '2' or $mo = '02'">February</xsl:when>
      <xsl:when test="$mo = '3' or $mo = '03'">March</xsl:when>
      <xsl:when test="$mo = '4' or $mo = '04'">April</xsl:when>
      <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
      <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
      <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
      <xsl:when test="$mo = '8' or $mo = '08'">August</xsl:when>
      <xsl:when test="$mo = '9' or $mo = '09'">September</xsl:when>
      <xsl:when test="$mo = '10'">October</xsl:when>
      <xsl:when test="$mo = '11'">November</xsl:when>
      <xsl:when test="$mo = '12'">December</xsl:when>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year"/>

  </xsl:template>

  <xsl:template match="end">
    <!--
    <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
   -->
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before(. ,'/')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring-after(.,'/')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$mo = '1' or $mo = '01'">January</xsl:when>
      <xsl:when test="$mo = '2' or $mo = '02'">February</xsl:when>
      <xsl:when test="$mo = '3' or $mo = '03'">March</xsl:when>
      <xsl:when test="$mo = '4' or $mo = '04'">April</xsl:when>
      <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
      <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
      <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
      <xsl:when test="$mo = '8' or $mo = '08'">August</xsl:when>
      <xsl:when test="$mo = '9' or $mo = '09'">September</xsl:when>
      <xsl:when test="$mo = '10'">October</xsl:when>
      <xsl:when test="$mo = '11'">November</xsl:when>
      <xsl:when test="$mo = '12'">December</xsl:when>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year"/>

  </xsl:template>

  <xsl:template match="vet_start_date">

    <!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
-->
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before(. ,'/')" />
    </xsl:variable>
    <xsl:variable name="day-temp">
      <xsl:value-of select="substring-after(.,'/')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring-after($day-temp,'/')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$mo = '1' or $mo = '01'">January</xsl:when>
      <xsl:when test="$mo = '2' or $mo = '02'">February</xsl:when>
      <xsl:when test="$mo = '3' or $mo = '03'">March</xsl:when>
      <xsl:when test="$mo = '4' or $mo = '04'">April</xsl:when>
      <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
      <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
      <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
      <xsl:when test="$mo = '8' or $mo = '08'">August</xsl:when>
      <xsl:when test="$mo = '9' or $mo = '09'">September</xsl:when>
      <xsl:when test="$mo = '10'">October</xsl:when>
      <xsl:when test="$mo = '11'">November</xsl:when>
      <xsl:when test="$mo = '12'">December</xsl:when>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year"/>


  </xsl:template>

  <xsl:template match="vet_end_date">
    <!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before(. ,'/')" />
    </xsl:variable>
    <xsl:variable name="day-temp">
      <xsl:value-of select="substring-after(.,'/')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring-after($day-temp,'/')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$mo = '1' or $mo = '01'">January</xsl:when>
      <xsl:when test="$mo = '2' or $mo = '02'">February</xsl:when>
      <xsl:when test="$mo = '3' or $mo = '03'">March</xsl:when>
      <xsl:when test="$mo = '4' or $mo = '04'">April</xsl:when>
      <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
      <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
      <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
      <xsl:when test="$mo = '8' or $mo = '08'">August</xsl:when>
      <xsl:when test="$mo = '9' or $mo = '09'">September</xsl:when>
      <xsl:when test="$mo = '10'">October</xsl:when>
      <xsl:when test="$mo = '11'">November</xsl:when>
      <xsl:when test="$mo = '12'">December</xsl:when>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year"/>

  </xsl:template>


  <xsl:template match="completion_date">

    <!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
-->

    <xsl:variable name="year">
      <xsl:value-of select="substring-after(.,'/')" />
    </xsl:variable>

    <xsl:value-of select="$year"/>

  </xsl:template>

  <!--<xsl:template match="completiondate">
    -->
  <!-- <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->
  <!--

    <xsl:variable name="year">
      <xsl:value-of select="substring-after(.,'/')" />
    </xsl:variable>
    <xsl:value-of select="$year"/>
  </xsl:template>-->

  <xsl:template match="completiondate">
    <!--
    <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->
    <xsl:variable name="mo">
      <xsl:value-of select="substring-before(. ,'/')" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring-after(.,'/')" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$mo = '1' or $mo = '01'">January</xsl:when>
      <xsl:when test="$mo = '2' or $mo = '02'">February</xsl:when>
      <xsl:when test="$mo = '3' or $mo = '03'">March</xsl:when>
      <xsl:when test="$mo = '4' or $mo = '04'">April</xsl:when>
      <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
      <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
      <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
      <xsl:when test="$mo = '8' or $mo = '08'">August</xsl:when>
      <xsl:when test="$mo = '9' or $mo = '09'">September</xsl:when>
      <xsl:when test="$mo = '10'">October</xsl:when>
      <xsl:when test="$mo = '11'">November</xsl:when>
      <xsl:when test="$mo = '12'">December</xsl:when>
    </xsl:choose>
    <xsl:text> </xsl:text>
    <xsl:value-of select="$year"/>

  </xsl:template>

</xsl:stylesheet>
