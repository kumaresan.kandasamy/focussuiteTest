<!-- 
    ************************************************************
    Copyright(c) 2008, Burning Glass Technologies
    XSL for ResumeTemplate01 for Focus
    ************************************************************
    Version 1.2
    
    Change Log: 
	30/JAN/2008 DIL  Created the XSL
	27/FEB/2008 PIS  Updated the XSL
	11/SEP/2008 PIS  Updated date format display
	11/Dec/2009 SIK  Updated the XSL to emit the content as left justified 
    02/Jul/2010 RRC  Updated the XSL to include references in more sections. 
    17/Aug/2010 RRC  Included condition to display Present in work experience.
    18/Aug/2010 RRC  Included intenships in more sections
    13/Oct/2010 RRC Included automated summary and removed XXXX from date.
    01/Nov/2010 AKV updated the xsl to format download PDF & RTF resume.
    11/Nov/2010 RRC Sorted work experience in reverse chronological order
    09/DEC/2010 AKV <NewLine>*<Space> is replaced with <Space> in Job Descriptions
	21/DEC/2010 RRC Updated objective,skills & reference tag due to changes in xpath of new lens version.
	27/DEC/2010 RRC Justifies bodies of the text. 
	05/JAN/2011 AKV Job sorting by pos id has been removed, now we are getting sorted jobs from WS 
	07/MAR/2011 RIR Modified paths as per modified Focus XML
  31/AUG/2011 RRC do not display employer, if the emploer is "Self" in experience.
  02/SEP/2011 RRC Added veteran discharge in military services & courses, honors, gpa, activities in education. 
  14/SEP/2011 AKV Dynamic font size change.
  16/SEP/2011 AKV Default font values are set for fontsize13, fontsize14 and fontsize16 parameters.
  16/SEP/2011 AKV Experience Date issue resolved if it is not in proper format 'to' or '-' appears. 
  20/SEP/2011 AKV Address2 is added in address section.
  28/SEP/2011 AKV Extra ',' issue in Experience Address is resolved.
  30/SEP/2011 AKV Extra NewLine issue while hiding contact details is resolved.
  18/OCT/2012 SAIN Bug Fix: Configured state to county for UK build
  21/NOV/2012 RRC Added website in contact details
  23/NOV/2012 SAIN Bug Fix: Contact information alignment differs in RTF
  23/NOV/2012 SAIN Bug Fix: Military Service date is displayed in 2 lines in RTF
  02/JAN/2013 SAV Rearranged the sections according to the new resume template.
  02/JAN/2013 SAV Removed Professional Summary and Apprenticeship section.
  02/JAN/2013 SAV Added emphasizeinfo parameter for dynamic section change.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xsl:output method="html" version="1.0" omit-xml-declaration="no" indent="yes" encoding="iso-8859-1"/>
    <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
    <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
    <xsl:param name="fontsize13" select="'13px'"/>
    <xsl:param name="fontsize14" select="'14px'"/>
    <xsl:param name="fontsize16" select="'16px'"/>
    <xsl:param name="emphasizeinfo"/>
    <xsl:template match="/">

        <style type="text/css">
            .font13 { font-family:arial;font-size:<xsl:value-of select="$fontsize13"/>; }
            .font14 { font-family:arial;font-size:<xsl:value-of select="$fontsize14"/>; }
            .font16 { font-family:arial;font-size:<xsl:value-of select="$fontsize16"/>; font-weight:bold; }
        </style>
        <html>
            <body>
                <font class="font14">
                    <table border="0" width="100%">
                        <tr>
                            <td>
                                <!--CONTACT INFORMATION-->
                                <xsl:if test="count(//contact)>0">
                                    <xsl:if test="count(//contact/name)>0">
                                        <font class="font16">
                                            <xsl:value-of select="(//givenname)[1]"/>
                                            <xsl:text>   </xsl:text>
                                            <xsl:value-of select="(//givenname)[2]"/>
                                            <xsl:text>   </xsl:text>
                                            <xsl:value-of select="(//surname)[1]"/>
                                        </font>
                                        <br/>
                                    </xsl:if>
                                    <font class="font13">
                                        <xsl:if test="string-length((//phone)[1])>0">
                                            <xsl:apply-templates select="(//resume/contact/phone)[1]"/>
                                            <br/>
                                        </xsl:if>
                                        <xsl:if test="string-length((//phone)[2])>0">
                                            <xsl:apply-templates select="(//resume/contact/phone)[2]"/>
                                            <br/>
                                        </xsl:if>

                                        <xsl:if test="string-length((//phone)[3])>0">
                                            <xsl:apply-templates select="(//resume/contact/phone)[3]"/>
                                            <br/>
                                        </xsl:if>
                                        <xsl:if test="string-length((//resume/contact/address)[1]/street[1])>0">
                                            <xsl:value-of select="(//resume/contact/address)[1]/street[1]"/>
                                            <br/>
                                        </xsl:if>
                                        <xsl:if test="string-length((//resume/contact/address)[1]/street[2])>0">
                                            <xsl:value-of select="(//resume/contact/address)[1]/street[2]"/>
                                            <br/>
                                        </xsl:if>
                                        <xsl:if test="count((//resume/contact/address)[1]/city[1]) > 0">
                                            <xsl:value-of select="(//resume/contact/address)[1]/city[1]"/>
                                        </xsl:if>
                                        <xsl:if test="count((//resume/contact/address)[1]/town[1]) > 0">
                                            <xsl:value-of select="(//resume/contact/address)[1]/town[1]"/>
                                        </xsl:if>
                                        <xsl:if test="count((//resume/contact/address)[1]/state[1]) > 0">
                                            <xsl:if test="count((//resume/contact/address)[1]/city[1])>0">
                                                <xsl:text>, </xsl:text>
                                            </xsl:if>
                                            <xsl:if test="(//resume/contact/address)[1]/state[1] != 'ZZ'">
                                                <xsl:value-of select="(//resume/contact/address)[1]/state[1]"/>
                                            </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="count((//resume/contact/address)[1]/county[1]) > 0 and count((//resume/contact/address)[1]/state[1]) = 0">
                                            <xsl:if test="count((//resume/contact/address)[1]/town[1])>0">
                                                <xsl:text>, </xsl:text>
                                            </xsl:if>
                                            <xsl:if test="(//resume/contact/address)[1]/state[1] != 'ZZ'">
                                                <xsl:value-of select="(//resume/contact/address)[1]/state[1]"/>
                                            </xsl:if>
                                            <xsl:if test="(//resume/contact/address)[1]/county[1] != 'ZZ'">
                                                <xsl:value-of select="(//resume/contact/address)[1]/county[1]"/>
                                            </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="count((//resume/contact/address)[1]/postalcode[1]) > 0">
                                            <xsl:if test="count((//resume/contact/address)[1]/state[1])>0">
                                                <xsl:text>  </xsl:text>
                                            </xsl:if>
                                            <xsl:if test="count((//resume/contact/address)[1]/county[1])>0">
                                                <xsl:text>  </xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="(//resume/contact/address)[1]/postalcode[1]"/>
                                        </xsl:if>
                                        <xsl:if test="count((//resume/contact/address)[1]/country[1]) > 0">
                                            <xsl:if test="count((//resume/contact/address)[1]/postalcode[1])>0">
                                            </xsl:if>
                                            <xsl:if test="(//resume/contact/address)[1]/country[1] != 'US'">
                                                <xsl:text>, </xsl:text>
                                                <xsl:value-of select="(//resume/contact/address)[1]/country_fullname[1]"/>
                                            </xsl:if>
                                            <br/>
                                        </xsl:if>
                                        <xsl:value-of select="(//resume/contact/email)[1]"/>
                                        <br/>
                                        <xsl:if test="count((//resume/contact/website)[1]) > 0">
                                            <xsl:value-of select="(//resume/contact/website)[1]" />
                                            <br/>
                                        </xsl:if>
                                    </font>

                                    <hr style="width:100%"/>
                                    <table>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </table>
                                </xsl:if>

															<!-- BRANDING (top)-->
															<xsl:if test="count(//special/branding)>0">
																<font class="font14">
																	<b>Branding statement</b>
																</font>

																<font class="font13">
																	<div style="text-align: justify;">
																		<xsl:value-of select="//special/branding"/>
																	</div>
																</font>
																<br/>
															</xsl:if>
															
                                <!-- OBJECTIVE (top)-->
                                <xsl:if test="count(//summary/objective)>0">
                                    <font class="font14">
                                        <b>Objective</b>
                                    </font>

                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:value-of select="//summary/objective"/>
                                        </div>
                                    </font>
                                    <br/>
                                </xsl:if>

                                <!-- SUMMARY (top of resume)-->
                                <xsl:if test="count(//summary/summary)>0">
                                    <xsl:if test="//summary/summary !=''">
                                        <font class="font14">
                                            <b>Summary</b>
                                        </font>
                                        <font class="font13" >
                                            <div style="text-align: justify;">
                                                <xsl:value-of select="//summary/summary"/>
                                            </div>
                                        </font>
                                        <br/>
                                    </xsl:if>
                                </xsl:if>

                                <!--<xsl:if test="//special/automatedsummary/@custom='1'">				
					<xsl:if test="count(//special/automatedsummary)>0">
					  <font class="font14">
						<b>Summary</b>
					  </font>
					  <br/>
					  <font class="font13">
						<xsl:value-of select="//special/automatedsummary"/>
					  </font>
					  <br/>
					  <br/>
					</xsl:if>
				</xsl:if>
				<xsl:if test="//special/automatedsummary/@custom='-1'">
					
					<xsl:if test="count(//special/snapshot)>0">
					  <font class="font14">
						<b>Summary</b>
					  </font>
					  <br/>
					  <font class="font13">
						<xsl:value-of select="//special/snapshot"/>
					  </font>
					  <br/>
					  <br/>
					</xsl:if>
				</xsl:if>-->

                                <!--Middle Sections-->
                                <xsl:choose>
                                    <xsl:when test='$emphasizeinfo="skills"'>
                                        <xsl:call-template name='skillssection'/>
                                        <xsl:call-template name='educationsection'/>
                                        <xsl:call-template name='experiencesection'/>
                                    </xsl:when>

                                    <xsl:when test ='$emphasizeinfo="education"'>
                                        <xsl:call-template name='educationsection'/>
                                        <xsl:call-template name='skillssection'/>
                                        <xsl:call-template name='experiencesection'/>
                                    </xsl:when>

                                    <xsl:otherwise>
                                        <xsl:call-template name='experiencesection'/>
                                        <xsl:call-template name='skillssection'/>
                                        <xsl:call-template name='educationsection'/>
                                    </xsl:otherwise>
                                </xsl:choose>

                                <!--Bottom Sections-->
                                <!-- Publications (bottom)-->
                                <xsl:if test="count(//professional/publications)>0">
                                    <font class="font14">
                                        <b>Publications</b>
                                    </font>

                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:for-each select="//professional/publications">
                                                <xsl:value-of select="."/>
                                                <br/>
                                            </xsl:for-each>
                                        </div>
                                    </font>
                                    <br/>
                                </xsl:if>

                                <!-- Affiliations (bottom)-->
                                <xsl:if test="count(//professional/affiliations)>0">
                                    <font class="font14">
                                        <b>Affiliations</b>
                                    </font>

                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:for-each select="//professional/affiliations">
                                                <xsl:value-of select="."/>
                                                <br/>
                                            </xsl:for-each>
                                        </div>
                                    </font>
                                    <br/>
                                </xsl:if>

                                <!-- Honors (bottom)-->
                                <xsl:if test="count(//statements/honors)>0">
                                    <font class="font14">
                                        <b>Honors</b>
                                    </font>

                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:for-each select="//statements/honors">
                                                <xsl:value-of select="."/>
                                                <br/>
                                            </xsl:for-each>
                                        </div>
                                    </font>
                                    <br/>
                                </xsl:if>

                                <!-- Volunteer Activities (bottom)-->
                                <xsl:if test="count(//special/volunteeractivities)>0">
                                    <font class="font14">
                                        <b>Volunteer Activities</b>
                                    </font>

                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:value-of select="//special/volunteeractivities"/>
                                        </div>
                                    </font>
                                    <br/>
                                </xsl:if>

                                <!-- Interests (bottom)-->
                                <xsl:if test="count(//special/interests)>0">
                                    <font class="font14">
                                        <b>Interests</b>
                                    </font>

                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:value-of select="//special/interests"/>
                                        </div>
                                    </font>
                                    <br/>
                                </xsl:if>

                                <!-- Personal (bottom)-->
                                <xsl:if test="count(//special/personal)>0">
                                    <font class="font14">
                                        <b>Personal</b>
                                    </font>

                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:value-of select="//special/personal"/>
                                        </div>
                                    </font>
                                    <br/>
                                </xsl:if>

																<!-- Thesis/Major Projects (bottom of resume)-->
																<xsl:if test="count(//thesismajorprojects)>0">
																	<font class="font14">
																		<b>Thesis/Major Projects</b>
																	</font>

																	<font class="font13">
																		<div style="text-align: justify;">
																			<xsl:for-each select="//thesismajorprojects">
																				<xsl:value-of select="."/>
																				<br/>
																			</xsl:for-each>
																		</div>
																	</font>
																	<br/>
																</xsl:if>

                                <!-- References (bottom of resume)-->
                                <xsl:if test="count(//references)>0">
                                    <font class="font14">
                                        <b>References</b>
                                    </font>

                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:for-each select="//references">
                                                <xsl:value-of select="."/>
                                                <br/>
                                            </xsl:for-each>
                                        </div>
                                    </font>
                                    <br/>
                                </xsl:if>

                            </td>
                        </tr>
                    </table>
                </font>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="skillssection">
        <!-- Skills (bottom)-->
        <xsl:if test="string-length(//skills/skills)>0">
            <font class="font14">
                <b>Skills</b>
            </font>

            <font class="font13">
                <div style="text-align: justify;">
                    <xsl:for-each select="//skills/skills">
                        <xsl:value-of select="."/>
                        <br/>
                    </xsl:for-each>
                </div>
            </font>
            <br/>
        </xsl:if>

        <!-- Technical Skills (top)-->
        <xsl:if test="count(//special/technicalskillstop)>0">
            <font class="font14">
                <b>Technical Skills</b>
            </font>

            <font class="font13">
                <div style="text-align: justify;">
                    <xsl:value-of select="//special/technicalskillstop"/>
                </div>
            </font>
            <br/>
        </xsl:if>

        <!-- Technical Skills (bottom)-->
        <xsl:if test="count(//special/technicalskills)>0">
            <font class="font14">
                <b>Technical Skills</b>
            </font>

            <font class="font13">
                <div style="text-align: justify;">
                    <xsl:value-of select="//special/technicalskills"/>
                </div>
            </font>
            <br/>
        </xsl:if>

        <!-- Technical Skills (bottom)-->
        <xsl:if test="count(//special/technicalskillsbottom)>0">
            <font class="font14">
                <b>Technical Skills</b>
            </font>

            <font class="font13">
                <div style="text-align: justify;">
                    <xsl:value-of select="//special/technicalskillsbottom"/>
                </div>
            </font>
            <br/>
        </xsl:if>

        <!--<xsl:if test="count(//skills)>0">
                  <br/>
                  <font class="font14">
                    <b>Skills</b>
                  </font>
                  <br/>
                  <br/>
                </xsl:if>
                <xsl:for-each select="//skillrollup">
                  <table border="0" width="100%">
                    <tr>
                      <td>
                        <xsl:if test="count(//canonskill) = 0 ">
                          <br/>
                        </xsl:if>
                        <font class="font13">
                          <xsl:for-each select="canonskill">
                            <xsl:for-each select="variant">
                              <xsl:apply-templates/>,
                            </xsl:for-each>
                          </xsl:for-each>
                        </font>
                      </td>
                    </tr>
                  </table>
                </xsl:for-each>
                <br/>-->

        <!-- Languages (bottom)-->
        <!--<xsl:if test="count(//skills/languages)>0"> -->
        <xsl:if test="string-length(//skills/languages/languages_profiency)>0">
            <font class="font14">
                <b>Languages</b>
            </font>

            <font class="font13">
                <div style="text-align: justify;">
                    <xsl:for-each select="//skills/languages/languages_profiency">
                        <xsl:value-of select="."/>
                        <br/>
                    </xsl:for-each>
                </div>
            </font>
            <br/>
        </xsl:if>

        <!-- Certifications (bottom)-->
        <!--<xsl:if test="count(//certification)>0"> -->
        <xsl:if test="string-length(//certification)>0">
            <font class="font14">
                <b>Certifications and Professional Licenses</b>
            </font>

            <font class="font13">
                <div style="text-align: justify;">
                    <xsl:for-each select="//certification">
                        <xsl:sort select="@completion_juliandate"/>
                        Certificate:
                        <xsl:value-of select="certificate"/>,
                        <xsl:value-of select="organization_name"/>,
                        <xsl:if test="count(//certification/completion_date)>0">
                            <xsl:apply-templates select="completion_date"/>;
                        </xsl:if>

                        <xsl:if test="count(//address/state_fullname)>0">
                            <xsl:value-of select="address/state_fullname"/>
                        </xsl:if>

                        <xsl:if test="count(//address/state_fullname)=0">
                            <xsl:call-template name="state">
                                <xsl:with-param name="code" select="(address/state)"/>
                            </xsl:call-template>
                        </xsl:if>


                        <!--<xsl:value-of select="address/state"/>-->
                        <xsl:if test="address/country != 'US'">
                            ,  <xsl:value-of select="address/country_fullname"/>
                        </xsl:if>

                        <br/>
                    </xsl:for-each>
                </div>
            </font>
            <br/>
        </xsl:if>


			<!-- National Career Readiness Certificate Credentials (bottom)-->
			<xsl:if test="(//ncrc_confirmation) = 'true' and (//ncrc_displayed) = 'true'">
				<font class="font14">
					<b>National Career Readiness Certificate&#8482;</b>
				</font>

				<font class="font13">
					<div style="text-align: justify;">
						<xsl:value-of select="//ncrc_level_name"/> level attained, ACT,
						<xsl:value-of select="//ncrc_issue_date"/>; 
						<xsl:value-of select="//ncrc_state_name"/>
					</div>
				</font>
				<br/>
			</xsl:if>

        <!-- Licenses (bottom of resume)-->
        <!-- <xsl:if test="count(//personal/license)>0"> -->
        <xsl:if test="string-length(//personal/license)>0">
            <xsl:if test="(//personal/license/driver_class) &gt; 0">

                <font class="font13">
                    <div style="text-align: justify;">

                        <xsl:if test="string-length(//personal/license/driver_class)>0 and string-length(//personal/license/driver_class_text)>0">
                            <font class="font14">
                                <b>Licenses</b>
                            </font>
                            <br/>
                            <xsl:if test="string-length(//personal/license/driver_state) > 0">
                                <font class="font13">
                                    <xsl:call-template name="state">
                                        <xsl:with-param name="code" select="(//personal/license/driver_state)"/>
                                    </xsl:call-template>
                                </font>
                            </xsl:if>
                            <font class="font13">
                                <xsl:value-of select="//personal/license/driver_class_text"/>
                            </font>
                        </xsl:if>


                        <xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 1">
                            <font class="font14">
                                <b>Licenses</b>
                            </font>
                            <br/>
                            <xsl:if test="string-length(//personal/license/driver_state) > 0">
                                <font class="font13">
                                    <xsl:call-template name="state">
                                        <xsl:with-param name="code" select="(//personal/license/driver_state)"/>
                                    </xsl:call-template>
                                </font>
                            </xsl:if>
                            <font class="font13">
                                , Class A/CDL:
                            </font>
                        </xsl:if>

                        <xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 2">
                            <font class="font14">
                                <b>Licenses</b>
                            </font>
                            <br/>
                            <xsl:if test="string-length(//personal/license/driver_state) > 0">
                                <font class="font13">
                                    <xsl:call-template name="state">
                                        <xsl:with-param name="code" select="(//personal/license/driver_state)"/>
                                    </xsl:call-template>
                                </font>
                            </xsl:if>
                            <font class="font13">
                                , Class B/CDL:
                            </font>

                        </xsl:if>

                        <xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 3">
                            <font class="font14">
                                <b>Licenses</b>
                            </font>
                            <br/>
                            <xsl:if test="string-length(//personal/license/driver_state) > 0">
                                <font class="font13">
                                    <xsl:call-template name="state">
                                        <xsl:with-param name="code" select="(//personal/license/driver_state)"/>
                                    </xsl:call-template>
                                </font>
                            </xsl:if>
                            <font class="font13">
                                , Class C/CDL:
                            </font>
                        </xsl:if>

                        <xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 4">
                            <xsl:if test="(//personal/license/drv_pass_flag) = -1 or (//personal/license/drv_hazard_flag) = -1 or (//personal/license/drv_tank_flag) = -1 or (//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 ">
                                <font class="font14">
                                    <b>Licenses</b>
                                </font>
                                <br/>
                                <xsl:if test="string-length(//personal/license/driver_state) > 0">
                                    <font class="font13">
                                        <xsl:call-template name="state">
                                            <xsl:with-param name="code" select="(//personal/license/driver_state)"/>
                                        </xsl:call-template>
                                    </font>
                                </xsl:if>
                                <font class="font13">
                                    , Class D/Regular:
                                </font>
                            </xsl:if>
                        </xsl:if>

                        <xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 5">
                            <xsl:if test="(//personal/license/drv_pass_flag) = -1 or (//personal/license/drv_hazard_flag) = -1 or (//personal/license/drv_tank_flag) = -1 or (//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 ">
                                <font class="font14">
                                    <b>Licenses</b>
                                </font>
                                <br/>
                                <xsl:if test="string-length(//personal/license/driver_state) > 0">
                                    <font class="font13">
                                        <xsl:call-template name="state">
                                            <xsl:with-param name="code" select="(//personal/license/driver_state)"/>
                                        </xsl:call-template>
                                    </font>
                                </xsl:if>
                                <font class="font13">
                                    , Motorcycle:
                                </font>
                            </xsl:if>
                        </xsl:if>

											<xsl:if test="(//personal/license/drv_pass_flag) = -1">
												<font class="font13">Pass Transport</font>
												<xsl:if test="(//personal/license/drv_hazard_flag) = -1 or (//personal/license/drv_tank_flag) = -1 or (//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
											</xsl:if>
											<xsl:if test="(//personal/license/drv_hazard_flag) = -1">
												<font class="font13">Hazardous Materials</font>
												<xsl:if test="(//personal/license/drv_tank_flag) = -1 or (//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
											</xsl:if>
											<xsl:if test="(//personal/license/drv_tank_flag) = -1">
												<font class="font13">Tank Vehicle</font>
												<xsl:if test="(//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
											</xsl:if>
											<xsl:if test="(//personal/license/drv_cycle_flag) = -1">
												<font class="font13">Motorcycle</font>
												<xsl:if test="(//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
											</xsl:if>
											<xsl:if test="(//personal/license/drv_bus_flag) = -1">
												<font class="font13">School Bus</font>
												<xsl:if test="(//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
											</xsl:if>
											<xsl:if test="(//personal/license/drv_double_flag) = -1">
												<font class="font13">Doubles/Triples</font>
												<xsl:if test="(//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
											</xsl:if>
											<xsl:if test="(//personal/license/drv_tankhazard_flag) = -1">
												<font class="font13">Tank Hazard</font>
												<xsl:if test="(//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
											</xsl:if>
											<xsl:if test="(//personal/license/drv_airbrake_flag) = -1">
												<font class="font13">Air Brakes</font>
												<xsl:if test="(//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
											</xsl:if>
											<xsl:if test="(//personal/license/drv_limo_flag) = -1">
												<font class="font13">Limo/Chauffeur</font>
											</xsl:if>
                    </div>
                </font>
                <br/>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template name="educationsection">
        <!--  <xsl:if test="count(//school) > 0"> -->
        <xsl:if test="string-length(//school)>0">

            <font class="font14">
                <b>Education</b>
            </font>
        </xsl:if>
        <xsl:for-each select="//education">
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
                <tbody>
                    <xsl:for-each select="school">
                        <tr>
                            <td width="" valign="top" cellpadding="0" cellspacing="0">
                                <font class="font13">
                                    <xsl:apply-templates select="./completiondate[1]"/>
                                    <xsl:for-each select="institution[1]">
                                        <xsl:text>    </xsl:text>
                                        <xsl:value-of select="."/>
                                    </xsl:for-each>
                                    <xsl:if test="count(address/city[1]) > 0">
                                        <xsl:text>, </xsl:text>
                                        <xsl:value-of select="address/city[1]"/>
                                    </xsl:if>
                                    <xsl:if test="count((address)[1]/city[1]) = 0 and count((address)[1]/town[1]) > 0">
                                        <xsl:text>, </xsl:text>
                                        <xsl:value-of select="(address)[1]/town[1]"/>
                                    </xsl:if>
                                    <xsl:if test="count(address/state[1]) > 0 and address/state[1] != 'ZZ'">
                                        <xsl:text>, </xsl:text>
                                        <xsl:if test="address/state[1] != 'ZZ'">
                                            <xsl:value-of select="address/state[1]"/>
                                        </xsl:if>
                                    </xsl:if>
                                    <xsl:if test="count((address)[1]/state[1]) = 0 and count((address)[1]/county[1]) > 0">
                                        <xsl:text>, </xsl:text>
                                        <xsl:value-of select="(address)[1]/county[1]"/>
                                    </xsl:if>
                                    <xsl:if test="(count(./address) > 0 and count(./address/state[1]) = 0 and count(./address/county[1]) = 0  ) and ( count(./address/city[1]) = 0 and count(./address/town[1]) = 0 ) ">
                                        <xsl:text>, </xsl:text>
                                        <xsl:value-of select="./address[1]"/>
                                    </xsl:if>

                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td width="" valign="top">
                                <font class="font13">
                                    <b>
                                        <xsl:for-each select="degree[1]">
                                            <xsl:if test="translate(.,$lowercase,$uppercase)!='GENERAL' and translate(.,$lowercase,$uppercase)!='N/A' and translate(.,$lowercase,$uppercase)!='NA'">
                                                <xsl:apply-templates/>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </b>
                                    <xsl:if test="count(major[1]) > 0  and string-length(major[1])>0">
                                        <xsl:for-each select="major[1]">
                                            <xsl:if test="translate(.,$lowercase,$uppercase)!='GENERAL' and translate(.,$lowercase,$uppercase)!='N/A' and translate(major,$lowercase,$uppercase)!='NA'">
                                                <xsl:if test="count(../degree[1]) > 0 ">
                                                    <xsl:text>:	</xsl:text>
                                                </xsl:if>
                                                <xsl:apply-templates/>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </xsl:if>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td width="" valign="top">
                                <font class="font13">
                                    <xsl:if test="string-length(courses[1]) > 0">
                                        <b>Course Work Completed : </b>
                                        <xsl:value-of select="courses[1]"/>
                                    </xsl:if>
                                    <xsl:if test="string-length(courses[1]) > 0 and string-length(honors[1]) > 0">
                                        <xsl:text>, </xsl:text>
                                    </xsl:if>
                                    <xsl:if test="string-length(honors[1]) > 0">
                                        <b>Honors : </b>
                                        <xsl:value-of select="honors[1]"/>
                                    </xsl:if>
                                    <xsl:if test="(string-length(courses[1]) > 0 or string-length(honors[1]) > 0) and string-length(gpa[1]) > 0 ">
                                        <xsl:text>, </xsl:text>
                                    </xsl:if>
                                    <xsl:if test="string-length(gpa[1]) > 0">
                                        <b>GPA :  </b>
                                        <xsl:value-of select="gpa[1]"/>
                                    </xsl:if>
                                    <xsl:if test="(string-length(courses[1]) > 0 or string-length(honors[1]) > 0 or string-length(gpa[1]) > 0 ) and string-length(activities[1]) > 0  ">
                                        <xsl:text>, </xsl:text>
                                    </xsl:if>
                                    <xsl:if test="string-length(activities[1]) > 0">
                                        <b>Activities : </b>
                                        <xsl:value-of select="activities[1]"/>
                                    </xsl:if>

                                </font >
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <br/>
                            </td>
                        </tr>
                    </xsl:for-each>

                </tbody>
            </table>
        </xsl:for-each>

        <!-- Professional Development (bottom)-->
        <xsl:if test="count(//professional/description)>0">
            <font class="font14">
                <b>Professional Development</b>
            </font>

            <font class="font13">
                <div style="text-align: justify;">
                    <xsl:for-each select="//professional/description">
                        <xsl:value-of select="."/>
                        <br/>
                    </xsl:for-each>
                </div>
            </font>
            <br/>
        </xsl:if>

    </xsl:template>
    
    <xsl:template name="experiencesection">
        <xsl:if test="count(//job) > 0">
            <font class="font14">
                <b>Experience</b>
            </font>
        </xsl:if>
        <xsl:for-each select="//experience">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                    <xsl:for-each select="job">
                        <xsl:if test="@pos !=''">
                            <tr>
                                <td valign="top" align="left">
                                    <font class="font13">
                                        <xsl:if test="employer != 'Self' and string-length(employer) > 0 ">
                                            <xsl:for-each select="employer">
                                                <xsl:apply-templates/>
                                            </xsl:for-each>
                                            <xsl:if test="string-length(address) > 0 and (count(address/city[1]) > 0 or count((address)[1]/town[1]) > 0)">
                                                <xsl:text>, </xsl:text>
                                            </xsl:if>
                                        </xsl:if>

                                        <xsl:if test="string-length(address) > 0">

                                            <xsl:if test="count(address/city[1]) > 0">
                                                <xsl:value-of select="address/city[1]"/>
                                            </xsl:if>
                                            <xsl:if test="count((address)[1]/city[1]) = 0 and count((address)[1]/town[1]) > 0">
                                                <xsl:value-of select="(address)[1]/town[1]"/>
                                            </xsl:if>
                                            <xsl:if test="count(address/state[1]) > 0">
                                                    <xsl:text>, </xsl:text>                                                
                                                    <xsl:if test="address/state[1] != 'ZZ'">
                                                    <xsl:value-of select="address/state[1]"/>
                                                </xsl:if>
                                            </xsl:if>
                                            <xsl:if test="count((address)[1]/state[1]) = 0 and count((address)[1]/county[1]) > 0">
                                                <xsl:text>, </xsl:text>
                                                <xsl:value-of select="(address)[1]/county[1]"/>
                                            </xsl:if>
                                            <xsl:if test="(count(./address) > 0 and count(./address/state[1]) = 0 and count(./address/county[1]) = 0  ) and ( count(./address/city[1]) = 0 and count(./address/town[1]) = 0 ) ">
                                                <xsl:text>, </xsl:text>
                                                <xsl:value-of select="./address[1]"/>
                                            </xsl:if>
                                        </xsl:if>
                                    </font>
                                </td>
                                <td align="right" width="45%">
                                    <font class="font13">
                                        <xsl:if test="string-length(daterange/start) > 0">
                                            <xsl:if test="string-length(daterange/end) > 0 and (daterange/start = daterange/end)">
                                                <xsl:apply-templates select="daterange/start"/>
                                            </xsl:if>
                                            <xsl:if test="string-length(daterange/end) > 0 and (daterange/start != daterange/end)">
                                                <xsl:apply-templates select="daterange/start"/>
                                                <xsl:variable name="temp1End">
                                                    <xsl:value-of select="substring-before(daterange/end ,'/')" />
                                                </xsl:variable>
                                                <xsl:if test="string-length(daterange/end) > 0 and string-length($temp1End) > 0">
                                                    <xsl:text> to </xsl:text>
                                                </xsl:if>
                                                <!--The below code is added by RRC on 17.08.2010 for displaying present-->
                                                <xsl:choose>
                                                    <xsl:when test="daterange/end/@currentjob = '1'">
                                                        <xsl:text>Present</xsl:text>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:apply-templates select="daterange/end"/>
                                                    </xsl:otherwise>

                                                </xsl:choose>
                                                <!--The below code is added by RRC for displaying present-->

                                                <!--<xsl:apply-templates select="daterange/end"/>-->

                                            </xsl:if>
                                            <xsl:if test="string-length(daterange/end) = '0'">
                                                <xsl:apply-templates select="daterange/start"/>
                                                <!-- <xsl:text> to xxxx</xsl:text>-->
                                            </xsl:if>
                                        </xsl:if>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b>
                                        <font class="font13">
                                            <xsl:for-each select="title">
                                                <xsl:apply-templates/>
                                            </xsl:for-each>
                                        </font>
                                        <br/>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="2" text-align="justify">
                                    <font class="font13">
                                        <div style="text-align: justify;">
                                            <xsl:if test="count(description)>0">
                                               
                                                <xsl:for-each select="description">
                                                    <xsl:variable name="updatedDescription">
                                                        <xsl:call-template name="string-replace-FC_CRLF_BULLETS">
                                                            <xsl:with-param name="text" select="." />
                                                            <xsl:with-param name="replace" select="'FC_CRLF* '" />
                                                            <xsl:with-param name="by" select="' '" />
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:call-template name="string-replace-BULLETS">
                                                        <xsl:with-param name="text" select="$updatedDescription" />
                                                        <xsl:with-param name="replace" select="'* '" />
                                                        <xsl:with-param name="by" select="''" />
                                                    </xsl:call-template>
                                                    <!--<br/>-->
                                                </xsl:for-each>
                                            </xsl:if >
                                        </div>
                                    </font>
                                    <!--<br/>-->
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br/>
                                </td>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:for-each select="job">
                        <xsl:if test="string-length(@pos)=0">
                            <tr>
                                <td valign="top" align="left">
                                    <font class="font13">
                                        <xsl:if test="employer != 'Self' and string-length(employer) > 0">
                                            <xsl:for-each select="employer">
                                                <xsl:apply-templates/>
                                            </xsl:for-each>
                                            <xsl:if test="string-length(address) > 0 and (count(address/city[1]) > 0 or count((address)[1]/town[1]) > 0)">
                                            <xsl:text>, </xsl:text>    
                                            </xsl:if>
                                        </xsl:if>
                                        <xsl:if test="string-length(address) > 0">
                                            <xsl:if test="count(address/city[1]) > 0">
                                                <!--<xsl:text>, </xsl:text>-->
                                                <xsl:value-of select="address/city[1]"/>
                                            </xsl:if>
                                            <xsl:if test="count((address)[1]/city[1]) = 0 and count((address)[1]/town[1]) > 0">
                                                <xsl:value-of select="(address)[1]/town[1]"/>
                                            </xsl:if>
                                            <xsl:if test="count(address/state[1]) > 0">
                                                <xsl:text>, </xsl:text>
                                                <xsl:if test="address/state[1] != 'ZZ'">
                                                    <xsl:value-of select="address/state[1]"/>
                                                </xsl:if>
                                            </xsl:if>
                                            <xsl:if test="count((address)[1]/state[1]) = 0 and count((address)[1]/county[1]) > 0">
                                               <xsl:text>, </xsl:text>
                                                <xsl:value-of select="(address)[1]/county[1]"/>
                                            </xsl:if>
                                            <xsl:if test="(count(./address) > 0 and count(./address/state[1]) = 0 and count(./address/county[1]) = 0  ) and ( count(./address/city[1]) = 0 and count(./address/town[1]) = 0 ) ">
                                                <xsl:text>, </xsl:text>
                                                <xsl:value-of select="./address[1]"/>
                                            </xsl:if>
                                        </xsl:if>
                                    </font>
                                </td>
                                <td align="right" width="45%">
                                    <font class="font13">
                                        <xsl:if test="string-length(daterange/start) > 0">
                                            <xsl:if test="string-length(daterange/end) > 0 and (daterange/start = daterange/end)">
                                                <xsl:apply-templates select="daterange/start"/>
                                            </xsl:if>
                                            <xsl:if test="string-length(daterange/end) > 0 and (daterange/start != daterange/end)">
                                                <xsl:apply-templates select="daterange/start"/>
                                                <xsl:variable name="temp2End">
                                                    <xsl:value-of select="substring-before(daterange/end ,'/')" />
                                                </xsl:variable>
                                                <xsl:if test="string-length(daterange/end) > 0 and string-length($temp2End) > 0">
                                                    <xsl:text> to </xsl:text>
                                                </xsl:if>
                                                <!--The below code is added by RRC on 17.08.2010 for displaying present-->
                                                <xsl:choose>
                                                    <xsl:when test="daterange/end/@currentjob = '1'">
                                                        <xsl:text>Present</xsl:text>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:apply-templates select="daterange/end"/>
                                                    </xsl:otherwise>

                                                </xsl:choose>
                                                <!--The below code is added by RRC for displaying present-->

                                                <!--<xsl:apply-templates select="daterange/end"/>-->

                                            </xsl:if>
                                            <xsl:if test="string-length(daterange/end) = '0'">
                                                <xsl:apply-templates select="daterange/start"/>
                                                <!-- <xsl:text> to xxxx</xsl:text>-->
                                            </xsl:if>
                                        </xsl:if>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b>
                                        <font class="font13">
                                            <xsl:for-each select="title">
                                                <xsl:apply-templates/>
                                            </xsl:for-each>
                                        </font>
                                        <br/>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <xsl:if test="count(description)>0">
                                    <td valign="top" colspan="2">
                                        <font class="font13">
                                            <div style="text-align: justify;">
                                                    <xsl:for-each select="description">
                                                        <xsl:variable name="updatedDescription">
                                                            <xsl:call-template name="string-replace-FC_CRLF_BULLETS">
                                                                <xsl:with-param name="text" select="." />
                                                                <xsl:with-param name="replace" select="'FC_CRLF* '" />
                                                                <xsl:with-param name="by" select="' '" />
                                                            </xsl:call-template>
                                                        </xsl:variable>
                                                        <xsl:call-template name="string-replace-BULLETS">
                                                            <xsl:with-param name="text" select="$updatedDescription" />
                                                            <xsl:with-param name="replace" select="'* '" />
                                                            <xsl:with-param name="by" select="''" />
                                                        </xsl:call-template>
                                                        <!--<br/>-->
                                                    </xsl:for-each>

                                                </div>
                                        </font>
                                        <!--<br/>-->
                                    </td>
                                </xsl:if>
                            </tr>
                            <tr>
                                <td>
                                    <br/>
                                </td>
                            </tr>
                        </xsl:if>

                    </xsl:for-each>

                </tbody>
            </table>
        </xsl:for-each>

        <!-- Veteran experience -->
        <xsl:if test="((string-length(//veteran/rank) > 0 and ((//veteran/rank) != 'Select Rank' and (//veteran/rank) != '0')) or (string-length(//veteran/moc) > 0) or (string-length(//veteran/vet_start_date) > 0) or (string-length(//veteran/branch_of_service) > 0 and (//veteran/branch_of_service) != 'Select Branch of Service') or (string-length(//veteran/unit_affiliation) > 0) or (string-length(//veteran/vet_discharge) > 0 and (//veteran/vet_discharge) = 'Honorable')) and (//veteran/vet_era) != 3">
            <font class="font14">
                <b>Military Service</b>
                <br/>

            </font>
            <xsl:if test="count(//veteran/rank) > 0">

                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                        <tr>
                            <td valign="top" align="left" width="75%">
                                <font class="font13">

                                    <xsl:if test="string-length(//veteran/rank) > 0 and ((//veteran/rank) != 'Select Rank' and (//veteran/rank) != '0')">
                                        <xsl:value-of select="//veteran/rank"/>
                                    </xsl:if>
                                    <xsl:if test="string-length(//veteran/moc) > 0">
                                        <xsl:if test="string-length(//veteran/rank) > 0 and ((//veteran/rank) != 'Select Rank' and (//veteran/rank) != '0')">
                                            <xsl:text>, </xsl:text>
                                        </xsl:if>
                                        <xsl:if test="string-length(//veteran/mostext) > 0">
                                            <xsl:value-of select="//veteran/mostext"/>
                                        </xsl:if>
                                        <xsl:if test="string-length(//veteran/mostext) = 0">
                                            <xsl:value-of select="//veteran/moc"/>
                                        </xsl:if>
                                    </xsl:if>
                                </font>
                            </td>
                            <td align="right" width="45%">
                                <font class="font13">
                                    <xsl:if test="string-length(//veteran/vet_start_date) > 0">
                                        <xsl:if test="string-length(//veteran/vet_end_date) > 0 and (//veteran/vet_start_date = //veteran/vet_end_date)">
                                            <xsl:apply-templates select="vet_start_date"/>
                                        </xsl:if>
                                        <xsl:if test="string-length(//veteran/vet_end_date) > 0 and (//veteran/vet_start_date != //veteran/vet_end_date)">
                                            <xsl:apply-templates select="//veteran/vet_start_date"/>
                                            <xsl:text> to </xsl:text>
                                            <xsl:apply-templates select="//veteran/vet_end_date"/>

                                        </xsl:if>
                                        <xsl:if test="string-length(//veteran/vet_end_date) = '0'">
                                            <xsl:apply-templates select="//veteran/vet_start_date"/>

                                        </xsl:if>
                                    </xsl:if>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>
                                    <font class="font13">
																			<xsl:value-of select="//veteran/branch_of_service"/>
                                    </font>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">

                                <font class="font13">
                                    <xsl:if test="string-length(//veteran/unit_affiliation) > 0">
                                        <xsl:value-of select="(//veteran/unit_affiliation)"/>
                                    </xsl:if>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">

                                <font class="font13">
                                    <xsl:if test="string-length(//veteran/vet_discharge) > 0 and (//veteran/vet_discharge) = 'Honorable'">
                                        <b>Veteran Discharge :  </b>
                                        <xsl:value-of select="(//veteran/vet_discharge)"/>
                                    </xsl:if>
                                </font>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </xsl:if>
            <br/>
        </xsl:if>

        <!-- Internships (bottom of resume)-->
        <xsl:if test="count(//special/internships)>0">
            <font class="font14">
                <b>Internships</b>
            </font>

            <font class="font13">
                <div style="text-align: justify;">
                    <xsl:for-each select="//special/internships">
                        <xsl:value-of select="."/>
                        <br/>
                    </xsl:for-each>
                </div>
            </font>
            <br/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="phone">

        <xsl:if test="(//resume/contact/address/country)[1] = 'US'">
            <xsl:choose>
                <xsl:when test="@type='work'">
                    <xsl:value-of select="format-number(.,'(000) 000-0000')"/>
                    <xsl:text> (Work)</xsl:text>
                </xsl:when>
                <xsl:when test="@type='cell'">
                    <xsl:value-of select="format-number(.,'(000) 000-0000')"/>
                    <xsl:text> (Cell)</xsl:text>
                </xsl:when>
                <xsl:when test="@type='fax'">
                    <xsl:value-of select="format-number(.,'(000) 000-0000')"/>
                    <xsl:text> (Fax)</xsl:text>
                </xsl:when>
                <xsl:when test="@type='pager'">
                    <xsl:value-of select="format-number(.,'(000) 000-0000')"/>
                    <xsl:text> (Pager)</xsl:text>
                </xsl:when>
                <xsl:when test="@type='home'">
                    <xsl:value-of select="format-number(.,'(000) 000-0000')"/>
                    <xsl:text> (Home)</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                    <xsl:text> (Non US)</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>

        <xsl:if test="(//resume/contact/address/country)[1] != 'US'">
            <xsl:choose>
                <xsl:when test="@type='work'">
                    <xsl:value-of select="."/>
                    <xsl:text> (Work)</xsl:text>
                </xsl:when>
                <xsl:when test="@type='cell'">
                    <xsl:value-of select="."/>
                    <xsl:text> (Cell)</xsl:text>
                </xsl:when>
                <xsl:when test="@type='fax'">
                    <xsl:value-of select="."/>
                    <xsl:text> (Fax)</xsl:text>
                </xsl:when>
                <xsl:when test="@type='pager'">
                    <xsl:value-of select="."/>
                    <xsl:text> (Pager)</xsl:text>
                </xsl:when>
                <xsl:when test="@type='home'">
                    <xsl:value-of select="."/>
                    <xsl:text> (Home)</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template name="string-replace-FC_CRLF_BULLETS">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-FC_CRLF_BULLETS">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="string-replace-BULLETS">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="starts-with($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-BULLETS">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="start">
        <!--
    <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->
        <xsl:variable name="mo">
            <xsl:value-of select="substring-before(. ,'/')" />
        </xsl:variable>
        <xsl:variable name="year">
            <xsl:value-of select="substring-after(.,'/')" />
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$mo = '1' or $mo = '01'">Jan.</xsl:when>
            <xsl:when test="$mo = '2' or $mo = '02'">Feb.</xsl:when>
            <xsl:when test="$mo = '3' or $mo = '03'">Mar.</xsl:when>
            <xsl:when test="$mo = '4' or $mo = '04'">Apr.</xsl:when>
            <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
            <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
            <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
            <xsl:when test="$mo = '8' or $mo = '08'">Aug.</xsl:when>
            <xsl:when test="$mo = '9' or $mo = '09'">Sept.</xsl:when>
            <xsl:when test="$mo = '10'">Oct.</xsl:when>
            <xsl:when test="$mo = '11'">Nov.</xsl:when>
            <xsl:when test="$mo = '12'">Dec.</xsl:when>
        </xsl:choose>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$year"/>

    </xsl:template>

    <xsl:template match="end">
        <!--
    <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
   -->
        <xsl:variable name="mo">
            <xsl:value-of select="substring-before(. ,'/')" />
        </xsl:variable>
        <xsl:variable name="year">
            <xsl:value-of select="substring-after(.,'/')" />
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$mo = '1' or $mo = '01'">Jan.</xsl:when>
            <xsl:when test="$mo = '2' or $mo = '02'">Feb.</xsl:when>
            <xsl:when test="$mo = '3' or $mo = '03'">Mar.</xsl:when>
            <xsl:when test="$mo = '4' or $mo = '04'">Apr.</xsl:when>
            <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
            <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
            <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
            <xsl:when test="$mo = '8' or $mo = '08'">Aug.</xsl:when>
            <xsl:when test="$mo = '9' or $mo = '09'">Sept.</xsl:when>
            <xsl:when test="$mo = '10'">Oct.</xsl:when>
            <xsl:when test="$mo = '11'">Nov.</xsl:when>
            <xsl:when test="$mo = '12'">Dec.</xsl:when>
        </xsl:choose>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$year"/>

    </xsl:template>

    <xsl:template match="vet_start_date">

        <!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
-->
        <xsl:variable name="mo">
            <xsl:value-of select="substring-before(. ,'/')" />
        </xsl:variable>
        <xsl:variable name="day-temp">
            <xsl:value-of select="substring-after(.,'/')" />
        </xsl:variable>
        <xsl:variable name="year">
            <xsl:value-of select="substring-after($day-temp,'/')" />
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$mo = '1' or $mo = '01'">Jan.</xsl:when>
            <xsl:when test="$mo = '2' or $mo = '02'">Feb.</xsl:when>
            <xsl:when test="$mo = '3' or $mo = '03'">Mar.</xsl:when>
            <xsl:when test="$mo = '4' or $mo = '04'">Apr.</xsl:when>
            <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
            <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
            <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
            <xsl:when test="$mo = '8' or $mo = '08'">Aug.</xsl:when>
            <xsl:when test="$mo = '9' or $mo = '09'">Sept.</xsl:when>
            <xsl:when test="$mo = '10'">Oct.</xsl:when>
            <xsl:when test="$mo = '11'">Nov.</xsl:when>
            <xsl:when test="$mo = '12'">Dec.</xsl:when>
        </xsl:choose>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$year"/>


    </xsl:template>

    <xsl:template match="vet_end_date">
        <!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->
        <xsl:variable name="mo">
            <xsl:value-of select="substring-before(. ,'/')" />
        </xsl:variable>
        <xsl:variable name="day-temp">
            <xsl:value-of select="substring-after(.,'/')" />
        </xsl:variable>
        <xsl:variable name="year">
            <xsl:value-of select="substring-after($day-temp,'/')" />
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$mo = '1' or $mo = '01'">Jan.</xsl:when>
            <xsl:when test="$mo = '2' or $mo = '02'">Feb.</xsl:when>
            <xsl:when test="$mo = '3' or $mo = '03'">Mar.</xsl:when>
            <xsl:when test="$mo = '4' or $mo = '04'">Apr.</xsl:when>
            <xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
            <xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
            <xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
            <xsl:when test="$mo = '8' or $mo = '08'">Aug.</xsl:when>
            <xsl:when test="$mo = '9' or $mo = '09'">Sept.</xsl:when>
            <xsl:when test="$mo = '10'">Oct.</xsl:when>
            <xsl:when test="$mo = '11'">Nov.</xsl:when>
            <xsl:when test="$mo = '12'">Dec.</xsl:when>
        </xsl:choose>
        <xsl:text> </xsl:text>
        <xsl:value-of select="$year"/>

    </xsl:template>


    <xsl:template match="completion_date">

        <!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
-->

        <xsl:variable name="year">
            <xsl:value-of select="substring-after(.,'/')" />
        </xsl:variable>

        <xsl:value-of select="$year"/>

    </xsl:template>

    <xsl:template match="completiondate">
        <!-- <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->

        <xsl:variable name="year">
            <xsl:value-of select="substring-after(.,'/')" />
        </xsl:variable>
        <xsl:value-of select="$year"/>
    </xsl:template>



    <xsl:template name="state">
        <xsl:param name="code" select="translate(.,$lowercase,$uppercase)" />
        <xsl:choose>
            <xsl:when test="$code='AE'">
                <xsl:value-of select="'A.F. Africa,CAN,EU,MidEast'"/>
            </xsl:when>
            <xsl:when test="$code='AA'">
                <xsl:value-of select="'A.F. Americas (Except CAN)'"/>
            </xsl:when>
            <xsl:when test="$code='AP'">
                <xsl:value-of select="'A.F. Pacific'"/>
            </xsl:when>
            <xsl:when test="$code='AL'">
                <xsl:value-of select="'Alabama'"/>
            </xsl:when>
            <xsl:when test="$code='AK'">
                <xsl:value-of select="'Alaska'"/>
            </xsl:when>
            <xsl:when test="$code='AS'">
                <xsl:value-of select="'American Samoa'"/>
            </xsl:when>
            <xsl:when test="$code='AZ'">
                <xsl:value-of select="'Arizona'"/>
            </xsl:when>
            <xsl:when test="$code='AR'">
                <xsl:value-of select="'Arkansas'"/>
            </xsl:when>
            <xsl:when test="$code='CA'">
                <xsl:value-of select="'California'"/>
            </xsl:when>
            <xsl:when test="$code='CO'">
                <xsl:value-of select="'Colorado'"/>
            </xsl:when>
            <xsl:when test="$code='CT'">
                <xsl:value-of select="'Connecticut'"/>
            </xsl:when>
            <xsl:when test="$code='DE'">
                <xsl:value-of select="'Delaware'"/>
            </xsl:when>
            <xsl:when test="$code='DC'">
                <xsl:value-of select="'District of Columbia'"/>
            </xsl:when>
            <xsl:when test="$code='FM'">
                <xsl:value-of select="'Fed. States of Micronesia'"/>
            </xsl:when>
            <xsl:when test="$code='FL'">
                <xsl:value-of select="'Florida'"/>
            </xsl:when>
            <xsl:when test="$code='GA'">
                <xsl:value-of select="'Georgia'"/>
            </xsl:when>
            <xsl:when test="$code='GU'">
                <xsl:value-of select="'Guam'"/>
            </xsl:when>
            <xsl:when test="$code='HI'">
                <xsl:value-of select="'Hawaii'"/>
            </xsl:when>
            <xsl:when test="$code='ID'">
                <xsl:value-of select="'Idaho'"/>
            </xsl:when>
            <xsl:when test="$code='IL'">
                <xsl:value-of select="'Illinois'"/>
            </xsl:when>
            <xsl:when test="$code='IN'">
                <xsl:value-of select="'Indiana'"/>
            </xsl:when>
            <xsl:when test="$code='IA'">
                <xsl:value-of select="'Iowa'"/>
            </xsl:when>
            <xsl:when test="$code='KS'">
                <xsl:value-of select="'Kansas'"/>
            </xsl:when>
            <xsl:when test="$code='KY'">
                <xsl:value-of select="'Kentucky'"/>
            </xsl:when>
            <xsl:when test="$code='LA'">
                <xsl:value-of select="'Louisiana'"/>
            </xsl:when>
            <xsl:when test="$code='ME'">
                <xsl:value-of select="'Maine'"/>
            </xsl:when>
            <xsl:when test="$code='MH'">
                <xsl:value-of select="'Marshall Islands'"/>
            </xsl:when>
            <xsl:when test="$code='MD'">
                <xsl:value-of select="'Maryland'"/>
            </xsl:when>
            <xsl:when test="$code='MA'">
                <xsl:value-of select="'Massachusetts'"/>
            </xsl:when>
            <xsl:when test="$code='MI'">
                <xsl:value-of select="'Michigan'"/>
            </xsl:when>
            <xsl:when test="$code='MN'">
                <xsl:value-of select="'Minnesota'"/>
            </xsl:when>
            <xsl:when test="$code='MS'">
                <xsl:value-of select="'Mississippi'"/>
            </xsl:when>
            <xsl:when test="$code='MO'">
                <xsl:value-of select="'Missouri'"/>
            </xsl:when>
            <xsl:when test="$code='MT'">
                <xsl:value-of select="'Montana'"/>
            </xsl:when>
            <xsl:when test="$code='NE'">
                <xsl:value-of select="'Nebraska'"/>
            </xsl:when>
            <xsl:when test="$code='NV'">
                <xsl:value-of select="'Nevada'"/>
            </xsl:when>
            <xsl:when test="$code='NH'">
                <xsl:value-of select="'New Hampshire'"/>
            </xsl:when>
            <xsl:when test="$code='NJ'">
                <xsl:value-of select="'New Jersey'"/>
            </xsl:when>
            <xsl:when test="$code='NM'">
                <xsl:value-of select="'New Mexico'"/>
            </xsl:when>
            <xsl:when test="$code='NY'">
                <xsl:value-of select="'New York'"/>
            </xsl:when>
            <xsl:when test="$code='NC'">
                <xsl:value-of select="'North Carolina'"/>
            </xsl:when>
            <xsl:when test="$code='ND'">
                <xsl:value-of select="'North Dakota'"/>
            </xsl:when>
            <xsl:when test="$code='MP'">
                <xsl:value-of select="'Northern Mariana Islands'"/>
            </xsl:when>
            <xsl:when test="$code='OH'">
                <xsl:value-of select="'Ohio'"/>
            </xsl:when>
            <xsl:when test="$code='OK'">
                <xsl:value-of select="'Oklahoma'"/>
            </xsl:when>
            <xsl:when test="$code='OR'">
                <xsl:value-of select="'Oregon'"/>
            </xsl:when>
            <xsl:when test="$code='ZZ'">
                <xsl:value-of select="'Outside U.S.'"/>
            </xsl:when>
            <xsl:when test="$code='PW'">
                <xsl:value-of select="'Palau'"/>
            </xsl:when>
            <xsl:when test="$code='PA'">
                <xsl:value-of select="'Pennsylvania'"/>
            </xsl:when>
            <xsl:when test="$code='PR'">
                <xsl:value-of select="'Puerto Rico'"/>
            </xsl:when>
            <xsl:when test="$code='RI'">
                <xsl:value-of select="'Rhode Island'"/>
            </xsl:when>
            <xsl:when test="$code='SC'">
                <xsl:value-of select="'South Carolina'"/>
            </xsl:when>
            <xsl:when test="$code='SD'">
                <xsl:value-of select="'South Dakota'"/>
            </xsl:when>
            <xsl:when test="$code='TN'">
                <xsl:value-of select="'Tennessee'"/>
            </xsl:when>
            <xsl:when test="$code='TX'">
                <xsl:value-of select="'Texas'"/>
            </xsl:when>
            <xsl:when test="$code='VI'">
                <xsl:value-of select="'U.S. Virgin Islands'"/>
            </xsl:when>
            <xsl:when test="$code='UT'">
                <xsl:value-of select="'Utah'"/>
            </xsl:when>
            <xsl:when test="$code='VT'">
                <xsl:value-of select="'Vermont'"/>
            </xsl:when>
            <xsl:when test="$code='VA'">
                <xsl:value-of select="'Virginia'"/>
            </xsl:when>
            <xsl:when test="$code='WA'">
                <xsl:value-of select="'Washington'"/>
            </xsl:when>
            <xsl:when test="$code='WV'">
                <xsl:value-of select="'West Virginia'"/>
            </xsl:when>
            <xsl:when test="$code='WI'">
                <xsl:value-of select="'Wisconsin'"/>
            </xsl:when>
            <xsl:when test="$code='WY'">
                <xsl:value-of select="'Wyoming'"/>
            </xsl:when>

            <xsl:otherwise>
                <xsl:value-of select="$code"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


</xsl:stylesheet>
