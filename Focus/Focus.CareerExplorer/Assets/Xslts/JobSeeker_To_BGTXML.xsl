<!-- 
    ************************************************************
    Copyright(c) 2010, Burning Glass Technologies
    XSL for converting EKOS XML to BGT XML in specific to SKY customer
    ************************************************************
    Version 1.1
    
    Change Log: 
	04/MAR/2011 SKG - Created the XSL
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml"  version="1.0" omit-xml-declaration="no" indent="yes" encoding="iso-8859-1"/>
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="yes" select="'YES'" />
  <xsl:variable name="disabled" select="'DISABLED'" />
  <xsl:template match="/">

    <ResDoc>
      <!-- special section-->
      <xsl:element name="special">
        <xsl:element name="cf001">
          <xsl:text>1</xsl:text>
        </xsl:element>
        <xsl:element name="cf002">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf003">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf004">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf005">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf006">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf007">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf008">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf009">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf010">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf011">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf012">
          <xsl:value-of select="RESUMEDOC/@ORIGIN_CD"/>
        </xsl:element>
        <xsl:element name="cf013">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf014">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf015">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf016">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf017">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf018">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf019">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf020">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf021">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf022">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf023">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf024">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="cf025">
          <xsl:text>0</xsl:text>
        </xsl:element>
        <xsl:element name="mode">
          <xsl:value-of select="RESUMEDOC/@MODE"/>
        </xsl:element>
        <xsl:element name="originid">
          <xsl:value-of select="RESUMEDOC/@ORIGIN_CD"/>
        </xsl:element>
        <xsl:element name="seeker_status_cd">
          <xsl:value-of select="RESUMEDOC/SEEKER_STATUS_CD"/>
        </xsl:element>
        <xsl:element name="job_seeker_flag">
          <xsl:value-of select="RESUMEDOC/JOB_SEEKER_FLAG"/>
        </xsl:element>

      </xsl:element>

      <!-- resume section -->
      <resume>
        <!-- contact section -->
        <contact>
          <!-- name section -->
          <name>
            <givenname>
              <xsl:value-of select="//FIRST_NAME"/>
            </givenname>
            <givenname>
              <xsl:value-of select="//MI"/>
            </givenname>
            <surname>
              <xsl:value-of select="//LAST_NAME"/>
            </surname>
          </name>
          <!-- address section -->
          Address :
          <address>
            <street>
              <xsl:value-of select="//ADDRESS1"/>
            </street>
            <street>
              <xsl:value-of select="//ADDRESS2"/>
            </street>
            <city>
              <xsl:value-of select="//CITY"/>
            </city>
            <state>
              <xsl:value-of select="//STATE"/>
            </state>
            <county>
              <xsl:value-of select="//COUNTY"/>
            </county>
            <country>
              <xsl:value-of select="//COUNTRY"/>
            </country>
            <postalcode>
              <xsl:value-of select="//ZIP"/>
            </postalcode>
          </address>

          Home Phone:
          <phone>
            <xsl:value-of select="//PHONE_PRIMARY"/>
          </phone>
          Work Phone:
          <phone>
            <xsl:value-of select="//PHONE_SECONDARY"/>
          </phone>
          <!-- <phone  type="fax">
			<xsl:value-of select="//fax_pri"/>
                            </phone>-->
          <email>
            <xsl:value-of select="//EMAIL"/>
          </email>

        </contact>
        <!--skills section -->

        <skills>
          <!--skills list-->
          <skills>
            <xsl:value-of select="//SKILLS"/>
          </skills>
          <!--certifications section-->
          <courses>
            <certifications>
              <xsl:for-each select="//CERTIFICATES">
                <certification>
                  <organization_name>
                    <xsl:value-of select="ORGANIZATION_NAME"/>
                  </organization_name>
                  <certificate>
                    <xsl:value-of select="CERTIFICATE"/>
                  </certificate>
                  <completion_date>
                    <xsl:call-template name="MMDDYYYYformatDateMMYYYY">
                      <xsl:with-param name="date" select="DATE_COMPLETED"/>
                    </xsl:call-template>
                  </completion_date>
                  <address>
                    <state>
                      <xsl:value-of select="STATE"/>
                    </state>
                    <country>
                      <xsl:value-of select="COUNTRY"/>
                    </country>
                  </address>
                </certification>
              </xsl:for-each>
            </certifications>
          </courses>
        </skills>

        <!--experience section-->
        <experience>
          <employment_status_cd>
            <xsl:value-of select="//EMPLOYMENT_STATUS_CD"/>
          </employment_status_cd>
          <xsl:for-each select="//WORK_HISTORY">
            <job>
              <employer>
                <xsl:value-of select="EMPLOYER"/>
              </employer>
              <title>
                <xsl:value-of select="JOB_TITLE"/>
              </title>
              <daterange>
                <start>
                  <!--<xsl:call-template name="DDMMYYYYformatDateYYYYMMDD">
                    <xsl:with-param name="date" select="START_DATE"/>
                  </xsl:call-template>-->
                  <xsl:value-of select="START_DATE"/>
                </start>
                <end>
                  <!--<xsl:call-template name="DDMMYYYYformatDateYYYYMMDD">
                    <xsl:with-param name="date" select="END_DATE"/>
                  </xsl:call-template>-->
                  <xsl:value-of select="END_DATE"/>
                </end>
              </daterange>
              <description>
                <xsl:value-of select="DUTIES"/>
              </description>
              <naics>
                <xsl:value-of select="NAICS"/>
              </naics>
            </job>
          </xsl:for-each>
        </experience>

        <!--education section-->
        <education>
          <norm_edu_level_cd>
            <xsl:value-of select="//NORMALIZED_EDU_LEVEL_CD"/>
          </norm_edu_level_cd>
          <school_status_cd>
            <xsl:value-of select ="//SCHOOL_STATUS_CD"/>
          </school_status_cd>

          <!--school section-->
          <school>
            <xsl:for-each select="//EDUCATION">
              <major>
                <xsl:value-of select="MAJOR"/>
              </major>
              <degree>
                <xsl:value-of select="DEGREE"/>
              </degree>
              <institution>
                <xsl:value-of select="SCHOOL_NAME"/>
              </institution>
              <address>
                <state>
                  <xsl:value-of select="STATE"/>
                </state>
                <country>
                  <xsl:value-of select="COUNTRY"/>
                </country>
              </address>
              <completiondate>
                <!--<xsl:attribute name="iso8601">
                  <xsl:call-template name="DDMMYYYYformatDateYYYYMMDD">
                    <xsl:with-param name="date" select="DATE_COMPLETED"/>
                  </xsl:call-template>
                </xsl:attribute>
                <xsl:call-template name="DDMMYYYYformatDateYYYYMMDD">
                  <xsl:with-param name="date" select="DATE_COMPLETED"/>
                </xsl:call-template>-->
                <xsl:value-of select="DATE_COMPLETED"/>
              </completiondate>
            </xsl:for-each>
          </school>
        </education>
        <!-- statements section -->
        <statements>
          <!-- personal section -->
          <personal>

            <veteran>
              <vet_flag>
                <xsl:if test="translate(//VETERAN_FLAG,$lowercase,$uppercase)=$yes">1</xsl:if>
                <xsl:if test="translate(//VETERAN_FLAG,$lowercase,$uppercase)!=$yes">0</xsl:if>
              </vet_flag>
              <vet_status>
                <xsl:value-of select="//VETERAN_STATUS_CD"/>
              </vet_status>
              <vet_start_date>
                <xsl:call-template name="YYYYMMDDformatDateMMDDYYYY">
                  <xsl:with-param name="date" select="//VETERAN_START_DATE"/>
                </xsl:call-template>
              </vet_start_date>
              <vet_end_date>
                <xsl:call-template name="YYYYMMDDformatDateMMDDYYYY">
                  <xsl:with-param name="date" select="//VETERAN_END_DATE"/>
                </xsl:call-template>
              </vet_end_date>
              <campaign_vet_flag>
                <xsl:if test="//CAMPAIGN_VETERAN_FLAG=1">-1</xsl:if>
                <xsl:if test="//CAMPAIGN_VETERAN_FLAG!=1">0</xsl:if>
              </campaign_vet_flag>
              <vet_disability_status>
                <xsl:if test="translate(//VET_DISABILITY_STATUS,$lowercase,$uppercase)=$disabled">-1</xsl:if>
                <xsl:if test="translate(//VET_DISABILITY_STATUS,$lowercase,$uppercase)!=$disabled">0</xsl:if>
              </vet_disability_status>
              <vet_tsm_type_cd>
                <xsl:value-of select="//VETERAN_TSM_TYPE_CD"/>
              </vet_tsm_type_cd>
              <vet_pre_1965_flag>
                <xsl:if test="//VIETNAM_PRE_1965_FLAG=1">-1</xsl:if>
                <xsl:if test="//VIETNAM_PRE_1965_FLAG!=1">0</xsl:if>
              </vet_pre_1965_flag>
            </veteran>
            <userinfo>
              <ssn>
                <xsl:value-of select="//SSN"/>
              </ssn>
              <username>
                <xsl:value-of select="//USERNAME"/>
              </username>
              <password>
                <xsl:value-of select="//PASSWORD"/>
              </password>
              <customerregistrationid>
                <xsl:value-of select="//RESUMEDOC/@ID"/>
              </customerregistrationid>
              <ssn_not_provided_flag>0</ssn_not_provided_flag>
            </userinfo>
            <dob>
              <!--<xsl:call-template name="MMDDYYYYformatDateYYYYMMDD">
                <xsl:with-param name="date" select="//DOB"/>
              </xsl:call-template>-->
              <xsl:value-of select="//DOB"/>
            </dob>
            <sex>
              <xsl:value-of select="//SEX"/>
            </sex>
            <citizen_flag>
              <xsl:if test="translate(//CITIZEN_FLAG,$lowercase,$uppercase)=$yes">-1</xsl:if>
              <xsl:if test="translate(//CITIZEN_FLAG,$lowercase,$uppercase)!=$yes">0</xsl:if>
            </citizen_flag>
            <alien_id>
              <xsl:value-of select="//ALIEN_ID"/>
            </alien_id>
            <alien_expires>
              <xsl:value-of select="//ALIEN_EXPIRES"/>
            </alien_expires>
            <alien_perm_flag>
              <xsl:if test="translate(//PERMENANT_ALIEN_FLAG,$lowercase,$uppercase)=$yes">-1</xsl:if>
              <xsl:if test="translate(//PERMENANT_ALIEN_FLAG,$lowercase,$uppercase)!=$yes">0</xsl:if>
            </alien_perm_flag>
            <employed_flag>
              <xsl:if test="translate(//EMPLOYED_FLAG,$lowercase,$uppercase)=$yes">-1</xsl:if>
              <xsl:if test="translate(//EMPLOYED_FLAG,$lowercase,$uppercase)!=$yes">0</xsl:if>
            </employed_flag>
            <disability_status>
              <xsl:value-of select="//DISABILITY_STATUS_CD"/>
            </disability_status>
            <disability_category_cd>
              <xsl:value-of select="//DISABILITY_CATEGORY_CD"/>
            </disability_category_cd>
            <ethnic_heritages>
              <xsl:for-each select="//ETHNECITY">
                <ethnic_heritage>
                  <ethnic_id>
                    <xsl:value-of select="ETHNICITY_ID"/>
                  </ethnic_id>
                  <xsl:if test="ETHNICITY_ID=3">
                    <selection_flag>1</selection_flag>
                  </xsl:if>
                  <xsl:if test="ETHNICITY_ID!=3">
                    <selection_flag>-1</selection_flag>
                  </xsl:if>
                </ethnic_heritage>
              </xsl:for-each>
            </ethnic_heritages>
            <internet_flag>
              <xsl:if test="//INTERNET_FLAG=1">-1</xsl:if>
              <xsl:if test="//INTERNET_FLAG!=1">0</xsl:if>
            </internet_flag>
            <confidential_flag>
              <xsl:if test="//CONFIDENTIAL_FLAG=1">-1</xsl:if>
              <xsl:if test="//CONFIDENTIAL_FLAG!=1">0</xsl:if>
            </confidential_flag>           
            <weeks_unemployed>
              <xsl:value-of select="//WEEKS_UNEMPLOYED"/>
            </weeks_unemployed>
            <available_date>
              <xsl:value-of select="//AVAILABLE_DATE"/>
            </available_date>
            
            <local_priority_flag>
              <xsl:if test="//LOCAL_PRIORITY_FLAG=1">-1</xsl:if>
              <xsl:if test="//LOCAL_PRIORITY_FLAG!=1">0</xsl:if>
            </local_priority_flag>
            <ui_claimant_status_cd>
              <xsl:value-of select="//UI_CLAIMANT_STATUS_CD"/>
            </ui_claimant_status_cd>
            <!--<job_seeker_flag>
              <xsl:value-of select="//job_seeker_flag"/>
            </job_seeker_flag>-->
            <!-- preferences section -->
            <preferences>
              <sal>
                <xsl:value-of select="//SALARY"/>
              </sal>
              <salary_unit_cd>
                <xsl:value-of select="//SALARY_UNIT_CD"/>
              </salary_unit_cd>
              <desired_titles>
                <desired_title>
                  <xsl:value-of select="//DESIRED_JOB_TITLE"/>
                </desired_title>
              </desired_titles>
              <desired_zips>
                <xsl:for-each select="//DESIRED_ZIP_AND_RADIUS">
                  <desired_zip>
                    <zip>
                      <xsl:value-of select="DESIRED_ZIP"/>
                    </zip>
                    <radius>
                      <xsl:value-of select="DESIRED_RADIUS"/>
                    </radius>
                  </desired_zip>
                </xsl:for-each>
              </desired_zips>
              <desired_states>
                <xsl:for-each select="//DESIRED_STATE">
                  <desired_state>
                    <xsl:attribute name="code">
                      <xsl:value-of select="STATE"/>
                    </xsl:attribute>
                  </desired_state>
                </xsl:for-each>
              </desired_states>
              <desired_countries>
                <xsl:for-each select="//DESIRED_COUNTRY">
                  <desired_country>
                    <xsl:value-of select="STATE"/>
                  </desired_country>
                </xsl:for-each>
              </desired_countries>
              <shift>
                <shift_first_flag>
                  <xsl:if test="//SHIFT_FIRST_FLAG=1">-1</xsl:if>
                  <xsl:if test="//SHIFT_FIRST_FLAG!=1">0</xsl:if>
                </shift_first_flag>
                <shift_second_flag>
                  <xsl:if test="//SHIFT_SECOND_FLAG=1">-1</xsl:if>
                  <xsl:if test="//SHIFT_SECOND_FLAG!=1">0</xsl:if>
                </shift_second_flag>
                <shift_third_flag>
                  <xsl:if test="//SHIFT_THIRD_FLAG=1">-1</xsl:if>
                  <xsl:if test="//SHIFT_THIRD_FLAG!=1">0</xsl:if>
                </shift_third_flag>
                <shift_rotating_flag>
                  <xsl:if test="//SHIFT_ROTATING_FLAG=1">-1</xsl:if>
                  <xsl:if test="//SHIFT_ROTATING_FLAG!=1">0</xsl:if>
                </shift_rotating_flag>
                <shift_split_flag>
                  <xsl:if test="//SHIFT_SPLIT_FLAG=1">-1</xsl:if>
                  <xsl:if test="//SHIFT_SPLIT_FLAG!=1">0</xsl:if>
                </shift_split_flag>
              </shift>
            </preferences>

            <!--license section -->
            <license>
              <driver_flag>
                <xsl:value-of select="//DRIVER_FLAG"/>
              </driver_flag>
              <driver_state>
                <xsl:value-of select="//DRIVER_STATE"/>
              </driver_state>
              <driver_class>
                <xsl:value-of select="//DRIVER_CLASS_CD"/>
              </driver_class>
              <drv_pass_flag>
                <xsl:if test="//DRIVER_PASS_FLAG=1">-1</xsl:if>
                <xsl:if test="//DRIVER_PASS_FLAG!=1">0</xsl:if>
              </drv_pass_flag>
              <drv_cycle_flag>
                <xsl:if test="//DRIVER_CYCLE_FLAG=1">-1</xsl:if>
                <xsl:if test="//DRIVER_CYCLE_FLAG!=1">0</xsl:if>
              </drv_cycle_flag>
              <drv_hazard_flag>
                <xsl:if test="//DRIVER_HAZARD_FLAG=1">-1</xsl:if>
                <xsl:if test="//DRIVER_HAZARD_FLAG!=1">0</xsl:if>
              </drv_hazard_flag>
              <drv_bus_flag>
                <xsl:if test="//DRIVER_BUS_FLAG=1">-1</xsl:if>
                <xsl:if test="//DRIVER_BUS_FLAG!=1">0</xsl:if>
              </drv_bus_flag>
              <drv_tank_flag>
                <xsl:if test="//DRIVER_TANK_FLAG=1">-1</xsl:if>
                <xsl:if test="//DRIVER_TANK_FLAG!=1">0</xsl:if>
              </drv_tank_flag>
              <drv_tankhazard_flag>
                <xsl:if test="//DRIVER_TANKHAZARD_FLAG=1">-1</xsl:if>
                <xsl:if test="//DRIVER_TANKHAZARD_FLAG!=1">0</xsl:if>
              </drv_tankhazard_flag>
              <drv_double_flag>
                <xsl:if test="//DRIVER_DOUBLE_FLAG=1">-1</xsl:if>
                <xsl:if test="//DRIVER_DOUBLE_FLAG!=1">0</xsl:if>
              </drv_double_flag>
              <drv_airbrake_flag>
                <xsl:if test="//DRIVER_AIRBRAKE_FLAG=1">-1</xsl:if>
                <xsl:if test="//DRIVER_AIRBRAKE_FLAG!=1">0</xsl:if>
              </drv_airbrake_flag>
            </license>
          </personal>

        </statements>

        <!-- objective section -->
        <objective>
          <xsl:value-of select="//OBJECTIVE"/>
        </objective>

      </resume>
      <clientresumedata>
      </clientresumedata>
    </ResDoc>

  </xsl:template>

  <xsl:template name="formatDateMMDDYYYY">
    <xsl:param name="date"/>
    <xsl:if test="string-length($date) = 10">
      <xsl:value-of select="substring($date, 1, 2)"/>
      <xsl:text>/</xsl:text>
      <xsl:value-of select="substring($date, 3, 2)"/>
      <xsl:text>/</xsl:text>
      <xsl:value-of select="substring($date, 5, 4)"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="DDMMYYYYformatDateYYYYMMDD">
    <xsl:param name="date"/>
    <xsl:if test="string-length($date) = 10">
      <xsl:value-of select="substring($date, 7, 4)"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="substring($date, 4, 2)"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="substring($date, 1, 2)"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="MMDDYYYYformatDateYYYYMMDD">
    <xsl:param name="date"/>
    <xsl:if test="string-length($date) = 10">
      <xsl:value-of select="substring($date, 7, 4)"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="substring($date, 1, 2)"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="substring($date, 4, 2)"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="MMDDYYYYformatDateMMYYYY">
    <xsl:param name="date"/>
    <xsl:if test="string-length($date) = 10">
      <xsl:value-of select="substring($date, 1, 2)"/>
      <xsl:text>/</xsl:text>
      <xsl:value-of select="substring($date, 7, 4)"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="YYYYMMDDformatDateMMDDYYYY">
    <xsl:param name="date"/>
    <xsl:if test="string-length($date) = 10">
      <xsl:value-of select="substring($date, 6, 2)"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="substring($date, 9, 2)"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="substring($date, 1, 4)"/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
