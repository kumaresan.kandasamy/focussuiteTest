<!-- 
*****************************************************************
Copyright(c) 2010, Burning Glass Technologies
XSL for Job alerts for Focus/Career in specific to BGT customer
*****************************************************************
Version 1.1

PLEASE NOTE: OUTPUT GENERATED BY THIS STYLE SHEET IS IN TEXT FORMAT
SINGLE SPACE PROVIDED WILL BE EMITTED IN THE OUTPUT; PLEASE DO NOT INTEND THIS OR ALIGN THE FORMAT
ALL THE DATA SHOULD BE IN LEFT ALIGN.

DEVELOPERS NOTE: THOUGH YOU CAN CHANGE THE INTEND DURING DEVELOPMENT, BUT PLEASE REMEMBER TO REMOVE THE INTEND
AND MAKE ALL THE LINES TO LEFT ALIGN AND BE CONFIDENT AND THINK TWICE BEFORE YOU USE <xsl:text> IN THIS STYLE SHEET

Change Log: 
07/MAY/2010 RRC  Created the XSL
12/MAY/2010 RRC - Added cached label in FOCUS URL
27/MAY/2010 RRC - Added Unsubscribe URL

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
<xsl:output method="text" version="1.0" indent="no" encoding="iso-8859-1"/>
<xsl:strip-space elements="*"/>

<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
<xsl:variable name="customername" select="'BGT'" />
<xsl:template match="/">

Hi <xsl:value-of select="jobalerts/userinfo/firstname"/>  <xsl:text> </xsl:text> <xsl:value-of select="jobalerts/userinfo/lastname"  />
Focus/Career Job Alert
<xsl:for-each select="jobalerts/posting[@customer=$customername] [position()=1]">
Your job matches from Focus/Career Job Alert
------------------------------------------------<xsl:text>&#x0A;</xsl:text>
Your resume generated the following matches
</xsl:for-each >
<xsl:for-each select="jobalerts/posting">
<xsl:choose>
<xsl:when test="translate(./@customer,$lowercase,$uppercase)=$customername">
<xsl:text>&#32;&#xA;&#xD;</xsl:text>
<xsl:value-of select="jobtitle"/>
<xsl:text>&#x0A;</xsl:text>
<xsl:value-of select="employer"/>
<xsl:if test='string-length(employer)>0'>
<xsl:text>, </xsl:text>
</xsl:if>
<xsl:value-of select="location"/>
<xsl:choose>
<xsl:when test="(score &gt; 0) and  (score &lt; 200)">
Match level: No Stars  <xsl:text> </xsl:text>
</xsl:when>
<xsl:when test="(score &gt; 199) and  score &lt; 400">
Match level: * (1 Star)<xsl:text>
</xsl:text>
</xsl:when>
<xsl:when test="(score &gt; 399) and  score &lt; 550">
Match level: ** (2 Stars) <xsl:text>
</xsl:text>
</xsl:when>
<xsl:when test="(score &gt; 549) and  score &lt; 700">
Match level: *** (3 Stars)<xsl:text>
</xsl:text>
</xsl:when>
<xsl:when test="(score &gt; 699) and  score &lt; 850">
Match level: **** (4 Stars) <xsl:text>
</xsl:text>
</xsl:when>
<xsl:when test="(score &gt; 849) and  score &lt; 1001">
Match level: ***** (5 Stars)<xsl:text>
</xsl:text>
</xsl:when>
</xsl:choose>
<xsl:value-of select="description"/>
<!--<xsl:if test="string-length(joburl)>0">
Job URL posted by employer:<xsl:value-of select="joburl"/>
</xsl:if>-->
<xsl:if test="string-length(focusurl)>0">
URL preserved by Focus/Career Job Alert (cached):<xsl:value-of select="focusurl"/>
</xsl:if>
</xsl:when>
</xsl:choose>
</xsl:for-each>
<xsl:text>&#x0A;</xsl:text>
-----------------------------------------------------------------------------------

You received this email because you signed up for job alerts.
To change settings or unsubscribe, go to: <xsl:value-of select="jobalerts/userinfo/unsubscribeurl"/>

-----------------------------------------------------------------------------------
Powered by Focus/Career
Copyright � 2011, Burning Glass International Inc.
</xsl:template>
</xsl:stylesheet>