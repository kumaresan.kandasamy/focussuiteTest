<!-- 
    ************************************************************
    Copyright(c) 2010, Burning Glass International Inc.
    XSL for Job alerts for Focus/Career in specific to BGT customer
    ************************************************************
    Version 1.1
    
    Change Log: 
		24/DEC/2010 RIR - Created for Forgot password
		04/APR/2013	MDW	-	Updated for next gen
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="html" version="1.0" omit-xml-declaration="no" indent="yes" encoding="iso-8859-1"/>
  <xsl:variable name="customername" select="'BGT'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
  <xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:template match="/">
    <style type="text/css">
      .font11 { font-family:arial;font-size:11px; }
      .font12 { font-family:arial;font-size:12px; }
      .font13 { font-family:arial;font-size:13px; }
      .font14 { font-family:arial;font-size:14px; }
      .font16 { font-family:arial;font-size:16px; }
      .font18 { font-family:arial;font-size:18px; }
      .font21 { font-family:arial;font-size:21px; }
    </style>
    <html>
      <table border="0" width="100%" role="presentation">
        <tr width="100%">
          <td width="80%">
            <!--  Main table -->
            <table border="0" width="100%" role="presentation">
              <tr width="100%">
                <td style="background-color:rgb(42, 48, 116);color:white;font-style:italic" width="100%">
									<font class="font14"><strong>Hi</strong></font>
                </td>
              </tr>
              <tr width="100%">
                <td width="100%" style="background-color:rgb(255,255,255);font-style:italic" >
                  <font class="font16">Username/Password help</font>
                </td>
              </tr>
              <tr>
                <td width="100%" style="background-color:white;" height="1px"/>
              </tr>
              <tr>
                <td width="100%" style="background-color:rgb(42, 48, 116);" height="3px"/>
              </tr>
            </table>
          </td>
          <td width="20%" align="right">
            <img src="{//url}{//logoUrl}"  />
          </td>
        </tr>
      </table>
      <table width="100%" border="0" role="presentation">
        <tr width="100%">
          <td width="100%">
            <font class="font13">
              This email is being sent to you because you forgot your password. The following new password has been generated for you:
              <br></br> <br></br>
              <!--Username: <xsl:value-of select="//username"/><br/-->
							Email Address: <xsl:value-of select="//email"/><br/>
              Password: <xsl:value-of select="//password"/><br/>
              <br></br>
              With this information, you should have no problem accessing your account. We do encourage you to come back often to
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="//url"/>
                </xsl:attribute>
                <xsl:attribute name="target">
                  <xsl:text>'newwindow'</xsl:text>
                </xsl:attribute>
                <font class="font13">
                  Focus/Career
                </font>
              </a> to manage your resume and job search.<br/>We wish you luck and look forward to helping you find your next job!
              <br></br> <br></br>
							<xsl:value-of select="//applicationName"/> Customer Support Team
              <br></br>
              Burning Glass International Inc.
            </font>
          </td>
        </tr>
      </table>
    </html>
  </xsl:template>
</xsl:stylesheet>