<?xml version='1.0'?>
<!-- 
    ************************************************************
    Copyright(c) 2010, Burning Glass Technologies
    XSL for Chronological format for Focus
    ************************************************************
    Version 3.5
    
    Change Log: 
	
	20-10-2011 UIM Created the XSLT
    20-10-2011 UIM Updated the Color of the description section
	21-10-2011 UIM Populated sections like summary,objective,references,statements and city and state values for Education section
	24-10-2011 SRIS Modified line spacing for education, jobs and reference
	25-10-2011 SRIS Realigned order of contact, Experience and Education
	28-10-2011 UIM Added Skills and Professional
	31-10-2011 SRIS  Updated style sheet to colorize details without suppressing resume details
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
<xsl:output method="html" indent="yes" encoding="utf-8" />

<xsl:template match="/">
<xsl:apply-templates select="//resume" />
</xsl:template>

<xsl:template match="resume">
	<xsl:apply-templates />
</xsl:template>

<!-- Contact section -->
<xsl:template match="contact|personal">
	<xsl:apply-templates />
</xsl:template>

<xsl:template match="contact/name|personal/name">
		<xsl:apply-templates/>
</xsl:template>

<xsl:template match="contact/name/givenname[1]|contact/name/surname[1]">
	<xsl:element name="font">
		<xsl:attribute name="class">contact yellowBg</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="contact/address/country|province|contact/address/street|contact/address/zipcode|pob">
	<xsl:element name="font">
			<xsl:attribute name="class">contact yellowBg</xsl:attribute>
			<xsl:text>&#160;</xsl:text>
			<xsl:apply-templates/>
		</xsl:element>
</xsl:template>

<xsl:template match="contact/address/city|contact/address/state|contact/address/postalcode">
	<xsl:element name="font">
		<xsl:attribute name="class">contact yellowBg</xsl:attribute>
		<xsl:text>&#160;</xsl:text>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="contact/phone|contact/email[1]|contact/website|personal/phone|personal/email[1]">
	<xsl:element name="font">
		<xsl:attribute name="class">contact yellowBg</xsl:attribute>
		<xsl:text>&#160;</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>,</xsl:text>
	</xsl:element>
</xsl:template>

<!-- Summary -->
<xsl:template match="summary">
	<xsl:element name="font"></xsl:element>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="daterange/start|daterange/end|completiondate|workperiod">
	 <xsl:element name="font">
		<xsl:attribute name="class">daterange</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>


<!-- Experience -->
<xsl:template match="job">
	<!--<br/>-->
	<xsl:apply-templates />
</xsl:template>

<xsl:template match="job/daterange/start">
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="job/daterange/end">
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="job/daterange[1]/start">
	 <xsl:element name="font">
		<xsl:attribute name="class">daterange</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="job/daterange[1]/end">
	 <xsl:element name="font">
		<xsl:attribute name="class">daterange</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="job/employer">
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="job/employer[1]">
	<xsl:element name="font">
		<xsl:attribute name="class">employer yellowBg</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="job/address">
	<font color="#000000">
		<xsl:text>&#160;</xsl:text>
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="job/address/state|job/address/city">
	<xsl:element name="font">
		<xsl:attribute name="class">location yellowBg</xsl:attribute>
		<xsl:text>&#160;</xsl:text>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="job/title[1]|job/function">
	<xsl:element name="font">
		<xsl:attribute name="class">jobposition yellowBg</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="job/description[1]">
	<xsl:element name="font">
		<xsl:attribute name="class">jobdescription</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>


<!-- Education -->
<xsl:template match="education/daterange/start|education/daterange/end">
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="school">
	<!--<br/>-->
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="school/institution[1]">
	<xsl:element name="font">
		<xsl:attribute name="class">institution yellowBg</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="school/daterange/start">
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="school/daterange/end">
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="school/daterange[1]/start">
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="school/daterange[1]/end">
	 <xsl:element name="font">
		<xsl:attribute name="class">daterange</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="school/major[1]">
	<xsl:element name="font">
		<xsl:attribute name="class">major yellowBg</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="school/address">
	<font color="#000000">
		<xsl:apply-templates/>
	</font>
</xsl:template>

<xsl:template match="school/address[1]/state|school/address[1]/street|school/address[1]/country|school/address[1]/postalcode">
	<xsl:element name="font">
		<xsl:attribute name="class">major yellowBg</xsl:attribute>
		<xsl:text>&#160;</xsl:text>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="school/degree">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="school/degree[1]">
	<xsl:element name="font">
		<xsl:attribute name="class">degree yellowBg</xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<xsl:template match="school/courses">
	<xsl:element name="font">
		<xsl:attribute name="class"></xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<!-- Skill -->
<xsl:template match="skills">
	<br/>
	<xsl:element name="font">
		<xsl:attribute name="class"></xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<!-- Professional -->
<xsl:template match="professional">
	<br/>
	<xsl:element name="font">
		<xsl:attribute name="class"></xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>

<!-- Statements -->
<xsl:template match="statements">
	<br/>
	<xsl:element name="font">
		<xsl:attribute name="class"></xsl:attribute>
		<xsl:apply-templates />
	</xsl:element>
</xsl:template>

<!-- References -->
<xsl:template match="references">
	<br/>
	<xsl:element name="font">
		<xsl:attribute name="class"></xsl:attribute>
		<xsl:apply-templates/>
	</xsl:element>
</xsl:template>


<xsl:template match="text()">
	<xsl:call-template name="add-line-breaks"> 
		<xsl:with-param name="string" select="." />
</xsl:call-template> 
</xsl:template>

<xsl:template name="add-line-breaks"> 
<xsl:param name="string" select="." />
	 <xsl:choose> 
		<xsl:when test="contains($string, '&#xA;')">
			 <xsl:value-of select="substring-before($string, '&#xA;')" /> 
			<br></br>
	 		<xsl:call-template name="add-line-breaks"> 
				<xsl:with-param name="string" select="substring-after($string, '&#xA;')" />
 			</xsl:call-template> 
		</xsl:when> 		
		<xsl:otherwise>			
			 <xsl:value-of select="$string" />
 		</xsl:otherwise>
	 </xsl:choose>	
</xsl:template> 

</xsl:stylesheet>