﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Framework.Core;

namespace Focus.CareerExplorer.Controls
{
	public partial class LanguageProficiencyManager : UserControlBase
	{
		public string ServicePath
		{
			set { aceLanguage.ServicePath = value; }
		}

		public string ServiceMethod
		{
			set { aceLanguage.ServiceMethod = value; }
		}

		public override bool Visible
		{
			set { UpdatePanel1.Visible = value; }
		}
    
    public string LanguageTextboxWidth { get; set; }
    public string LanguageProficiencyDropdownWidth { get; set; }
    public string LanguageTextboxRightMargin { get; set; }
    public bool ProficiencyLevelAndBelow { get; set; }
    public string InstructionalText { get; set; }
    public bool ReverseProficiencyOrder { get; set; }

    /// <summary>
    /// Handles the Load event of the control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void Page_Load(object sender, EventArgs e)
		{
			ApplyTheme();

			if (InstructionalText.IsNotNullOrEmpty()) LanguagesInstructionsLabel.DefaultText = InstructionalText;

		  if (Page.IsPostBack) 
				return;

		  BindProficiency();
		  LocaliseUi();

      // Set Controls
      LanguageTextBox.Width = LanguageTextboxWidth == null ? 250 : Unit.Parse(LanguageTextboxWidth);
      LanguageLabel.Width = LanguageTextBox.Width;
      ddlLanguageProficiency.Width = LanguageProficiencyDropdownWidth == null ? 250 : Unit.Parse(LanguageProficiencyDropdownWidth);
	    LanguageProficiencyLabel.Width = ddlLanguageProficiency.Width;

      if (LanguageTextboxRightMargin.IsNotNullOrEmpty()) LanguageTextBox.Style.Add("margin-right", LanguageTextboxRightMargin);
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUi()
		{
      LanguageTextBoxValidator.ErrorMessage = CodeLocalise("LanguageTextBoxValidator.ErrorMessage", "Duplicate languages types not allowed");
			LanguageProficiencyRequired.ErrorMessage = CodeLocalise("LanguageProficiency.RequiredErrorMessage", "Language proficiency level missing");
			ProficiencyMissing.ErrorMessage = CodeLocalise("LanguageProficiencyMissing.Error","Please update your languages to indicate your proficiency.");
		}

		private void ApplyTheme()
		{
			lnkLanguageProficiences.Visible = LanguagesInstructionsLabel.Visible =
				ProficiencyMissing.Visible = LanguageProficiencyLabel.Visible =
				ddlLanguageProficiency.Visible = (App.Settings.Theme == FocusThemes.Workforce);
		}

    /// <summary>
    /// Unbind the Languages Updateable List control (get items).
    /// </summary>
		public List<LanguageProficiency> UnBindLanguageRepeater()
		{
			if (UpdateableList.Items.IsNull())
				return null;

			return (from Item item in UpdateableList.Items
							select new LanguageProficiency
							{
								Language = item.Name,
								Proficiency = item.ExtraId
							}).ToList();
		}

    /// <summary>
    /// Binds the Languages Updateable List control.
    /// </summary>
    /// <param name="languageInfo">LanguageCriteria Class</param>
		public void BindLanguageRepeater(List<LanguageProficiency> languageInfo)
		{
			if (languageInfo.IsNullOrEmpty())
				return;

			var languageProficiencies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies);

			foreach (var item in languageInfo)
			{
				if (!item.Language.IsNotNullOrEmpty()) continue;

				LookupItemView lookupItemView = null;

				if (languageProficiencies != null)
				{
					if (item.Proficiency > 0)
						lookupItemView = languageProficiencies.SingleOrDefault(x => x.Id == item.Proficiency);
					else
						ProficiencyMissing.IsValid = false;
				}

				UpdateableList.AddItem(new Item
				{
					Id = item.Language,
          Name = item.Language,
				  ExtraName =
				    (lookupItemView != null)
              ? ProficiencyLevelAndBelow
				        ? CodeLocalise("OrBelow.Text", "{0} or below", lookupItemView.Text)
				        : lookupItemView.Text
				      : string.Empty,
					Type = "LANGUAGE",
					ExtraId = item.Proficiency
				});
			}
		}

    /// <summary>
    /// Handles the Click event of the LanguageAddButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void LanguageAddButton_Click(object sender, EventArgs e)
		{
      if (LanguageTextBox.TextTrimmed().IsNotNullOrEmpty())
      {
        if (App.Settings.Theme == FocusThemes.Education || ddlLanguageProficiency.SelectedIndex > 0)
        {
          var lookupItemView =
            ServiceClientLocator.CoreClient(App)
              .GetLookup(LookupTypes.LanguageProficiencies)
              .SingleOrDefault(x => x.Id.ToString(CultureInfo.InvariantCulture) == ddlLanguageProficiency.SelectedValue);
          var proficiencyName = string.Empty;

          if (lookupItemView != null)
            proficiencyName = ProficiencyLevelAndBelow
              ? lookupItemView.DisplayOrder > 1 ? CodeLocalise("OrBelow.Text", "{0} or below", lookupItemView.Text) : lookupItemView.Text
              : lookupItemView.Text;

          LanguageTextBoxValidator.IsValid = UpdateableList.AddTop(new Item
          {
            Id = LanguageTextBox.TextTrimmed(),
            Name = LanguageTextBox.TextTrimmed(),
            Type = "LANGUAGE",
            ExtraId = ddlLanguageProficiency.SelectedValueToLong(),
            ExtraName = proficiencyName
          });

          // Duplicate language
          if (LanguageTextBoxValidator.IsValid)
          {
            LanguageTextBox.Text = "";
            ddlLanguageProficiency.SelectedIndex = 0;
          }
        }
        else
        {
	        if (App.Settings.Theme == FocusThemes.Education) return;
	        // Language Proficiency required
	        LanguageTextBox.Text = "";
	        LanguageProficiencyRequired.IsValid = false;
        }
      }
		}

    /// <summary>
    /// Binds the Language Proficiency drop down control.
    /// </summary>
		private void BindProficiency()
    {
      var languageProficiencies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies)
                                                       .Select(l => new LookupItemView
                                                       {
                                                         Id = l.Id,
                                                         Text = ProficiencyLevelAndBelow && l.DisplayOrder > 1 ? CodeLocalise("OrBelow.Text", "{0} or below", l.Text) : l.Text
                                                       }).ToList();

		  if (ReverseProficiencyOrder) languageProficiencies.Reverse();

			ddlLanguageProficiency.DataTextField = "Text";
			ddlLanguageProficiency.DataValueField = "Id";
			ddlLanguageProficiency.DataSource = languageProficiencies;
			ddlLanguageProficiency.DataBind();
			ddlLanguageProficiency.Items.Insert(0, new ListItem("- select -", "0"));
		}

    /// <summary>
    /// Open the languages proficiencies modal.
    /// </summary>
		protected void lnkLanguageProficiences_Click(object sender, EventArgs e)
		{
			LangProficiencyModal.Show();
		}

    /// <summary>
    /// Server side validation the selection of a proficiency level
    /// </summary>
		protected void ProficiencyMissing_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			args.IsValid = UpdateableList.Items.IsNull() || !UpdateableList.Items.Any(item => item.ExtraId < 1);
		}
	}
}