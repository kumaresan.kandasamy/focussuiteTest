﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateableLanguageList.ascx.cs" Inherits="Focus.CareerExplorer.Controls.UpdateableLanguageList" %>
<%@ Register Src="DeleteResumeModal.ascx" TagName="DeleteResumeModal" TagPrefix="uc" %>

<asp:ListView ID="ItemsList" runat="server" OnItemDataBound="ItemsList_ItemDataBound">
	<LayoutTemplate>
		<table class="deletableListItemTable">
			<tr runat="server" id="itemPlaceHolder">
			</tr>
		</table>
	</LayoutTemplate>
	<ItemTemplate>
		<tr>
			<td id="tdBullet" runat="server">
				&bull; &nbsp;&nbsp;
			</td>
			<td class="column1">
				<asp:LinkButton ID="lnkName" runat="server" Text="<%# Items[Container.DataItemIndex].Name %> " OnClick="OnLinkNameClicked"></asp:LinkButton>
			</td>
			<td style="width: 10px;"></td>
			<td>
				<asp:LinkButton ID="LinkButton1" runat="server" Text="<%# Items[Container.DataItemIndex].ExtraName %> "></asp:LinkButton>
			</td>
			<td style="width: 10px;"></td>
			<td class="careercolumn2">
				<asp:ImageButton ID="ItemsListRemoveImageButton" runat="server" OnCommand="ItemsListRemoveButton_Command"
					CommandArgument="<%# Items[Container.DataItemIndex].Id %>" CommandName="<%# Items[Container.DataItemIndex].Type %>"
					ImageUrl="<%# UrlBuilder.CloseMedium() %>" CausesValidation="False" ClientIDMode="AutoID" style="position:relative; top:2px;" AlternateText='<%# HtmlLocalise("Global.Delete.ToolTip", "Delete") %>'
          data-action="DELETE" />
			</td>
		</tr>
	</ItemTemplate>
</asp:ListView>

<uc:DeleteResumeModal ID="DeleteResumeModal" runat="server" Visible="False" OnDeleteCommand="DeleteResumeModal_OnDeleteCommand" />


