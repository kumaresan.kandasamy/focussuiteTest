﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common.Code;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class ConfirmationModal : UserControlBase, IConfirmationModal
	{
		private const string CommandPrefix = "COMMAND:";
		private const string LinkPrefix = "LINK:";

		public delegate void CloseCommandHandler(object sender, CommandEventArgs eventArgs);
		public event CloseCommandHandler CloseCommand;

		public delegate void OkCommandHandler(object sender, CommandEventArgs eventArgs);
		public event OkCommandHandler OkCommand;

    /// <summary>
    /// Event for when the page loads
    /// </summary>
    /// <param name="sender">Page raising the event</param>
    /// <param name="e">Standard Event Argument</param>
	  protected void Page_Load(object sender, EventArgs e)
	  {
      // Because ConfirmationModals may appear multiple times on a page (if they are embedded in other user controls)
      // ensure the behaviour id is unique
			if (ModalPopup != null)	    
			ModalPopup.BehaviorID = string.Concat(ModalPopup.ClientID, "Behaviour");
	  }

	  /// <summary>
		/// Shows the modal.
		/// </summary>
		/// <param name="title">The title.</param>
		/// <param name="details">The details.</param>
		/// <param name="closeText">The close text.</param>
		/// <param name="closeCommandName">Name of the close command.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="okText">The ok text.</param>
		/// <param name="okCommandName">Name of the ok command.</param>
		/// <param name="okCommandArgument">The ok command argument.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <param name="y">The y.</param>
		public void Show(string title, string details, string closeText, string closeCommandName = "", string closeLink = "", string okText = null, string okCommandName = "", string okCommandArgument = "", int height = 200, int width = 400, int y = 210)
		{
			lblConfirmationModalTitle.Text = title;
			lblConfirmationModalMessage.Text = details;

			btnConfirmationModalClose.Text = closeText;

	    btnConfirmationModalClose.CommandName = string.Empty;
	    ModalPopup.CancelControlID = string.Empty;

			if (closeCommandName.IsNotNullOrEmpty())
				btnConfirmationModalClose.CommandName = CommandPrefix + closeCommandName;
			else if (closeLink.IsNotNullOrEmpty())
				btnConfirmationModalClose.CommandName = LinkPrefix + closeLink;
			else
				ModalPopup.CancelControlID = btnConfirmationModalClose.ClientID;

			if (okText.IsNotNullOrEmpty())
			{
				btnConfirmationModalOk.Text = okText;
				if (okCommandName.IsNotNullOrEmpty())
					btnConfirmationModalOk.CommandName = CommandPrefix + okCommandName;
				else if (okCommandArgument.IsNotNullOrEmpty())
					btnConfirmationModalOk.CommandArgument = okCommandArgument;
				else
					ModalPopup.CancelControlID = btnConfirmationModalOk.ClientID;
			}
			else
				btnConfirmationModalOk.Visible = false;

			//if (height != 0) lblConfirmationModalMessage.Style.Add("min-height", "auto");

			pnlConfirmationModalInner.Style.Add(HtmlTextWriterStyle.Width, ConvertToPixels(width));
            var viewHieght = (HiddenField)MasterPage.FindControl("viewPortHeight");
            ModalPopup.Y = Convert.ToInt16(viewHieght.Value.ToInt() * 0.35);
			ModalPopup.Show();
		}

		/// <summary>
		/// Converts an integer value to pixel string.
		/// </summary>
		/// <param name="size">The size.</param>
		/// <returns></returns>
		private string ConvertToPixels(int size)
		{
			return string.Format("{0}px", size);
		}

		/// <summary>
		/// Handles the Command event of the CloseButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void btnConfirmationModalClose_Command(object sender, CommandEventArgs eventArgs)
		{
			var command = GetCommand(eventArgs);
			var link = GetLink(eventArgs);

			if (command.IsNotNullOrEmpty())
				OnCloseCommand(new CommandEventArgs(command, null));
			else if (link.IsNotNullOrEmpty())
				Response.Redirect(link, true);

			ModalPopup.Hide();
		}

		/// <summary>
		/// Handles the Command event of the btnConfirmationModalOk control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void btnConfirmationModalOk_Command(object sender, CommandEventArgs eventArgs)
		{
			var command = GetCommand(eventArgs);

			if (command.IsNotNullOrEmpty())
				OnOkCommand(new CommandEventArgs(command, eventArgs.CommandArgument));

			ModalPopup.Hide();
		}

		/// <summary>
		/// Raises the <see cref="E:CloseCommand"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnCloseCommand(CommandEventArgs eventArgs)
		{
			if (CloseCommand != null)
				CloseCommand(this, eventArgs);
		}

		/// <summary>
		/// Raises the <see cref="E:OkCommand"/> event.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnOkCommand(CommandEventArgs eventArgs)
		{
			if (OkCommand != null)
				OkCommand(this, eventArgs);
		}

		/// <summary>
		/// Gets the command.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		/// <returns></returns>
		private static string GetCommand(CommandEventArgs eventArgs)
		{
			if (eventArgs.IsNull() || eventArgs.CommandName.IsNullOrEmpty())
				return null;

			var commandFull = eventArgs.CommandName;

			if (commandFull.StartsWith(CommandPrefix))
				return commandFull.Substring(8);

			return null;
		}

		/// <summary>
		/// Gets the link.
		/// </summary>
		/// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		/// <returns></returns>
		private static string GetLink(CommandEventArgs eventArgs)
		{
			if (eventArgs.IsNull() || eventArgs.CommandName.IsNullOrEmpty())
				return null;

			var commandFull = eventArgs.CommandName;

			if (commandFull.StartsWith(LinkPrefix))
				return commandFull.Substring(5);

			return null;
		}
	}
}
