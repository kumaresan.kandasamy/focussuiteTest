﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.CareerExplorer.Code;
using Focus.Common.Helpers;
using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class ResumeSearchLink : UserControlBase
	{
		private const string ResumeUrl = "/resume";
		private const string SearchResultsUrl = "searchresults";
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      DashboardLink.Visible = (App.Settings.Theme == FocusThemes.Education && !App.IsAnonymousAccess());

			if (App.User.IsAuthenticated)
			{
				YourResumeLink.NavigateUrl = UrlBuilder.YourResume();
				YourResumeLink.Enabled = true;
			}

      if (App.IsAnonymousAccess())
		    ResumeLink.Visible = false;

      SearchJobsLink.NavigateUrl = (App.IsAnonymousAccess() || App.Settings.Theme == FocusThemes.Workforce) 
        ? UrlBuilder.JobSearchCriteria() 
        : UrlBuilder.SearchPostingsEducation();

      if (App.GuideToResume())
        DashboardLink.Visible = JobSearchListItem.Visible = false;

			if (!Request.Url.AbsolutePath.Contains(SearchResultsUrl))
			{
				SearchResultsBackLink.Visible = false;
			}

			if (Request.UrlReferrer.IsNotNull() && Request.UrlReferrer.AbsolutePath.StartsWith(ResumeUrl))
			{
				SearchResultsBackLink.NavigateUrl = Request.UrlReferrer.AbsoluteUri;
			}
			else
			{
				SearchResultsBackLink.NavigateUrl = UrlBuilder.JobSearchCriteria();
			}
		}
	}
}
