﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorMessage.ascx.cs"
	Inherits="Focus.CareerExplorer.Controls.ErrorMessage" %>
<div id="divErrorMessages" class="dismissableStatusMessage" runat="server" visible="false">
	<img id="imgErrorMessage" src="<%= UrlBuilder.CloseMedium() %>"
		alt="<%= HtmlLocalise("Global.Close.ToolTip", "Close") %>" width="13" height="13" onclick="closeErrorMessage()" />
	<asp:Label ID="ErrorMessagesLabel" runat="server" ClientIDMode="Static" Style="font-weight: bold;" />
</div>
<script type="text/javascript">
	function closeErrorMessage() {
		$('.dismissableStatusMessage').hide();
		return false;
	}
</script>
