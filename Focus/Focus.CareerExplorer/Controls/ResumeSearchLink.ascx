﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeSearchLink.ascx.cs"
    Inherits="Focus.CareerExplorer.Controls.ResumeSearchLink" %>
<nav class="breadcrumb" style="clear: both">
	<ol>
		<li id="DashboardLink" runat="server" EnableViewState="False">
		  <a href="<%=UrlBuilder.Career() %>">
		    <focus:LocalisedLabel runat="server" ID="DashboardLinkLabel" LocalisationKey="DashboardLink.LinkText" DefaultText="Dashboard" RenderOuterSpan="False"/>
		  </a>
		</li>
		<li id="ResumeLink" runat="server">
		  <asp:HyperLink ID="YourResumeLink" runat="server" NavigateUrl="#" Enabled="False"><focus:LocalisedLabel runat="server" ID="YourResume" LocalisationKey="YourResume.LinkText" DefaultText="Your resume" RenderOuterSpan="False" /></asp:HyperLink>
		</li>
		<li id="JobSearchListItem" runat="server">
		  <asp:HyperLink ID="SearchJobsLink" runat="server"><focus:LocalisedLabel runat="server" ID="SearchJobs" LocalisationKey="SearchJob.LinkText" DefaultText="Search jobs" RenderOuterSpan="False" /></asp:HyperLink>
		</li>
		<li id="BackLink" runat="server" class="pull-right">
			<asp:HyperLink ID="SearchResultsBackLink" runat="server"><focus:LocalisedLabel runat="server" ID="ResultsBackLink" LocalisationKey="SearchResultBackLink.LinkText" DefaultText="Back" RenderOuterSpan="False" /></asp:HyperLink>
		</li>
	</ol>
</nav>
