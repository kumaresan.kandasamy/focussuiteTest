﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Framework.Core;
using Focus.CareerExplorer.Code;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class AlertModal : UserControlBase
	{
		/// <summary>
		/// Shows the modal alert dialog.
		/// </summary>
		/// <param name="closeLink">The close link.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		/// <param name="top">The top.</param>
		public void Show(string closeLink = "", int height = 20, int width = 400, int top = 210)
		{
			var lastError = App.GetSessionValue<string>(Constants.StateKeys.PageError);

			if (lastError.IsNullOrEmpty()) throw new Exception("The last handled error could not be found.");

			Show(lastError, closeLink, height, width);
		}

		/// <summary>
		/// Shows the modal alert dialog.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		public void Show(string message, string closeLink = "", int height = 20, int width = 400)
		{
			Show(AlertTypes.Error, message, CodeLocalise("Error.Title", "Error"), closeLink, height, width);
		}

		/// <summary>
		/// Shows the modal alert dialog.
		/// </summary>
		/// <param name="alertType">Type of the alert.</param>
		/// <param name="message">The message.</param>
		/// <param name="title">The title.</param>
		/// <param name="closeLink">The close link.</param>
		/// <param name="height">The height.</param>
		/// <param name="width">The width.</param>
		public void Show(AlertTypes alertType, string message, string title, string closeLink = "", int height = 20, int width = 400)
		{
			if (alertType == AlertTypes.Error) AlertModalPanel.CssClass += " errorModal";
			pnlAlertModalInner.Attributes.Add("style", string.Format("width:{0}px;", width));

			lblAlertModalTitle.Text = (alertType == AlertTypes.Error && title.IsNullOrEmpty()) ? CodeLocalise("Error.Title", "Error") : title;

			lblAlertModalMessage.Text = message;

			imgAlertModalClose.Attributes.Add("onclick", GenerateCloseLink(closeLink));
			imgAlertModalClose.Attributes.Add("onkeypress", GenerateCloseLink(closeLink));

			btnAlertModalClose.Text = HtmlLocalise("Global.Close.Text", "Close");
			btnAlertModalClose.OnClientClick = GenerateCloseLink(closeLink);

			AlertModalPopup.Show();
		}

    /// <summary>
    /// Generates the close link.
    /// </summary>
    /// <param name="closeLink">The close link.</param>
    /// <returns></returns>
		private string GenerateCloseLink(string closeLink)
		{
			return closeLink.IsNotNullOrEmpty()
				       ? string.Format("window.location='{0}'", closeLink)
				       : string.Format("$find('{0}').hide(); return false;", AlertModalPopup.ClientID);
		}

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				imgAlertModalClose.ImageUrl = UrlBuilder.Close();
				//imgAlertModalClose.AlternateText = HtmlLocalise("Global.Close.Text", "Close");
			}
		}
	}
}