﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobList.ascx.cs" Inherits="Focus.CareerExplorer.Controls.JobList" %>
<%@ Import Namespace="Focus.Core.Models.Career" %>
<asp:ListView ID="JobListView" runat="server" ItemPlaceholderID="JobListPlaceHolder"
	DataSourceID="JobListDataSource" OnItemDataBound="JobListView_ItemDataBound">
	<LayoutTemplate>
		<table class="jobList" role="presentation">
			<asp:PlaceHolder ID="JobListPlaceHolder" runat="server" />
		</table>
	</LayoutTemplate>
	<ItemTemplate>
		<tr>
			<td class="column1">
				<%# GetStars(((PostingSearchResultView)Container.DataItem).Rank)%>
			</td>
			<td class="column2">
				<asp:HyperLink ID="JobTitleHyperLink" runat="server" onclick="javascript:showProcessing();">
           <%#((PostingSearchResultView)Container.DataItem).JobTitle%>
				</asp:HyperLink>
			</td>
			<td class="column3">
				<%# ((PostingSearchResultView)Container.DataItem).Employer%>
			</td>
			<td class="column4">
				<%#((PostingSearchResultView)Container.DataItem).Location%>
			</td>
			<td class="column5">
				<%#((PostingSearchResultView)Container.DataItem).JobDate.ToString("dd MMM")%>
			</td>
		</tr>
	</ItemTemplate>
	<EmptyDataTemplate>
		<table class="jobList" role="presentation">
			<tr>
				<td><p><focus:LocalisedLabel runat="server" ID="NoResultsLabel" LocalisationKey="NoResults.Text" DefaultText="No matching job found" RenderOuterSpan="False"/></p></td>
			</tr>
		</table>
	</EmptyDataTemplate>
</asp:ListView>
<asp:ObjectDataSource ID="JobListDataSource" runat="server" TypeName="Focus.CareerExplorer.Controls.JobList"
	SelectMethod="GetSearchResults" SelectCountMethod="GetSearchResultCount" />
