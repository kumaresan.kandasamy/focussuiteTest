﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlertModal.ascx.cs" Inherits="Focus.CareerExplorer.Controls.AlertModal" %>

<asp:HiddenField ID="AlertModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="AlertModalPopup" runat="server" ClientIDMode="Static"
												TargetControlID="AlertModalDummyTarget"
												PopupControlID="AlertModalPanel"
												PopupDragHandleControlID="AlertModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground">
	<Animations>
		<OnShown>
			<ScriptAction Script="setButtonFocus('#btnAlertModalClose');" /> 
		</OnShown>
	</Animations>
</act:ModalPopupExtender>

<asp:Panel runat="server" ID="AlertModalPanel" CssClass="modal" style="display:none;" ClientIDMode="Static">
	<asp:Panel runat="server" ID="pnlAlertModalInner">
		<div class="modalHeader">
			<asp:Image runat="server" ID="imgAlertModalClose" CssClass="modalCloseIcon" alt="Close Alert Image"></asp:Image>
			<asp:Label runat="server" ID="lblAlertModalTitle" CssClass="modalTitle modalTitleLarge"></asp:Label>
		</div>
		<asp:Label runat="server" ID="lblAlertModalMessage" CssClass="modalMessage"></asp:Label>
		<div class="modalButtons"><asp:Button ID="btnAlertModalClose" runat="server" CssClass="buttonLevel2" ClientIDMode="Static" /></div>
	</asp:Panel>
</asp:Panel>
