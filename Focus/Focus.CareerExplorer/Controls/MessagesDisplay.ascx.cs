﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class MessagesDisplay : UserControlBase
	{
		/// <summary>
		/// Gets or sets the module so we can show the correct messages.
		/// </summary>
		/// <value>The module.</value>
		public FocusModules Module { get; set; }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Bind();
			}
		}

		/// <summary>
		/// Handles the ItemCommand event of the MessagesRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
		protected void MessagesRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Dismiss":
					long messageId;
					long.TryParse(e.CommandArgument.ToString(), out messageId);
					if (messageId != 0) ServiceClientLocator.CoreClient(App).DismissMessage(messageId);
					break;
			}

			Bind();
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			if (Module.IsNull()) throw new Exception("Unknown module");

			if (!App.User.IsAuthenticated)
				return;

			MessagesRepeater.DataSource = ServiceClientLocator.CoreClient(App).GetMessages(Module);
			MessagesRepeater.DataBind();
		}
	}
}