﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageProficiencyManager.ascx.cs"
    Inherits="Focus.CareerExplorer.Controls.LanguageProficiencyManager" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register Src="~/Controls/UpdateableList.ascx" TagName="UpdateableLanguageList"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/LanguageProficiencyDefinitionsModal.ascx" TagName="LangProficiencyDefinitionModal"
    TagPrefix="uc" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="dataInputField">
            <div class="instructionalText">
                <focus:LocalisedLabel runat="server" ID="LanguagesInstructionsLabel" RenderOuterSpan="False"
                    LocalisationKey="LanguagesInstructions.Label" DefaultText="Indicate all languages in which you are proficient." />
                <asp:LinkButton ID="lnkLanguageProficiences" runat="server" OnClick="lnkLanguageProficiences_Click">
                    <focus:LocalisedLabel runat="server" ID="PreviewLanguageProficiences" RenderOuterSpan="False"
                        LocalisationKey="PreviewLanguageProficiences.LinkText" DefaultText="(Language proficiency definitions)" />
                </asp:LinkButton>
            </div>
        </div>
        <div class="dataInputRow">
            <div class="dataInputGroupedItemHorizontal dataInputField col-xs-12 col-sm-5 col-md-5"
                style="margin-bottom: 10px;">
                <focus:LocalisedLabel runat="server" ID="LanguageTypeLabel" AssociatedControlID="LanguageTextBox"
                    CssClass="requiredData" LocalisationKey="Language.Label" DefaultText="Language" />
                <span class="inFieldLabel">
                    <focus:LocalisedLabel runat="server" ID="LanguageLabel" AssociatedControlID="LanguageTextBox"
                        LocalisationKey="Language.Label" DefaultText="Language" Width="250" />
                </span>
                <span id="language-outer">
                    <asp:TextBox ID="LanguageTextBox" runat="server" Width="250" ClientIDMode="Static" />
                </span>
                <asp:CustomValidator ID="LanguageTextBoxValidator" runat="server" CssClass="error"
                    ControlToValidate="LanguageTextBox" ValidateEmptyText="true" SetFocusOnError="True" />
                <ajaxtoolkit:AutoCompleteExtender ID="aceLanguage" runat="server" CompletionSetCount="20"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetLanguages"
                    ServicePath="~/Services/AjaxService.svc" TargetControlID="LanguageTextBox" CompletionInterval="1000"
                    UseContextKey="false" EnableCaching="true" MinimumPrefixLength="3" OnClientShown="fixAutoCompleteExtender">
                </ajaxtoolkit:AutoCompleteExtender>
            </div>
            <div class="dataInputGroupedItemHorizontal dataInputField col-xs-12 col-sm-6 col-md-6">
                <focus:LocalisedLabel runat="server" ID="LanguageProficiencyLabel" AssociatedControlID="ddlLanguageProficiency"
                    CssClass="requiredData" LocalisationKey="LanguageProficiency.Label" DefaultText="Language Proficiency" />
                <br />
                <span id="language-proficiency-outer">
                    <asp:DropDownList runat="server" ID="ddlLanguageProficiency" Enabled="false" Width="250" />
                 </span>
                   <asp:LinkButton ID="LanguageAddButton" runat="server" ClientIDMode="Static" Text="+ Add"
                        Style="margin-left: 10px;" OnClick="LanguageAddButton_Click" ValidationGroup="LanguageProficiency" />
                <asp:CustomValidator ID="LanguageProficiencyRequired" runat="server" ControlToValidate="ddlLanguageProficiency"
                    CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="LanguageProficiency"
                    ValidateEmptyText="True" ClientValidationFunction="ValidateLanguageProficiency" />
            </div>
        </div>
        <div class="dataInputRow">
            <asp:CustomValidator ID="ProficiencyMissing" runat="server" CssClass="error" SetFocusOnError="true"
                Display="Dynamic" ValidationGroup="Education" ValidateEmptyText="True" OnServerValidate="ProficiencyMissing_OnServerValidate" />
        </div>
        <div class="dataInputRow">
            <asp:Panel ID="LanguagePanel" CssClass="resumeEducationLanguageList" runat="server"
                ScrollBars="Auto">
                <uc:UpdateableLanguageList ID="UpdateableList" runat="server" DisplayBullet="false"
                    AddConfirmationModal="false" HasExtraInfo="True" RestrictDuplicates="true" />
            </asp:Panel>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="LanguageAddButton" EventName="Click" />
    </Triggers>
</asp:UpdatePanel>
<uc:LangProficiencyDefinitionModal ID="LangProficiencyModal" runat="server" />
<script type="text/javascript">

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(styleLanguageProficiencyControls);

    Sys.Application.add_load(LanguageProficiency_PageLoad);

    function LanguageProficiency_PageLoad() {

        SetLanguageProficiencyUIState();
        var lang = $("[id$='LanguageTextBox']").on("input", function () {

            var langProf = $("[id$='ddlLanguageProficiency']");
            if (lang.val().length > 0) {
                EnableStyledDropDown(langProf);
            } else {
                DisableStyledDropDown(langProf);
            }
        });
    };

    function styleLanguageProficiencyControls() {
        $("[id$='ddlLanguageProficiency']").customSelect();
        $("[id$='LanguageLabel']").inFieldLabels();
        SetLanguageProficiencyUIState();
    }

    function SetLanguageProficiencyUIState() {
        var lang = $("#<%= LanguageTextBox.ClientID %>");
        var langProf = $("[id$='ddlLanguageProficiency']");
        if ($(langProf).exists() && $(langProf).is(':disabled') && lang.val().length == 0) {
            DisableStyledDropDown(langProf, true);
        } else {
            EnableStyledDropDown(langProf);
        }
    }

    function ValidateLanguageProficiency(src, args) {
        var lang = $("#<%= LanguageTextBox.ClientID %>");
        var proficiency = $("[id$='ddlLanguageProficiency']");
        if (lang.val().length > 0 && !proficiency.is(":disabled")) {
            if (proficiency.children().first().is(':selected')) {
                args.IsValid = false;
            }
        }
    }
</script>
