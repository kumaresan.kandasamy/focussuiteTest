﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pager.ascx.cs" Inherits="Focus.CareerExplorer.Controls.Pager" %>
<asp:Panel runat="server" ID="pnlPagerContainer">
	<asp:Panel runat="server" ID="pnlPagerSizer" CssClass="pagerSizer form-inline col-xs-12 col-sm-6 col-md-6">
		Showing
		<%=(StartRowIndex + 1)%>-<%= ((StartRowIndex + PageSize) < TotalRowCount) ? (StartRowIndex + PageSize) : TotalRowCount%>
		of
		<%=(TotalRowCount)%>
		jobs
		<asp:DropDownList aria-label="Pager size" ID="PageSizer" ClientIDMode="Static" runat="server" CausesValidation="false" AutoPostBack="true" OnSelectedIndexChanged="PageSizer_SelectedIndexChanged">
			<asp:ListItem Text="10" Value="10" />
			<asp:ListItem Text="25" Value="25" />
			<asp:ListItem Text="50" Value="50" />
			<asp:ListItem Text="100" Value="100" />
		</asp:DropDownList>
		jobs per page
	</asp:Panel>
	<div class="pagerNavigation col-xs-12 col-sm-6 col-md-6">
		<asp:DataPager ID="MainDataPager" runat="server" style="float: right">
			<Fields>
				<asp:NextPreviousPagerField ShowFirstPageButton="true" ShowNextPageButton="false"
					ShowLastPageButton="false" ShowPreviousPageButton="true" ButtonType="Link" FirstPageText="&#171;"
					PreviousPageText="previous" />
				<asp:NumericPagerField ButtonType="Link" ButtonCount="5"  />
				<asp:NextPreviousPagerField ShowFirstPageButton="false" ShowNextPageButton="true"
					ShowLastPageButton="true" ShowPreviousPageButton="false" ButtonType="Link" LastPageText="&#187;"
					NextPageText="next" />
			</Fields>
		</asp:DataPager>
	</div>
</asp:Panel>
