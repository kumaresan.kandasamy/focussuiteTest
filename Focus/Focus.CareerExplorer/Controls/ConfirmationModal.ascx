﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ConfirmationModal.ascx.cs"
	Inherits="Focus.CareerExplorer.Controls.ConfirmationModal" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
	BehaviorID="ModalPopupBehaviour" PopupControlID="ModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
	<Animations>
					<OnShown>
               <ScriptAction Script="SetFocusToCloseButton();" />  
          </OnShown>
	</Animations>
</act:ModalPopupExtender>
<asp:Panel ID="ModalPanel" tabindex="-1" runat="server" CssClass="modal" Style="display: none; z-index: 100001;">
	<asp:Panel runat="server" ID="pnlConfirmationModalInner" style="max-width: 100%">
		<div class="modalHeader">
			<asp:Label runat="server" ID="lblConfirmationModalTitle" CssClass="modalTitle"></asp:Label>
		</div>
		<asp:Label runat="server" ID="lblConfirmationModalMessage" CssClass="modalMessage"></asp:Label>
		<div class="modalButtons">
			<asp:Button ID="btnConfirmationModalClose" runat="server" CssClass="buttonLevel3" CausesValidation="false" OnCommand="btnConfirmationModalClose_Command" ToolTip="OK"/>
			<asp:Button ID="btnConfirmationModalOk" runat="server" CssClass="buttonLevel2" CausesValidation="false" OnCommand="btnConfirmationModalOk_Command" ToolTip="Cancel"/>
		</div>
	</asp:Panel>
</asp:Panel>
<script type="text/javascript">
	function SetFocusToCloseButton() {
		setButtonFocus('#<%= btnConfirmationModalClose.ClientID %>');

		if ($('#lbSelectedStatements').exists()) {
			alwaysOnTop.init({
				targetid: 'selectedQuestions',
				orientation: 4,
				position: [50, 5],
				fadeduration: [0, 0],
				frequency: 1,
				hideafter: 0
			});
		}

		if ($('#ddlReportSpamOrClosed').exists())
			ApplyStyleToDropDowns();

		if ($('#ddlCounty').exists())
			$(".CountyClass").next().find(':first-child').text($("#ddlCounty option:selected").text()).parent().addClass('changed');

		if ($('#ddlMSA').exists())
			$(".MSAClass").next().find(':first-child').text($("#ddlMSA option:selected").text()).parent().addClass('changed');

		if ($('#ddlRank').exists())
			$(".RankClass").next().find(':first-child').text($("#ddlRank option:selected").text()).parent().addClass('changed');

		if ($('#chkCurrentlyWorkHere').exists())
			chkJobTitle();

		if ($('#SelectedStatementsHiddenField').exists() && $('#lbSelectedStatements').exists())
			UpdateSelectedStatementsList();
	}
</script>
