﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PageLocalise.ascx.cs" Inherits="Focus.Web.Code.Controls.User.PageLocalise" %>
<asp:Panel ID="pnlPageLocaliseItems" runat="server">
  <asp:UpdatePanel ID="UpdateItems" runat="server">
  <ContentTemplate>
	<asp:GridView ID="grdPageLocaliseItems" runat="server" AllowPaging="false" AllowSorting="false"  AutoGenerateColumns="False" OnRowDataBound="OnRowDataBound"
		 Width="100%" CellPadding="0" BorderWidth="0" CssClass="Grid" AlternatingRowStyle="gridAlternateRow">
		<Columns>
			<asp:BoundField DataField="LocaliseKey" HeaderText="Key" ReadOnly="true" ItemStyle-Width="25%" />
			<asp:BoundField DataField="DefaultValue" HeaderText="Default Value" ReadOnly="true" ItemStyle-Width="35%" />
			<asp:TemplateField HeaderText="Localised Value">
				<ItemTemplate>
					<asp:Label ID="lblLocalisedValue" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LocalisedValue") %>'></asp:Label>
         </ItemTemplate>
				</asp:TemplateField>
        <asp:TemplateField>
				<ItemTemplate>
					<asp:Button Width="100%" ID="btnUpdate" runat="server" CausesValidation="false" Text="Update" CssClass="button7"/>
				</ItemTemplate>
				</asp:TemplateField>
		</Columns>
	</asp:GridView>
  </ContentTemplate>
 <%-- <Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="OnClick" />
  </Triggers>--%>
  </asp:UpdatePanel>
 </asp:Panel>
