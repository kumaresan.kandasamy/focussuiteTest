﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.CareerExplorer.Controls
{
  public partial class Bookmarks : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        BindControls();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the BookmarkRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void BookmarkRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var bookmark = (BookmarkDto)e.Item.DataItem;

        var viewHyperLink = (HyperLink)e.Item.FindControl("ViewBookmarkHyperLink");

				switch(bookmark.Type)
				{
					case BookmarkTypes.ReportItem:
						viewHyperLink.NavigateUrl = UrlBuilder.Explore("bookmark", bookmark.Id.Value.ToString());
						break;
					case BookmarkTypes.Posting:
						viewHyperLink.NavigateUrl = GetRouteUrl("JobPosting", new { jobid = bookmark.Criteria, fromURL = "Home", pageSection = "Bookmarks" });
						break;
					default:
						viewHyperLink.NavigateUrl = UrlBuilder.Report(bookmark.Id.Value);
						break;
				}
				
        var deleteImageButton = (ImageButton)e.Item.FindControl("DeleteBookmarkImageButton");
        deleteImageButton.AlternateText = CodeLocalise("DeleteBookmark.Text", "Delete Bookmark");
      }
    }

    /// <summary>
    /// Handles the ItemCommand event of the BookmarkRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void BookmarkRepeater_ItemCommand(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "DeleteBookmark":

          long bookmarkId;
          if (long.TryParse(e.CommandArgument.ToString(), out bookmarkId))
          {
						ServiceClientLocator.AnnotationClient(App).DeleteBookmark(bookmarkId);
            BindControls();
          }

          break;
      }
    }

    /// <summary>
    /// Binds the controls.
    /// </summary>
    private void BindControls()
    {
			if (!App.User.IsAuthenticated) return;

			var expBookmarks = ServiceClientLocator.AnnotationClient(App).GetMyBookmarks();
      BookmarkRepeater.DataSource = expBookmarks;
      BookmarkRepeater.DataBind();
      
      if (expBookmarks.Count <= 0)
        Panel1.Visible = true;
    }
  }
}