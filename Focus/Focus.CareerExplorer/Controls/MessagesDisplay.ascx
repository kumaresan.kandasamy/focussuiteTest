﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessagesDisplay.ascx.cs" Inherits="Focus.CareerExplorer.Controls.MessagesDisplay" %>
<%@ Import Namespace="Focus.Core.Views" %>

<asp:UpdatePanel ID="MessagesUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<asp:Repeater	ID="MessagesRepeater" runat="server" OnItemCommand="MessagesRepeater_ItemCommand">
			<ItemTemplate>
				<p class="<%# ((MessageView)Container.DataItem).IsSystemAlert ? "messageSystem" : "messageStatus" %>">
				<asp:ImageButton ID="DismissMessageButton" runat="server" CommandName="Dismiss" CommandArgument="<%# ((MessageView)Container.DataItem).Id%>" ImageUrl="<%# UrlBuilder.CloseRoundOff() %>" CausesValidation="False" ImageAlign="Top" alt="Dismiss message"/>
				<%# ((MessageView)Container.DataItem).Text%>	
				</p>
			</ItemTemplate>
		</asp:Repeater>
	</ContentTemplate>
</asp:UpdatePanel>