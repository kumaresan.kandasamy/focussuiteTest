﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.CareerExplorer.Code;
using Focus.Common.Controls.Server;
using Focus.Common.Extensions;
using Focus.Core;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class TabNavigations : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				WorkforceTabs.Visible = true;
				if (IsHomePage()) TabWorkforceHome.Attributes["class"] += " active";
				if (IsJobSearchPage()) TabJobSearch.Attributes["class"] += " active";
			}
			else
			{
				EducationTabs.Visible = true;
				if (IsCareerHomePage() || IsJobSearchPage()) TabEducationHome.Attributes["class"] += " active";
			}

			if (IsNoticePage())
			{
				TabNotices.Attributes["class"] += " active";
				TabNotices.Visible = true;

				TabJobSearch.Visible = false;
				TabExplore.Visible = false;
				TabWorkforceHome.Visible = false;
				EducationTabs.Visible = false;
			}

			if (IsExplorePage()) 
        TabExplore.Attributes["class"] += " active";

			if (App.Settings.Module == FocusModules.Explorer)
				TabJobSearch.Visible = false;

			if (App.Settings.Module == FocusModules.Career)
				TabExplore.Visible = false;

      if (App.IsAnonymousAccess())
		    TabWorkforceHome.Visible = false;

		  if (App.GuideToResume())
        TabExplore.Visible = false;

		  SetTabOrder();
		}

    private void SetTabOrder()
    {
      OrderedTabs.Order = (App.Settings.CareerExplorerFeatureEmphasis == CareerExplorerFeatureEmphasis.Explorer)
                            ? OrderedContainer.RenderOrder.Reverse
                            : OrderedContainer.RenderOrder.Normal;
    }

		/// <summary>
		/// Determines whether [is home page].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is home page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsHomePage()
		{
			return (Page.Is(UrlBuilder.Home()) || Page.Is(UrlBuilder.MyBookmarks()));
		}

    private bool IsCareerHomePage()
    {
      return Page.Is(UrlBuilder.Career());
    }

		/// <summary>
		/// Determines whether [is job search page].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is job search page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsJobSearchPage()
		{
			return (Page.Is(UrlBuilder.JobSearch()) || Page.Is(UrlBuilder.ResumeWizard()) || Page.Is(UrlBuilder.YourResume()) || Page.Is(UrlBuilder.JobPosting()));
		}

		/// <summary>
		/// Determines whether [is explore page].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is explore page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsExplorePage()
		{
			return Page.IsDescendantOfFolder(UrlBuilder.ExplorerRoot());
		}

		/// <summary>
		/// Determines whether [is notice page].
		/// </summary>
		/// <returns>
		///   <c>true</c> if [is notice page]; otherwise, <c>false</c>.
		/// </returns>
		private bool IsNoticePage()
		{
			return Page.IsDescendantOfFolder(UrlBuilder.NoticeRoot());
		}
	}
}
