﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.CareerExplorer.WebAuth.Controls;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class UnAuthorizedInformation : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
		}
    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			SigninButton.Text = CodeLocalise("SigninButton.Label", "Sign in");
			CreateAccountButton.Text = CodeLocalise("CreateAccountButton.Label", "Create an Account");
			WithoutRegisterButton.Text = CodeLocalise("WithoutRegisterButton.Label", "Continue Without Registering");
		}

    /// <summary>
    /// Shows this instance.
    /// </summary>
		public void Show()
		{
			ConfirmationModal.Show();
		}

		#region Confirmation Messages

    /// <summary>
    /// Handles the Click event of the SigninButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void SigninButton_Click(object sender, EventArgs e)
		{
			var login = (LogInOrRegister)Page.Master.FindControl("MainLogIn");
			login.ShowLogIn();
		}

    /// <summary>
    /// Handles the Click event of the CreateAccountButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void CreateAccountButton_Click(object sender, EventArgs e)
		{
			var login = (LogInOrRegister)Page.Master.FindControl("MainLogIn");
			login.ShowRegister("");
		}

		#endregion
	}
}