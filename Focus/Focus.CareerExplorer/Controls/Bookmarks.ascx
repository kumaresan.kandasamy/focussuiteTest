﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Bookmarks.ascx.cs" Inherits="Focus.CareerExplorer.Controls.Bookmarks" %>
<%@ Register Src="~/Controls/UpdateableList.ascx" TagName="UpdateableList" TagPrefix="uc" %>
<div class="bookmarkList">
	<asp:Repeater ID="BookmarkRepeater" runat="server" OnItemDataBound="BookmarkRepeater_ItemDataBound" OnItemCommand="BookmarkRepeater_ItemCommand">
		<HeaderTemplate><ul></HeaderTemplate>
		<ItemTemplate>
			<li>
				<asp:HyperLink ID="ViewBookmarkHyperLink" runat="server"><%# Eval("Name") %></asp:HyperLink>&nbsp;
				<asp:ImageButton ID="DeleteBookmarkImageButton" runat="server" ImageUrl="<%# UrlBuilder.CloseMedium() %>" CommandName="DeleteBookmark" CommandArgument='<%# Eval("Id") %>' AlternateText='<%# HtmlLocalise("Global.Delete.ToolTip", "Delete") %>' ></asp:ImageButton>
			</li>
		</ItemTemplate>
		<FooterTemplate></ul></FooterTemplate>
	</asp:Repeater>
	<asp:Panel ID="Panel1" runat="server" Visible="false"><focus:LocalisedLabel runat="server" ID="MyBookmarks2Label" LocalisationKey="MyBookmarks2.Label" DefaultText="Click on the 'bookmark' link to save it here." RenderOuterSpan="False"/></asp:Panel>
</div>
