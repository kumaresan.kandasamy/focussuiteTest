﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Focus.Core;

using Framework.Core;

#endregion
namespace Focus.CareerExplorer.Controls
{
	public partial class UpdateableLanguageList : UserControlBase
	{
		private Boolean _isDisplayBullet = true;
		private string _linkUrlText = "";
		protected Boolean IsDisplayBullet = true;
		protected Boolean IsAddConfirmationModal = true;
		protected string LinkURLText = "";
		protected string DeleteLinkURLText = "";

		/// <summary>
		/// Gets or sets the display bullet.
		/// </summary>
		public Boolean DisplayBullet
		{
			get { return _isDisplayBullet; }
			set { _isDisplayBullet = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [add confirmation modal].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [add confirmation modal]; otherwise, <c>false</c>.
		/// </value>
		public Boolean AddConfirmationModal
		{
			get
			{
				return IsAddConfirmationModal;
			}
			set
			{
				IsAddConfirmationModal = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [set confirmation session].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [set confirmation session]; otherwise, <c>false</c>.
		/// </value>
		public Boolean SetConfirmationSession { get; set; }

		/// <summary>
		/// Gets or sets the target page.
		/// </summary>
		public string TargetPage
		{
			get { return _linkUrlText; }
			set { _linkUrlText = value; }
		}

		/// <summary>
		/// Gets or sets the delete redirection page.
		/// </summary>
		/// <value>The delete redirection page.</value>
		public string DeleteRedirectionPage
		{
			get
			{
				return DeleteLinkURLText;
			}
			set
			{
				DeleteLinkURLText = value;
			}
		}

		public bool RestrictDuplicates { get; set; }

		public delegate void LinkNameClick(object sender, EventArgs eventArgs);
		public event UpdateableList.LinkNameClick LinkNameClicked;

		public delegate void ItemRemovedHandler(object sender, CommandEventArgs eventArgs);
		public event UpdateableList.ItemRemovedHandler ItemRemoved;

		/// <summary>
		/// Gets or sets the items.
		/// </summary>
		/// <value>
		/// The items.
		/// </value>
		public List<Item> Items
		{
			get { return GetViewStateValue<List<Item>>("UpdateableList:" + ID + ":Items"); }
			set
			{
				SetViewStateValue("UpdateableList:" + ID + ":Items", value);
				Bind();
			}
		}

		/// <summary>
		/// Handles the ItemDataBound event of the ItemsList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs" /> instance containing the event data.</param>
		protected void ItemsList_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var lbtnName = (LinkButton)e.Item.FindControl("lnkName");
			
			var item = (Item)e.Item.DataItem;

			e.Item.FindControl("tdBullet").Visible = _isDisplayBullet;
			
			if (TargetPage.IsNotNullOrEmpty())
			{
				lbtnName.Enabled = true;
				lbtnName.CommandArgument = item.Id;
				//lbtnName.
				lbtnName.CommandName = TargetPage;
				if (IsDisplayBullet)
					lbtnName.OnClientClick = "showProcessing();";
			}
			else
				lbtnName.Enabled = false;
		}

		/// <summary>
		/// Called when [link name click].
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="eventArgs">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected virtual void OnLinkNameClicked(object sender, EventArgs eventArgs)
		{
			if (LinkNameClicked != null)
			{
				App.RemoveSessionValue("Career:DuplicateResume");
				LinkNameClicked(sender, eventArgs);
			}
		}

		/// <summary>
		/// Called when an item is removed.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="eventArgs">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected virtual void OnItemRemoved(object sender, CommandEventArgs eventArgs)
		{
			if (ItemRemoved != null)
				ItemRemoved(sender, eventArgs);
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				Bind();
		}

		/// <summary>
		/// Adds the item.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public bool AddItem(Item item)
		{
			if (Items == null)
				Items = new List<Item>();

			if (RestrictDuplicates && Items.Contains(item))
				return false;

			Items.Add(item);
			Bind();

			return true;
		}

		/// <summary>
		/// Adds the top.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public bool AddTop(Item item)
		{
			if (Items == null)
				Items = new List<Item>();
			if (RestrictDuplicates && Items.Contains(item))
				return false;
			Items.Insert(0, item);
			Bind();
			return true;
		}

		/// <summary>
		/// Handles the Command event of the ItemsListRemoveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
		protected void ItemsListRemoveButton_Command(object sender, CommandEventArgs e)
		{
			try
			{
				switch (e.CommandName.ToUpper())
				{
					case "DELETE_RESUME":
						if (SetConfirmationSession)
							App.SetSessionValue("Career:DeleteResume:ResumeID", ClientID);

						DeleteResumeModal.Visible = true;
						DeleteResumeModal.Show(Convert.ToInt64(e.CommandArgument));
						break;

					case "INDUSTRY":
					case "TARGETINDUSTRY":
					case "SKILL":
					case "LANGUAGE":
						var id = e.CommandArgument.ToString();
						Items.RemoveAll(x => x.Id == id);
						Bind();
						break;
				}

				OnItemRemoved(this, e);
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			ItemsList.DataSource = Items;
			ItemsList.DataBind();
		}

		/// <summary>
		/// Fires when the resume has been deleted
		/// </summary>
		/// <param name="sender">Delete Resume control</param>
		/// <param name="eventargs">Event arguments containing details of the deleted resume</param>
		protected void DeleteResumeModal_OnDeleteCommand(object sender, CommandEventArgs eventargs)
		{
			var resumeId = eventargs.CommandArgument;

			if (eventargs.CommandName != "DELETED")
				return;

			Items.RemoveAll(x => x.Id == resumeId.ToString());

			Bind();

			Response.RedirectToRoute(DeleteRedirectionPage.IsNotNullOrEmpty() ? DeleteRedirectionPage : "YourResume");
		}
	}

}