﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class JobList : UserControlBase
	{
		private int _searchResultCount;

		/// <summary>
		/// Gets or sets the result.
		/// </summary>
		private List<PostingSearchResultView> Result
		{
			get
			{
			  var contextValue = App.GetContextValue<List<PostingSearchResultView>>("JobList:Result");
			  if (contextValue == null)
			  {
          contextValue = GetResults();
          App.SetContextValue("JobList:Result", contextValue);
			  }

			  return contextValue;
			}
			set
			{
			  App.SetContextValue("JobList:Result", value);
			}
		}

    /// <summary>
    /// Gets or sets whether more jobs exist in the database
    /// </summary>
    public bool MoreJobsExist
    {
      get { return GetViewStateValue<bool>("JobList:MoreJobsExist"); }
      private set { SetViewStateValue("JobList:MoreJobsExist", value); }
    }

    /// <summary>
    /// Maximum job of jobs to list
    /// </summary>
    public int? MaximumJobCount { get; set; }

    /// <summary>
    /// Whether the list has already been loaded
    /// </summary>
	  private bool _listLoaded;
		
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
        if (!_listLoaded)
				  LoadList();
			}
		}
    
		/// <summary>
		/// Loads the list.
		/// </summary>
		public void LoadList()
		{
			try
			{
			  if (!App.User.IsAuthenticated) return;
			  var criteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");

			  if (criteria == null) return;

        var jobLimit = MaximumJobCount ?? 5;

				// If we have not posted back then the list is being loaded automatically by the control so don't mark it as a manual search
        Result = GetResults();

        MoreJobsExist = (Result.Count > jobLimit);
        if (MoreJobsExist)
          Result.RemoveAt(jobLimit);

			  BindSearchResultList();

			  _listLoaded = true;
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Gets the results of the job search from Lens
    /// </summary>
    /// <returns>A list of matching jobs</returns>
    private List<PostingSearchResultView> GetResults()
    {
      if (!App.User.IsAuthenticated) 
        return null;

      var criteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");

      if (criteria == null) 
        return null;

      var jobLimit = MaximumJobCount ?? 5;

      return ServiceClientLocator.SearchClient(App).GetSearchResults(criteria, jobLimit + 1, IsPostBack);
    }

		/// <summary>
		/// Gets the stars.
		/// </summary>
		/// <param name="rating">The rating.</param>
		/// <returns></returns>
		protected string GetStars(int rating)
		{
			rating = rating.Between(1, 199) ? 0 : rating;
			rating = rating.Between(200, 399) ? 1 : rating;
			rating = rating.Between(400, 549) ? 2 : rating;
			rating = rating.Between(550, 699) ? 3 : rating;
			rating = rating.Between(700, 849) ? 4 : rating;
			rating = rating.Between(850, 1000) ? 5 : rating;
			
			var output = string.Empty;

			for (var j = 1; j <= rating; j++) output += "<img src=\"" + UrlBuilder.StarOn() + "\" alt=\"Star Rating - "+ j + "\" />";
			for (var j = 1; j <= 5 - rating; j++) output += "<img src=\"" + UrlBuilder.StarOff() + "\" alt=\"\" />";

			return output;
		}

		/// <summary>
		/// Handles the ItemDataBound event of the JobListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs" /> instance containing the event data.</param>
		protected void JobListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var posting = (PostingSearchResultView)e.Item.DataItem;

			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var jobtitle = (HyperLink)e.Item.FindControl("JobTitleHyperLink");
				jobtitle.NavigateUrl = GetRouteUrl("JobPosting", new { jobid = posting.Id, fromURL = "Home", pageSection = "SavedSearches" });
			}
		}

		#region ObjectDataSource Method

		/// <summary>
		/// Gets the search results.
		/// </summary>
		/// <returns></returns>
		public List<PostingSearchResultView> GetSearchResults()
		{
			List<PostingSearchResultView> searchResults = null;

			if (Result != null && Result.Count > 0)
			{
				searchResults = Result;//.Take(5).ToList();
				_searchResultCount = Result.Count();

			}

			return searchResults;
		}

		/// <summary>
		/// Gets the referrals count.
		/// </summary>
		/// <returns></returns>
		public int GetSearchResultCount()
		{
			return _searchResultCount;
		}

		/// <summary>
		/// Binds the search result list.
		/// </summary>
		private void BindSearchResultList()
		{
			JobListView.DataBind();
		}

		#endregion
	}
}
