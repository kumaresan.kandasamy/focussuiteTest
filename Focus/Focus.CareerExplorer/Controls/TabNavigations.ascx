﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabNavigations.ascx.cs" Inherits="Focus.CareerExplorer.Controls.TabNavigations" %>

<nav class="navTab">
	<ul>
		<asp:PlaceHolder runat="server" ID="WorkforceTabs" EnableViewState="False" Visible="False">
			<li id="TabWorkforceHome" class="mainTabWorkforceHome" runat="server"><a href="<%= UrlBuilder.Career() %>" onclick="javascript:showProcessing();"><focus:LocalisedLabel runat="server" ID="TabWorkforceHomeLabel" LocalisationKey="WorkforceHome.Tab" DefaultText="Home" RenderOuterSpan="False"/></a></li>
			<li id="TabJobSearch" class="mainTabWorkforceSearch" runat="server"><a href="<%= UrlBuilder.JobSearch() %>" onclick="javascript:showProcessing();"><focus:LocalisedLabel runat="server" ID="TabJobSearchLabel" LocalisationKey="JobSearch.Tab" DefaultText="Job Search" RenderOuterSpan="False"/></a></li>	
		</asp:PlaceHolder>
    <focus:OrderedContainer ID="OrderedTabs" runat="server" Order="Normal">
		  <asp:PlaceHolder runat="server" ID="EducationTabs" EnableViewState="False" Visible="False">
			  <li id="TabEducationHome" class="mainTabEducationHome" runat="server"><a href="<%= UrlBuilder.Career() %>" onclick="javascript:showProcessing();"><focus:LocalisedLabel runat="server" ID="TabEducationHomeLabel" LocalisationKey="EducationHome.Tab" DefaultText="Jobs/Internship search" RenderOuterSpan="False"/></a></li>
		  </asp:PlaceHolder>
		  <li id="TabExplore" class="mainTabExplore" runat="server"><a href="<%= UrlBuilder.Explore() %>" onclick="javascript:showProcessing();"><focus:LocalisedLabel runat="server" ID="TabExploreLabel" LocalisationKey="Explore.Tab" DefaultText="Explore Careers & Education" RenderOuterSpan="False"/></a></li>
      <li id="TabNotices" class="mainTabExplore" runat="server" Visible="False"><a href="#" ><focus:LocalisedLabel runat="server" ID="TabNoticesLabel" LocalisationKey="Notices.Tab" DefaultText="Notices" RenderOuterSpan="False"/></a></li>
    </focus:OrderedContainer>
	</ul>
</nav>
