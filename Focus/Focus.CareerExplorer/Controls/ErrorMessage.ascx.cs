﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class ErrorMessage : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
				divErrorMessages.Visible = false;
		}

		/// <summary>
		/// Shows the specified alert type.
		/// </summary>
		/// <param name="alertType">Type of the alert.</param>
		/// <param name="errorMessage">The error message.</param>
		public void Show(AlertTypes alertType, string errorMessage)
		{
			divErrorMessages.Visible = true;
			ErrorMessagesLabel.Text = errorMessage;

			switch (alertType)
			{
				case AlertTypes.Error:
					ErrorMessagesLabel.CssClass = "alertError";
					break;

				case AlertTypes.Info:
					ErrorMessagesLabel.CssClass = "alertInfo";
					break;
			}
		}
	}
}