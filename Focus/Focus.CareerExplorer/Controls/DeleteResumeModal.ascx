﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeleteResumeModal.ascx.cs" Inherits="Focus.CareerExplorer.Controls.DeleteResumeModal" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
	BehaviorID="ModalPopupBehaviour" PopupControlID="ModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
	<Animations>
					<OnShown>
               <ScriptAction Script="SetFocusToCloseButton();" />  
          </OnShown>
	</Animations>
</act:ModalPopupExtender>

<asp:Panel ID="ModalPanel" tabindex="-1" runat="server" CssClass="modal" Style="display: none; z-index: 100001;">
	<asp:Panel runat="server" ID="DeleteResumeModalInnerPanel" Style="width: 400px;max-width:100%">
		<div class="modalHeader">
			<asp:Label runat="server" ID="DeleteResumeModalTitle" CssClass="modalTitle" data-modal="title"></asp:Label>
		</div>
		<asp:Label runat="server" ID="DeleteResumeModalMessage" CssClass="modalMessage" data-modal="text"></asp:Label>
    <asp:Panel runat="server" ID="CompletedResumesPanel">
      <asp:RadioButtonList runat="server" ID="RadioButtons"></asp:RadioButtonList>
      <asp:RequiredFieldValidator ID="ResumeRequired" runat="server" ControlToValidate="RadioButtons" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="DeleteResumeModalValidation" />
    </asp:Panel>
		<div class="modalButtons">
			<asp:Button ID="DeleteButton" runat="server" CssClass="buttonLevel3" OnCommand="DeleteButton_OnCommand" CommandName="DELETE" ValidationGroup="DeleteResumeModalValidation" />
			<asp:Button ID="CloseButton" runat="server" CssClass="buttonLevel3" CausesValidation="false" />
		</div>
	</asp:Panel>
</asp:Panel>

<script type="text/javascript">
  function SetFocusToCloseButton() {
		if ($('#<%= DeleteButton.ClientID %>').exists()) {
		  setButtonFocus('#<%= DeleteButton.ClientID %>');
	  } else {
		  setButtonFocus('#<%= CloseButton.ClientID %>');
	  }
  }
</script>