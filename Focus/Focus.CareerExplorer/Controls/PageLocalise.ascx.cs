﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Focus.Common.Helpers;
using Focus.Core;

#endregion

namespace Focus.Web.Code.Controls.User
{
  public partial class PageLocalise : UserControlBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        if (Context.User.IsInRole(Constants.RoleKeys.SystemAdmin))
        {
        pnlPageLocaliseItems.Visible = true;
        BindPageLocaliseItems();
        }
        else
        {
          pnlPageLocaliseItems.Visible = false;
        }
      }
    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
      if (e.Row.RowType != DataControlRowType.DataRow) return;
      var button = (Button) e.Row.FindControl("btnUpdate");
	    if (button != null)
	    {
		    button.Attributes.Add("onclick",
			    "javascript:OpenLocalisePopup ('" + DataBinder.Eval(e.Row.DataItem, "LocaliseKey") + "','" +
			    HttpContext.Current.Request.ApplicationPath +
					"');return false;");
				button.Attributes.Add("onkeypress",
					"javascript:OpenLocalisePopup ('" + DataBinder.Eval(e.Row.DataItem, "LocaliseKey") + "','" +
					HttpContext.Current.Request.ApplicationPath +
					"');return false;");
	    }
    }

    private void BindPageLocaliseItems()
    {
      var pageLocaliseKeys = LocalisationModeHelper.PageLocaliseKeys;
      var localisationModeHelper = new LocalisationModeHelper(App);
      var dict = localisationModeHelper.LocalisationDictionary;
     
      var pageEntries = (from pde in dict.Values
                         join plk in pageLocaliseKeys on pde.LocaliseKeyUpper
                           equals plk
                         orderby pde.LocaliseKeyUpper
                         select pde).ToList();

      grdPageLocaliseItems.DataSource = pageEntries;
      grdPageLocaliseItems.DataBind();
    }
  
    protected void grdPageLocaliseItems_RowEditing(object sender, GridViewEditEventArgs e)
    {
      grdPageLocaliseItems.EditIndex = e.NewEditIndex;
      BindPageLocaliseItems();
    }

    protected void grdPageLocaliseItems_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
      grdPageLocaliseItems.EditIndex = -1;
      BindPageLocaliseItems();
    }

    protected void grdPageLocaliseItems_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
      var row = grdPageLocaliseItems.Rows[e.RowIndex];

      var hidLocaliseKey = (HtmlInputHidden)row.FindControl("hidLocaliseKey");
      var txtLocalisedValue = (TextBox)row.FindControl("txtLocalisedValue");
      var hidDefaultValue = (HtmlInputHidden)row.FindControl("hidDefaultValue");
      if (hidLocaliseKey != null && !String.IsNullOrEmpty(hidLocaliseKey.Value)
          && txtLocalisedValue != null)
      {
        var localisationModeHelper = new LocalisationModeHelper(App);
        localisationModeHelper.SavePortalLocalisationItem(hidLocaliseKey.Value, txtLocalisedValue.Text,
                                                          hidDefaultValue.Value);
      }

      grdPageLocaliseItems.EditIndex = -1;
      BindPageLocaliseItems();
    }
  }
}