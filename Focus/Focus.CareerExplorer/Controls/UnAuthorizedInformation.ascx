﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UnAuthorizedInformation.ascx.cs"
	Inherits="Focus.CareerExplorer.Controls.UnAuthorizedInformation" %>
<act:ModalPopupExtender ID="ConfirmationModal" runat="server" BehaviorID="ConfirmationModal"
	TargetControlID="ConfirmationDummyTarget" PopupControlID="ConfirmationPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:HiddenField ID="ConfirmationDummyTarget" runat="server" />
<div id="ConfirmationPanel" tabindex="-1" class="modal" style="display:none; width:540px;">
	<div class="lightbox FocusCareer" id="ConfirmationLightBox">
		<div class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" alt="Close" width="20" height="20" onclick="$find('ConfirmationModal').hide();return false;"  class="modalCloseIcon" />
			<focus:LocalisedLabel runat="server" ID="UnAuthorisedInformationLabel" LocalisationKey="UnAuthorizedInformation.Label" DefaultText="Sorry..." CssClass="modalTitle"/>
		</div>
		<focus:LocalisedLabel runat="server" ID="NotificationTextLabel" LocalisationKey="Notification.Text" DefaultText="This feature is not available to a guest user. Please consider signing in or creating an account." CssClass="modalMessage"/>
		<div class="modalButtons modalButtonsLeft">
			<asp:Button ID="SigninButton" runat="server" class="buttonLevel2" OnClick="SigninButton_Click" />
			<asp:Button ID="CreateAccountButton" runat="server" class="buttonLevel2" OnClick="CreateAccountButton_Click" />
			<asp:Button ID="WithoutRegisterButton" runat="server" class="buttonLevel3" OnClientClick="javascript:$find('ConfirmationModal').hide(); return false;" />
		</div>
	</div>
</div>
