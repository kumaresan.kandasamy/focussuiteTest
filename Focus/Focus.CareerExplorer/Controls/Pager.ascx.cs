﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;

#endregion

namespace Focus.CareerExplorer.Controls
{
	public partial class Pager : UserControlBase
	{
		#region Private Properties

		private const string _CssClass = "CssClass";

		#endregion

		#region Public Properties

		public string PagedControlID
		{
			get { return MainDataPager.PagedControlID; }
			set { MainDataPager.PagedControlID = value; }
		}

		public int PageSize
		{
			get { return MainDataPager.PageSize; }
			set { MainDataPager.PageSize = value; }
		}

		public bool HidePageSizer { get; set; }

		public string CssClass
		{
			get { return (ViewState[_CssClass] == null) ? "pagerContainer" : (string)ViewState[_CssClass]; }
			set { ViewState[_CssClass] = value; }
		}
		public int StartRowIndex
		{
			get { return MainDataPager.StartRowIndex; }
		}
		public int TotalRowCount
		{
			get { return MainDataPager.TotalRowCount; }
		}

		#endregion
    
		public delegate void PageSize_Change(object sender, EventArgs eventArgs);
		public event PageSize_Change PagesizeChange;

		#region Methods

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				pnlPagerContainer.CssClass = CssClass;
				pnlPagerSizer.Visible = !HidePageSizer;
			}
		}

    /// <summary>
    /// Handles the SelectedIndexChanged event of the PageSizer control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void PageSizer_SelectedIndexChanged(object sender, EventArgs e)
		{
			var ddlPageSizer = (DropDownList)sender;
			var pageSize = Convert.ToInt32(ddlPageSizer.SelectedValue);
			MainDataPager.SetPageProperties(0, pageSize, true);
			if (PagesizeChange != null)
				PagesizeChange(sender, e);
		}

    /// <summary>
    /// Handles the Load event of the MainDataPager control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void MainDataPager_Load(object sender, EventArgs e)
		{
		}

    /// <summary>
    /// Returns to page.
    /// </summary>
    /// <param name="pageno">The pageno.</param>
    /// <param name="pagesize">The pagesize.</param>
		public void ReturnToPage(int pageno = 0, int pagesize = 0)
		{
			pageno = pageno * pagesize;
			MainDataPager.SetPageProperties(pageno, pagesize, true);
			PageSizer.SelectedValue = pagesize.ToString();
		}
		#endregion
	}
}
