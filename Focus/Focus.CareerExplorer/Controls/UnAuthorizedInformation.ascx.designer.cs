﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.CareerExplorer.Controls {
    
    
    public partial class UnAuthorizedInformation {
        
        /// <summary>
        /// ConfirmationModal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender ConfirmationModal;
        
        /// <summary>
        /// ConfirmationDummyTarget control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ConfirmationDummyTarget;
        
        /// <summary>
        /// UnAuthorisedInformationLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel UnAuthorisedInformationLabel;
        
        /// <summary>
        /// NotificationTextLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel NotificationTextLabel;
        
        /// <summary>
        /// SigninButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SigninButton;
        
        /// <summary>
        /// CreateAccountButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button CreateAccountButton;
        
        /// <summary>
        /// WithoutRegisterButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button WithoutRegisterButton;
    }
}
