﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Focus.Core.Views;

#endregion

namespace Focus.CareerExplorer.Controls
{
  public partial class DeleteResumeModal : UserControlBase
  {
    public delegate void DeleteCommandHandler(object sender, CommandEventArgs eventArgs);
    public event DeleteCommandHandler DeleteCommand;

    /// <summary>
    /// Event for when the page loads
    /// </summary>
    /// <param name="sender">Page raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      // Ensure the behaviour id is unique
      if (ModalPopup != null)
        ModalPopup.BehaviorID = string.Concat(ModalPopup.ClientID, "Behaviour");
    }

    /// <summary>
    /// Shows the modal
    /// </summary>
    public void Show(long resumeId)
    {
      DeleteButton.CommandArgument = resumeId.ToString(CultureInfo.InvariantCulture);
      DeleteButton.Visible = true;

      CompletedResumesPanel.Visible = false;

      ModalPopup.CancelControlID = CloseButton.ClientID;

      var resumeInfo = ServiceClientLocator.ResumeClient(App).GetAllResumeInfo();
      var thisResume = resumeInfo.FirstOrDefault(resume => resume.ResumeId == resumeId);
      var selectNewResume = false;
      if (thisResume != null && thisResume.IsDefault.GetValueOrDefault())
        selectNewResume = ServiceClientLocator.CandidateClient(App).GetUiClaimantStatus(App.User.PersonId.GetValueOrDefault(0));

      if (selectNewResume)
      {
        var otherCompletedResumes = resumeInfo.Where(resume => resume.ResumeId != resumeId && resume.CompletionStatus == ResumeCompletionStatuses.Completed)
                                              .ToList();
        if (otherCompletedResumes.Any())
        {
          DeleteResumeModalTitle.Text = HtmlLocalise("ReplaceDefaultResume.Header", "Replace default resume");
          DeleteResumeModalMessage.Text = HtmlLocalise("ReplaceDefaultResume.Label", "As an existing UI claimant, in order to delete your existing default resume, you must replace it with an alternative completed resume. Please select from your alternative resumes below to complete this action");

          RadioButtons.DataSource = otherCompletedResumes;
          RadioButtons.DataValueField = "ResumeId";
          RadioButtons.DataTextField = "ResumeName";
          RadioButtons.DataBind();

          CompletedResumesPanel.Visible = true;

          ResumeRequired.ErrorMessage = CodeLocalise("ResumeRequired.ErrorMessage", "Please select a resume");

          if (otherCompletedResumes.Count == 1)
            RadioButtons.SelectedIndex = 0;

          DeleteButton.Text = HtmlLocalise("Global.Delete", "Save");
          CloseButton.Text = HtmlLocalise("Global.Cancel", "Cancel");
        }
        else
        {
          DeleteResumeModalTitle.Text = HtmlLocalise("UnableToDelete.Header", "Unable to delete");
          DeleteResumeModalMessage.Text = resumeInfo.Count == 1
                                            ? HtmlLocalise("UnableToDeleteMore.Label", "As an existing UI claimant, you must have a searchable resume available to employers")
                                            : DeleteResumeModalMessage.Text = HtmlLocalise("UnableToDelete.Label", "As an existing UI claimant, you must have a completed searchable resume available to employers. You have existing incomplete resumes that you can complete in order to replace your existing default resume");

          DeleteButton.Visible = false;
          CloseButton.Visible = true;

          CloseButton.Text = HtmlLocalise("Global.OK", "OK");
        }
      }
      else
      {
        DeleteResumeModalTitle.Text = HtmlLocalise("Global.AreYouSure", "Are you sure?");
        DeleteResumeModalMessage.Text = HtmlLocalise("DeleteResume.Label", "Are you sure you want to delete this resume? Deleting this resume will erase all work history and education information. This may result in not meeting #FOCUSCAREER#'s work registration requirement and have implications for your job matching abilities");

        DeleteButton.Text = HtmlLocalise("Global.Delete", "Delete");
        CloseButton.Text = HtmlLocalise("Global.Cancel", "Cancel");
      }

      ModalPopup.Show();
    }

    /// <summary>
    /// Fires when the "Save" button is clicked to delete the resume
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void DeleteButton_OnCommand(object sender, CommandEventArgs e)
    {
      if (e.CommandName == "DELETE")
      {
        var resumeId = Convert.ToInt64(e.CommandArgument);
        var newDefaultId = RadioButtons.Visible 
          ? Convert.ToInt64(RadioButtons.SelectedValue)
          : (long?)null;

        if (resumeId > 0 && ServiceClientLocator.ResumeClient(App).DeleteResume(resumeId, newDefaultId))
        {
					if (App.UserData.HasDefaultResume && App.UserData.DefaultResumeId == resumeId)
            Utilities.UpdateUserContextFromDefaultResume();

          OnDeleteCommand(new CommandEventArgs("DELETED", resumeId));
        }
        else
        {
          OnDeleteCommand(new CommandEventArgs("NOTDELETED", resumeId));
        }
      }

      ModalPopup.Hide();
    }

    /// <summary>
    /// Raises the <see cref="DeleteCommand"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected virtual void OnDeleteCommand(CommandEventArgs eventArgs)
    {
      if (DeleteCommand != null)
        DeleteCommand(this, eventArgs);
    }
  }
}