﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Common.Models;
using Focus.Core;

#endregion


namespace Focus.CareerExplorer.Code
{
  public class ExplorerModalControl : UserControlBase
  {
    public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
    public event ItemClickedHandler ItemClicked;

    public delegate void SendEmailClickedHandler(object sender, LightboxEventArgs eventArgs);
    public event SendEmailClickedHandler SendEmailClicked;

    public delegate void BookmarkClickedHandler(object sender, LightboxEventArgs eventArgs);
    public event BookmarkClickedHandler BookmarkClicked;

    public delegate void PrintClickedHandler(object sender, LightboxEventArgs eventArgs);
    public event PrintClickedHandler PrintClicked;

    /// <summary>
    /// Raises the <see cref="ItemClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected virtual void OnItemClicked(CommandEventArgs eventArgs)
    {
      if (ItemClicked != null)
        ItemClicked(this, eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="BookmarkClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
    protected virtual void OnBookmarkClicked(LightboxEventArgs eventArgs)
    {
      if (BookmarkClicked != null)
        BookmarkClicked(this, eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="SendEmailClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
    protected virtual void OnSendEmailClicked(LightboxEventArgs eventArgs)
    {
      if (SendEmailClicked != null)
        SendEmailClicked(this, eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="PrintClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
    protected virtual void OnPrintClicked(LightboxEventArgs eventArgs)
    {
      if (PrintClicked != null)
        PrintClicked(this, eventArgs);
    }

    /// <summary>
    /// Opens the print (or pdf download) window.
    /// </summary>
    /// <param name="reportType">Type of the report.</param>
    /// <param name="printFormat">The print format.</param>
    protected void OpenPrintWindow(ReportTypes reportType, ModalPrintFormat printFormat)
    {
      var url = UrlBuilder.Print(reportType, printFormat);
      var js = String.Format("window.open('{0}','print','height=600,width=900,scrollbars=1');", url);

      Page.ClientScript.RegisterStartupScript(GetType(), "OpenPrint", js, true);
    }
  }
}