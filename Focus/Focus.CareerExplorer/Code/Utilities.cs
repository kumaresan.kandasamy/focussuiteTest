﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Xsl;

using ExpertPdf.HtmlToPdf;

using Focus.Common.Extensions;
using Focus.Common.Localisation;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models.Career;
using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.Code
{
  public class Utilities
  {
	  private static IApp _app;

    static Utilities()
    {
      if (_app.IsNull())
        _app = new HttpApp();
    }

		public Utilities(IApp app = null)
		{
			_app = app ?? new HttpApp();
		}

    /// <summary>
    /// Determines whether [is object modified] [the specified old object].
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="oldObject">The old object.</param>
    /// <param name="newObject">The new object.</param>
    /// <returns>
    ///   <c>true</c> if [is object modified] [the specified old object]; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsObjectModified<T>(T oldObject, T newObject)
    {
      if (oldObject.IsNull() && newObject.IsNull())
        return false;

      if (oldObject.IsNull() && newObject.IsNotNull())
        return true;

      if (oldObject.IsNotNull() && newObject.IsNull())
        return true;

      if (oldObject.Equals(newObject))
        return false;

      return oldObject.GetMD5HashCode() != newObject.GetMD5HashCode();
    }

    #region Apply Resume Templates

    /// <summary>
    /// Applies the template.
    /// </summary>
    /// <param name="resume">The resume.</param>
    /// <param name="xslPath">The XSL path.</param>
    /// <param name="app">The application.</param>
    /// <param name="resumeOrder">The resume order.</param>
    /// <param name="isHideResumeInfo">if set to <c>true</c> [is hide resume info].</param>
    /// <param name="isDownload">Defines whether the resulting output is being used for a PDF or RTF export</param>
    /// <returns></returns>
    public static string ApplyTemplate(string resume, string xslPath, IApp app, string resumeOrder = "experience", bool isHideResumeInfo = false, bool isDownload = false)
    {
      var htmlResume = "";

      try
      {        
        string resdoc_CRLF;
        var xDoc = new XmlDocument{PreserveWhitespace = true};

        xDoc.LoadXml(resume);

        // After updating the experience we are getting start and end date in this format: 01-2010
        // to follow the consistancy through the resume we have replaced "-" to "/"
        foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//daterange/start"))
        {
            if (dateRangeNode.InnerText.Contains("-"))
            {
                dateRangeNode.InnerText = dateRangeNode.InnerText.Replace('-', '/');
                dateRangeNode.InnerText = dateRangeNode.InnerText.Remove(3, 3);
            }
        }

        foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//daterange/end"))
        {
            if (dateRangeNode.InnerText.Contains("-"))
            {
                dateRangeNode.InnerText = dateRangeNode.InnerText.Replace('-', '/');
                dateRangeNode.InnerText = dateRangeNode.InnerText.Remove(3, 3);
            }
        }

        var model = OldApp_RefactorIfFound.GetSessionValue<ResumeModel>("Career:Resume");
				var hideResumeInfo = model.Special.HideInfo;

        //Checkin comments : Bug Fix: Blank popup is shown in job description page if user click on delete resume link. Update: Check is placed if Hide resume section model is null. Bug Fix: In Career common path first node is displaced if job title is more than 3 lines of box.
				if (isHideResumeInfo && OldApp_RefactorIfFound.User.IsNotNull() && OldApp_RefactorIfFound.User.UserId.IsNotNull() && model.IsNotNull() && hideResumeInfo.IsNotNull())
				{
					#region Hide - All Dates, Education dates, Work history dates

          if (hideResumeInfo.HideDateRange)
          {
            foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//daterange"))
              dateRangeNode.ParentNode.RemoveChild(dateRangeNode);

            foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//completiondate"))
              //dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
                dateRangeNode.InnerText = " ";   //FVN-6626 This is the fix for the undesirable unfinished degree message shown while hiding education dates - AIP

            foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//completion_date"))
              dateRangeNode.ParentNode.RemoveChild(dateRangeNode);


            foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//vet_start_date"))
              dateRangeNode.ParentNode.RemoveChild(dateRangeNode);

            foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//vet_end_date"))
              dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
          }
          else if (hideResumeInfo.HideWorkDates || hideResumeInfo.HideEducationDates)
          {
            if (hideResumeInfo.HideWorkDates)
            {
              foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//daterange"))
                dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
            }

            if (hideResumeInfo.HideEducationDates)
            {
              foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//completiondate"))
                 //dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
                dateRangeNode.InnerText = " " ;   //FVN-6626  This is the fix for the undesirable unfinished degree message shown while hiding education dates -- AIP
                
            }
					}

					#endregion

					#region Hide - Military service dates.

					if (hideResumeInfo.HideMilitaryServiceDates)
          {
            foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//statements/personal/veteran//vet_start_date"))
              dateRangeNode.ParentNode.RemoveChild(dateRangeNode);

            foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//statements/personal/veteran//vet_end_date"))
              dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
          }

					#endregion

					#region Hide - Contact

					if (hideResumeInfo.HideContact)
          {
            if (hideResumeInfo.HideName)
            {
              foreach (XmlNode contactNode in xDoc.SelectNodes("//resume/contact"))
                contactNode.ParentNode.RemoveChild(contactNode);
            }
            else
            {
              foreach (XmlNode address in xDoc.SelectNodes("//resume/contact/address"))
                address.ParentNode.RemoveChild(address);

              XmlNode contact = xDoc.SelectSingleNode("//resume/contact");
              if (contact != null)
              {
                if (contact.SelectNodes("phone").Count > 0)
                {
                  XmlNodeList phones = contact.SelectNodes("phone");
                  for (int i = 0; i < phones.Count; i++)
                    contact.RemoveChild(phones.Item(i));

                  contact.InnerXml = contact.InnerXml.Replace("home:", "").Replace("cell:", "").Replace("work:", "").Replace("fax:", "").Replace("NonUS:", "");
                }
              }

              XmlNode email = xDoc.SelectSingleNode("//resume/contact/email");
              if (email != null)
                email.ParentNode.RemoveChild(email);
            }
          }

					#endregion

					#region Hide - Name

					if (hideResumeInfo.HideName && !hideResumeInfo.HideContact)
          {
            foreach (XmlNode nameNode in xDoc.SelectNodes("//resume/contact/name"))
              nameNode.ParentNode.RemoveChild(nameNode);
          }

					#endregion

					#region Hide individual contact details if the contact section is visible

          if (!hideResumeInfo.HideContact)
          {
            XmlNode contact = xDoc.SelectSingleNode("//resume/contact");

						#region Hide - Email address

						if (hideResumeInfo.HideEmail)
            {
              XmlNode email = xDoc.SelectSingleNode("//resume/contact/email");
              if (email != null)
                email.ParentNode.RemoveChild(email);
            }

						#endregion

						#region Hide - Home Phone Number

            if (hideResumeInfo.HideHomePhoneNumber)
            {
              XmlNodeList homePhone = xDoc.SelectNodes("//resume/contact/phone[@type='home']");
              foreach (XmlNode hPhome in homePhone)
                hPhome.ParentNode.RemoveChild(hPhome);

              contact.InnerXml = contact.InnerXml.Replace("home:", "");
            }

						#endregion

						#region Hide - Work Phone Number

            if (hideResumeInfo.HideWorkPhoneNumber)
            {
              XmlNodeList workPhone = xDoc.SelectNodes("//resume/contact/phone[@type='work']");
              foreach (XmlNode wPhome in workPhone)
                wPhome.ParentNode.RemoveChild(wPhome);

              contact.InnerXml = contact.InnerXml.Replace("work:", "");
            }

						#endregion

						#region Hide - Cell Phone Number

						if (hideResumeInfo.HideCellPhoneNumber)
            {
              XmlNodeList cellPhone = xDoc.SelectNodes("//resume/contact/phone[@type='cell']");
              foreach (XmlNode cPhome in cellPhone)
                cPhome.ParentNode.RemoveChild(cPhome);

              contact.InnerXml = contact.InnerXml.Replace("cell:", "");
            }

						#endregion

						#region Hide - Fax Phone Number

						if (hideResumeInfo.HideFaxPhoneNumber)
            {
              XmlNodeList faxPhone = xDoc.SelectNodes("//resume/contact/phone[@type='fax']");
              foreach (XmlNode fPhome in faxPhone)
                fPhome.ParentNode.RemoveChild(fPhome);

              contact.InnerXml = contact.InnerXml.Replace("fax:", "");
            }

						#endregion

						#region Hide - NonUS Phone Number

            if (hideResumeInfo.HideNonUSPhoneNumber)
            {
              XmlNodeList nonUSPhone = xDoc.SelectNodes("//resume/contact/phone[@type='nonus']");
              foreach (XmlNode nUSPhome in nonUSPhone)
                nUSPhome.ParentNode.RemoveChild(nUSPhome);

              contact.InnerXml = contact.InnerXml.Replace("NonUS:", "");
            }

						#endregion
          }

					#endregion

					#region Hide - Affiliations

					if (hideResumeInfo.HideAffiliations)
          {
            foreach (XmlNode affiliationsNode in xDoc.SelectNodes("//professional/affiliations"))
              affiliationsNode.ParentNode.RemoveChild(affiliationsNode);
          }

					#endregion

					#region Hide - Certification and Professional License

					if (hideResumeInfo.HideCertificationsAndProfessionalLicenses)
          {
            foreach (XmlNode certificationsNode in xDoc.SelectNodes("//skills/courses/certifications/certification"))
              certificationsNode.ParentNode.RemoveChild(certificationsNode);
          }

					#endregion

					#region Hide - Driver's license

          if (!hideResumeInfo.UnHideDriverLicense || hideResumeInfo.HideDriverLicense)
          {
            foreach (XmlNode driversLicense in xDoc.SelectNodes("//personal/license/driver_class"))
              driversLicense.InnerText = "-1";
          }

					#endregion

					#region Hide - Honors

					if (hideResumeInfo.HideHonors)
          {
            foreach (XmlNode honorsNode in xDoc.SelectNodes("//special/honors"))
              honorsNode.ParentNode.RemoveChild(honorsNode);
          }

					#endregion

					#region Hide - Interests

          if (hideResumeInfo.HideInterests)
          {
            foreach (XmlNode interestsNode in xDoc.SelectNodes("//special/interests"))
              interestsNode.ParentNode.RemoveChild(interestsNode);
          }

					#endregion

					#region Hide - Internships

          if (hideResumeInfo.HideInternships)
          {
            foreach (XmlNode internshipsNode in xDoc.SelectNodes("//special/internships"))
              internshipsNode.ParentNode.RemoveChild(internshipsNode);
          }

					#endregion

					#region Hide - Languages

          if (hideResumeInfo.HideLanguages)
          {
            foreach (XmlNode languagesNode in xDoc.SelectNodes("//skills/languages/languages_profiency"))
              languagesNode.ParentNode.RemoveChild(languagesNode);

						foreach (XmlNode languagesNode in xDoc.SelectNodes("//skills/languages/language"))
							languagesNode.ParentNode.RemoveChild(languagesNode);
          }

					#endregion

					#region Hide - Military services

          if (hideResumeInfo.HideVeteran)
          {
            foreach (XmlNode veteranNode in xDoc.SelectNodes("//personal/veteran"))
              veteranNode.ParentNode.RemoveChild(veteranNode);
          }

					#endregion

					#region Hide - Objectives

          if (hideResumeInfo.HideObjective || app.Settings.HideObjectives)
          {
            foreach (XmlNode objectiveNode in xDoc.SelectNodes("//summary/objective"))
              objectiveNode.ParentNode.RemoveChild(objectiveNode);
          }

					#endregion

					#region Hide - Personal Information

          if (hideResumeInfo.HidePersonalInformation)
          {
            foreach (XmlNode personallNode in xDoc.SelectNodes("//special/personal"))
              personallNode.ParentNode.RemoveChild(personallNode);
          }

					#endregion

					#region Hide - Profession Development

          if (hideResumeInfo.HideProfessionalDevelopment)
          {
						foreach (XmlNode professionalNode in xDoc.SelectNodes("//professional/description"))
              professionalNode.ParentNode.RemoveChild(professionalNode);
          }

					#endregion

					#region Hide Publications

          if (hideResumeInfo.HidePublications)
          {
            foreach (XmlNode publicationsNode in xDoc.SelectNodes("//professional/publications"))
              publicationsNode.ParentNode.RemoveChild(publicationsNode);
          }

					#endregion

					#region Hide References

          if (hideResumeInfo.HideReferences)
          {
            foreach (XmlNode publicationsNode in xDoc.SelectNodes("//references"))
              publicationsNode.ParentNode.RemoveChild(publicationsNode);
          }

					#endregion

					#region Hide Skills

          if (hideResumeInfo.HideSkills)
          {
            foreach (XmlNode skillsNode in xDoc.SelectNodes("//skills/skills"))
              skillsNode.ParentNode.RemoveChild(skillsNode);
          }

					#endregion

					#region Hide Technical skills

          if (hideResumeInfo.HideTechnicalSkills)
          {
            foreach (XmlNode technicalSkillsNode in xDoc.SelectNodes("//special/technicalskills"))
              technicalSkillsNode.ParentNode.RemoveChild(technicalSkillsNode);
          }

					#endregion

					#region Hide Volunteer activities

          if (hideResumeInfo.HideVolunteerActivities)
          {
            foreach (XmlNode volunteerNode in xDoc.SelectNodes("//special/volunteeractivities"))
              volunteerNode.ParentNode.RemoveChild(volunteerNode);
          }

					#endregion

					#region Thesis/Major projects

					if (hideResumeInfo.HideThesisMajorProjects)
					{
						foreach (XmlNode thesisMajorProjectNode in xDoc.SelectNodes("//special/thesismajorprojects"))
							thesisMajorProjectNode.ParentNode.RemoveChild(thesisMajorProjectNode);
					}

					#endregion

          foreach (XmlNode job in xDoc.SelectNodes("//job"))
          {
            if (job.SelectSingleNode("jobid") != null && hideResumeInfo.HiddenJobs.Contains(job.SelectSingleNode("jobid").InnerText))
              job.ParentNode.RemoveChild(job);
          }

        }

				//TODO: this needs reviewing
				#region Lookup military IDs

      	var veteran = xDoc.SelectSingleNode("//personal/veteran");

				if (veteran != null)
				{
                    var vetflag_exist = xDoc.SelectSingleNode("//personal/veteran/vet_flag");
                    if(vetflag_exist != null)
                    {

					var vetFlag = Convert.ToBoolean(veteran.SelectSingleNode("vet_flag").InnerText.ToInt());

					if (vetFlag)
					{
					  var historyList = new List<XmlNode>();
					  var historyNodes = veteran.SelectNodes("history");
            if (historyNodes.IsNotNull())
              historyList.AddRange(historyNodes.Cast<XmlNode>());

					  if (!historyList.Any())
					    historyList.Add(veteran);

            foreach (var historyNode in historyList)
            {
              var serviceBranchKey = string.Empty;

              #region Branch of service

              var branchOfServiceElement = historyNode.SelectSingleNode("branch_of_service");
              if (branchOfServiceElement.IsNotNull())
              {
                var serviceBranch = branchOfServiceElement.InnerText;
                var serviceBranchName = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.MilitaryBranchesOfService)
                                                  .FirstOrDefault(x => x.ExternalId == serviceBranch.ToString(CultureInfo.InvariantCulture));

                if (serviceBranchName.IsNotNull())
                {
                  branchOfServiceElement.InnerText = serviceBranchName.Text;
                  serviceBranchKey = serviceBranchName.Key;
                }
                else
                {
                  branchOfServiceElement.InnerText = string.Empty;
                }
              }

              #endregion

              #region Rank

              if (serviceBranchKey.IsNotNullOrEmpty())
              {
                var rankElement = historyNode.SelectSingleNode("rank");
                if (rankElement.IsNotNull())
                {
                  var rankId = rankElement.InnerText;
                  var rankName = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.MilitaryRanks)
                                           .FirstOrDefault(x => x.ExternalId == rankId.ToString(CultureInfo.InvariantCulture) && x.ParentKey == serviceBranchKey);

                  rankElement.InnerText = rankName.IsNotNull() ? rankName.Text : string.Empty;
                }
              }

              #endregion 
            }
					}
                }
				}

				#endregion

				#region Languages

				if (xDoc.IsNotNull() && xDoc.DocumentElement.IsNotNull())
				{
					var languageProficiencyNodes = xDoc.DocumentElement.SelectNodes("resume/skills/languages/language/proficiency");
					if (languageProficiencyNodes.IsNotNull() && languageProficiencyNodes.Count > 0)
					{
						var proficiencyLookup = ServiceClientLocator.CoreClient(app).GetLookup(LookupTypes.LanguageProficiencies).ToDictionary(l => l.ExternalId, l => l.Text);
						foreach (XmlElement proficienyNode in languageProficiencyNodes)
						{
							if (!proficienyNode.HasChildNodes)
							{
								proficienyNode.AppendChild(xDoc.CreateTextNode(""));
							}

							var externalId = proficienyNode.FirstChild.Value;
							var proficienyText = proficiencyLookup.ContainsKey(externalId) ? proficiencyLookup[externalId] : "";
							if (proficienyText.Length > 0)
							{
								var checkPrefix = string.Concat(externalId, " - ");
								if (proficienyText.StartsWith(checkPrefix))
								{
									proficienyText = proficienyText.Substring(checkPrefix.Length);
								}
								proficienyText = string.Concat(" at ", proficienyText.ToLower());
							}
							proficienyNode.FirstChild.Value = proficienyText;
						}
					}
				}

				#endregion

				if (xDoc.SelectSingleNode("//ResDoc") != null)
          resdoc_CRLF = xDoc.SelectSingleNode("//ResDoc").OuterXml.Trim().Replace("\r\n", "\n").Replace("\n", "FC_CRLF").Replace("FC_CRLF*	", "FC_CRLF* ");
        else if (xDoc.SelectSingleNode("//resume") != null)
          resdoc_CRLF = xDoc.SelectSingleNode("//resume").OuterXml.Trim().Replace("\r\n", "\n").Replace("\n", "FC_CRLF").Replace("FC_CRLF*	", "FC_CRLF* ");
        else
          resdoc_CRLF = resume;

        resdoc_CRLF = resdoc_CRLF.Replace("&amp;#", "&#");

        var obj = new XslCompiledTransform();
        // The XMLComiledTrandsform should be enabled with script execution to work with HRXML 
        // which uses Java script to get the positiontype from a tagged resume
        var xslSecurity = new XsltSettings{EnableScript = true};

        obj.Load(xslPath, xslSecurity, new XmlUrlResolver());

        var strWriter = new StringWriterWithEncoding(System.Text.Encoding.GetEncoding("ISO-8859-1"));
        var xmlResume = new StringReader(resdoc_CRLF);
				var reader = XmlReader.Create(xmlResume);

        //To change style sheet font size dynamically
        var xslArg = new XsltArgumentList();
        xslArg.AddParam("emphasizeinfo", "", resumeOrder);
        xslArg.AddParam("fontsize13", "", "13px");
        xslArg.AddParam("fontsize14", "", "14px");
        xslArg.AddParam("fontsize16", "", "16px");

        
        xslArg.AddParam("TechnicalSkills", "", Localiser.Instance().Localise("ResumeSectionType.TechnicalSkills.NoEdit", "Technical Skills"));
        xslArg.AddParam("VolunteerActivities", "", Localiser.Instance().Localise("ResumeSectionType.VolunteerActivities.NoEdit", "Volunteer Activities"));
        xslArg.AddParam("Affiliations", "", Localiser.Instance().Localise("ResumeSectionType.Affiliations.NoEdit", "Affiliations"));
        xslArg.AddParam("Honors", "", Localiser.Instance().Localise("ResumeSectionType.Honors.NoEdit", "Honors"));
        xslArg.AddParam("ProfessionalDevelopment", "", Localiser.Instance().Localise("ResumeSectionType.ProfessionalDevelopment.NoEdit", "Professional Development"));
        xslArg.AddParam("Interests", "", Localiser.Instance().Localise("ResumeSectionType.Interests.NoEdit", "Interests"));
        xslArg.AddParam("References", "", Localiser.Instance().Localise("ResumeSectionType.References.NoEdit", "References"));
        xslArg.AddParam("Internships", "", Localiser.Instance().Localise("ResumeSectionType.Internships.NoEdit", "Internships"));
        xslArg.AddParam("Objectives", "", Localiser.Instance().Localise("ResumeSectionType.Objective.NoEdit", "Internships"));
        xslArg.AddParam("PersonalInformation", "", Localiser.Instance().Localise("ResumeSectionType.Personals.NoEdit", "Personal Information"));
        xslArg.AddParam("Publications", "", Localiser.Instance().Localise("ResumeSectionType.Publications.NoEdit", "Publications"));

				 //Nasty hack for FVN-1840
				if (!isDownload && _app.Settings.BrandIdentifier == "RightManagement")
				{
				  // reduce width of resume on screen to be more consistent with PDF/RTF output
				  // the amount we need to reduce it by varies depending on the template. which is nice.
				  var templateName = xslPath.Substring(xslPath.LastIndexOf("\\", StringComparison.InvariantCulture) + 1);
				  string layoutWidth;
				  switch (templateName)
				  {
				    case "ChronologicalFormat.xsl":
				      layoutWidth = "75%";
				      break;
				    case "FocusCareerFormat.xsl":
				      layoutWidth = "70%";
				      break;
				    case "ClassicFormat.xsl":
				      layoutWidth = "70%";
				      break;
				    case "ContemporaryFormat.xsl":
				      layoutWidth = "75%";
				      break;
				    case "ModernFormat.xsl":
				      layoutWidth = "70%";
				      break;
				    case "ProfessionalFormat.xsl":
				      layoutWidth = "70%";
				      break;
				    default:
				      layoutWidth = "100%";
				      break;
				  }
				  xslArg.AddParam("layoutWidth", "", layoutWidth);
				}

        //Another hack for FVN-1941 & FVN-1883
        if (isDownload)
        {
          xslArg.AddParam("crlf", "", "n");
          xslArg.AddParam("listitemindent", "", "-20px !important");
        }

        var util = new Utilities();
        xslArg.AddExtensionObject("urn:util", util);

        obj.Transform(reader, xslArg, strWriter);
        htmlResume = strWriter.ToString().Replace("FC_CRLF", "<br/>");
      }
      catch (Exception)
      { }

      return htmlResume;
    }

    #endregion

    #region Apply Style Sheet

    public static string ApplyStyleSheet(string xslPath, string xmlData)
    {
      try
      {
        var obj = new XslCompiledTransform();

        if (!String.IsNullOrEmpty(xmlData))
        {
          var FC_CRLF = xmlData.Replace("\n", "FC_CRLF");
          var xslSecruity = new XsltSettings { EnableScript = true };
          obj.Load(HttpContext.Current.Server.MapPath("~/Assets/Xslts/" + OldApp_RefactorIfFound.Settings.ResetPasswordFormat), xslSecruity, new XmlUrlResolver());
          var strWriter = new StringWriterWithEncoding(System.Text.Encoding.GetEncoding("ISO-8859-1"));
          var xmlResume = new StringReader(FC_CRLF);
          var reader = XmlReader.Create(xmlResume);
          obj.Transform(reader, null, strWriter);
          return strWriter.ToString().Replace("FC_CRLF", "<br/>");
        }

        return null;
      }
      catch (Exception)
      {
        return null;
      }
    }

    #endregion

    /// <summary>
    /// Fetches the XML resume.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <param name="convertSkillsToTitleCase">if set to <c>true</c> [convert skills to title case].</param>
    /// <returns></returns>
    public static string FetchXMLResume(ResumeModel model = null, bool convertSkillsToTitleCase = true)
    {
      var xResume = "";
      try
      {
        if (OldApp_RefactorIfFound.User.IsNotNull() && OldApp_RefactorIfFound.User.UserId.IsNotNull())
        {

					if (model.IsNull())
						model = OldApp_RefactorIfFound.GetSessionValue<ResumeModel>("Career:Resume");

					if (model.IsNotNull()
						&& model.ResumeContent.IsNotNull()
						&& model.ResumeContent.ExperienceInfo.IsNotNull()
						&& model.ResumeContent.ExperienceInfo.Jobs.IsNotNull()
						&& model.ResumeContent.ExperienceInfo.Jobs.Count > 1)
          {
						model.ResumeContent.ExperienceInfo.Jobs = model.ResumeContent.ExperienceInfo.Jobs.OrderByDescending(j => j.JobEndDate ?? DateTime.MaxValue).ThenByDescending(j => j.JobStartDate ?? DateTime.MinValue).ToList();
						OldApp_RefactorIfFound.SetSessionValue("Career:Resume", model);
          }

          List<string> originalSkills = null;

          if (convertSkillsToTitleCase)
          {
            if (model != null && model.ResumeContent.IsNotNull() && model.ResumeContent.Skills.IsNotNull() && model.ResumeContent.Skills.Skills.IsNotNull())
            {
              originalSkills = model.ResumeContent.Skills.Skills;
              model.ResumeContent.Skills.Skills = model.ResumeContent.Skills.Skills.Select(s => s.ToTitleCase()).ToList();
            }
          }

          xResume = ServiceClientLocator.ResumeClient(_app).GetTaggedResume(model);

          if (originalSkills.IsNotNull())
            model.ResumeContent.Skills.Skills = originalSkills;
        }
      }
      catch (ApplicationException ex)
      {
        throw ex;
      }

      return xResume;
    }

    #region Download RTF Resume

    /// <summary>
    /// Export2s the RTF.
    /// </summary>
    /// <param name="Resume">The resume.</param>
    /// <param name="rtfData">The RTF data.</param>
    /// <param name="NameOnResume">The name on resume.</param>
    /// <returns></returns>
    public static int Export2RTF(String Resume, out string rtfData, string NameOnResume)
    {
      rtfData = null;
      var cnvObj = new SautinSoft.HtmlToRtf();

      try
      {
        if (cnvObj != null)
        {
					cnvObj.Serial = "10201787834";
          cnvObj.OutputFormat = SautinSoft.HtmlToRtf.eOutputFormat.Rtf;
          cnvObj.PageStyle.PageSize.A4();
          cnvObj.PageAlignment = SautinSoft.HtmlToRtf.eAlign.Justify;
          cnvObj.PreserveFontFace = true;
          cnvObj.PreserveFontSize = cnvObj.PreserveFontFace = cnvObj.PreserveFontColor = true;
          cnvObj.PreserveHR = cnvObj.PreserveTables = cnvObj.PreserveAlignment = true;
          cnvObj.PreservePageBreaks = false;

					//Removed name from the header as per instruction from FVN-2949
					//if (!String.IsNullOrEmpty(NameOnResume))
					//  cnvObj.PageStyle.PageHeader.Html(NameOnResume);

          cnvObj.PageStyle.PageNumbers.Appearance = SautinSoft.HtmlToRtf.ePageNumbers.PageNumDisable;
          
          cnvObj.PageStyle.PageOrientation.Portrait();
					cnvObj.PageStyle.PageMarginTop.Mm(10);
					cnvObj.PageStyle.PageMarginLeft.Mm(10);
          cnvObj.PageStyle.PageMarginRight.Mm(10);
          cnvObj.PageStyle.PageMarginBottom.Mm(10);

          cnvObj.PreserveAlignment = true;

          var htmlString = Resume;
          var rtfString = cnvObj.ConvertString(htmlString);
          rtfData = rtfString;
        }
      }
      catch (System.Threading.ThreadAbortException)
      {
        // do nothing
      }
      catch (Exception)
      {
        if (cnvObj != null)
          cnvObj = null;
      }

      return 0;
    }

    /// <summary>
    /// Export2s the RTF.
    /// </summary>
    /// <param name="inResume">The in resume.</param>
    /// <param name="resumeName">Name of the resume.</param>
    public static void Export2RTF(String inResume, String resumeName)
    {
        try
      {
        string rtfData;
        var nameOnResume = "";
        var userName = "";

        if (OldApp_RefactorIfFound.User.IsNotNull() && OldApp_RefactorIfFound.User.UserId.IsNotNull())
        {
          userName = OldApp_RefactorIfFound.User.EmailAddress;
          nameOnResume = OldApp_RefactorIfFound.User.ScreenName;
        }

        Export2RTF(inResume, out rtfData, nameOnResume);


          if (rtfData == null) return;
          var response = System.Web.HttpContext.Current.Response;
          response.Clear();
          response.AddHeader("Content-Type", "binary/octet-stream");
          response.AddHeader("Content-Disposition",
              "attachment; filename=\"" + (resumeName.IsNotNullOrEmpty() ? resumeName : userName) + ".rtf\"" + "; size=" + rtfData.Length.ToString());
          response.Flush();
          response.Write(rtfData);
          response.Flush();
          response.End();
      }
      catch (System.Threading.ThreadAbortException)
      {
        // do nothing
      }
      catch (Exception)
      {
          // ignored
      }
    }

    #endregion

    #region Download PDF Resume

    /// <summary>
    /// Export2s the PDF.
    /// </summary>
    /// <param name="resume">The resume.</param>
    /// <param name="pdfBytes">The PDF bytes.</param>
    /// <param name="nameOnResume">The name on resume.</param>
    /// <returns></returns>
    public static int Export2PDF(String resume, out byte[] pdfBytes, string nameOnResume, string urlBase = "")
    {
      pdfBytes = null;

      try
      {
        var pdfConverter = new PdfConverter();
        //pdfConverter.LicenseKey = "ZU5XRV1FUEVdS1VFVlRLVFdLXFxcXA==";
          pdfConverter.LicenseKey = "1f7n9e314+H15Of75fXm5Pvk5/vs7Ozs";
        pdfConverter.RenderingEngine = 0;
        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression;

        pdfConverter.PdfDocumentOptions.ShowHeader = true;
        pdfConverter.PdfDocumentOptions.ShowFooter = true;
        pdfConverter.PdfDocumentOptions.LeftMargin = 10;
        pdfConverter.PdfDocumentOptions.RightMargin = 10;
        pdfConverter.PdfDocumentOptions.TopMargin = 10;
        pdfConverter.PdfDocumentOptions.BottomMargin = 10;
        pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;
        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
        pdfConverter.PdfDocumentOptions.SinglePage = false;
        pdfConverter.AvoidTextBreak = true;
		
        
        //pdfConverter.PageWidth = 500;
        //pdfConverter.PageHeight = 0;
        pdfConverter.PdfHeaderOptions.HeaderText = !String.IsNullOrEmpty(nameOnResume) ? nameOnResume : "";

        pdfConverter.PdfHeaderOptions.HeaderTextAlign = HorizontalTextAlign.Left; ;
        pdfConverter.PdfHeaderOptions.HeaderTextFontSize = 8;
        pdfConverter.PdfHeaderOptions.ShowOnFirstPage = false;
        pdfConverter.PdfHeaderOptions.DrawHeaderLine = false;
        pdfConverter.PdfHeaderOptions.HeaderSubtitleText = "";

        pdfConverter.PdfFooterOptions.FooterText = "";
        pdfConverter.PdfFooterOptions.FooterTextFontSize = 8;
        pdfConverter.PdfFooterOptions.DrawFooterLine = true;
        pdfConverter.PdfFooterOptions.ShowPageNumber = true;
        pdfConverter.PdfFooterOptions.PageNumberTextFontSize = 8;
        
        pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(resume, urlBase);
        
      }
      catch (System.Threading.ThreadAbortException)
      {
        // do nothing
      }
      catch (Exception e)
      { }

      return 0;
    }

    /// <summary>
    /// Export2s the PDF.
    /// </summary>
    /// <param name="inResume">The in resume.</param>
    /// <param name="resumeName">Name of the resume.</param>
    public static void Export2PDF(String inResume, String resumeName)
    {
      System.Web.HttpResponse response = null;
      try
      {
        byte[] pdfData = null;
        string NameOnResume = "";
        string UserName = "";


        if (OldApp_RefactorIfFound.User.IsNotNull() && OldApp_RefactorIfFound.User.IsNotNull())
        {
          UserName = OldApp_RefactorIfFound.User.EmailAddress;
          NameOnResume = OldApp_RefactorIfFound.User.ScreenName;
        }

        Export2PDF(inResume, out pdfData, NameOnResume);
        if (pdfData != null)
        {
          response = System.Web.HttpContext.Current.Response;
          response.Clear();
          response.AddHeader("Content-Type", "binary/octet-stream");
          response.AddHeader("Content-Disposition",
            "attachment; filename=\"" + (resumeName.IsNotNullOrEmpty() ? resumeName : UserName) + ".pdf\"" + "; size=" + pdfData.Length.ToString());
          response.Flush();
          response.BinaryWrite(pdfData);
          response.Flush();
          response.End();
        }
      }
      catch (System.Threading.ThreadAbortException)
      {
        // do nothing
      }
      catch (Exception)
      { }
    }

    #endregion

    #region Update ROnet & Onet

    /// <summary>
    /// Updates the onet R onet in resume.
    /// </summary>
    /// <param name="resume">The resume.</param>
    /// <returns></returns>
    public static bool UpdateOnetROnetInResume(ResumeModel resume)
    {
      var resumeUpdated = false;
      try
      {
        if (resume.IsNotNull()
          && resume.ResumeContent.IsNotNull()
          && resume.ResumeContent.ExperienceInfo.IsNotNull()
          && resume.ResumeContent.ExperienceInfo.Jobs.IsNotNullOrEmpty())
        {
          var resumeXml = ServiceClientLocator.ResumeClient(_app).GetTaggedResume(resume, true);
          if (resumeXml.IsNotNullOrEmpty())
          {
            var xmlDoc = new XmlDocument { PreserveWhitespace = true };
            xmlDoc.LoadXml(resumeXml);
            var onetAttr = xmlDoc.SelectSingleNode("//job[@pos=1]/@onet|//job[1]/@onet");
            if (onetAttr.IsNotNull())
            {
              var onet = onetAttr.InnerText;
              if (onet.IndexOf("|", StringComparison.Ordinal) > 0)
                onet = onet.SubstringBefore("|");

              if (onet.IsNotNullOrEmpty())
              {
                if (resume.ResumeMetaInfo.ResumeOnet != onet)
                {
                  resume.ResumeMetaInfo.ResumeOnet = onet;
                  resumeUpdated = true;
                }
                var ronet = ServiceClientLocator.ExplorerClient(_app).GetROnetFromResume(resumeXml);
                if ((resume.ResumeMetaInfo.ResumeROnet ?? "") != (ronet ?? ""))
                {
                  resume.ResumeMetaInfo.ResumeROnet = ronet;
                  resumeUpdated = true;
                }
              }
            }
          }
        }
      }
      catch { }

      return resumeUpdated;
    }

    #endregion
		
    #region Update UserContext

    /// <summary>
    /// Updates the user context user info.
    /// </summary>
    /// <param name="tabObject">The tab object.</param>
    /// <param name="completionStatus">The completion status.</param>
    /// <returns></returns>
    public static bool UpdateUserContextUserInfo(object tabObject, ResumeCompletionStatuses completionStatus)
    {
      bool updated = false;
      try
      {
        UserProfile oldProfile, newProfile;
        switch (completionStatus)
        {
          case ResumeCompletionStatuses.WorkHistory:
            {
              var tempResumeMetaInfo = (ResumeEnvelope)tabObject;
              OldApp_RefactorIfFound.UserData.DefaultResumeId = tempResumeMetaInfo.ResumeId;
	          }
            break;

          case ResumeCompletionStatuses.Contact:
            {
              var tempContact = (Contact)tabObject;
              oldProfile = OldApp_RefactorIfFound.UserData.Profile;
              newProfile = new UserProfile
              {
                EmailAddress = tempContact.EmailAddress,
                PostalAddress = tempContact.PostalAddress,
                PrimaryPhone = tempContact.PhoneNumber.IsNull() || tempContact.PhoneNumber.Count == 0 ? null : tempContact.PhoneNumber[0],
                UserGivenName = tempContact.SeekerName
              };

              if (oldProfile.IsNull())
                OldApp_RefactorIfFound.UserData.Profile = newProfile;
              else
              {
                oldProfile.EmailAddress = newProfile.EmailAddress;
                oldProfile.PostalAddress = newProfile.PostalAddress;
                oldProfile.PrimaryPhone = newProfile.PrimaryPhone;
                oldProfile.UserGivenName = newProfile.UserGivenName;
              }

              OldApp_RefactorIfFound.User.ScreenName = newProfile.UserGivenName.IsNotNull() ? newProfile.UserGivenName.FullName : null;
              _app.SetSessionValue("Career:ResumeUpload", true);
            }
            break;

          case ResumeCompletionStatuses.Profile:
            {
              var tempProfile = (UserProfile)tabObject;
              oldProfile = OldApp_RefactorIfFound.UserData.Profile;
              newProfile = new UserProfile
              {
                AlienExpires = tempProfile.AlienExpires,
                AlienId = tempProfile.AlienId,
                DisabilityCategoryIds = tempProfile.DisabilityCategoryIds,
                DisabilityStatus = tempProfile.DisabilityStatus,
                DOB = tempProfile.DOB,
                EthnicHeritage = tempProfile.EthnicHeritage,
                IsPermanentResident = tempProfile.IsPermanentResident,
                AuthorisedToWork = tempProfile.AuthorisedToWork,
                IsUSCitizen = tempProfile.IsUSCitizen,
                Sex = tempProfile.Sex,
                Veteran = tempProfile.Veteran
              };

              if (oldProfile.IsNull())
                OldApp_RefactorIfFound.UserData.Profile = newProfile;
              else
              {
                oldProfile.AlienExpires = newProfile.AlienExpires;
                oldProfile.AlienId = newProfile.AlienId;
                oldProfile.DisabilityCategoryIds = newProfile.DisabilityCategoryIds;
                oldProfile.DisabilityStatus = newProfile.DisabilityStatus;
                oldProfile.DOB = newProfile.DOB;
                oldProfile.EthnicHeritage = newProfile.EthnicHeritage;
                oldProfile.IsPermanentResident = newProfile.IsPermanentResident;
                oldProfile.AuthorisedToWork = newProfile.AuthorisedToWork;
                oldProfile.IsUSCitizen = newProfile.IsUSCitizen;
                oldProfile.Sex = newProfile.Sex;
                oldProfile.Veteran = newProfile.Veteran;
              }
            }
            break;
        }

				OldApp_RefactorIfFound.UserData.DefaultResumeCompletionStatus |= completionStatus;

        updated = true;
      }
      catch
      { }

      return updated;
    }

		#endregion

    #region Career Search

    /// <summary>
    /// Prepares the route to the job search depending on a command (resume, program of study, or both)
    /// </summary>
    /// <param name="commandName">The command for the job search</param>
    /// <param name="setLoggingCookie">Whether to set a cookie to pass to the search results to indicate a manual search</param>
    /// <returns>
    /// The route to which to redirect
    /// </returns>
    public static string RouteToJobSearch(string commandName, bool setLoggingCookie)
    {
      var redirectRoute = UrlBuilder.JobSearchResults();
      var defaultResume = ServiceClientLocator.ResumeClient(_app).GetDefaultResume();

      switch (commandName)
      {
        case "Resume":
          if (defaultResume.IsNotNull())
            SaveCriteriaAgainstResume(defaultResume, MatchingType.Resume, setLoggingCookie);
          else
            redirectRoute = UrlBuilder.YourResume();
          break;

        case "ProgramOfStudy":
          SaveCriteriaAgainstResume(defaultResume, MatchingType.ProgramOfStudy, setLoggingCookie);
          break;

        case "ResumeAndProgramOfStudy":
          SaveCriteriaAgainstResume(defaultResume, MatchingType.ResumeAndProgramOfStudy, setLoggingCookie);
          break;
      }

      return redirectRoute;
    }

    /// <summary>
    /// Sets up default search criteria if user has not specified any (and if App Settings are set accordingly)
    /// </summary>
    /// <param name="jobLocation">The job location criteria</param>
    public static void BuildDefaultSearchLocationCriteria(JobLocationCriteria jobLocation)
    {
      if (OldApp_RefactorIfFound.Settings.CareerSearchCentrePointType == SearchCentrePointOptions.Zipcode && OldApp_RefactorIfFound.Settings.CareerSearchRadius > 0 && OldApp_RefactorIfFound.Settings.CareerSearchZipcode.Length > 0)
      {
        var radius = new RadiusCriteria
        {
          Distance = OldApp_RefactorIfFound.Settings.CareerSearchRadius,
          DistanceUnits = DistanceUnits.Miles,
          PostalCode = OldApp_RefactorIfFound.Settings.CareerSearchZipcode
        };
        jobLocation.Radius = radius;
      }
      else if (OldApp_RefactorIfFound.Settings.CareerSearchCentrePointType == SearchCentrePointOptions.SelectedLocations && OldApp_RefactorIfFound.Settings.NearbyStateKeys.Any())
      {
        var lookups = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.States).Where(x => OldApp_RefactorIfFound.Settings.NearbyStateKeys.Contains(x.Key)).Select(x => x.ExternalId).ToList();

        if (lookups.IsNotNullOrEmpty())
          jobLocation.SearchMultipleStates = lookups;
      }
    }

    #endregion

    #region Title Case

    /// <summary>
    /// Ensures certain words remain lower case
    /// </summary>
    /// <param name="inText">The in text.</param>
    /// <returns></returns>
    public static string TitleCase(string inText)
    {
      string outText = inText;

      try
      {
        outText = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(inText).Trim().Replace("&Amp;", "&amp;").Replace("&Gt;", "&gt;").Replace("&Lt;", "&lt;").Replace("&Apos;", "&apos;").Replace("&Quot;", "&quot;").Replace("&AMP;", "&amp;");
        // Avoid capitalization to stop words
        // stop words - I, a, about, an, are, as, at, be, by, com, de, en, for, from, how, in, is, it, la, of, on, or, 
        //              that, the, this, to, was, what, when, where, who, will, with, und, www
        outText = outText.Replace(" A ", " a ").Replace(" An ", " an ").Replace(" At ", " at ").Replace(" And ", " and ").Replace(" Are ", " are ").Replace(" As ", " as ").Replace(" Be ", " be ");
        outText = outText.Replace(" By ", " by ").Replace(" Com ", " com ").Replace(" De ", " de ").Replace(" En ", " en ").Replace(" For ", " for ").Replace(" From ", " from ");
        outText = outText.Replace(" How ", " how ").Replace(" In ", " in ").Replace(" Is ", " is ").Replace(" It ", " it ").Replace(" La ", " la ").Replace(" Of ", " of ");
        outText = outText.Replace(" On ", " on ").Replace(" Or ", " or ").Replace(" That ", " that ").Replace(" The ", " the ").Replace(" To ", " to ").Replace(" Was ", " was ");
        outText = outText.Replace(" What ", " what ").Replace(" When ", " when ").Replace(" Where ", " where ").Replace(" Who ", " who ").Replace(" Will ", " will ").Replace(" With ", " with ");
        outText = outText.Replace(" Und ", " und ").Replace(" Www ", " www ");
      }
      catch (Exception)
      {
        outText = inText;
      }
      return outText;
    }

    #endregion

	#region Resume Related methods

    /// <summary>
    /// Populates missing information in pasted/uploaded resumes from the user's profile
    /// </summary>
    /// <param name="resume">The resume being uploaded or pasted</param>
    public static void PopulateMissingResumeDetailsFromUserData(ResumeModel resume)
    {
      if (resume.ResumeContent.SeekerContactDetails.IsNull())
        resume.ResumeContent.SeekerContactDetails = new Contact();

      if (resume.ResumeContent.EducationInfo.IsNull())
        resume.ResumeContent.EducationInfo = new EducationInfo();

      if (resume.ResumeContent.EducationInfo.SchoolStatus.HasValue && !IsValidSchoolStatus(resume.ResumeContent.EducationInfo.SchoolStatus.Value))
        resume.ResumeContent.EducationInfo.SchoolStatus = null;

      if (!resume.ResumeContent.EducationInfo.SchoolStatus.HasValue)
      {
        if (OldApp_RefactorIfFound.User.EnrollmentStatus.HasValue && IsValidSchoolStatus(OldApp_RefactorIfFound.User.EnrollmentStatus.Value))
          resume.ResumeContent.EducationInfo.SchoolStatus = OldApp_RefactorIfFound.User.EnrollmentStatus;
      }

      if (OldApp_RefactorIfFound.UserData.Profile.IsNotNull())
      {
        if (resume.ResumeContent.SeekerContactDetails.SeekerName.IsNull() || (resume.ResumeContent.SeekerContactDetails.SeekerName.FirstName.IsNullOrEmpty() && resume.ResumeContent.SeekerContactDetails.SeekerName.LastName.IsNullOrEmpty() && resume.ResumeContent.SeekerContactDetails.SeekerName.MiddleName.IsNullOrEmpty()))
          resume.ResumeContent.SeekerContactDetails.SeekerName = OldApp_RefactorIfFound.UserData.Profile.UserGivenName;

        if (resume.ResumeContent.SeekerContactDetails.PostalAddress.IsNull() || (resume.ResumeContent.SeekerContactDetails.PostalAddress.Street1.IsNullOrEmpty() && resume.ResumeContent.SeekerContactDetails.PostalAddress.Street2.IsNullOrEmpty() && resume.ResumeContent.SeekerContactDetails.PostalAddress.City.IsNullOrEmpty()
                                                                                 && resume.ResumeContent.SeekerContactDetails.PostalAddress.CountyName.IsNullOrEmpty() && resume.ResumeContent.SeekerContactDetails.PostalAddress.Zip.IsNullOrEmpty() && resume.ResumeContent.SeekerContactDetails.PostalAddress.MajorCity.IsNullOrEmpty()
                                                                                 && resume.ResumeContent.SeekerContactDetails.PostalAddress.CountryName.IsNullOrEmpty() && resume.ResumeContent.SeekerContactDetails.PostalAddress.StateName.IsNullOrEmpty()))
        {
          resume.ResumeContent.SeekerContactDetails.PostalAddress = OldApp_RefactorIfFound.UserData.Profile.PostalAddress;
        }

        if (resume.ResumeContent.SeekerContactDetails.PostalAddress.IsNotNull())
        {
          var stateId = resume.ResumeContent.SeekerContactDetails.PostalAddress.StateId;
          if (stateId.IsNotNull() && stateId.GetValueOrDefault(0) != ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.States, key: Constants.CodeItemKeys.States.ZZ)[0].Id)
          {
            if (resume.ResumeContent.SeekerContactDetails.PostalAddress.CountryId.IsNull())
              resume.ResumeContent.SeekerContactDetails.PostalAddress.CountryId = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.Countries, key: Constants.CodeItemKeys.Countries.US)[0].Id;
          }

          if (OldApp_RefactorIfFound.Settings.CountyEnabled)
          {
            var zipCode = resume.ResumeContent.SeekerContactDetails.PostalAddress.Zip;
            if (zipCode.IsNotNullOrEmpty() && resume.ResumeContent.SeekerContactDetails.PostalAddress.CountyId.IsNull())
            {
              long stateLookupId, countyLookupId;
              string cityName;
              ServiceClientLocator.CoreClient(_app).GetStateCityAndCountyForPostalCode(zipCode, out stateLookupId, out countyLookupId, out cityName);

              if (stateLookupId == stateId && countyLookupId != 0)
                resume.ResumeContent.SeekerContactDetails.PostalAddress.CountyId = countyLookupId;
            }
          }
        }

	      var invalidPhoneNumber = false;
	      var phoneNumber = resume.ResumeContent.SeekerContactDetails.PhoneNumber;
				if (phoneNumber.IsNullOrEmpty())
				{
					invalidPhoneNumber = true;
				}
				else
				{
					var number = phoneNumber[0].PhoneNumber;
					if (!Regex.IsMatch(number, _app.Settings.PhoneNumberRegExPattern) || Regex.Replace(number, "[^0-9]", "").Length != Regex.Replace(_app.Settings.PhoneNumberMaskPattern, "[^0-9]", "").Length)
					{
						invalidPhoneNumber = true;
					}
				}

				if (invalidPhoneNumber && OldApp_RefactorIfFound.UserData.Profile.PrimaryPhone.IsNotNull())
        {
          resume.ResumeContent.SeekerContactDetails.PhoneNumber = new List<Phone> { OldApp_RefactorIfFound.UserData.Profile.PrimaryPhone };
        }

        if (resume.ResumeContent.SeekerContactDetails.EmailAddress.IsNullOrEmpty())
          resume.ResumeContent.SeekerContactDetails.EmailAddress = OldApp_RefactorIfFound.UserData.Profile.EmailAddress;
      }

      if (OldApp_RefactorIfFound.Settings.CareerSearchResumesSearchable != ResumeSearchableOptions.NotApplicable)
      {
        if (resume.Special.Preferences.IsNull())
          resume.Special.Preferences = new Preferences();

        resume.Special.Preferences.IsResumeSearchable = (OldApp_RefactorIfFound.Settings.CareerSearchResumesSearchable == ResumeSearchableOptions.Yes ||
                                                         OldApp_RefactorIfFound.Settings.CareerSearchResumesSearchable == ResumeSearchableOptions.JobSeekersOptionYesByDefault);
      }
    }

    /// <summary>
    /// Updates the user context from default resume.
    /// </summary>
    /// <returns></returns>
    public static bool UpdateUserContextFromDefaultResume(ResumeModel defaultResume = null, bool updateOnets = true)
    {
      var updated = false;
      try
      {
        if (defaultResume.IsNull())
          defaultResume = ServiceClientLocator.ResumeClient(_app).GetDefaultResume();

        if (defaultResume.IsNull() || defaultResume.ResumeMetaInfo.CompletionStatus == 0)
        {
          OldApp_RefactorIfFound.UserData.DefaultResumeId = null;
	        OldApp_RefactorIfFound.UserData.DefaultResumeCompletionStatus = ResumeCompletionStatuses.None;
        }
        else
        {
          OldApp_RefactorIfFound.UserData.DefaultResumeId = defaultResume.ResumeMetaInfo.ResumeId;
	        OldApp_RefactorIfFound.UserData.DefaultResumeCompletionStatus = defaultResume.ResumeMetaInfo.CompletionStatus;

          if (updateOnets)
            CheckOnetsInResume(defaultResume, true);
        }
        updated = true;
      }
      catch
      {
      }

      return updated;
    }

    /// <summary>
    /// Checks the onets in the default resume
    /// </summary>
    /// <param name="defaultResume">The default resume</param>
    /// <param name="doSave">Whether to save the updated model</param>
    public static void CheckOnetsInResume(ResumeModel defaultResume, bool doSave)
    {
      var resumeMetaInfo = defaultResume.ResumeMetaInfo;
      if (resumeMetaInfo.IsNotNull())
      {
        var onet = resumeMetaInfo.ResumeOnet;
        var ronet = resumeMetaInfo.ResumeROnet;
        if (string.IsNullOrEmpty(onet) || string.IsNullOrEmpty(ronet))
        {
          var onetsUpdated = UpdateOnetROnetInResume(defaultResume);
          if (onetsUpdated && doSave)
            ServiceClientLocator.ResumeClient(_app).SaveResume(defaultResume);
        }
      }
    }

    /// <summary>
    /// Determines whether [is past five years veteran] [the specified start date].
    /// </summary>
    /// <param name="startDate">The start date.</param>
    /// <param name="endDate">The end date.</param>
    /// <returns>
    /// 	<c>true</c> if [is past five years veteran] [the specified start date]; otherwise, <c>false</c>.
    /// </returns>
    public static bool IsPastFiveYearsVeteran(DateTime? startDate, DateTime? endDate)
    {
      DateTime spanstart = DateTime.Today.AddYears(-5), spanend = DateTime.Today;
      return ((startDate.HasValue && startDate > spanstart && startDate < spanend) ||
              (endDate.HasValue && endDate > spanstart && endDate < spanend));
    }

    /// <summary>
    /// Saves the criteria against resume.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <param name="matchingType">Type of the matching.</param>
    /// <param name="setLoggingCookie">Whether to set a cookie to pass to the search results to indicate a manual search</param>
    public static void SaveCriteriaAgainstResume(ResumeModel model, MatchingType matchingType, bool setLoggingCookie = false)
    {
      var criteria = new SearchCriteria();
      var stateId = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.States).Where(x => x.Key == OldApp_RefactorIfFound.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

      #region Job Matching Criteria

    	criteria.RequiredResultCriteria = new RequiredResultCriteria
    	                                  	{
    	                                  		DocumentsToSearch = DocumentType.Posting,
    	                                  		MinimumStarMatch = (matchingType == MatchingType.ProgramOfStudy) ? StarMatching.None : (OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Education ? StarMatching.None : StarMatching.ThreeStar),
    	                                  		MaximumDocumentCount = OldApp_RefactorIfFound.Settings.MaximumNoDocumentToReturnInSearch
    	                                  	};

      #endregion

      #region Posting Age Criteria

      criteria.PostingAgeCriteria = new PostingAgeCriteria { PostingAgeInDays = _app.Settings.JobSearchPostingDate };

      #endregion

      #region Job Location Criteria

      var joblocation = new JobLocationCriteria();

      if (model.IsNotNull() && model.Special.IsNotNull())
      {
        var preference = model.Special.Preferences;
        if (preference.IsNotNull())
        {
          if (preference.SearchInMyState.HasValue && (bool)preference.SearchInMyState)
          {
            joblocation.OnlyShowJobInMyState = true;
            joblocation.Area = new AreaCriteria { StateId = stateId };
          }

          if (preference.ZipPreference.IsNotNull() && preference.ZipPreference.Count > 0)
          {
            var lookup = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.Radiuses).FirstOrDefault(x => x.Id == preference.ZipPreference[0].RadiusId);
            var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
            var radiusValue = radiusExternalId.ToLong();

            joblocation.Radius = new RadiusCriteria
            {
              Distance = radiusValue.GetValueOrDefault(),
              DistanceUnits = DistanceUnits.Miles,
              PostalCode = preference.ZipPreference[0].Zip
            };

            if (preference.SearchInMyState.HasValue && (bool)preference.SearchInMyState)
            {
              if (_app.Settings.NationWideInstance)
                if (OldApp_RefactorIfFound.UserData.Profile.IsNotNull() && OldApp_RefactorIfFound.UserData.Profile.PostalAddress.IsNotNull())
                  stateId = (long)OldApp_RefactorIfFound.UserData.Profile.PostalAddress.StateId;

              joblocation.Area = new AreaCriteria { StateId = stateId };
            }
          }
          else if (preference.MSAPreference.IsNotNull() && preference.MSAPreference.Count > 0)
          {
            joblocation.Area = new AreaCriteria
            {
              StateId = preference.MSAPreference[0].StateId,
              MsaId = preference.MSAPreference[0].MSAId
            };

            var lookup = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Id == preference.MSAPreference[0].StateId);
            if (lookup.IsNotNull())
              joblocation.SelectedStateInCriteria = lookup.ExternalId;

            //joblocation.Area = new AreaCriteria { State = preference.MSAPreference[0].StateCode };
            //if (preference.MSAPreference[0].MSAName.IsNotNull())
            //  joblocation.Area.MSA = preference.MSAPreference[0].MSAName;
          }

					// Added extra checking of the other location preferences because users can no longer select home based AND a location preference.  Location preference should take precendence if the resume xml hasn't been updated
					//if (preference.HomeBasedJobPostings.GetValueOrDefault() && ((preference.ZipPreference.IsNull() || preference.ZipPreference.Count == 0) && (preference.MSAPreference.IsNull() || preference.MSAPreference.Count == 0)))
					//	joblocation.HomeBasedJobPostings = true;
				}
      }

			criteria.JobLocationCriteria = joblocation;

      #endregion

      #region Reference Document Criteria

      if (model.IsNotNull())
      {
        if (matchingType == MatchingType.Resume || matchingType == MatchingType.ResumeAndProgramOfStudy)
        {
          criteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
          {
            DocumentId = model.ResumeMetaInfo.ResumeId.ToString(),
            DocumentType = DocumentType.Resume
          };
        }
      }

      #endregion

      #region Program of Study Criteria

      if (matchingType == MatchingType.ProgramOfStudy || matchingType == MatchingType.ResumeAndProgramOfStudy)
      {
        criteria.EducationProgramOfStudyCriteria = new EducationProgramOfStudyCriteria
        {
          ProgramAreaId = OldApp_RefactorIfFound.User.ProgramAreaId,
          DegreeEducationLevelId = OldApp_RefactorIfFound.User.DegreeEducationLevelId
        };
      }

      #endregion

      if (setLoggingCookie)
        _app.SetCookieValue("Career:ManualSearch", "true");

			OldApp_RefactorIfFound.SetSessionValue("Career:SearchCriteria", criteria);
    }

    /// <summary>
    /// Checks whether the pasted enrollment status is valid
    /// </summary>
    /// <param name="status">The enrollment status to check</param>
    /// <returns>A boolean indicating if the status is valid or not</returns>
    private static bool IsValidSchoolStatus(SchoolStatus status)
    {
      var validStatuses = new List<SchoolStatus>();

      if (OldApp_RefactorIfFound.Settings.Theme == FocusThemes.Education)
      {
        if (OldApp_RefactorIfFound.Settings.SchoolType == SchoolTypes.TwoYear)
        {
          validStatuses.Add(SchoolStatus.Prospective);
          validStatuses.Add(SchoolStatus.FirstYear);
          validStatuses.Add(SchoolStatus.SophomoreOrAbove);
          validStatuses.Add(SchoolStatus.NonCreditOther);
          validStatuses.Add(SchoolStatus.Alumni);
        }
        else if (OldApp_RefactorIfFound.Settings.SchoolType == SchoolTypes.FourYear)
        {
          validStatuses.Add(SchoolStatus.Prospective);
          validStatuses.Add(SchoolStatus.FirstYear);
          validStatuses.Add(SchoolStatus.Sophomore);
          validStatuses.Add(SchoolStatus.Junior);
          validStatuses.Add(SchoolStatus.Senior);
          validStatuses.Add(SchoolStatus.Graduate);
          validStatuses.Add(SchoolStatus.NonCreditOther);
          validStatuses.Add(SchoolStatus.Alumni);
        }
      }
      else
      {
        validStatuses.Add(SchoolStatus.Not_Attending_School_HS_Graduate);
        validStatuses.Add(SchoolStatus.Not_Attending_School_OR_HS_Dropout);
        validStatuses.Add(SchoolStatus.In_School_Post_HS);
        validStatuses.Add(SchoolStatus.In_School_Alternative_School);
        validStatuses.Add(SchoolStatus.In_School_HS_OR_less);
      }

      return validStatuses.Contains(status);
    }
    #endregion

    #region Other Career related methods

    /// <summary>
    /// Creates a bookmark in career
    /// </summary>
    /// <param name="name">The name</param>
    /// <param name="type">The type</param>
    /// <param name="criteria">The book mark id</param>
    public static bool CreateCareerBookmark(string name, BookmarkTypes type, string criteria = "")
    {
      var existingBookmarks = ServiceClientLocator.AnnotationClient(_app).GetMyBookmarks(BookmarkTypes.Posting, name);

      if (existingBookmarks.Any(b => string.Compare(criteria, b.Criteria, StringComparison.InvariantCulture) == 0))
        return false;

      ServiceClientLocator.AnnotationClient(_app).CreateBookmark(name, BookmarkTypes.Posting, criteria);

      return true;
    }

    #endregion

    #region Explorer Related Methods

    /// <summary>
    /// Gets program areas, with an optional 'undeclared' at the start of the list
    /// </summary>
    /// <param name="undeclaredOption">The text for the undeclared option</param>
    /// <returns>A list of program areas</returns>
    public static List<ProgramAreaDto> GetProgramAreas(string undeclaredOption = "")
    {
      var programAreas = ServiceClientLocator.ExplorerClient(_app).GetProgramAreas();

      if (undeclaredOption.Length > 0)
      {
        var programAreasWithUndeclared = new List<ProgramAreaDto>
        {
          new ProgramAreaDto {Description = undeclaredOption, Id = 0, Name = undeclaredOption}
        };

        programAreasWithUndeclared.AddRange(programAreas);

        return programAreasWithUndeclared;
      }

      return programAreas;
    }

    /// <summary>
    /// Populates a degree levels drop-down with only available levels if relevant
    /// </summary>
    /// <param name="dropdown">The drop-down to populate</param>
    /// <param name="certificateText">The text to display for the certificates option</param>
    /// <param name="bachelorsText">The text to display for the bachelors option</param>
    /// <param name="graduateText">The text to display for the graduates option</param>
    /// <param name="showSelect">if set to <c>true</c> show a 'select degree level' option.</param>
    public static void PopulateDegreeLevelDropdown(DropDownList dropdown, string certificateText, string bachelorsText, string graduateText, bool showSelect = false)
    {
      var showCertificate = false;
      var showBachelors = false;
      var showGraduate = false;

      if (OldApp_RefactorIfFound.Settings.ExplorerDegreeFilteringType != DegreeFilteringType.Both)
      {
        var degreeLevels = ServiceClientLocator.ExplorerClient(_app).GetDegreeAliases(0, "", OldApp_RefactorIfFound.Settings.ExplorerDegreeFilteringType == DegreeFilteringType.Theirs, DetailedDegreeLevels.NoneSpecified)
                                                  .Select(d => d.EducationLevel)
                                                  .Distinct()
                                                  .ToList();

        if (degreeLevels.Contains(ExplorerEducationLevels.Certificate) || degreeLevels.Contains(ExplorerEducationLevels.Associate))
          showCertificate = true;

        if (degreeLevels.Contains(ExplorerEducationLevels.Bachelor))
          showBachelors = true;

        if (degreeLevels.Any(el => el > ExplorerEducationLevels.Bachelor))
          showGraduate = true;
      }
      else
      {
        showCertificate = true;
        showBachelors = true;
        showGraduate = true;
      }

      if (showCertificate)
        dropdown.Items.AddEnum(DetailedDegreeLevels.CertificateOrAssociate, certificateText);

      if (showBachelors)
        dropdown.Items.AddEnum(DetailedDegreeLevels.Bachelors, bachelorsText);

      if (showGraduate)
        dropdown.Items.AddEnum(DetailedDegreeLevels.GraduateOrProfessional, graduateText);

      if (showSelect)
        dropdown.Items.AddLocalisedTopDefault("Global.DegreeLevel.TopDefault", "- select degree level -");
    }

    /// <summary>
    /// Creates a bookmark in Explorer
    /// </summary>
    /// <param name="name">The name of the bookmark to create</param>
    /// <param name="bookmarkType">The type of bookmark (either explorer home page, or report page)</param>
    /// <param name="criteria">The bookmark criteria</param>
    /// <returns>True if the bookmark was successfully created, false if a matching book mark exists</returns>
    public static bool CreateExplorerBookmark(string name, BookmarkTypes bookmarkType, ExplorerCriteria criteria)
    {
      var existingBookmarks = ServiceClientLocator.AnnotationClient(_app).GetMyBookmarks(bookmarkType, name);

      if (existingBookmarks.Any(b => criteria.Compare(b.Criteria.DeserializeJson<ExplorerCriteria>())))
        return false;

			ServiceClientLocator.AnnotationClient(_app).CreateBookmark(name, bookmarkType, criteria);

      return true;
    }

    #endregion

    #region Bind common dropdowns

    /// <summary>
    /// Adds the campuses to the campus drop down.
    /// </summary>
    /// <param name="campusDropDown">The drop down to which to bind the campuses.</param>
    public static void BindCampusDropDown(DropDownList campusDropDown)
    {
      campusDropDown.Items.Clear();

      campusDropDown.DataSource = ServiceClientLocator.CandidateClient(_app).GetCampuses().OrderBy(campus => campus.Name);
      campusDropDown.DataTextField = "Name";
      campusDropDown.DataValueField = "Id";
      campusDropDown.DataBind();

      campusDropDown.Items.AddLocalisedTopDefault("UpdateCampusDropDown.TopDefault", "- select campus location -");
    }

    #endregion
  }

  internal class IgnoreCaseStringComparer : IEqualityComparer<string>
  {
    /// <summary>
    /// Checks if 2 strings are equal
    /// </summary>
    /// <param name="item1">The item1.</param>
    /// <param name="item2">The item2.</param>
    /// <returns></returns>
    public bool Equals(string item1, string item2)
    {
      if (ReferenceEquals(item1, item2))
        return true;
      if (ReferenceEquals(item1, null) || ReferenceEquals(item2, null))
        return false;

      return item1.ToLower() == item2.ToLower();
    }

    /// <summary>
    /// Returns a hash code for this instance.
    /// </summary>
    /// <param name="item">The item.</param>
    /// <returns>
    /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
    /// </returns>
    public int GetHashCode(string item)
    {
      if (ReferenceEquals(item, null))
        return 0;

      return item.GetHashCode();
    }
  }
}
