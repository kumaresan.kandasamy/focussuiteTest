﻿namespace Focus.CareerExplorer
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Web;

	/// <summary>
	/// Magic strings used throughout the application. To avoid errors / bugs due to typos please store your magic string here and reference them.
	/// </summary>
	public static class MagicStrings
	{
		/// <summary>
		/// Magic keys used when accessing objects in the session by key.
		/// </summary>
		public static class SessionKeys
		{
			/// <summary>
			/// The career search criteria
			/// </summary>
			public static string CareerSearchCriteria = "Career:SearchCriteria";

			/// <summary>
			/// The career previous search criteria
			/// </summary>
			public static string CareerPreviousSearchCriteria = "Career:PreviousSearchCriteria";
		}
	}
}