﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.Security;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Criteria.Explorer;

#endregion

namespace Focus.CareerExplorer.Code
{
  public class CareerExplorerPageBase : PageBase
  {
    public CareerExplorerPageBase()
    {
      RedirectIfNotAuthenticated = false;
      CheckSession = false;
      ClearExplorerReturnSession = true;
    }

	  /// <summary>
	  /// Gets or sets a value indicating whether [redirect if not authenticated].
	  /// </summary>
	  /// <value>
	  /// 	<c>true</c> if [redirect if not authenticated]; otherwise, <c>false</c>.
	  /// </value>
    public bool RedirectIfNotAuthenticated { get; set; }

    public bool AnonymousAccessRequiresCheck { get; set; }

    public bool CheckSession { get; set; }

    /// <summary>
    /// Whether to clear down the session variable used in the "Return to Explorer" link
    /// </summary>
    public bool ClearExplorerReturnSession { get; set; }

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Load"/> event.
    /// </summary>
    /// <param name="e">The <see cref="T:System.EventArgs"/> object that contains the event data.</param>
    protected override void OnLoad(EventArgs e)
    {
        /** KRP 15.Feb.2017
         * FVN-4362 - Exception is thrown when the user logins simultaneously in two tabs of same browser
         * if (session.UserId != 0 && session.UserId != userId )
         *     throw new SessionException("Session does not belong to user");
         * Since userid is assigned only when AuthenticationService is called, userid will be '0' at this point
         * So removing the session only when the page is loaded on an event from the user
        */
        if (!IsPostBack && ClearExplorerReturnSession)
        {
            App.RemoveSessionValue("Explorer:ReturnBookmark");
        }

      if (App.IsAnonymousAccess())
      {
        if (!App.User.IsAuthenticated && !App.User.IsAnonymous)
        {
          if (!Page.Is(UrlBuilder.JobSearchCriteria()))
          {
            Response.Redirect(UrlBuilder.JobSearchCriteria());
          }
          else
          {
            var masterPage = MasterPage as Site;
            if (masterPage != null)
              masterPage.ShowTermsAndConditions();
          }
        }
      }
			else if (RedirectIfNotAuthenticated && !App.User.IsAuthenticated)
			{
				App.SetCookieValue("CareerExplore:Redirect", Request.Url.AbsolutePath, true);
				Response.Redirect(UrlBuilder.Home());
			}

      if (!App.Settings.UseSqlSessionState && CheckSession && !App.UserData.CareerUserDataCheck.HasValue)
      {
        try
        {
					ServiceClientLocator.AuthenticationClient(App).LogOut();
        }
        finally
        {
          FormsAuthentication.SignOut();
          App.ClearSession();

          App.SetCookieValue("CareerExplore:Timeout", "1");
          Response.Redirect(UrlBuilder.Default(), true);
        }
      }

      base.OnLoad(e);
    }
  }

  public interface IExplorerJobModalPage
  {
    ExplorerCriteria ReturnReportCriteria { get; }
  }
}