﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web;

using Focus.Core;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.Code
{
	public static class UrlBuilder
	{
		private static IApp _app;

		/// <summary>
		/// Gets the application.
		/// </summary>
		/// <value>
		/// The application.
		/// </value>
		private static IApp App
		{
			get { return _app ?? (_app = new HttpApp()); }
		}

		#region Career

		/// <summary>
		/// Returns a "Goto" url based on the name and optional arguments.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="args">The arguments.</param>
		/// <returns></returns>
		public static string Goto(string name, params object[] args)
		{
			if (name.IsNullOrEmpty())
				name = "";

			switch (name.ToLower())
			{
				case "resume":
					if (App.User.IsNotNull() && App.User.DefaultResumeId.IsNotNull())
						App.SetSessionValue("Career:ResumeID", App.User.DefaultResumeId);
					return ResumeWizard();

				case "resumetab":
					return args.IsNullOrEmpty() ? ResumeWizard() : ResumeWizard(Convert.ToString(args[0]));

				case "jobpostings":
					return JobPosting();

				case "searchjobpostings":
					return JobSearchCriteria();

				case "createresume":
					// By removing any ResumeID that may exist in the session we will force Focus to create a new one.
					App.RemoveSessionValue("Career:ResumeID");
					return ResumeWizard();

				case "explore":
					return Explore();

				default:
					return Default();
			}
		}

		public static string Content(string virtualPath)
		{
			return Path(virtualPath);
		}

		public static string Default()
		{
			return Home();
		}

		public static string LogIn() { return Path("~/login"); } // Joint usage, add app check

		public static string ReactivateAccount() { return Path("~/reactivateaccount"); }

		public static string Home()
		{
		  if (OldApp_RefactorIfFound.Settings.Module == FocusModules.Explorer) return Explorer();

			if (OldApp_RefactorIfFound.Settings.Module == FocusModules.Career) return Career();

		  return OldApp_RefactorIfFound.Settings.CareerExplorerFeatureEmphasis == CareerExplorerFeatureEmphasis.Explorer ? Explore() : Career();
		} // Joint usage, add app check

    public static string Career()
    {
			// ADC - I think that this has not been working for SAML integration so far so I have temporarially commented it out but this needs to be revisited
			//if (OldApp_RefactorIfFound.User.IsMigrated || !OldApp_RefactorIfFound.IsSSO)
				return Path("~/home");

			//return SSOCareerRegistration();
    }

		public static string SSOCareerRegistration(){return Path("~/ssoregistration");}

		public static string JobSearch() { return Path("~/jobsearch"); } // Joint usage, add app check
		public static string JobSearchCriteria() { return Path("~/jobsearch/searchjobpostings"); }
		public static string JobSearchCriteria(string Control) { return Path("~/jobsearch/" + Control); }
    public static string JobSearchResults(string resumeId = null)
		{
			var relativeUrl = "~/jobsearch/searchresults";
			if (resumeId.IsNotNullOrEmpty()) relativeUrl = relativeUrl + "?resumeId=" + resumeId;
			return Path(relativeUrl);
		}
    public static string JobPosting() { return Path("~/jobposting"); }
    public static string JobPosting(string jobid, string fromUrl, string pageSection) { return Path(string.Format("~/jobposting/{0}/{1}/{2}", jobid, fromUrl, pageSection)); }
    public static string Explore() { return Path("~/explorer/explore"); }  // Joint usage, add app check
		public static string ResumeWizard() { return Path("~/resume"); }
		public static string ResumeWizard(string tab) { return Path(String.Format("~/resume/{0}", tab)); }
		public static string YourResume() { return Path("~/resume/create"); }
		public static string SearchPostingsEducation() { return Path("~/jobsearch/searcheducationpostings"); }
    public static string Tutorial() { return Path("~/tutorial"); }

    public static string ExternalValidation() { return Path("~/externalvalidation"); }

		public static string ReturnToResumeWizard(ResumeCompletionStatuses completionStatus)
		{
			if ((completionStatus & ResumeCompletionStatuses.WorkHistory) != ResumeCompletionStatuses.WorkHistory)
				return ResumeWizard("workhistory");

			if ((completionStatus & ResumeCompletionStatuses.Contact) != ResumeCompletionStatuses.Contact)
				return ResumeWizard("contact");

			if ((completionStatus & ResumeCompletionStatuses.Education) != ResumeCompletionStatuses.Education)
				return ResumeWizard("education");

			if ((completionStatus & ResumeCompletionStatuses.Summary) != ResumeCompletionStatuses.Summary)
				return ResumeWizard("summary");

			if ((completionStatus & ResumeCompletionStatuses.Options) != ResumeCompletionStatuses.Options)
				return ResumeWizard("options");

			if ((completionStatus & ResumeCompletionStatuses.Profile) != ResumeCompletionStatuses.Profile)
				return ResumeWizard("profile");

			if ((completionStatus & ResumeCompletionStatuses.Preferences) != ResumeCompletionStatuses.Preferences)
				return ResumeWizard("preferences");

			return ResumeWizard("review");
		}
	
		public static string MyAccount() { return Path("~/myaccount"); }  // Joint usage, add app check
		public static string MyBookmarks() { return Path("~/mybookmarks"); }  // Joint usage, add app check
    public static string PinRegistration(bool fullPath = true) { return Path(fullPath, "/pinregistration/{0}"); }

		public static string NoticeRoot() { return Path("~/notice"); }
		public static string Notice(string title) { return Path("~/notice/" + title); }
		public static string Notice(string title, long category) { return Path("~/notice/" + title + "/" + category + "/" + DocumentFocusModules.Career); }
		public static string Notice(string title, long category, long group) { return Path("~/notice/" + title + "/" + category + "/" + group + "/" + DocumentFocusModules.Career); }

		public static string OAuth() { return Path("~/oauth"); }
		public static string OAuthVerifier(bool fullPath = true) { return Path(fullPath, "/oauth/verify"); }
		public static string OAuthLogin() { return Path("~/oauth/login"); }

		public static string Saml() { return Path("~/saml"); }
		public static string SamlResolveArtifacts(bool fullPath = true) { return Path(fullPath, "/saml/resolveartifacts"); }
		public static string SamlAssertionService(bool fullPath = true) { return Path(fullPath, "/saml/assert"); }
		public static string SamlLogout(bool fullPath = true) { return Path(fullPath, "/saml/logout"); }

		public static string SSOComplete() { return Path("~/ssocomplete"); }

		// Other content
    public static string MainLogoImage() { return OldApp_RefactorIfFound.Settings.Module == FocusModules.Explorer ? Path("~/Assets/Images/logo_focusexplorer.png") : ApplicationImage(ApplicationImageTypes.FocusCareerExplorerHeader); }  // Joint usage, add app check
		public static string MainCss() { return Path("~/Assets/Css/Focus.css"); }
    public static string ApplicationImage(ApplicationImageTypes applicationImageType) { return Path("~/applicationimage/" + applicationImageType); }

		public static string ProgressImage() { return BrandAssetPath("Images/indicator.gif"); }

		public static string StarOn() { return BrandAssetPath("Images/icon_star_on.png"); }
		public static string StarOff() { return BrandAssetPath("Images/icon_star_off.png"); }
		public static string StarCompanies() { return BrandAssetPath("Images/icon_star_companies.png"); }

		public static string Close() { return BrandAssetPath("Images/icon_close.png"); }
		public static string CloseMedium() { return BrandAssetPath("Images/icon_close_med.png"); }
		public static string CloseSmall() { return BrandAssetPath("Images/icon_close_sm.png"); }

		public static string CloseRoundOff() { return BrandAssetPath("Images/button_x_close_off.png"); }
		public static string CloseRoundOn() { return BrandAssetPath("Images/button_x_close_on.png"); }

		public static string HelpIcon() { return BrandAssetPath("Images/icon_help.png"); }

		public static string CreateResume() { return BrandAssetPath("Images/Icons/icon_createresume.png"); }
		public static string CreateResumeMedium() { return BrandAssetPath("Images/Icons/icon_createresume_med.png"); }
		public static string CreateResumeSmall() { return BrandAssetPath("Images/Icons/icon_createresume_sm.png"); }

		public static string DownloadIcon() { return BrandAssetPath("Images/icon_download.png"); }
		public static string EmailIcon() { return BrandAssetPath("Images/icon_email.png"); }
		public static string PrintIcon() { return BrandAssetPath("Images/icon_print.png"); }

		public static string PlusIcon() { return BrandAssetPath("Images/plus.png"); }
		public static string MinusIcon() { return BrandAssetPath("Images/minus.png"); }

		public static string SortUp() { return BrandAssetPath("Images/icon_triangle_up.png"); }
		public static string SortDown() { return BrandAssetPath("Images/icon_triangle_down.png"); }

		public static string BlockArrowRight() { return BrandAssetPath("Images/icon_arrow_block_right.png"); }
		public static string BlockArrowRightSmall() { return BrandAssetPath("Images/icon_arrow_block_right_sm.png"); }

		public static string BookmarkIcon() { return BrandAssetPath("Images/icon_bookmark.png"); }
		public static string NewWindowIcon() { return BrandAssetPath("Images/icon_newwindow.png"); }

		public static string ExclamationIcon() { return BrandAssetPath("Images/icon_exclamation.png"); }

		public static string Bullet() { return BrandAssetPath("Images/bullet.gif"); }

		public static string NonSpideredJobIconSmall() { return BrandAssetPath("Images/icon_nonspideredjob_small.png"); }

		#endregion

		#region Explorer

		// Login, logout, forgotten password, change password, account settings & register
		public static string ForgottenPassword() { return Path("~/forgottenpassword"); }
		public static string ChangePassword() { return Path("~/changepassword"); }
		public static string ChangeLogin() { return Path("~/changelogin"); }
		public static string Register() { return Path("~/register"); }
    public static string ResetPassword(bool fullPath = true) { return Path(fullPath, "/resetpassword"); }
		public static string AccountValidate(bool fullPath = true) { return Path(fullPath, "/validate/{0}"); }

		// Explorer
		public static string ExplorerRoot() { return Path("~/explorer"); }
		public static string Explorer(){return Path("~/explorer/explore/");}

		public static string Explore(string option) { return Path("~/explorer/explore/" + option); }
		public static string Explore(string option, string bookmarkId) { return Path("~/explorer/explore/" + option + "/" + bookmarkId); }
    public static string ExplorerBookmarkLink(string bookmarkId) { return OldApp_RefactorIfFound.Settings.CareerApplicationPath + "/explorer/explore/bookmark/" + bookmarkId; }

    public static string Print(ReportTypes reportType) { return Path("~/explorer/print/" + reportType.ToString().ToLower()); }
    public static string Print(ReportTypes reportType, ModalPrintFormat printFormat) { return Path("~/explorer/print/" + reportType.ToString().ToLower() + "/" + printFormat.ToString().ToLower()); }

    public static string Report() { return Path("~/explorer/report"); }
    public static string Report(long bookmarkId) { return Path("~/explorer/report/bookmark/" + bookmarkId); }
    public static string Report(ReportTypes reportType, long stateAreaId) { return Path("~/explorer/report/" + reportType.ToString().ToLower() + "/" + stateAreaId); }
    public static string Report(ReportTypes reportType, long stateAreaId, long careerAreaId) { return Path("~/explorer/report/" + reportType.ToString().ToLower() + "/" + stateAreaId + "/" + careerAreaId); }
    public static string Report(ReportTypes reportType, long stateAreaId, CareerAreaOccupationTypes careerAreaOccupationType, string careerAreaOccupationId) { return Path(String.Format("~/explorer/report/{0}/{1}/{2}/{3}", reportType.ToString().ToLower(), stateAreaId, careerAreaOccupationType.ToString().ToLower(), careerAreaOccupationId)); }
    public static string Report(ReportTypes reportType, long stateAreaId, CareerAreaOccupationTypes careerAreaOccupationType, string careerAreaOccupationId, string relatedId) { return Path(String.Format("~/explorer/report/{0}/{1}/{2}/{3}/{4}", reportType.ToString().ToLower(), stateAreaId, careerAreaOccupationType.ToString().ToLower(), careerAreaOccupationId, relatedId)); }
    public static string Search(string searchTerm) { return Path("~/explorer/search/" + searchTerm); }

		public static string ExploreSectionExploreImage() { return BrandAssetPath("Images/Icons/icon_explorecareers.png"); }
		public static string ExploreSectionExploreMediumImage() { return BrandAssetPath("Images/Icons/icon_explorecareers_med.png"); }
		public static string ExploreSectionExploreSmallImage() { return BrandAssetPath("Images/Icons/icon_explorecareers_sm.png"); }
		public static string ExploreSectionResearchImage() { return BrandAssetPath("Images/Icons/icon_research.png"); }
		public static string ExploreSectionStudyImage() { return BrandAssetPath("Images/Icons/icon_study.png"); }
		public static string ExploreSectionExperienceImage() { return BrandAssetPath("Images/Icons/icon_experience.png"); }
		public static string ExploreSectionSearchImage() { return BrandAssetPath("Images/Icons/icon_searchforjobs.png"); }
		public static string ExploreSectionSearchMediumImage() { return BrandAssetPath("Images/Icons/icon_searchforjobs_med.png"); }
		public static string ExploreSectionSearchSmallImage() { return BrandAssetPath("Images/Icons/icon_searchforjobs_sm.png"); }
		public static string ExploreSectionSchoolImage() { return BrandAssetPath("Images/Icons/icon_school.png"); }

		public static string JobSalaryHighImage() { return BrandAssetPath("Images/icon_salary_high.png"); }
		public static string JobSalaryMediumImage() { return BrandAssetPath("Images/icon_salary_med.png"); }
		public static string JobSalaryLowImage() { return BrandAssetPath("Images/icon_salary_low.png"); }
		public static string JobSalaryRangeHighImage() { return BrandAssetPath("Images/icon_salaryrange_high.png"); }
		public static string JobSalaryRangeMediumImage() { return BrandAssetPath("Images/icon_salaryrange_med.png"); }
		public static string JobSalaryRangeLowImage() { return BrandAssetPath("Images/icon_salaryrange_low.png"); }
		public static string JobPeopleHighImage() { return BrandAssetPath("Images/icon_people_high.png"); }
		public static string JobPeopleMediumImage() { return BrandAssetPath("Images/icon_people_med.png"); }
		public static string JobPeopleLowImage() { return BrandAssetPath("Images/icon_people_low.png"); }
		public static string JobHiringTrendVeryHighImage() { return BrandAssetPath("Images/icon_hiringdemand_veryhigh.png"); }
		public static string JobHiringTrendHighImage() { return BrandAssetPath("Images/icon_hiringdemand_high.png"); }
		public static string JobHiringTrendMediumImage() { return BrandAssetPath("Images/icon_hiringdemand_medium.png"); }
		public static string JobHiringTrendLowImage() { return BrandAssetPath("Images/icon_hiringdemand_low.png"); }
		public static string JobSalaryComparisonHigherImage() { return BrandAssetPath("Images/icon_hiringchange_up.png"); }
		public static string JobSalaryComparisonSimilarImage() { return BrandAssetPath("Images/icon_hiringchange_stable.png"); }
		public static string JobSalaryComparisonLowerImage() { return BrandAssetPath("Images/icon_hiringchange_down.png"); }

		public static string DegreeProgramsImage() { return BrandAssetPath("Images/icon_degreeprograms.png"); }

		public static string StarterJobsManyImage() { return BrandAssetPath("Images/icon_starterjobs_many.png"); }
		public static string StarterJobsSomeImage() { return BrandAssetPath("Images/icon_starterjobs_some.png"); }

		// Errors
		public static string Error(ErrorTypes errorType = ErrorTypes.Unknown) { return Path("~/error"); }

		// Help
		public static string Help() { return Path("~/help"); }
		public static string JavascriptHelp() { return Path("~/javascript/help"); }

		// Other content
		//public static string FocusSmallCss() { return Path("~/Assets/Css/Focus.css"); }
		public static string FocusMediumCss() { return Path("~/Assets/Css/Focus.Medium.css"); }
		public static string FocusLargeCss() { return Path("~/Assets/Css/Focus.Large.css"); }

		// Ajax
		public static string AjaxService() { return Path("~/Services/AjaxService.svc"); }


		private static string Path(string virtualPath)
		{
			return VirtualPathUtility.ToAbsolute(virtualPath);
		}

		/// <summary>
		/// Url builder helper.
		/// </summary>
		/// <param name="fullPath">Whether to output the full path to the root</param>
		/// <param name="virtualPath">The virtual path.</param>
		/// <returns></returns>
		private static string Path(bool fullPath, string virtualPath)
		{
			if (fullPath)
				virtualPath = string.Concat("~", virtualPath);

			return Path(virtualPath);
		}

		/// <summary>
		/// Builds an absolute path to a brand asset.
		/// </summary>
		/// <param name="assetPathStub">The virtual path stub for the brand asset.</param>
		/// <returns></returns>
		public static string BrandAssetPath(string assetPathStub)
		{
			return VirtualPathUtility.ToAbsolute(string.Format("~/Branding/{0}/{1}", App.Settings.BrandIdentifier, assetPathStub));
		}
		
		#endregion
	}
}
