﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Web.UI.WebControls;

using Focus.Core.Criteria.Explorer;

#endregion

namespace Focus.CareerExplorer.Code
{
  public abstract class ExplorerListControl : UserControlBase
  {
    public ExplorerCriteria ReportCriteria { get; set; }
    public int ListSize { get; set; }

    public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
    public event ItemClickedHandler ItemClicked;

    public abstract int RecordCount { get; }
    public abstract void BindList(bool forceReload);

    /// <summary>
    /// Raises an event to say an item has been clicked
    /// </summary>
    /// <param name="eventArgs">Arguments for the event</param>
    protected virtual void OnItemClicked(CommandEventArgs eventArgs)
    {
      if (ItemClicked != null)
        ItemClicked(this, eventArgs);
    }
  }
}