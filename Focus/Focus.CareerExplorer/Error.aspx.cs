﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common.Models;

#endregion

namespace Focus.CareerExplorer
{
	public partial class Error : PageBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ProcessException();
		}

    /// <summary>
    /// Overrides the on error event (to stop error page redirecting to itself continually)
    /// </summary>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected override void OnError(EventArgs e)
    {
      Response.Redirect(string.Concat("/ErrorSafe.htm?", Guid.NewGuid().ToString().ToLower()), true);
    }

    /// <summary>
    /// Processes the exception.
    /// </summary>
		private void ProcessException()
		{
			var errorInfo = string.Empty;
			var errorStack = string.Empty;

      ExceptionModel exception;
      try
      {
        exception = App.Exception;
      }
      catch (Exception ex)
      {
        exception = new ExceptionModel
        {
          Message = ex.Message,
          StackTrace = ex.StackTrace
        };
      }
			
			if (exception == null)
				errorInfo = CodeLocalise("ErrorType.Unknown", "Unknown error!");
			else
			{
				errorInfo = exception.Message;
        //TODO: Martha (New Functionality) - check below (new app setting)
        //if (exception.Message.Contains(App.Settings.DemoShieldMessage))
        //{
        //  Panel signin = (Panel)(this.Master.FindControl("AnonymousControls"));
        //  liHomeURL.Visible = signin.Visible = false;
        //}

				if (exception.StackTrace != null)
				{
#if DEBUG
					errorStack = exception.StackTrace.Replace("\n", "<br/>");
#else
					errorStack = "<!--\n\n" + exception.StackTrace + "\n\n-->";
#endif
				}
			}

			ErrorInfoLiteral.Text = errorInfo;
			ErrorStackLiteral.Text = errorStack;

			App.Exception = null;
			
		}
	}
}