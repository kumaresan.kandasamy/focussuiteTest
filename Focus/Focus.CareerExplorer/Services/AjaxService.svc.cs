﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Web.Security;
using AjaxControlToolkit;

using Focus.Common.Helpers;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models;
using Focus.Core.Views;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.Services
{
  [ServiceContract(Namespace = "http://api.burning-glass.org/")]
  [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
  public class AjaxService
  {
	  private readonly IApp _app;

    /// <summary>
    /// Default constructor to initializes a new instance of the <see cref="AjaxService"/> class.
    /// </summary>
    public AjaxService()
    {
      _app = new HttpApp();
    }

		public AjaxService(IApp app)
		{
			_app = app ?? new HttpApp();
		}

    #region Career methods

    /// <summary>
    /// Localises from code using the specified key and if not found it will return the default value.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="args">The args.</param>
    /// <returns></returns>
    protected string CodeLocalise(string key, string defaultValue, params object[] args)
    {

      return AjaxHelper.CodeLocalise(key, defaultValue, args);
    }

    /// <summary>
    /// Gets the job titles.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetJobTitles(string prefixText, int count)
    {
      return AjaxHelper.GetJobTitles(prefixText, count, null);
    }

		/// <summary>
		/// Gets the job titles with MO cs.
		/// </summary>
		/// <param name="prefixText">The prefix text.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		[OperationContract]
		public string[] GetJobTitlesWithMOCs(string prefixText, int count)
		{
			return AjaxHelper.GetJobTitlesWithMOCs(prefixText, count, null);
		}

    /// <summary>
    /// Gets the state city county and counties for postal code.
    /// </summary>
    /// <param name="postalCode">The postal code.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public PostalCodeStateCityCounty GetStateCityCountyAndCountiesForPostalCode(string postalCode)
    {
      return AjaxHelper.GetStateCityCountyAndCountiesForPostalCode(postalCode);
    }

    /// <summary>
    /// Gets the naics.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetNaics(string prefixText, int count)
    {
      return AjaxHelper.GetNaics(prefixText, count);
    }

    #region NAICS

    /// <summary>
    /// Checks if a NAICS exists
    /// </summary>
    /// <param name="naicsText">The NAICS.</param>
    /// <returns>A boolean indicating existence</returns>
    [OperationContract]
    public bool CheckNaicsExists(string naicsText)
    {
      return AjaxHelper.CheckNaicsExists(naicsText);
    }

    #endregion

    /// <summary>
    /// Checks the job title.
    /// </summary>
    /// <param name="jobTitle">The job title.</param>
    /// <returns></returns>
    [OperationContract]
    public string GetJobEmployer(string jobTitle)
    {
      //prefixText = "2514 - Flag Officer Writer";
      if (string.IsNullOrEmpty(jobTitle))
        return null;

      var title = jobTitle.ToLower();

      var mocTitles = ServiceClientLocator.OccupationClient(_app).GetMilitaryOccupationJobTitles(title, 10);

			if (mocTitles.IsNotNullOrEmpty())
			{
				var branchesOfService = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.MilitaryBranchesOfService);

			  //MilitaryOccupationJobTitleViewDto selectedMOC;
			  //if ((selectedMOC = mocTitles.FirstOrDefault(x => x.JobTitle.Trim().ToLower() == title)).IsNotNull())
				//	return string.Format("{0}||{1}", selectedMOC.BranchOfServiceId.ToString(), branchesOfService.Where(x => x.Id == selectedMOC.BranchOfServiceId).Select(x => x.Text).FirstOrDefault());

				var distinctBranchesOfService = mocTitles.Select(y => string.Format("{0}||{1}", y.BranchOfServiceId.ToString(), branchesOfService.Where(x => x.Id == y.BranchOfServiceId).Select(x => x.Text).FirstOrDefault())).Distinct().ToArray();
				return string.Join("$$", distinctBranchesOfService);
			}

    	var specialTitles = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.UnclassifiedJobs);
      if (specialTitles.IsNotNull())
      {
        var temp = from item in specialTitles
                   let sptitle = item.Text.ToLower()
                   let parts = sptitle.Split(',')
                   where sptitle == title || (parts.Length > 0 && parts.Contains(title))
                   select item;
        if (temp.Any())
          return "SPECIAL_JOB_EMPLOYER";
      }

      return null;
    }

    /// <summary>
    /// Gets the certifications.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetLicences(string prefixText, int count)
    {
      return AjaxHelper.GetLicences(prefixText, count);
    }

    /// <summary>
    /// Gets the licences.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetCertifications(string prefixText, int count)
    {
      return AjaxHelper.GetCertifications(prefixText, count);
    }
    
    /// <summary>
    /// Gets the languages.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetLanguages(string prefixText, int count)
    {
      return AjaxHelper.GetLanguages(prefixText, count);
    }

    /// <summary>
    /// Gets the degrees.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetDegrees(string prefixText, int count)
    {
      var degrees = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.Degrees);
      // TODO: Martha (check)
      var degreeTitles = (from i in degrees where !string.IsNullOrWhiteSpace(i.Text)  && i.Text.ToLower().Contains(prefixText.ToLower()) select i.Text).Distinct().ToList();
      return FormateLookupData(degreeTitles).ToArray();
    }

    /// <summary>
    /// Gets the majors.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetMajors(string prefixText, int count)
    {
      var majors = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.Majors);
      return (from i in majors where !string.IsNullOrWhiteSpace(i.Text) && i.Text.ToLower().Contains(prefixText.ToLower())  select i.Text).ToArray();
    }

    /// <summary>
    /// Gets the certificates.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetCertificates(string prefixText, int count)
    {
      return AjaxHelper.GetCertifications(prefixText, count);
    }

    /// <summary>
    /// Gets the skills.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetSkills(string prefixText, int count)
    {
      var skills = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.Skills);
      if (skills.Count > 0)
			  return (from i in skills where !string.IsNullOrWhiteSpace(i.Text) && i.Text.ToLower().Contains(prefixText.ToLower()) select i.Text).ToArray();

      var otherSkills = ServiceClientLocator.ExplorerClient(_app).GetSkills(null);
      return (from i in otherSkills where !string.IsNullOrWhiteSpace(i.Name) && i.Name.ToLower().Contains(prefixText.ToLower()) select i.Name).ToArray();
    }

    /// <summary>
    /// Gets the MOS.
    /// </summary>
    /// <param name="prefixText">The prefix text.</param>
    /// <param name="count">The count.</param>
    /// <param name="contextKey">The context key.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetMOS(string prefixText, int count, string contextKey)
    {
      return AjaxHelper.GetMilitaryJobTitles(prefixText, count, contextKey);
    }

    /// <summary>
    /// Gets the occupation.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetOccupation(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetOccupations(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets the search occupation.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetSearchOccupation(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetSearchOccupations(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets the industry detail.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetIndustryDetail(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetIndustryDetails(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets the county.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetCounty(string knownCategoryValues, string category)
    {
      var counties = AjaxHelper.GetCounties(knownCategoryValues, category);

      var ddlist = new List<CascadingDropDownNameValue>();

      string[] categoryValue = null;

      if (knownCategoryValues.Contains(":"))
        categoryValue = knownCategoryValues.Split(':');

      if (categoryValue.Length > 1 && categoryValue[1].IndexOf(';') > 0)
        categoryValue[1] = categoryValue[1].Remove(categoryValue[1].LastIndexOf(';'));

      //var counties = ServiceClientLocator.CoreClient(_app).GetLookup(Constants.DomainValues.USStates, subDomainValue: Constants.DomainValues.StateCounties.Replace("#", categoryValue[1]));

      if (counties.IsNotNullOrEmpty())
      {
        //ddlist.Add(new CascadingDropDownNameValue("- select county -", "", true));
        ddlist.Add(new CascadingDropDownNameValue("Outside U.S.", "0"));
        ddlist.AddRange(counties.Select(county => new CascadingDropDownNameValue(county.name, county.value)));
      }
      else
      {
        //ddlist.Add(new CascadingDropDownNameValue("- select county -", ""));
        ddlist.Add(new CascadingDropDownNameValue("Outside U.S.", "0", true));
      }

      return ddlist.ToArray();
    }

    /// <summary>
    /// Gets the MSA.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetMSA(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetMetropolitanStatisticalAreas(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets the rank.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    public CascadingDropDownNameValue[] GetRank(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetRank(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets the rank list.
    /// </summary>
    /// <param name="branchOfServiceId">The branch of service id.</param>
    /// <returns></returns>
    [OperationContract]
    public string[] GetRankList(long branchOfServiceId)
    {
      var allRanks = GetAllRanks(branchOfServiceId);
      if (allRanks.IsNotNull() && allRanks.Any())
      {
        return (from rank in allRanks
                select string.Format("{0}~{1}", rank.Id, rank.Text)).ToArray();
      }
      return null;
    }

    /// <summary>
    /// Shows the not what youre looking for.
    /// </summary>
    /// <param name="personId">The person id.</param>
    [OperationContract]
    public void ShowNotWhatYoureLookingFor(string personId)
    {
      ServiceClientLocator.CandidateClient(_app).ShowNotWhatYoureLookingFor(long.Parse(personId));
    }

    /// <summary>
    /// Gets all ranks.
    /// </summary>
    /// <param name="branchOfServiceId">The branch of service id.</param>
    /// <returns></returns>
    private IEnumerable<LookupItemView> GetAllRanks(long branchOfServiceId)
    {
			if (branchOfServiceId> 0)
      {
        var ranks = ServiceClientLocator.CoreClient(_app).GetLookup(LookupTypes.MilitaryRanks, branchOfServiceId);
      	return ranks;
      }

      return null;
    }


    /// <summary>
    /// Formates the lookup data.
    /// </summary>
    /// <param name="titles">The titles.</param>
    /// <returns></returns>
    public List<string> FormateLookupData(List<string> titles)
    {
      var lookupTitles = new List<string>();


      foreach (var title in titles)
      {
        var nodeText = title.Replace("&amp;", "&");

        if (nodeText.Contains("#"))
          nodeText = nodeText.Substring(nodeText.IndexOf("#") + 1);

        if (!lookupTitles.Contains(nodeText))
          lookupTitles.Add(nodeText);
      }

      return lookupTitles;
    }

    #endregion

    #region Explorer methods

    /// <summary>
    /// Gets the job title auto complete items
    /// </summary>
    /// <param name="term"></param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public AutoCompleteItem[] GetJobTitleAutoCompleteItems(string term)
    {
      var jobTitles = ServiceClientLocator.ExplorerClient(_app).GetJobTitles(term);

      // Take exact match first
      var a = (from jt in jobTitles
               where string.Equals(jt.Name, term, StringComparison.OrdinalIgnoreCase)
               orderby jt.Name
               select new AutoCompleteItem()
               {
                 Id = jt.Id.ToString(),
                 Label = jt.Name,
                 Value = jt.Name
               }).ToList();

      // Then add all others in alphabetical order next
      a.AddRange(from jt in jobTitles
                 where !string.Equals(jt.Name, term, StringComparison.OrdinalIgnoreCase)
                 orderby jt.Name
                 select new AutoCompleteItem()
                 {
                   Id = jt.Id.ToString(),
                   Label = jt.Name,
                   Value = jt.Name
                 });
      // Convert to array for output
      return a.ToArray();
    }

    /// <summary>
    /// Gets the location auto complete items
    /// </summary>
    /// <param name="term"></param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public AutoCompleteItem[] GetLocationAutoCompleteItems(string term)
    {
      return (from sa in ServiceClientLocator.ExplorerClient(_app).GetStateAreas()
              where sa.StateAreaName.IndexOf(term, StringComparison.CurrentCultureIgnoreCase) != -1
              orderby sa.StateAreaName
              select new AutoCompleteItem()
              {
                Id = sa.Id.ToString(),
                Label = sa.StateAreaName,
                Value = sa.StateAreaName
              }).ToArray();
    }

    /// <summary>
    /// Gets the degree education level auto complete items.
    /// </summary>
    /// <param name="term">The term.</param>
    /// <param name="stateAreaId">The state area id.</param>
    /// <param name="searchClientDataOnly">A flag indicating whether to search client data only.</param>
    /// <param name="DetailedDegreeLevel">The detailed degree level.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public AutoCompleteItem[] GetDegreeEducationLevelAutoCompleteItems(string term, long stateAreaId, bool? searchClientDataOnly, string DetailedDegreeLevel = "", int count = 10)
    {
      if (stateAreaId == 0) stateAreaId = ServiceClientLocator.ExplorerClient(_app).GetStateAreaDefault().Id.GetValueOrDefault();

      DetailedDegreeLevels detailedDegreeLevel;
      Enum.TryParse(DetailedDegreeLevel, true, out detailedDegreeLevel);

      var degreeEducationLevels = ServiceClientLocator.ExplorerClient(_app).GetDegreeAliases(stateAreaId, term, searchClientDataOnly, detailedDegreeLevel, count);

      return (from dc in degreeEducationLevels
              orderby dc.AliasExtended
              orderby dc.Alias
              select new AutoCompleteItem()
              {
                Id = dc.DegreeEducationLevelId.ToString(),
                Label = dc.AliasExtended,
                Value = dc.AliasExtended
              }).ToArray();
    }

    /// <summary>
    /// Gets the employer auto complete items.
    /// </summary>
    /// <param name="term">The term.</param>
    /// <param name="stateAreaId">The state area id.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public AutoCompleteItem[] GetEmployerAutoCompleteItems(string term, long stateAreaId)
    {

      if (stateAreaId == 0) stateAreaId = ServiceClientLocator.ExplorerClient(_app).GetStateAreaDefault().Id.GetValueOrDefault();

      var employers = ServiceClientLocator.ExplorerClient(_app).GetEmployers(stateAreaId, term);

      return (from e in employers
              orderby e.Name
              select new AutoCompleteItem()
              {
                Id = e.Id.Value.ToString(),
                Label = e.Name,
                Value = e.Name
              }).ToArray();
    }

    /// <summary>
    /// Gets the job auto complete items.
    /// </summary>
    /// <param name="term">The term.</param>
    /// <param name="stateAreaId">The state area id.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public AutoCompleteItem[] GetJobAutoCompleteItems(string term, long stateAreaId)
    {
      var jobs = ServiceClientLocator.ExplorerClient(_app).GetJobs(stateAreaId, term);

      return (from j in jobs
              orderby j.Name
              select new AutoCompleteItem()
              {
                Id = j.Id.Value.ToString(),
                Label = j.Name,
                Value = j.Name
              }).ToArray();
    }

    /// <summary>
    /// Gets the military occupations for an auto-complete text box
    /// </summary>
    /// <param name="term">A term to use when searching for the occupations</param>
    /// <returns>An array of AutoCompleteItems for the text box</returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public AutoCompleteItem[] GetMilitaryOccupationAutoCompleteItems(string term)
    {
      var militaryOccupations = ServiceClientLocator.OccupationClient(_app).GetMilitaryOccupationJobTitles(term, 100, true);

			var jobTitles = militaryOccupations.Select(m => m.JobTitle).Distinct();

	    var items = jobTitles.Select(jt => new AutoCompleteItem
						{
							Id = militaryOccupations.First(mo => mo.JobTitle == jt).MilitaryOccupationId.ToString(CultureInfo.InvariantCulture),
							Label = jt,
							Value = jt
						})
					.ToArray();

      return items;
    }

    /// <summary>
    /// Gets the skill complete items.
    /// </summary>
    /// <param name="term">A string to match against skill name</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public AutoCompleteItem[] GetSkillAutoCompleteItems(string term, int count = 50)
    {
      var skills = ServiceClientLocator.ExplorerClient(_app).GetSkillsForJobs(20, term, count);

      var relevantSkills = (from s in skills
              select new AutoCompleteItem()
              {
                Id = s.Id.ToString(),
                Label = s.Name,
                Value = s.Name
							}).ToArray();

	    return relevantSkills;
    }

    /// <summary>
    /// Gets the drop down areas
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetAreas(string knownCategoryValues, string category)
    {
      var areaValues = new List<CascadingDropDownNameValue>();

      long stateAreaId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out stateAreaId);

      if (stateAreaId > 0)
      {
        var locations = ServiceClientLocator.ExplorerClient(_app).GetAreas(stateAreaId);

        if (locations != null && locations.Count > 0)
          areaValues.AddRange(locations.Select(l => new CascadingDropDownNameValue(l.StateAreaName, l.Id.ToString())));
      }

      return areaValues.ToArray();
    }

    /// <summary>
    /// Gets the jobs related to a job title for the drop-down
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetRelatedJobs(string knownCategoryValues, string category)
    {
      var jobItems = new List<CascadingDropDownNameValue>();

      long jobTitleId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out jobTitleId);

      if (jobTitleId > 0)
      {
        var relatedJobs = ServiceClientLocator.ExplorerClient(_app).GetRelatedJobs(jobTitleId);

        if (relatedJobs.IsNotNullOrEmpty())
          jobItems.AddRange(relatedJobs.Select(j => new CascadingDropDownNameValue(j.Name, j.Id.ToString())));
      }

      return jobItems.ToArray();
    }

    /// <summary>
    /// Gets the jobs related to a military for the drop-down
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetJobsForMilitaryOccupation(string knownCategoryValues, string category)
    {
      var jobItems = new List<CascadingDropDownNameValue>();

      long militaryOccupationId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out militaryOccupationId);

      if (militaryOccupationId > 0)
      {
        var relatedJobs = ServiceClientLocator.ExplorerClient(_app).GetJobsForMilitaryOccupation(militaryOccupationId);

        if (relatedJobs.IsNotNullOrEmpty())
          jobItems.AddRange(relatedJobs.Select(j => new CascadingDropDownNameValue(j.Name, j.Id.ToString())));
      }

      return jobItems.ToArray();
    }

    /// <summary>
    /// Gets the skill sub categories.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetSkillSubCategories(string knownCategoryValues, string category)
    {
      var skillSubCategoryValues = new List<CascadingDropDownNameValue>();

      long parentSkillCategoryId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out parentSkillCategoryId);

      if (parentSkillCategoryId > 0)
      {
        var skillSubCategories = ServiceClientLocator.ExplorerClient(_app).GetSkillCategories(parentSkillCategoryId);

        if (skillSubCategories != null && skillSubCategories.Count > 0)
          skillSubCategoryValues.AddRange(skillSubCategories.Select(ca => new CascadingDropDownNameValue(ca.Name, ca.Id.ToString())));
      }

      return skillSubCategoryValues.OrderBy(x => x.name).ToArray();
    }

    /// <summary>
    /// Gets the explorer skills.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetExplorerSkills(string knownCategoryValues, string category)
    {
      var skillValues = new List<CascadingDropDownNameValue>();

      long parentSkillCategoryId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out parentSkillCategoryId);

      if (parentSkillCategoryId > 0)
      {
        var skills = ServiceClientLocator.ExplorerClient(_app).GetSkills(parentSkillCategoryId);

        if (skills != null && skills.Count > 0)
          skillValues.AddRange(skills.Select(ca => new CascadingDropDownNameValue(ca.Name, ca.Id.ToString())));
      }

      return skillValues.OrderBy(x => x.name).ToArray();
    }

    /// <summary>
    /// Gets the job skills.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetJobSkills(string knownCategoryValues, string category)
    {
      var skillValues = new List<CascadingDropDownNameValue>();

      long jobId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out jobId);

      if (jobId > 0)
      {
        var skills = ServiceClientLocator.ExplorerClient(_app).GetJobSkills(jobId);

        if (skills != null && skills.Count > 0)
          skillValues.AddRange(skills
            .Select(js => new { js.SkillName, js.SkillId })
            .Distinct()
            .Select(ca => new CascadingDropDownNameValue(ca.SkillName, ca.SkillId.ToString(CultureInfo.InvariantCulture))));
      }

      return skillValues.OrderBy(x => x.name).ToArray();
    }

    /// <summary>
    /// Gets the drop down areas
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns></returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetDegreesByProgramArea(string knownCategoryValues, string category)
    {
      long departmentId;
      long.TryParse(GetKnownCategoryValue(knownCategoryValues), out departmentId);

      var degreeValues = new List<CascadingDropDownNameValue> { new CascadingDropDownNameValue(CodeLocalise("Global.Degree.AllDegrees", "All Degrees"), string.Concat("-", departmentId)) };

      if (departmentId > 0)
      {
        var degrees = ServiceClientLocator.ExplorerClient(_app).GetProgramAreaDegrees(departmentId);

        if (degrees.IsNotNullOrEmpty())
          degreeValues.AddRange(degrees.Select(d => new CascadingDropDownNameValue(d.DegreeName, d.DegreeId.ToString(CultureInfo.InvariantCulture))));
      }

      return degreeValues.ToArray();
    }

    /// <summary>
    /// Gets the degree education levels by program area.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns>An array of cascading drop-down items</returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetDegreeEducationLevelsByProgramArea(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetDegreeEducationLevelsByProgramArea(knownCategoryValues, category);
    }

    /// <summary>
    /// Gets the degree education levels by degree level
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <param name="category">The category.</param>
    /// <returns>An array of cascading drop-down items</returns>
    [OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetDegreeEducationLevelsByDegreeLevel(string knownCategoryValues, string category)
    {
      return AjaxHelper.GetDegreeEducationLevelsByDegreeLevel(knownCategoryValues, category);
    }

		/// <summary>
		/// Gets jobs by career area.
		/// </summary>
		/// <param name="knownCategoryValues">The known category values.</param>
		/// <param name="category">The category.</param>
		/// <returns></returns>
		[OperationContract]
    [WebInvoke(ResponseFormat = WebMessageFormat.Json)]
    public CascadingDropDownNameValue[] GetJobsByCareerArea(string knownCategoryValues, string category)
		{
			return AjaxHelper.GetJobsByCareerAreaId(knownCategoryValues, category);
		}

    #endregion

    #region Localisation Item Update

    /// <summary>
    /// Updates the localisation item.
    /// </summary>
    /// <param name="key">The key.</param>
    /// <param name="localisedValue">The localised value.</param>
    /// <param name="defaultValue">The default value</param>
    /// <returns></returns>
    [OperationContract]
    public void UpdateLocalisationItem(string key, string localisedValue, string defaultValue)
    {
      var localisationModeHelper = new LocalisationModeHelper(_app);
      localisationModeHelper.SavePortalLocalisationItem(key, localisedValue, defaultValue);
    }

    #endregion

    /// <summary>
    /// Gets the known category value from the known category values.
    /// </summary>
    /// <param name="knownCategoryValues">The known category values.</param>
    /// <returns></returns>
    private static string GetKnownCategoryValue(string knownCategoryValues)
    {
      if (string.IsNullOrEmpty(knownCategoryValues)) return knownCategoryValues;
      if (knownCategoryValues.LastIndexOf(":") > 0) knownCategoryValues = knownCategoryValues.Substring(knownCategoryValues.LastIndexOf(":") + 1);
      if (knownCategoryValues.LastIndexOf(";") > 0) knownCategoryValues = knownCategoryValues.Substring(0, knownCategoryValues.LastIndexOf(";"));
      return knownCategoryValues;
		}

		#region Heartbeat

		/// <summary>
		/// Very simple "Heartbeat" service, used by Career/Explorer UI to ping the app.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[OperationContract]
		[WebInvoke(ResponseFormat = WebMessageFormat.Json)]
		public string HeartBeat(string value)
		{
			return "OK";
		}

		#endregion
		
		#region Timeout Logout

		/// <summary>
		/// Logs the user out following a timeout.
		/// </summary>
		[OperationContract]
		[WebInvoke(ResponseFormat = WebMessageFormat.Json)]
		private void LogOut()
		{
			try
			{
				if (_app.Settings.Module == FocusModules.Explorer)
				{
					ServiceClientLocator.AuthenticationClient(_app).LogOut();
				}
				else
				{
					var usercontext = _app.GetSessionValue<UserContext>("Career:UserContext");
					if (usercontext.IsNotNull())
					{
						#region Save Activity

						//TODO: Martha (new activity functionality)
						//var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
						//if (staffcontext.IsNull() || (staffcontext.IsNotNull() && !staffcontext.IsAuthenticated))
						//{
						//  var jobSeekerActivities = CareerCommon.ServiceClient.ServiceClientLocator.CoreClient(App).GetJobSeekerSessionActivities();
						//  if (jobSeekerActivities.IsNotNull())
						//  {
						//    string activityLog = string.Format(@"Session Duration: {0} minutes\nSearches: {1}\nJobs Viewed: {2}\nReferrals Requested: {3}\nResume Updated: {4}",
						//      Math.Ceiling(DateTime.Now.Subtract(jobSeekerActivities.SessionStartTime).TotalMinutes),
						//      jobSeekerActivities.SearchesMade,
						//      jobSeekerActivities.JobsViewed,
						//      jobSeekerActivities.ReferralsRequested,
						//      jobSeekerActivities.IsResumeUpdated);
						//    ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.JobSeeker, ActivityType.Automated, "SMRY01", activityLog);
						//    CareerCommon.ServiceClient.ServiceClientLocator.CoreClient(App).ClearJobSeekerSessionActivities();
						//  }
						#endregion
					}
						
					ServiceClientLocator.AuthenticationClient(_app).LogOut();
				}
			}
			finally
			{
				FormsAuthentication.SignOut();
				_app.ClearSession();
				if (_app.Settings.SSOEnabled || _app.Settings.SamlEnabledForCareer) _app.SetCookieValue("SSOSignOut", "true");
			}
		}

	  #endregion
	}
}
