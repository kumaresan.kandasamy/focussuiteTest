﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Linq;
using AjaxControlToolkit;

using Focus.CareerExplorer.Code;
using Focus.CareerExplorer.WebAuth.Controls;
using Focus.Common.Controls;
using Focus.Common.Localisation;
using Focus.Common;
using Focus.Common.Controls.Server;
using Focus.Core;
using Framework.Core;
using Focus.Common.Helpers;

#endregion

namespace Focus.CareerExplorer
{
    public partial class Site : MasterPageBase
    {
        internal bool? LoginEnabled { get; set; }

        private string _userName = string.Empty;
        private bool _isShadowingUser = false;
        private string _explorerTextSize = null;

        public delegate bool LogInCompleteHandler(object sender, CommandEventArgs eventArgs);
        public event LogInCompleteHandler LogInComplete;

        public delegate void LogInCancelledHandler(object sender, CommandEventArgs eventArgs);
        public event LogInCancelledHandler LogInCancelled;

        public delegate void LogOutCompleteHandler(object sender, EventArgs e);
        public event LogOutCompleteHandler LogOutComplete;

        public delegate void ProgramOfStudyChangedHandler(object sender, CommandEventArgs e);
        public event ProgramOfStudyChangedHandler ProgramOfStudyChanged;

        public string BodyClass
        {
            get { return string.Format("{0} {1} {2}", App.Settings.Theme, (Page.GetType().BaseType ?? Page.GetType()).Name, _explorerTextSize ?? App.GetCookieValue("Explorer:TextSize", "FocusSmall")); }
        }

        public string CoreStylesheetUrl
        {
            get { return UrlHelper.GetCacheBusterUrl(string.Format("~/Branding/{0}/Focus.CareerExplorer.css", App.Settings.BrandIdentifier)); }
        }

        public string DeviceStylesheetUrl
        {
            get { return UrlHelper.GetCacheBusterUrl(string.Format("~/Branding/{0}/Focus.Device.css", App.Settings.BrandIdentifier)); }
        }

        public GoogleAnalyticsScriptLiteral GoogleAnalyticsScriptLiteral
        {
            get { return GAScriptLiteral; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (App.User.IsAuthenticated)
            {
                var errorType = ServiceClientLocator.AuthenticationClient(App).ValidateSession();

                if (errorType.IsNotNull() && errorType != ErrorTypes.Ok)
                {
                    switch (errorType)
                    {
                        case ErrorTypes.UserBlocked:
                            App.SetCookieValue("MessageDisplay", "UserBlocked");
                            MainLogOut.LogOutSession();
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManagerMain.EnableCdn = App.Settings.EnableCDN;

            Page.Header.DataBind();

            if (App.User.IsAuthenticated)
                SetUpMasterPage(true, (App.User.ScreenName.IsNotNullOrEmpty()) ? App.User.ScreenName : App.User.UserName, App.User.IsShadowingUser);
            else
                SetUpMasterPage(false, "", false);

            // Need to establish what to show here but if there's a client logo then it should be fetched and listed here
            MainLogoImage.ImageUrl = UrlBuilder.MainLogoImage();

            // Set up the CSS
            SetUpCss();

            // Do we need to show burning glass logo
            FooterImage.Visible = App.Settings.ShowBurningGlassLogoInCareerExplorer;

            // Establish the correct 
            //FocusStyle.Text = "<link href=\"" + UrlBuilder.MainCss() + "\" rel=\"stylesheet\" type=\"text/css\" /> ";

            var message = App.GetCookieValue("MessageDisplay");
            App.RemoveCookieValue("MessageDisplay");
            if (message.IsNotNullOrEmpty())
            {
                switch (message)
                {
                    case "UserBlocked":
                        Confirmation.Show(
                            Localiser.Instance().Localise("AccountBlocked.Title", "Account on hold"),
                            Localiser.Instance().Localise("AccountBlocked.Message", "Your account has been placed on hold. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> for further assistance."),
                            "OK", "", "", "", "", "", 150, 300);
                        break;
                }
            }

            MainLogIn.Visible = MainLogIn.LoginEnabled = LoginEnabled ?? true;

			if (App.Settings.Theme == FocusThemes.Workforce && !App.Settings.HideLegalNoticeLink)
			{
				var category = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories).Where(x => x.Key.Equals("DocumentCategories.LegalNotice")).Select(x => x.Id).FirstOrDefault());
				LegalNoticeUrl.HRef = UrlBuilder.Notice(Uri.EscapeDataString(HtmlLocalise("LegalNotice.Title", "Legal Notices")),category);
			}
			else
			{
				LegalNoticePlaceHolder.Visible = false;
			}
		}

        /// <summary>
        /// Handles the PreRender event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            var script = string.Format("var SiteMaster_LoggedInUserName = '{0}', SiteMaster_IsShadowingUser = {1};", _userName, _isShadowingUser.ToString().ToLowerInvariant());
            Page.ClientScript.RegisterClientScriptBlock(GetType(), "SiteMaster_LoggedInUserName", script, true);
            System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallGetViewPort", "getViewPort();", true);
        }

        /// <summary>
        /// Handles the OnCommand event of the TextLinkButtons
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
        protected void TextLinkButton_Command(object sender, CommandEventArgs e)
        {
            _explorerTextSize = e.CommandName;
            App.SetCookieValue("Explorer:TextSize", _explorerTextSize);
            SetUpCss();
        }

        /// <summary>
        /// Handles the OnLoggedIn event of the MainLogIn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see>
        /// 	                    <cref>Focus.Web.Explorer.WebAuth.LogInOrRegister.LoggedInEventArgs</cref>
        ///                     </see> instance containing the event data.</param>
        protected bool MainLogIn_OnLoggedIn(object sender, LogInOrRegister.LoggedInEventArgs e)
        {
            SetUpMasterPage(true, e.ScreenName, e.IsShadowingUser);

            if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "FocusScript"))
                Page.ClientScript.RegisterStartupScript(GetType(), "FocusScript", "DisplaySessionTimeout();", true);
            return OnLogInComplete(new CommandEventArgs(e.SourceCommandName, null));
        }

        /// <summary>
        /// Handles the OnLoggedInCancelled event of the MainLogIn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void MainLogIn_OnLoggedInCancelled(object sender, CommandEventArgs e)
        {
            OnLogInCancelled(e);
        }

        /// <summary>
        /// Handles the OnLoggedOut event of the MainLogOut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void MainLogOut_OnLoggedOut(object sender, EventArgs e)
        {
            SetUpMasterPage(false, "", false);

            OnLogOutComplete(e);
        }

        protected void MyAccount_Updated(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "ProgramOfStudy" && App.Settings.Theme == FocusThemes.Education)
                OnProgramOfStudyChanged(new CommandEventArgs(e.CommandName, e.CommandArgument));
        }

        /// <summary>
        /// Gets the toolkit script manager.
        /// </summary>
        /// <returns></returns>
        protected override ToolkitScriptManager GetToolkitScriptManager()
        {
            return ScriptManagerMain;
        }


        /// <summary>
        /// Displays the error.
        /// </summary>
        /// <param name="alertType">Type of the alert.</param>
        /// <param name="message">The message.</param>
        protected override void DisplayError(AlertTypes alertType, string message = "")
        {
            DisplayError(message, "", 20, 400, 210);
        }

        /// <summary>
        /// Displays the error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="closeLink">The close link.</param>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        /// <param name="top">The top.</param>
        protected override void DisplayError(string message = "", string closeLink = "", int height = 20, int width = 400, int top = 210)
        {
            if (message.IsNullOrEmpty())
                AlertModal.Show(closeLink, height, width, top);
            else if (message == "This session is no longer valid. Please authenticate again.")
                MainLogOut.LogOutSession(alreadyLogout: true);
            else
                AlertModal.Show(message, closeLink, height, width);
        }

        /// <summary>
        /// Displays the modal alert.
        /// </summary>
        /// <param name="alertType">Type of the alert.</param>
        /// <param name="message">The message.</param>
        /// <param name="title">The title.</param>
        /// <param name="closeLink">The close link.</param>
        /// <param name="width">The width.</param>
        protected override void DisplayModalAlert(AlertTypes alertType, string message, string title = "", string closeLink = "", int width = 400)
        {
            AlertModal.Show(alertType, message, title, closeLink, 20, width);
        }

        /// <summary>
        /// Gets the module.
        /// </summary>
        /// <returns></returns>
        protected override FocusModules GetModule()
        {
            return App.Settings.Module;
        }

        /// <summary>
        /// Sets up master page.
        /// </summary>
        /// <param name="userIsAuthenticated">if set to <c>true</c> [user is authenticated].</param>
        /// <param name="userScreenName">Name of the user screen.</param>
        /// <param name="isShadowingUser">Whether this is an Assist user shadowing the career user</param>
        public void SetUpMasterPage(bool userIsAuthenticated, string userScreenName, bool isShadowingUser)
        {
            _userName = userScreenName;
            _isShadowingUser = isShadowingUser;

            UsernameSpeechBubblePanel.Visible = LoggedInControls.Visible = MainLogOut.Visible = userIsAuthenticated;

            if (App.Settings.ShowCareerTutorialLink && userIsAuthenticated)
            {
                TutorialHyperLink.NavigateUrl = UrlBuilder.Tutorial();
                TutorialHyperLink.Text = Localiser.Instance().Localise("TutorialHyperLink.Text", "Tutorial video");
            }
            else
                TutorialLink.Visible = false;

            if ((!String.IsNullOrEmpty(App.Settings.DemoShieldCustomerCode) && !DemoShieldValidated) || App.IsAnonymousAccess())
                LoginControls.Visible = false;
            else
                LoginControls.Visible = (!userIsAuthenticated);

            BookmarksPlaceHolder.Visible = !App.GuideToResume();

            var prefix = isShadowingUser
                ? HtmlLocalise("LoggedInAs.Literal", "Logged in as")
                : HtmlLocalise("Hi.Literal", "Hi,");

            HeadLoginName.Text = string.Concat(prefix, " ", userScreenName);

			MyAccountLink.Visible = App.Settings.MyAccountEnabled && userIsAuthenticated
        && !((App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer) && App.Settings.SSOManageAccountUrl.IsNullOrEmpty() && App.Settings.Theme == FocusThemes.Workforce)
				&& !App.Settings.HideCareerMyAccount;

            if (MyAccountLink.Visible)
            {
                MyAccountLinkButton.Text = App.Settings.UseUsernameAsAccountLink
                    ? string.Format("{0} {1}", App.User.FirstName, App.User.LastName)
                    : HtmlLocalise("MyAccount.LinkText", "My account");
            }

            RegisterLabel.Visible = !App.Settings.HideCareerRegister;
            LogInLabel.Visible = !App.Settings.HideCareerSignIn;

            LogOutLabel.Visible = !App.Settings.HideCareerSignOut;

            if (App.Settings.UseCustomHeaderHtml)
            {
                CustomHeader.InnerHtml = App.Settings.CustomHeaderHtml;
                HideStandardHeaderControls();
            }

            if (App.Settings.UseCustomFooterHtml)
                CustomFooter.InnerHtml = App.Settings.CustomFooterHtml;

        }

        private void HideStandardHeaderControls()
        {
            MainLogoImage.Visible = false;
            UsernameSpeechBubblePanel.Visible = false;
            HeadLoginName.Visible = false;
            SmallTextLinkButton.Visible = false;
            MediumTextLinkButton.Visible = false;
            LargeTextLinkButton.Visible = false;
            lnkTranslator.Visible = false;
        }

		protected void MyAccountLinkButton_Click(object sender, EventArgs e)
		{
      if ((App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer) && App.Settings.SSOManageAccountUrl.IsNotNullOrEmpty())
				Response.Redirect(App.Settings.SSOManageAccountUrl, false);
            //AIM-FVN-4914-To avoid the System Error,Checked whether the user is authenticated.
            else if (App.User.IsAuthenticated)
                MyAccountpopup.Bind();
            else
                Response.Redirect("Home");
	 }

        /// <summary>
        /// Sets up CSS.
        /// </summary>
        public void SetUpCss()
        {
            SmallTextLinkButton.Enabled = MediumTextLinkButton.Enabled = LargeTextLinkButton.Enabled = true;
            switch (_explorerTextSize ?? App.GetCookieValue("Explorer:TextSize", "FocusSmall"))
            {
                case "FocusSmall":
                    //FocusCssLiteral.Text = String.Format(@"<link href=""{0}"" rel=""stylesheet"" type=""text/css"" />",
                    //                                     UrlBuilder.FocusSmallCss());
                    // remove "font-size" stylesheet if they select small, otherwise if they have previously selected eg. Large, then switched back to Small, "Large" stylesheet will still be loaded
                    FocusCssLiteral.Text = "";
                    SmallTextLinkButton.Enabled = false;

                    break;

                case "FocusMedium":
                    FocusCssLiteral.Text = String.Format(@"<link href=""{0}"" rel=""stylesheet"" type=""text/css"" />", UrlHelper.GetCacheBusterUrl(UrlBuilder.FocusMediumCss()));
                    MediumTextLinkButton.Enabled = false;

                    break;

                case "FocusLarge":
                    FocusCssLiteral.Text = String.Format(@"<link href=""{0}"" rel=""stylesheet"" type=""text/css"" />", UrlHelper.GetCacheBusterUrl(UrlBuilder.FocusLargeCss()));
                    LargeTextLinkButton.Enabled = false;

                    break;
            }
            const string headerStyles = @"
<style>#header {{background:#{0}; position:relative; display:block;}} #headerTopWrap {{background:#{1}; padding:8px 0;}}</style>
";
            FocusStyle.Text = string.Format(headerStyles, App.Settings.CareerExplorerLightColour, App.Settings.CareerExplorerDarkColour);
        }

        /// <summary>
        /// Shows the log in modal
        /// </summary>
        public void ShowLogIn(string sourceCommandName)
        {
            MainLogIn.ShowLogIn(sourceCommandName);
        }

        /// <summary>
        /// Displays the change office modal.
        /// </summary>
        protected override void ShowChangeOfficeModal(Object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Shows the Terms and Conditions modal
        /// </summary>
        public void ShowTermsAndConditions()
        {
            MainLogIn.ShowAgreeToTerms();
        }

        public void ShowAppropriateWindow(bool isNewUser = false)
        {
            MainLogIn.ShowAppropriateWindow(isNewUser);
        }

        /// <summary>
        /// Raises the <see cref="E:LogInComplete"/> event.
        /// </summary>
        /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        protected virtual bool OnLogInComplete(CommandEventArgs eventArgs)
        {
            var redirectionOverride = false;
            if (LogInComplete != null)
                redirectionOverride = LogInComplete(this, eventArgs);

            return redirectionOverride;
        }

        /// <summary>
        /// Raises the <see cref="E:LogInCancelled"/> event.
        /// </summary>
        /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        protected virtual void OnLogInCancelled(CommandEventArgs eventArgs)
        {
            if (LogInCancelled != null)
                LogInCancelled(this, eventArgs);
        }

        /// <summary>
        /// Raises the <see cref="E:LogOutComplete"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void OnLogOutComplete(EventArgs e)
        {
            if (LogOutComplete != null)
                LogOutComplete(this, e);
        }


        /// <summary>
        /// Raises the <see cref="E:ProgramOfStudyChanged" /> event.
        /// </summary>
        /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
        protected virtual void OnProgramOfStudyChanged(CommandEventArgs e)
        {
            if (ProgramOfStudyChanged != null)
                ProgramOfStudyChanged(this, e);
        }

        /// <summary>
        /// Shows the register modal.
        /// </summary>
        /// <param name="sourceCommandName"></param>
        public void ShowRegister(string sourceCommandName)
        {
            MainLogIn.ShowRegister(sourceCommandName);
        }
    }

    //Derive a StringWriter class with encoding feature from StringWriter class
    public class StringWriterWithEncoding : StringWriter
    {
        private readonly System.Text.Encoding _myEncoding;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringWriterWithEncoding"/> class.
        /// </summary>
        /// <param name="encoding">The encoding.</param>
        public StringWriterWithEncoding(System.Text.Encoding encoding)
        {
            _myEncoding = encoding;
        }

        /// <summary>
        /// Gets the <see cref="T:System.Text.Encoding"/> in which the output is written.
        /// </summary>
        /// <value></value>
        /// <returns>The Encoding in which the output is written.</returns>
        public override System.Text.Encoding Encoding
        {
            get { return _myEncoding; }
        }
    }
}
