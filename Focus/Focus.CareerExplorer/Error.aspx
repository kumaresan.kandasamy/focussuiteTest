﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
	CodeBehind="Error.aspx.cs" Inherits="Focus.CareerExplorer.Error" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<h2>
		<focus:LocalisedLabel runat="server" ID="HeadingLabel" LocalisationKey="ErrorType.Heading" DefaultText="System  Error:" RenderOuterSpan="False"/> 
		<asp:Literal runat="server" ID="ErrorInfoLiteral" EnableViewState="False" />
	</h2>
	<p><asp:Literal runat="server" ID="ErrorStackLiteral" EnableViewState="False"/></p>
	<ul>
		<li id="liHomeURL" runat="server" EnableViewState="False">
		<a href="<%= UrlBuilder.Default() %>">Click here</a> to return
			to home page. 
			</li>
	</ul>
</asp:Content>
