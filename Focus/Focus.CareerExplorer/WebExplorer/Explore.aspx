﻿<%@ Page Title="Explorer - Explorer" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Explore.aspx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Explore"
    EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="Controls/StateAreaSelector.ascx" TagName="StateAreaSelector" TagPrefix="uc" %>
<%@ Register Src="Controls/JobTitleSelector.ascx" TagName="JobTitleSelector" TagPrefix="uc" %>
<%@ Register Src="Controls/JobModal.ascx" TagName="JobModal" TagPrefix="uc" %>
<%@ Register Src="Controls/EmployerModal.ascx" TagName="EmployerModal" TagPrefix="uc" %>
<%@ Register Src="Controls/DegreeCertificationModal.ascx" TagName="DegreeCertificationModal"
    TagPrefix="uc" %>
<%@ Register Src="Controls/ProgramAreaDegreeStudyModal.ascx" TagName="ProgramAreaDegreeStudyModal"
    TagPrefix="uc" %>
<%@ Register Src="Controls/SkillModal.ascx" TagName="SkillModal" TagPrefix="uc" %>
<%@ Register Src="Controls/EmailModal.ascx" TagName="EmailModal" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ConfirmationModal.ascx" TagName="ConfirmationModal"
    TagPrefix="uc" %>
<%@ Register Src="Controls/InternshipModal.ascx" TagName="InternshipModal" TagPrefix="uc" %>
<%@ Register Src="Controls/CareerAreaModal.ascx" TagName="CareerAreaModal" TagPrefix="uc" %>
<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="../Controls/UnAuthorizedInformation.ascx" TagName="UnAuthorizedModal" TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script language="javascript" type="text/javascript">
		// Set order of explore div sections
		$(document).ready(function () {
			//var exploreDivs = ["ExploreSection", "ResearchSection", "StudySection"];
			var exploreSections = <%= ExploreSections %>;
			for (var i = 0; i < exploreSections.length; i++) {
			  var exploreDiv = exploreSections[i] + "Section";
				$("#" + exploreDiv).appendTo("#ExploreSections");
	  		$("#" + exploreDiv).show();
			}
		});
		
    function ResearchJobTitleSelectorValidateShowJobs(textBoxId, dropDownId, serviceUrl, category, selectingText, loadingText) {
      var selectedValue = $('#' + textBoxId + '_SelectedValue').val();
      var selectedIndex = -1;

      var row = $('#' + textBoxId + 'JobRow');
      if (selectedValue.length > 0)
        row.show();
      else
        row.hide();

      LoadCascadingDropDownList(selectedValue, dropDownId, serviceUrl, category, selectingText, loadingText, selectedIndex);
    }
    </script>
    <asp:Literal ID="GoogleTrackingLiteral" runat="server" />
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div style="float: right; position: relative; bottom: 20px; clear: both;">
        <table role="presentation">
            <tr>
                <td valign="top">
                    <%= HtmlInFieldLabel("SearchTermTextBox", "SearchCareersAndEducation.Label", "Search careers and education", 300)%>
                    <asp:TextBox ID="SearchTermTextBox" runat="server" ClientIDMode="Static" Width="250px" />
                    <div style="position: absolute;">
                        <asp:RequiredFieldValidator ID="SearchTermRequired" runat="server" ControlToValidate="SearchTermTextBox"
                            CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="SiteSearch" />
                        <asp:CustomValidator ID="SearchTermValidator" runat="server" CssClass="error" ClientValidationFunction="SiteSearchTerm_Validate"
                            SetFocusOnError="true" Display="Dynamic" ValidationGroup="SiteSearch" ValidateEmptyText="False"></asp:CustomValidator>
                    </div>
                </td>
                <td valign="top">
                    <asp:Button ID="SearchGoButton" runat="server" ClientIDMode="Static" CausesValidation="True"
                        OnClick="SearchGoButton_Click" class="buttonLevel2" ValidationGroup="SiteSearch" />
                </td>
            </tr>
        </table>
    </div>
		<br class="clear" />
    <div class="landingPageNavigation explore accordion">
        <div id="ExploreSections">
        </div>
        <div id="SchoolSection" style="display: none;">
            <div id="schoolSectionTitle" class="multipleAccordionTitle careersAndEducation">
                <table role="presentation">
                    <tr>
                        <td class="column1">
                            <img src="<%= UrlBuilder.ExploreSectionSchoolImage() %>" alt="Explore your careers by Programs of Study"
                                width="50" height="51" />
                        </td>
                        <td class="column2">
                            <span class="largeHeading"><a href="#">
                                <%= HtmlLocalise("Study.Label", "Explore careers by #SCHOOLNAME# programs of study")%></a></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="schoolSectionContent" class="accordionContent careersAndEducation">
                <table width="364" role="presentation">
                    <tr>
                        <td colspan="2">
                            <asp:Panel ID="SchoolPanel" runat="server" ClientIDMode="Static">
                                <table class="accordionContentTable" role="presentation">
                                    <tr>
                                        <td colspan="2">
                                            <span class="promoStyle1">
                                                <%= HtmlLocalise("ExploreGeographySelection.Label", "Where would you like to look?")%></span><span
                                                    class="promoStyle1Arrow"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <uc:StateAreaSelector ID="SchoolStateAreaSelector" runat="server" StateBeforeHtml="<table role=&quot;presentation&quot;><tr><td>" StateAfterHtml="</td></tr>"
                                                AreaBeforeHtml="<tr><td>" AreaAfterHtml="</td></tr></table>" StateAreaValidationGroup="SchoolGo" />
                                        </td>
                                        <td>
                                            <asp:Button ID="SchoolGoButton" runat="server" CausesValidation="True" OnCommand="ExploreGoButton_Click"
                                                CommandArgument="School" class="buttonLevel2" ValidationGroup="SchoolGo" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="ExploreSection" style="display: none;">
            <div id="exploreSectionTitle" class="multipleAccordionTitle careersAndEducation">
                <table role="presentation">
                    <tr>
                        <td class="column1">
                            <img src="<%= UrlBuilder.ExploreSectionExploreImage() %>" alt="Explore my Career and internship options"
                                width="50" height="51" />
                        </td>
                        <td class="column2">
                            <span class="largeHeading"><a href="#">
                                <%= HtmlLocalise("Explore.Label", "Explore my career and internship options")%></a></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="exploreSectionContent" class="accordionContent careersAndEducation">
                <table width="100%" role="presentation">
                    <tr>
                        <td>
                            <asp:Panel ID="ExplorePanel" runat="server" ClientIDMode="Static">
                                <table class="accordionContentTable" role="presentation" width="60%">
                                    <tr>
                                        <td>
                                            <span class="promoStyle1">
                                                <%= HtmlLocalise("ExploreGeographySelection.Label", "Where would you like to look?")%></span><span
                                                    class="promoStyle1Arrow"></span>
                                        </td>
                                        <td align="right" valign="middle">
                                            <span class="steppedProcessSubhead">
                                                <asp:Literal runat="server" ID="ExploreStep1Text"></asp:Literal></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <uc:StateAreaSelector ID="ExploreStateAreaSelector" runat="server" StateBeforeHtml="<table role=&quot;presentation&quot;><tr><td>" StateAfterHtml="</td></tr>"
                                                AreaBeforeHtml="<tr><td>" AreaAfterHtml="</td></tr></table>" StateAreaValidationGroup="ExploreGo" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="ExploreGoButton" runat="server" CausesValidation="True" OnCommand="ExploreGoButton_Click"
                                                CommandArgument="Explore" class="buttonLevel2" ValidationGroup="ExploreGo" /><asp:Button
                                                    ID="ExploreNextButton" runat="server" CausesValidation="True" class="buttonLevel2"
                                                    ValidationGroup="ExploreGo" />
                                        </td>
                                    </tr>
                                </table>
                                <div class="divider">
                                </div>
                                <asp:Panel ID="ExploreStep2Panel" runat="server">
                                    <table class="accordionContentTable" width="60%" role="presentation">
                                        <tr>
                                            <td>
                                                <span class="promoStyle2">2<span class="content"><%= HtmlLocalise("ExploreCareerInternshipsStep2.Label", "What jobs would you like to see?")%></span></span><span
                                                    class="promoStyle2Arrow"></span>
                                            </td>
                                            <td align="right" valign="middle">
                                                <span class="steppedProcessSubhead">
                                                    <%= HtmlLocalise("ResearchStep2Of2.Label", "<strong>Step 2</strong> of 2")%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="selectionTable" role="presentation">
                                                    <asp:PlaceHolder runat="server" ID="ExploreAllJobsPlaceHolder">
                                                        <tr>
                                                            <td>
                                                                <asp:RadioButton ID="ExploreAllJobsRadioButton" runat="server" GroupName="ExploreChoiceList"
                                                                    Checked="True" />
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </asp:PlaceHolder>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="ExploreByCareerAreaRadionButton" runat="server" GroupName="ExploreChoiceList" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="CareerAreaDropDownList" runat="server" Width="348" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:RadioButton ID="ExploreInternshipsRadioButton" runat="server" GroupName="ExploreChoiceList" />
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:Button ID="ExploreStep2GoButton" OnClick="ExploreStep2GoButton_Click" runat="server"
                                                    CausesValidation="False" class="buttonLevel2" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="ResearchSection" style="display: none;">
            <div id="researchSectionTitle" class="multipleAccordionTitle careersAndEducation">
                <table role="presentation">
                    <tr>
                        <td class="column1">
                            <img src="<%= UrlBuilder.ExploreSectionResearchImage() %>" alt="Research a specific program of study, career or employer"
                                width="50" height="51" />
                        </td>
                        <td class="column2">
                            <asp:HyperLink runat="server" ID="ResearchHeadingLabel" CssClass="largeHeading" href="#" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="researchSectionContent" class="accordionContent careersAndEducation">
                <asp:Panel ID="ResearchPanel" runat="server" ClientIDMode="Static">
                    <table class="accordionContentTable" width="60%" role="presentation">
                        <tr>
                        <td>
                            <div class="col-md-12">
                            <div class="col-md-9">
                                <span class="promoStyle1">1<span class="explorercontent"><%= HtmlLocalise("ResearchGeographySelection.Label", "Where would you like to look?")%></span></span><span
                                    class="promoStyle2Arrow"></span></div>
                                <div class="col-md-3" align="right">
                                <span class="steppedProcessSubhead">
                                    <%= HtmlLocalise("ResearchStep1Of2.Label", "<strong>Step 1</strong> of 2")%></span>
                                    </div>
                            </div>
                           </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc:StateAreaSelector ID="ResearchStateAreaSelector" runat="server" StateBeforeHtml="<table role=&quot;presentation&quot;><tr><td>" StateAfterHtml="</td></tr>"
                                    AreaBeforeHtml="<tr><td colspan='2'>"  AreaAfterHtml="</td></tr></table>" StateAreaValidationGroup="ResearchNext" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="ResearchNextButton" runat="server" CausesValidation="True" class="buttonLevel2"
                                    ValidationGroup="ResearchNext" />
                            </td>
                        </tr>
                    </table>
                    <div class="divider">
                    </div>
                    <asp:Panel ID="ResearchStep2Panel" runat="server" Width="100%">
                        <table class="accordionContentTable" width="65%" role="presentation">
                            <tr>
                                <td>
                                <div class="col-md-12">
                                    <div class="col-md-9">
                                    <span class="promoStyle1"><span class="bold">2</span> <span class="explorercontent">
                                        <%= HtmlLocalise("ResearchTypeSelection.Label", "What would you like to research?")%></span></span><span
                                            class="promoStyle2Arrow"></span>
                                </div>
                                <div class="col-md-3" align="right">
                                    <span class="steppedProcessSubhead">
                                        <%= HtmlLocalise("ResearchStep2Of2.Label", "<strong>Step 2</strong> of 2")%></span>
                                </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#ffffff">
                                    <div class="selectionTable" id="ResearchStep2Content" role="presentation">
                                        <div id="ResearchCareerRow" runat="server">
                                            <asp:RadioButton ID="ResearchCareerRadioButton" runat="server" GroupName="ResearchType"
                                                    Checked="True" />
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="col-md-5 col-sm-5">
                                                <asp:RadioButton ID="ResearchStudyRadioButton" runat="server" GroupName="ResearchType" />
                                            </div>
                                            <div class="col-md-7 col-sm-7">
                                                <asp:Panel runat="server" ID="ResearchStudyTypeButtons">
                                                    <asp:RadioButton ID="ResearchStudyByDeptRadioButton" runat="server" GroupName="ResearchStudyType" />
                                                     <asp:RadioButton ID="ResearchStudyByDegreeRadioButton" runat="server" GroupName="ResearchStudyType" />
                                                    <br />
                                                </asp:Panel>
                                                <div id="StudyProgramAreaLists" runat="server">
                                                    <asp:DropDownList runat="server" ID="StudyProgramAreaDropDownList" Width="248" Title="A Progam area of study"/>
                                                    <br />
                                                    <asp:DropDownList runat="server" ID="StudyProgramAreaDegreesDropDownList" Width="248" Title="Degree Program area of study"/>
                                                    <br />
                                                </div>
                                                <div id="StudyDegreeLists" runat="server">
                                                    <asp:DropDownList runat="server" ID="DetailedDegreeLevelDropDownList" Width="260" style="margin-bottom:5px" Enabled="False" Title="Degree Program Of study"/>
                                                    <br />
                                                    <asp:DropDownList runat="server" ID="DetailedDegreeDropDownList" Width="260" Visible="False"/> 
                                                    <focus:AutoCompleteTextBox ID="ResearchStudyAutoCompleteTextBox" runat="server" AutoCompleteType="Disabled"
                                                         Width="260" style="margin-bottom:5px" Enabled="False" CssClass="explorerfield" />
                                                    <br />
                                                    <asp:Literal ID="ResearchStudyClientOnlyLiteral" runat="server" />
                                                    <asp:CheckBox ID="ResearchStudyClientOnlyCheckBox" runat="server" />
                                                    <asp:CustomValidator ID="ResearchStudyValidator" runat="server" CssClass="error ResearchValidator" EnableClientScript="True" ClientValidationFunction="ResearchStudy_Validate"
                                                   SetFocusOnError="true" Display="Dynamic" ValidationGroup="ResearchGroup" Text="Please select a degree" ValidateEmptyText="True"></asp:CustomValidator>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <asp:PlaceHolder runat="server" ID="ResearchJobPlaceHolder">
                                            <div class="col-md-5 col-sm-5" style="vertical-align: top">
                                                <asp:RadioButton ID="ResearchJobRadioButton" runat="server" GroupName="ResearchType" ></asp:RadioButton>
                                            </div>
                                            <div class="col-md-7 col-sm-7">
                                                <focus:AutoCompleteTextBox ID="ResearchJobTitleAutoCompleteTextBox" runat="server"
                                                    Width="260" style="margin-bottom:5px" AutoCompleteType="Disabled" Enabled="False" CssClass="explorerfield" />
                                                <div id="ResearchJobTitleAutoCompleteTextBoxJobRow" runat="server" style="display: none">
                                                    <asp:DropDownList ID="ResearchJobTitleAutoCompleteRelatedJobs" runat="server" Width="260" Title="Research Job Title"/>
                                                    <asp:CustomValidator ID="ResearchJobTitleValidator" runat="server" CssClass="error ResearchValidator" EnableClientScript="True" ClientValidationFunction="ResearchJob_Validate"
                                                   SetFocusOnError="true" Display="Dynamic" ValidationGroup="ResearchGroup" Text="Please select a job" ValidateEmptyText="True"></asp:CustomValidator>
                                                </div>
                                          </div>
                                        </asp:PlaceHolder>
                                    </div>
                                <div class="col-md-12 col-sm-12">
                                    <asp:PlaceHolder runat="server" ID="ResearchEmployerPlaceHolder">
                                        <div class="col-md-5 col-sm-5">
                                                <asp:RadioButton ID="ResearchEmployerRadioButton" runat="server" GroupName="ResearchType" />
                                         </div>
                                         <div class="col-md-7 col-sm-7">
                                             <focus:AutoCompleteTextBox ID="ResearchEmployerAutoCompleteTextBox" runat="server"
                                                 AutoCompleteType="Disabled" Width="260" Enabled="False" CssClass="explorerfield" />
                                              <asp:CustomValidator ID="ResearchEmployerValidator" runat="server" CssClass="error ResearchValidator" EnableClientScript="True" ClientValidationFunction="ResearchEmployer_Validate"
                                                   SetFocusOnError="true" Display="Dynamic" ValidationGroup="ResearchGroup" Text="Please select an employer" ValidateEmptyText="True"></asp:CustomValidator>
                                          </div>
                                     </asp:PlaceHolder>
                                </div>
                                 <div id="ResearchSkillRow" runat="server">
                                            <div>
                                                <asp:RadioButton ID="ResearchSkillRadioButton" runat="server" GroupName="ResearchType" />
                                            </div>
                                            <div>
                                                <focus:AutoCompleteTextBox ID="ResearchSkillAutoCompleteTextBox" runat="server" AutoCompleteType="Disabled"
                                                    Width="260" Enabled="False" CssClass="explorerfield" />
                                            </div>
                                            <div>
                                              <asp:CustomValidator ID="ResearchSkillValidator" runat="server" CssClass="error ResearchValidator" EnableClientScript="True" ClientValidationFunction="ResearchSkill_Validate"
                                                   SetFocusOnError="true" Display="Dynamic" ValidationGroup="ResearchGroup" Text="Please select a skill" ValidateEmptyText="True"></asp:CustomValidator>
                                            </div>
                                  </div>
                             </div>
                               </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="ResearchGoButton" runat="server" CausesValidation="True" OnClick="ResearchGoButton_Click"
                                        class="buttonLevel2" ValidationGroup="ResearchGroup" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </div>
        <div id="StudySection" style="display: none;">
            <div id="studySectionTitle" class="multipleAccordionTitle careersAndEducation">
                <table role="presentation">
                    <tr>
                        <td class="column1">
                            <img src="<%= UrlBuilder.ExploreSectionStudyImage() %>" alt="What can I study to get ahead"
                                width="50" height="51" />
                        </td>
                        <td class="column2">
                            <span class="largeHeading"><a href="#">
                                <%= HtmlLocalise("Study.Label", "See what I can study to get ahead")%></a></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="studySectionContent" class="accordionContent careersAndEducation">
                <asp:Panel ID="StudyPanel" runat="server" ClientIDMode="Static">
                    <table class="accordionContentTable" width="60%" role="presentation">
                        <tr>
                            <td>
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <span class="promoStyle1">1<span class="explorercontent"><%= HtmlLocalise("StudyGeographySelection.Label", "Where would you like to look?")%></span></span><span
                                    class="promoStyle2Arrow"></span></div>
                            <div class="col-md-3" align="right">
                                <span class="steppedProcessSubhead">
                                    <%= HtmlLocalise("StudyStep1Of2.Label", "<strong>Step 1</strong> of 2")%></span>
                            </div>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc:StateAreaSelector ID="StudyStateAreaSelector" runat="server" StateBeforeHtml="<table role=&quot;presentation&quot;><tr><td>" StateAfterHtml="</td></tr>"
                                    AreaBeforeHtml="<tr><td colspan='2'>"  AreaAfterHtml="</td></tr></table>" StateAreaValidationGroup="StudyNext" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="StudyNextButton" runat="server" CausesValidation="True" class="buttonLevel2"
                                    ValidationGroup="StudyNext" />
                            </td>
                        </tr>
                    </table>
                    <div class="divider">
                    </div>
                    <asp:Panel ID="StudyStep2Panel" runat="server">
                        <table class="accordionContentTable" width="60%" role="presentation">
                            <tr>
                                <td>
                                <div class="col-md-12">
                                    <div class="col-md-9">
                                    <span class="promoStyle1"><span class="bold">2</span> <span class="explorercontent">
                                        <%=HtmlLocalise("StudyTypeSelection.Label", "What would you like to see?")%></span>
                                    </span><span class="promoStyle2Arrow"></span></div>
                                  <div class="col-md-3" align="right">
                                    <span class="steppedProcessSubhead"><strong>Step 2</strong> of 2</span>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="selectionTable" role="presentation">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="StudyDegreeRadioButton" runat="server" GroupName="StudyType"
                                                        Checked="True" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="StudySkillRadioButton" runat="server" GroupName="StudyType" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="StudyGoButton" runat="server" CausesValidation="False" OnClick="StudyGoButton_Click"
                                        class="buttonLevel2" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </div>
        <div id="ExperienceSection" style="display: none;">
            <div id="experienceSectionTitle" class="multipleAccordionTitle careersAndEducation">
                <table role="presentation">
                    <tr>
                        <td class="column1">
                            <img src="<%= UrlBuilder.ExploreSectionExperienceImage() %>" alt="Where my experience takes me"
                                width="50" height="51" />
                        </td>
                        <td class="column2">
                            <span class="largeHeading"><a href="#">
                                <%= HtmlLocalise("Experience.Label", "See where my experience can take me")%></a></span>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="experienceSectionContent" class="accordionContent careersAndEducation">
                <asp:Panel ID="ExperiencePanel" runat="server" ClientIDMode="Static">
                    <table class="accordionContentTable" width="60%" role="presentation">
                        <tr>
                            <td>
                              <div class="col-md-12">
                                    <div class="col-md-9">
                                    <span class="promoStyle1">1<span class="explorercontent"><%= HtmlLocalise("ExperienceGeographySelection.Label", "Where would you like to look?")%></span></span><span
                                    class="promoStyle2Arrow"></span></div>
                                    <div class="col-md-3" align="right">
                                        <span class="steppedProcessSubhead">
                                            <%= HtmlLocalise("ExperienceStep1Of2.Label", "<strong>Step 1</strong> of 2")%></span>
                                     </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc:StateAreaSelector ID="ExperienceStateAreaSelector" runat="server" StateBeforeHtml="<table role=&quot;presentation&quot;><tr><td>" StateAfterHtml="</td></tr>"
                                    AreaBeforeHtml="<tr><td colspan='2'>" AreaAfterHtml="</td></tr></table>" StateAreaValidationGroup="ExperienceGo" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="ExperienceNextButton" runat="server" CausesValidation="True" class="buttonLevel2"
                                    ValidationGroup="ExperienceGo" />
                            </td>
                        </tr>
                    </table>
                    <div class="divider">
                    </div>
                    <asp:Panel ID="ExperienceStep2Panel" runat="server" Style="display: none;">
                        <table class="accordionContentTable" width="65%" role="presentation">
                            <tr>
                                <td>
                                    <div class="col-md-12">
                                        <div class="col-md-9">
                                            <span class="promoStyle1">
	                                            <span class="bold">2</span> 
										           <span class="explorercontent">
												       <focus:LocalisedLabel runat="server" ID="CurrentJobTitleMocCodeLabel" ClientIDMode="Static" DefaultText="What's your current job title/MOC code?"/>
										            </span>
                                                </span>
										            <span class="promoStyle2Arrow"></span>
                                        </div>
                                    <div class="col-md-3" align="right">
                                        <span class="steppedProcessSubhead"><strong>Step 2</strong> of 2</span>
                                    </div>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="background:#ffffff">
                                    <uc:JobTitleSelector ID="ExperienceJobTitleSelector" runat="server" SelectorValidationGroup="ExperienceGo"
                                        ShowJobsDropDownForJobTitles="True" ShowJobsDropDownForMilitaryOccupation="False" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="ExperienceGoButton" runat="server" CausesValidation="True" ValidationGroup="ExperienceGo"
                                        OnCommand="ExploreGoButton_Click" CommandArgument="Experience" class="buttonLevel2" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="SelectedExploreSection" runat="server" />
    <%-- Modals --%>
    <uc:JobModal ID="Job" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked"
        OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
    <uc:EmployerModal ID="Employer" runat="server" OnItemClicked="Modal_ItemClicked"
        OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked"
        OnPrintClicked="Modal_PrintClicked" />
    <uc:DegreeCertificationModal ID="DegreeCertification" runat="server" OnItemClicked="Modal_ItemClicked"
        OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked"
        OnPrintClicked="Modal_PrintClicked" />
    <uc:ProgramAreaDegreeStudyModal ID="ProgramAreaDegreeStudy" runat="server" OnItemClicked="Modal_ItemClicked"
        OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked"
        OnPrintClicked="Modal_PrintClicked" />
    <uc:SkillModal ID="Skill" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked"
        OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
    <uc:EmailModal ID="Email" runat="server" OnCancelled="Email_Cancelled" OnCompleted="Email_Completed" />
    <uc:ConfirmationModal ID="Confirmation" runat="server" OnCloseCommand="Confirmation_CloseCommand" />
		<uc:UnAuthorizedModal ID="UnAuthorizedModal" runat="server" />
    <uc:InternshipModal ID="Internship" runat="server" OnItemClicked="Modal_ItemClicked"
        OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked"
        OnPrintClicked="Modal_PrintClicked" />
    <uc:CareerAreaModal ID="CareerArea" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked"
        OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
    <script language="javascript" type="text/javascript">
        // following script will rebind tooltips after update panel postback
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            BindTooltips();
           });
           Sys.Application.add_load(function () {
           	setTimeout(function () {
           		if (!($('#<%=ResearchStudyRadioButton.ClientID %>').is(':checked'))) {
           			DisableStyledDropDown('#<%=StudyProgramAreaDropDownList.ClientID %>', true);
           			DisableStyledDropDown('#<%=StudyProgramAreaDegreesDropDownList.ClientID %>', true);
           			DisableStyledDropDown('#<%=DetailedDegreeLevelDropDownList.ClientID %>', true);
           			DisableStyledDropDown('#<%=DetailedDegreeDropDownList.ClientID %>', true);
           			$('#<%=ResearchStudyTypeButtons.ClientID %> input[type="radio"]').prop('disabled', 'disabled');
           		}
           		$('#ResearchStep2Content input[type="text"]').each(function () {
           			if ($(this).is(':disabled')) {
           				$(this).prev('.inFieldLabel').css('opacity', '0.4');
           			}
           		});
           	}, 500);
           });

           function ResearchStudy_Validate(src, args) {
             args.IsValid = true;
             var selectedId;
             if ($('#<%=ResearchStudyRadioButton.ClientID %>').prop('checked')) {
               if ($('#<%=ResearchStudyByDegreeRadioButton.ClientID %>').prop('checked') || $('#<%=ResearchStudyByDeptRadioButton.ClientID %>').length == 0) {
                 if ($('#<%=ResearchStudyAutoCompleteTextBox.SelectedValueClientId %>').length == 0) {
                   selectedId = $('#<%=DetailedDegreeDropDownList.ClientID %>').val();
                   if (!selectedId || selectedId.length == 0) {
                     $('#<%=DetailedDegreeDropDownList.ClientID %>').focus();
                     args.IsValid = false;
                   }
                 }
                 else {
                   selectedId = $('#<%=ResearchStudyAutoCompleteTextBox.SelectedValueClientId %>').val();
                   if (!selectedId || selectedId.length == 0) {
                     $('#<%=ResearchStudyAutoCompleteTextBox.ClientID %>').focus();
                     args.IsValid = false;
                   }
                 }
               }
               if ($('#<%=ResearchStudyByDeptRadioButton.ClientID %>').prop('checked')) {
                 selectedId = $('#<%=StudyProgramAreaDegreesDropDownList.ClientID %>').val();
                 if (!selectedId || selectedId.length == 0) {
                   $('#<%=StudyProgramAreaDegreesDropDownList.ClientID %>').focus();
                   args.IsValid = false;
                 }
               }
             }
           }

           function ResearchJob_Validate(src, args) {
             args.IsValid = true;
             if ($('#<%=ResearchJobRadioButton.ClientID%>').prop('checked')) {
               var selectedId = $('#<%=ResearchJobTitleAutoCompleteRelatedJobs.ClientID%>').val();
               if (!selectedId || selectedId.length == 0) {
                 $('#<%=ResearchJobTitleAutoCompleteTextBox.ClientID%>').focus();
                 args.IsValid = false;
               }
             }
           }
           
           function ResearchEmployer_Validate(src, args) {
             args.IsValid = true;
             if ($('#<%=ResearchEmployerRadioButton.ClientID%>').prop('checked')) {
               var selectedId = $('#<%=ResearchEmployerAutoCompleteTextBox.SelectedValueClientId%>').val();
               if (!selectedId || selectedId.length == 0) {
                 $('#<%=ResearchEmployerAutoCompleteTextBox.ClientID%>').focus();
                 args.IsValid = false;
               }
             }
           }

           function ResearchSkill_Validate(src, args) {
             args.IsValid = true;
             if ($('#<%=ResearchSkillRadioButton.ClientID%>').prop('checked')) {
               var selectedId = $('#<%=ResearchSkillAutoCompleteTextBox.SelectedValueClientId%>').val();
               if (!selectedId || selectedId.length == 0) {
                 $('#<%=ResearchSkillAutoCompleteTextBox.ClientID%>').focus();
                 args.IsValid = false;
               }
             }
           }
    </script>
</asp:Content>
