#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Core;
using Framework.Core;

#endregion

namespace  Focus.CareerExplorer.WebExplorer
{
	public partial class Print : PageBase
	{
	  private string _printName;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (!IsPostBack)
			{
				if (Page.RouteData.Values.ContainsKey("type"))
				{
					var type = Page.RouteData.Values["type"].ToString();
					ReportTypes reportType;
					Enum.TryParse(type, true, out reportType);

					string sessionKey;
					switch (reportType)
					{
						case ReportTypes.Job:
							sessionKey = "Explorer:JobPrintModel";
              var jobPrintModel = App.GetSessionValue<ExplorerJobPrintModel>(sessionKey);
							App.RemoveSessionValue(sessionKey);
              JobPrint.BindControls(jobPrintModel);
					    JobPrint.Visible = true;

					    _printName = jobPrintModel.Name;
              break;

						case ReportTypes.Employer:
							sessionKey = "Explorer:EmployerPrintModel";
              var employerPrintModel = App.GetSessionValue<ExplorerEmployerPrintModel>(sessionKey);
							App.RemoveSessionValue(sessionKey);
              EmployerPrint.BindControls(employerPrintModel);
              EmployerPrint.Visible = true;

              _printName = employerPrintModel.Name;
              break;

						case ReportTypes.DegreeCertification:
							sessionKey = "Explorer:DegreeCertificationPrintModel";
              var degreeCertificationPrintModel = App.GetSessionValue<ExplorerDegreeCertificationPrintModel>(sessionKey);
							App.RemoveSessionValue(sessionKey);
              DegreeCertificationPrint.BindControls(degreeCertificationPrintModel);
              DegreeCertificationPrint.Visible = true;

              _printName = degreeCertificationPrintModel.Name;
              break;

            case ReportTypes.ProgramAreaDegree:
              sessionKey = "Explorer:ProgramAreaDegreePrintModel";
              var printModel = App.GetSessionValue<ExplorerProgramAreaDegreePrintModel>(sessionKey);
							App.RemoveSessionValue(sessionKey);
              ProgramAreaDegreePrint.BindControls(printModel);
              ProgramAreaDegreePrint.Visible = true;

              _printName = printModel.Name;
              break;

						case ReportTypes.Skill:
							sessionKey = "Explorer:SkillPrintModel";
              var skillPrintModel = App.GetSessionValue<ExplorerSkillPrintModel>(sessionKey);
							App.RemoveSessionValue(sessionKey);
              SkillPrint.BindControls(skillPrintModel);
              SkillPrint.Visible = true;

              _printName = skillPrintModel.Name;
              break;

						case ReportTypes.Internship:
							sessionKey = "Explorer:InternshipPrintModel";
              var internshipPrintModel = App.GetSessionValue<ExplorerInternshipPrintModel>(sessionKey);
							App.RemoveSessionValue(sessionKey);
              InternshipPrint.BindControls(internshipPrintModel);
              InternshipPrint.Visible = true;

              _printName = internshipPrintModel.Name;
              break;

            case ReportTypes.CareerArea:
              sessionKey = "Explorer:CareerAreaPrintModel";
              var careerAreaPrintModel = App.GetSessionValue<ExplorerCareerAreaPrintModel>(sessionKey);
              App.RemoveSessionValue(sessionKey);
              CareerAreaPrint.BindControls(careerAreaPrintModel);
              CareerAreaPrint.Visible = true;

              _printName = careerAreaPrintModel.Name;
              break;
					}
				}
			}
		}

	  /// <summary>
	  /// Handles the PreRender event of the Page control.
	  /// </summary>
	  /// <param name="sender">The source of the event.</param>
	  /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void Page_PreRender(object sender, EventArgs e)
	  {
      var route = Page.RouteData.Values["format"];
	    if (route.IsNull())
	      return;

      var printFormat = route.ToString().AsEnum(ModalPrintFormat.None);
	    if (printFormat != ModalPrintFormat.Pdf)
	      return;

      ScriptHolder.Visible = false;

      var baseUrl = Request.ApplicationPath.IsNull()
                      ? string.Concat(Request.Url.Scheme, "://", Request.Url.Authority, "/")
                      : string.Concat(Request.Url.Scheme, "://", Request.Url.Authority, Request.ApplicationPath.TrimEnd('/'), "/");

      var html = new StringBuilder();
      var stringWriter = new StringWriter(html);
      using (var writer = new HtmlTextWriter(stringWriter))
      {
        Render(writer);
      }
      stringWriter.Close();

      byte[] pdfData;
      Utilities.Export2PDF(html.ToString(), out pdfData, string.Empty, baseUrl);

      /*
      var stream = new MemoryStream();
      var textWriter = new StreamWriter(stream);
      using (var writer = new HtmlTextWriter(textWriter))
      {
        Render(writer);
      }
      textWriter.Flush();
      stream.Position = 0;

      var license = new License();
      license.SetLicense("Aspose.Words.lic");

      var loadOptions = new LoadOptions { BaseUri = baseUrl, LoadFormat = LoadFormat.Html };
      var doc = new Document(stream, loadOptions);
      stream.Close();
      textWriter.Close();

      var saveOptions = new PdfSaveOptions { Compliance = PdfCompliance.PdfA1b };
      var outputStream = new MemoryStream();
      doc.Save(outputStream, saveOptions);
      outputStream.Close();

      var pdfData = outputStream.ToArray();
      */

	    var fileName = Regex.Replace(Regex.Replace(_printName, @"\\|\/|\:|\*|\?|\""|\<|\>|\|", ""), @"\s+", "_");
	    var fileLength = pdfData.Length.ToString(CultureInfo.InvariantCulture);

      Response.Clear();
      Response.AddHeader("Content-Type", "binary/octet-stream");
      Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.pdf; size={1}", fileName, fileLength));
      Response.Flush();
      Response.BinaryWrite(pdfData);
      Response.Flush();
      Response.End();
    }
	}
}