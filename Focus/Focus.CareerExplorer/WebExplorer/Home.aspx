﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Home" EnableEventValidation="false" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<asp:Literal ID="GoogleTrackingLiteral" runat="server" />
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<script language="javascript" type="text/javascript">
		// Set order of explore div sections
		$(document).ready(function () {
			//var exploreDivs = ["ExploreSection", "ResearchSection", "StudySection"];
			var exploreSections = <%= ExploreSections %>;
			for (var i = 0; i < exploreSections.length; i++) {
			  var exploreRow = exploreSections[i] + "Row";
				$("#" + exploreRow).appendTo("#ExploreSections");
	  		$("#" + exploreRow).show();
			  var exploreDiv = exploreSections[i] + "Divider";
				$("#" + exploreDiv).appendTo("#ExploreSections");
	  		$("#" + exploreDiv).show();
			}
		});
	</script>
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	
	<asp:Literal ID="DisclaimerLiteral" runat="server" />
	
	<asp:PlaceHolder ID="JobSearchPlaceHolder" runat="server">
		<table class="landingPageNavigation">
		  <tr class="jobSearch">
			  <td class="column1" valign="bottom">
				  <img src="<%=UrlBuilder.ExploreSectionSearchImage()%>" alt="Search for jobs" width="50" height="51" />
			  </td>
			  <td class="column2">
				  <a href=""><span class="largeHeading"><%= HtmlLocalise("JobSearch.Label", "Start or continue a job search")%></span></a>
				  <span class="headingContent"><%= HtmlLocalise("JobSearchSubHeading.Label", "includes building a resume")%></span>
			  </td>
		  </tr>
    </table>
	</asp:PlaceHolder>
  <br />
  <table class="landingPageNavigation" id="ExploreSections">

  </table>
  <table style="display: none" role="presentation">
		<tr class="careersAndEducation" id="SchoolRow">
			<td class="column1"><asp:Image ID="SchoolSectionImage" runat="server" Height="51" Width="50" AlternateText="School Section Image" /></td>
			<td class="column2"><span class="largeHeading"><asp:HyperLink ID="SchoolSectionHyperLink" runat="server" /></span></td>
		</tr>
		<tr class="divider" id="SchoolDivider"><td colspan="2"></td></tr>
		<tr class="careersAndEducation" id="ExploreRow">
			<td class="column1"><asp:Image ID="ExploreSectionImage" runat="server" Height="51" Width="50" AlternateText="Explore Section Image" /></td>
			<td class="column2"><span class="largeHeading"><asp:HyperLink ID="ExploreSectionHyperLink" runat="server" /></span></td>
		</tr>
		<tr class="divider" id="ExploreDivider"><td colspan="2"></td></tr>
		<tr class="careersAndEducation" id="ResearchRow">
			<td class="column1"><asp:Image ID="ResearchSectionImage" runat="server" Height="51" Width="50" AlternateText="Research Section Image"/></td>
			<td class="column2"><span class="largeHeading"><asp:HyperLink ID="ResearchSectionHyperLink" runat="server" /></span></td>
		</tr>
		<tr class="divider" id="ResearchDivider"><td colspan="2"></td></tr>
		<tr class="careersAndEducation" id="StudyRow">
			<td class="column1"><asp:Image ID="StudySectionImage" runat="server" Height="51" Width="50" AlternateText="Study Section Image" /></td>
			<td class="column2"><span class="largeHeading"><asp:HyperLink ID="StudySectionHyperLink" runat="server" /></span></td>
		</tr>
		<tr class="divider" id="StudyDivider"><td colspan="2"></td></tr>
		<tr class="careersAndEducation" id="ExperienceRow">
			<td class="column1"><asp:Image ID="ExperienceSectionImage" runat="server" Height="51" Width="50" AlternateText="Experience Section Image" /></td>
			<td class="column2"><span class="largeHeading"><asp:HyperLink ID="ExperienceSectionHyperLink" runat="server" /></span></td>
		</tr>
    <tr class="divider" id="ExperienceDivider"><td colspan="2"></td></tr>
	</table>		

</asp:Content>
