<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobModal.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.JobModal" %>
<%@ Register Src="ModalHeaderOptions.ascx" TagName="ModalHeaderOptions" TagPrefix="uc" %>
<%@ Register Src="EmployerList.ascx" TagName="JobEmployerList" TagPrefix="uc" %>
<%@ Register Src="JobSkillList.ascx" TagName="JobSkillList" TagPrefix="uc" %>
<%@ Register Src="JobList.ascx" TagName="SimilarJobList" TagPrefix="uc" %>
<%@ Register Src="Criteria.ascx" TagName="CriteriaFilter" TagPrefix="uc" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
    PopupControlID="ModalPanel" BehaviorID="JobModalPopup" BackgroundCssClass="modalBackground"
    RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal"
    Style="display: none;">
    <div class="lightbox lightboxmodal" id="jobLightBox">
        <uc:ModalHeaderOptions ID="ModalHeaderOptions" runat="server" OnBreadcrumbItemClicked="JobModalBreadcrumb_ItemClicked"
            OnBookmarkClicked="BookmarkLinkButton_Click" OnSendEmailClicked="SendEmailImageButton_Click"
            OnPrintCommand="ModalHeaderOptions_OnPrintCommand" OnCloseClicked="CloseButton_Click">
        </uc:ModalHeaderOptions>
        <div class="job-scroll-pane tooltipContainer">
            <table class="modalLeftPanel" role="presentation">
                <tr class="lightboxHeading">
                    <td>
                        <h2 style="display: inline;">
                            <span class="sr-only">Job Name</span>
                            <asp:Label ID="JobNameLabel" runat="server" />
                        </h2>
                        <asp:Panel ID="DegreeProgramPanel" runat="server" Visible="False" CssClass="reportColumnInlineTooltip">
                            <div class="tooltip">
                                <%= HtmlTooltipster("tooltipWithArrow degreeProgram", "DegreeProgram.TooltipText", "#SCHOOLNAME# offers a program of study for this career.")%>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="StarterJobPanel" runat="server" Visible="False" CssClass="reportColumnInlineTooltip">
                            <div class="tooltip">
                                <div id="StarterJobToolTipDiv" runat="server">
                                    <div class="toolTipNew tooltipImage" id="StarterJobToolTipImage" runat="server">
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
                <tr class="lightboxHeading">
                    <td>
                        <uc:CriteriaFilter ID="CriteriaFilter" runat="server" OnFilterCancelled="Filter_Cancelled" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal runat="server" ID="JobIntroductionLabel"></asp:Literal>
                    </td>
                </tr>
            </table>
            <table class="topJobTitles" width="67.5%" role="presentation">
                <tr>
                    <td valign="top" width="20%">
                        <%= HtmlLocalise("JobTitles.Label", "Common titles")%>:&nbsp;
                    </td>
                    <td valign="top">
                        <asp:Label ID="JobTitlesLabel" runat="server" />
                    </td>
                </tr>
            </table>
            <table width="100%" role="presentation">
                <tr class="modalLeftPanel">
                    <td valign="top">
                        <table style="width:100%" class="numberedAccordion" role="presentation">
                            <tr id="JobModalSection1TitleTableRow" class="multipleAccordionTitle2 on" runat="server">
                                <td width="10" class="column1">
                                    1
                                </td>
                                <td class="column2" width="600px">
                                    <a href="#">
                                        <%= HtmlLocalise("SalaryHiringTrends.Header", "Salary & hiring trends")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Gloabl.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr id="JobModalSection1BodyTableRow" class="accordionContent2 open" runat="server">
                                <td>
                                </td>
                                <td colspan="2">
                                    <p>
                                        <%= HtmlLocalise("SalaryHiringTrendsIntro.Text", "Salary and hiring trends provide an overview of salary and hiring demand, nationally and by geographic area, for this occupation � as well as the available positions in the selected area over the last 12 months.")%></p>
                                    <div class="chartSalaryTrend">
                                        <table role="presentation">
                                            <tr>
                                                <td colspan="4">
                                                    <%= HtmlLocalise("HiringDemand.Text", "Hiring demand")%>: <strong>
                                                        <asp:Literal ID="HiringTrendLiteral" runat="server" /></strong>
                                                </td>
                                            </tr>
                                            <tr class="chartLabels">
                                                <td>
                                                    <div>
                                                        <span class="toolTipNew" title="<%= HtmlLocalise("HiringDemandLow.TooltipText", "Based on demand, this job ranks in the bottom half of all occupations that are available.")%>">
                                                            <%= HtmlLocalise("HiringDemandLow.Label", "LOW")%></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>
                                                        <span class="toolTipNew" title="<%= HtmlLocalise("HiringDemandMedium.TooltipText", "Based on #BUSINESS#:LOWER demand, this job ranks in the lower tier of the top half of all occupations that are available.")%>">
                                                            <%= HtmlLocalise("HiringDemandMedium.Label", "MEDIUM")%></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>
                                                        <span class="toolTipNew" title="<%= HtmlLocalise("HiringDemandHigh.TooltipText", "Based on #BUSINESS#:LOWER demand, this job ranks in the top 70-80 occupations of all occupations that are available.")%>">
                                                            <%= HtmlLocalise("HiringDemandHigh.Label", "HIGH")%></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div>
                                                        <span class="toolTipNew" title="<%= HtmlLocalise("HiringDemandVeryHigh.TooltipText", "Based on emp#BUSINESS#:LOWERloyer demand, this job ranks in the top 20-30 occupations of all occupations that are available.")%>">
                                                            <%= HtmlLocalise("HiringDemandVeryHigh.Label", "VERY HIGH")%></span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="chartBars">
                                                <td>
                                                    <span></span>
                                                </td>
                                                <td>
                                                    <span></span>
                                                </td>
                                                <td>
                                                    <span></span>
                                                </td>
                                                <td>
                                                    <span></span>
                                                </td>
                                            </tr>
                                            <tr class="chartArrow">
                                                <td colspan="4">
                                                    <div class="arrowWrap">
                                                        <asp:Literal ID="HiringTrendArrowSpanLiteral" runat="server" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" role="presentation">
                                            <tr>
                                                <td width="50%">
                                                    <%= HtmlLocalise("HiringDemandPositionAvailable.Text", "Positions available in the past 12 months")%>:
                                                </td>
                                                <td class="chartInfo">
                                                    &nbsp; <strong>
                                                        <asp:Literal ID="HiringDemandLiteral" runat="server" /></strong>
                                                    <asp:Image ID="HiringDemandImage" runat="server" Width="35" Height="35" alt="Hiring Demand image" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" height="10px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <%= HtmlLocalise("HiringDemandSalaryRange.Text", "Salary range for people employed in this occupation in selected area")%>:
                                                </td>
                                                <td class="chartInfo">
                                                    &nbsp; <strong>
                                                        <asp:Literal ID="SalaryRangeLiteral" runat="server" /></strong>
                                                    <asp:Image ID="SalaryRangeImage" runat="server" Width="35" Height="35" alt="Salary Range Image" />
                                                    <div style="display: inline-block">
                                                        <img src="<%= UrlBuilder.HelpIcon() %>" width="18" height="17" class="toolTipNew"
                                                            alt="Help" title="<%= HtmlLocalise("HiringDemandSalaryRange.TooltipText", "For individuals employed in your selected search area (statewide or MSA), #FOCUSEXPLORER#� provides a salary range based on state-specific data from the U.S. Bureau of Labor Statistics.") %>" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <asp:PlaceHolder ID="SalaryNationwidePlaceHolder" runat="server">
                                                <tr>
                                                    <td colspan="2" height="10px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Literal ID="SalaryNationwideLabelLiteral" runat="server" />:
                                                    </td>
                                                    <td class="chartInfo">
                                                        &nbsp; <strong>
                                                            <asp:Literal ID="SalaryNationwideLiteral" runat="server" /></strong>
                                                        <asp:Image ID="SalaryNationwideImage" runat="server" Width="35" Height="35" alt="Salary Nation Wide Image" />
                                                        <div style="display: inline-block">
                                                            <img src="<%= UrlBuilder.HelpIcon() %>" width="18" height="17" class="toolTipNew"
                                                                alt="Help" title="<%= HtmlLocalise("HiringDemandAverageSalaryNationwide.TooltipText", "For individuals employed in this job, #FOCUSEXPLORER#� provides an average salary based on real-time #BUSINESS#:LOWER job postings nationwide.") %>" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </asp:PlaceHolder>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr id="JobModalSection2TitleTableRow" class="multipleAccordionTitle2">
                                <td class="column1">
                                    2
                                </td>
                                <td class="column2">
                                    <a href="#">
                                        <%= HtmlLocalise("TopEmployers.Header", "In-demand #BUSINESSES#:LOWER")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2">
                                <td>
                                </td>
                                <td>
                                    <p>
                                        <asp:Label runat="server" ID="TopEmployersIntroLabel"></asp:Label></p>
                                    <uc:JobEmployerList ID="JobEmployerList" runat="server" EmployerListMode="Job" OnItemClicked="ModalList_ItemClicked"
                                        ShowEmployerJobPostingCount="True" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="JobModalSection3TitleTableRow" class="multipleAccordionTitle2">
                                <td class="column1">
                                    3
                                </td>
                                <td class="column2">
                                    <a href="#">
                                        <%= HtmlLocalise("RequiredSkills.Header", "Required skills: specialized, software, foundation")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2">
                                <td>
                                </td>
                                <td>
                                    <p>
                                        <%= HtmlLocalise("RequiredSkillsIntro.Text", "Required skills data for this occupation are based on positions advertised nationally over the last 12 months.")%></p>
                                    <uc:JobSkillList ID="SkillList" runat="server" OnItemClicked="ModalList_ItemClicked" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="JobModalSection4TitleTableRow" class="multipleAccordionTitle2" runat="server">
                                <td class="column1">
                                    4
                                </td>
                                <td class="column2">
                                    <a href="#">
                                        <%= HtmlLocalise("RequiredEducation.Header", "Typical education required")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr id="JobModalSection4BodyTableRow" class="accordionContent2" runat="server">
                                <td>
                                </td>
                                <td>
                                    <p>
                                        <asp:Label ID="DegreeIntroLabel" runat="server" />
                                        <asp:Label runat="server" ID="DegreesNotFoundLabel" Visible="False"></asp:Label>
                                        <asp:Panel runat="server" ID="TierLinksPanel">
                                            <strong>
                                                <%= HtmlLocalise("Show.Text", "Show") %>:</strong>
                                            <asp:LinkButton runat="server" ID="SpecificProgramsOfStudyLink" OnCommand="ProgramsOfStudyLink_ItemClicked"
                                                CommandArgument="1" Enabled="False"></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="RelatedProgramsOfStudyLink" Text="Related Programs Of StudyLink"
                                                OnCommand="ProgramsOfStudyLink_ItemClicked" CommandArgument="2"></asp:LinkButton>
                                        </asp:Panel>
                                    </p>
                                    <asp:PlaceHolder runat="server" ID="ProgramAreaPlaceHolder">
                                        <asp:PlaceHolder runat="server" ID="ProgramAreaTier1PlaceHolder">
                                            <table width="100%" class="genericTable withHeader" role="presentation">
                                                <tr>
                                                    <td class="tableHead">
                                                        <asp:Literal ID="ProgramAreaTier1Title" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ul>
                                                            <asp:Repeater ID="ProgramAreaTier1Repeater" runat="server" OnItemDataBound="ProgramAreaRepeater_ItemDataBound"
                                                                OnItemCommand="ModalList_ItemClicked">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:LinkButton ID="ProgramAreaLinkButton" CssClass="modalLink" runat="server" CommandArgument='<%# Eval("ProgramAreaId").ToString() %>'
                                                                            CommandName="ViewProgramAreaDegree" Text='<%# Eval("ProgramAreaName").ToString() %>' />&nbsp;
                                                                        <ul>
                                                                            <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="ProgramAreaDegreeEducationLevelRepeater_ItemDataBound"
                                                                                OnItemCommand="ModalList_ItemClicked">
                                                                                <ItemTemplate>
                                                                                    <li>
                                                                                        <asp:LinkButton ID="DegreeEducationLevelLinkButton" CssClass="modalLink" runat="server"
                                                                                            CommandArgument='<%# Eval("Id").ToString() %>' CommandName="ViewDegreeCertification" />&nbsp;
                                                                                    </li>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ul>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                        <asp:Literal ID="ProgramAreaTier1RepeaterNoneFoundLiteral" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder runat="server" ID="ProgramAreaTier2PlaceHolder" Visible="False">
                                            <table width="100%" class="genericTable withHeader" role="presentation">
                                                <tr>
                                                    <td class="tableHead">
                                                        <asp:Literal ID="ProgramAreaTier2Title" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ul>
                                                            <asp:Repeater ID="ProgramAreaTier2Repeater" runat="server" OnItemDataBound="ProgramAreaRepeater_ItemDataBound"
                                                                OnItemCommand="ModalList_ItemClicked">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <asp:LinkButton ID="ProgramAreaLinkButton" CssClass="modalLink" runat="server" CommandArgument='<%# Eval("ProgramAreaId").ToString() %>'
                                                                            CommandName="ViewProgramAreaDegree" Text='<%# Eval("ProgramAreaName").ToString() %>' />&nbsp;
                                                                        <ul>
                                                                            <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="ProgramAreaDegreeEducationLevelRepeater_ItemDataBound"
                                                                                OnItemCommand="ModalList_ItemClicked">
                                                                                <ItemTemplate>
                                                                                    <li>
                                                                                        <asp:LinkButton ID="DegreeEducationLevelLinkButton" CssClass="modalLink" runat="server"
                                                                                            CommandArgument='<%# Eval("Id").ToString() %>' CommandName="ViewDegreeCertification" />&nbsp;
                                                                                    </li>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ul>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                        <asp:Literal ID="ProgramAreaTier2RepeaterNoneFoundLiteral" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:PlaceHolder>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder runat="server" ID="DegreePlaceHolder">
                                        <asp:PlaceHolder runat="server" ID="DegreeTier1PlaceHolder">
                                            <table width="100%" class="genericTable withHeader" role="presentation">
                                                <tr>
                                                    <td class="tableHead">
                                                        <asp:Literal ID="DegreeTier1Title" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ul>
                                                            <asp:Repeater ID="DegreeTier1Repeater" runat="server" OnItemDataBound="DegreeRepeater_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <%# Eval("Name") %>&nbsp;
                                                                        <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound"
                                                                            OnItemCommand="ModalList_ItemClicked">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="DegreeEducationLevelLinkButton" CssClass="modalLink" runat="server"
                                                                                    CommandArgument='<%# Eval("Id").ToString() %>' CommandName="ViewDegreeCertification" />&nbsp;
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                        <asp:Literal ID="DegreeTier1RepeaterNoneFoundLiteral" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder runat="server" ID="DegreeTier2PlaceHolder">
                                            <table width="100%" class="genericTable withHeader" role="presentation">
                                                <tr>
                                                    <td class="tableHead">
                                                        <asp:Literal ID="DegreeTier2Title" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <ul>
                                                            <asp:Repeater ID="DegreeTier2Repeater" runat="server" OnItemDataBound="DegreeTier2Repeater_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <%# Eval("Name") %>&nbsp;
                                                                        <asp:Repeater ID="DegreeEducationLevelTier2Repeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound"
                                                                            OnItemCommand="ModalList_ItemClicked">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="DegreeEducationLevelLinkButton" CssClass="modalLink" runat="server"
                                                                                    CommandArgument='<%# Eval("Id").ToString() %>' CommandName="ViewDegreeCertification" />&nbsp;
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                        <asp:Literal ID="DegreeTier2RepeaterNoneFoundLiteral" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:PlaceHolder>
                                    </asp:PlaceHolder>
                                    <asp:Literal runat="server" ID="ClientDegreeDisclaimerLiteral" Visible="False" />
                                    <asp:Panel ID="DegreePanel" runat="server">
                                        <br />
                                        <asp:PlaceHolder ID="EducationRequirementsPlaceHolder" runat="server">
                                            <table width="100%" class="genericTable withHeader" role="presentation">
                                                <tr>
                                                    <td>
                                                        <div style="display: inline-block">
                                                            <%= HtmlLocalise("MinimumDegreeLevelRequirementsIntro.Text", "Minimum degree level required in job postings nationwide")%>
                                                        </div>
                                                        <div id="MinimumDegreeTooltip" runat="server" style="display: inline-block">
                                                            <%= HtmlTooltipster("tooltipWithArrow", "MinimumDegreeLevelRequirementsIntro.Help", "This chart reflects the minimum degree #BUSINESSES#:LOWER will accept. #BUSINESSES# will often prefer completion of a higher-level degree for applicants without significant work experience. Similarly, #BUSINESSES#:LOWER will often allow significant work experience to substitute for their minimum degree requirement.")%>
                                                        </div>
                                                        <table class="horizontalBarChart" role="presentation">
                                                            <tr>
                                                                <td class="column1" style="width: 155px; vertical-align: middle">
                                                                    <%= HtmlLocalise("HighSchoolTechnicalTraining.BarChartLabel", "High school/technical training")%>
                                                                </td>
                                                                <td class="column2" rowspan="5">
                                                                </td>
                                                                <td class="column3">
                                                                    <asp:Literal ID="HighSchoolTechnicalTrainingLiteral" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="column1" style="vertical-align: middle">
                                                                    <%= HtmlLocalise("AssociatesDegree.BarChartLabel", "Associates degree")%>
                                                                </td>
                                                                <td class="column3">
                                                                    <asp:Literal ID="AssociatesDegreeLiteral" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="column1" style="vertical-align: middle">
                                                                    <%= HtmlLocalise("BachelorsDegree.BarChartLabel", "Bachelors degree")%>
                                                                </td>
                                                                <td class="column3">
                                                                    <asp:Literal ID="BachelorsDegreeLiteral" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="column1" style="vertical-align: middle">
                                                                    <%= HtmlLocalise("GraduateProfessionalDegree.BarChartLabel", "Graduate/professional degree")%>
                                                                </td>
                                                                <td class="column3">
                                                                    <asp:Literal ID="GraduateProfessionalDegreeLiteral" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div>
                                                            <%= HtmlLocalise("MinimumDegreeLevelRequirementsOutro.Text", "This chart reflects the minimum degree #BUSINESSES#:LOWER will accept. #BUSINESSES# will often prefer completion of a higher level degree for applicants without significant work experience. Similarly, #BUSINESSES#:LOWER will often allow significant work experience to substitute for their minimum degree requirement.")%></div>
                                                        <br />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:PlaceHolder>
                                    </asp:Panel>
                                    <br />
                                    <asp:Panel ID="CertificationPanel" runat="server">
                                        <table width="100%" class="genericTable withHeader" role="presentation">
                                            <tr>
                                                <td class="tableHead">
                                                    <div style="height: 19px;">
                                                        <span class="toolTipNew" title="<%= HtmlLocalise("Certifications.TooltipText", "Certifications are awarded levels of knowledge or practice, normally issued by a third-party vendor after you pass an examination. This qualifies you in a particular subject or competency, and may require continuing education credits for annual recertification.")%>">
                                                            <%= HtmlLocalise("CertificationsAndLicences.Header", "CERTIFICATIONS AND LICENSES")%></span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div>
                                                        <%= HtmlLocalise("CertificationIntro.Text", "Employers may give preference to applicants who have earned one or more of the following certifications")%></div>
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="CertificationRepeater" runat="server" OnItemCommand="ModalList_ItemClicked"
                                                OnItemDataBound="CertificationRepeater_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:Literal ID="CertificationRankLiteral" runat="server" />.
                                                            <%# Eval("CertificationName")%>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </asp:Panel>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="JobModalSection5TitleTableRow" class="multipleAccordionTitle2">
                                <td class="column1">
                                    5
                                </td>
                                <td class="column2">
                                    <a href="#">
                                        <%= HtmlLocalise("LevelOfExperience.Header", "Level of experience")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2">
                                <td>
                                </td>
                                <td>
                                    <p>
                                        <%= HtmlLocalise("LevelOfExperienceIntro.Text", "Minimum level of experience required in job postings nationwide:")%></p>
                                    <table class="horizontalBarChart" role="presentation">
                                        <tr>
                                            <td class="column1" width="155px;">
                                                <%= HtmlLocalise("LessThanTwoYears.BarChartLabel", "Less than 2 years")%>
                                            </td>
                                            <td class="column2" rowspan="5">
                                            </td>
                                            <td class="column3">
                                                <asp:Literal ID="LessThanTwoLiteral" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="column1">
                                                <%= HtmlLocalise("TwoToFiveYears.BarChartLabel", "2-5 years")%>
                                            </td>
                                            <td class="column3">
                                                <asp:Literal ID="TwoToFiveLiteral" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="column1">
                                                <%= HtmlLocalise("FiveToEightYears.BarChartLabel", "5-8 years")%>
                                            </td>
                                            <td class="column3">
                                                <asp:Literal ID="FiveToEightLiteral" runat="server" />
                                            </td>
                                        </tr>
                                        <tr class="last">
                                            <td class="column1">
                                                <%= HtmlLocalise("EightPlusYears.BarChartLabel", "8+ years")%>
                                            </td>
                                            <td class="column3">
                                                <asp:Literal ID="EightPlusLiteral" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="JobModalSection6TitleTableRow" class="multipleAccordionTitle2" data-joblist="<%=SimilarJobList.ClientID %>">
                                <td class="column1">
                                    6
                                </td>
                                <td class="column2">
                                    <a href="#">
                                        <%= HtmlLocalise("SimilarJobs.Header", "Jobs with similar education and/or skills")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2">
                                <td>
                                </td>
                                <td>
                                    <p>
                                        <%= HtmlLocalise("SimilarJobsIntro.Text", "This category displays jobs that require similar education and/or skills to the selected occupation.")%></p>
                                    <uc:SimilarJobList ID="SimilarJobList" runat="server" JobListMode="Similar" OnItemClicked="ModalList_ItemClicked" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr id="JobModalSection7TitleTableRow" class="multipleAccordionTitle2">
                                <td class="column1">
                                    7
                                </td>
                                <td class="column2">
                                    <a href="#">
                                        <%= HtmlLocalise("CommonCareerPaths.Header", "Explore common career paths")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2">
                                <td>
                                </td>
                                <td colspan="2">
                                    <p>
                                        <%= HtmlLocalise("CommonCareerPathsIntro.Text", "Click on &quot;How did other people get here&quot; to see what individuals in this occupation were doing before they took this job. Click on &quot;Where people go from here&quot; to see common next steps.")%></p>
                                    <asp:UpdatePanel ID="JobCareerPathUpdatePanel" runat="server" ChildrenAsTriggers="True"
                                        UpdateMode="Conditional"  class="carrerPathGraph">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="CareerPathSecondLevelLeftJobLinkButton" />
                                            <asp:PostBackTrigger ControlID="CareerPathSecondLevelMiddleLeftJobLinkButton" />
                                            <asp:PostBackTrigger ControlID="CareerPathSecondLevelMiddleRightJobLinkButton" />
                                            <asp:PostBackTrigger ControlID="CareerPathSecondLevelRightJobLinkButton" />
                                            <asp:PostBackTrigger ControlID="CareerPathThirdLevelLeftJobLinkButton" />
                                            <asp:PostBackTrigger ControlID="CareerPathThirdLevelMiddleLeftJobLinkButton" />
                                            <asp:PostBackTrigger ControlID="CareerPathThirdLevelMiddleRightJobLinkButton" />
                                            <asp:PostBackTrigger ControlID="CareerPathThirdLevelRightJobLinkButton" />
                                        </Triggers>
                                        <ContentTemplate>
                                            <table role="presentation">
                                                <tr>
                                                    <td width="50px" valign="top">
                                                        <strong>
                                                            <%= HtmlLocalise("Global.Show.Text", "Show")%>:</strong>
                                                    </td>
                                                    <td class="accordionLink">
                                                        <asp:LinkButton ID="CareerPathFromLinkButton" runat="server" OnCommand="CareerPathLinkButton_Command" />
                                                        <asp:LinkButton ID="CareerPathToLinkButton" runat="server" OnCommand="CareerPathLinkButton_Command" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" role="presentation">
                                                <tr>
                                                    <td align="center">
                                                        <asp:Label ID="NoCareerPathLabel" runat="server" />
                                                        <table role="presentation">
                                                            <tr>
                                                                <td>
                                                                    <asp:Panel ID="CareerPathPanel" runat="server" CssClass="careerPathChart">
                                                                        <!-- First Row -->
                                                                        <asp:Panel ID="CareerPathTopLevelPanel" runat="server" CssClass="careerpathChartItem top topArrowDown">
                                                                            <table class="box" role="presentation">
                                                                                <tr>
                                                                                    <td align="center" valign="middle">
                                                                                        <asp:Label ID="CareerPathJobNameLabel" runat="server" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <span class="arrowLineTop"></span>
                                                                        </asp:Panel>
                                                                        <div class="clear">
                                                                        </div>
                                                                        <!-- Second Row -->
                                                                        <asp:Panel ID="CareerPathSecondLevelPanel" runat="server" CssClass="careerPathRowWrap">
                                                                            <asp:Panel ID="CareerPathSecondLevelLeftPanel" runat="server" CssClass="careerpathChartItem left">
                                                                                <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                                                <asp:LinkButton ID="CareerPathSecondLevelLeftJobLinkButton" Text="Career Path Second Level Middle Left LinkButton"
                                                                                    runat="server" CommandName="ViewJob" CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
                                                                                <table class="box" role="presentation">
                                                                                    <tr>
                                                                                        <td align="center" valign="middle">
                                                                                            <asp:LinkButton ID="CareerPathSecondLevelLeftLinkButton" Text="Career Path Second Level Left LinkButton"
                                                                                                runat="server" OnCommand="SecondLevelLinkButton_Command" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <span class="BottomArrow"></span>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="CareerPathSecondLevelMiddleLeftPanel" runat="server" CssClass="careerpathChartItem">
                                                                                <asp:Literal ID="CareerPathSecondLevelMiddleLeftArrowLineLiteral" runat="server" /><span
                                                                                    class="arrowLineBottom"></span>
                                                                                <asp:LinkButton ID="CareerPathSecondLevelMiddleLeftJobLinkButton" runat="server"
                                                                                    CommandName="ViewJob" CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
                                                                                <table class="box" role="presentation">
                                                                                    <tr>
                                                                                        <td align="center" valign="middle">
                                                                                            <asp:LinkButton ID="CareerPathSecondLevelMiddleLeftLinkButton" Text="Career Path Second Level Middle Left LinkButton"
                                                                                                runat="server" OnCommand="SecondLevelLinkButton_Command" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <span class="BottomArrow"></span>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="CareerPathSecondLevelMiddleRightPanel" runat="server" CssClass="careerpathChartItem">
                                                                                <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                                                <asp:LinkButton ID="CareerPathSecondLevelMiddleRightJobLinkButton" runat="server"
                                                                                    CommandName="ViewJob" CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
                                                                                <table class="box" role="presentation">
                                                                                    <tr>
                                                                                        <td align="center" valign="middle">
                                                                                            <asp:LinkButton ID="CareerPathSecondLevelMiddleRightLinkButton" Text="Career Path Second Level Middle Right LinkButton"
                                                                                                runat="server" OnCommand="SecondLevelLinkButton_Command" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <span class="BottomArrow"></span>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="CareerPathSecondLevelRightPanel" runat="server" CssClass="careerpathChartItem right">
                                                                                <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                                                <asp:LinkButton ID="CareerPathSecondLevelRightJobLinkButton" runat="server" CommandName="ViewJob"
                                                                                    CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
                                                                                <table class="box" role="presentation">
                                                                                    <tr>
                                                                                        <td align="center" valign="middle">
                                                                                            <asp:LinkButton ID="CareerPathSecondLevelRightLinkButton" Text="Career Path Second Level Right LinkButton"
                                                                                                runat="server" OnCommand="SecondLevelLinkButton_Command" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                                <span class="BottomArrow"></span>
                                                                            </asp:Panel>
                                                                        </asp:Panel>
                                                                        <!-- Third Row -->
                                                                        <asp:Panel ID="CareerPathThirdLevelPanel" runat="server" CssClass="careerPathRowWrap">
                                                                            <asp:Panel ID="CareerPathThirdLevelLeftPanel" runat="server" CssClass="careerpathChartItem left">
                                                                                <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                                                <asp:LinkButton ID="CareerPathThirdLevelLeftJobLinkButton" runat="server" CommandName="ViewJob"
                                                                                    CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
                                                                                <table class="box" role="presentation">
                                                                                    <tr>
                                                                                        <td align="center" valign="middle">
                                                                                            <asp:Label ID="CareerPathThirdLevelLeftJobNameLabel" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="CareerPathThirdLevelMiddleLeftPanel" runat="server" CssClass="careerpathChartItem">
                                                                                <asp:Literal ID="CareerPathThirdLevelMiddleLeftArrowLineLiteral" runat="server" /><span
                                                                                    class="arrowLineBottom"></span>
                                                                                <asp:LinkButton ID="CareerPathThirdLevelMiddleLeftJobLinkButton" runat="server" CommandName="ViewJob"
                                                                                    CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
                                                                                <table class="box" role="presentation">
                                                                                    <tr>
                                                                                        <td align="center" valign="middle">
                                                                                            <asp:Label ID="CareerPathThirdLevelMiddleLeftJobNameLabel" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="CareerPathThirdLevelMiddleRightPanel" runat="server" CssClass="careerpathChartItem center">
                                                                                <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                                                <asp:LinkButton ID="CareerPathThirdLevelMiddleRightJobLinkButton" runat="server"
                                                                                    CommandName="ViewJob" CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
                                                                                <table class="box" role="presentation">
                                                                                    <tr>
                                                                                        <td align="center" valign="middle">
                                                                                            <asp:Label ID="CareerPathThirdLevelMiddleRightJobNameLabel" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                            <asp:Panel ID="CareerPathThirdLevelRightPanel" runat="server" CssClass="careerpathChartItem right">
                                                                                <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                                                <asp:LinkButton ID="CareerPathThirdLevelRightJobLinkButton" runat="server" CommandName="ViewJob"
                                                                                    CssClass="occupation" OnCommand="ModalList_ItemClicked">i+</asp:LinkButton>
                                                                                <table class="box" role="presentation">
                                                                                    <tr>
                                                                                        <td align="center" valign="middle">
                                                                                            <asp:Label ID="CareerPathThirdLevelRightJobNameLabel" runat="server" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </asp:Panel>
                                                                        </asp:Panel>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="modalRightPanel">
                    <td valign="top">
                        <asp:PlaceHolder ID="ReadyToMakeAMovePlaceHolder" runat="server"><span class="promoStyle3">
                            <%= HtmlLocalise("ReadyToMakeAMove.Text", "Ready to make a move?")%>
                        </span><span class="promoStyle3Arrow"></span></asp:PlaceHolder>
                        <asp:PlaceHolder ID="ViewCurrentPositionsPlaceHolder" runat="server">
                            <asp:LinkButton ID="ViewCurrentPostingsLinkButton" Text="View Current Postings LinkButton"
                                runat="server" OnClick="ViewCurrentPostingsLinkButton_Click">
                                <span class="promoStyle4"><span class="modalLink">
                                    <asp:Literal runat="server" ID="ViewCurrentPositionsLiteral" Text="See current postings for<br />this job �"></asp:Literal>
                                </span></span>
                            </asp:LinkButton>
                        </asp:PlaceHolder>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<script language="javascript" type="text/javascript">
    Sys.Application.add_load(JobModal_PageLoad);

    function JobModal_PageLoad(sender, args) {
        window.setTimeout(JobModal_AdjustScroll, 1);

        // Jaws Compatibility
        var modal = $find('JobModalPopup');

        if (modal != null) {
            modal.add_shown(function () {
                $('.modal').attr('aria-hidden', false);
                $('.page').attr('aria-hidden', true);
            });
            modal.add_hiding(function () {
                $('.modal').attr('aria-hidden', true);
                $('.page').attr('aria-hidden', false);
            });
        }
    };

    function JobModal_AdjustScroll() {
        var section = $("#<%=JobModalSection4TitleTableRow.ClientID%>.on");
        if (section.length > 0) {
            var scrollPanel = section.closest(".job-scroll-pane");
            scrollPanel.scrollTop(section.offset().top - scrollPanel.offset().top);
        }
    }

    Sys.Application.add_load(function () {
        equaliseCareerPathItems();
        $('#JobModalSection7TitleTableRow, #JobModalSection7TitleTableRow .show').click(equaliseCareerPathItems);
    });
</script>
