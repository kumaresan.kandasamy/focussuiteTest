<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailModal.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.EmailModal" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel" 
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">

<div id="emailModal">

    <table class="lightboxOptions" role="presentation">
      <tr>
        <td valign="top" align="right"><asp:Button ID="CloseButton" runat="server" SkinID="closeModal" CausesValidation="False" OnClick="CancelButton_Click" title="CloseButton" class="closeModal" /></td>
      </tr>
    </table>
    <div class="degree-scroll-pane">

    <table class="lightboxHeading" role="presentation">
      <tr>
      	<td><h2>
		      <span class="sr-only">Name</span>
		      <asp:Label ID="NameLabel" runat="server" />
	      </h2></td>
      	<td><asp:Label ID="LocationLabel" runat="server" /></td>
      </tr>
    </table>

    <table width="100%" role="presentation">
      <tr>
        <td><%= HtmlLocalise("EmailThisLinkTo.Text", "Email this link to")%></td>
      </tr>
      <tr>
        <td>
          <%= HtmlInFieldLabel("EmailAddressesTextBox", "EmailAddresses.InlineLabel", "enter email addresses, separated by commas", 350)%>
          <asp:TextBox ID="EmailAddressesTextBox" runat="server" ClientIDMode="Static" Width="95%" />
		      <asp:RequiredFieldValidator ID="EmailAddressesRequired" runat="server" ControlToValidate="EmailAddressesTextBox" Display="Dynamic" 
			    SetFocusOnError="true" CssClass="error" ValidationGroup="EmailModal"/>
          <asp:RegularExpressionValidator ID="EmailAddressesRegEx" runat="server" ControlToValidate="EmailAddressesTextbox" Display="Dynamic"
			    SetFocusOnError="true" CssClass="error" ValidationGroup="EmailModal"/>    
        </td>
      </tr>
      <tr>
        <td><div style="height:10px;" /></div></td>
      </tr>
      <tr>
      	<td><%= HtmlLocalise("Message.Text", "Message")%><asp:Literal ID="MessageTipLiteral" runat="server" /></td>
      </tr>
      <tr>
        <td>
          <%= HtmlInFieldLabel("EmailBodyTextBox", "EmailBody.InlineLabel", "type your message here", 350)%>
          <asp:TextBox ID="EmailBodyTextBox" runat="server" ClientIDMode="Static" Width="95%" TextMode="MultiLine" Rows="8" />
					<asp:RequiredFieldValidator ID="EmailBodyTextBoxRequired" runat="server" ControlToValidate="EmailBodyTextBox" Display="Dynamic" 
						SetFocusOnError="true" CssClass="error" ValidationGroup="EmailModal"/>
          <asp:RegularExpressionValidator ID="EmailBodyTextBoxRegEx" runat="server" ControlToValidate="EmailBodyTextBox" Display="Dynamic" 
						SetFocusOnError="true" CssClass="error" ValidationGroup="EmailModal" />
        </td>
      </tr>
      <tr>
        <td><%= HtmlLocalise("NumberOfCharacters.Text", "Number of characters remaining: ")%> <asp:Label ID="EmailCharactersRemainingLabel" runat="server" ClientIDMode="Static" /></td>
      </tr>
      <tr>
        <td><div style="height:10px;" /></div></td>
      </tr>
			<tr>
        <td align="right">
          <asp:Button ID="CancelButton" runat="server" class="buttonLevel3" CausesValidation="False" OnClick="CancelButton_Click" />
          <asp:Button ID="SendEmailButton" runat="server" class="buttonLevel2" CausesValidation="True" ValidationGroup="EmailModal" OnClick="SendEmailButton_Click" />
        </td>
      </tr>
    </table>
  </div>
  </div>

<script language="javascript" type="text/javascript">
	$(document).ready(function () {
		var maxMessageLength = <%= MaxMessageLength %>;
		$('#EmailBodyTextBox').keydown(function (e) {
			if (!isSpecialKey(e)) {
				if ($(this).val().length >= maxMessageLength) {
					e.preventDefault();
				}
			}
		});

		Sys.Application.add_load(Email_PageLoad);

		function Email_PageLoad(sender, args) {

			// Jaws Compatibility
			var modal = $find('ModalPopup');

			if (modal != null) {
				modal.add_shown(function() {
					$('#ModalPanel').attr('aria-hidden', false);
					$('.page').attr('aria-hidden', true);
					$('#EmailAddressesTextBox').focus();
				});

				modal.add_hiding(function() {
					$('#ModalPanel').attr('aria-hidden', true);
					$('.page').attr('aria-hidden', false);
				});
			}
		}

		$('#EmailBodyTextBox').keyup(function () {
			var emailBody = $(this).val();

			if (emailBody && emailBody.length > 0) {
				var charsRemaining = maxMessageLength - emailBody.length;

				if (charsRemaining < 0) {
					$('#EmailCharactersRemainingLabel').text('0');
				} else {
					$('#EmailCharactersRemainingLabel').text(charsRemaining);
				}
			}
			else {
				$('#EmailCharactersRemainingLabel').text(maxMessageLength);
			}
		});
	});
	
	function isSpecialKey(e) {
		if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40)
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
</script>
</asp:Panel>