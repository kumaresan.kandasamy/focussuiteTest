﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class Criteria : UserControlBase
	{
    public bool HideCloseIcons { get; set; }

    public ExplorerCriteria ReportCriteria
    {
      get { return GetViewStateValue<ExplorerCriteria>("Criteria:ReportCriteria"); }
      set { SetViewStateValue("Criteria:ReportCriteria", value); }
    }

    public StateAreaViewDto BaseStateArea
    {
      get
      {
        var baseStateArea = GetViewStateValue<StateAreaViewDto>("Criteria:BaseStateArea");
        if (baseStateArea == null)
					BaseStateArea = baseStateArea = (ServiceClientLocator.ExplorerClient(App).GetAllStatesAllAreasEntry() ?? ServiceClientLocator.ExplorerClient(App).GetStateAreaDefault());

        return baseStateArea;
      }
      set { SetViewStateValue("Criteria:BaseStateArea", value); }
    }

    public delegate void FilterCancelledHandler(object sender, FilterEventArgs eventArgs);
    public event FilterCancelledHandler FilterCancelled;


    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
			if (!IsPostBack)
			{
				ApplyBranding();
			}
    }

		/// <summary>
		/// Applies client specific branding.
		/// </summary>
		private void ApplyBranding()
		{
			AreaCancelButton.ImageUrl =
				EmployerCancelButton.ImageUrl =
				CareerAreaCancelButton.ImageUrl =
				JobTitleCancelButton.ImageUrl =
				ProgramAreaDegreeSpanCancelButton.ImageUrl =
				DegreeLevelCancelButton.ImageUrl =
				DegreeEducationLevelCancelButton.ImageUrl =
				SkillCategoryCancelButton.ImageUrl =
				SkillSubCategoryCancelButton.ImageUrl =
				SkillCancelButton.ImageUrl =
				SkillJobCancelButton.ImageUrl = SkillJobSkillCancelButton.ImageUrl = UrlBuilder.CloseSmall();
		}

    /// <summary>
    /// Binds the specified criteria.
    /// </summary>
    /// <param name="reportType">Type of the report.</param>
    /// <param name="criteria">The criteria.</param>
    public void Bind(ReportTypes reportType, ExplorerCriteria criteria)
    {
      ReportCriteria = criteria;

      // Hide all elements
      EmployerSpan.Visible =
        CareerAreaSpan.Visible =
        JobTitleSpan.Visible =
        DegreeLevelSpan.Visible =
        DegreeEducationLevelSpan.Visible =
        ProgramAreaDegreeSpan.Visible =
        SkillJobSkillSpan.Visible =
        SkillJobSpan.Visible =
        SkillSpan.Visible =
        SkillSubCategorySpan.Visible = 
        SkillCategorySpan.Visible = false;

      EmployerCancelButton.Visible =
        CareerAreaCancelButton.Visible =
        JobTitleCancelButton.Visible =
        DegreeLevelCancelButton.Visible =
        DegreeEducationLevelCancelButton.Visible =
        ProgramAreaDegreeSpanCancelButton.Visible =
        SkillJobSkillCancelButton.Visible =
        SkillJobCancelButton.Visible =
        SkillCancelButton.Visible =
        SkillSubCategoryCancelButton.Visible =
        SkillCategoryCancelButton.Visible = !HideCloseIcons;

      // Set all button values where applicable
      if (string.IsNullOrEmpty(criteria.StateArea))
				criteria.StateArea = ServiceClientLocator.ExplorerClient(App).GetStateAreaName(criteria.StateAreaId);

      AreaLabel.Text = criteria.StateArea;
      AreaCancelButton.Visible = !HideCloseIcons && (criteria.StateAreaId != BaseStateArea.Id);

      if (reportType != ReportTypes.Employer)
      {
        if (!string.IsNullOrEmpty(criteria.Employer))
        {
          EmployerLabel.Text = criteria.Employer;
          EmployerSpan.Visible = true;
        }
      }

      if (reportType != ReportTypes.Job && reportType != ReportTypes.CareerArea)
      {
        if (!string.IsNullOrEmpty(criteria.CareerAreaJob))
        {
          JobTitleLabel.Text = criteria.CareerAreaJob;
          JobTitleSpan.Visible = true;
        }
        else if (!string.IsNullOrEmpty(criteria.CareerAreaJobTitle))
        {
          JobTitleLabel.Text = criteria.CareerAreaJobTitle;
          JobTitleSpan.Visible = true;
        }
        else if (criteria.CareerAreaId.HasValue && !string.IsNullOrEmpty(criteria.CareerArea))
        {
          CareerAreaLabel.Text = criteria.CareerArea;
          CareerAreaSpan.Visible = true;
        }
      }

      if (reportType != ReportTypes.DegreeCertification)
      {
        if (criteria.EducationCriteriaOption == EducationCriteriaOptions.DegreeLevel && criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree &&
            !string.IsNullOrEmpty(criteria.DegreeLevelLabel))
        {
          DegreeLevelLabel.Text = criteria.DegreeLevelLabel;
          DegreeLevelSpan.Visible = true;
        }
        else if (criteria.EducationCriteriaOption == EducationCriteriaOptions.Specific &&
                 !string.IsNullOrEmpty(criteria.DegreeEducationLevel))
        {
          DegreeEducationLevelLabel.Text = criteria.DegreeEducationLevel;
          DegreeEducationLevelSpan.Visible = true;
        }
      }

      if (reportType != ReportTypes.ProgramAreaDegree)
      {
        if (!string.IsNullOrEmpty(criteria.Degree))
        {
          ProgramAreaDegreeLabel.Text = string.Concat(criteria.ProgramArea, " - ", criteria.Degree);
          ProgramAreaDegreeSpan.Visible = true;
        }
      }

      if (reportType != ReportTypes.Skill)
      {
        if (criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByJob)
        {
          if (!string.IsNullOrEmpty(criteria.SkillJobSkill))
          {
            SkillJobSkillLabel.Text = criteria.SkillJobSkill;
            SkillJobSkillSpan.Visible = true;
          }
          else if (!string.IsNullOrEmpty(criteria.SkillJob))
          {
            SkillJobLabel.Text = criteria.SkillJob;
            SkillJobSpan.Visible = true;
          }
        }
        else if (criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByCategory)
        {
          if (!string.IsNullOrEmpty(criteria.Skill))
          {
            SkillLabel.Text = criteria.Skill;
            SkillSpan.Visible = true;
          }
          else if (!string.IsNullOrEmpty(criteria.SkillSubCategory))
          {
            SkillSubCategoryLabel.Text = criteria.SkillSubCategory;
            SkillSubCategorySpan.Visible = true;
          }
          else if (!string.IsNullOrEmpty(criteria.SkillCategory))
          {
            SkillCategoryLabel.Text = criteria.SkillCategory;
            SkillCategorySpan.Visible = true;
          }
        }
        else
        {
          if (!string.IsNullOrEmpty(criteria.Skill))
          {
            SkillLabel.Text = criteria.Skill;
            SkillSpan.Visible = true;
          }
        }
      }

    }

    /// <summary>
    /// Handles the Click event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void CancelButton_Click(object sender, CommandEventArgs e)
    {
      var cancelledFilter = e.CommandArgument.ToString();
      var criteria = ReportCriteria;
      var oldCriteria = criteria.Clone();

      // Remove the cancelled filter
      switch (cancelledFilter)
      {
        case "Area":
          criteria.StateAreaId = BaseStateArea.Id.GetValueOrDefault();
          criteria.StateArea = BaseStateArea.StateAreaName;
          break;

        case "Employer":
          criteria.EmployerId = null;
          criteria.Employer = null;
          break;

        case "CareerArea":
          criteria.CareerAreaId = null;
          criteria.CareerArea = null;
          break;

        case "Job":
          criteria.CareerAreaJob = criteria.CareerAreaJobTitle = null;
          criteria.CareerAreaJobId = criteria.CareerAreaJobTitleId = null;
          break;

        case "DegreeLevel":
          criteria.DegreeLevelLabel = null;
          criteria.DegreeLevel = DegreeLevels.AnyOrNoDegree;
          break;

        case "DegreeEducationLevel":
          criteria.DegreeEducationLevel = null;
          criteria.DegreeEducationLevelId = null;
          break;

        case "ProgramAreaDegree":
          criteria.ProgramArea = null;
          criteria.ProgramAreaId = null;
          criteria.Degree = null;
          criteria.DegreeId = null;
          break;

        case "SkillCategory":
          criteria.SkillCategory = null;
          criteria.SkillCategoryId = null;
          break;

        case "SkillSubCategory":
          criteria.SkillSubCategory = null;
          criteria.SkillSubCategoryId = null;
          break;

        case "Skill":
          criteria.Skill = null;
          criteria.SkillId = null;
          break;

        case "SkillJob":
          criteria.SkillJob = null;
          criteria.SkillJobId = null;
          break;

        case "SkillJobSkill":
          criteria.SkillJobSkill = null;
          criteria.SkillJobSkillId = null;
          break;
      }

      // Throw event to allow modal to redraw
      OnFilterCancelled(new FilterEventArgs { CommandArgument = new FilterCommandArgument { NewCriteria = criteria, OldCriteria = oldCriteria } });
    }

    protected virtual void OnFilterCancelled(FilterEventArgs eventArgs)
    {
      if (FilterCancelled != null)
        FilterCancelled(this, eventArgs);
    }
  }
}