<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalBreadcrumb.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.ModalBreadcrumb" %>

<asp:PlaceHolder ID="BreadcrumbPlaceHolder" runat="server">
	<asp:Literal ID="ArrowsLiteral" runat="server" />
	<asp:LinkButton ID="Breadcrumb1LinkButton" Text="Breadcrumb1 LinkButton" runat="server" OnCommand="BreadcrumbLinkButton_Command" />
	<asp:Literal ID="Spacer1Literal" runat="server" />
	<asp:LinkButton ID="Breadcrumb2LinkButton"  Text="Breadcrumb2 LinkButton" runat="server" OnCommand="BreadcrumbLinkButton_Command" />
	<asp:Literal ID="Spacer2Literal" runat="server" />
	<asp:LinkButton ID="Breadcrumb3LinkButton"  Text="Breadcrumb3 LinkButton" runat="server" OnCommand="BreadcrumbLinkButton_Command" />
</asp:PlaceHolder>
