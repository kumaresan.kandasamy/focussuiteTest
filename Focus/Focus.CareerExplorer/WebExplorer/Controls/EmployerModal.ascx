<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerModal.ascx.cs"
    Inherits=" Focus.CareerExplorer.WebExplorer.Controls.EmployerModal" %>
<%@ Register Src="ModalHeaderOptions.ascx" TagName="ModalHeaderOptions" TagPrefix="uc" %>
<%@ Register Src="SkillListSimple.ascx" TagName="EmployerSkillList" TagPrefix="uc" %>
<%@ Register Src="Criteria.ascx" TagName="CriteriaFilter" TagPrefix="uc" %>
<%@ Register Src="InternshipList.ascx" TagName="InternshipList" TagPrefix="uc" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
    PopupControlID="ModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">
    <div class="lightbox lightboxmodal" id="employerLightBox">
        <uc:ModalHeaderOptions ID="ModalHeaderOptions" runat="server" OnBreadcrumbItemClicked="EmployerModalBreadcrumb_ItemClicked"
            OnBookmarkClicked="BookmarkLinkButton_Click" OnSendEmailClicked="SendEmailImageButton_Click"
            OnPrintCommand="ModalHeaderOptions_OnPrintCommand" OnCloseClicked="CloseButton_Click">
        </uc:ModalHeaderOptions>
        <div class="employer-scroll-pane">
            <table class="lightboxHeading" role="presentation">
                <tr>
                    <td>
                        <h2>
                            <span class="sr-only">Employer Name</span>
                            <asp:Label ID="EmployerNameLabel" runat="server" />
                        </h2>
                    </td>
                </tr>
                <tr class="lightboxHeading">
                    <td>
                        <uc:CriteriaFilter ID="CriteriaFilter" runat="server" OnFilterCancelled="Filter_Cancelled" />
                    </td>
                </tr>
            </table>
            <table width="100%" role="presentation">
                <tr class="modalLeftPanel">
                    <td>
                        <table width="100%" class="numberedAccordion" role="presentation">
                            <tr class="multipleAccordionTitle2 on">
                                <td class="column1">
                                    1
                                </td>
                                <td class="column2" width="600px">
                                    <div>
                                        <span class="toolTipNew" title="<%= SectionOneToolTip %>"><a href="#">
                                            <asp:Literal runat="server" ID="SectionOneTitle"></asp:Literal></a></span>
                                    </div>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2 open">
                                <td>
                                </td>
                                <td>
                                    <uc:InternshipList ID="InternshipsList" runat="server" OnItemClicked="ModalList_ItemClicked"
                                        ListMode="Employer" ListSize="10" />
                                    <table width="100%" class="genericTable employerHiringDemandTable" role="presentation">
                                        <asp:Repeater ID="EmployerJobRepeater" runat="server" OnItemDataBound="EmployerJobRepeater_ItemDataBound"
                                            OnItemCommand="ModalList_ItemClicked">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <strong>
                                                            <asp:Literal ID="JobRankLiteral" runat="server" />.&nbsp;
                                                            <asp:LinkButton ID="JobNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("ReportData.JobId").ToString() %>'
                                                                CommandName="ViewJob"><%# Eval("ReportData.JobName").ToString().Length > 35 ? Eval("ReportData.JobName").ToString().Substring(0, 35) + "..." : Eval("ReportData.JobName")%></asp:LinkButton>&nbsp;
                                                            <asp:Literal ID="JobOpeningsLiteral" runat="server" />
                                                        </strong>
                                                        <asp:Panel ID="DegreeProgramPanel" runat="server" Visible="False" CssClass="reportColumnInlineTooltip">
                                                            <div class="tooltip">
                                                                <%= HtmlTooltipster("tooltipWithArrow degreeProgram", "DegreeProgram.TooltipText", "#SCHOOLNAME# offers a program of study for this career.")%>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Panel ID="StarterJobPanel" runat="server" Visible="False" CssClass="reportColumnInlineTooltip">
                                                            <div class="tooltip">
                                                                <div id="StarterJobToolTipDiv" runat="server">
                                                                    <div class="toolTipNew" id="StarterJobToolTipImage" runat="server">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </td>
                                                    <td nowrap="nowrap">
                                                        <asp:LinkButton ID="JobSearchLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("ReportData.ROnet").ToString() %>'
                                                            CommandName="SearchJob"><%= HtmlLocalise("HiringDemand.JobListings", "Job listings")%></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr class="multipleAccordionTitle2" id="SkillsHeader" runat="server">
                                <td class="column1">
                                    2
                                </td>
                                <td class="column2">
                                    <a href="#">
                                        <%= HtmlLocalise("TopSkills.Header", "In-demand skills required by this #BUSINESS#:LOWER")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2" id="SkillsDetail" runat="server">
                                <td>
                                </td>
                                <td>
                                    <uc:EmployerSkillList ID="EmployerSkillList" runat="server" SkillsListMode="Employer"
                                        OnItemClicked="ModalList_ItemClicked" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="modalRightPanel">
                    <td valign="top">
                        <asp:PlaceHolder ID="ReadyToMakeAMovePlaceHolder" runat="server"><span class="promoStyle3">
                            <%= HtmlLocalise("ReadyToMakeAMove.Text", "Ready to make a move?")%>
                        </span><span class="promoStyle3Arrow"></span></asp:PlaceHolder>
                        <asp:PlaceHolder ID="ViewCurrentPositionsPlaceHolder" runat="server">
                            <asp:LinkButton ID="ViewCurrentPostingsLinkButton" Text="View Current Postings LinkButton"
                                runat="server" OnClick="ViewCurrentPostingsLinkButton_Click">
                                <span class="promoStyle4"><span class="modalLink">
                                    <asp:Literal runat="server" ID="ViewCurrentPositionsLiteral" Text="See current postings for<br />this employer �"></asp:Literal>
                                </span></span>
                            </asp:LinkButton>
                        </asp:PlaceHolder>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
