<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillListSimple.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.SkillListSimple" %>

<asp:UpdatePanel ID="SkillsListUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
	<ContentTemplate>
		<table width="100%" role="presentation">
			<tr>
				<td width="50px"><strong><%= HtmlLocalise("Global.Show.Text", "Show")%>:</strong></td>
				<td class="accordionLink">
					<asp:LinkButton ID="SpecializedSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command"></asp:LinkButton> 
					<asp:LinkButton ID="SoftwareSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command"></asp:LinkButton> 
					<asp:LinkButton ID="FoundationSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command"></asp:LinkButton>
				</td>
			</tr>
			<tr>
				<td><div style="height:10px;" ></div></td>
			</tr>
		</table>
		<table width="100%" class="genericTable withHeader multiToOne" role="presentation">
			<tr>
				<td id="SkillTypeTableCell" runat="server" class="tableHead" style="background: #DDE3E8;">
					<table  role="presentation">
						<tr>
							<td style="background: #DDE3E8;">
								<asp:Label ID="SkillTypeLabel" runat="server" />&nbsp;
							</td>
							<td valign="top" style="background: #DDE3E8;">
								<div class="tooltip">
									<%= HtmlTooltipster("tooltipWithArrow", null, SkillTypeToolTip) %>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="left" valign="top">
				  <asp:Label runat="server" ID="NoSkillsLabel"></asp:Label>
          <asp:PlaceHolder ID="SkillsColumnOnePlaceHolder" runat="server">
					  <table width="100%" role="presentation">
						  <asp:Repeater ID="SkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
							  <ItemTemplate>
								  <tr>
									  <td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton></strong></td>
								  </tr>			
							  </ItemTemplate>
						  </asp:Repeater>
					  </table>
          </asp:PlaceHolder>
				</td>
				<asp:PlaceHolder ID="SkillsColumnTwoPlaceHolder" runat="server">
				<td width="50%" align="left" valign="top">
					<table width="100%" role="presentation">
						<asp:Repeater ID="SkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
							<ItemTemplate>
								<tr>
									<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton></strong></td>
								</tr>			
							</ItemTemplate>
						</asp:Repeater>
					</table>
				</td>
				</asp:PlaceHolder>
			</tr>
		</table>
	</ContentTemplate>
</asp:UpdatePanel>
