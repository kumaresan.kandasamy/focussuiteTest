﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core.Criteria.Explorer;
using Focus.Core.Views;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class CareerAreaList : ExplorerListControl
	{
    public override int RecordCount { get { return CareerAreaListView != null ? CareerAreaListView.Items.Count : 0; } }

    /// <summary>
    /// Gets the career area reports.
    /// </summary>
    public List<CareerAreaJobTotalView> GetCareerAreaReports()
    {
      const string sessionKey = "Explorer:CareerAreaList";
      return App.GetSessionValue<List<CareerAreaJobTotalView>>(sessionKey);
    }

    /// <summary>
    /// Sets the CareerArea reports.
    /// </summary>
    /// <param name="careerAreaReports">The career area reports.</param>
    private void SetCareerAreaReports(List<CareerAreaJobTotalView> careerAreaReports)
    {
      const string sessionKey = "Explorer:CareerAreaList";
      App.SetSessionValue(sessionKey, careerAreaReports);
    }

    private int _rank = 1;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

		/// <summary>
		/// Handles the LayoutCreated event of the CareerAreaListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CareerAreaListView_LayoutCreated(object sender, EventArgs e)
		{
			// the list is initially ordered by total jobs, so assign the correct sort image on initial load
			var totalJobsSortImage = (Image)CareerAreaListView.FindControl("TotalJobsSortImage");
			totalJobsSortImage.ImageUrl = UrlBuilder.SortDown();
		}

    /// <summary>
    /// Binds the list.
    /// </summary>
    /// <param name="forceReload">if set to <c>true</c> [force reload].</param>
    public override void BindList(bool forceReload)
    {
      if (forceReload)
        SetCareerAreaReports(null);

      CareerAreaListView.DataBind();

      var careerAreaReports = GetCareerAreaReports();
      
      var totalJobs = careerAreaReports.IsNotNull() ? careerAreaReports.Sum(x => x.TotalJobs) : 0;
      TotalCareerAreasLiteral.Text = totalJobs.GetValueOrDefault(0).ToString(@"#,#");
    }

    /// <summary>
    /// Gets the Career Area reports.
    /// </summary>
    /// <param name="reportCriteria">The report criteria.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <param name="orderBy">The order by.</param>
    /// <returns></returns>
    public List<CareerAreaJobTotalView> GetCareerAreaReports(ExplorerCriteria reportCriteria, int listSize, string orderBy)
    {
      var doSort = true;

      // get reports from view state
      var careerAreaReports = GetCareerAreaReports();

      // check if anything returned
      if (careerAreaReports == null || careerAreaReports.Count == 0)
      {
        // check if we have report criteria
        if (reportCriteria != null)
        {
          careerAreaReports = ServiceClientLocator.ExplorerClient(App).GetCareerAreaJobTotals(reportCriteria, listSize);
          SetCareerAreaReports(careerAreaReports);
          doSort = false;
        }
        else
        {
          careerAreaReports = new List<CareerAreaJobTotalView>();
          doSort = false;
        }
      }

      if (doSort)
      {
        if (String.IsNullOrEmpty(orderBy))
          orderBy = "TotalJobs DESC";

        var sortDescending = false;
        if (orderBy.EndsWith(" DESC"))
        {
          sortDescending = true;
          orderBy = orderBy.Substring(0, orderBy.LastIndexOf(" DESC", StringComparison.Ordinal));
        }

        switch (orderBy)
        {
          case "CareerAreaName":
            careerAreaReports = sortDescending ? careerAreaReports.OrderByDescending(x => x.CareerName).ToList() : careerAreaReports.OrderBy(x => x.CareerName).ToList();
            break;

          case "TotalJobs":
            careerAreaReports = sortDescending ? careerAreaReports.OrderByDescending(x => x.TotalJobs).ToList() : careerAreaReports.OrderBy(x => x.TotalJobs).ToList();
            break;
        }
      }

      return careerAreaReports;
    }

    /// <summary>
    /// Handles the Selecting event of the CareerAreaListViewODS control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
    protected void CareerAreaListViewODS_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
      e.InputParameters["reportCriteria"] = ReportCriteria;
      e.InputParameters["listSize"] = ListSize;
    }

    /// <summary>
    /// Handles the ItemDataBound event of the CareerAreaListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void CareerAreaListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
      if (e.Item.ItemType == ListViewItemType.DataItem)
      {
        var rankLiteral = (Literal)e.Item.FindControl("RankLiteral");
        rankLiteral.Text = _rank.ToString(CultureInfo.InvariantCulture);
        _rank++;
      }
    }

    /// <summary>
    /// Handles the ItemCommand event of the CareerAreaListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewCommandEventArgs"/> instance containing the event data.</param>
    protected void CareerAreaListView_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
      if (e.CommandName == "ViewCareerArea")
        OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
    }

    /// <summary>
    /// Handles the Sorting event of the CareerAreaListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
    protected void CareerAreaListView_Sorting(object sender, ListViewSortEventArgs e)
    {
      var sortImageUrl = e.SortDirection == SortDirection.Ascending ? UrlBuilder.SortUp() : UrlBuilder.SortDown();

      var careerAreaNameSortImage = (Image)CareerAreaListView.FindControl("CareerAreaNameSortImage");
      var numberAvailableSortImage = (Image)CareerAreaListView.FindControl("TotalJobsSortImage");

      switch (e.SortExpression)
      {
        case "CareerAreaName":
          careerAreaNameSortImage.ImageUrl = sortImageUrl;
          careerAreaNameSortImage.Visible = true;
          numberAvailableSortImage.Visible = false;
          break;

        default:
          careerAreaNameSortImage.Visible = false;
          numberAvailableSortImage.ImageUrl = sortImageUrl;
          numberAvailableSortImage.Visible = true;
          break;
      }
    }
  }
}