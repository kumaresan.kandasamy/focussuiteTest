﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CareerAreaList.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.CareerAreaList" %>

<asp:Panel runat="server" ID="CareerAreaCountPanel">
	<strong>
	  <%= HtmlLocalise("TotalCareerAreas.Label", "Total jobs posted over the last 12 months in your selected area")%>: 
    <asp:Literal ID="TotalCareerAreasLiteral" runat="server" /><br />
	</strong>
</asp:Panel>

<asp:UpdatePanel ID="CareerAreaListUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
	<ContentTemplate>
    <asp:ListView ID="CareerAreaListView" runat="server" DataSourceID="CareerAreaListViewODS" ItemPlaceholderID="CareerAreaListViewPlaceHolder" 
	    OnItemDataBound="CareerAreaListView_ItemDataBound" OnSorting="CareerAreaListView_Sorting" OnItemCommand="CareerAreaListView_ItemCommand" OnLayoutCreated="CareerAreaListView_LayoutCreated">
	    <LayoutTemplate>
		    <table width="100%" class="genericTable withHeader" role="presentation">
			    <thead>
				    <tr>
					    <td class="tableHead">
						    <asp:LinkButton ID="CareerAreaNameSortButton" runat="server" CommandName="Sort" CommandArgument="CareerAreaName"><%= HtmlLocalise("CareerAreaName.ColumnTitle", "CAREER AREA")%></asp:LinkButton>
						    <asp:Image ID="CareerAreaNameSortImage" runat="server" Visible="False" AlternateText="CareerArea Name Sort Image" />
					    </td>
					    <td class="tableHead">
						    <asp:LinkButton ID="TotalJobsSortButton" runat="server" CommandName="Sort" CommandArgument="TotalJobs"><%= HtmlLocalise("TotalJobs.ColumnTitle", "NUMBER OF JOBS")%></asp:LinkButton>
						    <asp:Image ID="TotalJobsSortImage" runat="server" AlternateText="Total Jobs Sort Image"/>
					    </td>
				    </tr>
			    </thead>
			    <tbody id="CareerAreaListViewPlaceHolder" runat="server"></tbody>
		    </table>
	    </LayoutTemplate>
	    <ItemTemplate>
				    <tr>
					    <td width="40%">
					      <strong>
					        <asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;
                  <asp:LinkButton ID="CareerAreaNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("CareerAreaId").ToString() %>' CommandName="ViewCareerArea"><%# Eval("CareerName") %></asp:LinkButton>
					      </strong>
					    </td>
					    <td width="40%"><%# Eval("TotalJobs", "{0:#,#}")%></td>
				    </tr>
	    </ItemTemplate>
    </asp:ListView>

    <asp:ObjectDataSource ID="CareerAreaListViewODS" runat="server" 
	    TypeName=" Focus.CareerExplorer.WebExplorer.Controls.CareerAreaList" EnablePaging="False"
	    SelectMethod="GetCareerAreaReports" SortParameterName="orderBy" OnSelecting="CareerAreaListViewODS_Selecting">
	    <SelectParameters>
		    <asp:Parameter Name="reportCriteria" DbType="Object" />
		    <asp:Parameter Name="listSize" DbType="Int32" />
	    </SelectParameters>
    </asp:ObjectDataSource>
	</ContentTemplate>
</asp:UpdatePanel>