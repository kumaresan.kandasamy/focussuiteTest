﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CareerMovesList.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.CareerMovesList" %>
<%@ Register src="JobList.ascx" tagName="JobList" tagPrefix="uc" %>

<asp:Panel ID="CareerListPanel" runat="server">
  <table class="accordionWithIcon" role="presentation">
    <tr class="multipleAccordionTitle2 on" data-joblist="<%=JobListMain.ClientID %>">
      <td valign="top" width="5%">
        <span class="accordionIcon"></span>
      </td>
      <td valign="top" width="95%">
        <asp:Label runat="server" ID="MainLabel"></asp:Label>
      </td>
    </tr>
    <tr class="accordionContent2 open">
      <td colspan="2">
        <uc:JobList ID="JobListMain" runat="server" OnItemClicked="JobsList_ItemCommand"  />
      </td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr class="multipleAccordionTitle2" data-joblist="<%=JobListOther.ClientID %>">
      <td valign="top" width="5%">
        <span class="accordionIcon"></span>
      </td>
      <td valign="top" width="95%">
        <asp:Label runat="server" ID="OtherLabel"></asp:Label>
      </td>
    </tr>
    <tr class="accordionContent2">
      <td colspan="2">
        <uc:JobList ID="JobListOther" runat="server" OnItemClicked="JobsList_ItemCommand" />
      </td>
    </tr>
  </table>
</asp:Panel>