#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Views;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class EmployerPrint : UserControlBase
	{
    private int _rank = 1;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Binds the controls.
    /// </summary>
    /// <param name="printModel">The print model.</param>
    public void BindControls(ExplorerEmployerPrintModel printModel)
    {
      EmployerNameLabel.Text = printModel.Name;
      CriteriaFilter.Bind(ReportTypes.Employer, printModel.ReportCriteria);

      _rank = 1;
      if (printModel.Internships != null)
      {
        EmployerInternshipsRepeater.DataSource = printModel.Internships;
        EmployerInternshipsRepeater.DataBind();

        SectionOneTitle.Text = CodeLocalise("EmployerInternships.Header", "Internships for the last 12 months");
        JobListPlaceholder.Visible = false;
        InternshipListPlaceHolder.Visible = true;
      }
      else
      {
        EmployerJobRepeater.DataSource = printModel.Jobs;
        EmployerJobRepeater.DataBind();

        SectionOneTitle.Text = CodeLocalise("Global.HiringDemand.Header", "Hiring demand for the last 12 months");
        JobListPlaceholder.Visible = true;
        InternshipListPlaceHolder.Visible = false;
      }

      SpecializedSkillTypeLabel.Text = CodeLocalise("SpecializedSkills.Label", "SPECIALIZED SKILLS");
      _rank = 1;
      SpecializedSkillsColumnOneRepeater.DataSource = printModel.SpecializedSkills.Take(10).ToList();
      SpecializedSkillsColumnOneRepeater.DataBind();

      if (printModel.SpecializedSkills.Count > 10)
      {
        SpecializedSkillsColumnTwoRepeater.DataSource = printModel.SpecializedSkills.Skip(10).Take(10).ToList();
        SpecializedSkillsColumnTwoRepeater.DataBind();
      }
      else
      {
        SpecializedSkillsColumnTwoPlaceHolder.Visible = false;
      }

      SoftwareSkillTypeLabel.Text = CodeLocalise("SoftwareSkills.Label", "SOFTWARE SKILLS");
      _rank = 1;
      SoftwareSkillsColumnOneRepeater.DataSource = printModel.SoftwareSkills.Take(10).ToList();
      SoftwareSkillsColumnOneRepeater.DataBind();

      if (printModel.SoftwareSkills.Count > 10)
      {
        SoftwareSkillsColumnTwoRepeater.DataSource = printModel.SoftwareSkills.Skip(10).Take(10).ToList();
        SoftwareSkillsColumnTwoRepeater.DataBind();
      }
      else
      {
        SoftwareSkillsColumnTwoPlaceHolder.Visible = false;
      }

      FoundationSkillTypeLabel.Text = CodeLocalise("FoundationSkills.Label", "FOUNDATION SKILLS");
      _rank = 1;
      FoundationSkillsColumnOneRepeater.DataSource = printModel.FoundationSkills.Take(10).ToList();
      FoundationSkillsColumnOneRepeater.DataBind();

      if (printModel.FoundationSkills.Count > 10)
      {
        FoundationSkillsColumnTwoRepeater.DataSource = printModel.FoundationSkills.Skip(10).Take(10).ToList();
        FoundationSkillsColumnTwoRepeater.DataBind();
      }
      else
      {
        FoundationSkillsColumnTwoPlaceHolder.Visible = false;
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the EmployerJobRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void EmployerJobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var employerJob = (ExplorerEmployerJobReportView)e.Item.DataItem;

        var rankLiteral = (Literal) e.Item.FindControl("JobRankLiteral");
        rankLiteral.Text = _rank.ToString();

        var openingsLiteral = (Literal) e.Item.FindControl("JobOpeningsLiteral");
        openingsLiteral.Text = String.Format("({0}{1})", employerJob.ReportData.Positions.ToString("#,#"),
                                             _rank == 1
                                               ? " " + CodeLocalise("JobOpenings.Text", "openings")
                                               : String.Empty);


        var mortarIcon = (Image)e.Item.FindControl("MortarIcon");
        mortarIcon.ImageUrl = UrlBuilder.DegreeProgramsImage();
        mortarIcon.Visible = (App.Settings.Theme == FocusThemes.Education && employerJob.RelatedDegreeAvailableAtCurrentSchool);

        if (employerJob.ReportData.StarterJob.HasValue)
        {
          var icon = (Image)e.Item.FindControl("StarterJobIcon");
          icon.Visible = true;
          switch (employerJob.ReportData.StarterJob.Value)
          {
	          case StarterJobLevel.Many:
		          icon.ImageUrl = UrlBuilder.StarterJobsManyImage();
		          break;
	          case StarterJobLevel.Some:
		          icon.ImageUrl = UrlBuilder.StarterJobsSomeImage();
		          break;
          }
        }

        _rank++;
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the SkillsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void SkillsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var rankLiteral = (Literal) e.Item.FindControl("SkillRankLiteral");
        rankLiteral.Text = _rank.ToString();
        _rank++;
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the EmployerInternshipsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void EmployerInternshipsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var rankLiteral = (Literal) e.Item.FindControl("InternshipRankLiteral");
        rankLiteral.Text = _rank.ToString();
        _rank++;
      }
    }
  }
}
