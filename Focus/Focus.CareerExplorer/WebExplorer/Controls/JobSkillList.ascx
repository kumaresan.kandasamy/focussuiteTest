<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobSkillList.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.JobSkillList" %>

<asp:UpdatePanel ID="SkillsListUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
	<ContentTemplate>
		<table width="100%" role="presentation">
			<tr>
				<td width="50px"><strong><%= HtmlLocalise("Global.Show.Text", "Show")%>:</strong></td>
				<td class="accordionLink">
					<asp:LinkButton ID="SpecializedSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command" />
					<asp:LinkButton ID="SoftwareSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command" />
					<asp:LinkButton ID="FoundationSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command" /><br />
					<asp:LinkButton ID="HeldSkillsLinkButton" runat="server" OnCommand="SkillsLinkButton_Command" />
				</td>
			</tr>
			<tr>
				<td><div style="height:10px;"></div></td>
			</tr>
		</table>
		<asp:PlaceHolder ID="DemandedSkillsPlaceHolder" runat="server">
			<table width="100%" class="genericTable withHeader multiToOne" role="presentation">
				<tr>
					<td id="SkillTypeTableCell" class="tableHead" runat="server">
						<table  role="presentation">
							<tr>
								<td style="background: #DDE3E8;">
									<asp:Label ID="SkillTypeLabel" runat="server" />&nbsp;
								</td>
								<td valign="top" style="background: #DDE3E8;">
									<asp:Image Width="18" Height="17" CssClass="toolTipNew" popup="1" AlternateText="Skill Type" ID="SkillTypeToolTip" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="left" valign="top">
					  <asp:Label runat="server" ID="NoSkillsLabel"></asp:Label>
            <asp:PlaceHolder ID="SkillsColumnOnePlaceHolder" runat="server">
						  <table width="100%" role="presentation">
							  <asp:Repeater ID="SkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								  <ItemTemplate>
									  <tr>
										  <td valign="top">
											  <strong>
											    <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											  </strong>
										  </td>
									  </tr>			
								  </ItemTemplate>
							  </asp:Repeater>
						  </table>
            </asp:PlaceHolder>
					</td>
					<asp:PlaceHolder ID="SkillsColumnTwoPlaceHolder" runat="server">
					<td width="50%" align="left" valign="top">
						<table width="100%" role="presentation">
							<asp:Repeater ID="SkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td valign="top">
											<strong>
												<asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											</strong>
										</td>
									</tr>			
								</ItemTemplate>
							</asp:Repeater>
						</table>
					</td>
					</asp:PlaceHolder>
				</tr>
			</table>
		</asp:PlaceHolder>
		<asp:PlaceHolder ID="HeldSkillsPlaceHolder" runat="server">
			<table width="100%" class="genericTable withHeader" role="presentation">
				<tr>
					<td class="tableHead" >
						<table role="presentation">
							<tr>
								<td style="display: table-cell; background: #DDE3E8;">
									<%= HtmlLocalise("OtherSpecializedSkills.Text", "SPECIALIZED SKILLS") %>&nbsp;
								</td>
								<td valign="top" style="display: table-cell; background: #DDE3E8;">					
									<img src="<%=UrlBuilder.HelpIcon()%>" width="18" height="17" class="toolTipNew" alt="Help icon" title="<%= HtmlLocalise("SpecializedSkills.TooltipText", "Successful candidates for this occupation have shown these other skills on their resumes.")%>" />
								</td>
							</tr>
						</table>
					</td>
					<td class="tableHead" >
						<table role="presentation">
							<tr>
								<td style="display: table-cell; background: #DDE3E8;">
									<%= HtmlLocalise("OtherSoftwareSkills.Text", "SOFTWARE SKILLS") %>&nbsp;
								</td>
								<td valign="top" style="display: table-cell; background: #DDE3E8;">
									<img src="<%=UrlBuilder.HelpIcon()%>" width="18" height="17" class="toolTipNew" alt="Help icon" title="<%= HtmlLocalise("SoftwareSkills.TooltipText", "Successful candidates for this occupation have shown these other skills on their resumes.")%>" />					
								</td>
							</tr>
						</table>
					</td>
				</tr>
        <tr id="NoHeldSkillsRow" runat="server">
          <td colspan="2">
            <asp:Label runat="server" ID="NoHeldSkillsLabel"></asp:Label>
          </td>
        </tr>
				<tr id="HeldSkillsRow" runat="server">
					<td align="left" valign="top">
						<table width="100%" role="presentation">
							<asp:Repeater ID="HeldSpecializedSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td valign="top">
											<strong>
												<asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											</strong>
										</td>
									</tr>			
								</ItemTemplate>
							</asp:Repeater>
						</table>						
					</td>
					<td align="left" valign="top">
						<table width="100%" role="presentation">
							<asp:Repeater ID="HeldSoftwareSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td valign="top">
											<strong>
												<asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											</strong>
										</td>
									</tr>			
								</ItemTemplate>
							</asp:Repeater>
						</table>						
					</td>
				</tr>
			</table>			
		</asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="HeldFoundationSkillHolder">
            <table width="100%" class="genericTable withHeader" role="presentation">
				<tr>
					<td class="tableHead">
						<table role="presentation">
							<tr>
								<td style="display: table-cell">
									<%= HtmlLocalise("OtherFoundationSkills.Text", "FOUNDATION SKILLS") %>&nbsp;
								</td>
								<td valign="top" style="display: table-cell">					
									<img src="<%=UrlBuilder.HelpIcon()%>" width="18" height="17" class="toolTipNew" alt="Help icon" title="<%= HtmlLocalise("FoundationSkills.TooltipText", "Successful candidates for this occupation have shown these other skills on their resumes.")%>" />
								</td>
							</tr>
						</table>
					</td></tr>
				<tr>
					<td align="left" valign="top">
						<table width="100%" role="presentation">
							<asp:Repeater ID="HeldFoundationSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound" OnItemCommand="SkillsRepeater_ItemCommand">
								<ItemTemplate>
									<tr>
										<td valign="top">
											<strong>
												<asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <asp:Literal runat="server" ID="SkillNameLinkDisabled" Visible="False" /><asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName") %></asp:LinkButton>
											</strong>
										</td>
									</tr>			
								</ItemTemplate>
							</asp:Repeater>
						</table>						
					</td>
				</tr>
			</table>
        </asp:PlaceHolder>
	</ContentTemplate>
</asp:UpdatePanel>
