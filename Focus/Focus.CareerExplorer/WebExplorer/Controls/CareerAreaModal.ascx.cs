﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class CareerAreaModal : ExplorerModalControl
	{
    public CareerAreaDto CareerArea
    {
      get { return GetViewStateValue<CareerAreaDto>("CareerAreaModal:CareerArea"); }
      set { SetViewStateValue("CareerAreaModal:CareerArea", value); }
    }

    public ExplorerCriteria ReportCriteria
    {
      get { return GetViewStateValue<ExplorerCriteria>("CareerAreaModal:ReportCriteria"); }
      set { SetViewStateValue("CareerAreaModal:ReportCriteria", value); }
    }

    public List<LightboxBreadcrumb> Breadcrumbs
    {
      get { return GetViewStateValue("CareerAreaModal:Breadcrumb", new List<LightboxBreadcrumb>()); }
      set { SetViewStateValue("CareerAreaModal:Breadcrumb", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
        Localise();
    }

    /// <summary>
    /// Shows the specified career area id.
    /// </summary>
    /// <param name="careerAreaId">The career area id.</param>
    /// <param name="criteria">The criteria.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="printFormat">The format to print the modal</param>
    public void Show(long careerAreaId, ExplorerCriteria criteria, List<LightboxBreadcrumb> breadcrumbs, ModalPrintFormat printFormat = ModalPrintFormat.None)
    {
      var careerArea = ServiceClientLocator.ExplorerClient(App).GetCareerArea(careerAreaId);

      CareerArea = careerArea;
      criteria.CareerAreaId = careerArea.Id;
      criteria.CareerArea = careerArea.Name;

      ReportCriteria = criteria;
      ReportCriteria.ReportType = ReportTypes.CareerArea;

      Breadcrumbs = breadcrumbs;
      ModalHeaderOptions.BindBreadcrumbs(Breadcrumbs);

      CriteriaFilter.Bind(ReportTypes.CareerArea, criteria);

      CareerAreaNameLabel.Text = careerArea.Name;

      CareerAreaJobList.CareerAreaId = careerAreaId;
      CareerAreaJobList.ReportCriteria = criteria;
      CareerAreaJobList.ListSize = 50;
      CareerAreaJobList.EnablePaging = true;
      CareerAreaJobList.BindList(true);

      if (CareerAreaJobList.RecordCount == 0)
      {
        NoJobsLabel.Visible = true;
        NoJobsLabel.Text = CodeLocalise("NoJobs.Label", "No jobs have been advertised for this career in recent months in your area.");
      }
      else
      {
        NoJobsLabel.Visible = false;
        NoJobsLabel.Text = "";
      }

      CareerAreaSkillList.ReportCriteria = criteria;
      CareerAreaSkillList.ListSize = 20;
      CareerAreaSkillList.CareerAreaId = careerAreaId;
      CareerAreaSkillList.BindList(true, SkillTypes.Specialized);

      ModalPopup.Show();

      if (printFormat != ModalPrintFormat.None)
        PrintModal(printFormat);
    }

    /// <summary>
    /// Handles the Click event of the BookmarkLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void BookmarkLinkButton_Click(object sender, EventArgs e)
    {
      OnBookmarkClicked(new LightboxEventArgs
      {
        CommandName = "BookmarkCareerArea",
        CommandArgument = new LightboxEventCommandArguement
        {
          Id = CareerArea.Id,
          Breadcrumbs = Breadcrumbs,
          LightboxName = CareerArea.Name,
          LightboxLocation = "",//LocationLabel.Text
          CriteriaHolder = ReportCriteria
        }
      });
    }

    /// <summary>
    /// Handles the Click event of the SendEmailImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SendEmailImageButton_Click(object sender, EventArgs e)
    {
      OnSendEmailClicked(new LightboxEventArgs
      {
        CommandName = "SendCareerAreaEmail",
        CommandArgument = new LightboxEventCommandArguement
        {
          Id = CareerArea.Id,
          Breadcrumbs = Breadcrumbs,
          LightboxName = CareerArea.Name,
          LightboxLocation = "",//LocationLabel.Text
          CriteriaHolder = ReportCriteria
        }
      });
    }

    /// <summary>
    /// Handles the Click event of the PrintImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalHeaderOptions_OnPrintCommand(object sender, CommandEventArgs eventArgs)
    {
      var printFormat = (ModalPrintFormat)Enum.Parse(typeof(ModalPrintFormat), eventArgs.CommandArgument.ToString(), true);
      if (ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
      {
        PrintModal(printFormat);

        ModalPopup.Show();
      }
      else
      {
        OnPrintClicked(new LightboxEventArgs
        {
          CommandName = "PrintCareerArea",
          CommandArgument = new LightboxEventCommandArguement
          {
            Id = CareerArea.Id,
            Breadcrumbs = Breadcrumbs,
            LightboxName = CareerArea.Name,
            LightboxLocation = "",//LocationLabel.Text
            CriteriaHolder = ReportCriteria,
            PrintFormat = printFormat
          }
        });
      }
    }

    /// <summary>
    /// Handles the Click event of the CloseButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void CloseButton_Click(object sender, EventArgs e)
    {
      ModalPopup.Hide();
    }

    /// <summary>
    /// Handles the ItemClicked event of the CareerAreaModalBreadcrumb control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void CareerAreaModalBreadcrumb_ItemClicked(object sender, CommandEventArgs e)
    {
      long itemId;
      if (long.TryParse(e.CommandArgument.ToString(), out itemId))
        OnItemClicked(new CommandEventArgs(e.CommandName, new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = Breadcrumbs, MovingBack = true }));
    }

    /// <summary>
    /// Handles the Cancelled event of the Filter control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.FilterEventArgs"/> instance containing the event data.</param>
    protected void Filter_Cancelled(object sender, FilterEventArgs e)
    {
      var oldCriteria = e.CommandArgument.OldCriteria;
      var newCriteria = e.CommandArgument.NewCriteria;

      var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.CareerArea, LinkId = CareerArea.Id, LinkText = CareerArea.Name, Criteria = oldCriteria };
      var breadcrumbs = Breadcrumbs;
      breadcrumbs.Add(breadcrumb);

      OnItemClicked(new CommandEventArgs("ViewCareerArea",
                                         new LightboxEventCommandArguement
                                         {
                                           Id = oldCriteria.CareerAreaId.GetValueOrDefault(),
                                           Breadcrumbs = breadcrumbs,
                                           CriteriaHolder = newCriteria
                                         }));
    }

    /// <summary>
    /// Handles the ItemClicked event of the ModalList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "ViewJob":
        case "ViewSkill":
          long itemId;
          if (long.TryParse(e.CommandArgument.ToString(), out itemId))
          {
            var breadcrumb = new LightboxBreadcrumb
            {
              LinkType = ReportTypes.CareerArea,
              LinkId = CareerArea.Id,
              LinkText = CareerArea.Name,
              Criteria = ReportCriteria
            };
            var breadcrumbs = Breadcrumbs;
            breadcrumbs.Add(breadcrumb);

            OnItemClicked(new CommandEventArgs(e.CommandName, new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = breadcrumbs }));
          }
          break;
      }
    }

    private void Localise ()
    {
      Section1Title.Text = CodeLocalise("HiringDemand.Header", "Hiring demand over the last 12 months");
    }

    /// <summary>
    /// Prints the modal.
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintModal(ModalPrintFormat printFormat = ModalPrintFormat.Html)
    {
      var printModel = new ExplorerCareerAreaPrintModel
      {
        Name = CareerArea.Name,
        Location = "",
        Jobs = CareerAreaJobList.GetJobReports(JobListModes.CareerArea).ToList(),
        ReportCriteria = ReportCriteria
      };

      var skills = CareerAreaSkillList.GetSkills(SkillListSimple.SkillsListModes.CareerArea);
      printModel.SpecializedSkills = skills.Where(x => x.SkillType == SkillTypes.Specialized).ToList();
      printModel.SoftwareSkills = skills.Where(x => x.SkillType == SkillTypes.Software).ToList();
      printModel.FoundationSkills = skills.Where(x => x.SkillType == SkillTypes.Foundation).ToList();

      const string sessionKey = "Explorer:CareerAreaPrintModel";
      App.SetSessionValue(sessionKey, printModel);

      OpenPrintWindow(ReportTypes.CareerArea, printFormat);
    }
  }
}