#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.CareerExplorer.Code;
using Focus.Core.Views;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class JobPrint : UserControlBase
	{
		private ExplorerJobPrintModel _printModel;

		private int _rank = 1;

    public struct ProgramArea
    {
      public long Id;
      public string Name;
      public int Tier;

      public string ProgramAreaName { get { return Name; } }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
    /// <param name="printModel">The print model.</param>
		public void BindControls(ExplorerJobPrintModel printModel)
		{
			_printModel = printModel;

			JobNameLabel.Text = printModel.Name;
      CriteriaFilter.Bind(ReportTypes.Job, printModel.ReportCriteria);

		  JobIntroductionLabel.Text = printModel.JobIntroduction;
		  MortarIcon.Visible = printModel.JobRelatedToSchoolDegree;
	    MortarIcon.ImageUrl = UrlBuilder.DegreeProgramsImage();
      if (printModel.StarterJobLevel.HasValue)
      {
        StarterJobIcon.Visible = true;
        switch (printModel.StarterJobLevel.Value)
        {
          case StarterJobLevel.Many:
		        StarterJobIcon.ImageUrl = UrlBuilder.StarterJobsManyImage();
            break;

          case StarterJobLevel.Some:
		        StarterJobIcon.ImageUrl = UrlBuilder.StarterJobsSomeImage();
            break;
        }
      }

			JobTitlesLabel.Text = printModel.JobTitles;

			HiringTrendLiteral.Text = printModel.HiringTrend;
			HiringTrendArrowSpanLiteral.Text = printModel.HiringTrendArrowSpan;

			HiringDemandLiteral.Text = printModel.HiringDemand;
			HiringDemandImage.ImageUrl = printModel.HiringDemandImageUrl;

			SalaryRangeLiteral.Text = printModel.SalaryRange;
			if (!String.IsNullOrEmpty(printModel.SalaryRangeImageUrl))
			{
				SalaryRangeImage.ImageUrl = printModel.SalaryRangeImageUrl;
			}
			else
			{
				SalaryRangeImage.Visible = false;
			}

			if (!String.IsNullOrEmpty(printModel.SalaryNationwide))
			{
				SalaryNationwideLabelLiteral.Text = printModel.SalaryNationwideLabel;
				SalaryNationwideLiteral.Text = printModel.SalaryNationwide;
				SalaryNationwideImage.ImageUrl = printModel.SalaryNationwideImageUrl;
			}
			else
			{
				SalaryNationwidePlaceHolder.Visible = false;
      }

      #region Employers
      _rank = 1;
			EmployerColumnOneRepeater.DataSource = printModel.Employers.Take(10).ToList();
			EmployerColumnOneRepeater.DataBind();

			if (printModel.Employers.Count > 10)
			{
				EmployerColumnTwoRepeater.DataSource = printModel.Employers.Skip(10).ToList();
				EmployerColumnTwoRepeater.DataBind();
			}
			else
			{
				EmployerColumnTwoPlaceHolder.Visible = false;
      }
      #endregion

      #region Skills

      SpecializedSkillTypeLabel.Text = CodeLocalise("SpecializedSkills.Label", "SPECIALIZED SKILLS");
      SpecializedSkillsNoDataLiteral.Text =
        SoftwareSkillsNoDataLiteral.Text =
        FoundationSkillsNoDataLiteral.Text =
        CodeLocalise("NoSkills.Label",
                     "No required skills have been advertised for this occupation in the last 12 months.");
      OtherSpecializedSkillsNoDataLiteral.Text =
        OtherSoftwareSkillsNoDataLiteral.Text =
        OtherFoundationSkillsNoDataLiteral.Text =
        CodeLocalise("NoSkills.Label",
                     "No other skills were on the resumes of successful candidates for this occupation in the last 12 months.");

			_rank = 1;
			SpecializedSkillsColumnOneRepeater.DataSource =
				printModel.SpecializedSkills.OrderByDescending(x => x.DemandPercentile).Take(10).ToList();
			SpecializedSkillsColumnOneRepeater.DataBind();

      if (!printModel.SpecializedSkills.Any())
        SpecializedSkillsNoDataLiteral.Visible = true;

			if (printModel.SpecializedSkills.Count > 10)
			{
				SpecializedSkillsColumnTwoRepeater.DataSource =
					printModel.SpecializedSkills.OrderByDescending(x => x.DemandPercentile).Skip(10).Take(10).ToList();
				SpecializedSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				SpecializedSkillsColumnTwoPlaceHolder.Visible = false;
			}

			SoftwareSkillTypeLabel.Text = CodeLocalise("SoftwareSkills.Label", "SOFTWARE SKILLS");
			_rank = 1;
			SoftwareSkillsColumnOneRepeater.DataSource =
				printModel.SoftwareSkills.OrderByDescending(x => x.DemandPercentile).Take(10).ToList();
			SoftwareSkillsColumnOneRepeater.DataBind();

      if (!printModel.SoftwareSkills.Any())
        SoftwareSkillsNoDataLiteral.Visible = true;

			if (printModel.SoftwareSkills.Count > 10)
			{
				SoftwareSkillsColumnTwoRepeater.DataSource =
					printModel.SoftwareSkills.OrderByDescending(x => x.DemandPercentile).Skip(10).Take(10).ToList();
				SoftwareSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				SoftwareSkillsColumnTwoPlaceHolder.Visible = false;
			}

			FoundationSkillTypeLabel.Text = CodeLocalise("FoundationSkills.Label", "FOUNDATION SKILLS");
			_rank = 1;
			FoundationSkillsColumnOneRepeater.DataSource =
				printModel.FoundationSkills.OrderByDescending(x => x.DemandPercentile).Take(10).ToList();
      FoundationSkillsColumnOneRepeater.DataBind();

      if (!printModel.FoundationSkills.Any())
        FoundationSkillsNoDataLiteral.Visible = true;

			if (printModel.FoundationSkills.Count > 10)
			{
				FoundationSkillsColumnTwoRepeater.DataSource =
					printModel.FoundationSkills.OrderByDescending(x => x.DemandPercentile).Skip(10).Take(10).ToList();
				FoundationSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				FoundationSkillsColumnTwoPlaceHolder.Visible = false;
			}

			if (printModel.HeldSkills.Count > 0)
			{
				_rank = 1;
				HeldSpecializedSkillsRepeater.DataSource =
					printModel.HeldSkills.Where(x => x.SkillType == SkillTypes.Specialized).OrderByDescending(x => x.DemandPercentile).
						Take(5).ToList();
				HeldSpecializedSkillsRepeater.DataBind();

        if (printModel.HeldSkills.All(x => x.SkillType != SkillTypes.Specialized))
          OtherSpecializedSkillsNoDataLiteral.Visible = true;

				_rank = 1;
				HeldSoftwareSkillsRepeater.DataSource =
					printModel.HeldSkills.Where(x => x.SkillType == SkillTypes.Software).OrderByDescending(x => x.DemandPercentile).
						Take(5).ToList();
        HeldSoftwareSkillsRepeater.DataBind();

        if (printModel.HeldSkills.All(x => x.SkillType != SkillTypes.Software))
          OtherSoftwareSkillsNoDataLiteral.Visible = true;

        if (printModel.ShowOtherFoundationSkills)
        {
          _rank = 1;
          HeldFoundationSkillsRepeater.DataSource =
            printModel.HeldSkills.Where(x => x.SkillType == SkillTypes.Foundation).OrderByDescending(
              x => x.DemandPercentile).
              Take(5).ToList();
          HeldFoundationSkillsRepeater.DataBind();

          if (printModel.HeldSkills.All(x => x.SkillType != SkillTypes.Foundation))
            OtherFoundationSkillsNoDataLiteral.Visible = true;
        }
        else
          HeldFoundationSkillsPlaceHolder.Visible = false;
			}
			else
			{
				HeldSkillsPlaceHolder.Visible = false;
        HeldFoundationSkillsPlaceHolder.Visible = false;
      }
      #endregion

      #region Education

		  if (printModel.DegreeCertifications != null)
      {
        DegreeHeaderLiteral.Text = CodeLocalise("Degrees.Header", "DEGREES");
        DegreeIntroLabel.Text = printModel.DegreeIntro;

        if (printModel.DegreeCertifications.JobDegrees.Any(x => x.IsClientData))
        {
          ClientDegreeDisclaimerLiteral.Text = CodeLocalise("ClientDegreeDisclaimer.Text", string.Empty);
          ClientDegreeDisclaimerLiteral.Visible = true;
        }
        else
        {
          ClientDegreeDisclaimerLiteral.Visible = false;
        }

        if (App.Settings.ExplorerUseDegreeTiering)
        {
          if (App.Settings.Theme == FocusThemes.Education && printModel.ProgramAreaDegrees.IsNotNullOrEmpty())
          {
            ProgramAreaPlaceHolder.Visible = ProgramAreaTier2PlaceHolder.Visible = true;
            ProgramAreaHeaderLiteral.Text = CodeLocalise("ClientProgramAreaDegreesSpecific.Header",
                                                         "#SCHOOLNAME# PROGRAMS OF STUDY SPECIFIC TO THIS OCCUPATION").
              ToUpper();


            ProgramAreaTier2HeaderLiteral.Text = CodeLocalise("ClientProgramAreaDegreesRelated.Header",
                                                              "#SCHOOLNAME# PROGRAMS OF STUDY RELATED TO THIS OCCUPATION")
              .ToUpper();

            var degreeIds =
              printModel.DegreeCertifications.JobDegreeEducationLevels.Where(x => x.Tier == 1).Select(jd => jd.DegreeId)
                .Distinct();

            ProgramAreaRepeater.DataSource =
              printModel.ProgramAreaDegrees.Where(pad => degreeIds.Contains(pad.DegreeId)).Select(
                pa => new ProgramArea {Id = pa.ProgramAreaId, Name = pa.ProgramAreaName, Tier = 1}).Distinct().ToList();
            ProgramAreaRepeater.DataBind();
            ProgramAreaRepeater.Visible = true;

            degreeIds =
              printModel.DegreeCertifications.JobDegreeEducationLevels.Where(x => x.Tier == 2).Select(jd => jd.DegreeId)
                .Distinct();

            ProgramAreaTier2Repeater.DataSource =
              printModel.ProgramAreaDegrees.Where(pad => degreeIds.Contains(pad.DegreeId)).Select(
                pa => new ProgramArea {Id = pa.ProgramAreaId, Name = pa.ProgramAreaName, Tier = 2}).Distinct().ToList();
            ProgramAreaTier2Repeater.DataBind();
            ProgramAreaTier2Repeater.Visible = true;
          }
          else
          {
            if (printModel.DegreeCertifications.JobDegrees.Any(x => x.Tier == 1))
            {
              DegreePlaceHolder.Visible = true;
              DegreeHeaderLiteral.Text = CodeLocalise("Tier1Degrees.Header",
                                                      "Programs of study specific to this occupation");
              ClientDegreeRepeater.DataSource =
                printModel.DegreeCertifications.JobDegrees.Where(x => x.Tier == 1).ToList();
              ClientDegreeRepeater.DataBind();
            }
            if (printModel.DegreeCertifications.JobDegrees.Any(x => x.Tier == 2))
            {
              DegreeTier2PlaceHolder.Visible = true;
              DegreeTier2HeaderLiteral.Text = CodeLocalise("Tier2Degrees.Header",
                                                           "Programs of study that may be related to this occupation");
              DegreeTier2Repeater.DataSource =
                printModel.DegreeCertifications.JobDegrees.Where(x => x.Tier == 2).ToList();
              DegreeTier2Repeater.DataBind();
            }
          }

        }
        else
        {
          if (printModel.ProgramAreaDegrees.IsNotNullOrEmpty())
          {
            ProgramAreaHeaderLiteral.Text = CodeLocalise("Degrees.Header", "DEGREES");

            ProgramAreaPlaceHolder.Visible = true;
            ProgramAreaRepeater.DataSource = printModel.ProgramAreaDegrees
                                                       .Select(pa => new ProgramArea { Id = pa.ProgramAreaId, Name = pa.ProgramAreaName })
                                                       .Distinct()
                                                       .ToList();
            ProgramAreaRepeater.DataBind();
            ProgramAreaRepeater.Visible = true;
          }
          else
          {
            DegreePlaceHolder.Visible = true;
            DegreeHeaderLiteral.Text = CodeLocalise("Degrees.Header", "DEGREES");
            ClientDegreeRepeater.DataSource = printModel.DegreeCertifications.JobDegrees;
            ClientDegreeRepeater.DataBind();
          }
        }

        DegreePanel.Visible = printModel.DegreeCertifications.JobDegrees.Any();

        if (printModel.DegreeCertifications.JobCertifications.Count > 0)
        {
          CertificationRepeater.DataSource =
            printModel.DegreeCertifications.JobCertifications.OrderByDescending(
              x => x.DemandPercentile).Take(5).ToList();
          CertificationRepeater.DataBind();
        }
        else
        {
          CertificationPanel.Visible = false;
        }
			}
			else
			{
				DegreePanel.Visible = false;
				CertificationPanel.Visible = false;
			}

			if (printModel.EducationRequirementsVisible)
			{
				HighSchoolTechnicalTrainingLiteral.Text = printModel.HighSchoolTechnicalTrainingSpan;
				AssociatesDegreeLiteral.Text = printModel.AssociatesDegreeSpan;
				BachelorsDegreeLiteral.Text = printModel.BachelorsDegreeSpan;
				GraduateProfessionalDegreeLiteral.Text = printModel.GraduateProfessionalDegreeSpan;
			}
			else
			{
				EducationRequirementsPlaceHolder.Visible = false;
			}

			LessThanTwoLiteral.Text = printModel.LessThanTwoSpan;
			TwoToFiveLiteral.Text = printModel.TwoToFiveSpan;
			FiveToEightLiteral.Text = printModel.FiveToEightSpan;
			EightPlusLiteral.Text = printModel.EightPlusSpan;

      #endregion

      _rank = 1;
			JobRepeater.DataSource = printModel.SimilarJobs;
			JobRepeater.DataBind();

			if (printModel.CareerPathTo.Count > 0)
			{
				BindCareerPathTo();
			}
			else
			{
				CareerPathToPanel.Visible = false;
			}

			if (printModel.CareerPathFrom.Count > 0)
			{
				BindCareerPathFrom();
			}
			else
			{
				CareerPathFromPanel.Visible = false;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the EmployerRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var employer = (EmployerJobViewDto)e.Item.DataItem;

				var countLiteral = (Literal)e.Item.FindControl("EmployerCountLiteral");
				countLiteral.Text = _rank.ToString();

				var openingsLiteral = (Literal)e.Item.FindControl("EmployerOpeningsLiteral");
				openingsLiteral.Text = String.Format("({0}{1})", employer.Positions.ToString("#,#"),
																						 _rank == 1 ? " openings" : String.Empty);

				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the SkillsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void SkillsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var rankLiteral = (Literal)e.Item.FindControl("SkillRankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the ProgramAreaRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void ProgramAreaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var dataItem = (ProgramArea)e.Item.DataItem;
        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");
        var degreeCertifications = _printModel.DegreeCertifications;
        var programAreaDegrees = _printModel.ProgramAreaDegrees
                                   .Where(pad => pad.ProgramAreaId == dataItem.Id)
                                   .Select(pad => pad.DegreeId)
                                   .ToList();
        if (dataItem.Tier == 0)
          degreeEducationLevelRepeater.DataSource = degreeCertifications.DegreeEducationLevels(programAreaDegrees);
        else
        {
          programAreaDegrees =
            _printModel.ProgramAreaDegrees.Where(pad => pad.ProgramAreaId == dataItem.Id).Select(pad => pad.DegreeId).
              ToList();
          degreeEducationLevelRepeater.DataSource =
            degreeCertifications.ProgramAreaDegreeEducationLevelsByTier(programAreaDegrees, dataItem.Tier);
        }
        degreeEducationLevelRepeater.DataBind();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the ProgramAreaDegreeEducationLevelRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void ProgramAreaDegreeEducationLevelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var degreeEducationLevel = (DegreeEducationLevelDto)e.Item.DataItem;
        var degreeEducationLevelLinkButton = (Literal)e.Item.FindControl("DegreeEducationLevelNameLiteral");

        degreeEducationLevelLinkButton.Text = degreeEducationLevel.Name;
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void DegreeRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
		  DegreeRepeaterBinding(e, 1);
		}

    protected void DegreeRepeater_ItemDataBoundTier2(object sender, RepeaterItemEventArgs e)
    {
      DegreeRepeaterBinding(e, 2);
    }

    /// <summary>
    /// Degrees the repeater binding.
    /// </summary>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    /// <param name="tier">The tier.</param>
    private void DegreeRepeaterBinding(RepeaterItemEventArgs e, int tier)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var degree = (DegreeDto)e.Item.DataItem;

        var degreeCertifications = _printModel.DegreeCertifications;
        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");
        degreeEducationLevelRepeater.DataSource = App.Settings.ExplorerUseDegreeTiering ? degreeCertifications.DegreeEducationLevelsByTier(degree.Id.Value, tier) : degreeCertifications.DegreeEducationLevels(degree.Id.Value);
        degreeEducationLevelRepeater.DataBind();
      }
    }
    
    /// <summary>
    /// Handles the ItemDataBound event of the DegreeEducationLevelRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void DegreeEducationLevelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var degreeEducationLevel = (DegreeEducationLevelDto)e.Item.DataItem;

				var degreeEducationLevelNameLiteral = (Literal)e.Item.FindControl("DegreeEducationLevelNameLiteral");
				switch (degreeEducationLevel.EducationLevel)
				{
					case ExplorerEducationLevels.Certificate:
						degreeEducationLevelNameLiteral.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Certificate");
						break;

					case ExplorerEducationLevels.Associate:
						degreeEducationLevelNameLiteral.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Associate");
						break;

					case ExplorerEducationLevels.Bachelor:
						degreeEducationLevelNameLiteral.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Bachelor");
						break;

					case ExplorerEducationLevels.PostBachelorCertificate:
						degreeEducationLevelNameLiteral.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Post-Baccalaureate Certificate");
						break;

					case ExplorerEducationLevels.GraduateProfessional:
						degreeEducationLevelNameLiteral.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Graduate or Professional");
						break;

					case ExplorerEducationLevels.PostMastersCertificate:
						degreeEducationLevelNameLiteral.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Post-Master's Certificate");
						break;

					case ExplorerEducationLevels.Doctorate:
						degreeEducationLevelNameLiteral.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Doctorate");
						break;
				}
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the JobRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void JobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

      var jobReport = (ExplorerJobReportView) e.Item.DataItem;

      var rankLiteral = (Literal) e.Item.FindControl("RankLiteral");
      rankLiteral.Text = _rank.ToString();
      _rank++;

      var salaryImage = (Image) e.Item.FindControl("SalaryImage");
      switch (jobReport.ReportData.SalaryTrend)
      {
        case "$$$":
          salaryImage.ImageUrl = UrlBuilder.JobSalaryHighImage();
          break;

        case "$$":
          salaryImage.ImageUrl = UrlBuilder.JobSalaryMediumImage();
          break;

        case "$":
          salaryImage.ImageUrl = UrlBuilder.JobSalaryLowImage();
          break;

        default:
          salaryImage.Visible = false;
          break;
      }

      var mortarIcon = (Image)e.Item.FindControl("MortarIcon");
	    mortarIcon.ImageUrl = UrlBuilder.DegreeProgramsImage();
      mortarIcon.Visible = (App.Settings.Theme == FocusThemes.Education && jobReport.RelatedDegreeAvailableAtCurrentSchool);

      if (jobReport.ReportData.StarterJob.HasValue)
      {
        var icon = (Image) e.Item.FindControl("StarterJobIcon");
        icon.Visible = true;
				switch (jobReport.ReportData.StarterJob.Value)
				{
					case StarterJobLevel.Many:
						icon.ImageUrl = UrlBuilder.StarterJobsManyImage();
						break;
					case StarterJobLevel.Some:
						icon.ImageUrl = UrlBuilder.StarterJobsSomeImage();
						break;
				}
      }
    }

    /// <summary>
    /// Binds the career path to.
    /// </summary>
	  private void BindCareerPathTo()
		{
			CareerPathToSecondLevelLeftPanel.Visible =
				CareerPathToSecondLevelMiddleLeftPanel.Visible =
				CareerPathToSecondLevelMiddleRightPanel.Visible = CareerPathToSecondLevelRightPanel.Visible = true;

			CareerPathToTopLevelPanel.CssClass = "careerpathChartItem top topArrowDown";

			CareerPathToSecondLevelLeftPanel.CssClass = "careerpathChartItem left";
			CareerPathToSecondLevelMiddleLeftPanel.CssClass = "careerpathChartItem";
			CareerPathToSecondLevelMiddleRightPanel.CssClass = "careerpathChartItem";
			CareerPathToSecondLevelRightPanel.CssClass = "careerpathChartItem right";

			CareerPathToSecondLevelMiddleLeftArrowLineLiteral.Text = "<span class=\"arrowLineMiddle\"></span>";

			var careerPath = _printModel.CareerPathTo;
			CareerPathToJobNameLabel.Text = _printModel.Name;
			switch (careerPath.Count)
			{
				case 1:
					CareerPathToSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems1 centre";
          CareerPathToSecondLevelLeftPanel.Visible = false;
          CareerPathToSecondLevelMiddleLeftArrowLineLiteral.Text = String.Empty;
          CareerPathToSecondLevelMiddleLeftLiteral.Text = careerPath[0].Job.Name;
          CareerPathToSecondLevelMiddleRightPanel.Visible = false;
          CareerPathToSecondLevelRightPanel.Visible = false;
          break;

				case 2:
					CareerPathToSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems2 centre";
          CareerPathToSecondLevelLeftLiteral.Text = careerPath[0].Job.Name;
          CareerPathToSecondLevelMiddleLeftPanel.Visible = false;
          CareerPathToSecondLevelMiddleRightPanel.Visible = false;
          CareerPathToSecondLevelRightLiteral.Text = careerPath[1].Job.Name;
          break;

				case 3:
					CareerPathToSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems3 centre";
          CareerPathToSecondLevelLeftLiteral.Text = careerPath[0].Job.Name;
          CareerPathToSecondLevelMiddleLeftLiteral.Text = careerPath[1].Job.Name;
          CareerPathToSecondLevelMiddleRightPanel.Visible = false;
          CareerPathToSecondLevelRightLiteral.Text = careerPath[2].Job.Name;
          break;

				case 4:
					CareerPathToSecondLevelPanel.CssClass = "careerPathRowWrap";
          CareerPathToSecondLevelLeftLiteral.Text = careerPath[0].Job.Name;
          CareerPathToSecondLevelMiddleLeftLiteral.Text = careerPath[1].Job.Name;
          CareerPathToSecondLevelMiddleRightLiteral.Text = careerPath[2].Job.Name;
          CareerPathToSecondLevelRightLiteral.Text = careerPath[3].Job.Name;
          break;
			}
		}

    /// <summary>
    /// Binds the career path from.
    /// </summary>
		private void BindCareerPathFrom()
		{
			CareerPathFromSecondLevelLeftPanel.Visible =
				CareerPathFromSecondLevelMiddleLeftPanel.Visible =
				CareerPathFromSecondLevelMiddleRightPanel.Visible = CareerPathFromSecondLevelRightPanel.Visible = true;

			CareerPathFromTopLevelPanel.CssClass = "careerpathChartItem top topArrowUp";

			CareerPathFromSecondLevelLeftPanel.CssClass = "careerpathChartItem left";
			CareerPathFromSecondLevelMiddleLeftPanel.CssClass = "careerpathChartItem";
			CareerPathFromSecondLevelMiddleRightPanel.CssClass = "careerpathChartItem";
			CareerPathFromSecondLevelRightPanel.CssClass = "careerpathChartItem right";

			CareerPathFromSecondLevelMiddleLeftArrowLineLiteral.Text = "<span class=\"arrowLineMiddle\"></span>";

			var careerPath = _printModel.CareerPathFrom;
			CareerPathFromJobNameLabel.Text = _printModel.Name;
			switch (careerPath.Count)
			{
				case 1:
					CareerPathFromSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems1 centre";
          CareerPathFromSecondLevelLeftPanel.Visible = false;
          CareerPathFromSecondLevelMiddleLeftArrowLineLiteral.Text = String.Empty;
          CareerPathFromSecondLevelMiddleLeftLiteral.Text = careerPath[0].Job.Name;
          CareerPathFromSecondLevelMiddleRightPanel.Visible = false;
          CareerPathFromSecondLevelRightPanel.Visible = false;
          break;

				case 2:
					CareerPathFromSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems2 centre";
          CareerPathFromSecondLevelLeftLiteral.Text = careerPath[0].Job.Name;
          CareerPathFromSecondLevelMiddleLeftPanel.Visible = false;
          CareerPathFromSecondLevelMiddleRightPanel.Visible = false;
          CareerPathFromSecondLevelRightLiteral.Text = careerPath[1].Job.Name;
          break;

				case 3:
					CareerPathFromSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems3 centre";
          CareerPathFromSecondLevelLeftLiteral.Text = careerPath[0].Job.Name;
          CareerPathFromSecondLevelMiddleLeftLiteral.Text = careerPath[1].Job.Name;
          CareerPathFromSecondLevelMiddleRightPanel.Visible = false;
          CareerPathFromSecondLevelRightLiteral.Text = careerPath[2].Job.Name;
          break;

				case 4:
					CareerPathFromSecondLevelPanel.CssClass = "careerPathRowWrap";
          CareerPathFromSecondLevelLeftLiteral.Text = careerPath[0].Job.Name;
          CareerPathFromSecondLevelMiddleLeftLiteral.Text = careerPath[1].Job.Name;
          CareerPathFromSecondLevelMiddleRightLiteral.Text = careerPath[2].Job.Name;
          CareerPathFromSecondLevelRightLiteral.Text = careerPath[3].Job.Name;
          break;
			}			
		}
	}
}