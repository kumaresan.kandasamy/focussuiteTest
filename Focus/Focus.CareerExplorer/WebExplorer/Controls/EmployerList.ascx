<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerList.ascx.cs"
    Inherits=" Focus.CareerExplorer.WebExplorer.Controls.EmployerList" %>
<table width="100%" class="genericTable multiToOne" role="presentation">
    <tr>
        <td align="left" valign="top">
            <asp:Repeater ID="EmployerColumnOneRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound"
                OnItemCommand="EmployerRepeater_ItemCommand">
                <HeaderTemplate>
                    <table width="100%" role="presentation">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td valign="top">
                            <strong>
                                <asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp;
                                <asp:LinkButton ID="EmployerNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("EmployerId").ToString() %>'
                                    CommandName="ViewEmployer"><%# Eval("EmployerName") %></asp:LinkButton>&nbsp;
                                <asp:Literal ID="EmployerOpeningsLiteral" runat="server" />&nbsp; </strong>
                            <asp:PlaceHolder ID="PreferredEmployerPlaceHolder" runat="server">
                                <asp:Image runat="server" ID="PreferredEmployerImage" CssClass="toolTipNew" AlternateText="Preferred Employer Image"/>
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </td>
        <asp:PlaceHolder ID="EmployerColumnTwoPlaceHolder" runat="server">
            <td width="50%" align="left" valign="top">
                <asp:Repeater ID="EmployerColumnTwoRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound"
                    OnItemCommand="EmployerRepeater_ItemCommand">
                    <HeaderTemplate>
                        <table width="100%" role="presentation">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td valign="top">
                                <strong>
                                    <asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp;
                                    <asp:LinkButton ID="EmployerNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("EmployerId").ToString() %>'
                                        CommandName="ViewEmployer"><%# Eval("EmployerName") %></asp:LinkButton>&nbsp;
                                    <asp:Literal ID="EmployerOpeningsLiteral" runat="server" />
                                </strong>
                                <asp:PlaceHolder ID="PreferredEmployerPlaceHolder" runat="server">
                                    <asp:Image runat="server" ID="PreferredEmployerImage" CssClass="toolTipNew" AlternateText="Preferred Employer Image" />
                                </asp:PlaceHolder>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </asp:PlaceHolder>
    </tr>
</table>
