<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchResults.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.SearchResults" %>

<table width="600px">
  <tr>
    <td>
      <h1><%= HtmlLocalise("Heading.Text", "Search results")%></h1>
      <div><%= HtmlLocalise("YouSearchedFor.Text", "You searched for <strong>Biomedical Engineering</strong>.")%></div>
      <br />
      <table width="100%" class="numberedAccordion on">
        <tr class="multipleAccordionTitle">
          <td  colspan="2"><%= HtmlLocalise("JobsResults.Text", "Jobs (3 results)")%></td>
          <td class="column3" align="right" valign="top"><a class="show" href="#"><%= HtmlLocalise("Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide.Text", "HIDE")%></a></td>
        </tr>
        <tr class="accordionContent open">
          <td width="10"></td>
          <td colspan="2">
            <table>
              <tr>
                <td><a href="#"><%= HtmlLocalise("EducationLevel.Text", "Biomedical Engineer")%></a></td>
              </tr>
              <tr>
                <td><a href="#"><%= HtmlLocalise("EducationLevel.Text", "Medical Device Designer")%></a></td>
              </tr>
              <tr>
                <td><a href="#"><%= HtmlLocalise("EducationLevel.Text", "Some Other Job")%></a></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle on">
          <td  colspan="2"><%= HtmlLocalise("JobsResults.Text", "Degrees & certificates (10 results)")%></td>
          <td class="column3" align="right" valign="top"><a class="show" href="#"><%= HtmlLocalise("Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide.Text", "HIDE")%></a></td>
        </tr>
        <tr class="accordionContent open">
          <td width="10"></td>
          <td colspan="2">
            <table>
              <tr>
                <td width="400"><a href="#"><%= HtmlLocalise("DegreeAndCertificateResult.Text", "Associate's Degree in Medical Device Design")%></a></td>
                <td><a href="#"><%= HtmlLocalise("DegreeAndCertificateResult.Text", "Some Other Degree Name")%></a></td>
              </tr>
              <tr>
                <td width="400"><a href="#"><%= HtmlLocalise("DegreeAndCertificateResult.Text", "Associate's Degree in Medical Device Design")%></a></td>
                <td><a href="#"><%= HtmlLocalise("DegreeAndCertificateResult.Text", "Some Other Degree Name")%></a></td>
              </tr>
              <tr>
                <td width="400"><a href="#"><%= HtmlLocalise("DegreeAndCertificateResult.Text", "Associate's Degree in Medical Device Design")%></a></td>
                <td><a href="#"><%= HtmlLocalise("DegreeAndCertificateResult.Text", "Some Other Degree Name")%></a></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr class="multipleAccordionTitle on">
          <td  colspan="2"><%= HtmlLocalise("JobsResults.Text", "Skills (13 results)")%></td>
          <td class="column3" align="right" valign="top"><a class="show" href="#"><%= HtmlLocalise("Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Hide.Text", "HIDE")%></a></td>
        </tr>
        <tr class="accordionContent open">
          <td width="10"></td>
          <td colspan="2">
            <table>
              <tr>
                <td width="400"><a href="#"><%= HtmlLocalise("SkillName.Text", "Skill name")%></a></td>
                <td><a href="#"><%= HtmlLocalise("SkillName.Text", "Skill name")%></a></td>
              </tr>
              <tr>
                <td width="400"><a href="#"><%= HtmlLocalise("SkillName.Text", "Skill name")%></a></td>
                <td><a href="#"><%= HtmlLocalise("SkillName.Text", "Skill name")%></a></td>
              </tr>
              <tr>
                <td width="400"><a href="#"><%= HtmlLocalise("SkillName.Text", "Skill name")%></a></td>
                <td><a href="#"><%= HtmlLocalise("SkillName.Text", "Skill name")%></a></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>