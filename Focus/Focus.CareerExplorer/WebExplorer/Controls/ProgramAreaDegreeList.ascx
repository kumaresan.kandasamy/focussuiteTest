﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgramAreaDegreeList.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.ProgramAreaDegreeList" %>
<ul>
  <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound" OnItemCommand="ModalList_ItemClicked">
	  <ItemTemplate>
	    <li>
        <asp:LinkButton ID="DegreeEducationLevelLinkButton" CssClass="modalLink" runat="server" CommandName="ViewDegreeCertification"></asp:LinkButton>
      </li>
	  </ItemTemplate>
  </asp:Repeater>
</ul>
<br />