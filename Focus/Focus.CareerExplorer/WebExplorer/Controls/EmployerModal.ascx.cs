#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Focus.Core.Views;
using Focus.Services.Core;
using Framework.Core;

using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models.Career;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.CareerExplorer.Code;

#endregion 

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class EmployerModal : ExplorerModalControl
	{
		public EmployerDto Employer
		{
			get { return GetViewStateValue<EmployerDto>("EmployerModal:Employer", null); }
			set { SetViewStateValue("EmployerModal:Employer", value); }
		}

		public ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue<ExplorerCriteria>("EmployerModal:ReportCriteria"); }
			set { SetViewStateValue("EmployerModal:ReportCriteria", value); }
		}

		public List<LightboxBreadcrumb> Breadcrumbs
		{
			get { return GetViewStateValue("EmployerModal:Breadcrumb", new List<LightboxBreadcrumb>()); }
			set { SetViewStateValue("EmployerModal:Breadcrumb", value); }
		}

    private List<ExplorerEmployerJobReportView> EmployerJobs
		{
      get { return GetViewStateValue("EmployerModal:EmployerJobs", new List<ExplorerEmployerJobReportView>()); }
			set { SetViewStateValue("EmployerModal:EmployerJobs", value); }
		}

		public StateAreaViewDto StateArea
		{
			get { return GetViewStateValue<StateAreaViewDto>("JobModal:StateArea", null); }
			set { SetViewStateValue("JobModal:StateArea", value); }
		}

		private int _rank = 1;

		protected string SectionOneToolTip { get; set; }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      ReadyToMakeAMovePlaceHolder.Visible = ViewCurrentPostingsLinkButton.Visible = (App.Settings.Module != FocusModules.Explorer);
      SectionOneTitle.Text = CodeLocalise("Global.HiringDemand.Header", "Hiring demand for the last 12 months");
        }

    /// <summary>
    /// Shows the specified employer id.
    /// </summary>
    /// <param name="employerId">The employer id.</param>
    /// <param name="criteria">The criteria.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="printFormat">The format to print the modal</param>
    public void Show(long employerId, ExplorerCriteria criteria, List<LightboxBreadcrumb> breadcrumbs, ModalPrintFormat printFormat = ModalPrintFormat.None)
    {
      Breadcrumbs = breadcrumbs;
			var employer = ServiceClientLocator.ExplorerClient(App).GetEmployer(employerId);

      Employer = employer;
      criteria.Employer = Employer.Name;
      criteria.EmployerId = Employer.Id;
      ReportCriteria = criteria;
      ReportCriteria.ReportType = ReportTypes.Employer;
      CriteriaFilter.Bind(ReportTypes.Employer, criteria);

			StateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(criteria.StateAreaId);

      ModalHeaderOptions.BindBreadcrumbs(Breadcrumbs);

			EmployerNameLabel.Text = employer.Name;


      _rank = 1;

      // If coming from an internship modal then show the internship list and not the job list
      if (criteria.InternshipCategoryId.IsNotNull())
      {
        InternshipsList.ListMode = InternshipListModes.Employer;
        InternshipsList.ReportCriteria = criteria;
        InternshipsList.BindList(true);
        InternshipsList.Visible = true;
        EmployerJobRepeater.Visible = false;
        SectionOneTitle.Text = CodeLocalise("Internships.Header", "Internships for the last 12 months");
        SectionOneToolTip = CodeLocalise("Internships.TooltipText",
                                              "Internships tool tip required");
      }
      else
      {
        var employerJobs = ServiceClientLocator.ExplorerClient(App).GetEmployerJobs(criteria);
        EmployerJobs = employerJobs;

        EmployerJobRepeater.DataSource = employerJobs;
        EmployerJobRepeater.DataBind();

        InternshipsList.Visible = false;
        EmployerJobRepeater.Visible = true;
        SectionOneTitle.Text = CodeLocalise("Global.HiringDemand.Header", "Hiring demand for the last 12 months");
        SectionOneToolTip = CodeLocalise("HiringDemand.TooltipText",
																							"An #BUSINESS#:LOWER�s need for workers to fill their jobs is known as hiring demand. Select hiring demand to see the number of jobs, either nationally or by your geographic search area that #BUSINESSES#:LOWER posted over the last 12 months. Hiring demand is rated as low, medium, high, or very high for the area in your geographic search.");
      }

			EmployerSkillList.ReportCriteria = criteria;
			EmployerSkillList.ListSize = 20;
			EmployerSkillList.EmployerId = employerId;
			EmployerSkillList.BindList(true, SkillTypes.Specialized);

      if (App.Settings.Module != FocusModules.Explorer)
      {
        if (App.Settings.ExplorerReadyToMakeAMoveBubble)
        {
          ReadyToMakeAMovePlaceHolder.Visible = true;
					ViewCurrentPositionsLiteral.Text = HtmlLocalise("SeeCurrentPostingsForEmployer.Text", "See current postings for<br />this #BUSINESS#:LOWER �");
        }
        else
        {
          ReadyToMakeAMovePlaceHolder.Visible = false;
					ViewCurrentPositionsLiteral.Text = HtmlLocalise("ReadySeeCurrentPostingsForEmployer.Text", "Ready to make a move?<br /><br />See current postings for<br />this #BUSINESS#:LOWER �");
        }
      }

			ModalPopup.Show();

			if (printFormat != ModalPrintFormat.None)
        PrintModal(printFormat);
		}

    /// <summary>
    /// Handles the Click event of the BookmarkLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void BookmarkLinkButton_Click(object sender, EventArgs e)
		{
			OnBookmarkClicked(new LightboxEventArgs
			                  	{
			                  		CommandName = "BookmarkEmployer",
			                  		CommandArgument = new LightboxEventCommandArguement
			                  		                  	{
			                  		                  		Id = Employer.Id.Value,
			                  		                  		Breadcrumbs = Breadcrumbs,
			                  		                  		LightboxName = EmployerNameLabel.Text,
                                                  LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                  		                  	}
			                  	});
		}

    /// <summary>
    /// Handles the Click event of the SendEmailImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendEmailImageButton_Click(object sender, EventArgs e)
		{
			OnSendEmailClicked(new LightboxEventArgs
			                   	{
			                   		CommandName = "SendEmployerEmail",
			                   		CommandArgument = new LightboxEventCommandArguement
			                   		                  	{
			                   		                  		Id = Employer.Id.Value,
			                   		                  		Breadcrumbs = Breadcrumbs,
                                                  LightboxName = EmployerNameLabel.Text,
                                                  LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                   		                  	}
			                   	});
		}

    /// <summary>
    /// Handles the Click event of the PrintImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalHeaderOptions_OnPrintCommand(object sender, CommandEventArgs eventArgs)
    {
      var printFormat = (ModalPrintFormat)Enum.Parse(typeof(ModalPrintFormat), eventArgs.CommandArgument.ToString(), true);
			if (ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				PrintModal(printFormat);

				ModalPopup.Show();
			}
			else
			{
				OnPrintClicked(new LightboxEventArgs
				               	{
				               		CommandName = "PrintEmployer",
				               		CommandArgument = new LightboxEventCommandArguement
				               		                  	{
				               		                  		Id = Employer.Id.Value,
                                                Breadcrumbs = Breadcrumbs,
                                                LightboxName = EmployerNameLabel.Text,
                                                LightboxLocation = "",//LocationLabel.Text
                                                CriteriaHolder = ReportCriteria,
                                                PrintFormat = printFormat
				               		                  	}
				               	});
			}
		}

    /// <summary>
    /// Handles the Click event of the CloseButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Click(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

    /// <summary>
    /// Handles the Click event of the ViewCurrentPostingsLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ViewCurrentPostingsLinkButton_Click(object sender, EventArgs e)
		{
      var jobListSearchCriteria = new SearchCriteria();

			jobListSearchCriteria.KeywordCriteria = new KeywordCriteria(ReportCriteria.Employer)
			                                        	{
			                                        		SearchLocation = PostingKeywordScopes.Employer
			                                        	};

      jobListSearchCriteria.PostingAgeCriteria = new PostingAgeCriteria
      {
        PostingAgeInDays = App.Settings.JobSearchPostingDate
      };
      
			if (StateArea.StateCode.IsNotNullOrEmpty())
			{
				jobListSearchCriteria.JobLocationCriteria = new JobLocationCriteria
				                                            	{
																												Area = new AreaCriteria
				                                            		       	{
				                                            		       		MsaId = StateArea.AreaSortOrder == 0 ? (long?) null : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.StateMetropolitanStatisticalAreas).Where(x => x.ExternalId == StateArea.AreaCode).Select(x => x.Id).FirstOrDefault(),
				                                            		       		StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.ExternalId == StateArea.StateCode).Select(x => x.Id).FirstOrDefault()
				                                            		       	},
				                                            		SelectedStateInCriteria = StateArea.StateCode
				                                            	};
			}

      App.SetSessionValue("Career:SearchCriteria", jobListSearchCriteria);

      App.SetCookieValue("Career:ManualSearch", "true");
      Response.RedirectToRoute("JobSearchResults");
		}

    /// <summary>
    /// Handles the ItemDataBound event of the EmployerJobRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerJobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
        var employerJob = (ExplorerEmployerJobReportView)e.Item.DataItem;

				var rankLiteral = (Literal)e.Item.FindControl("JobRankLiteral");
				rankLiteral.Text = _rank.ToString();

				var openingsLiteral = (Literal)e.Item.FindControl("JobOpeningsLiteral");
        openingsLiteral.Text = String.Format("({0}{1})", employerJob.ReportData.Positions.ToString("#,#"),
																						 _rank == 1 ? " " + CodeLocalise("JobOpenings.Text", "openings") : String.Empty);

				var jobSearchLinkButton = (LinkButton) e.Item.FindControl("JobSearchLinkButton");
				jobSearchLinkButton.Visible = App.Settings.Module == FocusModules.CareerExplorer;


        if (App.Settings.Theme == FocusThemes.Education && employerJob.RelatedDegreeAvailableAtCurrentSchool)
        {
          e.Item.FindControl("DegreeProgramPanel").Visible = true;
        }

        if (employerJob.ReportData.StarterJob.HasValue)
        {
          e.Item.FindControl("StarterJobPanel").Visible = true;
          var starterJobTootTipDiv = (HtmlGenericControl)e.Item.FindControl("StarterJobToolTipDiv");
	        var starterJobToolTipImage = (HtmlGenericControl) e.Item.FindControl("StarterJobToolTipImage");
          if (employerJob.ReportData.StarterJob.Value == StarterJobLevel.Many)
          {
            starterJobTootTipDiv.Attributes.Add("class", "tooltipWithArrow starterJobMany");
						starterJobToolTipImage.Attributes.Add("title", CodeLocalise("StarterJobMany.TooltipText", "This occupation has substantial openings for new entrants with less than 2 years of work experience in this field."));
          }
          else if (employerJob.ReportData.StarterJob.Value == StarterJobLevel.Some)
          {
            starterJobTootTipDiv.Attributes.Add("class", "tooltipWithArrow starterJobSome");
            starterJobToolTipImage.Attributes.Add("title", CodeLocalise("StarterJobSome.TooltipText", "This occupation has substantial openings for new entrants with less than 3 years of work experience in this field."));
          }
        }
				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemClicked event of the EmployerModalBreadcrumb control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void EmployerModalBreadcrumb_ItemClicked(object sender, CommandEventArgs e)
		{
			long itemId;
			if (long.TryParse(e.CommandArgument.ToString(), out itemId))
			{
				OnItemClicked(new CommandEventArgs(e.CommandName,
																					 new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = Breadcrumbs, MovingBack = true}));
			}
		}

    /// <summary>
    /// Handles the Cancelled event of the Filter control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.FilterEventArgs"/> instance containing the event data.</param>
    protected void Filter_Cancelled(object sender, FilterEventArgs e)
    {
      var oldCriteria = e.CommandArgument.OldCriteria;
      var newCriteria = e.CommandArgument.NewCriteria;

      var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.Employer, LinkId = Employer.Id.GetValueOrDefault(), LinkText = Employer.Name, Criteria = oldCriteria };
      var breadcrumbs = Breadcrumbs;
      breadcrumbs.Add(breadcrumb);

      OnItemClicked(new CommandEventArgs("ViewEmployer",
                                         new LightboxEventCommandArguement
                                         {
                                           Id = oldCriteria.EmployerId.GetValueOrDefault(),
                                           Breadcrumbs = breadcrumbs,
                                           CriteriaHolder = newCriteria
                                         }));
    }

    /// <summary>
    /// Handles the ItemClicked event of the ModalList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ViewJob":
				case "ViewSkill":
        case "ViewInternship":
					long itemId;
					if (long.TryParse(e.CommandArgument.ToString(), out itemId))
					{
						var breadcrumb = new LightboxBreadcrumb() { LinkType = ReportTypes.Employer, LinkId = Employer.Id.GetValueOrDefault(), LinkText = Employer.Name, Criteria = ReportCriteria };
						var breadcrumbs = Breadcrumbs;
						breadcrumbs.Add(breadcrumb);

						OnItemClicked(new CommandEventArgs(e.CommandName,
																							 new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = breadcrumbs }));
					}
					break;

				case "SearchJob":
			    var ronet = e.CommandArgument.ToString();
			    var jobId = ServiceClientLocator.ExplorerClient(App).GetJobIdFromROnet(ronet);
			    long careerAreaId = 0;

          if (!ReportCriteria.CareerAreaId.HasValue)
          {
            var careerAreas = ServiceClientLocator.ExplorerClient(App).GetJobCareerAreas(jobId);
            if (careerAreas.IsNotNullOrEmpty())
              careerAreaId = careerAreas[0];
          }
          else
            careerAreaId = ReportCriteria.CareerAreaId.Value;

          if (jobId != default(long) && careerAreaId != default(long))
          {
            var jobListSearchCriteria = new SearchCriteria
                                          {
                                            ROnetCriteria = new ROnetCriteria { CareerAreaId = careerAreaId, ROnets = new List<string> { ronet } },
                                            KeywordCriteria = new KeywordCriteria(ReportCriteria.Employer)
                                                                {
                                                                  SearchLocation = PostingKeywordScopes.Employer
                                                                }
                                          };


						if (StateArea.StateCode.IsNotNullOrEmpty())
            {
							jobListSearchCriteria.JobLocationCriteria = new JobLocationCriteria
              {
                Area = new AreaCriteria 
											{
					       				MsaId = StateArea.AreaSortOrder == 0 ? (long?)null : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.StateMetropolitanStatisticalAreas).Where(x => x.ExternalId == StateArea.AreaCode).Select(x => x.Id).FirstOrDefault(), 
												StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.ExternalId == StateArea.StateCode).Select(x => x.Id).FirstOrDefault()
											},
								SelectedStateInCriteria = StateArea.StateCode

              };
            }

            App.SetSessionValue("Career:SearchCriteria", jobListSearchCriteria);

            App.SetCookieValue("Career:ManualSearch", "true");
            Response.RedirectToRoute("JobSearchResults");
					}
					break;
			}
		}

    /// <summary>
    /// Prints the modal.
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintModal(ModalPrintFormat printFormat = ModalPrintFormat.Html)
		{
			var printModel = new ExplorerEmployerPrintModel
			{
				Name = EmployerNameLabel.Text,
        Location = ReportCriteria.StateArea,
				Jobs = ReportCriteria.InternshipCategoryId.IsNull()? EmployerJobs : null,
        Internships = ReportCriteria.InternshipCategoryId.IsNotNull() ? InternshipsList.GetInternshipReports() : null,
        ReportCriteria = ReportCriteria
			};

			var skills = EmployerSkillList.GetSkills(SkillListSimple.SkillsListModes.Employer);
			printModel.SpecializedSkills = skills.Where(x => x.SkillType == SkillTypes.Specialized).ToList();
			printModel.SoftwareSkills = skills.Where(x => x.SkillType == SkillTypes.Software).ToList();
			printModel.FoundationSkills = skills.Where(x => x.SkillType == SkillTypes.Foundation).ToList();

			const string sessionKey = "Explorer:EmployerPrintModel";
			App.SetSessionValue(sessionKey, printModel);

      OpenPrintWindow(ReportTypes.Employer, printFormat);
		}
	}
}