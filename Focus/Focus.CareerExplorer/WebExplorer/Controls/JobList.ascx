<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobList.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.JobList" %>

<asp:UpdatePanel ID="JobListUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
	<ContentTemplate>
<asp:ListView ID="JobListView" runat="server" DataSourceID="JobListViewODS" ItemPlaceholderID="JobListViewPlaceHolder"
	OnItemDataBound="JobListView_ItemDataBound" OnSorting="JobListView_Sorting" OnItemCommand="JobListView_ItemCommand" OnLayoutCreated="JobListView_LayoutCreated">
	<LayoutTemplate>
		<table width="100%" class="genericTable withHeader hiringDemandTable" role="presentation">
			<thead>
			  <tr id="ExtraHeaderRow" runat="server" Visible="False">
			    <td class="tableHead" colspan="4"><asp:Literal runat="server" ID="ExtraHeaderLiteral"></asp:Literal></td>
			  </tr>
				<tr>
					<td class="tableHead" colspan="2">
						<asp:LinkButton ID="JobSortButton" runat="server" CommandName="Sort" CommandArgument="JobName"><%= HtmlLocalise("Job.ColumnTitle", "JOB")%></asp:LinkButton>
						<asp:Image ID="JobSortImage" runat="server" Visible="False" AlternateText="JobSortImage" ></asp:Image>
					</td>
					<td class="tableHead deviceNoWrap" style="white-space: nowrap">
            <div style="height:19px;">
              <span class="toolTipNew" id="HiringDemandHeaderTooltip" runat="server">
						    <asp:LinkButton ID="HiringTrendSortButton" runat="server" CommandName="Sort" CommandArgument="HiringDemand"><%= HtmlLocalise("HiringDemand.ColumnTitle", "HIRING DEMAND")%></asp:LinkButton>
						    <asp:Image ID="HiringTrendSortImage" runat="server" AlternateText="Hiring Trend Sort Image" ></asp:Image>
              </span>
            </div>
					</td>
					<td class="tableHead">
						<asp:LinkButton ID="SalarySortButton" runat="server" CommandName="Sort" CommandArgument="SalaryTrendAverage"><%= HtmlLocalise("Salary.ColumnTitle", "SALARY")%></asp:LinkButton>
						<asp:Image ID="SalarySortImage" runat="server" alt="Salary Sort Image" Visible="False" ></asp:Image>
					</td>
				</tr>
			</thead>
			<tbody id="JobListViewPlaceHolder" runat="server"></tbody>
		</table>
    <asp:DataPager ID="JobListPager" runat="server" PagedControlID="JobListView" PageSize="10" Visible="False">
   <Fields>
      <asp:NumericPagerField />
   </Fields>
</asp:DataPager>
	</LayoutTemplate>
	<ItemTemplate>
				<tr>
				  <td style="padding-left:4px;padding-right:4px;padding-top:12px"><strong><asp:Literal ID="RankLiteral" runat="server" />.</strong></td>
					<td style="padding-left:4px;">
						<strong>
							<asp:LinkButton ID="JobNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("ReportData.JobId").ToString() %>' CommandName="ViewJob"><%# Eval("ReportData.Name")%></asp:LinkButton>
						</strong>
						<asp:Panel ID="DegreeProgramPanel" runat="server" Visible="False" CssClass="reportColumnInlineTooltip">
							<div class="tooltip">
								<%# HtmlTooltipster("tooltipWithArrow degreeProgram", "DegreeProgram.TooltipText", "#SCHOOLNAME# offers a program of study for this career.")%>
					    </div>
						</asp:Panel>
						<asp:Panel ID="StarterJobPanel" runat="server" Visible="False" CssClass="reportColumnInlineTooltip">
							<div class="tooltip">
						    <div id="StarterJobToolTipDiv" runat="server">
							    <div class="toolTipNew tooltipImage" id="StarterJobToolTipImage" runat="server"></div>
						    </div>
					    </div>
						</asp:Panel>
					</td>
					<td width="25%">
						<span class="toolTipNew" id="HiringTrendToolTip" runat="server"><%# Eval("ReportData.HiringTrend")%></span>
					</td>
					<td>
					  <asp:Literal runat="server" ID="SalaryText" Visible="False"></asp:Literal>
						<asp:Image ID="SalaryImage" runat="server" CssClass="toolTipNew" AlternateText="SalaryImage" ></asp:Image>
					</td>
				</tr>
	</ItemTemplate>
</asp:ListView>
<asp:ObjectDataSource ID="JobListViewODS" runat="server" 
	TypeName=" Focus.CareerExplorer.WebExplorer.Controls.JobList" EnablePaging="False"
	SelectMethod="GetJobReports" SortParameterName="orderBy" OnSelecting="JobListViewODS_Selecting">
	<SelectParameters>
		<asp:Parameter Name="jobListMode" DbType="Object" />
		<asp:Parameter Name="reportCriteria" DbType="Object" />
		<asp:Parameter Name="listSize" DbType="Int32" />
		<asp:Parameter Name="jobId" DbType="Int64" />
		<asp:Parameter Name="degreeEducationLevelId" DbType="Int64" />
		<asp:Parameter Name="programAreaId" DbType="Int64" />
		<asp:Parameter Name="degreeId" DbType="Int64" />
		<asp:Parameter Name="skillId" DbType="Int64" />
		<asp:Parameter Name="careerMoves" DbType="Object" />
		<asp:Parameter Name="militaryOccupationROnetCodes" DbType="Object" />
		<asp:Parameter Name="jobDegreeTierLevel" DbType="Int32" />
		<asp:Parameter Name="careerAreaId" DbType="Int64" />
	</SelectParameters>
</asp:ObjectDataSource>
	</ContentTemplate>
</asp:UpdatePanel>
<script language="javascript" type="text/javascript">
  $(document).ready(function () {
    window.setTimeout('JobList_JobNameWrapping("<%=JobListUpdatePanel.ClientID %>")', 500);
    $("*[data-joblist='<%=ClientID %>']").on("click", function () {
      JobList_JobNameWrapping("<%=JobListUpdatePanel.ClientID %>");
    });
    });
    // following script will rebind tooltips after update panel postback
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
    	BindTooltips();
    });
</script>
