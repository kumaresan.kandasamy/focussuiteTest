<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobPrint.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.JobPrint" %>
<%@ Register src="Criteria.ascx" tagName="CriteriaFilter" tagPrefix="uc" %>

<div class="staticAccordion">
    <table>
        <tr class="lightboxHeading">
            <td>
                <h2 style="display: inline;">
                    <asp:Label ID="JobNameLabel" runat="server" /></h2>
                <asp:Image runat="server" ID="MortarIcon" Visible="False" AlternateText="Mortar Icon" />
                <asp:Image runat="server" ID="StarterJobIcon" Visible="False" AlternateText="Starter Job Icon" />
            </td>
        </tr>
        <tr>
          <td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" HideCloseIcons="True" /></td>
        </tr>
        <tr>
            <td>
                <asp:Literal runat="server" ID="JobIntroductionLabel"></asp:Literal>
            </td>
        </tr>
    </table>
    <table class="topJobTitles">
        <tr>
            <td style="vertical-align: top; width: 20%;">
                Top job titles:
            </td>
            <td valign="top">
                <asp:Label ID="JobTitlesLabel" runat="server" />
            </td>
        </tr>
    </table>
    <table width="100%" class="numberedAccordion">
        <tr class="accordionTitle">
            <td width="10" class="column1">
                1
            </td>
            <td class="column2">
                <%= HtmlLocalise("SalaryHiringTrends.Header", "Salary & hiring trends")%>
            </td>
            <td class="column3" align="right" valign="top">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <p>
                    <%= HtmlLocalise("SalaryHiringTrendsIntro.Text", "Salary and hiring trends provide an overview of salary and hiring demand, nationally and by geographic area, for this occupation � as well as the available positions in the selected area over the last 12 months.")%></p>
                <div class="chartSalaryTrend">
                    <table width="100%">
                        <tr>
                            <td colspan="5">
                                <%= HtmlLocalise("HiringDemand.Text", "Hiring demand")%>: <strong>
                                    <asp:Literal ID="HiringTrendLiteral" runat="server" /></strong>
                            </td>
                        </tr>
                        <tr class="chartLabels">
                            <td width="87px">
                                <%= HtmlLocalise("HiringDemandLow.Label", "LOW")%>
                            </td>
                            <td width="87px">
                                <%= HtmlLocalise("HiringDemandMedium.Label", "MEDIUM")%>
                            </td>
                            <td width="87px">
                                <%= HtmlLocalise("HiringDemandHigh.Label", "HIGH")%>
                            </td>
                            <td width="87px">
                                <%= HtmlLocalise("HiringDemandVeryHigh.Label", "VERY HIGH")%>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="chartBars">
                            <td>
                                <span></span>
                            </td>
                            <td>
                                <span></span>
                            </td>
                            <td>
                                <span></span>
                            </td>
                            <td>
                                <span></span>
                            </td>
                            <td width="260px">
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="chartArrow">
                            <td colspan="5">
                                <div class="arrowWrap">
                                    <asp:Literal ID="HiringTrendArrowSpanLiteral" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <%= HtmlLocalise("HiringDemandPositionAvailable.Text", "Positions available in the past 12 months")%>:
                            </td>
                            <td class="chartInfo">
                                &nbsp; <strong>
                                    <asp:Literal ID="HiringDemandLiteral" runat="server" /></strong>
                                <asp:Image ID="HiringDemandImage" runat="server" Width="35" Height="35" AlternateText="Hiring Demand Image" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="10px;">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= HtmlLocalise("HiringDemandSalaryRange.Text", "Salary range for people employed in this occupation in selected area")%>:
                            </td>
                            <td class="chartInfo">
                                &nbsp; <strong>
                                    <asp:Literal ID="SalaryRangeLiteral" runat="server" /></strong>
                                <asp:Image ID="SalaryRangeImage" runat="server" Width="35" Height="35" AlternateText="Salary Range Image" />
                            </td>
                        </tr>
                        <asp:PlaceHolder ID="SalaryNationwidePlaceHolder" runat="server">
                            <tr>
                                <td colspan="2" height="10px;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="SalaryNationwideLabelLiteral" runat="server" />:
                                </td>
                                <td class="chartInfo">
                                    &nbsp; <strong>
                                        <asp:Literal ID="SalaryNationwideLiteral" runat="server" /></strong>
                                    <asp:Image ID="SalaryNationwideImage" runat="server" Width="35" Height="35" AlternateText="Salary Nation wide Image" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                    </table>
                </div>
            </td>
            <td>
            </td>
        </tr>
        <tr class="accordionTitle">
            <td class="column1">
                2
            </td>
            <td class="column2">
                <%= HtmlLocalise("TopEmployers.Header", "In-demand #BUSINESSES#:LOWER")%>
            </td>
            <td class="column3" align="right" valign="top">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <p>
                    <%= HtmlLocalise("TopEmployersIntro.Text", "In-demand #BUSINESSES#:LOWER are those who have posted at least 10 openings for this position over the last 12 months. While there may have been numerous job openings for this occupation in recent months, an absence of data in this category means only that no individual #BUSINESS#:LOWER advertised 10 openings.")%></p>
                <!-- Employer list -->
                <table width="100%" class="genericTable">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Repeater ID="EmployerColumnOneRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
                                <HeaderTemplate>
                                    <table width="100%">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td valign="top">
                                            <strong>
                                                <asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp;
                                                <%# Eval("EmployerName") %>&nbsp;
                                                <asp:Literal ID="EmployerOpeningsLiteral" runat="server" /></strong>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                        <asp:PlaceHolder ID="EmployerColumnTwoPlaceHolder" runat="server">
                            <td width="50%" align="left" valign="top">
                                <asp:Repeater ID="EmployerColumnTwoRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
                                    <HeaderTemplate>
                                        <table width="100%">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td valign="top">
                                                <strong>
                                                    <asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp;
                                                    <%# Eval("EmployerName") %>&nbsp;
                                                    <asp:Literal ID="EmployerOpeningsLiteral" runat="server" /></strong>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </asp:PlaceHolder>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
        <tr class="accordionTitle">
            <td class="column1">
                3
            </td>
            <td class="column2">
                <%= HtmlLocalise("RequiredSkills.Header", "Required skills: specialized, software, foundation")%>
            </td>
            <td class="column3" align="right" valign="top">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <p>
                    <%= HtmlLocalise("RequiredSkillsIntro.Text", "Required skills data for this occupation are based on positions advertised nationally and by geographic area over the last 12 months.")%></p>
                <!-- Skill list -->
                <table width="100%" class="genericTable withHeader"  role='presentation'>
                    <tr>
                        <td class="tableHead" colspan="2">
                            <strong>
                                <asp:Label ID="SpecializedSkillTypeLabel" runat="server" /></strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%"  role='presentation'>
                                <asp:Repeater ID="SpecializedSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td valign="top">
                                                <strong>
                                                    <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                    <%# Eval("SkillName") %></strong>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                        <asp:PlaceHolder ID="SpecializedSkillsColumnTwoPlaceHolder" runat="server">
                            <td width="50%" align="left" valign="top">
                                <table width="100%"  role='presentation'>
                                    <asp:Repeater ID="SpecializedSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td valign="top">
                                                    <strong>
                                                        <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                        <%# Eval("SkillName") %></strong>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                        </asp:PlaceHolder>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:Literal runat="server" ID="SpecializedSkillsNoDataLiteral" Visible="False" /></td>
                    </tr>
                </table>
                <table width="100%" class="genericTable withHeader"  role='presentation'>
                    <tr>
                        <td class="tableHead" colspan="2">
                            <strong>
                                <asp:Label ID="SoftwareSkillTypeLabel" runat="server" /></strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%"  role='presentation'>
                                <asp:Repeater ID="SoftwareSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td valign="top">
                                                <strong>
                                                    <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                    <%# Eval("SkillName") %></strong>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                        <asp:PlaceHolder ID="SoftwareSkillsColumnTwoPlaceHolder" runat="server">
                            <td width="50%" align="left" valign="top">
                                <table width="100%"  role='presentation'>
                                    <asp:Repeater ID="SoftwareSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td valign="top">
                                                    <strong>
                                                        <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                        <%# Eval("SkillName") %></strong>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                        </asp:PlaceHolder>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:Literal runat="server" ID="SoftwareSkillsNoDataLiteral" Visible="False" /></td>
                    </tr>
                </table>
                <table width="100%" class="genericTable withHeader"  role='presentation'>
                    <tr>
                        <td class="tableHead" colspan="2">
                            <strong>
                                <asp:Label ID="FoundationSkillTypeLabel" runat="server" /></strong>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%"  role='presentation'>
                                <asp:Repeater ID="FoundationSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td valign="top">
                                                <strong>
                                                    <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                    <%# Eval("SkillName") %></strong>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                        <asp:PlaceHolder ID="FoundationSkillsColumnTwoPlaceHolder" runat="server">
                            <td width="50%" align="left" valign="top">
                                <table width="100%"  role='presentation'>
                                    <asp:Repeater ID="FoundationSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td valign="top">
                                                    <strong>
                                                        <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                        <%# Eval("SkillName") %></strong>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                        </asp:PlaceHolder>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:Literal runat="server" ID="FoundationSkillsNoDataLiteral" Visible="False" /></td>
                    </tr>
                </table>
                <asp:PlaceHolder ID="HeldSkillsPlaceHolder" runat="server">
                    <table width="100%" class="genericTable withHeader"  role='presentation'>
                        <tr>
                            <td class="tableHead">
                                <%= HtmlLocalise("OtherSpecializedSkills.Text", "OTHER SPECIALIZED SKILLS SUCCESSFUL CANDIDATES HAVE") %>&nbsp;
                            </td>
                            <td class="tableHead">
                                <%= HtmlLocalise("OtherSoftwareSkills.Text", "OTHER SOFTWARE SKILLS SUCCESSFUL CANDIDATES HAVE")%>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <table width="100%"  role='presentation'>
                                    <asp:Repeater ID="HeldSpecializedSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td valign="top">
                                                    <strong>
                                                        <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                        <%# Eval("SkillName") %></strong>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                            <td align="left" valign="top">
                                <table width="100%"  role='presentation'>
                                    <asp:Repeater ID="HeldSoftwareSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td valign="top">
                                                    <strong>
                                                        <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                        <%# Eval("SkillName") %></strong>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                        </tr>
                        
                    <tr>
                        <td colspan="2"><asp:Literal runat="server" ID="OtherSpecializedSkillsNoDataLiteral" Visible="False" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><asp:Literal runat="server" ID="OtherSoftwareSkillsNoDataLiteral" Visible="False" /></td>
                    </tr>
                    </table>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="HeldFoundationSkillsPlaceHolder" runat="server">
                    <table width="100%" class="genericTable withHeader"  role='presentation'>
                        <tr>
                            <td class="tableHead">
                                <%= HtmlLocalise("OtherFoundationSkills.Text", "OTHER FOUNDATION SKILLS SUCCESSFUL CANDIDATES HAVE") %>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                <table width="100%">
                                    <asp:Repeater ID="HeldFoundationSkillsRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td valign="top">
                                                    <strong>
                                                        <asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp;
                                                        <%# Eval("SkillName") %></strong>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </td>
                        </tr>
                    <tr>
                        <td><asp:Literal runat="server" ID="OtherFoundationSkillsNoDataLiteral" Visible="False" /></td>
                    </tr>
                    </table>
                </asp:PlaceHolder>
            </td>
            <td>
            </td>
        </tr>
        <tr class="accordionTitle">
            <td class="column1">
                4
            </td>
            <td class="column2">
                <%= HtmlLocalise("RequiredEducation.Header", "Typical education required")%>
            </td>
            <td class="column3" align="right" valign="top">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <p>
                    <asp:Label ID="DegreeIntroLabel" runat="server" />
                    <asp:Label runat="server" ID="DegreesNotFoundLabel" Visible="False" />
                </p>
                <asp:Panel ID="DegreePanel" runat="server">
                    <asp:PlaceHolder runat="server" ID="ProgramAreaPlaceHolder" Visible="False">
                        <table width="100%" class="genericTable withHeader"  role='presentation'>
                            <tr>
                                <td class="tableHead">
                                    <asp:Literal ID="ProgramAreaHeaderLiteral" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Repeater ID="ProgramAreaRepeater" runat="server" OnItemDataBound="ProgramAreaRepeater_ItemDataBound"
                                        Visible="False">
                                        <HeaderTemplate>
                                            <ul>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <%# Eval("ProgramAreaName")%>&nbsp;
                                                <ul>
                                                    <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="ProgramAreaDegreeEducationLevelRepeater_ItemDataBound">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:Literal ID="DegreeEducationLevelNameLiteral" runat="server" />
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="ProgramAreaTier2PlaceHolder" Visible="False">
                        <table width="100%" class="genericTable withHeader"  role='presentation'>
                            <tr>
                                <td class="tableHead">
                                    <asp:Literal ID="ProgramAreaTier2HeaderLiteral" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Repeater ID="ProgramAreaTier2Repeater" runat="server" OnItemDataBound="ProgramAreaRepeater_ItemDataBound"
                                        Visible="False">
                                        <HeaderTemplate>
                                            <ul>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <%# Eval("ProgramAreaName")%>&nbsp;
                                                <ul>
                                                    <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="ProgramAreaDegreeEducationLevelRepeater_ItemDataBound">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:Literal ID="DegreeEducationLevelNameLiteral" runat="server" />
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="DegreePlaceHolder" Visible="False">
                        <table width="100%" class="genericTable withHeader"  role='presentation'>
                            <tr>
                                <td class="tableHead">
                                    <asp:Literal ID="DegreeHeaderLiteral" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Repeater ID="ClientDegreeRepeater" runat="server" OnItemDataBound="DegreeRepeater_ItemDataBound">
                                        <HeaderTemplate>
                                            <ul>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <%# Eval("Name") %>&nbsp;
                                                <ul>
                                                    <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:Literal ID="DegreeEducationLevelNameLiteral" runat="server" /></li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="DegreeTier2PlaceHolder" Visible="False">
                        <table width="100%" class="genericTable withHeader"  role='presentation'>
                            <tr>
                                <td class="tableHead">
                                    <asp:Literal ID="DegreeTier2HeaderLiteral" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Repeater ID="DegreeTier2Repeater" runat="server" OnItemDataBound="DegreeRepeater_ItemDataBoundTier2">
                                        <HeaderTemplate>
                                            <ul>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <%# Eval("Name") %>&nbsp;
                                                <ul>
                                                    <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:Literal ID="DegreeEducationLevelNameLiteral" runat="server" /></li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </asp:PlaceHolder>
                    <br />
                    <asp:Literal ID="ClientDegreeDisclaimerLiteral" runat="server" />
                </asp:Panel>
                <asp:PlaceHolder ID="EducationRequirementsPlaceHolder" runat="server">
                    <br />
                    <table width="100%" class="genericTable withHeader" role="presentation">
                        <tr>
                            <td>
                                <div>
                                    <%= HtmlLocalise("MinimumDegreeLevelRequirementsIntro.Text", "Minimum degree level required in job postings nationwide")%></div>
                                <table class="horizontalBarChart">
                                    <tr>
                                        <td class="column1" width="155px;">
                                            <%= HtmlLocalise("HighSchoolTechnicalTraining.BarChartLabel", "High school/technical training")%>
                                        </td>
                                        <td class="column2" rowspan="5">
                                        </td>
                                        <td class="column3">
                                            <asp:Literal ID="HighSchoolTechnicalTrainingLiteral" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="column1">
                                            <%= HtmlLocalise("AssociatesDegree.BarChartLabel", "Associates degree")%>
                                        </td>
                                        <td class="column3">
                                            <asp:Literal ID="AssociatesDegreeLiteral" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="column1">
                                            <%= HtmlLocalise("BachelorsDegree.BarChartLabel", "Bachelors degree")%>
                                        </td>
                                        <td class="column3">
                                            <asp:Literal ID="BachelorsDegreeLiteral" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="column1">
                                            <%= HtmlLocalise("GraduateProfessionalDegree.BarChartLabel", "Graduate/professional degree")%>
                                        </td>
                                        <td class="column3">
                                            <asp:Literal ID="GraduateProfessionalDegreeLiteral" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                <div>
                                    <%= HtmlLocalise("MinimumDegreeLevelRequirementsOutro.Text", "This chart reflects the minimum degree #BUSINESSES#:LOWER will accept. #BUSINESSES# will often prefer completion of a higher level degree for applicants without significant work experience. Similarly, #BUSINESSES#:LOWER will often allow significant work experience to substitute for their minimum degree requirement.")%></div>
                                <br />
                            </td>
                        </tr>
                    </table>
                </asp:PlaceHolder>
                <br />
                <asp:Panel ID="CertificationPanel" runat="server">
                    <table width="100%" class="genericTable withHeader"  role='presentation'>
                        <tr>
                            <td class="tableHead">
                                <%= HtmlLocalise("CertificationsAndLicences.Header", "CERTIFICATIONS AND LICENSES")%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    <%= HtmlLocalise("CertificationIntro.Text", "#BUSINESSES# may give preference to applicants who have earned one or more of the following certifications")%></div>
                                <ul>
                                    <asp:Repeater ID="CertificationRepeater" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <%# Eval("CertificationName")%></li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>
            </td>
        </tr>
        <tr class="accordionTitle">
            <td class="column1">
                5
            </td>
            <td class="column2">
                <%= HtmlLocalise("LevelOfExperience.Header", "Level of experience")%>
            </td>
            <td class="column3" align="right" valign="top">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <p>
                    <%= HtmlLocalise("LevelOfExperienceIntro.Text", "Minimum level of experience required in job postings nationwide:")%></p>
                <table class="horizontalBarChart" role="presentation">
                    <tr>
                        <td class="column1" width="155px;">
                            <%= HtmlLocalise("LessThanTwoYears.BarChartLabel", "Less than 2 years")%>
                        </td>
                        <td class="column2" rowspan="5">
                        </td>
                        <td class="column3">
                            <asp:Literal ID="LessThanTwoLiteral" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="column1">
                            <%= HtmlLocalise("TwoToFiveYears.BarChartLabel", "2-5 years")%>
                        </td>
                        <td class="column3">
                            <asp:Literal ID="TwoToFiveLiteral" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="column1">
                            <%= HtmlLocalise("FiveToEightYears.BarChartLabel", "5-8 years")%>
                        </td>
                        <td class="column3">
                            <asp:Literal ID="FiveToEightLiteral" runat="server" />
                        </td>
                    </tr>
                    <tr class="last">
                        <td class="column1">
                            <%= HtmlLocalise("EightPlusYears.BarChartLabel", "8+ years")%>
                        </td>
                        <td class="column3">
                            <asp:Literal ID="EightPlusLiteral" runat="server" />
                        </td>
                    </tr>
                </table>
                <br />
            </td>
            <td>
            </td>
        </tr>
        <tr class="accordionTitle">
            <td class="column1">
                6
            </td>
            <td class="column2">
                <%= HtmlLocalise("SimilarJobs.Header", "Jobs with similar education and/or skills")%>
            </td>
            <td class="column3" align="right" valign="top">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <p>
                    <%= HtmlLocalise("SimilarJobsIntro.Text", "This category displays jobs that require similar education and/or skills to the selected occupation.")%></p>
                <!-- Similar Job list -->
                <table width="100%" class="genericTable withHeader"  role='presentation'>
                    <tr>
                        <td class="tableHead">
                            <%= HtmlLocalise("Job.ColumnTitle", "JOB")%>
                        </td>
                        <td class="tableHead">
                        </td>
                        <td class="tableHead">
                            <%= HtmlLocalise("HiringDemand.ColumnTitle", "HIRING DEMAND")%>
                        </td>
                        <td class="tableHead">
                            <%= HtmlLocalise("Salary.ColumnTitle", "SALARY")%>
                        </td>
                    </tr>
                    <asp:Repeater ID="JobRepeater" runat="server" OnItemDataBound="JobRepeater_ItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td width="40%">
                                    <strong>
                                        <asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<%# Eval("ReportData.Name")%></strong>
                                </td>
                                <td width="13%" nowrap="nowrap">
                                    <asp:Image runat="server" ID="MortarIcon" Visible="False" AlternateText="Mortar Icon" />
                                    <asp:Image runat="server" ID="StarterJobIcon" Visible="False" AlternateText="Starter Job Icon" />
                                </td>
                                <td width="30%">
                                    <%# Eval("ReportData.HiringTrend")%>
                                </td>
                                <td width="17%">
                                    <asp:Image ID="SalaryImage" runat="server" AlternateText="Salary Image" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </td>
            <td>
            </td>
        </tr>
        <tr class="accordionTitle">
            <td class="column1">
                7
            </td>
            <td class="column2">
                <%= HtmlLocalise("CommonCareerPaths.Header", "Explore common career paths")%>
            </td>
            <td class="column3" align="right" valign="top">
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <table width="100%" class="genericTable withHeader"  role='presentation'>
                    <tr>
                        <td class="tableHead">
                            <%= HtmlLocalise("WherePeopleGoFromHere.Text", "Where people go from here")%>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="100%"  role='presentation'>
                    <tr>
                        <td align="center">
                            <table  role='presentation'>
                                <tr>
                                    <td>
                                        <asp:Panel ID="CareerPathToPanel" runat="server" CssClass="careerPathChart">
                                            <!-- First Row -->
                                            <asp:Panel ID="CareerPathToTopLevelPanel" runat="server" CssClass="careerpathChartItem top topArrowDown">
                                                <table class="box">
                                                    <tr>
                                                        <td align="center" valign="middle">
                                                            <asp:Label ID="CareerPathToJobNameLabel" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <span class="arrowLineTop"></span>
                                            </asp:Panel>
                                            <div class="clear">
                                            </div>
                                            <!-- Second Row -->
                                            <asp:Panel ID="CareerPathToSecondLevelPanel" runat="server" CssClass="careerPathRowWrap">
                                                <asp:Panel ID="CareerPathToSecondLevelLeftPanel" runat="server" CssClass="careerpathChartItem left">
                                                    <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                    <table class="box">
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Literal ID="CareerPathToSecondLevelLeftLiteral" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span class="BottomArrow"></span>
                                                </asp:Panel>
                                                <asp:Panel ID="CareerPathToSecondLevelMiddleLeftPanel" runat="server" CssClass="careerpathChartItem">
                                                    <asp:Literal ID="CareerPathToSecondLevelMiddleLeftArrowLineLiteral" runat="server" /><span
                                                        class="arrowLineBottom"></span>
                                                    <table class="box">
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Literal ID="CareerPathToSecondLevelMiddleLeftLiteral" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span class="BottomArrow"></span>
                                                </asp:Panel>
                                                <asp:Panel ID="CareerPathToSecondLevelMiddleRightPanel" runat="server" CssClass="careerpathChartItem">
                                                    <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                    <table class="box">
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Literal ID="CareerPathToSecondLevelMiddleRightLiteral" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span class="BottomArrow"></span>
                                                </asp:Panel>
                                                <asp:Panel ID="CareerPathToSecondLevelRightPanel" runat="server" CssClass="careerpathChartItem right">
                                                    <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                    <table class="box">
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Literal ID="CareerPathToSecondLevelRightLiteral" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span class="BottomArrow"></span>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="100%" class="genericTable withHeader">
                    <tr>
                        <td class="tableHead">
                            <%= HtmlLocalise("HowDidOtherPeopleGetHere.LinkText", "How did other people get here")%>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="100%">
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Panel ID="CareerPathFromPanel" runat="server" CssClass="careerPathChart">
                                            <!-- First Row -->
                                            <asp:Panel ID="CareerPathFromTopLevelPanel" runat="server" CssClass="careerpathChartItem top topArrowDown">
                                                <table class="box">
                                                    <tr>
                                                        <td align="center" valign="middle">
                                                            <asp:Label ID="CareerPathFromJobNameLabel" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <span class="arrowLineTop"></span>
                                            </asp:Panel>
                                            <div class="clear">
                                            </div>
                                            <!-- Second Row -->
                                            <asp:Panel ID="CareerPathFromSecondLevelPanel" runat="server" CssClass="careerPathRowWrap">
                                                <asp:Panel ID="CareerPathFromSecondLevelLeftPanel" runat="server" CssClass="careerpathChartItem left">
                                                    <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                    <table class="box">
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Literal ID="CareerPathFromSecondLevelLeftLiteral" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span class="BottomArrow"></span>
                                                </asp:Panel>
                                                <asp:Panel ID="CareerPathFromSecondLevelMiddleLeftPanel" runat="server" CssClass="careerpathChartItem">
                                                    <asp:Literal ID="CareerPathFromSecondLevelMiddleLeftArrowLineLiteral" runat="server" /><span
                                                        class="arrowLineBottom"></span>
                                                    <table class="box">
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Literal ID="CareerPathFromSecondLevelMiddleLeftLiteral" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span class="BottomArrow"></span>
                                                </asp:Panel>
                                                <asp:Panel ID="CareerPathFromSecondLevelMiddleRightPanel" runat="server" CssClass="careerpathChartItem">
                                                    <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                    <table class="box">
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Literal ID="CareerPathFromSecondLevelMiddleRightLiteral" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span class="BottomArrow"></span>
                                                </asp:Panel>
                                                <asp:Panel ID="CareerPathFromSecondLevelRightPanel" runat="server" CssClass="careerpathChartItem right">
                                                    <span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                                    <table class="box">
                                                        <tr>
                                                            <td align="center" valign="middle">
                                                                <asp:Literal ID="CareerPathFromSecondLevelRightLiteral" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <span class="BottomArrow"></span>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		var heightsArray = [];
		$('.careerPathChart .box td').height('auto').each(function () {
			heightsArray.push($(this).height());
		});
		$('.careerPathChart .box td').height(Math.max.apply(Math, heightsArray));
	});
</script>
