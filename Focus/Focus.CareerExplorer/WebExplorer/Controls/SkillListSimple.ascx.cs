#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.Models;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class SkillListSimple : UserControlBase
	{
		public enum SkillsListModes
		{
			Employer,
			DegreeEducationLevel,
			Internship,
      CareerArea
		}

		public SkillsListModes SkillsListMode { get; set; }
		public ExplorerCriteria ReportCriteria { get; set; }
		public int ListSize { get; set; }
		public long EmployerId { get; set; }
		public long DegreeEducationLevelId { get; set; }
    public long InternshipCategoryId { get; set; }
    public long CareerAreaId { get; set; }

		protected string SkillTypeToolTip { get; set; }

		public List<SkillModel> GetSkills(SkillsListModes skillsListMode)
		{
			var sessionKey = "Explorer:SkillsSimple:" + skillsListMode.ToString();
			return App.GetSessionValue<List<SkillModel>>(sessionKey);
		}

		private void SetSkills(SkillsListModes skillsListMode, List<SkillModel> skills)
		{
			var sessionKey = "Explorer:SkillsSimple:" + skillsListMode.ToString();
			App.SetSessionValue(sessionKey, skills);
		}

		private int _rank = 1;

		public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
		public event ItemClickedHandler ItemClicked;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				BindControls();
			}
		}

    /// <summary>
    /// Handles the Command event of the SkillsLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void SkillsLinkButton_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "LoadSkills")
			{
				var skillType = (SkillTypes)Enum.Parse(typeof(SkillTypes), (string)e.CommandArgument);
				BindList(false, skillType);
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the SkillsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void SkillsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var rankLiteral = (Literal)e.Item.FindControl("SkillRankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemCommand event of the SkillsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
		protected void SkillsRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "ViewSkill")
			{
				OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
			}			
		}

    /// <summary>
    /// Raises the <see cref="E:ItemClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnItemClicked(CommandEventArgs eventArgs)
		{
			if (ItemClicked != null)
				ItemClicked(this, eventArgs);
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			SpecializedSkillsLinkButton.Text = CodeLocalise("SpecializedSkills.LinkLabel", "specialized skills");
			SoftwareSkillsLinkButton.Text = CodeLocalise("SoftwareSkills.LinkLabel", "software skills");
			FoundationSkillsLinkButton.Text = CodeLocalise("FoundationSkills.LinkLabel", "foundation skills");
      NoSkillsLabel.Text = CodeLocalise("NoSkills.Label", "No required skills have been advertised in the last 12 months.");
		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
		private void BindControls()
		{
			SpecializedSkillsLinkButton.CommandArgument = SkillTypes.Specialized.ToString();
			SpecializedSkillsLinkButton.CommandName = "LoadSkills";
			SoftwareSkillsLinkButton.CommandArgument = SkillTypes.Software.ToString();
			SoftwareSkillsLinkButton.CommandName = "LoadSkills";
			FoundationSkillsLinkButton.CommandArgument = SkillTypes.Foundation.ToString();
			FoundationSkillsLinkButton.CommandName = "LoadSkills";
		}

    /// <summary>
    /// Binds the list.
    /// </summary>
    /// <param name="forceReload">if set to <c>true</c> [force reload].</param>
    /// <param name="skillType">Type of the skill.</param>
		public void BindList(bool forceReload, SkillTypes skillType)
		{
			if (forceReload)
				SetSkills(SkillsListMode, null);

			var skills = GetSkills(SkillsListMode);
			if (skills == null)
			{
				switch (SkillsListMode)
				{
					case SkillsListModes.Employer:
            skills = ServiceClientLocator.ExplorerClient(App).GetEmployerSkills(ReportCriteria, ListSize);
            break;

					case SkillsListModes.DegreeEducationLevel:
						skills = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevelSkills(DegreeEducationLevelId, ReportCriteria.StateAreaId, ListSize);
						break;

          case SkillsListModes.Internship:
            skills = ServiceClientLocator.ExplorerClient(App).GetInternshipSkills(InternshipCategoryId, ListSize);
            break;

          case SkillsListModes.CareerArea:
				    skills = ServiceClientLocator.ExplorerClient(App).GetSkillReport(ReportCriteria, ListSize);
            break;
        }

				if (skills == null)
					skills = new List<SkillModel>();

				SetSkills(SkillsListMode, skills);
			}

			switch(skillType)
			{
				case SkillTypes.Specialized:
					SkillTypeLabel.Text = CodeLocalise("SpecializedSkills.Label", "SPECIALIZED SKILLS");
					SkillTypeToolTip = CodeLocalise("SpecializedSkills.TooltipText", "Technical skills are specific to a job or an occupational family, such as, procurement, supply-chain management, etc.");
          SpecializedSkillsLinkButton.Enabled = false;
					SoftwareSkillsLinkButton.Enabled = true;
					FoundationSkillsLinkButton.Enabled = true;
          break;

				case SkillTypes.Software:
					SkillTypeLabel.Text = CodeLocalise("SoftwareSkills.Label", "SOFTWARE SKILLS");
					SkillTypeToolTip = CodeLocalise("SoftwareSkills.TooltipText", "Software skills include all technology-related skills, both basic and job-specific, that most #BUSINESSES#:LOWER require or consider when hiring candidates.");
          SpecializedSkillsLinkButton.Enabled = true;
					SoftwareSkillsLinkButton.Enabled = false;
					FoundationSkillsLinkButton.Enabled = true;
          break;

				case SkillTypes.Foundation:
					SkillTypeLabel.Text = CodeLocalise("FoundationSkills.Label", "FOUNDATION SKILLS");
					SkillTypeToolTip = CodeLocalise("FoundationSkills.TooltipText", "Foundation skills are the core or basic skills needed to perform tasks across a wide range of occupations, such as, research, project management, written/oral communications, critical analysis, etc.");
          SpecializedSkillsLinkButton.Enabled = true;
					SoftwareSkillsLinkButton.Enabled = true;
					FoundationSkillsLinkButton.Enabled = false;
          break;
			}

			var skillsByType = skills.Where(x => x.SkillType == skillType).ToList();

      SkillsColumnOnePlaceHolder.Visible = true;
      NoSkillsLabel.Visible = false;

      var skillsByTypeCount = skillsByType.Count;

		  if (skillsByTypeCount > 0)
		  {
		    _rank = 1;
		    SkillsColumnOneRepeater.DataSource = skillsByType.OrderByDescending(x => x.DemandPercentile).ThenBy(x => x.Rank).Take(10).ToList();
		    SkillsColumnOneRepeater.DataBind();

		    if (skillsByType.Count > 10)
		    {
		      SkillTypeTableCell.ColSpan = 2;
		      SkillsColumnTwoPlaceHolder.Visible = true;

		      SkillsColumnTwoRepeater.DataSource = skillsByType.OrderByDescending(x => x.DemandPercentile).ThenBy(x => x.Rank).Skip(10).ToList();
		      SkillsColumnTwoRepeater.DataBind();
		    }
		    else
		    {
		      SkillsColumnTwoPlaceHolder.Visible = false;
		    }
		  }
		  else
		  {
        SkillsColumnOnePlaceHolder.Visible = false;
		    SkillsColumnTwoPlaceHolder.Visible = false;
        NoSkillsLabel.Visible = true;
		  }
		}
	}
}