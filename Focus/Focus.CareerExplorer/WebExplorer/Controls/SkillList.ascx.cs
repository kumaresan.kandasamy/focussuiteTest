#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.Models;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class SkillList : UserControlBase
	{
		public ExplorerCriteria ReportCriteria { get; set; }
    public int ListSize { get; set; }

	  public int RecordCount
	  {
	    get
	    {
	      return SpecializedSkillsRepeater.Items.Count + SoftwareSkillsRepeater.Items.Count +
	             FoundationSkillsRepeater.Items.Count;
	    }
	  }

	  private List<SkillModel> GetSkillReports()
	  {
		  const string sessionKey = "Explorer:SkillReports";
		  return App.GetSessionValue<List<SkillModel>>(sessionKey);
	  }

		private void SetSkillReports(List<SkillModel> skillReports)
		{
			const string sessionKey = "Explorer:SkillReports";
			App.SetSessionValue(sessionKey, skillReports);
		}

		private int _rank = 1;

		public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
		public event DegreeCertificationList.ItemClickedHandler ItemClicked;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

		}

    /// <summary>
    /// Binds the list.
    /// </summary>
    /// <param name="forceReload">if set to <c>true</c> [force reload].</param>
		public void BindList(bool forceReload)
		{
			if (forceReload)
				SetSkillReports(null);

			var skillReports = GetSkillReports();
			if (skillReports == null || skillReports.Count == 0)
			{
				skillReports = ServiceClientLocator.ExplorerClient(App).GetSkillReport(ReportCriteria, ListSize);
				SetSkillReports(skillReports);
			}

			//var skillCategorySkills = ServiceClientLocator.ExplorerClient(App).GetSkillCategorySkills();
			//var skillIds = skillReports.Select(x => x.SkillId).Distinct().ToList();

			//var skillCategories = (from sks in skillCategorySkills
			//                       where skillIds.Contains(sks.SkillId)
			//                       group sks by new {sks.SkillCategoryId, sks.SkillCategoryName}
			//                       into g
			//                       select new SkillCategoryDto()
			//                                {
			//                                  Id = g.Key.SkillCategoryId,
			//                                  Name = g.Key.SkillCategoryName
			//                                }).ToList();

			//SkillCategoryRepeater.DataSource = skillCategories;
			//SkillCategoryRepeater.DataBind();

			_rank = 1;
			SpecializedSkillsRepeater.DataSource =
				skillReports.Where(x => x.SkillType == SkillTypes.Specialized).OrderByDescending(x => x.DemandPercentile).ToList();
			SpecializedSkillsRepeater.DataBind();

			_rank = 1;
			SoftwareSkillsRepeater.DataSource =
				skillReports.Where(x => x.SkillType == SkillTypes.Software).OrderByDescending(x => x.DemandPercentile).ToList();
			SoftwareSkillsRepeater.DataBind();

			_rank = 1;
			FoundationSkillsRepeater.DataSource =
				skillReports.Where(x => x.SkillType == SkillTypes.Foundation).OrderByDescending(x => x.DemandPercentile).ToList();
			FoundationSkillsRepeater.DataBind();
		}

    /// <summary>
    /// Handles the ItemDataBound event of the SkillRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void SkillRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var rankLiteral = (Literal)e.Item.FindControl("RankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemCommand event of the SkillRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
		protected void SkillRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "ViewSkill")
			{
				OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
			}
		}

    /// <summary>
    /// Raises the <see cref="E:ItemClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnItemClicked(CommandEventArgs eventArgs)
		{
			if (ItemClicked != null)
				ItemClicked(this, eventArgs);
		}
	}
}