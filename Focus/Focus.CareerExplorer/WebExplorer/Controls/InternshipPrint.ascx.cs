#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class InternshipPrint : UserControlBase
	{
		private int _rank = 1;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
    /// <param name="printModel">The print model.</param>
		public void BindControls(ExplorerInternshipPrintModel printModel)
		{
			InternshipNameLabel.Text = printModel.Name;
      CriteriaFilter.Bind(ReportTypes.Internship, printModel.ReportCriteria);

      TopInternshipsTitleLabel.Text = CodeLocalise("TopInternship.Title", "Top Types of {0} Internships Nationwide", printModel.Name);

			SpecializedSkillTypeLabel.Text = CodeLocalise("SpecializedSkills.Label", "SPECIALIZED SKILLS");
			_rank = 1;
			SpecializedSkillsColumnOneRepeater.DataSource = printModel.SpecializedSkills.Take(10).ToList();
			SpecializedSkillsColumnOneRepeater.DataBind();

			if (printModel.SpecializedSkills.Count > 10)
			{
				SpecializedSkillsColumnTwoRepeater.DataSource = printModel.SpecializedSkills.Skip(10).Take(10).ToList();
				SpecializedSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				SpecializedSkillsColumnTwoPlaceHolder.Visible = false;
			}

			SoftwareSkillTypeLabel.Text = CodeLocalise("SoftwareSkills.Label", "SOFTWARE SKILLS");
			_rank = 1;
			SoftwareSkillsColumnOneRepeater.DataSource = printModel.SoftwareSkills.Take(10).ToList();
			SoftwareSkillsColumnOneRepeater.DataBind();

			if (printModel.SoftwareSkills.Count > 10)
			{
				SoftwareSkillsColumnTwoRepeater.DataSource = printModel.SoftwareSkills.Skip(10).Take(10).ToList();
				SoftwareSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				SoftwareSkillsColumnTwoPlaceHolder.Visible = false;
			}

			FoundationSkillTypeLabel.Text = CodeLocalise("FoundationSkills.Label", "FOUNDATION SKILLS");
			_rank = 1;
			FoundationSkillsColumnOneRepeater.DataSource = printModel.FoundationSkills.Take(10).ToList();
			FoundationSkillsColumnOneRepeater.DataBind();

			if (printModel.FoundationSkills.Count > 10)
			{
				FoundationSkillsColumnTwoRepeater.DataSource = printModel.FoundationSkills.Skip(10).Take(10).ToList();
				FoundationSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				FoundationSkillsColumnTwoPlaceHolder.Visible = false;
			}

			_rank = 1;
			EmployerColumnOneRepeater.DataSource = printModel.Employers.Take(10).ToList();
			EmployerColumnOneRepeater.DataBind();

			if (printModel.Employers.Count > 10)
			{
				EmployerColumnTwoRepeater.DataSource = printModel.Employers.Skip(10).ToList();
				EmployerColumnTwoRepeater.DataBind();
			}
			else
			{
				EmployerColumnTwoPlaceHolder.Visible = false;
			}

			_rank = 1;
			JobRepeater.DataSource = printModel.Jobs;
			JobRepeater.DataBind();
		}

    /// <summary>
    /// Handles the ItemDataBound event of the SkillsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void SkillsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var rankLiteral = (Literal)e.Item.FindControl("SkillRankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the EmployerRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
			  var countLiteral = (Literal)e.Item.FindControl("EmployerCountLiteral");
				countLiteral.Text = _rank.ToString();
        
				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the JobRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void JobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var internshipJob = (InternshipJobTitleDto)e.Item.DataItem;

				var countLiteral = (Literal)e.Item.FindControl("JobRankLiteral");
				countLiteral.Text = _rank.ToString();

				var openingsLiteral = (Literal)e.Item.FindControl("JobOpeningsLiteral");
				openingsLiteral.Text = String.Format("({0}{1})", internshipJob.Frequency.ToString("#,#"), _rank == 1 ? " openings" : String.Empty);

				_rank++;
			}
		}
	}
}