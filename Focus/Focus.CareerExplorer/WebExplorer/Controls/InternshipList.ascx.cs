#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class InternshipList : UserControlBase
	{
    public InternshipListModes ListMode { get; set; }
		public ExplorerCriteria ReportCriteria { get; set; }
    public int ListSize { get; set; }
    public int RecordCount { get { return InternshipListView != null ? InternshipListView.Items.Count : 0; } }

		public List<InternshipReportViewDto> GetInternshipReports()
		{
			const string sessionKey = "Explorer:InternshipReports";
			return App.GetSessionValue<List<InternshipReportViewDto>>(sessionKey);
		}

    /// <summary>
    /// Sets the internship reports.
    /// </summary>
    /// <param name="internshipReports">The internship reports.</param>
		private void SetInternshipReports(List<InternshipReportViewDto> internshipReports)
		{
			const string sessionKey = "Explorer:InternshipReports";
			App.SetSessionValue(sessionKey, internshipReports);
		}

		private int _rank = 1;

		public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
		public event ItemClickedHandler ItemClicked;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

		}

    /// <summary>
    /// Binds the list.
    /// </summary>
    /// <param name="forceReload">if set to <c>true</c> [force reload].</param>
		public void BindList(bool forceReload)
		{
		  InternshipCountPanel.Visible = ListMode == InternshipListModes.Report;

			if (forceReload)
				SetInternshipReports(null);

			InternshipListView.DataBind();

			var internshipReports = GetInternshipReports();
			if (internshipReports != null && internshipReports.Count > 0)
			{
				TotalInternshipsLiteral.Text = internshipReports.Sum(x => x.NumberAvailable).ToString("#,#");
			}
			else
			{
				TotalInternshipsLiteral.Text = "0";
			}
		}

    /// <summary>
    /// Gets the internship reports.
    /// </summary>
    /// <param name="internshipListmode">The internship listmode.</param>
    /// <param name="reportCriteria">The report criteria.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <param name="orderBy">The order by.</param>
    /// <returns></returns>
    public List<InternshipReportViewDto> GetInternshipReports(InternshipListModes internshipListmode, ExplorerCriteria reportCriteria, int listSize, string orderBy)
		{
			var doSort = true;
			// get reports from view state
			var internshipReports = GetInternshipReports();
			// check if anything returned
      if (internshipReports == null || internshipReports.Count == 0 || internshipListmode != InternshipListModes.Report)
			{
				// check if we have report criteria
				if (reportCriteria != null)
				{
          switch (internshipListmode)
          {
            case InternshipListModes.Employer:
              internshipReports = ServiceClientLocator.ExplorerClient(App).GetEmployerInternships(reportCriteria, listSize);
              break;
            case InternshipListModes.Skill:
              internshipReports = ServiceClientLocator.ExplorerClient(App).GetInternshipsBySkill(reportCriteria, listSize);
              break;
            default:
              internshipReports = ServiceClientLocator.ExplorerClient(App).GetInternshipReport(reportCriteria, listSize);
              break;
          }

					SetInternshipReports(internshipReports);
					doSort = false;
				}
				else
				{
					internshipReports = new List<InternshipReportViewDto>();
					doSort = false;
				}
			}

			if (doSort)
			{
				if (String.IsNullOrEmpty(orderBy))
					orderBy = "NumberAvailable DESC";

				var sortDescending = false;
				if (orderBy.EndsWith(" DESC"))
				{
					sortDescending = true;
					orderBy = orderBy.Substring(0, orderBy.LastIndexOf(" DESC"));
				}

				switch (orderBy)
				{
					case "InternshipName":
						internshipReports = sortDescending ? internshipReports.OrderByDescending(x => x.Name).ToList() : internshipReports.OrderBy(x => x.Name).ToList();
						break;

					case "NumberAvailable":
						internshipReports = sortDescending ? internshipReports.OrderByDescending(x => x.NumberAvailable).ToList() : internshipReports.OrderBy(x => x.NumberAvailable).ToList();
						break;
				}
			}

			return internshipReports;
		}

    /// <summary>
    /// Handles the Selecting event of the InternshipListViewODS control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void InternshipListViewODS_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["reportCriteria"] = ReportCriteria;
			e.InputParameters["listSize"] = ListSize;
		  e.InputParameters["internshipListmode"] = ListMode;
		}

    /// <summary>
    /// Handles the ItemDataBound event of the InternshipListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void InternshipListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var rankLiteral = (Literal)e.Item.FindControl("RankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemCommand event of the InternshipListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewCommandEventArgs"/> instance containing the event data.</param>
		protected void InternshipListView_ItemCommand(object sender, ListViewCommandEventArgs e)
		{
			if (e.CommandName == "ViewInternship")
			{
				OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
			}
		}

    /// <summary>
    /// Handles the Sorting event of the InternshipListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void InternshipListView_Sorting(object sender, ListViewSortEventArgs e)
		{
			var sortImageUrl = e.SortDirection == SortDirection.Ascending ? UrlBuilder.SortUp() : UrlBuilder.SortDown();

			var internshipNameSortImage = (Image)InternshipListView.FindControl("InternshipNameSortImage");
			var numberAvailableSortImage = (Image)InternshipListView.FindControl("NumberAvailableSortImage");

			switch (e.SortExpression)
			{
				case "InternshipName":
					internshipNameSortImage.ImageUrl = sortImageUrl;
					internshipNameSortImage.Visible = true;
					numberAvailableSortImage.Visible = false;
					break;

				case "NumberAvailable":
					internshipNameSortImage.Visible = false;
					numberAvailableSortImage.ImageUrl = sortImageUrl;
					numberAvailableSortImage.Visible = true;
					break;
			}
		}

    /// <summary>
    /// Raises the <see cref="E:ItemClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnItemClicked(CommandEventArgs eventArgs)
		{
			if (ItemClicked != null)
				ItemClicked(this, eventArgs);
		}

    /// <summary>
    /// Handles the OnLayoutCreated event of the InternshipListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void InternshipListView_OnLayoutCreated(object sender, EventArgs e)
    {
	    var internshipNameSortImage = (Image)InternshipListView.FindControl("InternshipNameSortImage");
	    var numberAvailableSortImage = (Image)InternshipListView.FindControl("NumberAvailableSortImage");
	    internshipNameSortImage.ImageUrl = numberAvailableSortImage.ImageUrl = UrlBuilder.SortDown();
	    internshipNameSortImage.Visible = numberAvailableSortImage.Visible = (ListMode == InternshipListModes.Report);

      ((LinkButton)InternshipListView.FindControl("InternshipNameSortButton")).Enabled = (ListMode == InternshipListModes.Report);
      ((LinkButton)InternshipListView.FindControl("NumberAvailableSortButton")).Enabled = (ListMode == InternshipListModes.Report);
      InternshipListView.FindControl("NumberAvailableSortButton").Visible = (ListMode != InternshipListModes.Skill);
    }
	}
}