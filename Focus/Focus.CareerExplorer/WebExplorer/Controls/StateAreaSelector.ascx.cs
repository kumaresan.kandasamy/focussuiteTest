﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.CareerExplorer.Code;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class StateAreaSelector : UserControlBase
	{
    public long? DefaultStateAreaId { get; set; }

		public string StateBeforeHtml
		{
			get { return ((string)ViewState["StateBeforeHtml"] ?? string.Empty); }
			set { ViewState["StateBeforeHtml"] = value; }
		}

		public string StateAfterHtml
		{
			get { return ((string)ViewState["StateAfterHtml"] ?? string.Empty); }
			set { ViewState["StateAfterHtml"] = value; }
		}

		public string AreaBeforeHtml
		{
			get { return ((string)ViewState["AreaBeforeHtml"] ?? string.Empty); }
			set { ViewState["AreaBeforeHtml"] = value; }
		}

		public string AreaAfterHtml
		{
			get { return ((string)ViewState["AreaAfterHtml"] ?? string.Empty); }
			set { ViewState["AreaAfterHtml"] = value; }
		}

		public string StateAreaValidationGroup
		{
			get { return ((string)ViewState["StateAreaValidationGroup"] ?? string.Empty); }
			set { ViewState["StateAreaValidationGroup"] = value; }
		}

		public string StateDropDownListClientID
		{
			get { return StateDropDownList.ClientID; }
		}

		public string AreaDropDownListClientID
		{
			get { return AreaDropDownList.ClientID; }
		}

		public long? StateSelectedValue
		{
			get { return StateDropDownList.SelectedValueToNullableLong(); }
		}

		public long? AreaSelectedValue
		{
			get { return AreaDropDownList.SelectedValueToNullableLong(); }
		}

	  private bool _areaDropdownBound;

    /// <summary>
    /// Resets the drop downs to a default value. This is designed to be used on post-back
    /// </summary>
    /// <param name="stateAreaId">The default state area/id</param>
    public void ResetStateArea(long? stateAreaId)
    {
      DefaultStateAreaId = stateAreaId;

      SelectDefaultState();
      RebindAreaDropdown(true);
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				BindControls();
			}

      RebindAreaDropdown(false);

			string js = String.Format(
				@"
$(document).ready(function() {{
	$('#{0}').change(function () {{
		var selectedIndex = ($(this).prop('selectedIndex') > 0) ? 1 : -1;

		LoadCascadingDropDownList($(this).val(),'{1}','{2}','{3}','{4}','{5}', selectedIndex);
	}});

		var selectedIndex = ($('#{1}').prop('selectedIndex') > 0) ? $('#{1}').prop('selectedIndex') : -1;
		if($('#{0}').find(':selected').val() != undefined)
			LoadCascadingDropDownList($('#{0}').val(),'{1}','{2}','{3}','{4}','{5}', selectedIndex);
	}});",
				StateDropDownList.ClientID, AreaDropDownList.ClientID, String.Concat(UrlBuilder.AjaxService(), "/GetAreas"), "Areas",
				CodeLocalise("Global.Area.TopDefault", "- select area -"),
				CodeLocalise("LoadingAreas.Text", "[Loading areas...]"));

			Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "StateDropDownListOnChange", js, true);
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			StateDropDownListRFV.Text = CodeLocalise("SelectState.RequiredErrorMessage", "<br />Select a state");
			AreaDropDownListRFV.Text = CodeLocalise("SelectArea.RequiredErrorMessage", "<br />Select an area");
		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
		private void BindControls()
		{
			StateDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetStates();
			StateDropDownList.DataTextField = "StateName";
			StateDropDownList.DataValueField = "Id";
			StateDropDownList.DataBind();
			StateDropDownList.Items.AddLocalisedTopDefault("Global.State.TopDefault", "- select state -");
      SelectDefaultState();

			AreaDropDownList.Items.AddLocalisedTopDefault("Global.Area.TopDefault", "- select area -");

			StateBeforeHtmlLiteral.Text = StateBeforeHtml;
			StateAfterHtmlLiteral.Text = StateAfterHtml;
			AreaBeforeHtmlLiteral.Text = AreaBeforeHtml;
			AreaAfterHtmlLiteral.Text = AreaAfterHtml;

			if (String.IsNullOrEmpty(StateAfterHtml) && String.IsNullOrEmpty(AreaBeforeHtml))
			{
				StateAfterHtmlLiteral.Text = "<br />";
			}

			StateDropDownListRFV.ValidationGroup = StateAreaValidationGroup;
			AreaDropDownListRFV.ValidationGroup = StateAreaValidationGroup;
		}

    /// <summary>
    /// Sets the state drop-down to the specified default value
    /// </summary>
    private void SelectDefaultState()
    {
      if (DefaultStateAreaId.HasValue)
      {
        var stateAreas = ServiceClientLocator.ExplorerClient(App).GetStateAreas();
        var stateArea = stateAreas.First(sa => sa.Id == DefaultStateAreaId.Value);
        var state = stateAreas.First(sa => sa.AreaSortOrder == 0 && sa.StateId == stateArea.StateId);

        StateDropDownList.SelectedValue = state.Id.ToString();
      }
    }

    /// <summary>
    /// Rebinds the area down, either on post back of if a default value has been set
    /// </summary>
    /// <param name="resetToDefaultOnPostBack">Reset to the default value even on post back</param>
    private void RebindAreaDropdown(bool resetToDefaultOnPostBack)
    {
      if (IsPostBack || DefaultStateAreaId.HasValue)
      {
        // need to rebind the area drop down as we are not using ajaxtoolkit cascading drop downs
        if (!String.IsNullOrEmpty(StateDropDownList.SelectedValue) && !_areaDropdownBound)
        {
          AreaDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetAreas(StateDropDownList.SelectedValueToLong());
          AreaDropDownList.DataTextField = "StateAreaName";
          AreaDropDownList.DataValueField = "Id";
          AreaDropDownList.DataBind();
          AreaDropDownList.Items.AddLocalisedTopDefault("Global.Area.TopDefault", "- select area -");

          var selectedArea = (!IsPostBack || (resetToDefaultOnPostBack && DefaultStateAreaId.HasValue))
            ? DefaultStateAreaId.ToString()
            : Request.Form[AreaDropDownList.UniqueID];

          if (!String.IsNullOrEmpty(selectedArea))
            AreaDropDownList.SelectedValue = selectedArea;

          _areaDropdownBound = true;
        }
      }
    }
	}
}