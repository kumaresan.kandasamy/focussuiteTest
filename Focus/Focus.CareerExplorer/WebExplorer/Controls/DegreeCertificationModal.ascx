<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DegreeCertificationModal.ascx.cs"
    Inherits=" Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationModal" %>
<%@ Register Src="ModalHeaderOptions.ascx" TagName="ModalHeaderOptions" TagPrefix="uc" %>
<%@ Register Src="JobList.ascx" TagName="DegreeCertificationJobList" TagPrefix="uc" %>
<%@ Register Src="SkillListSimple.ascx" TagName="DegreeCertificationSkillList" TagPrefix="uc" %>
<%@ Register Src="EmployerList.ascx" TagName="DegreeCertificationEmployerList" TagPrefix="uc" %>
<%@ Register Src="Criteria.ascx" TagName="CriteriaFilter" TagPrefix="uc" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
    PopupControlID="ModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">
    <div class="lightbox lightboxmodal" id="degreeCertificationLightBox" style="z-index: 10001">
        <uc:ModalHeaderOptions ID="ModalHeaderOptions" runat="server" OnBreadcrumbItemClicked="DegreeCertificationModalBreadcrumb_ItemClicked"
            OnBookmarkClicked="BookmarkLinkButton_Click" OnSendEmailClicked="SendEmailImageButton_Click"
            OnPrintCommand="ModalHeaderOptions_OnPrintCommand" OnCloseClicked="CloseButton_Click">
        </uc:ModalHeaderOptions>
        <div class="degree-scroll-pane">
            <table class="lightboxHeading" role="presentation">
                <tr>
                    <td>
                        <h2>
                            <span class="sr-only">Certification name</span>
                            <asp:Label ID="DegreeCertificationNameLabel" runat="server" />
                        </h2>
                    </td>
                </tr>
                <tr class="lightboxHeading">
                    <td>
                        <uc:CriteriaFilter ID="CriteriaFilter" runat="server" OnFilterCancelled="Filter_Cancelled" />
                    </td>
                </tr>
            </table>
            <table width="100%" role="presentation">
                <asp:PlaceHolder runat="server" ID="DegreeDetailsPlaceHolder" Visible="False">
                    <tr>
                        <td>
                            <p class="SeeMoreLessPanel">
                                <asp:Label runat="server" ID="DegreeDescription"></asp:Label>
                                <asp:PlaceHolder runat="server" ID="DegreeDescriptionOverflowPanel" Visible="False">
                                    <asp:Label runat="server" ID="DegreeDescriptionOverflowText" CssClass="SeeMoreLessText"></asp:Label>
                                    <a class="seeMore" href="#">
                                        <%= HtmlLocalise("Global.SeeMore.Text", "See More")%></a><a class="seeLess" href="#"><%= HtmlLocalise("Global.SeeLess.Text", "See Less")%></a>
                                </asp:PlaceHolder>
                            </p>
                            <p>
                                <asp:HyperLink runat="server" Target="_blank" ID="DegreeUrl"></asp:HyperLink>
                            </p>
                        </td>
                    </tr>
                </asp:PlaceHolder>
                <tr>
                    <td>
                        <asp:Label ID="DegreeCertificationBlurbLabel" runat="server" /><asp:Label ID="AverageSalaryLabel"
                            runat="server" />
                    </td>
                </tr>
                <tr class="modalLeftPanel">
                    <td>
                        <table width="100%" class="numberedAccordion" role="presentation">
                            <tr class="multipleAccordionTitle2 on" data-joblist="<%=DegreeCertificationJobList.ClientID %>">
                                <td class="column1">
                                    1
                                </td>
                                <td class="column2" width="600px">
                                    <div>
                                        <span class="toolTipNew" title="<%= HtmlLocalise("HiringDemand.TooltipText", "An #BUSINESS#:LOWER�s need for workers to fill their jobs is known as hiring demand. Select hiring demand to see the number of jobs, either nationally or by your geographic search area that #BUSINESSES#:LOWER posted over the last 12 months. Hiring demand is rated as low, medium, high, or very high for the area in your geographic search.") %>">
                                            <a href="#">
                                                <%= HtmlLocalise("Global.HiringDemand.Header", "Hiring demand for the last 12 months")%></a></span>
                                    </div>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2 open">
                                <td>
                                </td>
                                <td>
                                    <asp:Panel runat="server" ID="TieredJobsPanel" class="accordionLink" Style="padding-bottom: 15px">
                                        <asp:LinkButton ID="PrimaryJobsLinkButton" runat="server" OnCommand="JobDegreeTierLevel_ItemClicked"
                                            CommandArgument="1" Enabled="False" />
                                        <asp:LinkButton ID="SecondaryJobsLinkButton" runat="server" OnCommand="JobDegreeTierLevel_ItemClicked"
                                            CommandArgument="2" />
                                    </asp:Panel>
                                    <uc:DegreeCertificationJobList ID="DegreeCertificationJobList" runat="server" JobListMode="DegreeEducationLevel"
                                        OnItemClicked="ModalList_ItemClicked" />
                                    <asp:Literal runat="server" ID="NoJobsFoundLiteral" Visible="False"></asp:Literal>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr class="multipleAccordionTitle2">
                                <td class="column1">
                                    2
                                </td>
                                <td class="column2">
                                    <a href="#">
                                        <%= HtmlLocalise("TopSkills.Header", "In-demand skills")%></a>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2">
                                <td>
                                </td>
                                <td>
                                    <uc:DegreeCertificationSkillList ID="DegreeCertificationSkillList" runat="server"
                                        SkillsListMode="DegreeEducationLevel" OnItemClicked="ModalList_ItemClicked" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <asp:PlaceHolder runat="server" ID="TopEmployersSection">
                                <tr class="multipleAccordionTitle2">
                                    <td class="column1">
                                        3
                                    </td>
                                    <td class="column2">
                                        <a href="#">
                                            <%= HtmlLocalise("TopEmployers.Header", "In-demand #BUSINESSES#:LOWER")%></a>
                                    </td>
                                    <td class="column3" align="right" valign="top">
                                        <a class="show" href="#">
                                            <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                    </td>
                                </tr>
                                <tr class="accordionContent2">
                                    <td>
                                    </td>
                                    <td>
                                        <p>
                                            <asp:Label runat="server" ID="TopEmployersIntroLabel"></asp:Label></p>
                                        <uc:DegreeCertificationEmployerList ID="DegreeCertificationEmployerList" runat="server"
                                            EmployerListMode="DegreeEducationLevel" OnItemClicked="ModalList_ItemClicked"
                                            ShowEmployerJobPostingCount="True" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="Section4PlaceHolder" runat="server">
                                <tr class="multipleAccordionTitle2">
                                    <td class="column1">
                                        <asp:Literal runat="server" ID="Section4OverrideSectionNumber">4</asp:Literal>
                                    </td>
                                    <td class="column2">
                                        <asp:Literal ID="Section4HeaderLiteral" runat="server" />
                                    </td>
                                    <td class="column3" align="right" valign="top">
                                        <a class="show" href="#">
                                            <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                    </td>
                                </tr>
                                <tr class="accordionContent2">
                                    <td>
                                    </td>
                                    <td>
                                        <table width="100%" class="genericTable" role="presentation">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="StudyPlacePlaceHolder" runat="server">
                                                        <ul>
                                                            <asp:Repeater ID="StudyPlaceRepeater" runat="server">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <%# Eval("StudyPlaceName") %></li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </asp:PlaceHolder>
                                                    <asp:PlaceHolder ID="ClientDegreeInfoPlaceHolder" runat="server">
                                                        <ul>
                                                            <asp:Repeater ID="ClientDegreeInfoRepeater" runat="server">
                                                                <ItemTemplate>
                                                                    <li>
                                                                        <%# Eval("Name") %>&nbsp;
                                                                        <asp:HyperLink ID="ClientDegreeInfoHyperlink" runat="server" ImageUrl="<%# UrlBuilder.NewWindowIcon() %>"
                                                                            NavigateUrl='<%# Eval("Url") %>' Target="_blank" /></li>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ul>
                                                    </asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <asp:PlaceHolder ID="ClientDegreeProgramDisclosurePlaceHolder" runat="server">
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:Literal ID="ClientDegreeDisclaimerLiteral" runat="server" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                            </asp:PlaceHolder>
                        </table>
                    </td>
                </tr>
                <tr class="modalRightPanel">
                    <td valign="top">
                        <asp:PlaceHolder ID="ReadyToMakeAMovePlaceHolder" runat="server"><span class="promoStyle3">
                            <%= HtmlLocalise("ReadyToMakeAMove.Text", "Ready to make a move?")%>
                        </span><span class="promoStyle3Arrow"></span></asp:PlaceHolder>
                        <asp:PlaceHolder ID="ViewCurrentPositionsPlaceHolder" runat="server">
                            <asp:LinkButton ID="ViewCurrentPostingsLinkButton" Text="View Current Postings LinkButton"
                                runat="server" OnClick="ViewCurrentPostingsLinkButton_Click">
                                <span class="promoStyle4"><span class="modalLink">
                                    <asp:Literal runat="server" ID="ViewCurrentPositionsLiteral" Text="See current postings for<br />this program of study �"></asp:Literal>
                                </span></span>
                            </asp:LinkButton>
                        </asp:PlaceHolder>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
