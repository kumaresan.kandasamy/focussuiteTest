﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ModalHeaderOptions.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.ModalHeaderOptions" %>
<%@ Register src="ModalBreadcrumb.ascx" tagName="ModalBreadcrumb" tagPrefix="uc" %>

<table class="lightboxOptions" role="presentation">
  <tr>
    <td>
      <uc:ModalBreadcrumb ID="ModalBreadcrumb" runat="server" OnItemClicked="ModalBreadcrumb_OnItemClicked" />
    </td>
    <td valign="top" align="right">
      <asp:Image ID="ModalBookmarkImage" runat="server" Width="17" Height="32" alt="Bookmark Image" ></asp:Image>
    </td>
    <td width="145" valign="top">
      <asp:LinkButton ID="ModalBookmarkLinkButton" runat="server" class="toolTipNew" OnClick="ModalBookmarkLinkButton_OnClick" alt="Bookmark Link Button"><%= HtmlLocalise("BookmarkThisInfo.Text", "BOOKMARK THIS INFO")%></asp:LinkButton>
    </td>
    <td width="70" valign="top" align="right" id="EmailCell" runat="server">
      <asp:ImageButton ID="ModalSendEmailImageButton" runat="server"  CausesValidation="False" Height="24" Width="32" OnClick="ModalSendEmailImageButton_OnClick" alt="Send Email button"></asp:ImageButton>
    </td>
    <td width="40" valign="top" align="right" id="DownloadCell" runat="server">
      <asp:ImageButton ID="ModalDownloadImageButton" runat="server"  CausesValidation="False" Height="24" Width="30" CommandName="Print" CommandArgument="Pdf" OnCommand="ModalOptionButton_OnCommand" alt="Download button"  ></asp:ImageButton>
    </td>
    <td width="40" valign="top" align="right">
      <asp:ImageButton ID="ModalPrintImageButton" runat="server"  CausesValidation="False" Height="24" Width="30"  CommandName="Print" CommandArgument="Html" OnCommand="ModalOptionButton_OnCommand" alt="Print button"></asp:ImageButton>
    </td>
    <td width="70" valign="top" align="right">
      <asp:Button ID="ModalCloseButton" runat="server"  SkinID="closeModal" CausesValidation="false" OnClick="ModalCloseButton_OnClick" class="closeModal" />
    </td>
  </tr>
</table>
