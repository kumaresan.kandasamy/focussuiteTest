﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;

#endregion


namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class JobTitleSelector : UserControlBase
	{
    /// <summary>
    /// The Validation Group the selector belongs to
    /// </summary>
    public string SelectorValidationGroup
    {
      get { return ((string)ViewState["SelectorValidationGroup"] ?? string.Empty); }
      set { ViewState["SelectorValidationGroup"] = value; }
    }

    /// <summary>
    /// Whether to show the associated jobs list for job titles
    /// </summary>
    public bool ShowJobsDropDownForJobTitles
    {
      get { return (ViewState["ShowJobsDropDownForJobTitles"] != null && (bool)ViewState["ShowJobsDropDownForJobTitles"]); }
      set { ViewState["ShowJobsDropDownForJobTitles"] = value; }
    }

    /// <summary>
    /// Whether to show the associated jobs list for military occupations
    /// </summary>
    public bool ShowJobsDropDownForMilitaryOccupation
    {
      get { return (ViewState["ShowJobsDropDownForMilitaryOccupation"] != null && (bool)ViewState["ShowJobsDropDownForMilitaryOccupation"]); }
      set { ViewState["ShowJobsDropDownForMilitaryOccupation"] = value; }
    }

    /// <summary>
    /// Returns a boolean indicating whether the job title has been entered
    /// </summary>
    public bool JobTitleSelected
    {
      get { return JobTitleAutoCompleteTextBox_RadioButton.Checked;  }
    }

    /// <summary>
    /// Gets the value of the entered job title/military occupation
    /// </summary>
    public long? OccupationTitleValue
    {
      get 
      {
        return JobTitleSelected 
                ? JobTitleAutoCompleteTextBox.SelectedValueToNullableLong
                : MilitaryAutoCompleteTextBox.SelectedValueToNullableLong; 
      }
    }

    /// <summary>
    /// Gets the value of the related job
    /// </summary>
    public long? RelatedJobValue
    {
      get
      {
        return JobTitleSelected 
                ? JobTitleAutoComplete_RelatedJobs.SelectedValueToNullableLong()
                : MilitaryAutoCompleteTextBox_RelatedJobs.SelectedValueToNullableLong();
      }
    }

    /// <summary>
    /// Loading of the page to build the controls
    /// </summary>
    /// <param name="sender">Page being loaded</param>
    /// <param name="e">Standard event argument</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        LocaliseUI();
        BindControls();
      }
      else
      {
        // need to rebind the area drop down as we are not using ajaxtoolkit cascading drop downs
        if (!String.IsNullOrEmpty(JobTitleAutoCompleteTextBox.SelectedValue))
        {
          // Job Titles and Jobs
          JobTitleAutoComplete_RelatedJobs.DataSource =
            ServiceClientLocator.ExplorerClient(App).GetRelatedJobs(JobTitleAutoCompleteTextBox.SelectedValueToNullableLong.GetValueOrDefault(0));
          JobTitleAutoComplete_RelatedJobs.DataTextField = "Name";
          JobTitleAutoComplete_RelatedJobs.DataValueField = "Id";
          JobTitleAutoComplete_RelatedJobs.DataBind();
          JobTitleAutoComplete_RelatedJobs.Items.AddLocalisedTopDefault("Global.Job.TopDefault", "- select job -");

          var selectedJob = Request.Form[JobTitleAutoComplete_RelatedJobs.UniqueID];
          if (!String.IsNullOrEmpty(selectedJob))
          {
            JobTitleAutoComplete_RelatedJobs.SelectedValue = selectedJob;
          }

          JobTitleAutoCompleteTextBox_JobRow.Style[HtmlTextWriterStyle.Display] = "block";
        }

        if (!String.IsNullOrEmpty(MilitaryAutoCompleteTextBox.SelectedValue))
        {
          // Military Occupations
          MilitaryAutoCompleteTextBox_RelatedJobs.DataSource =
            ServiceClientLocator.ExplorerClient(App).GetJobsForMilitaryOccupation(MilitaryAutoCompleteTextBox.SelectedValueToNullableLong.GetValueOrDefault(0));
          MilitaryAutoCompleteTextBox_RelatedJobs.DataTextField = "Name";
          MilitaryAutoCompleteTextBox_RelatedJobs.DataValueField = "Id";
          MilitaryAutoCompleteTextBox_RelatedJobs.DataBind();
          MilitaryAutoCompleteTextBox_RelatedJobs.Items.AddLocalisedTopDefault("Global.Job.TopDefault", "- select job -");

          var selectedJob = Request.Form[MilitaryAutoCompleteTextBox_RelatedJobs.UniqueID];
          if (!String.IsNullOrEmpty(selectedJob))
          {
            MilitaryAutoCompleteTextBox_RelatedJobs.SelectedValue = selectedJob;
          }

          MilitaryAutoCompleteTextBox_JobRow.Style[HtmlTextWriterStyle.Display] = "block";
        }
      }

      SetupJavaScript();
    }

    /// <summary>
    /// Assign localised text to controls
    /// </summary>
    private void LocaliseUI()
    {
      JobTitleAutoCompleteTextBox_RadioButton.Text = CodeLocalise("StudyJobTitle.Label", "Job Title");
      JobTitleAutoCompleteTextBox.InFieldLabelText = CodeLocalise("TypeAJobTitle.Label", "Type a job title");
      JobTitleAutoCompleteTextBox_CustomValidator.Text = CodeLocalise("TypeAJobTitle.Validator", "Please enter a job title");

      MilitaryAutoCompleteTextBox_RadioButton.Text = CodeLocalise("StudyMilitary.Label", "Military Occupational Classification");
      MilitaryAutoCompleteTextBox.InFieldLabelText = CodeLocalise("TypeAMilitaryOccupation.Label", "Type an MOC code");
      MilitaryAutoCompleteTextBox_CustomValidator.Text = CodeLocalise("TypeAMilitaryOccupation.Validator", "Please enter an MOC code");
    }

    /// <summary>
    /// Binds controls on the page to data sources
    /// </summary>
    private void BindControls()
    {
      JobTitleAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
      JobTitleAutoCompleteTextBox.ServiceMethod = "GetJobTitleAutoCompleteItems";
      JobTitleAutoCompleteTextBox.ServiceMethodParameters = null;

      MilitaryAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
      MilitaryAutoCompleteTextBox.ServiceMethod = "GetMilitaryOccupationAutoCompleteItems";
      MilitaryAutoCompleteTextBox.ServiceMethodParameters = null;

      JobTitleAutoCompleteTextBox_CustomValidator.ValidationGroup = SelectorValidationGroup;
      MilitaryAutoCompleteTextBox_CustomValidator.ValidationGroup = SelectorValidationGroup;

      if (ShowJobsDropDownForJobTitles)
      {
        var js = String.Format(@"JobTitleSelectorValidateShowJobs(\'{0}\',\'{1}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');",
                               JobTitleAutoCompleteTextBox.ClientID,
                               JobTitleAutoComplete_RelatedJobs.ClientID,
                               String.Concat(UrlBuilder.AjaxService(), "/GetRelatedJobs"),
                               "RelatedJobs",
                               CodeLocalise("Global.Job.TopDefault", "- select job -"),
                               CodeLocalise("LoadingJobs.Text", "[Loading jobs...]"));

        JobTitleAutoCompleteTextBox.OnClientItemSelected = js;
        JobTitleAutoCompleteTextBox_JobRow.Visible = true;
        JobTitleAutoCompleteTextBox_JobRow.Style.Add(HtmlTextWriterStyle.Display, "none");

        JobTitleAutoComplete_RelatedJobs.Items.AddLocalisedTopDefault("Global.Jobs.TopDefault", "- select job -");
      }

      if (ShowJobsDropDownForMilitaryOccupation)
      {
        var js = String.Format(@"JobTitleSelectorValidateShowJobs(\'{0}\',\'{1}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');",
          MilitaryAutoCompleteTextBox.ClientID,
          MilitaryAutoCompleteTextBox_RelatedJobs.ClientID,
          String.Concat(UrlBuilder.AjaxService(), "/GetJobsForMilitaryOccupation"),
          "JobForMilitary",
          CodeLocalise("Global.Job.TopDefault", "- select job -"),
          CodeLocalise("LoadingJobs.Text", "[Loading jobs...]"));

        MilitaryAutoCompleteTextBox.OnClientItemSelected = js;
        MilitaryAutoCompleteTextBox_JobRow.Visible = true;
        MilitaryAutoCompleteTextBox_JobRow.Style.Add(HtmlTextWriterStyle.Display, "none");

        MilitaryAutoCompleteTextBox_RelatedJobs.Items.AddLocalisedTopDefault("Global.Jobs.TopDefault", "- select job -");
      }

			MilitaryRow.Visible = !App.Settings.HideMocCodes;
    }

    /// <summary>
    /// Sets up the javascript needed by the control
    /// </summary>
    private void SetupJavaScript()
    {
      var js =
  String.Format(@"
$(document).ready(function () 
{{
	$('#{0}').click(function () 
  {{
    if ($(this).prop('checked'))
    {{
		  $('#{5}').val('');
		  $('#{6}').val('');
		  $('#{7}').css('visibility','hidden');
      $('#{9}').hide();
      $('#{5}').blur();
    }}
	}});

	$('#{4}').click(function () 
  {{
    if ($(this).prop('checked'))
    {{
		  $('#{1}').val('');
		  $('#{2}').val('');
		  $('#{3}').css('visibility','hidden');
		  $('#{8}').hide();
		  $('#{1}').blur();
    }}
	}});

	$('#{1}').focus(function () 
  {{
		if (!$('#{0}').is(':checked'))
		{{
			$('#{0}').prop('checked', true);
			$('#{0}').click();
		}}
	}});

	$('#{5}').focus(function () 
  {{
		if (!$('#{4}').is(':checked'))
		{{
			$('#{4}').prop('checked', true);
			$('#{4}').click();
		}}
	}});
}});",
  JobTitleAutoCompleteTextBox_RadioButton.ClientID,
  JobTitleAutoCompleteTextBox.ClientID,
  JobTitleAutoCompleteTextBox.SelectedValueClientId,
  JobTitleAutoCompleteTextBox_CustomValidator.ClientID,
  MilitaryAutoCompleteTextBox_RadioButton.ClientID,
  MilitaryAutoCompleteTextBox.ClientID,
  MilitaryAutoCompleteTextBox.SelectedValueClientId,
  MilitaryAutoCompleteTextBox_CustomValidator.ClientID,
  JobTitleAutoCompleteTextBox_JobRow.ClientID,
  MilitaryAutoCompleteTextBox_JobRow.ClientID);

      Page.ClientScript.RegisterClientScriptBlock(GetType(), string.Concat(ClientID, ":ResetOptions"), js, true);

      js = @"
function JobTitleSelectorValidate(source, arguments)
{
  arguments.IsValid = true;

  var radioButton = $('#' + source.id.replace('_CustomValidator', '_RadioButton'));
  if (radioButton.is(':checked') && radioButton.is(':visible'))
  {
    var valueBox = $('#' + source.id.replace('_CustomValidator', '_SelectedValue'));
    if (valueBox.val().length == 0)
    {
      var row = $('#' + source.id.replace('_CustomValidator', '_JobRow'));
      row.hide();

      arguments.IsValid = false;
    }
  }
}

function JobTitleSelectorValidateShowJobs(textBoxId, dropDownId, serviceUrl, category, selectingText, loadingText)
{
  var selectedValue = $('#' + textBoxId + '_SelectedValue').val();
  var selectedIndex = -1;

  var row = $('#' + textBoxId + '_JobRow');
  var validator = $('#' + textBoxId + '_CustomValidator');
  if (selectedValue.length > 0)
  {
    row.show();
    validator.css('visibility','hidden');
  }
  else
  {
    row.hide();
  }

  LoadCascadingDropDownList(selectedValue, dropDownId, serviceUrl, category, selectingText, loadingText, selectedIndex);
}";
      Page.ClientScript.RegisterClientScriptBlock(GetType(), "JobTitleSelectorFunctions", js, true);
    }
  }
}