#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public enum EmployerListModes
	{
		Report,
		Job,
		DegreeEducationLevel,
		Skill,
		Internship
	}

	public partial class EmployerList : UserControlBase
	{
		public ExplorerCriteria ReportCriteria { get; set; }
		public int ListSize { get; set; }
		public long JobId { get; set; }
		public long DegreeEducationLevelId { get; set; }
		public long SkillId { get; set; }
    public long InternshipCategoryId { get; set; }
    public int RecordCount { get { return EmployerColumnOneRepeater != null ? EmployerColumnOneRepeater.Items.Count : 0; } }
    public bool ShowEmployerJobPostingCount { get; set; }

		public EmployerListModes EmployerListMode
		{
			get
			{
				return GetViewStateValue("EmployerListMode", EmployerListModes.Report);
			}
			set { SetViewStateValue("EmployerListMode", value); }
		}

		public List<EmployerJobViewDto> Employers
		{
			get { return GetViewStateValue("EmployerList:Employers", new List<EmployerJobViewDto>()); }
			set { SetViewStateValue("EmployerList:Employers", value); }
		}

		private int _rank = 1;
		private List<string> _preferredEmployers;

		public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
		public event ItemClickedHandler ItemClicked;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			_preferredEmployers = new List<string>();

			var preferredEmployers = CodeLocalise("Explorer.PreferredEmployers", String.Empty);
			if (!String.IsNullOrEmpty(preferredEmployers))
			{
				_preferredEmployers = preferredEmployers.Split('|').ToList();
			}
		}

    /// <summary>
    /// Binds the list.
    /// </summary>
		public void BindList()
		{
			var employers = new List<EmployerJobViewDto>();

			switch (EmployerListMode)
			{
				case EmployerListModes.Report:

					employers = ServiceClientLocator.ExplorerClient(App).GetEmployerReport(ReportCriteria, ListSize);

					EmployerColumnOneRepeater.DataSource = employers;
					EmployerColumnOneRepeater.DataBind();

					EmployerColumnTwoPlaceHolder.Visible = false;

					break;

				case EmployerListModes.Job:
				case EmployerListModes.DegreeEducationLevel:
				case EmployerListModes.Skill:
				case EmployerListModes.Internship:
          
          if (EmployerListMode == EmployerListModes.Job)
          {
            employers = ServiceClientLocator.ExplorerClient(App).GetJobEmployers(ReportCriteria, 20);
          }
          else if (EmployerListMode == EmployerListModes.DegreeEducationLevel)
          {
            ReportCriteria.DegreeEducationLevelId = DegreeEducationLevelId;
            employers = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevelEmployers(ReportCriteria, 20);
          }
          else if (EmployerListMode == EmployerListModes.Skill)
          {
            employers = ServiceClientLocator.ExplorerClient(App).GetSkillEmployers(ReportCriteria, 20);
          }
          else
          {
            employers = ServiceClientLocator.ExplorerClient(App).GetInternshipEmployers(ReportCriteria, 20);
          }

					EmployerColumnOneRepeater.DataSource = employers.Take(10).ToList();
					EmployerColumnOneRepeater.DataBind();

					if (employers.Count > 10)
					{
						EmployerColumnTwoRepeater.DataSource = employers.Skip(10).ToList();
						EmployerColumnTwoRepeater.DataBind();
					}
					else
					{
						EmployerColumnTwoPlaceHolder.Visible = false;
					}

					break;
			}

			Employers = employers;
		}

    /// <summary>
    /// Handles the ItemDataBound event of the EmployerRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var employer = (EmployerJobViewDto) e.Item.DataItem;

				var countLiteral = (Literal)e.Item.FindControl("EmployerCountLiteral");
				countLiteral.Text = _rank.ToString();

        if (ShowEmployerJobPostingCount)
        {
          var openingsLiteral = (Literal) e.Item.FindControl("EmployerOpeningsLiteral");
          openingsLiteral.Text = String.Format("({0}{1})", employer.Positions.ToString("#,#"),
                                               _rank == 1 ? " openings" : String.Empty);
        }

			  var preferredEmployersPlaceHolder = (PlaceHolder) e.Item.FindControl("PreferredEmployerPlaceHolder");
				var preferredEmployersImage = (Image) e.Item.FindControl("PreferredEmployerImage");
				preferredEmployersImage.ImageUrl = UrlBuilder.StarCompanies();
				preferredEmployersImage.ToolTip = CodeLocalise("PreferredEmployer.Text", "Preferred #BUSINESS#");
				if (_preferredEmployers != null && _preferredEmployers.Count > 0)
				{
					if (!_preferredEmployers.Contains(employer.EmployerName))
					{
						preferredEmployersPlaceHolder.Visible = false;
					}
				}
				else
				{
					preferredEmployersPlaceHolder.Visible = false;
				}

				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemCommand event of the EmployerRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
		protected void EmployerRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "ViewEmployer")
			{
				OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
			}			
		}

    /// <summary>
    /// Raises the <see cref="E:ItemClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnItemClicked(CommandEventArgs eventArgs)
		{
			if (ItemClicked != null)
				ItemClicked(this, eventArgs);
		}
	}
}