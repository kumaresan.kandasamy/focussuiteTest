﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class DegreeCertificationList : ExplorerListControl
	{
    public override int RecordCount { get { return DegreeAreaRepeater.Items.Count + DegreeListView.Items.Count + BachelorsDegreesListView.Items.Count + GraduateDegreesAndCertificatesListView.Items.Count; } }
    
	  private List<JobDegreeEducationLevelReportViewDto> CertificateAndAssociateDegreeEducationLevelReport
	  {
	    get
	    {
	      return App.GetSessionValue("Explorer:CertificateAndAssociateDegreeEducationLevelReport",
	                                 new List<JobDegreeEducationLevelReportViewDto>());
	    }
	    set { App.SetSessionValue("Explorer:CertificateAndAssociateDegreeEducationLevelReport", value); }
	  }

    private List<JobDegreeEducationLevelReportViewDto> BachelorDegreeEducationLevelReport
    {
      get
      {
        return App.GetSessionValue("Explorer:BachelorDegreeEducationLevelReport",
                                   new List<JobDegreeEducationLevelReportViewDto>());
      }
      set { App.SetSessionValue("Explorer:BachelorDegreeEducationLevelReport", value); }
    }

	  private List<JobDegreeEducationLevelReportViewDto> GraduateDegreeAndCertificateLevelReport
	  {
	    get
	    {
	      return App.GetSessionValue("Explorer:GraduateDegreeAndCertificateLevelReport",
	        new List<JobDegreeEducationLevelReportViewDto>());
	    }
	    set { App.SetSessionValue("Explorer:GraduateDegreeAndCertificateLevelReport", value); }
	  }

	  private int _rank = 1;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{

		}

    /// <summary>
    /// Binds the list.
    /// </summary>
    /// <param name="forceReload">if set to <c>true</c> [force reload].</param>
		public override void BindList(bool forceReload)
		{
      if (forceReload)
      {
        CertificateAndAssociateDegreeEducationLevelReport = null;
        BachelorDegreeEducationLevelReport = null;
        GraduateDegreeAndCertificateLevelReport = null;
      }

      // Bind degree lists
		  _rank = 1;
      DegreeListView.DataBind();
      _rank = 1;
      BachelorsDegreesListView.DataBind();
      _rank = 1;
      GraduateDegreesAndCertificatesListView.DataBind();

      // Bind degree area lists
      _rank = 1;
      var degreeCertificationReports = CertificateAndAssociateDegreeEducationLevelReport;
      DegreeAreaRepeater.DataSource =
        degreeCertificationReports.Where(x => x.IsDegreeArea).OrderByDescending(x => x.HiringDemand).
          ThenByDescending(x => x.DegreesAwarded).ToList();
      DegreeAreaRepeater.DataBind();
      DegreeAreaTable.Visible = DegreeAreaRepeater.Items.Count > 0;

      _rank = 1;
      degreeCertificationReports = BachelorDegreeEducationLevelReport;
      BachelorsDegreesRepeater.DataSource = degreeCertificationReports.Where(x => x.IsDegreeArea).OrderByDescending(x => x.HiringDemand).
        ThenByDescending(x => x.DegreesAwarded).ToList();
      BachelorsDegreesRepeater.DataBind();
      BachelorsDegreesTable.Visible = BachelorsDegreesRepeater.Items.Count > 0;

      _rank = 1;
      degreeCertificationReports = GraduateDegreeAndCertificateLevelReport;
      GraduateDegreesAndCertificatesRepeater.DataSource =
        degreeCertificationReports.Where(x => x.IsDegreeArea).OrderByDescending(x => x.HiringDemand).
          ThenByDescending(x => x.DegreesAwarded).ToList();
      GraduateDegreesAndCertificatesRepeater.DataBind();
      GraduateDegreesAndCertificatesTable.Visible = GraduateDegreesAndCertificatesRepeater.Items.Count > 0;
    }

    /// <summary>
    /// Gets the degree education level reports.
    /// </summary>
    /// <param name="reportCriteria">The report criteria.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <param name="degreeLevel">The degree level.</param>
    /// <param name="orderBy">The order by.</param>
    /// <returns></returns>
    public List<JobDegreeEducationLevelReportViewDto> GetDegreeEducationLevelReports(ExplorerCriteria reportCriteria, int listSize, DetailedDegreeLevels degreeLevel, string orderBy)
		{
			var doSort = true;
			// get reports from view state
      List<JobDegreeEducationLevelReportViewDto> degreeCertificationReports = null;
      switch (degreeLevel)
      {
        case DetailedDegreeLevels.CertificateOrAssociate:
          degreeCertificationReports = CertificateAndAssociateDegreeEducationLevelReport;
          break;

        case DetailedDegreeLevels.Bachelors:
          degreeCertificationReports = BachelorDegreeEducationLevelReport;
          break;

        case DetailedDegreeLevels.GraduateOrProfessional:
          degreeCertificationReports = GraduateDegreeAndCertificateLevelReport;
          break;
      }

			// check if anything returned
			if (degreeCertificationReports == null || degreeCertificationReports.Count == 0)
			{
				// check if we have report criteria
				if (reportCriteria != null)
				{
					// get reports and store in view state
          degreeCertificationReports = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevelReport(reportCriteria, degreeLevel, listSize);

          switch (degreeLevel)
          {
            case DetailedDegreeLevels.CertificateOrAssociate:
              CertificateAndAssociateDegreeEducationLevelReport = degreeCertificationReports;
              break;
            case DetailedDegreeLevels.Bachelors:
              BachelorDegreeEducationLevelReport = degreeCertificationReports;
              break;
            case DetailedDegreeLevels.GraduateOrProfessional:
              GraduateDegreeAndCertificateLevelReport = degreeCertificationReports;
              break;
          }

					doSort = false;
				}
				else
				{
					degreeCertificationReports = new List<JobDegreeEducationLevelReportViewDto>();
					doSort = false;
				}
			}

			degreeCertificationReports = degreeCertificationReports.Where(x => x.IsDegreeArea == false).ToList();

			if (doSort)
			{
				if (String.IsNullOrEmpty(orderBy))
					orderBy = "HiringDemand DESC";

				var sortDescending = false;
				if (orderBy.EndsWith(" DESC"))
				{
					sortDescending = true;
					orderBy = orderBy.Substring(0, orderBy.LastIndexOf(" DESC"));
				}

				switch (orderBy)
				{
					case "DegreeName":
						degreeCertificationReports = sortDescending
						                             	? degreeCertificationReports.OrderByDescending(x => x.DegreeName).
						                             	  	ToList()
						                             	: degreeCertificationReports.OrderBy(x => x.DegreeName).ToList();
						break;
				}
			}

			return degreeCertificationReports;
		}

    /// <summary>
    /// Handles the Selecting event of the DegreeListViewODS control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void DegreeListViewODS_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["reportCriteria"] = ReportCriteria;
			e.InputParameters["listSize"] = ListSize;
		}

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void DegreeListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
				var degreeCertificationReport = (JobDegreeEducationLevelReportViewDto) e.Item.DataItem;

				var rankLiteral = (Literal) e.Item.FindControl("RankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;

				var educationLevelLabel = (Label) e.Item.FindControl("EducationLevelLabel");
				switch (degreeCertificationReport.EducationLevel)
				{
					case ExplorerEducationLevels.Certificate:
						educationLevelLabel.Text = CodeLocalise(ExplorerEducationLevels.Certificate, "Certificate");
						educationLevelLabel.CssClass = "toolTipNew";
						educationLevelLabel.ToolTip = CodeLocalise("Certificate.TooltipText",
						                                           "Certificates are professional educational credentials that typically are below an associate degree level.");
            break;

					case ExplorerEducationLevels.Associate:
						educationLevelLabel.Text = CodeLocalise(ExplorerEducationLevels.Associate, "Associate");
						break;

					case ExplorerEducationLevels.Bachelor:
						educationLevelLabel.Text = CodeLocalise(ExplorerEducationLevels.Bachelor, "Bachelor");
						break;

					case ExplorerEducationLevels.PostBachelorCertificate:
						educationLevelLabel.Text = CodeLocalise(ExplorerEducationLevels.PostBachelorCertificate, "Post-Baccalaureate Certificate");
						break;

					case ExplorerEducationLevels.GraduateProfessional:
						educationLevelLabel.Text = CodeLocalise(ExplorerEducationLevels.GraduateProfessional, "Graduate or Professional");
						break;

					case ExplorerEducationLevels.PostMastersCertificate:
						educationLevelLabel.Text = CodeLocalise(ExplorerEducationLevels.PostMastersCertificate, "Graduate/Professional");
						break;

					case ExplorerEducationLevels.Doctorate:
						educationLevelLabel.Text = CodeLocalise(ExplorerEducationLevels.Doctorate, "Doctorate");
						break;
				}
			}
		}

    /// <summary>
    /// Handles the ItemCommand event of the DegreeListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewCommandEventArgs"/> instance containing the event data.</param>
		protected void DegreeListView_ItemCommand(object sender, ListViewCommandEventArgs e)
		{
			if (e.CommandName == "ViewDegreeCertification")
			  OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
			
		}

    /// <summary>
    /// Handles the Sorting event of the DegreeListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void DegreeListView_Sorting(object sender, ListViewSortEventArgs e)
		{
			var sortImageUrl = e.SortDirection == SortDirection.Ascending ? UrlBuilder.SortUp() : UrlBuilder.SortDown();

			var degreeSortImage = (Image)DegreeListView.FindControl("DegreeSortImage");
			var educationLevelSortImage = (Image)DegreeListView.FindControl("EducationLevelSortImage");

			switch (e.SortExpression)
			{
				case "DegreeName":
					degreeSortImage.ImageUrl = sortImageUrl;
					degreeSortImage.Visible = true;
					educationLevelSortImage.Visible = false;
					break;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeAreaRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void DegreeAreaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var rankLiteral = (Literal)e.Item.FindControl("RankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;				
			}
		}

    /// <summary>
    /// Handles the ItemCommand event of the DegreeAreaRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void DegreeAreaRepeater_ItemCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "ViewDegreeCertification")
			  OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
		}
	}
}