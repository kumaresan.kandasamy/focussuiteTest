﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class ProgramAreaDegreeStudyModal : ExplorerModalControl
	{
    public ProgramAreaDto ProgramArea
    {
      get { return GetViewStateValue<ProgramAreaDto>("ProgramAreaDegreeStudyModal:ProgramArea", null); }
      set { SetViewStateValue("ProgramAreaDegreeStudyModal:ProgramArea", value); }
    }

    public DegreeDto Degree
    {
      get { return GetViewStateValue<DegreeDto>("ProgramAreaDegreeStudyModal:Degree", null); }
      set { SetViewStateValue("ProgramAreaDegreeStudyModal:Degree", value); }
    }

    public ExplorerCriteria ReportCriteria
    {
      get { return GetViewStateValue<ExplorerCriteria>("ProgramAreaDegreeStudyModal:ReportCriteria"); }
      set { SetViewStateValue("ProgramAreaDegreeStudyModal:ReportCriteria", value); }
    }

    public List<LightboxBreadcrumb> Breadcrumbs
    {
      get { return GetViewStateValue("ProgramAreaDegreeStudyModal:Breadcrumb", new List<LightboxBreadcrumb>()); }
      set { SetViewStateValue("ProgramAreaDegreeStudyModal:Breadcrumb", value); }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Shows the modal control
    /// </summary>
    /// <param name="programAreaId">The id of the program area (department)</param>
    /// <param name="criteria">Any existing search criteria</param>
    /// <param name="breadcrumbs">The breadcrumb trail</param>
    /// <param name="printFormat">The format to print the modal</param>
    public void Show(long programAreaId, ExplorerCriteria criteria, List<LightboxBreadcrumb> breadcrumbs, ModalPrintFormat printFormat = ModalPrintFormat.None)
    {
      Show(programAreaId, 0, criteria, breadcrumbs, printFormat);
    }

    /// <summary>
    /// Shows the modal control
    /// </summary>
    /// <param name="programAreaId">The id of the program area (department)</param>
    /// <param name="degreeId">The Id of the degree (0 if not needed)</param>
    /// <param name="criteria">Any existing search criteria</param>
    /// <param name="breadcrumbs">The breadcrumb trail</param>
    /// <param name="printFormat">The format to print the modal</param>
    public void Show(long programAreaId, long degreeId, ExplorerCriteria criteria, List<LightboxBreadcrumb> breadcrumbs, ModalPrintFormat printFormat = ModalPrintFormat.None)
    {
      ProgramArea = ServiceClientLocator.ExplorerClient(App).GetProgramArea(programAreaId);
      Degree = degreeId > 0 ? ServiceClientLocator.ExplorerClient(App).GetDegree(degreeId) : null;

      //criteria.EducationCriteriaOption = EducationCriteriaOptions.Specific;
      criteria.ProgramAreaId = ProgramArea.Id;
      criteria.ProgramArea = ProgramArea.Name;

      if (Degree != null)
      {
        criteria.DegreeId = Degree.Id;
        criteria.Degree = Degree.Name;
      }
      else
      {
        criteria.DegreeId = null;
        criteria.Degree = null;
      }

      ReportCriteria = criteria;
      ReportCriteria.ReportType = ReportTypes.ProgramAreaDegree;

      Breadcrumbs = breadcrumbs;

      CriteriaFilter.Bind(ReportTypes.ProgramAreaDegree, criteria);

      ModalHeaderOptions.BindBreadcrumbs(Breadcrumbs);

      BindControls();

      ModalPopup.Show();

      if (printFormat != ModalPrintFormat.None)
        PrintModal(printFormat);
    }

    /// <summary>
    /// Binds the controls on the page to data sources
    /// </summary>
    protected void BindControls()
    {
      ProgramAreaDegreeStudyNameLabel.Text = (Degree == null)
        ? string.Concat(ProgramArea.Name, " - ", CodeLocalise("Global.Degree.AllDegrees", "All Degrees", "-1"))
        : string.Concat(ProgramArea.Name, " - ", Degree.Name);

      var description = ProgramArea.Description.Trim(new[] { ' ', '.' });
      var sentences = description.Split('.');
      if (sentences.Length > 3)
      {
        ProgramAreaDescription.Text = string.Concat(string.Join(". ", sentences, 0, 3), ". ");
        ProgramAreaDescriptionOverflowPanel.Visible = true;
        ProgramAreaDescriptionOverflowText.Text = string.Concat(string.Join(". ", sentences, 3, sentences.Length - 3), ".");
      }
      else
      {
        ProgramAreaDescription.Text = ProgramArea.Description;
      }

      ProgramAreaDegreeJobList.ProgramAreaId = ReportCriteria.ProgramAreaId.GetValueOrDefault(0);
      ProgramAreaDegreeJobList.DegreeId = ReportCriteria.DegreeId.GetValueOrDefault(0);
      ProgramAreaDegreeJobList.ReportCriteria = ReportCriteria;
      ProgramAreaDegreeJobList.ListSize = 50;
      ProgramAreaDegreeJobList.EnablePaging = true;
      ProgramAreaDegreeJobList.BindList(true);

      ProgramAreaDegreeEducationList.ReportCriteria = ReportCriteria;
      ProgramAreaDegreeEducationList.ListSize = 50;
      ProgramAreaDegreeEducationList.BindList();
    }

    /// <summary>
    /// Fires when a breadcrumb is clicked
    /// </summary>
    /// <param name="sender">Link raising the event</param>
    /// <param name="e">Standard event argument</param>
    protected void ProgramAreaDegreeStudyModalBreadcrumb_ItemClicked(object sender, CommandEventArgs e)
    {
      long itemId;
      if (long.TryParse(e.CommandArgument.ToString(), out itemId))
      {
        OnItemClicked(new CommandEventArgs(e.CommandName,
                                           new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = Breadcrumbs, MovingBack = true }));
      }
    }

    /// <summary>
    /// Fired when the "Bookmark This" link is click
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void BookmarkLinkButton_Click(object sender, EventArgs e)
    {
      OnBookmarkClicked(new LightboxEventArgs
      {
        CommandName = "BookmarkProgramAreaDegreeStudy",
        CommandArgument = new LightboxEventCommandArguement
        {
          Id = ProgramArea.Id.GetValueOrDefault(0),
          Breadcrumbs = Breadcrumbs,
          LightboxName = ProgramAreaDegreeStudyNameLabel.Text,
          LightboxLocation = "",
          CriteriaHolder = ReportCriteria
        }
      });
    }

    /// <summary>
    /// Fired when the Email link is click
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void SendEmailImageButton_Click(object sender, EventArgs e)
    {
      OnSendEmailClicked(new LightboxEventArgs
      {
        CommandName = "SendProgramAreaDegreeStudyEmail",
        CommandArgument = new LightboxEventCommandArguement
        {
          Id = ProgramArea.Id.GetValueOrDefault(0),
          Breadcrumbs = Breadcrumbs,
          LightboxName = ProgramAreaDegreeStudyNameLabel.Text,
          LightboxLocation = "",
          CriteriaHolder = ReportCriteria
        }
      });
    }

    /// <summary>
    /// Fired when the Print button is click
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalHeaderOptions_OnPrintCommand(object sender, CommandEventArgs eventArgs)
    {
      var printFormat = (ModalPrintFormat)Enum.Parse(typeof(ModalPrintFormat), eventArgs.CommandArgument.ToString(), true);
      if (ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
      {
        PrintModal(printFormat);

        ModalPopup.Show();
      }
      else
      {
        OnPrintClicked(new LightboxEventArgs()
        {
          CommandName = "PrintProgramAreaDegreeStudy",
          CommandArgument = new LightboxEventCommandArguement
          {
            Id = ProgramArea.Id.GetValueOrDefault(0),
            Breadcrumbs = Breadcrumbs,
            LightboxName = ProgramAreaDegreeStudyNameLabel.Text,
            LightboxLocation = "",
            CriteriaHolder = ReportCriteria,
            PrintFormat = printFormat
          }
        });
      }
    }

    /// <summary>
    /// Fired when a filter is cancelled
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Filter_Cancelled(object sender, FilterEventArgs e)
    {
      var oldCriteria = e.CommandArgument.OldCriteria;
      var newCriteria = e.CommandArgument.NewCriteria;

      var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.ProgramAreaDegree, LinkId = ProgramArea.Id.GetValueOrDefault(0), LinkText = ProgramArea.Name, Criteria = oldCriteria };
      var breadcrumbs = Breadcrumbs;
      breadcrumbs.Add(breadcrumb);

      OnItemClicked(new CommandEventArgs("ViewProgramAreaDegree",
                                         new LightboxEventCommandArguement
                                         {
                                           Id = oldCriteria.ProgramAreaId.GetValueOrDefault(),
                                           Breadcrumbs = breadcrumbs,
                                           CriteriaHolder = newCriteria
                                         }));
    }

    /// <summary>
    /// Fired when the close modal link is click
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void CloseButton_Click(object sender, EventArgs e)
    {
      ReportCriteria.CareerArea = "All Career Areas";
      ReportCriteria.ReportType = ReportTypes.Job;
      ReportCriteria.EducationCriteriaOption = EducationCriteriaOptions.DegreeLevel;
      ReportCriteria.DegreeEducationLevel = null;
      ReportCriteria.DegreeEducationLevelId = null;

      ModalPopup.Hide();
    }

    /// <summary>
    /// Fired when an item in one of the modals is clicked
    /// </summary>
    /// <param name="sender">Modal raising the event</param>
    /// <param name="e">Arguments for the item being clicked</param>
    protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "ViewJob":
        case "ViewDegreeCertification": 
          long itemId;
          if (long.TryParse(e.CommandArgument.ToString(), out itemId))
          {
            var breadcrumb = new LightboxBreadcrumb
            {
              LinkType = ReportTypes.ProgramAreaDegree,
              LinkId = ProgramArea.Id.GetValueOrDefault(0),
              LinkText = ProgramAreaDegreeStudyNameLabel.Text,
              Criteria = ReportCriteria
            };
            var breadcrumbs = Breadcrumbs;
            breadcrumbs.Add(breadcrumb);

            OnItemClicked(new CommandEventArgs(e.CommandName,
                                               new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = breadcrumbs }));
          }
          break;
      }
    }

    /// <summary>
    /// Opens the print window for the modal
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintModal(ModalPrintFormat printFormat = ModalPrintFormat.Html)
    {
      ProgramAreaDegreeEducationList.ReportCriteria = ReportCriteria;
      ProgramAreaDegreeEducationList.ListSize = 50;

      var printModel = new ExplorerProgramAreaDegreePrintModel
      {
        Name = ProgramAreaDegreeStudyNameLabel.Text,
        Description = ProgramAreaDescription.Text,
        Jobs = ProgramAreaDegreeJobList.GetJobReports(JobListModes.ProgramAreaDegree),
        Degrees = ProgramAreaDegreeEducationList.GetDegrees(),
        ReportCriteria = ReportCriteria
      };

      const string sessionKey = "Explorer:ProgramAreaDegreePrintModel";
      App.SetSessionValue(sessionKey, printModel);

      OpenPrintWindow(ReportTypes.ProgramAreaDegree, printFormat);
    }
  }
}