﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgramAreaDegreeStudyModal.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.ProgramAreaDegreeStudyModal" %>

<%@ Register src="ModalHeaderOptions.ascx" tagName="ModalHeaderOptions" tagPrefix="uc" %>
<%@ Register Src="Criteria.ascx" TagName="CriteriaFilter" tagPrefix="uc" %>
<%@ Register Src="JobList.ascx" TagName="ProgramAreaDegreeJobList" TagPrefix="uc" %>
<%@ Register Src="ProgramAreaDegreeList.ascx" TagName="ProgramAreaDegreeEducationList" TagPrefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel" 
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
                        
<asp:Panel id="PrintPanel" runat="server"></asp:Panel>

<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal lightbox" Style="display:none;">

  <div class="lightbox lightboxmodal" id="programAreaDegreeStudyLightBox" style="z-index: 10001">
        
    <uc:ModalHeaderOptions ID="ModalHeaderOptions" runat="server"
        OnBreadcrumbItemClicked="ProgramAreaDegreeStudyModalBreadcrumb_ItemClicked"
        OnBookmarkClicked="BookmarkLinkButton_Click"
        OnSendEmailClicked="SendEmailImageButton_Click"
        OnPrintCommand="ModalHeaderOptions_OnPrintCommand"
        OnCloseClicked="CloseButton_Click">
    </uc:ModalHeaderOptions>

    <div class="degree-scroll-pane">
      <table class="lightboxHeading" role="presentation">
        <tr>
          <td><h2>
	          <span class="sr-only">Study name</span>
	          <asp:Label ID="ProgramAreaDegreeStudyNameLabel" runat="server" />
          </h2></td>
        </tr>
        <tr class="lightboxHeading"><td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" OnFilterCancelled="Filter_Cancelled"/></td></tr>
      </table>
      
      <p class="SeeMoreLessPanel">
        <asp:Literal runat="server" ID="ProgramAreaDescription"></asp:Literal>
        <asp:PlaceHolder runat="server" ID="ProgramAreaDescriptionOverflowPanel" Visible="False">
          <asp:Label runat="server" ID="ProgramAreaDescriptionOverflowText" CssClass="SeeMoreLessText"></asp:Label>
          <a class="seeMore" href="#"><%= HtmlLocalise("Global.SeeMore.Text", "See More")%></a><a class="seeLess" href="#"><%= HtmlLocalise("Global.SeeLess.Text", "See Less")%></a>
        </asp:PlaceHolder>
      </p>

      <table width="100%" role="presentation">
        <tr>
          <td width="67.5%">
            
            <table width="100%" class="numberedAccordion" role="presentation">
              <tr class="multipleAccordionTitle2 on">
                <td width="10" class="column1">1</td>
                <td class="column2">
                  <div style="height:22px;">
                    <span class="toolTipHover"><%= HtmlLocalise("DegreesOffered.Header", "Degrees offered")%></span>
                  </div>
                </td>
                <td class="column3" align="right" valign="top"><a class="show" href="#"><%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a></td>
              </tr>
              <tr class="accordionContent2 open">
                <td></td>
                <td>
                  <uc:ProgramAreaDegreeEducationList ID="ProgramAreaDegreeEducationList" runat="server" OnItemClicked="ModalList_ItemClicked" />
                </td>
                <td></td>
              </tr>
               
              <tr class="multipleAccordionTitle2" data-joblist="<%=ProgramAreaDegreeJobList.ClientID %>">
                <td width="10" class="column1">2</td>
                <td class="column2">
                  <div style="height:22px;">
										<span class="toolTipNew" title="<%= HtmlLocalise("HiringDemand.TooltipText", "An #BUSINESS#:LOWER’s need for workers to fill their jobs is known as hiring demand. Select hiring demand to see the number of jobs, either nationally or by your geographic search area that #BUSINESSES#:LOWER posted over the last 12 months. Hiring demand is rated as low, medium, high, or very high for the area in your geographic search.")%>"><%= HtmlLocalise("Global.HiringDemand.Header", "Hiring demand for the last 12 months")%></span>
                  </div>
                </td>
                <td class="column3" align="right" valign="top"><a class="show" href="#"><%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a></td>
              </tr>
              <tr class="accordionContent2">
                <td></td>
                <td>
                    <uc:ProgramAreaDegreeJobList ID="ProgramAreaDegreeJobList" runat="server" JobListMode="ProgramAreaDegree" OnItemClicked="ModalList_ItemClicked" />
                </td>
                <td></td>
              </tr>
            </table>

          </td>
          <td></td>
        </tr>
      </table>
    </div>
  </div>
</asp:Panel>