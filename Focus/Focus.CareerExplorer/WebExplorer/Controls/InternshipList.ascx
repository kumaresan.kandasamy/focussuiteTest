<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternshipList.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.InternshipList" %>
<asp:Panel runat="server" ID="InternshipCountPanel">
	<strong><%= HtmlLocalise("TotalInternships.Label", "Total internships posted over the last 12 months in your selected area")%>: <asp:Literal ID="TotalInternshipsLiteral" runat="server" /><br /></strong>
</asp:Panel>

<asp:UpdatePanel ID="InternshipListUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
	<ContentTemplate>
<asp:ListView ID="InternshipListView" runat="server" DataSourceID="InternshipListViewODS" ItemPlaceholderID="InternshipListViewPlaceHolder" 
	OnItemDataBound="InternshipListView_ItemDataBound" OnSorting="InternshipListView_Sorting" OnItemCommand="InternshipListView_ItemCommand" OnLayoutCreated="InternshipListView_OnLayoutCreated">
	<LayoutTemplate>
		<table width="100%" class="genericTable withHeader" role="presentation">
			<thead>
				<tr>
					<td class="tableHead">
						<asp:LinkButton ID="InternshipNameSortButton" runat="server" CommandName="Sort" CommandArgument="InternshipName"><%= HtmlLocalise("Category.ColumnTitle", "CATEGORY")%></asp:LinkButton>
						<asp:Image ID="InternshipNameSortImage" runat="server" Visible="False" AlternateText="Internship Name Sort Image" />
					</td>
					<td class="tableHead">
						<asp:LinkButton ID="NumberAvailableSortButton" runat="server" CommandName="Sort" CommandArgument="NumberAvailable"><%= HtmlLocalise("NumberInternships.ColumnTitle", "NUMBER OF INTERNSHIPS")%></asp:LinkButton>
						<asp:Image ID="NumberAvailableSortImage" runat="server" AlternateText="Number Available Sort Image" />
					</td>
				</tr>
			</thead>
			<tbody id="InternshipListViewPlaceHolder" runat="server"></tbody>
		</table>
	</LayoutTemplate>
	<ItemTemplate>
				<tr>
					<td width="40%"><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="InternshipNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("InternshipCategoryId").ToString() %>' CommandName="ViewInternship"><%# Eval("Name") %></asp:LinkButton></strong></td>
					<td width="40%"><%# Eval("NumberAvailable", "{0:#,#}")%></td>
				</tr>
	</ItemTemplate>
</asp:ListView>
<asp:ObjectDataSource ID="InternshipListViewODS" runat="server" 
	TypeName=" Focus.CareerExplorer.WebExplorer.Controls.InternshipList" EnablePaging="False"
	SelectMethod="GetInternshipReports" SortParameterName="orderBy" OnSelecting="InternshipListViewODS_Selecting">
	<SelectParameters>
	  <asp:Parameter Name="internshipListmode" DbType="Object"/>
		<asp:Parameter Name="reportCriteria" DbType="Object" />
		<asp:Parameter Name="listSize" DbType="Int32" />
	</SelectParameters>
</asp:ObjectDataSource>
	</ContentTemplate>
</asp:UpdatePanel>