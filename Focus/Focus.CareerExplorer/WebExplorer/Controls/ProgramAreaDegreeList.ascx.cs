﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class ProgramAreaDegreeList : UserControlBase
	{
    public ExplorerCriteria ReportCriteria { get; set; }
    public int ListSize { get; set; }

    public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
    public event ItemClickedHandler ItemClicked;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Binds the list to the data source
    /// </summary>
    public void BindList()
		{
      DegreeEducationLevelRepeater.DataSource = GetDegrees();
      DegreeEducationLevelRepeater.DataBind();
    }

    /// <summary>
    /// Gets the degrees.
    /// </summary>
    /// <returns></returns>
    public List<ProgramAreaDegreeEducationLevelViewDto> GetDegrees()
    {
      return ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(ReportCriteria.ProgramAreaId, ReportCriteria)
                                    .OrderBy(del => del.DegreeName).ThenBy(del => del.EducationLevel).ToList();
    }

    /// <summary>
    /// Fired when an item is bound to the repeater control for degrees
    /// </summary>
    /// <param name="sender">Repeater control</param>
    /// <param name="e">Event arguments for the item being bound</param>
    protected void DegreeEducationLevelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var degreeEducationLevel = (ProgramAreaDegreeEducationLevelViewDto)e.Item.DataItem;
        var degreeEducationLevelLinkButton = (LinkButton)e.Item.FindControl("DegreeEducationLevelLinkButton");

        degreeEducationLevelLinkButton.CommandArgument = degreeEducationLevel.DegreeEducationLevelId.ToString();
        degreeEducationLevelLinkButton.Text = degreeEducationLevel.DegreeEducationLevelName;
      }
    }

    /// <summary>
    /// Fired when an item link is clicked
    /// </summary>
    /// <param name="sender">Link being clicked</param>
    /// <param name="e">Standard event arguments</param>
    protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
    {
      if (e.CommandName == "ViewDegreeCertification")
        OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
    }

    /// <summary>
    /// Raises an event if the an item has been clicked
    /// </summary>
    /// <param name="eventArgs">Arguments for the event</param>
    protected virtual void OnItemClicked(CommandEventArgs eventArgs)
    {
      if (ItemClicked != null)
        ItemClicked(this, eventArgs);
    }
  }
}