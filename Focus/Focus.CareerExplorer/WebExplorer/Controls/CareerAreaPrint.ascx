﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CareerAreaPrint.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.CareerAreaPrint" %>
<%@ Register src="Criteria.ascx" tagName="CriteriaFilter" tagPrefix="uc" %>

<div class="staticAccordion">
	
  <table class="lightboxHeading">
    <tr>
      <td><h1><asp:Label ID="CareerAreaNameLabel" runat="server" /> <%= HtmlLocalise("CareerArea.Label", "CareerArea")%></h1></td>
    </tr>
    <tr>
      <td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" HideCloseIcons="True" /></td>
    </tr>
  </table>
  
  <table width="100%" class="numberedAccordion" role='presentation' >
    <tr class="accordionTitle">
      <td valign="top" width="10" class="column1">1</td>
      <td class="column2">
        <%=HtmlLocalise("HiringDemand.Header", "Hiring demand over the last 12 months")%>
      </td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
      <td></td>
      <td>
			  <table width="100%" class="genericTable withHeader" role='presentation'>
				  <tr>
					  <td class="tableHead">
						  <%= HtmlLocalise("Job.ColumnTitle", "JOB")%>
					  </td>
					  <td class="tableHead">
						  <%= HtmlLocalise("HiringDemand.ColumnTitle", "HIRING DEMAND")%>
					  </td>
					  <td class="tableHead">
						  <%= HtmlLocalise("Salary.ColumnTitle", "SALARY")%>
					  </td>
				  </tr>
				  <asp:Repeater ID="JobRepeater" runat="server" OnItemDataBound="JobRepeater_ItemDataBound">
					  <ItemTemplate>
						  <tr>
							  <td width="55%"><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<%# Eval("ReportData.Name")%></strong> <asp:Image runat="server" ID="MortarIcon" Visible="False" AlternateText="Mortar Icon"/><asp:Image runat="server" ID="StarterJobIcon" Visible="False" AlternateText="Starter Job Icon"/></td>
							  <td width="25%"><%# Eval("ReportData.HiringTrend")%></td>
							  <td width="20%"><asp:Image ID="SalaryImage" runat="server" AlternateText="Salary Image"/></td>
						  </tr>
					  </ItemTemplate>
				  </asp:Repeater>
			  </table>
      </td>
      <td></td>
    </tr>
    <tr class="accordionTitle">
      <td class="column1">2</td>
      <td class="column2"><%= HtmlLocalise("TopSkills.Header", "Required skills: specialized, software, foundation")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
			<td></td>
			<td>
        <!-- skill list -->
			  <table width="100%" class="genericTable withHeader" role='presentation'>
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="SpecializedSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SpecializedSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="SpecializedSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SpecializedSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			  <table width="100%" class="genericTable withHeader" role='presentation'>
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="SoftwareSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="SoftwareSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="SoftwareSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="SoftwareSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			  <table width="100%" class="genericTable withHeader" role='presentation'>
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="FoundationSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="FoundationSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="FoundationSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="FoundationSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			</td>
			<td></td>
    </tr>
  </table>
</div>
