<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DegreeCertificationPrint.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationPrint" %>
<%@ Register src="Criteria.ascx" tagName="CriteriaFilter" tagPrefix="uc" %>

<div class="staticAccordion">

  <table class="lightboxHeading">
    <tr>
      <td><h1><asp:Label ID="DegreeCertificationNameLabel" runat="server" /></h1></td>
    </tr>
    <tr>
      <td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" HideCloseIcons="True" /></td>
    </tr>
  </table>

  <table>
    <tr>
      <td><asp:Label ID="DegreeCertificationBlurbLabel" runat="server" /><asp:Label ID="AverageSalaryLabel" runat="server" />
      </td>
    </tr>
  </table>

  <table width="100%" class="numberedAccordion">
    <tr class="accordionTitle">
      <td width="10" class="column1">1</td>
      <td class="column2"><%= HtmlLocalise("Global.HiringDemand.Header", "Hiring demand for the last 12 months")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
      <td></td>
      <td>
        <asp:Literal runat="server" ID="TieredDegreeJobListIntroLiteral"></asp:Literal>
        <!-- job list -->
		<table width="100%" class="genericTable withHeader">
			<tr>
				<td class="tableHead">
					<%= HtmlLocalise("Job.ColumnTitle", "JOB")%>
				</td>
				<td class="tableHead">
					<%= HtmlLocalise("HiringDemand.ColumnTitle", "HIRING DEMAND")%>
				</td>
				<td class="tableHead">
					<%= HtmlLocalise("Salary.ColumnTitle", "SALARY")%>
				</td>
			</tr>
			<asp:Repeater ID="JobRepeater" runat="server" OnItemDataBound="JobRepeater_ItemDataBound">
				<ItemTemplate>
					<tr>
						<td width="53%"><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<%# Eval("ReportData.Name") %></strong> <asp:Image runat="server" ID="MortarIcon" Visible="False" AlternateText="Mortar Icon"/> <asp:Image runat="server" ID="StarterJobIcon" Visible="False" AlternateText="Starter Job Icon"/></td>
						<td width="30%"><%# Eval("ReportData.HiringTrend")%></td>
						<td width="17%"><asp:Image ID="SalaryImage" runat="server" AlternateText="Salary Image" /></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
      </td>
      <td></td>
    </tr>
    <tr id="TieredJobsListRow" runat="server">
      <td></td>
      <td>
          
        <asp:Literal runat="server" ID="Tier2DegreeJobListIntroLiteral"></asp:Literal>
        <!-- job list -->
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead">
						  <%= HtmlLocalise("Job.ColumnTitle", "JOB")%>
					  </td>
					  <td class="tableHead">
						  <%= HtmlLocalise("HiringDemand.ColumnTitle", "HIRING DEMAND")%>
					  </td>
					  <td class="tableHead">
						  <%= HtmlLocalise("Salary.ColumnTitle", "SALARY")%>
					  </td>
				  </tr>
				  <asp:Repeater ID="Tier2JobRepeater" runat="server" OnItemDataBound="JobRepeater_ItemDataBound">
					  <ItemTemplate>
						  <tr>
							  <td width="53%"><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<%# Eval("ReportData.Name") %></strong> <asp:Image runat="server" ID="MortarIcon" Visible="False" AlternateText="Mortar Icon"/> <asp:Image runat="server" ID="StarterJobIcon" Visible="False" AlternateText="Starter Job Icon"/></td>
							  <td width="30%"><%# Eval("ReportData.HiringTrend")%></td>
							  <td width="17%"><asp:Image ID="SalaryImage" runat="server" AlternateText="Salary Image" /></td>
						  </tr>
					  </ItemTemplate>
				  </asp:Repeater>
			  </table>
      </td>
      <td></td>
    </tr>
    <tr class="accordionTitle">
      <td class="column1">2</td>
      <td class="column2"><%= HtmlLocalise("TopSkills.Header", "In-demand skills")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
		  <td></td>
		  <td>
        <!-- skill list -->
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="SpecializedSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SpecializedSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="SpecializedSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SpecializedSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="SoftwareSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SoftwareSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="SoftwareSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SoftwareSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="FoundationSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="FoundationSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="FoundationSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="FoundationSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
		  </td>
		  <td></td>
    </tr>
    <tr class="accordionTitle">
      <td class="column1">3</td>
      <td class="column2"><%= HtmlLocalise("TopEmployers.Header", "In-demand #BUSINESSES#:LOWER")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
 		  <td></td>
		  <td>
        <!-- employer list -->
			  <table width="100%" class="genericTable">
				  <tr>
					  <td align="left" valign="top">
						  <asp:Repeater ID="EmployerColumnOneRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
							  <HeaderTemplate>
								  <table width="100%">
							  </HeaderTemplate>
							  <ItemTemplate>
								  <tr>
									  <td valign="top">
										  <strong><asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp; <%# Eval("EmployerName") %>&nbsp; <asp:Literal ID="EmployerOpeningsLiteral" runat="server" /></strong>
									  </td>
								  </tr>
							  </ItemTemplate>
							  <FooterTemplate>
								  </table>
							  </FooterTemplate>
						  </asp:Repeater>
					  </td>
					  <asp:PlaceHolder ID="EmployerColumnTwoPlaceHolder" runat="server">
					  <td width="50%" align="left" valign="top">
						  <asp:Repeater ID="EmployerColumnTwoRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
							  <HeaderTemplate>
								  <table width="100%">
							  </HeaderTemplate>
							  <ItemTemplate>
								  <tr>
									  <td valign="top">
										  <strong><asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp; <%# Eval("EmployerName") %>&nbsp; <asp:Literal ID="EmployerOpeningsLiteral" runat="server" /></strong>
									  </td>
								  </tr>
							  </ItemTemplate>
							  <FooterTemplate>
								  </table>
							  </FooterTemplate>
						  </asp:Repeater>
					  </td>
					  </asp:PlaceHolder>
				  </tr>
			  </table>
		  </td>
		  <td></td>
    </tr>
		<asp:PlaceHolder ID="Section4PlaceHolder" runat="server">
			<tr class="accordionTitle">
				<td class="column1">4</td>
				<td class="column2"><%= HtmlLocalise("WhereCanIStudyThis.Header", "Where can I study this?")%></td>
				<td class="column3" align="right" valign="top"></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<table width="100%" class="genericTable">
						<tr>
							<td>
								<ul>
									<asp:Repeater ID="DegreeCertificationStudyPlaceRepeater" runat="server">
										<ItemTemplate>
											<li><%# Eval("StudyPlaceName") %></li>
										</ItemTemplate>
									</asp:Repeater>
								</ul>
							</td>
						</tr>
					</table>
				</td>
				<td></td>
			</tr>
		</asp:PlaceHolder>
  </table>

</div>