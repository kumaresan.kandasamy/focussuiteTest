﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DegreesBySchoolList.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.DegreesBySchoolList" %>

<asp:Panel ID="DegreeListPanel" runat="server">
    <asp:Repeater ID="ProgramAreaRepeater" runat="server" OnItemDataBound="ProgramAreaRepeater_ItemDataBound" OnItemCommand="DegreeEducationLevelRepeater_ItemCommand">
	    <ItemTemplate>
        <p>
            <asp:Literal runat="server" ID="ProgramAreaName"></asp:Literal>&nbsp;&nbsp;&nbsp;<asp:LinkButton runat="server" ID="ProgramAreaLink" CommandName="ViewProgramAreaDegree"></asp:LinkButton>
        </p>
        <p>
          <asp:UpdatePanel ID="DegreeEducationLevelUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional">
	          <ContentTemplate>
              <table width="100%" class="genericTable withHeader" role="presentation">
                <thead>
                  <tr>
                    <td class="tableHead" valign="top" width="70%">
                      <%= HtmlLocalise("DegreeList.Header.DegreesAndCertificates", "DEGREES/CERTIFICATE")%>
                      <asp:ImageButton runat="server" ID="DegreeEducationLevelSort" CommandName="SortASC" ImageUrl="<%# UrlBuilder.SortDown() %>" CommandArgument="ASC" OnCommand="DegreeEducationLevelSort_Click" />
                    </td>
                    <td class="tableHead" valign="top" width="30%">
                      <%= HtmlLocalise("DegreeList.Header.EducationLevel", "EDUCATION LEVEL")%>
                    </td>
                  </tr>
                </thead>
			          <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound" OnItemCommand="DegreeEducationLevelRepeater_ItemCommand">
				          <ItemTemplate>
				            <tr>
				              <td><asp:LinkButton runat="server" ID="DegreeEducationLevelLink" CommandName="ViewDegreeCertification"></asp:LinkButton></td>
                      <td><asp:Label runat="server" ID="DegreeEducationLevelLabel"></asp:Label></td>
				            </tr>
				          </ItemTemplate>
			          </asp:Repeater>
              </table>
            </ContentTemplate>
          </asp:UpdatePanel>
        </p>
	      <br />
	    </ItemTemplate>
    </asp:Repeater>
    <asp:UpdatePanel ID="OtherDegreeEducationLevelUpdatePanel" runat="server" ChildrenAsTriggers="True" UpdateMode="Conditional" Visible="False">
	    <ContentTemplate>
	      <p style="font-weight: bold;">
	        <asp:Literal runat="server" ID="OtherDegreeHeader"></asp:Literal>
	      </p>
        <table width="100%" class="genericTable withHeader">
          <thead>
            <tr>
              <td class="tableHead" valign="top" width="70%">
                <%= HtmlLocalise("DegreeList.Header.DegreesAndCertificates", "DEGREES/CERTIFICATE")%>
                <asp:ImageButton runat="server" ID="OtherDegreeEducationLevelSort" CommandName="SortASC" CommandArgument="ASC" OnCommand="DegreeEducationLevelSort_Click" />
              </td>
              <td class="tableHead" valign="top" width="30%">
                <%= HtmlLocalise("DegreeList.Header.EducationLevel", "EDUCATION LEVEL")%>
              </td>
            </tr>
          </thead>
			    <asp:Repeater ID="OtherDegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound" OnItemCommand="DegreeEducationLevelRepeater_ItemCommand">
				    <ItemTemplate>
				      <tr>
				        <td><asp:LinkButton runat="server" ID="DegreeEducationLevelLink" CommandName="ViewDegreeCertification"></asp:LinkButton></td>
                <td><asp:Label runat="server" ID="DegreeEducationLevelLabel"></asp:Label></td>
				      </tr>
				    </ItemTemplate>
			    </asp:Repeater>
        </table>
      </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
