﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Views;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class ProgramAreaDegreeStudyPrint : UserControlBase
	{
    private int _rank = 1;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Binds the controls on the page
    /// </summary>
    /// <param name="printModel">Details of the model being printed</param>
    public void BindControls(ExplorerProgramAreaDegreePrintModel printModel)
    {
      CriteriaFilter.Bind(ReportTypes.ProgramAreaDegree, printModel.ReportCriteria);

      ProgramAreaDegreeStudyNameLabel.Text = printModel.Name;
      ProgramAreaDescription.Text = printModel.Description;

      _rank = 1;
      JobRepeater.DataSource = printModel.Jobs;
      JobRepeater.DataBind();

      DegreeRepeater.DataSource = printModel.Degrees;
      DegreeRepeater.DataBind();
    }
    
    /// <summary>
    /// Fires when each Job is bound to the job repeater
    /// </summary>
    /// <param name="sender">Job Repeater control</param>
    /// <param name="e">Event argumenets for the item being bound</param>
    protected void JobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var jobReport = (ExplorerJobReportView)e.Item.DataItem;

        var rankLiteral = (Literal)e.Item.FindControl("RankLiteral");
        rankLiteral.Text = _rank.ToString(CultureInfo.InvariantCulture);
        _rank++;

        var salaryImage = (Image)e.Item.FindControl("SalaryImage");
        switch (jobReport.ReportData.SalaryTrend)
        {
          case "$$$":
            salaryImage.ImageUrl = UrlBuilder.JobSalaryHighImage();
            break;

          case "$$":
            salaryImage.ImageUrl = UrlBuilder.JobSalaryMediumImage();
            break;

          case "$":
            salaryImage.ImageUrl = UrlBuilder.JobSalaryLowImage();
            break;

          default:
            salaryImage.Visible = false;
            break;
        }
      }
    }
  }
}