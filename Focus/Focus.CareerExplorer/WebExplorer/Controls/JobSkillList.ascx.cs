#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class JobSkillList : UserControlBase
	{
		public ExplorerCriteria ReportCriteria { get; set; }
		public long JobId { get; set; }

		protected Boolean IsDisplaySkillGap;

		public Boolean DisplaySkillGap
		{
			get
			{
				return IsDisplaySkillGap;
			}
			set
			{
				IsDisplaySkillGap = value;
			}
		}

    public bool DisableLinks { get; set; }

    public Boolean ShowOtherFoundationSkills { get; set; }

		public List<JobSkillViewDto> Skills
		{
			get { return GetViewStateValue<List<JobSkillViewDto>>("JobSkills:Skills"); }
			set { SetViewStateValue("JobSkills:Skills", value); }
		}

		private int _rank = 1;

		public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
		public event ItemClickedHandler ItemClicked;

    /// <summary>
    /// Initializes a new instance of the <see cref="JobSkillList"/> class.
    /// </summary>
	  public JobSkillList()
	  {
	    IsDisplaySkillGap = (App.Settings.Theme == FocusThemes.Workforce);
	  }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
				BindControls();
			}
		}

    /// <summary>
    /// Handles the Command event of the SkillsLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void SkillsLinkButton_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "LoadDemandedSkills")
			{
				var skillType = (SkillTypes)Enum.Parse(typeof(SkillTypes), (string)e.CommandArgument);
				BindList(false, JobSkillTypes.Demanded, skillType);
			}
			else if (e.CommandName == "LoadHeldSkills")
			{
				BindList(false, JobSkillTypes.Held, SkillTypes.Specialized);
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the SkillsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void SkillsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var jobSkill = (JobSkillViewDto)e.Item.DataItem;

				var rankLiteral = (Literal)e.Item.FindControl("SkillRankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;
        
			  if (DisableLinks)
			  {
			    var skillNameLink = (LinkButton) e.Item.FindControl("SkillNameLinkButton");
			    skillNameLink.Visible = false;

          var skillNameLiteral = (Literal)e.Item.FindControl("SkillNameLinkDisabled");
			    skillNameLiteral.Visible = true;
			    skillNameLiteral.Text = jobSkill.SkillName;
			  }
			}
		}

    /// <summary>
    /// Handles the ItemCommand event of the SkillsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
		protected void SkillsRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "ViewSkill")
			{
				OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
			}
		}

    /// <summary>
    /// Raises the <see cref="E:ItemClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnItemClicked(CommandEventArgs eventArgs)
		{
			if (ItemClicked != null)
				ItemClicked(this, eventArgs);
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			SpecializedSkillsLinkButton.Text = CodeLocalise("SpecializedSkills.LinkLabel", "Specialized Skills");
			SoftwareSkillsLinkButton.Text = CodeLocalise("SoftwareSkills.LinkLabel", "Software Skills");
			FoundationSkillsLinkButton.Text = CodeLocalise("FoundationSkills.LinkLabel", "Foundation Skills");
			HeldSkillsLinkButton.Text = CodeLocalise("HeldSkills.LinkLabel", "Other Skills On Resumes Of Successful Candidates");

		  NoSkillsLabel.Text = CodeLocalise("NoSkills.Label", "No required skills have been advertised for this occupation in the last 12 months.");
      NoHeldSkillsLabel.Text = CodeLocalise("NoSkills.Label", "No other skills were on the resumes of successful candidates for this occupation in the last 12 months.");
		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
		private void BindControls()
		{
			SpecializedSkillsLinkButton.CommandArgument = SkillTypes.Specialized.ToString();
			SpecializedSkillsLinkButton.CommandName = "LoadDemandedSkills";
			SoftwareSkillsLinkButton.CommandArgument = SkillTypes.Software.ToString();
			SoftwareSkillsLinkButton.CommandName = "LoadDemandedSkills";
			FoundationSkillsLinkButton.CommandArgument = SkillTypes.Foundation.ToString();
			FoundationSkillsLinkButton.CommandName = "LoadDemandedSkills";
			HeldSkillsLinkButton.CommandName = "LoadHeldSkills";
		}

    /// <summary>
    /// Binds the list.
    /// </summary>
    /// <param name="forceReload">if set to <c>true</c> [force reload].</param>
    /// <param name="jobSkillType">Type of the job skill.</param>
    /// <param name="skillType">Type of the skill.</param>
		public void BindList(bool forceReload, JobSkillTypes jobSkillType, SkillTypes skillType)
		{
			if (forceReload)
				Skills = null;

			var skills = Skills;
			if (skills == null)
			{
				skills = ServiceClientLocator.ExplorerClient(App).GetJobSkills(JobId) ?? new List<JobSkillViewDto>();

			  Skills = skills;
			}

	    SkillTypeToolTip.ImageUrl = UrlBuilder.HelpIcon();

			switch (jobSkillType)
			{
				case JobSkillTypes.Demanded:
					
					DemandedSkillsPlaceHolder.Visible = true;
          HeldFoundationSkillHolder.Visible = HeldSkillsPlaceHolder.Visible = false;

					switch (skillType)
					{
						case SkillTypes.Specialized:
							SkillTypeLabel.Text = CodeLocalise("SpecializedSkills.Label", "SPECIALIZED SKILLS");
							SkillTypeToolTip.ToolTip = CodeLocalise("SpecializedSkills.TooltipText", "The technical skills that are specific to an occupation or occupational family (e.g., procurement, supply-chain management, etc.)");
              SpecializedSkillsLinkButton.Enabled = false;
							SoftwareSkillsLinkButton.Enabled = true;
							FoundationSkillsLinkButton.Enabled = true;
							HeldSkillsLinkButton.Enabled = true;
              break;

						case SkillTypes.Software:
							SkillTypeLabel.Text = CodeLocalise("SoftwareSkills.Label", "SOFTWARE SKILLS");
							SkillTypeToolTip.ToolTip = CodeLocalise("SoftwareSkills.TooltipText", "All technology-related skills, both basic and occupationally specific, that most #BUSINESSES#:LOWER require or consider in their hiring criteria.");
              SpecializedSkillsLinkButton.Enabled = true;
							SoftwareSkillsLinkButton.Enabled = false;
							FoundationSkillsLinkButton.Enabled = true;
							HeldSkillsLinkButton.Enabled = true;
              break;

						case SkillTypes.Foundation:
							SkillTypeLabel.Text = CodeLocalise("FoundationSkills.Label", "FOUNDATION SKILLS");
							SkillTypeToolTip.ToolTip = CodeLocalise("FoundationSkills.TooltipText", "The core or basic skills needed to perform tasks across a wide range of occupations (e.g., research, project management, written/oral communications, critical analysis, etc.)");
              SpecializedSkillsLinkButton.Enabled = true;
							SoftwareSkillsLinkButton.Enabled = true;
							FoundationSkillsLinkButton.Enabled = false;
							HeldSkillsLinkButton.Enabled = true;
              break;
					}

					var skillsByType = skills.Where(x => x.JobSkillType == JobSkillTypes.Demanded && x.SkillType == skillType).ToList();

					_rank = 1;
					SkillsColumnOneRepeater.DataSource = skillsByType.OrderByDescending(x => x.DemandPercentile).Take(10).ToList();
					SkillsColumnOneRepeater.DataBind();

					SkillsColumnOnePlaceHolder.Visible = true;
					NoSkillsLabel.Visible = false;

					var skillsByTypeCount = skillsByType.Count;

          if (skillsByTypeCount > 10)
					{
						SkillTypeTableCell.ColSpan = 2;
						SkillsColumnTwoPlaceHolder.Visible = true;

						SkillsColumnTwoRepeater.DataSource = skillsByType.OrderByDescending(x => x.DemandPercentile).Skip(10).Take(10).ToList();
						SkillsColumnTwoRepeater.DataBind();
					}
					else
					{
						SkillsColumnTwoPlaceHolder.Visible = false;
            if (skillsByTypeCount == 0)
            {
              SkillsColumnOnePlaceHolder.Visible = false;
              NoSkillsLabel.Visible = true;
            }
					}
					break;
					
				case JobSkillTypes.Held:
					
					DemandedSkillsPlaceHolder.Visible = false;
					HeldSkillsPlaceHolder.Visible = true;
          HeldFoundationSkillHolder.Visible = ShowOtherFoundationSkills;

					SpecializedSkillsLinkButton.Enabled = true;
					SoftwareSkillsLinkButton.Enabled = true;
					FoundationSkillsLinkButton.Enabled = true;
					HeldSkillsLinkButton.Enabled = false;

					_rank = 1;
					var heldSpecializedSkills =
					  skills.Where(x => x.JobSkillType == JobSkillTypes.Held && x.SkillType == SkillTypes.Specialized).
					          OrderByDescending(x => x.DemandPercentile).Take(5).ToList();
          HeldSpecializedSkillsRepeater.DataSource = heldSpecializedSkills;
					HeldSpecializedSkillsRepeater.DataBind();

					_rank = 1;
          var heldSoftwareSkills =
            skills.Where(x => x.JobSkillType == JobSkillTypes.Held && x.SkillType == SkillTypes.Software).
              OrderByDescending(x => x.DemandPercentile).Take(5).ToList();
          HeldSoftwareSkillsRepeater.DataSource = heldSoftwareSkills;
					HeldSoftwareSkillsRepeater.DataBind();

					if (heldSoftwareSkills.Count == 0 && heldSoftwareSkills.Count == 0)
					{
					  NoHeldSkillsRow.Visible = true;
            HeldSkillsRow.Visible = false;
					}
					else
          {
            NoHeldSkillsRow.Visible = false;
            HeldSkillsRow.Visible = true;
          }

					if (ShowOtherFoundationSkills)
          {
            _rank = 1;
            HeldFoundationSkillsRepeater.DataSource =
              skills.Where(x => x.JobSkillType == JobSkillTypes.Held && x.SkillType == SkillTypes.Foundation).
                OrderByDescending(x => x.DemandPercentile).Take(5).ToList();
            HeldFoundationSkillsRepeater.DataBind();
          }

					break;
			}
		}
	}
}