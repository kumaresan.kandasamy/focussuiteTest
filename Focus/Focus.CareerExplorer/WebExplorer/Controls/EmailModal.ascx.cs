#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Framework.Core;

using Focus.Common.Authentication;
using Focus.Core.Criteria.Explorer;
using Focus.CareerExplorer.Code;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
    public partial class EmailModal : UserControlBase
    {
        public string MaxMessageLength
        {
            get { return App.Settings.ExplorerSendEmailMaximumMessageLength.ToString(); }
        }

        private ExplorerCriteria ReportCriteria
        {
            get { return GetViewStateValue<ExplorerCriteria>("EmailModal:ReportCriteria"); }
            set { SetViewStateValue("EmailModal:ReportCriteria", value); }
        }

        public delegate void CompletedHandler(object sender, EventArgs eventArgs);
        public event CompletedHandler Completed;

        public delegate void CancelledHandler(object sender, EventArgs eventArgs);
        public event CancelledHandler Cancelled;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EmailAddressesRegEx.ValidationExpression = @"(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*,\s*|\s*$))*";
                EmailAddressesRegEx.Enabled = !String.IsNullOrEmpty(EmailAddressesRegEx.ValidationExpression);

                LocaliseUI();
            }
        }

        /// <summary>
        /// Shows the email modal.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="reportItemId">The report item id.</param>
        /// <param name="lightboxName">Name of the lightbox.</param>
        /// <param name="lightboxLocation">The lightbox location.</param>
        public void Show(ExplorerCriteria criteria, long reportItemId, string lightboxName, string lightboxLocation)
        {
            NameLabel.Text = lightboxName;
            LocationLabel.Text = lightboxLocation;

            ReportCriteria = criteria;
            ReportCriteria.ReportItemId = reportItemId;

            EmailAddressesTextBox.Text = EmailBodyTextBox.Text = String.Empty;

            var defaultMessageText = CodeLocalise("SendEmailDefaultMessage.Text", String.Empty);
            if (!String.IsNullOrEmpty(defaultMessageText))
            {
                EmailBodyTextBox.Text = defaultMessageText;
            }
            EmailCharactersRemainingLabel.Text =
                (App.Settings.ExplorerSendEmailMaximumMessageLength - EmailBodyTextBox.Text.Length).ToString();

            ModalPopup.Show();
        }

        /// <summary>
        /// Handles the Click event of the SendEmailButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SendEmailButton_Click(object sender, EventArgs e)
        {
            ModalPopup.Hide();
            var userName = CodeLocalise("BookmarkEmail.DefaultNme", "A friend");

            // AIM - FVN-4603-Session Issue-Mail is sent even after logout in another tab
            // Use screen name to personalise the email
            if ((ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated)))
            {
                if (Context.User != null)
                {
                    var user = (UserPrincipal)Context.User;
                    userName = user.UserContext.ScreenName.IsNotNullOrEmpty() ? user.UserContext.ScreenName : userName;
                }


                var bookmarkUrl = UrlBuilder.ExplorerBookmarkLink("[BOOKMARKID]"); // String.Format("http://{0}{1}", Request.Url.Host, UrlBuilder.Explore("bookmark", "[BOOKMARKID]"));
                var emailSubject =
                  String.Format(CodeLocalise("SendEmailSubject.Text", "{0} thinks you might be interested in " + NameLabel.Text),
                                userName);

                ServiceClientLocator.ExplorerClient(App).SendEmail(ReportCriteria, EmailAddressesTextBox.Text, emailSubject, EmailBodyTextBox.Text, bookmarkUrl);

                OnCompleted(new EventArgs());
            }
        }

        /// <summary>
        /// Handles the Click event of the CancelButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CancelButton_Click(object sender, EventArgs e)
        {
            ModalPopup.Hide();

            OnCancelled(new EventArgs());
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            EmailAddressesRegEx.ErrorMessage = CodeLocalise("EmailAddresses.RegularExpresionErrorMessage", "Email addresses are in the wrong format");
            EmailAddressesRequired.ErrorMessage = CodeLocalise("EmailAddresses.RequiredErrorMessage", "Email addresses are required");

            var defaultMessageText = CodeLocalise("SendEmailDefaultMessage.Text", String.Empty);
            if (String.IsNullOrEmpty(defaultMessageText))
            {
                MessageTipLiteral.Text = String.Format("&nbsp; <i>({0})</i>", CodeLocalise("EnterMessage.Text", "enter your message below"));
            }
            else
            {
                MessageTipLiteral.Text = String.Format("&nbsp; <i>({0})</i>",
                                                       CodeLocalise("EnterMessageOrCustomise.Text",
                                                                    "enter your message below or customise the one provided"));
            }

            EmailBodyTextBoxRequired.ErrorMessage = CodeLocalise("EmailBody.RequiredErrorMessage", "Email body is required");
            EmailBodyTextBoxRegEx.ErrorMessage = String.Format(CodeLocalise("EmailBody.RegExMessage", "Email body maximum {0} characters"), App.Settings.ExplorerSendEmailMaximumMessageLength.ToString());
            EmailBodyTextBoxRegEx.ValidationExpression = String.Format("^[\\s\\S]{{0,{0}}}$", App.Settings.ExplorerSendEmailMaximumMessageLength.ToString());

            CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
            SendEmailButton.Text = CodeLocalise("SendEmail.Text", "Send Email");
        }

        /// <summary>
        /// Raises the <see cref="E:Completed"/> event.
        /// </summary>
        /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void OnCompleted(EventArgs eventArgs)
        {
            if (Completed != null)
                Completed(this, eventArgs);
        }

        /// <summary>
        /// Raises the <see cref="E:Cancelled"/> event.
        /// </summary>
        /// <param name="eventArgs">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected virtual void OnCancelled(EventArgs eventArgs)
        {
            if (Cancelled != null)
                Cancelled(this, eventArgs);
        }
    }
}