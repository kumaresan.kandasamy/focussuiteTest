#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class HelpModal : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LocaliseUI();
			}

			if (ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				YourEmailTextBox.Text = App.User.EmailAddress;
			}
		}

    /// <summary>
    /// Handles the Click event of the SendEmailButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendEmailButton_Click(object sender, EventArgs e)
		{
			ModalPopup.Hide();

			var emailSubject = CodeLocalise("HelpEmailSubject.Text", "#FOCUSEXPLORER# Help Request");

      ServiceClientLocator.ExplorerClient(App).SendHelpEmail(emailSubject, YourEmailTextBox.Text + " sent this request: " + HelpMessageTextBox.Text);

			HelpConfirmModalPopup.Show();
		}

    /// <summary>
    /// Handles the Click event of the CancelButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelButton_Click(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

    /// <summary>
    /// Handles the Click event of the CancelConfirmButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CancelConfirmButton_Click(object sender, EventArgs e)
		{
			HelpConfirmModalPopup.Hide();
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			YourEmailTextBoxRequired.ErrorMessage = CodeLocalise("YourEmailTextBox.RequiredErrorMessage", "Your email is required");
			YourEmailTextBoxRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
			YourEmailTextBoxRegEx.ErrorMessage = CodeLocalise("YourEmailTextBox.RegExErrorMessage", "Email address is not the correct format");

			HelpMessageTextBoxRequired.ErrorMessage = CodeLocalise("EmailBody.RequiredErrorMessage", "Email body is required");

			CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			SendEmailButton.Text = CodeLocalise("SendEmail.Text", "Send Email");
			CancelConfirmButton.Text = CodeLocalise("Global.Close.Text", "Close");
		}
	}
}