#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.CareerExplorer.Code;
using Focus.Core.Views;
using Focus.Services.Core;
using Framework.Core;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class JobModal : ExplorerModalControl
	{
    public ExplorerJobReportView JobReport
		{
      get { return GetViewStateValue<ExplorerJobReportView>("JobModal:JobReport"); }
      set { SetViewStateValue("JobModal:JobReport", value); }
		}

		public ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue<ExplorerCriteria>("JobModal:ReportCriteria"); }
			set { SetViewStateValue("JobModal:ReportCriteria", value); }
		}

		public List<LightboxBreadcrumb> Breadcrumbs
		{
			get { return GetViewStateValue("JobModal:Breadcrumb", new List<LightboxBreadcrumb>()); }
			set { SetViewStateValue("JobModal:Breadcrumb", value); }
		}

		public JobDegreeCertificationModel DegreeCertifications
		{
			get { return GetViewStateValue<JobDegreeCertificationModel>("JobModal:DegreeCertifications"); }
			set { SetViewStateValue("JobModal:DegreeCertifications", value); }			
		}

    public List<ProgramAreaDegreeViewDto> ProgramAreaDegrees
    {
      get { return GetViewStateValue<List<ProgramAreaDegreeViewDto>>("JobModal:ProgramAreaDegrees"); }
      set { SetViewStateValue("JobModal:ProgramAreaDegrees", value); }
    }

		public CareerPathDirections CareerPathDirection
		{
			get { return GetViewStateValue<CareerPathDirections>("JobModal:CareerPathDirection"); }
			set { SetViewStateValue("JobModal:CareerPathDirection", value); }				
		}

		public JobCareerPathModel CareerPaths
		{
			get { return GetViewStateValue<JobCareerPathModel>("JobModal:CareerPaths"); }
			set { SetViewStateValue("JobModal:CareerPaths", value); }	
		}

    public StateAreaViewDto StateArea
    {
      get { return GetViewStateValue<StateAreaViewDto>("JobModal:StateArea"); }
      set { SetViewStateValue("JobModal:StateArea", value); }
    }

    public List<OnetDetailsView> OnetDetails
    {
      get { return GetViewStateValue<List<OnetDetailsView>>("JobModal:OnetDetails"); }
      set { SetViewStateValue("JobModal:OnetDetails", value); }
    }

    public struct ProgramArea
    {
      public long Id;
      public string Name;
      public int Tier;

      public long ProgramAreaId { get { return Id; } }
      public string ProgramAreaName { get { return Name; } }
      public int ProgramAreaTier { get { return Tier; } }
    }

	  /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack)
				return;

			LocaliseUI();
			BindControls();

      ReadyToMakeAMovePlaceHolder.Visible = ViewCurrentPostingsLinkButton.Visible = (App.Settings.Module != FocusModules.Explorer);
		}

    /// <summary>
    /// Shows the spec`fied job id.
    /// </summary>
    /// <param name="jobId">The job id.</param>
    /// <param name="criteria">The criteria.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="printFormat">The format to print the modal</param>
    public void Show(long jobId, ExplorerCriteria criteria, List<LightboxBreadcrumb> breadcrumbs, ModalPrintFormat printFormat = ModalPrintFormat.None)
		{
		  criteria.CareerAreaJobId = jobId;
			var jobReport = ServiceClientLocator.ExplorerClient(App).GetJobReport(criteria);
      
			JobReport = jobReport;
      criteria.CareerAreaJobId = jobReport.ReportData.JobId;
      criteria.CareerAreaJob = jobReport.ReportData.Name;
      ReportCriteria = criteria;
      ReportCriteria.ReportType = ReportTypes.Job;
			Breadcrumbs = breadcrumbs;
      CriteriaFilter.Bind(ReportTypes.Job, criteria);

			StateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(criteria.StateAreaId);

			ModalHeaderOptions.BindBreadcrumbs(Breadcrumbs);
			//if (breadcrumbs != null && breadcrumbs.Count == 3)
			//  Page.ClientScript.RegisterStartupScript(this.GetType(), "DisableLightboxModalLinks",
			//                                          String.Format("DisableLightboxModalLinks('jobLightBox','{0}');",
			//                                                        CodeLocalise("Modal.BreadcrumbLimitMessage",
			//                                                                     "The application only allows for three sets of data to be open at any one time.")),
			//                                          true);

      JobNameLabel.Text = jobReport.ReportData.Name;
     
      if (App.Settings.Theme == FocusThemes.Education && jobReport.RelatedDegreeAvailableAtCurrentSchool)
      {
        DegreeProgramPanel.Visible = true;
      }
      else
        DegreeProgramPanel.Visible = false;

      if (jobReport.ReportData.StarterJob.HasValue)
      {
        StarterJobPanel.Visible = true;

        if (jobReport.ReportData.StarterJob.Value == StarterJobLevel.Many)
        {
          StarterJobToolTipDiv.Attributes.Add("class", "tooltipWithArrow starterJobMany");
          StarterJobToolTipImage.Attributes["title"] = CodeLocalise("StarterJobMany.TooltipText", "This occupation has substantial openings for new entrants with less than 2 years of work experience in this field.");
        }
        else if (jobReport.ReportData.StarterJob.Value == StarterJobLevel.Some)
        {
          StarterJobToolTipDiv.Attributes.Add("class", "tooltipWithArrow starterJobSome");
          StarterJobToolTipImage.Attributes["title"] = CodeLocalise("StarterJobSome.TooltipText", "This occupation has substantial openings for new entrants with less than 3 years of work experience in this field.");
        }
      }
      else
        StarterJobPanel.Visible = false;

		  var jobTitles = ServiceClientLocator.ExplorerClient(App).GetJobJobTitles(jobReport.ReportData.JobId);
			if (jobTitles != null && jobTitles.Count > 0)
			{
				JobTitlesLabel.Text = jobTitles.Select(x => x.Name).Aggregate((x, y) => x + ", " + y);
			}
      var jobIntroduction = ServiceClientLocator.ExplorerClient(App).GetJobIntroduction(jobReport.ReportData.JobId);
      if (jobIntroduction.IsNotNullOrEmpty()) JobIntroductionLabel.Text = jobIntroduction;

			#region salary and hiring trends

			HiringTrendLiteral.Text = jobReport.ReportData.HiringTrend;

			string arrowPositionClass;
      switch (jobReport.ReportData.HiringTrend)
			{
				case "Very High":
					HiringDemandImage.ImageUrl = UrlBuilder.JobHiringTrendVeryHighImage();
          arrowPositionClass = jobReport.ReportData.HiringTrendSubLevel.ToLower() == "low" ? "vhighLow" : "vhighHigh";
					break;
				case "High":
					HiringDemandImage.ImageUrl = UrlBuilder.JobHiringTrendHighImage();
          arrowPositionClass = jobReport.ReportData.HiringTrendSubLevel.ToLower() == "low" ? "highLow" : "highHigh";
					break;
				case "Medium":
					HiringDemandImage.ImageUrl = UrlBuilder.JobHiringTrendMediumImage();
          arrowPositionClass = jobReport.ReportData.HiringTrendSubLevel.ToLower() == "low" ? "medLow" : "medHigh";
					break;
				case "Low":
					HiringDemandImage.ImageUrl = UrlBuilder.JobHiringTrendLowImage();
          arrowPositionClass = jobReport.ReportData.HiringTrendSubLevel.ToLower() == "low" ? "lowLow" : "lowHigh";
					break;
				default:
					HiringDemandImage.Visible = false;
					arrowPositionClass = "lowLow";
					break;
			}

			HiringTrendArrowSpanLiteral.Text = String.Format("<span class=\"arrow {0}\"></span>", arrowPositionClass);

			HiringDemandLiteral.Text = CodeLocalise("HiringDemandPositions.Text",
																							"{0} positions",
                                              jobReport.ReportData.HiringDemand.ToString("#,#"));

			SalaryRangeLiteral.Text = CodeLocalise("SalaryRangeNotAvailable.Text", "Not available");
      if (jobReport.ReportData.SalaryTrend != "NA")
			{
        if (jobReport.ReportData.SalaryTrendMin > 0 && jobReport.ReportData.SalaryTrendMax > 0 && jobReport.ReportData.SalaryTrendMax <= 200000)
				{
					SalaryRangeLiteral.Text = CodeLocalise("SalaryRange.Text", "{0} - {1}/yr",
                                                 jobReport.ReportData.SalaryTrendMin.ToString("$#,#"),
                                                 jobReport.ReportData.SalaryTrendMax.ToString("$#,#"));
				}
        else if (jobReport.ReportData.SalaryTrendMin > 0 && jobReport.ReportData.SalaryTrendMax > 200000)
				{
					// upper salary capped
					var salaryTrendMax = 200000;
					SalaryRangeLiteral.Text = CodeLocalise("SalaryRangeCapped.Text", "{0} - {1}+/yr",
                                                 jobReport.ReportData.SalaryTrendMin.ToString("$#,#"),
																								 salaryTrendMax.ToString("$#,#"));
					
				}
			}

			SalaryRangeImage.Visible = true;
      switch (jobReport.ReportData.SalaryTrend)
			{
				case "$$$":
					SalaryRangeImage.ImageUrl = UrlBuilder.JobSalaryRangeHighImage();
					break;
				case "$$":
					SalaryRangeImage.ImageUrl = UrlBuilder.JobSalaryRangeMediumImage();
					break;
				case "$":
					SalaryRangeImage.ImageUrl = UrlBuilder.JobSalaryRangeLowImage();
					break;
				default:
					SalaryRangeImage.Visible = false;
					break;
			}

      if ((App.Settings.ExplorerJobSalaryNationwideDisplayType == "Average" && jobReport.ReportData.SalaryTrendRealtimeAverage.HasValue)
        || (App.Settings.ExplorerJobSalaryNationwideDisplayType == "SalaryRange" && jobReport.ReportData.SalaryTrendRealtimeMin.HasValue && jobReport.ReportData.SalaryTrendRealtimeMax.HasValue))
			{
				SalaryNationwidePlaceHolder.Visible = true;
				if (App.Settings.ExplorerJobSalaryNationwideDisplayType == "Average")
				{
					SalaryNationwideLabelLiteral.Text = CodeLocalise("HiringDemandAverageSalaryNationwide.Text",
                                                           "Average salary advertised in recent job postings nationwide");
					SalaryNationwideLiteral.Text = CodeLocalise("AverageSalary.Text", "{0}/yr",
                                                      jobReport.ReportData.SalaryTrendRealtimeAverage.Value.ToString("$#,#"));
				}
				else
				{
					SalaryNationwideLabelLiteral.Text = CodeLocalise("HiringDemandSalaryRangeNationwide.Text",
																													 "Salary range advertised by #BUSINESSES#:LOWER in job postings nationwide");
					SalaryNationwideLiteral.Text = CodeLocalise("SalaryRange.Text", "{0} - {1}/yr",
                                                      jobReport.ReportData.SalaryTrendRealtimeMin.Value.ToString("$#,#"),
                                                      jobReport.ReportData.SalaryTrendRealtimeMax.Value.ToString("$#,#"));
				}

        switch (jobReport.ReportData.SalaryTrendRealtime)
				{
					case "$$$":
						SalaryNationwideImage.ImageUrl = UrlBuilder.JobSalaryRangeHighImage();
						break;
					case "$$":
						SalaryNationwideImage.ImageUrl = UrlBuilder.JobSalaryRangeMediumImage();
						break;
					case "$":
						SalaryNationwideImage.ImageUrl = UrlBuilder.JobSalaryRangeLowImage();
						break;
				}
			}
			else
			{
				SalaryNationwidePlaceHolder.Visible = false;
			}

			#endregion

			JobEmployerList.ReportCriteria = criteria;
      JobEmployerList.JobId = jobReport.ReportData.JobId;
			JobEmployerList.BindList();
      if (JobEmployerList.Employers.Count > 0)
        TopEmployersIntroLabel.Text = CodeLocalise("TopEmployersIntro.Text",
																									 "Top #BUSINESSES#:LOWER who have posted at least 10 openings for this position over the last 12 months:");
      else
        TopEmployersIntroLabel.Text = CodeLocalise("TopEmployersIntroNoResults.Text",
																									 "While there may have been numerous job openings for this occupation in recent months, no #BUSINESS#:LOWER has advertised 10 or more openings in your area.");

      SkillList.JobId = jobReport.ReportData.JobId;
			SkillList.BindList(true, JobSkillTypes.Demanded, SkillTypes.Specialized);

			#region degrees and certifications

      DegreeIntroLabel.Text = jobReport.ReportData.DegreeIntroStatement;

			var degreeCertifications = ServiceClientLocator.ExplorerClient(App).GetJobDegreeCertifications(ReportCriteria);
			DegreeCertifications = degreeCertifications;

			DegreePanel.Visible = true;
			CertificationPanel.Visible = true;
      
			if (degreeCertifications != null)
      {
        ProgramAreaTier1RepeaterNoneFoundLiteral.Visible =
          ProgramAreaTier2RepeaterNoneFoundLiteral.Visible =
          DegreeTier1RepeaterNoneFoundLiteral.Visible = DegreeTier2RepeaterNoneFoundLiteral.Visible = false;

        ProgramAreaTier1RepeaterNoneFoundLiteral.Text =
          ProgramAreaTier2RepeaterNoneFoundLiteral.Text =
          DegreeTier1RepeaterNoneFoundLiteral.Text = DegreeTier2RepeaterNoneFoundLiteral.Text = CodeLocalise("NoneFound.Text", "None Found");

        // Set basic tiering visibility
        TierLinksPanel.Visible = App.Settings.ExplorerUseDegreeTiering;

        if (degreeCertifications.JobDegreeEducationLevels.Any(x => x.IsClientData))
        {
          ClientDegreeDisclaimerLiteral.Visible = true;
          ClientDegreeDisclaimerLiteral.Text = CodeLocalise("ClientDegreeDisclaimer.Text", string.Empty);
        }
        else
        {
          ClientDegreeDisclaimerLiteral.Visible = false;
        }

        // Set basic labels
        if (App.Settings.ExplorerUseDegreeTiering)
        {
          SpecificProgramsOfStudyLink.Text = CodeLocalise("SpecificProgramsOfStudyLink.Text", "Programs of study specific to this occupation");
          RelatedProgramsOfStudyLink.Text = CodeLocalise("RelatedProgramsOfStudyLink.Text", "Other related programs of study");
          SpecificProgramsOfStudyLink.Enabled = false;
          RelatedProgramsOfStudyLink.Enabled = true;
        }

        if (App.Settings.Theme == FocusThemes.Education && degreeCertifications.JobDegrees.Any(x => x.IsClientData))
        {
          if (ProgramAreaDegrees.IsNullOrEmpty())
            ProgramAreaDegrees = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegrees();

          DegreePlaceHolder.Visible = false;
          ProgramAreaPlaceHolder.Visible = true;

          if (App.Settings.ExplorerUseDegreeTiering)
          {
            ProgramAreaTier2PlaceHolder.Visible = false;
            ProgramAreaTier1PlaceHolder.Visible = true;

            ProgramAreaTier1Title.Text =
              CodeLocalise("ClientProgramAreaDegreesSpecific.Header",
                           "#SCHOOLNAME# PROGRAMS OF STUDY SPECIFIC TO THIS OCCUPATION").ToUpper();
            ProgramAreaTier2Title.Text =
              CodeLocalise("ClientProgramAreaDegreesRelated.Header",
                           "#SCHOOLNAME# PROGRAMS OF STUDY RELATED TO THIS OCCUPATION").ToUpper();

            var degreeIds =
              degreeCertifications.JobDegreeEducationLevels.Where(x => x.Tier == 1).Select(jd => jd.DegreeId).Distinct();
            ProgramAreaTier1Repeater.DataSource =
              ProgramAreaDegrees.Where(pad => degreeIds.Contains(pad.DegreeId)).Select(
                pa => new ProgramArea {Id = pa.ProgramAreaId, Name = pa.ProgramAreaName, Tier = 1}).Distinct().ToList();
            ProgramAreaTier1Repeater.DataBind();

            ProgramAreaTier1RepeaterNoneFoundLiteral.Visible = (ProgramAreaTier1Repeater.Items.Count == 0);

            degreeIds =
              degreeCertifications.JobDegreeEducationLevels.Where(x => x.Tier == 2).Select(jd => jd.DegreeId).Distinct();
            ProgramAreaTier2Repeater.DataSource =
              ProgramAreaDegrees.Where(pad => degreeIds.Contains(pad.DegreeId)).Select(
                pa => new ProgramArea {Id = pa.ProgramAreaId, Name = pa.ProgramAreaName, Tier = 2}).Distinct().ToList();
            ProgramAreaTier2Repeater.DataBind();

            ProgramAreaTier2RepeaterNoneFoundLiteral.Visible = (ProgramAreaTier2Repeater.Items.Count == 0);

            // If nothing in tier 1 but ther is in tier 2 show tier 2 by default
            if (ProgramAreaTier1Repeater.Items.Count == 0 && ProgramAreaTier2Repeater.Items.Count != 0)
            {
              SpecificProgramsOfStudyLink.Enabled = true;
              RelatedProgramsOfStudyLink.Enabled = false;
              ProgramAreaTier1PlaceHolder.Visible = false;
              ProgramAreaTier2PlaceHolder.Visible = true;
            }
          }
          else
          {
            ProgramAreaTier2PlaceHolder.Visible = false;
            ProgramAreaTier1PlaceHolder.Visible = true;

            ProgramAreaTier1Title.Text =
              CodeLocalise("ClientProgramAreaDegrees.Header", "#SCHOOLNAME# PROGRAMS OF STUDY").ToUpper();
            var degreeIds =
              degreeCertifications.JobDegreeEducationLevels.Where(x => x.Tier == 0).Select(jd => jd.DegreeId).Distinct();
            ProgramAreaTier1Repeater.DataSource =
              ProgramAreaDegrees.Where(pad => degreeIds.Contains(pad.DegreeId)).Select(
                pa => new ProgramArea {Id = pa.ProgramAreaId, Name = pa.ProgramAreaName, Tier = 0}).Distinct().ToList();
            ProgramAreaTier1Repeater.DataBind();

            ProgramAreaTier1RepeaterNoneFoundLiteral.Visible = (ProgramAreaTier1Repeater.Items.Count == 0);
          }
        }
        else
        {
          ProgramAreaPlaceHolder.Visible = false;
          DegreePlaceHolder.Visible = true;
          if (App.Settings.ExplorerUseDegreeTiering)
          {
            DegreeTier1PlaceHolder.Visible = true;
            DegreeTier2PlaceHolder.Visible = false;

            DegreeTier1Title.Text = CodeLocalise("DegreesSpecific.Header",
                           "PROGRAMS OF STUDY SPECIFIC TO THIS OCCUPATION").ToUpper();
            DegreeTier2Title.Text = CodeLocalise("DegreesRelated.Header",
                           "PROGRAMS OF STUDY RELATED TO THIS OCCUPATION").ToUpper();

            DegreeTier1Repeater.DataSource = degreeCertifications.JobDegrees.Where(x => x.Tier == 1).ToList();
            DegreeTier1Repeater.DataBind();
            DegreeTier2Repeater.DataSource = degreeCertifications.JobDegrees.Where(x => x.Tier == 2).ToList();
            DegreeTier2Repeater.DataBind();

            DegreeTier1RepeaterNoneFoundLiteral.Visible = (DegreeTier1Repeater.Items.Count == 0);
            DegreeTier2RepeaterNoneFoundLiteral.Visible = (DegreeTier2Repeater.Items.Count == 0);

            // If nothing in tier 1 but ther is in tier 2 show tier 2 by default
            if (DegreeTier1Repeater.Items.Count == 0 && DegreeTier2Repeater.Items.Count != 0)
            {
              DegreeTier1PlaceHolder.Visible = false;
              DegreeTier2PlaceHolder.Visible = true;
              SpecificProgramsOfStudyLink.Enabled = true;
              RelatedProgramsOfStudyLink.Enabled = false;
            }
          }
          else
          {
            DegreeTier1PlaceHolder.Visible = true;
            DegreeTier2PlaceHolder.Visible = false;

            DegreeTier2PlaceHolder.Visible = false;
            DegreeTier1Title.Text = CodeLocalise("Degrees.Header",
                           "DEGREES").ToUpper();
            DegreeTier1Repeater.DataSource = degreeCertifications.JobDegrees;
            DegreeTier1Repeater.DataBind();

            DegreeTier1RepeaterNoneFoundLiteral.Visible = (DegreeTier1Repeater.Items.Count == 0);
          }
        }
        
				if (degreeCertifications.JobCertifications.Count > 0)
				{
					CertificationRepeater.DataSource =
						degreeCertifications.JobCertifications.OrderByDescending(
							x => x.DemandPercentile).Take(5).ToList();
					CertificationRepeater.DataBind();
				}
				else
				{
					CertificationPanel.Visible = false;
				}
			}
			else
			{
				DegreePanel.Visible = false;
				CertificationPanel.Visible = false;
			}

			#region education requirements

			EducationRequirementsPlaceHolder.Visible = true;

      var educationRequirements = ServiceClientLocator.ExplorerClient(App).GetJobEducationRequirements(jobReport.ReportData.JobId);
			if (educationRequirements.Count > 0)
			{
				var educationRequirement =
					educationRequirements.SingleOrDefault(
						x => x.EducationRequirementType == EducationRequirementTypes.HighSchoolTechnicalTraining);

				HighSchoolTechnicalTrainingLiteral.Text =
					String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
												educationRequirement == null
				              		? "0"
				              		: Math.Round(educationRequirement.RequirementPercentile, 0).ToString());

				educationRequirement =
					educationRequirements.SingleOrDefault(
						x => x.EducationRequirementType == EducationRequirementTypes.AssociatesDegree);

				AssociatesDegreeLiteral.Text =
					String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
												educationRequirement == null
													? "0"
													: Math.Round(educationRequirement.RequirementPercentile, 0).ToString());

				educationRequirement =
					educationRequirements.SingleOrDefault(
						x => x.EducationRequirementType == EducationRequirementTypes.BachelorsDegree);

				BachelorsDegreeLiteral.Text =
					String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
												educationRequirement == null
													? "0"
													: Math.Round(educationRequirement.RequirementPercentile, 0).ToString());

				educationRequirement =
					educationRequirements.SingleOrDefault(
						x => x.EducationRequirementType == EducationRequirementTypes.GraduateProfessionalDegree);

				GraduateProfessionalDegreeLiteral.Text =
					String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
												educationRequirement == null
													? "0"
													: Math.Round(educationRequirement.RequirementPercentile, 0).ToString());

			  if (App.Settings.Theme != FocusThemes.Education)
			    MinimumDegreeTooltip.Visible = false;
			}
			else
			{
				EducationRequirementsPlaceHolder.Visible = false;
			}

			#endregion

			#region experience levels

      var experienceLevels = ServiceClientLocator.ExplorerClient(App).GetJobExperienceLevels(jobReport.ReportData.JobId);

			var experienceLevel = experienceLevels.SingleOrDefault(x => x.ExperienceLevel == ExperienceLevels.LessThanTwoYears);

			LessThanTwoLiteral.Text =
				String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
											experienceLevel == null ? "0" : Math.Round(experienceLevel.ExperiencePercentile, 0).ToString());

			experienceLevel = experienceLevels.SingleOrDefault(x => x.ExperienceLevel == ExperienceLevels.TwoToFiveYears);

			TwoToFiveLiteral.Text =
				String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
											experienceLevel == null ? "0" : Math.Round(experienceLevel.ExperiencePercentile, 0).ToString());

			experienceLevel = experienceLevels.SingleOrDefault(x => x.ExperienceLevel == ExperienceLevels.FiveToEightYears);

			FiveToEightLiteral.Text =
				String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
											experienceLevel == null ? "0" : Math.Round(experienceLevel.ExperiencePercentile, 0).ToString());

			experienceLevel = experienceLevels.SingleOrDefault(x => x.ExperienceLevel == ExperienceLevels.EightPlusYears);

			EightPlusLiteral.Text =
				String.Format("<span class=\"bar\" style=\"width:{0}%\"></span><span class=\"percent\">{0}%</span>",
											experienceLevel == null ? "0" : Math.Round(experienceLevel.ExperiencePercentile, 0).ToString());

			#endregion

			#endregion

      SimilarJobList.JobId = jobReport.ReportData.JobId;
			SimilarJobList.ReportCriteria = criteria;
			SimilarJobList.ListSize = 50;
      SimilarJobList.EnablePaging = true;
			SimilarJobList.BindList(true);

			#region career paths

			CareerPathDirection = CareerPathDirections.ToThisJob;
      CareerPaths = ServiceClientLocator.ExplorerClient(App).GetJobCareerPaths(jobReport.ReportData.JobId);
			BindCareerPathSecondLevel();

			#endregion

      
    	ViewCurrentPostingsLinkButton.Enabled = jobReport.ReportData.ROnet.IsNotNullOrEmpty();

      if (App.Settings.Module != FocusModules.Explorer)
      {
        if (App.Settings.ExplorerReadyToMakeAMoveBubble)
        {
          ReadyToMakeAMovePlaceHolder.Visible = true;
          ViewCurrentPositionsLiteral.Text = HtmlLocalise("SeeCurrentPostingsForJob.Text", "See current postings for<br />this job �");
        }
        else
        {
          ReadyToMakeAMovePlaceHolder.Visible = false;
          ViewCurrentPositionsLiteral.Text = HtmlLocalise("ReadySeeCurrentPostingsForJob.Text", "Ready to make a move?<br /><br />See current postings for<br />this job �");
        }
      }

      ModalPopup.Show();

			if (printFormat != ModalPrintFormat.None)
        PrintModal(printFormat);
		}

    /// <summary>
    /// Handles the Click event of the BookmarkLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void BookmarkLinkButton_Click(object sender, EventArgs e)
		{
			OnBookmarkClicked(new LightboxEventArgs
			                  	{
			                  		CommandName = "BookmarkJob",
			                  		CommandArgument = new LightboxEventCommandArguement()
			                  		                  	{
                                                  Id = JobReport.ReportData.JobId,
			                  		                  		Breadcrumbs = Breadcrumbs,
			                  		                  		LightboxName = JobNameLabel.Text,
                                                  LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                  		                  	}
			                  	});
		}

    /// <summary>
    /// Handles the Click event of the SendEmailImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendEmailImageButton_Click(object sender, EventArgs e)
		{
		    OnSendEmailClicked(new LightboxEventArgs
		    {
			    CommandName = "SendJobEmail",
			    CommandArgument = new LightboxEventCommandArguement()
			    {
				    Id = JobReport.ReportData.JobId,
				    Breadcrumbs = Breadcrumbs,
				    LightboxName = JobNameLabel.Text,
				    LightboxLocation = "", //LocationLabel.Text
				    CriteriaHolder = ReportCriteria
			    }
		    });
		}

    /// <summary>
    /// Handles the Click event of the ViewCurrentPostingsLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ViewCurrentPostingsLinkButton_Click(object sender, EventArgs e)
		{
    	var jobListSearchCriteria = new SearchCriteria();
      long careerAreaId = 0;
      if (!ReportCriteria.CareerAreaId.HasValue)
      {
        var careerAreas = ServiceClientLocator.ExplorerClient(App).GetJobCareerAreas(JobReport.ReportData.JobId);
        if (careerAreas.IsNotNullOrEmpty())
          careerAreaId = careerAreas[0];
      }
      else
        careerAreaId = ReportCriteria.CareerAreaId.Value;


    	jobListSearchCriteria.ROnetCriteria = new ROnetCriteria
    	                                      	{
                                                CareerAreaId = careerAreaId,
    	                                      		ROnets = new List<string> {JobReport.ReportData.ROnet}
    	                                      	};
      
      if (StateArea.StateCode.IsNotNullOrEmpty())
      {
				jobListSearchCriteria.JobLocationCriteria = new JobLocationCriteria
                               {
                                 Area = new AreaCriteria 
																				{
					       													MsaId = StateArea.AreaSortOrder == 0 ? (long?)null : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.StateMetropolitanStatisticalAreas).Where(x => x.ExternalId == StateArea.AreaCode).Select(x => x.Id).FirstOrDefault(), 
																					StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.ExternalId == StateArea.StateCode).Select(x => x.Id).FirstOrDefault()
					       												},


                                 SelectedStateInCriteria = StateArea.StateCode
                               };
      }
      else
      {
        jobListSearchCriteria.JobLocationCriteria = new JobLocationCriteria
        {
          Area = new AreaCriteria
          {
            City = null,
            MsaId = 0,
            StateId = 0
          },
          OnlyShowJobInMyState = false,
          Radius = null,
          SelectedStateInCriteria = null
        };
      }

      jobListSearchCriteria.RequiredResultCriteria = new RequiredResultCriteria
      {
        DocumentsToSearch = DocumentType.Posting,
        IncludeData = null,
        MinimumStarMatch = StarMatching.None,
        MaximumDocumentCount = App.Settings.MaximumNoDocumentToReturnInJobAlert
      };

      jobListSearchCriteria.PostingAgeCriteria = new PostingAgeCriteria
      {
        PostingAgeInDays = App.Settings.JobSearchPostingDate
      };

      App.SetSessionValue("Career:SearchCriteria", jobListSearchCriteria);

      var mainPage = Page as IExplorerJobModalPage;
      var mainCriteria = mainPage == null ? ReportCriteria : mainPage.ReturnReportCriteria;

      if (Request.Browser.Browser == "IE" && Request.Browser.Version.StartsWith("9."))
      {
        App.SetSessionValue("Explorer:ReturnBookmark", new LightboxEventArgs
        {
          CommandName = "Job",
          CommandArgument = new LightboxEventCommandArguement
          {
            Id = JobReport.ReportData.JobId,
            Breadcrumbs = Breadcrumbs,
            CriteriaHolder = ReportCriteria,
            MainPageCriteria = mainCriteria,
            LightboxLocation = Request.Url.PathAndQuery
          }
        });
      }

      App.SetCookieValue("Career:ManualSearch", "true");
      Response.RedirectToRoute("JobSearchResults");
		}

    /// <summary>
    /// Handles the Click event of the PrintImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalHeaderOptions_OnPrintCommand(object sender, CommandEventArgs eventArgs)
    {
      var printFormat = (ModalPrintFormat)Enum.Parse(typeof(ModalPrintFormat), eventArgs.CommandArgument.ToString(), true);
			if (ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
        PrintModal(printFormat);

				ModalPopup.Show();
			}
			else
			{
				OnPrintClicked(new LightboxEventArgs
				               	{
				               		CommandName = "PrintJob",
				               		CommandArgument = new LightboxEventCommandArguement
				               		                  	{
                                                Id = JobReport.ReportData.JobId,
                                                Breadcrumbs = Breadcrumbs,
                                                LightboxName = JobNameLabel.Text,
                                                LightboxLocation = "",//LocationLabel.Text
                                                CriteriaHolder = ReportCriteria,
                                                PrintFormat = printFormat
				               		                  	}
				               	});
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void DegreeRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      DegreeRepeaterBinding(e, "DegreeEducationLevelRepeater", 1);

    }
    /// <summary>
    /// Handles the ItemDataBound event of the DegreeTier2Repeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void DegreeTier2Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      DegreeRepeaterBinding(e, "DegreeEducationLevelTier2Repeater", 2);
    }

    /// <summary>
    /// Degrees the repeater binding.
    /// </summary>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    /// <param name="controlId">The control id.</param>
    /// <param name="tier">The tier.</param>
    private void DegreeRepeaterBinding(RepeaterItemEventArgs e, string controlId, int tier)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var degree = (DegreeDto)e.Item.DataItem;

        var degreeCertifications = DegreeCertifications;
        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl(controlId);
        degreeEducationLevelRepeater.DataSource = 
          App.Settings.ExplorerUseDegreeTiering
              ? degreeCertifications.DegreeEducationLevelsByTier(degree.Id.GetValueOrDefault(0), tier)
              : degreeCertifications.DegreeEducationLevels(degree.Id.GetValueOrDefault(0));
        degreeEducationLevelRepeater.DataBind();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the ProgramAreaRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void ProgramAreaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var dataItem = (ProgramArea)e.Item.DataItem;
        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");

        if (dataItem.Tier == 0)
        degreeEducationLevelRepeater.DataSource =
          DegreeCertifications.DegreeEducationLevels(ProgramAreaDegrees.Where(pad => pad.ProgramAreaId == dataItem.Id).Select(pad => pad.DegreeId).ToList());
        else
          degreeEducationLevelRepeater.DataSource =
            DegreeCertifications.ProgramAreaDegreeEducationLevelsByTier(ProgramAreaDegrees.Where(pad => pad.ProgramAreaId == dataItem.Id).Select(pad => pad.DegreeId).ToList(), dataItem.Tier);

        degreeEducationLevelRepeater.DataBind();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the CertificationRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void CertificationRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      // Add number before item
      var rankLiteral = (Literal)e.Item.FindControl("CertificationRankLiteral");
      rankLiteral.Text = (e.Item.ItemIndex + 1).ToString();
    }

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeEducationLevelRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void DegreeEducationLevelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var degreeEducationLevel = (DegreeEducationLevelDto) e.Item.DataItem;
        var degreeEducationLevelLinkButton = (LinkButton)e.Item.FindControl("DegreeEducationLevelLinkButton");
				
        switch (degreeEducationLevel.EducationLevel)
        {
          case ExplorerEducationLevels.Certificate:
            degreeEducationLevelLinkButton.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Certificate");
            break;

          case ExplorerEducationLevels.Associate:
            degreeEducationLevelLinkButton.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Associate");
            break;

          case ExplorerEducationLevels.Bachelor:
            degreeEducationLevelLinkButton.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Bachelor");
            break;

          case ExplorerEducationLevels.PostBachelorCertificate:
            degreeEducationLevelLinkButton.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Post-Baccalaureate Certificate");
            break;

          case ExplorerEducationLevels.GraduateProfessional:
            degreeEducationLevelLinkButton.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Graduate or Professional");
            break;

          case ExplorerEducationLevels.PostMastersCertificate:
            degreeEducationLevelLinkButton.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Post-Master's Certificate");
            break;

          case ExplorerEducationLevels.Doctorate:
            degreeEducationLevelLinkButton.Text = CodeLocalise(degreeEducationLevel.EducationLevel, "Doctorate");
            break;
        }
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the ProgramAreaDegreeEducationLevelRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void ProgramAreaDegreeEducationLevelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var degreeEducationLevel = (DegreeEducationLevelDto)e.Item.DataItem;
        var degreeEducationLevelLinkButton = (LinkButton)e.Item.FindControl("DegreeEducationLevelLinkButton");

        degreeEducationLevelLinkButton.Text = degreeEducationLevel.Name;
      }
    }

    /// <summary>
    /// Handles the Command event of the CareerPathLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void CareerPathLinkButton_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "LoadCareerPaths")
			{
				CareerPathDirection = (CareerPathDirections)Enum.Parse(typeof(CareerPathDirections), (string)e.CommandArgument);
				BindCareerPathSecondLevel();			
			}
		}

    /// <summary>
    /// Handles the Command event of the SecondLevelLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void SecondLevelLinkButton_Command(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "ViewCareerPath")
			{
				long jobId;
				if (long.TryParse(e.CommandArgument.ToString(), out jobId))
				{
					CareerPathSecondLevelLeftPanel.CssClass = "careerpathChartItem left";
					CareerPathSecondLevelMiddleLeftPanel.CssClass = "careerpathChartItem";
					CareerPathSecondLevelMiddleRightPanel.CssClass = "careerpathChartItem";
					CareerPathSecondLevelRightPanel.CssClass = "careerpathChartItem right";
				  CareerPathSecondLevelMiddleRightLinkButton.Enabled =
				    CareerPathSecondLevelMiddleLeftLinkButton.Enabled =
				    CareerPathSecondLevelLeftLinkButton.Enabled = CareerPathSecondLevelRightLinkButton.Enabled = true;
					var secondLevelLinkButton = (LinkButton)sender;
					secondLevelLinkButton.Enabled = false;

					if (secondLevelLinkButton.Parent is Panel)
					{
						var careerPathDirection = CareerPathDirection;

						var parentPanel = (Panel)secondLevelLinkButton.Parent;
						parentPanel.CssClass += careerPathDirection == CareerPathDirections.FromThisJob ? " highlight bottomArrowDown" : " highlight bottomArrowUp";
					}

					BindCareerPathThirdLevel(jobId, secondLevelLinkButton.ID);
				}
			}
		}

    /// <summary>
    /// Handles the Cancelled event of the Filter control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.FilterEventArgs"/> instance containing the event data.</param>
    protected void Filter_Cancelled(object sender, FilterEventArgs e)
    {
      var oldCriteria = e.CommandArgument.OldCriteria;
      var newCriteria = e.CommandArgument.NewCriteria;

      var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.Job, LinkId = JobReport.ReportData.JobId, LinkText = JobReport.ReportData.Name, Criteria = oldCriteria };
      var breadcrumbs = Breadcrumbs;
      breadcrumbs.Add(breadcrumb);

      OnItemClicked(new CommandEventArgs("ViewJob",
                                         new LightboxEventCommandArguement
                                         {
                                           Id = oldCriteria.CareerAreaJobId.GetValueOrDefault(),
                                           Breadcrumbs = breadcrumbs,
                                           CriteriaHolder = newCriteria
                                         }));
    }

    /// <summary>
    /// Handles the Click event of the CloseButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Click(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

    /// <summary>
    /// Handles the ItemClicked event of the JobModalBreadcrumb control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void JobModalBreadcrumb_ItemClicked(object sender, CommandEventArgs e)
		{
			long itemId;
			if (long.TryParse(e.CommandArgument.ToString(), out itemId))
			{
			  OnItemClicked(new CommandEventArgs(e.CommandName,
			                                     new LightboxEventCommandArguement
			                                       {
			                                         Id = itemId,
			                                         Breadcrumbs = Breadcrumbs,
			                                         MovingBack = true
			                                       }));
			}
		}

    /// <summary>
    /// Handles the ItemClicked event of the ModalList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ViewJob":
				case "ViewEmployer":
				case "ViewDegreeCertification":
        case "ViewProgramAreaDegree":
				case "ViewSkill":
					long itemId;
					if (long.TryParse(e.CommandArgument.ToString(), out itemId))
					{
            var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.Job, LinkId = JobReport.ReportData.JobId, LinkText = JobReport.ReportData.Name, Criteria = ReportCriteria };
						var breadcrumbs = Breadcrumbs;
						breadcrumbs.Add(breadcrumb);

						OnItemClicked(new CommandEventArgs(e.CommandName,
																							 new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = breadcrumbs }));
					}
					break;
			}
		}

    /// <summary>
    /// Handles the ItemClicked event of the ProgramsOfStudyLink control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void ProgramsOfStudyLink_ItemClicked(object sender, CommandEventArgs e)
    {
      if (e.CommandArgument.ToString() == "1")
      {
        SpecificProgramsOfStudyLink.Enabled = false;
        RelatedProgramsOfStudyLink.Enabled = true;

       if (ProgramAreaPlaceHolder.Visible)
       {
         ProgramAreaTier1PlaceHolder.Visible = true;
         ProgramAreaTier2PlaceHolder.Visible = false;
       }
       else
       {
         DegreeTier1PlaceHolder.Visible = true;
         DegreeTier2PlaceHolder.Visible = false;
       }
      }
      else if (e.CommandArgument.ToString() == "2")
      {
        SpecificProgramsOfStudyLink.Enabled = true;
        RelatedProgramsOfStudyLink.Enabled = false;

        if (ProgramAreaPlaceHolder.Visible)
        {
          ProgramAreaTier1PlaceHolder.Visible = false;
          ProgramAreaTier2PlaceHolder.Visible = true;
        }
        else
        {
          DegreeTier1PlaceHolder.Visible = false;
          DegreeTier2PlaceHolder.Visible = true;
        }
      }

      JobModalSection1TitleTableRow.Attributes["class"] = "multipleAccordionTitle2";
      JobModalSection1BodyTableRow.Attributes["class"] = "accordionContent2";

      JobModalSection4TitleTableRow.Attributes["class"] = "multipleAccordionTitle2 on";
      JobModalSection4BodyTableRow.Attributes["class"] = "accordionContent2 open";

      ModalPopup.Show();

    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			CareerPathFromLinkButton.Text = CodeLocalise("WherePeopleGoFromHere.LinkText", "Where people go from here");
			CareerPathToLinkButton.Text = CodeLocalise("HowDidOtherPeopleGetHere.LinkText", "How did other people get here");
    }

    /// <summary>
    /// Binds the controls.
    /// </summary>
		private void BindControls()
		{
			CareerPathFromLinkButton.CommandArgument = CareerPathDirections.FromThisJob.ToString();
			CareerPathFromLinkButton.CommandName = "LoadCareerPaths";
			CareerPathToLinkButton.CommandArgument = CareerPathDirections.ToThisJob.ToString();
			CareerPathToLinkButton.CommandName = "LoadCareerPaths";
		}

    /// <summary>
    /// Binds the career path second level.
    /// </summary>
		private void BindCareerPathSecondLevel()
		{
			var careerPaths = CareerPaths;

			var careerPathDirection = CareerPathDirection;

			if (careerPathDirection == CareerPathDirections.FromThisJob)
			{
				CareerPathFromLinkButton.Enabled = false;
				CareerPathToLinkButton.Enabled = true;
			}
			else
			{
				CareerPathFromLinkButton.Enabled = true;
				CareerPathToLinkButton.Enabled = false;
			}

			if (
				(careerPathDirection == CareerPathDirections.FromThisJob && careerPaths.CareerPathTo.Count == 0)
				||
				(careerPathDirection == CareerPathDirections.ToThisJob && careerPaths.CareerPathFrom.Count == 0))
			{
				//CareerPathFromLinkButton.Enabled = false;
				//CareerPathToLinkButton.Enabled = false;

				CareerPathPanel.Visible = false;
				NoCareerPathLabel.Visible = true;
				NoCareerPathLabel.Text = CodeLocalise("NoCareerPath.Text", "No career path information available");
			}
			else
			{
				CareerPathPanel.Visible = true;
				NoCareerPathLabel.Visible = false;

				var careerPath = careerPathDirection == CareerPathDirections.FromThisJob
				                 	? careerPaths.CareerPathTo
				                 	: careerPaths.CareerPathFrom;

				CareerPathSecondLevelLeftPanel.Visible =
					CareerPathSecondLevelMiddleLeftPanel.Visible =
					CareerPathSecondLevelMiddleRightPanel.Visible = CareerPathSecondLevelRightPanel.Visible = true;

				CareerPathTopLevelPanel.CssClass = careerPathDirection == CareerPathDirections.FromThisJob
																						? "careerpathChartItem top topArrowDown"
																						: "careerpathChartItem top topArrowUp";

				CareerPathSecondLevelLeftPanel.CssClass = "careerpathChartItem left";
				CareerPathSecondLevelMiddleLeftPanel.CssClass = "careerpathChartItem";
				CareerPathSecondLevelMiddleRightPanel.CssClass = "careerpathChartItem";
				CareerPathSecondLevelRightPanel.CssClass = "careerpathChartItem right";

				CareerPathSecondLevelMiddleLeftArrowLineLiteral.Text = "<span class=\"arrowLineMiddle\"></span>";

        //CareerPathSecondLevelLeftLinkButton.Enabled =
        //  CareerPathSecondLevelMiddleLeftLinkButton.Enabled =
        //  CareerPathSecondLevelMiddleRightLinkButton.Enabled = CareerPathSecondLevelRightLinkButton.Enabled = false;

        CareerPathJobNameLabel.Text = JobReport.ReportData.Name.Trim(55);
				switch (careerPath.Count)
				{
					case 1:
						CareerPathSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems1 centre";
            CareerPathSecondLevelLeftPanel.Visible = false;
            CareerPathSecondLevelMiddleLeftArrowLineLiteral.Text = String.Empty;
            CareerPathSecondLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
            CareerPathSecondLevelMiddleLeftLinkButton.Text = careerPath[0].Job.Name.Trim(55);
						CareerPathSecondLevelMiddleLeftLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
						CareerPathSecondLevelMiddleLeftLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelMiddleLeftLinkButton.Enabled = (careerPath[0].CareerPathTo != null && careerPath[0].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleRightPanel.Visible = false;
            CareerPathSecondLevelRightPanel.Visible = false;
            break;

					case 2:
						CareerPathSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems2 centre";
            CareerPathSecondLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
            CareerPathSecondLevelLeftLinkButton.Text = careerPath[0].Job.Name.Trim(55);
						CareerPathSecondLevelLeftLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
						CareerPathSecondLevelLeftLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelLeftLinkButton.Enabled = (careerPath[0].CareerPathTo != null && careerPath[0].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleLeftPanel.Visible = false;
            CareerPathSecondLevelMiddleRightPanel.Visible = false;
            CareerPathSecondLevelRightJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
            CareerPathSecondLevelRightLinkButton.Text = careerPath[1].Job.Name.Trim(55);
						CareerPathSecondLevelRightLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
						CareerPathSecondLevelRightLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelRightLinkButton.Enabled = (careerPath[1].CareerPathTo != null && careerPath[1].CareerPathTo.Count > 0);
            break;

					case 3:
						CareerPathSecondLevelPanel.CssClass = "careerPathRowWrap noOfItems3 centre";
            CareerPathSecondLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
            CareerPathSecondLevelLeftLinkButton.Text = careerPath[0].Job.Name.Trim(55);
						CareerPathSecondLevelLeftLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
						CareerPathSecondLevelLeftLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelLeftLinkButton.Enabled = (careerPath[0].CareerPathTo != null && careerPath[0].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
            CareerPathSecondLevelMiddleLeftLinkButton.Text = careerPath[1].Job.Name.Trim(55);
            CareerPathSecondLevelMiddleLeftLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
            CareerPathSecondLevelMiddleLeftLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelMiddleLeftLinkButton.Enabled = (careerPath[1].CareerPathTo != null && careerPath[1].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleRightPanel.Visible = false;
            CareerPathSecondLevelRightJobLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
            CareerPathSecondLevelRightLinkButton.Text = careerPath[2].Job.Name.Trim(55);
            CareerPathSecondLevelRightLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
            CareerPathSecondLevelRightLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelRightLinkButton.Enabled = (careerPath[2].CareerPathTo != null && careerPath[2].CareerPathTo.Count > 0);
            break;

          case 4:
            CareerPathSecondLevelPanel.CssClass = "careerPathRowWrap";
            CareerPathSecondLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
            CareerPathSecondLevelLeftLinkButton.Text = careerPath[0].Job.Name.Trim(55);
            CareerPathSecondLevelLeftLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
            CareerPathSecondLevelLeftLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelLeftLinkButton.Enabled = (careerPath[0].CareerPathTo != null && careerPath[0].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
            CareerPathSecondLevelMiddleLeftLinkButton.Text = careerPath[1].Job.Name.Trim(55);
            CareerPathSecondLevelMiddleLeftLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
            CareerPathSecondLevelMiddleLeftLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelMiddleLeftLinkButton.Enabled = (careerPath[1].CareerPathTo != null && careerPath[1].CareerPathTo.Count > 0);
            CareerPathSecondLevelMiddleRightJobLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
            CareerPathSecondLevelMiddleRightLinkButton.Text = careerPath[2].Job.Name.Trim(55);
            CareerPathSecondLevelMiddleRightLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
            CareerPathSecondLevelMiddleRightLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelMiddleRightLinkButton.Enabled = (careerPath[2].CareerPathTo != null && careerPath[2].CareerPathTo.Count > 0);
            CareerPathSecondLevelRightJobLinkButton.CommandArgument = careerPath[3].Job.Id.ToString();
            CareerPathSecondLevelRightLinkButton.Text = careerPath[3].Job.Name.Trim(55);
            CareerPathSecondLevelRightLinkButton.CommandArgument = careerPath[3].Job.Id.ToString();
            CareerPathSecondLevelRightLinkButton.CommandName = "ViewCareerPath";
            CareerPathSecondLevelRightLinkButton.Enabled = (careerPath[3].CareerPathTo != null && careerPath[3].CareerPathTo.Count > 0);
            break;
				}

				CareerPathThirdLevelLeftPanel.Visible = false;
				CareerPathThirdLevelMiddleLeftPanel.Visible = false;
				CareerPathThirdLevelMiddleRightPanel.Visible = false;
				CareerPathThirdLevelRightPanel.Visible = false;
			}			
		}

    /// <summary>
    /// Binds the career path third level.
    /// </summary>
    /// <param name="secondLevelJobId">The second level job id.</param>
    /// <param name="secondLevelJobLinkButtonId">The second level job link button id.</param>
		private void BindCareerPathThirdLevel(long secondLevelJobId, string secondLevelJobLinkButtonId)
		{
			var careerPaths = CareerPaths;

			var careerPathDirection = CareerPathDirection;

			var careerPathModel = careerPathDirection == CareerPathDirections.FromThisJob
															? careerPaths.CareerPathTo.SingleOrDefault(x => x.Job.Id == secondLevelJobId)
															: careerPaths.CareerPathFrom.SingleOrDefault(x => x.Job.Id == secondLevelJobId);

			var careerPath = careerPathDirection == CareerPathDirections.FromThisJob
												? careerPathModel.CareerPathTo
												: careerPathModel.CareerPathFrom;

			CareerPathThirdLevelLeftPanel.Visible =
				CareerPathThirdLevelMiddleLeftPanel.Visible =
				CareerPathThirdLevelMiddleRightPanel.Visible = CareerPathThirdLevelRightPanel.Visible = false;

			CareerPathThirdLevelMiddleLeftArrowLineLiteral.Text = "<span class=\"arrowLineMiddle\"></span>";

			SetCareerPathThirdLevelPanelCssClass(secondLevelJobLinkButtonId, careerPath.Count);

			switch (careerPath.Count)
			{
				case 1:
					CareerPathThirdLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
          CareerPathThirdLevelMiddleLeftArrowLineLiteral.Text = String.Empty;
          CareerPathThirdLevelMiddleLeftPanel.Visible = true;
          CareerPathThirdLevelMiddleLeftJobNameLabel.Text = careerPath[0].Job.Name.Trim(55);
          break;

				case 2:
					CareerPathThirdLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
          CareerPathThirdLevelLeftPanel.Visible = true;
          CareerPathThirdLevelLeftJobNameLabel.Text = careerPath[0].Job.Name.Trim(55);
          CareerPathThirdLevelRightJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
          CareerPathThirdLevelRightPanel.Visible = true;
          CareerPathThirdLevelRightJobNameLabel.Text = careerPath[1].Job.Name.Trim(55);
          break;

				case 3:
					CareerPathThirdLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
          CareerPathThirdLevelLeftPanel.Visible = true;
          CareerPathThirdLevelLeftJobNameLabel.Text = careerPath[0].Job.Name.Trim(55);
          CareerPathThirdLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
          CareerPathThirdLevelMiddleLeftPanel.Visible = true;
          CareerPathThirdLevelMiddleLeftJobNameLabel.Text = careerPath[1].Job.Name.Trim(55);
          CareerPathThirdLevelRightJobLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
          CareerPathThirdLevelRightPanel.Visible = true;
          CareerPathThirdLevelRightJobNameLabel.Text = careerPath[2].Job.Name.Trim(55);
          break;

				case 4:
					CareerPathThirdLevelLeftJobLinkButton.CommandArgument = careerPath[0].Job.Id.ToString();
          CareerPathThirdLevelLeftPanel.Visible = true;
          CareerPathThirdLevelLeftJobNameLabel.Text = careerPath[0].Job.Name.Trim(55);
					CareerPathThirdLevelMiddleLeftJobLinkButton.CommandArgument = careerPath[1].Job.Id.ToString();
          CareerPathThirdLevelMiddleLeftPanel.Visible = true;
          CareerPathThirdLevelMiddleLeftJobNameLabel.Text = careerPath[1].Job.Name.Trim(55);
          CareerPathThirdLevelMiddleRightJobLinkButton.CommandArgument = careerPath[2].Job.Id.ToString();
          CareerPathThirdLevelMiddleRightPanel.Visible = true;
          CareerPathThirdLevelMiddleRightJobNameLabel.Text = careerPath[2].Job.Name.Trim(55);
          CareerPathThirdLevelRightJobLinkButton.CommandArgument = careerPath[3].Job.Id.ToString();
          CareerPathThirdLevelRightPanel.Visible = true;
          CareerPathThirdLevelRightJobNameLabel.Text = careerPath[3].Job.Name.Trim(55);
          break;
			}
		}

    /// <summary>
    /// Sets the career path third level panel CSS class.
    /// </summary>
    /// <param name="secondLevelJobLinkButtonId">The second level job link button id.</param>
    /// <param name="thirdLevelCareerPathCount">The third level career path count.</param>
		private void SetCareerPathThirdLevelPanelCssClass(string secondLevelJobLinkButtonId, int thirdLevelCareerPathCount)
		{
			var careerPaths = CareerPaths;
			var careerPathDirection = CareerPathDirection;

			var secondLevelCareerPath = careerPathDirection == CareerPathDirections.FromThisJob
			                            	? careerPaths.CareerPathTo
			                            	: careerPaths.CareerPathFrom;

			var secondLevelCareerPathCount = secondLevelCareerPath.Count;

			var clickPosition = secondLevelJobLinkButtonId.Replace("CareerPathSecondLevel", String.Empty);
			clickPosition = clickPosition.Replace("LinkButton", String.Empty);

			var cssClass = "careerPathRowWrap";

			if (thirdLevelCareerPathCount < 4)
			{
				if (secondLevelCareerPathCount == 1)
				{
					cssClass = String.Format("careerPathRowWrap noOfItems{0} centre", thirdLevelCareerPathCount.ToString());
				}
				else
				{
					cssClass = String.Format("careerPathRowWrap second{0}{1}third{2}", secondLevelCareerPathCount, clickPosition,
																	 thirdLevelCareerPathCount);					
				}
			}

			CareerPathThirdLevelPanel.CssClass = cssClass;
		}

    /// <summary>
    /// Prints the modal.
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintModal(ModalPrintFormat printFormat = ModalPrintFormat.Html)
    {
			var printModel = new ExplorerJobPrintModel
			{
				Name = JobNameLabel.Text,
				Location = "", // TODO: Add filter text?
        JobRelatedToSchoolDegree = DegreeProgramPanel.Visible,
        ShowOtherFoundationSkills = false,
        StarterJobLevel = JobReport.ReportData.StarterJob,
        JobIntroduction = JobIntroductionLabel.Text,
				JobTitles = JobTitlesLabel.Text,
				HiringTrend = HiringTrendLiteral.Text,
				HiringTrendArrowSpan = HiringTrendArrowSpanLiteral.Text,
				HiringDemand = HiringDemandLiteral.Text,
				HiringDemandImageUrl = HiringDemandImage.ImageUrl,
				SalaryRange = SalaryRangeLiteral.Text,
				SalaryRangeImageUrl = SalaryRangeImage.ImageUrl,
				SalaryNationwideLabel = SalaryNationwideLabelLiteral.Text,
				SalaryNationwide = SalaryNationwideLiteral.Text,
				SalaryNationwideImageUrl = SalaryNationwideImage.ImageUrl,
				Employers = JobEmployerList.Employers,
				DegreeIntro = DegreeIntroLabel.Text,
				DegreeCertifications = DegreeCertifications,
        ProgramAreaDegrees = ProgramAreaDegrees,
				LessThanTwoSpan = LessThanTwoLiteral.Text,
				TwoToFiveSpan = TwoToFiveLiteral.Text,
				FiveToEightSpan = FiveToEightLiteral.Text,
				EightPlusSpan = EightPlusLiteral.Text,
        SimilarJobs = SimilarJobList.GetJobReports(JobListModes.Similar),
        ReportCriteria = ReportCriteria
			};

			var skills = SkillList.Skills;
			printModel.SpecializedSkills =
				skills.Where(x => x.JobSkillType == JobSkillTypes.Demanded && x.SkillType == SkillTypes.Specialized).ToList();
			printModel.SoftwareSkills =
				skills.Where(x => x.JobSkillType == JobSkillTypes.Demanded && x.SkillType == SkillTypes.Software).ToList();
			printModel.FoundationSkills =
				skills.Where(x => x.JobSkillType == JobSkillTypes.Demanded && x.SkillType == SkillTypes.Foundation).ToList();
			printModel.HeldSkills =
				skills.Where(x => x.JobSkillType == JobSkillTypes.Held).ToList();

			printModel.EducationRequirementsVisible = EducationRequirementsPlaceHolder.Visible;
			if (printModel.EducationRequirementsVisible)
			{
				printModel.HighSchoolTechnicalTrainingSpan = HighSchoolTechnicalTrainingLiteral.Text;
				printModel.AssociatesDegreeSpan = AssociatesDegreeLiteral.Text;
				printModel.BachelorsDegreeSpan = BachelorsDegreeLiteral.Text;
				printModel.GraduateProfessionalDegreeSpan = GraduateProfessionalDegreeLiteral.Text;
			}

			printModel.CareerPathTo = CareerPaths.CareerPathTo;
			printModel.CareerPathFrom = CareerPaths.CareerPathFrom;

			const string sessionKey = "Explorer:JobPrintModel";
			App.SetSessionValue(sessionKey, printModel);

      OpenPrintWindow(ReportTypes.Job, printFormat);
		}
	}
}