﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.CareerExplorer.Code;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Framework.Core;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class DegreeCertificationModal : ExplorerModalControl
	{
		public DegreeEducationLevelViewDto DegreeEducationLevel
		{
			get { return GetViewStateValue<DegreeEducationLevelViewDto>("DegreeCertificationModal:DegreeEducationLevel"); }
			set { SetViewStateValue("DegreeCertificationModal:DegreeEducationLevel", value); }
		}

		public ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue<ExplorerCriteria>("DegreeCertificationModal:ReportCriteria"); }
			set { SetViewStateValue("DegreeCertificationModal:ReportCriteria", value); }
		}

		public List<LightboxBreadcrumb> Breadcrumbs
		{
			get { return GetViewStateValue("DegreeCertificationModal:Breadcrumb", new List<LightboxBreadcrumb>()); }
			set { SetViewStateValue("DegreeCertificationModal:Breadcrumb", value); }
		}

		private List<StudyPlaceDegreeEducationLevelViewDto> StudyPlaces
		{
			get { return GetViewStateValue("DegreeCertificationModal:StudyPlaces", new List<StudyPlaceDegreeEducationLevelViewDto>()); }
			set { SetViewStateValue("DegreeCertificationModal:StudyPlaces", value); }
		}

	  public int JobDegreeTierLevel
    {
      get { return GetViewStateValue("DegreeCertificationModal:JobDegreeTierLevel", 0); }
      set { SetViewStateValue("DegreeCertificationModal:JobDegreeTierLevel", value); }
	  }

		public StateAreaViewDto StateArea
		{
			get { return GetViewStateValue<StateAreaViewDto>("JobModal:StateArea", null); }
			set { SetViewStateValue("JobModal:StateArea", value); }
		}

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				LocaliseUI();
        ReadyToMakeAMovePlaceHolder.Visible = ViewCurrentPostingsLinkButton.Visible = (App.Settings.Module != FocusModules.Explorer);
			}
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      NoJobsFoundLiteral.Text = CodeLocalise("NoJobsFoundLiteral.Text", "No jobs found") + "</br>";

      if (App.Settings.ExplorerUseDegreeTiering)
      {
        PrimaryJobsLinkButton.Text = CodeLocalise("JobDegreeListTier1.Text", "Specific to your program of study");
        SecondaryJobsLinkButton.Text = CodeLocalise("JobDegreeListTier2.Text", "May be related to your studies");
      }
    }

    /// <summary>
    /// Shows the specified degree education level id.
    /// </summary>
    /// <param name="degreeEducationLevelId">The degree education level id.</param>
    /// <param name="criteria">The criteria.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="printFormat">The format to print the modal</param>
    public void Show(long degreeEducationLevelId, ExplorerCriteria criteria, List<LightboxBreadcrumb> breadcrumbs, ModalPrintFormat printFormat = ModalPrintFormat.None)
    {
			StateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(criteria.StateAreaId);

      var degreeEducationLevel = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevel(degreeEducationLevelId);

      DegreeEducationLevel = degreeEducationLevel;

      criteria.EducationCriteriaOption = EducationCriteriaOptions.Specific;
      criteria.DegreeEducationLevelId = DegreeEducationLevel.Id;
      criteria.DegreeEducationLevel = DegreeEducationLevel.DegreeEducationLevelName;

      ReportCriteria = criteria;
      ReportCriteria.ReportType = ReportTypes.DegreeCertification;
      Breadcrumbs = breadcrumbs;
      CriteriaFilter.Bind(ReportTypes.DegreeCertification, criteria);

      ModalHeaderOptions.BindBreadcrumbs(Breadcrumbs);
      //if (breadcrumbs != null && breadcrumbs.Count == 3)
      //  Page.ClientScript.RegisterStartupScript(this.GetType(), "DisableLightboxModalLinks",
      //                                          String.Format(
      //                                            "DisableLightboxModalLinks('degreeCertificationLightBox','{0}');",
      //                                            CodeLocalise("Modal.BreadcrumbLimitMessage",
      //                                                         "The application only allows for three sets of data to be open at any one time.")),
      //                                          true);

      DegreeCertificationNameLabel.Text = degreeEducationLevel.DegreeEducationLevelName;
      //LocationLabel.Text = String.Format("({0})", ServiceClientLocator.ExplorerClient(App).GetStateAreaName(criteria.StateAreaId));

      if (App.Settings.ExplorerUseDegreeTiering)
      {
        TieredJobsPanel.Visible = true;
        DegreeCertificationJobList.JobDegreeTierLevel = 1;
        PrimaryJobsLinkButton.Enabled = false;
        SecondaryJobsLinkButton.Enabled = true;
      }
      else
      {
        TieredJobsPanel.Visible = false;
      }

      DegreeCertificationJobList.DegreeEducationLevelId = degreeEducationLevelId;
      DegreeCertificationJobList.ReportCriteria = criteria;
      DegreeCertificationJobList.ListSize = 50;
      DegreeCertificationJobList.EnablePaging = true;
      DegreeCertificationJobList.BindList(true);

      if (App.Settings.ExplorerUseDegreeTiering && DegreeCertificationJobList.RecordCount == 0)
      {
        // If nothing in tier 1 but there is in tier 2 show tier 2 by default
        DegreeCertificationJobList.JobDegreeTierLevel = 2;
        DegreeCertificationJobList.BindList(true);
        if (DegreeCertificationJobList.RecordCount > 0)
        {
          PrimaryJobsLinkButton.Enabled = true;
          SecondaryJobsLinkButton.Enabled = false;
        }
      }

      NoJobsFoundLiteral.Visible = (DegreeCertificationJobList.RecordCount == 0);

      DegreeCertificationSkillList.ReportCriteria = criteria;
      DegreeCertificationSkillList.ListSize = 20;
      DegreeCertificationSkillList.DegreeEducationLevelId = degreeEducationLevelId;
      DegreeCertificationSkillList.BindList(true, SkillTypes.Specialized);

      DegreeCertificationEmployerList.ReportCriteria = criteria;
      DegreeCertificationEmployerList.DegreeEducationLevelId = degreeEducationLevelId;
      DegreeCertificationEmployerList.BindList();
      if (DegreeCertificationEmployerList.Employers.Count > 0)
        TopEmployersIntroLabel.Text = "";
      else
        TopEmployersIntroLabel.Text = CodeLocalise("TopEmployersIntroNoResults.Text",
																									 "While there may have been numerous job openings for this degree in recent months, no #BUSINESS#:LOWER has advertised 10 or more openings in your area.");

      Section4PlaceHolder.Visible = true;
      Section4HeaderLiteral.Text = CodeLocalise("WhereCanIStudyThis.Header", "Where can I study this?");

      StudyPlacePlaceHolder.Visible = true;
      ClientDegreeInfoPlaceHolder.Visible = false;
      ClientDegreeProgramDisclosurePlaceHolder.Visible = false;

      // TODO: Remove client specific code
      if (degreeEducationLevel.IsClientData && App.Settings.ExplorerDegreeClientDataTag == "ApolloUoPX")
      {
        Section4HeaderLiteral.Text = CodeLocalise("ClientDegreeSection4.Header", "University of Phoenix degree programs");
        StudyPlacePlaceHolder.Visible = false;
        ClientDegreeInfoPlaceHolder.Visible = true;
        ClientDegreeProgramDisclosurePlaceHolder.Visible = true;

        var degreeEducationLevelExts = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevelExts(degreeEducationLevelId);
        ClientDegreeInfoRepeater.DataSource = degreeEducationLevelExts;
        ClientDegreeInfoRepeater.DataBind();

        ClientDegreeDisclaimerLiteral.Text = CodeLocalise("ClientDegreeDisclaimer.Text", string.Empty);
      }
      else if (App.Settings.ExplorerShowDegreeStudyPlaces)
      {
        var stateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(criteria.StateAreaId);
        var studyPlaces = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevelStudyPlaces(degreeEducationLevelId,
                                                                                     stateArea.StateSortOrder == 0
                                                                                       ? 0
                                                                                       : criteria.StateAreaId, 999);

        StudyPlaces = studyPlaces;

        StudyPlaceRepeater.DataSource = studyPlaces;
        StudyPlaceRepeater.DataBind();
      }
      else
      {
        Section4PlaceHolder.Visible = false;
      }

      if (App.Settings.Theme == FocusThemes.Education)
      {
        var degreeEducationLevelExts = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevelExts(degreeEducationLevelId);
        if (degreeEducationLevelExts.Any())
        {
          var details = degreeEducationLevelExts.First();
          DegreeDetailsPlaceHolder.Visible = true;

          var description = details.Description.Trim(new[] {' ', '.'});
          var sentences = description.Split('.');
          if (sentences.Length > 3)
          {
            DegreeDescription.Text = string.Concat(string.Join(". ", sentences, 0, 3), ". ");
            DegreeDescriptionOverflowPanel.Visible = true;
            DegreeDescriptionOverflowText.Text = string.Concat(string.Join(". ", sentences, 3, sentences.Length - 3), ".");
          }
          else
          {
            DegreeDescription.Text = details.Description;
          }

          if (details.Url.IsNotNullOrEmpty())
          {
            DegreeUrl.Text = CodeLocalise("DegreeCertification.FindOutMore.Link", "Find out more");
            DegreeUrl.NavigateUrl = details.Url;
          }
        }
      }

      if (App.Settings.Module != FocusModules.Explorer)
      {
        if (App.Settings.ExplorerReadyToMakeAMoveBubble)
        {
          ReadyToMakeAMovePlaceHolder.Visible = true;
          ViewCurrentPositionsLiteral.Text = HtmlLocalise("SeeCurrentPostingsForJob.Text", "See current postings for<br />this program of study »");
        }
        else
        {
          ReadyToMakeAMovePlaceHolder.Visible = false;
          ViewCurrentPositionsLiteral.Text = HtmlLocalise("ReadySeeCurrentPostingsForJob.Text", "Ready to make a move?<br /><br />See current postings for<br />this program of study »");
        }
      }

      ModalPopup.Show();

      if (printFormat != ModalPrintFormat.None)
        PrintModal(printFormat);
    }

		/// <summary>
    /// Handles the Click event of the BookmarkLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void BookmarkLinkButton_Click(object sender, EventArgs e)
		{
			OnBookmarkClicked(new LightboxEventArgs
			                  	{
			                  		CommandName = "BookmarkDegreeCertification",
			                  		CommandArgument = new LightboxEventCommandArguement
			                  		                  	{
			                  		                  		Id = DegreeEducationLevel.Id.Value,
			                  		                  		Breadcrumbs = Breadcrumbs,
			                  		                  		LightboxName = DegreeCertificationNameLabel.Text,
                                                  LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                  		                  	}
			                  	});
		}

    /// <summary>
    /// Handles the Click event of the SendEmailImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendEmailImageButton_Click(object sender, EventArgs e)
		{
			OnSendEmailClicked(new LightboxEventArgs
			                   	{
			                   		CommandName = "SendDegreeCertificationEmail",
			                   		CommandArgument = new LightboxEventCommandArguement
			                   		                  	{
			                   		                  		Id = DegreeEducationLevel.Id.Value,
			                   		                  		Breadcrumbs = Breadcrumbs,
			                   		                  		LightboxName = DegreeCertificationNameLabel.Text,
                                                  LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                   		                  	}
			                   	});
		}

    /// <summary>
    /// Handles the Click event of the PrintImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalHeaderOptions_OnPrintCommand(object sender, CommandEventArgs eventArgs)
    {
      var printFormat = (ModalPrintFormat)Enum.Parse(typeof(ModalPrintFormat), eventArgs.CommandArgument.ToString(), true);
			if (ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
        PrintModal(printFormat);

				ModalPopup.Show();
			}
			else
			{
				OnPrintClicked(new LightboxEventArgs
				                 {
				               		CommandName = "PrintDegreeCertification",
				               		CommandArgument = new LightboxEventCommandArguement
				               		                  	{
				               		                  		Id = DegreeEducationLevel.Id.Value,
                                                Breadcrumbs = Breadcrumbs,
                                                LightboxName = DegreeCertificationNameLabel.Text,
                                                LightboxLocation = "",//LocationLabel.Text
                                                CriteriaHolder = ReportCriteria,
                                                PrintFormat = printFormat
				               		                  	}
				               	});
			}
		}

    /// <summary>
    /// Handles the Click event of the CloseButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Click(object sender, EventArgs e)
		{
			ReportCriteria.CareerArea = "All Career Areas";
			ReportCriteria.ReportType = ReportTypes.Job;
			ReportCriteria.EducationCriteriaOption = EducationCriteriaOptions.DegreeLevel;
			ReportCriteria.DegreeEducationLevel = null;
			ReportCriteria.DegreeEducationLevelId = null;

			ModalPopup.Hide();
		}

    /// <summary>
    /// Handles the ItemClicked event of the DegreeCertificationModalBreadcrumb control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void DegreeCertificationModalBreadcrumb_ItemClicked(object sender, CommandEventArgs e)
		{
			long itemId;
			if (long.TryParse(e.CommandArgument.ToString(), out itemId))
			{
				OnItemClicked(new CommandEventArgs(e.CommandName,
																					 new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = Breadcrumbs, MovingBack = true}));
			}
		}

    /// <summary>
    /// Handles the Cancelled event of the Filter control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.FilterEventArgs"/> instance containing the event data.</param>
    protected void Filter_Cancelled(object sender, FilterEventArgs e)
    {
      var oldCriteria = e.CommandArgument.OldCriteria;
      var newCriteria = e.CommandArgument.NewCriteria;

      var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.DegreeCertification, LinkId = DegreeEducationLevel.Id.GetValueOrDefault(), LinkText = DegreeEducationLevel.DegreeEducationLevelName, Criteria = oldCriteria };
      var breadcrumbs = Breadcrumbs;
      breadcrumbs.Add(breadcrumb);

      OnItemClicked(new CommandEventArgs("ViewDegreeCertification",
                                         new LightboxEventCommandArguement
                                         {
                                           Id = oldCriteria.DegreeEducationLevelId.GetValueOrDefault(),
                                           Breadcrumbs = breadcrumbs,
                                           CriteriaHolder = newCriteria
                                         }));
    }

    /// <summary>
    /// Handles the ItemClicked event of the ModalList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ViewJob":
				case "ViewSkill":
				case "ViewEmployer":
					long itemId;
					if (long.TryParse(e.CommandArgument.ToString(), out itemId))
					{
						var breadcrumb = new LightboxBreadcrumb
						                 	{
						                 		LinkType = ReportTypes.DegreeCertification,
						                 		LinkId = DegreeEducationLevel.Id.Value,
						                 		LinkText = DegreeEducationLevel.DegreeEducationLevelName,
                                Criteria = ReportCriteria
						                 	};
						var breadcrumbs = Breadcrumbs;
						breadcrumbs.Add(breadcrumb);

						OnItemClicked(new CommandEventArgs(e.CommandName,
																							 new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = breadcrumbs }));
					}
					break;
			}
		}

    /// <summary>
    /// Handles the ItemClicked event of the JobDegreeTierLevel control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void JobDegreeTierLevel_ItemClicked(object sender, CommandEventArgs e)
    {
      if (Convert.ToInt32(e.CommandArgument) == 1)
      {
        DegreeCertificationJobList.JobDegreeTierLevel = 1;
        PrimaryJobsLinkButton.Enabled = false;
        SecondaryJobsLinkButton.Enabled = true;
      }
      else if (Convert.ToInt32(e.CommandArgument) == 2)
      {
        DegreeCertificationJobList.JobDegreeTierLevel = 2;
        PrimaryJobsLinkButton.Enabled = true;
        SecondaryJobsLinkButton.Enabled = false;
      }
      DegreeCertificationJobList.DegreeEducationLevelId = DegreeEducationLevel.Id.GetValueOrDefault();
      DegreeCertificationJobList.ReportCriteria = ReportCriteria;
      DegreeCertificationJobList.ListSize = 50;
      DegreeCertificationJobList.BindList(true);
      NoJobsFoundLiteral.Visible = (DegreeCertificationJobList.RecordCount == 0);
      ModalPopup.Show();
    }

    /// <summary>
    /// Prints the modal.
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintModal(ModalPrintFormat printFormat = ModalPrintFormat.Html)
		{
			var printModel = new ExplorerDegreeCertificationPrintModel
			{
				Name = DegreeCertificationNameLabel.Text,
        Tier = DegreeEducationLevel.Tier.GetValueOrDefault(),
				Location = "",//LocationLabel.Text,
				Jobs = DegreeCertificationJobList.GetJobReports(JobListModes.DegreeEducationLevel, App.Settings.ExplorerUseDegreeTiering? 1 : 0),
				Employers = DegreeCertificationEmployerList.Employers,
        StudyPlaces = StudyPlaces,
        ReportCriteria = ReportCriteria
			};
      if (App.Settings.ExplorerUseDegreeTiering)
      {
        var tier2Jobs = DegreeCertificationJobList.GetJobReports(JobListModes.DegreeEducationLevel, 2);
        if (tier2Jobs.IsNullOrEmpty())
        {
          printModel.JobsTier2 = DegreeCertificationJobList.GetJobReports(JobListModes.DegreeEducationLevel,
                                                                          ReportCriteria, 10, 0,
                                                                          DegreeEducationLevel.Id.GetValueOrDefault(),
                                                                          0, 0, 0, new JobCareerPathModel() , new List<OnetCode>(), 2, 0, "");
        }
        else
        {
          printModel.JobsTier2 = tier2Jobs.Take(10).ToList();
        }
      }

			var skills = DegreeCertificationSkillList.GetSkills(SkillListSimple.SkillsListModes.DegreeEducationLevel);
			printModel.SpecializedSkills = skills.Where(x => x.SkillType == SkillTypes.Specialized).ToList();
			printModel.SoftwareSkills = skills.Where(x => x.SkillType == SkillTypes.Software).ToList();
			printModel.FoundationSkills = skills.Where(x => x.SkillType == SkillTypes.Foundation).ToList();

			const string sessionKey = "Explorer:DegreeCertificationPrintModel";
			App.SetSessionValue(sessionKey, printModel);

      OpenPrintWindow(ReportTypes.DegreeCertification, printFormat);
		}

		/// <summary>
		/// Handles the Click event of the ViewCurrentPostingsLinkButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ViewCurrentPostingsLinkButton_Click(object sender, EventArgs e)
		{
			var criteria = new SearchCriteria
				{
					EducationProgramOfStudyCriteria = new EducationProgramOfStudyCriteria
						{
							//ProgramAreaId = ,
							DegreeEducationLevelId = DegreeEducationLevel.Id
						}
				};

			if (StateArea.StateCode.IsNotNullOrEmpty())
			{
				criteria.JobLocationCriteria = new JobLocationCriteria
				{
					Area = new AreaCriteria
					{
						MsaId = StateArea.AreaSortOrder == 0 ? (long?)null : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.StateMetropolitanStatisticalAreas).Where(x => x.ExternalId == StateArea.AreaCode).Select(x => x.Id).FirstOrDefault(),
						StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.ExternalId == StateArea.StateCode).Select(x => x.Id).FirstOrDefault()
					},
					SelectedStateInCriteria = StateArea.StateCode
				};
			}

			App.SetSessionValue("Career:SearchCriteria", criteria);

      App.SetCookieValue("Career:ManualSearch", "true");
      Response.RedirectToRoute("JobSearchResults");
		}
	}
}