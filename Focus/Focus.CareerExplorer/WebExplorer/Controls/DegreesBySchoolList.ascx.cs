﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core.DataTransferObjects.FocusExplorer;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class DegreesBySchoolList : ExplorerListControl
	{
    private struct ProgramArea
    {
      public long Id;
      public string Name;
    }

    public override int RecordCount { get { return _degrees.Count(); } }

    private List<ProgramAreaDegreeEducationLevelViewDto> _degrees;
    private List<ProgramAreaDegreeViewDto> _programAreaDegrees;

    private int _programAreaIndex = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
			if (!IsPostBack) ApplyBranding();
    }

		/// <summary>
		/// Applies client specific branding.
		/// </summary>
		private void ApplyBranding()
		{
			OtherDegreeEducationLevelSort.ImageUrl = UrlBuilder.SortDown();
		}

    /// <summary>
    /// Binds the degree repeaters to the data sources
    /// </summary>
    /// <param name="forceReload"></param>
    public override void BindList(bool forceReload)
    {
      var degrees = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(null, ReportCriteria, checkDegreeLevelByJob: false);
      _degrees = degrees.Where(d => d.IsClientData).ToList();


      var degreeIds = degrees.Select(jd => jd.DegreeId);

      // Get the distinct program areas that link to the degrees
      _programAreaDegrees = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegrees().Where(pad => degreeIds.Contains(pad.DegreeId)).ToList();

      if (_programAreaDegrees.Any())
      {
        var progamAreas =
          _programAreaDegrees.Select(pa => new ProgramArea { Id = pa.ProgramAreaId, Name = pa.ProgramAreaName })
                             .Distinct()
                             .ToList();

        ProgramAreaRepeater.DataSource = progamAreas;
        ProgramAreaRepeater.DataBind();

        ProgramAreaRepeater.Visible = true;
      }
      else
      {
        ProgramAreaRepeater.Visible = false;
      }

      var programAreaDegreeIds = _programAreaDegrees.Select(pad => pad.DegreeId).ToList();
      var nonProgramAreaDegrees = _degrees.Where(jd => !programAreaDegreeIds.Contains(jd.DegreeId)).OrderBy(jd => jd.DegreeName).ThenBy(jd => jd.DegreeEducationLevelId);

      if (nonProgramAreaDegrees.Any())
      {
        OtherDegreeEducationLevelRepeater.DataSource = nonProgramAreaDegrees;
        OtherDegreeEducationLevelRepeater.DataBind();

        OtherDegreeHeader.Text = CodeLocalise("OtherDegrees.Header", "Other Degrees");
        OtherDegreeEducationLevelUpdatePanel.Visible = true;
      }
      else
      {
        OtherDegreeEducationLevelUpdatePanel.Visible = false;
      }
    }

    /// <summary>
    /// Binds an individual program area to the repeater
    /// </summary>
    /// <param name="sender">The repeater raising the event</param>
    /// <param name="e">The event argument containing details of the item</param>
    protected void ProgramAreaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var programArea = (ProgramArea)e.Item.DataItem;

        // Binds the link button to the program area
        var linkButton = (LinkButton) e.Item.FindControl("ProgramAreaLink");
        linkButton.CommandArgument = programArea.Id.ToString(CultureInfo.InvariantCulture);
        linkButton.Text = CodeLocalise("ProgramArea.DepartmentInformationLink", "General department information");
        var programAreaNameLiteral = (Literal) e.Item.FindControl("ProgramAreaName");
        programAreaNameLiteral.Text = programArea.Name;

        // Binds degrees to the child repeater
        var degrees = _programAreaDegrees.Where(pad => pad.ProgramAreaId == programArea.Id).ToList();
        var degreeIds = degrees.Select(d => d.DegreeId).ToList();

        var jobDegrees = _degrees.Where(jd => degreeIds.Contains(jd.DegreeId) && jd.ProgramAreaId == programArea.Id).OrderBy(jd => jd.DegreeName).ThenBy(jd => (int)jd.EducationLevel).ToList();

        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");
        degreeEducationLevelRepeater.DataSource = jobDegrees;

        degreeEducationLevelRepeater.DataBind();

        var sortButton = (ImageButton)e.Item.FindControl("DegreeEducationLevelSort");
        sortButton.CommandArgument = _programAreaIndex.ToString(CultureInfo.InvariantCulture);
        SetViewStateValue(string.Concat("DegreesBySchool:Degrees:", _programAreaIndex), jobDegrees);
        _programAreaIndex++;
      }
    }

    /// <summary>
    /// Fires when the sort button is clicked to sort the degree list
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Command Event Arguments (holds sort order)</param>
    protected void DegreeEducationLevelSort_Click(object sender, CommandEventArgs e)
    {
      var sortButton = (ImageButton) sender;
      var programAreaIndex = int.Parse(sortButton.CommandArgument);
      var jobDegrees = GetViewStateValue<List<ProgramAreaDegreeEducationLevelViewDto>>(string.Concat("DegreesBySchool:Degrees:", programAreaIndex));
      var degreeRepeater = (Repeater)ProgramAreaRepeater.Items[programAreaIndex].FindControl("DegreeEducationLevelRepeater");
      
      if (sortButton.CommandName == "SortDESC")
      {
        degreeRepeater.DataSource = jobDegrees.OrderBy(jd => jd.DegreeName).ThenBy(jd => jd.EducationLevel).ToList();
        sortButton.CommandName = "SortASC";
        sortButton.ImageUrl = UrlBuilder.SortDown();
      }
      else
      {
        degreeRepeater.DataSource = jobDegrees.OrderByDescending(jd => jd.DegreeName).ThenBy(jd => jd.EducationLevel).ToList();
        sortButton.CommandName = "SortDESC";
        sortButton.ImageUrl = UrlBuilder.SortUp();
      }
      degreeRepeater.DataBind();
    }

    /// <summary>
    /// Binds an individual degree to the repeater
    /// </summary>
    /// <param name="sender">The repeater raising the event</param>
    /// <param name="e">The event argument containing details of the item</param>
    protected void DegreeEducationLevelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var jobDegree = (ProgramAreaDegreeEducationLevelViewDto)e.Item.DataItem;

        // Binds the link button to the degree name
        var linkButton = (LinkButton)e.Item.FindControl("DegreeEducationLevelLink");
        linkButton.CommandArgument = jobDegree.DegreeEducationLevelId.ToString(CultureInfo.InvariantCulture);
        linkButton.Text = jobDegree.DegreeName;

        // Binds the label to the level
        var label = (Label)e.Item.FindControl("DegreeEducationLevelLabel");
        label.Text = jobDegree.EducationLevel.ToString();
      }
    }

    /// <summary>
    /// Fires when the 'degree education level' item is clicked
    /// </summary>
    /// <param name="sender">The link button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void DegreeEducationLevelRepeater_ItemCommand(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "ViewProgramAreaDegree":
        case "ViewDegreeCertification":
          OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));

          break;
      }
    }
  }
}