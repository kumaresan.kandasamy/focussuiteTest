#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models.Career;
using Focus.Services.Core;
using Framework.Core;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.CareerExplorer.Code;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class InternshipModal : ExplorerModalControl
	{
		public InternshipCategoryDto Internship
		{
			get { return GetViewStateValue<InternshipCategoryDto>("InternshipModal:Employer"); }
			set { SetViewStateValue("InternshipModal:Employer", value); }
		}

		public ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue<ExplorerCriteria>("InternshipModal:ReportCriteria"); }
			set { SetViewStateValue("InternshipModal:ReportCriteria", value); }
		}

		public List<LightboxBreadcrumb> Breadcrumbs
		{
			get { return GetViewStateValue("InternshipModal:Breadcrumb", new List<LightboxBreadcrumb>()); }
			set { SetViewStateValue("InternshipModal:Breadcrumb", value); }
		}

		private List<InternshipJobTitleDto> Jobs
		{
			get { return GetViewStateValue("InternshipModal:Jobs", new List<InternshipJobTitleDto>()); }
			set { SetViewStateValue("InternshipModal:Jobs", value); }
		}

		private int _rank = 1;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      ReadyToMakeAMovePlaceHolder.Visible = ViewCurrentPostingsLinkButton.Visible = (App.Settings.Module != FocusModules.Explorer);
		}

	  /// <summary>
    /// Shows the specified internship category id.
    /// </summary>
    /// <param name="internshipCategoryId">The internship category id.</param>
    /// <param name="criteria">The criteria.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="printFormat">The format to print the modal</param>
    public void Show(long internshipCategoryId, ExplorerCriteria criteria, List<LightboxBreadcrumb> breadcrumbs, ModalPrintFormat printFormat = ModalPrintFormat.None)
		{
			var internship = ServiceClientLocator.ExplorerClient(App).GetInternship(internshipCategoryId);

			Internship = internship;

		  criteria.InternshipCategoryId = Internship.Id;
		  criteria.InternshipCategory = Internship.Name;

      ReportCriteria = criteria;
      ReportCriteria.ReportType = ReportTypes.Internship;
			Breadcrumbs = breadcrumbs;
      CriteriaFilter.Bind(ReportTypes.Internship, criteria);

      ModalHeaderOptions.BindBreadcrumbs(Breadcrumbs);
			//if (breadcrumbs != null && breadcrumbs.Count == 3)
			//  Page.ClientScript.RegisterStartupScript(this.GetType(), "DisableLightboxModalLinks",
			//                                          String.Format(
			//                                            "DisableLightboxModalLinks('internshipLightBox','{0}');",
			//                                            CodeLocalise("Modal.BreadcrumbLimitMessage",
			//                                                         "The application only allows for three sets of data to be open at any one time.")),
			//                                          true);

			InternshipNameLabel.Text = internship.Name;
			//LocationLabel.Text = String.Format("({0})", ServiceClientLocator.ExplorerClient(App).GetStateAreaName(criteria.StateAreaId));
      TopInternshipsTitleLabel.Text = CodeLocalise("TopInternship.Title", "Top Types of {0} Internships Nationwide", internship.Name);

			InternshipSkillList.ReportCriteria = criteria;
			InternshipSkillList.ListSize = 20;
			InternshipSkillList.InternshipCategoryId = internshipCategoryId;
			InternshipSkillList.BindList(true, SkillTypes.Specialized);

      TopInternshipsDetailLabel.Text = InternshipSkillList.GetSkills(InternshipSkillList.SkillsListMode).Count > 0 
				? CodeLocalise("TopIntership.Detail.Results", "Top internships requiring {0} skills over the last 12 months:", internship.Name) 
				: CodeLocalise("TopIntership.Detail.NoResults", "While there may have been numerous internships for these skills in recent months, no internship has been advertised in your area.");

			InternshipEmployerList.ReportCriteria = criteria;
			InternshipEmployerList.InternshipCategoryId = internshipCategoryId;
			InternshipEmployerList.BindList();

			var jobs = ServiceClientLocator.ExplorerClient(App).GetInternshipJobs(internshipCategoryId, 10);

			Jobs = jobs;

			InternshipJobRepeater.DataSource = jobs;
			InternshipJobRepeater.DataBind();

      if (App.Settings.Module != FocusModules.Explorer)
      {
        if (App.Settings.ExplorerReadyToMakeAMoveBubble)
        {
          ReadyToMakeAMovePlaceHolder.Visible = true;
          ViewCurrentPositionsLiteral.Text = HtmlLocalise("SeeCurrentPostingsForJob.Text", "See current postings for<br />this internship �");
        }
        else
        {
          ReadyToMakeAMovePlaceHolder.Visible = false;
          ViewCurrentPositionsLiteral.Text = HtmlLocalise("ReadySeeCurrentPostingsForJob.Text", "Ready to make a move?<br /><br />See current postings for<br />this internship �");
        }
      }

			ModalPopup.Show();

			if (printFormat != ModalPrintFormat.None)
        PrintModal(printFormat);
		}

    /// <summary>
    /// Handles the ItemDataBound event of the InternshipJobRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void InternshipJobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var internshipJob = (InternshipJobTitleDto)e.Item.DataItem;

				var countLiteral = (Literal)e.Item.FindControl("JobRankLiteral");
				countLiteral.Text = _rank.ToString();

				var openingsLiteral = (Literal)e.Item.FindControl("JobOpeningsLiteral");
				openingsLiteral.Text = String.Format("({0}{1})", internshipJob.Frequency.ToString("#,#"),
																						 _rank == 1 ? " openings" : String.Empty);

				_rank++;
			}
		}

    /// <summary>
    /// Handles the Click event of the BookmarkLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void BookmarkLinkButton_Click(object sender, EventArgs e)
		{
			OnBookmarkClicked(new LightboxEventArgs
			                  	{
			                  		CommandName = "BookmarkInternship",
			                  		CommandArgument = new LightboxEventCommandArguement
			                  		                  	{
			                  		                  		Id = Internship.Id.Value,
			                  		                  		Breadcrumbs = Breadcrumbs,
			                  		                  		LightboxName = InternshipNameLabel.Text,
                                                  LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                  		                  	}
			                  	});
		}

    /// <summary>
    /// Handles the Click event of the SendEmailImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendEmailImageButton_Click(object sender, EventArgs e)
		{
			OnSendEmailClicked(new LightboxEventArgs
			                   	{
			                   		CommandName = "SendInternshipEmail",
			                   		CommandArgument = new LightboxEventCommandArguement
			                   		                  	{
			                   		                  		Id = Internship.Id.Value,
			                   		                  		Breadcrumbs = Breadcrumbs,
			                   		                  		LightboxName = InternshipNameLabel.Text,
                                                  LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                   		                  	}
			                   	});
		}

    /// <summary>
    /// Handles the Click event of the PrintImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalHeaderOptions_OnPrintCommand(object sender, CommandEventArgs eventArgs)
    {
      var printFormat = (ModalPrintFormat)Enum.Parse(typeof(ModalPrintFormat), eventArgs.CommandArgument.ToString(), true);
			if (ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
        PrintModal(printFormat);

				ModalPopup.Show();
			}
			else
			{
				OnPrintClicked(new LightboxEventArgs
				               	{
				               		CommandName = "PrintInternship",
				               		CommandArgument = new LightboxEventCommandArguement
				               		                  	{
				               		                  		Id = Internship.Id.Value,
                                                Breadcrumbs = Breadcrumbs,
                                                LightboxName = InternshipNameLabel.Text,
                                                LightboxLocation = "",//LocationLabel.Text
                                                CriteriaHolder = ReportCriteria,
                                                PrintFormat = printFormat
				               		                  	}
				               	});
			}
		}

    /// <summary>
    /// Handles the Click event of the CloseButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Click(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

    /// <summary>
    /// Handles the ItemClicked event of the InternshipModalBreadcrumb control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void InternshipModalBreadcrumb_ItemClicked(object sender, CommandEventArgs e)
		{
			long itemId;
			if (long.TryParse(e.CommandArgument.ToString(), out itemId))
			{
				OnItemClicked(new CommandEventArgs(e.CommandName,
																					 new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = Breadcrumbs, MovingBack = true}));
			}
		}

    /// <summary>
    /// Handles the Cancelled event of the Filter control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.FilterEventArgs"/> instance containing the event data.</param>
    protected void Filter_Cancelled(object sender, FilterEventArgs e)
    {
      var oldCriteria = e.CommandArgument.OldCriteria;
      var newCriteria = e.CommandArgument.NewCriteria;

      var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.Internship, LinkId = Internship.Id.GetValueOrDefault(), LinkText = Internship.Name, Criteria = oldCriteria };
      var breadcrumbs = Breadcrumbs;
      breadcrumbs.Add(breadcrumb);

      OnItemClicked(new CommandEventArgs("ViewInternship",
                                         new LightboxEventCommandArguement
                                         {
                                           Id = oldCriteria.InternshipCategoryId.GetValueOrDefault(),
                                           Breadcrumbs = breadcrumbs,
                                           CriteriaHolder = newCriteria
                                         }));
    }


    /// <summary>
    /// Handles the ItemClicked event of the ModalList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ViewSkill":
				case "ViewEmployer":
					long itemId;
					if (long.TryParse(e.CommandArgument.ToString(), out itemId))
					{
						var breadcrumb = new LightboxBreadcrumb
						{
							LinkType = ReportTypes.Internship,
							LinkId = Internship.Id.Value,
							LinkText = Internship.Name,
              Criteria = ReportCriteria
						};
						var breadcrumbs = Breadcrumbs;
						breadcrumbs.Add(breadcrumb);

						OnItemClicked(new CommandEventArgs(e.CommandName,
																							 new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = breadcrumbs }));
					}
					break;
			}
		}

    /// <summary>
    /// Prints the modal.
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintModal(ModalPrintFormat printFormat = ModalPrintFormat.Html)
		{
			var printModel = new ExplorerInternshipPrintModel
				                 {
				Name = InternshipNameLabel.Text,
				Location = "",//LocationLabel.Text,
				Employers = InternshipEmployerList.Employers,
        Jobs = Jobs,
        ReportCriteria = ReportCriteria
			};

			var skills = InternshipSkillList.GetSkills(SkillListSimple.SkillsListModes.Internship);
			printModel.SpecializedSkills = skills.Where(x => x.SkillType == SkillTypes.Specialized).ToList();
			printModel.SoftwareSkills = skills.Where(x => x.SkillType == SkillTypes.Software).ToList();
			printModel.FoundationSkills = skills.Where(x => x.SkillType == SkillTypes.Foundation).ToList();

			const string sessionKey = "Explorer:InternshipPrintModel";
			App.SetSessionValue(sessionKey, printModel);

      OpenPrintWindow(ReportTypes.Internship, printFormat);
		}

    /// <summary>
    /// Handles the Click event of the ViewCurrentPostingsLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ViewCurrentPostingsLinkButton_Click(object sender, EventArgs e)
    {
      var jobListSearchCriteria = new SearchCriteria();
      var stateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(ReportCriteria.StateAreaId);

      jobListSearchCriteria.InternshipCriteria = new InternshipCriteria
                                                   {
                                                     Internship = FilterTypes.ShowOnly,
                                                     InternshipCategories = new List<long> {Internship.Id.GetValueOrDefault()}
                                                   };

      if (stateArea.StateCode.IsNotNullOrEmpty())
      {
				jobListSearchCriteria.JobLocationCriteria = new JobLocationCriteria
      	                     	{
      	                     		Area = new AreaCriteria 
      	                     		       	{
      	                     		       		MsaId = stateArea.AreaSortOrder == 0 ? (long?) null : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.StateMetropolitanStatisticalAreas).Where(x => x.ExternalId == stateArea.AreaCode).Select(x => x.Id).FirstOrDefault(),
      	                     		       		StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.ExternalId == stateArea.StateCode).Select(x => x.Id).FirstOrDefault()
      	                     		       	},
      	                     		SelectedStateInCriteria = stateArea.StateCode
      	                     	};
			}

      jobListSearchCriteria.RequiredResultCriteria = new RequiredResultCriteria
      {
        DocumentsToSearch = DocumentType.Posting,
        IncludeData = null,
        MinimumStarMatch = StarMatching.None,
        MaximumDocumentCount = App.Settings.MaximumNoDocumentToReturnInJobAlert
      };

      jobListSearchCriteria.PostingAgeCriteria = new PostingAgeCriteria
      {
        PostingAgeInDays = App.Settings.JobSearchPostingDate
      };

      App.SetSessionValue("Career:SearchCriteria", jobListSearchCriteria);

      App.SetCookieValue("Career:ManualSearch", "true");
      Response.RedirectToRoute("JobSearchResults");
    }
	}
}