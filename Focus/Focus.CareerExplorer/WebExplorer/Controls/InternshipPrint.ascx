<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternshipPrint.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.InternshipPrint" %>
<%@ Register src="Criteria.ascx" tagName="CriteriaFilter" tagPrefix="uc" %>

<div class="staticAccordion">
	
  <table class="lightboxHeading" role='presentation'>
    <tr>
      <td><h1><asp:Label ID="InternshipNameLabel" runat="server" /> <%= HtmlLocalise("Internships.Label", "Internships") %></h1></td>
    </tr>
    <tr>
      <td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" HideCloseIcons="True" /></td>
    </tr>
  </table>

  <table>
    <tr>
      <td><asp:Label ID="InternshipBlurbLabel" runat="server" />
      </td>
    </tr>
  </table>

  <table width="100%" class="numberedAccordion" role='presentation'>
    <tr class="accordionTitle">
      <td class="column1">1</td>
      <td class="column2"><%= HtmlLocalise("TopSkills.Header", "In-demand skills: specialized, software, foundation")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
			<td></td>
			<td>
        <!-- skill list -->
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="SpecializedSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="SpecializedSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="SpecializedSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="SpecializedSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			  <table width="100%" class="genericTable withHeader" role='presentation'>
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="SoftwareSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="SoftwareSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="SoftwareSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="SoftwareSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			  <table width="100%" class="genericTable withHeader" role='presentation'>
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="FoundationSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="FoundationSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="FoundationSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%" role='presentation'>
								<asp:Repeater ID="FoundationSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			</td>
			<td></td>
    </tr>
    <tr class="accordionTitle">
      <td class="column1">2</td>
      <td class="column2"><%= HtmlLocalise("TopEmployers.Header", "In-demand #BUSINESSES#:LOWER")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
 			<td></td>
			<td>
        <!-- employer list -->
			  <table width="100%" class="genericTable" role='presentation'>
				  <tr>
					  <td align="left" valign="top">
						  <asp:Repeater ID="EmployerColumnOneRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
							  <HeaderTemplate>
								  <table width="100%" role='presentation'> 
							  </HeaderTemplate>
							  <ItemTemplate>
								  <tr>
									  <td valign="top">
										  <strong><asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp; <%# Eval("EmployerName") %></strong>
									  </td>
								  </tr>
							  </ItemTemplate>
							  <FooterTemplate>
								  </table>
							  </FooterTemplate>
						  </asp:Repeater>
					  </td>
					  <asp:PlaceHolder ID="EmployerColumnTwoPlaceHolder" runat="server">
					  <td width="50%" align="left" valign="top">
						  <asp:Repeater ID="EmployerColumnTwoRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
							  <HeaderTemplate>
								  <table width="100%" role='presentation'>
							  </HeaderTemplate>
							  <ItemTemplate>
								  <tr>
									  <td valign="top">
										  <strong><asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp; <%# Eval("EmployerName") %>&nbsp; <asp:Literal ID="EmployerOpeningsLiteral" runat="server" /></strong>
									  </td>
								  </tr>
							  </ItemTemplate>
							  <FooterTemplate>
								  </table>
							  </FooterTemplate>
						  </asp:Repeater>
					  </td>
					  </asp:PlaceHolder>
				  </tr>
			  </table>
			</td>
			<td></td>
    </tr>
    <tr class="accordionTitle">
      <td class="column1">3</td>
      <td class="column2"><asp:Label ID="TopInternshipsTitleLabel" runat="server"></asp:Label></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
      <td></td>
      <td>
			  <table width="100%" class="genericTable" role='presentation'>
          <asp:Repeater ID="JobRepeater" runat="server" OnItemDataBound="JobRepeater_ItemDataBound">
            <ItemTemplate>
              <tr>
                <td>
									<strong><asp:Literal ID="JobRankLiteral" runat="server" />.&nbsp; <%# Eval("JobTitle") %>&nbsp; <asp:Literal ID="JobOpeningsLiteral" runat="server" /></strong>
                </td>
              </tr>
            </ItemTemplate>
          </asp:Repeater>
        </table>
      </td>
			<td></td>
    </tr>
  </table>

</div>
