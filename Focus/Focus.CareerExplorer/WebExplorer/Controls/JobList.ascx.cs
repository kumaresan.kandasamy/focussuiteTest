#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Framework.Core;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public enum JobListModes
	{
		Report,
		Similar,
		DegreeEducationLevel,
    ProgramAreaDegree,
		Skill,
    CareerMovesMain,
    CareerMovesOther,
    MilitaryOccupation3Star,
    MilitaryOccupation2Star,
    CareerArea
	}

	public partial class JobList : ExplorerListControl
	{
		public JobListModes JobListMode
    {
      get { return GetViewStateValue("JobList:JobListMode", JobListModes.Report); }
      set { SetViewStateValue("JobList:JobListMode", value); }
    }

		public long JobId { get; set; }
    public long DegreeEducationLevelId { get; set; }
    public long ProgramAreaId { get; set; }
    public long DegreeId { get; set; }
    public long SkillId { get; set; }
    public long CareerAreaId { get; set; }

    public int JobDegreeTierLevel
    {
      get { return GetViewStateValue("JobList:JobDegreeTierLevel", 0); }
      set { SetViewStateValue("JobList:JobDegreeTierLevel", value); }
    }
    public override int RecordCount { get { return JobListView != null ? JobListView.Items.Count : 0; } }
	  public JobCareerPathModel CareerMoves;
    public List<OnetCode> MilitaryOccupationROnetCodes { get; set; }
    public bool EnablePaging { get; set; }

    public string ExtraHeaderText { get; set; }

		protected string HiringDemandToolTip { get; set; }

    /// <summary>
    /// Gets the job reports.
    /// </summary>
    /// <param name="jobListMode">The job list mode.</param>
    /// <param name="jobDegreeTier">The job degree tier.</param>
    /// <returns></returns>
    public List<ExplorerJobReportView> GetJobReports(JobListModes jobListMode, int jobDegreeTier = 0)
		{
      var sessionKey = "Explorer:JobReports:" + jobListMode.ToString() + (jobDegreeTier == 0 ? string.Empty : jobDegreeTier.ToString(CultureInfo.InvariantCulture));
      return App.GetSessionValue<List<ExplorerJobReportView>>(sessionKey);			
		}

    /// <summary>
    /// Sets the job reports.
    /// </summary>
    /// <param name="jobListMode">The job list mode.</param>
    /// <param name="jobReports">The job reports.</param>
    /// <param name="jobDegreeTier">The job degree tier.</param>
    private void SetJobReports(JobListModes jobListMode, List<ExplorerJobReportView> jobReports, int jobDegreeTier = 0)
		{
      var sessionKey = "Explorer:JobReports:" + jobListMode.ToString() + (jobDegreeTier == 0 ? string.Empty : jobDegreeTier.ToString(CultureInfo.InvariantCulture));
			App.SetSessionValue(sessionKey, jobReports);
		}

		private int _rank = 1;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Handles the PreRender event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
      Page.ClientScript.RegisterClientScriptBlock(GetType(), "JobList_JobNameWrapping", @"
function JobList_JobNameWrapping(panelId) {
  $('#' + panelId).find('.reportColumnInlineTooltip:visible').each(function (index, value) {
    if ($(value).position().left < 70) {
      var link = $(value).closest('td').find('a');
      var words = link.text().split(' ');
      if (words.length > 1) {
        words.splice(words.length - 1, 0, '<br />');
        link.html(words.join(' '));
      }
    }
  });
}", true);
    }

		/// <summary>
		/// Handles the LayoutCreated event of the JobListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void JobListView_LayoutCreated(object sender, EventArgs e)
		{
			// the list is initially ordered by hiring trend, so assign the correct sort image on initial load
			var hiringTrendSortImage = (Image)JobListView.FindControl("HiringTrendSortImage");
			hiringTrendSortImage.ImageUrl = UrlBuilder.SortDown();
		}

    /// <summary>
    /// Binds the list.
    /// </summary>
    /// <param name="forceReload">if set to <c>true</c> [force reload].</param>
		public override void BindList(bool forceReload)
		{
			if (forceReload)
        SetJobReports(JobListMode, null, JobDegreeTierLevel);

      JobListView.DataBind();

      var pager = (DataPager)JobListView.FindControl("JobListPager");
      if (pager != null)
      {
        pager.Visible = EnablePaging;
        if (!pager.Visible)
        {
          pager.PageSize = ListSize;
          _rank = 1;
        }

        if (forceReload)
          pager.SetPageProperties(0, pager.PageSize, false);
      }
      
      if (ExtraHeaderText.IsNotNullOrEmpty())
      {
        var extraHeaderRow = (HtmlTableRow)JobListView.FindControl("ExtraHeaderRow");
        extraHeaderRow.Visible = true;

        var extraHeaderLiteral = (Literal) JobListView.FindControl("ExtraHeaderLiteral");
        extraHeaderLiteral.Text = ExtraHeaderText;
      }

			var hiringDemandHeaderTooltip = (HtmlGenericControl)JobListView.FindControl("HiringDemandHeaderTooltip");
			if (hiringDemandHeaderTooltip != null)
			{
				hiringDemandHeaderTooltip.Attributes["title"] = CodeLocalise("HiringDemand.TooltipText",
																											 "An #BUSINESS#:LOWER�s need for workers to fill their jobs is known as hiring demand. Select hiring demand to see the number of jobs, either nationally or by your geographic search area that #BUSINESSES#:LOWER posted over the last 12 months. Hiring demand is rated as low, medium, high, or very high for the area in your geographic search.");
			}
		}

    /// <summary>
    /// Gets the job reports.
    /// </summary>
    /// <param name="jobListMode">The job list mode.</param>
    /// <param name="reportCriteria">The report criteria.</param>
    /// <param name="listSize">Size of the list.</param>
    /// <param name="jobId">The job id.</param>
    /// <param name="degreeEducationLevelId">The degree education level id.</param>
    /// <param name="programAreaId">The program area id.</param>
    /// <param name="degreeId">The degree id.</param>
    /// <param name="skillId">The skill id.</param>
    /// <param name="careerMoves">The career moves.</param>
    /// <param name="militaryOccupationROnetCodes">The military occupation R onet codes.</param>
    /// <param name="jobDegreeTierLevel">The job degree tier level.</param>
    /// <param name="orderBy">The order by.</param>
    /// <returns></returns>
    public List<ExplorerJobReportView> GetJobReports(JobListModes jobListMode, ExplorerCriteria reportCriteria, int listSize, long jobId, long degreeEducationLevelId, long programAreaId, long degreeId, long skillId, JobCareerPathModel careerMoves, List<OnetCode> militaryOccupationROnetCodes, int jobDegreeTierLevel, long careerAreaId, string orderBy)
		{
			var doSort = true;
			// get job reports from view state
      var jobReports = GetJobReports(jobListMode, jobDegreeTierLevel);
			// check if anything returned
			if (jobReports == null || jobReports.Count == 0)
			{
				// check if we have report criteria
				if (reportCriteria != null)
				{
					// get job reports and store in view state
					switch (jobListMode)
					{
						case JobListModes.Report:
							jobReports = ServiceClientLocator.ExplorerClient(App).GetJobReport(reportCriteria, listSize);
							break;

						case JobListModes.Similar:
					    reportCriteria.CareerAreaJobId = jobId;
							jobReports = ServiceClientLocator.ExplorerClient(App).GetSimilarJobReport(reportCriteria, listSize);
							break;

						case JobListModes.DegreeEducationLevel:
					    reportCriteria.DegreeEducationLevelId = degreeEducationLevelId;
              jobReports = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevelJobReport(reportCriteria, listSize, jobDegreeTierLevel);
							break;

            case JobListModes.ProgramAreaDegree:
              reportCriteria.ProgramAreaId = programAreaId;
              reportCriteria.DegreeId = degreeId;
              jobReports = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevelJobReport(reportCriteria, listSize, 0);
              break;

						case JobListModes.Skill:
					    reportCriteria.SkillId = skillId;
							jobReports = ServiceClientLocator.ExplorerClient(App).GetSkillJobReport(reportCriteria, listSize, null);
							break;

            case JobListModes.CareerMovesMain:
              jobReports = careerMoves.CareerPathTo.Select(cp => cp.JobReportDetails).ToList();
              break;

            case JobListModes.CareerMovesOther:
              var usedIds = careerMoves.CareerPathTo.Select(cp => cp.Job.Id).ToList();
              usedIds.Add(careerMoves.Job.Id);

              var otherCareerPaths = new List<JobCareerPathModel>();
              foreach (var careerPath in careerMoves.CareerPathTo)
              {
                var otherJobs = careerPath.CareerPathTo.Where(cp => !usedIds.Contains(cp.Job.Id)).ToList();
                otherCareerPaths.AddRange(otherJobs);

                usedIds.AddRange(otherJobs.Select(oj => oj.Job.Id));
              }

              otherCareerPaths = otherCareerPaths.OrderBy(cp => cp.Rank).Take(4).ToList();
              jobReports = otherCareerPaths.Select(cp => cp.JobReportDetails).ToList();
              break;

            case JobListModes.MilitaryOccupation3Star:
              reportCriteria.CareerAreaMilitaryOccupationROnetCodes = militaryOccupationROnetCodes.Where(o => o.StarRating == 3).Select(o => o.RonetCode).ToList();
							jobReports = ServiceClientLocator.ExplorerClient(App).GetJobReport(reportCriteria, listSize, false);
							break;

            case JobListModes.MilitaryOccupation2Star:
              reportCriteria.CareerAreaMilitaryOccupationROnetCodes = militaryOccupationROnetCodes.Where(o => o.StarRating == 2).Select(o => o.RonetCode).ToList();
              jobReports = ServiceClientLocator.ExplorerClient(App).GetJobReport(reportCriteria, listSize, false);
              break;

            case JobListModes.CareerArea:
              reportCriteria.CareerAreaId = careerAreaId;
              jobReports = ServiceClientLocator.ExplorerClient(App).GetJobReport(reportCriteria, listSize, false);
              break;
					}
					SetJobReports(jobListMode, jobReports, jobDegreeTierLevel);
					doSort = false;
				}
				else
				{
          jobReports = new List<ExplorerJobReportView>();
					doSort = false;
				}
			}

			if (doSort)
			{
				if (String.IsNullOrEmpty(orderBy))
					orderBy = "HiringDemand DESC";

				bool sortDescending = false;
				if (orderBy.EndsWith(" DESC"))
				{
					sortDescending = true;
					orderBy = orderBy.Substring(0, orderBy.LastIndexOf(" DESC", StringComparison.InvariantCulture));
				}

				switch (orderBy)
				{
					case "JobName":
						jobReports = sortDescending 
              ? jobReports.OrderByDescending(x => x.ReportData.Name).ToList() 
              : jobReports.OrderBy(x => x.ReportData.Name).ToList();
						break;

					case "HiringDemand":
            jobReports = sortDescending 
              ? jobReports.OrderByDescending(x => x.ReportData.HiringDemand).ThenByDescending(x => x.ReportData.Name).ToList()
              : jobReports.OrderBy(x => x.ReportData.HiringDemand).ThenBy(x => x.ReportData.Name).ToList();
						break;

					case "SalaryTrendAverage":
            jobReports = sortDescending 
              ? jobReports.OrderByDescending(x => x.ReportData.SalaryTrendAverage).ThenByDescending(x => x.ReportData.Name).ToList()
              : jobReports.OrderBy(x => x.ReportData.SalaryTrendAverage).ThenBy(x => x.ReportData.Name).ToList();
						break;
				}				
			}
			return jobReports;
      
		}

    /// <summary>
    /// Handles the Selecting event of the JobListViewODS control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs"/> instance containing the event data.</param>
		protected void JobListViewODS_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
		{
			e.InputParameters["jobListMode"] = JobListMode.ToString();
			e.InputParameters["reportCriteria"] = ReportCriteria;
			e.InputParameters["listSize"] = ListSize;

			if (JobListMode == JobListModes.Similar)
				e.InputParameters["jobId"] = JobId;

      if (JobListMode == JobListModes.DegreeEducationLevel)
      {
        e.InputParameters["degreeEducationLevelId"] = DegreeEducationLevelId;
        e.InputParameters["jobDegreeTierLevel"] = JobDegreeTierLevel;
      }

		  if (JobListMode == JobListModes.ProgramAreaDegree)
		  {
        e.InputParameters["programAreaId"] = ProgramAreaId;
        e.InputParameters["degreeId"] = DegreeId;
      }

		  if (JobListMode == JobListModes.Skill)
				e.InputParameters["skillId"] = SkillId;

		  if (JobListMode == JobListModes.CareerMovesMain || JobListMode == JobListModes.CareerMovesOther)
		    e.InputParameters["careerMoves"] = CareerMoves;

      if (JobListMode == JobListModes.MilitaryOccupation3Star || JobListMode == JobListModes.MilitaryOccupation2Star)
        e.InputParameters["militaryOccupationROnetCodes"] = MilitaryOccupationROnetCodes;

      if (JobListMode == JobListModes.CareerArea)
        e.InputParameters["careerAreaId"] = CareerAreaId;
    }

    /// <summary>
    /// Handles the ItemDataBound event of the JobListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void JobListView_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			if (e.Item.ItemType == ListViewItemType.DataItem)
			{
        var jobReport = (ExplorerJobReportView)e.Item.DataItem;

        // Get current page index to set correct job count label
			  var pager = (DataPager)JobListView.FindControl("JobListPager");
			  var pageNumber = 0;
        if (pager != null)
          pageNumber = pager.StartRowIndex;

				var rankLiteral = (Literal) e.Item.FindControl("RankLiteral");
        rankLiteral.Text = (pageNumber +_rank).ToString(CultureInfo.InvariantCulture);
				_rank++;

				if (App.Settings.ExplorerGoogleEventTrackingEnabled && JobListMode == JobListModes.Report)
				{
					var jobNameLinkButton = (LinkButton)e.Item.FindControl("JobNameLinkButton");
					jobNameLinkButton.OnClientClick = String.Format("javascript:GoogleTrackEvent('/report/job', 'ExploreReport', 'Job', '{0}');",
					                                                jobReport.ReportData.Name.Replace("'", "\\'"));
				}

				var hiringTrendTooltip = (HtmlGenericControl) e.Item.FindControl("HiringTrendToolTip");
        switch (jobReport.ReportData.HiringTrend)
				{
					case "Very High":
						hiringTrendTooltip.Attributes.Add("title", CodeLocalise("HiringTrendVeryHigh.TooltipText", "Based on #BUSINESS#:LOWER demand, this job ranks in the top 20-30 occupations of all occupations that are available."));
						break;

					case "High":
						hiringTrendTooltip.Attributes.Add("title", CodeLocalise("HiringTrendHigh.TooltipText", "Based on #BUSINESS#:LOWER demand, this job ranks in the top 70-80 occupations of all occupations that are available."));
						break;

					case "Medium":
						hiringTrendTooltip.Attributes.Add("title", CodeLocalise("HiringTrendMedium.TooltipText", "Based on #BUSINESS#:LOWER demand, this job ranks in the lower tier of the top half of all occupations that are available."));
						break;

					case "Low":
						hiringTrendTooltip.Attributes.Add("title", CodeLocalise("HiringTrendLow.TooltipText", "Based on demand, this job ranks in the bottom half of all occupations that are available."));
						break;

					default:
						hiringTrendTooltip.Attributes.Add("title", CodeLocalise("HiringTrendDefault.TooltipText", "Not applicable."));
						break;
				}

				var salaryImage = (Image) e.Item.FindControl("SalaryImage");
				var salaryText = (Literal)e.Item.FindControl("SalaryText");
        switch (jobReport.ReportData.SalaryTrend)
				{
					case "$$$":
						salaryImage.ImageUrl = UrlBuilder.JobSalaryHighImage();
						salaryImage.ToolTip = CodeLocalise("SalaryHigh.TooltipText", "In the search area you selected, the salary level for this job is high. According to the U.S. Bureau of Labor Statistics, the median salary is higher than the 75th percentile of the overall average salary in this area.");
						break;

					case "$$":
						salaryImage.ImageUrl = UrlBuilder.JobSalaryMediumImage();
						salaryImage.ToolTip = CodeLocalise("SalaryMedium.TooltipText", "In the search area you selected, the salary for this job is medium. According to the U.S. Bureau of Labor Statistics, the median salary is higher than the 25th percentile, but lower than the 75th percentile for overall average salary in this area.");
						break;

					case "$":
						salaryImage.ImageUrl = UrlBuilder.JobSalaryLowImage();
						salaryImage.ToolTip = CodeLocalise("SalaryLow.TooltipText", "In the search area you selected, the salary for this job is low. According to the U.S. Bureau of Labor Statistics, the median salary is lower than the 25th percentile as compared with the overall average salary in this area.");
						break;

					default:
				    salaryText.Visible = true;
				    salaryText.Text = CodeLocalise("SalaryText.NA", "N/A");
						salaryImage.Visible = false;
						break;
				}

        if (App.Settings.Theme == FocusThemes.Education && jobReport.RelatedDegreeAvailableAtCurrentSchool)
        {
          e.Item.FindControl("DegreeProgramPanel").Visible = true;
        }

			  if (jobReport.ReportData.StarterJob.HasValue)
        {
          e.Item.FindControl("StarterJobPanel").Visible = true;
          var starterJobTootTipDiv = (HtmlGenericControl) e.Item.FindControl("StarterJobToolTipDiv");
          var starterJobToolTip = (HtmlGenericControl)e.Item.FindControl("StarterJobToolTipImage");
          if (jobReport.ReportData.StarterJob.Value == StarterJobLevel.Many)
          {
            starterJobTootTipDiv.Attributes.Add("class", "tooltipWithArrow starterJobMany");
            starterJobToolTip.Attributes.Add("title", CodeLocalise("StarterJobMany.TooltipText", "This occupation has substantial openings for new entrants with less than 2 years of work experience in this field."));
          }
          else if (jobReport.ReportData.StarterJob.Value == StarterJobLevel.Some)
          {
            starterJobTootTipDiv.Attributes.Add("class", "tooltipWithArrow starterJobSome");
            starterJobToolTip.Attributes.Add("title", CodeLocalise("StarterJobSome.TooltipText", "This occupation has substantial openings for new entrants with less than 3 years of work experience in this field."));
          }
        }
			}
		}

    /// <summary>
    /// Handles the ItemCommand event of the JobListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewCommandEventArgs"/> instance containing the event data.</param>
		protected void JobListView_ItemCommand(object sender, ListViewCommandEventArgs e)
		{
			if (e.CommandName == "ViewJob")
			{
				OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));
			}
		}

    /// <summary>
    /// Handles the Sorting event of the JobListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewSortEventArgs"/> instance containing the event data.</param>
		protected void JobListView_Sorting(object sender, ListViewSortEventArgs e)
		{
			var sortImageUrl = e.SortDirection == SortDirection.Ascending ? UrlBuilder.SortUp() : UrlBuilder.SortDown();

			var jobSortImage = (Image)JobListView.FindControl("JobSortImage");
			var hiringTrendSortImage = (Image)JobListView.FindControl("HiringTrendSortImage");
			var salarySortImage = (Image)JobListView.FindControl("SalarySortImage");

      // Reset pager to ensure numbering resets
      var pager = (DataPager)JobListView.FindControl("JobListPager");
      pager.SetPageProperties(0, pager.PageSize, false);


			switch (e.SortExpression)
			{
				case "JobName":
					jobSortImage.ImageUrl = sortImageUrl;
					jobSortImage.Visible = true;
					hiringTrendSortImage.Visible = false;
					salarySortImage.Visible = false;
					break;

				case "HiringDemand":
					jobSortImage.Visible = false;
					hiringTrendSortImage.ImageUrl = sortImageUrl;
					hiringTrendSortImage.Visible = true;
					salarySortImage.Visible = false;
					break;

				case "SalaryTrendAverage":
					jobSortImage.Visible = false;
					hiringTrendSortImage.Visible = false;
					salarySortImage.ImageUrl = sortImageUrl;
					salarySortImage.Visible = true;
					break;
			}
		}
	}
}