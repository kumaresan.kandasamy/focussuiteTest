﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class CareerMovesList : ExplorerListControl
	{
    public override int RecordCount
    {
      get
      {
        return (_jobId.HasValue || _militaryOccupationId.HasValue) ? _careersFound : -1;
      }
    }

    private long? _jobId;
    private long? _militaryOccupationId;
    private int _careersFound;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Binds the list.
    /// </summary>
    /// <param name="forceReload">if set to <c>true</c> [force reload].</param>
    public override void BindList(bool forceReload)
    {
      _jobId = ReportCriteria.CareerAreaJobId;
      _militaryOccupationId = ReportCriteria.CareerAreaMilitaryOccupationId;

      if (_jobId.HasValue)
      {
        var careerMoves = ServiceClientLocator.ExplorerClient(App).GetJobCareerPaths(_jobId.Value, ReportCriteria.StateAreaId);
        _careersFound = careerMoves.CareerPathTo.Count;

        JobListMain.JobListMode = JobListModes.CareerMovesMain;
        JobListMain.ReportCriteria = ReportCriteria;
        JobListMain.ListSize = ListSize;
        JobListMain.CareerMoves = careerMoves;
        JobListMain.BindList(true);

        JobListOther.JobListMode = JobListModes.CareerMovesOther;
        JobListOther.ReportCriteria = ReportCriteria;
        JobListOther.ListSize = ListSize;
        JobListOther.CareerMoves = careerMoves;
        JobListOther.BindList(true);

        MainLabel.Text = HtmlLocalise("CareerMoves.Header.CommonMoves", "Most common career moves for your job title");
        OtherLabel.Text = HtmlLocalise("CareerMoves.Header.OtherMoves", "Other career moves for your job title");
        CareerListPanel.Visible = true;
      }
      else if (_militaryOccupationId.HasValue)
      {
        var onetCodes = ServiceClientLocator.OccupationClient(App).GetMocOccupationCodes(_militaryOccupationId.Value);
        _careersFound = onetCodes.Count;

        JobListMain.JobListMode = JobListModes.MilitaryOccupation3Star;
        JobListMain.MilitaryOccupationROnetCodes = onetCodes;
        JobListMain.ReportCriteria = ReportCriteria;
        JobListMain.ListSize = ListSize;
        JobListMain.BindList(true);

        JobListOther.JobListMode = JobListModes.MilitaryOccupation2Star;
        JobListOther.MilitaryOccupationROnetCodes = onetCodes;
        JobListOther.ReportCriteria = ReportCriteria;
        JobListOther.ListSize = ListSize;
        JobListOther.BindList(true);

        MainLabel.Text = HtmlLocalise("CareerMoves.Header.3StarMatches", "Jobs related to your military occupation");
        OtherLabel.Text = HtmlLocalise("CareerMoves.Header.2StarMatches", "Other jobs to explore");
        CareerListPanel.Visible = true;
      }
      else
      {
        CareerListPanel.Visible = false;
      }
    }

    /// <summary>
    /// Fires when the job item is clicked
    /// </summary>
    /// <param name="sender">The link button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void JobsList_ItemCommand(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "ViewJob":
          OnItemClicked(new CommandEventArgs(e.CommandName, e.CommandArgument));

          break;
      }
    }
  }
}