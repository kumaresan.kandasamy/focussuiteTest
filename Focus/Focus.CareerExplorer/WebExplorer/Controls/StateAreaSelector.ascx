﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StateAreaSelector.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.StateAreaSelector" %>

<asp:Literal ID="StateBeforeHtmlLiteral" runat="server" ></asp:Literal>
<asp:DropDownList ID="StateDropDownList" runat="server" Width="200" Title = "State" ></asp:DropDownList>
<asp:RequiredFieldValidator ID="StateDropDownListRFV" runat="server" ControlToValidate="StateDropDownList" CssClass="error" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:Literal ID="StateAfterHtmlLiteral" runat="server" ></asp:Literal>
<asp:Literal ID="AreaBeforeHtmlLiteral" runat="server" ></asp:Literal>
<asp:DropDownList ID="AreaDropDownList" runat="server" Width="250" Title = "Area"></asp:DropDownList>
<asp:RequiredFieldValidator ID="AreaDropDownListRFV" runat="server" ControlToValidate="AreaDropDownList" CssClass="error" SetFocusOnError="true" Display="Dynamic"></asp:RequiredFieldValidator>
<asp:Literal ID="AreaAfterHtmlLiteral" runat="server" ></asp:Literal>
