<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillModal.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.SkillModal" %>

<%@ Register src="ModalHeaderOptions.ascx" tagName="ModalHeaderOptions" tagPrefix="uc" %>
<%@ Register src="JobList.ascx" tagName="SkillJobList" tagPrefix="uc" %>
<%@ Register src="EmployerList.ascx" tagName="SkillEmployerList" tagPrefix="uc" %>
<%@ Register src="Criteria.ascx" tagName="CriteriaFilter" tagPrefix="uc" %>
<%@ Register src="InternshipList.ascx" tagName="InternshipList" tagPrefix="uc" %>

<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" 
												TargetControlID="ModalDummyTarget"
												PopupControlID="ModalPanel" 
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display:none;">
	
  <div class="lightbox lightboxmodal" id="skillLightBox">
    
    <uc:ModalHeaderOptions ID="ModalHeaderOptions" runat="server"
        OnBreadcrumbItemClicked="SkillModalBreadcrumb_ItemClicked"
        OnBookmarkClicked="BookmarkLinkButton_Click"
        OnSendEmailClicked="SendEmailImageButton_Click"
        OnPrintCommand="ModalHeaderOptions_OnPrintCommand"
        OnCloseClicked="CloseButton_Click">
    </uc:ModalHeaderOptions>

		<div class="skill-scroll-pane">

    <table class="lightboxHeading" role="presentation">
      <tr><td><h2>
	      <span class="sr-only">Skill name</span>
	      <asp:Label ID="SkillNameLabel" runat="server" />
      </h2></td></tr>
      <tr class="lightboxHeading"><td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" OnFilterCancelled="Filter_Cancelled"/></td></tr>
    </table>

    <table width="100%" role="presentation">
      <tr class="modalLeftPanel">
        <td width="67.5%">
          <table width="100%" class="numberedAccordion" role="presentation">
            <tr class="multipleAccordionTitle2 on" data-joblist="<%=SkillJobList.ClientID %>">
              <td valign="top" width="10" class="column1">1</td>
							<td class="column2">
                <div style="min-height:22px;">
									<span class="toolTipNew" id="Section1ToolTip" runat="server"><a href="#"><asp:Literal runat="server" ID="Section1Title"></asp:Literal></a></span>
                </div>
              </td>
              <td class="column3" align="right" valign="top"><a class="show" href="#"><%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a></td>
            </tr>
            <tr class="accordionContent2 open">
              <td></td>
              <td>
                <p><asp:Label runat="server" ID="NoJobsLabel"></asp:Label></p>
                <uc:InternshipList ID="InternshipsList" runat="server" OnItemClicked="ModalList_ItemClicked" ListMode="Skill" ListSize="10" />
                <uc:SkillJobList ID="SkillJobList" runat="server" JobListMode="Skill" OnItemClicked="ModalList_ItemClicked" />
              </td>
              <td></td>
            </tr>
            <asp:PlaceHolder runat="server" ID="SkillsDegreePlaceHolder">
              <tr class="multipleAccordionTitle2">
                <td class="column1">2</td>
                <td class="column2"><a href="#"><%= HtmlLocalise("TopDegreesAndCertificates.Header", "Top degrees & certificates for jobs requiring this skill")%></a></td>
                <td class="column3" align="right" valign="top"><a class="show" href="#"><%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a></td>
              </tr>
              <tr class="accordionContent2">
 	              <td></td>
	              <td>
	                <h3>
	                  <asp:Literal runat="server" ID="DegreesHeader" EnableViewState="False"></asp:Literal>
	                </h3>
                  <asp:Label runat="server" ID="NoDegreesFoundLabel" Visible="False" style="display:block"></asp:Label>
									<asp:Repeater ID="ProgramAreaRepeater" runat="server" OnItemDataBound="ProgramAreaRepeater_ItemDataBound">
										<ItemTemplate>
											<%# Eval("Name")%>&nbsp; 
                      <ul>
											<asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound" OnItemCommand="ModalList_ItemClicked">
												<ItemTemplate>
													<li>
														<asp:LinkButton ID="DegreeEducationLevelLinkButton" CssClass="modalLink" runat="server" CommandArgument='<%# Eval("DegreeEducationLevelId").ToString() %>' CommandName="ViewDegreeCertification" />&nbsp; 
                          </li>
												</ItemTemplate>
											</asp:Repeater>
                      </ul>
										</ItemTemplate>
									</asp:Repeater>
                  <br/>
                  <asp:PlaceHolder runat="server" id="OtherDegreesPlaceHolder">
                    <h3><%=CodeLocalise("AllOtherDegrees.Header", "ALL OTHER DEGREES")%></h3>
									  <asp:Repeater ID="DegreeRepeater" runat="server" OnItemDataBound="DegreeRepeater_ItemDataBound">
										  <ItemTemplate>
											  <%# Eval("Name")%>&nbsp; 
                        <ul>
											  <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" OnItemDataBound="DegreeEducationLevelRepeater_ItemDataBound" OnItemCommand="ModalList_ItemClicked">
												  <ItemTemplate>
													  <li>
														  <asp:LinkButton ID="DegreeEducationLevelLinkButton" CssClass="modalLink" runat="server" CommandArgument='<%# Eval("DegreeEducationLevelId").ToString() %>' CommandName="ViewDegreeCertification" />&nbsp; 
                            </li>
												  </ItemTemplate>
											  </asp:Repeater>
                        </ul>
										  </ItemTemplate>
									  </asp:Repeater>
	                  <br/>
                  </asp:PlaceHolder>
                  <div style="height:19px;">
									  <h3 class="toolTipNew" title="<%= HtmlLocalise("Certifications.TooltipText", "Certifications are awarded levels of knowledge or practice, normally issued by a third-party vendor after you pass an examination. This qualifies you in a particular subject or competency, and may require continuing education credits for annual recertification.")%>"><%= HtmlLocalise("CertificationsAndLicences.Header", "CERTIFICATIONS AND LICENSES")%></h3>
                  </div>
                  <asp:Label runat="server" ID="NoCertificatesFoundLabel" Visible="False" style="display:block"></asp:Label>
									<asp:Repeater ID="CertificationRepeater" runat="server" OnItemDataBound="CertificationRepeater_ItemDataBound">
										<ItemTemplate>
											<asp:Literal ID="CertificationRankLiteral" runat="server" />. <%# Eval("CertificationName")%>
										  <br />
										</ItemTemplate>
									</asp:Repeater>
	                <br />
	              </td>
	              <td></td>
              </tr>
            </asp:PlaceHolder>
            <tr class="multipleAccordionTitle2">
              <td class="column1"><asp:Literal runat="server" ID="SkillEmployerOptionNumber">3</asp:Literal></td>
              <td class="column2"><a href="#"><%= HtmlLocalise("TopEmployers.Header", "In-demand #BUSINESSES#:LOWER")%></a></td>
              <td class="column3" align="right" valign="top"><a class="show" href="#"><%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a></td>
            </tr>
            <tr class="accordionContent2">
 							<td></td>
							<td>
							  <asp:Label runat="server" ID="NoEmployersMessage"></asp:Label>
              	<uc:SkillEmployerList ID="SkillEmployerList" runat="server" EmployerListMode="Skill" OnItemClicked="ModalList_ItemClicked" ShowEmployerJobPostingCount="True" />
							</td>
							<td></td>
            </tr>
          </table>
        </td>
        </tr>
        <tr class="modalRightPanel">
        <td colspan="2"></td>
      </tr>
    </table>
		
		</div>

  </div>

</asp:Panel>
