﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CareerAreaModal.ascx.cs"
    Inherits="Focus.CareerExplorer.WebExplorer.Controls.CareerAreaModal" %>
<%@ Register Src="ModalHeaderOptions.ascx" TagName="ModalHeaderOptions" TagPrefix="uc" %>
<%@ Register Src="JobList.ascx" TagName="CareerAreaJobList" TagPrefix="uc" %>
<%@ Register Src="SkillListSimple.ascx" TagName="CareerAreaSkillList" TagPrefix="uc" %>
<%@ Register Src="Criteria.ascx" TagName="CriteriaFilter" TagPrefix="uc" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
    PopupControlID="ModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">
    <div class="lightbox lightboxmodal" id="careerAreaLightBox">
        <uc:ModalHeaderOptions ID="ModalHeaderOptions" runat="server" OnBreadcrumbItemClicked="CareerAreaModalBreadcrumb_ItemClicked"
            OnBookmarkClicked="BookmarkLinkButton_Click" OnSendEmailClicked="SendEmailImageButton_Click"
            OnPrintCommand="ModalHeaderOptions_OnPrintCommand" OnCloseClicked="CloseButton_Click">
        </uc:ModalHeaderOptions>
        <div class="careerarea-scroll-pane">
            <table class="lightboxHeading" role="presentation">
                <tr>
                    <td>
                        <h1>
                            <asp:Label ID="CareerAreaNameLabel" runat="server" />
                            <%= HtmlLocalise("CareerArea.Label", "Career Area")%></h1>
                    </td>
                </tr>
                <tr class="lightboxHeading">
                    <td>
                        <uc:CriteriaFilter ID="CriteriaFilter" runat="server" OnFilterCancelled="Filter_Cancelled" />
                    </td>
                </tr>
            </table>
            <table width="100%" role="presentation">
                <tr class="modalLeftPanel">
                    <td>
                        <table width="100%" class="numberedAccordion" role="presentation">
                            <tr class="multipleAccordionTitle2 on" data-joblist="<%=CareerAreaJobList.ClientID %>">
                                <td valign="top" width="10" class="column1">
                                    1
                                </td>
                                <td class="column2">
                                    <div>
                                        <span class="toolTipNew" title="<%=HtmlLocalise("HiringDemand.TooltipText", "An #BUSINESS#:LOWER’s need for workers to fill their jobs is known as hiring demand. Select hiring demand to see the number of jobs, either nationally or by your geographic search area that #BUSINESSES#:LOWER posted over the last 12 months. Hiring demand is rated as low, medium, high, or very high for the area in your geographic search.") %>">
                                            <a href="#">
                                                <asp:Literal runat="server" ID="Section1Title"></asp:Literal></a></span>
                                    </div>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2 open">
                                <td>
                                </td>
                                <td>
                                    <p>
                                        <asp:Label runat="server" ID="NoJobsLabel"></asp:Label></p>
                                    <uc:CareerAreaJobList ID="CareerAreaJobList" runat="server" JobListMode="CareerArea"
                                        OnItemClicked="ModalList_ItemClicked" />
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr class="multipleAccordionTitle2">
                                <td class="column1">
                                    2
                                </td>
                                <td class="column2">
                                    <%= HtmlLocalise("TopSkills.Header", "Required skills: specialized, software, foundation")%>
                                </td>
                                <td class="column3" align="right" valign="top">
                                    <a class="show" href="#">
                                        <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                </td>
                            </tr>
                            <tr class="accordionContent2">
                                <td>
                                </td>
                                <td>
                                    <uc:CareerAreaSkillList ID="CareerAreaSkillList" runat="server" SkillsListMode="CareerArea"
                                        OnItemClicked="ModalList_ItemClicked" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr class="modalRightPanel">
                    <td colspan="2">
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
