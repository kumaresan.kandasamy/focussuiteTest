<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmployerPrint.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.EmployerPrint" %>
<%@ Register src="Criteria.ascx" tagName="CriteriaFilter" tagPrefix="uc" %>

<div class="staticAccordion">

  <table class="lightboxHeading">
    <tr>
      <td><h1><asp:Label ID="EmployerNameLabel" runat="server" /></h1></td>
    </tr>
    <tr>
      <td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" HideCloseIcons="True" /></td>
    </tr>
  </table>

  <table width="100%" class="numberedAccordion">
    <tr class="accordionTitle">
      <td width="10" class="column1">1</td>
      <td class="column2"><asp:Literal runat="server" ID="SectionOneTitle"></asp:Literal></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
      <td></td>
      <td>
        <asp:PlaceHolder runat="server" ID="JobListPlaceholder">
        <table width="100%" class="genericTable">
          <asp:Repeater ID="EmployerJobRepeater" runat="server" OnItemDataBound="EmployerJobRepeater_ItemDataBound">
            <ItemTemplate>
						  <tr>
							  <td width="15"></td>
							  <td><strong><asp:Literal ID="JobRankLiteral" runat="server" />.&nbsp; <%# Eval("ReportData.JobName") %>&nbsp; <asp:Literal ID="JobOpeningsLiteral" runat="server" /></strong> <asp:Image runat="server" ID="MortarIcon" Visible="False" AlternateText="Mortar Icon"/> <asp:Image runat="server" ID="StarterJobIcon" Visible="False" AlternateText="Starter Job Icon"/></td>
						  </tr>
            </ItemTemplate>
          </asp:Repeater>
        </table>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="InternshipListPlaceHolder">
        <table width="100%" class="genericTable">
          <asp:Repeater ID="EmployerInternshipsRepeater" runat="server" OnItemDataBound="EmployerInternshipsRepeater_ItemDataBound">
            <ItemTemplate>
						  <tr>
							  <td width="15"></td>
							  <td><strong><asp:Literal ID="InternshipRankLiteral" runat="server" />.&nbsp; <%# Eval("Name") %>&nbsp; <asp:Literal ID="InternshipOpeningsLiteral" runat="server" /></strong></td>
						  </tr>
            </ItemTemplate>
          </asp:Repeater>
        </table>
        </asp:PlaceHolder>
      </td>
      <td></td>
    </tr>
    <tr class="accordionTitle">
      <td class="column1">2</td>
      <td class="column2"><%= HtmlLocalise("TopSkills.Header", "In-demand skills required by this #BUSINESS#:LOWER")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr class="accordionContent2">
      <td></td>
      <td>
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="SpecializedSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SpecializedSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="SpecializedSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SpecializedSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="SoftwareSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SoftwareSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="SoftwareSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="SoftwareSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead" colspan="2"><strong><asp:Label ID="FoundationSkillTypeLabel" runat="server" /></strong></td>
				  </tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="FoundationSkillsColumnOneRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<asp:PlaceHolder ID="FoundationSkillsColumnTwoPlaceHolder" runat="server">
						<td width="50%" align="left" valign="top">
							<table width="100%">
								<asp:Repeater ID="FoundationSkillsColumnTwoRepeater" runat="server" OnItemDataBound="SkillsRepeater_ItemDataBound">
									<ItemTemplate>
										<tr>
											<td valign="top"><strong><asp:Literal ID="SkillRankLiteral" runat="server" />.&nbsp; <%# Eval("SkillName") %></strong></td>
										</tr>			
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						</asp:PlaceHolder>
					</tr>
			  </table>
      </td>
      <td></td>
    </tr>
  </table>

</div>