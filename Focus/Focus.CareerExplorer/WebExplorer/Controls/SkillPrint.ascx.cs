#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.CareerExplorer.Code;
using Focus.Core.Views;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class SkillPrint : UserControlBase
	{
		private int _rank = 1;

    public struct ProgramArea
    {
      public long ProgramAreaId;
      public string ProgramAreaName;

      public string Name { get { return ProgramAreaName; } }
    }

    public struct Degree
    {
      public long DegreeId;
      public string DegreeName;

      public string Name { get { return DegreeName; } }
    }

    private List<JobDegreeEducationLevelModel> _jobDegreesWithProgramAreas;
    private List<JobDegreeEducationLevelModel> _jobDegreesWithoutProgramAreas;

    private bool _showSection2;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      _showSection2 = false;
		}

    /// <summary>
    /// Handles the pre-render event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
      LocaliseUI();
    }

    /// <summary>
    /// Localises any text on the page
    /// </summary>
    private void LocaliseUI()
    {
      DegreesHeader.Text = App.Settings.Theme == FocusThemes.Education
                             ? HtmlLocalise("Degree.Header", "#SCHOOLNAME# DEGREES").ToUpper(CultureInfo.CurrentCulture)
                             : HtmlLocalise("Degree.Header", "DEGREES");
    }

    /// <summary>
    /// Binds the controls.
    /// </summary>
    /// <param name="printModel">The print model.</param>
		public void BindControls(ExplorerSkillPrintModel printModel)
		{
			SkillNameLabel.Text = printModel.Name;
      CriteriaFilter.Bind(ReportTypes.Skill, printModel.ReportCriteria);

			_rank = 1;
      if (printModel.Internships != null)
      {
        SkillInternshipsRepeater.DataSource = printModel.Internships;
        SkillInternshipsRepeater.DataBind();

        InternshipListPlaceHolder.Visible = true;
        JobListPlaceholder.Visible = false;

        SectionOneTitle.Text = CodeLocalise("InternshipDemand.Header", "Internships with this skill for the last 12 months");
      }
      else
      {
        JobRepeater.DataSource = printModel.Jobs;
        JobRepeater.DataBind();

        InternshipListPlaceHolder.Visible = false;
        JobListPlaceholder.Visible = true;

        SectionOneTitle.Text = CodeLocalise("HiringDemand.Header", "Jobs requiring this skill: hiring demand for the last<br />12 months");
      }

		  _rank = 1;
			EmployerColumnOneRepeater.DataSource = printModel.Employers.Take(10).ToList();
			EmployerColumnOneRepeater.DataBind();

			if (printModel.Employers.Count > 10)
			{
				EmployerColumnTwoRepeater.DataSource = printModel.Employers.Skip(10).ToList();
				EmployerColumnTwoRepeater.DataBind();
			}
			else
			{
				EmployerColumnTwoPlaceHolder.Visible = false;
			}

      if (App.Settings.Theme == FocusThemes.Education && _showSection2)
      {
        EducationDegreesControls(printModel);
        SkillsDegreePlaceHolder.Visible = true;
      }
      else
      {
        SkillsDegreePlaceHolder.Visible = false;
        SkillEmployerOptionNumber.Text = "2";
      }
		}

    /// <summary>
    /// Educations the degrees controls.
    /// </summary>
    /// <param name="printModel">The print model.</param>
    private void EducationDegreesControls(ExplorerSkillPrintModel printModel)
    {
      _jobDegreesWithProgramAreas = printModel.JobDegreesWithProgramAreas;
      _jobDegreesWithoutProgramAreas = printModel.JobDegreesWithoutProgramAreas;

      if (_jobDegreesWithProgramAreas.Count > 0)
      {
        ProgramAreaRepeater.DataSource = _jobDegreesWithProgramAreas
          .Select(pa => new ProgramArea { ProgramAreaId = pa.ProgramAreaId, ProgramAreaName = pa.ProgramAreaName })
          .Distinct()
          .ToList();
        ProgramAreaRepeater.DataBind();
      }
      else
      {
        ProgramAreaRepeater.Visible = false;
        NoDegreesFoundLabel.Text = CodeLocalise("NoDegreesFound.Label", "No Degrees Found");
        NoDegreesFoundLabel.Visible = true;
      }

      if (_jobDegreesWithoutProgramAreas.Count > 0)
      {
        DegreeRepeater.DataSource = _jobDegreesWithoutProgramAreas
          .Select(d => new Degree { DegreeId = d.DegreeId, DegreeName = d.DegreeName })
          .Distinct()
          .ToList();
        DegreeRepeater.DataBind();
      }
      else
      {
        OtherDegreesPlaceHolder.Visible = false;
      }

      var degreeCertifications = printModel.DegreeCertifications;
      if (degreeCertifications.JobCertifications.Count > 0)
      {
        CertificationRepeater.DataSource =
          degreeCertifications.JobCertifications.OrderByDescending(
            x => x.DemandPercentile).Take(5).ToList();
        CertificationRepeater.DataBind();
      }
      else
      {
        CertificationRepeater.Visible = false;
        NoCertificatesFoundLabel.Visible = true;
        NoCertificatesFoundLabel.Text = CodeLocalise("NoCertificatesFound.Label", "No Certificates Found");
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the JobRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void JobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
        var jobReport = (ExplorerJobReportView)e.Item.DataItem;

				var rankLiteral = (Literal)e.Item.FindControl("RankLiteral");
				rankLiteral.Text = _rank.ToString(CultureInfo.InvariantCulture);
				_rank++;

				var salaryImage = (Image)e.Item.FindControl("SalaryImage");
				switch (jobReport.ReportData.SalaryTrend)
				{
					case "$$$":
						salaryImage.ImageUrl = UrlBuilder.JobSalaryHighImage();
						break;

					case "$$":
						salaryImage.ImageUrl = UrlBuilder.JobSalaryMediumImage();
						break;

					case "$":
						salaryImage.ImageUrl = UrlBuilder.JobSalaryLowImage();
						break;

					default:
						salaryImage.Visible = false;
						break;
				}

        var mortarIcon = (Image)e.Item.FindControl("MortarIcon");
        mortarIcon.ImageUrl = UrlBuilder.DegreeProgramsImage();
        mortarIcon.Visible = (App.Settings.Theme == FocusThemes.Education && jobReport.RelatedDegreeAvailableAtCurrentSchool);

        if (jobReport.ReportData.StarterJob.HasValue)
        {
          var icon = (Image)e.Item.FindControl("StarterJobIcon");
          icon.Visible = true;
          switch (jobReport.ReportData.StarterJob.Value)
          {
	          case StarterJobLevel.Many:
		          icon.ImageUrl = UrlBuilder.StarterJobsManyImage();
		          break;
	          case StarterJobLevel.Some:
		          icon.ImageUrl = UrlBuilder.StarterJobsSomeImage();
		          break;
          }
        }
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the EmployerRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var employer = (EmployerJobViewDto)e.Item.DataItem;

				var countLiteral = (Literal)e.Item.FindControl("EmployerCountLiteral");
				countLiteral.Text = _rank.ToString(CultureInfo.CurrentCulture);

				var openingsLiteral = (Literal)e.Item.FindControl("EmployerOpeningsLiteral");
				openingsLiteral.Text = String.Format("({0}{1})", employer.Positions.ToString("#,#"),
																						 _rank == 1 ? " openings" : String.Empty);

				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the SkillInternshipsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void SkillInternshipsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var rankLiteral = (Literal)e.Item.FindControl("InternshipRankLiteral");
        rankLiteral.Text = _rank.ToString(CultureInfo.CurrentCulture);
        _rank++;
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the ProgramAreaRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void ProgramAreaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var dataItem = (ProgramArea)e.Item.DataItem;
        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");

        degreeEducationLevelRepeater.DataSource = _jobDegreesWithProgramAreas.Where(jdel => jdel.ProgramAreaId == dataItem.ProgramAreaId);
        degreeEducationLevelRepeater.DataBind();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void DegreeRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var dataItem = (Degree)e.Item.DataItem;
        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");

        degreeEducationLevelRepeater.DataSource = _jobDegreesWithoutProgramAreas.Where(jdel => jdel.DegreeId == dataItem.DegreeId);
        degreeEducationLevelRepeater.DataBind();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the CertificationRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void CertificationRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      // Add number before item
      var rankLiteral = (Literal)e.Item.FindControl("CertificationRankLiteral");
      rankLiteral.Text = (e.Item.ItemIndex + 1).ToString(CultureInfo.CurrentCulture);
    }
	}
}