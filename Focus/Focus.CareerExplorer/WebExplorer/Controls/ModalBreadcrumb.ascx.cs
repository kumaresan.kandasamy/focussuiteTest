#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Models;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class ModalBreadcrumb : UserControlBase
	{
		public List<LightboxBreadcrumb> Breadcrumbs
		{
			get { return GetViewStateValue("ModalBreadcrumb:Breadcrumb", new List<LightboxBreadcrumb>()); }
			set { SetViewStateValue("ModalBreadcrumb:Breadcrumb", value); }
		}

		public delegate void ItemClickedHandler(object sender, CommandEventArgs eventArgs);
		public event ItemClickedHandler ItemClicked;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

    /// <summary>
    /// Handles the Command event of the BreadcrumbLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void BreadcrumbLinkButton_Command(object sender, CommandEventArgs e)
		{
			OnItemClicked(e);
		}

    /// <summary>
    /// Binds the breadcrumb.
    /// </summary>
		public void BindBreadcrumb()
		{
			var breadcrumbs = Breadcrumbs;
			if (breadcrumbs != null && breadcrumbs.Count > 0)
			{
				BreadcrumbPlaceHolder.Visible = true;

				ArrowsLiteral.Text = "� ";

				Spacer1Literal.Visible = Spacer2Literal.Visible = false;
				Breadcrumb2LinkButton.Visible = Breadcrumb3LinkButton.Visible = false;

				var breadcrumb = breadcrumbs.Last();
				Breadcrumb1LinkButton.CommandName = "View" + breadcrumb.LinkType.ToString();
				Breadcrumb1LinkButton.CommandArgument = breadcrumb.LinkId.ToString();
				Breadcrumb1LinkButton.Text = CodeLocalise("Global.Back.Text", "BACK").ToUpper();

			}
			else
			{
				BreadcrumbPlaceHolder.Visible = false;
			}
		}

    /// <summary>
    /// Raises the <see cref="E:ItemClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected virtual void OnItemClicked(CommandEventArgs eventArgs)
		{
			if (ItemClicked != null)
				ItemClicked(this, eventArgs);
		}
	}
}