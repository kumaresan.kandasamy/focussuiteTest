<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HelpModal.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.HelpModal" %>

<asp:HiddenField ID="HelpModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" BehaviorID="HelpModal"
												TargetControlID="HelpModalDummyTarget"
												PopupControlID="HelpPanel" 
												BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:Panel ID="HelpPanel" runat="server" CssClass="modal" ClientIDMode="Static" Style="display: none;" >
	
	<div class="lightbox" id="helpModal">

    <table class="lightboxOptions">
      <tr>
        <td valign="top" align="right"><asp:Button ID="CloseButton" runat="server" SkinID="closeModal" CausesValidation="False" OnClick="CancelButton_Click" class="closeModal" /></td>
      </tr>
    </table>
		
    <table class="lightboxHeading">
      <tr>
      	<td><h1><%= HtmlLocalise("Help.Heading", "Help")%></h1></td>
      </tr>
    </table>

		<table width="767px">
			<tr>
				<td>
					<%= HtmlLocalise("Global.ExplorerHelpIntructions.Text", "If you have problems with #FOCUSEXPLORER#, please email us for assistance. Be sure to provide a detailed description of your problem, your computer's operating system (e.g., Windows 7, Mac OSX, etc.), and your browser/version (e.g., Internet Explorer v9.0, Firefox, Chrome, Safari, etc.). Thank you!")%>
				</td>
			</tr>
      <tr>
        <td><div style="height:10px;" /></td>
      </tr>
    	<tr>
    		<td><%= HtmlLocalise("YourEmail.Text", "Your email")%></td>
    	</tr>
			<tr>
				<td>
					<%= HtmlInFieldLabel("YourEmailTextBox", "YourEmail.InlineLabel", "enter your email address", 350)%>
					<asp:TextBox ID="YourEmailTextBox" runat="server" Width="100%" />
					<asp:RequiredFieldValidator ID="YourEmailTextBoxRequired" runat="server" ControlToValidate="YourEmailTextBox" Display="Dynamic"
						SetFocusOnError="true" CssClass="error" ValidationGroup="HelpModal"/>
          <asp:RegularExpressionValidator ID="YourEmailTextBoxRegEx" runat="server" ControlToValidate="YourEmailTextBox" Display="Dynamic" 
						SetFocusOnError="true" CssClass="error" ValidationGroup="HelpModal" />
				</td>
			</tr>
      <tr>
      	<td><%= HtmlLocalise("Message.Text", "Message")%></td>
      </tr>
      <tr>
        <td>
          <%= HtmlInFieldLabel("HelpMessageTextBox", "HelpMessage.InlineLabel", "type your message here", 350)%>
          <asp:TextBox ID="HelpMessageTextBox" runat="server" Width="100%" TextMode="MultiLine" Rows="8" />
					<asp:RequiredFieldValidator ID="HelpMessageTextBoxRequired" runat="server" ControlToValidate="HelpMessageTextBox" Display="Dynamic" 
						SetFocusOnError="true" CssClass="error" ValidationGroup="HelpModal"/>
        </td>
      </tr>
      <tr>
        <td><div style="height:10px;" /></td>
      </tr>
			<tr>
        <td align="right">
          <asp:Button ID="CancelButton" runat="server" class="buttonLevel3" CausesValidation="False" OnClick="CancelButton_Click" />
          <asp:Button ID="SendEmailButton" runat="server" class="buttonLevel2" CausesValidation="True" ValidationGroup="HelpModal" OnClick="SendEmailButton_Click" />
        </td>
      </tr>
		</table>

	</div>

</asp:Panel>

<asp:HiddenField ID="HelpConfirmModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="HelpConfirmModalPopup" runat="server" BehaviorID="HelpConfirmModal"
												TargetControlID="HelpConfirmModalDummyTarget"
												PopupControlID="HelpConfirmPanel" 
												PopupDragHandleControlID="HelpConfirmPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" />

<asp:Panel ID="HelpConfirmPanel" runat="server" CssClass="modal" ClientIDMode="Static" Style="display: none;" >
	
	<div class="lightbox" id="helpConfirmModal">

    <table class="lightboxOptions">
      <tr>
        <td valign="top" align="right"><asp:Button ID="CloseConfirmButton" runat="server" SkinID="closeModal" CausesValidation="False" OnClick="CancelConfirmButton_Click" class="closeModal" /></td>
      </tr>
    </table>

    <table class="lightboxHeading">
      <tr>
      	<td><h1><%= HtmlLocalise("Help.Heading", "Help")%></h1></td>
      </tr>
    </table>

		<table width="250px">
			<tr>
				<td>
					<%= HtmlLocalise("HelpConfirmation.Text", "Your help message has been sent.")%>
				</td>
			</tr>
      <tr>
        <td><div style="height:20px;" /></td>
      </tr>
			<tr>
        <td align="right">
          <asp:Button ID="CancelConfirmButton" runat="server" class="buttonLevel3" CausesValidation="False" OnClick="CancelConfirmButton_Click" />
        </td>
      </tr>
		</table>
	</div>

</asp:Panel>
