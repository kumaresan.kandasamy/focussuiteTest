<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillSelector.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.SkillSelector" %>

<div class="overlayPanelContainer sidebarSkillsPanel">
  <div class="overlayPanelClosed overlayPanelOpenTrigger">
    <span class="promoStyle4"><a href="#">Tell us about your skills to<br />see your best job choices �</a></span>
  </div>
  <div class="overlayPanelOpen">
    <table>
      <tr>
        <td colspan="2"><span class="title">Skills</span> <a href="#"><img src="<%= UrlBuilder.CloseMedium() %>" alt="Close" class="overlayPanelClosedTrigger" width="13" height="13" /></a></td>
      </tr>
      <tr>
        <td align="right"><input type="radio" name="education" value="first" /></td>
        <td>Select skill by category</td>
      </tr>
      <tr>
        <td colspan="2">
          <asp:DropDownList ID="DropDownList1" runat="server" Width="190">
              <asp:ListItem>Skill Category 1</asp:ListItem>
          </asp:DropDownList>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <asp:DropDownList ID="DropDownList2" runat="server" Width="190">
              <asp:ListItem>Subcategory</asp:ListItem>
          </asp:DropDownList>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <asp:DropDownList ID="DropDownList3" runat="server" Width="190">
              <asp:ListItem>Skills</asp:ListItem>
          </asp:DropDownList>
        </td>
      </tr>
      <tr>
        <td align="right" valign="top"><input type="radio" name="education" value="second" /></td>
        <td>Select skill by job</td>
      </tr>
      <tr>
        <td></td>
        <td><asp:TextBox ID="TextBox1" runat="server" ClientIDMode="Static" Width="96%" /></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><asp:Button ID="Button1" runat="server" Text="Go &#0187;" class="buttonLevel2" /></td>
      </tr>
    </table>
  </div>
</div>