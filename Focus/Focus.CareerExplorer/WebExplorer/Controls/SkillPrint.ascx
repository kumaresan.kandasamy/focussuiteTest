<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillPrint.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.SkillPrint" %>
<%@ Register src="Criteria.ascx" tagName="CriteriaFilter" tagPrefix="uc" %>

<div class="staticAccordion">

  <table class="lightboxHeading">
    <tr>
      <td><h2>
	      <span class="sr-only">Skill name</span>
	      <asp:Label ID="SkillNameLabel" runat="server" />
      </h2></td>
    </tr>
    <tr>
      <td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" HideCloseIcons="True" /></td>
    </tr>
  </table>

  <table width="100%" class="numberedAccordion">
    <tr class="accordionTitle">
      <td valign="top" width="10" class="column1">1</td>
      <td class="column2"><asp:Literal runat="server" ID="SectionOneTitle"></asp:Literal></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
      <td></td>
      <td>
    	  <!-- job list -->
        <asp:PlaceHolder runat="server" ID="JobListPlaceholder">
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead">
						  <%= HtmlLocalise("Job.ColumnTitle", "JOB")%>
					  </td>
					  <td class="tableHead">
						  <%= HtmlLocalise("HiringDemand.ColumnTitle", "HIRING DEMAND")%>
					  </td>
					  <td class="tableHead">
						  <%= HtmlLocalise("Salary.ColumnTitle", "SALARY")%>
					  </td>
				  </tr>
				  <asp:Repeater ID="JobRepeater" runat="server" OnItemDataBound="JobRepeater_ItemDataBound">
					  <ItemTemplate>
						  <tr>
							  <td width="55%"><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<%# Eval("ReportData.Name")%></strong> <asp:Image runat="server" ID="MortarIcon" Visible="False" AlternateText="Mortar Icon"/><asp:Image runat="server" ID="StarterJobIcon" Visible="False" AlternateText="Starter Job Icon"/></td>
							  <td width="25%"><%# Eval("ReportData.HiringTrend")%></td>
							  <td width="20%"><asp:Image ID="SalaryImage" runat="server" AlternateText="Salary Image" /></td>
						  </tr>
					  </ItemTemplate>
				  </asp:Repeater>
			  </table>
        </asp:PlaceHolder>
        <asp:PlaceHolder runat="server" ID="InternshipListPlaceHolder">
        <table width="100%" class="genericTable">
          <asp:Repeater ID="SkillInternshipsRepeater" runat="server" OnItemDataBound="SkillInternshipsRepeater_ItemDataBound">
            <ItemTemplate>
						  <tr>
							  <td width="15"></td>
							  <td><strong><asp:Literal ID="InternshipRankLiteral" runat="server" />.&nbsp; <%# Eval("Name") %>&nbsp; <asp:Literal ID="InternshipOpeningsLiteral" runat="server" /></strong></td>
						  </tr>
            </ItemTemplate>
          </asp:Repeater>
        </table>
        </asp:PlaceHolder>
      </td>
      <td></td>
    </tr>
    <asp:PlaceHolder runat="server" ID="SkillsDegreePlaceHolder">
    <tr class="accordionTitle">
      <td class="column1">2</td>
      <td class="column2"><%= HtmlLocalise("TopDegreesAndCertificates.Header", "Top degrees & certificates for jobs requiring this skill")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
 		  <td></td>
 		  <td>
	      <h3>
	        <asp:Literal runat="server" ID="DegreesHeader" EnableViewState="False"></asp:Literal>
	      </h3>
 		    <asp:Label runat="server" ID="NoDegreesFoundLabel" Visible="False" style="display:block"></asp:Label>
        <asp:Repeater ID="ProgramAreaRepeater" runat="server" OnItemDataBound="ProgramAreaRepeater_ItemDataBound">
				  <ItemTemplate>
					  <%# Eval("Name")%>&nbsp; 
            <ul>
					  <asp:Repeater ID="DegreeEducationLevelRepeater" runat="server">
						  <ItemTemplate>
							  <li><%# Eval("DegreeEducationLevelName").ToString() %></li>
						  </ItemTemplate>
					  </asp:Repeater>
            </ul>
				  </ItemTemplate>
			  </asp:Repeater>
        <br/>
        <asp:PlaceHolder runat="server" id="OtherDegreesPlaceHolder">
          <h3><%=CodeLocalise("AllOtherDegrees.Header", "ALL OTHER DEGREES")%></h3>
					<asp:Repeater ID="DegreeRepeater" runat="server" OnItemDataBound="DegreeRepeater_ItemDataBound">
						<ItemTemplate>
							<%# Eval("Name")%>&nbsp; 
              <ul>
							<asp:Repeater ID="DegreeEducationLevelRepeater" runat="server" >
								<ItemTemplate>
									<li><%# Eval("DegreeEducationLevelName").ToString() %></li>
								</ItemTemplate>
							</asp:Repeater>
              </ul>
						</ItemTemplate>
					</asp:Repeater>
	        <br/>
        </asp:PlaceHolder>
        <h3><%= HtmlLocalise("CertificationsAndLicences.Header", "CERTIFICATIONS AND LICENSES")%></h3>
        <asp:Label runat="server" ID="NoCertificatesFoundLabel" Visible="False" style="display:block"></asp:Label>
				<asp:Repeater ID="CertificationRepeater" runat="server" OnItemDataBound="CertificationRepeater_ItemDataBound">
					<ItemTemplate>
						<asp:Literal ID="CertificationRankLiteral" runat="server" />. <%# Eval("CertificationName")%>
						<br />
					</ItemTemplate>
				</asp:Repeater>
        <br/>
 		  </td>
 		  <td></td>
    </tr>
    </asp:PlaceHolder>
    <tr class="accordionTitle">
      <td class="column1"><asp:Literal runat="server" ID="SkillEmployerOptionNumber">3</asp:Literal></td>
      <td class="column2"><%= HtmlLocalise("TopEmployers.Header", "In-demand #BUSINESSES#:LOWER")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
 		  <td></td>
		  <td>
        <!-- employer list -->
			  <table width="100%" class="genericTable">
				  <tr>
					  <td align="left" valign="top">
						  <asp:Repeater ID="EmployerColumnOneRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
							  <HeaderTemplate>
								  <table width="100%">
							  </HeaderTemplate>
							  <ItemTemplate>
								  <tr>
									  <td valign="top">
										  <strong><asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp; <%# Eval("EmployerName") %>&nbsp; <asp:Literal ID="EmployerOpeningsLiteral" runat="server" /></strong>
									  </td>
								  </tr>
							  </ItemTemplate>
							  <FooterTemplate>
								  </table>
							  </FooterTemplate>
						  </asp:Repeater>
					  </td>
					  <asp:PlaceHolder ID="EmployerColumnTwoPlaceHolder" runat="server">
					  <td width="50%" align="left" valign="top">
						  <asp:Repeater ID="EmployerColumnTwoRepeater" runat="server" OnItemDataBound="EmployerRepeater_ItemDataBound">
							  <HeaderTemplate>
								  <table width="100%">
							  </HeaderTemplate>
							  <ItemTemplate>
								  <tr>
									  <td valign="top">
										  <strong><asp:Literal ID="EmployerCountLiteral" runat="server" />.&nbsp; <%# Eval("EmployerName") %>&nbsp; <asp:Literal ID="EmployerOpeningsLiteral" runat="server" /></strong>
									  </td>
								  </tr>
							  </ItemTemplate>
							  <FooterTemplate>
								  </table>
							  </FooterTemplate>
						  </asp:Repeater>
					  </td>
					  </asp:PlaceHolder>
				  </tr>
			  </table>
		  </td>
		  <td></td>
    </tr>
  </table>

</div>