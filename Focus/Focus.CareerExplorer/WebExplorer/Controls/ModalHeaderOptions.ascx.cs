﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;

using Focus.Common.Models;
using Focus.Common;

#endregion

namespace Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class ModalHeaderOptions : UserControlBase
	{
    public bool IsInitialised 
    {
      get { return GetViewStateValue<bool>("ModalHeaderOptions:IsInitialised"); }
      set { SetViewStateValue("ModalHeaderOptions:IsInitialised", value); }
    }

    public delegate void BreadcrumbItemClickedHandler(object sender, CommandEventArgs eventArgs);
    public event BreadcrumbItemClickedHandler BreadcrumbItemClicked;

    public delegate void SendEmailClickedHandler(object sender, EventArgs eventArgs);
    public event SendEmailClickedHandler SendEmailClicked;

    public delegate void BookmarkClickedHandler(object sender, EventArgs eventArgs);
    public event BookmarkClickedHandler BookmarkClicked;

    public delegate void PrintCommandHandler(object sender, CommandEventArgs eventArgs);
    public event PrintCommandHandler PrintCommand;

    public delegate void CloseClickedHandler(object sender, EventArgs eventArgs);
    public event CloseClickedHandler CloseClicked;

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      if (!IsInitialised)
      {
        Localise();
        ApplySettings();

				ApplyBranding();

        IsInitialised = true;

        if (App.IsAnonymousAccess())
          EmailCell.Visible = DownloadCell.Visible = ModalBookmarkImage.Visible = ModalBookmarkLinkButton.Visible = ModalPrintImageButton.Visible = false;
      }
    }

		/// <summary>
		/// Applies client specific branding.
		/// </summary>
		private void ApplyBranding()
		{
			ModalBookmarkImage.ImageUrl = UrlBuilder.BookmarkIcon();
			ModalSendEmailImageButton.ImageUrl = UrlBuilder.EmailIcon();
			ModalDownloadImageButton.ImageUrl = UrlBuilder.DownloadIcon();
			ModalPrintImageButton.ImageUrl = UrlBuilder.PrintIcon();
		}

    /// <summary>
    /// Binds controls
    /// </summary>
    /// <param name="breadcrumbs">The breadcrumb trail</param>
    public void BindBreadcrumbs(List<LightboxBreadcrumb> breadcrumbs)
    {
      ModalBreadcrumb.Breadcrumbs = breadcrumbs;
      ModalBreadcrumb.BindBreadcrumb();
    }

    /// <summary>
    /// Handles the OnItemClicked event of the ModalBreadcrumb control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalBreadcrumb_OnItemClicked(object sender, CommandEventArgs eventArgs)
    {
      OnBreadcrumbItemClicked(eventArgs);
    }

    /// <summary>
    /// Handles the OnClick event of the ModalBookmarkLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ModalBookmarkLinkButton_OnClick(object sender, EventArgs eventArgs)
    {
      OnBookmarkClicked(eventArgs);
    }

    /// <summary>
    /// Handles the OnClick event of the ModalSendEmailImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="ImageClickEventArgs"/> instance containing the event data.</param>
    protected void ModalSendEmailImageButton_OnClick(object sender, ImageClickEventArgs eventArgs)
    {
      OnSendEmailClicked(eventArgs);
    }

    /// <summary>
    /// Handles the OnCommand event of the ModalOptionButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalOptionButton_OnCommand(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "Print":
          OnPrintCommand(e);
          break;
      }
    }

    /// <summary>
    /// Handles the OnClick event of the ModalCloseButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ModalCloseButton_OnClick(object sender, EventArgs eventArgs)
    {
      OnCloseClicked(eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="BreadcrumbItemClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected virtual void OnBreadcrumbItemClicked(CommandEventArgs eventArgs)
    {
      if (BreadcrumbItemClicked != null)
        BreadcrumbItemClicked(this, eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="SendEmailClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnSendEmailClicked(EventArgs eventArgs)
    {
      if (SendEmailClicked != null)
        SendEmailClicked(this, eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="BookmarkClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnBookmarkClicked(EventArgs eventArgs)
    {
      if (BookmarkClicked != null)
        BookmarkClicked(this, eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="PrintCommand"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected virtual void OnPrintCommand(CommandEventArgs eventArgs)
    {
      if (PrintCommand != null)
        PrintCommand(this, eventArgs);
    }

    /// <summary>
    /// Raises the <see cref="CloseClicked"/> event.
    /// </summary>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected virtual void OnCloseClicked(EventArgs eventArgs)
    {
      if (CloseClicked != null)
        CloseClicked(this, eventArgs);
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void Localise()
    {
      ModalSendEmailImageButton.ToolTip =  HtmlLocalise("Global.SendEmail.ToolTip", "Send Email");
      ModalPrintImageButton.ToolTip =  HtmlLocalise("Global.Print.ToolTip", "Print");
      ModalDownloadImageButton.ToolTip =  HtmlLocalise("Global.Download.ToolTip", "Download");
      ModalCloseButton.ToolTip = HtmlLocalise("Global.Close.ToolTip", "Close");
	    ModalBookmarkLinkButton.ToolTip = CodeLocalise("Bookmark.TooltipText","Save your search results as bookmarks and email to yourself or others. Each search you bookmark is saved in “My bookmarks” as a hyperlink that you may use in the future.");
    }

    /// <summary>
    /// Hides/Shows controls based on settings
    /// </summary>
    private void ApplySettings()
    {
      ModalBookmarkImage.Visible = true;
      ModalBookmarkLinkButton.Visible = true;
      if (!App.Settings.ExplorerShowBookmarkForNonLoggedInUser && !ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
      {
        ModalBookmarkImage.Visible = false;
        ModalBookmarkLinkButton.Visible = false;
      }

      EmailCell.Visible = App.Settings.ExplorerModalEmailIcon &&
                         (App.Settings.ExplorerShowEmailForNonLoggedInUser || ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated));

      DownloadCell.Visible = App.Settings.ExplorerModalDownloadIcon;
    }
  }
}