﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobTitleSelector.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.JobTitleSelector" %>

<div class="selectionTable" role="presentation">
    <div class="col-md-12 col-sm-12">
        <div class="col-md-6 col-sm-6">
            <asp:RadioButton ID="JobTitleAutoCompleteTextBox_RadioButton" runat="server" GroupName="StudyTitle" Checked="True"/>
        </div>
        <div class="col-md-6 col-sm-6">
            <focus:AutoCompleteTextBox ID="JobTitleAutoCompleteTextBox" runat="server" Width="225" style="margin-bottom:5px" AutoCompleteType="Disabled"/>
            <asp:CustomValidator id="JobTitleAutoCompleteTextBox_CustomValidator" runat="server" ControlToValidate="JobTitleAutoCompleteTextBox" ValidateEmptyText="True" ClientValidationFunction="JobTitleSelectorValidate" CssClass="error" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Test" />
            <div id="JobTitleAutoCompleteTextBox_JobRow" runat="server" Visible="False">
                <asp:DropDownList ID="JobTitleAutoComplete_RelatedJobs" runat="server" Width="225" Title="Job Titles"/>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12">
        <div class="col-md-6 col-sm-6" runat="server" id="MilitaryRow">
            <asp:RadioButton ID="MilitaryAutoCompleteTextBox_RadioButton" runat="server" GroupName="StudyTitle"/>
        </div>
        <div class="col-md-6 col-sm-6">
            <focus:AutoCompleteTextBox ID="MilitaryAutoCompleteTextBox" runat="server" Width="225" style="margin-bottom:5px" AutoCompleteType="Disabled" />
            <asp:CustomValidator id="MilitaryAutoCompleteTextBox_CustomValidator" runat="server" ControlToValidate="MilitaryAutoCompleteTextBox" ValidateEmptyText="True" ClientValidationFunction="JobTitleSelectorValidate" CssClass="error" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Test" />
        <div id="MilitaryAutoCompleteTextBox_JobRow" runat="server" Visible="False">
            <asp:DropDownList ID="MilitaryAutoCompleteTextBox_RelatedJobs" runat="server" Width="225" />
        </div>
       </div>
    </div>
</div>

