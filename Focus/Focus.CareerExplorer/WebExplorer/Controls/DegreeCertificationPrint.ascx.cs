#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.CareerExplorer.Code;
using Focus.Core.Views;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
	public partial class DegreeCertificationPrint : UserControlBase
	{
		private int _rank = 1;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
        TieredJobsListRow.Visible = App.Settings.ExplorerUseDegreeTiering;
      }
		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
    /// <param name="printModel">The print model.</param>
		public void BindControls(ExplorerDegreeCertificationPrintModel printModel)
		{
			DegreeCertificationNameLabel.Text = printModel.Name;
      CriteriaFilter.Bind(ReportTypes.DegreeCertification, printModel.ReportCriteria);

      if (App.Settings.ExplorerUseDegreeTiering)
      {
        TieredDegreeJobListIntroLiteral.Text =
          CodeLocalise("JobDegreeTier1.Text", "Jobs specific to your program of study") + "<br />";
        Tier2DegreeJobListIntroLiteral.Text =
          CodeLocalise("JobDegreeTier2.Text", "Jobs that may be related to your studies") + "<br />";
        Tier2JobRepeater.DataSource = printModel.JobsTier2;

        Tier2JobRepeater.DataBind();
      }
      else
      {
        TieredDegreeJobListIntroLiteral.Visible = false;
      }

			_rank = 1;
			JobRepeater.DataSource = printModel.Jobs;
			JobRepeater.DataBind();


			SpecializedSkillTypeLabel.Text = CodeLocalise("SpecializedSkills.Label", "SPECIALIZED SKILLS");
			_rank = 1;
			SpecializedSkillsColumnOneRepeater.DataSource = printModel.SpecializedSkills.Take(10).ToList();
			SpecializedSkillsColumnOneRepeater.DataBind();

			if (printModel.SpecializedSkills.Count > 10)
			{
				SpecializedSkillsColumnTwoRepeater.DataSource = printModel.SpecializedSkills.Skip(10).Take(10).ToList();
				SpecializedSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				SpecializedSkillsColumnTwoPlaceHolder.Visible = false;
			}

			SoftwareSkillTypeLabel.Text = CodeLocalise("SoftwareSkills.Label", "SOFTWARE SKILLS");
			_rank = 1;
			SoftwareSkillsColumnOneRepeater.DataSource = printModel.SoftwareSkills.Take(10).ToList();
			SoftwareSkillsColumnOneRepeater.DataBind();

			if (printModel.SoftwareSkills.Count > 10)
			{
				SoftwareSkillsColumnTwoRepeater.DataSource = printModel.SoftwareSkills.Skip(10).Take(10).ToList();
				SoftwareSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				SoftwareSkillsColumnTwoPlaceHolder.Visible = false;
			}

			FoundationSkillTypeLabel.Text = CodeLocalise("FoundationSkills.Label", "FOUNDATION SKILLS");
			_rank = 1;
			FoundationSkillsColumnOneRepeater.DataSource = printModel.FoundationSkills.Take(10).ToList();
			FoundationSkillsColumnOneRepeater.DataBind();

			if (printModel.FoundationSkills.Count > 10)
			{
				FoundationSkillsColumnTwoRepeater.DataSource = printModel.FoundationSkills.Skip(10).Take(10).ToList();
				FoundationSkillsColumnTwoRepeater.DataBind();
			}
			else
			{
				FoundationSkillsColumnTwoPlaceHolder.Visible = false;
			}

			_rank = 1;
			EmployerColumnOneRepeater.DataSource = printModel.Employers.Take(10).ToList();
			EmployerColumnOneRepeater.DataBind();

			if (printModel.Employers.Count > 10)
			{
				EmployerColumnTwoRepeater.DataSource = printModel.Employers.Skip(10).ToList();
				EmployerColumnTwoRepeater.DataBind();
			}
			else
			{
				EmployerColumnTwoPlaceHolder.Visible = false;
			}

			Section4PlaceHolder.Visible = true;
			if (App.Settings.ExplorerShowDegreeStudyPlaces)
			{
				DegreeCertificationStudyPlaceRepeater.DataSource = printModel.StudyPlaces;
				DegreeCertificationStudyPlaceRepeater.DataBind();
			}
			else
			{
				Section4PlaceHolder.Visible = false;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the JobRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void JobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var jobReport = (ExplorerJobReportView) e.Item.DataItem;

        var rankLiteral = (Literal) e.Item.FindControl("RankLiteral");
        rankLiteral.Text = _rank.ToString();
        _rank++;

        var salaryImage = (Image) e.Item.FindControl("SalaryImage");
        switch (jobReport.ReportData.SalaryTrend)
        {
          case "$$$":
            salaryImage.ImageUrl = UrlBuilder.JobSalaryHighImage();
            break;
          case "$$":
            salaryImage.ImageUrl = UrlBuilder.JobSalaryMediumImage();
            break;
          case "$":
            salaryImage.ImageUrl = UrlBuilder.JobSalaryLowImage();
            break;
          default:
            salaryImage.Visible = false;
            break;
        }

        var mortarIcon = (Image)e.Item.FindControl("MortarIcon");
	      mortarIcon.ImageUrl = UrlBuilder.DegreeProgramsImage();
        mortarIcon.Visible = (App.Settings.Theme == FocusThemes.Education && jobReport.RelatedDegreeAvailableAtCurrentSchool);

        if (jobReport.ReportData.StarterJob.HasValue)
        {
          var icon = (Image) e.Item.FindControl("StarterJobIcon");
          icon.Visible = true;
          switch (jobReport.ReportData.StarterJob.Value)
          {
	          case StarterJobLevel.Many:
		          icon.ImageUrl = UrlBuilder.StarterJobsManyImage();
		          break;
	          case StarterJobLevel.Some:
		          icon.ImageUrl = UrlBuilder.StarterJobsSomeImage();
		          break;
          }
        }
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the SkillsRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
	  protected void SkillsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var rankLiteral = (Literal)e.Item.FindControl("SkillRankLiteral");
				rankLiteral.Text = _rank.ToString();
				_rank++;
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the EmployerRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void EmployerRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var employer = (EmployerJobViewDto)e.Item.DataItem;

				var countLiteral = (Literal)e.Item.FindControl("EmployerCountLiteral");
				countLiteral.Text = _rank.ToString();

				var openingsLiteral = (Literal)e.Item.FindControl("EmployerOpeningsLiteral");
				openingsLiteral.Text = String.Format("({0}{1})", employer.Positions.ToString("#,#"),
																						 _rank == 1 ? " openings" : String.Empty);

				_rank++;
			}
		}
	}
}