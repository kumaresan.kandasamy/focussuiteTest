<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternshipModal.ascx.cs"
    Inherits=" Focus.CareerExplorer.WebExplorer.Controls.InternshipModal" %>
<%@ Register Src="ModalHeaderOptions.ascx" TagName="ModalHeaderOptions" TagPrefix="uc" %>
<%@ Register Src="SkillListSimple.ascx" TagName="InternshipSkillList" TagPrefix="uc" %>
<%@ Register Src="EmployerList.ascx" TagName="InternshipEmployerList" TagPrefix="uc" %>
<%@ Register Src="Criteria.ascx" TagName="CriteriaFilter" TagPrefix="uc" %>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
    PopupControlID="ModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">
    <div class="lightbox lightboxmodal" id="internshipLightBox">
        <uc:ModalHeaderOptions ID="ModalHeaderOptions" runat="server" OnBreadcrumbItemClicked="InternshipModalBreadcrumb_ItemClicked"
            OnBookmarkClicked="BookmarkLinkButton_Click" OnSendEmailClicked="SendEmailImageButton_Click"
            OnPrintCommand="ModalHeaderOptions_OnPrintCommand" OnCloseClicked="CloseButton_Click">
        </uc:ModalHeaderOptions>
        <div class="internship-scroll-pane">
            <table class="lightboxHeading" role="presentation">
                <tr>
                    <td>
                        <h1>
                            <asp:Label ID="InternshipNameLabel" runat="server" />
                            <%= HtmlLocalise("Internships.Label", "Internships") %></h1>
                    </td>
                </tr>
                <tr class="lightboxHeading">
                    <td>
                        <uc:CriteriaFilter ID="CriteriaFilter" runat="server" OnFilterCancelled="Filter_Cancelled" />
                    </td>
                </tr>
            </table>
            <table width="70%" role="presentation">
                <tr>
                    <td>
                        <asp:Label ID="InternshipBlurbLabel" runat="server" />
                    </td>
                </tr>
            </table>
            <div style="overflow-y: auto; overflow-x: hidden">
                <table width="100%" role="presentation">
                    <tr class="modalLeftPanel">
                        <td>
                            <table width="100%" class="numberedAccordion" role="presentation">
                                <tr class="multipleAccordionTitle2 on">
                                    <td class="column1">
                                        1
                                    </td>
                                    <td class="column2">
                                        <%= HtmlLocalise("TopSkills.Header", "In-demand skills: specialized, software, foundation")%>
                                    </td>
                                    <td class="column3" align="right" valign="top">
                                        <a class="show" href="#">
                                            <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                    </td>
                                </tr>
                                <tr class="accordionContent2 open">
                                    <td>
                                    </td>
                                    <td>
                                        <uc:InternshipSkillList ID="InternshipSkillList" runat="server" SkillsListMode="Internship"
                                            OnItemClicked="ModalList_ItemClicked" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr class="multipleAccordionTitle2">
                                    <td class="column1">
                                        2
                                    </td>
                                    <td class="column2">
                                        <%= HtmlLocalise("TopEmployers.Header", "In-demand #BUSINESSES#:LOWER")%>
                                    </td>
                                    <td class="column3" align="right" valign="top">
                                        <a class="show" href="#">
                                            <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                    </td>
                                </tr>
                                <tr class="accordionContent2">
                                    <td>
                                    </td>
                                    <td>
                                        <uc:InternshipEmployerList ID="InternshipEmployerList" runat="server" EmployerListMode="Internship"
                                            OnItemClicked="ModalList_ItemClicked" ShowEmployerJobPostingCount="False" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr class="multipleAccordionTitle2">
                                    <td class="column1">
                                        3
                                    </td>
                                    <td class="column2">
                                        <div>
                                            <span class="toolTipNew" style="position: relative" title="<%= HtmlLocalise("HiringDemand.TooltipText", "An #BUSINESS#:LOWER�s need for workers to fill their jobs is known as hiring demand. Select hiring demand to see the number of jobs, either nationally or by your geographic search area that #BUSINESSES#:LOWER posted over the last 12 months. Hiring demand is rated as low, medium, high, or very high for the area in your geographic search.") %>">
                                                <asp:Label ID="TopInternshipsTitleLabel" runat="server"></asp:Label></span>
                                        </div>
                                    </td>
                                    <td class="column3" align="right" valign="top">
                                        <a class="show" href="#">
                                            <%= HtmlLocalise("Global.Show.Text", "SHOW")%></a><a class="hide" href="#"><%= HtmlLocalise("Global.Hide.Text", "HIDE")%></a>
                                    </td>
                                </tr>
                                <tr class="accordionContent2">
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="TopInternshipsDetailLabel"></asp:Literal>
                                        <table width="100%" class="genericTable" role="presentation">
                                            <asp:Repeater ID="InternshipJobRepeater" runat="server" OnItemDataBound="InternshipJobRepeater_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <strong>
                                                                <asp:Literal ID="JobRankLiteral" runat="server" />.&nbsp;
                                                                <%# Eval("JobTitle") %>&nbsp;
                                                                <asp:Literal ID="JobOpeningsLiteral" runat="server" /></strong>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="modalRightPanel">
                        <td valign="top">
                            <asp:PlaceHolder ID="ReadyToMakeAMovePlaceHolder" runat="server"><span class="promoStyle3">
                                <%= HtmlLocalise("ReadyToMakeAMove.Text", "Ready to make a move?")%>
                            </span><span class="promoStyle3Arrow"></span></asp:PlaceHolder>
                            <asp:PlaceHolder ID="ViewCurrentPositionsPlaceHolder" runat="server">
                                <asp:LinkButton ID="ViewCurrentPostingsLinkButton" Text="View Current Postings LinkButton"
                                    runat="server" OnClick="ViewCurrentPostingsLinkButton_Click">
                                    <span class="promoStyle4"><span class="modalLink">
                                        <asp:Literal runat="server" ID="ViewCurrentPositionsLiteral" Text="See current postings for<br />this internship �"></asp:Literal>
                                    </span></span>
                                </asp:LinkButton>
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Panel>
