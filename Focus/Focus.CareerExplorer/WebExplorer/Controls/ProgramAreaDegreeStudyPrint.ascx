﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProgramAreaDegreeStudyPrint.ascx.cs" Inherits="Focus.CareerExplorer.WebExplorer.Controls.ProgramAreaDegreeStudyPrint" %>
<%@ Register src="Criteria.ascx" tagName="CriteriaFilter" tagPrefix="uc" %>

<div class="staticAccordion">

  <table class="lightboxHeading">
    <tr>
      <td><h1><asp:Label ID="ProgramAreaDegreeStudyNameLabel" runat="server" /></h1></td>
    </tr>
    <tr>
      <td><uc:CriteriaFilter ID="CriteriaFilter" runat="server" HideCloseIcons="True" /></td>
    </tr>
  </table>
  
    <p>
  <asp:Literal runat="server" ID="ProgramAreaDescription"></asp:Literal>
  </p>

  <table width="100%" class="numberedAccordion">
    <tr class="accordionTitle">
      <td width="10" class="column1">1</td>
      <td class="column2"><%= HtmlLocalise("DegreesOffered.Header", "Degrees offered")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
      <td></td>
      <td>
        <ul>
				  <asp:Repeater ID="DegreeRepeater" runat="server">
					  <ItemTemplate>
						  <li><%# Eval("DegreeEducationLevelName")%></li>
					  </ItemTemplate>
				  </asp:Repeater>
        </ul>
        <br />
      </td>
      <td></td>
    </tr>
    <tr class="accordionTitle">
      <td width="10" class="column1">2</td>
      <td class="column2"><%= HtmlLocalise("Global.HiringDemand.Header", "Hiring demand for the last 12 months")%></td>
      <td class="column3" align="right" valign="top"></td>
    </tr>
    <tr>
      <td></td>
      <td>
			  <table width="100%" class="genericTable withHeader">
				  <tr>
					  <td class="tableHead">
						  <%= HtmlLocalise("Job.ColumnTitle", "JOB")%>
					  </td>
					  <td class="tableHead">
						  <%= HtmlLocalise("HiringDemand.ColumnTitle", "HIRING DEMAND")%>
					  </td>
					  <td class="tableHead">
						  <%= HtmlLocalise("Salary.ColumnTitle", "SALARY")%>
					  </td>
				  </tr>
				  <asp:Repeater ID="JobRepeater" runat="server" OnItemDataBound="JobRepeater_ItemDataBound">
					  <ItemTemplate>
						  <tr>
							  <td width="40%"><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<%# Eval("ReportData.Name")%></strong></td>
							  <td width="40%"><%# Eval("ReportData.HiringTrend")%></td>
							  <td width="20%"><asp:Image ID="SalaryImage" runat="server" AlternateText="Salary Image" /></td>
						  </tr>
					  </ItemTemplate>
				  </asp:Repeater>
			  </table>
      </td>
      <td></td>
    </tr>
  </table>
</div>