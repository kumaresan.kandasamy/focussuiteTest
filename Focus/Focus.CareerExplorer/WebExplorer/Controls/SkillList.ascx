<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SkillList.ascx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Controls.SkillList" %>

<table width="100%" class="genericTable withHeader" role="presentation">
  <tr>
    <td class="tableHead" valign="top" width="33%" style="padding-left: 15px"><%= HtmlLocalise("SpecializedSkills.ColumnTitle", "SPECIALIZED SKILLS")%></td>
    <td class="tableHead" valign="top" width="33%" style="padding-left: 15px"><%= HtmlLocalise("SoftwareSkills.ColumnTitle", "SOFTWARE SKILLS")%></td>
    <td class="tableHead" valign="top" width="33%" style="padding-left: 15px"><%= HtmlLocalise("FoundationSkills.ColumnTitle", "FOUNDATION SKILLS")%></td>
  </tr>
	<tr>
		<td valign="top">
			<table>
				<asp:Repeater ID="SpecializedSkillsRepeater" runat="server" OnItemDataBound="SkillRepeater_ItemDataBound" 
				OnItemCommand="SkillRepeater_ItemCommand">
					<ItemTemplate>
						<tr>
							<td><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName")%></asp:LinkButton></strong></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>
		</td>
		<td valign="top">
			<table>
				<asp:Repeater ID="SoftwareSkillsRepeater" runat="server" OnItemDataBound="SkillRepeater_ItemDataBound" 
				OnItemCommand="SkillRepeater_ItemCommand">
					<ItemTemplate>
						<tr>
							<td><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName")%></asp:LinkButton></strong></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>
		</td>
		<td valign="top">
			<table>
				<asp:Repeater ID="FoundationSkillsRepeater" runat="server" OnItemDataBound="SkillRepeater_ItemDataBound" 
				OnItemCommand="SkillRepeater_ItemCommand">
					<ItemTemplate>
						<tr>
							<td><strong><asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="SkillNameLinkButton" runat="server" CssClass="modalLink" CommandArgument='<%# Eval("SkillId").ToString() %>' CommandName="ViewSkill"><%# Eval("SkillName")%></asp:LinkButton></strong></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>
		</td>
	</tr>
</table>
