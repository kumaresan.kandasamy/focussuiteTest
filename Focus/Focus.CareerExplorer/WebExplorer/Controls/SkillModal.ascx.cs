#region Copyright � 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models;

#endregion

namespace  Focus.CareerExplorer.WebExplorer.Controls
{
  public partial class SkillModal : ExplorerModalControl
	{
		public SkillDto Skill
		{
			get { return GetViewStateValue<SkillDto>("SkillModal:Skill"); }
			set { SetViewStateValue("SkillModal:Skill", value); }
		}

		public ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue<ExplorerCriteria>("SkillModal:ReportCriteria"); }
			set { SetViewStateValue("SkillModal:ReportCriteria", value); }
		}

		public List<LightboxBreadcrumb> Breadcrumbs
		{
			get { return GetViewStateValue("SkillModal:Breadcrumb", new List<LightboxBreadcrumb>()); }
			set { SetViewStateValue("SkillModal:Breadcrumb", value); }
		}

    public JobDegreeCertificationModel DegreeCertifications
    {
      get { return GetViewStateValue<JobDegreeCertificationModel>("JobModal:DegreeCertifications"); }
      set { SetViewStateValue("JobModal:DegreeCertifications", value); }
    }

    public List<JobDegreeEducationLevelModel> JobDegreesWithProgramAreas
    {
      get { return GetViewStateValue<List<JobDegreeEducationLevelModel>>("JobModal:JobDegreesWithProgramAreas"); }
      set { SetViewStateValue("JobModal:JobDegreesWithProgramAreas", value); }
    }

    public List<JobDegreeEducationLevelModel> JobDegreesWithoutProgramAreas
    {
      get { return GetViewStateValue<List<JobDegreeEducationLevelModel>>("JobModal:JobDegreesWithoutProgramAreas"); }
      set { SetViewStateValue("JobModal:JobDegreesWithoutProgramAreas", value); }
    }

    public struct ProgramArea
    {
      public long ProgramAreaId;
      public string ProgramAreaName;

      public string Name { get { return ProgramAreaName; } }
    }

    public struct Degree
    {
      public long DegreeId;
      public string DegreeName;

      public string Name { get { return DegreeName; } }
    }

    private List<JobDegreeEducationLevelModel> _jobDegreesWithProgramAreas;
    private List<JobDegreeEducationLevelModel> _jobDegreesWithoutProgramAreas;

    private bool _showSection2;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
    {
      _showSection2 = false;
    }

    /// <summary>
    /// Handles the pre-render event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
      LocaliseUI();
    }

    /// <summary>
    /// Localises any text on the page
    /// </summary>
    private void LocaliseUI()
    {
      DegreesHeader.Text = App.Settings.Theme == FocusThemes.Education
                             ? HtmlLocalise("Degree.Header", "#SCHOOLNAME# DEGREES").ToUpper(CultureInfo.CurrentCulture)
                             : HtmlLocalise("Degree.Header", "DEGREES");
      Section1Title.Text = CodeLocalise("HiringDemand.Header",
                                            "Jobs requiring this skill: hiring demand for the last 12 months");
    }

    /// <summary>
    /// Shows the specified skill id.
    /// </summary>
    /// <param name="skillId">The skill id.</param>
    /// <param name="criteria">The criteria.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="printFormat">The format to print the modal</param>
    public void Show(long skillId, ExplorerCriteria criteria, List<LightboxBreadcrumb> breadcrumbs, ModalPrintFormat printFormat = ModalPrintFormat.None)
		{
			var skill = ServiceClientLocator.ExplorerClient(App).GetSkill(skillId);

			Skill = skill;
      criteria.SkillId = skill.Id;
		  criteria.Skill = skill.Name;

      ReportCriteria = criteria;
      ReportCriteria.ReportType = ReportTypes.Skill;
			Breadcrumbs = breadcrumbs;
      CriteriaFilter.Bind(ReportTypes.Skill, criteria);

      ModalHeaderOptions.BindBreadcrumbs(Breadcrumbs);
			//if (breadcrumbs != null && breadcrumbs.Count == 3)
			//  Page.ClientScript.RegisterStartupScript(this.GetType(), "DisableLightboxModalLinks",
			//                                          String.Format("DisableLightboxModalLinks('skillLightBox','{0}');",
			//                                                        CodeLocalise("Modal.BreadcrumbLimitMessage",
			//                                                                     "The application only allows for three sets of data to be open at any one time.")),
			//                                          true);

			SkillNameLabel.Text = skill.Name;
			//LocationLabel.Text = String.Format("({0})", ServiceClientLocator.ExplorerClient(App).GetStateAreaName(criteria.StateAreaId));

      if (criteria.InternshipCategoryId.HasValue)
      {
        InternshipsList.ReportCriteria = criteria;
        InternshipsList.BindList(true);
        InternshipsList.Visible = true;

        InternshipsList.Visible = true;
        SkillJobList.Visible = false;

        Section1Title.Text = CodeLocalise("Internships.Header",
                                          "Internships with this skill: for the last 12 months");
        Section1ToolTip.Attributes["title"] = CodeLocalise("Internships.TooltipText",
																						"An #BUSINESS#:LOWER�s need for workers to fill their jobs is known as hiring demand. Select hiring demand to see the number of jobs, either nationally or by your geographic search area that #BUSINESSES#:LOWER posted over the last 12 months. Hiring demand is rated as low, medium, high, or very high for the area in your geographic search.");
      }
      else
      {
        SkillJobList.SkillId = skillId;
        SkillJobList.ReportCriteria = criteria;
        SkillJobList.ListSize = 50;
        SkillJobList.EnablePaging = true;
        SkillJobList.BindList(true);

        SkillJobList.Visible = true;
        InternshipsList.Visible = false;

        Section1Title.Text = CodeLocalise("HiringDemand.Header",
                                          "Jobs requiring this skill: hiring demand for the last 12 months");
        Section1ToolTip.Attributes["title"] = CodeLocalise("HiringDemand.TooltipText",
																						"An #BUSINESS#:LOWER�s need for workers to fill their jobs is known as hiring demand. Select hiring demand to see the number of jobs, either nationally or by your geographic search area that #BUSINESSES#:LOWER posted over the last 12 months. Hiring demand is rated as low, medium, high, or very high for the area in your geographic search.");
      }

      if (InternshipsList.RecordCount == 0 && SkillJobList.RecordCount == 0)
      {
        NoJobsLabel.Visible = true;
        NoJobsLabel.Text = CodeLocalise("NoJobs.Label", "No jobs have been advertised for this skill in recent months in your area.");
        InternshipsList.Visible = SkillJobList.Visible = false;
      }
      else
      {
        NoJobsLabel.Visible = false;
        NoJobsLabel.Text = "";
      }

			SkillEmployerList.SkillId = skillId;
			criteria.ListSize = 20;
			SkillEmployerList.ReportCriteria = criteria;
			SkillEmployerList.BindList();

      if (SkillEmployerList.Employers.Count == 0)
      {
        SkillEmployerList.Visible = false;
        NoEmployersMessage.Text = CodeLocalise("NoEmployersMessage.Text",
																							 "While there may have been numerous job openings for this skill in recent months, no #BUSINESS#:LOWER has advertised 10 or more openings in your area.");
        NoEmployersMessage.Visible = true;
      }
      else
      {
        SkillEmployerList.Visible = true;
        NoEmployersMessage.Text = "";
        NoEmployersMessage.Visible = false;
      }

      var showDegrees = false;

      if (_showSection2 && !criteria.InternshipCategoryId.HasValue && !criteria.DegreeEducationLevelId.HasValue && App.Settings.Theme == FocusThemes.Education)
      {
        showDegrees = true;

        var jobIds = new List<long>();
        jobIds.AddRange(SkillJobList.GetJobReports(JobListModes.Skill).Select(j => j.ReportData.JobId));

        ReportCriteria.CareerAreaJobIdList = jobIds;
        var degreeCertifications = ServiceClientLocator.ExplorerClient(App).GetJobDegreeReportCertifications(ReportCriteria);
        ReportCriteria.CareerAreaJobIdList = null;

		    var programAreaDegreeEducationLevels = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels();

        _jobDegreesWithProgramAreas = degreeCertifications.JobDegreeEducationLevelReports
		                                    .Join(programAreaDegreeEducationLevels, jd => jd.DegreeEducationLevelId, pad => pad.DegreeEducationLevelId,
		                                        (jd, pad) => new {JobDegree = jd, ProgramArea = pad})
		                                    .GroupBy(jd => new
		                                      {
                                            jd.JobDegree.DegreeEducationLevelId,
                                            jd.JobDegree.DegreeEducationLevelName, 
                                            jd.ProgramArea.ProgramAreaId,
                                            jd.ProgramArea.ProgramAreaName
		                                      })
                                        .Select(jd => new JobDegreeEducationLevelModel
		                                      {
                                            DegreeEducationLevelId = jd.Key.DegreeEducationLevelId,
                                            ProgramAreaId = jd.Key.ProgramAreaId,
                                            ProgramAreaName = jd.Key.ProgramAreaName,
                                            HiringDemand = jd.Max(grp => grp.JobDegree.HiringDemand),
                                            DegreeEducationLevelName = jd.Key.DegreeEducationLevelName
		                                      })
                                        .OrderByDescending(jd => jd.HiringDemand)
                                        .Take(5)
		                                    .ToList();

		    var programAreaDegreeIds = programAreaDegreeEducationLevels.Select(pad => pad.DegreeId).Distinct();
        _jobDegreesWithoutProgramAreas = degreeCertifications.JobDegreeEducationLevelReports
		                                        .Where(jd => !programAreaDegreeIds.Contains(jd.DegreeId))
		                                        .GroupBy(jd => new
		                                          {
                                                jd.DegreeEducationLevelId,
                                                jd.DegreeEducationLevelName, 
                                                jd.DegreeId,
                                                jd.DegreeName
		                                          })
                                            .Select(jd => new JobDegreeEducationLevelModel
 		                                          {
                                                DegreeEducationLevelId = jd.Key.DegreeEducationLevelId,
                                                DegreeId = jd.Key.DegreeId,
                                                DegreeName = jd.Key.DegreeName,
                                                HiringDemand = jd.Max(grp => grp.HiringDemand),
                                                DegreeEducationLevelName = jd.Key.DegreeEducationLevelName
		                                          })
                                            .OrderByDescending(jd => jd.HiringDemand)
                                            .Take(5)
		                                        .ToList();

        ProgramAreaRepeater.DataSource = _jobDegreesWithProgramAreas
                                           .Select(pa => new ProgramArea { ProgramAreaId = pa.ProgramAreaId, ProgramAreaName = pa.ProgramAreaName })
                                           .Distinct()
                                           .ToList();
		    ProgramAreaRepeater.DataBind();

        if (_jobDegreesWithProgramAreas.Count > 0)
        {
          ProgramAreaRepeater.DataSource = _jobDegreesWithProgramAreas
                                             .Select(pa => new ProgramArea { ProgramAreaId = pa.ProgramAreaId, ProgramAreaName = pa.ProgramAreaName })
                                             .Distinct()
                                             .ToList();
          ProgramAreaRepeater.DataBind();
        }
        else
        {
          ProgramAreaRepeater.Visible = false;
          NoDegreesFoundLabel.Text = CodeLocalise("NoDegreesFound.Label", "No Degrees Found for #SCHOOLNAME#");
          NoDegreesFoundLabel.Visible = true;
        }

        if (_jobDegreesWithoutProgramAreas.Count > 0)
        {
          DegreeRepeater.DataSource = _jobDegreesWithoutProgramAreas
                                        .Select(d => new Degree { DegreeId = d.DegreeId, DegreeName = d.DegreeName })
                                        .Distinct()
                                        .ToList();
          DegreeRepeater.DataBind();

          OtherDegreesPlaceHolder.Visible = true;
        }
        else
        {
          OtherDegreesPlaceHolder.Visible = false;
        }

        if (degreeCertifications.JobCertifications.Count > 0)
        {
          CertificationRepeater.Visible = true;
          NoCertificatesFoundLabel.Visible = false;
          CertificationRepeater.DataSource =
            degreeCertifications.JobCertifications.OrderByDescending(
              x => x.DemandPercentile).Take(5).ToList();
          CertificationRepeater.DataBind();
        }
        else
        {
          CertificationRepeater.Visible = false;
          NoCertificatesFoundLabel.Visible = true;
          NoCertificatesFoundLabel.Text = CodeLocalise("NoCertificatesFound.Label", "No Certificates Found");
        }

        DegreeCertifications = degreeCertifications;
		    JobDegreesWithProgramAreas = _jobDegreesWithProgramAreas;
		    JobDegreesWithoutProgramAreas = _jobDegreesWithoutProgramAreas;
		  }
        
      if (showDegrees)
      {
        SkillsDegreePlaceHolder.Visible = true;
      }
      else
      {
        SkillsDegreePlaceHolder.Visible = false;
        SkillEmployerOptionNumber.Text = @"2";
      }

			ModalPopup.Show();

			if (printFormat != ModalPrintFormat.None)
        PrintModal(printFormat);
		}

    /// <summary>
    /// Handles the ItemDataBound event of the ProgramAreaRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void ProgramAreaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var dataItem = (ProgramArea)e.Item.DataItem;
        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");

        degreeEducationLevelRepeater.DataSource = _jobDegreesWithProgramAreas.Where(jdel => jdel.ProgramAreaId == dataItem.ProgramAreaId);
        degreeEducationLevelRepeater.DataBind();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void DegreeRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var dataItem = (Degree)e.Item.DataItem;
        var degreeEducationLevelRepeater = (Repeater)e.Item.FindControl("DegreeEducationLevelRepeater");

        degreeEducationLevelRepeater.DataSource = _jobDegreesWithoutProgramAreas.Where(jdel => jdel.DegreeId == dataItem.DegreeId);
        degreeEducationLevelRepeater.DataBind();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeEducationLevelRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void DegreeEducationLevelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
      {
        var degreeEducationLevel = (JobDegreeEducationLevelModel)e.Item.DataItem;
        var degreeEducationLevelLinkButton = (LinkButton)e.Item.FindControl("DegreeEducationLevelLinkButton");

        degreeEducationLevelLinkButton.Text = degreeEducationLevel.DegreeEducationLevelName;
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the CertificationRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void CertificationRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      // Add number before item
      var rankLiteral = (Literal)e.Item.FindControl("CertificationRankLiteral");
      rankLiteral.Text = (e.Item.ItemIndex + 1).ToString(CultureInfo.CurrentCulture);
    }

    /// <summary>
    /// Handles the Click event of the BookmarkLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void BookmarkLinkButton_Click(object sender, EventArgs e)
		{
			OnBookmarkClicked(new LightboxEventArgs
			                  	{
			                  		CommandName = "BookmarkSkill",
			                  		CommandArgument = new LightboxEventCommandArguement
			                  		                  	{
			                  		                  		Id = Skill.Id.Value,
			                  		                  		Breadcrumbs = Breadcrumbs,
			                  		                  		LightboxName = SkillNameLabel.Text,
																									LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                  		                  	}
			                  	});
		}

    /// <summary>
    /// Handles the Click event of the SendEmailImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SendEmailImageButton_Click(object sender, EventArgs e)
		{
			OnSendEmailClicked(new LightboxEventArgs
			                   	{
			                   		CommandName = "SendSkillEmail",
			                   		CommandArgument = new LightboxEventCommandArguement
			                   		                  	{
			                   		                  		Id = Skill.Id.Value,
			                   		                  		Breadcrumbs = Breadcrumbs,
			                   		                  		LightboxName = SkillNameLabel.Text,
                                                  LightboxLocation = "",//LocationLabel.Text
                                                  CriteriaHolder = ReportCriteria
			                   		                  	}
			                   	});
		}

    /// <summary>
    /// Handles the Click event of the PrintImageButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="eventArgs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void ModalHeaderOptions_OnPrintCommand(object sender, CommandEventArgs eventArgs)
    {
      var printFormat = (ModalPrintFormat)Enum.Parse(typeof(ModalPrintFormat), eventArgs.CommandArgument.ToString(), true);
			if (ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
        PrintModal(printFormat);

				ModalPopup.Show();
			}
			else
			{
				OnPrintClicked(new LightboxEventArgs
				               	{
				               		CommandName = "PrintSkill",
				               		CommandArgument = new LightboxEventCommandArguement
				               		                  	{
				               		                  		Id = Skill.Id.Value,
                                                Breadcrumbs = Breadcrumbs,
                                                LightboxName = SkillNameLabel.Text,
                                                LightboxLocation = "",//LocationLabel.Text
                                                CriteriaHolder = ReportCriteria,
                                                PrintFormat = printFormat
				               		                  	}
				               	});
			}
		}

    /// <summary>
    /// Handles the Click event of the CloseButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CloseButton_Click(object sender, EventArgs e)
		{
			ModalPopup.Hide();
		}

    /// <summary>
    /// Handles the ItemClicked event of the SkillModalBreadcrumb control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void SkillModalBreadcrumb_ItemClicked(object sender, CommandEventArgs e)
		{
			long itemId;
			if (long.TryParse(e.CommandArgument.ToString(), out itemId))
			{
				OnItemClicked(new CommandEventArgs(e.CommandName,
                                           new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = Breadcrumbs, MovingBack = true }));
			}
		}

    /// <summary>
    /// Handles the Cancelled event of the Filter control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.FilterEventArgs"/> instance containing the event data.</param>
    protected void Filter_Cancelled(object sender, FilterEventArgs e)
    {
      var oldCriteria = e.CommandArgument.OldCriteria;
      var newCriteria = e.CommandArgument.NewCriteria;

      var breadcrumb = new LightboxBreadcrumb { LinkType = ReportTypes.Skill, LinkId = Skill.Id.GetValueOrDefault(), LinkText = Skill.Name, Criteria = oldCriteria };
      var breadcrumbs = Breadcrumbs;
      breadcrumbs.Add(breadcrumb);

      OnItemClicked(new CommandEventArgs("ViewSkill",
                                         new LightboxEventCommandArguement
                                         {
                                           Id = oldCriteria.SkillId.GetValueOrDefault(),
                                           Breadcrumbs = breadcrumbs,
                                           CriteriaHolder = newCriteria
                                         }));
    }

    /// <summary>
    /// Handles the ItemClicked event of the ModalList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ModalList_ItemClicked(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "ViewJob":
				case "ViewEmployer":
        case "ViewInternship":
        case "ViewDegreeCertification":
					long itemId;
					if (long.TryParse(e.CommandArgument.ToString(), out itemId))
					{
						var breadcrumb = new LightboxBreadcrumb() { LinkType = ReportTypes.Skill, LinkId = Skill.Id.Value, LinkText = Skill.Name, Criteria = ReportCriteria};
						var breadcrumbs = Breadcrumbs;
						breadcrumbs.Add(breadcrumb);

						OnItemClicked(new CommandEventArgs(e.CommandName,
																							 new LightboxEventCommandArguement { Id = itemId, Breadcrumbs = breadcrumbs }));
					}
					break;
			}
		}

    /// <summary>
    /// Prints the modal.
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintModal(ModalPrintFormat printFormat = ModalPrintFormat.Html)
		{
			var printModel = new ExplorerSkillPrintModel
			{
				Name = SkillNameLabel.Text,
        Location = ReportCriteria.StateArea,//LocationLabel.Text,
				Jobs = !ReportCriteria.InternshipCategoryId.HasValue? SkillJobList.GetJobReports(JobListModes.Skill).Take(10).ToList() : null,
				Employers = SkillEmployerList.Employers,
        Internships = ReportCriteria.InternshipCategoryId.HasValue ? InternshipsList.GetInternshipReports() : null,
        DegreeCertifications = DegreeCertifications,
        JobDegreesWithProgramAreas = JobDegreesWithProgramAreas,
        JobDegreesWithoutProgramAreas = JobDegreesWithoutProgramAreas,
        ReportCriteria = ReportCriteria
			};

			const string sessionKey = "Explorer:SkillPrintModel";
			App.SetSessionValue(sessionKey, printModel);

      OpenPrintWindow(ReportTypes.Skill, printFormat);
		}
	}
}