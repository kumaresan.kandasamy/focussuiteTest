<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DegreeCertificationList.ascx.cs"
  Inherits=" Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationList" %>
<table width="100%" class="accordionWithIcon" role="presentation">
  <tr class="multipleAccordionTitle2 on">
    <td valign="top" width="5%">
      <span class="accordionIcon"></span>
    </td>
    <td valign="top" width="95%">
      <%= HtmlLocalise("DegreeList.Header.CertificateAndAssociateDegrees", "Certificate and Associate Degrees")%> 
    </td>
  </tr>
  <tr class="accordionContent2 open">
    <td colspan="2">
      <asp:UpdatePanel ID="DegreeListUpdatePanel" runat="server" ChildrenAsTriggers="True"
        UpdateMode="Conditional">
        <ContentTemplate>
          <asp:ListView ID="DegreeListView" runat="server" DataSourceID="DegreeListViewODS"
            ItemPlaceholderID="DegreeListViewPlaceholder" OnItemDataBound="DegreeListView_ItemDataBound"
            OnSorting="DegreeListView_Sorting" OnItemCommand="DegreeListView_ItemCommand">
            <LayoutTemplate>
              <table width="100%" class="genericTable withHeader" role="presentation">
                <thead>
                  <tr>
                    <td class="tableHead" valign="top" width="70%">
                      <asp:LinkButton ID="DegreeSortButton" runat="server" CommandName="Sort" CommandArgument="DegreeName"><%= HtmlLocalise("DegreeName.ColumnTitle", "DEGREES")%></asp:LinkButton>
                      <asp:Image ID="DegreeSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Degree Sort Image" />
                    </td>
                    <td class="tableHead" valign="top" width="30%">
                      <asp:LinkButton ID="EducationLevelSortButton" runat="server" CommandName="Sort" CommandArgument="EducationLevel"><%= HtmlLocalise("EducationLevel.ColumnTitle", "EDUCATION LEVEL")%></asp:LinkButton>
                      <asp:Image ID="EducationLevelSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Education Level Sort Image" />
                    </td>
                  </tr>
                </thead>
                <tr>
                  <td colspan="3">
                    <table width="100%" role="presentation">
                      <tr id="DegreeListViewPlaceholder" runat="server">
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </LayoutTemplate>
            <ItemTemplate>
              <tr>
                <td width="5%">
                </td>
                <td width="65%">
                  <strong>
                    <asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="DegreeNameLinkButton"
                      runat="server" CssClass="modalLink" CommandArgument='<%# Eval("DegreeEducationLevelId").ToString() %>'
                      CommandName="ViewDegreeCertification"><%# Eval("DegreeName")%></asp:LinkButton></strong>
                </td>
                <td width="30%">
                  <div style="height: 19px;">
                    <asp:Label ID="EducationLevelLabel" runat="server" />
                  </div>
                </td>
              </tr>
            </ItemTemplate>
          </asp:ListView>
        </ContentTemplate>
      </asp:UpdatePanel>
      <asp:PlaceHolder runat="server" id="DegreeAreaTable" runat="server">
      <table width="100%" class="genericTable withHeader" role="presentation">
        <thead>
          <tr>
            <td class="tableHead" valign="top" colspan="2">
              <a href="#">
                <%= HtmlLocalise("DegreeArea.ColumnTitle", "DEGREE AREAS")%></a>
            </td>
          </tr>
        </thead>
        <asp:Repeater ID="DegreeAreaRepeater" runat="server" OnItemDataBound="DegreeAreaRepeater_ItemDataBound"
          OnItemCommand="DegreeAreaRepeater_ItemCommand">
          <ItemTemplate>
            <tr>
              <td width="3%">
              </td>
              <td width="97%">
                <strong>
                  <asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="DegreeAreaNameLinkButton"
                    runat="server" CssClass="modalLink" CommandArgument='<%# Eval("DegreeEducationLevelId").ToString() %>'
                    CommandName="ViewDegreeCertification"><%# Eval("DegreeEducationLevelName")%></asp:LinkButton></strong>
              </td>
            </tr>
          </ItemTemplate>
        </asp:Repeater>
      </table>
      </asp:PlaceHolder>
    </td>
  </tr>
  <tr class="multipleAccordionTitle2">
    <td valign="top" width="5%">
      <span class="accordionIcon"></span>
    </td>
    <td valign="top" width="95%">
      <%= HtmlLocalise("DegreeList.Header.BachelorsDegrees", "Bachelors Degrees")%>
    </td>
  </tr>
  <tr class="accordionContent2">
    <td colspan="2">
      <asp:UpdatePanel ID="BachelorsDegreesUpdatePanel" runat="server" ChildrenAsTriggers="True"
        UpdateMode="Conditional">
        <ContentTemplate>
          <asp:ListView ID="BachelorsDegreesListView" runat="server" DataSourceID="BachelorsDegreesSource"
            ItemPlaceholderID="BachelorsDegreesPlaceholder" OnItemDataBound="DegreeListView_ItemDataBound"
            OnSorting="DegreeListView_Sorting" OnItemCommand="DegreeListView_ItemCommand">
            <LayoutTemplate>
              <table width="100%" class="genericTable withHeader" role="presentation">
                <thead>
                  <tr>
                    <td class="tableHead" valign="top" width="70%">
                      <asp:LinkButton ID="DegreeSortButton" runat="server" CommandName="Sort" CommandArgument="DegreeName"><%= HtmlLocalise("DegreeName.ColumnTitle", "DEGREES")%></asp:LinkButton>
                      <asp:Image ID="DegreeSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Degree Sort Image"/>
                    </td>
                    <td class="tableHead" valign="top" width="30%">
                      <asp:LinkButton ID="EducationLevelSortButton" runat="server" CommandName="Sort" CommandArgument="EducationLevel"><%= HtmlLocalise("EducationLevel.ColumnTitle", "EDUCATION LEVEL")%></asp:LinkButton>
                      <asp:Image ID="EducationLevelSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Education Level Sort Image"/>
                    </td>
                  </tr>
                </thead>
                <tr>
                  <td colspan="3">
                    <table width="100%" role="presentation">
                      <tr id="BachelorsDegreesPlaceholder" runat="server">
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </LayoutTemplate>
            <ItemTemplate>
              <tr>
                <td width="5%">
                </td>
                <td width="65%">
                  <strong>
                    <asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="DegreeNameLinkButton"
                      runat="server" CssClass="modalLink" CommandArgument='<%# Eval("DegreeEducationLevelId").ToString() %>'
                      CommandName="ViewDegreeCertification"><%# Eval("DegreeName")%></asp:LinkButton></strong>
                </td>
                <td width="30%">
                  <div style="height: 19px;">
                    <asp:Label ID="EducationLevelLabel" runat="server" />
                  </div>
                </td>
              </tr>
            </ItemTemplate>
          </asp:ListView>
        </ContentTemplate>
      </asp:UpdatePanel>
      <asp:PlaceHolder id="BachelorsDegreesTable" runat="server">
      <table width="100%" class="genericTable withHeader" role="presentation">
        <thead>
          <tr>
            <td class="tableHead" valign="top" colspan="2">
              <a href="#">
                <%= HtmlLocalise("DegreeArea.ColumnTitle", "DEGREE AREAS")%></a>
            </td>
          </tr>
        </thead>
        <asp:Repeater ID="BachelorsDegreesRepeater" runat="server" OnItemDataBound="DegreeAreaRepeater_ItemDataBound"
          OnItemCommand="DegreeAreaRepeater_ItemCommand">
          <ItemTemplate>
            <tr>
              <td width="3%">
              </td>
              <td width="97%">
                <strong>
                  <asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="DegreeAreaNameLinkButton"
                    runat="server" CssClass="modalLink" CommandArgument='<%# Eval("DegreeEducationLevelId").ToString() %>'
                    CommandName="ViewDegreeCertification"><%# Eval("DegreeEducationLevelName")%></asp:LinkButton></strong>
              </td>
            </tr>
          </ItemTemplate>
        </asp:Repeater>
      </table>
      </asp:PlaceHolder>
    </td>
  </tr>
  <tr class="multipleAccordionTitle2">
    <td valign="top" width="5%">
      <span class="accordionIcon"></span>
    </td>
    <td valign="top" width="95%">
      <%= HtmlLocalise("DegreeList.Header.GraduateDegreesAndCertificates", "Graduate Degrees and Certificates")%>
    </td>
  </tr>
  <tr class="accordionContent2">
    <td colspan="2">
      <asp:UpdatePanel ID="GraduateDegreesAndCertificatesUpdatePanel" runat="server" ChildrenAsTriggers="True"
        UpdateMode="Conditional">
        <ContentTemplate>
          <asp:ListView ID="GraduateDegreesAndCertificatesListView" runat="server" DataSourceID="GraduateDegreesAndCertificatesSource"
            ItemPlaceholderID="GraduateDegreesAndCertificatesPlaceholder" OnItemDataBound="DegreeListView_ItemDataBound"
            OnSorting="DegreeListView_Sorting" OnItemCommand="DegreeListView_ItemCommand">
            <LayoutTemplate>
              <table width="100%" class="genericTable withHeader" role="presentation">
                <thead>
                  <tr>
                    <td class="tableHead" valign="top" width="70%">
                      <asp:LinkButton ID="DegreeSortButton" runat="server" CommandName="Sort" CommandArgument="DegreeName"><%= HtmlLocalise("DegreeName.ColumnTitle", "DEGREES")%></asp:LinkButton>
                      <asp:Image ID="DegreeSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Degree Sort Image"/>
                    </td>
                    <td class="tableHead" valign="top" width="30%">
                      <asp:LinkButton ID="EducationLevelSortButton" runat="server" CommandName="Sort" CommandArgument="EducationLevel"><%= HtmlLocalise("EducationLevel.ColumnTitle", "EDUCATION LEVEL")%></asp:LinkButton>
                      <asp:Image ID="EducationLevelSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Education Level Sort Image" />
                    </td>
                  </tr>
                </thead>
                <tr>
                  <td colspan="3">
                    <table width="100%" role="presentation">
                      <tr id="GraduateDegreesAndCertificatesPlaceholder" runat="server">
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </LayoutTemplate>
            <ItemTemplate>
              <tr>
                <td width="5%">
                </td>
                <td width="65%">
                  <strong>
                    <asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="DegreeNameLinkButton"
                      runat="server" CssClass="modalLink" CommandArgument='<%# Eval("DegreeEducationLevelId").ToString() %>'
                      CommandName="ViewDegreeCertification"><%# Eval("DegreeName")%></asp:LinkButton></strong>
                </td>
                <td width="30%">
                  <div style="height: 19px;">
                    <asp:Label ID="EducationLevelLabel" runat="server" />
                  </div>
                </td>
              </tr>
            </ItemTemplate>
          </asp:ListView>
        </ContentTemplate>
      </asp:UpdatePanel>
      <asp:PlaceHolder id="GraduateDegreesAndCertificatesTable" runat="server">
      <table width="100%" class="genericTable withHeader" role="presentation">
        <thead>
          <tr>
            <td class="tableHead" valign="top" colspan="2">
              <a href="#">
                <%= HtmlLocalise("DegreeArea.ColumnTitle", "DEGREE AREAS")%></a>
            </td>
          </tr>
        </thead>
        <asp:Repeater ID="GraduateDegreesAndCertificatesRepeater" runat="server" OnItemDataBound="DegreeAreaRepeater_ItemDataBound"
          OnItemCommand="DegreeAreaRepeater_ItemCommand">
          <ItemTemplate>
            <tr>
              <td width="3%">
              </td>
              <td width="97%">
                <strong>
                  <asp:Literal ID="RankLiteral" runat="server" />.&nbsp;&nbsp;<asp:LinkButton ID="DegreeAreaNameLinkButton"
                    runat="server" CssClass="modalLink" CommandArgument='<%# Eval("DegreeEducationLevelId").ToString() %>'
                    CommandName="ViewDegreeCertification"><%# Eval("DegreeEducationLevelName")%></asp:LinkButton></strong>
              </td>
            </tr>
          </ItemTemplate>
        </asp:Repeater>
      </table>
      </asp:PlaceHolder>
    </td>
  </tr>
</table>


<asp:ObjectDataSource ID="DegreeListViewODS" runat="server" TypeName=" Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationList"
  EnablePaging="False" SelectMethod="GetDegreeEducationLevelReports" SortParameterName="orderBy"
  OnSelecting="DegreeListViewODS_Selecting">
  <SelectParameters>
    <asp:Parameter Name="reportCriteria" DbType="Object" />
    <asp:Parameter Name="listSize" DbType="Int32" />
    <asp:Parameter Name="degreeLevel" DbType="String" DefaultValue="CertificateOrAssociate" />
  </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="BachelorsDegreesSource" runat="server" TypeName=" Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationList"
  EnablePaging="False" SelectMethod="GetDegreeEducationLevelReports" SortParameterName="orderBy"
  OnSelecting="DegreeListViewODS_Selecting">
  <SelectParameters>
    <asp:Parameter Name="reportCriteria" DbType="Object" />
    <asp:Parameter Name="listSize" DbType="Int32" />
    <asp:Parameter Name="degreeLevel" DbType="String" DefaultValue="Bachelors" />
  </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="GraduateDegreesAndCertificatesSource" runat="server" TypeName=" Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationList"
  EnablePaging="False" SelectMethod="GetDegreeEducationLevelReports" SortParameterName="orderBy"
  OnSelecting="DegreeListViewODS_Selecting">
  <SelectParameters>
    <asp:Parameter Name="reportCriteria" DbType="Object" />
    <asp:Parameter Name="listSize" DbType="Int32" />
    <asp:Parameter Name="degreeLevel" DbType="String" DefaultValue="GraduateOrProfessional" />
  </SelectParameters>
</asp:ObjectDataSource>
