<%@ Page Title="Print" Language="C#" AutoEventWireup="true" CodeBehind="Print.aspx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Print" %>
<%@ Import Namespace="Focus.Common.Helpers" %>

<%@ Register src="Controls/DegreeCertificationPrint.ascx" tagName="DegreeCertificationPrint" tagPrefix="uc" %>
<%@ Register src="Controls/ProgramAreaDegreeStudyPrint.ascx" tagName="ProgramAreaDegreePrint" tagPrefix="uc" %>
<%@ Register src="Controls/EmployerPrint.ascx" tagName="EmployerPrint" tagPrefix="uc" %>
<%@ Register src="Controls/InternshipPrint.ascx" tagName="InternshipPrint" tagPrefix="uc" %>
<%@ Register src="Controls/JobPrint.ascx" tagName="JobPrint" tagPrefix="uc" %>
<%@ Register src="Controls/SkillPrint.ascx" tagName="SkillPrint" tagPrefix="uc" %>
<%@ Register src="Controls/CareerAreaPrint.ascx" tagName="CareerAreaPrint" tagPrefix="uc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
  <link href="<%= UrlHelper.GetCacheBusterUrl(string.Format("~/Branding/{0}/Focus.CareerExplorer.css", App.Settings.BrandIdentifier)) %>" rel="stylesheet" type="text/css" />
	<link href="<%= UrlHelper.GetCacheBusterUrl("~/Assets/Css/Focus.Device.css") %>" rel="stylesheet" type="text/css"/>
	<script src="<%= ResolveUrl("~/Assets/Scripts/jquery-1.9.1.min.js") %>" type="text/javascript"></script>
  <script language="javascript" type="text/javascript">
    function DisplaySessionTimeout() { }
  </script>
</head>
<body>
	<form id="form1" runat="server">
    <div>
			<uc:DegreeCertificationPrint ID="DegreeCertificationPrint" runat="server" Visible="False" />
			<uc:ProgramAreaDegreePrint ID="ProgramAreaDegreePrint" runat="server" Visible="False" />
			<uc:EmployerPrint ID="EmployerPrint" runat="server" Visible="False" />
			<uc:InternshipPrint ID="InternshipPrint" runat="server" Visible="False" />
			<uc:JobPrint ID="JobPrint" runat="server" Visible="False" />
			<uc:SkillPrint ID="SkillPrint" runat="server" Visible="False" />
			<uc:CareerAreaPrint ID="CareerAreaPrint" runat="server" Visible="False" />
		</div>
	</form>
  <asp:PlaceHolder runat="server" ID="ScriptHolder">
	<script language="javascript" type="text/javascript">
			window.print();
	</script>
  </asp:PlaceHolder>
</body>
</html>
