﻿<%@ Page Title="Search Result" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchResults.aspx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.SearchResults" %>

<%@ MasterType VirtualPath="~/Site.Master" %>

<%@ Register src="../Controls/TabNavigations.ascx" tagName="TabNavigation" tagPrefix="uc" %>
<%@ Register src="Controls/JobModal.ascx" tagName="JobModal" tagPrefix="uc" %>
<%@ Register src="Controls/EmployerModal.ascx" tagName="EmployerModal" tagPrefix="uc" %>
<%@ Register src="Controls/DegreeCertificationModal.ascx" tagName="DegreeCertificationModal" tagPrefix="uc" %>
<%@ Register src="Controls/ProgramAreaDegreeStudyModal.ascx" tagName="ProgramAreaDegreeStudyModal" tagPrefix="uc" %>
<%@ Register src="Controls/SkillModal.ascx" tagName="SkillModal" tagPrefix="uc" %>
<%@ Register src="Controls/InternshipModal.ascx" tagName="InternshipModal" tagPrefix="uc" %>

<%@ Register src="Controls/EmailModal.ascx" tagName="EmailModal" tagPrefix="uc" %>
<%@ Register src="../Controls/ConfirmationModal.ascx" tagName="ConfirmationModal" tagPrefix="uc" %>
<%@ Register Src="../Controls/UnAuthorizedInformation.ascx" TagName="UnAuthorizedModal" TagPrefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
	<asp:Literal ID="GoogleTrackingLiteral" runat="server" />
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	
  <div style="float: left;position:relative;bottom:20px;">
    <asp:Literal ID="ArrowsLiteral" runat="server" />
	  <asp:LinkButton ID="BackButton" runat="server" OnClick="BackButton_Click" Text="Back" />
  </div>
	<div style="float: right;position:relative;bottom:20px;">
		<table role="presentation">
			<tr>
				<td valign="top">
					<%= HtmlInFieldLabel("SearchTermTextBox", "SearchCareersAndEducation.Label", "Search careers and education", 300)%>
					<asp:TextBox ID="SearchTermTextBox" runat="server" ClientIDMode="Static" Width="250px" />
					<asp:RequiredFieldValidator ID="SearchTermRequired" runat="server" ControlToValidate="SearchTermTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="SiteSearch" />
					<asp:CustomValidator ID="SearchTermValidator" runat="server" CssClass="error" ClientValidationFunction="SiteSearchTerm_Validate" SetFocusOnError="true" Display="Dynamic" ValidationGroup="SiteSearch" ValidateEmptyText="False"></asp:CustomValidator>
				</td>
				<td valign="top">
					<asp:Button ID="SearchGoButton" runat="server" ClientIDMode="Static" CausesValidation="True" OnClick="SearchGoButton_Click" class="buttonLevel2" ValidationGroup="SiteSearch"/>
				</td>
			</tr>
		</table>
	</div>

	<h1 style="clear: both">Search Results</h1>

	<p><%= HtmlLocalise("YouSearchedFor.Text", "You searched for: ") %><strong><asp:Label ID="SearchTermLabel" runat="server" /></strong></p>

	<table width="100%" class="accordionWithIcon" role="presentation">
		<tr id="JobResultsTitleTableRow" class="multipleAccordionTitle">
			<td valign="top" width="5%">
				<span class="accordionIcon"></span>
			</td>
			<td width="95%"><%= HtmlLocalise("Jobs.Label", "Jobs") %> <asp:Label ID="JobResultsLabel" runat="server" /></td>
		</tr>
		<tr class="accordionContent">
			<td colspan="2">
				<table width="100%" role="presentation">
					<tr>
						<td width="5%"></td>
						<td width="50%" valign="top">
							<table role="presentation">
								<asp:Repeater ID="JobLeftColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="JobLeftColumnLinkButton" runat="server" CommandName="ViewJob" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<td valign="top">
							<table role="presentation">
								<asp:Repeater ID="JobRightColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="JobRightColumnLinkButton" runat="server" CommandName="ViewJob" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="EmployerResultsTitleTableRow" class="multipleAccordionTitle">
			<td valign="top">
				<span class="accordionIcon"></span>
			</td>
			<td><%= HtmlLocalise("Employers.Label", "#BUSINESSES#")%> <asp:Label ID="EmployerResultsLabel" runat="server" /></td>
		</tr>
		<tr class="accordionContent">
			<td colspan="2">
				<table width="100%" role="presentation">
					<tr>
						<td width="5%"></td>
						<td width="50%" valign="top">
							<table role="presentation">
								<asp:Repeater ID="EmployerLeftColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="EmployerLeftColumnLinkButton" runat="server" CommandName="ViewEmployer" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<td valign="top">
							<table role="presentation">
								<asp:Repeater ID="EmployerRightColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="EmployerRightColumnLinkButton" runat="server" CommandName="ViewEmployer" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="DegreeCertificationResultsTitleTableRow" class="multipleAccordionTitle">
			<td valign="top">
				<span class="accordionIcon"></span>
			</td>
			<td><%= HtmlLocalise("DegreesCertificates.Label", "Degrees & certificates") %> <asp:Label ID="DegreeCertificationResultsLabel" runat="server" /></td>
		</tr>
		<tr class="accordionContent">
			<td colspan="2">
				<table width="100%" role="presentation">
					<tr>
						<td width="5%"></td>
						<td width="50%" valign="top">
							<table role="presentation">
								<asp:Repeater ID="DegreeCertificationLeftColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="DegreeCertificationLeftColumnLinkButton" runat="server" CommandName="ViewDegreeCertification" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<td valign="top">
							<table role="presentation">
								<asp:Repeater ID="DegreeCertificationRightColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="DegreeCertificationRightColumnLinkButton" runat="server" CommandName="ViewDegreeCertification" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
					</tr>
				</table>				
			</td>
		</tr>
    <asp:PlaceHolder runat="server" id="ProgramAreaPlaceHolder" Visible="False">
      <tr id="ProgramAreaResultsTitleTableRow" class="multipleAccordionTitle">
			  <td valign="top">
				  <span class="accordionIcon"></span>
			  </td>
			  <td><%= HtmlLocalise("ProgramAreas.Label", "Departments") %> <asp:Label ID="ProgramAreaResultsLabel" runat="server" /></td>
		  </tr>
      <tr class="accordionContent">
			  <td colspan="2">
				  <table width="100%" role="presentation">
					  <tr>
						  <td width="5%"></td>
						  <td width="50%" valign="top">
							  <table role="presentation">
								  <asp:Repeater ID="ProgramAreaLeftColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									  <ItemTemplate>
										  <tr>
											  <td>
												  <strong><asp:LinkButton ID="ProgramAreaLeftColumnLinkButton" runat="server" CommandName="ViewProgramAreaDegree" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											  </td>
										  </tr>
									  </ItemTemplate>
								  </asp:Repeater>
							  </table>
						  </td>
						  <td valign="top">
							  <table role="presentation">
								  <asp:Repeater ID="ProgramAreaRightColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									  <ItemTemplate>
										  <tr>
											  <td>
												  <strong><asp:LinkButton ID="ProgramAreaLeftColumnLinkButton" runat="server" CommandName="ViewProgramAreaDegree" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											  </td>
										  </tr>
									  </ItemTemplate>
								  </asp:Repeater>
							  </table>
						  </td>
					  </tr>
				  </table>				
			  </td>
      </tr>
    </asp:PlaceHolder>
		<tr id="SkillResultsTitleTableRow" class="multipleAccordionTitle">
			<td valign="top">
				<span class="accordionIcon"></span>
			</td>
			<td><%= HtmlLocalise("Skills.Label", "Skills") %> <asp:Label ID="SkillResultsLabel" runat="server" /></td>
		</tr>
		<tr class="accordionContent">
			<td colspan="2">
				<table width="100%" role="presentation">
					<tr>
						<td width="5%"></td>
						<td width="50%" valign="top">
							<table role="presentation">
								<asp:Repeater ID="SkillLeftColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="SkillLeftColumnLinkButton" runat="server" CommandName="ViewSkill" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<td valign="top">
							<table role="presentation">
								<asp:Repeater ID="SkillRightColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="SkillRightColumnLinkButton" runat="server" CommandName="ViewSkill" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="InternshipResultsTitleTableRow" class="multipleAccordionTitle">
			<td valign="top">
				<span class="accordionIcon"></span>
			</td>
			<td><%= HtmlLocalise("Internships.Label", "Internships") %> <asp:Label ID="InternshipResultsLabel" runat="server" /></td>
		</tr>
		<tr class="accordionContent">
			<td colspan="2">
				<table width="100%" role="presentation">
					<tr>
						<td width="5%"></td>
						<td width="50%" valign="top">
							<table role="presentation">
								<asp:Repeater ID="InternshipLeftColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="InternshipLeftColumnLinkButton" runat="server" CommandName="ViewInternship" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
						<td valign="top">
							<table role="presentation">
								<asp:Repeater ID="InternshipRightColumnRepeater" runat="server" OnItemCommand="ResultRepeater_ItemCommand">
									<ItemTemplate>
										<tr>
											<td>
												<strong><asp:LinkButton ID="InternshipRightColumnLinkButton" runat="server" CommandName="ViewInternship" CommandArgument='<%# Eval("ResultId") %>'><%# Eval("ResultName") %></asp:LinkButton></strong>
											</td>
										</tr>
									</ItemTemplate>
								</asp:Repeater>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<%-- Modals --%>
	<uc:JobModal ID="Job" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
	<uc:EmployerModal ID="Employer" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
	<uc:DegreeCertificationModal ID="DegreeCertification" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
	<uc:ProgramAreaDegreeStudyModal ID="ProgramAreaDegreeStudy" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
	<uc:SkillModal ID="Skill" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
	<uc:InternshipModal ID="Internship" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />

	<uc:EmailModal ID="Email" runat="server" OnCancelled="Email_Cancelled" OnCompleted="Email_Completed" />
	<uc:ConfirmationModal ID="Confirmation" runat="server" OnCloseCommand="Confirmation_CloseCommand" />
	<uc:UnAuthorizedModal ID="UnAuthorizedModal" runat="server" />

	<script language="javascript" type="text/javascript">
		// following script will rebind tooltips after update panel postback
		var prm = Sys.WebForms.PageRequestManager.getInstance();
		prm.add_endRequest(function () {
			BindTooltips();
		});
	</script>
</asp:Content>
