﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Text;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.CareerExplorer.WebAuth.Controls;
using Focus.Common.Extensions;
using Focus.Core;

#endregion

namespace  Focus.CareerExplorer.WebExplorer
{
	public partial class Home : PageBase
	{
    public string ExploreSections
    {
      get
      {
        var sectionOrder = App.Settings.ExplorerSectionOrder.Split('|');

        return string.Concat("['", String.Join("','", sectionOrder), "']");
      }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (App.IsAnonymousAccess())
        Response.Redirect(UrlBuilder.JobSearchCriteria());

			if (App.Settings.Module == FocusModules.Explorer)
			{
				JobSearchPlaceHolder.Visible = false;
			}

			#region Show a disclaimer of there is one to show

			var disclaimer = CodeLocalise("Global.ExplorerDisclaimer.Text", "");

			if (string.IsNullOrEmpty(disclaimer))
				DisclaimerLiteral.Visible = false;
			else
			{
				DisclaimerLiteral.Text = @"<div class='disclaimer'>" + disclaimer + @"</div>";
				DisclaimerLiteral.Visible = true;
			}
		
			#endregion

      SchoolSectionImage.ImageUrl = UrlBuilder.ExploreSectionStudyImage();
      SchoolSectionHyperLink.NavigateUrl = UrlBuilder.Explore("school");
      SchoolSectionHyperLink.Text = CodeLocalise("School.LinkText", "Explore careers by #SCHOOLNAME# programs of study");

      // Ordering is now done client side
      ExploreSectionImage.ImageUrl = UrlBuilder.ExploreSectionExploreImage();
      ExploreSectionHyperLink.NavigateUrl = UrlBuilder.Explore("explore");
      ExploreSectionHyperLink.Text = CodeLocalise("Explore.LinkText", "Explore my career and internship options");

      ResearchSectionImage.ImageUrl = UrlBuilder.ExploreSectionResearchImage();
      ResearchSectionHyperLink.NavigateUrl = UrlBuilder.Explore("research");
      ResearchSectionHyperLink.Text = App.Settings.Theme == FocusThemes.Education
				? HtmlLocalise("Explorer.Label.ResearchStudy.Education.LinkText", "Research a program of study, job, #BUSINESS#:LOWER or skill")
				: HtmlLocalise("Explorer.Label.ResearchStudy.Education.LinkText", "Research a specific program of study, career or #BUSINESS#:LOWER");

      StudySectionImage.ImageUrl = UrlBuilder.ExploreSectionStudyImage();
      StudySectionHyperLink.NavigateUrl = UrlBuilder.Explore("study");
      StudySectionHyperLink.Text = CodeLocalise("Study.LinkText", "See what I can study to get ahead");

      ExperienceSectionImage.ImageUrl = UrlBuilder.ExploreSectionExperienceImage();
      ExperienceSectionHyperLink.NavigateUrl = UrlBuilder.Explore("experience");
      ExperienceSectionHyperLink.Text = CodeLocalise("Experience.LinkText", "See where my experience can take me");

			if (App.Settings.ExplorerGoogleEventTrackingEnabled)
			{
        var eventCalls = new StringBuilder();
        var sectionOrder = App.Settings.ExplorerSectionOrder.Split('|');

        foreach (var section in sectionOrder)
        {
          var link = (HyperLink)Page.FindControl(string.Concat(section, "SectionHyperLink"));
          var action = section == "Explore" ? "ExploreOptions" : section;

          eventCalls.AppendFormat("ddGoogleClickEvent('{0}', '/home', 'Explore', '{1}');\n", link.ClientID, action);
        }

				var js =
					String.Format(
						@"
	<script src=""/Assets/Scripts/googleTrack.js"" type=""text/javascript""></script>
	<script type=""text/javascript"">
		$(document).ready(function ()
		{{
			googlePageTrackerUrl = '/home';
			AddGooglePageEvents('home');
      {0}
		}});
	</script>",
            eventCalls);

				GoogleTrackingLiteral.Text = js;
			} 
		}
	}
}