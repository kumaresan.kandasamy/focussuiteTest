﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Framework.Core;

using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;
using Focus.CareerExplorer.Code;

#endregion
 
namespace  Focus.CareerExplorer.WebExplorer
{
  public partial class Report : CareerExplorerPageBase, IExplorerJobModalPage
	{
		#region page properties

		protected ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue("Explorer:ReportCriteria", new ExplorerCriteria()); }
			set { SetViewStateValue("Explorer:ReportCriteria", value); }
		}

		protected LightboxEventArgs SendEmailCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:SendEmailCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:SendEmailCompletedEventArgs", value); }
		}

		protected LightboxEventArgs BookmarkCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:BookmarkCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:BookmarkCompletedEventArgs", value); }
		}

		protected LightboxEventArgs PrintCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:PrintCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:PrintCompletedEventArgs", value); }
		}

    public ExplorerCriteria ReturnReportCriteria
    {
      get { return ReportCriteria; }
    }

		#endregion

		#region page events

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
			RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education);

      base.OnInit(e);

			Master.LogInComplete += Master_LogInComplete;
			Master.LogInCancelled += Master_LogInCancelled;
			Master.LogOutComplete += Master_LogOutComplete;
		}

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
        if (App.GuideToResume())
          Response.Redirect(UrlBuilder.YourResume());

			  ExplorerCriteria sessionCriteria;
        LightboxEventArgs returnBookmark;

				ApplyBranding();
				LocaliseUI();
				BindControls();

				var criteria = ReportCriteria;

				#region check route data

        if ((returnBookmark = App.GetSessionValue<LightboxEventArgs>("Explorer:OpenBookmark")) != null)
        {
          criteria = returnBookmark.CommandArgument.MainPageCriteria;
          switch (returnBookmark.CommandName)
          {
            case "Job":
              Job.Show(returnBookmark.CommandArgument.Id, returnBookmark.CommandArgument.CriteriaHolder, returnBookmark.CommandArgument.Breadcrumbs);
              break;
          }
          App.RemoveSessionValue("Explorer:OpenBookmark");
        }
        else if (Page.RouteData.Values.ContainsKey("bookmarkId"))
				{
                    Page.Title = CodeLocalise("ExplorerReportBookmark", @"Explorer - Report Bookmark");
					long bookmarkId;
					if (long.TryParse(Page.RouteData.Values["bookmarkId"].ToString(), out bookmarkId))
					{
						var bookmark = ServiceClientLocator.AnnotationClient(App).GetBookmark(bookmarkId);
						if (bookmark != null && bookmark.Criteria.IsNotNullOrEmpty())
						{
							criteria = (ExplorerCriteria) bookmark.Criteria.DeserializeJson(typeof (ExplorerCriteria));
						}
					}
				}
        else if ((sessionCriteria = App.GetSessionValue<ExplorerCriteria>("Explorer:ReportCriteria")) != null)
        {
            Page.Title = CodeLocalise("ExplorerOpenBookmark", @"Explorer - Report Criteria");
          criteria = sessionCriteria;
          App.RemoveSessionValue("Explorer:ReportCriteria");
        }
				else
				{
					if (Page.RouteData.Values.ContainsKey("type"))
					{
                        Page.Title = CodeLocalise("ExplorerReportForType", @"Explorer - Report for type");
						var type = Page.RouteData.Values["type"].ToString();
						ReportTypes reportType;
						Enum.TryParse(type, true, out reportType);

						criteria.ReportType = reportType;
						ReportTypeDropDownList.SelectedValue = reportType.ToString();

						if (Page.RouteData.Values.ContainsKey("stateAreaId"))
						{
                            
                            
							long stateAreaId;
              if (long.TryParse(Page.RouteData.Values["stateAreaId"].ToString(), out stateAreaId))
              {
                criteria.StateAreaId = stateAreaId;
                criteria.StateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(stateAreaId).StateAreaName;
                Page.Title = CodeLocalise("ExplorerReportForTypeState", @"Explorer - Report for state " + criteria.StateArea);
                if (Page.RouteData.Values.ContainsKey("careerAreaId"))
                {
                  long careerAreaId;
                  if (long.TryParse(Page.RouteData.Values["careerAreaId"].ToString(), out careerAreaId))
                  {
                    var careerArea = ServiceClientLocator.ExplorerClient(App).GetCareerAreas().FirstOrDefault(x => x.Id == careerAreaId);
                    
                    if (careerArea != null)
                    {
                      criteria.CareerArea = careerArea.Name;
                      criteria.CareerAreaId = careerArea.Id;
                      Page.Title = CodeLocalise("ExplorerReportForTypeStateAndCareerArea", @"Explorer - Report for state career area " + criteria.CareerArea);
                    }
                  }
                }
              }
						}

					    if (Page.RouteData.Values.ContainsKey("careerAreaOccupationType"))
					    {
					        CareerAreaRoutes(criteria);
                            Page.Title = CodeLocalise("ExplorerReportForTypeStateAndOccupation", @"Explorer - Report for type state and occupation ");
					    }

					}
				}

				#endregion

        ReportCriteria = criteria;
        SetControlVisibility();
				
				BindReportCriteria();

				LoadReport();
			}
			else
			{
				#region manually re-bind ui controls that have been changed client side

        if (Request.Form[StateAreaGoButton.UniqueID].IsNullOrEmpty() && ReportStateAreaId.Value.Length > 0)
        {
          ReportStateAreaSelector.ResetStateArea(long.Parse(ReportStateAreaId.Value));
        }

				// need to check if career area selector reset client side
				if (CareerAreaDropDownList.SelectedIndex <= 0
					&& String.IsNullOrEmpty(JobTitleAutoCompleteTextBox.SelectedValue)
          && String.IsNullOrEmpty(MilitaryOccupationTextBox.SelectedValue)
					&& CareerAreaLabel.Text != CareerAreaDropDownList.Items[0].Text)
				{
					CareerAreaLabel.Text = CareerAreaDropDownList.Items[0].Text;
				}

				// need to check if education selector reset client side
				if (EducationOptionNonSpecificRadioButton.Checked 
					&& EducationOptionNonSpecificDropDownList.SelectedIndex <= 0
					&& EducationLabel.Text != EducationOptionNonSpecificDropDownList.Items[0].Text)
				{
					EducationLabel.Text = EducationOptionNonSpecificDropDownList.Items[0].Text;
					EducationCurrentLabel.Text = EducationOptionNonSpecificDropDownList.Items[0].Text;
				}

				
        if (App.Settings.Theme == FocusThemes.Education && App.Settings.ShowProgramArea)
        {// need to rebind the degree drop down as we are not using ajaxtoolkit cascading drop downs
          if (!String.IsNullOrEmpty(StudyProgramAreaDropDownList.SelectedValue))
          {
            StudyProgramAreaDegreesDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(StudyProgramAreaDropDownList.SelectedValueToLong());
            StudyProgramAreaDegreesDropDownList.DataTextField = "DegreeEducationLevelName";
            StudyProgramAreaDegreesDropDownList.DataValueField = "DegreeEducationLevelId";
            StudyProgramAreaDegreesDropDownList.DataBind();
            StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.AllDegrees", "All Degrees", string.Concat("-", StudyProgramAreaDropDownList.SelectedValue));
            StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

            var selectedDegree = Request.Form[StudyProgramAreaDegreesDropDownList.UniqueID];
            if (selectedDegree.IsNotNullOrEmpty() && StudyProgramAreaDegreesDropDownList.Items.FindByValue(selectedDegree).IsNotNull())
            {
              StudyProgramAreaDegreesDropDownList.SelectedValue = selectedDegree;
            }
          }
          BindElementJavaScriptEvents();
        }

        if (!String.IsNullOrEmpty(DetailedDegreeLevelDropDownList.SelectedValue))
        {
          if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
            BindDetailedDegreeDropDownList();
        }

        #endregion
			}

			#region show hide bookmark

			BookmarkImage.Visible = true;
			BookmarkLinkButton.Visible = true;
      if (App.IsAnonymousAccess() || (!App.Settings.ExplorerShowBookmarkForNonLoggedInUser && !ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated)))
			{
				BookmarkImage.Visible = false;
				BookmarkLinkButton.Visible = false;
			}

			#endregion

			#region Register Client Script

			#region google tracking

			string js;
			if (App.Settings.ExplorerGoogleEventTrackingEnabled)
			{
				js = String.Format(
					@"
	<script src=""/Assets/Scripts/googleTrack.js"" type=""text/javascript""></script>
	<script type=""text/javascript"">
		$(document).ready(function ()
		{{
			googlePageTrackerUrl = '{0}';
			AddGooglePageEvents('report');

			AddGoogleChangeEvent('{1}', '', 'ExploreReport', 'ReportSelectType', true);

			$('#{2}').click(function ()
			{{
				var careerAreaSelection = GetCareerAreaSelection();
				if (careerAreaSelection.length > 0)
				{{
					GoogleTrackEvent(googlePageTrackerUrl, 'ExploreReport', 'CareerAreaGo', careerAreaSelection);
				}}
			}});

			$('#{3}').click(function ()
			{{
				var educationSelection = GetEducationSelection();
				if (educationSelection.length > 0)
				{{
					GoogleTrackEvent(googlePageTrackerUrl, 'ExploreReport', 'EducationGo', educationSelection);
				}}
			}});

			AddGoogleClickEvent('TellUsAboutSkillsFilter', '', 'ExploreReport', 'TellUsAboutSkills');
			AddGoogleClickEvent('{4}', '', 'ExploreReport', 'Bookmark');
		}});
	</script>",
					Request.Url.AbsolutePath,
					ReportTypeDropDownList.ClientID,
					CareerAreaGoButton.ClientID,
					EducationGoButton.ClientID,
					BookmarkLinkButton.ClientID);

				GoogleTrackingLiteral.Text = js;
			}

			#endregion

			#region career area selector

			js =
				String.Format(
          @"
$(document).ready(function() {{
	$('#{0}').change(function ()
	{{
    $('#{4}').prop('checked', true);

    if ($(this).prop('selectedIndex') > 0)
		{{
      if ($('#{2}').length > 0)
      {{
			  if ($('#{2}').val().length > 0)
			  {{
				  $('#{2}').val('');
          $('#{2}').blur();
			  }}
      }}

			if ($('#{1}').val().length > 0)
			{{
				$('#{1}').val('');
        $('#{1}').blur();
			}}

			if (!$('#{3}').is(':checked'))
			{{
				DoResetEducationSelector();
			}}
		}}
	}});
}});",
					CareerAreaDropDownList.ClientID, JobTitleAutoCompleteTextBox.ClientID,
          MilitaryOccupationTextBox.ClientID, 
          EducationOptionNonSpecificRadioButton.ClientID, CareerAreaRadioButton.ClientID);

			Page.ClientScript.RegisterStartupScript(GetType(), "CareerAreaDropDownOnChange", js, true);

			js =
				String.Format(
          @"
function ResetCareerAreaDropDown(radioChecked)
{{
	if ($('#{0}').prop('selectedIndex') > 0)
	{{
		$('#{0}').prop('selectedIndex', 0);
		var spanBox = $('#{0}').next().find(':first-child');
		if (spanBox) {{
			spanBox.text('{1}');
		}}
	}}

  if (radioChecked == 'jobtitle')
    $('#{2}').prop('checked', true);
  else
    $('#{3}').prop('checked', true);
}}",
					CareerAreaDropDownList.ClientID, CodeLocalise("CareerArea.TopDefault", "All Career Areas"),
          JobTitleRadioButton.ClientID, MilitaryOccupationRadioButton.ClientID);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "ResetCareerAreaDropDown", js, true);

			js =
				String.Format(
					@"
function DoResetCareerAreaSelector()
{{
	if (ResetCareerAreaSelector('{0}', '{1}', '{2}', '{3}'))
	{{
		$('#EducationSelectorResetTableRow').show();
	}}
}}",
					CareerAreaLabel.ClientID, CodeLocalise("CareerArea.TopDefault", "All Career Areas"),
					CareerAreaDropDownList.ClientID, JobTitleAutoCompleteTextBox.ClientID);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "DoResetCareerAreaSelector", js, true);

			js =
				String.Format(
					@"
function GetCareerAreaSelection()
{{
	var careerAreaSelection = '';
	if ($('#{0}').val() && $('#{0}').val().length > 0)
	{{
		careerAreaSelection = $('#{1}').val();
	}}
	else
	{{
		careerAreaSelection = $('#{2}').children('option:selected').text();
	}}

	return careerAreaSelection;
}}",
					JobTitleAutoCompleteTextBox.SelectedValueClientId, JobTitleAutoCompleteTextBox.ClientID,
					CareerAreaDropDownList.ClientID);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "GetCareerAreaSelection", js, true);

      js =
        String.Format(
          @"
$(document).ready(function ()
{{
	$('#{0}').click(function ()
	{{
		if ($('#{2}').length > 0 && $('#{2}').val().length > 0) 
    {{
      $('#{2}').val('');
      $('#{2}').blur();
		}}

		if ($('#{1}').val().length > 0) 
    {{
			$('#{1}').val('');
			$('#{1}').blur();
		}}
	}});
}});",
          CareerAreaRadioButton.ClientID, JobTitleAutoCompleteTextBox.ClientID, MilitaryOccupationTextBox.ClientID);

      Page.ClientScript.RegisterClientScriptBlock(GetType(), "CareerAreaRadioButtonSelected", js, true);

      CareerAreaDisplayArea.Style["display"] = "none";

			#endregion

			#region education selector

      if (App.Settings.Theme == FocusThemes.Education && App.Settings.ShowProgramArea)
      {

        js = String.Format(
          @"
$(document).ready(function ()
{{
  checkDegreeSubPanels();

	$('#{0}').click(function ()
	{{
    checkDegreeSubPanels();
  }});

	$('#{1}').click(function ()
	{{
    checkDegreeSubPanels();
  }});
}});

function checkDegreeSubPanels()
{{
  if ($('#{0}').prop('checked'))
  {{
    $('#{2}').show();
    $('#{3}').show();
    $('#{4}').hide();
    $('#{5}').hide();

    $('#{8}').prop('selectedIndex', 0);
    $('#{8}').change();
    $('#{9}').prop('selectedIndex', 0);
    $('#{9}').change();
  }}
  else
  {{
    $('#{2}').hide();
    $('#{3}').hide();
    $('#{4}').show();
    $('#{5}').show();

		$('#{6}').val('');
		$('#{6}').blur();
    $('#{7}').prop('selectedIndex', 0);
  }}
}}",
          DegreeRadioButton.ClientID, ProgramAreaRadioButton.ClientID,
          DegreeRow1.ClientID, DegreeRow2.ClientID,
          ProgramAreaRow1.ClientID, ProgramAreaRow2.ClientID,
          DegreeCertificationAutoCompleteTextBox.ClientID, DetailedDegreeLevelDropDownList.ClientID,
          StudyProgramAreaDropDownList.ClientID, StudyProgramAreaDegreesDropDownList.ClientID);

        Page.ClientScript.RegisterStartupScript(GetType(), "EducationReady", js, true);
      }

      js =
				String.Format(
          @"
$(document).ready(function ()
{{
	$('#{0}').change(function ()
	{{
		$('#{1}').prop('checked', true);

		if ($('#{2}').length > 0 && $('#{2}').val().length > 0) {{
			$('#{2}').val('');
			$('#{2}').blur();
		}}

    $('#{4}').prop('selectedIndex', 0);
    $('#{4}').change();
    $('#{5}').prop('selectedIndex', 0);
    $('#{5}').change();
    $('#{6}').prop('selectedIndex', 0);
    $('#{6}').change();

		if ($('#{3}').val().length > 0)
		{{
			DoResetCareerAreaSelector();
		}}
	}});

	$('#{1}').click(function ()
	{{
		if ($('#{2}').length > 0 && $('#{2}').val().length > 0) {{
			$('#{2}').val('');
			$('#{2}').blur();
		}}

    $('#{4}').prop('selectedIndex', 0);
    $('#{4}').change();
    $('#{5}').prop('selectedIndex', 0);
    $('#{5}').change();
    $('#{6}').prop('selectedIndex', 0);
    $('#{6}').change();
	}});
}});",
					EducationOptionNonSpecificDropDownList.ClientID, EducationOptionNonSpecificRadioButton.ClientID,
					DegreeCertificationAutoCompleteTextBox.ClientID, JobTitleAutoCompleteTextBox.ClientID,
          StudyProgramAreaDropDownList.ClientID, StudyProgramAreaDegreesDropDownList.ClientID,
          DetailedDegreeLevelDropDownList.ClientID);

			Page.ClientScript.RegisterStartupScript(GetType(), "EducationDropDownOnChange", js, true);

			js =
				String.Format(
					@"
function ResetEducationDropDown()
{{
	if ($('#{0}').prop('selectedIndex') > 0)
	{{
		$('#{0}').prop('selectedIndex', 0);
		var spanBox = $('#{0}').next().find(':first-child');
		if (spanBox) {{
			spanBox.text('{1}');
		}}
	}}

	$('#{2}').prop('checked', true);
	$('#{3}').prop('checked', true);
}}",
					EducationOptionNonSpecificDropDownList.ClientID, CodeLocalise(DegreeLevels.AnyOrNoDegree, "Any/No Degree"),
          EducationOptionSpecificRadioButton.ClientID, DegreeRadioButton.ClientID);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "ResetEducationDropDown", js, true);

			js =
				 String.Format(
					 @"
function DoResetEducationSelector()
{{
	if (ResetEducationSelector('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}'))
	{{
		$('#CareerAreaSelectorResetTableRow').show();
	}}
	 Page_ClientValidate(""CareerAreas"");
}}
",
					 EducationLabel.ClientID,
					 CodeLocalise(DegreeLevels.AnyOrNoDegree, "Any/No Degree"),
					 EducationCurrentLabel.ClientID, EducationOptionNonSpecificRadioButton.ClientID,
           EducationOptionNonSpecificDropDownList.ClientID, DegreeCertificationAutoCompleteTextBox.ClientID, ProgramAreaRadioButton.ClientID, StudyProgramAreaDropDownList.ClientID, StudyProgramAreaDegreesDropDownList.ClientID);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "DoResetEducationSelector", js, true);

			js =
				String.Format(
          @"
function GetEducationSelection()
{{
	var educationSelection = '';
	if ($('#{0}').prop('checked'))
	{{
		educationSelection = $('#{1}').children('option:selected').text();
	}}
	else
	{{
		if ($('#{2}').length > 0 && $('#{2}').val() && $('#{2}').val().length > 0)
		{{
			educationSelection = $('#{3}').val();
		}}
    else
    {{
		  if ($('#{4}').length > 0 && $('#{4}').val() && $('#{4}').val().length > 0)
		  {{
			  educationSelection = $('#{4}').val();
		  }}
    }}
	}}

	return educationSelection;
}}",
					EducationOptionNonSpecificRadioButton.ClientID, EducationOptionNonSpecificDropDownList.ClientID,
          DegreeCertificationAutoCompleteTextBox.SelectedValueClientId, DegreeCertificationAutoCompleteTextBox.ClientID,
          DetailedDegreeDropDownList.ClientID);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "GetEducationSelection", js, true);


      js = String.Format(
  @"
$(document).ready(function() {{
	$('#{0}').change(function () {{
    var degreeBox = $('#{1}');
    if (degreeBox.length > 0) {{
	    degreeBox.val('').blur();
	    $('#{2}').val('');
    }}
	}});
}});",
  DetailedDegreeLevelDropDownList.ClientID,
  DegreeCertificationAutoCompleteTextBox.ClientID,
  DegreeCertificationAutoCompleteTextBox.SelectedValueClientId);

      Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "DetailedDegreeLevelDropDownListOnChange", js, true);
			#endregion

      #endregion
		}

		#endregion

		#region bookmark and search events

    /// <summary>
    /// Handles the Click event of the BookmarkLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void BookmarkLinkButton_Click(object sender, EventArgs e)
		{
			if (!ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				UnAuthorizedModal.Show();
			}
			else
			{
				var name = CodeLocalise("BookmarkName.Text", "Top {0} {1} for people with {2} in {3}{4}",
													ListSizeDropDownList.SelectedValue, ReportTypeDropDownList.SelectedItem.Text.ToLower(),
													EducationLabel.Text.ToLower(), CareerAreaLabel.Text.ToLower(),
                          String.IsNullOrEmpty(StateAreaSelectorLabel.Text)
														? String.Empty
														: String.Concat(" ", CodeLocalise("Global.In.Text", "In").ToLower(), " ",
                                            StateAreaSelectorLabel.Text));

				CreateBookmark(name, BookmarkTypes.Report, ReportCriteria);
			}
		}

    /// <summary>
    /// Handles the Click event of the SearchGoButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SearchGoButton_Click(object sender, EventArgs e)
		{
      App.SetSessionValue("Explorer:ReportCriteria", ReportCriteria);
			string url = UrlBuilder.Search(HttpUtility.UrlEncode(SearchTermTextBox.Text).Replace("+", "%20"));
			Response.Redirect(url);
		}

		#endregion

		#region report criteria and list events

    /// <summary>
    /// Handles the SelectedIndexChanged event of the ReportTypeDropDownList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ReportTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
		{
			var criteria = ReportCriteria;
			criteria.ReportType = ReportTypeDropDownList.SelectedValueToEnum(ReportTypes.Job);

			switch (criteria.ReportType)
			{
				case ReportTypes.Job:
					ReportTypeDropDownList.ToolTip = CodeLocalise("ReportTypeJob.TooltipText", "Shown nationally or by geographic area, these are the occupations most in demand, with at least 10 job openings over the last 12 months.");
					break;

				case ReportTypes.Employer:
					ReportTypeDropDownList.ToolTip = CodeLocalise("ReportTypeEmployer.TooltipText", "Shown nationally or by geographic area, these are the #BUSINESSES#:LOWER most in demand, who have listed at least 10 job openings over the last 12 months.");
					break;

				case ReportTypes.DegreeCertification:
					ReportTypeDropDownList.ToolTip = CodeLocalise("ReportTypeDegreeCertification.TooltipText", "Shown nationally or by geographic search area, these degrees/certificates are most commonly needed. These data are synthesized from numerous sources, including #BUSINESS#:LOWER job postings, resume data of people in an occupation, information provided by partner organizations, and qualitative research.");
					break;

				case ReportTypes.Skill:
					ReportTypeDropDownList.ToolTip = CodeLocalise("ReportTypeSkill.TooltipText", "Skills are shown nationally for #BUSINESSES#:LOWER who listed at least 10 job openings for this occupation over the last 12 months.");
					break;

				case ReportTypes.Internship:
					ReportTypeDropDownList.ToolTip = CodeLocalise("ReportTypeInternship.TooltipText", "Shown nationally or by geographic area, these internship categories have been the most commonly requested over the last 12 months. Internships are grouped into 22 categories based on the primary skill area listed in the openings.");
					break;

        case ReportTypes.CareerMoves:
          // TODO: Need text here
					ReportTypeDropDownList.ToolTip = CodeLocalise("ReportTypeCareerMoves.TooltipText", "See where my experience can take me<br /><br />");
					break;

        case ReportTypes.CareerArea:
					ReportTypeDropDownList.ToolTip = CodeLocalise("ReportTypeCareerAReas.TooltipText", "Shows the number of jobs in a given location for each career area<br /><br />");
          break;
      }

      ReportCriteria = criteria;

      SetControlVisibility();

      SetReportCriteria(ReportCriteria, false);

			LoadReport();
		}

    /// <summary>
    /// Handles the Click event of the CareerAreaGoButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CareerAreaGoButton_Click(object sender, EventArgs e)
		{
      if (RelatedJobsDropDown.SelectedValueToNullableLong().IsNotNull())
      {
        var criteria = new ExplorerCriteria();
        SetLocationCriteria(criteria);
        SetCareerAreaCriteria(criteria, true);
        
        Job.Show(RelatedJobsDropDown.SelectedValueToNullableLong().Value, criteria, null);
      }
			SetReportCriteria(ReportCriteria, false);
			LoadReport();
		}


		/// <summary>
		/// Handles the Click event of the EducationGoButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void EducationGoButton_Click(object sender, EventArgs e)
		{
		  if (EducationOptionSpecificRadioButton.Checked &&
           ((DegreeRadioButton.Checked && DegreeCertificationAutoCompleteTextBox.SelectedValueToNullableLong != null) ||
             DetailedDegreeDropDownList.SelectedValueToNullableLong() != null || 
             (ProgramAreaRadioButton.Checked && StudyProgramAreaDropDownList.SelectedIndex != 0)))
		  {
		    var criteria = new ExplorerCriteria();
        SetLocationCriteria(criteria);
        criteria.ReportType = ReportTypes.DegreeCertification;
		    SetEducationCriteria(criteria, true);

        if (DegreeRadioButton.Checked)
        {
          if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
            DegreeCertification.Show(DetailedDegreeDropDownList.SelectedValueToNullableLong().GetValueOrDefault(0), criteria, null);
          else
            DegreeCertification.Show(DegreeCertificationAutoCompleteTextBox.SelectedValueToNullableLong.GetValueOrDefault(0), criteria, null);
        }
        else if (ProgramAreaRadioButton.Checked)
        {
          if (StudyProgramAreaDegreesDropDownList.SelectedIndex > 1)
            DegreeCertification.Show(StudyProgramAreaDegreesDropDownList.SelectedValueToLong(), criteria, null);
          else if (StudyProgramAreaDropDownList.SelectedIndex != 0)
            ProgramAreaDegreeStudy.Show(StudyProgramAreaDropDownList.SelectedValueToLong(), criteria, null);
        }
		  }
      SetReportCriteria(ReportCriteria, false);
		  LoadReport();
		}
    
    /// <summary>
    /// Handles the SelectedValueChanged event of the EmployerAutoCompleteTextBox control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void EmployerAutoCompleteTextBox_SelectedValueChanged(object sender, EventArgs e)
		{
      if (EmployerAutoCompleteTextBox.SelectedValueToNullableLong != null)
      {
        var criteria = new ExplorerCriteria();
        SetLocationCriteria(criteria);
        SetEmployerCriteria(criteria);
        Employer.Show(EmployerAutoCompleteTextBox.SelectedValueToNullableLong.Value, criteria, null);
      }

      SetReportCriteria(ReportCriteria, false);
       LoadReport();
		}

    /*
		protected void LocationAutoCompleteTextBox_SelectedValueChanged(object sender, EventArgs e)
		{
      SetReportCriteria(ReportCriteria, false);
			LoadReport();
		}
     */

    /// <summary>
    /// Handles the SelectedIndexChanged event of the ListSizeDropDownList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ListSizeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
		{
      SetReportCriteria(ReportCriteria, false);
			LoadReport();
		}

    
   /// <summary>
    /// Handles the ItemClicked event of the ReportList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void ReportList_ItemClicked(object sender, CommandEventArgs e)
		{
      // Check to make sure we have an Id to display
		  long entitiyId;
      if (!long.TryParse(e.CommandArgument.ToString(), out entitiyId))
        return;
      
      // Get the criteria
      var criteria = new ExplorerCriteria();
      if (e.CommandName == "ViewInternship")
        SetLocationCriteria(criteria);
      else
        SetReportCriteria(criteria, true);

      //criteria.CareerAreaId = null;
      //criteria.CareerArea = null;

      // Show the right modal
			switch (e.CommandName)
			{
				case "ViewJob":
          Job.Show(entitiyId, criteria, null);
					break;

        case "ViewEmployer":
          Employer.Show(entitiyId, criteria, null);
					break;

        case "ViewDegreeCertification":
          ResetCareerArea(criteria);
          DegreeCertification.Show(entitiyId, criteria, null);
					break;

        case "ViewProgramAreaDegree":
          ProgramAreaDegreeStudy.Show(entitiyId, criteria, null);
			    break;

        case "ViewSkill":
          Skill.Show(entitiyId, criteria, null);
					break;

        case "ViewInternship":
          Internship.Show(entitiyId, criteria, null);
					break;

        case "ViewCareerArea":
          CareerArea.Show(entitiyId, criteria, null);
          break;
			}
		}

    /// <summary>
    /// Handles the SelectedValueChanged event of the JobTitleAutoCompleteTextBox control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void JobTitleAutoCompleteTextBox_SelectedValueChanged(object sender, EventArgs e)
    {
      // To do: Add new rows and pass job title id to method to return related jobs. (Also make sure criteria and bookmarked jobs work)
      if (!String.IsNullOrEmpty(JobTitleAutoCompleteTextBox.SelectedValue))
      {
        CareerAreaDisplayArea.Style["display"] = "block";
        RelatedJobsSelectorTableRow.Visible = true;
        BindRelatedJobs(Convert.ToInt64(JobTitleAutoCompleteTextBox.SelectedValue), null);

        if (App.Settings.Theme == FocusThemes.Education)
        {
          MilitaryOccupationTextBox.Text = "";
          MilitaryOccupationTextBox.SelectedValue = "";
          StudyProgramAreaDegreesDropDownList.Items.Clear();
          StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.AllDegrees", "All Degrees", string.Concat("-", StudyProgramAreaDropDownList.SelectedValue));
          StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");
        }
      }
      else
      {
        CareerAreaDisplayArea.Style["display"] = (CareerAreaRadioButton.Checked ? "block" : "none");
        RelatedJobsSelectorTableRow.Visible = false;
        if (RelatedJobsDropDown.Items.Count > 0)
          RelatedJobsDropDown.SelectedIndex = 0;
      }
    }

    /// <summary>
    /// Fires when the 'Go' button is clicked to change the state/area
    /// </summary>
    /// <param name="sender">The button raising the event</param>
    /// <param name="e">Standard Event Arguments</param>
	  protected void StateAreaGoButton_Click(object sender, EventArgs e)
	  {
      var stateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(ReportStateAreaSelector.AreaSelectedValue.GetValueOrDefault(0));

      ReportStateAreaId.Value = stateArea.Id.ToString();
      StateAreaSelectorLabel.Text = stateArea.StateSortOrder == 0 ? stateArea.StateName : stateArea.StateAreaName;

      SetReportCriteria(ReportCriteria, false);
      LoadReport();
	  }

	  #endregion
    
		#region modal action events

		#region send email, bookmark and print

		#region send email

		/// <summary>
		/// Handles the SendEmailClicked event of the Modal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
		protected void Modal_SendEmailClicked(object sender, LightboxEventArgs e)
		{
			SendEmailCompletedEventArgs = e;
			
			if (!ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				UnAuthorizedModal.Show();
			}
			else
			{
				var args = e.CommandArgument;

				switch (e.CommandName)
				{
					case "SendJobEmail":
					case "SendEmployerEmail":
					case "SendDegreeCertificationEmail":
					case "SendSkillEmail":
					case "SendInternshipEmail":
          case "SendCareerAreaEmail":
            Email.Show(args.CriteriaHolder, args.Id, args.LightboxName, args.LightboxLocation);
            break;
        }
			}
		}

		protected void Email_Cancelled(object sender, EventArgs e)
		{
			SendEmailCompleted();
		}

		protected void Email_Completed(object sender, EventArgs e)
		{
			Confirmation.Show(CodeLocalise("EmailConfirmation.Title", "Email Confirmation"),
			                  CodeLocalise("EmailConfirmation.Body", "Your email has been sent"),
			                  CodeLocalise("Global.Close.Text", "Close"), "EmailConfirmationClose", height: 100, width: 250);
		}

		private void SendEmailCompleted()
		{
			var args = SendEmailCompletedEventArgs;
		  var criteria = args.CommandArgument.CriteriaHolder ?? ReportCriteria;

			switch(args.CommandName)
			{
				case "SendJobEmail":
          Job.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs);

					break;

				case "SendEmployerEmail":
          Employer.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs);

					break;

				case "SendDegreeCertificationEmail":
          DegreeCertification.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs);

					break;

				case "SendSkillEmail":
          Skill.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs);

					break;

				case "SendInternshipEmail":
          Internship.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs);

					break;

        case "SendCareerAreaEmail":
          CareerArea.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs);

          break;
      }

			SendEmailCompletedEventArgs = null;
		}

		#endregion

		#region bookmark

		protected void Modal_BookmarkClicked(object sender, LightboxEventArgs e)
		{
			BookmarkCompletedEventArgs = e;

			if (!ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				UnAuthorizedModal.Show();
			}
			else
			{
				var name = String.Format("{0} {1}", e.CommandArgument.LightboxName, e.CommandArgument.LightboxLocation);
				var criteria = e.CommandArgument.CriteriaHolder;
				criteria.ReportItemId = e.CommandArgument.Id;

				CreateBookmark(name, BookmarkTypes.ReportItem, criteria);				
			}
		}

		private void BookmarkCompleted()
		{
			var args = BookmarkCompletedEventArgs;
			if (args != null)
			{
				switch (args.CommandName)
				{
					case "BookmarkJob":
            Job.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);

						break;

					case "BookmarkEmployer":
            Employer.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);

						break;

					case "BookmarkDegreeCertification":
            DegreeCertification.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);

						break;

          case "BookmarkProgramAreaDegreeStudy":
            ProgramAreaDegreeStudy.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);

            break;

					case "BookmarkSkill":
            Skill.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);

						break;

					case "BookmarkInternship":
            Internship.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);

						break;

          case "BookmarkCareerArea":
            CareerArea.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);

            break;
				}

				BookmarkCompletedEventArgs = null;
			}
		}

		#endregion

		#region print

		protected void Modal_PrintClicked(object sender, LightboxEventArgs e)
		{
			PrintCompletedEventArgs = e;

			UnAuthorizedModal.Show();
		}

    /// <summary>
    /// Performs a print of the modal
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintCompleted(ModalPrintFormat printFormat)
		{
			var args = PrintCompletedEventArgs;
		  var criteria = args.CommandArgument.CriteriaHolder;
			switch (args.CommandName)
			{
				case "PrintJob":
          Job.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
					break;

				case "PrintEmployer":
          Employer.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintDegreeCertification":
          DegreeCertification.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

        case "PrintProgramAreaDegreeStudy":
          ProgramAreaDegreeStudy.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintSkill":
          Skill.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintInternship":
          Internship.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

        case "PrintCareerArea":
          CareerArea.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;
      }
		}

		#endregion

		#region login/register/logout events

		/// <summary>
		/// Handles the LogInComplete event of the Master control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected bool Master_LogInComplete(object sender, CommandEventArgs e)
		{
		  var redirectionOverride = false;
			if (!String.IsNullOrEmpty(e.CommandName))
			{
				switch (e.CommandName)
				{
					case "BookmarkJob":
					case "BookmarkEmployer":
					case "BookmarkDegreeCertification":
          case "BookmarkProgramAreaDegreeStudy":
					case "BookmarkSkill":
					case "BookmarkInternship":
          case "BookmarkCareerArea":
						var bookmarkArgs = BookmarkCompletedEventArgs;

						var name = String.Format("{0} {1}", bookmarkArgs.CommandArgument.LightboxName, bookmarkArgs.CommandArgument.LightboxLocation);
						var criteria = bookmarkArgs.CommandArgument.CriteriaHolder ?? ReportCriteria;
						criteria.ReportItemId = bookmarkArgs.CommandArgument.Id;

						CreateBookmark(name, BookmarkTypes.ReportItem, criteria);
				    redirectionOverride = true;
						break;

					case "SendJobEmail":
					case "SendEmployerEmail":
					case "SendDegreeCertificationEmail":
					case "SendSkillEmail":
          case "SendInternshipEmail":
          case "SendCareerAreaEmail":
            var sendEmailArgs = SendEmailCompletedEventArgs;
            var emailcriteria = sendEmailArgs.CommandArgument.CriteriaHolder ?? ReportCriteria;

            Email.Show(emailcriteria, sendEmailArgs.CommandArgument.Id, sendEmailArgs.CommandArgument.LightboxName, sendEmailArgs.CommandArgument.LightboxLocation);
				    redirectionOverride = true;
						break;

					case "PrintJob":
					case "PrintEmployer":
          case "PrintDegreeCertification":
          case "PrintProgramAreaDegreeStudy":
          case "PrintSkill":
          case "PrintInternship":
          case "PrintCareerArea":
            PrintCompleted(PrintCompletedEventArgs.CommandArgument.PrintFormat);
				    redirectionOverride = true;
						break;

          case "ResumeUpload":
            if (App.Settings.Module == FocusModules.CareerExplorer)
              Response.RedirectToRoute("YourResume");
						break;
				}
			}

			if (!App.Settings.ExplorerShowBookmarkForNonLoggedInUser)
			{
				BookmarkImage.Visible = true;
				BookmarkLinkButton.Visible = true;
			}
      
		  return redirectionOverride;
		}

		protected void Master_LogInCancelled(object sender, CommandEventArgs e)
		{
			if (!String.IsNullOrEmpty(e.CommandName))
			{
				switch (e.CommandName)
				{
					case "BookmarkJob":
					case "BookmarkEmployer":
					case "BookmarkDegreeCertification":
          case "BookmarkProgramAreaDegreeStudy":
					case "BookmarkSkill":
          case "BookmarkInternship":
          case "BookmarkCareerArea":
            BookmarkCompleted();
						break;

					case "SendJobEmail":
					case "SendEmployerEmail":
					case "SendDegreeCertificationEmail":
					case "SendSkillEmail":
          case "SendInternshipEmail":
          case "SendCareerAreaEmail":
            SendEmailCompleted();
						break;

					case "PrintJob":
					case "PrintEmployer":
					case "PrintDegreeCertification":
          case "PrintProgramAreaDegreeStudy":
					case "PrintSkill":
          case "PrintInternship":
          case "PrintCareerArea":
            PrintCompleted(ModalPrintFormat.None);
						break;

          case "ResumeUpload":
						//ResumeUpload.Show();
						break;
				}
			}
		}

		protected void Master_LogOutComplete(object sender, EventArgs e)
		{
			BookmarkImage.Visible = true;
			BookmarkLinkButton.Visible = true;
			if (!App.Settings.ExplorerShowBookmarkForNonLoggedInUser)
			{
				BookmarkImage.Visible = false;
				BookmarkLinkButton.Visible = false;
			}
		}

		#endregion

		protected void Confirmation_CloseCommand(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "EmailConfirmationClose":
					SendEmailCompleted();
					break;

				case "BookmarkConfirmationClose":
					BookmarkCompleted();
					break;
			}
		}

		#endregion

    /// <summary>
    /// Handles the ItemClicked event of the Modal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void Modal_ItemClicked(object sender, CommandEventArgs e)
    {
      var args = (LightboxEventCommandArguement)e.CommandArgument;
      List<LightboxBreadcrumb> breadcrumbs;
      ExplorerCriteria criteria;
      switch (e.CommandName)
      {
        case "ViewJob":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Job, out breadcrumbs, out criteria);
          Job.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewEmployer":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Employer, out breadcrumbs, out criteria);
          Employer.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewDegreeCertification":
        case "ViewDegreeCertificationStudy":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.DegreeCertification, out breadcrumbs, out criteria);
          DegreeCertification.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewProgramAreaDegree":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.DegreeCertification, out breadcrumbs, out criteria);
          ProgramAreaDegreeStudy.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewSkill":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Skill, out breadcrumbs, out criteria);
          Skill.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewInternship":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Internship, out breadcrumbs, out criteria);
          Internship.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewCareerArea":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.CareerArea, out breadcrumbs, out criteria);
          CareerArea.Show(args.Id, criteria, breadcrumbs);
          break;
      }
    }

    /// <summary>
    /// Builds the breadcrumbs and criteria.
    /// </summary>
    /// <param name="args">The args.</param>
    /// <param name="reportType">Type of the report.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="criteria">The criteria.</param>
    private void BuildBreadcrumbsAndCriteria(LightboxEventCommandArguement args, ReportTypes reportType, out List<LightboxBreadcrumb> breadcrumbs, out ExplorerCriteria criteria)
    {
      if (args.Breadcrumbs != null)
      {
        #region Filter cancelled
        // If a filter is cancelled we have the criteria and breadcrumbs already
        if (args.CriteriaHolder.IsNotNull())
        {
          criteria = args.CriteriaHolder;
          breadcrumbs = args.Breadcrumbs;
          return;
        }
        #endregion


        // Reuse the last criteria otherwise
        criteria = args.Breadcrumbs.Last().Criteria.Clone();

        #region Moving back through breadcrumbs
        // Remove last criteria from breadcrumbs if moving backwards
        if (args.MovingBack)
        {
          args.Breadcrumbs.RemoveAt(args.Breadcrumbs.Count - 1);
          breadcrumbs = args.Breadcrumbs;
          return;
        }
        #endregion

        // Set breadcrumbs
        breadcrumbs = args.Breadcrumbs;
        
        // When moving forward we may need to reset criteria depending on source and destination
        if (!args.MovingBack)
        {
          if (reportType == ReportTypes.Job || reportType == ReportTypes.Internship)
          {
            // Always clear down when moving to a job or internship modal
            var newCriteria = new ExplorerCriteria
                                {
                                  StateArea = criteria.StateArea,
                                  StateAreaId = criteria.StateAreaId
                                };
            criteria = newCriteria;
          }

          if (args.Breadcrumbs.Last().LinkType != ReportTypes.Internship)
          {
            // Remove internship details when movng away from the modal.
            criteria.InternshipCategoryId = null;
            criteria.InternshipCategory = null;
          }
          // Clear down job specifics when moving from a job modal
          if (args.Breadcrumbs.Last().LinkType == ReportTypes.Job)
          {
            criteria.CareerAreaJobId = null;
            criteria.CareerAreaJob = null;
          }
          // Reset degree data when travelling to a degree modal
          if (reportType == ReportTypes.DegreeCertification)
          {
            // If moving from the job to the degree modal reset the degree criteria (Modal will set the right Ids)
            criteria.EducationCriteriaOption = EducationCriteriaOptions.DegreeLevel;
            criteria.DegreeLevel = DegreeLevels.AnyOrNoDegree;
            criteria.DegreeLevelLabel = CodeLocalise(DegreeLevels.AnyOrNoDegree, "Any/No Degree");
            criteria.DegreeEducationLevel = null;
            criteria.DegreeEducationLevelId = null;
            // Reset career area as well
           ResetCareerArea(criteria);
          }
          else if (reportType == ReportTypes.Skill)
          {
            criteria.EmployerId = null;
            criteria.Employer = null;
          }
        }
      }
      else
      {
        criteria = ReportCriteria;
        breadcrumbs = null;
      }
    }

	  #endregion

		#region localisation and binding

    /// <summary>
    /// Education routes.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    private void CareerAreaRoutes(ExplorerCriteria criteria)
    {
      CareerAreaOccupationTypes occupationType;

      if (Enum.TryParse(Page.RouteData.Values["careerAreaOccupationType"].ToString(), true, out occupationType))
      {
        long occupationId;

        if (long.TryParse(Page.RouteData.Values["careerAreaOccupationId"].ToString(), out occupationId))
        {
          if (occupationType == CareerAreaOccupationTypes.Job)
          {
            criteria.CareerAreaJobTitleId = occupationId;
            //criteria.CareerAreaJobTitle = ServiceClientLocator.ExplorerClient(App).GetJobTitle(criteria.CareerAreaJobTitleId.Value).Name;
          }
          else if (occupationType == CareerAreaOccupationTypes.MOS)
          {
            criteria.CareerAreaMilitaryOccupationId = occupationId;
            //criteria.CareerAreaMilitaryOccupation = ServiceClientLocator.OccupationClient(App).GetMilitaryOccupationJobTitle(occupationId).JobTitle;
          }

          if (Page.RouteData.Values.ContainsKey("careerAreaJobId"))
          {
            long jobId;
            if (long.TryParse(Page.RouteData.Values["careerAreaJobId"].ToString(), out jobId))
            {
              criteria.CareerAreaJobId = jobId;
            }
          }
        }
      }
    }

    /// <summary>
    /// Sets the control visibility.
    /// </summary>
    private void SetControlVisibility()
    {
      CareerAreaLabelPanel.CssClass = "overlayPanelClosed overlayPanelOpenTrigger toolTipHover";
      EducationLabelPanel.CssClass = "overlayPanelClosed overlayPanelOpenTrigger toolTipHover";
      EmployerAutoCompleteTextBox.Enabled = true;
      InDemandLiteral.Visible = true;

      MilitaryOccupationRow1.Visible = MilitaryOccupationRow2.Visible = App.Settings.ExplorerSectionOrder.Contains("Experience") && !App.Settings.HideMocCodes;

      if (ReportCriteria.ReportType == ReportTypes.Internship)
      {

        CareerAreaDropDownList.SelectedIndex = 0;
        JobTitleAutoCompleteTextBox.Text = String.Empty;
        JobTitleAutoCompleteTextBox.SelectedValue = String.Empty;
        CareerAreaLabel.Text = CodeLocalise("CareerArea.TopDefault", "All Career Areas");

        CareerAreaLabelPanel.CssClass = "overlayPanelClosed Disabled";


        EducationOptionNonSpecificRadioButton.Checked = true;
        EducationOptionSpecificRadioButton.Checked = false;
        EducationOptionNonSpecificDropDownList.SelectedIndex = 0;
        DegreeCertificationAutoCompleteTextBox.Text = String.Empty;
        DegreeCertificationAutoCompleteTextBox.SelectedValue = String.Empty;
        DetailedDegreeDropDownList.SelectedIndex = -1;
        EducationCurrentLabel.Text = EducationLabel.Text = CodeLocalise(DegreeLevels.AnyOrNoDegree, "Any/No Degree");

        EducationLabelPanel.CssClass = "overlayPanelClosed Disabled";
      }
      else if (ReportCriteria.ReportType == ReportTypes.CareerArea)
      {
        CareerAreaDropDownList.SelectedIndex = 0;
        JobTitleAutoCompleteTextBox.Text = String.Empty;
        JobTitleAutoCompleteTextBox.SelectedValue = String.Empty;
        CareerAreaLabel.Text = CodeLocalise("CareerArea.TopDefault", "All Career Areas");

        CareerAreaLabelPanel.CssClass = "overlayPanelClosed Disabled";
      }
      else if (ReportCriteria.ReportType == ReportTypes.Skill)
      {
        EmployerAutoCompleteTextBox.SelectedValue = null;
        EmployerAutoCompleteTextBox.Text = string.Empty;
        EmployerAutoCompleteTextBox.Enabled = false;
      }
      else if (ReportCriteria.ReportType == ReportTypes.ProgramAreaDegree ||
               ReportCriteria.ReportType == ReportTypes.DegreeCertification)
      {
        InDemandLiteral.Visible = false;
      }
    }

		/// <summary>
		/// Applies client specific branding.
		/// </summary>
		private void ApplyBranding()
		{
			BookmarkImage.ImageUrl = UrlBuilder.BookmarkIcon();
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
		  InDemandLiteral.Text = CodeLocalise("Global.Top.Text", "In-demand");

			SearchTermRequired.Text = CodeLocalise("SearchTerm.RequiredErrorMessage", "<br />Search term is required");
			SearchTermValidator.Text = CodeLocalise("SearchTerm.ValidatorErrorMessage", "<br />Search term minimum 3 characters");

			SearchGoButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go").ToUpper();

			ReportTypeDropDownList.ToolTip = CodeLocalise("ReportTypeJob.TooltipText", "Shown nationally or by geographic area, these are the occupations most in demand, with at least 10 job openings over the last 12 months.");

            CareerAreaRadioButton.InputAttributes.Add("Title", "Select by Area");
            JobTitleRadioButton.InputAttributes.Add("Title", "Select by Title");
            MilitaryOccupationRadioButton.InputAttributes.Add("Title", "Select by military Occupation");
			CareerAreaLabel.Text = CodeLocalise("CareerArea.TopDefault", "All Career Areas");
      CareerAreaGoButton.Text = string.Concat(CodeLocalise("Global.Go.Label", "Go"), " »");
	    CareerAreaLabelPanel.ToolTip = CodeLocalise("CareerArea.TooltipText",
																									"Careers/jobs are grouped into 23 career areas based on standard occupational titles in the O*Net classification system. The career areas show how jobs appear in #BUSINESS#:LOWER job postings.");

        EducationOptionNonSpecificRadioButton.InputAttributes.Add("Title", "Non Specific Education Option");
        EducationOptionSpecificRadioButton.InputAttributes.Add("Title", "Specific Education Option");
			EducationLabel.Text = CodeLocalise(DegreeLevels.AnyOrNoDegree, "Any/No Degree");
			EducationCurrentLabel.Text = CodeLocalise(DegreeLevels.AnyOrNoDegree, "Any/No Degree");

      EducationGoButton.Text = string.Concat(CodeLocalise("Global.Go.Label", "Go"), " »");

      JobTitleCustomValidator.ErrorMessage = CodeLocalise("JobTitleCustomValidator.ErrorMessage", "Please select a job title from the list");
      RelatedJobsCustomValidator.ErrorMessage = CodeLocalise("RelatedJobsCustomValidator.ErrorMessage", "Please select a related job");
      MilitaryOccupationCustomValidator.ErrorMessage = CodeLocalise("MilitaryOccupationCustomValidator.ErrorMessage", "Please select a job title from the list");
      DegreeCertificationValidator.ErrorMessage = CodeLocalise("DegreeCertificationValidator.ErrorMessage", "Please select a degree from the list");
	    BookmarkLinkButton.ToolTip = CodeLocalise("Bookmark.TooltipText","Save your search results as bookmarks and email to yourself or others. Each search you bookmark is saved in “My bookmarks” as a hyperlink that you may use in the future.");
	    StateAreaSelectorPanel.ToolTip = CodeLocalise("ReportStateArea.TooltipText",
	                                                  "Change the criteria by selecting another location to look in here.");
		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
		private void BindControls()
		{
      var dropdownOptions = App.Settings.ExplorerInDemandDropdownOrder.Split(new[] { '|' });
      foreach (var option in dropdownOptions.Where(option => Enum.IsDefined(typeof(ReportTypes), option)))
      {
        ReportTypeDropDownList.Items.AddEnum((ReportTypes)Enum.Parse(typeof(ReportTypes), option));
      }
      ReportTypeDropDownList.SelectedIndex = 0;

			CareerAreaDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetCareerAreas();
			CareerAreaDropDownList.DataTextField = "Name";
			CareerAreaDropDownList.DataValueField = "Id";
			CareerAreaDropDownList.DataBind();
			CareerAreaDropDownList.Items.AddLocalisedTopDefault("CareerArea.TopDefault", "All Career Areas");

			JobTitleAutoCompleteTextBox.InFieldLabelText = CodeLocalise("TypeAJobTitle.Label", "type a job title");
			JobTitleAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
			JobTitleAutoCompleteTextBox.ServiceMethod = "GetJobTitleAutoCompleteItems";
			JobTitleAutoCompleteTextBox.ServiceMethodParameters = null;
			JobTitleAutoCompleteTextBox.OnClientItemSelected = "ResetCareerAreaDropDown(\"jobtitle\"); DoResetEducationSelector();";

      MilitaryOccupationTextBox.InFieldLabelText = CodeLocalise("TypeAMilitaryOccupation.Label", "Type an MOC code");
      MilitaryOccupationTextBox.ServiceUrl = UrlBuilder.AjaxService();
      MilitaryOccupationTextBox.ServiceMethod = "GetMilitaryOccupationAutoCompleteItems";
      MilitaryOccupationTextBox.ServiceMethodParameters = null;
      MilitaryOccupationTextBox.OnClientItemSelected = "ResetCareerAreaDropDown(\"military\"); DoResetEducationSelector();";

      var js = String.Format(@"ResetCareerAreaDropDown(\'military\'); DoResetEducationSelector();ClearJobDropdown(\'{0}\',\'{1}\',\'{2}\');",
                  JobTitleAutoCompleteTextBox.ClientID,
                  JobTitleAutoCompleteTextBox.SelectedValueClientId,
                  RelatedJobsDropDown.ClientID);

      MilitaryOccupationTextBox.OnClientItemSelected = js;

			EducationOptionNonSpecificDropDownList.Items.AddEnum(DegreeLevels.AnyOrNoDegree, "Any/No Degree");
			EducationOptionNonSpecificDropDownList.Items.AddEnum(DegreeLevels.LessThanBachelors, "Less Than a Bachelors");
			EducationOptionNonSpecificDropDownList.Items.AddEnum(DegreeLevels.BachelorsOrHigher, "Bachelors or Higher");

      Utilities.PopulateDegreeLevelDropdown(DetailedDegreeLevelDropDownList,
        CodeLocalise("CertificateOrAssociate", "Certificate or Associate"),
        CodeLocalise("Bachelors", "Bachelors"),
        CodeLocalise("GraduateOrProfessional", "Graduate or Professional")
      );

			DegreeCertificationAutoCompleteTextBox.InFieldLabelText = CodeLocalise("TypeADegreeCertification.Label", "type a degree or certification");
			DegreeCertificationAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
			DegreeCertificationAutoCompleteTextBox.ServiceMethod = "GetDegreeEducationLevelAutoCompleteItems";
		  var serviceMethodParameters = new List<AutoCompleteMethodParameter>
		                                  {
		                                    new AutoCompleteMethodParameter
		                                      {
		                                        ControlId = DetailedDegreeLevelDropDownList.ClientID,
		                                        ParameterName = "DetailedDegreeLevel",
		                                        ParameterType = "string"
		                                      }
		                                  };
      if (App.Settings.ExplorerDegreeFilteringType == DegreeFilteringType.TheirsThenOurs)
      {
        serviceMethodParameters.Add(new AutoCompleteMethodParameter
        {
          OverrideValue = "true",
          ParameterName = "searchClientDataOnly",
          ParameterType = "bool"
        });
      }

      DegreeCertificationAutoCompleteTextBox.ServiceMethodParameters = serviceMethodParameters.ToArray();
      
			DegreeCertificationAutoCompleteTextBox.OnClientItemSelected = "ResetEducationDropDown(); DoResetCareerAreaSelector();";

			EmployerAutoCompleteTextBox.InFieldLabelText = CodeLocalise("AllEmployers.Label", "All #BUSINESSES#");
			EmployerAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
			EmployerAutoCompleteTextBox.ServiceMethod = "GetEmployerAutoCompleteItems";
			serviceMethodParameters = new List<AutoCompleteMethodParameter>
				                              	{
				                              		new AutoCompleteMethodParameter
				                              			{
				                              				ControlId = ReportStateAreaId.ClientID,
				                              				ParameterName = "stateAreaId",
				                              				ParameterType = "long"
				                              			}
				                              	};
			EmployerAutoCompleteTextBox.ServiceMethodParameters = serviceMethodParameters.ToArray();

		  if (App.Settings.Theme != FocusThemes.Education || !App.Settings.ShowProgramArea)
		  {
        ProgramAreaPlaceHolder.Visible = false;
		    DegreeRadioButton.Checked = true;
		    EducationOptionSpecificRadioButton.Checked = true;

        if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
        {
          DetailedDegreeDropDownList.Visible = true;
          DegreeCertificationAutoCompleteTextBox.Visible = false;
          BindDetailedDegreeDropDownList();
        }
		  }
		  else
		  {
		    ProgramAreaPlaceHolder.Visible = true;
		    ProgramAreaRadioButton.Checked = true;

		    StudyProgramAreaDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreas();
		    StudyProgramAreaDropDownList.DataTextField = "Name";
		    StudyProgramAreaDropDownList.DataValueField = "Id";
		    StudyProgramAreaDropDownList.DataBind();
		    StudyProgramAreaDropDownList.Items.AddLocalisedTopDefault("Global.ProgramArea.TopDefault", "- select department -");

		    StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");
		    BindElementJavaScriptEvents();
		  }
		}

    /// <summary>
    /// Binds the details degree down-down list
    /// </summary>
    private void BindDetailedDegreeDropDownList()
    {
      DetailedDegreeLevels detailedDegreeLevel;
      Enum.TryParse(DetailedDegreeLevelDropDownList.SelectedValue, true, out detailedDegreeLevel);

      DetailedDegreeDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(null, null, detailedDegreeLevel)
                                                                     .OrderBy(d => d.DegreeName);
      DetailedDegreeDropDownList.DataTextField = "DegreeName";
      DetailedDegreeDropDownList.DataValueField = "DegreeEducationLevelId";
      DetailedDegreeDropDownList.DataBind();
      DetailedDegreeDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

      var selectedDegree = Request.Form[DetailedDegreeDropDownList.UniqueID];
      if (selectedDegree.IsNotNullOrEmpty())
        DetailedDegreeDropDownList.SelectedValue = selectedDegree;

      BindElementJavaScriptEvents();
    }
    
    /// <summary>
    /// Binds the element java script events.
    /// </summary>
    private void BindElementJavaScriptEvents()
    {
      var js = String.Format(
       @"
$(document).ready(function() {{
	$('#{0}').change(function () {{
		var selectedIndex = ($(this).prop('selectedIndex') > 0) ? 1 : -1;
		LoadCascadingDropDownList($(this).val(),'{1}','{2}','{3}','{4}','{5}', selectedIndex);

    if ($(this).prop('selectedIndex') > 0) {{
      $('#{6}').prop('checked', true);
    }}
	}});
  $('#{1}').change(function () {{
    if ($(this).prop('selectedIndex') > 0) {{
      $('#{6}').prop('checked', true);
    }}
	}});
}});",
       StudyProgramAreaDropDownList.ClientID, StudyProgramAreaDegreesDropDownList.ClientID, String.Concat(UrlBuilder.AjaxService(), "/GetDegreeEducationLevelsByProgramArea"), "Degrees",
       CodeLocalise("Global.Area.TopDefault", "- select degree -"),
       CodeLocalise("LoadingAreas.Text", "[Loading degrees...]"), EducationOptionSpecificRadioButton.ClientID);

      Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "StudyProgramAreaDropDownListOnChange", js, true);

      if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
      {
        js = String.Format(
          @"
$(document).ready(function() {{
	$('#{0}').change(function () {{
		var selectedIndex = ($(this).prop('selectedIndex') > 0) ? 1 : -1;

		LoadCascadingDropDownList($(this).val(),'{1}','{2}','{3}','{4}','{5}', selectedIndex);

    if ($(this).prop('selectedIndex') > 0) {{
      $('#{6}').prop('checked', true);
    }}
	}});
  $('#{1}').change(function () {{
    if ($(this).prop('selectedIndex') > 0) {{
      $('#{6}').prop('checked', true);
    }}
	}});
}});",
          DetailedDegreeLevelDropDownList.ClientID, 
          DetailedDegreeDropDownList.ClientID,
          String.Concat(UrlBuilder.AjaxService(), "/GetDegreeEducationLevelsByDegreeLevel"), "Degrees",
          "",
          CodeLocalise("LoadingAreas.Text", "[Loading degrees...]"), 
          EducationOptionSpecificRadioButton.ClientID);

        Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "DetailedDegreeLevelDropDownListOnChangeEducation", js, true);
      }
    }

    private void BindReportCriteria()
    {
      var criteria = ReportCriteria;

      // report type
      ReportTypeDropDownList.SelectedValue = criteria.ReportType.ToString();

      if (criteria.ReportType == ReportTypes.Internship)
      {
        CareerAreaLabelPanel.CssClass = "overlayPanelClosed Disabled";
        EducationLabelPanel.CssClass = "overlayPanelClosed Disabled";
      }
      else
      {
        if (criteria.ReportType == ReportTypes.CareerArea)
        {
          CareerAreaLabelPanel.CssClass = "overlayPanelClosed Disabled";
        }
        else
        {
          CareerAreaLabelPanel.CssClass = "overlayPanelClosed overlayPanelOpenTrigger toolTipHover";

          // career area
          if (criteria.CareerAreaId.HasValue)
          {
            CareerAreaRadioButton.Checked = true;
            CareerAreaDropDownList.SelectedValue = criteria.CareerAreaId.Value.ToString(CultureInfo.InvariantCulture);
            CareerAreaLabel.Text = CareerAreaDropDownList.SelectedItem.Text;
          }
          else if (criteria.CareerAreaJobTitleId.HasValue)
          {
            var jobTitle = ServiceClientLocator.ExplorerClient(App).GetJobTitle(criteria.CareerAreaJobTitleId.Value);

            JobTitleAutoCompleteTextBox.Text = jobTitle.Name;
            JobTitleAutoCompleteTextBox.SelectedValue = jobTitle.Id.ToString(CultureInfo.InvariantCulture);
            CareerAreaLabel.Text = jobTitle.Name;

            BindRelatedJobs(criteria.CareerAreaJobTitleId.Value, criteria.CareerAreaJobId);
            RelatedJobsSelectorTableRow.Visible = true;
            if (criteria.CareerAreaJobId.HasValue)
            {
              CareerAreaLabel.Text = RelatedJobsDropDown.SelectedItem.Text;
            }

            JobTitleRadioButton.Checked = true;
          }
          else if (criteria.CareerAreaMilitaryOccupationId.HasValue)
          {
            var occupation =
              ServiceClientLocator.OccupationClient(App).GetMilitaryOccupationJobTitle(criteria.CareerAreaMilitaryOccupationId.Value);

            if (occupation.IsNotNull())
            {
              MilitaryOccupationTextBox.Text = occupation.JobTitle;
              MilitaryOccupationTextBox.SelectedValue =
                criteria.CareerAreaMilitaryOccupationId.GetValueOrDefault(0).ToString(CultureInfo.InvariantCulture);
              CareerAreaLabel.Text = occupation.JobTitle;

              MilitaryOccupationRadioButton.Checked = true;
            }
          }
          else
          {
            CareerAreaRadioButton.Checked = true;
          }
        }

        // education
        EducationLabelPanel.CssClass = "overlayPanelClosed overlayPanelOpenTrigger toolTipHover";
        switch (criteria.EducationCriteriaOption)
        {
          case EducationCriteriaOptions.DegreeLevel:
            if (criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree)
            {
              EducationOptionNonSpecificRadioButton.Checked = true;
              EducationOptionSpecificRadioButton.Checked = false;

              EducationOptionNonSpecificDropDownList.SelectedValue = criteria.DegreeLevel.ToString();
              EducationLabel.Text =
                EducationCurrentLabel.Text = EducationOptionNonSpecificDropDownList.SelectedItem.Text;
            }

            break;

          case EducationCriteriaOptions.Specific:
            EducationOptionNonSpecificRadioButton.Checked = false;
            EducationOptionSpecificRadioButton.Checked = true;
            DegreeRadioButton.Checked = true;

            var degreeEducationLevel =
              ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevel(criteria.DegreeEducationLevelId.Value);

            if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
            {
              DetailedDegreeDropDownList.SelectValue(degreeEducationLevel.Id.Value.ToString(CultureInfo.InvariantCulture));
            }
            else
            {
              DegreeCertificationAutoCompleteTextBox.Text = degreeEducationLevel.DegreeEducationLevelName;
              DegreeCertificationAutoCompleteTextBox.SelectedValue =
                degreeEducationLevel.Id.Value.ToString(CultureInfo.InvariantCulture);
            }

            EducationLabel.Text = EducationCurrentLabel.Text = degreeEducationLevel.DegreeEducationLevelName;

            break;
        }

      }

      // employer
      if (criteria.EmployerId.HasValue)
      {
        var employer = ServiceClientLocator.ExplorerClient(App).GetEmployer(criteria.EmployerId.Value);
        EmployerAutoCompleteTextBox.Text = employer.Name;
        EmployerAutoCompleteTextBox.SelectedValue = employer.Id.Value.ToString(CultureInfo.InvariantCulture);
      }

      // location
      if (criteria.StateAreaId > 0)
      {
        var stateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(criteria.StateAreaId);

        StateAreaSelectorLabel.Text = stateArea.StateSortOrder == 0 ? stateArea.StateName : stateArea.StateAreaName;

        ReportStateAreaSelector.DefaultStateAreaId = stateArea.Id;
        ReportStateAreaId.Value = stateArea.Id.ToString();
      }
    }
    
    /// <summary>
    /// Binds the related jobs.
    /// </summary>
    /// <param name="jobTitleId">The job title id.</param>
    /// <param name="selectedJobId">The selected job id.</param>
    private void BindRelatedJobs(long jobTitleId, long? selectedJobId)
    {
      RelatedJobsDropDown.DataSource = ServiceClientLocator.ExplorerClient(App).GetRelatedJobs(jobTitleId);
      RelatedJobsDropDown.DataTextField = "Name";
      RelatedJobsDropDown.DataValueField = "Id";
      RelatedJobsDropDown.DataBind();
      RelatedJobsDropDown.Items.AddLocalisedTopDefault("RelatedJobs.TopDefault", "- select job -");

      if (selectedJobId.HasValue)
        RelatedJobsDropDown.SelectedValue = selectedJobId.Value.ToString(CultureInfo.InvariantCulture);
    }

		#endregion

    #region Bind Criteria object

    /// <summary>
    /// Sets the report criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="forModal">if set to <c>true</c> [for modal].</param>
    private void SetReportCriteria(ExplorerCriteria criteria, bool forModal)
    {
      SetLocationCriteria(criteria);
      SetEmployerCriteria(criteria);
      SetCareerAreaCriteria(criteria, forModal);
      SetEducationCriteria(criteria, forModal);
    }

    /// <summary>
    /// Sets the location criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    private void SetLocationCriteria(ExplorerCriteria criteria)
    {
      if (!String.IsNullOrEmpty(ReportStateAreaId.Value))
      {
        criteria.StateAreaId = long.Parse(ReportStateAreaId.Value);
        criteria.StateArea = StateAreaSelectorLabel.Text;
      }
      else
      {
        var defaultState = ServiceClientLocator.ExplorerClient(App).GetStateAreaDefault();
        criteria.StateAreaId = defaultState.Id.Value;
        criteria.StateArea = defaultState.StateAreaName;
      }

    }

    /// <summary>
    /// Sets the employer criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    private void SetEmployerCriteria(ExplorerCriteria criteria)
    {
      criteria.EmployerId = null;
      criteria.Employer = null;

      if (!String.IsNullOrEmpty(EmployerAutoCompleteTextBox.SelectedValue))
      {
        criteria.EmployerId = EmployerAutoCompleteTextBox.SelectedValueToNullableLong;
        criteria.Employer = EmployerAutoCompleteTextBox.Text;
      }
    }

    /// <summary>
    /// Sets the career area criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="forModal">if set to <c>true</c> [for modal].</param>
    private void SetCareerAreaCriteria(ExplorerCriteria criteria, bool forModal)
    {
      // Reset criteria
      ResetCareerArea(criteria);

      if (!String.IsNullOrEmpty(RelatedJobsDropDown.SelectedValue) && !forModal)
      {
        criteria.CareerAreaJob = CareerAreaLabel.Text = RelatedJobsDropDown.SelectedItem.Text;
        criteria.CareerAreaJobTitleId = JobTitleAutoCompleteTextBox.SelectedValueToNullableLong;
        criteria.CareerAreaJobId = RelatedJobsDropDown.SelectedValueToNullableLong();
      }
      else if (!String.IsNullOrEmpty(JobTitleAutoCompleteTextBox.SelectedValue) && !forModal)
      {
        criteria.CareerAreaJobTitle = CareerAreaLabel.Text = JobTitleAutoCompleteTextBox.Text;
        criteria.CareerAreaJobTitleId = JobTitleAutoCompleteTextBox.SelectedValueToNullableLong;

        CareerAreaDropDownList.SelectedIndex = 0;
      }
      else if (MilitaryOccupationTextBox.SelectedValue.IsNotNullOrEmpty() && !forModal)
      {
        criteria.CareerAreaMilitaryOccupation = CareerAreaLabel.Text = MilitaryOccupationTextBox.Text;
        criteria.CareerAreaMilitaryOccupationId = MilitaryOccupationTextBox.SelectedValueToNullableLong;

        CareerAreaDropDownList.SelectedIndex = 0;
      }
      else if (!String.IsNullOrEmpty(CareerAreaDropDownList.SelectedValue))
      {
        criteria.CareerArea = CareerAreaLabel.Text = CareerAreaDropDownList.SelectedItem.Text;

        criteria.CareerAreaId = CareerAreaDropDownList.SelectedValueToNullableLong();
      }
      else
      {
        criteria.CareerArea = CodeLocalise("CareerArea.TopDefault", "All Career Areas");
      }

      if (String.IsNullOrEmpty(JobTitleAutoCompleteTextBox.SelectedValue) && String.IsNullOrEmpty(MilitaryOccupationTextBox.SelectedValue))
      {
        CareerAreaRadioButton.Checked = true;
        JobTitleRadioButton.Checked = false;
        MilitaryOccupationRadioButton.Checked = false;
      }
    }

	  /// <summary>
    /// Sets the education criteria.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="forModal">if set to <c>true</c> [for modal].</param>
    private void SetEducationCriteria(ExplorerCriteria criteria, bool forModal)
    {
      criteria.DegreeLevelLabel = criteria.DegreeEducationLevel = null;
      if (EducationOptionNonSpecificRadioButton.Checked)
      {
        criteria.DegreeLevelLabel = EducationLabel.Text = EducationCurrentLabel.Text = EducationOptionNonSpecificDropDownList.SelectedItem.Text;

        criteria.EducationCriteriaOption = EducationCriteriaOptions.DegreeLevel;
        criteria.DegreeLevel = EducationOptionNonSpecificDropDownList.SelectedValueToEnum(DegreeLevels.AnyOrNoDegree);
        criteria.DegreeEducationLevelId = null;
      }
      else if (EducationOptionSpecificRadioButton.Checked)
      {
        if (DegreeRadioButton.Checked)
        {
          if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
          {
            if (String.IsNullOrEmpty(DetailedDegreeDropDownList.SelectedValue))
            {
              EducationOptionNonSpecificRadioButton.Checked = true;
              EducationOptionSpecificRadioButton.Checked = false;
            }
            else if (!forModal)
            {
              EducationLabel.Text = EducationCurrentLabel.Text = DetailedDegreeDropDownList.SelectedItem.Text;

              criteria.EducationCriteriaOption = EducationCriteriaOptions.Specific;
              criteria.DegreeEducationLevelId = DetailedDegreeDropDownList.SelectedValueToNullableLong();
              criteria.DegreeEducationLevel = DetailedDegreeDropDownList.SelectedItem.Text;
              criteria.DegreeLevel = DegreeLevels.AnyOrNoDegree;
            }
          }
          else
          {
            if (String.IsNullOrEmpty(DegreeCertificationAutoCompleteTextBox.SelectedValue))
            {
              EducationOptionNonSpecificRadioButton.Checked = true;
              EducationOptionSpecificRadioButton.Checked = false;
            }
            else if (!forModal)
            {
              EducationLabel.Text = EducationCurrentLabel.Text = DegreeCertificationAutoCompleteTextBox.Text;

              criteria.EducationCriteriaOption = EducationCriteriaOptions.Specific;
              criteria.DegreeEducationLevelId = DegreeCertificationAutoCompleteTextBox.SelectedValueToNullableLong;
              criteria.DegreeEducationLevel = DegreeCertificationAutoCompleteTextBox.Text;
              criteria.DegreeLevel = DegreeLevels.AnyOrNoDegree;
            }
          }
        }
        else //Program area
        {
          EducationLabel.Text = EducationCurrentLabel.Text = StudyProgramAreaDegreesDropDownList.SelectedItem.Text + @", " + StudyProgramAreaDropDownList.SelectedItem.Text;
          if (StudyProgramAreaDegreesDropDownList.SelectedIndex <= 1 && StudyProgramAreaDropDownList.SelectedIndex != 0)
          {
            criteria.ProgramAreaId = StudyProgramAreaDropDownList.SelectedValueToLong();
            EducationLabel.Text = EducationCurrentLabel.Text = StudyProgramAreaDropDownList.SelectedItem.Text;
          }
          else if (StudyProgramAreaDegreesDropDownList.SelectedIndex > 1 && StudyProgramAreaDropDownList.SelectedIndex != 0)
          {
            criteria.ProgramAreaId = StudyProgramAreaDropDownList.SelectedValueToLong();
            criteria.EducationCriteriaOption = EducationCriteriaOptions.Specific;
            criteria.DegreeEducationLevelId = StudyProgramAreaDegreesDropDownList.SelectedValueToLong();
            criteria.DegreeEducationLevel = StudyProgramAreaDegreesDropDownList.SelectedItem.Text;
            criteria.DegreeLevel = DegreeLevels.AnyOrNoDegree;
          }
        }
      }
    }

    /// <summary>
    /// Resets the career area.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    private void ResetCareerArea(ExplorerCriteria criteria)
    {
      // Reset criteria
      criteria.CareerAreaJobId = criteria.CareerAreaJobTitleId = criteria.CareerAreaId = criteria.CareerAreaMilitaryOccupationId = null;
      criteria.CareerAreaJob = criteria.CareerAreaJobTitle = criteria.CareerArea = criteria.CareerAreaMilitaryOccupation = null;
      criteria.CareerAreaMilitaryOccupationROnetCodes = null;
    }
    
    #endregion

    #region load report

    /// <summary>
    /// Loads the report.
    /// </summary>
    private void LoadReport()
    {
      EmployerList.Visible = false;
      DegreeCertificationList.Visible = false;
      DegreesBySchoolList.Visible = false;
      SkillList.Visible = false;
      InternshipList.Visible = false;
      JobList.Visible = false;
      CareerMoves.Visible = false;
      CareerAreaList.Visible = false;

      switch (ReportCriteria.ReportType)
      {
        case ReportTypes.Job:

          JobList.ReportCriteria = ReportCriteria;
          JobList.ListSize = String.IsNullOrEmpty(ListSizeDropDownList.SelectedValue)
                               ? 10
                               : Convert.ToInt32(ListSizeDropDownList.SelectedValue);
          JobList.JobId = 0;

          JobList.BindList(true);

          JobList.Visible = true;

          if (JobList.RecordCount == 0)
          {
            NoDataAvailableLabel.Text = CodeLocalise("ExplorerJobReport.NoData",
                                                     @"There have not been enough recent job postings for the job and area you specified for us to provide you with accurate information.  You may wish to consider searching in a broader area. Or, here are some tips: + Search statewide or nationwide instead of local area.  +Search for career area instead of specific job.  +Search for degree level instead of specific degree.");
          }
          else
          {
            NoDataAvailableLabel.Text = string.Empty;
          }

          break;

        case ReportTypes.Employer:

          EmployerList.ReportCriteria = ReportCriteria;
          EmployerList.ListSize = String.IsNullOrEmpty(ListSizeDropDownList.SelectedValue)
                                    ? 10
                                    : Convert.ToInt32(ListSizeDropDownList.SelectedValue);

          EmployerList.BindList();

          if (EmployerList.RecordCount == 0)
          {
            NoDataAvailableLabel.Text = CodeLocalise("ExplorerEmployerReport.NoData",
																										 @"There have not been enough #BUSINESSES#:LOWER postings jobs in the area you specified for us to provide you with accurate information.  You may wish to consider searching in a broader area. Or, here are some tips: + Search statewide or nationwide instead of local area.  +Search for career area instead of specific job.  +Search for degree level instead of specific degree.");
          }
          else
          {
            NoDataAvailableLabel.Text = string.Empty;
          }

          EmployerList.Visible = true;

          break;

        case ReportTypes.DegreeCertification:
        case ReportTypes.ProgramAreaDegree:
          ExplorerListControl degreeControl;

          if (App.Settings.Theme == FocusThemes.Education && App.Settings.ShowProgramArea)
            degreeControl = DegreesBySchoolList;
          else
            degreeControl = DegreeCertificationList;

          degreeControl.ReportCriteria = ReportCriteria;
          degreeControl.ListSize = String.IsNullOrEmpty(ListSizeDropDownList.SelectedValue)
                                     ? 10
                                     : Convert.ToInt32(ListSizeDropDownList.SelectedValue);
          degreeControl.BindList(true);

          if (degreeControl.RecordCount == 0)
          {
            NoDataAvailableLabel.Text = CodeLocalise("ExplorerDegreeCertificationReport.NoData",
                                                     @"There have not been degrees or certifications in the area you specified for us to provide you with accurate information.  You may wish to consider searching in a broader area. Or, here are some tips: + Search statewide or nationwide instead of local area.  +Search for career area instead of specific job.  +Search for degree level instead of specific degree.");
          }
          else
          {
            NoDataAvailableLabel.Text = string.Empty;
          }

          degreeControl.Visible = true;

          break;

        case ReportTypes.Skill:

          SkillList.ReportCriteria = ReportCriteria;
          SkillList.ListSize = String.IsNullOrEmpty(ListSizeDropDownList.SelectedValue)
                                 ? 10
                                 : Convert.ToInt32(ListSizeDropDownList.SelectedValue);
          SkillList.BindList(true);
          if (SkillList.RecordCount == 0)
          {
            NoDataAvailableLabel.Text = CodeLocalise("ExplorerSkillsReport.NoData",
                                                     @"There have not been enough jobs in the skills area you specified for us to provide you with accurate information.  You may wish to consider searching in a broader area. Or, here are some tips: + Search statewide or nationwide instead of local area.  +Search for career area instead of specific job.  +Search for degree level instead of specific degree.");
          }
          else
          {
            NoDataAvailableLabel.Text = string.Empty;
          }

          SkillList.Visible = true;

          break;

        case ReportTypes.Internship:

          InternshipList.ReportCriteria = ReportCriteria;
          InternshipList.ListSize = String.IsNullOrEmpty(ListSizeDropDownList.SelectedValue)
                                      ? 10
                                      : Convert.ToInt32(ListSizeDropDownList.SelectedValue);
          InternshipList.BindList(true);
          if (InternshipList.RecordCount == 0)
          {
            NoDataAvailableLabel.Text = CodeLocalise("ExplorerInternshipReport.NoData",
                                                     @"There have not been enough internships in the area you specified for us to provide you with accurate information.  You may wish to consider searching in a broader area. Or, here are some tips: + Search statewide or nationwide instead of local area.");
          }
          else
          {
            NoDataAvailableLabel.Text = string.Empty;
          }

          InternshipList.Visible = true;

          break;

        case ReportTypes.CareerMoves:
          CareerMoves.ReportCriteria = ReportCriteria;
          CareerMoves.ListSize = String.IsNullOrEmpty(ListSizeDropDownList.SelectedValue)
                                   ? 10
                                   : Convert.ToInt32(ListSizeDropDownList.SelectedValue);
          CareerMoves.BindList(true);
          CareerMoves.Visible = true;

          switch (CareerMoves.RecordCount)
          {
            case -1:
              NoDataAvailableLabel.Text = CodeLocalise("ExplorerCareerMovesReport.NoData",
                                                       "A job or military occupation must be selected before viewing career moves.");
              break;
            case 0:
              NoDataAvailableLabel.Text = CodeLocalise("ExplorerCareerMovesReport.NoData",
                                                       "There is currently no information avaiable about possible career moves.");
              break;
            default:
              NoDataAvailableLabel.Text = string.Empty;
              break;
          }

          break;

        case ReportTypes.CareerArea:
          CareerAreaList.ReportCriteria = ReportCriteria;
          CareerAreaList.ListSize = ListSizeDropDownList.SelectedValue.IsNullOrEmpty() ? 10 : ListSizeDropDownList.SelectedValueToInt();
          CareerAreaList.BindList(true);
          CareerAreaList.Visible = true;

          NoDataAvailableLabel.Text = (CareerAreaList.RecordCount == 0)
            ? CodeLocalise("ExplorerCareerAreasReport.NoData", @"No career areas found")
            : string.Empty;

          break;
      }
    }

	  #endregion

		#region create bookmark

    /// <summary>
    /// Creates the bookmark.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="bookmarkType">Type of the bookmark.</param>
    /// <param name="criteria">The criteria.</param>
		private void CreateBookmark(string name, BookmarkTypes bookmarkType, ExplorerCriteria criteria)
		{
			if (Utilities.CreateExplorerBookmark(name, bookmarkType, criteria))
  			Confirmation.Show(CodeLocalise("BookmarkConfirmation.Title", "Bookmark Confirmation"),
									  			CodeLocalise("BookmarkConfirmation.Body", "Your bookmark has been added"),
									  			CodeLocalise("Global.Close.Text", "Close"), "BookmarkConfirmationClose", height: 50, width: 250);
			else
        Confirmation.Show(CodeLocalise("BookmarkDuplication.Title", "Bookmark Duplication"),
                          CodeLocalise("BookmarkDuplication.Body", "A bookmark has been already been added for this item"),
                          CodeLocalise("Global.Close.Text", "Close"), "BookmarkConfirmationClose", height: 50, width: 250);
		}

		#endregion
	}
}
