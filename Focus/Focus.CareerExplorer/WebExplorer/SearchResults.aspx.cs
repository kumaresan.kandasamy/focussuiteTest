﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Core.Models;
using Framework.Core;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;

#endregion

namespace  Focus.CareerExplorer.WebExplorer
{
  public partial class SearchResults : CareerExplorerPageBase, IExplorerJobModalPage
	{
		#region page properties

		private const int DefaultColumnListSize = 10;

		protected ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue("Explorer:ReportCriteria", new ExplorerCriteria()); }
			set { SetViewStateValue("Explorer:ReportCriteria", value); }
		}

		protected LightboxEventArgs SendEmailCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:SendEmailCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:SendEmailCompletedEventArgs", value); }
		}

		protected LightboxEventArgs BookmarkCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:BookmarkCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:BookmarkCompletedEventArgs", value); }
		}

		protected LightboxEventArgs PrintCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:PrintCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:PrintCompletedEventArgs", value); }
		}

    private ExplorerCriteria OriginalReportCriteria
    {
      get { return GetViewStateValue<ExplorerCriteria>("Explorer:OriginalReportCriteria"); }
      set { SetViewStateValue("Explorer:OriginalReportCriteria", value); }
    }

	  private Stack<string> SearchBreadcrumbs
	  {
      get { return GetViewStateValue<Stack<string>>("Explorer:SearchBreadcrumbs"); }
      set { SetViewStateValue("Explorer:SearchBreadcrumbs", value); }
	  }

    public ExplorerCriteria ReturnReportCriteria
    {
      get { return OriginalReportCriteria; }
    }

		#endregion

		#region page events

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
      RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education);

			base.OnInit(e);

			Master.LogInComplete += Master_LogInComplete;
			Master.LogInCancelled += Master_LogInCancelled;
		}

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
        LightboxEventArgs returnBookmark;

			  OriginalReportCriteria = App.GetSessionValue<ExplorerCriteria>("Explorer:ReportCriteria");
        App.RemoveSessionValue("Explorer:ReportCriteria");

			  LocaliseUI();

				SearchTermRequired.Text = CodeLocalise("SearchTerm.RequiredErrorMessage", "<br />Search term is required");
				SearchTermValidator.Text = CodeLocalise("SearchTerm.ValidatorErrorMessage", "<br />Search term minimum 3 characters");

				SearchGoButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go").ToUpper();

        if ((returnBookmark = App.GetSessionValue<LightboxEventArgs>("Explorer:OpenBookmark")) != null)
        {
          OriginalReportCriteria = returnBookmark.CommandArgument.MainPageCriteria;
          switch (returnBookmark.CommandName)
          {
            case "Job":
              Job.Show(returnBookmark.CommandArgument.Id, returnBookmark.CommandArgument.CriteriaHolder, returnBookmark.CommandArgument.Breadcrumbs);
              break;
          }
          App.RemoveSessionValue("Explorer:OpenBookmark");
        }

				if (Page.RouteData.Values.ContainsKey("searchTerm"))
				{
					SearchTermLabel.Text = Page.RouteData.Values["searchTerm"].ToString();
					BindResults();
				}
			}

			if (App.Settings.ExplorerGoogleEventTrackingEnabled)
			{
				const string js = @"
	<script src=""/Assets/Scripts/googleTrack.js"" type=""text/javascript""></script>
	<script type=""text/javascript"">
		$(document).ready(function ()
		{
			googlePageTrackerUrl = '/search';
			AddGooglePageEvents('searchresults');
		});
	</script>";

				GoogleTrackingLiteral.Text = js;
			}

			Master.GoogleAnalyticsScriptLiteral.PagePreviewUrl = String.Format("/search?st={0}", HttpUtility.UrlEncode(SearchTermLabel.Text));
		}

		#endregion

		#region search and results events

    /// <summary>
    /// Handles the Click event of the BackButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void BackButton_Click(object sender, EventArgs e)
    {
      if (SearchBreadcrumbs.IsNotNullOrEmpty())
      {
        var lastTerm = SearchBreadcrumbs.Pop();

        SearchTermLabel.Text = lastTerm;
        BindResults();
        SearchTermTextBox.Text = String.Empty;
      }
      else
      {
        var criteria = OriginalReportCriteria;

        if (criteria != null)
        {
          App.SetSessionValue("Explorer:ReportCriteria", criteria);
          var url = UrlBuilder.Report(criteria.ReportType, criteria.StateAreaId);
          Response.Redirect(url);
        }
        else
        {
          Response.Redirect(UrlBuilder.Explore());
        }
      }
    }

    /// <summary>
    /// Handles the Click event of the SearchGoButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SearchGoButton_Click(object sender, EventArgs e)
		{
      var breadCrumbs = SearchBreadcrumbs ?? new Stack<string>();
      breadCrumbs.Push(SearchTermLabel.Text.Trim());
      SearchBreadcrumbs = breadCrumbs;

			SearchTermLabel.Text = SearchTermTextBox.Text;
			BindResults();
			SearchTermTextBox.Text = String.Empty;
		}

    /// <summary>
    /// Handles the ItemCommand event of the ResultRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
		protected void ResultRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			long resultId;
			if (long.TryParse(e.CommandArgument.ToString(), out resultId))
			{
				var stateArea = ServiceClientLocator.ExplorerClient(App).GetStateAreaDefault();

				switch (e.CommandName)
				{
					case "ViewJob":
						ReportCriteria = new ExplorerCriteria {ReportType = ReportTypes.Job, StateAreaId = stateArea.Id.Value, StateArea = stateArea.StateAreaName};
						Job.Show(resultId, ReportCriteria, null);
            break;

					case "ViewEmployer":
            ReportCriteria = new ExplorerCriteria { ReportType = ReportTypes.Employer, StateAreaId = stateArea.Id.Value, StateArea = stateArea.StateAreaName};
						Employer.Show(resultId, ReportCriteria, null);
            break;

					case "ViewDegreeCertification":
						ReportCriteria = new ExplorerCriteria { ReportType = ReportTypes.DegreeCertification, StateAreaId = stateArea.Id.Value, StateArea = stateArea.StateAreaName};
						DegreeCertification.Show(resultId, ReportCriteria, null);
            break;

          case "ViewProgramAreaDegree":
            ReportCriteria = new ExplorerCriteria { ReportType = ReportTypes.ProgramAreaDegree, StateAreaId = stateArea.Id.Value, StateArea = stateArea.StateAreaName };
            ProgramAreaDegreeStudy.Show(resultId, ReportCriteria, null);
            break;

					case "ViewSkill":
            ReportCriteria = new ExplorerCriteria() { ReportType = ReportTypes.Skill, StateAreaId = stateArea.Id.Value, StateArea = stateArea.StateAreaName};
						Skill.Show(resultId, ReportCriteria, null);
            break;

					case "ViewInternship":
            ReportCriteria = new ExplorerCriteria() { ReportType = ReportTypes.Internship, StateAreaId = stateArea.Id.Value, StateArea = stateArea.StateAreaName};
						Internship.Show(resultId, ReportCriteria, null);
            break;
				}
			}
		}

		#endregion

		#region modal action events

		#region send email, bookmark and print

		#region send email

		/// <summary>
		/// Handles the SendEmailClicked event of the Modal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
		protected void Modal_SendEmailClicked(object sender, LightboxEventArgs e)
		{
			SendEmailCompletedEventArgs = e;

			if (!ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				UnAuthorizedModal.Show();
			}
			else
			{
				var args = e.CommandArgument;

				switch (e.CommandName)
				{
					case "SendJobEmail":
						Email.Show(e.CommandArgument.CriteriaHolder, args.Id, args.LightboxName, args.LightboxLocation);
						break;

					case "SendEmployerEmail":
            Email.Show(e.CommandArgument.CriteriaHolder, args.Id, args.LightboxName, args.LightboxLocation);
						break;

					case "SendDegreeCertificationEmail":
            Email.Show(e.CommandArgument.CriteriaHolder, args.Id, args.LightboxName, args.LightboxLocation);
						break;

          case "SendProgramAreaDegreeStudyEmail":
            Email.Show(e.CommandArgument.CriteriaHolder, args.Id, args.LightboxName, args.LightboxLocation);
            break;

					case "SendSkillEmail":
            Email.Show(e.CommandArgument.CriteriaHolder, args.Id, args.LightboxName, args.LightboxLocation);
						break;

					case "SendInternshipEmail":
            Email.Show(e.CommandArgument.CriteriaHolder, args.Id, args.LightboxName, args.LightboxLocation);
						break;
				}
			}
		}

    /// <summary>
    /// Handles the Cancelled event of the Email control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Email_Cancelled(object sender, EventArgs e)
		{
			SendEmailCompleted();
		}

    /// <summary>
    /// Handles the Completed event of the Email control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Email_Completed(object sender, EventArgs e)
		{
			Confirmation.Show(CodeLocalise("EmailConfirmation.Title", "Email Confirmation"),
												CodeLocalise("EmailConfirmation.Body", "Your email has been sent"),
												CodeLocalise("Global.Close.Text", "Close"), "EmailConfirmationClose", height: 100, width: 250);
		}

    /// <summary>
    /// Sends the email completed.
    /// </summary>
		private void SendEmailCompleted()
		{
			var args = SendEmailCompletedEventArgs;

			switch (args.CommandName)
			{
				case "SendJobEmail":
          Job.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

				case "SendEmployerEmail":
          Employer.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

				case "SendDegreeCertificationEmail":
          DegreeCertification.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "SendProgramAreaDegreeStudyEmail":
          ProgramAreaDegreeStudy.Show(args.CommandArgument.Id,  args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

				case "SendSkillEmail":
          Skill.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

				case "SendInternshipEmail":
          Internship.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;
			}

			SendEmailCompletedEventArgs = null;
		}

		#endregion

		#region bookmark

    /// <summary>
    /// Handles the BookmarkClicked event of the Modal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
		protected void Modal_BookmarkClicked(object sender, LightboxEventArgs e)
		{
			BookmarkCompletedEventArgs = e;

			if (!ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				UnAuthorizedModal.Show();
			}
			else
			{
				var name = String.Format("{0} {1}", e.CommandArgument.LightboxName, e.CommandArgument.LightboxLocation);
				var criteria = e.CommandArgument.CriteriaHolder;
				criteria.ReportItemId = e.CommandArgument.Id;

				CreateBookmark(name, BookmarkTypes.ReportItem, criteria);
			}
		}

    /// <summary>
    /// Bookmarks the completed.
    /// </summary>
		private void BookmarkCompleted()
		{
			var args = BookmarkCompletedEventArgs;
			if (args != null)
			{
				switch (args.CommandName)
				{
					case "BookmarkJob":
						Job.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
            break;

					case "BookmarkEmployer":
            Employer.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
            break;

					case "BookmarkDegreeCertification":
            DegreeCertification.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
            break;

          case "BookmarkProgramAreaDegreeStudy":
            ProgramAreaDegreeStudy.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
            break;

					case "BookmarkSkill":
            Skill.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
            break;

					case "BookmarkInternship":
            Internship.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
            break;
				}

				BookmarkCompletedEventArgs = null;
			}
		}

		#endregion

		#region print

    /// <summary>
    /// Handles the PrintClicked event of the Modal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
		protected void Modal_PrintClicked(object sender, LightboxEventArgs e)
		{
			PrintCompletedEventArgs = e;

			UnAuthorizedModal.Show();
		}

    /// <summary>
    /// Prints the completed.
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintCompleted(ModalPrintFormat printFormat)
		{
			var args = PrintCompletedEventArgs;
      var criteria = args.CommandArgument.CriteriaHolder;
			switch (args.CommandName)
			{
				case "PrintJob":
          Job.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintEmployer":
          Employer.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintDegreeCertification":
          DegreeCertification.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

        case "PrintProgramAreaDegreeStudy":
          ProgramAreaDegreeStudy.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintSkill":
          Skill.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintInternship":
          Internship.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;
			}
		}

		#endregion

		#region login/register events

		/// <summary>
		/// Handles the LogInComplete event of the Master control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected bool Master_LogInComplete(object sender, CommandEventArgs e)
		{
		  var redirectionOverride = false;
			if (!String.IsNullOrEmpty(e.CommandName))
			{
				switch (e.CommandName)
				{
					case "BookmarkJob":
					case "BookmarkEmployer":
          case "BookmarkDegreeCertification":
          case "BookmarkProgramAreaDegree":
          case "BookmarkSkill":
					case "BookmarkInternship":
						var args = BookmarkCompletedEventArgs;

						var name = String.Format("{0} {1}", args.CommandArgument.LightboxName, args.CommandArgument.LightboxLocation);
						var criteria = args.CommandArgument.CriteriaHolder ?? ReportCriteria;
						criteria.ReportItemId = args.CommandArgument.Id;

						CreateBookmark(name, BookmarkTypes.ReportItem, criteria);
				    redirectionOverride = true;
						break;

					case "SendJobEmail":
					case "SendEmployerEmail":
          case "SendDegreeCertificationEmail":
          case "SendProgramAreaDegreeStudyEmail":
          case "SendSkillEmail":
					case "SendInternshipEmail":
						var sendEmailArgs = SendEmailCompletedEventArgs;
            var emailcriteria = sendEmailArgs.CommandArgument.CriteriaHolder ?? ReportCriteria;
						switch (e.CommandName)
						{
							case "SendJobEmail":
                Email.Show(emailcriteria, sendEmailArgs.CommandArgument.Id, sendEmailArgs.CommandArgument.LightboxName,
													 sendEmailArgs.CommandArgument.LightboxLocation);
								break;

							case "SendEmployerEmail":
                Email.Show(emailcriteria, sendEmailArgs.CommandArgument.Id, sendEmailArgs.CommandArgument.LightboxName,
													 sendEmailArgs.CommandArgument.LightboxLocation);
								break;

							case "SendDegreeCertificationEmail":
                Email.Show(emailcriteria, sendEmailArgs.CommandArgument.Id,
													 sendEmailArgs.CommandArgument.LightboxName, sendEmailArgs.CommandArgument.LightboxLocation);
								break;

              case "SendProgramAreaDegreeStudyEmail":
                Email.Show(emailcriteria, sendEmailArgs.CommandArgument.Id,
                           sendEmailArgs.CommandArgument.LightboxName, sendEmailArgs.CommandArgument.LightboxLocation);
                break;

							case "SendSkillEmail":
                Email.Show(emailcriteria, sendEmailArgs.CommandArgument.Id, sendEmailArgs.CommandArgument.LightboxName,
													 sendEmailArgs.CommandArgument.LightboxLocation);
								break;

							case "SendInternshipEmail":
                Email.Show(emailcriteria, sendEmailArgs.CommandArgument.Id, sendEmailArgs.CommandArgument.LightboxName,
													 sendEmailArgs.CommandArgument.LightboxLocation);
								break;
						}
				    redirectionOverride = true;
						break;

					case "PrintJob":
					case "PrintEmployer":
          case "PrintDegreeCertification":
          case "PrintProgramAreaDegreeStudy":
          case "PrintSkill":
					case "PrintInternship":
            PrintCompleted(PrintCompletedEventArgs.CommandArgument.PrintFormat);
				    redirectionOverride = true;
						break;
				}
			}

		  return redirectionOverride;
		}

    /// <summary>
    /// Handles the LogInCancelled event of the Master control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Master_LogInCancelled(object sender, CommandEventArgs e)
		{
			if (!String.IsNullOrEmpty(e.CommandName))
			{
				switch (e.CommandName)
				{
					case "BookmarkJob":
					case "BookmarkEmployer":
          case "BookmarkDegreeCertification":
          case "BookmarkProgramAreaDegreeStudy":
          case "BookmarkSkill":
					case "BookmarkInternship":
						BookmarkCompleted();
						break;

					case "SendJobEmail":
					case "SendEmployerEmail":
          case "SendDegreeCertificationEmail":
          case "SendProgramAreaDegreeStudyEmail":
          case "SendSkillEmail":
					case "SendInternshipEmail":
						SendEmailCompleted();
						break;

					case "PrintJob":
					case "PrintEmployer":
          case "PrintDegreeCertification":
          case "PrintProgramAreaDegreeStudy":
          case "PrintSkill":
					case "PrintInternship":
						PrintCompleted(ModalPrintFormat.None);
						break;
				}
			}
		}

		#endregion

    /// <summary>
    /// Handles the CloseCommand event of the Confirmation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_CloseCommand(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "EmailConfirmationClose":
					SendEmailCompleted();
					break;

				case "BookmarkConfirmationClose":
					BookmarkCompleted();
					break;
			}
		}

		#endregion

    /// <summary>
    /// Handles the ItemClicked event of the Modal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Modal_ItemClicked(object sender, CommandEventArgs e)
		{
      var args = (LightboxEventCommandArguement)e.CommandArgument;
			List<LightboxBreadcrumb> breadcrumbs;
      ExplorerCriteria criteria;
			switch (e.CommandName)
			{
				case "ViewJob":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Job, out breadcrumbs, out criteria);
          Job.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewEmployer":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Employer, out breadcrumbs, out criteria);
          Employer.Show(args.Id, criteria, breadcrumbs);
          break;

				case "ViewDegreeCertification":
        case "ViewDegreeCertificationStudy":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.DegreeCertification, out breadcrumbs, out criteria);
          DegreeCertification.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewProgramAreaDegree":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.ProgramAreaDegree, out breadcrumbs, out criteria);
          ProgramAreaDegreeStudy.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewSkill":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Skill, out breadcrumbs, out criteria);
          Skill.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewInternship":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Internship, out breadcrumbs, out criteria);
          Internship.Show(args.Id, criteria, breadcrumbs);
          break;
			}
		}

    /// <summary>
    /// Builds the breadcrumbs and criteria.
    /// </summary>
    /// <param name="args">The args.</param>
    /// <param name="reportType">Type of the report.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="criteria">The criteria.</param>
    private void BuildBreadcrumbsAndCriteria(LightboxEventCommandArguement args, ReportTypes reportType, out List<LightboxBreadcrumb> breadcrumbs, out ExplorerCriteria criteria)
    {
      if (args.Breadcrumbs != null)
      {
        #region Filter cancelled
        // If a filter is cancelled we have the criteria and breadcrumbs already
        if (args.CriteriaHolder.IsNotNull())
        {
          criteria = args.CriteriaHolder;
          breadcrumbs = args.Breadcrumbs;
          return;
        }
        #endregion


        // Reuse the last criteria otherwise
        criteria = args.Breadcrumbs.Last().Criteria.Clone();

        #region Moving back through breadcrumbs
        // Remove last criteria from breadcrumbs if moving backwards
        if (args.MovingBack)
        {
          args.Breadcrumbs.RemoveAt(args.Breadcrumbs.Count - 1);
          breadcrumbs = args.Breadcrumbs;
          return;
        }
        #endregion

        // Set breadcrumbs
        breadcrumbs = args.Breadcrumbs;

        // When moving forward we may need to reset criteria depending on source and destination
        if (!args.MovingBack)
        {
          if (reportType == ReportTypes.Job || reportType == ReportTypes.Internship)
          {
            // Always clear down when moving to a job or internship modal
            var newCriteria = new ExplorerCriteria
            {
              StateArea = criteria.StateArea,
              StateAreaId = criteria.StateAreaId
            };
            criteria = newCriteria;
          }

          if (args.Breadcrumbs.Last().LinkType != ReportTypes.Internship)
          {
            // Remove internship details when movng away from the modal.
            criteria.InternshipCategoryId = null;
            criteria.InternshipCategory = null;
          }
          // Reset degree data when travelling to a degree modal
          if (reportType == ReportTypes.DegreeCertification)
          {
            // If moving from the job to the degree modal reset the degree criteria (Modal will set the right Ids)
            criteria.EducationCriteriaOption = EducationCriteriaOptions.DegreeLevel;
            criteria.DegreeLevel = DegreeLevels.AnyOrNoDegree;
            criteria.DegreeLevelLabel = CodeLocalise(DegreeLevels.AnyOrNoDegree, "Any/No Degree");
            criteria.DegreeEducationLevel = null;
            criteria.DegreeEducationLevelId = null;
            // Reset career area as well
            criteria.CareerArea = null;
            criteria.CareerAreaId = null;
          }
          else if (reportType == ReportTypes.Skill)
          {
            criteria.EmployerId = null;
            criteria.Employer = null;
          }
        }
      }
      else
      {
        criteria = ReportCriteria;
        breadcrumbs = null;
      }
    }

		#endregion

		#region page methods

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      ArrowsLiteral.Text = @"« ";
      BackButton.Text = CodeLocalise("Global.Back.Text", "BACK").ToUpper();
    }

    /// <summary>
    /// Binds the results.
    /// </summary>
		private void BindResults()
		{
			var firstResultsTitleTableRowId = String.Empty;
			var js = String.Empty;

			var siteSearchResults = String.IsNullOrEmpty(SearchTermLabel.Text)
			                        	? new List<SiteSearchResultModel>()
			                        	: ServiceClientLocator.ExplorerClient(App).GetSiteSearchResults(SearchTermLabel.Text);

			var results = siteSearchResults.OrderBy(x => x.ResultName).Where(x => x.ResultType == ReportTypes.Job).ToList();

			JobResultsLabel.Text = String.Format("({0} results)", results.Count);

			JobLeftColumnRepeater.Visible =
				JobRightColumnRepeater.Visible =
				EmployerLeftColumnRepeater.Visible =
				EmployerRightColumnRepeater.Visible =
				DegreeCertificationLeftColumnRepeater.Visible =
				DegreeCertificationRightColumnRepeater.Visible =
        ProgramAreaLeftColumnRepeater.Visible =
        ProgramAreaRightColumnRepeater.Visible = 
				SkillLeftColumnRepeater.Visible = SkillRightColumnRepeater.Visible = 
				InternshipLeftColumnRepeater.Visible = InternshipRightColumnRepeater.Visible = true;

			var columnListSize = DefaultColumnListSize;
			if (results.Count > 0)
			{
				if (String.IsNullOrEmpty(firstResultsTitleTableRowId))
					firstResultsTitleTableRowId = "JobResultsTitleTableRow";

				columnListSize = GetColumnListSize(results.Count);

				JobLeftColumnRepeater.DataSource = results.Take(columnListSize).ToList();
				JobLeftColumnRepeater.DataBind();
			}
			else
			{
				js += @"
$('#JobResultsTitleTableRow').unbind('click');";

				JobLeftColumnRepeater.Visible = false;
				JobRightColumnRepeater.Visible = false;
			}

			if (results.Count > columnListSize)
			{
				JobRightColumnRepeater.DataSource = results.Skip(columnListSize).Take(columnListSize).ToList();
				JobRightColumnRepeater.DataBind();
			}
			else
			{
				JobRightColumnRepeater.Visible = false;
			}

			results = siteSearchResults.OrderBy(x => x.ResultName).Where(x => x.ResultType == ReportTypes.Employer).ToList();

			EmployerResultsLabel.Text = String.Format("({0} results)", results.Count);

			if (results.Count > 0)
			{
				if (String.IsNullOrEmpty(firstResultsTitleTableRowId))
					firstResultsTitleTableRowId = "EmployerResultsTitleTableRow";

				columnListSize = GetColumnListSize(results.Count);

				EmployerLeftColumnRepeater.DataSource = results.Take(columnListSize).ToList();
				EmployerLeftColumnRepeater.DataBind();
			}
			else
			{
				js += @"
$('#EmployerResultsTitleTableRow').unbind('click');";

				EmployerLeftColumnRepeater.Visible = false;
				EmployerRightColumnRepeater.Visible = false;
			}

			if (results.Count > columnListSize)
			{
				EmployerRightColumnRepeater.DataSource = results.Skip(columnListSize).Take(columnListSize).ToList();
				EmployerRightColumnRepeater.DataBind();
			}
			else
			{
				EmployerRightColumnRepeater.Visible = false;
			}

			results = siteSearchResults.OrderBy(x => x.ResultName).Where(x => x.ResultType == ReportTypes.DegreeCertification).ToList();

			DegreeCertificationResultsLabel.Text = String.Format("({0} results)", results.Count);

			if (results.Count > 0)
			{
				if (String.IsNullOrEmpty(firstResultsTitleTableRowId))
					firstResultsTitleTableRowId = "DegreeCertificationResultsTitleTableRow";

				columnListSize = GetColumnListSize(results.Count);

				DegreeCertificationLeftColumnRepeater.DataSource = results.Take(columnListSize).ToList();
				DegreeCertificationLeftColumnRepeater.DataBind();
			}
			else
			{
				js += @"
$('#DegreeCertificationResultsTitleTableRow').unbind('click');";

				DegreeCertificationLeftColumnRepeater.Visible = false;
				DegreeCertificationRightColumnRepeater.Visible = false;
			}

			if (results.Count > columnListSize)
			{
				DegreeCertificationRightColumnRepeater.DataSource = results.Skip(columnListSize).Take(columnListSize).ToList();
				DegreeCertificationRightColumnRepeater.DataBind();
			}
			else
			{
				DegreeCertificationRightColumnRepeater.Visible = false;
			}

      if (App.Settings.Theme == FocusThemes.Education)
      {
        ProgramAreaPlaceHolder.Visible = true;

        results = siteSearchResults.OrderBy(x => x.ResultName).Where(x => x.ResultType == ReportTypes.ProgramAreaDegree).ToList();

        ProgramAreaResultsLabel.Text = String.Format(" ({0} results)", results.Count);

        if (results.Count > 0)
        {
          if (String.IsNullOrEmpty(firstResultsTitleTableRowId))
            firstResultsTitleTableRowId = "ProgramAreaResultsTitleTableRow";

          columnListSize = GetColumnListSize(results.Count);

          ProgramAreaLeftColumnRepeater.DataSource = results.Take(columnListSize).ToList();
          ProgramAreaLeftColumnRepeater.DataBind();
        }
        else
        {
          js += @"$('#ProgramAreaResultsTitleTableRow').unbind('click');";

          ProgramAreaLeftColumnRepeater.Visible = false;
          ProgramAreaRightColumnRepeater.Visible = false;
        }

        if (results.Count > columnListSize)
        {
          ProgramAreaRightColumnRepeater.DataSource = results.Skip(columnListSize).Take(columnListSize).ToList();
          ProgramAreaRightColumnRepeater.DataBind();
        }
        else
        {
          ProgramAreaRightColumnRepeater.Visible = false;
        }
      }

			results = siteSearchResults.OrderBy(x => x.ResultName).Where(x => x.ResultType == ReportTypes.Skill).ToList();

			SkillResultsLabel.Text = String.Format("({0} results)", results.Count);

			if (results.Count > 0)
			{
				if (String.IsNullOrEmpty(firstResultsTitleTableRowId))
					firstResultsTitleTableRowId = "SkillResultsTitleTableRow";

				columnListSize = GetColumnListSize(results.Count);

				SkillLeftColumnRepeater.DataSource = results.Take(columnListSize).ToList();
				SkillLeftColumnRepeater.DataBind();
			}
			else
			{
				js += @"
$('#SkillResultsTitleTableRow').unbind('click');";

				SkillLeftColumnRepeater.Visible = false;
				SkillRightColumnRepeater.Visible = false;
			}

			if (results.Count > columnListSize)
			{
				SkillRightColumnRepeater.DataSource = results.Skip(columnListSize).Take(columnListSize).ToList();
				SkillRightColumnRepeater.DataBind();
			}
			else
			{
				SkillRightColumnRepeater.Visible = false;
			}

			results = siteSearchResults.OrderBy(x => x.ResultName).Where(x => x.ResultType == ReportTypes.Internship).ToList();

			InternshipResultsLabel.Text = String.Format("({0} results)", results.Count);

			if (results.Count > 0)
			{
				if (String.IsNullOrEmpty(firstResultsTitleTableRowId))
					firstResultsTitleTableRowId = "InternshipResultsTitleTableRow";

				columnListSize = GetColumnListSize(results.Count);

				InternshipLeftColumnRepeater.DataSource = results.Take(columnListSize).ToList();
				InternshipLeftColumnRepeater.DataBind();
			}
			else
			{
				js += @"
$('#InternshipResultsTitleTableRow').unbind('click');";

				InternshipLeftColumnRepeater.Visible = false;
				InternshipRightColumnRepeater.Visible = false;
			}

			if (results.Count > columnListSize)
			{
				InternshipRightColumnRepeater.DataSource = results.Skip(columnListSize).Take(columnListSize).ToList();
				InternshipRightColumnRepeater.DataBind();
			}
			else
			{
				InternshipRightColumnRepeater.Visible = false;
			}

			if (!String.IsNullOrEmpty(firstResultsTitleTableRowId))
			{
				js = String.Format(@"
$(document).ready(function() {{
$('#{0}').click();
{1}
}});", firstResultsTitleTableRowId,
				                   js);
			}

			Page.ClientScript.RegisterStartupScript(GetType(), "SiteSearchResultsSetup", js, true);
		}

		private int GetColumnListSize(int resultCount)
		{
			var listSize = DefaultColumnListSize;
			if (resultCount > (DefaultColumnListSize * 2))
			{
				listSize = ((int) (resultCount/2)) + 1;
			}

			return listSize;
		}

    /// <summary>
    /// Creates the bookmark.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="bookmarkType">Type of the bookmark.</param>
    /// <param name="criteria">The criteria.</param>
		private void CreateBookmark(string name, BookmarkTypes bookmarkType, ExplorerCriteria criteria)
		{
			ServiceClientLocator.AnnotationClient(App).CreateBookmark(name, bookmarkType, criteria);

			Confirmation.Show(CodeLocalise("BookmarkConfirmation.Title", "Bookmark Confirmation"),
												CodeLocalise("BookmarkConfirmation.Body", "Your bookmark has been added"),
												CodeLocalise("Global.Close.Text", "Close"), "BookmarkConfirmationClose", height: 50, width: 250);
		}

		#endregion
	} 
}