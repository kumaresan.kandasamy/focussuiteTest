﻿<%@ Page Title="Explorer Report" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.Report" EnableEventValidation="false" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="Controls/JobList.ascx" TagName="JobList" TagPrefix="uc" %>
<%@ Register Src="Controls/EmployerList.ascx" TagName="EmployerList" TagPrefix="uc" %>
<%@ Register Src="Controls/DegreeCertificationList.ascx" TagName="DegreeCertificationList" TagPrefix="uc" %>
<%@ Register Src="Controls/DegreesBySchoolList.ascx" TagName="DegreesBySchoolList" TagPrefix="uc" %>
<%@ Register Src="Controls/SkillList.ascx" TagName="SkillList" TagPrefix="uc" %>
<%@ Register Src="Controls/InternshipList.ascx" TagName="InternshipList" TagPrefix="uc" %>
<%@ Register Src="Controls/CareerMovesList.ascx" TagName="CareerMoves" TagPrefix="uc" %>
<%@ Register Src="Controls/CareerAreaList.ascx" TagName="CareerAreaList" TagPrefix="uc" %>
<%@ Register Src="Controls/JobModal.ascx" TagName="JobModal" TagPrefix="uc" %>
<%@ Register Src="Controls/EmployerModal.ascx" TagName="EmployerModal" TagPrefix="uc" %>
<%@ Register Src="Controls/DegreeCertificationModal.ascx" TagName="DegreeCertificationModal" TagPrefix="uc" %>
<%@ Register Src="Controls/ProgramAreaDegreeStudyModal.ascx" TagName="ProgramAreaDegreeStudyModal" TagPrefix="uc" %>
<%@ Register Src="Controls/SkillModal.ascx" TagName="SkillModal" TagPrefix="uc" %>
<%@ Register Src="Controls/InternshipModal.ascx" TagName="InternshipModal" TagPrefix="uc" %>
<%@ Register Src="Controls/CareerAreaModal.ascx" TagName="CareerAreaModal" TagPrefix="uc" %>
<%@ Register Src="Controls/EmailModal.ascx" TagName="EmailModal" TagPrefix="uc" %>
<%@ Register Src="../Controls/ConfirmationModal.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>
<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="Controls/StateAreaSelector.ascx" TagName="StateAreaSelector" TagPrefix="uc" %>
<%@ Register Src="../Controls/UnAuthorizedInformation.ascx" TagName="UnAuthorizedModal" TagPrefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <asp:Literal ID="GoogleTrackingLiteral" runat="server" />
    <script language="javascript" type="text/javascript">
        function ClearJobDropdown(jobTextBoxId, jobTextBoxValueId, jobDropdownId) {
            $('#' + jobTextBoxId).val('');
            $('#' + jobTextBoxValueId).val('');
            $('#' + jobDropdownId).prop('selectedIndex', -1);
            $('#' + jobDropdownId).closest(".dropdownRow").hide();
        }
    </script>
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div style="position: relative; bottom: 20px;" id="BookmarkSearchPanel">
        <table width="100%" role="presentation">
            <tr>
                <td>
                    <div class="col-md-offset-5 col-md-7 col-sm-offset-4 col-sm-8 col-xs-12 col-lg-offset-6 col-lg-6">
                        <div class="bookMarkWidth col-md-4 col-sm-5 col-xs-12 col-lg-5">
                            <asp:Image ID="BookmarkImage" runat="server" Width="17" Height="32" AlternateText="Bookmark Image" />
                            <asp:LinkButton ID="BookmarkLinkButton" class="toolTipNew" runat="server" OnClick="BookmarkLinkButton_Click"><%= HtmlLocalise("BookmarkThisInfo.Text", "BOOKMARK THIS INFO")%></asp:LinkButton>
                        </div>
                        <div class="col-md-8 col-sm-7 col-xs-12 col-lg-7">
                            <div class="col-md-9 col-sm-10 col-xs-10 col-lg-9">
                                <%= HtmlInFieldLabel("SearchTermTextBox", "SearchCareersAndEducation.Label", "Search careers and education", 300)%>
                                <asp:TextBox ID="SearchTermTextBox" runat="server" ClientIDMode="Static" Width="245px" />
                                <asp:RequiredFieldValidator ID="SearchTermRequired" runat="server" ControlToValidate="SearchTermTextBox"
                                    CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="SiteSearch" />
                                <asp:CustomValidator ID="SearchTermValidator" runat="server" CssClass="error" ClientValidationFunction="SiteSearchTerm_Validate"
                                    SetFocusOnError="true" Display="Dynamic" ValidationGroup="SiteSearch" ValidateEmptyText="False"></asp:CustomValidator>
                            </div>
                            <div class="col-md-3 col-sm-2 col-xs-2 col-lg-3">
                                <asp:Button ID="SearchGoButton" runat="server" ClientIDMode="Static" CausesValidation="True"
                                    OnClick="SearchGoButton_Click" class="buttonLevel2" ValidationGroup="SiteSearch"
                                    UseSubmitBehavior="False" />
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <table style="clear:both; width:100%" role="presentation">
        <!-- row 1 - main report filter -->
        <tr>
            <td>
            <div id="MainReportFilter">
            <div class="col-md-5 col-sm-6 col-xs-12 col-lg-4">
                        <div class="col-md-5 col-sm-5 col-lg-5">
                            <span class="content">
                                <asp:Literal runat="server" ID="InDemandLiteral"></asp:Literal></span>
                        </div>
                        <div class="col-md-7 col-sm-7 col-lg-7">
                            <asp:DropDownList ID="ReportTypeDropDownList" runat="server" AutoPostBack="True"
                                OnSelectedIndexChanged="ReportTypeDropDownList_SelectedIndexChanged" Width="190" CssClass="toolTipNew" />
                        </div>
            </div>
            <div class="col-md-7 col-sm-6 col-xs-12 col-lg-8">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                        <div class="col-md-2 col-sm-2 col-lg-1">
                            <span class="content">
                                <%= HtmlLocalise("Global.In.Text", "in")%></span>
                        </div>
                        <div class=" dropDrownSpaceAlign col-md-10 col-sm-10 col-lg-11">
                            <div class="standardPanel">
                                <asp:Panel ID="CareerAreaLabelPanel" runat="server" CssClass="overlayPanelClosed overlayPanelOpenTrigger toolTipNew" Width="90%">
                                    <asp:Label ID="CareerAreaLabel" runat="server" Style="display: block;
                                        overflow: hidden;" />
                                </asp:Panel>
                                <div class="overlayPanelOpen explorerCareerDisplayArea" id="CareerAreaDisplayArea" runat="server">
                                    <table role="presentation">
                                        <tr>
                                            <td colspan="2">
                                                <span class="title">
                                                    <%= HtmlLocalise("CareerAreasPanel.Title", "Career Areas")%></span> <a href="#">
                                                        <img src="<%= UrlBuilder.CloseMedium() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>"
                                                            class="overlayPanelClosedTrigger" width="13" height="13" /></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                                <asp:RadioButton ID="CareerAreaRadioButton" runat="server" GroupName="CareerOption"
                                                    Checked="True" />
                                            </td>
                                            <td>
                                                <%= HtmlLocalise("SelectAnArea.Text", "Select an area")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:DropDownList ID="CareerAreaDropDownList" class="careerDropDown" runat="server" Title="Select Career Area" Width="330"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="10">
                                                <asp:RadioButton ID="JobTitleRadioButton" runat="server" GroupName="CareerOption" Title="job title"/>
                                            </td>
                                            <td>
                                                <%= HtmlLocalise("Explorer.EnterJobTitle", "or enter a job title")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <focus:AutoCompleteTextBox ID="JobTitleAutoCompleteTextBox" class="careerDropDown" runat="server" ClientIDMode="Static"
                                                    Width="330" AutoCompleteType="Disabled" AutoPostBackOnChange="True" OnSelectedValueChanged="JobTitleAutoCompleteTextBox_SelectedValueChanged" />
                                                <br />
                                                <asp:CustomValidator ID="JobTitleCustomValidator" runat="server" CssClass="error"
                                                    ClientValidationFunction="JobTitle_Validate" Display="Dynamic" ValidationGroup="CareerAreas"
                                                    SetFocusOnError="False"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr id="RelatedJobsSelectorTableRow" runat="server" visible="False" class="dropdownRow careerDropDown">
                                            <td colspan="2">
                                                <%= HtmlLocalise("Explorer.ThisJobTitleMostLike", "This job title is most like")%>
                                                <br />
                                                <asp:DropDownList runat="server" ID="RelatedJobsDropDown" class="careerDropDown" Width="330">
                                                </asp:DropDownList>
                                                <br />
                                                <asp:CustomValidator ID="RelatedJobsCustomValidator" runat="server" CssClass="error"
                                                    ClientValidationFunction="RelatedJobs_Validate" Display="Dynamic" ValidationGroup="CareerAreas"
                                                    SetFocusOnError="False"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr id="MilitaryOccupationRow1" runat="server" clientidmode="Static">
                                            <td width="10">
                                                <asp:RadioButton ID="MilitaryOccupationRadioButton" runat="server" GroupName="CareerOption" Title="military occupation classification"/>
                                            </td>
                                            <td>
	                                            <focus:LocalisedLabel runat="server" ID="MocLabel" ClientIDMode="Static" DefaultText="or enter a military occupational classification"/>
                                            </td>
                                        </tr>
                                        <tr id="MilitaryOccupationRow2" runat="server" clientidmode="Static">
                                            <td colspan="2">
                                                <focus:AutoCompleteTextBox ID="MilitaryOccupationTextBox" class="careerDropDown" runat="server" ClientIDMode="Static"
                                                    Width="330" AutoCompleteType="Disabled" />
                                                <br />
                                                <asp:CustomValidator ID="MilitaryOccupationCustomValidator" runat="server" CssClass="error"
                                                    ClientValidationFunction="MilitaryOccupation_Validate" Display="Dynamic" ValidationGroup="CareerAreas"
                                                    SetFocusOnError="False"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr id="CareerAreaSelectorResetTableRow" style="display: none;">
                                            <td colspan="2">
                                                <img src="<%= UrlBuilder.ExclamationIcon() %>" width="18" height="18"
                                                    alt="" /><%= HtmlLocalise("CareerAreaSelectorReset.Text", "To display the best results for your selection, we've changed your education selection to 'Any/No Degree'") %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:Button ID="CareerAreaGoButton" runat="server" class="buttonLevel2" OnClick="CareerAreaGoButton_Click"
                                                    ValidationGroup="CareerAreas" />
                                            </td>
                                        </tr>
                                    </table>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-6">
                        <div class="col-md-2 col-sm-2 col-lg-2">
                            <span class="content">
                                <%= HtmlLocalise("Global.For.Text", "for")%></span>
                        </div>
                        <div class=" dropDrownSpaceAlign col-md-10 col-sm-10 col-lg-10">
                            <div class="standardPanel">
                                <asp:Panel ID="EducationLabelPanel" runat="server" CssClass="overlayPanelClosed overlayPanelOpenTrigger" Width="90%">
                                    <asp:Label ID="EducationLabel" runat="server" CssClass="overlayPanelLabel" />
                                </asp:Panel>
                                <div class="overlayPanelOpen explorerCareerDegreeArea">
                                    <table role="presentation">
                                        <tr>
                                            <td colspan="2">
                                                <span class="title">
                                                    <%= HtmlLocalise("EducationPanel.Title", "Education")%></span> <a href="#">
                                                        <img src="<%= UrlBuilder.CloseMedium() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>"
                                                            class="overlayPanelClosedTrigger" width="13" height="13" /></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <%= HtmlLocalise("Currently.Text", "Currently")%>: <strong>
                                                    <asp:Label ID="EducationCurrentLabel" runat="server" /></strong>
                                            </td>
                                        </tr>
                                        <tr class="divider">
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <%= HtmlLocalise("ShowResultsFor.Text", "Show results for")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <asp:RadioButton ID="EducationOptionNonSpecificRadioButton" runat="server" GroupName="EducationOption"
                                                    Checked="True" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="EducationOptionNonSpecificDropDownList" runat="server" Width="260" Title="Select Education Option"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" valign="top">
                                                <asp:RadioButton ID="EducationOptionSpecificRadioButton" runat="server" GroupName="EducationOption" />
                                            </td>
                                            <td>
                                                <%= HtmlLocalise("SpecificDegreeOrCertification.Text", "this specific degree or certification")%>
                                            </td>
                                        </tr>
                                        <asp:PlaceHolder runat="server" ID="ProgramAreaPlaceHolder" Visible="False">
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="ProgramAreaRadioButton" runat="server" GroupName="EducationSubOption" />
                                                    <%= HtmlLocalise("ProgramAreaRadioButton.Text", "By department")%>
                                                    &nbsp;
                                                    <asp:RadioButton ID="DegreeRadioButton" runat="server" GroupName="EducationSubOption" />
                                                    <%= HtmlLocalise("DegreeRadioButton.Text", "By degree")%>
                                                </td>
                                            </tr>
                                            <tr id="ProgramAreaRow1" runat="server">
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="StudyProgramAreaDropDownList" Width="260" />
                                                </td>
                                            </tr>
                                            <tr id="ProgramAreaRow2" runat="server">
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="StudyProgramAreaDegreesDropDownList" Width="260" />
                                                </td>
                                            </tr>
                                        </asp:PlaceHolder>
                                        <tr id="DegreeRow1" runat="server">
                                            <td>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="DetailedDegreeLevelDropDownList" Width="260" Title="Select Degree Level">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="DegreeRow2" runat="server">
                                            <td>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="DetailedDegreeDropDownList" Width="288" Visible="False"/>
                                                <focus:AutoCompleteTextBox ID="DegreeCertificationAutoCompleteTextBox" class="careerDropDown" runat="server"
                                                    ClientIDMode="Static" Width="260" AutoCompleteType="Disabled" />
                                                <br />
                                                <asp:CustomValidator ID="DegreeCertificationValidator" runat="server" CssClass="error"
                                                    ClientValidationFunction="DegreeCertification_Validate" Display="Dynamic" ValidationGroup="DegreeArea"
                                                    SetFocusOnError="False"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr id="EducationSelectorResetTableRow" style="display: none;">
                                            <td colspan="2">
                                                <img src="<%= UrlBuilder.ExclamationIcon() %>" width="18" height="18"
                                                    alt="" /><%= HtmlLocalise("EducationSelectorReset.Text", "To display the best results for your selection, we've changed your career area selection to 'All Career Areas'")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:Button ID="EducationGoButton" runat="server" Text="Go »" class="buttonLevel2"
                                                    OnClick="EducationGoButton_Click" ValidationGroup="DegreeArea" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            </div>
                            </div>
                             </div>
                            </div>
                        </td>
                        </tr>
         <!-- row 3 - sub report filter -->
        <tr>
            <td>
                   <div id="subReportFilter" class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-5 col-sm-6 col-lg-4" style="margin-top:10px">
                        <div class="col-md-3 col-sm-3 col-lg-3">
                            <%= HtmlLocalise("Employer.Label", "#BUSINESS#")%>:
                        </div>
                        <div class="col-md-9 col-sm-9 col-lg-9">
                            <focus:AutoCompleteTextBox ID="EmployerAutoCompleteTextBox" runat="server" ClientIDMode="Static"
                                Width="250" AutoCompleteType="Disabled" AutoPostBackOnChange="True" OnSelectedValueChanged="EmployerAutoCompleteTextBox_SelectedValueChanged" />
                        </div>
                     </div>
                     <div class="col-md-7 col-sm-6 col-lg-8">
                     <div class=" col-md-12 col-sm-12 col-lg-7" style="margin-top:10px">
                     <div class="col-md-2 col-sm-3 col-lg-3">
                            <span>
                                <%= HtmlLocalise("Location.Label", "Location")%>:</span>
                        </div>
                        <div class="dropDrownSpaceAlign col-md-10 col-sm-9 col-lg-9">
                            <asp:PlaceHolder runat="server" ID="StateAreaPlaceHolder">
                                <div class="standardPanel" runat="server" style="width: 311px">
                                    <asp:Panel ID="StateAreaSelectorPanel" runat="server" CssClass="overlayPanelClosed overlayPanelOpenTrigger toolTipNew"
                                        width="90%" ToolTip="">
                                        <asp:Label ID="StateAreaSelectorLabel" runat="server" />
                                    </asp:Panel>
                                    <div class="overlayPanelOpen" id="StateAreaSelectorControlDiv">
                                        <table class="accordionContentTable" role="presentation" >
                                            <tr>
                                                <td>
                                                    <span class="title">
                                                        <%= HtmlLocalise("ReportStateArea.Title", "Location")%></span>
                                                </td>
                                                <td>
                                                    <a href="#">
                                                        <img src="<%= UrlBuilder.CloseMedium() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>"
                                                            class="overlayPanelClosedTrigger" width="13" height="13" /></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <uc:StateAreaSelector ID="ReportStateAreaSelector" runat="server" StateAfterHtml="<br />"
                                                        AreaBeforeHtml="<br />" AreaAfterHtml="" StateAreaValidationGroup="ReportStateArea" />
                                                    <asp:HiddenField runat="server" ID="ReportStateAreaId" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="text-align: right">
                                                    <asp:Button ID="StateAreaGoButton" runat="server" Text="Go »" class="buttonLevel2"
                                                        OnClick="StateAreaGoButton_Click" ValidationGroup="ReportStateArea" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 col-lg-4" style="margin-top:10px">
                        <div class="col-md-4 col-sm-5 col-lg-8">
                            <span style="white-space: nowrap;">
                                <%= HtmlLocalise("NumberOfResults.Label", "Number of results")%>:</span></div>
                                <div class="col-md-8 col-sm-7 col-lg-4">
                            <asp:DropDownList ID="ListSizeDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ListSizeDropDownList_SelectedIndexChanged"
                                Width="100" Title="Select Number of results">
                                <asp:ListItem Text="10" Value="10" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                <asp:ListItem Text="50" Value="50"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        </div>
                        </div>
                    </div>
            </td>
        </tr>
    </table>
    <div class="horizontalRule">
    </div>
    <div style="overflow-x:auto; overflow-y:hidden">
    <table width="100%" role="presentation" >
        <tr>
            <td>
                <asp:Literal runat="server" ID="NoDataAvailableLabel"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <uc:JobList ID="JobList" runat="server" OnItemClicked="ReportList_ItemClicked" JobListMode="Report" />
                <uc:EmployerList ID="EmployerList" runat="server" EmployerListMode="Report" OnItemClicked="ReportList_ItemClicked" />
                <uc:DegreeCertificationList ID="DegreeCertificationList" runat="server" OnItemClicked="ReportList_ItemClicked" />
                <uc:DegreesBySchoolList ID="DegreesBySchoolList" runat="server" OnItemClicked="ReportList_ItemClicked" />
                <uc:SkillList ID="SkillList" runat="server" OnItemClicked="ReportList_ItemClicked" />
                <uc:InternshipList ID="InternshipList" runat="server" OnItemClicked="ReportList_ItemClicked"
                    ListMode="Report" />
                <uc:CareerMoves ID="CareerMoves" runat="server" OnItemClicked="ReportList_ItemClicked" />
                <uc:CareerAreaList ID="CareerAreaList" runat="server" OnItemClicked="ReportList_ItemClicked" />
            </td>
        </tr>
    </table>
    </div>
    <%-- Modals --%>
    <uc:JobModal ID="Job" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked"
        OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
    <uc:EmployerModal ID="Employer" runat="server" OnItemClicked="Modal_ItemClicked"
        OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked"
        OnPrintClicked="Modal_PrintClicked" />
    <uc:DegreeCertificationModal ID="DegreeCertification" runat="server" OnItemClicked="Modal_ItemClicked"
        OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked"
        OnPrintClicked="Modal_PrintClicked" />
    <uc:ProgramAreaDegreeStudyModal ID="ProgramAreaDegreeStudy" runat="server" OnItemClicked="Modal_ItemClicked"
        OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked"
        OnPrintClicked="Modal_PrintClicked" />
    <uc:SkillModal ID="Skill" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked"
        OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />
    <uc:InternshipModal ID="Internship" runat="server" OnItemClicked="Modal_ItemClicked"
        OnSendEmailClicked="Modal_SendEmailClicked" OnBookmarkClicked="Modal_BookmarkClicked"
        OnPrintClicked="Modal_PrintClicked" />
    <uc:CareerAreaModal ID="CareerArea" runat="server" OnItemClicked="Modal_ItemClicked" OnSendEmailClicked="Modal_SendEmailClicked"
        OnBookmarkClicked="Modal_BookmarkClicked" OnPrintClicked="Modal_PrintClicked" />

    <uc:EmailModal ID="Email" runat="server" OnCancelled="Email_Cancelled" OnCompleted="Email_Completed" />
    <uc:ConfirmationModal ID="Confirmation" runat="server" OnCloseCommand="Confirmation_CloseCommand" />
		<uc:UnAuthorizedModal ID="UnAuthorizedModal" runat="server" />

    <script language="javascript" type="text/javascript">
        // following script will rebind tooltips after update panel postback
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            BindTooltips();
        });

        function JobTitle_Validate(source, args) {
          if ($("#<%=JobTitleRadioButton.ClientID %>").is(":checked")) {
            args.IsValid = ($("#JobTitleAutoCompleteTextBox_SelectedValue").val() != '');
          } else {
            args.IsValid = true;
          }
        }

        function RelatedJobs_Validate(source, args) {
          if ($("#<%=JobTitleRadioButton.ClientID %>").is(":checked")) {
            args.IsValid = ($("#<%=ReportTypeDropDownList.ClientID %>").val() != 'CareerMoves' || $("#<%=RelatedJobsDropDown.ClientID %>").val() != '');
          }
        }

        function MilitaryOccupation_Validate(source, args) {
            if ($("#MilitaryOccupationAutoCompleteTextBox").val() == '') return true;
            args.IsValid = ($("#MilitaryOccupationAutoCompleteTextBox_SelectedValue").val() != '');
        }

        function DegreeCertification_Validate(source, args) {
          if ($("#DegreeCertificationAutoCompleteTextBox").val() == '') return true;
          args.IsValid = ($("#DegreeCertificationAutoCompleteTextBox_SelectedValue").val() != '');
        }
    </script>
</asp:Content>
