<%@ Page Title="All Page Elements" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllPageElements.aspx.cs" Inherits=" Focus.CareerExplorer.WebExplorer.AllPageElements" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

<style type="text/css">
  #allPageElements  tr  td {padding:4px;}
</style>

<table id="allPageElements" class="FocusCareer">
  <tr>
    <td>Body text</td>
    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="#">hyperlink</a></td>
  </tr>
  <tr>
    <td>Local navigation text in Focus/Career� (below tab)</td>
    <td><p class="pageNav"><strong>Your Resume</strong> | <a href="#">Search jobs</a><a href="#"></a></p></td>
  </tr>
  <tr>
    <td>Instructional Text</td>
    <td><span class="instructionalText">last saved Oct 31</span></td>
  </tr>
  <tr>
    <td>Page and lightbox heads</td>
    <td><h1>Lorem ipsum</h1></td>
  </tr>
  <tr>
    <td valign="top">Button level 2</td>
    <td><input type="submit" name="ctl00$MainContent$Button2" value="Lorem ipsum" id="Submit4" class="buttonLevel2"></td>
  </tr>
  <tr>
    <td valign="top">Button level 3</td>
    <td><input type="submit" name="ctl00$MainContent$Button2" value="Lorem ipsum" id="MainContent_Button2" class="buttonLevel3"></td>
  </tr>
  <tr>
    <td valign="top">Bulletted list</td>
    <td><ul>
                  <li><a href="#">Skill 1</a></li>
                  <li><a href="#">Skill 2</a></li>
                  <li><a href="#">Skill 3</a></li>
                </ul>
      </td>
  </tr>
  <tr>
    <td valign="top">Accordion with icon table</td>
    <td>
      <table class="accordionWithIcon">
        <tbody><tr class="multipleAccordionTitle on">
          <td valign="top" width="3%"><span class="accordionIcon"></span></td>
          <td valign="top" width="25%"><a href="#">Job</a></td>
          <td valign="top" width="24%"><a href="#">Hiring Demand</a></td>
          <td valign="top" width="24%"><a href="#">Salary</a></td>
          <td valign="top" width="24%"><a href="#">People<br>Employed</a></td>
        </tr>
        <tr class="accordionContent open" style="display: table-row; ">
          <td colspan="5">
            <table width="100%">
              <tbody><tr>
                <td width="3%"></td>
                <td width="25%"><strong>1. <a href="#">Clerk</a></strong></td>
                <td width="24%">High and growing</td>
                <td width="24%"><img src="<%= UrlBuilder.JobSalaryHighImage() %>" alt="High Salary" width="37" height="19"></td>
                <td width="24%"><img src="<%= UrlBuilder.JobPeopleMediumImage() %>" alt="Some People" width="24" height="30"></td>
              </tr>
            </tbody></table>
          </td>
        </tr>
      </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Promo style 3</td>
    <td><span class="promoStyle3">Ready to make a move?</span><span class="promoStyle3Arrow"></span></td>
  </tr>
  <tr>
    <td valign="top">Promo style 4</td>
    <td><span class="promoStyle4"><a href="#">See current postings for<br>this job �</a></span></td>
  </tr>
  <tr>
    <td valign="top">Overlay panel closed</td>
    <td>
      <div class="overlayPanelContainer standardPanel">

  <div class="overlayPanelClosed overlayPanelOpenTrigger" style="width: 195px; ">
    <span>All Career Areas</span>
  </div>

  <div class="overlayPanelOpen" style="display: none; ">
    <table>
      <tbody><tr>
        <td colspan="2"><span class="title">Career Areas</span> <a href="#"><img src="<%= UrlBuilder.CloseMedium() %>" alt="Close" class="overlayPanelClosedTrigger" width="13" height="13"></a></td>
      </tr>
      <tr>
        <td colspan="2">Select an area</td>
      </tr>
      <tr>
        <td colspan="2">
          <select name="ctl00$MainContent$DropDownList1" id="MainContent_DropDownList1" style="width: 190px; position: absolute; opacity: 0; font-size: 1.05em; height: 4px; ">
	<option value="All Career Areas">All Career Areas</option>

</select><span class="customStyleSelectBox" style="display: inline-block; "><span class="customStyleSelectBoxInner" style="width: 172px; display: inline-block; ">All Career Areas</span></span>
        </td>
      </tr>
      <tr>
        <td colspan="2">or</td>
      </tr>
      <tr>
        <td></td>
        <td><input name="ctl00$MainContent$ctl00" type="text" style="width:96%;"></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="ctl00$MainContent$Button1" value="Go �" id="MainContent_Button1" class="buttonLevel2"></td>
      </tr>
    </tbody></table>
  </div>

</div>
    </td>
  </tr>
  <tr>
    <td valign="top">Overlay panel open</td>
    <td><div class="overlayPanelOpen" style="display: block; position:static !important; ">
    <table>
      <tbody><tr>
        <td colspan="2"><span class="title">Career Areas</span> <a href="#"><img src="<%= UrlBuilder.CloseMedium() %>" alt="Close" class="overlayPanelClosedTrigger" width="13" height="13"></a></td>
      </tr>
      <tr>
        <td colspan="2">Select an area</td>
      </tr>
      <tr>
        <td colspan="2">
          <span class="customStyleSelectBox" style="display: inline-block; "><span class="customStyleSelectBoxInner" style="width: 172px; display: inline-block; ">All Career Areas</span></span>
        </td>
      </tr>
      <tr>
        <td colspan="2">or</td>
      </tr>
      <tr>
        <td></td>
        <td><input name="ctl00$MainContent$ctl00" type="text" style="width:96%;"></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="ctl00$MainContent$Button1" value="Go �" id="Submit1" class="buttonLevel2"></td>
      </tr>
    </tbody></table>
  </div></td>
  </tr>
  <tr>
    <td valign="top">Log in panel</td>
    <td>
      <table width="540px" class="LoginRegisterPanel" style="margin-left:0;">
      <tbody><tr>
        <td colspan="2">
         <div class="LoginRegisterHeaderPanel on">Log in</div>
        </td>
      </tr>
      <tr>
        <td width="10px"></td>
        <td>
          <table>
            <tbody><tr>
              <td colspan="2">
                <span class="inFieldLabel"><label for="EmailTextBox" style="width: 288px">EmailTextBox.Label</label></span>
                <input name="ctl00$MainContent$EmailTextBox" type="text" id="EmailTextBox" style="width:288px;">
              </td>
            </tr>
            <tr>
              <td>
                <span class="inFieldLabel"><label for="PasswordTextBox" style="width: 288px">PasswordTextBox.Label</label></span>
                <input name="ctl00$MainContent$PasswordTextBox" type="text" id="PasswordTextBox" style="width:288px;">
              </td>
              <td><input type="submit" name="ctl00$MainContent$Button1" value="Log in �" id="Submit2" class="buttonLevel2"></td>
            </tr>
            <tr>
              <td colspan="2"><a href="#">Forgot your username or password?</a></td>
            </tr>
          </tbody></table>
        </td>
      </tr>
      <tr>
        <td colspan="2">
         <div class="LoginRegisterHeaderPanel">Register for an account</div>
        </td>
      </tr>
      <tr class="LoginRegisterHeaderContent">
        <td width="10px"></td>
        <td>Registering only takes two steps, and allows you to create or upload a resume and see all job postings, including ones recommended for you based on your skills and expertise.</td>
      </tr>
      <tr>
        <td colspan="2"><br></td>
      </tr>
      <tr>
        <td colspan="2">
         <div class="LoginRegisterHeaderPanel">Search for jobs without registering</div>
        </td>
      </tr>
      <tr class="LoginRegisterHeaderContent">
        <td width="10px"></td>
        <td>You may search for jobs without registering, but you will not be able to view the full range of jobs available, and results will not be tailored to your skills and expertise.</td>
      </tr>
      <tr>
        <td colspan="2"><br></td>
      </tr>
    </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Dismissable status message</td>
    <td><div class="dismissableStatusMessage">
      <img src="<%= UrlBuilder.CloseMedium() %>" alt="Icon Close" width="13" height="13">
      Your changes to <strong>Job Search Name 1</strong> have been saved. Updated results are below.
      </div></td>
  </tr>
  <tr>
    <td valign="top">Show/hide accordion</td>
    <td>
      <table width="100%" class="showHideAccordion">
      <tbody><tr class="showHideAccordionTitle on">
        <td class="column2">New matches for your saved searches</td>
        <td class="column3" align="right" valign="top"><a class="show" href="#">SHOW</a><a class="hide" href="#">HIDE</a></td>
      </tr>
      <tr class="accordionContent open" style="display: table-row; ">
        <td colspan="2">
          <table>
            <tbody><tr>
              <td width="100px"><strong>Active search</strong></td>
              <td width="180px">
                <span class="customStyleSelectBox" style="display: inline-block; "><span class="customStyleSelectBoxInner" style="width: 134px; display: inline-block; ">Job Search Name 1</span></span>
              </td>
              <td><a href="#">Edit this search</a></td>
            </tr>
          </tbody></table>
          <table>
            <tbody><tr>
              <td width="90px">
                <img src="<%= UrlBuilder.StarOn() %>" alt="Star1" width="12" height="12">
                <img src="<%= UrlBuilder.StarOn() %>" alt="Star2" width="12" height="12">
                <img src="<%= UrlBuilder.StarOn() %>" alt="Star3" width="12" height="12">
                <img src="<%= UrlBuilder.StarOff() %>" alt="Star4" width="12" height="12">
                <img src="<%= UrlBuilder.StarOff() %>" alt="Star5" width="12" height="12">
              </td>
              <td colspan="2"><strong><a href="#">Recommended job</a></strong> - Employer Name (Nov7)</td>
            </tr>
            <tr>
              <td colspan="3"><a href="#">See all matches for this search</a></td>
            </tr>
          </tbody></table>
        </td>
      </tr>

    </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">In field label</td>
    <td>
            <span class="inFieldLabel"><label for="FindAJobTextBox" style="width: 168px">FindAJobTextBox.Label</label></span>
            <input name="ctl00$MainContent$FindAJobTextBox" type="text" id="FindAJobTextBox" style="width:168px;">
            <input type="submit" name="ctl00$MainContent$Button1" value="Go �" id="Submit3" class="buttonLevel2">
          </td>
  </tr>
  <tr>
    <td valign="top">Purple side panel</td>
    <td><a href="#" class="purpleSidePanel">
        <table>
          <tbody><tr>
            <td><img src="<%= UrlBuilder.CreateResume() %>" alt="Edit Resume" width="26" height="26"></td>
            <td>Create, edit, or upload a resume</td>
          </tr>
        </tbody></table>
      </a></td>
  </tr>
  <tr>
    <td valign="top">Blue side panel</td>
    <td><a href="#" class="blueSidePanel">
        <table>
          <tbody><tr>
            <td><img src="<%= UrlBuilder.ExploreSectionExploreImage() %>" alt="Explore Careers" width="26" height="26"></td>
            <td>Explore my career options</td>
          </tr>
        </tbody></table>
      </a></td>
  </tr>
  <tr>
    <td valign="top">Heading 2</td>
    <td><h2>My resumes</h2></td>
  </tr>
  <tr>
    <td valign="top">Dashboard side panel text</td>
    <td class="dashboardSidePanel"><a href="#">This Is My Resume Name</a> (Mar 3) <a href="#"><img src="<%= UrlBuilder.CloseSmall() %>" alt="Close" width="10" height="10"></a></td>
  </tr>
  <tr>
    <td valign="top">Tooltips</td>
    <td>
      <img src="<%= UrlBuilder.HelpIcon() %>" alt="Help" width="18" height="17" class="toolTipHover">
      <div class="tooltip">
        <div class="tooltipWithArrow helpMessage">
          <div class="tooltipPopup" style="display:block;position:static !important;margin-top:0 !important; ">
            <div class="tooltipPopupInner">
            <div class="toolTipClose"></div>
            <div class="arrow" style="margin-top: 20px; "></div>
            nnerThis is an extra long tooltip with arrow. This is an extra lonrow. This is an extra long tooltip with arrow. oltip with arrow. This is an extra lonrow. This is an extra long tooltip with arrow.
            </div>
          </div> 
        </div>
      </div>
    </td>
  </tr>
  <tr>
    <td valign="top">Check boxes</td>
    <td>
      <table id="MainContent_ctl01" class="checkBoxList">
	      <tbody><tr>
		      <td><input id="MainContent_ctl01_0" type="checkbox" name="ctl00$MainContent$ctl01$0" value="Less than a bachelor's degree (e.g., high school, associate degree)"><label for="MainContent_ctl01_0">Less than a bachelor's degree (e.g., high school, associate degree)</label></td>
	      </tr><tr>
		      <td><input id="MainContent_ctl01_1" type="checkbox" name="ctl00$MainContent$ctl01$1" value="Bachelor's degree"><label for="MainContent_ctl01_1">Bachelor's degree</label></td>
	      </tr><tr>
		      <td><input id="MainContent_ctl01_2" type="checkbox" name="ctl00$MainContent$ctl01$2" value="Graduate degree"><label for="MainContent_ctl01_2">Graduate degree</label></td>
	      </tr>
      </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Radio buttons</td>
    <td>
      <table width="100%">
              <tbody><tr class="first">
                <td width="176px"><input id="MainContent_JobsThatAreAtLeast" type="radio" name="ctl00$MainContent$JobMatching" value="JobsThatAreAtLeast"><label for="MainContent_JobsThatAreAtLeast">Jobs that are at least a</label></td>

                <td>for my resume</td>
              </tr>
              <tr class="last">
                <td colspan="3" width="176px"><input id="MainContent_AllJobMatchingResume" type="radio" name="ctl00$MainContent$JobMatching" value="AllJobMatchingResume"><label for="MainContent_AllJobMatchingResume">All jobs, without matching your resume</label></td>
              </tr>
            </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Grey bordered textbox</td>
    <td class="steppedProcess">
      <input name="ctl00$MainContent$TextBox1" type="text" id="MainContent_TextBox1" style="width:300px;">
    </td>
  </tr>
  <tr>
    <td valign="top">Purple bordered textbox</td>
    <td><input name="ctl00$MainContent$TextBox1" type="text" id="Text1" style="width:300px;"></td>
  </tr>
  <tr>
    <td valign="top">Stepped process subhead</td>
    <td class="steppedProcessSubhead"><strong>Step 2</strong> of 2</td>
  </tr>
  <tr>
    <td valign="top"></td>
    <td>
      <table class="landingPageNavigation" style="margin-top:40px;">
      <tbody><tr class="jobSearch large">
        <td class="column1" valign="bottom"><img src="<%= UrlBuilder.CreateResumeMedium() %>" alt="Search for jobs" width="41" height="40"></td>
        <td class="column2"><a href=""><span class="largeHeading">Create or upload a resume</span></a><span class="headingContent">best  bet for best matches</span></td>
      </tr>
      <tr class="divider"><td colspan="2"></td></tr>
      <tr class="multipleAccordionTitle careersAndEducation">
        <td class="column1"><img src="<%= UrlBuilder.ExploreSectionExploreMediumImage() %>" alt="Search for jobs" width="41" height="40"></td>
        <td class="column2"><a href=""><span class="largeHeading">Explore my career options</span></a></td>
      </tr>
    </tbody></table>
    </td>
  </tr>
    <tr>
    <td valign="top">Landing page navigation (expanded)</td>
    <td>
      <table width="100%" class="FocusCareer">
          <tbody><tr>
            <td><p class="pageNav"><a href="#">Your resume</a> | Search jobs<strong></strong></p></td>
          </tr>
          <tr>
            <td>
              <table class="landingPageNavigation" style="margin-top:0;" width="100%">
                <tbody>
                <tr class="jobSearch open" style="">
                  <td class="column1" valign="bottom"><img src="<%= UrlBuilder.ExploreSectionSearchImage() %>" alt="Search for jobs" width="50" height="51"></td>
                  <td><a href=""><span class="largeHeading">Search for jobs</span></a></td>
                  <td class="column2"><span class="headingContent">results are limited without a resume</span></td>
                </tr>

                <tr class="horizontalRule">
                  <td colspan="3"><div></div></td>
                </tr>
                <tr class="jobSearch expanded">
                  <td class="column1" valign="bottom"></td>
                  <td><img src="<%= UrlBuilder.BlockArrowRight() %>" alt="Search for jobs" width="26" height="26"><a href=""><span class="largeHeading">Start or continue a job search without a resume</span></a></td>
                  <td class="column2"><span class="headingContent">results are limited without a resume</span></td>
                </tr>
              </tbody></table>	
            </td>
          </tr>
        </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Pager</td>
    <td>
      Page(s) � <a href="#">previous</a> 1 2  3 ....  8 9 10 <a href="#">next</a> �
    </td>
  </tr>
  <tr>
    <td valign="top">Generic table with header</td>
    <td>
      <table width="100%" class="genericTable withHeader">
        <tbody><tr>
          <th valign="top" width="20%"><a href="#">DATE <img src="<%= UrlBuilder.SortUp() %>" alt="Up" width="10" height="7" style="position:relative;bottom:3px;"></a></th>
          <th valign="top" width="20%"><a href="#">JOB TITLE</a></th>
          <th valign="top">EMPLOYER</th>
          <th valign="top">JOB LOCATIONS</th>
          <th valign="top">ACTIONS</th>
        </tr>
        <tr>
          <td>23 Oct</td>
          <td><a href="#">Maintenance Supervisor</a></td>
          <td>Employer Name</td>
          <td>Baton Rouge, LA</td>
          <td><a href="#">Find more jobs like this</a></td>
        </tr>
      </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Promo style 5</td>
    <td><span class="promoStyle5"><a href="#">Register for Focus/Career� and create a resume</a> to see how well you match up against this job �</span></td>
  </tr>
  <tr>
    <td valign="top">Bookmark/mail/print icons</td>
    <td>
      <table>
        <tbody><tr>
          <td><img src="<%= UrlBuilder.BookmarkIcon() %>" alt="Search for jobs" width="17" height="32"></td>
          <td valign="top">BOOKMARK THIS INFO</td>
          <td valign="top" align="right"><a href="#"><img src="<%= UrlBuilder.EmailIcon() %>" alt="Search for jobs" width="32" height="24"></a></td>
          <td width="40px" valign="top" align="right"><a href="#"><img src="<%= UrlBuilder.PrintIcon() %>" alt="Search for jobs" width="30" height="24"></a></td>
        </tr>
      </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Upload resume with file upload</td>
    <td>
      <table width="100%" class="genericTable withHeader">
        <tbody><tr>
          <th valign="top">Upload a resume</th>
        </tr>
        <tr>
          <td>You may upload resumes in DOC, DOCX, RTF, or PDF formats.  Your PDF must be in text, not images � if you can search, highlight, and copy the words within your resume, the PDF is in text.  After you�ve uploaded your resume, you�ll have the opportunity to edit it.  You may also set the resume to be viewed with its original formatting by selecting �Original� on the review section. <u> Note: The ability to view the uploaded resume with its original formatting is only available for DOC, DOCX, or RTF formats and not PDF.</u></td>
        </tr>
        <tr>
          <td align="right">
          <div class="customfile"><input type="file" name="Test" class="fileUpload customfile-input"></div>
          </td>
        </tr>
        <tr class="last">
          <td align="right"><a href="#">Choose different file</a></td>
        </tr>
      </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top"></td>
    <td>
    <table>
      <tr>
    <td valign="bottom" align="right">
      <table id="ChartTable">
        <tbody><tr>
          <td style="padding:20px 20px 0 0" align="right">estimated time remaining:<br>50 minutes</td>
          <td align="left">
          <div id="ChartWrap">
            <div id="pieChartBorder"></div>
            <div id="PieChartWrap">
              <div id="PieChart" style="width: 84px; height: 84px; position: relative; " class="jqplot-target"><canvas width="84" height="84" style="position: absolute; left: 0px; top: 0px; " class="jqplot-base-canvas"></canvas><div class="jqplot-title" style="height: 0px; width: 0px; "></div><canvas width="84" height="84" class="jqplot-grid-canvas" style="position: absolute; left: 0px; top: 0px; "></canvas><canvas width="84" height="84" style="position: absolute; left: 0px; top: 0px; " class="jqplot-series-shadowCanvas"></canvas><canvas width="84" height="84" style="position: absolute; left: 0px; top: 0px; " class="jqplot-series-canvas"></canvas><canvas width="84" height="84" style="position: absolute; left: 0px; top: 0px; " class="jqplot-pieRenderer-highlight-canvas"></canvas><canvas width="84" height="84" style="position: absolute; left: 0px; top: 0px; " class="jqplot-event-canvas"></canvas></div>
            </div>
          </div>
          </td>
        </tr>
      </tbody></table>
    </td>
  </tr>
  </table>
    </td>
  </tr>
  <tr>
    <td valign="top"></td>
    <td>
      <div id="RCPNavigation">
        <div id="RCPNavigationTop" style="width:590px;">
          <ul>
            <li class="first selected"><a href="#">WORK HISTORY</a></li>
            <li><a href="#">CONTACT</a></li>
            <li><a href="#">EDUCATION</a></li>
            <li><a href="#">SUMMARY</a></li>
            <li><a href="#">ADD-INS</a></li>
            <li><a href="#">PROFILE</a></li>
            <li><a href="#">PREFERENCES</a></li>
            <li class="last"><a href="#">REVIEW</a></li>
          </ul>
        </div>
        <div id="RCPNavigationBottom" style="width:590px;">
          <ol>
            <li><a href="#">Add a job</a></li>
            <li class="selected"><a href="#">Job title details</a></li>
            <li><a href="#">Work activities</a></li>
            <li><a href="#">Job description</a></li>
          </ol>
        </div>
      </div>
    </td>
  </tr>
  <tr>
    <td valign="top">Collapsable panel</td>
    <td>
      <table class="collapsablePanel">
              <tbody><tr class="collapsableAccordionTitle">
                <td valign="top" width="30px"><span class="accordionIcon"></span></td>
                <td valign="top" width="235px"><a href="#">KEYWORDS</a></td>
                <td valign="top" width="400px"></td>
              </tr>

            </tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Numbered accordion</td>
    <td>
      <table width="400px" class="FocusCareer numberedAccordion">
  <tbody>
  <tr id="JobModalSection1TitleTableRow" class="multipleAccordionTitle2 on">
              <td width="10" class="column1">1</td>
              <td class="column2">Salary &amp; hiring trends</td>
              <td class="column3" align="right" valign="top"><a class="show" href="#">SHOW</a><a class="hide" href="#">HIDE</a></td>
            </tr>
            <tr class="accordionContent2 open" style="display: table-row; ">
              <td></td>
              <td>
              	<p>Salary and hiring trends provide an overview of salary and hiring demand, nationally and by geographic area, for this occupation � as well as the available positions in the selected area over the last 12 months.</p>
              	<div class="chartSalaryTrend">
                  <table width="100%" role="presentation">
                    <tbody><tr>
                    	<td colspan="5">Hiring demand: <strong>Very High</strong></td>
                    </tr>
                    <tr class="chartLabels">
                      <td width="87px">
                        <div style="height:16px;margin-left:27px;">
                        <span class="toolTipHover">LOW</span>
                          <div class="tooltip">
									          <div class="tooltipWithArrow helpMessage">
										          <div class="tooltipPopup" style="margin-left: 27px; margin-top: -24px; ">
											          <div class="tooltipPopupInner">
											          <div class="arrow" style="margin-top: 6px; "></div>
												          Based on demand, this job ranks in the bottom half of all occupations that are available.
											          </div>
										          </div> 
									          </div>
								          </div>
                        </div>
                      </td>
                      <td width="87px">
                        <div style="height:16px;margin-left:12px;">
                        <span class="toolTipHover">MEDIUM</span>
                          <div class="tooltip">
									          <div class="tooltipWithArrow helpMessage">
										          <div class="tooltipPopup" style="margin-left: 67px; margin-top: -31.5px; ">
											          <div class="tooltipPopupInner">
											          <div class="arrow" style="margin-top: 12.5px; "></div>
												          Based on employer demand, this job ranks in the lower tier of the top half of all occupations that are available.
											          </div>
										          </div> 
									          </div>
								          </div>
                        </div>
                      </td>
                      <td width="87px">
                        <div style="height:16px;margin-left:23px;">
                        <span class="toolTipHover">HIGH</span>
                          <div class="tooltip">
									          <div class="tooltipWithArrow helpMessage">
										          <div class="tooltipPopup" style="margin-left: 31px; margin-top: -24px; ">
											          <div class="tooltipPopupInner">
											          <div class="arrow" style="margin-top: 6px; "></div>
												          Based on employer demand, this job ranks in the top 70-80 occupations of all occupations that are available.
											          </div>
										          </div> 
									          </div>
								          </div>
                        </div>
                      </td>
                      <td width="87px">
                        <div style="height:16px;margin-left:7px;">
                        <span class="toolTipHover">VERY HIGH</span>
                          <div class="tooltip">
									          <div class="tooltipWithArrow helpMessage">
										          <div class="tooltipPopup">
											          <div class="tooltipPopupInner">
											          <div class="arrow"></div>
												          Based on employer demand, this job ranks in the top 20-30 occupations of all occupations that are available.
											          </div>
										          </div> 
									          </div>
								          </div>
                        </div>
                      </td>
                      <td></td>
                    </tr>
                    <tr class="chartBars">
                      <td><span></span></td>
                      <td><span></span></td>
                      <td><span></span></td>
                      <td><span></span></td>
                      <td width="260px">&nbsp;</td>
                    </tr>
                    <tr class="chartArrow">
                      <td colspan="5">
                        <div class="arrowWrap">
                        	<span class="arrow vhighHigh"></span>
                        </div>
                      </td>
                    </tr>
                  </tbody></table>
                  <table width="100%" role="presentation">
                    <tbody><tr>
                      <td width="50%">
                      	Positions available in the past 12 months: 
                      </td>
											<td class="chartInfo">
												&nbsp; <strong>654,987 positions</strong> <img id="MainContent_Job_HiringDemandImage" src="<%= UrlBuilder.JobHiringTrendVeryHighImage() %>" style="height:35px;width:35px;" alt="MainContent Job HiringDemand Image">
											</td>
                    </tr>
										<tr>
											<td colspan="2" height="10px;"></td>
										</tr>
                    <tr>
                      <td>
                      	Salary range for people employed in this occupation in selected area: 
                      </td>
											<td class="chartInfo">
												&nbsp; <strong>$70,300 - $111,990/yr</strong> 
                        <img id="MainContent_Job_SalaryRangeImage" src="<%= UrlBuilder.JobSalaryRangeHighImage() %>" style="height:35px;width:35px;" alt="MainContent Job SalaryRange Image">
                        <div style="display:inline-block">
                        <img src="<%= UrlBuilder.HelpIcon() %>" width="18" height="17" class="toolTipHover" alt="Help">
									      <div class="tooltip">
										      <div class="tooltipWithArrow helpMessage">
											      <div class="tooltipPopup">
												      <div class="tooltipPopupInner">
												      <div class="arrow"></div>
													      For individuals employed in your selected search area (statewide or MSA), Focus/Explorer� provides a salary range based on state-specific data from the U.S. Bureau of Labor Statistics.
												      </div>
											      </div> 
										      </div>
									      </div>
                        </div>
											</td>
                    </tr>
										
										<tr>
											<td colspan="2" height="10px;"></td>
										</tr>
                    <tr>
                      <td>
                      	Average salary advertised in recent job postings nationwide: 
                      </td>
											<td class="chartInfo">
												&nbsp; <strong>$77,954/yr</strong> <img id="MainContent_Job_SalaryNationwideImage" src="<%= UrlBuilder.JobSalaryRangeHighImage() %>" style="height:35px;width:35px;" alt="MainContent Job Salary Nationwide Image">
                        <div style="display:inline-block">
                        <img src="<%= UrlBuilder.HelpIcon() %>" width="18" height="17" class="toolTipHover" alt="Help">
									      <div class="tooltip">
										      <div class="tooltipWithArrow helpMessage">
											      <div class="tooltipPopup">
												      <div class="tooltipPopupInner">
												      <div class="arrow"></div>
													      For individuals employed in this job, Focus/Explorer� provides an average salary based on real-time employer job postings nationwide.
												      </div>
											      </div> 
										      </div>
									      </div>
                        </div>
											</td>
                    </tr>
										
                  </tbody></table>
                </div>
              </td>
							<td></td>
            </tr>

  <tr id="JobModalSection2TitleTableRow" class="multipleAccordionTitle2 on">
              <td class="column1">2</td>
              <td class="column2">In-demand employers</td>
              <td class="column3" align="right" valign="top"><a class="show" href="#">SHOW</a><a class="hide" href="#">HIDE</a></td>
            </tr>
            <tr class="accordionContent2 open" style="display: table-row; ">
            	<td></td>
              <td>
              	

<table width="100%" class="genericTable">
  <tbody><tr>
    <td align="left" valign="top">
			
					<table width="100%" role="presentation">
				
					<tbody><tr>
						<td valign="top">
              <table role="presentation">
                <tbody><tr>
                  <td valign="top">
                    <strong>
								      1.&nbsp; 
								      <a id="MainContent_Job_JobEmployerList_EmployerColumnOneRepeater_EmployerNameLinkButton_0" class="modalLink" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$JobEmployerList$EmployerColumnOneRepeater$ctl01$EmployerNameLinkButton", "", true, "", "", false, true))'>Hewlett-Packard</a>&nbsp; 
								      (3,660 openings)&nbsp; 
							      </strong>
                  </td>
                  <td valign="top">
                    
                  </td>
                </tr>
              </tbody></table>
						</td>
					</tr>
				
					<tr>
						<td valign="top">
              <table>
                <tbody><tr>
                  <td valign="top">
                    <strong>
								      2.&nbsp; 
								      <a id="MainContent_Job_JobEmployerList_EmployerColumnOneRepeater_EmployerNameLinkButton_1" class="modalLink" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$JobEmployerList$EmployerColumnOneRepeater$ctl02$EmployerNameLinkButton", "", true, "", "", false, true))'>IBM</a>&nbsp; 
								      (3,433)&nbsp; 
							      </strong>
                  </td>
                  <td valign="top">
                    
                  </td>
                </tr>
              </tbody></table>
						</td>
					</tr>
				
					<tr>
						<td valign="top">
              <table>
                <tbody><tr>
                  <td valign="top">
                    <strong>
								      3.&nbsp; 
								      <a id="MainContent_Job_JobEmployerList_EmployerColumnOneRepeater_EmployerNameLinkButton_2" class="modalLink" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$JobEmployerList$EmployerColumnOneRepeater$ctl03$EmployerNameLinkButton", "", true, "", "", false, true))'>Northrop Grumman</a>&nbsp; 
								      (3,091)&nbsp; 
							      </strong>
                  </td>
                  <td valign="top">
                    
                  </td>
                </tr>
              </tbody></table>
						</td>
					</tr>
				

					</tbody></table>
				
		</td>
		
    <td width="50%" align="left" valign="top">
			
					<table width="100%">
				
					<tbody><tr>
						<td valign="top">
							<strong>
								11.&nbsp; 
								<a id="MainContent_Job_JobEmployerList_EmployerColumnTwoRepeater_EmployerNameLinkButton_0" class="modalLink" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$JobEmployerList$EmployerColumnTwoRepeater$ctl01$EmployerNameLinkButton", "", true, "", "", false, true))'>Computer Sciences Corporation</a>&nbsp; 
								(1,748)
							</strong>
							
						</td>
					</tr>
				
					<tr>
						<td valign="top">
							<strong>
								12.&nbsp; 
								<a id="MainContent_Job_JobEmployerList_EmployerColumnTwoRepeater_EmployerNameLinkButton_1" class="modalLink" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$JobEmployerList$EmployerColumnTwoRepeater$ctl02$EmployerNameLinkButton", "", true, "", "", false, true))'>Dell</a>&nbsp; 
								(1,581)
							</strong>
							
						</td>
					</tr>
				
					<tr>
						<td valign="top">
							<strong>
								13.&nbsp; 
								<a id="MainContent_Job_JobEmployerList_EmployerColumnTwoRepeater_EmployerNameLinkButton_2" class="modalLink" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$JobEmployerList$EmployerColumnTwoRepeater$ctl03$EmployerNameLinkButton", "", true, "", "", false, true))'>UnitedHealth Group</a>&nbsp; 
								(1,551)
							</strong>
							
						</td>
					</tr>
				

					</tbody></table>
				
		</td>
		
  </tr>
</tbody></table>
              </td>
							<td></td>
            </tr>
  <tr class="multipleAccordionTitle on">
    <td class="column1">3</td>
    <td class="column2">Level of experience</td>
    <td class="column3" align="right" valign="top"><a class="show" href="#">SHOW</a><a class="hide" href="#">HIDE</a></td>
  </tr>
  <tr class="accordionContent open" >
    <td></td>
    <td colspan="2">
      <table>
        <tbody><tr>
          <td><strong>Experience levels of successful candidates</strong></td>
          <td></td>
        </tr>
        <tr>
          <td width="500px">
            <table class="horizontalBarChart">
              <tbody><tr>
                <td class="column1" width="155px;">High school graduate</td>
                <td class="column2" rowspan="5"></td>
                <td class="column3">
                  <span class="bar" style="width:2%"></span>
                  <span class="percent">2%</span>
                </td>
              </tr>
              <tr>
                <td class="column1">Low but Associate degree /<br> some college /<br> other post-high school</td>
                <td class="column3">
                  <span class="bar" style="width:9%"></span>
                  <span class="percent">9%</span>
                </td>
              </tr>
              <tr>
                <td class="column1">Bachelor's degree</td>
                <td class="column3">
                  <span class="bar" style="width:100%"></span>
                  <span class="percent">100%</span>
                </td>
              </tr>
              <tr>
                <td class="column1">Master's degree /<br> other post-college</td>
                <td class="column3">
                  <span class="bar" style="width:16%"></span>
                  <span class="percent">16%</span>
                </td>
              </tr>
              <tr class="last">
                <td class="column1">Doctoral degree</td>
                <td class="column3">
                  <span class="bar" style="width:5%"></span>
                  <span class="percent">5%</span>
                </td>
              </tr>
            </tbody></table>
          </td>
          <td valign="top">
          </td>
        </tr>
      </tbody></table>
    </td>
  </tr>
  <tr class="multipleAccordionTitle on">
    <td class="column1">4</td>
    <td class="column2">Common career paths</td>
    <td class="column3" align="right" valign="top"><a class="show" href="#">SHOW</a><a class="hide" href="#">HIDE</a></td>
  </tr>
  <tr class="accordionContent open ">
    <td></td>
    <td>
      <div id="MainContent_Job_JobCareerPathUpdatePanel">
		
										<table>
											<tbody><tr>
												<td width="50px"><strong>Show</strong></td>
												<td>
													<a id="MainContent_Job_CareerPathFromLinkButton" class="aspNetDisabled">Where people go from here</a>&nbsp; 
													<a id="MainContent_Job_CareerPathToLinkButton" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$CareerPathToLinkButton", "", true, "", "", false, true))'>How did other people get here</a>
												</td>
											</tr>
										</tbody></table>
										<table width="100%">
											<tbody><tr>
												<td align="center">
              						
													<table>
														<tbody><tr>
															<td>
														
																<div id="MainContent_Job_CareerPathPanel" class="careerPathChart">
			
																	<!-- First Row -->
																	<div id="MainContent_Job_CareerPathTopLevelPanel" class="careerpathChartItem top topArrowDown">
				
																		<table class="box">
                    									<tbody><tr>
                    										<td align="center" valign="middle"><span id="MainContent_Job_CareerPathJobNameLabel">Software Developer / Engineer</span></td>
                    									</tr>
																		</tbody></table>
																		<span class="arrowLineTop"></span>
																	
			</div>
																	<div class="clear"></div>
																	<!-- Second Row -->
																	<div id="MainContent_Job_CareerPathSecondLevelPanel" class="careerPathRowWrap">
				
																	  <div id="MainContent_Job_CareerPathSecondLevelLeftPanel" class="careerpathChartItem left">
					
																		  <span class="arrowLineTop"></span><span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                      <a id="MainContent_Job_CareerPathSecondLevelLeftJobLinkButton" class="occupation" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$CareerPathSecondLevelLeftJobLinkButton", "", true, "", "", false, true))'>i+</a>
																		  <table class="box">
																			  <tbody><tr>
																				  <td align="center" valign="middle"><a id="MainContent_Job_CareerPathSecondLevelLeftLinkButton" class="aspNetDisabled">Computer Programmer</a></td>
																			  </tr>
																		  </tbody></table>
																		  <span class="BottomArrow"></span>
																	  
				</div>
																	  <div id="MainContent_Job_CareerPathSecondLevelMiddleLeftPanel" class="careerpathChartItem">
					
																		  <span class="arrowLineTop"></span><span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                      <a id="MainContent_Job_CareerPathSecondLevelMiddleLeftJobLinkButton" class="occupation" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$CareerPathSecondLevelMiddleLeftJobLinkButton", "", true, "", "", false, true))'>i+</a>
																		  <table class="box">
																			  <tbody><tr>
																				  <td align="center" valign="middle"><a id="MainContent_Job_CareerPathSecondLevelMiddleLeftLinkButton"  title="Career Path Second Level Middle Left LinkButton" class="aspNetDisabled">Computer Systems Engineer / Architect</a></td>
																			  </tr>
																		  </tbody></table>
																		  <span class="BottomArrow"></span>
																	  
				</div>
																	  <div id="MainContent_Job_CareerPathSecondLevelMiddleRightPanel" class="careerpathChartItem">
					
																		  <span class="arrowLineTop"></span><span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                      <a id="MainContent_Job_CareerPathSecondLevelMiddleRightJobLinkButton" class="occupation" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$CareerPathSecondLevelMiddleRightJobLinkButton", "", true, "", "", false, true))'>i+</a>
																		  <table class="box">
																			  <tbody><tr>
																				  <td align="center" valign="middle"><a id="MainContent_Job_CareerPathSecondLevelMiddleRightLinkButton"  title="Career Path Second Level Middle Right LinkButton" class="aspNetDisabled">Web Developer</a></td>
																			  </tr>
																		  </tbody></table>
																		  <span class="BottomArrow"></span>
																	  
				</div>
																	  <div id="MainContent_Job_CareerPathSecondLevelRightPanel" class="careerpathChartItem right">
					
																		  <span class="arrowLineTop"></span><span class="arrowLineMiddle"></span><span class="arrowLineBottom"></span>
                                      <a id="MainContent_Job_CareerPathSecondLevelRightJobLinkButton" class="occupation" href='javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$MainContent$Job$CareerPathSecondLevelRightJobLinkButton", "", true, "", "", false, true))'>i+</a>
																		  <table class="box">
																			  <tbody><tr>
																				  <td align="center" valign="middle"><a id="MainContent_Job_CareerPathSecondLevelRightLinkButton" class="aspNetDisabled">Software QA Engineer / Tester</a></td>
																			  </tr>
																		  </tbody></table>
																		  <span class="BottomArrow"></span>
																	  
				</div>
                                  
			</div>
																	<!-- Third Row -->
																	<div id="MainContent_Job_CareerPathThirdLevelPanel" class="careerPathRowWrap">
				
																	  
																	  
																	  
																	  
																	
			</div>
	 															
		</div>

															</td>
														</tr>
													</tbody></table>											
												</td>
											</tr>
										</tbody></table>
									
	</div>
    </td>
    <td></td>
  </tr>
</tbody></table>
    </td>
  </tr>
  <tr>
    <td valign="top">Simple modal</td>
    <td>
      <div id="MainContent_TestModalPopup_ModalPanel" class="modal" style="width:305px">
	
        <div class="lightbox" id="jobDetailsLightBox">

	        <table width="100%">
            <tbody><tr>
              <td valign="top" align="right"><a href="#"><img src="<%= UrlBuilder.Close() %>" alt="Search for jobs" width="20" height="20"></a></td>
            </tr>
          </tbody></table>
	
          <table>
            <tbody><tr>
              <td>Your resume has been uploaded successfully.</td>
            </tr>
            <tr>
              <td align="right"><input type="submit" name="ctl00$MainContent$Button1" value="Continue" id="Submit5" class="buttonLevel2"></td>
            </tr>
          </tbody></table>


        </div>

      </div>
    </td>
  </tr>
  <tr>
    <td valign="top"></td>
    <td></td>
  </tr>
</table>

</asp:Content>
