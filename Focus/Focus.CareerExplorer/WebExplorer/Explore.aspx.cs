﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.CareerExplorer.WebAuth.Controls;
using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Explorer;

using Framework.Core;

#endregion

namespace  Focus.CareerExplorer.WebExplorer
{
  public partial class Explore : CareerExplorerPageBase, IExplorerJobModalPage
	{
		#region page properites

		public string ExploreSections
		{
			get
			{
				var sectionOrder = App.Settings.ExplorerSectionOrder.Split('|');
        return string.Concat("['", String.Join("','", sectionOrder), "']");
			}
		}

		protected ExplorerCriteria ReportCriteria
		{
			get { return GetViewStateValue("Explorer:ReportCriteria", new ExplorerCriteria()); }
			set { SetViewStateValue("Explorer:ReportCriteria", value); }
		}

		protected LightboxEventArgs SendEmailCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:SendEmailCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:SendEmailCompletedEventArgs", value); }
		}

		protected LightboxEventArgs BookmarkCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:BookmarkCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:BookmarkCompletedEventArgs", value); }
		}

		protected LightboxEventArgs PrintCompletedEventArgs
		{
			get { return GetViewStateValue<LightboxEventArgs>("Explorer:PrintCompletedEventArgs"); }
			set { SetViewStateValue("Explorer:PrintCompletedEventArgs", value); }
		}

    public ExplorerCriteria ReturnReportCriteria
    {
      get { return ReportCriteria; }
    }

		#endregion

    #region page declarations

	  private const string RightArrow = " »";

    #endregion

    #region page events

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
			RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education) && 
																	 (App.Settings.CareerExplorerFeatureEmphasis == CareerExplorerFeatureEmphasis.Career &&
                                    App.Settings.Module != FocusModules.Explorer);

			base.OnInit(e);

			Master.LogInComplete += Master_LogInComplete;
			Master.LogInCancelled += Master_LogInCancelled;
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (!IsPostBack)
			{
        if (App.GuideToResume())
          Response.Redirect(UrlBuilder.YourResume());

        LightboxEventArgs returnBookmark;

			  LocaliseUI();

        if (!App.User.IsAuthenticated && !App.User.IsAnonymous)
        {
          if (App.Settings.Theme == FocusThemes.Education 
          && (App.Settings.CareerExplorerFeatureEmphasis == CareerExplorerFeatureEmphasis.Explorer || App.Settings.Module == FocusModules.Explorer))
          {
            var login = (LogInOrRegister) Master.FindControl("MainLogIn");

            login.ShowLogIn();
          }
        }

        if ((returnBookmark = App.GetSessionValue<LightboxEventArgs>("Explorer:OpenBookmark")) != null)
        {
          ReportCriteria = returnBookmark.CommandArgument.MainPageCriteria;
          switch (returnBookmark.CommandName)
          {
            case "Job":
              Job.Show(returnBookmark.CommandArgument.Id, returnBookmark.CommandArgument.CriteriaHolder, returnBookmark.CommandArgument.Breadcrumbs);
              break;
          }
          App.RemoveSessionValue("Explorer:OpenBookmark");
        }
        else if (Page.RouteData.Values.ContainsKey("option"))
			  {
			    string option = Page.RouteData.Values["option"].ToString();
			    if (!String.IsNullOrEmpty(option))
			    {
			      if (option == "bookmark")
			      {
			        #region bookmark from email

			        if (Page.RouteData.Values.ContainsKey("bookmarkId"))
			        {
			          long bookmarkId;
			          if (long.TryParse(Page.RouteData.Values["bookmarkId"].ToString(), out bookmarkId))
			          {
                  var bookmark = ServiceClientLocator.AnnotationClient(App).GetBookmark(bookmarkId);
                  Page.Title = Page.Title + "-" + bookmarkId;
			            if (bookmark != null)
			            {
			              if (bookmark.Type == BookmarkTypes.ReportItem || bookmark.Type == BookmarkTypes.EmailedReportItem)
			              {
			                var criteria =
			                  ReportCriteria = (ExplorerCriteria) bookmark.Criteria.DeserializeJson(typeof (ExplorerCriteria));
			                if (criteria != null && criteria.ReportItemId.HasValue)
			                {
			                  switch (criteria.ReportType)
			                  {
			                    case ReportTypes.Job:
			                      Job.Show(criteria.ReportItemId.Value, criteria, null);

			                      break;

			                    case ReportTypes.Employer:
			                      Employer.Show(criteria.ReportItemId.Value, criteria, null);

			                      break;

			                    case ReportTypes.DegreeCertification:
			                      DegreeCertification.Show(criteria.ReportItemId.Value, criteria, null);

			                      break;

                          case ReportTypes.ProgramAreaDegree:
                            ProgramAreaDegreeStudy.Show(criteria.ReportItemId.GetValueOrDefault(0), criteria, null);
			                      break;

			                    case ReportTypes.Skill:
			                      Skill.Show(criteria.ReportItemId.Value, criteria, null);

			                      break;

			                    case ReportTypes.Internship:
			                      Internship.Show(criteria.ReportItemId.Value, criteria, null);
			                      break;

                          case ReportTypes.CareerArea:
                            CareerArea.Show(criteria.ReportItemId.Value, criteria, null);
                            break;
			                  }
			                }
			              }
			            }
			          }
			        }

			        #endregion
			      }
			      else
			      {
			        #region from home page

			        SelectedExploreSection.Value = option;

			        #endregion
			      }
			    }
			  }

			  BindControls();

        if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
        {
          DetailedDegreeDropDownList.Visible = true;
          ResearchStudyAutoCompleteTextBox.Visible = false;
          BindDetailedDegreeDropDownList();
        }
			}
      else
			{
				if (!ResearchStudyByDegreeRadioButton.Checked && !ResearchStudyByDeptRadioButton.Checked)
				{
					ResearchStudyByDegreeRadioButton.Checked = App.Settings.Theme == FocusThemes.Education ? !App.Settings.ShowProgramArea : true;
					ResearchStudyByDeptRadioButton.Checked = App.Settings.Theme == FocusThemes.Education ? App.Settings.ShowProgramArea : false;
				}

        // need to rebind the area drop down as we are not using ajaxtoolkit cascading drop downs
        if (!String.IsNullOrEmpty(ResearchJobTitleAutoCompleteTextBox.SelectedValue))
        {
          // Job Titles and Jobs
          ResearchJobTitleAutoCompleteRelatedJobs.DataSource =
            ServiceClientLocator.ExplorerClient(App).GetRelatedJobs(ResearchJobTitleAutoCompleteTextBox.SelectedValueToNullableLong.GetValueOrDefault(0));
          ResearchJobTitleAutoCompleteRelatedJobs.DataTextField = "Name";
          ResearchJobTitleAutoCompleteRelatedJobs.DataValueField = "Id";
          ResearchJobTitleAutoCompleteRelatedJobs.DataBind();
          ResearchJobTitleAutoCompleteRelatedJobs.Items.AddLocalisedTopDefault("Global.Job.TopDefault", "- select job -");

          var selectedJob = Request.Form[ResearchJobTitleAutoCompleteRelatedJobs.UniqueID];
          if (!String.IsNullOrEmpty(selectedJob))
          {
            ResearchJobTitleAutoCompleteRelatedJobs.SelectedValue = selectedJob;
          }

          ResearchJobTitleAutoCompleteTextBoxJobRow.Style[HtmlTextWriterStyle.Display] = "block";
        }

        // need to rebind the degree drop down as we are not using ajaxtoolkit cascading drop downs
        if (!String.IsNullOrEmpty(StudyProgramAreaDropDownList.SelectedValue))
        {
          StudyProgramAreaDegreesDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(StudyProgramAreaDropDownList.SelectedValueToLong());
          StudyProgramAreaDegreesDropDownList.DataTextField = "DegreeEducationLevelName";
          StudyProgramAreaDegreesDropDownList.DataValueField = "DegreeEducationLevelId";
          StudyProgramAreaDegreesDropDownList.DataBind();
          StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.AllDegrees", "All Degrees", string.Concat("-", StudyProgramAreaDropDownList.SelectedValue));
          StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

          var selectedDegree = Request.Form[StudyProgramAreaDegreesDropDownList.UniqueID];
          if (selectedDegree.IsNotNullOrEmpty())
          {
            StudyProgramAreaDegreesDropDownList.SelectedValue = selectedDegree;
          }
        }
			}

      if (!String.IsNullOrEmpty(DetailedDegreeLevelDropDownList.SelectedValue))
      {
        if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
          BindDetailedDegreeDropDownList();
      }

		  if (ResearchStudyByDegreeRadioButton.Checked || !App.Settings.ShowProgramArea)
		  {
        StudyProgramAreaLists.Style.Add(HtmlTextWriterStyle.Display, "none");
        StudyDegreeLists.Style.Add(HtmlTextWriterStyle.Display, "block");
		  }
		  else
      {
        StudyProgramAreaLists.Style.Add(HtmlTextWriterStyle.Display, "block");
        StudyDegreeLists.Style.Add(HtmlTextWriterStyle.Display, "none");
      }

		  string js;
			if (App.Settings.ExplorerGoogleEventTrackingEnabled)
			{
				js =
					String.Format(
            @"
	<script src=""/Assets/Scripts/googleTrack.js"" type=""text/javascript""></script>
	<script type=""text/javascript"">
		$(document).ready(function ()
		{{
			googlePageTrackerUrl = '/explore';
			AddGooglePageEvents('explore');

			AddGoogleChangeEvent('{0}', '', 'Explore', 'ExploreSelectState');
			AddGoogleChangeEvent('{1}', '', 'Explore', 'ExploreSelectArea');
			AddGoogleClickEvent('{2}', '', 'Explore', 'ExploreGo');

			AddGoogleChangeEvent('{3}', '', 'Explore', 'ResearchSelectState');
			AddGoogleChangeEvent('{4}', '', 'Explore', 'ResearchSelectArea');
			AddGoogleClickEvent('{5}', '', 'Explore', 'ResearchNext');

			$('#{6}').click(function ()
			{{
				var researchOption = GetResearchOption();
				if (researchOption.length > 0)
				{{
					var researchOptionParts = researchOption.split('|');
					if (researchOptionParts.length > 0)
					{{
						GoogleTrackEvent(googlePageTrackerUrl, 'Explore', researchOptionParts[0], researchOptionParts[1]);
						GoogleTrackEvent(googlePageTrackerUrl, 'Explore', 'ResearchGo', '');
					}}
				}}
			}});

			AddGoogleChangeEvent('{7}', '', 'Explore', 'StudySelectState');
			AddGoogleChangeEvent('{8}', '', 'Explore', 'StudySelectArea');
			AddGoogleClickEvent('{9}', '', 'Explore', 'StudyNext');

			$('#{10}').click(function ()
			{{	
				var studyOption = GetStudyOption();
				if (studyOption.length > 0)
				{{
					GoogleTrackEvent(googlePageTrackerUrl, 'Explore', studyOption, '');
					GoogleTrackEvent(googlePageTrackerUrl, 'Explore', 'StudyGo', '');					
				}}
			}});

      AddGoogleClickEvent('{11}', '', 'Explore', 'SchoolGo');
		}});
	</script>",
						ExploreStateAreaSelector.StateDropDownListClientID, ExploreStateAreaSelector.AreaDropDownListClientID,
						ExploreGoButton.ClientID, 
						ResearchStateAreaSelector.StateDropDownListClientID, ResearchStateAreaSelector.AreaDropDownListClientID, 
						ResearchNextButton.ClientID, ResearchGoButton.ClientID, 
						StudyStateAreaSelector.StateDropDownListClientID, StudyStateAreaSelector.AreaDropDownListClientID,
						StudyNextButton.ClientID, StudyGoButton.ClientID,
            SchoolGoButton.ClientID);

				GoogleTrackingLiteral.Text = js;
			}

			#region Register Client Script

			#region reset research option

			js =
				String.Format(
          @"
function ResetResearchOption(type, checked)
{{
  $('.ResearchValidator').hide();
  if (checked)
  {{
    if (type != 'Job')
    {{
			$('#{0}').val('').blur().prop('disabled', 'disabled').prev().css('opacity','0.4');
		  $('#{1}').val('');
		  DisableStyledDropDown('#{15}', true);
      $('#{16}').hide();
    }}
		else
		{{
			$('#{0}').prop('disabled', false).prev().css('opacity','1');
			EnableStyledDropDown('#{15}');
		}}

	  if (type != 'Employer')
	  {{
		  $('#{2}').val('').blur().prop('disabled', 'disabled').prev().css('opacity','0.4');
		  $('#{3}').val('').blur().prop('disabled', 'disabled');
    }}
		else
		{{
			$('#{2}').prop('disabled', false).prev().css('opacity','1');
		  $('#{3}').prop('disabled', false);
		}}

    if (type != 'Study')
    {{
		  $('#{4}').val('').blur().prop('disabled','disabled').prev().css('opacity','0.4');
		  $('#{5}').val('').prop('disabled','disabled');

			DisableStyledDropDown('#{10}', true);
			DisableStyledDropDown('#{19}', true);
      DisableStyledDropDown('#{11}', true);
      DisableStyledDropDown('#{17}', true);
      $('#{18} input[type=""radio""]').prop('disabled', 'disabled');
    }}
		else
		{{
			$('#{4}').prop('disabled',false).prev().css('opacity','1');
		  $('#{5}').prop('disabled',false);

			EnableStyledDropDown('#{10}');
			EnableStyledDropDown('#{19}');
			EnableStyledDropDown('#{11}');
      EnableStyledDropDown('#{17}');
      $('#{18} input[type=""radio""]').prop('disabled', false);
		}}

    if (type != 'Skill')
    {{
		  $('#{13}').val('').blur().prop('disabled','disabled').prev().css('opacity','0.4');
		  $('#{14}').val('').prop('disabled','disabled');
    }}
		else
		{{
			$('#{13}').prop('disabled',false).prev().css('opacity','1');
		  $('#{14}').prop('disabled',false);
		}}
  }}
}}

$(document).ready(function () {{
	$('#{6}').click(function () {{
		ResetResearchOption('Job',$(this).prop('checked'));
	}});

	$('#{7}').click(function () {{
		ResetResearchOption('Employer',$(this).prop('checked'));
	}});

	$('#{8}').click(function () {{
		ResetResearchOption('Study',$(this).prop('checked'));
	}});

	$('#{9}').click(function () {{
		ResetResearchOption('Career',$(this).prop('checked'));
	}});

	$('#{12}').click(function () {{
		ResetResearchOption('Skill',$(this).prop('checked'));
	}});
}});",
          ResearchJobTitleAutoCompleteTextBox.ClientID,
          ResearchJobTitleAutoCompleteTextBox.SelectedValueClientId,
					ResearchEmployerAutoCompleteTextBox.ClientID,
					ResearchEmployerAutoCompleteTextBox.SelectedValueClientId,
					ResearchStudyAutoCompleteTextBox.ClientID,
					ResearchStudyAutoCompleteTextBox.SelectedValueClientId,
					ResearchJobRadioButton.ClientID,
					ResearchEmployerRadioButton.ClientID,
					ResearchStudyRadioButton.ClientID,
          ResearchCareerRadioButton.ClientID,
          DetailedDegreeLevelDropDownList.ClientID,
          StudyProgramAreaDropDownList.ClientID,
          ResearchSkillRadioButton.ClientID,
          ResearchSkillAutoCompleteTextBox.ClientID,
          ResearchSkillAutoCompleteTextBox.SelectedValueClientId,
          ResearchJobTitleAutoCompleteRelatedJobs.ClientID,
          ResearchJobTitleAutoCompleteTextBoxJobRow.ClientID,
					StudyProgramAreaDegreesDropDownList.ClientID,
          ResearchStudyTypeButtons.ClientID,
          DetailedDegreeDropDownList.ClientID);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "ResetResearchOption", js, true);

		  if (Page.IsPostBack)
		  {
		    js =
		      String.Format(@"
$(document).ready(function () {{
  if ($('#{0}').is(':checked')) {{
    ResetResearchOption('Job', true);
  }}

  if ($('#{1}').is(':checked')) {{
    ResetResearchOption('Employer', true);
  }}

  if ($('#{2}').is(':checked')) {{
    ResetResearchOption('Study', true);
  }}

  if ($('#{3}').is(':checked')) {{
    ResetResearchOption('Career', true);
  }}

  if ($('#{4}').is(':checked')) {{
    ResetResearchOption('Skill', true);
  }}
}});",
		        ResearchJobRadioButton.ClientID,
		        ResearchEmployerRadioButton.ClientID,
		        ResearchStudyRadioButton.ClientID,
		        ResearchCareerRadioButton.ClientID,
		        ResearchSkillRadioButton.ClientID);

		    Page.ClientScript.RegisterClientScriptBlock(GetType(), "ResetResearchOptionPostBack", js, true);
		  }

		  //ResearchJobTitleAutoCompleteTextBox.Attributes.Add("onfocus", String.Format("javascript:$('#{0}').click();", ResearchJobRadioButton.ClientID));
      //ResearchSkillAutoCompleteTextBox.Attributes.Add("onfocus", String.Format("javascript:$('#{0}').click();", ResearchSkillRadioButton.ClientID));

			#endregion

			#region get research option

			js =
				String.Format(
          @"
function GetResearchOption()
{{
	var researchOption = '';
	if (Page_ClientValidate('ResearchNext') && Page_ClientValidate('ResearchGroup'))
	{{
		if ($('#{0}').prop('checked'))
		{{
			researchOption = 'ResearchCareer|';
		}}
		if ($('#{1}').prop('checked'))
		{{
			researchOption = 'ResearchJob|' + $('#{2}').val();
		}}
		if ($('#{3}').prop('checked'))
		{{
			researchOption = 'ResearchEmployer|' + $('#{4}').val();
		}}
		if ($('#{5}').prop('checked'))
		{{
			researchOption = 'ResearchStudy|' + $('#{6}').val();
		}}
		if ($('#{7}').prop('checked'))
		{{
			researchOption = 'ResearchStudy|' + $('#{8}').val();
		}}
	}}

	return researchOption;
}}",
					ResearchCareerRadioButton.ClientID,
          ResearchJobRadioButton.ClientID, ResearchJobTitleAutoCompleteRelatedJobs.ClientID, 
					ResearchEmployerRadioButton.ClientID, ResearchEmployerAutoCompleteTextBox.ClientID, 
					ResearchStudyRadioButton.ClientID, ResearchStudyAutoCompleteTextBox.ClientID,
          ResearchSkillRadioButton.ClientID, ResearchSkillAutoCompleteTextBox.ClientID
          );

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "GetResearchOption", js, true);

			#endregion

      #region validate study option

			js = @"
function ValidateStudyOption	() {
	if (!Page_ClientValidate('StudyNext')) {
		return false;
	}

	return true;
};";

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "ValidateStudyOption", js, true);

			#endregion

			#region get study option

			js =
				String.Format(
					@"
function GetStudyOption()
{{
	var studyOption = '';
	if (ValidateStudyOption())
	{{
		if ($('#{0}').prop('checked'))
		{{
			studyOption = 'StudyDegreeCertificate';
		}}
		if ($('#{1}').prop('checked'))
		{{
			studyOption = 'StudySkills';
		}}
	}}

	return studyOption;
}}",
					StudyDegreeRadioButton.ClientID, StudySkillRadioButton.ClientID);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "GetStudyOption", js, true);

		  js = String.Format(
        @"
$(document).ready(function() 
{{
  $('#{0}').click(function()
  {{
    ResetResearchOption('Study',$(this).prop('checked'));

    $('#{1}').show();
    $('#{3}').hide();

		$('#{5}').val('');
		$('#{6}').val('');
		$('#{5}').blur();
    $('#{7}').prop('selectedIndex', 0);
    $('#{7}').change();

    $('#{4}').click();
  }});

  $('#{2}').click(function()
  {{
    ResetResearchOption('Study',$(this).prop('checked'));

    $('#{1}').hide();
    $('#{3}').show();

    $('#{8}').prop('selectedIndex', 0);
    $('#{8}').change();

    $('#{4}').click();
  }});
}});",
        ResearchStudyByDeptRadioButton.ClientID,
        StudyProgramAreaLists.ClientID,
        ResearchStudyByDegreeRadioButton.ClientID, 
        StudyDegreeLists.ClientID,
        ResearchStudyRadioButton.ClientID,
        ResearchStudyAutoCompleteTextBox.ClientID,
        ResearchStudyAutoCompleteTextBox.SelectedValueClientId,
        DetailedDegreeLevelDropDownList.ClientID,
        StudyProgramAreaDropDownList.ClientID);

      Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "StudyTypeRadioButtonClicks", js, true);

      js = String.Format(
        @"
$(document).ready(function() {{
	$('#{0}').change(function () {{
		var selectedIndex = ($(this).prop('selectedIndex') > 0) ? 1 : -1;

		LoadCascadingDropDownList($(this).val(),'{1}','{2}','{3}','{4}','{5}', selectedIndex);
	}});
}});",
        StudyProgramAreaDropDownList.ClientID, StudyProgramAreaDegreesDropDownList.ClientID, String.Concat(UrlBuilder.AjaxService(), "/GetDegreeEducationLevelsByProgramArea"), "Degrees",
        CodeLocalise("Global.Area.TopDefault", "- select degree -"),
        CodeLocalise("LoadingAreas.Text", "[Loading degrees...]"));

      Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "StudyProgramAreaDropDownListOnChange", js, true);


      if (App.Settings.Theme == FocusThemes.Education && !App.Settings.ShowProgramArea)
      {
        js = String.Format(
          @"
$(document).ready(function() {{
	$('#{0}').change(function () {{
		var selectedIndex = ($(this).prop('selectedIndex') > 0) ? 1 : -1;

		LoadCascadingDropDownList($(this).val(),'{1}','{2}','{3}','{4}','{5}', selectedIndex);
	}});
}});",
          DetailedDegreeLevelDropDownList.ClientID, DetailedDegreeDropDownList.ClientID,
          String.Concat(UrlBuilder.AjaxService(), "/GetDegreeEducationLevelsByDegreeLevel"), "Degrees",
          "",
          CodeLocalise("LoadingAreas.Text", "[Loading degrees...]"));

        Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "DetailedDegreeLevelDropDownListOnChange", js, true);
      }
      else
      {
        js = String.Format(
          @"
$(document).ready(function() {{
	$('#{0}').change(function () {{
	  $('#{1}').val('').blur();
	  $('#{2}').val('');
	}});
}});",
          DetailedDegreeLevelDropDownList.ClientID,					
          ResearchStudyAutoCompleteTextBox.ClientID,
					ResearchStudyAutoCompleteTextBox.SelectedValueClientId);

        Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "DetailedDegreeLevelDropDownListOnChange", js, true);        
      }

			#endregion

      #region set selected section

      js = String.Format(
        @"
$(document).ready(function() {{
    document.title = 'Explorer - Explorer';
	$('#exploreSectionTitle').click(function ()
	{{
		$('#{0}').val('explore');
		googlePageTrackerUrl = '/explore/explore';
        document.title = 'Explorer - Explore';
	}});
	$('#researchSectionTitle').click(function ()
	{{
		$('#{0}').val('research');
		googlePageTrackerUrl = '/explore/research';
        document.title = 'Explorer - Research';
	}});
	$('#studySectionTitle').click(function ()
	{{
		$('#{0}').val('study');
		googlePageTrackerUrl = '/explore/study';
        document.title = 'Explorer - Study';
	}});
	$('#experienceSectionTitle').click(function ()
	{{
		$('#{0}').val('experience');
		googlePageTrackerUrl = '/explore/experience';
        document.title = 'Explorer - Experience';
	}});
	$('#schoolSectionTitle').click(function ()
	{{
		$('#{0}').val('school');
		googlePageTrackerUrl = '/explore/school';
        document.title = 'Explorer - School';
	}});
}});",
				SelectedExploreSection.ClientID);

			Page.ClientScript.RegisterStartupScript(GetType(), "setSelectedSection", js, true);

			#endregion

			#region show selected section

			if (!String.IsNullOrEmpty(SelectedExploreSection.Value))
			{
				js = String.Format(
					@"
$(document).ready(function() {{
	$('#{0}SectionTitle').click();
}});", SelectedExploreSection.Value);

				Page.ClientScript.RegisterStartupScript(GetType(), "showSelectedSection", js, true);				
			}

			#endregion

			#endregion
		}

		protected override void OnLoadComplete(EventArgs e)
		{
			base.OnLoadComplete(e);

			// We need to log the activity only after the page load has completed in case the user is redirected elsewhere or prohibited from viewing the ocntent for some reason.
      // But note that the method throws an error if the user is not authenticated
      if (App.User.UserId > 0)
			  ServiceClientLocator.CoreClient(App).SaveSelfService(ActionTypes.ViewCareerExplorationTools, App.User.UserId);
		}

		protected void SearchGoButton_Click(object sender, EventArgs e)
		{
			string url = UrlBuilder.Search(HttpUtility.UrlEncode(SearchTermTextBox.Text).Replace("+", "%20"));
			Response.Redirect(url);
		}

		protected void ExploreGoButton_Click(object sender, CommandEventArgs e)
		{
		  var url = string.Empty;

      switch (e.CommandArgument.ToString())
		  {
        case "Explore":
          var exploreStateId = ExploreStateAreaSelector.AreaSelectedValue.GetValueOrDefault(0);
          url = UrlBuilder.Report(ReportTypes.Job, exploreStateId);
          break;

        case "Experience":
          var experienceStateId = ExperienceStateAreaSelector.AreaSelectedValue.GetValueOrDefault(0);
          var experienceOccupationType = ExperienceJobTitleSelector.JobTitleSelected
		                                       ? CareerAreaOccupationTypes.Job
		                                       : CareerAreaOccupationTypes.MOS;
          url = ExperienceJobTitleSelector.RelatedJobValue.HasValue
                  ? UrlBuilder.Report(ReportTypes.CareerMoves, experienceStateId, experienceOccupationType, ExperienceJobTitleSelector.OccupationTitleValue.ToString(), ExperienceJobTitleSelector.RelatedJobValue.ToString())
                  : UrlBuilder.Report(ReportTypes.CareerMoves, experienceStateId, experienceOccupationType, ExperienceJobTitleSelector.OccupationTitleValue.ToString());
		      break;

        case "School":
          var schoolStateId = SchoolStateAreaSelector.AreaSelectedValue.GetValueOrDefault(0);
          url = UrlBuilder.Report(ReportTypes.DegreeCertification, schoolStateId);
          break;
		  }

      Response.Redirect(url);
		}

		protected void ResearchGoButton_Click(object sender, EventArgs e)
		{
			ResearchNextButton.Style.Add(HtmlTextWriterStyle.Display, "none");
			ResearchStep2Panel.Style.Add(HtmlTextWriterStyle.Display, "block");

			if (ResearchCareerRadioButton.Checked)
			{
				string url = UrlBuilder.Report(ReportTypes.Job, ResearchStateAreaSelector.AreaSelectedValue.Value);
				Response.Redirect(url);				
			}
			else
			{
				var criteria = new ExplorerCriteria()
			               		{
													StateAreaId = ResearchStateAreaSelector.AreaSelectedValue.Value,
                          StateArea = ServiceClientLocator.ExplorerClient(App).GetStateArea(ResearchStateAreaSelector.AreaSelectedValue.Value).StateAreaName
			               		};
				ReportCriteria = criteria;

        if (ResearchJobRadioButton.Checked && ResearchJobTitleAutoCompleteRelatedJobs.SelectedValueToNullableLong().HasValue)
				{
					criteria.ReportType = ReportTypes.Job;

          Job.Show(ResearchJobTitleAutoCompleteRelatedJobs.SelectedValueToNullableLong().GetValueOrDefault(0), criteria, null);
				}                                               
				else if (ResearchEmployerRadioButton.Checked && ResearchEmployerAutoCompleteTextBox.SelectedValueToNullableLong.HasValue)
				{
					criteria.ReportType = ReportTypes.Employer;

					Employer.Show(ResearchEmployerAutoCompleteTextBox.SelectedValueToNullableLong.Value, criteria, null);
				}
				else if (ResearchStudyRadioButton.Checked)
				{
				  if (ResearchStudyByDegreeRadioButton.Checked && ResearchStudyAutoCompleteTextBox.SelectedValueToNullableLong.HasValue)
				  {
				    criteria.ReportType = ReportTypes.DegreeCertification;

				    DegreeCertification.Show(ResearchStudyAutoCompleteTextBox.SelectedValueToNullableLong.Value, criteria, null);
				  }
          else if (ResearchStudyByDegreeRadioButton.Checked && DetailedDegreeDropDownList.SelectedValueToNullableLong().HasValue)
          {
            criteria.ReportType = ReportTypes.DegreeCertification;

            DegreeCertification.Show(DetailedDegreeDropDownList.SelectedValueToNullableLong().GetValueOrDefault(0), criteria, null);
          }
          else if (ResearchStudyByDeptRadioButton.Checked && StudyProgramAreaDegreesDropDownList.SelectedValue.IsNotNullOrEmpty())
				  {
            if (StudyProgramAreaDegreesDropDownList.SelectedValueToLong() > 0)
            {
              criteria.ReportType = ReportTypes.DegreeCertification;

              DegreeCertification.Show(StudyProgramAreaDegreesDropDownList.SelectedValueToLong(), criteria, null);
            }
            else
            {
              criteria.ReportType = ReportTypes.ProgramAreaDegree;

              ProgramAreaDegreeStudy.Show(StudyProgramAreaDropDownList.SelectedValueToLong(), criteria, null);
            }
				  }
				}
        else if (ResearchSkillRadioButton.Checked && ResearchSkillAutoCompleteTextBox.SelectedValueToNullableLong.HasValue)
        {
          criteria.ReportType = ReportTypes.Skill;

          Skill.Show(ResearchSkillAutoCompleteTextBox.SelectedValueToNullableLong.Value, criteria, null);
        }  
			}
		}

    /// <summary>
    /// Fires when the 'Go' button is clicked in the 'See what I can study' section
    /// </summary>
    /// <param name="sender">Button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
		protected void StudyGoButton_Click(object sender, EventArgs e)
		{
		  var stateId = StudyStateAreaSelector.AreaSelectedValue.GetValueOrDefault(0);
      var url = UrlBuilder.Report(StudyDegreeRadioButton.Checked ? ReportTypes.DegreeCertification : ReportTypes.Skill, stateId);

		  Response.Redirect(url);
		}

    /// <summary>
    /// Handles the Click event of the ExploreStep2GoButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ExploreStep2GoButton_Click(object sender, EventArgs e)
    {
      var stateId = ExploreStateAreaSelector.AreaSelectedValue.GetValueOrDefault(0);

      if (ExploreAllJobsRadioButton.Checked)
        Response.Redirect(UrlBuilder.Report(ReportTypes.Job, stateId));
      else if (ExploreByCareerAreaRadionButton.Checked)
      {
        Response.Redirect(CareerAreaDropDownList.SelectedValue.IsNullOrEmpty()
                            ? UrlBuilder.Report(ReportTypes.Job, stateId)
                            : UrlBuilder.Report(ReportTypes.Job, stateId, CareerAreaDropDownList.SelectedValueToLong()));
      }
      else if (ExploreInternshipsRadioButton.Checked)
        Response.Redirect(UrlBuilder.Report(ReportTypes.Internship, stateId));
    }

		#endregion

		#region modal events

		#region send email, bookmark and print

		#region send email

		/// <summary>
		/// Handles the SendEmailClicked event of the Modal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
		protected void Modal_SendEmailClicked(object sender, LightboxEventArgs e)
		{
			SendEmailCompletedEventArgs = e;

			if (!ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				UnAuthorizedModal.Show();
			}
			else
			{
				var args = e.CommandArgument;

				switch (e.CommandName)
				{
					case "SendJobEmail":
					case "SendEmployerEmail":
					case "SendDegreeCertificationEmail":
          case "SendProgramAreaDegreeStudyEmail":
					case "SendSkillEmail":
          case "SendInternshipEmail":
          case "SendCareerAreaEmail":
            Email.Show(e.CommandArgument.CriteriaHolder, args.Id, args.LightboxName, args.LightboxLocation);
						break;
				}
			} 
		}

    /// <summary>
    /// Handles the Cancelled event of the Email control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Email_Cancelled(object sender, EventArgs e)
		{
			SendEmailCompleted();
		}

    /// <summary>
    /// Handles the Completed event of the Email control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Email_Completed(object sender, EventArgs e)
		{
			Confirmation.Show(CodeLocalise("EmailConfirmation.Title", "Email Confirmation"),
												CodeLocalise("EmailConfirmation.Body", "Your email has been sent"),
												CodeLocalise("Global.Close.Text", "Close"), "EmailConfirmationClose", height: 50, width: 250);
		}

    /// <summary>
    /// Sends the email completed.
    /// </summary>
		private void SendEmailCompleted()
		{
			var args = SendEmailCompletedEventArgs;

			switch (args.CommandName)
			{
				case "SendJobEmail":
					Job.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

				case "SendEmployerEmail":
          Employer.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

				case "SendDegreeCertificationEmail":
          DegreeCertification.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "SendProgramAreaDegreeStudyEmail":
          ProgramAreaDegreeStudy.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "SendSkillEmail":
          Skill.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "SendInternshipEmail":
          Internship.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "SendCareerAreaEmail":
          CareerArea.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;
      }
		}

		#endregion

		#region bookmark

    /// <summary>
    /// Handles the BookmarkClicked event of the Modal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
		protected void Modal_BookmarkClicked(object sender, LightboxEventArgs e)
		{
			BookmarkCompletedEventArgs = e;

			if (!ServiceClientLocator.ExplorerClient(App).UserLoggedIn(Request.IsAuthenticated))
			{
				UnAuthorizedModal.Show();
			}
			else
			{
				var name = String.Format("{0} {1}", e.CommandArgument.LightboxName, e.CommandArgument.LightboxLocation);
				var criteria = e.CommandArgument.CriteriaHolder;
				criteria.ReportItemId = e.CommandArgument.Id;

				CreateBookmark(name, BookmarkTypes.ReportItem, criteria);
			}
		}

    /// <summary>
    /// Bookmark completed.
    /// </summary>
		private void BookmarkCompleted()
		{
			var args = BookmarkCompletedEventArgs;
      // If we don't have any crtieria by which to a new modal just show the current page
      if (ReportCriteria.StateAreaId == 0) return;
			switch (args.CommandName)
			{
				case "BookmarkJob":
					Job.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

				case "BookmarkEmployer":
          Employer.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

				case "BookmarkDegreeCertification":
          DegreeCertification.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "BookmarkProgramAreaDegreeStudy":
          ProgramAreaDegreeStudy.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "BookmarkSkill":
          Skill.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "BookmarkInternship":
          Internship.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;

        case "BookmarkCareerArea":
          CareerArea.Show(args.CommandArgument.Id, args.CommandArgument.CriteriaHolder, args.CommandArgument.Breadcrumbs);
          break;
			}
		}

		#endregion

		#region print

    /// <summary>
    /// Handles the PrintClicked event of the Modal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Focus.Common.Models.LightboxEventArgs"/> instance containing the event data.</param>
		protected void Modal_PrintClicked(object sender, LightboxEventArgs e)
		{
			PrintCompletedEventArgs = e;

			UnAuthorizedModal.Show();
		}

    /// <summary>
    /// Prints the completed.
    /// </summary>
    /// <param name="printFormat">The format to print the modal</param>
    private void PrintCompleted(ModalPrintFormat printFormat)
		{
			var args = PrintCompletedEventArgs;
      var criteria = args.CommandArgument.CriteriaHolder;
			switch (args.CommandName)
			{
        case "PrintJob":
          Job.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

        case "PrintEmployer":
          Employer.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintDegreeCertification":
          DegreeCertification.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

        case "PrintProgramAreaDegreeStudy":
          ProgramAreaDegreeStudy.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

				case "PrintSkill":
          Skill.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

        case "PrintInternship":
          Internship.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;

        case "PrintCareerArea":
          CareerArea.Show(args.CommandArgument.Id, criteria, args.CommandArgument.Breadcrumbs, printFormat);
          break;
			}
		}

		#endregion

		#region login/register events

		/// <summary>
		/// Handles the LogInComplete event of the Master control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected bool Master_LogInComplete(object sender, CommandEventArgs e)
		{
		  var redirectionOverride = false;

			if (!String.IsNullOrEmpty(e.CommandName))
			{
				switch (e.CommandName)
				{
					case "BookmarkJob":
					case "BookmarkEmployer":
					case "BookmarkDegreeCertification":
          case "BookmarkProgramAreaDegreeStudy":
          case "BookmarkSkill":
          case "BookmarkInternship":
          case "BookmarkCareerArea":
            var args = BookmarkCompletedEventArgs;

						var name = String.Format("{0} {1}", args.CommandArgument.LightboxName, args.CommandArgument.LightboxLocation);
            var criteria = args.CommandArgument.CriteriaHolder ?? ReportCriteria;
						criteria.ReportItemId = args.CommandArgument.Id;

						CreateBookmark(name, BookmarkTypes.ReportItem, criteria);
				    redirectionOverride = true;
						break;

					case "SendJobEmail":
					case "SendEmployerEmail":
					case "SendDegreeCertificationEmail":
          case "SendProgramAreaDegreeStudyEmail":
          case "SendSkillEmail":
          case "SendInternshipEmail":
          case "SendCareerAreaEmail":
            var sendEmailArgs = SendEmailCompletedEventArgs;
            var emailcriteria = sendEmailArgs.CommandArgument.CriteriaHolder ?? ReportCriteria;

            Email.Show(emailcriteria, sendEmailArgs.CommandArgument.Id, sendEmailArgs.CommandArgument.LightboxName, sendEmailArgs.CommandArgument.LightboxLocation);
            redirectionOverride = true;
						break;

					case "PrintJob":
					case "PrintEmployer":
          case "PrintDegreeCertification":
          case "PrintProgramAreaDegreeStudy":
          case "PrintSkill":
          case "PrintInternship":
          case "PrintCareerArea": 
            PrintCompleted(PrintCompletedEventArgs.CommandArgument.PrintFormat);
            redirectionOverride = true;
						break;
        }
			}

		  return redirectionOverride;
		}

    /// <summary>
    /// Handles the LogInCancelled event of the Master control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Master_LogInCancelled(object sender, CommandEventArgs e)
		{
			if (!String.IsNullOrEmpty(e.CommandName))
			{
				switch (e.CommandName)
				{
					case "BookmarkJob":
					case "BookmarkEmployer":
					case "BookmarkDegreeCertification":
          case "BookmarkProgramAreaDegreeStudy":
					case "BookmarkSkill":
          case "BookmarkInternship":
          case "BookmarkCareerArea":
            BookmarkCompleted();
						break;

					case "SendJobEmail":
					case "SendEmployerEmail":
					case "SendDegreeCertificationEmail":
          case "SendProgramAreaDegreeStudyEmail":
					case "SendSkillEmail":
          case "SendInternshipEmail":
          case "SendCareerAreaEmail":
            SendEmailCompleted();
						break;

          case "PrintJob":
          case "PrintEmployer":
          case "PrintDegreeCertification":
          case "PrintProgramAreaDegreeStudy":
          case "PrintSkill":
          case "PrintInternship":
          case "PrintCareerArea":
            PrintCompleted(ModalPrintFormat.None);
            break;
        }
			}
		}

		#endregion

    /// <summary>
    /// Handles the CloseCommand event of the Confirmation control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Confirmation_CloseCommand(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "EmailConfirmationClose":
					SendEmailCompleted();
					break;

				case "BookmarkConfirmationClose":
					BookmarkCompleted();
					break;
			}
		}

		#endregion

    /// <summary>
    /// Handles the ItemClicked event of the Modal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void Modal_ItemClicked(object sender, CommandEventArgs e)
		{
      var args = (LightboxEventCommandArguement)e.CommandArgument;
      List<LightboxBreadcrumb> breadcrumbs;
      ExplorerCriteria criteria;
      switch (e.CommandName)
      {
        case "ViewJob":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Job, out breadcrumbs, out criteria);
          Job.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewEmployer":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Employer, out breadcrumbs, out criteria);
          Employer.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewDegreeCertification":
        case "ViewDegreeCertificationStudy":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.DegreeCertification, out breadcrumbs, out criteria);
          DegreeCertification.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewProgramAreaDegree":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.ProgramAreaDegree, out breadcrumbs, out criteria);
          ProgramAreaDegreeStudy.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewSkill":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Skill, out breadcrumbs, out criteria);
          Skill.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewInternship":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.Internship, out breadcrumbs, out criteria);
          Internship.Show(args.Id, criteria, breadcrumbs);
          break;

        case "ViewCareerArea":
          BuildBreadcrumbsAndCriteria(args, ReportTypes.CareerArea, out breadcrumbs, out criteria);
          CareerArea.Show(args.Id, criteria, breadcrumbs);
          break;
      }
		}

    /// <summary>
    /// Builds the breadcrumbs and criteria.
    /// </summary>
    /// <param name="args">The args.</param>
    /// <param name="reportType">Type of the report.</param>
    /// <param name="breadcrumbs">The breadcrumbs.</param>
    /// <param name="criteria">The criteria.</param>
    private void BuildBreadcrumbsAndCriteria(LightboxEventCommandArguement args, ReportTypes reportType, out List<LightboxBreadcrumb> breadcrumbs, out ExplorerCriteria criteria)
    {
      if (args.Breadcrumbs != null)
      {
        #region Filter cancelled
        // If a filter is cancelled we have the criteria and breadcrumbs already
        if (args.CriteriaHolder.IsNotNull())
        {
          criteria = args.CriteriaHolder;
          breadcrumbs = args.Breadcrumbs;
          return;
        }
        #endregion


        // Reuse the last criteria otherwise
        criteria = args.Breadcrumbs.Last().Criteria.Clone();

        #region Moving back through breadcrumbs
        // Remove last criteria from breadcrumbs if moving backwards
        if (args.MovingBack)
        {
          args.Breadcrumbs.RemoveAt(args.Breadcrumbs.Count - 1);
          breadcrumbs = args.Breadcrumbs;
          return;
        }
        #endregion

        // Set breadcrumbs
        breadcrumbs = args.Breadcrumbs;

        // When moving forward we may need to reset criteria depending on source and destination
        if (!args.MovingBack)
        {
          if (reportType == ReportTypes.Job || reportType == ReportTypes.Internship)
          {
            // Always clear down when moving to a job or internship modal
            var newCriteria = new ExplorerCriteria
            {
              StateArea = criteria.StateArea,
              StateAreaId = criteria.StateAreaId
            };
            criteria = newCriteria;
          }

          if (args.Breadcrumbs.Last().LinkType != ReportTypes.Internship)
          {
            // Remove internship details when movng away from the modal.
            criteria.InternshipCategoryId = null;
            criteria.InternshipCategory = null;
          }

          // Reset degree data when travelling to a degree modal
          if (reportType == ReportTypes.DegreeCertification)
          {
            // If moving from the job to the degree modal reset the degree criteria (Modal will set the right Ids)
            criteria.EducationCriteriaOption = EducationCriteriaOptions.DegreeLevel;
            criteria.DegreeLevel = DegreeLevels.AnyOrNoDegree;
            criteria.DegreeLevelLabel = CodeLocalise(DegreeLevels.AnyOrNoDegree, "Any/No Degree");
            criteria.DegreeEducationLevel = null;
            criteria.DegreeEducationLevelId = null;
            // Reset career area as well
            criteria.CareerArea = null;
            criteria.CareerAreaId = null;
          }
          else if (reportType == ReportTypes.Skill)
          {
            criteria.EmployerId = null;
            criteria.Employer = null;
          }
        }
      }
      else
      {
        criteria = ReportCriteria;
        breadcrumbs = null;
      }
    }

		#endregion

    /// <summary>
    /// Creates the bookmark.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="bookmarkType">Type of the bookmark.</param>
    /// <param name="criteria">The criteria.</param>
		private void CreateBookmark(string name, BookmarkTypes bookmarkType, ExplorerCriteria criteria)
		{
      if (Utilities.CreateExplorerBookmark(name, bookmarkType, criteria))
        Confirmation.Show(CodeLocalise("BookmarkConfirmation.Title", "Bookmark Confirmation"),
                          CodeLocalise("BookmarkConfirmation.Body", "Your bookmark has been added"),
                          CodeLocalise("Global.Close.Text", "Close"), "BookmarkConfirmationClose", height: 50, width: 250);
      else
        Confirmation.Show(CodeLocalise("BookmarkDuplication.Title", "Bookmark Duplication"),
                          CodeLocalise("BookmarkDuplication.Body", "A bookmark has been already been added for this item"),
                          CodeLocalise("Global.Close.Text", "Close"), "BookmarkConfirmationClose", height: 50, width: 250);
		}

    /// <summary>
    /// Binds the controls.
    /// </summary>
    private void BindControls()
    {
      #region setup controls

      ResearchNextButton.OnClientClick =
        String.Format("javascript:ShowPanel('{0}', '{1}', true, 'ResearchNext'); return false;",
                      ResearchStep2Panel.ClientID, ResearchNextButton.ClientID);
      ResearchStep2Panel.Style.Add(HtmlTextWriterStyle.Display, "none");

      ResearchGoButton.OnClientClick = "javascript:if (!Page_ClientValidate('ResearchNext')) return false;";

      Utilities.PopulateDegreeLevelDropdown(DetailedDegreeLevelDropDownList,
        CodeLocalise("CertificateOrAssociate", "Certificate or Associate"),
        CodeLocalise("Bachelors", "Bachelors"),
        CodeLocalise("GraduateOrProfessional", "Graduate or Professional")
      );

      #region Auto Complete TextBox setup

      var serviceMethodParameters = new List<AutoCompleteMethodParameter>
			    {
			      new AutoCompleteMethodParameter
			        {
			          ControlId = ResearchStateAreaSelector.AreaDropDownListClientID,
			          ParameterName = "stateAreaId",
			          ParameterType = "long"
			        }
			    };

      ResearchJobTitleAutoCompleteTextBox.InFieldLabelText = CodeLocalise("TypeAJobTitle.Label", "type a job title");
      ResearchJobTitleAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
      ResearchJobTitleAutoCompleteTextBox.ServiceMethod = "GetJobTitleAutoCompleteItems";
      ResearchJobTitleAutoCompleteTextBox.ServiceMethodParameters = serviceMethodParameters.ToArray();

      var js = String.Format(@"ResearchJobTitleSelectorValidateShowJobs(\'{0}\',\'{1}\',\'{2}\',\'{3}\',\'{4}\',\'{5}\');",
                       ResearchJobTitleAutoCompleteTextBox.ClientID,
                       ResearchJobTitleAutoCompleteRelatedJobs.ClientID,
                       String.Concat(UrlBuilder.AjaxService(), "/GetRelatedJobs"),
                       "RelatedJobs",
                       CodeLocalise("Global.Job.TopDefault", "- select job -"),
                       CodeLocalise("LoadingJobs.Text", "[Loading jobs...]"));

      ResearchJobTitleAutoCompleteTextBox.OnClientItemSelected = js;
      ResearchJobTitleAutoCompleteTextBoxJobRow.Style.Add(HtmlTextWriterStyle.Display, "none");

      ResearchJobTitleAutoCompleteRelatedJobs.Items.AddLocalisedTopDefault("Global.Jobs.TopDefault", "- select job -");

      ResearchEmployerAutoCompleteTextBox.InFieldLabelText = CodeLocalise("TypeAnEmployerName.Label",
																																					"type a #BUSINESS#:LOWER name");
      ResearchEmployerAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
      ResearchEmployerAutoCompleteTextBox.ServiceMethod = "GetEmployerAutoCompleteItems";
      ResearchEmployerAutoCompleteTextBox.ServiceMethodParameters = serviceMethodParameters.ToArray();

      ResearchStudyAutoCompleteTextBox.InFieldLabelText = CodeLocalise("TypeAProgramOfStudyName.Label",
                                                                       "type a program of study name");
      ResearchStudyAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
      ResearchStudyAutoCompleteTextBox.ServiceMethod = "GetDegreeEducationLevelAutoCompleteItems";
	    ResearchStudyAutoCompleteTextBox.ResultSize = 10;

      ResearchSkillAutoCompleteTextBox.InFieldLabelText = CodeLocalise("TypeASkill.Label", "type a skill");
      ResearchSkillAutoCompleteTextBox.ServiceUrl = UrlBuilder.AjaxService();
      ResearchSkillAutoCompleteTextBox.ServiceMethod = "GetSkillAutoCompleteItems";
      ResearchSkillAutoCompleteTextBox.ServiceMethodParameters = serviceMethodParameters.ToArray();
	    ResearchSkillAutoCompleteTextBox.ResultSize = 10;

      if (App.Settings.ExplorerDegreeFilteringType.IsNotIn(DegreeFilteringType.Both, DegreeFilteringType.TheirsThenOurs))
      {
        ResearchStudyClientOnlyLiteral.Text = String.Empty;
        ResearchStudyClientOnlyCheckBox.InputAttributes.Add("Title", "Research Study");
        ResearchStudyClientOnlyCheckBox.Style.Add("display", "none");
        if (App.Settings.ExplorerDegreeFilteringType == DegreeFilteringType.TheirsThenOurs)
        {
          serviceMethodParameters.Add(new AutoCompleteMethodParameter
          {
            OverrideValue = "true",
            ParameterName = "searchClientDataOnly",
            ParameterType = "bool"
          });
        }
      }
      else
      {
        ResearchStudyClientOnlyLiteral.Text = @"<br />";
        // TODO: Remove client hard-coding once localised data added to database for Apollo
        ResearchStudyClientOnlyCheckBox.Text = CodeLocalise(string.Concat("Explorer.Label.ResearchStudyClientOnly.", App.Settings.ExplorerDegreeClientDataTag),
                                                            App.Settings.ExplorerDegreeClientDataTag == "ApolloUoPX"
                                                              ? "UoPX degrees only"
                                                              : "#SCHOOLNAME# degrees only");
        ResearchStudyClientOnlyCheckBox.InputAttributes.Add("Title", "Research Study");
        ResearchStudyClientOnlyCheckBox.TextAlign = TextAlign.Right;
        ResearchStudyClientOnlyCheckBox.Checked = false;

        serviceMethodParameters.Add(new AutoCompleteMethodParameter
        {
          ControlId = ResearchStudyClientOnlyCheckBox.ClientID,
          ParameterName = "searchClientDataOnly",
          ParameterType = "bool"
        });
      }
      serviceMethodParameters.Add(new AutoCompleteMethodParameter
      {
        ControlId = DetailedDegreeLevelDropDownList.ClientID,
        ParameterName = "DetailedDegreeLevel",
        ParameterType = "string"
      });
      ResearchStudyAutoCompleteTextBox.ServiceMethodParameters = serviceMethodParameters.ToArray();
      DetailedDegreeDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

      StudyProgramAreaDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreas();
      StudyProgramAreaDropDownList.DataTextField = "Name";
      StudyProgramAreaDropDownList.DataValueField = "Id";
      StudyProgramAreaDropDownList.DataBind();
      StudyProgramAreaDropDownList.Items.AddLocalisedTopDefault("Global.ProgramArea.TopDefault", "- select department -");

      StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

      #endregion

      StudyNextButton.OnClientClick =
        String.Format("javascript:ShowPanel('{0}', '{1}', true, 'StudyNext'); return false;",
                      StudyStep2Panel.ClientID, StudyNextButton.ClientID);
      StudyStep2Panel.Style.Add(HtmlTextWriterStyle.Display, "none");

      ExperienceNextButton.OnClientClick =
        String.Format("javascript:ShowPanel('{0}', '{1}', true, 'ExperienceGo'); return false;",
                       ExperienceStep2Panel.ClientID, ExperienceNextButton.ClientID);

      switch (App.Settings.Theme)
      {
        case FocusThemes.Education:
          ResearchCareerRadioButton.Checked = false;
          ResearchStudyByDegreeRadioButton.Checked = !App.Settings.ShowProgramArea;
          ResearchStudyByDeptRadioButton.Checked = App.Settings.ShowProgramArea;
          ResearchStudyTypeButtons.Visible = App.Settings.ShowProgramArea;
          ResearchCareerRow.Visible = false;
          ResearchSkillRow.Visible = true;

          ExploreGoButton.Visible = false;
          ExploreNextButton.OnClientClick =
            String.Format("javascript:ShowPanel('{0}', '{1}', true, 'ExploreGo'); return false;",
                          ExploreStep2Panel.ClientID, ExploreNextButton.ClientID);
          ExploreStep2Panel.Style.Add(HtmlTextWriterStyle.Display, "none");
          CareerAreaDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetCareerAreas();
          CareerAreaDropDownList.DataTextField = "Name";
          CareerAreaDropDownList.DataValueField = "Id";
          CareerAreaDropDownList.DataBind();
          CareerAreaDropDownList.Items.AddLocalisedTopDefault("CareerArea.TopDefault", "All Career Areas");
          CareerAreaDropDownList.Attributes.Add("onclick", string.Format("document.getElementById(\"{0}\").checked = true;", ExploreByCareerAreaRadionButton.ClientID));
          CareerAreaDropDownList.Attributes.Add("onkeypress", string.Format("document.getElementById(\"{0}\").checked = true;", ExploreByCareerAreaRadionButton.ClientID));

          if (App.Settings.ExplorerOnlyShowJobsBasedOnClientDegrees)
          {
            ExploreAllJobsPlaceHolder.Visible =
              ResearchEmployerPlaceHolder.Visible = ResearchJobPlaceHolder.Visible = false;
            ExploreAllJobsRadioButton.Checked = false;
            ExploreByCareerAreaRadionButton.Checked = true;
          }

          break;

        default:
          ResearchCareerRadioButton.Checked = true;
          ResearchStudyByDeptRadioButton.Checked = false;
          ResearchStudyByDegreeRadioButton.Checked = true;
          ResearchStudyTypeButtons.Visible = false;
          ResearchCareerRow.Visible = true;
          ResearchSkillRow.Visible = false;

          ExploreNextButton.Visible = false;
          ExploreStep2Panel.Visible = false;
          ExploreStep1Text.Visible = false;

          break;
      }


      StudyGoButton.OnClientClick = "javascript:return ValidateStudyOption();";

      #endregion

      ExploreStateAreaSelector.DefaultStateAreaId = SchoolStateAreaSelector.DefaultStateAreaId = ResearchStateAreaSelector.DefaultStateAreaId = StudyStateAreaSelector.DefaultStateAreaId = ExperienceStateAreaSelector.DefaultStateAreaId = ServiceClientLocator.ExplorerClient(App).GetStateAreaDefault().Id;
    }

    /// <summary>
    /// Binds the details degree down-down list
    /// </summary>
    private void BindDetailedDegreeDropDownList()
    {
      DetailedDegreeLevels detailedDegreeLevel;
      Enum.TryParse(DetailedDegreeLevelDropDownList.SelectedValue, true, out detailedDegreeLevel);

      DetailedDegreeDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(null, null, detailedDegreeLevel)
                                                                     .OrderBy(d => d.DegreeName);
      DetailedDegreeDropDownList.DataTextField = "DegreeName";
      DetailedDegreeDropDownList.DataValueField = "DegreeEducationLevelId";
      DetailedDegreeDropDownList.DataBind();
      DetailedDegreeDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

      var selectedDegree = Request.Form[DetailedDegreeDropDownList.UniqueID];
      if (selectedDegree.IsNotNullOrEmpty())
        DetailedDegreeDropDownList.SelectedValue = selectedDegree;
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
      var goText = string.Concat(CodeLocalise("Global.Go.Text.NoEdit", "Go"), RightArrow);

		  ResearchHeadingLabel.Text = App.Settings.Theme == FocusThemes.Education
				? HtmlLocalise("Explorer.Label.ResearchStudy.Education", "Research a program of study, job, #BUSINESS#:LOWER or skill")
				: HtmlLocalise("Explorer.Label.ResearchStudy", "Research a specific program of study, career or #BUSINESS#:LOWER");
			
			SearchTermRequired.Text = CodeLocalise("SearchTerm.RequiredErrorMessage", "Search term is required");
			SearchTermValidator.Text = CodeLocalise("SearchTerm.ValidatorErrorMessage", "<div></div>Search term minimum 3 characters");

			SearchGoButton.Text = CodeLocalise("Global.Go.Text.NoEdit", "Go").ToUpper();

      SchoolGoButton.Text = ExploreGoButton.Text = goText;
      
			ResearchNextButton.Text = CodeLocalise("Global.Next.Text", "Next");
			ResearchCareerRadioButton.Text = CodeLocalise("ResearchCareer.Label", "Career options");
			ResearchJobRadioButton.Text = CodeLocalise("ResearchJob.Label", "A type of job");
			ResearchEmployerRadioButton.Text = CodeLocalise("ResearchEmployer.Label", "A #BUSINESS#:LOWER");
			ResearchStudyRadioButton.Text = CodeLocalise("ResearchStudy.Label", "A program of study");
      ResearchStudyByDeptRadioButton.Text = CodeLocalise("ResearchStudyProgramArea.Label", "Search by department");
      ResearchStudyByDegreeRadioButton.Text = CodeLocalise("ResearchStudyDegree.Label", "Search by degree");
      ResearchSkillRadioButton.Text = CodeLocalise("ResearchSkill.Label", "A skill");
      ResearchGoButton.Text = goText;

			StudyNextButton.Text = CodeLocalise("Global.Next.Button", "Next");
      StudyDegreeRadioButton.Text = CodeLocalise("StudyDegree.Label", "Degrees and certificates in demand");
      StudySkillRadioButton.Text = CodeLocalise("StudySkill.Label", "Skills in demand");
      StudyGoButton.Text = goText;

      ExperienceNextButton.Text = CodeLocalise("Global.Next.Text", "Next");
		  ExperienceGoButton.Text = goText;

      ExploreNextButton.Text = CodeLocalise("Global.Next.Text", "Next");
		  ExploreStep1Text.Text = CodeLocalise("ResearchStep1Of2.Label", "<strong>Step 1</strong> of 2");
      ExploreAllJobsRadioButton.Text = CodeLocalise("ExplorerAllJobsInArea.Label", "All jobs in your chosen location");
		  ExploreInternshipsRadioButton.Text = CodeLocalise("ExplorerIntrernships.Label", "Internships");
      ExploreByCareerAreaRadionButton.Text = CodeLocalise("ExploreJobsByCareerArea.Label", "Jobs related to this career area");
      ExploreStep2GoButton.Text = goText;

      ResearchStudyValidator.Text = CodeLocalise("ResearchStudyValidator.Text", "Please select a degree");
      ResearchJobTitleValidator.Text = CodeLocalise("ResearchJobTitleValidator.Text", "Please select a job");
			ResearchEmployerValidator.Text = CodeLocalise("ResearchEmployerValidator.Text", "Please select a #BUSINESS#:LOWER");
      ResearchSkillValidator.Text = CodeLocalise("ResearchSkillValidator.Text", "Please select a skill");

			if (App.Settings.HideMocCodes)
				CurrentJobTitleMocCodeLabel.DefaultText = CodeLocalise("WhatsYourCurrentJobTitle.Text", "What's your current job title?");
    }
	}
}
