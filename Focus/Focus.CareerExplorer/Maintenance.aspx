﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Maintenance.aspx.cs" Inherits="Focus.CareerExplorer.Maintenance" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<div class="site-maintenance">
		<h1><%= HtmlLocalise("SiteMaintenance.Heading", "Site Under Maintenance") %></h1>
		<p><%= HtmlLocalise("SiteMaintance.Text", "The site is currently unavailable while we perform routine maintenance. We apologize for any inconvenience caused.") %></p>
	</div>
</asp:Content>