﻿using System;
using System.Linq;
using Focus.Common.Helpers;

namespace Focus.Web
{
  public partial class LocalisePopup : PageBase
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      if (IsPostBack) return;
      var localiseKey = Request.QueryString.Get("LocaliseKey");
      if (String.IsNullOrEmpty(localiseKey)) return;

      var localisationModeHelper = new LocalisationModeHelper(App);
      var dict = localisationModeHelper.LocalisationDictionary;
      var entry = dict.GetValue(localiseKey);

      if (entry == null) return;
      txtKey.Text = entry.LocaliseKey;
      txtDefaultValue.Text = entry.DefaultValue;
      txtLocalisedValue.Text = entry.LocalisedValue;
      Page.ClientScript.RegisterHiddenField("hidLocaliseKey", localiseKey);
      Page.ClientScript.RegisterHiddenField("hidDefaultValue", entry.DefaultValue);
    }

  }
}