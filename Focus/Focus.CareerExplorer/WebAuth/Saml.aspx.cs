﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Security;
using System.Xml;
using BgtSSO;
using ComponentPro.Saml;
using ComponentPro.Saml2;
using ComponentPro.Saml2.Binding;
using Focus.Core;
using Focus.Core.Models;
using Framework.Core;
using Framework.Logging;
using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.CareerExplorer.WebAuth
{
	public partial class Saml : PageBase
	{
		private SamlContext _samlContext;

		internal const string GotoValueCookieKey = "GotoValueCookieKey";
		internal const string RquestedResourceUrl = "RquestedResourceUrl";
		internal const string SSOProfileKey = "SSOProfileKey";

		private string GetGoto()
		{
			var gotoValue = Request.QueryString["goto"] ?? "home";
			return gotoValue;
		}

		private void SetGoto()
		{
			var gotoValue = Request.QueryString["goto"];
			App.SetSessionValue(GotoValueCookieKey, gotoValue);
		}

		#region Certificates

		public X509Certificate2 ServiceProviderCertificate
		{
			get
			{
				var certificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
				certificateStore.Open(OpenFlags.ReadOnly);

				var certificate = certificateStore.Certificates.Cast<X509Certificate2>().FirstOrDefault(c => c.Subject == "CN=" + App.Settings.SamlSpKeyName);

				certificateStore.Close();

				return certificate;
			}
		}

		public List<X509Certificate2> IdentityProviderCertificates
		{
			get
			{
				var certificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
				certificateStore.Open(OpenFlags.ReadOnly);

				var commonNames = App.Settings.SamlIdPCertKeyName.Split(';').Select(s => "CN=" + s);

				var certificates = certificateStore.Certificates.Cast<X509Certificate2>().Where(c => commonNames.Contains(c.Subject)).ToList();

				certificateStore.Close();

				return certificates;
			}
		}

        public List<X509Certificate2> SecondaryIdentityProviderCertificates
        {
            get
            {
                var certificateStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                certificateStore.Open(OpenFlags.ReadOnly);

                var commonNames = App.Settings.SamlSecIdPCertKeyName.Split(';').Select(s => "CN=" + s);

                var certificates = certificateStore.Certificates.Cast<X509Certificate2>().Where(c => commonNames.Contains(c.Subject)).ToList();

                certificateStore.Close();

                return certificates;
            }
        }


		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Redirect to Login if this module isn't running under saml
			if (!(App.Settings.SamlEnabled || App.Settings.SamlEnabledForCareer)) Response.Redirect(UrlBuilder.LogIn());

			var isAssert = Page.Is(UrlBuilder.SamlAssertionService());
			var isResolveArtifacts = Page.Is(UrlBuilder.SamlResolveArtifacts());
			var isLogout = Page.Is(UrlBuilder.SamlLogout());

			var applicationUrl = new Uri(HttpContext.Current.Request.Url,
												(HttpContext.Current.Request.ApplicationPath == "/"
													 ? "/" : HttpContext.Current.Request.ApplicationPath + "/"));

			// Do this before checking whether the user is aythen ticated to prevent a redirect to the home page when we actualy want to log out.
			if (isLogout)
			{
				Logout(applicationUrl.AbsoluteUri);
				return;
			}

			if (App.User.IsAuthenticated)
			{
				Response.Redirect(UrlBuilder.Goto(GetGoto()), false);
				return;
			}

			_samlContext = new SamlContext(applicationUrl.AbsoluteUri);

			// If we have a goto value then store it as a cookie value
			SetGoto();

			Logger.Debug("Saml : Path = " + Page.Request.Path + " - Is Saml assertion service? " + isAssert + " - Is Saml resolve artifacts service? " + isResolveArtifacts);

			if (isAssert)
				ProcessAssertion();
			else if (isResolveArtifacts)
				ResolveArtifacts();
			else
				StartSaml();
		}

		/// <summary>
		/// Start authentication using Saml.
		/// </summary>
		private void StartSaml()
		{
			Logger.Debug("Saml : StartSaml");

			try
			{
				ServiceClientLocator.AuthenticationClient(App).LogOut();
			}
			finally
			{
				FormsAuthentication.SignOut();
        App.ClearSession();
			}

			// Authorize the Saml details
			Logger.Debug("Saml : Request Authorisation");

			var request = _samlContext.CreateRequest(
				App.Settings.SamlIdPToSpBindingUrl,
				App.Settings.SamlSpToIdPBindingUrl,
				App.Settings.SamlIdPSingleSignonIdProviderUrl,
				ServiceProviderCertificate
				);

			Logger.Debug("Saml : Send authn request");
			_samlContext.SendRequest(request, HttpContext.Current, App.Settings.SamlSpToIdPBindingUrl, App.Settings.SamlIdPSingleSignonIdProviderUrl, ServiceProviderCertificate, App.Settings.SamlArtifactIdentityUrl);
		}

		/// <summary>
		/// Resolves the artifact received from the identity provider.
		/// </summary>
		private void ResolveArtifacts()
		{
			Logger.Debug("Saml : Resolve Saml artifacts");

			try
			{
				// Create an artifact resolve from the XML data received from the IdP.
				var artifactResolve = ArtifactResolve.Create(Request);

				// Get the artifact.
				var httpArtifact = new Saml2ArtifactType0004(artifactResolve.Artifact.ArtifactValue);

				// Remove the artifact state from the cache.
				var artifactXml = (XmlElement)SamlSettings.CacheProvider.Remove(httpArtifact.ToString());
				if (artifactXml == null)
					return;

				// Create an artifact response and add the cached SAML message
				var artifactResponse = new ArtifactResponse
					{
						Issuer = new Issuer(HttpContext.Current.Request.ApplicationPath),
						Message = artifactXml
					};

				// Send the artifact response.
				artifactResponse.Send(Response);
			}
			catch (Exception ex)
			{
				Logger.Debug("Saml : Exception: " + ex);
				throw;
			}
		}

		/// <summary>
		/// Processes the assertion.
		/// </summary>
		/// <exception cref="ComponentPro.Saml.SamlException">Assertion received from the identity provider was not valid.</exception>
		private void ProcessAssertion()
		{
			Logger.Debug("Saml : Processing assertion");

			var relayState = String.Empty;
			Response samlResponse = null;

			// Get the SAML response
			switch (App.Settings.SamlIdPToSpBindingUrl)
			{
				case SamlBindingUri.HttpPost:
					//var response = HttpContext.Current.Request.Form.GetValues("SAMLResponse");
					samlResponse = ComponentPro.Saml2.Response.Create(Request);
					relayState = samlResponse.RelayState;
					break;

				case SamlBindingUri.HttpArtifact:
					var value = _samlContext.ResolveArtifacts(HttpContext.Current, App.Settings.SamlIdPArtifactIdProviderUrl);
					samlResponse = value.Item1;
					relayState = value.Item2;
					break;
			}

			// Remove the relayState from the cache.  
			if (!String.IsNullOrEmpty(relayState)) SamlSettings.CacheProvider.Remove(relayState);

			if (samlResponse == null)
			{
				throw new SamlException("Failed to retrieve samlResponse.");
			}

			Logger.Debug("samlResponse = " + samlResponse.GetXml().OuterXml);

			if (!samlResponse.IsSuccess())
			{
				throw new SamlException("The saml response was not successful.");
			}

            Assertion assertion = null;

            if (App.Settings.SamlSecExternalClientTag.Split(',').ToList<string>().Any(x => samlResponse.GetXml().GetElementsByTagName("saml:NameID").Item(0).InnerText.StartsWith(x)))
                assertion = _samlContext.GetValidAssertion(samlResponse, SecondaryIdentityProviderCertificates, ServiceProviderCertificate);
            else
                assertion = _samlContext.GetValidAssertion(samlResponse, IdentityProviderCertificates, ServiceProviderCertificate);

			if (assertion == null)
			{
				throw new SamlException("The saml response contained no assertion.");
			}

			var profile = _samlContext.GetProfile(assertion, App.Settings.SamlFirstNameAttributeName, App.Settings.SamlLastNameAttributeName, App.Settings.SamlEmailAddressAttributeName, App.Settings.SamlScreenNameAttributeName, App.Settings.SamlCreateRolesAttributeName, App.Settings.SamlUpdateRolesAttributeName);

			App.SetSessionValue(SSOProfileKey, profile);

			Response.Redirect(UrlBuilder.SSOComplete());
		}

		/// <summary>
		/// Logouts this instance.
		/// </summary>
		private void Logout(string applicationUrl)
		{
			Logger.Debug("Saml: The following query string was sent: " + Request.Url.Query);

			// Get logout request.  We will try all the certificates in turn and pick the one that doesn't error!
			// Throws an error if the query string parameter is not present.
			var logoutRequest = IdentityProviderCertificates.Select(c =>
			{
				try
				{
					return LogoutRequest.Create(Request, c.PublicKey.Key);
				}
				catch (SamlException)
				{
					return null;
				}
			}).FirstOrDefault(l => l != null);

			if (logoutRequest.IsNull()) throw new SamlException("Logout request was not successfully parsed.");
			
			// Log xml decoded as for assertion to get useful debugging info
			Logger.Debug(logoutRequest.GetXml().OuterXml);

			var logoutResponse = new LogoutResponse
				{
					Issuer = new Issuer(applicationUrl),
					InResponseTo = logoutRequest.Id
				};

			if (!App.User.IsAuthenticated)
			{
				logoutResponse.Status = new Status(SamlPrimaryStatusCode.Success, "User was already logged out.");
			}
			else
			{
				var externalId = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId).UserDetails.ExternalId;

				if (externalId.IsNotNullOrEmpty() && externalId == logoutRequest.NameId.NameIdentifier)
				{
					try
					{
						if (App.Settings.Module == FocusModules.Explorer)
						{
							ServiceClientLocator.AuthenticationClient(App).LogOut();
						}
						else
						{
							var usercontext = App.GetSessionValue<UserContext>("Career:UserContext");
							if (usercontext.IsNotNull())
							{
																										#region Save Activity

							//TODO: Martha (new activity functionality)
							//var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
							//if (staffcontext.IsNull() || (staffcontext.IsNotNull() && !staffcontext.IsAuthenticated))
							//{
							//  var jobSeekerActivities = CareerCommon.ServiceClient.ServiceClientLocator.CoreClient(App).GetJobSeekerSessionActivities();
							//  if (jobSeekerActivities.IsNotNull())
							//  {
							//    string activityLog = string.Format(@"Session Duration: {0} minutes\nSearches: {1}\nJobs Viewed: {2}\nReferrals Requested: {3}\nResume Updated: {4}",
							//      Math.Ceiling(DateTime.Now.Subtract(jobSeekerActivities.SessionStartTime).TotalMinutes),
							//      jobSeekerActivities.SearchesMade,
							//      jobSeekerActivities.JobsViewed,
							//      jobSeekerActivities.ReferralsRequested,
							//      jobSeekerActivities.IsResumeUpdated);
							//    ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.JobSeeker, ActivityType.Automated, "SMRY01", activityLog);
							//    CareerCommon.ServiceClient.ServiceClientLocator.CoreClient(App).ClearJobSeekerSessionActivities();
							//  }
						}

							#endregion

							ServiceClientLocator.AuthenticationClient(App).LogOut();
						}
					}
					catch (Exception ex)
					{
						Logger.Error(ex.Message, ex);
					}
					finally
					{
						FormsAuthentication.SignOut();
						App.ClearSession();
					}

					logoutResponse.Status = new Status(SamlPrimaryStatusCode.Success, "Successful logout");
				}
				else
				{
					logoutResponse.Status = new Status(SamlPrimaryStatusCode.Responder, "NameID did not match the external id of the current user");
				}
			}

			// If we have a location to return the logout response to then do so.  Otherwise redirect to the sso return url (which we know has a default)
			var logoutUrl = App.Settings.SamlFederatedLogoutReturnUrl.IsNotNullOrEmpty()
												? App.Settings.SamlFederatedLogoutReturnUrl
												: App.Settings.SSOReturnUrl;

			logoutResponse.Destination = logoutUrl;

			logoutResponse.Redirect(Response, logoutUrl, logoutRequest.RelayState, ServiceProviderCertificate.PrivateKey);
		}
	}
}