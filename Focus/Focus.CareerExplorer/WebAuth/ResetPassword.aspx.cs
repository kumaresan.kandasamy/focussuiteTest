﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebAuth
{
  public partial class ResetPassword : PageBase
  {
    private long? UserId
    {
      get { return GetViewStateValue<long?>("ResetPassword:UserId"); }
      set { SetViewStateValue("ResetPassword:UserId", value); }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      Page.Title = CodeLocalise("PageTitle", "Reset Password");

      if (!IsPostBack)
      {
        var validationKey = (Request.QueryString.Count > 0) ? Request.QueryString[0] : string.Empty;

        ErrorTypes errorType;
				UserId = ServiceClientLocator.AuthenticationClient(App).ValidatePasswordResetValidationKey(validationKey, out errorType);

        if (!UserId.HasValue)
        {
          PasswordFieldsPlaceHolder.Visible = false;
          InvalidKeyPlaceHolder.Visible = true;

          switch (errorType)
          {
              case ErrorTypes.UserValidationKeyExpired:
                ResetPasswordTitleLabel.DefaultText = "Password reset has expired";
                ResetPasswordTitleLabel.LocalisationKey = "ResetPasswordExpired.Label";
                InvalidKeyLabel.Text = App.Settings.Theme == FocusThemes.Education
                  ? HtmlLocalise("UserValidationKeyExpired.Message.Education", "Your request to reset your password has expired. Click on the link below to return to the #FOCUSCAREER# login page and click \"Forgot your password/PIN?\" to request another reset")
                  : HtmlLocalise("UserValidationKeyExpired.Message", "Your request to reset your password has expired. Click on the link below to return to the #FOCUSCAREER# login page and click \"Forgot your password?\" to request another reset");
                break;

              case ErrorTypes.UserValidationKeyAlreadyUsed:
                ResetPasswordTitleLabel.DefaultText = "Password reset completed";
                ResetPasswordTitleLabel.LocalisationKey = "ResetPasswordCompleted.Label";
                InvalidKeyLabel.Text = App.Settings.Theme == FocusThemes.Education
                  ? HtmlLocalise("UserValidationKeyAlreadyUsed.Message.Education", "Your password has already been reset. Click on the link below to return to the #FOCUSCAREER# login page and enter your updated password. If you have forgotten it, click \"Forgot your password/PIN?\" to reset it again")
                  : HtmlLocalise("UserValidationKeyAlreadyUsed.Message", "Your password has already been reset. Click on the link below to return to the #FOCUSCAREER# login page and enter your updated password. If you have forgotten it, click \"Forgot your password?\" to reset it again");
                break;

              default:
                ResetPasswordTitleLabel.DefaultText = "Password reset failed";
                ResetPasswordTitleLabel.LocalisationKey = "ResetPasswordFailed.Label";
                InvalidKeyLabel.Text = CodeLocalise(errorType);
                break;
          }
        }

        LocaliseUI();
        ResetPasswordRegEx.ValidationExpression = RetypedResetPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
      }

      ResetPasswordModal.Show();
    }

    /// <summary>
    /// Handles the Click event of the ResetButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ResetButton_Click(object sender, EventArgs e)
    {
      var resetPassword = (ResetPasswordTextBox.Text ?? string.Empty).Trim();

      try
      {
        var validationKey = (Request.QueryString.Count > 0) ? Request.QueryString[0] : string.Empty;
				ServiceClientLocator.AccountClient(App).ResetPassword(UserId, resetPassword, validationKey);

        ResetPasswordModal.Hide();

        ConfirmationModal.Show(CodeLocalise("SuccessfulChange.Title", "Password Reset"),
                               CodeLocalise("SuccessfulChange.Body", "You have successfully reset your password."),
                               CodeLocalise("SuccessfulChange.Close", "Login"),
                               height: 150,
                               closeLink: UrlBuilder.Home());
      }
      catch (Exception ex)
      {
        ResetPasswordValidator.IsValid = false;
        ResetPasswordValidator.ErrorMessage = ex.Message;
      }
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      ResetPasswordRequired.ErrorMessage = CodeLocalise("ResetPassword.RequiredErrorMessage", "New password is required");
      ResetPasswordRegEx.ErrorMessage = CodeLocalise("ResetPassword.RegExErrorMessage", "New password must be 6-20 characters and contain at least one number");
      RetypedResetPasswordRequired.ErrorMessage = CodeLocalise("ResetPassword.RequiredErrorMessage","Retype new password is required");
      RetypedResetPasswordRegEx.ErrorMessage = CodeLocalise("ResetPassword.RegExErrorMessage", "Retyped new password must be 6-20 characters and contain at least one number");
      ResetPasswordCompare.ErrorMessage = CodeLocalise("ResetPasswordCompare.CompareErrorMessage", "New passwords must match");
      ResetButton.Text = CodeLocalise("Global.Change.Text", "Reset password");

      ForgotPasswordLink.Text = HtmlLocalise("ForgottenPasswordLink.Text", "Return to #FOCUSCAREER# login page");
      CloseLink.NavigateUrl = ForgotPasswordLink.NavigateUrl = UrlBuilder.Home();
    }
  }
}