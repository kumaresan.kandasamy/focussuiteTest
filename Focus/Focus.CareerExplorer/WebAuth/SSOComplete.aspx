﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
	CodeBehind="SSOComplete.aspx.cs" Inherits="Focus.CareerExplorer.WebAuth.SSOComplete" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<h1>
		<asp:Literal runat="server" ID="SSOCompleteTitle"></asp:Literal>
	</h1>
	<p>
		<asp:Literal runat="server" ID="SSOCompleteExplanation"></asp:Literal>
	</p>
	<table class="genericTable sso-table">
		<asp:PlaceHolder runat="server" ID="EmailRow" Visible="False">
			<tr>
				<td>
					<asp:Label runat="server" AssociatedControlID="EmailAddressTextBox" ID="EmailAddressTextBoxLabel"></asp:Label>
				</td>
				<td>
					<asp:TextBox runat="server" ID="EmailAddressTextBox"></asp:TextBox>
				</td>
				<td>
					<asp:RequiredFieldValidator runat="server" ID="EmailAddressRequiredValidator" ControlToValidate="EmailAddressTextBox"
						ValidationGroup="ExtraInformationValidation" SetFocusOnError="true" Display="Dynamic"
						CssClass="error"></asp:RequiredFieldValidator>
					<asp:RegularExpressionValidator ID="EmailAddressRegexValidator" runat="server" ControlToValidate="EmailAddressTextBox"
						ValidationGroup="ExtraInformationValidation" SetFocusOnError="true" Display="Dynamic"
						CssClass="error" />
				</td>
			</tr>
		</asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="FirstNameRow" Visible="False">
			<tr>
				<td>
					<asp:Label runat="server" AssociatedControlID="FirstNameTextBox" ID="FirstNameTextBoxLabel"></asp:Label>
				</td>
				<td>
					<asp:TextBox runat="server" ID="FirstNameTextBox"></asp:TextBox>
				</td>
				<td>
					<asp:RequiredFieldValidator runat="server" ID="FirstNameRequiredValidator" ControlToValidate="FirstNameTextBox"
						ValidationGroup="ExtraInformationValidation" SetFocusOnError="true" Display="Dynamic"
						CssClass="error"></asp:RequiredFieldValidator>
					<asp:RegularExpressionValidator runat="server" ValidationExpression="^.{1,50}$" ID="FirstNameLengthValidator" ControlToValidate="FirstNameTextBox"
						ValidationGroup="ExtraInformationValidation" SetFocusOnError="true" Display="Dynamic"
						CssClass="error"></asp:RegularExpressionValidator>
				</td>
			</tr>
		</asp:PlaceHolder>
		<asp:PlaceHolder runat="server" ID="LastNameRow" Visible="False">
			<tr>
				<td>
					<asp:Label runat="server" AssociatedControlID="LastNameTextBox" ID="LastNameTextBoxLabel"></asp:Label>
				</td>
				<td>
					<asp:TextBox runat="server" ID="LastNameTextBox"></asp:TextBox>
				</td>
				<td>
					<asp:RequiredFieldValidator runat="server" ID="LastNameRequiredValidator" ControlToValidate="LastNameTextBox"
						ValidationGroup="ExtraInformationValidation" SetFocusOnError="true" Display="Dynamic"
						CssClass="error"></asp:RequiredFieldValidator>
					<asp:RegularExpressionValidator runat="server" ValidationExpression="^.{1,50}$" ID="LastNameLengthValidator" ControlToValidate="LastNameTextBox"
						ValidationGroup="ExtraInformationValidation" SetFocusOnError="true" Display="Dynamic"
						CssClass="error"></asp:RegularExpressionValidator>
				</td>
			</tr>
		</asp:PlaceHolder>
	</table>
	<asp:Button runat="server" ID="SubmitButton" OnClick="SubmitExtraInformation" CausesValidation="True"
		ValidationGroup="ExtraInformationValidation" CssClass="buttonLevel2" />
</asp:Content>
