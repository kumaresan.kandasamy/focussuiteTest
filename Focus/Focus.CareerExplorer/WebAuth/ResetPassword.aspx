﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="Focus.CareerExplorer.WebAuth.ResetPassword" %>

<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigation" TagPrefix="uc" %>
<%@ Register src="../Controls/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<asp:HiddenField ID="ResetPasswordModalDummyTarget" runat="server" />
	<act:ModalPopupExtender ID="ResetPasswordModal" runat="server" 
                          BehaviorID="ResetPasswordModal"
													TargetControlID="ResetPasswordModalDummyTarget"
													PopupControlID="ResetPasswordPanel"
	                        BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

	<asp:Panel ID="ResetPasswordPanel" runat="server" CssClass="modal">
	  <div class="lightbox FocusCareer resetPasswordModal">
	    <div class="modalHeader">
        <asp:HyperLink runat="server" ID="CloseLink">
	        <img id="ResetPasswordCloseIcon" src="<%= UrlBuilder.Close() %>" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
        </asp:HyperLink>
	      <focus:LocalisedLabel runat="server" ID="ResetPasswordTitleLabel" LocalisationKey="ResetPassword.Label" DefaultText="Reset Password" CssClass="modalTitle modalTitleLarge"/>
	    </div>
      <asp:PlaceHolder runat="server" ID="PasswordFieldsPlaceHolder">
		    <div class="dataInputContainer">
			    <div class="dataInputRow">
            <focus:LocalisedLabel runat="server" ID="ResetPasswordLabel" CssClass="dataInputLabel" AssociatedControlID="ResetPasswordTextBox" LocalisationKey="ResetPasswordLabel.Text" DefaultText="New password"/>
				    <span class="dataInputField">
				      <asp:TextBox ID="ResetPasswordTextBox" runat="server" ClientIDMode="Static" TextMode="Password" Width="225" /><br />
				      <asp:RequiredFieldValidator ID="ResetPasswordRequired" runat="server" ControlToValidate="ResetPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
				      <asp:RegularExpressionValidator ID="ResetPasswordRegEx" runat="server" ControlToValidate="ResetPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </span>
			    </div>
			    <div class="dataInputRow">
            <focus:LocalisedLabel runat="server" ID="RetypedResetPasswordLabel" CssClass="dataInputLabel" AssociatedControlID="RetypedResetPasswordTextBox" LocalisationKey="RetypedResetPasswordTextBox.Text" DefaultText="Re-type new password"/>
				    <span class="dataInputField">
				      <asp:TextBox ID="RetypedResetPasswordTextBox" runat="server" ClientIDMode="Static" TextMode="Password" Width="225" /><br />
				      <asp:RequiredFieldValidator ID="RetypedResetPasswordRequired" runat="server" ControlToValidate="RetypedResetPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
				      <asp:RegularExpressionValidator ID="RetypedResetPasswordRegEx" runat="server" ControlToValidate="RetypedResetPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
				      <asp:CompareValidator ID="ResetPasswordCompare" runat="server" ControlToValidate="RetypedResetPasswordTextBox" ControlToCompare="ResetPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" />
            </span>
			    </div>
        </div>
		    <div class="modalButtons">
			    <asp:Button ID="ResetButton" runat="server" CssClass="buttonLevel2" OnClick="ResetButton_Click" />
		    </div>
        <p>
          <asp:CustomValidator ID="ResetPasswordValidator" runat="server" Display="Dynamic" CssClass="error" />
        </p>
      </asp:PlaceHolder>
      <asp:PlaceHolder runat="server" ID="InvalidKeyPlaceHolder" Visible="False">
        <p>
          <asp:Label runat="server" id="InvalidKeyLabel"></asp:Label>
        </p>
        <p>
          <asp:HyperLink runat="server" ID="ForgotPasswordLink"></asp:HyperLink>
        </p>
      </asp:PlaceHolder>
    </div>
	</asp:Panel>

	<uc:ConfirmationModal ID="ConfirmationModal" runat="server"  />
</asp:Content>
