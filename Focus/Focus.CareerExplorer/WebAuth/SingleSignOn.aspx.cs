﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web;
using System.Web.Security;

using Focus.CareerExplorer.WebAuth.Controls;
using Focus.Common;
using Focus.Core;
using Focus.CareerExplorer.Code;
using Focus.Common.Authentication;

#endregion

namespace Focus.CareerExplorer.WebAuth
{
	public partial class SingleSignOn : PageBase
	{
		/// <summary>
		/// Gets a value indicating whether this instance is single sign on.
		/// </summary>
		protected override bool IsSingleSignOn { get { return true; } }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				// Logout any previous session and clear it out
				try
				{
					ServiceClientLocator.AuthenticationClient(App).LogOut();
				}
				finally
				{
					var demoShieldValidated = DemoShieldValidated;

					FormsAuthentication.SignOut();
          App.ClearSession();

					DemoShieldValidated = demoShieldValidated;
				}

				var key = new Guid();
				var actionType = SSOActionType.None;

				if (Page.RouteData.Values.ContainsKey("key"))
				{
					Guid.TryParse(Page.RouteData.Values["key"].ToString(), out key);
					actionType = SSOActionType.AccessEmployeeAccount;
				}

				if (Page.RouteData.Values.ContainsKey("reactivate"))
				{
					App.SetSessionValue("ReactivateRequestKey", true);
				}

				if (Page.RouteData.Values.ContainsKey("validate"))
				{
					Guid.TryParse(Page.RouteData.Values["validate"].ToString(), out key);
					actionType = SSOActionType.CompleteRegistration;
				}

				if (actionType == SSOActionType.None)
				{
					DemoShieldValidated = false;
					Response.Redirect(UrlBuilder.LogIn());
				}

				try
				{
					var userData = ServiceClientLocator.AuthenticationClient(App).ValidateSingleSignOn(key);

					App.UserData.Load(userData.UserData);

					if (actionType == SSOActionType.CompleteRegistration)
						ServiceClientLocator.AccountClient(App).ActivateAccount(userData.UserContext.UserId);

					var authTicket = new FormsAuthenticationTicket(1, userData.UserContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, userData.UserContext.SerializeUserContext());
					var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

					var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
					Response.Cookies.Add(authCookie);

					Context.User = new UserPrincipal(Context.User.Identity, userData.UserContext);

					/* Assist users should not be shown Welcome Modal (FVN-2601)
					if (App.Settings.Theme == FocusThemes.Education && (App.Settings.Module == FocusModules.Career || App.Settings.Module == FocusModules.CareerExplorer))
					{
						var login = (LogInOrRegister) Page.Master.FindControl("MainLogIn");

            if (actionType == SSOActionType.AccessEmployeeAccount)
						  login.ShowAppropriateWindow();
            else
              login.ShowMigratedAgreeToTerms();
					}
					else
					*/
					Response.Redirect(UrlBuilder.Default(), false);
				}
				catch
				{
					DemoShieldValidated = false;
					Response.Redirect(UrlBuilder.Default());
				}
			}
		}
	}
}