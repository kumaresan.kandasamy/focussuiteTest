﻿using System;
using System.Web;
using System.Web.Security;
using BgtSSO;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Framework.Core;
using Framework.Logging;

namespace Focus.CareerExplorer.WebAuth
{
	public partial class SSOComplete : PageBase
	{
		private string GetGoto()
		{
			var gotoValue = App.GetSessionValue<string>(Saml.GotoValueCookieKey);
			App.RemoveSessionValue(Saml.GotoValueCookieKey);
			return gotoValue;
		}

		private string GetOriginalUrl()
		{
			var rquestedResourceUrl = App.GetCookieValue(Saml.RquestedResourceUrl);
			App.RemoveCookieValue(Saml.RquestedResourceUrl);
			return rquestedResourceUrl;
		}

		protected SSOProfile Profile
		{
			get { return GetViewStateValue<SSOProfile>("SSOComplete:Profile"); }
			set { SetViewStateValue("SSOComplete:Profile", value); }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
      if (App.User.IsAuthenticated || !(App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer)) Response.Redirect(UrlBuilder.Home());

			if (!IsPostBack)
			{
				Master.LoginEnabled = false;

				Profile = App.GetSessionValue<SSOProfile>(Saml.SSOProfileKey);

				// If there is no profile in the session state then we can't continue because we have no external id to associate the profile with
				if (Profile.IsNull()) Response.Redirect(UrlBuilder.Saml());

				// Check if user already exists
				var user = ServiceClientLocator.AccountClient(App).FindUserByExternalId(Profile.Id);

				// If the user already exists then log them in.
				if (user.IsNotNull() && user.IsMigrated) SamlSuccess(Profile);

				var emailAddressNotProvided = Profile.EmailAddress.IsNullOrEmpty();
				var firstNameNotProvided = Profile.FirstName.IsNullOrEmpty();
				var lastNameNotProvded = Profile.LastName.IsNullOrEmpty();

				if (!(emailAddressNotProvided || firstNameNotProvided || lastNameNotProvded))
				{
					if (!App.Settings.SSOForceNewUserToRegistration) SamlSuccess(Profile);
					Master.LoginEnabled = true;
					SubmitButton.Visible = false;
					return;
				}

				SSOCompleteTitle.Text = CodeLocalise("SSOCompleteTitle.Text", "Some required information has not been supplied");
				SSOCompleteExplanation.Text = CodeLocalise("SSOCompleteExplanation.Text", "Please enter the following information so that we can create your account");
				SubmitButton.Text = CodeLocalise("SSOCompleteSubmitButton.Text", "Submit");

				if (emailAddressNotProvided)
				{
					EmailRow.Visible = true;
					EmailAddressTextBoxLabel.Text = CodeLocalise("SSOCompleteEmailAddressLabel", "Email address");
					EmailAddressRequiredValidator.ErrorMessage = CodeLocalise("SSOCompleteEmailAddressRequiredValidatorMessage", "An email address is required");
					EmailAddressRegexValidator.ErrorMessage = CodeLocalise("SSOCompleteEmailAddressRegexValidatorMessage", "The email address provided is not in an accepted format.");
					EmailAddressRegexValidator.ValidationExpression = App.Settings.EmailAddressRegExPattern;
				}

				if (firstNameNotProvided)
				{
					FirstNameRow.Visible = true;
					FirstNameTextBoxLabel.Text = CodeLocalise("SSOCompleteFirstNameLabel", "First name");
					FirstNameRequiredValidator.ErrorMessage = CodeLocalise("SSOCompleteFirstNameRequiredValidatorMessage", "A first name is required");
					FirstNameLengthValidator.ErrorMessage = CodeLocalise("SSOCompleteFirstNameLengthValidatorMessage", "First name cannot be more than 50 characters");
				}

				if (lastNameNotProvded)
				{
					LastNameRow.Visible = true;
					LastNameTextBoxLabel.Text = CodeLocalise("SSOCompleteLastNameLabel", "Last name");
					LastNameRequiredValidator.ErrorMessage = CodeLocalise("SSOCompleteLastNameRequiredValidatorMessage", "A last name is required");
					LastNameLengthValidator.ErrorMessage = CodeLocalise("SSOCompleteLastNameLengthValidatorMessage", "Last name cannot be more than 50 characters");
				}
			}
		}

		protected void SubmitExtraInformation(object sender, EventArgs e)
		{
			if (EmailAddressTextBox.Text.IsNotNullOrEmpty()) Profile.EmailAddress = EmailAddressTextBox.Text;

			if (FirstNameTextBox.Text.IsNotNullOrEmpty()) Profile.FirstName = FirstNameTextBox.Text;

			if (LastNameTextBox.Text.IsNotNullOrEmpty()) Profile.LastName = LastNameTextBox.Text;

			if (!App.Settings.SSOForceNewUserToRegistration) SamlSuccess(Profile);

			App.SetSessionValue(Saml.SSOProfileKey, Profile);

			Response.Redirect(UrlBuilder.SSOComplete());
		}

		/// <summary>
		/// Processes a successful SAML response.
		/// </summary>
		private void SamlSuccess(ISSOProfile profile)
		{
			// Clear out value to keep session clean
			App.RemoveSessionValue(Saml.SSOProfileKey);

			// Login with the profile
			Logger.Debug("Saml : Logging into application using profile: " + profile.Id + " - " + profile.ScreenName);
			var loggedInUser = ServiceClientLocator.AuthenticationClient(App).LogIn(profile, App.Settings.Module);

			App.UserData.Load(loggedInUser.UserData);

			var authTicket = new FormsAuthenticationTicket(1, loggedInUser.UserContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, loggedInUser.UserContext.SerializeUserContext());
			var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

			var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
			Response.Cookies.Add(authCookie);

			Logger.Debug("Saml : Authenticated");

			// When we get the Goto value it will be removed from the cookie.  By only removing it when it is used we can capture a goto that is specified before SAML authentication is requested.
			// This allows us to accept goto values in an initial link and therefore allows us to use them with service provider initiated authentication scenarios.
			//This is the same behaviour for the original url that the user attempted to access.
			var originalUrl = GetOriginalUrl();
			var gotoValue = GetGoto();

			// If we have a goto value then override the user's original choice
			if (gotoValue.IsNotNullOrEmpty())
			{
				Response.Redirect(UrlBuilder.Goto(gotoValue, App), true);
			}
			else if(originalUrl.IsNotNullOrEmpty())
			{
				Response.Redirect(originalUrl, true);
			}
			else
			{
				Response.Redirect(UrlBuilder.Home());
			}
		}
	}
}