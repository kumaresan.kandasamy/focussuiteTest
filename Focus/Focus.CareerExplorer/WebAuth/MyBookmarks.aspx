﻿<%@ Page Title="My Bookmarks" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
	CodeBehind="MyBookmarks.aspx.cs" Inherits="Focus.CareerExplorer.WebAuth.MyBookmarks" %>

<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigation" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Bookmarks.ascx" TagName="Bookmarks" TagPrefix="uc" %>
<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigation ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<h1><focus:LocalisedLabel runat="server" ID="MyBookmarksTitleLabel" LocalisationKey="MyBookmarks.Title" DefaultText="My bookmarks" RenderOuterSpan="False"/></h1>
	<div class="myBookmarkList">
		<uc:Bookmarks ID="Bookmarks" runat="server" />
	</div>
</asp:Content>
