﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web;
using System.Web.Security;

using Framework.Logging;

using Focus.CareerExplorer.Code;
using Focus.Common.Authentication;
using Focus.Common.Extensions;
using Focus.Common;

#endregion

namespace Focus.Web.WebAuth
{
	public partial class OAuth : PageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (App.User.IsAuthenticated) Response.Redirect(UrlBuilder.Home());

			var isVerify = Page.Is(UrlBuilder.OAuthVerifier());
			var isLogin = Page.Is(UrlBuilder.OAuthLogin());

			Logger.Debug("oAuth : Path = " + Page.Request.Path + " - Is OAuth Verifier? " + isVerify + " - Is OAuth Login? " + isLogin);

			// Redirect to Login if this module isn't running under oAuth
			if (!App.Settings.OAuthEnabled)
				Response.Redirect(UrlBuilder.LogIn());

			var oauthContext = new OAuthContext(App.Settings, UrlBuilder.OAuthVerifier(false));

			Logger.Debug("oAuth : CallbackUrl = " + oauthContext.CallbackUrl);
			Logger.Debug("oAuth : OAuthRequestTokenUrl = " + App.Settings.OAuthRequestTokenUrl);
			Logger.Debug("oAuth : OAuthVerifierUrl = " + App.Settings.OAuthVerifierUrl);
			Logger.Debug("oAuth : OAuthRequestAccessTokenUrl = " + App.Settings.OAuthRequestAccessTokenUrl);

			if (!isVerify & !isLogin)
				StartOAuth();
			else if (isVerify)
				VerifyOAuth();
			else
				LoginOAuth();
		}

		private void StartOAuth()
		{
			Logger.Debug("oAuth : StartOAuth");

			Logger.Debug("oAuth : Logout any previous session and clear it out");
			try
			{
				ServiceClientLocator.AuthenticationClient(App).LogOut();
			}
			finally
			{
				FormsAuthentication.SignOut();
        App.ClearSession();
			}

			// Create an oAuth Context and get cracking
			Logger.Debug("oAuth : Create an oAuth Context and get cracking");
			var oauthContext = new OAuthContext(App.Settings, UrlBuilder.OAuthVerifier(false));
			OAuthContext.Current = oauthContext;

			// Authorize the oAuth details
			Logger.Debug("oAuth : Authorize");
			oauthContext.Authorize();

			// Obtain the Verifier
			Logger.Debug("oAuth : ObtainVerifier");
			oauthContext.ObtainVerifier();
		}

		private void VerifyOAuth()
		{
			Logger.Debug("oAuth : Verify OAuth");

			try
			{
				// Get back the saved oAuth Context
				Logger.Debug("oAuth : Getting OAuthContext.Current");
				var oauthContext = OAuthContext.Current;

				// End authentication and register tokens.
				Logger.Debug("oAuth : End authentication and register tokens");
				oauthContext.EndAuthenticationAndRegisterTokens();

				// Verify the oAuth details
				Logger.Debug("oAuth : Verifying");
				oauthContext.Verify();
				Logger.Debug("oAuth : Verified");

				Logger.Debug("oAuth : Logging in");
				Response.Redirect(UrlBuilder.OAuthLogin());
			}
			catch (Exception ex)
			{
				Logger.Debug("oAuth : Exception: " + ex);
				throw;
			}
		}

		private void LoginOAuth()
		{
			Logger.Debug("oAuth : LoginOAuth");

			try
			{
				// Get back the saved oAuth Context
				Logger.Debug("oAuth : Getting OAuthContext.Current");
				var oauthContext = OAuthContext.Current;

				// Get the Profile
				Logger.Debug("oAuth : Getting Profile");
				var profile = oauthContext.GetProfile();
				Logger.Debug("oAuth : Got Profile");

				// Login with the profile
				Logger.Debug("oAuth : Logging into application using profile: " + profile.Id + " - " + profile.ScreenName);
				var userContext = ServiceClientLocator.AuthenticationClient(App).LogIn(profile, App.Settings.Module);

				App.UserData.Load(userContext.UserData);

				var authTicket = new FormsAuthenticationTicket(1, userContext.UserContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, userContext.UserContext.SerializeUserContext());
				var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

				var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
				Response.Cookies.Add(authCookie);

				Logger.Debug("oAuth : Authenticated");
				Response.Redirect(UrlBuilder.Home(), false);
			}
			catch (Exception ex)
			{
				Logger.Debug("oAuth : Exception: " + ex);
				throw;
			}
		}
	}
}