﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReactivateAccount.aspx.cs" Inherits="Focus.CareerExplorer.WebAuth.ReactivateAccount" %>

<%@ Register Src="Controls/AccountReactivationModal.ascx" TagName="AccountReactivationModal" TagPrefix="uc" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
	<uc:AccountReactivationModal ID="AccountReactivation" runat="server" OnSubmitted="AccountReactivation_OnSubmitted" />
</asp:Content>