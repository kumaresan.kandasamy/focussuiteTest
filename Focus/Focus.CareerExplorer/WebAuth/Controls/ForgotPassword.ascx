﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.ForgotPassword" %>

<asp:HiddenField ID="ForgotPasswordModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ForgotPasswordModal" runat="server" TargetControlID="ForgotPasswordModalDummyTarget" PopupControlID="ForgotPasswordPanel" BackgroundCssClass="modalBackground" BehaviorID="ForgotPasswordModal" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:Panel ID="ForgotPasswordPanel" runat="server" CssClass="modal">
	<div class="lightbox FocusCareer forgotPasswordModal">
		<div class="modalHeader">
			<input type="image" id="forgotPasswordCloseIcon" src="<%= UrlBuilder.Close() %>" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
			<focus:LocalisedLabel runat="server" ID="ForgotPasswordTitleLabel" LocalisationKey="ForgotPassword.Label" DefaultText="Forgot Your Password test?" CssClass="modalTitle modalTitleLarge"/>
		</div>
		<focus:LocalisedLabel runat="server" ID="PasswordHelpInfoLabel" CssClass="modalMessage" LocalisationKey="PasswordHelpInfo.Text" />
		<div class="dataInputContainer">
			<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailAddressLabel" CssClass="dataInputLabel" AssociatedControlID="ForgotPasswordEmailAddressTextbox" LocalisationKey="EmailAddress" DefaultText="Email address"/>
				<span class="dataInputField">
					<asp:TextBox ID="ForgotPasswordEmailAddressTextbox" runat="server" ClientIDMode="Static" Width="100%"  MaxLength="100" AutoCompleteType="Disabled" />
					<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="ForgotPasswordEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
					<asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="ForgotPasswordEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
				</span>
			</div>
			<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailAddressConfirmationLabel" CssClass="dataInputLabel" AssociatedControlID="ForgotPasswordConfirmEmailAddressTextbox" LocalisationKey="EmailAddressConfirmation" DefaultText="Re-enter email address"/>
				<span class="dataInputField">
					<asp:TextBox ID="ForgotPasswordConfirmEmailAddressTextbox" runat="server" ClientIDMode="Static" Width="100%"  MaxLength="100" AutoCompleteType="Disabled" />
					<asp:RequiredFieldValidator ID="ConfirmEmailAddressRequired" runat="server" ControlToValidate="ForgotPasswordConfirmEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
					<asp:CompareValidator ID="ConfirmEmailAddressCompare" runat="server" ControlToValidate="ForgotPasswordConfirmEmailAddressTextbox" ControlToCompare="ForgotPasswordEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
				</span>
			</div>
		</div>
		<asp:Label ID="lblResetPassword" runat="server" Visible="false" ClientIDMode="Static"></asp:Label>
		<div class="modalButtons"><asp:Button ID="btnRequestPassword" runat="server" class="buttonLevel2" ValidationGroup="PasswordHelp" OnClick="btnRequestPassword_Click" /></div>
	</div>
</asp:Panel>

<asp:HiddenField ID="ForgotPasswordModal2DummyTarget" runat="server" />
<act:ModalPopupExtender ID="ForgotPasswordModal2" runat="server" TargetControlID="ForgotPasswordModal2DummyTarget" PopupControlID="ForgotPasswordPanel2" BackgroundCssClass="modalBackground" BehaviorID="ForgotPasswordModal2" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:Panel ID="ForgotPasswordPanel2" runat="server" CssClass="modal">
	<div style="width: 100%;">
		<asp:Panel ID="ForgotPasswordHeader2" runat="server" CssClass="modal-header">
			<focus:LocalisedLabel runat="server" ID="ForgotPasswordTitleLabel2" LocalisationKey="ForgotPassword.Label" DefaultText="Forgot Your Password?" CssClass="modalTitle modalTitleLarge"/>
		</asp:Panel>
		<p>
			<%= HtmlLocalise("Notification.Text", "Please enter your username or email address below to help us locate your account.")%></p>
			<focus:LocalisedLabel runat="server" ID="ErrorDescription2" CssClass="modalMessage" LocalisationKey="PasswordHelpInfo.Text" />
		<p>
			<span class="inFieldLabel">
				<%= HtmlLabel(ForgotPasswordUserNameTextBox2, "UserName.Label", "username")%>
				<asp:TextBox ID="ForgotPasswordUserNameTextBox2" runat="server" ClientIDMode="Static" Width="275px" />
			</span>
		</p>
		<p>
			<%= HtmlLocalise("Or.Text", "or")%></p>
		<p>
			<span class="inFieldLabel">
				<%= HtmlLabel(ForgotPasswordEmailAddressTextBox2, "EmailAddress.Label", "email address")%>
				<asp:TextBox ID="ForgotPasswordEmailAddressTextBox2" runat="server" ClientIDMode="Static" Width="275px" />
                
		<asp:CustomValidator	ID="EitherFieldValidator" runat="server" 
													ErrorMessage="Please enter at least a username or email address" 
													Text="*"
													ValidationGroup="ForgotPassword2" 
													ClientValidationFunction="ValidateEitherFieldPresent"
													OnServerValidate="EitherFieldValidator2_OnServerValidate" />

		<asp:CustomValidator	ID="NotBothFieldValidator" 
													runat="server" 
													ValidationGroup="ForgotPassword2"
													ErrorMessage="Please enter a valid username or email address, not both"
													Text="*"
													ClientValidationFunction="NotBothFieldsPresent"
													OnServerValidate="NotBothFieldsValidator_OnServerValidate"  />
        <asp:RegularExpressionValidator ID="EmailAddressRegEx2" 
                                                           runat="server" ErrorMessage ="Email address is not correct format"
                                                           ControlToValidate="ForgotPasswordEmailAddressTextBox2" Display="None"
                                                            CssClass="error" ValidationGroup="ForgotPassword2" />
			</span>
		</p>
		<p>
			<a href="<%= UrlBuilder.Home() %>">
				<%= HtmlLocalise("Login.Text", "Log in")%>
			</a>
		</p>
		<asp:Label ID="lblResetPassword2" runat="server" Visible="false" ClientIDMode="Static"></asp:Label>


		<focus:AjaxValidationSummary ID="ForgotPasswordValidationSummary2" runat="server" DisplayMode="List" CssClass="error" ValidationGroup="ForgotPassword2" />
		<div style="width: 100%; text-align: right; margin-top: 10px;">
			<asp:Button ID="SubmitButton2" runat="server" CssClass="buttonLevel2" OnClick="SubmitButton2_Click" ValidationGroup="ForgotPassword2" />
		</div>
	</div>
</asp:Panel>

<asp:HiddenField ID="ForgotPasswordModal3DummyTarget" runat="server" />
<act:ModalPopupExtender ID="ForgotPasswordModal3" runat="server" TargetControlID="ForgotPasswordModal3DummyTarget" PopupControlID="ForgotPasswordPanel3" BackgroundCssClass="modalBackground" BehaviorID="ForgotPassword3Modal" RepositionMode="RepositionOnWindowResizeAndScroll" />

<asp:Panel ID="ForgotPasswordPanel3" runat="server" CssClass="modal">
	<div class="lightbox FocusCareer forgotPasswordModal">
		<div class="modalHeader">
			<focus:LocalisedLabel runat="server" ID="ForgotPasswordTitleLabel3" LocalisationKey="ForgotPassword.Label" DefaultText="Forgot Your Password?" CssClass="modalTitle modalTitleLarge"/>
		</div>
		<focus:LocalisedLabel runat="server" ID="PasswordHelpInfoLabel3" CssClass="modalMessage" LocalisationKey="PasswordHelpInfo.Text" />
		<div class="dataInputContainer">
      <asp:PlaceHolder runat="server" ID="ExtraQuestionsPlaceHolder3">
          <div class="career-scroll-pane">
			  <div class="dataInputRow">
				  <focus:LocalisedLabel runat="server" ID="FPSecurityQuestionDropDownLocalisedLabel3" CssClass="dataInputLabel" AssociatedControlID="FPSecurityQuestionDropDown3" LocalisationKey="SelectSecurityQuestionLocalisationKey" DefaultText="Security question"/>
				  <span class="dataInputField">
					  <div style="position: relative;">
						  <asp:DropDownList ID="FPSecurityQuestionDropDown3" ClientIDMode="Static" runat="server"  Width="100%" />
						  <asp:RequiredFieldValidator runat="server" InitialValue="0" ID="SecurityQuestionRequiredFieldValidator3" ControlToValidate="FPSecurityQuestionDropDown3" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ForgotPassword3" />
					  </div>
					  <div id="FPSecurityQuestionDiv3" style="display: none; padding-top: 8px;">
						  <asp:TextBox ID="FPSecurityQuestionTextBox3" runat="server" title="FP Security Question TextBox 3" ClientIDMode="Static" MaxLength="100" Width="100%" />
						  <asp:RequiredFieldValidator runat="server" ID="SecurityQuestionTextBoxRequiredFieldValidator3" ControlToValidate="FPSecurityQuestionTextBox3" ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ForgotPassword3" />
					  </div>
				  </span>
			  </div>
			  <div class="dataInputRow">
				  <focus:LocalisedLabel runat="server" ID="SecurityAnswerTextBoxLocalisedLabel3" CssClass="dataInputLabel" AssociatedControlID="SecurityAnswerTextBox3" LocalisationKey="SelectSecuritAnswerLocalisationKey" DefaultText="Enter security answer"/>
				  <span class="dataInputField">
					  <asp:TextBox ID="SecurityAnswerTextBox3" runat="server" Width="100%"  MaxLength="100" AutoCompleteType="Disabled" />
					  <asp:RequiredFieldValidator ID="SecurityAnswerTextBoxRequiredFieldValidator3" runat="server" ControlToValidate="SecurityAnswerTextBox3" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ForgotPassword3" />
				  </span>
			  </div>
			  <div class="dataInputRow">
				  <focus:LocalisedLabel runat="server" ID="DayOfBirthLocalisedLabel3" CssClass="dataInputLabel" AssociatedControlID="DayOfBirthTextBox3" LocalisationKey="DayOfBirthTextBoxLabel" DefaultText="Enter day of birth <em>dd</em>"/>
				  <span class="dataInputField">
					  <asp:TextBox ID="DayOfBirthTextBox3" runat="server" ClientIDMode="Static" Width="35"  MaxLength="2" AutoCompleteType="Disabled" />
					  <span>
              <asp:RequiredFieldValidator ID="DayOfBirthTextBoxRequiredFieldValidator3" runat="server" ControlToValidate="DayOfBirthTextBox3" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ForgotPassword3" />
					    <asp:RegularExpressionValidator ID="DayOfBirthTextBoxRegularExpressionValidator3" ValidationExpression="(0[1-9]|[12]\d|3[01])" runat="server" ControlToValidate="DayOfBirthTextBox3" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ForgotPassword3" />
				    </span>
          </span>
			  </div>
			  <div class="dataInputRow">
				  <focus:LocalisedLabel runat="server" ID="MonthOfBirthLocalisedLabel3" CssClass="dataInputLabel" AssociatedControlID="MonthOfBirthDropDownList3" LocalisationKey="MonthOfBirthLocalisedLabel" DefaultText="Select month of birth"/>
          <span class="dataInputField">
				    <asp:DropDownList runat="server" ID="MonthOfBirthDropDownList3"  Width="140"/>
            <span>
				      <asp:RequiredFieldValidator ID="MonthOfBirthRequiredFieldValidator3" runat="server" ControlToValidate="MonthOfBirthDropDownList3" InitialValue="0" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ForgotPassword3" />
            </span>
          </span>
			  </div>
              </div>
				<a href="<%= UrlBuilder.Home() %>">
					<%= HtmlLocalise("Login.Text", "Log in")%>
				</a>
      </asp:PlaceHolder>
		</div>
		<asp:Label ID="lblResetPassword3" runat="server" Visible="false" ClientIDMode="Static"></asp:Label>
		<div class="modalButtons"><asp:Button ID="btnRequestPassword3" runat="server"  ValidationGroup="ForgotPassword3" CssClass="buttonLevel2" OnClick="btnRequestPassword3_Click" /></div>
	</div>
</asp:Panel>

<script type="text/javascript">
  Sys.Application.add_load(ForgotPassword_PageLoad);

  function ForgotPassword_PageLoad(sender, args) {
	  $('#forgotPasswordCloseIcon').click(function() {
		  $find('ForgotPasswordModal').hide();

		  if ('<%= App.Settings.EnableCareerPasswordSecurity %>') {
			  $find('ForgotPasswordModal').hide();
		  }

		  $("#lblResetPassword").text('');
		  $("#ForgotPasswordEmailAddressTextbox").val('');
		  $("#ForgotPasswordConfirmEmailAddressTextbox").val('');

		  $("#lblResetPassword3").text('');
		  $("#ForgotPasswordEmailAddressTextbox3").val('');
		  $("#ForgotPasswordConfirmEmailAddressTextbox3").val('');

		  $find('LogInModal').show();
		  return false;
	  });
	
	  var questionDropdown3 = $("#FPSecurityQuestionDropDown3");
	  if (questionDropdown3.exists()) {
		  $("#FPSecurityQuestionDropDown3").change(function () {
			  ForgotPassword3_ValidatorEnable();
		  });
		  ForgotPassword3_ValidatorEnable();
	  }
  }

		function ForgotPassword3_ValidatorEnable() {
			if ($("#FPSecurityQuestionDropDown3").val() == "<%= CreateOwnSecurityQuestionValue %>") {
				$("#FPSecurityQuestionDiv3").show();
				ValidatorEnable(document.getElementById('SecurityQuestionTextBoxRequiredFieldValidator3'), true);
			} else {
				$("#FPSecurityQuestionDiv3").hide();
				ValidatorEnable(document.getElementById('SecurityQuestionTextBoxRequiredFieldValidator3'), false);
			}
		}

		function ValidateEitherFieldPresent(sender, args) {
		  var userNameEntered = $("#ForgotPasswordUserNameTextBox2").val().trim().length > 0;
		  var emailAddressEntered = $("#ForgotPasswordEmailAddressTextBox2").val().trim().length > 0;

			args.IsValid = (userNameEntered || emailAddressEntered);
		}

		function NotBothFieldsPresent(sender, args) {
		  var userNameEntered = $("#ForgotPasswordUserNameTextBox2").val().trim().length > 0;
		  var emailAddressEntered = $("#ForgotPasswordEmailAddressTextBox2").val().trim().length > 0;

			args.IsValid = !(userNameEntered && emailAddressEntered);
		}

</script>
