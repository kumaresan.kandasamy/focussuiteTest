﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class ChangePassword : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				NewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
				LocaliseUI();
			}

			if (App.User.IsShadowingUser)
			{
				PasswordTextBox.Attributes["value"] = "********";
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Clicked(object sender, EventArgs e)
		{
			var currentPassword = (PasswordTextBox.Text ?? string.Empty).Trim();
			var newPassword = (NewPasswordTextBox.Text ?? string.Empty).Trim();

			try
			{
				ServiceClientLocator.AccountClient(App).ChangePassword(currentPassword, newPassword);

				ConfirmationModal.Show(CodeLocalise("PasswordSuccessfulChange.Title", "Password Changed"),
															 CodeLocalise("PasswordSuccessfulChange.Body", "You have successfully changed your password."),
															 CodeLocalise("Global.Close.Text", "Close"));
			}
			catch (Exception ex)
			{
				ChangePasswordValidator.IsValid = false;
				ChangePasswordValidator.ErrorMessage = ex.Message;
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			PasswordRequired.ErrorMessage = CodeLocalise("Password.RequiredErrorMessage", "Password is required");
			NewPasswordRequired.ErrorMessage = CodeLocalise("NewPassword.RequiredErrorMessage", "New password is required");
			NewPasswordRegEx.ErrorMessage = CodeLocalise("NewPasswordTextBox.RegExErrorMessage", "New password must be 6-20 characters and contain at least 1 uppercase and 1 lowercase character.");
			ConfirmNewPasswordRequired.ErrorMessage = CodeLocalise("ConfirmNewPassword.RequiredErrorMessage", "Confirm password is required");
			ConfirmNewPasswordCompare.ErrorMessage = CodeLocalise("ConfirmNewPasswordCompare.CompareErrorMessage", "Passwords must match");
			SaveButton.Text = CodeLocalise("Global.Save.Text", "Save");
		}
	}
}