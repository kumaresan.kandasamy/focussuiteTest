﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.Security;
using System.Linq;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class LogOut : UserControlBase
	{
		public event EventHandler LoggedOut;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

		/// <summary>
		/// Shows the log out modal.
		/// </summary>
		public void Show()
		{
			LogOutModal.Show();
		}

		/// <summary>
		/// Handles the Click event of the CompleteLogOutButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void CompleteLogOutButton_Click(object sender, EventArgs e)
		{
			LogOutSession();
		}

		/// <summary>
		/// Logs the out session.
		/// </summary>
		/// <param name="isRedirectToLogin">if set to <c>true</c> [is redirect to login].</param>
		/// <param name="alreadyLogout">if set to <c>true</c> [already logout].</param>
		public void LogOutSession(bool isRedirectToLogin = true, bool alreadyLogout = false)
		{
            bool redirectSecondarySSO = false;
            if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer)
            {
                var user = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
                if (!App.Settings.SamlSecExternalClientTag.Equals(string.Empty) && user.IsNotNull())
                    redirectSecondarySSO = App.Settings.SamlSecExternalClientTag.Split(',').ToList<string>().Any(t => user.UserDetails.ExternalId.StartsWith(t));
            }
			try
			{
				if (App.Settings.Module == FocusModules.Explorer)
				{
					ServiceClientLocator.AuthenticationClient(App).LogOut();
				}
				else
				{
					if (!alreadyLogout)
					{
						var usercontext = App.GetSessionValue<UserContext>("Career:UserContext");
						if (usercontext.IsNotNull())
						{
							#region Save Activity

							//TODO: Martha (new activity functionality)
							//var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
							//if (staffcontext.IsNull() || (staffcontext.IsNotNull() && !staffcontext.IsAuthenticated))
							//{
							//  var jobSeekerActivities = CareerCommon.ServiceClient.ServiceClientLocator.CoreClient(App).GetJobSeekerSessionActivities();
							//  if (jobSeekerActivities.IsNotNull())
							//  {
							//    string activityLog = string.Format(@"Session Duration: {0} minutes\nSearches: {1}\nJobs Viewed: {2}\nReferrals Requested: {3}\nResume Updated: {4}",
							//      Math.Ceiling(DateTime.Now.Subtract(jobSeekerActivities.SessionStartTime).TotalMinutes),
							//      jobSeekerActivities.SearchesMade,
							//      jobSeekerActivities.JobsViewed,
							//      jobSeekerActivities.ReferralsRequested,
							//      jobSeekerActivities.IsResumeUpdated);
							//    ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.JobSeeker, ActivityType.Automated, "SMRY01", activityLog);
							//    CareerCommon.ServiceClient.ServiceClientLocator.CoreClient(App).ClearJobSeekerSessionActivities();
							//  }
						}

							#endregion

						ServiceClientLocator.AuthenticationClient(App).LogOut();
					}
				}
			}
			catch(Exception ex)
			{
				Logger.Error(ex.Message, ex);
			}
			finally
			{
				FormsAuthentication.SignOut();
				App.ClearSession();
				LoggedOut(this, EventArgs.Empty);
			}

      if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer)
      {
          if (redirectSecondarySSO)
              Response.Redirect(App.Settings.SamlLogoutRedirect, true);
          else
              Response.Redirect(App.Settings.SSOReturnUrl, true);
          return;
      }

			if (!isRedirectToLogin) return;

			if (!String.IsNullOrEmpty(App.Settings.DemoShieldCustomerCode) &&
			    !String.IsNullOrEmpty(App.Settings.DemoShieldRedirectUrl))
				Response.Redirect(App.Settings.DemoShieldRedirectUrl, true);
			else
			{
				if (App.Settings.LogoutRedirectUrl.IsNotNullOrEmpty())
					Response.Redirect(App.Settings.LogoutRedirectUrl, true);
				else
				{
					Response.Redirect(UrlBuilder.Default(), true);
					var login = (LogInOrRegister)MasterPage.FindControl("MainLogIn");
					login.ShowLogIn();
				}
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			CompleteLogOutButton.Text = CodeLocalise("Global.LogOut.Text.NoEdit", "Sign out");
		}
	}
}
