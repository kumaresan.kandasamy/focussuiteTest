﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyAccount.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.MyAccount" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<asp:HiddenField ID="MyAccountModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="MyAccountModal" runat="server" BehaviorID="MyAccountModal"
	TargetControlID="MyAccountModalDummyTarget" PopupControlID="MyAccountPanel" BackgroundCssClass="modalBackground"
	RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel runat="server" ID="MyAccountPanel" TabIndex="-1" class="modal myAccountModal career-scroll-pane"
	Style="z-index: 100001; display: none;">
	<%--<div id="MyAccountPanel" class="modal myAccountModal" style="z-index: 100001; display: none;">--%>
	<div class="modalHeader">
		<input type="image" src="<%= UrlBuilder.Close() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>"
			width="20" height="20" class="modalCloseIcon" onclick="$find('MyAccountModal').hide();return false;" />
		<focus:LocalisedLabel runat="server" ID="MyAccountHeadingLabel" LocalisationKey="My Account.Label"
			DefaultText="Update your login credentials" CssClass="modalTitle modalTitleLarge" />
	</div>
	<div class="collapsiblePanel collapsibleSingle cpCollapsed" id="UsernameDiv" runat="server">
		<div class="collapsiblePanelHeader" style="font-size:14px">
			<div class="cpIcon">
			</div>
			<span id="ChangeUsernameHeadingLabel" class="collapsiblePanelHeaderLabel cpHeaderControl" style="left:25px">
				<a id="ChangeUsernameHeadingLink" href="#">
					<%= HtmlLocalise("AccordionHeading1.Label", "Change Username")%></a></span>
		</div>
		<div class="collapsiblePanelContent">
			<div class="dataInputContainer">
				<asp:PlaceHolder runat="server" ID="UsernameTextBoxesPlaceHolder">
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="OldUsernameLabel" AssociatedControlID="txtOldUsername"
							LocalisationKey="OldUsername.Text" DefaultText="Old username" CssClass="dataInputLabel requiredData" />
						<span class="dataInputField">
							<asp:TextBox ID="txtOldUsername" runat="server" ReadOnly="true" ClientIDMode="Static"></asp:TextBox>
						</span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="NewUsernameLabel" AssociatedControlID="txtNewUsername"
							LocalisationKey="NewUsername.Text" DefaultText="New username" CssClass="dataInputLabel requiredData" />
						<span class="dataInputField">
							<asp:TextBox ID="txtNewUsername" runat="server" ClientIDMode="Static"></asp:TextBox>
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="UsernameRequired" runat="server" ControlToValidate="txtNewUsername"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateUsername" />
							</span><span class="dataInputFieldSubContainer">
								<asp:RegularExpressionValidator ID="NewUsernameRegEx" runat="server" ControlToValidate="txtNewUsername"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateUsername" />
								<asp:RegularExpressionValidator ID="NewUserNameMinLengthRegEx" runat="server" ControlToValidate="txtNewUsername"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateUsername" />
							</span></span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="ReNewUsernameLabel" AssociatedControlID="txtReNewUsername"
							LocalisationKey="ReNewUsername.Text" DefaultText="Confirm username" CssClass="dataInputLabel requiredData" />
						<span class="dataInputField">
							<asp:TextBox ID="txtReNewUsername" runat="server" ClientIDMode="Static"></asp:TextBox>
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="ConfirmUsernameRequired" runat="server" ControlToValidate="txtReNewUsername"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateUsername" />
								<asp:CompareValidator ID="ConfirmUsernameCompare" runat="server" ControlToValidate="txtReNewUsername"
									ControlToCompare="txtNewUsername" SetFocusOnError="true" Display="Dynamic" CssClass="error"
									ValidationGroup="UpdateUsername" />
							</span></span>
					</div>
				</asp:PlaceHolder>
				<div class="modalButtons">
					<asp:Button ID="UpdateUsernameButton" runat="server" class="buttonLevel2" ValidationGroup="UpdateUsername" Title="UpdateUsernameButton"
						OnClick="UpdateUsernameButton_Click" /></div>
			</div>
		</div>
	</div>
	<div class="collapsiblePanel collapsibleSingle cpCollapsed" id="PasswordDiv" runat="server">
		<div class="collapsiblePanelHeader" style="font-size:14px">
			<div class="cpIcon">
			</div>
			<span id="ChangePasswordHeadingLabel" class="collapsiblePanelHeaderLabel cpHeaderControl" style="left:25px">
				<a href="#">
					<%= HtmlLocalise("AccordionHeading2.Label", "Change Password")%></a></span>
		</div>
		<div class="collapsiblePanelContent">
			<div class="dataInputContainer">
				<asp:PlaceHolder runat="server" ID="PasswordTextBoxesPlaceHolder">
                <span class="instructions"><focus:LocalisedLabel ID="LocalisedLabel1" runat="server" DefaultText="Password is case-sensitive." /></span>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="OldPasswordLabel" AssociatedControlID="txtOldPassword" 
							CssClass="dataInputLabel requiredData" LocalisationKey="OldPassword.Text" DefaultText="Old password" />
						<span class="dataInputField">
							<asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" autocomplete="off" width="210px"></asp:TextBox>
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="OldPasswordRequired" runat="server" ControlToValidate="txtOldPassword"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdatePassword" />
							</span></span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="NewPasswordLabel" AssociatedControlID="txtNewPassword"
							CssClass="dataInputLabel requiredData" LocalisationKey="NewPassword.Text" DefaultText="New password" />
						<span class="dataInputField">
							<asp:TextBox ID="txtNewPassword" runat="server" MaxLength="20" TextMode="Password" width="210px"
								autocomplete="off"></asp:TextBox>
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="txtNewPassword"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdatePassword" />
							</span><span class="dataInputFieldSubContainer">
								<asp:RegularExpressionValidator ID="NewPasswordRegEx" runat="server" ControlToValidate="txtNewPassword"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdatePassword" />
							</span><span class="dataInputFieldSubContainer">
								<asp:CompareValidator ID="UserNameAndPasswordCompare" runat="server" ControlToValidate="txtNewPassword"
									ControlToCompare="txtOldUsername" SetFocusOnError="true" Display="Dynamic" Operator="NotEqual"
									CssClass="error" ValidationGroup="UpdatePassword" />
							</span><span class="dataInputFieldSubContainer">
								<asp:RegularExpressionValidator ID="PasswordCharRegEx" runat="server" ControlToValidate="txtNewPassword"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdatePassword" />
							</span></span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="ReNewPasswordLabel" AssociatedControlID="txtReNewPassword"
							CssClass="dataInputLabel requiredData" LocalisationKey="ReNewPassword.Text" DefaultText="Confirm password" />
						<span class="dataInputField">
							<asp:TextBox ID="txtReNewPassword" runat="server" MaxLength="20" TextMode="Password" width="210px"
								autocomplete="off"></asp:TextBox>
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="txtReNewPassword"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdatePassword" />
							</span><span class="dataInputFieldSubContainer">
								<asp:CompareValidator ID="ConfirmNewPasswordCompare" runat="server" ControlToValidate="txtReNewPassword"
									ControlToCompare="txtNewPassword" SetFocusOnError="true" Display="Dynamic" CssClass="error"
									ValidationGroup="UpdatePassword" />
							</span></span>
					</div>
				</asp:PlaceHolder>
				<div class="modalButtons">
					<asp:Button ID="UpdatePasswordButton" runat="server" class="buttonLevel2" OnClick="UpdatePasswordButton_Click" Title="UpdatePasswordButton"
						ValidationGroup="UpdatePassword" /></div>
			</div>
		</div>
	</div>
	<asp:PlaceHolder runat="server" ID="SecurityQuestionsPlaceHolder">
		<div class="collapsiblePanel collapsibleSingle cpCollapsed" id="SecurityQuestionDiv"
			runat="server">
			<div class="collapsiblePanelHeader" style="font-size:14px">
				<div class="cpIcon">
				</div>
				<span id="ChangeSecurityHeadingLabel" class="collapsiblePanelHeaderLabel cpHeaderControl" style="left:25px">
					<a href="#">
						<%= HtmlLocalise("AccordionHeading3.Label", "Change Security Question/Answer")%></a></span>
			</div>
			<div class="collapsiblePanelContent">
				<div class="dataInputContainer">
                <span class="instructions"><focus:LocalisedLabel ID="SecurityAnswerInstructionsLabel" runat="server" DefaultText="Answer is case-sensitive" /></span>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="OldSecurityQuestionLabel" AssociatedControlID="OldSecurityQuestionTextBox"
							CssClass="dataInputLabel requiredData" LocalisationKey="OldSecurityQuestion" DefaultText="Old security question" />
						<span class="dataInputField">
							<asp:TextBox ID="OldSecurityQuestionTextBox" runat="server" Width="286px" ReadOnly="true"
								Enabled="false"></asp:TextBox>
						</span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="OldSecurityAnswerLabel" AssociatedControlID="OldSecurityAnswerTextBox"
							CssClass="dataInputLabel requiredData" LocalisationKey="OldSecurityAnswer" DefaultText="Old security answer" />
						<span class="dataInputField">
							<asp:TextBox ID="OldSecurityAnswerTextBox" runat="server" Width="286px" ReadOnly="true"
								Enabled="false"></asp:TextBox>
						</span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="SecurityQuestionLabel" AssociatedControlID="UpdateSecurityQuestionDropDown"
							CssClass="dataInputLabel requiredData" LocalisationKey="SecurityQuestion" DefaultText="New security question" />
						<span class="dataInputField">
							<asp:DropDownList ID="UpdateSecurityQuestionDropDown" runat="server" ClientIDMode="Static"
								Width="290px" />
							<span class="dataInputFieldSubContainer" id="UpdateSecurityQuestionDiv" style="display: none;
								padding-top: 8px;">
								<asp:TextBox ID="UpdateSecurityQuestionTextBox" runat="server" ClientIDMode="Static"
									Width="286px" MaxLength="100" Title="Updated Security Question Text"/>
								<span class="dataInputFieldSubContainer">
									<asp:CustomValidator ID="UpdateSecurityQuestionCustomValidator" runat="server" ControlToValidate="UpdateSecurityQuestionTextBox"
										ClientValidationFunction="validateOwnUpdateSecurityQuestion" ValidationGroup="UpdateSecurityQuestion"
										ValidateEmptyText="True" CssClass="error" SetFocusOnError="true" Display="Dynamic" /></span>
							</span><span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="UpdateSecurityQuestionRequired" runat="server" ControlToValidate="UpdateSecurityQuestionDropDown"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateSecurityQuestion" /></span>
						</span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="SecurityAnswerLabel" AssociatedControlID="UpdateSecurityAnswerTextBox"
							CssClass="dataInputLabel requiredData" LocalisationKey="SecurityAnswer" DefaultText="New security answer" />
						<span class="dataInputField">
							<asp:TextBox ID="UpdateSecurityAnswerTextBox" runat="server" ClientIDMode="Static"
								Width="286px" MaxLength="30" />
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="UpdateSecurityAnswerRequired" runat="server" ControlToValidate="UpdateSecurityAnswerTextBox"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateSecurityQuestion" />
								<asp:CompareValidator ID="UpdateSecurityAnswerDiffenceValidator" runat="server" ControlToValidate="UpdateSecurityAnswerTextBox"
									ControlToCompare="OldSecurityAnswerTextBox" SetFocusOnError="true" Display="Dynamic"
									CssClass="error" Operator="NotEqual" ValidationGroup="UpdateSecurityQuestion" />
							</span></span>
					</div>
					<div class="modalButtons">
						<asp:Button ID="UpdateSecurityQuestion" runat="server" class="buttonLevel2" ValidationGroup="UpdateSecurityQuestion" title="UpdateSecurityQuestion"
							OnClick="UpdateSecurityQuestion_Click" /></div>
				</div>
			</div>
		</div>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="EducationPlaceHolder1" Visible="false">
		<div class="collapsiblePanel collapsibleSingle cpCollapsed">
			<div class="collapsiblePanelHeader" style="font-size:14px">
				<div class="cpIcon">
				</div>
				<span id="ChangeProgramOfStudyLabel" class="collapsiblePanelHeaderLabel cpHeaderControl">
					<a href="#">
						<%= HtmlLocalise("AccordionHeading4.Label", "Change Program Of Study")%></a></span>
			</div>
			<div class="collapsiblePanelContent">
				<div class="dataInputContainer">
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="OldProgramOfStudyLabel" AssociatedControlID="txtOldProgramOfStudy"
							CssClass="dataInputLabel" LocalisationKey="OldStudyProgram.Text" DefaultText="Old program of study" />
						<span class="dataInputField">
							<asp:TextBox ID="txtOldProgramOfStudy" runat="server" Width="330px" ReadOnly="true"
								Enabled="false"></asp:TextBox>
						</span>
					</div>
					<asp:PlaceHolder runat="server" ID="ProgramOfStudySelectionPlaceHolder">
						<div class="dataInputRow">
							<focus:LocalisedLabel runat="server" ID="NewProgramOfStudyLabel" CssClass="dataInputLabel requiredData"
								AssociatedControlID="UpdateProgramOfStudyAreaDropDown" LocalisationKey="NewStudyProgram.Text"
								DefaultText="New program of study" Width="350" />
							<span class="dataInputField">
								<asp:DropDownList runat="server" ID="UpdateProgramOfStudyAreaDropDown" Width="350" />
								<span class="dataInputFieldSubContainer">
									<asp:RequiredFieldValidator ID="ProgramOfStudyRequired" runat="server" ControlToValidate="UpdateProgramOfStudyAreaDropDown"
										SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateProgramOfStudy" />
								</span></span><span class="dataInputField" style="margin-top: 8px">
									<focus:AjaxDropDownList runat="server" ID="UpdateProgramOfStudyAreaDegreesDropDown"
										Width="350" />
									<act:CascadingDropDown ID="DegreeDropDownListCascadingDropDown" BehaviorID="DegreeDropDownListCascadingDropDownBehavior"
										runat="server" TargetControlID="UpdateProgramOfStudyAreaDegreesDropDown" ParentControlID="UpdateProgramOfStudyAreaDropDown"
										ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetDegreeEducationLevelsByProgramArea"
										Category="DegreesWithUndeclared" LoadingText="[Loading degrees...]" PromptText="- select degree -" />
									<span class="dataInputFieldSubContainer">
										<asp:RequiredFieldValidator ID="DegreeRequired" runat="server" ControlToValidate="UpdateProgramOfStudyAreaDegreesDropDown"
											SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateProgramOfStudy" />
									</span></span>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder runat="server" ID="DegreeSelectionPlaceholder">
						<div class="dataInputRow">
							<asp:DropDownList runat="server" ID="DetailedDegreeLevelDropDownList" Width="350" />
						</div>
						<div class="dataInputRow">
							<focus:AjaxDropDownList ID="DetailedDegreeAjaxDropDownList" runat="server" Width="308px"
								CssClass="DetailedDegreeClass" ClientIDMode="Static" TabIndex="8" />
							<ajaxtoolkit:CascadingDropDown ID="DetailedDegreeCascadingDropDown" runat="server"
								Category="ClientDataOnly" LoadingText="[Loading degrees...]" ParentControlID="DetailedDegreeLevelDropDownList"
								ServiceMethod="GetDegreeEducationLevelsByDegreeLevel" ServicePath="~/Services/AjaxService.svc"
								BehaviorID="DetailedDegreeCascadingDropDownBehavior" TargetControlID="DetailedDegreeAjaxDropDownList">
							</ajaxtoolkit:CascadingDropDown>
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="DetailedDegreeAutoCompleteTextBoxValidator" runat="server"
									ControlToValidate="DetailedDegreeAjaxDropDownList" SetFocusOnError="true" Display="Dynamic"
									CssClass="error" ValidationGroup="UpdateProgramOfStudy" />
							</span>
						</div>
					</asp:PlaceHolder>
					<div class="modalButtons">
						<asp:Button ID="UpdateProgramOfStudyButton" runat="server" class="buttonLevel2" OnClick="UpdateProgramOfStudyButton_Click" title="UpdateProgramOfStudyButton"
							ValidationGroup="UpdateProgramOfStudy" />
					</div>
				</div>
			</div>
		</div>
		<div class="collapsiblePanel collapsibleSingle cpCollapsed" id="MyAccountSection_Enrollment"
			clientidmode="Static">
			<div class="collapsiblePanelHeader">
				<div class="cpIcon">
				</div>
				<span id="ChangeEnrollmentStatusLabel" class="collapsiblePanelHeaderLabel cpHeaderControl">
					<a href="#">
						<%= HtmlLocalise("AccordionHeading.Label", "Change Enrollment Status")%></a></span>
			</div>
			<div class="collapsiblePanelContent">
				<div class="dataInputContainer">
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="OldEnrollmentStatusLabel" AssociatedControlID="txtOldEnrollmentStatus"
							CssClass="dataInputLabel" LocalisationKey="OldEnrollmentStatus.Text" DefaultText="Old enrollment status" />
						<span class="dataInputField">
							<asp:TextBox ID="txtOldEnrollmentStatus" runat="server" Width="300px" ReadOnly="true"
								Enabled="false"></asp:TextBox>
						</span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="NewEnrollmentStatusLabel" CssClass="dataInputLabel requiredData"
							AssociatedControlID="UpdateEnrollmentStatusDropDown" LocalisationKey="NewEnrollmentStatus.Text"
							DefaultText="New enrollment status" />
						<span class="dataInputField">
							<asp:DropDownList runat="server" ID="UpdateEnrollmentStatusDropDown" Width="305px" />
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="EnrollmentStatusRequired" runat="server" ControlToValidate="UpdateEnrollmentStatusDropDown"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateEnrollmentStatus" />
							</span></span>
					</div>
					<div class="modalButtons">
						<asp:Button ID="UpdateEnrollmentStatusButton" runat="server" class="buttonLevel2" title="UpdateEnrollmentStatusButton"
							OnClick="UpdateEnrollmentStatusButton_Click" ValidationGroup="UpdateEnrollmentStatus" />
					</div>
				</div>
			</div>
		</div>
		<asp:PlaceHolder runat="server" ID="CampusSection">
			<div class="collapsiblePanel collapsibleSingle cpCollapsed">
				<div class="collapsiblePanelHeader">
					<div class="cpIcon">
					</div>
					<span id="ChangeCampusLabel" class="collapsiblePanelHeaderLabel cpHeaderControl"><a
						href="#">
						<asp:Literal runat="server" ID="ChangeCampusLiteral"></asp:Literal>
					</a></span>
				</div>
				<div class="collapsiblePanelContent">
					<div class="dataInputContainer">
						<div class="dataInputRow">
							<focus:LocalisedLabel runat="server" ID="CampusLabel" AssociatedControlID="OldCampusTextBox"
								CssClass="dataInputLabel" LocalisationKey="CellCampus.Label" DefaultText="Old campus location" />
							<span class="dataInputField">
								<asp:TextBox ID="OldCampusTextBox" runat="server" Width="300px" ReadOnly="true" Enabled="false"></asp:TextBox>
							</span>
						</div>
						<div class="dataInputRow">
							<focus:LocalisedLabel runat="server" ID="NewCampusLabel" CssClass="dataInputLabel requiredData"
								AssociatedControlID="UpdateCampusDropDown" LocalisationKey="NewCampus.Text" DefaultText="New campus location" />
							<span class="dataInputField">
								<asp:DropDownList runat="server" ID="UpdateCampusDropDown" Width="305px" />
								<span class="dataInputFieldSubContainer">
									<asp:RequiredFieldValidator ID="CampusRequired" runat="server" ControlToValidate="UpdateCampusDropDown"
										SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateCampus" />
								</span></span>
						</div>
						<div class="modalButtons">
							<asp:Button ID="UpdateCampusButton" runat="server" class="buttonLevel2" OnClick="UpdateCampusButton_Click" title="UpdateCampusButton"
								ValidationGroup="UpdateCampus" />
						</div>
					</div>
				</div>
			</div>
		</asp:PlaceHolder>
	</asp:PlaceHolder>
	<asp:PlaceHolder runat="server" ID="ChangeEmailAddressPlaceholder">
		<div class="collapsiblePanel collapsibleSingle cpCollapsed">
			<div class="collapsiblePanelHeader" style="font-size:14px">
				<div class="cpIcon">
				</div>
				<span id="ChangeEmailAddressLabel" class="collapsiblePanelHeaderLabel cpHeaderControl" style="left:25px">
					<a href="#">
						<%= HtmlLocalise("AccordionHeadingEmail.Label", "Change Resume Email")%></a></span>
			</div>
			<div class="collapsiblePanelContent">
				<div class="dataInputContainer">
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="OldEmailAddressLabel" AssociatedControlID="OldEmailAddressTextBox"
							CssClass="dataInputLabel requiredData" LocalisationKey="OldEmailAddress.Label"
							DefaultText="Old email address" />
						<span class="dataInputField">
                          <asp:TextBox ID="OldEmailAddressTextBox" runat="server" ReadOnly="True" ClientIDMode="Static" Width="286px"  />
						</span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="NewEmailAddressLabel" AssociatedControlID="NewEmailAddressTextBox"
							CssClass="dataInputLabel requiredData" LocalisationKey="NewEmailAddress.Label"
							DefaultText="New email address" />
						<span class="dataInputField">
                          <asp:TextBox ID="NewEmailAddressTextBox" runat="server" ClientIDMode="Static" Width="286px"/>
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="NewEmailAddressRequired" runat="server" ControlToValidate="NewEmailAddressTextBox"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateEmailAddress" />
								<asp:RegularExpressionValidator ID="NewEmailAddressRegEx" runat="server" ControlToValidate="NewEmailAddressTextBox"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateEmailAddress" />
							</span></span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="ConfirmEmailAddressLabel" AssociatedControlID="ConfirmEmailAddressTextBox"
							CssClass="dataInputLabel requiredData" LocalisationKey="ConfirmEmailAddress.Label"
							DefaultText="Confirm email address" />
						<span class="dataInputField">
                          <asp:TextBox ID="ConfirmEmailAddressTextBox" runat="server" ClientIDMode="Static" Width="286px" />
							<span class="dataInputFieldSubContainer">
								<asp:RequiredFieldValidator ID="ConfirmEmailAddressRequired" runat="server" ControlToValidate="ConfirmEmailAddressTextBox"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateEmailAddress" />
								<asp:CompareValidator ID="ConfirmEmailAddressCompare" runat="server" ControlToValidate="ConfirmEmailAddressTextBox"
									ControlToCompare="NewEmailAddressTextBox" SetFocusOnError="true" Display="Dynamic"
									CssClass="error" ValidationGroup="UpdateEmailAddress" />
							</span></span>
					</div>
					<div class="modalButtons">
						<asp:Button ID="UpdateEmailAddressButton" runat="server" class="buttonLevel2" OnClick="UpdateEmailAddressButton_Click" title="UpdateEmailAddressButton"
							ValidationGroup="UpdateEmailAddress" /> 
					</div>
				</div>
			</div>
		</div>
	</asp:PlaceHolder>
	
		<asp:PlaceHolder runat="server" ID="ChangeAccountTypePlaceholder">
		<div class="collapsiblePanel collapsibleSingle cpCollapsed">
			<div class="collapsiblePanelHeader" style="font-size:14px">
				<div class="cpIcon">
				</div>
				<span id="Span1" class="collapsiblePanelHeaderLabel cpHeaderControl"   style="left:25px">
					<a href="#">
						<%= HtmlLocalise("AccordionHeadingAccountType.Label", "Change Account Type")%></a></span>
			</div>
			<div class="collapsiblePanelContent">
				<div class="dataInputContainer">
					<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="CurrentAccountTypeLabel" CssClass="dataInputLabel requiredData"
								AssociatedControlID="CurrentAccountTypeDropDownList" LocalisationKey="CurrentAccountType.Text" DefaultText="Current account type" />
							<span class="dataInputField">
								<asp:DropDownList runat="server" ID="CurrentAccountTypeDropDownList" Width="305px" />
								<span class="dataInputFieldSubContainer">
						</span>
						</span>
					</div>
				<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="NewAccountTypeLabel" CssClass="dataInputLabel requiredData"
								AssociatedControlID="NewAccountTypeDropdownList" LocalisationKey="NewAccountType.Text" DefaultText="New account type" />
							<span class="dataInputField">
								<asp:DropDownList runat="server" ID="NewAccountTypeDropdownList" Width="305px" />
								<span class="dataInputFieldSubContainer">
									<asp:RequiredFieldValidator ID="CareerTypeAccountRequired" runat="server" ControlToValidate="NewAccountTypeDropdownList"
										SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdateAccountType" />
								</span>
						</span>
					</div>
					<div class="modalButtons">
						<asp:Button ID="UpdateAccountTypeButton" runat="server" class="buttonLevel2" OnClick="UpdateAccountTypeButton_Click" OnClientClick="SwapSelection()" Title="UpdateAccountTypeButton"
							ValidationGroup="UpdateAccountType" />
					</div>
				</div>
			</div>
		</div>
	</asp:PlaceHolder>

	<asp:PlaceHolder runat="server" ID="EducationPlaceHolder2" Visible="false">
		<div class="collapsiblePanel collapsibleSingle cpCollapsed">
			<div class="collapsiblePanelHeader" style="font-size:14px">
				<div class="cpIcon">
				</div>
				<span id="ChangePhoneNumberLabel" class="collapsiblePanelHeaderLabel cpHeaderControl">
					<a href="#">
						<%= HtmlLocalise("CellPhoneNumberHeading.Label", "Change Cellphone Number/Provider")%></a></span>
			</div>
			<div class="collapsiblePanelContent">
				<div class="dataInputContainer">
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="PhoneNumberLabel" AssociatedControlID="PhoneNumberTextBox"
							CssClass="dataInputLabel" LocalisationKey="CellPhoneNumber.Label" DefaultText="Cellphone number" />
						<span class="dataInputField">
							<asp:TextBox ID="PhoneNumberTextBox" runat="server" ClientIDMode="Static" Width="120px"
								MaxLength="30" />
							<%--<ajaxtoolkit:MaskedEditExtender ID="maskedPhoneNumber" BehaviorID="maskedPhoneNumberBehaviour"
								runat="server" MaskType="Number" TargetControlID="PhoneNumberTextBox" PromptCharacter="_"
								ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false">
							</ajaxtoolkit:MaskedEditExtender>--%>
							<span class="dataInputFieldSubContainer">
								<asp:RegularExpressionValidator ID="PhoneNumberRegEx" runat="server" ControlToValidate="PhoneNumberTextBox"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdatePhoneNumber" />
							</span></span>
					</div>
					<div class="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="PhoneProviderLabel" AssociatedControlID="PhoneProviderDropdown"
							CssClass="dataInputLabel" LocalisationKey="CellPhoneProvider.Label" DefaultText="Cellphone provider" />
						<span class="dataInputField">
							<asp:DropDownList ID="PhoneProviderDropdown" runat="server" ClientIDMode="Static"
								Width="180px" />
							<span class="dataInputFieldSubContainer">
								<asp:CustomValidator ID="CustomPhoneProviderValidator" runat="server" ControlToValidate="PhoneProviderDropdown"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UpdatePhoneNumber"
									ClientValidationFunction="ValidatePhoneProvider" ValidateEmptyText="true" ClientIDMode="Static" />
							</span></span>
					</div>
					<div class="modalButtons">
						<asp:Button ID="UpdatePhoneNumberButton" runat="server" class="buttonLevel2" OnClick="UpdatePhoneNumberButton_Click" title="UpdatePhoneNumberButton"
							ValidationGroup="UpdatePhoneNumber" />
					</div>
				</div>
			</div>
		</div>
	</asp:PlaceHolder>
	<asp:HiddenField runat="server" ID="MyAccountOpenPanel" Value="" ClientIDMode="Static" />
</asp:Panel>
<%--</div>--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("#PhoneNumberTextBox").mask('(999) 999-9999', { placeholder: "_" });
		$('#UpdateSecurityQuestionDropDown').change(function () {
			if ($(this).val() === '<%= CreateOwnSecurityQuestionValue %>') {
				$('#UpdateSecurityQuestionDiv').slideDown();
			} else {
				$('#UpdateSecurityQuestionDiv').slideUp();
			}
		});

		if ($('#UpdateSecurityQuestionDropDown').val() === '<%= CreateOwnSecurityQuestionValue %>') {
			$('#UpdateSecurityQuestionDiv').show();
		}

		var openPanel = $("#MyAccountOpenPanel");
		if (openPanel.val().length > 0) {
			OpenHideCollapsiblePanel($("#MyAccountSection_" + openPanel.val()), 10);
			openPanel.val("");
		}
		// Jaws Compatibility
		$('.myAccountModal').attr('aria-hidden', true);
	});

	Sys.Application.add_load(MyAccount_PageLoad);
	function MyAccount_PageLoad(sender, args) {
		var cdd = $find("DegreeDropDownListCascadingDropDownBehavior");
		if (cdd != null)
			cdd.add_populated(onPopulated);

		var cdd2 = $find("DetailedDegreeCascadingDropDownBehavior");
		if (cdd2 != null)
			cdd2.add_populated(onPopulatedDetails);

		// Jaws Compatibility
		var modal = $find('MyAccountModal');

		if (modal != null) {
			var modalPanel = $('.myAccountModal');
			if (modalPanel.is(":visible"))
				window.setTimeout(MyAccount_PageLayout, 5);

			modal.add_shown(function () {
				window.setTimeout(MyAccount_PageLayout, 5);
				modalPanel.attr('aria-hidden', false);
				$('.page').attr('aria-hidden', true);
				$('.modalCloseIcon').focus();
			});
			modal.add_hiding(function () {
				modalPanel.attr('aria-hidden', true);
				$('.page').attr('aria-hidden', false);
			});
		}
	}

	function MyAccount_PageLayout() {
		var modal = $find('MyAccountModal');
		if (modal != null) {
			modal._layout();
		}
	}

	function onPopulated() {
		UpdateStyledDropDown($('#<%=UpdateProgramOfStudyAreaDegreesDropDown.ClientID%>'));
	}

	function onPopulatedDetails() {
		UpdateStyledDropDown($('#<%=DetailedDegreeAjaxDropDownList.ClientID%>'));
	}

	function validateOwnUpdateSecurityQuestion(src, args) {
		if ($('#UpdateSecurityQuestionDropDown').val() === '<%= CreateOwnSecurityQuestionValue %>') {
			var ownSecurityQuestion = $('#UpdateSecurityQuestionTextBox').val();
			if (ownSecurityQuestion.length > 0) ownSecurityQuestion = TrimText(ownSecurityQuestion);
			args.IsValid = (ownSecurityQuestion !== '');
		} else {
			args.IsValid = true;
		}
	}

	function ValidateCellphone(sender, args) {
		if (args.Value.length > 0 && args.Value != "(___) ___-____") {
			var phoneRegx = /^\([0-9]\d{2}\)\s?\d{3}\-\d{4}$/;
			if (phoneRegx.test(args.Value) == false) {
				args.IsValid = false;
			}
		}
		else {
			args.IsValid = true;
		}
	}

	function ValidatePhoneProvider(sender, args) {
		if (args.Value.length == 0) {
			var phoneNumber = $("#PhoneNumberTextBox").val();
			if (phoneNumber.length > 0 && phoneNumber != "") {
				args.IsValid = false;
			}
			else {
				args.IsValid = true;
			}
		}
		else {
			args.IsValid = true;
		}
	}



</script>
