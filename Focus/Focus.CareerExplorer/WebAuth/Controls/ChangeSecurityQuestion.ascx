﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeSecurityQuestion.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.ChangeSecurityQuestion" %>

<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Controls/ConfirmationModal.ascx" %>

<hr/>
<table width="100%">
	<tr>
		<td><b><%= HtmlLocalise("Title", "Change security question")%></b></td>
		<td style="text-align: right">
			<a id="HideChangeSecurityQuestionContent" href="#" onclick="javascript:closeChangeSecurityQuestion();" style="display: none;"><%= HtmlLocalise("Hide.Text", "Hide")%></a>
			<a id="ShowChangeSecurityQuestionContent" href="#" onclick="javascript:expandChangeSecurityQuestion();"><%= HtmlLocalise("Show.Text", "Show")%></a>
		</td>
	</tr>
</table>
<p></p>
<div id="ChangeSecurityQuestionContent" class="myAccountContent">
	<table width="500px">
		<tr>
			<td width="250px"><%= HtmlLabel("CurrentSecurityQuestion", "Current security question")%></td>
			<td><asp:Literal ID="CurrentSecurityQuestionLiteral" runat="server" /></td>
		</tr>
		<tr>
			<td><%= HtmlRequiredLabel(SecurityQuestionDropDown, "NewSecurityQuestion", "Change security question")%></td>
			<td>
				<asp:DropDownList ID="SecurityQuestionDropDown" runat="server" ClientIDMode="Static" TabIndex="21" />
				<div id="SecurityQuestionDiv" style="display: none; padding-top: 8px;">
					<asp:TextBox ID="SecurityQuestionTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="22" MaxLength="100"  />
					<asp:CustomValidator ID="SecurityQuestionCustomValidator" runat="server" ControlToValidate="SecurityQuestionTextBox" ClientValidationFunction= "validateOwnSecurityQuestion" 
																ValidationGroup="ChangeSecurityQuestion" ValidateEmptyText="True" CssClass="error" SetFocusOnError="true" Display="Dynamic" />
				</div>
				<asp:RequiredFieldValidator ID="SecurityQuestionRequired" runat="server" ControlToValidate="SecurityQuestionDropDown" SetFocusOnError="true" Display="Dynamic" CssClass="error" 
																		ValidationGroup="ChangeSecurityQuestion"/>
				
			</td>
		</tr>
		<tr>
			<td><%= HtmlRequiredLabel(SecurityAnswerTextBox, "SecurityAnswer", "Security answer")%></td>
			<td>
				<asp:TextBox ID="SecurityAnswerTextBox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="23" MaxLength="30" />
				<asp:RequiredFieldValidator ID="SecurityAnswerRequired" runat="server" ControlToValidate="SecurityAnswerTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" 
																			ValidationGroup="ChangeSecurityQuestion" />
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: right"><asp:Button ID="SaveButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel2" ValidationGroup="ChangeSecurityQuestion" TabIndex="24" OnClick="SaveButton_Clicked"/></td>
		</tr>
	</table>
</div>

<asp:HiddenField ID="ChangeSecurityQuestionExpanded" runat="server" value="0" ClientIDMode="Static"/>

<%-- Confirmation Modal --%>
<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />

<script type="text/javascript">
	$(document).ready(function () {
		$("#SecurityQuestionDropDown").change(function () {
			if ($(this).val() == "<%= CreateOwnSecurityQuestionValue %>") {
				$("#SecurityQuestionDiv").show();
			} else {
				$("#SecurityQuestionDiv").hide();
			}
		});

		if ($("#SecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
			$("#SecurityQuestionDiv").show();
		}

		if ($('#ChangeSecurityQuestionExpanded').val() == "0") {
			closeChangeSecurityQuestion();
		}
		else {
			expandChangeSecurityQuestion();
		}
	});

	function validateOwnSecurityQuestion(src, args) {
		if ($("#SecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
			var ownSecurityQuestion = $("#SecurityQuestionTextBox").val();
			if (ownSecurityQuestion.length > 0) ownSecurityQuestion = TrimText(ownSecurityQuestion);
			args.IsValid = (ownSecurityQuestion != "");
		} else {
			args.IsValid = true;
		}
	}

	function closeChangeSecurityQuestion() {
		$('#ChangeSecurityQuestionContent').hide();
		$('#HideChangeSecurityQuestionContent').hide();
		$('#ShowChangeSecurityQuestionContent').show();
		$('#ChangeSecurityQuestionExpanded').val("0");
	}

	function expandChangeSecurityQuestion() {
		$('#ChangeSecurityQuestionContent').show();
		$('#HideChangeSecurityQuestionContent').show();
		$('#ShowChangeSecurityQuestionContent').hide();
		$('#ChangeSecurityQuestionExpanded').val("1");
	}
</script>