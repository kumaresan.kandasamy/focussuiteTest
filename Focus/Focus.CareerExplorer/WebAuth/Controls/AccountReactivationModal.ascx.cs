﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Core.Models.Career;
using Focus.Core;
using Focus.Common;
using Focus.Core.Views;
using Framework.Core;
using Framework.Logging;
using Focus.Common.Authentication;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
    public partial class AccountReactivationModal : UserControlBase
    {
        protected string EnrollmentStatusErrorMessage = "";
        protected string EducationLevelErrorMessage = "";
        protected string EducationLevelRequired = "";

        public delegate void SubmittedHandler(object sender);
        public event SubmittedHandler Submitted;

        private long CandidateResumeId
        {
            get { return GetViewStateValue("AccountReactivationModal:CandidateResumeId", (long)0); }
            set { SetViewStateValue("AccountReactivationModal:CandidateResumeId", value); }
        }

        private long? PersonId
        {
            get { return GetViewStateValue("AccountReactivationModal:PersonId", (long)0); }
            set { SetViewStateValue("AccountReactivationModal:PersonId", value); }
        }

        
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LocaliseUI();

        }

        /// <summary>
        /// Shows this instance.
        /// </summary>
        public void Show()
        {
            if (App.User.PersonId.HasValue)
            {
                PersonId = App.User.PersonId;
                CandidateResumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(Convert.ToInt64(App.User.PersonId));
                if (CandidateResumeId == 0)
                {
                    ServiceClientLocator.AccountClient(App).ActivateAccount(App.User.UserId);
                    CompleteActivation();
                    return;
                }
            }

            EnrollmentStatusDropdown.Visible = !App.Settings.HideEnrollmentStatus;
            BindEnrollmentStatusDropDown(EnrollmentStatusDropdown);

            EmploymentStatusDropdown.DataSource = Enum.GetValues(typeof(EmploymentStatus)).Cast<EmploymentStatus>().Where(x => x != EmploymentStatus.None).Select(employmentStatus => new ListItem { Text = CodeLocalise(employmentStatus, true), Value = ((int)employmentStatus).ToString() }).ToList();
            EmploymentStatusDropdown.DataTextField = "Text";
            EmploymentStatusDropdown.DataValueField = "Value";
            EmploymentStatusDropdown.DataBind();

            EmploymentStatusDropdown.Items.AddLocalisedTopDefault("EmploymentStatus.TopDefault", "- select employment status -");

            CommonUtilities.GetEducationLevelDropDownList(EducationStatusDropdown);

            AccountReactivationModalPopup.Show();
        }

        /// <summary>
        /// Hides this instance.
        /// </summary>
        public void Hide()
        {
            AccountReactivationModalPopup.Hide();
        }

        /// <summary>
        /// Handles the Click event of the Submit control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        /// 

        protected void Submit_Click(object sender, EventArgs e)
        {
            var IsAccountInactive = ServiceClientLocator.CoreClient(App).CheckLastAction(App.User.PersonId);
            if (IsAccountInactive)
            {
                
                    // Ensure this is the same user as registration can swap users
                    if (PersonId != App.User.PersonId)
                    {
                        var logout = (LogOut)MasterPage.FindControl("MainLogOut");
                        logout.LogOutSession(false);
                        AccountReactivationModalPopup.Show();

                        Confirmation.Show(CodeLocalise("AccountReactivationInProgress.Title", "Account Reactivation in progress"),
                        CodeLocalise("AccountReactivationInProgress.Message",
                          "Another account has been detected and has been logged out automatically. Please log in again in order to continue."),
                        CodeLocalise("CloseModal.Text", "OK"),
                        closeLink: UrlBuilder.Home());
                        return;
                    }

                    ServiceClientLocator.AccountClient(App).ActivateAccount(App.User.UserId);

                    if (CandidateResumeId > 0)
                    {
                        var resumeModel = ServiceClientLocator.ResumeClient(App).GetResume(CandidateResumeId);

                        if (resumeModel.IsNotNull())
                        {
                            var schoolStatusText = EnrollmentStatusDropdown.SelectedItem.Value;
                            var schoolStatus = schoolStatusText.Length > 0
                                ? (SchoolStatus)Enum.Parse(typeof(SchoolStatus), EnrollmentStatusDropdown.SelectedItem.Value, true)
                                : SchoolStatus.NA;

                            var educationLevel = EducationStatusDropdown.SelectedValueToEnum<EducationLevel>();
                            var employmentStatus = (EmploymentStatus)Int32.Parse(EmploymentStatusDropdown.SelectedItem.Value);

                            resumeModel.ResumeContent.EducationInfo.SchoolStatus = schoolStatus;
                            resumeModel.ResumeContent.EducationInfo.EducationLevel = educationLevel;
                            resumeModel.ResumeContent.ExperienceInfo.EmploymentStatus = employmentStatus;

                            resumeModel.Special.Preferences.IsResumeSearchable = ResumeSearchableRadioList.SelectedValue.ToLower() == "yes";

                            ServiceClientLocator.ResumeClient(App).SaveResume(resumeModel, false, Convert.ToInt64(App.User.PersonId));
                        }
                    }

                    // Update the last surveyed on date to be today so that the user doesn't get another survey immediately after completing this survey.
                    ServiceClientLocator.AccountClient(App).UpdateLastSurveyedDate(Convert.ToInt64(App.User.PersonId));
                    
                    CompleteActivation();

                
            }
            else
            {
                var logout = (LogOut)MasterPage.FindControl("MainLogOut");
                logout.LogOutSession(false);
                AccountReactivationModalPopup.Show();

                Confirmation.Show(CodeLocalise("AccountReactivationInProgress.Title", "Account Reactivation in progress"),
                CodeLocalise("AccountReactivationInProgress.Message",
                  "Another account has been detected and has been logged out automatically. Please log in again in order to continue."),
                CodeLocalise("CloseModal.Text", "OK"),
                closeLink: UrlBuilder.Home());
                return;
            }
            
        }

        private void CompleteActivation()
        {
            var user = App.User;
            user.IsEnabled = true;

            Context.User = new UserPrincipal(Context.User.Identity, user);

            App.RemoveSessionValue("ReactivateRequestKey");

            App.RemoveCookieValue("AccountReactivation:Show");

            OnSubmitted();

            AccountReactivationModalPopup.Hide();
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            EnrollmentStatusRequiredValidator.ErrorMessage = CodeLocalise("EnrollmentStatus.Required", "Enrollment status is required");
            EducationStatusRequiredValidator.ErrorMessage = CodeLocalise("EducationStatus.Required", "Education level is required");
            EmploymentStatusRequiredValidator.ErrorMessage = CodeLocalise("EducationStatus.Required", "Employment status is required");
            ResumeSearchableRequired.ErrorMessage = CodeLocalise("ResumeSearchableRequired.Required", "Indication of whether default resume should be searchable is required");

            EnrollmentStatusErrorMessage = CodeLocalise("EnrollmentStatus.ErrorMessage", "Enrollment status cannot be greater than education level");
            EducationLevelErrorMessage = CodeLocalise("EducationLevel.ErrorMessage", "Enrollment status cannot be less than education level");

            SubmitButton.Text = CodeLocalise("SubmitButton.Text", "Submit");
        }

        /// <summary>
        /// Raises the <see cref="E:Submitted" /> event.
        /// </summary>
        protected virtual void OnSubmitted()
        {
            if (Submitted != null)
                Submitted(this);
        }
    }
}