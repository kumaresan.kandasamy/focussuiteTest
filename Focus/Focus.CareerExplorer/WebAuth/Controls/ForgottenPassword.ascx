﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgottenPassword.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.ForgottenPassword" %>

<%@ Register src="~/Code/Controls/User/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="ForgottenPasswordModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ForgottenPasswordModal" runat="server" BehaviorID="ForgottenPasswordModal"
												TargetControlID="ForgottenPasswordModalDummyTarget"
												PopupControlID="ForgottenPasswordPanel" 
												BackgroundCssClass="modalBackground" Y="210" />

<asp:Panel ID="ForgottenPasswordPanel" runat="server" CssClass="modal" Width="325px" ClientIDMode="Static" >
	<table width="100%">
		<tr>
			<th><%= HtmlLocalise("Header.Text", "Forgot your password?")%></th>
		</tr>
		<tr>
			<td valign="top">
				<p><%= HtmlLocalise("Notification.Text", "Please enter your email address below, and we'll send you an email to help get back online.")%></p>
				<br />
				<p>
					<span class="inFieldLabel">
						<label for="ForgottenPasswordEmailAddressTextBox"><%= HtmlLocalise("ForgottenPasswordEmailAddress.Label", "email address")%></label>
						<asp:TextBox ID="ForgottenPasswordEmailAddressTextBox" runat="server" ClientIDMode="Static" Width="275px" />
					</span>
				</p>
				<p><a href="#" onclick="javascript:$find('ForgottenPasswordModal').hide();$find('LogInModal').show();"><%= HtmlLocalise("Login.Text", "Log in")%></a></p>
				<focus:AjaxValidationSummary ID="ForgottenPasswordValidationSummary" runat="server" DisplayMode="SingleParagraph" CssClass="error" ValidationGroup="ForgottenPassword" />
			</td>
		</tr>
		<tr>
			<td align="right">
				<asp:Button ID="SubmitButton" runat="server" class="buttonLevel2" ValidationGroup="ForgottenPassword" OnClick="SubmitButton_Click" />
			</td>
		</tr>
	</table>

	<asp:RequiredFieldValidator ID="ForgottenPasswordEmailAddressRequired" runat="server" ControlToValidate="ForgottenPasswordEmailAddressTextBox" SetFocusOnError="true" Display="None" ValidationGroup="ForgottenPassword" />
	<asp:RegularExpressionValidator ID="ForgottenPasswordEmailAddressRegEx" runat="server" ControlToValidate="ForgottenPasswordEmailAddressTextBox" SetFocusOnError="true" Display="None" ValidationGroup="ForgottenPassword" />
	<asp:CustomValidator ID="ForgottenPasswordValidator" runat="server" Display="None" ValidationGroup="ForgottenPassword" />
</asp:Panel>

<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />