﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogOut.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.LogOut" %>
<asp:HiddenField ID="LogOutModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="LogOutModal" runat="server" BehaviorID="LogOutModal"
                        TargetControlID="LogOutModalDummyTarget" PopupControlID="LogOutPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<div id="LogOutPanel" class="modal logOutModal" style="z-index: 100001; display: none;">
	<div class="lightbox FocusCareer">
		<div class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('LogOutModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.LinkText", "Close") %>" width="20" height="20" />
			<focus:LocalisedLabel runat="server" ID="SignOutHeaderLabel" CssClass="modalTitle modalTitleLarge" LocalisationKey="SignOut.Label" DefaultText="Sign out"/>
		</div>
		<focus:LocalisedLabel runat="server" ID="SignOutQuestionLabel" CssClass="modalMessage" LocalisationKey="Notification.Text" DefaultText="Are you sure you want to Sign out?"/>
		<div class="modalButtonBorder">
			<div class="modalButtons"><asp:Button ID="CompleteLogOutButton" runat="server" class="buttonLevel2" OnClick="CompleteLogOutButton_Click" /></div>
		</div>
	</div>
</div>
