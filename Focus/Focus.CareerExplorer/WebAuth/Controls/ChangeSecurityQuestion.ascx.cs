﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class ChangeSecurityQuestion : UserControlBase
	{
		protected const string CreateOwnSecurityQuestionValue = "Own";

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				BindSecurityQuestionDropDown();
			}

			LocaliseUI();
		}

		/// <summary>
		/// Handles the Clicked event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Clicked(object sender, EventArgs e)
		{
			var question = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue)
																? null
																: SecurityQuestionTextBox.TextTrimmed();
			var questionId = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue)
																? SecurityQuestionDropDown.SelectedValueToInt()
																: (long?)null;
			var answer = SecurityAnswerTextBox.TextTrimmed();

			ServiceClientLocator.AccountClient(App).ChangeSecurityQuestion(question, questionId, answer);

			ConfirmationModal.Show(CodeLocalise("SecurityQuestionSuccessfulChange.Title", "Security question changed"),
															 CodeLocalise("PasswordSuccessfulChange.Body", "You have successfully changed your security question."),
															 CodeLocalise("Global.Close.Text", "Close"));

			CurrentSecurityQuestionLiteral.Text = question;
			SecurityQuestionDropDown.SelectedIndex = 0;
			SecurityQuestionTextBox.Text = SecurityAnswerTextBox.Text = "";
		}

		/// <summary>
		/// Binds the specified model.
		/// </summary>
		/// <param name="model">The model.</param>
		public void Bind(MyAccountModel model)
		{
			if (model.SecurityQuestionId.HasValue)
			{
				CurrentSecurityQuestionLiteral.Text = SecurityQuestionDropDown.Items.FindByValue(model.SecurityQuestionId.ToString()).Text;
			}
			else
			{
				CurrentSecurityQuestionLiteral.Text = model.SecurityQuestion;				
			}
		}

		/// <summary>
		/// Binds the security question drop down.
		/// </summary>
		private void BindSecurityQuestionDropDown()
		{
			SecurityQuestionDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions), null, CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"));
			SecurityQuestionDropDown.Items.Add(new ListItem(CodeLocalise("CreateOwnSecurityQuestion.DropDownValue", "Create own security question"), CreateOwnSecurityQuestionValue));
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			SecurityQuestionRequired.ErrorMessage = CodeLocalise("SecurityQuestionRequired.ErrorMessage", "Security question is required");
			SecurityQuestionCustomValidator.ErrorMessage = CodeLocalise("SecurityQuestionCustomValidator.ErrorMessage", "Security question is required");
			SecurityAnswerRequired.ErrorMessage = CodeLocalise("SecurityAnswerRequired.ErrorMessage", "Security answer is required");
			SaveButton.Text = CodeLocalise("Global.Save.Text", "Save");
		}
	}
}