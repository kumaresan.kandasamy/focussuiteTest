﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword2.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.ForgotPassword2" %>
<asp:HiddenField ID="ForgotPasswordModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ForgotPasswordModal" runat="server" BehaviorID="ForgotPasswordModal"
	TargetControlID="ForgotPasswordModalDummyTarget" PopupControlID="ForgotPasswordPanel"
	BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />

<script type="text/javascript">
	Sys.Application.add_load(ForgotPassword_PageLoad);

	function ForgotPassword_PageLoad(sender, args) {
		$('#forgotPasswordCloseIcon').click(function () {
			$find('ForgotPasswordModal').hide();

			$("#lblResetPassword").text('');
			$("#ForgotPasswordEmailAddressTextbox").val('');
			$("#ForgotPasswordConfirmEmailAddressTextbox").val('');

			$find('LogInModal').show();
			return false;
		});

		var questionDropdown = $("#FPSecurityQuestionDropDown");
		if (questionDropdown.exists()) {
			$("#FPSecurityQuestionDropDown").change(function () {
				ForgotPassword_ValidatorEnable();
			});
			ForgotPassword_ValidatorEnable();
		}
	}

	function ForgotPassword_ValidatorEnable() {
		if ($("#FPSecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
			$("#FPSecurityQuestionDiv").show();
			ValidatorEnable(document.getElementById('SecurityQuestionTextBoxRequiredFieldValidator'), true);
		} else {
			$("#FPSecurityQuestionDiv").hide();
			ValidatorEnable(document.getElementById('SecurityQuestionTextBoxRequiredFieldValidator'), false);
		}
	}
</script>
<asp:Panel ID="ForgotPasswordPanel" runat="server" CssClass="modal">
	<div class="lightbox FocusCareer forgotPasswordModal">
		<div class="modalHeader">
			<input type="image" id="forgotPasswordCloseIcon" src="<%= UrlBuilder.Close() %>" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
			<focus:LocalisedLabel runat="server" ID="ForgotPasswordTitleLabel" LocalisationKey="ForgotPassword.Label" DefaultText="Forgot Your Password?" CssClass="modalTitle modalTitleLarge"/>
		</div>
		<focus:LocalisedLabel runat="server" ID="PasswordHelpInfoLabel" CssClass="modalMessage" LocalisationKey="PasswordHelpInfo.Text" />
		<div class="dataInputContainer">
			<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailAddressLabel" CssClass="dataInputLabel" AssociatedControlID="ForgotPasswordEmailAddressTextbox" LocalisationKey="EmailAddress" DefaultText="Email address"/>
				<span class="dataInputField">
					<asp:TextBox ID="ForgotPasswordEmailAddressTextbox" runat="server" ClientIDMode="Static" Width="100%" TabIndex="1" MaxLength="100" AutoCompleteType="Disabled" />
					<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="ForgotPasswordEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
					<asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="ForgotPasswordEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
				</span>
			</div>
			<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailAddressConfirmationLabel" CssClass="dataInputLabel" AssociatedControlID="ForgotPasswordConfirmEmailAddressTextbox" LocalisationKey="EmailAddressConfirmation" DefaultText="Re-enter email address"/>
				<span class="dataInputField">
					<asp:TextBox ID="ForgotPasswordConfirmEmailAddressTextbox" runat="server" ClientIDMode="Static" Width="100%" TabIndex="2" MaxLength="100" AutoCompleteType="Disabled" />
					<asp:RequiredFieldValidator ID="ConfirmEmailAddressRequired" runat="server" ControlToValidate="ForgotPasswordConfirmEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
					<asp:CompareValidator ID="ConfirmEmailAddressCompare" runat="server" ControlToValidate="ForgotPasswordConfirmEmailAddressTextbox" ControlToCompare="ForgotPasswordEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
				</span>
			</div>
      <asp:PlaceHolder runat="server" ID="ExtraQuestionsPlaceHolder">
			  <div class="dataInputRow">
				  <focus:LocalisedLabel runat="server" ID="FPSecurityQuestionDropDownLocalisedLabel" CssClass="dataInputLabel" AssociatedControlID="FPSecurityQuestionDropDown" LocalisationKey="SelectSecurityQuestionLocalisationKey" DefaultText="Select security question"/>
				  <span class="dataInputField">
					  <div style="position: relative;">
						  <asp:DropDownList ID="FPSecurityQuestionDropDown" ClientIDMode="Static" runat="server" TabIndex="3" Width="100%" />
						  <asp:RequiredFieldValidator runat="server" InitialValue="0" ID="SecurityQuestionRequiredFieldValidatro" ControlToValidate="FPSecurityQuestionDropDown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
					  </div>
					  <div id="FPSecurityQuestionDiv" style="display: none; padding-top: 8px;">
						  <asp:TextBox ID="FPSecurityQuestionTextBox" runat="server" ClientIDMode="Static" TabIndex="4" MaxLength="100" Width="100%" />
						  <asp:RequiredFieldValidator runat="server" ID="SecurityQuestionTextBoxRequiredFieldValidator" ControlToValidate="FPSecurityQuestionTextBox" ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
					  </div>
				  </span>
			  </div>
			  <div class="dataInputRow">
				  <focus:LocalisedLabel runat="server" ID="SecurityAnswerTextBoxLocalisedLabel" CssClass="dataInputLabel" AssociatedControlID="SecurityAnswerTextBox" LocalisationKey="SelectSecuritAnswerLocalisationKey" DefaultText="Enter security answer"/>
				  <span class="dataInputField">
					  <asp:TextBox ID="SecurityAnswerTextBox" runat="server" Width="100%" TabIndex="5" MaxLength="100" AutoCompleteType="Disabled" />
					  <asp:RequiredFieldValidator ID="SecurityAnswerTextBoxRequiredFieldValidator" runat="server" ControlToValidate="SecurityAnswerTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
				  </span>
			  </div>
			  <div class="dataInputRow">
				  <focus:LocalisedLabel runat="server" ID="DayOfBirthLocalisedLabel" CssClass="dataInputLabel" AssociatedControlID="DayOfBirthTextBox" LocalisationKey="DayOfBirthTextBoxLabel" DefaultText="Enter day of birth <em>dd</em>"/>
				  <span class="dataInputField">
					  <asp:TextBox ID="DayOfBirthTextBox" runat="server" ClientIDMode="Static" Width="35" TabIndex="6" MaxLength="2" AutoCompleteType="Disabled" />
					  <asp:RequiredFieldValidator ID="DayOfBirthTextBoxRequiredFieldValidator" runat="server" ControlToValidate="DayOfBirthTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
					  <asp:RegularExpressionValidator ID="DayOfBirthTextBoxRegularExpressionValidator" ValidationExpression="^[0-9]{2}$" runat="server" ControlToValidate="DayOfBirthTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
				  </span>
			  </div>
			  <div class="dataInputRow">
				  <focus:LocalisedLabel runat="server" ID="MonthOfBirthLocalisedLabel" CssClass="dataInputLabel" AssociatedControlID="DayOfBirthTextBox" LocalisationKey="MonthOfBirthLocalisedLabel" DefaultText="Select month of birth"/>
          <span class="dataInputField">
				    <asp:DropDownList runat="server" ID="MonthOfBirthDropDownList" TabIndex="7" Width="140"/>
				    <asp:RequiredFieldValidator ID="MonthOfBirthRequiredFieldValidator" runat="server" ControlToValidate="MonthOfBirthDropDownList" InitialValue="0" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="PasswordHelp" />
          </span>
			  </div>
      </asp:PlaceHolder>
		</div>
		<asp:Label ID="lblResetPassword" runat="server" Visible="false" ClientIDMode="Static"></asp:Label>
		<div class="modalButtons"><asp:Button ID="btnRequestPassword" runat="server" class="buttonLevel2" ValidationGroup="PasswordHelp" OnClick="btnRequestPassword_Click" /></div>
	</div>
</asp:Panel>
