﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
  public partial class RegistrationConfirmation : UserControlBase
	{
    protected string ExploreSections
    {
      get
      {
        var sectionOrder = App.Settings.ExplorerSectionOrder.Split('|');
        return string.Concat("['", String.Join("','", sectionOrder), "']");
      }
    }

    protected string MajorDivOrder
    {
      get
      {
        var orderList = new List<string>();

        switch (App.Settings.Module)
        {
          case FocusModules.Explorer:
            orderList.Add("ExplorerOptionsPanel");
            break;
          case FocusModules.Career:
            orderList.Add(App.Settings.Theme == FocusThemes.Education ? "EducationOptionsPanel" : "CareerOptionsPanel");
            break;
          default:
            if (App.Settings.CareerExplorerFeatureEmphasis == CareerExplorerFeatureEmphasis.Explorer)
            {
              orderList.Add("ExplorerOptionsPanel");
              orderList.Add(App.Settings.Theme == FocusThemes.Education ? "EducationOptionsPanel" : "CareerOptionsPanel");
            }
            else
            {
              orderList.Add(App.Settings.Theme == FocusThemes.Education ? "EducationOptionsPanel" : "CareerOptionsPanel");
              orderList.Add("ExplorerOptionsPanel");
            }
            break;
        }


        return string.Concat("['", String.Join("','", orderList), "']");
      }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
	    InitialiseUi();
      LocaliseUI();
    }

    /// <summary>
    /// Shows this instance.
    /// </summary>
    public void Show(bool firstTimeAccess = false, bool redirectOnClose = false)
    {
      if (App.User != null && App.GuideToResume(App.User.PersonId))
        Response.Redirect(UrlBuilder.YourResume());

      if (firstTimeAccess)
      {
        WelcomeLabel.Text = App.Settings.Module == FocusModules.Explorer
                              ? CodeLocalise("ExplorerRegistrationComplete.Welcome", "Thanks for registering for Focus/Explorer!")
                              : CodeLocalise("CareerRegistrationComplete.Welcome", "Thanks for registering for #FOCUSCAREER#!");

        RegistrationConfirmationHeadingLabel.DefaultText = "Your account is ready to use. Get started now:";
        RegistrationConfirmationHeadingLabel.LocalisationKey = "GetStartedFirstTime.Label";
      }
      else
      {
        WelcomeLabel.Text = App.Settings.Module == FocusModules.Explorer
                              ? CodeLocalise("ExplorerNoResumeComplete.Welcome", "Welcome to Focus/Explorer!")
                              : CodeLocalise("NoResumeComplete.Welcome", "Welcome to #FOCUSCAREER#!");

        RegistrationConfirmationHeadingLabel.DefaultText = " Get started now:";
        RegistrationConfirmationHeadingLabel.LocalisationKey = "GetStarted.Label";
      }
      
      SetVisibility();

      if (!redirectOnClose)
        RedirectCloseImage.OnClientClick = "javascript:$find('RegistrationConfirmationModal').hide();return false";
      RegistrationConfirmationModal.Show();
    }

		/// <summary>
		/// Initialises the UI.
		/// </summary>
		private void InitialiseUi()
		{
			MyResumeDiv.Visible = MyResumeAndProgrammeOfStudyDiv.Visible = !App.Settings.CareerHideSearchJobsByResume;
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      ResearchStudyLabel.LocalisationKey = App.Settings.Theme == FocusThemes.Education
        ? "Explorer.Label.ResearchStudy.Education"
        : "Explorer.Label.ResearchStudy";

			ResearchStudyLabel.DefaultText = CodeLocalise("ResearchStudyLabel.Text", "Research a specific program of study, career or #BUSINESS#:LOWER");

      RedirectCloseImage.ImageUrl = UrlBuilder.Close();
      RedirectCloseImage.AlternateText = HtmlLocalise("Global.Close.ToolTip", "Close");
    }

    /// <summary>
    /// Sets the visibility of the controls depending on the current module
    /// </summary>
    private void SetVisibility()
    {
      switch (App.Settings.Module)
      {
				case FocusModules.Explorer:
		      CareerOptionsPanel.Visible = false;
				  break;
				case FocusModules.Career:
		      ExplorerOptionsPanel.Visible = false;
				  break;
      }

      switch (App.Settings.Theme)
      {
        case FocusThemes.Workforce:
          EducationOptionsPanel.Visible = false;
          break;
        case FocusThemes.Education:
          CareerOptionsPanel.Visible = false;
          break;
      }

      if (App.UserData.HasDefaultResume)
        EducationJobsBasedOnResumeHintLabel.Visible = JobsBasedOnResumeHintLabel.Visible = false;

      TutorialVideoPlaceHolder.Visible = (App.Settings.ShowCareerTutorialLink && App.User.PersonId != 0);
      if (TutorialVideoPlaceHolder.Visible)
      {
        TutorialVideoLink.NavigateUrl = UrlBuilder.Tutorial();
        TutorialVideoLink.Text = HtmlLocalise("TutorialVideoLink.Text", "Click here to learn more");
      }
    }

    /// <summary>
    /// Handles the Click event of the RedirectCloseImage control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
    protected void RedirectCloseImage_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
			// Only redirect to certain pages for now, as other pages may cause issues (SSO is one)
			if (Page.Is(UrlBuilder.JobSearchResults()) || Page.Is(UrlBuilder.JobPosting()))
				Response.Redirect(Request.RawUrl);
			else
		    Response.Redirect(UrlBuilder.Home());
    }

    /// <summary>
    /// Fires when the Search Jobs link button is clicked to redirect to the search page with the relevant criteria
    /// </summary>
    /// <param name="sender">Link button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
		protected void JobSearchWithoutResumeLink_Click(object sender, EventArgs e)
		{
			App.SetSessionValue("Career:SearchCriteria", new SearchCriteria());
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostingswithoutresume" });
		}

    /// <summary>
    /// Handles the Command event of the MatchingAction control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void MatchingAction_Command(object sender, CommandEventArgs e)
    {
			if (App.User.ProgramAreaId == 0 && e.CommandName == "ProgramOfStudy")
			{
				MasterPage.ShowModalAlert(AlertTypes.Info,
					CodeLocalise("ProgramOfStudy.Error", "You do not have a program of study stored.\r\nClick on My Account and choose a program of study in order to see job recommendations."));
			}
			else
			{
			  if (App.UserData.HasDefaultResume)
			  {
			    var resumeStatus = App.UserData.DefaultResumeCompletionStatus;

			    if (resumeStatus == ResumeCompletionStatuses.Completed)
			    {
			      var redirectRoute = Utilities.RouteToJobSearch(e.CommandName, true);
			      Response.Redirect(redirectRoute);
			    }
			    else
			    {
			      // Redirect to resume wizard
			      App.SetSessionValue("Career:ResumeID", App.UserData.DefaultResumeId);
			      ResumeWizardRedirectConfirmationModal.Show("", "It seems you were in the middle of building your resume. We have taken you back to where you have left off.", "Cancel", "CANCEL", "", "Ok", ((int) resumeStatus).ToString(), height: 50);
			    }
			  }
			  else
			  {
          Response.RedirectToRoute("YourResume");
			  }
			}
    }

    /// <summary>
    /// Fires when the Search Jobs link button is clicked in the education panel to redirect to the search page with the relevant criteria
    /// </summary>
    /// <param name="sender">Link button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void EducationJobSearchWithoutResumeLink_Click(object sender, EventArgs e)
    {
			App.SetCookieValue("Career:PostingSearchInternshipFilter", FilterTypes.Exclude.ToString());
      Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
    }

    /// <summary>
    /// Fires when the Internships link button is clicked to redirect to the search page with the relevant criteria
    /// </summary>
    /// <param name="sender">Link button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void InternshipsSearchLink_Click(object sender, EventArgs e)
    {
      App.SetCookieValue("Career:PostingSearchInternshipFilter", FilterTypes.ShowOnly.ToString());
      Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
    }

    /// <summary>
    /// Fires when the Internships link button is clicked to redirect to the search page with the relevant criteria
    /// </summary>
    /// <param name="sender">Link button raising the event</param>
    /// <param name="e">Standard Event Argument</param>
    protected void TutorialVideoLink_Click(object sender, EventArgs e)
    {
      Response.RedirectToRoute("CareerTutorial");
    }
	}
}
