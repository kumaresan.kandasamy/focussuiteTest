﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Controls.Server;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Messages.AuthenticationService;
using Focus.Services.Core;
using Framework.Core;

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class ForgotPassword2 : UserControlBase
	{
		 <summary>
		 ForgottenPasswordModal control.
		 </summary>
		 <remarks>
		 Auto-generated field.
		 To modify move field declaration from designer file to code-behind file.
		 </remarks>
		protected global::AjaxControlToolkit.ModalPopupExtender ForgotPasswordModal;
		protected const string CreateOwnSecurityQuestionValue = "Own";

		 <summary>
		 Handles the Load event of the Page control.
		 </summary>
		 <param name="sender">The source of the event.</param>
		 <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
				ApplyTheme();
				LocaliseUI();
				BindSecurityQuestionDropDown();
			}
		}

		 <summary>
		 Handles the Click event of the btnRequestPassword control.
		 </summary>
		 <param name="sender">The source of the event.</param>
		 <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void btnRequestPassword_Click(object sender, EventArgs e)
		{
			try
			{
				var emailAddress = (ForgotPasswordEmailAddressTextbox.Text ?? string.Empty).Trim();

				var applicationPath = App.Settings.Module == FocusModules.Explorer
					? App.Settings.ExplorerApplicationPath
					: App.Settings.CareerApplicationPath;

				var resetPasswordUrl = string.Concat(applicationPath, UrlBuilder.ResetPassword(false));

				var pinRegistrationUrl = (App.Settings.Theme == FocusThemes.Education)
					? string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.PinRegistration(false))
					: string.Empty;

				string securityQuestion = "", securityAnswer = "";
				long? securityQuestionId = null;
				int dayOfBirth = 0, monthOfBirth = 0;

				if (ExtraQuestionsPlaceHolder.Visible)
				{
					securityQuestion = FPSecurityQuestionDropDown.SelectedValue == CreateOwnSecurityQuestionValue
						? FPSecurityQuestionTextBox.Text
						: null;

					securityQuestionId = FPSecurityQuestionDropDown.SelectedValue == CreateOwnSecurityQuestionValue
						? (long?)null
						: FPSecurityQuestionDropDown.SelectedValueToInt();
					securityAnswer = SecurityAnswerTextBox.Text;

					dayOfBirth = int.Parse(DayOfBirthTextBox.Text);
					monthOfBirth = int.Parse(MonthOfBirthDropDownList.SelectedValue);
				}

				var attemptsLeft = ServiceClientLocator.AuthenticationClient(App).ProcessForgottenPassword(string.Empty, emailAddress, resetPasswordUrl, App.Settings.Module, securityQuestion, securityQuestionId, securityAnswer, dayOfBirth, monthOfBirth, pinRegistrationUrl);
				if (attemptsLeft.IsNull())
				{
					ShowResult(false, CodeLocalise("EmailSent", "Email has been successfully sent."));
				}
				else
				{
					var message = attemptsLeft.Value == 1
						? CodeLocalise("FailedAttemptLeft", "You have 1 more attempt to answer correctly before your account becomes blocked")
						: CodeLocalise("FailedAttemptsLeft", "You have {0} more attempts to answer correctly before your account becomes blocked", attemptsLeft.Value);

					ShowResult(true, message);
				}
			}
			catch (ApplicationException ex)
			{
				var errorType = ex.GetErrorType();

				switch (errorType)
				{
					case ErrorTypes.ValidationFailed:
					case ErrorTypes.UserBlocked:
						ShowResult(true, CodeLocalise("Global.OnHoldMessage", "Your account has been placed on hold. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> for further assistance."));
						break;

					default:
						ShowResult(true, CodeLocalise("ResetPasswordError.Text", "No match to the email address entered was found. Please check and try again or contact your administration team to request an invitation to register."));
						break;
				}
			}

			ForgotPasswordModal.Show();
		}

		 <summary>
		 Shows the result of the reset password operation on the page.
		 </summary>
		 <param name="isError">if set to <c>true</c> [is error].</param>
		 <param name="message">The message.</param>
		private void ShowResult(bool isError, string message)
		{
			lblResetPassword.Text = message;
			lblResetPassword.CssClass = isError ? "inPageError" : "inPageMessage";
			lblResetPassword.Visible = true;
		}

		 <summary>
		 Localises the UI.
		 </summary>
		private void LocaliseUI()
		{
			EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address is not correct format");
			ConfirmEmailAddressRequired.ErrorMessage = CodeLocalise("ConfirmEmailAddress.RequiredErrorMessage", "Confirm email address is required");
			ConfirmEmailAddressCompare.ErrorMessage = CodeLocalise("ConfirmEmailAddress.CompareErrorMessage", "Email addresses must match");
			btnRequestPassword.Text = App.Settings.Theme == FocusThemes.Workforce
				? CodeLocalise("Global.RequestPassword.Text", "Request password reset")
				: CodeLocalise("Global.RequestPassword.Text:Education", "Request password/PIN reset");

			DayOfBirthTextBoxRegularExpressionValidator.ErrorMessage = CodeLocalise("DayOfBirthTooLong.ErrorMessage", "The day of birth must be 2 digits");
			SecurityQuestionRequiredFieldValidatro.ErrorMessage = CodeLocalise("SecurityQuestionRequiredFieldValidator.ErrorMessage", "Security question is required");
			SecurityQuestionTextBoxRequiredFieldValidator.ErrorMessage = CodeLocalise("SecurityQuestionTextBoxRequiredFieldValidator.ErrorMessage", "Security question is required");
			SecurityAnswerTextBoxRequiredFieldValidator.ErrorMessage = CodeLocalise("SecurityAnswerRequiredFieldValidator.ErrorMessage", "Security answer is required");
			DayOfBirthTextBoxRequiredFieldValidator.ErrorMessage = CodeLocalise("DayOfBirthRequired.ErrorMessage", "Day of birth is required");
			MonthOfBirthRequiredFieldValidator.ErrorMessage = CodeLocalise("MonthOfBirthRequired.ErrorMessage", "Month of birth is required");

			 ReSharper disable LocalizableElement
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = true, Text = HtmlLocalise("SelectMonth.Text", "- select month -"), Value = "0" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.January.Label", "January"), Value = "1" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.February.Label", "February"), Value = "2" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.March.Label", "March"), Value = "3" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.April.Label", "April"), Value = "4" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.May.Label", "May"), Value = "5" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.June.Label", "June"), Value = "6" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.July.Label", "July"), Value = "7" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.August.Label", "August"), Value = "8" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.September.Label", "September"), Value = "9" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.October.Label", "October"), Value = "10" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.November.Label", "November"), Value = "11" });
			MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.December.Label", "December"), Value = "12" });
			 ReSharper restore LocalizableElement
		}

		 <summary>
		 Applies the theme.
		 </summary>
		private void ApplyTheme()
		{
			ExtraQuestionsPlaceHolder.Visible = App.Settings.EnableCareerPasswordSecurity;
			if (App.Settings.Theme != FocusThemes.Education)
			{
				PasswordHelpInfoLabel.LocalisationKey = App.Settings.EnableCareerPasswordSecurity ? "PasswordWithSecurityHelpInfo.Text" : "PasswordHelpInfo.Text";
				PasswordHelpInfoLabel.DefaultText = App.Settings.EnableCareerPasswordSecurity
					? "When you enter your email address and answer the security questions correctly, #FOCUSCAREER# will send you an email containing a link to a web page where you can reset your password. The required security information is what we already know about you from account registration or by subsequent account updates. You will be given 3 attempts to provide the correct information before your account becomes locked.<br /><br />Once you have reset your password, return to the login page and use it to log in as usual. Once you have logged in, remember you can change your username, password and security questions in My Account.<br /><br />If you do not receive the password reset email or your account has become locked, please email <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> for further assistance."
					: "To request a password reset, please enter your email address below. #FOCUSCAREER# will send you an email containing a link to a web page where you can reset your password.";
			}
			else
			{
				ForgotPasswordTitleLabel.DefaultText = "Forgot your Password/PIN?";
				PasswordHelpInfoLabel.DefaultText = "When you enter your email address, and have already registered, Focus/Career will send you an email containing a link to a web page where you can reset your password. Once you have reset your password, return to the login page and use it to log in as usual. If you have not registered, the system will resend the PIN number assigned to you in order for to complete registration. Once you have logged in, remember you can change your username, password and security questions in My Account.<br /><br />If you do not receive this email or cannot access your account, please contact support at <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a>.";
			}
		}

		 <summary>
		 Binds the security question drop down.
		 </summary>
		private void BindSecurityQuestionDropDown()
		{
			FPSecurityQuestionDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions), "0", CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"), "0");

			FPSecurityQuestionDropDown.Items.Add(new ListItem(CodeLocalise("CreateOwnSecurityQuestion.DropDownValue", "Create own security question"), CreateOwnSecurityQuestionValue));
		}

		SecurityQuestionResponse Questions
		{
		  get { return (SecurityQuestionResponse)Session["SecurityQuestionResponse"]; }
		  set { Session["SecurityQuestionResponse"] = value; }
		}

		string InputUsername
		{
		  get { return (string) Session["Username"]; }
		  set { Session["Username"] = value; }
		}

		string InputEmail
		{
		  get { return (string)Session["Email"]; }
		  set { Session["Email"] = value; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
		  //ForgotPasswordFullModal.Hide();
			
		  if (App.Settings.EnableCareerPasswordSecurity)
		  {
		    if (!IsPostBack)
		    {
		      //EmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
		      LocaliseUi();
		    }
		  }
		  else
		  {
		    //ForgotPasswordFullModal.Show();
		  }
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
		  if (Questions != null && Questions.SecurityQuestion1 != null)
		  {
		    var QuestionDropDown = (DropDownList)FindControl("FPSecurityQuestionDropDown");
		    QuestionDropDown.Items.Clear();
		    QuestionDropDown.Items.Add(Questions.SecurityQuestion1);
		  }
		}

		private void LocaliseUi()
		{
		  UserNameRegEx.ErrorMessage = CodeLocalise("UserNameRegEx.ErrorMessage", "Username is not the correct format");
		  EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddressRegEx.ErrorMessage", "Email address is not the correct format");
		  EitherFieldValidator.ErrorMessage = CodeLocalise("EitherFieldValidator.RequiredErrorMessage", "Please enter either a username or email address");
		  NotBothFieldsValidator.ErrorMessage = CodeLocalise("EitherFieldValidator.RequiredErrorMessage", "Please enter just one field, but not both");
		  SubmitButton.Text = CodeLocalise("Global.Submit.Text", "Submit");

		  //EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
		  EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address is not correct format");
		  //ConfirmEmailAddressRequired.ErrorMessage = CodeLocalise("ConfirmEmailAddress.RequiredErrorMessage", "Confirm email address is required");
		  //ConfirmEmailAddressCompare.ErrorMessage = CodeLocalise("ConfirmEmailAddress.CompareErrorMessage", "Email addresses must match");
		  btnRequestPassword.Text = App.Settings.Theme == FocusThemes.Workforce
		    ? CodeLocalise("Global.RequestPassword.Text", "Request password reset")
		    : CodeLocalise("Global.RequestPassword.Text:Education", "Request password/PIN reset");

		  DayOfBirthTextBoxRegularExpressionValidator.ErrorMessage = CodeLocalise("DayOfBirthTooLong.ErrorMessage", "The day of birth must be 2 digits");
		  SecurityQuestionRequiredFieldValidatro.ErrorMessage = CodeLocalise("SecurityQuestionRequiredFieldValidator.ErrorMessage", "Security question is required");
		  SecurityQuestionTextBoxRequiredFieldValidator.ErrorMessage = CodeLocalise("SecurityQuestionTextBoxRequiredFieldValidator.ErrorMessage", "Security question is required");
		  SecurityAnswerTextBoxRequiredFieldValidator.ErrorMessage = CodeLocalise("SecurityAnswerRequiredFieldValidator.ErrorMessage", "Security answer is required");
		  DayOfBirthTextBoxRequiredFieldValidator.ErrorMessage = CodeLocalise("DayOfBirthRequired.ErrorMessage", "Day of birth is required");
		  MonthOfBirthRequiredFieldValidator.ErrorMessage = CodeLocalise("MonthOfBirthRequired.ErrorMessage", "Month of birth is required");

		  // ReSharper disable LocalizableElement
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = true, Text = HtmlLocalise("SelectMonth.Text", "- select month -"), Value = "0" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.January.Label", "January"), Value = "1" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.February.Label", "February"), Value = "2" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.March.Label", "March"), Value = "3" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.April.Label", "April"), Value = "4" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.May.Label", "May"), Value = "5" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.June.Label", "June"), Value = "6" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.July.Label", "July"), Value = "7" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.August.Label", "August"), Value = "8" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.September.Label", "September"), Value = "9" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.October.Label", "October"), Value = "10" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.November.Label", "November"), Value = "11" });
		  MonthOfBirthDropDownList.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.December.Label", "December"), Value = "12" });
		  // ReSharper restore LocalizableElement

		}
		/// <summary>
		/// Handles the Click event of the btnRequestPassword control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void btnRequestPassword_Click(object sender, EventArgs e)
		{
		  try
		  {
		    //var emailAddress = (TextBox1.Text ?? string.Empty).Trim();

		    var applicationPath = App.Settings.Module == FocusModules.Explorer
		      ? App.Settings.ExplorerApplicationPath
		      : App.Settings.CareerApplicationPath;

		    var resetPasswordUrl = string.Concat(applicationPath, UrlBuilder.ResetPassword(false));

		    var pinRegistrationUrl = (App.Settings.Theme == FocusThemes.Education)
		      ? string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.PinRegistration(false))
		      : string.Empty;
				
		    string securityQuestion = "", securityAnswer = "";
		    long? securityQuestionId = null;
		    int dayOfBirth = 0, monthOfBirth = 0;

		    if (ExtraQuestionsPlaceHolder.Visible)
		    {
		      securityQuestion = FPSecurityQuestionDropDown.SelectedValue == CreateOwnSecurityQuestionValue
		        ? FPSecurityQuestionTextBox.Text
		        : null;

		      securityQuestionId = FPSecurityQuestionDropDown.SelectedValue == CreateOwnSecurityQuestionValue
		        ? (long?)null
		        : FPSecurityQuestionDropDown.SelectedValueToInt();
		      securityAnswer = SecurityAnswerTextBox.Text;

		      dayOfBirth = int.Parse(DayOfBirthTextBox.Text);
		      monthOfBirth = int.Parse(MonthOfBirthDropDownList.SelectedValue);
		    }

		    var attemptsLeft = ServiceClientLocator.AuthenticationClient(App).ProcessForgottenPassword(InputUsername, InputEmail, resetPasswordUrl, App.Settings.Module, securityQuestion, securityQuestionId, securityAnswer, dayOfBirth, monthOfBirth, pinRegistrationUrl);
		    if (attemptsLeft.IsNull())
		    {
		      ShowResult(false, CodeLocalise("EmailSent", "Email has been successfully sent."));
		    }
		    else
		    {
		      var message = attemptsLeft.Value == 1
		        ? CodeLocalise("FailedAttemptLeft", "You have 1 more attempt to answer correctly before your account becomes blocked")
		        : CodeLocalise("FailedAttemptsLeft", "You have {0} more attempts to answer correctly before your account becomes blocked", attemptsLeft.Value);

		      ShowResult(true, message);
		    }
		  }
		  catch (ApplicationException ex)
		  {
		    var errorType = ex.GetErrorType();

		    switch (errorType)
		    {
		      case ErrorTypes.ValidationFailed:
		      case ErrorTypes.UserBlocked:
		        ShowResult(true, CodeLocalise("Global.OnHoldMessage", "Your account has been placed on hold. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> for further assistance."));
		        break;

		      default:
		        ShowResult(true, CodeLocalise("ResetPasswordError.Text", "No match to the email address entered was found. Please check and try again or contact your administration team to request an invitation to register."));
		        break;
		    }
		  }

		  ForgotPasswordModal.Show();
		}

		/// <summary>
		/// Shows the result of the reset password operation on the page.
		/// </summary>
		/// <param name="isError">if set to <c>true</c> [is error].</param>
		/// <param name="message">The message.</param>
		private void ShowResult(bool isError, string message)
		{
		  lblResetPassword.Text = message;
		  lblResetPassword.CssClass = isError ? "inPageError" : "inPageMessage";
		  lblResetPassword.Visible = true;
		}

		/// <summary>
		/// Server validation to check either field is entered
		/// </summary>
		/// <param name="source">The custom validator</param>
		/// <param name="args">Validation arguments</param>
		protected void EitherFieldValidator_OnServerValidate(object source, ServerValidateEventArgs args)
		{
		  var userNameEntered = ForgotPasswordUserNameTextBox.Text.Trim().Length > 0;
		  var emailAddressEntered = ForgotPasswordEmailAddressTextBox.Text.Trim().Length > 0;

		  args.IsValid = (userNameEntered || emailAddressEntered);
		}

		/// <summary>
		/// Server validation to check that not both fields is entered
		/// </summary>
		/// <param name="source">The custom validator</param>
		/// <param name="args">Validation arguments</param>
		protected void NotBothFieldsValidator_OnServerValidate(object source, ServerValidateEventArgs args)
		{
		  var userNameEntered = ForgotPasswordUserNameTextBox.Text.Trim().Length > 0;
		  var emailAddressEntered = ForgotPasswordEmailAddressTextBox.Text.Trim().Length > 0;

		  args.IsValid = !(userNameEntered && emailAddressEntered);
		}

		/// <summary>
		/// Handles the Click event of the SubmitButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SubmitButton_Click(object sender, EventArgs e)
		{
		  if (App.Settings.Module != FocusModules.Talent)
		  {
		    InputUsername = ForgotPasswordUserNameTextBox.Text.Trim();
		    InputEmail = ForgotPasswordEmailAddressTextBox.Text.Trim();
		    Questions = ServiceClientLocator.AuthenticationClient(App)
		      .GetSecurityQuestions(InputEmail, InputUsername, App.Settings.Module);

		    if (Questions != null && !string.IsNullOrWhiteSpace(Questions.SecurityQuestion1))
		    {
		      ForgotPassword2Modal.Hide();
		      ForgotPasswordModal.Show();
		      //ForgotPasswordFullModal.Hide();
		    }
		    else
		    {
		      //ForgotPasswordFullModal.Hide();
		      ForgotPassword2Modal.Show();
		      var helpInfo = (LocalisedLabel)FindControl("ErrorDescription");
		      helpInfo.DefaultText =
		        "No match could be found. Please check and try again or contact #SUPPORTEMAIL# or #SUPPORTPHONE# for further assistance";
		    }
		  }
		}
	}
}