﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.ChangePassword" %>

<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Controls/ConfirmationModal.ascx" %>

<hr/>
<table width="100%">
	<tr>
		<td><b><%= HtmlLocalise("Title", "Change password") %></b></td>
		<td style="text-align: right">
			<a id="HideChangePasswordContent" href="#" onclick="javascript:closeChangePassword();" style="display: none;"><%= HtmlLocalise("Hide.Text", "Hide")%></a>
			<a id="ShowChangePasswordContent" href="#" onclick="javascript:expandChangePassword();"><%= HtmlLocalise("Show.Text", "Show")%></a>
		</td>
	</tr>
</table>
<p></p>
<div id="ChangePasswordContent" class="myAccountContent" style="display: none;">
	<table width="500px">
		<tr>
			<td width="250px"><%= HtmlRequiredLabel(PasswordTextBox, "Password", "Password")%></td>
			<td>
				<asp:TextBox ID="PasswordTextBox" runat="server" ClientIDMode="Static" Width="98%" TextMode="Password" TabIndex="11" MaxLength="20" /><br />				
				<asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="PasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangePassword"/>
			</td>
		</tr>
		<tr>
			<td><%= HtmlRequiredLabel(NewPasswordTextBox, "NewPassword", "New password")%></td>
			<td>
				<asp:TextBox ID="NewPasswordTextBox" runat="server" ClientIDMode="Static" Width="98%" TextMode="Password" TabIndex="12" MaxLength="20" /><br />				
				<asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangePassword"/>
				<asp:RegularExpressionValidator ID="NewPasswordRegEx" runat="server" ControlToValidate="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangePassword"/>
			</td>
		</tr>
		<tr>
			<td><%= HtmlRequiredLabel(ConfirmNewPasswordTextBox, "ConfirmNewPassword", "Confirm new password")%></td>
			<td>
				<asp:TextBox ID="ConfirmNewPasswordTextBox" runat="server" ClientIDMode="Static" Width="98%" TextMode="Password" TabIndex="13" MaxLength="20" />
				<asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" 
																		ValidationGroup="ChangePassword"/>
				<asp:CompareValidator ID="ConfirmNewPasswordCompare" runat="server" ControlToValidate="ConfirmNewPasswordTextBox" ControlToCompare="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" 
															ValidationGroup="ChangePassword"/>
				<asp:CustomValidator ID="ChangePasswordValidator" runat="server" CssClass="error" Display="Dynamic" />
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: right"><asp:Button ID="SaveButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel2" ValidationGroup="ChangePassword" TabIndex="14" OnClick="SaveButton_Clicked" /></td>
		</tr>
	</table>
</div>

<asp:HiddenField ID="ChangePasswordExpanded" runat="server" value="0" ClientIDMode="Static"/>

<%-- Confirmation Modal --%>
<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />

<script type="text/javascript">
	$(document).ready(function () {
		if ($('#ChangePasswordExpanded').val() == "0") {
			closeChangePassword();
		}
		else {
			expandChangePassword();
		}
	});

	function closeChangePassword() {
		$('#ChangePasswordContent').hide();
		$('#HideChangePasswordContent').hide();
		$('#ShowChangePasswordContent').show();
		$('#ChangePasswordExpanded').val("0");
	}

	function expandChangePassword() {
		$('#ChangePasswordContent').show();
		$('#HideChangePasswordContent').show();
		$('#ShowChangePasswordContent').hide();
		$('#ChangePasswordExpanded').val("1");
	}
</script>