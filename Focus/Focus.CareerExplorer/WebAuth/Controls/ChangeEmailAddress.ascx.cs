﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core.Models;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class ChangeEmailAddress : UserControlBase
	{
		public event ScreenNameEventHandler ScreenNameChanged;
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				NewEmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
				NewEmailAddressRegEx.Enabled = NewEmailAddressRegEx.ValidationExpression.IsNotNullOrEmpty();
				LocaliseUI();
			}
		}

		/// <summary>
		/// Handles the Clicked event of the SaveButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveButton_Clicked(object sender, EventArgs e)
		{
			var screenName = ScreenNameTextbox.TextTrimmed();
			var currentEmail = EmailAddressTextbox.TextTrimmed();
			var newEmail = NewEmailAddressTextbox.TextTrimmed();

			// Update username
			try
			{
				ServiceClientLocator.AccountClient(App).ChangeUsername(currentEmail, newEmail);
			}
			catch (Exception ex)
			{
				ChangeEmailValidator.IsValid = false;
				ChangeEmailValidator.ErrorMessage = ex.Message;
				return;
			}

			// Update email address and screen name
			ServiceClientLocator.AccountClient(App).UpdateScreenNameAndEmailAddress(screenName, newEmail);
			
			// Update user context
			App.User.ScreenName = screenName;
			App.User.EmailAddress = newEmail;
			App.User.Save();

			// Clear email values for security
			EmailAddressTextbox.Text = NewEmailAddressTextbox.Text = ConfirmEmailAddressTextbox.Text = "";

			// Throw event so username in master page can be changed
			OnScreenNameChanged(EventArgs.Empty);

			ConfirmationModal.Show(CodeLocalise("ScreenNameAndEmailAddressSuccessfulChange.Title", "Screen name and email address changed"),
															 CodeLocalise("ScreenNameAndEmailAddressSuccessfulChange.Body", "You have successfully changed your screen name and email address."),
															 CodeLocalise("Global.Close.Text", "Close"));
		}

		/// <summary>
		/// Binds the specified model.
		/// </summary>
		/// <param name="model">The model.</param>
		public void Bind(MyAccountModel model)
		{
			ScreenNameTextbox.Text = model.ScreenName;
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ScreenNameRequired.ErrorMessage = CodeLocalise("ScreenNameRequired.RequiredErrorMessage", "Screen name is required");
			EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			NewEmailAddressRequired.ErrorMessage = CodeLocalise("NewEmailAddress.RequiredErrorMessage", "New email address is required");
			NewEmailAddressRegEx.ErrorMessage = CodeLocalise("NewEmailAddress.RegExErrorMessage", "New email address is not correct format");
			ConfirmEmailAddressRequired.ErrorMessage = CodeLocalise("ConfirmEmailAddress.RequiredErrorMessage", "Confirm email address is required");
			ConfirmEmailAddressCompare.ErrorMessage = CodeLocalise("ConfirmEmailAddress.CompareErrorMessage", "Email addresses must match");
			SaveButton.Text = CodeLocalise("Global.Save.Text", "Save");
		}

		public delegate void ScreenNameEventHandler(object o, EventArgs e);

    /// <summary>
    /// Raises the <see cref="E:ScreenNameChanged"/> event.
    /// </summary>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public virtual void OnScreenNameChanged(EventArgs e)
		{
			if (ScreenNameChanged != null)
				ScreenNameChanged(this, e);
		}
	}
}