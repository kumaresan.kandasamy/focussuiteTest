﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="LogInOrRegister.ascx.cs"
    Inherits="Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister" %>
<%@ Register Src="RegistrationConfirmation.ascx" TagName="RegistrationConfirmation"
    TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Controls/ConfirmationModal.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<script src="<%= ResolveUrl("~/Assets/Scripts/moment.min.js") %>" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js" type="text/javascript"></script>
<asp:HiddenField ID="LogInModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="LogInModal" runat="server" BehaviorID="LogInModal" TargetControlID="LogInModalDummyTarget"
    PopupControlID="LoginModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="LoginModalPanel" runat="server" CssClass="modal" Style="display: none;">
    <asp:Panel runat="server" ID="LogInWorkforcePanel" Visible="False">
        <div class="lightboxmodal lightbox FocusCareer loginModal" style="max-height: 404px;
            overflow: auto">
            <div role="presentation" class="col-sm-5 col-md-5 clearfix">
                <div>
                    <asp:Panel ID="panLoginNote" runat="server" ScrollBars="Auto" Style="height: 380px;">
                        <%= HtmlLocalise("LoginNote.Label", "<h1>Your Job Search Starts With Your Resume</h1>The first step toward a job is your resume. Most #BUSINESSES#:LOWER want to see your resume before they'll even consider you for the job. Your resume is a great way to start planning and collecting the necessary information for a successful job search. <br /><br />That's why #FOCUSCAREER# starts by helping you build a complete, detailed resume that includes your skills and experience. If you've already written your resume, you can upload or paste it into #FOCUSCAREER#. We'll help you improve it! <br /><br />Think of your resume as a work in progress - you can go back and change it to better fit a job match or whenever your situation changes, like completing a training course. Remember, you only get one chance to make a first impression... and that's why your resume needs to be the best it can be. <br /><br />Login or create your new account, then let's get started!")%>
                    </asp:Panel>
                </div>
            </div>
            <div style="vertical-align: top;" class="col-sm-7 col-md-7 clearfix">
                <div class="LoginRegisterPanel" style="margin-left: 10px" role="presentation">
                    <asp:Panel ID="CareerLoginPanel" runat="server">
                        <div>
                            <div class="LoginRegisterHeaderPanel on" style="font-size: 25px;">
                                <%= HtmlLocalise("LoginRegisterHeader1.Label", "Log in")%></div>
                        </div>
                        <asp:Panel ID="panLogin" runat="server" DefaultButton="LogInButton">
                            <div role="presentation" class="content">
                                <div>
                                    <div>
                                        <span class="inFieldLabel">
                                            <label for="UserNameTextBox" style="width: 275px">
                                                <%= HtmlLocalise("UserName.Label", "Username")%></label></span>
                                        <asp:TextBox ID="UserNameTextBox" runat="server" ClientIDMode="Static" Width="275"
                                            CssClass="loginInput" AutoCompleteType="Disabled" />
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserNameTextBox"
                                            SetFocusOnError="true" Display="None" ValidationGroup="Login" />
                                        <asp:Image ID="LoginHelpToolTip" runat="server" CssClass="toolTipNew" AlternateText="Login Help Tool Tip" /></div>
                                </div>
                                <div>
                                    <div>
                                        <span class="inFieldLabel">
                                            <label for="PasswordTextBox" style="width: 275px">
                                                <%= HtmlLocalise("Password.Label", "Password")%></label></span>
                                        <asp:TextBox ID="PasswordTextBox" runat="server" ClientIDMode="Static" TextMode="Password"
                                            CssClass="loginInput" Width="275px" AutoCompleteType="Disabled" MaxLength="20"
                                            ValidationGroup="Login" />
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="PasswordTextBox"
                                            SetFocusOnError="true" Display="None" ValidationGroup="Login" />
                                        <focus:DoubleClickDisableButton ID="LogInButton" ClientIDMode="Static" Text="Sign in &#187;"
                                            runat="server" class="buttonLevel2" ValidationGroup="Login" OnClick="LogInButton_Click"
                                            Style="margin-left: 10px" />
                                    </div>
                                </div>
                                <div style="clear: left">
                                    <span class="instructionalText">
                                        <%= HtmlLocalise("PasswordInstructions", "Password is case-sensitive.")%>
                                    </span>
                                </div>
                                <div style="clear: left">
                                    <asp:LinkButton ID="lnkForgotPasword" runat="server" OnClientClick="DisplayForgotPasswordModal(); return false;"
                                        Text="Forgot your password"></asp:LinkButton>
                                </div>
                                <div>
                                    <focus:AjaxValidationSummary ID="LogInValidationSummary" runat="server" DisplayMode="List"
                                        CssClass="error" ValidationGroup="Login" />
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="RegisterSettingPanel" runat="server">
                        <div>
                            <div class="LoginRegisterHeaderPanel" style="font-size: 25px;">
                                <a id="RegisterForAnAccountLink" href="#" onclick="javascript:$find('LogInModal').hide();$find ('RegisterModal').show();">
                                    <%= HtmlLocalise("Register.Title", "Register for an account") %></a></div>
                        </div>
                        <div class="LoginRegisterHeaderContent">
                            <%= HtmlLocalise("LoginRegisterHeader2.Label", "Registering only takes two steps, and allows you to create or upload a resume and see all job postings, including ones recommended for you based on your skills and expertise.")%>
                        </div>
                    </asp:Panel>
                    <div>
                        <div class="LoginRegisterHeaderPanel" style="font-size: 25px;">
                            <asp:LinkButton runat="server" ID="SearchNoLogin" OnClick="SearchJobWithoutReg_Click"
                                CausesValidation="false" /></div>
                    </div>
                    <div class="LoginRegisterHeaderContent">
                        <%= HtmlLocalise("LoginRegisterHeader3.Label", "You may search for jobs without registering, but you will not be able to view the full range of jobs available, and results will not be tailored to your skills and expertise.")%>
                    </div>
                    <%--<tr>
						<td colspan="2">
							<br />
						</td>
					</tr>--%>
                </div>
                <asp:CustomValidator ID="LoginValidator" runat="server" Display="None" ValidationGroup="Login" />
            </div>
        </div>
    </asp:Panel>
</asp:Panel>
<asp:HiddenField ID="RegisterModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="RegisterModal" runat="server" BehaviorID="RegisterModal"
    TargetControlID="RegisterModalDummyTarget" PopupControlID="RegisterModalPanel"
    BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<div id="RegisterModalPanel" class="modal" style="position: fixed; z-index: 100001;
    left: 480.5px; top: 68.5px; display: none;">
    <div class="lightboxmodal lightbox FocusCareer loginAndRegister" id="registerAccountLightbox"
        style="position: relative">
        <div class="lightboxOptions" role="presentation">
            <div class="col-sm-5 col-md-5 clearfix">
                <asp:Panel ID="panRegisterStep1Heading" runat="server">
                    <h1>
                        <%= HtmlLocalise("Heading1.Label", "Register for an account")%>
                    </h1>
                </asp:Panel>
                <asp:Panel ID="panRegisterStep2Heading" runat="server" Visible="false">
                    <h1>
                        <asp:Literal runat="server" ID="Heading2Literal"></asp:Literal>
                    </h1>
                </asp:Panel>
            </div>
            <div class="steppedProcessSubhead col-sm-2 col-md-2 clearfix">
                <asp:Panel ID="panRegisterStep1" runat="server">
                    <%--<%= HtmlLocalise("steppedProcessSubhead.Label", "<strong>Step 1</strong> of 2")%>--%>
                    <asp:Label runat="server" ID="RegistrationLabel"></asp:Label>
                </asp:Panel>
                <%--	<asp:Panel ID="panRegisterStep2" runat="server" Visible="false">
						<%= HtmlLocalise("steppedProcessSubhead.Label", "<strong>Step 2</strong> of 2")%>
					</asp:Panel>--%>
            </div>
            <div style="text-align: right" class="col-sm-5 col-md-5 clearfix">
                <asp:Panel ID="panSignInLinks" runat="server">
                    <a href="#" onclick="javascript:$find('LogInModal').show();$find('RegisterModal').hide();">
                        <%= HtmlLocalise("Global.LogIn.Text", "Sign in")%></a> |
                    <asp:LinkButton runat="server" ID="SearchJobWithoutReg" OnClick="SearchJobWithoutReg_Click"
                        CausesValidation="false" />
                </asp:Panel>
            </div>
        </div>
        <asp:Panel ID="RegisterStep1Panel" runat="server" ScrollBars="Auto" Style="height: 380px;
            position: relative; clear: left">
            <div role="presentation">
                <%= HtmlRequiredLabel("RedAsterik.Label", "")%>
                <span style="color: #264C69;">
                    <%= HtmlLocalise("instructionalText.Label", "required fields")%></span>
            </div>
            <div style="width: 100%;" role="presentation">
                <table style="width: 100%;" role="presentation">
                    <tr id="ScreenNameRow" runat="server">
                        <td style="width: 209px;">
                            <%= HtmlRequiredLabel(ScreenNameTextbox, "ScreenName", "Screen name")%>
                        </td>
                        <td style="width: 300px;">
                            <asp:TextBox ID="ScreenNameTextbox" runat="server" ClientIDMode="Static" Width="300px"
                                MaxLength="20" AutoCompleteType="Disabled" />
                            <asp:RequiredFieldValidator ID="ScreenNameRequired" runat="server" ControlToValidate="ScreenNameTextbox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                        </td>
                    </tr>
                </table>
                <div id="EmailRow" runat="server">
                    <div class="col-sm-4 col-md-3 clearfix">
                        <%= HtmlRequiredLabel(EmailAddressTextbox, "EmailAddress", "Email address")%></div>
                    <div class="col-sm-8 col-md-9 clearfix">
                        <div class="col-md-6">
                            <asp:TextBox ID="EmailAddressTextbox" runat="server" ClientIDMode="Static" Width="300px"
                                AutoCompleteType="Disabled" />
                            <br />
                            <asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailAddressTextbox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                            <asp:RegularExpressionValidator ID="EmailAddressRegEx" runat="server" ControlToValidate="EmailAddressTextbox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" /></div>
                        <div class="col-md-6 instructionalText clearfix">
                            <asp:PlaceHolder runat="server" ID="RegistrationEmailHelpText">
                                <%= HtmlLocalise("Email.HelpText1", "The email address you provide will be used as your account username.") %><br />
                                <%= HtmlLocalise("Email.HelpText2", "No email account? Use a free service:")%><br />
                                <a href="http://webmail.aol.com" target="_blank">
                                    <%= HtmlLocalise("AOLMail", "AOLMail")%></a>, <a href="http://mail.google.com" target="_blank">
                                        <%= HtmlLocalise("Gmail", "Gmail")%></a>, <a href="https://login.live.com/" target="_blank">
                                            <%= HtmlLocalise("Windowslive", "Windows Live")%></a>, <a href="http://mail.yahoo.com"
                                                target="_blank">
                                                <%= HtmlLocalise("Yahoo", "Yahoo!")%></a> </asp:PlaceHolder>
                        </div>
                    </div>
                </div>
                <div id="ConfirmEmailAddressRow" runat="server">
                    <div class="col-sm-4 col-md-3 clearfix">
                        <%= HtmlRequiredLabel(ConfirmRegEmailAddressTextbox, "EmailAddressConfirmation", "Re-enter email address")%>
                    </div>
                    <div class="col-sm-8 col-md-9 clearfix">
                        <asp:TextBox ID="ConfirmRegEmailAddressTextbox" runat="server" ClientIDMode="Static"
                            Width="300px" AutoCompleteType="Disabled" />
                        <br />
                        <asp:RequiredFieldValidator ID="ConfirmEmailAddressRequired" runat="server" ControlToValidate="ConfirmRegEmailAddressTextbox"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                        <asp:CompareValidator ID="ConfirmEmailAddressCompare" runat="server" ControlToValidate="ConfirmRegEmailAddressTextbox"
                            ControlToCompare="EmailAddressTextbox" SetFocusOnError="true" Display="Dynamic"
                            CssClass="error" ValidationGroup="RegisterStep1" />
                    </div>
                </div>
                <div id="PasswordRow" runat="server" style="clear: left">
                    <div class="col-sm-4 col-md-3 clearfix">
                        <%= HtmlRequiredLabel(NewPasswordTextBox, "NewPassword", "Password")%>
                    </div>
                    <div class="col-sm-8 col-md-9 clearfix">
                        <div class="col-md-6">
                            <asp:TextBox ID="NewPasswordTextBox" runat="server" ClientIDMode="Static" Width="300px"
                                TextMode="Password" MaxLength="20" /><br />
                            <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPasswordTextBox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                            <asp:RegularExpressionValidator ID="NewPasswordRegEx" runat="server" ControlToValidate="NewPasswordTextBox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                            <asp:CompareValidator ID="UserNameAndPasswordCompare" runat="server" ControlToValidate="NewPasswordTextBox"
                                ControlToCompare="EmailAddressTextbox" SetFocusOnError="true" Display="Dynamic"
                                Operator="NotEqual" CssClass="error" ValidationGroup="RegisterStep1" />
                        </div>
                        <div class="col-md-6 instructionalText clearfix">
                            <%= HtmlLocalise("PasswordInstructions", "6-20 characters; must include at least one number; must not contain spaces; password is case-sensitive.")%>
                        </div>
                    </div>
                </div>
                <div id="ConfirmPasswordRow" runat="server" style="clear: left">
                    <div class="col-sm-4 col-md-3 clearfix">
                        <%= HtmlRequiredLabel(ConfirmNewPasswordTextBox, "ConfirmNewPassword", "Re-enter password")%>
                    </div>
                    <div class="col-sm-8 col-md-9 clearfix">
                        <asp:TextBox ID="ConfirmNewPasswordTextBox" runat="server" ClientIDMode="Static"
                            Width="300px" TextMode="Password" MaxLength="20" /><br />
                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPasswordTextBox"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                        <asp:CompareValidator ID="ConfirmNewPasswordCompare" runat="server" ControlToValidate="ConfirmNewPasswordTextBox"
                            ControlToCompare="NewPasswordTextBox" SetFocusOnError="true" Display="Dynamic"
                            CssClass="error" ValidationGroup="RegisterStep1" />
                    </div>
                </div>
                <asp:PlaceHolder ID="WorkforcePlaceHolder1" runat="server">
                    <div id="SSNRow" runat="server" style="clear: left">
                        <div style="vertical-align: top;" class="col-sm-4 col-md-3">
                            <%= HtmlLabel(SocialSecurityNumberPart1TextBox, "SocialSecurityNumber", "Social Security Number (SSN)")%>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <div style="vertical-align: top;" class="col-md-6">
                                <asp:TextBox ID="SocialSecurityNumberPart1TextBox" runat="server" ClientIDMode="Static"
                                    title="SsnTextBox1" MaxLength="3" size="3" Width="25" onKeyup="autotab(this, 'SocialSecurityNumberPart2TextBox')"
                                    TextMode="Password" />
                                -
                                <asp:TextBox ID="SocialSecurityNumberPart2TextBox" runat="server" ClientIDMode="Static"
                                    MaxLength="2" size="2" title="SsnTextBox2" Width="15" onKeyup="autotab(this, 'SocialSecurityNumberPart3TextBox')"
                                    TextMode="Password" />
                                -
                                <asp:TextBox ID="SocialSecurityNumberPart3TextBox" runat="server" ClientIDMode="Static"
                                    title="SsnTextBox3" MaxLength="4" size="4" Width="30" TextMode="Password" /><br />
                                <asp:CustomValidator ID="SocialSecurityNumberValidator" runat="server" ClientValidationFunction="validateSsn"
                                    ValidationGroup="RegisterStep1" ValidateEmptyText="True" CssClass="error" SetFocusOnError="true"
                                    Display="Dynamic" />
                            </div>
                            <div class="col-md-6 instructionalText clearfix">
                                <%= HtmlLocalise("SocialSecurityNumber1.HelpText", "Your SSN will be stored securely. The SSN is used only to match your account to existing accounts and consolidate your records, when appropriate. This may include previous accounts for case-management, Unemployment Insurance, etc.")%>
                            </div>
                        </div>
                    </div>
                    <div id="SSN2Row" runat="server" style="clear: left">
                        <div class="col-sm-4 col-md-3">
                            <%= HtmlLabel(ConfirmSocialSecurityNumberPart1TextBox, "ConfirmSocialSecurityNumber", "Re-enter SSN")%>
                        </div>
                        <div class="col-sm-8 col-md-9">
                            <asp:TextBox ID="ConfirmSocialSecurityNumberPart1TextBox" runat="server" ClientIDMode="Static"
                                title="ConfirmSsnTextBox1" MaxLength="3" size="3" Width="25" onKeyup="autotab(this, 'ConfirmSocialSecurityNumberPart2TextBox')"
                                TextMode="Password" />
                            -
                            <asp:TextBox ID="ConfirmSocialSecurityNumberPart2TextBox" runat="server" ClientIDMode="Static"
                                title="ConfirmSsnTextBox2" MaxLength="2" size="2" Width="15" onKeyup="autotab(this, 'ConfirmSocialSecurityNumberPart3TextBox')"
                                TextMode="Password" />
                            -
                            <asp:TextBox ID="ConfirmSocialSecurityNumberPart3TextBox" runat="server" ClientIDMode="Static"
                                title="ConfirmSsnTextBox3" MaxLength="4" size="4" Width="30" TextMode="Password" /><br />
                            <asp:CustomValidator ID="ConfirmSocialSecurityNumberValidator" runat="server" ClientValidationFunction="validateConfirmSsn"
                                ValidationGroup="RegisterStep1" ValidateEmptyText="True" CssClass="error" SetFocusOnError="true"
                                Display="Dynamic" />
                        </div>
                    </div>
                </asp:PlaceHolder>
                <div id="SecurityQuestionRow" runat="server" style="clear: left">
                    <div class="col-sm-4 col-md-3">
                        <%= HtmlRequiredLabel(SecurityQuestionDropDown, "SecurityQuestion", "Security question")%>
                    </div>
                    <div class="col-sm-8 col-md-9">
                        <div style="position: relative;">
                            <asp:DropDownList ID="SecurityQuestionDropDown" runat="server" ClientIDMode="Static"
                                Width="308px" />
                        </div>
                        <div id="SecurityQuestionDiv" style="display: none; padding-top: 8px;">
                            <asp:TextBox ID="SecurityQuestionTextBox" runat="server" ClientIDMode="Static" Width="300px"
                                MaxLength="100" Title="Security Question Text" /></br>
                            <asp:CustomValidator ID="SecurityQuestionCustomValidator" runat="server" ControlToValidate="SecurityQuestionTextBox"
                                ClientValidationFunction="validateOwnSecurityQuestion" ValidationGroup="RegisterStep1"
                                ValidateEmptyText="True" CssClass="error" SetFocusOnError="true" Display="Dynamic" />
                        </div>
                        <asp:RequiredFieldValidator ID="SecurityQuestionRequired" runat="server" ControlToValidate="SecurityQuestionDropDown"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                    </div>
                </div>
                <div id="SecurityAnswerRow" runat="server" style="clear: left">
                    <div class="col-sm-4 col-md-3 clearfix">
                        <%= HtmlRequiredLabel(SecurityAnswerTextBox, "SecurityAnswer", "Security answer")%>
                    </div>
                    <div class="col-sm-8 col-md-9 clearfix">
                        <div class="col-md-6 clearfix">
                            <asp:TextBox ID="SecurityAnswerTextBox" runat="server" ClientIDMode="Static" Width="300px"
                                MaxLength="30" /><br />
                            <asp:RequiredFieldValidator ID="SecurityAnswerRequired" runat="server" ControlToValidate="SecurityAnswerTextBox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                        </div>
                        <div class="instructionalText col-md-6 clearfix">
                            <%= HtmlLocalise("SecurityAnswerInstructions", "Answer is case-sensitive.")%>
                        </div>
                    </div>
                </div>
                <asp:PlaceHolder runat="server" ID="SeekerAccountTypePlaceholder" Visible="false">
                    <div style="clear: left">
                        <div>
                            <%= HtmlRequiredLabel(SeekerAccountTypeDropDownList, "SeekerAccountType.Label", "Account Type")%>
                        </div>
                        <div>
                            <div style="position: relative;">
                                <asp:DropDownList ID="SeekerAccountTypeDropDownList" runat="server" ClientIDMode="Static"
                                    Width="308px" />
                            </div>
                            <asp:RequiredFieldValidator ID="SeekerAccountTypeRequired" runat="server" ControlToValidate="SeekerAccountTypeDropDownList"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1" />
                        </div>
                    </div>
                </asp:PlaceHolder>
                <div id="RecaptchaRow" clientidmode="Static" runat="server" visible="false">
                    <div width="25%">
                        <%= HtmlRequiredLabel("g-recaptcha-response", "Recaptcha.Label", "Authenticate user")%>
                    </div>
                    <div colspan="2">
                        <div class="g-recaptcha" data-sitekey='<%= Publickey %>'>
                        </div>
                        <%--<recaptcha:RecaptchaControl ID="Recaptcha" runat="server" PrivateKey="setincodebehind"
                            PublicKey="setincodebehind" Theme="clean" />--%>
                    </div>
                </div>
                <div>
                    <div>
                        <asp:Label runat="server" ID="RecaptchaErrorMessage" CssClass="error" Visible="false"
                            ClientIDMode="Static" />
                        <br />
                        <%--<asp:CustomValidator ID="RecaptchaValidator" runat="server" SetFocusOnError="true"
                            Display="Dynamic" CssClass="error" ClientValidationFunction="Register_ValidateRecaptcha"
                            ValidateEmptyText="true" ValidationGroup="RegisterStep1" />--%>
                    </div>
                </div>
            </div>
            <div>
                <div id="AdditionalInformationRow" style="display: none;" runat="server">
                    <div class="label" style="vertical-align: top;" colspan="6">
                        <b>
                            <%= HtmlRequiredLabel("AdditionalInformationQuestion", "Additional information")%>
                        </b>
                    </div>
                </div>
                <div id="AdditionalInformationNameRow" runat="server" style="vertical-align: top;
                    clear: left">
                    <div class="col-sm-12 col-md-5">
                        <div class="col-sm-4 col-md-5 clearfix">
                            <%= HtmlRequiredLabel(AdditionalInformationFirstNameTextBox, "Firstname.Label", "First name")%>
                        </div>
                        <div class="col-sm-8 col-md-7 clearfix">
                            <asp:TextBox ID="AdditionalInformationFirstNameTextBox" runat="server" ClientIDMode="Static"
                                Width="180px" MaxLength="20" /><br />
                            <asp:CustomValidator ID="AdditionalInformationFirstNameValidator" runat="server"
                                Display="Dynamic" ControlToValidate="AdditionalInformationFirstNameTextBox" CssClass="error"
                                ValidationGroup="RegisterStep1" ClientValidationFunction="LogInOrRegister_ValidateAdditionalField"
                                ValidateEmptyText="true" SetFocusOnError="true" />
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="col-sm-12 col-md-4">
                            <div class="col-sm-4 col-md-8 clearfix">
                                <%= HtmlLocalise("Middleinitial.Label", "Middle initial")%>
                            </div>
                            <div class="col-sm-8 col-md-4 clearfix">
                                <asp:TextBox ID="AdditionalInformationMiddleInitialTextBox" MaxLength="1" runat="server"
                                    ClientIDMode="Static" Width="30px" Title="Middle initial" /><br />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <div class="col-sm-4 col-md-3 clearfix">
                                <%= HtmlRequiredLabel(AdditionalInformationLastNameTextBox, "Lastname.Label", "Last name")%>
                            </div>
                            <div class="col-sm-8 col-md-9 clearfix">
                                <asp:TextBox ID="AdditionalInformationLastNameTextBox" runat="server" ClientIDMode="Static"
                                    Width="180px" MaxLength="20" /><br />
                                <asp:CustomValidator ID="AdditionalInformationLastNameValidator" runat="server" Display="Dynamic"
                                    ControlToValidate="AdditionalInformationLastNameTextBox" CssClass="error" ValidationGroup="RegisterStep1"
                                    ClientValidationFunction="LogInOrRegister_ValidateAdditionalField" ValidateEmptyText="true"
                                    SetFocusOnError="true" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-offset-4 col-md-8">
                            <div class="col-sm-4 col-md-3 clearfix">
                                <%= HtmlLabel(AdditionalInformationSuffixDropDown, "Suffix.Label", "Suffix")%>
                            </div>
                            <div class="col-sm-8 col-md-9 clearfix">
                                <asp:DropDownList runat="server" ID="AdditionalInformationSuffixDropDown" ClientIDMode="Static"
                                    Width="150px" autocomplete="off" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="AdditionalInformationDateOfBirthRow" runat="server" class="col-md-5">
                    <div class="col-sm-4 col-md-5 clearfix">
                        <%= HtmlRequiredLabel(AdditionalInformationDateOfBirthTextBox, "AdditionalInformationDateOfBirth", "Date of birth")%>
                    </div>
                    <div class="col-sm-3 col-md-6 clearfix">
                        <table role="presentation">
                            <tr>
                                <td style="padding-left: 0px">
                                    <asp:TextBox ID="AdditionalInformationDateOfBirthTextBox" runat="server" ClientIDMode="Static"
                                        Width="180px" />
                                    <%--<ajaxtoolkit:MaskedEditExtender ID="meDOBDate" runat="server" Mask="99/99/9999" MaskType="None"
                                        TargetControlID="AdditionalInformationDateOfBirthTextBox" ClearMaskOnLostFocus="false"
                                        PromptCharacter="_">
                                    </ajaxtoolkit:MaskedEditExtender>--%>
                                </td>
                                <td>
                                    <span class="instructionalText">mm/dd/yyyy</span>
                                </td>
                            </tr>
                        </table>
                        <asp:CustomValidator ID="LoginDOBValidator" runat="server" ControlToValidate="AdditionalInformationDateOfBirthTextBox"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1"
                            ClientValidationFunction="LogInOrRegister_ValidateDateOfBirth" ValidateEmptyText="true" />
                    </div>
                </div>
                <div id="OptionalAdditionalInformationRow" runat="server" style="clear: left" class="col-md-5">
                    <div class="col-sm-4 col-md-5 clearfix">
                        <%= HtmlRequiredLabel(AdditionalInformationPhoneNumberTextBox, "AdditionalInformationPhoneNumber", "Phone number")%>
                    </div>
                    <div class="col-sm-4 col-md-5 clearfix">
                        <asp:TextBox ID="AdditionalInformationPhoneNumberTextBox" runat="server" ClientIDMode="Static"
                            Width="180px" />
                        <%--<ajaxtoolkit:MaskedEditExtender ID="meAdditionalInformationPhoneNumber" runat="server"
                            MaskType="Number" TargetControlID="AdditionalInformationPhoneNumberTextBox" PromptCharacter="_"
                            ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false" /><br/>--%>
                        <asp:CustomValidator ID="PhoneNumberRequiredValidator" runat="server" ControlToValidate="AdditionalInformationPhoneNumberTextBox"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1"
                            ClientValidationFunction="LogInOrRegister_ValidatePhoneNumberRequired" ValidateEmptyText="true" />
                        <asp:CustomValidator ID="PhoneNumberValidator" runat="server" ControlToValidate="AdditionalInformationPhoneNumberTextBox"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="RegisterStep1"
                            ClientValidationFunction="LogInOrRegister_ValidatePhoneNumber" ValidateEmptyText="true" />
                    </div>
                    <div class="col-sm-4 col-md-2 clearfix">
                        <div id="PhoneNoTypeCell" runat="server" visible="False">
                            <asp:DropDownList ID="PhoneNoDropDownList" runat="server" Width="95px" ClientIDMode="Static"
                                OnChange="PhoneNoTypeChanged();">
                                <asp:ListItem Value="" Text="- select -" />
                                <asp:ListItem Value="Home" Text="Home" />
                                <asp:ListItem Value="Work" Text="Work" />
                                <asp:ListItem Value="Cell" Text="Cell" />
                                <asp:ListItem Value="Fax" Text="Fax" />
                                <asp:ListItem Value="NonUS" Text="Non US" />
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div id="CellphoneProviderRow" style="display: none; vertical-align: top; clear: left"
                    runat="server" class="col-md-5">
                    <div class="col-sm-4 col-md-5 clearfix">
                        <%= HtmlLocalise("CellphoneProvider.Label", "Cellphone provider")%>
                    </div>
                    <div class="col-sm-8 col-md-6 clearfix">
                        <asp:DropDownList runat="server" ID="CellphoneProviderDropDown" ClientIDMode="Static"
                            Width="300px" Title="Cellphone provider" />
                    </div>
                </div>
                <div style="vertical-align: top; clear: left" class="col-md-5">
                    <div class="col-sm-4 col-md-5 clearfix">
                        <%= HtmlRequiredLabel(AdditionalInformationZipTextBox, "AdditionalInformationZIPcode.Label", "ZIP code")%>
                    </div>
                    <div class="col-sm-8 col-md-7 clearfix">
                        <asp:TextBox ID="AdditionalInformationZipTextBox" runat="server" ClientIDMode="Static"
                            Width="180px" MaxLength="10" />
                        <br />
                        <asp:RegularExpressionValidator ID="AdditionalInformationZipRegexValidator" runat="server"
                            CssClass="error" ControlToValidate="AdditionalInformationZipTextBox" SetFocusOnError="true"
                            Display="Dynamic" ValidationGroup="RegisterStep1" />
                        <asp:CustomValidator ID="AdditionalInformationZipValidator" runat="server" Display="Dynamic"
                            ControlToValidate="AdditionalInformationZipTextBox" CssClass="error" ValidationGroup="RegisterStep1"
                            ClientValidationFunction="LogInOrRegister_ValidateAdditionalField" ValidateEmptyText="true"
                            SetFocusOnError="true" />
                    </div>
                </div>
                <div style="vertical-align: top; clear: left" class="col-md-5">
                    <div class="col-sm-4 col-md-5 clearfix">
                        <%= HtmlRequiredLabel(AdditionalInformationAddressTextBox, "AdditionalInformationAddress", "Address")%>
                    </div>
                    <div class="col-sm-8 col-md-7 clearfix">
                        <asp:TextBox ID="AdditionalInformationAddressTextBox" runat="server" ClientIDMode="Static"
                            Width="180px" MaxLength="200" />
                        <br />
                        <asp:CustomValidator ID="AdditionalInformationAddressValidator" runat="server" Display="Dynamic"
                            ControlToValidate="AdditionalInformationAddressTextBox" CssClass="error" ValidationGroup="RegisterStep1"
                            ClientValidationFunction="LogInOrRegister_ValidateAdditionalField" ValidateEmptyText="true"
                            SetFocusOnError="true" />
                    </div>
                </div>
                <div style="vertical-align: top; clear: left">
                    <div class="col-md-5">
                        <div class="col-sm-4 col-md-5 clearfix">
                            <%= HtmlRequiredLabel(AdditionalInformationCityTextBox, "AdditionalInformationCity", "City")%>
                        </div>
                        <div class="col-sm-8 col-md-7 clearfix">
                            <asp:TextBox ID="AdditionalInformationCityTextBox" runat="server" ClientIDMode="Static"
                                Width="180px" MaxLength="100" />
                            <br />
                            <asp:CustomValidator ID="AdditionalInformationCityValidator" runat="server" Display="Dynamic"
                                ControlToValidate="AdditionalInformationCityTextBox" CssClass="error" ValidationGroup="RegisterStep1"
                                ClientValidationFunction="LogInOrRegister_ValidateAdditionalField" ValidateEmptyText="true"
                                SetFocusOnError="true" />
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="col-sm-4 col-md-4 clearfix" style="clear: left">
                            <%= HtmlRequiredLabel(AdditionalInformationStateDropdown, "AdditionalInformationStateDropdown", "State")%>
                        </div>
                        <div class="col-sm-8 col-md-8 clearfix">
                            <div style="position: relative;">
                                <asp:DropDownList ID="AdditionalInformationStateDropdown" runat="server" CssClass="StateClass"
                                    Width="285px" ClientIDMode="Static">
                                    <asp:ListItem Text="- select a state -" />
                                </asp:DropDownList>
                            </div>
                            <asp:CustomValidator ID="AdditionalInformationStateValidator" runat="server" Display="Dynamic"
                                ControlToValidate="AdditionalInformationStateDropdown" CssClass="error" ValidationGroup="RegisterStep1"
                                ClientValidationFunction="LogInOrRegister_ValidateState" ValidateEmptyText="true"
                                SetFocusOnError="true" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="panRegisterStep1Button" runat="server">
            <table style="width: 100%;" role="presentation">
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="UsernameInUseErrorMessage" CssClass="error" ClientIDMode="Static" />
                    </td>
                    <td style="text-align: right;">
                        <asp:Button ID="RegisterButton" Text="Register &#187;" runat="server" ClientIDMode="Static"
                            CssClass="buttonLevel2" OnClick="RegisterButton_Click" ValidationGroup="RegisterStep1" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="RegisterStep2Panel" runat="server" Visible="false">
            <table style="width: 100%;" role="presentation">
                <asp:PlaceHolder runat="server" ID="TermsConditionsWorkforce" Visible="False">
                    <tr>
                        <td>
                            <strong>
                                <%= HtmlLocalise("ConfidentialityConsent.Label", "Confidentiality and Consent")%></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="registrationTerms">
                                <%= HtmlLocalise("Consent.Label", "#FOCUSCAREER# is a secure, web-based system for resume building and job search. State and federal law requires the State of state to protect the confidentiality of your personal, identifying information. When you use this system, you become an active job seeker. Appropriate staff may contact you about jobs or other services by telephone, email, or other electronic means. Please read the information below to understand our services and how they affect you. <br /><br /><i><b>Information Sharing</b></i><br />To use #FOCUSCAREER#, you voluntarily agree to be entered into our computerized system. You authorize appropriate personnel in state's Workforce System to release your information to its agents, contractors, or partner agencies should you seek educational, training, and/or employment services from them. Only authorized personnel may view your information or provide these services to you.<br /><br /><i><b>Social Security Number Collection</b></i><br />It is unlawful for any federal, state or local government to deny you any right, benefit, or privilege, provided by law because you refuse to disclose your Social Security Number (SSN). To comply with the Employment and Training Administration’s training programs, staff may use your SSN to calculate program performance measures outcomes. To be eligible for Unemployment Insurance (UI) benefits, you must provide your SSN because staff must verify your wage records. The UI program is exempt from The Privacy Act. As a job seeker, who also may be collecting UI benefits, your SSN has been provided legally and confidentially to the #FOCUSCAREER# system under the Information Sharing arrangement described above. If you are not claiming UI, you are not required to provide your SSN to #FOCUSCAREER#. If you provide this information voluntarily for account validation and UI coordination purposes, staff must ensure that your SSN is maintained in a secure and confidential manner, as prescribed by law. Under no circumstances will your SSN information be disclosed, displayed, or disseminated to a prospective employer; another job seeker; the public; or any agent, contractor, or partner agency beyond the Information Sharing arrangement described above. If you have questions about your privacy rights, refer to the Privacy Act of 1974, as amended, Section 7 [5 U.S.C. Section 552a Note (Disclosure of Social Security Number).] <br /><br /><i><b>Veteran Priority of Service</b></i><br />If you served in the military and were discharged under honorable conditions, you are eligible for Priority of Service. The level of services you receive depends on multiple factors, such as the length of military service, disability-connected injuries, and services available to spouses of a deceased service member. For more information, you may read the U.S. Department of Labor’s regulations at 20 CFR 1010.200(b).")%>
                            </div>
                        </td>
                    </tr>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="TermsConditionsEducation" Visible="False">
                    <tr>
                        <td>
                            <div class="registrationTerms">
                                <%= HtmlLocalise("Global.TermsOfUseEducation.Text", DefaultText.EducationTermsAndConditions)%>
                            </div>
                        </td>
                    </tr>
                </asp:PlaceHolder>
                <tr>
                    <td style="text-align: right;">
                        <table role="presentation">
                            <tr>
                                <td>
                                    <asp:LinkButton ID="LaborRegulationsLink" Visible="false" OnClientClick="javascript:OpenConsent(); return false;"
                                        runat="server"></asp:LinkButton>
                                </td>
                                <td>
                                    <asp:Panel ID="panNormalUserSection" runat="server">
                                        <asp:Button ID="CancelButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel3"
                                            CausesValidation="false" OnClick="CancelRegistration_Click" />
                                        <asp:Button ID="AgreeToTermsButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel2"
                                            CausesValidation="false" OnClick="AgreeToTermsButton_Click" />
                                    </asp:Panel>
                                    <asp:Panel ID="panMigratedUserSection" runat="server" Visible="false">
                                        <asp:Button ID="btnMigratedCancel" runat="server" ClientIDMode="Static" CssClass="buttonLevel3"
                                            CausesValidation="false" OnClick="btnMigratedCancel_Click" />
                                        <asp:Button ID="panMigratedAgreeToTerms" runat="server" ClientIDMode="Static" CssClass="buttonLevel2"
                                            CausesValidation="false" OnClick="panMigratedAgreeToTerms_Click" />
                                    </asp:Panel>
                                    <asp:Panel ID="panConsentUserSection" runat="server" Visible="false">
                                        <asp:Button ID="CancelAndLogoutButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel3"
                                            CausesValidation="false" OnClick="CancelAndLogoutButton_Click" />
                                        <asp:Button ID="ConsentAgreeToTermsButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel2"
                                            CausesValidation="false" OnClick="ConsentAgreeToTermsButton_Click" />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div>
<asp:HiddenField ID="SearchWithoutLoginDummyTarget" runat="server" />
<act:ModalPopupExtender ID="SearchWithoutLogin" runat="server" BehaviorID="SearchWithoutLogin"
    TargetControlID="SearchWithoutLoginDummyTarget" PopupControlID="SearchWithoutLoginDiv"
    BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<div id="SearchWithoutLoginDiv" class="modal" style="z-index: 100001; display: none;">
    <div class="lightboxmodal lightbox FocusCareer loginAndRegister" id="SearchWithoutLoginLightbox">
        <asp:Panel ID="SearchWithoutLoginPanel" runat="server">
            <table role="presentation">
                <tr>
                    <td>
                        <h1>
                            <%= HtmlLocalise("TermsAndConditions.Label", "Terms and Conditions")%></h1>
                    </td>
                </tr>
            </table>
            <table style="width: 100%;" role="presentation">
                <tr>
                    <td>
                        <asp:Panel ID="panTermsAndConditionsSWS" runat="server" BorderWidth="0px" Height="320px"
                            ScrollBars="Auto" Style="text-align: left;">
                            <%= HtmlLocalise("TermsAndConditions.SearchWithoutSignIn.Label", "While this website administrator has taken measures to enhance information quality and precautions to offer users protection from potential misinformation and/or internet fraud, all users are reminded that there are fraudulent schemes on the internet that appear legitimate enterprises.  Be wary of online business opportunities, work-at-home schemes, and sites that ask for personal information online.  Identity theft is a significant and potentially costly problem and internet users should never provide their social security number, birth date, credit card numbers or other private information when responding to a job opportunity online.<br /><br /><b>Terms and Conditions of Use</b><br /><br />This site and the information contained herein may contain inaccuracies or typographical or other errors. The state Department of Commerce and partners in state's Workforce System, this site and the hosting company make no representations regarding the accuracy, reliability, completeness, or timeliness of the site or any of the information contained therein. The use of the site is at your own, sole, risk. <br /><br />This site is not and shall not be considered an employer with respect to your use of the site and this site shall not be responsible for any employment decisions, for whatever reason made by any user, company or firm whose job postings on this site. You shall have the sole responsibility for evaluating and determining with which prospective employers you communicate. You further agree that the specific terms and conditions of your transactions with prospective employers initiated by or through the site's use, shall be as determined by you and such prospective employer or employee.<br /><br />The state Department of Commerce and partners in state's Workforce System, this site and its hosting company are merely providing a venue and a conduit for your convenience to facilitate your employment search and interactions with prospective employers and do not represent themselves as employers, agents or otherwise involved in the interaction between prospective employees and employers or the employment process between parties using this site.<br /><br />Nothing contained in or available via the site shall constitute an affiliation, sponsorship, or endorsement by state Department of Commerce and partners in state's Workforce System or the hosting site of any of the employers using the site, or of the respective products and services they may buy, sell or otherwise provide.<br /><br />This site, including all associated services and material, are provided on as 'as is' and 'as available' basis without any warranties of any kind.  This site, to the full extent permitted by law, disclaims all warranties, express or implied, including any warranties of merchantability, fitness for a particular purpose or use, or title or non-infringement.   The state Department of Commerce and partners in state's Workforce System, this site, and its hosting company, make no warranties about the accuracy, reliability, completeness, or timeliness of the site, including any materials, services, software, text, graphics, and links associated with or included in the site, or that the site (or any part thereof) will be error-free or that defects will be corrected.  In addition, this site or its hosting company do not warrant that the site (or any part thereof) is appropriate or available for use in any particular jurisdiction, and accessing or using the site (or any part thereof) from jurisdictions where access or use is illegal is expressly prohibited.")%>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        <asp:Button ID="SWSCancelButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel3"
                            CausesValidation="false" OnClientClick="javascript:$find('SearchWithoutLogin').hide(); $find('LogInModal').show(); return false;" />
                        <asp:Button ID="SWSAgreeToTermsButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel2"
                            CausesValidation="false" OnClick="SWSAgreeToTermsButton_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div>
<asp:HiddenField ID="UserConversionDummyTarget" runat="server" />
<act:ModalPopupExtender ID="UserConversion" runat="server" BehaviorID="UserConversion"
    TargetControlID="UserConversionDummyTarget" PopupControlID="UserConversionDiv"
    BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<div id="UserConversionDiv" class="modal" style="z-index: 100001; display: none;">
    <div class="lightboxmodal lightbox FocusCareer loginAndRegister" id="UserConversionLightbox">
        <asp:Panel ID="UserConversionPanel" runat="server">
            <table role="presentation">
                <tr>
                    <td>
                        <h1>
                            <asp:Label runat="server" ID="ConvertAccountHeaderLabel"></asp:Label>
                        </h1>
                    </td>
                    <td class="steppedProcessSubhead">
                        <asp:Label runat="server" ID="ConvertAccountPageLabel"></asp:Label>
                    </td>
                    <%--<td style="text-align: right">
						<a href="#" onclick="javascript:$find('LogInModal').show();$find('UserConversion').hide();">
							<%= HtmlLocalise("Global.LogIn.Text", "Sign in")%></a> |
						<asp:LinkButton runat="server" ID="UCSearchJobWithoutReg" OnClick="SearchJobWithoutReg_Click"
							CausesValidation="false" />
					</td>--%>
                </tr>
            </table>
            <table style="width: 100%;" role="presentation">
                <asp:PlaceHolder runat="server" ID="ConversionPlaceHolder">
                    <tr>
                        <td>
                            <%= HtmlRequiredLabel(UCOldUsernameTextbox, "OldEmailAddress", "Old email address")%>
                        </td>
                        <td>
                            <asp:TextBox ID="UCOldUsernameTextbox" runat="server" ClientIDMode="Static" Width="300px"
                                Enabled="false" AutoCompleteType="Disabled" />
                        </td>
                        <td class="instructionalText">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= HtmlRequiredLabel(UCOldPassword, "OldPassword", "Old password")%>
                        </td>
                        <td>
                            <asp:TextBox ID="UCOldPassword" runat="server" ClientIDMode="Static" Width="300px"
                                Enabled="false" AutoCompleteType="Disabled" TextMode="Password" />
                        </td>
                        <td class="instructionalText">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= HtmlRequiredLabel(UCEmailAddressTextbox, "EmailAddress", "Email address")%>
                        </td>
                        <td>
                            <asp:TextBox ID="UCEmailAddressTextbox" runat="server" ClientIDMode="Static" Width="300px"
                                MaxLength="40" AutoCompleteType="Disabled" />
                            <asp:RequiredFieldValidator ID="UCEmailAddressRequired" runat="server" ControlToValidate="UCEmailAddressTextbox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UserConversion" />
                            <asp:RegularExpressionValidator ID="UCEmailAddressRegEx" runat="server" ControlToValidate="UCEmailAddressTextbox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UserConversion" />
                        </td>
                        <td class="instructionalText">
                            <%= HtmlLocalise("Email.HelpText1", "The email address you provide will be used as your account username.") %><br />
                            <%= HtmlLocalise("Email.HelpText2", "No email account? Use a free service:")%><br />
                            <a href="http://webmail.aol.com" target="_blank">
                                <%= HtmlLocalise("AOLMail", "AOLMail")%></a>, <a href="http://mail.google.com" target="_blank">
                                    <%= HtmlLocalise("Gmail", "Gmail")%></a>, <a href="https://login.live.com/" target="_blank">
                                        <%= HtmlLocalise("Windowslive", "Windows Live")%></a>, <a href="http://mail.yahoo.com"
                                            target="_blank">
                                            <%= HtmlLocalise("Yahoo", "Yahoo!")%></a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= HtmlRequiredLabel(UCConfirmEmailAddressTextbox, "EmailAddressConfirmation", "Re-enter email address")%>
                        </td>
                        <td>
                            <asp:TextBox ID="UCConfirmEmailAddressTextbox" runat="server" ClientIDMode="Static"
                                Width="300px" MaxLength="40" AutoCompleteType="Disabled" />
                            <asp:RequiredFieldValidator ID="UCConfirmEmailAddressRequired" runat="server" ControlToValidate="UCConfirmEmailAddressTextbox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UserConversion" />
                            <asp:CompareValidator ID="UCConfirmEmailAddressCompare" runat="server" ControlToValidate="UCConfirmEmailAddressTextbox"
                                ControlToCompare="UCEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic"
                                CssClass="error" ValidationGroup="UserConversion" />
                        </td>
                        <td class="instructionalText">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= HtmlRequiredLabel(UCNewPasswordTextBox, "NewPassword", "Password")%>
                        </td>
                        <td>
                            <asp:TextBox ID="UCNewPasswordTextBox" runat="server" ClientIDMode="Static" Width="300px"
                                TextMode="Password" MaxLength="20" /><br />
                            <asp:RequiredFieldValidator ID="UCNewPasswordRequired" runat="server" ControlToValidate="UCNewPasswordTextBox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UserConversion" />
                            <asp:RegularExpressionValidator ID="UCNewPasswordRegEx" runat="server" ControlToValidate="UCNewPasswordTextBox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UserConversion" />
                            <asp:CompareValidator ID="UCUserNameAndPasswordCompare" runat="server" ControlToValidate="UCNewPasswordTextBox"
                                ControlToCompare="UCEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic"
                                Operator="NotEqual" CssClass="error" ValidationGroup="UserConversion" />
                        </td>
                        <td class="instructionalText">
                            <%= HtmlLocalise("PasswordInstructions", "6-20 characters; must include at least one number.")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= HtmlRequiredLabel(UCConfirmNewPasswordTextBox, "ConfirmNewPassword", "Re-enter password")%>
                        </td>
                        <td>
                            <asp:TextBox ID="UCConfirmNewPasswordTextBox" runat="server" ClientIDMode="Static"
                                Width="300px" TextMode="Password" MaxLength="20" />
                            <asp:RequiredFieldValidator ID="UCConfirmNewPasswordRequired" runat="server" ControlToValidate="UCConfirmNewPasswordTextBox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UserConversion" />
                            <asp:CompareValidator ID="UCConfirmNewPasswordCompare" runat="server" ControlToValidate="UCConfirmNewPasswordTextBox"
                                ControlToCompare="UCNewPasswordTextBox" SetFocusOnError="true" Display="Dynamic"
                                CssClass="error" ValidationGroup="UserConversion" />
                        </td>
                        <td class="instructionalText">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= HtmlRequiredLabel(UCSecurityQuestionDropDown, "SecurityQuestion", "Security question")%>
                        </td>
                        <td>
                            <div style="position: relative;">
                                <asp:DropDownList ID="UCSecurityQuestionDropDown" runat="server" ClientIDMode="Static"
                                    Width="290px" />
                            </div>
                            <div id="UCSecurityQuestionDiv" style="display: none; padding-top: 8px;">
                                <asp:TextBox ID="UCSecurityQuestionTextBox" runat="server" ClientIDMode="Static"
                                    Width="300px" MaxLength="100" Title="Security Question Text" />
                                <asp:CustomValidator ID="UCSecurityQuestionCustomValidator" runat="server" ControlToValidate="UCSecurityQuestionTextBox"
                                    ClientValidationFunction="validateUCOwnSecurityQuestion" ValidationGroup="UserConversion"
                                    ValidateEmptyText="True" CssClass="error" SetFocusOnError="true" Display="Dynamic" />
                            </div>
                            <asp:RequiredFieldValidator ID="UCSecurityQuestionRequired" runat="server" ControlToValidate="UCSecurityQuestionDropDown"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UserConversion" />
                        </td>
                        <td class="instructionalText">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%= HtmlRequiredLabel(UCSecurityAnswerTextBox, "SecurityAnswer", "Security answer")%>
                        </td>
                        <td>
                            <asp:TextBox ID="UCSecurityAnswerTextBox" runat="server" ClientIDMode="Static" Width="300px"
                                MaxLength="30" />
                            <asp:RequiredFieldValidator ID="UCSecurityAnswerRequired" runat="server" ControlToValidate="UCSecurityAnswerTextBox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="UserConversion" />
                        </td>
                        <td class="instructionalText">
                            <%= HtmlLocalise("SecurityAnswerInstructions", "Answer is case-sensitive.")%>
                        </td>
                    </tr>
                </asp:PlaceHolder>
                <tr id="OAuthSsnRow" runat="server">
                    <td style="vertical-align: top; width: 25%;">
                        <%= HtmlLocalise("SocialSecurityNumber", "Social Security Number (SSN)")%>
                        <asp:Label ID="OAuthSSNRedAsterik" runat="server" Visible="false" ClientIDMode="Static"><%= HtmlRequiredLabel("RedAsterik.Label", "")%></asp:Label>
                    </td>
                    <td style="vertical-align: top; width: 20%;">
                        <asp:TextBox ID="OAuthSocialSecurityNumberPart1TextBox" runat="server" ClientIDMode="Static"
                            title="OAuthSsnTextBox1" MaxLength="3" size="3" Width="25" onKeyup="autotab(this, 'OAuthSocialSecurityNumberPart2TextBox')"
                            TextMode="Password" />
                        -
                        <asp:TextBox ID="OAuthSocialSecurityNumberPart2TextBox" runat="server" ClientIDMode="Static"
                            title="OAuthSsnTextBox2" MaxLength="2" size="2" Width="15" onKeyup="autotab(this, 'OAuthSocialSecurityNumberPart3TextBox')"
                            TextMode="Password" />
                        -
                        <asp:TextBox ID="OAuthSocialSecurityNumberPart3TextBox" runat="server" ClientIDMode="Static"
                            title="OAuthSsnTextBox3" MaxLength="4" size="4" Width="30" TextMode="Password" />
                        <asp:CustomValidator ID="OAuthSocialSecurityNumberValidator" runat="server" ClientValidationFunction="validateSsn"
                            ValidationGroup="OAuthRegister" ValidateEmptyText="True" CssClass="error" SetFocusOnError="true"
                            Display="Dynamic" />
                    </td>
                    <td class="instructionalText" rowspan="2">
                        <%= HtmlLocalise("SocialSecurityNumber1.HelpText", "Your SSN will be stored securely, and is used only to link your account to any existing accounts for unemployment insurance or One-Stop Career Centers.")%>
                        <asp:Panel ID="panOAuthSSNInfo" runat="server">
                            <br />
                            <%= HtmlLocalise("SocialSecurityNumber2.HelpText", "If you do not provide an SSN, you will be asked for additional information.")%>
                        </asp:Panel>
                    </td>
                </tr>
                <tr id="OAuthSsnRow2" runat="server">
                    <td>
                        <%= HtmlLocalise("ConfirmSocialSecurityNumber", "Re-enter SSN")%>
                        <asp:Label ID="OAuthConfirmSSNRedAsterik" runat="server" Visible="false" ClientIDMode="Static"><%= HtmlRequiredLabel("RedAsterik.Label", "")%></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="OAuthConfirmSocialSecurityNumberPart1TextBox" runat="server" ClientIDMode="Static"
                            title="OAuthConfirmSsnTextBox1" MaxLength="3" size="3" Width="25" onKeyup="autotab(this, 'OAuthConfirmSocialSecurityNumberPart2TextBox')"
                            TextMode="Password" />
                        -
                        <asp:TextBox ID="OAuthConfirmSocialSecurityNumberPart2TextBox" runat="server" ClientIDMode="Static"
                            title="OAuthConfirmSsnTextBox2" MaxLength="2" size="2" Width="15" onKeyup="autotab(this, 'OAuthConfirmSocialSecurityNumberPart3TextBox')"
                            TextMode="Password" />
                        -
                        <asp:TextBox ID="OAuthConfirmSocialSecurityNumberPart3TextBox" runat="server" ClientIDMode="Static"
                            title="OAuthConfirmSsnTextBox3" MaxLength="4" size="4" Width="30" TextMode="Password" />
                        <asp:CustomValidator ID="OAuthConfirmSocialSecurityNumberValidator" runat="server"
                            ClientValidationFunction="validateConfirmSsn" ValidationGroup="OAuthRegister"
                            ValidateEmptyText="True" CssClass="error" SetFocusOnError="true" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table runat="server" id="OAuthAdditionalInfoTable" style="display: none" role="presentation">
                            <tr>
                                <td>
                                    <%= HtmlRequiredLabel(OAuthDateOfBirthTextBox, "OAuthDateOfBirth", "Date of birth")%>
                                </td>
                                <td>
                                    <asp:TextBox ID="OAuthDateOfBirthTextBox" runat="server" ClientIDMode="Static" />
                                    <%--<ajaxtoolkit:MaskedEditExtender ID="MeOAuthDateOfBirth" runat="server" Mask="99/99/9999"
                                        MaskType="Date" TargetControlID="OAuthDateOfBirthTextBox" ClearMaskOnLostFocus="false"
                                        PromptCharacter="_">
                                    </ajaxtoolkit:MaskedEditExtender>--%>
                                </td>
                                <td colspan="2">
                                    <span class="instructionalText">mm/dd/yyyy</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:CustomValidator ID="OAuthDateOfBirthValidator" runat="server" ControlToValidate="OAuthDateOfBirthTextBox"
                                        SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OAuthRegister"
                                        ClientValidationFunction="LogInOrRegister_ValidateDateOfBirth" ValidateEmptyText="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= HtmlLocalise("OAuthPhoneNumber", "Phone number")%>
                                </td>
                                <td>
                                    <asp:TextBox ID="OAuthPhoneNumberTextBox" runat="server" ClientIDMode="Static" Title="OAuth Phone Number Text" />
                                    <%--<ajaxtoolkit:MaskedEditExtender ID="MeOAuthPhoneNumber" runat="server" Mask="(999) 999-9999"
                                        MaskType="Number" TargetControlID="OAuthPhoneNumberTextBox" PromptCharacter="_"
                                        ClearMaskOnLostFocus="false" AutoComplete="false" EnableViewState="true" ClearTextOnInvalid="false">
                                    </ajaxtoolkit:MaskedEditExtender>--%>
                                </td>
                                <td colspan="2">
                                    <div id="OAuthPhoneTypeCell" runat="server" visible="False">
                                        <asp:DropDownList ID="OAuthPhoneTypeDropDown" runat="server" ClientIDMode="Static"
                                            OnChange="PhoneNoTypeChanged();">
                                            <asp:ListItem Value="" Text="- select -" />
                                            <asp:ListItem Value="Home" Text="Home" />
                                            <asp:ListItem Value="Work" Text="Work" />
                                            <asp:ListItem Value="Cell" Text="Cell" />
                                            <asp:ListItem Value="Fax" Text="Fax" />
                                            <asp:ListItem Value="NonUS" Text="Non US" />
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:CustomValidator ID="OAuthPhoneNumberValidator" runat="server" ControlToValidate="OAuthPhoneNumberTextBox"
                                        SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="OAuthRegister"
                                        ClientValidationFunction="LogInOrRegister_ValidatePhoneNumber" ValidateEmptyText="true" />
                                </td>
                            </tr>
                            <tr id="OAuthCellphoneProviderRow" style="display: none;" runat="server">
                                <td>
                                    <%= HtmlLocalise("CellphoneProvider.Label", "Cellphone provider")%>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList runat="server" ID="OAuthCellphoneProviderDropDown" ClientIDMode="Static"
                                        Width="300px" Title="Cellphone provider" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= HtmlRequiredLabel(OAuthZIPcodeTextBox, "OAuthZIPcode.Label", "ZIP code")%>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="OAuthZIPcodeTextBox" runat="server" ClientIDMode="Static" Width="180px"
                                        MaxLength="10" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:RequiredFieldValidator runat="server" ID="OAuthZIPcodeRequired" ControlToValidate="OAuthZIPcodeTextBox"
                                        ValidationGroup="OAuthRegister" SetFocusOnError="true" Display="Dynamic" CssClass="error"
                                        ValidateEmptyText="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= HtmlRequiredLabel(OAuthAddressTextBox, "OAuthAddress", "Address")%>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="OAuthAddressTextBox" runat="server" ClientIDMode="Static" Width="180px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:RequiredFieldValidator runat="server" ID="OAuthAddressRequired" ControlToValidate="OAuthAddressTextBox"
                                        ValidationGroup="OAuthRegister" SetFocusOnError="true" Display="Dynamic" CssClass="error"
                                        ValidateEmptyText="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <%= HtmlRequiredLabel(OAuthCityTextBox, "OAuthCity", "City")%>
                                </td>
                                <td>
                                    <asp:TextBox ID="OAuthCityTextBox" runat="server" ClientIDMode="Static" Width="180px" />
                                </td>
                                <td>
                                    <%= HtmlRequiredLabel(OAuthStateDropDown, "OAuthStateDropdown", "State")%>
                                </td>
                                <td>
                                    <div style="position: relative;">
                                        <asp:DropDownList ID="OAuthStateDropDown" runat="server" CssClass="StateClass" Width="285px"
                                            ClientIDMode="Static">
                                            <asp:ListItem Text="- select a state -" />
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:RequiredFieldValidator runat="server" ID="OAuthCityRequired" ControlToValidate="OAuthCityTextBox"
                                        ValidationGroup="OAuthRegister" SetFocusOnError="true" Display="Dynamic" CssClass="error"
                                        ValidateEmptyText="true" />
                                </td>
                                <td colspan="2">
                                    <asp:RequiredFieldValidator runat="server" ID="OAuthStateRequired" ControlToValidate="OAuthStateDropDown"
                                        ValidationGroup="OAuthRegister" SetFocusOnError="true" Display="Dynamic" CssClass="error"
                                        ValidateEmptyText="true" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="text-align: right;">
                        <asp:Button ID="btnBackUserConversion" runat="server" ClientIDMode="Static" CssClass="buttonLevel3"
                            CausesValidation="false" OnClick="btnBackUserConversion_Click" />
                        <asp:Button ID="btnUpdateMyAccount" runat="server" ClientIDMode="Static" CssClass="buttonLevel2"
                            ValidationGroup="UserConversion" OnClick="btnUpdateMyAccount_Click" OnClientClick="return isOAuthConvertAdditionalInfoRequired();" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div>
<uc:RegistrationConfirmation ID="RegistrationConfirmation" runat="server" Visible="False" />
<uc:ConfirmationModal ID="ConfirmationModal" runat="server" OnCloseCommand="ConfirmationModal_OnCloseCommand" />
<script type="text/javascript">
	$(document).ready(function () {    
    $("#OAuthPhoneNumberTextBox").mask('(999) 999-9999');
    $("#AdditionalInformationPhoneNumberTextBox").mask('(999) 999-9999');
    $("#OAuthDateOfBirthTextBox").mask('99/99/9999');
    $("#AdditionalInformationDateOfBirthTextBox").mask('99/99/9999', { placeholder: " " });
	  $("#AdditionalInformationZipTextBox").mask("99999-9999");
		$("#AdditionalInformationZipTextBox").change(function () { GetStateCountyCityAndCountiesForPostalCode($(this).val(), "AdditionalInformationStateDropdown", "AdditionalInformationCityTextBox", "RegisterStep1"); });
		//$("#AdditionalInformationZipTextBox").mask("<%= App.Settings.ExtendedPostalCodeMaskPattern %>", { placeholder: " " });
		$("#OAuthZIPcodeTextBox").mask("99999-9999", { placeholder: " " });
		$("#OAuthZIPcodeTextBox").change(function () { GetStateCountyCityAndCountiesForPostalCode($(this).val(), "OAuthStateDropDown", "OAuthCityTextBox", "OAuthRegister"); });
		//$("#OAuthZIPcodeTextBox").mask("99999-9999", { placeholder: " " });
		$("#SecurityQuestionDropDown").change(function () {
			if ($(this).val() == "<%= CreateOwnSecurityQuestionValue %>") {
				$("#SecurityQuestionDiv").show();
			} else {
				$("#SecurityQuestionDiv").hide();
			}
		});

		$("#EmailAddressTextbox").keyup(function () {
			$("#UsernameInUseErrorMessage").text('');
		});


		if ($("#SecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
			$("#SecurityQuestionDiv").show();
		}

		$("#UCSecurityQuestionDropDown").change(function () {
			if ($(this).val() == "<%= CreateOwnSecurityQuestionValue %>") {
				$("#UCSecurityQuestionDiv").show();
			} else {
				$("#UCSecurityQuestionDiv").hide();
			}
		});

		if ($("#UCSecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
			$("#UCSecurityQuestionDiv").show();
		}

		if ($('#UsernameInUseErrorMessage').text().length > 0) isAdditionalInformationRequired();
	});

	Sys.Application.add_load(LoginOrRegister_PageLoad);

	function LoginOrRegister_PageLoad(sender, args) {
	
		// Jaws Compatibility
		var modal = $find('LogInModal');

		if (modal != null) {
			modal.add_shown(function () {
				$('#LoginModalPanel').attr('aria-hidden', false);
				$('.page').attr('aria-hidden', true);
				$('#UserNameText').focus();
			});
			modal.add_hiding(function () {
				$('#LoginModalPanel').attr('aria-hidden', true);
				$('.page').attr('aria-hidden', false);
			});
		}

		//Recaptcha Accessibility
		if ($('#recaptcha_reload').length > 0) {
			//AccessibleRecaptcha('#recaptcha_reload');
			//AccessibleRecaptcha('#recaptcha_switch_audio');
			//AccessibleRecaptcha('#recaptcha_whatsthis');

			// Change Style to Match Talent
			$('#recaptcha_privacy > a').css({ color: '#BFBFBF' });
		}
	}

	function pageLoad(sender, args) {

	}

	function DisplayForgotPasswordModal() {
		$find('LogInModal').hide();
		$find('ForgotPasswordModal<%= App.Settings.EnableCareerPasswordSecurity ? "2" : "" %>').show();

		return false;
	}

	// Changes recaptcha buttons to be more accessible
	function AccessibleRecaptcha(id) {
		var attrs = {};

		$.each($(id)[0].attributes, function (idx, attr) {
			attrs[attr.nodeName] = attr.nodeValue;
		});

		$(id).replaceWith(function () {
			return $("<input />", attrs).append($(id).contents());
		});
		$(id).prop("type", "image");
		$(id).parent().attr("onclick", "return false;");
		$(id).parent().attr("onkeypress", "return false;");
	}

	function Register_ValidateRecaptcha(source, arguments) {
	  var recaptchaField = $("#g-recaptcha-response");
	  arguments.IsValid = (recaptchaField.length == 0 || recaptchaField.val().trim().length > 0);
	  $("#<%=RecaptchaErrorMessage.ClientID %>").hide();
	}

  function validateSsn(src, args) {
    var ssnPart1 = $("#SocialSecurityNumberPart1TextBox").val();
    var ssnPart2 = $("#SocialSecurityNumberPart2TextBox").val();
    var ssnPart3 = $("#SocialSecurityNumberPart3TextBox").val();
    if (ssnPart1 == "" && ssnPart2 == "" && ssnPart3 == "") {
      args.IsValid = true;
    } else {
      var ssn = ssnPart1 + "-" + ssnPart2 + "-" + ssnPart3;
      var ssnRegExp = /^[0-9]{3}[\- ]?[0-9]{2}[\- ]?[0-9]{4}$/;
      args.IsValid = ssnRegExp.test(ssn);
    }
    }
  
    function validateUIClaimantSsn(src, args) {
    var ssnPart1 = $("#SocialSecurityNumberPart1TextBox").val();
    var ssnPart2 = $("#SocialSecurityNumberPart2TextBox").val();
    var ssnPart3 = $("#SocialSecurityNumberPart3TextBox").val();
    if (ssnPart1 == "" && ssnPart2 == "" && ssnPart3 == "") {
      src.innerHTML = "<%= SSNRequiredMessage %>";
      args.IsValid = false;
    } else {
      var ssn = ssnPart1 + "-" + ssnPart2 + "-" + ssnPart3;
      var ssnRegExp = /^[0-9]{3}[\- ]?[0-9]{2}[\- ]?[0-9]{4}$/;
      if (ssnRegExp.test(ssn) == false) {
        src.innerHTML = "<%= SSNErrorMessage %>";
        args.IsValid = false;
      }
    }
  }

  function validateConfirmSsn(src, args) {
    var ssnPart1 = $("#SocialSecurityNumberPart1TextBox").val();
    var ssnPart2 = $("#SocialSecurityNumberPart2TextBox").val();
    var ssnPart3 = $("#SocialSecurityNumberPart3TextBox").val();
    var confirmSsnPart1 = $("#ConfirmSocialSecurityNumberPart1TextBox").val();
    var confirmSsnPart2 = $("#ConfirmSocialSecurityNumberPart2TextBox").val();
    var confirmSsnPart3 = $("#ConfirmSocialSecurityNumberPart3TextBox").val();

    args.IsValid = ((ssnPart1 == confirmSsnPart1) && (ssnPart2 == confirmSsnPart2) && (ssnPart3 == confirmSsnPart3));
  }

    //function isAdditionalInformationRequired(src, args) {
    function isAdditionalInformationRequired() {
      if ($('#AdditionalInformationInstructions').exists() && focusSuite.theme == "education")
        $("#AdditionalInformationInstructions").show();

      if (focusSuite.theme == "education")
        $("#AdditionalInformationRow").show();
      
      $("#AdditionalInformationNameRow").show();
      $("#AdditionalInformationDateOfBirthRow").show();
      $("#OptionalAdditionalInformationRow").show();
    }

    function isOAuthConvertAdditionalInfoRequired() {
    	if ("<%=EnableOAuthFeatures%>" == "True") {
    		if (checkNoOAuthConvertSsnInformationEntered()) {
    			if ($('#OAuthAdditionalInfoTable').exists()) {
    				if ($("#OAuthAdditionalInfoTable").css('display') == 'none') {
    					$("#OAuthAdditionalInfoTable").show();
    					return false;
    				}
    			}
    		}
    		else {
    			if ($('#OAuthAdditionalInfoTable').exists())
    				$("#OAuthAdditionalInfoTable").hide();
    		}
    	}
    	return true;
    }

  function checkNoSsnInformationEntered() {
  	if (!($('#SocialSecurityNumberPart1TextBox').exists()))
  		return true;
	  
    var ssnPart1 = $("#SocialSecurityNumberPart1TextBox").val();
    var ssnPart2 = $("#SocialSecurityNumberPart2TextBox").val();
    var ssnPart3 = $("#SocialSecurityNumberPart3TextBox").val();
    var confirmSsnPart1 = $("#ConfirmSocialSecurityNumberPart1TextBox").val();
    var confirmSsnPart2 = $("#ConfirmSocialSecurityNumberPart2TextBox").val();
    var confirmSsnPart3 = $("#ConfirmSocialSecurityNumberPart3TextBox").val();

    return (ssnPart1 == "" && ssnPart2 == "" && ssnPart3 == "" && confirmSsnPart1 == "" && confirmSsnPart2 == "" && confirmSsnPart3 == "");
   }

   function checkNoOAuthConvertSsnInformationEntered() {
   	if (!($('#SocialSecurityNumberPart1TextBox').exists()))
   		return true;

   	var ssnPart1 = $("#OAuthSocialSecurityNumberPart1TextBox").val();
   	var ssnPart2 = $("#OAuthSocialSecurityNumberPart2TextBox").val();
   	var ssnPart3 = $("#OAuthSocialSecurityNumberPart3TextBox").val();
   	var confirmSsnPart1 = $("#OAuthConfirmSocialSecurityNumberPart1TextBox").val();
   	var confirmSsnPart2 = $("#OAuthConfirmSocialSecurityNumberPart2TextBox").val();
   	var confirmSsnPart3 = $("#OAuthConfirmSocialSecurityNumberPart3TextBox").val();

   	return (ssnPart1 == "" && ssnPart2 == "" && ssnPart3 == "" && confirmSsnPart1 == "" && confirmSsnPart2 == "" && confirmSsnPart3 == "");
   }

  function validateOwnSecurityQuestion(src, args) {
    if ($("#SecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
      var ownSecurityQuestion = $("#SecurityQuestionTextBox").val();
      if (ownSecurityQuestion.length > 0) ownSecurityQuestion = TrimText(ownSecurityQuestion);
      args.IsValid = (ownSecurityQuestion != "");
    } else {
      args.IsValid = true;
    }
  }

  function validateUCOwnSecurityQuestion(src, args) {
    if ($("#UCSecurityQuestionDropDown").val() == "<%= CreateOwnSecurityQuestionValue %>") {
      var ownSecurityQuestion = $("#UCSecurityQuestionTextBox").val();
      if (ownSecurityQuestion.length > 0) ownSecurityQuestion = TrimText(ownSecurityQuestion);
      args.IsValid = (ownSecurityQuestion != "");
    } else {
      args.IsValid = true;
    }
  }
  function LogInOrRegister_ValidateAdditionalField(src, args) {
    args.IsValid = true;
    if ($("#" + src.controltovalidate).is(":visible")) {
      var fieldValue = TrimText(args.Value);
      args.IsValid = (fieldValue != null && fieldValue.length > 0);
    }
  }
  
  function LogInOrRegister_ValidateState(src, args) {
    args.IsValid = true;
    if ($("#" + src.controltovalidate).is(":visible")) {
      var stateId = args.Value;
      args.IsValid = (stateId > 0);
    }
  }
 
  function LogInOrRegister_ValidateDateOfBirth(sender, args) {
    if ($("#" + sender.controltovalidate).is(":visible")) {
      if (args.Value != "") {
        var dateRegx = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;

        if (dateRegx.test(args.Value) == false || (!moment(args.Value, "MM/DD/YYYY", true).isValid())) {
          sender.innerHTML = "<%= DateErrorMessage %>";
          args.IsValid = false;
        } else {
          var birthStrings = args.Value.split("/");
          var birthDate = new Date(parseInt(birthStrings[2], 10), parseInt(birthStrings[0], 10) - 1, parseInt(birthStrings[1], 10));

          var age = GetAgeForDate(birthDate);
          if (age < <%=App.Settings.MinimumCareerAgeThreshold %>) {
            sender.innerHTML = "<%= DOBMinLimitErrorMessage %>";
            args.IsValid = false;
          } else if (age > 99) {
            sender.innerHTML = "<%= DOBMaxLimitErrorMessage %>";
            args.IsValid = false;
          }
        }
      }
      else {
        sender.innerHTML = "<%= DateOfBirthRequiredMessage %>";
        args.IsValid = false;
      }
    }
     }

     function LogInOrRegister_ValidatePhoneNumberRequired(sender, args) {
	     args.IsValid = args.Value != '';
     	}

  function LogInOrRegister_ValidatePhoneNumber(sender, args) {
    if (args.Value != '') {
      var phoneRegx = /^\([0-9]\d{2}\)\s?\d{3}\-\d{4}$/;
      if (phoneRegx.test(args.Value) == false) {
        args.IsValid = false;
      }
    }
  }

  function GetStateCountyCityAndCountiesForPostalCode(postalCode, stateDropDownList, cityTextBox, valiationGroupName) {
    var options = { type: "POST",
      url: "<%= UrlBuilder.AjaxService() %>/GetStateCityCountyAndCountiesForPostalCode",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      async: true,
      data: '{"postalCode": "' + postalCode + '"}',
      success: function (response) {

        var results = response.d;
        if (results && results.StateId) {
          $("#" + stateDropDownList).val(results.StateId);
          $("#" + stateDropDownList).next().find(':first-child').text($("#" + stateDropDownList + " option:selected").text()).parent().addClass('changed');

          if (results.CityName != null) {
            $("#" + cityTextBox).val(results.CityName);
          }
        }
        else {
          $("#" + stateDropDownList).val('');
          $("#" + cityTextBox).val('');
        }

        if (valiationGroupName != null)
          Page_ClientValidate(valiationGroupName);
      }
    };

    $.ajax(options);
   }

  function OpenConsent() {
  	window.open("<%= LaborRegulationsUrl %>");
		return false;
	}
  
  function PhoneNoTypeChanged() {
  	var phoneValue;
  	var phoneDropDown = $('#PhoneNoDropDownList');
  	if ($(phoneDropDown).exists())
  		phoneValue = $(phoneDropDown).val();

  	if (phoneValue == "Cell") {
  		if ($("#CellphoneProviderRow").exists())
  			$("#CellphoneProviderRow").show();

  		if ($("OAuthCellphoneProviderRow").exists())
  			$("OAuthCellphoneProviderRow").show();
  	}
  	else {
  		if ($("#CellphoneProviderRow").exists())
  			$("#CellphoneProviderRow").hide();

  		if ($("OAuthCellphoneProviderRow").exists())
  			$("OAuthCellphoneProviderRow").hide();
  	}

  	ChangePhoneNoFormat();
  }

	function ChangePhoneNoFormat() {
		var phoneValue;
		var phoneDropDown = $('#PhoneNoDropDownList');
		if ($(phoneDropDown).exists())
			phoneValue = $(phoneDropDown).val();
		var phoneTextBox = $('#PhoneNoTextBox');

		//var behaviorPhone = $find('meAdditionalInformationPhoneNumber');
		if (phoneValue != "NonUS") {
			$('#AdditionalInformationPhoneNumberTextBox').mask('(999) 999-9999');
		}
		else {
			var value = $('#AdditionalInformationPhoneNumberTextBox').val();
            value = value.replace(/\(|\-|\)|\s/g, '');
            $('#AdditionalInformationPhoneNumberTextBox').unmask();
            $('#AdditionalInformationPhoneNumberTextBox').val(value);
		}
		$(phoneTextBox).focus();
	}

    $(window).on('load', function() {
    if($("iframe[width='304']").length){$("iframe[width='304']").attr('title','recaptcha_iframe');}
    // code here
    });
    //# sourceURL=Register.js
</script>
