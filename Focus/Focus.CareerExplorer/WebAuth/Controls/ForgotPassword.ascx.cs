﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Controls.Server;
using Focus.Core;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class ForgotPassword : UserControlBase
	{
		protected const string CreateOwnSecurityQuestionValue = "Own";

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				EmailAddressRegEx.ValidationExpression =
                    EmailAddressRegEx2.ValidationExpression = App.Settings.EmailAddressRegExPattern;
				ApplyTheme();
				LocaliseUI();
			}

		}


		protected bool CheckUserHasASavedQuestion(string userName, string emailAddress)
		{
			var result = false;

			var questionDropDown = (DropDownList)FindControl("FPSecurityQuestionDropDown3");
			questionDropDown.Items.Clear();

			if (App.Settings.EnableCareerPasswordSecurity && App.Settings.Module != FocusModules.Talent)
			{
				var username = userName.Trim();
				var email = emailAddress.Trim();
				var questions = ServiceClientLocator.AuthenticationClient(App).GetSecurityQuestions(email, username, App.Settings.Module);

				if (questions.Error == ErrorTypes.UserNotFound)
				{
					ForgotPasswordModal.Show();
				}
				else
				{
					var questionList = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions);
				  if (questions.SecurityQuestionId1.HasValue)
				  {
				    var questionObj = questionList.FirstOrDefault(sq => sq.Id == questions.SecurityQuestionId1);
            var listItem = new ListItem(questionObj.IsNull() ? string.Empty : questionObj.Text, questions.SecurityQuestionId1.ToString());
            questionDropDown.Items.Add(listItem);
				    result = true;
				  }
          else if (questions.SecurityQuestion1.IsNotNullOrEmpty())
				  {
            var listItem = new ListItem(questions.SecurityQuestion1, "");
            questionDropDown.Items.Add(listItem);
            result = true;
				  }

					if (questions.SecurityQuestionId2.HasValue)
					{
						var questionObj = questionList.FirstOrDefault(sq => sq.Id == questions.SecurityQuestionId2);
            var listItem = new ListItem(questionObj.IsNull() ? string.Empty : questionObj.Text, questions.SecurityQuestionId2.ToString());
            questionDropDown.Items.Add(listItem);
						result = true;
					}
          else if (questions.SecurityQuestion2.IsNotNullOrEmpty())
          {
            var listItem = new ListItem(questions.SecurityQuestion2, "");
            questionDropDown.Items.Add(listItem);
            result = true;
          }

				}
			}
			return result;
		}

		/// <summary>
		/// Handles the Click event of the btnRequestPassword control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void btnRequestPassword_Click(object sender, EventArgs e)
		{
			try
			{
				string resetPasswordUrl;
				string pinRegistrationUrl;
				string securityQuestion;
				string securityAnswer;
				long? securityQuestionId;
				int dayOfBirth;
				int monthOfBirth;
				var emailAddress = RetrieveSecurityArguments(out resetPasswordUrl, out pinRegistrationUrl, out securityQuestion, out securityAnswer, out securityQuestionId, out dayOfBirth, out monthOfBirth);

				var attemptsLeft = ServiceClientLocator.AuthenticationClient(App)
					.ProcessForgottenPassword(InputUsername, emailAddress, resetPasswordUrl, App.Settings.Module, securityQuestion,
						securityQuestionId, securityAnswer, dayOfBirth, monthOfBirth, pinRegistrationUrl);
				if (attemptsLeft.IsNull())
				{
					ShowResult(false, CodeLocalise("EmailSent", "Email has been successfully sent."));
				}
				else
				{
					var message = attemptsLeft.Value == 1
						? CodeLocalise("FailedAttemptLeft",
							"You have 1 more attempt to answer correctly before your account becomes blocked")
						: CodeLocalise("FailedAttemptsLeft",
							"You have {0} more attempts to answer correctly before your account becomes blocked", attemptsLeft.Value);

					ShowResult(true, message);
				}
			}
			catch (ApplicationException ex)
			{
				var errorType = ex.GetErrorType();

				switch (errorType)
				{
					case ErrorTypes.ValidationFailed:
					case ErrorTypes.UserBlocked:
						ShowResult(true,
							CodeLocalise("Global.OnHoldMessage",
								"Your account has been placed on hold. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> for further assistance."));
						break;

					default:
						ShowResult(true,CodeLocalise("ResetPasswordError.Text",
                            "No match to the email address entered was found."));
						break;
				}
			}

			ForgotPasswordModal.Show();
		}

		private string RetrieveSecurityArguments(out string resetPasswordUrl, out string pinRegistrationUrl,
			out string securityQuestion, out string securityAnswer, out long? securityQuestionId, out int dayOfBirth,
			out int monthOfBirth)
		{
			var emailAddress = (ForgotPasswordEmailAddressTextbox.Text ?? string.Empty).Trim();

			var applicationPath = App.Settings.Module == FocusModules.Explorer
				? App.Settings.ExplorerApplicationPath
				: App.Settings.CareerApplicationPath;

			resetPasswordUrl = string.Concat(applicationPath, UrlBuilder.ResetPassword(false));

			pinRegistrationUrl = (App.Settings.Theme == FocusThemes.Education)
				? string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.PinRegistration(false))
				: string.Empty;

			securityQuestion = "";
			securityAnswer = "";
			securityQuestionId = null;
			dayOfBirth = 0;
			monthOfBirth = 0;

			return emailAddress;
		}

		/// <summary>
		/// Shows the result of the reset password operation on the page.
		/// </summary>
		/// <param name="isError">if set to <c>true</c> [is error].</param>
		/// <param name="message">The message.</param>
		private void ShowResult(bool isError, string message)
		{
			lblResetPassword.Text = message;
			lblResetPassword2.Text = message;
			lblResetPassword3.Text = message;
			lblResetPassword.CssClass = isError ? "inPageError" : "inPageMessage";
			lblResetPassword2.CssClass = isError ? "inPageError" : "inPageMessage";
			lblResetPassword3.CssClass = isError ? "inPageError" : "inPageMessage";
			lblResetPassword.Visible = true;
			lblResetPassword2.Visible = true;
			lblResetPassword3.Visible = true;

		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address is not correct format");
			ConfirmEmailAddressRequired.ErrorMessage = CodeLocalise("ConfirmEmailAddress.RequiredErrorMessage",
				"Confirm email address is required");
			ConfirmEmailAddressCompare.ErrorMessage = CodeLocalise("ConfirmEmailAddress.CompareErrorMessage",
				"Email addresses must match");
			btnRequestPassword.Text = App.Settings.Theme == FocusThemes.Workforce
				? CodeLocalise("Global.RequestPassword.Text", "Request password reset")
				: CodeLocalise("Global.RequestPassword.Text:Education", "Request password/PIN reset");

			SubmitButton2.Text = CodeLocalise("Global.Submit", "Submit");
			btnRequestPassword3.Text = App.Settings.Theme == FocusThemes.Workforce
	? CodeLocalise("Global.RequestPassword.Text", "Request password reset")
	: CodeLocalise("Global.RequestPassword.Text:Education", "Request password/PIN reset");

			SecurityQuestionRequiredFieldValidator3.ErrorMessage = CodeLocalise("SecurityQuestionRequiredFieldValidator3.ErrorMessage", "Security question is required");
			SecurityQuestionTextBoxRequiredFieldValidator3.ErrorMessage = CodeLocalise("SecurityQuestionTextBoxRequiredFieldValidator3.ErrorMessage", "Security question is required");
			SecurityAnswerTextBoxRequiredFieldValidator3.ErrorMessage = CodeLocalise("SecurityAnswerTextBoxRequiredFieldValidator3.ErrorMessage", "Security answer is required");
			DayOfBirthTextBoxRequiredFieldValidator3.ErrorMessage = CodeLocalise("DayOfBirthTextBoxRequiredFieldValidator3.ErrorMessage", "Day of birth is required");
			DayOfBirthTextBoxRegularExpressionValidator3.ErrorMessage = CodeLocalise("DayOfBirthTextBoxRequiredFieldValidator3.ErrorMessage", "Invalid day of birth");
			MonthOfBirthRequiredFieldValidator3.ErrorMessage = CodeLocalise("MonthOfBirthRequiredFieldValidator3.ErrorMessage", "Month of birth is required");

			// ReSharper disable LocalizableElement
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = true, Text = HtmlLocalise("SelectMonth.Text", "- select month -"), Value = "0" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.January.Label", "January"), Value = "1" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.February.Label", "February"), Value = "2" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.March.Label", "March"), Value = "3" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.April.Label", "April"), Value = "4" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.May.Label", "May"), Value = "5" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.June.Label", "June"), Value = "6" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.July.Label", "July"), Value = "7" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.August.Label", "August"), Value = "8" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.September.Label", "September"), Value = "9" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.October.Label", "October"), Value = "10" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.November.Label", "November"), Value = "11" });
			MonthOfBirthDropDownList3.Items.Add(new ListItem { Selected = false, Text = HtmlLocalise("Global.December.Label", "December"), Value = "12" });
			// ReSharper restore LocalizableElement
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			PasswordHelpInfoLabel.LocalisationKey = App.Settings.EnableCareerPasswordSecurity
				? "PasswordWithSecurityHelpInfo.Text"
				: "PasswordHelpInfo.Text";
			PasswordHelpInfoLabel.DefaultText = App.Settings.EnableCareerPasswordSecurity
				? "When you enter your email address and answer the security questions correctly, #FOCUSCAREER# will send you an email containing a link to a web page where you can reset your password. The required security information is what we already know about you from account registration or by subsequent account updates. You will be given 3 attempts to provide the correct information before your account becomes locked.<br /><br />Once you have reset your password, return to the login page and use it to log in as usual. Once you have logged in, remember you can change your username, password and security questions in My Account.<br /><br />If you do not receive the password reset email or your account has become locked, please email <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> for further assistance."
				: "To request a password reset, please enter your email address below. #FOCUSCAREER# will send you an email containing a link to a web page where you can reset your password.";
		}


		#region FVN-3488 - Provide security questions specific to user

		private string InputUsername
		{
			get { return GetViewStateValue("ForgotPassword:InputUsername", ""); }
			set { SetViewStateValue("ForgotPassword:InputUsername", value); }
		}

		private string InputEmail
		{
			get { return GetViewStateValue("ForgotPassword:InputEmail", ""); }
			set { SetViewStateValue("ForgotPassword:InputEmail", value); }
		}

		/// <summary>
		/// Handles the Click event of the SubmitButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SubmitButton2_Click(object sender, EventArgs e)
		{
			// Page.Validate occurs for all validators on the page even those not relevant.  This method validates those in the ForgotPassword2Modal only
			if (!ValidateForgotPassword2Modal()) 
				return;

			if (App.Settings.Module != FocusModules.Talent)
			{
				InputUsername = ForgotPasswordUserNameTextBox2.Text.Trim();
				InputEmail = ForgotPasswordEmailAddressTextBox2.Text.Trim();

				try
				{
					if (CheckUserHasASavedQuestion(InputUsername, InputEmail))
					{
						ClearValidationMessages();
						ForgotPasswordModal.Hide();
						ForgotPasswordModal2.Hide();
						ForgotPasswordModal3.Show();
					}
					else
					{
						ForgotPasswordModal.Hide();
						ForgotPasswordModal3.Hide();
						ForgotPasswordModal2.Show();
						var helpInfo = (LocalisedLabel) FindControl("ErrorDescription2");
                        helpInfo.DefaultText =
                            ForgotPasswordUserNameTextBox2.Text.IsNotNullOrEmpty() ?
                             CodeLocalise("ResetPasswordError.Text",
                             "No match to the username entered was found.") : CodeLocalise("ResetPasswordError.Text",
                            "No match to the email address entered was found.");
					}
				}
				catch (Exception ex)
				{
					ShowResult(true, CodeLocalise("ResetPasswordError.Text", ex.Message));
								//"No match to the email address entered was found. Please check and try again or contact your administration team to request an invitation to register."));
				}
		}
		}

		private bool ValidateForgotPassword2Modal()
		{
			var eitherFieldvalidator = (CustomValidator)FindControl("EitherFieldValidator");
			var notBothFieldsValidator = (CustomValidator)FindControl("NotBothFieldValidator");
			if (eitherFieldvalidator.Enabled && eitherFieldvalidator.IsValid && notBothFieldsValidator.Enabled &&
			    notBothFieldsValidator.IsValid)
			{
				return true;
			}
			return false;
		}

		private void ClearValidationMessages()
		{
			lblResetPassword.Text = 
				lblResetPassword2.Text =
					lblResetPassword3.Text = "";

			lblResetPassword.CssClass =
				lblResetPassword2.CssClass =
					lblResetPassword3.CssClass = "";

			lblResetPassword.Visible =
				lblResetPassword2.Visible =
					lblResetPassword3.Visible = false;
		}

		/// <summary>
		/// Handles the Click event of the btnRequestPassword control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void btnRequestPassword3_Click(object sender, EventArgs e)
		{
			try
			{
				var applicationPath = App.Settings.Module == FocusModules.Explorer
					? App.Settings.ExplorerApplicationPath
					: App.Settings.CareerApplicationPath;

				var resetPasswordUrl = string.Concat(applicationPath, UrlBuilder.ResetPassword(false));

				var pinRegistrationUrl = (App.Settings.Theme == FocusThemes.Education)
					? string.Concat(App.Settings.CareerApplicationPath, UrlBuilder.PinRegistration(false))
					: string.Empty;

				string securityQuestion = "", securityAnswer = "";
				long? securityQuestionId = null;
				int dayOfBirth = 0, monthOfBirth = 0;

				if (ExtraQuestionsPlaceHolder3.Visible)
				{
				  securityQuestion = FPSecurityQuestionDropDown3.SelectedItem.Text;

          securityQuestionId = FPSecurityQuestionDropDown3.SelectedItem.Value.IsNullOrEmpty() ? 
            (long?)null :
            Convert.ToInt64(FPSecurityQuestionDropDown3.SelectedItem.Value);

					securityAnswer = SecurityAnswerTextBox3.Text;

					dayOfBirth = int.Parse(DayOfBirthTextBox3.Text);
					monthOfBirth = int.Parse(MonthOfBirthDropDownList3.SelectedValue);
				}

				var attemptsLeft = ServiceClientLocator.AuthenticationClient(App).ProcessForgottenPassword(InputUsername, InputEmail, resetPasswordUrl, App.Settings.Module, securityQuestion, securityQuestionId, securityAnswer, dayOfBirth, monthOfBirth, pinRegistrationUrl);
				if (attemptsLeft.IsNull())
				{
					ShowResult(false, CodeLocalise("EmailSent", "Email has been successfully sent."));
				}
				else
				{
					var message = attemptsLeft.Value == 1
						? CodeLocalise("FailedAttemptLeft", "You have 1 more attempt to answer correctly before your account becomes blocked")
						: CodeLocalise("FailedAttemptsLeft", "You have {0} more attempts to answer correctly before your account becomes blocked", attemptsLeft.Value);

					ShowResult(true, message);
				}
			}
			catch (ApplicationException ex)
			{
				var errorType = ex.GetErrorType();

				switch (errorType)
				{
					case ErrorTypes.ValidationFailed:
					case ErrorTypes.UserBlocked:
						ShowResult(true, CodeLocalise("Global.OnHoldMessage", "Your account has been placed on hold. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> for further assistance."));
						break;

					default:
						ShowResult(true, CodeLocalise("ResetPasswordError.Text", "No match to the email address entered was found."));
						break;
				}
			}

			ForgotPasswordModal3.Show();
		}

		/// <summary>
		/// Server validation to check either field is entered
		/// </summary>
		/// <param name="source">The custom validator</param>
		/// <param name="args">Validation arguments</param>
		protected void EitherFieldValidator2_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var userNameEntered = ForgotPasswordUserNameTextBox2.Text.Trim().Length > 0;
			var emailAddressEntered = ForgotPasswordEmailAddressTextBox2.Text.Trim().Length > 0;

			args.IsValid = (userNameEntered || emailAddressEntered);
		}

		/// <summary>
		/// Server validation to check that not both fields is entered
		/// </summary>
		/// <param name="source">The custom validator</param>
		/// <param name="args">Validation arguments</param>
		protected void NotBothFieldsValidator_OnServerValidate(object source, ServerValidateEventArgs args)
		{
			var userNameEntered = ForgotPasswordUserNameTextBox2.Text.Trim().Length > 0;
			var emailAddressEntered = ForgotPasswordEmailAddressTextBox2.Text.Trim().Length > 0;

			args.IsValid = !(userNameEntered && emailAddressEntered);
		}


		#endregion

	}
}