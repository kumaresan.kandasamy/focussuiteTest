﻿using System;

using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.ServiceClients;
using Focus.Core;

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class ForgottenPassword : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				ForgottenPasswordEmailAddressRegEx.ValidationExpression = App.Settings.UserNameRegExPattern;
				ForgottenPasswordEmailAddressRegEx.Enabled = (ForgottenPasswordEmailAddressRegEx.ValidationExpression.IsNotNullOrEmpty());
				LocaliseUI();
			}
		}

		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			var emailAddress = (ForgottenPasswordEmailAddressTextBox.Text ?? string.Empty).Trim();

			try
			{
				var resetPasswordUrl = "";

				switch (App.Settings.Module)
				{
					case FocusModules.Explorer:
						resetPasswordUrl = string.Format("{0}{1}", App.Settings.ExplorerApplicationPath, UrlBuilder.ResetPassword());
						break;
					default:
						throw new Exception(FormatError(ErrorTypes.UnableToProcessForgottenPassword, "Unable to process forgotten password"));
				}

				AuthenticationClient.Instance.ProcessForgottenPassword(emailAddress, emailAddress, resetPasswordUrl);

				ConfirmationModal.Show(CodeLocalise("Success.Title", "Forgotten Password Request Sent"),
											 CodeLocalise("Success.Body", "An email has been sent to your account which will allow you to reset your password."),
											 CodeLocalise("Global.Close.Text", "Close"));

				ForgottenPasswordEmailAddressTextBox.Text = "";
			}
			catch (Exception ex)
			{
				ForgottenPasswordValidator.IsValid = false;
				ForgottenPasswordValidator.ErrorMessage = ex.Message;
				ForgottenPasswordModal.Show();
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			ForgottenPasswordEmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			ForgottenPasswordEmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address is not correct format");
			SubmitButton.Text = CodeLocalise("Global.Submit.Text", "Submit");
		}
	}
}