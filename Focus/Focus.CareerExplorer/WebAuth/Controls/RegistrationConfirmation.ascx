﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RegistrationConfirmation.ascx.cs"
	Inherits="Focus.CareerExplorer.WebAuth.Controls.RegistrationConfirmation" %>
<%@ Register TagPrefix="uc" TagName="ResumeWizardRedirectConfirmationModal" Src="~/Controls/ConfirmationModal.ascx" %>
<%@ Import Namespace="Focus" %>

<asp:HiddenField ID="RegistrationConfirmationModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="RegistrationConfirmationModal" runat="server" BehaviorID="RegistrationConfirmationModal"
	TargetControlID="RegistrationConfirmationModalDummyTarget" PopupControlID="RegistrationConfirmationPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel id="RegistrationConfirmationPanel" runat="server" class="modal registrationConfirmationModal" style="z-index: 100001; display: none;">
	<div class="FocusCareer">
		<div class="modalHeader">
			<asp:ImageButton ID="RedirectCloseImage" runat="server" class="modalCloseIcon" width="20" height="20" OnClick="RedirectCloseImage_Click" />
			<asp:Label runat="server" ID="WelcomeLabel" CssClass="modalTitle modalTitleLarge"></asp:Label>
		</div>
    
    <div style="width: 100%">
      <div style="float: left">
  		  <focus:LocalisedLabel runat="server" ID="RegistrationConfirmationHeadingLabel" CssClass="modalMessage embolden" LocalisationKey="RegistrationConfirmationHeading.Label" DefaultText="Your account is ready to use. Get started now:"/>
      </div>

      <asp:PlaceHolder runat="server" ID="TutorialVideoPlaceHolder">
        <div style="float: right" class="landingPageLink">
          <asp:HyperLink runat="server" ID="TutorialVideoLink"></asp:HyperLink>
        </div>
      </asp:PlaceHolder>  
      <div style="clear:both"></div>    
    </div>

		<div class="landingPageNavigation landingPageRegistrationConfirmation <%= OldApp_RefactorIfFound.Settings.Theme %>Theme" id="NavigationPlaceholder">
			<asp:Panel runat="server" ID="CareerOptionsPanel">
				<div class="lpItem lpItemMed">
					<div class="lpItemHeaderBlue lpCreateEditResume">
						<span><a href="<%=UrlBuilder.YourResume()%>" class="lpItemHeaderText"><focus:LocalisedLabel runat="server" ID="CreateUploadResume1Label" RenderOuterSpan="False" LocalisationKey="CreateUploadResume1.LinkText" DefaultText="Create or upload a resume"/></a></span>
						<focus:LocalisedLabel runat="server" ID="CreateUploadResume2Label" LocalisationKey="CreateUploadResume2.LinkText" DefaultText="best  bet for best matches" CssClass="lpItemHeaderBlueHint"/>
					</div>
				</div>
				<div class="lpItem lpItemMed collapsiblePanel cpCollapsed">
					<div class="lpItemHeaderBlue lpJobSearch cpHeaderControl">
						<focus:LocalisedLabel runat="server" ID="SearchForJobsLabel" CssClass="lpItemHeaderText" LocalisationKey="SearchForJobs.LinkText" DefaultText="Search for jobs"/>
					</div>
					<div class="lpItemContentPanelBlue collapsiblePanelContent">
						<div class="lpItemContentLinkRowBlue">
    					<asp:LinkButton ID="lnkJobBasedResume" runat="server" CssClass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="Resume"><focus:LocalisedLabel runat="server" ID="JobsBasedOnResumeLabel" RenderOuterSpan="False" LocalisationKey="JobsBasedOnResume.LinkText" DefaultText="See job recommendations based on my resume"/></asp:LinkButton>
							<focus:LocalisedLabel runat="server" ID="JobsBasedOnResumeHintLabel" LocalisationKey="JobsBasedOnResumeHint.Label" DefaultText="we'll help you build or upload your resume first" CssClass="lpItemContentLinkBlueHint"/>
						</div>
						<div class="lpItemContentLinkRowBlue">
							<asp:LinkButton ID="JobSearchWithoutResumeLink" runat="server" CssClass="lpItemContentLinkText" OnClick="JobSearchWithoutResumeLink_Click"><focus:LocalisedLabel runat="server" ID="JobsWithoutResumeLabel" LocalisationKey="JobsWithoutResume.LinkText" DefaultText="Start or continue a job search without a resume" RenderOuterSpan="False"/></asp:LinkButton>
							<focus:LocalisedLabel runat="server" ID="JobsWithoutResumeHintLabel" LocalisationKey="JobsWithoutResumeHint.Label" DefaultText="results are limited without a resume" CssClass="lpItemContentLinkBlueHint"/>
						</div>
					</div>
				</div>
			</asp:Panel>
			<asp:Panel runat="server" ID="ExplorerOptionsPanel">
			  <div id="SchoolRegistrationConfirmationDivs"></div>
				<div class="lpItem lpItemMed" id="SchoolRegistrationConfirmationDiv" style="display:none">
					<div class="lpItemHeaderCyan lpSchoolStudy">
						<a href='<%=UrlBuilder.Explore("school") %>' class="lpItemHeaderText"><focus:LocalisedLabel runat="server" ID="SchoolStudyLabel" RenderOuterSpan="False" LocalisationKey="ExploreSchool.LinkText" DefaultText="Explore careers by #SCHOOLNAME# programs of study"/></a>
					</div>
				</div>
				<div class="lpItem lpItemMed" id="ExploreRegistrationConfirmationDiv" style="display:none">
					<div class="lpItemHeaderCyan lpExploreCareer">
						<a href='<%=UrlBuilder.Explore("explore") %>' class="lpItemHeaderText"><focus:LocalisedLabel runat="server" ID="ExploreCareerOptionsLabel" RenderOuterSpan="False" LocalisationKey="ExploreCareerOptions.LinkText" DefaultText="Explore my career and internship options"/></a>
					</div>
				</div>
				<div class="lpItem lpItemMed" id="ResearchRegistrationConfirmationDiv" style="display:none">
					<div class="lpItemHeaderCyan lpResearchCareer">
						<a href='<%=UrlBuilder.Explore("research") %>' class="lpItemHeaderText"><focus:LocalisedLabel runat="server" ID="ResearchStudyLabel" RenderOuterSpan="False" LocalisationKey="ResearchStudy.LinkText" DefaultText="Research a specific program of study, career or #BUSINESS#:LOWER"/></a>
					</div>
				</div>
				<div class="lpItem lpItemMed" id="StudyRegistrationConfirmationDiv" style="display:none">
					<div class="lpItemHeaderCyan lpGetAhead">
						<a href='<%=UrlBuilder.Explore("study") %>' class="lpItemHeaderText"><focus:LocalisedLabel runat="server" ID="SeeWhatICanStudyLabel" RenderOuterSpan="False" LocalisationKey="SeeWhatICanStudy.LinkText" DefaultText="See what I can study to get ahead"/></a>
					</div>
				</div>
				<div class="lpItem lpItemMed" id="ExperienceRegistrationConfirmationDiv" style="display:none">
					<div class="lpItemHeaderCyan lpExperience">
						<a href='<%=UrlBuilder.Explore("experience") %>' class="lpItemHeaderText"><focus:LocalisedLabel runat="server" ID="ExperienceTakesMeLabel" RenderOuterSpan="False" LocalisationKey="ExperienceTakesMe.LinkText" DefaultText="See where my experience can take me"/></a>
					</div>
				</div>
			</asp:Panel>
      <asp:Panel runat="server" ID="EducationOptionsPanel">
        <div class="lpItem lpItemMed collapsiblePanel">
          <div class="lpItemHeaderBlue lpRecommendJobs cpHeaderControl">
					  <focus:LocalisedLabel runat="server" ID="JobRecommendationsLabels" CssClass="lpItemHeaderText" LocalisationKey="RecommendationsBasedOn.LinkText" DefaultText="See job recommendations based on"/>
          </div>
          <div class="lpItemContentPanelBlue collapsiblePanelContent">
            <div class="lpItemContentLinkRowBlue">
							<asp:linkbutton ID="EducationJobsBasedOnProgramOfStudyLink" runat="server" cssclass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="ProgramOfStudy">
							  <focus:LocalisedLabel runat="server" ID="EducationJobsBasedOnProgramOfStudyLabel" RenderOuterSpan="False" LocalisationKey="JobsBasedOnProgramOfStudy.LinkText" DefaultText="My program of study"/>
							</asp:linkbutton>
            </div>
            <div class="lpItemContentLinkRowBlue" id="MyResumeDiv" runat="server">
							<asp:linkbutton ID="EducationJobsBasedOnResumeLink" runat="server" cssclass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="Resume">
							  <focus:LocalisedLabel runat="server" ID="EducationJobsBasedOnResumeLabel" RenderOuterSpan="False" LocalisationKey="JobsBasedOnResume.LinkText" DefaultText="My resume"/>
							</asp:linkbutton>
							<focus:LocalisedLabel runat="server" ID="EducationJobsBasedOnResumeHintLabel" LocalisationKey="JobsBasedOnResumeHint.LinkText" DefaultText="we'll help you build or upload your resume first" CssClass="lpItemContentLinkBlueHint lpItemContentLinkBlueHintBuildResume"/>
            </div>
            <div class="lpItemContentLinkRowBlue" id="MyResumeAndProgrammeOfStudyDiv" runat="server">
   						<asp:linkbutton ID="EducationJobsBasedOnResumeAndProgramOfStudyLink" runat="server" cssclass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="ResumeAndProgramOfStudy">
							  <focus:LocalisedLabel runat="server" ID="EducationJobsBasedOnResumeAndProgramOfStudyLabel" RenderOuterSpan="False" LocalisationKey="JobsBasedOnResumeAndProgramOfStudy.LinkText" DefaultText="My resume and my program of study"/>
							</asp:linkbutton>
            </div>
          </div>
        </div>
        <div class="lpItem lpItemMed">
          <div class="lpItemHeaderBlue lpJobSearch">
						<asp:LinkButton ID="EducationJobSearchWithoutResumeLink" runat="server" CssClass="lpItemHeaderText" OnClick="EducationJobSearchWithoutResumeLink_Click">
						  <focus:LocalisedLabel runat="server" ID="EducationJobsWithoutResumeLabel" LocalisationKey="SearchForJobs.LinkText" DefaultText="Search for jobs" RenderOuterSpan="False"/>
						</asp:LinkButton>
          </div>
        </div>
        <div class="lpItem lpItemMed">
          <div class="lpItemHeaderBlue lpInternshipSearch">
						<asp:LinkButton ID="InternshipsSearchLink" runat="server" CssClass="lpItemHeaderText" OnClick="InternshipsSearchLink_Click">
						  <focus:LocalisedLabel runat="server" ID="InternshipsLabel" LocalisationKey="Internships.LinkText" DefaultText="Search for internship opportunities" RenderOuterSpan="False"/>
						</asp:LinkButton>
          </div>
        </div>
      </asp:Panel>
      </div>
	</div>
</asp:Panel>
<uc:ResumeWizardRedirectConfirmationModal ID="ResumeWizardRedirectConfirmationModal" runat="server" />
<script language="javascript" type="text/javascript">
	// Set order of explore div sections
	$(document).ready(function () {
	    var navigationSections = <%= MajorDivOrder %>;
	    for (var h = 0; h < navigationSections.length; h++) {
	        $("#" + navigationSections[h]).appendTo("#NavigationPlaceholder");
	    }

	    //var exploreDivs = ["ExploreSection", "ResearchSection", "StudySection"];
		var exploreSections = <%= ExploreSections %>;
		for (var i = 0; i < exploreSections.length; i++) {
			var exploreDiv = exploreSections[i] + "RegistrationConfirmationDiv";
			$("#" + exploreDiv).appendTo("#SchoolRegistrationConfirmationDivs");
	  	$("#" + exploreDiv).show();
		}
	});
	
	Sys.Application.add_load(RegistrationConfirmation_PageLoad);

	function RegistrationConfirmation_PageLoad(sender, args) {
		// Jaws Compatibility
		var modal = $find('RegistrationConfirmationModal');

		if (modal != null) {
			modal.add_shown(function () {
				$('#RegistrationConfirmationPanel').attr('aria-hidden', false);
				$('.page').attr('aria-hidden', true);
				// set focus on first exposed link, and remove Chrome orange outline (but then reinstate it for when the user tabs away from and then back onto that link)
				$('#EducationJobsBasedOnProgramOfStudyLink').focus().css('outline-width', '0').blur(function () {
					$(this).focus(function () {
						$(this).css('outline-width', '5px');
					});
				});
			});
			modal.add_hiding(function () {
				$('#RegistrationConfirmationPanel').attr('aria-hidden', true);
				$('.page').attr('aria-hidden', false);
			});
		}
	}
</script>
