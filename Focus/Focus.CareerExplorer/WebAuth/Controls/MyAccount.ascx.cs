﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Web.Core.Models;
using Framework.Core;
using Framework.Exceptions;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class MyAccount : UserControlBase
	{
		protected const string CreateOwnSecurityQuestionValue = "Own";

		private bool _isCareerTypeSet;

		private UserDetailsModel _userDetails;

		private UserDetailsModel UserDetails
		{
			get
			{
				if (_userDetails.IsNull())
					_userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId, 0, true);

				return _userDetails;
			}
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			ApplyTheme();
			//maskedPhoneNumber.Mask = App.Settings.PhoneNumberMaskPattern;
			PhoneNumberRegEx.ValidationExpression = App.Settings.PhoneNumberRegExPattern;
      NewEmailAddressTextBox.MaxLength = ConfirmEmailAddressTextBox.MaxLength = App.Settings.MaximumEmailLength;
			txtNewUsername.MaxLength = txtReNewUsername.MaxLength = App.Settings.MaximumUserNameLength;
		}

		#region Bind MyAccount section

		/// <summary>
		/// Binds this instance.
		/// </summary>
		internal void Bind()
		{
			if (App.Settings.Theme == FocusThemes.Education)
				BindMobilePhoneProvider();

			NewUsernameRegEx.ValidationExpression = App.Settings.UserNameRegExPattern;
			NewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
			NewUserNameMinLengthRegEx.ValidationExpression = App.Settings.UserNameMinLengthPattern;
			//Added by SAV on 2012.03.06 : Added validation for password characters.
			PasswordCharRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;

			if (App.User.IsShadowingUser)
			{
				//txtOldPassword.ReadOnly = true;
				txtOldPassword.Attributes["value"] = "********";
			}

			NewEmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;

			if (App.UserData.Profile.IsNotNull())
				txtOldUsername.Text = App.User.UserName;

			OldEmailAddressTextBox.Text = App.User.EmailAddress;

			LoadSecurityQuestionAnswer();

			LocaliseUI();

			txtNewUsername.Text = txtReNewUsername.Text = "";
			NewEmailAddressTextBox.Text = ConfirmEmailAddressTextBox.Text = "";

			if (App.Settings.DisplayCareerAccountType)
			{
				BindAccountTypeDropdown(CurrentAccountTypeDropDownList);
				BindAccountTypeDropdown(NewAccountTypeDropdownList, false);
				ChangeAccountTypePlaceholder.Visible = true;
				CareerTypeAccountRequired.Enabled = true;
				var js =
					String.Format(
						@"function SwapSelection() {{ if (!{0} && $('#{2}').val() !='' ){{ $('#{1}').val($('#{2}').val()); }}}}",
						_isCareerTypeSet.ToString().ToLower(), NewAccountTypeDropdownList.ClientID,
						CurrentAccountTypeDropDownList.ClientID);

				Page.ClientScript.RegisterClientScriptBlock(GetType(), "SwapSelection", js, true);
			}
			else
			{
				ChangeAccountTypePlaceholder.Visible = false;
				CareerTypeAccountRequired.Enabled = false;
			}

			MyAccountModal.Show();
		}

		#endregion

		/// <summary>
		/// Loads the security question answer.
		/// </summary>
		private void LoadSecurityQuestionAnswer()
		{
			var questions = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions);

			UpdateSecurityQuestionDropDown.BindLookup(questions, null, CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"));
			UpdateSecurityQuestionDropDown.Items.Add(new ListItem(CodeLocalise("CreateOwnSecurityQuestion.DropDownValue", "Create own security question"), CreateOwnSecurityQuestionValue));

			UpdateSecurityQuestionDropDown.SelectedIndex = 0;
			OldSecurityQuestionTextBox.Text =
				OldSecurityAnswerTextBox.Text =
					UpdateSecurityQuestionTextBox.Text =
						UpdateSecurityAnswerTextBox.Text = string.Empty;

			var questionId = UserDetails.SecurityQuestionId;
			if (questionId.HasValue)
			{
				var question = questions.FirstOrDefault(q => q.Id == questionId);
				OldSecurityQuestionTextBox.Text = question.IsNotNull() ? question.Text : string.Empty;
			}
			else
			{
				OldSecurityQuestionTextBox.Text = UserDetails.SecurityQuestion;				
			}

			OldSecurityAnswerTextBox.Text = UserDetails.SecurityAnswer;
		}

		private void BindAccountTypeDropdown(ListControl dropDownList, bool setDefault = true)
		{
			dropDownList.Items.Clear();
			dropDownList.Items.AddEnum(CareerAccountType.JobSeeker, CodeLocalise("Global.CandidateType.Workforce", "Job Seeker"));
			dropDownList.Items.AddEnum(CareerAccountType.Student, CodeLocalise("Student", "Student"));
			dropDownList.Items.AddLocalisedTopDefault("Global.TopDefault", "- select -");

			if (UserDetails.PersonDetails.AccountType.IsNotNull() && setDefault)
			{
				_isCareerTypeSet = true;
				dropDownList.SelectedValue = UserDetails.PersonDetails.AccountType.ToString();
			}
		}


		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			UpdateSecurityQuestion.Text =
				UpdateProgramOfStudyButton.Text =
					UpdateEnrollmentStatusButton.Text =
						UpdateEmailAddressButton.Text =
							UpdatePhoneNumberButton.Text =
								UpdateCampusButton.Text = 
								UpdateAccountTypeButton.Text = CodeLocalise("UpdateButton.Text", "Update");

			if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer))
			{
				UpdateUsernameButton.Text = UpdatePasswordButton.Text = CodeLocalise("UpdateButton.Text", "Update");
			}
			else
			{
				UpdateUsernameButton.Text = CodeLocalise("UpdateButton.Text", "Change username");
				UpdatePasswordButton.Text = CodeLocalise("UpdateButton.Text", "Change password");
			}
			UsernameRequired.ErrorMessage = CodeLocalise("Username.RequiredErrorMessage", "Username is required");
			NewUsernameRegEx.ErrorMessage = CodeLocalise("Username.FormatErrorMessage", "Username is not in a valid email format");
			NewUserNameMinLengthRegEx.ErrorMessage = CodeLocalise("Username.UserMinLengthErrorMessage", "Username should be minimum of 6 characters");
			ConfirmUsernameRequired.ErrorMessage = CodeLocalise("ConfirmUsername.RequiredErrorMessage", "Confirm username is required");
			ConfirmUsernameCompare.ErrorMessage = CodeLocalise("ConfirmUsername.CompareErrorMessage", "Usernames must match");
			OldPasswordRequired.ErrorMessage = NewPasswordRequired.ErrorMessage = CodeLocalise("NewPassword.RequiredErrorMessage", "Password is required");
			NewPasswordRegEx.ErrorMessage = CodeLocalise("NewPasswordTextBox.RegExErrorMessage", "Password must be 6-20 characters and contain at least 1 number.");
			ConfirmNewPasswordRequired.ErrorMessage = CodeLocalise("ConfirmNewPassword.RequiredErrorMessage", "Confirm password is required");
			ConfirmNewPasswordCompare.ErrorMessage = CodeLocalise("ConfirmNewPasswordCompare.CompareErrorMessage", "Passwords must match");
			//Added by SAV on 2012.02.28 : To display error message when user enters Email address and password as same. 
			UserNameAndPasswordCompare.ErrorMessage = CodeLocalise("UsernameAndPasswordCompare.CompareErrorMessage", "Email address and password cannot be same");
			UpdateSecurityQuestionRequired.ErrorMessage = CodeLocalise("SecurityQuestionRequired.ErrorMessage", "A security question is required");
			UpdateSecurityQuestionCustomValidator.ErrorMessage = CodeLocalise("SecurityQuestionCustomValidator.ErrorMessage", "A security question is required");
			UpdateSecurityAnswerRequired.ErrorMessage = CodeLocalise("SecurityAnswerRequired.ErrorMessage", "A security answer is required");
			UpdateSecurityAnswerDiffenceValidator.ErrorMessage = CodeLocalise("UpdateSecurityAnswerDiffenceValidator.ErrorMessage", "Your security answer cannot be the same as your previous answer");
			ProgramOfStudyRequired.ErrorMessage = CodeLocalise("ProgramOfStudy.RequiredErrorMessage", "Department is required");
			DegreeRequired.ErrorMessage = DetailedDegreeAutoCompleteTextBoxValidator.ErrorMessage = CodeLocalise("Degree.RequiredErrorMessage", "Degree is required");
			EnrollmentStatusRequired.ErrorMessage = CodeLocalise("EnrollmentStatus.RequiredErrorMessage", "Enrollment status is required");
			CareerTypeAccountRequired.ErrorMessage = CodeLocalise("CareerAccountType.RequiredErrorMessage", "Account type is required");
			ChangeCampusLiteral.Text = HtmlLocalise("ChangeCampusLiteral.Label", "Change Campus Location");
			CampusRequired.Text = CodeLocalise("CampusRequired.Text", "Campus is required");

			//CellphoneNumberValidator.ErrorMessage = CodeLocalise("PhoneNumber.FormatErrorMessage", "Cellphone number is not in the correct format");
			CustomPhoneProviderValidator.ErrorMessage = CodeLocalise("PhoneProvider.RequiredErrorMessage", "Cellphone provider is required when number is entered");

			NewEmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			NewEmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address format is invalid");

			ConfirmEmailAddressRequired.ErrorMessage = CodeLocalise("ConfirmEmailAddress.RequiredErrorMessage", "Email address is required");
			ConfirmEmailAddressCompare.ErrorMessage = CodeLocalise("ConfirmEmailAddress.CompareErrorMessage", "Email addresses must match");
			PhoneNumberRegEx.ErrorMessage = CodeLocalise("PhoneNumberRegEx.ErrorMessage", "A valid phone number is required");
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			if (App.Settings.Theme == FocusThemes.Education)
			{
				if (!IsPostBack)
				{
					#region Hide Controls

					EducationPlaceHolder1.Visible = true;
					EducationPlaceHolder2.Visible = true;

					CampusSection.Visible = App.Settings.ShowCampus;

					#endregion

					#region Bind

					BindStudyProgramAreaDropDown();
					BindEnrollmentStatusDropDown(UpdateEnrollmentStatusDropDown);

					if (App.Settings.ShowCampus)
						Utilities.BindCampusDropDown(UpdateCampusDropDown);

					#endregion
				}

				txtOldProgramOfStudy.Text = GetUserProgramOfStudy();
				OldCampusTextBox.Text = GetCampus();

				txtOldEnrollmentStatus.Text = App.User.EnrollmentStatus.HasValue
					? CodeLocalise(App.User.EnrollmentStatus, true)
					: string.Empty;
			}

			if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer)) return;

			if (App.Settings.SSOManageAccountUrl.IsNullOrEmpty())
				PasswordDiv.Visible = UsernameDiv.Visible = ChangeEmailAddressPlaceholder.Visible = SecurityQuestionsPlaceHolder.Visible = false;
			else
				UsernameTextBoxesPlaceHolder.Visible = PasswordTextBoxesPlaceHolder.Visible = ChangeEmailAddressPlaceholder.Visible = SecurityQuestionsPlaceHolder.Visible = false;
		}

		/// <summary>
		/// Populates the cell-phone provider drop-down
		/// </summary>
		private void BindMobilePhoneProvider()
		{
			if (PhoneProviderDropdown.Items.Count == 0)
				PhoneProviderDropdown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.CellphoneProviders), null, CodeLocalise("CellphoneProviders.TopDefault", "- select a provider -"));

			var currentUser = UserDetails;
			var phoneNumbers = new List<PhoneNumberDto>
			{
				currentUser.PrimaryPhoneNumber,
				currentUser.AlternatePhoneNumber1,
				currentUser.AlternatePhoneNumber2
			};

			var currentMobile = phoneNumbers.FirstOrDefault(pn => pn != null && pn.PhoneType == PhoneTypes.Mobile);

			if (currentMobile != null)
			{
				PhoneNumberTextBox.Text = currentMobile.Number;
				PhoneProviderDropdown.SelectedValue = currentMobile.ProviderId.ToString();
				SetViewStateValue("MyAccount:PhoneNumber", currentMobile);
			}
			else
			{
				var phoneNumber = new PhoneNumberDto
				{
					PersonId = App.User.PersonId.GetValueOrDefault(0),
					IsPrimary = phoneNumbers.FirstOrDefault(pn => pn != null).IsNull(),
					ProviderId = null,
					PhoneType = PhoneTypes.Mobile
				};
				PhoneProviderDropdown.SelectedIndex = 0;
				SetViewStateValue("MyAccount:PhoneNumber", phoneNumber);
			}
		}

		/// <summary>
		/// Handles the Click event of the UpdatePasswordButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void UpdatePasswordButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer))
					{
						try
						{

							if (App.Settings.Theme == FocusThemes.Workforce && App.User.PersonId > 0)
							{
								ServiceClientLocator.CandidateClient(App).ResolveCandidateIssues(App.User.PersonId ?? 0, new List<CandidateIssueType>() { CandidateIssueType.InappropriateEmailAddress }, true);
							}

							ServiceClientLocator.AccountClient(App).ChangePassword(txtOldPassword.Text, txtNewPassword.Text);

							#region Save Activity

							//TODO: Martha (new activity functionality)
							//var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
							//if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
							//  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "PWC01", staffProfile: staffcontext.StaffInfo);
							//else
							//  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.JobSeeker, ActivityType.Automated, "PWC01");

							#endregion

							MasterPage.ShowModalAlert(AlertTypes.Info, "Password successfully changed.", "Password changed");
						}
						catch (Exception ex)
						{
							MasterPage.ShowError(AlertTypes.Error, "Password change failed. " + ex.Message);
						}
					}
					else
					{
						if (App.Settings.SSOManageAccountUrl.IsNotNullOrEmpty())
							Response.Redirect(App.Settings.SSOManageAccountUrl);
					}
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Handles the Click event of the UpdateUsernameButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void UpdateUsernameButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer))
				{
					if (App.User.UserId.IsNotNull())
					{
						try
						{
							ServiceClientLocator.AccountClient(App).ChangeUsername(txtOldUsername.Text, txtNewUsername.Text);
							App.User.UserName = txtNewUsername.Text;
							App.User.Save();

							#region Save Activity

							//TODO: Martha (new activity functionality)
							//var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
							//if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
							//  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "UNC01", staffProfile: staffcontext.StaffInfo);
							//else
							//  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.JobSeeker, ActivityType.Automated, "UNC01");

							#endregion

							MasterPage.ShowModalAlert(AlertTypes.Info, "Username successfully changed.", "Username changed");

							//Clear the fields
							txtNewUsername.Text = txtReNewUsername.Text = "";
						}
						catch (Exception ex)
						{
							var message = ex.GetErrorType().IsIn(ErrorTypes.UnableToChangeUserName, ErrorTypes.UserNameAlreadyExists)
								? ex.Message
								: "Username change failed.";
							MasterPage.ShowError(AlertTypes.Error, message);
						}
					}
				}
				else
				{
					if (App.Settings.SSOManageAccountUrl.IsNotNullOrEmpty())
						Response.Redirect(App.Settings.SSOManageAccountUrl);
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Handles the Click event of the UpdateSecurityQuestion control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void UpdateSecurityQuestion_Click(object sender, EventArgs e)
		{
			try
			{
                 //AIM 25 April 2017
                //Bug Fix: Update security question only when it belong to the current user
			    if (txtOldUsername.Text == App.User.UserName)
			    {
			        var question = (UpdateSecurityQuestionDropDown.SelectedValue != "" &&
			                        UpdateSecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue)
			            ? null
			            : UpdateSecurityQuestionTextBox.TextTrimmed();
			        var questionId = (UpdateSecurityQuestionDropDown.SelectedValue != "" &&
			                          UpdateSecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue)
			            ? UpdateSecurityQuestionDropDown.SelectedValueToInt()
			            : (long?) null;
			        var isSecurityQuestionAnswerChanged = ServiceClientLocator.AccountClient(App)
			            .ChangeSecurityQuestion(question, questionId, UpdateSecurityAnswerTextBox.TextTrimmed());
			        if (isSecurityQuestionAnswerChanged)
			        {
			            #region Save Activity

			            //TODO: Martha (new activity functionality)
			            //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
			            //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
			            //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "SQA01", staffProfile: staffcontext.StaffInfo);
			            //else
			            //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.JobSeeker, ActivityType.Automated, "SQA01");

			            #endregion

			            MasterPage.ShowModalAlert(AlertTypes.Info, "Security question/answer successfully changed.",
			                "Security question/answer changed");
			        }
			        else
			            MasterPage.ShowError(AlertTypes.Error, "Security question/answer change failed.");
                }
                else
                    MasterPage.ShowError(AlertTypes.Error, "Security question/answer change failed.");


			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Fires when the button is clicked to update the email address
		/// </summary>
		/// <param name="sender">Button raising the event</param>
		/// <param name="e">Standard event argument</param>
		protected void UpdateEmailAddressButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (App.User.UserId.IsNotNull())
				{
					try
					{
						var newEmailAddress = NewEmailAddressTextBox.Text.Trim();
						
						if (App.Settings.Theme == FocusThemes.Workforce && App.User.PersonId > 0)
						{
							ServiceClientLocator.CandidateClient(App).ResolveCandidateIssues(App.User.PersonId ?? 0, new List<CandidateIssueType>() { CandidateIssueType.InappropriateEmailAddress }, true);
						}

						ServiceClientLocator.AccountClient(App).ChangeEmailAddress(OldEmailAddressTextBox.Text, newEmailAddress);
						App.User.EmailAddress = newEmailAddress;
						App.User.Save();

						MasterPage.ShowModalAlert(AlertTypes.Info, "Email address successfully changed.", "Email address changed");
					}
					catch (ServiceCallException ex)
					{
						var message = "";
						if (ex.Message == string.Concat("[ErrorType.", ErrorTypes.EmailAddressAlreadyExists, "]"))
							message = HtmlLocalise(ex.Message, "A user with this email address already exists");

						MasterPage.ShowError(AlertTypes.Error, string.Concat("Email Address change failed: ", message));
					}
					finally
					{
						NewEmailAddressTextBox.Text = ConfirmEmailAddressTextBox.Text = "";
					}
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Handles the Click event of the UpdateEnrollmentStatusButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void UpdateEnrollmentStatusButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					try
					{
						if (ServiceClientLocator.AccountClient(App).ChangeEnrollmentStatus(UpdateEnrollmentStatusDropDown.SelectedValue.AsEnum<SchoolStatus>()))
						{
							MasterPage.ShowModalAlert(AlertTypes.Info, "Enrollment status successfully changed.", "Enrollment status changed");

							App.User.EnrollmentStatus = UpdateEnrollmentStatusDropDown.SelectedValue.AsEnum<SchoolStatus>();
							App.User.Save();

							ServiceClientLocator.ResumeClient(App).RegisterDefaultResume();

							UpdateEnrollmentStatusDropDown.SelectedIndex = -1;
						}
					}
					catch (Exception ex)
					{
						MasterPage.ShowError(AlertTypes.Error, "Enrollment status change failed. " + ex.Message);
					}
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Handles the Click event of the UpdateCampusButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void UpdateCampusButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					try
					{
						var campusId = UpdateCampusDropDown.SelectedValue.AsLong();
						if (ServiceClientLocator.AccountClient(App).ChangeCampus(campusId))
						{
							MasterPage.ShowModalAlert(AlertTypes.Info, "Campus location successfully changed.", "Campus location changed");

							App.User.CampusId = campusId;
							App.User.Save();

							UpdateCampusDropDown.SelectedIndex = -1;
						}
					}
					catch (Exception ex)
					{
						MasterPage.ShowError(AlertTypes.Error, "Campus location change failed. " + ex.Message);
					}
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Handles the Click event of the UpdatePhoneNumberButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void UpdatePhoneNumberButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					try
					{
						var phoneNumber = GetViewStateValue<PhoneNumberDto>("MyAccount:PhoneNumber");
						phoneNumber.Number = PhoneNumberTextBox.Text.Trim();
						phoneNumber.ProviderId = PhoneProviderDropdown.SelectedValueToNullableLong();

						if (ServiceClientLocator.AccountClient(App).ChangePhoneNumber(phoneNumber))
						{
							MasterPage.ShowModalAlert(AlertTypes.Info,
								HtmlLocalise("PhoneNumberChanged.ModalBody", "Phone number successfully changed."),
								HtmlLocalise("PhoneNumberChanged.ModalHeader", "Phone number changed."));
						}
					}
					catch (Exception ex)
					{
						MasterPage.ShowError(AlertTypes.Error, "Cellphone number change failed. " + ex.Message);
					}
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		protected void UpdateAccountTypeButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					try
					{
						var accountType = NewAccountTypeDropdownList.SelectedValueToEnum(CareerAccountType.JobSeeker);

						if (ServiceClientLocator.AccountClient(App).ChangeCareerAccountType(accountType))
						{
							MasterPage.ShowModalAlert(AlertTypes.Info,
								HtmlLocalise("AccountTypeChanged.ModalBody", "Account type successfully changed."),
								HtmlLocalise("AccountTypeChanged.ModalHeader", "Account type changed."));
						}
					}
					catch (Exception ex)
					{
						MasterPage.ShowError(AlertTypes.Error, "Account type change failed. " + ex.Message);
					}
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		#region Program Of Study

		/// <summary>
		/// Binds the study program area drop down.
		/// </summary>
		private void BindStudyProgramAreaDropDown()
		{
			if (App.Settings.ShowProgramArea)
			{
				DegreeSelectionPlaceholder.Visible = false;
				var programAreas = Utilities.GetProgramAreas(CodeLocalise("ProgramArea.Undeclared.Text", "Undeclared"));

				UpdateProgramOfStudyAreaDropDown.DataSource = programAreas;
				UpdateProgramOfStudyAreaDropDown.DataTextField = "Name";
				UpdateProgramOfStudyAreaDropDown.DataValueField = "Id";
				UpdateProgramOfStudyAreaDropDown.DataBind();

				UpdateProgramOfStudyAreaDropDown.Items.AddLocalisedTopDefault("Global.ProgramArea.TopDefault",
					"- select department -", "-1");
			}
			else
			{
				ProgramOfStudySelectionPlaceHolder.Visible = false;

				Utilities.PopulateDegreeLevelDropdown(DetailedDegreeLevelDropDownList,
					CodeLocalise("CertificateOrAssociate", "Certificate or Associate"),
					CodeLocalise("Bachelors", "Bachelors"),
					CodeLocalise("GraduateOrProfessional", "Graduate or Professional"));
			}
		}

		/// <summary>
		/// Handles the Click event of the UpdateProgramOfStudyButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void UpdateProgramOfStudyButton_Click(object sender, EventArgs e)
		{
			long? selectedProgramAreaId, selectedProgramDegreeId;
			if (App.Settings.ShowProgramArea)
			{
				selectedProgramAreaId = UpdateProgramOfStudyAreaDropDown.SelectedValue.ToLong();
				selectedProgramDegreeId = UpdateProgramOfStudyAreaDegreesDropDown.SelectedValue.ToLong();
			}
			else
			{
				selectedProgramAreaId = null;
				selectedProgramDegreeId = DetailedDegreeAjaxDropDownList.SelectedValueToNullableLong();
			}

			if (selectedProgramDegreeId.HasValue)
			{
				try
				{
					if (App.User.IsAuthenticated)
					{
						try
						{
							if (ServiceClientLocator.AccountClient(App).ChangeProgramOfStudy(selectedProgramAreaId, selectedProgramDegreeId))
							{
								MasterPage.ShowModalAlert(AlertTypes.Info, "Program of study successfully changed.", "Program of study changed");

								App.User.ProgramAreaId = selectedProgramAreaId;
								App.User.DegreeEducationLevelId = selectedProgramDegreeId;

								App.User.Save();

								ServiceClientLocator.ResumeClient(App).RegisterDefaultResume();

								// Clear the selected values in case the user re-opens the modal
								if (App.Settings.ShowProgramArea)
								{
									UpdateProgramOfStudyAreaDropDown.SelectedIndex = -1;
									UpdateProgramOfStudyAreaDegreesDropDown.SelectedIndex = -1;
								}
								else
								{
									DetailedDegreeAjaxDropDownList.SelectedIndex = -1;
									DetailedDegreeLevelDropDownList.SelectedIndex = 0;
								}

								// Bubble changes down to Master page so it can be bubbled up to what ever page requires it
								OnMyAccountUpdated(new CommandEventArgs("ProgramOfStudy", selectedProgramDegreeId));

								//Response.Redirect(Request.RawUrl);
							}
						}
						catch (Exception ex)
						{
							MasterPage.ShowError(AlertTypes.Error, "Program of study change failed. " + ex.Message);
						}
					}
				}
				catch (ApplicationException ex)
				{
					MasterPage.ShowError(AlertTypes.Error, ex.Message);
				}
			}
		}

		#endregion

		#region Helper methods

		/// <summary>
		/// Gets the user program of study.
		/// </summary>
		/// <returns></returns>
		private string GetUserProgramOfStudy()
		{
			var programArea = String.Empty;
			var programAreaDegree = String.Empty;

			if (App.User.ProgramAreaId != null && App.User.ProgramAreaId > 0)
				programArea = ServiceClientLocator.ExplorerClient(App).GetProgramArea((long)App.User.ProgramAreaId).Name;
			else if (App.User.ProgramAreaId != null && App.User.ProgramAreaId == 0)
				programArea = CodeLocalise("ProgramArea.Undeclared.Text", "Undeclared");

			if (App.User.DegreeEducationLevelId != null && App.User.DegreeEducationLevelId > 0)
				programAreaDegree = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevel((long)App.User.DegreeEducationLevelId).DegreeName;
			else if (App.User.DegreeEducationLevelId != null && App.User.DegreeEducationLevelId == 0)
				programAreaDegree = CodeLocalise("Degree.Undeclared.Text", "Undeclared");
			else if (App.User.DegreeEducationLevelId != null && App.User.DegreeEducationLevelId == -App.User.ProgramAreaId)
				programAreaDegree = CodeLocalise("Global.Degree.AllDegrees", "All Degrees");

			if (App.Settings.ShowProgramArea)
				return programArea.IsNullOrEmpty() || programAreaDegree.IsNullOrEmpty() ? String.Empty : programArea + "/" + programAreaDegree;

			return programAreaDegree.IsNullOrEmpty() ? String.Empty : programAreaDegree;
		}

		/// <summary>
		/// Gets the name of the user's campus.
		/// </summary>
		/// <returns>The name of the campus</returns>
		private string GetCampus()
		{
			return App.User.CampusId.HasValue
				? ServiceClientLocator.CandidateClient(App).GetCampus(App.User.CampusId.Value).Name
				: string.Empty;
		}

		#endregion

		#region Events

		public delegate void MyAccountUpdatedHandler(object sender, CommandEventArgs eventArgs);

		public event MyAccountUpdatedHandler MyAccountUpdated;

		protected virtual void OnMyAccountUpdated(CommandEventArgs eventargs)
		{
			MyAccountUpdatedHandler handler = MyAccountUpdated;
			if (handler != null) handler(this, eventargs);
		}

		#endregion

	}
}
