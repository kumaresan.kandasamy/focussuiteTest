﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangeEmailAddress.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.ChangeEmailAddress" %>

<%@ Register TagPrefix="uc" TagName="ConfirmationModal" Src="~/Controls/ConfirmationModal.ascx" %>

<hr/>
<table width="100%">
	<tr>
		<td><b><%= HtmlLocalise("Title", "Change screen name and email address") %></b></td>
		<td style="text-align: right">
			<a id="HideChangeEmailAddressContent" href="#" onclick="javascript:closeChangeEmailAddress();"><%= HtmlLocalise("Hide.Text", "Hide")%></a>
			<a id="ShowChangeEmailAddressContent" href="#" onclick="javascript:expandChangeEmailAddress();" style="display: none;"><%= HtmlLocalise("Show.Text", "Show")%></a>
		</td>
	</tr>
</table>
<p></p>
<div id="ChangeEmailAddressContent" class="myAccountContent">
	<table width="500px">
		<tr>
			<td width="250px"><%= HtmlRequiredLabel(ScreenNameTextbox, "ScreenName", "Screen name")%></td>
			<td>
				<asp:Textbox ID="ScreenNameTextbox" runat="server" ClientIDMode="Static" TabIndex="1" width="98%" MaxLength="20" AutoCompleteType="Disabled" />
				<asp:RequiredFieldValidator ID="ScreenNameRequired" runat="server" ControlToValidate="ScreenNameTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeEmailAddress" />
			</td>
		</tr>
		<tr>
			<td><%= HtmlRequiredLabel(EmailAddressTextbox, "EmailAddress", "Email address")%></td>
			<td>
				<asp:Textbox ID="EmailAddressTextbox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="2" MaxLength="100" AutoCompleteType="Disabled" />
				<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeEmailAddress" />
			</td>
		</tr>
		<tr>
			<td><%= HtmlRequiredLabel(NewEmailAddressTextbox, "NewEmailAddress", "New email address")%></td>
			<td>
				<asp:Textbox ID="NewEmailAddressTextbox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="3" MaxLength="100" AutoCompleteType="Disabled" />
				<asp:RequiredFieldValidator ID="NewEmailAddressRequired" runat="server" ControlToValidate="NewEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="ChangeEmailAddress" />
				<asp:RegularExpressionValidator ID="NewEmailAddressRegEx" runat="server" ControlToValidate="NewEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" 
																				ValidationGroup="ChangeEmailAddress" />
			</td>
		</tr>
		<tr>
			<td><%= HtmlRequiredLabel(ConfirmEmailAddressTextbox, "EmailAddressConfirmation", "Re-enter email address")%></td>
			<td>
				<asp:Textbox ID="ConfirmEmailAddressTextbox" runat="server" ClientIDMode="Static" Width="98%" TabIndex="4" MaxLength="100" AutoCompleteType="Disabled" />
				<asp:RequiredFieldValidator ID="ConfirmEmailAddressRequired" runat="server" ControlToValidate="ConfirmEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" CssClass="error" 
																		ValidationGroup="ChangeEmailAddress" />
				<asp:CompareValidator ID="ConfirmEmailAddressCompare" runat="server" ControlToValidate="ConfirmEmailAddressTextbox" ControlToCompare="NewEmailAddressTextbox" SetFocusOnError="true" Display="Dynamic" 
															CssClass="error" ValidationGroup="ChangeEmailAddress"/>
				<asp:CustomValidator ID="ChangeEmailValidator" runat="server" CssClass="error" Display="Dynamic" />
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: right"><asp:Button ID="SaveButton" runat="server" ClientIDMode="Static" CssClass="buttonLevel2" ValidationGroup="ChangeEmailAddress" TabIndex="5" OnClick="SaveButton_Clicked" /></td>
		</tr>
	</table>		
</div>

<asp:HiddenField ID="ChangeEmailAddressExpanded" runat="server" value="1" ClientIDMode="Static"/>

<%-- Confirmation Modal --%>
<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />

<script type="text/javascript">
	$(document).ready(function () {
		if ($('#ChangeEmailAddressExpanded').val() == "0") {
			closeChangeEmailAddress();
		}
		else {
			expandChangeEmailAddress();
		}
	});

	function closeChangeEmailAddress() {
		$('#ChangeEmailAddressContent').hide();
		$('#HideChangeEmailAddressContent').hide();
		$('#ShowChangeEmailAddressContent').show();
		$('#ChangeEmailAddressExpanded').val("0");
	}

	function expandChangeEmailAddress() {
		$('#ChangeEmailAddressContent').show();
		$('#HideChangeEmailAddressContent').show();
		$('#ShowChangeEmailAddressContent').hide();
		$('#ChangeEmailAddressExpanded').val("1");
	}
</script>


