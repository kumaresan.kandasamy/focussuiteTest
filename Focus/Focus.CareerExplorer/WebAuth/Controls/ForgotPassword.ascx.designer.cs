﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.CareerExplorer.WebAuth.Controls {
    
    
    public partial class ForgotPassword {
        
        /// <summary>
        /// ForgotPasswordModalDummyTarget control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ForgotPasswordModalDummyTarget;
        
        /// <summary>
        /// ForgotPasswordModal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender ForgotPasswordModal;
        
        /// <summary>
        /// ForgotPasswordPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ForgotPasswordPanel;
        
        /// <summary>
        /// ForgotPasswordTitleLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ForgotPasswordTitleLabel;
        
        /// <summary>
        /// PasswordHelpInfoLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel PasswordHelpInfoLabel;
        
        /// <summary>
        /// EmailAddressLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel EmailAddressLabel;
        
        /// <summary>
        /// ForgotPasswordEmailAddressTextbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ForgotPasswordEmailAddressTextbox;
        
        /// <summary>
        /// EmailAddressRequired control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator EmailAddressRequired;
        
        /// <summary>
        /// EmailAddressRegEx control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator EmailAddressRegEx;
        
        /// <summary>
        /// EmailAddressConfirmationLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel EmailAddressConfirmationLabel;
        
        /// <summary>
        /// ForgotPasswordConfirmEmailAddressTextbox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ForgotPasswordConfirmEmailAddressTextbox;
        
        /// <summary>
        /// ConfirmEmailAddressRequired control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator ConfirmEmailAddressRequired;
        
        /// <summary>
        /// ConfirmEmailAddressCompare control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CompareValidator ConfirmEmailAddressCompare;
        
        /// <summary>
        /// lblResetPassword control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblResetPassword;
        
        /// <summary>
        /// btnRequestPassword control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnRequestPassword;
        
        /// <summary>
        /// ForgotPasswordModal2DummyTarget control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ForgotPasswordModal2DummyTarget;
        
        /// <summary>
        /// ForgotPasswordModal2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender ForgotPasswordModal2;
        
        /// <summary>
        /// ForgotPasswordPanel2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ForgotPasswordPanel2;
        
        /// <summary>
        /// ForgotPasswordHeader2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ForgotPasswordHeader2;
        
        /// <summary>
        /// ForgotPasswordTitleLabel2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ForgotPasswordTitleLabel2;
        
        /// <summary>
        /// ErrorDescription2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ErrorDescription2;
        
        /// <summary>
        /// ForgotPasswordUserNameTextBox2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ForgotPasswordUserNameTextBox2;
        
        /// <summary>
        /// ForgotPasswordEmailAddressTextBox2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox ForgotPasswordEmailAddressTextBox2;
        
        /// <summary>
        /// EitherFieldValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator EitherFieldValidator;
        
        /// <summary>
        /// NotBothFieldValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator NotBothFieldValidator;
        
        /// <summary>
        /// EmailAddressRegEx2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator EmailAddressRegEx2;
        
        /// <summary>
        /// lblResetPassword2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblResetPassword2;
        
        /// <summary>
        /// ForgotPasswordValidationSummary2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.AjaxValidationSummary ForgotPasswordValidationSummary2;
        
        /// <summary>
        /// SubmitButton2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SubmitButton2;
        
        /// <summary>
        /// ForgotPasswordModal3DummyTarget control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ForgotPasswordModal3DummyTarget;
        
        /// <summary>
        /// ForgotPasswordModal3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender ForgotPasswordModal3;
        
        /// <summary>
        /// ForgotPasswordPanel3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ForgotPasswordPanel3;
        
        /// <summary>
        /// ForgotPasswordTitleLabel3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ForgotPasswordTitleLabel3;
        
        /// <summary>
        /// PasswordHelpInfoLabel3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel PasswordHelpInfoLabel3;
        
        /// <summary>
        /// ExtraQuestionsPlaceHolder3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder ExtraQuestionsPlaceHolder3;
        
        /// <summary>
        /// FPSecurityQuestionDropDownLocalisedLabel3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel FPSecurityQuestionDropDownLocalisedLabel3;
        
        /// <summary>
        /// FPSecurityQuestionDropDown3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList FPSecurityQuestionDropDown3;
        
        /// <summary>
        /// SecurityQuestionRequiredFieldValidator3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator SecurityQuestionRequiredFieldValidator3;
        
        /// <summary>
        /// FPSecurityQuestionTextBox3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox FPSecurityQuestionTextBox3;
        
        /// <summary>
        /// SecurityQuestionTextBoxRequiredFieldValidator3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator SecurityQuestionTextBoxRequiredFieldValidator3;
        
        /// <summary>
        /// SecurityAnswerTextBoxLocalisedLabel3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel SecurityAnswerTextBoxLocalisedLabel3;
        
        /// <summary>
        /// SecurityAnswerTextBox3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SecurityAnswerTextBox3;
        
        /// <summary>
        /// SecurityAnswerTextBoxRequiredFieldValidator3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator SecurityAnswerTextBoxRequiredFieldValidator3;
        
        /// <summary>
        /// DayOfBirthLocalisedLabel3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel DayOfBirthLocalisedLabel3;
        
        /// <summary>
        /// DayOfBirthTextBox3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox DayOfBirthTextBox3;
        
        /// <summary>
        /// DayOfBirthTextBoxRequiredFieldValidator3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator DayOfBirthTextBoxRequiredFieldValidator3;
        
        /// <summary>
        /// DayOfBirthTextBoxRegularExpressionValidator3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RegularExpressionValidator DayOfBirthTextBoxRegularExpressionValidator3;
        
        /// <summary>
        /// MonthOfBirthLocalisedLabel3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel MonthOfBirthLocalisedLabel3;
        
        /// <summary>
        /// MonthOfBirthDropDownList3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList MonthOfBirthDropDownList3;
        
        /// <summary>
        /// MonthOfBirthRequiredFieldValidator3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator MonthOfBirthRequiredFieldValidator3;
        
        /// <summary>
        /// lblResetPassword3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblResetPassword3;
        
        /// <summary>
        /// btnRequestPassword3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnRequestPassword3;
    }
}
