﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountReactivationModal.ascx.cs" Inherits="Focus.CareerExplorer.WebAuth.Controls.AccountReactivationModal" %>

<%@ Register src="~/Controls/ConfirmationModal.ascx" tagname="ConfirmationModal" tagprefix="uc" %>

<asp:HiddenField ID="AccountReactivationModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="AccountReactivationModalPopup" runat="server" 
												TargetControlID="AccountReactivationModalDummyTarget"
												PopupControlID="AccountReactivationModalPanel"
												RepositionMode="RepositionOnWindowResizeAndScroll"
												BackgroundCssClass="modalBackground" BehaviorID="AccountReactivationModalBehavior">
	<Animations>
		<OnShown>
			<ScriptAction Script="ApplyCustomSelectStyling($('.reactivationModal select'), 'refresh')"/>
		</OnShown>
	</Animations>
</act:ModalPopupExtender>

<asp:Panel ID="AccountReactivationModalPanel" TabIndex="-1" runat="server" CssClass="modal reactivationModal" Style="display:none;width:700px">
	<h1><%= HtmlLocalise("AccountReactivationQuestionnaire.Label", "Account Reactivation Questionnaire")%></h1>
    <p><%= HtmlLocalise("UpdateStatus.Label", "Welcome back! Before we reactivate your account, please help us by answering a few questions about your current status.")%></p>
    <table class="FocusCareer">
      <tr>
        <td>
            <div class="col-sm-6"><%= App.Settings.HideEnrollmentStatus? "" : HtmlRequiredLabel(EnrollmentStatusDropdown, "EnrollmentStatus", "Enrollment Status")%></div>
	        <div class="col-sm-6"><asp:DropDownList ID="EnrollmentStatusDropdown" runat="server" /></div>
        </td>
      </tr>
      <tr>
        <td>
	        <asp:RequiredFieldValidator ID="EnrollmentStatusRequiredValidator" runat="server" ControlToValidate="EnrollmentStatusDropdown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="AccountReactivationGroup" />
        </td>
      </tr>
      <tr>
	      <td>
            <div class="col-sm-6"><%= HtmlRequiredLabel(EducationStatusDropdown, "EducationLevel", "Education Level")%></div>
	        <div class="col-sm-6"><asp:DropDownList ID="EducationStatusDropdown" runat="server" /></div>
        </td>
      </tr>
      <tr>
        <td>
	        <div class="col-sm-6"><asp:RequiredFieldValidator ID="EducationStatusRequiredValidator" runat="server" ControlToValidate="EducationStatusDropdown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="AccountReactivationGroup" /></div>
            <div class="col-sm-6"><asp:CustomValidator ID="custEnrolmentEducationLevel" runat="server" ControlToValidate="EducationStatusDropdown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ClientValidationFunction="ValidateEnrolmentEducationLevel" ValidateEmptyText="true"  ValidationGroup="AccountReactivationGroup"/></div>
        </td>
        </tr>
        <tr>
	        <td>
            <div class="col-sm-6"><%= HtmlRequiredLabel(EmploymentStatusDropdown, "EmploymentStatus", "Employment Status")%></div>
            <div class="col-sm-6"><asp:DropDownList ID="EmploymentStatusDropdown" runat="server" /></div>
            </td>
      </tr>
			<tr>
        <td>
	        <div class="col-sm-6"><asp:RequiredFieldValidator ID="EmploymentStatusRequiredValidator" runat="server" ControlToValidate="EmploymentStatusDropdown" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="AccountReactivationGroup" /></div>
        </td>
      </tr>
      <tr>
				<td colspan="2" style="text-align:justify">
					<focus:LocalisedLabel runat="server" ID="SearchableLabel" DefaultText="We encourage all returning users to immediately update their contact information and resumes with the most recent information available. #BUSINESSES# want to see your latest information! Currently, your resume is NOT searchable to #BUSINESSES#:LOWER. If you would like to activate this feature now, select “Yes” below. If you prefer to update your resume before making it searchable, you must activate this selection on your Preferences tab after updating your resumes."/>
				</td>
      </tr>
			<tr>
				<td>
					<div class="col-sm-6"><strong><%= HtmlRequiredLabel(ResumeSearchableRadioList, "ResumeSearchable", "Make your resume searchable to interested #BUSINESSES#:LOWER NOW?")%></strong></div>				
					<div class="col-sm-6">
                    <asp:RadioButtonList ID="ResumeSearchableRadioList" runat="server" CssClass="dataInputHorizontalRadio" RepeatLayout="Flow"	RepeatDirection="Horizontal" ClientIDMode="Static">
						<asp:ListItem Text="Yes" Value="yes" />
						<asp:ListItem Text="No" Value="no" />
					</asp:RadioButtonList>
                    </div>
				</td>
			</tr>
			<tr>
        <td>
          <asp:RequiredFieldValidator ID="ResumeSearchableRequired" runat="server" ControlToValidate="ResumeSearchableRadioList" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AccountReactivationGroup" />
        </td>
        </tr>
				<tr>
                <td></td>
				<td align="left">
					<div class="col-sm-6"><asp:Button ID="SubmitButton" runat="server" OnClick="Submit_Click" CssClass="buttonLevel2" ValidationGroup="AccountReactivationGroup" OnClientClick="show();" /></div>
				</td>
			    </tr>
    </table>
    
</asp:Panel>

<uc:ConfirmationModal ID="Confirmation" runat="server" OnClientClick="return false;" Width="300px" />

<script language="javascript" type="text/javascript">


    function show() {
        var validate = Page_ClientValidate("AccountReactivationGroup");
        if (validate) { showProcessing(); }
    }


    function ValidateEnrolmentEducationLevel(src, args) {
        var enrollmentStatus = $('#<%= EnrollmentStatusDropdown.ClientID %>').val();
        var educationLevel = $('#<%= EducationStatusDropdown.ClientID %>').val();
        educationLevel = educationLevel.substr(educationLevel.length - 2);

        if ((enrollmentStatus == '<%= SchoolStatus.In_School_Post_HS %>' || enrollmentStatus == '<%= SchoolStatus.Not_Attending_School_HS_Graduate %>') && educationLevel <= 12) {
            src.innerHTML = "<%= EnrollmentStatusErrorMessage %>";
            args.IsValid = false;
        }
        else if ((enrollmentStatus == '<%= SchoolStatus.In_School_HS_OR_less %>' || enrollmentStatus == '<%= SchoolStatus.Not_Attending_School_OR_HS_Dropout %>' || enrollmentStatus == '<%= SchoolStatus.In_School_Alternative_School %>') && educationLevel > 12) {
            src.innerHTML = "<%= EducationLevelErrorMessage %>";
            args.IsValid = false;
        }
    }
</script>