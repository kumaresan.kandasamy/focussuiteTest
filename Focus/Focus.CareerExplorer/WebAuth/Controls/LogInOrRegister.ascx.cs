﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common.Authentication;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Web.Core.Models;

using Framework.Core;
using BgtSSO;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

#endregion

namespace Focus.CareerExplorer.WebAuth.Controls
{
	public partial class LogInOrRegister : UserControlBase
	{
		#region Properties

	  private bool _dropdownsBound;

		protected const string CreateOwnSecurityQuestionValue = "Own";
		protected string DateErrorMessage = "";
		protected string DOBMinLimitErrorMessage = "";
		protected string DOBMaxLimitErrorMessage = "";
		protected string DateOfBirthRequiredMessage = "";
		protected string SSNRequiredMessage = "";
		protected string SSNErrorMessage = "";
		protected string LaborRegulationsUrl = "";
		protected string UserName = "";
		protected long? ProgrammeAreaId = null;
		protected long? DegreeId = null;
		protected SchoolStatus? EnrollmentStatus = null;

		protected bool IsUsernameChanged = false;
		protected bool IsPasswordChanged = false;
		protected bool IsSecurityQuestionAnswerChanged = false;
		protected bool IsMigratedStatusChanged = false;
		protected bool IsConsentStatusChanged = false;

		protected bool EnableOAuthFeatures = false;
		protected bool EnableSamlFeatures = false;

		public event LoggedInEventHandler LoggedIn;
		public event LogInCancelledEventHandler LogInCancelled;

		public delegate bool LoggedInEventHandler(object o, LoggedInEventArgs e);
		public delegate void LogInCancelledEventHandler(object o, CommandEventArgs e);

		public bool EnableCareerPasswordSecurity;

        public string success { get; set; }
        public string Privatekey { get; set; }
        public string Publickey { get; set; }
		private UserConversionDetail UpdateAccountInfo
		{
			get { return GetViewStateValue("Registration:UserConversionDetail", new UserConversionDetail()); }
			set { SetViewStateValue("Registration:UserConversionDetail", value); }
		}

		private string SourceCommandName
		{
			get { return GetViewStateValue("LogIn:SourceCommandName", string.Empty); }
			set { SetViewStateValue("LogIn:SourceCommandName", value); }
		}

		private string Pin
		{
			get { return GetViewStateValue("PinRegistration:Pin", string.Empty); }
			set { SetViewStateValue("PinRegistration:Pin", value); }
		}

		private string EmailAddress
		{
			get { return GetViewStateValue("PinRegistration:EmailAddress", string.Empty); }
			set { SetViewStateValue("PinRegistration:EmailAddress", value); }
		}

		#endregion

		/// <summary>
		/// Gets or sets the model.
		/// </summary>
		/// <value>
		/// The model.
		/// </value>
		private RegistrationModel Model
		{
			get
			{
				if (App.Settings.Module == FocusModules.Explorer)
					return GetViewStateValue("LogIn:RegistrationModel", new ExplorerRegistrationModel());

				return GetViewStateValue("LogIn:RegistrationModel", new CareerRegistrationModel());
			}
			set { SetViewStateValue("LogIn:RegistrationModel", value); }
		}

		/// <summary>
		/// Any saved external Id
		/// </summary>
		protected string ExternalId
		{
			get { return GetViewStateValue<string>("LoginOrRegister:ExternalId"); }
			set { SetViewStateValue("LoginOrRegister:ExternalId", value); }
		}

		protected SSOProfile Profile
		{
			get { return GetViewStateValue<SSOProfile>("LoginOrRegister:Profile"); }
			set { SetViewStateValue("LoginOrRegister:Profile", value); }
		}

		internal bool LoginEnabled { get; set; }

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Profile = App.GetSessionValue<SSOProfile>(Saml.SSOProfileKey);
				App.RemoveSessionValue(Saml.SSOProfileKey);
			}

		    UsernameInUseErrorMessage.Text = SocialSecurityNumberValidator.ErrorMessage = "";
			//meAdditionalInformationPhoneNumber.Mask = App.Settings.PhoneNumberMaskPattern;
			if (!LoginEnabled) return;

			if (Profile.IsNull())
			{
				if (!App.User.IsAuthenticated && App.Settings.OAuthEnabled)
					Response.Redirect(UrlBuilder.OAuth());
				else if (!App.User.IsAuthenticated && (App.Settings.SamlEnabled || App.Settings.SamlEnabledForCareer))
					Response.Redirect(UrlBuilder.Saml());
			}
			else
				ShowSSORegistration();

			UserNameTextBox.MaxLength = App.Settings.MaximumUserNameLength;
			EmailAddressTextbox.MaxLength = ConfirmRegEmailAddressTextbox.MaxLength = Math.Min(App.Settings.MaximumEmailLength, App.Settings.MaximumUserNameLength);
			EmailAddressRegEx.ValidationExpression = UCEmailAddressRegEx.ValidationExpression = App.Settings.EmailAddressRegExPattern;
			AdditionalInformationZipRegexValidator.ValidationExpression = App.Settings.ExtendedPostalCodeRegExPattern;
      
      if (App.Settings.Module == FocusModules.Explorer)
			{
				NewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
			}
			else
			{
				UCNewPasswordRegEx.ValidationExpression = NewPasswordRegEx.ValidationExpression = App.Settings.PasswordRegExPattern;
			}

		  SeekerAccountTypePlaceholder.Visible = SeekerAccountTypeRequired.Enabled = App.Settings.DisplayCareerAccountType;

            //// Need to do this during the load because recaptcha loses this when set in code behind
            if (App.Settings.RecaptchaEnabled)
            {
                Publickey = App.Settings.RecaptchaPublicKey;
                Privatekey = App.Settings.RecaptchaPrivateKey;
                ShowRecaptcha(false);
            }

			// Set Login and Register as Not so when enabled the panel is hidden.
			RegisterSettingPanel.Visible = !App.Settings.HideCareerRegister;
			CareerLoginPanel.Visible = !App.Settings.HideCareerSignIn;

			if (!IsPostBack)
			{
				ShowFocusThemeLogInPanel();
				BindDropdowns();
				BindSecurityQuestionDropDown();
				SetApplicationSpecificRowVisibility();
				if (App.Settings.Module == FocusModules.Explorer)
					Model = new ExplorerRegistrationModel();
				else
					Model = new CareerRegistrationModel();
			}

			LocaliseUI();

			if (App.Settings.LaborRegulationsUrl.IsNullOrEmpty())
			{
				LaborRegulationsLink.Visible = true;
				LaborRegulationsLink.Text = LaborRegulationsUrl = App.Settings.LaborRegulationsUrl;
			}
			else
				LaborRegulationsLink.Visible = false;

			if (!IsPostBack && App.Settings.Module == FocusModules.CareerExplorer)
			{
				// To redirect staff user to view jobseeker's detail
				//TODO: Martha (New Functionality)- Check below
				//if (Request.Params.Get("s") != null)
				//  ValidateStaffUser(Request.Params.Get("s"));
				//else if (CareerCommon.CareerApp.Settings.UIClaimantEnabled && Request.Params.Get("UIClaimant") != null && Request.Params.Get("code") != null)
				//  ValidateUIClaimantUser(Request.Params.Get("UIClaimant"), Request.Params.Get("code"), (Request.Params.Get("UIReg") != null && Request.Params.Get("UIReg") == "1") ? true : false);
			}
		}

		#region Show the log in panel according to Theme

		/// <summary>
		/// Shows the log in panel according to Focus Theme.
		/// </summary>
		private void ShowFocusThemeLogInPanel()
		{
			// switch T&Cs
			TermsConditionsEducation.Visible = true;

			LogInWorkforcePanel.Visible = true;
			if (App.Settings.Module == FocusModules.Explorer)
			{
				WorkforcePlaceHolder1.Visible = false;
				AdditionalInformationNameRow.Visible =
				AdditionalInformationDateOfBirthRow.Visible = OptionalAdditionalInformationRow.Visible = false;
			}
		}

		#endregion

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init" /> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs" /> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			ConfirmationModal.OkCommand += ConfirmationModal_OkCommand;
			ConfirmationModal.CloseCommand += ConfirmationModal_CloseCommand;

			EnableCareerPasswordSecurity = App.Settings.EnableCareerPasswordSecurity;
		}

		/// <summary>
		/// Shows the log in.
		/// </summary>
		/// <param name="sourceCommandName">Name of the source command.</param>
		public void ShowLogIn(string sourceCommandName)
		{
			SourceCommandName = sourceCommandName;
			LogInModal.Show();

		}

		/// <summary>
		/// Shows the log in.
		/// </summary>
		internal void ShowLogIn(bool showTimeoutModal = false, string userName = null, bool showRedirectModal = false)
		{
			if (showTimeoutModal)
			{
				var title = CodeLocalise("TimeoutModal.Title", "Session Timeout");
				var body = CodeLocalise("TimeoutModal.Body", "Your session has timed out.<br />Please sign in again to continue");

				ConfirmationModal.Show(title, body, "sign in", "CANCEL", height: 80, width: 250);
			}
			else
			{
				if (showRedirectModal)
				{
					var title = CodeLocalise("RedirectModal.Title", "Not Authenticated");
					var body = CodeLocalise("RedirectModal.Body", "Please log in to access the requested page");
					ConfirmationModal.Show(title, body, "Log in", null, height: 80, width: 250);
				}
				if (userName.IsNotNullOrEmpty())
				{
					UserNameTextBox.Text = userName;
				}
				LogInModal.Show();
			}
		}

		/// <summary>
		/// Shows the SSO registration.
		/// </summary>
		public void ShowSSORegistration()
		{
			// Convert the OAuth user to a Career user
			if (!IsPostBack && (App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer) && !App.User.IsMigrated && App.Settings.SSOForceNewUserToRegistration)
			{
				AdditionalInformationFirstNameTextBox.Text = Profile.FirstName;
				AdditionalInformationFirstNameTextBox.Enabled = false;
				AdditionalInformationLastNameTextBox.Text = Profile.LastName;
				AdditionalInformationLastNameTextBox.Enabled = false;
				ScreenNameTextbox.Text = Profile.ScreenName;
				ScreenNameTextbox.Enabled = false;
				ExternalId = Profile.Id;

				if (App.Settings.Theme == FocusThemes.Education)
					ShowAgreeToTerms();
				else
					ConvertUserToCareerUser();
			}
		}

		/// <summary>
		/// Handles the Click event of the LogInButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void LogInButton_Click(object sender, EventArgs e)
		{
			string password;

			UserName = (UserNameTextBox.Text ?? string.Empty).Trim();
			password = (PasswordTextBox.Text ?? string.Empty).Trim();

			try
			{
				LogIn(password);
			}
			catch (Exception ex)
			{
				var logErrror = true;

				// Reset the forms
				ResetForms();

				switch (ex.Message.ToLowerInvariant())
				{
					case "user blocked":
						ConfirmationModal.Show(
							CodeLocalise("AccountBlocked.Title", "Account blocked"),
							CodeLocalise("Global.BlockedMessage", "Your account has been blocked. Please contact Support at #SUPPORTPHONE# or <a href='mailto:#SUPPORTEMAIL#'>#SUPPORTEMAIL#</a> for further assistance."),
							"OK", "UserBlocked", "", "", "", "", 150, 300);

						logErrror = false;
						break;
				}

				if (logErrror)
				{
					// Log the error
					LoginValidator.IsValid = false;
					LoginValidator.ErrorMessage = ex.Message;

					LogInModal.Show();
				}
			}
		}

		/// <summary>
		/// Handles the OnCloseCommand event of the ConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void ConfirmationModal_OnCloseCommand(object sender, CommandEventArgs eventargs)
		{
			LogInModal.Show();
		}

		/// <summary>
		/// Handles the Click event of the RegisterButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void RegisterButton_Click(object sender, EventArgs e)
		{
		  // Ensure the user hasn't loggeed in using a new window or tab
		  if (App.User.IsAuthenticated)
		  {
        var logout = (LogOut)MasterPage.FindControl("MainLogOut");
			  logout.LogOutSession(false);
		    RegisterModal.Show();

		    MasterPage.ShowModalAlert(AlertTypes.Info, CodeLocalise("RegistrationInProgress.Message",
		      "Another account has been detected and has been logged out automatically in order to continue."),
		      CodeLocalise("RegistrationInProgress.Title", "Registration in progress"), null);
		  }
          //if (Validate())
          //{
          //    lblmsg.Text = "Valid Recaptcha";
          //    lblmsg.ForeColor = System.Drawing.Color.Green;
          //}

          //else
          //{
          //    lblmsg.Text = "Not Valid Recaptcha";
          //    lblmsg.ForeColor = System.Drawing.Color.Red;
          //}

          if (App.Settings.RecaptchaEnabled && !Validate())
			{
				SocialSecurityNumberPart1TextBox.Attributes["value"] = SocialSecurityNumberPart1TextBox.Text;
				SocialSecurityNumberPart2TextBox.Attributes["value"] = SocialSecurityNumberPart2TextBox.Text;
				SocialSecurityNumberPart3TextBox.Attributes["value"] = SocialSecurityNumberPart3TextBox.Text;

				ConfirmSocialSecurityNumberPart1TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart1TextBox.Text;
				ConfirmSocialSecurityNumberPart2TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart2TextBox.Text;
				ConfirmSocialSecurityNumberPart3TextBox.Attributes["value"] = ConfirmSocialSecurityNumberPart3TextBox.Text;

				RegisterModal.Show();
				RecaptchaErrorMessage.Visible = true;
				RegisterStep1Panel.Visible =
					panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
				Page.ClientScript.RegisterStartupScript(GetType(), "AdditionalFields", "isAdditionalInformationRequired();",
					true);
			}
			else
			{
				RecaptchaErrorMessage.Visible = false;
				if (App.Settings.Theme == FocusThemes.Education)
				{
					if (App.Settings.Module == FocusModules.Explorer)
						Model = GetExplorerRegistrationModel();
					else
						Model = GetCareerRegistrationModel();

					CompleteStep2();
				}
				else
				{
					if (ExternalId.IsNullOrEmpty() &&
						 this.SocialSecurityNumberPart1TextBox.Text.IsNotNullOrEmpty() &&
						 this.SocialSecurityNumberPart2TextBox.Text.IsNotNullOrEmpty() &&
						 this.SocialSecurityNumberPart3TextBox.Text.IsNotNullOrEmpty())
					{
						// check whether the SSN (if entered) is already in use
						string fullSocialSecurityNumber = String.Concat(this.SocialSecurityNumberPart1TextBox.Text,
							this.SocialSecurityNumberPart2TextBox.Text, this.SocialSecurityNumberPart3TextBox.Text);
						if (ServiceClientLocator.AccountClient(this.App).CheckSocialSecurityNumberInUse(fullSocialSecurityNumber))
						{
							RegisterModal.Show();
							SocialSecurityNumberValidator.Visible = true;
							RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
							RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
							SocialSecurityNumberValidator.IsValid = false;
							SocialSecurityNumberValidator.ErrorMessage = CodeLocalise("SocialSecurityNumberValidator.AlreadyInUse", "Social security number is already in use");
						}
						else
						{
							CompleteStep1();
						}
					}
					else
					{
						CompleteStep1();
					}
				}
			}
		}

		private void CompleteStep1()
		{
			// Store the password as it will be wiped soon as it's a password field!
			if (App.Settings.Module == FocusModules.Explorer)
				RegistrationStep1Explorer();
			else
				RegistrationStep1Career();

			RegisterModal.Show();
		}

		private bool CompleteStep2()
		{
			try
			{
				if (App.Settings.Module == FocusModules.Explorer)
					return RegisterExplorer();
				else
					return RegisterCareer();
			}
			catch (ApplicationException ex)
			{
				RaiseRegistrationError(ex.Message);
				RegisterStep1Panel.Visible =
					panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
				RegisterModal.Show();
				return false;
			}
		}

        public bool Validate()
        {
            string Response = Request["g-recaptcha-response"];//Getting Response String Append to Post Method
            bool Valid = false;
            //Request to Google Server
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create
            (" https://www.google.com/recaptcha/api/siteverify?secret= "+ Privatekey +"&response=" + Response);
            //"https://www.google.com/recaptcha/api/siteverify?secret=" + ReCaptcha_Secret + "&response=" + response;
            try
            {
                //Google recaptcha Response
                using (WebResponse wResponse = req.GetResponse())
                {

                    using (StreamReader readStream = new StreamReader(wResponse.GetResponseStream()))
                    {
                        string jsonResponse = readStream.ReadToEnd();

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        LogInOrRegister data = js.Deserialize<LogInOrRegister>(jsonResponse);// Deserialize Json

                        Valid = Convert.ToBoolean(data.success);
                    }
                }

                return Valid;
            }
            catch (WebException ex)
            {
                throw ex;
            }
        }


		/// <summary>
		/// Handles the Click event of the btnUpdateMyAccount control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void btnUpdateMyAccount_Click(object sender, EventArgs e)
		{
			try
			{
				var question = (UCSecurityQuestionDropDown.SelectedValue != "" && UCSecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue)
															? null
															: UCSecurityQuestionTextBox.TextTrimmed();
				long? questionId = (UCSecurityQuestionDropDown.SelectedValue != "" && UCSecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue)
															? UCSecurityQuestionDropDown.SelectedValueToInt()
															: (long?)null;

				UpdateAccountInfo = new UserConversionDetail
				{
					OldUsername = UCOldUsernameTextbox.TextTrimmed(),
					OldPassword = App.Settings.Theme == FocusThemes.Education ? UCOldPassword.TextTrimmed() : UCOldPassword.Attributes["value"],
					NewUsername = UCEmailAddressTextbox.TextTrimmed(),
					NewPassword = UCNewPasswordTextBox.TextTrimmed(),
					SecurityQuestion = question,
					SecurityQuestionId = questionId,
					SecurityAnswer = UCSecurityAnswerTextBox.TextTrimmed()
				};

				if (App.Settings.Theme != FocusThemes.Education)
				{
					UserConversion.Hide();
					if (App.Settings.Module == FocusModules.Explorer)
						Model = GetExplorerRegistrationModel();
					else
						Model = GetCareerRegistrationModel();
					ShowMigratedAgreeToTerms();
				}
				else
				{
					CompleteUserMigration();
				}
			}
			catch (ApplicationException ex)
			{
				App.RemoveSessionValue("Career:UserContext");
				// Reset the forms
				ResetForms();

				// Log the error
				LoginValidator.IsValid = false;
				LoginValidator.ErrorMessage = ex.Message;

				LogInModal.Show();
			}
		}

		/// <summary>
		/// Shows the migrated agree to terms.
		/// </summary>
		public void ShowMigratedAgreeToTerms()
		{
			RegisterModal.Show();
			panSignInLinks.Visible = panNormalUserSection.Visible = RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = false;
			panMigratedUserSection.Visible = RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = true;

			// If this is an education client then the T&C's are shown first in the migration process
			if (App.Settings.Theme == FocusThemes.Education)
			{
				panRegisterStep1.Visible = true;
			}
		}

		/// <summary>
		/// Shows the agree to terms.
		/// </summary>
		public void ShowAgreeToTerms()
		{
			RegisterModal.Show();
			panSignInLinks.Visible = panConsentUserSection.Visible = RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = panMigratedUserSection.Visible = false;
			panNormalUserSection.Visible = RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = true;

			// If this is an education client then the T&C's are shown first in the migration process
			if (App.Settings.Theme == FocusThemes.Education)
			{
				panRegisterStep1.Visible = true;
			}

			if (App.IsAnonymousAccess())
			{
				var message = HttpUtility.JavaScriptStringEncode(CodeLocalise("CancelButton.AnonymousMessage", "You must accept the terms and conditions to continue or close your browser to exit"));
				CancelButton.Attributes.Add("onclick", string.Format("alert('{0}');return false;", message));
				CancelButton.Attributes.Add("onkeypress", string.Format("alert('{0}');return false;", message));

				RegistrationLabel.Visible = false;
			}
		}

		/// <summary>
		/// Handles the Click event of the panMigratedAgreeToTerms control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void panMigratedAgreeToTerms_Click(object sender, EventArgs e)
		{
			if (App.Settings.Theme == FocusThemes.Education)
				//show additional data screen
				ConvertUserToCareerUser();
			else
				CompleteUserMigration();
		}

		/// <summary>
		/// Completes the user migration.
		/// </summary>
		private void CompleteUserMigration()
		{
			var userConverted = false;

			try
			{
				if (App.User.UserId.IsNotNull())
				{
					if (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer))
					{
						IsUsernameChanged = ServiceClientLocator.AccountClient(App).ChangeUsername(UpdateAccountInfo.OldUsername, UpdateAccountInfo.NewUsername);

						if (IsUsernameChanged && App.Settings.Module != FocusModules.Explorer)
						{
							IsPasswordChanged = ServiceClientLocator.AccountClient(App).ChangePassword(UpdateAccountInfo.OldPassword, UpdateAccountInfo.NewPassword);

							IsSecurityQuestionAnswerChanged = ServiceClientLocator.AccountClient(App).ChangeSecurityQuestion(UpdateAccountInfo.SecurityQuestion, UpdateAccountInfo.SecurityQuestionId, UpdateAccountInfo.SecurityAnswer);

							// Only update the Migration and Consent flags if the username, password and security question has been successfully set
							if (IsUsernameChanged && IsPasswordChanged && IsSecurityQuestionAnswerChanged)
							{
								//Update Migrated Status
								IsMigratedStatusChanged = ServiceClientLocator.AccountClient(App).ChangeMigratedStatus(true);

								//Update Consent status
								IsConsentStatusChanged = ServiceClientLocator.AccountClient(App).ChangeConsentStatus(true);
							}
						}

						userConverted = IsUsernameChanged && IsPasswordChanged && IsSecurityQuestionAnswerChanged && IsMigratedStatusChanged && IsConsentStatusChanged;
					}
					else
					{
						#region Commented out code
						//// In OAuth mode only either ssn or the address will have been entered on user conversion, the rest of the info is in the db and should remain the same
						//var userModel = App.User.UserId != 0
						//  ? ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId)
						//  : new UserDetailsModel();

						//var ssn = OAuthSocialSecurityNumberPart1TextBox.Text + OAuthSocialSecurityNumberPart2TextBox.Text + OAuthSocialSecurityNumberPart3TextBox.Text;
						//ssn = Regex.Replace(ssn, @"[^0-9]", "");

						//userModel.PersonDetails.DateOfBirth = OAuthDateOfBirthTextBox.Text.ToDate();

						//var phoneType = PhoneTypes.Phone;
						//var selectedPhoneType = OAuthPhoneTypeDropDown.SelectedValue;
						//if (selectedPhoneType.IsNotNullOrEmpty())
						//{
						//  var tempPhoneType = (PhoneType)Enum.Parse(typeof(PhoneType), selectedPhoneType, true);

						//  switch (tempPhoneType)
						//  {
						//    case PhoneType.Cell:
						//      phoneType = PhoneTypes.Mobile;
						//      break;
						//    case PhoneType.NonUS:
						//    case PhoneType.Home:
						//    case PhoneType.Work:
						//      phoneType = PhoneTypes.Phone;
						//      break;
						//    case PhoneType.Fax:
						//      phoneType = PhoneTypes.Fax;
						//      break;
						//    default:
						//      phoneType = PhoneTypes.Phone;
						//      break;
						//  }
						//}

						//userModel.PrimaryPhoneNumber = new PhoneNumberDto
						//{
						//  IsPrimary = true,
						//  Number = OAuthPhoneNumberTextBox.Text,
						//  PhoneType = phoneType,
						//  ProviderId = OAuthCellphoneProviderDropDown.SelectedValueToLong()
						//};

						//userModel.PersonDetails.SocialSecurityNumber = ssn;

						//if (userModel.AddressDetails.IsNull())
						//  userModel.AddressDetails = new PersonAddressDto();

						//userModel.AddressDetails.Line1 = OAuthAddressTextBox.TextTrimmed();
						//userModel.AddressDetails.TownCity = OAuthCityTextBox.TextTrimmed();
						//userModel.AddressDetails.StateId = Convert.ToInt64(OAuthStateDropDown.SelectedValue);
						//userModel.AddressDetails.PostcodeZip = OAuthZIPcodeTextBox.TextTrimmed();

						//ServiceClientLocator.AccountClient(App).UpdateUser(userModel);

						//userConverted = true;
						#endregion

						if (App.Settings.Module == FocusModules.Explorer)
							RegisterExplorer();
						else
							RegisterCareer();
					}
				}

				if (userConverted)
				{
					App.User.IsMigrated = App.User.RegulationsConsent = true;

					FormsAuthentication.SignOut();
					var authTicket = new FormsAuthenticationTicket(1, App.User.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, App.User.SerializeUserContext());
					var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

					var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
					Response.Cookies.Add(authCookie);

					UpdateUserAccountInfo();
				}
				else
				{
					App.RemoveSessionValue("Career:UserContext");
					// Reset the forms
					ResetForms();

					// Log the error
					LoginValidator.IsValid = false;
					LoginValidator.ErrorMessage = @"User conversion has failed.";

					LogInModal.Show();
				}
			}
			catch (ApplicationException ex)
			{
				App.RemoveSessionValue("Career:UserContext");
				// Reset the forms
				ResetForms();

				// Log the error
				LoginValidator.IsValid = false;
				LoginValidator.ErrorMessage = ex.Message;

				LogInModal.Show();
			}
		}

		/// <summary>
		/// Handles the Click event of the btnBackUserConversion control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void btnBackUserConversion_Click(object sender, EventArgs e)
		{
			LogOut();
		}

		/// <summary>
		/// Handles the Click event of the ConsentAgreeToTermsButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ConsentAgreeToTermsButton_Click(object sender, EventArgs e)
		{
			if (App.Settings.Module != FocusModules.Explorer)
			{
				if (App.Settings.Theme == FocusThemes.Education)
				{
					//show additional data screen
					ConvertUserToCareerUser();
				}
				else
				{
					CompleteUserMigration();
					ShowAppropriateWindow();
				}
			}
			else
			{
				//Update Consent status
				IsConsentStatusChanged = ServiceClientLocator.AccountClient(App).ChangeConsentStatus(true);
				App.User.RegulationsConsent = true;
				UpdateUserAccountInfo();
				ShowAppropriateWindow();
			}
		}

		protected void CancelAndLogoutButton_Click(object sender, EventArgs e)
		{
			LogOut();
		}

		private void RegistrationStep1Explorer()
		{
			Model = GetExplorerRegistrationModel();

			// Check to see if the username already exists in the system
			if (ServiceClientLocator.AccountClient(App).CheckUserExists(Model).Item1)
			{
				RaiseUsernameInUseError();
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
			}
			else
			{
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = false;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = true;
			}
		}

		/// <summary>
		/// Registration - Step 1 : Career.
		/// </summary>
		private void RegistrationStep1Career()
		{
			Model = GetCareerRegistrationModel();

			// Check to see if the username already exists in the system
			var isIntegrationUser = ExternalId.IsNotNullOrEmpty();
			if (!String.IsNullOrEmpty(Model.AccountUserName) && ServiceClientLocator.AccountClient(App).CheckUserExists(Model, checkLocalOnly: isIntegrationUser).Item1)
			{
				var errorMessage = "User with the given information already present in the system.";

				RaiseRegistrationError(errorMessage);

				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;

			}
			else
			{
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = false;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = true;
			}
		}

		/// <summary>
		/// Handles the Click event of the CancelRegistration control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void CancelRegistration_Click(object sender, EventArgs e)
		{
			if (App.IsAnonymousAccess())
				return;

			RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
			RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
			ResetForms();

			RegisterModal.Hide();

			OnLogInCancelled(new CommandEventArgs(SourceCommandName, null));

			LogInModal.Show();
		}

		/// <summary>
		/// Handles the Click event of the btnMigratedCancel control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void btnMigratedCancel_Click(object sender, EventArgs e)
		{
			LogOut();
		}
		
		/// <summary>
		/// Handles the Click event of the SearchJobWithoutReg control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void SearchJobWithoutReg_Click(object sender, EventArgs e)
		{
			App.RemoveSessionValue("Career:UserContext");
			RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
			RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
			ResetForms();

			RegisterModal.Hide();

			OnLogInCancelled(new CommandEventArgs(SourceCommandName, null));

			if (App.Settings.Module != FocusModules.Explorer)
				SearchWithoutLogin.Show();
		}

		/// <summary>
		/// Handles the Click event of the SWSAgreeToTermsButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void SWSAgreeToTermsButton_Click(object sender, EventArgs e)
		{
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
		}

		/// <summary>
		/// Handles the Click event of the AgreeToTermsButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void AgreeToTermsButton_Click(object sender, EventArgs e)
		{
			if (App.IsAnonymousAccess())
			{
				RegisterModal.Hide();

				panSignInLinks.Visible =
					panConsentUserSection.Visible =
					RegisterStep1Panel.Visible =
					panRegisterStep1Heading.Visible =
					panRegisterStep1.Visible =
					panRegisterStep1Button.Visible =
					panMigratedUserSection.Visible =
					panNormalUserSection.Visible =
					RegisterStep2Panel.Visible =
					panRegisterStep2Heading.Visible = false;

				var userContext = new UserContext
				{
					FirstName = "Anonymous",
					PersonId = null,
					IsAnonymous = true
				};

				var authTicket = new FormsAuthenticationTicket(1, userContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, userContext.SerializeUserContext());
				var encryptedTicket = FormsAuthentication.Encrypt(authTicket);

				var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
				Response.Cookies.Add(authCookie);

				var identity = new FormsIdentity(authTicket);
				var principal = new UserPrincipal(identity, userContext);
				Context.User = principal;

				return;
			}

			if (App.Settings.Theme == FocusThemes.Education)
			{
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;

				RegistrationLabel.Text = HtmlLocalise("steppedProcessSubhead.Label", "<strong>Step 2</strong> of 2");
				ApplyTheme();
				RegisterModal.Show();
				EmailAddressTextbox.Text = EmailAddress;
			}
			else
			{
				Model.TandCConsentGiven = true;
				if (CompleteStep2())
				{
					ResetForms();

					if (String.IsNullOrEmpty(SourceCommandName))
					{
						RegistrationConfirmation.Visible = true;
						RegistrationConfirmation.Show(true, true);
					}
					RegisterModal.Hide();
				}
			}
		}

		/// <summary>
		/// Registers the user in career and career/explorer mode.
		/// </summary>
		private bool RegisterCareer()
		{
			// Don't get the model again because at the point this is called (Agreeing to t's & c's) the password has gone
			// The model is populated once the user has filled in all the registration details (register button click)
			//var model = GetCareerRegistrationModel();

			var isIntegrationUser = ExternalId.IsNotNullOrEmpty();
			if (!String.IsNullOrEmpty(Model.AccountUserName) && ServiceClientLocator.AccountClient(App).CheckUserExists(Model, checkLocalOnly: isIntegrationUser).Item1)
			{
				var errorMessage = "User with the given information already present in the system.";

				RaiseRegistrationError(errorMessage);

				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
				RegisterModal.Show();

				return false;

			}

			ServiceClientLocator.AccountClient(App).RegisterCareerUser((CareerRegistrationModel)Model);

			if (Model.RegistrationError == ErrorTypes.UserNameAlreadyExists)
			{
				// Username has been taken between step 1 and registration completion
				RaiseUsernameInUseError();
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
				RegisterModal.Show();

				return false;
			}

			UserName = Model.AccountUserName;

			LogIn(Model.AccountPassword, true);

			LoggedIn(this, new LoggedInEventArgs(!String.IsNullOrEmpty(App.User.ScreenName) ? App.User.ScreenName : App.User.EmailAddress, SourceCommandName, App.User.IsShadowingUser));
			
			return true;
		}

		/// <summary>
		/// Registers the user in Explorer only mode.
		/// </summary>
		private bool RegisterExplorer()
		{

			ServiceClientLocator.AccountClient(App).RegisterExplorerUser((ExplorerRegistrationModel)Model);

			if (Model.RegistrationError == ErrorTypes.UserNameAlreadyExists)
			{
				// Username has been taken between step 1 and registration completion
				RaiseUsernameInUseError();
				RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
				RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;
				RegisterModal.Show();

				return false;
			}

			UserName = Model.AccountUserName;
			LogIn(Model.AccountPassword);
			RegisterModal.Hide();
			LoggedIn(this, new LoggedInEventArgs(!String.IsNullOrEmpty(App.User.ScreenName) ? App.User.ScreenName : App.User.EmailAddress, SourceCommandName, App.User.IsShadowingUser));
			if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer) Response.Redirect(UrlBuilder.Home());
			
			return true;
		}


		/// <summary>
		/// Gets the career registration model.
		/// </summary>
		/// <returns></returns>
		private CareerRegistrationModel GetCareerRegistrationModel()
		{
			// Create model and initialise it with Email Address, Security Question and Answer
			var model = new CareerRegistrationModel
			{
				AccountUserName = EmailAddressTextbox.TextTrimmed(defaultValue: null),
				AccountPassword = NewPasswordTextBox.TextTrimmed(defaultValue: null),
				EmailAddress = EmailAddressTextbox.TextTrimmed(defaultValue: null),
				SecurityQuestion = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue)
					? null
					: SecurityQuestionTextBox.TextTrimmed(),
				SecurityQuestionId = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue)
					? SecurityQuestionDropDown.SelectedValueToInt()
					: (long?)null,
				SecurityAnswer = SecurityAnswerTextBox.TextTrimmed(),
				Pin = Pin,
				PinRegisteredEmailAddress = EmailAddress,
				ExternalId = ExternalId,
        AccountType = (App.Settings.DisplayCareerAccountType && SeekerAccountTypeDropDownList.SelectedItem.IsNotNull()) ? (CareerAccountType)int.Parse(SeekerAccountTypeDropDownList.SelectedItem.Value) : (CareerAccountType?) null
			};

			if (Profile.IsNotNull())
			{
				model.EmailAddress = Profile.EmailAddress;
				model.ScreenName = Profile.ScreenName;
			}

			// Establish if this is a registration using SSN
			var ssn = string.Concat(SocialSecurityNumberPart1TextBox.Text, SocialSecurityNumberPart2TextBox.Text, SocialSecurityNumberPart3TextBox.Text);
			if (ssn.IsNullOrEmpty()) ssn = string.Concat(OAuthSocialSecurityNumberPart1TextBox.Text, OAuthSocialSecurityNumberPart2TextBox.Text, OAuthSocialSecurityNumberPart3TextBox.Text);
			if (ssn.IsNullOrEmpty() && ExternalId.IsNotNullOrEmpty())
				ssn = string.Concat(SocialSecurityNumberPart1TextBox.Attributes["value"], SocialSecurityNumberPart2TextBox.Attributes["value"], SocialSecurityNumberPart3TextBox.Attributes["value"]);

			ssn = Regex.Replace(ssn, @"[^0-9]", "");

			var isSsn = !(ssn.Length < 9);

			// If SSN registrataion then just use this else set up all the other data items needed for the registration model
			if (isSsn)
				model.SocialSecurityNumber = ssn;

			model.FirstName = AdditionalInformationFirstNameTextBox.TextTrimmed(defaultValue: null);
			model.MiddleName = AdditionalInformationMiddleInitialTextBox.TextTrimmed(defaultValue: null);
			model.LastName = AdditionalInformationLastNameTextBox.TextTrimmed(defaultValue: null);
			model.SuffixId = AdditionalInformationSuffixDropDown.SelectedValueToNullableLong();

			model.DateOfBirth = AdditionalInformationDateOfBirthTextBox.TextTrimmed().ToDate();

			var phoneType = PhoneType.Home;

			var selectedPhoneType = PhoneNoDropDownList.SelectedValue;

			if (selectedPhoneType.IsNotNullOrEmpty())
				phoneType = (PhoneType)Enum.Parse(typeof(PhoneType), selectedPhoneType, true);

			model.PrimaryPhone = new Phone
			{
				PhoneNumber = Regex.Replace(AdditionalInformationPhoneNumberTextBox.TextTrimmed(), @"[^0-9]", ""),
				PhoneType = phoneType,
				Provider = CellphoneProviderDropDown.SelectedValue.IsNotNullOrEmpty() ? Convert.ToInt64(CellphoneProviderDropDown.SelectedValue) : (long?)null
			};

			model.PostalAddress = new Address
			{
				Street1 = AdditionalInformationAddressTextBox.TextTrimmed(defaultValue: null),
				City = AdditionalInformationCityTextBox.TextTrimmed(defaultValue: null),
				StateId = AdditionalInformationStateDropdown.SelectedIndex != 0 ? AdditionalInformationStateDropdown.SelectedValue.ToLong() : null,
				StateName = AdditionalInformationStateDropdown.SelectedIndex != 0 ? AdditionalInformationStateDropdown.SelectedItem.Text : null,
				Zip = AdditionalInformationZipTextBox.TextTrimmed(defaultValue: null)
			};

			model.ProgramAreaId = null;
			model.DegreeId = null;

			model.CampusId = null;

			return model;
		}

		/// <summary>
		/// Gets the explorer registration model.
		/// </summary>
		/// <returns></returns>
		private ExplorerRegistrationModel GetExplorerRegistrationModel()
		{
			var model = new ExplorerRegistrationModel
			{
				AccountUserName = EmailAddressTextbox.TextTrimmed(defaultValue: null),
				AccountPassword = NewPasswordTextBox.TextTrimmed(defaultValue: null),
				ScreenName = ScreenNameTextbox.TextTrimmed(),
				EmailAddress = EmailAddressTextbox.TextTrimmed(defaultValue: null),
				SecurityQuestion = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue) ? null : SecurityQuestionTextBox.TextTrimmed(),
				SecurityQuestionId = (SecurityQuestionDropDown.SelectedValue != "" && SecurityQuestionDropDown.SelectedValue != CreateOwnSecurityQuestionValue) ? SecurityQuestionDropDown.SelectedValueToInt() : (long?)null,
				SecurityAnswer = SecurityAnswerTextBox.TextTrimmed()   
			};

			if (Profile.IsNotNull())
			{
				model.EmailAddress = Profile.EmailAddress;
				model.ScreenName = Profile.ScreenName;
				model.FirstName = Profile.FirstName;
				model.LastName = Profile.LastName;
				model.ExternalId = Profile.Id;
			}

			// Establish if this is a registration using SSN
			var ssn = string.Concat(SocialSecurityNumberPart1TextBox.Text, SocialSecurityNumberPart2TextBox.Text, SocialSecurityNumberPart3TextBox.Text);
			if (ssn.IsNullOrEmpty()) ssn = string.Concat(OAuthSocialSecurityNumberPart1TextBox.Text, OAuthSocialSecurityNumberPart2TextBox.Text, OAuthSocialSecurityNumberPart3TextBox.Text);
			if (ssn.IsNullOrEmpty() && ExternalId.IsNotNullOrEmpty())
				ssn = string.Concat(SocialSecurityNumberPart1TextBox.Attributes["value"], SocialSecurityNumberPart2TextBox.Attributes["value"], SocialSecurityNumberPart3TextBox.Attributes["value"]);

			ssn = Regex.Replace(ssn, @"[^0-9]", "");

			var isSsn = !(ssn.Length < 9);

			// If SSN registrataion then just use this else set up all the other data items needed for the registration model
			if (isSsn)
				model.SocialSecurityNumber = ssn;

			return model;
		}


		/// <summary>
		/// Raises the username in use error.
		/// </summary>
		private void RaiseUsernameInUseError()
		{
			UsernameInUseErrorMessage.Text = CodeLocalise("UsernameErrorMessage.Text", "Email address is already in use.");
		}

		/// <summary>
		/// Raises the registration error.
		/// </summary>
		/// <param name="errorMessage">The error message.</param>
		private void RaiseRegistrationError(string errorMessage)
		{
			UsernameInUseErrorMessage.Text = CodeLocalise("UsernameErrorMessage.Text", errorMessage);
		}

		/// <summary>
		/// Binds the security question drop down.
		/// </summary>
		private void BindSecurityQuestionDropDown()
		{
			if (App.Settings.Module == FocusModules.Explorer)
				BindExplorerDropDown(SecurityQuestionDropDown, ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions), null, CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"));
			else
			{
				SecurityQuestionDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions), null, CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"));
				UCSecurityQuestionDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SecurityQuestions), null, CodeLocalise("SecurityQuestion.TopDefault", "- select a question -"));
			}

			SecurityQuestionDropDown.Items.Add(new ListItem(CodeLocalise("CreateOwnSecurityQuestion.DropDownValue", "Create own security question"), CreateOwnSecurityQuestionValue));
			UCSecurityQuestionDropDown.Items.Add(new ListItem(CodeLocalise("CreateOwnSecurityQuestion.DropDownValue", "Create own security question"), CreateOwnSecurityQuestionValue));
		}

		/// <summary>
		/// Binds the lookup.
		/// </summary>
		/// <param name="dropDownList">The drop down list.</param>
		/// <param name="lookupItems">The lookup items.</param>
		/// <param name="currentValue">The current value.</param>
		/// <param name="topDefault">The top default.</param>
		private static void BindExplorerDropDown(DropDownList dropDownList, IList<LookupItemView> lookupItems, string currentValue, string topDefault)
		{
			dropDownList.DataSource = lookupItems;
			dropDownList.DataValueField = "Id";
			dropDownList.DataTextField = "Text";
			dropDownList.DataBind();

			if (!String.IsNullOrEmpty(currentValue) && dropDownList.Items.FindByValue(currentValue) != null) dropDownList.SelectValue(currentValue);
			if (!String.IsNullOrEmpty(topDefault)) dropDownList.Items.Insert(0, new ListItem(topDefault, string.Empty));
		}

		/// <summary>
		/// Sets the application specific row visibility.
		/// </summary>
		private void SetApplicationSpecificRowVisibility()
		{
			ScreenNameRow.Visible = (App.Settings.Module == FocusModules.Explorer);
			SSNRow.Visible = SSN2Row.Visible = (App.Settings.Module != FocusModules.Explorer);

            WorkforcePlaceHolder1.Visible = App.Settings.EnableSsnFeatureGroup;
            AdditionalInformationDateOfBirthRow.Visible = App.Settings.EnableDobFeatureGroup;

			if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer)
			{
				EmailRow.Visible =
				ConfirmEmailAddressRow.Visible =
				PasswordRow.Visible =
				ConfirmPasswordRow.Visible =
				SecurityQuestionRow.Visible =
				SecurityAnswerRow.Visible = false;
			}
		}

		/// <summary>
		/// Logs the in.
		/// </summary>
		/// <param name="password">The password.</param>
		/// <param name="isNewUser">Whether this is a newly registered user</param>
		private void LogIn(string password, bool isNewUser = false)
		{
			var validationUrl = "";

			if (App.Settings.Theme == FocusThemes.Education)
				validationUrl = App.Settings.CareerApplicationPath + UrlBuilder.AccountValidate(false);

			ValidatedUserView integrationUser = null;

			var userData = (App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer)
				? ServiceClientLocator.AuthenticationClient(App).LogIn(Profile, App.Settings.Module)
				: ServiceClientLocator.AuthenticationClient(App).LogInToCareer(UserName, password, validationUrl, out integrationUser);
			App.UserData.Load(userData.UserData);

			ExternalId = null;

			if (userData.UserContext.UserId > 0)
			{
				StoreUserContext(userData.UserContext);

				if (App.Settings.DisplaySurveyFrequency)
					App.SetCookieValue("JobSeekerSurvey:Show", "true");

				if (userData.AccountReactivated)
				{
					App.SetSessionValue("ReactivateRequestKey", true);
					Response.Redirect(UrlBuilder.ReactivateAccount());
					return;
				}

				ServiceClientLocator.CandidateClient(App).ResolveCandidateIssues(userData.UserContext.PersonId ?? 0, new List<CandidateIssueType>() { CandidateIssueType.NotLoggingIn }, true);
			}
			else if (integrationUser.IsNotNull())
			{
			  RegisterExternalUser(integrationUser);
				return;
			}

			//TODO: Martha (UI Claimant) - Don't think we will need this

			//Set UI Claimant status 
			//if (CareerCommon.CareerApp.Settings.UIClaimantEnabled)
			//{
			//  if (App.GetSessionValue<string>("Career:UIClaimantStatus").IsNullOrEmpty())
			//  {
			//    //Check for UI Claimant
			//    if (usercontext.UserInfo != null && usercontext.UserInfo.IsUIClaimant.HasValue && (bool)usercontext.UserInfo.IsUIClaimant == true)
			//      App.SetSessionValue("Career:UIClaimantStatus", "2");
			//    else
			//      App.SetSessionValue("Career:UIClaimantStatus", "0");
			//  }

			//  if (App.GetSessionValue<string>("Career:UIClaimantStatus") == "2")
			//  {
			//    //Check for registered date
			//    if (usercontext.RegisteredOn != null && DateTime.Now.Subtract(usercontext.RegisteredOn).Days > CareerCommon.CareerApp.Settings.UIClaimantMaxDays)
			//      App.SetSessionValue("Career:UIClaimMaxDaysExceeded", true);
			//    else
			//      App.SetSessionValue("Career:UIClaimMaxDaysExceeded", false);
			//  }
			//}

			// Convert the user to a Career user
			if (!App.User.IsMigrated && App.Settings.Theme != FocusThemes.Education && App.Settings.Module != FocusModules.Explorer)
			{
				ConvertUserToCareerUser(password);
			}
			else if (!App.User.IsMigrated && App.Settings.Theme == FocusThemes.Education && userData.UserContext.UserId == 0)
			{
				var title = CodeLocalise("SuccessfulCareerRegistration.Title", "Thank you for registering with #FOCUSCAREER#");
				var body = CodeLocalise("SuccessfulCareerRegistration.Body", "We've sent an email to your inbox with a hyperlink to confirm your email address and complete your account registration. The email will also include information about the account validation process and provide contact information for questions or assistance. When you check your email, click on the link to authenticate your account. If you do not receive this email within 24 hours, please check your computer or network's filters, which could block your receipt or place mail in a spam folder.<br />Email us at #SUPPORTEMAIL# for assistance with registration completion. Our business hours are #SUPPORTHOURSOFBUSINESS#");

				ConfirmationModal.Show(title, body, "CLOSE", "CANCEL");
			}
			else
			{
				UpdateUserAccountInfo(isNewUser);
			}
		}

		/// <summary>
		/// Shows the registration modal for an external user, with fields pre-filled
		/// </summary>
		/// <param name="integrationUser">The external user</param>
		public void RegisterExternalUser(ValidatedUserView integrationUser)
		{
			LogInModal.Hide();

			ExternalId = integrationUser.ExternalId;

			EmailAddressTextbox.Text = ConfirmRegEmailAddressTextbox.Text = integrationUser.EmailAddress;

			AdditionalInformationFirstNameTextBox.Text = integrationUser.FirstName;
      AdditionalInformationMiddleInitialTextBox.Text = integrationUser.MiddleInitial;
      AdditionalInformationLastNameTextBox.Text = integrationUser.LastName;
			AdditionalInformationZipTextBox.Text = integrationUser.AddressPostcodeZip;
			AdditionalInformationAddressTextBox.Text = integrationUser.AddressLine1;
			AdditionalInformationCityTextBox.Text = integrationUser.AddressTownCity;
			AdditionalInformationPhoneNumberTextBox.Text = integrationUser.PrimaryPhone;

		  AdditionalInformationFirstNameTextBox.Enabled = AdditionalInformationFirstNameTextBox.Text.IsNullOrEmpty();
		  AdditionalInformationMiddleInitialTextBox.Enabled = AdditionalInformationMiddleInitialTextBox.Text.IsNullOrEmpty();
		  AdditionalInformationLastNameTextBox.Enabled = AdditionalInformationLastNameTextBox.Text.IsNullOrEmpty();
      AdditionalInformationZipTextBox.Enabled = AdditionalInformationZipTextBox.Text.IsNullOrEmpty();
      AdditionalInformationAddressTextBox.Enabled = AdditionalInformationAddressTextBox.Text.IsNullOrEmpty();
      AdditionalInformationCityTextBox.Enabled = AdditionalInformationCityTextBox.Text.IsNullOrEmpty();
      AdditionalInformationPhoneNumberTextBox.Enabled = AdditionalInformationPhoneNumberTextBox.Text.IsNullOrEmpty();

		  if (integrationUser.SocialSecurityNumber.IsNotNullOrEmpty())
      {
        SocialSecurityNumberPart1TextBox.Attributes["value"] =
          ConfirmSocialSecurityNumberPart1TextBox.Attributes["value"] =
					SocialSecurityNumberPart1TextBox.Text = integrationUser.SocialSecurityNumber.Substring(0, 3);
        SocialSecurityNumberPart2TextBox.Attributes["value"] =
          ConfirmSocialSecurityNumberPart2TextBox.Attributes["value"] =
					SocialSecurityNumberPart2TextBox.Text = integrationUser.SocialSecurityNumber.Substring(3, 2);
        SocialSecurityNumberPart3TextBox.Attributes["value"] =
          ConfirmSocialSecurityNumberPart3TextBox.Attributes["value"] =
					SocialSecurityNumberPart3TextBox.Text = integrationUser.SocialSecurityNumber.Substring(5, 4);

        SocialSecurityNumberPart1TextBox.Enabled =
          ConfirmSocialSecurityNumberPart1TextBox.Enabled =
          SocialSecurityNumberPart2TextBox.Enabled =
          ConfirmSocialSecurityNumberPart2TextBox.Enabled =
          SocialSecurityNumberPart3TextBox.Enabled =
          ConfirmSocialSecurityNumberPart3TextBox.Enabled = false;
      }

		  if (integrationUser.AddressStateId.GetValueOrDefault(0) > 0)
		  {
        BindDropdowns();
		    AdditionalInformationStateDropdown.SelectValue(integrationUser.AddressStateId.ToString());
		    AdditionalInformationStateDropdown.Enabled = false;
		  }

		  if (integrationUser.DateOfBirth.HasValue)
		  {
				AdditionalInformationDateOfBirthTextBox.Text = integrationUser.DateOfBirth.Value.ToString("MM/dd/yyyy");
        AdditionalInformationDateOfBirthTextBox.Enabled = false;
      }

			RegisterModal.Show();
		}

		/// <summary>
		/// Converts the user to career user.
		/// </summary>
		/// <param name="password">The password.</param>
		public void ConvertUserToCareerUser(string password = "")
		{
			// TODO: Not sure I like this
			UCOldUsernameTextbox.Text = App.User.UserName;

			if (password.IsNotNullOrEmpty())
				UCOldPassword.Attributes.Add("value", password);

			UserConversion.Show();

			if (App.Settings.Theme == FocusThemes.Workforce)
        ConversionPlaceHolder.Visible = (!(App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer));
		}

		/// <summary>
		/// Updates the user account info.
		/// </summary>
		/// <param name="isNewUser">Whether this is a newly registered user</param>
		private void UpdateUserAccountInfo(bool isNewUser = false)
		{
			var redirectionOverride = LoggedIn(this, new LoggedInEventArgs(App.User.ScreenName.IsNotNullOrEmpty() ? App.User.ScreenName : App.User.UserName, SourceCommandName, App.User.IsShadowingUser));

			if (redirectionOverride)
			{
				LogInModal.Hide();
			}
			else
			{
				if (!App.User.RegulationsConsent)
				{
					LogInModal.Hide();
					RegisterModal.Show();
					RegisterStep1Panel.Visible =
						panRegisterStep1Heading.Visible =
						panRegisterStep1.Visible =
						panRegisterStep1Button.Visible =
						panNormalUserSection.Visible = panMigratedUserSection.Visible = panSignInLinks.Visible = false;
					RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = panConsentUserSection.Visible = true;
				}
				else
				{
					var checkRedirect = App.GetCookieValue("CareerExplore:Redirect", versionCookie: true);
					if (checkRedirect.IsNotNullOrEmpty())
					{
						App.RemoveCookieValue("CareerExplore:Redirect", true);
						Response.Redirect(checkRedirect, true);
					}
					else
					{
						ShowAppropriateWindow(isNewUser);
					}
				}
			}
		}

		/// <summary>
		/// Shows the appropriate window.
		/// </summary>
		/// <param name="isNewUser">Whether this is a newly registered user</param>
		public void ShowAppropriateWindow(bool isNewUser = false)
		{
			if (App.Settings.Theme == FocusThemes.Workforce && App.UserData.HasDefaultResume && App.UserData.DefaultResumeCompletionStatus != ResumeCompletionStatuses.Completed)
			{
				App.SetSessionValue("Career:ResumeID", App.UserData.DefaultResumeId);
				if (App.User != null && App.GuideToResume(App.User.PersonId))
				{
					App.SetCookieValue("ShowIncompleteResumeConfirmation", "1");
					Response.Redirect(UrlBuilder.YourResume());
				}
				else
				{
					ConfirmationModal.Show("", "During your last session you were in the middle of building your default resume.  Do you want to continue where you left off?", "No, return to home page", "CANCEL", "", "Yes, continue", "OK", height: 50);
				}
			}
			else
			{
				if (App.Settings.Module != FocusModules.Explorer && !App.UserData.HasDefaultResume && ServiceClientLocator.CandidateClient(App).GetUiClaimantStatus(App.User.PersonId.GetValueOrDefault(0), isNewUser))
				{
					Response.RedirectToRoute("ResumeWizard");
				}
				else
				{
					LogInModal.Hide();
					RegistrationConfirmation.Visible = true;
					RegistrationConfirmation.Show(false, true);
				}
			}
		}

        //internal bool RecaptchaVerifyResult
        //{
        //    get { return Recaptcha.IsValid; }
        //}

		/// <summary>
		/// Handles the OkCommand event of the ConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
		public void ConfirmationModal_OkCommand(object sender, CommandEventArgs e)
		{
			Response.RedirectToRoute("ResumeWizard");
		}

		/// <summary>
		/// Handles the CloseCommand event of the ConfirmationModal control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		public void ConfirmationModal_CloseCommand(object sender, CommandEventArgs e)
		{
			if (!App.User.IsMigrated && App.Settings.Theme == FocusThemes.Education)
				ShowLogIn();
			else
				Response.Redirect(Page.Request.Url.ToString());
		}


		/// <summary>
		/// Raises the <see cref="E:LogInCancelled" /> event.
		/// </summary>
		/// <param name="e">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
		public virtual void OnLogInCancelled(CommandEventArgs e)
		{
			if (LogInCancelled != null)
				LogInCancelled(this, e);
		}

		/// <summary>
		/// Shows the register.
		/// </summary>
		/// <param name="sourceCommandName">Name of the source command.</param>
		public void ShowRegister(string sourceCommandName)
		{
			SourceCommandName = sourceCommandName;
			RegisterModal.Show();
		}

		/// <summary>
		/// Stores the user context.
		/// </summary>
		/// <param name="userContext">The user context.</param>
		private void StoreUserContext(UserContext userContext)
		{
			var authTicket = new FormsAuthenticationTicket(1, userContext.FirstName, DateTime.Now, DateTime.Now.AddMinutes(App.Settings.UserSessionTimeout), false, userContext.SerializeUserContext());
			var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
			var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

			Response.Cookies.Add(authCookie);

			Context.User = new UserPrincipal(Context.User.Identity, userContext);
		}

		/// <summary>
		/// Binds the dropdowns.
		/// </summary>
		private void BindDropdowns()
		{
		  if (_dropdownsBound)
		    return;

		  _dropdownsBound = true;

			AdditionalInformationSuffixDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Suffixes), "", CodeLocalise("Suffix.TopDefault", "- select suffix -"));
			AdditionalInformationStateDropdown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), "", CodeLocalise("State.TopDefault", "- select state -"));

      if (App.Settings.SSOEnabled || App.Settings.SamlEnabledForCareer)
				OAuthStateDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), "", CodeLocalise("State.TopDefault", "- select state -"));

      BindCareerAccountTypeDropdown();
		}


    protected void BindCareerAccountTypeDropdown()
    {
      SeekerAccountTypeDropDownList.Items.Add(new ListItem(CodeLocalise("Global.CandidateType.Workforce", "Job Seeker"),"0"));
      SeekerAccountTypeDropDownList.Items.Add(new ListItem(CodeLocalise("Student", "Student"),"1"));
      SeekerAccountTypeDropDownList.Items.AddLocalisedTopDefault("Global.TopDefault", "- select -");
    }

		/// <summary>
		/// Shows the recaptcha.
		/// </summary>
		/// <param name="previouslyProgressedToStep2">if set to <c>true</c> [previously progressed automatic step2].</param>
        internal void ShowRecaptcha(bool previouslyProgressedToStep2)
        {
        //    // Show recaptcha if this control is visible, recaptcha is enabled and the module is Talent (Assist users will already be logged in)
        //    var show = (Visible && !previouslyProgressedToStep2) && App.Settings.RecaptchaEnabled && App.Settings.Module == FocusModules.CareerExplorer;
        //    Recaptcha.SkipRecaptcha = !show;
            RecaptchaRow.Visible = (Visible && !previouslyProgressedToStep2) && App.Settings.RecaptchaEnabled && App.Settings.Module == FocusModules.CareerExplorer; 
        }

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			if (App.Settings.Theme == FocusThemes.Education)
			{
				ConvertAccountHeaderLabel.Text = HtmlLocalise("TellAboutYourself.Label", "Tell us about yourself");
				ConvertAccountPageLabel.Text = HtmlLocalise("steppedProcess2Subhead.Label", "<strong>Step 2</strong> of 2");
				Heading2Literal.Text = CodeLocalise("Heading2Education.Text", "Terms of Use");
			}
			else
			{
				ConvertAccountHeaderLabel.Text = HtmlLocalise("UserConversionHeading.Label", "Convert your #FOCUSCAREER# account");
				ConvertAccountPageLabel.Text = HtmlLocalise("steppedProcessSubhead.Label", "<strong>Step 1</strong> of 2");
				Heading2Literal.Text = CodeLocalise("Heading2Workforce.Text", "Terms and Conditions");
			}

			RegistrationLabel.Text = HtmlLocalise("steppedProcessSubhead.Label", "<strong>Step 1</strong> of 2");

			UserNameRequired.ErrorMessage = CodeLocalise("UserName.RequiredErrorMessage", "Username is required");
			PasswordRequired.ErrorMessage = CodeLocalise("Password.RequiredErrorMessage", "Password is required");
			LoginHelpToolTip.AlternateText = HtmlLocalise("Global.Help.ToolTip", "Help");
			LoginHelpToolTip.ToolTip = HtmlLocalise("LoginHelpToolTip.ToolTip", "Forgotten your username? Try entering the email address that you provided at account registration.");
			LoginHelpToolTip.ImageUrl = UrlBuilder.HelpIcon();
			
			EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			EmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address is not correct format");

			UCEmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			UCEmailAddressRegEx.ErrorMessage = CodeLocalise("EmailAddress.RegExErrorMessage", "Email address is not correct format");

			UCConfirmEmailAddressRequired.ErrorMessage = ConfirmEmailAddressRequired.ErrorMessage = CodeLocalise("ConfirmEmailAddress.RequiredErrorMessage", "Confirm email address is required");
			UCConfirmEmailAddressCompare.ErrorMessage = ConfirmEmailAddressCompare.ErrorMessage = CodeLocalise("ConfirmEmailAddress.CompareErrorMessage", "Email addresses must match");
			UCNewPasswordRequired.ErrorMessage = NewPasswordRequired.ErrorMessage = CodeLocalise("NewPassword.RequiredErrorMessage", "Password is required");
			UCNewPasswordRegEx.ErrorMessage = NewPasswordRegEx.ErrorMessage = CodeLocalise("NewPasswordTextBox.RegExErrorMessage", "Password must be 6-20 characters, contain at least 1 number and must not include spaces.");
			UCConfirmNewPasswordRequired.ErrorMessage = ConfirmNewPasswordRequired.ErrorMessage = CodeLocalise("ConfirmNewPassword.RequiredErrorMessage", "Confirm password is required");
			UCConfirmNewPasswordCompare.ErrorMessage = ConfirmNewPasswordCompare.ErrorMessage = CodeLocalise("ConfirmNewPasswordCompare.CompareErrorMessage", "Passwords must match");
			UCUserNameAndPasswordCompare.ErrorMessage = UserNameAndPasswordCompare.ErrorMessage = CodeLocalise("PasswordCompareErrorMessage.ErrorMessage", "Email address and password cannot be the same");

			SocialSecurityNumberValidator.ErrorMessage = CodeLocalise("SocialSecurityNumberValidator.ErrorMessage", "Invalid social security number");
			ConfirmSocialSecurityNumberValidator.ErrorMessage = CodeLocalise("ConfirmSocialSecurityNumberValidator.ErrorMessage", "SSN must match");
			UCSecurityQuestionRequired.ErrorMessage = SecurityQuestionRequired.ErrorMessage = CodeLocalise("SecurityQuestionRequired.ErrorMessage", "Security question is required");
			UCSecurityQuestionCustomValidator.ErrorMessage = SecurityQuestionCustomValidator.ErrorMessage = CodeLocalise("SecurityQuestionCustomValidator.ErrorMessage", "Security question is required");
			UCSecurityAnswerRequired.ErrorMessage = SecurityAnswerRequired.ErrorMessage = CodeLocalise("SecurityAnswerRequired.ErrorMessage", "Security answer is required");
			DateErrorMessage = CodeLocalise("Date.ErrorMessage", "Valid date is required");
			DateOfBirthRequiredMessage = CodeLocalise("DateOfBirthRequired.ErrorMessage", "Date of birth is required");
			DOBMinLimitErrorMessage = String.Format(CodeLocalise("DateOfBirth.ErrorMessage", "You must be at least {0} years of age."), App.Settings.MinimumCareerAgeThreshold);
			DOBMaxLimitErrorMessage = CodeLocalise("DateOfBirth.ErrorMessage", "You must not be more than 99 years of age.");
			AdditionalInformationFirstNameValidator.ErrorMessage = CodeLocalise("AdditionalInformationFirstNameValidator.ErrorMessage", "First name is required");
			AdditionalInformationLastNameValidator.ErrorMessage = CodeLocalise("AdditionalInformationFirstNameValidator.ErrorMessage", "Last name is required");
			AdditionalInformationZipValidator.ErrorMessage = CodeLocalise("AdditionalInformationZipValidator.ErrorMessage", "Zip code is required");
			AdditionalInformationZipRegexValidator.ErrorMessage = CodeLocalise("AdditionalInformationZipRegexValidator.ErrorMessage", "ZIP code must be in a 5 or 9-digit format");
			AdditionalInformationAddressValidator.ErrorMessage = CodeLocalise("AdditionalInformationAddressValidator.ErrorMessage", "Address is required");
			AdditionalInformationCityValidator.ErrorMessage = CodeLocalise("AdditionalInformationCityValidator.ErrorMessage", "City is required");
			AdditionalInformationStateValidator.ErrorMessage = CodeLocalise("AdditionalInformationStateValidator.ErrorMessage", "State is required");
			RegisterButton.Text = CodeLocalise("RegisterButton.Text", "Register");
			LogInButton.Text = CodeLocalise("LogInButton.Text", @"Sign in " + (char)187);
			SearchJobWithoutReg.Text = CodeLocalise("lightboxOptions2.Label", "Search for jobs without registering");
			SearchNoLogin.Text = CodeLocalise("lightboxOptions2.Label", "Search for jobs without registering");
			btnMigratedCancel.Text = SWSCancelButton.Text = CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
			btnBackUserConversion.Text = CodeLocalise("Global.Back.Text", "Back");
			btnUpdateMyAccount.Text = CodeLocalise("UpdateAccount.Text", "Update my account");
			panMigratedAgreeToTerms.Text = SWSAgreeToTermsButton.Text = AgreeToTermsButton.Text = CodeLocalise("AgreeToTermsButton.Text", "I agree to these terms");
			PhoneNumberRequiredValidator.ErrorMessage = CodeLocalise("PhoneNumberRequiredValidator.ErrorMessage", "Phone number is required");
			PhoneNumberValidator.ErrorMessage = CodeLocalise("PhoneNumber.ErrorMessage", "U.S. phone number must be 10 digits");
			ConsentAgreeToTermsButton.Text = panMigratedAgreeToTerms.Text = SWSAgreeToTermsButton.Text = AgreeToTermsButton.Text = CodeLocalise("AgreeToTermsButton.Text", "I agree to these terms");
			CancelAndLogoutButton.Text = btnMigratedCancel.Text = SWSCancelButton.Text = CancelButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");

			SeekerAccountTypeRequired.ErrorMessage = CodeLocalise("SeekerAccountType.RequiredErrorMessage","Account type is required");

			//RecaptchaValidator.ErrorMessage = CodeLocalise("RecaptchaValidator.RequiredErrorMessage", "Enter what you see in order to continue");
			RecaptchaErrorMessage.Text = CodeLocalise("RecaptchaErrorMessage.Text", "What you have entered is incorrect, try again");
		}

		/// <summary>
		/// Resets the forms.
		/// </summary>
		private void ResetForms()
		{
			// Login
			UserNameTextBox.Text = "";
			ScreenNameTextbox.Text = "";

			// Registration
			EmailAddressTextbox.Text = "";
			ConfirmRegEmailAddressTextbox.Text = "";
			SocialSecurityNumberPart1TextBox.Text = "";
			SocialSecurityNumberPart2TextBox.Text = "";
			SocialSecurityNumberPart3TextBox.Text = "";
			ConfirmSocialSecurityNumberPart1TextBox.Text = "";
			ConfirmSocialSecurityNumberPart2TextBox.Text = "";
			ConfirmSocialSecurityNumberPart3TextBox.Text = "";
			SecurityQuestionDropDown.SelectedIndex = 0;
      SeekerAccountTypeDropDownList.SelectedIndex = 0;
			SecurityQuestionTextBox.Text = "";
			SecurityAnswerTextBox.Text = "";
			AdditionalInformationFirstNameTextBox.Text = "";
			AdditionalInformationMiddleInitialTextBox.Text = "";
			AdditionalInformationSuffixDropDown.SelectedIndex = -1;
			AdditionalInformationLastNameTextBox.Text = "";
			AdditionalInformationDateOfBirthTextBox.Text = "";
			AdditionalInformationPhoneNumberTextBox.Text = "";
			AdditionalInformationAddressTextBox.Text = "";
			AdditionalInformationCityTextBox.Text = "";
			AdditionalInformationZipTextBox.Text = "";

      AdditionalInformationFirstNameTextBox.Enabled =
        AdditionalInformationLastNameTextBox.Enabled =
        AdditionalInformationMiddleInitialTextBox.Enabled =
        AdditionalInformationZipTextBox.Enabled =
        AdditionalInformationAddressTextBox.Enabled =
        AdditionalInformationCityTextBox.Enabled =
        AdditionalInformationPhoneNumberTextBox.Enabled = 
        AdditionalInformationSuffixDropDown.Enabled =
        SocialSecurityNumberPart1TextBox.Enabled =
        ConfirmSocialSecurityNumberPart1TextBox.Enabled =
        SocialSecurityNumberPart2TextBox.Enabled =
        ConfirmSocialSecurityNumberPart2TextBox.Enabled =
        SocialSecurityNumberPart3TextBox.Enabled =
        ConfirmSocialSecurityNumberPart3TextBox.Enabled =
        AdditionalInformationStateDropdown.Enabled = 
        AdditionalInformationDateOfBirthTextBox.Enabled = true;

			// Education registration
			CellphoneProviderDropDown.SelectedIndex = -1;

			var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Key == App.Settings.DefaultStateKey);
			if (lookup.IsNotNull())
				AdditionalInformationStateDropdown.SelectValue(lookup.Id.ToString());

			RegisterStep1Panel.Visible = panRegisterStep1Heading.Visible = panRegisterStep1.Visible = panRegisterStep1Button.Visible = true;
			RegisterStep2Panel.Visible = panRegisterStep2Heading.Visible = false;

			//User Conversion
			UCOldUsernameTextbox.Text = "";
			UCOldPassword.Text = "";
			UCEmailAddressTextbox.Text = "";
			UCConfirmEmailAddressTextbox.Text = "";
			UCNewPasswordTextBox.Text = "";
			UCConfirmNewPasswordTextBox.Text = "";
			UCSecurityQuestionDropDown.SelectedIndex = 0;
			UCSecurityQuestionTextBox.Text = "";
			UCSecurityAnswerTextBox.Text = "";
		}

		/// <summary>
		/// Logs the out.
		/// </summary>
		private void LogOut()
		{
			RegisterModal.Hide();
			var logout = (LogOut)MasterPage.FindControl("MainLogOut");
			logout.LogOutSession();
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{

		}

		public class LoggedInEventArgs : EventArgs
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="LoggedInEventArgs" /> class.
			/// </summary>
			/// <param name="screenName">Name of the screen.</param>
			/// <param name="sourceCommandName">Name of the source command.</param>
			/// <param name="isShadowingUser">Whether this is an Assist user shadowing the career user</param>
			public LoggedInEventArgs(string screenName, string sourceCommandName, bool isShadowingUser)
			{
				ScreenName = screenName;
				SourceCommandName = sourceCommandName;
				IsShadowingUser = isShadowingUser;
			}

			public string ScreenName { get; private set; }
			public string SourceCommandName { get; private set; }
			public bool IsShadowingUser { get; private set; }
		}
	}

	#region Helper class
	[Serializable]
	public class UserConversionDetail
	{
		public string OldUsername { get; set; }
		public string OldPassword { get; set; }
		public string NewUsername { get; set; }
		public string NewPassword { get; set; }
		public string SecurityQuestion { get; set; }
		public long? SecurityQuestionId { get; set; }
		public string SecurityAnswer { get; set; }
		public long? ProgramAreaId { get; set; }
		public long? DegreeId { get; set; }
		public SchoolStatus? EnrollmentStatus { get; set; }
		public string CellphoneNumber { get; set; }
		public int? CellphoneProviderId { get; set; }
	}

	#endregion
}
