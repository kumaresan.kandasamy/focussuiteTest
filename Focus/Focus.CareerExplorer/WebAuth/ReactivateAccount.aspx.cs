﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebAuth
{
  public partial class ReactivateAccount : PageBase
  {
		protected void Page_Load(object sender, EventArgs e)
		{
      Page.Title = CodeLocalise("Page.Title", "Reactivate Account");
			if (!((ViewState["DoneFirstLoad"] as bool?) ?? false))
			{
				ViewState["DoneFirstLoad"] = true;
				AccountReactivation.Show();
			}
		}

		/// <summary>
		/// Handles the OnSubmitted event of the AccountReactivation control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		protected void AccountReactivation_OnSubmitted(object sender)
		{
			ServiceClientLocator.CandidateClient(App).ResolveCandidateIssues(App.User.PersonId ?? 0, new List<CandidateIssueType>() { CandidateIssueType.NotLoggingIn }, true);

			Response.Redirect(UrlBuilder.Home());
		}
  }
}