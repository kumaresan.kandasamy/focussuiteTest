﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SSOError.aspx.cs" Inherits="Focus.CareerExplorer.WebAuth.SSOError" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
	    <h2>There has been an error whilst logging in</h2>
			<p>Please contact your system administrator</p>
			<p>Error message:
			<asp:Label runat="server" ID="ErrorMessage"></asp:Label>
			</p>
			<p>
				<asp:LinkButton OnClick="RetryClick" runat="server">Click here to retry</asp:LinkButton>
			</p>
    </div>
    </form>
</body>
</html>
