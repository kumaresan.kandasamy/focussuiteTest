﻿<%@ Page Title="Job search" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
	CodeBehind="JobSearch.aspx.cs" Inherits="Focus.CareerExplorer.WebCareer.JobSearch" %>

<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="../Controls/ResumeSearchLink.ascx" TagName="ResumeSearchLink" TagPrefix="uc" %>
<%@ Register Src="Controls/JobSearch/JobSearchStartPage.ascx" TagName="JobSearchStartPage"
	TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel ID="UpdatePanelJobSearch" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<asp:Panel ID="Panel1" runat="server">
				<div class="FocusCareer">
					<div><uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" /></div>
					<div><uc:JobSearchStartPage ID="JobSearchStartPage" runat="server" /></div>
				</div>
			</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
