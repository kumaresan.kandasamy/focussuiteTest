﻿<%@ Page Title="Your resume" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="YourResume.aspx.cs" Inherits="Focus.CareerExplorer.WebCareer.YourResume" %>

<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="../Controls/ResumeSearchLink.ascx" TagName="ResumeSearchLink" TagPrefix="uc" %>
<%@ Register Src="Controls/JobSearch/CreateEditResume.ascx" TagName="CreateEditResume" TagPrefix="uc" %>
<%@ Register Src="Controls/JobSearch/UploadResume.ascx" TagName="UploadResume" TagPrefix="uc" %>
<%@ Register Src="Controls/JobSearch/PasteTypeResume.ascx" TagName="PasteTypeResume" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/ResumeMissingInfo.ascx" TagName="ResumeMissingInfo" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ConfirmationModal.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/MyResumes.ascx" TagName="MyResumes" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/ResumeList.ascx" TagName="ResumeList" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel ID="UpdatePanelYourResume" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<div class="bootstrapped">
				<uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" />
				<h3 class="sr-only"><asp:Label ID="lblResumeLimitReached" runat="server" ForeColor="#FF0000"/>ResumeLimitReached</h3>
				<h1><focus:LocalisedLabel runat="server" ID="Heading1Label" RenderOuterSpan="False" LocalisationKey="Heading1.Label" DefaultText="Your resume" /></h1>
				<p><asp:Label runat="server" ID="ResumeInfoLabel" /></p>
				<asp:Panel runat="server" ID="SearchUsingThisResumePanel" CssClass="section-border-bottom section-border-top panel panel-minimal">
					<div class="panel-heading">
						<focus:LocalisedLabel DefaultText="Use this resume" runat="server" ID="UseThisResumeLocalLabel" LocalisationKey="UseThisResume.Label" RenderOuterSpan="False" />
					</div>
					<div class="panel-body form-inline">
						<asp:DropDownList ID="ResumeNamesDropDown" style="margin-bottom: 10px" runat="server" CssClass="form-control" Title="Resumes"/>
						<focus:ModernButton ID="SearchButton" style="margin-bottom: 10px" CssClass="btn btn-default space-right-lg" IconProvider="FontAwesome" IconCssClass="fa-search" runat="server" OnClick="SearchButtonCLicked" />
						<strong><focus:LocalisedLabel DefaultText="-OR-" runat="server" ID="OrLocalisedLabel" LocalisationKey="Or.Label" /></strong>
					</div>
				</asp:Panel>
				<div id="ResumeOptionsPanel">
					<div class="col-md-8">
						<div>
							<div class="col-md-6">
								<uc:CreateEditResume ID="CreateEditResume" runat="server" />
							</div>
							<div class="col-md-6">
								<uc:UploadResume ID="UploadResume" runat="server" />
							</div>
						</div>
						<asp:Panel ID="EditRow" runat="server">
							<div class="col-md-6">
								<uc:MyResumes ID="MyResumes" runat="server" />
							</div>
							<div class="col-md-6">
								<asp:Panel ID="EditResumesPanel" runat="server" CssClass="panel panel-lightbox">
									<div class="panel-heading">
										<focus:LocalisedLabel runat="server" ID="lblEditResume" LocalisationKey="MyResumes5.Label" DefaultText="Edit a resume" RenderOuterSpan="False" />
									</div>
									<div class="panel-body">
										<uc:ResumeList ID="MyResumeList" runat="server" TargetPage="ResumeWizard" CssClass="genericContainerContent" OnLinkNameClicked="MyResumeList_linkNameclick" />
									</div>
									<div class="panel-footer"><%--Added to ensure panel heights match when used in Your Resume page--%></div>
								</asp:Panel>
							</div>
						</asp:Panel>
					</div>
					<div class="col-md-4">
						<uc:PasteTypeResume ID="PasteTypeResume" runat="server" />
					</div>
				</div>
			</div>
			<uc:ResumeMissingInfo ID="ResumeMissingInfoPopup" runat="server" />
			<uc:ConfirmationModal ID="ConfirmationModal" runat="server" OnOkCommand="ConfirmationModal_OkCommand" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<script>
		$(document).ready(function () {
			function equalisePanels() {
			/// <summary>Equalises the height of the four short panels.</summary>
				$('#ResumeOptionsPanel .row .col-md-6 .panel-body').height('auto');
				if ($(window).width() > 992) { // desktop size in Bootstrap
					$('#ResumeOptionsPanel .row').each(function () {
						var bodyHeightsArray = [];
						$(this).find('.col-md-6 .panel-body').each(function () {
							bodyHeightsArray.push($(this).height());
						}).height(Math.max.apply(Math, bodyHeightsArray));
					});
				}
			}

			equalisePanels();

			$(window).on('resize', function() {
				equalisePanels();
			});
		});
	</script>
</asp:Content>
