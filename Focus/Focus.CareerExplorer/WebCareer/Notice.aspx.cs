﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Criteria.Document;
using Focus.Core.Models.Assist;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
	public partial class Notice : PageBase
	{
		#region Properties

		private List<GroupedDocumentsModel> _documentGroups { get; set; }

		private DocumentCriteria _criteria { get; set; }

		#endregion

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Page.RouteData.Values.ContainsKey("title"))
			{
				var title =  CodeLocalise("Page.Title", Page.RouteData.Values["title"].ToString());
				Page.Title = PageTitleLabel.DefaultText = title;
			}
				
			if (Page.RouteData.Values.ContainsKey("category"))
				_criteria = new DocumentCriteria{Category = Convert.ToInt64(Page.RouteData.Values["category"].ToString())};

			if (Page.RouteData.Values.ContainsKey("group"))
			{
				if (_criteria.IsNull())
					_criteria = new DocumentCriteria();

				_criteria.Group = Convert.ToInt64(Page.RouteData.Values["group"].ToString());
			}

			if (Page.RouteData.Values.ContainsKey("module"))
			{
				if (_criteria.IsNull())
					_criteria = new DocumentCriteria();

				_criteria.Module = (DocumentFocusModules)Enum.Parse(typeof(DocumentFocusModules), Page.RouteData.Values["module"].ToString());
			}
				
		  if (!IsPostBack)
		  {
			  BindList();
		  }
		}

		/// <summary>
		/// Binds the list.
		/// </summary>
		private void BindList()
		{
			if (_documentGroups.IsNullOrEmpty())
			{
				_documentGroups = ServiceClientLocator.CoreClient(App).GetGroupedDocuments(_criteria);
			}

			if (_documentGroups.IsNullOrEmpty()) return;

			DocumentGroupRepeater.DataSource = _documentGroups;
			DocumentGroupRepeater.DataBind();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the DocumentGroupRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RepeaterItemEventArgs" /> instance containing the event data.</param>
		protected void DocumentGroupRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var group = e.Item.DataItem as GroupedDocumentsModel;

				if (group.IsNotNull())
				{
					var groupLabel = e.Item.FindControl("GroupLabel") as Label;

					if (groupLabel.IsNotNull())
						groupLabel.Text = group.GroupName;

					if (group.IsNotNull())
					{
						var documentRepeater = e.Item.FindControl("DocumentRepeater") as Repeater;

						if (documentRepeater.IsNull()) return;

						documentRepeater.DataSource = group.Documents;
						documentRepeater.DataBind();
					}
				}
				
			}
		}

		/// <summary>
		/// Handles the ItemDataBound event of the DocumentRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RepeaterItemEventArgs" /> instance containing the event data.</param>
		protected void DocumentRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				var downloadButton = e.Item.FindControl("DownloadDocumentImageButton") as ImageButton;

				if (downloadButton.IsNotNull())
				{
					downloadButton.ToolTip = CodeLocalise("DownloadDocumentImageButton.ToolTip", "Download document");
					downloadButton.AlternateText = CodeLocalise("DownloadDocumentImageButton.AlternativeText", "Download");
				}

				var emailButton = e.Item.FindControl("EmailDocumentImageButton") as ImageButton;

				if (emailButton.IsNotNull())
				{
					emailButton.ToolTip = CodeLocalise("EmailDocumentImageButton.ToolTip", "Email document record");
					emailButton.AlternateText = CodeLocalise("EmailDocumentImageButton.AlternativeText", "Email");
				}
			}
		}

		/// <summary>
		/// Handles the ItemCommand event of the DocumentRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewCommandEventArgs" /> instance containing the event data.</param>
		protected void DocumentRepeater_ItemCommand(object sender, RepeaterCommandEventArgs e)
		{
			var documentId = Convert.ToInt64(e.CommandArgument);

			switch (e.CommandName)
			{
				case "EmailDocument":
					EmailDocumentModal.Show(documentId);
					break;
				case "DownloadDocument":
					var document = ServiceClientLocator.CoreClient(App).GetDocument(documentId);

					Response.Clear();
					Response.Buffer = true;
					Response.Charset = "";
					Response.Cache.SetCacheability(HttpCacheability.NoCache);
					Response.AddHeader("Content-type", document.MimeType);
					Response.AddHeader("content-disposition", "attachment;filename=" + document.FileName);
					Response.BinaryWrite(document.File);
					Response.Flush();
					Response.End();

					break;
			}
		}
	}
}