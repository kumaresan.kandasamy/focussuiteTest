﻿<%@ Page Title="Job search results" Language="C#" MasterPageFile="~/Site.Master"
	AutoEventWireup="True" CodeBehind="JobSearchResults.aspx.cs" Inherits="Focus.CareerExplorer.WebCareer.JobSearchResults" %>
<%@ Import Namespace="Focus.Core.Models.Career" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="../Controls/UnAuthorizedInformation.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>
<%@ Register Src="../Controls/ResumeSearchLink.ascx" TagName="ResumeSearchLink" TagPrefix="uc" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="SearchCriteriaErrorModal" Src="~/Controls/ConfirmationModal.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel ID="UpdatePanelJobSearchResults" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<div class="bootstrapped">
				<div class="row">
					<div class="col-md-12">
						<uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" />
					  <asp:PlaceHolder runat="server" ID="ExplorerReturnLinkPanel" Visible="False">
					    <p>
					      <asp:LinkButton ID="LnkReturnToExplorer" runat="server" OnClick="LnkReturnToExplorers_Click">
			            <%= HtmlLocalise("Promo33.LinkText", "&#171; #FOCUSEXPLORER#")%>
                </asp:LinkButton>
              </p>
            </asp:PlaceHolder>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-toolbox">
							<div class="form-inline">
							    <div><div class="col-sm-12 col-md-9 col-lg-8 clearfix">
								<div class="form-group">
									<%= HtmlInFieldLabel("SearchTermTextBox", "AddWordsTextBox.Label", "Search for these words", 200)%>
									<asp:TextBox ID="SearchTermTextBox" runat="server" Width="210px" ClientIDMode="Static" CssClass="form-control"/>
								</div>
								<div class="form-group">
                                    <%=HtmlLabel(ddlSearchLocation,"in.Label","in")%>
									<asp:DropDownList ID="ddlSearchLocation" runat="server" ClientIDMode="Static" CssClass="form-control">
										<asp:ListItem Text="complete job posting" />
									</asp:DropDownList>
								</div>
								<div class="form-group">
                                    <%=HtmlLabel(ddlRadius,"Within.Label","Within") %>
									<asp:DropDownList ID="ddlRadius" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
								</div>
								<div class="form-group">
                                    <%=HtmlLabel(txtZipCode,"of.Label","of")%>
									<asp:TextBox ID="txtZipCode" runat="server" ClientIDMode="Static" Width="74px" MaxLength="5" CssClass="form-control" />
									<ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="txtZipCode"
										FilterType="Numbers" />
								</div>
							</div>
                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-4 clearfix">
								<div class="form-group" id="searchFilterDiv"><asp:LinkButton ID="btnGO" runat="server" CssClass="btn btn-default" OnClick="btnGO_Click" ValidationGroup="Location">
									<span aria-hidden="true" class="fa fa-filter"></span> <%= HtmlLocalise("Filter.Label", "Filter")%>
								</asp:LinkButton>
								<asp:LinkButton ID="btnShow" runat="server" CssClass="btn btn-default" Width="75px" OnClick="lnkReviewSearchCriteria_Click">
									Show <span aria-hidden="true" class="fa fa-caret-down"></span>
								</asp:LinkButton></div>
								<asp:PlaceHolder runat="server" ID="SearchAndHintsPlaceHolder">
										<div class="form-group" style="display: inline-block;">
											<asp:LinkButton ID="btnSaveNotify" runat="server" CssClass="btn btn-info" OnClick="LnkSaveSearch_Click">
												<%= HtmlLocalise("SaveNotify.Label", "Save &amp; Notify")%>
											</asp:LinkButton>
											<div class="btn-group">
												<asp:LinkButton ID="btnHints" runat="server" CssClass="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span aria-hidden="true" class="fa fa-search"></span> Hints
													<span class="fa fa-caret-down"></span>
													<span class="sr-only">	<%= HtmlLocalise("ToggleDropdown.Label", "Toggle Dropdown")%></span>
												</asp:LinkButton>
												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="#" onclick="ShowNotWhatYoureLookingFor()"><%= HtmlLocalise("Promo32.LinkText", "Don't see what you're looking for?")%></a></li>
													<li><asp:LinkButton ID="lnkWiden" runat="server" OnClick="LnkStatewideMatches_Click"><%= HtmlLocalise("Promo31.LinkText", "Widen your net: see matches statewide")%></asp:LinkButton></li>
												</ul>
											</div>
										</div>
									</asp:PlaceHolder></div></div>
							</div>
							<div class="form-group space-top-md">
								<asp:RadioButton runat="server" ID="rbSearchAllWords" GroupName="Operator"/>&nbsp;&nbsp;&nbsp;
								<asp:RadioButton runat="server" ID="rbSearchAnyWords" GroupName="Operator"/>
							</div>
							<asp:CustomValidator ID="RadiusValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
										CssClass="error" ValidationGroup="Location" ClientValidationFunction="ValidateRadius"
										ValidateEmptyText="true" />
							<asp:Panel ID="pnlReviewSearchCriteriaInfo" runat="server" CssClass="well" Visible="False">
								<p><asp:Literal ID="lblReviewSearchCriteriaInfo" runat="server"></asp:Literal></p>
								<asp:Button ID="ChangeCriteriaButton" runat="server" class="btn btn-info btn-sm" OnClick="ChangeCriteriaButton_Click" />
							</asp:Panel>
						</div>
					</div>
				</div>
			</div>
			<div class="FocusCareer">
                <div class="horizontalRule">
                </div>
				<focus:LocalisedLabel runat="server" ID="SpideredJobsDisclaimerLabel" RenderOuterSpan="True" CssClass="information-panel" />
				<asp:UpdatePanel ID="SearchResultUpdatePanel" runat="server" UpdateMode="Conditional">
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="btnGO" EventName="Click" />
					</Triggers>
					<ContentTemplate>
						<asp:Button runat="server" ID="NotAuthenticatedJobButton" OnClick="NotAuthenticatedJobButton_OnClick" style="display:none"/>
						<asp:ListView ID="SearchResultListView" runat="server" ItemPlaceholderID="SearchResultPlaceHolder"
							DataSourceID="SearchResultDataSource" OnSorting="SearchResultListView_Sorting"
							OnItemDataBound="SearchResultListView_ItemDataBound" OnPagePropertiesChanging="SearchResultListView_PagePropertiesChanging" OnLayoutCreated="SearchResultListView_LayoutCreated">
							<LayoutTemplate>
							    <div style="overflow-x: scroll">
								<table width="100%" class="genericTable withHeader">
									<tr>
										<th width="370px">
											<asp:LinkButton ID="JobTitleSortButton" runat="server" CommandName="Sort" CommandArgument="JobTitle"><%= HtmlLocalise("JobTitle.ColumnTitle", "JOB TITLE")%></asp:LinkButton>
											<asp:Image ID="JobTitleSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Job Title Sort Image" />
										</th>
										<th width="200px">
											<asp:LinkButton ID="EmployerSortButton" runat="server" CommandName="Sort" CommandArgument="Employer"><%= HtmlLocalise("Employer.ColumnTitle", "EMPLOYER")%></asp:LinkButton>
											<asp:Image ID="EmployerSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Employer Sort Image" />
										</th>
										<th width="150px">
											<asp:LinkButton ID="JobLocationSortButton" runat="server" CommandName="Sort" CommandArgument="JobLocation"><%= HtmlLocalise("JobLocation.ColumnTitle", "JOB LOCATION")%></asp:LinkButton>
											<asp:Image ID="JobLocationSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Job Location Sort Image"/>
										</th>
										<th width="110px">
											<asp:LinkButton ID="DateSortButton" runat="server" CommandName="Sort" CommandArgument="Date"><%= HtmlLocalise("Date.ColumnTitle", "DATE")%></asp:LinkButton>
											<asp:Image ID="DateSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Date Sort Image"/>
										</th>
										<th width="90px">
											<asp:LinkButton ID="RatingSortButton" runat="server" CommandName="Sort" CommandArgument="Rating"><%= HtmlLocalise("Rating.ColumnTitle", "RATING")%></asp:LinkButton>
											<asp:Image ID="RatingSortImage" runat="server" ImageUrl="<%# UrlBuilder.SortDown() %>" Visible="False" AlternateText="Rating Sort Image"/>
											<span class="fa fa-info-circle" data-toggle="tooltip" data-placement="bottom" id="RatingToolTip" runat="server"></span>
										</th>
										<th width="175px" id="YrsOfExperienceHeader" runat="server">
											<%= HtmlLocalise("YrsOfExperience.ColumnTitle", "YRS OF EXPERIENCE")%>
										</th>
										<th id="Th1" width="75px" class="table-icon" runat="server">
											<%= HtmlLocalise("MoreLikeThis.ColumnTitle", "MORE <br/> LIKE THIS")%>
										</th>
										<th id="Th2" width="75px" class="table-icon" runat="server">
											<%= HtmlLocalise("HideThis.ColumnTitle", "HIDE <br/> THIS JOB")%>
										</th>
										<th id="Th3" width="75px" class="table-icon" runat="server">
											<%= HtmlLocalise("AMIAMatch.ColumnTitle", "AM I A <br/> MATCH?")%>
										</th>
									</tr>
									<asp:PlaceHolder ID="SearchResultPlaceHolder" runat="server" />
								</table></div>
							</LayoutTemplate>
							<ItemTemplate>
								<tr id="JobSearchResultRow" runat="server">
									<td>
										<asp:HyperLink ID="JobTitleHyperLink" runat="server" onclick="javascript:showProcessing();" CssClass="jobSearchResultTitleLink"><%#((PostingSearchResultView)Container.DataItem).JobTitle%></asp:HyperLink>
										<asp:Image ID="NonSpideredJobIcon" runat="server" Visible="false" ImageUrl="<%# UrlBuilder.NonSpideredJobIconSmall() %>" CssClass="tooltipImage toolTipNew" AlternateText="Non Spidered Job Icon" /><br/>
										<asp:Label runat="server" ID="JobDescriptionLabel"></asp:Label>
									</td>
									<td>
										<%# ((PostingSearchResultView)Container.DataItem).Employer.Replace("UNKNOWN","-")%>
										<br/>
										<asp:Label runat="server" ID="TalentAccountTypeLabel"></asp:Label>
									</td>
									<td>
										<%#((PostingSearchResultView)Container.DataItem).Location%>
									</td>
									<td>
										<%#((PostingSearchResultView)Container.DataItem).JobDate.ToString("MMM dd, yyyy")%>
									</td>
									<td>
										<%# GetStars(((PostingSearchResultView)Container.DataItem).Rank)%>
									</td>
									<td runat="server" id="YearsOfExperienceCell">
										<asp:Label runat="server" ID="YearsOfExperienceLabel"></asp:Label>
									</td>
									<td class="table-icon">
                                        <asp:PlaceHolder runat="server" ID="FindMoreJobsPlaceholder">
										    <asp:LinkButton ID="FindMoreJobsHyperLink" runat="server" OnClick="FindMoreJobsHyperLink_Click">
												<span class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="bottom" title="Find more jobs like this. Selecting this action results in a new search."></span>
											</asp:LinkButton>
                                        </asp:PlaceHolder>
									</td>
									<td class="table-icon">
                                        <asp:PlaceHolder runat="server" ID="DoNotDisplayPlaceholder">
										    <asp:LinkButton ID="DoNotDisplayHyperLink" runat="server" OnClick="DoNotDisplayHyperLink_Click">
											    <span class="fa fa-thumbs-down" data-toggle="tooltip" data-placement="bottom" title="Remove this job from these results. This job will also not appear in any other job searches you perform."></span>
										    </asp:LinkButton>
                                        </asp:PlaceHolder>	
									</td>
									<td class="table-icon">
										<asp:PlaceHolder runat="server" ID="AmIAGoodMatchPlaceholder">
											<asp:LinkButton ID="AmIGoodMatchHyperLink" runat="server" OnClick="AmIGoodMatchHyperLink_Click">
											<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="bottom" title="See how well this job matches your skills and experience."></span>
											</asp:LinkButton>
										</asp:PlaceHolder>	
									</td>
								</tr>
							</ItemTemplate>
							<EmptyDataTemplate>
								<table>
									<tr>
										<td colspan="6">
											<b>
												<%= HtmlLocalise("NoResults.Text", "No matching jobs found; please refine your search")%></b>
										</td>
									</tr>
								</table>
							</EmptyDataTemplate>
						</asp:ListView>
						<div class="Pager">
									<uc:Pager ID="BottomPager" runat="server" PagedControlID="SearchResultListView" ClientIDMode="Static" />
							</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
			<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
			<uc:SearchCriteriaErrorModal ID="SearchCriteriaErrorModal" runat="server" />
			<asp:ObjectDataSource ID="SearchResultDataSource" runat="server" TypeName="Focus.CareerExplorer.WebCareer.JobSearchResults"
				EnablePaging="True" SelectMethod="GetSearchResults" SelectCountMethod="GetSearchResultCount"
				SortParameterName="orderBy" />
			<asp:HiddenField ID="WhatYouRLookingForDummyTarget" runat="server" />
			<act:ModalPopupExtender ID="WhatYouRLookingForModal" runat="server" BehaviorID="WhatYouRLookingForModal"
				TargetControlID="WhatYouRLookingForDummyTarget" PopupControlID="WhatYouRLookingForModalPanel"
				BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
			<div id="WhatYouRLookingForModalPanel" class="modal" style="z-index: 100001; display: none;">
				<div class="lightboxmodal lightbox FocusCareer bootstrapped" id="WhatYouRLookingForLightbox">
					<div class="modalHeader">
						<div class="pull-right">
							<button type="button" title="Close What you are looking for modal" class="btn btn-close-icon" onclick="javascript:$find('WhatYouRLookingForModal').hide();return false;"><span class="fa fa-times-circle"></span></button>
						</div>
						<h3 class="modalTitle modalTitleLarge"><%= HtmlLocalise("Heading1.Label", "Don't see what you're looking for?")%></h3>
					</div>
					<p><%= HtmlLocalise("Content1.Label", "Here's how to fix the most common problems people have with their search results.")%></p>
                    <div class="career-scroll-pane" style="height:400px">
					<h4><%= HtmlLocalise("Heading2.Label", "Job results are in the wrong location")%></h4>
					<div class="row">
						<div class="col-sm-3"><%= HtmlLocalise("Content2.Label", "Location:")%></div>
						<div class="col-sm-9">
							<div><asp:Literal ID="lblLocation" runat="server" /></div>
							<div><%= HtmlLocalise("Content2-2.Label", "<a href='" + UrlBuilder.JobSearchCriteria() + "'>Change the location for this search only</a>")%></div>
							<div id="divLocation" style="display: none;">
								<a id="A4" runat="server" href="<%$RouteUrl:Tab=preferences,routename=ResumeWizardTab %>">
									<%= HtmlLocalise("Content2-3.Label", "Change the location in your resume preferences")%></a><%=CodeLocalise("Willaffect.Label"," (will affect all searches)") %>
							</div>
						</div>
					</div>
					<h4><%= HtmlLocalise("Heading3.Label", "Not the type of job you're looking for")%></h4>
					<div class="row">
						<div class="col-sm-3"><asp:Literal runat="server" id="JobTypeSubHeading"></asp:Literal></div>
						<div class="col-sm-9">
							<div>
								<asp:Label ID="lblIndustry" runat="server" />
							</div>
							<div runat="server" ID="JobTypeSearchHyperDiv">
								<asp:HyperLink runat="server" ID="JobTypeSearchHyperLink"></asp:HyperLink>
              </div>
							<div runat="server" ID="InternshipAreaSearchDiv">
								<asp:HyperLink runat="server" ID="InternshipAreaSearchHyperlink"></asp:HyperLink>
              </div>
							<div>
								<%= HtmlLocalise("Content3-3.Label", "<a href='" + UrlBuilder.JobSearchCriteria() + "'>Use keywords to find a specific job title</a>")%></div>
							<div>
								<asp:HyperLink runat="server" ID="ExcludeJobHyperlink"></asp:HyperLink>
								<div id="divJobHistory" style="display: none;">
									<a id="A1" runat="server" href="<%$RouteUrl:Tab=workhistory,SubTab=addajob,routename=ResumeWizardSubTab %>">
										<%= HtmlLocalise("Content3-5.Label", "Update your job history in your resume")%>
									</a>
									<%=CodeLocalise("Willaffect.Label"," (will affect all searches)") %>
								</div>
								<div id="divIgnoreResume" style="display: none;">
									<%= HtmlLocalise("Content3-6.Label", "<a href='" + UrlBuilder.JobSearchCriteria("IgnoreResume") + "'>Ignore your resume when searching</a>")%>
								</div>
							</div>
						</div>
					</div>
					<asp:PlaceHolder runat="server" ID="JobResultsLevelPlaceHolder">
					<h4><%= HtmlLocalise("Heading4.Label", "Job results aren't at the right level")%> <small><%= HtmlLocalise("Heading4-1.Label", "(e.g., higher/lower education level, licenses/certificates not taken into account, too senior/junior, wrong salary level)")%></small></h4>
					<div class="row">
						<div class="col-sm-3"><%= HtmlLocalise("Content4.Label", "Experience and education level:")%></div>
						<div class="col-sm-9">
							<div>
								<asp:Label ID="lblEducation" runat="server" />
							</div>
							<div>
								<asp:HyperLink runat="server" ID="EducationSearchHyperlink"></asp:HyperLink>
							</div>
							<div>
								<%= HtmlLocalise("Content4-3.Label", "<a href='" + UrlBuilder.JobSearchCriteria() + "'>Use keywords to find a specific qualification</a>")%></div>
							<div id="divJobHistory2" style="display: none;">
								<a id="A2" runat="server" href="<%$RouteUrl:Tab=workhistory,SubTab=addajob,routename=ResumeWizardSubTab %>">
									<%= HtmlLocalise("Content4-4.Label", "Update your job history in your resume") %>
								</a>
								<%=CodeLocalise("Willaffect.Label"," (will affect all searches)") %>
							</div>
							<div id="divEducation" style="display: none;">
								<a id="A3" runat="server" href="<%$RouteUrl:Tab=education,routename=ResumeWizardTab %>">
									<%= HtmlLocalise("Content4-5.Label", "Update your education in your resume")%></a><%=CodeLocalise("Willaffect.Label"," (will affect all searches)") %>
							</div>
							<div>
								<asp:HyperLink runat="server" ID="SalarySearchHyperLink"></asp:HyperLink>
							</div>
						</div>
					</div>
					</asp:PlaceHolder>
					<h4><%= HtmlLocalise("Heading5.Label", "Job results are out-of-date")%></h4>
					<div class="row">
						<div class="col-sm-3"><%= HtmlLocalise("Content5.Label", "Job posting date:")%></div>
						<div class="col-sm-9">
							<div><asp:Label ID="lblPostingDate" runat="server" /></div>
							<div><%= HtmlLocalise("Content5-2.Label", "<a href='" + UrlBuilder.JobSearchCriteria("PostingDate") + "'>Limit the posting date for this search only</a>")%></div>
						</div>
					</div>

				</div>
              </div>
					<div class="text-right">
						<asp:Button ID="Button2" Text="Cancel" runat="server" class="btn btn-info" OnClientClick="javascript:$find('WhatYouRLookingForModal').hide(); return false;" />
					</div>			</div>
			<asp:HiddenField ID="SaveSearchDummyTarget" runat="server" />
			<act:ModalPopupExtender ID="SaveSearchModal" runat="server" BehaviorID="SaveSearchModal"
				TargetControlID="SaveSearchDummyTarget" PopupControlID="SaveSearchModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
				<Animations>
								<OnShown>
                     <ScriptAction Script="ResetSaveSearchForm();" />  
                </OnShown>
				</Animations>
			</act:ModalPopupExtender>
			<div id="SaveSearchModalPanel" tabindex="-1" class="modal" style="z-index: 100001; display: none;width: 700px;max-width: 90%">
				<div class="lightbox bootstrapped" id="SaveSearchLightBox">
					<div class="modalHeader">
						<div class="pull-right">
							<button type="button" title="Close save search modal" class="btn btn-close-icon" onclick="javascript:$find('SaveSearchModal').hide(); return false;"><span class="fa fa-times-circle"></span></button>
						</div>
						<h3 class="modalTitle modalTitleLarge"><%= HtmlLocalise("Heading1.Label", "Save search")%></h3>
					</div>
					<div class="form-horizontal career-scroll-pane">
						<div class="form-group">
							<focus:LocalisedLabel runat="server" ID="SearchNameLabel" AssociatedControlID="txtSearchName" LocalisationKey="Label1.Label" DefaultText="Search name" CssClass="col-sm-3 control-label requiredData"/>
							<div class="col-sm-9">
								<asp:TextBox ID="txtSearchName" runat="server" MaxLength="30" CssClass="form-control" ClientIDMode="Static" />
								<asp:RequiredFieldValidator ID="SearchNameRequired" runat="server" ControlToValidate="txtSearchName"
									SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="SaveSearch" />
							</div>
						</div>
						<div class="form-group">
                            <focus:LocalisedLabel runat="server" ID="SearchCriteriaLabel" AssociatedControlID="lblSearchCriteria" LocalisationKey="SearchCriteria.Label" DefaultText="Search criteria" CssClass="col-sm-3 control-label" />
							<div class="col-sm-9">
								<p class="form-control-static"><asp:Label ID="lblSearchCriteria" runat="server" Text="" /></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
								<div>
									<div class="checkbox">
										<label>
											<asp:CheckBox runat="server" ID="EmailSavedSearchCheckbox" onClick="EmailAlertSelected(this)" /> 
											<asp:Literal ID="EmailSavedSearchLabel" runat="server" />
										</label>
									</div>
								</div>
								<div class="form-inline">
									<asp:TextBox ID="txtEmailAddress" aria-label="Email Address" runat="server" MaxLength="100" ClientIDMode="Static" Enabled="False" CssClass="form-control" />
									<asp:DropDownList ID="ddlAlertFrequency" aria-label="Alert Frequency" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="False" Width="100px"></asp:DropDownList>
									<label for="ddlAlertType">as</label>
									<asp:DropDownList ID="ddlAlertType" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="False" Width="100px"></asp:DropDownList>
								</div>
								<div>
									<asp:RegularExpressionValidator ID="valEmailAddress" runat="server" ControlToValidate="txtEmailAddress"
										CssClass="error" Display="Dynamic" SetFocusOnError="True" ValidationExpression="^([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"
										ValidationGroup="SaveSearch"></asp:RegularExpressionValidator>
                  <asp:CustomValidator runat="server" ID="customValidateEmailAddress" ControlToValidate="txtEmailAddress"
                    CssClass="error" Display="Dynamic" SetFocusOnError="True" ClientValidationFunction="ValidateAlertEmail"
                    ValidationGroup="SaveSearch" ValidateEmptyText="True"></asp:CustomValidator>
								</div>
							</div>
						</div>
					</div>
                    <div class="text-right">
						<asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="btn btn-default" OnClientClick="javascript:$find('SaveSearchModal').hide(); return false;" />
						<asp:Button ID="btnSaveSearch" Text="Save" runat="server" CssClass="btn btn-primary" ValidationGroup="SaveSearch" OnClick="btnSaveSearch_Click" />
					</div>
                </div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
	<script type="text/javascript">
		function pageLoad() {
			var completion = "<%= CompletionStatus %>";
			var hasResume = "<%= HasResume %>";
			if (completion >= 1) {
				$('#divJobHistory').show();
				$('#divJobHistory2').show();
			}
			if (completion >= 4)
				$('#divEducation').show();
			if (completion >= 64) {
				$('#divLocation').show();
				if (hasResume == 1)
					$('#divIgnoreResume').show();
			}
		}

		$(function() {
			$('[data-toggle="tooltip"]').tooltip();
		});

		Sys.Application.add_load(SaveSearch_PageLoad);

		function SaveSearch_PageLoad() {
			// Jaws Compatibility
			var modal = $find('SaveSearchModal');
			if (modal != null) {
				modal.add_shown(function() {
					$('#SaveSearchModalPanel').attr('aria-hidden', false);
					$('.page').attr('aria-hidden', true);
					$('#txtSearchName').focus();
				});

				modal.add_hiding(function() {
					$('#SaveSearchModalPanel').attr('aria-hidden', true);
					$('.page').attr('aria-hidden', false);
				});
			}
		}

		function ValidateRadius(sender, args) {
			var ddlRadius = document.getElementById("ddlRadius");
			var Radius = ddlRadius.options[ddlRadius.selectedIndex].value;
			var txtZipCode = document.getElementById("txtZipCode");
			if (!((Radius == "" && txtZipCode.value == "") || (Radius != "" && txtZipCode.value.trim().length == 5))) {
				args.IsValid = false;
			}
		}

		function ResetSaveSearchForm() {
		  $("#txtSearchName").val("");
		  $("#txtEmailAddress").val("");
		  $("#<%=EmailSavedSearchCheckbox.ClientID%>").prop("checked", false);

			$("#ddlAlertFrequency").get(0).selectedIndex = 0;
			$("#ddlAlertType").get(0).selectedIndex = 0;
		}

		function EmailAlertSelected(sender) 
		{
		  var frequency = document.getElementById("<%=ddlAlertFrequency.ClientID %>");
		  var format = document.getElementById("<%=ddlAlertType.ClientID %>");
		  var email = document.getElementById("<%=txtEmailAddress.ClientID %>");

		  frequency.disabled = format.disabled = email.disabled = !sender.checked;

		  if (!sender.checked) 
		  {
		    document.getElementById("<%=customValidateEmailAddress.ClientID %>").style.display = 'none';
		    email.value = "";
		  }
		}

		function ValidateAlertEmail(sender, args) 
		{
		  var email = document.getElementById("<%=txtEmailAddress.ClientID %>");
		  var emailRequired = document.getElementById("<%=EmailSavedSearchCheckbox.ClientID %>");

		  if (email.value == "") 
		  {
		    if (emailRequired.checked) 
		    {
		      //sender.innerHTML = "<%= AlertEmailRequired %>";
		      args.IsValid = false;
		    }
		  }
		}
	</script>
</asp:Content>
