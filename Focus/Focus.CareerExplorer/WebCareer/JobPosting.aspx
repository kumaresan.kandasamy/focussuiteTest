﻿<%@ Page Title="Job posting" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True"
	CodeBehind="JobPosting.aspx.cs" Inherits="Focus.CareerExplorer.WebCareer.JobPosting" %>

<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="../Controls/UnAuthorizedInformation.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>
<%@ Register Src="../Controls/ResumeSearchLink.ascx" TagName="ResumeSearchLink" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ConfirmationModal.ascx" TagName="ConfirmationBookMarkModal" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" />
	<asp:UpdatePanel ID="UpdatePanelJobPosting" runat="server" UpdateMode="Always">
		<ContentTemplate>
				<div>
					<asp:HyperLink ID="BackHyperLink" runat="server" onclick="javascript:showProcessing();">
					     <focus:LocalisedLabel runat="server" ID="JobResultsLabel" RenderOuterSpan="False" LocalisationKey="JobResults.Label" DefaultText="&#171; Job results"/>
					</asp:HyperLink>
				</div>
				<div class="jobPostingSummary">
					<div class="jobPostingSummaryMain">
						<h1><asp:Label ID="lblPostingTitle" runat="server"></asp:Label></h1>
						<asp:PlaceHolder runat="server" ID="EmployerNamePlaceHolder" Visible="False">
							<asp:Label ID="EmployerNameLabel" runat="server"></asp:Label><br />
						</asp:PlaceHolder>
						<p>
							<asp:Label ID="lblCustomerJobID" runat="server"></asp:Label><br/>
							<asp:Label ID="lblJobID" runat="server" />
						</p>
						<asp:Button ID="btnHowToApply" runat="server" class="buttonLevel2" OnClick="BtnHowToApply_Click" Style="margin-left: 0;" ClientIDMode="Static" />
            <asp:PlaceHolder runat="server" id="NoApplyHolder" Visible="False">
              <p>
                <asp:Label runat="server" ID="NoApplyLabel"></asp:Label>
              </p>
            </asp:PlaceHolder>
					</div>
					<div class="jobPostingSummaryReport">
					  <asp:PlaceHolder runat="server" ID="ReportJobPlaceHolder" Visible="False">
						  <focus:LocalisedLabel runat="server" ID="ReportJobLabel" AssociatedControlID="ddlReportSpamOrClosed" LocalisationKey="ReportJob.Label" DefaultText="Report job"/>
						  <div class="FocusCareer blueDropDown">
							  <asp:DropDownList ID="ddlReportSpamOrClosed" runat="server" Width="130px" ClientIDMode="Static">
								  <asp:ListItem Value="SPAM">as spam</asp:ListItem>
								  <asp:ListItem Value="CLOSED">as closed</asp:ListItem>
							  </asp:DropDownList>
						  </div>
						  <asp:Button ID="btnReportSpamOrClosed" Text="Go &#187;" runat="server" class="buttonLevel2" OnClick="btnReportSpamOrClosed_Click" />
            </asp:PlaceHolder>
						<asp:Panel ID="panRegisterSection" runat="server" CssClass="jobPostingRegister">
							<span class="promoStyle5">
								<a href='#' onclick="javascript:$find('RegisterModal').show();"><focus:LocalisedLabel runat="server" ID="RegisterCreateResumeLabel" RenderOuterSpan="False" LocalisationKey="RegisterCreateResume.Label" DefaultText="Register for #FOCUSCAREER# and create a resume"/></a> 
								<focus:LocalisedLabel runat="server" ID="RegisterCreateResumeOtherLabel" RenderOuterSpan="False" LocalisationKey="RegisterCreateResumeOther.Label" DefaultText="to see how well you match up against this job &#187;"/>
							</span>
						</asp:Panel>
					</div>
				</div>
				<div class="jobPostingTools">
					<div class="jobPostingToolsCriteria col-sm-7 col-md-8">
						<ul>
						    <asp:PlaceHolder runat="server" ID="AmIAGoodMatchPlaceholder">
							<li>
								<asp:LinkButton ID="AmIGoodMatchHyperLink" runat="server" OnClick="AmIGoodMatchHyperLink_Click">
									<focus:LocalisedLabel runat="server" ID="AmIGoodMatchText" RenderOuterSpan="False" LocalisationKey="AmIGoodMatch.Text" DefaultText="Am I a good match?"/>
								</asp:LinkButton>
							</li>
                            </asp:PlaceHolder>
							<li>
								<asp:LinkButton ID="FindMoreJobsHyperLink" runat="server" OnClick="FindMoreJobsHyperLink_Click">
									<focus:LocalisedLabel runat="server" ID="FindMoreJobsText" RenderOuterSpan="False" LocalisationKey="FindMoreJobs.Text" DefaultText="Find more jobs like this"/>
								</asp:LinkButton>
							</li>
							<li>
								<asp:LinkButton ID="DoNotDisplayHyperLink" runat="server" OnClick="DoNotDisplayHyperLink_Click">
									<focus:LocalisedLabel runat="server" ID="DoNotDisplayText" RenderOuterSpan="False" LocalisationKey="DoNotDisplay.Text" DefaultText="Do not display this job again"/>
								</asp:LinkButton>
							</li>
						</ul>
						<asp:Label ID="ViewCountLabel" runat="server" CssClass="instructionalText"></asp:Label>
					</div>
					<div class="jobPostingToolsShare col-sm-5 col-md-4">
						<asp:LinkButton ID="btnSaveBookmark" runat="server" OnClick="btnSaveBookmark_Click" CssClass="bookmarkLink">
							<focus:LocalisedLabel runat="server" ID="BookmarkLabel" RenderOuterSpan="False" LocalisationKey="Bookmark.Label" DefaultText="Bookmark this info"/>
						</asp:LinkButton>
						<asp:ImageButton ID="ibEmailJob" runat="server" Width="32px" Height="24px" OnClick="EmailJob_Click" ToolTip="Email Job" AlternateText="Email Job" />
            <asp:PlaceHolder runat="server" ID="PrintPlaceHolder">
						  <a href="#" onclick="return PrintPosting('POSTING');"><img src="<%= UrlBuilder.PrintIcon() %>" alt="Print" width="30" height="24" /></a>
            </asp:PlaceHolder>
					</div>
				</div>
			<div id="PostingDetailIframeDevice" style="clear: both;">
				<iframe id="PostingDetail" runat="server" style="width:100%;height:400px;" title="PostingDetail" ClientIDMode="Static">Posting Detail</iframe>
			</div>
			<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
			<uc:ConfirmationBookMarkModal ID="ConfirmationBookMarkModal" runat="server" />
			<asp:HiddenField ID="HowToApplyDummyTarget" runat="server" />
			<asp:HiddenField ID="HiddenEmailJob" runat="server" />
			<asp:HiddenField runat="server" ID="hdnActions" ClientIDMode="Static" />
			<asp:HiddenField runat="server" ID="hdnActionsYes" ClientIDMode="Static" />
			<asp:HiddenField runat="server" ID="hdnActionsWaive" ClientIDMode="Static" />
			<act:ModalPopupExtender ID="HowToApplyModal" runat="server" BehaviorID="HowToApplyModal"
				TargetControlID="HowToApplyDummyTarget" PopupControlID="HowToApplyModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
				<Animations>
								<OnShown>
                     <ScriptAction Script="ApplyStyleToDropDowns();" />  
                </OnShown>
				</Animations>
			</act:ModalPopupExtender>
			<div id="HowToApplyModalPanel" tabindex="-1" class="modal" style="z-index:100001; display:none;">
				<div class="lightbox" id="HowToApplyLightBox">
					<asp:Panel ID="panJobRequirements" runat="server" Visible="false" CssClass="jobPostingRequirementsModal">
						<div class="modalHeader">
							<input type="image" src="<%= UrlBuilder.Close() %>" title="Close" onclick="$find('HowToApplyModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
							<focus:LocalisedLabel runat="server" ID="JobPostingRequirementsTitleLabel" LocalisationKey="JobPostingRequirementsTitle.Label" DefaultText="Apply for this job" CssClass="modalTitle modalTitleLarge"/>
						</div>
						<focus:LocalisedLabel runat="server" ID="JobRequirementsInstructionLabel" CssClass="modalMessage" LocalisationKey="JobRequirementsInstruction.Label" DefaultText="Please indicate whether you meet the following requirements. Only applicants who meet these requirements will be considered for this job."/>
						<div>
							<asp:GridView ID="grdJobSkills" runat="server" AutoGenerateColumns="False" CssClass="jobPostingRequirementsGrid" PageSize="20">
								<AlternatingRowStyle CssClass="gridAlternateRow" />
								<PagerSettings FirstPageText="&lt;&lt; Prev" LastPageText="Last &gt;&gt;" Mode="NumericFirstLast" NextPageText="" Position="TopAndBottom" PreviousPageText="" />
								<PagerStyle CssClass="gridPager" HorizontalAlign="Right" ForeColor="Black" />
								<Columns>
									<asp:TemplateField HeaderText="Category">
										<ItemTemplate>
											<asp:Label ID="lblCategory" runat="server" Width="100px" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Requirements">
										<ItemTemplate>
											<asp:Label ID="lblRequirement" runat="server" Width="530px" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Are you able to meet this requirement?">
										<ItemTemplate>
											<asp:RadioButtonList runat="server" ID="rblAction" RepeatDirection="Horizontal" RepeatColumns="2"
												Width="70px" EnableViewState="true">
												<asp:ListItem Text="Yes" Value="1"></asp:ListItem>
												<asp:ListItem Text="No" Value="0"></asp:ListItem>
											</asp:RadioButtonList>
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
						</div>
						<div class="modalButtons">
							<button class="buttonLevel3" id="btnIDoNotMeetReq" onclick="$find('HowToApplyModal').hide(); return false;">I do not meet these requirements</button>
							<focus:DoubleClickDisableButton ID="btnIMeetReq" Text="I meet all of these requirements" runat="server" class="buttonLevel2" OnClick="btnIMeetReq_Click" />
						</div>
					</asp:Panel>
					<asp:Panel ID="panJobApplicationInfo" runat="server" Visible="false" CssClass="jobPostingApplicationInfoModal">
						<div class="modalHeader">
							<input name="closeImage" type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('HowToApplyModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
							<focus:LocalisedLabel runat="server" ID="JobPostingApplicationTitleLabel" LocalisationKey="JobPostingApplicationTitle.Label" DefaultText="Job application information" CssClass="modalTitle modalTitleLarge"/>
						</div>
						<asp:Label ID="lblReferralDetails" runat="server" ClientIDMode="Static" CssClass="modalMessage"></asp:Label>
            <span>
              <asp:Label ID="lblLinkToApply" runat="server" ClientIDMode="Static"></asp:Label>
              <asp:HyperLink ID="SpideredjobUrl" Target="_blank" Width="350" runat="server" visible="False"></asp:HyperLink>
            </span>
						<div class="modalButtons">
							<button class="buttonLevel3" onclick="$find('HowToApplyModal').hide(); return false;"><%= HtmlLocalise("OK.Text", "OK") %></button>
							<button class="buttonLevel2" onclick="return PrintPosting('JOB_INFO');">Print</button>
							<asp:Button ID="btnSendJobInfo" Text="Send to your email" runat="server" class="buttonLevel2" OnClick="btnSendJobInfo_Click" />
						</div>
					</asp:Panel>
					<asp:Panel ID="panJobDetails" runat="server" CssClass="jobPostingDetailModal">
						<div class="modalHeader">
							<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('HowToApplyModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
							<focus:LocalisedLabel runat="server" ID="JobPostingDetailTitleLabel" LocalisationKey="JobPostingDetailTitle.Label" DefaultText="Apply for this job" CssClass="modalTitle modalTitleLarge"/>
						</div>
						<div class="dataInputRow">
							<focus:LocalisedLabel runat="server" ID="JobTitleLabel" CssClass="dataInputLabel" LocalisationKey="JobTitle.Label" DefaultText="Job Title:"/>
							<asp:Label ID="JobTitleDataLabel" runat="server" CssClass="dataInputField" />
						</div>
						<div class="dataInputRow" id="ApplyCustomerJobIdDiv" runat="server">
							<focus:LocalisedLabel runat="server" ID="CustomerJobIDLabel" CssClass="dataInputLabel" LocalisationKey="CustomerJobID.Label" DefaultText="Customer Job ID:"/>
							<asp:Label ID="CustomerJobIDDataLabel" runat="server" CssClass="dataInputField" />
						</div>
						<div class="dataInputRow">
							<asp:Label ID="lblJobInfoJobID" runat="server" CssClass="dataInputLabel" />
							<asp:Label ID="JobIDLabel" runat="server" CssClass="dataInputField" />
						</div>
						<div class="dataInputRow" id="ApplyOnlineDiv" runat="server">
							<focus:LocalisedLabel runat="server" ID="ApplyOnlineLabel" CssClass="dataInputLabel" LocalisationKey="ApplyOnline.Label" DefaultText="Apply Online:"/>
							<span class="dataInputField"><asp:HyperLink ID="lnkJobURL" Target="_blank" Width="350" runat="server"></asp:HyperLink></span>
						</div>
						<div class="dataInputRow">
							<focus:LocalisedLabel runat="server" ID="InfoLabel" CssClass="dataInputField" LocalisationKey="Info.Label" Visible="False" DefaultText="By clicking on Submit, your intention to apply to this job will be registered."/>
						</div>
					</asp:Panel>
					<asp:Panel ID="panReferralStatus" runat="server" Visible="false" CssClass="jobPostingReferralModal">
						<div class="modalHeader" id="JobPostingReferralDiv" runat="server">
							<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('HowToApplyModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
							<focus:LocalisedLabel runat="server" ID="JobPostingReferralTitleLabel" LocalisationKey="JobPostingReferralTitle.Label" DefaultText="Job Requirements" CssClass="modalTitle modalTitleLarge"/>
							<focus:LocalisedLabel runat="server" ID="SpideredJobPostingReferralTitleLabel" LocalisationKey="SpideredJobPostingReferralTitleLabel.Label" DefaultText="Job Application Details" Visible="False" CssClass="modalTitle modalTitleLarge"/>
						</div>
            <focus:LocalisedLabel runat="server" ID="SpideredJobPostingDisclaimerLabel" RenderOuterSpan="True" CssClass="information-panel" Visible="false" />
						<asp:Label ID="lblReferralStatus" runat="server" ClientIDMode="Static" CssClass="modalMessage"></asp:Label>
						<div class="dataInputRow">
							<span class="dataInputField"><asp:HyperLink ID="jobUrl" Target="_blank" Width="350" runat="server" Visible="False"></asp:HyperLink></span>
						</div>
            <asp:Label ID="lblAlreadyReferred" runat="server" ClientIDMode="Static" CssClass="modalMessage"></asp:Label>
            <div class="modalButtons" id="SendButtons" runat="server" Visible="False">
							<button class="buttonLevel3" onclick="$find('HowToApplyModal').hide(); return false;">Close</button>
							<focus:DoubleClickDisableButton ID="btnReferralStatusSend" Text="Send" runat="server" class="buttonLevel2" OnClick="btnReferralStatusSend_Click" />
            </div>
						<div class="modalButtons" id="SubmitButtons" runat="server">
							<button ID="btnReferralStatusCancel" class="buttonLevel3" onclick="$find('HowToApplyModal').hide(); return false;">Cancel</button>
							<focus:DoubleClickDisableButton ID="btnReferralStatusSubmit" Text="Submit" runat="server" class="buttonLevel2" OnClick="btnReferralStatusSubmit_Click" />
						</div>
						<div class="modalButtons" id="OKButtons" runat="server">
							<%--<button class="buttonLevel2" onclick="$find('HowToApplyModal').hide(); return false;"><%= HtmlLocalise("OK.Text", "OK") %></button>--%>
              <focus:DoubleClickDisableButton ID="btnReferralStatusOK" Text="OK" runat="server" class="buttonLevel2" OnClick="btnReferralStatusOK_Click" />
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="panResumeRequirement" Visible="False" CssClass="jobPostingResumeRequirementModal">
						<div class="modalHeader" id="JobPostingResumeRequirementDiv" runat="server">
							<focus:LocalisedLabel runat="server" ID="JobPostingResumeRequirementTitleLabel" LocalisationKey="JobPostingResumeRequirementTitle.Label" DefaultText="Resume requirement" CssClass="modalTitle modalTitleLarge"/>
						</div>
						<div class="dataInputRow">
							<focus:LocalisedLabel runat="server" ID="ResumeRequirementMessageLabel" CssClass="dataInputField" LocalisationKey="Info.Label" DefaultText="In order to be considered for this posting, please complete your resume and ensure it is selected as your default.<br/><br/> If you need assistance with completing your resume please contact your local Career Center."/>
						</div>
						<div class="modalButtons" id="Div1" runat="server">
							<button class="buttonLevel2" onclick="$find('HowToApplyModal').hide(); return false;"><%= HtmlLocalise("OK.Text", "OK") %></button>
						</div>
					</asp:Panel>
          <asp:Panel ID="panScreeningPreferences" runat="server" Visible="false" CssClass="jobPostingScreeningPreferencesModal">
						<div class="modalHeader">
							<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('HowToApplyModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
							<focus:LocalisedLabel runat="server" ID="ScreeningPreferencesHeader" LocalisationKey="ScreeningPreferencesHeader.Label" DefaultText="Unable to apply" CssClass="modalTitle modalTitleLarge"/>
						</div>
            <focus:LocalisedLabel runat="server" ID="ScreeningPreferencesBody" LocalisationKey="ScreeningPreferencesBody.Label" DefaultText="You do not meet the required match score to apply to this posting. Please contact your Career Center for further advice." CssClass="modalMessage"/>
          </asp:Panel>
					<asp:Panel ID="panReferralDenied" runat="server" Visible="false" CssClass="jobPostingScreeningPreferencesModal">
						<div class="modalHeader">
							<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('HowToApplyModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
							<focus:LocalisedLabel runat="server" ID="referralDeniedTitle" LocalisationKey="ScreeningPreferencesHeader.Label" DefaultText="Referral denied" CssClass="modalTitle modalTitleLarge"/>
						</div>
            <focus:LocalisedLabel runat="server" ID="ReferralDeniedMessage" LocalisationKey="ScreeningPreferencesBody.Label" DefaultText="Your alien registration expiry date has lapsed and therefore you cannot be considered for this posting. Please contact your Career Center for further assistance." CssClass="modalMessage"/>
          </asp:Panel>
				</div>
			</div>
			<act:ModalPopupExtender ID="EmailJobModal" runat="server" BehaviorID="EmailJobModal"
				TargetControlID="HiddenEmailJob" PopupControlID="EmailJobPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
				<Animations>
								<OnShown>
                     <ScriptAction Script="ApplyStyleToDropDowns();" />  
                </OnShown>
				</Animations>
			</act:ModalPopupExtender>
			<div id="EmailJobPanel" class="modal" style="z-index: 100001; display: none;">
				<div class="lightbox jobPostingEmailModal">
					<div class="modalHeader">
						<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('EmailJobModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
						<focus:LocalisedLabel runat="server" ID="JobPostingEmailTitleLabel" LocalisationKey="JobPostingEmailTitle.Label" DefaultText="Email job" CssClass="modalTitle modalTitleLarge" role="heading" aria-level="4"/>
					</div>
					<div class="emailDetailPanel">
						<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="EmailInstructionLabel" RenderOuterSpan="False" LocalisationKey="EmailInstruction.Label" DefaultText="Separate email addresses with a semicolon and do not use a space."/></p>
						<div class="dataInputRow">
							<focus:LocalisedLabel runat="server" ID="EmailJobToLabel" ClientIDMode="Static" AssociatedControlID="txtToEmailAddress" CssClass="dataInputLabel" LocalisationKey="EmailJobTo.Label" DefaultText="To:"/>
							<span class="dataInputField">
								<asp:TextBox ID="txtToEmailAddress" runat="server" Width="400" ClientIDMode="Static"></asp:TextBox>
								<asp:CustomValidator ID="custToEmail" runat="server" ControlToValidate="txtToEmailAddress" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="EmailJob" ClientValidationFunction="ValidateToEmail" ValidateEmptyText="true" />
							</span>
						</div>
						<div class="dataInputRow">
							<focus:LocalisedLabel runat="server" ID="EmailJobSubjectLabel" AssociatedControlID="txtEmailSubject" CssClass="dataInputLabel" LocalisationKey="EmailJobSubject.Label" DefaultText="Subject:"/>
							<span class="dataInputField">
								<asp:TextBox ID="txtEmailSubject" runat="server" ClientIDMode="Static" Width="400"></asp:TextBox>
								<asp:RequiredFieldValidator ID="EmailSubjectRequired" runat="server" ControlToValidate="txtEmailSubject" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="EmailJob" />
							</span>
						</div>
					</div>
					<div class="modalButtons">
						<asp:Button ID="btnSendEmailJob" runat="server" class="buttonLevel2" Text="Send" ValidationGroup="EmailJob" OnClick="btnSendEmailJob_Click" />
					</div>
				</div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
	<script type="text/javascript">
	  function ApplyStyleToDropDowns() {
	    //$('#ddlReportSpamOrClosed').customSelect();
	    ApplyCustomSelectStyling($('#ddlReportSpamOrClosed'));
	  }

	  Sys.Application.add_load(Email_PageLoad);

	  function Email_PageLoad(sender, args) {
	    // Jaws Compatibility

	    var emailModal = $find('EmailJobModal');
	    if (emailModal != null) {

	      emailModal.add_shown(function () {
	        $('#EmailJobPanel').attr('aria-hidden', false);
	        $('.page').attr('aria-hidden', true);
	        $('#txtToEmailAddress').focus();
	      });

	      emailModal.add_hiding(function () {
	        $('#EmailJobPanel').attr('aria-hidden', true);
	        $('.page').attr('aria-hidden', false);
	      });
	    }

	    var applyModal = $find('HowToApplyModal');
	    if (applyModal != null) {
	      applyModal.add_shown(function () {
	        $('#HowToApplyModalPanel').attr('aria-hidden', false);
	        $('.page').attr('aria-hidden', true);
	        FitToContent();
	        DoubleClickDisableButton_RemoveOverlay("<%=btnIMeetReq.ClientID %>");
	        DoubleClickDisableButton_RemoveOverlay("<%=btnReferralStatusSend.ClientID %>");
	        DoubleClickDisableButton_RemoveOverlay("<%=btnReferralStatusSubmit.ClientID %>");
	      });

	      applyModal.add_hiding(function () {
	        $('#HowToApplyModalPanel').attr('aria-hidden', true);
	        $('.page').attr('aria-hidden', false);
	      });
	    }

	  }

	  function PrintPosting(section) {
	    var displaySetting = 'left=150px,top=70px,width=850px,height=850px,addressbar=no,location=no,scrollbars=yes,status=no,resizable=no';
	    var contentInnerHtml = '';

	    if (section === 'POSTING') {
	      var jobDetails = '<strong>Customer Job ID:</strong> ' + '<%= CustomerJobId %>' + '<br /><strong>' + '<%= BrandName %>' + ' Job ID:</strong> ' + '<%= LensPostingId %>' + '<br />';
	      contentInnerHtml = jobDetails + $('#PostingDetail').contents().find('body').html();
	    }
	    else if (section === 'JOB_INFO')
	      contentInnerHtml = $('#lblReferralDetails').html();

	    var documentPrint = window.open('', '_blank', displaySetting);
	    documentPrint.document.open();
	    documentPrint.document.write('<html><head></head>');
	    documentPrint.document.write('<body onload="self.close();">');
	    documentPrint.document.write(contentInnerHtml);
	    documentPrint.document.write('</body></html>');
	    documentPrint.print();
	    documentPrint.document.close();
	    return false;
	  }

	  function ValidateToEmail(sender, args) {
	    var reg = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
	    var address = $('#txtToEmailAddress').val();
	    if (args.Value.length === 0) {
	      sender.innerHTML = '<%= EmailAddressRequired %>';
	      args.IsValid = false;
	    }
	    else if (args.Value.length < 6) {
	      sender.innerHTML = '<%= EmailAddressCharLimit %>';
	      args.IsValid = false;
	    }
	    else if (reg.test(address) === false) {
	      sender.innerHTML = '<%= EmailAddressErrorMessage %>';
	      args.IsValid = false;
	    }
	  }

	  function checkInclude(radioClicked) {
	    var radioButtonList = $('#' + radioClicked.id);
	    var radioButtonListSubId = radioClicked.id.substring(radioClicked.id.lastIndexOf('_') + 1);
	    var hdnActions = $('#hdnActions');
	    var hdnActionsYes = $('#hdnActionsYes');
	    var hdnActionsWaive = $('#hdnActionsWaive');

	    if ($(radioButtonList).find('input[value="0"]').is(':checked')) {
	      if ($(hdnActions).val().indexOf("'" + radioButtonListSubId + "'") < 0)
	        $(hdnActions).val($(hdnActions).val() + "'" + radioButtonListSubId + "'#");
	    }
	    else {
	      if ($(hdnActions).val().indexOf("'" + radioButtonListSubId + "'") >= 0)
	        $(hdnActions).val($(hdnActions).val().replace("'" + radioButtonListSubId + "'", ""));
	    }

	    if ($(radioButtonList).find('input[value="1"]').is(':checked')) {
	      if ($(hdnActionsYes).val().indexOf("'" + radioButtonListSubId + "'") < 0)
	        $(hdnActionsYes).val($(hdnActionsYes).val() + "'" + radioButtonListSubId + "'#");
	    }
	    else {
	      if ($(hdnActionsYes).val().indexOf("'" + radioButtonListSubId + "'") >= 0)
	        $(hdnActionsYes).val($(hdnActionsYes).val().replace("'" + radioButtonListSubId + "'", ""));
	    }

	    if ($(radioButtonList).find('input[value="-1"]').is(':checked')) {
	      if ($(hdnActionsWaive).val().indexOf("'" + radioButtonListSubId + "'") < 0)
	        $(hdnActionsWaive).val($(hdnActionsWaive).val() + "'" + radioButtonListSubId + "'#");
	    }
	    else {
	      if ($(hdnActionsWaive).val().indexOf("'" + radioButtonListSubId + "'") >= 0)
	        $(hdnActionsWaive).val($(hdnActionsWaive).val().replace("'" + radioButtonListSubId + "'", ""));
	    }

	    return true;
	  }

	  function JobPosting_DisableJobSeekerLink(careerUrl, linkText) {
	    var contents = $("#<%=PostingDetail.ClientID %>").contents();

	    var link = contents.find("a[href^='" + careerUrl + "']");
	    if (link.length == 0)
	      link = contents.find("a:contains('" + linkText + "')");

	    if (link.length > 0) {
	      link.each(function () {
	        var $t = jQuery(this);
	        $t.after($t.text());
	        $t.remove();
	      });
	    }
	  }

	  function JobPosting_DisableAllLinks() {
	    var iframe = $("#<%=PostingDetail.ClientID %>");
	    var contents = iframe.contents();

	    var link = contents.find("a[href!='']");
	    if (link.length > 0) {
	      link.each(function () {
	        var $t = jQuery(this);
	        $t.after($t.text());
	        $t.remove();
	      });
	    }
	    iframe.contents().on('click', function (e) {
	      return false;
	    });
	  }

	  function FitToContent() {

	    var textbox = document.getElementById("<%=lblReferralStatus.ClientID %>");

	    if (textbox != null) {
	      /* Accounts for rows being deleted, pixel value may need adjusting */
	      if (textbox.clientHeight == textbox.scrollHeight) {
	        textbox.style.height = "30px";
	      }

	      var adjustedHeight = textbox.clientHeight;

	      adjustedHeight = Math.max(textbox.scrollHeight, adjustedHeight);

	      if (adjustedHeight > textbox.clientHeight)
	        textbox.style.height = adjustedHeight + "px";
	    }
	  }

	  /* Ipad Iframe fix */
	  $(function () {
	    var iframe = $("#PostingDetail");
	    iframe.load(function () {
	      $("body").height(iframe.height());
	    });
	  });
	</script>
</asp:Content>
