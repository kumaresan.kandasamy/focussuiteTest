﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Focus.Common.Localisation;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
	public partial class ResumeDisplay : System.Web.UI.Page
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if ("netscape|gecko|firefox|opera".IndexOf(Request.Browser.Browser.ToLower()) >= 0)
				ClientTarget = "Uplevel";

      var resumeId = Request.QueryString["resumeid"];
      if (resumeId != null)
      {
        var mimeType = Page.RouteData.Values.ContainsKey("mimeType") ? Page.RouteData.Values["mimeType"].ToString() : string.Empty;

        byte[] resumeBytes;
        var resumeHtml = GetResumeHtml(long.Parse(resumeId), out resumeBytes);

        if (mimeType.Equals("pdf", StringComparison.OrdinalIgnoreCase))
        {
          Response.AddHeader("Content-Type", "application/pdf");
          Response.AddHeader("Content-Length", resumeBytes.Length.ToString());
          Response.BinaryWrite(resumeBytes);
        }
        else
        {
          Response.Write(resumeHtml);
        }
      }
		}

    /// <summary>
    /// Gets the HTML for the original resume if possible
    /// </summary>
    /// <param name="resumeId">The ID of the resume</param>
    /// <param name="resumeBytes">The byte array for the original resume</param>
    /// <returns>The HTML string for the resume</returns>
    private string GetResumeHtml(long resumeId, out byte[] resumeBytes)
    {
      string fileName, mimeType, htmlResume;

      resumeBytes = ServiceClientLocator.ResumeClient(new HttpApp()).GetOriginalUploadedResume(resumeId, out fileName, out mimeType, out htmlResume);

      if (htmlResume.ToUpper().StartsWith("BGTEC022:"))
      {
          htmlResume = Localiser.Instance().Localise("ReviewInvalid.Body", "Your uploaded file format is a PDF and therefore cannot be displayed with its original formatting.  Please choose a different format or upload a DOC, DOCX, or RTF file.");
      }

      return htmlResume;
    }
	}
}
