﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Education.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.Education" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register Src="../../Controls/UpdateableList.ascx" TagName="UpdateableList" TagPrefix="uc" %>
<%@ Register Src="../../Controls/LanguageProficiencyManager.ascx" TagName="LanguageProficiencyManager" TagPrefix="uc" %>
<script src="<%= ResolveUrl("~/Assets/Scripts/moment.min.js") %>" type="text/javascript"></script>

<div class="resumeSection">
	<div class="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2 greyedOut" ValidationGroup="Education" OnClick="SaveMoveToNextStepButton_Click" disabled="disabled" ClientIDMode="Static"/>
	</div>
	<div class="resumeSectionContent">
		<div class="resumeSectionSubHeading" id="EducationEnrollmentHeading" runat="server">
		  <asp:Literal runat="server" ID="EducationEnrollmentSubHeadingLabel"></asp:Literal>
      &nbsp;
		  <asp:Label runat="server" ID="EducationEnrollmentSubHeadingRequiredLabel" CssClass="requiredDataLegend"></asp:Label>
		</div>

		<div class="resumeEducation resumeEducationEnrollment">
			<p class="instructionalText" id="resumeEducationInstructionPara" runat="server">
			  <asp:Literal runat="server" ID="EducationInstructionLiteral" Visible="False"></asp:Literal>
			</p>

      <div class="dataInputRow" id="EnrollmentStatusRow" runat="server">
				<focus:LocalisedLabel runat="server" ID="EnrollmentStatusLabel" AssociatedControlID="ddlEnrollmentStatus" CssClass="dataInputLabel requiredData" LocalisationKey="EnrollmentStatus.Label" DefaultText="Enrollment status"/>
				<span class="dataInputField" data-span="EnrollmentStatus">
					<asp:DropDownList ID="ddlEnrollmentStatus" runat="server" Width="330px" ClientIDMode="Static">
						<asp:ListItem Text="select enrollment status" />
					</asp:DropDownList>
					<br />
					<asp:CustomValidator ID="EnrollmentStatusRequired" runat="server" ControlToValidate="ddlEnrollmentStatus" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Education" ValidateEmptyText="True" ClientValidationFunction="ValidateEnrolmentStatus" />
				</span>
			</div>

			<div class="dataInputRow" id="EducationLevelRow" runat="server">
				<focus:LocalisedLabel runat="server" ID="EducationLevelLabel" AssociatedControlID="ddlEducationLevel" CssClass="dataInputLabel requiredData" LocalisationKey="EducationLevel.Label" DefaultText="Education level"/>
				<span class="dataInputField" id="EducationLevelSpan">
					<asp:DropDownList ID="ddlEducationLevel" runat="server" ClientIDMode="Static">
						<asp:ListItem Text="select education level" />
					</asp:DropDownList>
					<br />
					<asp:CustomValidator ID="custEnrolmentEducationLevel" runat="server" ControlToValidate="ddlEducationLevel" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateEnrolmentEducationLevel" ValidateEmptyText="true" />
				</span>
			</div>
		</div>

		<div class="resumeSectionSubHeading">
			<focus:LocalisedLabel runat="server" ID="DegreesAndDiplomasSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="DegreesAndDiplomasSubHeading.Label" DefaultText="Degrees and diplomas"/>
			&nbsp;<focus:LocalisedLabel runat="server" ID="DegreesAndDiplomasSubHeadingRequiredLabel" CssClass="requiredDataLegend" LocalisationKey="DegreesAndDiplomasSubHeadingRequired.Label" DefaultText="required fields"/>
		</div>
		<div class="resumeEducation resumeEducationDegreeList">
			<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="DegreesAndDiplomasInstuctionsLabel" RenderOuterSpan="False" LocalisationKey="DegreesAndDiplomasInstuctions.Label" DefaultText="If you wish to display a degree or diploma on your resume, you must complete the degree/diploma name, major/subject, and school/university, and select a completion status." /></p>
			<asp:Repeater ID="DegreeRepeater" runat="server" OnItemDataBound="DegreeRepeater_ItemDataBound" OnItemCommand="DegreeRepeater_ItemCommand">
				<ItemTemplate>
					<div class="resumeEducationDegree">
						<div class="dataInputRow">
							<span class="dataInputGroupedItemHorizontal">
							  <span class="dataInputField">
								  <span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="DegreeNameLabel" AssociatedControlID="DegreeTextBox" LocalisationKey="DegreeName.Label" DefaultText="Degree or diploma name" Width="432"/></span>
								  <asp:TextBox ID="DegreeTextBox" runat="server" Width="432px" /><%= HtmlRequiredLabel("RedAsterik.Label", "")%>
								  <ajaxtoolkit:AutoCompleteExtender ID="aceDegree" runat="server" CompletionSetCount="20"
									  CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
									  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetDegrees"
									  ServicePath="~/Services/AjaxService.svc" TargetControlID="DegreeTextBox" CompletionInterval="1000"
									  UseContextKey="false" EnableCaching="true" MinimumPrefixLength="2" OnClientShown="fixAutoCompleteExtender">
								  </ajaxtoolkit:AutoCompleteExtender>
								  <asp:HiddenField ID="DegreeHiddenID" runat="server" />
                </span>
							</span>
							<span class="dataInputGroupedItemHorizontal">
								<asp:LinkButton ID="AddDegreeButton" ClientIDMode="AutoID" runat="server" CommandName="Add" CommandArgument='<%# Eval("DegreeID") %>'>+ Add another degree</asp:LinkButton>
							</span>
							<span class="dataInputGroupedItemHorizontal">
								<asp:LinkButton ID="DeleteDegreeButton" ClientIDMode="AutoID" runat="server" CommandName="Delete" CommandArgument='<%# Eval("DegreeID") %> '>- Delete this degree</asp:LinkButton>
							</span>
							<br />
							<asp:CustomValidator ID="DegreeNameValidator" runat="server" ControlToValidate="DegreeTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateDegreeName" ValidateEmptyText="true" />
						</div>
						<div class="dataInputRow">
                        <p class="instructionalText">
                                    <focus:LocalisedLabel runat="server" ID="MajorSubjectInstuctionsLabel" RenderOuterSpan="False"
                                        LocalisationKey="MajorSubjectInstuctions.Label" DefaultText="For High School diplomas without a major/subject, enter &quot;General studies&quot;." />
                                </p>
							<span class="dataInputGroupedItemHorizontal">
							  <span class="dataInputField">
								  <span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="MajorSubjectLabel" AssociatedControlID="MajorSubjectTextBox" LocalisationKey="MajorSubject.Label" DefaultText="Major/subject" Width="100"/></span>
								  <asp:TextBox ID="MajorSubjectTextBox" runat="server" Width="200px" /><%= HtmlRequiredLabel("RedAsterik.Label", "")%>                                
								  <ajaxtoolkit:AutoCompleteExtender ID="aceMajor" runat="server" CompletionSetCount="20"
									  CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
									  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetMajors"
									  ServicePath="~/Services/AjaxService.svc" TargetControlID="MajorSubjectTextBox"
									  CompletionInterval="1000" UseContextKey="false" EnableCaching="true" MinimumPrefixLength="3" OnClientShown="fixAutoCompleteExtender">
								  </ajaxtoolkit:AutoCompleteExtender>
								  <br />
								  <asp:CustomValidator ID="MajorValidator" runat="server" ControlToValidate="MajorSubjectTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateMajorSubject" ValidateEmptyText="true" />
                </span>                
							</span> 
							<span class="dataInputGroupedItemHorizontal">
								<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="SchoolUniLabel" AssociatedControlID="SchoolUniTextBox" LocalisationKey="SchoolUni.Label" DefaultText="School/university" Width="150"/></span>
								<asp:TextBox ID="SchoolUniTextBox" runat="server" Width="200px" /><%= HtmlRequiredLabel("RedAsterik.Label", "")%>
								<br />
								<asp:CustomValidator ID="SchoolValidator" runat="server" ControlToValidate="SchoolUniTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateSchoolUniversity" ValidateEmptyText="true" />
							</span>                                             
						</div>
						<div class="dataInputRow">
							<span class="dataInputGroupedItemHorizontal">
							  <span class="dataInputField">
								  <span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="CityLabel" AssociatedControlID="DegreeCityTextBox" LocalisationKey="DegreeCity.Label" DefaultText="City" Width="150"/></span>
								  <asp:TextBox ID="DegreeCityTextBox" runat="server" Width="200px" />
								  <ajaxtoolkit:AutoCompleteExtender ID="aceCity" runat="server" CompletionSetCount="20"
									  CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
									  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetCities"
									  ServicePath="~/Services/AjaxService.svc" TargetControlID="DegreeCityTextBox" CompletionInterval="1000"
									  UseContextKey="false" EnableCaching="true" MinimumPrefixLength="3" OnClientShown="fixAutoCompleteExtender">
								  </ajaxtoolkit:AutoCompleteExtender>
                </span>
							</span>
							<span class="dataInputField dataInputGroupedItemHorizontal" id="DegreeStateSpan">
								<asp:DropDownList ID="DegreeStateDropDownList" runat="server">
									<asp:ListItem Text="- select a state -" />
								</asp:DropDownList>
								<br />
								<asp:CustomValidator ID="DegreeStateValidator" runat="server" ControlToValidate="DegreeStateDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateDegreeState" ValidateEmptyText="true" />
							</span>
							<span class="dataInputField dataInputGroupedItemHorizontal" id="DegreeCountrySpan">
								<asp:DropDownList ID="DegreeCountryDropDownList" runat="server">
									<asp:ListItem Text="United States" />
								</asp:DropDownList>
								<br />
								<asp:CustomValidator ID="DegreeCountryValidator" runat="server" ControlToValidate="DegreeCountryDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateDegreeCountry" ValidateEmptyText="true" />
							</span>
						</div>
						<div class="dataInputRow">
                            <focus:LocalisedLabel runat="server" CssClass="requiredData" ID="CompletionStatusLabel"
                                LocalisationKey="CompletionStatus.Label" DefaultText="Completion Status" />
                            <p class="instructionalText">
                                <focus:LocalisedLabel runat="server" ID="CompletionStatusInstructionsLabel" RenderOuterSpan="False"
                                    LocalisationKey="CompletionStatusInstuctions.Label" DefaultText="On the resume Review tab, use the Hide features to suppress graduation dates." />
                            </p>
							<span class="dataInputGroupedItemHorizontal">
								<asp:RadioButton GroupName="EndDate" ID="CompletedRadioButton" runat="server" Onclick="EndDateRadioButtonClicked(this)"/>
								<focus:LocalisedLabel CssClass="dataInputField" runat="server" ID="DegreeCompletedLabel" LocalisationKey="Completed.Label" DefaultText="Completed degree or diploma"/>&nbsp;&nbsp;&nbsp; 
								<span class="dataInputGroupedItemHorizontal">
									<asp:TextBox ID="CompletedDegreeTextBox" runat="server" Width="102px" category='degreeCompletion'/>
									<%--<ajaxtoolkit:MaskedEditExtender ID="meCompletedDegreeDate" runat="server" Mask="99/9999"
									TargetControlID="CompletedDegreeTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_">
									</ajaxtoolkit:MaskedEditExtender>--%>
									<span class="instructionalText">mm/yyyy</span>
									<br />
									<asp:CustomValidator ID="CompletedDegreeValidator" runat="server" ControlToValidate="CompletedDegreeTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateCompletedDegree" ValidateEmptyText="true" />
								</span>
							</span>
							<span class="dataInputGroupedItemHorizontal">
								<asp:RadioButton GroupName="EndDate" ID="ExpectedCompletionRadioButton" runat="server" Onclick="ExpectedEndDateRadioButtonClicked(this)"/>
								<focus:LocalisedLabel runat="server" ID="ExpectedCompletionLabel" CssClass="dataInputField" LocalisationKey="ExpectedCompletion.Label" DefaultText="Currently enrolled; expected completion date"/>
								<span class="dataInputGroupedItemHorizontal">
									<asp:TextBox ID="ExpectedCompletionTextBox" runat="server" Width="102px" category='expectedCompletion'/>
									<%--<ajaxtoolkit:MaskedEditExtender ID="meExpectedCompletion" runat="server" Mask="99/9999"
									TargetControlID="ExpectedCompletionTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_">
									</ajaxtoolkit:MaskedEditExtender>--%>
									<span class="instructionalText">mm/yyyy</span>
									<br/>
									<asp:CustomValidator ID="ExpectedCompletionValidator" runat="server" ControlToValidate="ExpectedCompletionTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateExpectedCompletionDegree" ValidateEmptyText="true" />
								</span>
							</span>
                                    <span class="dataInputGroupedItemHorizontal">
                                        <asp:RadioButton GroupName="EndDate" ID="UnfinishedCompletionRadioButton" runat="server"
                                            Onclick="UnfinishedEndDateRadioButtonClicked(this)" />
                                        <focus:LocalisedLabel runat="server" ID="UnfinishedCompletionLabel" CssClass="dataInputField"
                                            LocalisationKey="UnfinishedCompletion.Label" DefaultText="Unfinished degree or diploma. Not currently enrolled. Do not have an expected completion date. " />
                                    </span>
                            <asp:CustomValidator ID="CompletionDateValidator" runat="server" CssClass="error" Display="Dynamic"
                                ValidationGroup="Education" ClientValidationFunction="ValidateCompletionStatus" ValidateEmptyText="true"/>
						</div>
						<div class="dataInputRow">
							<span class="resumeEducationDegreeCourses">
								<span class="inFieldLabel"><focus:LocalisedLabel ID="DegreeCoursesLabel" runat="server" AssociatedControlID="CoursesTextBox" LocalisationKey="DegreeCourses.Label" DefaultText="Courses" Width="280" /></span>
								<asp:TextBox ID="CoursesTextBox" runat="server" Width="280px" TextMode="MultiLine" />
							</span>
							<span class="resumeEducationDegreeHonors">
								<span class="inFieldLabel"><focus:LocalisedLabel ID="DegreeHonorsLabel" runat="server" AssociatedControlID="HonorsTextBox" LocalisationKey="DegreeHonors.Label" DefaultText="Honors" Width="280" /></span>
								<asp:TextBox ID="HonorsTextBox" runat="server" Width="280px" TextMode="MultiLine" />
							</span>
							<span class="resumeEducationDegreeGPA">
								<span class="inFieldLabel"><focus:LocalisedLabel ID="DegreeGPALabel" runat="server" AssociatedControlID="GPATextBox" LocalisationKey="DegreeGPA.Label" DefaultText="GPA" Width="60" /></span>
									<asp:TextBox ID="GPATextBox" runat="server" Width="60px" /> 
								</span>
							<span class="resumeEducationDegreeActivities">
								<span class="inFieldLabel"><focus:LocalisedLabel ID="DegreeActivitiesLabel" runat="server" AssociatedControlID="ActivitiesTextBox" LocalisationKey="DegreeActivities.Label" DefaultText="Activities" Width="280" /></span>
								<asp:TextBox ID="ActivitiesTextBox" runat="server" Width="280px" TextMode="MultiLine" />
							</span>
						</div>
							<div class="dataInputRow" style="padding-left:612px">
								<asp:RegularExpressionValidator ID="DegreeGPAValidator" runat="server" ControlToValidate="GPATextBox" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?(/[0-9]+(\.[0-9]{1,2})?)?$" CssClass="error" SetFocusOnError="True" Display="Dynamic" Text="Invalid format" ValidationGroup="Education" />
								<asp:CustomValidator ID="DegreeGPARangeValidator" runat="server" ControlToValidate="GPATextBox"  CssClass="error" SetFocusOnError="True" Display="Dynamic" Text="Range" ClientValidationFunction="ValidateGPA" ValidationGroup="Education" />
							</div>
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</div>
		<div class="resumeSectionSubHeading">
				<focus:LocalisedLabel runat="server" ID="CertsSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="CertsSubHeading.Label" DefaultText="Occupational licenses and certifications"/>
		</div>
		<div class="resumeEducation resumeEducationCertList">
			<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="CertsInstructionsLabel" Visible="False" RenderOuterSpan="False" LocalisationKey="CertsInstructions.Label" DefaultText='For occupational licenses or certifications in progress, please enter the expected completion date in the "Issue Date" column.'/></p>
			<asp:Repeater ID="LicenseRepeater" runat="server" OnItemDataBound="LicenseRepeater_ItemDataBound" OnItemCommand="LicenseRepeater_ItemCommand">
				<ItemTemplate>
					<div class="resumeEducationCert">
						<div class="dataInputRow">
							<span class="dataInputGroupedItemHorizontal">
							  <span class="dataInputField">
								  <span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="ProfessionalLicenceLabel" AssociatedControlID="ProfessionalLicenceTextBox" LocalisationKey="ProfessionalLicence.Label" DefaultText="Occupational license or certification" Width="280"/></span>
								  <asp:TextBox ID="ProfessionalLicenceTextBox" runat="server" Width="280px" />
								  <ajaxtoolkit:AutoCompleteExtender ID="aceProfessionalLicence" runat="server" CompletionSetCount="20"
									  CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
									  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetCertificates"
									  ServicePath="~/Services/AjaxService.svc" TargetControlID="ProfessionalLicenceTextBox"
									  CompletionInterval="1000" UseContextKey="false" EnableCaching="true" MinimumPrefixLength="3" OnClientShown="fixAutoCompleteExtender">
								  </ajaxtoolkit:AutoCompleteExtender>
								  <asp:HiddenField ID="LicenseHiddenID" runat="server" ClientIDMode="Static" />
                </span>
							</span>
							<span class="dataInputGroupedItemHorizontal">
								<asp:LinkButton ID="AddLicenseButton" ClientIDMode="AutoID" runat="server" CommandName="Add" CommandArgument='<%# Eval("LicenseID") %>'>+ Add</asp:LinkButton>
							</span>
							<span class="dataInputGroupedItemHorizontal">
								<asp:LinkButton ID="DeleteLicenseButton" ClientIDMode="AutoID" runat="server" CommandName="Delete" CommandArgument='<%# Eval("LicenseID") %>'>- Delete</asp:LinkButton>
							</span>
							<br />
							<asp:CustomValidator ID="ProfessionalLicenseValidator" runat="server" ControlToValidate="ProfessionalLicenceTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateProfessionalLicense" ValidateEmptyText="true" />
						</div>
						<div class="dataInputRow">
							<span class="dataInputGroupedItemHorizontal">
								<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="IssuingOrgLabel" AssociatedControlID="IssuingOrgTextBox" LocalisationKey="IssuingOrg.Label" DefaultText="Issuing organization" Width="280"/></span>
								<asp:TextBox ID="IssuingOrgTextBox" runat="server" Width="280px" />
								<br />
								<asp:CustomValidator ID="IssuingOrgValidator" runat="server" ControlToValidate="IssuingOrgTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateIssuingOrg" ValidateEmptyText="true" />
							</span>
						</div>

						<div class="dataInputRow">
							<span class="dataInputGroupedItemHorizontal">
								<asp:RadioButton GroupName="LicenceEndDate" ID="CompletedLicenceRadioButton" runat="server" Onclick="LicenceEndDateRadioButtonClicked(this)" />
								<focus:LocalisedLabel runat="server" ID="CompletedLicenceLabel" AssociatedControlID="CompletedLicenceTextBox" LocalisationKey="CompletedLicence.Label" DefaultText="Issue date" CssClass="requiredData"/>
								<asp:TextBox ID="CompletedLicenceTextBox" runat="server" Width="100px" category="licenceCompletion" />
								<%--<ajaxtoolkit:MaskedEditExtender ID="meCompletedLicenceDate" runat="server" Mask="99/9999"
									TargetControlID="CompletedLicenceTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_">
								</ajaxtoolkit:MaskedEditExtender>--%>
								<span class="instructionalText">mm/yyyy</span>
							  <br />
								<asp:CustomValidator ID="CompletedLicenceValidator" runat="server" ControlToValidate="CompletedLicenceTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateCompletedLicense" ValidateEmptyText="true" />
							</span>
							<span class="dataInputGroupedItemHorizontal">
								<asp:RadioButton GroupName="LicenceEndDate" ID="ExpectedLicenceRadioButton" runat="server" Onclick="ExpectedLicenceEndDateRadioButtonClicked(this)" />
								<focus:LocalisedLabel runat="server" ID="ExpectedLicenceLabel" CssClass="requiredData" LocalisationKey="ExpectedLicence.Label" DefaultText="Expected issue date"/>
								<span class="dataInputGroupedItemHorizontal">
									<asp:TextBox ID="ExpectedLicenceTextBox" runat="server" Width="102px" category="expectedLicenceCompletion" />
									<%--<ajaxtoolkit:MaskedEditExtender ID="meExpectedLicence" runat="server" Mask="99/9999" TargetControlID="ExpectedLicenceTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_">
									</ajaxtoolkit:MaskedEditExtender>--%>
									<span class="instructionalText">mm/yyyy</span>
									<br/>
									<asp:CustomValidator ID="ExpectedLicenceValidator" runat="server" ControlToValidate="ExpectedLicenceTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateExpectedLicenceDate" ValidateEmptyText="true" />
								</span>
							</span>
						</div>

						<div class="dataInputRow">
							<span class="dataInputGroupedItemHorizontal">
							  <span class="dataInputField">
								  <span class="inFieldLabel"><focus:LocalisedLabel ID="LicenceCityLabel" runat="server" AssociatedControlID="LicenseCityTextBox" LocalisationKey="LicenceCity.Label" DefaultText="City" Width="150" /></span>
								  <asp:TextBox ID="LicenseCityTextBox" runat="server" Width="200px" />
								  <ajaxtoolkit:AutoCompleteExtender ID="aceCityLicense" runat="server" CompletionSetCount="20"
									  CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
									  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetCities"
									  ServicePath="~/Services/AjaxService.svc" TargetControlID="LicenseCityTextBox" CompletionInterval="1000"
									  UseContextKey="false" EnableCaching="true" MinimumPrefixLength="3" OnClientShown="fixAutoCompleteExtender">
								  </ajaxtoolkit:AutoCompleteExtender>
                </span>
							</span>
							<span class="dataInputField dataInputGroupedItemHorizontal">
								<asp:DropDownList ID="LicenseStateDropDownList" runat="server" Width="285px">
									<asp:ListItem Text="-select a state-" />
								</asp:DropDownList>
								<br />
								<asp:CustomValidator ID="LicenseStateValidator" runat="server" ControlToValidate="LicenseStateDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateLicenseState" ValidateEmptyText="true" />
							</span>
							<span class="dataInputField dataInputGroupedItemHorizontal" id="LicenseCountrySpan">
								<asp:DropDownList ID="LicenseCountryDropDownList" runat="server">
									<asp:ListItem Text="-United States-" />
								</asp:DropDownList>
								<br />
								<asp:CustomValidator ID="LicenseCountryValidator" runat="server" ControlToValidate="LicenseCountryDropDownList" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateLicenseCountry" ValidateEmptyText="true" />
							</span>
						</div>
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</div>
		
		<asp:Panel runat="server" ID="NCRCPanel">
			<div class="resumeSectionSubHeading">
				<focus:LocalisedLabel runat="server" ID="NCRCSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="NCRCSubHeading.Label" DefaultText="National Career Readiness Certificate&trade; Credentials" />
			</div>
			<div class="resumeEducation">
				<div class="dataInputRow resumeJustify">
					<asp:CheckBox ID="NCRCConfirmationCheckbox" runat="server" ClientIDMode="Static" onclick="NCRCChanged();"/><focus:LocalisedLabel ID="NCRCLabel" runat="server" CssClass="instructionalText" DefaultText="I hold the National Career Readiness Certificate&trade; from American College Testing. I confirm that I achieved at least a Bronze level for the Applied Mathematics, the Locating Information, and the Reading for Information WorkKeys assessments." />
				</div>	
				<div class="dataInputRow">
					<span class="dataInputField resumeEducationNCRCSInput">
						<focus:LocalisedLabel runat="server" ID="NCRCLevelLabel" AssociatedControlID="ddlNCRCLevel" CssClass="requiredData" LocalisationKey="NCRCLevel.Label" DefaultText="NCRC level"/>
						<asp:DropDownList ID="ddlNCRCLevel" ClientIDMode="Static" runat="server" Width="250px" ></asp:DropDownList>
						<br />
						<asp:CustomValidator ID="NCRCLevelRequired" runat="server" ControlToValidate="ddlNCRCLevel" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateNCRCLevel" />
					</span>
					<span class="dataInputField resumeEducationNCRCSInput">
						<focus:LocalisedLabel runat="server" ID="NCRCStateLabel" AssociatedControlID="ddlNCRCState" CssClass="requiredData" LocalisationKey="NCRCState.Label" DefaultText="State of issue"/>
						<asp:DropDownList ID="ddlNCRCState" ClientIDMode="Static" runat="server" Width="250px" ></asp:DropDownList>
						<br />
						<asp:CustomValidator ID="NCRCStateRequired" runat="server" ControlToValidate="ddlNCRCState" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateNCRCState" />
					</span>
					<span class="resumeEducationNCRCSInput">
						<focus:LocalisedLabel runat="server" ID="NCRCIssueDateLabel" AssociatedControlID="txtNCRCIssueDate" CssClass="requiredData" LocalisationKey="NCRCIssueDate.Label" DefaultText="Issue date"/>
						<asp:TextBox ID="txtNCRCIssueDate" ClientIDMode="Static" runat="server" Width="100px"/>
						<%--<ajaxtoolkit:MaskedEditExtender ID="meNCRCIssueDate" runat="server" Mask="99/99/9999" TargetControlID="txtNCRCIssueDate" ClearMaskOnLostFocus="false" PromptCharacter="_"></ajaxtoolkit:MaskedEditExtender>--%>
						<focus:LocalisedLabel CssClass="instructionalText" runat="server" ID="NCRCIssueDateInstructionsLabel" LocalisationKey="NCRCIssueDateInstructions.Label" DefaultText= "mm/dd/yyyy" />
						<br />
						<asp:CustomValidator ID="NCRCIssueDateValidate" runat="server" ControlToValidate="txtNCRCIssueDate" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="ValidateNCRCIssueDate" />
					</span>
				</div>
				<div class="dataInputRow">
				 <span class="resumeEducationNCRCSInput">
					<asp:CheckBox ID="DisplayNCRCOnResumeCheckBox" runat="server" ClientIDMode="Static" /><focus:LocalisedLabel ID="DisplayNCRCOnResumeLabel" runat="server" AssociatedControlID="DisplayNCRCOnResumeCheckBox" LocalisationKey="DisplayNCRCOnResume.Label" DefaultText="Display my NCRC credentials on my resume" CssClass="checkbox" />
				 </span> 
				</div>
			</div>
		</asp:Panel>
    <asp:PlaceHolder runat="server" ID="LicencePlaceHolder">
		  <div class="resumeSectionSubHeading" id="DriversLicenseHeader">
			  <focus:LocalisedLabel runat="server" ID="LicenceSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="LicenceSubHeading.Label" DefaultText="Driver's License"/>
			  &nbsp;<focus:LocalisedLabel runat="server" ID="LicenceSubHeadingRequiredLabel" CssClass="requiredDataLegend" LocalisationKey="LicenceSubHeadingRequired.Label" DefaultText="required fields"/>
		  </div>
		  <div class="resumeEducation resumeEducationLicence">
			  <span class="resumeEducationLicenceDetail">
				  <span class="resumeEducationLicenceHeld">
					  <focus:LocalisedLabel runat="server" ID="DriverLicenceHeldLabel" AssociatedControlID="ddlDriverLicense" CssClass="requiredData" LocalisationKey="DriverLicenceHeld.Label" DefaultText="Driver's license"/>
					  <asp:DropDownList ID="ddlDriverLicense" runat="server" ClientIDMode="Static" onchange="return DriverLicenseChange();" Width="100px">
						  <asp:ListItem Selected="True" Text="- select -" Value="select" />
						  <asp:ListItem Text="Yes" Value="yes" />
						  <asp:ListItem Text="No" Value="no" />
					  </asp:DropDownList>
                      <br/>
					  <asp:CustomValidator ID="DriverLicenseValidate" runat="server" ControlToValidate="ddlDriverLicense" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="validateDriverLicense" />
				  </span>
				  <span class="resumeEducationLicenceState">
					  <focus:LocalisedLabel runat="server" ID="DriverLicenceStateLabel" AssociatedControlID="ddlState" CssClass="requiredData" LocalisationKey="DriverLicenceState.Label" DefaultText="State of issue"/>
					  <asp:DropDownList ID="ddlState" runat="server" Width="250px" ClientIDMode="Static" CssClass="DriverLicenseStateClass" Enabled="false">
						  <asp:ListItem Text="- select a state -" />
					  </asp:DropDownList>
					  <br />
					  <asp:CustomValidator ID="StateValidator" runat="server" ControlToValidate="ddlState" SetFocusOnError="true" Display="Dynamic" CssClass="error"  ClientValidationFunction="validateState" ValidateEmptyText="true" />
				  </span>
				  <span class="resumeEducationLicenceType">
					  <focus:LocalisedLabel runat="server" ID="DriverLicenceTypeLabel" AssociatedControlID="ddlLicenseType" CssClass="requiredData" LocalisationKey="DriverLicenceType.Label" DefaultText="Type of license"/>
					  <asp:DropDownList ID="ddlLicenseType" runat="server" Width="200px" CssClass="DriverLicenseTypeClass" ClientIDMode="Static" Enabled="false" onchange="return DriverLicenseTypeChange();">
						  <asp:ListItem Text="- select a type -" />
					  </asp:DropDownList>
					  <br />
					  <asp:CustomValidator ID="LicenseTypeValidator" runat="server" ControlToValidate="ddlLicenseType" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Education" ClientValidationFunction="validateLicenseType" ValidateEmptyText="true" />
				  </span>
				  <span class="resumeEducationLicenceDisplay">
					  <asp:CheckBox runat="server" ClientIDMode="Static" ID="chkUnHideClassD"/><focus:LocalisedLabel runat="server" DefaultText="Display driver’s license information on my resume."/>
					 <%-- <asp:CheckBoxList ID="chkUnHideClassD" runat="server" ClientIDMode="Static">
						  <asp:ListItem Text="Display driver’s license information on my resume."></asp:ListItem>
					  </asp:CheckBoxList>--%>
				  </span>
			  </span>
			  <span class="resumeEducationLicenceEndorsements">
				  <focus:LocalisedLabel runat="server" ID="DriverLicenceEndorsementsLabel" AssociatedControlID="EndorsementCheckBoxList" LocalisationKey="DriverLicenceEndorsements.Label" DefaultText="Endorsements (check all that apply)"/>
				  <asp:CheckBoxList ID="EndorsementCheckBoxList" RepeatColumns="3" runat="server" ClientIDMode="Static" CssClass="resumeEducationLicenceEndorsementList" />
			  </span>
		  </div>
    </asp:PlaceHolder>
		<div runat="server" class="resumeSectionSubHeading">
			<focus:LocalisedLabel runat="server" ID="LanguagesSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="LanguagesSubHeading.Label" DefaultText="Languages"/>
		</div>
		<div class="resumeEducation resumeEducationLanguages">
			<uc:LanguageProficiencyManager ID="LanguageProficiencyMgr" runat="server"	ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetLanguages"/>
		</div>
		<asp:PlaceHolder runat="server" ID="EducationEducationPlaceHolder1">
			<div class="resumeSectionSubHeading">
				<focus:LocalisedLabel runat="server" ID="SkillsSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="SkillsSubHeading.Label" DefaultText="Skills"/>
			</div>
			<div class="resumeEducation resumeEducationSkills">
				<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="SkillsInstructionsLabel" RenderOuterSpan="False" LocalisationKey="SkillsInstructions.Label" DefaultText="We've identified certain skills from your resume. Please add other skills you would like #BUSINESSES#:LOWER to know you have."/></p>
				<asp:UpdatePanel ID="SkillUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<div class="dataInputRow">
							<span class="dataInputGroupedItemHorizontal">
							  <span class="dataInputField">
								  <span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="SkillLabel" AssociatedControlID="SkillTextBox" LocalisationKey="Skill.Label" DefaultText="Skill" Width="250"/></span>
								  <asp:TextBox ID="SkillTextBox" runat="server" Width="250" ClientIDMode="Static" />
								  <ajaxtoolkit:AutoCompleteExtender ID="aceSkill" runat="server" CompletionSetCount="20"
									  CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
									  CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetSkills"
									  ServicePath="~/Services/AjaxService.svc" TargetControlID="SkillTextBox" CompletionInterval="1000"
									  UseContextKey="false" EnableCaching="true" MinimumPrefixLength="3" OnClientShown="fixAutoCompleteExtender">
								  </ajaxtoolkit:AutoCompleteExtender>
                </span>
							</span>
							<span class="dataInputGroupedItemHorizontal"><asp:LinkButton ID="SkillAddButton" runat="server" ClientIDMode="Static" Text="+ Add" OnClick="SkillAddButton_Click" /></span>
						</div>
						<div class="dataInputRow">
							<asp:Panel ID="SkillPanel" runat="server" CssClass="resumeEducationSkillList">
								<uc:UpdateableList ID="SkillUpdateableList" runat="server" DisplayBullet="false" AddConfirmationModal="false" />
							</asp:Panel>
						</div>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="SkillAddButton" EventName="Click" />
					</Triggers>
				</asp:UpdatePanel>
			</div>
		</asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="IndustryPlaceHolder">
      <div id="MostExperiencePanel">
				<div class="resumeSectionSubHeading">
					<focus:LocalisedLabel runat="server" ID="IndustrySubHeadingLabel" RenderOuterSpan="False" LocalisationKey="MostExperienceIndustriesSubHeading.Label" DefaultText="Most experienced industries"/>
				</div>
				<div class="resumeEducation resumeEducationSkills">
					<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="IndustryInstructionsLabel" RenderOuterSpan="False" LocalisationKey="IndustryInstructions.Label" DefaultText="Indicate up to two industries that you have the most exprience of working in."/></p>
					<asp:UpdatePanel ID="IndustryUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="dataInputRow">
								<span class="dataInputGroupedItemHorizontal">
									<span class="dataInputField">
										<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="IndustryLabel" AssociatedControlID="MostExperienceIndustriesTextBox" LocalisationKey="MostExperienceIndustries.Label" DefaultText="Experienced Industry" Width="250"/></span>
										<asp:TextBox ID="MostExperienceIndustriesTextBox" runat="server" Width="250" ClientIDMode="Static" MaxLength="200" AutoCompleteType="Disabled"  />	
										<act:AutoCompleteExtender ID="IndustrialClassificationAutoCompleteExtender" runat="server" TargetControlID="MostExperienceIndustriesTextBox" MinimumPrefixLength="2" 
															CompletionListCssClass="autocomplete_completionListElement" CompletionInterval="100" CompletionSetCount="15" UseContextKey="true"
															ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetNaics" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem" OnClientShown="fixAutoCompleteExtender"  /><br />
										<asp:CustomValidator runat="server" ID="IndustrialClassificationValidator" ControlToValidate="MostExperienceIndustriesTextBox" ClientValidationFunction="doesNAICSExist"  SetFocusOnError="true" Display="Dynamic" CssClass="error"/>
										 <asp:Label runat="server" ID="MaxIndustryLabel" Visible="False" CssClass="error"/>
									</span>
								</span>
								<span class="dataInputGroupedItemHorizontal"><asp:LinkButton ID="IndustryAddButton" runat="server" ClientIDMode="Static" Text="+ Add" OnClick="IndustryAddButton_Click" /></span>
							</div>
							<div class="dataInputRow">
								<asp:Panel ID="IndustryPanel" runat="server" CssClass="resumeEducationSkillList">
									<uc:UpdateableList ID="IndustryUpdateableList" runat="server" DisplayBullet="false" AddConfirmationModal="false" />
								</asp:Panel>
							</div>
					 </ContentTemplate>
							<Triggers>
							<asp:AsyncPostBackTrigger ControlID="IndustryAddButton" EventName="Click" />
						</Triggers>
					</asp:UpdatePanel>
				</div>
      </div>
		</asp:PlaceHolder>
    <asp:PlaceHolder runat="server" ID="TargetIndPlaceHolder">
      <div id="TargetIndustriesPanel">
				<div class="resumeSectionSubHeading">
					<focus:LocalisedLabel runat="server" ID="TargetIndSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="TargetIndSubHeading.Label" DefaultText="Targeted Industries"/>
				</div>
				<div class="resumeEducation resumeEducationSkills">
					<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="TargetIndInstructionsLabel" RenderOuterSpan="False" LocalisationKey="TargetIndInstructions.Label" DefaultText="Indicate up to two industries that you would like to target for your next role."/></p>
					 <asp:UpdatePanel ID="TargetIndustryUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="dataInputRow">
								<span class="dataInputGroupedItemHorizontal">
									<span class="dataInputField">
										<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="TargetIndLabel" AssociatedControlID="TargetIndTextBox" LocalisationKey="TargetIndustries.Label" DefaultText="Targeted Industry" Width="250"/></span>
										<asp:TextBox ID="TargetIndTextBox" runat="server" Width="250" ClientIDMode="Static" />	
										<act:AutoCompleteExtender ID="IndustrialTargetAutoCompleteExtender" runat="server" TargetControlID="TargetIndTextBox" MinimumPrefixLength="2" 
															CompletionListCssClass="autocomplete_completionListElement" CompletionInterval="100" CompletionSetCount="15" UseContextKey="true"
															ServicePath="~/Services/AjaxService.svc" ServiceMethod="GetNaics" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" CompletionListItemCssClass="autocomplete_listItem" OnClientShown="fixAutoCompleteExtender"  /><br />
										<asp:CustomValidator runat="server" ID="IndustrialTargetClassificationValidator" ControlToValidate="TargetIndTextBox" ClientValidationFunction="doesNAICSExist"  SetFocusOnError="true" Display="Dynamic" CssClass="error"/>
										 <asp:Label runat="server" ID="MaxTargetIndValidator" Visible="False" CssClass="error"/>
									</span>
								</span>
								<span class="dataInputGroupedItemHorizontal"><asp:LinkButton ID="TargetIndAddButton" runat="server" ClientIDMode="Static" Text="+ Add" OnClick="TargetIndAddButton_Click" /></span>
							</div>
							<div class="dataInputRow">
								<asp:Panel ID="TargetIndPanel" runat="server" CssClass="resumeEducationSkillList">
									 <uc:UpdateableList ID="TargetIndUpdateableList" runat="server" DisplayBullet="false" AddConfirmationModal="false" />
								</asp:Panel>
							</div>
							</ContentTemplate>
							<Triggers>
							<asp:AsyncPostBackTrigger ControlID="TargetIndAddButton" EventName="Click" />
						</Triggers>
					</asp:UpdatePanel>
				</div>
      </div>
		</asp:PlaceHolder>
	</div>
	<div class="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton1" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2 greyedOut" ValidationGroup="Education" OnClick="SaveMoveToNextStepButton_Click" disabled="disabled" ClientIDMode="Static"/>
	</div>
	
</div>

<asp:HiddenField ID="DegreeValidationDummyTarget" runat="server" />
<act:ModalPopupExtender ID="DegreeValidationModalPopup" runat="server" TargetControlID="DegreeValidationDummyTarget"
	PopupControlID="DegreeValidationModalPanel" BackgroundCssClass="modalBackground"
	CancelControlID="btnYesAdd" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="DegreeValidationModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">
	<div class="resumeDegreeValidationModal">
		<asp:Label ID="lblDegreeValidationInfo" runat="server" CssClass="modalMessage"></asp:Label>
		<div class="modalButtons">
			<asp:Button ID="btnNoThanks" runat="server" class="buttonLevel3" TabIndex="0" Text="No thanks" OnClick="btnNoThanks_Click" />
			<asp:Button ID="btnYesAdd" runat="server" class="buttonLevel2" Text="Go to Degrees and diplomas panel" />
		</div>
	</div>
</asp:Panel>

<script type="text/javascript">
    $(document).ready(function () {        
        DegreeRepeaterItems();
        LicenceRepeaterItems();
        MaskNCRCIsuueDate();
        DriverLicenseChange();
        SetEnrollmentStatusUIState();
        NCRCChanged();
    });
	
    $(document).ready(function () {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(NCRCChanged);
    });

    function SetEnrollmentStatusUIState() {
        var enrollmentStatus = $('#ddlEnrollmentStatus');
        if ($(enrollmentStatus).exists() && $(enrollmentStatus).is(':disabled'))
            DisableStyledDropDown(enrollmentStatus, false);
    }

    function GetRepeaterIndex(srcCtrlId) {
        var indexIdentifier = srcCtrlId.lastIndexOf('_') + 1;
        return srcCtrlId.substr(indexIdentifier, srcCtrlId.length - indexIdentifier);
    }
	
    function ValidateEnrolmentStatus(src, args) {
        var schoolStatus = $('#ddlEnrollmentStatus');
        if (!schoolStatus.is(":disabled")) {
            if (schoolStatus.children().first().is(':selected')) {
                args.IsValid = false;
            }
        }
    }

    function ValidateEnrolmentEducationLevel(src, args) {
        var schoolStatus = $('#ddlEnrollmentStatus').val();
        var education = $('#ddlEducationLevel');
        var educationLevel = $(education).val();
        educationLevel = educationLevel.substr(educationLevel.length - 2);
		
        if ($(education).children().first().is(':selected')) {
            src.innerHTML = '<%= EducationLevelRequired %>';
            args.IsValid = false;
        }
        else if ((schoolStatus == '<%= SchoolStatus.In_School_Post_HS %>' || schoolStatus == '<%= SchoolStatus.Not_Attending_School_HS_Graduate %>') && educationLevel <= 12) {
            src.innerHTML = "<%= EnrollmentStatusErrorMessage %>";
            args.IsValid = false;
        }
        else if ((schoolStatus == '<%= SchoolStatus.In_School_HS_OR_less %>' || schoolStatus == '<%= SchoolStatus.Not_Attending_School_OR_HS_Dropout %>' || schoolStatus == '<%= SchoolStatus.In_School_Alternative_School %>') && educationLevel > 12) {
            src.innerHTML = "<%= EducationLevelErrorMessage %>";
            args.IsValid = false;
        }
    }

    function validateState(src, args) {
        var license = $('#ddlDriverLicense').val();
        if (license != 'no' && license != 'select') {
            args.IsValid = ($('#ddlState').val() != '');
        }
    }

    function validateDriverLicense(src, args) {
        args.IsValid = ($('#ddlDriverLicense').val() != 'select');
    }

    function validateLicenseType(src, args) {
        var license = $('#ddlDriverLicense').val();
        if (license != 'no' && license != 'select') {
            args.IsValid = ($('#ddlLicenseType').val() != '');
        }
    }

    function ValidateDegreeName(src, args) {
        var controlId = src.id;
        if (isDegreeUpdated(GetRepeaterIndex(controlId))) {
            if (args.Value.length == 0) {
                src.innerHTML = '<%= DegreeNameRequired %>';
                args.IsValid = false;
            }
        }
    }

    function ValidateMajorSubject(src, args) {
        var controlId = src.id;
        if (isDegreeUpdated(GetRepeaterIndex(controlId))) {
            if (args.Value.length == 0) {
                src.innerHTML = '<%= MajorSubjectRequired %>';
                args.IsValid = false;
            }
        }
    }

    function ValidateSchoolUniversity(src, args) {
        var controlId = src.id;
        if (isDegreeUpdated(GetRepeaterIndex(controlId))) {
            if (args.Value.length == 0) {
                src.innerHTML = '<%= SchoolUniversityRequired %>';
                args.IsValid = false;
            }
        }
    }

    function ValidateGPA(src, args) {
        var controlId = src.id;
        if (isDegreeUpdated(GetRepeaterIndex(controlId))) {
            var regexp = /^([0-9]+(\.[0-9]{1,2})?)(\/[0-9]+(\.[0-9]{1,2})?)?$/g;
            var matches = regexp.exec(args.Value);
            if (matches != null && matches.length > 0) {
                var value = parseFloat(matches[1]);
                if (value < 0 || value > <%= MaxGPA %>) {
                    src.innerHTML = '<%= GPAOutofRange %>';
                    args.IsValid = false;
                }
            }
        }
    }
	
    function ValidateCompletionDate(src, value, isExpected, dateRequiredMessage, dateInvalidMessage, minimumMessage, maximumMessage, dobMessage) {
        var datePattern = /^((0[1-9])|(1[0-2]))\/(\d{4})$/;
        var ccur_date = new Date();
        var ccur_year = (ccur_date.getFullYear()).valueOf();
        var ccur_month = (ccur_date.getMonth()).valueOf() + 1;

        var date = value.valueOf().split("/");
        var year = date[1];
        var month = date[0];

        if (value === '__/____' || value === '') {
            src.innerHTML = dateRequiredMessage;
            return false;
        }
        if (datePattern.test(value) === false) {
            src.innerHTML = dateInvalidMessage;
            return false;
        }
		
        if (isExpected) {
            if ((ccur_year - year).valueOf() < -100) {
                src.innerHTML = minimumMessage;
                return false;
            }
            if (year < ccur_year || (ccur_year == year && month < ccur_month)) {
                src.innerHTML = maximumMessage;
                return false;
            }
        } else {
            if (year > ccur_year || (ccur_year == year && month > ccur_month)) {
                src.innerHTML = maximumMessage;
                return false;
            }
		
            var dobYear = (Education_DateOfBirth.getFullYear()).valueOf();
            var dobMonth = (Education_DateOfBirth.getMonth()).valueOf() + 1;
			
            if (year == dobYear + 10 && month < dobMonth) {
                src.innerHTML = Education_hasDOB ? dobMessage : minimumMessage;
                return false;
            }
            if (year < dobYear + 10) {
                src.innerHTML = Education_hasDOB ? dobMessage : minimumMessage;
                return false;
            } 
        }

        return true;
    }

    function ValidateCompletedDegree(src, args) {
        if (!isDegreeUpdated(GetRepeaterIndex(src.id))) {
            args.IsValid = true;
            return;
        }

        var isSelected = $('#' + src.id).parent().siblings('input:radio').is(':checked');
        if (!isSelected) {
            args.IsValid = true;
            return;
        }
		
        args.IsValid = ValidateCompletionDate(src, args.Value, false, '<%= CompletedDateRequired %>', '<%= DateErrorMessage %>', '<%=TooEarlyToCompleteMessage %>', '<%= FutureDateErrorMessage %>', '<%=TooYoungToCompleteMessage %>');
    }
	
    function ValidateExpectedCompletionDegree(src, args) {
        if (!isDegreeUpdated(GetRepeaterIndex(src.id))) {
            args.IsValid = true;
            return;
        }
		
        var isSelected = $('#' + src.id).parent().siblings('input:radio').is(':checked');
        if (!isSelected) {
            args.IsValid = true;
            return;
        }
		
        args.IsValid = ValidateCompletionDate(src, args.Value, true, '<%= ExpectedCompletionDateRequired %>', '<%= DateErrorMessage %>', '<%=ExpectedYearLimitsErrorMessage %>', '<%= PastDateErrorMessage %>', null);
    }

    function ValidateDegreeState(src, args) {
        var controlId = src.id;
        var index = GetRepeaterIndex(controlId);
        if (isDegreeUpdated(index)) {
            if ($('#' + controlId).siblings('select').children().first().is(':selected')) {
                src.innerHTML = '<%= StateRequired %>';
                args.IsValid = false;
            }
        }
        UpdateDegreeContactCountryCode(index);
    }

    function ValidateDegreeCountry(src, args) {
        var controlId = src.id;
        var index = GetRepeaterIndex(controlId);
        if (isDegreeUpdated(index)) {
            if ($('#' + controlId).siblings('select').children().first().is(':selected')) {
                src.innerHTML = '<%= CountryRequired %>';
                args.IsValid = false;
            }
        }
        UpdateDegreeContactStateCode(index);
    }

    function ValidateCompletionStatus(src, args){
        var controlId = src.id;
        var i = GetRepeaterIndex(controlId);
        if (isDegreeUpdated(i)) {
            var CompletedRadioButton = $('[id*=CompletedRadioButton_'+i+']').is(":checked");
            var ExpectedCompletionRadioButton  =$('[id*=ExpectedCompletionRadioButton_'+i+']').is(":checked");
            var UnfinishedCompletionRadioButton = $('[id*=UnfinishedCompletionRadioButton_'+i+']').is(":checked");
             if(CompletedRadioButton || ExpectedCompletionRadioButton || UnfinishedCompletionRadioButton) {
            //              
            }
            else
            {
                src.innerHTML = '<%= CompletionDateRequired %>';
                args.IsValid = false;
            }

        }
    }

    function UpdateDegreeContactCountryCode(id) {
        var stateValue = $('[id*=DegreeStateDropDownList_' + id + ']').val();
        var country = $('[id*=DegreeCountryDropDownList_' + id + ']');
        var countryValue = $(country).val();

        if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>' && countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
            $(country).children().first().prop('selected', true);
            $(country).next().find(':first-child').text($(country).children('option:selected').text()).parent().addClass('changed');
        }
        if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>' && stateValue != ' ' && countryValue != '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
            $(country).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>"]').prop('selected', true);
            $(country).next().find(':first-child').text($(country).children('option:selected').text()).parent().addClass('changed');
        }
    }

    function UpdateDegreeContactStateCode(id) {
        var state = $('[id*=DegreeStateDropDownList_' + id + ']');
        var stateValue = $(state).val();
        var countryValue = $('[id*=DegreeCountryDropDownList_' + id + ']').val();

        if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>' && countryValue != '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
            $(state).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>"]').prop('selected', true);
            $(state).next().find(':first-child').text($(state).children('option:selected').text()).parent().addClass('changed');
        }
        if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>' && countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
            $(state).children().first().prop('selected', true);
            $(state).next().find(':first-child').text($(state).children('option:selected').text()).parent().addClass('changed');
        }
    }

    function isDegreeUpdated(index) {
        var isUpdated = false;
		
        if ($('[id*=DegreeTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=MajorSubjectTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=SchoolUniTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=CompletedDegreeTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=ExpectedCompletionTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=CoursesTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=HonorsTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=GPATextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=ActivitiesTextBox_' + index + ']').val() != '')
            isUpdated = true;
        return isUpdated;
    }

    function ValidateProfessionalLicense(src, args) {
        var controlId = src.id;
        if (isLicenseUpdated(GetRepeaterIndex(controlId))) {
            if (args.Value.length == 0) {
                src.innerHTML = '<%= ProfessionalLicenseRequired %>';
                args.IsValid = false;
            }
        }
    }

    function ValidateIssuingOrg(src, args) {
        var controlId = src.id;
        if (isLicenseUpdated(GetRepeaterIndex(controlId))) {
            if (args.Value.length == 0) {
                src.innerHTML = '<%= IssuingOrganizationRequired %>';
                args.IsValid = false;
            }
        }
    }

    function ValidateCompletedLicense(src, args) {
        if (!isLicenseUpdated(GetRepeaterIndex(src.id))) {
            args.IsValid = true;
            return;
        }
		
        var isSelected = $('#' + src.id.replace("CompletedLicenceValidator", "CompletedLicenceRadioButton")).is(':checked');
        if (!isSelected) {
            args.IsValid = true;
            return;
        }		
		
        args.IsValid = ValidateCompletionDate(src, args.Value, false, '<%= LicenseCompletedDateRequired %>', '<%= DateErrorMessage %>', '<%=LicenseCompletedYearLimitsErrorMessage %>', '<%= LicenseCompletedDateTooLate %>', '<%=LicenseCompletedTooYoung %>');
    }

    function ValidateExpectedLicenceDate(src, args) {
        if (!isLicenseUpdated(GetRepeaterIndex(src.id))) {
            args.IsValid = true;
            return;
        }
		
        var isSelected = $('#' + src.id.replace("ExpectedLicenceValidator", "ExpectedLicenceRadioButton")).is(':checked');
        if (!isSelected) {
            args.IsValid = true;
            return;
        }		
		
        args.IsValid = ValidateCompletionDate(src, args.Value, true, '<%= LicenseExpectedDateRequired %>', '<%= DateErrorMessage %>', '<%=LicenseExpectedYearLimitsErrorMessage %>', '<%= LicenseExpectedDateTooEarly %>', null);
    }

    function ValidateLicenseState(src, args) {
        var controlId = src.id;
        var index = GetRepeaterIndex(controlId);
        if (isLicenseUpdated(index)) {
            if ($('#' + controlId).siblings('select').children('option:first').is(':selected')) {
                src.innerHTML = '<%= StateRequired %>';
                args.IsValid = false;
            }
        }
        UpdateLicenseContactCountryCode(index);
    }

    function ValidateLicenseCountry(src, args) {
        var controlId = src.id;
        var index = GetRepeaterIndex(controlId);
        if (isLicenseUpdated(index)) {
            if ($('#' + controlId).siblings('select').children('option:first').is(':selected')) {
                src.innerHTML = '<%= CountryRequired %>';
                args.IsValid = false;
            }
        }
        UpdateLicenseContactStateCode(index);
    }

    function UpdateLicenseContactCountryCode(id) {
        var stateValue = $('[id*=LicenseStateDropDownList_' + id + ']').val();
        var country = $('[id*=LicenseCountryDropDownList_' + id + ']');
        var countryValue = $(country).val();

        if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>' && countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
            $(country).children('option:first').prop('selected', true);
            $(country).next().find(':first-child').text($(country).children('option:selected').text()).parent().addClass('changed');
        }
        if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>' && stateValue != ' ' && countryValue != '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
            $(country).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>"]').prop('selected', true);
            $(country).next().find(':first-child').text($(country).children('option:selected').text()).parent().addClass('changed');
        }
    }

    function UpdateLicenseContactStateCode(id) {
        var state = $('[id*=LicenseStateDropDownList_' + id + ']');
        var stateValue = $(state).val();
        var countryValue = $('[id*=LicenseCountryDropDownList_' + id + ']').val();

        if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>' && countryValue != '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
            $(state).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>"]').prop('selected', true);
            $(state).next().find(':first-child').text($(state).children('option:selected').text()).parent().addClass('changed');
        }
        if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>' && countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
            $(state).children().first().prop('selected', true);
            $(state).next().find(':first-child').text($(state).children('option:selected').text()).parent().addClass('changed');
        }
    }

    function isLicenseUpdated(index) {
        var isUpdated = false;

        if ($('[id*=ProfessionalLicenceTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=IssuingOrgTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=CompletedLicenceTextBox_' + index + ']').val() != '')
            isUpdated = true;
        else if ($('[id*=ExpectedLicenceTextBox_' + index + ']').val() != '')
            isUpdated = true;

        return isUpdated;
    }

    function DriverLicenseChange() {
        var license = $('#ddlDriverLicense');
        var licenseState = $('#ddlState');
        var licenseType = $('#ddlLicenseType');

        if (license.length == 0) {
            return true;
        }
		
        if ($(license).val() != 'no' && $(license).val() != 'select') {
            if ($(license).val() == 'yes') {
                if ($(licenseState).children('option:first').is(':selected')) {
                    $(licenseState).children('option[value="<%= UserStateCode %>"]').prop('selected', true);
                    $(licenseState).next().find(':first-child').text($(licenseState).children('option:selected').text()).parent().addClass('changed');
                }

                if ($(licenseType).children('option:first').is(':selected')) {
                    $(licenseType).children('option[value="<%= DefaultDrivingLicense %>"]').prop('selected', true); // Keep "Class D/Regular Operator" as default selection
                    $(licenseType).next().find(':first-child').text($(licenseType).children('option:selected').text()).parent().addClass('changed');
					
                    var displayLicense = $('#chkUnHideClassD input:first');
                    displayLicense.prop('disabled', false);
                    displayLicense.parent().removeAttr('style');
                }
            }

            $(licenseState).prop('disabled', false).next().removeClass('stateDisabled');
            $(licenseType).prop('disabled', false).next().removeClass('stateDisabled');
        }
        else {
            $(licenseState).children().first().prop('selected', true);
            $(licenseState).next().find(':first-child').text($(licenseState).children('option:selected').text()).parent().addClass('changed');
            $(licenseType).children().first().prop('selected', true);
            $(licenseType).next().find(':first-child').text($(licenseType).children('option:selected').text()).parent().addClass('changed');
            $(licenseState).prop('disabled', true).next().addClass('stateDisabled');
            $(licenseType).prop('disabled', true).next().addClass('stateDisabled');
        }

        DriverLicenseTypeChange();

        return false;
    }

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        if (args.get_error() == undefined) {
            DriverLicenseChange();
            SetEnrollmentStatusUIState();
        }
    }

    function DriverLicenseTypeChange() {
        var driverLicense = $('#ddlLicenseType');
        var endorsementTypes = $('#EndorsementCheckBoxList');
        var displayLicense = $('#chkUnHideClassD');
		
        var driverLicenseValue = $(driverLicense).val();
        var arrEndorsementTypes = $(endorsementTypes).find('input');
        var arrDisplayLicense = $(displayLicense).find('input:first');

        if (driverLicenseValue === '0' || $(driverLicense).children().first().is(':selected') || driverLicenseValue === '<%= GetLookupId(Constants.CodeItemKeys.DrivingLicenceClasses.ClassD) %>') {

            if (driverLicenseValue === '<%= GetLookupId(Constants.CodeItemKeys.DrivingLicenceClasses.ClassD) %>') {
                $(arrDisplayLicense).prop('disabled', false).parent().removeAttr('style');
            }
            else {
                $(arrDisplayLicense).prop('checked', false);
                $(arrDisplayLicense).prop('disabled', true).parent().css('color', 'gray');
            }
        }
        else {
            $(arrDisplayLicense).prop('disabled', false).parent().removeAttr('style');
        }

        var licenceLicenceEndorsements = <%= LicenceEndorsementsJavascriptArray %>;

        var validEndorsements = licenceLicenceEndorsements[driverLicenseValue];

        if (!validEndorsements)
            validEndorsements = '';

        $(arrEndorsementTypes).each(function () {
            if (validEndorsements.indexOf($(this).val()) != -1) {
                $(this).prop('disabled', false).parent().removeAttr('style');
                return true;
            }
            $(this).prop('checked', false);
            $(this).prop('disabled', true).parent().css('color', 'gray');
        });

        return false;
    }  
	
    function ExpectedEndDateRadioButtonClicked(sender) {

        var index = GetRepeaterIndex(sender.id);
        var txtCompletedDate = $('[id*=CompletedDegreeTextBox_' + index + ']');
        var txtExpectedDate = $('[id*=ExpectedCompletionTextBox_' + index + ']');

        $(txtExpectedDate).prop('disabled', false);
        $(txtCompletedDate).val('').prop('disabled', true);
        $(txtCompletedDate).get(0).Mask = '99/9999';
        $(txtCompletedDate).get(0).PromptCharacter = '_';

        $('[id*=CompletedDegreeValidator_' + index + ']').html('');
    }
	
    function EndDateRadioButtonClicked(sender) {

        var index = GetRepeaterIndex(sender.id);
        var txtCompletedDate = $('[id*=CompletedDegreeTextBox_' + index + ']');
        var txtExpectedDate = $('[id*=ExpectedCompletionTextBox_' + index + ']');

        $(txtCompletedDate).prop('disabled', false);
        $(txtExpectedDate).val('').prop('disabled', true);
        $(txtExpectedDate).get(0).Mask = "99/9999";
        $(txtExpectedDate).get(0).PromptCharacter = "_";

        $('[id*=ExpectedCompletionValidator_' + index + ']').html('');
    }

        function UnfinishedEndDateRadioButtonClicked(sender) {

        var index = GetRepeaterIndex(sender.id);
        var txtCompletedDate = $('[id*=CompletedDegreeTextBox_' + index + ']');
        var txtExpectedDate = $('[id*=ExpectedCompletionTextBox_' + index + ']');

        $(txtCompletedDate).prop('disabled', true);
        $(txtExpectedDate).val('').prop('disabled', true);
        $(txtExpectedDate).prop('disabled', true);
        $(txtCompletedDate).val('').prop('disabled', true);

        $('[id*=ExpectedCompletionValidator_' + index + ']').html('');
        $('[id*=CompletedDegreeValidator_' + index + ']').html('');
    }
	
	
    function LicenceEndDateRadioButtonClicked(sender) {
        var index = GetRepeaterIndex(sender.id);
        var txtCompletedDate = $('[id*=CompletedLicenceTextBox_' + index + ']');
        var txtExpectedDate = $('[id*=ExpectedLicenceTextBox_' + index + ']');

        $(txtCompletedDate).prop('disabled', false);
        $(txtExpectedDate).val('').prop('disabled', true);
        $(txtExpectedDate).get(0).Mask = "99/9999";
        $(txtExpectedDate).get(0).PromptCharacter = "_";

        $('[id*=ExpectedLicenceValidator_' + index + ']').html('');		
    }

    function ExpectedLicenceEndDateRadioButtonClicked(sender) {
        var index = GetRepeaterIndex(sender.id);
        var txtCompletedDate = $('[id*=CompletedLicenceTextBox_' + index + ']');
        var txtExpectedDate = $('[id*=ExpectedLicenceTextBox_' + index + ']');

        $(txtExpectedDate).prop('disabled', false);
        $(txtCompletedDate).val('').prop('disabled', true);
        $(txtCompletedDate).get(0).Mask = '99/9999';
        $(txtCompletedDate).get(0).PromptCharacter = '_';

        $('[id*=CompletedLicenceValidator_' + index + ']').html('');
    }

    function doesNAICSExist(source, arguments)
    {
        var exists;

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "<%= UrlBuilder.AjaxService() %>/CheckNaicsExists", //+ arguments.Value,
            dataType: "json",
            data: '{"naicsText": "' + arguments.Value + '"}',
            async: false,
            success: function (result) {
                exists = (result.d);
            }
        });

        arguments.IsValid = exists;
    }

    function NCRCChanged() {
        var ncrcLevelDropdown = $('#ddlNCRCLevel');
        var ncrcStateDropdown = $('#ddlNCRCState');
        var ncrcIssueDateTextbox = $('#txtNCRCIssueDate');
	   
        if ($('#NCRCConfirmationCheckbox').is(':checked')) {
            EnableStyledDropDown(ncrcLevelDropdown);
            EnableStyledDropDown(ncrcStateDropdown);
            $(ncrcIssueDateTextbox).prop('disabled', false);
        }
        else {
            ncrcLevelDropdown.val('select');
            ncrcStateDropdown.val('select');
            ncrcIssueDateTextbox.val('');

            DisableStyledDropDown(ncrcLevelDropdown);
            DisableStyledDropDown(ncrcStateDropdown);
            $(ncrcIssueDateTextbox).val('').prop('disabled', true);
        }
    }
   
    function ValidateNCRCLevel(src, args) {
        if ($('#NCRCConfirmationCheckbox').is(':checked')) {
            var ncrcLevelDropdown = $('#ddlNCRCLevel');
			
            if (ncrcLevelDropdown.val() == 'select') {
                src.innerHTML = '<%= NCRCLevelRequiredErrorMessage %>';
                args.IsValid = false;
            }
        }
    }
	 
    function ValidateNCRCState(src, args) {
        if ($('#NCRCConfirmationCheckbox').is(':checked')) {
            var ncrcStateDropdown = $('#ddlNCRCState');
			
            if (ncrcStateDropdown.val() == 'select') {
                src.innerHTML = '<%= NCRCStateRequiredErrorMessage %>';
                args.IsValid = false;
            }
        }
    }
	 
    function ValidateNCRCIssueDate(src, args) {
        if ($('#NCRCConfirmationCheckbox').is(':checked')) {
            var ncrcIssueDateTextbox = $('#txtNCRCIssueDate');
			
            if (ncrcIssueDateTextbox.val() == '') {
                src.innerHTML = '<%= NCRCIssueDateRequiredErrorMessage %>';
                args.IsValid = false;
            }
            else {
                var NCRCIssueDate = moment(args.Value, "MM/DD/YYYY", true);
                if (!NCRCIssueDate.isValid()) {
                    src.innerHTML = '<%= NCRCIssueDateInvalidErrorMessage %>';
                    args.IsValid = false;
                }
                else if (NCRCIssueDate.isAfter(moment())) {
                    src.innerHTML = '<%= NCRCIssueDateInFutureErrorMessage %>';
                    args.IsValid = false;
                }
                else if (moment().year() - NCRCIssueDate.year() > 100) {
                    src.innerHTML = '<%= NCRCIssueDateInPastErrorMessage %>';
                    args.IsValid = false;
                }
            }
        }        
    }
    
    function DegreeRepeaterItems(){
        $("input[category='degreeCompletion']").each(function(){
            $(this).mask('99/9999');
        });        
        $("input[category='expectedCompletion']").each(function(){
            $(this).mask('99/9999');
        });
        return true;
    }

    function LicenceRepeaterItems(){
        $("input[category='licenceCompletion']").each(function(){
            $(this).mask('99/9999');
        });        
        $("input[category='expectedLicenceCompletion']").each(function(){
            $(this).mask('99/9999');
        });
        return true;
    }
    function MaskNCRCIsuueDate(){
        $("#txtNCRCIssueDate").mask('99/99/9999');
    }

    //# sourceURL=test.js
</script>
