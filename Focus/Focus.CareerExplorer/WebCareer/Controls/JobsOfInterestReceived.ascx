﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobsOfInterestReceived.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.JobsOfInterestReceived" %>
<asp:UpdatePanel ID="updJobList" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<p>
			<%= HtmlLocalise("Global.Show", "Show")%>&nbsp;
			<asp:DropDownList ID="NumberOfDaysDropDown" runat="server" OnSelectedIndexChanged="NumberOfDaysDropDown_OnSelectedIndexChanged" AutoPostBack="true" Width="150px" Title="Number of days"></asp:DropDownList>
		</p>
		<asp:Repeater ID="JobsOfInterestRepeater" runat="server" OnItemDataBound="JobsOfInterestRepeater_ItemDataBound">
				<HeaderTemplate>
					<table class="jobList" role="presentation">
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td class="column1">
							<span style="display: inline-block; width:80px">
								<asp:image ID="MatchScoreImage1" runat="server" alt="" />
								<asp:image ID="MatchScoreImage2" runat="server" alt=""/>
								<asp:image ID="MatchScoreImage3" runat="server" alt="" />
								<asp:image ID="MatchScoreImage4" runat="server" alt="" />
								<asp:image ID="MatchScoreImage5" runat="server" alt="" />
							</span>
						</td>
						<td class="column2">
							<asp:LinkButton ID="linkJobTitle" runat="server" OnClientClick="showProcessing();" OnClick="LinkJobTitle_Click"></asp:LinkButton>
						</td>
						<td class="column3">
							<asp:Literal ID="EmployerNameLabel" runat="server"></asp:Literal>
						</td>
						<td class="column4">
							<asp:Literal ID="LocationLabel" runat="server"></asp:Literal>
						</td>
						<td class="column5">
							<asp:Literal ID="InviteCreatedOnLabel" runat="server"></asp:Literal>
						</td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
						</tbody>
					</table>
				</FooterTemplate>
		</asp:Repeater>
		<asp:Literal runat="server" ID="NoJobsOfInterestReceived" Visible="False" />
	</ContentTemplate>
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="NumberOfDaysDropDown" EventName="SelectedIndexChanged" />
	</Triggers>
</asp:UpdatePanel>
