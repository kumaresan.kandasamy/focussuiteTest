﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class DownloadResume : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
        BindDropdowns();

      ScriptManager.GetCurrent(Page).RegisterPostBackControl(FindControl("btnDownloadResume"));
    }

    /// <summary>
    /// Binds the dropdowns.
    /// </summary>
		private void BindDropdowns()
		{
			ddlDownloadResumeFormat.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats), null, null);
		}

		/// <summary>
		/// Shows the modal for downloading resumes
		/// </summary>
		/// <param name="defaultResumeFormat">The default resume format.</param>
		/// <param name="defaultReviewResumeOrder">The default review resume order.</param>
		/// <param name="original">if set to <c>true</c> [original].</param>
		/// <param name="hasHiddenContact">Whether contact information has been hidden</param>
    public void Show(string defaultResumeFormat, string defaultReviewResumeOrder, bool original, bool hasHiddenContact)
		{
			if (original)
			{
				if (!ddlDownloadResumeFormat.Items.Contains(new ListItem("Original", "0")))
					ddlDownloadResumeFormat.Items.Insert(0, new ListItem("Original", "0"));

				ddlDownloadResumeOrder.Enabled = false;
			}
			ddlDownloadResumeFormat.SelectedValue = defaultResumeFormat;
			ddlDownloadResumeOrder.SelectedValue = defaultReviewResumeOrder;
			DownloadResumeModal.Show();

	    HiddenContactPlaceHolder.Visible = hasHiddenContact;
			rblHideOptions.SelectedIndex = hasHiddenContact ? 1 : 0;
		}

    /// <summary>
    /// Handles the Click event of the btnDownloadResume control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnDownloadResume_Click(object sender, EventArgs e)
    {
      try
      {
	      var resumeFormatId = ddlDownloadResumeFormat.SelectedValueToLong();
				var model = App.GetSessionValue<ResumeModel>("Career:Resume");
				
				if (resumeFormatId == 0)
				{
				  string fileName;
				  string mimeType;
				  string htmlResume;

          var resumeBytes = ServiceClientLocator.ResumeClient(App).GetOriginalUploadedResume(model.ResumeMetaInfo.ResumeId.Value, out fileName, out mimeType, out htmlResume);
					if (resumeBytes.IsNotNull())
					{
					  var isHtml = string.Compare(mimeType, "text/html", StringComparison.OrdinalIgnoreCase) == 0;

						HttpResponse response = HttpContext.Current.Response;
						response.Clear();
            response.AddHeader("Content-Type", mimeType);
            response.AddHeader("Content-Length", resumeBytes.Length.ToString(CultureInfo.InvariantCulture));
					  var length = isHtml ? htmlResume.Length : resumeBytes.Length;
            response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"; size=" + length);
						response.Flush();
            if (isHtml)
              response.Write(htmlResume);
            else
						  response.BinaryWrite(resumeBytes);
						response.Flush();
						response.End();
					}
				}
				else 
        {
					var lookupItem = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats).FirstOrDefault(x => x.Id == resumeFormatId);
					var xslPath = (lookupItem.IsNotNull()) ? App.Settings.GetCustomXslForResume(lookupItem.ExternalId) : String.Empty;

					try
					{
						xslPath = HttpContext.Current.Server.MapPath(xslPath);
					}
					catch { }

	        if (xslPath.IsNotNullOrEmpty())
	        {
						var resumeName = string.Empty;
		        var htmlResume = string.Empty;
		        
		        if (resumeFormatId != 0 && model.IsNotNull() && model.ResumeMetaInfo.IsNotNull() && model.ResumeMetaInfo.ResumeName.IsNotNullOrEmpty())
			        resumeName = model.ResumeMetaInfo.ResumeName.Replace("\\", "").Replace("/", "").Replace(":", "").Replace("*", "").Replace("?", "").Replace("\"", "").Replace("<", "").Replace(">", "").Replace("|", "");

		        var xResume = Utilities.FetchXMLResume(model);

		        htmlResume = (rblHideOptions.SelectedIndex == 0)
			        ? Utilities.ApplyTemplate(xResume, xslPath, App, ddlDownloadResumeOrder.SelectedValue, true, true)
              : Utilities.ApplyTemplate(xResume, xslPath, App, ddlDownloadResumeOrder.SelectedValue, false, true);

		        if (rdoDownloadType.SelectedIndex == 0)
			        Utilities.Export2RTF(htmlResume, resumeName);
		        else
			        Utilities.Export2PDF(htmlResume, resumeName);
	        }
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }
  }

}

