﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReferralsList.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.ReferralsList" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>

<asp:Panel ID="NoReferralsPanel" runat="server">
	<p>
	    <asp:Label runat="server" ID="NoResultsLabel"></asp:Label>
	</p>
</asp:Panel>
<asp:Panel ID="ReferralsPanel" runat="server">
	<asp:UpdatePanel ID="updJobList" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
      <p>
        <%= HtmlLocalise("Global.Show", "Show")%>
			  <asp:DropDownList ID="NumberOfReferralsDropDown" title="Number Of Referrals DropDown" runat="server" OnSelectedIndexChanged="NumberOfReferralsDropDown_OnSelectedIndexChanged" AutoPostBack="true" Width="150px"></asp:DropDownList>
			</p>
	    <asp:ListView ID="ReferralsListView" runat="server" OnItemDataBound="ReferralsListView_OnItemDataBound" DataKeyNames="Id">
	      <ItemTemplate>
	        <p>
	          <div style="display: inline-block; width: 80px; float:left">
	            <asp:image ID="ReferralScoreImage1" runat="server" alt=""/>
	            <asp:image ID="ReferralScoreImage2" runat="server" alt=""/>
	            <asp:image ID="ReferralScoreImage3" runat="server" alt=""/>
	            <asp:image ID="ReferralScoreImage4" runat="server" alt=""/>
	            <asp:image ID="ReferralScoreImage5" runat="server" alt=""/>
            </div>
            <div style="float: right; width:685px">
              <asp:HyperLink runat="server" ID="JobLink"></asp:HyperLink>
              <asp:Label runat="server" ID="JobDetails">
						    <asp:Literal runat="server" ID="OpenBracketLiteral"></asp:Literal>
                <asp:Literal runat="server" ID="ApprovalStatus"></asp:Literal>
						    <asp:Literal runat="server" ID="CloseBracketLiteral"></asp:Literal>
                <asp:PlaceHolder ID="EmployerDashPlaceHolder" ClientIDMode="Static" runat="server"> - </asp:PlaceHolder>
						    <asp:Literal runat="server" ID="EmployerName"></asp:Literal>
						    <asp:PlaceHolder ID="LocationDashPlaceHolder" ClientIDMode="Static" runat="server"> - </asp:PlaceHolder>
						     <asp:Literal runat="server" ID="Location"></asp:Literal>
                (<asp:Literal runat="server" ID="ApplicationDate"></asp:Literal>)
              </asp:Label>
	            <br />
              <asp:PlaceHolder runat="server" ID="ContactPlaceHolder">
	              <asp:Label runat="server" ID="ContactLabel"></asp:Label>
							  <asp:Literal runat="server" ID="EmployeeName"></asp:Literal>
							  <asp:Label runat="server" ID="CommaLabel">,</asp:Label>
							  <asp:Literal runat="server" ID="EmployeePhone" ></asp:Literal>
							  <asp:HyperLink runat="server" ID="EmployeeEmail"></asp:HyperLink>
							  <asp:Label runat="server" ID="ContactPreferencesLabel"></asp:Label>
							  <span id="AdditionalContactPreferencesLink_<%#((ReferralViewDto)Container.DataItem).Id%>">
							  <asp:HyperLink runat="server" ID="ShowAdditionalContactPreferencesLink" Visible="False" NavigateUrl="javascript:void(0);"
                        ClientIDMode="Predictable"></asp:HyperLink></span> <span style="display: none" id="AdditionalContactPreferences_<%#((ReferralViewDto)Container.DataItem).Id%>">
                          <asp:Literal runat="server" ID="AdditionalContactPreferencesLiteral"></asp:Literal>
                          <a href="javascript:void(0);" onclick="HideAdditionalContactPreferences(<%#((ReferralViewDto)Container.DataItem).Id%>);">
                            <%=HtmlLocalise("Hide.text", "Hide") %></a></span>
						  </asp:PlaceHolder>
              <asp:Literal runat="server" ID="ApprovalText" Visible="False"></asp:Literal>
            </div>
            <div style="clear:both"></div>
	        </p>
	      </ItemTemplate>
	    </asp:ListView>
		</ContentTemplate>
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="NumberOfReferralsDropDown" EventName="SelectedIndexChanged" />
		</Triggers>
	</asp:UpdatePanel>
</asp:Panel>
<script type="text/javascript">
	
	function ShowAdditionalContactPreferences(id) {
		$('#AdditionalContactPreferences_' + id).show();
		$('#AdditionalContactPreferencesLink_' + id).hide();
	}
	function HideAdditionalContactPreferences(id) {
		$('#AdditionalContactPreferences_' + id).hide();
		$('#AdditionalContactPreferencesLink_' + id).show();
	}
</script>

