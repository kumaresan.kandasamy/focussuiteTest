﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PasteTypeResume.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.JobSearch.PasteTypeResume" %>
<div class="panel panel-lightbox">
	<div class="panel-heading">
		<focus:LocalisedLabel runat="server" ID="lblHeader" LocalisationKey="PasteTypeResume.Label" DefaultText="Paste/type your resume" RenderOuterSpan="False" />
	</div>
	<div class="panel-body">
		<focus:LocalisedLabel runat="server" ID="lblPastedResume" AssociatedControlID="txtPastedResume"
			LocalisationKey="PasteTypeResume2.Label" DefaultText="Paste or type your resume, and the resume builder will help you format and enhance it." />
		<asp:TextBox ID="txtPastedResume" runat="server" ClientIDMode="Static" TextMode="MultiLine" Rows="18" CssClass="form-control" />
		<small><em><span id="lblWordsCount">0</span> words</em></small>
		<br />
		<asp:RequiredFieldValidator ID="valPlainResumeRequired" runat="server" ControlToValidate="txtPastedResume"
			CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="PasteResume" />
		<asp:CustomValidator runat="server" ID="valPlainResumeHtmlValidator" ControlToValidate="txtPastedResume" CssClass="error" SetFocusOnError="true" Display="Dynamic" 
		     ValidationGroup="PasteResume" ClientValidationFunction="PasteTypeResume_CheckHTMLTag"></asp:CustomValidator>
	</div>
	<div class="panel-footer">
		<focus:ModernButton ID="btnPastedResume" runat="server" CssClass="btn btn-default" IconCssClass="fa-caret-right" IconProvider="FontAwesome" IconPlacement="Right" OnClick="btnPastedResume_Click" ValidationGroup="PasteResume" />
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('#txtPastedResume').on('keyup keydown', function () {
			textCounter($(this), $('#lblWordsCount'));
		});

		$('#txtPastedResume').on('paste', function () {
			var pasteInput = this;
			setTimeout(function () {
				textCounter(pasteInput, $('#lblWordsCount'));
			}, 100);
		});
	});
	
	function PasteTypeResume_CheckHTMLTag(sender, args) {
		var patt = new RegExp("<[A-Za-z]");
		args.IsValid = !patt.test(args.Value);
	}
</script>