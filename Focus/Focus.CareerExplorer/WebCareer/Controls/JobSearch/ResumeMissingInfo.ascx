﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ResumeMissingInfo.ascx.cs"
            Inherits="Focus.CareerExplorer.WebCareer.Controls.JobSearch.ResumeMissingInfo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>

<asp:HiddenField ID="ResumeMissingInfoModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ResumeMissingInfoModal" runat="server" BehaviorID="ResumeMissingInfoModal"
                        TargetControlID="ResumeMissingInfoModalDummyTarget" PopupControlID="ResumeMissingInfoModalPanel"
                        BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
  <Animations>
    <OnShown>
      <ScriptAction Script="UpdateCountyAndTextCounter();" />  
    </OnShown>
  </Animations>
</act:ModalPopupExtender>
<asp:Panel ID="ResumeMissingInfoModalPanel" tabindex="-1" runat="server" CssClass="modal" style="display:none;">
  <div class="resumeMissingInfoModal" id="ResumeMissingInfoLightBox" style="overflow:auto">
	  <div class="modalHeader">
		  <input type="image" src="<%= UrlBuilder.Close() %>" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" class="modalCloseIcon" onclick="$find('ResumeMissingInfoModal').hide();return false;" />
	  </div>
		<asp:Label ID="UploadedInfo" runat="server" Text="Your resume has been uploaded successfully. However, we weren't able to identify the following information:" CssClass="modalMessage"></asp:Label>
    <asp:Panel ID="ResumeMissingInfoDetail" runat="server" CssClass="resumeMissingInfoDetail">
	    <asp:Panel runat="server" ID="ContactPanel" CssClass="collapsiblePanel">
			  <div class="collapsiblePanelHeader">
				  <div class="cpIcon"></div>
					<focus:LocalisedLabel runat="server" ID="ContactInfoLabel" CssClass="cpHeaderControl collapsiblePanelHeaderLabel" LocalisationKey="ContactInfo.Label" DefaultText="Contact Information"/>
			  </div>
				<div class="collapsiblePanelContent">
					<asp:Panel runat="server" ID="NameInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<span class="dataInputGroupedItemHorizontal">
								<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="FirstNameTextBoxLabel" AssociatedControlID="FirstNameTextBox" LocalisationKey="FirstNameTextBox.Label" DefaultText="First name*" Width="220"/></span>
								<asp:TextBox ID="FirstNameTextBox" runat="server" ClientIDMode="Static" Width="220px" MaxLength="20" />
								<br />
								<asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstNameTextBox" CssClass="error" SetFocusOnError="false" Display="Dynamic" ValidationGroup="ResumeMissingInfo" />
							</span>
							<span class="dataInputGroupedItemHorizontal">
								<span class="dataInputGroupedItemHorizontal">
									<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="LastNameTextBoxLabel" AssociatedControlID="LastNameTextBox" LocalisationKey="LastNameTextBox.Label" DefaultText="Last name*" Width="220"/></span>
									<asp:TextBox ID="LastNameTextBox" runat="server" ClientIDMode="Static" Width="220px" MaxLength="20" />
									<br />
									<asp:RequiredFieldValidator ID="LastNameRequired" runat="server" ControlToValidate="LastNameTextBox" CssClass="error" SetFocusOnError="false" Display="Dynamic" ValidationGroup="ResumeMissingInfo" />
								</span>
							</span>
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="AddressInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="AddressTextBoxLabel" AssociatedControlID="AddressTextBox" LocalisationKey="AddressTextBox.Label" DefaultText="Address*" Width="220"/></span>
							<asp:TextBox ID="AddressTextBox" runat="server" ClientIDMode="Static" Width="220px" />
							<br />
							<asp:RequiredFieldValidator ID="AddressRequired" runat="server" ControlToValidate="AddressTextBox" CssClass="error" SetFocusOnError="false" Display="Dynamic" ValidationGroup="ResumeMissingInfo" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="CityInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="CityTextBoxLabel" AssociatedControlID="CityTextBox" LocalisationKey="CityTextBox.Label" DefaultText="City*" Width="220"/></span>
							<asp:TextBox ID="CityTextBox" runat="server" ClientIDMode="Static" Width="220px" />
							<br/>
							<asp:RequiredFieldValidator ID="CityRequired" runat="server" ControlToValidate="CityTextBox" CssClass="error" SetFocusOnError="false" Display="Dynamic" ValidationGroup="ResumeMissingInfo" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="StateInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<asp:DropDownList ID="ddlState" runat="server" Width="300px" ClientIDMode="Static" onchange="UpdateCountry();" Title="State Drop Down">
								<asp:ListItem Text="- select a state -" />
							</asp:DropDownList>
							<br/>
							<asp:RequiredFieldValidator ID="StateRequired" runat="server" ControlToValidate="ddlState" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="CountyInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<focus:AjaxDropDownList ID="ddlCounty" runat="server" Width="300px" ClientIDMode="Static" onchange="UpdateStateCountry()" Title="County Drop Down"/>
							<ajaxtoolkit:CascadingDropDown ID="ccdCounty" runat="server" Category="County" LoadingText="[Loading counties...]"
								ParentControlID="ddlState" ServiceMethod="GetCounty" ServicePath="~/Services/AjaxService.svc"
								BehaviorID="ccdCounty" TargetControlID="ddlCounty" PromptText="- select county -" PromptValue="">
							</ajaxtoolkit:CascadingDropDown>
							<br/>
							<asp:RequiredFieldValidator ID="CountyRequired" runat="server" ControlToValidate="ddlCounty" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="CountryInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<asp:DropDownList ID="ddlCountry" runat="server" Width="300px" ClientIDMode="Static" onchange="UpdateStateCounty();" Title="Country Drop Down">
								<asp:ListItem Text="United States" />
							</asp:DropDownList>
							<br/>
							<asp:RequiredFieldValidator ID="CountryRequired" runat="server" ControlToValidate="ddlCountry" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="ZipCodeInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="ZipCodeTextBoxLabel" AssociatedControlID="ZipCodeTextBox" LocalisationKey="ZipCodeTextBox.Label" DefaultText="ZIP Code*" Width="220"/></span>
							<asp:TextBox ID="ZipCodeTextBox" runat="server" ClientIDMode="Static" Width="220px" MaxLength="9" />
							<ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="ZipCodeTextBox" FilterType="Numbers" />
							<br />
							<asp:CustomValidator ID="ZipRequired" runat="server" ControlToValidate="ZipCodeTextBox" ClientIDMode="Static" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" ClientValidationFunction="ValidateZipCode" ValidateEmptyText="true" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="PhoneInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<span class="dataInputGroupedItemHorizontal">
								<asp:TextBox ID="PhoneNoTextBox" runat="server" ClientIDMode="Static" Width="220px" Title="Phone Number Text"/>
								<focus:LocalisedLabel runat="server" ID="PhoneNumberRequiredLabel" CssClass="requiredData" LocalisationKey="RedAsterisk.Label" DefaultText=""/>
								<%--<ajaxtoolkit:MaskedEditExtender ID="mePhoneNo" BehaviorID="meBehaviourPhoneNo" runat="server" Mask="(999) 999-9999" MaskType="Number" TargetControlID="PhoneNoTextBox" PromptCharacter="_" ClearMaskOnLostFocus="false" AutoComplete="false"></ajaxtoolkit:MaskedEditExtender>--%>
							</span>
							<span class="dataInputGroupedItemHorizontal">
								<asp:DropDownList ID="PhoneNoDropDownList" runat="server" Width="85px" ClientIDMode="Static" OnChange="ChangePhoneNoFormat();" Title="Phone Type Drop Down">
									<asp:ListItem Value="" Text="- select -" />
									<asp:ListItem Value="Home" Text="Home" />
									<asp:ListItem Value="Work" Text="Work" />
									<asp:ListItem Value="Cell" Text="Cell" />
									<asp:ListItem Value="Fax" Text="Fax" />
									<asp:ListItem Value="NonUS" Text="Non US" />
								</asp:DropDownList>
							</span>
							<asp:CustomValidator ID="PhoneNoValidator" runat="server" ControlToValidate="PhoneNoTextBox" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" ClientValidationFunction="ValidatePhoneNo" ValidateEmptyText="true" ClientIDMode="Static" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="EmailInfo" CssClass="dataInputRow">
						<span class="dataInputField">
							<span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="EmailTextBoxLabel" AssociatedControlID="EmailTextBox" LocalisationKey="EmailTextBox.Label" DefaultText="Email address*" Width="220"/></span>
							<asp:TextBox ID="EmailTextBox" runat="server" ClientIDMode="Static" Width="220px" />
							<br/>
							<asp:RegularExpressionValidator ID="valEmailAddress" runat="server" ControlToValidate="EmailTextBox" CssClass="error" Display="Dynamic" SetFocusOnError="false" ValidationGroup="ResumeMissingInfo"></asp:RegularExpressionValidator>
							<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailTextBox" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" />
						</span>
					</asp:Panel>
				</div>
	    </asp:Panel>
			<asp:Panel runat="server" ID="EducationPanel" CssClass="collapsiblePanel">
				<div class="collapsiblePanelHeader">
				  <div class="cpIcon"></div>
					<focus:LocalisedLabel runat="server" ID="EducationInfoLabel" CssClass="cpHeaderControl collapsiblePanelHeaderLabel" LocalisationKey="EducationInfo.Label" DefaultText="Education Information"/>
			  </div>
				<div class="collapsiblePanelContent">
					<asp:Panel runat="server" ID="EnrollmentStatus" CssClass="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="EnrollmentStatusLabel" AssociatedControlID="ddlEnrollmentStatus" CssClass="dataInputLabel requiredData" LocalisationKey="EnrollmentStatus.Label" DefaultText="Enrollment status"/>
						<span class="dataInputField">
							<asp:DropDownList ID="ddlEnrollmentStatus" runat="server" Width="300px" ClientIDMode="Static"></asp:DropDownList>
							<br />
							<asp:RequiredFieldValidator ID="EnrollmentStatusRequired" runat="server" ControlToValidate="ddlEnrollmentStatus" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="Education" CssClass="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="EducationLevelLabel" AssociatedControlID="ddlEducationLevel" CssClass="dataInputLabel requiredData" LocalisationKey="EducationLevel.Label" DefaultText="Education level"/>
						<span class="dataInputField">
							<asp:DropDownList ID="ddlEducationLevel" runat="server" Width="300px" ClientIDMode="Static"></asp:DropDownList>
							<br />
							<asp:CustomValidator ID="custEnrolmentEducationLevel" runat="server" ControlToValidate="ddlEducationLevel" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" ClientValidationFunction="ValidateEnrolmentEducationLevel" ValidateEmptyText="true" />
						</span>
					</asp:Panel>
				</div>
			</asp:Panel>
			<asp:Panel runat="server" ID="LicensePanel" CssClass="collapsiblePanel">
				<div class="collapsiblePanelHeader">
					<div class="cpIcon"></div>
					<focus:LocalisedLabel runat="server" ID="LicenseInfoLabel" CssClass="cpHeaderControl collapsiblePanelHeaderLabel" LocalisationKey="LicenseInfo.Label" DefaultText="License Information"/>
				</div>
				<div class="collapsiblePanelContent">
					<asp:Panel runat="server" ID="DriverLicense" CssClass="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="DriverLicenseDropDownLabel" AssociatedControlID="DriverLicenseDropDown" CssClass="dataInputLabel requiredData" LocalisationKey="DriverLicenseDropDown.Label" DefaultText="Driver's license"/>
						<span class="dataInputField">
							<asp:DropDownList ID="DriverLicenseDropDown" runat="server" Width="300px" ClientIDMode="Static" onchange="return DriverLicenseChange();"></asp:DropDownList>
							<br />
							<asp:CustomValidator ID="DriverLicenseRequired" runat="server" ControlToValidate="DriverLicenseDropDown" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" ClientValidationFunction="ValidateDriverLicense" ValidateEmptyText="true" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="DriverState" CssClass="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="StateOfIssueLabel" AssociatedControlID="DriverLicenseStateDropDown" CssClass="dataInputLabel requiredData" LocalisationKey="StateOfIssue.Label" DefaultText="State of issue"/>
						<span class="dataInputField">
							<asp:DropDownList ID="DriverLicenseStateDropDown" runat="server" Width="300px" ClientIDMode="Static"></asp:DropDownList>
							<br />
							<asp:CustomValidator ID="DriverLicenseStateRequired" runat="server" ControlToValidate="DriverLicenseStateDropDown" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" ClientValidationFunction="ValidateDriverState" ValidateEmptyText="true" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="DriverType" CssClass="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="TypeOfLicenseLabel" AssociatedControlID="DriverLicenseTypeDropDown" CssClass="dataInputLabel requiredData" LocalisationKey="TypeOfLicense.Label" DefaultText="Type of license"/>
						<span class="dataInputField">
							<asp:DropDownList ID="DriverLicenseTypeDropDown" runat="server" Width="300px" ClientIDMode="Static" onchange="return DriverLicenseTypeChange();"/>
							<br />
							<asp:CustomValidator ID="DriverLicenseTypeRequired" runat="server" ControlToValidate="DriverLicenseTypeDropDown" SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="ResumeMissingInfo" ClientValidationFunction="ValidateLicenseType" ValidateEmptyText="true" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="DriverEndorsements" CssClass="dataInputRow">
						<focus:LocalisedLabel runat="server" ID="LicenseEndorsementsLabel"  Visible="false" AssociatedControlID="EndorsementCheckBoxList" CssClass="dataInputLabel" LocalisationKey="LicenseEndorsements.Label" DefaultText="Endorsements (check all that apply)"/>
						<span class="dataInputField">
							<asp:CheckBoxList ID="EndorsementCheckBoxList" RepeatColumns="2" runat="server" ClientIDMode="Static" CssClass="dataInputHorizontalCheckBoxList" />
						</span>
					</asp:Panel>
					<asp:Panel runat="server" ID="HideLicense" CssClass="dataInputRow">
						<div class="dataInputFieldNoLabel">
							<asp:CheckBoxList ID="HideLicenseCheckBox" runat="server" ClientIDMode="Static" CssClass="dataInputHorizontalCheckBoxList"></asp:CheckBoxList>
						</div>
					</asp:Panel>
				</div>
			</asp:Panel>
    </asp:Panel>
		<focus:LocalisedLabel runat="server" ID="DemographicInfoLabel" CssClass="modalMessage" LocalisationKey="DemographicInfo.Label" DefaultText="You can now review what we have extracted from your uploaded resume and add some simple demographic information that we require along with your search preferences."/>
		<div>
			<focus:AjaxValidationSummary ID="ResumeMissingInfoValidationSummary" runat="server" DisplayMode="BulletList" CssClass="error" ValidationGroup="ResumeMissingInfo" />
		</div>
		<div class="modalButtons">
			<asp:Button ID="btnSaveMissingResumeInfo" Text="Continue" runat="server" class="buttonLevel2" OnClick="btnSaveMissingResumeInfo_Click" ValidationGroup="ResumeMissingInfo" />
		</div>
  </div>
</asp:Panel>
<script type="text/javascript">
	Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
	function EndRequestHandler(sender, args) {
		if (args.get_error() == undefined) {
		  if (window.DriverLicenseChange) {
		    DriverLicenseChange();
		  }
		}
		 }
		 Sys.Application.add_load(ResumeMissingInfoModal_PageLoad);

		 function ResumeMissingInfoModal_PageLoad(sender, args) {
		 /*
		   var behavior = $find('<%=ccdCounty.BehaviorID %>');
		   if (behavior != null) {
		     behavior.add_populated(function () {
		       var countyDropDown = $('#ddlCounty');
		       var stateDropDown = $("#ddlState"); ;
		       countyDropDown.next().find(':first-child').text(countyDropDown.children('option:selected').text()).parent().addClass('changed');
		       UpdateStyledDropDown(countyDropDown, stateDropDown.val());
		     });
		   }
		   */
		 	// Jaws Compatibility
		 	var modal = $find('ResumeMissingInfoModal');
			 if (modal != null) {
			 		modal.add_shown(function () {
			 			$('.modal').attr('aria-hidden', false);
			 			$('.page').attr('aria-hidden', true);
			 			$('#FirstNameTextBox').focus();
				 });

					modal.add_hiding(function () {
						$('.modal').attr('aria-hidden', true);
						$('.page').attr('aria-hidden', false);
				 });
			 }
			}
</script>
