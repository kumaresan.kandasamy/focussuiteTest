﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateEditResume.ascx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.Controls.JobSearch.CreateEditResume" %>

<asp:PlaceHolder runat="server" ID="CreateResumePanel">
  <div class="panel panel-lightbox">
	  <div class="panel-heading">
		  <focus:LocalisedLabel runat="server" ID="lblHeader" LocalisationKey="CreateResume.Label" DefaultText="Create a resume" RenderOuterSpan="False" />
	  </div>
		<div class="panel-body">
			<focus:LocalisedLabel runat="server" ID="lblDetail" LocalisationKey="ResumeBuilder.Label" DefaultText="The resume builder will guide you through all the steps of creating a resume. On average, creating a complete resume takes between 15-30 minutes, depending on how much detail you wish to provide. You'll be able to save your work and return to it at any time." />
		</div>
		<div class="panel-footer">
			<focus:ModernButton ID="btnStartResume" runat="server" class="btn btn-default" IconProvider="FontAwesome" IconPlacement="Right" IconCssClass="fa-caret-right" OnClick="btnStartResume_Click" />
		</div>
  </div>
</asp:PlaceHolder>