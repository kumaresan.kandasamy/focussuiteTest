﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using System.Collections.Generic;
using Framework.Core;
using System.Web.UI.WebControls;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.JobSearch
{
  public partial class SavedSearch : UserControlBase
	{
    #region Page Properties
    protected string AlertFrequencyRequired = "";
    protected string AlertFormatRequired = "";
    protected string AlertEmailRequired = "";
  	private long _savedSearchId;
    #endregion


    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);
      ConfirmationModal.CloseCommand += ConfirmationModal_CloseCommand;
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      var savedSearchId = App.GetSessionValue<long>("Career:SavedSearchId");

      if (!savedSearchId.IsNotNull() || savedSearchId <= 0) return;
      _savedSearchId = savedSearchId;
      var searchList = App.GetSessionValue<List<SearchDetails>>("Career:SavedSearches");

			if (IsLastAlert(searchList, _savedSearchId)) 
	    {
				// If a UI claimant has only one saved search disable switching off of emails.
				if (ServiceClientLocator.CandidateClient(App).GetUiClaimantStatus(App.User.PersonId.GetValueOrDefault()))
		    {
			    EmailSavedSearchCheckbox.Visible = true;
			    EmailSavedSearchCheckbox.Enabled = false;
		    }
			}

			if (searchList.IsNullOrEmpty())
				return;

      var searchDetails = searchList.FirstOrDefault(x => x.SearchId == savedSearchId);
      if (!IsPostBack)
      {
        //TODO: Martha: (check) do we need the AlertFrequencyRequired fields etc ?? 
        LocaliseUI();
        Bind(searchDetails);
      }
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      AlertFrequencyRequired = CodeLocalise("AlertFrequency.RequiredErrorMessage", "Alert frequency is required");
      AlertFormatRequired = CodeLocalise("AlertFormat.RequiredErrorMessage", "Email format is required");
      AlertEmailRequired = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
      valEmailAddress.ErrorMessage = CodeLocalise("EmailAddressValidate.ErrorMessage", "Email address is not correct format");

      EmailSavedSearchCheckbox.Text = HtmlLocalise("SendMeEmail.Label", "Send me email");
    }

    /// <summary>
    /// Binds the specified search details.
    /// </summary>
    /// <param name="searchDetails">The search details.</param>
    private void Bind(SearchDetails searchDetails)
    {
      if (searchDetails.IsNotNull())
      {
        lblSearchName.Text = searchDetails.Name;
        lblLastSaved.Text = string.Format("Last saved on {0}", searchDetails.SavedTime.ToString("m"));

        if(searchDetails.SearchCriteria.IsNotNull() && searchDetails.SearchCriteria.ReferenceDocumentCriteria.IsNotNull())
				{
					hdnResumeAssociation.Value = searchDetails.SearchCriteria.ReferenceDocumentCriteria.DocumentId;
        }
      }

      //int tempId;
      //var criterion = searchDetails.IsNotNull() ? (ReferenceDocumentCriterion)searchDetails.SearchCriteria.Where(x => x is ReferenceDocumentCriterion || x is ReferencePostingCriterion).FirstOrDefault() : null;
      //var resumeId = criterion.IsNotNull() && Int32.TryParse(criterion.DocumentId, out tempId) ? criterion.DocumentId : (App.UserData.HasDefaultResume ? App.UserData.DefaultResume.ResumeId : null);
      //var resumeInfo = ServiceClientLocator.ResumeClient(App).GetResumeNames();
      //ddlResumeNames.BindLookup(resumeInfo, resumeId, null);

      EmailAlertFrequencies? alertFrequency = null;
      EmailFormats? alertFormat = null;

      if (searchDetails.IsNotNull() && searchDetails.JobAlert.IsNotNull())
      {
        lnkDeleteSearch.Visible = false;
        lnkUnsubscribeAlert.Visible = true;
        alertFrequency = searchDetails.JobAlert.Frequency;
        alertFormat = searchDetails.JobAlert.Format;
        txtEmail.Text = searchDetails.JobAlert.ToEmailAddress;
        EmailSavedSearchCheckbox.Checked = searchDetails.JobAlert.EmailRequired;

        ddlAlertFrequency.Enabled = ddlAlertFormat.Enabled = txtEmail.Enabled = searchDetails.JobAlert.EmailRequired;
      }
      else
      {
        lnkUnsubscribeAlert.Visible = false;
        lnkDeleteSearch.Visible = true;
      }
      App.SetSessionValue("Career:SearchCriteria", searchDetails.SearchCriteria);

      BindAlertFrequencyDropDown(alertFrequency);
      BindAlertFormatDropDown(alertFormat);
    }

    /// <summary>
    /// Binds the alert frequency drop down.
    /// </summary>
    /// <param name="selectedValue">The selected value.</param>
    private void BindAlertFrequencyDropDown(EmailAlertFrequencies? selectedValue)
    {
      ddlAlertFrequency.Items.Clear();

      ddlAlertFrequency.Items.AddEnum(EmailAlertFrequencies.Daily, "Daily");
      ddlAlertFrequency.Items.AddEnum(EmailAlertFrequencies.Weekly, "Weekly");

      ddlAlertFrequency.Items.AddLocalisedTopDefault("ddlAlertFrequency.TopDefault", "- select alert frequency -");

      if (selectedValue.HasValue)
        ddlAlertFrequency.SelectValue(selectedValue.ToString());
    }

    /// <summary>
    /// Binds the alert format drop down.
    /// </summary>
    /// <param name="selectedValue">The selected value.</param>
    private void BindAlertFormatDropDown(EmailFormats? selectedValue)
    {
      ddlAlertFormat.Items.Clear();
      ddlAlertFormat.Items.AddEnum(EmailFormats.HTML, "Text and pictures (HTML)");
      ddlAlertFormat.Items.AddEnum(EmailFormats.TextOnly, "Text only");
      ddlAlertFormat.Items.AddLocalisedTopDefault("ddlAlertFormat.TopDefault", "- select email format -");
      if (selectedValue.HasValue)
        ddlAlertFormat.SelectValue(selectedValue.ToString());
    }

    /// <summary>
    /// Handles the Click event of the lnkDeleteSaveSearch control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void lnkDeleteSaveSearch_Click(object sender, EventArgs e)
    {
			var status = false;
			try
			{
				if (App.Settings.Theme == FocusThemes.Workforce)
				{
					var searchList = App.GetSessionValue<List<SearchDetails>>("Career:SavedSearches");
					if (IsLastAlert(searchList, _savedSearchId))
					{
						var isUiClaimant = ServiceClientLocator.CandidateClient(App).GetUiClaimantStatus(Convert.ToInt64(App.User.PersonId), false);
						if (isUiClaimant)
						{
							ConfirmationModal.Show("Unable to unsubscribe", "Due to your UI claimant status, you must have at least one search alert enabled", "Ok", "REDIRECT_CURRENT", height: 50, width: 600);
							return;
						}
					}
				}

				if (App.User.IsAuthenticated)
				{
					status = ServiceClientLocator.AnnotationClient(App).DeleteSaveSearch(_savedSearchId);
				}

				if (status)
				{
					var searchList = App.GetSessionValue<List<SearchDetails>>("Career:SavedSearches", null);

					searchList.RemoveAll(x => x.Name == lblSearchName.Text.Trim());
					if (searchList.IsNullOrEmpty())
					{
						App.RemoveSessionValue("Career:SavedSearchId");
					}
					else
					{
						var savedSearchId = App.GetSessionValue<long>("Career:SavedSearchId");

						if (savedSearchId.IsNotNull() && savedSearchId > 0)
							App.SetSessionValue("Career:SavedSearchId", searchList.First().SearchId);
					}


				  App.SetSessionValue("Career:SavedSearches", searchList);
					App.RemoveSessionValue("Career:SavedSearchName");
					App.RemoveSessionValue("Career:SearchCriteria");

					ConfirmationModal.Show("Saved search deleted", "Saved search '" + lblSearchName.Text.Trim() + "' has been deleted. We are taking you to your home page.", "Ok", "REDIRECT_HOME", height: 50, width: 400);
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
    }

    protected void btnSaveSearch_Click(object sender, EventArgs e)
    {
      try
      {
        var searchName = lblSearchName.Text.Trim();
        var searchCriteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");

        var frequency = EmailAlertFrequencies.Weekly;
        var format = EmailFormats.HTML;
        var toEmailAddress = "";

        if (EmailSavedSearchCheckbox.Checked)
        {
          if (!string.IsNullOrEmpty(ddlAlertFrequency.SelectedValue))
            frequency = ddlAlertFrequency.SelectedValueToEnum(EmailAlertFrequencies.Weekly);
          if (!string.IsNullOrEmpty(ddlAlertFormat.SelectedValue))
            format = ddlAlertFormat.SelectedValueToEnum(EmailFormats.HTML);

          toEmailAddress = txtEmail.TextTrimmed();
        }

        const AlertStatus alertStatus = AlertStatus.Active;

				ServiceClientLocator.AnnotationClient(App).SaveSearch(_savedSearchId, searchName, searchCriteria, SavedSearchTypes.CareerPostingSearch, frequency, format,
                                             toEmailAddress, alertStatus, EmailSavedSearchCheckbox.Checked);

        ConfirmationModal.Show("", "Your changes to '" + lblSearchName.Text.Trim() + "' have been saved. We are taking you to home page to view updated results.", "Ok", "REDIRECT_HOME", height: 50, width: 400);
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the CloseCommand event of the ConfirmationModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    public void ConfirmationModal_CloseCommand(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "REDIRECT_HOME":
          try
          {
            Response.RedirectToRoute("home");
          }
          catch (Exception)
          { }

          break;
				case "REDIRECT_CURRENT":
					Response.Redirect(Request.RawUrl);
					break;
      }
    }

		/// <summary>
		/// Check if this is the last email alert
		/// </summary>
		/// <param name="searchList">The list of all searches</param>
		/// <param name="savedSearchId">The id of the current search</param>
		/// <returns>A boolean indicating if there are no more search alerts</returns>
		private static bool IsLastAlert(List<SearchDetails> searchList, long savedSearchId)
		{
			if (searchList.IsNullOrEmpty())
				return true;

			var currentSearch = searchList.FirstOrDefault(s => s.SearchId == savedSearchId);
			if (currentSearch.IsNull() || currentSearch.JobAlert.IsNull() || !currentSearch.JobAlert.EmailRequired)
				return false;

			return !searchList.Any(s => s.SearchId != savedSearchId && s.JobAlert.IsNotNull() && s.JobAlert.EmailRequired);
		}
  }
}