﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.JobSearch
{
	public partial class PasteTypeResume : UserControlBase
	{
		public int PastedRows { get; set; }

		public event EventHandler PastedResumeClick;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();

			txtPastedResume.Rows = PastedRows;
		}

		/// <summary>
		/// Handles the Click event of the btnPastedResume control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void btnPastedResume_Click(object sender, EventArgs e)
		{
			try
			{
				var tResume = ServiceClientLocator.UtilityClient(App).GetPlainDocument(DocumentType.Resume, txtPastedResume.Text.Trim());
				if (!String.IsNullOrEmpty(tResume))
				{
					var resume = ServiceClientLocator.ResumeClient(App).GetResumeObject(tResume);
	        
					resume.ResumeMetaInfo.ResumeCreationMethod = ResumeCreationMethod.CopyPaste;
					resume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.None;

					Utilities.PopulateMissingResumeDetailsFromUserData(resume);
				  Utilities.UpdateOnetROnetInResume(resume);

          App.SetSessionValue("Career:Resume",resume);
					
					OpenMissingInfoPopup();
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Opens the missing info popup.
		/// </summary>
		private void OpenMissingInfoPopup()
		{
			// To open popup which is in another user control
			if (PastedResumeClick != null)
				PastedResumeClick(this, EventArgs.Empty);
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			btnPastedResume.Text = CodeLocalise("Next.Button", "Next");
			valPlainResumeRequired.ErrorMessage = CodeLocalise("PlainResume.RequiredErrorMessage", "Resume is required");
			valPlainResumeHtmlValidator.ErrorMessage = CodeLocalise("PlainResume.HtmlMarkupMessage", "Resume must not contain HTML mark-up");
		}
	}
}
