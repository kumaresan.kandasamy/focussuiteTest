﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.IO;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.JobSearch
{
  public partial class UploadResume : UserControlBase
	{
    public event EventHandler UploadResumeClick;
    private const long MaxResumeLength = 5; // Convert to Bytes - default 1MB [UIS]: increased to 5MB

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        LocaliseUI();
      }
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
			btnUploadResume.Text = CodeLocalise("Next.Button", "Next");
      valUploadResume.ErrorMessage = CodeLocalise("UploadResume.RequiredErrorMessage", "Resume is required");
      valFileUpload.ErrorMessage = CodeLocalise("FileUpload.ErrorMessage", "Provide a valid resume");
    }

    protected void btnUploadResume_Click(object sender, EventArgs e)
    {
      try
      {
        var resumeBytes = FileUploadResume.FileBytes;
        if (resumeBytes.Length > 0)
        {
          if (resumeBytes.Length > (MaxResumeLength * 1024 * 1024))
            throw new Exception("Maximum file size supported is " + MaxResumeLength + " MB. Please attempt to save the content of the file to text format.");

          string resumeHtml;
					var taggedResume = ServiceClientLocator.UtilityClient(App).GetTaggedDocument(DocumentType.Resume, Path.GetExtension(FileUploadResume.FileName), resumeBytes, false, out resumeHtml);

          if (String.IsNullOrEmpty(taggedResume) || taggedResume.ToLower().Contains("<error>") || taggedResume.ToLower().Contains("text too large"))
          {
            MasterPage.ShowModalAlert(AlertTypes.Error, "The amount of text in the resume uploaded is beyond the permitted limits. Please resubmit the resume after making the descriptions more concise.", "Resume too large");
            taggedResume = null;
          }

          if (!String.IsNullOrEmpty(taggedResume))
          {
            var resume = ServiceClientLocator.ResumeClient(App).GetResumeObject(taggedResume);
            
						resume.ResumeMetaInfo.ResumeCreationMethod = ResumeCreationMethod.Upload;
						resume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.None;
           
            var resumeDocument = new ResumeDocumentDto
            {
              FileName = Path.GetFileName(FileUploadResume.PostedFile.FileName),
              ContentType = FileUploadResume.PostedFile.ContentType,
              DocumentBytes = resumeBytes,
              Html = resumeHtml
            };

            Utilities.PopulateMissingResumeDetailsFromUserData(resume);
            Utilities.UpdateOnetROnetInResume(resume);

          	App.SetSessionValue("Career:ResumeDocument", resumeDocument);
            App.SetSessionValue("Career:Resume", resume);
            OpenMissingInfoPopup();
          }
        }
        else
          MasterPage.ShowModalAlert(AlertTypes.Error, "The amount of text in the resume uploaded is below the permitted limits. Please resubmit the resume after adding the text content.", "Resume too small");
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Opens the missing info popup.
    /// </summary>
    private void OpenMissingInfoPopup()
    {
      //To open popup which is in another user control
      if (UploadResumeClick != null)
        UploadResumeClick(this, EventArgs.Empty);
    }
  }
}
