﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Common.Code;
using Focus.Core.Views;
using Framework.Core;

using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.CareerExplorer.Code;
using Focus.Common.Helpers;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.JobSearch
{
  public partial class ResumeMissingInfo : UserControlBase
  {
    #region Page Properties

		private long? _userStateCode;
		protected string LicenceEndorsementsJavascriptArray; 

    #endregion

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      LocaliseUI();
      CountyInfo.Visible = App.Settings.CountyEnabled;

			if (!App.Settings.HideDrivingLicence)
				LicenceEndorsementsJavascriptArray = GetValidLicenceEndorsementsJavascriptArray();

      valEmailAddress.ValidationExpression = App.Settings.EmailAddressRegExPattern;
      EmailTextBox.MaxLength = App.Settings.MaximumEmailLength;
    }

		protected void Page_PreRender(object source, EventArgs e)
		{
			RegisterCodeValuesJson("resumeMissingInfoScriptValues", "resumeMissingInfoCodeValues", InitialiseClientSideCodeValues());

			if (!App.Settings.HideDrivingLicence)
			{
				var script = string.Format("var licenceLicenceEndorsements = {0};", LicenceEndorsementsJavascriptArray);
				ScriptManager.RegisterClientScriptBlock(this, GetType(), "licenceLicenceEndorsements", script, true);
			}
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      FirstNameRequired.ErrorMessage = CodeLocalise("FirstName.RequiredErrorMessage", "First name is required");
      LastNameRequired.ErrorMessage = CodeLocalise("LastName.RequiredErrorMessage", "Last name is required");
      AddressRequired.ErrorMessage = CodeLocalise("Address.RequiredErrorMessage", "Address is required");
      CityRequired.ErrorMessage = CodeLocalise("City.RequiredErrorMessage", "City is required");
      StateRequired.ErrorMessage = CodeLocalise("State.RequiredErrorMessage", "State is required");
      if (App.Settings.CountyEnabled)
        CountyRequired.ErrorMessage = CodeLocalise("County.RequiredErrorMessage", "County is required");
      CountryRequired.ErrorMessage = CodeLocalise("Country.RequiredErrorMessage", "Country is required");
      EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
      valEmailAddress.ErrorMessage = CodeLocalise("EmailAddressValidate.ErrorMessage", "Email address is not correct format");

			if (!App.Settings.HideEnrollmentStatus)
				EnrollmentStatusRequired.ErrorMessage = CodeLocalise("EnrollmentStatus.RequiredErrorMessage", "Enrollment status is required");

			if (!App.Settings.HideDrivingLicence)
			{
				DriverLicenseRequired.ErrorMessage = CodeLocalise("DriverLicense.RequiredErrorMessage", "Driver license is required");
				DriverLicenseStateRequired.ErrorMessage = CodeLocalise("State.RequiredErrorMessage", "State is required");
				DriverLicenseTypeRequired.ErrorMessage = CodeLocalise("LicenseType.ValidatorErrorMessage", "License type is required");
			}
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      ddlState.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).SingleOrDefault().ToString(), CodeLocalise("State.TopDefault", "- select state -"));
      ddlCountry.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Key == App.Settings.DefaultCountryKey).Select(x => x.Id).SingleOrDefault().ToString(), CodeLocalise("Country.TopDefault", "- select country -"));
      DriverLicenseStateDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States),"", CodeLocalise("State.TopDefault", "- select state -"));
      DriverLicenseTypeDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses), null, CodeLocalise("LicenseType.TopDefault", "- select a type -"));

			if (!App.Settings.HideEnrollmentStatus)
				BindEnrollmentStatusDropDown();

      BindEducationLevelDropDown();

			if (!App.Settings.HideDrivingLicence)
			{
				HideLicenseCheckBox.Items.Clear();
				HideLicenseCheckBox.Items.Add(CodeLocalise("DriversLicenseShow.Text", "Display driver’s license information on my resume."));

				BindDriverLicenseEndorsements();
				BindDriverLicenseDropDown();
			}
    }

    /// <summary>
    /// Binds the driver license endporsements check box list.
    /// </summary>
    private void BindDriverLicenseEndorsements()
    {
      var items = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements).OrderBy(lv => lv.Text).ToList();

      EndorsementCheckBoxList.BindLookup(items);
      LicenseEndorsementsLabel.Visible = true;
       
    }

    /// <summary>
    /// Binds the enrollment status drop down.
    /// </summary>
    private void BindEnrollmentStatusDropDown()
    {
      ddlEnrollmentStatus.Items.Clear();

      if (App.Settings.Theme == FocusThemes.Education)
      {
        if (App.Settings.SchoolType == SchoolTypes.TwoYear)
        {
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Prospective, "Prospective student");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.FirstYear, "First year student");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.SophomoreOrAbove, "Sophomore or above");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.NonCreditOther, "Non-credit/Other");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Alumni, "Alumni");
        }
        else if (App.Settings.SchoolType == SchoolTypes.FourYear)
        {
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Prospective, "Prospective student");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.FirstYear, "First year student");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Sophomore, "Sophomore");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Junior, "Junior");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Senior, "Senior");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Graduate, "Graduate student");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.NonCreditOther, "Non-credit/Other");
          ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Alumni, "Alumni");
        }
      }
      else
      {
        ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Not_Attending_School_HS_Graduate, "Not attending school, H.S. Graduate");
        ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Not_Attending_School_OR_HS_Dropout, "Not attending school, H.S. Drop out");
        ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.In_School_Post_HS, "In school, Post H.S.");
        ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.In_School_Alternative_School, "In School, Alternative School");
        ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.In_School_HS_OR_less, "In School, H.S. or less");
      }

      ddlEnrollmentStatus.Items.AddLocalisedTopDefault("EnrollmentStatus.TopDefault", "- select enrollment status -");
    }

    /// <summary>
    /// Binds the driver license drop down.
    /// </summary>
    private void BindDriverLicenseDropDown()
    {
      DriverLicenseDropDown.Items.Clear();
      DriverLicenseDropDown.Items.AddLocalisedTopDefault("DriverLicense.TopDefault", "- select -");

      var yesText = CodeLocalise("Global.Yes.Label", "Yes");
      var noText = CodeLocalise("Global.No.Label", "No");
      DriverLicenseDropDown.Items.Add(new ListItem(yesText, yesText));
      DriverLicenseDropDown.Items.Add(new ListItem(noText, noText));
    }

    /// <summary>
    /// Binds the education level drop down.
    /// </summary>
    private void BindEducationLevelDropDown()
    {
      CommonUtilities.GetEducationLevelDropDownList(ddlEducationLevel);
    }

    /// <summary>
    /// Shows the popup.
    /// </summary>
    public void ShowPopup()
    {
      LocaliseUI();
      Bind();
      DisplayFields();
      ResumeMissingInfoModal.Show();
			ScriptManager.RegisterClientScriptInclude(this, GetType(), "resumeMissingInfoScriptInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.ResumeMissingInfo.min.js"));
    }

    /// <summary>
    /// Initialises the client side code values.
    /// </summary>
    /// <returns></returns>
		private NameValueCollection InitialiseClientSideCodeValues()
		{
			var nvc = new NameValueCollection
				          {
					          {"isCountyEnabled", App.Settings.CountyEnabled ? "True" : "False"},
					          {"ccdCountyBehaviorId", ccdCounty.BehaviorID},
					          {"userState", _userStateCode.ToString()},
										{"drivingLicenseCodeDefault", ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses).Where(x => x.Key == App.Settings.DefaultDrivingLicenseType).Select(x => x.Id).FirstOrDefault().ToString()},
										{"drivingLicenseCodeClassD", GetLookupId(Constants.CodeItemKeys.DrivingLicenceClasses.ClassD).ToString()},
										{"drivingLicenseCodeMotorcycle", GetLookupId(Constants.CodeItemKeys.DrivingLicenceClasses.Motorcycle).ToString()},
										{"drivingLicenseEndorsementCodeMotorcycle", ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements).Where(x => x.Key == "DrivingLicenceEndorsements.Motorcycle").Select(x => x.Id).SingleOrDefault().ToString()},
										{"stateCodeOutsideUS", GetLookupId(Constants.CodeItemKeys.States.ZZ).ToString()},
										{"countryCodeUS", GetLookupId(Constants.CodeItemKeys.Countries.US).ToString()},
										{"errorZipCodeRequired", CodeLocalise("Zip.RequiredErrorMessage", "ZIP is required")},
										{"errorZipCodeLimit", CodeLocalise("Zip.ErrorMessage", "U.S. ZIP code must be in a 5 or 9-digit format")},
										{"errorEducationLevelRequired", CodeLocalise("EducationLevel.RequiredErrorMessage", "Education level is required")},
										{"errorEnrollmentStatus", CodeLocalise("EnrollmentStatus.ErrorMessage", "Enrollment status cannot be greater than education level")},
										{"errorEducationLevel", CodeLocalise("EducationLevel.ErrorMessage", "Enrollment status cannot be less than education level")},
										{"errorPhoneNumberRequired", CodeLocalise("PhoneNumber.RequiredErrorMessage", "Phone number is required")},
										{"errorPhoneNumberFormat", CodeLocalise("PhoneNumber.ErrorMessage", "U.S. phone number must be 10 digits")},
										{"errorPhoneTypeRequired", CodeLocalise("PhoneType.RequiredErrorMessage", "Phone type is required")},
										{"schoolStatusPostHS", SchoolStatus.In_School_Post_HS.ToString()},
										{"schoolStatusHS", SchoolStatus.In_School_HS_OR_less.ToString()},
										{"schoolStatusHSGraduate", SchoolStatus.Not_Attending_School_HS_Graduate.ToString()},
										{"schoolStatusNotAttendingOrDropOut", SchoolStatus.Not_Attending_School_OR_HS_Dropout.ToString()},
										{"schoolStatusAlternative", SchoolStatus.In_School_Alternative_School.ToString()},
                    {"phoneNumberRegExPattern", App.Settings.PhoneNumberRegExPattern},
                    {"phoneNumberMaskPattern", App.Settings.PhoneNumberMaskPattern},
                    {"nonUsPhoneNumberMaskPattern", App.Settings.NonUsPhoneNumberMaskPattern},
										{"HideDrivingLicence", App.Settings.HideDrivingLicence ? "True" : "False"}
				          };
			return nvc;
		}

    /// <summary>
    /// Displays the fields.
    /// </summary>
    private void DisplayFields()
    {
      if (App.User.IsAuthenticated)
      {
        var model = App.GetSessionValue<ResumeModel>("Career:Resume");

        if (model.IsNotNull())
        {
          if (model.ResumeContent.SeekerContactDetails.IsNotNull())
          {
            if (model.ResumeContent.SeekerContactDetails.SeekerName.IsNotNull())
            {
              if (String.IsNullOrEmpty(model.ResumeContent.SeekerContactDetails.SeekerName.FirstName) ||
                  String.IsNullOrEmpty(model.ResumeContent.SeekerContactDetails.SeekerName.LastName))
              {
                if (!String.IsNullOrEmpty(model.ResumeContent.SeekerContactDetails.SeekerName.FirstName))
                  FirstNameTextBox.Text = model.ResumeContent.SeekerContactDetails.SeekerName.FirstName.Trim();

                if (!String.IsNullOrEmpty(model.ResumeContent.SeekerContactDetails.SeekerName.LastName))
                  LastNameTextBox.Text = model.ResumeContent.SeekerContactDetails.SeekerName.LastName.Trim();
              }
              else
                NameInfo.Visible = false;
            }

            if (model.ResumeContent.SeekerContactDetails.PostalAddress.IsNotNull())
            {
	            var streetValid = model.ResumeContent.SeekerContactDetails.PostalAddress.Street1.IsNotNullOrEmpty();
	            var cityValid = model.ResumeContent.SeekerContactDetails.PostalAddress.City.IsNotNullOrEmpty();
	            var countyValid = !App.Settings.CountyEnabled || model.ResumeContent.SeekerContactDetails.PostalAddress.CountyId.IsNotNull();
							var stateValid = model.ResumeContent.SeekerContactDetails.PostalAddress.StateId.IsNotNull() && ddlState.Items.FindByValue(model.ResumeContent.SeekerContactDetails.PostalAddress.StateId.ToString()).IsNotNull();
							var countryValid = model.ResumeContent.SeekerContactDetails.PostalAddress.CountryId.IsNotNull() && ddlCountry.Items.FindByValue(model.ResumeContent.SeekerContactDetails.PostalAddress.CountryId.ToString()).IsNotNull();
	            var zipValid = model.ResumeContent.SeekerContactDetails.PostalAddress.Zip.IsNotNullOrEmpty();

							if (streetValid && cityValid && stateValid && countryValid && zipValid && countyValid)
							{
								AddressInfo.Visible =
								CityInfo.Visible =
								StateInfo.Visible =
								CountryInfo.Visible =
								CountyInfo.Visible = 
								ZipCodeInfo.Visible = false;
							}
							else
							{
								AddressTextBox.Text = streetValid ? model.ResumeContent.SeekerContactDetails.PostalAddress.Street1 : string.Empty;
								CityTextBox.Text = cityValid ? model.ResumeContent.SeekerContactDetails.PostalAddress.City : string.Empty;
								ZipCodeTextBox.Text = zipValid ? model.ResumeContent.SeekerContactDetails.PostalAddress.Zip : string.Empty;

								ddlState.SelectedIndex = 0;
								if (stateValid)
								{
									_userStateCode = model.ResumeContent.SeekerContactDetails.PostalAddress.StateId;
									ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(model.ResumeContent.SeekerContactDetails.PostalAddress.StateId.ToString()));
								}
								if (ddlState.SelectedIndex == 0)
									ddlState.SelectedValue = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).SingleOrDefault().ToString();

								ddlCountry.SelectedIndex = 0;
								if (countryValid)
								{
									ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(model.ResumeContent.SeekerContactDetails.PostalAddress.CountryId.ToString()));
								}
								if (ddlCountry.SelectedIndex == 0)
									ddlCountry.SelectedValue = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Key == App.Settings.DefaultCountryKey).Select(x => x.Id).SingleOrDefault().ToString();

								if (App.Settings.CountyEnabled)
								{
									ccdCounty.SelectedValue = "";
									ddlCounty.SelectedIndex = 0;
									if (countyValid)
									{
										ccdCounty.SelectedValue = model.ResumeContent.SeekerContactDetails.PostalAddress.CountyId.ToString();
									}
								}
							} 
            }

						if (model.ResumeContent.SeekerContactDetails.PhoneNumber.IsNotNull() && model.ResumeContent.SeekerContactDetails.PhoneNumber.Count > 0)
							PhoneInfo.Visible = false;

						if (model.ResumeContent.SeekerContactDetails.EmailAddress.IsNotNullOrEmpty())
							EmailInfo.Visible = false;
          }

          if (!NameInfo.Visible && !AddressInfo.Visible &&
              !CityInfo.Visible && !StateInfo.Visible &&
              (!App.Settings.CountyEnabled || (App.Settings.CountyEnabled && !CountyInfo.Visible)) &&
              !ZipCodeInfo.Visible && !PhoneInfo.Visible && !EmailInfo.Visible)
            ContactPanel.Visible = false;

          EnrollmentStatus.Visible = Education.Visible = EducationPanel.Visible = true;

          if (model.ResumeContent.EducationInfo.IsNotNull())
          {
            NormalizeEducationStatus(model.ResumeContent.EducationInfo);

            if (model.ResumeContent.EducationInfo.EducationLevel.HasValue && model.ResumeContent.EducationInfo.EducationLevel != EducationLevel.No_Grade_00)
            {
              // Ensure the Education Level exists before setting the drop down
              if (ddlEducationLevel.Items.FindByValue(model.ResumeContent.EducationInfo.EducationLevel.ToString()).IsNotNull())
              {
                ddlEducationLevel.SelectedValue = model.ResumeContent.EducationInfo.EducationLevel.ToString();
                // May be set to visible if School Status is missing
                Education.Visible = false;
              }
            }

            if (App.UserData.HasDefaultResume || App.Settings.Theme == FocusThemes.Education)
              Education.Visible = false;

						if (App.Settings.HideEnrollmentStatus)
							EnrollmentStatus.Visible = false;

						if (model.ResumeContent.EducationInfo.SchoolStatus.HasValue && model.ResumeContent.EducationInfo.SchoolStatus != SchoolStatus.NA && !App.Settings.HideEnrollmentStatus)
            {
              ddlEnrollmentStatus.SelectedValue = model.ResumeContent.EducationInfo.SchoolStatus.ToString();
              EnrollmentStatus.Visible = false;
            }

						if (App.Settings.Theme == FocusThemes.Workforce && (Education.Visible || EnrollmentStatus.Visible) && !App.Settings.HideEnrollmentStatus)
							Education.Visible = EnrollmentStatus.Visible = true;
          }

          if (App.UserData.HasDefaultResume || App.Settings.HideDrivingLicence)
            LicensePanel.Visible = false;

          if (!EnrollmentStatus.Visible && !Education.Visible)
            EducationPanel.Visible = false;

          if (!ContactPanel.Visible && !EducationPanel.Visible)
            UploadedInfo.Text = @"Your resume has been uploaded successfully.";
        }
      }
    }

    /// <summary>
    /// Handles the Click event of the btnSaveMissingResumeInfo control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnSaveMissingResumeInfo_Click(object sender, EventArgs e)
    {
      if (App.User.IsAuthenticated)
      {
        var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");

        if (currentResume.IsNotNull())
        {
          if (currentResume.ResumeContent.SeekerContactDetails.PostalAddress.IsNull())
            currentResume.ResumeContent.SeekerContactDetails.PostalAddress = new Address();

          bool educationUpdated, enrolmentUpdated;
          FrameMissingResumeInfo(currentResume, out educationUpdated, out enrolmentUpdated);

          var tempResume = ServiceClientLocator.ResumeClient(App).GetDefaultResume();

          if (App.UserData.HasDefaultResume)
          {
            currentResume.ResumeContent.Profile = tempResume.ResumeContent.Profile;

            if (currentResume.ResumeContent.ExperienceInfo.IsNotNull()
              && currentResume.ResumeContent.ExperienceInfo.Jobs.IsNotNull()
              && currentResume.ResumeContent.ExperienceInfo.Jobs.Count > 0)
              currentResume.ResumeContent.ExperienceInfo.EmploymentStatus = EmploymentStatus.Employed;

            if (tempResume.Special.Preferences.IsNotNull())
            {
              if (currentResume.Special.IsNull())
                currentResume.Special = new ResumeSpecialInfo();
              currentResume.Special.Preferences = tempResume.Special.Preferences;
            }

            if (!App.Settings.HideDrivingLicence && tempResume.Special.HideInfo.IsNotNull())
            {
              if (currentResume.Special.IsNull())
                currentResume.Special = new ResumeSpecialInfo();

              if (currentResume.Special.HideInfo.IsNull())
                currentResume.Special.HideInfo = new HideResumeInfo();

              currentResume.Special.HideInfo.UnHideDriverLicense = tempResume.Special.HideInfo.UnHideDriverLicense;
              currentResume.Special.HideInfo.HideDriverLicense = !tempResume.Special.HideInfo.UnHideDriverLicense;
            }

            if (App.Settings.Theme == FocusThemes.Workforce && tempResume.ResumeContent.EducationInfo.IsNotNull() && !educationUpdated && !enrolmentUpdated)
            {
              if (currentResume.ResumeContent.EducationInfo.IsNull())
                currentResume.ResumeContent.EducationInfo = new EducationInfo();

              currentResume.ResumeContent.EducationInfo.EducationLevel = tempResume.ResumeContent.EducationInfo.EducationLevel;
              currentResume.ResumeContent.EducationInfo.SchoolStatus = tempResume.ResumeContent.EducationInfo.SchoolStatus;
            }
          }

          if (App.Settings.CareerSearchResumesSearchable != ResumeSearchableOptions.NotApplicable)
          {
            if (currentResume.Special.Preferences.IsNull())
              currentResume.Special.Preferences = new Core.Models.Career.Preferences();

            currentResume.Special.Preferences.IsResumeSearchable = (App.Settings.CareerSearchResumesSearchable == ResumeSearchableOptions.Yes ||
                                                                    App.Settings.CareerSearchResumesSearchable == ResumeSearchableOptions.JobSeekersOptionYesByDefault);
          }

          currentResume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.None;
          currentResume.ResumeMetaInfo.ResumeName = null;

          try
          {
						
						var resume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, true);
            var resumeId = resume.ResumeMetaInfo.ResumeId;
            if (resumeId.IsNotNull())
            {
              var resumeDocument = App.GetSessionValue<ResumeDocumentDto>("Career:ResumeDocument");
              if (resumeDocument.IsNotNull())
              {
                if (resumeDocument.Html.IsNotNullOrEmpty())
                {
                  resumeDocument.ResumeId = (long) resumeId;
                  ServiceClientLocator.ResumeClient(App).SaveResumeDocument(resumeDocument);
                  App.RemoveSessionValue("Career:ResumeDocument");
                }
              }
            }


            var defaultResume = ServiceClientLocator.ResumeClient(App).GetDefaultResume();
	          if (defaultResume.IsNotNull())
	          {
		          currentResume.ResumeMetaInfo = defaultResume.ResumeMetaInfo;
							App.UserData.DefaultResumeId = defaultResume.ResumeMetaInfo.ResumeId;
		          App.UserData.DefaultResumeCompletionStatus = defaultResume.ResumeMetaInfo.CompletionStatus;
						
							Utilities.UpdateUserContextUserInfo(defaultResume.ResumeContent.SeekerContactDetails, ResumeCompletionStatuses.Contact);
						}

            if (EnrollmentStatus.Visible && ddlEnrollmentStatus.SelectedIndex != 0 && !App.Settings.HideEnrollmentStatus)
            {
              var schoolStatus = ddlEnrollmentStatus.SelectedValue.AsEnum<SchoolStatus>();
              ServiceClientLocator.AccountClient(App).ChangeEnrollmentStatus(schoolStatus);

              App.User.EnrollmentStatus = schoolStatus;
              App.User.Save();
            }

            //UpdateROnet("11-1011.00", "Onet");
            App.RemoveSessionValue("Career:Resume");
            App.SetSessionValue("Career:ResumeID", resumeId);
            App.RemoveSessionValue("Career:NewJobDetails");
            Response.Redirect(UrlBuilder.ResumeWizard("workhistory"));
          }
          catch (ApplicationException ex)
          {
            MasterPage.ShowError(AlertTypes.Error, ex.Message);
          }
        }
      }
    }

    //private void UpdateROnet(string ronet, string onet)
    //{
    //  var resumeModel = App.GetSessionValue<MyResumeModel>("Explorer:ResumeModel");

    //  if (!resumeModel.IsNotNull()) return;
    //  resumeModel.PersonResume.PrimaryROnet = ronet;
    //  resumeModel.PersonResume.PrimaryROnet = onet;
    //  App.SetSessionValue("Explorer:ResumeModel", resumeModel);
    //}

    /// <summary>
    /// Frames the missing resume info.
    /// </summary>
    /// <param name="resume">The resume.</param>
    /// <param name="educationUpdated">Whether the education level was updated</param>
    /// <param name="enrolmentUpdated">Whether the enrollment status was updated</param>
    private void FrameMissingResumeInfo(ResumeModel resume, out bool educationUpdated, out bool enrolmentUpdated)
    {
      educationUpdated = false;
      enrolmentUpdated = false;

      if (ContactPanel.Visible)
      {
        if (NameInfo.Visible)
        {
          if (FirstNameTextBox.Text.IsNotNullOrEmpty())
            resume.ResumeContent.SeekerContactDetails.SeekerName.FirstName = FirstNameTextBox.Text.Trim();
          if (LastNameTextBox.Text.IsNotNullOrEmpty())
            resume.ResumeContent.SeekerContactDetails.SeekerName.LastName = LastNameTextBox.Text.Trim();
        }

        if (AddressInfo.Visible)
        {
          if (AddressTextBox.Text.IsNotNullOrEmpty())
            resume.ResumeContent.SeekerContactDetails.PostalAddress.Street1 = AddressTextBox.Text.Trim();
        }

        if (CityInfo.Visible)
        {
          if (CityTextBox.Text.IsNotNullOrEmpty())
            resume.ResumeContent.SeekerContactDetails.PostalAddress.City = CityTextBox.Text.Trim();
        }

        if (StateInfo.Visible)
        {
          if (ddlState.SelectedIndex != 0)
          {
            resume.ResumeContent.SeekerContactDetails.PostalAddress.StateId = ddlState.SelectedValueToLong();
            resume.ResumeContent.SeekerContactDetails.PostalAddress.StateName = ddlState.SelectedItem.Text;
          }
        }


        if (App.Settings.CountyEnabled && CountyInfo.Visible)
        {
          if (ddlCounty.SelectedValue.IsNotNullOrEmpty())
          {
            resume.ResumeContent.SeekerContactDetails.PostalAddress.CountyId = ddlCounty.SelectedValueToLong();
            resume.ResumeContent.SeekerContactDetails.PostalAddress.CountyName = ddlCounty.SelectedItem.Text;
          }
        }

        if (CountryInfo.Visible)
        {
          if (ddlCountry.SelectedIndex != 0)
          {
            resume.ResumeContent.SeekerContactDetails.PostalAddress.CountryId = ddlCountry.SelectedValueToLong();
            resume.ResumeContent.SeekerContactDetails.PostalAddress.CountryName = ddlCountry.SelectedItem.Text;
          }
        }

        if (ZipCodeInfo.Visible)
        {
          if (ZipCodeTextBox.Text.IsNotNullOrEmpty())
            resume.ResumeContent.SeekerContactDetails.PostalAddress.Zip = ZipCodeTextBox.Text.Trim();
        }

        if (PhoneInfo.Visible)
        {
          if (PhoneNoTextBox.Text.IsNotNullOrEmpty())
          {
            var primaryPhone = new Phone();
            primaryPhone.PhoneNumber = System.Text.RegularExpressions.Regex.Replace(PhoneNoTextBox.TextTrimmed(), @"[^0-9]", "");
            primaryPhone.PhoneType = (PhoneType)Enum.Parse(typeof(PhoneType), PhoneNoDropDownList.SelectedValue, true);

            if (resume.ResumeContent.SeekerContactDetails.PhoneNumber.IsNull())
              resume.ResumeContent.SeekerContactDetails.PhoneNumber = new System.Collections.Generic.List<Phone>();

            resume.ResumeContent.SeekerContactDetails.PhoneNumber.Add(primaryPhone);
          }
        }

        if (EmailInfo.Visible)
        {
          if (EmailTextBox.Text.IsNotNullOrEmpty())
            resume.ResumeContent.SeekerContactDetails.EmailAddress = EmailTextBox.Text.Trim();
        }
      }

      if (EducationPanel.Visible)
      {
        if (EnrollmentStatus.Visible)
        {
          if (ddlEnrollmentStatus.SelectedIndex != 0)
          {
            enrolmentUpdated = true;

            if (resume.ResumeContent.EducationInfo != null)
              resume.ResumeContent.EducationInfo.SchoolStatus = ddlEnrollmentStatus.SelectedValueToEnum<SchoolStatus>();
            else
            {
              resume.ResumeContent.EducationInfo = new EducationInfo
              {
                SchoolStatus = ddlEnrollmentStatus.SelectedValueToEnum<SchoolStatus>()
              };
            }
          }
        }

        if (Education.Visible)
        {
          if (ddlEducationLevel.SelectedIndex != 0)
          {
            educationUpdated = true;

            if (resume.ResumeContent.EducationInfo != null)
              resume.ResumeContent.EducationInfo.EducationLevel = ddlEducationLevel.SelectedValueToEnum<EducationLevel>();
            else
            {
              resume.ResumeContent.EducationInfo = new EducationInfo
              {
                EducationLevel = ddlEducationLevel.SelectedValueToEnum<EducationLevel>()
              };
            }
          }
        }

        if (LicensePanel.Visible)
        {
          if (resume.Special.HideInfo.IsNull())
            resume.Special.HideInfo = new HideResumeInfo();

          resume.Special.HideInfo.UnHideDriverLicense = HideLicenseCheckBox.Items[0].Selected;
          resume.Special.HideInfo.HideDriverLicense = !resume.Special.HideInfo.UnHideDriverLicense;

          resume.ResumeContent.Profile.License = new Core.Models.Career.LicenseInfo
                                                   {
                                                     HasDriverLicense = DriverLicenseDropDown.SelectedValue.AsBoolean(),
                                                     DriverStateId =
                                                       DriverLicenseStateDropDown.SelectedValue.AsBoolean()
                                                         ? DriverLicenseStateDropDown.SelectedValueToLong()
                                                         : (long?) null,
                                                     DriverStateName =
                                                       DriverLicenseStateDropDown.SelectedValue.AsBoolean()
                                                         ? DriverLicenseStateDropDown.SelectedItem.Text
                                                         : null,
                                                     DrivingLicenceClassId =
                                                       DriverLicenseTypeDropDown.SelectedValue.AsBoolean()
                                                         ? DriverLicenseTypeDropDown.SelectedValueToLong()
                                                         : (long?) null,
                                                     DrivingLicenceEndorsementIds =
                                                       EndorsementCheckBoxList.Items.Cast<ListItem>()
                                                                              .Where(checkbox => checkbox.Selected)
                                                                              .Select(
                                                                                checkbox => checkbox.Value.ToLong())
                                                                              .Where(
                                                                                checkboxValue => checkboxValue.HasValue)
                                                                              .Select(
                                                                                checkboxValue =>
                                                                                checkboxValue != null
                                                                                  ? checkboxValue.Value
                                                                                  : 0)
                                                                              .ToList(),
                                                     DriverClassText =
                                                       DriverLicenseTypeDropDown.SelectedValue.AsBoolean()
                                                         ? DriverLicenseTypeDropDown.SelectedItem.Text
                                                         : null,
                                                   };

        }
      }
    }

    /// <summary>
    /// Normalizes the education status.
    /// </summary>
    /// <param name="educationInfo">The education info.</param>
    private void NormalizeEducationStatus(EducationInfo educationInfo)
    {
      if (educationInfo.EducationLevel.HasValue)
      {
        var eduLevel = EducationLevel.No_Grade_00;
        //secondary:10:
        //highschool:12:
        //nonpgdiploma:13:
        //certificate:13:
        //associate:14:
        //bachelor:16:
        //pgdiploma:17:
        //master:18:
        //doctor:21:
        switch ((int)educationInfo.EducationLevel)
        {
          case 10:
            eduLevel = EducationLevel.Grade_10_10;
            break;
          case 12:
            eduLevel = EducationLevel.Grade_12_No_Diploma_12;
            break;
          case 13:
            eduLevel = EducationLevel.Grade_12_HS_Graduate_13;
            break;
          case 14:
            eduLevel = EducationLevel.HS_2_Year_Associates_Degree_22;
            break;
          case 16:
            eduLevel = EducationLevel.Bachelors_OR_Equivalent_24;
            break;
          case 17:
          case 18:
            eduLevel = EducationLevel.Masters_Degree_25;
            break;
          case 21:
            eduLevel = EducationLevel.Doctorate_Degree_26;
            break;
          default:
            eduLevel = 0;
            break;
        }
        educationInfo.EducationLevel = eduLevel;
      }
    }

		/// <summary>
		/// Gets the valid licence endorsements javascript array.
		/// </summary>
		/// <returns></returns>
		private string GetValidLicenceEndorsementsJavascriptArray()
		{
			var javascriptBuilder = new StringBuilder(@"{"""":"""",");

			var endorsementRules = App.Settings.DrivingLicenceEndorsementRules;

			var drivingLicenceLookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses);
			var drivingLicenceEndorsementLookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements);

			foreach (var rule in endorsementRules)
			{
				var drivingLicenceKey = string.Format("DrivingLicenceClasses.{0}", rule.LicenceKey);

				var drivingLicenceId = drivingLicenceLookUps.Where(x => x.Key == drivingLicenceKey).Select(x => x.Id).FirstOrDefault();

				if (drivingLicenceId.IsNotNull())
				{
					javascriptBuilder.AppendFormat(@"""{0}"": """, drivingLicenceId);

					foreach (var endorsement in rule.EndorsementKeys)
					{
						var endorsementKey = string.Format("DrivingLicenceEndorsements.{0}", endorsement);

						var endorsementId = drivingLicenceEndorsementLookUps.Where(x => x.Key == endorsementKey).Select(x => x.Id).FirstOrDefault();

						if (endorsementId.IsNotNull())
							javascriptBuilder.AppendFormat("{0},", endorsementId);
					}

					javascriptBuilder.Append(@""",");
				}
			}

			javascriptBuilder.Append("}");

			return javascriptBuilder.ToString().Replace(",}", "}");
		}
  }

}
