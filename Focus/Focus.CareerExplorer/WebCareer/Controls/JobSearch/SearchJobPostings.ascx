﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchJobPostings.ascx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.Controls.JobSearch.SearchJobPostings" %>
		<div class="savedSearchTop" role="presentation">
			<div class="col-xs-9 col-sm-11 col-md-11 clearfix">
				<h1>
					<%= HtmlLocalise("Heading1.Label", "Search job postings")%></h1>
			</div>
			<div  class="col-xs-3 col-sm-1 col-md-1" style="text-align: right;">
				<asp:UpdatePanel ID="UpdatePanel1" runat="server">
				<Triggers> <asp:AsyncPostBackTrigger ControlID="btnSearchJobPostings" EventName="Click" /> </Triggers>
					<ContentTemplate>
						<asp:Button ID="btnSearchJobPostings" Text="Go &#187;" runat="server" class="buttonLevel2"
							OnClick="btnSearchJobPostings_Click" ValidationGroup="Location" />
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>
		<div class="horizontalRule" style="clear:left">
		</div>
