﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.JobSearch
{
    public partial class CreateEditResume : UserControlBase
    {
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LocaliseUI();
            SetVisibility();
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            btnStartResume.Text = CodeLocalise("Next.Button", "Next");
        }

        /// <summary>
        /// Sets the visibility.
        /// </summary>
        private void SetVisibility()
        {
            CreateResumePanel.Visible = (App.Settings.MaximumAllowedResumeCount == 0 || MasterPage.ResumeCount < App.Settings.MaximumAllowedResumeCount);
        }

        /// <summary>
        /// Handles the Click event of the btnStartResume control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnStartResume_Click(object sender, EventArgs e)
        {
            App.RemoveSessionValue("Career:Resume");
            App.RemoveSessionValue("Career:ResumeID");
            var newJob = new WorkHistory.AddWorkHistory { IsNewJob = true };
            App.SetSessionValue("Career:NewJobDetails", newJob);
            var resumeCount = ServiceClientLocator.ResumeClient(App).GetResumeCount();
            if (resumeCount >= App.Settings.MaximumAllowedResumeCount)
            {
                Response.RedirectToRoute("home");
                App.SetSessionValue<bool>("CreateEditResumePopup", true);
            }
            else
            {
                try
                {
                    ResumeModel defaultresume = null;
                    var useTemplate = false;
                    if (App.Settings.UseResumeTemplate)
                    {
                        defaultresume = ServiceClientLocator.ResumeClient(App).GetResumeTemplate();
                        useTemplate = defaultresume.IsNotNull();
                    }

                    if (defaultresume.IsNull())
                        defaultresume = ServiceClientLocator.ResumeClient(App).GetDefaultResume();

                    if (defaultresume.IsNotNull())
                    {
                        var resume = useTemplate ? defaultresume : new ResumeModel
                        {
                            ResumeMetaInfo = new ResumeEnvelope
                            {
                                CompletionStatus = 0
                            },
                            ResumeContent = new ResumeBody
                            {
                                SeekerContactDetails = defaultresume.ResumeContent.SeekerContactDetails,
                                EducationInfo = defaultresume.ResumeContent.EducationInfo,
                                Skills = defaultresume.ResumeContent.Skills,
                                Profile = defaultresume.ResumeContent.Profile,
                                ExperienceInfo = new ExperienceInfo
                                {
                                    EmploymentStatus = defaultresume.ResumeContent.ExperienceInfo.EmploymentStatus
                                }
                            },
                            Special = new ResumeSpecialInfo
                            {
                                Preferences = defaultresume.Special.Preferences
                            }
                        };

                        if (App.Settings.CareerSearchResumesSearchable != ResumeSearchableOptions.NotApplicable)
                        {
                            if (resume.Special.Preferences.IsNull())
                                resume.Special.Preferences = new Core.Models.Career.Preferences();

                            resume.Special.Preferences.IsResumeSearchable = (App.Settings.CareerSearchResumesSearchable == ResumeSearchableOptions.Yes ||
                                                                             App.Settings.CareerSearchResumesSearchable == ResumeSearchableOptions.JobSeekersOptionYesByDefault);
                        }

                        if (defaultresume.ResumeMetaInfo.CompletionStatus == 0)
                            resume.ResumeMetaInfo.ResumeId = defaultresume.ResumeMetaInfo.ResumeId;

                        App.SetSessionValue("Career:Resume", resume);
                    }
                }
                catch
                { }

                Response.RedirectToRoute("ResumeWizard");
            }
        }
    }
}
