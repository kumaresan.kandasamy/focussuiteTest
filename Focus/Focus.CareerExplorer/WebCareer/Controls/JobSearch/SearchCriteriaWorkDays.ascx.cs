﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Common.Helpers;
using Focus.Core;
using Focus.Core.Models.Career;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.JobSearch
{
  public partial class SearchCriteriaWorkDays : UserControlBase
  {
    private bool _controlsBuilt;

    private bool HasWorkWeeks()
    {
      return App.Settings.Theme == FocusThemes.Workforce;
    }

    /// <summary>
    /// Fires when the page is loaded
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
        BuildControls();

      ApplyTheme();

      ScriptManager.RegisterClientScriptInclude(this, GetType(), "SearchCriteriaWorkDaysInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.SearchCriteriaWorkDays.js"));
    }

    /// <summary>
    /// Applies the theme
    /// </summary>
    private void ApplyTheme()
    {
      if (!HasWorkWeeks())
      {
        VariesTableRow.Attributes["class"] = "last";
        WorkWeeksPlaceHolder.Visible = false;
      }
    }

    /// <summary>
    /// Builds the controls on the page
    /// </summary>
    private void BuildControls()
    {
      if (_controlsBuilt)
        return;

      WeekDaysCheckBoxList.Items.Clear();

      WeekDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("WeekDays.Label", "Weekdays"), DaysOfWeek.Weekdays.ToString()));
      WeekDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("Monday.Label", "Mon"), DaysOfWeek.Monday.ToString()));
      WeekDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("Tuesday.Label", "Tue"), DaysOfWeek.Tuesday.ToString()));
      WeekDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("Wednesday.Label", "Wed"), DaysOfWeek.Wednesday.ToString()));
      WeekDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("Thursday.Label", "Thu"), DaysOfWeek.Thursday.ToString()));
      WeekDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("Friday.Label", "Fri"), DaysOfWeek.Friday.ToString()));

      WeekendDaysCheckBoxList.Items.Clear();

      WeekendDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("Weekends.Label", "Weekend"), DaysOfWeek.Weekends.ToString()));
      WeekendDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("Saturday.Label", "Sat"), DaysOfWeek.Saturday.ToString()));
      WeekendDaysCheckBoxList.Items.Add(new ListItem(HtmlLocalise("Sunday.Label", "Sun"), DaysOfWeek.Sunday.ToString()));

			ToolTipster.Attributes.Add("title", HasWorkWeeks()
        ? CodeLocalise("WorkAvailabiltiy.ToolTip.WF", "To find jobs which specify particular work periods, filter by part-time/full-time or select days of the week relating to those you are available to work (or a combination of these). Remember this may restrict the number of matches.")
				: CodeLocalise("WorkAvailabiltiy.ToolTip.EDU", "To find jobs with a particular set of working days, select days of the week relating to those you are available to work. Remember this may restrict the number of job matches."));
	    ToolTipster.ImageUrl = UrlBuilder.HelpIcon();

      VariesCheckBox.Text = HtmlLocalise("Varies.Label", "Varies");

      if (HasWorkWeeks())
      {
        WorkWeekDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmploymentStatuses), null, CodeLocalise("WeekWorkDropDown.TopDefault", "- select employment status -"));
        WorkWeekDropDown.SelectedIndex = 0;
      }
        if(WeekDaysCheckBoxList!=null)
      WeekDaysCheckBoxList.Attributes.Add("role", "presentation");
        if (WeekendDaysCheckBoxList != null)
            WeekendDaysCheckBoxList.Attributes.Add("role", "presentation");

      _controlsBuilt = true;
    }

    /// <summary>
    /// Binds the controls to the data source
    /// </summary>
    /// <param name="criteria">The search criteria</param>
    public void Bind(WorkDaysCriteria criteria)
    {
      if (criteria.IsNull())
        return;

      BuildControls();

      BindCheckBoxList(criteria, WeekDaysCheckBoxList);
      BindCheckBoxList(criteria, WeekendDaysCheckBoxList);

      VariesCheckBox.Checked = criteria.IsNotNull() && criteria.WeekDayVaries;

      if (criteria.WorkDays.GetValueOrDefault(DaysOfWeek.None) != DaysOfWeek.None || criteria.WeekDayVaries || criteria.WorkWeek.IsNotNullOrZero())
      {
        accWorkDaysTitle.Attributes.Add("class", "collapsableAccordionTitle on");
        accWorkDaysContent.Attributes.Add("class", "collapsableAccordionContent open");
      }

      if (HasWorkWeeks())
      {
        if (criteria.WorkWeek.IsNotNullOrZero())
          WorkWeekDropDown.SelectValue(criteria.WorkWeek.ToString());
      }
    }

    /// <summary>
    /// Binds a checkbox list to the criteria
    /// </summary>
    /// <param name="criteria">The search criteria</param>
    /// <param name="checkBoxList">The check box list</param>
    private void BindCheckBoxList(WorkDaysCriteria criteria, CheckBoxList checkBoxList)
    {
      foreach (ListItem item in checkBoxList.Items)
      {
        item.Selected = false;
      }

      if (!criteria.WorkDays.HasValue) 
        return;

      var weekDays = criteria.WorkDays;
      foreach (ListItem item in checkBoxList.Items)
      {
        var workDay = (DaysOfWeek) Enum.Parse(typeof (DaysOfWeek), item.Value);
        if ((weekDays & workDay) == workDay)
          item.Selected = true;
      }
    }

    /// <summary>
    /// Builds the criteria object for searching on work days
    /// </summary>
    /// <returns>The work days criteria</returns>
    public WorkDaysCriteria BuildSearchExpression()
    {
      if (WeekDaysCheckBoxList.SelectedIndex >= 0 || WeekendDaysCheckBoxList.SelectedIndex >= 0 || VariesCheckBox.Checked || WorkWeekDropDown.SelectedIndex > 0)
      {
        var workDays = DaysOfWeek.None;

        GetSelectedDaysFromCheckBoxList(WeekDaysCheckBoxList, ref workDays);
        GetSelectedDaysFromCheckBoxList(WeekendDaysCheckBoxList, ref workDays);

        return new WorkDaysCriteria
        {
          WorkDays = workDays,
          WeekDayVaries = VariesCheckBox.Checked,
          WorkWeek = (HasWorkWeeks() && WorkWeekDropDown.SelectedIndex > 0) ? WorkWeekDropDown.SelectedValue.AsLong() : (long?)null
        };
      }

      return null;
    }

    /// <summary>
    /// Gets the selected work days from a check box list
    /// </summary>
    /// <param name="checkBoxList">The check box list</param>
    /// <param name="workDays">The work days to set</param>
    private void GetSelectedDaysFromCheckBoxList(CheckBoxList checkBoxList, ref DaysOfWeek workDays)
    {
      var selectedDays = (from ListItem item in checkBoxList.Items 
                          where item.Selected 
                          select (DaysOfWeek) Enum.Parse(typeof (DaysOfWeek), item.Value)).ToList();

      workDays = selectedDays.Aggregate(workDays, (current, day) => (current | day));
    }
  }
}