﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadResume.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.JobSearch.UploadResume" %>
<div class="panel panel-lightbox">
	<div class="panel-heading">
		<focus:LocalisedLabel runat="server" ID="lblHeader" LocalisationKey="UploadAResume.Label" DefaultText="Upload a resume" RenderOuterSpan="False" />
	</div>
	<div class="panel-body">
		<focus:LocalisedLabel runat="server" ID="lblFileUploadResume" AssociatedControlID="FileUploadResume" LocalisationKey="DocTypes.Label" DefaultText="You may upload resumes in DOC, DOCX, RTF, or PDF formats.  Your PDF must be in text, not images – if you can search, highlight, and copy the words within your resume, the PDF is in text.  After you’ve uploaded your resume, you’ll have the opportunity to edit it.  You may also set the resume to be viewed with its original formatting by selecting “Original” on the review section.  <u>Note: The ability to view the uploaded resume with its original formatting is only available for DOC, DOCX, or RTF formats and not PDF.</u>"/>
		<span class="btn btn-block btn-primary btn-file">
        <span><u>Browse&hellip;</u></span> <asp:FileUpload ID="FileUploadResume" runat="server" />
    </span>
		<asp:RequiredFieldValidator ID="valUploadResume" runat="server" ControlToValidate="FileUploadResume" CssClass="error" SetFocusOnError="false" Display="Dynamic" ValidationGroup="UploadResume"></asp:RequiredFieldValidator>
		<focus:InsensitiveRegularExpressionValidator ID="valFileUpload" runat="server" ControlToValidate="FileUploadResume"
			CssClass="error" ValidationExpression="(.*)(\.)((doc)|(pdf)|(txt)|(odf)|(rtf)|(odt)|(docx)|(cln)|(htm)|(html)|(xls))$"
			SetFocusOnError="True" Display="Dynamic" ValidationGroup="UploadResume"></focus:InsensitiveRegularExpressionValidator>
	</div>
	<div class="panel-footer">
			<focus:ModernButton ID="btnUploadResume" runat="server" CssClass="btn btn-default" IconProvider="FontAwesome" IconCssClass="fa-caret-right" IconPlacement="Right" OnClick="btnUploadResume_Click" ValidationGroup="UploadResume" />
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		// bind file input event (shows feedback in dummy file input)
		$('.btn-file :file').on('fileselect', function (event, numFiles, label) {
			var msgHolder = $(this).parents('.btn-file').find('span'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;
										if (msgHolder.length) {
											msgHolder.text(log);
										}
		});
	});

	$(document).on('change', '.btn-file :file', function () {
		var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
	});
</script>
