﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobSearchStartPage.ascx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.Controls.JobSearch.JobSearchStartPage" %>
<%@ Register Src="~/Controls/UnAuthorizedInformation.ascx" TagName="ConfirmationModal"
	TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ResumeWizardRedirectConfirmationModal" Src="~/Controls/ConfirmationModal.ascx" %>
<asp:Panel ID="Panel1" runat="server" Visible="false">
	<div class="landingPageNavigation landingPageJobSearch">
		<div class="lpItem">
			<div class="lpItemHeaderBlue lpCreateEditResume">
				<asp:LinkButton ID="lnkCreateUploadResume" runat="server" OnClick="lnkJobBasedResume_Click" CssClass="lpItemHeaderText"><focus:LocalisedLabel runat="server" ID="LandingPage1Label" LocalisationKey="LandingPage1.LinkText" DefaultText="Create or upload a resume" RenderOuterSpan="False"/></asp:LinkButton>
				<focus:LocalisedLabel runat="server" ID="LandingPage2Label" LocalisationKey="LandingPage2.Label" DefaultText="best  bet for best matches" CssClass="lpItemHeaderBlueHint"/>
			</div>
		</div>
		<div class="lpItem lpItemExpanded">
			<div class="lpItemHeaderBlue lpJobSearch"><focus:LocalisedLabel runat="server" ID="LandingPage3Label" LocalisationKey="LandingPage3.LinkText" DefaultText="Search for jobs" CssClass="lpItemHeaderText"/></div>
			<div class="lpItemContentPanelBlue">
				<div class="lpItemContentLinkRowBlue">
					<asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnkJobBasedResume_Click" CssClass="lpItemContentLinkText"><focus:LocalisedLabel runat="server" ID="LandingPage5Label" LocalisationKey="LandingPage5.LinkText" DefaultText="See job recommendations based on my resume" RenderOuterSpan="False"/></asp:LinkButton>
					<focus:LocalisedLabel runat="server" ID="LandingPage6Label" LocalisationKey="LandingPage6.Label" DefaultText="we'll help you build or upload your resume first" CssClass="lpItemContentLinkBlueHint"/>
				</div>
				<div class="lpItemContentLinkRowBlue">
					<a href="<%=UrlBuilder.JobSearchCriteria()%>" class="lpItemContentLinkText"><focus:LocalisedLabel runat="server" ID="LandingPage7Label" LocalisationKey="LandingPage7.LinkText" DefaultText="Start or continue a job search without a resume" RenderOuterSpan="False"/></a>
					<focus:LocalisedLabel runat="server" ID="LandingPage8Label" LocalisationKey="LandingPage8.Label" DefaultText="results are limited without a resume" CssClass="lpItemContentLinkBlueHint"/>
				</div>
			</div>
		</div>
	</div>
</asp:Panel>
<asp:Panel ID="Panel2" runat="server" Visible="false">
	<div class="landingPageNavigation landingPageJobSearch">
		<div class="lpItem">
			<div class="lpItemHeaderBlue lpCreateEditResume"><a href="<%=UrlBuilder.YourResume()%>" class="lpItemHeaderText"><focus:LocalisedLabel runat="server" ID="LandingPage9Label" LocalisationKey="LandingPage9.LinkText" DefaultText="Create, upload, or edit a resume" RenderOuterSpan="False"/></a></div>
		</div>
		<div class="lpItem">
			<div class="lpItemHeaderBlue lpJobSearch"><asp:LinkButton ID="lnkSearchBasedOnResume" runat="server" CssClass="lpItemHeaderText" OnClick="LnkSearchBasedOnResume_Click"><focus:LocalisedLabel runat="server" ID="LandingPage10Label" LocalisationKey="LandingPage10.LinkText" DefaultText="See job recommendations based on my resume" RenderOuterSpan="False"/></asp:LinkButton></div>
		</div>
		<div class="lpItem">
			<div class="lpItemHeaderBlue lpJobSearch"><asp:LinkButton ID="StartJobSearchLink" runat="server" CssClass="lpItemHeaderText" OnClick="StartJobSearchLink_Click"><focus:LocalisedLabel runat="server" ID="LandingPage11Label" LocalisationKey="LandingPage10.LinkText" DefaultText="Start or continue a job search" RenderOuterSpan="False"/></asp:LinkButton></div>
		</div>
	</div>
</asp:Panel>
<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
<uc:ResumeWizardRedirectConfirmationModal ID="ResumeWizardRedirectConfirmationModal" runat="server" />
