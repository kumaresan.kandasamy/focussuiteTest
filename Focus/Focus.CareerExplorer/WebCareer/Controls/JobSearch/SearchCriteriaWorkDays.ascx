﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchCriteriaWorkDays.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.JobSearch.SearchCriteriaWorkDays" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>

<table class="collapsablePanel" role="presentation">
	<tr id="accWorkDaysTitle" runat="server" class="collapsableAccordionTitle">
		<td valign="top" width="30px">
			<span class="accordionIcon"></span>
		</td>
		<td valign="top">
			<a href="javascript:function NoTop() {return false;}">
				<%= HtmlLocalise("WorkAvailabiltiy.Label", "WORK AVAILABILITY")%>
      </a>
<%--		</td>

	<td valign="top" width="400px" style="position: relative">--%>
										<asp:Image ID="ToolTipster" runat="server" class="toolTipNew tooltipstered" AlternateText="Tool Tipster" />
								</td>
	</tr>
	<tr id="accWorkDaysContent" runat="server" class="collapsableAccordionContent">
		<td colspan="3">
			<table width="100%" role="presentation">
				<tr class="first">
					<td style="padding-left: 12px">
						<%= HtmlLocalise("NormalWorkDays.Label", "Normal work days")%>&nbsp;
					</td>
          <td>
            <asp:CheckBoxList runat="server" ID="WeekDaysCheckBoxList" RepeatDirection="Vertical" CssClass="daysOfWeekCheckBoxList" CellPadding="0" CellSpacing="0" />
          </td>
				</tr>
				<tr>
					<td>
					  &nbsp;
					</td>
					<td>
            <asp:CheckBoxList runat="server" ID="WeekendDaysCheckBoxList" RepeatDirection="vertical" CssClass="daysOfWeekCheckBoxList" CellPadding="0" CellSpacing="0" />
					</td>
				</tr>
				<tr id="VariesTableRow" runat="server">
					<td>
					  &nbsp;
					</td>
					<td>
            <asp:CheckBox runat="server" ID="VariesCheckBox" />
					</td>
				</tr>
        <asp:PlaceHolder runat="server" ID="WorkWeeksPlaceHolder">
          <tr>
            <td class="last" style="padding-left: 12px">
						  <%= HtmlLabel(WorkWeekDropDown, "NormalWorkWeek.Label", "Hours")%>&nbsp;
					  </td>
            <td>
              <asp:DropDownList runat="server" ID="WorkWeekDropDown" Width="230px"/>
            </td>
           </tr>
        </asp:PlaceHolder>
      </table>
		</td>
	</tr>
</table>
<script type="text/javascript">
  Sys.Application.add_load(function () {
    focusSuite.searchCriteriaWorkDays.BindSearchCriteriaWorkDaysOnClick('<%=WeekDaysCheckBoxList.ClientID %>', '<%=WeekendDaysCheckBoxList.ClientID %>');
  });
</script>