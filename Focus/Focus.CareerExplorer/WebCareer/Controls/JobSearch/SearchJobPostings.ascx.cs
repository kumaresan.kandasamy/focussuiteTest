﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.JobSearch
{
	public partial class SearchJobPostings : UserControlBase
	{
		public event EventHandler SearchJobPostingsClick;

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnSearchJobPostings_Click(object sender, EventArgs e)
		{
			if (SearchJobPostingsClick != null)
				SearchJobPostingsClick(this, EventArgs.Empty);
		}
	}
}