﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Services.Core;
using Framework.Core;
using Framework.DataAccess;
using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.JobSearch
{
  public partial class JobSearchStartPage : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      DisplayStartPage();
    }

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
      base.OnInit(e);

      ResumeWizardRedirectConfirmationModal.OkCommand += ResumeWizardRedirectConfirmationModal_OkCommand;
      ResumeWizardRedirectConfirmationModal.CloseCommand += ResumeWizardRedirectConfirmationModal_CloseCommand;
    }

    /// <summary>
    /// Displays the start page.
    /// </summary>
    private void DisplayStartPage()
    {
			App.SetSessionValue("Career:SearchCriteria", new SearchCriteria());

			if (App.User.IsAuthenticated)
			{
				var completionStatus = (int)App.UserData.DefaultResumeCompletionStatus;

				if (App.UserData.HasDefaultResume && completionStatus >= (int)GuidedPath.UptoPreferences)
					Panel2.Visible = true;
				else
					Panel1.Visible = true;
			}
			else
        Panel1.Visible = true;
    }

    /// <summary>
    /// Handles the Click event of the LnkSearchBasedOnResume control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void LnkSearchBasedOnResume_Click(object sender, EventArgs e)
    {
    	SearchBasedOnResume();
    }

    /// <summary>
    /// Handles the Click event of the lnkJobBasedResume control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void lnkJobBasedResume_Click(object sender, EventArgs e)
    {
      if (App.User.IsAuthenticated)
      {
        if (App.UserData.HasDefaultResume)
        {
					var resumeStatus = App.UserData.DefaultResumeCompletionStatus;

					if (resumeStatus == ResumeCompletionStatuses.Completed)
					{
						SearchBasedOnResume();
					}
					else
					{
						// Redirect to resume wizard
						App.SetSessionValue("Career:ResumeID", App.UserData.DefaultResumeId);
						ResumeWizardRedirectConfirmationModal.Show("", "It seems you were in the middle of building your resume. We have taken you back to where you have left off.", "Cancel", "CANCEL", "", "Ok", ((int)resumeStatus).ToString(), height: 50);
					}
        }
        else
          Response.RedirectToRoute("YourResume");
      }
      else
        ConfirmationModal.Show();
    }

    /// <summary>
    /// Handles the Click event of the StartJobSearchLink control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void StartJobSearchLink_Click(object sender, EventArgs e)
    {
      try
      {
        if (App.User.IsAuthenticated)
        {
          #region Save Activity
          //TODO: Martha (new activity functionality)
          //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff,
          //                                         ActivityType.Automated, "39", staffProfile: staffcontext.StaffInfo);
          //else
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.JobSeeker,
          //                                         ActivityType.Automated, "361");

          #endregion
        }

				Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });

      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the OkCommand event of the ResumeWizardRedirectConfirmationModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    public void ResumeWizardRedirectConfirmationModal_OkCommand(object sender, CommandEventArgs e)
    {
      Response.Redirect(UrlBuilder.ReturnToResumeWizard(((ResumeCompletionStatuses)e.CommandName.AsInt())));
    }

    /// <summary>
    /// Handles the CloseCommand event of the ResumeWizardRedirectConfirmationModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    public void ResumeWizardRedirectConfirmationModal_CloseCommand(object sender, CommandEventArgs e)
    {
      Response.Redirect(Page.Request.Url.ToString());
    }

    /// <summary>
    /// Searches the based on resume.
    /// </summary>
		private void SearchBasedOnResume()
		{
			var searchCriteria = App.GetSessionValue("Career:SearchCriteria", new SearchCriteria());

			#region Job Matching Criteria

			searchCriteria.RequiredResultCriteria = new RequiredResultCriteria { MinimumStarMatch = StarMatching.ThreeStar };

			#endregion

			#region Posting Age Criteria

			searchCriteria.PostingAgeCriteria = new PostingAgeCriteria { PostingAgeInDays = App.Settings.JobSearchPostingDate };

			#endregion

			#region Job Location Criteria

			var joblocation = new JobLocationCriteria();

			if (!App.Settings.NationWideInstance)
				joblocation.Area = new AreaCriteria { StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault() };

			var model = ServiceClientLocator.ResumeClient(App).GetDefaultResume();

			if (model.IsNotNull() && model.Special.IsNotNull())
			{
				var preference = model.Special.Preferences;

				if (preference.IsNotNull())
				{
					if (preference.SearchInMyState.HasValue && (bool)preference.SearchInMyState)
					{
						joblocation.OnlyShowJobInMyState = true;
						joblocation.Area = new AreaCriteria { StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault() };
					}

					if (preference.ZipPreference.IsNotNull() && preference.ZipPreference.Count > 0)
					{
						var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).FirstOrDefault(x => x.Id == preference.ZipPreference[0].RadiusId);
						var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
						var radius = radiusExternalId.ToLong();

						joblocation.Radius = new RadiusCriteria
						{
							Distance = radius.GetValueOrDefault(),
							DistanceUnits = DistanceUnits.Miles,
							PostalCode = preference.ZipPreference[0].Zip
						};

						if (preference.SearchInMyState.HasValue && (bool)preference.SearchInMyState)
						{
							var state = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

							if (App.Settings.NationWideInstance)
								if (App.UserData.HasProfile && App.UserData.Profile.PostalAddress.IsNotNull() && App.UserData.Profile.PostalAddress.StateId.HasValue)
									state = App.UserData.Profile.PostalAddress.StateId.Value;

							joblocation.Area = new AreaCriteria { StateId = state };
						}
					}
					else if (preference.MSAPreference.IsNotNull() && preference.MSAPreference.Count > 0)
					{
						var state = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

						if (App.Settings.NationWideInstance)
							state = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.ExternalId == preference.MSAPreference[0].StateName).Select(x => x.Id).FirstOrDefault();
						else
						{
							if (!(preference.SearchInMyState.HasValue && (bool)preference.SearchInMyState))
								state = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.ExternalId == preference.MSAPreference[0].StateName).Select(x => x.Id).FirstOrDefault();
						}

						joblocation.Area = new AreaCriteria
						{
							StateId = preference.SearchInMyState.AsBoolean() ? state : (long?)null,
							MsaId = preference.MSAPreference[0].MSAId
						};

						joblocation.SelectedStateInCriteria = preference.MSAPreference[0].StateName;
					}
            
          //joblocation.HomeBasedJobPostings = preference.HomeBasedJobPostings.GetValueOrDefault();

					searchCriteria.JobLocationCriteria = joblocation;
				}
			}

			#endregion

			#region Reference Document Criteria

			searchCriteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
			                                           	{
			                                           		DocumentId = App.UserData.DefaultResumeId.ToString(),
			                                           		DocumentType = DocumentType.Resume
			                                           	};

			#endregion

			App.SetSessionValue("Career:ResumeID", App.UserData.DefaultResumeId);
			App.SetSessionValue("Career:SearchCriteria", searchCriteria);

			#region Save Activity
			//TODO: Martha (new activity functionality)
			//var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
			//if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
			//  ServiceClientLocator.AnnotationClient(App).SaveActivity(userContext.UserId, userContext.Username, ActivityOwner.Staff, ActivityType.Automated, "39", staffProfile: staffcontext.StaffInfo);
			//else
			//  ServiceClientLocator.AnnotationClient(App).SaveActivity(userContext.UserId, userContext.Username, ActivityOwner.JobSeeker, ActivityType.Automated, "361");
			#endregion

      App.SetCookieValue("Career:ManualSearch", "true");
			Response.RedirectToRoute("JobSearchResults");
		}
  }
}
