﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class EmailResume : UserControlBase
	{
		#region Page Properties
		protected string EmailAddressRequired = "";
		protected string EmailAddressCharLimit = "";
		protected string EmailAddressErrorMessage = "";
		#endregion

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			LocaliseUI();
			if (!IsPostBack)
				BindDropdowns();
		}

		/// <summary>
		/// Shows the specified default resume format id.
		/// </summary>
		/// <param name="defaultResumeFormatId">The default resume format id.</param>
		/// <param name="defaultReviewResumeOrder">The default review resume order.</param>
		/// <param name="original">if set to <c>true</c> [original].</param>
		/// <param name="hasHiddenContact">Whether contact information has been hidden</param>
		public void Show(long defaultResumeFormatId, string defaultReviewResumeOrder, bool original, bool hasHiddenContact)
		{
			if (original)
			{
				if (!ddlEmailResumeFormat.Items.Contains(new ListItem("Original", "0")))
					ddlEmailResumeFormat.Items.Insert(0, new ListItem("Original", "0"));

				ddlEmailResumeOrder.Enabled = false;
			}
			ddlEmailResumeFormat.SelectValue(defaultResumeFormatId.ToString());
			ddlEmailResumeOrder.SelectedValue = defaultReviewResumeOrder;
			EmailResumeModal.Show();

			HiddenContactPlaceHolder.Visible = hasHiddenContact;
			rblHideOptions.SelectedIndex = hasHiddenContact ? 1 : 0;
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			EmailSubjectRequired.ErrorMessage = CodeLocalise("EmailSubject.RequiredErrorMessage", "Subject is required");
			EmailAddressRequired = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
			EmailAddressCharLimit = CodeLocalise("EmailAddressCharLimit.ErrorMessage", "Email address must be at least 6 characters");
			EmailAddressErrorMessage = CodeLocalise("EmailAddress.ErrorMessage", "Email address is not correct format");
		}

    /// <summary>
    /// Binds the dropdowns.
    /// </summary>
		private void BindDropdowns()
		{
			ddlEmailResumeFormat.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats), null, null);
		}

    /// <summary>
    /// Handles the Click event of the btnEmailResume control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void btnEmailResume_Click(object sender, EventArgs e)
		{
			try
			{
				var resume = string.Empty;
			  string mailBody;
				ResumeModel currentResume = null;

				if (ddlEmailResumeFormat.SelectedValue == "0" || (ddlEmailResumeFormat.SelectedValue != "0" && rdoDownloadType.SelectedIndex != 0))
				{
					currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");					
				}

				if (ddlEmailResumeFormat.SelectedValue == "0")
				{					
					if (currentResume.IsNotNull() && currentResume.ResumeMetaInfo.IsNotNull())
					{
					  string fileName;
					  string mimeType;
            string htmlResume;

            var resumeBytes = ServiceClientLocator.ResumeClient(App).GetOriginalUploadedResume(currentResume.ResumeMetaInfo.ResumeId.Value, out fileName, out mimeType, out htmlResume);

						mailBody = "<font face='arial'>This email is being sent to you containing a resume attachment that may be of interest to you.<br/>To open or save your resume file, please select the attachment below.</font>";
            ServiceClientLocator.CoreClient(App).SendEmail(txtToEmailAddress.Text.Trim(), txtEmailSubject.Text.Trim(), mailBody, true, resumeBytes, fileName);
					}
				}
				else
				{
					//var xslPath = App.Settings.GetCustomXslForResume(ddlEmailResumeFormat.SelectedValue);
                    //FVN-6797 AIP
				    var lookupItem = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats).FirstOrDefault(x => x.Id == ddlEmailResumeFormat.SelectedValueToLong());
				    var xslPath = App.Settings.GetCustomXslForResume(lookupItem.ExternalId);
                    /******************/
					if (xslPath.IsNotNullOrEmpty())
					{
						try
						{
							xslPath = HttpContext.Current.Server.MapPath(xslPath);
						}
						catch { }

						var xResume = Utilities.FetchXMLResume();
						resume = (rblHideOptions.SelectedIndex == 0)
                       ? Utilities.ApplyTemplate(xResume, xslPath, App, ddlEmailResumeOrder.SelectedValue, true)
                       : Utilities.ApplyTemplate(xResume, xslPath, App, ddlEmailResumeOrder.SelectedValue);
					}

					switch (rdoDownloadType.SelectedIndex)
					{
						case 0:
							{
                                resume = resume.Replace("\n","");
                                resume = resume.Replace("\r","");

								mailBody = string.Format("<font face='arial'>This message was sent to you by : {0}<br /></font>{1}", App.User.EmailAddress.Trim(),resume );
                ServiceClientLocator.CoreClient(App).SendEmail(txtToEmailAddress.Text.Trim(), txtEmailSubject.Text.Trim(), mailBody, isHtml:true);
								break;
							}
						case 1:
							{
								string rtfData;
                Utilities.Export2RTF(resume, out rtfData, App.User.ScreenName.IsNotNull() ? App.User.ScreenName : "");
								var byteArray = System.Text.Encoding.UTF8.GetBytes(rtfData);
								mailBody = "<font face='arial'>This email is being sent to you containing a resume RTF attachment that may be of interest to you.<br/>To open or save your resume file, please select the attachment below.</font>";
                ServiceClientLocator.CoreClient(App).SendEmail(txtToEmailAddress.Text.Trim(), txtEmailSubject.Text.Trim(), mailBody, true, byteArray, (App.User.IsNotNull() ? App.User.EmailAddress : "resume") + ".rtf");
								break;
							}
						case 2:
							{
								byte[] pdfData;
                Utilities.Export2PDF(resume, out pdfData, App.User.ScreenName.IsNotNull() ? App.User.ScreenName : "");
								mailBody = "<font face='arial'>This email is being sent to you containing a resume PDF attachment that may be of interest to you.<br/>To open or save your resume file, please select the attachment below.</font>";

                ServiceClientLocator.CoreClient(App).SendEmail(txtToEmailAddress.Text.Trim(), txtEmailSubject.Text.Trim(), mailBody, true, pdfData, (App.User.IsNotNull() ? App.User.EmailAddress : "resume") + ".pdf");
								break;
							}
					}

				}

				MasterPage.ShowModalAlert(AlertTypes.Info, "Email has been successfully sent.", "Email sent");
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
			EmailResumeModal.Hide();
		}
	}
}