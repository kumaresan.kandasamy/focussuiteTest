﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Profile.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.Profile" %>
<%@ Register Src="~/WebCareer/Controls/MilitaryHistory.ascx" TagName="MilitaryHistory"
    TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<script src="<%= ResolveUrl("~/Assets/Scripts/moment.min.js") %>" type="text/javascript"></script>
<div class="resumeSection">
    <asp:Panel ID="WorkforceProfileSaveNext1" runat="server" CssClass="resumeSectionAction">
        <asp:Button ID="SaveMoveToNextStepButton" Text="Save &amp; Move to Next Step &#187;"
            runat="server" class="buttonLevel2 greyedOut" ValidationGroup="Profile" OnClick="SaveMoveToNextStepButton_Click"
            disabled="disabled" OnClientClick="return CheckTypeofVeteran();" ClientIDMode="Static" />
    </asp:Panel>
    <div class="resumeSectionContent">
        <asp:Panel ID="EducationProfileSubHeading" runat="server" Visible="false" CssClass="resumeSectionSubHeading">
            <focus:LocalisedLabel runat="server" ID="EducationProfileSubHeadingLabel" RenderOuterSpan="False"
                LocalisationKey="EducationProfileSubHeading.Label" DefaultText="Profile" />
            &nbsp;<focus:LocalisedLabel runat="server" ID="EducationProfileSubHeadingRequiredLabel"
                CssClass="requiredDataLegend" LocalisationKey="EducationProfileSubHeadingRequired.Label"
                DefaultText="required fields" />
        </asp:Panel>
        <asp:PlaceHolder runat="server" ID="WorkforceProfileSubHeading">
            <p class="resumeJustify">
                <focus:LocalisedLabel runat="server" ID="WorkforceProfileSubHeadingInstructionLabel"
                    RenderOuterSpan="False" LocalisationKey="WorkforceProfileSubHeadingInstruction.Label"
                    DefaultText="We are required to ask a few demographic questions for federal reporting purposes. <span class='embolden underline'>None of the information you supply will display on your resume other than military service.</span> (You may hide military service on your resume when the resume is completed.)" />
            </p>
            <div class="resumeSectionSubHeading">
                <focus:LocalisedLabel runat="server" ID="WorkforceProfileSubHeadingRequiredLabel"
                    CssClass="requiredDataLegend" LocalisationKey="WorkforceProfileSubHeadingRequired.Label"
                    DefaultText="required fields" />
            </div>
        </asp:PlaceHolder>
        <div class="resumeProfile">
            <asp:PlaceHolder ID="WorkforceProfilePlaceHolder1" runat="server">
                <asp:PlaceHolder ID="DOBPlaceholder" runat="server">
                <div class="dataInputRow col-sm-12">
                    <div class="col-sm-3 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="DateOfBirthLabel" AssociatedControlID="DOBTextBox"
                            CssClass="dataInputLabel requiredData" LocalisationKey="DateOfBirth.Label" DefaultText="Date of birth" />
                    </div>
                    <div class="col-sm-9 col-md-10">
                        <span class="dataInputField"><span class="dataInputGroupedItemHorizontal">
                            <asp:TextBox ID="DOBTextBox" runat="server" ClientIDMode="Static" Width="150px"/>
                            <span class="instructionalText">mm/dd/yyyy</span>
                            <%--<ajaxtoolkit:MaskedEditExtender ID="meDOBDate" runat="server" Mask="99/99/9999" MaskType="None"
                                TargetControlID="DOBTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_">
                            </ajaxtoolkit:MaskedEditExtender>--%>
                            <br />
                            <asp:CustomValidator ID="DOBValidator" runat="server" ControlToValidate="DOBTextBox"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Profile"
                                ClientValidationFunction="ValidateProfileDOB" ValidateEmptyText="true" />
                            <asp:Label runat="server" CssClass="error" Visible="False" ID="DOBReadOnlyButRequiredLabel"></asp:Label>
                        </span></span>
                    </div>
                </div>
               </asp:PlaceHolder>
                <div class="dataInputRow col-sm-12">
                    <div class="col-sm-3 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="EmpStatusLabel" AssociatedControlID="ddlEmploymentStatus"
                            CssClass="dataInputLabel requiredData" LocalisationKey="EmpStatus.Label" DefaultText="Employment status" />
                    </div>
                    <div class="col-sm-9 col-md-10">
                        <span class="dataInputField">
                            <asp:DropDownList ID="ddlEmploymentStatus" runat="server" ClientIDMode="Static" Width="300px">
                                <asp:ListItem Text="- select status -" />
                            </asp:DropDownList>
                            <br />
                            <asp:CustomValidator ID="EmploymentStatusValidator" runat="server" ControlToValidate="ddlEmploymentStatus"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Profile"
                                ClientValidationFunction="ValidateEmploymentStatusofUser" ValidateEmptyText="true" />
                        </span>
                    </div>
                </div>
                <div class="dataInputRow col-sm-12">
                    <div class="col-sm-3 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="GenderLabel" AssociatedControlID="ddlGender"
                            CssClass="dataInputLabel requiredData" LocalisationKey="Gender.Label" DefaultText="Gender" />
                    </div>
                    <div class="col-sm-9 col-md-10">
                        <span class="dataInputField">
                            <asp:DropDownList ID="ddlGender" runat="server" Width="150px" ClientIDMode="Static">
                                <asp:ListItem Text="- select gender -" />
                            </asp:DropDownList>
                            <br />
                            <asp:RequiredFieldValidator ID="GenderRequired" runat="server" ControlToValidate="ddlGender"
                                CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" />
                        </span>
                    </div>
                </div>
                <div class="dataInputRow col-sm-12">
                    <div class="col-xs-4 col-sm-3 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="EthnicityLabel" AssociatedControlID="rblEthnicityHeritage"
                            CssClass="dataInputLabel requiredData" LocalisationKey="Ethnicity.Label" DefaultText="Ethnicity/heritage" />
                    </div>
                    <div class="col-xs-8 col-sm-9 col-md-10">
                        <span class="dataInputField">
                            <asp:RadioButtonList ID="rblEthnicityHeritage" CssClass="dataInputHorizontalRadioFloating"
                                runat="server" ClientIDMode="Static">
                            </asp:RadioButtonList>
                            <asp:CustomValidator ID="EthnicityHeritageValidator" runat="server" SetFocusOnError="true"
                                ControlToValidate="rblEthnicityHeritage" Display="Dynamic" CssClass="error" ValidationGroup="Profile"
                                ClientValidationFunction="validateEthnicityHeritage" ValidateEmptyText="true" />
                        </span>
                    </div>
                </div>
                <div class="dataInputRow col-sm-12">
                    <div class="col-xs-4 col-sm-3 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="RaceLabel" AssociatedControlID="cblRace"
                            CssClass="dataInputLabel requiredData" LocalisationKey="Race.Label" DefaultText="Race" />
                    </div>
                    <div class="col-xs-8 col-sm-9 col-md-10">
                        <span class="dataInputField">
                            <asp:CheckBoxList ID="cblRace" runat="server" CssClass="dataInputHorizontalCheckBoxListFloating"
                                ClientIDMode="Static">
                            </asp:CheckBoxList>
                            <asp:CustomValidator ID="RaceValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
                                ClientIDMode="Static" CssClass="error" ValidationGroup="Profile" ClientValidationFunction="validateRace"
                                ValidateEmptyText="true" />
                        </span>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="EducationProfilePlaceHolder" Visible="False">
                <div class="dataInputRow col-sm-12">
                    <div class="col-sm-3 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="ToApplyEducationLabel" Visible="False" CssClass="instructionalText instructionalTextBlock"
                            LocalisationKey="ToApply.Label" DefaultText="To apply for jobs, you must meet one of the following criteria. Please select which one applies to you?" />
                    </div>
                    <div class="col-sm-9 col-md-10">
                        <span class="dataInputField">
                            <asp:RadioButtonList ID="rblEligibleToWork" runat="server" CssClass="dataInputHorizontalRadio"
                                RepeatDirection="Vertical" ClientIDMode="Static">
                                <asp:ListItem Text="US citizen" Value="0" />
                                <asp:ListItem Text="Permanent resident alien" Value="1" />
                                <asp:ListItem Text="Authorized to work in the US through other criteria" Value="2" />
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="EligibleToWorkValidator" runat="server" ControlToValidate="rblEligibleToWork"
                                CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" />
                        </span>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="WorkforceProfilePlaceHolder2" runat="server">
                <div class="dataInputRow col-sm-12">
                    <div class="col-xs-4 col-sm-3 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="USCitizenLabel" LocalisationKey="USCitizen.Label"
                            DefaultText="U.S. citizen" AssociatedControlID="rblUSCitizen" CssClass="dataInputLabel requiredData" />
                    </div>
                    <div class="col-xs-8 col-sm-9 col-md-10">
                        <span class="dataInputField">
                            <asp:RadioButtonList ID="rblUSCitizen" runat="server" CssClass="dataInputHorizontalRadio"
                                RepeatDirection="Horizontal" onclick="ShowHideAlienRegistrationDetail();" ClientIDMode="Static">
                                <asp:ListItem Text="Yes" Value="yes" Selected="True" />
                                <asp:ListItem Text="No" Value="no" />
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="USCitizenRequired" runat="server" ControlToValidate="rblUSCitizen"
                                CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" />
                            <focus:LocalisedLabel runat="server" ID="ToApplyLabel" CssClass="instructionalText instructionalTextBlock"
                                LocalisationKey="ToApply.Label" DefaultText="To apply for jobs, you must be a U.S. citizen, a permanent resident<br />alien, or authorized to work in the U.S." />
                        </span>
                    </div>
                </div>
                <asp:Panel ID="panAlienRegistrationDetail" runat="server" CssClass="alienRegistrationDetail"
                    ClientIDMode="Static" Style="display: none; clear: left">
                    <p>
                        <focus:LocalisedLabel runat="server" ID="AlienRegistrationInstructionLabel" RenderOuterSpan="False"
                            LocalisationKey="AlienRegistrationInstruction.Label" DefaultText="Please tell us about your alien registration." />
                    </p>
                    <div class="dataInputRow">
                        <div class="col-sm-3 col-md-2">
                            <focus:LocalisedLabel runat="server" ID="AlienRegistrationLabel" AssociatedControlID="txtAlienRegNo"
                                CssClass="dataInputLabel requiredData" LocalisationKey="AlienRegistration.Label"
                                DefaultText="Alien Registration #" />
                        </div>
                        <div class="col-sm-9 col-md-10">
                            <span class="dataInputField dataInputGroupedItemHorizontal">
                                <asp:TextBox ID="txtAlienRegNo" runat="server" ClientIDMode="Static" Width="150px"
                                    MaxLength="10" />
                                <focus:LocalisedLabel runat="server" ID="AlienRegistrationExampleLabel" CssClass="instructionalText"
                                    LocalisationKey="AlienRegistrationExample.Label" DefaultText="(Example:A12345678 or A123456789)" />
                                <br />
                                <asp:CustomValidator ID="custAlienRegNo" runat="server" ControlToValidate="txtAlienRegNo"
                                    SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Profile"
                                    ClientValidationFunction="validateAlienRegNo" ValidateEmptyText="true" />
                            </span>
                        </div>
                    </div>
                    <div class="dataInputRow">
                        <div class="col-sm-3 col-md-2">
                            <focus:LocalisedLabel runat="server" ID="PermanentResidentLabel" AssociatedControlID="rblPermanentResident"
                                CssClass="dataInputLabel requiredData" LocalisationKey="PermanentResident.Label"
                                DefaultText="Are you a permanent resident or refugee?" />
                        </div>
                        <div class="col-sm-9 col-md-10">
                            <span class="dataInputField">
                                <asp:RadioButtonList ID="rblPermanentResident" runat="server" CssClass="dataInputHorizontalRadio"
                                    RepeatDirection="Horizontal" ClientIDMode="Static">
                                    <asp:ListItem Text="Yes" Value="yes" />
                                    <asp:ListItem Selected="True" Text="No" Value="no" />
                                </asp:RadioButtonList>
                            </span>
                        </div>
                    </div>
                    <div class="dataInputRow" id="AlienRegExpiryDiv">
                        <div class="col-sm-3 col-md-2">
                            <focus:LocalisedLabel runat="server" ID="AlienRegExpireLabel" AssociatedControlID="txtAlienRegExpire"
                                CssClass="dataInputLabel requiredData" LocalisationKey="AlienRegExpire.Label"
                                DefaultText="Expires" />
                        </div>
                        <div class="col-sm-9 col-md-10">
                            <span class="dataInputField dataInputGroupedItemHorizontal">
                                <asp:TextBox ID="txtAlienRegExpire" runat="server" ClientIDMode="Static" Width="150px"/>
                                <span class="instructionalText">mm/dd/yyyy</span>
                                <%--<ajaxtoolkit:MaskedEditExtender ID="meAlienRegExpire" runat="server" Mask="99/99/9999"
                                    MaskType="Date" TargetControlID="txtAlienRegExpire" ClearMaskOnLostFocus="false"
                                    PromptCharacter="_">
                                </ajaxtoolkit:MaskedEditExtender>--%>
                                <br />
                                <asp:CustomValidator ID="custAlienRegExpire" runat="server" ControlToValidate="txtAlienRegExpire"
                                    SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="Profile"
                                    ClientValidationFunction="validateAlienRegExpire" ValidateEmptyText="true" />
                            </span>
                        </div>
                    </div>
                </asp:Panel>
                <div class="dataInputRow col-sm-12">
                    <div class="col-xs-4 col-sm-3 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="DisabilityLabel" AssociatedControlID="ddlDisabilityStatus"
                            CssClass="dataInputLabel requiredData" LocalisationKey="Disability.Label" DefaultText="Disability" />
                    </div>
                    <div class="col-xs-8 col-sm-9 col-md-10">
                        <span class="dataInputField"><span class="dataInputGroupedItemHorizontal">
                            <asp:DropDownList ID="ddlDisabilityStatus" runat="server" Width="210px" ClientIDMode="Static"
                                onchange="EnableDisableStatus();">
                                <asp:ListItem Text="Not disabled" />
                            </asp:DropDownList>
                            <br />
                            <asp:RequiredFieldValidator ID="DisabilityStatusRequired" runat="server" ControlToValidate="ddlDisabilityStatus"
                                CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" />
                        </span>&nbsp;&nbsp;&nbsp;<span class="dataInputGroupedItemHorizontal">
                            <focus:LocalisedLabel runat="server" ID="lblDisabilityCategory" CssClass="instructionalText instructionalTextBlock"
                                ClientIDMode="Static" LocalisationKey="ToApply.Label" DefaultText="Check all that apply" />
                            <asp:CheckBoxList ID="cblDisabledCategory" runat="server" CssClass="dataInputHorizontalCheckBoxListFloatingTwo"
                                ClientIDMode="Static">
                            </asp:CheckBoxList>
                            <br />
                            <asp:CustomValidator ID="DisabilityValidator" runat="server" SetFocusOnError="true"
                                Display="Dynamic" ClientIDMode="Static" CssClass="error" ValidationGroup="Profile"
                                ClientValidationFunction="validateDisabledCategory" ValidateEmptyText="true" />
                            <asp:CustomValidator ID="cblDisabledCategoryValidator" runat="server" SetFocusOnError="true"
                                Display="Dynamic" ClientIDMode="Static" CssClass="error" ValidationGroup="Profile"
                                ClientValidationFunction="ValidateDisabilityNotDisclosedChecked" />
                        </span></span>
                    </div>
                </div>
                <div class="dataInputRow" id="SelectiveServiceDiv" clientidmode="Static">
                    <div class="dataInputRow">
                        <div class="col-xs-4 col-sm-3 col-md-2">
                            <span class="dataInputField">
                                <asp:RadioButtonList ID="rblSelectiveService" runat="server" CssClass="dataInputHorizontalRadio"
                                    RepeatDirection="Horizontal" ClientIDMode="Static">
                                    <asp:ListItem Text="Yes" Value="yes" />
                                    <asp:ListItem Text="No" Value="no" />
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="SelectiveServiceRequired" runat="server" ControlToValidate="rblSelectiveService"
                                    CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile"
                                    ClientIDMode="Static" />
                            </span>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-9">
                            <span class="dataInputField">
                                <focus:LocalisedLabel runat="server" ID="SelectiveServiceLabel" AssociatedControlID="rblSelectiveService"
                                    CssClass="requiredData" LocalisationKey="SelectiveServiceLabel.Label" DefaultText="Have you registered with the Selective Service?" />
                            </span>
                        </div>
                    </div>
                </div>
                <div class="dataInputRow">
                    <div class="col-xs-4 col-sm-3 col-md-2">
                        <span class="dataInputField">
                            <asp:RadioButtonList ID="rblMSFW" runat="server" CssClass="dataInputHorizontalRadio"
                                RepeatDirection="Horizontal" ClientIDMode="Static">
                                <asp:ListItem Text="Yes" Value="yes" />
                                <asp:ListItem Text="No" Value="no" />
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator ID="MSFWRequired" runat="server" ControlToValidate="rblMSFW"
                                CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" />
                        </span>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-9">
                        <span class="dataInputField">
                            <focus:LocalisedLabel runat="server" ID="MSFWLabel" LocalisationKey="MSFW.Label"
                                RenderOuterSpan="True" DefaultText="Have you done farm or food-processing work during the past year?"
                                AssociatedControlID="rblMSFW" CssClass="requiredData" />
                        </span>
                    </div>
                </div>
                <asp:Panel ID="panMSFWDetail" runat="server" CssClass="alienRegistrationDetail" ClientIDMode="Static"
                    Style="display: none;">
                    <div>
                        <focus:LocalisedLabel ID="MSFWCheckAllThatApply" runat="server" LocalisationKey="MSFWCheckAllThatApply.Label"
                            DefaultText="Please check all that apply:" />
                    </div>
                    <div class="dataInputRow">
                        <span class="dataInputField">
                            <asp:CheckBoxList ID="cblMSFWQuestions" runat="server" CssClass="dataInputVerticalRadio"
                                RepeatDirection="Vertical" ClientIDMode="Static">
                            </asp:CheckBoxList>
                            <%--<asp:CustomValidator ID="MSFWQuestionRequired" runat="server" CssClass="error" SetFocusOnError="true"
                                Display="Dynamic" ValidationGroup="Profile" ClientValidationFunction="validateMSFWQuestions"
                                ValidateEmptyText="true" />--%>
                        </span>
                    </div>
                </asp:Panel>
            </asp:PlaceHolder>
            <div class="dataInputRow col-sm-12">
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <span class="dataInputField">
                        <asp:RadioButtonList ID="rblMilitaryService" runat="server" CssClass="dataInputHorizontalRadio"
                            RepeatDirection="Horizontal" onclick="ShowHideMilitaryServiceDetail(true);" ClientIDMode="Static">
                            <asp:ListItem Text="Yes" Value="yes" />
                            <asp:ListItem Text="No" Value="no" />
                        </asp:RadioButtonList>
                        <asp:CustomValidator ID="MilitaryServiceRequired" runat="server" CssClass="error"
                            SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" ClientValidationFunction="validateMilitaryService"
                            ControlToValidate="rblMilitaryService" ValidateEmptyText="true" ErrorMessage="Military service status is required" />
                    </span>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-9">
                    <span class="dataInputField">
                        <focus:LocalisedLabel runat="server" ID="MilitaryServiceLabel" AssociatedControlID="rblMilitaryService"
                            CssClass="requiredData" LocalisationKey="MilitaryService.Label" DefaultText="Are you a veteran of the US Armed Forces, an eligible veteran spouse, or a current service member (or spouse of) who is leaving or retiring from the military within the next 12 months?" />
                    </span>
                </div>
            </div>
            <asp:Panel ID="panMilitaryService" runat="server" ClientIDMode="Static" class="militaryServiceDetails"
                Style="display: none; clear: left">
                <div class="resumeSectionSubHeading">
                    <focus:LocalisedLabel runat="server" ID="MilitaryServiceInstructionLabel" RenderOuterSpan="False"
                        LocalisationKey="MilitaryServiceInstruction.Label" DefaultText="Please tell us about your military service." />
                </div>
                <uc:MilitaryHistory runat="server" ID="MilitaryHistory" />
                <div class="dataInputRow">
                    <div id="HomelessSpan" class="col-xs-7 col-sm-5 col-md-2">
                        <focus:LocalisedLabel runat="server" ID="HomelessLabel" AssociatedControlID="rblHomeless"
                            CssClass="dataInputLabel" LocalisationKey="Homeless.Label" DefaultText="Are you currently homeless?" />
                    </div>
                    <div class="dataInputField">
                        <asp:RadioButtonList ID="rblHomeless" runat="server" CssClass="dataInputHorizontalRadio"
                            RepeatDirection="Horizontal" ClientIDMode="Static">
                            <asp:ListItem Text="Yes" Value="yes" />
                            <asp:ListItem Text="No" Value="no" Selected="True" />
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="dataInputRow">
                    <div class="militaryServiceLeft col-sm-12 col-md-6" id="TapSpan" runat="server" clientidmode="Static">
                        <div class="col-xs-7 col-sm-5 col-md-4">
                            <focus:LocalisedLabel runat="server" ID="TapLabel" AssociatedControlID="rblTap" CssClass="dataInputLabel"
                                LocalisationKey="Tap.Label" DefaultText="Have you attended a Transition Assistance Program course within the last three years?" />
                        </div>
                        <div class="dataInputField">
                            <asp:RadioButtonList ID="rblTap" runat="server" CssClass="dataInputHorizontalRadio"
                                RepeatDirection="Horizontal" ClientIDMode="Static">
                                <asp:ListItem Text="Yes" Value="yes" />
                                <asp:ListItem Text="No" Value="no" Selected="True" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="militaryServiceRight">
                    </div>
                </div>
                <div class="dataInputRow" id="TapDateDiv">
                    <div class="militaryServiceLeft col-sm-12 col-md-6" id="TapDateSpan" runat="server"
                        clientidmode="Static">
                        <div class="col-xs-7 col-sm-5 col-md-4">
                            <focus:LocalisedLabel runat="server" ID="TapAttendedDateLabel" AssociatedControlID="TapAttendanceDateTextBox"
                                CssClass="dataInputLabel requiredData" LocalisationKey="Tap.Label" DefaultText="TAP course attended date" />
                        </div>
                        <div class="dataInputField">
                            <asp:TextBox ID="TapAttendanceDateTextBox" runat="server" ClientIDMode="Static" Width="150px"/>
                            <%--<ajaxtoolkit:MaskedEditExtender ID="meTapAttendanceDate" runat="server" Mask="99/9999"
                                MaskType="None" TargetControlID="TapAttendanceDateTextBox" ClearMaskOnLostFocus="false"
                                PromptCharacter="_" />--%>
                            <span class="instructionalText">mm/yyyy</span>
                            <asp:CustomValidator ID="TapAttendanceDateValidator" runat="server" ControlToValidate="TapAttendanceDateTextBox"
                                SetFocusOnError="false" Display="Dynamic" CssClass="error" ValidationGroup="Profile"
                                ClientValidationFunction="ValidateTapAttendanceDate" ValidateEmptyText="true" />
                        </div>
                    </div>
                </div>
                <div class="dataInputRow">
                    <div class="militaryServiceLeft col-sm-12 col-md-6" id="SubscribeToVetAlertsSpan"
                        runat="server">
                        <div class="col-xs-7 col-sm-5 col-md-4">
                            <label class="dataInputLabel" for="rblSubscribeToVetAlerts">
                                <%=HtmlLocalise("SubscribeToVetAlerts.Label", "Unsubscribe from Veteran Priority of Service job alerts")%>
                                <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                    title="<%=HtmlLocalise("SubscribeVetAlerts.Label", "As a veteran or other eligible entitled to Priority of Service, you may receive special email alerts on jobs for which you qualify. Alerts are sent in advance of non-veteran job candidates viewing these job postings. You may unsubscribe to this service if you prefer not to be considered in advance of non-veteran candidates.") %>" />
                            </label>
                        </div>
                        <div class="dataInputField">
                            <asp:RadioButtonList ID="rblSubscribeToVetAlerts" runat="server" CssClass="dataInputHorizontalRadio"
                                RepeatDirection="Horizontal" ClientIDMode="Static">
                                <asp:ListItem Text="Yes" Value="yes" />
                                <asp:ListItem Text="No" Value="no" Selected="True" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="dataInputRow">
                <div class="col-xs-4 col-sm-3 col-md-2">
                </div>
                <div class="col-xs-8 col-sm-8 col-md-9">
                    <span class="dataInputField"></span>
                    <!--instructionalText instructionalTextBlock-->
                    <focus:LocalisedLabel runat="server" ID="IssuePanelInstructionalText" CssClass="instructionalText instructionalTextBlock resumeJustify"
                        LocalisationKey="MilitaryServiceL.Label" DefaultText="Please review each of the panels below. They describe possible issues you may face in finding employment and will allow us to identify your needs and programs that may be available to help you.<b>The information you provide is secure and strictly confidential. It will not appear on your resume. It will not be shared with any potential employer.</b>" />
                </div>
            </div>
            <div class="dataInputRow col-sm-12" style="clear: left">
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <span class="dataInputField">
                        <asp:LinkButton ID="HouseIssueOpenButton" runat="server" Text="open" OnClientClick="return ShowHouseIssueDetail();" />
                        <asp:LinkButton ID="HouseIssueCloseButton" runat="server" Text="close" OnClientClick="return HideHouseIssueDetail();"
                            CssClass="divider" />
                    </span>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-9">
                    <focus:LocalisedLabel runat="server" ID="HouseIssueLocalisedLabel" AssociatedControlID="rblMilitaryService"
                        CssClass="dataInputField" LocalisationKey="one.Label" DefaultText="Could you possibly have housing issues?" />
                </div>
            </div>
            <asp:Panel ID="panHouseIssueDetail" runat="server" CssClass="alienRegistrationDetail"
                Style="display: none; clear: left" ClientIDMode="Static">
                <div class="dataInputRow">
                    <span class="dataInputField"><i>Please check all items that describe your circumstances</i>
                    </span>
                    <br />
                </div>
                <div class="dataInputRow">
                    <div>
                        <span class="dataInputField">
                            <asp:CheckBox ID="cbHouseIssueHomeless" Text="I am currently homeless and have nowhere to stay."
                                runat="server" CssClass="dataInputHorizontalCheckBoxList" Value="no" ClientIDMode="Static" /></span>
                    </div>
                    <div>
                        <span class="dataInputField">
                            <asp:CheckBox ID="cbHouseIssueStaying" Text="I am currently homeless but I am staying in a shelter, a hotel, or with family or friends."
                                runat="server" CssClass="dataInputHorizontalCheckBoxList" ClientIDMode="Static"
                                Value="no" /></span>
                        <div>
                            <asp:CustomValidator ID="HomelessCheckBoxValidator" runat="server" ErrorMessage="Please choose between check box 1 and 2  but not both"
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Profile"
                                ClientValidationFunction="ValidateHomelessCheckBox" ValidateEmptyText="true" />
                        </div>
                    </div>
                    <div>
                        <span class="dataInputField">
                            <asp:CheckBox ID="cbHouseIssueYouth" Text="I am currently a runaway youth under age 18. "
                                runat="server" CssClass="dataInputHorizontalCheckBoxList" ClientIDMode="Static"
                                Value="no" />
                        </span>
                        <div>
                            <asp:CustomValidator ID="RunAwayYouthValidator" runat="server" ErrorMessage="Your date of birth indicates that you are over 18 years of age. To select this item, you must be 18 years old or younger."
                                SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Profile"
                                ClientValidationFunction="ValidateRunawayYouth" ValidateEmptyText="true" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="dataInputRow  col-sm-12">
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <span class="dataInputField">
                        <asp:LinkButton ID="IncomeIssueOpenButton" runat="server" ClientIDMode="Static" Text="open"
                            OnClientClick="return ShowIncomeIssueDetail();" />
                        <asp:LinkButton ID="IncomeIssueCloseButton" runat="server" ClientIDMode="Static"
                            OnClientClick="return HideIncomeIssueDetail();" Text="close" CssClass="divider" /></span></div>
                <div class="col-xs-8 col-sm-8 col-md-9">
                    <focus:LocalisedLabel runat="server" ID="IncomeIssueLocalisedLabel" AssociatedControlID="rblMilitaryService"
                        CssClass="dataInputField" LocalisationKey="one.Label" DefaultText="Could you possibly have income issues?" />
                </div>
            </div>
            <asp:Panel ID="panIncomeIssueDetail" runat="server" CssClass="alienRegistrationDetail"
                Style="display: none; clear: left" ClientIDMode="Static">
                <div class="dataInputRow col-xs-12 col-sm-12 col-md-12">
                    <div class="dataInputField col-xs-6 col-sm-8 col-md-8">
                        <focus:LocalisedLabel ID="IncomeIssueLabel1" runat="server" LocalisationKey="TotalDependance.Label"
                            DefaultText="Including myself, the total number of dependents who live with me is" />
                    </div>
                    <div class="dataInputField col-sm-1 col-md-1">
                    </div>
                    <div class="dataInputField col-xs-4 col-sm-3 col-md-3">
                        <asp:TextBox ID="DependantsTextBox" runat="server" Width="90px" MaxLength="2" placeholder="Enter number"
                            ClientIDMode="Static" />
                        <ajaxtoolkit:FilteredTextBoxExtender ID="fteDependants" runat="server" TargetControlID="DependantsTextBox"
                            FilterType="Custom" ValidChars="0123456789" />
                        <asp:CustomValidator ID="DependantsValidator" runat="server" CssClass="error" ControlToValidate="DependantsTextBox"
                            SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" ClientValidationFunction="validateDependants"
                            ValidateEmptyText="true" />
                    </div>
                </div>
                <div class="dataInputRow col-xs-12 col-sm-12 col-md-12">
                    <div class="dataInputField col-xs-6 col-sm-8 col-md-8">
                        <focus:LocalisedLabel ID="IncomeIssueLabel2" runat="server" LocalisationKey="EstimatedMonthlyEarning.Label"
                            DefaultText="The estimated monthly earnings for all family members in my household are (<b>Note:</b>  If <b>you</b> are disabled, enter <b>only your</b> monthly earnings.)" />
                    </div>
                    <div class="dataInputField col-sm-1 col-md-1">
                    </div>
                    <div class="dataInputField col-xs-4 col-sm-3 col-md-3">
                        <asp:TextBox ID="IncomeTextBox" runat="server" Width="150px" ClientIDMode="Static"
                            MaxLength="10" placeholder="$ Enter monthly amount" />
                        <ajaxtoolkit:FilteredTextBoxExtender ID="fteIncome" runat="server" TargetControlID="IncomeTextBox"
                            FilterType="custom" ValidChars="0123456789.," />
                        <br />
                        <%--<asp:CustomValidator ID="IncomeTextboxValidator" runat="server" CssClass="error"
                            ControlToValidate="IncomeTextBox" FilterType="Custom" SetFocusOnError="false"
                            Display="Dynamic" ValidationGroup="Profile" ClientValidationFunction="validateIncomeTextBox"
                            ValidateEmptyText="true" />--%>
                    </div>
                </div>
                <div class="dataInputRow">
                    <span class="dataInputField">
                        <focus:LocalisedLabel ID="IncomeIssueLabel3" runat="server" LocalisationKey="panIncomeIssue.Label"
                            DefaultText="<i>Please check all items that describe your circumstances. </i>" />
                    </span>
                </div>
                <div class="dataInputRow">
                    <div class="dataInputField">
                        <asp:CheckBox ID="cbDisplacedHomemaker" runat="server" ClientIDMode="Static" CssClass="dataInputHorizontalCheckBoxList label"
                            Text=" I am an unemployed homemaker who depended on another person to provide income for the family. That person is no longer providing this income or the income has been <br/>significantly reduced." /><br />
                        <asp:CustomValidator ID="employeValidator" runat="server" CssClass="error" ErrorMessage="Employment status must be “Not Employed” to make this selection."
                            FilterType="Custom" SetFocusOnError="false" Display="Dynamic" ValidationGroup="Profile"
                            ClientValidationFunction="validateEmploye" ValidateEmptyText="true" />
                    </div>
                    <div class="dataInputField">
                        <asp:CheckBox ID="cbSingleParent" runat="server" ClientIDMode="Static" CssClass="dataInputHorizontalCheckBoxList label"
                            Text=" I am single, separated, divorced, or widowed and have dependents under age 18. This also includes any single female dependent, no matter what her age, if she is pregnant. " />
                    </div>
                    <div class="dataInputField">
                        <asp:CheckBox ID="cbLowIncomeStatus" runat="server" ClientIDMode="Static" CssClass="dataInputHorizontalCheckBoxList label"
                            Text=" I, or one of my dependent family members, received SNAP, TANF, or SSI payments in the last six months." />
                    </div>
                </div>
            </asp:Panel>
            <div class="dataInputRow col-xs-12 col-sm-12 col-md-12">
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <span class="dataInputField">
                        <asp:LinkButton ID="CulturalIssueOpenButton" runat="server" ClientIDMode="Static"
                            Text="open" OnClientClick="return ShowCulturalIssueDetail();" />
                        <asp:LinkButton ID="CulturalIssueCloseButton" runat="server" ClientIDMode="Static"
                            Text="close" OnClientClick="return HideCulturalIssueDetail();" CssClass="divider" /></span>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-9">
                    <focus:LocalisedLabel runat="server" ID="CulturalIssueLocalisedLabel" AssociatedControlID="rblMilitaryService"
                        CssClass="dataInputField" LocalisationKey="one.Label" DefaultText="Could you possibly have language or cultural issues?" />
                </div>
            </div>
            <asp:Panel ID="panCulturalIssueDetail" runat="server" CssClass="alienRegistrationDetail"
                Style="display: none; clear: left" ClientIDMode="Static">
                <div class="dataInputRow">
                    <span class="dataInputField"><i>Please check all items that describe your circumstances.
                        Items marked with &nbsp; </i><span class="requiredDataLegend"></span>&nbsp; <i>are required
                        </i></span>
                </div>
                <div class="dataInputRow col-xs-12 col-sm-12 col-md-12" style="clear: left">
                    <div class="dataInputField col-xs-4 col-sm-3 col-md-2">
                        <focus:LocalisedLabel ID="CultureIssueDropdownLabel" runat="server" LocalisationKey="panCultureIssue.Label"
                            DefaultText="My preferred language is: " />
                    </div>
                    <div class="dataInputField col-xs-8 col-sm-4 col-md-4">
                        <asp:DropDownList ID="ddlPreferredLanguage" runat="server" Width="180px" ClientIDMode="Static">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="dataInputRow" style="clear: left">
                    <div class="dataInputField">
                        <asp:CheckBox ID="cbLowLevelLiteracy" runat="server" CssClass="dataInputHorizontalCheckBox langPref"
                            ClientIDMode="Static" Text=" I am uncomfortable in a workplace where I must read, write, or speak English to perform my job." />
                        <asp:Panel ID="panLanguageDetail" runat="server" CssClass="alienRegistrationDetail"
                            Style="border: none; margin-left: 25px;" ClientIDMode="Static">
                            <div style="clear: left">
                                <div class="col-sm-4 col-md-4">
                                    <focus:LocalisedLabel ID="CultureIssueNativeLocalisedLbel" runat="server" LocalisationKey="panCultureIssueCheckDetail.Label"
                                        CssClass="requiredData" DefaultText="My native language is" />
                                </div>
                                <div class="col-sm-7 col-md-8">
                                    <asp:DropDownList ID="ddlNativeLanguage" runat="server" Width="210px" ClientIDMode="Static">
                                    </asp:DropDownList>
                                    <asp:CustomValidator ID="NativeLanguageRequired" runat="server" CssClass="error"
                                        SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" ClientValidationFunction="validateNativeLanguage"
                                        ValidateEmptyText="true" />
                                </div>
                            </div>
                            <div style="clear: left; margin-top: 4px" class="col-xs-12">
                                <div class="col-sm-8 col-md-9">
                                    <focus:LocalisedLabel ID="CultureIssueCommonLanguageLocalisedLabel" runat="server"
                                        LocalisationKey="panCultureIssueCheckDetail.Label" CssClass="requiredData" DefaultText="I live in an area where the most common language used is" />
                                </div>
                                <div class="col-sm-4 col-md-3">
                                    <asp:DropDownList ID="ddlCommonLanguage" runat="server" Width="210px" ClientIDMode="Static">
                                    </asp:DropDownList>
                                    <asp:CustomValidator ID="CommonLanguageRequired" runat="server" CssClass="error"
                                        SetFocusOnError="true" Display="Dynamic" ValidationGroup="Profile" ClientValidationFunction="validateCommonLanguage"
                                        ValidateEmptyText="true" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="dataInputRow">
                    <span class="dataInputField">
                        <asp:CheckBox ID="cbCulturalBarriers" runat="server" CssClass="dataInputHorizontalCheckBoxList"
                            ClientIDMode="Static" Text=" I am concerned that my personal culture, beliefs, or dress may be perceived as barriers to my employment."
                            Value="no" />
                    </span>
                </div>
            </asp:Panel>
            <div class="dataInputRow col-xs-12">
                <div class="col-xs-4 col-sm-3 col-md-2">
                    <span class="dataInputField" id="">
                        <asp:LinkButton ID="LegalIssueOpenButton" runat="server" ClientIDMode="Static" Text="open"
                            OnClientClick="return ShowLegalIssueDetail();" />
                        <asp:LinkButton ID="LegalIssueCloseButton" runat="server" ClientIDMode="Static" Text="close"
                            OnClientClick="return HideLegalIssueDetail();" CssClass="divider" />
                    </span>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-9">
                    <focus:LocalisedLabel runat="server" ID="LegalIssueLocalisedLabel" AssociatedControlID="rblMilitaryService"
                        CssClass="dataInputField" LocalisationKey="one.Label" DefaultText="Could you possibly have legal issues? " />
                </div>
            </div>
            <asp:Panel ID="panLegalIssueDetail" runat="server" CssClass="alienRegistrationDetail"
                Style="display: none; clear: left" ClientIDMode="Static">
                <div class="dataInputRow">
                    <span class="dataInputField"><i>Please check all items that describe your circumstances.</i>
                    </span>
                </div>
                <div class="dataInputRow">
                    <span class="dataInputField">
                        <asp:CheckBox ID="panelLegalIssuesCheckbox" runat="server" CssClass="dataInputHorizontalCheckBoxList"
                            ClientIDMode="Static" Text="I have an arrest or court conviction in my background." />
                    </span>
                </div>
            </asp:Panel>
        </div>
    </div>
    <asp:Panel ID="WorkforceProfileSaveNext2" runat="server" CssClass="resumeSectionAction"
        Style="clear: left">
        <asp:Button ID="SaveMoveToNextStepButton1" Text="Save &amp; Move to Next Step &#187;"
            runat="server" class="buttonLevel2 greyedOut" ValidationGroup="Profile" OnClick="SaveMoveToNextStepButton_Click"
            OnClientClick="return CheckTypeofVeteran();" disabled="disabled" ClientIDMode="Static" />
    </asp:Panel>
</div>
<script language="javascript" type="text/javascript">
    Sys.Application.add_load(Profile_PageLoad);
    $(document).ready(function () {
        $('#DOBTextBox').mask('99/99/9999');
        $('#txtAlienRegExpire').mask('99/99/9999');
        $('#TapAttendanceDateTextBox').mask('99/9999');
    });
</script>
