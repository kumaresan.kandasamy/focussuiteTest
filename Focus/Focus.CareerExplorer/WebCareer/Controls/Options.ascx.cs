﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Core.Views;
using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class Options : UserControlBase
	{
		private List<Section> _section;
		
    	/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			AddSectionValidator.ErrorMessage = CodeLocalise("AddSectionValidator.Button", "&#171; Click here to add sections");
		}

		/// <summary>
		/// Handles the Click event of the SaveMoveToNextStepButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected void SaveMoveToNextStepButton_Click(object sender, EventArgs e)
		{
			_section = UnbindSectionRepeater();

			if (SaveModel())
			{
				if (App.Settings.HideResumeProfile && App.Settings.Theme == FocusThemes.Workforce) Response.RedirectToRoute("ResumeWizardTab", new { Tab = "preferences" });
				else Response.RedirectToRoute("ResumeWizardTab", new { Tab = "Profile" });
			}
		}

		# region Bind Add-Ins section

		/// <summary>
		/// Binds the step.
		/// </summary>
		internal void BindStep()
		{
			BindCheckBoxLists();

			var model = App.GetSessionValue<ResumeModel>("Career:Resume");

			if (model.IsNotNull() && model.ResumeContent.IsNotNull())
			{
				var sections = model.ResumeContent.AdditionalResumeSections;

				if (sections.IsNotNull())
				{
					_section = (from i in sections
											select new Section
											{
												ResumeSection = i.SectionType.IsNotNull() ? i.SectionType : ResumeSectionType.None,
												SectionName = i.SectionType.ToString(),
												Descriptions = i.SectionContent.ToString()
											}).OrderBy(s => s.SectionName).ToList();
				}
			}

			Bind();
		}

		#endregion

		# region Save Add-Ins section

		/// <summary>
		/// Saves the model.
		/// </summary>
		/// <returns></returns>
    private bool SaveModel()
    {
      bool saveStatus;
     
      var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
      var clonedResume = currentResume.CloneObject();
      var tempOptSections = _section.IsNull() ? null : (from i in _section
                                                        select new AdditionalResumeSection
                                                        {
                                                          UseSectionInResume = true,
                                                          SectionType = i.ResumeSection,
                                                          SectionContent = i.Descriptions
                                                        }).ToList();
      if (tempOptSections.Count == 0)
        tempOptSections = null;
      
      if (currentResume.IsNull())
      {
        currentResume = new ResumeModel
        {
          ResumeContent = new ResumeBody
          {
            AdditionalResumeSections = tempOptSections
          },
          ResumeMetaInfo = new ResumeEnvelope { CompletionStatus = ResumeCompletionStatuses.Options }
        };
      }
      else
      {
        currentResume.ResumeContent.AdditionalResumeSections = tempOptSections;
        currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.Options;
      }

			// FVN-785 If the profile tab is hidden mark that section as complete also
			if (App.Settings.HideResumeProfile)
				currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.Profile;

			var resumeNameUpdated = false; 
			var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
			if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
			{
				currentResume.ResumeMetaInfo.ResumeName = resumeName;
				resumeNameUpdated = true;
			}

			if (!Utilities.IsObjectModified(clonedResume, currentResume))
				return true;

			try
			{
				
				var resume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);
        var resumeId = resume.ResumeMetaInfo.ResumeId;
        
				if (!App.UserData.HasDefaultResume)
          App.UserData.DefaultResumeId = resumeId;

				if (App.UserData.DefaultResumeId == resumeId)
					Utilities.UpdateUserContextUserInfo(null, ResumeCompletionStatuses.Options);

				if (currentResume.ResumeMetaInfo.ResumeId.IsNull())
					currentResume.ResumeMetaInfo.ResumeId = resumeId;

				App.SetSessionValue("Career:ResumeID", resumeId);
				App.SetSessionValue("Career:Resume", currentResume);
				saveStatus = true;

				#region Save Activity

				if(App.UserData.HasDefaultResume && App.UserData.DefaultResumeId == resumeId)
				{
          //TODO: Martha (new activity functionality)
          //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "37", staffProfile: staffcontext.StaffInfo);
				}

				#endregion
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
				saveStatus = false;
			}
			return saveStatus;
		}

    
		#endregion

		#region Bind Components

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			BindSectionSelection();
			BindSectionRepeater();
		}

		/// <summary>
		/// Binds the section selection.
		/// </summary>
		private void BindSectionSelection()
		{
			if (_section.IsNotNull())
			{
				foreach (ListItem checkbox in SectionsCheckBoxList.Items)
				{
					var checkboxvalue = checkbox.Value;

					if (_section.Any(section => section.ResumeSection.ToString() == checkboxvalue))
					{
						checkbox.Selected = true;
						checkbox.Enabled = false;
					}
				}
			}
		}

		/// <summary>
		/// Binds the section repeater.
		/// </summary>
		private void BindSectionRepeater()
		{
			SectionRepeater.DataSource = _section;
			SectionRepeater.DataBind();
		}

		#endregion

		# region Unbind Components

		/// <summary>
		/// Unbinds the section repeater.
		/// </summary>
		/// <returns></returns>
		private List<Section> UnbindSectionRepeater()
		{
			return (from RepeaterItem item in SectionRepeater.Items
							let sectionId = ((HiddenField)item.FindControl("SectionHiddenID")).Value
							let sectionName = ((Label)item.FindControl("SectionLabel")).Text
							let description = ((TextBox)item.FindControl("SectionTextBox")).TextTrimmed()
							select new Section
							{
								ResumeSection = sectionId.AsEnum<ResumeSectionType>().GetValueOrDefault(),
								SectionName = sectionName,
								Descriptions = description
              }).OrderBy(s => s.SectionName).ToList();
		}

		#endregion

		#region ItemDataBound

		/// <summary>
		/// Handles the ItemDataBound event of the SectionRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="RepeaterItemEventArgs" /> instance containing the event data.</param>
		protected void SectionRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			var section = (Section)e.Item.DataItem;

			var sectionId = ((HiddenField)e.Item.FindControl("SectionHiddenID"));
			sectionId.Value = section.ResumeSection.ToString();

			var sectionname = ((Label)e.Item.FindControl("SectionLabel"));
		  sectionname.Text = CodeLocalise(string.Format("{0}.{1}.NoEdit", "ResumeSectionType", section.SectionName),
		                                  section.SectionName);
			
			var description = ((TextBox)e.Item.FindControl("SectionTextBox"));
			description.Text = section.Descriptions;

			var errmsg = ((RequiredFieldValidator)e.Item.FindControl("SectionRequired"));
			errmsg.ErrorMessage = CodeLocalise("Options.RequiredErrorMessage", string.Format("{0} is required", section.SectionName));

			if (sectionId.Value == ResumeSectionType.Branding.ToString())
			{
				Page.ClientScript.RegisterStartupScript(GetType(), "BrandingMaxLengthScript", "$('#" + description.ClientID + "').maxlength({max: 150});", true);
			}
		}

		#endregion

		#region ItemCommand - Delete

		/// <summary>
		/// Handles the ItemCommand event of the SectionRepeater control.
		/// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
		protected void SectionRepeater_ItemCommand(object source, CommandEventArgs e)
		{
			var selectedRow = e.CommandArgument.AsEnum<ResumeSectionType>();
			_section = UnbindSectionRepeater();

			switch (e.CommandName)
			{
				case "Delete":
					{
						_section.RemoveAll(x => x.ResumeSection == selectedRow);
						var checkbox = SectionsCheckBoxList.Items.FindByValue(selectedRow.ToString());
						checkbox.Selected = false;
						checkbox.Enabled = true;
					}
					break;

				case "Add":
					{
						foreach (var checkbox in SectionsCheckBoxList.Items.Cast<ListItem>().Where(checkbox => (checkbox.Selected && checkbox.Enabled)).OrderByDescending(cb => cb.Text))
						{
							_section.Insert(0, new Section { ResumeSection = checkbox.Value.AsEnum<ResumeSectionType>().GetValueOrDefault(), SectionName = checkbox.Text });
							checkbox.Enabled = false;
						}
					}
					break;
			}

			BindSectionRepeater();
		}

		#endregion

		# region Check box list binding

		/// <summary>
		/// Binds the check box lists.
		/// </summary>
		private void BindCheckBoxLists()
		{
			SectionsCheckBoxList.Items.Clear();

			SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.Affiliations, "Affiliations");

			if (App.Settings.ShowBrandingStatement)
				SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.Branding, "Branding Statement");

      SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.Honors, "Honors");
      SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.Interests, "Interests");

      if (App.Settings.Theme == FocusThemes.Workforce)
        SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.Internships, "Internships");

      if (!App.Settings.HideObjectives)
        SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.Objective, "Objective");

			if (!App.Settings.HidePersonalInformation)
				SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.Personals, "Personal Information");

      SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.ProfessionalDevelopment, "Professional Development");
			SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.Publications, "Publications");

			if (!App.Settings.HideReferences)
				SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.References, "References");

      if (App.Settings.Theme == FocusThemes.Workforce)
        SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.TechnicalSkills, "Technical Skills");

      if (App.Settings.Theme == FocusThemes.Education)
        SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.ThesisMajorProjects, "Thesis/Major Projects");

      SectionsCheckBoxList.Items.AddEnum(ResumeSectionType.VolunteerActivities, "Volunteer Activities");
		}

		# endregion
	}

	#region Helper Class

	public class Section
	{
		public ResumeSectionType ResumeSection { get; set; }
		public string SectionName { get; set; }
		public string Descriptions { get; set; }
	}

	#endregion
}
