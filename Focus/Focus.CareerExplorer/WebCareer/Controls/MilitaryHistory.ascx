﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MilitaryHistory.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.MilitaryHistory" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<div id="MilitaryHistoryControl">
<asp:Repeater ID="MilitaryHistoryRepeater" runat="server" OnItemDataBound="MilitaryHistoryRepeater_ItemDataBound" OnItemCommand="MilitaryHistoryRepeater_ItemCommand">
	<ItemTemplate>
	  <asp:PlaceHolder runat="server" ID="MultipleHeaderPlaceHolder">
    <div style="width: 100%">
      <div style="float: left">
    	  <h3><asp:Literal runat="server" ID="HistoryHeader" /></h3>
      </div>
      <div style="float: right">
				<asp:LinkButton ID="AddHistoryButton" ClientIDMode="AutoID" runat="server" CommandName="Add" />
        &nbsp;
				<asp:LinkButton ID="DeleteHistoryButton" ClientIDMode="AutoID" runat="server" CommandName="Delete" />
      </div>
      <div style="clear: both"></div>
    </div>
    </asp:PlaceHolder>
    <div class="dataInputRow col-md-12">
	    <div class="militaryServiceLeft col-sm-12 col-md-6">
		    <div class="col-sm-5 col-md-4"><focus:LocalisedLabel runat="server" ID="TypeOfVeteranLabel" AssociatedControlID="ddlVeteranType" CssClass="dataInputLabel requiredData" LocalisationKey="TypeOfVeteran.Label" DefaultText="Type of veteran"/>
		    </div><div class="dataInputField col-sm-6 col-md-8">
			    <asp:DropDownList ID="ddlVeteranType" runat="server" Width="300px">
				    <asp:ListItem Text="- select type -" />
			    </asp:DropDownList>
                <br/>
			    <asp:CustomValidator ID="custVeteranType" runat="server" ClientValidationFunction="ValidateTypeofVeteran" ControlToValidate="ddlVeteranType" CssClass="error" SetFocusOnError="true" ValidateEmptyText="True" Display="Dynamic" ValidationGroup="Profile"></asp:CustomValidator>
        </div>
	    </div>
	    <div class="militaryServiceRight col-sm-12 col-md-6">
		    <div class="col-sm-5 col-md-4"><focus:LocalisedLabel runat="server" ID="ConnectedDisabilityLabel" AssociatedControlID="ddlServiceDisability" CssClass="dataInputLabel" LocalisationKey="ConnectedDisability.Label" DefaultText="Do you have a service-connected disability?"/></div>
		    <div class="dataInputField col-sm-6 col-md-8">
			    <asp:DropDownList ID="ddlServiceDisability" runat="server" Width="255px">
				    <asp:ListItem Text="- select status -" />
			    </asp:DropDownList>
                <br/>
			    <asp:CustomValidator ID="custServiceDisability" runat="server" ClientValidationFunction="ValidateConnectedDisability" ControlToValidate="ddlServiceDisability" CssClass="error" SetFocusOnError="true" ValidateEmptyText="True" Display="Dynamic" ValidationGroup="VeteranType"></asp:CustomValidator>
		    </div>
	    </div>
    </div>
        <div class="dataInputRow col-md-12">
            <div class="col-md-6">
                <div class="militaryServiceLeft dataInputRow col-sm-12 col-md-12">
                    <div class="col-sm-5 col-md-4">
                        <focus:LocalisedLabel runat="server" ID="MilitaryEmploymentStatusLabel" AssociatedControlID="ddlMilitaryEmploymentStatus"
                            CssClass="dataInputLabel" LocalisationKey="MilitaryEmployment.Label" DefaultText="Employment status" />
                    </div>
                    <div class="dataInputField">
                        <asp:DropDownList ID="ddlMilitaryEmploymentStatus" runat="server" Width="300px">
                            <asp:ListItem Text="- select status -" />
                        </asp:DropDownList>
                        <br />
                        <asp:CustomValidator ID="custMilitaryEmploymentStatus" runat="server" ClientValidationFunction="ValidateEmploymentStatus"
                            ControlToValidate="ddlMilitaryEmploymentStatus" CssClass="error" SetFocusOnError="true"
                            ValidateEmptyText="True" Display="Dynamic" ValidationGroup="VeteranType"></asp:CustomValidator>
                    </div>
                </div>
                <div class="militaryServiceLeft dataInputRow col-sm-12 col-md-12">
                    <div class="col-sm-5 col-md-4">
                        <focus:LocalisedLabel runat="server" ID="TransitioningTypeLabel" AssociatedControlID="ddlTransitioningType"
                            CssClass="dataInputLabel" LocalisationKey="TransitioningType.Label" DefaultText="Transitioning type" />
                    </div>
                    <div class="dataInputField">
                        <asp:DropDownList ID="ddlTransitioningType" runat="server" Width="300px">
                            <asp:ListItem Text="- select type -" />
                        </asp:DropDownList>
                        <br />
                        <asp:CustomValidator ID="custTransitioningType" runat="server" ClientValidationFunction="ValidateTransType"
                            ControlToValidate="ddlTransitioningType" CssClass="error" SetFocusOnError="true"
                            ValidateEmptyText="True" Display="Dynamic" ValidationGroup="VeteranType"></asp:CustomValidator>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="militaryServiceLeft dataInputRow  col-sm-12 col-md-12">
                    <div class="col-sm-5 col-md-4">
                        <focus:LocalisedLabel runat="server" ID="BranchOfServiceLabel" AssociatedControlID="ddlBranchOfService"
                            CssClass="dataInputLabel" LocalisationKey="BranchOfService.Label" DefaultText="Branch of service" />
                    </div>
                    <div class="dataInputField">
                        <asp:DropDownList ID="ddlBranchOfService" runat="server" Width="255px">
                            <asp:ListItem>- select branch -</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        <asp:CustomValidator ID="custServiceBranch" runat="server" ClientValidationFunction="ValidateServiceBranch"
                            ControlToValidate="ddlBranchOfService" CssClass="error" SetFocusOnError="false"
                            ValidateEmptyText="True" Display="Dynamic" ValidationGroup="VeteranType"></asp:CustomValidator>
                    </div>
                </div>
                <div class="militaryServiceLeft dataInputRow col-sm-12 col-md-12">
                    <div class="col-sm-5 col-md-4">
                        <focus:LocalisedLabel runat="server" ID="RankLabel" AssociatedControlID="ddlRank"
                            CssClass="dataInputLabel" LocalisationKey="Rank.Label" DefaultText="Rank" />
                    </div>
                    <div class="dataInputField">
                        <focus:AjaxDropDownList ID="ddlRank" runat="server" Width="255px" />
                        <ajaxtoolkit:CascadingDropDown ID="ccdRank" runat="server" Category="Rank" LoadingText="[Loading rank...]"
                            ParentControlID="ddlBranchOfService" ServiceMethod="GetRank" ServicePath="~/Services/AjaxService.svc"
                            BehaviorID="ccdRank" TargetControlID="ddlRank">
                        </ajaxtoolkit:CascadingDropDown>
                        <br />
                        <asp:CustomValidator ID="custRank" runat="server" ClientValidationFunction="ValidateRank"
                            ControlToValidate="ddlRank" CssClass="error" SetFocusOnError="false" ValidateEmptyText="True"
                            Display="Dynamic" ValidationGroup="VeteranType"></asp:CustomValidator>
                    </div>
                </div>
            </div>
        </div>
    <div class="dataInputRow">
	    <div class="militaryServiceLeft col-sm-12 col-md-6">
		    <div class="col-sm-5 col-md-4"><focus:LocalisedLabel runat="server" ID="ServiceDutyStartDateLabel" AssociatedControlID="SDSDTextBox" CssClass="dataInputLabel" LocalisationKey="ServiceDutyStartDate.Label" DefaultText="Service duty start date"/></div>
		    <div class="dataInputField">
			    <span class="dataInputGroupedItemHorizontal">
				    <asp:TextBox ID="SDSDTextBox" runat="server" Width="150px" category="SDSDTextBox"/>
						<span class="instructionalText">mm/dd/yyyy</span>
				    <%--<ajaxtoolkit:MaskedEditExtender ID="meSDSD" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="SDSDTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_"></ajaxtoolkit:MaskedEditExtender>--%>
				    <br/>
				    <asp:CustomValidator ID="custSDSD" runat="server" ClientValidationFunction="ValidateStartDateFormat" ControlToValidate="SDSDTextBox" CssClass="error" SetFocusOnError="true" ValidateEmptyText="True" Display="Dynamic" ValidationGroup="VeteranType"></asp:CustomValidator>
			    </span>
		    </div>
	    </div>
	    <div class="militaryServiceRight col-sm-12 col-md-6">
		    <div class="col-sm-5 col-md-4"><focus:LocalisedLabel runat="server" ID="MilitaryOccupationTitle1Label" CssClass="dataInputLabel" LocalisationKey="MilitaryOccupationTitle1.Label" DefaultText="Military occupation title/specialty"/></div>
		    <div class="dataInputField">
			    <span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="MilitaryOccupationTitle2Label" AssociatedControlID="txtMilitaryOccupationTitle" LocalisationKey="MilitaryOccupationTitle2.Label" DefaultText="Enter your MOS, AFSC, NEC, etc." Width="250"/></span>
			    <asp:TextBox ID="txtMilitaryOccupationTitle" runat="server" Width="250px" onkeydown="SetContextKey();" />
			    <ajaxtoolkit:AutoCompleteExtender ID="aceMilitaryOccupationTitle" runat="server" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetMOS" ServicePath="~/Services/AjaxService.svc" TargetControlID="txtMilitaryOccupationTitle" CompletionInterval="1000" UseContextKey="true" EnableCaching="false" MinimumPrefixLength="2" OnClientShown="fixAutoCompleteExtender"></ajaxtoolkit:AutoCompleteExtender>
		    </div>
	    </div>
    </div>
    <div class="dataInputRow col-md-12">
	    <div class="militaryServiceLeft col-sm-12 col-md-6">
		    <div class="col-sm-5 col-md-4"><asp:Label runat="server" ID="lblPlannedEndDate" AssociatedControlID="PEDTextBox" CssClass="dataInputLabel" Text="Service duty end date" /></div>
		    <div class="dataInputField">
			    <span class="dataInputGroupedItemHorizontal">
				    <asp:TextBox ID="PEDTextBox" runat="server" Width="150px" category="PEDTextBox"/>
						<span class="instructionalText">mm/dd/yyyy</span>
				    <%--<ajaxtoolkit:MaskedEditExtender ID="mePED" runat="server" Mask="99/99/9999" MaskType="Date" TargetControlID="PEDTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_"></ajaxtoolkit:MaskedEditExtender>--%>
				    <br/>
				    <asp:CustomValidator ID="custPED" runat="server" ClientValidationFunction="ValidateEndDateFormat" ControlToValidate="PEDTextBox" CssClass="error" SetFocusOnError="true" ValidateEmptyText="True" Display="Dynamic" ValidationGroup="VeteranType"></asp:CustomValidator>
			    </span>
		    </div>
	    </div>
	    <div class="militaryServiceRight col-sm-12 col-md-6">
		    <div class="col-sm-5 col-md-4"><focus:LocalisedLabel runat="server" ID="UnitOrAffiliation1Label" CssClass="dataInputLabel" LocalisationKey="UnitOrAffiliation1.Label" DefaultText="Unit, Affiliation, and Honors"/></div>
		    <div class="dataInputField">
			    <span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="UnitOrAffiliation2Label" AssociatedControlID="txtUnit" LocalisationKey="UnitOrAffiliation2.Label" DefaultText="examples: Regiment, Brigade,<br/> Command, Fleet, or Squadron" Width="250"/></span>
			    <asp:TextBox ID="txtUnit" runat="server" Width="250px" TextMode="MultiLine" Rows="2" />
		    </div>
	    </div>
    </div>
		<div class="dataInputRow col-md-12">
	    <div class="militaryServiceLeft col-sm-12 col-md-6">
		    <div class="col-sm-5 col-md-4"><focus:LocalisedLabel runat="server" ID="MilitaryDischargeLabel" AssociatedControlID="ddlMilitaryDischarge" CssClass="dataInputLabel" LocalisationKey="MilitaryDischarge.Label" DefaultText="Military discharge"/></div>
		    <div class="dataInputField">
			    <asp:DropDownList ID="ddlMilitaryDischarge" runat="server" Width="300px">
				    <asp:ListItem Text="- select discharge -" />
			    </asp:DropDownList>
                <br/>
          <asp:CustomValidator ID="custMilitaryDischarge" runat="server" ClientValidationFunction="ValidateMilitaryDischarge" ControlToValidate="ddlMilitaryDischarge" CssClass="error" SetFocusOnError="true" ValidateEmptyText="True" Display="Dynamic" ValidationGroup="Profile"></asp:CustomValidator>
        </div>
	    </div>
	    <div class="militaryServiceRight col-md-6"></div>
    </div>
    <div class="dataInputRow col-md-12">
	    <div class="militaryServiceLeft col-sm-12 col-md-6">
		    <div class="col-xs-3 col-sm-5 col-md-4"><focus:LocalisedLabel runat="server" ID="CampaignVeteranLabel" AssociatedControlID="rblCampaignVeteran" CssClass="dataInputLabel" LocalisationKey="CampaignVeteran.Label" DefaultText="Campaign veteran"/></div>
		    <div class="dataInputField">
			    <asp:RadioButtonList ID="rblCampaignVeteran" runat="server" CssClass="dataInputHorizontalRadio" RepeatDirection="Horizontal">
				    <asp:ListItem Text="Yes" Value="yes" />
				    <asp:ListItem Text="No" Value="no" Selected="True" />
			    </asp:RadioButtonList>
		    </div>
	    </div>
	    <div class="militaryServiceRight col-md-6"></div>
    </div>
  </ItemTemplate>
</asp:Repeater>
</div>
<div class="resumeSectionSubHeading" style="clear:left"></div>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        MilitaryRepeaterItem();
    });
	Sys.Application.add_load(MilitaryHistory_PageLoad);
	Sys.WebForms.PageRequestManager.getInstance().add_endRequest(MilitaryHistory_UpdateStyles);

	function MilitaryRepeaterItem() {
	    $("input[category='SDSDTextBox']").each(function () {
	        $(this).mask('99/99/9999');
	    });
	    $("input[category='PEDTextBox']").each(function () {
	        $(this).mask('99/99/9999');
	    });
	    return true;
	}
</script>
