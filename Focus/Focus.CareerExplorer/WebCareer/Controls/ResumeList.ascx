﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeList.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.ResumeList" %>
<%@ Register Src="~/Controls/UpdateableList.ascx" TagName="UpdateableList" TagPrefix="uc" %>
		<div runat="server" ID="MyResumesContainer">
			<uc:UpdateableList ID="MyResumeList" runat="server" TargetPage="ResumeWizard" OnLinkNameClicked="OnLinkNameClicked" />
		</div>
