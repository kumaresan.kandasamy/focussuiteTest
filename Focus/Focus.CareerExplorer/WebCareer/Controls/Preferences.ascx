﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Preferences.ascx.cs"	Inherits="Focus.CareerExplorer.WebCareer.Controls.Preferences" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register Src="../../Controls/UpdateableList.ascx" TagName="UpdateableList" TagPrefix="uc" %>
<%@ Register src="ScrollableSkillsAccordion.ascx" tagname="SkillsAccordion" tagprefix="uc" %>

<div class="resumeSection">
	<asp:Panel ID="WorkforcePreferencesPlaceHolder1" runat="server" CssClass="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2 greyedOut" OnClientClick="return validateAllPreferences();" OnClick="SaveMoveToNextStepButton_Click" disabled="disabled" ClientIDMode="Static"  />
	</asp:Panel>
	<div class="resumeSectionContent">
		<div class="resumeSectionSubHeading" id="MyPreferencesSectionHeading" runat="server">
			<focus:LocalisedLabel runat="server" ID="MyPreferencesSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="MyPreferencesSubHeading.Label" DefaultText="My preferences"/>
			&nbsp;<focus:LocalisedLabel runat="server" ID="MyPreferencesSubHeadingRequiredLabel" CssClass="requiredDataLegend" LocalisationKey="MyPreferencesSubHeadingRequired.Label" DefaultText="required fields"/>
		</div>
		<div class="resumePreferences resumePreferencesJobPreferences" id="MyPreferencesSectionContent" runat="server">
			<div class="dataInputRow" id="ResumeSearchableRow" runat="server">
				<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" ID="MakeResumeLabel" AssociatedControlID="rblResumeSearchable" LocalisationKey="MakeResume.Label" DefaultText="Make my resume searchable to qualified #BUSINESSES#:LOWER" CssClass="dataInputLabel requiredData"/>
                <div><focus:LocalisedLabel CssClass="instructionalText instructionalTextBlock" runat="server" ID="ResumeSearchableInstructionsLabel" LocalisationKey="ResumeSearchableInstructions.Label" DefaultText="Your name and contact info will not be displayed – #BUSINESSES#:LOWER will only be able to contact you by confidential email."></focus:LocalisedLabel></div></div>
				<div class="dataInputField col-sm-4 col-md-4">
					<asp:RadioButtonList ID="rblResumeSearchable" runat="server" CssClass="dataInputHorizontalRadio" RepeatLayout="Flow"	RepeatDirection="Horizontal" onclick="ResumeSearchableChanged();" ClientIDMode="Static">
						<asp:ListItem Text="Yes" Value="yes" />
						<asp:ListItem Text="No" Value="no" />
					</asp:RadioButtonList>
					<asp:RequiredFieldValidator ID="ResumeSearchableRequired" runat="server" ControlToValidate="rblResumeSearchable" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Preferences" />
				</div>
				
			</div>

			<div class="dataInputRow" id="BridgesToOppRow" runat="server">
				<div class="dataInputLabel col-sm-4 col-md-4">
					<focus:LocalisedLabel runat="server" ID="BridgesToOppRowLabel" AssociatedControlID="BridgesToOppRowRadioButtons" LocalisationKey="BridgesToOppRow.Label" DefaultText="Hey, Are you interested in the Bridges to Opportunities training program in the Louisville area?" CssClass="requiredData"/>
					<a onclick="$find('BridgesToOppModal').show();" href="#">More information</a>
				</div>
				<div class="dataInputField">
					<asp:RadioButtonList ID="BridgesToOppRowRadioButtons" runat="server" CssClass="dataInputHorizontalRadio" RepeatLayout="Flow" RepeatDirection="Horizontal" ClientIDMode="Static">
						<asp:ListItem Text="Yes" Value="yes" />
						<asp:ListItem Text="No" Value="no" Selected="True" />
					</asp:RadioButtonList>
					<asp:RequiredFieldValidator ID="BridgesToOppValidator" runat="server" ControlToValidate="BridgesToOppRowRadioButtons" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Preferences" />
				</div>
			</div>

			<div class="dataInputRow" id="ContactVisibleToEmployersRow" runat="server">
				<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" ID="ContactDetailsVisibleLabel" AssociatedControlID="rblContactDetailsVisible" LocalisationKey="ContactDetailsVisible.Label" DefaultText="Make my contact details available to #BUSINESSES#:LOWER" CssClass="dataInputLabel requiredData embolden"/></div>
				<div class="dataInputField col-sm-4 col-md-4">
					<asp:RadioButtonList ID="rblContactDetailsVisible" runat="server" CssClass="dataInputHorizontalRadio" RepeatLayout="Flow"	RepeatDirection="Horizontal" onclick="ResumeSearchableChanged();" ClientIDMode="Static">
						<asp:ListItem Text="Yes" Value="yes" />
						<asp:ListItem Text="No" Value="no" />
					</asp:RadioButtonList>
					<asp:RequiredFieldValidator ID="ContactDetailsVisibleRequired" runat="server" ControlToValidate="rblContactDetailsVisible" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Preferences" />
				</div>
				<div class="col-sm-4 col-md-4"><focus:LocalisedLabel CssClass="instructionalText instructionalTextBlock embolden" runat="server" ID="ContactDetailsVisibleInstructionsLabel" LocalisationKey="ContactDetailsVisibleInstructions.Label" DefaultText="Choosing 'No' means your name and contact info will not be displayed – #BUSINESSES#:LOWER will only be able to contact you by confidential email."></focus:LocalisedLabel></div>
			</div>
			<asp:Panel ID="EducationPreferencesPlaceHolder1" runat="server" Visible="false" CssClass="dataInputRow">
				<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" ID="PostingsToSearchLabel" AssociatedControlID="PostingsToSearchCheckboxList" CssClass="requiredData dataInputLabel embolden" LocalisationKey="InterestedPostings.Label" DefaultText="Postings I am interested in"/></div>
				<div class="dataInputField col-md-4">
					<asp:CheckBoxList runat="server" ID="PostingsToSearchCheckboxList" CssClass="dataInputHorizontalRadio" RepeatLayout="Flow" RepeatDirection="Horizontal" ClientIDMode="Static" onclick="PostingsInterestedInChanged();" />
            <asp:CustomValidator ID="PostingsToSearchValidator" runat="server" ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Preferences" ClientValidationFunction="ValidatePostingsToSearch" ValidateEmptyText="true" />
				</div>
			</asp:Panel>
			<asp:PlaceHolder ID="WorkforcePreferencesPlaceHolder2" runat="server">
				<div class="dataInputRow">
					<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" ID="WagesPayLabel" AssociatedControlID="txtWages" CssClass="dataInputLabel requiredData" LocalisationKey="WagesPay.Label" DefaultText="Wages/pay unit"/>
                    <div><focus:LocalisedLabel runat="server" ID="WagesInstructionLabel" CssClass="instructionalText instructionalTextBlock" LocalisationKey="WagesInstruction.Label" DefaultText="The salary information you provide will not appear in your resume and is collected only to help provide you with matches."/></div></div>
					<div class="dataInputField col-sm-4 col-md-4">
						<span class="dataInputGroupedItemHorizontal">
							<asp:TextBox ID="txtWages" runat="server" Width="100px" ClientIDMode="Static" onBlur="this.value=formatCurrency(this.value); return AddAJob_Wages();" MaxLength="10" />
							<ajaxtoolkit:FilteredTextBoxExtender ID="fteWages" runat="server" TargetControlID="txtWages" FilterType="Custom" ValidChars=",.0123456789" />
						</span>
						<span class="dataInputGroupedItemHorizontal">
							<asp:DropDownList ID="ddlPayUnit" runat="server" ClientIDMode="Static" CssClass="PayUnitClass" Width="145px">
								<asp:ListItem Text="- select pay type -" />
							</asp:DropDownList>
						</span><br/>
						<asp:CustomValidator ID="PayUnitValidator" runat="server" ControlToValidate="ddlPayUnit" Display="Dynamic" CssClass="error" ValidationGroup="Preferences" ClientValidationFunction="validatePreferencesPayUnit" ValidateEmptyText="true" />
					</div>
					
				</div>
				<div class="dataInputRow">
					<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" ID="WorkOverTimeLabel" AssociatedControlID="rblWorkOverTime" CssClass="dataInputLabel requiredData" LocalisationKey="WorkOverTime.Label" DefaultText="Are you willing to work overtime?"/>
                    </div>
					<div class="dataInputField col-sm-4 col-md-4">
						<asp:RadioButtonList ID="rblWorkOverTime" runat="server" CssClass="dataInputHorizontalRadio" RepeatLayout="Flow" RepeatDirection="Horizontal">
							<asp:ListItem Text="Yes" Value="yes" />
							<asp:ListItem Text="No" Value="no" />
						</asp:RadioButtonList><br />

						<asp:RequiredFieldValidator ID="WorkOverTimeValidator" runat="server" ControlToValidate="rblWorkOverTime" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Preferences" />
                        
                       </div>
                      <div class="col-sm-4 col-md-4"></div>
				</div>
				<div class="dataInputRow">
					<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" ID="RelocateLabel" AssociatedControlID="rblRelocate" CssClass="dataInputLabel requiredData" LocalisationKey="Relocate.Label" DefaultText="Are you willing to relocate?"/>
                     <focus:LocalisedLabel CssClass="instructionalText instructionalTextBlock" runat="server" ID="RelocateInstructionsLabel"></focus:LocalisedLabel>
                    </div>
					<div class="dataInputField col-sm-4 col-md-4">
						<asp:RadioButtonList ID="rblRelocate" runat="server" CssClass="dataInputHorizontalRadio" RepeatLayout="Flow" RepeatDirection="Horizontal">
							<asp:ListItem Text="Yes" Value="yes" />
							<asp:ListItem Text="No" Value="no" />
						</asp:RadioButtonList>
                       
					  <br />
						<asp:RequiredFieldValidator ID="RelocateValidator" runat="server" ControlToValidate="rblRelocate" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Preferences" />
					</div>
					
				</div>
			</asp:PlaceHolder>
			<div class="dataInputRow">
				<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" ID="WorkWeekLabel" LocalisationKey="WorkWeek.Label" DefaultText="Work week" AssociatedControlID="ddlWorkWeek" CssClass="dataInputLabel requiredData"/></div>
				<div class="dataInputField">
					<asp:DropDownList ID="ddlWorkWeek" runat="server" Width="224" ClientIDMode="Static">
						<asp:ListItem Text="- select work week -" />
					</asp:DropDownList>
					<br />
					<asp:CustomValidator ID="WorkWeekRequired" runat="server" Display="Dynamic" CssClass="error" ValidationGroup="Preferences" ClientValidationFunction="validateWorkingWeek"	ValidateEmptyText="true" />
				</div>
			</div>
			<div class="dataInputRow">
				<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" DefaultText="Duration" LocalisationKey="Duration.Label" ID="DurationLabel" CssClass="dataInputLabel requiredData" AssociatedControlID="ddlDuration"/></div>
				<div class="dataInputField">
					<asp:DropDownList ID="ddlDuration" runat="server" Width="224">
						<asp:ListItem Text="- select duration -" />
					</asp:DropDownList>
					<br />
					<asp:RequiredFieldValidator ID="DurationRequired" runat="server" ControlToValidate="ddlDuration" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Preferences" />
				</div>
			</div>
			<asp:Panel ID="WorkforcePreferencesPlaceHolder3" runat="server" CssClass="dataInputRow">
				<div class="col-sm-4 col-md-4"><focus:LocalisedLabel runat="server" ID="ShiftAvailLabel" AssociatedControlID="cbAny" CssClass="dataInputLabel requiredData" LocalisationKey="ShiftAvail.Label" DefaultText="Shift availability"/></div>
				<div class="dataInputField">
					<asp:CheckBox ID="cbAny" runat="server" ClientIDMode="Static" onclick="AnyClicked()" />
					<asp:CheckBoxList ID="cblShift" runat="server" RepeatColumns="2" ClientIDMode="Static" onclick="CheckforAny()" CssClass="dataInputHorizontalCheckBoxList"></asp:CheckBoxList>
					<asp:CustomValidator ID="ShiftValidator" runat="server" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Preferences" ClientValidationFunction="validateShift"	ValidateEmptyText="true" />
				</div>
			</asp:Panel>
		</div>

		<asp:PlaceHolder id="EducationPreferencesPlaceHolder2" runat="server" Visible="false">
      <asp:UpdatePanel ID="SkillUpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
		      <div id="EducationPreferencesSkillsDiv">
			      <div class="resumeSectionSubHeading">
				      <focus:LocalisedLabel runat="server" ID="SkillsSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="Skills.Label" DefaultText="Skills"/>
			      </div>
			      <div class="resumeProfile resumeProfileSkills">
				      <p class="instructionalText">
				        <focus:LocalisedLabel runat="server" ID="NoSkillsLabel" Visible="False" RenderOuterSpan="False" LocalisationKey="NoSkills.Label" DefaultText="We've been unable to identify any skills from your resume."/>
                
				        <focus:LocalisedLabel runat="server" ID="SkillsInstructionsLabel" RenderOuterSpan="False" LocalisationKey="SkillsInstructions.Label" DefaultText="We've identified certain skills from your resume."/>
                <focus:LocalisedLabel runat="server" ID="OtherSkillsInstructionsLabel1" RenderOuterSpan="False" LocalisationKey="OtherSkillsInstructions.Label" DefaultText="Please add other skills you would like #BUSINESSES#:LOWER to know you have."/>
				      </p>
						  <div class="dataInputRow" id="AddSkillsPanel" runat="server">
							  <span class="dataInputGroupedItemHorizontal">
							    <span class="dataInputField">
								    <span class="inFieldLabel"><focus:LocalisedLabel runat="server" ID="SkillLabel" AssociatedControlID="SkillTextBox" LocalisationKey="Skill.Label" DefaultText="Skill" Width="250"/></span>
								    <asp:TextBox ID="SkillTextBox" runat="server" Width="250" ClientIDMode="Static" />
								    <ajaxtoolkit:AutoCompleteExtender ID="aceSkill" runat="server" CompletionSetCount="20"
									    CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
									    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetSkills"
									    ServicePath="~/Services/AjaxService.svc" TargetControlID="SkillTextBox" CompletionInterval="1000"
									    UseContextKey="false" EnableCaching="true" MinimumPrefixLength="3" OnClientShown="fixAutoCompleteExtender">
								    </ajaxtoolkit:AutoCompleteExtender>
							    </span>
                </span>
							  <span class="dataInputGroupedItemHorizontal">
							    <asp:LinkButton ID="SkillAddButton" runat="server" ClientIDMode="Static" Text="+ Add" OnClick="SkillAddButton_Click" />
							  </span>
						  </div>
						  <div class="dataInputRow">
							  <asp:Panel ID="SkillPanel" runat="server" CssClass="resumeProfileSkillList">
								  <uc:UpdateableList ID="SkillUpdateableList" runat="server" DisplayBullet="false" AddConfirmationModal="false" />
							  </asp:Panel>
						  </div>
			      </div>
          </div>
          <div id="EducationPreferencesInternshipSkillsDiv" clientidmode="Static">
            <div class="resumeSectionSubHeading">
              <focus:LocalisedLabel runat="server" ID="InternshipSkillsInstructionsLabel" RenderOuterSpan="False" LocalisationKey="OtherSkillsIHave.Label" DefaultText="Other skills I have"/>
              <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%=HtmlLocalise("OtherSkillsHelpIcon.Title", "Check to add skills learned through non-work activities (eg. class project).") %>"/>
            </div>
            <p>
              <asp:CheckBox runat="server" ID="SkillsAlreadyCapturedBox" ClientIDMode="Static"/>
              <br />
 					    <asp:CustomValidator ID="SkillsAlreadyCapturedValidator" runat="server" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Preferences" ClientValidationFunction="validateSkillsAlreadyCapturedBox"	ValidateEmptyText="true" />
            </p>
            <div id="SkillsAccordionDiv" runat="server" clientidmode="Static">
              <uc:SkillsAccordion runat="Server" ID="SkillsAccordion" WrapperCssClass="skillsAccordionWrapper" AccordionCssClass="skillsAccordion" SummaryCssClass="skillsAccordionSummary"/>
            </div>
          </div>
			  </ContentTemplate>
			  <Triggers>
				  <asp:AsyncPostBackTrigger ControlID="SkillAddButton" EventName="Click" />
			  </Triggers>
      </asp:UpdatePanel>
		</asp:PlaceHolder>

		<div class="resumeSectionSubHeading">
			<focus:LocalisedLabel runat="server" ID="LocationPreferencesSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="LocationPreferencesSubHeading.Label" DefaultText="Location preferences"/>
			&nbsp;<focus:LocalisedLabel runat="server" ID="LocationPreferencesSubHeadingRequiredLabel" CssClass="requiredDataLegend" LocalisationKey="LocationPreferencesSubHeadingRequired.Label" DefaultText="required fields"/>
		</div>
		<div class="resumePreferences resumePreferencesLocation">
			<div class="dataInputRow">
				<div class="dataInputGroupedItemHorizontal col-sm-4 col-md-4 col-lg-4">
					<asp:RadioButton ID="rbSearchArea" CssClass="dataInputSelectOptionRow" Text="Search within this area" runat="server" onclick="Hide_Radius_StateMSA(this)" GroupName="Location" ClientIDMode="Static" />
				</div>
				<div class="col-sm-8 col-md-8 col-lg-8">
				<div class="col-sm-5 col-md-4 col-lg-3" style="margin-bottom:5px"><span class="dataInputGroupedItemHorizontal">	<asp:DropDownList ID="ddlRadius" runat="server" ClientIDMode="Static" Width="150px" onchange="CheckRadius();" CssClass="RadiusClass"></asp:DropDownList>
				</span></div>
				<div class="dataInputGroupedItemHorizontal col-sm-7 col-md-8 col-lg-9" style="margin-bottom:5px">
					<span class="dataInputGroupedItemHorizontal">
					<focus:LocalisedLabel runat="server" ID="OfZipLabel" AssociatedControlID="txtZip" CssClass="requiredData" LocalisationKey="OfZip.Label" DefaultText="of ZIP code"/>
				</span>
                <asp:TextBox ID="txtZip" runat="server" Width="128px" ClientIDMode="Static" MaxLength="5" onblur="CheckRadius();" />
					<ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="txtZip" FilterType="Numbers" />
					<asp:CustomValidator ID="RadiusValidator" runat="server" SetFocusOnError="true" Display="Dynamic" ControlToValidate="txtZip" CssClass="error" ValidationGroup="Preferences" ClientValidationFunction="ValidateRadius" ValidateEmptyText="true" />
				</div></div>
			</div>
            <asp:Panel ID="WorkforcePreferencesPlaceHolder4" runat="server" CssClass="dataInputRow">
				<span class="dataInputField">
					<asp:CheckBox ID="cbSearchInMyState" runat="server" CssClass="checkBoxList" ClientIDMode="Static" />
					<img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%=HtmlLocalise("SearchInMyStateToolTip.Label", "If your search area is near a state line, the radius criteria may deliver results for jobs in your neighboring state(s).  Check this box to limit search results only to jobs in your state. <br/> Note:  If you opt to limit your search to in-state jobs, make sure that the ZIP code and radius you select includes areas within the state.  Otherwise, you will receive no matches.") %>"/>
				</span>
			</asp:Panel>
			<div class="dataInputRow">
				<div class="dataInputGroupedItemHorizontal col-sm-4 col-md-4">
					<asp:RadioButton ID="rbSearchMSA" CssClass="dataInputSelectOptionRow" Text="Search this state/city" runat="server" GroupName="Location" onclick="Hide_Radius_StateMSA(this)" ClientIDMode="Static" />
				</div>
                <div class="col-sm-8 col-md-8">
				<div class="dataInputGroupedItemHorizontal col-md-6">
					<asp:DropDownList ID="ddlState" runat="server" Width="280px" ClientIDMode="Static" onchange="CheckMSA();" CssClass="StateClass">
						<asp:ListItem Value="">- select a state -</asp:ListItem>
					</asp:DropDownList><br />
                    <asp:CustomValidator ID="StateValidator" runat="server" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Preferences" ClientValidationFunction="ValidateState" ValidateEmptyText="true" />	
				</div>
				<div class="dataInputGroupedItemHorizontal">
					<focus:AjaxDropDownList ID="ddlMSA" CssClass="MSAClass" runat="server" Width="280px" onchange="CheckMSA();" ClientIDMode="Static" TabIndex="10" />
					<ajaxtoolkit:CascadingDropDown ID="ccdMSA" runat="server" Category="MSA" LoadingText="[Loading City...]" ParentControlID="ddlState" ServiceMethod="GetMSA" ServicePath="~/Services/AjaxService.svc" BehaviorID="ccdMSABehaviorID" TargetControlID="ddlMSA"></ajaxtoolkit:CascadingDropDown>
          
				</div></div>
			</div>
			
<%--			<asp:Panel ID="HomeBasedJobPostingsPanel" runat="server" CssClass="dataInputRow">
				<span class="dataInputField">
          <asp:RadioButton ID="HomeBasedJobPostingsRadioButton" ClientIDMode="Static" Text="Include home-based job postings" runat="server" GroupName="Location" onclick="Hide_Radius_StateMSA(this)" />
          <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%=HtmlLocalise("HomeBasedJobPostingsToolTip.Label", "Selecting this option means only jobs posted directly from this website that have been highlighted as suitable for home-based workers will be displayed in your search results. Spidered jobs from other #BUSINESSES#:LOWER will be excluded.") %>"/>
				</span>
			</asp:Panel>--%>
      <div>
        <asp:CustomValidator ID="LocationPreferenceValidator" runat="server" ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Preferences" ClientValidationFunction="ValidateLocationPreference" ValidateEmptyText="true" />
      </div>
		</div>
	</div>
	<asp:Panel ID="WorkforcePreferencesPlaceHolder5" runat="server" CssClass="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton1" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2 greyedOut" OnClientClick="return validateAllPreferences();" OnClick="SaveMoveToNextStepButton_Click" disabled="disabled" ClientIDMode="Static" />
	</asp:Panel>
</div>

<asp:PlaceHolder runat="server" ID="BridgesToOppModalPlaceHolder">
	<asp:HiddenField ID="BridgesToOppDummyTarget" runat="server" />
	<act:ModalPopupExtender ID="BridgesToOppModalPopup" runat="server" TargetControlID="BridgesToOppDummyTarget"
		PopupControlID="BridgesToOppModalPanel" BackgroundCssClass="modalBackground" BehaviorID="BridgesToOppModal"
		CancelControlID="BridgesToOppOKButton" RepositionMode="RepositionOnWindowResizeAndScroll" />
	<asp:Panel ID="BridgesToOppModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none; width: 400px">
		<div class="lightbox FocusCareer">
			<div class="modalHeader">
				<focus:LocalisedLabel runat="server" ID="BridgesToOppInfoHeader" CssClass="modalTitle modalTitleLarge" LocalisationKey="BridgesToOppInfoHeader.Label" DefaultText="More Information"/>
			</div>
			<div class="modalMessage">
				<focus:LocalisedLabel runat="server" ID="BridgesToOppInfoLabel" LocalisationKey="BridgesToOppInfoHeader.Label" DefaultText="Bridges to Opportunities is a workforce development training program to ensure women and minorities are an integral part of the Bridges Project and help create a more diverse workforce for regional construction projects."/>
			</div>
			<div class="modalButtons">
				<asp:Button ID="BridgesToOppOKButton" runat="server" class="buttonLevel2" Text="OK" />
			</div>
		</div>
	</asp:Panel>
</asp:PlaceHolder>

<script language="javascript" type="text/javascript">
  Sys.Application.add_load(Preferences_PageLoad);
</script>
