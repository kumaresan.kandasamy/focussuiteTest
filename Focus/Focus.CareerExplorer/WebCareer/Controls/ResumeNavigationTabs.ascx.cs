﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Framework.Core;

using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class ResumeNavigationTabs : UserControlBase
	{
		protected String SelectedTab;

		public String SelectedNavigationTab
		{
			get
			{
				return SelectedTab;
			}
			set
			{
				SelectedTab = value;
			}
		}

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (SelectedTab.IsNotNull())
			{
				WorkHistoryLink.Attributes.Add("class", "first");
				ReviewLink.Attributes.Add("class", "last");

				ProfileLink.Visible = !App.Settings.HideResumeProfile;

				switch (SelectedTab)
				{
					case "WORKHISTORY":
						WorkHistoryLink.Attributes.Add("class", "first selected");
						break;

					case "CONTACT":
						WorkHistoryLink.Attributes.Add("class", "first selectedPrevious");
						ContactLink.Attributes.Add("class", "selected");
						break;

					case "EDUCATION":
						ContactLink.Attributes.Add("class", "selectedPrevious");
						EducationLink.Attributes.Add("class", "selected");
						break;

					case "SUMMARY":
						EducationLink.Attributes.Add("class", "selectedPrevious");
						SummaryLink.Attributes.Add("class", "selected");
						break;

					case "OPTIONS":
						SummaryLink.Attributes.Add("class", "selectedPrevious");
						AddInsLink.Attributes.Add("class", "selected");
						break;

					case "PROFILE":
						AddInsLink.Attributes.Add("class", "selectedPrevious");
						ProfileLink.Attributes.Add("class", "selected");
						break;

					case "PREFERENCES":
						if (App.Settings.HideResumeProfile && App.Settings.Theme == FocusThemes.Workforce) AddInsLink.Attributes.Add("class", "selectedPrevious");
						else ProfileLink.Attributes.Add("class", "selectedPrevious");
						PreferencesLink.Attributes.Add("class", "selected");
						break;

					case "REVIEW":
						PreferencesLink.Attributes.Add("class", "selectedPrevious");
						ReviewLink.Attributes.Add("class", "last selected");
						break;
				}
			}
			GuidedPath();
		}

    /// <summary>
    /// Guideds the path.
    /// </summary>
		private void GuidedPath()
		{
			var resume = App.GetSessionValue<ResumeModel>("Career:Resume");
			var resumeStatus = resume.IsNotNull() ? (int)(resume.ResumeMetaInfo.CompletionStatus) : 0;
			var tempStatus = (ResumeCompletionStatuses)((resumeStatus << 1) + 1);
			linkWorkHistory.Enabled = ((tempStatus & ResumeCompletionStatuses.WorkHistory) == ResumeCompletionStatuses.WorkHistory);

			LnkContact.LinkDisabled(((tempStatus & ResumeCompletionStatuses.Contact) != ResumeCompletionStatuses.Contact));
			LnkEducation.LinkDisabled(((tempStatus & ResumeCompletionStatuses.Education) != ResumeCompletionStatuses.Education));
			LnkSummary.LinkDisabled(((tempStatus & ResumeCompletionStatuses.Summary) != ResumeCompletionStatuses.Summary));
			LnkAddIns.LinkDisabled(((tempStatus & ResumeCompletionStatuses.Options) != ResumeCompletionStatuses.Options));
			LnkProfile.LinkDisabled(((tempStatus & ResumeCompletionStatuses.Profile) != ResumeCompletionStatuses.Profile));
			LnkPreferences.LinkDisabled(((tempStatus & ResumeCompletionStatuses.Preferences) != ResumeCompletionStatuses.Preferences));
			LnkReview.LinkDisabled((resumeStatus & (int)ResumeCompletionStatuses.Preferences) != (int)ResumeCompletionStatuses.Preferences);
		}
    
    /// <summary>
    /// Handles the Click event of the linkWorkHistory control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void linkWorkHistory_Click(object sender, EventArgs e)
		{
			App.RemoveSessionValue("Career:NewJobDetails");
			Response.RedirectToRoute("ResumeWizardTab", new { Tab = "workhistory" });
		}
	}
}
