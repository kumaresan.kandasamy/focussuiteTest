﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class ResumePreview : UserControlBase
	{
    private bool _isOriginal = false;

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
        BindLookups();
    }

    /// <summary>
    /// Binds the lookups.
    /// </summary>
    private void BindLookups()
    {
      ddlResumeFormat.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats), null, null);
      //ddlResumeFormat.SelectedIndex = 1;
    }

    #region Bind Resume Preview section

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
      ResumeDetail.Attributes["src"] = "";

      try
      {
        var lookup =
          ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats).FirstOrDefault(x => x.Id == ddlResumeFormat.SelectedValueToLong());

        var xslPath = (lookup.IsNotNull()) ? App.Settings.GetCustomXslForResume(lookup.ExternalId) : String.Empty;
        
        if (xslPath.IsNotNullOrEmpty())
        {
          try
          {
            xslPath = HttpContext.Current.Server.MapPath(xslPath);
          }
          catch { }
        }

        if (!String.IsNullOrEmpty(xslPath))
        {
          var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
          var IsHTMLFormattedResume = false;
          
          if (currentResume.IsNotNull() && currentResume.ResumeMetaInfo.IsNotNull())
          {
            if (currentResume.ResumeMetaInfo.ResumeCreationMethod == ResumeCreationMethod.Upload)
            {
              if (!ddlResumeFormat.Items.Contains(new ListItem("Original", "0")))
                ddlResumeFormat.Items.Insert(0, new ListItem("Original", "0"));

              if (currentResume.Special.IsNotNull() && currentResume.Special.PreservedFormatId.IsNotNull())
              {
                ddlResumeFormat.SelectedValue = currentResume.Special.PreservedFormatId.ToString();

                if (currentResume.Special.PreservedFormatId == 0)
                  IsHTMLFormattedResume = true;
              }
            }

            if (currentResume.IsNotNull() && !IsHTMLFormattedResume)
            {
              SetOriginalResume(false);
              var xmlResume = Utilities.FetchXMLResume(currentResume);
              lblFormattedOutput.Text = Utilities.ApplyTemplate(xmlResume, xslPath, App, ddlResumeOrder.SelectedValue, true);
            }

            if (IsHTMLFormattedResume)
              DisplayOriginalFormat();
          }
          else
          {
            ddlResumeFormat.Enabled = ddlResumeOrder.Enabled = ResumeDetail.Visible = false;
          }
        }
        
        //if (!String.IsNullOrEmpty(xslPath))
        //{
        //  ResumePreviewModal.Show();
        //  string xResume = Utilities.FetchXMLResume();
        //  lblFormattedOutput.Text = Utilities.ApplyTemplate(xResume, xslPath, ddlResumeOrder.SelectedValue);
        //}
        ResumePreviewModal.Show();
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }
    
    private void DisplayOriginalFormat()
    {
      SetOriginalResume(true);
      var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
      if (currentResume.IsNotNull() && currentResume.ResumeMetaInfo.IsNotNull())
      {
        var resumeId = currentResume.ResumeMetaInfo.ResumeId.GetValueOrDefault(0);

        ResumeDetail.Attributes["src"] = GetRouteUrl("ResumeDisplayWithType", new
        {
          resumeid = resumeId,
          mimeType = @"html"
        });
      }
    }

    /// <summary>
    /// Sets the original resume.
    /// </summary>
    /// <param name="status">if set to <c>true</c> [status].</param>
    private void SetOriginalResume(bool status)
    {
      _isOriginal = status;
      //lnkHideResumeInfo.Visible = !status;
      ddlResumeOrder.Enabled = !status;
      panFormattedOutput.Visible = !status;
      lblFormattedOutput.Visible = !status;
      ResumeDetail.Visible = status;
    }

    #endregion

    /// <summary>
    /// Handles the SelectedIndexChanged event of the ddlResumeFormat control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ddlResumeFormat_SelectedIndexChanged(object sender, EventArgs e)
    {
      ResumeDetail.Attributes["src"] = "";

      try
      {
        var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats).FirstOrDefault(x => x.Id == ddlResumeFormat.SelectedValueToLong());
        var xslPath = (lookup.IsNotNull()) ? App.Settings.GetCustomXslForResume(lookup.ExternalId) : String.Empty;

        if (xslPath.IsNotNullOrEmpty())
        {
          try
          {
            xslPath = HttpContext.Current.Server.MapPath(xslPath);
          }
          catch { }
        }

        if (ddlResumeFormat.SelectedValue == "0")
        {
          DisplayOriginalFormat();
        }
        else
        {
          SetOriginalResume(false);
          string xResume = Utilities.FetchXMLResume();
          lblFormattedOutput.Text = Utilities.ApplyTemplate(xResume, xslPath, App, ddlResumeOrder.SelectedValue, true);
        }

        ResumePreviewModal.Show();
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }
  }
}

