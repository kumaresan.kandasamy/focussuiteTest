﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.CareerExplorer.WebCareer.Controls.EducationControls;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class Review : UserControlBase
	{
		private bool IsOriginal
		{
			get { return GetViewStateValue<bool>("ResumeReview:IsOriginal", false); }
			set { SetViewStateValue("ResumeReview:IsOriginal", value); }
		}

    //public event EventHandler SaveResumeTitle;


    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      ApplyTheme();
			ApplyBrand();
    }

		/// <summary>
		/// Applies client branding.
		/// </summary>
		private void ApplyBrand()
		{
			ibEditResume.ImageUrl = UrlBuilder.CreateResumeSmall();
			ibDownloadResume.ImageUrl = UrlBuilder.DownloadIcon();
			ibEditResume.ImageUrl = UrlBuilder.EmailIcon();
			ibEmailResume.ImageUrl = UrlBuilder.EmailIcon();
		}

    /// <summary>
    /// Applies theme specific changes
    /// </summary>
    private void ApplyTheme()
    {
      if (App.Settings.Theme == FocusThemes.Education)
        HideJobsSelectMultipleLabel.DefaultText = "To select multiple items, press and hold CTRL while highlighting the items you want. Similarly, to deselect already highlighted items, press and hold CTRL while you unhighlight them.";
    }

    #region Bind Review section

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
      ResumeDetail.Attributes["src"] = "";

      try
      {
        if (!IsPostBack)
        {
					ddlReviewResumeFormat.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats), null, null);

          BindHideOptions();
          LoadHideOptions();

					var lookupItem = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats).FirstOrDefault(x => x.Id == ddlReviewResumeFormat.SelectedValueToLong());
					var xslPath = App.Settings.GetCustomXslForResume(lookupItem.ExternalId);

          if (xslPath.IsNotNullOrEmpty())
          {
            try
            {
              xslPath = HttpContext.Current.Server.MapPath(xslPath);
            }
            catch
            {
            }

            var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
            var IsHTMLFormattedResume = false;

						if (currentResume.IsNotNull() && currentResume.ResumeMetaInfo.IsNotNull() && currentResume.ResumeMetaInfo.ResumeCreationMethod == ResumeCreationMethod.Upload)
            {
              if (!ddlReviewResumeFormat.Items.Contains(new ListItem("Original", "0")))
                ddlReviewResumeFormat.Items.Insert(0, new ListItem("Original", "0"));

	            if (currentResume.Special.IsNotNull() && currentResume.Special.PreservedFormatId.IsNotNull())
              {
              	ddlReviewResumeFormat.SelectedValue = currentResume.Special.PreservedFormatId.ToString();

                if (currentResume.Special.PreservedFormatId == 0)
                  IsHTMLFormattedResume = true;
              }
            }

            if (currentResume.IsNotNull() && !IsHTMLFormattedResume)
            {
              SetOriginalResume(false);
              var xmlResume = Utilities.FetchXMLResume(currentResume);
              htmlJobDescription.Content = lblReviewFormattedOutput.Text = Utilities.ApplyTemplate(xmlResume, xslPath, App, ddlReviewResumeOrder.SelectedValue, true);
            }

            if (IsHTMLFormattedResume)
              DisplayOriginalFormat();
          }

          //TODO: Martha (UI Claimant)
          //if (App.Settings.UIClaimantEnabled && (App.GetSessionValue<string>("Career:UIClaimantStatus").IsNotNullOrEmpty() && App.GetSessionValue<string>("Career:UIClaimantStatus") != "0"))
          //{
          //  bool displayUIClaimantButton = false;
          //  if (App.GetSessionValue<string>("Career:UIClaimantStatus") == "1")
          //    displayUIClaimantButton = true;
          //  else if (App.GetSessionValue<string>("Career:UIClaimantStatus") == "2")
          //  {
          //    if (App.GetSessionValue<bool>("Career:UIClaimMaxDaysExceeded").IsNotNull() && !App.GetSessionValue<bool>("Career:UIClaimMaxDaysExceeded"))
          //      displayUIClaimantButton = true;
          //  }

          //  if (displayUIClaimantButton)
          //    btnSaveResumeFormatTop.Text = btnSaveResumeFormatBottom.Text = CodeLocalise("UIClaimantButtonInReview", @"View jobs & complete my claim " + (char)187);
          //}
        }

      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    private void DisplayOriginalFormat()
    {
      SetOriginalResume(true);
      var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
      if (currentResume.IsNotNull() && currentResume.ResumeMetaInfo.IsNotNull())
      {
        var resumeId = currentResume.ResumeMetaInfo.ResumeId.GetValueOrDefault(0);

        string fileName, mimeType, htmlResume;

        ServiceClientLocator.ResumeClient(App).GetOriginalUploadedResume(resumeId, out fileName, out mimeType, out htmlResume);

        if (htmlResume.ToUpper().StartsWith("BGTEC022:"))
        {
            var title = CodeLocalise("ReviewInvalid.Title", "Invalid Format");
            var body = CodeLocalise("ReviewInvalid.Body", "Your uploaded file format is a PDF and therefore cannot be displayed with its original formatting.  Please choose a different format or upload a DOC, DOCX, or RTF file.");

            ConfirmationModal.Show(title, body, closeText: "OK", height: 80, width: 250);
        }

        ResumeDetail.Attributes["src"] = GetRouteUrl("ResumeDisplayWithType", new
        {
          resumeid = currentResume.ResumeMetaInfo.ResumeId,
          mimeType = @"html"
        });
      }
    }
    /// <summary>
    /// Sets the original resume.
    /// </summary>
    /// <param name="status">if set to <c>true</c> [status].</param>
    private void SetOriginalResume(bool status)
    {
			IsOriginal = status;
      HideResumeInfoModal.Enabled = !status;
      lnkHideResumeInfo.Enabled = !status;
      if (status)
        lnkHideResumeInfo.Style["color"] = "Gray";
      else
        lnkHideResumeInfo.Style["color"] = "#264C69";
      ddlReviewResumeOrder.Enabled = !status;
      panReviewFormattedOutput.Visible = !status;
      lblReviewFormattedOutput.Visible = !status;
      ResumeDetail.Visible = status;
    }

    #endregion

    /// <summary>
    /// Handles the Click event of the ibEditResume control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ibEditResume_Click(object sender, EventArgs e)
    {
      htmlJobDescription.Visible = true;
      lblReviewFormattedOutput.Visible = false;
    }

    /// <summary>
    /// Loads the hide options.
    /// </summary>
    private void LoadHideOptions()
    {
      if (App.User.IsAuthenticated)
      {
        var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");

        if (currentResume.Special == null)
        {
          currentResume.Special = new ResumeSpecialInfo
          {
            HideInfo = new HideResumeInfo
            {
              HiddenJobs = new List<string>()
            }
          };
        }
        else if (currentResume.Special.HideInfo == null)
        {
          currentResume.Special.HideInfo = new HideResumeInfo
          {
            HiddenJobs = new List<string>()
          };
        }

        if (currentResume.IsNotNull() && currentResume.ResumeContent.IsNotNull())
        {
          var resumeContent = currentResume.ResumeContent;
          var addInsSection = resumeContent.AdditionalResumeSections.IsNotNull() ? resumeContent.AdditionalResumeSections : null;
          var userPhone = resumeContent.SeekerContactDetails.IsNotNull() && resumeContent.SeekerContactDetails.PhoneNumber.IsNotNull() && resumeContent.SeekerContactDetails.PhoneNumber.Count > 0
                            ? resumeContent.SeekerContactDetails.PhoneNumber
                            : null;

          for (int index = 0; index < cblHideSections.Items.Count; index++)
          {
            switch (cblHideSections.Items[index].Value)
            {
              case "ALL_DATES":
                cblHideSections.Items.FindByValue("ALL_DATES").Selected = currentResume.Special.HideInfo.HideDateRange;
                break;

              case "EDUCATION_DATES":
                cblHideSections.Items.FindByValue("EDUCATION_DATES").Selected = currentResume.Special.HideInfo.HideEducationDates;
                break;

              case "MILITARY_SERVICE_DATES":
                cblHideSections.Items.FindByValue("MILITARY_SERVICE_DATES").Selected = currentResume.Special.HideInfo.HideMilitaryServiceDates;
                break;

              case "WORK_HISTORY_DATES":
                cblHideSections.Items.FindByValue("WORK_HISTORY_DATES").Selected = currentResume.Special.HideInfo.HideWorkDates;
                break;

              case "CONTACT_INFO":
                cblHideSections.Items.FindByValue("CONTACT_INFO").Selected = currentResume.Special.HideInfo.HideContact;
                break;

              case "NAME":
                cblHideSections.Items.FindByValue("NAME").Selected = currentResume.Special.HideInfo.HideName;
                break;

              case "EMAIL":
                cblHideSections.Items.FindByValue("EMAIL").Selected = currentResume.Special.HideInfo.HideEmail;
                break;

              case "PHONE_HOME":
                if (userPhone.IsNotNull())
                {
                  if (userPhone.Exists(x => x.PhoneType == PhoneType.Home))
                    cblHideSections.Items.FindByValue("PHONE_HOME").Selected = currentResume.Special.HideInfo.HideHomePhoneNumber;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_HOME")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_HOME")));
                  index--;
                }
                break;

              case "PHONE_WORK":
                if (userPhone.IsNotNull())
                {
                  if (userPhone.Exists(x => x.PhoneType == PhoneType.Work))
                    cblHideSections.Items.FindByValue("PHONE_WORK").Selected = currentResume.Special.HideInfo.HideWorkPhoneNumber;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_WORK")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_WORK")));
                  index--;
                }
                break;

              case "PHONE_CELL":
                if (userPhone.IsNotNull())
                {
                  if (userPhone.Exists(x => x.PhoneType == PhoneType.Cell))
                    cblHideSections.Items.FindByValue("PHONE_CELL").Selected = currentResume.Special.HideInfo.HideCellPhoneNumber;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_CELL")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_CELL")));
                  index--;
                }
                break;

              case "PHONE_FAX":
                if (userPhone.IsNotNull())
                {
                  if (userPhone.Exists(x => x.PhoneType == PhoneType.Fax))
                    cblHideSections.Items.FindByValue("PHONE_FAX").Selected = currentResume.Special.HideInfo.HideFaxPhoneNumber;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_FAX")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_FAX")));
                  index--;
                }
                break;

              case "PHONE_NONUS":
                if (userPhone.IsNotNull())
                {
                  if (userPhone.Exists(x => x.PhoneType == PhoneType.NonUS))
                    cblHideSections.Items.FindByValue("PHONE_NONUS").Selected = currentResume.Special.HideInfo.HideNonUSPhoneNumber;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_NONUS")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PHONE_NONUS")));
                  index--;
                }
                break;

              case "AFFILIATIONS":
                var affiliationPresent = false;
                if (resumeContent.Professional.IsNotNull() && resumeContent.Professional.Affiliations.IsNotNull() && resumeContent.Professional.Affiliations.Count > 0)
                {
                  affiliationPresent = true;
                  cblHideSections.Items.FindByValue("AFFILIATIONS").Selected = currentResume.Special.HideInfo.HideAffiliations;
                }

                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.Affiliations))
                    cblHideSections.Items.FindByValue("AFFILIATIONS").Selected = currentResume.Special.HideInfo.HideAffiliations;
                  else if (!affiliationPresent)
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("AFFILIATIONS")));
                    index--;
                  }
                }
                else if (!affiliationPresent)
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("AFFILIATIONS")));
                  index--;
                }
                break;

              case "PROFESSIONAL_LICENSES":
                cblHideSections.Items.FindByValue("PROFESSIONAL_LICENSES").Selected = currentResume.Special.HideInfo.HideCertificationsAndProfessionalLicenses;
                break;

              case "DRIVER_LICENSE":
                if (currentResume.Special.HideInfo.UnHideDriverLicense)
                  cblHideSections.Items.FindByValue("DRIVER_LICENSE").Selected = (currentResume.Special.HideInfo.HideDriverLicense && currentResume.Special.HideInfo.UnHideDriverLicense);
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("DRIVER_LICENSE")));
                  index--;
                }
                break;

              case "HONORS":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.Honors))
                    cblHideSections.Items.FindByValue("HONORS").Selected = currentResume.Special.HideInfo.HideHonors;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("HONORS")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("HONORS")));
                  index--;
                }
                break;

              case "INTERESTS":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.Interests))
                    cblHideSections.Items.FindByValue("INTERESTS").Selected = currentResume.Special.HideInfo.HideInterests;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("INTERESTS")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("INTERESTS")));
                  index--;
                }
                break;

              case "INTERNSHIPS":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.Internships))
                    cblHideSections.Items.FindByValue("INTERNSHIPS").Selected = currentResume.Special.HideInfo.HideInternships;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("INTERNSHIPS")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("INTERNSHIPS")));
                  index--;
                }
                break;

              case "LANGUAGES":
                cblHideSections.Items.FindByValue("LANGUAGES").Selected = currentResume.Special.HideInfo.HideLanguages;
                break;

              case "MILITARY_SERVICE":
                cblHideSections.Items.FindByValue("MILITARY_SERVICE").Selected = currentResume.Special.HideInfo.HideVeteran;
                break;

              case "OBJECTIVE":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.Objective))
                    cblHideSections.Items.FindByValue("OBJECTIVE").Selected = currentResume.Special.HideInfo.HideObjective;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("OBJECTIVE")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("OBJECTIVE")));
                  index--;
                }
                break;

              case "PERSONAL_INFO":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.Personals))
                    cblHideSections.Items.FindByValue("PERSONAL_INFO").Selected = currentResume.Special.HideInfo.HidePersonalInformation;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PERSONAL_INFO")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PERSONAL_INFO")));
                  index--;
                }
                break;

              case "PROFESSIONAL_DEVELOPMENT":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.ProfessionalDevelopment))
                    cblHideSections.Items.FindByValue("PROFESSIONAL_DEVELOPMENT").Selected = currentResume.Special.HideInfo.HideProfessionalDevelopment;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PROFESSIONAL_DEVELOPMENT")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PROFESSIONAL_DEVELOPMENT")));
                  index--;
                }
                break;

              case "PUBLICATIONS":
                if (resumeContent.Professional.IsNotNull() && resumeContent.Professional.Publications.IsNotNull() && resumeContent.Professional.Publications.Count > 0)
                  cblHideSections.Items.FindByValue("PUBLICATIONS").Selected = currentResume.Special.HideInfo.HidePublications;
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("PUBLICATIONS")));
                  index--;
                }
                break;

              case "REFERENCES":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.References))
                    cblHideSections.Items.FindByValue("REFERENCES").Selected = currentResume.Special.HideInfo.HideReferences;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("REFERENCES")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("REFERENCES")));
                  index--;
                }
                break;

              case "SKILLS":
                cblHideSections.Items.FindByValue("SKILLS").Selected = currentResume.Special.HideInfo.HideSkills;
                break;

              case "TECHNICAL_SKILLS":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.TechnicalSkills))
                    cblHideSections.Items.FindByValue("TECHNICAL_SKILLS").Selected = currentResume.Special.HideInfo.HideTechnicalSkills;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("TECHNICAL_SKILLS")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("TECHNICAL_SKILLS")));
                  index--;
                }
                break;

              case "VOLUNTEER_ACTIVITIES":
                if (addInsSection.IsNotNull())
                {
                  if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.VolunteerActivities))
                    cblHideSections.Items.FindByValue("VOLUNTEER_ACTIVITIES").Selected = currentResume.Special.HideInfo.HideVolunteerActivities;
                  else
                  {
                    cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("VOLUNTEER_ACTIVITIES")));
                    index--;
                  }
                }
                else
                {
                  cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("VOLUNTEER_ACTIVITIES")));
                  index--;
                }
                break;

							case "THESIS_MAJOR_PROJECTS":
								if (addInsSection.IsNotNull())
								{
									if (addInsSection.Exists(x => x.SectionType == ResumeSectionType.ThesisMajorProjects))
										cblHideSections.Items.FindByValue("THESIS_MAJOR_PROJECTS").Selected = currentResume.Special.HideInfo.HideThesisMajorProjects;
									else
									{
										cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("THESIS_MAJOR_PROJECTS")));
										index--;
									}
								}
								else
								{
									cblHideSections.Items.RemoveAt(cblHideSections.Items.IndexOf(cblHideSections.Items.FindByValue("THESIS_MAJOR_PROJECTS")));
									index--;
								}
								break;
            }
          }
        }

        //Set Saved Resume Format
        if (currentResume.Special.PreservedFormatId.IsNotNull())
          ddlReviewResumeFormat.SelectValue(currentResume.Special.PreservedFormatId.ToString());

        if (currentResume.Special.PreservedOrder.IsNotNullOrEmpty())
          ddlReviewResumeOrder.SelectedIndex = ddlReviewResumeOrder.Items.IndexOf(ddlReviewResumeOrder.Items.FindByText(currentResume.Special.PreservedOrder));

        lbHideJobs.Items.Clear();
        if (currentResume.ResumeContent.ExperienceInfo.IsNotNull() && currentResume.ResumeContent.ExperienceInfo.Jobs.IsNotNull())
        {
          var index = 0;
          foreach (var exp in currentResume.ResumeContent.ExperienceInfo.Jobs)
          {
            lbHideJobs.Items.Add(new ListItem((exp.Title.IsNotNullOrEmpty() ? exp.Title.Trim() : "N/A") + " - " + (exp.Employer.IsNotNullOrEmpty() ? exp.Employer.Trim() : "N/A"), exp.JobId.ToString()));
            if (currentResume.Special.HideInfo.IsNotNull() && currentResume.Special.HideInfo.HiddenJobs.IsNotNull())
              lbHideJobs.Items[index].Selected = currentResume.Special.HideInfo.HiddenJobs.Contains(exp.JobId.ToString());
            index++;
          }
        }
      }
    }

    /// <summary>
    /// Handles the Click event of the btnHideResumeInfo control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnHideResumeInfo_Click(object sender, EventArgs e)
    {
      try
      {
        if (cblHideSections.Items.FindByValue("WORK_HISTORY_DATES").Selected || cblHideSections.Items.FindByValue("ALL_DATES").Selected)
          ModalPopup.Show();

        SaveHideOptions();
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Saves the hide options.
    /// </summary>
    private void SaveHideOptions()
    {
      try
      {
        HideOptionsInResume();

				var lookupItem = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats).FirstOrDefault(x => x.Id == ddlReviewResumeFormat.SelectedValueToLong());
				var xslPath = App.Settings.GetCustomXslForResume(lookupItem.ExternalId);

        if (xslPath.IsNotNullOrEmpty())
        {
          try
          {
            xslPath = HttpContext.Current.Server.MapPath(xslPath);
          }
          catch
          {
          }

          string xResume = Utilities.FetchXMLResume();
          htmlJobDescription.Content = lblReviewFormattedOutput.Text = Utilities.ApplyTemplate(xResume, xslPath, App, ddlReviewResumeOrder.SelectedValue, true);
        }

        HideResumeInfoModal.Hide();
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the Click event of the OkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void OkButton_Click(object sender, EventArgs e)
    {
      try
      {
        //FVN-5538 - commenting the below line as per Gail's comment to  not de-select the hide options though  redirected to summary tab
        //cblHideSections.Items.FindByValue("WORK_HISTORY_DATES").Selected = cblHideSections.Items.FindByValue("ALL_DATES").Selected = false;
        SaveHideOptions();
        Response.RedirectToRoute("ResumeWizardTab", new { Tab = "summary" });
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Hides the options in resume.
    /// </summary>
    private void HideOptionsInResume()
    {
      try
      {
        var model = App.GetSessionValue<ResumeModel>("Career:Resume");
        var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
        var clonedResume = currentResume.CloneObject();

        foreach (ListItem option in cblHideSections.Items)
        {
          switch (option.Value)
          {
            case "ALL_DATES":
              currentResume.Special.HideInfo.HideDateRange = cblHideSections.Items.FindByValue("ALL_DATES").Selected;
              break;

            case "EDUCATION_DATES":
              currentResume.Special.HideInfo.HideEducationDates = cblHideSections.Items.FindByValue("EDUCATION_DATES").Selected;
              break;

            case "MILITARY_SERVICE_DATES":
              currentResume.Special.HideInfo.HideMilitaryServiceDates = cblHideSections.Items.FindByValue("MILITARY_SERVICE_DATES").Selected;
              break;

            case "WORK_HISTORY_DATES":
              currentResume.Special.HideInfo.HideWorkDates = cblHideSections.Items.FindByValue("WORK_HISTORY_DATES").Selected;
              break;

            case "CONTACT_INFO":
              currentResume.Special.HideInfo.HideContact = cblHideSections.Items.FindByValue("CONTACT_INFO").Selected;
              break;

            case "NAME":
              currentResume.Special.HideInfo.HideName = cblHideSections.Items.FindByValue("NAME").Selected;
              break;

            case "EMAIL":
              currentResume.Special.HideInfo.HideEmail = cblHideSections.Items.FindByValue("EMAIL").Selected;
              break;

            case "PHONE_HOME":
              currentResume.Special.HideInfo.HideHomePhoneNumber = cblHideSections.Items.FindByValue("PHONE_HOME").Selected;
              break;

            case "PHONE_WORK":
              currentResume.Special.HideInfo.HideWorkPhoneNumber = cblHideSections.Items.FindByValue("PHONE_WORK").Selected;
              break;

            case "PHONE_CELL":
              currentResume.Special.HideInfo.HideCellPhoneNumber = cblHideSections.Items.FindByValue("PHONE_CELL").Selected;
              break;

            case "PHONE_FAX":
              currentResume.Special.HideInfo.HideFaxPhoneNumber = cblHideSections.Items.FindByValue("PHONE_FAX").Selected;
              break;

            case "PHONE_NONUS":
              currentResume.Special.HideInfo.HideNonUSPhoneNumber = cblHideSections.Items.FindByValue("PHONE_NONUS").Selected;
              break;

            case "AFFILIATIONS":
              currentResume.Special.HideInfo.HideAffiliations = cblHideSections.Items.FindByValue("AFFILIATIONS").Selected;
              break;

            case "PROFESSIONAL_LICENSES":
              currentResume.Special.HideInfo.HideCertificationsAndProfessionalLicenses = cblHideSections.Items.FindByValue("PROFESSIONAL_LICENSES").Selected;
              break;

            case "DRIVER_LICENSE":
              currentResume.Special.HideInfo.HideDriverLicense = cblHideSections.Items.FindByValue("DRIVER_LICENSE").Selected;
              break;

            case "HONORS":
              currentResume.Special.HideInfo.HideHonors = cblHideSections.Items.FindByValue("HONORS").Selected;
              break;

            case "INTERESTS":
              currentResume.Special.HideInfo.HideInterests = cblHideSections.Items.FindByValue("INTERESTS").Selected;
              break;

            case "INTERNSHIPS":
              currentResume.Special.HideInfo.HideInternships = cblHideSections.Items.FindByValue("INTERNSHIPS").Selected;
              break;

            case "LANGUAGES":
              currentResume.Special.HideInfo.HideLanguages = cblHideSections.Items.FindByValue("LANGUAGES").Selected;
              break;

            case "MILITARY_SERVICE":
              currentResume.Special.HideInfo.HideVeteran = cblHideSections.Items.FindByValue("MILITARY_SERVICE").Selected;
              break;

            case "OBJECTIVE":
              currentResume.Special.HideInfo.HideObjective = cblHideSections.Items.FindByValue("OBJECTIVE").Selected;
              break;

            case "PERSONAL_INFO":
              currentResume.Special.HideInfo.HidePersonalInformation = cblHideSections.Items.FindByValue("PERSONAL_INFO").Selected;
              break;

            case "PROFESSIONAL_DEVELOPMENT":
              currentResume.Special.HideInfo.HideProfessionalDevelopment = cblHideSections.Items.FindByValue("PROFESSIONAL_DEVELOPMENT").Selected;
              break;

            case "PUBLICATIONS":
              currentResume.Special.HideInfo.HidePublications = cblHideSections.Items.FindByValue("PUBLICATIONS").Selected;
              break;

            case "REFERENCES":
              currentResume.Special.HideInfo.HideReferences = cblHideSections.Items.FindByValue("REFERENCES").Selected;
              break;

            case "SKILLS":
              currentResume.Special.HideInfo.HideSkills = cblHideSections.Items.FindByValue("SKILLS").Selected;
              break;

            case "TECHNICAL_SKILLS":
              currentResume.Special.HideInfo.HideTechnicalSkills = cblHideSections.Items.FindByValue("TECHNICAL_SKILLS").Selected;
              break;

            case "VOLUNTEER_ACTIVITIES":
              currentResume.Special.HideInfo.HideVolunteerActivities = cblHideSections.Items.FindByValue("VOLUNTEER_ACTIVITIES").Selected;
              break;

						case "THESIS_MAJOR_PROJECTS":
							currentResume.Special.HideInfo.HideThesisMajorProjects = cblHideSections.Items.FindByValue("THESIS_MAJOR_PROJECTS").Selected;
							break;
          }
        }

        if (currentResume.Special.HideInfo.HiddenJobs.IsNotNull())
        {
          currentResume.Special.HideInfo.HiddenJobs.Clear();
          foreach (ListItem job in lbHideJobs.Items)
          {
            if (job.Selected)
              currentResume.Special.HideInfo.HiddenJobs.Add(job.Value);
          }
        }

        var resumeNameUpdated = false;
        var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
        if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
        {
          currentResume.ResumeMetaInfo.ResumeName = resumeName;
          resumeNameUpdated = true;
        }

        if (!Utilities.IsObjectModified(clonedResume, currentResume))
          return;

				ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);

        App.SetSessionValue("Career:ResumeID", model.ResumeMetaInfo.ResumeId);

        #region Save Activity

        if (App.UserData.HasDefaultResume && App.UserData.DefaultResumeId == model.ResumeMetaInfo.ResumeId)
        {
          //TODO: Martha (new activity functionality)
          //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "37", staffProfile: staffcontext.StaffInfo);
        }

        #endregion

        App.SetSessionValue("Career:Resume", currentResume);

      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    //private void UpdateROnet(string ronet, string onet)
    //{
    //  var resumeModel = App.GetSessionValue<MyResumeModel>("Explorer:ResumeModel");

    //  if (!resumeModel.IsNotNull()) return;
    //  resumeModel.PersonResume.PrimaryROnet = ronet;
    //  resumeModel.PersonResume.PrimaryROnet = onet;
    //  App.SetSessionValue("Explorer:ResumeModel", resumeModel);
    //}

    /// <summary>
    /// Handles the Click event of the IbDownLoadResume control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void IbDownLoadResume_Click(object sender, EventArgs e)
    {
			DownloadResumePopup.Show(ddlReviewResumeFormat.SelectedValue, ddlReviewResumeOrder.SelectedValue, IsOriginal, HasHiddenContactDetails());
    }

    /// <summary>
    /// Handles the Click event of the IbEmailResume control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void IbEmailResume_Click(object sender, EventArgs e)
    {
			EmailResumePopup.Show(ddlReviewResumeFormat.SelectedIndex, ddlReviewResumeOrder.SelectedValue, IsOriginal, HasHiddenContactDetails());
    }

		/// <summary>
		/// Check if there are hidden options selected
		/// </summary>
		/// <returns>True if any options have been hidden</returns>
		private bool HasHiddenContactDetails()
		{
			return (cblHideSections.Items.FindByValue("CONTACT_INFO").IsNotNull() && cblHideSections.Items.FindByValue("CONTACT_INFO").Selected)
							|| (cblHideSections.Items.FindByValue("NAME").IsNotNull() && cblHideSections.Items.FindByValue("NAME").Selected)
							|| (cblHideSections.Items.FindByValue("EMAIL").IsNotNull() && cblHideSections.Items.FindByValue("EMAIL").Selected)
							|| (cblHideSections.Items.FindByValue("PHONE_HOME").IsNotNull() && cblHideSections.Items.FindByValue("PHONE_HOME").Selected);
		}

    /// <summary>
    /// Handles the SelectedIndexChanged event of the ddlReviewResumeFormat control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void ddlReviewResumeFormat_SelectedIndexChanged(object sender, EventArgs e)
    {
      ResumeDetail.Attributes["src"] = "";

      try
      {
        if (ddlReviewResumeFormat.SelectedValue == "0")
        {
          DisplayOriginalFormat();
        }
        else
        {
          var lookupItem = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.ResumeFormats).FirstOrDefault(x => x.Id == ddlReviewResumeFormat.SelectedValueToLong());
          var xslPath = (lookupItem.IsNotNull()) ? App.Settings.GetCustomXslForResume(lookupItem.ExternalId) : String.Empty;

          if (xslPath.IsNotNullOrEmpty())
          {
            try
            {
              xslPath = HttpContext.Current.Server.MapPath(xslPath);
            }
            catch
            {
            }
          }

          SetOriginalResume(false);
          var xResume = Utilities.FetchXMLResume();
          htmlJobDescription.Content = lblReviewFormattedOutput.Text = Utilities.ApplyTemplate(xResume, xslPath, App, ddlReviewResumeOrder.SelectedValue, true);
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnSaveResumeFormat control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnSaveResumeFormat_Click(object sender, EventArgs e)
    {
      try
      {
        if (App.User.IsAuthenticated)
        {
          var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
          var clonedResume = currentResume.CloneObject();

          if (currentResume.Special.IsNull())
            currentResume.Special = new ResumeSpecialInfo { PreservedFormatId = ddlReviewResumeFormat.SelectedValueToLong(), PreservedOrder = ddlReviewResumeOrder.SelectedItem.Text };
          else
          {
            currentResume.Special.PreservedFormatId = ddlReviewResumeFormat.SelectedValueToLong();
            currentResume.Special.PreservedOrder = ddlReviewResumeOrder.SelectedItem.Text;
          }
          
          var resumeNameUpdated = false;
          var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
          if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
          {
            currentResume.ResumeMetaInfo.ResumeName = resumeName;
            resumeNameUpdated = true;
          }

          if (currentResume.ResumeMetaInfo.IsDefault.GetValueOrDefault(false))
            Utilities.CheckOnetsInResume(currentResume, false);

					if (Utilities.IsObjectModified(clonedResume, currentResume))
						ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);

	        if (currentResume.ResumeMetaInfo.IsDefault.GetValueOrDefault(false))
		        Utilities.UpdateUserContextFromDefaultResume(currentResume, false);

					if (App.Settings.Theme == FocusThemes.Education)
					{
						SearchPostingsPopup.Visible = true;
						SearchPostingsPopup.ShowPopup();
					}
					else
					{
            Utilities.SaveCriteriaAgainstResume(currentResume, MatchingType.Resume, true);
            Response.RedirectToRoute("JobSearchResults");
					}
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

		/// <summary>
		/// Handles the Click event of the JobRecommendations control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="SearchPostingsPopup.JobRecommendationsClickEventArgs"/> instance containing the event data.</param>
		protected void JobRecommendations_Click(object sender, SearchPostingsPopup.JobRecommendationsClickEventArgs e)
		{

			var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
			Utilities.SaveCriteriaAgainstResume(currentResume, e.MatchingType, true);
			Response.RedirectToRoute("JobSearchResults");
		}

    /// <summary>
    /// Binds the hide options.
    /// </summary>
    private void BindHideOptions()
    {
      //TODO: Martha (UI) - Use a bitwise enum to store hide options
      var hideOptions = new Dictionary<string, string>
			                  	{
			                  		{"ALL_DATES", CodeLocalise("HideAllDates.Text", "Hide all dates")},
														{"EDUCATION_DATES", CodeLocalise("HideEducationDates.Text", "Hide education dates")},
														{"MILITARY_SERVICE_DATES", CodeLocalise("HideMilitaryServiceDates.Text", "Hide military service dates")},
														{"WORK_HISTORY_DATES", CodeLocalise("HideWorkHistoryDates.Text", "Hide work history dates")},
														{"CONTACT_INFO", CodeLocalise("HideContactInformation.Text", "Hide my contact information")},
														{"NAME", CodeLocalise("HideName.Text", "Hide my name")},
														{"EMAIL", CodeLocalise("HideEmail.Text", "Hide my email address")},
														{"PHONE_HOME", CodeLocalise("HideHomePhone.Text", "Hide home phone number")},
														{"PHONE_WORK", CodeLocalise("HideWorkPhone.Text", "Hide work phone number")},
														{"PHONE_CELL", CodeLocalise("HideCellPhone.Text", "Hide cell phone number")},
														{"PHONE_FAX", CodeLocalise("HideFaxNumber.Text", "Hide fax phone number")},
														{"PHONE_NONUS", CodeLocalise("HideNonUSNumber.Text", "Hide non US phone number")},
														{"AFFILIATIONS", CodeLocalise("HideAffiliations.Text", "Hide affiliations")},
														{"PROFESSIONAL_LICENSES", CodeLocalise("HideLicenseCertification.Text", "Hide occupational license and certification")},
														{"DRIVER_LICENSE", CodeLocalise("HideDriverLicense.Text", "Hide driver's license")},
														{"HONORS", CodeLocalise("HideHonors.Text", "Hide honors")},
														{"INTERESTS", CodeLocalise("HideInterests.Text", "Hide interests")},
														{"INTERNSHIPS", CodeLocalise("HideInternships.Text", "Hide internships")},
														{"LANGUAGES", CodeLocalise("HideLanguages.Text", "Hide languages")},
														{"MILITARY_SERVICE", CodeLocalise("HideMilitaryService.Text", "Hide military service")},
														{"OBJECTIVE", CodeLocalise("HideObjectives.Text", "Hide objectives")},
														{"PERSONAL_INFO", CodeLocalise("HidePersonalInformation.Text", "Hide personal information")},
														{"PROFESSIONAL_DEVELOPMENT", CodeLocalise("HideProfessionalDevelopment.Text", "Hide professional development")},
														{"PUBLICATIONS", CodeLocalise("HidePublications.Text", "Hide publications")},
														{"REFERENCES", CodeLocalise("HideReferences.Text", "Hide references")},
														{"SKILLS", CodeLocalise("HideSkills.Text", "Hide skills")},
														{"TECHNICAL_SKILLS", CodeLocalise("HideTechnicalSkills.Text", "Hide technical skills")},
														{"VOLUNTEER_ACTIVITIES", CodeLocalise("HideVolunteerActivities.Text", "Hide volunteer activities")},
														{"THESIS_MAJOR_PROJECTS", CodeLocalise("HideThesisMajorProjects.Text", "Hide thesis/major projects")},
			                  	};

      if (App.Settings.HideObjectives)
        hideOptions.Remove("OBJECTIVE");

      cblHideSections.DataSource = hideOptions;
      cblHideSections.DataValueField = "Key";
      cblHideSections.DataTextField = "Value";
      cblHideSections.DataBind();
    }
  }
}




