﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailResume.ascx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.Controls.EmailResume" %>
<asp:HiddenField ID="EmailResumeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="EmailResumeModal" runat="server" BehaviorID="EmailResumeModal"
	TargetControlID="EmailResumeModalDummyTarget" PopupControlID="EmailResumeModalPanel"
	BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" ClientIDMode="Static">
	<Animations>
    <OnShown>
			<ScriptAction Script="EnableDisableEmailResumeOrder();" />  
    </OnShown>
	</Animations>
</act:ModalPopupExtender>
<div id="EmailResumeModalPanel" tabindex="-1" class="modal" style="z-index:100001; display:none;">
	<div class="lightbox resumeEmailModal">
		<div class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('EmailResumeModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
			<focus:LocalisedLabel runat="server" ID="ResumeEmailTitleLabel" LocalisationKey="ResumeEmailTitle.Label" DefaultText="Email your resume" CssClass="modalTitle modalTitleLarge"/>
		</div>
        <div class="emailScrollPane">
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="ResumeFormatLabel" AssociatedControlID="ddlEmailResumeFormat" CssClass="dataInputLabel" LocalisationKey="ResumeFormat.Label" DefaultText="Resume style"/>
			<span class="dataInputField">
				<asp:DropDownList ID="ddlEmailResumeFormat" runat="server" Width="220px" ClientIDMode="Static" onchange="EnableDisableEmailResumeOrder();"></asp:DropDownList>
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="ResumeOrderLabel" AssociatedControlID="ddlEmailResumeOrder" CssClass="dataInputLabel" LocalisationKey="ResumeOrder.Label" DefaultText="Resume order"/>
			<span class="dataInputField">
				<asp:DropDownList ID="ddlEmailResumeOrder" runat="server" Width="220px" ClientIDMode="Static">
					<asp:ListItem Text="Emphasize my experience" Value="experience"></asp:ListItem>
					<asp:ListItem Text="Emphasize my education" Value="education"></asp:ListItem>
					<asp:ListItem Text="Emphasize my skills" Value="skills"></asp:ListItem>
				</asp:DropDownList>
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="FileFormatLabel" AssociatedControlID="rdoDownloadType" CssClass="dataInputLabel" LocalisationKey="FileFormat.Label" DefaultText="File format"/>
			<span class="dataInputField">
				<asp:RadioButtonList ID="rdoDownloadType" runat="server" ClientIDMode="Static" RepeatLayout="Flow" RepeatDirection="Vertical" CssClass="dataInputVerticalRadio">
					<asp:ListItem Selected="True" Text="in message body" />
					<asp:ListItem Text="as RTF (Word) attachment" />
					<asp:ListItem Text="as PDF attachment" />
				</asp:RadioButtonList>
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="HideOptionsLabel" AssociatedControlID="rblHideOptions" CssClass="dataInputLabel" LocalisationKey="HideOptions.Label" DefaultText="Apply hide options"/>
			<span class="dataInputField">
				<asp:RadioButtonList ID="rblHideOptions" runat="server" ClientIDMode="Static" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="dataInputHorizontalRadio">
					<asp:ListItem Selected="True" Text="Yes" />
					<asp:ListItem Text="No" />
				</asp:RadioButtonList>
			</span>
		</div>
		<asp:PlaceHolder runat="server" ID="HiddenContactPlaceHolder">
			<div>
				<focus:LocalisedLabel runat="server" ID="HiddenContactLabel"  CssClass="error" LocalisationKey="HiddenContact.Label" DefaultText="One or more pieces of your contact information are currently hidden. Employers who like your resume may have difficulty contacting you if you continue with the selected hide options applied."/>
			</div>
			<br />
		</asp:PlaceHolder>
		<div class="emailDetailPanel">
			<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="EmailInstructionLabel" RenderOuterSpan="False" LocalisationKey="EmailInstruction.Label" DefaultText="Separate email addresses with a semicolon and do not use a space."/></p>
			<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailResumeToLabel" AssociatedControlID="txtToEmailAddress" CssClass="dataInputLabel" LocalisationKey="EmailResumeTo.Label" DefaultText="To:"/>
				<span class="dataInputField">
					<asp:TextBox ID="txtToEmailAddress" runat="server" Width="280px" ClientIDMode="Static"></asp:TextBox>
					<asp:CustomValidator ID="custToEmail" runat="server" ControlToValidate="txtToEmailAddress" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="EmailResume" ClientValidationFunction="ValidateToEmail" ValidateEmptyText="true" />
				</span>
			</div>
			<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailResumeSubjectLabel" AssociatedControlID="txtEmailSubject" CssClass="dataInputLabel" LocalisationKey="EmailResumeSubject.Label" DefaultText="Subject:"/>
				<span class="dataInputField">
					<asp:TextBox ID="txtEmailSubject" runat="server" Width="280px"></asp:TextBox>
					<asp:RequiredFieldValidator ID="EmailSubjectRequired" runat="server" ControlToValidate="txtEmailSubject" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="EmailResume" />
				</span>
			</div>
		</div>
        </div>
        <div class="modalButtons">
			<asp:Button ID="btnEmailResume" Text="Send" runat="server" class="buttonLevel2" OnClick="btnEmailResume_Click" ValidationGroup="EmailResume" />
		</div>

	</div>
</div>
<script type="text/javascript">
    Sys.Application.add_load(MyEmailResume_PageLoad);
    function MyEmailResume_PageLoad(sender, args) {
    	// Jaws Compatibility
    	var modal = $find('EmailResumeModal');
	    if (modal != null) {
	    	 modal.add_shown(function () {
	    	 	$('.modal').attr('aria-hidden', false);
	    	 	$('.page').attr('aria-hidden', true);
	    	 	$('#ddlEmailResumeFormat').focus();
		    });

			   modal.add_hiding(function () {
			   	$('.modal').attr('aria-hidden', true);
			   	$('.page').attr('aria-hidden', false);
		    });
	    }
    }

    function ValidateToEmail(sender, args) {
		var reg = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
		var address = $('#txtToEmailAddress').val();
		if (args.Value.length === 0 || args.Value === '') {
			sender.innerHTML = '<%= EmailAddressRequired %>';
			args.IsValid = false;
		}
		else if (args.Value.length < 6) {
			sender.innerHTML = '<%= EmailAddressCharLimit %>';
			args.IsValid = false;
		}
		else if (reg.test(address) === false) {
			sender.innerHTML = '<%= EmailAddressErrorMessage %>';
			args.IsValid = false;
		}
	}

	function EnableDisableEmailResumeOrder() {
		if ($('#ddlEmailResumeFormat').val() !== "0") {
			$('#ddlEmailResumeOrder').prop('disabled', false).next().removeClass('stateDisabled');
			$("*[name$='rdoDownloadType']").removeAttr('disabled');
			$("*[name$='rblHideOptions']").removeAttr('disabled');
		} else {
			$('#ddlEmailResumeOrder').prop('disabled', true).next().addClass('stateDisabled');
			$("*[name$='rdoDownloadType']").prop('disabled', true);
			$("*[name$='rblHideOptions']").prop('disabled', true);
		}
	}
</script>
