﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.CareerExplorer.WebCareer.Controls {
    
    
    public partial class ResumePreview {
        
        /// <summary>
        /// ResumePreviewModalDummyTarget control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField ResumePreviewModalDummyTarget;
        
        /// <summary>
        /// ResumePreviewModal control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.ModalPopupExtender ResumePreviewModal;
        
        /// <summary>
        /// ResumePreviewTitleLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ResumePreviewTitleLabel;
        
        /// <summary>
        /// ResumeFormatLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ResumeFormatLabel;
        
        /// <summary>
        /// ddlResumeFormat control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlResumeFormat;
        
        /// <summary>
        /// ResumeOrderLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ResumeOrderLabel;
        
        /// <summary>
        /// ddlResumeOrder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlResumeOrder;
        
        /// <summary>
        /// ResumePreviewInfoLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel ResumePreviewInfoLabel;
        
        /// <summary>
        /// upPanResume control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel upPanResume;
        
        /// <summary>
        /// ResumeDetail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl ResumeDetail;
        
        /// <summary>
        /// panFormattedOutput control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel panFormattedOutput;
        
        /// <summary>
        /// lblFormattedOutput control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblFormattedOutput;
    }
}
