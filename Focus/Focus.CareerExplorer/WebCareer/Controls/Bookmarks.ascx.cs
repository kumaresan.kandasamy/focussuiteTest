﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Controls;
using Focus.Common.ServiceClients;
using Focus.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class Bookmarks : UserControlBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				DisplayBookmarks();
		}

		private void Bind()
		{
			try
			{
				// Existing Resumes
				if (App.User.IsAuthenticated)
				{
					var jobInfo = AnnotationClient.Instance.GetBookmarkNames();
					if (jobInfo.Count > 0)
					{
						Panel1.Visible = false;
						Panel2.Visible = true;
						MyBookmarkList.Items = (from i in jobInfo select new Item { Id = i.Id.ToString(), Name = i.Text, Type = "DELETE_BOOKMARKS" }).ToList();
					}
					else
					{
						Panel1.Visible = true;
						Panel2.Visible = false;
					}
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		private void DisplayBookmarks()
		{
			if (App.User.IsAuthenticated)
			{
				Panel1.Visible = false;
				Panel2.Visible = true;
				Bind();
			}
			else
			{
				Panel1.Visible = true;
				Panel2.Visible = false;
			}
		}

		protected void lnkMYBookmark_Click(object sender, EventArgs e)
		{
			var lbtnName = (LinkButton)sender;
			if (!String.IsNullOrEmpty(lbtnName.CommandArgument))
				Response.RedirectToRoute("JobPosting", new { jobid = lbtnName.CommandArgument, fromURL = "Home" });
		}
	}
}