﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateEditResumePopup.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.CreateEditResumePopup" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/UploadResume.ascx" TagName="UploadResume" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/PasteTypeResume.ascx" TagName="PasteTypeResume" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/ResumeMissingInfo.ascx" TagName="ResumeMissingInfo" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/CreateEditResume.ascx" TagName="CreateEditResume" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/ResumeList.ascx" TagName="ResumeList" TagPrefix="uc" %>
<asp:HiddenField ID="CreateOrEditResumeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="CreateOrEditResumeModal" runat="server" BehaviorID="CreateOrEditResumeModal" TargetControlID="CreateOrEditResumeModalDummyTarget" PopupControlID="CreateOrEditResumeModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<div id="CreateOrEditResumeModalPanel" tabindex="-1" class="modal" style="z-index: 100001; display: none; overflow:auto;">
	<div class="lightbox lightboxmodal FocusCareer">
		<asp:Panel class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" class="modalCloseIcon" onclick="javascript:$find('CreateOrEditResumeModal').hide();return false;" alt="<%= HtmlLocalise("Global.Close.ToolTip", "Close") %>" width="20" height="20" />
		</asp:Panel>
		<div class="modalMessage" runat="server" ID="ModalPanelHeader" ClientIDMode="Static">
			<asp:Label ID="ResumeLimitReachedLabel" runat="server" CssClass="modalTitle" Visible="False" ForeColor="#FF0000" />
			<focus:LocalisedLabel runat="server" ID="MoreInfoLabel" LocalisationKey="TheMoreInfo.Label" DefaultText="The more information you provide on your resume, the better the matches we can provide. When you're done, you'll be able to choose which information to display to #BUSINESSES#:LOWER." RenderOuterSpan="False"/>
		</div>
		<asp:Panel runat="server" ID="CreateEditUploadPanel" CssClass="accordionContainerCreateEditResume">
			<div class="landingPageNavigation accordionNew">
				<div class="accordionSectionNew">
					<div class="accordionTitleNew accordionCreateEditResume">
						<focus:LocalisedLabel runat="server" ID="CreateOrEditResumeLabel" LocalisationKey="CreateOrEditResume.Label" DefaultText="Create or edit a resume" style="margin-top:3px" />
						<focus:LocalisedLabel runat="server" ID="CreateResumeLabel" LocalisationKey="CreateOrEditResume.Label" DefaultText="Create a resume" Visible="False" />
					</div>
					<div class="accordionContentNew" id="CreateEditDiv" runat="server">
						<uc:CreateEditResume ID="CreateResume" runat="server" />
						<div class="genericContainer">
							<focus:LocalisedLabel runat="server" ID="EditDeleteResumeLabel" LocalisationKey="EditDeleteResume.Label" DefaultText="Edit or delete a resume" CssClass="genericContainerHeader" />
						</div>
						<uc:ResumeList ID="MyResumeList" runat="server" TargetPage="ResumeWizard" CssClass="genericContainerContent" OnLinkNameClicked="OnLinkNameClicked" SetConfirmationSession="True" />
					</div>
				</div>
				<asp:PlaceHolder runat="server" ID="UploadSection">
					<div class="accordionSectionNew">
						<div class="accordionTitleNew accordionCreateEditResume">
						  <focus:LocalisedLabel runat="server" ID="UploadAResumeLabel" LocalisationKey="UploadAResume.Label" DefaultText="Upload a resume"/>
						</div>
						<div class="accordionContentNew">
							<uc:UploadResume ID="UploadResume" runat="server" />
						</div>
					</div>
				</asp:PlaceHolder>
				<asp:PlaceHolder runat="server" ID="PasteSection">
					<div class="accordionSectionNew">
						<div class="accordionTitleNew accordionCreateEditResume">
						  <focus:LocalisedLabel runat="server" ID="PasteAResumeLabel" LocalisationKey="PasteAResume.Label" DefaultText="Paste a resume"/>
						</div>
						<div class="accordionContentNew">
							<uc:PasteTypeResume ID="PasteTypeResume" runat="server" />
						</div>
					</div>
				</asp:PlaceHolder>
			</div>
		</asp:Panel>
	</div>
</div>
<uc:ResumeMissingInfo ID="ResumeMissingInfoPopup" runat="server" Visible="False" />
