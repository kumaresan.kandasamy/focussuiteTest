﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.CareerExplorer.Code;
using Focus.Core.Views;
using Framework.Core;
#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class Summary : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
				LocaliseUI();
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			SummaryValidator.ErrorMessage = CodeLocalise("Summary.RequiredErrorMessage", "Summary is required");
		}

		protected void SaveMoveToNextStepButton_Click(object sender, EventArgs e)
		{
			if (SaveModel())
        Response.RedirectToRoute("ResumeWizardTab", new { Tab = "Options" });
		}

    /// <summary>
    /// Binds the step.
    /// </summary>
		internal void BindStep()
		{

			var model = App.GetSessionValue<ResumeModel>("Career:Resume");

			if (model.IsNotNull() && model.ResumeContent.IsNotNull())
			{
				var summaryinfo = model.ResumeContent.SummaryInfo;
				if (summaryinfo.IsNotNull())
				{
					SummaryRadioButtonList.SelectValue(summaryinfo.IncludeSummary.HasValue ? summaryinfo.IncludeSummary.AsYesNo() : "yes");
					if (!string.IsNullOrWhiteSpace(summaryinfo.Summary))
						SummaryTextBox.Text = summaryinfo.Summary;
					else if (!string.IsNullOrWhiteSpace(summaryinfo.AutomatedSummary))
						SummaryTextBox.Text = summaryinfo.AutomatedSummary;
					else
						BindSnapShot();
				}
				else
					BindSnapShot();

				hdnOriginalSummary.Value = SummaryTextBox.Text;
			}
		}

    /// <summary>
    /// Binds the snap shot.
    /// </summary>
		private void BindSnapShot()
		{
			try
			{				
				var model = App.GetSessionValue<ResumeModel>("Career:Resume");
				var automatedSummary = ServiceClientLocator.ResumeClient(App).GetSnapShot(model, false);
				SummaryTextBox.Text = automatedSummary;
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		#region Save Summary section

    /// <summary>
    /// Saves the model.
    /// </summary>
    /// <returns></returns>
		private bool SaveModel()
		{
			bool saveStatus;
			
			var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
			var clonedResume = currentResume.CloneObject();

			var tempSummary = new SummaryInfo
			{
				IncludeSummary = SummaryRadioButtonList.SelectedValue.AsBoolean(),
				Summary = string.IsNullOrWhiteSpace(SummaryTextBox.Text) ? null : SummaryTextBox.TextTrimmed().Replace("\r\n", "\n")
			};

			var previousResumeCompletionStatus = ResumeCompletionStatuses.None;

			if (currentResume.IsNull())
			{
				currentResume = new ResumeModel
				{
					ResumeContent = new ResumeBody
					{
						SummaryInfo = tempSummary
					},
					ResumeMetaInfo = new ResumeEnvelope { CompletionStatus = ResumeCompletionStatuses.Summary }
				};
			}
			else
			{
				previousResumeCompletionStatus = currentResume.ResumeMetaInfo.CompletionStatus;
				currentResume.ResumeContent.SummaryInfo = tempSummary;

				currentResume.ResumeMetaInfo.CompletionStatus = previousResumeCompletionStatus | ResumeCompletionStatuses.Summary;
			}

      var resumeNameUpdated = false;

			var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
			if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
			{
				currentResume.ResumeMetaInfo.ResumeName = resumeName;
				resumeNameUpdated = true;
			}

			if (!Utilities.IsObjectModified(clonedResume, currentResume))
				return true;

			try
			{
				
				var resumeModel = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);
				var resumeId = resumeModel.ResumeMetaInfo.ResumeId;

				if (!App.UserData.HasDefaultResume)
          App.UserData.DefaultResumeId = resumeId;
				if (App.UserData.DefaultResumeId == resumeId)
					Utilities.UpdateUserContextUserInfo(null, ResumeCompletionStatuses.Summary);
				currentResume.ResumeMetaInfo.CompletionStatus = (previousResumeCompletionStatus | ResumeCompletionStatuses.Summary);
				if (currentResume.ResumeMetaInfo.ResumeId.IsNull())
					currentResume.ResumeMetaInfo.ResumeId = resumeId;

				App.SetSessionValue("Career:ResumeID", resumeId);
				App.SetSessionValue("Career:Resume", currentResume);

				saveStatus = true;

				#region Save Activity

				if (App.UserData.HasDefaultResume && App.UserData.DefaultResumeId == resumeId)
				{
          //TODO: Martha (new activity functionality)
          //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "37", staffProfile: staffcontext.StaffInfo);
				}

				#endregion
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
				saveStatus = false;
			}
			return saveStatus;
		}

		#endregion
	}
}
