﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Services.Core;
using Focus.CareerExplorer.Code;
using Framework.Core;

#endregion


namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class Contact : UserControlBase
	{
    #region Page Properties
    protected string PhoneNumberRequired = "";
    protected string PhoneTypeRequired = "";
    protected string PhoneNumberError = "";
    protected string AtleastOneUSPhoneNumberRequired = "";
    protected string IsCountyEnabled = "";
    protected string ZIPCodeRequired = "";
    protected string ZIPCodeLimitError = "";
    #endregion

    //public event EventHandler SaveResumeTitle;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      LocaliseUI();
      tableCounty.Visible = App.Settings.CountyEnabled;
      IsCountyEnabled = App.Settings.CountyEnabled ? "True" : "False";
			EmailAddressTextBox.MaxLength = App.Settings.MaximumEmailLength;            
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallMaskPhoneNumber_1", "MaskPhoneNumber();", true);
        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallMaskAdditionalPhoneNumber_1", "MaskAdditionalPhoneNumber();", true);
    }

    /// <summary>
    /// Handles the Click event of the SaveMoveToNextStepButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveMoveToNextStepButton_Click(object sender, EventArgs e)
    {
      UnBindPhoneNumbers();
      if (SaveModel())
        Response.RedirectToRoute("ResumeWizardTab", new { Tab = "Education" });
    }

    #region Bind contact section

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
      BindDropdowns();
      var additionalNumbers = new List<AdditionalNumber>();
      var usercontext = App.GetSessionValue<UserContext>("Career:UserContext");

      var contact = GetContactDetails();

      if (contact.IsNotNull())
      {
        #region Set name details

        var seekerName = contact.SeekerName;
        if (seekerName.IsNotNull())
        {
          FirstNameTextBox.Text = seekerName.FirstName != "Unknown" ? seekerName.FirstName : string.Empty;
          // Truncate middle name to a single initial
          MiddleInitialTextBox.Text =seekerName.MiddleName.IsNotNullOrEmpty()? seekerName.MiddleName.Substring(0,1) : "";
          LastNameTextBox.Text = seekerName.LastName != "Unknown" ? seekerName.LastName : string.Empty;
          if (seekerName.SuffixId.IsNotNull())
            SuffixDropDown.SelectValue(seekerName.SuffixId.ToString());
        }

        #endregion

        #region Set address details

        if (contact.PostalAddress.IsNotNull())
        {
          AddressTextBox.Text = contact.PostalAddress.Street1;
          Address2TextBox.Text = contact.PostalAddress.Street2;
          CityTextBox.Text = contact.PostalAddress.City;

          if (contact.PostalAddress.StateId.IsNotNull())
            ddlState.SelectValue(contact.PostalAddress.StateId.ToString());

          if (contact.PostalAddress.CountryId.IsNotNull())
            ddlCountry.SelectValue(contact.PostalAddress.CountryId.ToString());

          if (App.Settings.CountyEnabled && contact.PostalAddress.CountyId.IsNotNull())
          {
            ddlCounty.SelectValue(contact.PostalAddress.CountyId.ToString());
            ccdCounty.SelectedValue = contact.PostalAddress.CountyId.ToString();
          }
        }

        ZipTextBox.Text = contact.PostalAddress.Zip;

        #endregion

        #region Set phone details

        if (contact.PhoneNumber.IsNotNull())
          additionalNumbers = (from i in contact.PhoneNumber
                               select new AdditionalNumber
                               {
                                 Dropdown = i.PhoneType.ToString(),
                                 Textbox = i.PhoneNumber
                               }).ToList();

        if (additionalNumbers.Count > 0)
        {
					PrimaryContactPhoneNoTextBox.Text = additionalNumbers[0].Textbox;
					PrimaryContactPhoneNoDropDownList.SelectValue(additionalNumbers[0].Dropdown);

          if (additionalNumbers[0].Dropdown == PhoneType.NonUS.ToString())
          {
						//mePrimaryContactPhoneNo.Mask = "99999999999999999999";
						//mePrimaryContactPhoneNo.MaskType = AjaxControlToolkit.MaskedEditType.None;
						//mePrimaryContactPhoneNo.PromptCharacter = " ";
          }

          additionalNumbers.RemoveAt(0);
        }

        if (additionalNumbers.IsNull() || additionalNumbers.Count == 0)
        {
          additionalNumbers.Add(new AdditionalNumber { Dropdown = "", Textbox = string.Empty });
        }

        #endregion region

        EmailAddressTextBox.Text = contact.EmailAddress.IsNotNullOrEmpty() ? contact.EmailAddress : App.User.EmailAddress;
        WebsiteTextBox.Text = contact.WebSite;
      }
      else
        EmailAddressTextBox.Text = (usercontext.IsNotNull() && App.User.EmailAddress.IsNotNullOrEmpty()) ? App.User.EmailAddress : "";

      AdditionalNumberRepeater.DataSource = additionalNumbers;
      AdditionalNumberRepeater.DataBind();
    }
    #endregion

    #region Save contact section

		/// <summary>
		/// Performs some extra server side validation on the model
		/// </summary>
		/// <param name="tempContact">The model to validate.</param>
		/// <returns>
		/// Whether the model is valid
		/// </returns>
		private bool ValidateModel(Core.Models.Career.Contact tempContact)
		{
			if (ServiceClientLocator.AccountClient(App).CheckEmailAddress(tempContact.EmailAddress))
			{
				EmailAddressExistsValidator.IsValid = false;
				return false;
			}

			return true;
		}

    /// <summary>
    /// Saves the model.
    /// </summary>
    /// <returns></returns>
    private bool SaveModel()
    {
      bool saveStatus;

      var userId = (App.User.IsAuthenticated) ? App.User.UserId : (long?)null;

      var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
      var clonedResume = currentResume.CloneObject();
      var tempContact = new Core.Models.Career.Contact
      {
        SeekerName = new Name
        {
          FirstName = FirstNameTextBox.TextTrimmed(defaultValue: null),
          MiddleName = MiddleInitialTextBox.TextTrimmed(defaultValue: null),
          LastName = LastNameTextBox.TextTrimmed(defaultValue: null),
          SuffixId = SuffixDropDown.SelectedValueToNullableLong(),
          SuffixName = SuffixDropDown.SelectedValueToNullableLong().IsNullOrZero() ? null : SuffixDropDown.SelectedItem.Text,
        },
        PostalAddress = new Address
        {
          Street1 = AddressTextBox.TextTrimmed(defaultValue: null),
          Street2 = Address2TextBox.TextTrimmed(defaultValue: null),
          City = CityTextBox.TextTrimmed(defaultValue: null),
          StateId = ddlState.SelectedValueToLong(),
          StateName = ddlState.SelectedItem.Text,
          CountyId = App.Settings.CountyEnabled ? ddlCounty.SelectedValueToLong() : 0,
          CountyName = App.Settings.CountyEnabled ? ddlCounty.SelectedItem.Text : string.Empty,

          CountryId = Convert.ToInt64(ddlCountry.SelectedValue),
          CountryName = ddlCountry.SelectedItem.Text,
          Zip = ZipTextBox.TextTrimmed(defaultValue: null)
        },
        PhoneNumber = SetPhoneNumbers(),
        EmailAddress = EmailAddressTextBox.TextTrimmed(defaultValue: null),
        WebSite = WebsiteTextBox.TextTrimmed(defaultValue: null)
      };


      //if ((currentResume.ResumeMetaInfo.CompletionStatus & ResumeCompletionStatuses.Contact) == ResumeCompletionStatuses.Contact
      //  && currentResume.IsNotNull()
      //  && currentResume.ResumeContent.IsNotNull()
      //  && !Utilities.IsObjectModified(currentResume.ResumeContent.SeekerContactDetails, tempContact))
      //  return true;

      //var previousResumeCompletionStatus = ResumeCompletionStatuses.None;
      if (currentResume.IsNull())
      {
        currentResume = new ResumeModel
        {
          ResumeContent = new ResumeBody
          {
            SeekerContactDetails = tempContact
          },
          ResumeMetaInfo = new ResumeEnvelope { CompletionStatus = ResumeCompletionStatuses.Contact }
        };
      }
      else
      {
        //previousResumeCompletionStatus = currentResume.ResumeMetaInfo.CompletionStatus;
        currentResume.ResumeContent.SeekerContactDetails = tempContact;

        //if (previousResumeCompletionStatus < (int)ResumeCompletionStatus.Contact)
        currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.Contact;

      }


      bool resumeNameUpdated = false;
      var resumeName = ((ResumeWizard)this.Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
      if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
      {
        currentResume.ResumeMetaInfo.ResumeName = resumeName;
        resumeNameUpdated = true;
      }

      if (!Utilities.IsObjectModified(clonedResume, currentResume))
        return true;

			if (!ValidateModel(tempContact))
				return false;

      try
      {
	     
				var resumeModel = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);
        var resumeId = resumeModel.ResumeMetaInfo.ResumeId;

        //if (SaveResumeTitle != null)
        //  SaveResumeTitle(this, EventArgs.Empty);

        if (!App.UserData.HasDefaultResume)
          App.UserData.DefaultResumeId = resumeId;

				if (App.UserData.DefaultResumeId == resumeId)
				{
					Utilities.UpdateUserContextUserInfo(tempContact, ResumeCompletionStatuses.Contact);

					App.User.EmailAddress = tempContact.EmailAddress;
					App.User.Save();
				}
	      //currentResume.ResumeMetaInfo.CompletionStatus = (previousResumeCompletionStatus | ResumeCompletionStatuses.Contact);
        if (currentResume.ResumeMetaInfo.ResumeId.IsNotNull())
          currentResume.ResumeMetaInfo.ResumeId = resumeId;

        App.SetSessionValue("Career:ResumeID", resumeId);
        App.SetSessionValue("Career:Resume", currentResume);

        saveStatus = true;

        #region Save Activity
        if (App.UserData.HasDefaultResume && App.UserData.DefaultResumeId == resumeId)
        {
          //TODO: Martha (new activity functionality)
          //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "37", staffProfile: staffcontext.StaffInfo);
        }

        #endregion
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
        saveStatus = false;
      }
      return saveStatus;
    }

    //private void UpdateROnet(string ronet, string onet)
    //{
    //  var resumeModel = App.GetSessionValue<MyResumeModel>("Explorer:ResumeModel");
    //  if (resumeModel.IsNotNull())
    //  {
    //    resumeModel.PersonResume.PrimaryROnet = ronet;
    //    resumeModel.PersonResume.PrimaryROnet = onet;
    //    App.SetSessionValue("Explorer:ResumeModel", resumeModel);
    //  }
    //}

    /// <summary>
    /// Sets the phone numbers.
    /// </summary>
    /// <returns></returns>
    private List<Phone> SetPhoneNumbers()
    {
      var phones = new List<Phone>();
			phones.Add(new Phone { PhoneNumber = System.Text.RegularExpressions.Regex.Replace(PrimaryContactPhoneNoTextBox.TextTrimmed(), @"[^0-9]", ""), PhoneType = (PhoneType)Enum.Parse(typeof(PhoneType), PrimaryContactPhoneNoDropDownList.SelectedValue, true) });
      var temp = (from i in UnBindPhoneNumbers()
                  select new Phone
                  {
                    PhoneType = (PhoneType)Enum.Parse(typeof(PhoneType), i.Dropdown, true),
                    PhoneNumber = i.Textbox
                  }).ToList();
      phones.AddRange(temp);
      return phones;
    }
    #endregion

    #region Additional Number Bind, Unbind & DataBound

    /// <summary>
    /// Handles the Click event of the AddAnotherButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void AddAnotherButton_Click(object sender, EventArgs e)
    {
      BindAdditionalNumberRepeater(UnBindPhoneNumbers());

      //Check for ZIP Validation
			if (ddlCountry.SelectedValue != GetLookupId(Constants.CodeItemKeys.Countries.US).ToString())
      {
        //lblRedAsterik.Style["display"] = "none";
	      ZipCodeLabel.CssClass = "dataInputLabel";
        ZipRequired.Enabled = false;
      }
      else
      {
        //lblRedAsterik.Style["display"] = "";
	      ZipCodeLabel.CssClass = "dataInputLabel requiredData";
        ZipRequired.Enabled = true;
      }
    }

    /// <summary>
    /// Binds the additional number repeater.
    /// </summary>
    /// <param name="additionalNumber">The additional number.</param>
    private void BindAdditionalNumberRepeater(List<AdditionalNumber> additionalNumber)
    {
      additionalNumber.Add(new AdditionalNumber { Dropdown = "", Textbox = string.Empty });
      AdditionalNumberRepeater.DataSource = additionalNumber;
      AdditionalNumberRepeater.DataBind();
    }

    /// <summary>
    /// Uns the bind phone numbers.
    /// </summary>
    /// <returns></returns>
    private List<AdditionalNumber> UnBindPhoneNumbers()
    {
      var resultList = new List<AdditionalNumber>();
      foreach (RepeaterItem item in AdditionalNumberRepeater.Items)
      {
				if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
				{
					var additionalNumberTextbox = (TextBox)item.FindControl("AdditionalNumberTextBox");
					var additionalNumberDropDownList = (DropDownList)item.FindControl("AdditionalNumberDropDownList");
					if (additionalNumberTextbox.IsNotNull() && additionalNumberTextbox.TextTrimmed().IsNotNullOrEmpty() && additionalNumberTextbox.TextTrimmed() != "(___) ___-____")
						resultList.Add(new AdditionalNumber { Dropdown = additionalNumberDropDownList.SelectedValue, Textbox = System.Text.RegularExpressions.Regex.Replace(additionalNumberTextbox.TextTrimmed(), @"[^0-9]", "") });
				}
      }
      return resultList;
    }

    /// <summary>
    /// Handles the ItemDataBound event of the AdditionalNumberRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void AdditionalNumberRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        return;

      var additionalNumberTextBox = (TextBox)e.Item.FindControl("AdditionalNumberTextBox");
      var additionalNumberDropDownList = (DropDownList)e.Item.FindControl("AdditionalNumberDropDownList");
      //var additionalPhoneMasking = (AjaxControlToolkit.MaskedEditExtender)e.Item.FindControl("meAdditionalNumber");
      //additionalPhoneMasking.BehaviorID = "meBehaviourAdditionalNumber_" + e.Item.ItemIndex;

	  additionalNumberDropDownList.Attributes.Add("onchange", string.Format("ChangeAdditionalPhoneNoFormat('{0}', '{1}')", additionalNumberDropDownList.ClientID, additionalNumberTextBox.ClientID));

      var additionalNumber = ((AdditionalNumber)e.Item.DataItem);
      additionalNumberTextBox.Text = additionalNumber.Textbox;
      additionalNumberDropDownList.SelectValue(additionalNumber.Dropdown);

      /*if (additionalNumber.Dropdown == PhoneType.NonUS.ToString())
      {
        //additionalPhoneMasking.Mask = "99999999999999999999";
        //additionalPhoneMasking.MaskType = AjaxControlToolkit.MaskedEditType.None;
        //additionalPhoneMasking.PromptCharacter = " ";
      }*/

      System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallMaskAdditionalPhoneNumber", "MaskAdditionalPhoneNumber();", true);
    }

    #endregion

    #region Dropdown binding & UI localization

    /// <summary>
    /// Binds the dropdowns.
    /// </summary>
    private void BindDropdowns()
    {
      SuffixDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Suffixes), null, CodeLocalise("SuffixDropDown.TopDefault", "- select suffix -"));
      ddlState.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), "", CodeLocalise("State.TopDefault", "- select state -"));
      ddlCountry.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Key == App.Settings.DefaultCountryKey).Select(x => x.Id).SingleOrDefault().ToString(), CodeLocalise("Country.TopDefault", "- select country -"));
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      FirstNameRequired.ErrorMessage = CodeLocalise("FirstName.RequiredErrorMessage", "First name is required");
      LastNameRequired.ErrorMessage = CodeLocalise("LastName.RequiredErrorMessage", "Last name is required");
      AddressRequired.ErrorMessage = CodeLocalise("Address.RequiredErrorMessage", "Address is required");
      CityRequired.ErrorMessage = CodeLocalise("City.RequiredErrorMessage", "City is required");
      if (App.Settings.CountyEnabled)
        CountyRequired.ErrorMessage = CodeLocalise("County.RequiredErrorMessage", "County is required");
      ZIPCodeRequired = CodeLocalise("Zip.RequiredErrorMessage", "ZIP is required");
      ZIPCodeLimitError = CodeLocalise("Zip.ErrorMessage", "U.S. ZIP code must be in a 5 or 9-digit format");
      StateRequired.ErrorMessage = CodeLocalise("State.RequiredErrorMessage", "State is required");
      CountryRequired.ErrorMessage = CodeLocalise("Country.RequiredErrorMessage", "Country is required");
      PhoneNumberRequired = CodeLocalise("PhoneNumber.RequiredErrorMessage", "Phone number is required");
      PhoneTypeRequired = CodeLocalise("PhoneType.RequiredErrorMessage", "Phone type is required");
			PhoneTypeSelectedValidator.ErrorMessage = CodeLocalise("PhoneType.RequiredErrorMessage", "Phone type is required");
      PhoneNumberError = CodeLocalise("PhoneNumber.ErrorMessage", "U.S. phone number must be 10 digits");
      EmailAddressRequired.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
      valEmailAddress.ErrorMessage = CodeLocalise("EmailAddressValidate.ErrorMessage", "Email address is not correct format");
      AtleastOneUSPhoneNumberRequired = CodeLocalise("AtleastOneUSPhoneNumber.ErrorMessage", "At least one U.S. phone number required");
			MiddleInitialRegEx.ErrorMessage = CodeLocalise("MiddleInitialRegEx.ErrorMessage", "Only letters are allowed");
	    EmailAddressExistsValidator.ErrorMessage = CodeLocalise("EmailAddressExistsValidator.ErrorMessage", "Email address is already in use");
    }

    #endregion

    /// <summary>
    /// Gets the contact details.
    /// </summary>
    /// <returns></returns>
    private Core.Models.Career.Contact GetContactDetails()
    {
      var user = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);
      var model = App.GetSessionValue<ResumeModel>("Career:Resume");

      var contact = new Core.Models.Career.Contact();

      if (model.IsNull())
        return null;

      if (model.ResumeContent.SeekerContactDetails.SeekerName.IsNull() || model.ResumeContent.SeekerContactDetails.SeekerName.LastName.IsNullOrEmpty())
      {
        contact.SeekerName = new Name
        {
          FirstName = user.PersonDetails.FirstName,
          MiddleName = user.PersonDetails.MiddleInitial,
          LastName = user.PersonDetails.LastName,
          SuffixId = user.PersonDetails.SuffixId
        };
      }
      else
      {
        contact.SeekerName = model.ResumeContent.SeekerContactDetails.SeekerName;
      }

      if ((model.ResumeContent.SeekerContactDetails.PostalAddress.IsNull() || model.ResumeContent.SeekerContactDetails.PostalAddress.Street1.IsNullOrEmpty()) && user.AddressDetails.IsNotNull())
      {
        contact.PostalAddress = new Address
        {
          Street1 = user.AddressDetails.Line1,
          Street2 = user.AddressDetails.Line2,
          City = user.AddressDetails.TownCity,
          StateId = user.AddressDetails.StateId,
          CountryId = user.AddressDetails.CountryId,
          CountyId = user.AddressDetails.CountyId,
          Zip = user.AddressDetails.PostcodeZip
        };
      }
      else
      {
        contact.PostalAddress = model.ResumeContent.SeekerContactDetails.PostalAddress;
      }

      if (model.ResumeContent.SeekerContactDetails.PhoneNumber.IsNullOrEmpty())
      {
        contact.PhoneNumber = new List<Phone>();

        if (user.PrimaryPhoneNumber.IsNotNull())
          contact.PhoneNumber.Add(new Phone
          {
            PhoneType = MapPhoneTypes(user.PrimaryPhoneNumber.PhoneType),
            PhoneNumber = user.PrimaryPhoneNumber.Number,
            Extention = user.PrimaryPhoneNumber.Extension
          });

        if (user.AlternatePhoneNumber1.IsNotNull())
          contact.PhoneNumber.Add(new Phone
          {
            PhoneType = MapPhoneTypes(user.AlternatePhoneNumber1.PhoneType),
            PhoneNumber = user.AlternatePhoneNumber1.Number,
            Extention = user.AlternatePhoneNumber1.Extension
          });

        if (user.AlternatePhoneNumber2.IsNotNull())
          contact.PhoneNumber.Add(new Phone
          {
            PhoneType = MapPhoneTypes(user.AlternatePhoneNumber2.PhoneType),
            PhoneNumber = user.AlternatePhoneNumber2.Number,
            Extention = user.AlternatePhoneNumber2.Extension
          });
      }
      else
      {
        contact.PhoneNumber = model.ResumeContent.SeekerContactDetails.PhoneNumber;
      }

      contact.EmailAddress = model.ResumeContent.SeekerContactDetails.EmailAddress.IsNotNullOrEmpty() ? model.ResumeContent.SeekerContactDetails.EmailAddress : user.PersonDetails.EmailAddress;
      contact.WebSite = model.ResumeContent.SeekerContactDetails.WebSite;

      return contact;
    }

    /// <summary>
    /// Maps the phone types.
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns></returns>
    private PhoneType MapPhoneTypes(PhoneTypes type)
    {
      switch (type)
      {
        case PhoneTypes.Phone:
          return PhoneType.Home;

        case PhoneTypes.Mobile:
          return PhoneType.Cell;

        case PhoneTypes.Fax:
          return PhoneType.Fax;

        // The types don't quite match up so have put the below 2 cases here for now but these may need changing or extra PhoneType values created
        case PhoneTypes.Other:
          return PhoneType.NonUS;

        default:
          return PhoneType.NonUS;
      }
    }
  }

  #region Helper classes

  public class AdditionalNumber
  {
    public string Dropdown { get; set; }
    public string Textbox { get; set; }
  }

  #endregion
}
