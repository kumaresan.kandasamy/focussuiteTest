﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="WorkHistoryList.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.WorkHistory.WorkHistoryList" %>
<div class="resumeSection">
	<div class="resumeSectionAction">
		<asp:Button ID="btnAddAnotherJob" Text="Add Another Job" runat="server" class="buttonLevel3" OnClick="btnAddAnotherJob_Click" />
		<asp:Button ID="btnWorkHistoryListSaveTop" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2" OnClick="btnWorkHistoryListSaveTop_Click" />
	</div>
	<div class="resumeSectionContent">
		<p><focus:LocalisedLabel runat="server" ID="WorkHistoryIntroductionLabel" RenderOuterSpan="False" LocalisationKey="WorkHistoryIntroduction.Label" DefaultText="Review the information below, make any changes necessary, and click 'Save & Move to Next Step' when you're satisfied everything is correct."/></p>
		<asp:UpdatePanel ID="JobListUpdatepanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<asp:Repeater ID="JobListRepeater" runat="server" OnItemDataBound="JobListRepeater_ItemDataBound" OnItemCommand="JobListRepeater_ItemCommand">
					<HeaderTemplate>
						<table class="resumeJobList">
							<tbody>
					</HeaderTemplate>
					<FooterTemplate>
							</tbody>
						</table>
					</FooterTemplate>
					<ItemTemplate>
								<tr>
									<td width="60%"><asp:Label ID="lblJobTitleEmployer" runat="server"></asp:Label></td>
                                    <td width="20%"><asp:Label runat="server" ID="lblMissingInformationRequired" CssClass="error embolden"></asp:Label></td>
									<td width="10%">
										<asp:LinkButton ID="linkEditJob" runat="server" CommandArgument='<%# Eval("JobId") %>' CommandName="Edit">
											<focus:LocalisedLabel runat="server" ID="EditJobLinkLabel" RenderOuterSpan="False" LocalisationKey="EditJobLinkLabel" DefaultText="Edit this job"/>
										</asp:LinkButton>
									</td>
									<td width="10%">
										<asp:LinkButton ID="linkDeleteJob" runat="server" CommandArgument='<%# Eval("JobId") %>' CommandName="Delete">
											<focus:LocalisedLabel runat="server" ID="DeleteJobLinkLabel" RenderOuterSpan="False" LocalisationKey="DeleteJobLinkLabel" DefaultText="Delete this job"/>
										</asp:LinkButton>
									</td>
								</tr>
					</ItemTemplate>
				</asp:Repeater>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	<div class="resumeSectionAction">
		<asp:Button ID="btnWorkHistoryListSaveBottom" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2" OnClick="btnWorkHistoryListSaveTop_Click" />
	</div>
</div>

<asp:HiddenField ID="JobValidationDummyTarget" runat="server" />
<act:ModalPopupExtender ID="JobValidationModalPopup" runat="server" TargetControlID="JobValidationDummyTarget"
	PopupControlID="JobValidationModalPanel" BackgroundCssClass="modalBackground" CancelControlID="DontDeleteJobButton" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="JobValidationModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">
	<div class="resumeConfirmDeleteJobModal">
		<focus:LocalisedLabel runat="server" ID="ConfirmDeleteJobLabel" CssClass="modalMessage" LocalisationKey="ConfirmDeleteJob.Label" DefaultText="Your resume needs at least one job to be considered complete. You may delete this one, but will need to add another one to replace it."/>
		<div class="modalButtons">
			<asp:Button ID="DontDeleteJobButton" runat="server" class="buttonLevel3" TabIndex="0" />
			<asp:Button ID="DeleteJobAndSaveNewButton" runat="server" class="buttonLevel2" OnClick="DeleteJobAndSaveNewButton_Click" />
		</div>
	</div>
</asp:Panel>
