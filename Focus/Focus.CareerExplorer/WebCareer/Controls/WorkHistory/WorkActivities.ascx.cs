﻿  #region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.Common.Helpers;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory
{
  public partial class WorkActivities : UserControlBase
	{

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region Bind Work Activities section

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
	    var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
	    if (addworkhistory.IsNotNull() && addworkhistory.OnetCode.IsNotNullOrEmpty())
	    {
		    if (addworkhistory.OnetCode != "Internship")
		    {
			    LoadTaskQuestions(addworkhistory.OnetCode);
			    skillsAccordionDiv.Visible = false;
		    }
		    else
		    {
			    selectedQuestions.Visible = false;
			    skillsAccordionDiv.Visible = true;
		      
					if (!IsPostBack)
						if (addworkhistory.EducationInternshipSkills.IsNotNullOrEmpty())
							SkillsAccordion.SelectedItems = addworkhistory.EducationInternshipSkills.Select(x=> x.Id).ToList();
		    }
		    lblWorkActivitiesInfo.Text =
			    string.Concat(
				    "<p>Choose the most appropriate answer under each of the following work activities to describe the tasks you carried out in your job as a <strong>",
				    addworkhistory.JobTitle,
				    (addworkhistory.IsMOCTitle && addworkhistory.JobTitle != addworkhistory.MOC
					     ? " (" + addworkhistory.MOC + ")"
					     : ""), "</strong>", (addworkhistory.IsMOCTitle ? " in the" : " at"), " <strong>", addworkhistory.Employer,
				    "</strong>.</p><p>Generic Job Title: <strong>", addworkhistory.GenericJobTitle, "</strong></p>");
	    }
	    ScriptManager.RegisterClientScriptInclude(this, GetType(), "resumeBuilderWorkActivitiesScriptInclude",
	                                              UrlHelper.GetCacheBusterUrl(
		                                              "~/Assets/Scripts/Focus.ResumeBuilder.WorkActivities.min.js"));
    }

    #endregion

		/// <summary>
		/// Gets or sets the CSS class to apply to each question wrapper DIV.
		/// </summary>
		[CssClassProperty]
		public string QuestionCssClass { get; set; }

		/// <summary>
		/// Gets or sets the CSS class to apply to each question prompt.
		/// </summary>
		[CssClassProperty]
		public string PromptCssClass { get; set; }

		/// <summary>
		/// Gets or sets the CSS class to apply to each question response.
		/// </summary>
		[CssClassProperty]
		public string ResponseCssClass { get; set; }

    /// <summary>
    /// Loads the task questions.
    /// </summary>
    /// <param name="socCodes">The soc codes.</param>
    protected void LoadTaskQuestions(string socCodes)
    {
      try
      {
        var soc = new ArrayList();

        foreach (var soccode in socCodes.Split(',').Where(soccode => !String.IsNullOrEmpty(soccode.Trim())))
	        soc.Add(soccode.Trim());

        var questions = new List<Question>();
        var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");

        foreach (string soccode in soc)
        {
          if (!string.IsNullOrEmpty(soccode.Trim()))
          {
            var rbQuestions = ServiceClientLocator.OccupationClient(App).ResumeBuilderQuestions(soccode);
            if (rbQuestions.Count > 0)
              questions.AddRange(rbQuestions);
            //TODO: Martha (new functionality) 
            //else if (qResponse.Description.IsNotNullOrEmpty())
            //{
            //  //For Military Specific titles redirect to Job Description page
            //  if (addWorkHistory.StatementsToUse.IsNotNull())
            //    addWorkHistory.StatementsToUse.Clear();
            //  if (addWorkHistory.KeywordsToUse.IsNotNull())
            //    addWorkHistory.KeywordsToUse.Clear();

            //  if (addWorkHistory.Statements.IsNotNull())
            //  {
            //    addWorkHistory.Statements.Clear();
            //    addWorkHistory.Statements.Add(qResponse.Description);
            //  }
            //  else
            //    addWorkHistory.Statements = new List<string>
            //  {
            //     qResponse.Description
            //  };
            //  App.SetSessionValue("Carrer:NewJobDetails", addWorkHistory);
            //  Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "jobdescription" });
            //}
          }

          addworkhistory.Questions = questions;
          panTaskQuestions.Controls.Clear();

          var responseToQuestions = new NameValueCollectionSerializable();
          if (addworkhistory.IsNotNull() && addworkhistory.ResponseToQuestions.IsNotNull() &&
              addworkhistory.ResponseToQuestions.Count != 0)
            responseToQuestions = addworkhistory.ResponseToQuestions;

          var index = 0;
          var chkMyAccomplishment = new CheckBox();
          foreach (var question in questions)
          {
            var lblTask = new Label();
            var rdbQuestionEveryDay = new RadioButton();
            var rdbQuestionSomeTimes = new RadioButton();
            var rdbQuestionNo = new RadioButton();
            var chkOptions = new CheckBoxList();
            var txtOpen = new TextBox();

						panTaskQuestions.Controls.Add(new LiteralControl("<div class=\"" + QuestionCssClass + "\"><div class=\"" + PromptCssClass + "\">"));

            lblTask.ID = "lblTask_" + index.ToString();
            lblTask.Text = question.Prompt.Trim();

            //AKV 2012.09.11 : This section can be commented and "onlyYesNoOptions = true" to keep Yes & No options. After discussion with kishore logic should be change.
            var onlyYesNoOptions = true;
            //bool onlyYesNoOptions = false;
            //if (question != null && question.IsCertificate)
            //  onlyYesNoOptions = true;

            lblTask.EnableViewState = true;

            bool isMyAccomplishmentQuestion;
            if (question.QuestionType == JobTaskTypes.Open && question.Response == "My accomplishments included:")
            {
              chkMyAccomplishment.ID = "chkMyAccomplishment";
              chkMyAccomplishment.ClientIDMode = ClientIDMode.Static;
              chkMyAccomplishment.Checked = false;
							chkMyAccomplishment.Attributes.Add("onclick", "return EnableDisableMyAccomplishment('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() + "');");
							chkMyAccomplishment.Attributes.Add("onkeypress", "return EnableDisableMyAccomplishment('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() + "');");
              panTaskQuestions.Controls.Add(chkMyAccomplishment);
              panTaskQuestions.Controls.Add(new LiteralControl("&nbsp;"));
              isMyAccomplishmentQuestion = true;
            }
            else
              isMyAccomplishmentQuestion = false;

            panTaskQuestions.Controls.Add(lblTask);
						panTaskQuestions.Controls.Add(new LiteralControl("</div><div class=\"" + ResponseCssClass + "\">"));
            switch (question.QuestionType)
            {
              case JobTaskTypes.YesNo:
                {
                  rdbQuestionEveryDay.ID = "rdbQuestionEveryDay_" + index.ToString();

                  rdbQuestionEveryDay.Text = onlyYesNoOptions ? "Yes" : "Every day";

                  rdbQuestionEveryDay.GroupName = lblTask.ID;
                  rdbQuestionEveryDay.Checked = false;
                  rdbQuestionEveryDay.EnableViewState = true;

                  if (onlyYesNoOptions)
                  {
                    rdbQuestionSomeTimes.ID = "rdbQuestionSometimes_C_" + index.ToString();
                    rdbQuestionSomeTimes.Text = "No";
                  }
                  else
                  {
                    rdbQuestionSomeTimes.ID = "rdbQuestionSometimes_" + index.ToString();
                    rdbQuestionSomeTimes.Text = "Now and then";
                  }

                  rdbQuestionSomeTimes.GroupName = lblTask.ID;
                  rdbQuestionSomeTimes.Checked = false;
                  rdbQuestionSomeTimes.EnableViewState = true;

                  if (!onlyYesNoOptions)
                  {
                    rdbQuestionNo.ID = "rdbQuestionNo_" + index.ToString();
                    rdbQuestionNo.Text = "Never";
                    rdbQuestionNo.GroupName = lblTask.ID;
                    rdbQuestionNo.Checked = true;
                    rdbQuestionNo.EnableViewState = true;
                    rdbQuestionNo.ClientIDMode = ClientIDMode.Static;
                  }
                  else
                  {
                    rdbQuestionSomeTimes.ClientIDMode = ClientIDMode.Static;
                    rdbQuestionSomeTimes.Checked = true;
                  }

                  if (responseToQuestions.GetValues(index.ToString()) != null)
                  {
                    var response = responseToQuestions.Get(index.ToString()).Split('ˆ');
                    if (response.Length > 1 && response[0] == "R")
                    {
                      switch (response[1])
                      {
                        case "0": rdbQuestionEveryDay.Checked = true; rdbQuestionSomeTimes.Checked = false; rdbQuestionNo.Checked = false; break;
                        case "1": rdbQuestionEveryDay.Checked = false; rdbQuestionSomeTimes.Checked = true; rdbQuestionNo.Checked = false; break;
                        default: rdbQuestionEveryDay.Checked = false; rdbQuestionSomeTimes.Checked = false; rdbQuestionNo.Checked = true; break;
                      }
                    }
                  }

									rdbQuestionEveryDay.Attributes.Add("onclick", "return AddYesNoInStatementList('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() + "');");
									rdbQuestionEveryDay.Attributes.Add("onkeypress", "return AddYesNoInStatementList('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() + "');");

	                if (!onlyYesNoOptions)
	                {
		                rdbQuestionSomeTimes.Attributes.Add("onclick",
			                "return AddYesNoInStatementList('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() +
											"');");
										rdbQuestionSomeTimes.Attributes.Add("onkeypress",
											"return AddYesNoInStatementList('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() +
											"');");
	                }
	                else
	                {
										rdbQuestionSomeTimes.Attributes.Add("onclick", "return RemoveYesNoFromStatementList('" + index + "');");
										rdbQuestionSomeTimes.Attributes.Add("onkeypress", "return RemoveYesNoFromStatementList('" + index + "');");
	                }

	                if (rdbQuestionEveryDay.Checked || (!onlyYesNoOptions && rdbQuestionSomeTimes.Checked))
                  {
                    var addValue = true;
                    foreach (var item in lbSelectedStatements.Items.Cast<ListItem>().Where(item => item.Text.Contains(question.Response.Trim())))
                      addValue = false;

                    if (addValue)
                      lbSelectedStatements.Items.Add(
                        new ListItem((lbSelectedStatements.Items.Count + 1) + ". " + question.Response.Trim(),
                                     index.ToString()));
                  }

                  panTaskQuestions.Controls.Add(rdbQuestionEveryDay);
                  panTaskQuestions.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
                  panTaskQuestions.Controls.Add(rdbQuestionSomeTimes);
                  if (!onlyYesNoOptions)
                  {
										rdbQuestionNo.Attributes.Add("onclick", "return RemoveYesNoFromStatementList('" + index + "');");
										rdbQuestionNo.Attributes.Add("onkeypress", "return RemoveYesNoFromStatementList('" + index + "');");
                    panTaskQuestions.Controls.Add(new LiteralControl("&nbsp;&nbsp;&nbsp;"));
                    panTaskQuestions.Controls.Add(rdbQuestionNo);
                  }

                  break;
                }

              case JobTaskTypes.MultiOption:
                {
                  var itemsToBeSelected = new ArrayList();
                  if (responseToQuestions.GetValues(index.ToString()) != null)
                  {
                    var response = responseToQuestions.Get(index.ToString()).Split('ˆ');
                    if (response.Length > 1 && response[0] == "C")
                      itemsToBeSelected = ArrayList.Adapter(response[1].Split('|'));
                  }

                  chkOptions.ID = "chkOptions_" + index.ToString();

                  var itemIndex = 0;
                  var selectedOptions = "";
                  foreach (string option in question.Options)
                  {
                    var lItem = new ListItem(option);
                    if (itemsToBeSelected.Contains(itemIndex.ToString()))
                    {
                      lItem.Selected = true;

                      if (selectedOptions != "")
                        selectedOptions += ", " + lItem.Text;
                      else
                        selectedOptions = lItem.Text;
                    }

                    lItem.Attributes.CssStyle.Add("display", "inline-block");

                    chkOptions.Items.Add(lItem);
                    itemIndex++;
                  }

                  if (itemsToBeSelected.Count > 0 && selectedOptions != "")
                  {
                    var addValue = true;
                    foreach (var item in lbSelectedStatements.Items.Cast<ListItem>().Where(item => item.Text.Contains(question.Response.Trim())))
                      addValue = false;

                    if (addValue)
                      lbSelectedStatements.Items.Add(
                        new ListItem(
                          (lbSelectedStatements.Items.Count + 1) + ". " + question.Response.Trim() + " " +
                          selectedOptions,
                          index.ToString()));
                  }

                  chkOptions.ClientIDMode = ClientIDMode.Static;
									chkOptions.Attributes.Add("onclick", "return AddRemoveMultiInStatementList('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() + "');");
									chkOptions.Attributes.Add("onkeypress", "return AddRemoveMultiInStatementList('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() + "');");
									chkOptions.RepeatDirection = RepeatDirection.Horizontal;
	                chkOptions.RepeatColumns = 5;
                  chkOptions.RepeatLayout = RepeatLayout.Flow; 
                  
                  panTaskQuestions.Controls.Add(chkOptions);

                  break;
                }
              case JobTaskTypes.Open:
                {
                  txtOpen.ID = "txtOpen_" + index.ToString();
                  txtOpen.TextMode = TextBoxMode.MultiLine;
                  txtOpen.Style.Add("height", "30px");

                  if (responseToQuestions.GetValues(index.ToString()) != null)
                  {
                    var response = responseToQuestions.Get(index.ToString()).Split('ˆ');
                    if (response.Length > 1 && response[0] == "T")
                    {
                      txtOpen.Text = response[1].Replace(question.Response, "").Trim();
                      //if (!isMyAccomplishmentQuestion)

                      var addValue = true;
                      foreach (var item in lbSelectedStatements.Items.Cast<ListItem>().Where(item => item.Text.Contains(response[1])))
                        addValue = false;
                      
                      if(addValue)
                        lbSelectedStatements.Items.Add(new ListItem((lbSelectedStatements.Items.Count + 1) + ". " + response[1], index.ToString()));
                    }
                  }

                  txtOpen.ClientIDMode = ClientIDMode.Static;

                  if (isMyAccomplishmentQuestion)
                  {
                    if (txtOpen.Text.IsNotNullOrEmpty())
                    {
                      var myAccomp = (CheckBox)panTaskQuestions.FindControl("chkMyAccomplishment");
                      if (myAccomp.IsNotNull())
                        myAccomp.Checked = true;
                      txtOpen.Enabled = true;
                    }
                    else
                      txtOpen.Enabled = false;
                  }
                  //else
                  txtOpen.Attributes.Add("onfocusout", "return AddRemoveOpenInStatementList('" + index + "','" + question.Response.Replace("'", "ˆ").Trim() + "');");

                  panTaskQuestions.Controls.Add(txtOpen);

                  break;
                }
            }

            panTaskQuestions.Controls.Add(new LiteralControl("</div></div>"));

            index++;
          }
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnWorkActivitiesSave control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnWorkActivitiesSave_Click(object sender, EventArgs e)
    {
      //if (SaveResumeTitle != null)
      //  SaveResumeTitle(this, EventArgs.Empty);
      ((ResumeWizard)this.Page).SaveResumeTitleButton_Click(sender, e);

      var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
      if (addworkhistory.IsNotNull())
      {
        var newJob = addworkhistory;

        #region update work history

        if (newJob.IsNotNull() && newJob.Questions.IsNotNull() && newJob.Questions.Count > 0 &&
            addworkhistory.OnetCode != "Internship")
        {

          newJob.Certificates = new List<string>();
          newJob.Statements = new List<string>();
          newJob.StatementsToUse = new List<string>();
          newJob.ResponseToQuestions = new NameValueCollectionSerializable();
          if (newJob.EducationInternshipSkills.IsNotNullOrEmpty())
            newJob.EducationInternshipSkills.Clear();

          foreach (Control ctrl in panTaskQuestions.Controls)
          {
            string cntrlID;

            if (ctrl.ClientID.Contains("rdbQuestionEveryDay"))
            {
              if (((RadioButton) ctrl).Checked)
              {
                cntrlID = ctrl.ClientID.Substring(ctrl.ClientID.LastIndexOf("_") + 1);
                var nodeID = Convert.ToInt32(cntrlID);

                // If the question is of type certificate then isolate them to a different category
                if (((RadioButton) ctrl).Text == "yes")
                {
                  if (newJob.Questions[nodeID].IsCertificate)
                    newJob.Certificates.Add(newJob.Questions[nodeID].Response);
                  else
                    newJob.Statements.Add(newJob.Questions[nodeID].Response);
                }
                else
                  newJob.Statements.Add(newJob.Questions[nodeID].Response);

                newJob.ResponseToQuestions.Add(cntrlID, "Rˆ0");
              }
            }

            if (ctrl.ClientID.Contains("rdbQuestionSometimes"))
            {
              if (((RadioButton) ctrl).Checked)
              {
                if (!ctrl.ClientID.Contains("rdbQuestionSometimes_C"))
                {
                  cntrlID = ctrl.ClientID.Substring(ctrl.ClientID.LastIndexOf("_") + 1);
                  var nodeID = Convert.ToInt32(cntrlID);

                  // If the question is of type certificate then isolate them to a different category
                  if (((RadioButton) ctrl).Text != "No")
                  {
                    // If count of valid statements is less than 12; append every now and then statements in the review job page
                    newJob.Statements.Add(newJob.Questions[nodeID].Response);
                  }
                  newJob.ResponseToQuestions.Add(cntrlID, "Rˆ1");
                }
              }
            }

            if (ctrl.ClientID.Contains("rdbQuestionNo"))
            {
              if (((RadioButton) ctrl).Checked)
              {
                if (ctrl.ClientID.Contains("rdbQuestionNo"))
                {
                  cntrlID = ctrl.ClientID.Substring(ctrl.ClientID.LastIndexOf("_") + 1);
                  var nodeID = Convert.ToInt32(cntrlID);
                  newJob.StatementsToUse.Add(newJob.Questions[nodeID].Response);
                }
              }
            }

            if (ctrl.ClientID.Contains("chkOptions"))
            {
              if (((CheckBoxList) ctrl).SelectedIndex != -1)
              {
                var options = "";
                var itemIndex = 0;
                var itemsSelected = "";
                var unSelectedOptions = "";

                foreach (ListItem item in ((CheckBoxList) ctrl).Items)
                {
                  if (item.Selected)
                  {
                    options += item.Text + ", ";
                    itemsSelected += itemIndex + "|";
                  }
                  else
                    unSelectedOptions += item.Text + ", ";

                  itemIndex++;
                }

                if (options.LastIndexOf(", ") > 0)
                  options = options.Substring(0, options.LastIndexOf(", ")) + ".";

                if (itemsSelected.LastIndexOf("|") > 0)
                  itemsSelected = itemsSelected.Substring(0, itemsSelected.LastIndexOf("|"));

                cntrlID = ctrl.ClientID.Substring(ctrl.ClientID.LastIndexOf("_") + 1,
                                                  ctrl.ClientID.Length - (ctrl.ClientID.LastIndexOf("_") + 1));

                var nodeID = Convert.ToInt32(cntrlID);

                newJob.Statements.Add(newJob.Questions[nodeID].Response + " " + options);
                newJob.ResponseToQuestions.Add(cntrlID, "Cˆ" + itemsSelected);

                if (!String.IsNullOrEmpty(unSelectedOptions))
                {
                  if (unSelectedOptions.LastIndexOf(", ") > 0)
                    unSelectedOptions = unSelectedOptions.Substring(0, unSelectedOptions.LastIndexOf(", ")) + ".";
                  newJob.StatementsToUse.Add(string.Format("{0} {1}", newJob.Questions[nodeID].Response,
                                                           unSelectedOptions));
                }
              }
              else
              {
                var unSelectedOptions = "";

                foreach (ListItem item in ((CheckBoxList) ctrl).Items)
                  unSelectedOptions += item.Text + ", ";

                cntrlID = ctrl.ClientID.Substring(ctrl.ClientID.LastIndexOf("_") + 1,
                                                  ctrl.ClientID.Length - (ctrl.ClientID.LastIndexOf("_") + 1));

                var nodeID = Convert.ToInt32(cntrlID);

                if (!String.IsNullOrEmpty(unSelectedOptions))
                {
                  if (unSelectedOptions.LastIndexOf(", ") > 0)
                    unSelectedOptions = unSelectedOptions.Substring(0, unSelectedOptions.LastIndexOf(", ")) + ".";
                  newJob.StatementsToUse.Add(string.Format("{0} {1}", newJob.Questions[nodeID].Response,
                                                           unSelectedOptions));
                }
              }
            }

            if (ctrl.ClientID.Contains("txtOpen"))
            {
              if (!String.IsNullOrEmpty(((TextBox) ctrl).Text))
              {
                cntrlID = ctrl.ClientID.Substring(ctrl.ClientID.LastIndexOf("_") + 1,
                                                  ctrl.ClientID.Length - (ctrl.ClientID.LastIndexOf("_") + 1));

                var openNodeID = Convert.ToInt32(cntrlID);
                var response = "";

                if (newJob.Questions[openNodeID].Response != null)
                  response = newJob.Questions[openNodeID].Response.Trim() + " ";

                newJob.Statements.Add(response + ((TextBox) ctrl).Text.Trim());
                newJob.ResponseToQuestions.Add(cntrlID, "Tˆ" + response + ((TextBox) ctrl).Text.Trim());
              }
            }
          }

        }
        else
        {
          if (SkillsAccordion.SelectedItems.IsNotNullOrEmpty())
          {
            if (newJob.EducationInternshipSkills.IsNull())
            {
              newJob.EducationInternshipSkills = new List<InternshipSkill>();
            }
            else
            {
              newJob.EducationInternshipSkills.Clear();
            }

            for (var index = 0; index < SkillsAccordion.SelectedItems.Count; index++)
              newJob.EducationInternshipSkills.Add(new InternshipSkill { Id = SkillsAccordion.SelectedItems[index], Name = SkillsAccordion.SelectedNames[index] });
            
          }
        }

        if (newJob.StepsCompleted < 3)
          newJob.StepsCompleted = 3;

        App.SetSessionValue("Career:NewJobDetails", newJob);
        Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "jobdescription" });
        #endregion
      }
    }
  }
}
