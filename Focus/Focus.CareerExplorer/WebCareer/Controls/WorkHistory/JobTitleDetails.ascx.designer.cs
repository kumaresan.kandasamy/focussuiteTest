﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory {
    
    
    public partial class JobTitleDetails {
        
        /// <summary>
        /// btnJobTitleDetailsSave control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnJobTitleDetailsSave;
        
        /// <summary>
        /// JobTitleInstructionLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel JobTitleInstructionLabel;
        
        /// <summary>
        /// JobTitleSubHeadingLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel JobTitleSubHeadingLabel;
        
        /// <summary>
        /// JobTitleSubHeadingRequiredLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel JobTitleSubHeadingRequiredLabel;
        
        /// <summary>
        /// panNormalTitles control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel panNormalTitles;
        
        /// <summary>
        /// lblGenericJobTitleInstruction control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblGenericJobTitleInstruction;
        
        /// <summary>
        /// rblOccupations control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList rblOccupations;
        
        /// <summary>
        /// OccupationsValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator OccupationsValidator;
        
        /// <summary>
        /// MOCUpdatepanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel MOCUpdatepanel;
        
        /// <summary>
        /// MOCDataPager control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataPager MOCDataPager;
        
        /// <summary>
        /// MOCListView control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListView MOCListView;
        
        /// <summary>
        /// custMOCTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator custMOCTitle;
        
        /// <summary>
        /// hiddenSelectedMOCTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hiddenSelectedMOCTitle;
        
        /// <summary>
        /// lblAlertInfo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblAlertInfo;
        
        /// <summary>
        /// JobTitleOptionsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel JobTitleOptionsLabel;
        
        /// <summary>
        /// mnuBrowseJob control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TreeView mnuBrowseJob;
        
        /// <summary>
        /// panSpecialtitles control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel panSpecialtitles;
        
        /// <summary>
        /// SpecialJobTitleLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel SpecialJobTitleLabel;
        
        /// <summary>
        /// SpecialJobTitleInstructionsLabel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Focus.Common.Controls.Server.LocalisedLabel SpecialJobTitleInstructionsLabel;
        
        /// <summary>
        /// cblSpecialTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList cblSpecialTitle;
        
        /// <summary>
        /// custSpecialTitle control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CustomValidator custSpecialTitle;
    }
}
