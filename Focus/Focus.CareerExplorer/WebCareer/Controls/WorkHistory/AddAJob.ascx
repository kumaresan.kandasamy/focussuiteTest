﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="AddAJob.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.WorkHistory.AddAJob" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<div class="resumeSection">
    <input type="hidden" id="firstTitleCheck" value="True" />
    <div class="resumeSectionAction">
        <asp:Button ID="SaveAndReturnTopButton" runat="server" class="buttonLevel3" OnClientClick="return validateAll();"
            OnClick="SaveAndReturnButton_Click" Visible="false" />
        <asp:Button ID="btnAddAJobSaveTop" runat="server" class="buttonLevel2" OnClientClick="return validateAll();"
            OnClick="btnAddAJobSaveTop_Click" />
    </div>
    <div class="resumeSectionContent">
        <p>
            <focus:LocalisedLabel runat="server" ID="AddAJobInstructionLabel" RenderOuterSpan="False"
                LocalisationKey="AddAJobInstruction.Label" DefaultText="We'd like you to describe each of your recent work experiences. Go back 10-15 years if possible.<br />Include jobs you might not want to show on your resume – you'll have the opportunity to hide them later on." />
        </p>
        <div class="collapsiblePanel cpCollapsed">
            <div class="collapsiblePanelHeader" id="collapsiblePanelHeaderAddJob">
                <div class="cpIcon">
                </div>
                <%--<span class="collapsiblePanelHeaderLabel cpHeaderControl" ><a href="#" ID="VeteranInstructionsHeadingLabel"><%= HtmlLocalise("VeteranInstructionsHeading.Label", "Special instructions for veterans, homemakers, volunteers, or those without job history")%></a></span>--%>
                <span class="collapsiblePanelHeaderLabel cpHeaderControl">
                    <focus:LocalisedLabel runat="server" ID="VeteranInstructionsHeadingLabel" RenderOuterSpan="False" />
                </span>
            </div>
            <div class="collapsiblePanelContent">
                <h5>
                    <%= HtmlLocalise("Veterans.Label", "Veterans")%></h5>
                <p>
                    <%= HtmlLocalise("VeteransInstructions.Label", "If you have served in the military, we will ask you about your military occupation in the Profile area. If you feel your military rank and service constitutes a significant portion of your work experience, you can list your rank and military occupation as a job on your resume. We will try to find questions or text that relate to civilian equivalents.")%></p>
                <h5>
                    <%= HtmlLocalise("Homemakers.Label","Homemakers")%></h5>
                <p>
                    <%= HtmlLocalise("HomemakersInstructions.Label", "If you have experience as a homemaker, care giver or stay-at-home parent, please enter 'Homemaker', 'Care Giver' or 'Home Manager' in the job title field so that we can further assist you in identifying transferrable skills valuable to #BUSINESSES#:LOWER. (We suggest you list the #BUSINESS#:LOWER for the time you spent in such roles as \"Self\".)")%></p>
                <h5>
                    <%= HtmlLocalise("Volunteer.Label","Volunteer activities")%></h5>
                <p>
                    <%= HtmlLocalise("VolunteerInstuctions.Label","Describe your volunteer activities in the Add-Ins area. However, if internships or volunteer work make up a large part of your work history, or their absence would create major time gaps in your resume, you may include them in work history.")%></p>
                <asp:Panel ID="NoJobHistoryPanel" runat="server">
                    <span id="NoJobHistorySpan" runat="server">
                        <h5>
                            <%= HtmlLocalise("JobHistory.Label","No job history to enter?")%></h5>
                        <p>
                            <asp:LinkButton ID="lnkSkipWorkHistory" runat="server" OnClick="lnkSkipWorkHistory_Click"
                                Text="Follow this link"></asp:LinkButton><%= HtmlLocalise("JumpToContact.Label"," to jump to the Contact area.")%></p>
                    </span>
                </asp:Panel>
            </div>
        </div>
        <div class="resumeSectionSubHeading">
            <focus:LocalisedLabel runat="server" ID="AddAJobSubHeadingLabel" RenderOuterSpan="False"
                LocalisationKey="AddAJobSubHeading.Label" DefaultText="1. Add a job" />
            <focus:LocalisedLabel runat="server" ID="EditAJobSubHeadingLabel" RenderOuterSpan="False"
                LocalisationKey="EditAJobSubHeading.Label" DefaultText="1. Edit a job" Visible="false" />
            &nbsp;<focus:LocalisedLabel runat="server" ID="AddAJobSubHeadingRequiredLabel" CssClass="requiredDataLegend"
                LocalisationKey="AddAJobSubHeadingRequired.Label" DefaultText="required fields" />
        </div>
        <div class="resumeAddJob">
            <div class="dataInputRow">
                <div class="dataInputGrouped ">
                    <div class="dataInputGroupedItemVertical  col-sm-8 col-md-6 clearfix">
                        <div class="col-sm-3 col-md-3 clearfix">
                            <focus:LocalisedLabel runat="server" ID="JobTitleLabel" AssociatedControlID="txtJobTitle"
                                CssClass="dataInputLabel requiredData" LocalisationKey="JobName.Label" DefaultText="Job title" />
                        </div>
                        <div class="dataInputField col-sm-9 col-md-9 clearfix">
                            <asp:TextBox ID="txtJobTitle" runat="server" ClientIDMode="Static" />
                            <ajaxtoolkit:AutoCompleteExtender ID="aceJobTitle" runat="server" CompletionSetCount="20"
                                CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem"
                                CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetJobTitlesWithMOCs" OnClientShowing=""
                                ServicePath="~/Services/AjaxService.svc" TargetControlID="txtJobTitle" CompletionInterval="1000"
                                UseContextKey="false" EnableCaching="true" MinimumPrefixLength="2" OnClientShown="fixAutoCompleteExtender">
                            </ajaxtoolkit:AutoCompleteExtender>
                            <asp:RequiredFieldValidator ID="JobTitleRequired" runat="server" ControlToValidate="txtJobTitle"
                                CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AddAJob" />
                        </div>
                    </div>
                    <div class="instructionalText instructionalTextBlock  col-sm-4 col-md-6 clearfix">
                        <focus:LocalisedLabel runat="server" ID="LocalisedLabel1" RenderOuterSpan="False"
                            LocalisationKey="JobTitleInstructions.Label" DefaultText="Standard job titles are provided for your convenience. You may still use your own job title if it's not on our list." />
                    </div>
                </div>
                <div class="dataInputGrouped col-sm-12 col-md-12 clearfix">
                    <div class="dataInputGroupedItemVertical  col-sm-8 col-md-6 clearfix">
                        <div class="col-sm-3 col-md-3 clearfix">
                            <focus:LocalisedLabel runat="server" ID="EmployerTitle" AssociatedControlID="txtEmployer"
                                CssClass="dataInputLabel requiredData" LocalisationKey="Employer.Label" DefaultText="#BUSINESS#" />
                        </div>
                        <div class="dataInputField col-sm-9 col-md-9 clearfix">
                            <asp:TextBox ID="txtEmployer" runat="server" ClientIDMode="Static"
                                MaxLength="40" />
                            <asp:Panel class="dataInputFieldSubContainer" runat="server" ClientIDMode="Static"
                                ID="hideemployer" Style="display: none;">
                                <asp:DropDownList ID="ddlEmployer" runat="server" Width="345px" ClientIDMode="Static">
                                    <asp:ListItem Value="" Text="- select branch -" />
                                </asp:DropDownList>
                            </asp:Panel>
                            <asp:RequiredFieldValidator ID="EmployerRequired" runat="server" ControlToValidate="txtEmployer"
                                CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AddAJob" />
                        </div>
                    </div>
                    <div class="instructionalText instructionalTextBlock col-md-6 clearfix">
                        <focus:LocalisedLabel runat="server" ID="JobTitleMOCInstructionsLabel" RenderOuterSpan="False"
                            LocalisationKey="JobTitleMOCInstructions.Label" DefaultText="If you are a veteran or military service member, enter your Military Occupational Classification (MOC) code to the job title field. We will populate your #BUSINESS#:LOWER with the appropriate service branches, and allowing you to select your branch and rank. We also provide work statements for many military occupations to help you describe your skills and duties for these jobs. This information will display on your resume and help us match you to civilian job openings where your military skills will transfer." />
                    </div>
                </div>
            </div>
            <asp:Panel class="dataInputRow" ID="hiderank" ClientIDMode="Static" runat="server"
                Style="display: none;">
                <div class="col-sm-8 col-md-6 clearfix">
                    <div class="col-sm-3 col-md-3 clearfix">
                        <focus:LocalisedLabel runat="server" ID="RankLabel" AssociatedControlID="ddlRank"
                            CssClass="dataInputLabel" LocalisationKey="RankLabel" DefaultText="Rank" />
                    </div>
                    <div class="dataInputField">
                        <asp:DropDownList ID="ddlRank" runat="server" Width="345px" ClientIDMode="Static">
                            <asp:ListItem Value="none" Text="- select rank -" />
                        </asp:DropDownList>
                        <asp:HiddenField ID="hdnRank" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="BranchOfServiceHidden" runat="server" ClientIDMode="Static" />
                    </div>
                </div>
                <div class="instructionalText instructionalTextBlock col-sm-4 col-md-6 clearfix">
                    <focus:LocalisedLabel runat="server" ID="MilitaryServiceInfoLabel" RenderOuterSpan="False"
                        LocalisationKey="MilitaryServiceInfo.Label" DefaultText="Additional military service information is required on the Profile tab. Your resume will be reverted to a status of Incomplete until this information has been provided." />
                </div>
            </asp:Panel>
            <div class="dataInputRow col-sm-8 col-md-6 clearfix">
                <div class="col-sm-3 col-md-3 clearfix">
                    <focus:LocalisedLabel runat="server" ID="StartDateLabel" AssociatedControlID="StartDateTextBox"
                        CssClass="dataInputLabel requiredData" LocalisationKey="StartDate.Label" DefaultText="Start date" />
                </div>
                <div class="dataInputField">
                    <span class="dataInputFieldSubContainer">
                        <asp:TextBox ID="StartDateTextBox" runat="server" ClientIDMode="Static" Width="80px" />
                        <%--<ajaxtoolkit:MaskedEditExtender ID="meStartDate" runat="server" Mask="99/99/9999"
                            TargetControlID="StartDateTextBox" ClearMaskOnLostFocus="false" PromptCharacter="_">
                        </ajaxtoolkit:MaskedEditExtender>--%>
                        <span class="instructionalText">mm/dd/yyyy</span>
                        <asp:CheckBox ID="chkCurrentlyWorkHere" Text="I currently work here" runat="server"
                            onclick="return OnDateUpdate(false);" CssClass="currentlyWorkHere" ClientIDMode="Static" />
                    </span><span class="dataInputFieldSubContainer">
                        <asp:CustomValidator ID="StartDateValidator" runat="server" ControlToValidate="StartDateTextBox"
                            SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="AddAJob"
                            ClientValidationFunction="ValidateStartDate" ValidateEmptyText="true" />
                    </span>
                </div>
            </div>
            <div id="inputRowEndDate" style="clear: left">
                <div class="dataInputRow col-sm-8 col-md-6 clearfix">
                    <div class="col-sm-3 col-md-3 clearfix">
                        <focus:LocalisedLabel runat="server" ID="EndDateLabel" AssociatedControlID="EndDateTextBox"
                            CssClass="dataInputLabel requiredData" LocalisationKey="EndDate.Label" DefaultText="End date" />
                    </div>
                    <div class="dataInputField">
                        <span class="dataInputFieldSubContainer">
                            <asp:TextBox ID="EndDateTextBox" runat="server" ClientIDMode="Static" Width="80px"/>
                           <%-- <ajaxtoolkit:MaskedEditExtender ID="meEndDate" runat="server" Mask="99/99/9999" TargetControlID="EndDateTextBox"
                                ClearMaskOnLostFocus="false" PromptCharacter="_">
                            </ajaxtoolkit:MaskedEditExtender>--%>
                            <span class="instructionalText">mm/dd/yyyy</span> </span><span class="dataInputFieldSubContainer">
                                <asp:CustomValidator ID="EndDateValidator" runat="server" ControlToValidate="EndDateTextBox"
                                    SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="AddAJob"
                                    ClientValidationFunction="ValidateEndDate" ValidateEmptyText="true" ClientIDMode="Static" />
                                <asp:CustomValidator ID="custJobDateValidator" runat="server" SetFocusOnError="true"
                                    Display="Dynamic" CssClass="error" ValidationGroup="AddAJob" ClientValidationFunction="ValidateJobDate" />
                            </span>
                    </div>
                </div>
                <div class="dataInputRow col-sm-8 col-md-6 clearfix">
                    <div class="col-sm-3 col-md-4 clearfix">
                        <focus:LocalisedLabel runat="server" ID="ReasonForLeavingLabel" AssociatedControlID="ddlReasonForLeaving"
                            CssClass="dataInputLabel requiredData" LocalisationKey="ReasonForLeaving.Label"
                            DefaultText="Reason For Leaving" />
                    </div>
                    <div class="dataInputField">
                        <asp:DropDownList ID="ddlReasonForLeaving" runat="server" Width="345px" ClientIDMode="Static">
                            <asp:ListItem>select a reason</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ReasonForLeavingRequired" runat="server" ControlToValidate="ddlReasonForLeaving"
                            CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AddAJob" />
                    </div>
                </div>
            </div>
            <div class="dataInputRow col-sm-8 col-md-6 clearfix" style="clear: left">
                <div class="col-sm-3 col-md-3 clearfix">
                    <focus:LocalisedLabel runat="server" ID="CityLabel" AssociatedControlID="txtCity"
                        CssClass="dataInputLabel requiredData" LocalisationKey="City.Label" DefaultText="Location" />
                </div>
                <div class="dataInputField">
                    <span class="inFieldLabel col-sm-9 col-md-9">
                        <focus:LocalisedLabel runat="server" ID="CityTextBoxLabel" AssociatedControlID="txtCity"
                            LocalisationKey="AddAJob.FindAJobTextBox.Label" DefaultText="City" Width="350" />
                    </span>
                    <asp:TextBox ID="txtCity" runat="server" ClientIDMode="Static" />
                    <asp:RequiredFieldValidator ID="CityRequired" runat="server" ControlToValidate="txtCity"
                        CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AddAJob" />
                </div>
            </div>
            <div class="dataInputRow col-sm-8 col-md-6 clearfix" style="clear: left">
                <div class="dataInputField dataInputFieldNoLabel col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-3 clearfix">
                    <asp:DropDownList ID="ddlState" runat="server" Width="345px" ClientIDMode="Static">
                        <asp:ListItem>select a state</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="StateRequired" runat="server" ControlToValidate="ddlState"
                        SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="AddAJob" />
                </div>
            </div>
            <div class="dataInputRow col-sm-8 col-md-6 clearfix" style="clear: left">
                <div class="dataInputField dataInputFieldNoLabel col-sm-9 col-md-10 col-sm-offset-3 col-md-offset-3 clearfix">
                    <asp:DropDownList ID="ddlCountry" runat="server" Width="345px" ClientIDMode="Static">
                        <asp:ListItem>United States</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="CountryRequired" runat="server" ControlToValidate="ddlCountry"
                        SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="AddAJob" />
                </div>
            </div>
            <div class="dataInputRow col-sm-8 col-md-6 clearfix" style="clear: left">
                <div class="col-sm-3 col-md-3 clearfix">
                    <focus:LocalisedLabel runat="server" ID="WagesLabel" AssociatedControlID="txtWages"
                        CssClass="dataInputLabel" LocalisationKey="Wages.Label" DefaultText="Wages/pay unit" />
                </div>
                <div class="dataInputField">
                    <span class="dataInputGroupedItemHorizontal" style="margin-right: 5px">
                        <asp:TextBox ID="txtWages" runat="server" Width="100px" ClientIDMode="Static" MaxLength="10" />
                        <ajaxtoolkit:FilteredTextBoxExtender ID="fteWages" runat="server" TargetControlID="txtWages"
                            FilterType="Custom" ValidChars=",.0123456789" />
                    </span><span class="dataInputGroupedItemHorizontal">
                        <asp:DropDownList ID="ddlPayUnit" runat="server" Width="230px" ClientIDMode="Static"
                            Enabled="false">
                            <asp:ListItem>Monthly</asp:ListItem>
                        </asp:DropDownList>
                    </span>
                    <asp:CustomValidator ID="PayUnitValidator" runat="server" ControlToValidate="ddlPayUnit"
                        Display="Dynamic" CssClass="error" ValidationGroup="Wages" ClientValidationFunction="validatePayUnit"
                        ValidateEmptyText="true" />
                </div>
            </div>
            <div class="dataInputRow col-sm-4 col-md-6 clearfix">
                <focus:LocalisedLabel runat="server" ID="WagesInstructionLabel" CssClass="instructionalText instructionalTextBlock"
                    LocalisationKey="WagesInstruction.Label" DefaultText="The salary information you provide will not appear in your resume and is collected only to help provide you with matches." />
            </div>
        </div>
    </div>
    <div class="resumeSectionAction" style="clear: left">
        <asp:Button ID="SaveAndReturnBottomButton" runat="server" class="buttonLevel3" OnClientClick="return validateAll();"
            OnClick="SaveAndReturnButton_Click" Visible="false" />
        <asp:Button ID="btnAddAJobSaveBottom" runat="server" class="buttonLevel2" OnClientClick="return validateAll();"
            OnClick="btnAddAJobSaveTop_Click" />
    </div>
</div>
<script type="text/javascript">
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(bindEvents);
    $(document).ready(function () {
        $('#EndDateTextBox').mask('99/99/9999');
        $('#StartDateTextBox').mask('99/99/9999');
    });
</script>
