﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory
{
  public partial class JobTitleDetails : UserControlBase
	{
    #region Page Properties
    protected string SpecialGenericTitleRequired = "";
    protected string SpecialGenericTitleLimit = "";
    protected string MOCTitleRequired = "";
    private const string SelectedOnetcode = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
      LocaliseUI();
			ApplyBranding();
    }

		/// <summary>
		/// Applies client specific branding.
		/// </summary>
		private void ApplyBranding()
		{
			mnuBrowseJob.CollapseImageUrl = UrlBuilder.MinusIcon();
			mnuBrowseJob.ExpandImageUrl = UrlBuilder.PlusIcon();
		}

    #region Bind Job Title Details section

    internal void BindStep()
    {
      try
      {
        if (!IsPostBack)
        {
					var addWorkHistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");

					if (addWorkHistory.IsNotNull() && addWorkHistory.JobTitle.IsNotNullOrEmpty())
          {
						custSpecialTitle.Enabled = panSpecialtitles.Visible = addWorkHistory.IsSpecialTitle;
						panNormalTitles.Visible = !addWorkHistory.IsSpecialTitle;

						if (addWorkHistory.IsMOCTitle)
							lblGenericJobTitleInstruction.Text = CodeLocalise("GenericJobTitleInstruction.Military.Label",
																																"Which of these jobs is most closely related to your military experience?");
						else
							lblGenericJobTitleInstruction.Text = CodeLocalise("GenericJobTitleInstruction.Civilian.Label",
							                                                  "Which of these titles best fits your job?");

						if (addWorkHistory.IsSpecialTitle)
						{
							cblSpecialTitle.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SpecialGenericTitles), null, true);

						  if (addWorkHistory.OnetCode.IsNotNullOrEmpty())
						  {
						    foreach (
						      var genericTitle in
						        addWorkHistory.OnetCode.Split(',')
						                      .Select(
						                        onetCode =>
						                        ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SpecialGenericTitles)
						                                  .FirstOrDefault(x => x.ExternalId == onetCode))
						                      .Where(genericTitle => genericTitle != null && genericTitle.Id != -1))
						    {
                  var listItem = cblSpecialTitle.Items.FindByValue(genericTitle.ExternalId);
						      if (listItem.IsNotNull())
						        listItem.Selected = true;
						     
						    }
						  }
						}

						else
						{
							MOCListView.Visible = custMOCTitle.Enabled = addWorkHistory.IsMOCTitle;

							List<OnetCode> occupations = null;

							if (addWorkHistory.IsMOCTitle)
							{
								var jobTitle = "";
								
								if (addWorkHistory.JobTitle.IsNotNullOrEmpty() && addWorkHistory.MOC.IsNotNullOrEmpty())
								{
									if (addWorkHistory.JobTitle != addWorkHistory.MOC)
										jobTitle = string.Format("{0} - {1}", addWorkHistory.MOC, addWorkHistory.JobTitle);
									else if (addWorkHistory.MOC.IsNotNullOrEmpty())
										jobTitle = addWorkHistory.MOC;
								}
								
								if(jobTitle.IsNotNullOrEmpty())
									occupations = ServiceClientLocator.OccupationClient(App).MOCJobTitles(jobTitle, 0);

								if (occupations.IsNotNullOrEmpty())
								{
									App.SetSessionValue("Career:MOCTitles", occupations);
									MOCListView.DataSource = occupations;
									MOCListView.DataBind();
									MOCDataPager.Visible = occupations.Count <= 10 ? false : true;
								}
								else
								{
									occupations = null; // set null to confirm the UI display of 2* categories.
									MOCDataPager.Visible = false;
								}

								if (SelectedOnetcode.IsNotNullOrEmpty())
									MOCListView_PagePropertiesChanging(null, new PagePropertiesChangingEventArgs(0, 10));
							}
							else
							{
								occupations = ServiceClientLocator.OccupationClient(App).GenericJobTitles(addWorkHistory.JobTitle, 5);
								LoadFunctionalTitles(occupations, addWorkHistory.OnetCode);

							  rblOccupations.SelectedIndex = App.Settings.Theme == FocusThemes.Education &&
							                                 (IsInternJobTitle(addWorkHistory.JobTitle) ||
							                                  (addWorkHistory.EducationInternshipSkills.IsNotNullOrEmpty()))
							                                   ? occupations.Count
							                                   : 0;

							  OccupationsValidator.Enabled = true;
							}

							if (occupations.IsNotNull() && occupations.Count > 0)
								lblAlertInfo.Visible = false;
							else
							{
								lblAlertInfo.Text = CodeLocalise("AlertInfo.Label",
								                                 "Please browse more choices to find a generic jobtitle for <strong>" +
								                                 addWorkHistory.JobTitle + "</strong> at <strong>" + addWorkHistory.Employer +
								                                 "</strong>.", new { addWorkHistory.JobTitle, addWorkHistory.Employer });
								lblAlertInfo.Visible = true;
							}

              var onetJobFamilies = ServiceClientLocator.SearchClient(App).GetOnets(true).Select(onet => onet.JobFamilyId).Distinct();
              var jobFamilies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobFamily).Where(family => onetJobFamilies.Contains(family.Id)).ToList();

              if (jobFamilies.Any())
              {
								mnuBrowseJob.Visible = true;
								LoadOnetJobFamilies(jobFamilies);
							}
							else
								mnuBrowseJob.Visible = false;
						}
          }
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    #endregion

    /// <summary>
    /// Handles the PagePropertiesChanging event of the MOCListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.PagePropertiesChangingEventArgs"/> instance containing the event data.</param>
    protected void MOCListView_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
      var occupations = App.GetSessionValue<List<OnetCode>>("Career:MOCTitles");

      int startIndex = e.StartRowIndex, maxRows = e.MaximumRows;
      if (sender.IsNull() && SelectedOnetcode.IsNotNullOrEmpty() && occupations.IsNotNull())
      {
        var selIndex = occupations.FindIndex(x => x.Code == SelectedOnetcode);
        if (selIndex > 0)
          startIndex = (selIndex / maxRows) * maxRows;
      }

      //set current page startindex, max rows and rebind to false
      MOCDataPager.SetPageProperties(startIndex, maxRows, false);

      //rebind List View
      if (occupations.IsNotNull())
      {
        MOCListView.DataSource = occupations;
        MOCListView.DataBind();
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the MOCListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.ListViewItemEventArgs"/> instance containing the event data.</param>
    protected void MOCListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var onet = (OnetCode)e.Item.DataItem;

      var mocDetails = string.Format("{0}:{1}:{2}:{3}", onet.Occupation.Replace("'", "ˆ").Trim(), onet.Code, onet.RonetCode, onet.StarRating);
      var mocLiteral = (Literal)e.Item.FindControl("ltMOCTitle");
      mocLiteral.Text = string.Format("<input type=\"radio\" name=\"moctitles\" onclick=\"SelectedMOCTitle('{0}')\"{1}/>{2}", mocDetails, (SelectedOnetcode.IsNotNullOrEmpty() && SelectedOnetcode == onet.Code ? " checked=\"checked\"" : ""), onet.Occupation);
      if (SelectedOnetcode.IsNotNullOrEmpty() && SelectedOnetcode == onet.Code)
        hiddenSelectedMOCTitle.Value = mocDetails;
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			SpecialGenericTitleRequired = CodeLocalise("SpecialGenericTitle.RequiredErrorMessage", "Select at least one title");
			SpecialGenericTitleLimit = CodeLocalise("SpecialGenericTitle.ErrorMessage", "Maximum of two titles are allowed");
			MOCTitleRequired = CodeLocalise("MOCTitle.RequiredErrorMessage", "Generic job title is required");
		}

    /// <summary>
    /// Handles the Click event of the btnJobTitleDetailsSave control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnJobTitleDetailsSave_Click(object sender, EventArgs e)
    {
      ((ResumeWizard)Page).SaveResumeTitleButton_Click(sender, e);

      var newJob = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
      if (!newJob.IsNotNull()) return;
      var starRating = 0;
      string onetcode = "", ronetcode = "", genericTitle = "";
      if (newJob.IsSpecialTitle)
      {
        var onetCodes = new List<string>();
        var genericTitles = new List<string>();
        foreach (var item in cblSpecialTitle.Items.Cast<ListItem>().Where(item => item.Selected))
        {
          onetCodes.Add(item.Value);
          genericTitles.Add(item.Text);
        }

        onetcode = onetCodes.Count > 0 ? string.Join<string>(",", onetCodes) : ",";
        genericTitle = string.Join<string>(",", genericTitles);
      }
      else
      {
        if (newJob.IsMOCTitle)
        {
          if (hiddenSelectedMOCTitle.Value.IsNotNullOrEmpty())
          {
            var onetronet = hiddenSelectedMOCTitle.Value.Split(':');
            genericTitle = onetronet[0].Replace("ˆ", "'");
            onetcode = onetronet[1];
            ronetcode = onetronet[2];
            starRating = onetronet[3].AsInt32();
          }
        }
        else if (rblOccupations.SelectedItem != null)
        {
          genericTitle = rblOccupations.SelectedItem.Text;
          onetcode = rblOccupations.SelectedItem.Value;
        }
      }

      newJob.GenericJobTitle = genericTitle;
      newJob.OnetCode = onetcode;
      newJob.ROnetCode = ronetcode;
      newJob.StarRating = starRating;

      if (SelectedOnetcode.IsNotNullOrEmpty() && SelectedOnetcode != newJob.OnetCode)
      {
        newJob.ResponseToQuestions = null;
        newJob.Certificates = null;
        newJob.Statements = null;
        newJob.StatementsToUse = null;
      }

      //if(JobTitleInstructionLabel.)
      App.SetSessionValue("Career:NewJobDetails", newJob);
      Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "workactivities" });
    }


    /// <summary>
    /// Loads the functional titles.
    /// </summary>
    /// <param name="onets">The onets.</param>
    /// <param name="selectedOnet">The selected onet.</param>
    private void LoadFunctionalTitles(IEnumerable<OnetCode> onets, string selectedOnet = null)
    {
      foreach (OnetCode onet in onets)
        if (selectedOnet.IsNotNull() && selectedOnet == onet.Code)
          rblOccupations.Items.Add(new ListItem(onet.Occupation, onet.Code, true));
        else
          rblOccupations.Items.Add(new ListItem(onet.Occupation, onet.Code));

      if(App.Settings.Theme == FocusThemes.Education)
        rblOccupations.Items.Add(new ListItem(CodeLocalise("JobTitles.Internship", "Internship")));
    }

    /// <summary>
    /// Loads the onet job families.
    /// </summary>
    /// <param name="jobFamilies">The job families.</param>
    private void LoadOnetJobFamilies(IEnumerable<LookupItemView> jobFamilies)
    {
      mnuBrowseJob.Nodes.Clear();

      foreach (var jFamily in jobFamilies)
      {
        var onetNode = new TreeNode(jFamily.Text)
        {
          Value = jFamily.Id.ToString(),
          Text = jFamily.Text,
          SelectAction = TreeNodeSelectAction.SelectExpand
        };
        onetNode.ChildNodes.Add(new TreeNode());
        mnuBrowseJob.Nodes.Add(onetNode);
      }

      mnuBrowseJob.CollapseAll();
    }

    /// <summary>
    /// Handles the TreeNodeExpanded event of the mnuBrowseJob control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.TreeNodeEventArgs"/> instance containing the event data.</param>
    protected void mnuBrowseJob_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
      try
      {
        var onetOccups = ServiceClientLocator.SearchClient(App).GetOnets(Convert.ToInt64(e.Node.Value));

        if (onetOccups != null && onetOccups.Count > 0)
        {
          e.Node.ChildNodes.Clear();

          foreach (var onetOccup in onetOccups)
          {
            var onetNode = new TreeNode(onetOccup.Occupation)
                             {
                               Value = "ˆ" + onetOccup.OnetCode, Text = onetOccup.Occupation,
                               ImageUrl = UrlBuilder.Bullet()
                             };
            e.Node.ChildNodes.Add(onetNode);
          }

          e.Node.Expand();
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the TreeNodeCollapsed event of the mnuBrowseJob control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.TreeNodeEventArgs"/> instance containing the event data.</param>
    protected void mnuBrowseJob_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
    { }

    /// <summary>
    /// Handles the SelectedNodeChanged event of the mnuBrowseJob control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void mnuBrowseJob_SelectedNodeChanged(object sender, EventArgs e)
		{
			if (mnuBrowseJob.SelectedNode.Value.Contains("ˆ"))
			{
				var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
				string onetcode = mnuBrowseJob.SelectedNode.Value.Replace("ˆ", "");
				addworkhistory.OnetCode = onetcode;
				addworkhistory.GenericJobTitle = mnuBrowseJob.SelectedNode.Text;

				if (addworkhistory.StepsCompleted < 2)
					addworkhistory.StepsCompleted = 2;
				App.SetSessionValue("Career:NewJobDetails", addworkhistory);

				Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "workactivities" });
			}
		}

    /// <summary>
    /// Checks if job title contains intern words
    /// </summary>
    /// <param name="jobTitle">The job title.</param>
    /// <returns>
    /// True or false
    /// </returns>
    private bool IsInternJobTitle(string jobTitle)
    {
      var internshipWords = new List<string>
		                          {
		                            CodeLocalise("JobTitle.Intern", "intern"),
		                            CodeLocalise("JobTitle.Interns", "interns"),
		                            CodeLocalise("JobTitle.Internship", "internship"),
		                            CodeLocalise("JobTitle.Internship", "internships"),
		                            CodeLocalise("JobTitle.Apprentice", "apprentice")
		                          };

      return internshipWords.Any(internshipWord => Regex.IsMatch(jobTitle, string.Concat(@"\b", internshipWord, @"\b"), RegexOptions.IgnoreCase));
    }
	}

}
