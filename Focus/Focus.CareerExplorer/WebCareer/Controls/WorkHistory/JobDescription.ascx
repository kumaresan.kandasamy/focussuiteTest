﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="JobDescription.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.WorkHistory.JobDescription" %>
<%@ Register src="ManyHatsModal.ascx" tagPrefix="uc" tagName="ManyHatsModal" %>
<div class="resumeSection">
	<div class="resumeSectionAction">
		<asp:Button ID="btnSaveAnotherJob" runat="server" class="buttonLevel3" ValidationGroup="JobDescription" OnClick="btnSaveAnotherJob_Click" />
		<asp:Button ID="btnJobDescriptionSave" runat="server" class="buttonLevel2" ValidationGroup="JobDescription" OnClick="btnJobDescriptionSave_Click" />
		<asp:Button ID="SaveAndReturnButton" runat="server" class="buttonLevel2" OnClick="SaveAndReturnButton_Click" Visible="false" />
	</div>
	<div class="resumeSectionContent">
		<div class="resumeSectionSubHeading">
			<focus:LocalisedLabel runat="server" ID="JobDescriptionSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="JobDescriptionSubHeading.Label" DefaultText="4. Job description"/>
			<focus:LocalisedLabel runat="server" ID="EditJobDescriptionSubHeadingLabel" RenderOuterSpan="False" LocalisationKey="EditJobDescriptionSubHeading.Label" DefaultText="2. Job description" Visible="false" />
			&nbsp;<focus:LocalisedLabel runat="server" ID="JobDescriptionSubHeadingRequiredLabel" CssClass="requiredDataLegend" LocalisationKey="JobDescriptionSubHeadingRequired.Label" DefaultText="required fields"/>
		</div>
        <div class="resumeJobDescription clearfix col-sm-12">
            <div class="resumeJobDescriptionOptions col-sm-5 col-sm-push-6 col-md-4  col-md-push-7" id="ManyHatsDiv">
                <span class="promoStyle4 smallSize resumeJobDescriptionInstructions">
                    <focus:LocalisedLabel runat="server" ID="ManyHatsLabel" LocalisationKey="ManyHats.Label" DefaultText="Did you wear many hats at this job?"/><br/>
                    <asp:LinkButton runat="server" ID="OtherJobActivitesLink" OnClick="OtherJobActivitesLink_Clicked"></asp:LinkButton>
                </span>
            </div>
            <div class="resumeJobDescriptionSelected col-sm-6 col-sm-pull-5  col-md-7 col-md-pull-4 resumeJustify" style="font-size: 16px"><focus:LocalisedLabel runat="server" ID="JobDescriptionInstructionLabel" LocalisationKey="JobDescriptionInstruction.Label" DefaultText="Edit your job description to personalize it. Your description will be even better if you add very specific descriptions of what you did, initiatives you led, and your particular accomplishments and contributions. To help you, we've included a list of keywords and statements you may select to add to your description."/></div>
        </div>
		<div class="resumeJobDescription clearfix">
			<span class="resumeJobDescriptionSelected col-xs-12 col-sm-6 col-md-7 ">
				<asp:TextBox ID="txtJobDescription" runat="server" TextMode="MultiLine" Rows="25" ClientIDMode="Static" />
				<asp:RequiredFieldValidator ID="DescriptionRequired" runat="server" ControlToValidate="txtJobDescription" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="JobDescription" />
			</span>
			<div class="resumeJobDescriptionOptions col-sm-5 col-md-4">
				<h5><focus:LocalisedLabel runat="server" ID="AdditionalJobDetailsHeadingLabel" RenderOuterSpan="False" LocalisationKey="AdditionalJobDetailsHeading.Label" DefaultText="Additional jobs details"/></h5>
				<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="AdditionalJobDetailsInstructionLabel" RenderOuterSpan="False" LocalisationKey="AdditionalJobDetailsInstruction.Label" DefaultText="Other skills or knowledge sets for this job include:"/></p>
				<div class="collapsiblePanel cpExpanded">
					<div class="collapsiblePanelHeader">
						<div class="cpIcon"></div>
						<focus:LocalisedLabel runat="server" ID="JobDescriptionKeywordsPanelLabel" CssClass="collapsiblePanelHeaderLabel cpHeaderControl" LocalisationKey="JobDescriptionKeywordsPanel.Label" DefaultText="Keywords"/>
					</div>
					<div class="collapsiblePanelContent">
						<asp:CheckBoxList ID="chkKeywords" runat="server" ClientIDMode="Static" RepeatLayout="Flow" RepeatDirection="Vertical"></asp:CheckBoxList>
						<asp:Label ID="lblKeywords" runat="server" Visible="false" CssClass="error embolden" />
					</div>
				</div>
        <asp:PlaceHolder ID="StatementsPlaceHolder" runat="server">
				  <div class="collapsiblePanel cpCollapsed">
					  <div class="collapsiblePanelHeader">
						  <div class="cpIcon"></div>
						  <focus:LocalisedLabel runat="server" ID="JobDescriptionStatementsPanelLabel" CssClass="collapsiblePanelHeaderLabel cpHeaderControl" LocalisationKey="JobDescriptionStatementsPanel.Label" DefaultText="Statements"/>
					  </div>
					  <div class="collapsiblePanelContent">
						  <asp:CheckBoxList ID="chkStatements" runat="server" ClientIDMode="Static" RepeatLayout="Flow" RepeatDirection="Vertical"></asp:CheckBoxList>
						  <asp:Label ID="lblStatements" runat="server" Visible="false" CssClass="error embolden" />
					  </div>
				  </div>
        </asp:PlaceHolder>
        <div style="float:left">
				  <button class="buttonLevel2 buttonLeft" onclick="return AddSelectedStatements();">&#171; Add</button>
        </div>
        <div style="float:right">
          <asp:Button ID="RefreshButton" runat="server" class="buttonLevel2" OnClick="RefreshButton_Click" />
        </div>
        <div style="clear:both"></div>
			</div>
		</div>
	</div>
</div>

<asp:HiddenField ID="JobValidationDummyTarget" runat="server" />
<act:ModalPopupExtender ID="JobValidationModalPopup" runat="server" TargetControlID="JobValidationDummyTarget"
	PopupControlID="JobValidationModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="JobValidationModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">
	<div class="resumeJobValidationModal">
		<asp:Label ID="lblJobValidationInfo" runat="server" CssClass="modalMessage"></asp:Label>
		<div class="modalButtons">
			<asp:Button ID="DoNotAddJobButton" runat="server" class="buttonLevel3" TabIndex="0" OnClick="DoNotAddJobButton_Click" />
			<asp:Button ID="AddAnotherJobButton" runat="server" class="buttonLevel2" OnClick="AddAnotherJob" />
		</div>
	</div>
</asp:Panel>
<uc:ManyHatsModal runat="server" ID="ManyHatsModalWindow" Visible="False" OnManyHatsDescriptionsAdded="ManyHatsModalWindow_ManyHatsDescriptionsAdded" />