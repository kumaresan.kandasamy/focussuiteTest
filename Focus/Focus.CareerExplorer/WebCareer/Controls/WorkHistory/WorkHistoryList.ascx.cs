﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core.Views;
using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory
{
  public partial class WorkHistoryList : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      
    }

    #region Bind Work History List section

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
      if (!IsPostBack)
      {
        var model = App.GetSessionValue<ResumeModel>("Career:Resume", null);

        if (model.ResumeContent.ExperienceInfo.Jobs.Count > 0)
        {
          if (model.ResumeContent.ExperienceInfo.Jobs.Count > 1)
          {
            JobListRepeater.DataSource = model.ResumeContent.ExperienceInfo.Jobs.OrderByDescending(j => j.JobEndDate ?? DateTime.MaxValue).ThenByDescending(j => j.JobStartDate ?? DateTime.MinValue);
            App.SetSessionValue("Career:Resume", model);
          }
          else
            JobListRepeater.DataSource = model.ResumeContent.ExperienceInfo.Jobs;

          JobListRepeater.DataBind();

          JobListUpdatepanel.Update();
        }
        SetButtonAccessibility(model.ResumeContent.ExperienceInfo.AllJobsComplete);
      }
    }

    #endregion

    /// <summary>
    /// Handles the ItemCommand event of the JobListRepeater control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
    protected void JobListRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
      //int selectedRow = e.Item.ItemIndex;
      var selectedJobId = e.CommandArgument.AsNullableGuid();
      var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
      var selectedJob = currentResume.ResumeContent.ExperienceInfo.Jobs.Where(x => x.JobId == selectedJobId).FirstOrDefault();

      switch (e.CommandName)
      {
        case "Edit":
          {
            var editJob = new AddWorkHistory
            {
              JobId = selectedJob.JobId,
              IsNewJob = false,

              MOC = selectedJob.MilitaryOccupationCode,
              JobTitle = selectedJob.Title,
              IsMOCTitle = selectedJob.MilitaryOccupationCode.IsNotNullOrEmpty(),

              Employer = selectedJob.Employer,
              RankId = selectedJob.RankId,
							BranchOfServiceId = selectedJob.BranchOfServiceId,
              IsPresentJob = selectedJob.IsCurrentJob.HasValue && (bool)selectedJob.IsCurrentJob,
              StartDate = selectedJob.JobStartDate,
              EndDate = selectedJob.JobEndDate,
              City = selectedJob.JobAddress.City,
              StateId = selectedJob.JobAddress.StateId,
              StateName = selectedJob.JobAddress.StateName,
              CountryId = selectedJob.JobAddress.CountryId,
              CountryName = selectedJob.JobAddress.CountryName,
              JobDescription = selectedJob.Description,
              Statements = selectedJob.Description.IsNotNullOrEmpty() ? selectedJob.Description.Split(new string[] { "\n", "* " }, StringSplitOptions.RemoveEmptyEntries).ToList() : null,
              OnetCode = selectedJob.OnetCode,
              Wage = selectedJob.Salary,
              PayFrequencyId = selectedJob.SalaryFrequencyId,
              EducationInternshipSkills = selectedJob.InternshipSkills,
              ReasonForLeaving = selectedJob.JobReasonForLeaving.AsEnum<ReasonForLeaving>(ReasonForLeaving.None)
            };

            App.SetSessionValue("Career:NewJobDetails", editJob);
            Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "addajob" });
          }
          break;

        case "Delete":
          {
            if (currentResume.ResumeContent.ExperienceInfo.Jobs.Count != 1)
            {
              currentResume.ResumeContent.ExperienceInfo.Jobs.Remove(selectedJob);
              currentResume.ResumeContent.ExperienceInfo.IsExperienceInfoUpdated = true;
              Utilities.UpdateOnetROnetInResume(currentResume);

              // Save if we are deleting otherwise it gets readded!
              if (currentResume.ResumeMetaInfo.ResumeId.GetValueOrDefault() != 0)
              {
                ResetCompletionStatus(currentResume);
								
                currentResume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume);
              }

              App.SetSessionValue("Career:Resume", currentResume);
              JobListRepeater.DataSource = currentResume.ResumeContent.ExperienceInfo.Jobs;
              JobListRepeater.DataBind();
            }
            else
            {
              //lblJobValidationInfo.Text = CodeLocalise("LastJobDeleteMSG.Label", "Your resume needs at least one job to be considered complete. You may delete this one, but will need to add another one to replace it.");
              DontDeleteJobButton.Text = CodeLocalise("Global.Cancel.Text", "Cancel");
              DeleteJobAndSaveNewButton.Text = CodeLocalise("DeleteLastJob.Text", "Delete this job and add a new one");
              JobValidationModalPopup.Show();
            }
          }
          break;
      }
      SetButtonAccessibility(currentResume.ResumeContent.ExperienceInfo.AllJobsComplete);
    }

    private void ResetCompletionStatus(ResumeModel currentResume)
    {
      if (currentResume.ResumeMetaInfo.ResumeCreationMethod.IsIn(ResumeCreationMethod.Upload, ResumeCreationMethod.CopyPaste) && currentResume.ResumeContent.ExperienceInfo.AllJobsComplete && currentResume.ResumeMetaInfo.CompletionStatus == ResumeCompletionStatuses.None)
      {
        // Uploaded and pasted resumes have a more complete resume so set the status accordingly
        currentResume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.WorkHistory;
      }
    }

    /// <summary>
    /// Handles the Click event of the DeleteJobAndSaveNewButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void DeleteJobAndSaveNewButton_Click(object sender, EventArgs e)
    {
      try
      {
        var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume", null);
        currentResume.ResumeContent.ExperienceInfo.Jobs.RemoveAt(0);
        currentResume.ResumeContent.ExperienceInfo.IsExperienceInfoUpdated = true;

        var newJob = new AddWorkHistory { IsNewJob = true };
        App.SetSessionValue("Career:NewJobDetails", newJob);

				long? resumeId = 0;
				
				Utilities.UpdateOnetROnetInResume(currentResume);

        if (currentResume.ResumeMetaInfo.ResumeId.GetValueOrDefault() != 0)
        {
          ResetCompletionStatus(currentResume);
          var resume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume);
          resumeId = resume.ResumeMetaInfo.ResumeId;
          if (!App.UserData.HasDefaultResume || App.UserData.DefaultResumeId == resumeId)
          {
            if (!App.UserData.HasDefaultResume)
              currentResume.ResumeMetaInfo = ServiceClientLocator.ResumeClient(App).GetResumeInfo(resumeId);
          }
        }
        else
          resumeId = App.GetSessionValue<long?>("Career:ResumeID");

				if (!currentResume.ResumeMetaInfo.ResumeId.HasValue)
					currentResume.ResumeMetaInfo.ResumeId = resumeId;

				App.SetSessionValue("Career:ResumeID", resumeId);
				App.SetSessionValue("Career:Resume", currentResume);

				Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "addajob" });
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the JobListRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void JobListRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        return;

      var job = (Job)e.Item.DataItem;

      var jobTitle = (Label)e.Item.FindControl("lblJobTitleEmployer");
      jobTitle.Text = (!String.IsNullOrEmpty(job.Title) ? job.Title.Trim() + (job.MilitaryOccupationCode.IsNotNullOrEmpty() && job.MilitaryOccupationCode != job.Title ? " (" + job.MilitaryOccupationCode + ")" : "") : "N/A") + " - " + (!String.IsNullOrEmpty(job.Employer) ? job.Employer.Trim() : "N/A");

      var missingInfoLabel = (Label)e.Item.FindControl("lblMissingInformationRequired");
      missingInfoLabel.Visible = !job.IsComplete;
      if (!job.IsComplete)
      {
        missingInfoLabel.Text = CodeLocalise("lblMissingInformationRequired.Text", "Missing required information");
      }
    }

    /// <summary>
    /// Handles the Click event of the btnWorkHistoryListSaveTop control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnWorkHistoryListSaveTop_Click(object sender, EventArgs e)
    {
      try
      {
        var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume", null);

        var clonedResume = currentResume.CloneObject();
        var resumeId = (currentResume.IsNotNull() && currentResume.ResumeMetaInfo.IsNotNull()) ? currentResume.ResumeMetaInfo.ResumeId : 0;

        currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.WorkHistory;

        Utilities.UpdateOnetROnetInResume(currentResume);

        var resumeNameUpdated = false;
        var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
        if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
        {
          currentResume.ResumeMetaInfo.ResumeName = resumeName;
          resumeNameUpdated = true;
        }

				if (currentResume.ResumeContent.ExperienceInfo.IsExperienceInfoUpdated || Utilities.IsObjectModified(clonedResume, currentResume))
        {
          var resume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);
          resumeId = resume.ResumeMetaInfo.ResumeId;
          if (!App.UserData.HasDefaultResume || App.UserData.DefaultResumeId == resumeId)
          {
            if (!App.UserData.HasDefaultResume)
              currentResume.ResumeMetaInfo = ServiceClientLocator.ResumeClient(App).GetResumeInfo(resumeId);

            Utilities.UpdateUserContextUserInfo(currentResume.ResumeMetaInfo, ResumeCompletionStatuses.WorkHistory);
          }
        }

        if (!App.UserData.HasDefaultResume)
          App.UserData.DefaultResumeId = resumeId;

        if (!currentResume.ResumeMetaInfo.ResumeId.HasValue)
          currentResume.ResumeMetaInfo.ResumeId = resumeId;

        App.SetSessionValue("Career:ResumeID", resumeId);
        App.SetSessionValue("Career:Resume", currentResume);
        Response.RedirectToRoute("ResumeWizardTab", new { Tab = "contact" });
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnAddAnotherJob control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnAddAnotherJob_Click(object sender, EventArgs e)
    {
      var newJob = new AddWorkHistory { IsNewJob = true };
      App.SetSessionValue("Career:NewJobDetails", newJob);
      Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "addajob" });
    }

    /// <summary>
    /// Sets the button accessibility depending on whether there are jobs still to complete
    /// </summary>
    /// <param name="allJobsComplete">if set to <c>true</c> [all jobs complete].</param>
    private void SetButtonAccessibility(bool allJobsComplete)
    {
      if (allJobsComplete)
      {
        btnWorkHistoryListSaveTop.Attributes.Add("class", "buttonLevel2");
        btnWorkHistoryListSaveBottom.Attributes.Add("class", "buttonLevel2");
        btnWorkHistoryListSaveTop.Attributes.Remove("disabled");
        btnWorkHistoryListSaveBottom.Attributes.Remove("disabled");
      }
      else
      {
        btnWorkHistoryListSaveTop.Attributes.Add("class", "buttonLevel2 greyedOut");
        btnWorkHistoryListSaveBottom.Attributes.Add("class", "buttonLevel2 greyedOut");
        btnWorkHistoryListSaveTop.Attributes.Add("disabled", "true");
        btnWorkHistoryListSaveBottom.Attributes.Add("disabled", "true");
      }
    }
	}
}
