﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManyHatsModal.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.WorkHistory.ManyHatsModal" %>
<%@Register src="AdditionalWorkHistory/JobSelectorStep1.ascx" tagPrefix="uc" tagName="JobSelectorStep1" %>
<%@ Register src="AdditionalWorkHistory/JobTitleSpecifics.ascx" tagPrefix="uc"  tagName="JobTitleSpecifics"%>
<%@ Register src="AdditionalWorkHistory/Activities.ascx" tagPrefix="uc"  tagName="Activities"%>
<asp:HiddenField ID="ManyHatsDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ManyHatsModalWindow" runat="server" BehaviorID="ManyHatsModalWindow" TargetControlID="ManyHatsDummyTarget" PopupControlID="ManyHatsPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll"/>
<asp:Panel runat="server" ID="ManyHatsPanel" CssClass="modal" >
    <input type="image" id="ManyHatsCloseIcon" src="<%= UrlBuilder.Close() %>" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" onclick="$find('ManyHatsModalWindow').hide(); return false;" />
    <uc:JobSelectorStep1 ID="Step1" runat="server" OnNextStepClicked="StepCompleted_NextClicked" />
    <uc:JobTitleSpecifics ID="Step2" runat="server" OnNextStepClicked="StepCompleted_NextClicked" OnTreePostBackRequest="PostBack_Triggerd" />
    <uc:Activities ID="Step3" runat="server" OnNextStepClicked="StepCompleted_NextClicked" QuestionCssClass="workActivityQuestion" PromptCssClass="workActivityQuestionPrompt" ResponseCssClass="otherworkActivityQuestionAnswer" />
</asp:Panel>