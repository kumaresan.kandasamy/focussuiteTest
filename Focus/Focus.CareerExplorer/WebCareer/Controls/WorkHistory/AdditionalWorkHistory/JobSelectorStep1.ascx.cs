﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Views;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory.AdditionalWorkHistory
{
  public partial class JobSelectorStep1 : UserControlBase
	{
    
    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack) Localise();
    }

    /// <summary>
    /// Shows this instance.
    /// </summary>
    public void Show()
    {
      Reset();
      Visible = true;
    }

    /// <summary>
    /// Unbinds the specified work history.
    /// </summary>
    /// <param name="workHistory">The work history.</param>
    public void Unbind(AddWorkHistory workHistory)
    {
      workHistory.JobTitle = JobTitleTextBox.TextTrimmed();
      
      var spTitles = (List<LookupItemView>)ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.UnclassifiedJobs);

      if (spTitles.IsNotNull() && spTitles.Exists(x => (x.Text.ToLower() == workHistory.JobTitle.ToLower() || (x.Text.Contains(",") && ((x.Text.Split(',')[0].IsNotNullOrEmpty() && x.Text.Split(',')[0] == workHistory.JobTitle.ToLower()) || (x.Text.Split(',')[1].IsNotNullOrEmpty() && x.Text.Split(',')[1] == workHistory.JobTitle.ToLower()))))))
        workHistory.IsSpecialTitle = true;
      else
        workHistory.IsSpecialTitle = false;

      if (!workHistory.IsSpecialTitle)
      {
        var mocTitles = ServiceClientLocator.OccupationClient(App).GetMilitaryOccupationJobTitles(workHistory.JobTitle, 250);

        if (mocTitles.Count > 0)
        {
          MilitaryOccupationJobTitleViewDto selectedMOC = null;

          if ((selectedMOC = mocTitles.FirstOrDefault(x => x.JobTitle.Trim().ToLower() == workHistory.JobTitle.ToLower())).IsNotNull())
          {
            workHistory.MOC = selectedMOC.JobTitle.Substring(0, selectedMOC.JobTitle.IndexOf(" - "));
            workHistory.JobTitle = selectedMOC.JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ") + 3);
            workHistory.IsMOCTitle = true;
          }
          else
          {
            selectedMOC = mocTitles.First();
            workHistory.JobTitle = selectedMOC.JobTitle.Substring(0, selectedMOC.JobTitle.IndexOf(" - "));
            if (mocTitles.Count() == 1)
              workHistory.JobTitle = selectedMOC.JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ") + 3);
            else
            {
              var branchOfServiceId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MilitaryBranchesOfService).Where(x => x.Text.ToLower() == workHistory.Employer.ToLower()).Select(x => x.Id).FirstOrDefault();

              if (branchOfServiceId.IsNotNull() && branchOfServiceId > 0)
              {
                var mocs = mocTitles.Where(x => x.BranchOfServiceId == branchOfServiceId).ToList();

                if (mocs.IsNotNullOrEmpty() && mocs.Count == 1)
                  workHistory.JobTitle = mocs.First().JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ") + 3);
              }
            }
            workHistory.IsMOCTitle = true;
          }
        }

      }
    }


    protected void GoButton_Clicked(object sender, EventArgs e)
    {
      OnNextStepClicked(new CommandEventArgs("JobSelectorStep1", string.Empty));
    }

    /// <summary>
    /// Resets this instance.
    /// </summary>
    private void Reset()
    {
      JobTitleTextBox.Text = string.Empty;
    }

    private void Localise()
    {
      GoButton.Text = CodeLocalise("Global.Go", "Go");
      JobTitleRequired.ErrorMessage = CodeLocalise("JobTitle.RequiredErrorMessage", "Job title is required");
    }

    #region page generated events

    public delegate void NextStepHandler(object sender, CommandEventArgs eventArgs);
    public event NextStepHandler NextStepClicked;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    public void OnNextStepClicked(CommandEventArgs eventargs)
    {
      var handler = NextStepClicked;
      if (handler != null) handler(this, eventargs);
    }

    #endregion
  }
}