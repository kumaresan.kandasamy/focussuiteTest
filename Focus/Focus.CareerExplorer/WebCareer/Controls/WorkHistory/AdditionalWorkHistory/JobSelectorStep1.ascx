﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobSelectorStep1.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.WorkHistory.AdditionalWorkHistory.JobSelectorStep1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<div style="width: 100%;">
    <h1><%= HtmlLocalise("JobSelectorStep1.Title", "Other job activities")%></h1>
    <%=HtmlLocalise("JobSelectorStep1.Text", "Enter a job title that describes other activities you performed in this job.")%><br/>
    <asp:TextBox ID="JobTitleTextBox" runat="server" Width="80%" Height="20px" ClientIDMode="Static" AutoCompleteType="Disabled" />&nbsp;<asp:Button runat="server" ID="GoButton" CssClass="buttonLevel2" CausesValidation="True" ValidationGroup="JobSelectorStep1" OnClick="GoButton_Clicked"/>
    <ajaxtoolkit:AutoCompleteExtender ID="JobTitleAutoComplete" runat="server" CompletionSetCount="20" CompletionListCssClass="autocomplete_completionListElement" CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" ServiceMethod="GetJobTitlesWithMOCs" ServicePath="~/Services/AjaxService.svc" TargetControlID="JobTitleTextBox" CompletionInterval="1000" UseContextKey="false" EnableCaching="true" MinimumPrefixLength="2" OnClientShown="fixAutoCompleteExtender" />
	<br/><asp:RequiredFieldValidator ID="JobTitleRequired" runat="server" ControlToValidate="JobTitleTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="JobSelectorStep1" />
</div>
