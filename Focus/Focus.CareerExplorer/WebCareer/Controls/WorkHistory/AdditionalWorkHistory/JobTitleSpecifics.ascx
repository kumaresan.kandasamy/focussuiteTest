﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobTitleSpecifics.ascx.cs"
    Inherits="Focus.CareerExplorer.WebCareer.Controls.WorkHistory.AdditionalWorkHistory.JobTitleSpecifics" %>
<div class="resumeSectionContent">
    <h1>
        <%= HtmlLocalise("JobTitleSpecifics.Title", "Other Job Activities") %></h1>
    <p>
        <focus:LocalisedLabel runat="server" ID="JobTitleInstructionLabel" RenderOuterSpan="False"
            LocalisationKey="JobTitleInstruction.Label" DefaultText="Next, we're going to ask you about your job. To ask the right questions, we need to make sure we understand what your job was about." />
    </p>
    <div class="resumeSectionSubHeading">
        <focus:LocalisedLabel runat="server" ID="JobTitleSubHeadingRequiredLabel" CssClass="requiredDataLegend" LocalisationKey="JobTitleSubHeadingRequired.Label" DefaultText="required fields" />
    </div>
    <div class="resumeJobTitle otherJobActivities">
        <asp:Panel ID="panNormalTitles" runat="server">
            <div class="dataInputRow">
                <asp:Label ID="lblGenericJobTitleInstruction" runat="server" AssociatedControlID="rblOccupations" CssClass="dataInputLabel"></asp:Label>
                <span class="dataInputField">
                    <asp:RadioButtonList ID="rblOccupations" runat="server" class="radioButtonList" ClientIDMode="Static"
                        RepeatLayout="Flow" RepeatDirection="Vertical" CssClass="dataInputVerticalRadio">
                    </asp:RadioButtonList><br/>
                    <asp:CustomValidator ID="OccupationsValidator" runat="server" CssClass="error" ControlToValidate="rblOccupations"
                        ClientValidationFunction="CheckOccupationsMH" Display="Dynamic" ValidateEmptyText="true"
                        Enabled="false" ValidationGroup="JobTitleSpecifcs"></asp:CustomValidator>
                    <asp:UpdatePanel ID="MOCUpdatepanel" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td style="text-align: right;">
                                        <asp:DataPager ID="MOCDataPager" runat="server" PagedControlID="MOCListView" PageSize="10">
                                            <Fields>
                                                <asp:NumericPagerField NextPageText="View more &#187;" PreviousPageText="&#171; Previous" />
                                            </Fields>
                                        </asp:DataPager>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ListView ID="MOCListView" runat="server" ItemPlaceholderID="MOCPlaceHolder"
                                            OnPagePropertiesChanging="MOCListView_PagePropertiesChanging" OnItemDataBound="MOCListView_ItemDataBound">
                                            <LayoutTemplate>
                                                <table style="width: 100%;">
                                                    <asp:PlaceHolder ID="MOCPlaceHolder" runat="server" />
                                                </table>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:Literal ID="ltMOCTitle" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:CustomValidator ID="custMOCTitle" runat="server" CssClass="error" Enabled="false"
                        SetFocusOnError="true" ClientValidationFunction="CheckMOCTitleMH" Display="Dynamic"
                        ValidationGroup="JobTitleSpecifcs"></asp:CustomValidator>
                    <asp:HiddenField ID="hiddenSelectedMOCTitle" ClientIDMode="Static" runat="server" />
                    <asp:Label ID="lblAlertInfo" runat="server" CssClass="error" Visible="False"></asp:Label>
                </span>
            </div>
            <div class="dataInputRow">
                <focus:LocalisedLabel runat="server" ID="JobTitleOptionsLabel" AssociatedControlID="mnuBrowseJob"
                    CssClass="dataInputLabel" LocalisationKey="JobTitleOptions.Label" DefaultText="If none of these titles fits, select from the following list:" />
                <span class="dataInputField manyHatsBrowseJob">
                    <asp:TreeView ID="mnuBrowseJob" runat="server" AutoGenerateDataBindings="False" CssClass="label-small" EnableClientScript="False" 
                        PopulateNodesFromClient="False" OnTreeNodeExpanded="mnuBrowseJob_TreeNodeExpanded"
                        OnTreeNodeCollapsed="mnuBrowseJob_TreeNodeCollapsed" OnSelectedNodeChanged="mnuBrowseJob_SelectedNodeChanged" SkipLinkText="">
                        <NodeStyle HorizontalPadding="23px" NodeSpacing="2px" />
                    </asp:TreeView>
                </span>
            </div>
        </asp:Panel>
        <asp:Panel ID="panSpecialtitles" runat="server" Visible="false">
            <div class="dataInputRow">
                <focus:LocalisedLabel runat="server" ID="SpecialJobTitleLabel" AssociatedControlID="cblSpecialTitle"
                    CssClass="dataInputLabel" LocalisationKey="SpecialJobTitle.Label" DefaultText="Select areas of interest" />
                <span class="dataInputField">
                    <focus:LocalisedLabel runat="server" ID="SpecialJobTitleInstructionsLabel" LocalisationKey="SpecialJobTitleInstructions.Label"
                        DefaultText="Stay-at-home parents and care givers wear many hats, but resumes that list too many tasks can be less compelling. To assist you in specifying your most relevant activities, we recommend you select the two categories that best represent your interests and talents:" />
                    <asp:CheckBoxList ID="cblSpecialTitle" runat="server" ClientIDMode="Static">
                    </asp:CheckBoxList>
                    <asp:CustomValidator ID="custSpecialTitleNotFound" runat="server" CssClass="error" Enabled="false" SetFocusOnError="true" ClientValidationFunction="CheckSpecialTitleNotFoundMH" Display="Dynamic" ValidationGroup="JobTitleSpecifcs"></asp:CustomValidator>
                    <asp:CustomValidator ID="custSpecialTitleLimit" runat="server" CssClass="error" Enabled="false" SetFocusOnError="true" ClientValidationFunction="CheckSpecialTitleNotFoundMH" Display="Dynamic" ValidationGroup="JobTitleSpecifcs"></asp:CustomValidator>
                </span>
            </div>
        </asp:Panel>
    </div>
    <asp:Button runat="server" ID="CancelButton" CssClass="buttonLevel3" />&nbsp;<asp:Button
        runat="server" ID="NextButton" CssClass="buttonLevel2" OnClick="NextButton_Clicked"
        CausesValidation="True" ValidationGroup="JobTitleSpecifcs" />
</div>
