﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Activities.ascx.cs"
    Inherits="Focus.CareerExplorer.WebCareer.Controls.WorkHistory.AdditionalWorkHistory.Activities" %>
<%@ Register Src="~/WebCareer/Controls/ScrollableSkillsAccordion.ascx" TagName="SkillsAccordion"
    TagPrefix="uc" %>
<h1><%= HtmlLocalise("OtherJobActivities.Text", "Other job activities")%></h1>
<div class="jobActivities">
    <div class="col-md-12 col-sm-12">
        <div class="col-md-6 col-sm-6 col-xs-12"><asp:Literal ID="lblWorkActivitiesInfo" runat="server"></asp:Literal></div>
        <div class="col-md-6 col-sm-6 col-xs-12"><div id="selectedQuestions" runat="server" ClientIDMode="Static">
	<div class="selectedQuestionsInner" >
		<div>
			<focus:LocalisedLabel runat="server" ID="WorkActivitiesSelectedStatementsLabel" RenderOuterSpan="False" CssClass="embolden" LocalisationKey="WorkActivitiesSelectedStatements.Label" DefaultText="Your selected statements"/>
			&nbsp;&nbsp;<focus:LocalisedLabel runat="server" ID="WorkActivitiesSelectedStatementsInfoLabel" CssClass="smallItalic" LocalisationKey="WorkActivitiesSelectedStatementsInfo.Label" DefaultText="you can edit these in the next step"/>
			<img src="<%= UrlBuilder.CloseRoundOn() %>" alt="<%= HtmlLocalise("Global.Close.ToolTip", "Close") %>" width="16" height="15" onclick="alwaysOnTop.hidediv('selectedQuestions');" />
		</div>
		<div>
			<asp:ListBox ID="lbSelectedStatements" runat="server" Width="95%" class="ListBox" Rows="5" ClientIDMode="Static"></asp:ListBox>
		</div>
	</div>
</div>
    </div></div>

<div id="StatementsDiv" runat="server" class="resumeSectionContent">
    <div class="resumeWorkActivities">
        <div>
            <focus:LocalisedLabel runat="server" ID="WorkActivitiesStatementsLabel" RenderOuterSpan="False"
                LocalisationKey="WorkActivitiesStatements.Label" DefaultText="You may pick up to 12 statements." />
        </div>
        <div class="resumeWorkActivitiesStatements manyHatsActivities">
            <asp:Panel ID="panTaskQuestions" runat="server" BorderWidth='0'>
            </asp:Panel>
        </div>
    </div>
</div>


<div id="skillsAccordionDiv" runat="server" class="manyHatsActivities">
	<asp:Panel ID="SkillsAccordionPanel" runat="server" ClientIDMode="Static">
		    <uc:SkillsAccordion runat="Server" ID="SkillsAccordion" WrapperCssClass="skillsAccordionWrapper" AccordionCssClass="skillsAccordion" SummaryCssClass="skillsAccordionSummary customStyleSelectBoxIgnore"/>
		  </asp:Panel>
</div>
<asp:Button runat="server" ID="CancelButton" CssClass="buttonLevel3"/>&nbsp;<asp:Button runat="server" ID="NextButton" CssClass="buttonLevel2" OnClick="NextButton_Clicked" CausesValidation="False" />
<asp:HiddenField ID="StatementSelectionInfoModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="StatementSelectionInfoModal" runat="server" BehaviorID="StatementSelectionInfoModal"
	TargetControlID="StatementSelectionInfoModalDummyTarget" PopupControlID="StatementSelectionInfoModalPanel"
	BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<div id="StatementSelectionInfoModalPanel" tabindex="-1" class="modal" style="z-index:100001; display:none;">
	<div class="lightbox resumeStatementSelectionModal">
		<div class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('StatementSelectionInfoModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.ToolTip", "Close") %>" width="20" height="20" />
		</div>
		<focus:LocalisedLabel runat="server" ID="StatementSelectionLabel" CssClass="modalMessage" LocalisationKey="StatmentSelection.Label" DefaultText="You may only choose 12 statements to describe your job tasks. If this work statement is important to you, please unselect one of your previously chosen work statements."/>
	</div>
</div>
<asp:HiddenField ID="SelectedStatementsHiddenField" runat="server" ClientIDMode="Static" />
</div>
