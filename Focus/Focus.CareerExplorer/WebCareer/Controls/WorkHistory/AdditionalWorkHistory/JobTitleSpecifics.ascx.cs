﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory.AdditionalWorkHistory
{
  public partial class JobTitleSpecifics : UserControlBase
	{
    #region Page Properties

    private const string SelectedOnetcode = "";

    private AddWorkHistory WorkHistory
    {
      get { return GetViewStateValue<AddWorkHistory>("JobTitleSpecifics:WorkHistory"); }
      set { SetViewStateValue("JobTitleSpecifics:WorkHistory", value); }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Page.IsPostBack)
      {
	      Localise();
				ApplyBranding();
      }
    }

		/// <summary>
		/// Applies client specific branding.
		/// </summary>
		private void ApplyBranding()
		{
			mnuBrowseJob.CollapseImageUrl = UrlBuilder.MinusIcon();
			mnuBrowseJob.ExpandImageUrl = UrlBuilder.PlusIcon();
		}

    /// <summary>
    /// Shows the specified work history.
    /// </summary>
    /// <param name="workHistory">The work history.</param>
    public void Show(AddWorkHistory workHistory)
    {
      WorkHistory = workHistory;
      Visible = true;
      Bind();
    }

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      try
      {
        MOCDataPager.Visible = MOCListView.Visible = rblOccupations.Visible = false;
        NextButton.Visible = true;
        cblSpecialTitle.Items.Clear();
        if (WorkHistory.IsNotNull() && WorkHistory.JobTitle.IsNotNullOrEmpty())
        {
          custSpecialTitleNotFound.Enabled = custSpecialTitleLimit.Enabled = panSpecialtitles.Visible = WorkHistory.IsSpecialTitle;
          panNormalTitles.Visible = !WorkHistory.IsSpecialTitle;

          if (WorkHistory.IsMOCTitle)
            lblGenericJobTitleInstruction.Text = CodeLocalise("GenericJobNameInstruction.Military.Label",
                                                              "Which of these jobs is most closely related to your military experience?");
          else
            lblGenericJobTitleInstruction.Text = CodeLocalise("GenericJobNameInstruction.Civilian.Label",
                                                              "Which of these titles best fits your job?");

          if (WorkHistory.IsSpecialTitle)
          {
            cblSpecialTitle.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SpecialGenericTitles), null, true);

            if (WorkHistory.OnetCode.IsNotNullOrEmpty())
            {
              foreach (
                var genericTitle in
                  WorkHistory.OnetCode.Split(',')
                    .Select(
                      onetCode =>
                      ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.SpecialGenericTitles)
                        .FirstOrDefault(x => x.ExternalId == onetCode))
                    .Where(genericTitle => genericTitle != null && genericTitle.Id != -1))
              {
                var listItem = cblSpecialTitle.Items.FindByValue(genericTitle.ExternalId);
                if (listItem.IsNotNull())
                  listItem.Selected = true;

              }
            }
          }

          else
          {
            MOCListView.Visible = custMOCTitle.Enabled = WorkHistory.IsMOCTitle;

            List<OnetCode> occupations = null;

            if (WorkHistory.IsMOCTitle)
            {
              var jobTitle = "";

              if (WorkHistory.JobTitle.IsNotNullOrEmpty() && WorkHistory.MOC.IsNotNullOrEmpty())
              {
                if (WorkHistory.JobTitle != WorkHistory.MOC)
                  jobTitle = string.Format("{0} - {1}", WorkHistory.MOC, WorkHistory.JobTitle);
                else if (WorkHistory.MOC.IsNotNullOrEmpty())
                  jobTitle = WorkHistory.MOC;
              }

              if (jobTitle.IsNotNullOrEmpty())
                occupations = ServiceClientLocator.OccupationClient(App).MOCJobTitles(jobTitle, 0);

              if (occupations.IsNotNullOrEmpty())
              {
                App.SetSessionValue("JobTitleSpecifics:MOCTitles", occupations);
                MOCListView.DataSource = occupations;
                MOCListView.DataBind();
                MOCListView.Visible = true;
                MOCDataPager.Visible = occupations.Count <= 10 ? false : true;
              }
              else
              {
                occupations = null; // set null to confirm the UI display of 2* categories.
              }

              if (SelectedOnetcode.IsNotNullOrEmpty())
                MOCListView_PagePropertiesChanging(null, new PagePropertiesChangingEventArgs(0, 10));
            }
            else
            {
              occupations = ServiceClientLocator.OccupationClient(App).GenericJobTitles(WorkHistory.JobTitle, 5);
              LoadFunctionalTitles(occupations, WorkHistory.OnetCode);

              if (rblOccupations.Items.Count > 0)
                rblOccupations.SelectedIndex = 0;
              //rblOccupations.SelectedIndex = App.Settings.Theme == FocusThemes.Education &&
              //                               (IsInternJobTitle(WorkHistory.JobTitle) ||
              //                                (WorkHistory.EducationInternshipSkills.IsNotNullOrEmpty()))
              //                                 ? occupations.Count
              //                                 : 0;

             
            }

            if (occupations.IsNotNull() && occupations.Count > 0)
            {
              OccupationsValidator.Enabled = true;
              lblAlertInfo.Visible = false;
            }
            else
            {
              lblAlertInfo.Text = CodeLocalise("AlertInfo.Label",
                                               "Please browse more choices to find a generic jobtitle for <strong>{0}</strong> at <strong>{1}</strong>.", WorkHistory.JobTitle, WorkHistory.Employer);
              lblAlertInfo.Visible = true;
              NextButton.Visible = false;
            }

            var jobFamilies = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.JobFamily);
            if (jobFamilies != null && jobFamilies.Count > 0)
            {
              mnuBrowseJob.Visible = true;
              LoadOnetJobFamilies(jobFamilies);
            }
            else
              mnuBrowseJob.Visible = false;
          }

        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }


    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void Localise()
    {
      custSpecialTitleNotFound.ErrorMessage = CodeLocalise("SpecialGenericName.RequiredErrorMessage", "Select at least one title");
      custSpecialTitleLimit.ErrorMessage = CodeLocalise("SpecialGenericName.ErrorMessage", "Maximum of two titles are allowed");
      OccupationsValidator.ErrorMessage = custMOCTitle.ErrorMessage = CodeLocalise("MOCName.RequiredErrorMessage", "Generic job title is required<br/>");
      CancelButton.Text = CodeLocalise("CancelButton.Text", "Cancel");
      NextButton.Text = CodeLocalise("NextButton.Text", "Next: enter work activities");
    }

    /// <summary>
    /// Loads the functional titles.
    /// </summary>
    /// <param name="onets">The onets.</param>
    /// <param name="selectedOnet">The selected onet.</param>
    private void LoadFunctionalTitles(IEnumerable<OnetCode> onets, string selectedOnet = null)
    {
      rblOccupations.Items.Clear();
      foreach (OnetCode onet in onets)
        if (selectedOnet.IsNotNull() && selectedOnet == onet.Code)
          rblOccupations.Items.Add(new ListItem(onet.Occupation, onet.Code, true));
        else
          rblOccupations.Items.Add(new ListItem(onet.Occupation, onet.Code));

      rblOccupations.Visible = true;
      //if (App.Settings.Theme == FocusThemes.Education)
      //  rblOccupations.Items.Add(new ListItem(CodeLocalise("JobTitles.Internship", "Internship")));
    }


    /// <summary>
    /// Loads the onet job families.
    /// </summary>
    /// <param name="jobFamilies">The job families.</param>
    private void LoadOnetJobFamilies(IEnumerable<LookupItemView> jobFamilies)
    {
      mnuBrowseJob.Nodes.Clear();

      foreach (var jFamily in jobFamilies)
      {
        var onetNode = new TreeNode(jFamily.Text)
                         {
                           Value = jFamily.Id.ToString(),
                           Text = jFamily.Text,
                           SelectAction = TreeNodeSelectAction.SelectExpand
                         };
        onetNode.ChildNodes.Add(new TreeNode());
        mnuBrowseJob.Nodes.Add(onetNode);
      }

      mnuBrowseJob.CollapseAll();
    }

    /// <summary>
    /// Checks if job title contains intern words
    /// </summary>
    /// <param name="jobTitle">The job title.</param>
    /// <returns>
    /// True or false
    /// </returns>
    private bool IsInternJobTitle(string jobTitle)
    {
      var internshipWords = new List<string>
                              {
                                CodeLocalise("JobTitle.Intern", "intern"),
                                CodeLocalise("JobTitle.Interns", "interns"),
                                CodeLocalise("JobTitle.Internship", "internship"),
                                CodeLocalise("JobTitle.Internship", "internships"),
                                CodeLocalise("JobTitle.Apprentice", "apprentice")
                              };

      return
        internshipWords.Any(
          internshipWord =>
          Regex.IsMatch(jobTitle, string.Concat(@"\b", internshipWord, @"\b"), RegexOptions.IgnoreCase));
    }

    #region events

    protected void MOCListView_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
      var occupations = App.GetSessionValue<List<OnetCode>>("JobTitleSpecifics:MOCTitles");

      int startIndex = e.StartRowIndex, maxRows = e.MaximumRows;
      if (sender.IsNull() && SelectedOnetcode.IsNotNullOrEmpty() && occupations.IsNotNull())
      {
        var selIndex = occupations.FindIndex(x => x.Code == SelectedOnetcode);
        if (selIndex > 0)
          startIndex = (selIndex/maxRows)*maxRows;
      }

      //set current page startindex, max rows and rebind to false
      MOCDataPager.SetPageProperties(startIndex, maxRows, false);

      //rebind List View
      if (occupations.IsNotNull())
      {
        MOCListView.DataSource = occupations;
        MOCListView.DataBind();
      }
      OnTreePostBackRequest(new CommandEventArgs("TreePostBackRequest", null));
    }

    protected void MOCListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
      var onet = (OnetCode) e.Item.DataItem;

      var mocDetails = string.Format("{0}:{1}:{2}:{3}", onet.Occupation.Replace("'", "ˆ").Trim(), onet.Code,
                                     onet.RonetCode, onet.StarRating);
      var mocLiteral = (Literal) e.Item.FindControl("ltMOCTitle");
      mocLiteral.Text =
        string.Format("<input type=\"radio\" name=\"moctitles\" onclick=\"SelectedMOCTitleMH('{0}')\"{1}/>{2}", mocDetails,
                      (SelectedOnetcode.IsNotNullOrEmpty() && SelectedOnetcode == onet.Code
                         ? " checked=\"checked\""
                         : ""), onet.Occupation);
      if (SelectedOnetcode.IsNotNullOrEmpty() && SelectedOnetcode == onet.Code)
        hiddenSelectedMOCTitle.Value = mocDetails;
    }

    protected void mnuBrowseJob_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
    {
      var onetOccups = ServiceClientLocator.SearchClient(App).GetOnets(Convert.ToInt64(e.Node.Value));

      if (onetOccups != null && onetOccups.Count > 0)
      {
        e.Node.ChildNodes.Clear();

        foreach (var onetOccup in onetOccups)
        {
          var onetNode = new TreeNode(onetOccup.Occupation)
                           {
                             Value = "ˆ" + onetOccup.OnetCode,
                             Text = onetOccup.Occupation,
                             ImageUrl = UrlBuilder.Bullet()
                           };
          e.Node.ChildNodes.Add(onetNode);
        }

        e.Node.Expand();
        e.Node.SelectAction = TreeNodeSelectAction.None;
      }
      OnTreePostBackRequest(new CommandEventArgs("TreePostBackRequest", null));
    }

    protected void mnuBrowseJob_TreeNodeCollapsed(object sender, TreeNodeEventArgs e)
    {
      e.Node.Collapse();
      e.Node.SelectAction = TreeNodeSelectAction.SelectExpand;
      OnTreePostBackRequest(new CommandEventArgs("TreePostBackRequest", null));
    }

    protected void mnuBrowseJob_SelectedNodeChanged(object sender, EventArgs e)
    {
      if (mnuBrowseJob.SelectedNode.Value.Contains("ˆ"))
      {
        var onetcode = mnuBrowseJob.SelectedNode.Value.Replace("ˆ", "");
        WorkHistory.OnetCode = onetcode;
        WorkHistory.GenericJobTitle = mnuBrowseJob.SelectedNode.Text;

        OnNextStepClicked(new CommandEventArgs("JobTitleSpecifics", WorkHistory));
      }
    }

    protected void NextButton_Clicked(object sender, EventArgs e)
    {
      var starRating = 0;
      string onetcode = "", ronetcode = "", genericTitle = "";
      if (WorkHistory.IsSpecialTitle)
      {
        var onetCodes = new List<string>();
        var genericTitles = new List<string>();
        foreach (var item in cblSpecialTitle.Items.Cast<ListItem>().Where(item => item.Selected))
        {
          onetCodes.Add(item.Value);
          genericTitles.Add(item.Text);
        }

        onetcode = onetCodes.Count > 0 ? string.Join<string>(",", onetCodes) : ",";
        genericTitle = string.Join<string>(",", genericTitles);
      }
      else
      {
        if (WorkHistory.IsMOCTitle)
        {
          if (hiddenSelectedMOCTitle.Value.IsNotNullOrEmpty())
          {
            var onetronet = hiddenSelectedMOCTitle.Value.Split(':');
            genericTitle = onetronet[0].Replace("ˆ", "'");
            onetcode = onetronet[1];
            ronetcode = onetronet[2];
            starRating = onetronet[3].AsInt32();
          }
        }
        else if (rblOccupations.SelectedItem != null)
        {
          genericTitle = rblOccupations.SelectedItem.Text;
          onetcode = rblOccupations.SelectedItem.Value;
        }
      }

      WorkHistory.GenericJobTitle = genericTitle;
      WorkHistory.OnetCode = onetcode;
      WorkHistory.ROnetCode = ronetcode;
      WorkHistory.StarRating = starRating;

      if (SelectedOnetcode.IsNotNullOrEmpty() && SelectedOnetcode != WorkHistory.OnetCode)
      {
        WorkHistory.ResponseToQuestions = null;
        WorkHistory.Certificates = null;
        WorkHistory.Statements = null;
        WorkHistory.StatementsToUse = null;
      }

      OnNextStepClicked(new CommandEventArgs("JobTitleSpecifics", WorkHistory));
    }
    #endregion

    #region page generated events

    public delegate void NextStepHandler(object sender, CommandEventArgs eventArgs);
    public event NextStepHandler NextStepClicked;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    public void OnNextStepClicked(CommandEventArgs eventargs)
    {
      var handler = NextStepClicked;
      if (handler != null) handler(this, eventargs);
    }

    public delegate void TreePostBackHandler(object sender, CommandEventArgs eventArgs);
    public event TreePostBackHandler TreePostBackRequest;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    public void OnTreePostBackRequest(CommandEventArgs eventargs)
    {
      var handler = TreePostBackRequest;
      if (handler != null) handler(this, eventargs);
    }

    #endregion

  }
}