﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory
{
	public class WorkHistoryControlBase : UserControlBase
	{
		/// <summary>
		/// Saves the work history.
		/// </summary>
		/// <param name="nJob">The n job.</param>
		/// <param name="redirectTab">Whether to redirect to a specific tab</param>
		/// <returns></returns>
		internal bool SaveWorkHistory(AddWorkHistory nJob, out string redirectTab)
		{
			bool saveStatus;

			redirectTab = null;
			
			if (nJob.IsNull())
				throw new Exception();

			var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
			var clonedResume = currentResume.IsNotNull() ? currentResume.CloneObject() : null;

			var tempJob = new Job
			{
				JobId = nJob.JobId,
				Title = nJob.JobTitle,
				MilitaryOccupationCode = nJob.IsMOCTitle ? nJob.MOC : null,
				RankId = nJob.RankId,
				BranchOfServiceId = nJob.BranchOfServiceId,
				Employer = nJob.Employer,
				JobStartDate = nJob.StartDate,
				Description = nJob.JobDescription,
				JobAddress = new Address
				{
					City = nJob.City,
					StateId = nJob.StateId,
					StateName = nJob.StateName,
					CountryId = nJob.CountryId,
					CountryName = nJob.CountryName
				},
				Salary = nJob.Wage,
				OnetCode = nJob.OnetCode,
				ROnetCode = nJob.ROnetCode,
				StarRating = nJob.StarRating,
				InternshipSkills = nJob.EducationInternshipSkills
			};

			if (!nJob.IsPresentJob)
			{
				tempJob.JobEndDate = nJob.EndDate;
				tempJob.IsCurrentJob = false;
			}
			else
				tempJob.IsCurrentJob = true;

            tempJob.JobReasonForLeaving = (int) nJob.ReasonForLeaving;

			if (tempJob.Salary > 0)
				tempJob.SalaryFrequencyId = nJob.PayFrequencyId;

			if (nJob.IsNewJob)
			{
				if (currentResume.IsNull())
				{
					currentResume = new ResumeModel
					{
						ResumeContent = new ResumeBody
						{
							ExperienceInfo = new ExperienceInfo
							{
								Jobs = new List<Job>()
							}
						},
						ResumeMetaInfo =
							new ResumeEnvelope { CompletionStatus = ResumeCompletionStatuses.WorkHistory }
					};

					currentResume.ResumeContent.ExperienceInfo.Jobs.Add(tempJob);
				}
				else
				{
					if (currentResume.ResumeContent.IsNull())
						currentResume.ResumeContent = new ResumeBody();
					if (currentResume.ResumeContent.ExperienceInfo.IsNotNull())
					{
						if (currentResume.ResumeContent.ExperienceInfo.Jobs.IsNotNull())
							currentResume.ResumeContent.ExperienceInfo.Jobs.Add(tempJob);
						else
						{
							currentResume.ResumeContent.ExperienceInfo.Jobs = new List<Job> { tempJob };
						}
					}
					else
					{
						currentResume.ResumeContent.ExperienceInfo = new ExperienceInfo { Jobs = new List<Job> { tempJob } };
					}
				}
				currentResume.ResumeContent.ExperienceInfo.IsExperienceInfoUpdated = true;
			}
			else if (!nJob.IsNewJob && currentResume.IsNotNull())
			{
				var selectedJob = currentResume.ResumeContent.ExperienceInfo.Jobs.FirstOrDefault(j => j.JobId == nJob.JobId);
				if (currentResume.ResumeContent.IsNotNull()
						&& Utilities.IsObjectModified(selectedJob, tempJob))
					currentResume.ResumeContent.ExperienceInfo.IsExperienceInfoUpdated = true;

				currentResume.ResumeContent.ExperienceInfo.Jobs.Remove(selectedJob);
				currentResume.ResumeContent.ExperienceInfo.Jobs.Add(tempJob);
      }

      // Set completion status to work history if it is not work history already (i.e.Don't reset when they've completed this step already and are revisiting)
      if ((currentResume.ResumeMetaInfo.CompletionStatus & ResumeCompletionStatuses.WorkHistory) != ResumeCompletionStatuses.WorkHistory)
      {
        if (currentResume.ResumeMetaInfo.ResumeCreationMethod.IsIn(ResumeCreationMethod.Upload, ResumeCreationMethod.CopyPaste))
        {
          // Reset post paste/uppload statuses
          if (currentResume.ResumeMetaInfo.CompletionStatus == ResumeCompletionStatuses.None && currentResume.ResumeContent.ExperienceInfo.AllJobsComplete)
            currentResume.ResumeMetaInfo.CompletionStatus = currentResume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.WorkHistory | ResumeCompletionStatuses.Contact;
        }
        else
          currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.WorkHistory;
      }

		  if (nJob.IsMOCTitle)
			{

				Job latestMocJob = null;
				DateTime startDate = DateTime.MaxValue, endDate = DateTime.MinValue;
				if (currentResume.ResumeContent.ExperienceInfo.Jobs.Count > 0)
				{
					foreach (var job in currentResume.ResumeContent.ExperienceInfo.Jobs.Where(x => x.MilitaryOccupationCode.IsNotNullOrEmpty()))
					{
						if (job.JobStartDate.HasValue && job.JobStartDate < startDate)
							startDate = (DateTime)job.JobStartDate;
						if (job.IsCurrentJob.HasValue && (bool)job.IsCurrentJob)
						{
							endDate = DateTime.Today;
							latestMocJob = job;
						}
						else if (job.JobEndDate.HasValue && job.JobEndDate > endDate)
						{
							endDate = (DateTime)job.JobEndDate;
							latestMocJob = job;
						}
					}
				}
				if (latestMocJob.IsNotNull())
				{
					if (currentResume.ResumeContent.IsNull())
						currentResume.ResumeContent.Profile = new UserProfile();
					
          if (currentResume.ResumeContent.Profile.IsNull())
						currentResume.ResumeContent.Profile = new UserProfile();

          if (currentResume.ResumeContent.Profile.Veteran.IsNull())	// || !currentResume.ResumeContent.UserInfo.Profile.Veteran.IsVeteran
            currentResume.ResumeContent.Profile.Veteran = new VeteranInfo { VeteranPriorityServiceAlertsSubscription = true };

					var veteranInfo = currentResume.ResumeContent.Profile.Veteran;
                    if (!veteranInfo.IsVeteran.GetValueOrDefault() || veteranInfo.IsVeteran.IsNull())
					{
						var ronets = new Dictionary<string, int>();
						if (latestMocJob.ROnetCode.IsNotNullOrEmpty())
							ronets.Add(System.Text.RegularExpressions.Regex.Replace(latestMocJob.ROnetCode, @"[^0-9]+", ""), latestMocJob.StarRating.HasValue ? (int)latestMocJob.StarRating : 0);

            if (veteranInfo.History.IsNullOrEmpty())
              veteranInfo.History = new List<VeteranHistory> { new VeteranHistory() };

            veteranInfo.History[0].VeteranEra = VeteranEra.OtherVet;
            veteranInfo.History[0].MilitaryBranchOfServiceId = latestMocJob.BranchOfServiceId;
            veteranInfo.History[0].VeteranStartDate = startDate;
            veteranInfo.History[0].VeteranEndDate = endDate;
            veteranInfo.History[0].RankId = latestMocJob.RankId;
            veteranInfo.History[0].MilitaryOccupationCode = latestMocJob.MilitaryOccupationCode;
            veteranInfo.History[0].MilitaryOccupation = latestMocJob.Title;
						veteranInfo.MOCRonetInfo = ronets;
						veteranInfo.IsVeteran = true;

						if (currentResume.Special.IsNull())
							currentResume.Special = new ResumeSpecialInfo();
						if (
							!(currentResume.Special.IsPastFiveYearsVeteran.HasValue &&
								(bool)currentResume.Special.IsPastFiveYearsVeteran))
							currentResume.Special.IsPastFiveYearsVeteran = Utilities.IsPastFiveYearsVeteran(startDate, endDate);

						// Force the user to enter profile again
						if ((currentResume.ResumeMetaInfo.CompletionStatus & ResumeCompletionStatuses.Profile) == ResumeCompletionStatuses.Profile)
						{
							currentResume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.WorkHistory | ResumeCompletionStatuses.Contact | ResumeCompletionStatuses.Education | ResumeCompletionStatuses.Summary | ResumeCompletionStatuses.Options;
							redirectTab = "profile";
						}

						if (currentResume.Special.Preferences.IsNotNull())
							currentResume.Special.Preferences.IsResumeSearchable = null;
					}
				}
			}

			try
			{
				long? resumeId = null;
				if (currentResume.IsNull()
						||
						(currentResume.ResumeMetaInfo.CompletionStatus & ResumeCompletionStatuses.WorkHistory) !=
						ResumeCompletionStatuses.WorkHistory
						|| (currentResume.IsNotNull()
								&& currentResume.ResumeContent.IsNotNull()
								&& currentResume.ResumeContent.ExperienceInfo.IsNotNull()
								&& currentResume.ResumeContent.ExperienceInfo.IsExperienceInfoUpdated))
				{
					Utilities.UpdateOnetROnetInResume(currentResume);

					var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
					if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
						currentResume.ResumeMetaInfo.ResumeName = resumeName;

					if (Utilities.IsObjectModified(clonedResume, currentResume))
					{
						
						var resume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, true);
						resumeId = resume.ResumeMetaInfo.ResumeId;
					}

					if ((!App.UserData.HasDefaultResume) || App.UserData.DefaultResumeId == resumeId)
					{
						if (!App.UserData.HasDefaultResume)
							currentResume.ResumeMetaInfo = ServiceClientLocator.ResumeClient(App).GetResumeInfo(resumeId);

						Utilities.UpdateUserContextUserInfo(currentResume.ResumeMetaInfo, ResumeCompletionStatuses.WorkHistory);
					}

					currentResume.ResumeContent.ExperienceInfo.IsExperienceInfoUpdated = false;
				}

				if (!App.UserData.HasDefaultResume)
					App.UserData.DefaultResumeId = resumeId;


        if (currentResume.ResumeMetaInfo.ResumeCreationMethod.IsIn(ResumeCreationMethod.Upload, ResumeCreationMethod.CopyPaste))
        {
          // If resume is uploaded/pasted we either need to set the status as none (some job history is missing) or to the status set after a resume has been uplaoded successfully with complete work history
          if (currentResume.ResumeContent.ExperienceInfo.IsNotNull() && !currentResume.ResumeContent.ExperienceInfo.AllJobsComplete)
            currentResume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.None;
          else if (currentResume.ResumeContent.ExperienceInfo.IsNotNull() && currentResume.ResumeContent.ExperienceInfo.AllJobsComplete && currentResume.ResumeMetaInfo.CompletionStatus == ResumeCompletionStatuses.None)
            currentResume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.WorkHistory | ResumeCompletionStatuses.Contact | ResumeCompletionStatuses.Education | ResumeCompletionStatuses.Summary | ResumeCompletionStatuses.Options;
        }
        else
          currentResume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.WorkHistory;

				if (!currentResume.ResumeMetaInfo.ResumeId.HasValue)
					currentResume.ResumeMetaInfo.ResumeId = resumeId;

				App.SetSessionValue("Career:ResumeID", resumeId);
				App.SetSessionValue("Career:Resume", currentResume);

				saveStatus = true;
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
				saveStatus = false;
			}

			return saveStatus;
		}
	}
}
