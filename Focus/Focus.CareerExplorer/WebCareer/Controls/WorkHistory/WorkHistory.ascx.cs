﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory
{
	public partial class WorkHistory : UserControlBase
	{
		//public event EventHandler SaveResumeTitle;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
			if (IsJobList() && addworkhistory.IsNull())
			{
				SetWorkHistoryTab(true);
				WorkHistoryList.Visible = true;
				WorkHistoryList.BindStep();
			}
			else
			{
				var lsSubTab = Page.RouteData.Values["SubTab"] as string;
				if (lsSubTab != null)
					SetCurrentWHTab(lsSubTab);
				else
					SetCurrentWHTab("addajob");

				SetWorkHistoryTab(false);
			}
		}

		protected override void OnInit(EventArgs e)
		{
			//AddAJobTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
			//JobTitleDetailsTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
			//WorkActivitiesTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
			//JobDescriptionTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
			//WorkHistoryList.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
		}

    /// <summary>
    /// Handles the Click event of the SaveResumeTitleButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveResumeTitleButton_Click(object sender, EventArgs e)
		{
			//if (SaveResumeTitle != null)
			//  SaveResumeTitle(this, EventArgs.Empty);
			((ResumeWizard)this.Page).SaveResumeTitleButton_Click(sender, e);
		}

    /// <summary>
    /// Checks the job title.
    /// </summary>
    /// <param name="n1">The n1.</param>
    /// <param name="n2">The n2.</param>
    /// <returns></returns>
		public string CheckJobTitle(string n1, string n2)
		{
			var sum = n1 + "/" + n2;
			return sum;
		}

    /// <summary>
    /// Determines whether [is job list].
    /// </summary>
    /// <returns>
    /// 	<c>true</c> if [is job list]; otherwise, <c>false</c>.
    /// </returns>
		private bool IsJobList()
		{
			var jobStatus = false;
			var model = App.GetSessionValue<ResumeModel>("Career:Resume", null);

			if (model.IsNotNull()
				&& model.ResumeContent.IsNotNull()
				&& model.ResumeContent.ExperienceInfo.IsNotNull()
				&& model.ResumeContent.ExperienceInfo.Jobs.IsNotNull()
				&& model.ResumeContent.ExperienceInfo.Jobs.Count > 0)
				jobStatus = true;

			return jobStatus;
		}

    /// <summary>
    /// Sets the work history tab.
    /// </summary>
    /// <param name="isWorkHistoryList">if set to <c>true</c> [is work history list].</param>
		public void SetWorkHistoryTab(bool isWorkHistoryList)
		{
			var stepsCompleted = 0;
    	var isNewJob = true;

			var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails", null);
			if (isWorkHistoryList)
				stepsCompleted = -1;
			else if (!isWorkHistoryList && addworkhistory.IsNotNull())
			{
				stepsCompleted = addworkhistory.StepsCompleted;
				isNewJob = addworkhistory.IsNewJob;
			}

    	switch (stepsCompleted)
			{
				case -1:
					hrefAddAJob.LinkDisabled(true);
					hrefJobTitleDetails.LinkDisabled(true);
					hrefWorkActivities.LinkDisabled(true);
					hrefJobDescription.LinkDisabled(true);
					break;
				case 0:
					hrefAddAJob.LinkDisabled(false);
					hrefJobTitleDetails.LinkDisabled(true);
					hrefWorkActivities.LinkDisabled(true);
					hrefJobDescription.LinkDisabled(true);
					break;
				case 1:
					hrefAddAJob.LinkDisabled(false);
					hrefJobTitleDetails.LinkDisabled(false);
					hrefWorkActivities.LinkDisabled(true);
					hrefJobDescription.LinkDisabled(true);
					break;
				case 2:
					hrefAddAJob.LinkDisabled(false);
					hrefJobTitleDetails.LinkDisabled(false);
					hrefWorkActivities.LinkDisabled(false);
					hrefJobDescription.LinkDisabled(true);
					break;
				case 3:
					hrefAddAJob.LinkDisabled(false);
					hrefJobTitleDetails.LinkDisabled(false);
					hrefWorkActivities.LinkDisabled(false);
					hrefJobDescription.LinkDisabled(false);
					break;
				default:
					hrefAddAJob.LinkDisabled(false);
					hrefJobTitleDetails.LinkDisabled(true);
					hrefWorkActivities.LinkDisabled(true);
					hrefJobDescription.LinkDisabled(true);
					break;
			}

    	JobTitleDetailsLink.Visible = isNewJob;
    	WorkActivitiesLink.Visible = isNewJob;
    	AddAJobLabel.Visible = isNewJob;
    	EditAJobLabel.Visible = !isNewJob;
		}

    /// <summary>
    /// Sets the current WH tab.
    /// </summary>
    /// <param name="tabName">Name of the tab.</param>
		public void SetCurrentWHTab(string tabName)
		{
			switch (tabName.ToLower())
			{
				case "addajob":
					AddAJobLink.Attributes.Add("class", "selected");
					AddAJobTab.Visible = true;
					AddAJobTab.BindStep();
					break;
				case "jobtitledetails":
					JobTitleDetailsLink.Attributes.Add("class", "selected");
					JobTitleDetailsTab.Visible = true;
					JobTitleDetailsTab.BindStep();
					break;
				case "workactivities":
					WorkActivitiesLink.Attributes.Add("class", "selected");
					WorkActivitiesTab.Visible = true;
					WorkActivitiesTab.BindStep();
					break;
				case "jobdescription":
					JobDescriptionLink.Attributes.Add("class", "selected");
					JobDescriptionTab.Visible = true;
					JobDescriptionTab.BindStep();
					break;
				default:
					AddAJobLink.Attributes.Add("class", "selected");
					AddAJobTab.Visible = true;
					AddAJobTab.BindStep();
					break;
			}
		}
	}

	#region Helper class
  [Serializable]
	public class AddWorkHistory
	{
		public Guid? JobId { get; set; }
		public bool IsNewJob { get; set; }
		public bool IsSpecialTitle { get; set; }
		public bool IsMOCTitle { get; set; }
		public string JobTitle { get; set; }
		public string Employer { get; set; }
		public long? RankId { get; set; }
		public long? BranchOfServiceId { get; set; }
		public bool IsPresentJob { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public string City { get; set; }
		public string StateName { get; set; }
		public long? StateId { get; set; }
		public string CountryName { get; set; }
		public long? CountryId { get; set; }
		public float? Wage { get; set; }
		public long? PayFrequencyId { get; set; }
		public string JobDescription { get; set; }

		public string MOC { get; set; }
		public string OnetCode { get; set; }
		public string ROnetCode { get; set; }
		public int StarRating { get; set; }
		public string GenericJobTitle { get; set; }

		public List<Question> Questions { get; set; }
    public NameValueCollectionSerializable ResponseToQuestions { get; set; }
		public List<string> Certificates { get; set; }
		public List<string> Statements { get; set; }
		public List<string> StatementsToUse { get; set; }
		public List<string> KeywordsToUse { get; set; }
    public List<InternshipSkill> EducationInternshipSkills { get; set; }
		public int StepsCompleted { get; set; }
        public ReasonForLeaving ReasonForLeaving { get; set; }
	}

	#endregion
}
