﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusExplorer;

using Framework.Core;
using Framework.DataAccess;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common.Helpers;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory
{
    public partial class AddAJob : WorkHistoryControlBase
    {
        #region Page Properties

        private string _isMOCJobTitle = "";

        #endregion

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // AIM- FVN-4651-CE:Work history accessible without signing in
            if (!App.User.IsAuthenticated)
            {
                Response.Redirect(UrlBuilder.Home());
            }
        }

        #region Bind Add a Job section

        internal void BindStep()
        {
            /** KRP - 09.June.2016
             * Binds the veteran header text
             * Veteran header text disappearing when a postback happends.
             * So rebinding should happen for postback
             */
            BindHeaderPanel();
            if (!IsPostBack)
            {
                LocaliseUI();
		ReasonForLeavingLabel.Visible = ddlReasonForLeaving.Visible = App.Settings.ShowReasonForLeavingFeature;
                BindDropdowns();
                LoadAddAJob();
                ScriptManager.RegisterClientScriptInclude(this, GetType(), "resumeBuilderAddJobScriptInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.ResumeBuilder.AddJob.min.js"));
                RegisterCodeValuesJson("resumeBuilderAddJobScriptValues", "resumeAddJobCodeValues", InitialiseClientSideCodeValues());
            }
        }

        #endregion

        /// <summary>
        /// Determines whether [is job list].
        /// </summary>
        /// <returns>
        /// 	<c>true</c> if [is job list]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsJobList()
        {
            var jobStatus = false;
            var model = App.GetSessionValue<ResumeModel>("Career:Resume", null);

            if (model.IsNotNull()
              && model.ResumeContent.IsNotNull()
              && model.ResumeContent.ExperienceInfo.IsNotNull()
              && model.ResumeContent.ExperienceInfo.Jobs.IsNotNull()
              && model.ResumeContent.ExperienceInfo.Jobs.Count > 0)
                jobStatus = true;

            return jobStatus;
        }

        /// <summary>
        /// Handles the Click event of the lnkSkipWorkHistory control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkSkipWorkHistory_Click(object sender, EventArgs e)
        {
            var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");

            var tempResumeInfo = new ResumeEnvelope
            {
                CompletionStatus = ResumeCompletionStatuses.WorkHistory,
                ResumeCreationMethod = ResumeCreationMethod.BuildWizard,
                ResumeStatus = ResumeStatuses.Active
            };
            if (currentResume.IsNull())
                currentResume = new ResumeModel();
            if (currentResume.ResumeMetaInfo.IsNull())
                currentResume.ResumeMetaInfo = tempResumeInfo;
            else
                currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.WorkHistory;
            if (currentResume.ResumeContent.IsNull())
                currentResume.ResumeContent = new ResumeBody();

            if (!App.UserData.HasDefaultResume)
            {
                var tempResume = ServiceClientLocator.ResumeClient(App).GetDefaultResume();
                if (tempResume.IsNotNull())
                {
                    var prevResumeInfo = currentResume.ResumeMetaInfo;
                    currentResume.ResumeMetaInfo = tempResume.ResumeMetaInfo;
                    currentResume.ResumeMetaInfo.ResumeCreationMethod = prevResumeInfo.ResumeCreationMethod;
                    currentResume.ResumeMetaInfo.ResumeStatus = prevResumeInfo.ResumeStatus;
                    currentResume.ResumeMetaInfo.CompletionStatus |= prevResumeInfo.CompletionStatus;
                    currentResume.ResumeMetaInfo.ResumeName = null;
                }
            }

            var resumeName = ((ResumeWizard)this.Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
            if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
                currentResume.ResumeMetaInfo.ResumeName = resumeName;

            try
            {

                var resume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, true);
                var resumeId = resume.ResumeMetaInfo.ResumeId;

                //UpdateROnet("11-1011.00", "Onet");
                App.SetSessionValue("Career:ResumeID", resumeId);
                App.SetSessionValue("Career:Resume", currentResume);
                Response.RedirectToRoute("ResumeWizardTab", new { Tab = "contact" });
            }
            catch (ApplicationException ex)
            {
                MasterPage.ShowError(AlertTypes.Error, ex.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the btnAddAJobSaveTop control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnAddAJobSaveTop_Click(object sender, EventArgs e)
        {
            FrameAddAJob();

            ((ResumeWizard)Page).SaveResumeTitleButton_Click(sender, e);

            var newJob = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");

            // If editing go straight to job description
            if (newJob.IsNotNull() && !newJob.IsNewJob)
                Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "jobdescription" });
            else
                Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "jobtitledetails" });
        }

        /// <summary>
        /// Handles the Click event of the SaveAndReturnButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void SaveAndReturnButton_Click(object sender, EventArgs e)
        {
            FrameAddAJob();

            var workHistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");

            string redirectTab;
            if (SaveWorkHistory(workHistory, out redirectTab))
            {
                ((ResumeWizard)Page).SaveResumeTitleButton_Click(sender, e);

                App.RemoveSessionValue("Career:NewJobDetails");

                Response.RedirectToRoute("ResumeWizardTab", new { Tab = "workhistory" });
            }
        }

        /// <summary>
        /// Loads the add A job.
        /// </summary>
        private void LoadAddAJob()
        {
            var newJob = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");

            if (newJob.IsNull())
                return;

            AddAJobSubHeadingLabel.Visible = newJob.IsNewJob;
            EditAJobSubHeadingLabel.Visible = !newJob.IsNewJob;
            AddAJobInstructionLabel.Visible = newJob.IsNewJob;
            NoJobHistorySpan.Visible = newJob.IsNewJob;
            SaveAndReturnTopButton.Visible = SaveAndReturnBottomButton.Visible = !newJob.IsNewJob;

            _isMOCJobTitle = newJob.IsMOCTitle ? "true" : "false";

            if (newJob.JobTitle.IsNotNullOrEmpty() && newJob.MOC.IsNotNullOrEmpty())
                txtJobTitle.Text = newJob.JobTitle != newJob.MOC ? string.Format("{0} - {1}", newJob.MOC, newJob.JobTitle) : newJob.MOC;
            else
                txtJobTitle.Text = string.Concat(newJob.MOC, newJob.JobTitle);

            if (!string.IsNullOrEmpty(newJob.Employer))
                txtEmployer.Text = newJob.Employer;

            var mocTitles = ServiceClientLocator.OccupationClient(App).GetMilitaryOccupationJobTitles(newJob.MOC + " - " + newJob.JobTitle, 10);
            var branchesOfService = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MilitaryBranchesOfService);
            var distinctBranchesOfService = mocTitles.Select(y => branchesOfService.FirstOrDefault(x => x.Id == y.BranchOfServiceId)).ToList();

            if (distinctBranchesOfService.Count() > 1)
            {
                hideemployer.Attributes.Remove("style"); ;
                txtEmployer.Attributes.Add("style", @"display:none;");
            }

            ddlEmployer.Items.Clear();
            ddlEmployer.Items.Add(new ListItem { Value = "", Text = "- select branch -" });
            distinctBranchesOfService.ForEach(b => ddlEmployer.Items.Add(new ListItem { Value = b.Id.ToString(), Text = b.Text }));

            if (newJob.BranchOfServiceId.HasValue)
            {
                BranchOfServiceHidden.Value = newJob.BranchOfServiceId.Value.ToString();
                ddlEmployer.SelectedValue = BranchOfServiceHidden.Value;
                var ranks = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MilitaryRanks, newJob.BranchOfServiceId.Value).ToList();
                ranks.ForEach(r => ddlRank.Items.Add(new ListItem { Value = r.Id.ToString(), Text = r.Text }));
                hiderank.Attributes.Remove("style");
            }

            if (newJob.RankId.HasValue)
            {
                hdnRank.Value = newJob.RankId.Value.ToString();
                ddlRank.SelectedValue = newJob.RankId.Value.ToString();
            }

            var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
            if (currentResume.IsNotNull() && currentResume.ResumeContent.Profile.IsNotNull())
            {
                var veteranInfo = currentResume.ResumeContent.Profile.Veteran;
                var jobseekerIsVeteran = veteranInfo.IsVeteran ?? false;
                // Hide the info if veteran information has been completed
                if (veteranInfo.IsNotNull() && jobseekerIsVeteran && (veteranInfo.History[0].VeteranEndDate.IsNotNull() || veteranInfo.History[0].VeteranEra == VeteranEra.OtherEligible))
                {
                    MilitaryServiceInfoLabel.Visible = false;
                }
            }

            if (newJob.StartDate.HasValue && newJob.StartDate != DateTime.MinValue)
                StartDateTextBox.Text = ((DateTime)newJob.StartDate).ToString("MM/dd/yyyy");

            EndDateTextBox.Enabled = !(chkCurrentlyWorkHere.Checked = newJob.IsPresentJob);
            if (!newJob.IsPresentJob && newJob.EndDate.HasValue && newJob.EndDate != DateTime.MinValue)
                EndDateTextBox.Text = ((DateTime)newJob.EndDate).ToString("MM/dd/yyyy");

            if (!string.IsNullOrEmpty(newJob.City))
                txtCity.Text = newJob.City;

            if (newJob.StateId.HasValue)
                ddlState.SelectValue(newJob.StateId.ToString());

            if (newJob.CountryId.HasValue)
                ddlCountry.SelectValue(newJob.CountryId.ToString());

            if (newJob.Wage != 0)
            {
                txtWages.Text = newJob.Wage.ToString();
                if (newJob.PayFrequencyId.HasValue)
                    ddlPayUnit.SelectValue(newJob.PayFrequencyId.ToString());
            }

            ddlReasonForLeaving.SelectValue(newJob.ReasonForLeaving.ToString());

            //if(App.Settings.Theme == FocusThemes.Education && newJob.JobId.IsNotNull())
            //  newJob.EducationInternshipSkillIds = ServiceClientLocator.ResumeClient(App).GetEducationInternshipSkills((Guid) newJob.JobId);
        }

        /// <summary>
        /// Frames the add A job.
        /// </summary>
        private void FrameAddAJob()
        {
            var newJob = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
            if (newJob.IsNull() || newJob.IsNewJob || newJob.JobId.IsNull())
                newJob = new AddWorkHistory { IsNewJob = true };

            newJob.JobTitle = txtJobTitle.TextTrimmed();
            newJob.MOC = null;
            newJob.IsMOCTitle = false;

            newJob.Employer = txtEmployer.TextTrimmed(defaultValue: null);
            newJob.RankId = hdnRank.Value.ToLong();
            newJob.BranchOfServiceId = BranchOfServiceHidden.Value.ToLong();

            if (newJob.Employer.IsNotNullOrEmpty())
                newJob.Employer = Utilities.TitleCase(newJob.Employer);

            var jobTitle = newJob.JobTitle.Trim().ToLower();
            if (jobTitle.IsNotNullOrEmpty())
                newJob.JobTitle = newJob.JobTitle.TitleCase();

            var specialTitles = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.UnclassifiedJobs);

            if (specialTitles.IsNotNull())
            {
                var temp = from item in specialTitles
                           let title = item.Text.ToLower()
                           let parts = title.Split(',')
                           where title == jobTitle || (parts.Length > 0 && parts.Contains(jobTitle))
                           select item;
                newJob.IsSpecialTitle = temp.Any();
            }

            var onetCodesForJobTitle = ServiceClientLocator.OccupationClient(App).GenericJobTitles(newJob.JobTitle, 1);
            if (onetCodesForJobTitle.IsNotNullOrEmpty())
            {
                newJob.OnetCode = onetCodesForJobTitle[0].Code;
            }

            //Reason For Leaving
            newJob.ReasonForLeaving = chkCurrentlyWorkHere.Checked ? ReasonForLeaving.StillEmployed : ddlReasonForLeaving.SelectedValueToEnum<ReasonForLeaving>();

            //Check for special job title
            if (newJob.JobTitle.IsNotNullOrEmpty())
            {
                var spTitles = (List<LookupItemView>)ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.UnclassifiedJobs);
                if (spTitles.IsNotNull() && spTitles.Exists(x => (x.Text.ToLower() == newJob.JobTitle.ToLower() || (x.Text.Contains(",") && ((x.Text.Split(',')[0].IsNotNullOrEmpty() && x.Text.Split(',')[0] == newJob.JobTitle.ToLower()) || (x.Text.Split(',')[1].IsNotNullOrEmpty() && x.Text.Split(',')[1] == newJob.JobTitle.ToLower()))))))
                    newJob.IsSpecialTitle = true;
                else
                    newJob.IsSpecialTitle = false;

                if (!newJob.IsSpecialTitle)
                {
                    var mocTitles = ServiceClientLocator.OccupationClient(App).GetMilitaryOccupationJobTitles(jobTitle, 250);

                    if (mocTitles.Count > 0)
                    {
                        MilitaryOccupationJobTitleViewDto selectedMOC = null;

                        if ((selectedMOC = mocTitles.FirstOrDefault(x => x.JobTitle.Trim().ToLower() == jobTitle)).IsNotNull())
                        {
                            newJob.MOC = selectedMOC.JobTitle.Substring(0, selectedMOC.JobTitle.IndexOf(" - "));
                            newJob.JobTitle = selectedMOC.JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ") + 3);
                            newJob.IsMOCTitle = true;
                        }
                        else
                        {
                            selectedMOC = mocTitles.First();
                            newJob.JobTitle = selectedMOC.JobTitle.Substring(0, selectedMOC.JobTitle.IndexOf(" - "));
                            if (mocTitles.Count() == 1)
                                newJob.JobTitle = selectedMOC.JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ") + 3);
                            else
                            {
                                var branchOfServiceId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MilitaryBranchesOfService).Where(x => x.Text.ToLower() == newJob.Employer.ToLower()).Select(x => x.Id).FirstOrDefault();

                                if (branchOfServiceId.IsNotNull() && branchOfServiceId > 0)
                                {
                                    var mocs = mocTitles.Where(x => x.BranchOfServiceId == branchOfServiceId).ToList();

                                    if (mocs.IsNotNullOrEmpty() && mocs.Count == 1)
                                        newJob.JobTitle = mocs.First().JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ") + 3);
                                }
                            }
                            newJob.IsMOCTitle = true;
                        }
                    }

                }

            }

            DateTime startDate, endDate;
            var enUS = new CultureInfo("en-US");

            DateTime.TryParseExact(StartDateTextBox.Text, "MM/dd/yyyy", enUS, DateTimeStyles.None, out startDate);
            newJob.StartDate = startDate;

            newJob.IsPresentJob = chkCurrentlyWorkHere.Checked;
            if (chkCurrentlyWorkHere.Checked)
                newJob.EndDate = DateTime.Today;
            else
            {
                if (DateTime.TryParseExact(EndDateTextBox.Text, "MM/dd/yyyy", enUS, DateTimeStyles.None, out endDate))
                    newJob.EndDate = endDate;
            }

            newJob.City = txtCity.TextTrimmed(defaultValue: null);
            newJob.StateId = ddlState.SelectedValueToLong();
            newJob.StateName = ddlState.SelectedItem.Text;
            newJob.CountryId = ddlCountry.SelectedValueToLong();
            newJob.CountryName = ddlCountry.SelectedItem.Text;

            if (!string.IsNullOrEmpty(txtWages.Text))
            {
                newJob.Wage = txtWages.TextTrimmed(defaultValue: null).AsFloat().AsNullIfDefault<float>();
                newJob.PayFrequencyId = ddlPayUnit.SelectedValueToLong();
            }
            else
            {
                newJob.Wage = 0;
                newJob.PayFrequencyId = null;
            }

            if (newJob.StepsCompleted < 1)
                newJob.StepsCompleted = (newJob.IsNewJob) ? 1 : 3;

            App.SetSessionValue("Career:NewJobDetails", newJob);
        }

        /// <summary>
        /// Binds the dropdowns.
        /// </summary>
        private void BindDropdowns()
        {
            ddlState.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), "", CodeLocalise("State.TopDefault", "- select state -"));
            ddlCountry.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), "", CodeLocalise("Country.TopDefault", "- select country -"));
            ddlPayUnit.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Frequencies), null, CodeLocalise("PayUnit.TopDefault", "- select pay type -"));
            //Reason For Leaving Drop Down
            ddlReasonForLeaving.Items.Clear();
            ddlReasonForLeaving.Items.AddEnum(ReasonForLeaving.ClosureLayoff, "Business closure or mass layoff");
            ddlReasonForLeaving.Items.AddEnum(ReasonForLeaving.Fired, "Discharged, fired, or terminated");
            ddlReasonForLeaving.Items.AddEnum(ReasonForLeaving.LaidOff, "Laid off; lack of work");
            ddlReasonForLeaving.Items.AddEnum(ReasonForLeaving.Quit, "Quit, resigned");
            ddlReasonForLeaving.Items.AddEnum(ReasonForLeaving.Retired, "Retired");
            ddlReasonForLeaving.Items.AddEnum(ReasonForLeaving.Other, "Other");

            ddlReasonForLeaving.Items.AddLocalisedTopDefault("ReasonForLeaving.TopDefault", "- select a reason -");
        }

        /// <summary>
        /// KRP - 09.June.2016
        /// Binds the veteran header text
        /// Veteran header text disappearing when a postback happends.
        /// So rebinding should happen for postback
        /// </summary>
        private void BindHeaderPanel()
        {
            if (IsJobList())
            {
                VeteranInstructionsHeadingLabel.DefaultText = CodeLocalise("VeteranInstructionsHeading.Label",
                  "Special instructions for veterans, homemakers or volunteers");
                NoJobHistoryPanel.Visible = false;
            }
            else
            {
                VeteranInstructionsHeadingLabel.DefaultText =
                  CodeLocalise("VeteranInstructionsHeadingNoJobHistory.Label",
                    "Special instructions for veterans, homemakers, volunteers, or those without job history");
                NoJobHistoryPanel.Visible = true;
            }
        }
        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            JobTitleRequired.ErrorMessage = CodeLocalise("JobTitle.RequiredErrorMessage", "Job title is required");
            EmployerRequired.ErrorMessage = CodeLocalise("Employer.RequiredErrorMessage", "#BUSINESS# is required");
            ReasonForLeavingRequired.ErrorMessage = CodeLocalise("ReasonForLeaving.RequiredErrorMessage", "Reason For Leaving is required");
            CityRequired.ErrorMessage = CodeLocalise("City.RequiredErrorMessage", "City is required");
            StateRequired.ErrorMessage = CodeLocalise("State.RequiredErrorMessage", "State is required");
            CountryRequired.ErrorMessage = CodeLocalise("Country.RequiredErrorMessage", "Country is required");

            SaveAndReturnTopButton.Text = SaveAndReturnBottomButton.Text = CodeLocalise("SaveAndReturnButton.Text", "Save & Return to Job List >>");
            btnAddAJobSaveTop.Text = btnAddAJobSaveBottom.Text = CodeLocalise("AddAJobSaveButton.Text", "Save & Move to Next Step >>");

        }

        /// <summary>
        /// Initialises the client side code values.
        /// </summary>
        /// <returns></returns>
        private NameValueCollection InitialiseClientSideCodeValues()
        {
            var nvc = new NameValueCollection
				          {
					          { "isMOCJobTitle", _isMOCJobTitle },
										{ "errorPayUnitRequired", CodeLocalise("PayUnit.ValidatorErrorMessage", "Pay type is required") },
										{ "errorHourlyLimit", CodeLocalise("PayUnit.HourLimitErrorMessage", "Hour is between:	$4.90 to $168.25") },
										{ "errorDailyLimit", CodeLocalise("PayUnit.DayLimitErrorMessage", "Day is between:	$39.25 to $1346.00") },
										{ "errorWeeklyLimit", CodeLocalise("PayUnit.WeekLimitErrorMessage", "Week is between:	$196.00 to $6730.75") },
										{ "errorMonthlyLimit", CodeLocalise("PayUnit.MonthLimitErrorMessage", "Month is between: $850.00 to $29166.50") },
										{ "errorYearlyLimit", CodeLocalise("PayUnit.YearLimitErrorMessage", "Year is between:	$10192.00 to $350000.00") },
										{ "errorWages", CodeLocalise("Wages.ErrorMessage", "Wages is required") }, 
										{ "errorStartDateRequired", CodeLocalise("StartDate.RequiredErrorMessage", "Start date is required") },
										{ "errorDateFormat", CodeLocalise("Date.ErrorMessage", "Valid date is required") },
										{ "errorFutureDate", CodeLocalise("FutureDate.ErrorMessage", "Job date should not be a future date") },
										{ "errorYearLimit", CodeLocalise("Year.LimitsErrorMessage", "Date must not be more than 100 years in the past") }, 
										{ "errorDateMax", CodeLocalise("Date.MaxErrorMessage", "Date should not be greater than current date") },
										{ "errorEndDateRequired", CodeLocalise("EndDate.RequiredErrorMessage", "End date is required") }, 
										{ "errorEndMonth", CodeLocalise("EndMonth.ErrorMessage", "End date month should not be less than start date month") }, 
										{ "errorEndYear", CodeLocalise("EndYear.ErrorMessage", "End date year should not be less than start date year") }, 
                                        { "errorEndDay", CodeLocalise("EndDay.ErrorMessage", "End date day should not be less than start date day")},
										{ "stateCodeOutsideUS", GetLookupId(Constants.CodeItemKeys.States.ZZ).ToString() }, 
										{ "countryCodeUS", GetLookupId(Constants.CodeItemKeys.Countries.US).ToString() }, 
										{ "frequencyHourly", GetLookupId(Constants.CodeItemKeys.Frequencies.Hourly).ToString() }, 
										{ "frequencyDaily", GetLookupId(Constants.CodeItemKeys.Frequencies.Daily).ToString() }, 
										{ "frequencyWeekly", GetLookupId(Constants.CodeItemKeys.Frequencies.Weekly).ToString() },
										{ "frequencyMonthly", GetLookupId(Constants.CodeItemKeys.Frequencies.Monthly).ToString() }, 
										{ "frequencyYearly", GetLookupId(Constants.CodeItemKeys.Frequencies.Yearly).ToString() }, 
										{ "urlAjaxService", ResolveUrl("~/Services/AjaxService.svc") }
				          };
            return nvc;
        }
    }
}
