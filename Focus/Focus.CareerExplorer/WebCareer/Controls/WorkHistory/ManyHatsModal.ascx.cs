﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory
{
  public partial class ManyHatsModal : UserControlBase
	{

    private int CurrentStep
    {
      get { return GetViewStateValue<int>("ManyHatsModal:CurrentStep"); }
      set { SetViewStateValue("ManyHatsModal:CurrentStep", value); }
    }

    private Tuple<string, string> JobCompany
    {
      get { return GetViewStateValue<Tuple<string, string>>("ManyHatsModal:JobCompany"); }
      set { SetViewStateValue("ManyHatsModal:JobCompany", value); }
    }

    private AddWorkHistory WorkHistory
    {
      get { return GetViewStateValue<AddWorkHistory>("ManyHatsModal:WorkHistory"); }
      set { SetViewStateValue("ManyHatsModal:WorkHistory", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Shows the specified job name.
    /// </summary>
    /// <param name="jobName">Name of the job.</param>
    /// <param name="jobEmployer">The job employer.</param>
    public void Show(string jobName, string jobEmployer)
    {
      WorkHistory = null;
      Visible = true;
      JobCompany = new Tuple<string, string>(jobName, jobEmployer);
      CurrentStep = 1;
      ShowStep();
    }

    /// <summary>
    /// Shows the step.
    /// </summary>
    private void ShowStep()
    {
      Step3.Visible = Step2.Visible = Step1.Visible = false;
      switch (CurrentStep)
      {
        case 1:
          Step1.Show();
          break;
        case 2:
          Step2.Show(WorkHistory);
          break;
        case 3:
          Step3.Show(WorkHistory, JobCompany.Item1, JobCompany.Item2);
          break;
      }
      ManyHatsModalWindow.Show();
    }

    /// <summary>
    /// Functions the bind.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    private void UnBind(CommandEventArgs eventargs)
    {
      if (WorkHistory.IsNull()) WorkHistory = new AddWorkHistory{JobTitle = JobCompany.Item1, Employer = JobCompany.Item2};
      switch (CurrentStep)
      {
        case 1:
          Step1.Unbind(WorkHistory);
          break;
        case 2:
          WorkHistory = (AddWorkHistory)eventargs.CommandArgument;
          break;
        case 3:
          WorkHistory = (AddWorkHistory)eventargs.CommandArgument;
          OnManyHatsDescriptionsAdded(new CommandEventArgs("ManyHatsModal", WorkHistory));
          ManyHatsModalWindow.Hide();
          return;
      }
      CurrentStep++;
      ShowStep();
    }

    protected void StepCompleted_NextClicked(object sender, CommandEventArgs eventargs)
    {
      UnBind(eventargs);
    }

    protected void PostBack_Triggerd(object sender, CommandEventArgs eventargs)
    {
      ManyHatsModalWindow.Show();
    }

    #region page generated events

    public delegate void ManyHatsDescriptionsAddedHandler(object sender, CommandEventArgs eventArgs);
    public event ManyHatsDescriptionsAddedHandler ManyHatsDescriptionsAdded;

    /// <summary>
    /// Raises the <see cref="CommandEventArgs" /> event.
    /// </summary>
    /// <param name="eventargs">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    public void OnManyHatsDescriptionsAdded(CommandEventArgs eventargs)
    {
      var handler = ManyHatsDescriptionsAdded;
      if (handler != null) handler(this, eventargs);
    }
    #endregion
  }
}