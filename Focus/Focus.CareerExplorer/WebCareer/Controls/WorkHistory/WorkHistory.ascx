﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorkHistory.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.WorkHistory.WorkHistory" %>
<%@ Register Src="~/WebCareer/Controls/WorkHistory/AddAJob.ascx" TagName="AddAJob" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/WorkHistory/JobTitleDetails.ascx" TagName="JobTitleDetails" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/WorkHistory/WorkActivities.ascx" TagName="WorkActivities" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/WorkHistory/JobDescription.ascx" TagName="JobDescription" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/WorkHistory/WorkHistoryList.ascx" TagName="WorkHistoryList" TagPrefix="uc" %>

<div id="RCPNavigation">
  <nav id="RCPNavigationBottom">
    <ol id="RPCNavigationOrderedList">
      <li id="AddAJobLink" runat="server">
	      <a id="hrefAddAJob" runat="server" href="<%$RouteUrl:Tab=workhistory,SubTab=addajob,routename=ResumeWizardSubTab %>">
					<focus:LocalisedLabel runat="server" ID="AddAJobLabel" RenderOuterSpan="False" LocalisationKey="AddAJob.LinkText" DefaultText="Add a job"/>
					<focus:LocalisedLabel runat="server" ID="EditAJobLabel" RenderOuterSpan="False" LocalisationKey="EditAJob.LinkText" DefaultText="Edit a job" Visible="false" />
				</a>
      </li>
      <li id="JobTitleDetailsLink" runat="server">
	      <a id="hrefJobTitleDetails" runat="server" href="<%$RouteUrl:Tab=workhistory,SubTab=jobtitledetails,routename=ResumeWizardSubTab %>">
		      <focus:LocalisedLabel runat="server" ID="JobTitleDetailsLabel" RenderOuterSpan="False" LocalisationKey="JobNameDetails.LinkText" DefaultText="Job title details"/>
				</a>
      </li>
      <li id="WorkActivitiesLink" runat="server">
	      <a id="hrefWorkActivities" runat="server" href="<%$RouteUrl:Tab=workhistory,SubTab=workactivities,routename=ResumeWizardSubTab %>">
		      <focus:LocalisedLabel runat="server" ID="WorkActivitiesLabel" RenderOuterSpan="False" LocalisationKey="WorkActivities.LinkText" DefaultText="Work activities"/>
				</a>
      </li>
      <li id="JobDescriptionLink" runat="server">
	      <a id="hrefJobDescription" runat="server" href="<%$RouteUrl:Tab=workhistory,SubTab=jobdescription,routename=ResumeWizardSubTab %>">
		      <focus:LocalisedLabel runat="server" ID="JobDescriptionLabel" RenderOuterSpan="False" LocalisationKey="JobDescription.LinkText" DefaultText="Job description"/>
				</a>
      </li>
    </ol>                
  </nav>      
</div>   

<%--WORKHISTORYLIST--%>
<uc:WorkHistoryList ID="WorkHistoryList" runat="server" Visible="false" />
<%--ADDAJOB--%>
<uc:AddAJob ID="AddAJobTab" runat="server" Visible="false" />
<%--JOBTITLEDETAILS--%>
<uc:JobTitleDetails ID="JobTitleDetailsTab" runat="server" Visible="false" />
<%--WORKACTIVITIES--%>
<uc:WorkActivities ID="WorkActivitiesTab" runat="server" Visible="false" QuestionCssClass="workActivityQuestion" PromptCssClass="workActivityQuestionPrompt" ResponseCssClass="workActivityQuestionAnswer" />
<%--JOBDESCRIPTION--%>
<uc:JobDescription ID="JobDescriptionTab" runat="server" Visible="false" />

<script language="javascript">
  $(document).ready(function () {
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(WorkHistory_FixListPre);
  });

  function WorkHistory_FixListPre() {
    window.setTimeout(WorkHistory_FixListPost, 1);
  }

  function WorkHistory_FixListPost() {
    var ols = $("#RPCNavigationOrderedList")[0];
    var explicitStart = ols.start;

    if (explicitStart > 0) {
      ols.start = -1;
      ols.start = explicitStart;
    }
    else {
      ols.start = 1;
    }
  }
</script>