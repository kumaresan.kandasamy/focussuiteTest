﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core.Criteria.EducationInternship;
using Framework.Core;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Core;
using Focus.Common.Helpers;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.WorkHistory
{
  public partial class JobDescription : WorkHistoryControlBase
	{
    /// <summary>
    /// Gets or sets the data source for the control.
    /// </summary>
    public List<string> Statements { get; set; }

	  protected string RedirectTab
	  {
		  get { return GetViewStateValue<string>("JobDescription:RedirectTab"); }
			set { SetViewStateValue("JobDescription:RedirectTab", value); }
	  }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    #region Bind Job Description section

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
      var newjobdetails = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
      if (newjobdetails.IsNotNull() && newjobdetails.IsNewJob && newjobdetails.OnetCode == "Internship" && Statements.IsNullOrEmpty())
        LoadEducationInternshipStatements(newjobdetails);

      if (!IsPostBack)
      {
        LocaliseUI();

        if (newjobdetails.IsNotNull())
          LoadDescription();

        ScriptManager.RegisterClientScriptInclude(Page, GetType(), "resumeBuilderJobDescriptionScript", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.ResumeBuilder.JobDescription.min.js"));
      }

    	StatementsPlaceHolder.Visible = false;

      if (newjobdetails.IsNotNull() && newjobdetails.JobTitle.IsNotNullOrEmpty())
      {
        GetKeywordsICanUse(newjobdetails);
        
				if(newjobdetails.IsNewJob)
					GetStatementsICanUse(newjobdetails);
      }

			if (newjobdetails.IsNotNull())
			{
				JobDescriptionSubHeadingLabel.Visible = newjobdetails.IsNewJob;
				EditJobDescriptionSubHeadingLabel.Visible = !newjobdetails.IsNewJob;
				btnSaveAnotherJob.Visible = newjobdetails.IsNewJob;
				btnJobDescriptionSave.Visible = newjobdetails.IsNewJob;
				SaveAndReturnButton.Visible = !newjobdetails.IsNewJob;
			}
    }

    #endregion

    #region events
    /// <summary>
    /// Handles the Click event of the RefreshButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    public void RefreshButton_Click(object sender, EventArgs e)
    {
      var newjobdetails = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
      GetKeywordsICanUse(newjobdetails, true);
    }

    /// <summary>
    /// Handles the Click event of the btnJobDescriptionSave control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnJobDescriptionSave_Click(object sender, EventArgs e)
    {
			var workHistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
			if (workHistory.IsNull())
				throw new Exception();

    	workHistory.JobDescription = txtJobDescription.TextTrimmed();
			string redirectTab;
			if (SaveWorkHistory(workHistory, out redirectTab))
      {
        var model = App.GetSessionValue<ResumeModel>("Career:Resume");
        if (model.ResumeContent.ExperienceInfo.Jobs.Count > 1)
        {
          var isNewJob = workHistory.IsNewJob;
          App.RemoveSessionValue("Career:NewJobDetails");
          Response.RedirectToRoute("ResumeWizardTab", isNewJob ? new { Tab = redirectTab ?? "contact" } : new { Tab = "workhistory" });
        }
        else
        {
	        RedirectTab = redirectTab;
          lblJobValidationInfo.Text = CodeLocalise("AddAnotherJobMSG.Label", "You've only added one job to your job history. Did you want to add more?");
          DoNotAddJobButton.Text = CodeLocalise("Global.No.Text", "No");
          AddAnotherJobButton.Text = CodeLocalise("AddAnotherJob.Text", "Yes, add another job");
          JobValidationModalPopup.Show();
        }
      }
    }

    /// <summary>
    /// Handles the CloseCommand event of the ConfirmationModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    public void ConfirmationModal_CloseCommand(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "NO_JOB_ADD":
          try
          {
            var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
            App.RemoveSessionValue("Career:NewJobDetails");
            Response.RedirectToRoute("ResumeWizardTab",
                                     addworkhistory.IsNewJob ? new { Tab = "contact" } : new { Tab = "workhistory" });
          }
          catch (Exception)
          {
          }

          break;
      }
    }

    /// <summary>
    /// Handles the OkCommand event of the ConfirmationModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    public void ConfirmationModal_OkCommand(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "ADD_ANOTHER_JOB":
          try
          {
            var newJob = new AddWorkHistory { IsNewJob = true };
            App.SetSessionValue("Career:NewJobDetails", newJob);
            Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "addajob" });
          }
          catch (Exception)
          {
          }

          break;
      }
    }

    /// <summary>
    /// Handles the Click event of the btnSaveAnotherJob control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnSaveAnotherJob_Click(object sender, EventArgs e)
    {
			var workHistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
			if (workHistory.IsNull())
				throw new Exception();

			workHistory.JobDescription = txtJobDescription.TextTrimmed();
	    string redirectTab;
			if (SaveWorkHistory(workHistory, out redirectTab))
      {
        AddAnotherJob(sender, e);
      }
    }

		/// <summary>
		/// Handles the Click event of the SaveAndReturnButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void SaveAndReturnButton_Click(object sender, EventArgs e)
		{
			var workHistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
			
			if (workHistory.IsNull())
				throw new Exception();

			workHistory.JobDescription = txtJobDescription.TextTrimmed();
			string redirectTab;
			if (SaveWorkHistory(workHistory, out redirectTab))
			{
				App.RemoveSessionValue("Career:NewJobDetails");
				Response.RedirectToRoute("ResumeWizardTab", new { Tab = "workhistory" });
			}
		}

    /// <summary>
    /// Handles the Click event of the DoNotAddJobButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void DoNotAddJobButton_Click(object sender, EventArgs e)
    {
      try
      {
        var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
        App.RemoveSessionValue("Career:NewJobDetails");
        Response.RedirectToRoute("ResumeWizardTab",
                                 addworkhistory.IsNewJob ? new { Tab = RedirectTab ?? "contact" } : new { Tab = "workhistory" });
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    protected void OtherJobActivitesLink_Clicked(object sender, EventArgs e)
    {
      var workHistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
      ManyHatsModalWindow.Show(workHistory.JobTitle, workHistory.Employer);
    }


    protected void ManyHatsModalWindow_ManyHatsDescriptionsAdded(object sender, CommandEventArgs eventargs)
    {
      LoadDescription((AddWorkHistory)eventargs.CommandArgument);
    }
    #endregion

    /// <summary>
    /// Adds another job.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void AddAnotherJob(object sender, EventArgs e)
    {
      var newJob = new AddWorkHistory { IsNewJob = true };
      App.SetSessionValue<AddWorkHistory>("Career:NewJobDetails", newJob);
      Response.RedirectToRoute("ResumeWizardSubTab", new { Tab = "workhistory", SubTab = "addajob" });
    }

   

    /// <summary>
    /// Loads the description.
    /// </summary>
    private void LoadDescription(AddWorkHistory addWorkHistory = null)
    {
      if (addWorkHistory.IsNull())
        addWorkHistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
      
      if (txtJobDescription.Text.Length > 0)
        txtJobDescription.Text += "\n";

      if (addWorkHistory.OnetCode != "Internship")
      {
        if (addWorkHistory.Statements.IsNotNull() && addWorkHistory.Statements.Count > 0)
          txtJobDescription.Text += "* " + string.Join<string>("\n* ", addWorkHistory.Statements);
        if (addWorkHistory.Certificates.IsNotNull() && addWorkHistory.Certificates.Count > 0)
          txtJobDescription.Text += "* " + string.Join<string>("\n* ", addWorkHistory.Certificates);
      }
      else
      {
        if (Statements.IsNotNull() && Statements.Count > 0)
          txtJobDescription.Text += "* " + string.Join<string>("\n* ", Statements);
      }
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      DescriptionRequired.ErrorMessage = CodeLocalise("JobDescription.RequiredErrorMessage", "Job description is required");
      RefreshButton.Text = CodeLocalise("RefreshButton.Text", "Refresh");
			SaveAndReturnButton.Text = CodeLocalise("SaveAndReturnButton.Text", "Save & Return to Job List >>");
			btnSaveAnotherJob.Text = CodeLocalise("SaveAnotherJobButton.Text", "Save & Add Another Job >>");
			btnJobDescriptionSave.Text = CodeLocalise("JobDescriptionSaveButton.Text", "Save & Move to Next Step >>");
      OtherJobActivitesLink.Text = CodeLocalise("OtherJobActivitesLink.Text", "Describe other job activities you performed.");
    }

    /// <summary>
    /// Loads the education internship statements.
    /// </summary>
    /// <param name="newJobDetails">The new job details.</param>
    private void LoadEducationInternshipStatements(AddWorkHistory newJobDetails)
    {
			if (newJobDetails.EducationInternshipSkills.IsNotNullOrEmpty() && newJobDetails.EducationInternshipSkills.Count > 0)
      {
        var statements =
          ServiceClientLocator.CoreClient(App).GetEducationInternshipStatements(new EducationInternshipStatementCriteria
          {
            EducationInternshipSkillIds = newJobDetails.EducationInternshipSkills.Select(x => x.Id).ToList()
          });
        Statements = new List<string>();
        foreach (var statement in statements)
        {
          Statements.Add(statement.PastTenseStatement);
        }
      }
    }

    /// <summary>
    /// Gets the statements I can use.
    /// </summary>
    /// <param name="newJob">The new job.</param>
    private void GetStatementsICanUse(AddWorkHistory newJob)
    {
      try
      {
        //IEnumerable<string> statements = newJob.StatementsToUse;
        IEnumerable<string> statements = null;

        if (newJob.OnetCode != "Internship")
        {
          if (!Page.IsPostBack)
          {
            var descText = txtJobDescription.Text.ToLower();
            if (!string.IsNullOrEmpty(newJob.OnetCode))
            {
              if (!newJob.OnetCode.Contains(','))
              {
                var onetStatements = ServiceClientLocator.OccupationClient(App).Statements(newJob.OnetCode, StatementTense.Past);

                var addlStatements = from s in onetStatements
                                     let statement = s.Trim()
                                     where !descText.Contains(statement.ToLower())
                                     select statement;
                statements = addlStatements;
              }

              if (statements.IsNull() || !statements.Any())
              {
                // Get resume builder questions and display them
                var onetCodes = newJob.OnetCode.Split(',');

                if(statements.IsNull()) statements = new List<string>();

                statements = onetCodes.Select(onetCode => ServiceClientLocator.OccupationClient(App).ResumeBuilderQuestions(onetCode)).Aggregate(statements, (current, rbQuestions) => current.Concat(from q in rbQuestions
                                                                                                                                                                                     let questionType = q.QuestionType
                                                                                                                                                                                     let question = questionType == JobTaskTypes.MultiOption ? string.Format("{0} {1}.", q.Response.Trim(), string.Join<string>(", ", q.Options)) : q.Response.Trim()
                                                                                                                                                                                     where questionType.IsNotNull() && !descText.Contains(question.ToLower())
                                                                                                                                                                                     select question));
              }

              newJob.StatementsToUse = statements.Distinct(new IgnoreCaseStringComparer()).OrderBy(x => x).ToList();
              App.SetSessionValue("Career:NewJobDetails", newJob);
            }
          }
          else
          {
            var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
            statements = addworkhistory.StatementsToUse;
          }

          StatementsPlaceHolder.Visible = true;
        }
        else
        {
          statements = Statements;
          StatementsPlaceHolder.Visible = false;
        }

        if (statements.IsNotNullOrEmpty())
        {
          chkStatements.Items.Clear();
          chkStatements.Visible = true;
          lblStatements.Visible = false;
          chkStatements.Items.AddRange((from s in statements select new ListItem(s)).ToArray());
        }
        else
        {
          chkStatements.Visible = false;
          lblStatements.Visible = true;
          lblStatements.Text = CodeLocalise("Statements.Error", "System could not identify a set of statements for the generic job title you have chosen.");
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Gets the keywords I can use.
    /// </summary>
    /// <param name="newJob">The new job.</param>
    /// <param name="refreshKeywords">Whether to refresh the keywords</param>
    private void GetKeywordsICanUse(AddWorkHistory newJob, bool refreshKeywords = false)
    {
      try
      {
        IEnumerable<string> keywords = null;

        if (!Page.IsPostBack || refreshKeywords)
        {
          if (!String.IsNullOrEmpty(newJob.JobTitle))
          {
            var jobDescription = string.Concat(newJob.JobTitle.Trim(), "\n ", txtJobDescription.Text.Trim());
            keywords = ServiceClientLocator.SearchClient(App).ClarifyText(jobDescription, App.Settings.MinimumScoreForClarify, App.Settings.MaximumSearchCountForClarify, "keywords");

            if (!String.IsNullOrEmpty(newJob.OnetCode) && !newJob.OnetCode.Contains(",") && newJob.OnetCode != "Internship")
            {
              var onetKeywords = ServiceClientLocator.SearchClient(App).GetOnetKeywords(newJob.OnetCode);

              if (onetKeywords.Count > 0)
                keywords = keywords.Concat(onetKeywords);

              //TODO: Martha (new OnetSkills functionality)
              var onetSkills = ServiceClientLocator.OccupationClient(App).OnetSkills(newJob.OnetCode);
              //if (onetSkills.Count > 0)
              //  keywords = keywords.Concat(onetSkills);
            }
          }

          keywords = (from keyword in keywords
                      where keyword.IsNotNullOrEmpty()
                      orderby keyword
                      select keyword).Distinct(new IgnoreCaseStringComparer());
          newJob.KeywordsToUse = keywords.ToList();
          App.SetSessionValue("Career:NewJobDetails", newJob);
        }
        else
        {
          var addworkhistory = App.GetSessionValue<AddWorkHistory>("Career:NewJobDetails");
          keywords = addworkhistory.KeywordsToUse;
        }

        if (keywords.IsNotNull())
        {
          if (keywords.Any())
          {
            chkKeywords.Items.Clear();
            chkKeywords.Visible = true;
            lblKeywords.Visible = false;
            chkKeywords.Items.AddRange((from k in keywords select new ListItem(k)).ToArray());
          }
          else
          {
            chkKeywords.Visible = false;
            lblKeywords.Visible = true;
            lblKeywords.Text = CodeLocalise("Keywords.Error", "System could not identify a set of keywords for the generic job titles you have chosen.");
          }
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

  }
}
