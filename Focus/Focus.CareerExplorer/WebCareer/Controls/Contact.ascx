﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Contact.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.Contact" %>
<%@ Import Namespace="Focus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<div class="resumeSection">
	<div class="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2 greyedOut" ValidationGroup="Contact" OnClick="SaveMoveToNextStepButton_Click" disabled="disabled" ClientIDMode="Static" />
	</div>
	<div class="resumeSectionContent">
		<div class="resumeSectionSubHeading">
			<focus:LocalisedLabel runat="server" ID="ContactSubHeadingRequiredLabel" CssClass="requiredDataLegend" LocalisationKey="ContactSubHeadingRequired.Label" DefaultText="required fields"/>
		</div>
		<div class="resumeContact">
			<div class="dataInputRow">
				<div class="resumeContactLeft col-xs-12  col-sm-12 col-md-5">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="FirstnameLabel" AssociatedControlID="FirstNameTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="Firstname.Label" DefaultText="First name"/></div>
					<div class="dataInputField">
						<asp:TextBox ID="FirstNameTextBox" runat="server" ClientIDMode="Static" Width="280px" MaxLength="20" /><br />
						<asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="FirstNameTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Contact" />
					</div>
				</div>
				<div class="resumeContactRight col-xs-12  col-sm-12 col-md-7">
						<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="MiddleInitialLabel" AssociatedControlID="MiddleInitialTextBox" CssClass="dataInputLabel" LocalisationKey="MiddleInitial.Label" DefaultText="Middle initial"/></div>
						<div class="dataInputField col-sm-1 col-md-1">
							<asp:TextBox ID="MiddleInitialTextBox" runat="server" ClientIDMode="Static" Width="25px" MaxLength="1" /><br/>
							<asp:RegularExpressionValidator runat="server" ID="MiddleInitialRegEx" ControlToValidate="MiddleInitialTextBox" ValidationExpression="^[a-zA-Z]" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Contact" />
						</div>
						<div class="col-sm-2 col-md-3"><focus:LocalisedLabel runat="server" ID="LastNameLabel" AssociatedControlID="LastNameTextBox" CssClass="dataInputLabel requiredData lastName" LocalisationKey="LastName.Label" DefaultText="Last name"/></div>
						<div class="dataInputField">
							<asp:TextBox ID="LastNameTextBox" runat="server" ClientIDMode="Static" Width="200px" MaxLength="20" /><br />
							<asp:RequiredFieldValidator ID="LastNameRequired" runat="server" ControlToValidate="LastNameTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Contact" />
						</div>
				</div>
			</div>
			<div class="dataInputRow">
				<div class="resumeContactLeft col-xs-12 col-sm-12 col-md-5">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="ZipCodeLabel" EnableViewState="True" ClientIDMode="Static" AssociatedControlID="ZipTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="ZipCode.Label" DefaultText="ZIP code"/></div>
					<div class="dataInputField">
						<asp:TextBox ID="ZipTextBox" runat="server" ClientIDMode="Static" Width="280px" MaxLength="10" /><br />
						<asp:CustomValidator ID="ZipRequired" runat="server" ControlToValidate="ZipTextBox" ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Contact" ClientValidationFunction="ValidateZipCode" ValidateEmptyText="true" />
					</div>
				</div>
        <div class="resumeContactRight col-xs-12  col-sm-12 col-md-7">
          <div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="SuffixLabel" AssociatedControlID="SuffixDropDown" CssClass="dataInputLabel" LocalisationKey="Suffix.Label" DefaultText="Suffix"/></div>
          <div class="dataInputField">
					  <asp:DropDownList ID="SuffixDropDown" runat="server" Width="150px" ClientIDMode="Static"></asp:DropDownList>
          </div>
        </div>
			</div>
			<div class="dataInputRow col-sm-12 col-md-5">
				<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="Address1Label" AssociatedControlID="AddressTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="Address1.Label" DefaultText="Address"/></div>
				<div class="dataInputField col-xs-12 col-sm-9 col-md-7">
					<asp:TextBox ID="AddressTextBox" runat="server" ClientIDMode="Static" MaxLength="200" /><br />
					<asp:RequiredFieldValidator ID="AddressRequired" runat="server" ControlToValidate="AddressTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Contact" />
				</div>
			</div>
			<div class="dataInputRow col-sm-12 col-md-5" style="clear:left">
				<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="Address2Label" AssociatedControlID="Address2TextBox" CssClass="dataInputLabel" LocalisationKey="Address2.Label" DefaultText="Address"/></div>
				<div class="dataInputField col-xs-12 col-sm-9 col-md-7">
					<asp:TextBox ID="Address2TextBox" runat="server" ClientIDMode="Static" MaxLength="200" />
				</div>
			</div>
			<div class="dataInputRow" style="clear:left">
				<div class="resumeContactLeft col-xs-12  col-sm-12 col-md-5">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="CityLabel" AssociatedControlID="CityTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="CityLabel" DefaultText="City"/></div>
					<div class="dataInputField">
						<asp:TextBox ID="CityTextBox" runat="server" ClientIDMode="Static" Width="280px" MaxLength="100" /><br />
						<asp:RequiredFieldValidator ID="CityRequired" runat="server" ControlToValidate="CityTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Contact" />
					</div>
				</div>
				<div class="resumeContactRight col-xs-12  col-sm-12 col-md-7" id="tableCounty" runat="server" clientidmode="Static">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="CountyLabel" AssociatedControlID="ddlCounty" CssClass="dataInputLabel requiredData" LocalisationKey="CountyLabel" DefaultText="County"/></div>
					<div class="dataInputField notranslate" id="resumeContactCounty">
						<focus:AjaxDropDownList ID="ddlCounty" runat="server" Width="380px" ClientIDMode="Static" onchange="UpdateStateCountry()"/>
						<ajaxtoolkit:CascadingDropDown ID="ccdCounty" runat="server" Category="County" LoadingText="[Loading counties...]"
							ParentControlID="ddlState" ServiceMethod="GetCounty" ServicePath="~/Services/AjaxService.svc"
							BehaviorID="ccdCounty" TargetControlID="ddlCounty" PromptText="- select county -"
							PromptValue="">
						</ajaxtoolkit:CascadingDropDown>
						<br />
						<asp:RequiredFieldValidator ID="CountyRequired" runat="server" ControlToValidate="ddlCounty" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Contact" />
					</div>
				</div>
			</div>
			<div class="dataInputRow">
				<div class="resumeContactLeft col-xs-12  col-sm-12 col-md-5">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="StateLabel" AssociatedControlID="ddlState" CssClass="dataInputLabel requiredData" LocalisationKey="State.Label" DefaultText="State"/></div>
					<div class="dataInputField notranslate">
						<asp:DropDownList ID="ddlState" runat="server" Width="285px" ClientIDMode="Static" onchange="UpdateCountry();">
							<asp:ListItem Text="- select a state -" />
						</asp:DropDownList>
						<br />
						<asp:RequiredFieldValidator ID="StateRequired" runat="server" ControlToValidate="ddlState" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Contact" />
					</div>
				</div>
				<div class="resumeContactRight col-xs-12  col-sm-12 col-md-7">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="CountryLabel" AssociatedControlID="ddlCountry" CssClass="dataInputLabel requiredData" LocalisationKey="Country.Label" DefaultText="Country"/></div>
					<div class="dataInputField notranslate">
						<asp:DropDownList ID="ddlCountry" runat="server" ClientIDMode="Static" Width="380px" onchange="UpdateStateCounty();">
							<asp:ListItem Text="United States" />
						</asp:DropDownList>
						<br />
						<asp:RequiredFieldValidator ID="CountryRequired" runat="server" ControlToValidate="ddlCountry" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Contact" />
					</div>
				</div>
			</div>
			<div class="dataInputRow">
				<div class="resumeContactLeft col-xs-12  col-sm-12 col-md-5">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="PhoneNumberLabel" AssociatedControlID="PrimaryContactPhoneNoTextBox" CssClass="dataInputLabel requiredData" style= "margin-right:0px" LocalisationKey="PhoneNumber.Label" DefaultText="Phone number"/></div>
					<div class="dataInputField">
						<span class="dataInputGroupedItemHorizontal">
							<asp:TextBox ID="PrimaryContactPhoneNoTextBox" runat="server" ClientIDMode="Static" Width="190px"/>
							<%--<ajaxtoolkit:MaskedEditExtender ID="mePrimaryContactPhoneNo" BehaviorID="meBehaviourPrimaryContactPhoneNo" runat="server"
								Mask="(999) 999-9999" MaskType="Number" TargetControlID="PrimaryContactPhoneNoTextBox" PromptCharacter="_"
								ClearMaskOnLostFocus="false" AutoComplete="false">
							</ajaxtoolkit:MaskedEditExtender>--%>
						</span>
						<span class="dataInputGroupedItemHorizontal">
							<asp:DropDownList ID="PrimaryContactPhoneNoDropDownList" runat="server" Width="85px" ClientIDMode="Static" onchange="ChangePrimaryContactPhoneNoFormat();">
								<asp:ListItem Value="" Text="- select -" />
								<asp:ListItem Value="Home" Text="Home" />
								<asp:ListItem Value="Work" Text="Work" />
								<asp:ListItem Value="Cell" Text="Cell" />
								<asp:ListItem Value="Fax" Text="Fax" />
								<asp:ListItem Value="NonUS" Text="Non US" />
							</asp:DropDownList>
						</span>
						<br />
						<div><asp:CustomValidator ID="PrimaryContactPhoneNoValidator" runat="server" ControlToValidate="PrimaryContactPhoneNoTextBox" 
							ClientIDMode="Static" SetFocusOnError="true" Display="Dynamic" CssClass="error"
							ValidationGroup="Contact" ClientValidationFunction="ValidatePhoneNo" ValidateEmptyText="true" /></div>
						<div><asp:RequiredFieldValidator ID="PhoneTypeSelectedValidator" runat="server" ControlToValidate="PrimaryContactPhoneNoDropDownList" SetFocusOnError="True" Display="Dynamic" CssClass="error" ValidationGroup="Contact"></asp:RequiredFieldValidator></div>
					</div>				  
				</div>
				<div class="resumeContactRight col-xs-12  col-sm-12 col-md-7">
					<div><focus:LocalisedLabel runat="server" ID="AdditionalPhoneNumberLabel" CssClass="additionalPhoneNumberControl" LocalisationKey="AdditionalPhoneNumber.Label" DefaultText="Additional phone number(s)"/></div>
					<asp:Repeater ID="AdditionalNumberRepeater" runat="server" OnItemDataBound="AdditionalNumberRepeater_ItemDataBound">
						<HeaderTemplate><div id="additionalPhoneNumberGroup"></HeaderTemplate>
						<ItemTemplate>
							<div class="additionalPhoneNumberRecord dataInputField dataInputGroupedItemVertical">
								<span class="dataInputGroupedItemHorizontal">
									<asp:TextBox ID="AdditionalNumberTextBox" runat="server" ClientIDMode="AutoID" Width="190px" category="AdditionalPhoneNumber"/>
									<%--<ajaxtoolkit:MaskedEditExtender ID="meAdditionalNumber" BehaviorID="meBehaviourAdditionalNumber"
										ClientIDMode="AutoID" runat="server" Mask="(999) 999-9999" MaskType="Number" TargetControlID="AdditionalNumberTextBox"
										PromptCharacter="_" ClearMaskOnLostFocus="false" AutoComplete="false">
									</ajaxtoolkit:MaskedEditExtender>--%>
								</span>
								<span class="dataInputGroupedItemHorizontal">
									<asp:DropDownList ID="AdditionalNumberDropDownList" runat="server" Width="85px" ClientIDMode="AutoID">
										<asp:ListItem Value="" Text="- select -" />
										<asp:ListItem Value="Home" Text="Home" />
										<asp:ListItem Value="Work" Text="Work" />
										<asp:ListItem Value="Cell" Text="Cell" />
										<asp:ListItem Value="Fax" Text="Fax" />
										<asp:ListItem Value="NonUS" Text="Non US" />
									</asp:DropDownList>
								</span>
								<div>
									<div><asp:CustomValidator ID="AdditionalNumberValidator" runat="server" ControlToValidate="AdditionalNumberTextBox"
										SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Contact"
										ClientValidationFunction="ValidateAdditionalNumber" ValidateEmptyText="true" /></div>
									<div><asp:CustomValidator ID="AdditionalNumberDropDownValidator" runat="server" ControlToValidate="AdditionalNumberDropDownList" SetFocusOnError="True" Display="Dynamic" CssClass="error" ValidationGroup="Contact" ValidateEmptyText="True" ClientValidationFunction="ValidateAdditionalNumberDropDown"></asp:CustomValidator></div>
								</div>
							</div>
						</ItemTemplate>
						<FooterTemplate></div></FooterTemplate>
					</asp:Repeater>
					<div class="additionalPhoneNumberControl"><asp:LinkButton ID="AddAnotherButton" runat="server" OnClick="AddAnotherButton_Click">+ Add another </asp:LinkButton></div>
				</div>
			</div>
            <div class="col-md-12">
            <div class="resumeContactLeft col-xs-12  col-sm-12 col-md-5">
            <p class="instructionalText" style="clear:left"><br/><focus:LocalisedLabel runat="server" ID="EmailAddressInstructionLabel" RenderOuterSpan="False" LocalisationKey="EmailAddressInstructionLabel.Label" DefaultText="The field below populates with the email address you selected for your Account username. You may change it below to use a second email address for your resume."/></p>
            </div>
            <div class="col-md-7"></div></div>
			<div class="dataInputRow">
				<div class="resumeContactLeft col-xs-12  col-sm-12 col-md-5">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="EmailAddressLabel" AssociatedControlID="EmailAddressTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="EmailAddress.Label" DefaultText="Resume email address"/></div>
					<div class="dataInputField">
						<asp:TextBox ID="EmailAddressTextBox" runat="server" ClientIDMode="Static" Width="280px" /><br />
						<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Contact" />
						<focus:InsensitiveRegularExpressionValidator ID="valEmailAddress" runat="server" ControlToValidate="EmailAddressTextBox" CssClass="error" Display="Dynamic" SetFocusOnError="True" ValidationExpression="^([a-z0-9_\-\.\']+)@([a-z0-9_\-\.]+)\.([a-z]{2,5})$" ValidationGroup="Contact"></focus:InsensitiveRegularExpressionValidator>
						<asp:CustomValidator ID="EmailAddressExistsValidator" runat="server" ControlToValidate="EmailAddressTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Contact" ValidateEmptyText="true" />
					</div>
				</div>
				<div class="resumeContactRight col-xs-12  col-sm-12 col-md-7">
					<div class="col-sm-3 col-md-3"><focus:LocalisedLabel runat="server" ID="WebsiteLabel" AssociatedControlID="WebsiteTextBox" CssClass="dataInputLabel" LocalisationKey="Website.Label" DefaultText="LinkedIn profile / Website"/></div>
					<div class="dataInputField">
						<asp:TextBox ID="WebsiteTextBox" runat="server" ClientIDMode="Static" Width="312px" /><br />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton1" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2 greyedOut" ValidationGroup="Contact" OnClick="SaveMoveToNextStepButton_Click" disabled="disabled" ClientIDMode="Static" />
	</div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    MaskPhoneNumber();
    MaskAdditionalPhoneNumber();
    $('#ZipTextBox').change(function() { Contact_GetStateCountyCityAndCountiesForPostalCode($(this).val(), 'ddlState','CityTextBox','ddlCounty','ccdCounty'); });
  });

  function MaskPhoneNumber(){
    $('#PrimaryContactPhoneNoTextBox').mask('(999) 999-9999');
  }
  
  function MaskAdditionalPhoneNumber(){
    $("input[category='AdditionalPhoneNumber']").each(function(){
            $(this).mask('(999) 999-9999');
        }); 
  }
  Sys.Application.add_load(Contact_PageLoad);

  function Contact_PageLoad(sender, args) {
    var isCounty = '<%= IsCountyEnabled %>';

    if (isCounty == 'True') {
      var behavior = $find('<%=ccdCounty.BehaviorID %>');
      if (behavior != null) {
        behavior.add_populated(function() {
          Contact_UpdateCountyDropDown($('#ddlCounty'), $("#ddlState"));
        });
      }
    }

    var countryValue = $('#ddlCountry').val();
	  var zipCodeLabel = $('#ZipCodeLabel');
	  var zipCodeValidator = $('#ZipRequired')[0];

    if (countryValue != <%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>) {
      $(zipCodeLabel).removeClass('requiredData');
      Contact_SetZipMask(false);
      ValidatorEnable(zipCodeValidator, false);
    }
    else {
      $(zipCodeLabel).addClass('requiredData');
      Contact_SetZipMask(true);
    }
  }
  
  function Contact_SetZipMask(isUS) {
    if (isUS) {
      $('#ZipTextBox').mask("99999-9999", { placeholder: ' ', autoclear: false });
    } else {
      $('#ZipTextBox').unmask();
    }
  }

  function Contact_UpdateCountyDropDown(countyDropDown, stateDropDown) {
    if (countyDropDown.children('option:selected').val() != '')
      countyDropDown.next().find(':first-child').text(countyDropDown.children('option:selected').text()).parent().addClass('changed');
    else
      SetCounty(stateDropDown.val() == <%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>);
          
    UpdateStyledDropDown(countyDropDown, stateDropDown.val());
  }

  function ValidateZipCode(sender, args) {
    var countryValue = $('#ddlCountry').val();

    if (args.Value.length == 0) {
      sender.innerHTML = "<%= ZIPCodeRequired %>";
      args.IsValid = false;
    }
    else if (countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>' && !args.Value.match(/^<%=App.Settings.ExtendedPostalCodeRegExPattern %>$/g)) {
      sender.innerHTML = "<%= ZIPCodeLimitError %>";
      args.IsValid = false;
    }
  }

  function ValidatePhoneNo(sender, args) {
    var phoneRegx = /^\([0-9]\d{2}\)\s?\d{3}\-\d{4}$/;
    var phoneValue;
	  var phoneDropDown = $('#PrimaryContactPhoneNoDropDownList');
	  if ($(phoneDropDown).exists())
		  phoneValue = $(phoneDropDown).val();

    if (phoneValue !== 'NonUS') {
      if (args.Value === '(___) ___-____' || args.Value ==='') {
        sender.innerHTML = '<%= PhoneNumberRequired %>';
        args.IsValid = false;
      }
      else if (phoneRegx.test(args.Value) == false) {
        sender.innerHTML = '<%= PhoneNumberError %>';
        args.IsValid = false;
      }
    }
    else if ($('#PrimaryContactPhoneNoTextBox').val() === '                    ' || $('#PrimaryContactPhoneNoTextBox').val() === '') {
      sender.innerHTML = '<%= PhoneNumberRequired %>';
      args.IsValid = false;
    }
    if (args.IsValid === true && phoneValue === 'NonUS') {
	    var hasUsNumber = false;
	    $('#additionalPhoneNumberGroup>.dataInputGroupedItemVertical').each(function() {
		    var additionalPhoneNumberTypeField = $(this).find('select');
		    var additionalPhoneNumberValueField = $(this).find('input:text');
		    if ($(additionalPhoneNumberTypeField).exists() && $(additionalPhoneNumberTypeField).val() !== 'NonUS') {
			    if ($(additionalPhoneNumberValueField).exists() && $(additionalPhoneNumberValueField).val() !== '(___) ___-____' && $(additionalPhoneNumberValueField).val() !== '') {
				    hasUsNumber = true;
			    }
		    }
	    });
	    if (!hasUsNumber) {
		    sender.innerHTML = '<%= AtleastOneUSPhoneNumberRequired %>';
        args.IsValid = false;
	    }
    }
  }

  function ValidateAdditionalNumber(sender, args) {
    if (args.Value != "(___) ___-____" && args.Value != "                    " && args.Value != "") {
      var phoneRegx = /^\([0-9]\d{2}\)\s?\d{3}\-\d{4}$/;
	    var phoneTextBox = $('#' + sender.controltovalidate);
	    var phoneTypeDropDown = $(phoneTextBox).parent().next().find('select');
	    var phoneValue = '';
	    if ($(phoneTypeDropDown).exists())
		    phoneValue = $(phoneTypeDropDown).val();

      if (phoneValue != "NonUS" && phoneRegx.test(args.Value) == false) {
        sender.innerHTML = "<%= PhoneNumberError %>";
        args.IsValid = false;
      }
    }
  }

  function ChangeAdditionalPhoneNoFormat(typeDropDown, valueTextBox) {
    var phoneTypeDropDown = $('#' + typeDropDown);
    var phoneText = $('#' + valueTextBox);
		var phoneValue = $(phoneTypeDropDown).val();
    if (phoneValue != "NonUS") {
      $('#' + valueTextBox).mask("(999) 999-9999");
    }
    else {
      var value = $('#' + valueTextBox).val();
      value = value.replace(/\(|\-|\)|\s/g, '');
      $('#' + valueTextBox).unmask();
      $('#' + valueTextBox).val(value);
    }
	  $(phoneText).focus();
  }
  
  function ChangePrimaryContactPhoneNoFormat() {
	  var phoneValue;
	  var phoneDropDown = $('#PrimaryContactPhoneNoDropDownList');
	  if ($(phoneDropDown).exists())
		  phoneValue = $(phoneDropDown).val();
	  var phoneTextBox = $('#PrimaryContactPhoneNoTextBox');

    if (phoneValue != "NonUS") {
       $('#PrimaryContactPhoneNoTextBox').mask('(999) 999-9999');
    }
    else {
        var value = $('#PrimaryContactPhoneNoTextBox').val();
        value = value.replace(/\(|\-|\)|\s/g, '');
        $('#PrimaryContactPhoneNoTextBox').unmask();
        $('#PrimaryContactPhoneNoTextBox').val(value);
    }
	  $(phoneTextBox).focus();
  }
	
  function ValidateAdditionalNumberDropDown(sender, args) {
    var senderValidator = $('#' + sender.id);
    var phoneText = $(senderValidator).parents(".additionalPhoneNumberRecord").find("input[category='AdditionalPhoneNumber']").val();
    if (args.Value === '' && phoneText != "(___) ___-____" && phoneText != "                    " && phoneText != "") {
      sender.innerHTML = '<%= PhoneTypeRequired %>';
      args.IsValid = false;
    }
    else {
      args.IsValid = true;
    }
  }

  function UpdateCountry() {
    var stateValue = $('#ddlState').val();
	  var zipLabel = $('#ZipCodeLabel');
	  var zipValidator = $('#ZipRequired')[0];

    if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
      $(zipLabel).addClass('requiredData');
	    ValidatorEnable(zipValidator, true);
    }
    else {
      $(zipLabel).removeClass('requiredData');
	    ValidatorEnable(zipValidator, false);
    }

    SetCountry(stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>');
  }

  function UpdateStateCounty() {
    var IsCounty = '<%= IsCountyEnabled %>';
    var countryValue = $('#ddlCountry').val();
    var stateDropdown = $('#ddlState');
	  var stateValue = stateDropdown.val();
	  var zipLabel = $('#ZipCodeLabel');
	  var zipValidator = $('#ZipRequired')[0];

    if (countryValue == '<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>') {
      if (stateValue == '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
        SetState(false);

        if (IsCounty == 'True')
          County_InvokeCountyCascadeDropdown();
      }

      $(zipLabel).addClass('requiredData');
      Contact_SetZipMask(true);
	    ValidatorEnable(zipValidator, true);
    }
    else {
      if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>') {
        SetState(true);

        if (IsCounty == 'True')
          County_InvokeCountyCascadeDropdown();
      }

      $(zipLabel).removeClass('requiredData');
      Contact_SetZipMask(false);
	    ValidatorEnable(zipValidator, false);
    }
  }
  
  function County_InvokeCountyCascadeDropdown() {
    $get("<%= ddlCounty.ClientID %>")._behaviors[0]._onParentChange(null, null);
  }

  function UpdateStateCountry() {
    var countyValue = $('#ddlCounty').val();
	  var stateValue = $('#ddlState').val();

    if (countyValue == '<%= GetLookupId(Constants.CodeItemKeys.Counties.OutsideUS) %>') {
      if (stateValue != '<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>')
        SetState(true);
      SetCountry(true);
    }
  }

  function SetState(outsideUS) {
    var stateDropDown = $('#ddlState');

    if (outsideUS)
	    $(stateDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.States.ZZ) %>"]').prop('selected', true);
    else
      $(stateDropDown).children().first().prop('selected', true);
	  
		$(stateDropDown).next().find(':first-child').text($(stateDropDown).children('option:selected').text()).parent().addClass('changed');
  }

  function SetCounty(outsideUS) {
    var countyDropDown = $('#ddlCounty');

    if (outsideUS)
	    $(countyDropDown).children('option[value="0"]').prop('selected', true);
    else
      $(countyDropDown).children().first().prop('selected', true);
	  
	  $(".CountyClass").next().find(':first-child').text($("#ddlCounty option:selected").text()).parent().addClass('changed');
  }

  function SetCountry(outsideUS) {
    var countryDropDown = $('#ddlCountry');

    if (outsideUS)
      $(countryDropDown).children().first().prop('selected', true);
    else 
	    $(countryDropDown).children('option[value="<%= GetLookupId(Constants.CodeItemKeys.Countries.US) %>"]').prop('selected', true);
	  
	  $(countryDropDown).next().find(':first-child').text($(countryDropDown).children('option:selected').text()).parent().addClass('changed');
  }
	
  function Contact_GetStateCountyCityAndCountiesForPostalCode(postalCode, stateDropDownList, cityTextBox, countyDropDownList, countyCascadingDropDownList) {
    var options = { 
	    type: 'POST',
      url: '<%= UrlBuilder.AjaxService() %>/GetStateCityCountyAndCountiesForPostalCode',
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      async: true,
      data: '{"postalCode": "' + postalCode + '"}',
      success: function (response) {

        var stateDropDown = $("#" + stateDropDownList);
        var countyDropDown = $("#" + countyDropDownList);
        var currentStateId = stateDropDown.val();
        
        var results = response.d;
        if (results && results.StateId) {
          stateDropDown.val(results.StateId);
          stateDropDown.next().find(':first-child').text($("#" + stateDropDownList + " option:selected").text()).parent().addClass('changed');
         
          if (results.CityName != null) {
            $("#" + cityTextBox).val(results.CityName);
          }
        }
        else {
          stateDropDown.val('');
          $("#" + cityTextBox).val('');
        }

        if (stateDropDown.val() != currentStateId) {
          if (results.Counties && results.Counties.length > 0) {
            countyDropDown.html('');
            $('<option><%=HtmlLocalise("Global.County.TopDefault", "- select county -") %></option>').appendTo("#" + countyDropDownList);

            var text = '';
            for (i = 0; i < results.Counties.length; i++) {
              $('<option value="' + results.Counties[i].value + '">' + results.Counties[i].name + '</option>').appendTo("#" + countyDropDownList);
              if (results.Counties[i].value == results.CountyId)
                text = results.Counties[i].name;
            }

            if (results.CountyId) {
              countyDropDown.val(results.CountyId);
              $find(countyCascadingDropDownList).set_SelectedValue(results.CountyId, text);
            }

            countyDropDown.removeAttr('disabled');
          } else {
					  countyDropDown.html('');
					  $('<option><%=HtmlLocalise("Global.County.TopDefault", "- select county -") %></option>').appendTo("#" + countyDropDownList);
					  countyDropDown.val('');
					  countyDropDown.attr('disabled', '');
					  $find(countyCascadingDropDownList).set_SelectedValue('', '');
          }

          Contact_UpdateCountyDropDown(countyDropDown, stateDropDown);
        }
      }
    };

    $.ajax(options);
  }
  //# sourceURL=test1.js
</script>
