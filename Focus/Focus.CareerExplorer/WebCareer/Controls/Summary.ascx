﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Summary.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.Summary" %>
<div class="resumeSection FocusCareer">
	<div class="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2" OnClick="SaveMoveToNextStepButton_Click" ValidationGroup="Summary" />
	</div>
	<div class="resumeSectionContent">
		<p><focus:LocalisedLabel runat="server" ID="SummaryIntroductionLabel" RenderOuterSpan="False" LocalisationKey="SummaryIntroduction.Label" DefaultText="We created this summary for you based on your resume so far. You may use it as is, edit it below, or omit a summary from your resume."/></p>
		<div>
			<asp:RadioButtonList ID="SummaryRadioButtonList" runat="server" Cssclass="dataInputHorizontalRadioFloat" ClientIDMode="Static">
				<asp:ListItem Value="yes" Text="Use this summary" Selected="True" />
				<asp:ListItem Value="no" Text="Do not include a summary in my resume" />
			</asp:RadioButtonList>
		</div>
		<div class="resumeSummary">
			<asp:TextBox ID="SummaryTextBox" runat="server" ClientIDMode="Static" TextMode="MultiLine" Rows="17" onkeydown="SummaryUpdated();" />
			<br />
			<asp:CustomValidator ID="SummaryValidator" runat="server" ControlToValidate="SummaryTextBox" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="Summary" ClientValidationFunction="ValidateSummary" ValidateEmptyText="true" />
			<asp:HiddenField ID="hdnOriginalSummary" runat="server" ClientIDMode="Static" />
		</div>
		<div>
			<button id="RevertToOriginalSummaryButton" class="buttonLevel3 greyedOut buttonLeft" onclick="return RevertToOriginalSummary();">Revert to original summary</button>
		</div>
	</div>
	<div class="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton1" Text="Save &amp; Move to Next Step &#187;" runat="server" class="buttonLevel2" OnClick="SaveMoveToNextStepButton_Click" ValidationGroup="Summary" />
	</div>
</div>

<script type="text/javascript">
	function ValidateSummary(src, args) {
		if ($('#SummaryRadioButtonList input:first').is(':checked')) {
			if (args.Value.length === 0)
				args.IsValid = false;
		}
	}

	function RevertToOriginalSummary() {
		if ($('#hdnOriginalSummary').val() !== '') {
			$('#SummaryTextBox').val($('#hdnOriginalSummary').val());
			$('#RevertToOriginalSummaryButton').addClass('greyedOut');
		}
		return false;
	}

	function SummaryUpdated() {
		$('#RevertToOriginalSummaryButton').removeClass('greyedOut');
	}
</script>
