﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Services.Core.Extensions;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class ReferralsList : UserControlBase
	{
		public long PersonId { get; set; }

		private bool IsBound
		{
			get { return GetViewStateValue("ReferralsList:IsBound", false); }
			set { SetViewStateValue("ReferralsList:IsBound", value); }
		}

		/// <summary>
		/// Constructor
		/// </summary>
		public ReferralsList()
		{
			PersonId = App.User.PersonId.GetValueOrDefault(0);
			IsBound = false;
		}

		#region Events

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (!IsBound)
			{
				BindDropDowns();
				BindReferralsList();

				IsBound = true;
			}
		}

		/// <summary>
		/// Handles the OnSelectedIndexChanged event of the NumberOfReferralsDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void NumberOfReferralsDropDown_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			BindReferralsList();
		}

		/// <summary>
		/// Handles the OnItemDataBound event of the ReferralsListView control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ReferralsListView_OnItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var referral = (ReferralViewDto)e.Item.DataItem;

			BindScore(e.Item, referral.CandidateApplicationScore);

			var jobTitleLink = (HyperLink)e.Item.FindControl("JobLink");
			jobTitleLink.Text = Server.HtmlEncode(referral.JobTitle);
			jobTitleLink.NavigateUrl = UrlBuilder.JobPosting(referral.LensPostingId, "Home", "ReferralsList");

			if (referral.CandidateApplicationAutomaticallyApproved.IsNotNull() && !(bool)referral.CandidateApplicationAutomaticallyApproved)
			{
				var openBracketLiteral = (Literal)e.Item.FindControl("OpenBracketLiteral");
				openBracketLiteral.Text = @"(";

				var statusLiteral = (Literal)e.Item.FindControl("ApprovalStatus");
				statusLiteral.Text = CodeLocalise(referral.ApplicationApprovalStatus, true);

				var closeBracketLiteral = (Literal)e.Item.FindControl("CloseBracketLiteral");
				closeBracketLiteral.Text = @")";
			}

			var dateLiteral = (Literal)e.Item.FindControl("ApplicationDate");
			dateLiteral.Text = referral.ApplicationDate.ToString("MMM dd");

			var employerLiteral = (Literal)e.Item.FindControl("EmployerName");

			if (referral.EmployerName.IsNullOrEmpty())
			{
				var employerDashPlaceHolder = (PlaceHolder)e.Item.FindControl("EmployerDashPlaceHolder");
				employerDashPlaceHolder.Visible = false;
			}
			else
			{
				var showEmployerDetails = referral.ApplicationApprovalStatus.IsIn(ApprovalStatuses.Approved, ApprovalStatuses.None) || referral.PostingEmployerName.IsNullOrEmpty();
				employerLiteral.Text = Server.HtmlEncode(showEmployerDetails ? referral.EmployerName : referral.PostingEmployerName);
			}

			var locationLiteral = (Literal)e.Item.FindControl("Location");

			locationLiteral.Text = referral.JobId.IsNotNull()
															 ? Server.HtmlEncode(referral.Location)
															 : HtmlLocalise("ExternalJob.Text", "External posting");

			var locationDashPlaceHolder = (PlaceHolder)e.Item.FindControl("LocationDashPlaceHolder");

			if (referral.Location.IsNullOrEmpty() && referral.JobId.IsNotNull())
			{
				locationDashPlaceHolder.Visible = false;
			}

			var isConfidential = referral.IsConfidential.GetValueOrDefault(false);

			//TODO: This code could do with some tidying up in conjunction with the code block with the comment FVN-2375.  
			// Shows Contact infomation only for Approved applications and currently on-hold that have previously been approved.
      if (referral.ApplicationApprovalStatus.IsIn(ApprovalStatuses.Approved, ApprovalStatuses.None) 
		    	|| (referral.PreviousApprovalStatus.IsNotNull() && (referral.ApplicationApprovalStatus == ApprovalStatuses.OnHold && referral.PreviousApprovalStatus == ApprovalStatuses.Approved)) 
					|| !isConfidential //TODO: contact information is going to get set up for any referral that isn't confidential regardless of the approval status
					|| App.User.IsShadowingUser)
      {
				employerLiteral.Text = Server.HtmlEncode(referral.EmployerName);

				if (referral.InterviewContactPreferences.IsNotNull())
				{
					var commaLabel = (Label)e.Item.FindControl("CommaLabel");
					var employeeLiteral = (Literal)e.Item.FindControl("EmployeeName");
					var phoneLiteral = (Literal)e.Item.FindControl("EmployeePhone");
					var emailLink = (HyperLink)e.Item.FindControl("EmployeeEmail");

					employeeLiteral.Visible = false;
					phoneLiteral.Visible = false;
					emailLink.Visible = false;
					commaLabel.Visible = false;

					var contactPreferences = BuildContactPreferences(referral);
					var contactPreferencesLabel = (Label)e.Item.FindControl("ContactPreferencesLabel");
					var showAdditionalContactPreferencesLink = (HyperLink)e.Item.FindControl("ShowAdditionalContactPreferencesLink");

					if (contactPreferences.Count < 4)
					{
						contactPreferencesLabel.Text = string.Join(", ", contactPreferences);
					}
					else
					{
						const string comma = ", ";
						contactPreferencesLabel.Text = string.Concat(string.Join(comma, contactPreferences.Take(2)), comma);
						var additionalContactPreferencesLiteral = (Literal)e.Item.FindControl("AdditionalContactPreferencesLiteral");
						additionalContactPreferencesLiteral.Text = string.Join(", ", contactPreferences.Skip(2));

						showAdditionalContactPreferencesLink.Visible = true;
						showAdditionalContactPreferencesLink.Text = CodeLocalise("AdditionalIssuesLink.Text", "+ {0} more",
							(contactPreferences.Count - 2));
						showAdditionalContactPreferencesLink.Attributes.Add("onclick",
							string.Format("ShowAdditionalContactPreferences({0});",
								referral.Id.ToString()));
						showAdditionalContactPreferencesLink.Attributes.Add("onkeypress",
							string.Format("ShowAdditionalContactPreferences({0});",
								referral.Id.ToString()));
					}
				}
				else if (referral.EmployeeName.IsNotNullOrEmpty())
				{
					var employeeLiteral = (Literal)e.Item.FindControl("EmployeeName");
					employeeLiteral.Text = Server.HtmlEncode(referral.EmployeeName);

					var phoneLiteral = (Literal)e.Item.FindControl("EmployeePhone");

					if (referral.EmployeePhoneNumber.IsNotNullOrEmpty())
						phoneLiteral.Text = string.Concat(Server.HtmlEncode(referral.EmployeePhoneNumber), ",");
					else
						phoneLiteral.Visible = false;

					if (referral.EmployeeEmail.IsNotNullOrEmpty())
					{
						var emailLink = (HyperLink)e.Item.FindControl("EmployeeEmail");
						emailLink.NavigateUrl = string.Format("mailto:{0}?subject={1}", Server.HtmlEncode(referral.EmployeeEmail), Server.HtmlEncode(referral.JobTitle));
						emailLink.Text = Server.HtmlEncode(referral.EmployeeEmail);
					}
				}
				else
				{
					var contactPlaceholder = (PlaceHolder)e.Item.FindControl("ContactPlaceHolder");
					contactPlaceholder.Visible = false;
				}
			}
			else
			{
				var contactPlaceholder = (PlaceHolder)e.Item.FindControl("ContactPlaceHolder");
				contactPlaceholder.Visible = false;
			}

			// FVN-2375 - Do not display contact details if the application is not approved
			if (referral.ApplicationApprovalStatus.IsNotIn(ApprovalStatuses.Approved, ApprovalStatuses.None))
			{
				var contactPlaceholder = (PlaceHolder)e.Item.FindControl("ContactPlaceHolder");
				contactPlaceholder.Visible = false;
				var approvalLiteral = (Literal)e.Item.FindControl("ApprovalText");

				approvalLiteral.Text = HtmlLocalise("ReferralNotApproved.Text", "Interview contact information will only be displayed on approved referral requests");
				approvalLiteral.Visible = true;
			}
		}

		#endregion

		#region Private Methods

		private List<string> BuildContactPreferences(ReferralViewDto job)
		{
			var preferenceList = new List<string>();
			var contactPreferences = job.InterviewContactPreferences;

			if ((contactPreferences & ContactMethods.FocusTalent) == ContactMethods.FocusTalent)
			{
				var contactVia = " Via #FOCUSTALENT#";
				contactVia = contactVia.Replace(Constants.PlaceHolders.FocusTalent, App.Settings.FocusTalentApplicationName);
				preferenceList.Add(CodeLocalise(ContactMethods.FocusTalent, contactVia));
			}

			if ((contactPreferences & ContactMethods.Mail) == ContactMethods.Mail)
			{
				preferenceList.Add(CodeLocalise(ContactMethods.Mail, String.Format("Mail: {0}",job.InterviewMailAddress)));
			}


			if ((contactPreferences & ContactMethods.Email) == ContactMethods.Email)
			{
				preferenceList.Add(CodeLocalise(ContactMethods.Email, String.Format("Email: {0}", job.InterviewEmailAddress)));
			}


			if ((contactPreferences & ContactMethods.Fax) == ContactMethods.Fax)
			{
				preferenceList.Add(CodeLocalise(ContactMethods.Fax, String.Format("Fax: {0}", job.InterviewFaxNumber)));
			}


			if ((contactPreferences & ContactMethods.InPerson) == ContactMethods.InPerson)
			{
				preferenceList.Add(CodeLocalise(ContactMethods.InPerson, String.Format("In Person: {0}", job.InterviewDirectApplicationDetails)));
			}

			if ((contactPreferences & ContactMethods.Online) == ContactMethods.Online)
			{
				preferenceList.Add(CodeLocalise(ContactMethods.Online, String.Format("Online: {0}", job.InterviewApplicationUrl)));
			}

			if ((contactPreferences & ContactMethods.Telephone) == ContactMethods.Telephone)
			{
				preferenceList.Add(CodeLocalise(ContactMethods.Telephone, String.Format("Phone: {0}", job.InterviewPhoneNumber)));
			}

			if (job.InterviewOtherInstructions.IsNotNullOrEmpty())
			{
				preferenceList.Add(CodeLocalise("InteviewOtherInstructions", String.Format("Other: {0}", job.InterviewOtherInstructions)));
			}

			return preferenceList;
		}

		/// <summary>
		/// Binds the drop-downs in the controls
		/// </summary>
		private void BindDropDowns()
		{
			NumberOfReferralsDropDown.Items.AddLocalised("FiveMostRecentReferrals.Option", "5 most recent", "5");
			NumberOfReferralsDropDown.Items.AddLocalised("TwentyMostRecentReferrals.Option", "20 most recent", "20");
			NumberOfReferralsDropDown.Items.AddLocalised("FiftyMostRecentReferrals.Option", "50 most recent", "50");
			NumberOfReferralsDropDown.Items.AddLocalised("AllReferrals.Option", "All", "-1");
		}

		/// <summary>
		/// Binds the referrals list
		/// </summary>
		private void BindReferralsList()
		{
			if (App.User.IsAuthenticated)
			{
				var dropdownValue = NumberOfReferralsDropDown.SelectedValueToInt();
				var referralsRequired = dropdownValue == -1 ? (int?)null : dropdownValue;

				ReferralsListView.DataSource = ServiceClientLocator.CandidateClient(App).GetReferralViewList(PersonId, referralsRequired, dayLimit: 30);
				ReferralsListView.DataBind();

				if (ReferralsListView.Items.Count == 0)
				{
					NoReferralsPanel.Visible = true;
					ReferralsPanel.Visible = false;
					NoResultsLabel.Text = (App.Settings.Theme == FocusThemes.Workforce)
																	? HtmlLocalise("NoResultsLabel.Text", "No referrals found")
																	: HtmlLocalise("NoResultsLabel.Text.Education", "No applications found");
				}
				else
				{
					NoReferralsPanel.Visible = false;
					ReferralsPanel.Visible = true;
				}
			}
			else
			{
				NoReferralsPanel.Visible = false;
				ReferralsPanel.Visible = false;
			}
		}

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="listViewItem">The list view item holding the referral</param>
		/// <param name="score">The score.</param>
		private static void BindScore(ListViewItem listViewItem, int score)
		{
			var starRating = score.ToStarRating(OldApp_RefactorIfFound.Settings.StarRatingMinimumScores);

			for (var imageNumber = 1; imageNumber <= 5; imageNumber++)
			{
				var scoreImage = (Image)listViewItem.FindControl(string.Concat("ReferralScoreImage", imageNumber));
				scoreImage.ImageUrl = imageNumber <= starRating ? UrlBuilder.StarOn() : UrlBuilder.StarOff();
			}
		}

		#endregion
	}
}