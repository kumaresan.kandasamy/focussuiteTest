﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.Common;
using Focus.Common.Extensions;
using Focus.Common.Helpers;
using Focus.Core;
using Focus.Core.Models.Career;

using Framework.Core;

using AjaxControlToolkit;
using Framework.DataAccess;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class MilitaryHistory : UserControlBase
  {
    private List<VeteranHistory> _veteranHistory = new List<VeteranHistory>();

    protected void Page_Load(object sender, EventArgs e)
    {
			ScriptManager.RegisterClientScriptInclude(this, GetType(), "resumeBuilderMilitaryHistoryScriptInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.ResumeBuilder.MilitaryHistory.min.js"));
			RegisterCodeValuesJson("resumeBuilderMilitaryHistoryScriptValues", "resumeMilitaryHistoryCodeValues", InitialiseClientSideCodeValues());
		}

    protected void Page_PreRender(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallMilitaryRepeaterItems", "MilitaryRepeaterItem();", true);
    }

    /// <summary>
    /// Handles the ItemDataBound event of the MilitaryHistoryRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void MilitaryHistoryRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        return;

      var historyItem = (VeteranHistory)e.Item.DataItem;

      // Get Controls
      var historyHeaderLiteral = ((Literal)e.Item.FindControl("HistoryHeader"));
      var veteranTypeDropDown = ((DropDownList)e.Item.FindControl("ddlVeteranType"));
      var militaryEmploymentStatusDropDown = ((DropDownList)e.Item.FindControl("ddlMilitaryEmploymentStatus"));
      var transitioningTypeDropDown = ((DropDownList)e.Item.FindControl("ddlTransitioningType"));
      var startDateTextBox = ((TextBox)e.Item.FindControl("SDSDTextBox"));
      var endDateTextBox = ((TextBox)e.Item.FindControl("PEDTextBox"));
      var campaignVeteranButtons = ((RadioButtonList)e.Item.FindControl("rblCampaignVeteran"));
      var serviceDisabilityDropDown = ((DropDownList)e.Item.FindControl("ddlServiceDisability"));
      var custServiceDisability = ((CustomValidator) e.Item.FindControl("custServiceDisability"));
      var plannedEndDateLabel = ((Label)e.Item.FindControl("lblPlannedEndDate"));
      var militaryDischargeDropDown = ((DropDownList)e.Item.FindControl("ddlMilitaryDischarge"));
      var custMilitaryDischarge = ((CustomValidator)e.Item.FindControl("custMilitaryDischarge"));
      var branchOfServiceDropDown = ((DropDownList)e.Item.FindControl("ddlBranchOfService"));
      var rankDropDown = ((DropDownList)e.Item.FindControl("ddlRank"));
      var ccdRank = ((CascadingDropDown)e.Item.FindControl("ccdRank"));
      var militaryOccupationTitleTextBox = ((TextBox)e.Item.FindControl("txtMilitaryOccupationTitle"));
      var unitTextBox = ((TextBox)e.Item.FindControl("txtUnit"));
      var addHistoryButton = ((LinkButton)e.Item.FindControl("AddHistoryButton"));
      var deleteHistoryButton = ((LinkButton)e.Item.FindControl("DeleteHistoryButton"));
      var multipleHeaderPlaceHolder = ((PlaceHolder)e.Item.FindControl("multipleHeaderPlaceHolder"));

      // Localise controls
      historyHeaderLiteral.Text = e.Item.ItemIndex == 0
                                    ? CodeLocalise("HistoryHeader.Latest", "Latest")
                                    : CodeLocalise("HistoryHeader.Previous", "Previous");
      addHistoryButton.Text = CodeLocalise("AddHistoryButton.Text", " + Add");
      deleteHistoryButton.Text = CodeLocalise("DeleteHistoryButton.Text", " - Delete");
      custServiceDisability.ErrorMessage = CodeLocalise("ServiceDisability.RequiredErrorMessage", "Service-connected disability is required");
      custMilitaryDischarge.ErrorMessage = CodeLocalise("MilitaryDischarge.RequiredErrorMessage", "Military discharge is required");
      startDateTextBox.ToolTip = CodeLocalise("SDSDTextBox.ToolTip", "Please enter the service duty start date as mm/dd/yyyy");
      endDateTextBox.ToolTip = CodeLocalise("PEDTextBox.ToolTip", "Please enter the service duty end date as mm/dd/yyyy");

      for (var index = 0; index <= 1; index++ )
        campaignVeteranButtons.Items[index].Attributes.Add("data-name", string.Concat("rblCampaignVeteran_", e.Item.ItemIndex));

      // Bind dropdowns
      branchOfServiceDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MilitaryBranchesOfService), null, CodeLocalise("BranchOfService.TopDefault", "- select branch -"));
      militaryDischargeDropDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MilitaryDischargeTypes), null, CodeLocalise("MilitaryDischarge.TopDefault", "- select discharge -"));

      BindVeteranTypeDropDown(veteranTypeDropDown);
      BindRankDropDown(rankDropDown, ccdRank);
      BindServiceDisabilityDropDown(serviceDisabilityDropDown);
      BindMilitaryEmploymentStatusDropDown(militaryEmploymentStatusDropDown);
      BindTransitioningTypeDropDown(transitioningTypeDropDown);

      // Add/Delete buttons
      multipleHeaderPlaceHolder.Visible = App.Settings.AllowDateBoundMilitaryService;

      addHistoryButton.CommandArgument =
        deleteHistoryButton.CommandArgument = e.Item.ItemIndex.ToString(CultureInfo.InvariantCulture);
      if (e.Item.ItemIndex == 0 && _veteranHistory.Count <= 1)
        deleteHistoryButton.Visible = false;
			if (e.Item.ItemIndex > 0 || (App.Settings.MaxDateBoundMilitaryServicePeriods > 0 && App.Settings.MaxDateBoundMilitaryServicePeriods <= _veteranHistory.Count))
        addHistoryButton.Visible = false;

      // Bind controls to data
      if (historyItem.VeteranEra == VeteranEra.TransitioningServiceMemberSpouse || historyItem.VeteranEra == VeteranEra.TransitioningVietnamServiceMember)
        veteranTypeDropDown.SelectValue(VeteranEra.TransitioningServiceMember.ToString());
      else if (historyItem.VeteranEra == VeteranEra.OtherVet || historyItem.VeteranEra == VeteranEra.Vietnam)
        veteranTypeDropDown.SelectValue(VeteranEra.OtherVet.ToString());
      else
        veteranTypeDropDown.SelectValue(historyItem.VeteranEra.ToString());
			
      if (veteranTypeDropDown.SelectedValueToEnum<VeteranEra>() == VeteranEra.OtherVet)
      {
        plannedEndDateLabel.Text = CodeLocalise("ServiceDutyEndDate.Text", "Service duty end date");
      }
      else if (veteranTypeDropDown.SelectedValueToEnum<VeteranEra>() == VeteranEra.TransitioningServiceMember)
      {
        plannedEndDateLabel.Text = CodeLocalise("PlannedEndDate.Text", "Planned end date");
      }
			
      if (historyItem.TransitionType.HasValue)
        transitioningTypeDropDown.SelectValue(historyItem.TransitionType.ToString());
      else
        transitioningTypeDropDown.SelectedIndex = 0;

			if (historyItem.VeteranStartDate.HasValue && historyItem.VeteranStartDate.Value != DateTime.MinValue)
        startDateTextBox.Text = ((DateTime)historyItem.VeteranStartDate).ToString("MM/dd/yyyy");

			if (historyItem.VeteranEndDate.HasValue && historyItem.VeteranEndDate.Value != DateTime.MinValue)
        endDateTextBox.Text = ((DateTime)historyItem.VeteranEndDate).ToString("MM/dd/yyyy");

      if (historyItem.IsCampaignVeteran.HasValue && (bool)historyItem.IsCampaignVeteran)
        campaignVeteranButtons.SelectedIndex = 0;
      else
        campaignVeteranButtons.SelectedIndex = 1;

      if (historyItem.VeteranDisabilityStatus.HasValue)
        serviceDisabilityDropDown.SelectValue(historyItem.VeteranDisabilityStatus.ToString());
      else
        serviceDisabilityDropDown.SelectedIndex = 0;

      if (historyItem.EmploymentStatus.HasValue)
        militaryEmploymentStatusDropDown.SelectValue(historyItem.EmploymentStatus.ToString());
      else
        militaryEmploymentStatusDropDown.SelectedIndex = 0;

      if (historyItem.MilitaryDischargeId.HasValue)
        militaryDischargeDropDown.SelectValue(historyItem.MilitaryDischargeId.ToString());
      else
        militaryDischargeDropDown.SelectedIndex = 0;

      ccdRank.BehaviorID = string.Concat("ccdRank_", e.Item.ItemIndex);
      if (historyItem.MilitaryBranchOfServiceId.HasValue)
      {
        branchOfServiceDropDown.SelectValue(historyItem.MilitaryBranchOfServiceId.ToString());
        rankDropDown.Items.Clear();

        BindRankDropDown(rankDropDown, ccdRank, historyItem.MilitaryBranchOfServiceId);

        if (historyItem.RankId.HasValue)
        {
          rankDropDown.SelectValue(historyItem.RankId.ToString());
          ccdRank.SelectedValue = historyItem.RankId.ToString();
        }
        else
        {
          rankDropDown.SelectValue("");
          ccdRank.SelectedValue = "";
        }

        if (historyItem.MilitaryOccupation.IsNotNullOrEmpty() && historyItem.MilitaryOccupationCode.IsNotNullOrEmpty() && historyItem.MilitaryOccupation != historyItem.MilitaryOccupationCode)
          militaryOccupationTitleTextBox.Text = string.Format("{0} - {1}", historyItem.MilitaryOccupationCode, historyItem.MilitaryOccupation);
        else if (historyItem.MilitaryOccupationCode.IsNotNullOrEmpty())
          militaryOccupationTitleTextBox.Text = historyItem.MilitaryOccupationCode;

        if (historyItem.UnitOrAffiliation.IsNotNullOrEmpty())
          unitTextBox.Text = historyItem.UnitOrAffiliation;
      }
      else
        branchOfServiceDropDown.SelectedIndex = 0;
    }

    /// <summary>
    /// Handles the ItemCommand event of the MilitaryHistoryRepeater control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
    protected void MilitaryHistoryRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
      var selectedRow = e.CommandArgument.AsInt();
      _veteranHistory = UnbindHistory();

      switch (e.CommandName)
      {
        case "Delete":
          _veteranHistory.RemoveAt(selectedRow);
          if (!_veteranHistory.Any())
            _veteranHistory.Add(new VeteranHistory());
          break;
        case "Add":
          _veteranHistory.Add(new VeteranHistory());
          break;
      }
      BindHistory();
    }

    /// <summary>
    /// Binds the control on the page
    /// </summary>
    /// <param name="veteran">The object containing veteran information</param>
    public void Bind(VeteranInfo veteran)
    {
      if (veteran.IsNotNull())
      {
        _veteranHistory = App.Settings.AllowDateBoundMilitaryService
                            ? veteran.History
                            : veteran.History.Take(1).ToList();
      }

      BindHistory();
    }


    /// <summary>
    /// Updates the model with veteran informatiom
    /// </summary>
    /// <param name="veteran">The veteran object to update</param>
    public void SaveModel(VeteranInfo veteran)
    {
      _veteranHistory = UnbindHistory();
      if (veteran.IsNotNull())
        veteran.History = _veteranHistory;

      var firstHistory = _veteranHistory[0];
      if (firstHistory.MilitaryOccupationCode.IsNotNullOrEmpty())
      {
        string mocTitle = null;
        if (firstHistory.MilitaryOccupation.IsNotNullOrEmpty() && firstHistory.MilitaryOccupationCode.IsNotNullOrEmpty() && firstHistory.MilitaryOccupation != firstHistory.MilitaryOccupationCode)
          mocTitle = string.Format("{0} - {1}", firstHistory.MilitaryOccupationCode, firstHistory.MilitaryOccupation);
        else if (firstHistory.MilitaryOccupationCode.IsNotNullOrEmpty())
          mocTitle = firstHistory.MilitaryOccupationCode;

        var occupations = ServiceClientLocator.OccupationClient(App).MOCJobTitles(mocTitle, 0);
        if (occupations.Any())
        {
          if (occupations.Exists(x => x.CareerArea.IsNotNullOrEmpty()))
          {
            veteran.MOCRonetInfo = new Dictionary<string, int>();
            foreach (var occupation in occupations)
            {
              var ronet = System.Text.RegularExpressions.Regex.Replace(occupation.RonetCode, @"[^0-9]+", "");
              if (!veteran.MOCRonetInfo.ContainsKey(ronet))
                veteran.MOCRonetInfo.Add(ronet, occupation.StarRating.HasValue ? (int)occupation.StarRating : 0);
            }
            //veteran.MOCRonet = string.Join<string>(",", occupations.Select(x => x.RonetCode).Distinct());
            //veteran.MOCRonetStarRating = occupations[0].StarRating;
          }
          //else
          //{
          //  var twoStarRonetList = ServiceClientLocator.CoreClient(App).GetLookup(Constants.DomainValues.TwoStarROnetsList);
          //  var twoStarRonetOfficers = ServiceClientLocator.CoreClient(App).GetLookup(Constants.DomainValues.TwoStarROnetsOfficer);
          //  var twoStarAllROnets = twoStarRonetList.Concat(twoStarRonetOfficers);
          //  veteran.MOCRonet = string.Join<string>(",", twoStarAllROnets.Select(x => x.Id).Distinct());
          //  veteran.MOCRonetStarRating = 2;
          //}
        }
      }
    }

    /// <summary>
    /// Gets the latest military employment status
    /// </summary>
    /// <returns></returns>
    public string GetLatestEmploymentStatus()
    {
      var items = MilitaryHistoryRepeater.Items;
      if (items.Count > 0)
      {
        var item = items[0];
        var militaryEmploymentStatusDropDown = ((DropDownList)item.FindControl("ddlMilitaryEmploymentStatus"));

        return militaryEmploymentStatusDropDown.SelectedValue;
      }

      return null;
    }

    private void BindHistory()
    {
      if (!_veteranHistory.Any())
        _veteranHistory.Add(new VeteranHistory());

      MilitaryHistoryRepeater.DataSource = _veteranHistory;
      MilitaryHistoryRepeater.DataBind();
    }

    private List<VeteranHistory> UnbindHistory()
    {
      var militaryHistory = new List<VeteranHistory>();

      foreach (RepeaterItem item in MilitaryHistoryRepeater.Items)
      {
        var history = new VeteranHistory();
        militaryHistory.Add(history);

        // Get Controls
        var veteranTypeDropDown = ((DropDownList)item.FindControl("ddlVeteranType"));
        var militaryEmploymentStatusDropDown = ((DropDownList)item.FindControl("ddlMilitaryEmploymentStatus"));
        var transitioningTypeDropDown = ((DropDownList)item.FindControl("ddlTransitioningType"));
        var startDateTextBox = ((TextBox)item.FindControl("SDSDTextBox"));
        var endDateTextBox = ((TextBox)item.FindControl("PEDTextBox"));
        var campaignVeteranButtons = ((RadioButtonList)item.FindControl("rblCampaignVeteran"));
        var serviceDisabilityDropDown = ((DropDownList)item.FindControl("ddlServiceDisability"));
        var militaryDischargeDropDown = ((DropDownList)item.FindControl("ddlMilitaryDischarge"));
        var branchOfServiceDropDown = ((DropDownList)item.FindControl("ddlBranchOfService"));
        var rankDropDown = ((DropDownList)item.FindControl("ddlRank"));
        var militaryOccupationTitleTextBox = ((TextBox)item.FindControl("txtMilitaryOccupationTitle"));
        var unitTextBox = ((TextBox)item.FindControl("txtUnit"));

        if (militaryEmploymentStatusDropDown.SelectedValue.Length > 0)
          history.EmploymentStatus = (EmploymentStatus)Enum.Parse(typeof(EmploymentStatus), militaryEmploymentStatusDropDown.SelectedValue, true);

        DateTime? startDate = null;
				DateTime? endDate = null;

        //To check if provided start and & dates overlap with veteran start & end dates, hence following conditions are applied
        //Threshold:       |---------------|
        //Condition 1: |---------|
        //Condition 2:                |---------|
        //Condition 3:        |---------|
        //Condition 4:   |-------------------|

        var isVietnam = false;
        if (startDateTextBox.Text != "" && startDateTextBox.Text != EmptyDateString && endDateTextBox.Text != "" && endDateTextBox.Text != EmptyDateString)
        {
          startDate = Convert.ToDateTime(startDateTextBox.Text);
          endDate = Convert.ToDateTime(endDateTextBox.Text);

          var vetstartdate = Convert.ToDateTime(App.Settings.VietmanWarStartDate);
          var vetenddate = Convert.ToDateTime(App.Settings.VietmanWarEndDate);

          isVietnam = startDate <= vetenddate && endDate >= vetstartdate;
        }

        if (veteranTypeDropDown.SelectedValueToEnum<VeteranEra>() != VeteranEra.TransitioningServiceMember)
        {
          if (veteranTypeDropDown.SelectedValueToEnum<VeteranEra>() == VeteranEra.OtherVet)
          {
            history.VeteranEra = isVietnam ? VeteranEra.Vietnam : veteranTypeDropDown.SelectedValueToEnum<VeteranEra>();
          }
          else
            history.VeteranEra = veteranTypeDropDown.SelectedValueToEnum<VeteranEra>();
        }
        else
        {
	        history.VeteranEra = VeteranEra.TransitioningServiceMember;
        }

        if (veteranTypeDropDown.SelectedValueToEnum<VeteranEra>() == VeteranEra.OtherVet)
        {
          history.VeteranStartDate = startDate;
          history.VeteranEndDate = endDate;

          history.IsCampaignVeteran = campaignVeteranButtons.SelectedIndex == 0;
          history.VeteranDisabilityStatus = serviceDisabilityDropDown.SelectedValueToEnum<VeteranDisabilityStatus>();
        }

        if (veteranTypeDropDown.SelectedValueToEnum<VeteranEra>() == VeteranEra.TransitioningServiceMember)
        {
          if (transitioningTypeDropDown.SelectedValueToEnum<VeteranTransitionType>() != VeteranTransitionType.Spouse)
          {
            history.VeteranStartDate = startDate;
            history.VeteranEndDate = endDate;

            history.IsCampaignVeteran = campaignVeteranButtons.SelectedIndex == 0;

            history.VeteranDisabilityStatus = serviceDisabilityDropDown.SelectedValueToEnum<VeteranDisabilityStatus>();

            if (isVietnam)
            {
              history.TransitionType = transitioningTypeDropDown.SelectedValueToEnum<VeteranTransitionType>();
              history.VeteranEra = VeteranEra.TransitioningVietnamServiceMember;
            }
            else if (transitioningTypeDropDown.SelectedValueToEnum<VeteranTransitionType>() == VeteranTransitionType.Retirement || transitioningTypeDropDown.SelectedValueToEnum<VeteranTransitionType>() == VeteranTransitionType.Discharge)
            {
              history.TransitionType = transitioningTypeDropDown.SelectedValueToEnum<VeteranTransitionType>();
              history.VeteranEra = VeteranEra.TransitioningServiceMember;
            }
          }
          else if (transitioningTypeDropDown.SelectedValueToEnum<VeteranTransitionType>() == VeteranTransitionType.Spouse)
          {
            history.TransitionType = transitioningTypeDropDown.SelectedValueToEnum<VeteranTransitionType>();
            history.VeteranEra = VeteranEra.TransitioningServiceMemberSpouse;
          }
        }

        history.MilitaryDischargeId = militaryDischargeDropDown.SelectedValueToLong();
        history.MilitaryBranchOfServiceId = branchOfServiceDropDown.SelectedValueToLong();
        history.RankId = rankDropDown.SelectedValueToLong();
        history.UnitOrAffiliation = unitTextBox.TextTrimmed(defaultValue: null);

        history.MilitaryOccupationCode = null;
        history.MilitaryOccupation = militaryOccupationTitleTextBox.TextTrimmed();

        var militaryOccupation = history.MilitaryOccupation.ToLower();

        if (militaryOccupation.IsNotNullOrEmpty())
        {
          var mocTitles = ServiceClientLocator.OccupationClient(App).GetMilitaryOccupationJobTitles(militaryOccupation, 200);
          if (mocTitles.Count > 0)
          {
            var selectedMOC = mocTitles.FirstOrDefault(x => x.JobTitle.Trim().ToLower() == militaryOccupation);
            if (selectedMOC.IsNotNull())
            {
              history.MilitaryOccupationCode = selectedMOC.JobTitle.Substring(0, selectedMOC.JobTitle.IndexOf(" - ", StringComparison.Ordinal));
              history.MilitaryOccupation = selectedMOC.JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ", StringComparison.Ordinal) + 3);
            }
            else
            {
              selectedMOC = mocTitles.First();
              history.MilitaryOccupation = selectedMOC.JobTitle.Substring(0, selectedMOC.JobTitle.IndexOf(" - ", StringComparison.Ordinal));
              if (mocTitles.Count() == 1)
                history.MilitaryOccupation = selectedMOC.JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ", StringComparison.Ordinal) + 3);
              else
              {
                var branchOfServiceId = history.MilitaryBranchOfServiceId;

                if (branchOfServiceId.IsNotNull() && branchOfServiceId > 0)
                {
                  var mocs = mocTitles.Where(x => x.BranchOfServiceId == branchOfServiceId).ToList();

                  if (mocs.IsNotNullOrEmpty() && mocs.Count == 1)
                    history.MilitaryOccupation = mocs.First().JobTitle.Substring(selectedMOC.JobTitle.IndexOf(" - ", StringComparison.Ordinal) + 3);
                }
              }
            }
          }
        }
      }

      return militaryHistory;
    }

    /// <summary>
    /// Initialises the client side code values.
    /// </summary>
    /// <returns></returns>
    private NameValueCollection InitialiseClientSideCodeValues()
    {
      var nvc = new NameValueCollection
      {
        { "MilitaryHistoryRepeaterClientId", MilitaryHistoryRepeater.ClientID },
        //{ "ccdRankBehaviorId", ccdRank.BehaviorID },
        //{ "aceMilitaryOccupationTitleClientId", aceMilitaryOccupationTitle.ClientID },
				{ "errorDateFormat", CodeLocalise("DateFormat.ErrorMessage", "Valid date is required") },
				{ "errorMilitaryEmploymentStatus", CodeLocalise("MilitaryEmploymentStatus.ErrorMessage", "Transitioning Service Member must be Employed with Notice of Termination") },
	    	{ "errorMilitaryEmploymentStatusRequired", CodeLocalise("MilitaryEmploymentStatus.RequiredErrorMessage", "Employment status is required") },
				{ "errorBranchOfServiceRequired", CodeLocalise("BranchOfServiceRequired.ErrorMessage", "Branch of service is required") },
				{ "errorRankRequired", CodeLocalise("RankRequired.RequiredErrorMessage", "Rank is required") },
				{ "errorVeteranTypeRequired", CodeLocalise("VeteranType.RequiredErrorMessage", "Veteran type is required") },
				{ "errorTransitioningTypeRequired", CodeLocalise("TransitioningType.RequiredErrorMessage", "Transitioning type is required") },
        { "errorServiceDutyStartDateRequired", CodeLocalise("ServiceDutyStartDate.RequiredErrorMessage", "Service duty start date is required") },
				{ "errorServiceDutyStartDateLimit", CodeLocalise("ServiceDutyStartDate.ErrorMessage", "Service duty start date should not be greater than current date") },
				{ "errorServiceDutyStartDateDOBLimit", CodeLocalise("ServiceDutyStartDateDOB.ErrorMessage", "Please check your birthdate above. You must have been at least {0} years or older to enter military service.", App.Settings.MilitaryServiceMinimumAge.ToString(CultureInfo.CurrentUICulture)) },
				{ "errorServiceDutyEndDateRequired", CodeLocalise("ServiceDutyEndDate.RequiredErrorMessage", "Service duty end date is required") },
				{ "errorServiceDutyEndDateLimit", CodeLocalise("ServiceDutyEndDateLimit.ErrorMessage", "Service duty end date should not be greater than current date") },
				{ "errorServiceDutyEndDate", CodeLocalise("ServiceDutyEndDate.ErrorMessage", "Service duty end date should not be less than service duty start date") },
				{ "errorPlannedEndDateRequired", CodeLocalise("PlannedEndDate.RequiredErrorMessage", "Planned end date is required") },
				{ "errorPlannedEndDateLimit", CodeLocalise("PlannedEndDateLimit.ErrorMessage", "Planned end date should not be less than service duty start date") },
				{ "errorVEDLimit", CodeLocalise("VeteranEndDateLimit.ErrorMessage", "Veteran end date must be greater than today for Transitioning Service Member") },
				{ "errorDischargingTransitioningVeteranLimit", CodeLocalise("DischargingTransitioningVeteran.ErrorMessage", "Planned end date for a discharging transitioning veteran must be within one year from current date") },
				{ "errorRetiringTransitioningVeteranLimit", CodeLocalise("RetiringTransitioningVeteran.ErrorMessage", "Planned end date for a retiring transitioning veteran must be within two year from current date") },
				{ "employmentStatusEmployed", EmploymentStatus.Employed.ToString() },
				{ "employmentStatusEmployedTerminationReceived", EmploymentStatus.EmployedTerminationReceived.ToString() },
				{ "employmentStatusUnemployed", EmploymentStatus.UnEmployed.ToString() },
  			{ "veteranEraTransitioningServiceMember", VeteranEra.TransitioningServiceMember.ToString() },
				{ "veteranEraOtherEligible", VeteranEra.OtherEligible.ToString() },
				{ "veteranEraOtherVet", VeteranEra.OtherVet.ToString() },
				{ "veteranTransitionTypeSpouse", VeteranTransitionType.Spouse.ToString() },
				{ "veteranTransitionTypeRetirement", VeteranTransitionType.Retirement.ToString() },
				{ "veteranTransitionTypeDischarge", VeteranTransitionType.Discharge.ToString() },
				{ "MilitaryServiceMinimumAge", App.Settings.MilitaryServiceMinimumAge.ToString(CultureInfo.InvariantCulture) }
      };

      return nvc;
    }

    /// <summary>
    /// Binds the rank drop down.
    /// </summary>
    /// <param name="rankDownDown">The rank down down.</param>
    /// <param name="ccdRank">The CCD rank.</param>
    /// <param name="serviceBranchId">The service branch id.</param>
    private void BindRankDropDown(DropDownList rankDownDown, CascadingDropDown ccdRank, long? serviceBranchId = null)
    {
      rankDownDown.Items.Clear();

      if (serviceBranchId.IsNotNull() && serviceBranchId > 0)
        rankDownDown.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MilitaryRanks, serviceBranchId.Value), null, null, "0");

      rankDownDown.Items.AddLocalisedTopDefault("ddlRank.TopDefault", "- select rank -");
      ccdRank.LoadingText = CodeLocalise("ccdRank.LoadingText", "[Loading rank...]");
    }

    /// <summary>
    /// Binds the veteran type drop down.
    /// </summary>
    /// <param name="veteranTypeDropDown">Veteran Dropdown.</param>
    private void BindVeteranTypeDropDown(DropDownList veteranTypeDropDown)
    {
      veteranTypeDropDown.Items.Clear();

      veteranTypeDropDown.Items.AddEnum(VeteranEra.OtherVet, "Veteran");
      veteranTypeDropDown.Items.AddEnum(VeteranEra.TransitioningServiceMember, "Transitioning Service Member");
      veteranTypeDropDown.Items.AddEnum(VeteranEra.OtherEligible, "Other Eligible");

      veteranTypeDropDown.Items.AddLocalisedTopDefault("VeteranType.TopDefault", "- select type -");
    }

    /// <summary>
    /// Binds the service disability drop down.
    /// </summary>
    /// <param name="serviceDisabilityDropDown">Veteran Dropdown.</param>
    private void BindServiceDisabilityDropDown(DropDownList serviceDisabilityDropDown)
    {
      serviceDisabilityDropDown.Items.Clear();

      serviceDisabilityDropDown.Items.AddEnum(VeteranDisabilityStatus.NotDisabled, "Not Disabled");
      serviceDisabilityDropDown.Items.AddEnum(VeteranDisabilityStatus.Disabled, "Disabled (Up to 20%)");
      serviceDisabilityDropDown.Items.AddEnum(VeteranDisabilityStatus.SpecialDisabled, "Special Disabled (30% or more)");

      serviceDisabilityDropDown.Items.AddLocalisedTopDefault("ServiceDisability.TopDefault", "- select status -");
    }

    /// <summary>
    /// Binds the military employment status drop down.
    /// </summary>
    /// <param name="militaryEmploymentStatusDropDown">Military Employment Status Dropdown.</param>
    private void BindMilitaryEmploymentStatusDropDown(DropDownList militaryEmploymentStatusDropDown)
    {
      militaryEmploymentStatusDropDown.Items.Clear();

      militaryEmploymentStatusDropDown.Items.AddEnum(EmploymentStatus.Employed, "Employed");
      militaryEmploymentStatusDropDown.Items.AddEnum(EmploymentStatus.EmployedTerminationReceived, "Employed - Rcvd Notice of Termination");
      militaryEmploymentStatusDropDown.Items.AddEnum(EmploymentStatus.UnEmployed, "Not Employed");

      militaryEmploymentStatusDropDown.Items.AddLocalisedTopDefault("MilitaryEmploymentStatus.TopDefault", "- select status -");
    }

    /// <summary>
    /// Binds the transitioning type drop down.
    /// </summary>
    /// <param name="transitioningTypeDropDown">Transitioning Type Dropdown.</param>
    private void BindTransitioningTypeDropDown(DropDownList transitioningTypeDropDown)
    {
      transitioningTypeDropDown.Items.Clear();

      transitioningTypeDropDown.Items.AddEnum(VeteranTransitionType.Discharge, "Discharge");
      transitioningTypeDropDown.Items.AddEnum(VeteranTransitionType.Retirement, "Retirement");
      transitioningTypeDropDown.Items.AddEnum(VeteranTransitionType.Spouse, "Spouse");

      transitioningTypeDropDown.Items.AddLocalisedTopDefault("TransitioningType.TopDefault", "- select type -");
    }
  }
}