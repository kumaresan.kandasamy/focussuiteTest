﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="SavedSearchList.ascx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.Controls.SavedSearchList" EnableViewState="true" %>
<%@ Register Src="~/Controls/JobList.ascx" TagName="JobList" TagPrefix="uc" %>
<asp:Panel ID="Panel1" runat="server">
	<p>
	    <asp:Label runat="server" ID="NoResultsLabel"></asp:Label>
	</p>
</asp:Panel>
<asp:Panel ID="Panel2" runat="server">
	<asp:UpdatePanel ID="updJobList" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
		    <asp:Label runat="server" ID="ActiveSearchLabel" />&nbsp;
			<asp:DropDownList ID="ddlActiveSearch" runat="server" OnSelectedIndexChanged="ddlActiveSearch_IndexChanged"
				AutoPostBack="true" ClientIDMode="AutoID" CssClass="ActiveSearchClass" Width="250px" Title="Active Search">
			</asp:DropDownList>
			<asp:LinkButton ID="lnkEditSaveSearch" runat="server" OnClick="lnkEditSaveSearch_Click">
				<%= HtmlLocalise("EditSearch.label", "Edit this search")%>
			</asp:LinkButton>
			<br />
			<asp:PlaceHolder runat="server" ID="ResultsSpacerPlaceHolder"><br /></asp:PlaceHolder> 
			<uc:JobList ID="JobList" runat="server" MaximumJobCount="10" />
			<asp:LinkButton ID="lnkSeeAllMatches" runat="server" OnClick="lnkSeeAllMatches_Click">
					<%= HtmlLocalise("SeeAllMatches.Label", "See all matches for this search")%>
			</asp:LinkButton>
		</ContentTemplate>
		<Triggers>
			<asp:AsyncPostBackTrigger ControlID="ddlActiveSearch" EventName="SelectedIndexChanged" />
		</Triggers>
	</asp:UpdatePanel>
</asp:Panel>
