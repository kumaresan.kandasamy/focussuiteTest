﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Framework.Core;
using Focus.Core;
using Focus.Common;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class CreateEditResumePopup : UserControlBase
	{
		public delegate void LinkNameClick(object sender, EventArgs eventArgs);

		public event LinkNameClick LinkNameClicked;

		protected void Page_Load(object sender, EventArgs e)
		{
			UploadResume.UploadResumeClick += OnUploadResumeClick;
			PasteTypeResume.PastedResumeClick += OnUploadResumeClick;
            ScriptManager.GetCurrent(Page).RegisterPostBackControl(UploadResume.FindControl("btnUploadResume"));
            var resumeCount = App.User.IsAuthenticated && !App.GetSessionValue<bool>("ReactivateRequestKey") ? ServiceClientLocator.ResumeClient(App).GetResumeCount() : 0;
            if (App.Settings.MaximumAllowedResumeCount != 0 && resumeCount >= App.Settings.MaximumAllowedResumeCount)
			{
				ResumeLimitReachedLabel.Text = App.Settings.MaximumAllowedResumeCount > 1
					? CodeLocalise("MaxResumeCountReached.Text",
						"You can only save up to {0} resumes. Please delete older versions if you wish to continue to create a new one.",
						App.Settings.MaximumAllowedResumeCount)
					: CodeLocalise("MaxResumeCountReachedOne.Text",
						"You can only save up to 1 resume. Please delete your current resume if you wish to continue to create a new one.",
						App.Settings.MaximumAllowedResumeCount);
               	ResumeLimitReachedLabel.Visible = true;
				CreateOrEditResumeLabel.Visible = false;
				CreateResumeLabel.Visible = false;
				MyResumeList.Visible = true;
				EditDeleteResumeLabel.Visible = true;
				CreateResume.Visible = false;
				if( !CreateEditDiv.Attributes["class"].Split( ' ' ).Contains( "open" ) )
				{
					CreateEditDiv.Attributes["class"] = string.Concat( CreateEditDiv.Attributes["class"], " open" );
				}
				UploadSection.Visible = PasteSection.Visible = false;
			}
			else
			{
				if( !CreateEditDiv.Attributes["class"].Split( ' ' ).Contains( "open" ) )
				{
					CreateEditDiv.Attributes["class"] = string.Concat( CreateEditDiv.Attributes["class"], " open" );
				}
				CreateResume.Visible = true;
				ResumeLimitReachedLabel.Visible = false;

				if (MasterPage.ResumeCount == 0)
				{
					CreateResumeLabel.Visible = true;
					CreateOrEditResumeLabel.Visible = false;
					MyResumeList.Visible = false;
					EditDeleteResumeLabel.Visible = false;
				}
				else
				{
					CreateResumeLabel.Visible = false;
					CreateOrEditResumeLabel.Visible = true;
					MyResumeList.Visible = true;
					EditDeleteResumeLabel.Visible = true;
				}

				UploadSection.Visible = PasteSection.Visible = true;
			}
		}

		protected void Page_PreRender(object sender, EventArgs e)
		{
			var checkConfirmation = App.GetSessionValue("Career:DeleteResume:ResumeID", "");
			if (checkConfirmation.IsNotNullOrEmpty() && checkConfirmation.StartsWith(ClientID + "_"))
			{
				ShowPopUp();
				if( !CreateEditDiv.Attributes["class"].Split( ' ' ).Contains( "open" ) )
					CreateEditDiv.Attributes["class"] = string.Concat( CreateEditDiv.Attributes["class"], " open" );

				App.RemoveSessionValue("Career:DeleteResume:ResumeID");
			}
		}

		protected void OnUploadResumeClick(object sender, EventArgs e)
		{
			ResumeMissingInfoPopup.Visible = true;
			ResumeMissingInfoPopup.ShowPopup();
		}

		public void ShowPopUp()
		{
          CreateOrEditResumeModal.Show();
		}
        

		/// <summary>
		/// Called when [link name click].
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="eventArgs">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected virtual void OnLinkNameClicked(object sender, EventArgs eventArgs)
		{
			if (LinkNameClicked != null)
			{
				LinkNameClicked(sender, eventArgs);
			}
		}
	}
}
