﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class JobSeekerSurveyModal : UserControlBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Localise();
				BindQuestion2RadioButtonList();
				BindQuestion3RadioButtonList();
				BindQuestion1DropDownList();
			}
		}

		/// <summary>
		/// Localises this instance.
		/// </summary>
		private void Localise()
		{
			Question1Label.Text = CodeLocalise("Question1Label.Text",
																				 "Were you satisfied with the quality of matches you found or received via " + Constants.PlaceHolders.FocusCareer + "?");

			Question2Label.Text = CodeLocalise("Question2Label.Text",
																				 "Did you find or receive matches for jobs you had not previously considered via " + Constants.PlaceHolders.FocusCareer + "?");

			Question3Label.Text = CodeLocalise("Question3Label.Text",
                                                                                 "Did you receive #BUSINESS#:LOWER invitations, staff recommendations, or apply for any of the jobs to which you were matched via " + Constants.PlaceHolders.FocusCareer + "?");

			CloseButton.Text = CodeLocalise("CloseButton.Text", "Close survey");
			SubmitButton.Text = CodeLocalise("SubmitButton.Text", "Submit results");
		}

		/// <summary>
		/// Binds the question1 drop down list.
		/// </summary>
		private void BindQuestion1DropDownList()
		{
			Question1AnswerDropDownList.Items.Clear();

            Question1AnswerDropDownList.Items.Add(new ListItem("- select response -", string.Empty));
			Question1AnswerDropDownList.Items.AddEnum(SatisfactionLevels.VerySatisfied, "Very satisfied");
			Question1AnswerDropDownList.Items.AddEnum(SatisfactionLevels.SomewhatSatisfied, "Somewhat satisfied");
			Question1AnswerDropDownList.Items.AddEnum(SatisfactionLevels.SomewhatDissatisfied, "Somewhat dissatisfied");
			Question1AnswerDropDownList.Items.AddEnum(SatisfactionLevels.VeryDissatisfied, "Very dissatisfied");

			//Question1AnswerDropDownList.SelectValue(SatisfactionLevels.VerySatisfied.ToString());
		}

		/// <summary>
		/// Binds the question2 radio button list.
		/// </summary>
		private void BindQuestion2RadioButtonList()
		{
			Question2AnswerRadioButtonList.Items.Clear();
			Question2AnswerRadioButtonList.Items.Add(new ListItem(CodeLocalise("Global.Yes.Label", "Yes"), true.ToString()));
			Question2AnswerRadioButtonList.Items.Add(new ListItem(CodeLocalise("Global.No.Label", "No"), false.ToString()));
			//Question2AnswerRadioButtonList.SelectValue(true.ToString());
            if (Question2AnswerRadioButtonList != null)
                Question2AnswerRadioButtonList.Attributes.Add("role", "presentation");
		}

		/// <summary>
		/// Binds the question3 radio button list.
		/// </summary>
		private void BindQuestion3RadioButtonList()
		{
			Question3AnswerRadioButtonList.Items.Clear();
			Question3AnswerRadioButtonList.Items.Add(new ListItem(CodeLocalise("Global.Yes.Label", "Yes"), true.ToString()));
			Question3AnswerRadioButtonList.Items.Add(new ListItem(CodeLocalise("Global.No.Label", "No"), false.ToString()));
			//Question3AnswerRadioButtonList.SelectValue(true.ToString());
            if (Question3AnswerRadioButtonList != null)
                Question3AnswerRadioButtonList.Attributes.Add("role", "presentation");
		}

		/// <summary>
		/// Binds the referral outcome.
		/// </summary>
		/// <param name="referralOutcomeDropDownList">The referral outcome drop down list.</param>
		/// <param name="validator">The validator.</param>
		private void BindReferralOutcome(DropDownList referralOutcomeDropDownList, RequiredFieldValidator validator)
		{
			referralOutcomeDropDownList.Items.Clear();

			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise("ApplicationStatus.TopDefault", "- choose referral outcome -"), string.Empty));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.DidNotApply, "Failed to apply to job"), ((int)ApplicationStatusTypes.DidNotApply).ToString(CultureInfo.InvariantCulture)));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToShow, "Failed to report to interview"), ((int)ApplicationStatusTypes.FailedToShow).ToString(CultureInfo.InvariantCulture)));
      referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToReportToJob, "Failed to report to job"), ((int)ApplicationStatusTypes.FailedToReportToJob).ToString(CultureInfo.InvariantCulture)));
      referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FailedToRespondToInvitation, "Failed to respond to invitation"), ((int)ApplicationStatusTypes.FailedToRespondToInvitation).ToString(CultureInfo.InvariantCulture)));
      referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.FoundJobFromOtherSource, "Found job from other source"), ((int)ApplicationStatusTypes.FoundJobFromOtherSource).ToString(CultureInfo.InvariantCulture)));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.Hired, "Hired"), ((int)ApplicationStatusTypes.Hired).ToString(CultureInfo.InvariantCulture)));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.InterviewDenied, "Interview denied"), ((int)ApplicationStatusTypes.InterviewDenied).ToString(CultureInfo.InvariantCulture)));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.JobAlreadyFilled, "Job already filled"), ((int)ApplicationStatusTypes.JobAlreadyFilled).ToString(CultureInfo.InvariantCulture)));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotHired, "Not hired"), ((int)ApplicationStatusTypes.NotHired).ToString(CultureInfo.InvariantCulture)));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.NotQualified, "Not qualified"), ((int)ApplicationStatusTypes.NotQualified).ToString(CultureInfo.InvariantCulture)));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.RefusedOffer, "Refused job"), ((int)ApplicationStatusTypes.RefusedOffer).ToString(CultureInfo.InvariantCulture)));
			referralOutcomeDropDownList.Items.Add(new ListItem(CodeLocalise(ApplicationStatusTypes.RefusedReferral, "Refused referral"), ((int)ApplicationStatusTypes.RefusedReferral).ToString(CultureInfo.InvariantCulture)));
		}

		/// <summary>
		/// Updates the last surveyed date.
		/// </summary>
		private void UpdateLastSurveyedDate()
		{
			if (App.User.IsAuthenticated)
			{
				var lastSurveyedOn = ServiceClientLocator.AccountClient(App).GetUserDetails(0, Convert.ToInt64(App.User.PersonId)).PersonDetails.LastSurveyedOn;

				// Only update the Last Surveyed Date once in one 24 hour period
				if (lastSurveyedOn.IsNull() || (DateTime.Now.Date - Convert.ToDateTime(lastSurveyedOn)).TotalHours > 24)
					ServiceClientLocator.AccountClient(App).UpdateLastSurveyedDate(Convert.ToInt64(App.User.PersonId));
			}
		}

		/// <summary>
		/// Shows the pop up.
		/// </summary>
		public void Show()
		{
      // Record that the user has been surveyed (regardless of whether they answer anything)
      UpdateLastSurveyedDate();

			JobSeekerSurvey.Show();
		}

    /// <summary>
    /// Shows the pop-up only on condition if the required time period has elapsed
    /// </summary>
    public void ConditionalShow()
    {
	    
			var surveyShown = false;

			if (App.Settings.DisplaySurveyFrequency)
			{
			  var userDetails = ServiceClientLocator.AccountClient(App).GetUserDetails(0, Convert.ToInt64(App.User.PersonId));
			  var surveyLastShownOn = userDetails.PersonDetails.LastSurveyedOn ?? (DateTime?)userDetails.UserDetails.CreatedOn;

			  var timeSinceLastSurvey = surveyLastShownOn.IsNull() ? (TimeSpan?)null : (DateTime.Now - surveyLastShownOn.Value);
			  var timeSinceFirstLoggedOn = userDetails.UserDetails.FirstLoggedInOn.IsNull() ? (TimeSpan?)null : (DateTime.Now - userDetails.UserDetails.FirstLoggedInOn.Value);

			  // Only display the survey when it is the number of days set in the config settings since the last survey was shown and the user has previously logged on, i.e. they're not a newly registered user
			  // and only show it if it is the x frequency days from the day the user first logged on the system
			  if (timeSinceLastSurvey.IsNull() || timeSinceLastSurvey.Value.TotalDays > App.Settings.SurveyFrequency && (timeSinceFirstLoggedOn.IsNotNull() && timeSinceFirstLoggedOn.Value.TotalDays > App.Settings.SurveyFrequency) && userDetails.UserDetails.LastLoggedInOn.IsNotNull())
			  {
			    surveyShown = true;
			    Show();
				}
				else if (timeSinceLastSurvey.Value.TotalHours < 24 && userDetails.PersonDetails.LastSurveyedOn.IsNotNull() && (timeSinceFirstLoggedOn.IsNotNull() && timeSinceFirstLoggedOn.Value.TotalDays > App.Settings.SurveyFrequency))
				{
					// If in the last 24 hours, show it again if not completed
					var lastSurvey = ServiceClientLocator.CandidateClient(App).GetLatestJobSeekerSurvey(App.User.PersonId);
					if (lastSurvey.IsNull() || (DateTime.Now - lastSurvey.CreatedOn).TotalHours >= 24)
					{
						surveyShown = true;
						Show();
					}
				}
			}

			if (!surveyShown)
				App.RemoveCookieValue("JobSeekerSurvey:Show");
    }

		/// <summary>
		/// Gets the referrals.
		/// </summary>
		/// <returns></returns>
		public List<NewApplicantReferralViewDto> GetReferrals()
		{
			var referrals = new List<NewApplicantReferralViewDto>();

		  if (App.User.IsAuthenticated)
		    referrals = ServiceClientLocator.CandidateClient(App).GetNewApplicantReferralViewPagedList(App.User.PersonId.GetValueOrDefault(0), true);

			return referrals;
		}

		/// <summary>
		/// Handles the Click event of the SubmitButton control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SubmitButton_Click(object sender, EventArgs e)
		{
			SatisfactionLevels satisfactionLevel;

            if (Question1AnswerDropDownList.SelectedIndex != 0 || Question2AnswerRadioButtonList.SelectedValue.IsNotNullOrEmpty() || Question3AnswerRadioButtonList.SelectedValue.IsNotNullOrEmpty())
            {
                 SatisfactionLevels? satisfactionLevelChosen = null;
                 if (Question1AnswerDropDownList.SelectedIndex != 0)
                 {
                     Enum.TryParse(Question1AnswerDropDownList.SelectedValue, out satisfactionLevel);
                     satisfactionLevelChosen = satisfactionLevel;
                 }

                ServiceClientLocator.CandidateClient(App).SaveJobSeekerSurvey(new JobSeekerSurveyDto
                    {
                        PersonId = Convert.ToInt64(App.User.PersonId),
                        SatisfactionLevel = satisfactionLevelChosen,
                        UnanticipatedMatches = Question2AnswerRadioButtonList.SelectedValue.IsNotNullOrEmpty()? (bool?)(Convert.ToBoolean(Question2AnswerRadioButtonList.SelectedValue)) :null,
                        WasInvitedDidApply = Question3AnswerRadioButtonList.SelectedValue.IsNotNullOrEmpty()?(bool?)(Convert.ToBoolean(Question3AnswerRadioButtonList.SelectedValue)) : null
                    });
            }

			foreach (var referral in ReferralsListView.Items)
			{
				var referralOutcomeDropDownList = (DropDownList)referral.FindControl("ReferralOutcomeDropDown");

				if (referralOutcomeDropDownList.SelectedValue != string.Empty)
				{
          var currentStatus = Convert.ToInt32(((HiddenField)referral.FindControl("ApplicationStatusHiddenField")).Value);
				  var newStatus = referralOutcomeDropDownList.SelectedValue.AsInt32();

          if (currentStatus != newStatus)
          {
            var applicationId = Convert.ToInt64(((HiddenField)referral.FindControl("ApplicationIdHiddenField")).Value);
            ServiceClientLocator.CandidateClient(App).UpdateApplicationStatus(applicationId, (ApplicationStatusTypes)newStatus);
          }
				}
			}

      CloseSurvey();
		}

		/// <summary>
		/// Handles the ItemDataBound event of the ReferralsRepeater control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="ListViewItemEventArgs"/> instance containing the event data.</param>
		protected void ReferralsRepeater_ItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var referralOutcomeDropDownList = (DropDownList)e.Item.FindControl("ReferralOutcomeDropDown");
			var referralOutcomeValidator = (RequiredFieldValidator)e.Item.FindControl("ReferralOutcomeRequired");
			BindReferralOutcome(referralOutcomeDropDownList, referralOutcomeValidator);

			// Set referral outcome dropdown
			var referral = ((NewApplicantReferralViewDto)e.Item.DataItem);
			referralOutcomeDropDownList.SelectedValue = referral.CandidateApplicationStatus.ToString();

			var employerName = (Literal)e.Item.FindControl("EmployerNameLiteral");
			employerName.Text = referral.ApprovalStatus.IsIn(ApprovalStatuses.None, ApprovalStatuses.Approved)
				                    ? referral.ActualEmployerName
				                    : referral.EmployerName;
		}


    /// <summary>
    /// Handles the OnDataBound event of the ReferralsListView control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void ReferralsListView_OnDataBound(object sender, EventArgs e)
    {
      ReferralsRow.Visible = ReferralsListView.Items.Count > 0;
    }

    /// <summary>
    /// Handles the OnClick event of the CloseButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
	  protected void CloseButton_OnClick(object sender, EventArgs e)
	  {
      CloseSurvey();
	  }

    /// <summary>
    /// Closes the survey window
    /// </summary>
    private void CloseSurvey()
    {
      JobSeekerSurvey.Hide();
      App.RemoveCookieValue("JobSeekerSurvey:Show");      
    }
	}
}