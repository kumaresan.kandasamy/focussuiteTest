﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Review.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.Review" %>
<%@ Register Src="~/WebCareer/Controls/DownloadResume.ascx" TagName="DownloadResume" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/EmailResume.ascx" TagName="EmailResume" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/EducationControls/SearchPostingsPopup.ascx" TagName="SearchPostingsPopup" TagPrefix="uc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmationModal.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>

<div class="resumeSection FocusCareer">
	<div class="resumeSectionHelpText">
		<focus:LocalisedLabel runat="server" ID="ReviewHelpLabel" RenderOuterSpan="True" DefaultText="You have now completed your resume, to go back and edit any of the information displayed, click on the relevant section headings." CssClass="instructionalText instructionalJustitfy"/>
	</div>
	<div class="resumeSectionAction">		
		<asp:Button ID="btnSaveResumeFormatTop" Text="Save Resume &amp; View Job Postings &#187;" runat="server" class="buttonLevel2" style="margin-left:0px" OnClick="btnSaveResumeFormat_Click" />
	</div>
	<div class="resumeSectionContent">
		<div class="resumeReviewOptions">
       <%--	<div class="resumeOutputOptions">--%>
        <div class=" col-xs-12 col-sm-6 col-md-4">
            <div class="col-xs-3 col-sm-3 col-md-4">
                <focus:LocalisedLabel runat="server" ID="ResumeFormatLabel" AssociatedControlID="ddlReviewResumeFormat" CssClass="dataInputLabel" LocalisationKey="ResumeFormat.Label" DefaultText="Resume style"/></div>
				<span class="dataInputField">
					<asp:DropDownList ID="ddlReviewResumeFormat" runat="server" AutoPostBack="true" Width="200px" ClientIDMode="Static" CssClass="ResumeFormatClass" OnSelectedIndexChanged="ddlReviewResumeFormat_SelectedIndexChanged"></asp:DropDownList>
				</span>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5">
            <div class="col-xs-3 col-sm-3 col-md-3 ">
   				<focus:LocalisedLabel runat="server" ID="ResumeOrderLabel" AssociatedControlID="ddlReviewResumeOrder" CssClass="dataInputLabel" LocalisationKey="ResumeOrder.Label" DefaultText="Resume order"/></div>
				<span class="dataInputField">
					<asp:DropDownList ID="ddlReviewResumeOrder" runat="server" Width="200px" AutoPostBack="true" CssClass="ResumeOrderClass" ClientIDMode="Static" OnSelectedIndexChanged="ddlReviewResumeFormat_SelectedIndexChanged">
						<asp:ListItem Text="Emphasize my experience" Value="experience"></asp:ListItem>
						<asp:ListItem Text="Emphasize my education" Value="education"></asp:ListItem>
						<asp:ListItem Text="Emphasize my skills" Value="skills"></asp:ListItem>
					</asp:DropDownList>
				</span>
        </div>
      		<div class="resumeProductionOptions col-xs-12 col-sm-12 col-md-3">
                <asp:LinkButton ID="lnkHideResumeInfo" runat="server" Text="Hide options" />
				<asp:ImageButton Visible="false" ID="ibEditResume" runat="server" Width="26" Height="26" OnClick="ibEditResume_Click" />
				<asp:ImageButton ID="ibDownloadResume" runat="server" OnClick="IbDownLoadResume_Click" ToolTip="Download Resume"/>
				<asp:ImageButton ID="ibEmailResume" runat="server" Width="30" Height="24" OnClick="IbEmailResume_Click" ToolTip="Email Resume" />
				<img src="<%= UrlBuilder.PrintIcon() %>" alt="Print" width="30" height="24" onclick="return PrintResume();" title="Print Resume" />
			</div>
		</div>
      </div>
		<div class="reviewResumeHint">
			<focus:LocalisedLabel runat="Server" ID="ReviewResumeHintLabel" LocalisationKey="Resume.Review.ReviewResumeHintLabel" DefaultText="" RenderOuterSpan="False"/>
		</div>
		<div class="resumeReviewPreview">
			<asp:UpdatePanel UpdateMode="Always" ID="upReviewPanResume" runat="server">
				<ContentTemplate>
					<iframe id="ResumeDetail" runat="server" width="1080" height="500" title="ResumeDetail">Resume Detail</iframe>
					<asp:Panel ID="panReviewFormattedOutput" runat="server" CssClass="resumeReviewPreviewOutput">
						<asp:Label ID="lblReviewFormattedOutput" runat="server" Style="padding-left: 5px;" ClientIDMode="Static" Width="98%"></asp:Label>
						<cc1:Editor ID="htmlJobDescription" Width="1080px" Height="500px" runat="server" Visible="false" />
					</asp:Panel>
					<script type="text/javascript">
						Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
						Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

						function BeginRequestHandler(sender, args) {
							try {
								args.get_request().set_userContext(args.get_postBackElement().id);
							}
							catch (e) {
							}
						}

						function EndRequestHandler(sender, args) {
							try {
								if (args.get_error() == undefined) {
									EnableDisableResumeOrder();
								}
							}
							catch (e) {
							}
						}
					</script>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="ddlReviewResumeFormat" EventName="SelectedIndexChanged" />
					<asp:AsyncPostBackTrigger ControlID="ddlReviewResumeOrder" EventName="SelectedIndexChanged" />
					<asp:AsyncPostBackTrigger ControlID="btnHideResumeInfo" EventName="Click" />
					<asp:AsyncPostBackTrigger ControlID="ibEditResume" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>
		</div>
		<div class="resumeSectionAction">
			<asp:Button ID="btnSaveResumeFormatBottom" Text="Save Resume &amp; View Job Postings &#187;" runat="server" class="buttonLevel2" Style="Margin: 10px 0px 0px 0px;" OnClick="btnSaveResumeFormat_Click" />
		</div>
	</div>

<uc:DownloadResume ID="DownloadResumePopup" runat="server" />
<uc:EmailResume ID="EmailResumePopup" runat="server" />
<uc:SearchPostingsPopup ID="SearchPostingsPopup" runat="server" Visible="False" OnJobRecommendationsClick="JobRecommendations_Click" />
<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />

<act:ModalPopupExtender ID="HideResumeInfoModal" runat="server" BehaviorID="HideResumeInfoModal"
	TargetControlID="lnkHideResumeInfo" PopupControlID="HideResumeInfoModalPanel" BackgroundCssClass="modalBackground"
	RepositionMode="RepositionOnWindowResizeAndScroll" />
<div id="HideResumeInfoModalPanel" tabindex="-1" class="modal career-scroll-pane" style="z-index:100001;display: none;width:400px">
	<div class="lightbox resumeHideInfoModal">
		<div class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('HideResumeInfoModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
			<focus:LocalisedLabel runat="server" ID="HideOptionsTitleLabel" LocalisationKey="HideOptionsTitle.Label" DefaultText="Hide options" CssClass="modalTitle modalTitleLarge"/>
		</div>
		<div class="resumeHideInfoModalSection">
			<focus:LocalisedLabel runat="server" ID="HideSectionsLabel" CssClass="embolden" LocalisationKey="HideSections.Label" DefaultText="Hide sections of my resume"/>
			<div class="hiddenSections">
				<asp:CheckBoxList ID="cblHideSections" runat="server" ClientIDMode="Static" CssClass="chkStyle"></asp:CheckBoxList>
			</div>
		</div>
        <br />
		<div class="resumeHideInfoModalSection">
			<focus:LocalisedLabel runat="server" ID="HideJobsLabel" CssClass="embolden" LocalisationKey="HideJobs.Label" DefaultText="Hide the job(s) below"/>
			<asp:ListBox ID="lbHideJobs" CssClass="ListBox" runat="server" max-width="400px" SelectionMode="Multiple" Rows="5"></asp:ListBox>
			<focus:LocalisedLabel runat="server" ID="HideJobsSelectMultipleLabel"  LocalisationKey="HideJobsSelectMultiple.Label"/>
		</div>
		<div class="modalButtons"><asp:Button ID="btnHideResumeInfo" Text="Go" runat="server" class="buttonLevel2" OnClick="btnHideResumeInfo_Click" /></div>
	</div>
</div>
<asp:HiddenField ID="ModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ModalPopup" runat="server" TargetControlID="ModalDummyTarget"
	PopupControlID="ModalPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="ModalPanel" TabIndex="-1" runat="server" CssClass="modal" Style="display: none;">
	<div class="resumeCheckDatesModal">
		<div class="modalHeader"><focus:LocalisedLabel runat="server" ID="CheckDatesHeadingLabel" CssClass="modalTitle modalTitleLarge" LocalisationKey="CheckDatesHeading.Label" DefaultText="You may want to check..."/></div>
		<focus:LocalisedLabel runat="server" ID="CheckDatesMessageLabel" CssClass="modalMessage" LocalisationKey="CheckDatesMessage.Label" DefaultText="Since you have chosen to hide dates, it may be a good idea to check your summary section which may include your years of experience or the dates of your jobs."/>
		<div class="modalButtons">
			<asp:Button ID="CloseButton" runat="server" class="buttonLevel3" TabIndex="0" Text="Ignore" />
			<asp:Button ID="OkButton" runat="server" class="buttonLevel2" Text="Review summary" OnClick="OkButton_Click" />
		</div>
	</div>
</asp:Panel>
<script type="text/javascript">
	function pageLoad(sender, args) {
		EnableDisableResumeOrder();
	}
	
	Sys.Application.add_load(ResumeHide_PageLoad);

	function ResumeHide_PageLoad(sender, args) {
		// Jaws Compatibility

		var modal = $find('HideResumeInfoModal');
		if (modal != null) {

			modal.add_shown(function() {
				$('.modal').attr('aria-hidden', false);
				$('.page').attr('aria-hidden', true);
				$('#ddlResumeFormat').focus();
			});

			modal.add_hiding(function() {
				$('.modal').attr('aria-hidden', true);
				$('.page').attr('aria-hidden', false);
			});
		}

	}

	function PrintResume() {
	  var display_setting = 'left=150px, top=70px, width=850px, height=850px, addressbar=no, location=no, scrollbars=yes, status=no, resizable=no';
	  var content_innerhtml = '';
	  if ($('#ddlReviewResumeFormat').val() !== '0')
	    content_innerhtml = $('#lblReviewFormattedOutput').html();
	  else
	    content_innerhtml = $('#<%=ResumeDetail.ClientID %>').contents().find('body').html();

	  var document_print = window.open('PrintPreview', '_blank', display_setting);
	  document_print.document.open();
	  document_print.document.write("<html><head><");
	  document_print.document.write("script>function printResume() { "); //
	  document_print.document.write("  if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) { ");
	  document_print.document.write("    window.PPClose = false; ");
	  document_print.document.write("    window.onbeforeunload = function () { ");
	  document_print.document.write("      if (window.PPClose === false) { ");
	  document_print.document.write("        return 'Leaving this page will block the parent window!\\nPlease select \"Stay on this Page\" option and use the\\nCancel button instead to close the Print Preview Window.\\n';");
	  document_print.document.write("      }");
	  document_print.document.write("    };");
	  document_print.document.write("    window.print();");
	  document_print.document.write("    window.PPClose = true;");
	  document_print.document.write("    window.close();");
	  document_print.document.write("  } else {");
	  document_print.document.write("    window.print();");
	  document_print.document.write("    window.close();");
	  document_print.document.write("  }");
	  document_print.document.write("}</");
	  document_print.document.write("script></head>");
	  document_print.document.write("<body onload='printResume();'>");
	  document_print.document.write(content_innerhtml);
	  document_print.document.write("</body></html>");
	  document_print.document.close();
	  return false;
	}

	function EnableDisableResumeOrder() {
		if ($("#ddlReviewResumeFormat").val() != "0") {
			$('#ddlReviewResumeOrder').prop('disabled', false).next().removeClass('stateDisabled');
		} else {
			$('#ddlReviewResumeOrder').prop('disabled', true).next().addClass('stateDisabled');
		}
	}
</script>
