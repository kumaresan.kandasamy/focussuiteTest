﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumePreview.ascx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.Controls.ResumePreview" %>
<asp:HiddenField ID="ResumePreviewModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="ResumePreviewModal" runat="server" BehaviorID="ResumePreviewModal"
	TargetControlID="ResumePreviewModalDummyTarget" PopupControlID="ResumePreviewModalPanel"
	BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" ClientIDMode="Static">
	<Animations>
    <OnShown>
          <ScriptAction Script="EnableDisablePreviewResumeOrder();" />  
    </OnShown>
	</Animations>
</act:ModalPopupExtender>
<div id="ResumePreviewModalPanel" tabindex="-1" class="modal" style="z-index:100001; display:none;">
	<div class="lightbox resumePreviewModal">
		<div class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('ResumePreviewModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
			<focus:LocalisedLabel runat="server" ID="ResumePreviewTitleLabel" LocalisationKey="ResumePreviewTitle.Label" DefaultText="Resume preview" CssClass="modalTitle modalTitleLarge"/>
		</div>
        <div class="previewScrollPane">
		<div class="dataInputRow">
			<div class="col-sm-2 col-md-2"><focus:LocalisedLabel runat="server" ID="ResumeFormatLabel" AssociatedControlID="ddlResumeFormat" CssClass="dataInputLabel" LocalisationKey="ResumeFormat.Label" DefaultText="Resume style"/></div>
			<div class="dataInputField col-sm-4 col-md-4">
				<asp:DropDownList ID="ddlResumeFormat" runat="server" AutoPostBack="true" Width="200px" OnSelectedIndexChanged="ddlResumeFormat_SelectedIndexChanged" ClientIDMode="Static"></asp:DropDownList>
			</div>
			<div class="col-sm-1 col-md-2"><focus:LocalisedLabel runat="server" ID="ResumeOrderLabel" AssociatedControlID="ddlResumeOrder" CssClass="dataInputLabel" LocalisationKey="ResumeOrder.Label" DefaultText="Resume order"/></div>
			<div class="dataInputField col-sm-5 col-md-4">
				<asp:DropDownList ID="ddlResumeOrder" runat="server" Width="220px" AutoPostBack="true" OnSelectedIndexChanged="ddlResumeFormat_SelectedIndexChanged" ClientIDMode="Static">
					<asp:ListItem Text="Emphasize my experience" Value="experience"></asp:ListItem>
					<asp:ListItem Text="Emphasize my education" Value="education"></asp:ListItem>
					<asp:ListItem Text="Emphasize my skills" Value="skills"></asp:ListItem>
				</asp:DropDownList>
			</div>
		</div>
		<focus:LocalisedLabel runat="server" ID="ResumePreviewInfoLabel" CssClass="modalMessage" LocalisationKey="ResumePreviewInfo.Label" DefaultText="You'll be able to hide parts of your resume after it's completed."/>
		<div class="resumePreviewDisplay">
			<asp:UpdatePanel UpdateMode="Always" ID="upPanResume" runat="server">
				<ContentTemplate>
					<iframe id="ResumeDetail" runat="server" width="800px" height="400px" title="ResumeDetail">Resume Detail</iframe>
					<asp:Panel ID="panFormattedOutput" runat="server" CssClass="resumePreviewFormattedOutput">
						<asp:Label ID="lblFormattedOutput" Width="99%" runat="server"></asp:Label>
					</asp:Panel>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="ddlResumeFormat" EventName="SelectedIndexChanged" />
					<asp:AsyncPostBackTrigger ControlID="ddlResumeOrder" EventName="SelectedIndexChanged" />
				</Triggers>
			</asp:UpdatePanel>
		</div>
        </div>
	</div>
</div>
<script type="text/javascript">
	function EnableDisablePreviewResumeOrder() {
		if (!$('#ddlResumeFormat').is(':disabled')) {
			if ($('#ddlResumeFormat').val() !== 'Original') {
				$('#ddlResumeOrder').prop('disabled', false).next().removeClass('stateDisabled');
			} else {
				$('#ddlResumeOrder').prop('disabled', true).next().addClass('stateDisabled');
			}
		} else {
			$('#ddlResumeFormat').prop('disabled', true).next().addClass('stateDisabled');
			$('#ddlResumeOrder').prop('disabled', true).next().addClass('stateDisabled');
		}
	}

	$(document).ready(function () {
		if ($('#lbSelectedStatements').exists()) {
			alwaysOnTop.init({
				targetid: 'selectedQuestions',
				orientation: 4,
				position: [50, 5],
				fadeduration: [0, 0],
				frequency: 1,
				hideafter: 0
			});
		}

		Sys.Application.add_load(ResumePreview_PageLoad);
		function ResumePreview_PageLoad(sender, args) {
			// Jaws Compatibility
			var modal = $find('ResumePreviewModal');
			if (modal != null) {
				modal.add_shown(function () {
					$('#ResumePreviewModalPanel').attr('aria-hidden', false);
					$('.page').attr('aria-hidden', true);
					$('#ddlResumeFormat').focus();
				});

				modal.add_hiding(function () {
					$('#ResumePreviewModalPanel').attr('aria-hidden', true);
					$('.page').attr('aria-hidden', false);
				});
			}
		}

		if ($('#ddlCounty').exists())
			$(".CountyClass").next().find(':first-child').text($("#ddlCounty option:selected").text()).parent().addClass('changed');

		if ($('#ddlMSA').exists())
			$(".MSAClass").next().find(':first-child').text($("#ddlMSA option:selected").text()).parent().addClass('changed');

		if ($('#ddlRank').exists())
			$(".RankClass").next().find(':first-child').text($("#ddlRank option:selected").text()).parent().addClass('changed');

		if ($('#chkCurrentlyWorkHere').exists())
			chkJobTitle();

		if ($('#SelectedStatementsHiddenField').exists() && $('#lbSelectedStatements').exists())
			UpdateSelectedStatementsList();
	});
</script>
