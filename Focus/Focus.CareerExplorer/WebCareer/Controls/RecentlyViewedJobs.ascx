﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecentlyViewedJobs.ascx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.Controls.RecentlyViewedJobs" %>
<asp:Panel ID="Panel1" runat="server">
	<p>
		<asp:LinkButton ID="StartJobSearchLink" runat="server" OnClick="StartJobSearchLink_Click"><focus:LocalisedLabel runat="server" ID="StartJobSearchLabel" RenderOuterSpan="False" LocalisationKey="StartJobSearch.Label" DefaultText="Start your job search now"/></asp:LinkButton>
	</p>
	<br />
</asp:Panel>
<asp:Panel ID="Panel2" runat="server">
	<asp:Repeater ID="RecentlyViewedJobRepeater" runat="server" OnItemDataBound="RecentlyViewedJobRepeater_ItemDataBound">
		<HeaderTemplate>
			<table class="homeJobList" role="presentation">
				<tbody>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
				<td class="column1">
					<asp:Label ID="lblViewedTime" runat="server"></asp:Label>&nbsp;-&nbsp;
				</td>
				<td>
					<asp:LinkButton ID="linkJobTitle" runat="server" OnClientClick="showProcessing();"
						OnClick="LinkJobTitle_Click"></asp:LinkButton>
					,&nbsp;<asp:Label ID="lblEmployer" runat="server"></asp:Label>
				</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
				</tbody>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</asp:Panel>
