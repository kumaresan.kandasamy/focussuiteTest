﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Xml.XPath;
using Focus.Common.Authentication;
using Focus.Core.Views;
using Framework.Core;
using Framework.DataAccess;

using Focus.CareerExplorer.Code;
using Focus.CareerExplorer.Controls;
using Focus.Common.Extensions;
using Focus.Common.Helpers;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Services.Core;

using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class Preferences : UserControlBase
	{
		#region Page Properties

    protected string InternshipCheckboxLabel = "";

    #endregion

    //public event EventHandler SaveResumeTitle;

    protected void Page_Load(object sender, EventArgs e)
    {
			if (!IsPostBack)
			{
				LocaliseUI();
				ApplyTheme();
			}
    }

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			ScriptManager.RegisterClientScriptInclude(this, GetType(), "resumeBuilderPreferencesScriptInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.ResumeBuilder.Preferences.min.js"));
			RegisterCodeValuesJson("resumeBuilderPreferencesScriptValues", "resumePreferencesCodeValues", InitialiseClientSideCodeValues());
		}

    /// <summary>
    /// Handles the ItemRemoved event of the SkillUpdateableList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    protected void SkillUpdateableList_ItemRemoved(object sender, CommandEventArgs e)
    {
      var model = App.GetSessionValue<ResumeModel>("Career:Resume");
      var resumeSkills = model.ResumeContent.Skills;

      if (resumeSkills.IsNotNull() && resumeSkills.InternshipSkills.IsNotNullOrEmpty())
      {
        var skillName = e.CommandArgument.ToString().Trim().ToLower();

        // Get list of skills still selected
        var currentSkillNames = ((UpdateableList) sender).Items.Select(s => s.Name.Trim().ToLower()).ToList();

        // Create a dictionary to look-up skills by name (with a slash indicating two names)
        var internshipSkillLookup = new Dictionary<String, InternshipSkill>();
        foreach (var internshipSkill in resumeSkills.InternshipSkills)
          internshipSkill.Name.Split('/').ToList().ForEach(s => internshipSkillLookup.Add(s.Trim().ToLower(), internshipSkill));

        // Get the tagged resume Xml
        var taggedResumeXml = ServiceClientLocator.ResumeClient(App).GetTaggedResume(model, true);
        var taggedResume = XDocument.Parse(taggedResumeXml);

        // Get variant names for the skill being removed
        var variants =
          taggedResume.XPathSelectElements(string.Format("//skillrollup/canonskill[@name='{0}']/variant", skillName))
            .Select(v => v.Value.Trim().ToLower())
            .Distinct()
            .ToList();

        // Get internship skills with exactly the same name
        var intershipSkillsToCheck = internshipSkillLookup
          .Where(pair => variants.Contains(pair.Key))
          .Select(pair => pair.Key.Trim().ToLower())
          .ToList();

        // Get internship skills with only a partial match
        variants.ForEach(v =>
          intershipSkillsToCheck.AddRange(
            internshipSkillLookup
              .Where(pair => (pair.Key.StartsWith(string.Concat(v, " ")) || pair.Key.EndsWith(string.Concat(" ", v))))
              .Select(pair => pair.Key.Trim().ToLower())));

        if (intershipSkillsToCheck.Any())
        {
          // Get variant skill names for skills yet to be removed
          var existingVariants = new List<string>();
          currentSkillNames.ForEach(currentName => 
            existingVariants.AddRange(
              taggedResume.XPathSelectElements(string.Format("//skillrollup/canonskill[@name='{0}']/variant", currentName))
                .Select(v => v.Value.Trim().ToLower())));

          foreach (var internshipSkillName in intershipSkillsToCheck)
          {
            // Where an internship skill may be made up of two names, check if other name is still being used
            var otherInternshipSkillNames =
              internshipSkillLookup.Where(
                pair => pair.Value == internshipSkillLookup[internshipSkillName] && pair.Key != internshipSkillName)
                                   .Select(pair => pair.Key)
                                   .ToList();
            var exists = false;
            foreach (var otherInternshipSkillName in otherInternshipSkillNames)
            {
              exists = existingVariants.Exists(v => v.Equals(otherInternshipSkillName));
              if (!exists)
                exists = existingVariants.Exists(v => otherInternshipSkillName.StartsWith(string.Concat(v, " ")) || otherInternshipSkillName.EndsWith(string.Concat(" ", v)));
            }

            if (!exists)
              SkillsAccordion.SelectedItems.Remove(internshipSkillLookup[internshipSkillName].Id);
          }
          SkillsAccordion.BindControl();
        }
      }
    }

    /// <summary>
    /// Handles the Click event of the SaveMoveToNextStepButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveMoveToNextStepButton_Click(object sender, EventArgs e)
    {
			if (SaveModel())
				Response.RedirectToRoute("ResumeWizardTab", new { Tab = "Review" });
    }

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
      BindDropdowns();
			BindPostingsInterested();
      var model = App.GetSessionValue<ResumeModel>("Career:Resume");

      string defaultSearchable = null;
      
			switch (App.Settings.CareerSearchResumesSearchable)
			{
				case ResumeSearchableOptions.Yes:
					defaultSearchable = "yes";
					break;
				case ResumeSearchableOptions.No:
					defaultSearchable = "no";
					break;
			}

      if (model.IsNotNull() && model.Special.IsNotNull())
      {
        var preference = model.Special.Preferences;
        if (preference.IsNotNull())
        {
          if (defaultSearchable.IsNull() && preference.IsResumeSearchable.HasValue)
            defaultSearchable = preference.IsResumeSearchable.AsYesNo();

          txtWages.Text = preference.Salary.ToString();

					if (App.Settings.Theme == FocusThemes.Education)
					{
						if (preference.PostingsInterestedIn.IsNotNull())
						{
							foreach (JobTypes val in Enum.GetValues(typeof(JobTypes)))
							{
								if (val != JobTypes.None && preference.PostingsInterestedIn.HasFlag(val) )
								{
									foreach (var checkbox in PostingsToSearchCheckboxList.Items.Cast<ListItem>())
									{
										if (((JobTypes)(checkbox.Value.SelectedValueToFlagEnum<JobTypes>())).HasFlag(val))
											checkbox.Selected = true;
									}
								}
							}
						}
					}

          if (preference.SalaryFrequencyId.HasValue)
            ddlPayUnit.SelectValue(preference.SalaryFrequencyId.ToString());

          if (preference.WillingToRelocate.HasValue)
            rblRelocate.SelectValue(preference.WillingToRelocate.HasValue ? preference.WillingToRelocate.AsYesNo() : "");

					if ((int)model.ResumeMetaInfo.CompletionStatus == (int)GuidedPath.UptoProfile)
					{
						// If preferences haven't previously been saved then default shift checkbox list to "First (day)"
						var shifts = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkShifts);
						var firstDay = shifts.FirstOrDefault(x => x.Key.Equals("WorkShifts.FirstDay"));
							
						if (firstDay.IsNotNull())
							cblShift.SelectValue(firstDay.Id.ToString());
					}
					else
					{
						if (preference.ShiftDetail.IsNotNull())
						{
							foreach (ListItem checkbox in cblShift.Items)
							{
								var checkboxValue = checkbox.Value.ToLong();
								if (checkboxValue.HasValue)
									checkbox.Selected = preference.ShiftDetail.ShiftTypeIds.Contains(checkboxValue.Value);
							}
						}
					}

          if (preference.ShiftDetail.IsNotNull())
          {
            rblWorkOverTime.SelectValue(preference.ShiftDetail.WorkOverTime.HasValue ? preference.ShiftDetail.WorkOverTime.AsYesNo() : "");

            if (preference.ShiftDetail.WorkWeekId.HasValue)
              ddlWorkWeek.SelectValue(preference.ShiftDetail.WorkWeekId.ToString());

            if (preference.ShiftDetail.WorkDurationId.HasValue)
            {
              var workDurationText = preference.ShiftDetail.WorkDurationId.ToString();
              if (ddlDuration.Items.FindByValue(workDurationText) != null)
                ddlDuration.SelectValue(workDurationText);
            }
          }

          cbSearchInMyState.Checked = preference.SearchInMyState.GetValueOrDefault();

          if (preference.MSAPreference.IsNotNull() && preference.MSAPreference.Any())
          {

            ddlState.SelectValue(preference.MSAPreference[0].StateId.ToString());
            ddlMSA.SelectValue(preference.MSAPreference[0].MSAId.ToString());
            ccdMSA.SelectedValue = preference.MSAPreference[0].MSAId.ToString();
            rbSearchMSA.Checked = true;
          }

          if (preference.ZipPreference.IsNotNull() && preference.ZipPreference.Any())
          {
            if (preference.ZipPreference[0].RadiusId.HasValue)
              ddlRadius.SelectValue(preference.ZipPreference[0].RadiusId.ToString());

            txtZip.Text = preference.ZipPreference[0].Zip;
            rbSearchArea.Checked = true;
          }

					//if (!rbSearchMSA.Checked && !rbSearchArea.Checked) // Fix for having changed the home based checkbox to a radio button, location preferences should take precendence over the home based preference if both have been selected
					//	HomeBasedJobPostingsRadioButton.Checked = preference.HomeBasedJobPostings.GetValueOrDefault();
        }

        if (App.Settings.Theme == FocusThemes.Education)
          BindSkills(model.ResumeContent.Skills, model.ResumeMetaInfo.ResumeId, model);

	      if (App.Settings.UnderAgeJobSeekerRestrictionThreshold > 0 && App.User.PersonId.HasValue)
	      {
		      var person = ServiceClientLocator.CandidateClient(App).GetCandidatePerson((App.User.PersonId.Value));
		      if (person == null || !person.Age.HasValue ||
		          (person.Age.Value < App.Settings.UnderAgeJobSeekerRestrictionThreshold))
		      {
			      MyPreferencesSectionContent.Visible = false;
			      MyPreferencesSectionHeading.Visible = false;
		      }
	      }
      }

      if (defaultSearchable.IsNull())
      {
        switch (App.Settings.CareerSearchResumesSearchable)
        {
          case ResumeSearchableOptions.JobSeekersOptionYesByDefault:
            defaultSearchable = "yes";
            break;
          case ResumeSearchableOptions.JobSeekersOptionNoByDefault:
            defaultSearchable = "no";
            break;
        }
      }

      rblResumeSearchable.SelectValue(defaultSearchable ?? string.Empty);

      rblResumeSearchable.Enabled = (App.Settings.CareerSearchResumesSearchable != ResumeSearchableOptions.No &&
                                   App.Settings.CareerSearchResumesSearchable != ResumeSearchableOptions.Yes);

	    var preferences = model.Special.Preferences;

      rblContactDetailsVisible.SelectValue(preferences.IsContactInfoVisible.IsNull() || (bool)preferences.IsContactInfoVisible ? "yes" : "no");
      if (rblResumeSearchable.SelectedValue == "no")
        rblContactDetailsVisible.Attributes.Add("disabled", "disabled");

	    BridgesToOppRowRadioButtons.SelectValue(preferences.BridgestoOppsInterest.GetValueOrDefault(false) ? "yes" : "no");
    }

    /// <summary>
    /// Binds the skills.
    /// </summary>
    /// <param name="skillsInfo">The skills info from the resume model.</param>
    /// <param name="resumeId">The resume id.</param>
    /// <param name="model">The resume model.</param>
    internal void BindSkills(SkillInfo skillsInfo, long? resumeId, ResumeModel model)
    {
      List<string> skill = null;
      List<InternshipSkill> internships = null;
      var autoPopulateInternshipSkills = false;

      if (skillsInfo.IsNotNull())
      {
        if (skillsInfo.Skills.IsNotNull() && skillsInfo.Skills.Count > 0)
          skill = skillsInfo.Skills.OrderBy(x => x.ToLower()).ToList();

        internships = skillsInfo.InternshipSkills;
      }

      if (skill.IsNotNull() || internships.IsNotNull())
      {
        if (skill.IsNotNull())
        {
          foreach (var item in skill.Where(item => item.IsNotNullOrEmpty()))
            SkillUpdateableList.AddItem(new Item { Id = item, Name = Utilities.TitleCase(item), Type = "SKILL" });
        }

        if (skillsInfo.SkillsAlreadyCaptured.HasValue)
        {
          SkillsAlreadyCapturedBox.Checked = skillsInfo.SkillsAlreadyCaptured.Value;

          if (internships.IsNotNullOrEmpty())
            SkillsAccordion.SelectedItems = internships.Select(s => s.Id).ToList();
        }
        else
        {
          autoPopulateInternshipSkills = true;
        }
      }
      else
      {
        autoPopulateInternshipSkills = true;
      }

      if (autoPopulateInternshipSkills)
      {
        if (model.ResumeContent.IsNotNull() && model.ResumeContent.ExperienceInfo.IsNotNull())
        {
          var jobs = model.ResumeContent.ExperienceInfo.Jobs;
          if (jobs.IsNotNullOrEmpty())
          {
            var internshipSkillIds = new List<long>();
            foreach (var internshipSkills in jobs.Select(job => job.InternshipSkills).Where(internshipSkills => internshipSkills.IsNotNullOrEmpty()))
            {
              internshipSkillIds.AddRange(internshipSkills.Where(s => !internshipSkillIds.Contains(s.Id)).Select(s => s.Id));
            }
            SkillsAccordion.SelectedItems = internshipSkillIds;
          }
        }
      }

      if (SkillUpdateableList.Items.IsNullOrEmpty())
      {
        SkillsInstructionsLabel.Visible = false;
        NoSkillsLabel.Visible = true;
      }
      else
      {
        SkillsInstructionsLabel.Visible = true;
        NoSkillsLabel.Visible = false;
      }
    }

		/// <summary>
		/// Performs some extra server side validation on the model
		/// </summary>
		/// <param name="tempPreference">The model to validate.</param>
		/// <returns>Whether the model is valid</returns>
		private bool ValidateModel(Core.Models.Career.Preferences tempPreference)
		{
			if (tempPreference.Salary.GetValueOrDefault() > 0 && tempPreference.SalaryFrequencyId.IsNullOrZero())
			{
				PayUnitValidator.ErrorMessage = CodeLocalise("PayUnit.ValidatorErrorMessage", "Pay type is required");
				PayUnitValidator.IsValid = false;
				return false;
			}

			return true;
		}

    /// <summary>
    /// Saves the model.
    /// </summary>
    /// <returns></returns>
    private bool SaveModel()
    {
      bool saveStatus;
      var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
      var clonedResume = currentResume.CloneObject();

      var skill = new List<string>();

      #region Education specific data

      if (App.Settings.Theme == FocusThemes.Education)
      {
        if (SkillUpdateableList.Items.IsNotNull())
          skill.AddRange(SkillUpdateableList.Items.Select(item => item.Name));

        var skillInfo = currentResume.ResumeContent.Skills;

        if (skillInfo.IsNull())
          skillInfo = new SkillInfo();

        var hiddenInfo = currentResume.Special.HideInfo;

        if (hiddenInfo.IsNull())
          hiddenInfo = new HideResumeInfo();

        if (hiddenInfo.HiddenSkills.IsNull())
          hiddenInfo.HiddenSkills = new List<string>();

        if (skillInfo.Skills.IsNotNullOrEmpty())
        {
          var newSkills = SkillUpdateableList.Items.Select(s => s.Name.ToLower()).ToList();

          foreach (var removedSkill in skillInfo.Skills.Where(currentSkill => !newSkills.Contains(currentSkill, StringComparer.OrdinalIgnoreCase)))
          {
            hiddenInfo.HiddenSkills.Add(removedSkill);
          }
        }

        skillInfo.Skills = skill;

        skillInfo.InternshipSkills = new List<InternshipSkill>();

        var skillIndex = 0;
        foreach (var skillId in SkillsAccordion.SelectedItems)
        {
          skillInfo.InternshipSkills.Add(new InternshipSkill
          {
            Id = skillId,
            Name = SkillsAccordion.SelectedNames[skillIndex]
          });

          skillIndex++;
        }

        skillInfo.SkillsAlreadyCaptured = SkillsAlreadyCapturedBox.Checked;
      }


      #endregion

      var tempZips = new List<DesiredZip>();
      var tempMSAs = new List<DesiredMSA>();

      if (rbSearchArea.Checked)
      {
        var radiusId = ddlRadius.SelectedValueToLong(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).Where(x => x.Key == App.Settings.DefaultSearchRadiusKey).Select(x => x.Id).SingleOrDefault());

        if (txtZip.TextTrimmed().IsNotNullOrEmpty())
        {
          var zip = new DesiredZip
          {
            RadiusId = radiusId,
            Zip = txtZip.TextTrimmed(defaultValue: null)
          };
          tempZips.Add(zip);
        }
      }

    	if (rbSearchMSA.Checked)
      {
        var msa = new DesiredMSA
        {
          StateId = ddlState.SelectedValueToLong(),
          StateName = ddlState.SelectedItem.Text,
          MSAId = ddlMSA.SelectedValueToLong()
        };
        tempMSAs.Add(msa);
      }

    	var postingsInterestedIn = new JobTypes();

			if (App.Settings.Theme == FocusThemes.Education)
			{
				foreach (var checkbox in PostingsToSearchCheckboxList.Items.Cast<ListItem>().Where(checkbox => (checkbox.Selected && checkbox.Enabled)))
				{
					postingsInterestedIn |= checkbox.Value.AsEnum<JobTypes>().GetValueOrDefault();
				}

			}

      var tempPreference = new Core.Models.Career.Preferences
      {
        IsResumeSearchable = rblResumeSearchable.SelectedValue.AsBoolean(),
				BridgestoOppsInterestChanged = false,
				BridgestoOppsInterest = BridgesToOppRowRadioButtons.SelectedValue.AsBoolean(),
				IsContactInfoVisible = rblContactDetailsVisible.SelectedValue.AsBoolean(),
        Salary = txtWages.TextTrimmed().AsFloat().AsNullIfDefault<float>(),
        SalaryFrequencyId = ddlPayUnit.SelectedValueToLong(),
        ShiftDetail = new Shift
        {
          WorkOverTime = rblWorkOverTime.SelectedValue.AsBoolean(),
          WorkWeekId = ddlWorkWeek.SelectedValueToLong(),
          WorkDurationId = ddlDuration.SelectedValueToLong(),
          ShiftTypeIds = GetShiftType(),
        },
        WillingToRelocate = rblRelocate.SelectedValue.AsBoolean(),
        SearchInMyState = cbSearchInMyState.Checked,
        ZipPreference = tempZips.Count > 0 ? tempZips : null,
        MSAPreference = tempMSAs.Count > 0 ? tempMSAs : null,
				PostingsInterestedIn = App.Settings.Theme == FocusThemes.Education ? postingsInterestedIn : JobTypes.None,
				//HomeBasedJobPostings = HomeBasedJobPostingsRadioButton.Checked
      };

			if (App.Settings.UnderAgeJobSeekerRestrictionThreshold > 0 && App.User.PersonId.HasValue)
			{
	    var person = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(App.User.PersonId.Value);
				if (person == null || !person.Age.HasValue || (person.Age.Value < App.Settings.UnderAgeJobSeekerRestrictionThreshold))
				{
					tempPreference.IsResumeSearchable = false;
				}
			}

      //if ((currentResume.ResumeMetaInfo.CompletionStatus & ResumeCompletionStatus.Preferences) == ResumeCompletionStatus.Preferences
      //  && currentResume.IsNotNull()
      //  && currentResume.Special.IsNotNull()
      //  && !Utilities.IsObjectModified(currentResume.Special.Preferences, tempPreference))
      //  return true;

	    if (App.Settings.ShowBridgestoOppsInterest && App.Settings.BridgestoOppsInterestActivityExternalId > 0 && tempPreference.BridgestoOppsInterest.GetValueOrDefault(false))
	    {
		    var previousBridges = false;
				if (currentResume.IsNotNull() && currentResume.Special.IsNotNull() && currentResume.Special.Preferences.IsNotNull())
					previousBridges = currentResume.Special.Preferences.BridgestoOppsInterest.GetValueOrDefault(false);

				if (!previousBridges)
				{
					tempPreference.BridgestoOppsInterestChanged = true;
				}
	    }

      var previousResumeCompletionStatus = ResumeCompletionStatuses.None;

      if (currentResume.IsNull())
      {
        currentResume = new ResumeModel
        {
          Special = new ResumeSpecialInfo
          {
            Preferences = tempPreference
          },
          ResumeMetaInfo = new ResumeEnvelope { CompletionStatus = ResumeCompletionStatuses.Preferences }
        };
      }
      else
      {
        previousResumeCompletionStatus = currentResume.ResumeMetaInfo.CompletionStatus;
        if (currentResume.Special.IsNull())
          currentResume.Special = new ResumeSpecialInfo();
        currentResume.Special.Preferences = tempPreference;

        //if (previousResumeCompletionStatus < (int)ResumeCompletionStatus.Preferences)
        currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.Preferences;
      }

      bool resumeNameUpdated = false;
      var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
      if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
      {
        currentResume.ResumeMetaInfo.ResumeName = resumeName;
        resumeNameUpdated = true;

      }

			if (!ValidateModel(tempPreference))
				return false;

			if (!Utilities.IsObjectModified(clonedResume, currentResume))
        return true;

			try
      {
				
				var resumeModel = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);
        var resumeId = resumeModel.ResumeMetaInfo.ResumeId;

        //if (SaveResumeTitle != null)
        //  SaveResumeTitle(this, EventArgs.Empty);

        if (!App.UserData.HasDefaultResume)
          App.UserData.DefaultResumeId = resumeId;
        if (App.UserData.DefaultResumeId == resumeId)
          Utilities.UpdateUserContextUserInfo(null, ResumeCompletionStatuses.Preferences);
        
				if (currentResume.ResumeMetaInfo.ResumeId.IsNull())
          currentResume.ResumeMetaInfo.ResumeId = resumeId;

        App.SetSessionValue("Career:ResumeID", resumeId);
        App.SetSessionValue("Career:Resume", currentResume);

        #region Save Activity
        if (App.UserData.HasDefaultResume && App.UserData.DefaultResumeId == resumeId)
        {
          //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "37", staffProfile: staffcontext.StaffInfo);

          ////Save Resume Complete Activity, should be at first time resume complete
          //if (previousResumeCompletionStatus < (int)GuidedPath.UptoPreferences)
          //{
          //  if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //    ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "RES01", staffProfile: staffcontext.StaffInfo);
          //  else
          //    ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.JobSeeker, ActivityType.Automated, "RES01");
          //}
        }
        #endregion

        if (resumeId != null) SaveSearchAgainstResume(currentResume, (long)resumeId);
        saveStatus = true;
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
        saveStatus = false;
      }
      return saveStatus;
    }

    /// <summary>
    /// Gets the type of the shift.
    /// </summary>
    /// <returns></returns>
    private List<long> GetShiftType()
    {
      var shiftTypes = new List<long>();
      foreach (var checkbox in cblShift.Items.Cast<ListItem>().Where(checkbox => checkbox.Selected))
      {
        var shiftTypeId = checkbox.Value.ToLong();
        if (shiftTypeId.HasValue)
          shiftTypes.Add(shiftTypeId.Value);
      }

      return shiftTypes;
    }

    # region Dropdown binding & UI Localization

    /// <summary>
    /// Binds the dropdowns.
    /// </summary>
    private void BindDropdowns()
    {
      ddlPayUnit.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Frequencies), null, CodeLocalise("PayUnit.TopDefault", "- select pay type -"));
      ddlWorkWeek.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkWeeks), null, CodeLocalise("WorkWeek.TopDefault", "- select work week -"));

      var excludeDurations = App.Settings.HideSeasonalDuration
        ? new List<string> { "Duration.PartTimeSeasonal", "Duration.FullTimeSeasonal" }
        : null;
      ddlDuration.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Durations), null, CodeLocalise("Duration.TopDefault", "- select duration -"), string.Empty, excludeDurations);

      ddlRadius.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses), null, CodeLocalise("Radius.TopDefault", "- select radius -"));

      var nearbyStates = (from s in ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States) where App.Settings.NearbyStateKeys.Contains(s.Key) select s).ToList();
      ddlState.BindLookup(nearbyStates, null, CodeLocalise("State.TopDefault", "- select state -"), "0");
      ddlState.Items[0].Value = "none";

			cblShift.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.WorkShifts));
    }

    /// <summary>
    /// Binds the postings interested.
    /// </summary>
		private void BindPostingsInterested()
		{
			PostingsToSearchCheckboxList.Items.Clear();

			PostingsToSearchCheckboxList.Items.AddEnum(JobTypes.Job, "jobs");
			PostingsToSearchCheckboxList.Items.AddEnum(JobTypes.InternshipPaid | JobTypes.InternshipUnpaid, "internships");

			InternshipCheckboxLabel = CodeLocalise("Internships.Label", "internships");
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      cbAny.Text = CodeLocalise("Shift.Any", "Any");
      RadiusValidator.ErrorMessage = CodeLocalise("RadiusLocation.RequiredErrorMessage", "Both radius and 5 digit zip code is required");
      StateValidator.ErrorMessage = CodeLocalise("StateValidator.RequiredErrorMessage", "Please select a state");
      cbSearchInMyState.Text = CodeLocalise("OnlyShow.Label", "Only show in-state jobs");
			//HomeBasedJobPostingsRadioButton.Text = CodeLocalise("HomeBasedJobPostingsRadioButton.Text", "Include home-based job postings");
      ResumeSearchableRequired.ErrorMessage = CodeLocalise("ResumeSearchable.RequiredErrorMessage", "Resume searchable status is required");
      PostingsToSearchValidator.ErrorMessage = CodeLocalise("PostingsToSearched.RequiredErrorMessage", "At least one posting type is required");
      WorkWeekRequired.ErrorMessage = CodeLocalise("WorkWeek.RequiredErrorMessage", "Work week is required");
      DurationRequired.ErrorMessage = CodeLocalise("Duration.RequiredErrorMessage", "Duration is required");
      ShiftValidator.ErrorMessage = CodeLocalise("Shift.RequiredErrorMessage", "Shift is required");
      WorkOverTimeValidator.ErrorMessage = CodeLocalise("WorkOverTime.RequiredErrorMessage", "Answer work overtime question");
      RelocateValidator.ErrorMessage = CodeLocalise("Relocate.RequiredErrorMessage", "Relocation preference is required");
      SkillsAlreadyCapturedBox.Text = HtmlLocalise("SkillsAlreadyCapturedBox.Label",
                                                   "My skill set has already been captured by my program of study and work history information");
      SkillsAlreadyCapturedValidator.ErrorMessage = CodeLocalise("SkillsAlreadyCapturedBox.ErrorMessage", "Please select at least one skill");
			rbSearchMSA.Text = CodeLocalise("Preferences.rbSearchMSA.Text", "Search this state/city");

      LocationPreferenceValidator.Text = CodeLocalise("LocationPreferenceValidator.Text", "Please enter a location preference");
      RelocateInstructionsLabel.DefaultText = CodeLocalise("RelocateInstructionsLabel.Text", "If you select “yes,” you may be contacted about job postings outside of your preferred location preferences. If you do not wish to be contacted about jobs outside of your preferred location preferences, please select “no.”");

			if (!App.Settings.TalentModulePresent)
			{
				ResumeSearchableRow.Visible = false;
				ContactVisibleToEmployersRow.Visible = false;
			}
			else if (App.Settings.TalentModulePresent && App.Settings.ShowContactDetails == ShowContactDetails.None)
			{
				ContactVisibleToEmployersRow.Visible = false;
			}
			else if (App.Settings.ShowContactDetails != ShowContactDetails.None)
				ResumeSearchableInstructionsLabel.Visible = false;
		}

		/// <summary>
		/// Applies the theme (e.g. Workforce or Education).
		/// </summary>
		private void ApplyTheme()
		{
			if (App.Settings.Theme == FocusThemes.Education)
			{
				#region Label text changes

				MyPreferencesSubHeadingLabel.DefaultText = CodeLocalise("MyPreferenceEducation.Label", "Resume and job preferences");

				#endregion

				#region Hide controls

				WorkforcePreferencesPlaceHolder1.Visible = false;
				WorkforcePreferencesPlaceHolder2.Visible = false;
				WorkforcePreferencesPlaceHolder3.Visible = false;
				WorkforcePreferencesPlaceHolder4.Visible = false;
				WorkforcePreferencesPlaceHolder5.Visible = false;

				#endregion

				#region Display controls

				EducationPreferencesPlaceHolder1.Visible = true;
			  EducationPreferencesPlaceHolder2.Visible = true;

				#endregion

        DurationLabel.Visible = 
          ddlDuration.Visible = 
			    DurationRequired.Visible = App.Settings.DisplayResumeBuilderDuration;
			}

			//if (App.Settings.Theme == FocusThemes.Education || !App.Settings.TalentModulePresent)
			//	HomeBasedJobPostingsPanel.Visible = false;
				
			BridgesToOppRow.Visible =
				BridgesToOppModalPlaceHolder.Visible = App.Settings.ShowBridgestoOppsInterest;
		}

    /// <summary>
    /// Initialises the client side code values.
    /// </summary>
    /// <returns></returns>
		private NameValueCollection InitialiseClientSideCodeValues()
    {
      var maxSalaryThreshold = Convert.ToDouble(App.Settings.MaxPreferredSalaryThreshold);

      var maxThresholds = new Dictionary<string, string>
      {
        {"Hourly", CalculatateThreshold(maxSalaryThreshold, 260*8)},
        {"Daily", CalculatateThreshold(maxSalaryThreshold, 260)},
        {"Weekly", CalculatateThreshold(maxSalaryThreshold, 52)},
        {"Monthly", CalculatateThreshold(maxSalaryThreshold, 12)},
        {"Yearly", maxSalaryThreshold.ToString("0.00")}
      };

      var minThresholds = new Dictionary<string, string>
      {
        {"Hourly", "4.90"},
        {"Daily", "39.25"},
        {"Weekly", "196.00"},
        {"Monthly", "850.00"},
        {"Yearly", "10192.00"}
      };

			var nvc = new NameValueCollection
				          {
										{ "ccdMSABehaviorID", ccdMSA.BehaviorID },
										{ "errorPayUnitRequired", CodeLocalise("PayUnit.ValidatorErrorMessage", "Pay type is required") },
										{ "errorWagesRequired", CodeLocalise("Wages.RequiredErrorMessage", "Wages is required") },
										{ "frequencyHourly", GetLookupId(Constants.CodeItemKeys.Frequencies.Hourly).ToString() },
										{ "frequencyDaily", GetLookupId(Constants.CodeItemKeys.Frequencies.Daily).ToString() },
										{ "frequencyWeekly", GetLookupId(Constants.CodeItemKeys.Frequencies.Weekly).ToString() },
										{ "frequencyMonthly", GetLookupId(Constants.CodeItemKeys.Frequencies.Monthly).ToString() },
										{ "frequencyYearly", GetLookupId(Constants.CodeItemKeys.Frequencies.Yearly).ToString() },
										{ "labelInternshipCheckbox", InternshipCheckboxLabel }
				          };

      foreach (var item in maxThresholds)
      {
        nvc.Add(string.Format("max{0}Threshold", item.Key), item.Value);
        nvc.Add(string.Format("min{0}Threshold", item.Key), minThresholds[item.Key]);
        nvc.Add(string.Format("error{0}PayLimit", item.Key), CodeLocalise("PayUnit.{0}LimitErrorMessage", "{0} wage preference must be between: ${1} to ${2}", item.Key, minThresholds[item.Key], item.Value));
      }

			return nvc;
		}

    /// <summary>
    /// Calculate the threshold for a time period
    /// </summary>
    /// <param name="value">The original value</param>
    /// <param name="divisor">The divisor representing the time period</param>
    /// <returns>A formatting value</returns>
    private static string CalculatateThreshold(double value, double divisor)
    {
      return (Math.Floor((value/divisor)*4)/4).ToString("0.00");
    }

	  #endregion

    /// <summary>
    /// Handles the Click event of the SkillAddButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SkillAddButton_Click(object sender, EventArgs e)
    {
      if (SkillTextBox.TextTrimmed().IsNotNullOrEmpty())
      {
        SkillUpdateableList.AddTop(new Item { Id = SkillTextBox.TextTrimmed(), Name = SkillTextBox.TextTrimmed(), Type = "SKILL" });
        SkillTextBox.Text = "";
      }
    }

    /// <summary>
    /// Saves the search against resume.
    /// </summary>
    /// <param name="model">The model.</param>
    /// <param name="resumeid">The resumeid.</param>
    private void SaveSearchAgainstResume(ResumeModel model, long resumeid)
    {
      var criteria = new SearchCriteria();

      #region Job Matching Criteria
			
			criteria.RequiredResultCriteria = new RequiredResultCriteria { MinimumStarMatch = StarMatching.ThreeStar };
      
			#endregion

      #region Posting Age Criteria

      criteria.PostingAgeCriteria = new PostingAgeCriteria { PostingAgeInDays = App.Settings.JobSearchPostingDate };
      
			#endregion

      #region Job Location Criteria
      
			var joblocation = new JobLocationCriteria();

      if (!App.Settings.NationWideInstance)
        joblocation.Area = new AreaCriteria { StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault() };

      if (model.IsNotNull() && model.Special.IsNotNull())
      {
        var preference = model.Special.Preferences;
        if (preference.IsNotNull())
        {
          if (preference.SearchInMyState.HasValue && (bool)preference.SearchInMyState)
          {
            //UserContext usercontext = App.GetSessionValue<UserContext>("Career:UserContext");
            joblocation.OnlyShowJobInMyState = true;
            joblocation.Area = new AreaCriteria { StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault() };
          }

          if (preference.ZipPreference.IsNotNull() && preference.ZipPreference.Count > 0)
          {
            var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).FirstOrDefault(x => x.Id == preference.ZipPreference[0].RadiusId);
            var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
            var radius = radiusExternalId.ToLong();

          	joblocation.Radius = new RadiusCriteria
          	                     	{
          	                     		Distance = (radius.HasValue) ? radius.Value : 0,
          	                     		DistanceUnits = DistanceUnits.Miles,
          	                     		PostalCode = preference.ZipPreference[0].Zip
          	                     	};

            if (preference.SearchInMyState.AsBoolean())
            {
              var state = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

              if (App.Settings.NationWideInstance)
                if (App.UserData.HasProfile && App.UserData.Profile.PostalAddress.IsNotNull() && App.UserData.Profile.PostalAddress.StateId.HasValue)
                  state = App.UserData.Profile.PostalAddress.StateId.Value;

              joblocation.Area = new AreaCriteria { StateId = state };
            }
          }
          else if (preference.MSAPreference.IsNotNull() && preference.MSAPreference.Count > 0)
          {
            //var state = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();
            //var stateLookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Id == preference.MSAPreference[0].StateId).FirstOrDefault();
            //var stateExternalId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Id == preference.MSAPreference[0].StateId).Select(x => x.ExternalId).FirstOrDefault();

            joblocation.Area = new AreaCriteria
            {
                StateId = preference.MSAPreference[0].StateId,
              MsaId = preference.MSAPreference[0].MSAId
            };
            
						//joblocation.SelectedStateInCriteria = stateExternalId;

            //joblocation.Area = new AreaCriteria { State = preference.MSAPreference[0].StateCode };
            //if (preference.MSAPreference[0].MSAName.IsNotNull())
            //  joblocation.Area.MSA = preference.MSAPreference[0].MSAName;
          }

          //joblocation.HomeBasedJobPostings = preference.HomeBasedJobPostings.GetValueOrDefault();

          criteria.JobLocationCriteria = joblocation;
        }
      }
      #endregion

      #region Reference Document Criteria

    	criteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
    	                                     	{
    	                                     		DocumentId = resumeid.ToString(),
    	                                     		DocumentType = DocumentType.Resume
    	                                     	};
      
			#endregion

      App.SetSessionValue("Career:SearchCriteria", criteria);

      try
      {
        ServiceClientLocator.AnnotationClient(App).SaveSearch(0, "Job Leads " + model.ResumeMetaInfo.ResumeId, criteria, SavedSearchTypes.CareerPreferencesSearch, EmailAlertFrequencies.Weekly, EmailFormats.HTML, "", AlertStatus.Inactive);
      }
      catch
      {

      }
    }

    /// <summary>
    /// Saves this instance.
    /// </summary>
    /// <returns></returns>
		public bool Save()
		{
			return SaveModel();
		}
  }
}
