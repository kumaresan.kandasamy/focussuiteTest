﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MyResumes.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.MyResumes" %>
<asp:Panel ID="Panel1" runat="server" CssClass="panel panel-lightbox">
	<div class="panel-body">
		<p><a href="<%= UrlBuilder.YourResume() %>">
	     <focus:LocalisedLabel runat="server" ID="lblMyResumesLink" LocalisationKey="MyResumes2.LinkText" DefaultText="Create or upload a resume best bet for best matches" RenderOuterSpan="False" />
	   </a></p>
		<p><small><focus:LocalisedLabel runat="server" ID="lblMyResumesLimited" LocalisationKey="MyResumes3.Label" DefaultText="You can search for jobs without a resume, but results are limited." RenderOuterSpan="False"/></small></p>
	</div>
</asp:Panel>
<asp:Panel ID="Panel2" runat="server" CssClass="panel panel-lightbox">
	<asp:PlaceHolder runat="server" ID="CreateResumePanel">
		<div class="panel-heading">
			<focus:LocalisedLabel runat="server" ID="lblCreateResumeFromExisting" LocalisationKey="MyResumes4.Label" DefaultText="Create a new resume based on an existing one" RenderOuterSpan="False"/>
		</div>
		<div class="panel-body">
			<focus:LocalisedLabel runat="server" ID="MyResumesInfoLocalisedLabel" AssociatedControlID="ddlResumeNames" LocalisationKey="MyResumesInfo.Label" DefaultText="You can create a new resume based upon the contents of an existing one. Later, you can edit this resume."/>
			<div class="form-group">
				<asp:DropDownList CssClass="form-control" ID="ddlResumeNames" runat="server" Title="My Resumes">
					<asp:ListItem Text="This Is My Resume Name" />
				</asp:DropDownList>
				<asp:RequiredFieldValidator ID="ResumeNameRequired" runat="server" ControlToValidate="ddlResumeNames" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="MyResume" />
			</div>
		</div>
		<div class="panel-footer">
			<focus:ModernButton ID="btnStart" runat="server" CssClass="btn btn-default" IconProvider="FontAwesome" IconCssClass="fa-caret-right" IconPlacement="Right" OnClick="btnStart_Click" ValidationGroup="MyResume" />
		</div>
  </asp:PlaceHolder>
</asp:Panel>