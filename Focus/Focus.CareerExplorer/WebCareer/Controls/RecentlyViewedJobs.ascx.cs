﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class RecentlyViewedJobs : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				if (App.User.IsAuthenticated)
				{
					Panel1.Visible = false;
					Panel2.Visible = true;

					LoadRecentlyViewedPostings();
				}
				else
				{
					Panel1.Visible = true;
					Panel2.Visible = false;
				}
			}
		}

    /// <summary>
    /// Loads the recently viewed postings.
    /// </summary>
		private void LoadRecentlyViewedPostings()
		{
			try
			{
				var rPostings = ServiceClientLocator.PostingClient(App).GetViewedPostings();
				if (rPostings.Count > 0)
				{
					RecentlyViewedJobRepeater.DataSource = rPostings;
					RecentlyViewedJobRepeater.DataBind();
				}
				else
				{
					Panel1.Visible = true;
					Panel2.Visible = false;
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Handles the ItemDataBound event of the RecentlyViewedJobRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void RecentlyViewedJobRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
				return;

			var posting = (ViewedPostingModel)e.Item.DataItem;

			var viewedTime = (Label)e.Item.FindControl("lblViewedTime");
			viewedTime.Text = posting.ViewedOn.ToString("MMM dd");

			var jobTitle = (LinkButton)e.Item.FindControl("linkJobTitle");
			jobTitle.Text = posting.JobTitle;

      jobTitle.CommandArgument = posting.LensPostingId;

			var employer = (Label)e.Item.FindControl("lblEmployer");
			employer.Text = posting.EmployerName;
      
		}

    /// <summary>
    /// Handles the Click event of the LinkJobTitle control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void LinkJobTitle_Click(object sender, EventArgs e)
		{
			var btnTitle = (LinkButton)sender;
			Response.RedirectToRoute("JobPosting", new { jobid = btnTitle.CommandArgument, fromURL = "Home", pageSection = "RecentlyViewedPostings"});
		}

    /// <summary>
    /// Handles the Click event of the StartJobSearchLink control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void StartJobSearchLink_Click(object sender, EventArgs e)
		{
			App.SetSessionValue("Career:SearchCriteria", new SearchCriteria());
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
		}
	}
}