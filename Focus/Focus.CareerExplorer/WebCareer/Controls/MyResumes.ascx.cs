﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Controls;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Framework.Core;
using System.Collections.Generic;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class MyResumes : UserControlBase
	{
		public Dictionary<string, string> Resumes { get; set; }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
		{
			DisplayMyResume();
			LocaliseUI();
			if (!IsPostBack)
				Bind();
		}

    /// <summary>
    /// Localises the UI.
    /// </summary>
		private void LocaliseUI()
		{
			btnStart.Text = CodeLocalise("Next.Button", "Next");
			ResumeNameRequired.ErrorMessage = CodeLocalise("ResumeName.RequiredErrorMessage", "Resume name is required");
		}

    /// <summary>
    /// Displays my resume.
    /// </summary>
		private void DisplayMyResume()
		{
			if (App.User.IsAuthenticated)
			{
				Panel1.Visible = false;
				Panel2.Visible = true;

        CreateResumePanel.Visible = (MasterPage.ResumeCount > 0 && (App.Settings.MaximumAllowedResumeCount == 0 || MasterPage.ResumeCount < App.Settings.MaximumAllowedResumeCount));
			}
			else
			{
				Panel1.Visible = true;
				Panel2.Visible = false;
			}
		}

    /// <summary>
    /// Binds this instance.
    /// </summary>
		private void Bind()
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					var resumeInfo = Resumes;
					ddlResumeNames.BindDictionary(resumeInfo, null, CodeLocalise("Global.PersonalTitle.TopDefault", "- select resume -"));

					if (resumeInfo.IsNull() || (resumeInfo.IsNotNull() && resumeInfo.Count == 0))
						ddlResumeNames.Enabled = btnStart.Enabled = false;
					else
						ddlResumeNames.Enabled = btnStart.Enabled = true;
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Handles the Click event of the btnStart control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void btnStart_Click(object sender, EventArgs e)
		{
			if (ddlResumeNames.SelectedIndex > 0)
			{
				var resumeId = ddlResumeNames.SelectedValueToLong();
				App.SetSessionValue("Career:ResumeID", resumeId);
				App.SetSessionValue("Career:DuplicateResume", true);
				App.RemoveSessionValue("Career:NewJobDetails");
				Response.RedirectToRoute("ResumeWizard");
			}
		}
	}
}
