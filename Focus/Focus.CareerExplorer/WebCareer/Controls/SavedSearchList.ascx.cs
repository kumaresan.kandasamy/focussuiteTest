﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class SavedSearchList : UserControlBase
	{
    /// <summary>
    /// Gets a value indicating whether the user has a program area or degree selected
    /// </summary>
    private bool HasStudyProgram
    {
      get { return App.User.ProgramAreaId.GetValueOrDefault() != 0 || App.User.DegreeEducationLevelId.GetValueOrDefault() != 0; }
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			BuildPage(false);
		}

    /// <summary>
    /// Reloads the results.
    /// </summary>
    public void ReloadResults()
    {
      BuildPage(true);
    }

		/// <summary>
		/// Gets the saved searches count.
		/// </summary>
		/// <returns></returns>
		public int GetSavedSearchesCount()
		{
			return ddlActiveSearch.Items.Count;
		}

    /// <summary>
    /// Builds the page.
    /// </summary>
    /// <param name="ignorePostBack">if set to <c>true</c> [ignore post back].</param>
    private void BuildPage(bool ignorePostBack)
    {
      if (ignorePostBack || !Page.IsPostBack) LoadSavedSearches();

      SetVisibility();
      LocaliseUI();
      // Due to needing to update this list when a program of study changes we need to force updates of the list
      updJobList.Update();
    }

    private void SetVisibility()
    {
      if ((HasStudyProgram && App.Settings.Theme == FocusThemes.Education) || ddlActiveSearch.Items.Count != 0)
      {
        Panel1.Visible = false;
        Panel2.Visible = true;
        if (ddlActiveSearch.Items.Count == 0 && App.Settings.Theme == FocusThemes.Education)
          // Searching by program of study so hide saved search options
          ddlActiveSearch.Visible = ActiveSearchLabel.Visible = lnkEditSaveSearch.Visible = ResultsSpacerPlaceHolder.Visible = false;
        else
          ddlActiveSearch.Visible = ActiveSearchLabel.Visible = lnkEditSaveSearch.Visible = ResultsSpacerPlaceHolder.Visible = true;
      }
      else
      {
        Panel2.Visible = false;
        Panel1.Visible = true;
      }
    }

    private void LocaliseUI()
    {
      if (ddlActiveSearch.Items.Count == 0)
      {
        if (!HasStudyProgram && App.Settings.Theme == FocusThemes.Education)
          NoResultsLabel.Text = CodeLocalise("NoSavedSearchOrProgramOfStudy.Text", "You do not have a program of study stored. Click on My Account and choose a program of study in order to see job recommendations");
        else
          NoResultsLabel.Text = CodeLocalise("NoSavedSearch.Text", "No results found");
      }
      ActiveSearchLabel.Text = CodeLocalise("ActiveSearch.Label", "Active Search");
    }

    /// <summary>
    /// Handles the Click event of the lnkSeeAllMatches control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lnkSeeAllMatches_Click(object sender, EventArgs e)
		{
			Response.RedirectToRoute("JobSearchResults");
		}

    /// <summary>
    /// Loads the saved searches.
    /// </summary>
		private void LoadSavedSearches()
		{
      try
      {
        if (!App.User.IsAuthenticated) return;
        var searchList = ServiceClientLocator.AnnotationClient(App).GetSavedSearches(SavedSearchTypes.CareerPostingSearch);
        App.SetSessionValue("Career:SavedSearches", searchList);

        if (searchList.IsNotNullOrEmpty())
        {
          var savedSearchInfo = (from s in searchList
                                 where s.Name.IsNotNullOrEmpty()
                                 select new LookupItemView { Id = s.SearchId, Text = s.Name }).ToList();

          //if(savedSearchInfo.IsNotNullOrEmpty())
          ddlActiveSearch.BindLookup(savedSearchInfo, null, null);
        }

        if (ddlActiveSearch.Items.Count > 0)
        {
          ddlActiveSearch.SelectedIndex = 0;
          GetSearchResult(ddlActiveSearch.SelectedItem.Text);
        }
        else if (HasStudyProgram)
          GetSearchResult(Guid.NewGuid().ToString());

        OnSavedSearchesRetrieved(new CommandEventArgs("SearchesRetrived", ddlActiveSearch.Items.Count));
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
		}

    /// <summary>
    /// Handles the IndexChanged event of the ddlActiveSearch control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ddlActiveSearch_IndexChanged(object sender, EventArgs e)
		{
			try
			{
				GetSearchResult(ddlActiveSearch.SelectedItem.Text);
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

    /// <summary>
    /// Handles the Click event of the lnkEditSaveSearch control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lnkEditSaveSearch_Click(object sender, EventArgs e)
		{
			App.SetSessionValue("Career:SavedSearchId", ddlActiveSearch.SelectedValue.ToLong());
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "savedsearch" });
		}
    
    /// <summary>
    /// Gets the search result.
    /// </summary>
    /// <param name="searchName">Name of the search.</param>
		private void GetSearchResult(string searchName)
		{
			try
			{
				var searchList = App.GetSessionValue<List<SearchDetails>>("Career:SavedSearches");

        if (searchList.IsNotNull() && searchList.FirstOrDefault(x => x.Name == searchName).IsNotNull())
        {
          var searchDetails = searchList.FirstOrDefault(x => x.Name == searchName);
          App.SetSessionValue("Career:SearchCriteria", searchDetails.SearchCriteria);
        }
        else if (HasStudyProgram)
        {
          var defaultResume = ServiceClientLocator.ResumeClient(App).GetDefaultResume();
          Utilities.SaveCriteriaAgainstResume(defaultResume, MatchingType.ProgramOfStudy);
        }
        else return;
			  JobList.LoadList();

        lnkSeeAllMatches.Visible = JobList.MoreJobsExist;
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}


    #region Events
    public delegate void SavedSearchesRetrievedHandler(object sender, CommandEventArgs eventArgs);
    public event SavedSearchesRetrievedHandler SavedSearchesRetrieved;

	  protected virtual void OnSavedSearchesRetrieved(CommandEventArgs eventargs)
	  {
	    SavedSearchesRetrievedHandler handler = SavedSearchesRetrieved;
	    if (handler != null) handler(this, eventargs);
    }
    #endregion
  }
}