﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Controls;
using Focus.Common;

namespace Focus.CareerExplorer.WebCareer.Controls
{
	public partial class ResumeList : UserControlBase
	{
		public string CssClass { get; set; }

		public string DeleteRedirectionPage
		{
			get { return MyResumeList.DeleteRedirectionPage; }
			set { MyResumeList.DeleteRedirectionPage = value; }
		}

		public string TargetPage
		{
			get { return MyResumeList.TargetPage; }
			set { MyResumeList.TargetPage = value; }
		}

		public bool SetConfirmationSession
		{
			get { return MyResumeList.SetConfirmationSession; }
			set { MyResumeList.SetConfirmationSession = value; }
		}
		public string MarginLeft { get; set; }

		public delegate void LinkNameClick( object sender, EventArgs eventArgs );
		public event LinkNameClick LinkNameClicked;

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load( object sender, EventArgs e )
		{
			if (!IsPostBack)
			{
				var resumes = ServiceClientLocator.ResumeClient(App).GetResumeNames();
				MyResumeList.Items = (from i in resumes select new Item { Id = i.Key, Name = i.Value, Type = "DELETE_RESUME" }).ToList();
				if (CssClass != null)
				{
					MyResumesContainer.Attributes.Remove("class");
					MyResumesContainer.Attributes.Add("class", CssClass);
				}
				if( MarginLeft != null )
				{
					MyResumesContainer.Style.Remove( "margin-left" );
					MyResumesContainer.Style.Add( "margin-left", MarginLeft );
				}
			}
		}

		/// <summary>
		/// Called when [link name click].
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="eventArgs">The <see cref="EventArgs" /> instance containing the event data.</param>
		protected virtual void OnLinkNameClicked( object sender, EventArgs eventArgs )
		{
			if( LinkNameClicked != null )
			{
				LinkNameClicked( sender, eventArgs );
			}
		}
	}
}