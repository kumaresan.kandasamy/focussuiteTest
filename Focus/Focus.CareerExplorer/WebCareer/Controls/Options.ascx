﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Options.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.Options" %>
<link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/Assets/Css/jquery.maxlength.css") %>"/> 
<script type="text/javascript" src="<%= ResolveUrl("~/Assets/Scripts/jquery.plugin.js") %>"></script> 
<script type="text/javascript" src="<%= ResolveUrl("~/Assets/Scripts/jquery.maxlength.js") %>"></script> 

<div class="resumeSection FocusCareer">
	<div class="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton" Text="Save &amp; Move to Next Step &#187;" ValidationGroup="AddIns" runat="server" class="buttonLevel2" OnClick="SaveMoveToNextStepButton_Click" />
	</div>
	<div class="resumeSectionContent">
		<p><focus:LocalisedLabel runat="server" ID="OptionsIntroductionLabel" RenderOuterSpan="False" LocalisationKey="OptionsIntroduction.Label" DefaultText="Add optional sections to your resume."/></p>
		<div class="defaultSubSection resumeOptionalSectionList">
		  <asp:CheckBoxList ID="SectionsCheckBoxList" runat="server" ClientIDMode="Static" CssClass="dataInputHorizontalCheckBoxListFloat" />
		</div>
		<div class="defaultSubSection">
			<asp:LinkButton ID="AddSectionButton" runat="server" CssClass="buttonLevel2 buttonLeft" ClientIDMode="Static" CommandName="Add" OnCommand="SectionRepeater_ItemCommand">
				<focus:LocalisedLabel runat="server" ID="AddSectionButtonLabel" RenderOuterSpan="False" LocalisationKey="AddSectionButton.Label" DefaultText="Add sections"/>
			</asp:LinkButton>
			<asp:CustomValidator ID="AddSectionValidator" runat="server" SetFocusOnError="true" Display="Dynamic" CssClass="error" ValidationGroup="AddIns" ClientValidationFunction="AnyClicked" ValidateEmptyText="true" />
		</div>
		<div>
			<asp:UpdatePanel ID="SectionUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<asp:Repeater ID="SectionRepeater" runat="server" OnItemDataBound="SectionRepeater_ItemDataBound" OnItemCommand="SectionRepeater_ItemCommand">
						<ItemTemplate>
							<div class="resumeOptionalSection">
								<div>
									<asp:HiddenField ID="SectionHiddenID" runat="server" ClientIDMode="Static" />
									<asp:Label ID="SectionLabel" runat="server" ClientIDMode="Static" CssClass="embolden" />&nbsp;&nbsp;-&nbsp;&nbsp;
									<asp:LinkButton ID="DeleteSectionButton" runat="server" CommandName="Delete" CommandArgument='<%# Eval("ResumeSection") %>'>
										<focus:LocalisedLabel runat="server" ID="DeleteSectionButtonLabel" RenderOuterSpan="False" LocalisationKey="DeleteSectionButton.Label" DefaultText="Delete section"/>
									</asp:LinkButton>
								</div>
								<div><asp:TextBox ID="SectionTextBox" runat="server" TextMode="MultiLine" Rows="10"/></div>
								<div><asp:RequiredFieldValidator ID="SectionRequired" runat="server" ControlToValidate="SectionTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="AddIns" /></div>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
	</div>
	<div class="resumeSectionAction">
		<asp:Button ID="SaveMoveToNextStepButton1" Text="Save &amp; Move to Next Step &#187;" ValidationGroup="AddIns" runat="server" class="buttonLevel2" OnClick="SaveMoveToNextStepButton_Click" />
	</div>
</div>
<script type="text/javascript">
	function AnyClicked(sender, args) {
		args.IsValid = true;
		$('#SectionsCheckBoxList > input').each(function () {
			if ($(this).is(':checked') && !($(this).is(':disabled')))
				args.IsValid = false;
		});
	}
</script>
