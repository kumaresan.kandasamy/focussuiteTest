﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DownloadResume.ascx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.Controls.DownloadResume" %>
<asp:HiddenField ID="DownloadResumeModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="DownloadResumeModal" runat="server" BehaviorID="DownloadResumeModal"
	TargetControlID="DownloadResumeModalDummyTarget" PopupControlID="DownloadResumeModalPanel"
	BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" ClientIDMode="Static">
	<Animations>
    <OnShown>
          <ScriptAction Script="EnableDisableDownloadResumeOrder();" />  
    </OnShown>
	</Animations>
</act:ModalPopupExtender>
<div id="DownloadResumeModalPanel" tabindex="-1" class="modal" style="z-index: 100001; display: none;">
	<div class="lightbox resumeDownloadModal">
		<div class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('DownloadResumeModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20" height="20" />
			<focus:LocalisedLabel runat="server" ID="ResumeDownloadTitleLabel" LocalisationKey="ResumeDownloadTitle.Label" DefaultText="Download your resume" CssClass="modalTitle modalTitleLarge"/>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="ResumeFormatLabel" AssociatedControlID="ddlDownloadResumeFormat" CssClass="dataInputLabel" LocalisationKey="ResumeFormat.Label" DefaultText="Resume style"/>
			<span class="dataInputField">
				<asp:DropDownList ID="ddlDownloadResumeFormat" runat="server" Width="220px" ClientIDMode="Static" onchange="EnableDisableDownloadResumeOrder();"></asp:DropDownList>
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="ResumeOrderLabel" AssociatedControlID="ddlDownloadResumeOrder" CssClass="dataInputLabel" LocalisationKey="ResumeOrder.Label" DefaultText="Resume order"/>
			<span class="dataInputField">
				<asp:DropDownList ID="ddlDownloadResumeOrder" runat="server" Width="220px" ClientIDMode="Static">
					<asp:ListItem Text="Emphasize my experience" Value="experience"></asp:ListItem>
					<asp:ListItem Text="Emphasize my education" Value="education"></asp:ListItem>
					<asp:ListItem Text="Emphasize my skills" Value="skills"></asp:ListItem>
				</asp:DropDownList>
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="FileFormatLabel" AssociatedControlID="rdoDownloadType" CssClass="dataInputLabel" LocalisationKey="FileFormat.Label" DefaultText="File format"/>
			<span class="dataInputField">
				<asp:RadioButtonList ID="rdoDownloadType" runat="server" ClientIDMode="Static" RepeatLayout="Flow" RepeatDirection="Vertical" CssClass="dataInputVerticalRadio">
					<asp:ListItem Selected="True" Text="RTF (Word), to edit" />
					<asp:ListItem Text="PDF, to send or publish" />
				</asp:RadioButtonList>
			</span>
		</div>
		<div class="dataInputRow">
			<focus:LocalisedLabel runat="server" ID="HideOptionsLabel" AssociatedControlID="rblHideOptions" CssClass="dataInputLabel" LocalisationKey="HideOptions.Label" DefaultText="Apply hide options"/>
			<span class="dataInputField">
				<asp:RadioButtonList ID="rblHideOptions" runat="server" ClientIDMode="Static" RepeatLayout="Flow" RepeatDirection="Horizontal" CssClass="dataInputHorizontalRadio">
					<asp:ListItem Selected="True" Text="Yes" />
					<asp:ListItem Text="No" />
				</asp:RadioButtonList>
			</span>
		</div>
		<asp:PlaceHolder runat="server" ID="HiddenContactPlaceHolder">
			<div>
				<focus:LocalisedLabel runat="server" ID="HiddenContactLabel"  CssClass="error" LocalisationKey="HiddenContact.Label" DefaultText="One or more pieces of your contact information are currently hidden. Employers who like your resume may have difficulty contacting you if you continue with the selected hide options applied."/>
			</div>
		</asp:PlaceHolder>
		<div class="modalButtons">
			<asp:Button ID="btnDownloadResume" Text="Download" runat="server" OnClick="btnDownloadResume_Click" class="buttonLevel2" OnClientClick="$find('DownloadResumeModal').hide();" />
		</div>
	</div>
</div>
<script type="text/javascript">

	Sys.Application.add_load(MyDownload_PageLoad);
	function MyDownload_PageLoad(sender, args) {
		// Jaws Compatibility
		var modal = $find('DownloadResumeModal');
		if (modal != null) {
			modal.add_shown(function () {
				$('.modal').attr('aria-hidden', false);
				$('.page').attr('aria-hidden', true);
				$('#ddlDownloadResumeFormat').focus();
			});

			modal.add_hiding(function () {
				$('.modal').attr('aria-hidden', true);
				$('.page').attr('aria-hidden', false);
			});
		}
	}
	function EnableDisableDownloadResumeOrder() {
		if ($('#ddlDownloadResumeFormat').val() !== '0') {
			$('#ddlDownloadResumeOrder').prop('disabled', false).next().removeClass('stateDisabled');
			$("*[name$='rdoDownloadType']").removeAttr("disabled");
			$("*[name$='rblHideOptions']").removeAttr("disabled");
		} else {
			$('#ddlDownloadResumeOrder').prop('disabled', true).next().addClass('stateDisabled');
			$("*[name$='rdoDownloadType']").prop('disabled', true);
			$("*[name$='rblHideOptions']").prop('disabled', true);
		}
	}
</script>
