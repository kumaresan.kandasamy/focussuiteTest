﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailDocumentModal.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.EmailDocument" %>

<asp:HiddenField ID="EmailDocumentDummyTarget" runat="server" />
<act:ModalPopupExtender ID="EmailDocumentModal" runat="server" BehaviorID="EmailDocumentModal"
				TargetControlID="EmailDocumentDummyTarget" PopupControlID="EmailDocumentPanel" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
			</act:ModalPopupExtender>
<asp:Panel ID="EmailDocumentPanel" runat="server" CssClass="modal" Width="440px" ClientIDMode="Static" Style="display:none;" >
	<div class="lightbox emailDocumentModal">
	  <div class="modalHeader"><input type="image" src="<%= UrlBuilder.Close() %>" onclick="$find('EmailDocumentModal').hide();return false;" class="modalCloseIcon" alt="<%= HtmlLocalise("Global.Close.Text", "Close") %>" width="20px" height="20px" /></div>
		<h2><focus:LocalisedLabel runat="server" ID="EmailDocumentTitleLabel" LocalisationKey="EmailDocumentTitleLabel.Label" DefaultText="Email document" CssClass="modalTitle modalTitleLarge" role="heading" aria-level="4"/></h2>
		<div class="emailDetailPanel career-scroll-pane">
			<div class="instructionalText"><focus:LocalisedLabel runat="server" ID="EmailInstructionLabel" RenderOuterSpan="False" LocalisationKey="EmailInstruction.Label" DefaultText="Separate email addresses with a semicolon and do not use a space."/></div>
			<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailDocumentToLabel" ClientIDMode="Static" AssociatedControlID="ToEmailAddressTextBox" CssClass="dataInputLabel" LocalisationKey="EmailDocumentTo.Label" DefaultText="To"/>
				<span class="dataInputField">
					<asp:TextBox ID="ToEmailAddressTextBox" runat="server" ClientIDMode="Static"></asp:TextBox>
          <div>
            <asp:CustomValidator ID="custToEmail" runat="server" ControlToValidate="ToEmailAddressTextBox" SetFocusOnError="True" Display="Dynamic" CssClass="error" ValidationGroup="EmailDocument" ClientValidationFunction="ToEmailAddressTextBox_ValidateToEmail" ValidateEmptyText="True" EnableClientScript="True"/>
          </div>
				</span>
			</div>
			<div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailDocumentSubjectLabel" AssociatedControlID="EmailSubjectTextBox" CssClass="dataInputLabel" LocalisationKey="EmailDocumentSubject.Label" DefaultText="Subject"/>
				<span class="dataInputField">
					<asp:TextBox ID="EmailSubjectTextBox" runat="server" ClientIDMode="Static"></asp:TextBox>
          <div>
            <asp:RequiredFieldValidator ID="EmailSubjectRequired" runat="server" ControlToValidate="EmailSubjectTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="EmailDocument" />
          </div>
				</span>
			</div>
      <div class="dataInputRow">
				<focus:LocalisedLabel runat="server" ID="EmailDocumentBodyLabel" AssociatedControlID="EmailBodyTextBox" CssClass="dataInputLabel" LocalisationKey="EmailDocumentBody.Label" DefaultText="Body"/>
				<span class="dataInputField">
					<asp:TextBox ID="EmailBodyTextBox" runat="server" ClientIDMode="Static" TextMode="MultiLine" Rows="10"></asp:TextBox>
          <div>
            <asp:RequiredFieldValidator ID="EmailBodyRequired" runat="server" ControlToValidate="EmailBodyTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="EmailDocument" />
          </div>
				</span>
			</div>
      <div class="dataInputRow">
        <focus:LocalisedLabel runat="server" ID="AttachmentLabel" AssociatedControlID="AttachmentNameLabel" CssClass="dataInputLabel" LocalisationKey="Attachment.Label" DefaultText="Attachment:"/>
        <span class="dataInputField">
          <focus:LocalisedLabel runat="server" ID="AttachmentNameLabel" LocalisationKey="Attachment.Label" DefaultText="Attachment" CssClass="emailDocumentName" />
        </span>
      </div>
		</div>
		<div class="modalButtons">
			<asp:Button ID="SendButton" runat="server" CssClass="buttonLevel2" Text="Send" ValidationGroup="EmailDocument" OnClick="SendButton_Click" CausesValidation="True" />
		</div>
	</div>
  
</asp:Panel>
<script type="text/javascript">

  function ToEmailAddressTextBox_ValidateToEmail(sender, args) {
    var reg = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
    args.IsValid = true;
    if (args.Value.length === 0) {
      sender.innerHTML = emailDocumentModalCodeValues.emailAddressRequired;
      args.IsValid = false;
    }
    else if (args.Value.length < 6) {
      sender.innerHTML = emailDocumentModalCodeValues.emailAddressCharLimit;
      args.IsValid = false;
    }
    else if (reg.test(args.Value) === false) {
      sender.innerHTML = emailDocumentModalCodeValues.emailAddressErrorMessage;
      args.IsValid = false;
    }

    
  }  

</script>