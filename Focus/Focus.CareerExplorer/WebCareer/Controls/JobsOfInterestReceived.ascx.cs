﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Services.Core.Extensions;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class JobsOfInterestReceived : UserControlBase
	{
		private bool IsBound
		{
			get { return GetViewStateValue("ReferralsList:IsBound", false); }
			set { SetViewStateValue("ReferralsList:IsBound", value); }
		}

	  public JobsOfInterestReceived()
	  {
			IsBound = false;
	  }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
    {
			if (!IsBound)
      {
				BindDropDowns();
	      if (App.User.IsAuthenticated)
	      {
		      LoadPostingsOfInterestReceived();
	      }
	      else
	      {
					JobsOfInterestRepeater.Visible = NoJobsOfInterestReceived.Visible = false;
	      }
				IsBound = true;
      }
    }

		/// <summary>
		/// Handles the OnSelectedIndexChanged event of the NumberOfReferralsDropDown control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void NumberOfDaysDropDown_OnSelectedIndexChanged(object sender, EventArgs e)
		{
			LoadPostingsOfInterestReceived();
		}


		/// <summary>
		/// Binds the drop-downs in the controls
		/// </summary>
		private void BindDropDowns()
		{
			NumberOfDaysDropDown.Items.AddLocalised("Last7Days.Option", "Last 7 days", "7");
			NumberOfDaysDropDown.Items.AddLocalised("Last14Days.Option", "Last 14 days", "14");
			NumberOfDaysDropDown.Items.AddLocalised("Last30Days.Option", "Last 30 days", "30");
			NumberOfDaysDropDown.Items.AddLocalised("AllJobs.Option", "All", "0");

			NumberOfDaysDropDown.SelectedIndex = 0;
		}

    /// <summary>
    /// Loads the postings of interest received.
    /// </summary>
    private void LoadPostingsOfInterestReceived()
    {
	    var days = NumberOfDaysDropDown.SelectedValueToInt();
			var postings = ServiceClientLocator.CandidateClient(App).GetPostingsOfInterestReceived(NumberOfDaysDropDown.SelectedValueToInt());
      if (postings.IsNotNullOrEmpty())
      {
	      JobsOfInterestRepeater.Visible = true;
				NoJobsOfInterestReceived.Visible = false;
				JobsOfInterestRepeater.DataSource = postings;
        JobsOfInterestRepeater.DataBind();
      }
      else
      {
        JobsOfInterestRepeater.Visible = false;
	      NoJobsOfInterestReceived.Visible = true;
				NoJobsOfInterestReceived.Text = days == 0
					? CodeLocalise("NoJobsOfInterestReceived.Text", "There are no jobs for which you've been encouraged to apply")
					: CodeLocalise("NoJobsOfInterestReceivedInLast.Text", "There are no jobs for which you've been encouraged to apply in the last {0} days", days);
      }
    }

    /// <summary>
    /// Handles the ItemDataBound event of the JobsOfInterestRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void JobsOfInterestRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        return;

			var postingOfInterest = (JobPostingOfInterestSentViewDto)e.Item.DataItem;

			BindScore(e.Item, postingOfInterest.Score.GetValueOrDefault());

      var jobTitleButton = (LinkButton)e.Item.FindControl("linkJobTitle");
      jobTitleButton.Text = postingOfInterest.JobTitle;

      jobTitleButton.CommandArgument = postingOfInterest.LensPostingId;

			var employerLabel = (Literal)e.Item.FindControl("EmployerNameLabel");
	    employerLabel.Text = postingOfInterest.EmployerName;

	    if (postingOfInterest.JobLocation.IsNotNullOrEmpty())
	    {
				var jobLocationLabel = (Literal)e.Item.FindControl("LocationLabel");
		    jobLocationLabel.Text = postingOfInterest.JobLocation;
	    }
			var viewedTime = (Literal)e.Item.FindControl("InviteCreatedOnLabel");
			viewedTime.Text = postingOfInterest.SentOn.ToString("MMM dd");
		}

    /// <summary>
    /// Handles the Click event of the LinkJobTitle control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void LinkJobTitle_Click(object sender, EventArgs e)
    {
      var btnTitle = (LinkButton)sender;
      Response.RedirectToRoute("JobPosting", new { jobid = btnTitle.CommandArgument, fromURL = "Home", pageSection = "RecentlyViewedPostings" });
    }

		/// <summary>
		/// Binds the score.
		/// </summary>
		/// <param name="listItem">The list item holding the details</param>
		/// <param name="score">The score.</param>
		private static void BindScore(Control listItem, int score)
		{
			var starRating = score.ToStarRating(OldApp_RefactorIfFound.Settings.StarRatingMinimumScores);

			for (var imageNumber = 1; imageNumber <= 5; imageNumber++)
			{
				var scoreImage = (Image)listItem.FindControl(string.Concat("MatchScoreImage", imageNumber));
				scoreImage.ImageUrl = imageNumber <= starRating ? UrlBuilder.StarOn() : UrlBuilder.StarOff();
			}
		}
  }
}