﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobSeekerSurveyModal.ascx.cs"
    Inherits="Focus.CareerExplorer.WebCareer.Controls.JobSeekerSurveyModal" %>
<%@ Import Namespace="Focus.Core.DataTransferObjects.FocusCore" %>
<asp:HiddenField ID="JobSeekerSurveyDummyTarget" runat="server" />
<act:ModalPopupExtender ID="JobSeekerSurvey" runat="server" BehaviorID="JobSeekerSurvey"
    TargetControlID="JobSeekerSurveyDummyTarget" PopupControlID="JobSeekerSurveyPanel"
    BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<asp:Panel ID="JobSeekerSurveyPanel" TabIndex="-1" runat="server" CssClass="modal"
    Style="display: none; overflow-y: auto">
    <table width="100%" style="max-width:500px">
        <tr>
            <th valign="top">
                <%= HtmlLocalise("Title.Text", "How is your job search going?")%>
            </th>
        </tr>
    </table>
    <table width="100%" style="max-width:500px" class="FocusCareer" role="presentation" >
        <tr id="SurveyRow" runat="server">
            <td valign="top" height="300px">
                <br />
                <table role="presentation">
                    <tr>
                        <td valign="top">
                            1.
                        </td>
                        <td style="padding-bottom: 15px;">
                            <asp:Literal ID="Question1Label" runat="server" /><br />
                            <asp:DropDownList ID="Question1AnswerDropDownList" runat="server" ClientIDMode="Static"
                                Width="200px" Title="Question 1"/>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            2.
                        </td>
                        <td style="padding-bottom: 15px;">
                            <asp:Literal ID="Question2Label" runat="server" /><br />
                            <fieldset>
                                <legend class="sr-only">xx</legend>
                                <asp:RadioButtonList ID="Question2AnswerRadioButtonList" runat="server" RepeatDirection="Horizontal" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            3.
                        </td>
                        <td style="padding-bottom: 15px;">
                            <asp:Literal ID="Question3Label" runat="server" /><br />
                            <fieldset>
                                <legend class="sr-only">xx</legend>
                                <asp:RadioButtonList ID="Question3AnswerRadioButtonList" runat="server" RepeatDirection="Horizontal" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr id="ReferralsRow" runat="server">
                        <td valign="top">
                            4.
                        </td>
                        <td style="padding-bottom: 15px;">
                            <%= HtmlLocalise("ApplicantsUpdate.Text", "Please update referral outcomes for the jobs below:")%>
                            <br />
                            <br />
                            <asp:Panel ID="ReferralsPanel" runat="server" CssClass="scrollableArea">
                                <asp:ListView ID="ReferralsListView" runat="server" DataSourceID="ReferralsDataSource"
                                    OnDataBound="ReferralsListView_OnDataBound" OnItemDataBound="ReferralsRepeater_ItemDataBound"
                                    ItemPlaceholderID="ReferralsListPlaceHolder" DataKeyNames="Id">
                                    <LayoutTemplate>
                                        <table role="presentation">
                                            <asp:PlaceHolder ID="ReferralsListPlaceHolder" runat="server" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <%# ((NewApplicantReferralViewDto)Container.DataItem).JobTitle%>
                                            </td>
                                            <td>
                                                <asp:Literal ID="EmployerNameLiteral" runat="server" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ReferralOutcomeDropDown" runat="server" ClientIDMode="Predictable"
                                                    Width="240px" Title="Referral Outcome"/>
                                            </td>
                                        </tr>
                                        <%--<tr><td><asp:RequiredFieldValidator runat="server" ID="ReferralOutcomeRequired" CssClass="error" Display="Dynamic" SetFocusOnError="True" ControlToValidate="ReferralOutcomeDropDown" ClientIDMode="Predictable" ValidationGroup="JobSeekerSurveyModal"></asp:RequiredFieldValidator></td></tr>--%>
                                        <asp:HiddenField runat="server" ID="ApplicationIdHiddenField" Value="<%# ((NewApplicantReferralViewDto)Container.DataItem).ApplicationId %>" />
                                        <asp:HiddenField runat="server" ID="ApplicationStatusHiddenField" Value="<%# ((NewApplicantReferralViewDto)Container.DataItem).CandidateApplicationStatus %>" />
                                    </ItemTemplate>
                                </asp:ListView>
                                <asp:ObjectDataSource ID="ReferralsDataSource" runat="server" TypeName="Focus.CareerExplorer.WebCareer.Controls.JobSeekerSurveyModal"
                                    SelectMethod="GetReferrals"></asp:ObjectDataSource>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Button ID="CloseButton" runat="server" CssClass="buttonLevel3" CausesValidation="false"
                    OnClick="CloseButton_OnClick" Style="padding-bottom: 1px" />
                <asp:Button ID="SubmitButton" runat="server" class="buttonLevel2" CausesValidation="true"
                    ValidationGroup="JobSeekerSurveyModal" OnClick="SubmitButton_Click" />
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        Sys.Application.add_load(JobSeekerSurveyModal_PageLoad);

        function JobSeekerSurveyModal_PageLoad(sender, args) {
            var height = $(window).height();
            var modal = $("#<%=JobSeekerSurveyPanel.ClientID %>");
            if (parseInt(modal.css("height"), 10) > height - 50) {
                modal.css("height", (height - 50) + "px");
            }
        }
    </script>
</asp:Panel>
