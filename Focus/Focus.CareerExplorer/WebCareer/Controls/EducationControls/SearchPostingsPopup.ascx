﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchPostingsPopup.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.EducationControls.SearchPostingsPopup" %>
<asp:HiddenField ID="SearchPostingsModalDummyTarget" runat="server" />
<act:ModalPopupExtender ID="SearchPostingsModal" runat="server" BehaviorID="SearchPostingsModal"
	TargetControlID="SearchPostingsModalDummyTarget" PopupControlID="SearchPostingsModalPanel"
	BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" />
<div id="SearchPostingsModalPanel" tabindex="-1" class="modal" style="z-index: 100001; display: none;">
	<div class="lightbox lightboxmodal FocusCareer">
		<div class="modalHeader">
			<input type="image" src="<%= UrlBuilder.Close() %>" class="modalCloseIcon" onclick="javascript:$find('SearchPostingsModal').hide();return false;" alt="<%= HtmlLocalise("Global.Close.ToolTip", "Close") %>" width="20" height="20" />
		</div>
		<div class="modalMessage">
			<focus:LocalisedLabel runat="server" ID="lblSearchPostings" LocalisationKey="SearchPostings.Label" DefaultText="Search postings" CssClass="modalTitle modalTitleLarge"/>
		</div>
		<div class="landingPageNavigation searchPostingsModal">
			<div class="lpItem">
				<div class="lpItemHeaderBlue">
					<focus:LocalisedLabel runat="server" ID="lblRecommendationsBasedOn" LocalisationKey="RecommendationsBasedOn.Label" DefaultText="See job recommendations based on" CssClass="lpItemHeaderText"/>
				</div>
				<div class="lpItemContentPanelBlue">
					<div class="lpItemContentLinkRowBlue">
						<asp:linkbutton ID="lnkProgramOfStudy" runat="server" cssclass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="ProgramOfStudy">My program of study</asp:linkbutton>
					</div>
				</div>
				<div class="lpItemContentPanelBlue" id="MyResumeDiv" runat="server">
					<div class="lpItemContentLinkRowBlue">
						<asp:linkbutton ID="Linkbutton2" runat="server" cssclass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="Resume">My resume</asp:linkbutton>
					</div>
				</div>
				<div class="lpItemContentPanelBlue" id="MyResumeAndProgramOfStudyDiv" runat="server">
					<div class="lpItemContentLinkRowBlue">
						<asp:linkbutton ID="Linkbutton4" runat="server" cssclass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="ResumeAndProgramOfStudy">My resume and my program of study</asp:linkbutton>
					</div>
				</div>
			</div>
			<div class="lpItem">
				<div class="lpItemHeaderBlue">
					<asp:linkbutton ID="lnkSearchJobs" runat="server" cssclass="lpItemContentLinkText lpItemHeaderText" OnClick="lnkSearchJobs_Click"><focus:LocalisedLabel runat="server" ID="SearchJobsLabel" LocalisationKey="SearchJobs.Label" DefaultText="Search for jobs" RenderOuterSpan="False"/></asp:linkbutton>
				</div>
			</div>
			<div class="lpItem">
				<div class="lpItemHeaderBlue">
					<asp:linkbutton ID="lnkSearchInternships" runat="server" cssclass="lpItemContentLinkText lpItemHeaderText" OnClick="lnkSearchInternships_Click"><focus:LocalisedLabel runat="server" ID="SearchInternshipsLabel" LocalisationKey="SearchInternships.Label" DefaultText="Search for internship opportunities" RenderOuterSpan="False"/></asp:linkbutton>
				</div>
			</div>
		</div>
	</div>
</div>

