﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternshipSearchSkillClusters.ascx.cs" Inherits="Focus.CareerExplorer.WebCareer.Controls.EducationControls.InternshipSearchSkillClusters" %>
<span id="scrollableSkillClustersWrapper" runat="server">
	<p class="instructionalText">Please choose from the following:</p>
	<span id="scrollableSkillClusters" runat="server" style="overflow-y:scroll;">
		<asp:Repeater runat="server" ID="SkillClusterRepeater" OnItemDataBound="SkillClusterRepeater_ItemDataBound">
			<HeaderTemplate><ul></HeaderTemplate>
			<ItemTemplate>
				<li>
					<asp:CheckBoxList runat="server" ID="SkillClusterCheckBoxList"></asp:CheckBoxList>
				</li>
			</ItemTemplate>
			<FooterTemplate></ul></FooterTemplate>
		</asp:Repeater>
	</span>
	<span id="scrollableSkillClustersSummary" runat="server">
		<p class="instructionalText embolden">Your choices:</p>
		<table class="deletableListItemTable" id="selectedSkillClusters">
		</table>
    <asp:HiddenField runat="server" ID="SkillClustersIdsHiddenField" />
    <asp:HiddenField runat="server" ID="SkillClustersNamesHiddenField" />
		<%--<asp:TextBox runat="server" ID="SkillClustersIdsHiddenField" style="display:none;" />
    <asp:TextBox runat="server" ID="SkillClustersNamesHiddenField" style="display:none;" />--%>
	</span>
</span>

<script type="text/javascript">
	var <%=scrollableSkillClusters.ClientID %>_Details = new InternshipSkillClusters(
    '<%=scrollableSkillClusters.ClientID %>',
    '<%=scrollableSkillClustersSummary.ClientID %>',
    '<%=SkillClustersIdsHiddenField.ClientID %>',
    '<%=SkillClustersNamesHiddenField.ClientID %>',
		'<%=UrlBuilder.CloseMedium() %>'
  );
	
	function <%=scrollableSkillClusters.ClientID %>_Load() {
    <%=scrollableSkillClusters.ClientID %>_Details.SetUpCheckBoxes();
  }

  $(document).ready(function () {
    <%=scrollableSkillClusters.ClientID %>_Load();
    // Ensure accordion still works when used in conjunction with update panel
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(<%=scrollableSkillClusters.ClientID %>_Load);
  });
</script>