﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.EducationControls
{
	public partial class SearchPostingsPopup : UserControlBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
		}

    /// <summary>
    /// Shows the popup.
    /// </summary>
		public void ShowPopup()
    {
	    InitialiseUi();
			SearchPostingsModal.Show();
		}

    /// <summary>
    /// Handles the Click event of the lnkSearchJobs control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lnkSearchJobs_Click(object sender, EventArgs e)
		{
			App.SetCookieValue("Career:PostingSearchInternshipFilter", FilterTypes.Exclude.ToString());
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
			
		}

    /// <summary>
    /// Handles the Click event of the lnkSearchInternships control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lnkSearchInternships_Click(object sender, EventArgs e)
		{
      App.SetCookieValue("Career:PostingSearchInternshipFilter", FilterTypes.ShowOnly.ToString());
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
		}

		/// <summary>
		/// Handles the Command event of the MatchingAction control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void MatchingAction_Command(object sender, CommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Resume":
					OnJobRecommendationsClick(new JobRecommendationsClickEventArgs(MatchingType.Resume));
					SearchPostingsModal.Hide();
					break;
				case "ProgramOfStudy":
					if (App.User.ProgramAreaId == 0)
					{
						MasterPage.ShowModalAlert(AlertTypes.Info,
							CodeLocalise("ProgramOfStudy.Error", "You do not have a program of study stored.\r\nClick on My Account and choose a program of study in order to see job recommendations."));
					}
					else
					{
						OnJobRecommendationsClick(new JobRecommendationsClickEventArgs(MatchingType.ProgramOfStudy));
						SearchPostingsModal.Hide();
					}
					break;
				case "ResumeAndProgramOfStudy":
					OnJobRecommendationsClick(new JobRecommendationsClickEventArgs(MatchingType.ResumeAndProgramOfStudy));
					SearchPostingsModal.Hide();
					break;
			}
		}

		public event JobRecommendationsClickHandler JobRecommendationsClick;

		/// <summary>
		/// Raises the <see cref="E:JobRecommendationsClick" /> event.
		/// </summary>
		/// <param name="e">The <see cref="JobRecommendationsClickEventArgs"/> instance containing the event data.</param>
		protected virtual void OnJobRecommendationsClick(JobRecommendationsClickEventArgs e)
		{
			if (JobRecommendationsClick != null)
				JobRecommendationsClick(this, e);
		}

		/// <summary>
		/// Initialises the UI.
		/// </summary>
		private void InitialiseUi()
		{
			MyResumeDiv.Visible = MyResumeAndProgramOfStudyDiv.Visible = !App.Settings.CareerHideSearchJobsByResume;
		}

		#region Delegates

		public delegate void JobRecommendationsClickHandler(object o, JobRecommendationsClickEventArgs e);

		#endregion

		#region EventArgs

		public class JobRecommendationsClickEventArgs : EventArgs
		{
			public readonly MatchingType MatchingType;

			public JobRecommendationsClickEventArgs(MatchingType matchingType)
			{
				MatchingType = matchingType;
			}
		}

		#endregion

	}
}