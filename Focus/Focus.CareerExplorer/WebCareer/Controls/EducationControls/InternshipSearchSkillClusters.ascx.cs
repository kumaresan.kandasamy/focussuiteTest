﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Framework.Core;

using Focus.Common.Helpers;
using Focus.Common;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls.EducationControls
{
	public partial class InternshipSearchSkillClusters : UserControlBase
	{
		/// <summary>
		/// Gets or sets the CSS class to apply to the outer wrapper of the control.
		/// </summary>
		[CssClassProperty]
		public string WrapperCssClass { get; set; }

		/// <summary>
		/// Gets or sets the CSS class to apply to the scrollable skill clusters area.
		/// </summary>
		[CssClassProperty]
		public string SkillClustersCssClass { get; set; }

		/// <summary>
		/// Gets or sets the CSS class to apply to the summary list box wrapper.
		/// </summary>
		[CssClassProperty]
		public string SummaryCssClass { get; set; }

		/// <summary>
		/// Gets or sets the data source for the control.
		/// </summary>
		public List<InternshipCategoryDto> InternshipCategories { get; set; }

		private List<long> _skillClustersIds;
		private List<string> _skillClustersNames;

		public List<long> SelectedItems
		{
			get
			{
				if (_skillClustersIds == null)
				{
					var skills = SkillClustersIdsHiddenField.Value.Split(',');
					_skillClustersIds = skills.Where(id => id.Length > 0).Select(id => Convert.ToInt64(id)).ToList();
				}
				return _skillClustersIds;
			}
			set
			{
				_skillClustersIds = value;
				SkillClustersIdsHiddenField.Value = string.Join(",", _skillClustersIds.Select(s => s.ToString(CultureInfo.InvariantCulture)));
			}
		}

		public List<string> SelectedNames
		{
			get
			{
				return _skillClustersNames ?? (_skillClustersNames = SkillClustersNamesHiddenField.Value.Split(',').Where(id => id.Length > 0).ToList());
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}

		/// <summary>
		/// Handles the PreRender event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (WrapperCssClass.IsNotNullOrEmpty())
				scrollableSkillClustersWrapper.Attributes.Add("class", WrapperCssClass);

			if (SkillClustersCssClass.IsNotNullOrEmpty())
				scrollableSkillClusters.Attributes.Add("class", SkillClustersCssClass);

			if (SummaryCssClass.IsNotNullOrEmpty())
				scrollableSkillClustersSummary.Attributes.Add("class", SummaryCssClass);

			if (!IsPostBack)
				BindControl();

			ScriptManager.RegisterClientScriptInclude(this, GetType(), "internshipSearchSkillClustersScriptInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.InternshipSearchSkillClusters.js"));
		}

		/// <summary>
		/// Binds the control.
		/// </summary>
		public void BindControl()
		{
			if (InternshipCategories.IsNullOrEmpty())
			{
				InternshipCategories = ServiceClientLocator.ExplorerClient(App).GetInternshipCategories();
			}

			if (InternshipCategories.IsNullOrEmpty()) return;
			SkillClusterRepeater.DataSource = InternshipCategories;
			SkillClusterRepeater.DataBind();
		}

		protected void SkillClusterRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;
			var skillCluster = e.Item.DataItem as InternshipCategoryDto;
			var checkBoxList = e.Item.FindControl("SkillClusterCheckBoxList") as CheckBoxList;
			if (checkBoxList.IsNull()) return;
			checkBoxList.Items.Clear();

			var listItem = new ListItem
			{
				Text = skillCluster.Name,
				Value = skillCluster.Id.ToString(),
				Selected = SelectedItems.Contains(skillCluster.Id.GetValueOrDefault(0))
			};
			checkBoxList.Items.Add(listItem);
		}
	}
}