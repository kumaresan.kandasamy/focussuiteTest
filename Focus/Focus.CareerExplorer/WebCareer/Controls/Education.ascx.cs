﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.CareerExplorer.Controls;
using Focus.Common;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Framework.Core;
using Framework.DataAccess;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class Education : UserControlBase
  {
    #region Page Properties

    protected string EducationLevelRequired = "";
    protected string EnrollmentStatusErrorMessage = "";
    protected string EducationLevelErrorMessage = "";
    protected string DegreeNameRequired = "";
    protected string MajorSubjectRequired = "";
    protected string SchoolUniversityRequired = "";
    protected string CompletionDateRequired = "";
	  protected string GPAOutofRange = "";
	  protected string MaxGPA = "";
  	protected string ExpectedYearLimitsErrorMessage = "";
    protected string FutureDateErrorMessage = "";
  	protected string PastDateErrorMessage = "";
    protected string StateRequired = "";
    protected string CountryRequired = "";
    protected string ProfessionalLicenseRequired = "";
    protected string IssuingOrganizationRequired = "";
		protected string CompletedDateRequired = "";
  	protected string ExpectedCompletionDateRequired = "";
    protected string DateErrorMessage = "";
		protected string LicenseCompletedDateRequired = "";
		protected string LicenseCompletedDateTooLate = "";
		protected string LicenseCompletedYearLimitsErrorMessage = "";
		protected string LicenseCompletedTooYoung = "";
		protected string LicenseExpectedDateRequired = "";
		protected string LicenseExpectedDateTooEarly = "";
	  protected string LicenseExpectedYearLimitsErrorMessage = "";
		protected string TooYoungToCompleteMessage = "";
    protected string TooEarlyToCompleteMessage = "";
    protected string SelectOneEducationDateMessage = "";
	  protected string NCRCLevelRequiredErrorMessage = "";
		protected string NCRCStateRequiredErrorMessage = "";
		protected string NCRCIssueDateRequiredErrorMessage = "";
	  protected string NCRCIssueDateInvalidErrorMessage = "";
		protected string NCRCIssueDateInFutureErrorMessage = "";
		protected string NCRCIssueDateInPastErrorMessage = "";
		protected long? UserStateCode;
    protected long? DefaultDrivingLicense;
	  protected long? MotorcycleEndorsementCode;
		protected long? SchoolBusEndorsementCode;
	  protected string LicenceEndorsementsJavascriptArray;

    #endregion
    
		// TODO: This page needs tidying up completely.  The page needs splitting out into controls.
    
    private List<DegreeInfo> _degreeInfo = new List<DegreeInfo>();
    private List<LicenseInfo> _licenseInfo = new List<LicenseInfo>();
		private List<LanguageProficiency> _languageInfo = new List<LanguageProficiency>();
    private List<long> _endorsement = new List<long>();
    private List<string> _skill = new List<string>();
    private List<string> _hiddenSkills = new List<string>();
    private List<string> _industry = new List<string>();
    private List<string> _targetindustry = new List<string>();

		private List<String> CurrentLanguages
		{
			get { return GetViewStateValue<List<String>>("Education:CurrentLanguages"); }
			set { SetViewStateValue("Education:CurrentLanguages", value); }
		}

    protected void Page_Load(object sender, EventArgs e)
    {
      LocaliseUI();      
      NCRCPanel.Visible = App.Settings.EnableNCRCFeatureGroup;
			if (!IsPostBack)
				LicencePlaceHolder.Visible = !App.Settings.HideDrivingLicence;            
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
      ApplyTheme();
    
      var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");

      DateTime dateOfBirth;
      bool hasDateOfBirth;

      if (App.Settings.Theme == FocusThemes.Education)
      {
        hasDateOfBirth = false;
        dateOfBirth = DateTime.Now.AddYears(-100);
      }
      else
      {
        if (currentResume.IsNotNull() && currentResume.ResumeContent.IsNotNull() && currentResume.ResumeContent.Profile.IsNotNull() && currentResume.ResumeContent.Profile.DOB.IsNotNull())
        {
          hasDateOfBirth = true;
          dateOfBirth = currentResume.ResumeContent.Profile.DOB.Value;
        }
        else
        {
          var user = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);

          if (user.PersonDetails.DateOfBirth.HasValue)
          {
            hasDateOfBirth = true;
            dateOfBirth = (DateTime) user.PersonDetails.DateOfBirth;
          }
          else
          {
            hasDateOfBirth = false;
            dateOfBirth = DateTime.Now.AddYears(-100);
          }
        }
      }

      IndustryPlaceHolder.Visible = App.Settings.ShowExpIndustries;
      TargetIndPlaceHolder.Visible = App.Settings.ShowTargetIndustries;

			LicenceEndorsementsJavascriptArray = GetValidLicenceEndorsementsJavascriptArray();
      
      var script = string.Format("var Education_hasDOB = {0}; var Education_DateOfBirth = new Date({1}, {2}, {3});",
                                   hasDateOfBirth.ToString().ToLower(), 
                                   dateOfBirth.Year, 
                                   dateOfBirth.Month - 1,
                                   dateOfBirth.Day);
      Page.ClientScript.RegisterClientScriptBlock(GetType(), "DateOfBirth", script, true);
      System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallLicenceRepeaterItems_1", "LicenceRepeaterItems();", true);
      System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallDegreeRepeaterItems_1", "DegreeRepeaterItems();", true);
      System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallMaskNCRCIsuueDate_1", "MaskNCRCIsuueDate();", true);
    }

    public void UpdateResumeInSession(long? resumeId)
    {
      var resume = ServiceClientLocator.ResumeClient(App).GetResume(resumeId);
      App.SetSessionValue("Career:Resume", resume);
    }

    /// <summary>
    /// Handles the Click event of the SaveMoveToNextStepButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveMoveToNextStepButton_Click(object sender, EventArgs e)
    {
			UnbindControls();

      if (ddlEducationLevel.SelectedIndex != 0 && ddlEnrollmentStatus.SelectedIndex != 0 && _degreeInfo.Count == 0)
      {
        var selectedEnrollmentStatus = ddlEnrollmentStatus.SelectedValueToEnum<SchoolStatus>();
        //Show popup fof following conditions
        //Condition 1: If Enrollment status is "In School" & Degree is not provided.
        //Condition 2: If Enrollment ststus is "Not Attending School HS Graduate" & Education level is "Bachelors or equivalent" & Degree is not provided.
        var message = "";

        if (selectedEnrollmentStatus.IsIn(SchoolStatus.In_School_Alternative_School, SchoolStatus.In_School_HS_OR_less, SchoolStatus.In_School_Post_HS))
        {
            message = CodeLocalise("InSchool.Message", "You've indicated you're currently in school. Selecting an education level from the drop-down will not place this information on your resume or match your educational achievements to available jobs. You must enter this information to the Degrees and Diploma panel.");
        }
        else
        {
          if (selectedEnrollmentStatus == SchoolStatus.Not_Attending_School_HS_Graduate)
          {
            var selectedEducationLevel = ddlEducationLevel.SelectedValueToEnum<EducationLevel>();
            var messageLevel = string.Empty;
            switch (selectedEducationLevel)
            {
              case EducationLevel.Grade_12_HS_Graduate_13:
                messageLevel = CodeLocalise("HighSchoolDiploma.ExtraText", "high school diploma");
                break;
              case EducationLevel.HS_1_Year_College_OR_VOC_Tech_No_Degree_15:
                messageLevel = CodeLocalise("HS_1_Year_College_OR_VOC_Tech_No_Degree_15.ExtraText", "high school diploma + 1 yr college or voc/tech – no degree");
                break;
              case EducationLevel.HS_2_Year_College_OR_VOC_Tech_No_Degree_16:
                messageLevel = CodeLocalise("HS_2_Year_College_OR_VOC_Tech_No_Degree_16.ExtraText", "high school diploma + 2 yr college or voc/tech – no degree");
                break;
              case EducationLevel.HS_3_Year_College_OR_VOC_Tech_No_Degree_17:
                messageLevel = CodeLocalise("HS_3_Year_College_OR_VOC_Tech_No_Degree_17.ExtraText", "high school diploma + 3 yr college or voc/tech – no degree");
                break;
              case EducationLevel.HS_1_Year_Vocational_Degree_18:
                messageLevel = CodeLocalise("HS_1_Year_Vocational_Degree_18.ExtraText", "high school diploma + 1 yr vocational degree");
                break;
              case EducationLevel.HS_2_Year_Vocational_Degree_19:
                messageLevel = CodeLocalise("HS_2_Year_Vocational_Degree_19.ExtraText", "high school diploma + 2 yr vocational degree");
                break;
              case EducationLevel.HS_3_Year_Vocational_Degree_20:
                messageLevel = CodeLocalise("HS_3_Year_Vocational_Degree_20.ExtraText", "high school diploma + 3 yr vocational degree");
                break;
              case EducationLevel.HS_1_Year_Associates_Degree_21:
                messageLevel = CodeLocalise("HS_1_Year_Associates_Degree_21.ExtraText", "high school diploma + 1 yr Associates degree");
                break;
              case EducationLevel.HS_2_Year_Associates_Degree_22:
                messageLevel = CodeLocalise("HS_2_Year_Associates_Degree_22.ExtraText", "high school diploma + 2 yr Associates degree");
                break;
              case EducationLevel.HS_3_Year_Associates_Degree_23:
                messageLevel = CodeLocalise("HS_3_Year_Associates_Degree_23.ExtraText", "high school diploma + 3 yr Associates degree");
                break;
              case EducationLevel.GED_88:
              case EducationLevel.Bachelors_OR_Equivalent_24:
              case EducationLevel.Masters_Degree_25:
              case EducationLevel.Doctorate_Degree_26:
                messageLevel = CodeLocalise(selectedEducationLevel, true).ToLower();
                break;
            }
            if (messageLevel.Length > 0)
                message = CodeLocalise("HasDegree.Message", "You've indicated that you have a {0}. Selecting an education level from the drop-down will not place this information on your resume or match your educational achievements to available jobs. You must enter this information to the Degrees and Diploma panel.", messageLevel);
          }
        }

        if (message.IsNotNullOrEmpty())
        {
          lblDegreeValidationInfo.Text = message;
          DegreeValidationModalPopup.Show();
        }
        else if (SaveModel())
          Response.RedirectToRoute("ResumeWizardTab", new { Tab = "Summary" });
      }
      else if (SaveModel())
        Response.RedirectToRoute("ResumeWizardTab", new { Tab = "Summary" });
    }

    /// <summary>
    /// Handles the Click event of the btnNoThanks control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnNoThanks_Click(object sender, EventArgs e)
    {
      try
      {
				UnbindControls();

        if (SaveModel())
          Response.RedirectToRoute("ResumeWizardTab", new { Tab = "Summary" });
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    #region Bind education section

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
      BindDropdowns();
      BindCheckBoxLists();

	    MotorcycleEndorsementCode =
		    ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements).Where(
			    x => x.Key == "DrivingLicenceEndorsements.Motorcycle").Select(x => x.Id).SingleOrDefault();

			SchoolBusEndorsementCode =
				ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements).Where(
					x => x.Key == "DrivingLicenceEndorsements.SchoolBus").Select(x => x.Id).SingleOrDefault();

      var resumeId = App.GetSessionValue<long?>("Career:ResumeID");
      UpdateResumeInSession(resumeId);

      var model = App.GetSessionValue<ResumeModel>("Career:Resume");
   
      if (model.IsNotNull() && model.ResumeContent.IsNotNull()) // && Model.ResumeMetaInfo.ResumeId.IsNotNullOrEmpty())
      {
        var education = model.ResumeContent.EducationInfo;
        var skill = model.ResumeContent.Skills;
        var profile = model.ResumeContent.Profile;

        if (education.IsNotNull())
        {
					if (App.Settings.Theme == FocusThemes.Education)
					{
						if (App.User.EnrollmentStatus.HasValue)
							ddlEnrollmentStatus.SelectedValue = App.User.EnrollmentStatus.ToString();
					}
					else
					{
						if (education.SchoolStatus.HasValue)
							ddlEnrollmentStatus.SelectValue(education.SchoolStatus.ToString());
					}

        	if (education.EducationLevel.IsNotNull())
            ddlEducationLevel.SelectedValue = education.EducationLevel.ToString();

          if (education.Schools.IsNotNull())
            _degreeInfo = (from i in education.Schools
                           select new DegreeInfo
                           {
                             DegreeID = i.SchoolId,
                             Year = i.CompletionDate.HasValue ? ((DateTime)i.CompletionDate).ToString("yyyy") : "",
                             Month = i.CompletionDate.HasValue ? ((DateTime)i.CompletionDate).ToString("MM") : "",
														 ExpectedYear = i.ExpectedCompletionDate.HasValue ? ((DateTime)i.ExpectedCompletionDate).ToString("yyyy") : "",
														 ExpectedMonth = i.ExpectedCompletionDate.HasValue ? ((DateTime)i.ExpectedCompletionDate).ToString("MM") : "",
                             DegreeName = i.Degree,
                             University = i.Institution,
                             Major = i.Major,
                             Course = i.Courses,
                             Honors = i.Honors,
                             GPA = i.GradePointAverage.SetGradePointAverage(),
                             Activities = i.Activities,
                             City = i.SchoolAddress.City,
                             StateId = i.SchoolAddress.StateId,
                             CountryId = i.SchoolAddress.CountryId
                           }).ToList();
        }

        if (profile.IsNotNull())
        {
          var license = profile.License;
          if (license.IsNotNull())
          {
            if (license.HasDriverLicense.IsNotNull() && license.HasDriverLicense.HasValue)
              ddlDriverLicense.SelectedValue = license.HasDriverLicense.AsYesNo();

            if (license.DriverStateId.HasValue)
              ddlState.SelectValue(license.DriverStateId.ToString());

            if (license.DrivingLicenceClassId.HasValue)
              ddlLicenseType.SelectValue(license.DrivingLicenceClassId.ToString());
            else
              ddlLicenseType.SelectedIndex = 0;

            _endorsement = license.DrivingLicenceEndorsementIds;

            var hiddenFieldInfo = model.Special.HideInfo;
            chkUnHideClassD.Checked = hiddenFieldInfo.IsNotNull() && hiddenFieldInfo.UnHideDriverLicense;
          }
        }

        if (skill.IsNotNull())
        {
          if (skill.Certifications.IsNotNull())
            _licenseInfo = (from i in skill.Certifications
                            select new LicenseInfo
                            {
                              LicenseName = i.Certificate,
                              City = i.CertificationAddress.City,
                              StateId = i.CertificationAddress.StateId,
                              StateName = i.CertificationAddress.StateName,
                              CountryId = i.CertificationAddress.CountryId,
                              CountryName = i.CertificationAddress.CountryName,
                              Year = i.CompletionDate.HasValue ? ((DateTime)i.CompletionDate).ToString("yyyy") : "",
                              Month = i.CompletionDate.HasValue ? ((DateTime)i.CompletionDate).ToString("MM") : "",
															ExpectedYear = i.ExpectedCompletionDate.HasValue ? ((DateTime)i.ExpectedCompletionDate).ToString("yyyy") : "",
															ExpectedMonth = i.ExpectedCompletionDate.HasValue ? ((DateTime)i.ExpectedCompletionDate).ToString("MM") : "",
                              Organization = i.OrganizationName
                            }).ToList();

	        if (skill.LanguageProficiencies.IsNotNullOrEmpty())
	        {
						_languageInfo = (from i in skill.LanguageProficiencies
														 select new LanguageProficiency
														{
															Language = i.Language,
															Proficiency = i.Proficiency
														}).OrderBy(l => l.Language, StringComparer.InvariantCultureIgnoreCase).ToList();
		        CurrentLanguages = _languageInfo.Select(l => l.Language).ToList();
	        }

          if (skill.Skills.IsNotNull() && skill.Skills.Count > 0)	//_skill.Count == 0 && 
            _skill = skill.Skills.OrderBy(x => x.ToLower()).ToList();
        }

        //If user selects “yes” in driver's license, change “State of Issue” to user’s state (as shown on Contact page).
        if (model.ResumeContent.SeekerContactDetails.IsNotNull() &&
          model.ResumeContent.SeekerContactDetails.PostalAddress.IsNotNull() &&
          model.ResumeContent.SeekerContactDetails.PostalAddress.StateId.IsNotNull())
        {
          UserStateCode = model.ResumeContent.SeekerContactDetails.PostalAddress.StateId;
          DefaultDrivingLicense = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses).Where(x => x.Key == App.Settings.DefaultDrivingLicenseType).Select(x => x.Id).FirstOrDefault());
        }
      }
      else
				AddOneEmpty();
        
      foreach (var item in model.ResumeContent.EducationInfo.MostExperienceIndustries)
      {
        var nacis = ServiceClientLocator.CoreClient(App).GetNaics(item.ToString(CultureInfo.InvariantCulture), 1).FirstOrDefault();
        _industry.Add(nacis.Code + " - " + nacis.Name);
      }

      foreach (var item in model.ResumeContent.EducationInfo.TargetIndustries)
      {
        var nacis = ServiceClientLocator.CoreClient(App).GetNaics(item.ToString(CultureInfo.InvariantCulture), 1).FirstOrDefault();
        _targetindustry.Add(nacis.Code + " - " + nacis.Name);
      }

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				NCRCConfirmationCheckbox.Checked = model.ResumeContent.EducationInfo.NCRCConfirmation;

				if (model.ResumeContent.EducationInfo.NCRCLevel.IsNotNull())
					ddlNCRCLevel.SelectValue(model.ResumeContent.EducationInfo.NCRCLevel.ToString());

				if (model.ResumeContent.EducationInfo.NCRCStateId.IsNotNull())
					ddlNCRCState.SelectValue(model.ResumeContent.EducationInfo.NCRCStateId.ToString());

				if (model.ResumeContent.EducationInfo.NCRCIssueDate.IsNotNull())
					txtNCRCIssueDate.Text = ((DateTime)model.ResumeContent.EducationInfo.NCRCIssueDate).ToString("MM/dd/yyyy");

				if (model.ResumeMetaInfo.CompletionStatus == (ResumeCompletionStatuses.WorkHistory | ResumeCompletionStatuses.Contact))
					DisplayNCRCOnResumeCheckBox.Checked = true;
				else if (model.ResumeContent.EducationInfo.NCRCDisplayed.HasValue)
					DisplayNCRCOnResumeCheckBox.Checked = (bool)model.ResumeContent.EducationInfo.NCRCDisplayed;
			}
			
      Bind();
    }

    /// <summary>
    /// Adds the one empty.
    /// </summary>
    private void AddOneEmpty()
    {
      _degreeInfo = new List<DegreeInfo>();
      _degreeInfo.Add(new DegreeInfo { DegreeID = _degreeInfo.Count() });
      _licenseInfo = new List<LicenseInfo>();
      _licenseInfo.Add(new LicenseInfo { LicenseID = _licenseInfo.Count() });
    }

    #endregion

    #region Save education section

	  /// <summary>
	  /// Performs some extra server side validation on the model
	  /// </summary>
	  /// <param name="tempEducationInfo">The model to validate.</param>
	  /// <returns>
	  /// Whether the model is valid
	  /// </returns>
	  private bool ValidateModel(EducationInfo tempEducationInfo)
	  {
		  if (tempEducationInfo.Schools.IsNotNull())
		  {
			  var index = 0;
			  var valid = true;
				foreach (var school in tempEducationInfo.Schools)
				{
					var item = DegreeRepeater.Items[index];

					if (school.Major.IsNullOrEmpty())
					{
						var majorValidator = (CustomValidator) item.FindControl("MajorValidator");
						majorValidator.IsValid = false;
						majorValidator.ErrorMessage = MajorSubjectRequired;
						valid = false;
					}

					if (school.Degree.IsNullOrEmpty())
					{
						var degreeValidator = (CustomValidator)item.FindControl("DegreeNameValidator");
						degreeValidator.IsValid = false;
						degreeValidator.ErrorMessage = DegreeNameRequired;
						valid = false;
					}

					if (school.Institution.IsNullOrEmpty())
					{
						var schoolValidator = (CustomValidator)item.FindControl("SchoolValidator");
						schoolValidator.IsValid = false;
						schoolValidator.ErrorMessage = SchoolUniversityRequired;
						valid = false;
					}
                    if (school.Degree.IsNotNullOrEmpty() && school.CompletionDate.IsNull() && school.ExpectedCompletionDate.IsNull())
                    {
                        var unfinishedRadioButton = (RadioButton)item.FindControl("UnfinishedCompletionRadioButton");
                        if (!unfinishedRadioButton.Checked)
                        {
                            var completionDateValidator = (CustomValidator)item.FindControl("CompletionDateValidator");
                            completionDateValidator.IsValid = false;
                            completionDateValidator.ErrorMessage = CompletionDateRequired;
                            valid = false;
                        }
                    }
					index++;
				}

			  if (!valid)
				  return false;
		  }

		  if (tempEducationInfo.EducationLevel.IsNull())
		  {
			  EnrollmentStatusRequired.IsValid = false;
			  return false;
		  }

		  if (tempEducationInfo.SchoolStatus.IsNull() && !App.Settings.HideEnrollmentStatus)
		  {
			  custEnrolmentEducationLevel.IsValid = false;
			  return false;
		  }

		  return true;
		}

    /// <summary>
    /// Saves the model.
    /// </summary>
    /// <returns></returns>
    private bool SaveModel()
    {
      bool saveStatus;

	    var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
      var clonedResume = currentResume.CloneObject();
      App.SetSessionValue("Career:ResumeUpload", false);

      var tempEducationInfo = new EducationInfo
      {
        TargetIndustries = new List<long>(),
        MostExperienceIndustries = new List<long>(),
				SchoolStatus = App.Settings.Theme != FocusThemes.Education ? ddlEnrollmentStatus.SelectedValue.AsEnum<SchoolStatus>() : null,
        EducationLevel = ddlEducationLevel.SelectedValueToEnum<EducationLevel>(),
        Schools = (_degreeInfo.IsNull() || _degreeInfo.Count == 0) ? null : (from i in _degreeInfo
                                                                             select new School
                                                                             {
                                                                               SchoolId = i.DegreeID,
                                                                               CompletionDate = (i.Year.IsNotNullOrEmpty() && i.Month.IsNotNullOrEmpty() ? (DateTime?)(new DateTime(i.Year.AsInt(), i.Month.AsInt(), 1)) : null),
																																							 ExpectedCompletionDate = (i.ExpectedYear.IsNotNullOrEmpty() && i.ExpectedMonth.IsNotNullOrEmpty() ? (DateTime?)(new DateTime(i.ExpectedYear.AsInt(), i.ExpectedMonth.AsInt(), 1)) : null),
                                                                               Degree = string.IsNullOrEmpty(i.DegreeName) ? null : i.DegreeName,
                                                                               Institution = string.IsNullOrEmpty(i.University) ? null : i.University,
                                                                               Major = string.IsNullOrEmpty(i.Major) ? null : i.Major,
                                                                               Courses = string.IsNullOrEmpty(i.Course) ? null : i.Course,
                                                                               Honors = string.IsNullOrEmpty(i.Honors) ? null : i.Honors,
                                                                               GradePointAverage = i.GPA.GetGradePointAverage(),
                                                                               Activities = string.IsNullOrEmpty(i.Activities) ? null : i.Activities,
                                                                               SchoolAddress = new Address
                                                                               {
                                                                                 City = string.IsNullOrEmpty(i.City) ? null : i.City,
                                                                                 StateId = i.StateId.IsNull() ? null : i.StateId,
                                                                                 StateName = string.IsNullOrEmpty(i.StateName) ? null : i.StateName,
                                                                                 CountryId = i.CountryId.IsNull() ? null : i.CountryId,
                                                                                 CountryName = string.IsNullOrEmpty(i.CountryName) ? null : i.CountryName
                                                                               }
                                                                             }).ToList(),
				NCRCConfirmation = NCRCConfirmationCheckbox.Checked,
				NCRCLevel = ddlNCRCLevel.SelectedValue.ToLong(),
				NCRCLevelName = ddlNCRCLevel.SelectedValue.AsBoolean() ? ddlNCRCLevel.SelectedItem.Text : null,
				NCRCStateId = ddlNCRCState.SelectedValue.ToLong(),
				NCRCStateName = ddlNCRCState.SelectedValue.AsBoolean() ? ddlNCRCState.SelectedItem.Text : null,
				NCRCIssueDate = txtNCRCIssueDate.Text != "" && txtNCRCIssueDate.Text != "__/__/____" ? (DateTime?)DateTime.Parse(txtNCRCIssueDate.Text, CultureInfo.CurrentCulture) : null,
				NCRCDisplayed = DisplayNCRCOnResumeCheckBox.Checked
      };

      var tempUserInfo = new UserProfile();

      if (!App.Settings.HideDrivingLicence)
      {
        tempUserInfo.License = new Core.Models.Career.LicenseInfo
        {
          HasDriverLicense = ddlDriverLicense.SelectedValue.AsBoolean(),
          DriverStateId = ddlDriverLicense.SelectedValue.AsBoolean() ? ddlState.SelectedValueToLong() : (long?)null,
          DriverStateName = ddlDriverLicense.SelectedValue.AsBoolean() ? ddlState.SelectedItem.Text : null,
          DrivingLicenceClassId = ddlDriverLicense.SelectedValue.AsBoolean() ? ddlLicenseType.SelectedValueToLong() : (long?)null,
          DrivingLicenceEndorsementIds = _endorsement,
          DriverClassText = ddlDriverLicense.SelectedValue.AsBoolean() ? ddlLicenseType.SelectedItem.Text : null,
        };
      }


      foreach (var industryCode in _targetindustry.Select(industry => industry.Split('-').FirstOrDefault()))
      {
        tempEducationInfo.TargetIndustries.Add(long.Parse(industryCode));
      }

      foreach (var industryCode in _industry.Select(industry => industry.Split('-').FirstOrDefault()))
      {
        tempEducationInfo.MostExperienceIndustries.Add(long.Parse(industryCode));
      }

      var tempUnHideClassDLicense = new HideResumeInfo();
      var inHideResumeInfo = currentResume.Special.HideInfo;
      var unHideLicense = chkUnHideClassD.Checked;
      if (inHideResumeInfo.IsNotNull())
      {
        if (inHideResumeInfo.UnHideDriverLicense != unHideLicense)
        {
          inHideResumeInfo.UnHideDriverLicense = unHideLicense;
          inHideResumeInfo.HideDriverLicense = !unHideLicense;
        }
      }
      else
      {
        tempUnHideClassDLicense = new HideResumeInfo
        {
          UnHideDriverLicense = unHideLicense,
          HideDriverLicense = !unHideLicense
        };
      }

      var tempSkillInfo = new SkillInfo
      {
        Certifications = (_licenseInfo.IsNull() || _licenseInfo.Count == 0) ? null : (from i in _licenseInfo
                                                                                      select new Certification
                                                                                      {
                                                                                        Certificate = i.LicenseName,
                                                                                        CertificationAddress = new Address
                                                                                        {
                                                                                          City = i.City,
                                                                                          StateId = i.StateId,
                                                                                          StateName = i.StateName,
                                                                                          CountryId = i.CountryId,
                                                                                          CountryName = i.CountryName
                                                                                        },
																																												CompletionDate = (i.Year.IsNotNullOrEmpty() && i.Month.IsNotNullOrEmpty() ? (DateTime?)(new DateTime(i.Year.AsInt(), i.Month.AsInt(), 1)) : null),
																																												ExpectedCompletionDate = (i.ExpectedYear.IsNotNullOrEmpty() && i.ExpectedMonth.IsNotNullOrEmpty() ? (DateTime?)(new DateTime(i.ExpectedYear.AsInt(), i.ExpectedMonth.AsInt(), 1)) : null),
																																												OrganizationName = i.Organization
                                                                                      }).ToList(),
        LanguageProficiencies = _languageInfo,
        Skills = _skill
      };


      if (currentResume.IsNull())
      {
        currentResume = new ResumeModel
        {
          ResumeContent = new ResumeBody
          {
            EducationInfo = tempEducationInfo,
            Profile = tempUserInfo,
            Skills = tempSkillInfo,
          },
          ResumeMetaInfo = new ResumeEnvelope { CompletionStatus = ResumeCompletionStatuses.Education },
          Special = new ResumeSpecialInfo
          {
            HideInfo = new HideResumeInfo
            {
              UnHideDriverLicense = chkUnHideClassD.Checked,
              HideDriverLicense = !chkUnHideClassD.Checked
            },
          }
        };
      }
      else
      {
        currentResume.ResumeContent.EducationInfo = tempEducationInfo;

        if (currentResume.ResumeContent.Profile.IsNull())
          currentResume.ResumeContent.Profile = tempUserInfo;

        else
          currentResume.ResumeContent.Profile.License = tempUserInfo.License;
        currentResume.ResumeContent.Skills = tempSkillInfo;

        if (currentResume.Special.HideInfo.IsNull())
          currentResume.Special.HideInfo = tempUnHideClassDLicense;

        currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.Education;
      }

      var hiddenInfo = currentResume.Special.HideInfo;
      if (hiddenInfo.HiddenSkills.IsNull())
        hiddenInfo.HiddenSkills = new List<string>();

      var hiddenSkills = hiddenInfo.HiddenSkills;
			hiddenSkills.AddRange(_hiddenSkills.Where(skill => !hiddenSkills.Contains(skill, StringComparer.OrdinalIgnoreCase)));

      var resumeNameUpdated = false;
      var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
      if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
      {
        currentResume.ResumeMetaInfo.ResumeName = resumeName;
        resumeNameUpdated = true;
      }

      if (!Utilities.IsObjectModified(clonedResume, currentResume))
        return true;

			if (!ValidateModel(tempEducationInfo))
				return false;
      
      try
      {
				
				var resumeModel = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);
        var resumeId = resumeModel.ResumeMetaInfo.ResumeId;

        if (!App.UserData.HasDefaultResume)
          App.UserData.DefaultResumeId = resumeId;
        if (App.UserData.DefaultResumeId == resumeId)
          Utilities.UpdateUserContextUserInfo(null, ResumeCompletionStatuses.Education);
        if (currentResume.ResumeMetaInfo.ResumeId.IsNull())

          currentResume.ResumeMetaInfo.ResumeId = resumeId;

        App.SetSessionValue("Career:ResumeID", resumeId);
        App.SetSessionValue("Career:Resume", currentResume);

        saveStatus = true;

        #region Save Activity

        if (App.UserData.HasDefaultResume && App.UserData.DefaultResumeId == resumeId)
        {
          //TODO: Martha (new activity functionality)
          //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "37", staffProfile: staffcontext.StaffInfo);
        }

        #endregion
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
        saveStatus = false;
      }

      return saveStatus;
    }

    #endregion

    #region Binding Components

    /// <summary>
    /// Binds this instance.
    /// </summary>
    private void Bind()
    {
      BindEducationRepeater();
      BindLicenseRepeater();
      BindLanguageRepeater();
      BindSkillRepeater();
      BindEndorsement();
      BindIndustryRepeater();
      BindTargetIndustryRepeater();
    }

    /// <summary>
    /// Binds the education repeater.
    /// </summary>
    private void BindEducationRepeater()
    {
      if (_degreeInfo.Count == 0)
        _degreeInfo.Add(new DegreeInfo { DegreeID = 0 });
      DegreeRepeater.DataSource = _degreeInfo;
      DegreeRepeater.DataBind();
    }

    /// <summary>
    /// Binds the license repeater.
    /// </summary>
    private void BindLicenseRepeater()
    {
      if (_licenseInfo.Count == 0)
        _licenseInfo.Add(new LicenseInfo { LicenseID = 0 });
      LicenseRepeater.DataSource = _licenseInfo;
      LicenseRepeater.DataBind();
    }
    /// <summary>
    /// Binds the language repeater.
    /// </summary>
    private void BindLanguageRepeater()
    {
      if (_languageInfo.IsNullOrEmpty())
        return;
	    LanguageProficiencyMgr.BindLanguageRepeater(_languageInfo);
    }
    /// <summary>
    /// Binds the skill repeater.
    /// </summary>
    private void BindSkillRepeater()
    {
      if (_skill.IsNull())
        return;
      foreach (var item in _skill)
        if (item.IsNotNullOrEmpty())
          SkillUpdateableList.AddItem(new Item { Id = item, Name = item, Type = "SKILL" });
    }

    /// <summary>
    /// Binds the industry repeater.
    /// </summary>
    private void BindIndustryRepeater()
    {
      if (_industry.IsNull())
        return;
      foreach (var item in _industry)
        if (item.IsNotNull())
          IndustryUpdateableList.AddItem(new Item { Id = item, Name = item, Type = "INDUSTRY" });
    }

    /// <summary>
    /// Binds the industry repeater.
    /// </summary>
    private void BindTargetIndustryRepeater()
    {
      if (_targetindustry.IsNull())
        return;
      foreach (var item in _targetindustry)
        if (item.IsNotNull())
          TargetIndUpdateableList.AddItem(new Item { Id = item, Name = item, Type = "TARGETINDUSTRY" });
    }


    /// <summary>
    /// Binds the endorsement.
    /// </summary>
    private void BindEndorsement()
    {
      foreach (ListItem checkbox in EndorsementCheckBoxList.Items)
      {
        var checkboxvalue = checkbox.Value;
        checkbox.Selected = _endorsement.Any(x => x.ToString() == checkboxvalue);
      }
    }
    #endregion

    #region Unbind Components

		/// <summary>
		/// Unbinds the controls
		/// </summary>
		private void UnbindControls()
		{
			_degreeInfo = UnbindEducationRepeater();
			_licenseInfo = UnbindLicenseRepeater();
			_languageInfo = UnBindLanguageRepeater();
			_skill = UnBindSkillRepeater(_hiddenSkills);

			if (CurrentLanguages.IsNotNull())
			{
				var newLanguageNames = (_languageInfo.IsNotNull()) ? _languageInfo.Select(l => l.Language).ToList() : new List<string>();
				var deletedLanguages = CurrentLanguages.Where(l => !newLanguageNames.Contains(l, StringComparer.OrdinalIgnoreCase));
				_skill.RemoveAll(skill => deletedLanguages.Contains(skill, StringComparer.OrdinalIgnoreCase));
			}

			_industry = UnBindIndustryRepeater();
			_targetindustry = UnBindTargetIndustryRepeater();

			UnBindEndorsement();
		}

    /// <summary>
    /// Unbinds the education repeater.
    /// </summary>
    /// <returns></returns>
    private List<DegreeInfo> UnbindEducationRepeater()
    {

            return (from RepeaterItem item in DegreeRepeater.Items
                    let degreeId = ((HiddenField)item.FindControl("DegreeHiddenID")).Value
                    let degreename = ((TextBox)item.FindControl("DegreeTextBox")).TextTrimmed()
                    let major = ((TextBox)item.FindControl("MajorSubjectTextBox")).TextTrimmed()
                    let university = ((TextBox)item.FindControl("SchoolUniTextBox")).TextTrimmed()
                    let month = ((RadioButton)item.FindControl("CompletedRadioButton")).Checked ? (string.Equals(((TextBox)item.FindControl("CompletedDegreeTextBox")).TextTrimmed(),string.Empty) ? "": ((TextBox)item.FindControl("CompletedDegreeTextBox")).TextTrimmed().Split('/')[0]) : ""
                    let year = ((RadioButton)item.FindControl("CompletedRadioButton")).Checked ?(string.Equals(((TextBox)item.FindControl("CompletedDegreeTextBox")).TextTrimmed(),string.Empty) ? "":  ((TextBox)item.FindControl("CompletedDegreeTextBox")).TextTrimmed().Split('/')[1]) : ""
                    let expectedmonth = ((RadioButton)item.FindControl("ExpectedCompletionRadioButton")).Checked ? (string.Equals(((TextBox)item.FindControl("ExpectedCompletionTextBox")).TextTrimmed(),string.Empty) ? "" : ((TextBox)item.FindControl("ExpectedCompletionTextBox")).TextTrimmed().Split('/')[0]) : ""
                    let expectedyear = ((RadioButton)item.FindControl("ExpectedCompletionRadioButton")).Checked ? (string.Equals(((TextBox)item.FindControl("ExpectedCompletionTextBox")).TextTrimmed(), string.Empty) ? "" : ((TextBox)item.FindControl("ExpectedCompletionTextBox")).TextTrimmed().Split('/')[1]) : ""
                    let city = ((TextBox)item.FindControl("DegreeCityTextBox")).TextTrimmed()
                    let state = ((DropDownList)item.FindControl("DegreeStateDropDownList"))
                    let country = ((DropDownList)item.FindControl("DegreeCountryDropDownList"))
                    let course = ((TextBox)item.FindControl("CoursesTextBox")).TextTrimmed()
                    let honors = ((TextBox)item.FindControl("HonorsTextBox")).TextTrimmed()
                    let GPA = ((TextBox)item.FindControl("GPATextBox")).TextTrimmed()
                    let activities = ((TextBox)item.FindControl("ActivitiesTextBox")).TextTrimmed()
                    where degreename.IsNotNullOrEmpty()
                    select new DegreeInfo
                    {
                        DegreeID = degreeId.AsInt(),
                        DegreeName = degreename,
                        Major = major,
                        University = university,
                        Month = month,
                        Year = year,
                        ExpectedMonth = expectedmonth,
                        ExpectedYear = expectedyear,
                        City = city,
                        StateId = state.SelectedValue.ToLong(),
                        StateName = state.SelectedItem.Text,
                        CountryId = country.SelectedValue.ToLong(),
                        CountryName = country.SelectedItem.Text,
                        Course = course,
                        Honors = honors,
                        GPA = GPA,
                        Activities = activities
                    }).ToList();
        }
        /// <summary>
        /// Unbinds the license repeater.
        /// </summary>
        /// <returns></returns>
        private List<LicenseInfo> UnbindLicenseRepeater()
        {
            return (from RepeaterItem item in LicenseRepeater.Items
                    let licenseId = ((HiddenField)item.FindControl("LicenseHiddenID")).Value
                    let licenseName = ((TextBox)item.FindControl("ProfessionalLicenceTextBox")).TextTrimmed()
                    let oranization = ((TextBox)item.FindControl("IssuingOrgTextBox")).TextTrimmed()

							let completedLicenceRadioButton = (RadioButton)item.FindControl("CompletedLicenceRadioButton")
							let completedLicenceTextBox = (TextBox)item.FindControl("CompletedLicenceTextBox")
							let expectedLicenceRadioButton = (RadioButton)item.FindControl("ExpectedLicenceRadioButton")
							let expectedLicenceTextBox = (TextBox)item.FindControl("ExpectedLicenceTextBox")

							let month = completedLicenceRadioButton.Checked ? (string.Equals(completedLicenceTextBox.TextTrimmed(),string.Empty) ? "" : completedLicenceTextBox.TextTrimmed().Split('/')[0]) : ""
							let year = completedLicenceRadioButton.Checked ? (string.Equals(completedLicenceTextBox.TextTrimmed(),string.Empty) ? "" :completedLicenceTextBox.TextTrimmed().Split('/')[1]) : ""
							let expectedmonth = expectedLicenceRadioButton.Checked ? (string.Equals(expectedLicenceTextBox.TextTrimmed(),string.Empty) ? "" :expectedLicenceTextBox.TextTrimmed().Split('/')[0]) : ""
							let expectedyear = expectedLicenceRadioButton.Checked ? (string.Equals(expectedLicenceTextBox.TextTrimmed(),string.Empty) ? "" :expectedLicenceTextBox.TextTrimmed().Split('/')[1]) : ""

              let city = ((TextBox)item.FindControl("LicenseCityTextBox")).TextTrimmed()
              let state = ((DropDownList)item.FindControl("LicenseStateDropDownList"))
              let country = ((DropDownList)item.FindControl("LicenseCountryDropDownList"))
              where licenseName.IsNotNullOrEmpty()
              select new LicenseInfo
              {
                LicenseID = licenseId.AsInt(),
                LicenseName = licenseName,
                Organization = oranization,
                Month = month,
                Year = year,
								ExpectedMonth = expectedmonth,
								ExpectedYear = expectedyear,
                City = city,
                StateId = state.SelectedValue.ToLong(),
                StateName = state.SelectedItem.Text,
                CountryId = country.SelectedValue.ToLong(),
                CountryName = country.SelectedItem.Text
              }).ToList();
    }

    /// <summary>
    /// Uns the bind language repeater.
    /// </summary>
    /// <returns></returns>
		private List<LanguageProficiency> UnBindLanguageRepeater()
    {
	    return LanguageProficiencyMgr.UnBindLanguageRepeater();
    }

    /// <summary>
    /// Unbinds the skill repeater
    /// </summary>
    /// <param name="hiddenSkills">Will be populated with removed skills</param>
    /// <returns>A list of skills</returns>
    private List<string> UnBindSkillRepeater(List<string> hiddenSkills)
    {
      _hiddenSkills.Clear();

      var model = App.GetSessionValue<ResumeModel>("Career:Resume");

      var skillInfo = model.ResumeContent.Skills;
      if (skillInfo.IsNotNull() && skillInfo.Skills.IsNotNullOrEmpty())
      {
        var newSkills = SkillUpdateableList.Items.IsNotNull() 
          ? SkillUpdateableList.Items.Select(s => s.Name.ToLower()).ToList()
          : new List<string>();

        hiddenSkills.AddRange(skillInfo.Skills.Where(currentSkill => !newSkills.Contains(currentSkill, StringComparer.OrdinalIgnoreCase)));
      }

      var resultList = new List<string>();

      if (SkillUpdateableList.Items.IsNotNull())
        foreach (var item in SkillUpdateableList.Items)
          resultList.Add(Utilities.TitleCase(item.Name));

      return resultList;
    }

    /// <summary>
    /// Uns the bind industry repeater.
    /// </summary>
    /// <returns></returns>
    private List<string> UnBindIndustryRepeater()
    {
      var resultList = new List<string>();

      if (IndustryUpdateableList.Items.IsNotNull())
        foreach (var item in IndustryUpdateableList.Items)
          resultList.Add(Utilities.TitleCase(item.Name));

      return resultList;
    }

    /// <summary>
    /// Uns the bind industry repeater.
    /// </summary>
    /// <returns></returns>
    private List<string> UnBindTargetIndustryRepeater()
    {
      var resultList = new List<string>();

      if (TargetIndUpdateableList.Items.IsNotNull())
        foreach (var item in TargetIndUpdateableList.Items)
          resultList.Add(Utilities.TitleCase(item.Name));

      return resultList;
    }

    /// <summary>
    /// Uns the bind endorsement.
    /// </summary>
    private void UnBindEndorsement()
    {
      _endorsement = new List<long>();
      foreach (var checkboxValue in EndorsementCheckBoxList.Items.Cast<ListItem>().Where(checkbox => checkbox.Selected).Select(checkbox => checkbox.Value.ToLong()).Where(checkboxValue => checkboxValue.HasValue))
      {
        _endorsement.Add(checkboxValue.Value);
      }
    }

    #endregion

    #region ItemDataBound

    /// <summary>
    /// Handles the ItemDataBound event of the DegreeRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void DegreeRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        return;

      var degree = (DegreeInfo)e.Item.DataItem;
       var model = App.GetSessionValue<ResumeModel>("Career:Resume");

      var degreeId = ((HiddenField)e.Item.FindControl("DegreeHiddenID"));
      degreeId.Value = degree.DegreeID.ToString();

      var degreename = ((TextBox)e.Item.FindControl("DegreeTextBox"));
      degreename.Text = degree.DegreeName;

      var major = ((TextBox)e.Item.FindControl("MajorSubjectTextBox"));
      major.Text = degree.Major;

      var university = ((TextBox)e.Item.FindControl("SchoolUniTextBox"));
      university.Text = degree.University;

			var completedDate = ((TextBox)e.Item.FindControl("CompletedDegreeTextBox"));
            if (degree.Month.IsNullOrEmpty() || degree.Year.IsNullOrEmpty())
                completedDate.Text = "";
            else
                completedDate.Text = degree.Month + "/" + degree.Year;

      completedDate.ToolTip = CodeLocalise("ClosingDateTextBox.ToolTip", "Please enter the completion date as mm/yyyy");

      var completedRadioButton = ((RadioButton)e.Item.FindControl("CompletedRadioButton"));
      if (degree.Month.IsNotNullOrEmpty() && degree.Year.IsNotNullOrEmpty())
      {
      completedRadioButton.Checked = true;
      ((TextBox)e.Item.FindControl("ExpectedCompletionTextBox")).Enabled = false;
      }

			var expectedCompletionDate = ((TextBox)e.Item.FindControl("ExpectedCompletionTextBox"));
            if(degree.ExpectedMonth.IsNullOrEmpty() || degree.ExpectedYear.IsNullOrEmpty())
                expectedCompletionDate.Text = "";
            else
                expectedCompletionDate.Text = degree.ExpectedMonth + "/" + degree.ExpectedYear;

      expectedCompletionDate.ToolTip = CodeLocalise("ClosingDateTextBox.ToolTip", "Please enter the expected date as mm/yyyy");

			if (degree.ExpectedMonth.IsNotNullOrEmpty() && degree.ExpectedYear.IsNotNullOrEmpty())
			{
        completedRadioButton.Checked = false;
        
        var expectedCompletionRadioButton = ((RadioButton)e.Item.FindControl("ExpectedCompletionRadioButton"));
			  completedRadioButton.Checked = false;
				expectedCompletionRadioButton.Checked = true;
				((TextBox) e.Item.FindControl("CompletedDegreeTextBox")).Enabled = false;
        ((TextBox)e.Item.FindControl("ExpectedCompletionTextBox")).Enabled = true;
			}

            bool IsUpload = false;
            if (model.ResumeMetaInfo.ResumeCreationMethod == ResumeCreationMethod.Upload)
                IsUpload = App.GetSessionValue<bool>("Career:ResumeUpload");

            if (degreeId.IsNotNull() && degree.DegreeName.IsNotNull() && model.ResumeMetaInfo.CompletionStatus != (ResumeCompletionStatuses.WorkHistory | ResumeCompletionStatuses.Contact) && !IsUpload)          
            {
                if (degree.Month.IsNullOrEmpty() && degree.Year.IsNullOrEmpty() && degree.ExpectedMonth.IsNullOrEmpty() && degree.ExpectedYear.IsNullOrEmpty())
                {
                    var expectedCompletionRadioButton = ((RadioButton)e.Item.FindControl("ExpectedCompletionRadioButton"));
                    completedRadioButton.Checked = false;
                    expectedCompletionRadioButton.Checked = false;
                    var unfinishedCompletionRadioButton = ((RadioButton)e.Item.FindControl("UnfinishedCompletionRadioButton"));
                    unfinishedCompletionRadioButton.Checked = true;
                }
            }

      var city = ((TextBox)e.Item.FindControl("DegreeCityTextBox"));
      city.Text = degree.City;

      var state = ((DropDownList)e.Item.FindControl("DegreeStateDropDownList"));
      state.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), degree.StateId.IsNotNull() ? degree.StateId.ToString() : "", CodeLocalise("State.TopDefault", "- select state -"));

      var country = ((DropDownList)e.Item.FindControl("DegreeCountryDropDownList"));
      country.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), degree.CountryId.IsNotNull() ? degree.CountryId.ToString() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Key == App.Settings.DefaultCountryKey).Select(x => x.Id).SingleOrDefault().ToString(), CodeLocalise("Country.TopDefault", "- select country -"));

      var course = ((TextBox)e.Item.FindControl("CoursesTextBox"));
      course.Text = degree.Course;

      var honors = ((TextBox)e.Item.FindControl("HonorsTextBox"));
      honors.Text = degree.Honors;

      var gpa = ((TextBox)e.Item.FindControl("GPATextBox"));
      gpa.Text = degree.GPA;

      var activities = ((TextBox)e.Item.FindControl("ActivitiesTextBox"));
      activities.Text = degree.Activities;

      if (IsPostBack)
      {
        state.CssClass = "DegreeStateClass";
        country.CssClass = "DegreeCountryClass";
      }

      System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallDegreeRepeaterItems", "DegreeRepeaterItems();", true);
    }

    /// <summary>
    /// Handles the ItemDataBound event of the LicenseRepeater control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
    protected void LicenseRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
        return;

      var license = (LicenseInfo)e.Item.DataItem;

      var licenseId = ((HiddenField)e.Item.FindControl("LicenseHiddenID"));
      licenseId.Value = license.LicenseID.ToString();
      var licensename = ((TextBox)e.Item.FindControl("ProfessionalLicenceTextBox"));
      licensename.Text = license.LicenseName;
      var organization = ((TextBox)e.Item.FindControl("IssuingOrgTextBox"));
      organization.Text = license.Organization;

			var issuingDate = ((TextBox)e.Item.FindControl("CompletedLicenceTextBox"));
			var expectedDate = ((TextBox)e.Item.FindControl("ExpectedLicenceTextBox"));
			if (license.ExpectedMonth.IsNullOrEmpty() || license.ExpectedYear.IsNullOrEmpty())
			{
                if (license.Month.IsNullOrEmpty() || license.Year.IsNullOrEmpty())
                    issuingDate.Text = "";
                else
                    issuingDate.Text = string.Concat(license.Month, "/", license.Year);

				expectedDate.Text = ""; 
				
				var completedRadioButton = ((RadioButton)e.Item.FindControl("CompletedLicenceRadioButton"));
				completedRadioButton.Checked = true;
				expectedDate.Enabled = false;
			}
			else
			{
				issuingDate.Text = "";
                if(license.ExpectedMonth.IsNullOrEmpty() || license.ExpectedYear.IsNullOrEmpty())
                    expectedDate.Text = "";
                else
                    expectedDate.Text = string.Concat(license.ExpectedMonth, "/", license.ExpectedYear);
				
				var expectedRadioButton = ((RadioButton)e.Item.FindControl("ExpectedLicenceRadioButton"));
				expectedRadioButton.Checked = true;
				issuingDate.Enabled = false;
			}

      var city = ((TextBox)e.Item.FindControl("LicenseCityTextBox"));
      city.Text = license.City;
      var state = ((DropDownList)e.Item.FindControl("LicenseStateDropDownList"));
      state.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), license.StateId.IsNotNull() ? license.StateId.ToString() : "", CodeLocalise("State.TopDefault", "- select state -"));
      var country = ((DropDownList)e.Item.FindControl("LicenseCountryDropDownList"));
      country.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries), license.CountryId.IsNotNull() ? license.CountryId.ToString() : ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Countries).Where(x => x.Key == App.Settings.DefaultCountryKey).Select(x => x.Id).SingleOrDefault().ToString(), CodeLocalise("Country.TopDefault", "- select country -"));

      var validator = (CustomValidator) e.Item.FindControl("IssuingOrgValidator");
      validator.Enabled = (App.Settings.Theme != FocusThemes.Education);

      if (IsPostBack)
      {
        state.CssClass = "LicenseStateClass";
        country.CssClass = "LicenseCountryClass";
      }

      System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), "CallDegreeRepeaterItems", "LicenceRepeaterItems();", true);
    }
    #endregion

    #region ItemCommand - Add & Delete

    /// <summary>
    /// Handles the ItemCommand event of the DegreeRepeater control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
    protected void DegreeRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
      var selectedRow = e.CommandArgument.AsInt();
      _degreeInfo = UnbindEducationRepeater();
      switch (e.CommandName)
      {
        case "Delete":
          {
            _degreeInfo.RemoveAll(x => x.DegreeID == selectedRow);
            if (!_degreeInfo.Any())
              _degreeInfo.Add(new DegreeInfo { DegreeID = 0 });
          }
          break;
        case "Add":
          {
            _degreeInfo.Add(new DegreeInfo { DegreeID = _degreeInfo.Count() + 1 });            
          }
          break;
      }
      BindEducationRepeater();
    }

    /// <summary>
    /// Handles the ItemCommand event of the LicenseRepeater control.
    /// </summary>
    /// <param name="source">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterCommandEventArgs"/> instance containing the event data.</param>
    protected void LicenseRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
      var selectedRow = e.CommandArgument.AsInt();
      _licenseInfo = UnbindLicenseRepeater();
      switch (e.CommandName)
      {
        case "Delete":
          {
            _licenseInfo.RemoveAll(x => x.LicenseID == selectedRow);
            if (!_licenseInfo.Any())
              _licenseInfo.Add(new LicenseInfo { LicenseID = 0 });
          }
          break;
        case "Add":
          {
            _licenseInfo.Add(new LicenseInfo { LicenseID = _licenseInfo.Count() + 1 });
          }
          break;
      }
      BindLicenseRepeater();
    }

    #endregion

    #region Dropdown Bindings & UI localization

    /// <summary>
    /// Binds the dropdowns.
    /// </summary>
    private void BindDropdowns()
    {
      BindEnrollmentStatusDropDown();
      BindEducationLevelDropDown();
      ddlState.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), null, CodeLocalise("State.TopDefault", "- select state -"));
      ddlLicenseType.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses), null, CodeLocalise("LicenseType.TopDefault", "- select a type -"));

			if (App.Settings.Theme == FocusThemes.Workforce)
			{
				BindNCRCLevelDropDown();
				BindNCRCStateOfIssueDropDown();
			}
    }

		/// <summary>
		/// Binds the NCRC level drop down.
		/// </summary>
		private void BindNCRCLevelDropDown()
		{
			ddlNCRCLevel.Items.Clear();

			ddlNCRCLevel.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.NCRCLevel), null, CodeLocalise("NCRCLevel.TopDefault", "- select NCRC level -"), "select");
		}

		/// <summary>
		/// Binds the NCRC state of issue drop down.
		/// </summary>
		private void BindNCRCStateOfIssueDropDown()
		{
			ddlNCRCState.Items.Clear();

			ddlNCRCState.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States), null, CodeLocalise("NCRCStateOfIssue.TopDefault", "- select state -"), "select");
		}

    /// <summary>
    /// Binds the enrollment status drop down.
    /// </summary>
    private void BindEnrollmentStatusDropDown()
    {
      ddlEnrollmentStatus.Items.Clear();

			if (App.Settings.Theme == FocusThemes.Education)
			{
				if (App.Settings.SchoolType == SchoolTypes.TwoYear)
				{
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Prospective, "Prospective student");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.FirstYear, "First year student");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.SophomoreOrAbove, "Sophomore or above");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.NonCreditOther, "Non-credit/Other");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Alumni, "Alumni");
				}
				else if (App.Settings.SchoolType == SchoolTypes.FourYear)
				{
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Prospective, "Prospective student");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.FirstYear, "First year student");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Sophomore, "Sophomore");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Junior, "Junior");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Senior, "Senior");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Graduate, "Graduate student");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.NonCreditOther, "Non-credit/Other");
					ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Alumni, "Alumni");
				}
			}
			else
			{
				ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Not_Attending_School_HS_Graduate, "Not attending school, H.S. Graduate");
				ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.Not_Attending_School_OR_HS_Dropout, "Not attending school, H.S. Drop out");
				ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.In_School_Post_HS, "In school, Post H.S.");
				ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.In_School_Alternative_School, "In School, Alternative School");
				ddlEnrollmentStatus.Items.AddEnum(SchoolStatus.In_School_HS_OR_less, "In School, H.S. or less");
			}

    	ddlEnrollmentStatus.Items.AddLocalisedTopDefault("EnrollmentStatus.TopDefault", "- select enrollment status -");
    }

    /// <summary>
    /// Binds the education level drop down.
    /// </summary>
    private void BindEducationLevelDropDown()
    {
      CommonUtilities.GetEducationLevelDropDownList(ddlEducationLevel);
    }

    /// <summary>
    /// Binds the check box lists.
    /// </summary>
    private void BindCheckBoxLists()
    {
      var items = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements).OrderBy(lv => lv.Text).ToList();

      EndorsementCheckBoxList.BindLookup(items);
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      if (App.Settings.Theme == FocusThemes.Education)
      {
        if (!App.Settings.HideEnrollmentStatus)
        {
          EducationEnrollmentSubHeadingLabel.Text = HtmlLocalise("EnrollmentSubHeadingLabel.Text", "Enrollment status");
          EducationEnrollmentSubHeadingRequiredLabel.Text = HtmlLocalise("RequiredField.Text", "required field");
        }
      }
      else
      {
        if (App.Settings.HideEnrollmentStatus)
        {
          EducationEnrollmentSubHeadingLabel.Text = HtmlLocalise("EducationSubHeadingLabel.Text", "Education level");
          EducationEnrollmentSubHeadingRequiredLabel.Text = HtmlLocalise("RequiredField.Text", "required field");
        }
        else
        {
          EducationEnrollmentSubHeadingLabel.Text = HtmlLocalise("EducationEnrollmentSubHeadingLabel.Text", "Enrollment status & education level");
          EducationEnrollmentSubHeadingRequiredLabel.Text = HtmlLocalise("RequiredFields.Text", "required fields");
        }
      }

			NCRCLevelRequiredErrorMessage = CodeLocalise("NCRCLevel.RequiredErrorMessage", "NCRC level is required");
			NCRCStateRequiredErrorMessage = CodeLocalise("NCRCState.RequiredErrorMessage", "State of issue is required");
			NCRCIssueDateRequiredErrorMessage = CodeLocalise("NCRCIssueDate.RequiredErrorMessage", "Issue date is required");
			NCRCIssueDateInvalidErrorMessage = CodeLocalise("NCRCIssueDateInvalid.ErrorMessage", "Issue date is not a valid date");
			NCRCIssueDateInFutureErrorMessage = CodeLocalise("NCRCIssueDateInFuture.ErrorMessage", "Issue date must not be in the future");
			NCRCIssueDateInPastErrorMessage = CodeLocalise("NCRCIssueDateInPast.ErrorMessage", "Issue date must not be more than 100 years in the past");

      EnrollmentStatusRequired.ErrorMessage = CodeLocalise("EnrollmentStatus.RequiredErrorMessage", "Enrollment status is required");
      EducationLevelRequired = CodeLocalise("EducationLevel.RequiredErrorMessage", "Education level is required");
      EnrollmentStatusErrorMessage = CodeLocalise("EnrollmentStatus.ErrorMessage", "Enrollment status cannot be greater than education level");
      EducationLevelErrorMessage = CodeLocalise("EducationLevel.ErrorMessage", "Enrollment status cannot be less than education level");

      StateValidator.ErrorMessage = CodeLocalise("State.RequiredErrorMessage", "State is required");
      LicenseTypeValidator.ErrorMessage = CodeLocalise("LicenseType.ValidatorErrorMessage", "License type is required");

      DegreeNameRequired = CodeLocalise("DegreeName.ErrorMessage", "Degree name is required");
      MajorSubjectRequired = CodeLocalise("Major/Subject.ErrorMessage", "Major/subject is required");
      SchoolUniversityRequired = CodeLocalise("School/University.ErrorMessage", "School/university is required");
      CompletionDateRequired = CodeLocalise("CompletionDate.ErrorMessage", "You must select a completion status.");
			GPAOutofRange = CodeLocalise("School/GPA.ErrorMessage", "GPA must be between 0 and {0}", App.Settings.MaximumGradePointAverage);
	    MaxGPA = App.Settings.MaximumGradePointAverage.ToString(CultureInfo.InvariantCulture);
			ExpectedYearLimitsErrorMessage = CodeLocalise("ExpectedCompletedYear.LimitsErrorMessage", "Expected completion year must not be more than 100 years in the future");
      FutureDateErrorMessage = CodeLocalise("CompletedDate.ErrorMessage", "Degree completed date must not be in the future");
			PastDateErrorMessage = CodeLocalise("ExpectedDegreeCompletionDate.ErrorMessage", "Expected degree completion date must be in the future");
      StateRequired = CodeLocalise("State.RequiredErrorMessage", "State is required");
      CountryRequired = CodeLocalise("Country.RequiredErrorMessage", "Country is required");
      ProfessionalLicenseRequired = CodeLocalise("ProfessionalLicense.ErrorMessage", "Occupational license is required");
      IssuingOrganizationRequired = CodeLocalise("IssuingOrganization.ErrorMessage", "Issuing organization is required");

			CompletedDateRequired = CodeLocalise("CompletedDegreeDate.RequiredErrorMessage", "Degree completed date is required");
			ExpectedCompletionDateRequired = CodeLocalise("ExpectedDegreeCompletionDate.RequiredErrorMessage", "Expected degree completion date is required");
      DateErrorMessage = CodeLocalise("Date.ErrorMessage", "Valid date is required");
      LicenseCompletedDateRequired = CodeLocalise("LicenseCompletedDate.RequiredErrorMessage", "Issue date is required");
      LicenseCompletedDateTooLate = CodeLocalise("LicenseCompletedDateTooLate.RequiredrrorMessage", "Issue date must not be in the future");
			LicenseCompletedYearLimitsErrorMessage = CodeLocalise("LicenceCompletedYear.LimitsErrorMessage", "Issue date must not be more than 100 years in the past");
			LicenseCompletedTooYoung = CodeLocalise("LicenseCompletedTooYoung.ErrorMessage", "Issue date should be at least 10 years after date of birth");
			LicenseExpectedDateRequired = CodeLocalise("LicenseExpectedDate.RequiredErrorMessage", "Expected issue date is required");
			LicenseExpectedDateTooEarly = CodeLocalise("LicenseExpectedDateTooEarly.RequiredrrorMessage", "Expected issue date must be in the future");
			LicenseExpectedYearLimitsErrorMessage = CodeLocalise("LicenseExpectedYear.LimitsErrorMessage", "Expected issue date must not be more than 100 years in the future");

      DriverLicenseValidate.ErrorMessage = CodeLocalise("DriverLicense.RequiredErrorMessage", "Driver license is required");

      TooYoungToCompleteMessage = CodeLocalise("TooYoungToCompleteMessage.ErrorMessage", "Completion date should be at least 10 years after date of birth");
      TooEarlyToCompleteMessage = CodeLocalise("TooEarlyToCompleteMessage.ErrorMessage", "Completion date should not be before {0}/{1}", DateTime.Now.Month.ToString("00"), DateTime.Now.Year - 90);
      SelectOneEducationDateMessage = CodeLocalise("SelectOneEducationDateMessage.ErrorMessage", "Enter either a completion date or expected completion date");
    }

    /// <summary>
    /// Applies the theme.
    /// </summary>
		private void ApplyTheme()
		{
			if (App.Settings.Theme == FocusThemes.Education)
			{
				#region Label text changes

				EnrollmentStatusLabel.DefaultText = CodeLocalise("EnrollmentStatusEducation.Label", "Enrollment status");

				#endregion

				#region Hide/show controls

				EducationLevelRow.Visible = false;
				EducationEducationPlaceHolder1.Visible = false;
				NCRCPanel.Visible = false;

				#endregion

				#region Enable/Disable controls

				ddlEnrollmentStatus.Enabled = false;
        EducationInstructionLiteral.Visible = !App.Settings.HideEnrollmentStatus;

			  EducationInstructionLiteral.Text = HtmlLocalise(
			    "EducationInstruction.Label",
			    "If there has been a change to your enrollment status, please go to the {0} link to edit your status.",
          string.Format("<a href=\"#\" onclick=\"document.getElementById('MyAccountOpenPanel').value='Enrollment';document.getElementById('MyAccountLinkButton').click();\">{0}</a>", HtmlLocalise("MyAccount.Label", "My account")));

        if (App.User.EnrollmentStatus.HasValue)
        {
          var enrollmentStatus = App.User.EnrollmentStatus.ToString();
          if (ddlEnrollmentStatus.Items.FindByValue(enrollmentStatus) != null)
            ddlEnrollmentStatus.SelectedValue = enrollmentStatus;
        }

			  #endregion
			}

      EducationEnrollmentHeading.Visible = !App.Settings.HideEnrollmentStatus || App.Settings.Theme != FocusThemes.Education;
      EnrollmentStatusRow.Visible = !App.Settings.HideEnrollmentStatus;
		}

    #endregion

    /// <summary>
    /// Handles the Click event of the SkillAddButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SkillAddButton_Click(object sender, EventArgs e)
    {
      if (SkillTextBox.TextTrimmed().IsNotNullOrEmpty())
      {
        SkillUpdateableList.AddTop(new Item { Id = SkillTextBox.TextTrimmed(), Name = SkillTextBox.TextTrimmed(), Type = "SKILL" });
        SkillTextBox.Text = "";
      }
    }

    /// <summary>
    /// Handles the Click event of the LanguageAddButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		//protected void LanguageAddButton_Click(object sender, EventArgs e)
		//{
		//  if (LanguageTextBox.TextTrimmed().IsNotNullOrEmpty())
		//  {
		//    LanguageUpdateableList.AddTop(new Item { Id = LanguageTextBox.TextTrimmed(), Name = LanguageTextBox.TextTrimmed(), Type = "LANGUAGE" });
		//    LanguageTextBox.Text = "";
		//  }
		//}


    /// <summary>
    /// Handles the Click event of the Industry control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void IndustryAddButton_Click(object sender, EventArgs e)
    {
      if (MostExperienceIndustriesTextBox.TextTrimmed().IsNotNullOrEmpty())
      {
        if (IndustryUpdateableList.Items.IsNotNullOrEmpty())
        {
          if (IndustryUpdateableList.Items.Count >= 2)
          {
            MaxIndustryLabel.Visible = true;
            MaxIndustryLabel.Text = CodeLocalise("Validate.GreaterThanTwoIndustries", "Only 2 industries can be selected");
            return;
          }
        }
          MaxIndustryLabel.Visible = false;
          IndustryUpdateableList.AddTop(new Item { Id = MostExperienceIndustriesTextBox.TextTrimmed(), Name = MostExperienceIndustriesTextBox.TextTrimmed(), Type = "INDUSTRY" });
          MostExperienceIndustriesTextBox.Text = "";       
      }
    }

    /// <summary>
    /// Handles the Click event of the Industry control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void TargetIndAddButton_Click(object sender, EventArgs e)
    {
      if (TargetIndTextBox.TextTrimmed().IsNotNullOrEmpty())
      {
        if (TargetIndUpdateableList.Items.IsNotNullOrEmpty())
        {
          if (TargetIndUpdateableList.Items.Count >= 2)
          {
            MaxTargetIndValidator.Visible = true;
            MaxTargetIndValidator.Text = CodeLocalise("Validate.GreaterThanTwoIndustries", "Only 2 industries can be selected");
            return;
          }
        }
        MaxTargetIndValidator.Visible = false;
        TargetIndUpdateableList.AddTop(new Item { Id = TargetIndTextBox.TextTrimmed(), Name = TargetIndTextBox.TextTrimmed(), Type = "TARGETINDUSTRY" });
        TargetIndTextBox.Text = "";
      }
    }

		/// <summary>
		/// Gets the valid licence endorsements javascript array.
		/// </summary>
		/// <returns></returns>
	  private string GetValidLicenceEndorsementsJavascriptArray()
	  {
		  var javascriptBuilder = new StringBuilder(@"{"""":"""",");

		  var endorsementRules = App.Settings.DrivingLicenceEndorsementRules;

		  var drivingLicenceLookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceClasses);
			var drivingLicenceEndorsementLookUps = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DrivingLicenceEndorsements);

		  foreach (var rule in endorsementRules)
		  {
				var drivingLicenceKey = string.Format("DrivingLicenceClasses.{0}", rule.LicenceKey);

				var drivingLicenceId = drivingLicenceLookUps.Where(x => x.Key == drivingLicenceKey).Select(x => x.Id).FirstOrDefault();

			  if (drivingLicenceId.IsNotNull())
			  {
				  javascriptBuilder.AppendFormat(@"""{0}"": """, drivingLicenceId);

				  foreach (var endorsement in rule.EndorsementKeys)
				  {
						var endorsementKey = string.Format("DrivingLicenceEndorsements.{0}", endorsement);

						var endorsementId = drivingLicenceEndorsementLookUps.Where(x => x.Key == endorsementKey).Select(x => x.Id).FirstOrDefault();

					  if (endorsementId.IsNotNull())
						  javascriptBuilder.AppendFormat("{0},", endorsementId);
				  }

				  javascriptBuilder.Append(@""",");
			  }
		  }

		  javascriptBuilder.Append("}");

		  return javascriptBuilder.ToString().Replace(",}", "}");
	  }
  }

  #region Helper classes

  public class DegreeInfo
  {
    public int DegreeID { get; set; }
    public string DegreeName { get; set; }
    public string Major { get; set; }
    public string University { get; set; }
    public string Month { get; set; }
		public string Year { get; set; }
		public string ExpectedMonth { get; set; }
		public string ExpectedYear { get; set; }
    public string City { get; set; }
    public long? StateId { get; set; }
    public string StateName { get; set; }
    public long? CountryId { get; set; }
    public string CountryName { get; set; }
    public string Course { get; set; }
    public string Honors { get; set; }
    public string GPA { get; set; }
    public string Activities { get; set; }
  }

  public class LicenseInfo
  {
    public int LicenseID { get; set; }
    public string LicenseName { get; set; }
    public string Organization { get; set; }
    public string Month { get; set; }
    public string Year { get; set; }
		public string ExpectedMonth { get; set; }
		public string ExpectedYear { get; set; }
		public string City { get; set; }
    public long? StateId { get; set; }
    public string StateName { get; set; }
    public long? CountryId { get; set; }
    public string CountryName { get; set; }
  }

  #endregion
}
