﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResumeNavigationTabs.ascx.cs"
    Inherits="Focus.CareerExplorer.WebCareer.Controls.ResumeNavigationTabs" %>
<nav id="RCPNavigationTop">
    <a href="#" id="pull"></a>
    <ul>
        <li id="WorkHistoryLink" runat="server">
            <asp:LinkButton ID="linkWorkHistory" runat="server" OnClick="linkWorkHistory_Click">
	            <focus:LocalisedLabel runat="server" ID="WorkHistoryLabel" RenderOuterSpan="False" LocalisationKey="WorkHistory.LinkText" DefaultText="Work History"/>
            </asp:LinkButton>
        </li>
        <li id="ContactLink" runat="server">
	        <a id="LnkContact" runat="server" href="<%$RouteUrl:Tab=contact,routename=ResumeWizardTab %>">
		        <focus:LocalisedLabel runat="server" ID="ContactLabel" RenderOuterSpan="False" LocalisationKey="Contact.LinkText" DefaultText="Contact"/>
	        </a>
        </li>
        <li id="EducationLink" runat="server">
	        <a id="LnkEducation" runat="server" href="<%$RouteUrl:Tab=education,routename=ResumeWizardTab %>">
		        <focus:LocalisedLabel runat="server" ID="EducationLabel" RenderOuterSpan="False" LocalisationKey="Education.LinkText" DefaultText="Education"/>
	        </a>
        </li>
        <li id="SummaryLink" runat="server">
	        <a id="LnkSummary" runat="server" href="<%$RouteUrl:Tab=summary,routename=ResumeWizardTab %>">
		        <focus:LocalisedLabel runat="server" ID="SummaryLabel" RenderOuterSpan="False" LocalisationKey="Summary.LinkText" DefaultText="Summary"/>
	        </a>
        </li>
        <li id="AddInsLink" runat="server">
	        <a id="LnkAddIns" runat="server" href="<%$RouteUrl:Tab=options,routename=ResumeWizardTab %>">
		        <focus:LocalisedLabel runat="server" ID="AddInsLabel" RenderOuterSpan="False" LocalisationKey="AddIns.LinkText" DefaultText="Add-Ins"/>
	        </a>
        </li>
        <li id="ProfileLink" runat="server">
	        <a id="LnkProfile" runat="server" href="<%$RouteUrl:Tab=profile,routename=ResumeWizardTab %>">
		        <focus:LocalisedLabel runat="server" ID="ProfileLabel" RenderOuterSpan="False" LocalisationKey="Profile.LinkText" DefaultText="Profile"/>
	        </a>
        </li>
        <li id="PreferencesLink" runat="server">
	        <a id="LnkPreferences" runat="server" href="<%$RouteUrl:Tab=preferences,routename=ResumeWizardTab %>">
		        <focus:LocalisedLabel runat="server" ID="PreferencesLabel" RenderOuterSpan="False" LocalisationKey="Preferences.LinkText" DefaultText="Preferences"/>
	        </a>
        </li>
        <li id="ReviewLink" runat="server">
	        <a id="LnkReview" runat="server" href="<%$RouteUrl:Tab=review,routename=ResumeWizardTab %>">
		        <focus:LocalisedLabel runat="server" ID="ReviewLabel" RenderOuterSpan="False" LocalisationKey="Review.LinkText" DefaultText="Review"/>
	        </a>
        </li>
    </ul>
</nav>
<script type="text/javascript">
//	//Fix for breadcrumb sizing issues FVN-2618
/** KRP This script is not executing after Ajax Call. Moved this part
* to ResumeWizard.aspx

    $(document).ready(function () {
        //        var pl = $("[id$='ProfileLink']");
        //        if (pl.length === 0) {
        //        	$("#RCPNavigationTop ul li.last a").css("padding-right", "115px");
        //        }

        //        var ua = navigator.userAgent.toLowerCase();
        //        var isAndroid = ua.indexOf("android") > -1;
        //        if (isAndroid) {
        //        	$('#MainContent_UpdatePanelResumeWizard #RCPNavigation #RCPNavigationTop ul li a').css({
        //        		'padding-left': '0px', 'padding-right': '25px'
        //        	});
        //        }

        var pull = $('#pull');
        menu = $('#RCPNavigationTop ul');
        menuHeight = menu.height();

        listItems = menu.find("li").each(function () {
            var item = $(this);
            if (item.hasClass("selected")) {
                pull.text(item.text());
            }
            // rest of code.
        });

        $(pull).on('click', function (e) {
            e.preventDefault();
            menu.slideToggle();
        });

        $(window).resize(function () {
            var w = $(window).width();
            if (w > 768 && menu.is(':hidden')) {
                menu.removeAttr('style');
            }
        });
    });*/
</script>
