﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Common.Helpers;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;

using Framework.Core;
using System.Collections.Generic;

#endregion

namespace Focus.CareerExplorer.WebCareer.Controls
{
  public partial class Profile : UserControlBase
	{
    #region Page Properties
    protected string IsCurrentJob = "";
    #endregion

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      LocaliseUI();
			ApplyTheme();
            DOBPlaceholder.Visible = App.Settings.EnableDobFeatureGroup;

      if (IsPostBack)
        panMilitaryService.Style["display"] = (rblMilitaryService.SelectedValue == "yes") ? "" : "none";
    }

		protected void Page_PreRender(object sender, EventArgs e)
		{
			ScriptManager.RegisterClientScriptInclude(this, GetType(), "resumeBuilderProfileScriptInclude", UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/Focus.ResumeBuilder.Profile.min.js"));
			RegisterCodeValuesJson("resumeBuilderProfileScriptValues", "resumeProfileCodeValues", InitialiseClientSideCodeValues());
		}

    /// <summary>
    /// Handles the Click event of the SaveMoveToNextStepButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void SaveMoveToNextStepButton_Click(object sender, EventArgs e)
    {
			if (SaveModel())
				Response.RedirectToRoute("ResumeWizardTab", new { Tab = "Preferences" });
    }

    #region Bind Profile section

    /// <summary>
    /// Binds the step.
    /// </summary>
    internal void BindStep()
    {
      BindDropdowns();

      var model = App.GetSessionValue<ResumeModel>("Career:Resume");

      if (model.IsNotNull() && model.ResumeContent.IsNotNull())
      {
        if (model.ResumeContent.ExperienceInfo.IsNotNull() && model.ResumeContent.ExperienceInfo.EmploymentStatus.HasValue)
          ddlEmploymentStatus.SelectValue(model.ResumeContent.ExperienceInfo.EmploymentStatus.ToString());

        if (model.ResumeContent.Profile.IsNotNull())
        {
          var temp = model.ResumeContent.Profile;

          if (temp.DOB.IsNotNull() && temp.DOB.HasValue)
          {
            var dt = (DateTime)temp.DOB;
            DOBTextBox.Text = dt.ToString("MM/dd/yyyy");
          }
          else
          {
            var user = ServiceClientLocator.AccountClient(App).GetUserDetails(App.User.UserId);

            if (user.PersonDetails.DateOfBirth.IsNotNull() && user.PersonDetails.DateOfBirth.HasValue)
            {
              var dt = (DateTime)user.PersonDetails.DateOfBirth;
              DOBTextBox.Text = dt.ToString("MM/dd/yyyy");
            }
          }

          /*
          if (App.Settings.UnderAgeJobSeekerRestrictionThreshold > 0)
          {
            DOBTextBox.Enabled = false;
            DOBTextBox.Style.Add("background-color", "rgb(242, 242, 242);");
          }
          */

          if (temp.Sex.HasValue)
            ddlGender.SelectValue(temp.Sex.ToString());

          if (temp.EthnicHeritage.IsNotNull())
          {
            if (temp.EthnicHeritage.EthnicHeritageId.HasValue)
              rblEthnicityHeritage.SelectValue(temp.EthnicHeritage.EthnicHeritageId.ToString());

            if (temp.EthnicHeritage.RaceIds.IsNotNull() && temp.EthnicHeritage.RaceIds.Count > 0)

              foreach (ListItem racebox in cblRace.Items)
              {
                var raceboxValue = racebox.Value.ToLong();
                if (raceboxValue.HasValue)
                  racebox.Selected = temp.EthnicHeritage.RaceIds.Contains(raceboxValue.Value);
              }
          }

          if (App.Settings.Theme == FocusThemes.Workforce)
          {
            if (temp.IsUSCitizen.HasValue)
            {
              rblUSCitizen.SelectedValue = temp.IsUSCitizen.HasValue ? temp.IsUSCitizen.AsYesNo() : "yes";
              if (rblUSCitizen.SelectedValue == "no")
              {
                panAlienRegistrationDetail.Style["display"] = "";

                if (temp.AlienId.IsNotNullOrEmpty())
                  txtAlienRegNo.Text = temp.AlienId;

                if (temp.IsPermanentResident != null && temp.IsPermanentResident == false)
                  rblPermanentResident.SelectedIndex = 1;
                else
                  rblPermanentResident.SelectedIndex = 0;

                if (temp.AlienExpires.IsNotNull() && temp.AlienExpires.HasValue)
                {
                  var alienExpDt = (DateTime) temp.AlienExpires;
                  txtAlienRegExpire.Text = alienExpDt.ToString("MM/dd/yyyy");
                }
              }
              else
              {
                panAlienRegistrationDetail.Style["display"] = "none";
              }
            }

            if ((bool)temp.IsHomeless)
                cbHouseIssueHomeless.Checked = true;
            else
                cbHouseIssueHomeless.Checked = false;

            if ((bool)temp.IsStaying)
                cbHouseIssueStaying.Checked = true;
            else
                cbHouseIssueStaying.Checked = false;
            
            if ((bool)temp.IsUnderYouth)
                cbHouseIssueYouth.Checked = true;
            else
                cbHouseIssueYouth.Checked = false;


            if ((bool)temp.IsExOffender)
                panelLegalIssuesCheckbox.Checked = true;
            else
                panelLegalIssuesCheckbox.Checked = false;

            panMSFWDetail.Style["display"] = "none";
            if (temp.IsMSFW.HasValue)
            {
              rblMSFW.SelectedValue = temp.IsMSFW.AsYesNo();

              if (rblMSFW.SelectedValue == "yes" && App.Settings.PotentialMSFWQuestionsEnabled)
              {
                  panMSFWDetail.Style["display"] = "block";

                  if (temp.MSFWQuestions.IsNotNullOrEmpty())
                  {
                      foreach (ListItem msfwBox in cblMSFWQuestions.Items)
                      {
                          var msfwBoxValue = msfwBox.Value.ToLong();
                          if (msfwBoxValue.HasValue)
                              msfwBox.Selected = temp.MSFWQuestions.Contains(msfwBoxValue.Value);
                      }
                  }
              }
             
            }
          }
          else
          {
            if (temp.IsUSCitizen.GetValueOrDefault())
              rblEligibleToWork.SelectedIndex = 0;
            else if (temp.IsPermanentResident.GetValueOrDefault())
              rblEligibleToWork.SelectedIndex = 1;
            else if (temp.AuthorisedToWork.GetValueOrDefault())
              rblEligibleToWork.SelectedIndex = 2;
          }

					if (App.Settings.Theme == FocusThemes.Workforce && App.Settings.SelectiveServiceRegistration && temp.RegisteredWithSelectiveService.HasValue)
					{
						rblSelectiveService.SelectedValue = temp.RegisteredWithSelectiveService.HasValue ? temp.RegisteredWithSelectiveService.AsYesNo() : "yes";
					}

        if (temp.DisabilityStatus.HasValue)
        {
            ddlDisabilityStatus.SelectValue(temp.DisabilityStatus.ToString());
            if (temp.DisabilityStatus == DisabilityStatus.Disabled)
            {
                if (temp.DisabilityCategoryId.IsNotNull())
                {
                    cblDisabledCategory.SelectedValue = temp.DisabilityCategoryId.ToString();
                    temp.DisabilityCategoryId = null;
                }
                else if (temp.DisabilityCategoryIds.IsNotNull())
                {
                    if (temp.DisabilityCategoryIds.IsNotNull() && temp.DisabilityCategoryIds.Count > 0)
                        foreach (ListItem disability in cblDisabledCategory.Items)
                        {
                            var disabilityValue = disability.Value.ToLong();
                            if (disabilityValue.HasValue)
                                disability.Selected = temp.DisabilityCategoryIds.Contains(disabilityValue.Value);
                        }
                }
               
            }
        }

        cbLowLevelLiteracy.Checked = (bool)temp.LowLevelLiteracy ? true : false;
        if (!temp.PreferredLanguage.Equals("Not Disclosed"))
        {
            var Preferred = temp.PreferredLanguage.ToString();
            ddlPreferredLanguage.SelectedIndex = ddlPreferredLanguage.Items.IndexOf(ddlPreferredLanguage.Items.FindByText(Preferred));
        }
        else
            ddlPreferredLanguage.SelectedIndex = -1;

        if (!temp.LowLevelLiteracyIssues.NativeLanguage.Equals("Not disclosed") && cbLowLevelLiteracy.Checked)
        {
            panLanguageDetail.Style["display"] = "";
            ddlNativeLanguage.SelectedIndex = ddlNativeLanguage.Items.IndexOf(ddlNativeLanguage.Items.FindByText(temp.LowLevelLiteracyIssues.NativeLanguage));
            ddlCommonLanguage.SelectedIndex = ddlCommonLanguage.Items.IndexOf(ddlCommonLanguage.Items.FindByText(temp.LowLevelLiteracyIssues.CommonLanguage));
        }
        else
        {
            panLanguageDetail.Style["display"] = "none";
            ddlNativeLanguage.SelectedIndex = ddlCommonLanguage.SelectedIndex = -1;
        }


        cbCulturalBarriers.Checked = (bool)temp.CulturalBarriers ? true : false;
        if(!temp.NoOfDependents.Equals("Not Disclosed"))
            DependantsTextBox.Text = temp.NoOfDependents.ToString();
        if (!temp.EstMonthlyIncome.Equals("Not Disclosed"))
            IncomeTextBox.Text = temp.EstMonthlyIncome.ToString();
        cbDisplacedHomemaker.Checked = (bool)temp.DisplacedHomemaker ? true : false;
        cbSingleParent.Checked = (bool)temp.SingleParent ? true : false;
        cbLowIncomeStatus.Checked = (bool)temp.LowIncomeStatus ? true : false;


        if (temp.Veteran.IsNotNull())
          {
            if(temp.Veteran.IsVeteran.IsNotNull())
            {
            rblMilitaryService.SelectValue(((bool?)temp.Veteran.IsVeteran).AsYesNo());            
            panMilitaryService.Style["display"] = "none";
            if (rblMilitaryService.SelectedValue == "yes")
            {
              panMilitaryService.Style["display"] = "";

							if (temp.Veteran.IsHomeless.HasValue && (bool)temp.Veteran.IsHomeless)
								rblHomeless.SelectedIndex = 0;
							else
								rblHomeless.SelectedIndex = 1;

							if (temp.Veteran.AttendedTapCourse.HasValue && (bool)temp.Veteran.AttendedTapCourse)
								rblTap.SelectedIndex = 0;
							else
								rblTap.SelectedIndex = 1;

							if (temp.Veteran.DateAttendedTapCourse.HasValue)
                TapAttendanceDateTextBox.Text = ((DateTime)temp.Veteran.DateAttendedTapCourse).ToString("MM/yyyy");

              rblSubscribeToVetAlerts.SelectedIndex = temp.Veteran.VeteranPriorityServiceAlertsSubscription ? 1 : 0;
            }

            }
          } 
          MilitaryHistory.Bind(temp.Veteran);
        }

        if (App.Settings.ReadOnlyResumeDOB && DOBTextBox.Text.IsNullOrEmpty())
        {
          DOBValidator.IsValid = false;
          DOBValidator.ErrorMessage = string.Empty;
          DOBReadOnlyButRequiredLabel.Visible = true;
        }

      }
    }

    #endregion

		/// <summary>
		/// Performs some extra server side validation on the model
		/// </summary>
		/// <param name="tempProfile">The model to validate.</param>
		/// <returns>Whether the model is valid</returns>
		private bool ValidateModel(UserProfile tempProfile)
		{
            if (tempProfile.DisabilityStatus == DisabilityStatus.Disabled && tempProfile.DisabilityCategoryIds.IsNull())
			{
				DisabilityValidator.IsValid = false;
				return false;
			}

			return true;
		}

    /// <summary>
    /// Saves the model.
    /// </summary>
    /// <returns></returns>
    private bool SaveModel()
    {
      bool saveStatus;
      var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
      var tempProfile = new UserProfile();
      // AIM - FVN-4716-Reactivating same JobSeeker 2 times results in error
      if (currentResume.IsNotNull())
        tempProfile = currentResume.ResumeContent.Profile;
      else
          Response.Redirect(UrlBuilder.Home());
      var veteran = new VeteranInfo();
      var clonedResume = currentResume.CloneObject();
      
			#region Workforce specific data

			var tempEmploymentStatus = EmploymentStatus.None;

			if (App.Settings.Theme == FocusThemes.Workforce )
			{
                if (App.Settings.EnableDobFeatureGroup)
                {
                    tempProfile.DOB = Convert.ToDateTime(DOBTextBox.Text);
                }
				tempProfile.Sex = ddlGender.SelectedValue.IsNotNullOrEmpty()
														? ddlGender.SelectedValueToEnum<Genders>()
														: (Genders?)null;
				tempProfile.EthnicHeritage = new EthnicHeritage
				{
					EthnicHeritageId =
						rblEthnicityHeritage.SelectedValue.IsNotNullOrEmpty()
							? rblEthnicityHeritage.SelectedValueToLong()
							: (long?)null,
					RaceIds = (from ListItem checkbox in cblRace.Items
										 where checkbox.Selected
										 select checkbox.Value.ToLong()
											 into raceId
											 where raceId.HasValue
											 select raceId.Value).ToList().ConvertAll<long?>(x => x)
				};

                tempProfile.DisabilityStatus = (DisabilityStatus)Enum.Parse(typeof(DisabilityStatus), ddlDisabilityStatus.SelectedValue, true);

                tempProfile.DisabilityCategoryIds = (from ListItem checkbox in cblDisabledCategory.Items
                                                     where checkbox.Selected && checkbox.Value.IsNotNullOrEmpty()
                                                     select checkbox.Value.ToLong()
                                                         into disabilityid
                                                         where disabilityid.HasValue
                                                         select disabilityid.Value).ToList().ConvertAll<long?>(x => x);
                

				//var isOtherInfoUpdated = false;
				tempEmploymentStatus = (EmploymentStatus)Enum.Parse(typeof(EmploymentStatus), ddlEmploymentStatus.SelectedValue, true);
				//if (currentResume.ResumeContent.ExperienceInfo.IsNull() || currentResume.ResumeContent.ExperienceInfo.EmploymentStatus != tempEmploymentStatus)
				//  isOtherInfoUpdated = true;
                tempProfile.PreferredLanguage = ddlPreferredLanguage.SelectedIndex!=0? ddlPreferredLanguage.SelectedItem.Text :"Not disclosed" ;
                tempProfile.LowLevelLiteracy = cbLowLevelLiteracy.Checked.AsBoolean();
                tempProfile.CulturalBarriers = cbCulturalBarriers.Checked.AsBoolean();

                if (cbLowLevelLiteracy.Checked)
                {
                    tempProfile.LowLevelLiteracyIssues = new LowLevelLiteracyIssues
                    {
                        CommonLanguage = ddlCommonLanguage.SelectedItem.Text,
                        NativeLanguage = ddlNativeLanguage.SelectedItem.Text

                    };
                }

                tempProfile.NoOfDependents = !DependantsTextBox.Text.IsNullOrEmpty()?DependantsTextBox.Text.ToString():"Not disclosed" ;
                tempProfile.EstMonthlyIncome = !IncomeTextBox.Text.IsNullOrEmpty()?IncomeTextBox.Text.ToString():"Not disclosed" ;
                tempProfile.DisplacedHomemaker = cbDisplacedHomemaker.Checked.AsBoolean();
                tempProfile.SingleParent = cbSingleParent.Checked.AsBoolean();
                tempProfile.LowIncomeStatus = cbLowIncomeStatus.Checked.AsBoolean();

			}
       
			#endregion

			#region Generic data

			if (rblMilitaryService.SelectedIndex == 0)
			{
				#region Military Service

				veteran.IsVeteran = true;

        MilitaryHistory.SaveModel(veteran);

				veteran.IsHomeless = rblHomeless.SelectedIndex == 0;
				veteran.AttendedTapCourse = rblTap.SelectedIndex == 0;
				
				veteran.VeteranPriorityServiceAlertsSubscription = rblSubscribeToVetAlerts.SelectedIndex != 0;
				// Checks if the user has turned on the subscription if was currently off as a action will be logged
				if (currentResume.ResumeContent.Profile.Veteran.VeteranPriorityServiceAlertsSubscription && !veteran.VeteranPriorityServiceAlertsSubscription)
					veteran.VeteranPriorityServiceAlertsSubscriptionChanged = true;

				if (veteran.AttendedTapCourse.HasValue && (bool)veteran.AttendedTapCourse)
					veteran.DateAttendedTapCourse = Convert.ToDateTime(TapAttendanceDateTextBox.Text);

				#endregion
			}
			else
				veteran.IsVeteran = false;

      if (App.Settings.Theme == FocusThemes.Workforce)
      {
        tempProfile.IsUSCitizen = rblUSCitizen.SelectedValue.AsBoolean();
        tempProfile.IsExOffender = panelLegalIssuesCheckbox.Checked.AsBoolean();
        tempProfile.IsHomeless = cbHouseIssueHomeless.Checked.AsBoolean();
        tempProfile.IsStaying = cbHouseIssueStaying.Checked.AsBoolean();
        tempProfile.IsUnderYouth = cbHouseIssueYouth.Checked.AsBoolean();

        tempProfile.AlienId = (rblUSCitizen.SelectedValue == "no") ? txtAlienRegNo.TextTrimmed(defaultValue: null) : null;
        tempProfile.IsPermanentResident = (rblUSCitizen.SelectedValue == "no")
                                            ? (bool?)rblPermanentResident.SelectedValue.AsBoolean()
                                            : null;

	      //var mask = meAlienRegExpire.Mask.Replace("9", meAlienRegExpire.PromptCharacter);
        tempProfile.AlienExpires = (rblUSCitizen.SelectedValue == "no" && txtAlienRegExpire.Text.IsNotNullOrEmpty() /*&& txtAlienRegExpire.Text != mask*/)
                                     ? (DateTime?)Convert.ToDateTime(txtAlienRegExpire.TextTrimmed())
                                     : null;



	      tempProfile.RegisteredWithSelectiveService = rblSelectiveService.SelectedValue.IsNotNullOrEmpty() ? rblSelectiveService.SelectedValue.AsBoolean() : (bool?)null;

        tempProfile.IsMSFW = rblMSFW.SelectedValue.AsBoolean();
        tempProfile.MSFWQuestions = (from ListItem checkbox in cblMSFWQuestions.Items
                                     where checkbox.Selected && checkbox.Value.IsNotNullOrEmpty()
                                     select checkbox.Value.ToLong().GetValueOrDefault(0)).ToList();
      }
      else
      {
        tempProfile.IsUSCitizen = rblEligibleToWork.SelectedValue == "0";
        tempProfile.IsPermanentResident = rblEligibleToWork.SelectedValue == "1";
        tempProfile.AuthorisedToWork = rblEligibleToWork.SelectedValue == "2";
        tempProfile.AlienId = null;
        tempProfile.AlienExpires = null;
      }

      tempProfile.Veteran = veteran;
      if (tempProfile.Veteran.IsNotNull() && tempProfile.Veteran.IsVeteran.GetValueOrDefault(false))
      {
        //isOtherInfoUpdated = true;
        var militaryEmploymentStatus = MilitaryHistory.GetLatestEmploymentStatus();
        if (militaryEmploymentStatus != "" && (!currentResume.ResumeContent.ExperienceInfo.EmploymentStatus.HasValue || ((int)currentResume.ResumeContent.ExperienceInfo.EmploymentStatus).ToString() != militaryEmploymentStatus))
          currentResume.ResumeContent.ExperienceInfo.EmploymentStatus = (EmploymentStatus)Enum.Parse(typeof(EmploymentStatus), militaryEmploymentStatus, true);

        bool pastFiveYearsVeteran = Utilities.IsPastFiveYearsVeteran(tempProfile.Veteran.History[0].VeteranStartDate, tempProfile.Veteran.History[0].VeteranEndDate);
        if (currentResume.Special.IsNotNull())
          currentResume.Special.IsPastFiveYearsVeteran = pastFiveYearsVeteran;
        else
          currentResume.Special = new ResumeSpecialInfo { IsPastFiveYearsVeteran = pastFiveYearsVeteran };
      }
      else
      {
        if (currentResume.Special.IsNotNull() && currentResume.Special.IsPastFiveYearsVeteran.HasValue && (bool)currentResume.Special.IsPastFiveYearsVeteran)
        {
          currentResume.Special.IsPastFiveYearsVeteran = false;
          //isOtherInfoUpdated = true;
        }
      }

      //int previousResumeCompletionStatus = 0;
      if (currentResume.IsNull())
      {
        currentResume = new ResumeModel
        {
          ResumeContent = new ResumeBody
          {
            Profile = tempProfile,
            ExperienceInfo = new ExperienceInfo
            {
              EmploymentStatus = tempEmploymentStatus != EmploymentStatus.None ? tempEmploymentStatus : (EmploymentStatus?)null
            }
          },
          ResumeMetaInfo = new ResumeEnvelope { CompletionStatus = ResumeCompletionStatuses.Profile }
        };
      }
      else
      {
        //previousResumeCompletionStatus = (int)currentResume.ResumeMetaInfo.CompletionStatus;
        currentResume.ResumeContent.Profile = tempProfile;
        if (currentResume.ResumeContent.ExperienceInfo.IsNotNull())
          currentResume.ResumeContent.ExperienceInfo.EmploymentStatus = tempEmploymentStatus;
        else
          currentResume.ResumeContent.ExperienceInfo = new ExperienceInfo { EmploymentStatus = tempEmploymentStatus };


        //if (previousResumeCompletionStatus < (int)ResumeCompletionStatus.Profile)
        currentResume.ResumeMetaInfo.CompletionStatus |= ResumeCompletionStatuses.Profile;
      }

      var resumeNameUpdated = false;
      var resumeName = ((ResumeWizard)Page).ResumeTitleTextBox.TextTrimmed(defaultValue: null);
      if (resumeName.IsNotNullOrEmpty() && currentResume.ResumeMetaInfo.ResumeName != resumeName)
      {
        currentResume.ResumeMetaInfo.ResumeName = resumeName;
        resumeNameUpdated = true;
      }

			#endregion

			if (!ValidateModel(tempProfile))
				return false;

			if (!Utilities.IsObjectModified(clonedResume, currentResume))
        return true;

	    try
      {
				
				var resume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, resumeNameUpdated);
        var resumeId = resume.ResumeMetaInfo.ResumeId;

        if (!App.UserData.HasDefaultResume)
          App.UserData.DefaultResumeId = resumeId;

        if (App.UserData.DefaultResumeId == resumeId)
          Utilities.UpdateUserContextUserInfo(tempProfile, ResumeCompletionStatuses.Profile);

        if (currentResume.ResumeMetaInfo.ResumeId.IsNotNull())
          currentResume.ResumeMetaInfo.ResumeId = resumeId;

        App.SetSessionValue("Career:ResumeID", resumeId);
        App.SetSessionValue("Career:Resume", currentResume);
        saveStatus = true;

        #region Save Activity

        if (App.UserData.HasDefaultResume && App.UserData.DefaultResumeId == resumeId)
        {
          //var staffcontext = App.GetSessionValue<StaffContext>("Career:StaffContext");
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(usercontext.UserId, usercontext.Username, ActivityOwner.Staff, ActivityType.Automated, "37", staffProfile: staffcontext.StaffInfo);
        }

        #endregion
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
        saveStatus = false;
      }
      return saveStatus;
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      GenderRequired.ErrorMessage = CodeLocalise("Gender.RequiredErrorMessage", "Gender is required");
      DisabilityStatusRequired.ErrorMessage = CodeLocalise("DisabilityStatus.RequiredErrorMessage", "Disability status is required");
      DisabilityValidator.ErrorMessage = CodeLocalise("DisabilityType.RequiredErrorMessage", "At least one selection is required");
      cblDisabledCategoryValidator.ErrorMessage = CodeLocalise("DisabilityType.RequiredErrorMessage", "\"Not disclosed\" cannot be selected with other impairments");
      USCitizenRequired.ErrorMessage = CodeLocalise("USCitizen.RequiredErrorMessage", "U.S. citizen status is required");
      MSFWRequired.ErrorMessage = CodeLocalise("MSFWRequired.RequiredErrorMessage", "MSFW status is required");
      //MSFWQuestionRequired.ErrorMessage = CodeLocalise("MSFWRequired.RequiredErrorMessage", "At least one option must be selected");
      EligibleToWorkValidator.ErrorMessage = CodeLocalise("EligibleToWork.RequiredErrorMessage", "Work criteria is required");
      MilitaryServiceRequired.ErrorMessage = CodeLocalise("MilitaryServiceRequired.RequiredErrorMessage", "Military service status is required");
      RaceValidator.ErrorMessage = CodeLocalise("Race.RequiredErrorMessage", "Race is required");
      EthnicityHeritageValidator.ErrorMessage = CodeLocalise("EthnicityHeritage.RequiredErrorMessage", "Ethnicity/heritage is required");
      RunAwayYouthValidator.ErrorMessage = CodeLocalise("RunAwayYouthValidator.RequiredErrorMessage", "Your date of birth indicates that you are over 18 years of age. To select this item, you must be 18 years old or younger.");
      HomelessCheckBoxValidator.ErrorMessage = CodeLocalise("HomelessCheckBoxValidator.RequiredErrorMessage", " Please choose between check box 1 and 2, but not both.");
      NativeLanguageRequired.ErrorMessage = CodeLocalise("NativeLanguageRequired.RequiredErrorMessage", "Language is required");
      CommonLanguageRequired.ErrorMessage = CodeLocalise("CommonLanguageRequired.RequiredErrorMessage", "Language is required");
      DependantsValidator.ErrorMessage = CodeLocalise("DependantsValidator.RequiredErrorMessage", "You must enter at least 1 to claim yourself.");
      employeValidator.ErrorMessage = CodeLocalise("employeValidator.RequiredErrorMessage", "Employment status must be “Not Employed” to make this selection.");
      DOBTextBox.ToolTip = CodeLocalise("DOBTextBox.ToolTip", "Please enter the date of birth as mm/dd/yyyy");
      
      if (App.Settings.ReadOnlyResumeDOB)
        DOBReadOnlyButRequiredLabel.Text = CodeLocalise("DOBReadOnlyButRequiredLabel.Text", "Please contact your local office to add a date of birth");

			SelectiveServiceRequired.ErrorMessage = CodeLocalise("SelectiveService.RequiredErrorMessage", "Selective Service response is required.");
    }

    /// <summary>
    /// Applies the theme.
    /// </summary>
		private void ApplyTheme()
		{
			if (App.Settings.Theme == FocusThemes.Education)
			{
        WorkforceProfileSubHeading.Visible =
          WorkforceProfilePlaceHolder1.Visible =
          WorkforceProfilePlaceHolder2.Visible =
          WorkforceProfileSaveNext2.Visible =
          WorkforceProfileSaveNext1.Visible =
          HomelessSpan.Visible =
          TapDateSpan.Visible =
          TapSpan.Visible =
          ToApplyLabel.Visible = false;

        ToApplyEducationLabel.Visible =
          EducationProfilePlaceHolder.Visible =
          EducationProfileSubHeading.Visible = true;
			}

      SubscribeToVetAlertsSpan.Visible = App.Settings.VeteranPriorityServiceEnabled;

			DOBTextBox.Enabled = !App.Settings.ReadOnlyResumeDOB;
		}
    /// <summary>
    /// Binds the dropdowns.
    /// </summary>
    private void BindDropdowns()
    {
      CommonUtilities.GetEmploymentStatusDropDownList(ddlEmploymentStatus);
      BindGenderLookup();
      rblEthnicityHeritage.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EthnicHeritages), null); //((int)EthnicityCode.NotDisclosed).ToString()
      cblRace.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Races), null); //((int)RaceCode.NotDisclosed).ToString()
      cblMSFWQuestions.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.MSFWQuestions), null);
      foreach (ListItem item in cblRace.Items)
      {
				item.Attributes.Add("onclick", "CheckNotDisclosed($(this))");
				item.Attributes.Add("onkeypress", "CheckNotDisclosed($(this))");
      }
      BindDisabilityStatusDropDown();
      cblDisabledCategory.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DisabilityCategories), null);
      foreach (ListItem item in cblDisabledCategory.Items)
      {
          item.Attributes.Add("onclick", "CheckDisabledNotDisclosed($(this))");
      }

      ddlCommonLanguage.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Languages), null, CodeLocalise("CommonLanguage.TopDefault", "- select common language -"));
      ddlNativeLanguage.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Languages), null, CodeLocalise("CommonLanguage.TopDefault", "- select native language -"));
      ddlPreferredLanguage.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Languages), null, CodeLocalise("CommonLanguage.TopDefault", "- select preferred language -"));
      SetDefaults();
    }

    /// <summary>
    /// Binds the gender lookup.
    /// </summary>
    private void BindGenderLookup()
    {
      ddlGender.Items.Clear();

      ddlGender.Items.AddEnum(Genders.Female, "Female");
      ddlGender.Items.AddEnum(Genders.Male, "Male");
      ddlGender.Items.AddEnum(Genders.NotDisclosed, "Not Disclosed");

      ddlGender.Items.AddLocalisedTopDefault("ddlGender.TopDefault", "- select gender -");
    }

    /// <summary>
    /// Binds the disability status drop down.
    /// </summary>
    private void BindDisabilityStatusDropDown()
    {
      ddlDisabilityStatus.Items.Clear();

      ddlDisabilityStatus.Items.AddEnum(DisabilityStatus.NotDisabled, "Not Disabled");
      ddlDisabilityStatus.Items.AddEnum(DisabilityStatus.Disabled, "Disabled");
      ddlDisabilityStatus.Items.AddEnum(DisabilityStatus.NotDisclosed, "Not Disclosed");

      ddlDisabilityStatus.Items.AddLocalisedTopDefault("DisabilityStatus.TopDefault", "- select status -");
    }

    /// <summary>
    /// Sets the defaults.
    /// </summary>
    private void SetDefaults()
    {
      var model = App.GetSessionValue<ResumeModel>("Career:Resume");
      if (model.IsNotNull() && model.ResumeContent.IsNotNull() && model.ResumeContent.ExperienceInfo.IsNotNull() && model.ResumeContent.ExperienceInfo.Jobs.IsNotNull())
      {
        foreach (var j in model.ResumeContent.ExperienceInfo.Jobs)
        {
          if (j.IsCurrentJob.HasValue && j.IsCurrentJob == true)
          {
            ddlEmploymentStatus.SelectValue(EmploymentStatus.Employed.ToString());
            IsCurrentJob = ((int)EmploymentStatus.Employed).ToString();
            break;
          }
        }
        if (App.Settings.Theme == FocusThemes.Workforce)
        {
          rblUSCitizen.SelectedIndex = 0;
         //rblMSFW.SelectedIndex = 1;
          rblMilitaryService.SelectedIndex = -1;
          
        }
      }
    }

    /// <summary>
    /// Initialises the client side code values.
    /// </summary>
    /// <returns></returns>
		private NameValueCollection InitialiseClientSideCodeValues()
		{
			var nvc = new NameValueCollection
				          {
										{ "errorDOBServiceDutyStartDateLimit", CodeLocalise("DOBServiceDutyStartDate.ErrorMessage", "Please check your birthdate. You must have been at least {0} years <br/>or older to enter military service.", App.Settings.MilitaryServiceMinimumAge.ToString(CultureInfo.CurrentUICulture)) },
										{ "errorDateOfBirthRequired", App.Settings.ReadOnlyResumeDOB? string.Empty : CodeLocalise("DateOfBirth.RequiredErrorMessage", "Date of birth is required") }, 
										{ "errorDateFormat", CodeLocalise("DateFormat.ErrorMessage", "Valid date is required") },
										{ "errorDateOfBirthLimitMin",string.Format( CodeLocalise("DOBLimitMin.ErrorMessage", "You must be at least {0} years of age."), App.Settings.MinimumCareerAgeThreshold) },
										{ "errorDateOfBirthLimitMax", CodeLocalise("DOBLimitMax.ErrorMessage", "You must not be more than 99 years of age.") },
										{ "errorEmploymentStatusNotUnemployed", CodeLocalise("MilitaryEmploymentStatus.ErrorMessage", "Transitioning Service Member must not be unemployed") },
										{ "errorAlienRegistrationNumberRequired", CodeLocalise("AlienRegistrationNumber.RequiredErrorMessage", "Alien registration number is required") },
										{ "errorAlienRegistrationNumber", CodeLocalise("AlienRegistrationNumber.ErrorMessage", "Alien registration number is invalid") },
										{ "errorAlienExpirationDateRequired", CodeLocalise("AlienExpirationDate.RequiredErrorMessage", "Alien expiration date is required") },
										{ "errorAlienExpirationDate", CodeLocalise("AlienExpirationDate.ErrorMessage", "Alien expiration date must be a future date") },
										{ "errorEmploymentIsOtherJob", CodeLocalise("EmploymentIsCurrentJob.ErrorMessage", "Employment status conflicting with the job where you are currently working") },
										{ "errorEmploymentStatusRequired", CodeLocalise("EmploymentStatus.RequiredErrorMessage", "Employment status is required") },
										{ "errorEmploymentStatusMismatch", CodeLocalise("EmploymentStatusMismatch.ErrorMessage", "Employment status does not match with veteran employment status") },
										{ "disabilityStatusDisabled", DisabilityStatus.Disabled.ToString() },
										{ "veteranEraTransitioningServiceMember", VeteranEra.TransitioningServiceMember.ToString() },
										{ "veteranEraOtherEligible", VeteranEra.OtherEligible.ToString() },
										{ "veteranEraOtherVet", VeteranEra.OtherVet.ToString() },
										{ "employmentStatusEmployed", EmploymentStatus.Employed.ToString() },
										{ "employmentStatusEmployedTerminationReceived", EmploymentStatus.EmployedTerminationReceived.ToString() },
										{ "employmentStatusUnemployed", EmploymentStatus.UnEmployed.ToString() },
										{ "isCurrentJob", IsCurrentJob },
										{ "errorTapAttendanceDateRequired", CodeLocalise("TapAttendenceDate.RequiredErrorMessage", "TAP course attended date is required") }, 
										{ "errorTapAttendanceDateFormat", CodeLocalise("TapAttendenceDate.DateFormat.ErrorMessage", "Valid TAP course attended date is required") },
										{ "errorTapAttendanceDateTooEarly", CodeLocalise("TapAttendanceDateTooEarly.ErrorMessage", "TAP course attended date cannot be more than 3 years ago") },
										{ "errorTapAttendanceDateTooEarInFutureTooEarly", CodeLocalise("TapAttendanceDateInFuture.ErrorMessage", "TAP course attended date cannot be in the future") },
										{ "TapAttendanceDateValidatorClientId", TapAttendanceDateValidator.ClientID },
										{ "SelectiveServiceRegistration", App.Settings.SelectiveServiceRegistration ? "True" : "False" },
										{ "MilitaryServiceMinimumAge", App.Settings.MilitaryServiceMinimumAge.ToString(CultureInfo.InvariantCulture) },
										{ "UnderAgeJobSeekerRestrictionThreshold", App.Settings.MinimumCareerAgeThreshold.ToString() },
										{ "EnableOptionalAlienRegExpiryDate", App.Settings.EnableOptionalAlienRegExpiryDate ? "1" : "0" }
                                        //{"errorMonthlyIncomeLimit",CodeLocalise("errorMonthlyIncomeLimit.ErrorMessage","Monthly earnings are between: $850.00 to $29,166.50.")}

				          };
			return nvc;
		}

    /// <summary>
    /// Saves this instance.
    /// </summary>
    /// <returns></returns>
	  public bool Save()
		{
			return SaveModel();
		}
      protected void ddlDisabilityStatus_SelectedIndexChanged(object sender, EventArgs e)
      {
          if (ddlDisabilityStatus.SelectedValue == "Disabled")
              cblDisabledCategory.Visible = true;
          else
              cblDisabledCategory.Visible = false;
      }
  }
}
