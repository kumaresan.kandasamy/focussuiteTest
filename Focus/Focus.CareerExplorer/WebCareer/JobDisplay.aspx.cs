﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Text.RegularExpressions;
using Focus.Common.Extensions;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
	public partial class JobDisplay : System.Web.UI.Page
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
        {
            if ("netscape|gecko|firefox|opera".IndexOf(Request.Browser.Browser.ToLower()) >= 0)
            {
                ClientTarget = "Uplevel";
            }

            //FVN-6443 - Accessibility Issues 508 compliance
            //if (Request.Params.Get("postingid") != null)
            //    this.Page.Title = this.Page.Title + "_" + Request.Params.Get("postingid");
            
            
            string Posting = OldApp_RefactorIfFound.GetSessionValue<string>("Career:DisplayPosting:" + Request.Params.Get("postingid"));
            
            
            if (Posting != null)
            {
                Posting = Posting.Replace("<html>", "<html lang='en'> #HEADERCONTENT#");
                Posting = Posting.Replace("#HEADERCONTENT#", "<head><title>Job Posting - " + (Request.Params.Get("postingid")!= null ? Request.Params.Get("postingid") : new Random().Next().ToString()) + "</title></head>");
                Posting = Posting.Replace("<body>", "<body><h1 style='display:none'>Description</h1>");
                Response.Write(Posting.AddAltTagWithValueForImg("Spidered job images"));
            }
        }
	}
}
