﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SearchPostingsEducation.aspx.cs" Inherits="Focus.CareerExplorer.WebCareer.SearchPostingsEducation" %>

<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="../Controls/ResumeSearchLink.ascx" TagName="ResumeSearchLink" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<div class="FocusCareer">
		<div><uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" /></div>
		<div class="landingPageNavigation landingPageJobSearch">
			<div class="lpItem">
				<div class="lpItemHeaderBlue lpJobSearch">
					<focus:LocalisedLabel runat="server" ID="lblRecommendationsBasedOn" LocalisationKey="RecommendationsBasedOn.Label" DefaultText="See job recommendations based on" CssClass="lpItemHeaderText"/>
				</div>
				<div class="lpItemContentPanelBlue">
					<div class="lpItemContentLinkRowBlue">
						<asp:LinkButton ID="lnkProgramOfStudy" runat="server" CssClass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="ProgramOfStudy">My program of study</asp:LinkButton>
					</div>
				</div>
				<div class="lpItemContentPanelBlue"  id="MyResumeDiv" runat="server">
					<div class="lpItemContentLinkRowBlue">
						<asp:LinkButton ID="Linkbutton2" runat="server" CssClass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="Resume">My resume</asp:LinkButton>
            <focus:LocalisedLabel runat="server" ID="JobsBasedOnResumeHintLabel" LocalisationKey="JobsBasedOnResumeHint.Label" DefaultText="we'll help you build or upload your resume first" CssClass="lpItemContentLinkBlueHint"/>
					</div>
				</div>
				<div class="lpItemContentPanelBlue" id="MyResumeAndProgramOfStudyDiv" runat="server">
					<div class="lpItemContentLinkRowBlue">
						<asp:LinkButton ID="Linkbutton4" runat="server" CssClass="lpItemContentLinkText" OnCommand="MatchingAction_Command" CommandName="ResumeAndProgramOfStudy">My resume and my program of study</asp:LinkButton>
					</div>
				</div>
			</div>
			<div class="lpItem">
				<div class="lpItemHeaderBlue lpJobSearch">
					<asp:LinkButton ID="lnkSearchJobs" runat="server" CssClass="lpItemContentLinkText lpItemHeaderText" OnClick="lnkSearchJobs_Click"><focus:LocalisedLabel runat="server" ID="SearchJobsLabel" LocalisationKey="SearchJobs.Label" DefaultText="Search for jobs" RenderOuterSpan="False"/></asp:LinkButton>
				</div>
			</div>
			<div class="lpItem">
				<div class="lpItemHeaderBlue lpJobSearch">
					<asp:LinkButton ID="lnkSearchInternships" runat="server" CssClass="lpItemContentLinkText lpItemHeaderText" OnClick="lnkSearchInternships_Click"><focus:LocalisedLabel runat="server" ID="SearchInternshipsLabel" LocalisationKey="SearchInternships.Label" DefaultText="Search for internship opportunities" RenderOuterSpan="False"/></asp:LinkButton>
				</div>
			</div>
		</div>
	</div>
</asp:Content>