﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.CareerExplorer.WebAuth.Controls;
using Focus.Common;
using Focus.Common.Controls.Server;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
	public partial class Home : CareerExplorerPageBase
	{
		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (App.IsAnonymousAccess())
				Response.Redirect(UrlBuilder.JobSearchCriteria());
            if (App.GetSessionValue<bool>("CreateEditResumePopup"))
            {
                CreateEditResume.ShowPopUp();
                App.SetSessionValue<bool>("CreateEditResumePopup", false);
            }

			if (!IsPostBack)
			{
				if (App.GuideToResume())
					Response.Redirect(UrlBuilder.YourResume());

				var actionType = CareerHomepageActionType.None;
				var key = "";

				if (Page.RouteData.Values.ContainsKey("pin"))
				{
					key = Page.RouteData.Values["pin"].ToString();
					actionType = CareerHomepageActionType.PinRegistration;
				}

				if (Page.Is(UrlBuilder.SSOCareerRegistration()))
					actionType = CareerHomepageActionType.SSORegistration;

				if (Page.Is(UrlBuilder.ExternalValidation()))
					actionType = CareerHomepageActionType.ExternalValidation;

				ApplyTheme();
				LocaliseUI();

				if (!App.User.IsAuthenticated)
				{
					var isTimeout = App.GetCookieValue("CareerExplore:Timeout");
					if (isTimeout.IsNotNullOrEmpty())
						App.RemoveCookieValue("CareerExplore:Timeout");

					var login = (LogInOrRegister)Master.FindControl("MainLogIn");

					var showLogin = true;
					if (actionType == CareerHomepageActionType.ExternalValidation)
						showLogin = !ValidateExternalUser(login);

					if (showLogin)
					{
						var checkRedirect = App.GetCookieValue("CareerExplore:Redirect", versionCookie: true).IsNotNullOrEmpty();

						login.ShowLogIn(isTimeout == "1", null, checkRedirect);

						panLoginResume.Visible = false;
						panNoLoginResume.Visible = true;
					}
				}
				else
				{
					Bind();

					if (App.Settings.TalentModulePresent)
					{
						SetResumeCount();
						DefaultResumeLabel.Visible = true;
					}

					if (!App.User.IsMigrated && actionType == CareerHomepageActionType.SSORegistration)
					{
						var login = (LogInOrRegister) Master.FindControl("MainLogIn");
						login.ShowSSORegistration();
					}
					else
					{
						DisplayJobSeekerSurvey();

						if (App.GetSessionValue("ReactivateRequestKey", false))
							Response.Redirect(UrlBuilder.ReactivateAccount());
					}
				}

				CreateOrUploadResumeLink.NavigateUrl = UrlBuilder.YourResume();
			}

			if (IsPostBack)
			{
				RegisterClientStartupScript("HomeScript");
				SetTitle(ListSearch.GetSavedSearchesCount());

				//if (App.GetCookieValue("AccountReactivation:Show", "").Equals("true", StringComparison.OrdinalIgnoreCase))
				//DisplayAccountReactivation();
			}

			SetupSideBar();
			ReOrderSideBar();
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			//To enable backspace button for masking in chrome
			if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MaskedEditFix"))
				Page.ClientScript.RegisterStartupScript(GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("~/Assets/Scripts/MaskedEditFix.js")));

			base.OnInit(e);
			MyResumeList.LinkNameClicked += MyResumeList_linkNameclick;
			Master.ProgramOfStudyChanged += Master_ProgramOfStudyChanged;
		}

		/// <summary>
		/// Performs validation of the user
		/// </summary>
		/// <returns>Whether to show the login page as normal</returns>
		private bool ValidateExternalUser(LogInOrRegister login)
		{
			var values = Request.QueryString.AllKeys.ToDictionary(key => key, key => Request.QueryString[key]);

			var integrationUser = ServiceClientLocator.AuthenticationClient(App).ValidateExternalAuthentication(values);

			if (integrationUser.IsNotNull())
			{
				if (integrationUser.FocusUserName.IsNullOrEmpty())
					login.RegisterExternalUser(integrationUser);
				else
					login.ShowLogIn(userName: integrationUser.FocusUserName);

				panLoginResume.Visible = false;
				panNoLoginResume.Visible = false;

				return true;
			}

			return false;
		}

		/// <summary>
		/// Binds this instance.
		/// </summary>
		private void Bind()
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					var resumeInfo = ServiceClientLocator.ResumeClient(App).GetAllResumeInfo();
					if (resumeInfo != null && resumeInfo.Count > 0)
					{
						var resumeDictionary = resumeInfo.Where(r => r.CompletionStatus == ResumeCompletionStatuses.Completed).ToDictionary(x => x.ResumeId.ToString(), x => x.ResumeName);
						ddlResumeNames.BindDictionary(resumeDictionary, null, CodeLocalise("Global.PersonalTitle.TopDefault", null));
						ddlResumeNames.SelectValue(ServiceClientLocator.ResumeClient(App).GetDefaultResumeId().ToString(CultureInfo.InvariantCulture));

						panLoginResume.Visible = true;
						panNoLoginResume.Visible = false;

						CreateResumeLabel.Visible = false;
						CreateEditResumeLabel.Visible = true;
					}
					else
					{
						panLoginResume.Visible = false;
						panNoLoginResume.Visible = true;

						CreateResumeLabel.Visible = true;
						CreateEditResumeLabel.Visible = false;
					}

					PersonDto person = null;

					if (App.Settings.UnderAgeJobSeekerRestrictionThreshold > 0 && App.User.PersonId.HasValue)
					{
						person = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(App.User.PersonId.Value);
						if (person != null)
						{
							if (!person.Age.HasValue || (person.Age.Value < App.Settings.UnderAgeJobSeekerRestrictionThreshold))
							{
								RecentlySentMatchesPanel.Visible = false;
								ReferralPanel.Visible = false;
								ResumeViewCountContainer.Visible = false;
								DefaultResumeContainer.Visible = false;
							}
						}
					}

					var staticMessages = new List<string>();

					if (App.Settings.ShowSSNStatusMessage && App.User.PersonId.HasValue)
					{
						var ssn = person.IsNotNull() 
											? person.SocialSecurityNumber
											: ServiceClientLocator.AccountClient(App).GetSSN(App.User.PersonId.GetValueOrDefault(0));

						var ssnMessage = ssn.IsNotNullOrEmpty()
																? string.Format("SSN xxx-xx-{0} is currently attached to your account", ssn.Replace("-", "").Substring(5, 4))
																: string.Format("There is no SSN currently attached to your account");

						staticMessages.Add(string.Format("Your customer account ID is {0}<br />{1}", App.User.PersonId, ssnMessage));
					}

					if (staticMessages.IsNotNullOrEmpty())
					{
						StaticMessagesRepeater.DataSource = staticMessages;
						StaticMessagesRepeater.DataBind();
					}
					StaticMessagesRepeater.Visible = staticMessages.IsNotNullOrEmpty();
				}
				else
				{
					panLoginResume.Visible = false;
					panNoLoginResume.Visible = true;

					CreateResumeLabel.Visible = true;
					CreateEditResumeLabel.Visible = false;
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			FindJobRequired.ErrorMessage = CodeLocalise("FindJob.RequiredErrorMessage", "Search terms are required.");

			AdjustLocalisedLabel(ResearchCareerLabel,
														"Explorer.Label.ResearchStudy",
														"Research a program of study, job, #BUSINESS#:LOWER or skill",
														"Research a specific program of study, career or #BUSINESS#:LOWER");

			AdjustLocalisedLabel(AccordionHeadingReferralsLabel,
														"AccordionHeadingReferrals.Label",
														"Job application activity",
														"Jobs for which you have referral activity");

			CreateOrUploadResumeLink.Text = CodeLocalise("CreateOrUploadResumeLink.Text", "Create or upload a resume");
		}

		/// <summary>
		/// Adjusts the key and default text of localised label controls
		/// </summary>
		/// <param name="label">The label to adjust</param>
		/// <param name="key">The localisation key</param>
		/// <param name="educationText">Default text for EDU</param>
		/// <param name="workforceText">Default text for WF</param>
		private void AdjustLocalisedLabel(LocalisedLabel label, string key, string educationText, string workforceText)
		{
			label.LocalisationKey = App.Settings.Theme == FocusThemes.Education
				? string.Concat(key, ".Education")
				: key;

			label.DefaultText = App.Settings.Theme == FocusThemes.Education
				? educationText
				: workforceText;
		}

		/// <summary>
		/// Displays the job seeker survey (only if required conditions are met)
		/// </summary>
		private void DisplayJobSeekerSurvey()
		{
			if (App.GetCookieValue("JobSeekerSurvey:Show", "").Equals("true", StringComparison.OrdinalIgnoreCase))
				JobSeekerSurvey.ConditionalShow();
		}

		/// <summary>
		/// Handles the Click event of the btnFindJob control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void btnFindJob_Click(object sender, EventArgs e)
		{
			if (FindAJobTextBox.Text.Trim() != "")
			{
				var jobLocationCriteria = new JobLocationCriteria();

				if (!App.Settings.NationWideInstance)
					jobLocationCriteria.Area = new AreaCriteria { StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault() };

				var searchCriteria = new SearchCriteria
				{
					KeywordCriteria = new KeywordCriteria
					{
						KeywordText = FindAJobTextBox.Text.Trim(),
						SearchLocation = PostingKeywordScopes.Anywhere
					},
					PostingAgeCriteria = new PostingAgeCriteria { PostingAgeInDays = 30 },
					JobLocationCriteria = jobLocationCriteria
				};

				App.SetSessionValue("Career:SearchCriteria", searchCriteria);
				Response.RedirectToRoute("JobSearchResults");
			}
		}

		/// <summary>
		/// Handles the Click event of the lbSearchForJobs control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lbSearchForJobs_Click(object sender, EventArgs e)
		{
			App.RemoveSessionValue("Career:SearchCriteria");
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
		}

		/// <summary>
		/// Handles the SelectedIndexChanged event of the ddlResumeNames control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void ddlResumeNames_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (App.User.IsAuthenticated)
				{
					var resumeId = ddlResumeNames.SelectedValueToLong();

					if (resumeId.IsNotNull() && resumeId > 0)
						if (ServiceClientLocator.ResumeClient(App).SetDefaultResume(resumeId))
						{
							Utilities.UpdateUserContextFromDefaultResume();
							MasterPage.ShowModalAlert(AlertTypes.Info, "'" + ddlResumeNames.SelectedItem.Text + "' is saved as default resume.", "Default resume saved");
						}
						else
							MasterPage.ShowError(AlertTypes.Error, "Error in setting default resume.");
				}
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Handles the SearchesRetrievd event of the ListSearch control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void ListSearch_SearchesRetrievd(object sender, CommandEventArgs e)
		{
			SetTitle(Convert.ToInt32(e.CommandArgument));
		}

		/// <summary>
		/// Handles the linkNameclick event of the MyResumeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void MyResumeList_linkNameclick(object sender, EventArgs e)
		{
			var lbtnName = (LinkButton)sender;
			App.SetSessionValue("Career:ResumeID", lbtnName.CommandArgument.ToLong());
			Response.RedirectToRoute(lbtnName.CommandName);
		}


		/// <summary>
		/// Handles the ProgramOfStudyChanged event of the Master control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
		protected void Master_ProgramOfStudyChanged(object sender, CommandEventArgs e)
		{
			ListSearch.ReloadResults();
		}

		/// <summary>
		/// Sets the title.
		/// </summary>
		/// <param name="savedSearchCount">The saved search count.</param>
		private void SetTitle(int savedSearchCount)
		{
			if (savedSearchCount == 0 && App.Settings.Theme == FocusThemes.Education)
			{
				AccordionHeading1Label.LocalisationKey = "NoSavedSearches.Label";
				AccordionHeading1Label.DefaultText = "Job search results for your program of study";
			}
			else
			{
				AccordionHeading1Label.LocalisationKey = "AccordionHeading1.Label";
				AccordionHeading1Label.DefaultText = "New matches for your saved searches";
			}
		}

		/// <summary>
		/// Sets the resume count.
		/// </summary>
		private void SetResumeCount()
		{
			var viewCount = ServiceClientLocator.ResumeClient(App).GetCountOfResumesRecentlyViewedByEmployees();

			ResumeViewCount.Text = viewCount == 1 ? CodeLocalise("ResumeViewCountSingular.Text", "1 #BUSINESS#:LOWER has viewed your resume in the past 90 days.")
																						: CodeLocalise("ResumeViewCountPural.Text", "{0} #BUSINESSES#:LOWER have viewed your resume in the past 90 days.", viewCount);
		}

		/// <summary>
		/// Res the order side bar.
		/// </summary>
		private void ReOrderSideBar()
		{
			sideBarExplore.Visible = false;
			sideBarResearch.Visible = false;
			sideBarStudy.Visible = false;
			sideBarSchool.Visible = false;
			sideBarExperience.Visible = false;

			var sectionOrder = App.Settings.ExplorerSectionOrder.Split('|');
			var controls = sectionOrder.Select(section => ExplorerLinksPanel.FindControl(string.Concat("sideBar", section)));

			// Re-order the controls based on config
			foreach (var control in controls)
			{
				control.Visible = true;
				ExplorerLinksPanel.Controls.Add(control);
			}
		}

		/// <summary>
		/// Hide/Show controls based on the theme
		/// </summary>
		private void ApplyTheme()
		{
			ResumeSearchLink.Visible = App.Settings.Theme == FocusThemes.Education;

			// only show Explorer type sidebar links if we're in Workforce and CareerExplorer modes
			ExplorerLinksPanel.Visible = (App.Settings.Theme == FocusThemes.Workforce && App.Settings.Module == FocusModules.CareerExplorer);
		}

		/// <summary>
		/// Setups the side bar links.
		/// </summary>
		private void SetupSideBar()
		{
			// Set URLs for the controls
			sideBarSchool.Attributes.Add("onclick", "window.location='" + UrlBuilder.Explore("school") + "';");
			sideBarExplore.Attributes.Add("onclick", "window.location='" + UrlBuilder.Explore("explore") + "';");
			sideBarResearch.Attributes.Add("onclick", "window.location='" + UrlBuilder.Explore("research") + "';");
			sideBarStudy.Attributes.Add("onclick", "window.location='" + UrlBuilder.Explore("study") + "';");
			sideBarExperience.Attributes.Add("onclick", "window.location='" + UrlBuilder.Explore("experience") + "';");

			sideBarSchool.Attributes.Add("onkeypress", "window.location='" + UrlBuilder.Explore("school") + "';");
			sideBarExplore.Attributes.Add("onkeypress", "window.location='" + UrlBuilder.Explore("explore") + "';");
			sideBarResearch.Attributes.Add("onkeypress", "window.location='" + UrlBuilder.Explore("research") + "';");
			sideBarStudy.Attributes.Add("onkeypress", "window.location='" + UrlBuilder.Explore("study") + "';");
			sideBarExperience.Attributes.Add("onkeypress", "window.location='" + UrlBuilder.Explore("experience") + "';");
		}
	}
}
