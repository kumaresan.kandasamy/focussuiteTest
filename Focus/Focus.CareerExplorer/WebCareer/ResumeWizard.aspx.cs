﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Focus.Common;
using Focus.Core.Views;
using Framework.Core;
using Framework.Logging;

using Focus.CareerExplorer.Code;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
  public partial class ResumeWizard : CareerExplorerPageBase
	{
    public string lsTab;
    public static long UserId;
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
			var lsTab = Page.RouteData.Values["Tab"] as string;

            /** 
             * KRP - Reverting this change due to inconsistent issue
             * Redirecting happens when save and move to next step is clicked
             * AIM 17 May 2017 - FVN-4962
             * To avoid the storing of another JS's resume during sequential access of 2 JS with resume wizard 
             
            if (!IsPostBack)
            {
                UserId = App.User.UserId;
            }

            if (IsPostBack)
            {
                if (UserId == App.User.UserId)
                {
                    RegisterClientStartupScript("ResumeWizardScript");
                }
                else
                {
                    Response.Redirect(UrlBuilder.Home());
                }
            }*/
                
			if (IsPostBack)
				RegisterClientStartupScript("ResumeWizardScript");
			else
			{
				LocaliseUI();
        // Ensure the currently logged on user is asscociated with the current resume
			  if (ServiceClientLocator.ResumeClient(App)
			    .GetResumeNames()
			    .ContainsKey(App.GetSessionValue<long?>("Career:ResumeID").ToString()))
			  {
			    LoadResume();
			  }
      
				SetActiveTab(lsTab);
			}
    }

    /// <summary>
    /// Handles the PreRender event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
      DeleteResumeLinkButton.Visible = App.GetSessionValue<long?>("Career:ResumeID").IsNotNull();
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    protected void Page_Init(object sender, EventArgs e)
    {
      if (!Page.ClientScript.IsStartupScriptRegistered(GetType(), "MaskedEditFix"))
      {
        Page.ClientScript.RegisterStartupScript(GetType(), "MaskedEditFix", String.Format("<script type='text/javascript' src='{0}'></script>", Page.ResolveUrl("~/Assets/Scripts/MaskedEditFix.js")));
      }
    }

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      ResumeTitleRequired.ErrorMessage = CodeLocalise("ResumeTitle.RequiredErrorMessage", "Resume title is required");
    }

    protected override void OnInit(EventArgs e)
    {
      CheckSession = true;
			RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education);

      base.OnInit(e);

      # region Save Resume Titles

      //ContactTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
      //EducationTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
      //SummaryTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
      //AddInsTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
      //ProfileTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
      //PreferencesTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
      //ReviewTab.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);
      //WorkHistoryTabs.SaveResumeTitle += new EventHandler(SaveResumeTitleButton_Click);

      #endregion
    }

    #region Delete resume

    /// <summary>
    /// Handles the Click event of the DeleteResumeLinkButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void DeleteResumeLinkButton_Click(object sender, EventArgs e)
    {
      var resumeId = App.GetSessionValue<long?>("Career:ResumeID");
      if (resumeId.IsNull())
        return;

      DeleteResumeModal.Visible = true;
      DeleteResumeModal.Show(resumeId.Value);
    }

    /// <summary>
    /// Fires when the resume has been deleted
    /// </summary>
    /// <param name="sender">Delete Resume control</param>
    /// <param name="eventargs">Event arguments containing details of the deleted resume</param>
    protected void DeleteResumeModal_OnDeleteCommand(object sender, CommandEventArgs eventargs)
    {
      var resumeId = eventargs.CommandArgument.AsLong();
      if (resumeId > 0)
      {
        if (eventargs.CommandName != "DELETED")
          return;

        App.RemoveSessionValue("Career:Resume");

        Response.RedirectToRoute("YourResume");
      }
      else
      {
        MasterPage.ShowError(AlertTypes.Error, "You can perform this operation only on saved resume.");
      }
    }

    /// <summary>
    /// Handles the Click event of the SaveResumeTitleButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    public void SaveResumeTitleButton_Click(object sender, EventArgs e)
    {
      var resumeName = ResumeTitleTextBox.TextTrimmed();
      var needSave = true;

      if (resumeName.IsNotNullOrEmpty())
      {
        var userId = (App.User.IsAuthenticated) ? App.User.UserId : (long?)null;

        if (userId.HasValue)
        {
          var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");

          if (currentResume.IsNotNull())
          {
            if (currentResume.ResumeMetaInfo.IsNotNull())
            {
              if (currentResume.ResumeMetaInfo.ResumeName == resumeName)
                needSave = false;
              else
                currentResume.ResumeMetaInfo.ResumeName = resumeName;
            }
            else
              currentResume.ResumeMetaInfo = new ResumeEnvelope { ResumeName = resumeName };
          }
          else
            currentResume = new ResumeModel { ResumeMetaInfo = new ResumeEnvelope { ResumeName = resumeName } };

          if (currentResume.ResumeContent.IsNull())
            currentResume.ResumeContent = new ResumeBody();

          if (needSave)
            try
            {
							
							
							var resume = ServiceClientLocator.ResumeClient(App).SaveResume(currentResume, true);

							var resumeId = resume.ResumeMetaInfo.ResumeId;

              if (currentResume.ResumeMetaInfo.ResumeId.IsNull())
                currentResume.ResumeMetaInfo.ResumeId = resumeId;
              App.SetSessionValue("Career:ResumeID", resumeId);
              App.SetSessionValue("Career:Resume", currentResume);

              //if (resumeId.IsNotNullOrEmpty())
              //  MasterPage.ShowError(AlertType.Info, "Resume title saved successfully.");
              //else
              //  MasterPage.ShowError(AlertType.Error, "Error in saving resume title.");
            }
            catch (ApplicationException ex)
            {
              Logger.Error(ex.Message);
              //MasterPage.ShowError(AlertType.Error, ex.Message);
            }
        }
      }
    }

    #endregion

    /// <summary>
    /// Loads the resume.
    /// </summary>
    /// <returns></returns>
    public bool LoadResume()
    {
      var resumeId = App.GetSessionValue<long?>("Career:ResumeID");
      var loadSuccessful = false;

			if (resumeId.IsNull())
      {
	      var resumeCount = App.User.IsAuthenticated ? ServiceClientLocator.ResumeClient(App).GetResumeCount() : 0;
				if (App.Settings.MaximumAllowedResumeCount != 0 && resumeCount >= App.Settings.MaximumAllowedResumeCount)
				{
					if (App.User.PersonId != null)
						resumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId(App.User.PersonId.Value);
					else
					{
						Response.Redirect(UrlBuilder.Default(), false);
						return false;
					}
				}
      }

      if (resumeId.IsNotNull())
      {
        try
        {
          var resume = App.GetSessionValue<ResumeModel>("Career:Resume");

          if (resumeId.IsNotNull() || (resume.IsNotNull() && resume.ResumeMetaInfo.IsNotNull() && resume.ResumeMetaInfo.ResumeId != resumeId))
          {
            App.RemoveSessionValue("Career:ResumeID");
            App.RemoveSessionValue("Career:Resume");
            resume = null;
          }

          if (resume.IsNull())
          {
            resume = ServiceClientLocator.ResumeClient(App).GetResume(resumeId);
            if (resume.IsNotNull())
            {
              if (resume.ResumeMetaInfo.ResumeId.IsNotNull())
                resume.ResumeMetaInfo.ResumeId = resumeId;

              var isDuplicateResume = App.GetSessionValue("Career:DuplicateResume", false);
              if (isDuplicateResume)
              {
                resume.ResumeMetaInfo.ResumeId = null;
                resume.ResumeMetaInfo.ResumeName = null;
								resume.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.None;
								resume.ResumeMetaInfo.ResumeCreationMethod = ResumeCreationMethod.BuildWizard;
								resume.ResumeMetaInfo.ResumeStatus = ResumeStatuses.Active;
              	resume.ResumeMetaInfo.ResumeVersion = 0;

								App.RemoveSessionValue("Career:DuplicateResume");
              }
              else
                App.SetSessionValue("Career:ResumeID", resume.ResumeMetaInfo.ResumeId);

              App.SetSessionValue("Career:Resume", resume);
            }

          }

          if (resume.IsNotNull() && resume.ResumeMetaInfo.IsNotNull())
            ResumeTitleTextBox.Text = resume.ResumeMetaInfo.ResumeName;
          loadSuccessful = true;

        }
        catch (ApplicationException ex)
        {
          MasterPage.ShowError(AlertTypes.Error, ex.Message);
        }
      }

      return loadSuccessful;
    }

    private string GetCurrentTab(ResumeCompletionStatuses completionStatus)
    {
      if ((completionStatus & ResumeCompletionStatuses.WorkHistory) != ResumeCompletionStatuses.WorkHistory)
        return "workhistory";

      if ((completionStatus & ResumeCompletionStatuses.Contact) != ResumeCompletionStatuses.Contact)
        return "contact";

      if ((completionStatus & ResumeCompletionStatuses.Education) != ResumeCompletionStatuses.Education)
        return "education";

      if ((completionStatus & ResumeCompletionStatuses.Summary) != ResumeCompletionStatuses.Summary)
        return "summary";

      if ((completionStatus & ResumeCompletionStatuses.Options) != ResumeCompletionStatuses.Options)
        return "options";

      if ((completionStatus & ResumeCompletionStatuses.Profile) != ResumeCompletionStatuses.Profile)
        return "profile";

      if ((completionStatus & ResumeCompletionStatuses.Preferences) != ResumeCompletionStatuses.Preferences)
        return "preferences";

      return "review";
    }

    public void SetCurrentTab(string tabName)
    {
      switch (tabName.ToLower())
      {
        case "workhistory":
          ResumeNavigationTabs.SelectedNavigationTab = "WORKHISTORY";
          WorkHistoryTabs.Visible = true;
          break;

        case "contact":
          ResumeNavigationTabs.SelectedNavigationTab = "CONTACT";
          ContactTab.Visible = true;
          if (!IsPostBack)
            ContactTab.BindStep();
          break;

        case "education":
          ResumeNavigationTabs.SelectedNavigationTab = "EDUCATION";
          EducationTab.Visible = true;
          if (!IsPostBack)
            EducationTab.BindStep();
          break;

        case "summary":
          ResumeNavigationTabs.SelectedNavigationTab = "SUMMARY";
          SummaryTab.Visible = true;
          if (!IsPostBack)
            SummaryTab.BindStep();
          break;

        case "options":
          ResumeNavigationTabs.SelectedNavigationTab = "OPTIONS";
          OptionsTab.Visible = true;
          if (!IsPostBack)
						OptionsTab.BindStep();
          break;

        case "profile":
          ResumeNavigationTabs.SelectedNavigationTab = "PROFILE";
          ProfileTab.Visible = true;
          if (!IsPostBack)
            ProfileTab.BindStep();
          break;

        case "preferences":
          ResumeNavigationTabs.SelectedNavigationTab = "PREFERENCES";
          PreferencesTab.Visible = true;
          if (!IsPostBack)
            PreferencesTab.BindStep();
          break;

				case "review":
          ResumeNavigationTabs.SelectedNavigationTab = "REVIEW";
          ReviewTab.Visible = true;
          if (!IsPostBack)
            ReviewTab.BindStep();
          break;
      }
    }

    /// <summary>
    /// Handles the Click event of the linkPreviewResume control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void linkPreviewResume_Click(object sender, EventArgs e)
    {
      ResumePreviewPopup.BindStep();
    }

    /// <summary>
    /// Sets the active tab.
    /// </summary>
    /// <param name="lsTab">The ls tab.</param>
		private void SetActiveTab(string lsTab)
		{
			var currentResume = App.GetSessionValue<ResumeModel>("Career:Resume");
			var actualStatus = 0;

			if (currentResume.IsNotNull())
			{
				actualStatus = (int)currentResume.ResumeMetaInfo.CompletionStatus;
				actualStatus = (actualStatus << 1) + 1;
			}

			var requiredStatus = lsTab.AsEnum<ResumeCompletionStatuses>();

			if (requiredStatus.HasValue && actualStatus > 0)
      {
	      var finalStatus = actualStatus > (int) requiredStatus ? (int) requiredStatus : actualStatus;
				lsTab = GetExpectedTab(finalStatus);
      }
      else if (lsTab.IsNotNullOrEmpty() && lsTab.Equals("new") && actualStatus > 3)
        lsTab = "contact";
      else if (actualStatus > 0)
        lsTab = GetExpectedTab(actualStatus);
      else
        lsTab = "workhistory";

			SetCurrentTab(lsTab);
		}

    /// <summary>
    /// Gets the expected tab.
    /// </summary>
    /// <param name="tempStatus">The temp status.</param>
    /// <returns></returns>
		private string GetExpectedTab(int tempStatus)
		{
			var lsTab = string.Empty;
			switch (tempStatus)
			{
				case (int)GuidedPath.UptoWorkHistory:
					lsTab = "workhistory";
					break;

				case (int)GuidedPath.UptoContact:
				case (int)ResumeCompletionStatuses.Contact:
					lsTab = "contact";
					break;

				case (int)GuidedPath.UptoEducation:
				case (int)ResumeCompletionStatuses.Education:
					lsTab = "education";
					break;

				case (int)GuidedPath.UptoSummary:
				case (int)ResumeCompletionStatuses.Summary:
					lsTab = "summary";
					break;

				case (int)GuidedPath.UptoOptions:
				case (int)ResumeCompletionStatuses.Options:
					lsTab = "options";
					break;

				case (int)GuidedPath.UptoProfile:
				case (int)ResumeCompletionStatuses.Profile:
					lsTab = App.Settings.HideResumeProfile ? "options" : "profile";
					break;

				case (int)GuidedPath.UptoPreferences:
				case (int)ResumeCompletionStatuses.Preferences:
					lsTab = "preferences";
					break;

				default:
					lsTab = "review";
					break;
			}

			return lsTab;
		}
  }
}
