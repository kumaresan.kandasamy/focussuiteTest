﻿<%@ Page Title="Resume wizard"  Language="C#" AutoEventWireup="True" MasterPageFile="~/Site.Master" CodeBehind="ResumeWizard.aspx.cs"
	Inherits="Focus.CareerExplorer.WebCareer.ResumeWizard" EnableEventValidation="false" %>

<%@ Register Src="../Controls/ResumeSearchLink.ascx" TagName="ResumeSearchLink" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/ResumeNavigationTabs.ascx" TagName="ResumeNavigationTabs" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/WorkHistory/WorkHistory.ascx" TagName="WorkHistory" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/Contact.ascx" TagName="Contact" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/Education.ascx" TagName="Education" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/Summary.ascx" TagName="Summary" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/Options.ascx" TagName="Options" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/Profile.ascx" TagName="Profile" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/Preferences.ascx" TagName="Preferences" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/Review.ascx" TagName="Review" TagPrefix="uc" %>
<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/ResumePreview.ascx" TagName="ResumePreview" TagPrefix="uc" %>
<%@ Register Src="../Controls/DeleteResumeModal.ascx" TagName="DeleteResumeModal" TagPrefix="uc" %>
<%@ Import namespace="Focus.Common.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="<%= UrlHelper.GetCacheBusterUrl("~/Assets/Scripts/ScrollableSkillsAccordion.js")%>" type="text/javascript"></script>
  <script language="javascript" type="text/javascript">
    // Ensure new accordion is not broken by update panel being refreshed
      $(document).ready(function () {
      	Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetupAccordionNew);
      	Sys.Application.add_load(ResumeWizard_PageLoad);
        Sys.Application.add_load(menuToggle);

      	function ResumeWizard_PageLoad(sender, args) {
      	    $("#SaveMoveToNextStepButton").prop('disabled', false);
      	    $("#SaveMoveToNextStepButton1").prop('disabled', false);
      	    $("#SaveMoveToNextStepButton").removeClass('greyedOut');
      	    $("#SaveMoveToNextStepButton1").removeClass('greyedOut');
      	}

        /** KRP This script was not executing after Ajax Call. Moved this part
        * from ResumeWizardNavigationTabs.aspx*/
        function menuToggle() {
              var pull = $('#pull');
              var menu = $('#RCPNavigationTop ul');
              var menuHeight = menu.height();

              var listItems = menu.find("li").each(function () {
                  var item = $(this);
                  if (item.hasClass("selected")) {
                      pull.text(item.text());
                  }
                  // rest of code.
              });

              $(pull).on('click', function (e) {
                  e.preventDefault();
                  menu.slideToggle();
              });

              $(window).resize(function () {
                  var w = $(window).width();
                  if (w > 768 && menu.is(':hidden')) {
                      menu.removeAttr('style');
                  }
              });
          }
      });
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel runat="server" ID="UpdatePanelResumeWizard" UpdateMode="Always">
		<ContentTemplate>
			<div class="FocusCareer">
				<uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" />
				<h1><focus:LocalisedLabel runat="server" ID="CreateResumeHeadingLabel" RenderOuterSpan="False" LocalisationKey="CreateResumeHeading.Label" DefaultText="Create a resume"/></h1>
				<div class="dataInputRow defaultResumeActions">
					<div class="col-sm-2 col-md-2"><focus:LocalisedLabel runat="server" ID="ResumeTitleLabel" AssociatedControlID="ResumeTitleTextBox" CssClass="dataInputLabel requiredData" LocalisationKey="ResumeTitle.Label" DefaultText="Resume title"/></div>
					<div class="dataInputField col-sm-10 col-md-10">
						<div class="col-sm-6 col-md-5">
						    <%= HtmlInFieldLabel("ResumeTitleTextBox", "ResumeTitleTextBox.Label", "Enter resume title", 210)%>
						<asp:TextBox ID="ResumeTitleTextBox" runat="server" ClientIDMode="Static" Width="210px" MaxLength="30" />
						<asp:Button ID="SaveResumeTitleButton" Text="Save" runat="server" class="buttonLevel2" OnClick="SaveResumeTitleButton_Click" ValidationGroup="ResumeTitle" />
                        <br/>
                        <asp:RequiredFieldValidator ID="ResumeTitleRequired" runat="server" ControlToValidate="ResumeTitleTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="ResumeTitle" />
						</div>
						<div class="col-sm-6 col-md-7" style="padding-top:10px"><asp:LinkButton ID="linkPreviewResume" style="margin-left:0px" runat="server" OnClick="linkPreviewResume_Click">
							<focus:LocalisedLabel runat="server" ID="PreviewResumeLabel" RenderOuterSpan="False" LocalisationKey="PreviewResume.LinkText" DefaultText="Preview resume"/>
						</asp:LinkButton>
						<img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%=HtmlLocalise("ToolTip1.Label", "You can preview your resume before filling out your demographic information and communication preferences.") %>"/>
						<asp:LinkButton ID="DeleteResumeLinkButton" OnClick="DeleteResumeLinkButton_Click" runat="server">
							<focus:LocalisedLabel runat="server" ID="DeleteResumeLabel" RenderOuterSpan="False" LocalisationKey="DeleteResume.LinkText" DefaultText="Delete resume"/>
						</asp:LinkButton>
						</div>
					</div>
				</div>
				<div id="RCPNavigation">
					<uc:ResumeNavigationTabs ID="ResumeNavigationTabs" runat="server" />
				</div>
			</div>
			<%--WORK HISTORY--%>
			<uc:WorkHistory ID="WorkHistoryTabs" runat="server" Visible="false" />
			<%--CONTACT--%>
			<uc:Contact ID="ContactTab" runat="server" Visible="false" />
			<%--EDUCATION--%>
			<uc:Education ID="EducationTab" runat="server" Visible="false" />
			<%--SUMMARY--%>
			<uc:Summary ID="SummaryTab" runat="server" Visible="false" />
			<%--ADDINS--%>
			<uc:Options ID="OptionsTab" runat="server" Visible="false" />
			<%--PROFILE--%>
			<uc:Profile ID="ProfileTab" runat="server" Visible="false" />
			<%--PREFERENCES--%>
			<uc:Preferences ID="PreferencesTab" runat="server" Visible="false" />
			<%--REVIEW--%>
			<uc:Review ID="ReviewTab" runat="server" Visible="false" />
			<uc:ResumePreview ID="ResumePreviewPopup" runat="server" ClientIDMode="Static" />
      <uc:DeleteResumeModal ID="DeleteResumeModal" runat="server" Visible="False" OnDeleteCommand="DeleteResumeModal_OnDeleteCommand" />
		</ContentTemplate>
	</asp:UpdatePanel>
    
<script type="text/javascript">

    function CheckOccupationsMH(sender, args) {
        var radioChecked = false;
        $('#rblOccupations input').each(function () {
            if ($(this).is(':checked'))
                radioChecked = true;
        });
        if (!radioChecked) {
            args.IsValid = false;
        }
    }
    function CheckSpecialTitleNotFoundMH(sender, args) {
        var cnt = 0;
        $('#cblSpecialTitle input').each(function () {
            if ($(this).is(':checked'))
                cnt++;
        });
        if (cnt == 0) {
            args.IsValid = false;
        }
    }


    function CheckSpecialTitleLimitMH(sender, args) {
        var cnt = 0;
        $('#cblSpecialTitle input').each(function () {
            if ($(this).is(':checked'))
                cnt++;
        });
        if (cnt > 2) {
            args.IsValid = false;
        }
    }

    function CheckMOCTitleMH(sender, args) {
        if ($('#hiddenSelectedMOCTitle').val() === '') {
            args.IsValid = false;
        }
    }

    function SelectedMOCTitleMH(mocDetail) {
        $('#hiddenSelectedMOCTitle').val(mocDetail);
    }
</script>
</asp:Content>