﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Security.Permissions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Core;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.CareerExplorer.Controls;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
  public partial class YourResume : CareerExplorerPageBase
	{
		/// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
			RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education);

      base.OnInit(e);
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			UploadResume.UploadResumeClick += new EventHandler(onUploadResumeClick);
			PasteTypeResume.PastedResumeClick += new EventHandler(onUploadResumeClick);

			ScriptManager.GetCurrent(Page).RegisterPostBackControl(UploadResume.FindControl("btnUploadResume"));
			ScriptManager.GetCurrent(Page).RegisterPostBackControl(MyResumeList);
            var resumeCount = App.User.IsAuthenticated && !App.GetSessionValue<bool>("ReactivateRequestKey") ? ServiceClientLocator.ResumeClient(App).GetResumeCount() : 0;
            if (App.Settings.MaximumAllowedResumeCount != 0 && resumeCount >= App.Settings.MaximumAllowedResumeCount)
		  {
        lblResumeLimitReached.Text = App.Settings.MaximumAllowedResumeCount > 1
          ? CodeLocalise("MaxResumeCountReached.Text",
                         "You can only save up to {0} resumes. Please delete older versions if you wish to continue to create a new one.",
                          App.Settings.MaximumAllowedResumeCount)
          : CodeLocalise("MaxResumeCountReachedOne.Text",
                         "You can only save up to 1 resume. Please delete your current resume if you wish to continue to create a new one.",
                          App.Settings.MaximumAllowedResumeCount);

		    lblResumeLimitReached.Visible = true;
		    UploadResume.Visible = PasteTypeResume.Visible = false;

				EditResumesPanel.CssClass = "left";
		  }
      else
      {
        lblResumeLimitReached.Visible = false;
        UploadResume.Visible = PasteTypeResume.Visible = true;
      }

			ResumeInfoLabel.Text = App.Settings.TalentModulePresent 
				? CodeLocalise("ResumeInfoLabel.DefaultText", 
											 "The more information you provide on your resume, the better the matches we can provide<br />When you're done, you'll be able to choose which information to display to #BUSINESSES#:LOWER.")
				: CodeLocalise("ResumeInfoLabel.NoTalentModulePresentText", 
											 "The more information you provide on your resume, the better the matches we can provide.<br />When you're done, you'll be able to choose which information to display when downloading" +
											 " or emailing your resume from the system, to apply to jobs.");

      if (IsPostBack)
      {
        RegisterClientStartupScript("YourResumeScript");
      }
      else
      {
        if (App.GetCookieValue("ShowIncompleteResumeConfirmation") == "1")
        {
          App.RemoveCookieValue("ShowIncompleteResumeConfirmation");

          ConfirmationModal.Show(
            "", 
            HtmlLocalise("Confirmation.Body", "During your last session you were in the middle of building your resume.  Do you want to continue where you left off?"), 
            HtmlLocalise("Confirmation.No", "No, return to home page"), 
            "", 
            "", 
            HtmlLocalise("Confirmation.Yes", "Yes, continue"), 
            HtmlLocalise("Global.OK", "OK"), 
            height: 50);
        }
      }

			SearchButton.Text = CodeLocalise("SearchButton.Text", "Search");

			if (!IsPostBack)
			{

				var resumes = ServiceClientLocator.ResumeClient(App).GetResumeNames();
				MyResumes.Resumes = resumes;

				ResumeNamesDropDown.Items.Clear();

				if (resumes != null && resumes.Count > 0)
				{
					var defaultResumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId();

					foreach (var resume in resumes)
					{
						var item = new ListItem { Value = resume.Key, Text = resume.Value };
						if (long.Parse(resume.Key) == defaultResumeId) item.Selected = true;
						ResumeNamesDropDown.Items.Add(item);
					}

					lblEditResume.Visible = true;

					PasteTypeResume.PastedRows = 14;
				}
				else
				{
					EditRow.Visible = false;

					SearchUsingThisResumePanel.Visible = false;

					PasteTypeResume.PastedRows = 4;
				}
			}


		}

    /// <summary>
    /// Ons the upload resume click.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void onUploadResumeClick(object sender, EventArgs e)
		{
			ResumeMissingInfoPopup.ShowPopup();
		}

    /// <summary>
    /// Handles the OkCommand event of the ConfirmationModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs" /> instance containing the event data.</param>
    public void ConfirmationModal_OkCommand(object sender, CommandEventArgs e)
    {
      Response.RedirectToRoute("ResumeWizard");
    }

		/// <summary>
		/// Searches the button c licked.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected void SearchButtonCLicked(object sender, EventArgs e)
		{
			var resumeId = ResumeNamesDropDown.SelectedValue;
			Response.Redirect(UrlBuilder.JobSearchResults(resumeId));
		}

		/// <summary>
		/// Handles the linkNameclick event of the MyResumeList control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void MyResumeList_linkNameclick(object sender, EventArgs e)
		{
			var lbtnName = (LinkButton)sender;
			App.SetSessionValue("Career:ResumeID", long.Parse(lbtnName.CommandArgument));
			Response.RedirectToRoute(lbtnName.CommandName);
		}
	}
}
