﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Services.Core;
using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;

using Framework.Core;
using Framework.DataAccess;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
	public partial class JobSearchCriteria : CareerExplorerPageBase
	{
		public string IsAuthenticated
		{
			get { return GetViewStateValue("SearchJob:IsAuthenticated", string.Empty); }
			set { SetViewStateValue("SearchJob:IsAuthenticated", value); }
		}

		/// <summary>
		/// Handles the Load event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
            this.Page.Title = "Job Search Criteria";

            if(cblEducationRequired != null)
                cblEducationRequired.Attributes.Add("role", "presentation");
            if (cblEmergingSectors != null)
                cblEmergingSectors.Attributes.Add("role", "presentation");
            if (cblDisabilityCategories != null)
                cblDisabilityCategories.Attributes.Add("role", "presentation");
            if (cblExcludeJobs != null)
                cblExcludeJobs.Attributes.Add("role", "presentation");

			var lsSubTab = Page.RouteData.Values["Control"] as string;
			if (lsSubTab != null)
			{
                if (lsSubTab == "searchjobpostings" || lsSubTab == "searchjobpostingswithoutresume")
                    this.Page.Title = "Search Job Postings";
                else if (lsSubTab == "changecriteria")
                    this.Page.Title = "Change Criteria";

				if (lsSubTab == "searchjobpostings" || lsSubTab == "changecriteria" ||
						lsSubTab == "searchjobpostingswithoutresume")
				{
					SearchJobPostings.Visible = true;
					SearchJobPostings.SearchJobPostingsClick += OnSearchJobPostingsClick;
				}
				else if (lsSubTab == "savedsearch")
				{
					if (IsPostBack)
					{
						var searchCriteria = BuildSearchExpression();
						if (searchCriteria.IsNotNull())
							App.SetSessionValue("Career:SearchCriteria", searchCriteria);
					}
					SavedSearch.Visible = true;
				}
				else
					DoNotSeeSearch(lsSubTab);
			}
			else
				SearchJobPostings.Visible = true;

			if (!IsPostBack)
			{
				if (App.GuideToResume())
					Response.Redirect(UrlBuilder.YourResume());

				LocaliseUI();
				HideControl();
				BindDropdowns();
				BindSearchExpression(lsSubTab == "searchjobpostingswithoutresume");
				ApplyTheme();
			}
		}

		/// <summary>
		/// Applies the theme.
		/// </summary>
		private void ApplyTheme()
		{
			ExpandCollapseAllButton.Visible = App.Settings.Theme == FocusThemes.Workforce;

			//if (App.Settings.Theme == FocusThemes.Education || !App.Settings.TalentModulePresent)
			//	HomeBasedJobPostingsPlaceHolder.Visible = false;
		}

		/// <summary>
		/// Does the not see search.
		/// </summary>
		/// <param name="tab">The tab.</param>
		private void DoNotSeeSearch(string tab)
		{
			SearchJobPostings.Visible = true;
			SearchJobPostings.SearchJobPostingsClick += OnSearchJobPostingsClick;

			switch (tab)
			{
				case "Industry":
					accOccupationTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accOccupationContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;

				case "ExcludeJob":
					accExcludeTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accExcludeContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;

				case "IgnoreResume":
					accJobMatchTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accJobMatchContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;

				case "EducationLevel":
					accEducationTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accEducationContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;

				case "PostingDate":
					accPostingTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accPostingContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;

				case "SalaryLevel":
					accSalaryTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accSalaryContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;
			}
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
			rbSearchAllWords.Text = CodeLocalise("rbSearchAllWords.Text", "Include all words");
			rbSearchAnyWords.Text = CodeLocalise("rbSearchAllWords.Text", "Include any words");

			RadiusValidator.ErrorMessage =
				SavedSearchRadiusValidator.ErrorMessage =
					CodeLocalise("RadiusLocation.RequiredErrorMessage", "Both radius and 5 digit zip code is required");
			//AreaValidator.ErrorMessage = CodeLocalise("AreaLocation.RequiredErrorMessage", "Both state and city is required");
			cbJobsInMyState.Text = CodeLocalise("OnlyShow.Label", "Only show in-state jobs");
			//HomeBasedJobPostingsRadioButton.Text = CodeLocalise("HomeBasedJobPostingsCheckBox.Label",
			//	"Search home-based job postings");
			ExpandCollapseAllButton.Text = HtmlLocalise("CollapseAllButton.Text", "Expand all");
			rbMSAState.Text = CodeLocalise("JobSearchCriteria.rbMSAState.Text", "Search this state/city");
		}

		/// <summary>
		/// Binds the dropdowns.
		/// </summary>
		private void BindDropdowns()
		{
			try
			{
				BindStarMatchDropDown(StarMatching.ThreeStar);
				ddlRadius.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses), null,
					CodeLocalise("Radius.TopDefault", "- select radius -"));
				BindPosingKeywordScopeDropDown(PostingKeywordScopes.Anywhere);

				var postingAgeLookups = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges);
				var defaultPostingAgeLookup =
					postingAgeLookups.FirstOrDefault(
						x => x.ExternalId == App.Settings.JobSearchPostingDate.ToString(CultureInfo.InvariantCulture));
				ddlPostingAge.BindLookup(postingAgeLookups,
					(defaultPostingAgeLookup.IsNotNull())
						? defaultPostingAgeLookup.Id.ToString(CultureInfo.InvariantCulture)
						: "7", null);
				var educationLevels = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.RequiredEducationLevels);
				foreach (var educationLevel in educationLevels.ToList())
				{
					if (educationLevel.Key == "RequiredEducationLevel.LessThanBachelorsDegree")
						educationLevels.Remove(educationLevel);

					if (educationLevel.Key == "RequiredEducationLevel.GraduateDegree")
						educationLevels.Remove(educationLevel);
				}
				cblEducationRequired.BindLookup(educationLevels);
				BindInternshipFilterDropDown();
				BindHomeBasedJobsFilterDropDown();
				BindCommissionBasedJobsFilterDropDown();
				BindSalaryAndCommissionBasedJobsFilterDropDown();
				cblDisabilityCategories.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PhysicalAbilities));

				var lookupDictionary = ServiceClientLocator.CoreClient(App).GetLocalisationDictionary();

				var emergingSectors = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.EmergingSectors);
				//TODO: hmmm , test this and possibly simplify
				var emergingSectorToolTips = (from emergingSector in emergingSectors
																			let localisationDictionaryEntry = lookupDictionary.GetValue(string.Concat(emergingSector.Key, ".ToolTip"))
																			where localisationDictionaryEntry != null
																			where localisationDictionaryEntry.IsNotNull()
																			select localisationDictionaryEntry.LocalisedValue).ToList();
				cblEmergingSectors.BindLookup(emergingSectors, emergingSectorToolTips);

				if (App.User.IsAuthenticated)
				{
					IsAuthenticated = App.User.IsAuthenticated.ToString();

					var resumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId();
					var distinctJobs = ServiceClientLocator.ResumeClient(App).GetDistinctJobs(resumeId);

					if (distinctJobs.IsNotNullOrEmpty())
					{
						cblExcludeJobs.DataSource = distinctJobs;
						cblExcludeJobs.DataValueField = "Key";
						cblExcludeJobs.DataTextField = "Value";
						cblExcludeJobs.DataBind();

						tblExcludeJobs.Visible = true;
					}
					else
						tblExcludeJobs.Visible = false;
				}

				BindJobFamilyDropDown();

				ddlOccupation.Items.AddLocalisedTopDefault("Occupations.SelectOccupation", "- select occupation -");
				ddlIndustry.BindDictionary(ServiceClientLocator.CoreClient(App).GetIndustryClassificationLookup(0), null,
					CodeLocalise("Industry.TopDefault", "- select industry -"), "0");
				ddlIndustryDetail.Items.AddLocalisedTopDefault("OccupationDetails.SelectOccupationDetails",
					"- select occupation details -");

				var nearbyStates = (from s in ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States)
														where App.Settings.NearbyStateKeys.Contains(s.Key)
														select s).ToList();
				ddlState.BindLookup(nearbyStates, null, CodeLocalise("State.TopDefault", "- select state -"), "0");
				ddlMSA.Items.AddLocalisedTopDefault("City.SelectCity", "- select city -");
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Binds the job family drop down.
		/// </summary>
		private void BindJobFamilyDropDown()
		{
			ddlJobFamily.DataSource = ServiceClientLocator.ExplorerClient(App).GetCareerAreas();
			ddlJobFamily.DataTextField = "Name";
			ddlJobFamily.DataValueField = "Id";
			ddlJobFamily.DataBind();
			ddlJobFamily.Items.AddLocalisedTopDefault("JobFamily.TopDefault", "- select job family -", "0");
		}

		/// <summary>
		/// Binds the star match drop down.
		/// </summary>
		/// <param name="selectedValue">The selected value.</param>
		private void BindStarMatchDropDown(StarMatching selectedValue)
		{
			ddlStarMatch.Items.Clear();

			ddlStarMatch.Items.AddEnum(StarMatching.OneStar, "1-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.TwoStar, "2-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.ThreeStar, "3-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.FourStar, "4-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.FiveStar, "5-star match");

			if (selectedValue.IsNotNull())
				ddlStarMatch.SelectValue(selectedValue.ToString());
		}

		/// <summary>
		/// Binds the internship filter drop down.
		/// </summary>
		private void BindInternshipFilterDropDown()
		{
			ddlInternshipRequired.Items.Clear();

			ddlInternshipRequired.Items.AddEnum(FilterTypes.Exclude, "Exclude internships");
			ddlInternshipRequired.Items.AddEnum(FilterTypes.NoFilter, "Search for all jobs (including internships)");
			ddlInternshipRequired.Items.AddEnum(FilterTypes.ShowOnly, "Search only for internships");
			//ddlInternshipRequired.Items.AddLocalisedTopDefault("RequiredInternship.TopDefault", "- select internship -");
			ddlInternshipRequired.SelectValue(FilterTypes.NoFilter.ToString());
		}

		/// <summary>
		/// Binds the home based jobs filter drop down.
		/// </summary>
		private void BindHomeBasedJobsFilterDropDown()
		{
			ddlHomeBasedJobsRequired.Items.Clear();

			ddlHomeBasedJobsRequired.Items.AddEnum(FilterTypes.Exclude, "Exclude Home-based jobs");
			ddlHomeBasedJobsRequired.Items.AddEnum(FilterTypes.NoFilter, "Search for all jobs (including home-based jobs)");
			ddlHomeBasedJobsRequired.Items.AddEnum(FilterTypes.ShowOnly, "Search only for Home-based jobs");
			ddlHomeBasedJobsRequired.SelectValue(FilterTypes.NoFilter.ToString());
		}

		/// <summary>
		/// Binds the commission-only jobs filter drop down.
		/// </summary>
		private void BindCommissionBasedJobsFilterDropDown()
		{
			ddlCommissionBasedJobsRequired.Items.Clear();

			ddlCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.Exclude, "Exclude Commission-only jobs");
			ddlCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.NoFilter, "Search for all jobs (including Commission-only jobs)");
			ddlCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.ShowOnly, "Search only for Commission-only jobs");
			ddlCommissionBasedJobsRequired.SelectValue(FilterTypes.NoFilter.ToString());
		}

		/// <summary>
		/// Binds the commission + salary jobs jobs filter drop down.
		/// </summary>
		private void BindSalaryAndCommissionBasedJobsFilterDropDown()
		{
			ddlSalaryAndCommissionBasedJobsRequired.Items.Clear();

			ddlSalaryAndCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.Exclude, "Exclude Commission + salary jobs");
			ddlSalaryAndCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.NoFilter, "Search for all jobs (including Commission + salary jobs)");
			ddlSalaryAndCommissionBasedJobsRequired.Items.AddEnum(FilterTypes.ShowOnly, "Search only for Commission + salary jobs");
			ddlSalaryAndCommissionBasedJobsRequired.SelectValue(FilterTypes.NoFilter.ToString());
		}

		/// <summary>
		/// Binds the posing keyword scope drop down.
		/// </summary>
		/// <param name="selectedValue">The selected value.</param>
		private void BindPosingKeywordScopeDropDown(PostingKeywordScopes selectedValue)
		{
			ddlSearchLocation.Items.Clear();

			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Anywhere, "Anywhere");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobDescription, "Job Description");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Employer, "#BUSINESS#");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobTitle, "Job Title");

			if (selectedValue.IsNotNull())
				ddlSearchLocation.SelectValue(selectedValue.ToString());
		}


		/// <summary>
		/// Called when [search job postings click].
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void OnSearchJobPostingsClick(object sender, EventArgs e)
		{
			var sExp = BuildSearchExpression();
			if (sExp.JobIdCriteria.IsNotNullOrEmpty() && !App.User.IsAuthenticated)
			{
				ConfirmationModal.Show();
			}
			else
			{
				App.SetSessionValue("Career:SearchCriteria", sExp);
				App.SetCookieValue("Career:ManualSearch", "true");
				Response.RedirectToRoute("JobSearchResults");
			}
		}

		/// <summary>
		/// Builds the search expression.
		/// </summary>
		/// <returns></returns>
		private SearchCriteria BuildSearchExpression()
		{
			var criteria = new SearchCriteria();
			if (!BuildJobIdCriteria(criteria))
			{
				BuildJobMatchingCriteria(criteria);
				BuildJobLocationCriteria(criteria);
				BuildKeywordCriteria(criteria);
				BuildPostingAgeCriteria(criteria);
				BuildEducationLevelCriteria(criteria);
				BuildLanguageCriteria(criteria);
				BuildSalaryLevelCriteria(criteria);
				BuildInternshipCriteria(criteria);
				BuildHomeBasedJobsCriteria(criteria);
				BuildCommissionBasedJobsCriteria(criteria);
				BuildSalaryAndCommissionBasedJobsCriteria(criteria);
				BuildOccupationAndIndustryCriteria(criteria);
				BuildEmergingSectorCriteria(criteria);
				BuildPhysicalAbilitiesCriteria(criteria);
				ExcludePastJobCriteria(criteria);
				BuildROnetCriteria(criteria);
				criteria.WorkDaysCriteria = SearchCriteriaWorkDay.BuildSearchExpression();
			}
			return criteria;
		}

		/// <summary>
		/// Builds the r onet criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildROnetCriteria(SearchCriteria criteria)
		{
			var careerAreaId = ddlJobFamily.SelectedValueToLong();

			if (careerAreaId.IsNotNull() && careerAreaId > 0)
			{
				var roNet = ddlOccupation.SelectedValue;

				var roNets = new List<string>();
				var isAll = false;

				if (roNet.IsNullOrEmpty())
				{
					roNets = null;
				}
				else if (roNet == "-" + careerAreaId)
				{
					roNets =
						ServiceClientLocator.ExplorerClient(App).GetCareerAreaJobs(careerAreaId).Select(x => x.ROnet).ToList();
					isAll = true;
				}
				else if (roNet.Substring(0, 1) != "-")
				{
					roNets.Add(roNet);
				}

				criteria.ROnetCriteria = new ROnetCriteria
				{
					CareerAreaId = careerAreaId,
					ROnets = roNets,
					IsAllROnets = isAll
				};
			}
		}

		/// <summary>
		/// Excludes the past job criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void ExcludePastJobCriteria(SearchCriteria criteria)
		{
			if (rbStarMatch.Checked)
			{
				var resumeId = (long?)null;
				if (App.User.IsAuthenticated)
					resumeId = App.UserData.DefaultResumeId;

				criteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
				{
					DocumentId = resumeId.ToString(),
					DocumentType = DocumentType.Resume
				};


				var selectedJobs = (from job in cblExcludeJobs.Items.Cast<ListItem>().Where(checkbox => checkbox.Selected)
														select new Job { JobId = job.Value.AsNullableGuid(), Description = job.Text }).ToList();

				if (selectedJobs.Any())
					criteria.ReferenceDocumentCriteria.ExcludedJobs = selectedJobs;
			}
		}

		/// <summary>
		/// Builds the physical abilities criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildPhysicalAbilitiesCriteria(SearchCriteria criteria)
		{
			var selectedPhysicalabilities = new List<long>();
			string abilitiesList = "";

			foreach (ListItem checkbox in cblDisabilityCategories.Items)
			{
				if (checkbox.Selected)
				{
					var abilityId = checkbox.Value.ToLong();
					if (abilityId.HasValue)
					{
						selectedPhysicalabilities.Add(abilityId.Value);
						abilitiesList += checkbox.Text + ", ";
					}
				}
			}

			if (selectedPhysicalabilities.Count > 0)
			{
				abilitiesList = abilitiesList.Trim();
				if (abilitiesList.EndsWith(","))
					abilitiesList = abilitiesList.Substring(0, abilitiesList.Length - 1);

				criteria.PhysicalAbilityCriteria = new PhysicalAbilityCriteria
				{
					PhysicalAbilityIds = selectedPhysicalabilities,
					PhysicalAbilities = abilitiesList
				};
			}
		}

		/// <summary>
		/// Builds the emerging sector criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildEmergingSectorCriteria(SearchCriteria criteria)
		{
			var selectedSectors = new List<long>();
			string sectors = "";

			foreach (ListItem checkbox in cblEmergingSectors.Items)
			{
				if (checkbox.Selected)
				{
					var sectorId = checkbox.Value.ToLong();
					if (sectorId.HasValue)
					{
						selectedSectors.Add(sectorId.Value);
						sectors += checkbox.Text + ", ";
					}
				}
			}

			if (selectedSectors.Count > 0)
			{
				sectors = sectors.Trim();
				if (sectors.EndsWith(","))
					sectors = sectors.Substring(0, sectors.Length - 1);
				criteria.JobSectorCriteria = new JobSectorCriteria
				{
					RequiredJobSectorIds = selectedSectors,
					RequiredJobSectors = sectors
				};
			}
		}

		/// <summary>
		/// Builds the occupation and industry criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildOccupationAndIndustryCriteria(SearchCriteria criteria)
		{
			var industryId = ddlIndustry.SelectedValueToLong();

			var industryDetailId = ddlIndustryDetail.SelectedValueToLong();
			var industryDetailIds = (industryDetailId.IsNotNull() && industryDetailId > 0)
				? new List<long> { industryDetailId }
				: null;

			if (industryId.IsNotNull() && industryId > 0)
			{
				criteria.IndustryCriteria = new IndustryCriteria
				{
					IndustryId = industryId,
					IndustryDetailIds = industryDetailIds
				};
			}
		}

		/// <summary>
		/// Builds the internship criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildInternshipCriteria(SearchCriteria criteria)
		{
			var selectedIntership = ddlInternshipRequired.SelectedValueToEnum(FilterTypes.NoFilter);
			if (selectedIntership.IsNotNull())
			{
				criteria.InternshipCriteria = new InternshipCriteria
				{
					Internship = selectedIntership
				};
			}
		}

		/// <summary>
		/// Builds the home based jobs criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildHomeBasedJobsCriteria(SearchCriteria criteria)
		{
			var selectedHomeBasedJobs = ddlHomeBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter);
			if (selectedHomeBasedJobs.IsNotNull())
			{
				criteria.HomeBasedJobsCriteria = new HomeBasedJobsCriteria
				{
					HomeBasedJobs = selectedHomeBasedJobs
				};
			}
		}

		/// <summary>
		/// Builds the commission based jobs criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildCommissionBasedJobsCriteria(SearchCriteria criteria)
		{
			var selectedCommissionBasedJobs = ddlCommissionBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter);
			if (selectedCommissionBasedJobs.IsNotNull())
			{
				criteria.CommissionBasedJobsCriteria = new CommissionBasedJobsCriteria
				{
					CommissionBasedJobs = selectedCommissionBasedJobs
				};
			}
		}

		/// <summary>
		/// Builds the salary and commission based jobs criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildSalaryAndCommissionBasedJobsCriteria(SearchCriteria criteria)
		{
			var selectedSalaryAndCommissionBasedJobs = ddlSalaryAndCommissionBasedJobsRequired.SelectedValueToEnum(FilterTypes.NoFilter);
			if (selectedSalaryAndCommissionBasedJobs.IsNotNull())
			{
				criteria.SalaryAndCommissionBasedJobsCriteria = new SalaryAndCommissionBasedJobsCriteria
				{
					SalaryAndCommissionBasedJobs = selectedSalaryAndCommissionBasedJobs
				};
			}
		}

		/// <summary>
		/// Builds the salary level criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildSalaryLevelCriteria(SearchCriteria criteria)
		{
			if (minimumSalaryTextBox.Text.AsFloat() > 0)
			{
				criteria.SalaryCriteria = new SalaryCriteria
				{
					IncludeJobsWithoutSalaryInformation = cbWithoutSalaryInfo.Checked,
					MinimumSalary = minimumSalaryTextBox.Text.AsFloat()
				};
			}
		}

		/// <summary>
		/// Builds the education level criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildEducationLevelCriteria(SearchCriteria criteria)
		{
			var selectedEducation = (from ListItem checkbox in cblEducationRequired.Items
															 where checkbox.Selected
															 select checkbox.Value.ToLong()
																 into checkboxValue
																 where checkboxValue.HasValue
																 select checkboxValue.Value).ToList();

			if (selectedEducation.IsNotNullOrEmpty())
			{
				criteria.EducationLevelCriteria = new EducationLevelCriteria
				{
					IncludeJobsWithoutEducationRequirement = cbWithoutEducationInfo.Checked,
					RequiredEducationIds = selectedEducation
				};
			}
		}

		/// <summary>
		/// Builds the Language criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildLanguageCriteria(SearchCriteria criteria)
		{
			var languages = LanguageProficiencyMgr.UnBindLanguageRepeater();

			if (languages.IsNotNull() && languages.Count > 0)
			{
				criteria.LanguageCriteria = new LanguageCriteria
				{
					LanguagesWithProficiencies = languages
				};
			}
		}

		/// <summary>
		/// Builds the posting age criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildPostingAgeCriteria(SearchCriteria criteria)
		{
			var postingAgeLookup =
					ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges).FirstOrDefault(
							x => x.Id == ddlPostingAge.SelectedValueToLong());
			var selectedPostingAge = (postingAgeLookup.IsNotNull()) ? postingAgeLookup.ExternalId : String.Empty;

			if (selectedPostingAge.IsNotNullOrEmpty())
			{
				criteria.PostingAgeCriteria = new PostingAgeCriteria
				{
					PostingAgeInDays = selectedPostingAge.AsInt()
				};
			}
		}

		private bool BuildJobIdCriteria(SearchCriteria criteria)
		{
			if (!string.IsNullOrWhiteSpace(txtJobID.Text))
			{
				criteria.JobIdCriteria = txtJobID.TextTrimmed();
				return true;
			}
			return false;
		}

		/// <summary>
		/// Builds the keyword criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildKeywordCriteria(SearchCriteria criteria)
		{
			if (SearchTermTextBox.TextTrimmed() != "")
			{
				var searchLocation = ddlSearchLocation.SelectedValueToEnum(PostingKeywordScopes.Anywhere);
				criteria.KeywordCriteria = new KeywordCriteria(SearchTermTextBox.TextTrimmed())
				{
					SearchLocation = searchLocation,
					Operator = rbSearchAllWords.Checked ? LogicalOperators.And : LogicalOperators.Or
				};
			}
		}

		/// <summary>
		/// Builds the job location criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildJobLocationCriteria(SearchCriteria criteria)
		{
			var joblocation = new JobLocationCriteria();

			if (cbJobsInMyState.Checked)
			{
				joblocation.OnlyShowJobInMyState = true;
				joblocation.Area = new AreaCriteria
				{
					StateId =
							ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(
									x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault()
				};
			}

			if (rbArea.Checked && txtZipCode.TextTrimmed() != "")
			{
				var lookup =
						ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).FirstOrDefault(
								x => x.Id == ddlRadius.SelectedValueToLong());
				var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
				var radiusValue = radiusExternalId.ToLong();

				var radius = new RadiusCriteria
				{
					Distance = (radiusValue.HasValue) ? radiusValue.Value : 0,
					DistanceUnits = DistanceUnits.Miles,
					PostalCode = txtZipCode.TextTrimmed()
				};
				joblocation.Radius = radius;

				if (cbJobsInMyState.Checked)
				{
					var state =
							ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(
									x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

					if (!App.Settings.NationWideInstance)
					{
						if (App.User.IsAuthenticated && App.UserData.HasProfile &&
								App.UserData.Profile.PostalAddress.IsNotNull() &&
								App.UserData.Profile.PostalAddress.StateId.HasValue)
							state = App.UserData.Profile.PostalAddress.StateId.Value;
					}
					AreaCriteria area = null;
					area = new AreaCriteria
					{
						StateId = state
					};
					joblocation.Area = area;
				}
			}
			else if (rbMSAState.Checked && (ddlState.SelectedValue != "" || ddlMSA.SelectedValue != ""))
			{
				var stateId = new long();

				if (App.Settings.NationWideInstance)
				{
					stateId = ddlState.SelectedValueToLong();
				}
				else
				{
					if (!cbJobsInMyState.Checked) stateId = ddlState.SelectedValueToLong();
				}

				AreaCriteria area = null;

				area = new AreaCriteria
				{
					StateId = stateId,
					MsaId = ddlMSA.SelectedValueToLong()
				};
				joblocation.Area = area;

				var lookup =
						ServiceClientLocator.CoreClient(App)
								.GetLookup(LookupTypes.States)
								.FirstOrDefault(x => x.Id == ddlState.SelectedValueToLong());

				if (lookup.IsNotNull())
					joblocation.SelectedStateInCriteria = lookup.ExternalId;
			}
			joblocation.OnlyShowJobInMyState = cbJobsInMyState.Checked;

			//if (HomeBasedJobPostingsRadioButton.Checked)
			//	joblocation.HomeBasedJobPostings = true;

			if (!rbArea.Checked && !rbMSAState.Checked)
				Utilities.BuildDefaultSearchLocationCriteria(joblocation);

			criteria.JobLocationCriteria = joblocation;
		}

		/// <summary>
		/// Builds the job matching criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		private void BuildJobMatchingCriteria(SearchCriteria criteria)
		{
			criteria.RequiredResultCriteria = new RequiredResultCriteria
			{
				DocumentsToSearch = DocumentType.Posting,
				MinimumStarMatch =
						rbStarMatch.Checked ? ddlStarMatch.SelectedValueToEnum(StarMatching.ZeroStar) : StarMatching.None,
				MaximumDocumentCount = App.Settings.MaximumNoDocumentToReturnInSearch
			};
		}

		/// <summary>
		/// Binds the prefered location.
		/// </summary>
		private void BindPreferedLocation()
		{
			if (App.User.IsAuthenticated)
			{
				try
				{
					var model = ServiceClientLocator.ResumeClient(App).GetDefaultResume();
					if (model.IsNotNull() && model.Special.IsNotNull())
					{
						var preference = model.Special.Preferences;
						if (preference.IsNotNull())
						{
							cbJobsInMyState.Checked = preference.SearchInMyState.HasValue && preference.SearchInMyState.Value;

							//HomeBasedJobPostingsRadioButton.Checked = preference.HomeBasedJobPostings.GetValueOrDefault();

							if (preference.MSAPreference.IsNotNull() && preference.MSAPreference.Any())
							{
								ddlState.SelectValue(preference.MSAPreference[0].StateId.ToString());
								ddlMSA.SelectValue(preference.MSAPreference[0].MSAId.ToString());
								ccdMSA.SelectedValue = preference.MSAPreference[0].MSAId.ToString();
								rbMSAState.Checked = true;
							}

							if (preference.ZipPreference.IsNotNull() && preference.ZipPreference.Any())
							{
								if (preference.ZipPreference[0].RadiusId.IsNotNull())
								{
									ddlRadius.SelectValue(preference.ZipPreference[0].RadiusId.ToString());
									txtZipCode.Text = preference.ZipPreference[0].Zip;
									rbArea.Checked = true;
								}
							}
						}
					}

					if (!rbMSAState.Checked && !rbArea.Checked && !App.Settings.NationWideInstance)
					{
						var stateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault();

						ddlState.SelectValue(stateId.ToString(CultureInfo.InvariantCulture));
						rbMSAState.Checked = true;
					}
				}
				catch (ApplicationException ex)
				{
					MasterPage.ShowError(AlertTypes.Error, ex.Message);
				}
			}
		}

		/// <summary>
		/// Binds the search expression.
		/// <param name="withoutresume">if set to <c>true</c> then job matching without resume</param>
		/// </summary>
		private void BindSearchExpression(bool withoutResume = false)
		{
			var searchCriteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");
			if (searchCriteria.IsNotNull())
			{
				var searchExpression = searchCriteria;

				#region Job Id

				txtJobID.Text = searchExpression.JobIdCriteria;

				#endregion

				#region Job Matching Criteria

				if (searchExpression.RequiredResultCriteria.IsNotNull())
				{
					var minimumscore = searchExpression.RequiredResultCriteria.MinimumStarMatch;
					if (minimumscore != StarMatching.None && minimumscore != StarMatching.ZeroStar)
					{
						rbStarMatch.Checked = true;
						rbWithoutMatch.Checked = false;

						ddlStarMatch.SelectValue(minimumscore.ToString());

						accJobMatchTitle.Attributes.Add("class", "collapsableAccordionTitle on");
						accJobMatchContent.Attributes.Add("class", "collapsableAccordionContent open");
					}
					else
					{
						rbWithoutMatch.Checked = true;
						rbStarMatch.Checked = false;
					}
				}
				else
				{
					SetSearchDefaults(jobMatching: true, withoutResume: withoutResume);
				}

				#endregion

				#region Job Location Criteria

				if (searchExpression.JobLocationCriteria.IsNotNull())
				{
					var locationCriteria = searchExpression.JobLocationCriteria;
					cbJobsInMyState.Checked = locationCriteria.OnlyShowJobInMyState;

					//HomeBasedJobPostingsRadioButton.Checked = locationCriteria.HomeBasedJobPostings;

					var radius = locationCriteria.Radius;
					if (radius.IsNotNull())
					{
						rbArea.Checked = true;

						var lookup =
							ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses)
												.FirstOrDefault(x => x.ExternalId == radius.Distance.ToString());

						if (lookup.IsNotNull())
							ddlRadius.SelectValue(lookup.Id.ToString());

						txtZipCode.Text = radius.PostalCode;
						if (locationCriteria.OnlyShowJobInMyState)
							rbMSAState.Checked = false;
					}

					var area = locationCriteria.Area;
					if (radius.IsNull() && area.IsNotNull())
					{
						rbMSAState.Checked = true;

						if (area.MsaId.HasValue)
						{
							ddlMSA.SelectValue(area.MsaId.ToString());
							ccdMSA.SelectedValue = area.MsaId.ToString();
						}

						if (area.StateId.HasValue)
						{
							ddlState.SelectValue((area.StateId.ToString()));
						}
					}

					if (area.IsNull() && radius.IsNull())
						rbArea.Checked = true;
				}
				else
				{
					SetSearchDefaults(searchRadius: true);
					BindPreferedLocation();
				}

				#endregion

				#region Keyword Criteria

				if (searchExpression.KeywordCriteria.IsNotNull())
				{
					ddlSearchLocation.SelectValue((searchExpression.KeywordCriteria.SearchLocation.IsNotNull())
																					? searchExpression.KeywordCriteria.SearchLocation.ToString()
																					: "");
					SearchTermTextBox.Text = searchExpression.KeywordCriteria.KeywordText;

					if (searchExpression.KeywordCriteria.Operator == LogicalOperators.Or)
						rbSearchAnyWords.Checked = true;
					else
						rbSearchAllWords.Checked = true;
				}

				#endregion

				#region Posting Age Criteria

				if (searchExpression.PostingAgeCriteria.IsNotNull() &&
						searchExpression.PostingAgeCriteria.PostingAgeInDays.IsNotNull())
				{
					var lookup =
						ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges)
											.FirstOrDefault(
												x => x.ExternalId == searchExpression.PostingAgeCriteria.PostingAgeInDays.ToString());

					if (lookup.IsNotNull())
						ddlPostingAge.SelectValue(lookup.Id.ToString());

					if (searchExpression.PostingAgeCriteria.PostingAgeInDays != App.Settings.JobSearchPostingDate)
					{
						accPostingTitle.Attributes.Add("class", "collapsableAccordionTitle on");
						accPostingContent.Attributes.Add("class", "collapsableAccordionContent open");
					}
				}

				#endregion

				#region Language Criteria

				if (searchExpression.LanguageCriteria.IsNotNull())
				{
					LanguageProficiencyMgr.BindLanguageRepeater(searchExpression.LanguageCriteria.LanguagesWithProficiencies);

					accLanguagesTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accLanguagesContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Education Level Criteria

				if (searchExpression.EducationLevelCriteria.IsNotNull())
				{
					cbWithoutEducationInfo.Checked =
						searchExpression.EducationLevelCriteria.IncludeJobsWithoutEducationRequirement;

					foreach (
						var checkbox in
							searchExpression.EducationLevelCriteria.RequiredEducationIds.SelectMany(
								requiredEducationId =>
								cblEducationRequired.Items.Cast<ListItem>()
																		.Where(checkbox => checkbox.Value == requiredEducationId.ToString())))
					{
						checkbox.Selected = true;
					}

					accEducationTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accEducationContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Salary Level Criteria

				if (searchExpression.SalaryCriteria.IsNotNull())
				{
					cbWithoutSalaryInfo.Checked = searchExpression.SalaryCriteria.IncludeJobsWithoutSalaryInformation;
					minimumSalaryTextBox.Text = searchExpression.SalaryCriteria.MinimumSalary.ToString();
					accSalaryTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accSalaryContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Intership Criteria

				if (searchExpression.InternshipCriteria.IsNotNull())
				{
					var intership = searchExpression.InternshipCriteria.Internship;

					if (intership.IsNotNull())
						ddlInternshipRequired.SelectValue(intership.ToString());

					accInternshipsTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accInternshipsContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Job Types Criteria

				if (searchExpression.HomeBasedJobsCriteria.IsNotNull())
				{
					var homeBasedJob = searchExpression.HomeBasedJobsCriteria.HomeBasedJobs;

					if (homeBasedJob.IsNotNull())
						ddlHomeBasedJobsRequired.SelectValue(homeBasedJob.ToString());

					accJobTypesTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accJobTypesContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				if (searchExpression.CommissionBasedJobsCriteria.IsNotNull())
				{
					var commissionBasedJob = searchExpression.CommissionBasedJobsCriteria.CommissionBasedJobs;

					if (commissionBasedJob.IsNotNull())
						ddlCommissionBasedJobsRequired.SelectValue(commissionBasedJob.ToString());

					accJobTypesTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accJobTypesContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				if (searchExpression.SalaryAndCommissionBasedJobsCriteria.IsNotNull())
				{
					var salaryAndCommissionBasedJob = searchExpression.SalaryAndCommissionBasedJobsCriteria.SalaryAndCommissionBasedJobs;

					if (salaryAndCommissionBasedJob.IsNotNull())
						ddlSalaryAndCommissionBasedJobsRequired.SelectValue(salaryAndCommissionBasedJob.ToString());

					accJobTypesTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accJobTypesContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Occupation and Industry Criteria

				if (searchExpression.OccupationCriteria.IsNotNull())
				{
					if (searchExpression.OccupationCriteria.JobFamilyId.IsNotNull() &&
							searchExpression.OccupationCriteria.JobFamilyId > 0)
						ddlJobFamily.SelectValue(searchExpression.OccupationCriteria.JobFamilyId.ToString());

					if (searchExpression.OccupationCriteria.OccupationIds.IsNotNullOrEmpty())
					{
						if (searchExpression.OccupationCriteria.OccupationIds[0].IsNotNull() &&
								searchExpression.OccupationCriteria.OccupationIds[0] > 0)
						{
							ddlOccupation.SelectValue(searchExpression.OccupationCriteria.OccupationIds[0].ToString());
							ccdOccupation.SelectedValue = searchExpression.OccupationCriteria.OccupationIds[0].ToString();
						}
					}
					accOccupationTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accOccupationContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				if (searchExpression.IndustryCriteria.IsNotNull())
				{
					if (searchExpression.IndustryCriteria.IndustryId.HasValue)
						ddlIndustry.SelectValue(searchExpression.IndustryCriteria.IndustryId.ToString());

					if (searchExpression.IndustryCriteria.IndustryDetailIds.IsNotNull() && searchExpression.IndustryCriteria.IndustryDetailIds.Count() == 1)
					{
						if (searchExpression.IndustryCriteria.IndustryDetailIds[0].IsNotNull())
						{
							ddlIndustry.SelectValue(searchExpression.IndustryCriteria.IndustryDetailIds[0].ToString());
							ccdIndustryDetail.SelectedValue = searchExpression.IndustryCriteria.IndustryDetailIds[0].ToString();
						}
					}
					accOccupationTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accOccupationContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Emerging Sectors Criteria

				if (searchExpression.JobSectorCriteria.IsNotNull())
				{
					foreach (var sectorId in searchExpression.JobSectorCriteria.RequiredJobSectorIds)
					{
						foreach (var checkbox in cblEmergingSectors.Items.Cast<ListItem>())
						{
							if (sectorId.ToString() == checkbox.Value)
								checkbox.Selected = true;
						}
					}

					accEmergingTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accEmergingContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Physical abilities Criteria

				if (searchExpression.PhysicalAbilityCriteria.IsNotNull())
				{
					foreach (var abilityId in searchExpression.PhysicalAbilityCriteria.PhysicalAbilityIds)
					{
						foreach (var checkbox in cblDisabilityCategories.Items.Cast<ListItem>())
						{
							if (abilityId.ToString() == checkbox.Value)
								checkbox.Selected = true;
						}
					}
					accPhysicalTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accPhysicalContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Exclude Job Criteria

				if (searchExpression.ReferenceDocumentCriteria.IsNotNull())
				{
					foreach (ListItem checkbox in cblExcludeJobs.Items)
					{
						var excludejob = searchExpression.ReferenceDocumentCriteria.ExcludedJobs;

						if (excludejob.IsNotNull())
							foreach (var job in excludejob.Where(job => job.JobId == checkbox.Value.AsNullableGuid()))
							{
								checkbox.Selected = true;
							}
					}
					accExcludeTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accExcludeContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region ROnet Criteria

				if (searchExpression.ROnetCriteria.IsNotNull())
				{
					if (searchExpression.ROnetCriteria.CareerAreaId.IsNotNull())
					{
						ddlJobFamily.SelectValue(searchExpression.ROnetCriteria.CareerAreaId.ToString());
					}

					if (searchCriteria.ROnetCriteria.ROnets.IsNotNullOrEmpty() && !searchCriteria.ROnetCriteria.IsAllROnets)
					{
						ddlOccupation.SelectValue(searchCriteria.ROnetCriteria.ROnets[0]);
						ccdOccupation.SelectedValue = searchExpression.ROnetCriteria.ROnets[0].ToString(CultureInfo.InvariantCulture);
					}
					accOccupationTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accOccupationContent.Attributes.Add("class", "collapsableAccordionContent open");
				}

				#endregion

				#region Work Availability

				SearchCriteriaWorkDay.Bind(searchCriteria.WorkDaysCriteria);

				#endregion
			}
			else
			{
				SetSearchDefaults(true, true);
				BindPreferedLocation();
			}

			// Disable resume fields in the case of no resume being present
			if (!App.UserData.HasDefaultResume)
			{
				rbWithoutMatch.Checked = true;
				rbStarMatch.Checked = false;
				rbStarMatch.Enabled = false;
				ddlStarMatch.Enabled = false;
			}
		}

		/// <summary>
		/// Set default search settings
		/// </summary>
		/// <param name="jobMatching">if set to <c>true</c> then default job matching fields</param>
		/// <param name="searchRadius">if set to <c>true</c> then default radius fields</param>
		/// <param name="withoutresume">if set to <c>true</c> then job matching without resume so default to WithoutMatch</param>
		private void SetSearchDefaults(bool jobMatching = false, bool searchRadius = false, bool withoutResume = false)
		{
			if (jobMatching)
			{
				if (withoutResume)
				{
					rbWithoutMatch.Checked = true;
				}
				else
				{
					rbWithoutMatch.Checked = App.Settings.CareerSearchShowAllJobs;
				}
				rbStarMatch.Checked = !rbWithoutMatch.Checked;
				ddlStarMatch.SelectedIndex = App.Settings.CareerSearchMinimumStarMatchScore - 1;
			}

			if (searchRadius && App.Settings.CareerSearchDefaultRadiusId > 0)
			{
				rbArea.Checked = true;
				ddlRadius.SelectedValue = App.Settings.CareerSearchDefaultRadiusId.ToString(CultureInfo.InvariantCulture);
				if (App.UserData.Profile.IsNotNull() && App.UserData.Profile.PostalAddress.IsNotNull() && App.UserData.Profile.PostalAddress.Zip.IsNotNullOrEmpty())
					txtZipCode.Text = App.UserData.Profile.PostalAddress.Zip;
			}
		}

		/// <summary>
		/// Hides the control.
		/// </summary>
		private void HideControl()
		{
			if (App.User.IsAuthenticated)
			{
				tblExcludeJobs.Visible = true;
				ddlStarMatch.Enabled = true;

				tblExcludeJobs.Visible = App.UserData.HasDefaultResume;
			}
			else
			{
				tblExcludeJobs.Visible = false;
				ddlStarMatch.Enabled = false;
			}

			SearchCriteriaWorkDay.Visible = App.Settings.WorkAvailabilitySearch;

			JobIDFilterPlaceHolder.Visible = App.Settings.EnableJobSearchByJobId;
		}

	}
}
