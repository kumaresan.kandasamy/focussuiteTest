﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True"
	CodeBehind="Home.aspx.cs" Inherits="Focus.CareerExplorer.WebCareer.Home" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="~/Controls/Bookmarks.ascx" TagName="Bookmarks" TagPrefix="uc" %>
<%@ Register Src="Controls/RecentlyViewedJobs.ascx" TagName="RecentlyViewedJobs" TagPrefix="uc" %>
<%@ Register Src="Controls/JobsOfInterestReceived.ascx" TagName="JobsOfInterestReceived" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/SavedSearchList.ascx" TagName="ListSearches" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/CreateEditResumePopup.ascx" TagName="CreateEditResumePopup" TagPrefix="uc" %>
<%@ Register src="~/Controls/MessagesDisplay.ascx" tagname="MessagesDisplay" tagprefix="uc" %>
<%@ Register src="~/Controls/ResumeSearchLink.ascx" tagPrefix="uc" tagName="ResumeSearchLink" %>
<%@ Register Src="Controls/JobSeekerSurveyModal.ascx" TagName="JobSeekerSurveyModal" TagPrefix="uc" %>
<%@ Register Src="Controls/ReferralsList.ascx" TagName="ReferralsList" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/ResumeList.ascx" TagName="ResumeList" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server" >
<script type="text/javascript">
	$(document).ready(function () {
		$(".inFieldLabelStyled > label").inFieldLabels();
	});
</script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" />
	<uc:MessagesDisplay ID="Messages" runat="server" Module="CareerExplorer" />
	<asp:UpdatePanel runat="server" ID="UpdatePanelHome" UpdateMode="Always">
		<ContentTemplate>
			<div class="FocusCareer">
				<div class="staticMessages col-sm-8">
					<asp:Repeater	ID="StaticMessagesRepeater" runat="server">
						<ItemTemplate>
							<span class="information-panel"><%# Container.DataItem %></span>		
						</ItemTemplate>
					</asp:Repeater>
				</div>
				<div class="findAJob">
					<%= HtmlInFieldLabel("FindAJobTextBox", "FindAJobTextBox.Label", "find a job", 168, "inFieldLabelStyled")%>
					<asp:TextBox ID="FindAJobTextBox" runat="server" ClientIDMode="Static" Width="168px" />
					<asp:Button ID="btnFindJob" Text="Go &#187;" runat="server" class="buttonLevel2" OnClick="btnFindJob_Click" ValidationGroup="Findjob" />
					<br />
					<asp:RequiredFieldValidator ID="FindJobRequired" runat="server" ControlToValidate="FindAJobTextBox" CssClass="error" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Findjob" />
				</div>
				<div class="homeWrapper">
					<section class="homeMain col-xs-12 col-sm-8 col-md-9">
						<div class="collapsiblePanel">
							<div class="collapsiblePanelHeader">
								<focus:LocalisedLabel runat="server" ID="AccordionHeading1Label" RenderOuterSpan="False" LocalisationKey="AccordionHeading1.Label" DefaultText="New matches for your saved searches"/>
								<div class="collapsiblePanelControl">
									<focus:LocalisedLabel runat="server" ID="ShowAccordion1Label" CssClass="cpShow" LocalisationKey="Show.Label" DefaultText="Show"/>
									<focus:LocalisedLabel runat="server" ID="HideAccordion1Label" CssClass="cpHide" LocalisationKey="Hide.Label" DefaultText="Hide"/>
								</div>
							</div>
							<div class="collapsiblePanelContent">
								<uc:ListSearches ID="ListSearch" runat="server" OnSavedSearchesRetrieved="ListSearch_SearchesRetrievd" />
							</div>
						</div>
						<div class="collapsiblePanel" id="RecentlySentMatchesPanel" runat="server">
							<div class="collapsiblePanelHeader">
								<focus:LocalisedLabel runat="server" ID="AccordionHeading2Label" RenderOuterSpan="False" LocalisationKey="AccordionHeading2.Label" DefaultText="Jobs for which you've been encouraged to apply"/>
								<div class="collapsiblePanelControl">
									<focus:LocalisedLabel runat="server" ID="ShowAccordion2Label" CssClass="cpShow" LocalisationKey="Show.Label" DefaultText="Show"/>
									<focus:LocalisedLabel runat="server" ID="HideAccordion2Label" CssClass="cpHide" LocalisationKey="Hide.Label" DefaultText="Hide"/>
								</div>
							</div>
							<div class="collapsiblePanelContent">
							    <uc:JobsOfInterestReceived ID="JobsOfInterestReceivedList" runat="server" />
							</div>
						</div>
            <div class="collapsiblePanel" id="ReferralPanel" runat="server">
							<div class="collapsiblePanelHeader">
								<focus:LocalisedLabel runat="server" ID="AccordionHeadingReferralsLabel" RenderOuterSpan="False" LocalisationKey="AccordionHeadingReferrals.Label" DefaultText="Jobs for which you have referral activity"/>
								<div class="collapsiblePanelControl">
									<focus:LocalisedLabel runat="server" ID="ShowAccordionHeadingReferralsLabel" CssClass="cpShow" LocalisationKey="Show.Label" DefaultText="Show"/>
									<focus:LocalisedLabel runat="server" ID="HideAccordionHeadingReferralsLabel" CssClass="cpHide" LocalisationKey="Hide.Label" DefaultText="Hide"/>
								</div>
							</div>
							<div class="collapsiblePanelContent">
							  <uc:ReferralsList ID="ReferralsList" runat="server" />
 							</div>
            </div>
						<div class="collapsiblePanel">
							<div class="collapsiblePanelHeader">
								<focus:LocalisedLabel runat="server" ID="AccordionHeading3Label" RenderOuterSpan="False" LocalisationKey="AccordionHeading3.Label" DefaultText="Jobs you've recently viewed"/>
								<div class="collapsiblePanelControl">
									<focus:LocalisedLabel runat="server" ID="ShowAccordion3Label" CssClass="cpShow" LocalisationKey="Show.Label" DefaultText="Show"/>
									<focus:LocalisedLabel runat="server" ID="HideAccordion3Label" CssClass="cpHide" LocalisationKey="Hide.Label" DefaultText="Hide"/>
								</div>
							</div>
							<div class="collapsiblePanelContent">
								<uc:RecentlyViewedJobs ID="RecentlyViewedJobs" runat="server" />
							</div>
						</div>
					</section>
					<aside class="homeSideBar col-xs-12 col-sm-4 col-md-3">
						<div class="sideBarSection">
							<div id="sideBarCreateResume" onclick="$find('CreateOrEditResumeModal').show();" class="purpleSideBarLink sideBarCreateResume sideBarLink">
							  <div class="sideBarLinkMiddle">
							    <a href="#" ><focus:LocalisedLabel runat="server" ID="CreateEditResumeLabel" CssClass="sideBarLinkInner" LocalisationKey="CreateResume.Label" DefaultText="Create, edit or upload a resume"/><focus:LocalisedLabel runat="server" ID="CreateResumeLabel" Visible="False" CssClass="sideBarLinkInner" LocalisationKey="EditResume.Label" DefaultText="Create or upload a resume"/></a>
							  </div>
							</div>
							<div id="sideBarJobSearch" runat="server" class="purpleSideBarLink sideBarJobSearch sideBarLink"><div class="sideBarLinkMiddle"><div class="sideBarLinkInner"><asp:LinkButton ID="lbSearchForJobs" runat="server" OnClick="lbSearchForJobs_Click"><focus:LocalisedLabel runat="server" ID="SearchJobsLabel" RenderOuterSpan="False" LocalisationKey="SearchJobs.LinkText" DefaultText="Search for jobs"/></asp:LinkButton></div></div></div>
						</div>
						<asp:Panel runat="server" ID="ExplorerLinksPanel" CssClass="sideBarSection">
							<div id="sideBarExplore" runat="server" Visible="False" class="blueSideBarLink sideBarCareerOptions sideBarLink"><div class="sideBarLinkMiddle"><a href="#" ><focus:LocalisedLabel runat="server" ID="ExploreCareerLabel" CssClass="sideBarLinkInner" LocalisationKey="ExploreCareer.Label" DefaultText="Explore my career options"/></a></div></div>
							<div id="sideBarResearch" runat="server" Visible="False" class="blueSideBarLink sideBarResearch sideBarLink"><div class="sideBarLinkMiddle"><a href="#" ><focus:LocalisedLabel runat="server" ID="ResearchCareerLabel" CssClass="sideBarLinkInner" LocalisationKey="ResearchCareer.Label" DefaultText="Research a specific career, #BUSINESS#:LOWER, or program of study"/></a></div></div>
							<div id="sideBarStudy" runat="server" Visible="False" class="blueSideBarLink sideBarStudy sideBarLink"><div class="sideBarLinkMiddle"><a href="#" ><focus:LocalisedLabel runat="server" ID="GetAheadLabel" CssClass="sideBarLinkInner" LocalisationKey="GetAhead.Label" DefaultText="See what I can study to get ahead"/></a></div></div>
							<div id="sideBarSchool" runat="server" Visible="False" class="blueSideBarLink sideBarSchool sideBarLink"><div class="sideBarLinkMiddle"><a href="#" ><focus:LocalisedLabel runat="server" ID="SchoolLabel" CssClass="sideBarLinkInner" LocalisationKey="School.Label" DefaultText="Explore careers by #SCHOOLNAME# programs of study"/></a></div></div>
							<div id="sideBarExperience" runat="server" Visible="False" class="blueSideBarLink sideBarExperience sideBarLink"><div class="sideBarLinkMiddle"><a href="#" ><focus:LocalisedLabel runat="server" ID="ExperienceLabel" CssClass="sideBarLinkInner" LocalisationKey="Experience.Label" DefaultText="See where my experience can take me"/></a></div></div>
            </asp:Panel>
						<div class="sideBarSection" id="MyResumesPanel" runat="server">
							<h2><focus:LocalisedLabel runat="server" ID="MyResumesLabel" RenderOuterSpan="False" LocalisationKey="MyResumes.Label" DefaultText="My resumes" /></h2>
							<asp:Panel ID="panNoLoginResume" runat="server">
								<p>
								  <asp:HyperLink ID="CreateOrUploadResumeLink" runat="server" NavigateUrl="#" />
                  <%= HtmlLocalise("MyResumes2Trailer.Label", "best bet for best matches")%>
								</p>
								<p class="instructionalText"><focus:LocalisedLabel runat="server" ID="MyResumes3Label" RenderOuterSpan="False" LocalisationKey="MyResumes3.Label" DefaultText="You can search for jobs without a resume, but results are limited."/></p>
							</asp:Panel>
							<asp:Panel ID="panLoginResume" runat="server">
								<p id="ResumeViewCountContainer" runat="server"><asp:Literal id="ResumeViewCount" runat="server" EnableViewState="false" /></p>
								<div id="DefaultResumeContainer" runat="server">
									<focus:LocalisedLabel runat="server" ID="DefaultResumeLabel" RenderOuterSpan="False" Visible="False" LocalisationKey="DefaultResume.Label" DefaultText="Allow #BUSINESSES#:LOWER to search this resume"/>
									<asp:UpdatePanel ID="DefaultResumeUpdatePanel" runat="server" UpdateMode="Conditional">
											<ContentTemplate>
												<asp:DropDownList ID="ddlResumeNames" runat="server" CssClass="DefaultResumeClass"
													AutoPostBack="true" OnSelectedIndexChanged="ddlResumeNames_SelectedIndexChanged"
													ClientIDMode="AutoID" Title="Resume Names Drop Down">
												</asp:DropDownList>
												<script type="text/javascript">
													//Added Global variable  for getting postback control id
													var sourcecontrol = "";
													function pageLoad() {
														Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
														if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack()) {
															if (sourcecontrol.id == "ctl00_MainContent_ddlResumeNames")
																ApplyCustomSelectStyling($('.DefaultResumeClass'));
															//$('.DefaultResumeClass').customSelect();
															else if (sourcecontrol.id == "ctl00_MainContent_ListSearch_ddlActiveSearch")
															//$('.ActiveSearchClass').customSelect();
																ApplyCustomSelectStyling($('.ActiveSearchClass'));
														}
													}
													//The below function is added to find out which control causing the postback
													function BeginRequestHandler(sender, args) {
														sourcecontrol = args.get_postBackElement();  //the control causing the postback
													}
												</script>
											</ContentTemplate>
											<Triggers>
												<asp:AsyncPostBackTrigger ControlID="ddlResumeNames" EventName="SelectedIndexChanged" />
											</Triggers>
										</asp:UpdatePanel>
								</div>
								<uc:ResumeList ID="MyResumeList" runat="server" DeleteRedirectionPage="Home" TargetPage="ResumeWizard" MarginLeft="5px" OnLinkNameClicked="MyResumeList_linkNameclick"/>
							</asp:Panel>
						</div>
						<div class="sideBarSection">
							<h2><focus:LocalisedLabel runat="server" ID="MyBookmarksLabel" RenderOuterSpan="False" LocalisationKey="MyBookmarks.Label" DefaultText="My bookmarks"/></h2>
							<uc:Bookmarks ID="Bookmarks" runat="server" />
						</div>
					</aside>
				</div>
			</div>
			<uc:CreateEditResumePopup ID="CreateEditResume" runat="server" OnLinkNameClicked="MyResumeList_linkNameclick" />
			<uc:JobSeekerSurveyModal ID="JobSeekerSurvey" runat="server" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
