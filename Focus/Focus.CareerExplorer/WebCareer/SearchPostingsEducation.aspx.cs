﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Core;


#endregion

namespace Focus.CareerExplorer.WebCareer
{
  public partial class SearchPostingsEducation : CareerExplorerPageBase
	{

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
			RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education);

      base.OnInit(e);
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
		  if (!Page.IsPostBack)
		    SetVisibility();
		}

    /// <summary>
    /// Handles the Click event of the lnkSearchJobs control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lnkSearchJobs_Click(object sender, EventArgs e)
		{
			App.SetCookieValue("Career:PostingSearchInternshipFilter", FilterTypes.Exclude.ToString());
			App.RemoveSessionValue("Career:SearchCriteria");
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });

		}

    /// <summary>
    /// Handles the Click event of the lnkSearchInternships control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void lnkSearchInternships_Click(object sender, EventArgs e)
		{
			App.SetCookieValue("Career:PostingSearchInternshipFilter", FilterTypes.ShowOnly.ToString());
			App.RemoveSessionValue("Career:SearchCriteria");
			Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
		}

    /// <summary>
    /// Handles the Command event of the MatchingAction control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
		protected void MatchingAction_Command(object sender, CommandEventArgs e)
		{
			if (App.User.ProgramAreaId == 0 && e.CommandName == "ProgramOfStudy")
			{
				MasterPage.ShowModalAlert(AlertTypes.Info,
					CodeLocalise("ProgramOfStudy.Error", "You do not have a program of study stored.\r\nClick on My Account and choose a program of study in order to see job recommendations."));
			}
			else
			{
				var redirectRoute = Utilities.RouteToJobSearch(e.CommandName, true);
        Response.Redirect(redirectRoute);
			}
		}

    /// <summary>
    /// Sets the visibility of various controls on the page
    /// </summary>
    private void SetVisibility()
    {
      JobsBasedOnResumeHintLabel.Visible = !App.UserData.HasDefaultResume;
			MyResumeDiv.Visible = MyResumeAndProgramOfStudyDiv.Visible = !App.Settings.CareerHideSearchJobsByResume;
    }
	}
}