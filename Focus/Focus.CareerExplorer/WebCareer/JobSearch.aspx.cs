﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.CareerExplorer.Code;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
	public partial class JobSearch : PageBase
	{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
      if (App.IsAnonymousAccess())
        Response.Redirect(UrlBuilder.JobSearchCriteria());

      if (App.GuideToResume())
        Response.Redirect(UrlBuilder.YourResume());
		}
	}
}
