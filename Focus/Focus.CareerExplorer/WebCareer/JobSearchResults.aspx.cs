﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Focus.CareerExplorer.Code;
using Focus.Common.Code;
using Focus.Common.Extensions;
using Focus.Common.Models;
using Focus.Common;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Services;
using Focus.Services.Core;
using Focus.Services.Core.Extensions;
using Focus.Services.Mappers;
using Framework.ServiceLocation;
using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;
using Focus.CareerExplorer.WebAuth.Controls;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Job;
using Focus.Core.Messages.JobService;
using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
    public partial class JobSearchResults : CareerExplorerPageBase
    {
        #region Page properties

        internal IRuntimeContext RuntimeContext
        {
            get { return ServiceLocator.Current.Resolve<IRuntimeContext>(); }
        }

        private int _searchResultCount;

        protected List<PostingSearchResultView> Result
        {
            get { return App.GetSessionValue<List<PostingSearchResultView>>("JobSearchResults:Result"); }
            set { App.SetSessionValue("JobSearchResults:Result", value); }
        }

        protected Dictionary<string, long?> EmployerAccountTypes
        {
            get { return GetViewStateValue<Dictionary<string, long?>>("JobSearchResults:EmployerAccountTypes"); }
            set { SetViewStateValue("JobSearchResults:EmployerAccountTypes", value); }
        }

        protected Dictionary<long, string> AccountTypes
        {
            get { return GetViewStateValue<Dictionary<long, string>>("JobSearchResults:AccountTypes"); }
            set { SetViewStateValue("JobSearchResults:AccountTypes", value); }
        }



        protected int CompletionStatus = 0;
        protected int HasResume = 0;
        protected string AlertEmailRequired = "";

        private string _postBackReferenceForNotAuth = "";

        #endregion

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education);
            ClearExplorerReturnSession = false;

            base.OnInit(e);
            SearchCriteriaErrorModal.OkCommand += ReviewSearchCriteriaConfirmationModal_OkCommand;
        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (App.GuideToResume())
                    Response.Redirect(UrlBuilder.YourResume());

                if (App.UserData.HasDefaultResume)
                    CompletionStatus = (int)App.UserData.DefaultResumeCompletionStatus;

                LocaliseUI();
                BindHyperlinks();
                BindDropdowns();

                //TODO: Martha (UI Claimant)
                //Check for UI Claimant
                //if (App.Settings.UIClaimantEnabled && (App.GetSessionValue<string>("Career:UIClaimantStatus").IsNotNullOrEmpty() && App.GetSessionValue<string>("Career:UIClaimantStatus") != "0"))
                //{
                //  bool displayUIClaimantButton = false;
                //  if (App.GetSessionValue<string>("Career:UIClaimantStatus") == "1")
                //    displayUIClaimantButton = true;
                //  else if (App.GetSessionValue<string>("Career:UIClaimantStatus") == "2")
                //  {
                //    if (App.GetSessionValue<bool>("Career:UIClaimMaxDaysExceeded").IsNotNull() && !App.GetSessionValue<bool>("Career:UIClaimMaxDaysExceeded"))
                //      displayUIClaimantButton = true;
                //  }

                //  if (displayUIClaimantButton)
                //    panUIClaimantComplete.Visible = true;
                //}

                SearchCriteria criteria;

                var url = HttpContext.Current.Request.RawUrl;
                var index = url.IndexOf('?');

                if (index >= 0)
                {
                    var queryString = (index < url.Length - 1) ? url.Substring(index + 1) : String.Empty;

                    criteria = ParseCriteriaString(queryString);

                    if (criteria.IsNotNull())
                    {
                        criteria = SetDefaultSearchCriteria(criteria);
                        App.SetSessionValue(MagicStrings.SessionKeys.CareerSearchCriteria, criteria);
                    }
                }
                else
                {
                    // If coming from explorer we may have specific explorer criteria
                    criteria = App.GetSessionValue(MagicStrings.SessionKeys.CareerSearchCriteria, new SearchCriteria());
                }

                var explorerReturnBookmark = App.GetSessionValue<LightboxEventArgs>("Explorer:ReturnBookmark");
                if (explorerReturnBookmark.IsNotNull())
                    ExplorerReturnLinkPanel.Visible = true;

                // Criteria can be null if searching by url query and some of the criteria were invalid
                if (criteria.IsNotNull())
                {
                    BindKeywordAndLocationCriteria(criteria);
                    SetDoNotSeeScreen(criteria);
                    LoadList(criteria, bool.Parse(App.GetCookieValue("Career:ManualSearch") ?? "false"));
                    App.RemoveCookieValue("Career:ManualSearch");
                }

                GetAccountTypes();
            }

            if (IsPostBack)
                RegisterClientStartupScript("JobSearchResultsScript");
            //AIM - To Avoid Invalid postback or callback argument System Error
            if (App.GetSessionValue<SearchCriteria>("Career:SearchCriteria").IsNull())
            {
                Response.Redirect("Home");
            }
            ApplyTheme();
            DisplayTopSection();
            BindSearchResultList();
            RegisterJavascript();
        }

        /// <summary>
        /// Parses the criteria string and does validation on the parameters
        /// </summary>
        /// <param name="criteriaQuery">The criteria query.</param>
        /// <returns></returns>
        private SearchCriteria ParseCriteriaString(string criteriaQuery)
        {
            var queryStrings = HttpUtility.ParseQueryString(criteriaQuery);

            if (queryStrings.Count == 0)
                return null;

            var validationErrors = new List<string>();

            var keys = queryStrings.AllKeys;

            if (keys.Contains("industrydetail") && !keys.Contains("industry"))
                validationErrors.Add(CodeLocalise("IndustryMissingError.Text", "Industry criteria missing (industry detail specified)"));

            if (keys.Contains("occupation") && !keys.Contains("jobfamily"))
                validationErrors.Add(CodeLocalise("JobFamilyMissingError.Text", "Job family criteria missing (occupation specified)"));

            var searchCriteria = new SearchCriteria();
            var inStateOnlySet = false;

            foreach (string criteria in queryStrings.Keys)
            {
                if (criteria.IsNotNullOrEmpty())
                {
                    var values = queryStrings.GetValues(criteria);

                    if (values.IsNotNull() && values.Any())
                    {
                        if (criteria.EqualsIgnoreCase("employer"))
                        {
                            if (searchCriteria.KeywordCriteria.IsNull())
                                searchCriteria.KeywordCriteria = new KeywordCriteria();

                            var employerList = values[0].Split('|');

                            foreach (var employer in employerList)
                            {
                                searchCriteria.KeywordCriteria.KeywordText = searchCriteria.KeywordCriteria.KeywordText.IsNotNullOrEmpty() ? searchCriteria.KeywordCriteria.KeywordText + @" """ + employer + @"""" : @"""" + employer + @"""";
                            }

                            searchCriteria.KeywordCriteria.SearchLocation = PostingKeywordScopes.Employer;
                            searchCriteria.KeywordCriteria.Operator = LogicalOperators.Or;
                        }
                        else if (criteria.EqualsIgnoreCase("location") && !inStateOnlySet) // InStateOnly parameter should always take precendence to a specificed state ID
                        {
                            long locationId;

                            if (Int64.TryParse(values[0], out locationId))
                            {
                                searchCriteria.JobLocationCriteria = new JobLocationCriteria
                                {
                                    Area = new AreaCriteria
                                    {
                                        StateId = Convert.ToInt64(values[0])
                                    }
                                };
                            }
                            else
                            {
                                validationErrors.Add(CodeLocalise("LocationDataTypeError.Text", "Location must be numeric"));
                            }
                        }
                        else if (criteria.EqualsIgnoreCase("jobfamily"))
                        {
                            long jobFamilyId;

                            if (Int64.TryParse(values[0], out jobFamilyId))
                            {
                                if (jobFamilyId.IsNotNull() && jobFamilyId > 0)
                                {
                                    if (searchCriteria.ROnetCriteria.IsNull())
                                        searchCriteria.ROnetCriteria = new ROnetCriteria();

                                    searchCriteria.ROnetCriteria.CareerAreaId = jobFamilyId;
                                }
                            }
                            else
                            {
                                validationErrors.Add(CodeLocalise("JobFamilyDataTypeError.Text", "Job family must be numeric"));
                            }
                        }
                        else if (criteria.EqualsIgnoreCase("occupation"))
                        {
                            {
                                var roNet = values[0];

                                if (roNet.IsNotNull() && roNet.Length > 0)
                                {
                                    if (searchCriteria.ROnetCriteria.IsNull())
                                        searchCriteria.ROnetCriteria = new ROnetCriteria();

                                    searchCriteria.ROnetCriteria.ROnets = new List<string> { roNet };
                                }
                            }
                        }
                        else if (criteria.EqualsIgnoreCase("industry"))
                        {
                            long industryId;

                            if (Int64.TryParse(values[0], out industryId))
                            {
                                if (industryId.IsNotNull() && industryId > 0)
                                {
                                    if (searchCriteria.IndustryCriteria.IsNull())
                                        searchCriteria.IndustryCriteria = new IndustryCriteria();

                                    searchCriteria.IndustryCriteria.IndustryId = industryId;
                                }
                            }
                            else
                            {
                                validationErrors.Add(CodeLocalise("IndustryDataTypeError.Text", "Industry must be numeric"));
                            }
                        }
                        else if (criteria.EqualsIgnoreCase("industrydetail"))
                        {
                            long industryDetailId;

                            if (Int64.TryParse(values[0], out industryDetailId))
                            {
                                if (industryDetailId.IsNotNull() && industryDetailId > 0)
                                {
                                    if (searchCriteria.IndustryCriteria.IsNull())
                                        searchCriteria.IndustryCriteria = new IndustryCriteria();

                                    searchCriteria.IndustryCriteria.IndustryDetailIds = new List<long> { industryDetailId };
                                }
                            }
                            else
                            {
                                validationErrors.Add(CodeLocalise("IndustryDetailDataTypeError.Text", "Industry detail must be numeric"));
                            }
                        }

                        // InStateOnly parameter should always take precendence to a specificed state ID
                        if (criteria.EqualsIgnoreCase("instateonly"))
                        {
                            if (values[0].EqualsIgnoreCase("true") || values[0].EqualsIgnoreCase("1"))
                            {
                                inStateOnlySet = true;
                                searchCriteria.JobLocationCriteria = new JobLocationCriteria
                                {
                                    OnlyShowJobInMyState = true,
                                    Area = new AreaCriteria
                                    {
                                        StateId =
                                                                                                ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(
                                                                                                    x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault()
                                    }
                                };
                            }
                            else
                            {
                                validationErrors.Add(CodeLocalise("InStateOnlyError.Text", "Valid values for in-state only are: 1, 0, true, false"));
                            }
                        }

                        if (criteria.EqualsIgnoreCase("resumeId"))
                        {
                            searchCriteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
                            {
                                DocumentType = DocumentType.Resume,
                                DocumentId = queryStrings[criteria]
                            };
                        }
                    }
                }
            }

            var errors = ValidateUrlCriteriaIds(searchCriteria);

            if (errors.IsNotNullOrEmpty() && errors.Count > 0)
                validationErrors.AddRange(errors);

            if (validationErrors.IsNotNullOrEmpty() && validationErrors.Count > 0)
            {
                searchCriteria = null;

                var errorBullets = BuildErrorBulletList(validationErrors);

                SearchCriteriaErrorModal.Show(
                    CodeLocalise("SearchCriteriaErrorModal.Title", "Invalid search criteria"),
                    CodeLocalise("SearchCriteriaErrorModal.Details", "The following errors have been identified:" + errorBullets),
                    "Ok");
            }

            return searchCriteria;
        }

        /// <summary>
        /// Builds the error bullet list to be used in the error modal
        /// </summary>
        /// <param name="errors">The errors.</param>
        /// <returns></returns>
        private string BuildErrorBulletList(IEnumerable<string> errors)
        {
            var builder = new StringBuilder();

            builder.Append("<ul>");

            foreach (var error in errors)
            {
                builder.Append("<li>");
                builder.Append(error);
                builder.Append("</li>");
            }

            builder.Append("</ul>");

            return builder.ToString();
        }

        /// <summary>
        /// Validates the URL criteria.
        /// To validate the IDs entered into the criteria when entered via a URL
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        private List<string> ValidateUrlCriteriaIds(SearchCriteria criteria)
        {
            var errors = new List<string>();

            if (criteria.IsNotNull())
            {
                if (criteria.ROnetCriteria.IsNotNull())
                {
                    if (criteria.ROnetCriteria.CareerAreaId.HasValue)
                    {
                        var validCareerArea = ServiceClientLocator.ExplorerClient(App).GetCareerAreas().Any(x => x.Id.Equals(Convert.ToInt64(criteria.ROnetCriteria.CareerAreaId)));

                        if (!validCareerArea)
                            errors.Add(CodeLocalise("JobFamilyInvalid.Text", "Job family is invalid"));
                    }

                    if (criteria.ROnetCriteria.ROnets.IsNotNullOrEmpty() && criteria.ROnetCriteria.ROnets.Count > 0 && criteria.ROnetCriteria.CareerAreaId.HasValue)
                    {
                        // Currently there's always only one RoNet so don't need to figure out "which" one is invalid
                        var validRonet = ServiceClientLocator.ExplorerClient(App).GetCareerAreaJobs(Convert.ToInt64(criteria.ROnetCriteria.CareerAreaId)).Any(x => criteria.ROnetCriteria.ROnets.Contains(x.ROnet));

                        if (!validRonet)
                            errors.Add(CodeLocalise("OccupationInvalid.Text", "Occupation is invalid for job family specified"));
                    }
                }

                if (criteria.IndustryCriteria.IsNotNull())
                {
                    if (criteria.IndustryCriteria.IndustryId.HasValue)
                    {
                        var validIndustry = ServiceClientLocator.CoreClient(App).GetIndustryClassificationLookup(0).Any(x => x.Key.Equals(criteria.IndustryCriteria.IndustryId.ToString()));

                        if (!validIndustry)
                            errors.Add(CodeLocalise("IndustryInvalid.Text", "Industry is invalid"));
                    }

                    if (criteria.IndustryCriteria.IndustryDetailIds.IsNotNullOrEmpty() && criteria.IndustryCriteria.IndustryDetailIds.Count > 0 && criteria.IndustryCriteria.IndustryId.HasValue)
                    {
                        var industryIdsAsStrings = criteria.IndustryCriteria.IndustryDetailIds.ConvertAll(i => i.ToString());

                        // Currently there's always only one industry detail ID so don't need to figure out "which" one is invalid
                        var validIndustryDetails = ServiceClientLocator.CoreClient(App).GetIndustryClassificationLookup(criteria.IndustryCriteria.IndustryId, 4).Any(x => industryIdsAsStrings.Contains(x.Key));

                        if (!validIndustryDetails)
                            errors.Add(CodeLocalise("OccupationInvalid.Text", "Industry detail is invalid for industry specified"));
                    }
                }
            }

            return errors;
        }

        /// <summary>
        /// Sets the default search criteria.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <returns></returns>
        private SearchCriteria SetDefaultSearchCriteria(SearchCriteria criteria)
        {
            criteria.PostingAgeCriteria = new PostingAgeCriteria { PostingAgeInDays = 7 };
            criteria.InternshipCriteria = new InternshipCriteria { Internship = FilterTypes.NoFilter };
            criteria.HomeBasedJobsCriteria = new HomeBasedJobsCriteria { HomeBasedJobs = FilterTypes.NoFilter };
            criteria.CommissionBasedJobsCriteria = new CommissionBasedJobsCriteria { CommissionBasedJobs = FilterTypes.NoFilter };
            criteria.SalaryAndCommissionBasedJobsCriteria = new SalaryAndCommissionBasedJobsCriteria { SalaryAndCommissionBasedJobs = FilterTypes.NoFilter };

            criteria.RequiredResultCriteria = new RequiredResultCriteria
            {
                DocumentsToSearch = DocumentType.Posting,
                IncludeDuties = false,
                MaximumDocumentCount = 200,
                MinimumStarMatch = StarMatching.None
            };


            // If Career area/job jamily has been set but no occupations/ROnets have been selected then need to populate the search criteria with ALL the ROnets for that career area
            if (criteria.ROnetCriteria.IsNotNull() && criteria.ROnetCriteria.CareerAreaId.HasValue && criteria.ROnetCriteria.CareerAreaId > 0 && criteria.ROnetCriteria.ROnets.IsNullOrEmpty())
            {
                criteria.ROnetCriteria.ROnets = ServiceClientLocator.ExplorerClient(App).GetCareerAreaJobs(Convert.ToInt64(criteria.ROnetCriteria.CareerAreaId)).Select(x => x.ROnet).ToList();
            }


            return criteria;
        }

        /// <summary>
        /// Sets the do not see screen.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        private void SetDoNotSeeScreen(SearchCriteria criteria)
        {
            lblLocation.Text = CommonUtilities.GetCriteria(new SearchCriteria { JobLocationCriteria = criteria.JobLocationCriteria }).Replace("Location:", "").SetDefaultString("N/A");
            lblIndustry.Text = CommonUtilities.GetCriteria(new SearchCriteria { IndustryCriteria = criteria.IndustryCriteria }).SetDefaultString("Based on your job history");
            lblEducation.Text = CommonUtilities.GetCriteria(new SearchCriteria { EducationLevelCriteria = criteria.EducationLevelCriteria }).Replace("Education level:", "").SetDefaultString("Based on your job history");
            lblPostingDate.Text = CommonUtilities.GetCriteria(new SearchCriteria { PostingAgeCriteria = criteria.PostingAgeCriteria }).Replace("Job posted:", "").SetDefaultString("Any posting date");

            var linkVisibilty = !(criteria.InternshipCriteria.IsNotNull() && criteria.InternshipCriteria.Internship == FilterTypes.ShowOnly);
            JobResultsLevelPlaceHolder.Visible = JobTypeSearchHyperLink.Visible = JobTypeSearchHyperDiv.Visible = ExcludeJobHyperlink.Visible = linkVisibilty;

            linkVisibilty = (criteria.InternshipCriteria.IsNotNull() && criteria.InternshipCriteria.Internship != FilterTypes.Exclude);
            InternshipAreaSearchHyperlink.Visible = InternshipAreaSearchDiv.Visible = linkVisibilty;
        }

        /// <summary>
        /// Applies the theme.
        /// </summary>
        private void ApplyTheme()
        {
            SpideredJobsDisclaimerLabel.Visible = App.Settings.SearchResultsDisclaimerEnabled;
            SpideredJobsDisclaimerLabel.DefaultText = App.Settings.SearchResultsDisclaimerText;
        }

        /// <summary>
        /// Localises the UI.
        /// </summary>
        private void LocaliseUI()
        {
            rbSearchAllWords.Text = CodeLocalise("rbSearchAllWords.Text", "Include all words");
            rbSearchAnyWords.Text = CodeLocalise("rbSearchAllWords.Text", "Include any words");

            RadiusValidator.ErrorMessage = CodeLocalise("RadiusLocation.RequiredErrorMessage", "Both radius and 5 digit zip code is required");
            SearchNameRequired.ErrorMessage = CodeLocalise("SearchName.RequiredErrorMessage", "Search name is required");
            valEmailAddress.ErrorMessage = CodeLocalise("EmailAddressValidate.ErrorMessage", "Email address is invalid");
            AlertEmailRequired = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
            customValidateEmailAddress.ErrorMessage = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
            EmailSavedSearchLabel.Text = CodeLocalise("AlertWhenJobsMatching.Label", "Alert me when jobs matching my search criteria are posted.");

            ExcludeJobHyperlink.Text = HtmlLocalise("Content3-4.Label", "Hide one or more of your previous jobs when searching");

            SalarySearchHyperLink.Text = HtmlLocalise("Content4-6.Label", "Change the salary level for this search only");

            ChangeCriteriaButton.Text = CodeLocalise("ChangeCriteria.Text", "Change criteria");

            txtEmailAddress.Attributes.Add("placeholder", CodeLocalise("EmailThisAd.Label", "Email this address"));

            switch (App.Settings.Theme)
            {
                case FocusThemes.Education:
                    JobTypeSubHeading.Text = HtmlLocalise("Content3.Label", "Job type:");
                    JobTypeSearchHyperLink.Text = HtmlLocalise("Content3-2.Label", "Limit your search to a specific occupation");
                    InternshipAreaSearchHyperlink.Text = HtmlLocalise("Content3-2b.Label", "Limit your search to a specific internship area");
                    EducationSearchHyperlink.Text = HtmlLocalise("Content4-2.Label", "Change the program of study for this search only");
                    break;

                default:
                    JobTypeSubHeading.Text = HtmlLocalise("Content3.Label", "Job type and industry:");
                    JobTypeSearchHyperLink.Text = HtmlLocalise("Content3-2.Label", "Limit your search to a specific occupation, industry, and/or emerging sector");
                    EducationSearchHyperlink.Text = HtmlLocalise("Content4-2.Label", "Change the education level for this search only");
                    break;
            }
        }

        /// <summary>
        /// Gets the account types.
        /// </summary>
        private void GetAccountTypes()
        {
            AccountTypes = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.AccountTypes).ToDictionary(x => x.Id, x => x.Text);
        }

        /// <summary>
        /// Bind hyperlinks to their URLs
        /// </summary>
        private void BindHyperlinks()
        {
            JobTypeSearchHyperLink.NavigateUrl = UrlBuilder.JobSearchCriteria("Industry");
            InternshipAreaSearchHyperlink.NavigateUrl = UrlBuilder.JobSearchCriteria("InternshipArea");
            EducationSearchHyperlink.NavigateUrl = UrlBuilder.JobSearchCriteria("EducationLevel");
            ExcludeJobHyperlink.NavigateUrl = UrlBuilder.JobSearchCriteria("ExcludeJob");
            SalarySearchHyperLink.NavigateUrl = UrlBuilder.JobSearchCriteria("SalaryLevel");
        }

        /// <summary>
        /// Binds the dropdowns.
        /// </summary>
        private void BindDropdowns()
        {
            ddlRadius.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses), null, CodeLocalise("Radius.TopDefault", "- select radius -"));
            BindPosingKeywordScopeDropDown(PostingKeywordScopes.Anywhere);
            BindAlertFrequencyDropDown(EmailAlertFrequencies.Weekly);
            BindAlertFormatDropDown(EmailFormats.HTML);
        }

        /// <summary>
        /// Binds the posing keyword scope drop down.
        /// </summary>
        /// <param name="selectedValue">The selected value.</param>
        private void BindPosingKeywordScopeDropDown(PostingKeywordScopes selectedValue)
        {
            ddlSearchLocation.Items.Clear();

            ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Anywhere, "Anywhere");
            ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobDescription, "Job Description");
            ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Employer, "#BUSINESS#");
            ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobTitle, "Job Title");

            if (selectedValue.IsNotNull())
                ddlSearchLocation.SelectValue(selectedValue.ToString());
        }

        /// <summary>
        /// Binds the alert frequency drop down.
        /// </summary>
        /// <param name="selectedValue">The selected value.</param>
        private void BindAlertFrequencyDropDown(EmailAlertFrequencies selectedValue)
        {
            ddlAlertFrequency.Items.Clear();

            ddlAlertFrequency.Items.AddEnum(EmailAlertFrequencies.Daily, "Daily");
            ddlAlertFrequency.Items.AddEnum(EmailAlertFrequencies.Weekly, "Weekly");

            if (selectedValue.IsNotNull())
                ddlAlertFrequency.SelectValue(selectedValue.ToString());
        }

        /// <summary>
        /// Binds the alert format drop down.
        /// </summary>
        /// <param name="selectedValue">The selected value.</param>
        private void BindAlertFormatDropDown(EmailFormats selectedValue)
        {
            ddlAlertType.Items.Clear();

            ddlAlertType.Items.AddEnum(EmailFormats.HTML, "Text and pictures (HTML)");
            ddlAlertType.Items.AddEnum(EmailFormats.TextOnly, "Text only");

            if (selectedValue.IsNotNull())
                ddlAlertType.SelectValue(selectedValue.ToString());
        }

        /// <summary>
        /// Binds the keyword and location criteria.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        private void BindKeywordAndLocationCriteria(SearchCriteria criteria)
        {
            if (criteria.JobLocationCriteria.IsNotNull())
            {
                var radius = criteria.JobLocationCriteria.Radius;
                if (radius.IsNotNull())
                {
                    long? radiusId = 0;

                    if (radius.Distance.IsNotNull())
                    {
                        radiusId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).Where(x => x.ExternalId == radius.Distance.ToString()).Select(x => x.Id).FirstOrDefault();
                    }

                    if (radiusId > 0)
                        ddlRadius.SelectValue(radiusId.GetValueOrDefault().ToString());

                    txtZipCode.Text = radius.PostalCode;
                }
                else
                    txtZipCode.Text = "";
            }

            if (criteria.KeywordCriteria.IsNotNull())
            {
                var keywordlocation = criteria.KeywordCriteria.SearchLocation;

                if (keywordlocation.IsNotNull())
                    ddlSearchLocation.SelectValue(keywordlocation.ToString());

                SearchTermTextBox.Text = criteria.KeywordCriteria.KeywordText;

                if (criteria.KeywordCriteria.Operator == LogicalOperators.Or)
                    rbSearchAnyWords.Checked = true;
                else
                    rbSearchAllWords.Checked = true;
            }

            if (criteria.ReferenceDocumentCriteria.IsNotNull())
            {
                HasResume = 1;
            }

            if (App.IsAnonymousAccess())
            {
                if (criteria.ExcludePostingCriteria.IsNull())
                    criteria.ExcludePostingCriteria = new ExcludePostingCriteria();

                criteria.ExcludePostingCriteria.AnonymousOnly = true;
            }
        }

        /// <summary>
        /// Loads the list.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="manualSearch">The manual search.</param>
        private void LoadList(SearchCriteria criteria, bool? manualSearch = null)
        {

            try
            {
                if (!string.IsNullOrWhiteSpace(criteria.JobIdCriteria))
                {
                    var checkVeteranStatus = true;
                    if (App.User.IsNotNull() && App.User.PersonId.HasValue)
                    {
                        var person = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(App.User.PersonId.Value);
                        checkVeteranStatus = !person.IsVeteran.GetValueOrDefault(false);
                    }

                    if (!App.User.IsAuthenticated)
                    {
                        Result = new List<PostingSearchResultView>();
                        ConfirmationModal.Show();
                        return;
                    }

                    var lensPostingId = ServiceClientLocator.JobClient(App).GetJobLensPostingId(criteria.JobIdCriteria, checkVeteranStatus, new List<JobStatuses> { JobStatuses.Active, JobStatuses.Closed });
                    if (!string.IsNullOrWhiteSpace(lensPostingId))
                    {
                        var url = GetRouteUrl("JobPosting", new { jobid = lensPostingId, fromURL = "JobSearchCriteria", pageSection = "JobSearchCriteria" });
                        Response.Redirect(url);
                    }
                    else
                    {
                        Result = new List<PostingSearchResultView>();
                    }
                }
                else
                {

                    Result = ServiceClientLocator.SearchClient(App).GetSearchResults(criteria, manualSearch: (manualSearch ?? IsPostBack));
                }
                var lensPostingIds = Result.Select(x => x.Id).ToList();
                EmployerAccountTypes = ServiceClientLocator.EmployerClient(App).GetEmployerAccountTypes(lensPostingIds);

                if (App.User.IsAuthenticated)
                    ServiceClientLocator.CandidateClient(App).ResolveCandidateIssues(App.User.PersonId ?? 0, new List<CandidateIssueType>() { CandidateIssueType.NotSearchingJobs }, true);
            }
            catch (ApplicationException ex)
            {
                MasterPage.ShowError(AlertTypes.Error, ex.Message);
            }
        }

        #region ObjectDataSource Method

        /// <summary>
        /// Gets the search results.
        /// </summary>
        /// <param name="orderBy">The order by.</param>
        /// <param name="startRowIndex">Start index of the row.</param>
        /// <param name="maximumRows">The maximum rows.</param>
        /// <returns></returns>
        public List<PostingSearchResultView> GetSearchResults(string orderBy, int startRowIndex, int maximumRows)
        {
            var searchResults = new List<PostingSearchResultView>();

            if (Result.IsNotNull())
            {
                if (App.Settings.ConfidentialEmployersDefault && !App.Settings.ConfidentialEmployersEnabled && !App.User.IsShadowingUser)
                {
                    foreach (var resultItem in Result)
                    {
                        if (resultItem != null && !resultItem.IsReferred && (resultItem.OriginId.IsTalentOrigin(App.Settings)))
                        {
                            var stateName = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States)
                                                                                                                                    .Where(l => l.Key == App.Settings.DefaultStateKey)
                                                                                                                                    .Select(l => l.Text)
                                                                                                                                    .FirstOrDefault();
                            if (stateName.IsNullOrEmpty())
                                stateName = "Confidential";

                            resultItem.Employer = CodeLocalise("Global.ConfidentialEmployerLabel", "[Confidential]", stateName);
                        }
                    }
                }

                var sortDescending = false;

                if (orderBy.EndsWith(" DESC"))
                {
                    sortDescending = true;
                    orderBy = orderBy.Substring(0, orderBy.LastIndexOf(" DESC"));
                }

                switch (orderBy)
                {
                    case "Rating":
                        Result = sortDescending ? Result.OrderByDescending(x => x.Rank).ToList() : Result.OrderBy(x => x.Rank).ToList();
                        break;

                    case "Date":
                        Result = sortDescending ? Result.OrderByDescending(x => x.JobDate).ToList() : Result.OrderBy(x => x.JobDate).ToList();
                        break;

                    case "JobTitle":
                        Result = sortDescending ? Result.OrderByDescending(x => x.JobTitle).ToList() : Result.OrderBy(x => x.JobTitle).ToList();
                        break;

                    case "Employer":
                        Result = sortDescending ? Result.OrderByDescending(x => x.Employer).ToList() : Result.OrderBy(x => x.Employer).ToList();
                        break;

                    case "JobLocation":
                        Result = sortDescending ? Result.OrderByDescending(x => x.Location).ToList() : Result.OrderBy(x => x.Location).ToList();
                        break;
                }

                searchResults = (startRowIndex == 0) ? Result.Take(maximumRows).ToList() : Result.Skip(startRowIndex).Take(maximumRows).ToList();
                _searchResultCount = Result.Count();
            }

            return searchResults;
        }

        /// <summary>
        /// Gets the referrals count.
        /// </summary>
        /// <returns></returns>
        public int GetSearchResultCount()
        {
            return _searchResultCount;
        }

        #endregion

        /// <summary>
        /// Handles the Sorting event of the SearchResultListView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ListViewSortEventArgs" /> instance containing the event data.</param>
        protected void SearchResultListView_Sorting(object sender, ListViewSortEventArgs e)
        {
            var sortImageUrl = e.SortDirection == System.Web.UI.WebControls.SortDirection.Ascending ? UrlBuilder.SortUp() : UrlBuilder.SortDown();
            var ratingSortImage = (Image)SearchResultListView.FindControl("RatingSortImage");
            var dateSortImage = (Image)SearchResultListView.FindControl("DateSortImage");
            var jobTitleSortImage = (Image)SearchResultListView.FindControl("JobTitleSortImage");
            var employerSortImage = (Image)SearchResultListView.FindControl("EmployerSortImage");
            var jobLocationSortImage = (Image)SearchResultListView.FindControl("JobLocationSortImage");

            switch (e.SortExpression)
            {
                case "Rating":
                    ratingSortImage.ImageUrl = sortImageUrl;
                    ratingSortImage.Visible = true;
                    dateSortImage.Visible = false;
                    jobTitleSortImage.Visible = false;
                    employerSortImage.Visible = false;
                    jobLocationSortImage.Visible = false;
                    break;

                case "Date":
                    dateSortImage.ImageUrl = sortImageUrl;
                    ratingSortImage.Visible = false;
                    dateSortImage.Visible = true;
                    jobTitleSortImage.Visible = false;
                    employerSortImage.Visible = false;
                    jobLocationSortImage.Visible = false;
                    break;

                case "JobTitle":
                    jobTitleSortImage.ImageUrl = sortImageUrl;
                    ratingSortImage.Visible = false;
                    dateSortImage.Visible = false;
                    jobTitleSortImage.Visible = true;
                    employerSortImage.Visible = false;
                    jobLocationSortImage.Visible = false;
                    break;

                case "Employer":
                    employerSortImage.ImageUrl = sortImageUrl;
                    ratingSortImage.Visible = false;
                    dateSortImage.Visible = false;
                    jobTitleSortImage.Visible = false;
                    employerSortImage.Visible = true;
                    jobLocationSortImage.Visible = false;
                    break;

                case "JobLocation":
                    jobLocationSortImage.ImageUrl = sortImageUrl;
                    ratingSortImage.Visible = false;
                    dateSortImage.Visible = false;
                    jobTitleSortImage.Visible = false;
                    employerSortImage.Visible = false;
                    jobLocationSortImage.Visible = true;
                    break;
            }
        }

        /// <summary>
        /// Binds the search result list.
        /// </summary>
        private void BindSearchResultList()
        {
            SearchResultListView.DataBind();
            //TopPager.Visible = BottomPager.Visible = (TopPager.TotalRowCount > 0);
            BottomPager.Visible = (BottomPager.TotalRowCount > 0);
            var displayContext = App.GetSessionValue<PageDisplayContext>("Career:DisplayContext");
            if (displayContext.IsNotNull() && displayContext.displaypreference.IsNotNull() && !IsPostBack)
            {
                // Reset paging back to first page
                displayContext.displaypreference.PageNumber = 0;
                App.SetSessionValue("Career:DisplayContext", displayContext);
                BottomPager.ReturnToPage(displayContext.displaypreference.PageNumber, displayContext.displaypreference.PageSize);
            }

            BindBackToJobResultsLink();
        }

        private void BindBackToJobResultsLink()
        {
            var criteria = App.GetSessionValue<SearchCriteria>(MagicStrings.SessionKeys.CareerSearchCriteria);
            var previousCriteria = App.GetSessionValue<SearchCriteria>(MagicStrings.SessionKeys.CareerPreviousSearchCriteria);
            //			this.placeHolderBackToJobResults.Visible = previousCriteria.IsNotNull() &&
            //			                                           criteria.SearchType == PostingSearchTypes.PostingsLikeThis;
        }

        /// <summary>
        /// Handles the PagePropertiesChanging event of the SearchResultListView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.PagePropertiesChangingEventArgs"/> instance containing the event data.</param>
        protected void SearchResultListView_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            var displayContext = App.GetSessionValue<PageDisplayContext>("Career:DisplayContext", new PageDisplayContext());
            if (displayContext.IsNotNull() && displayContext.displaypreference.IsNotNull())
            {
                displayContext.displaypreference.PageNumber = e.StartRowIndex / e.MaximumRows;
                displayContext.displaypreference.PageSize = e.MaximumRows;
            }
            else
            {
                displayContext = new PageDisplayContext
                {
                    displaypreference = new DisplayPreference
                    {
                        PageNumber = e.StartRowIndex / e.MaximumRows,
                        PageSize = e.MaximumRows
                    }
                };
            }
            App.SetSessionValue("Career:DisplayContext", displayContext);
        }

        /// <summary>
        /// Handles the PageSizeChange event of the TopPager control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void TopPager_PageSizeChange(object sender, EventArgs e)
        {
            var ddlPageSizer = (DropDownList)sender;
            var pageSize = Convert.ToInt32(ddlPageSizer.SelectedValue);
            var displayContext = App.GetSessionValue<PageDisplayContext>("Career:DisplayContext", new PageDisplayContext());
            if (displayContext.IsNotNull() && displayContext.displaypreference.IsNotNull())
                displayContext.displaypreference.PageSize = pageSize;
            App.SetSessionValue("Career:DisplayContext", displayContext);
        }

        /// <summary>
        /// Gets the stars.
        /// </summary>
        /// <param name="score">The score.</param>
        /// <returns></returns>
        protected string GetStars(int score)
        {
            var starRating = score.ToStarRating(App.Settings.StarRatingMinimumScores);

            var output = string.Empty;
            for (var j = 1; j <= starRating; j++) output += "<img src='" + UrlBuilder.StarOn() + "' alt='Star on image "+ j +"' />";
            for (var j = 1; j <= 5 - starRating; j++) output += "<img src='" + UrlBuilder.StarOff() + "' alt='Star off image "+ j +"' />";

            return output;
        }

        /// <summary>
        /// Registers the javascript for recording online help usage.
        /// </summary>
        private void RegisterJavascript()
        {
            var script = @"function ShowNotWhatYoureLookingFor(sender, args)
					{
						$find('WhatYouRLookingForModal').show();
						var options = { type: ""POST"",
							url: """ + UrlBuilder.AjaxService() + @"/ShowNotWhatYoureLookingFor"",
							contentType: ""application/json; charset=utf-8"",
							dataType: ""json"",
							async: true,
							data: '{""personId"": """ + App.User.PersonId + @"""}'			
						};						

						$.ajax(options);						
					}";


            Page.ClientScript.RegisterStartupScript(GetType(), "ShowNotWhatYoureLookingFor", script, true);
        }

        /// <summary>
        /// Handles the Click event of the lnkReviewSearchCriteria control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkReviewSearchCriteria_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lblReviewSearchCriteriaInfo.Text))
            {
                var searchcriteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");
                lblReviewSearchCriteriaInfo.Text = CommonUtilities.GetCriteria(searchcriteria);
                //CriteriaCloseButton.Text = CodeLocalise("Global.Close.Text", "Close");
                //ChangeCriteriaButton.Text = CodeLocalise("ChangeCriteria.Text", "Change criteria");
                //ReviewSearchCriteriaModalPopup.Show();
                btnShow.Text = "Hide <span aria-hidden=\"true\" class=\"fa fa-caret-up\"></span>";
                pnlReviewSearchCriteriaInfo.Visible = true;
            }
            else
            {
                lblReviewSearchCriteriaInfo.Text = null;
                btnShow.Text = "Show <span aria-hidden=\"true\" class=\"fa fa-caret-down\"></span>";
                pnlReviewSearchCriteriaInfo.Visible = false;
            }
        }

        /// <summary>
        /// Displays the top section.
        /// </summary>
        private void DisplayTopSection()
        {
            SearchAndHintsPlaceHolder.Visible = App.User.IsAuthenticated && !App.User.IsAnonymous;
        }

        /// <summary>
        /// Handles the Click event of the ChangeCriteriaButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void ChangeCriteriaButton_Click(object sender, EventArgs e)
        {
            App.RemoveSessionValue(MagicStrings.SessionKeys.CareerPreviousSearchCriteria);
            Response.RedirectToRoute("JobSearchCriteria", new { Control = "changecriteria" });
        }

        /// <summary>
        /// Handles the ItemDataBound event of the SearchResultListView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ListViewItemEventArgs" /> instance containing the event data.</param>
        protected void SearchResultListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var posting = (PostingSearchResultView)e.Item.DataItem;

            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                var jobtitle = (HyperLink)e.Item.FindControl("JobTitleHyperLink");
                var amIGoodMatchHyperLink = (LinkButton)e.Item.FindControl("AmIGoodMatchHyperLink");
                var findMoreJobsHyperLink = (LinkButton)e.Item.FindControl("FindMoreJobsHyperLink");
                var doNotDisplayHyperLink = (LinkButton)e.Item.FindControl("DoNotDisplayHyperLink");

                if (App.User.IsAuthenticated)
                {
                    jobtitle.NavigateUrl = GetRouteUrl("JobPosting", new { jobid = posting.Id, fromURL = "JobSearchResults", pageSection = "JobSearchResults" });
                }
                else
                {
                    if (_postBackReferenceForNotAuth.IsNullOrEmpty())
                    {
                        var postBackOptions = new PostBackOptions(NotAuthenticatedJobButton)
                        {
                            AutoPostBack = false,
                            RequiresJavaScriptProtocol = true,
                            PerformValidation = false
                        };

                        _postBackReferenceForNotAuth = Page.ClientScript.GetPostBackEventReference(postBackOptions);
                    }
                    jobtitle.NavigateUrl = _postBackReferenceForNotAuth;
                }

                findMoreJobsHyperLink.CommandArgument = posting.Id;
                doNotDisplayHyperLink.CommandArgument = posting.Id;

                if (posting.Internship)
                    e.Item.FindControl("AmIAGoodMatchPlaceholder").Visible = false;
                else
                {
                    amIGoodMatchHyperLink.CommandArgument = posting.Id; // NavigateUrl = GetRouteUrl("JobMatching", new { jobid = posting.ID, fromURL = "JobSearchResults" });
                    var amIGoodMatchTrigger = new AsyncPostBackTrigger { ControlID = amIGoodMatchHyperLink.UniqueID };
                    SearchResultUpdatePanel.Triggers.Add(amIGoodMatchTrigger);
                }

                var findMoreJobsTrigger = new AsyncPostBackTrigger { ControlID = findMoreJobsHyperLink.UniqueID };
                SearchResultUpdatePanel.Triggers.Add(findMoreJobsTrigger);

                var doNotDisplayTrigger = new AsyncPostBackTrigger { ControlID = doNotDisplayHyperLink.UniqueID };
                SearchResultUpdatePanel.Triggers.Add(doNotDisplayTrigger);

                // apply row shading to Talent jobs to make them stand out from spidered jobs
                // if posting.OriginId = 999 then it's a spidered job, else it's Talent
                if (posting.OriginId.IsTalentOrigin(App.Settings))
                {
                    ((HtmlTableRow)e.Item.FindControl("JobSearchResultRow")).Attributes.Add("class", "nonSpideredJobSearchResult");
                    // FVN-1085
                    var nonSpideredJobIcon = (Image)e.Item.FindControl("NonSpideredJobIcon");
                    nonSpideredJobIcon.Visible = true;
                    nonSpideredJobIcon.ToolTip = CodeLocalise("NonSpideredJobIcon.Tooltip",
                        "This job has been posted directly by a #BUSINESS#:LOWER registered with this website");
                }

                if (App.Settings.JobDescriptionCharsShown > 0)
                {
                    var jobDescriptionLabel = (Label)e.Item.FindControl("JobDescriptionLabel");
                    var jobDescription = posting.Duties.IsNotNullOrEmpty() && posting.Duties.Count > 0 ? posting.Duties.ToConcatenatedString(Environment.NewLine) : string.Empty;

                    var length = jobDescription.Length > App.Settings.JobDescriptionCharsShown ? App.Settings.JobDescriptionCharsShown : jobDescription.Length;
                    jobDescriptionLabel.Text = jobDescription.Substring(0, length);
                }

                var yearsOfExperienceLabel = (Label)e.Item.FindControl("YearsOfExperienceLabel");

                if (App.Settings.ShowYearsOfExperienceInPostingResults)
                {
                    var minExperience = posting.MinExperience;

                    if (minExperience.IsNotNull() && minExperience > 0)
                    {
                        var years = Math.Truncate(Convert.ToDecimal(minExperience / 12));
                        var months = minExperience % 12;

                        string yearsText;
                        if (years == 1)
                            yearsText = CodeLocalise("OneYear.Text", "1 yr");
                        else if (years > 1)
                            yearsText = CodeLocalise("MoreYears.Text", "{0} yrs", years);
                        else
                            yearsText = "";

                        string monthsText;
                        if (months == 1)
                            monthsText = CodeLocalise("OneMonth.Text", "1 month");
                        else if (months > 1)
                            monthsText = CodeLocalise("MoreMonths.Text", "{0} months", months);
                        else
                            monthsText = "";

                        yearsOfExperienceLabel.Text = CodeLocalise("YearsOfExperienceLabel.Text", "{0} {1}", yearsText, monthsText);
                    }
                    else
                    {
                        yearsOfExperienceLabel.Visible = false;
                    }
                }
                else
                {
                    var yearsOfExperienceCell = (HtmlTableCell)e.Item.FindControl("YearsOfExperienceCell");
                    yearsOfExperienceCell.Visible = false;
                    yearsOfExperienceLabel.Visible = false;
                }

                if (App.Settings.AccountTypeDisplayType == ControlDisplayType.Mandatory || App.Settings.AccountTypeDisplayType == ControlDisplayType.Optional)
                {
                    if (EmployerAccountTypes.IsNotNullOrEmpty())
                    {
                        if (EmployerAccountTypes.ContainsKey(posting.Id))
                        {
                            var employerAccountTypeId = EmployerAccountTypes[posting.Id];

                            var accountType = string.Empty;

                            if (employerAccountTypeId.IsNotNull() && AccountTypes.ContainsKey(Convert.ToInt64(employerAccountTypeId)))
                                accountType = AccountTypes[Convert.ToInt64(employerAccountTypeId)];

                            var talentAccountTypeLabel = (Label)e.Item.FindControl("TalentAccountTypeLabel");
                            talentAccountTypeLabel.Text = accountType.IsNotNullOrEmpty() ? CodeLocalise("TalentAccountTypeLabel.Text", "({0})", accountType) : "";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the LayoutCreated event of the SearchResultListView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void SearchResultListView_LayoutCreated(object sender, EventArgs e)
        {
            if (!App.Settings.ShowYearsOfExperienceInPostingResults)
            {
                var yearsOfExperienceHeader = (HtmlTableCell)SearchResultListView.FindControl("YrsOfExperienceHeader");
                yearsOfExperienceHeader.Visible = false;
            }
            var ratingToolTip = (HtmlGenericControl)SearchResultListView.FindControl("RatingToolTip");
            if (ratingToolTip.IsNotNull())
            {
                ratingToolTip.Attributes.Add("title", CodeLocalise("Rating.ToolTip", "Ratings are based on your skills, qualifications and work experience, not just job titles and keywords. As such, ratings are not available when you are searching without matching to your resume."));
            }
        }

        /// <summary>
        /// Handles the Click event of the lnkUIClaimantComplete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkUIClaimantComplete_Click(object sender, EventArgs e)
        {
            try
            {
                //TODO: Martha (UI Claimant)
                var logout = (LogOut)this.MasterPage.FindControl("MainLogOut");
                logout.LogOutSession(false);
                //Response.Redirect(CareerApp.Settings.UIClaimantRedirectURL, true);
            }
            catch (ApplicationException ex)
            {
                MasterPage.ShowError(AlertTypes.Error, ex.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the AmIGoodMatchHyperLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void AmIGoodMatchHyperLink_Click(object sender, EventArgs e)
        {
            if (App.User.IsAuthenticated)
            {
                var lbtnName = (LinkButton)sender;
                var lensPostingId = lbtnName.CommandArgument;

                var post = (lensPostingId.IsNotNullOrEmpty()) ? ServiceClientLocator.PostingClient(App).GetJobPosting(lensPostingId) : null;

                /*03.Feb.2016 - KRP
                * Bugfix: included lens posting id in the session variable to avoid mismatch in the job posting.
                * Also updated the same for get methods*/
                if (post.IsNotNull() && post.PostingXml.IsNotNullOrEmpty())
                    App.SetSessionValue("Career:XMLPosting:" + lbtnName.CommandArgument, post.PostingXml);

                ServiceClientLocator.CoreClient(App).SaveSelfService(ActionTypes.ViewJobInsights, App.User.UserId);

                Response.RedirectToRoute("JobMatching", new { jobid = lbtnName.CommandArgument, fromURL = "JobSearchResults" });
            }
            else
                ConfirmationModal.Show();
        }

        /// <summary>
        /// Handles the Click event of the FindMoreJobsHyperLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void FindMoreJobsHyperLink_Click(object sender, EventArgs e)
        {
            var criteria = App.GetSessionValue<SearchCriteria>(MagicStrings.SessionKeys.CareerSearchCriteria);
            App.SetSessionValue(MagicStrings.SessionKeys.CareerPreviousSearchCriteria, criteria.CloneObject());
            var lbtnName = (LinkButton)sender;
            criteria = new SearchCriteria
            {
                JobLocationCriteria = criteria.JobLocationCriteria,
                ReferenceDocumentCriteria = new ReferenceDocumentCriteria
                {
                    DocumentType = DocumentType.Posting,
                    DocumentId = lbtnName.CommandArgument
                },
                SearchType = PostingSearchTypes.PostingsLikeThis
            };
            App.SetSessionValue(MagicStrings.SessionKeys.CareerSearchCriteria, criteria);
            LoadList(criteria, true);
            BindSearchResultList();
        }

        /// <summary>
        /// Handles the Click event of the DoNotDisplayHyperLink control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void DoNotDisplayHyperLink_Click(object sender, EventArgs e)
        {
            if (App.User.IsAuthenticated)
            {
                var lbtnName = (LinkButton)sender;

                try
                {
                    var lensPostingId = lbtnName.CommandArgument;

                    if (lensPostingId.IsNotNullOrEmpty() && ServiceClientLocator.OrganizationClient(App).SetDoNotDisplay(lensPostingId))
                    {
                        Result = Result.Where(x => x.Id != lbtnName.CommandArgument.ToString()).ToList();
                        BindSearchResultList();
                    }
                }
                catch (ApplicationException ex)
                {
                    MasterPage.ShowError(AlertTypes.Error, ex.Message);
                }
            }
            else
                ConfirmationModal.Show();
        }

        /// <summary>
        /// Handles the Click event of the btnGO control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnGO_Click(object sender, EventArgs e)
        {
            var criteria = App.GetSessionValue<SearchCriteria>(MagicStrings.SessionKeys.CareerSearchCriteria, new SearchCriteria());
            //AIM - FVN-4648-UserName in bubble is not changed while accessing different JS in different tab
            Response.RedirectToRoute("JobSearchResults");
            #region Job Location Criteria

            //criteria.Remove(criteria.Where(x => x is JobLocationCriterion).FirstOrDefault());
            if (txtZipCode.TextTrimmed() != "")
            {
                if (criteria.JobLocationCriteria.IsNull())
                    criteria.JobLocationCriteria = new JobLocationCriteria();

                var joblocation = criteria.JobLocationCriteria;

                // Reset any state/area criteria
                joblocation.Area = null;
                joblocation.SelectedStateInCriteria = null;

                var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).FirstOrDefault(x => x.Id == ddlRadius.SelectedValueToLong());
                if (lookup != null)
                {
                    var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
                    var radius = radiusExternalId.ToLong();

                    joblocation.Radius = new RadiusCriteria
                    {
                        Distance = (radius.HasValue) ? radius.Value : 0,
                        DistanceUnits = DistanceUnits.Miles,
                        PostalCode = txtZipCode.TextTrimmed()
                    };
                }
            }
            else
            {
                var joblocation = criteria.JobLocationCriteria;

                if (joblocation.IsNotNull())
                    joblocation.Radius = null;
            }

            #endregion

            #region Keyword Criteria

            criteria.KeywordCriteria = null;

            if (SearchTermTextBox.TextTrimmed() != "")
            {
                var searchLocation = ddlSearchLocation.SelectedValueToEnum(PostingKeywordScopes.Anywhere);
                criteria.KeywordCriteria = new KeywordCriteria(SearchTermTextBox.TextTrimmed())
                        {
                            SearchLocation = searchLocation,
                            Operator = rbSearchAllWords.Checked ? LogicalOperators.And : LogicalOperators.Or
                        };
            }

            #endregion

            App.SetSessionValue(MagicStrings.SessionKeys.CareerSearchCriteria, criteria);
            App.RemoveSessionValue(MagicStrings.SessionKeys.CareerPreviousSearchCriteria);
            LoadList(criteria, true);
            BindSearchResultList();

            // Update the Review Search Criteria Info
            if (pnlReviewSearchCriteriaInfo.Visible) lblReviewSearchCriteriaInfo.Text = CommonUtilities.GetCriteria(criteria);
        }

        /// <summary>
        /// Handles the Click event of the btnSaveSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void btnSaveSearch_Click(object sender, EventArgs e)
        {
            try
            {
                var searchCriteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");

                var alertSearchCriteria = searchCriteria.CloneObject<SearchCriteria>();
                var tempCriterion = alertSearchCriteria.RequiredResultCriteria;

                if (tempCriterion.IsNull())
                {
                    var minimumStarScoreForSearch = StarMatching.ZeroStar;

                    if (App.Settings.StarRatingMinimumScores.Length > 5 && App.Settings.MinimumScoreForSearch >= App.Settings.StarRatingMinimumScores[5])
                        minimumStarScoreForSearch = StarMatching.FiveStar;
                    else if (App.Settings.StarRatingMinimumScores.Length > 4 && App.Settings.MinimumScoreForSearch >= App.Settings.StarRatingMinimumScores[4])
                        minimumStarScoreForSearch = StarMatching.FourStar;
                    else if (App.Settings.StarRatingMinimumScores.Length > 3 && App.Settings.MinimumScoreForSearch >= App.Settings.StarRatingMinimumScores[3])
                        minimumStarScoreForSearch = StarMatching.ThreeStar;
                    else if (App.Settings.StarRatingMinimumScores.Length > 2 && App.Settings.MinimumScoreForSearch >= App.Settings.StarRatingMinimumScores[2])
                        minimumStarScoreForSearch = StarMatching.TwoStar;
                    else if (App.Settings.StarRatingMinimumScores.Length > 1 && App.Settings.MinimumScoreForSearch >= App.Settings.StarRatingMinimumScores[1])
                        minimumStarScoreForSearch = StarMatching.OneStar;

                    alertSearchCriteria.RequiredResultCriteria = new RequiredResultCriteria
                    {
                        DocumentsToSearch = DocumentType.Posting,
                        MinimumStarMatch = minimumStarScoreForSearch,
                        MaximumDocumentCount = App.Settings.MaximumNoDocumentToReturnInJobAlert
                    };
                }
                else
                    tempCriterion.MaximumDocumentCount = App.Settings.MaximumNoDocumentToReturnInJobAlert;

                var format = EmailFormats.HTML;
                var frequency = EmailAlertFrequencies.Weekly;
                var emailAddress = "";

                if (EmailSavedSearchCheckbox.Checked)
                {
                    if (!string.IsNullOrEmpty(ddlAlertType.SelectedValue))
                        format = ddlAlertType.SelectedValue.AsEnum(EmailFormats.HTML);
                    if (!string.IsNullOrEmpty(ddlAlertFrequency.SelectedValue))
                        frequency = ddlAlertFrequency.SelectedValue.AsEnum(EmailAlertFrequencies.Weekly);

                    emailAddress = txtEmailAddress.Text.Trim();
                }

                var status = ServiceClientLocator.AnnotationClient(App).SaveSearch(0, txtSearchName.TextTrimmed(), alertSearchCriteria, SavedSearchTypes.CareerPostingSearch, frequency, format, emailAddress, AlertStatus.Active, EmailSavedSearchCheckbox.Checked);

                if (status)
                    MasterPage.ShowModalAlert(AlertTypes.Info, "'" + txtSearchName.Text.Trim() + "' search has been saved.", "Search saved");
            }
            catch (ApplicationException ex)
            {
                MasterPage.ShowError(AlertTypes.Error, ex.Message);
            }
        }

        /// <summary>
        /// Handles the Click event of the LnkSaveSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void LnkSaveSearch_Click(object sender, EventArgs e)
        {
            try
            {
                var searchcriteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");
                lblSearchCriteria.Text = CommonUtilities.GetCriteria(searchcriteria);

                SaveSearchModal.Show();
            }
            catch (ApplicationException ex)
            {
                MasterPage.ShowError(AlertTypes.Error, ex.Message);
            }
        }

        /// <summary>
        /// Handles the OkCommand event of the ReviewSearchCriteriaConfirmationModal control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
        public void ReviewSearchCriteriaConfirmationModal_OkCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "REVIEW_CRITERIA")
                Response.RedirectToRoute("JobSearchCriteria", new { Control = "searchjobpostings" });
            if (e.CommandName == "WIDEN_SEARCH")
            {
                var searchCriteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");
                if (searchCriteria.JobLocationCriteria.Radius.IsNotNull())
                {
                    long stateId, countyId;
                    string cityName;
                    ServiceClientLocator.CoreClient(App).GetStateCityAndCountyForPostalCode(searchCriteria.JobLocationCriteria.Radius.PostalCode,
                        out stateId, out countyId, out cityName);
                    searchCriteria.JobLocationCriteria.Radius = null;
                    searchCriteria.JobLocationCriteria.Area = new AreaCriteria { StateId = stateId, MsaId = 0 };
                    var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Id == stateId);

                    if (lookup.IsNotNull())
                        searchCriteria.JobLocationCriteria.SelectedStateInCriteria = lookup.ExternalId;
                }
                else if (searchCriteria.JobLocationCriteria.Area.IsNotNull() && searchCriteria.JobLocationCriteria.Area.MsaId != 0)
                {
                    searchCriteria.JobLocationCriteria.Area.MsaId = 0;
                }

                BindDropdowns();

                BindKeywordAndLocationCriteria(searchCriteria);
                LoadList(searchCriteria, true);

                BindSearchResultList();
                lblReviewSearchCriteriaInfo.Text = null;
                btnShow.Text = "Show <span aria-hidden=\"true\" class=\"fa fa-caret-down\"></span>";
            }
        }

        /// <summary>
        /// Handles the Click event of the LnkStatewideMatches control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void LnkStatewideMatches_Click(object sender, EventArgs e)
        {
            var searchCriteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");
            if (searchCriteria.JobLocationCriteria.IsNull())
            {
                MasterPage.ShowModalAlert(AlertTypes.Info,
                                                                    CodeLocalise("SearchingNationWideAlready.AlertText",
                        "Your location search criteria contains no geographical filters so cannot be expanded any further."));
                return;
            }
            else if (searchCriteria.JobLocationCriteria.Radius.IsNotNull())
            {
                long stateId, countyId;
                string cityName;
                ServiceClientLocator.CoreClient(App).GetStateCityAndCountyForPostalCode(searchCriteria.JobLocationCriteria.Radius.PostalCode,
                            out stateId, out countyId, out cityName);
                searchCriteria.JobLocationCriteria.Radius = null;
                searchCriteria.JobLocationCriteria.Area = new AreaCriteria { StateId = stateId, MsaId = 0 };
                var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Id == stateId);

                if (lookup.IsNotNull())
                    searchCriteria.JobLocationCriteria.SelectedStateInCriteria = lookup.ExternalId;
            }
            else if (searchCriteria.JobLocationCriteria.Area.IsNotNull() && searchCriteria.JobLocationCriteria.Area.MsaId != 0)
            {
                searchCriteria.JobLocationCriteria.Area.MsaId = 0;
            }
            else if (searchCriteria.JobLocationCriteria.Area.IsNull() && searchCriteria.JobLocationCriteria.Radius.IsNull())
            {
                MasterPage.ShowModalAlert(AlertTypes.Info,
                                          CodeLocalise("SearchingNationWideAlready.AlertText",
                                "Your location search criteria is already searching nationwide and cannot be widened any further."));
                return;
            }
            else
            {
                MasterPage.ShowModalAlert(AlertTypes.Info,
                                          CodeLocalise("SearchingStateWideAlready.AlertText",
                                "Your location search criteria is already searching statewide and cannot be widened any further. To search nationwide, choose to change your current criteria and remove any geographical filters being used."));
                return;
            }

            SearchCriteriaErrorModal.Show("Confirm", "Do you want to expand the search to include state-wide jobs?", "Cancel", string.Empty, string.Empty, "Search", "WIDEN_SEARCH");

        }

        /// <summary>
        /// Handles the Click event of the DontSeeWhatYourLookingFor control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void DontSeeWhatYourLookingFor_Click(object sender, EventArgs e)
        {
            Console.WriteLine("here");
        }

        /// <summary>
        /// Handles the Click event of the LnkReturnToExplorers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void LnkReturnToExplorers_Click(object sender, EventArgs e)
        {
            var bookmark = App.GetSessionValue<LightboxEventArgs>("Explorer:ReturnBookmark");
            App.SetSessionValue("Explorer:OpenBookmark", bookmark);
            App.RemoveSessionValue("Explorer:ReturnBookmark");
            Response.Redirect(bookmark.IsNotNull() ? bookmark.CommandArgument.LightboxLocation : UrlBuilder.Explore());
        }

        protected void lnkBackToJobResults_OnClick(object sender, EventArgs e)
        {
            var previousCriteria = App.GetSessionValue<SearchCriteria>(MagicStrings.SessionKeys.CareerPreviousSearchCriteria);
            App.SetSessionValue(MagicStrings.SessionKeys.CareerSearchCriteria, previousCriteria);
            App.RemoveSessionValue(MagicStrings.SessionKeys.CareerPreviousSearchCriteria);
            LoadList(previousCriteria, false);
            BindSearchResultList();
        }

        /// <summary>
        /// Hidden button used to display "Not Authenticated" modal when a job is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void NotAuthenticatedJobButton_OnClick(object sender, EventArgs e)
        {
            ConfirmationModal.Show();
        }
    }
}
