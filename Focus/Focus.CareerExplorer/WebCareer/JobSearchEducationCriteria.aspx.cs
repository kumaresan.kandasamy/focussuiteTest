﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

using Focus.CareerExplorer.Code;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Common.Extensions;
using Focus.Common;
using Focus.Services.Core;
using DistanceUnits = Focus.Core.Models.Career.DistanceUnits;

using Framework.Core;
using Framework.DataAccess;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
  public partial class JobSearchEducationCriteria : CareerExplorerPageBase
	{
	  private ResumeModel _defaultResume;

	  private ResumeModel DefaultResume
	  {
	    get { return _defaultResume ?? (_defaultResume = ServiceClientLocator.ResumeClient(App).GetDefaultResume()); }
	  }

		public string IsAuthenticated
		{
			get { return GetViewStateValue("SearchJob:IsAuthenticated", string.Empty); }
			set { SetViewStateValue("SearchJob:IsAuthenticated", value); }
		}

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
	  protected override void OnInit(EventArgs e)
	  {
			RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education);

	    base.OnInit(e);
	  }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
	  protected void Page_Load(object sender, EventArgs e)
    {
	    if (!App.Settings.TalentModulePresent)
				JobsAndIntershipsTable.Style.Add("display", "none");
			
			var lsSubTab = Page.RouteData.Values["Control"] as string;
			if (lsSubTab != null)
			{
				if (lsSubTab == "searchjobpostings" || lsSubTab == "changecriteria")
				{
					SearchJobPostings.Visible = true;
					SearchJobPostings.SearchJobPostingsClick += OnSearchJobPostingsClick;
				}
				else if (lsSubTab == "savedsearch")
				{
					if (IsPostBack)
					{
						var searchCriteria = BuildSearchExpression();
						if (searchCriteria.IsNotNull())
							App.SetSessionValue("Career:SearchCriteria", searchCriteria);
					}
					SavedSearch.Visible = true;
				}
				else
					DoNotSeeSearch(lsSubTab);
			}
			else
				SearchJobPostings.Visible = true;

			if (!IsPostBack)
			{
        if (App.GuideToResume())
          Response.Redirect(UrlBuilder.YourResume());

				LocaliseUI();
				HideControl();
				BindDropdowns();
        BindSearchExpression(lsSubTab == "changecriteria");

				var postingSearchType = App.GetCookieValue("Career:PostingSearchInternshipFilter");

				if (postingSearchType.IsNotNull())
				{
					InternshipFilterDropDown.SelectValue(postingSearchType);

					App.RemoveCookieValue("Career:PostingSearchInternshipFilter");
				}
			}
			else
			{
        // need to rebind the degree drop down as we are not using ajaxtoolkit cascading drop downs
        BindStudyProgramAreaDegreesDropDown();
			}

		  RegisterJavascript();
		}

		/// <summary>
		/// Does the not see search.
		/// </summary>
		/// <param name="tab">The tab.</param>
		private void DoNotSeeSearch(string tab)
		{
			SearchJobPostings.Visible = true;
			SearchJobPostings.SearchJobPostingsClick += OnSearchJobPostingsClick;

			switch (tab)
			{
				case "Industry":
					accOccupationTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accOccupationContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;

				case "ExcludeJob":
					accExcludeTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accExcludeContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;

				case "IgnoreResume":
					accResumeMatchingTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accResumeMatchingContent.Attributes.Add("class", "collapsableAccordionContent open");
			    break;

				case "PostingDate":
					accPostingTitle.Attributes.Add("class", "collapsableAccordionTitle on");
					accPostingContent.Attributes.Add("class", "collapsableAccordionContent open");
					break;

        case "InternshipArea":
          accInternshipSearchTitle.Attributes.Add("class", "collapsableAccordionTitle on");
          accInternshipSearchContent.Attributes.Add("class", "collapsableAccordionContent open");
          break;
      }
		}

		/// <summary>
		/// Localises the UI.
		/// </summary>
		private void LocaliseUI()
		{
      rbSearchAllWords.Text = CodeLocalise("rbSearchAllWords.Text", "Include all words");
      rbSearchAnyWords.Text = CodeLocalise("rbSearchAllWords.Text", "Include any words");

      RadiusValidator.ErrorMessage = CodeLocalise("RadiusLocation.RequiredErrorMessage", "Both radius and 5 digit zip code is required");
		  OnlyForSchoolCheckBox.Text = CodeLocalise("OnlyShowPostingsForSchool", "Only show postings exclusively available for #SCHOOLNAME#");
			rbMSAState.Text = CodeLocalise("JobSearchEducationCriteria.rbMSAState.Text", "Search this state/city");
		}

		/// <summary>
		/// Binds the dropdowns.
		/// </summary>
		private void BindDropdowns()
		{
			try
			{
				BindStarMatchDropDown(StarMatching.ThreeStar);
        if (App.Settings.ShowProgramArea)
          BindStudyProgramAreaDropDown();
        else
        {
          BindDetailedDegreeLevelDropDownList();
          JobSearchDetailedDegreeAjaxDropDownList.Items.AddLocalisedTopDefault("DetailsDegrees.SelectDegree", "- select degree -");
        }
        BindInternshipFilterDropDown();

				ddlRadius.BindLookup(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses), null, CodeLocalise("Radius.TopDefault", "- select radius -"));
				BindPosingKeywordScopeDropDown(PostingKeywordScopes.Anywhere);

				var postingAgeLookups = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges);
        var defaultPostingAgeLookup = postingAgeLookups.FirstOrDefault(x => x.ExternalId == App.Settings.JobSearchPostingDate.ToString(CultureInfo.InvariantCulture));
        ddlPostingAge.BindLookup(postingAgeLookups, (defaultPostingAgeLookup.IsNotNull()) ? defaultPostingAgeLookup.Id.ToString(CultureInfo.InvariantCulture) : "7", null);
				
				if (App.User.IsAuthenticated)
				{
					IsAuthenticated = App.User.IsAuthenticated.ToString();

					var resumeId = ServiceClientLocator.ResumeClient(App).GetDefaultResumeId();
					var distinctJobs = ServiceClientLocator.ResumeClient(App).GetDistinctJobs(resumeId);

					if(distinctJobs.IsNotNullOrEmpty())
					{
						cblExcludeJobs.DataSource = distinctJobs;
						cblExcludeJobs.DataValueField = "Key";
						cblExcludeJobs.DataTextField = "Value";
						cblExcludeJobs.DataBind();

						ExcludeJobsTable.Visible = !App.Settings.CareerHideSearchJobsByResume;
					}
					else
						ExcludeJobsTable.Visible = false;
				}

				BindJobFamilyDropDown();
				ddlOccupation.Items.AddLocalisedTopDefault("Occupations.SelectOccupation", "- select occupation -");
        
				var nearbyStates = (from s in ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States) where App.Settings.NearbyStateKeys.Contains(s.Key) select s).ToList();
				ddlState.BindLookup(nearbyStates, null, CodeLocalise("State.TopDefault", "- select state -"), "0");
			}
			catch (ApplicationException ex)
			{
				MasterPage.ShowError(AlertTypes.Error, ex.Message);
			}
		}

		/// <summary>
		/// Binds the job family drop down.
		/// </summary>
		private void BindJobFamilyDropDown()
		{
			ddlJobFamily.DataSource = ServiceClientLocator.ExplorerClient(App).GetCareerAreas();
			ddlJobFamily.DataTextField = "Name";
			ddlJobFamily.DataValueField = "Id";
			ddlJobFamily.DataBind();
			ddlJobFamily.Items.AddLocalisedTopDefault("JobFamily.TopDefault", "- select job family -", "0");
		}

		/// <summary>
		/// Binds the star match drop down.
		/// </summary>
		/// <param name="selectedValue">The selected value.</param>
		private void BindStarMatchDropDown(StarMatching selectedValue)
		{
			ddlStarMatch.Items.Clear();

			ddlStarMatch.Items.AddEnum(StarMatching.OneStar, "1-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.TwoStar, "2-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.ThreeStar, "3-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.FourStar, "4-star match");
			ddlStarMatch.Items.AddEnum(StarMatching.FiveStar, "5-star match");

			if(selectedValue.IsNotNull())
				ddlStarMatch.SelectValue(selectedValue.ToString());
		}

		/// <summary>
		/// Binds the internship filter drop down.
		/// </summary>
		private void BindInternshipFilterDropDown()
		{
			InternshipFilterDropDown.Items.Clear();
			InternshipFilterDropDown.Items.AddEnum(FilterTypes.Exclude, "Jobs");
			InternshipFilterDropDown.Items.AddEnum(FilterTypes.ShowOnly, "Internships");
			InternshipFilterDropDown.Items.AddEnum(FilterTypes.NoFilter, "Jobs and Internships");
		}

    /// <summary>
    /// Binds the program of study dropdowns.
    /// </summary>
    private void BindStudyProgramAreaDropDown()
    {
      StudyProgramAreaDropDownList.DataSource = Utilities.GetProgramAreas();
      StudyProgramAreaDropDownList.DataTextField = "Name";
      StudyProgramAreaDropDownList.DataValueField = "Id";
      StudyProgramAreaDropDownList.DataBind();
      StudyProgramAreaDropDownList.Items.AddLocalisedTopDefault("Global.ProgramArea.TopDefault", "- select department -");

      StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");
    }

    /// <summary>
    /// Binds the study program area degrees drop down.
		/// </summary>
    private void BindStudyProgramAreaDegreesDropDown()
    {
      if (!String.IsNullOrEmpty(StudyProgramAreaDropDownList.SelectedValue))
      {
        StudyProgramAreaDegreesDropDownList.DataSource = ServiceClientLocator.ExplorerClient(App).GetProgramAreaDegreeEducationLevels(StudyProgramAreaDropDownList.SelectedValueToLong());
				StudyProgramAreaDegreesDropDownList.DataTextField = "DegreeEducationLevelName";
				StudyProgramAreaDegreesDropDownList.DataValueField = "DegreeEducationLevelId";
        StudyProgramAreaDegreesDropDownList.DataBind();
        StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.AllDegrees", "All Degrees", string.Concat("-", StudyProgramAreaDropDownList.SelectedValue));
        StudyProgramAreaDegreesDropDownList.Items.AddLocalisedTopDefault("Global.Degree.TopDefault", "- select degree -");

        var selectedDegree = Request.Form[StudyProgramAreaDegreesDropDownList.UniqueID];
        if (selectedDegree.IsNotNullOrEmpty())
        {
          StudyProgramAreaDegreesDropDownList.SelectedValue = selectedDegree;
        }
      }
    }

    /// <summary>
    /// Binds the detailed degree level drop down list.
    /// </summary>
    private void BindDetailedDegreeLevelDropDownList()
    {
      Utilities.PopulateDegreeLevelDropdown(DetailedDegreeLevelDropDownList,
                                            CodeLocalise("CertificateOrAssociate", "Certificate or Associate"),
                                            CodeLocalise("Bachelors", "Bachelors"),
                                            CodeLocalise("GraduateOrProfessional", "Graduate or Professional"),
                                            true);
    }

    /// <summary>
    /// Registers the javascript.
    /// </summary>
    private void RegisterJavascript()
    {
      var js = String.Format(
        @"
$(document).ready(function() {{
	$('#{0}').change(function () {{
		var selectedIndex = ($(this).prop('selectedIndex') > 0) ? 1 : -1;

		LoadCascadingDropDownList($(this).val(),'{1}','{2}','{3}','{4}','{5}', selectedIndex);
	}});
}});",
				StudyProgramAreaDropDownList.ClientID, StudyProgramAreaDegreesDropDownList.ClientID, String.Concat(UrlBuilder.AjaxService(), "/GetDegreeEducationLevelsByProgramArea"), "Degrees",
        CodeLocalise("Global.Area.TopDefault", "- select degree -"),
        CodeLocalise("LoadingAreas.Text", "[Loading degrees...]"));

      Page.ClientScript.RegisterStartupScript(GetType(), ClientID + "StudyProgramAreaDropDownListOnChange", js, true);
    }

		/// <summary>
		/// Binds the posing keyword scope drop down.
		/// </summary>
		/// <param name="selectedValue">The selected value.</param>
		private void BindPosingKeywordScopeDropDown(PostingKeywordScopes selectedValue)
		{
			ddlSearchLocation.Items.Clear();

			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Anywhere, "Anywhere");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobDescription, "Job Description");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.Employer, "#BUSINESS#");
			ddlSearchLocation.Items.AddEnum(PostingKeywordScopes.JobTitle, "Job Title");

			if (selectedValue.IsNotNull())
				ddlSearchLocation.SelectValue(selectedValue.ToString()); 
		}

    /// <summary>
    /// Called when [search job postings click].
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		public void OnSearchJobPostingsClick(object sender, EventArgs e)
		{
			var sExp = BuildSearchExpression();
			App.SetSessionValue("Career:SearchCriteria", sExp);
			App.SetCookieValue("Career:ManualSearch", "true");
			Response.RedirectToRoute("JobSearchResults");
		}

    /// <summary>
    /// Builds the search expression.
    /// </summary>
    /// <returns></returns>
		private SearchCriteria BuildSearchExpression()
		{
			var criteria = new SearchCriteria();

      #region EducationSchool Criteria
     
      criteria.EducationSchoolCriteria = new EducationSchoolCriteria { OnlyShowExclusivePostings = OnlyForSchoolCheckBox.Checked };

      #endregion

			#region Job Matching Criteria
      
			criteria.RequiredResultCriteria = new RequiredResultCriteria
			                                  	{
			                                  		DocumentsToSearch = DocumentType.Posting,
			                                  		MinimumStarMatch = rbStarMatch.Checked ? ddlStarMatch.SelectedValueToEnum(StarMatching.ZeroStar) : StarMatching.None,
			                                  		MaximumDocumentCount = App.Settings.MaximumNoDocumentToReturnInSearch
			                                  	};

			#endregion

			#region Job Location Criteria

			var joblocation = new JobLocationCriteria();

      if (!App.Settings.NationWideInstance)
      {
				joblocation.Area = new AreaCriteria
				{
				  StateId = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).Where(x => x.Key == App.Settings.DefaultStateKey).Select(x => x.Id).FirstOrDefault(),MsaId = 0,City = ""};

        var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Key == App.Settings.DefaultStateKey);

        if (lookup.IsNotNull())
          joblocation.SelectedStateInCriteria = lookup.ExternalId;
      }

      if (rbArea.Checked && txtZipCode.TextTrimmed() != "")
			{
				var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses).FirstOrDefault(x => x.Id == ddlRadius.SelectedValueToLong());
				var radiusExternalId = (lookup.IsNotNull()) ? lookup.ExternalId : "0";
				var radiusValue = radiusExternalId.ToLong();

				var radius = new RadiusCriteria
				             	{
				             		Distance = (radiusValue.HasValue) ? radiusValue.Value : 0,
				             		DistanceUnits = DistanceUnits.Miles,
				             		PostalCode = txtZipCode.TextTrimmed()
				             	};
				joblocation.Radius = radius;

			}
			else if (rbMSAState.Checked && (ddlState.SelectedValue != "0" || ddlMSA.SelectedValue != ""))
			{
        var stateId = ddlState.SelectedValueToLong();

			  var area = new AreaCriteria
			                        {
			                          StateId = stateId,
			                          MsaId = ddlMSA.SelectedValueToLong()
			                        };
				joblocation.Area = area;

				var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.States).FirstOrDefault(x => x.Id == ddlState.SelectedValueToLong());
				
				if(lookup.IsNotNull())
					joblocation.SelectedStateInCriteria = lookup.ExternalId;
			}

      //if (!rbArea.Checked && !rbMSAState.Checked)
      //  Utilities.BuildDefaultSearchLocationCriteria(joblocation);

      criteria.JobLocationCriteria = joblocation;

			#endregion

			#region Keyword Criteria

			if (SearchTermTextBox.TextTrimmed() != "")
			{
				var searchLocation = ddlSearchLocation.SelectedValueToEnum(PostingKeywordScopes.Anywhere);
        criteria.KeywordCriteria = new KeywordCriteria(SearchTermTextBox.TextTrimmed())
        {
          SearchLocation = searchLocation,
          Operator = rbSearchAllWords.Checked ? LogicalOperators.And : LogicalOperators.Or
        };
			}

			#endregion

			#region Posting Age Criteria

			var postingAgeLookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges).FirstOrDefault(x => x.Id == ddlPostingAge.SelectedValueToLong());
			var selectedPostingAge = (postingAgeLookup.IsNotNull()) ? postingAgeLookup.ExternalId : String.Empty;

			if (selectedPostingAge.IsNotNullOrEmpty())
			{
				criteria.PostingAgeCriteria = new PostingAgeCriteria
				                              	{
				                              		PostingAgeInDays = selectedPostingAge.AsInt()
				                              	};
			}

			#endregion

			#region Internship Criteria

			var internshipFilter = (FilterTypes)InternshipFilterDropDown.SelectedValueToFlagEnum<FilterTypes>();
			criteria.InternshipCriteria = new InternshipCriteria { Internship = internshipFilter};

			// Internship Categories
			if (internshipFilter != FilterTypes.Exclude && InternshipSkillClusters.SelectedItems.IsNotNull())
			{
				criteria.InternshipCriteria.InternshipCategories = new List<long>();

				foreach (var selectedItem in InternshipSkillClusters.SelectedItems)
				{
					criteria.InternshipCriteria.InternshipCategories.Add(selectedItem);
				}
			}

    	#endregion

      #region Education ProgramOfStudy Criteria

			if (internshipFilter != FilterTypes.ShowOnly && StudyProgramAreaDegreesDropDownList.SelectedIndex > 0)
        criteria.EducationProgramOfStudyCriteria = new EducationProgramOfStudyCriteria
                                                     {
																											 DegreeEducationLevelId = StudyProgramAreaDegreesDropDownList.SelectedValueToLong(),
																											 ProgramAreaId = StudyProgramAreaDropDownList.SelectedValueToLong()
                                                     };

      if (internshipFilter != FilterTypes.ShowOnly && JobSearchDetailedDegreeAjaxDropDownList.SelectedValueToNullableLong().HasValue)
        criteria.EducationProgramOfStudyCriteria = new EducationProgramOfStudyCriteria
        {
          DegreeEducationLevelId = JobSearchDetailedDegreeAjaxDropDownList.SelectedValueToNullableLong()
        };

      #endregion
      
			#region Exclude Past Job Criteria

			if (rbStarMatch.Checked)
			{
				var resumeId = (long?)null;
				if (App.User.IsAuthenticated)
					resumeId = App.UserData.DefaultResumeId;

				criteria.ReferenceDocumentCriteria = new ReferenceDocumentCriteria
				{
					DocumentId = resumeId.ToString(),
					DocumentType = DocumentType.Resume
				};


        var selectedJobs = (from job in cblExcludeJobs.Items.Cast<ListItem>().Where(checkbox => checkbox.Selected)
                            select new Job { JobId = job.Value.AsNullableGuid(), Description = job.Text }).ToList();

        if (selectedJobs.Any())
          criteria.ReferenceDocumentCriteria.ExcludedJobs = selectedJobs;
			}

		  #endregion

      #region ROnet\Occupation and Industry Criteria

      if (internshipFilter != FilterTypes.ShowOnly)
			{
				var careerAreaId = ddlJobFamily.SelectedValueToLong();

		    if (careerAreaId.IsNotNull() && careerAreaId > 0)
		    {
		    	var roNet = ddlOccupation.SelectedValue;

		    	var roNets = new List<string>();

					if (roNet.IsNullOrEmpty())
						roNets = null;
					else if (roNet.Substring(0, 1) == "-")
						roNets = ServiceClientLocator.ExplorerClient(App).GetCareerAreaJobs(careerAreaId).Select(x => x.ROnet).ToList();
					else if (roNet.Substring(0, 1) != "-")
						roNets.Add(roNet);

		      criteria.ROnetCriteria = new ROnetCriteria
		        {
		          CareerAreaId = careerAreaId,
		          ROnets = roNets
		        };
		    }
			}

    	#endregion

      #region Exclude jobs on hold

      if (App.Settings.Theme == FocusThemes.Education)
			{
				

				if (criteria.ExcludePostingCriteria.IsNull())
					criteria.ExcludePostingCriteria = new ExcludePostingCriteria();

				criteria.ExcludePostingCriteria.JobStatuses = new List<string> { "5" };


      }

      #endregion

      #region Work Availability

      criteria.WorkDaysCriteria = SearchCriteriaWorkDay.BuildSearchExpression();

      #endregion

      return criteria;
		}

    /// <summary>
    /// Binds the preferred location.
    /// </summary>
		private void BindPreferredLocation()
		{
			if (App.User.IsAuthenticated)
			{
				try
				{
          var model = DefaultResume;
					if (model.IsNotNull() && model.Special.IsNotNull())
					{
						var preference = model.Special.Preferences;
						if (preference.IsNotNull())
						{
							if (preference.MSAPreference.IsNotNull() && preference.MSAPreference.Any())
							{
								ddlState.SelectValue(preference.MSAPreference[0].StateId.ToString());
								ddlMSA.SelectValue(preference.MSAPreference[0].MSAId.ToString());
								ccdMSA.SelectedValue = preference.MSAPreference[0].MSAId.ToString();
								rbMSAState.Checked = true;
							}

							if (preference.ZipPreference.IsNotNull() && preference.ZipPreference.Any())
							{
								if (preference.ZipPreference[0].RadiusId.IsNotNull())
								{
									ddlRadius.SelectValue(preference.ZipPreference[0].RadiusId.ToString());
									txtZipCode.Text = preference.ZipPreference[0].Zip;
									rbArea.Checked = true;
								}
							}
						}
					}
				}
				catch (ApplicationException ex)
				{
					MasterPage.ShowError(AlertTypes.Error, ex.Message);
				}
			}
		}

    /// <summary>
    /// Set default search settings
    /// </summary>
    private void SetSearchDefaults()
    {
      rbWithoutMatch.Checked = App.Settings.CareerSearchShowAllJobs;
      rbStarMatch.Checked = !rbWithoutMatch.Checked;
      ddlStarMatch.SelectedIndex = App.Settings.CareerSearchMinimumStarMatchScore - 1;

      if (App.Settings.CareerSearchDefaultRadiusId > 0)
      {
        rbArea.Checked = true;
        ddlRadius.SelectedValue = App.Settings.CareerSearchDefaultRadiusId.ToString(CultureInfo.InvariantCulture);
        if (App.UserData.Profile.IsNotNull() && App.UserData.Profile.PostalAddress.IsNotNull() && App.UserData.Profile.PostalAddress.Zip.IsNotNullOrEmpty())
          txtZipCode.Text = App.UserData.Profile.PostalAddress.Zip;
      }
    }

		/// <summary>
		/// Binds the search expression.
		/// </summary>
		private void BindSearchExpression(bool isChangeCriteria)
		{
			var searchCriteria = App.GetSessionValue<SearchCriteria>("Career:SearchCriteria");
		  if (searchCriteria.IsNotNull())
		  {
		    var searchExpression = searchCriteria;
        if (searchExpression.JobLocationCriteria.IsNotNull() && !isChangeCriteria)
		      BindPreferredLocation();

		    #region Job Matching Criteria

		    if (searchExpression.RequiredResultCriteria.IsNotNull())
		    {
		      var minimumscore = searchExpression.RequiredResultCriteria.MinimumStarMatch;
		      if (minimumscore != StarMatching.None && minimumscore != StarMatching.ZeroStar)
		      {
		        rbStarMatch.Checked = true;
		        rbWithoutMatch.Checked = false;

		        ddlStarMatch.SelectValue(minimumscore.ToString());

		        accResumeMatchingTitle.Attributes.Add("class", "collapsableAccordionTitle on");
		        accResumeMatchingContent.Attributes.Add("class", "collapsableAccordionContent open");
		      }
		      else
		      {
		        rbWithoutMatch.Checked = true;
		        rbStarMatch.Checked = false;
		      }
		    }

		    #endregion

		    #region Job Location Criteria

		    if (searchExpression.JobLocationCriteria.IsNotNull())
		    {
		      var locationCriteria = searchExpression.JobLocationCriteria;

		      var radius = locationCriteria.Radius;
		      if (radius.IsNotNull())
		      {
		        rbArea.Checked = true;

		        var lookup =
		          ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.Radiuses)
		                    .FirstOrDefault(x => x.ExternalId == radius.Distance.ToString());

		        if (lookup.IsNotNull())
		          ddlRadius.SelectValue(lookup.Id.ToString());

		        txtZipCode.Text = radius.PostalCode;
		        if (locationCriteria.OnlyShowJobInMyState)
		          rbMSAState.Checked = false;
		      }

		      var area = locationCriteria.Area;
		      if (radius.IsNull() && area.IsNotNull())
		      {
		        rbMSAState.Checked = true;

		        if (area.StateId.HasValue)
		          ddlState.SelectedValue = area.StateId.ToString();

		        if (area.MsaId.HasValue)
		        {
		          ddlMSA.SelectValue(area.MsaId.ToString());
		          ccdMSA.SelectedValue = area.MsaId.ToString();
		        }
		      }
		    }

		    #endregion

		    #region Keyword Criteria

		    if (searchExpression.KeywordCriteria.IsNotNull())
		    {
		      ddlSearchLocation.SelectValue((searchExpression.KeywordCriteria.SearchLocation.IsNotNull())
		                                      ? searchExpression.KeywordCriteria.SearchLocation.ToString()
		                                      : "");
		      SearchTermTextBox.Text = searchExpression.KeywordCriteria.KeywordText;

          if (searchExpression.KeywordCriteria.Operator == LogicalOperators.Or)
            rbSearchAnyWords.Checked = true;
          else
            rbSearchAllWords.Checked = true;
		    }

		    #endregion

		    #region Posting Age Criteria

		    if (searchExpression.PostingAgeCriteria.IsNotNull() &&
		        searchExpression.PostingAgeCriteria.PostingAgeInDays.IsNotNull())
		    {
		      var lookup =
		        ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.PostingAges)
		                  .FirstOrDefault(x => x.ExternalId == searchExpression.PostingAgeCriteria.PostingAgeInDays.ToString(CultureInfo.InvariantCulture));

		      if (lookup.IsNotNull())
            ddlPostingAge.SelectValue(lookup.Id.ToString(CultureInfo.InvariantCulture));

		      if (searchExpression.PostingAgeCriteria.PostingAgeInDays != App.Settings.JobSearchPostingDate)
		      {
		        accPostingTitle.Attributes.Add("class", "collapsableAccordionTitle on");
		        accPostingContent.Attributes.Add("class", "collapsableAccordionContent open");
		      }
		    }

		    #endregion

		    #region Internship Criteria

		    if (searchExpression.InternshipCriteria.IsNotNull())
		    {
		      var internshipFilter = searchExpression.InternshipCriteria.Internship;

		      if (internshipFilter.IsNotNull())
		        InternshipFilterDropDown.SelectValue(internshipFilter.ToString());

					if (searchExpression.InternshipCriteria.InternshipCategories.IsNotNullOrEmpty())
					{
						InternshipSkillClusters.SelectedItems = searchExpression.InternshipCriteria.InternshipCategories;

						accInternshipSearchTitle.Attributes.Add("class", "collapsableAccordionTitle on");
						accInternshipSearchContent.Attributes.Add("class", "collapsableAccordionContent open");
					}
		    }

		    #endregion

		    #region Education ProgramOfStudy Criteria

		    if (searchCriteria.EducationProgramOfStudyCriteria.IsNotNull())
		    {
          if (App.Settings.ShowProgramArea)
          {
            if (searchCriteria.EducationProgramOfStudyCriteria.ProgramAreaId.HasValue)
              StudyProgramAreaDropDownList.SelectValue(
                searchCriteria.EducationProgramOfStudyCriteria.ProgramAreaId.ToString());
            BindStudyProgramAreaDegreesDropDown();
            if (searchCriteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId.HasValue)
              StudyProgramAreaDegreesDropDownList.SelectValue(
                searchCriteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId.ToString());
          }
          else
          {
             if (searchCriteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId.HasValue)
             {
                var degree = ServiceClientLocator.ExplorerClient(App).GetDegreeEducationLevel(
                 searchCriteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId.GetValueOrDefault());
                if (degree.IsNotNull())
                {
                  switch (degree.EducationLevel)
                  {
                    case ExplorerEducationLevels.Certificate:
                    case ExplorerEducationLevels.Associate:
                      DetailedDegreeLevelDropDownList.SelectValue(DetailedDegreeLevels.CertificateOrAssociate.ToString());
                      break;
                    case ExplorerEducationLevels.Bachelor:
                      DetailedDegreeLevelDropDownList.SelectValue(DetailedDegreeLevels.Bachelors.ToString());
                      break;
                    case ExplorerEducationLevels.GraduateProfessional:
                      DetailedDegreeLevelDropDownList.SelectValue(DetailedDegreeLevels.GraduateOrProfessional.ToString());
                      break;
                  }

                  JobSearchDetailedDegreeAjaxDropDownList.SelectValue(degree.Id.ToString());
                  JobSearchDetailedDegreeCascadingDropDown.SelectedValue = degree.Id.ToString();
                }
             }
          }
		    }

		    #endregion

		    #region EducationSchool Criteria

		    if (searchExpression.EducationSchoolCriteria.IsNotNull())
		    {
		      OnlyForSchoolCheckBox.Checked = searchCriteria.EducationSchoolCriteria.OnlyShowExclusivePostings;
		    }

		    #endregion

		    #region Exclude Job Criteria

		    if (searchExpression.ReferenceDocumentCriteria.IsNotNull())
		    {
		      foreach (ListItem checkbox in cblExcludeJobs.Items)
		      {
		        var excludejob = searchExpression.ReferenceDocumentCriteria.ExcludedJobs;

		        if (excludejob.IsNotNull())
		          foreach (var job in excludejob.Where(job => job.JobId == checkbox.Value.AsNullableGuid()))
		          {
		            checkbox.Selected = true;
		          }
		      }
		      accExcludeTitle.Attributes.Add("class", "collapsableAccordionTitle on");
		      accExcludeContent.Attributes.Add("class", "collapsableAccordionContent open");
		    }

		    #endregion

        #region ROnet\Occupation and Industry Criteria

        if (searchExpression.ROnetCriteria.IsNotNull())
        {
          if (searchExpression.ROnetCriteria.CareerAreaId.IsNotNull())
          {
            ddlJobFamily.SelectValue(searchExpression.ROnetCriteria.CareerAreaId.ToString());
          }

          if (searchCriteria.ROnetCriteria.ROnets.IsNotNullOrEmpty() && searchCriteria.ROnetCriteria.ROnets.Count == 1)
          {
            ddlOccupation.SelectValue(searchCriteria.ROnetCriteria.ROnets[0]);
            ccdOccupation.SelectedValue = searchExpression.ROnetCriteria.ROnets[0].ToString(CultureInfo.InvariantCulture);
          }
        }

				#endregion

        #region Work Availability

        SearchCriteriaWorkDay.Bind(searchCriteria.WorkDaysCriteria);

        #endregion
			}
		  else
		  {
        //SetSearchDefaults();
		    BindPreferredLocation();
		  }

			// Disable resume fields in the case of no resume being present
      if (!App.UserData.HasDefaultResume)
      {
        rbWithoutMatch.Checked = true;
        rbStarMatch.Enabled = false;
        ddlStarMatch.Enabled = false;
      }
		}

    /// <summary>
    /// Hides the control.
    /// </summary>
		private void HideControl()
		{
			if (App.User.IsAuthenticated)
			{
				ExcludeJobsTable.Visible = true;
				ddlStarMatch.Enabled = true;

				ExcludeJobsTable.Visible = App.UserData.HasDefaultResume && !App.Settings.CareerHideSearchJobsByResume;
			}
			else
			{
				ExcludeJobsTable.Visible = false;
			}

      if (App.Settings.ShowProgramArea)
        DetailedDegreeLevelPlaceHolder.Visible = DetailedDegreePlaceHolder.Visible = false;
      else
        StudyProgramAreaPlaceHolder.Visible = StudyProgramAreaDegreesPlaceHolder.Visible = false;

			ResumeMatchingTable.Visible = ExcludeJobsTable.Visible = !App.Settings.CareerHideSearchJobsByResume;

      SearchCriteriaWorkDay.Visible = App.Settings.WorkAvailabilitySearch;
		}
	}
}
