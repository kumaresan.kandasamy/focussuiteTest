﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.CareerExplorer.Code;
using Focus.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
    public partial class Info : CareerExplorerPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
					RedirectIfNotAuthenticated = (App.Settings.Theme == FocusThemes.Education);
            base.OnInit(e);
        }
    }
}