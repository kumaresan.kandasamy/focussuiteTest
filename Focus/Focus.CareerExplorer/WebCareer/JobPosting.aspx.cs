﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;

using Focus.CareerExplorer.Code;
using Focus.Common;
using Focus.Common.Extensions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Models.Career;
using Focus.Services.Core.Extensions;

using Framework.Core;

#endregion

namespace Focus.CareerExplorer.WebCareer
{
  public partial class JobPosting : CareerExplorerPageBase
  {

    #region Page Properties

    protected string EmailAddressRequired = "";
    protected string EmailAddressCharLimit = "";
    protected string EmailAddressErrorMessage = "";
    protected string LensPostingId = string.Empty;
    protected string FromUrl = "";
    protected string PageSection = "";
    protected string CustomerJobId = string.Empty;
    protected string BrandName = string.Empty;

    private long? OriginId
    {
      get { return GetViewStateValue<long?>("JobPosting:OriginId"); }
      set { SetViewStateValue("JobPosting:OriginId", value); }
    }

    private long? JobId
    {
      get { return GetViewStateValue<long?>("JobPosting:JobId"); }
      set { SetViewStateValue("JobPosting:JobId", value); }
    }

    private string ReferralApplyMessage
    {
      get { return GetViewStateValue("JobPosting:ReferralApplyMessage", string.Empty); }
      set { SetViewStateValue("JobPosting:ReferralApplyMessage", value); }
    }

    private string ContactName
    {
      get { return GetViewStateValue("JobPosting:ContactName", string.Empty); }
      set { SetViewStateValue("JobPosting:ContactName", value); }
    }

    private string ContactEmailAddress
    {
      get { return GetViewStateValue("JobPosting:ContactEmailAddress", string.Empty); }
      set { SetViewStateValue("JobPosting:ContactEmailAddress", value); }
    }

    private ContactMethods ContactMethod
    {
      get { return GetViewStateValue("JobPosting:ContactMethod", ContactMethods.None); }
      set { SetViewStateValue("JobPosting:ContactMethod", value); }
    }

    private bool ReferralEligible
    {
      get { return GetViewStateValue("JobPosting:ReferralEligible", false); }
      set { SetViewStateValue("JobPosting:ReferralEligible", value); }
    }

    private int ReferralScore
    {
      get { return GetViewStateValue("JobPosting:ReferralScore", 0); }
      set { SetViewStateValue("JobPosting:ReferralScore", value); }
    }

    private string ReferralMessage
    {
      get { return GetViewStateValue("JobPosting:ReferralMessage", ""); }
      set { SetViewStateValue("JobPosting:ReferralMessage", value); }
    }

    private string JobTitle
    {
      get { return GetViewStateValue("JobPosting:JobTitle", ""); }
      set { SetViewStateValue("JobPosting:JobTitle", value); }
    }

    private bool IsAlreadyReferred
    {
      get { return GetViewStateValue("JobPosting:IsAlreadyReferred", false); }
      set { SetViewStateValue("JobPosting:IsAlreadyReferred", value); }
    }

    private ApprovalStatuses ApprovalStatus
    {
      get { return GetViewStateValue("JobPosting:ApprovalStatus", ApprovalStatuses.None); }
      set { SetViewStateValue("JobPosting:ApprovalStatus", value); }
    }

    private string XmlRequirement
    {
      get { return GetViewStateValue("JobPosting:XmlRequirements", ""); }
      set { SetViewStateValue("JobPosting:XmlRequirements", value); }
    }

    private long? PostingId
    {
      get { return GetViewStateValue<long?>("JobPosting:PostingId"); }
      set { SetViewStateValue("JobPosting:PostingId", value); }
    }

    private ReferralModel Model
    {
      get { return GetViewStateValue<ReferralModel>("JobPosting:ReferralModel"); }
      set { SetViewStateValue("JobPosting:ReferralModel", value); }
    }

    private PostingEnvelope JobInfo
    {
      get { return GetViewStateValue<PostingEnvelope>("JobPosting:JobInfo"); }
      set { SetViewStateValue("JobPosting:JobInfo", value); }
    }

    private int? MatchingScore
    {
      get { return GetViewStateValue<int?>("JobPosting:MatchingScore"); }
      set { SetViewStateValue("JobPosting:MatchingScore", value); }
    }

    private bool IsInviteeOrRecommended
    {
      get { return GetViewStateValue<bool>("JobPosting:IsInviteeOrRecommended"); }
      set { SetViewStateValue("JobPosting:IsInviteeOrRecommended", value); }
    }

    private List<string> WaivedRequirements
    {
      get { return GetViewStateValue<List<string>>("JobPosting:WaivedRequirements"); }
      set { SetViewStateValue("JobPosting:WaivedRequirements", value); }
    }

    #endregion

    /// <summary>
    /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event to initialize the page.
    /// </summary>
    /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
    protected override void OnInit(EventArgs e)
    {
      var focusUrlObj = Page.RouteData.Values["fromURL"];
      FromUrl = focusUrlObj.IsNull() ? "" : focusUrlObj.ToString();

      PageSection = Page.RouteData.Values["pageSection"].IsNotNull() ? Page.RouteData.Values["pageSection"].ToString() : string.Empty;

      RedirectIfNotAuthenticated = true;
      ClearExplorerReturnSession = false;

      base.OnInit(e);
      ConfirmationBookMarkModal.CloseCommand += ConfirmationModal_CloseCommand;
    }

    private string CalculateBackUrl()
    {
      if (FromUrl == "JobSearchCriteria")
      {
        return UrlBuilder.JobSearchCriteria();
      }
      return GetRouteUrl(FromUrl, null);
    }

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
      DisplayRegisterSection();

      LensPostingId = Page.RouteData.Values["jobid"].ToString();

      BackHyperLink.NavigateUrl = CalculateBackUrl();

      if (FromUrl == "JobSearchCriteria")
      {
        JobResultsLabel.DefaultText = "&#171; Job search";
      }

      LocaliseUI();
      ApplyBranding();

      if (App.IsAnonymousAccess())
      {
        btnHowToApply.Visible =
          btnSaveBookmark.Visible =
          ibEmailJob.Visible =
          PrintPlaceHolder.Visible = false;
      }

	    var isVeteran = false;
			if (App.User.PersonId.HasValue)
			{
				var person = ServiceClientLocator.CandidateClient(App).GetCandidatePerson(App.User.PersonId.Value);
				if (person != null)
					isVeteran = person.IsVeteran.GetValueOrDefault(false);

				if (App.Settings.UnderAgeJobSeekerRestrictionThreshold > 0)
				{
					if (person == null || !person.Age.HasValue ||
							(person.Age.Value < App.Settings.UnderAgeJobSeekerRestrictionThreshold))
					{
						btnHowToApply.Visible = false;
					}
				}
			}

      if (!IsPostBack)
      {
        // Logs the candidate action
        ServiceClientLocator.CandidateClient(App).ViewJobDetails(App.User.PersonId, LensPostingId);

        if (App.User.IsAuthenticated && !App.User.IsShadowingUser)
          IsInviteeOrRecommended = ServiceClientLocator.JobClient(App).UpdateInviteToApply(LensPostingId, App.User.PersonId.GetValueOrDefault());

        ServiceClientLocator.CandidateClient(App).ResolveCandidateIssues(App.User.PersonId ?? 0, new List<CandidateIssueType>() { CandidateIssueType.NotClickingLeads }, true);

        //ibEmailJob.ImageUrl = "~/Assets/Images/icon_email.png";
				DisplayJobPosting(LensPostingId, isVeteran);

                if (PageSection.IsNotNullOrEmpty())
                    this.Page.Title = PostingId != null ? this.Page.Title + "_" + PageSection + "_" + PostingId : this.Page.Title + "_" + PageSection;
                else
                    this.Page.Title = PostingId != null ? this.Page.Title + "_" + PostingId : this.Page.Title;
        //BindLookup();
      }
      else
          DisplayJob(LensPostingId);
    }

    private void ApplyBranding()
    {
      ibEmailJob.ImageUrl = UrlBuilder.EmailIcon();
    }

    //private void BindLookup()
    //{
    // ddlResumes.BindLookup(ServiceClientLocator.ResumeClient(App).GetResumeNames(), null, CodeLocalise("State.TopDefault", "This Is My Resume Name"));
    //}

    /// <summary>
    /// Localises the UI.
    /// </summary>
    private void LocaliseUI()
    {
      EmailSubjectRequired.ErrorMessage = CodeLocalise("EmailSubject.RequiredErrorMessage", "Subject is required");
      EmailAddressRequired = CodeLocalise("EmailAddress.RequiredErrorMessage", "Email address is required");
      EmailAddressCharLimit = CodeLocalise("EmailAddressCharLimit.ErrorMessage", "Email address must be at least 6 characters");
      EmailAddressErrorMessage = CodeLocalise("EmailAddress.ErrorMessage", "Email address is not correct format");
      btnSendEmailJob.Attributes.Add("tabindex", "0");
      btnHowToApply.Text = Server.HtmlDecode(CodeLocalise("Button.HowToApply", "How to apply for this job &#187;"));
    }

    /// <summary>
    /// Handles the Click event of the AmIGoodMatchHyperLink control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void AmIGoodMatchHyperLink_Click(object sender, EventArgs e)
    {
      if (App.User.IsAuthenticated)
      {
        ServiceClientLocator.CoreClient(App).SaveSelfService(ActionTypes.ViewJobInsights, App.User.UserId);
        Response.RedirectToRoute("JobMatching", new { jobid = LensPostingId, fromURL = "JobSearchResults" });
      }
      else
        ConfirmationModal.Show();
    }

    /// <summary>
    /// Handles the Click event of the FindMoreJobsHyperLink control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void FindMoreJobsHyperLink_Click(object sender, EventArgs e)
    {
      var criteria = App.GetSessionValue<SearchCriteria>(MagicStrings.SessionKeys.CareerSearchCriteria);
      if (criteria.IsNotNull())
      {
        App.SetSessionValue(MagicStrings.SessionKeys.CareerPreviousSearchCriteria, criteria.CloneObject());
      }

      criteria = new SearchCriteria
      {
        JobLocationCriteria = (criteria.IsNotNull()) ? criteria.JobLocationCriteria : null,
        ReferenceDocumentCriteria = new ReferenceDocumentCriteria
        {
          DocumentType = DocumentType.Posting,
          DocumentId = LensPostingId
        },
        SearchType = PostingSearchTypes.PostingsLikeThis
      };
      App.SetSessionValue(MagicStrings.SessionKeys.CareerSearchCriteria, criteria);
      Response.RedirectToRoute("JobSearchResults");
    }

    /// <summary>
    /// Handles the Click event of the DoNotDisplayHyperLink control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void DoNotDisplayHyperLink_Click(object sender, EventArgs e)
    {
      if (App.User.IsAuthenticated)
      {
        try
        {
          if (LensPostingId.IsNotNullOrEmpty())
            ServiceClientLocator.OrganizationClient(App).SetDoNotDisplay(LensPostingId);
          Response.Redirect(CalculateBackUrl());
        }
        catch (ApplicationException ex)
        {
          MasterPage.ShowError(AlertTypes.Error, ex.Message);
        }
      }
      else
        ConfirmationModal.Show();
    }

    /// <summary>
    /// Displays the job posting.
    /// </summary>
    /// <param name="lensPostingId">The lens posting id.</param>
    private void DisplayJobPosting(string lensPostingId, bool isVeteran)
    {
            App.RemoveSessionValue("Career:XMLPosting:" + LensPostingId);
            App.RemoveSessionValue("Career:XMLRequirement:" + LensPostingId);
            App.RemoveSessionValue("Career:XMLEosPosting:" + LensPostingId);
            App.RemoveSessionValue("Career:DisplayPosting:" + PostingId);

      try
      {
        bool logViewAction = FromUrl.Equals("JobSearchResults", StringComparison.OrdinalIgnoreCase) || (FromUrl.Equals("Home", StringComparison.OrdinalIgnoreCase) && PageSection.Equals("SavedSearches", StringComparison.OrdinalIgnoreCase));

        var post = ServiceClientLocator.PostingClient(App).GetJobPosting(lensPostingId, logViewAction);
        ViewCountLabel.Text = HtmlLocalise("ViewedJob.Label", "This job has been viewed " + post.PostingInfo.ViewedCount + " times");

        if (post.PostingInfo.IsNotNull())
        {
          OriginId = post.PostingInfo.PostingOriginId;

          JobId = post.PostingInfo.FocusJobId;
          PostingId = post.PostingInfo.FocusPostingId; // ServiceClientLocator.PostingClient(App).GetPostingId(lensPostingId);
          CustomerJobId = CustomerJobIDDataLabel.Text = post.PostingInfo.CustomerJobId;
          ApplyCustomerJobIdDiv.Visible = CustomerJobId.IsNotNullOrEmpty() && (CustomerJobId != "-999");
          var focusJobId = post.PostingInfo.FocusJobId;
          var focusPostingId = post.PostingInfo.FocusPostingId;

          BrandName = App.Settings.BrandName;

          lblCustomerJobID.Text = (CustomerJobId.IsNotNullOrEmpty() && (CustomerJobId != "-999"))
            ? string.Format(CodeLocalise("CustomerJobId.Label", "Customer Job ID {0}"), CustomerJobId)
            : String.Empty;
          lblJobID.Text = string.Format(CodeLocalise("CustomerPostingId.Label", "{0} Posting ID {1}"),
            App.Settings.BrandName, focusPostingId);
          if (focusJobId != null)
            lblJobID.Text += string.Format(CodeLocalise("CustomerJobId.Label", " (Job ID {0})"), focusJobId);

          if (post.PostingInfo.Url.IsNotNullOrEmpty())
          {
            var uriObj = new Uri(post.PostingInfo.Url);
            lnkJobURL.NavigateUrl = uriObj.OriginalString;
            lnkJobURL.Text = string.Concat(uriObj.Scheme, "://", uriObj.Host, "/...");
          }

          lblPostingTitle.Text = JobTitleDataLabel.Text = post.PostingInfo.Title;
          JobTitle = post.PostingInfo.Title;
          AmIAGoodMatchPlaceholder.Visible = !post.PostingInfo.Internship;

          var jobId = JobId.GetValueOrDefault();
          var job = ServiceClientLocator.JobClient(App).GetJob(jobId);

          if (job != null)
          {
						if (job.VeteranPriorityEndDate.IsNotNull() && job.VeteranPriorityEndDate >= DateTime.Now && !isVeteran)
						{
							Response.Redirect(UrlBuilder.Home(), true);
						}

	          if (job.JobStatus != JobStatuses.Active)
	          {
		          btnHowToApply.Visible = false;
		          NoApplyHolder.Visible = true;

		          if (job.JobStatus == JobStatuses.Closed)
		          {
								NoApplyLabel.Text = (job.ClosingOn.HasValue && (DateTime.Now - (DateTime)job.ClosingOn).TotalDays <= 30)
																			? HtmlLocalise("NoApply.ClosedRecent.Label", "This posting has expired and not currently available to apply to")
																			: HtmlLocalise("NoApply.Closed.Label", "This posting has closed and is no longer available to apply to");
		          }
		          else
		          {
			          NoApplyLabel.Text = HtmlLocalise("NoApply.Hold.Label", "This posting is currently under review and not currently available to apply to");
		          }
	          }
          }
          else
          {
						if (DateTime.Now.Date.Subtract(post.PostingInfo.CreatedOn.Date).TotalDays >= App.Settings.JobDevelopmentSpideredJobListPostingAge)
						{
							btnHowToApply.Visible = false;
							NoApplyHolder.Visible = true;

							NoApplyLabel.Text = HtmlLocalise("NoApply.Spidered.Label", "This job has now closed and is no longer available to apply to");
						}
          }
          lblJobInfoJobID.Text = string.Format(CodeLocalise("CustomerPostingId.Label", "{0} Posting ID: "), App.Settings.BrandName);
          JobIDLabel.Text = focusPostingId.ToString();
          if (focusJobId != null) JobIDLabel.Text += string.Format(CodeLocalise("CustomerJobId.Label", " (Job ID: {0})"), focusJobId);
        }
        else
        {
          lblJobInfoJobID.Text = string.Format(CodeLocalise("CustomerJobId.Label", "{0} Job ID: "), App.Settings.BrandName);
          JobIDLabel.Text = lensPostingId;
        }

        if (post.PostingXml.IsNotNullOrEmpty())
            App.SetSessionValue("Career:XMLPosting:" + LensPostingId, post.PostingXml);

        if (App.Settings.JobRequirements)
        {
            if (post.RequirementsXml.IsNotNullOrEmpty())
                App.SetSessionValue("Career:XMLRequirement:" + LensPostingId, post.RequirementsXml);

            if (post.EosPostingXml.IsNotNullOrEmpty())
                App.SetSessionValue("Career:XMLEosPosting:" + LensPostingId, post.EosPostingXml);
        }

        // No need to add the footer if it's a spidered job
        if (post.PostingInfo.PostingOriginId.IsNotNull() && post.PostingInfo.PostingOriginId != 999)
            post.PostingHtml = InsertPostingFooter(post.PostingHtml, Convert.ToInt64(post.PostingInfo.FocusJobId));

        App.SetSessionValue("Career:DisplayPosting:" + PostingId, post.PostingHtml);

        if (App.User.IsAuthenticated)
        {
          ServiceClientLocator.PostingClient(App).AddViewedPosting(new ViewedPostingDto { LensPostingId = lensPostingId });
        }

        PostingDetail.Attributes["src"] = GetRouteUrl("JobDisplay", new { postingid = PostingId });

        if (OriginId.HasValue && OriginId.IsTalentOrigin(App.Settings))
        {
          PostingDetail.Attributes["onload"] = string.Format("JobPosting_DisableJobSeekerLink('{0}', '{1}')",
            App.Settings.CareerApplicationPath,
            CodeLocalise("Posting.ApplyThroughFocusCareer.Label", "Log in to #FOCUSCAREER# and submit your resume"));
        }
        else
        {
          PostingDetail.Attributes["onload"] = "JobPosting_DisableAllLinks()";
        }

        JobInfo = !App.User.IsAnonymous && LensPostingId.IsNotNullOrEmpty()
          ? ServiceClientLocator.OrganizationClient(App).GetJobInformation(LensPostingId)
          : null;

        var referrals = JobInfo.IsNotNull()
                          ? ServiceClientLocator.CandidateClient(App).GetReferralViewList(Convert.ToInt64(App.User.PersonId), null, JobInfo.PostingInfo.FocusPostingId)
                          : null;

        var referralCount = referrals.IsNotNull() ? referrals.Count : 0;

        if (referrals.IsNotNull() && (App.Settings.ConfidentialEmployersDefault || App.Settings.ConfidentialEmployersEnabled))
        {
          var existingReferral = referrals.FirstOrDefault(referral => referral.LensPostingId == LensPostingId);
          if (existingReferral.IsNotNull() && existingReferral.ApplicationApprovalStatus.IsIn(ApprovalStatuses.None, ApprovalStatuses.Approved))
          {
            EmployerNameLabel.Text = existingReferral.EmployerName ?? existingReferral.PostingEmployerName;
            EmployerNamePlaceHolder.Visible = true;
          }
        }

        if (App.User.IsShadowingUser)
        {
          EmployerNameLabel.Text = JobInfo.PostingInfo.Employer;
          EmployerNamePlaceHolder.Visible = true;
        }

        int score;
        string referralMessage;
        bool allCandidatesScreened;

        var referralEligible = IsReferralEligible(out score, out referralMessage, out allCandidatesScreened);

        ReferralScore = score;

        Model = new ReferralModel
        {
          AllCandidatesScreened = allCandidatesScreened,
          ReferralCount = referralCount,
          ReferralEligible = referralEligible,
          ReferralMessage = referralMessage,
          Score = score
        };
      }
      catch (ApplicationException ex)
      {
        ConfirmationBookMarkModal.Show("", ex.Message + "  We are taking you to previous page.", "Ok", "REDIRECT_PREVIOUS", height: 50);
      }
    }

    private void DisplayJob(string lensPostingId)
    {
        App.RemoveSessionValue("Career:XMLPosting:" + LensPostingId);
        App.RemoveSessionValue("Career:XMLRequirement:" + LensPostingId);
        App.RemoveSessionValue("Career:XMLEosPosting:" + LensPostingId);
        App.RemoveSessionValue("Career:DisplayPosting:" + PostingId);
        try
        {
            var post = ServiceClientLocator.PostingClient(App).GetJobPosting(lensPostingId);
            if (post.PostingXml.IsNotNullOrEmpty())
                App.SetSessionValue("Career:XMLPosting:" + LensPostingId, post.PostingXml);

            if (App.Settings.JobRequirements)
            {
                if (post.RequirementsXml.IsNotNullOrEmpty())
                    App.SetSessionValue("Career:XMLRequirement:" + LensPostingId, post.RequirementsXml);

                if (post.EosPostingXml.IsNotNullOrEmpty())
                    App.SetSessionValue("Career:XMLEosPosting:" + LensPostingId, post.EosPostingXml);
            }

            // No need to add the footer if it's a spidered job
            if (post.PostingInfo.PostingOriginId.IsNotNull() && post.PostingInfo.PostingOriginId != 999)
                post.PostingHtml = InsertPostingFooter(post.PostingHtml, Convert.ToInt64(post.PostingInfo.FocusJobId));

            App.SetSessionValue("Career:DisplayPosting:" + PostingId, post.PostingHtml.AddAltTagWithValueForImg("Display posting image"));
        }

        catch (ApplicationException ex)
        {
            ConfirmationBookMarkModal.Show("", ex.Message + "  We are taking you to previous page.", "Ok", "REDIRECT_PREVIOUS", height: 50);
        }

    }
    /// <summary>
    /// Handles the CloseCommand event of the ConfirmationModal control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Web.UI.WebControls.CommandEventArgs"/> instance containing the event data.</param>
    public void ConfirmationModal_CloseCommand(object sender, CommandEventArgs e)
    {
      switch (e.CommandName)
      {
        case "REDIRECT_PREVIOUS":
          try
          {
            Response.Redirect(CalculateBackUrl());
          }
          catch (Exception)
          {
            // ignored
          }

          break;
      }
    }

    /// <summary>
    /// Handles the Click event of the btnReportSpamOrClosed control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnReportSpamOrClosed_Click(object sender, EventArgs e)
    {
      if (App.User.IsAuthenticated)
      {
        var xmlposting = App.GetSessionValue<string>("Career:XMLPosting:" + LensPostingId);
        var displayposting = App.GetSessionValue<string>("Career:DisplayPosting:" + PostingId);

        if (xmlposting.IsNotNull())
        {
          var jobOriginId = "";
          var jobId = Page.RouteData.Values["jobid"] as string;
          var applicationUrl = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port;
          var navigateUrl = "";

          var xDoc = new XmlDocument { PreserveWhitespace = true };
          xDoc.LoadXml(xmlposting);

          var jobOriginIdNode = xDoc.SelectSingleNode("//cf012");
          if (jobOriginIdNode != null && (!String.IsNullOrEmpty(jobOriginIdNode.InnerText)))
          {
            var singleNode = xDoc.SelectSingleNode("//cf012");
            if (singleNode != null) jobOriginId = singleNode.InnerText;
          }

          var joburlNode = xDoc.SelectSingleNode("//joburl");
          if (joburlNode != null && (!String.IsNullOrEmpty(joburlNode.InnerText)))
          {
            var node = xDoc.SelectSingleNode("//joburl");
            if (node != null) navigateUrl = node.InnerText;
          }

          if (ddlReportSpamOrClosed.SelectedValue == "SPAM")
          {
            if (App.Settings.SpamJobOriginId.IsNotNullOrEmpty() && App.Settings.SpamJobOriginId.Split(',').Contains(jobOriginId))
            {
              var mailBody = "<font face='arial'>This job has been reported as spam by the jobseeker.<br/><br/>";
              mailBody += "<br/><b>Application URL:</b> " + applicationUrl;
              if (App.User.IsAuthenticated)
                mailBody += "<br/><b>Jobseeker Username:</b> " + App.User.EmailAddress;
              if (!String.IsNullOrEmpty(navigateUrl))
                mailBody += "<br/><b>Job URL:</b> " + navigateUrl;
              mailBody += "<br/><b>Job ID:</b> " + jobId;
              mailBody += "<br/><b>Reason:</b> Reported as spam.";
              mailBody += "</font><br/><br/>";
              mailBody += "<b>JOB DETAILS</b><hr/><br/>" + displayposting;
              try
              {
                ServiceClientLocator.CoreClient(App).SendEmail(App.Settings.SpamMailAddress, CodeLocalise("ReportAsJobAsSpam.EmailSubject", "Reported the Job as Spam – ID {0}", jobId), mailBody, true);

                MasterPage.ShowModalAlert(AlertTypes.Info, "Thank you for reporting this job as spam. Your report has been referred to site administrators for further review.", "Thank you");

              }
              catch (Exception ex)
              {
                MasterPage.ShowError(AlertTypes.Error, ex.Message);
              }
            }
          }
          else if (ddlReportSpamOrClosed.SelectedValue == "CLOSED")
          {
            if (App.Settings.ClosedJobOriginId.IsNotNullOrEmpty() && App.Settings.ClosedJobOriginId.Split(',').Contains(jobOriginId))
            {
              var phrase = "";
              var jobClosedAtSource = true;

              if (navigateUrl.IsNotNullOrEmpty())
              {
                try
                {
                  var urlCheck = new Uri(navigateUrl);
                  var request = (HttpWebRequest)WebRequest.Create(urlCheck);
                  //Timeout is set to '30000' to get the response from Https links also.
                  request.Timeout = 30000;
                  var response = (HttpWebResponse)request.GetResponse();

                  if (response.StatusCode == HttpStatusCode.OK)
                  {
                    var receiveStream = response.GetResponseStream();
                    var encode = System.Text.Encoding.GetEncoding("utf-8");
                    var sReader = new StreamReader(receiveStream, encode);
                    var siteData = sReader.ReadToEnd().ToLower();
                    sReader.Close();

                    string[] jobClosePhrases = { "no longer available", "no longer exists", "no longer running", "job is closed", "job closed", "posting closed", "posting is closed", "job has expired", "position has been filled", "no longer active", "job posting you are looking for has expired", "position has already been filled" };
                    foreach (var closePhrase in jobClosePhrases)
                    {
                      if (siteData.Contains(closePhrase) && (siteData.Contains("job") || siteData.Contains("posting")))
                      {
                        phrase = closePhrase;
                        break;
                      }
                    }
                  }
                  else
                  {
                    phrase = "NO_URL";
                  }
                }
                catch (Exception ex)
                {
                  //Error: '410' means that the URL is dead or not usable anymore.
                  //Error: '404' means that the site can't find the page we are looking for.
                  if (ex.Message == "Unable to cast object of type 'System.Net.FileWebRequest' to type 'System.Net.HttpWebRequest'." ||
                                                                                  ex.Message == "The remote server returned an error: (410) Gone." ||
                                                                                  ex.Message == "The remote server returned an error: (404) Not Found.")
                    phrase = "NO_URL";
                }

                if (String.IsNullOrEmpty(phrase))
                  jobClosedAtSource = false;
              }

              var jobClosed = false;
              if (jobClosedAtSource)
                jobClosed = ServiceClientLocator.PostingClient(App).CloseJobPosting(jobId);

              string mailBody;

              if (jobClosed)
              {
                mailBody = "<font face='arial'>Based on the request from the jobseeker we validated the job posting URL and found to be closed or no longer available.<br/><br/>";
                mailBody += "<b>More information:</b> " + ((phrase == "NO_URL") ? "URL broken" : ("Found keywords '" + phrase + "'"));
                mailBody += "<br/><b>Application URL:</b> " + applicationUrl;
                mailBody += "<br/><b>Jobseeker Username:</b> " + App.User.EmailAddress;
                if (navigateUrl.IsNotNullOrEmpty())
                  mailBody += "<br/><b>Job URL:</b> " + navigateUrl;
                mailBody += "<br/><b>Job ID:</b>" + jobId;
                mailBody += "</font><br><br>";
              }
              else
              {
                mailBody = "<font face='arial'>The following job is reported as closed.<br/><br/>";
                mailBody += "<br/><b>Application URL:</b> " + applicationUrl;
                mailBody += "<br/><b>Jobseeker Username:</b> " + App.User.EmailAddress;
                if (navigateUrl.IsNotNullOrEmpty())
                  mailBody += "<br/><b>Job URL:</b> " + navigateUrl;
                mailBody += "<br/><b>Job ID:</b>" + jobId;
                mailBody += "</font><br><br>";
              }


              try
              {
                ServiceClientLocator.CoreClient(App).SendEmail(App.Settings.SpamMailAddress,
                                              CodeLocalise("ReportAsJobAsClosed.EmailSubject",
                                                           "Reported the Job as Closed – ID {0}", jobId), mailBody, true);
                MasterPage.ShowModalAlert(AlertTypes.Info,
                                          "Thank you for reporting this job as closed. Your report has been referred to site administrators for further review.",
                                          "Thank you");

              }
              catch (Exception ex)
              {
                MasterPage.ShowError(AlertTypes.Error, ex.Message);
              }
            }
          }
        }
      }
      else
        ConfirmationModal.Show();
    }

    /// <summary>
    /// Displays the register section.
    /// </summary>
    private void DisplayRegisterSection()
    {
      if (App.User.IsAuthenticated || App.User.IsAnonymous)
        panRegisterSection.Visible = false;
    }

    /// <summary>
    /// Handles the Click event of the EmailJob control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void EmailJob_Click(object sender, EventArgs e)
    {
      if (App.User.IsAuthenticated)
      {
          this.Page.Title = this.Page.Title + " - Email";
        EmailJobModal.Show();
      }
      else
        ConfirmationModal.Show();
    }

    /// <summary>
    /// Handles the Click event of the btnSaveBookmark control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnSaveBookmark_Click(object sender, EventArgs e)
    {
      if (App.User.IsAuthenticated)
      {
        try
        {
          var status = Utilities.CreateCareerBookmark(JobTitle, BookmarkTypes.Posting, Page.RouteData.Values["jobid"].ToString());

          if (status)
            ConfirmationBookMarkModal.Show(CodeLocalise("BookmarkConfirmation.Title", "Bookmark Confirmation"),
                                           CodeLocalise("BookmarkAdded.Label", "Your bookmark has been added"),
                                           CodeLocalise("Global.Close.Text", "Close"), "BookmarkConfirmationClose", height: 50, width: 250);
          else
            ConfirmationBookMarkModal.Show(CodeLocalise("BookmarkDuplication.Title", "Bookmark Duplication"),
                                           CodeLocalise("BookmarkDuplication.Body", "A bookmark has been already been added for this job"),
                                           CodeLocalise("Global.Close.Text", "Close"), "BookmarkConfirmationClose", height: 50, width: 250);
        }
        catch (ApplicationException ex)
        {
          MasterPage.ShowError(AlertTypes.Error, ex.Message);
        }
      }
      else
        ConfirmationModal.Show();
    }

    /// <summary>
    /// Handles the Click event of the btnSendEmailJob control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnSendEmailJob_Click(object sender, EventArgs e)
    {
      var xmlposting = App.GetSessionValue<string>("Career:XMLPosting:" + LensPostingId);
      var displayposting = App.GetSessionValue<string>("Career:DisplayPosting:" + PostingId);

      if (xmlposting.IsNotNull())
      {
        var navigateUrl = "";
        var navText = "For information about how to apply, click job link:<br/>";
        var xDoc = new XmlDocument { PreserveWhitespace = true };
        xDoc.LoadXml(xmlposting);

        var selectSingleNode = xDoc.SelectSingleNode("//url");
        if (selectSingleNode != null && (!String.IsNullOrEmpty(selectSingleNode.InnerText)))
        {
          var singleNode = xDoc.SelectSingleNode("//url");
          if (singleNode != null) navigateUrl = singleNode.InnerText;
        }
        else if (!String.IsNullOrEmpty(LensPostingId))
        {
          navigateUrl = HttpContext.Current.Request.Url.AbsoluteUri;
        }
        else
        {
          navText = String.Empty;
        }

        var joblink = "<a href='" + navigateUrl + "'>" + navigateUrl + "</a>";

        var mailBody = "<font face='arial'>This message was sent to you by : " + (App.User.IsAuthenticated ? App.User.EmailAddress : "") + "</br></font>" + "<table><tr><td width='100%' style='background-color:white;color:black;'>" + navText + joblink + "</td></tr></table><br/><br/>" + (displayposting.IsNotNull() ? displayposting : "");

        try
        {
          ServiceClientLocator.CoreClient(App).SendEmail(txtToEmailAddress.Text.Trim(), txtEmailSubject.Text.Trim(), mailBody, isHtml: true);
          MasterPage.ShowModalAlert(AlertTypes.Info, "Your email has been successfully sent.", "Thank you");
        }
        catch (Exception ex)
        {
          MasterPage.ShowError(AlertTypes.Error, ex.Message);
        }
      }
      EmailJobModal.Hide();
    }

    /// <summary>
    /// Handles the Click event of the BtnHowToApply control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void BtnHowToApply_Click(object sender, EventArgs e)
    {
      bool alienRegistrationExpire = false;

      if (App.User.IsAuthenticated)
      {

        var defaultResume = ServiceClientLocator.ResumeClient(App).GetDefaultResume();

        if (defaultResume.IsNotNull() && defaultResume.ResumeContent.Profile.IsNotNull())
        {
          var userProfile = defaultResume.ResumeContent.Profile;

          if (!(userProfile.IsUSCitizen ?? true))
          {
            if (userProfile.AlienExpires.HasValue)
            {
              if (userProfile.AlienExpires < DateTime.Now)
              {
                alienRegistrationExpire = true;
              }
            }
          }
        }

        // if non US and registraton expired then do not allow posting
        if (alienRegistrationExpire)
        {
          MasterPage.ShowModalAlert(AlertTypes.Info, "Your alien registration expiry date has lapsed and therefore you cannot be considered for this posting. Please contact your Career Center for further assistance.", "Referral denied");
        }
        else
        {

          lblReferralStatus.Text = "";
          lblAlreadyReferred.Text = "";
          jobUrl.Visible = false;

          // Log 'click how to apply' action
          ServiceClientLocator.CandidateClient(App).RegisterHowToApply(LensPostingId, App.User.PersonId.GetValueOrDefault());

          try
          {
            if (App.UserData.DefaultResumeCompletionStatus != ResumeCompletionStatuses.Completed && (OriginId.IsTalentOrigin(App.Settings) || !App.Settings.AllowSelfReferralOnSpideredPostingsWithNoResume))
            {
              panJobDetails.Visible = panJobApplicationInfo.Visible = panReferralStatus.Visible = panScreeningPreferences.Visible = false;
              panResumeRequirement.Visible = true;
              HowToApplyModal.Show();
            }
            else
            {
              XmlRequirement = App.GetSessionValue<string>("Career:XMLRequirement:" + LensPostingId);

              var xRequirementDoc = new XmlDocument();

              if (XmlRequirement.IsNotNullOrEmpty())
              {
                xRequirementDoc.LoadXml(XmlRequirement);
                var screeningPreferencePassed = IsInviteeOrRecommended || IsScreeningPreferenceValid(xRequirementDoc);

                if (!screeningPreferencePassed)
                {
                  panJobDetails.Visible = panJobApplicationInfo.Visible = panReferralStatus.Visible = panResumeRequirement.Visible = false;
                  panScreeningPreferences.Visible = true;
                  HowToApplyModal.Show();

                  return;
                }
              }

              if (OriginId.HasValue && (OriginId.IsTalentOrigin(App.Settings)))
              {
                var referralPostings = ServiceClientLocator.OrganizationClient(App).GetApplicationList(-1, DateTime.Today.AddYears(-5), DateTime.Now);

                foreach (var referralPosting in referralPostings.Where(referralPosting => referralPosting.LensPostingId == LensPostingId))
                {
                  if (referralPosting.PostingStatus.IsNotNull())
                    ApprovalStatus = referralPosting.ApprovalStatus.IsNotNull()
                      ? referralPosting.ApprovalStatus
                      : ApprovalStatuses.None;

                  break;
                }
              }

              ContactMethod = GetContactMethod();
              IsAlreadyReferred = ServiceClientLocator.CandidateClient(App).GetReferralViewList(Convert.ToInt64(App.User.PersonId), 1, PostingId, ignoreVeteranPriorityService: true).Any();

              // If denied by staff, we are allowing to refer it again. If reviewStatus = 0, then it is denied by staff
              if (IsAlreadyReferred)
              {
                if (OriginId.IsTalentOrigin(App.Settings))
                {
                  switch (ApprovalStatus)
                  {
                    case ApprovalStatuses.Rejected:
                      if (XmlRequirement.IsNotNullOrEmpty())
                        DisplayRequirements(xRequirementDoc);
                      if (!Model.HasRequirements)
                        GetJobInfo();
                      break;
                    case ApprovalStatuses.Approved:
                      GetJobInfo();
                      break;
                    case ApprovalStatuses.WaitingApproval:
                      MasterPage.ShowModalAlert(AlertTypes.Error,
                        "Thank you for showing interest in this job. We've received your resume already and it is being reviewed by our staff. We shall get back to you shortly.",
                        "Resume already received");
                      break;
                  }
                }
                else
                {
                  GetJobInfo();
                }
              }
              else
              {
                if (XmlRequirement.IsNotNullOrEmpty())
                {
                  DisplayRequirements(xRequirementDoc);
                }
                else
                {
                  //if (App.UserData.HasDefaultResume && App.UserData.DefaultResume.ResumeId.HasValue && LensPostingId.IsNotNullOrEmpty())
                  //  ServiceClientLocator.OrganizationClient(App).ApplyForJob(LensPostingId, App.UserData.DefaultResume.ResumeId.Value, true, reviewApplication: true);

                  //TODO: Martha (new functionality) -  Staff details are added during referral job. 
                  //bool status = AccountOrganizerClient.Instance(App).ApplyForJob(
                  //                     PostingPageUserContext.UserId,
                  //                     jobId,
                  //                     PostingPageUserContext.DefaultResumeInfo.ResumeId,
                  //                     OriginId,
                  //                     staffDetail:
                  //                     staffcontext.IsNotNull() && staffcontext.IsAuthenticated ? new StaffContext
                  //                     {
                  //                       Username = staffcontext.Username,
                  //                       StaffInfo = staffcontext.StaffInfo,
                  //                       Password = staffcontext.Password
                  //                     } : null);

                  #region Save Activity

                  //TODO: Martha (new activity functionality)
                  //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
                  //  ServiceClientLocator.AnnotationClient(App).SaveActivity(PostingPageUserContext.UserId, PostingPageUserContext.Username, ActivityOwner.Staff, ActivityType.Manual, "REF01", customMessage: string.Format("Referral for the job (Customer Job ID: {0}; {2} Job ID: {1}) has been added", customerJobId, jobId, BrandName), staffProfile: staffcontext.StaffInfo);
                  //else
                  //  ServiceClientLocator.AnnotationClient(App).SaveActivity(PostingPageUserContext.UserId, PostingPageUserContext.Username, ActivityOwner.JobSeeker, ActivityType.Manual, "REF01", customMessage: string.Format("Referral for the job (Customer Job ID: {0}; {2} Job ID: {1}) has been added", customerJobId, jobId, BrandName));

                  #endregion

                  panJobDetails.Visible = true;

                  if (OriginId.HasValue && (!OriginId.IsTalentOrigin(App.Settings)))
                  {
                    InfoLabel.Visible = panReferralStatus.Visible = SubmitButtons.Visible = true;
                    JobPostingReferralDiv.Visible = SendButtons.Visible = OKButtons.Visible = ApplyOnlineDiv.Visible = false;
                    ReferralApplyMessage = CodeLocalise("ReferralEligibleOnlineMessage", "Please follow the link below to apply to this posting ");
                    jobUrl.NavigateUrl = lnkJobURL.NavigateUrl;
                    jobUrl.Text = lnkJobURL.Text;

                    InfoLabel.DefaultText = CodeLocalise("","By clicking on Submit, your application will be recorded on your dashboard for tracking purposes.");
                  }
                  else
                  {
                    ApplyOnlineDiv.Visible = false;
                    panReferralStatus.Visible = SendButtons.Visible = true;
                    SubmitButtons.Visible = JobPostingReferralDiv.Visible = lblReferralStatus.Visible = jobUrl.Visible = false;
                  }

                  panJobRequirements.Visible = panJobApplicationInfo.Visible = panScreeningPreferences.Visible = false;
                  HowToApplyModal.Show();
                }
              }
            }

          }
          catch (ApplicationException ex)
          {
            MasterPage.ShowError(AlertTypes.Error, ex.Message);
          }
        }
      }
      else
        ConfirmationModal.Show();
    }

    private void DisplayRequirements(XmlDocument xRequirementDoc)
    {
      //Display Requirement popup
      panJobDetails.Visible =
        panJobApplicationInfo.Visible = panReferralStatus.Visible = panScreeningPreferences.Visible = false;

      var backgroundXML = App.GetSessionValue<string>("Career:XMLPosting:"+LensPostingId);
      var backgroundXMLDoc = new XmlDocument();

      if (backgroundXML.IsNotNullOrEmpty())
        backgroundXMLDoc.LoadXml(backgroundXML);

      LoadRequirements(xRequirementDoc, backgroundXMLDoc);
      panJobRequirements.Visible = true;

			LoadRequirements(xRequirementDoc, backgroundXMLDoc);

			hdnActions.Value = hdnActionsWaive.Value = hdnActionsYes.Value = "";

      HowToApplyModal.Show();
    }


    /// <summary>
    /// Handles the Click event of the btnIMeetReq control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnIMeetReq_Click(object sender, EventArgs e)
    {
      try
      {
        var categories = new List<string>();
        var unQualifiedItems = new List<string>();

        if (ViewState["CurrentTableRequirements"] != null)
        {
          var dt = (DataTable)ViewState["CurrentTableRequirements"];
          if (dt.Rows.Count > 0)
          {
            for (var i = 0; i < dt.Rows.Count; i++)
              categories.Add(((Label)grdJobSkills.Rows[i].Cells[0].FindControl("lblCategory")).Text);
          }
        }

        var denyMessage = "You have indicated that you cannot meet the ";
        foreach (var id in hdnActions.Value.Replace("'", "").Split('#'))
        {
          if (!String.IsNullOrEmpty(id))
          {
            unQualifiedItems.Add(categories[Convert.ToInt32(id)]);
            var category = categories[Convert.ToInt32(id)];
            if (category != "NCRC")
              category = category.ToLower();

            denyMessage += category + ", ";
          }
        }

        // get the qualified items
        var qualifiedItems = (from id in hdnActionsYes.Value.Replace("'", "").Split('#')
                              where !String.IsNullOrEmpty(id)
                              select categories[Convert.ToInt32(id)]).ToList();

        // get the waived items
        var waivedItems = (from id in hdnActionsWaive.Value.Replace("'", "").Split('#')
                           where !String.IsNullOrEmpty(id)
                           select categories[Convert.ToInt32(id)]).ToList();

        var answeredAll = (qualifiedItems.Count + unQualifiedItems.Count + waivedItems.Count) == categories.Count;

        if (answeredAll)
        {
          if (unQualifiedItems.Count > 0 && denyMessage.EndsWith(", "))
          {
            denyMessage = denyMessage.Substring(0, denyMessage.LastIndexOf(", ", StringComparison.Ordinal));
            denyMessage +=
              " requirements and as a result, we regret that we cannot refer you to this position. If you have answered these questions in error please go back and amend your response. " +
              CodeLocalise("DenyMessageSuffix", "For additional assistance, please contact your nearest Career Center.");
          }
          else
          {
            denyMessage = "";
          }

        }
        else
          denyMessage = "You have not answered all the requirements and as a result, we regret that we cannot refer you to this position. If you have answered these questions in error please go back and amend your response. " + CodeLocalise("DenyMessageSuffix", "For additional assistance, please contact your nearest Career Center.");

        if (String.IsNullOrEmpty(denyMessage))
          VerifyReferral(waivedItems);
        else
          MasterPage.ShowModalAlert(AlertTypes.Error, denyMessage, "Application denied");
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnSendJobInfo control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnSendJobInfo_Click(object sender, EventArgs e)
    {
      try
      {
        ServiceClientLocator.CoreClient(App).SendEmail(App.User.EmailAddress, CodeLocalise("SendJobInfo.EMailSubject", "Job Application Information"), lblReferralDetails.Text, true);
        MasterPage.ShowModalAlert(AlertTypes.Info, "Job application information successfully sent as an email", "Thank you");
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the click event of the btnReferralStatusSend_Click control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnReferralStatusSend_Click(object sender, EventArgs e)
    {
      if (ContactMethod.HasFlag(ContactMethods.Email) && ReferralEligible)
      {
        // Send Email with the resume as an attachment
        if (SendEmailWithAttachment())
        {
          // Notify the user that the email was sent
          if (ReferralEligible)
          {
            MasterPage.ShowModalAlert(AlertTypes.Info,
              CodeLocalise("Interest.NoApproval", "Thank you for showing interest in this posting."),
              CodeLocalise("Global.ThankYou", "Thank you"));
          }
          else
          {
            MasterPage.ShowModalAlert(AlertTypes.Info,
              "Thank you for showing interest in this posting. We shall review your resume and get back to you within the next 2 working days",
              "Thank you");
          }
        }
      }
    }

    /// <summary>
    /// Handles the OK event of the btnReferralStatusOK control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnReferralStatusOK_Click(object sender, EventArgs e)
    {
      try
      {
        if (OriginId.IsTalentOrigin(App.Settings))
        {
          if (ReferralEligible)
          {
            MasterPage.ShowModalAlert(AlertTypes.Info,
              CodeLocalise("Interest.NoApproval", "Thank you for showing interest in this posting."),
              CodeLocalise("Global.ThankYou", "Thank you"));
          }
          else
          {
            MasterPage.ShowModalAlert(AlertTypes.Info,
              "Thank you for showing interest in this posting. We shall review your resume and get back to you within the next 2 working days",
              "Thank you");
          }
        }
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Handles the Click event of the btnReferralStatusSubmit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void btnReferralStatusSubmit_Click(object sender, EventArgs e)
    {
      try
      {
        bool interviewOtherInstructions = false;
        bool applicationLogged = false;

        // Don't redisplay the modal if the requirements form has been displayed
        if ((!OriginId.IsTalentOrigin(App.Settings)) || (ReferralEligible || !App.Settings.JobSeekerReferralsEnabled))
        {
          var interestedLogged = false;
          var defaultResumeID = App.UserData.DefaultResumeId ?? 0;

          if (OriginId.IsTalentOrigin(App.Settings))
          {
            interviewOtherInstructions = ServiceClientLocator.JobClient(App).GetJob(Convert.ToInt64(JobId))
              .InterviewOtherInstructions.IsNotNullOrEmpty();
          }

          // If not Email and Talent
          if ((ContactMethod & ContactMethods.Email) == 0 && (ContactMethod & ContactMethods.FocusTalent) == 0)
          {
            if (!OriginId.IsTalentOrigin(App.Settings))
            {
              // Register self-referral
              interestedLogged = ServiceClientLocator.CandidateClient(App)
                .RegisterSelfReferral(Convert.ToInt64(PostingId), App.User.PersonId.GetValueOrDefault(), ReferralScore);

              JobPostingReferralDiv.Visible = jobUrl.Visible = true;
              JobPostingReferralTitleLabel.Visible = false;
              SpideredJobPostingReferralTitleLabel.Visible = true;
              SpideredJobPostingDisclaimerLabel.Visible = App.Settings.SpideredJobDisclaimerEnabled;
              SpideredJobPostingDisclaimerLabel.DefaultText = App.Settings.SpideredJobDisclaimerText;
            }
            else if (!IsAlreadyReferred && App.UserData.HasDefaultResume)
            {
              ServiceClientLocator.OrganizationClient(App).ApplyForJob(
                LensPostingId,
                defaultResumeID,
                true,
                reviewApplication: true
                );              
            }
            applicationLogged = true;
          }
          else
          {
            // If one of the Contact Methods is Focus Talent and the email isn't a contact method, send a New Applicant Notification
            if (ContactMethod.HasFlag(ContactMethods.FocusTalent) && !ContactMethod.HasFlag(ContactMethods.Email))
            {
              var talentPath = App.Settings.TalentApplicationPath.EndsWith("/")
                ? App.Settings.TalentApplicationPath
                : string.Concat(App.Settings.TalentApplicationPath, "/");

              var jobLinkUrl = string.Format("{0}job/view/{1}", talentPath, JobId);

              if (JobId.HasValue)
              {
                var personNameEmail =
                  ServiceClientLocator.JobClient(App).GetHiringManagerPersonNameEmailForJob(JobId.Value);

                var templateValues = new EmailTemplateData
                {
                  RecipientName = personNameEmail.Name,
                  JobLinkUrls = new List<string> {jobLinkUrl},
                  JobTitle = JobTitle,
                  JobId = JobId.Value.ToString(CultureInfo.InvariantCulture),
                  SenderName = string.Concat(App.User.FirstName, " ", App.User.LastName)
                };

                var emailTemplate =
                  ServiceClientLocator.CoreClient(App)
                    .GetEmailTemplatePreview(EmailTemplateTypes.NewApplicantNotification, templateValues);
                ServiceClientLocator.CoreClient(App)
                  .SendEmail(personNameEmail.Email, emailTemplate.Subject, emailTemplate.Body);
              }
            }
          }

          // No need to show the Submit button again. Show the Send Button only if one of the contact methods is email, otherwise OK
          SubmitButtons.Visible = false;
          SendButtons.Visible = ContactMethod.HasFlag(ContactMethods.Email);
          OKButtons.Visible = !ContactMethod.HasFlag(ContactMethods.Email);

          if ((IsAlreadyReferred && ApprovalStatus == ApprovalStatuses.None) && XmlRequirement.IsNullOrEmpty())
          {
            if (App.UserData.HasDefaultResume && LensPostingId.IsNotNullOrEmpty())
              ServiceClientLocator.OrganizationClient(App)
                .ApplyForJob(LensPostingId, defaultResumeID, true, reviewApplication: true);
          }

          if ((ContactMethod != ContactMethods.FocusTalent) ||
              (ContactMethod == ContactMethods.FocusTalent && interviewOtherInstructions))
          {
            lblAlreadyReferred.Text = (IsAlreadyReferred || interestedLogged)
              ? HtmlLocalise("lblAlreadyReferred.Text", "Your interest in this posting has already been logged.")
              : "";

            lblReferralStatus.Text = ReferralApplyMessage.Replace("\r\n", "<br />");

            panReferralStatus.Visible = true;
            panJobRequirements.Visible =
              panJobApplicationInfo.Visible = panJobDetails.Visible = panScreeningPreferences.Visible = false;

            HowToApplyModal.Show();
          }
        }

        // Save the Referral, showing an acknowledgement only if the Contact Method is Focus Talent and no Other Instructions
        if (!applicationLogged)
        {
          SaveReferral(ReferralEligible, false, ReferralScore, ReferralMessage,
            (ContactMethod == ContactMethods.FocusTalent && !interviewOtherInstructions));
        }

        if (ServiceClientLocator.CandidateClient(App).IsCandidateInvitedForThisJob(JobId ?? 0, App.User.PersonId ?? 0))
        {
          ServiceClientLocator.CandidateClient(App).ResolveCandidateIssues(App.User.PersonId ?? 0,
              new List<CandidateIssueType>() {CandidateIssueType.NotRespondingToEmployerInvites}, true);
        }
      
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Send Application Email with Attachment
    /// </summary>
    /// <returns>True if the email is sent.</returns>
    protected bool SendEmailWithAttachment()
    {
      try
      {
        var resumeHtml = ServiceClientLocator.CandidateClient(App).GetResumeAsHtml(App.User.PersonId.GetValueOrDefault(0), true, false);
        byte[] pdfData;
        Utilities.Export2PDF(resumeHtml, out pdfData, App.User.ScreenName.IsNotNull() ? App.User.ScreenName : "");

        var talentPath = App.Settings.TalentApplicationPath.EndsWith("/")
          ? App.Settings.TalentApplicationPath
          : string.Concat(App.Settings.TalentApplicationPath, "/");

        var jobLinkUrl = string.Format("{0}job/view/{1}", talentPath, JobId);

        var templateValues = new EmailTemplateData
        {
          RecipientName = ContactName,
          JobLinkUrls = new List<string> { jobLinkUrl },
          JobTitle = JobTitle,
          SenderName = App.User.FirstName
        };
        var emailTemplate = ServiceClientLocator.CoreClient(App).GetEmailTemplatePreview(EmailTemplateTypes.TalentPostingApplicationtViaEmail, templateValues);

        ServiceClientLocator.CoreClient(App).SendEmail(ContactEmailAddress, emailTemplate.Subject, emailTemplate.Body, false, pdfData, "Resume.pdf");
        return true;
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
        return false;
      }
    }

    /// <summary>
    /// Checks whether the job seeker matches any screening preference on the job for a minimum star limit to even apply
    /// </summary>
    /// <param name="requirementsXml">The requirements XML which contains the screening preference</param>
    /// <returns>True if they pass (or if no such screening preference), false if not</returns>
    private bool IsScreeningPreferenceValid(XmlDocument requirementsXml)
    {
      if (!App.Settings.ScreeningPrefExclusiveToMinStarMatch)
        return true;

      var requirementsNode = requirementsXml.SelectSingleNode("//REQUIREMENTS");
      if (requirementsNode.IsNull() || requirementsNode.Attributes.IsNull())
        return true;

      var screeningAttribute = (XmlAttribute)requirementsNode.Attributes.GetNamedItem("screening");
      if (screeningAttribute.IsNull())
        return true;

      var starMatchLimit = 0;
      switch (screeningAttribute.Value)
      {
        case "7":
          starMatchLimit = 1;
          break;

        case "8":
          starMatchLimit = 2;
          break;

        case "9":
          starMatchLimit = 3;
          break;

        case "10":
          starMatchLimit = 4;
          break;

        case "11":
          starMatchLimit = 5;
          break;
      }

      if (starMatchLimit == 0)
        return true;

      var outScore = GetMatchScore();
      var stars = outScore.ToStarRating(App.Settings.StarRatingMinimumScores);

      return stars >= starMatchLimit;
    }

    /// <summary>
    /// Loads the requirements.
    /// </summary>
    /// <param name="xDoc">The requirements.</param>
    /// <param name="backgroundDoc"></param>
    /// <para name="backgroundDoc">The background document</para>
    private void LoadRequirements(XmlDocument xDoc, XmlDocument backgroundDoc)
    {
      //<REQUIREMENTS screening='0'>
      //  <REQUIREMENT type="AGE" mandate="1">
      //    <MIN>23</MIN>
      //    <REASON> -- REASON -- </REASON>
      //  </REQUIREMENT>
      //  <REQUIREMENT type="MIN_EDU" mandate="1">
      //      Bachelors or equivalent
      //  </REQUIREMENT>
      //  <REQUIREMENT type="LICENSES" mandate="1">
      //    <LICENSE>Licensed Programmer</LICENSE>
      //  </REQUIREMENT>
      //  <REQUIREMENT type="CERTIFICATIONS" mandate="1">
      //    <CERTIFICATE>Certified programmer</CERTIFICATE>
      //  </REQUIREMENT>
      //  <REQUIREMENT type="LANGUAGES" mandate="1">
      //    nEnglish
      //  </REQUIREMENT>
      //  <REQUIREMENT type="SKILLS" mandate="1">
      //  </REQUIREMENT>
      //  <REQUIREMENT type="SPECIAL" mandate="0">
      //    <SPECIAL>Applicant must be able to lift 30 pounds.</SPECIAL>
      //    <SPECIAL>Applicant must work on weekends & holidays upon request</SPECIAL>
      //  </REQUIREMENT>
      //  <REQUIREMENT type="DRIVERS_LICENSE" mandate="1">
      //    <CLASS>Class A</CLASS>
      //    <ENDORSEMENTS>Motorcycle, Passport</ENDORSEMENTS>
      //  </REQUIREMENT>
      //  <REQUIREMENT type="ONET" mandate="1">
      //    <EXPERIENCE>30</EXPERIENCE>
      //    <CODES>11-1011.00</CODES>
      //  </REQUIREMENT>
      //</REQUIREMENTS>

      int? experience = null;

      var job = ServiceClientLocator.JobClient(App).GetJob(JobId??0);

        //additional item to add to the requriment collection which displays mandatory experience  
      if (backgroundDoc.IsNotNull())
      {
        XmlNode backgroundNode = backgroundDoc.SelectSingleNode(@"//background/experience");

        if (!backgroundNode.IsNull() && !String.IsNullOrEmpty(backgroundNode.InnerText.Trim()))
        {
          experience = backgroundNode.InnerText.Trim().ToInt();
        }
      }


      try
      {
        var tblRequirements = new DataTable();
        tblRequirements.Columns.Add(new DataColumn("Category", typeof(string)));
        tblRequirements.Columns.Add(new DataColumn("Requirement", typeof(string)));
        tblRequirements.Columns.Add(new DataColumn("Action", typeof(string)));

        var trRequirement = tblRequirements.NewRow();
        var requirementExists = false;

        // FVN - 5579 - check whether the experience is a mandatory requirement before displaying to the job seeker
        if (experience.IsNotNull() && job.IsNotNull() && job.MinimumExperienceRequired.GetValueOrDefault())
        {
          trRequirement["Category"] = "Experience";

          trRequirement["Requirement"] = CodeLocalise("ExperienceRequirementMessage",
                                                  "You are required to have at least {0} ", experience + " month(s) of experience");
          trRequirement["Action"] = "-1";
          tblRequirements.Rows.Add(trRequirement);


        }

        if (xDoc.IsNotNull())
        {
          var xmlNodeList = xDoc.SelectNodes("//REQUIREMENTS/REQUIREMENT");
          if (xmlNodeList != null)
            foreach (XmlNode requirement in xmlNodeList)
            {
              if (requirement.Attributes != null &&
                  ((requirement.Attributes["mandate"] != null && requirement.Attributes["mandate"].Value == "1") ||
                   (requirement.Attributes["mandatory"] != null && requirement.Attributes["mandatory"].Value == "1") ||
                   (requirement.Attributes["type"] != null && requirement.Attributes["type"].Value == "SPECIAL")))
              {

                trRequirement = tblRequirements.NewRow();
                var requirementRows = new List<DataRow> { trRequirement };

                switch (requirement.Attributes["type"].Value)
                {
                  case "AGE":
                    if (requirement.SelectSingleNode("MIN") != null &&
                        !String.IsNullOrEmpty(requirement.SelectSingleNode("MIN").InnerText.Trim()))
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Minimum age";
                      var selectSingleNode = requirement.SelectSingleNode("MIN");
                      if (selectSingleNode != null)
                        trRequirement["Requirement"] = CodeLocalise("AgeRequirementMessage",
                          "You are required to be at least {0}", selectSingleNode.InnerText.Trim() + " year(s) of age.");

                      if (requirement.SelectSingleNode("REASON") != null &&
                          !String.IsNullOrEmpty(requirement.SelectSingleNode("REASON").InnerText.Trim()))
                        trRequirement["Requirement"] += CodeLocalise("AgeRequirementReasonMessage", "<br/>Reason: {0}", requirement.SelectSingleNode("REASON").InnerText.Trim());

                      trRequirement["Action"] = "-1";
                    }
                    break;

                  case "MIN_EDU":
                    if (!String.IsNullOrEmpty(requirement.InnerText.Trim()))
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Education level";
                      trRequirement["Requirement"] = CodeLocalise("EducationRequirementMessage",
                        "You are required to have at least a '{0}", requirement.InnerText.Trim() + "' or equivalent.");
                      trRequirement["Action"] = "-1";
                    }
                    break;

                  case "LICENSES":
                    if (requirement.SelectNodes("LICENSE").Count > 0)
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Occupational license";
                      trRequirement["Requirement"] = "You are required to hold the following license(s)<br/>";
                      trRequirement["Requirement"] += "<ul>";
                      foreach (XmlNode license in requirement.SelectNodes("LICENSE"))
                        if (!String.IsNullOrEmpty(license.InnerText.Trim()))
                          trRequirement["Requirement"] += "<li>" + license.InnerText.Trim() + "</li>";
                      trRequirement["Requirement"] += "</ul>";
                      trRequirement["Requirement"] = CodeLocalise("LicenceRequirementMessage", trRequirement["Requirement"].ToString());
                      trRequirement["Action"] = "-1";
                    }
                    break;

                  case "CERTIFICATIONS":
                    if (requirement.SelectNodes("CERTIFICATE").Count > 0)
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Certificate";
                      trRequirement["Requirement"] = "You are required to hold the following certificate(s)<br/>";
                      trRequirement["Requirement"] += "<ul>";
                      foreach (XmlNode certificate in requirement.SelectNodes("CERTIFICATE"))
                        if (!String.IsNullOrEmpty(certificate.InnerText.Trim()))
                          trRequirement["Requirement"] += "<li>" + certificate.InnerText.Trim() + "</li>";
                      trRequirement["Requirement"] += "</ul>";
                      trRequirement["Requirement"] = CodeLocalise("CertificateRequirementMessage", trRequirement["Requirement"].ToString());
                      trRequirement["Action"] = "-1";
                    }
                    break;

                  case "LANGUAGES":
                    if ((requirement.SelectNodes("LANGUAGE").Count == 0 && requirement.InnerText.Trim().IsNotNullOrEmpty()) || requirement.SelectNodes("LANGUAGE[not(@PROFICIENCY)]").Count > 0)
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Language";

                      var languages = new StringBuilder();
                      var languageNodes = requirement.SelectNodes("LANGUAGE[not(@PROFICIENCY)]");
                      if (languageNodes.Count > 0)
                      {
                        languages.Append("<ul>");
                        foreach (XmlNode lang in languageNodes)
                          languages.Append(lang.InnerText).Append(", ");

                        languages.Append("</ul>");
                      }
                      else
                        languages.Append(requirement.InnerText.Trim());

                      trRequirement["Requirement"] = CodeLocalise("LanguageRequirementMessage", "You are required to be proficient in the following language(s); {0} ", languages.ToString());
                      trRequirement["Action"] = "-1";
                    }
                    else
                    {
                      requirementRows.Clear();
                    }

                    var proficiencyNodes = requirement.SelectNodes("LANGUAGE[@PROFICIENCY]");
                    if (proficiencyNodes.Count > 0)
                    {
                      requirementExists = true;
                      foreach (XmlElement proficiencyNode in proficiencyNodes)
                      {
                        var lookup = ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.LanguageProficiencies).FirstOrDefault(l => l.ExternalId == proficiencyNode.GetAttribute("PROFICIENCY"));

                        trRequirement = tblRequirements.NewRow();
                        requirementRows.Add(trRequirement);
                        trRequirement["Category"] = "Language";

                        var language = lookup.IsNotNull() ? string.Concat(proficiencyNode.FirstChild.Value, " ", lookup.Text) : proficiencyNode.FirstChild.Value;

                        trRequirement["Requirement"] = CodeLocalise("LanguageRequirementMessage", "You are required to have knowledge of the following language and proficiency level; {0}", language);
                        trRequirement["Action"] = "-1";
                      }
                    }

                    break;

                  case "NCRCLEVEL":
                    var careerLevel = requirement.InnerText.Trim();
                    if (requirement.InnerText.Trim().IsNotNullOrEmpty())
                    {
                      requirementExists = true;
                      trRequirement["Category"] = CodeLocalise("NCRC.Text", "NCRC");
                      trRequirement["Requirement"] = CodeLocalise("NCRC.Text", "You are required to hold a {0} National Career Readiness Certificate", careerLevel + "<br />");
                      trRequirement["Action"] = "-1";
                    }
                    break;

                  case "SKILLS":
                    if (!String.IsNullOrEmpty(requirement.InnerText.Trim()) || requirement.SelectNodes("SKILL").Count > 0)
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Skill";
                      string skills;
                      if (requirement.SelectNodes("SKILL").Count > 0)
                      {
                        skills = requirement.SelectNodes("SKILL").Cast<XmlNode>().Aggregate("<ul>", (current, skill) => current + ("<li>" + skill.InnerText + "</li>"));
                        skills += "</ul>";
                      }
                      else
                        skills = "<br/>" + requirement.InnerText.Trim();

                      trRequirement["Requirement"] = CodeLocalise("SkillRequirementMessage",
                        "You are required to have experience with the following skill(s) {0}", skills);
                      trRequirement["Action"] = "-1";
                    }
                    break;

                  case "SPECIAL":
                    bool mandatoryRequirement = false;
                    if (requirement.Attributes["mandate"] != null && requirement.Attributes["mandate"].Value == "1")
                      mandatoryRequirement = true;
                    else if (requirement.Attributes["mandatory"] != null && requirement.Attributes["mandatory"].Value == "1")
                      mandatoryRequirement = true;
                    else if (requirement.Attributes["mandate"] == null && requirement.Attributes["mandatory"] == null &&
                             requirement.SelectNodes("SPECIAL").Count > 0)
                      mandatoryRequirement = true;

                    if (mandatoryRequirement)
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Special";
                      trRequirement["Requirement"] =
                        "You are required to meet the following mandatory requirements of this job<br/>";
                      trRequirement["Requirement"] += "<ul>";
                      foreach (XmlNode special in requirement.SelectNodes("SPECIAL"))
                        if (!String.IsNullOrEmpty(special.InnerText.Trim()))
                          trRequirement["Requirement"] += "<li>" + special.InnerText.Trim() + "</li>";
                      trRequirement["Requirement"] += "</ul>";
                      trRequirement["Requirement"] = CodeLocalise("SpecialRequirementMessage", trRequirement["Requirement"].ToString());
                      trRequirement["Action"] = "-1";
                    }
                    break;

                  case "DRIVERS_LICENSE":
                    if (requirement.SelectSingleNode("CLASS") != null &&
                        !String.IsNullOrEmpty(requirement.SelectSingleNode("CLASS").InnerText.Trim()))
                    {
                      string endorsements = "";
                      if (requirement.SelectSingleNode("ENDORSEMENTS") != null &&
                          !String.IsNullOrEmpty(requirement.SelectSingleNode("ENDORSEMENTS").InnerText.Trim()))
                        endorsements = requirement.SelectSingleNode("ENDORSEMENTS").InnerText.Trim();

                      requirementExists = true;
                      trRequirement["Category"] = "Driving license";
                      if (!String.IsNullOrEmpty(endorsements))
                        trRequirement["Requirement"] = "You are required to have a '" +
                                                       requirement.SelectSingleNode("CLASS").InnerText.Trim() + "' license with " +
                                                       requirement.SelectSingleNode("ENDORSEMENTS").InnerText.Trim() +
                                                       " endorsements.";
                      else
                        trRequirement["Requirement"] = "You are required to have a '" +
                                                       requirement.SelectSingleNode("CLASS").InnerText.Trim() + "' license.";
                      trRequirement["Action"] = "-1";
                    }
                    break;

                  case "ONET":
                    if (requirement.SelectSingleNode("EXPERIENCE") != null &&
                        !String.IsNullOrEmpty(requirement.SelectSingleNode("EXPERIENCE").InnerText.Trim()) &&
                        requirement.SelectSingleNode("CODES") != null &&
                        !String.IsNullOrEmpty(requirement.SelectSingleNode("CODES").InnerText.Trim()))
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Occupation";
                      string codes;
                      trRequirement["Action"] = "-1";
                      if (requirement.SelectNodes("CODES/CODE").Count > 0)
                      {
                        codes = requirement.SelectNodes("CODES/CODE").Cast<XmlNode>().Aggregate("<ul>", (current, code) => current + ("<li>" + code.InnerText + "</li>"));
                        codes += "</ul>";
                      }
                      else
                        codes = requirement.SelectSingleNode("CODES").InnerText.Trim();

                      trRequirement["Requirement"] = CodeLocalise("OnetRequirementMessage", "You are required to have at least {0}",
                        requirement.SelectSingleNode("EXPERIENCE").InnerText.Trim() +
                        " month(s) of experiecnce in the following occupations.<br/>{0}", codes);

                    }
                    break;

                  case "PROGRAMSOFSTUDY":

                    if (requirement.SelectNodes("PROGRAMOFSTUDY").Count > 0)
                    {
                      requirementExists = true;
                      trRequirement["Category"] = "Programs of Study";
                      trRequirement["Requirement"] = "You are required to be enrolled in the following program(s) of study<br/>";
                      trRequirement["Requirement"] += "<ul>";
                      foreach (XmlNode pos in requirement.SelectNodes("PROGRAMOFSTUDY"))
                      {
                        if (!String.IsNullOrEmpty(pos.InnerText.Trim()))
                          trRequirement["Requirement"] += "<li>" + pos.InnerText.Trim() + "</li>";
                      }
                      trRequirement["Requirement"] += "</ul>";
                      trRequirement["Requirement"] = CodeLocalise("StydyRequirementMessage",
                        trRequirement["Requirement"].ToString());

                      trRequirement["Action"] = "-1";
                    }

                    break;
                }

                if (requirementExists)
                {
                  requirementRows.ForEach(rowToAdd => tblRequirements.Rows.Add(rowToAdd));
                  Model.HasRequirements = true;
                }
              }
            }
        }
        grdJobSkills.DataSource = tblRequirements;
        grdJobSkills.DataBind();
        ViewState["CurrentTableRequirements"] = tblRequirements;

        if (tblRequirements.Rows.Count > 0)
        {
          for (var i = 0; i < tblRequirements.Rows.Count; i++)
          {
            var lblCatrgories = (Label)grdJobSkills.Rows[i].Cells[0].FindControl("lblCategory");
            var lblRequirements = (Label)grdJobSkills.Rows[i].Cells[1].FindControl("lblRequirement");

            var rblAction = (RadioButtonList)grdJobSkills.Rows[i].Cells[2].FindControl("rblAction");
            if (App.User.IsShadowingUser)
            {
              rblAction.RepeatColumns = 3;

              rblAction.Items.Clear();
              rblAction.Items.Add(new ListItem(HtmlLocalise("Global.Yes", "Yes"), "1"));
              rblAction.Items.Add(new ListItem(HtmlLocalise("Global.No", "No"), "0"));
              rblAction.Items.Add(new ListItem(HtmlLocalise("Global.Waive", "Waive"), "-1"));
            }

            rblAction.ID = string.Concat("rblAction_", i.ToString(CultureInfo.InvariantCulture));
            rblAction.Attributes.Add("onclick", "return checkInclude(this);");
            rblAction.Attributes.Add("onkeypress", "return checkInclude(this);");

            lblCatrgories.Text = tblRequirements.Rows[i]["Category"].ToString();
            lblRequirements.Text = tblRequirements.Rows[i]["Requirement"].ToString();
            rblAction.EnableViewState = true;
          }
        }
        else
        {
          VerifyReferral(null);
        }
      }
      catch (ApplicationException ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Verifies the referral.
    /// </summary>
    private void VerifyReferral(List<string> waivedItems)
    {
      try
      {
        WaivedRequirements = waivedItems;

        int score;
        string referralMessage;
        bool outAllCandidatesScreened;
        var referralEligible = IsReferralEligible(out score, out referralMessage, out outAllCandidatesScreened);

        ContactMethod = GetContactMethod();

        ReferralEligible = referralEligible;
        ReferralScore = score;
        ReferralMessage = referralMessage;
        ReferralApplyMessage = string.Empty;

        OKButtons.Visible = false;
        SendButtons.Visible = false;
        SubmitButtons.Visible = true;

        // Setup Email to employer
        if (ContactMethod.HasFlag(ContactMethods.Email))
          SetContactDetailsForEmail();

        var job = ServiceClientLocator.JobClient(App).GetJob(Convert.ToInt64(JobId));
        var otherInfo = job.InterviewOtherInstructions;

        if (ReferralEligible || !App.Settings.JobSeekerReferralsEnabled)
        {
          // Set the Referral Text which is used in the first Job Requirements Modal for confirming the referral          
          if (ContactMethod == ContactMethods.FocusTalent) // Contact Method is Talent
          {
            lblReferralStatus.Text = CodeLocalise("ReferralEligibleDirectEmployer",
              "The #BUSINESS#:LOWER of this posting has chosen to receive applications via their #FOCUSTALENT# account. " +
              "Please click 'Submit' below to forward your resume for consideration.");
          }
          else if (ContactMethod.HasFlag(ContactMethods.FocusTalent)) // Contact Method is Talent and others
          {
            lblReferralStatus.Text = CodeLocalise("ReferralEligibleTalentAndEmail",
              "The #BUSINESS#:LOWER of this posting has chosen to receive applications via their #FOCUSTALENT# account and at least one other method directly. " +
              "When you click 'Submit' you will be shown details of how to make contact and your application will be logged.");
          }
          else // Contact Method is not Talent
          {
            lblReferralStatus.Text = CodeLocalise("ReferralEligibleEmail",
              "The #BUSINESS#:LOWER of this posting has chosen to receive applications directly. When you click 'Submit' you will be shown details " +
              "of how to make contact and your application will be logged.");
          }

          // Set the ReferralApplyMessage Text which is used in the second Job Requirements modal
          if (ContactMethod == ContactMethods.FocusTalent)
          {
            if (otherInfo.IsNotNullOrEmpty()) ReferralApplyMessage += "Other or specific instructions: " + otherInfo;
          }
          if ((ContactMethod == ContactMethods.Email) || ContactMethod == (ContactMethods.Email | ContactMethods.FocusTalent))
          {
            ReferralApplyMessage = CodeLocalise("ReferralEligibleEmailMessage",
              "The #BUSINESS#:LOWER has chosen to receive applications via email to the following address: {0}. Click 'Send' to have a copy of your resume " +
              "forwarded automatically. ", ContactEmailAddress);

            if (otherInfo.IsNotNullOrEmpty()) ReferralApplyMessage += "\r\n\r\nOther or specific instructions: " + otherInfo;
          }
          if (ContactMethod.HasFlag(ContactMethods.Fax) || ContactMethod.HasFlag(ContactMethods.Mail) || ContactMethod.HasFlag(ContactMethods.Online) ||
              ContactMethod.HasFlag(ContactMethods.InPerson) || ContactMethod.HasFlag(ContactMethods.Telephone))
          {
            // Other Contact Methods
            #region Build other Contact Method String

            var contactMethodsString = string.Empty;
            var contactMethodTalentEmailString = string.Empty;
            var methods = 0;

            if ((ContactMethod & ContactMethods.FocusTalent) != 0)
            {
              contactMethodsString += "\r\n#FOCUSTALENT#";
              contactMethodTalentEmailString = "\r\n\r\nYour application has been updated in the business's account.";
              methods++;
            }

            if ((ContactMethod & ContactMethods.Email) != 0)
            {
              contactMethodsString += "\r\nEmail resume to " + ContactEmailAddress;
              if (contactMethodTalentEmailString.Length == 0) contactMethodTalentEmailString = "\r\n\r\n";
              contactMethodTalentEmailString +=
                " Click 'Send' to have a copy of your resume emailed automatically and ensure you also make contact using the other method(s) displayed.";
              methods++;
            }
            else
            {
              if (ContactMethod.HasFlag(ContactMethods.FocusTalent))
              {
                contactMethodTalentEmailString = "\r\n\r\nYour application has been updated in the business's account and ensure you also make contact using the other method(s) displayed.";
              }
            }

            if ((ContactMethod & ContactMethods.Online) != 0)
            {
              contactMethodsString += "\r\nApply online to " + job.InterviewApplicationUrl;
              methods++;
            }

            if ((ContactMethod & ContactMethods.Mail) != 0)
            {
              contactMethodsString += "\r\nMail resume to " + job.InterviewMailAddress.Replace("\r\n", ", ");
              methods++;
            }

            if ((ContactMethod & ContactMethods.Fax) != 0)
            {
              contactMethodsString += "\r\nFax resume to " +
                                      Regex.Replace(job.InterviewFaxNumber, App.Settings.PhoneNumberStrictRegExPattern,
                                        App.Settings.PhoneNumberFormat);
              methods++;
            }

            if ((ContactMethod & ContactMethods.Telephone) != 0)
            {
              contactMethodsString += "\r\nCall for an appointment on " +
                                      Regex.Replace(job.InterviewPhoneNumber, App.Settings.PhoneNumberStrictRegExPattern,
                                        App.Settings.PhoneNumberFormat);
              methods++;
            }

            if ((ContactMethod & ContactMethods.InPerson) != 0)
            {
              contactMethodsString += "\r\nApply in person at " +
                                      job.InterviewDirectApplicationDetails.Replace("\r\n", ", ");
              methods++;
            }

            var otherInstructions = job.InterviewOtherInstructions.IsNotNullOrEmpty()
              ? string.Concat("\r\n", job.InterviewOtherInstructions)
              : string.Empty;

            #endregion

            ReferralApplyMessage += methods > 1
              ? CodeLocalise("ReferralEligibleEmailMessages", "The #BUSINESS#:LOWER has chosen to receive applications via the following methods: {0} {1}{2}", contactMethodsString, otherInstructions, contactMethodTalentEmailString)
              : CodeLocalise("ReferralEligibleEmailMessage", "The #BUSINESS#:LOWER has chosen to receive applications via the following method: {0} {1}{2}", contactMethodsString, otherInstructions, contactMethodTalentEmailString);
          }

          // If referral eligible and Contact Method is not Talent save the referral but do not show the Job Requirements modal
          if (!ContactMethod.HasFlag(ContactMethods.Email) && !ContactMethod.HasFlag(ContactMethods.FocusTalent) && !ContactMethod.HasFlag(ContactMethods.Fax) && !ContactMethod.HasFlag(ContactMethods.Mail) &&
            !ContactMethod.HasFlag(ContactMethods.Online) && !ContactMethod.HasFlag(ContactMethods.InPerson) && !ContactMethod.HasFlag(ContactMethods.Telephone))
          {
            SaveReferral(true, false, score, referralMessage, true);
          }
        }
        else // Referral not eligible
        {
          if (outAllCandidatesScreened)
          {
            if (Model.ReferralCount > 0 && !Model.ReferralEligible && Model.AllCandidatesScreened &&
                ApprovalStatus != ApprovalStatuses.Approved)
              lblReferralStatus.Text = CodeLocalise("lblReferralDetails.Text",
                "This #BUSINESS#:LOWER has requested that all applicants are screened by staff before they can be considered. Your previous referral request to this posting was denied however please click Submit if you wish to be reconsidered");
            else
              lblReferralStatus.Text = CodeLocalise("ReferralAllCandidatesScreened", "This #BUSINESS#:LOWER has requested that all applicants are screened by staff before they can be considered. Please click Submit if you wish to be considered for this role. You will receive notification within 2 days if you have been successful. Please contact our staff if you have a question or require assistance.");

            panJobRequirements.Visible = true;
          }
          else
          {
            lblReferralStatus.Text = (ContactMethod & ContactMethods.FocusTalent) != 0 
              ? CodeLocalise("ReferralNotEligibleDirectEmployer", "You do not meet the designated match score to be able to apply for this job without approval. Staff will contact you within 2 working " +
                "days if your referral request has been successful. Please contact our staff if you have a question or need assistance.")
              : CodeLocalise("ReferralNotEligibleOtherSource", "This #BUSINESS#:LOWER has requested that your resume be screened by #FOCUSASSIST# staff. Please click 'submit' below to send your resume " +
                "for review. If approved, #FOCUSASSIST# staff will send you an email letting you know whether you have been approved and, if so, providing you with application instructions.");
          }
        }

        // Set up the modal if the ReferralStatus has been set
        if (lblReferralStatus.Text.IsNotNullOrEmpty())
        {
          panReferralStatus.Visible = true;
          panJobRequirements.Visible = panJobApplicationInfo.Visible = panJobDetails.Visible = panScreeningPreferences.Visible = false;
          HowToApplyModal.Show();
        }
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Gets the contact method
    /// </summary>
    /// <returns>The contact method specified for the job</returns>
    private ContactMethods GetContactMethod()
    {
      var contactMethod = ContactMethods.None;

      var clientJob = App.GetSessionValue<string>("Career:XMLEosPosting:" + LensPostingId);
      var clientJobData = new XmlDocument();
      if (clientJob.IsNotNullOrEmpty())
        clientJobData.LoadXml(clientJob);

      if (clientJobData.IsNotNull())
      {
        var childNodes = clientJobData.SelectNodes("//*[starts-with(local-name(), 'CONTACT_')][normalize-space(text())='1']");
        if (childNodes.IsNotNull())
        {
          foreach (XmlNode childNode in childNodes)
          {
            switch (childNode.LocalName)
            {
              case "CONTACT_TALENT_FLAG":
                contactMethod |= ContactMethods.FocusTalent;
                break;

              case "CONTACT_EMAIL_FLAG":
                contactMethod |= ContactMethods.Email;
                break;

              case "CONTACT_FAX_FLAG":
                contactMethod |= ContactMethods.Fax;
                break;

              case "CONTACT_POSTAL_FLAG":
                contactMethod |= ContactMethods.Mail;
                break;

              case "CONTACT_PHONE_FLAG":
                contactMethod |= ContactMethods.Telephone;
                break;

              case "CONTACT_URL_FLAG":
                contactMethod |= ContactMethods.Online;
                break;

              case "CONTACT_SEND_DIRECT_FLAG":
                contactMethod |= ContactMethods.InPerson;
                break;
            }
          }
        }
      }

      return contactMethod;
    }

    /// <summary>
    /// Determines whether [is referral eligible] [the specified out score].
    /// </summary>
    /// <param name="outScore">The out score.</param>
    /// <param name="outReferralMessage">The out referral message.</param>
    /// <param name="outAllCandidatesScreened">Whether all candidates should be screened</param>
    /// <returns>
    ///   <c>true</c> if [is referral eligible] [the specified out score]; otherwise, <c>false</c>.
    /// </returns>
    private bool IsReferralEligible(out int outScore, out string outReferralMessage, out bool outAllCandidatesScreened)
    {
      var referralEligible = false;
      outScore = 0;
      outReferralMessage = "";
      outAllCandidatesScreened = false;

      try
      {
        outScore = GetMatchScore();

        if (App.User.IsShadowingUser || IsInviteeOrRecommended)
          return true;

        var xmlrequirement = App.GetSessionValue<string>("Career:XMLRequirement:" + LensPostingId);
        var stars = outScore.ToStarRating(App.Settings.StarRatingMinimumScores);

        if (xmlrequirement.IsNotNullOrEmpty())
        {
          var xDoc = new XmlDocument();
          xDoc.LoadXml(xmlrequirement);

          int screening = 0;
          int minstarrating = 0;
          bool isMinstarrating = false;

          var selectSingleNode = xDoc.SelectSingleNode("//REQUIREMENTS");
          if (selectSingleNode != null && selectSingleNode.Attributes != null)
          {
            if (xDoc.SelectSingleNode("//REQUIREMENTS").Attributes["screening"] != null)
              screening = Convert.ToInt32(xDoc.SelectSingleNode("//REQUIREMENTS").Attributes["screening"].Value);

            //Attribute 'minstarrating' should hold values 0 to 5
            if (xDoc.SelectSingleNode("//REQUIREMENTS").Attributes["minstarrating"] != null)
            {
              minstarrating = Convert.ToInt32(xDoc.SelectSingleNode("//REQUIREMENTS").Attributes["minstarrating"].Value);
              isMinstarrating = true;
            }
          }

          // screening = 1; all referral should be reviewed
          // screening = 0; if referrals are greater than 3 stars; self-referral is allowed
          // screening = -1; no review is required
          // If the screening attribute is set to 0, then the ReferralsMinStars has to be overwritten by this minstarrating value, else use the value provided in configuration of ReferralMinStars.
          switch (screening)
          {
            case -1:
              referralEligible = true;
              outReferralMessage = "All referrals are approved automatically";
              break;

            case 0:
              var addReferralToReview = (stars <= (isMinstarrating ? minstarrating : App.Settings.ReferralMinStars));
              outReferralMessage = addReferralToReview ? "Referral does not meet minimum match score to apply without approval" : "Referral does not need approval";
              break;

            case 1:
              outReferralMessage = "All referrals have to be reviewed by staff";
              outAllCandidatesScreened = true;
              break;

            case 2:
              referralEligible = (stars >= 1);
              outReferralMessage = OutReferralMessage(referralEligible);
              break;

            case 3:
              referralEligible = (stars >= 2);
              outReferralMessage = OutReferralMessage(referralEligible);
              break;

            case 4:
              referralEligible = (stars >= 3);
              outReferralMessage = OutReferralMessage(referralEligible);
              break;

            case 5:
              referralEligible = (stars >= 4);
              outReferralMessage = OutReferralMessage(referralEligible);
              break;

            case 6:
              referralEligible = (stars == 5);
              outReferralMessage = OutReferralMessage(referralEligible);
              break;

            default:
              referralEligible = true;
              break;
          }
        }

      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }

      return referralEligible;
    }

    /// <summary>
    /// Outs the referral message.
    /// </summary>
    /// <param name="referralEligible">if set to <c>true</c> [referral eligible].</param>
    /// <returns></returns>
    private string OutReferralMessage(bool referralEligible)
    {
      return referralEligible ? CodeLocalise("ScoreHigherThanConfiguredLimit", "Referral does not need approval") : CodeLocalise("ScoreLessThanConfiguredLimit", "Referral does not meet minimum match score to apply without approval");
    }

    /// <summary>
    /// Saves the referral.
    /// </summary>
    /// <param name="inReferralEligibleStatus">if set to <c>true</c> [in referral eligible status].</param>
    /// <param name="reviewApplication">if set to <c>true</c> [review application].</param>
    /// <param name="inScore">The in score.</param>
    /// <param name="inReferralMessage">The in referral message.</param>
    /// <param name="showThankYouModal">Show an additional confirmation modal.</param>
    private void SaveReferral(bool inReferralEligibleStatus, bool reviewApplication, int inScore, string inReferralMessage, bool showThankYouModal)
    {
      try
      {
        bool status;

        if (App.UserData.HasDefaultResume && LensPostingId.IsNotNullOrEmpty())
        {
          var defaultResumeID = App.UserData.DefaultResumeId ?? 0;
          status = ServiceClientLocator.OrganizationClient(App).ApplyForJob(
            LensPostingId,
            defaultResumeID,
            inReferralEligibleStatus,
            inScore,
            inReferralMessage,
            reviewApplication,
            WaivedRequirements
          );

          WaivedRequirements = null;
        }
        else
        {
          throw new Exception("No default resume id");
        }

        if (status)
        {
          #region Save Activity
          //TODO: Martha (new activity functionality)
          //if (staffcontext.IsNotNull() && staffcontext.IsAuthenticated)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(PostingPageUserContext.UserId, PostingPageUserContext.Username, ActivityOwner.Staff, ActivityType.Manual, "REF01", customMessage: string.Format("Referral for the job (Customer Job ID: {0}; {2} Job ID: {1}) has been added", customerJobId, jobId, BrandName), staffProfile: staffcontext.StaffInfo);
          //else if (inReferralEligibleStatus)
          //  ServiceClientLocator.AnnotationClient(App).SaveActivity(PostingPageUserContext.UserId, PostingPageUserContext.Username, ActivityOwner.JobSeeker, ActivityType.Manual, "REF01", customMessage: string.Format("Referral for the job (Customer Job ID: {0}; {2} Job ID: {1}) has been added", customerJobId, jobId, BrandName));
          #endregion

          if (inReferralEligibleStatus && IsAlreadyReferred && Model.ReferralCount == 0 && !Model.ReferralEligible)
          {
            GetJobInfo();
          }
          else
          {
            if (showThankYouModal)
            {
              if (inReferralEligibleStatus)
              {
                MasterPage.ShowModalAlert(AlertTypes.Info,
                  CodeLocalise("Interest.NoApproval", "Thank you for showing interest in this posting."),
                  CodeLocalise("Global.ThankYou", "Thank you"));
              }
              else
              {
                MasterPage.ShowModalAlert(AlertTypes.Info,
                  "Thank you for showing interest in this posting. We shall review your resume and get back to you within the next 2 working days",
                  "Thank you");
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Gets the job info.
    /// </summary>
    private void GetJobInfo()
    {
      try
      {
        if (JobInfo.IsNotNull())
        {
          if (JobInfo.PostingStatus.IsNull() || (JobInfo.PostingStatus.IsNotNull() && JobInfo.PostingStatus != PostingStatuses.Active))
          {
            MasterPage.ShowError(AlertTypes.Error, "Job specified might be closed or inactive");
            return;
          }

          if (OriginId.IsTalentOrigin(App.Settings) && Model.ReferralCount > 0 && !Model.ReferralEligible && Model.AllCandidatesScreened && ApprovalStatus != ApprovalStatuses.Approved)
          {
            lblReferralStatus.Text = CodeLocalise("lblReferralDetails.Text", "This #BUSINESS#:LOWER has requested that all applicants are screened by staff before they can be considered. Your previous referral request to this posting was denied however please click Submit if you wish to be reconsidered");

            panJobRequirements.Visible = panJobDetails.Visible = panJobApplicationInfo.Visible = panScreeningPreferences.Visible = false;
            panReferralStatus.Visible = true;
          }
          else if (OriginId.IsTalentOrigin(App.Settings) &&  Model.ReferralCount > 0 && !Model.ReferralEligible && !Model.AllCandidatesScreened && ApprovalStatus != ApprovalStatuses.Approved)
          {
            lblReferralStatus.Text = CodeLocalise("lblReferralDetails.Text", "You do not meet the designated match score to be able to apply for this job without approval. Your previous referral request to this posting was denied however please click Submit if you wish to be reconsidered");

            panJobRequirements.Visible = panJobDetails.Visible = panJobApplicationInfo.Visible = panScreeningPreferences.Visible = false;
            panReferralStatus.Visible = true;
          }
          else
          {
            if (!OriginId.IsTalentOrigin(App.Settings))
            {
              lblReferralDetails.Text = @"The following is the application information for this job.<br />";
              lblReferralDetails.Text += @"Your interest in this posting has already been logged.<br/ ><br/ >";
              if (JobInfo.PostingInfo.IsNotNull())
              {
                lblReferralDetails.Text += @"<b>" + string.Format(CodeLocalise("CustomerPostingId.Label", "{0} Posting ID:</b> {1}<br/ >"),
                  App.Settings.BrandName, JobInfo.PostingInfo.FocusPostingId);
                lblReferralDetails.Text += @"<b>Job Title:</b> " + JobInfo.PostingInfo.Title.Trim() + @"<br />";
                lblReferralDetails.Text += @"<b>Employer:</b> " + JobInfo.PostingInfo.Employer + @"<br />";
								try
								{
									var uriObj = new Uri(lnkJobURL.NavigateUrl);
									lblReferralDetails.Text += @"<b>Link to apply:</b> <a target='_blank' href='" + uriObj.OriginalString + "'>" + uriObj.Scheme + "://" + uriObj.Host + "/..." + "</a><br />";
								}
								catch (Exception)
								{
									lblReferralDetails.Text += @"<b>Link to apply:</b> <a target='_blank' href='" + lnkJobURL.NavigateUrl + "'>" + lnkJobURL.Text + "</a><br />";
								}
								//lblLinkToApply.Text = @"<b>Link to apply:</b> ";
								//SpideredjobUrl.Visible = true;
								//SpideredjobUrl.NavigateUrl = lnkJobURL.NavigateUrl;
								//SpideredjobUrl.Text = lnkJobURL.Text;
              }
            }
            else
            {
              lblReferralDetails.Text = @"The following is the application information for this job<br/><br/>";
              lblReferralDetails.Text = @"The following is the application information for this job<br/><br/>";

              var clientJob = App.GetSessionValue<string>("Career:XMLEosPosting:"+LensPostingId);
              var clientJobData = new XmlDocument();
              if (clientJob.IsNotNullOrEmpty())
                  clientJobData.LoadXml(clientJob);

              var posting = App.GetSessionValue<string>("Career:XMLPosting:"+LensPostingId);
              var postingData = new XmlDocument();
              if (posting.IsNotNullOrEmpty())
                  postingData.LoadXml(posting);

              if (JobInfo.PostingInfo.Employer.Equals("[Confidential]"))
              {
                  if (clientJobData.IsNotNull() && postingData.IsNotNull())
                  {
                      if (clientJobData.SelectSingleNode("//CONTACT_TALENT_FLAG") != null && clientJobData.SelectSingleNode("//CONTACT_TALENT_FLAG").InnerText.Trim() == "1")
                      {
                          var job = ServiceClientLocator.JobClient(App).GetJob(JobInfo.PostingInfo.FocusJobId ?? 0);
                          var employer = ServiceClientLocator.EmployerClient(App).GetEmployer(job.EmployerId);
                          JobInfo.PostingInfo.Employer = employer.Name;
                      }
                  }
              }

              if (JobInfo.PostingInfo.IsNotNull() && JobInfo.PostingInfo.CustomerJobId.IsNotNullOrEmpty() &&
                  (JobInfo.PostingInfo.CustomerJobId != "-999"))
                lblReferralDetails.Text += @"<b>Job Order:</b> " + JobInfo.PostingInfo.CustomerJobId + @"<br />";

              // lblReferralDetails.Text += "<b>" + App.Settings.BrandName + " Job ID:</b> " + LensPostingId + "<br />";

              lblReferralDetails.Text += @"<b>" +
                                         string.Format(
                                           CodeLocalise("CustomerPostingId.Label", "{0} Posting ID:</b> {1}"),
                                           App.Settings.BrandName, JobInfo.PostingInfo.FocusPostingId);
              if (JobInfo.PostingInfo.FocusJobId != null)
                lblReferralDetails.Text += string.Format(CodeLocalise("CustomerJobId.Label", " (<b>Job ID:</b> {0})"),
                  JobInfo.PostingInfo.FocusJobId);

              lblReferralDetails.Text += @"<br/><br/>";

              if (JobInfo.PostingInfo.IsNotNull() && JobInfo.PostingInfo.Title.IsNotNullOrEmpty())
                lblReferralDetails.Text += @"<b>Job Title:</b> " + JobInfo.PostingInfo.Title.Trim() + @"<br />";

              if (OriginId.HasValue && (OriginId.IsTalentOrigin(App.Settings)))
              {
                if (App.User.IsAuthenticated)
                {
                  if (JobInfo.PostingInfo.IsNotNull() && JobInfo.PostingInfo.Employer.IsNotNullOrEmpty())
                    lblReferralDetails.Text += @"<b>" + CodeLocalise("ReferralDetailsEmployer.Text", "Employer") +
                                               @":</b> " + JobInfo.PostingInfo.Employer + @"<br />";
                }
              }
              else // for jobs other than 7, 8, 9
              {
                if (JobInfo.PostingInfo.IsNotNull() && JobInfo.PostingInfo.Employer.IsNotNullOrEmpty())
                  lblReferralDetails.Text += @"<b>" + CodeLocalise("ReferralDetailsEmployer.Text", "Employer") +
                                             @":</b> " + JobInfo.PostingInfo.Employer + @"<br />";
              }

              lblReferralDetails.Text += @"<br/><u>Interview contact information</u><br/>";

              if (clientJobData.IsNotNull() && postingData.IsNotNull())
              {
                var contactPresent = false;
                if (clientJobData.SelectSingleNode("//CONTACT_TALENT_FLAG") != null && clientJobData.SelectSingleNode("//CONTACT_TALENT_FLAG").InnerText.Trim() == "1")
                {
                  lblReferralDetails.Text += CodeLocalise("ReferralDetailsReview.Text", "Your resume has been forwarded to the #BUSINESS# for review.") + @"<br/>";
                  contactPresent = true;
                }

                var sendDirectFlag = false;
                if (clientJobData.SelectSingleNode("//CONTACT_SEND_DIRECT_FLAG") != null && clientJobData.SelectSingleNode("//CONTACT_SEND_DIRECT_FLAG").InnerText.Trim() == "1")
                {
                  lblReferralDetails.Text += @"<b>Apply In-Person</b> ";

                  if (postingData.SelectSingleNode("//contact_method/inperson") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/inperson").InnerText))
                    lblReferralDetails.Text += @"at " + postingData.SelectSingleNode("//contact_method/inperson").InnerText;
                  else
                  {
                    if (clientJobData.SelectSingleNode("//CONTACT_SEND_DIRECT_ADDRESS") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//CONTACT_SEND_DIRECT_ADDRESS").InnerText))
                      lblReferralDetails.Text += @"at " + clientJobData.SelectSingleNode("//CONTACT_SEND_DIRECT_ADDRESS").InnerText;
                  }

                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                  sendDirectFlag = true;
                }

                if ((clientJobData.SelectSingleNode("//CONTACT_POSTAL_FLAG") != null && clientJobData.SelectSingleNode("//CONTACT_POSTAL_FLAG").InnerText.Trim() == "1") || sendDirectFlag)
                {
                  string mailText = "";

                  if (clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR").InnerText))
                  {
                    mailText += clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR").InnerText;
                  }
                  else
                  {
                    if (clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR_1") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR_1").InnerText))
                      mailText += clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR_1").InnerText + "<br />";

                    if (clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR_2") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR_2").InnerText))
                      mailText += clientJobData.SelectSingleNode("//JOB_CONTACT_ADDR_2").InnerText + "<br />";

                    if (clientJobData.SelectSingleNode("//JOB_CONTACT_CITY") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//JOB_CONTACT_CITY").InnerText))
                      mailText += clientJobData.SelectSingleNode("//JOB_CONTACT_CITY").InnerText + ", ";

                    if (clientJobData.SelectSingleNode("//JOB_CONTACT_STATE") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//JOB_CONTACT_STATE").InnerText))
                      mailText += clientJobData.SelectSingleNode("//JOB_CONTACT_STATE").InnerText + ", ";

                    if (clientJobData.SelectSingleNode("//JOB_CONTACT_COUNTRY") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//JOB_CONTACT_COUNTRY").InnerText))
                      mailText += clientJobData.SelectSingleNode("//JOB_CONTACT_COUNTRY").InnerText + ", ";

                    if (clientJobData.SelectSingleNode("//JOB_CONTACT_ZIP") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//JOB_CONTACT_ZIP").InnerText))
                      mailText += clientJobData.SelectSingleNode("//JOB_CONTACT_ZIP").InnerText;

                    // If it is a talent posting; 
                    if (postingData.SelectSingleNode("//contact_method/mail") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/mail").InnerText))
                      mailText += postingData.SelectSingleNode("//contact_method/mail").InnerText;
                  }

                  if (!String.IsNullOrEmpty(mailText.Trim()))
                    lblReferralDetails.Text += @"<b>Mail:</b> " + mailText;

                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }

                if (clientJobData.SelectSingleNode("//CONTACT_PHONE_FLAG") != null && clientJobData.SelectSingleNode("//CONTACT_PHONE_FLAG").InnerText.Trim() == "1")
                {
                  lblReferralDetails.Text += @"<b>Phone:</b> ";
                  // If it is a talent posting; 
                  if (postingData.SelectSingleNode("//contact_method/call") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/call").InnerText))
                    lblReferralDetails.Text += postingData.SelectSingleNode("//contact_method/call").InnerText;
                  else
                  {
                    if (postingData.SelectSingleNode("//JobDoc/posting/contact/phone") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//JobDoc/posting/contact/phone").InnerText))
                      lblReferralDetails.Text += postingData.SelectSingleNode("//JobDoc/posting/contact/phone").InnerText;
                  }
                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }

                if (clientJobData.SelectSingleNode("//CONTACT_FAX_FLAG") != null && clientJobData.SelectSingleNode("//CONTACT_FAX_FLAG").InnerText.Trim() == "1")
                {
                  lblReferralDetails.Text += @"<b>Fax:</b> ";

                  // If it is a talent posting; 
                  if (postingData.SelectSingleNode("//contact_method/fax") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/fax").InnerText))
                    lblReferralDetails.Text += postingData.SelectSingleNode("//contact_method/fax").InnerText;
                  else
                  {
                    if (postingData.SelectSingleNode("//JobDoc/posting/contact/fax") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//JobDoc/posting/contact/fax").InnerText))
                      lblReferralDetails.Text += postingData.SelectSingleNode("//JobDoc/posting/contact/fax").InnerText;
                  }
                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }

                if (clientJobData.SelectSingleNode("//CONTACT_EMAIL_FLAG") != null && clientJobData.SelectSingleNode("//CONTACT_EMAIL_FLAG").InnerText.Trim() == "1")
                {
                  lblReferralDetails.Text += @"<b>E-Mail:</b> ";
                  // If it is a talent posting; 
                  if (postingData.SelectSingleNode("//contact_method/email") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/email").InnerText))
                    lblReferralDetails.Text += postingData.SelectSingleNode("//contact_method/email").InnerText;
                  else
                  {
                    if (postingData.SelectSingleNode("//JobDoc/posting/contact/email") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//JobDoc/posting/contact/email").InnerText))
                      lblReferralDetails.Text += postingData.SelectSingleNode("//JobDoc/posting/contact/email").InnerText;
                  }
                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }

                var contactURL = false;
                var URL = "";
                if (postingData.SelectSingleNode("//joburl") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//joburl").InnerText.Trim()))
                {
                  URL = postingData.SelectSingleNode("//joburl").InnerText.Trim();
                  contactURL = true;
                }

                if (contactURL && ((clientJobData.SelectSingleNode("//CONTACT_URL_FLAG") != null && clientJobData.SelectSingleNode("//CONTACT_URL_FLAG").InnerText == "1") || !OriginId.IsTalentOrigin(App.Settings)))
                {
                  try
                  {
                    var uriObj = new Uri(URL);
                    lblReferralDetails.Text += @"<b>Apply on-line:</b> <a target='_blank' href='" +
                                               uriObj.OriginalString + @"'>" + uriObj.Scheme + @"://" + uriObj.Host + @"/..." + @"</a><br />";
                  }
                  catch (Exception)
                  {
                    lblReferralDetails.Text += @"<b>Apply on-line:</b> <a target='_blank' href='" + URL + @"'>" + URL + @"</a><br />";
                  }

                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }

                if ((postingData.SelectSingleNode("//contact_method/applicant_address") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/applicant_address").InnerText)) ||
                    (postingData.SelectSingleNode("//contact_method/other") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/other").InnerText)) ||
                    (postingData.SelectSingleNode("//contact_method/jobreferenceid") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/jobreferenceid").InnerText)) ||
                    (clientJobData.SelectSingleNode("//CONTACT_OTHER_INSTRUCTIONS") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//CONTACT_OTHER_INSTRUCTIONS").InnerText)))
                  lblReferralDetails.Text += @"<u>Special Instructions</u><br/> ";

                if (postingData.SelectSingleNode("//contact_method/applicant_address") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/applicant_address").InnerText))
                {
                  lblReferralDetails.Text += @"<b>Hiring manager:</b> ";
                  lblReferralDetails.Text += postingData.SelectSingleNode("//contact_method/applicant_address").InnerText;
                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }

                if (postingData.SelectSingleNode("//contact_method/other") != null && !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/other").InnerText))
                {
                  lblReferralDetails.Text += @"<b>Specific instructions:</b> ";
                  lblReferralDetails.Text += postingData.SelectSingleNode("//contact_method/other").InnerText;
                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }
                else if (clientJobData.SelectSingleNode("//CONTACT_OTHER_INSTRUCTIONS") != null && !String.IsNullOrEmpty(clientJobData.SelectSingleNode("//CONTACT_OTHER_INSTRUCTIONS").InnerText))
                {
                  lblReferralDetails.Text += @"<b>Specific instructions:</b> ";
                  lblReferralDetails.Text += clientJobData.SelectSingleNode("//CONTACT_OTHER_INSTRUCTIONS").InnerText;
                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }

                if (postingData.SelectSingleNode("//contact_method/jobreferenceid") != null &&
                    !String.IsNullOrEmpty(postingData.SelectSingleNode("//contact_method/jobreferenceid").InnerText))
                {
                  lblReferralDetails.Text += CodeLocalise("ReferralDetailsJobNo.Text", "The #BUSINESS# asks that you reference job number:");
                  lblReferralDetails.Text += postingData.SelectSingleNode("//contact_method/jobreferenceid").InnerText;
                  lblReferralDetails.Text += @"<br/>";
                  contactPresent = true;
                }

                if (!contactPresent)
                  lblReferralDetails.Text += @"No contact information is provided.<br/>";
              }
            }

            if (App.User.IsShadowingUser)
              btnSendJobInfo.Text = @"Send as email to Jobseeker";

            panJobRequirements.Visible = panJobDetails.Visible = panReferralStatus.Visible = panScreeningPreferences.Visible = false;
            panJobApplicationInfo.Visible = true;
          }

          HowToApplyModal.Show();
        }
      }
      catch (Exception ex)
      {
        MasterPage.ShowError(AlertTypes.Error, ex.Message);
      }
    }

    /// <summary>
    /// Gets the match score.
    /// </summary>
    /// <returns></returns>
    private int GetMatchScore()
    {
      if (App.UserData.HasDefaultResume)
      {
        if (MatchingScore.IsNotNull())
          return MatchingScore.Value;

        // Send match command to check score
        var defaultResumeID = App.UserData.DefaultResumeId ?? 0;
        MatchingScore = (LensPostingId.IsNotNullOrEmpty()) ? ServiceClientLocator.OrganizationClient(App).GetMatchScore(defaultResumeID, LensPostingId) : 0;

        return MatchingScore.Value;
      }

      return 0;
    }

    /// <summary>
    /// Inserts the posting footer.
    /// </summary>
    /// <param name="postingHtml">The posting HTML.</param>
    /// <param name="jobId">The job id.</param>
    /// <returns></returns>
    private string InsertPostingFooter(string postingHtml, long jobId)
    {
      var criminalBackgroundCheckRequired = ServiceClientLocator.JobClient(App).GetCriminalBackgroundExclusionRequired(jobId);
      var creditCheckRequired = ServiceClientLocator.JobClient(App).GetJobCreditCheckRequired(jobId);

      #region Posting footer title

      if ((App.Settings.ShowCriminalBackgroundCheck && criminalBackgroundCheckRequired) || (App.Settings.ShowCreditCheck && creditCheckRequired))
      {
        postingHtml = postingHtml.Replace(Constants.PlaceHolders.PostingFooterTitle, CodeLocalise("PostingFooterTitle.text", App.Settings.PostingFooterTitle));
      }
      else
      {
        postingHtml = postingHtml.Replace(Constants.PlaceHolders.PostingFooterTitle, string.Empty);
      }

      #endregion

      #region Criminal background check footer

      if (App.Settings.ShowCriminalBackgroundCheck && criminalBackgroundCheckRequired)
      {
        // Add the criminal background check TEGL footer
        postingHtml = postingHtml.Replace(Constants.PlaceHolders.CrimFooter, CodeLocalise("JobSeekerCriminalBackgroundJobPostingWarning.text", App.Settings.JobSeekerCriminalBackgroundJobPostingWarning));

        #region Add the URL to the notices page that will show Criminal Background Check legal notices

        var category = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories).Where(x => x.Key.Equals("DocumentCategories.LegalNotice")).Select(x => x.Id).FirstOrDefault());
        var group = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentGroups).Where(x => x.Key.Equals("DocumentGroups.CriminalBackgroundCheck")).Select(x => x.Id).FirstOrDefault());

        postingHtml = postingHtml.Replace(Constants.PlaceHolders.CriminalBackgroundUrl, UrlBuilder.Notice(Uri.EscapeDataString(CodeLocalise("LegalNotice.Title", "Legal Notices")), category, group));

        #endregion
      }
      else
      {
        postingHtml = postingHtml.Replace(Constants.PlaceHolders.CrimFooter, string.Empty);
      }

      #endregion

      #region Credit check footer

      if (App.Settings.ShowCreditCheck && creditCheckRequired)
      {
        #region Add the URL to the notices page that will show Credit Check legal notices

        var category = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentCategories).Where(x => x.Key.Equals("DocumentCategories.LegalNotice")).Select(x => x.Id).FirstOrDefault());
        var group = Convert.ToInt64(ServiceClientLocator.CoreClient(App).GetLookup(LookupTypes.DocumentGroups).Where(x => x.Key.Equals("DocumentGroups.CreditCheck")).Select(x => x.Id).FirstOrDefault());

        var url = UrlBuilder.Notice(Uri.EscapeDataString(CodeLocalise("LegalNotice.Title", "Legal Notices")), category, group);

        #endregion

        // Add the credit check TEGL footer
        postingHtml = postingHtml.Replace(Constants.PlaceHolders.CredFooter, CodeLocalise("JobSeekerCreditCheckJobPostingWarning.text", App.Settings.JobSeekerCreditCheckJobPostingWarning, url));
      }
      else
      {
        postingHtml = postingHtml.Replace(Constants.PlaceHolders.CredFooter, string.Empty);
      }

      #endregion

      return postingHtml.AddAltTagWithValueForImg("Posting image");
    }

    private string SetContactDetailsForEmail()
    {
      var posting = App.GetSessionValue<string>("Career:XMLPosting:" + LensPostingId);
      var postingData = new XmlDocument();
      if (posting.IsNotNullOrEmpty())
        postingData.LoadXml(posting);

      ContactEmailAddress = string.Empty;
      ContactName = string.Empty;

      var emailNode = postingData.SelectSingleNode("//JobDoc/posting/contact/email");
      if (emailNode != null && !String.IsNullOrEmpty(emailNode.InnerText))
        ContactEmailAddress = emailNode.InnerText;

      var contactNameNode = postingData.SelectSingleNode("//JobDoc/posting/contact/person");
      if (contactNameNode != null && !String.IsNullOrEmpty(contactNameNode.InnerText))
        ContactName = contactNameNode.InnerText;

      var clientJob = App.GetSessionValue<string>("Career:XMLEosPosting:"+LensPostingId);
      var clientJobData = new XmlDocument();
      if (clientJob.IsNotNullOrEmpty())
        clientJobData.LoadXml(clientJob);

      var otherNode = clientJobData.SelectSingleNode("//CONTACT_OTHER_INSTRUCTIONS");
      if (otherNode != null && !String.IsNullOrEmpty(otherNode.InnerText))
        return string.Concat(" and ", otherNode.InnerText);

      return string.Empty;
    }

    [Serializable]
    private class ReferralModel
    {
      public long ReferralCount { get; set; }
      public int Score { get; set; }
      public string ReferralMessage { get; set; }
      public bool AllCandidatesScreened { get; set; }
      public bool ReferralEligible { get; set; }
      public bool HasRequirements { get; set; }
    }
  }
}
