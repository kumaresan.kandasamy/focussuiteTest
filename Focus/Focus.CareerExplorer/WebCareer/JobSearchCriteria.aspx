<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JobSearchCriteria.aspx.cs"
    Inherits="Focus.CareerExplorer.WebCareer.JobSearchCriteria" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="../Controls/UnAuthorizedInformation.ascx" TagName="ConfirmationModal"
    TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/SavedSearch.ascx" TagName="SavedSearch"
    TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/SearchJobPostings.ascx" TagName="SearchJobPostings"
    TagPrefix="uc" %>
<%@ Register Src="../Controls/ResumeSearchLink.ascx" TagName="ResumeSearchLink" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/SearchCriteriaWorkDays.ascx" TagName="SearchCriteriaWorkDay"
    TagPrefix="uc" %>
<%@ Register Src="../Controls/LanguageProficiencyManager.ascx" TagName="LanguageProficiencyManager"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
    <uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanelJobSearchCriteria" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="FocusCareer">
                <div>
                    <uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" />
                </div>
                <%--SEARCH JOB POSTINGS--%>
                <uc:SearchJobPostings ID="SearchJobPostings" runat="server" Visible="false" />
                <%--SAVED SEARCH--%>
                <uc:SavedSearch ID="SavedSearch" runat="server" Visible="False" />
                <div class="savedSearchBottom FocusCareer" role="presentation">
                    <div>
                        <div class="col-md-6 col-lg-6 clearfix labelAlign">
                            <div>
                                <%= HtmlInFieldLabel("SearchTermTextBox", "SearchTermTextBox.Label", "search for these words", 210)%>
                                <asp:TextBox ID="SearchTermTextBox" runat="server" ClientIDMode="Static" Width="258px" />
                                <span class="boldedText">
                                    <%= HtmlLocalise("in.Label", "in")%>
                                </span>
                                <asp:DropDownList ID="ddlSearchLocation" runat="server" Width="150px" Title="Search Location Drop Down">
                                    <asp:ListItem>complete job posting</asp:ListItem>
                                </asp:DropDownList>
                                <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                    title="<%=HtmlLocalise("SearchTipsHelp.ToolTip", "<b>Search tips</b><br/>By default, we will search for all words or phrases you enter in the search box.<br/> - To search for an exact phrase, enclose it in quotation marks.<br/> -  Use a minus sign (-) to exclude a term from your search.<br/>") %>" /></div>
                            <div style="padding-top: 14px">
                                <asp:RadioButton runat="server" ID="rbSearchAllWords" GroupName="Operator" Checked="True" />&nbsp;&nbsp;&nbsp;
                                <asp:RadioButton runat="server" ID="rbSearchAnyWords" GroupName="Operator" />
                            </div>
                        </div>
                        <div class="col-md-6 clearfix" style="padding-top: 6px">
                            <div class="col-sm-3 col-md-4 clearfix">
                                <asp:RadioButton ID="rbArea" Text="Search within this area" runat="server" ClientIDMode="Static"
                                    GroupName="Search" onclick="Hide_Radius_StateMSA(this)" /></div>
                            <div class="col-sm-9 col-md-7 col-lg-6 clearfix">
                                <div class="col-sm-5 col-md-8 clearfix">
                                    <asp:DropDownList ID="ddlRadius" runat="server" Width="150px" ClientIDMode="Static"
                                        onchange="CheckRadius();" CssClass="RadiusClass" Title="Radius Drop Down">
                                    </asp:DropDownList>
                                    &nbsp;of</div>
                                <div class="col-sm-7 col-md-4 clearfix" id="zipCodeDiv">
                                    <%= HtmlInFieldLabel("txtZipCode", "ZipCodeBox.Label", "ZIP Code", 128)%>
                                    <asp:TextBox ID="txtZipCode" runat="server" ClientIDMode="Static" Width="128px" MaxLength="5"
                                        onblur="CheckRadius();" />
                                    <ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="txtZipCode"
                                        FilterType="Numbers" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-offset-4 clearfix">
                                <asp:CheckBox ID="cbJobsInMyState" runat="server" CssClass="checkBoxList" ClientIDMode="Static" />
                                <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                    title="<%=HtmlLocalise("SearchAreaHelp.ToolTip", "If your search area is near a state line, the radius criteria may deliver results for jobs in your neighboring state(s).  Check this box to limit search results only to jobs in your state. <br/> Note:  If you opt to limit your search to in-state jobs, make sure that the ZIP code and radius you select includes areas within the state.  Otherwise, you will receive no matches.") %>" />
                                <br />
                                <asp:CustomValidator ID="RadiusValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
                                    CssClass="error" ValidationGroup="Location" ClientValidationFunction="ValidateRadius"
                                    ValidateEmptyText="true" />
                                <asp:CustomValidator ID="SavedSearchRadiusValidator" runat="server" SetFocusOnError="true"
                                    Display="Dynamic" CssClass="error" ValidationGroup="SavedSearch" ClientValidationFunction="ValidateRadius"
                                    ValidateEmptyText="true" />
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-6 col-md-offset-6 clearfix">
                        <div class="col-md-4 clearfix" style="vertical-align: top">
                            <asp:RadioButton ID="rbMSAState" ClientIDMode="Static" Text="Search this state/city"
                                runat="server" GroupName="Search" onclick="Hide_Radius_StateMSA(this)" />
                        </div>
                        <div class="col-md-8 clearfix" style="vertical-align: top;">
                            <asp:DropDownList ID="ddlState" runat="server" Width="320px" ClientIDMode="Static"
                                onchange="CheckMSA();" CssClass="StateClass" Title="State Drop Down">
                                <asp:ListItem Value="">- select a state -</asp:ListItem>
                            </asp:DropDownList>
                            <div style="height: 10px;">
                            </div>
                            <focus:AjaxDropDownList ID="ddlMSA" CssClass="MSAClass" runat="server" Width="320px"
                                onchange="CheckMSA();" ClientIDMode="Static" Title="MSA Drop Down" />
                            <ajaxtoolkit:CascadingDropDown ID="ccdMSA" runat="server" Category="MSA" LoadingText="[Loading City...]"
                                ParentControlID="ddlState" ServiceMethod="GetMSA" ServicePath="~/Services/AjaxService.svc"
                                BehaviorID="ccdMSABehaviorID" TargetControlID="ddlMSA">
                            </ajaxtoolkit:CascadingDropDown>
                        </div>
                    </div>
                    <%--	<tr>
						<td colspan="5">
						</td>
						<td colspan="4">
							<asp:CustomValidator ID="AreaValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
								CssClass="error" ValidationGroup="Location" ClientValidationFunction="ValidateArea"
								ValidateEmptyText="true" />
						</td>
					</tr>--%>
                    <div style="clear: left">
                        <asp:PlaceHolder runat="server" ID="JobIDFilterPlaceHolder">
                            <%= HtmlLocalise("JobID.Label", "Job ID")%>&nbsp;
                            <asp:TextBox ID="txtJobID" title="txt JobID" runat="server" ClientIDMode="Static"
                                Width="128px" MaxLength="36" />
                        </asp:PlaceHolder>
                    </div>
                    <%--              <asp:PlaceHolder runat="server" ID="HomeBasedJobPostingsPlaceHolder">
                <tr>
								  <td colspan="4">
                  </td>
								  <td colspan="4">
								    <asp:RadioButton ID="HomeBasedJobPostingsRadioButton" ClientIDMode="Static" Text="Test" runat="server" GroupName="Search"  onclick="Hide_Radius_StateMSA(this)" />
                    <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%=HtmlLocalise("HomeBasedJobPostingsToolTip.Label", "Selecting this option means only jobs posted directly from this website that have been highlighted as suitable for home-based workers will be displayed in your search results. Spidered jobs from other #BUSINESSES#:LOWER will be excluded.") %>"/>
								  </td>
                </tr>
              </asp:PlaceHolder>--%>
                </div>
                <div class="horizontalRule" style="clear: left">
                </div>
                <table width="100%" class="FocusCareer collapsableTable" role="presentation">
                    <%--SEARCH CRITERIA--%>
                    <tr>
                        <td colspan="3">
                            <asp:Button ID="ExpandCollapseAllButton" runat="server" class="buttonLevel2 is-collapsed"
                                ClientIDMode="Static" Style="float: right" />
                        </td>
                    </tr>
                    <tr class="collapsableRow">
                        <td valign="top" width="800px">
                            <table class="collapsablePanel" role="presentation">
                                <tr id="accJobMatchTitle" runat="server" class="collapsableAccordionTitle">
                                    <td valign="top" width="30px">
                                        <span class="accordionIcon"></span>
                                    </td>
                                    <td valign="top" style="white-space: nowrap; padding-right: 10px">
                                        <a href="javascript:function NoTop() {return false;}">
                                            <%= HtmlLocalise("AccordionHeading2.Label", "JOB MATCHING")%></a>
                                        <%--                                </td>
                                <td valign="top" style="position: relative">
                                        --%>
                                        <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                            title="<%= HtmlLocalise("JobMatchingHelp.ToolTip", "Select the star match (1-5) to find jobs based on how closely your resume matches. A 5-star match will find only jobs to which you are most qualified. Other star match selections find all jobs with that score or higher, but not those with lower-score matches. If you wish to see all available jobs without considering your resume, select the second search option.") %>" />
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr id="accJobMatchContent" runat="server" class="collapsableAccordionContent">
                                    <td colspan="5">
                                        <div class="col-sm-12 col-xs-12">
                                            <table width="100%" role="presentation">
                                                <tr class="first">
                                                <td width="30px">
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-12">
                                                            <asp:RadioButton ID="rbStarMatch" Text="Jobs that are at least a" runat="server"
                                                                GroupName="Match" onclick="CheckIsAuthenticated_JobMatch();EnableDisableExcludeJob();"
                                                                ClientIDMode="Static" />
                                                            <asp:DropDownList ID="ddlStarMatch" runat="server" ClientIDMode="Static" CssClass="StarMatchClass"
                                                                width="110" Title="Start Match Drop Down">
                                                            </asp:DropDownList>
                                                            for your resume
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="last">
                                                    <td width="2%">
                                                    </td>
                                                    <td>
                                                        <div class="col-sm-12 col-xs-12">
                                                            <asp:RadioButton ID="rbWithoutMatch" Text="All jobs, without matching your resume"
                                                                Checked="true" onclick="ResetStar();EnableDisableExcludeJob();" runat="server"
                                                                GroupName="Match" ClientIDMode="Static" />
                                                        </div>
                                                    </td>
                                                </tr>
                                  </table>      
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="collapsablePanel" role="presentation">
                    <tr id="accPostingTitle" runat="server" class="collapsableAccordionTitle">
                        <td valign="top" width="30px">
                            <span class="accordionIcon"></span>
                        </td>
                        <td valign="top">
                            <a href="javascript:function NoTop() {return false;}">
                                <%= HtmlLocalise("AccordionHeading2.Label.NoEdit", "POSTING DATE")%></a><img src="<%= UrlBuilder.HelpIcon() %>"
                                    class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                    title="<%= HtmlLocalise("JobPostingDateHelp.ToolTip", "Select a job-posting date to find jobs based on when a #BUSINESS#:LOWER posted the job. To see all jobs, select 'More than 30 days.' To narrow your search results, decrease the time frame for the posting date.") %>" />
                        </td>
                        <%--                                <td valign="top" width="400px" style="position: relative">
                                </td>--%>
                    </tr>
                    <tr id="accPostingContent" runat="server" class="collapsableAccordionContent">
                        <td colspan="5">
                            <table width="100%" role="presentation">
                                <tr class="first last">
                                    <td width="30px">
                                    </td>
                                    <td width="40%">
                                        Display jobs added
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPostingAge" runat="server" Width="176" Title="Posting Age Drop Down">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="collapsablePanel" role="presentation">
                    <tr id="accEducationTitle" runat="server" class="collapsableAccordionTitle">
                        <td valign="top" width="30px">
                            <span class="accordionIcon"></span>
                        </td>
                        <td valign="top" style="white-space: nowrap; padding-right: 10px">
                            <a href="javascript:function NoTop() {return false;}">
                                <%= HtmlLocalise("AccordionHeading3.Label.NoEdit", "EDUCATION LEVEL")%></a>
                            <%--                                </td>
                                <td valign="top" width="400px" style="position: relative;">--%>
                            <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                title="<%= HtmlLocalise("EducationLevelHelp.ToolTip", "To find jobs with a specific education level, select the appropriate check box to display only openings for that level.  Remember that some #BUSINESSES#:LOWER may not provide an educational level requirement. The last check box will include those jobs in your search results.") %>" />
                        </td>
                    </tr>
                    <tr id="accEducationContent" runat="server" class="collapsableAccordionContent">
                        <td colspan="5">
                            <table width="100%" role="presentation">
                                <tr class="first">
                                    <td width="30px">
                                    </td>
                                    <td>
                                        <asp:CheckBoxList ID="cblEducationRequired" runat="server" class="checkBoxList">
                                        </asp:CheckBoxList>
                                    </td>
                                </tr>
                                <tr class="last">
                                    <td width="30px">
                                    </td>
                                    <td colspan="3">
                                        <asp:CheckBox ID="cbWithoutEducationInfo" runat="server" CssClass="checkBoxList"
                                            Text="Also show jobs without education information" Checked="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="collapsablePanel" role="presentation">
                    <tr id="accSalaryTitle" runat="server" class="collapsableAccordionTitle">
                        <td valign="top" width="30px">
                            <span class="accordionIcon"></span>
                        </td>
                        <td valign="top" style="white-space: nowrap; padding-right: 10px">
                            <a href="javascript:function NoTop() {return false;}">
                                <%= HtmlLocalise("AccordionHeading4.Label.NoEdit", "SALARY LEVEL")%></a>
                            <%--</td>
                                <td valign="top" width="400px" style="position: relative">--%>
                            <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                title="<%= HtmlLocalise("SalaryLevelHelp.ToolTip", "To find jobs with at a specific salary level, enter the salary minimum you hope to find. Keep in mind that many #BUSINESSES#:LOWER prefer to negotiate salaries and do not provide this information in their job descriptions. The last check box will include jobs in your search results that do not list salary information.") %>" />
                        </td>
                    </tr>
                    <tr id="accSalaryContent" runat="server" class="collapsableAccordionContent">
                        <td colspan="5">
                            <table width="100%" role="presentation">
                                <tr class="first">
                                    <td width="30px">
                                    </td>
                                    <td width="14px">
                                        $
                                    </td>
                                    <td width="140px">
                                        <%= HtmlInFieldLabel("minimumSalaryTextBox", "minimumSalaryTextBox.Label", "minimum", 128)%>
                                        <asp:TextBox ID="minimumSalaryTextBox" runat="server" ClientIDMode="Static" Width="128px"
                                            MaxLength="10" onBlur="this.value=formatCurrency(this.value);" />
                                        <ajaxtoolkit:FilteredTextBoxExtender ID="fteWages" runat="server" TargetControlID="minimumSalaryTextBox"
                                            FilterType="Custom" ValidChars=",.0123456789" />
                                    </td>
                                    <td width="400px">
                                        <%= HtmlLocalise("PerYear.Label", "per year")%>
                                    </td>
                                </tr>
                                <tr class="last">
                                    <td width="30px">
                                    </td>
                                    <td colspan="3">
                                        <asp:CheckBox ID="cbWithoutSalaryInfo" runat="server" CssClass="checkBoxList" Text="Also show jobs without salary information"
                                            Checked="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="collapsablePanel" role="presentation">
                    <tr id="accInternshipsTitle" runat="server" class="collapsableAccordionTitle">
                        <td valign="top" width="30px">
                            <span class="accordionIcon"></span>
                        </td>
                        <td valign="top" style="white-space: nowrap; padding-right: 10px">
                            <a href="javascript:function NoTop() {return false;}">
                                <%= HtmlLocalise("AccordionHeading5.Label.NoEdit", "INTERNSHIPS")%></a>
                            <%--</td>
                                <td valign="top" width="400px" style="position: relative">--%>
                            <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                title="<%= HtmlLocalise("IntershipHelp.ToolTip", "The availability of paid and unpaid internships varies from one area to another. You can select whether or not to include or exclude these jobs.") %>" />
                        </td>
                    </tr>
                    <tr id="accInternshipsContent" runat="server" class="collapsableAccordionContent">
                        <td colspan="3">
                            <table width="100%" role="presentation">
                                <tr class="first last">
                                    <td>
                                        <div class="col-md-4 col-sm-3 collapsableLabel">
                                            Display these jobs</div>
                                        <div class="col-md-8 col-sm-9">
                                            <asp:DropDownList ID="ddlInternshipRequired" runat="server" Width="335" Title="Internship Required Drop Down">
                                                <asp:ListItem>all employment opportunities</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="collapsablePanel" role="presentation">
                    <tr id="accLanguagesTitle" runat="server" class="collapsableAccordionTitle">
                        <td valign="top" width="30px">
                            <span class="accordionIcon"></span>
                        </td>
                        <td valign="top" style="white-space: nowrap; padding-right: 10px">
                            <a href="javascript:function NoTop() {return false;}">
                                <%= HtmlLocalise("AccordionHeading5.Label.NoEdit", "LANGUAGES")%></a>
                            <%--</td>
                                <td valign="top" style="position: relative">--%>
                            <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                title="<%= HtmlLocalise("LanguagesHelp.ToolTip", "Add a language and proficiency level to find jobs requiring language skills. When adding multiple languages, you can choose whether you want the search to return jobs containing all or any of your selected languages.") %>" />
                        </td>
                    </tr>
                    <tr id="accLanguagesContent" runat="server" class="collapsableAccordionContent">
                        <td colspan="3">
                            <table width="100%" role="presentation">
                                <tr class="first last">
                                    <td>
                                        <uc:LanguageProficiencyManager ID="LanguageProficiencyMgr" runat="server" ServicePath="~/Services/AjaxService.svc"
                                            ServiceMethod="GetLanguages" LanguageTextboxWidth="200" LanguageTextboxRightMargin="10px"
                                            LanguageProficiencyDropdownWidth="280" ProficiencyLevelAndBelow="true" InstructionalText="Display jobs with these selected language and proficiency levels."
                                            ReverseProficiencyOrder="true" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </td>
                <td width="2%">
                </td>
                </tr>
                <tr class="collapsableRow">
                    <td valign="top" width="800px">
                        <uc:SearchCriteriaWorkDay ID="SearchCriteriaWorkDay" runat="server" />
                        <table class="collapsablePanel" role="presentation">
                            <tr id="accOccupationTitle" runat="server" class="collapsableAccordionTitle">
                                <td valign="top" width="30px">
                                    <span class="accordionIcon"></span>
                                </td>
                                <td valign="top" style="white-space: nowrap; padding-right: 10px">
                                    <a href="javascript:function NoTop() {return false;}">
                                        <%= HtmlLocalise("AccordionHeading6.Label.NoEdit", "OCCUPATION AND INDUSTRY")%></a>
                                    <%--</td>
                                <td valign="top" width="400px" style="position: relative">--%>
                                    <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                        title="<%= HtmlLocalise("OccupationIndustry.ToolTip", "Select an occupation and/or industry from the menus. An occupation describes a specific type of job; an industry describes what a #BUSINESS#:LOWER does overall.") %>" />
                                </td>
                            </tr>
                            <tr id="accOccupationContent" runat="server" class="collapsableAccordionContent">
                                <td colspan="3">
                                    <table width="100%" role="presentation">
                                        <tr class="first last">
                                            <td width="20px">
                                            </td>
                                            <td width="450px">
                                                <p>
                                                    <%= HtmlLocalise("LookingFor.Label", "What are you looking for?")%>
                                                </p>
                                                <p>
                                                    <div style="display: inline-block; width: 95px">
                                                        Occupation</div>
                                                    <div style="display: inline-block;">
                                                        <asp:DropDownList ID="ddlJobFamily" runat="server" Width="370px" ClientIDMode="Static"
                                                            Title="Job Family Drop Down">
                                                            <asp:ListItem>- select job family -</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </p>
                                                <p>
                                                    <div style="display: inline-block; width: 95px">
                                                    </div>
                                                    <div style="display: inline-block;">
                                                        <focus:AjaxDropDownList ID="ddlOccupation" CssClass="OccupationClass" runat="server"
                                                            Width="370px" ClientIDMode="Static" Title="Occupation Drop Down" />
                                                        <ajaxtoolkit:CascadingDropDown ID="ccdOccupation" runat="server" Category="Occupation"
                                                            LoadingText="[Loading occupation...]" ParentControlID="ddlJobFamily" ServiceMethod="GetJobsByCareerArea"
                                                            ServicePath="~/Services/AjaxService.svc" BehaviorID="ccdOccupationBehaviorID"
                                                            TargetControlID="ddlOccupation">
                                                        </ajaxtoolkit:CascadingDropDown>
                                                    </div>
                                                </p>
                                                <p>
                                                    <div style="display: inline-block; width: 95px">
                                                        Industry</div>
                                                    <div style="display: inline-block;">
                                                        <asp:DropDownList ID="ddlIndustry" runat="server" Width="370px" Title="Industry Drop Down">
                                                            <asp:ListItem>- select industry -</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </p>
                                                <p>
                                                    <div style="display: inline-block; width: 95px">
                                                    </div>
                                                    <div style="display: inline-block;">
                                                        <focus:AjaxDropDownList ID="ddlIndustryDetail" CssClass="IndustryClass" runat="server"
                                                            Width="370px" ClientIDMode="Static" Title="Industry Detail Drop Down" />
                                                        <ajaxtoolkit:CascadingDropDown ID="ccdIndustryDetail" runat="server" Category="Industry"
                                                            LoadingText="[Loading industry details...]" ParentControlID="ddlIndustry" ServiceMethod="GetIndustryDetail"
                                                            ServicePath="~/Services/AjaxService.svc" BehaviorID="ccdIndustryBehaviorID" TargetControlID="ddlIndustryDetail">
                                                        </ajaxtoolkit:CascadingDropDown>
                                                    </div>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table class="collapsablePanel" role="presentation">
                            <tr id="accEmergingTitle" runat="server" class="collapsableAccordionTitle">
                                <td valign="top" width="30px">
                                    <span class="accordionIcon"></span>
                                </td>
                                <td valign="top" style="white-space: nowrap; padding-right: 10px">
                                    <a href="javascript:function NoTop() {return false;}">
                                        <%= HtmlLocalise("AccordionHeading7.Label.NoEdit", "EMERGING/HIGH-GROWTH SECTORS")%></a>
                                    <%--</td>
                                <td valign="top" width="400px" style="position: relative;">--%>
                                    <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                        title="<%= HtmlLocalise("EmergingSectorsHelp.ToolTip", "Select an emerging may be high-growth sector. These are industries that are hiring and expanding rapidly.") %>" />
                                </td>
                            </tr>
                            <tr id="accEmergingContent" runat="server" class="collapsableAccordionContent">
                                <td colspan="5">
                                    <table width="100%" role="presentation">
                                        <tr class="first last">
                                            <td width="30px">
                                            </td>
                                            <td>
                                                <focus:ToolTipCheckBoxList ID="cblEmergingSectors" runat="server" class="dataInputHorizontalCheckBoxListFloatingTwo"
                                                    Width="100%">
                                                </focus:ToolTipCheckBoxList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table class="collapsablePanel" role="presentation">
                            <tr id="accPhysicalTitle" runat="server" class="collapsableAccordionTitle">
                                <td valign="top" width="30px">
                                    <span class="accordionIcon"></span>
                                </td>
                                <td valign="top" style="white-space: nowrap; padding-right: 10px">
                                    <a href="javascript:function NoTop() {return false;}">
                                        <%= HtmlLocalise("AccordionHeading8.Label.NoEdit", "PHYSICAL ABILITIES")%></a>
                                    <%--</td>
                                <td valign="top" width="400px" style="position: relative;">--%>
                                    <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                        title="<%= HtmlLocalise("PhysicalAbilitiesHelp.ToolTip", "If you have any physical issues that affect your ability to do a job, use this area to omit jobs with physical requirements from your search results.") %>" />
                                </td>
                            </tr>
                            <tr id="accPhysicalContent" runat="server" class="collapsableAccordionContent">
                                <td colspan="5">
                                    <table width="100%" role="presentation">
                                        <tr class="first">
                                            <td width="30px">
                                            </td>
                                            <td>
                                                <%= HtmlLocalise("PhysicalAbilities.Label", "Do not show jobs that typically involve the following physical abilities:")%>
                                            </td>
                                        </tr>
                                        <tr class="last">
                                            <td width="30px">
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="cblDisabilityCategories" runat="server" class="checkBoxList"
                                                    RepeatColumns="2" Width="100%">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table class="collapsablePanel" role="presentation">
                            <tr id="accJobTypesTitle" runat="server" class="collapsableAccordionTitle">
                                <td valign="top" width="30px">
                                    <span class="accordionIcon"></span>
                                </td>
                                <td valign="top" style="white-space: nowrap; padding-right: 10px">
                                    <a href="javascript:function NoTop() {return false;}">
                                        <%= HtmlLocalise("AccordionHeading12.Label.NoEdit", "TARGET/EXCLUDE BY JOB TYPE")%></a>
                                    <%--                                </td>
                                <td valign="top" width="400px" style="position: relative;">--%>
                                    <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                        title="<%= HtmlLocalise("JobTypesHelp.ToolTip", "The availability of different type of jobs varies from one area to another. You can select whether or not to include or exclude these jobs.") %>" />
                                </td>
                            </tr>
                            <tr id="accJobTypesContent" runat="server" class="collapsableAccordionContent">
                                <td colspan="3">
                                    <table width="100%" role="presentation">
                                        <tr class="first last">
                                            <td width="20px">
                                            </td>
                                            <td>
                                                <div class="col-sm-12 col-xs-12">
                                                    Commission-only jobs
                                                    <asp:DropDownList ID="ddlCommissionBasedJobsRequired" runat="server" Width="335"
                                                        Title="Commision Based Jobs Drop Down">
                                                        <asp:ListItem>all employment opportunities</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="first last">
                                            <td width="20px">
                                            </td>
                                            <td>
                                                <div class="col-sm-12 col-xs-12">
                                                    Commission + salary jobs
                                                    <asp:DropDownList ID="ddlSalaryAndCommissionBasedJobsRequired" runat="server" Width="335"
                                                        Title="Salary And Commission Based Jobs Drop Down">
                                                        <asp:ListItem>all employment opportunities</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="first last">
                                            <td width="20px">
                                            </td>
                                            <td>
                                                <div class="col-sm-12 col-xs-12">
                                                    Home-based jobs
                                                    <asp:DropDownList ID="ddlHomeBasedJobsRequired" runat="server" Width="335" Title="Home Based Jobs Drop Down">
                                                        <asp:ListItem>all employment opportunities</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table id="tblExcludeJobs" runat="server" class="collapsablePanel" clientidmode="Static"
                            role="presentation">
                            <tr id="accExcludeTitle" runat="server" class="collapsableAccordionTitle">
                                <td valign="top" width="30px">
                                    <span class="accordionIcon"></span>
                                </td>
                                <td valign="top" style="white-space: nowrap; padding-right: 10px">
                                    <a href="javascript:function NoTop() {return false;}">
                                        <%= HtmlLocalise("AccordionHeading3.Label.NoEdit", "EXCLUDE JOBS")%></a>
                                    <%--</td>
                                    <td valign="top" width="400px" style="position: relative;">--%>
                                    <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>"
                                        title="<%= HtmlLocalise("ExcludeJobs.ToolTip", "If you want to search for jobs that match some or none of the shown on your resume, select those you wish to exclude from your search results. This helps to narrow your search results only to jobs that are still of interest to you.") %>" />
                                </td>
                            </tr>
                            <tr id="accExcludeContent" runat="server" class="collapsableAccordionContent">
                                <td colspan="5">
                                    <table width="100%" role="presentation">
                                        <tr class="first">
                                            <td width="30px">
                                            </td>
                                            <td>
                                                <%= HtmlLocalise("ExcludeJob.Label", "Search based on your resume but exclude these jobs from being factored into your search results:")%>
                                            </td>
                                        </tr>
                                        <tr class="last">
                                            <td width="30px">
                                            </td>
                                            <td>
                                                <asp:CheckBoxList ID="cblExcludeJobs" runat="server" class="checkBoxList" Width="100%">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
                <uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#MSTWGoImage').attr('alt', 'Microsoft Translator');
        });

        Sys.Application.add_load(function () {

            bindExpandCollapseAllPanels('<%= ExpandCollapseAllButton.ClientID %>');

            var behavior = $find('<%=ccdOccupation.BehaviorID %>');
            if (behavior != null) {
                behavior.add_populated(function () {
                    $(".OccupationClass").next().find(':first-child').text($("#ddlOccupation option:selected").text()).parent().addClass('changed');
                });
            }
            var indBehavior = $find('<%=ccdIndustryDetail.BehaviorID %>');
            if (indBehavior != null) {
                indBehavior.add_populated(function () {
                    $(".IndustryClass").next().find(':first-child').text($("#ddlIndustryDetail option:selected").text()).parent().addClass('changed');
                });
            }
            var indBehavior = $find('<%=ccdMSA.BehaviorID %>');
            if (indBehavior != null) {
                indBehavior.add_populated(function () {
                    $('#ddlMSA').next().find(':first-child').text($('#ddlMSA option:selected').text()).parent().addClass('changed');
                });
            }

            var moneyText = document.getElementById("minimumSalaryTextBox");
            moneyText.value = formatCurrency(moneyText.value);

            var rbArea = $('#rbMSAState');
            var rbRadius = $('#rbArea');

            if ($(rbArea).is(':checked')) {
                Hide_Radius_StateMSA(rbArea);
            }
            else if ($(rbRadius).is(':checked')) {
                Hide_Radius_StateMSA(rbRadius);
            }

            EnableDisableExcludeJob();

            $find('<%=ccdOccupation.BehaviorID %>').add_populated(UpdateOccupation);
        });

        function UpdateOccupation() {
            var jobFamilyvalue = $('#ddlJobFamily').val();

            var occupation = $('#ddlOccupation');
            var occupationVal = occupation.val();
            var selectVal = (occupationVal == null || occupationVal == '') ? -jobFamilyvalue : occupationVal;

            UpdateStyledDropDown(occupation, selectVal);
        }

        function Hide_Radius_StateMSA(sender) {
            var rbArea = $('#rbArea');
            var rbMSA = $('#rbMSAState');
            var ddlState = $('#ddlState');
            var ddlMSA = $('#ddlMSA');
            var ddlRadius = $('#ddlRadius');
            var txtZipCode = $('#txtZipCode');
            var cbJobsInMyState = $('#cbJobsInMyState');

            if ($(sender).is($(rbArea))) {
                EnableStyledDropDown(ddlRadius);
                $(txtZipCode).prop('disabled', false);
                cbJobsInMyState.prop('disabled', false);

                $(ddlMSA).empty();
                $(ddlMSA).append($('<option>', {
                    value: '',
                    text: '- select city -'
                }));

                DisableStyledDropDown(ddlState, true);
                DisableStyledDropDown(ddlMSA, true);
            }
            else {
                EnableStyledDropDown(ddlState);
                EnableStyledDropDown(ddlMSA);
                DisableStyledDropDown(ddlRadius, true);
                $(txtZipCode).prop('disabled', true);
                $(txtZipCode).val('');
                cbJobsInMyState.prop('disabled', true);
                cbJobsInMyState.prop('checked', false);
            }
        }

        function CheckRadius() {
            var rbArea = $('#rbArea');
            var radius = $('#ddlRadius').val();
            var zipCode = $('#txtZipCode').val();
            if (radius != '' || zipCode.value != '') {
                $(rbArea).prop('checked', true);
                Hide_Radius_StateMSA(rbArea);
            }
        }

        function CheckMSA() {
            var rbSearchMSA = $('#rbMSAState');
            var state = $('#ddlState').val();
            var MSA = $('#ddlMSA').val();
            if (state != '' | MSA != '') {
                $(rbSearchMSA).prop('checked', true);
                Hide_Radius_StateMSA(rbSearchMSA);
            }
        }

        function CheckIsAuthenticated_JobMatch() {
            var rbStarMatch = document.getElementsByName("rbStarMatch");
            var rbWithoutMatch = document.getElementsByName("rbWithoutMatch");
            if (rbStarMatch != null) {
                var isAuthenticated = "<%= IsAuthenticated %>";
                if ($("#rbStarMatch").is(':checked') && isAuthenticated == "") {
                    $find("ConfirmationModal").show();
                    $("#rbStarMatch").attr('checked', false);
                    $("#rbWithoutMatch").attr('checked', true);
                }
            }
        }

        function ResetStar() {
            $("#ddlStarMatch").get(0).selectedIndex = 2;
            $(".StarMatchClass").next().find(':first-child').text($("#ddlStarMatch option:selected").text()).parent().addClass('changed');
        }

        function EnableDisableExcludeJob() {
            var rbStarMatch = document.getElementById("rbStarMatch");
            var rbWithoutMatch = document.getElementById("rbWithoutMatch");
            var excludeJobBox = document.getElementById("tblExcludeJobs");
            if (rbStarMatch.checked && excludeJobBox != null) {
                var elements = excludeJobBox.getElementsByTagName("input");
                for (var i = 0; i < elements.length; i++)
                    if (elements[i].type == "checkbox") {
                        elements[i].disabled = false;
                    }
                //excludeJobBox.style.visibility = "visible";
                //excludeJobBox.style.display = "";
            }
            else if (rbWithoutMatch.checked && excludeJobBox != null) {
                var elements = excludeJobBox.getElementsByTagName("input");
                for (var i = 0; i < elements.length; i++)
                    if (elements[i].type == "checkbox") {
                        //elements[i].checked = false;
                        elements[i].disabled = true;
                    }
                //excludeJobBox.style.visibility = "hidden";
                //excludeJobBox.style.display = "none";
            }
        }

        function validateWages(sender, args) {
            var wages = document.getElementById("minimumSalaryTextBox").value;
            var wagesRegEx = /^\d{1,3}(?:\,?\d{3,3})*(?:\.\d{2,2})?$/;
            if (wagesRegEx.test(wages) == false) {
                args.IsValid = false;
            }
        }
        function ValidateRadius(sender, args) {
            var radius = $('#ddlRadius').val();
            var zipCode = $('#txtZipCode').val().trim();
            args.IsValid = !($("#rbArea").is(':checked')) || (radius.length > 0 && zipCode.length == 5);
        }
        //		function ValidateArea(sender, args) {
        //			var ddlState = document.getElementById("ddlState");
        //			var State = ddlState.options[ddlState.selectedIndex].value;
        //			var ddlMSA = document.getElementById("ddlMSA");
        //			var MSA = ddlMSA.options[ddlMSA.selectedIndex].value;
        //			if (!(((State == "none" || State == "") && MSA == "") || ((State != "none" || State != "") && MSA != ""))) {
        //				args.IsValid = false;
        //			}
        //		}
    </script>
</asp:Content>
