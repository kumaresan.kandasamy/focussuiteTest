<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="JobSearchEducationCriteria.aspx.cs"	Inherits="Focus.CareerExplorer.WebCareer.JobSearchEducationCriteria" %>
<%@ Import Namespace="Focus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxtoolkit" %>
<%@ Register Src="../Controls/TabNavigations.ascx" TagName="TabNavigations" TagPrefix="uc" %>
<%@ Register Src="../Controls/UnAuthorizedInformation.ascx" TagName="ConfirmationModal" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/SavedSearch.ascx" TagName="SavedSearch" TagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/SearchJobPostings.ascx" TagName="SearchJobPostings" TagPrefix="uc" %>
<%@ Register Src="../Controls/ResumeSearchLink.ascx" TagName="ResumeSearchLink" TagPrefix="uc" %>
<%@ Register src="~/WebCareer/Controls/EducationControls/InternshipSearchSkillClusters.ascx" tagName="SkillClusters" tagPrefix="uc" %>
<%@ Register Src="~/WebCareer/Controls/JobSearch/SearchCriteriaWorkDays.ascx" TagName="SearchCriteriaWorkDay" TagPrefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderContent" runat="server">
	<uc:TabNavigations ID="TabNavigationMain" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<asp:UpdatePanel ID="UpdatePanelJobSearchCriteria" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<div class="FocusCareer">
					<div><uc:ResumeSearchLink ID="ResumeSearchLink" runat="server" /></div>
			</div>
			<table width="100%" class="FocusCareer">
				<%--SEARCH JOB POSTINGS--%>
				<uc:SearchJobPostings ID="SearchJobPostings" runat="server" Visible="false" />
				<%--SAVED SEARCH--%>
				<uc:SavedSearch ID="SavedSearch" runat="server" Visible="false" />
				<%--SEARCH CRITERIA--%>
				<tr>
					<td colspan="3">
						<table class="savedSearchMiddle">
							<tr>
								<td colspan="4">
									<%= HtmlLocalise("Search.Label", "Search") %>&nbsp;<asp:DropDownList runat="server" ID="InternshipFilterDropDown" ClientIDMode="Static" Width="200" />&nbsp;
								</td>
								<td width="200px">
									<asp:RadioButton ID="rbArea" Text="Search within this area" runat="server" ClientIDMode="Static"
										GroupName="Search" onclick="Hide_Radius_StateMSA(this)" />
								</td>
								<td width="150px">
									<asp:DropDownList ID="ddlRadius" runat="server" Width="150px" ClientIDMode="Static" onchange="CheckRadius();"
										CssClass="RadiusClass">
									</asp:DropDownList>
								</td>
								<td width="25px">
									&nbsp;of
								</td>
								<td>
									<%= HtmlInFieldLabel("txtZipCode", "ZipCodeBox.Label", "ZIP Code", 128)%>
									<asp:TextBox ID="txtZipCode" runat="server" ClientIDMode="Static" Width="128px" MaxLength="5" onblur="CheckRadius();" />
									<ajaxtoolkit:FilteredTextBoxExtender ID="ftbeZip" runat="server" TargetControlID="txtZipCode"
										FilterType="Numbers" />
								</td>
							</tr>
							<tr>
								<td colspan="5">
								</td>
								<td colspan="4">
									<asp:CustomValidator ID="RadiusValidator" runat="server" SetFocusOnError="true" Display="Dynamic"
										CssClass="error" ValidationGroup="Location" ClientValidationFunction="ValidateRadius"
										ValidateEmptyText="true" />
								</td>
							</tr>
							<tr>
								<td width="300px">
									<%= HtmlInFieldLabel("SearchTermTextBox", "SearchTermTextBox.Label", "search for these words", 168)%>
									<asp:TextBox ID="SearchTermTextBox" runat="server" ClientIDMode="Static" Width="278px" />
								</td>
								<td width="34px">
									<span class="boldedText">
										<%= HtmlLocalise("in.Label", "in")%>
									</span>
								</td>
								<td width="155px">
									<asp:DropDownList ID="ddlSearchLocation" runat="server" Width="150px">
										<asp:ListItem>complete job posting</asp:ListItem>
									</asp:DropDownList>
								</td>
								<td>
								   <img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew" alt="<%=HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%=HtmlLocalise("tooltip1.Label", "<b>Search tips</b><br/>By default, we will search for all words or phrases you enter in the search box.<br/> - To search for an exact phrase, enclose it in quotation marks.<br/> -  Use a minus sign (-) to exclude a term from your search.<br/>") %>"/>
								</td>
								<td style="vertical-align: top">
									<asp:RadioButton ID="rbMSAState" ClientIDMode="Static" Text="Search this state/city"
										runat="server" GroupName="Search" onclick="Hide_Radius_StateMSA(this)" />
								</td>
								<td colspan="3" style="vertical-align: top;">
									<asp:DropDownList ID="ddlState" runat="server" Width="320px" ClientIDMode="Static" onchange="CheckMSA();"
										CssClass="StateClass">
										<asp:ListItem Value="">- select a state -</asp:ListItem>
									</asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td colspan="5">
								  <asp:RadioButton runat="server" ID="rbSearchAllWords" GroupName="Operator" Checked="True"/>&nbsp;&nbsp;&nbsp;
								  <asp:RadioButton runat="server" ID="rbSearchAnyWords" GroupName="Operator"/>
								</td>
								<td colspan="3">
									<focus:AjaxDropDownList ID="ddlMSA" CssClass="MSAClass" runat="server" Width="320px" onchange="CheckMSA();"
										ClientIDMode="Static" TabIndex="10" />
									<ajaxtoolkit:CascadingDropDown ID="ccdMSA" runat="server" Category="MSA" LoadingText="[Loading City...]"
										ParentControlID="ddlState" ServiceMethod="GetMSA" ServicePath="~/Services/AjaxService.svc"
										BehaviorID="ccdMSABehaviorID" TargetControlID="ddlMSA">
									</ajaxtoolkit:CascadingDropDown>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="horizontalRule" style="margin-bottom: 0;">
						</div>
					</td>
				</tr>
				<tr>
					<td width="49%" valign="top">
						<table id="PostingDateTable" clientidmode="Static" class="collapsablePanel">
							<tr id="accPostingTitle" runat="server" class="collapsableAccordionTitle">
								<td valign="top" width="30px">
									<span class="accordionIcon"></span>
								</td>
								<td valign="top" width="115px">
									<a href="javascript:function NoTop() {return false;}">
										<%= HtmlLocalise("PostingDate.Label", "POSTING DATE")%></a>
								</td>
								<td style="vertical-align: top; text-align: left" >
									<img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%= HtmlLocalise("PostingDatetooltip.Label", "Select a job-posting date to find jobs based on when an #BUSINESS#:LOWER posted the job. To see all jobs, select 'More than 30 days.' To narrow your search results, decrease the time frame for the posting date.") %>"/>
								</td>
							</tr>
							<tr id="accPostingContent" runat="server" class="collapsableAccordionContent">
								<td colspan="5">
									<table width="100%">
										<tr class="first last">
											<td width="30px">
											</td>
											<td width="135px">
												Display jobs added
											</td>
											<td>
												<asp:DropDownList ID="ddlPostingAge" runat="server" Width="168">
												</asp:DropDownList>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table id="JobsAndIntershipsTable" clientidmode="Static" class="collapsablePanel" runat="server">
							<tr id="accJobsAndIntershipsTitle" runat="server" class="collapsableAccordionTitle on">
								<td valign="top" width="30px">
									<span class="accordionIcon"></span>
								</td>
								<td valign="top" width="560px">
									<a href="javascript:function NoTop() {return false;}">
										<%= HtmlLocalise("JobsAndInternships.Label", "JOBS AND INTERNSHIPS")%></a>
								</td>
							</tr>
							<tr id="accJobsAndIntershipsContent" runat="server" class="collapsableAccordionContent open">
								<td colspan="5">
									<table width="100%" class="first last">
										<tr>
											<td><asp:CheckBox runat="server" ID="OnlyForSchoolCheckBox"/></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table id="ProgramOfStudyTable" clientidmode="Static" class="collapsablePanel">
							<tr id="accProgramOfStudyTitle" runat="server" class="collapsableAccordionTitle on">
								<td valign="top" width="30px">
									<span class="accordionIcon"></span>
								</td>
								<td valign="top" width="560px">
									<a href="javascript:function NoTop() {return false;}">
										<%= HtmlLocalise("ProgramOfStudy.Label", "PROGRAM OF STUDY")%></a>
								</td>
							</tr>
							<tr id="accProgramOfStudyContent" runat="server" class="collapsableAccordionContent open">
								<td colspan="5">
									<table width="100%">
										<tr class="first">
											<td><%= HtmlLocalise("ShowPostingsRelatedTo.Label", "Show postings related to") %></td>
                      <td>
                        <asp:PlaceHolder runat="server" ID="StudyProgramAreaPlaceHolder">
                        <asp:DropDownList runat="server" ID="StudyProgramAreaDropDownList" ClientIDMode="Static" Width="350" Title="Program Area of Study"/>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="DetailedDegreeLevelPlaceHolder">
                            <asp:DropDownList runat="server" ID="DetailedDegreeLevelDropDownList" Width="350" />
                        </asp:PlaceHolder>
                      </td>
					</tr>
                    <tr class="last">
                      <td></td>
                      <td>
                        <asp:PlaceHolder runat="server" ID="StudyProgramAreaDegreesPlaceHolder">
                        <asp:DropDownList runat="server" ID="StudyProgramAreaDegreesDropDownList" ClientIDMode="Static" Width="350" Title="Degree Program Area of Study"/>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="DetailedDegreePlaceHolder">
                            <focus:AjaxDropDownList ID="JobSearchDetailedDegreeAjaxDropDownList" runat="server" Width="370px" CssClass="DetailedDegreeClass" TabIndex="8" />
												    <ajaxtoolkit:CascadingDropDown ID="JobSearchDetailedDegreeCascadingDropDown" runat="server" Category="ClientDataOnly"
													    LoadingText="[Loading degrees...]" ParentControlID="DetailedDegreeLevelDropDownList" ServiceMethod="GetDegreeEducationLevelsByDegreeLevel"
													    ServicePath="~/Services/AjaxService.svc" BehaviorID="ccdDetailedDegreeBehaviorID" TargetControlID="JobSearchDetailedDegreeAjaxDropDownList">
												    </ajaxtoolkit:CascadingDropDown>
                        </asp:PlaceHolder>
                      </td>
                    </tr>
									</table>
								</td>
							</tr>
						</table>
						<table id="ResumeMatchingTable" clientidmode="Static" class="collapsablePanel" runat="server">
							<tr id="accResumeMatchingTitle" runat="server" class="collapsableAccordionTitle on">
								<td valign="top" width="30px">
									<span class="accordionIcon"></span>
								</td>
								<td valign="top" width="160px">
									<a href="javascript:function NoTop() {return false;}">
										<%= HtmlLocalise("ResumeMatching.Label", "RESUME MATCHING")%></a>
								</td>
								<td style="text-align: left; vertical-align: top">
									<img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%= HtmlLocalise("ResumeMatchingtooltip.Label", "Select the star match (1-5) to find jobs based on how closely your resume matches. A 5-star match will find only jobs to which you are most qualified. Other star match selections find all jobs with that score or higher, but not those with lower-score matches. If you wish to see all available jobs without considering your resume, select the second search option.") %>"/>
								</td>
							</tr>
							<tr id="accResumeMatchingContent" runat="server" class="collapsableAccordionContent open">
								<td colspan="5">
										<table width="100%">
										<tr class="first">
											<td width="30px">
											</td>
											<td>
												<asp:RadioButton ID="rbStarMatch" Text="Jobs that are at least a" runat="server"
													GroupName="Match" onclick="CheckIsAuthenticated_JobMatch();EnableDisableExcludeJob();"
													ClientIDMode="Static" />
												<asp:DropDownList ID="ddlStarMatch" runat="server" ClientIDMode="Static" CssClass="StarMatchClass"
													Width="140">
												</asp:DropDownList>
												for your resume
											</td>
										</tr>
										<tr class="last">
											<td width="30px">
											</td>
											<td>
												<asp:RadioButton ID="rbWithoutMatch" Text="All jobs, without matching your resume"
													Checked="true" onclick="ResetStar();EnableDisableExcludeJob();" runat="server"
													GroupName="Match" ClientIDMode="Static" />
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
					<td width="2%">
					</td>
					<td width="49%" valign="top">
					  <uc:SearchCriteriaWorkDay Id="SearchCriteriaWorkDay" runat="server" />

						<table id="OccupationAndIndustryTable" clientidmode="Static" class="collapsablePanel" role="presentation">
							<tr id="accOccupationTitle" runat="server" class="collapsableAccordionTitle">
								<td valign="top" width="30px">
									<span class="accordionIcon"></span>
								</td>
								<td valign="top" width="115px">
									<a href="javascript:function NoTop() {return false;}">
										<%= HtmlLocalise("Occupation.Label", "OCCUPATION")%></a>
								</td>
								<td style="vertical-align: top; text-align: left;">
									<img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%= HtmlLocalise("Occupationtooltip.Label", "Select an occupation from the menus. An occupation describes a specific type of job.") %>"/>
								</td>
							</tr>
							<tr id="accOccupationContent" runat="server" class="collapsableAccordionContent">
								<td colspan="5">
									<table width="100%" role="presentation">
										<tr class="first">
											<td width="30px">
											</td>
											<td colspan="2">
												<%= HtmlLocalise("LookingFor.Label", "What are you looking for?")%>
											</td>
										</tr>
										<tr>
											<td>
											</td>
											<td width="80px" valign="top">
												Occupation
											</td>
											<td>
												<asp:DropDownList ID="ddlJobFamily" runat="server" Width="370px">
													<asp:ListItem>- select job family -</asp:ListItem>
												</asp:DropDownList>
												<br />
												<br />
												<focus:AjaxDropDownList ID="ddlOccupation" CssClass="OccupationClass" runat="server"
													Width="370px" ClientIDMode="Static" TabIndex="8" />
												<ajaxtoolkit:CascadingDropDown ID="ccdOccupation" runat="server" Category="Occupation"
													LoadingText="[Loading occupation...]" ParentControlID="ddlJobFamily" ServiceMethod="GetJobsByCareerArea"
													ServicePath="~/Services/AjaxService.svc" BehaviorID="ccdOccupationBehaviorID" TargetControlID="ddlOccupation">
												</ajaxtoolkit:CascadingDropDown>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<table ID="ExcludeJobsTable" runat="server" class="collapsablePanel" clientidmode="Static">
							<tr id="accExcludeTitle" runat="server" class="collapsableAccordionTitle">
								<td valign="top" width="30px">
									<span class="accordionIcon"></span>
								</td>
								<td valign="top" width="115px">
									<a href="javascript:function NoTop() {return false;}">
										<%= HtmlLocalise("ExcludeJobs.Label", "EXCLUDE JOBS")%></a>
								</td>
								<td style="vertical-align: top; text-align: left;">
									<img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%= HtmlLocalise("ExcludeJobstooltip.ToolTip", "If you want to search for jobs that match some of the jobs shown on your resume, select those you wish to exclude from your search results. This helps to narrow your search results only to jobs that are still of interest to you.") %>"/>
								</td>
							</tr>
							<tr id="accExcludeContent" runat="server" class="collapsableAccordionContent">
								<td colspan="5">
									<table width="100%">
										<tr class="first">
											<td width="30px">
											</td>
											<td>
												<%= HtmlLocalise("ExcludeJob.Label", "Search based on your resume but exclude these jobs from being factored into your search results:")%>
											</td>
										</tr>
										<tr class="last">
											<td width="30px">
											</td>
											<td>
												<asp:CheckBoxList ID="cblExcludeJobs" runat="server" class="checkBoxList" 
													Width="100%">
												</asp:CheckBoxList>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
            <table ID="InternshipSearchTable" runat="server" class="collapsablePanel" clientidmode="Static">
							<tr id="accInternshipSearchTitle" runat="server" class="collapsableAccordionTitle">
								<td valign="top" width="30px">
									<span class="accordionIcon"></span>
								</td>
								<td valign="top" width="135px">
									<a href="javascript:function NoTop() {return false;}">
										<%= HtmlLocalise("InternshipArea.Label", "INTERNSHIP AREA")%></a>
								</td>
								<td style="vertical-align: top; text-align: left;">
									<img src="<%= UrlBuilder.HelpIcon() %>" class="toolTipNew tooltipstered" alt="<%= HtmlLocalise("Global.Help.ToolTip", "Help") %>" title="<%= HtmlLocalise("InternshipAreatooltip.Label", "Search by skills that are relevant to Internships") %>"/>
								</td>
							</tr>
							<tr id="accInternshipSearchContent" runat="server" class="collapsableAccordionContent">
								<td colspan="5">
									<table width="100%">
										<tr class="last">
											<td>
												<uc:SkillClusters runat="server" ID="InternshipSkillClusters" WrapperCssClass="internshipSkillClustersWrapper" SkillClustersCssClass="internshipSkillClusters" SummaryCssClass="internshipSkillClustersSummary" />
											</td>
										</tr>
									</table>
									
								</td>
							</tr>
            </table>
					</td>
				</tr>
			</table>
			<uc:ConfirmationModal ID="ConfirmationModal" runat="server" />
		</ContentTemplate>
	</asp:UpdatePanel>
	<script type="text/javascript">
	  Sys.Application.add_load(JobSearchCriteria_PageLoad);
	  function JobSearchCriteria_PageLoad(sender, args) {
	    $("input[type='radio'],input[type='checkbox']").each(function () {
	      $(this).attr("autocomplete", "off");
	    });

	    var degBehavior = $find('<%=JobSearchDetailedDegreeCascadingDropDown.BehaviorID %>');
	    if (degBehavior != null) {
	      degBehavior.add_populated(function () {
	        $(".DetailedDegreeClass").next().find(':first-child').text($("#<%=JobSearchDetailedDegreeAjaxDropDownList.ClientID%> option:selected").text()).parent().addClass('changed');
	      });
	    }
	    
	    var behavior = $find('<%=ccdOccupation.BehaviorID %>');
	    if (behavior != null) {
	      behavior.add_populated(function () {
	        $(".OccupationClass").next().find(':first-child').text($("#ddlOccupation option:selected").text()).parent().addClass('changed');
	      });
	}
	      
	    var indBehavior = $find('<%=ccdMSA.BehaviorID %>');
	    if (indBehavior != null) {
	      indBehavior.add_populated(function () {
	        $(".MSAClass").next().find(':first-child').text($("#ddlMSA option:selected").text()).parent().addClass('changed');
	      });
	    }
	    var rbArea = document.getElementById("rbMSAState");
	    var rbRadius = document.getElementById("rbArea");
	    //			var cbJobsInMyState = document.getElementById("cbJobsInMyState");
	    if ($('#rbMSAState').is(':checked')) {
	      Hide_Radius_StateMSA(rbArea);
	    }
	    else if ($('#rbArea').is(':checked')) {
	      Hide_Radius_StateMSA(rbRadius);
	    } else {
	      Hide_Radius_StateMSA(null);
	    }

	     EnableDisableExcludeJob();

	     $find('<%=ccdOccupation.BehaviorID %>').add_populated(UpdateOccupation);

	     if (degBehavior != null) {
	       degBehavior.add_populated(UpdateDetailedDegree);
	     }
	    }

	    function UpdateOccupation() {
	    	var jobFamilyvalue = $('#ddlJobFamily').val();
	    	var occupation = $('#ddlOccupation');

	    	UpdateStyledDropDown(occupation, -jobFamilyvalue);
	   }

	   function UpdateDetailedDegree() {
	     var degreeLevel = $('#<%=DetailedDegreeLevelDropDownList.ClientID%>').val();
	     var degree = $('#<%=JobSearchDetailedDegreeAjaxDropDownList.ClientID%>');

	     UpdateStyledDropDown(degree, degreeLevel);
	   }

    //TODO: Martha - check below (low) function replaced 07.03.2013 - backup available
		function Hide_Radius_StateMSA(sender) {
		  var rbArea = document.getElementById("rbArea");
		  var rbMSAState = document.getElementById("rbMSAState");
			var ddlState = document.getElementById("ddlState");
			var ddlMSA = document.getElementById("ddlMSA");
			var ddlRadius = document.getElementById("ddlRadius");
			var txtZipCode = document.getElementById("txtZipCode");
			if (sender == rbArea) {
				ddlRadius.disabled = false;
				txtZipCode.disabled = false;
				
				$("#ddlMSA").empty();
				var opt = document.createElement("option");
				opt.value = "";
				opt.text = "- select city -";
				ddlMSA.options.add(opt);

				ddlState.disabled = true;
				ddlMSA.disabled = true;
				$("#ddlState").get(0).selectedIndex = 0;
				$(".StateClass").next().find(':first-child').text($("#ddlState option:selected").text()).parent().addClass('changed');
				$("#ddlMSA").get(0).selectedIndex = 0;
				$(".MSAClass").next().find(':first-child').text($("#ddlMSA option:selected").text()).parent().addClass('changed');
			}
		else if (sender == rbMSAState) {
			  ddlState.disabled = false;
			  ddlMSA.disabled = false;
			  ddlRadius.disabled = true;
			  txtZipCode.disabled = true;
			  $("#ddlRadius").get(0).selectedIndex = 0;
			  $(".RadiusClass").next().find(':first-child').text($("#ddlRadius option:selected").text()).parent().addClass('changed');
			  txtZipCode.value = "";
			} else {
			  ddlRadius.disabled = true;
			  txtZipCode.disabled = true;
			  ddlState.disabled = true;
			  ddlMSA.disabled = true;
			}
		}

	  function CheckRadius() {
	  	var rbArea = document.getElementById("rbArea");
	    var ddlRadius = document.getElementById("ddlRadius");
	    var Radius = ddlRadius.options[ddlRadius.selectedIndex].value;
	    var txtZipCode = document.getElementById("txtZipCode");
	    if (Radius != "" || txtZipCode.value != "") {
	      $('#rbArea').attr('checked', 'checked');
	      Hide_Radius_StateMSA(rbArea);
	    }
	  }
	  function CheckMSA() {
	    var rbMSAState = document.getElementById("rbMSAState");
	    var ddlState = document.getElementById("ddlState");
	    var State = ddlState.options[ddlState.selectedIndex].value;
	    var ddlMSA = document.getElementById("ddlMSA");
	    var MSA = ddlMSA.options[ddlMSA.selectedIndex].value;
	    if (State != "" | MSA != "") {
	      $('#rbMSAState').attr('checked', 'checked');
	      Hide_Radius_StateMSA(rbMSAState);
	    }
	  }

	  function CheckIsAuthenticated_JobMatch() {
	    var rbStarMatch = document.getElementsByName("rbStarMatch");
	    if (rbStarMatch != null) {
	      var isAuthentication = "<%= IsAuthenticated %>";
	      if ($("#rbStarMatch").is(':checked') && isAuthentication == "") {
	        $find("ConfirmationModal").show();
	        $("#rbStarMatch").attr('checked', false);
	        $("#rbWithoutMatch").attr('checked', true);
	      }
	    }
	  }

	  function ResetStar() {
	    $("#ddlStarMatch").get(0).selectedIndex = 2;
	    $(".StarMatchClass").next().find(':first-child').text($("#ddlStarMatch option:selected").text()).parent().addClass('changed');
	  }

	  function EnableDisableExcludeJob() {
	    var rbStarMatch = document.getElementById("rbStarMatch");
	    var rbWithoutMatch = document.getElementById("rbWithoutMatch");
	    var excludeJobBox = document.getElementById("ExcludeJobsTable");
	    if (rbStarMatch != null && rbStarMatch.checked && excludeJobBox != null) {
	      var elements = excludeJobBox.getElementsByTagName("input");
	      for (var i = 0; i < elements.length; i++)
	        if (elements[i].type == "checkbox") {
	          elements[i].disabled = false;
	        }
	    }
	    else if (rbStarMatch != null && rbWithoutMatch.checked && excludeJobBox != null) {
	      var elements = excludeJobBox.getElementsByTagName("input");
	      for (var i = 0; i < elements.length; i++)
	        if (elements[i].type == "checkbox") {
	          elements[i].disabled = true;
	        }
	    }
	  }

	  function ValidateRadius(sender, args) {
	    var ddlRadius = document.getElementById("ddlRadius");
	    var Radius = ddlRadius.options[ddlRadius.selectedIndex].value;
	    var txtZipCode = document.getElementById("txtZipCode");
	    if (!((Radius == "" && txtZipCode.value == "") || (Radius != "" && txtZipCode.value != ""))) {
	      args.IsValid = false;
	    }
	  }


	  $(document).ready(function () {
	    ShowHidePanels();

	    var starMatchDropDown = $('#ddlStarMatch');
	    if (starMatchDropDown.is(":disabled")) {
	      starMatchDropDown.next(".customStyleSelectBox").addClass('stateDisabled');
	      $("#<%=accResumeMatchingTitle.ClientID%>").removeClass("on");
	      $("#<%=accResumeMatchingContent.ClientID%>").removeClass("open");
	      $("#<%=accResumeMatchingContent.ClientID%>").hide();
	    }
	  });

	  $('#InternshipFilterDropDown').change(function () {
	  	ShowHidePanels();
	  });

		function ShowHidePanels() {
			switch ($('#InternshipFilterDropDown').prop("selectedIndex"))
			{
				// Jobs
				case 0:
					$('#ProgramOfStudyTable').show();
					$('#OccupationAndIndustryTable').show();
					$('#ExcludeJobsTable').show();
					$('#InternshipSearchTable').hide();
					break;

				//Internships
				case 1:
					$('#ProgramOfStudyTable').hide();
					$('#OccupationAndIndustryTable').hide();
					$('#ExcludeJobsTable').hide();
					$('#InternshipSearchTable').show();
					break;

				//Jobs and Internships
				case 2:
					$('#ProgramOfStudyTable').show();
					$('#OccupationAndIndustryTable').show();
					$('#ExcludeJobsTable').show();
					$('#InternshipSearchTable').show();
					break;
 			}
		}
</script>
</asp:Content>
