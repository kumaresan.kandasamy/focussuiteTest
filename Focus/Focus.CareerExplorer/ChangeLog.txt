﻿
Version 2.0.6 Build 00
Authors: 

UdayaSankar
11/12/2012 Bug fixes: State drop-down
12/12/2012 Bug Fixes: Meeting dt.11-12-12(point#4-7,16-20)
Change in user session storage
12/12/2012 Bug fix: HideOption
DebKumar
13/12/2012 Bug fixes for SaveSearch and Resume info
UdayaSankar
14/12/2012 Bug fixes: Adding to Top (Language & Skill in Education tab)
Process bar Icon for "Am I good match"
Radius duplicating in Review criteria 
Added:3A
DebKumar
14/12/2012 Bug Fix: using basic information in a resume.
14/12/2012 BugFix : Taking resume content from registration info.
Udaya Sankar
14/12/2012 Bug fix: Bugzilla ID: 6592
Bug Fix: Bubzilla Bug ID: 6617
17/12/2012 Bug fixes: Wages validation
DebKumar
17/12/2012 Save Search defaults has now displayed and selected values are taken from saved search.
UdayaSankar
18/12/2012 Bug fixes: 
341	Need Validation In Email Job popup box in Job Result Page.
342	Job Titles in Add a job tab is not shown in proper place
343	Page text is getting overlapped with the button
344	Zip Code field accepts more than 9 digits
346	Paste/Type Your Resume - No. of words is not preserved
Debkumar
19/12/2012 County has been added in Contact tab.
City has been added to degree section of Education tab.
UdayaSankar
20/12/2012 Added: County in Contact tab
Added: City in Education tab
20/12/2012 bug fix:330	"Please wait" when clicked from preview page
Hided: County in contact tab, city in Education tab for 21/12 release

