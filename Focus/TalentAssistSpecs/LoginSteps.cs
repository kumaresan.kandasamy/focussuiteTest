﻿using System;
using TechTalk.SpecFlow;

namespace Focus.UnitTests....TalentAssistSpecs
{
    [Binding]
    public class LoginSteps
    {
        [Given(@"I have entered a valid username and password")]
        public void GivenIHaveEnteredAValidUsernameAndPassword()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I click the login button")]
        public void WhenIClickTheLoginButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should successfully log in")]
        public void ThenIShouldSuccessfullyLogIn()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
