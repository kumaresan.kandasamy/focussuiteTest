﻿using System;
using Focus;
using Focus.Core.Models;
using Focus.Services;
using Focus.Services.Core;
using Framework.PageState;
using Framework.ServiceLocation;

namespace TalentAssistSpecs.Code
{
	internal class TestApp : IApp
	{
		private ServicesRuntime<StructureMapServiceLocator> _servicesRuntime;

		//internal TestApp(StructureMapServiceLocator serviceLocator)
		//{
		//  _servicesRuntime = new ServicesRuntime<StructureMapServiceLocator>(ServiceRuntimeEnvironment.DllOrExe, serviceLocator);
		//  User = new UserContext();
		//}

		private const string _clientTag = "SomeClientTag";
		private const string _guidString = "21EC2020-3AEA-1069-A2DD-08002B30309D";

		public void Start()
		{
			_servicesRuntime = new ServicesRuntime<StructureMapServiceLocator>(ServiceRuntimeEnvironment.Web, new StructureMapServiceLocator());
			_servicesRuntime.Start();

			// Initialize the services
			_servicesRuntime.Start();
		}

		public void Shutdown()
		{
		}

		public void BeginRequest()
		{
		}

		public void EndRequest()
		{
		}

		public string ClientTag { get { return _clientTag; } }
		public Guid SessionId { get { return new Guid(_guidString); } }
		public Guid RequestId { get { return new Guid(_guidString); } }
		public IUserContext User { get; set; }
		public IAppSettings Settings { get {return new FakeAppSettings();} }
		public string VersionInfo { get; private set; }
		public string CurrentModuleNamePlaceHolder { get; private set; }
		public Exception Exception { get; set; }
		public IUserContext DeserializeUserContext(string serializedUserData)
		{
			return default(IUserContext);
		}

		public void UpdateRoles(long? userId)
		{
		}

		public string BuildKey(string key)
		{
			return null;
		}

		public T GetCacheValue<T>(string key, T defaultValue = default(T))
		{
			return default(T);
		}

		public void SetCacheValue<T>(string key, T value)
		{
		}

		public T GetSessionValue<T>(string key, T defaultValue = default(T))
		{
			return default(T);
		}

		public void SetSessionValue<T>(string key, T value)
		{
		}

		public void RemoveSessionValue(string key)
		{
		}

		public void SetCookieValue(string key, string value)
		{
		}

		public string GetCookieValue(string key, string defaultValue = null)
		{
			return null;
		}

		public void RemoveCookieValue(string key)
		{
		}

		public T GetContextValue<T>(string key)
		{
			return default(T);
		}

		public void SetContextValue<T>(string key, T value)
		{
		}

		public string Content(string virtualPath)
		{
			return null;
		}

		public bool IsAssistUserAndCanViewEmployers(IUserContext userContext)
		{
			return false;
		}

		public bool IsAssistUserAndCanViewJobSeekers(IUserContext userContext)
		{
			return false;
		}

		public bool IsAssistUserAndCanViewQueues(IUserContext userContext)
		{
			return false;
		}

		public bool IsInRole(IUserContext userContext, string role)
		{
			return false;
		}

		public void Save(IUserContext userContext)
		{
		}

		public string SerializeUserContext(IUserContext userContext)
		{
			return null;
		}
	}
}
