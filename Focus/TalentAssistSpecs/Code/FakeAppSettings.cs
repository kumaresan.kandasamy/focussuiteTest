﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Text;

using Framework.Logging;
using Framework.PageState;

using Focus.Core;
using Focus.Services.Core.Onet;
using Focus.Services.Core;
using Focus.Services.Repositories.Lens;

#endregion

namespace TalentAssistSpecs.Code
{
	public  class FakeAppSettings : IAppSettings
	{
    public FakeAppSettings()
    {
      ProfilingEnabled = false;
      ValidateServiceVersion = false;
    }

		/// <summary>
		/// Gets the config.
		/// </summary>
		/// <value>The config.</value>
		public string Config
		{
			get { return "UnitTest"; }
		}

		/// <summary>
		/// Gets the module.
		/// </summary>
		/// <value>The module.</value>
		public FocusModules Module
		{
			get { return FocusModules.Talent; }
		}

		/// <summary>
		/// Gets the theme.
		/// </summary>
		/// <value>The theme.</value>
		public FocusThemes Theme
		{
			get { return FocusThemes.Workforce; }
		}

		/// <summary>
		/// Gets the application.
		/// </summary>
		public string Application
		{
			get { return "Focus Fake"; }
		}

		/// <summary>
		/// Gets the client tag.
		/// </summary>
		/// <value>The client tag.</value>
		public string ClientTag
		{
			get { return "ABC123"; }
		}

		/// <summary>
		/// Gets the log severity.
		/// </summary>
		public LogSeverity LogSeverity
		{
			get { return (LogSeverity) Enum.Parse(typeof (LogSeverity), "Info"); }
		}

    /// <summary>
    /// Gets a value indicating whether [profiling enabled].
    /// </summary>
    public bool ProfilingEnabled { get; set; }

		/// <summary>
		/// Gets the cache prefix.
		/// </summary>
		public string CachePrefix
		{
			get { return "Focus"; }
		}

		/// <summary>
		/// Gets the on error email.
		/// </summary>
		public string OnErrorEmail
		{
			get { return "test@test.com"; }
		}

		/// <summary>
		/// Gets the app version.
		/// </summary>
		public string AppVersion
		{
			get { return "Version Unit.Test"; }
		}

		/// <summary>
		/// Gets the license expiration.
		/// </summary>
		/// <value>The license expiration.</value>
		public int LicenseExpiration
		{
			get { return 120; }
		}

		/// <summary>
		/// Gets the SMTP server.
		/// </summary>
		public string SmtpServer
		{
			get { return "NOTSET"; }
		}

		/// <summary>
		/// Gets the user name reg ex pattern.
		/// </summary>
		public string UserNameRegExPattern
		{
			get { return @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"; }
		}

		/// <summary>
		/// Gets the email address reg ex pattern.
		/// </summary>
		public string EmailAddressRegExPattern
		{
			get { return @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"; }
		}

		/// <summary>
		/// Gets the strong password reg ex.
		/// </summary>
		public string PasswordRegExPattern
		{
			get { return @"^[a-zA-Z0-9_]{6,20}$"; } 
		}

    /// <summary>
    /// Gets the URL reg ex pattern.
    /// </summary>
    public string UrlRegExPattern
    {
      get { return @"\b(((\S+)?)(@|mailto\:|(news|(ht|f)tp(s?))\://)\S+)\b"; }
    }

    /// <summary>
    /// Gets the URL reg ex pattern.
    /// </summary>
    public string FEINRegExPattern
    {
      get { return @"^[0-9]{2}-[0-9]{7}$"; }
    }

		/// <summary>
		/// Gets a value indicating whether to allow a talent registration to update company information.
		/// </summary>
		public bool AllowTalentCompanyInformationUpdate
		{
			get { return false; }
		}


		public bool TalentModulePresent
		{
			get { return true; }
		}

		/// <summary>
		/// Gets the resume upload allowed file reg ex pattern.
		/// </summary>
		public string ResumeUploadAllowedFileRegExPattern
		{
			get { return @"(.*)(\.)(([d|D][o|O][c|C])|([p|P][d|D][f|F])|([t|T][x|X][t|T])|([o|O][d|D][f|F])|([r|R][t|T][f|F])|([o|O][d|D][t|T])|([d|D][o|O][c|C][x|X])|([c|C][l|L][n|N])|([h|H][t|T][m|M])|([h|H][t|T][m|M][l|L]))$"; }
		}

		/// <summary>
		/// Gets the size of the resume upload maximum allowed file.
		/// </summary>
		public int ResumeUploadMaximumAllowedFileSize
		{
			get { return 5; }
		}

		/// <summary>
		/// Gets the talent pool search maximum records to return.
		/// </summary>
		public int TalentPoolSearchMaximumRecordsToReturn
		{
			get { return 100; }
		}
		
		/// <summary>
		/// Gets the star rating minimum scores.
		/// </summary>
		public int[] StarRatingMinimumScores
		{
			get { return new [] {0, 200, 400, 550, 700, 850}; }
		}

		/// <summary>
		/// Gets the focus career URL.
		/// </summary>
		public string FocusCareerUrl
		{
			get { return "http://www.focuscareer.com/"; }
		}

		/// <summary>
		/// Gets the page state persister.
		/// </summary>
		public PageStatePersisters PageStatePersister
		{
			get { return PageStatePersisters.Page; }
		}

		/// <summary>
		/// Gets the length of the page state queue max.
		/// </summary>
		public int PageStateQueueMaxLength
		{
			get { return 10; }
		}

		/// <summary>
		/// Gets the page state time out.
		/// </summary>
		public int PageStateTimeOut
		{
			get { return 2880; }
		}

		/// <summary>
		/// Gets the page state connection string.
		/// </summary>
		public string PageStateConnectionString
		{
			get { return ""; }
		}

		/// <summary>
		/// Gets the distance units.
		/// </summary>
		public DistanceUnits DistanceUnits
		{
			get { return DistanceUnits.Miles; }
		}

		/// <summary>
		/// Gets the default job expiry.
		/// </summary>
		public int DefaultJobExpiry
		{
			get { return 14; }
		}

		/// <summary>
		/// Gets the similar jobs count.
		/// </summary>
		public int SimilarJobsCount
		{
			get { return 20; }
		}

		/// <summary>
		/// Gets the session timeout.
		/// </summary>
		/// <value>The session timeout.</value>
		public int SessionTimeout
		{
			get { return 24*60; }
		}

		/// <summary>
		/// Gets the employee search maximum records to return.
		/// </summary>
		/// <value>The employee search maximum records to return.</value>
		public int EmployeeSearchMaximumRecordsToReturn
		{
			get { return 100; }
		}

    /// <summary>
    /// Gets the job expiry countdown days.
    /// </summary>
    /// <value>The job expiry countdown days.</value>
		public int[] JobExpiryCountdownDays
		{
			get { return new int[] { 1, 8, 15 }; }
		}

		/// <summary>
		/// Gets the talent application path.
		/// </summary>
		/// <value>The talent application path.</value>
		public string TalentApplicationPath
		{
			get { return @"http://localhost:8859"; }
		}

		/// <summary>
		/// Gets the assist application path.
		/// </summary>
		/// <value>The assist application path.</value>
		public string AssistApplicationPath
		{
			get { return @"http://localhost:8859"; }
		}


    /// <summary>
    /// Gets the career application path.
    /// </summary>
    /// <value>
    /// The career application path.
    /// </value>
    public string CareerApplicationPath
    {
      get { return @"http://localhost:8859"; }
    }

		/// <summary>
		/// Gets a value indicating whether [show burning glass logo in talent].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [show burning glass logo in talent]; otherwise, <c>false</c>.
		/// </value>
		public bool ShowBurningGlassLogoInTalent
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether [show burning glass logo in assist].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [show burning glass logo in assist]; otherwise, <c>false</c>.
		/// </value>
		public bool ShowBurningGlassLogoInAssist
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether [show burning glass logo in explorer].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [show burning glass logo in explorer]; otherwise, <c>false</c>.
		/// </value>
		public bool ShowBurningGlassLogoInExplorer
		{
			get { return false; }
		}

    /// <summary>
    /// Gets a value indicating whether new employer requests require approval
    /// </summary>
    public NewEmployerApprovalsOptions NewEmployerApproval
    {
      get { return NewEmployerApprovalsOptions.ApprovalSystemAvailable; }
    }

		/// <summary>
		/// Gets a value indicating whether [all job postings require approval].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [all job postings require approval]; otherwise, <c>false</c>.
		/// </value>
		public bool AllJobPostingsRequireApproval
		{
			get { return false; }
		}

		/// <summary>
		/// Gets a value indicating whether [activation yellow word filtering].
		/// </summary>
		public bool ActivationYellowWordFiltering
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether [employer preferences overide system defaults].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [employer preferences overide system defaults]; otherwise, <c>false</c>.
		/// </value>
		public bool EmployerPreferencesOverideSystemDefaults
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the feedback email address.
		/// </summary>
		/// <value>The feedback email address.</value>
		public string FeedbackEmailAddress
		{
			get { return "ajones@burning-glass.com"; }
		}

		/// <summary>
		/// Gets the job posting stylesheet.
		/// </summary>
		/// <value>The job posting stylesheet.</value>
		public string JobPostingStylesheet
		{
			get { return "DefaultPosting.xslt"; }
		}

		/// <summary>
		/// Gets the resume stylesheet.
		/// </summary>
		/// <value>The resume stylesheet.</value>
		public string FullResumeStylesheet
		{
			get { return "DefaultFullResume.xslt"; }
		}

		/// <summary>
		/// Gets the partial resume stylesheet.
		/// </summary>
		/// <value>The partial resume stylesheet.</value>
		public string PartialResumeStylesheet
		{
			get { return "DefaultPartialResume.xslt"; }
		}

		/// <summary>
		/// Gets a value indicating whether [emails enabled].
		/// </summary>
		/// <value><c>true</c> if [emails enabled]; otherwise, <c>false</c>.</value>
		public bool EmailsEnabled
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the valid client tags.
		/// </summary>
		/// <value>The client tags.</value>
		public string[] ValidClientTags
		{
			get { return new [] { "ABC123", "EFG456" }; }
		}

    /// <summary>
    /// Gets the registration complete action.
    /// </summary>
    /// <value>The registration complete action.</value>
	  public RegistrationCompleteAction RegistrationCompleteAction
	  {
	    get { return RegistrationCompleteAction.RestrictedAccess; }
	  }

    /// <summary>
    /// Gets the registration route.
    /// </summary>
    /// <value>The registration route.</value>
    public RegistrationRoute RegistrationRoute
    {
      get { return RegistrationRoute.Default; }
    }

		/// <summary>
		/// Gets the dummy username format.
		/// </summary>
		/// <value>The dummy username format.</value>
		public string DummyUsernameFormat
		{
			get { return "{0}@ci.bgt.com"; }
		}

		/// <summary>
		/// Gets a value indicating whether [authenticate assist user via client].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [authenticate assist user via client]; otherwise, <c>false</c>.
		/// </value>
		public bool AuthenticateAssistUserViaClient
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether [client allows password reset].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [client allows password reset]; otherwise, <c>false</c>.
		/// </value>
		public bool ClientAllowsPasswordReset
		{
			get { return false; }
		}

	  public bool ShowStudentExternalId
	  {
      get { return false; }
	  }

	  /// <summary>
		/// Gets the talent light colour.
		/// </summary>
		/// <value>The talent light colour.</value>
		public string TalentLightColour
		{
			get { return "cccccc"; }
		}

		/// <summary>
		/// Gets the talent dark colour.
		/// </summary>
		/// <value>The talent dark colour.</value>
		public string TalentDarkColour
		{
			get { return "ececec"; }
		}

		/// <summary>
		/// Gets the assist light colour.
		/// </summary>
		/// <value>The assist light colour.</value>
		public string AssistLightColour
		{
			get { return "cccccc"; }
		}

		/// <summary>
		/// Gets the assist dark colour.
		/// </summary>
		/// <value>The assist dark colour.</value>
		public string AssistDarkColour
		{
			get { return "ececec"; }
		}

		/// <summary>
		/// Gets the default state key.
		/// </summary>
		/// <value>The default state key.</value>
	  public string DefaultStateKey
	  {
      get { return "State.KY"; }
	  }

		/// <summary>
		/// Gets a value indicating whether [job seeker referrals enabled].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [job seeker referrals enabled]; otherwise, <c>false</c>.
		/// </value>
		public bool JobSeekerReferralsEnabled
		{
			get { return true; }
		}

		/// <summary>
		/// Gets the name of the focus talent application.
		/// </summary>
		/// <value>The name of the focus talent application.</value>
		public string FocusTalentApplicationName
		{
			get { return "Focus/Talent"; }
		}

		/// <summary>
		/// Gets the name of the focus assist application.
		/// </summary>
		/// <value>The name of the focus assist application.</value>
		public string FocusAssistApplicationName
		{
			get { return "Focus/Assist"; }
		}

		/// <summary>
		/// Gets the name of the focus career application.
		/// </summary>
		/// <value>The name of the focus career application.</value>
		public string FocusCareerApplicationName
		{
			get { return "Focus/Career"; }
		}

		/// <summary>
		/// Gets the name of the focus reporting application.
		/// </summary>
		/// <value>The name of the focus reporting application.</value>
		public string FocusReportingApplicationName
		{
			get { return "Focus/Reporting"; }
		}

		/// <summary>
		/// Gets the name of the focus explorer application.
		/// </summary>
		/// <value>The name of the focus explorer application.</value>
		public string FocusExplorerApplicationName
		{
			get { return "Focus/Explorer"; }
		}

		/// <summary>
		/// Gets the support email.
		/// </summary>
		/// <value>The support email.</value>
		public string SupportEmail
		{
			get { return "support@burning-glass.com"; }
		}

		/// <summary>
		/// Gets the support phone.
		/// </summary>
		/// <value>The support phone.</value>
		public string SupportPhone
		{
			get { return "+1 (617) 227 4875"; }
		}

		/// <summary>
		/// Gets the support hours of business.
		/// </summary>
		public string SupportHoursOfBusiness
		{
			get { return "Business hours are 8:00-4:30 EST."; }
		}

		/// <summary>
		/// Gets the support help team.
		/// </summary>
		/// <value>The support help team.</value>
		public string SupportHelpTeam
		{
			get { return "Support Help Team"; }
		}

		/// <summary>
		/// Gets the postal code reg ex pattern.
		/// </summary>
		public string PostalCodeRegExPattern
		{
			get { return @"(\d{5,})"; }
		}

		/// <summary>
		/// Gets the postal code mask pattern.
		/// </summary>
		public string PostalCodeMaskPattern
		{
			get { return "99999"; }
		}

    /// <summary>
    /// Sets whether all UI actions that affect logging in, changing passwords, managing the users account should be hidden in Assist
    /// </summary>
    public bool DisableAssistAuthentication
    {
      get { return false; }
    }

    /// <summary>
    /// Sets whether all UI actions that affect logging in, changing passwords, managing the users account should be hidden in Talent
    /// </summary>
    public bool DisableTalentAuthentication
    {
      get { return false; }
    }

    /// <summary>
    /// Gets a value indicating whether [reporting enabled].
    /// </summary>
    /// <value><c>true</c> if [reporting enabled]; otherwise, <c>false</c>.</value>
	  public bool ReportingEnabled
	  {
      get { return true; }
	  }

    public int ReportActionDaysLimit
    {
      get { return 120; }
    }

	  public bool ExplorerOnlyShowJobsBasedOnClientDegrees
	  {
	    get { return false; }
	  }

	  public List<LensService> LensServices
		{
			get
			{
				return new List<LensService>
				       	{
				       		new LensService
				       			{
				       				Host = "BOSAPPSVR005.usdev.burninglass.com",
				       				PortNumber = 2003,
				       				Timeout = 600,
				       				Vendor = "careerjobs",
				       				CharacterSet = "iso-8859-1",
				       				ServiceType = LensServiceTypes.Posting,
				       				Description = "Career posting repository",
											ReadOnly = false,
				       				CustomFilters = new CustomFilterList
				       				                	{
				       				                		new CustomFilter {Key = Constants.CustomFilterKeys.Status, Tag = "cf001"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.OpenDate, Tag = "cf002"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.ClosingOn, Tag = "cf003"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.MinimumSalary, Tag = "cf004"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.MaximumSalary, Tag = "cf005"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.ExternalPostingId, Tag = "cf007"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Occupation, Tag = "cf008"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.StateCode, Tag = "cf009"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.PostingId, Tag = "cf010"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Origin, Tag = "cf012"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Sic2, Tag = "cf013"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Industry, Tag = "cf014"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.EducationLevel, Tag = "cf016"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.GreenJob, Tag = "cf022"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Sector, Tag = "cf024"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.CountyCode, Tag = "cf025"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Internship, Tag = "cf026"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.ProgramsOfStudy, Tag = "cf027"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.AnnualPay, Tag = "cf028"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Salary, Tag = "cf029"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Msa, Tag = "cf030" },
																					new CustomFilter { Key = Constants.CustomFilterKeys.ROnet, Tag = "cf031" },
																					new CustomFilter { Key = Constants.CustomFilterKeys.UnpaidPosition, Tag = "cf033" },
																					new CustomFilter { Key = Constants.CustomFilterKeys.InternshipCategories, Tag = "cf034"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.RequiredProgramOfStudyId, Tag = "cf036"},
				       				                	}
				       			},
				       		new LensService
				       			{
				       				Host = "BOSAPPSVR005.usdev.burninglass.com",
				       				PortNumber = 2002,
				       				Timeout = 600,
				       				Vendor = "careermartha",
				       				CharacterSet = "iso-8859-1",
				       				ServiceType = LensServiceTypes.Resume,
				       				Description = "Talent/Assist resume repository",
											ReadOnly = false,
				       				CustomFilters = new CustomFilterList
				       				                	{
				       				                		new CustomFilter {Key = Constants.CustomFilterKeys.Status, Tag = "cf001"},
																					new CustomFilter {Key = Constants.CustomFilterKeys.RegisteredOn, Tag = "cf002"},
																					new CustomFilter {Key = Constants.CustomFilterKeys.ModifiedOn, Tag = "cf003"},
																					new CustomFilter {Key = Constants.CustomFilterKeys.DateOfBirth, Tag = "cf004"},
																					new CustomFilter {Key = Constants.CustomFilterKeys.CitizenFlag, Tag = "cf005"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.VeteranStatus, Tag = "cf007"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Languages, Tag = "cf008"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.DrivingLicenceClass, Tag = "cf009"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.DrivingLicenceEndorsement, Tag = "cf010"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Certificates, Tag = "cf011"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.WorkOvertime, Tag = "cf012"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.ShiftType, Tag = "cf013"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.WorkType, Tag = "cf014"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.WorkWeek, Tag = "cf015"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.EducationLevel, Tag = "cf016"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.JobseekerId, Tag = "cf017"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.Relocation, Tag = "cf019"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.JobTypes, Tag = "cf020"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.GPA, Tag = "cf021"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.MonthsExperience, Tag = "cf022"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.EnrollmentStatus, Tag = "cf023"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.ProgramsOfStudy, Tag = "cf024"},
																					new CustomFilter { Key = Constants.CustomFilterKeys.DegreeCompletionDate, Tag = "cf025"},
																				}
				       			},
									new LensService
				       			{
				       				Host = "BOSAPPSVR005.usdev.burninglass.com",
				       				PortNumber = 2002,
				       				Timeout = 600,
				       				Vendor = "career",
				       				CharacterSet = "iso-8859-1",
				       				ServiceType = LensServiceTypes.Generic,
				       				Description = "Generic Lens",
											ReadOnly = true
				       			},
									new LensService
				       			{
				       				Host = "BOSAPPSVR006.usdev.burninglass.com",
				       				PortNumber = 3102,
				       				Timeout = 600,
				       				Vendor = "career",
				       				CharacterSet = "iso-8859-1",
				       				ServiceType = LensServiceTypes.JobMine,
				       				Description = "Job Mine",
											ReadOnly = true
				       			}
				       	};
			}
		}

    /// <summary>
    /// Gets the DEX settings.
    /// </summary>
    public DEXSettings DexSettings
    {
      get
      {
        return new DEXSettings
        {
          Host = "BOSAPPSVR006.usdev.burninglass.com",
          PortNumber = 3102,
          Timeout =600,
          CharacterSet = Encoding.GetEncoding("iso-8859-1"),

        };
      }
    }

		/// <summary>
		/// Gets the user session timeout.
		/// </summary>
		public int UserSessionTimeout
		{
			get { return 60; }
		}

		/// <summary>
		/// Gets the maximum number of days can extend job by.
		/// </summary>
		public int MaximumNumberOfDaysCanExtendJobBy
		{
			get { return 45; }
		}

		/// <summary>
		/// Gets the maximum salary.
		/// </summary>
		public int MaximumSalary
		{
			get { return 350000; }
		}

    /// <summary>
    /// Gets the maximum saved searches.
    /// </summary>
    public int MaximumSavedSearches
    {
      get { return 20; }
    }

		/// <summary>
		/// Gets a value indicating whether [career enabled].
		/// </summary>
		/// <value><c>true</c> if [career enabled]; otherwise, <c>false</c>.</value>
		public bool CareerEnabled
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the explorer resume upload allowed file reg ex pattern.
		/// </summary>
		/// <value>The explorer resume upload allowed file reg ex pattern.</value>
		public string ExplorerResumeUploadAllowedFileRegExPattern
		{
			get { return  @"(.*)(\.)(([d|D][o|O][c|C])|([p|P][d|D][f|F])|([r|R][t|T][f|F])|([d|D][o|O][c|C][x|X]))$"; }
		}

		/// <summary>
		/// Gets the maximum size of the explorer resume upload allowed file.
		/// </summary>
		/// <value>The maximum size of the explorer resume upload allowed file.</value>
		public int ExplorerResumeUploadMaximumAllowedFileSize
		{
			get { return 5; }
		}

		/// <summary>
		/// Gets the explorer application path.
		/// </summary>
		/// <value>The explorer application path.</value>
		public string ExplorerApplicationPath
		{
			get { return @"http://explorerdev.burning-glass.com"; }
		}

		/// <summary>
		/// Gets a value indicating whether [bookmark shown for not logged in user].
		/// </summary>
		/// <value><c>true</c> if [bookmark shown for not logged in user]; otherwise, <c>false</c>.</value>
		public bool ExplorerShowBookmarkForNonLoggedInUser
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether [send email shown for not logged in user].
		/// </summary>
		/// <value><c>true</c> if [send email shown for not logged in user]; otherwise, <c>false</c>.</value>
		public bool ExplorerShowEmailForNonLoggedInUser
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether [resume upload shown for not logged in user].
		/// </summary>
		/// <value><c>true</c> if [resume upload shown for not logged in user]; otherwise, <c>false</c>.</value>
		public bool ExplorerShowResumeUploadForNonLoggedInUser
		{
			get { return true; }
		}

		/// <summary>
		/// Gets the explorer section order.
		/// </summary>
		/// <value>The explorer section order.</value>
		public string ExplorerSectionOrder
		{
			get { return "Explore|Research|Study"; }
		}

    /// <summary>
    /// Gets the order of the In-Demand dropdown
    /// </summary>
    public string ExplorerInDemandDropdownOrder
    {
      get { return "Job|Employer|DegreeCertification|Skill|Internship|CareerMoves"; }
    }

		// Added 2 unimplemented members [CK]
		public bool ExplorerModalEmailIcon
		{
			get { return true; }
		}

		public bool ExplorerModalDownloadIcon
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the explorer send email maximum message length.
		/// </summary>
		/// <value>The explorer send email maximum message length.</value>
		public int ExplorerSendEmailMaximumMessageLength
		{
			get { return 1000; }
		}

		/// <summary>
		/// Gets a value indicating whether [email from shown in send email].
		/// </summary>
		/// <value><c>true</c> if [email from shown in send email]; otherwise, <c>false</c>.</value>
		public bool ExplorerShowEmailFromInSendEmail
		{
			get { return true; }
		}

		/// <summary>
		/// Gets the explorer send email from email.
		/// </summary>
		/// <value>The explorer send email from email.</value>
		public string ExplorerSendEmailFromEmail
		{
			get { return String.Empty; }
		}

		/// <summary>
		/// Gets a value indicating whether [personalization by resume is allowed].
		/// </summary>
		/// <value><c>true</c> if [personalization by resume is allowed]; otherwise, <c>false</c>.</value>
		public bool ExplorerAllowPersonalizationByResume
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the explorer job salary nationwide display type.
		/// </summary>
		/// <value>The explorer job salary nationwide display type.</value>
		public string ExplorerJobSalaryNationwideDisplayType
		{
			get { return "Average"; }
		}

    /// <summary>
    /// Gets the percentage to compare salaries.
    /// </summary>
    /// <value>The explorer job salary nationwide display type.</value>
    public int ExplorerJobSalaryComparisonPercent
    {
      get { return 1; }
    }

		/// <summary>
		/// Gets the explorer registration notification emails.
		/// </summary>
		/// <value>The explorer registration notification emails.</value>
		public string ExplorerRegistrationNotificationEmails
		{
			get { return String.Empty; }
		}

		/// <summary>
		/// Gets the explorer degree client data tag.
		/// </summary>
		/// <value>The explorer degree client data tag.</value>
		public string ExplorerDegreeClientDataTag
		{
			get { return String.Empty; }
		}

    /// <summary>
    /// Gets the explorer degree filtering type.
    /// </summary>
    /// <value>The explorer degree filtering type.</value>
    public DegreeFilteringType ExplorerDegreeFilteringType
    {
      get { return DegreeFilteringType.Ours; }
    }

		/// <summary>
		/// Gets a value indicating whether [explorer use degree tiering].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [explorer use degree tiering]; otherwise, <c>false</c>.
		/// </value>
    public bool ExplorerUseDegreeTiering
    {
      get { return false; }
    }

		/// <summary>
		/// Gets a value indicating whether [degree study places are shown].
		/// </summary>
		/// <value><c>true</c> if [degree study places are shown]; otherwise, <c>false</c>.</value>
		public bool ExplorerShowDegreeStudyPlaces
		{
			get { return true; }
		}

		/// <summary>
		/// Gets the explorer google tracking id.
		/// </summary>
		/// <value>The explorer google tracking id.</value>
		public string ExplorerGoogleTrackingId
		{
			get { return String.Empty; }
		}

		/// <summary>
		/// Gets a value indicating whether [google event tracking is enabled].
		/// </summary>
		/// <value><c>true</c> if [google event tracking is enabled]; otherwise, <c>false</c>.</value>
		public bool ExplorerGoogleEventTrackingEnabled
		{
			get { return false; }
		}

    /// <summary>
    /// 
    /// </summary>
    /// <value>Confidential employers enabled</value>
    public bool ConfidentialEmployersEnabled
    {
      get { return true; }
    }

		/// <summary>
		/// Gets the explorer help email
		/// </summary>
		/// <value>The explorer help email.</value>
		public string ExplorerHelpEmail
		{
			get { return "test@test.com"; }
		}

		// Career

		/// <summary>
		/// Gets the demo shield customer code.
		/// </summary>
		public string DemoShieldCustomerCode
		{
			get { return String.Empty; }
		}

		/// <summary>
		/// Gets the demo shield redirect url.
		/// </summary>
		public string DemoShieldRedirectUrl
		{
			get { return String.Empty; }
		}

		/// <summary>
		/// Gets the name of the ping DOM user.
		/// </summary>
		/// <value>The name of the ping DOM user.</value>
		public string PingDomUserName
		{
			get { return string.Empty; }
		}

		/// <summary>
		/// Gets the ping DOM password.
		/// </summary>
		/// <value>The ping DOM password.</value>
		public string PingDomPassword
		{
			get { return string.Empty; }
		}

		/// <summary>
		/// Gets the maximum allowed resume count.
		/// </summary>
		/// <value>The maximum allowed resume count.</value>
		public int MaximumAllowedResumeCount
		{
			get { return Defaults.ConfigurationItemDefaults.MaximumAllowedResumeCount; }
		}

		/// <summary>
		/// Gets the password limit reg ex pattern.
		/// </summary>
		/// <value>The password limit reg ex pattern.</value>
		public string PasswordLimitRegExPattern
		{
			get { return @"^[a-zA-Z0-9-_@!$*+#.&%^]{6,20}$"; }
		}

		/// <summary>
		/// Gets the default country key.
		/// </summary>
		/// <value>The default country key.</value>
		public string DefaultCountryKey
		{
			get { return Defaults.ConfigurationItemDefaults.DefaultCountryKey; }
		}

		/// <summary>
		/// Gets the passive authentication code.
		/// </summary>
		/// <value>The passive authentication code.</value>
		public string PassiveAuthenticationCode
		{
			get { return string.Empty; }
		}

		/// <summary>
		/// Gets the masking address.
		/// </summary>
		/// <value>The masking address.</value>
		public string MaskingAddress
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the SMTP server.
		/// </summary>
		/// <value>The SMTP server.</value>
		public string SMTPServer
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the SMTP port.
		/// </summary>
		/// <value>The SMTP port.</value>
		public int SMTPPort
		{
			get { return 25; }
		}

		/// <summary>
		/// Gets the SMTP username.
		/// </summary>
		/// <value>The SMTP username.</value>
		public string SMTPUsername
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the SMTP password.
		/// </summary>
		/// <value>The SMTP password.</value>
		public string SMTPPassword
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the minimum score for clarify.
		/// </summary>
		/// <value>The minimum score for clarify.</value>
		public int MinimumScoreForClarify
		{
			get { return Defaults.ConfigurationItemDefaults.MinimumScoreForClarify; }
		}

		/// <summary>
		/// Gets the maximum search count for clarify.
		/// </summary>
		/// <value>The maximum search count for clarify.</value>
		public int MaximumSearchCountForClarify
		{
			get { return Defaults.ConfigurationItemDefaults.MaximumSearchCountForClarify; }
		}

		/// <summary>
		/// Gets the minimum score for search.
		/// </summary>
		/// <value>The minimum score for search.</value>
		public int MinimumScoreForSearch
		{
			get { return Defaults.ConfigurationItemDefaults.MinimumSearchScore; }
		}

		/// <summary>
		/// Gets the maximum no document to return in search.
		/// </summary>
		/// <value>The maximum no document to return in search.</value>
		public int MaximumNoDocumentToReturnInSearch
		{
			get { return Defaults.ConfigurationItemDefaults.MaxiumSearchDocumentCount; }
		}

		/// <summary>
		/// Gets the maximum no document to return in job alert.
		/// </summary>
		/// <value>The maximum no document to return in job alert.</value>
		public int MaximumNoDocumentToReturnInJobAlert
		{
			get { return Defaults.ConfigurationItemDefaults.MaxiumSearchDocumentCount; }
		}

		/// <summary>
		/// Gets the spam job origin id.
		/// </summary>
		/// <value>The spam job origin id.</value>
		public string SpamJobOriginId
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the spam mail address.
		/// </summary>
		/// <value>The spam mail address.</value>
		public string SpamMailAddress
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the spam BCC address.
		/// </summary>
		/// <value>The spam BCC address.</value>
		public string SpamBccAddress
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the closed job origin id.
		/// </summary>
		/// <value>The closed job origin id.</value>
		public string ClosedJobOriginId
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the closed mail address.
		/// </summary>
		/// <value>The closed mail address.</value>
		public string ClosedMailAddress
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the closed BCC address.
		/// </summary>
		/// <value>The closed BCC address.</value>
		public string ClosedBccAddress
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the cache size for prefix domain.
		/// </summary>
		/// <value>The cache size for prefix domain.</value>
		public int CacheSizeForPrefixDomain
		{
			get { return Defaults.ConfigurationItemDefaults.CacheSizeForPrefixDomain; }
		}

		/// <summary>
		/// Gets the name of the brand.
		/// </summary>
		/// <value>The name of the brand.</value>
		public string BrandName
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the reset password format.
		/// </summary>
		/// <value>The reset password format.</value>
		public string ResetPasswordFormat
		{
			get { return null; }
		}

		/// <summary>
		/// Gets the customer.
		/// </summary>
		/// <value>The customer.</value>
		public string Customer
		{
			get { return Defaults.Customer; }
		}

		/// <summary>
		/// Gets the customer center code.
		/// </summary>
		/// <value>The customer center code.</value>
		public string CustomerCenterCode
		{
			get { return Defaults.CustomerCenterCode; }
		}
		
		/// <summary>
		/// Gets the customer representative id.
		/// </summary>
		/// <value>The customer representative id.</value>
		public string CustomerRepresentativeId
		{
			get { return Defaults.CustomerRepresentativeId; }
		}

		/// <summary>
		/// Gets the nation wide instance.
		/// </summary>
		/// 
		public bool NationWideInstance
		{
			get { return true; }
		}

		/// <summary>
		/// Gets the nearby state keys.
		/// </summary>
		/// <value>The nearby state keys.</value>
		public string[] NearbyStateKeys
		{
			get { return new[] { "State.IL", "State.IN", "State.KY", "State.MO", "State.OH", "State.TN", "State.VA", "State.WV" }; }
		}

		/// <summary>
		/// Gets the searchable state keys.
		/// </summary>
		/// <value>The searchable state keys.</value>
		public string[] SearchableStateKeys
		{
			get { return new[] { "State.IL", "State.IN", "State.KY", "State.MO", "State.OH", "State.TN", "State.VA", "State.WV" }; }
		}

    /// <summary>
    /// Gets the special state keys (States that may be oversees territories)
    /// </summary>
    /// <value>The nearby state keys.</value>
    public string[] SpecialStateKeys
    {
      get
      {
        return new[] { "State.AE", "State.AA", "State.AP", "State.AS", "State.FM", "State.MH", "State.MP", "State.ZZ", "State.PW", "State.GU", "State.PR", "State.VI" };
      }
    }

		/// <summary>
		/// Gets a value indicating whether [job requirements].
		/// </summary>
		/// <value><c>true</c> if [job requirements]; otherwise, <c>false</c>.</value>
		public bool JobRequirements
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the referral min stars.
		/// </summary>
		/// <value>The referral min stars.</value>
		public int ReferralMinStars
		{
			get { return -1; }
		}

		/// <summary>
		/// Gets the maximum recently viewed postings.
		/// </summary>
		/// <value>The maximum recently viewed postings.</value>
		public int MaximumRecentlyViewedPostings
		{
			get { return 5; }
		}

		#region Job Development

		/// <summary>
		/// Gets the job development score threshold.
		/// </summary>
		/// <value>The job development score threshold.</value>
		public int JobDevelopmentScoreThreshold
		{
			get { return 400; }
		}

		/// <summary>
		/// Gets the minimum stars for the spidered jobs search on the Assist job development pages.
		/// </summary>
		/// <value>The minimum stars.</value>
		public int JobDevelopmentSpideredJobSearchMatchesMinimumStars
		{
			get { return 1; }
		}

		/// <summary>
		/// Gets the number of postings required for an employer to appear on the job development spidered job list.
		/// </summary>
		/// <value>
		/// The number of postings required.
		/// </value>
		public int JobDevelopmentSpideredJobListNumberOfPostingsRequired
		{
			get { return 2; }
		}

		/// <summary>
		/// Gets the maximum number of employers to diaplay on the job development spidered job list.
		/// </summary>
		/// <value>
		/// The  maximum number of employers.
		/// </value>
		public int JobDevelopmentSpideredJobListMaximumNumberOfEmployers
		{
			get { return 30; }
		}

		/// <summary>
		/// Gets the number of postings to display on thejob development spidered job list.
		/// </summary>
		/// <value>The number of postings to display.</value>
		public int JobDevelopmentSpideredJobListPostingsToDisplay
		{
			get { return 20; }
		}

		/// <summary>
		/// Gets the maximum posting age to display on the job development spidered job list.
		/// </summary>
		/// <value>The posting age.</value>
		public int? JobDevelopmentSpideredJobListPostingAge
		{
			get { return 7; }
		}

		/// <summary>
		/// Gets the job development employer placements no of days.
		/// </summary>
		/// <value>The job development employer placements no of days.</value>
		public int JobDevelopmentEmployerPlacementsNoOfDays
		{
			get { return 7; }
		}

		#endregion

		#region Live Jobs Repository Settings

		/// <summary>
		/// Gets the live jobs API root URL.
		/// </summary>
		/// <value>The live jobs API root URL.</value>
		public string LiveJobsApiRootUrl
		{
			get { return "http://10.0.1.223/LiveJobsApi_v110"; }
		}

		/// <summary>
		/// Gets the live jobs API key.
		/// </summary>
		/// <value>The live jobs API key.</value>
		public string LiveJobsApiKey
		{
			get { return "BGT"; }
		}

		/// <summary>
		/// Gets the live jobs API secret.
		/// </summary>
		/// <value>The live jobs API secret.</value>
		public string LiveJobsApiSecret
		{
			get { return "086718ED10AC48B685F93535208AE903"; }
		}

		/// <summary>
		/// Gets the live jobs API token.
		/// </summary>
		/// <value>The live jobs API token.</value>
		public string LiveJobsApiToken
		{
			get { return "Core"; }
		}

		/// <summary>
		/// Gets the live jobs API token secret.
		/// </summary>
		/// <value>The live jobs API token secret.</value>
		public string LiveJobsApiTokenSecret
		{
			get { return "3250A588F0FD11E1A0EA0CF26088709B"; }
		}

		#endregion

		/// <summary>
		/// Gets the recommended matches minimum stars.
		/// </summary>
		/// <value>The recommended matches minimum stars.</value>
		public int RecommendedMatchesMinimumStars
		{
			get { return 0; }
		}

		/// <summary>
		/// Gets the resume referral approvals minimum stars.
		/// </summary>
		/// <value>The resume referral approvals minimum stars.</value>
		public int ResumeReferralApprovalsMinimumStars
		{
			get { return 3; }
		}

		/// <summary>
		/// Gets a value indicating whether [show license requirement].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [show license requirement]; otherwise, <c>false</c>.
		/// </value>
		public bool ShowLicenseRequirement
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether [filter skills in automated summary].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [filter skills in automated summary]; otherwise, <c>false</c>.
		/// </value>
		public bool FilterSkillsInAutomatedSummary
		{
			get { return true; }
		}

		/// <summary>
		/// Gets a value indicating whether [remove spaces from automated summary].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [remove spaces from automated summary]; otherwise, <c>false</c>.
		/// </value>
		public bool RemoveSpacesFromAutomatedSummary
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the non customer system origin ids.
		/// </summary>
		/// <value>The non customer system origin ids.</value>
		public int[] NonCustomerSystemOriginIds
		{
			get { return new[] { 999 }; }
		}

		/// <summary>
		/// Gets the review application eligibility origin ids.
		/// </summary>
		/// <value>The review application eligibility origin ids.</value>
		public int[] ReviewApplicationEligibilityOriginIds
		{
			get { return new[] { 7, 8, 9 }; }
		}

    /// <summary>
    /// Gets a value indicating whether to check for criminal record exclusion.
    /// </summary>
    public bool CheckCriminalRecordExclusion
    {
      get { return false; }
    }

		/// <summary>
		/// Gets a value indicating whether [offices enabled].
		/// </summary>
		/// <value><c>true</c> if [offices enabled]; otherwise, <c>false</c>.</value>
		public bool OfficesEnabled
		{
			get { return Theme == FocusThemes.Workforce; }
		}

    /// <summary>
    /// Provider used to get the migration data
    /// </summary>
    public string MigrationProvider
    {
      get { return "fake"; }
    }

    /// <summary>
    /// Number of records for the migration service to read at a time
    /// </summary>
    public int MigrationBatchSize
    {
      get { return 10; }
    }

    /// <summary>
    /// Number of records for the import service to read at a time
    /// </summary>
    public int ImportBatchSize
    {
      get { return 10; }
    }

		/// <summary>
		/// Gets the name of the school.
		/// </summary>
		/// <value>The name of the school.</value>
		public string SchoolName
		{
			get { return "Dummy School"; }
		}

    /// <summary>
    /// Gets the type of the school.
    /// </summary>
    /// <value>
    /// The type of the school.
    /// </value>
	  public SchoolTypes SchoolType
	  {
	    get { return SchoolTypes.None; }
	  }

		/// <summary>
		/// Gets the document store service URL.
		/// </summary>
		/// <value>The document store service URL.</value>
		public string DocumentStoreServiceUrl
		{
			get { return "http://careerdocstoresvcstage.usdev.burninglass.com/DocStoreSvc.asmx"; }
		}

	  public CareerExplorerFeatureEmphasis CareerExplorerFeatureEmphasis { get {return CareerExplorerFeatureEmphasis.Career;} }

	  public bool ShowProgramArea
	  {
      get { return true; }
	  }

    public bool ShowCampus
    {
      get { return false; }
    }

    public bool SupportsProspectiveStudents
    {
      get { return true; }
    }

    public bool SplitJobsByLocation
    {
      get { return false; }
    }

	  #region Special Configuration Items

		/// <summary>
		/// Gets the custom XSL for resume.
		/// </summary>
		/// <param name="resumeFormat">The resume format.</param>
		/// <returns></returns>
		public string GetCustomXslForResume(string resumeFormat)
		{
			return Defaults.ResumeDefaults.ResumeDisplayStylePath;
		}

		public string JobPostingStylePath
		{
			get { return Defaults.JobPostingDefaults.JobPostingStylePath; }
		}

    /// <summary>
    /// Gets the recent placement timeframe.
    /// </summary>
    /// <returns></returns>
    public int RecentPlacementTimeframe
    {
      get { return 90; }
    }

		#endregion


		#region Assist Job Issues

		// Assist job order issue settings

		/// <summary>
		/// Gets the job minimum match count threshold.
		/// </summary>
		/// 
		public int JobIssueMinimumMatchCountThreshold
		{
			get { return 5; }
		}

		/// <summary>
		/// Gets the job minimum stars threshold (used for minimum match count).
		/// </summary>
		/// 
		public int JobIssueMatchMinimumStarsThreshold
		{
			get { return 4; }
		}

		/// <summary>
		/// Gets the job match days threshold (used for minimum match count).
		/// </summary>
		/// 
		public int JobIssueMatchDaysThreshold
		{
			get { return 5; }
		}

		/// <summary>
		/// Gets the job minimum referrals count.
		/// </summary>
		/// 
		public int JobIssueMinimumReferralsCountThreshold
		{
			get { return 5; }
		}

		/// <summary>
		/// Gets the days threshold for the job referral count.
		/// </summary>
		/// 
		public int JobIssueReferralCountDaysThreshold
		{
			get { return 10; }
		}

		/// <summary>
		/// Gets the job days threshold for not clicking referrals.
		/// </summary>
		/// 
		public int JobIssueNotClickingReferralsDaysThreshold
		{
			get { return 3; }
		}

		/// <summary>
		/// Gets the job days threshold for not clicking referrals.
		/// </summary>
		/// 
		public int JobIssueSearchesWithoutInviteThreshold
		{
			get { return 3; }
		}

    /// <summary>
    /// Gets the job issue searches without invite days threshold.
    /// </summary>
	  public int JobIssueSearchesWithoutInviteDaysThreshold
	  {
	    get { return 5; }
	  }

	  /// <summary>
		/// Gets the job days threshold for not clicking referrals.
		/// </summary>
		/// 
		public int JobIssuePostClosedEarlyDaysThreshold
		{
			get { return 3; }
		}

		/// <summary>
		/// Gets the job days threshold for not clicking referrals.
		/// </summary>
		/// 
		public int JobIssueHiredDaysElapsedThreshold
		{
			get { return 14; }
		}

		// Assist job order issue switches

		/// <summary>
		/// Gets whether a low number of minimum star matches is flagged.
		/// </summary>
		/// 
		public bool JobIssueFlagLowMinimumStarMatches
		{
			get { return true; }
		}

		/// <summary>
		/// Gets whether low number of referrals is flagged.
		/// </summary>
		/// 
		public bool JobIssueFlagLowReferrals
		{
			get { return true; }
		}

		/// <summary>
		/// Gets whether a contact not clicking referrals is flagged.
		/// </summary>
		/// 
		public bool JobIssueFlagNotClickingReferrals
		{
			get { return true; }
		}

		/// <summary>
		/// Gets whether a contact searching but not inviting candidates is flagged.
		/// </summary>
		/// 
		public bool JobIssueFlagSearchingNotInviting
		{
			get { return true; }
		}

		/// <summary>
		/// Gets whether refreshing job closing date is flagged.
		/// </summary>
		///
		public bool JobIssueFlagClosingDateRefreshed
		{
			get { return true; }
		}

		/// <summary>
		/// Gets whether closing a job before it's closing date is flagged.
		/// </summary>
		///
		public bool JobIssueFlagJobClosedEarly
		{
			get { return true; }
		}

		/// <summary>
		/// Gets whether a job is flagged a certain period of time after hiring.
		/// </summary>
		///
		public bool JobIssueFlagJobAfterHiring
		{
			get { return true; }
		}

		/// <summary>
		/// Gets whether a contact providing negative feedback to customer service questions is flagged.
		/// </summary>
		///
		public bool JobIssueFlagNegativeFeedback
		{
			get { return true; }
		}

		/// <summary>
		/// Gets whether a contact providing positive feedback to customer service questions is flagged.
		/// </summary>
		/// 
		public bool JobIssueFlagPositiveFeedback
		{
			get { return true; }
		}

		#endregion


		public FocusModules CareerExplorerModule
		{
			get { return FocusModules.CareerExplorer; }
		}

		public string CareerRegistrationNotificationEmails
		{
			get { return ""; }
		}

		public bool NotLoggingInEnabled
		{
			get { return true; }
		}

		public bool NotClickingLeadsEnabled
		{
			get { return true; }
		}

		public bool RejectingJobOffersEnabled
		{
			get { return true; }
		}

		public bool NotReportingForInterviewEnabled
		{
			get { return true; }
		}

		public bool NotRespondingToEmployerInvitesEnabled
		{
			get { return true; }
		}

		public bool NotRespondingToReferralSuggestionsEnabled
		{
			get { return true; }
		}

		public bool SelfReferringToUnqualifiedJobsEnabled
		{
			get { return true; }
		}

		public bool ShowingLowQualityMatchesEnabled
		{
			get { return true; }
		}

		public bool PostingLowQualityResumeEnabled
		{
			get { return true; }
		}

		public bool PostHireFollowUpEnabled
		{
			get { return true; }
		}

		public bool FollowUpEnabled
		{
			get { return true; }
		}

		public bool NotSearchingJobsEnabled
		{
			get { return true; }
		}

    public bool InappropriateEmailAddressCheckEnabled
    {
      get { return false; }
    }

    public int NotLoggingInDaysThreshold
		{
			get { return 2; }
		}

		public int NotClickingLeadsCountThreshold
		{
			get { return 2; }
		}

		public int NotClickingLeadsDaysThreshold
		{
			get { return 14; }
		}

		public int RejectingJobOffersCountThreshold
		{
			get { return 2; }
		}

		public int RejectingJobOffersDaysThreshold
		{
			get { return 14; }
		}

		public int NotReportingForInterviewCountThreshold
		{
			get { return 2; }
		}

		public int NotReportingForInterviewDaysThreshold
		{
			get { return 14; }
		}

		public int NotRespondingToEmployerInvitesCountThreshold
		{
			get { return 2; }
		}

		public int NotRespondingToEmployerInvitesDaysThreshold
		{
			get { return 14; }
		}

		public int NotRespondingToEmployerInvitesGracePeriodDays
		{
			get { return 2; }
		}

		public int NotRespondingToReferralSuggestionsCountThreshold
		{
			get { return 2; }
		}

		public int NotRespondingToReferralSuggestionsDaysThreshold
		{
			get { return 14; }
		}

		public int SelfReferringToUnqualifiedJobsCountThreshold
		{
			get { return 2; }
		}

		public int SelfReferringToUnqualifiedJobsDaysThreshold
		{
			get { return 14; }
		}

		public int SelfReferringToUnqualifiedMinStarThreshold
		{
			get { return 3; }
		}

		public int SelfReferringToUnqualifiedMaxStarThreshold
		{
			get { return 2; }
		}

		public int ShowingLowQualityMatchesCountThreshold
		{
			get { return 2; }
		}

		public int ShowingLowQualityMatchesDaysThreshold
		{
			get { return 14; }
		}

		public int ShowingLowQualityMatchesMinStarThreshold
		{
			get { return 2; }
		}

		public int PostingLowQualityResumeWordCountThreshold
		{
			get { return 14; }
		}

		public int PostingLowQualityResumeSkillCountThreshold
		{
			get { return 2; }
		}

		public int NotSearchingJobsDaysThreshold
		{
			get { return 7; }
		}

		public int NotSearchingJobsCountThreshold
		{
			get { return 2; }
		}

    #region Career Search Defaults

    public bool CareerSearchShowAllJobs
    {
      get { return true; }
    }

    public int CareerSearchMinimumStarMatchScore
    {
      get { return 3; }
    }

    public int CareerSearchDefaultRadiusId
    {
      get { return 0; }
    }

    public SearchCentrePointOptions CareerSearchCentrePointType
    {
      get { return SearchCentrePointOptions.Zipcode; }
    }

    public long CareerSearchRadius
    {
      get { return -1; }
    }

    public string CareerSearchZipcode
    {
      get { return string.Empty; }
    }

    public ResumeSearchableOptions CareerSearchResumesSearchable
    {
      get { return ResumeSearchableOptions.JobSeekersOptionYesByDefault; }
    }

    public bool VeteranPriorityServiceEnabled
    {
      get { return false; }
    }

    public int VeteranPriorityStarMatching
    {
      get { return 3; }
    }

    #endregion

    #region Alert Defaults

    public JobAlertsOptions JobAlertsOption
    {
      get { return JobAlertsOptions.UserConfiguration; }
    }

    public AlertFrequencies JobAlertFrequency
    {
      get { return AlertFrequencies.Weekly; }
    }

		public int JobAlertTruncateWords
		{
			get { return 50; }
		}

		public bool SendOtherJobsEmail
    {
      get { return false; }
    }

    public AlertFrequencies StaffEmailAlerts
    {
      get { return AlertFrequencies.Never; }
    }

    #endregion

    #region Labor Insight Access

    public bool LaborInsightAccess
    {
      get { return true; }
    }

    public string LaborInsightLinkUrl
    {
      get { return "http://www.burning-glass.com/products/labor.html"; }
    }

    #endregion

    #region Sharing Defaults

    public bool SharingJobsForSameFEIN
    {
      get { return true; }
    }

    #endregion

    #region Job Seeker Inactivity Email Defaults

    public bool JobSeekerSendInactivityEmail
    {
      get { return false; }
    }

    public int JobSeekerInactivityEmailLimit
    {
      get { return 60; }
    }

    public int JobSeekerInactivityEmailGrace
    {
      get { return 14; }
    }

    #endregion

		public bool SendHiringFromTaxCreditProgramNotification
		{
			get { throw new NotImplementedException(); }
		}
		
		public bool ShowBurningGlassLogoInCareerExplorer
		{
			get { throw new NotImplementedException(); }
		}

		public string CareerExplorerLightColour
		{
			get { throw new NotImplementedException(); }
		}

		public string CareerExplorerDarkColour
		{
			get { throw new NotImplementedException(); }
		}

		public string MessageBusUsername
		{
			get { return "IntegrationUser"; }
		}

		public string PhoneNumberRegExPattern
		{
			get { return @"^(([0-9]+|(\([0-9]+\)))((-| )(?!$))?)*$"; } 
		}

		public string PhoneNumberMaskPattern
		{
			get { return "(999) 999-9999"; }
		}

		public string NonUsPhoneNumberMaskPattern
		{
			get { return "99999999999999999999"; }
		}

		public string PhoneNumberStrictRegExPattern { get { return @"(\d{3})(\d{3})(\d{4})"; } }

		public string PhoneNumberFormat { get { return "($1) $2-$3"; } }

		public List<BatchProcessSetting> BatchProcessSettings
		{
			get
			{
				return new List<BatchProcessSetting>
				       	{
				       		new BatchProcessSetting {Process = BatchProcesses.AboutToExpiredJobs, Identifier = "75965dec0c6f4cbba3eaedbbd2ff3262"},
									new BatchProcessSetting {Process = BatchProcesses.CandidateIssues, Identifier = "c1144ccbed7749359aaa964512a7e900"},
									new BatchProcessSetting {Process = BatchProcesses.EmailAlerts, Identifier = "97d8c47be2cb45deb7d93e08c2864702"},
									new BatchProcessSetting {Process = BatchProcesses.ExpiredJobs, Identifier = "35cbf2025ffb44899528a2adb31a198f"},
									new BatchProcessSetting {Process = BatchProcesses.Reminders, Identifier = "58481ee4f600419c9cf4960aa2309514"},
									new BatchProcessSetting {Process = BatchProcesses.SearchAlerts, Identifier = "6d4c26985c7245c096999cd42bab4677"}
				       	};
			}
		}

		// Assist Email from Overrides
		public bool OverideSentEmailFromAddressForAssistUser
		{
			get { return false; }
		}

		public AppSettings.ConfigSettingInfo[] GetConfigSettings()
		{
			return new AppSettings.ConfigSettingInfo[1];
		}

		public string DefaultDrivingLicenseType
		{
			get { return "DrivingLicenceClasses.ClassD"; }
		}

		public string LaborRegulationsUrl
		{
			get { return  null; }
		}

		public bool DisplaySurveyFrequency
		{
			get { return false; }
		}

		public bool NewEmployerCensorshipFiltering
		{
			get { return true; }
		}

		public bool CountyEnabled
		{
			get { return Theme == FocusThemes.Workforce; }
		}

		public bool HideSalary
		{
			get { return false; }
		}

		public bool MyAccountEnabled
		{
			get { return true; }
		}

		public bool DisplayResumeBuilderDuration
		{
			get { return false; }
		}

		public int SurveyFrequency
		{
			get { return 30; }
		}

		public string LogoutRedirectUrl
		{
			get { return null; }
		}

		public string DefaultSearchRadiusKey
		{
			get { return Defaults.ConfigurationItemDefaults.DefaultSearchRadiusKey; }
		}

		#region IAppSettings Members


		public string OAuthConsumerKey
		{
			get { return String.Empty; }
		}

		public string OAuthConsumerSecret
		{
			get { return String.Empty; }
		}

		public string OAuthRequestTokenUrl
		{
			get { return String.Empty; }
		}

		public string OAuthVerifierUrl
		{
			get { return String.Empty; }
		}

		public string OAuthRequestAccessTokenUrl
		{
			get { return String.Empty; }
		}

		public string OAuthRequestProfileUrl
		{
			get { return String.Empty; }
		}

		public string OAuthScope
		{
			get { return String.Empty; }
		}

		public string OAuthRealm
		{
			get { return String.Empty; }
		}

		public string OAuthProfileMapperCode
		{
			get { return String.Empty; }
		}

		public string SSOReturnUrl
		{
			get { return String.Empty; }
		}

		public string SSOManageAccountUrl
		{
			get { return String.Empty; }
		}

		public string OAuthVersion
		{
			get { return String.Empty; }
		}

		public bool OAuthEnabled
		{
			get { return false; }
		}

		/// <summary>
		/// Gets the O auth logout URL.
		/// </summary>
		public string OAuthLogoutUrl
		{
			get { return String.Empty; }
		}

		/// <summary>
		/// Gets the O auth change password URL.
		/// </summary>
		/// <value>
		public string OAuthChangePasswordUrl
		{
			get { return String.Empty; }
		}

		/// <summary>
		/// Gets the O auth change user name URL.
		/// </summary>
		public string OAuthChangeUserNameUrl
		{
			get { return String.Empty; }
		}

		public bool SamlEnabled { get { return false; } }
		public string SamlIdPArtifactIdProviderUrl { get { return string.Empty; } }
		public string SamlIdPSingleSignonIdProviderUrl { get { return string.Empty; } }
		public string SamlMetadataUrl { get { return string.Empty; } }
		public string SamlSpToIdPBindingUrl { get { return String.Empty; } }
		public string SamlIdPToSpBindingUrl { get { return String.Empty; } }
		public string SamlQueryStringBindingVar { get { return String.Empty; } }
		public string SamlArtifactIdentityUrl { get { return String.Empty; } }
		public string SamlSpKeyLocation { get { return String.Empty; } }
		public string SamlSpKeyName { get { return String.Empty; } }
		public string SamlSpKeyPassword { get { return String.Empty; } }
		public string SamlIdPCertKeyLocation { get { return String.Empty; } }
		public string SamlIdPCertKeyName { get { return String.Empty; } }
		public string SamlFirstNameAttributeName { get { return String.Empty; } }
		public string SamlLastNameAttributeName { get { return String.Empty; } }
		public string SamlScreenNameAttributeName { get { return String.Empty; } }
		public string SamlEmailAddressAttributeName { get { return String.Empty; } }

		public bool RecaptchaEnabled { get { return false; } }
		public string RecaptchaPublicKey { get { return String.Empty; } }
		public string RecaptchaPrivateKey { get { return String.Empty; } }

		#endregion

    #region Service Method Validation

    private bool _usesAccessToken;
    private string _accessTokenKey = string.Empty;
    private string _accessTokenSecret = string.Empty;

	  /// <summary>
	  /// Gets whether to check the version number in service method calls
	  /// </summary>
	  public bool ValidateServiceVersion { get; set; }

	  /// <summary>
    /// Gets whether AccessToken validation of service methods is enabled
    /// </summary>
    public bool UsesAccessToken
    {
      get { return _usesAccessToken; }
    }

    /// <summary>
    /// Gets the key for AccessToken validation
    /// </summary>
    public string AccessTokenKey
    {
      get { return _accessTokenKey; }
    }

    /// <summary>
    /// Gets the secret code for AccessToken validation
    /// </summary>
    public string AccessTokenSecret
    {
      get { return _accessTokenSecret; }
    }

    public void EnableAccessTokens(string key, string secret)
    {
      _usesAccessToken = key.Length > 0 && secret.Length > 0;
      _accessTokenKey = key;
      _accessTokenSecret = secret;
    }

    public void DisableAccessTokens()
    {
      _usesAccessToken = false;
      _accessTokenKey = string.Empty;
      _accessTokenSecret = string.Empty;
    }

    #endregion

		#region Caching

		public CachingMethod CacheMethod
		{
			get
			{
				CachingMethod cacheMethod;
				Enum.TryParse(Constants.ConfigurationItemKeys.CacheMethod, true, out cacheMethod);
				return cacheMethod;
			} 
		}

		public string AppFabricServer { get { return string.Empty; } }

		public int AppFabricPort { get { return 0; } }

		public string AppFabricCacheName { get { return string.Empty; } }

		#endregion

		#region Integration

		public IntegrationClient IntegrationClient { get { return IntegrationClient.Fake; } }

		// Encryption
		public bool EncryptionEnabled { get { return false; } }
		public string EncryptionKey { get { return string.Empty; } }
		public string EncryptionVector { get { return string.Empty; } }

		#endregion

		#region Custom Header Footer

		public bool UseCustomHeaderHtml { get { return false; } }
		public bool UseCustomFooterHtml { get { return false; } }
		public bool HideTalentAssistAccountSettings { get { return false; } }
		public bool HideTalentAssistSignOut { get { return false; } }
		public bool HideCareerSignIn { get { return false; } }
		public bool HideCareerRegister { get { return false; } }
		public bool HideCareerMyAccount { get { return false; } }
		public bool HideCareerSignOut { get { return false; } }
		public string CustomHeaderHtml { get { return ""; } }
		public string CustomFooterHtml { get { return ""; } }

		#endregion

	}
}