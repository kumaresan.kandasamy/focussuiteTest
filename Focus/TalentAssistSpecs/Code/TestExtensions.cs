﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Core.Messages;
using Focus.Core.Models;
using NUnit.Framework;

#endregion

namespace TalentAssistSpecs.Code
{
	/// <summary>
	/// Test Extensions 
	/// </summary>
	internal static class TestExtensions
	{
		public static void ShouldEqual(this object actual, object expected)
		{
			Assert.AreEqual(expected, actual);
		}

		public static void ShouldNotEqual(this object actual, object expected)
		{
			Assert.AreNotEqual(expected, actual);
		}

		public static void ShouldBeTheSameAs(this object actual, object expected)
		{
			Assert.AreSame(expected, actual);
		}

		public static void ShouldNotBeTheSameAs(this object actual, object expected)
		{
			Assert.AreNotSame(expected, actual);
		}

		public static void ShouldBeGreater(this int actual, int expected)
		{
			Assert.Greater(actual, expected);
		}

    public static void ShouldBeGreaterOrEqual(this int actual, long expected)
    {
      Assert.GreaterOrEqual(actual, expected);
    }

		public static void ShouldBeLess(this int actual, int expected)
		{
			Assert.Less(actual, expected);
		}

		public static void ShouldBeLessOrEqual(this int actual, int expected)
		{
			Assert.LessOrEqual(actual, expected);
		}

		public static void ShouldBeGreater(this long actual, long expected)
		{
			Assert.Greater(actual, expected);
		}

		public static void ShouldBeNull(this object actual)
		{
			Assert.IsNull(actual);
		}

		public static void ShouldNotBeNull(this object actual)
		{
			Assert.IsNotNull(actual);
		}

		public static void ShouldBeNullOrEmpty(this string actual)
		{
			Assert.IsNullOrEmpty(actual);
		}

		public static void ShouldNotBeNullOrEmpty(this string actual)
		{
			Assert.IsNotNullOrEmpty(actual);
		}

		public static void ShouldBeTrue(this bool b)
		{
			Assert.IsTrue(b);
		}

		public static void ShouldBeFalse(this bool b)
		{
			Assert.IsFalse(b);
		}

		public static Exception ShouldBeThrownBy(this Type exceptionType, TestDelegate code)
		{
			return Assert.Throws(exceptionType, code);
		}

		public static T ShouldBe<T>(this object actual)
		{
			Assert.IsInstanceOf<T>(actual);
			return (T)actual;
		}

		/// <summary>
		/// Helper method that adds ClientTag, SessionId and RequestId to all request types.
		/// </summary>
		/// <typeparam name="T">The request type.</typeparam>
		/// <param name="request">The request</param>
		/// <param name="clientTag">The client tag.</param>
		/// <param name="sessionId">The session id.</param>
		/// <param name="userContext">The user context.</param>
		/// <param name="culture">The culture.</param>
		/// <returns>Fully prepared request, ready to use.</returns>
		public static T Prepare<T>(this T request, string clientTag, Guid sessionId, UserContext userContext, string culture) where T : ServiceRequest
		{
			request.ClientTag = clientTag;
			request.SessionId = sessionId;
			request.RequestId = Guid.NewGuid();

			request.UserContext = userContext;
			request.UserContext.Culture = culture;

		  request.VersionNumber = Constants.SystemVersion;

			return request;
		}
	}
}
