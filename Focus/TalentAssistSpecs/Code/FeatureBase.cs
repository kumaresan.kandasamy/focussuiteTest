﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Transactions;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.AuthenticationService;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Data.Migration.Entities;
using Focus.Data.Report.Entities;
using Focus.Data.Reporting.Entities;
using Focus.Data.Repositories;
using Focus.Data.Repositories.Contracts;
using Focus.Services;
using Focus.Services.Core;
using Focus.Services.Helpers;
using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;

using Framework.Caching;
using Framework.Caching.Providers;
using Framework.Email;
using Framework.Email.Providers;
using Framework.Logging;
using Framework.Messaging;
using Framework.ServiceLocation;

using Mindscape.LightSpeed;

using Moq;

using NUnit.Framework;
using TalentAssistSpecs.Code;
using TechTalk.SpecFlow;

#endregion

namespace TalentAssistSpecs.Code
{
	public class TestRunBase
	{
		/// <summary>
		/// This method
		/// </summary>
		[BeforeTestRun]
		public static void BeforeTestRun()
		{
			
		}

		[AfterTestRun]
		public static void AfterTestRun()
		{
			
		}
	}
}
