<?xml version="1.0"?>
<project name="Focus Suite" xmlns="http://nant.sf.net/release/0.91-alpha1/nant.xsd" default="default">

	<!-- Version Details -->
	<property name="branch.number" value="RC" />
	<property name="tc.roothpath" value="${environment::get-variable('CurrentDirectoryForNant')}" />
	
	<if test="${property::exists('version')}">
		<echo message="System Version : ${version}" />
		<property name="branch.number" value="${version}" />
	</if>

	<echo message="Branch Number : ${branch.number}" />

	<!-- Compilation properties -->
	<property name="nant.settings.currentframework" value="net-4.0"/>
	<property name="msbuild.path" value="${framework::get-framework-directory(framework::get-target-framework())}\msbuild.exe"/>
	<property name="build.configuration" value="release"/>
	<property name="solution.file" value="${directory::get-current-directory()}\Focus\Focus.sln"/>

	<property name="build.path" value="D:\Data\Builds\Deploy\FocusSuite\"/>
	<property name="deployment.path" value="D:\Data\Apps\Release\FocusSuite\${branch.number}"/>
	<property name="zipped.path" value="D:\Data\Builds\Zipped\"/>

	<!-- Test settings -->
  <property name="test.assemblies" value="${build.path}Focus.UnitTests.dll" />
	<property name="nunit.path" value="Libraries\nunit\nunit-console-x86.exe" />

	<!-- Site configuration -->
	<property name="focusassisttalentpublish.path" value="_PublishedWebsites\Focus.Web\"/>
	<property name="focuscareerexplorerpublish.path" value="_PublishedWebsites\Focus.CareerExplorer\"/>
	<property name="messagebuspublish.path" value="MessageBus\"/>
	<property name="migrationservicespublish.path" value="MigrationServices\"/>
	<property name="serviceshostpublish.path" value="_PublishedWebsites\Focus.Services.Host\"/>

	<property name="scriptstorun.path" value="Scripts\Deployments\"/>
	<property name="v1dbmigrationscriptstorun.path" value="Scripts\Migrations\Migration Providers\FocusCareer01\"/>
	
	<property name="web.assisttalent.project.file" value="Focus.Web\Focus.Web.CSProj"/>
	<property name="web.careerexplorer.project.file" value="Focus.CareerExplorer\Focus.CareerExplorer.CSProj"/>
	<property name="messagebusproject.file" value="Focus.MessageBus\Focus.MessageBus.CSProj"/>
	<property name="migrationservicesproject.file" value="Focus.MigrationServices\Focus.MigrationServices.CSProj"/>
	<property name="serviceshostproject.file" value="Focus.Services.Host\Focus.Services.Host.CSProj"/>
	<property name="installerproject.file" value="Focus.Installer\Focus.Installer.CSProj"/>

	<property name="FullBuildPathAssist" value="${build.path}\Web\Assist"/>
	<property name="FullBuildPathCareer" value="${build.path}\Web\career\"/>
	<property name="FullBuildPathMessageBus" value="${build.path}\messagebus\"/>
	<property name="FullBuildPathMigrationServices" value="${build.path}\migrationservices\"/>
	<property name="FullBuildPathServicesHost" value="${build.path}\serviceshost\"/>
	<property name="FullBuildPathInstaller" value="${build.path}\installer\"/>

	<!-- <property name="testapp" value="${deployment.path}\Focus.Installer.exe"/> -->
	<property name="debug" value="true"/>

	<target name="default" depends="compile test deploy zip ftp" description="Compiles, tests, deploys, zips and ftps the solution" />
	<target name ="testonly" depends="compile test" description="Runs tests" />
	<target name ="notest" depends="deploy zip ftp" description="Deploys, zips and ftps the solution" />

	<!-- Compile -->
	<target name="compile" description="Compiles the solution" >
		<exec program="${msbuild.path}" commandline="${solution.file} /p:Configuration=${build.configuration} /t:Rebuild /p:OutDir=${build.path} /v:Minimal"></exec>
	</target>

	<!-- Test -->
	<target name="test">
		<!-- RUNS THE UNIT TESTS -->
		<property name="nunit.args" value="${test.assemblies}" />

		<if test="${property::exists('build.number')}">
			<property name="CurrentBuild" value="${build.number}"/>
			<echo message="Current Build : ${CurrentBuild}"  />
		</if>

		<if test="${property::exists('teamcity.dotnet.nunitlauncher')}">
			<property name="nunit.path" value="${teamcity.dotnet.nunitlauncher}" />
			<!-- Could remove this -->
			<property name="nunit.args" value="v4.0 x64 NUnit-2.5.10 ${test.assemblies}" />
		</if>

		<exec program="${nunit.path}" commandline="${nunit.args}" />
	</target>

	<!-- Deploy -->
	<target name="deploy" description="Deploys the solution">
		<echo message="Building AssistTalent: " />
		<exec program="${msbuild.path}" commandline="${directory::get-current-directory()}\Focus\${web.assisttalent.project.file} /p:Configuration=${build.configuration} /t:Build;_CopyWebApplication /p:OutDir=${FullBuildPathAssist} /v:Minimal"></exec>

		<echo message="Building Career: " />
		<exec program="${msbuild.path}" commandline="${directory::get-current-directory()}\Focus\${web.careerexplorer.project.file} /p:Configuration=${build.configuration} /t:Build;_CopyWebApplication /p:OutDir=${FullBuildPathCareer} /v:Minimal"></exec>

		<!-- Clean down previous package -->
		<echo message="Clean down previous package" />
		<property name="SourceLocation" value="${deployment.path}"/>
		<call target="DeleteDir"/>
		
		<!--Build Assist-->
		<property name="TargetLocation" value="${deployment.path}\Web\Assist"/>
		<property name="SourceLocation" value="${build.path}\Web\Assist\${focusassisttalentpublish.path}"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<!-- remove talent from the assist -->
		<echo message="Remove Talent pages from Assist"/>
		<property name="SourceLocation" value="${deployment.path}\Web\Assist\WebTalent"/>
		<call target="DeleteDir"/>

		<!-- Remove Designs folder from Assist -->
		<echo message="Remove Designs folder from Assist"/>
		<property name="SourceLocation" value="${deployment.path}\Web\Assist\Designs"/>
		<call target="DeleteDir"/>

		<!--Build Talent-->
		<property name="TargetLocation" value="${deployment.path}\Web\Talent"/>
		<property name="SourceLocation" value="${build.path}\Web\Assist\${focusassisttalentpublish.path}"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<!-- Remove Assist and Reporting from Talent-->
		<echo message="Remove Assist/Reporting pages from Talent"/>
		<property name="SourceLocation" value="${deployment.path}\Web\Talent\WebAssist"/>
		<call target="DeleteDir"/>

		<property name="SourceLocation" value="${deployment.path}\Web\Talent\WebReporting"/>
		<call target="DeleteDir"/>

		<!-- Remove Designs folder from Talent -->
		<echo message="Remove Designs folder from Talent"/>
		<property name="SourceLocation" value="${deployment.path}\Web\Talent\Designs"/>
		<call target="DeleteDir"/>

		<!--Build Career-->
		<property name="TargetLocation" value="${deployment.path}\Web\Career"/>
		<property name="SourceLocation" value="${build.path}\Web\Career\${focuscareerexplorerpublish.path}"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<!--Build Career Explorer-->
		<property name="TargetLocation" value="${deployment.path}\Web\CareerExplorer"/>
		<property name="SourceLocation" value="${build.path}\Web\Career\${focuscareerexplorerpublish.path}"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<!--Build Explorer-->
		<property name="TargetLocation" value="${deployment.path}\Web\Explorer"/>
		<property name="SourceLocation" value="${build.path}\Web\Career\${focuscareerexplorerpublish.path}"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<echo message="Building Message Bus: " />
		<exec program="${msbuild.path}" commandline="${directory::get-current-directory()}\Focus\${messagebusproject.file} /p:Configuration=${build.configuration} /t:Build /p:OutDir=${FullBuildPathMessageBus}"></exec>

		<echo message="Building Migration Services: " />
		<exec program="${msbuild.path}" commandline="${directory::get-current-directory()}\Focus\${migrationservicesproject.file} /p:Configuration=${build.configuration} /t:Build /p:OutDir=${FullBuildPathMigrationServices}"></exec>

		<echo message="Building Services Host: " />
		<exec program="${msbuild.path}" commandline="${directory::get-current-directory()}\Focus\${serviceshostproject.file} /p:Configuration=${build.configuration} /t:Build;_CopyWebApplication /p:OutDir=${FullBuildPathServicesHost}"></exec>

		<echo message="Building Installer: " />
		<exec program="${msbuild.path}" commandline="${directory::get-current-directory()}\Focus\${installerproject.file} /p:Configuration=${build.configuration} /t:Build /p:OutDir=${FullBuildPathInstaller}"></exec>

		<property name="TargetLocation" value="${deployment.path}\messagebus"/>
		<property name="SourceLocation" value="${build.path}\messagebus"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<property name="TargetLocation" value="${deployment.path}\migrationservices"/>
		<property name="SourceLocation" value="${build.path}\migrationservices"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<property name="TargetLocation" value="${deployment.path}\serviceshost"/>
		<property name="SourceLocation" value="${build.path}\serviceshost\${serviceshostpublish.path}"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<property name="TargetLocation" value="${deployment.path}\Installer"/>
		<property name="SourceLocation" value="${build.path}\installer"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<!--Scripts to Run-->
		<property name="TargetLocation" value="${deployment.path}\Scripts"/>
		<property name="SourceLocation" value="${scriptstorun.path}"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>

		<!-- Migration Scripts to Run -->
		<property name="TargetLocation" value="${deployment.path}\Scripts\V1MigrationScripts"/>
		<property name="SourceLocation" value="${v1dbmigrationscriptstorun.path}"/>
		<property name="SourceFiles" value="**\*"/>
		<call target="CopyFiles"/>
		
		<!-- Remove the Bdd Data Folders-->
		<echo message="Remove Bdd Data Folders"/>
		<property name="SourceLocation" value="${deployment.path}\Scripts"/>
		<property name="SourceFiles" value="**/BddData/*"/>
		<call target="DeleteFiles"/>
		
		
		<call target="WipeExistingSite"/>
	</target>

	<target name="WipeExistingSite">
		<!-- Clean up the target directories -->
		<property name="SourceLocation" value="${build.path}"/>
		<call target="DeleteDir"/>
	</target>

	<target name="zip">
		<zip zipfile="${zipped.path}${branch.number}.zip">
			<fileset basedir="${deployment.path}">
				<include name="**/*" />
			</fileset>
		</zip>
	</target>

	<target name ="ftp">
		<property name="UploadDir" value="UkDevTeam/release"/>
		<property name="ftp_server_name_live" value="transfer.burning-glass.com" />
		<property name="ftp_server_user_live" value="fcftp01" />
		<property name="ftp_server_password_live" value="f0cu5f0cu5" />
		<call target="CopyToFtp"/>
	</target>

	<target description="Copy to the FTP server" name="CopyToFtp">
		<echo message="${tc.roothpath}" />
		<exec program="${tc.roothpath}\Focus\Libraries\Nant\app\ncftpput.exe">
			<arg line=" -u ${ftp_server_user_live} -p ${ftp_server_password_live} ${ftp_server_name_live} ${UploadDir} ${zipped.path}${branch.number}.zip" />
		</exec>          
	</target>


	<!-- Utility Methods -->
	<target name="CopyFiles">
		<echo message="Copy from "  />
		<echo message="${SourceLocation}"  />
		<echo message="To"  />
		<echo message="${TargetLocation}"  />

		<copy todir="${TargetLocation}">
			<fileset basedir="${SourceLocation}">
				<include name="${SourceFiles}"/>
			</fileset>
		</copy>
	</target>

	<target name="DeleteFiles">
		<echo message="Delete files from "  />
		<echo message="${SourceLocation}\${SourceFiles}"  />

		<delete>
			<fileset>
				<include name="${SourceLocation}\${SourceFiles}" />
			</fileset>
		</delete>
	</target>

	<target name ="DeleteDir">
		<echo message="Delete directory  "  />
		<echo message="${SourceLocation}"  />
		<delete dir="${SourceLocation}" />
	</target>

</project>
