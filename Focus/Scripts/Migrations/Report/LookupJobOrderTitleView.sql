/****** Object:  View [dbo].[Report.LookupJobOrderTitleView]    Script Date: 09/12/2013 10:25:14 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobOrderTitleView]'))
DROP VIEW [dbo].[Report.LookupJobOrderTitleView]
GO

/****** Object:  View [dbo].[Report.LookupJobOrderTitleView]    Script Date: 09/12/2013 10:25:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobOrderTitleView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.JobTitle
	FROM
	(
		SELECT DISTINCT
			JobTitle
		FROM 
			[Report.JobOrder]
	) Data

GO


