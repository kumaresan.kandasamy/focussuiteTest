IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrder]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobOrder](
	[Id] [bigint] NOT NULL,
	[FocusJobId] [bigint] NOT NULL DEFAULT(0),
	[EmployerId] [bigint] NOT NULL DEFAULT(0),
	[JobStatus] [int] NOT NULL DEFAULT(0),
	[CountyId] [bigint] NULL,
	[County] [nvarchar](400) NULL,
	[StateId] [bigint] NULL DEFAULT(0),
	[State] [nvarchar](400) NULL,
	[PostalCode] [nvarchar](40) NULL DEFAULT (''),
	[Office] [nvarchar](400) NULL,
	[MinimumEducationLevel] [int] NULL,
	[PostingFlags] [int] NULL,
	[WorkOpportunitiesTaxCreditHires] [int] NULL,
	[ForeignLabourCertification] [bit] NULL,
  [ForeignLabourType] [int] NULL,
  [FederalContractor] [bit] NULL,
  [CourtOrderedAffirmativeAction] [int] NULL,
	[ScreeningPreferences] [int] NULL,
	[MinSalary] [decimal](24, 18) NULL,
	[MaxSalary] [decimal](24, 18) NULL,
	[SalaryFrequency] [int] NULL,
	[OnetCode] [nvarchar](20) NULL,
	[PostedOn] [datetime] NULL,
	[ClosingOn] [datetime] NULL,
	[LatitudeRadians] [float] NOT NULL DEFAULT(0),
	[LongitudeRadians] [float] NOT NULL DEFAULT(0),
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[JobTitle] [nvarchar](400) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK__Report.JobOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderAction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobOrderAction](
	[Id] [bigint] NOT NULL,
	[ActionDate] [datetime] NOT NULL,
	[JobOrderId] [bigint] NOT NULL,
	[StaffReferrals] [int] NOT NULL DEFAULT(0),
	[SelfReferrals] [int] NOT NULL DEFAULT(0),
	[ReferralsRequested] [int] NOT NULL DEFAULT(0),
	[EmployerInvitationsSent] [int] NOT NULL DEFAULT(0),
	[ApplicantsInterviewed] [int] NOT NULL DEFAULT(0),
	[ApplicantsFailedToShow] [int] NOT NULL DEFAULT(0),
	[ApplicantsDeniedInterviews] [int] NOT NULL DEFAULT(0),
	[ApplicantsHired] [int] NOT NULL DEFAULT(0),
	[JobOffersRefused] [int] NOT NULL DEFAULT(0),
	[ApplicantsDidNotApply] [int] NOT NULL DEFAULT(0),
	[InvitedJobSeekerViewed] [int] NOT NULL DEFAULT(0),
	[InvitedJobSeekerClicked] [int] NOT NULL DEFAULT(0),
	[ApplicantsNotHired] [int] NOT NULL DEFAULT(0),
	[ApplicantsNotYetPlaced] [int] NOT NULL DEFAULT(0),
	[FailedToRespondToInvitation] [int] NOT NULL DEFAULT(0),
	[FailedToReportToJob] [int] NOT NULL DEFAULT(0),
	[FoundJobFromOtherSource] [int] NOT NULL DEFAULT(0),
	[JobAlreadyFilled] [int] NOT NULL DEFAULT(0),
	[NewApplicant] [int] NOT NULL DEFAULT(0),
	[NotQualified] [int] NOT NULL DEFAULT(0),
	[ApplicantRecommended] [int] NOT NULL DEFAULT(0),
	[RefusedReferral] [int] NOT NULL DEFAULT(0),
	[UnderConsideration] [int] NOT NULL DEFAULT(0)
 CONSTRAINT [PK_Report.JobOrderAction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.JobOrder]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.JobOrderAction]'))
ALTER TABLE [dbo].[Report.JobOrderAction]  WITH NOCHECK ADD  CONSTRAINT [FK_Report.JobOrderAction_Report.JobOrder] FOREIGN KEY([JobOrderId])
REFERENCES [dbo].[Report.JobOrder] ([Id])
NOT FOR REPLICATION
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderOffice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobOrderOffice](
	[Id] [bigint] NOT NULL,
	[OfficeName] [nvarchar](100) NOT NULL,
	[OfficeId] [bigint] NULL,
	[JobOrderId] [bigint] NOT NULL DEFAULT(0),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.JobOrder]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.JobOrderOffice]'))
ALTER TABLE [dbo].[Report.JobOrderOffice]  WITH CHECK ADD FOREIGN KEY([JobOrderId])
REFERENCES [dbo].[Report.JobOrder] ([Id])
GO

