/****** Object:  View [dbo].[Report.LookupJobSeekerCertificateView]    Script Date: 09/12/2013 10:20:40 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerCertificateView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerCertificateView]
GO

/****** Object:  View [dbo].[Report.LookupJobSeekerCertificateView]    Script Date: 09/12/2013 10:20:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobSeekerCertificateView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.[Description]
	FROM
	(
		SELECT DISTINCT
			[Description] 
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 0
	) Data

GO


