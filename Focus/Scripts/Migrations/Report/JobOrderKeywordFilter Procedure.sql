/****** Object:  StoredProcedure [dbo].[Report.JobOrderKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderKeywordFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobOrderKeywordFilter]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobOrderKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 26 September 2013
-- Description:	Filter job Orders by keyword
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobOrderKeywordFilter]
	@Keywords XML,
	@CheckTitle BIT,
	@CheckDescription BIT,
	@CheckEmployer BIT,
	@SearchAll BIT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #Keywords
	(
		KeywordId INT IDENTITY(1, 1),
		TitleKeyword NVARCHAR(500),
		DescriptionKeyword NVARCHAR(500),
		EmployerKeyword NVARCHAR(500)
	)
	
	DECLARE @KeywordsToCheck INT
	
	INSERT INTO #KeyWords ( TitleKeyword, DescriptionKeyword, EmployerKeyword )
    SELECT
		CASE @CheckTitle WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckDescription WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckEmployer WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END
    FROM @Keywords.nodes('//keyword') AS x(t) 
    
	SET @KeywordsToCheck = CASE @SearchAll WHEN 1 THEN @@ROWCOUNT ELSE 1 END

	SELECT 
		J.Id
	FROM 
		[Report.JobOrder] J
	INNER JOIN [Report.Employer] E
		ON E.Id = J.EmployerId
	INNER JOIN #Keywords K
		ON J.JobTitle LIKE K.TitleKeyword
		OR J.[Description] LIKE K.DescriptionKeyword
		OR E.Name LIKE K.EmployerKeyword
	GROUP BY 
		J.Id
	HAVING
		COUNT(DISTINCT K.KeywordId) >= @KeywordsToCheck
END
GO

GRANT EXEC ON [dbo].[Report.JobOrderKeywordFilter] TO [FocusAgent]