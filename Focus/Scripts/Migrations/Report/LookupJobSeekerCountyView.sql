/****** Object:  View [dbo].[Report.LookupJobSeekerCountyView]    Script Date: 09/12/2013 10:25:14 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerCountyView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerCountyView]
GO

/****** Object:  View [dbo].[Report.LookupJobSeekerCountyView]    Script Date: 09/12/2013 10:25:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobSeekerCountyView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.County
	FROM
	(
		SELECT DISTINCT
			County
		FROM 
			[Report.JobSeeker]
	) Data

GO


