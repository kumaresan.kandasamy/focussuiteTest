/****** Object:  View [dbo].[Report.LookupJobSeekerOfficeView]    Script Date: 09/12/2013 10:30:05 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerOfficeView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerOfficeView]
GO

/****** Object:  View [dbo].[Report.LookupJobSeekerOfficeView]    Script Date: 09/12/2013 10:30:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobSeekerOfficeView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.Office
	FROM
	(
		SELECT DISTINCT
			Office
		FROM 
			[Report.JobSeeker]
	) Data

GO


