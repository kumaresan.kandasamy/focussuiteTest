
/****** Object:  StoredProcedure [dbo].[Report.JobOrderActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderActionTotalsForIds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobOrderActionTotalsForIds]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobOrderActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 27 September 2013
-- Description:	Get report totals for job Order actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobOrderActionTotalsForIds]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@JobOrderIds XML
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #JobOrderIds
	(
		Id BIGINT
	)
	
	INSERT INTO #JobOrderIds ( Id )
    SELECT t.value('.', 'bigint')
    FROM @JobOrderIds.nodes('//id') AS x(t) 

	SELECT
		JSA.JobOrderId AS Id,
		SUM(JSA.StaffReferrals) AS StaffReferrals,  
		SUM(JSA.SelfReferrals) AS SelfReferrals,  
		SUM(JSA.ReferralsRequested) AS ReferralsRequested,
		SUM(JSA.EmployerInvitationsSent) AS EmployerInvitationsSent,  
		SUM(JSA.ApplicantsInterviewed) AS ApplicantsInterviewed,  
		SUM(JSA.ApplicantsFailedToShow) AS ApplicantsFailedToShow,  
		SUM(JSA.ApplicantsDeniedInterviews) AS ApplicantsDeniedInterviews,  
		SUM(JSA.ApplicantsHired) AS ApplicantsHired,  
		SUM(JSA.JobOffersRefused) AS JobOffersRefused,  
		SUM(JSA.ApplicantsDidNotApply) AS ApplicantsDidNotApply,
		SUM(JSA.InvitedJobSeekerViewed) AS InvitedJobSeekerViewed,
		SUM(JSA.InvitedJobSeekerClicked) AS InvitedJobSeekerClicked,
		SUM(JSA.ApplicantsNotHired) AS ApplicantsNotHired, 
		SUM(JSA.ApplicantsNotYetPlaced) AS ApplicantsNotYetPlaced, 
		SUM(JSA.FailedToRespondToInvitation) AS FailedToRespondToInvitation, 
		SUM(JSA.FailedToReportToJob) AS FailedToReportToJob, 
		SUM(JSA.FoundJobFromOtherSource) AS FoundJobFromOtherSource, 
		SUM(JSA.JobAlreadyFilled) AS JobAlreadyFilled, 
		SUM(JSA.NewApplicant) AS NewApplicant, 
		SUM(JSA.NotQualified) AS NotQualified, 
		SUM(JSA.ApplicantRecommended) AS ApplicantRecommended, 
		SUM(JSA.RefusedReferral) AS RefusedReferral, 
		SUM(JSA.UnderConsideration) AS UnderConsideration
	FROM 
		[Report.JobOrderAction] JSA
	INNER JOIN #JobOrderIds JSI
		ON JSA.JobOrderId = JSI.Id
	WHERE 
		JSA.ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		JSA.JobOrderId
END
GO

GRANT EXEC ON [dbo].[Report.JobOrderActionTotalsForIds] TO [FocusAgent]
