
/****** Object:  StoredProcedure [dbo].[Report.EmployerActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerActionTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.EmployerActionTotals]
GO

/****** Object:  StoredProcedure [dbo].[Report.EmployerActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 27 September 2013
-- Description:	Get report totals for employer actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.EmployerActionTotals]
	@FromDate DATETIME,
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		EmployerId AS Id,
		SUM(JobOrdersEdited) AS JobOrdersEdited,
		SUM(JobSeekersInterviewed) AS JobSeekersInterviewed,
		SUM(JobSeekersHired) AS JobSeekersHired,
		SUM(JobOrdersCreated) AS JobOrdersCreated,
		SUM(JobOrdersPosted) AS JobOrdersPosted,
		SUM(JobOrdersPutOnHold) AS JobOrdersPutOnHold,
		SUM(JobOrdersRefreshed) AS JobOrdersRefreshed,
		SUM(JobOrdersClosed) AS JobOrdersClosed,
		SUM(InvitationsSent) AS InvitationsSent,
		SUM(JobSeekersNotHired) AS JobSeekersNotHired,
		SUM(SelfReferrals) AS SelfReferrals,
		SUM(StaffReferrals) AS StaffReferrals
	FROM 
		[Report.EmployerAction]
	WHERE 
		ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		EmployerId
END
GO

GRANT EXEC ON [dbo].[Report.EmployerActionTotals] TO [FocusAgent]