SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.Employer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.Employer](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](400) NOT NULL DEFAULT(''),
	[FederalEmployerIdentificationNumber] [nvarchar](72) NOT NULL DEFAULT(''),
	[StateId] [bigint] NULL DEFAULT(0),
	[PostalCode] [nvarchar](40) NULL DEFAULT(''),
	[Office] [nvarchar](400) NULL DEFAULT(''),
	[CreationDateTime] [datetime] NULL,
	[CountyId] [bigint] NULL,
	[FocusBusinessUnitId] [bigint] NOT NULL DEFAULT(0),
	[FocusEmployerId] [bigint] NULL,
	[State] [nvarchar](400) NULL,
	[County] [nvarchar](400) NULL,
	[LatitudeRadians] [float] NOT NULL DEFAULT(0),
	[LongitudeRadians] [float] NOT NULL DEFAULT(0),
	[CreatedOn] [datetime] NOT NULL DEFAULT (getdate()),
	[UpdatedOn] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK__Report.Employer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerAction]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.EmployerAction](
	[Id] [bigint] NOT NULL,
	[ActionDate] [datetime] NOT NULL,
	[EmployerId] [bigint] NOT NULL,
	[JobOrdersEdited] [int] NOT NULL DEFAULT(0),
	[JobSeekersInterviewed] [int] NOT NULL DEFAULT(0),
	[JobSeekersHired] [int] NOT NULL DEFAULT(0),
	[JobOrdersCreated] [int] NOT NULL DEFAULT(0),
	[JobOrdersPosted] [int] NOT NULL DEFAULT(0),
	[JobOrdersPutOnHold] [int] NOT NULL DEFAULT(0),
	[JobOrdersRefreshed] [int] NOT NULL DEFAULT(0),
	[JobOrdersClosed] [int] NOT NULL DEFAULT(0),
	[InvitationsSent] [int] NOT NULL DEFAULT(0),
	[JobSeekersNotHired] [int] NOT NULL DEFAULT(0),
	[SelfReferrals] [int] NOT NULL DEFAULT(0),
	[StaffReferrals] [int] NOT NULL DEFAULT(0)
 CONSTRAINT [PK_Report.EmployerAction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.Employer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.EmployerAction]'))
ALTER TABLE [dbo].[Report.EmployerAction]  WITH CHECK ADD  CONSTRAINT [FK_Report.EmployerAction_Report.Employer] FOREIGN KEY([EmployerId])
REFERENCES [dbo].[Report.Employer] ([Id])
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerOffice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.EmployerOffice](
	[Id] [bigint] NOT NULL,
	[OfficeName] [nvarchar](100) NOT NULL,
	[OfficeId] [bigint] NULL,
	[EmployerId] [bigint] NOT NULL DEFAULT(0),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.Employer]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.EmployerOffice]'))
ALTER TABLE [dbo].[Report.EmployerOffice]  WITH CHECK ADD FOREIGN KEY([EmployerId])
REFERENCES [dbo].[Report.Employer] ([Id])
GO

