IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Report.Reporting_Activity]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Data.Report.Reporting_Activity]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Report.Reporting_Employer]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Data.Report.Reporting_Employer]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Report.Reporting_Employer_Grouped]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Data.Report.Reporting_Employer_Grouped]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Report.Reporting_JobOrder]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Data.Report.Reporting_JobOrder]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Report.Reporting_JobOrder_Grouped]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Data.Report.Reporting_JobOrder_Grouped]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Report.Reporting_JobSeeker]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Data.Report.Reporting_JobSeeker]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Report.Reporting_JobSeeker_Grouped]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[Data.Report.Reporting_JobSeeker_Grouped]
GO
