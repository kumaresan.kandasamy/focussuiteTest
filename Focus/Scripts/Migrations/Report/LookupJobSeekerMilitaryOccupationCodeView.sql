/****** Object:  View [dbo].[Report.LookupJobSeekerMilitaryOccupationCodeView]    Script Date: 09/12/2013 10:27:15 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupJobSeekerMilitaryOccupationCodeView]'))
DROP VIEW [dbo].[Report.LookupJobSeekerMilitaryOccupationCodeView]
GO

/****** Object:  View [dbo].[Report.LookupJobSeekerMilitaryOccupationCodeView]    Script Date: 09/12/2013 10:27:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupJobSeekerMilitaryOccupationCodeView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.MilitaryOccupationCode
	FROM
	(
		SELECT DISTINCT
			[Description] AS MilitaryOccupationCode
		FROM 
			[Report.JobSeekerData]
		WHERE
			DataType = 7
	) Data

GO


