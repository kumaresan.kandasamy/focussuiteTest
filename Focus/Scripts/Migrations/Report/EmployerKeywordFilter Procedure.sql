/****** Object:  StoredProcedure [dbo].[Report.EmployerKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerKeywordFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.EmployerKeywordFilter]
GO

/****** Object:  StoredProcedure [dbo].[Report.EmployerKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 04 October 2013
-- Description:	Filter job Orders by keyword
-- =============================================
CREATE PROCEDURE [dbo].[Report.EmployerKeywordFilter]
	@Keywords XML,
	@CheckName BIT,
	@CheckJobTitle BIT,
	@CheckDescription BIT = 0,
	@SearchAll BIT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #Keywords
	(
		KeywordId INT IDENTITY(1, 1),
		NameKeyword NVARCHAR(500),
		JobTitleKeyword NVARCHAR(500),
		DescriptionKeyword NVARCHAR(500)
	)
	
	DECLARE @KeywordsToCheck INT
	
	INSERT INTO #KeyWords ( NameKeyword, JobTitleKeyword, DescriptionKeyword )
    SELECT
		CASE @CheckName WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckJobTitle WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckDescription WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END
    FROM @Keywords.nodes('//keyword') AS x(t) 
    
	SET @KeywordsToCheck = CASE @SearchAll WHEN 1 THEN @@ROWCOUNT ELSE 1 END
	
	SELECT DISTINCT
		E.Id
	FROM 
		[Report.Employer] E
	INNER JOIN [Report.JobOrder] J
		ON J.EmployerId = E.Id
	INNER JOIN #Keywords K
		ON E.Name LIKE K.NameKeyword
		OR J.JobTitle LIKE K.JobTitleKeyword
		OR J.[Description] LIKE K.DescriptionKeyword
	GROUP BY 
		E.Id,
		J.Id
	HAVING
		COUNT(DISTINCT K.KeywordId) >= @KeywordsToCheck
END
GO

GRANT EXEC ON [dbo].[Report.EmployerKeywordFilter] TO [FocusAgent]