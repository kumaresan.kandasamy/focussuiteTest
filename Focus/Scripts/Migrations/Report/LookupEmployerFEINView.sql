/****** Object:  View [dbo].[Report.LookupEmployerFEINView]    Script Date: 09/12/2013 10:25:14 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.LookupEmployerFEINView]'))
DROP VIEW [dbo].[Report.LookupEmployerFEINView]
GO

/****** Object:  View [dbo].[Report.LookupEmployerFEINView]    Script Date: 09/12/2013 10:25:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Report.LookupEmployerFEINView]

AS
	SELECT
		ROW_NUMBER() OVER (ORDER BY @@RowCount) AS Id,
		Data.FederalEmployerIdentificationNumber
	FROM
	(
		SELECT DISTINCT
			FederalEmployerIdentificationNumber
		FROM 
			[Report.Employer]
	) Data

GO


