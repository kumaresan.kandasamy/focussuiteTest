/****** Object:  StoredProcedure [dbo].[Report.JobSeekerKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerKeywordFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobSeekerKeywordFilter]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobSeekerKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 13 September 2013
-- Description:	Filter job seekers by keyword
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobSeekerKeywordFilter]
	@Keywords XML,
	@TypesToCheck XML,
	@SearchAll BIT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #Keywords
	(
		Keyword NVARCHAR(500)
	)
	
	DECLARE @Types TABLE
	(
		KeywordType INT
	)
	
	DECLARE @KeywordsToCheck INT
	
	INSERT INTO #KeyWords ( Keyword )
    SELECT '%' + t.value('.', 'varchar(500)') + '%'
    FROM @Keywords.nodes('//keyword') AS x(t) 
    
  SET @KeywordsToCheck = CASE @SearchAll WHEN 1 THEN @@ROWCOUNT ELSE 1 END

	INSERT INTO @Types ( KeywordType )
    SELECT t.value('.', 'int')
    FROM @TypesToCheck.nodes('//type') AS x(t) 

	SELECT 
		JSD.JobSeekerId AS Id
	FROM
		[Report.JobSeekerData] JSD
	INNER JOIN #Keywords K
		ON JSD.[Description] LIKE K.Keyword
	WHERE
		JSD.DataType IN (SELECT KeywordType FROM @Types)
	GROUP BY
		JSD.JobSeekerId
	HAVING
		COUNT(DISTINCT K.Keyword) >= @KeywordsToCheck
END
GO

GRANT EXEC ON [dbo].[Report.JobSeekerKeywordFilter] TO [FocusAgent]