/*
-
- This script should be run on the Focus database to set the LSCS client filtering/tiering configuration values
-
*/
Print 'Setting Explorer config to the LSCS settings'
BEGIN TRANSACTION
-- Add/update ExplorerDegreeFilteringType
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeFilteringType')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = '4'
	WHERE
		[Key] = 'ExplorerDegreeFilteringType'
END
ELSE
BEGIN
	BEGIN TRANSACTION
		
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerDegreeFilteringType',
		'4'		
	)
		
	COMMIT TRANSACTION
END
-- Add/update ExplorerDegreeClientDataTag
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeClientDataTag')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'LSCS'
	WHERE
		[Key] = 'ExplorerDegreeClientDataTag'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerDegreeClientDataTag',
		'LSCS'		
	)
		
END

-- Add/update ExplorerUseDegreeTiering
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerUseDegreeTiering')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'true'
	WHERE
		[Key] = 'ExplorerUseDegreeTiering'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerUseDegreeTiering',
		'true'		
	)
	
END

-- Add/update ExplorerSection Order
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerSectionOrder')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'School|Research|Explore|Experience'
	WHERE
		[Key] = 'ExplorerSectionOrder'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerSectionOrder',
		'School|Research|Explore|Experience'		
	)
	
END
 -- ExplorerShowDegreeStudyPlaces

 -- Hide study places as we don't ave LS data and don't want to show other institutions
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerShowDegreeStudyPlaces')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'false'
	WHERE
		[Key] = 'ExplorerShowDegreeStudyPlaces'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerShowDegreeStudyPlaces',
		'false'		
	)
		
END
COMMIT TRANSACTION