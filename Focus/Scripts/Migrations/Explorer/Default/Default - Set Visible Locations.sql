/*
-
- This script should be run on the explorer database to set the default locations default
-
*/

UPDATE
	[Library.StateArea]
SET
	Display = 1,
	IsDefault = 0
	

UPDATE
	[Library.StateArea]
SET
	IsDefault = 1
WHERE
	AreaCode = '00000'