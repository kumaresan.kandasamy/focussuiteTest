/*
-
- This script should be run on the explorer database to set the only OK state as visible
-
*/

Print 'Resetting all OK state locations to visible'

UPDATE
	dbo.StateArea
SET
	Display = 0,
	IsDefault = 0

UPDATE
	sa
SET
	sa.Display = 1
FROM
	StateArea sa
INNER JOIN [State] s ON s.Id = sa.StateId
WHERE
	s.Code = 'OK'
	
UPDATE
	sa
SET
	sa.IsDefault = 1
FROM
	StateArea sa
INNER JOIN [State] s ON s.Id = sa.StateId
INNER JOIN Area a ON a.Id = sa.AreaId
WHERE
	s.Code = 'OK'
AND a.SortOrder = 0