/*
Script to be run when migrating to the new Apollo Explorer database

1. Make a copy of of the FocusBase database, this will be the new Apollo Explorer database
2. Restore the current Apollo Focus databse to the same server as 1
3. Run this script on the database created in step 1
4. Run the config and localisation scripts for apollo on the database created in step 1
5. You'rwe good to go, just reset any web.config values!

HISTORY

16-Jul-2013 - Created (Ade)


*/
INSERT INTO [Data.Application.Person]
           ([Id]
           ,[TitleId]
           ,[FirstName]
           ,[MiddleInitial]
           ,[LastName]
           ,[DateOfBirth]
           ,[SocialSecurityNumber]
           ,[JobTitle]
           ,[EmailAddress])
SELECT [Id]
      ,[TitleId]
      ,[FirstName]
      ,[MiddleInitial]
      ,[LastName]
      ,[DateOfBirth]
      ,[SocialSecurityNumber]
      ,[JobTitle]
      ,[EmailAddress]
FROM [FocusApollo].[dbo].[Person]
WHERE ID NOT IN (1,1701625)


INSERT INTO [Data.Application.User]
           ([Id]
           ,[UserName]
           ,[PasswordHash]
           ,[PasswordSalt]
           ,[Enabled]
           ,[DeletedOn]
           ,[CreatedOn]
           ,[UpdatedOn]
           ,[ValidationKey]
           ,[UserType]
           ,[LoggedInOn]
           ,[LastLoggedInOn]
           ,[ExternalId]
           ,[IsClientAuthenticated]
           ,[ScreenName]
           ,[SecurityQuestion]
           ,[SecurityAnswerHash]
           ,[RegulationsConsent]
           ,[PersonId])
SELECT [Id]
      ,[UserName]
      ,[PasswordHash]
      ,[PasswordSalt]
      ,[Enabled]
      ,[DeletedOn]
      ,[CreatedOn]
      ,[UpdatedOn]
      ,[ValidationKey]
      ,8
      ,[LoggedInOn]
      ,[LastLoggedInOn]
      ,[ExternalId]
      ,[IsClientAuthenticated]
      ,[ScreenName]
      ,[SecurityQuestion]
      ,[SecurityAnswerHash]
      ,0
      ,[PersonId]
  FROM [FocusApollo].[dbo].[User]
  WHERE [Id] NOT IN (2,3)


INSERT INTO [Data.Application.UserRole]
           ([Id]
           ,[RoleId]
           ,[UserId])
SELECT [Id]
      ,[RoleId]
      ,[UserId]
  FROM [FocusApollo].[dbo].[UserRole]
  WHERE [UserId] NOT IN (2,3) AND RoleId = 4756810



INSERT INTO [Data.Application.Bookmark]
           ([Id]
           ,[Name]
           ,[Type]
           ,[Criteria]
           ,[UserId]
           ,[UserType])
SELECT [Id]
      ,[Name]
      ,[Type]
      ,[Criteria]
      ,[UserId]
      ,[UserType]
  FROM [FocusApollo].[dbo].[Bookmark]


