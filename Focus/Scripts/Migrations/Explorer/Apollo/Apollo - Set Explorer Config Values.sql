/*
-
- This script should be run on the Focus database to set the Apollo client filtering/tiering configuration values
-
*/
Print 'Setting Explorer config to the Apollo settings'

DECLARE @NextId bigint
DECLARE @EndOfBlockNextId bigint
DECLARE @IdsRequired bigint

SET @IdsRequired = 1
-- Add/update ExplorerDegreeFilteringType
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeFilteringType')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = '3'
	WHERE
		[Key] = 'ExplorerDegreeFilteringType'
END
ELSE
BEGIN
	BEGIN TRANSACTION
	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired
	
	INSERT INTO [Config.ConfigurationItem]
	(
		Id,
		[Key],
		Value
	)
	VALUES
	(
		@NextId,
		'ExplorerDegreeFilteringType',
		'3'		
	)
		
	COMMIT TRANSACTION
END
-- Add/update ExplorerDegreeClientDataTag
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeClientDataTag')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'ApolloUoPX'
	WHERE
		[Key] = 'ExplorerDegreeClientDataTag'
END
ELSE
BEGIN
	BEGIN TRANSACTION
	UPDATE KeyTable SET NextId = NextId + @IdsRequired

	SELECT @EndOfBlockNextId = NextId FROM KeyTable

	SELECT @NextId = @EndOfBlockNextId - @IdsRequired
	
	INSERT INTO [Config.ConfigurationItem]
	(
		Id,
		[Key],
		Value
	)
	VALUES
	(
		@NextId,
		'ExplorerDegreeClientDataTag',
		'ApolloUoPX'		
	)
		
	COMMIT TRANSACTION
END

-- Add/update ExplorerUseDegreeTiering
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerUseDegreeTiering')
BEGIN
	-- Use default setting
	DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerUseDegreeTiering'
END


-- Add/update Explorer section order
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerSectionOrder')
BEGIN
	-- Use default setting
	DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerSectionOrder'
END
