DECLARE @Items TABLE
(
	[Key] NVARCHAR(400),
	[Value] NVARCHAR(1000)
)

DECLARE @LocalisationId bigint

SELECT @LocalisationId = [Id] FROM [Config.Localisation] WHERE Culture = '**-**'

INSERT INTO @Items ([Key], [Value])
VALUES('Global.HiringDemand.Header:Education', 'Hiring demand for graduates over the last 12 months')

INSERT INTO @Items ([Key], [Value])
VALUES('Focus.CareerExplorer.WebCareer.Controls.Preferences.Skills.Label:Education', 'Skills from my resume')

DELETE TI FROM @Items TI
INNER JOIN [Config.LocalisationItem] LI ON LI.[Key] = TI.[Key]

BEGIN TRANSACTION

INSERT INTO [Config.LocalisationItem] ([Key], [Value], LocalisationId, ContextKey, Localised)
SELECT [Key], [Value], @LocalisationId, '', 0
FROM @Items

COMMIT TRANSACTION