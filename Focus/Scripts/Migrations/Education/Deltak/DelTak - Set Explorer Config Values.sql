PRINT "THIS SCRIPT IS ONLY FOR DELTAK ON 0303"
RETURN

-- Add/update ExplorerSection Order
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerSectionOrder')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'Research|Explore'
	WHERE
		[Key] = 'ExplorerSectionOrder'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerSectionOrder',
		'Research|Explore'		
	)
	
END

-- Add/update In-Demand Dropdown Order
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerInDemandDropdownOrder')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'Job|Employer|Internship'
	WHERE
		[Key] = 'ExplorerInDemandDropdownOrder'
END
ELSE
BEGIN
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerInDemandDropdownOrder',
		'Job|Employer|Internship'		
	)
END

-- Show ProgramArea
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowProgramArea')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'false'
	WHERE
		[Key] = 'ShowProgramArea'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ShowProgramArea',
		'false'		
	)
	
END

-- Show jobs based on client degrees
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerOnlyShowJobsBasedOnClientDegrees')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'true'
	WHERE
		[Key] = 'ExplorerOnlyShowJobsBasedOnClientDegrees'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerOnlyShowJobsBasedOnClientDegrees',
		'true'		
	)
	
END

-- Add/update degree filter type Order
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeFilteringType')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = '2'
	WHERE
		[Key] = 'ExplorerDegreeFilteringType'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerDegreeFilteringType',
		'2'		
	)
	
END


-- Add/update ExplorerDegreeClientDataTag
IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeClientDataTag')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		VALUE = 'BEN'
	WHERE
		[Key] = 'ExplorerDegreeClientDataTag'
END
ELSE
BEGIN
	
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value
	)
	VALUES
	(
		'ExplorerDegreeClientDataTag',
		'BEN'		
	)
		
END

-- Change Default State to Ilinois
UPDATE [Config.ConfigurationItem] SET Value = 'State.IL' WHERE [Key] = 'DefaultStateKey'

UPDATE
	SA
SET
	IsDefault = 0
FROM
	[Library.StateArea] SA

UPDATE
	SA
SET
	IsDefault = 1
FROM
	[Library.StateArea] SA
INNER JOIN [Library.State] S
	ON S.Id = SA.StateId
INNER JOIN [Library.Area] A
	ON A.Id = SA.AreaId
WHERE 
	S.Code = 'IL'
	AND A.SortOrder = 0

/* 
Rollback
DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('ExplorerOnlyShowJobsBasedOnClientDegrees', 'ShowProgramArea', 'ExplorerSectionOrder', 'ExplorerDegreeFilteringType', 'ExplorerDegreeClientDataTag')
*/
