﻿INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('AppVersion','<span style="color:red;font-weight:bold;">stage-southdakota</span>',1),
('ResumeFormat_Standard','~\Assets\Xslts\FocusCareerFormat.xsl',1),
('ResumeFormat_Professional','~\Assets\Xslts\ProfessionalFormat.xsl',1),
('ResumeFormat_Modern','~\Assets\Xslts\ModernFormat.xsl',1),
('ResumeFormat_Contemporary','~\Assets\Xslts\ContemporaryFormat.xsl',1),
('ResumeFormat_Classic','~\Assets\Xslts\ClassicFormat.xsl',1),
('ResumeFormat_Chronological','~\Assets\Xslts\ChronologicalFormat.xsl',1),
('CareerApplicationPath','http://stage-southdakota.focus-career.com/career',1),
('ValidClientTags','SOUDAKSTG',1),
('TalentApplicationPath','http://stage-southdakota.focus-career.com/talent',1),
('AssistApplicationPath','http://stage-southdakota.focus-career.com/assist',1),
('LensService','{"host":"focuslivelensserver","portNumber":2006,"timeout":600,"vendor":"southdakotastage","characterSet":"iso-8859-1","serviceType":"Posting","description":"Career posting repository","readonly":false,"customFilters":[{"key":"Status","tag":"cf001"},{"key":"OpenDate","tag":"cf002"},{"key":"ClosingOn","tag":"cf003"},{"key":"MinimumSalary","tag":"cf004"},{"key":"MaximumSalary","tag":"cf005"},{"key":"ExternalPostingId","tag":"cf007"},{"key":"Occupation","tag":"cf008"},{"key":"StateCode","tag":"cf009"},{"key":"PostingId","tag":"cf010"},{"key":"Origin","tag":"cf012"},{"key":"Sic2","tag":"cf013"},{"key":"Industry","tag":"cf014"},{"key":"EducationLevel","tag":"cf016"},{"key":"GreenJob","tag":"cf022"},{"key":"Sector","tag":"cf024"},{"key":"CountyCode","tag":"cf025"},{"key":"Internship","tag":"cf026"},{"key":"ProgramsOfStudy","tag":"cf027"},{"key":"AnnualPay","tag":"cf028"},{"key":"Salary","tag":"cf029"},{"key":"Msa","tag":"cf030"},{"key":"ROnet","tag":"cf031"},{"key":"UnpaidPosition","tag":"cf033"},{"key":"InternshipCategories","tag":"cf034"},{"key":"RequiredProgramOfStudyId","tag":"cf036"}]}',1),
('LensService','{"host":"focuslivelensserver","portNumber":2006,"timeout":600,"vendor":"southdakotastage","characterSet":"iso-8859-1","serviceType":"Resume","description":"Talent/Assist resume repository","readonly":false,"customFilters":[{"key":"Status","tag":"cf001"},{"key":"RegisteredOn","tag":"cf002"},{"key":"ModifiedOn","tag":"cf003"},{"key":"DateOfBirth","tag":"cf004"},{"key":"CitizenFlag","tag":"cf005"},{"key":"VeteranStatus","tag":"cf007"},{"key":"Languages","tag":"cf008"},{"key":"DrivingLicenceClass","tag":"cf009"},{"key":"DrivingLicenceEndorsement","tag":"cf010"},{"key":"Certificates","tag":"cf011"},{"key":"WorkOvertime","tag":"cf012"},{"key":"ShiftType","tag":"cf013"},{"key":"WorkType","tag":"cf014"},{"key":"WorkWeek","tag":"cf015"},{"key":"EducationLevel","tag":"cf016"},{"key":"JobseekerId","tag":"cf017"},{"key":"Relocation","tag":"cf019"},{"key":"JobTypes","tag":"cf020"},{"key":"GPA","tag":"cf021"},{"key":"MonthsExperience","tag":"cf022"},{"key":"EnrollmentStatus","tag":"cf023"},{"key":"ProgramsOfStudy","tag":"cf024"},{"key":"DegreeCompletionDate","tag":"cf025"}]}',1),
('LensService','{"host":"focuslivelensserver","portNumber":2006,"timeout":600,"vendor":"southdakotastage","characterSet":"iso-8859-1","serviceType":"Generic","description":"Generic Lens","readonly":true}',1),
('LensService','{"host":"focuslivelensserver","portNumber":3505,"timeout":600,"vendor":"southdakotastage","characterSet":"iso-8859-1","serviceType":"JobMine","description":"Job Mine","readonly":true}',1),
('LensService','{"host":"focuslivelensserver","portNumber":2003,"timeout":600,"vendor":"","characterSet":"iso-8859-1","serviceType":"Posting","description":"Spidered posting repository","readonly":true,"newjobspostingdelaydays":1,"customFilters":[{"key":"Status","tag":"cf001"},{"key":"OpenDate","tag":"cf002"},{"key":"ClosingOn","tag":"cf003"},{"key":"MinimumSalary","tag":"cf004"},{"key":"MaximumSalary","tag":"cf005"},{"key":"ExternalPostingId","tag":"cf007"},{"key":"Occupation","tag":"cf008"},{"key":"StateCode","tag":"cf009"},{"key":"PostingId","tag":"cf010"},{"key":"Origin","tag":"cf012"},{"key":"Sic2","tag":"cf013"},{"key":"Industry","tag":"cf014"},{"key":"EducationLevel","tag":"cf016"},{"key":"GreenJob","tag":"cf022"},{"key":"Sector","tag":"cf024"},{"key":"CountyCode","tag":"cf025"},{"key":"Internship","tag":"cf026"},{"key":"ProgramsOfStudy","tag":"cf027"},{"key":"AnnualPay","tag":"cf028"},{"key":"Salary","tag":"cf029"},{"key":"Msa","tag":"cf030"},{"key":"ROnet","tag":"cf031"},{"key":"UnpaidPosition","tag":"cf033"},{"key":"InternshipCategories","tag":"cf034"},{"key":"RequiredProgramOfStudyId","tag":"cf036"}]}',1),
('DefaultStateKey','State.SD',1),
('DocumentStoreServiceUrl','http://USHtmlDocStore/CareerDocStoreSvc/DocStoreSvc.asmx',1),
('LiveJobsApiRootUrl','http://10.0.2.86/LiveJobsApi/V110_Stag',1),
('LiveJobsApiKey','FocusStag',1),
('LiveJobsApiSecret','AEAA00C48697417A8A236BA09567E649',1),
('LiveJobsApiToken','Core',1),
('LiveJobsApiTokenSecret','7A07362AF1EE11E192DF85E56188709B',1),
-- Talent Additions
('NearbyStateKeys','State.ND,State.MN,State.IA,State.NE,State.WY,State.MT',1)


