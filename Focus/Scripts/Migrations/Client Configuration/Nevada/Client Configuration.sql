﻿--Enable Custom Footer
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'UseCustomFooterHtml'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('UseCustomFooterHtml','True',1)

--Insert Legal notice into footer
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml-Workforce-CareerExplorer'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('CustomFooterHtml-Workforce-CareerExplorer','<span style="font-size:small">This project was funded &#36;781,178 (50&#37; of its total cost), from a grant awarded under the Trade Adjustment Assistance Community College and Career Training Grants, as implemented by the U.S. Department of Labor&#39;s Employment and Training Administration. Auxiliary aids and services are available upon request to individuals with disabilities.</span>',1)

