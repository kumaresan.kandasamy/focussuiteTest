UPDATE
	[Data.Application.User]
SET
	UserName = REPLACE(UserName, '@ci.bgt.com', '')
WHERE
	UserName LIKE '%@ci.bgt.com' 
	AND UserType = 1
	AND ExternalId IS NOT NULL
	AND ExternalId <> ''
	
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'DummyUsernameFormat'