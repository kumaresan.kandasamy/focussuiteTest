DECLARE @ClientDataTag NVARCHAR(10)
DECLARE @ApplicationStatusEnum INT

SET @ClientDataTag = 'KY'
SET @ApplicationStatusEnum = 21

-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @ApplicationStatusEnum AND [ClientDataTag] = @ClientDataTag	

-- Insert application status items
INSERT INTO [Config.ExternalLookUpItem] 
	([InternalId], [ExternalId], [ExternalLookUpType], [ClientDataTag], [FocusId])
VALUES 
	('DidNotApply', '3', @ApplicationStatusEnum, @ClientDataTag, 5),
	('FailedToShow', '3', @ApplicationStatusEnum, @ClientDataTag, 9),
	('FailedToReportToJob', '4', @ApplicationStatusEnum, @ClientDataTag, 14),
	('FailedToRespondToInvitation', '3', @ApplicationStatusEnum, @ClientDataTag, 15),
	('Hired', '1', @ApplicationStatusEnum, @ClientDataTag, 12),
	('InterviewDenied', '7', @ApplicationStatusEnum, @ClientDataTag, 8),
	('JobAlreadyFilled', '5', @ApplicationStatusEnum, @ClientDataTag, 17),
	('NotHired', '2', @ApplicationStatusEnum, @ClientDataTag, 13),
	('NotQualified', '7', @ApplicationStatusEnum, @ClientDataTag, 18),
	('RefusedOffer', '6', @ApplicationStatusEnum, @ClientDataTag, 11)