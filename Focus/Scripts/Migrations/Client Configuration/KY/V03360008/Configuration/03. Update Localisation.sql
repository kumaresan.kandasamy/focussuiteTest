DECLARE @Items TABLE
(
	[Key] NVARCHAR(100),
	Value NVARCHAR(100)
)
INSERT INTO @Items ([Key], Value)
VALUES
	('ApplicationStatusTypes.DidNotApply', 'Failed to apply to job'),
	('ApplicationStatusTypes.FailedToShow', 'Failed to report to interview')
	
UPDATE
	LI
SET
	Value = I.Value
FROM
	[Config.LocalisationItem] LI
INNER JOIN @Items I
	ON I.[Key] = LI.[Key]