DECLARE @ClientDataTag NVARCHAR(10)
DECLARE @ActionTypeEnum INT

SET @ClientDataTag = 'KY'
SET @ActionTypeEnum = 23

BEGIN -- Add Action types
	-- Clear down existing data
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @ActionTypeEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert new action type look ups
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChangeUserName', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChangeJobSeekerSsn', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChangeSecurityQuestion', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('SavePostingSavedSearch', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('LogIn', '45', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('FindJobsForSeeker', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ExternalReferral', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('SelfReferral', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ViewJobInsights', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('SaveResume', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('RunManualJobSearch', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChangePassword', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('CompleteResume', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('EditDefaultCompleteResume', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ResumePreparationAssistance', '37', @ActionTypeEnum, @ClientDataTag, 1)
END