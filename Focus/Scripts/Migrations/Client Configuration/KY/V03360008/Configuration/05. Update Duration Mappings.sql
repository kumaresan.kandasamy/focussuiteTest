DECLARE @ClientDataTag NVARCHAR(10)
DECLARE @DurationEnum INT

SET @ClientDataTag = 'KY'
SET @DurationEnum = 33

DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DurationEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Duration.PartTimeTemporary' THEN '5'
		WHEN 'Duration.PartTimeShortTerm' THEN '4'
		WHEN 'Duration.PartTimeSeasonal' THEN '8'
		WHEN 'Duration.PartTimeRegular' THEN '6'
		WHEN 'Duration.FullTimeTemporary' THEN '2'
		WHEN 'Duration.FullTimeShorTerm' THEN '1'
		WHEN 'Duration.FullTimeSeasonal' THEN '7'
		WHEN 'Duration.FullTimeRegular' THEN '3'
	END,
	@DurationEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Durations' AND [Key] IN ('Duration.FullTimeShorTerm', 'Duration.FullTimeTemporary', 'Duration.FullTimeRegular', 'Duration.PartTimeShortTerm', 'Duration.PartTimeTemporary', 'Duration.PartTimeRegular', 'Duration.FullTimeSeasonal', 'Duration.PartTimeSeasonal')

