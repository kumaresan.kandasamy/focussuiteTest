﻿DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep1.Notification.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep1.Notification.Text','Important: You will need the Federal Employer Identification (FEIN) for your business to create your #FOCUSTALENT# account.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.ConfirmCorrectEmployer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.ConfirmCorrectEmployer.Text','Next Step:  Based on the FEIN you entered ({0}), our records indicate that you are associated with an existing business.  Choose the appropriate action below.
<ul>
<li>If you entered the incorrect FEIN, please click "Previous Step" to edit.</li>
<li>If your FEIN is correct, choose the business you are associated with from the "Select Business" drop-down and click "Next Step" to enter your contact information.</li> 
<li>If your FEIN is correct but your business does not appear in the "Select Business" drop-down, choose "Add new business unit" to provide your business information.</li>
<li>If you need help completing your registration, contact our support team at #SUPPORTEMAIL# or #SUPPORTPHONE#. Our business hours are #SUPPORTHOURSOFBUSINESS#</li></ul>', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.BusinessUnitTradeName.TopDefault' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.BusinessUnitTradeName.TopDefault','- Select business - ', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.Company.TopDefault' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.Company.TopDefault','- select a business -', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.EmployerName.TopDefault' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.EmployerName.TopDefault','- Add new business unit -', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.Label','Doing Business As', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.NoOfEmployees.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.NoOfEmployees.Label','Number of Employees', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.CompanyDescription' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.CompanyDescription','Business Description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.AccountType.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.AccountType.Label','Business Type', 0, 12090)
end


DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.NoOfEmployees.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.NoOfEmployees.Label','Number of Employees', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnit.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnit.Label','Doing Business As', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.CompanyDescription.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.CompanyDescription.Label','Business description', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.AccountType.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.AccountType.Label','Business Type', 0, 12090)
end

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnitTradeName.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnitTradeName.Label','Select Business', 0, 12090)
end


