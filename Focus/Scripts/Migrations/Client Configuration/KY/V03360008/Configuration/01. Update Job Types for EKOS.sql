IF ISNULL((SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient'), '0') <> '3'
BEGIN
	RETURN
END


DECLARE @LookupType NVARCHAR(20)
DECLARE @CodeGroupId BIGINT
DECLARE @LocalisationId BIGINT

SET @LookupType = 'JobTypes'

DECLARE @Lookups TABLE
(
	[Key] NVARCHAR(50),
	DisplayOrder INT,
	Value NVARCHAR(255),
	ExternalId NVARCHAR(10)
)

INSERT INTO @Lookups ([Key], DisplayOrder, Value, ExternalId)
VALUES 
(@LookupType + '.Permanent', 1, 'Regular', '1'),
(@LookupType + '.Temporary', 2, 'Temporary', '2'),
(@LookupType + '.Interim', 3, 'Short Term', '3'),
(@LookupType + '.Seasonal', 4, 'Seasonal', '4')

IF NOT EXISTS(SELECT 1 FROM [Config.CodeGroup] WHERE [Key] = @LookupType)
BEGIN
	INSERT INTO [Config.CodeGroup] ([Key])
	VALUES (@LookupType)
END

INSERT INTO [Config.CodeItem] 
(
	[Key], 
	IsSystem,
	ExternalId
)
SELECT
	S.[Key],
	0,
	S.ExternalId
FROM
	@Lookups S
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeItem] CI
		WHERE
			CI.[Key] = S.[Key]
	)

UPDATE
	CI
SET
	ExternalId = S.ExternalId
FROM
	@Lookups S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
WHERE
	ISNULL(CI.ExternalId, '') <> S.ExternalId	

SELECT
	@CodeGroupId = Id
FROM 
	[Config.CodeGroup]
WHERE
	[Key] = @LookupType

INSERT INTO [Config.CodeGroupItem]
(
	DisplayOrder,
	CodeGroupId,
	CodeItemId
)
SELECT
	S.DisplayOrder,
	@CodeGroupId,
	CI.Id
FROM
	@Lookups S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeGroupItem] CGI
		WHERE
			CGI.CodeGroupId = @CodeGroupId
			AND CGI.CodeItemId = CI.Id
	)
	
UPDATE
	CGI
SET
	DisplayOrder = S.DisplayOrder
FROM
	@Lookups S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
INNER JOIN [Config.CodeGroupItem] CGI
	ON CGI.CodeGroupId = @CodeGroupId
	AND CGI.CodeItemId = CI.Id
WHERE
	CGI.DisplayOrder <> S.DisplayOrder
	
SELECT 
	@LocalisationId = L.Id
FROM 
	[Config.Localisation] L
WHERE 
	L.Culture = '**-**'

INSERT INTO [Config.LocalisationItem]
(
	[Key],
	Value,
	LocalisationId
)
SELECT
	S.[Key],
	S.Value,
	@LocalisationId
FROM 
	@Lookups S
WHERE
	NOT EXISTS
	(
		SELECT 
			1
		FROM
			[Config.LocalisationItem] LI
		WHERE
			LI.[Key] = S.[Key]
	)
	
UPDATE
	LI
SET
	Value = S.Value
FROM
	@Lookups S
INNER JOIN [Config.LocalisationItem] LI
	ON LI.[Key] = S.[Key]
WHERE
	LI.Value <> S.Value