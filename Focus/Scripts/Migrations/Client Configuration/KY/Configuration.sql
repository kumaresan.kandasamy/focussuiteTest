﻿DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ConfidentialEmployersDefault'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('ConfidentialEmployersDefault','True',1)


DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowBridgestoOppsInterest'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('ShowBridgestoOppsInterest','True',1)


 DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'EnableOptionalAlienRegExpiryDate'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('EnableOptionalAlienRegExpiryDate','True',1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'MandatoryHoursPerWeek'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('MandatoryHoursPerWeek','False',1) 

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'MandatoryHours'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('MandatoryHours','True',1) 

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'MandatoryJobType'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('MandatoryJobType','True',1) 

