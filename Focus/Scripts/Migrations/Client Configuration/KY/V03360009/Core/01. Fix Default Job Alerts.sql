UPDATE
	SS
SET
	SearchCriteria = REPLACE(SS.SearchCriteria, N'</SearchCriteria>', N'<ReferenceDocumentCriteria><DocumentType>Resume</DocumentType><DocumentId>' + CAST(R.Id AS NVARCHAR(30)) + N'</DocumentId></ReferenceDocumentCriteria></SearchCriteria>')
FROM
	[Data.Application.SavedSearch] SS
INNER JOIN [Data.Application.SavedSearchUser] SSU
	ON SSU.SavedSearchId = SS.Id
INNER JOIN [Data.Application.User] U
	ON U.Id = SSU.UserId
INNER JOIN [Data.Application.Resume] R
	ON R.PersonId = U.PersonId
	AND R.ResumeCompletionStatusId = 127
	AND R.StatusId = 1
	AND R.IsDefault = 1
WHERE
	SS.Name = N'Default job alert'
	AND SS.[Type] = 2
	AND SS.SearchCriteria LIKE N'%<DocumentsToSearch>Posting</DocumentsToSearch>%<MinimumStarMatch>ThreeStar</MinimumStarMatch>%'
	AND SS.SearchCriteria NOT LIKE N'%<ReferenceDocumentCriteria>%<DocumentType>Resume</DocumentType>%<DocumentId>%</DocumentId>%</ReferenceDocumentCriteria>%'
