﻿

DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.AccessEmployeeAccount.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.AccessEmployeeAccount.NoEdit','Access business account', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.ApprovePostingReferral.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.ApprovePostingReferral.NoEdit','Job post approved', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.AssignEmployerToStaff.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.AssignEmployerToStaff.NoEdit','Assign business to staff', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.AssignJobOrderToStaff.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.AssignJobOrderToStaff.NoEdit','Assign job posting to staff', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.BlockAllEmployerEmployeesAccount.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.BlockAllEmployerEmployeesAccount.NoEdit','Block all users for this FEIN', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.BlockUnblockJobSeeker.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.BlockUnblockJobSeeker.NoEdit','Block/unblock #CANDIDATETYPE# ', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.CloseJob.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.CloseJob.NoEdit','Job post closed', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.CreateJob.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.CreateJob.NoEdit','Job post drafted', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.EditCompanyAddress.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.EditCompanyAddress.NoEdit','Edit business contact infomation', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.EmailEmployee.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.EmailEmployee.NoEdit','Email business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.EmailEmployee.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.EmailEmployee.NoEdit','Email hiring manager/s', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.HoldJob.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.HoldJob.NoEdit','Job post on hold', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.MarkAsPreferredEmployer.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.MarkAsPreferredEmployer.NoEdit','Mark as preferred business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.PostJob.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.PostJob.NoEdit','Job post posted', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.ReactivateJob.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.ReactivateJob.NoEdit','Job post reactivated', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.RefreshJob.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.RefreshJob.NoEdit','Job post refreshed', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.SaveJob.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.SaveJob.NoEdit','Job post edited', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.UnblockAllEmployerEmployeesAccount.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.UnblockAllEmployerEmployeesAccount.NoEdit','Unblock all users for this business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.UnmarkAsPreferredEmployer.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.UnmarkAsPreferredEmployer.NoEdit','Unmark as preferred business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.ViewEmployerProfile.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.ViewEmployerProfile.NoEdit','View business profile', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActivityType.Employer.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActivityType.Employer.NoEdit','Business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='AsyncReportType.EmployerActivity.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('AsyncReportType.EmployerActivity.NoEdit','Business activity report', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='CandidateIssueType.NotRespondingToEmployerInvites.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('CandidateIssueType.NotRespondingToEmployerInvites.NoEdit','#CANDIDATETYPE# not responding to hiring manager invitations', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='DetailedDegreeLevels.CertificateOrAssociate.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('DetailedDegreeLevels.CertificateOrAssociate.NoEdit','Diploma or Associate', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='DistanceUnits.Kilometres.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('DistanceUnits.Kilometres.NoEdit','Kilometres', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='DistanceUnits.Miles.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('DistanceUnits.Miles.NoEdit','Miles', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.ApproveCandidateReferral' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.ApproveCandidateReferral','Approve job seeker referral request', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.ApproveEmployerAccountReferral' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.ApproveEmployerAccountReferral','Approve business account (delete referral)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.ContactEmployerAboutCandidate' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.ContactEmployerAboutCandidate','Contact hiring manager about candidate', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.DenyCandidateReferral' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.DenyCandidateReferral','Deny job seeker referral request', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.DenyEmployerAccountReferral' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.DenyEmployerAccountReferral','Deny business account (delete referral)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.DenyPostingReferral' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.DenyPostingReferral','Deny job posting (delete referral)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.EditPostingReferralConfirmation' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.EditPostingReferralConfirmation','Confirm job posting editing (delete referral)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.EmployeeReminder' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.EmployeeReminder','Company reminder', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.EmployerNotificationJobExpiration' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.EmployerNotificationJobExpiration','Notify hiring manager of closed job post', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.EmployerNotificationPendingJobExpiration' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.EmployerNotificationPendingJobExpiration','Notify hiring manager of pending job close', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.HiringFromTaxCreditProgramNotification' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.HiringFromTaxCreditProgramNotification','Notify staff for tax credit outreach', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='EmailTemplateTypes.HoldEmployerAccountReferral' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('EmailTemplateTypes.HoldEmployerAccountReferral','Confirm business account on-hold (delete referral)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ExplorerEducationLevels.Certificate.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ExplorerEducationLevels.Certificate.NoEdit','Diploma', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='FeedbackIssues.EmployerIssues' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('FeedbackIssues.EmployerIssues','Business issues', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.CareerExplorer.WebCareer.YourResume.MaxResumeCountReachedOne.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.CareerExplorer.WebCareer.YourResume.MaxResumeCountReachedOne.Text','You already have your resume loaded. Please delete your current resume if you wish to continue to create a new one.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.CareerExplorer.WebExplorer.Explore.StudyDegree.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.CareerExplorer.WebExplorer.Explore.StudyDegree.Label','Degrees and diplomas in demand', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.CareerExplorer.WebExplorer.Report.CertificateOrAssociate' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.CareerExplorer.WebExplorer.Report.CertificateOrAssociate','Diploma or Associate', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.CareerExplorer.WebExplorer.Report.TypeADegreeCertification.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.CareerExplorer.WebExplorer.Report.TypeADegreeCertification.Label','type a degree or diploma', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.AccessEmployeePopupBlocked.Error.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.AccessEmployeePopupBlocked.Error.Text','Unable to access business account as the popup was blocked. Please make an exception for this site in your popup blocker and try again.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.ApproveEmployeeReferral.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.ApproveEmployeeReferral.Title','Approve rejected business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.ApproveEmployeeReferralConfirmation.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.ApproveEmployeeReferralConfirmation.Body','Rejected business account has now been approved.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.ApproveEmployeeReferralConfirmation.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.ApproveEmployeeReferralConfirmation.Title','Rejected business approved', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.AssignEmployerConfirmation.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.AssignEmployerConfirmation.Body','The business was assigned to the staff member.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.AssignEmployerConfirmation.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.AssignEmployerConfirmation.Title','Business assigned to staff', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.BlockAllEmployerEmployeesAccount.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.BlockAllEmployerEmployeesAccount.Body','Please confirm you would like to block all accounts for this business.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.BlockAllEmployerEmployeesAccount.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.BlockAllEmployerEmployeesAccount.Title','Block all business accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.BlockAllEmployerEmployeesAccountConfirmation.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.BlockAllEmployerEmployeesAccountConfirmation.Body','All user accounts for the business have been blocked.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.EmployeeApproved.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.EmployeeApproved.Hint','Business approved', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.EmployeeAwaitingApproval' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.EmployeeAwaitingApproval','Business awaiting approval', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.EmployeeRejected.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.EmployeeRejected.Hint','Business rejected', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.EmployerParent.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.EmployerParent.Hint','Parent Business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.EmployerPreferred' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.EmployerPreferred','Business preferred', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.UnblockAllEmployerEmployeesAccount.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.UnblockAllEmployerEmployeesAccount.Body','Please confirm you would like to approve this rejected business.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.UnblockAllEmployerEmployeesAccount.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.UnblockAllEmployerEmployeesAccount.Body','Please confirm you would like to unblock all accounts for this business.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.UnblockAllEmployerEmployeesAccount.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.UnblockAllEmployerEmployeesAccount.Title','Unblock all business accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.Controls.FindEmployer.UnblockAllEmployerEmployeesAccountConfirmation.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.Controls.FindEmployer.UnblockAllEmployerEmployeesAccountConfirmation.Body','All user accounts for the business have been unblocked.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.EmployerProfile.ReturnToEmployerProfile' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.EmployerProfile.ReturnToEmployerProfile','return to business profile', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.EmployerProfile.ReturnToEmployerReferral' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.EmployerProfile.ReturnToEmployerReferral','return to business account request', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Assist.EmployerProfile.ReturnToEmployers' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Assist.EmployerProfile.ReturnToEmployers','return to Find an employer', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.BusinessUnitSelector.AddNewEmployer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.BusinessUnitSelector.AddNewEmployer.Text','Add new business location', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnit.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnit.Label','Select a company', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.AddressPostcodeRegexValidator.ErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.AddressPostcodeRegexValidator.ErrorMessage','ZIP code must be in a 5 or 9-digit format.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.ContactEmailAddress.RegExErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.ContactEmailAddress.RegExErrorMessage','Email address format is invalid.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.ContactEmailAddress.RequiredErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.ContactEmailAddress.RequiredErrorMessage','Email address is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.ContactPhone.RequiredErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.ContactPhone.RequiredErrorMessage','A valid phone number is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.FirstName.RequiredErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.FirstName.RequiredErrorMessage','First name is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.JobTitle.RequiredErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.JobTitle.RequiredErrorMessage','Job title is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.LastName.RequiredErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.LastName.RequiredErrorMessage','Last name is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.PersonalTitle.RequiredErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.PersonalTitle.RequiredErrorMessage','Title is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.AddressPostcodeRegexValidator.ErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.AddressPostcodeRegexValidator.ErrorMessage','ZIP code must be in a 5 or 9-digit format.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.CompanyDescription' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.CompanyDescription','Company description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.Label','Company name (DBA)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.Label','Doing business as', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.RequiredErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.RequiredErrorMessage','Business name is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.NoOfEmployees.RequiredErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.NoOfEmployees.RequiredErrorMessage','Number of employees is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditCompanyModal.StateEmployerIdentificationNumber.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditCompanyModal.StateEmployerIdentificationNumber.Label','SEIN', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmailEmployeeModal.EmailEmployeeConfirmation.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmailEmployeeModal.EmailEmployeeConfirmation.Body','Thank you. We have sent the email to the business you selected.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmailEmployeeModal.EmailEmployeesConfirmation.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmailEmployeeModal.EmailEmployeesConfirmation.Body','Thank you. We have sent the email to the businesses you selected.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmailEmployeeModal.EmailEmployeesConfirmation.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmailEmployeeModal.EmailEmployeesConfirmation.Title','Email businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmailEmployeeModal.Title.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmailEmployeeModal.Title.Text','Email businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistration.ProgressBar.Step2And3.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistration.ProgressBar.Step2And3.Text','Business Information', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistration.ProgressBar.Step4.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistration.ProgressBar.Step4.Text','Hiring Contact Information', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep1.Notification.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep1.Notification.Text','Please note: you''ll need your company''s Federal Employer Identification Number (FEIN) to register for #FOCUSTALENT#', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnit.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnit.Label','Company name (DBA)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnitTradeName.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnitTradeName.Label','Select Company', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.CompanyDescription.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.CompanyDescription.Label','Company description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.ConfirmCorrectEmployer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.ConfirmCorrectEmployer.Text','Please use the drop down below to select an existing company already linked to the Federal Employer Identification Number that you have entered ({0}) or select “New company” to enter your company details.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.StateEmployerIdentificationNumber.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.StateEmployerIdentificationNumber.Label','SEIN', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep3.CompanyDescription' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep3.CompanyDescription','Business description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep3.CompanyName.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep3.CompanyName.Label','Doing business as', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep3.EmployerWasFound.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep3.EmployerWasFound.Label','Based on the FEIN that you have entered ({0}), our records indicate that neither your company nor any of its business units or hiring managers have registered with our system previously.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep5.AssistTermsOfUse.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep5.AssistTermsOfUse.Label','I have informed the company of these terms of use.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.JobActivityList.CreateJob.ActionDescription' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.JobActivityList.CreateJob.ActionDescription','Job drafted', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.JobActivityList.PostJob.ActionDescription' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.JobActivityList.PostJob.ActionDescription','Job posted', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ContactEmployer.ContactInfo.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ContactEmployer.ContactInfo.Label','Contact business about {0} {1}', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ContactEmployer.EmailEmployeeConfirmation.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ContactEmployer.EmailEmployeeConfirmation.Title','Contact business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ContactEmployer.Page.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ContactEmployer.Page.Title','Email businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ActivityAssignment.ActivityBackDateTo.ErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ActivityAssignment.ActivityBackDateTo.ErrorMessage','Activity date cannot be earlier than {0} days.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.AllJobs.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.AllJobs.Text.NoEdit','All job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.CommissionOnly.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.CommissionOnly.Text.NoEdit','Commission only job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.CourtOrderedAffirmativeActionJobs.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.CourtOrderedAffirmativeActionJobs.Text.NoEdit','Court-ordered affirmative action job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.FederalContractorJobs.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.FederalContractorJobs.Text.NoEdit','Federal contractor job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsH2A.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsH2A.Text.NoEdit','Foreign labor (H2A-ag) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsH2B.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsH2B.Text.NoEdit','Foreign labor (H2B-non-ag) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsOther.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsOther.Text.NoEdit','Foreign labor (other) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.SalaryAndCommission.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.SalaryAndCommission.Text.NoEdit','Salary + commission-based job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistActivities.Enabled.ColumnTitle' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistActivities.Enabled.ColumnTitle','Activity on / off', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.','Enable these job seeker flags', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.DarkColour.Required' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.DarkColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagContactNegativeFeedback.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagContactNegativeFeedback.Label1','Flag job post when hiring manager responds negatively to the customer service questions', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagContactPositiveFeedback.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagContactPositiveFeedback.Label1','Flag job post when hiring manager responds positively to the customer service questions', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagDefaults.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagDefaults.Label','Job seeker issue defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagJobClosedEarly.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagJobClosedEarly.Label1','Flag job post in hiring manager closes the job', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagJobClosedEarly.Label2' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagJobClosedEarly.Label2','days before job is set to expire', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagJobClosingDateRefreshed.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagJobClosingDateRefreshed.Label1','Flag job post if hiring manager refreshes the closing date', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagLowMatches.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagLowMatches.Label','Low number of high-quality matches', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagLowMatches.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagLowMatches.Label1','Flag job post if fewer than', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagLowReferrals.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagLowReferrals.Label1','Flag job post if fewer than ', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagNotClickingReferrals.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagNotClickingReferrals.Label1','Flag job post if hiring manager has not clicked on referred applicants after', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagPostHireFollowUp.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagPostHireFollowUp.Label1','Hire reported by hiring manager, job seeker or staff', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagPotentialMSFWLabel' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagPotentialMSFWLabel','Job seeker is potential MSFW', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSearchWithoutInvite.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSearchWithoutInvite.Label1','Flag job post if hiring manager performs', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotClickingLeads.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotClickingLeads.Label','Job seeker not clicking on leads', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotLoggedOn.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotLoggedOn.Label','Job seeker not signing in', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotReportingInterviews.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotReportingInterviews.Label','Job seeker not reporting for interviews', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotReportingInterviews.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotReportingInterviews.Label1','Flag if job seeker did not report for', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label','Job seeker not responding to hiring manager invitations', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label','Seeker not responding to hiring manager invitations', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label1','Flag if job seeker did not respond to', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label2' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label2','or more invitations in', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotSearchingJobs.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotSearchingJobs.Label','Job seeker not searching for jobs', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerRejectingOffers.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerRejectingOffers.Label','Job seeker rejecting job offers', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.GivingPositiveFeedback.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.GivingPositiveFeedback.Label1','Flag when job seeker responds positively to the customer service questions', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.JobOrderDefaults.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.JobOrderDefaults.Label','Job post issue defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.JobOrderDefaults.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.JobOrderDefaults.Label','Job posting issue defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.LaborInsightsDefaults.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.LaborInsightsDefaults.Label','Staff access to Labor Insight', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.LightColour.Required' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.LightColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.NegativeSurvey.Label1' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.NegativeSurvey.Label1','Flag when job seeker responds negatively to the customer service questions', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.RecenPlacementsFlag.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.RecenPlacementsFlag.Label','Recent placements to matched applicants', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.Stars.MinimumScore' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.Stars.MinimumScore','Job seeker matching job post with a minimum score of', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CandidateSearchResultList.StudyProgram.ColumnTitle' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CandidateSearchResultList.StudyProgram.ColumnTitle','Program of Study', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CareerDefaults.DarkColour.Required' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CareerDefaults.DarkColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CareerDefaults.LightColour.Required' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CareerDefaults.LightColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CareerDefaults.ResumesSearchable.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CareerDefaults.ResumesSearchable.Label','Resumes searchable by interested businesses?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CreateAnnouncement.Audience.Employers.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CreateAnnouncement.Audience.Employers.Label','Hiring managers', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.DashboardReferralsReport.JobPostings.Header.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.DashboardReferralsReport.JobPostings.Header.NoEdit','Job posts pending approval', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.DashboardReferralsReport.NewEmployerAccounts.Header.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.DashboardReferralsReport.NewEmployerAccounts.Header.NoEdit','Business account requests', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.DashboardReferralsReport.NewEmployerAccounts.Header.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.DashboardReferralsReport.NewEmployerAccounts.Header.NoEdit','Business accounts pending approval', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.DashboardReferralsReport.ReferralRequests.Header.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.DashboardReferralsReport.ReferralRequests.Header.NoEdit','Referral requests pending approval', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.DenyEmployerReferralModal.DenyAndSendMessageButton.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.DenyEmployerReferralModal.DenyAndSendMessageButton.Text','Email business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.ContactNameHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.ContactNameHeader.Text','Hiring contact name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployeeApproved.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployeeApproved.Hint','Business approved', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployeeAwaitingApproval' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployeeAwaitingApproval','Business pending approval', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployeeRejected.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployeeRejected.Hint','Business denied', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerName.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerName.Label','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerNameHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerNameHeader.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerParent.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerParent.Hint','Parent business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerParent.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerParent.Hint','Parent company', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerPreferred' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerPreferred','Preferred business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.CreateEmployerButton.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.CreateEmployerButton.Text','Create new business & job posting', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeApproved.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeApproved.Hint','Business approved', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeAwaitingApproval' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeAwaitingApproval','Business pending approval', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeRejected.Hint' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeRejected.Hint','Business denied', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployeesListValidator.ErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployeesListValidator.ErrorMessage','You must select a business record to run this action.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployerName.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployerName.Label','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployerNameHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployerNameHeader.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.HoldEmployerReferralModal.DenyAndSendMessageButton.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.HoldEmployerReferralModal.DenyAndSendMessageButton.Text','Email business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.JobIssueResolutionModal.CheckBoxListValidationError.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.JobIssueResolutionModal.CheckBoxListValidationError.Text','Please select at least one issue to resolve.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.JobReferralSummaryList.ExperienceHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.JobReferralSummaryList.ExperienceHeader.Text','Years of Experience', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.RegisterJobSeeker.DateOfBirthMax.ErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.RegisterJobSeeker.DateOfBirthMax.ErrorMessage','You may not be more than 100 years of age.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ReplaceParentModal.' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ReplaceParentModal.','Select which linked business you wish to replace the existing parent business with:', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TabNavigation.TabEmployers.Label.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TabNavigation.TabEmployers.Label.NoEdit','Assist businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.ActivationYellowWordFiltering.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.ActivationYellowWordFiltering.Label','Activate yellow-word filtering for job postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.DarkColour.Required' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.DarkColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.JobOrderSettings.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.JobOrderSettings.Label','Job post settings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.LightColour.Required' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.LightColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.NewBusinessUnitAccount.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.NewBusinessUnitAccount.Label','New business unit accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerApproval.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerApproval.Label','New company accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerCensorshipFiltering.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerCensorshipFiltering.Label','Activate business email filtering', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.NewHiringManagerAccount.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.NewHiringManagerAccount.Label','New hiring manager accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.PreferenceDefaultsEmployer.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.PreferenceDefaultsEmployer.Label','Hiring manager preferences override system defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.PreferenceDefaultsSystem.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.PreferenceDefaultsSystem.Label','System defaults override hiring manager preferences', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.ResumeReferralApprovals.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.ResumeReferralApprovals.Label','Screening preference defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.ScreeningPrefrencesDefault.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.ScreeningPrefrencesDefault.Label','Screening preference default', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.SharingDefaultsEmployerUsers.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.SharingDefaultsEmployerUsers.Label','Hiring managers at the same FEIN may share job postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.WelcomeToAssistModal.OfficeConfirmation.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.WelcomeToAssistModal.OfficeConfirmation.Text','Please confirm the office where you will be working.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmailAlerts.EmployerAccountRequests.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmailAlerts.EmployerAccountRequests.Text','Business Account Requests', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmailAlerts.NewJobPostings.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmailAlerts.NewJobPostings.Text','Job Postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmailAlerts.NewReferralRequests.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmailAlerts.NewReferralRequests.Text','Referral Requests', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.CompanyInformation.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.CompanyInformation.Label','Business Information', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.CompanyNameHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.CompanyNameHeader.Text','BUSINESS NAME', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.EmployeeNameHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.EmployeeNameHeader.Text','HIRING MGR. NAME', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.EmployerProfile' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.EmployerProfile','Business profile', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.JobSeekersPlacedAtThisEmployer.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.JobSeekersPlacedAtThisEmployer.Label','#CANDIDATETYPES# Placed at this Business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.LinkedCompanies.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.LinkedCompanies.Label','Linked Businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.NoOfJobOrdersEditedByStaff' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.NoOfJobOrdersEditedByStaff','Number of job posts edited by staff', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.NoOfJobOrdersOpen' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.NoOfJobOrdersOpen','Number of job posts open', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.NoOfJobSeekersHired' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.NoOfJobSeekersHired','Number of job seekers hired', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.NoOfJobSeekersInterviewed' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.NoOfJobSeekersInterviewed','Number of job seekers interviewed', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.NoOfJobSeekersRejected' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.NoOfJobSeekersRejected','Number of job seekers rejected', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.NoOfTalentLogins7Days' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.NoOfTalentLogins7Days','Number of #FOCUSTALENT# sign-ins for all hiring managers in the past 7 days', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.ReturnToEmployerProfile' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.ReturnToEmployerProfile','return to business profile', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerProfile.ReturnToJobOrderDashboard' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerProfile.ReturnToJobOrderDashboard','return to Job post dashboard', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.ApproveButton.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.ApproveButton.Text','Approve business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.CompanyInformation.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.CompanyInformation.Label','Business information', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.DenyButton.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.DenyButton.Text','Deny business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.Employee.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.Employee.Label','Hiring contact', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.EmployerNameTitleLiteral.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.EmployerNameTitleLiteral.Text','Business name:', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.EmployerNotesAndReminders.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.EmployerNotesAndReminders.Label','Business notes and reminders', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.EmployerProfileLink.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.EmployerProfileLink.Text','View business profile', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.EmployerReferral.Header' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.EmployerReferral.Header','Approve business account request', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.EmployerReferral.Header' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.EmployerReferral.Header','Approve business account request:', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.Page.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.Page.Title','Approve business account request', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.ReferralApprovedConfirmation.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.ReferralApprovedConfirmation.Body','The employer account has been approved.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferral.ReferralApprovedConfirmation.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferral.ReferralApprovedConfirmation.Title','Employer account approved', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferrals.ContactName.ColumnTitle' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferrals.ContactName.ColumnTitle','Hiring contact name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferrals.EmployerName.ColumnTitle' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferrals.EmployerName.ColumnTitle','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferrals.EmployerReferrals.Header' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferrals.EmployerReferrals.Header','Approve business account requests', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.EmployerReferrals.Page.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.EmployerReferrals.Page.Title','Approve business account requests', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Employers.CreateNewEmployerButton.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Employers.CreateNewEmployerButton.Text','Create new business account', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Employers.FindAnEmployer.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Employers.FindAnEmployer.Title','Find a business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Employers.Page.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Employers.Page.Title','Assist Businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.HiringManagerProfile.AccessEmployersAccount.Label.' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.HiringManagerProfile.AccessEmployersAccount.Label.','Access business''s #FOCUSTALENT# account', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.HiringManagerProfile.ReturnToEmployerProfile' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.HiringManagerProfile.ReturnToEmployerProfile','return to Business profile', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.AccessEmployersAccount' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.AccessEmployersAccount','Access hiring manager''s account', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.ActivitySummary' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.ActivitySummary','Job Post Activity Summary', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.Description' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.Description','Job Post Description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.EmailEmployer' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.EmailEmployer','Email hiring manager', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.EmployerEmail.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.EmployerEmail.Label','Hiring manager email', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.EmployerInvitationsSent.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.EmployerInvitationsSent.Label','Hiring manager invitations sent', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.EmployerTelephone.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.EmployerTelephone.Label','Hiring manager telephone', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.JobOrder.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.JobOrder.Label','Job post', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.JobStatus.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.JobStatus.Label','Job status', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.JobView.ReturnToJobOrderDashboard' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.JobView.ReturnToJobOrderDashboard','return to Job post dashboard', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.CreateJobOrder.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.CreateJobOrder.Label','Create a job posting', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.CreateNewEmployerAndJobOrderButton.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.CreateNewEmployerAndJobOrderButton.Text.NoEdit','Create new business & job posting', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.EmployerName.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.EmployerName.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.EmployerNameHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.EmployerNameHeader.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.FindAJobOrder.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.FindAJobOrder.Label','Find a job posting', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.FindAnEmployer.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.FindAnEmployer.Label','Find a business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.JobOrdersFilterDropDown.Default.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.JobOrdersFilterDropDown.Default.NoEdit','All job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.Page.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.Page.Title','Job Post Dashboard', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobSeekers.DateOfBirthMax.ErrorMessage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobSeekers.DateOfBirthMax.ErrorMessage','You may not be more than 100 years of age.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ManageStaffAccounts.EmployerAccounts.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ManageStaffAccounts.EmployerAccounts.Label','Business accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ManageStaffAccounts.EmployerReportsCheckBox.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ManageStaffAccounts.EmployerReportsCheckBox.Label','Business/hiring manager reports', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ManageStaffAccounts.ManageEmployerAccountsLabel.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ManageStaffAccounts.ManageEmployerAccountsLabel.Text','Manage business accounts & job postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ManageStaffAccounts.ManageFocus.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ManageStaffAccounts.ManageFocus.Label','Manage staff', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ManageStaffAccounts.ManageJobSeekersAccountBlocker.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ManageStaffAccounts.ManageJobSeekersAccountBlocker.Label','Block & unblock accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.ManageStaffAccounts.ManageReports.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.ManageStaffAccounts.ManageReports.Label','Manage reports', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.MessageEmployers.Column1Header.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.MessageEmployers.Column1Header.Employer.Text','Which businesses are you looking for?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.MessageEmployers.Column2Header.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.MessageEmployers.Column2Header.Employer.Text','View the business activity', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.MessageEmployers.Column3Header.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.MessageEmployers.Column3Header.Employer.Text','Where are the businesses located and what is the overall time period for this search?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.MessageEmployers.Employers.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.MessageEmployers.Employers.Title','Businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Pool.MatchesForThisJob.Label:Education' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Pool.MatchesForThisJob.Label:Education','Program of study matches', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.PostingReferrals.EmployerName.ColumnTitle' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.PostingReferrals.EmployerName.ColumnTitle','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaCompliance.ForiegnLabourCertificationMatchesHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaCompliance.ForiegnLabourCertificationMatchesHeader.Text','Other compliance conditions', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaEmployerActivity.CriteriaEmployerActivityLabel.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaEmployerActivity.CriteriaEmployerActivityLabel.Text','Business activity', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics.CriteriaEmployerCharacteristicsLabel.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics.CriteriaEmployerCharacteristicsLabel.Text','Business characteristics', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics.DirectEmployer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics.DirectEmployer.Text','Direct Business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics.EmployersHeader.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics.EmployersHeader.Text','Businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaJobCharacteristics.SearchResumeContextCheckBoxList.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaJobCharacteristics.SearchResumeContextCheckBoxList.Employer.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaJobSeekerActivitySummary.SurveysCheckBox.SurveyReceivedInvitations.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaJobSeekerActivitySummary.SurveysCheckBox.SurveyReceivedInvitations.Text','Received invitations to apply', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaKeywordAndContext.SearchResumeContextCheckBoxList.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaKeywordAndContext.SearchResumeContextCheckBoxList.Employer.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaSelector.Column1Header.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaSelector.Column1Header.Employer.Text','Which businesses are you looking for?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaSelector.Column1Header.JobOrder.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaSelector.Column1Header.JobOrder.Text','Which job posts are you looking for?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaSelector.Column2Header.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaSelector.Column2Header.Employer.Text','View the business activity', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaSelector.Column2Header.JobOrder.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaSelector.Column2Header.JobOrder.Text','How are the job postings progressing?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaSelector.Column3Header.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaSelector.Column3Header.Employer.Text','Where are the businesses located and what''s the overall time period for this report?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaSelector.Column3Header.JobOrder.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaSelector.Column3Header.JobOrder.Text','Where are the job postings located and what''s the overall time period for this report?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaSelector.CriteriaHeader.Employer.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaSelector.CriteriaHeader.Employer.Text','Report on businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaSelector.CriteriaHeader.JobOrder.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaSelector.CriteriaHeader.JobOrder.Text','Report on job postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.ReportSelector.EmployerReportButton.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.ReportSelector.EmployerReportButton.Text.NoEdit','Businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.ReportSelector.JobOrderReportButton.Text.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.ReportSelector.JobOrderReportButton.Text.NoEdit','Job postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.ReportSelector.WhatDoYouWantLiteral.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.ReportSelector.WhatDoYouWantLiteral.Text','What type of report would you like?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebTalent.ManageCompany.AssignNewBusinessUnitButton.Text' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebTalent.ManageCompany.AssignNewBusinessUnitButton.Text','Add new or existing business location', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebTalent.ManageCompany.BusinessUnit.Label' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebTalent.ManageCompany.BusinessUnit.Label','Select Company name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.AllEmployers.TopDefault.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.AllEmployers.TopDefault.NoEdit','All businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.AssistEmployerReferrals.LinkText' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.AssistEmployerReferrals.LinkText','Approve business account requests', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.AssistEmployers.LinkText' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.AssistEmployers.LinkText','Find a business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.BusinessUnitTradeName.TopDefault' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.BusinessUnitTradeName.TopDefault','- Select Company - ', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.Company.TopDefault' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.Company.TopDefault','- select a company -', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.EmployerName.TopDefault' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.EmployerName.TopDefault','- New company -', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.JobOrderDashboard.LinkText' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.JobOrderDashboard.LinkText','Job Post Dashboard', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.ManageOffices.LinkText' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.ManageOffices.LinkText','Manage offices', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.ManageSeekerAccounts' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.ManageSeekerAccounts','Manage job seeker accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.RequestPassword.Text:Education' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.RequestPassword.Text:Education','Request Password/PIN', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobDenialReasons.JobDuplicate' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobDenialReasons.JobDuplicate','Duplicate job posting was created in error', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobDenialReasons.JobLanguage' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobDenialReasons.JobLanguage','Job posting contains discriminatory language', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobDenialReasons.JobRequired' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobDenialReasons.JobRequired','Job posting requires an up-front fee to be paid to the employer', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobDenialReasons.JobVerify' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobDenialReasons.JobVerify','Unable to verify job location address', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobDenialReasons.OtherReason' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobDenialReasons.OtherReason','Other', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.CourtOrderedAffirmativeActionJobs.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.CourtOrderedAffirmativeActionJobs.NoEdit','Court-ordered affirmative action job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.FederalContractorJobs.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.FederalContractorJobs.NoEdit','Federal contactor job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobs.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobs.NoEdit','Foreign labor job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobsAll.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobsAll.NoEdit','Foreign labor (All) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobsH2A.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobsH2A.NoEdit','Foreign labor (H2A-ag) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobsH2B.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobsH2B.NoEdit','Foreign labor (H2B-non-ag) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobsOther.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobsOther.NoEdit','Foreign labor (Other) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.NewJobs.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.NewJobs.NoEdit','New job posts (in last 3 days)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.None.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.None.NoEdit','All job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobStatuses.Active.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobStatuses.Active.NoEdit','Active job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobStatuses.Closed.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobStatuses.Closed.NoEdit','Closed job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobStatuses.Draft.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobStatuses.Draft.NoEdit','Draft job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobStatuses.OnHold.NoEdit' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobStatuses.OnHold.NoEdit','On hold job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='MSFWQuestions.HalfFromFarmWork' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('MSFWQuestions.HalfFromFarmWork','At least half of my income or my family?s income was from farm work', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='SuccessfulSubmitValidFEIN.Body' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('SuccessfulSubmitValidFEIN.Body','Since this registration does not require additional staff review, it will not go to the Approve Business Account queue. You may post jobs immediately on the businesses behalf.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='SuccessfulSubmitValidFEIN.Title' 
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('SuccessfulSubmitValidFEIN.Title','Thank you for registering this business with #FOCUSTALENT#', 0, 12090)
end
