﻿DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebTalent.ManageCompany.BusinessUnit.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebTalent.ManageCompany.BusinessUnit.Label','Select Company name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.Label','Company name (DBA)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.CareerExplorer.WebCareer.YourResume.MaxResumeCountReachedOne.Text'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.CareerExplorer.WebCareer.YourResume.MaxResumeCountReachedOne.Text','You already have your resume loaded. Please delete your current resume if you wish to continue to create a new one.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep1.Notification.Text'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep1.Notification.Text','Please note: you''ll need your company''s Federal Employer Identification Number (FEIN) to register for #FOCUSTALENT#', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep3.EmployerWasFound.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep3.EmployerWasFound.Label','Based on the FEIN that you have entered ({0}), our records indicate that neither your company nor any of its business units or hiring managers have registered with our system previously.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistration.ProgressBar.Step2And3.Text'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistration.ProgressBar.Step2And3.Text','Business Information', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep3.CompanyName.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep3.CompanyName.Label','Doing business as', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep5.AssistTermsOfUse.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep5.AssistTermsOfUse.Label','I have informed the company of these terms of use.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebTalent.ManageCompany.AssignNewBusinessUnitButton.Text'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebTalent.ManageCompany.AssignNewBusinessUnitButton.Text','Add new or existing business location', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnit.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.BusinessUnitSelector.BusinessUnit.Label','Select a company', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.Company.TopDefault'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.Company.TopDefault','- select a company -', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.BusinessUnitSelector.AddNewEmployer.Text'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.BusinessUnitSelector.AddNewEmployer.Text','Add new business location', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.CompanyDescription'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.CompanyDescription','Company description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnitTradeName.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnitTradeName.Label','Select Company', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnit.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.BusinessUnit.Label','Company name (DBA)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.BusinessUnitTradeName.TopDefault'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.BusinessUnitTradeName.TopDefault','- Select Company - ', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.CompanyDescription.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.CompanyDescription.Label','Company description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep2.ConfirmCorrectEmployer.Text'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep2.ConfirmCorrectEmployer.Text','Please use the drop down below to select an existing company already linked to the Federal Employer Identification Number that you have entered ({0}) or select “New company” to enter your company details.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.RequiredErrorMessage'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.EmployerName.RequiredErrorMessage','Business name is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.AccessEmployeeAccount.NoEdit'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.AccessEmployeeAccount.NoEdit','Access business account', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistration.ProgressBar.Step4.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistration.ProgressBar.Step4.Text','Hiring Contact Information', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EmployerRegistrationStep3.CompanyDescription'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EmployerRegistrationStep3.CompanyDescription','Business description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.DashboardReferralsReport.NewEmployerAccounts.Header.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.DashboardReferralsReport.NewEmployerAccounts.Header.NoEdit','Business account requests', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.CreateNewEmployerAndJobOrderButton.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.CreateNewEmployerAndJobOrderButton.Text.NoEdit','Create new business & job posting', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.FindAJobOrder.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.FindAJobOrder.Label','Find a job posting', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.EmployerName.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.EmployerName.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.EmployerNameHeader.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.EmployerNameHeader.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployerName.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployerName.Label','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.CreateJobOrder.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.CreateJobOrder.Label','Create a job posting', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.JobOrderDashboard.FindAnEmployer.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.JobOrderDashboard.FindAnEmployer.Label','Find a business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerName.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerName.Label','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.AssignJobOrderToStaff.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.AssignJobOrderToStaff.NoEdit','Assign job posting to staff', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActionTypes.EmailEmployee.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActionTypes.EmailEmployee.NoEdit','Email hiring manager/s', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.JobView.ReturnToJobOrderDashboard'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.JobView.ReturnToJobOrderDashboard','return to Job post dashboard', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.JobOrder.Label'
begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.JobOrder.Label','Job post', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.ActivitySummary'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.ActivitySummary','Job Post Activity Summary', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.Description'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.Description','Job Post Description', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.AccessEmployersAccount'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.AccessEmployersAccount','Access hiring manager''s account', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.EmailEmployer'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.EmailEmployer','Email hiring manager', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.EmployerInvitationsSent.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.EmployerInvitationsSent.Label','Hiring manager invitations sent', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.EmployerTelephone.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.EmployerTelephone.Label','Hiring manager telephone', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Job.EmployerEmail.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Job.EmployerEmail.Label','Hiring manager email', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerNameHeader.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerNameHeader.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.ContactNameHeader.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.ContactNameHeader.Text','Hiring contact name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeApproved.Hint'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeApproved.Hint','Business approved', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeAwaitingApproval'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeAwaitingApproval','Business pending approval', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='AsyncReportType.EmployerActivity.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('AsyncReportType.EmployerActivity.NoEdit','Business activity report', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.NoOfEmployees.RequiredErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.NoOfEmployees.RequiredErrorMessage','Number of employees is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployeeApproved.Hint'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployeeApproved.Hint','Business approved', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployeeAwaitingApproval'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployeeAwaitingApproval','Business pending approval', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeRejected.Hint'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployeeRejected.Hint','Business denied', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployeesListValidator.ErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployeesListValidator.ErrorMessage','You must select a business record to run this action.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployeeRejected.Hint'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployeeRejected.Hint','Business denied', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerParent.Hint'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerParent.Hint','Parent company', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.AddressPostcodeRegexValidator.ErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.AddressPostcodeRegexValidator.ErrorMessage','ZIP code must be in a 5 or 9-digit format.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.ContactEmailAddress.RegExErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.ContactEmailAddress.RegExErrorMessage','Email address format is invalid.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.ContactEmailAddress.RequiredErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.ContactEmailAddress.RequiredErrorMessage','Email address is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.ContactPhone.RequiredErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.ContactPhone.RequiredErrorMessage','A valid phone number is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.FirstName.RequiredErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.FirstName.RequiredErrorMessage','First name is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.JobTitle.RequiredErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.JobTitle.RequiredErrorMessage','Job title is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.LastName.RequiredErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.LastName.RequiredErrorMessage','Last name is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.ContactInformation.PersonalTitle.RequiredErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.ContactInformation.PersonalTitle.RequiredErrorMessage','Title is required.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditBusinessUnit.AddressPostcodeRegexValidator.ErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditBusinessUnit.AddressPostcodeRegexValidator.ErrorMessage','ZIP code must be in a 5 or 9-digit format.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ActivityAssignment.ActivityBackDateTo.ErrorMessage'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ActivityAssignment.ActivityBackDateTo.ErrorMessage','Activity date cannot be earlier than {0} days.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindEmployer.EmployerPreferred'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindEmployer.EmployerPreferred','Preferred business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.JobIssueResolutionModal.CheckBoxListValidationError.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.JobIssueResolutionModal.CheckBoxListValidationError.Text','Please select at least one issue to resolve.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.AssistEmployerReferrals.LinkText'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.AssistEmployerReferrals.LinkText','Approve business account requests', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.PreferenceDefaultsEmployer.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.PreferenceDefaultsEmployer.Label','Hiring manager preferences override system defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.PreferenceDefaultsSystem.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.PreferenceDefaultsSystem.Label','System defaults override hiring manager preferences', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerApproval.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerApproval.Label','New company accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.NewBusinessUnitAccount.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.NewBusinessUnitAccount.Label','New business unit accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.NewHiringManagerAccount.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.NewHiringManagerAccount.Label','New hiring manager accounts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerCensorshipFiltering.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerCensorshipFiltering.Label','Activate business email filtering', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.ActivationYellowWordFiltering.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.ActivationYellowWordFiltering.Label','Activate yellow-word filtering for job postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.JobOrderSettings.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.JobOrderSettings.Label','Job post settings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.SharingDefaultsEmployerUsers.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.SharingDefaultsEmployerUsers.Label','Hiring managers at the same FEIN may share job postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.ResumeReferralApprovals.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.ResumeReferralApprovals.Label','Screening preference defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistActivities.Enabled.ColumnTitle'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistActivities.Enabled.ColumnTitle','Activity on / off', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.ScreeningPrefrencesDefault.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.ScreeningPrefrencesDefault.Label','Screening preference default', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='ActivityType.Employer.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('ActivityType.Employer.NoEdit','Business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CareerDefaults.ResumesSearchable.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CareerDefaults.ResumesSearchable.Label','Resumes searchable by interested businesses?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.DarkColour.Required'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.DarkColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.DarkColour.Required'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.DarkColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.TalentDefaults.LightColour.Required'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.TalentDefaults.LightColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.LightColour.Required'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.LightColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CareerDefaults.DarkColour.Required'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CareerDefaults.DarkColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CareerDefaults.LightColour.Required'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CareerDefaults.LightColour.Required','Please select a color', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagLowMatches.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagLowMatches.Label','Low number of high-quality matches', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label','Seeker not responding to hiring manager invitations', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label2'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagSeekerNotRespondingInvites.Label2','or more invitations in', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.LaborInsightsDefaults.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.LaborInsightsDefaults.Label','Staff access to Labor Insight', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.FlagDefaults.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.FlagDefaults.Label','Job seeker issue defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.JobOrderDefaults.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.JobOrderDefaults.Label','Job posting issue defaults', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.RecenPlacementsFlag.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.RecenPlacementsFlag.Label','Recent placements to matched applicants', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.AssistDefaults.Stars.MinimumScore'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.AssistDefaults.Stars.MinimumScore','Job seeker matching job post with a minimum score of', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.CreateAnnouncement.Audience.Employers.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.CreateAnnouncement.Audience.Employers.Label','Hiring managers', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobStatuses.Active.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobStatuses.Active.NoEdit','Active job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobStatuses.Closed.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobStatuses.Closed.NoEdit','Closed job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobStatuses.Draft.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobStatuses.Draft.NoEdit','Draft job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobStatuses.OnHold.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobStatuses.OnHold.NoEdit','On hold job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobsAll.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobsAll.NoEdit','Foreign labor (All) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.CourtOrderedAffirmativeActionJobs.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.CourtOrderedAffirmativeActionJobs.NoEdit','Court-ordered affirmative action job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.FederalContractorJobs.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.FederalContractorJobs.NoEdit','Federal contactor job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobs.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobs.NoEdit','Foreign labor job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.NewJobs.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.NewJobs.NoEdit','New job posts (in last 3 days)', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.WelcomeToAssistModal.OfficeConfirmation.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.WelcomeToAssistModal.OfficeConfirmation.Text','Please confirm the office where you will be working.', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.ReportSelector.JobOrderReportButton.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.ReportSelector.JobOrderReportButton.Text.NoEdit','Job postings', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.ReportSelector.EmployerReportButton.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.ReportSelector.EmployerReportButton.Text.NoEdit','Businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.ReportSelector.WhatDoYouWantLiteral.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.ReportSelector.WhatDoYouWantLiteral.Text','What type of report would you like?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobsH2A.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobsH2A.NoEdit','Foreign labor (H2A-ag) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsH2A.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsH2A.Text.NoEdit','Foreign labor (H2A-ag) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.CommissionOnly.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.CommissionOnly.Text.NoEdit','Commission only job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.CourtOrderedAffirmativeActionJobs.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.CourtOrderedAffirmativeActionJobs.Text.NoEdit','Court-ordered affirmative action job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.FederalContractorJobs.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.FederalContractorJobs.Text.NoEdit','Federal contractor job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsH2B.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsH2B.Text.NoEdit','Foreign labor (H2B-non-ag) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsOther.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.ForeignLaborJobsOther.Text.NoEdit','Foreign labor (other) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.SalaryAndCommission.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.SalaryAndCommission.Text.NoEdit','Salary + commission-based job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.None.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.None.NoEdit','All job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.ApprovalQueueFilter.AllJobs.Text.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.ApprovalQueueFilter.AllJobs.Text.NoEdit','All job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.AllEmployers.TopDefault.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.AllEmployers.TopDefault.NoEdit','All businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.EditCompanyModal.StateEmployerIdentificationNumber.Label'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.EditCompanyModal.StateEmployerIdentificationNumber.Label','SEIN', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.EmployerNameHeader.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.EmployerNameHeader.Text','Business name', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Controls.FindHiringManager.CreateEmployerButton.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Controls.FindHiringManager.CreateEmployerButton.Text','Create new business & job posting', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='JobListFilter.ForeignLaborJobsH2B.NoEdit'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('JobListFilter.ForeignLaborJobsH2B.NoEdit','Foreign labor (H2B-non-ag) job posts', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Employers.CreateNewEmployerButton.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Employers.CreateNewEmployerButton.Text','Create new business account', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.Employers.Page.Title'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.Employers.Page.Title','Assist Businesses', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Global.AssistEmployers.LinkText'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Global.AssistEmployers.LinkText','Find a business', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaEmployerActivity.CriteriaEmployerActivityLabel.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaEmployerActivity.CriteriaEmployerActivityLabel.Text','Business activity', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics.CriteriaEmployerCharacteristicsLabel.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebReporting.Controls.CriteriaEmployerCharacteristics.CriteriaEmployerCharacteristicsLabel.Text','Business characteristics', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.MessageEmployers.Column1Header.Employer.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.MessageEmployers.Column1Header.Employer.Text','Which businesses are you looking for?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.MessageEmployers.Column2Header.Employer.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.MessageEmployers.Column2Header.Employer.Text','View the business activity', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.WebAssist.MessageEmployers.Column3Header.Employer.Text'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.WebAssist.MessageEmployers.Column3Header.Employer.Text','Where are the businesses located and what is the overall time period for this search?', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.JobActivityList.PostJob.ActionDescription'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.JobActivityList.PostJob.ActionDescription','Job posted', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] ='Focus.Web.Code.Controls.User.JobActivityList.CreateJob.ActionDescription'begin
 insert [Config.LocalisationItem]
   ([Key], [Value], [Localised], [LocalisationId])
 values ('Focus.Web.Code.Controls.User.JobActivityList.CreateJob.ActionDescription','Job drafted', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'Global.EmployerName.TopDefault'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Global.EmployerName.TopDefault', '- New company -', 0, 12090)
end

