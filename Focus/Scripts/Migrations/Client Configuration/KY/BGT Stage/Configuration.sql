﻿DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('IntegrationClient', '3', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationSettings'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('IntegrationSettings', '{"Url": "https://www.ekostraining.ky.gov/zdac/xml/", "DefaultSecurityName": "nkumar", "DefaultPassword": "abcd1234", "DefaultOfficeId": "KY0001", "AcceptAllCertificates": "true", "DefaultAdminId": "KY100106472", "CtAggrVersion": "184", "OriginationMethod": "8", "EKOSUrl": "http://162.114.36.41/seekerwebservice.asmx", "EKOSUserName": "nkumar", "EKOSPassword": "abcd1234", "EKOSNamespaceUri": "http://162.114.36.141/", "DefaultNaics": "81", "SelfServiceActivityId": "361", "AssistedServiceActivityId": "39", "EkosServiceUserName": "bguser", "EkosServicePassword": "bglass", "DefaultOnet3": "51900000", "KewesEncryptionKey" : "ab48495fdjk4950dj39405fkbacderakjlmklr23423", "ActionTypeRestrictions" : [  { "ExternalActivityId": "361", "RestrictedToInstancePerHour": "24" }, { "ExternalActivityId": "45", "RestrictedToInstancePerHour": "24" }, { "ExternalActivityId": "39", "RestrictedToInstancePerHour": "24" }, { "ExternalActivityId": "37", "RestrictedToInstancePerHour": "24" } ] }', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'SEINRegExPattern'
DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'SEINMaskPattern'

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'DaysForMinimumJobClosingDate'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('DaysForMinimumJobClosingDate', '4', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'MinimumSalary'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('MinimumSalary', '10192', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'AdjustSalaryRangeByType'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('AdjustSalaryRangeByType', 'True', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'ShowBridgestoOppsInterest'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('ShowBridgestoOppsInterest', 'True', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'BridgestoOppsInterestActivityExternalId'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('BridgestoOppsInterestActivityExternalId', '1242', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'DaysForFederalContractorLifetime'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('DaysForFederalContractorLifetime', '365', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'MandatoryHoursPerWeek'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('MandatoryHoursPerWeek', 'False', 1) 

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'MandatoryHours'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('MandatoryHours', 'True', 1) 

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'MandatoryJobType'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('MandatoryJobType', 'True', 1) 
