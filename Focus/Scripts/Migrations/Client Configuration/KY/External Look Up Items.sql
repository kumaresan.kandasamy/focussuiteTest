DECLARE @LookupMapper TABLE
(
	[Key] NVARCHAR(200),
	ExternalId NVARCHAR(36)
)

DECLARE @EducationLevelEnum INT
DECLARE @JobStatusEnum INT
DECLARE @GenderEnum INT
DECLARE @DisabilityStatusEnum INT
DECLARE @DisabilityCategoryEnum INT
DECLARE @SalaryUnitEnum INT
DECLARE @WorkShiftEnum INT
DECLARE @DrivingLicenceClassEnum INT
DECLARE @WorkDurationEnum INT
DECLARE @EthnicityEnum INT
DECLARE @RaceEnum INT
DECLARE @ShiftEnum INT
DECLARE @DrivingEndorsementEnum INT
DECLARE @EmployerOwnerEnum INT
DECLARE @SalutationEnum INT
DECLARE @VeteranEraEnum INT
DECLARE @VeteranDisabilityStatusEnum INT
DECLARE @VeteranTransitionTypeEnum INT
DECLARE @CountiesEnum INT
DECLARE @StatesEnum INT
DECLARE @CountryEnum INT
DECLARE @EmploymentStatusEnum INT
DECLARE @SchoolStatusEnum INT
DECLARE @WorkWeekEnum INT
DECLARE @ApplicationStatusEnum INT
DECLARE @NaicsEnum INT
DECLARE @ActionTypeEnum INT
DECLARE @InsuranceBenefitsEnum INT
DECLARE @LeaveBenefitsEnum INT
DECLARE @RetirementsBenefitsEnum INT
DECLARE @MiscellaneousBenefitsEnum INT
DECLARE @DurationEnum INT
DECLARE @ClientDataTag NVARCHAR(10)

SET @ClientDataTag = 'KY'
SET @EducationLevelEnum = 0
SET @JobStatusEnum = 1
SET @GenderEnum = 2
SET @DisabilityStatusEnum = 3
SET @SalaryUnitEnum = 4
SET @WorkShiftEnum = 5
SET @DrivingLicenceClassEnum = 6
SET @InsuranceBenefitsEnum = 7
SET @LeaveBenefitsEnum = 8
SET @RetirementsBenefitsEnum = 9
SET @MiscellaneousBenefitsEnum = 10
SET @WorkDurationEnum = 11
SET @EthnicityEnum = 12
SET @RaceEnum = 25
SET @ShiftEnum = 13
SET @DrivingEndorsementEnum = 14
SET @EmployerOwnerEnum = 15
SET @SalutationEnum = 16
SET @VeteranEraEnum = 200
SET @VeteranDisabilityStatusEnum = 201
SET @VeteranTransitionTypeEnum = 202
SET @CountiesEnum = 300
SET @StatesEnum = 301
SET @CountryEnum = 302
SET @EmploymentStatusEnum = 17
SET @SchoolStatusEnum = 18
SET @WorkWeekEnum = 19
SET @DisabilityCategoryEnum = 20
SET @ApplicationStatusEnum = 21
SET @NaicsEnum = 22
SET @ActionTypeEnum = 23
SET @DurationEnum = 33

BEGIN -- Add education level look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EducationLevelEnum AND [ClientDataTag] = @ClientDataTag
-- Insert education external Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('No_Grade_00','0',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_1_01','1',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_2_02','2',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_3_03','3',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_4_04','4',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_5_05','5',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_6_06','6',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_7_07','7',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_8_08','8',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_9_09','9',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_10_10','10',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_11_11','11',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_12_No_Diploma_12','12',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('GED_88','88',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Grade_12_HS_Graduate_13','13',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Disabled_w_Cert_IEP_14','14',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_1_Year_College_OR_VOC_Tech_No_Degree_15','15',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_2_Year_College_OR_VOC_Tech_No_Degree_16','16',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_3_Year_College_OR_VOC_Tech_No_Degree_17','17',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_1_Year_Vocational_Degree_18','18',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_2_Year_Vocational_Degree_19','19',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_3_Year_Vocational_Degree_20','20',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_1_Year_Associates_Degree_21','21',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_2_Year_Associates_Degree_22','22',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HS_3_Year_Associates_Degree_23','23',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Bachelors_OR_Equivalent_24','24',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Masters_Degree_25','25',@EducationLevelEnum, @ClientDataTag, 0)
           
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Doctorate_Degree_26','26',@EducationLevelEnum, @ClientDataTag, 0)
END

BEGIN -- Add Job status look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @JobStatusEnum AND [ClientDataTag] = @ClientDataTag
-- Insert job status Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Active','1',@JobStatusEnum, @ClientDataTag, 1)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('OnHold','5',@JobStatusEnum, @ClientDataTag, 2)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Closed','7',@JobStatusEnum, @ClientDataTag, 3)
END

BEGIN -- Add gender look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @GenderEnum AND [ClientDataTag] = @ClientDataTag
-- Insert gender Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Female','1',@GenderEnum, @ClientDataTag, 1)
-- Insert gender Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Male','2',@GenderEnum, @ClientDataTag, 2)
-- Insert gender Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('NotDisclosed','3',@GenderEnum, @ClientDataTag, 3)
END

BEGIN -- Add disability status look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DisabilityStatusEnum AND [ClientDataTag] = @ClientDataTag
-- Insert disability status Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('NotDisabled','1',@DisabilityStatusEnum, @ClientDataTag, 1)

INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Disabled','2',@DisabilityStatusEnum, @ClientDataTag, 2)

INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('NotDisclosed','3',@DisabilityStatusEnum, @ClientDataTag, 3)
END

BEGIN -- Add disability category
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DisabilityCategoryEnum AND [ClientDataTag] = @ClientDataTag

	-- Insert disability categories
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'DisabilityCategory.PhysicalImpairment' THEN '1'
			WHEN 'DisabilityCategory.MentalImpairment' THEN '2'
			WHEN 'DisabilityCategory.BothPhysicalMentalImpairment' THEN '3'
			WHEN 'DisabilityCategory.NotDisclosed' THEN '4'
		END,
		@DisabilityCategoryEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'DisabilityCategories' 
		AND [Key] IN 
		(
			'DisabilityCategory.PhysicalImpairment',
			'DisabilityCategory.NotDisclosed',
			'DisabilityCategory.MentalImpairment',
			'DisabilityCategory.BothPhysicalMentalImpairment'
		)
END


BEGIN -- Add salary unit look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @SalaryUnitEnum AND [ClientDataTag] = @ClientDataTag
-- Insert salary unit Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Frequencies.Hourly' THEN '1'
		WHEN 'Frequencies.Daily' THEN '2'
		WHEN 'Frequencies.Weekly' THEN '3'
		WHEN 'Frequencies.Monthly' THEN '4'
		WHEN 'Frequencies.Yearly' THEN '5'
	END,
	@SalaryUnitEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Frequencies' AND [Key] IN ('Frequencies.Hourly', 'Frequencies.Daily', 'Frequencies.Weekly', 'Frequencies.Monthly', 'Frequencies.Yearly')
END

BEGIN -- Add workshift look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @WorkShiftEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'WorkShifts.FirstDay' THEN '1'
		WHEN 'WorkShifts.SecondEvening' THEN '2'
		WHEN 'WorkShifts.ThirdNight' THEN '3'
		WHEN 'WorkShifts.Rotating' THEN '4'
		WHEN 'WorkShifts.Split' THEN '5'
		WHEN 'WorkShifts.Varies' THEN '6'
	END,
	@WorkShiftEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'WorkShifts' AND [Key] IN ('WorkShifts.FirstDay', 'WorkShifts.SecondEvening', 'WorkShifts.ThirdNight', 'WorkShifts.Rotating', 'WorkShifts.Split', 'WorkShifts.Varies')
END

BEGIN -- Add driving licence class
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DrivingLicenceClassEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'DrivingLicenceClasses.ClassA' THEN '1'
		WHEN 'DrivingLicenceClasses.ClassB' THEN '2'
		WHEN 'DrivingLicenceClasses.ClassC' THEN '3'
		WHEN 'DrivingLicenceClasses.ClassD' THEN '4'
		WHEN 'DrivingLicenceClasses.Motorcycle' THEN '5'
	END,
	@DrivingLicenceClassEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'DrivingLicenceClasses' AND [Key] IN ('DrivingLicenceClasses.ClassA', 'DrivingLicenceClasses.ClassB', 'DrivingLicenceClasses.ClassC', 'DrivingLicenceClasses.ClassD', 'DrivingLicenceClasses.Motorcycle')
END

BEGIN -- Add insurance benefit types
	-- Clear down existing data
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @InsuranceBenefitsEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert insurance benefit types
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('Health', '1', @InsuranceBenefitsEnum, @ClientDataTag, 1) -- Health
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('Dental', '2', @InsuranceBenefitsEnum, @ClientDataTag, 1) -- Dental

END

BEGIN -- Add leave benefit types
	-- Clear down existing data
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @LeaveBenefitsEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert leave benefit types
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('Vacation', '3', @LeaveBenefitsEnum, @ClientDataTag, 1) -- Vacation
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('Sick', '4', @LeaveBenefitsEnum, @ClientDataTag, 1) -- Sick
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('PaidHolidays', '5', @LeaveBenefitsEnum, @ClientDataTag, 1) -- Paid holidays

END

BEGIN -- Add retirement benefit types
	-- Clear down existing data
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @RetirementsBenefitsEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert retirement benefit types
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('PensionPlan', '6', @RetirementsBenefitsEnum, @ClientDataTag, 1) -- Pension plan

END

BEGIN -- Add misc benefit types
	-- Clear down existing data
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @MiscellaneousBenefitsEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert misc benefit types
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ClothingAllowance', '7', @MiscellaneousBenefitsEnum, @ClientDataTag, 1) -- Clothing allowance
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChildCare', '8', @MiscellaneousBenefitsEnum, @ClientDataTag, 1) -- Clothing allowance

END



BEGIN -- Add work duration
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @WorkDurationEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Duration.FullTimeShorTerm' THEN '2'
		WHEN 'Duration.FullTimeTemporary' THEN '2'
		WHEN 'Duration.FullTimeRegular' THEN '1'
		WHEN 'Duration.PartTimeShortTerm' THEN '2'
		WHEN 'Duration.PartTimeTemporary' THEN '2'
		WHEN 'Duration.PartTimeRegular' THEN '1'
		WHEN 'Duration.FullTimeSeasonal' THEN '2'
		WHEN 'Duration.PartTimeSeasonal' THEN '2'
	END,
	@WorkDurationEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Durations' AND [Key] IN ('Duration.FullTimeShorTerm', 'Duration.FullTimeTemporary', 'Duration.FullTimeRegular', 'Duration.PartTimeShortTerm', 'Duration.PartTimeTemporary', 'Duration.PartTimeRegular', 'Duration.FullTimeSeasonal', 'Duration.PartTimeSeasonal')
END

BEGIN -- Add work week
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @WorkWeekEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert workshift items
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'WorkWeeks.FullTime' THEN '1'
			WHEN 'WorkWeeks.PartTime' THEN '2'
			WHEN 'WorkWeeks.Any' THEN '3'
		END,
		@WorkWeekEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'WorkWeeks' 
		AND [Key] IN ('WorkWeeks.Any', 'WorkWeeks.FullTime', 'WorkWeeks.PartTime')
END

BEGIN -- Add ethnicity
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @RaceEnum AND [ClientDataTag] = @ClientDataTag

-- Insert ethnicity items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Race.White' THEN '1'
		WHEN 'Race.NotDisclosed' THEN '-1'
		WHEN 'Race.HawaiianPacificIslander' THEN '6'
		WHEN 'Race.BlackAfricanAmerican' THEN '2'
		WHEN 'Race.Asian' THEN '5'
		WHEN 'Race.AlaskanAmericanIndian' THEN '4'
	END,
	@RaceEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Races' AND [Key] IN ('Race.White', 'Race.NotDisclosed', 'Race.BlackAfricanAmerican', 'Race.AlaskanAmericanIndian', 'Race.Asian', 'Race.HawaiianPacificIslander')
END

BEGIN -- Add race
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EthnicityEnum AND [ClientDataTag] = @ClientDataTag

INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)), 
	CASE [Key]
		WHEN 'EthnicHeritage.NotDisclosed' THEN '-1'
		WHEN 'EthnicHeritage.NonHispanicLatino' THEN '0'
		WHEN 'EthnicHeritage.HispanicLatino' THEN '1'
	END,
	@EthnicityEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'EthnicHeritages' AND [Key] IN ('EthnicHeritage.NotDisclosed', 'EthnicHeritage.NonHispanicLatino', 'EthnicHeritage.HispanicLatino')
END

BEGIN -- Add shift
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @ShiftEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'WorkShifts.FirstDay' THEN 'shift_first_flag'
		WHEN 'WorkShifts.SecondEvening' THEN 'shift_second_flag'
		WHEN 'WorkShifts.ThirdNight' THEN 'shift_third_flag'
		WHEN 'WorkShifts.Rotating' THEN 'shift_rotating_flag'
		WHEN 'WorkShifts.Split' THEN 'shift_split_flag'
	END,
	@ShiftEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'WorkShifts' AND [Key] IN ('WorkShifts.FirstDay', 'WorkShifts.SecondEvening', 'WorkShifts.ThirdNight', 'WorkShifts.Rotating', 'WorkShifts.Split')
END

BEGIN -- Add shift
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DrivingEndorsementEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'DrivingLicenceEndorsements.Airbrakes' THEN 'drv_airbrake_flag'
		WHEN 'DrivingLicenceEndorsements.SchoolBus' THEN 'drv_bus_flag'
		WHEN 'DrivingLicenceEndorsements.Motorcycle' THEN 'drv_cycle_flag'
		WHEN 'DrivingLicenceEndorsements.DoublesTriples' THEN 'drv_double_flag'
		WHEN 'DrivingLicenceEndorsements.HazerdousMaterials' THEN 'drv_hazard_flag'
		WHEN 'DrivingLicenceEndorsements.PassTransport' THEN 'drv_pass_flag'
		WHEN 'DrivingLicenceEndorsements.TankVehicle' THEN 'drv_tank_flag'
		WHEN 'DrivingLicenceEndorsements.TankHazard' THEN 'drv_tankhazard_flag'
	END,
	@DrivingEndorsementEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'DrivingLicenceEndorsements' AND [Key] IN ('DrivingLicenceEndorsements.Airbrakes', 'DrivingLicenceEndorsements.SchoolBus', 'DrivingLicenceEndorsements.Motorcycle', 'DrivingLicenceEndorsements.DoublesTriples', 'DrivingLicenceEndorsements.HazerdousMaterials', 'DrivingLicenceEndorsements.PassTransport', 'DrivingLicenceEndorsements.TankVehicle', 'DrivingLicenceEndorsements.TankHazard')
END

BEGIN -- Add employer owner
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EmployerOwnerEnum AND [ClientDataTag] = @ClientDataTag
-- Insert employer owners
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'OwnershipTypes.FederalGovernment' THEN '1'
		WHEN 'OwnershipTypes.ForeignInternational' THEN '7'
		WHEN 'OwnershipTypes.LocalGovernment' THEN '3'
		WHEN 'OwnershipTypes.NonProfit' THEN '4'
		WHEN 'OwnershipTypes.PrivateCorporation' THEN '5'
		WHEN 'OwnershipTypes.StateGovernment' THEN '2'
	END,
	@EmployerOwnerEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'OwnershipTypes' AND [Key] IN ('OwnershipTypes.FederalGovernment', 'OwnershipTypes.ForeignInternational', 'OwnershipTypes.LocalGovernment', 'OwnershipTypes.NonProfit', 'OwnershipTypes.PrivateCorporation', 'OwnershipTypes.StateGovernment')
END

BEGIN -- Add salutation
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @SalutationEnum AND [ClientDataTag] = @ClientDataTag
-- Insert salutation
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Title.Mr' THEN '1'
		WHEN 'Title.Mrs' THEN '2'
		WHEN 'Title.Ms' THEN '4'
		WHEN 'Title.Miss' THEN '3'
		WHEN 'Title.Dr' THEN '5'
	END,
	@SalutationEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Titles' AND [Key] IN ('Title.Mr', 'Title.Mrs', 'Title.Ms', 'Title.Miss', 'Title.Dr')
END

BEGIN -- Add veteran era look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @VeteranEraEnum AND [ClientDataTag] = @ClientDataTag
-- Insert veteran era Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Vietnam','1',@VeteranEraEnum, @ClientDataTag, 1)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('OtherVet','2',@VeteranEraEnum, @ClientDataTag, 2)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('OtherEligible','3',@VeteranEraEnum, @ClientDataTag, 3)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('TransitioningServiceMember','4',@VeteranEraEnum, @ClientDataTag, 4)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('TransitioningVietnamServiceMember','5',@VeteranEraEnum, @ClientDataTag, 5)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('TransitioningServiceMemberSpouse','6',@VeteranEraEnum, @ClientDataTag, 6)
END

BEGIN -- Add veteran era look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @VeteranDisabilityStatusEnum AND [ClientDataTag] = @ClientDataTag
-- Insert veteran era Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('NotDisabled','1',@VeteranDisabilityStatusEnum, @ClientDataTag, 1)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Disabled','2',@VeteranDisabilityStatusEnum, @ClientDataTag, 2)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('SpecialDisabled','3',@VeteranDisabilityStatusEnum, @ClientDataTag, 3)           
END

BEGIN -- Add veteran transition type look ups
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @VeteranTransitionTypeEnum AND [ClientDataTag] = @ClientDataTag

	-- Insert veteran transition type Items
	INSERT INTO [Config.ExternalLookUpItem]
			   ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	VALUES
			   ('Retirement','1',@VeteranTransitionTypeEnum, @ClientDataTag, 1),
			   ('Discharge','2',@VeteranTransitionTypeEnum, @ClientDataTag, 2),
			   ('Spouse','3',@VeteranTransitionTypeEnum, @ClientDataTag, 3)
END

BEGIN -- Add states
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @StatesEnum AND [ClientDataTag] = @ClientDataTag
-- Insert states
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'State.AE' THEN 'AE'
		WHEN 'State.AA' THEN 'AA'
		WHEN 'State.AP' THEN 'AP'
		WHEN 'State.AL' THEN 'AL'
		WHEN 'State.AK' THEN 'AK'
		WHEN 'State.AS' THEN 'AS'
		WHEN 'State.AZ' THEN 'AZ'
		WHEN 'State.AR' THEN 'AR'
		WHEN 'State.CA' THEN 'CA'
		WHEN 'State.CO' THEN 'CO'
		WHEN 'State.CT' THEN 'CT'
		WHEN 'State.DE' THEN 'DE'
		WHEN 'State.DC' THEN 'DC'
		WHEN 'State.FM' THEN 'FM'
		WHEN 'State.FL' THEN 'FL'
		WHEN 'State.GA' THEN 'GA'
		WHEN 'State.GU' THEN 'GU'
		WHEN 'State.HI' THEN 'HI'
		WHEN 'State.ID' THEN 'ID'
		WHEN 'State.IL' THEN 'IL'
		WHEN 'State.IN' THEN 'IN'
		WHEN 'State.IA' THEN 'IA'
		WHEN 'State.KS' THEN 'KS'
		WHEN 'State.KY' THEN 'KY'
		WHEN 'State.LA' THEN 'LA'
		WHEN 'State.ME' THEN 'ME'
		WHEN 'State.MH' THEN 'MH'
		WHEN 'State.MD' THEN 'MD'
		WHEN 'State.MA' THEN 'MA'
		WHEN 'State.MI' THEN 'MI'
		WHEN 'State.MN' THEN 'MN'
		WHEN 'State.MS' THEN 'MS'
		WHEN 'State.MO' THEN 'MO'
		WHEN 'State.MT' THEN 'MT'
		WHEN 'State.NE' THEN 'NE'
		WHEN 'State.NV' THEN 'NV'
		WHEN 'State.NH' THEN 'NH'
		WHEN 'State.NJ' THEN 'NJ'
		WHEN 'State.NM' THEN 'NM'
		WHEN 'State.NY' THEN 'NY'
		WHEN 'State.NC' THEN 'NC'
		WHEN 'State.ND' THEN 'ND'
		WHEN 'State.MP' THEN 'MP'
		WHEN 'State.OH' THEN 'OH'
		WHEN 'State.OK' THEN 'OK'
		WHEN 'State.OR' THEN 'OR'
		WHEN 'State.ZZ' THEN 'ZZ'
		WHEN 'State.PW' THEN 'PW'
		WHEN 'State.PA' THEN 'PA'
		WHEN 'State.PR' THEN 'PR'
		WHEN 'State.RI' THEN 'RI'
		WHEN 'State.SC' THEN 'SC'
		WHEN 'State.SD' THEN 'SD'
		WHEN 'State.TN' THEN 'TN'
		WHEN 'State.TX' THEN 'TX'
		WHEN 'State.VI' THEN 'VI'
		WHEN 'State.UT' THEN 'UT'
		WHEN 'State.VT' THEN 'VT'
		WHEN 'State.VA' THEN 'VA'
		WHEN 'State.WA' THEN 'WA'
		WHEN 'State.WV' THEN 'WV'
		WHEN 'State.WI' THEN 'WI'
		WHEN 'State.WY' THEN 'WY'
	END,
	@StatesEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'States' AND [Key] IN ('State.AE', 'State.AA', 'State.AP', 'State.AL', 'State.AK', 'State.AS', 'State.AZ', 'State.AR', 'State.CA', 'State.CO', 'State.CT', 'State.DE', 'State.DC', 'State.FM', 'State.FL', 'State.GA', 'State.GU', 'State.HI', 'State.ID', 'State.IL', 'State.IN', 'State.IA', 'State.KS', 'State.KY', 'State.LA', 'State.ME', 'State.MH', 'State.MD', 'State.MA', 'State.MI', 'State.MN', 'State.MS', 'State.MO', 'State.MT', 'State.NE', 'State.NV', 'State.NH', 'State.NJ', 'State.NM', 'State.NY', 'State.NC', 'State.ND', 'State.MP', 'State.OH', 'State.OK', 'State.OR', 'State.ZZ', 'State.PW', 'State.PA', 'State.PR', 'State.RI', 'State.SC', 'State.SD', 'State.TN', 'State.TX', 'State.VI', 'State.UT', 'State.VT', 'State.VA', 'State.WA', 'State.WV', 'State.WI', 'State.WY')
END

BEGIN -- Add states
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @CountryEnum AND [ClientDataTag] = @ClientDataTag
-- Insert states
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Country.AF' THEN 'AF'
		WHEN 'Country.AX' THEN 'AX'
		WHEN 'Country.AL' THEN 'AL'
		WHEN 'Country.DZ' THEN 'DZ'
		WHEN 'Country.AD' THEN 'AD'
		WHEN 'Country.AO' THEN 'AO'
		WHEN 'Country.AI' THEN 'AI'
		WHEN 'Country.AQ' THEN 'AQ'
		WHEN 'Country.AG' THEN 'AG'
		WHEN 'Country.AR' THEN 'AR'
		WHEN 'Country.AM' THEN 'AM'
		WHEN 'Country.AW' THEN 'AW'
		WHEN 'Country.AC' THEN 'AC'
		WHEN 'Country.AU' THEN 'AU'
		WHEN 'Country.AT' THEN 'AT'
		WHEN 'Country.AZ' THEN 'AZ'
		WHEN 'Country.BS' THEN 'BS'
		WHEN 'Country.BH' THEN 'BH'
		WHEN 'Country.BD' THEN 'BD'
		WHEN 'Country.BB' THEN 'BB'
		WHEN 'Country.BY' THEN 'BY'
		WHEN 'Country.BE' THEN 'BE'
		WHEN 'Country.BZ' THEN 'BZ'
		WHEN 'Country.BJ' THEN 'BJ'
		WHEN 'Country.BM' THEN 'BM'
		WHEN 'Country.BT' THEN 'BT'
		WHEN 'Country.BO' THEN 'BO'
		WHEN 'Country.BA' THEN 'BA'
		WHEN 'Country.BW' THEN 'BW'
		WHEN 'Country.BV' THEN 'BV'
		WHEN 'Country.BR' THEN 'BR'
		WHEN 'Country.IO' THEN 'IO'
		WHEN 'Country.BN' THEN 'BN'
		WHEN 'Country.BG' THEN 'BG'
		WHEN 'Country.BF' THEN 'BF'
		WHEN 'Country.BI' THEN 'BI'
		WHEN 'Country.KH' THEN 'KH'
		WHEN 'Country.CM' THEN 'CM'
		WHEN 'Country.CA' THEN 'CA'
		WHEN 'Country.CV' THEN 'CV'
		WHEN 'Country.KY' THEN 'KY'
		WHEN 'Country.CF' THEN 'CF'
		WHEN 'Country.TD' THEN 'TD'
		WHEN 'Country.CL' THEN 'CL'
		WHEN 'Country.CN' THEN 'CN'
		WHEN 'Country.CX' THEN 'CX'
		WHEN 'Country.CC' THEN 'CC'
		WHEN 'Country.CO' THEN 'CO'
		WHEN 'Country.KM' THEN 'KM'
		WHEN 'Country.CG' THEN 'CG'
		WHEN 'Country.CD' THEN 'CD'
		WHEN 'Country.CK' THEN 'CK'
		WHEN 'Country.CR' THEN 'CR'
		WHEN 'Country.CI' THEN 'CI'
		WHEN 'Country.HR' THEN 'HR'
		WHEN 'Country.CU' THEN 'CU'
		WHEN 'Country.CY' THEN 'CY'
		WHEN 'Country.CZ' THEN 'CZ'
		WHEN 'Country.DK' THEN 'DK'
		WHEN 'Country.DJ' THEN 'DJ'
		WHEN 'Country.DM' THEN 'DM'
		WHEN 'Country.DO' THEN 'DO'
		WHEN 'Country.TP' THEN 'TP'
		WHEN 'Country.EC' THEN 'EC'
		WHEN 'Country.EG' THEN 'EG'
		WHEN 'Country.SV' THEN 'SV'
		WHEN 'Country.GQ' THEN 'GQ'
		WHEN 'Country.ER' THEN 'ER'
		WHEN 'Country.EE' THEN 'EE'
		WHEN 'Country.ET' THEN 'ET'
		WHEN 'Country.FK' THEN 'FK'
		WHEN 'Country.FO' THEN 'FO'
		WHEN 'Country.FJ' THEN 'FJ'
		WHEN 'Country.FI' THEN 'FI'
		WHEN 'Country.FR' THEN 'FR'
		WHEN 'Country.GF' THEN 'GF'
		WHEN 'Country.PF' THEN 'PF'
		WHEN 'Country.TF' THEN 'TF'
		WHEN 'Country.GA' THEN 'GA'
		WHEN 'Country.GM' THEN 'GM'
		WHEN 'Country.GE' THEN 'GE'
		WHEN 'Country.DE' THEN 'DE'
		WHEN 'Country.GH' THEN 'GH'
		WHEN 'Country.GI' THEN 'GI'
		WHEN 'Country.GR' THEN 'GR'
		WHEN 'Country.GL' THEN 'GL'
		WHEN 'Country.GD' THEN 'GD'
		WHEN 'Country.GP' THEN 'GP'
		WHEN 'Country.GT' THEN 'GT'
		WHEN 'Country.GG' THEN 'GG'
		WHEN 'Country.GN' THEN 'GN'
		WHEN 'Country.GW' THEN 'GW'
		WHEN 'Country.GY' THEN 'GY'
		WHEN 'Country.HT' THEN 'HT'
		WHEN 'Country.HM' THEN 'HM'
		WHEN 'Country.VA' THEN 'VA'
		WHEN 'Country.HN' THEN 'HN'
		WHEN 'Country.HK' THEN 'HK'
		WHEN 'Country.HU' THEN 'HU'
		WHEN 'Country.IS' THEN 'IS'
		WHEN 'Country.IN' THEN 'IN'
		WHEN 'Country.ID' THEN 'ID'
		WHEN 'Country.IR' THEN 'IR'
		WHEN 'Country.IQ' THEN 'IQ'
		WHEN 'Country.IE' THEN 'IE'
		WHEN 'Country.IM' THEN 'IM'
		WHEN 'Country.IL' THEN 'IL'
		WHEN 'Country.IT' THEN 'IT'
		WHEN 'Country.JM' THEN 'JM'
		WHEN 'Country.JP' THEN 'JP'
		WHEN 'Country.JE' THEN 'JE'
		WHEN 'Country.JO' THEN 'JO'
		WHEN 'Country.KZ' THEN 'KZ'
		WHEN 'Country.KE' THEN 'KE'
		WHEN 'Country.KI' THEN 'KI'
		WHEN 'Country.KP' THEN 'KP'
		WHEN 'Country.KR' THEN 'KR'
		WHEN 'Country.KW' THEN 'KW'
		WHEN 'Country.KG' THEN 'KG'
		WHEN 'Country.LA' THEN 'LA'
		WHEN 'Country.LV' THEN 'LV'
		WHEN 'Country.LB' THEN 'LB'
		WHEN 'Country.LS' THEN 'LS'
		WHEN 'Country.LR' THEN 'LR'
		WHEN 'Country.LY' THEN 'LY'
		WHEN 'Country.LI' THEN 'LI'
		WHEN 'Country.LT' THEN 'LT'
		WHEN 'Country.LU' THEN 'LU'
		WHEN 'Country.MO' THEN 'MO'
		WHEN 'Country.MK' THEN 'MK'
		WHEN 'Country.MG' THEN 'MG'
		WHEN 'Country.MW' THEN 'MW'
		WHEN 'Country.MY' THEN 'MY'
		WHEN 'Country.MV' THEN 'MV'
		WHEN 'Country.ML' THEN 'ML'
		WHEN 'Country.MT' THEN 'MT'
		WHEN 'Country.MQ' THEN 'MQ'
		WHEN 'Country.MR' THEN 'MR'
		WHEN 'Country.MU' THEN 'MU'
		WHEN 'Country.YT' THEN 'YT'
		WHEN 'Country.MX' THEN 'MX'
		WHEN 'Country.MD' THEN 'MD'
		WHEN 'Country.MC' THEN 'MC'
		WHEN 'Country.MN' THEN 'MN'
		WHEN 'Country.ME' THEN 'ME'
		WHEN 'Country.MS' THEN 'MS'
		WHEN 'Country.MA' THEN 'MA'
		WHEN 'Country.MZ' THEN 'MZ'
		WHEN 'Country.MM' THEN 'MM'
		WHEN 'Country.NA' THEN 'NA'
		WHEN 'Country.NR' THEN 'NR'
		WHEN 'Country.NP' THEN 'NP'
		WHEN 'Country.NL' THEN 'NL'
		WHEN 'Country.AN' THEN 'AN'
		WHEN 'Country.NC' THEN 'NC'
		WHEN 'Country.NZ' THEN 'NZ'
		WHEN 'Country.NI' THEN 'NI'
		WHEN 'Country.NE' THEN 'NE'
		WHEN 'Country.NG' THEN 'NG'
		WHEN 'Country.NU' THEN 'NU'
		WHEN 'Country.NF' THEN 'NF'
		WHEN 'Country.NO' THEN 'NO'
		WHEN 'Country.OM' THEN 'OM'
		WHEN 'Country.PK' THEN 'PK'
		WHEN 'Country.PS' THEN 'PS'
		WHEN 'Country.PA' THEN 'PA'
		WHEN 'Country.PG' THEN 'PG'
		WHEN 'Country.PY' THEN 'PY'
		WHEN 'Country.PE' THEN 'PE'
		WHEN 'Country.PH' THEN 'PH'
		WHEN 'Country.PN' THEN 'PN'
		WHEN 'Country.PL' THEN 'PL'
		WHEN 'Country.PT' THEN 'PT'
		WHEN 'Country.QA' THEN 'QA'
		WHEN 'Country.RE' THEN 'RE'
		WHEN 'Country.RO' THEN 'RO'
		WHEN 'Country.RU' THEN 'RU'
		WHEN 'Country.RW' THEN 'RW'
		WHEN 'Country.BL' THEN 'BL'
		WHEN 'Country.SH' THEN 'SH'
		WHEN 'Country.KN' THEN 'KN'
		WHEN 'Country.LC' THEN 'LC'
		WHEN 'Country.MF' THEN 'MF'
		WHEN 'Country.PM' THEN 'PM'
		WHEN 'Country.VC' THEN 'VC'
		WHEN 'Country.WS' THEN 'WS'
		WHEN 'Country.SM' THEN 'SM'
		WHEN 'Country.ST' THEN 'ST'
		WHEN 'Country.SA' THEN 'SA'
		WHEN 'Country.SN' THEN 'SN'
		WHEN 'Country.RS' THEN 'RS'
		WHEN 'Country.SC' THEN 'SC'
		WHEN 'Country.SL' THEN 'SL'
		WHEN 'Country.SG' THEN 'SG'
		WHEN 'Country.SK' THEN 'SK'
		WHEN 'Country.SI' THEN 'SI'
		WHEN 'Country.SB' THEN 'SB'
		WHEN 'Country.SO' THEN 'SO'
		WHEN 'Country.ZA' THEN 'ZA'
		WHEN 'Country.GS' THEN 'GS'
		WHEN 'Country.ES' THEN 'ES'
		WHEN 'Country.LK' THEN 'LK'
		WHEN 'Country.SD' THEN 'SD'
		WHEN 'Country.SR' THEN 'SR'
		WHEN 'Country.SJ' THEN 'SJ'
		WHEN 'Country.SZ' THEN 'SZ'
		WHEN 'Country.SE' THEN 'SE'
		WHEN 'Country.CH' THEN 'CH'
		WHEN 'Country.SY' THEN 'SY'
		WHEN 'Country.TW' THEN 'TW'
		WHEN 'Country.TJ' THEN 'TJ'
		WHEN 'Country.TZ' THEN 'TZ'
		WHEN 'Country.TH' THEN 'TH'
		WHEN 'Country.TL' THEN 'TL'
		WHEN 'Country.TG' THEN 'TG'
		WHEN 'Country.TK' THEN 'TK'
		WHEN 'Country.TO' THEN 'TO'
		WHEN 'Country.TT' THEN 'TT'
		WHEN 'Country.TN' THEN 'TN'
		WHEN 'Country.TR' THEN 'TR'
		WHEN 'Country.TM' THEN 'TM'
		WHEN 'Country.TC' THEN 'TC'
		WHEN 'Country.TV' THEN 'TV'
		WHEN 'Country.UG' THEN 'UG'
		WHEN 'Country.UA' THEN 'UA'
		WHEN 'Country.AE' THEN 'AE'
		WHEN 'Country.GB' THEN 'GB'
		WHEN 'Country.UK' THEN 'UK'
		WHEN 'Country.US' THEN 'US'
		WHEN 'Country.UY' THEN 'UY'
		WHEN 'Country.UZ' THEN 'UZ'
		WHEN 'Country.VU' THEN 'VU'
		WHEN 'Country.VE' THEN 'VE'
		WHEN 'Country.VN' THEN 'VN'
		WHEN 'Country.VG' THEN 'VG'
		WHEN 'Country.WF' THEN 'WF'
		WHEN 'Country.EH' THEN 'EH'
		WHEN 'Country.YE' THEN 'YE'
		WHEN 'Country.YU' THEN 'YU'
		WHEN 'Country.ZM' THEN 'ZM'
		WHEN 'Country.ZW' THEN 'ZW'
	END,
	@CountryEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Countries' AND [Key] IN ('Country.ZW', 'Country.ZM', 'Country.ZA', 'Country.YU', 'Country.YT', 'Country.YE', 'Country.WS', 'Country.WF', 'Country.VU', 'Country.VN', 'Country.VG', 'Country.VE', 'Country.VC', 'Country.VA', 'Country.UZ', 'Country.UY', 'Country.US', 'Country.UK', 'Country.UG', 'Country.UA', 'Country.TZ', 'Country.TW', 'Country.TV', 'Country.TT', 'Country.TR', 'Country.TP', 'Country.TO', 'Country.TN', 'Country.TM', 'Country.TL', 'Country.TK', 'Country.TJ', 'Country.TH', 'Country.TG', 'Country.TF', 'Country.TD', 'Country.TC', 'Country.SZ', 'Country.SY', 'Country.SV', 'Country.ST', 'Country.SR', 'Country.SO', 'Country.SN', 'Country.SM', 'Country.SL', 'Country.SK', 'Country.SJ', 'Country.SI', 'Country.SH', 'Country.SG', 'Country.SE', 'Country.SD', 'Country.SC', 'Country.SB', 'Country.SA', 'Country.RW', 'Country.RU', 'Country.RS', 'Country.RO', 'Country.RE', 'Country.QA', 'Country.PY', 'Country.PT', 'Country.PS', 'Country.PN', 'Country.PM', 'Country.PL', 'Country.PK', 'Country.PH', 'Country.PG', 'Country.PF', 'Country.PE', 'Country.PA', 'Country.OM', 'Country.NZ', 'Country.NU', 'Country.NR', 'Country.NP', 'Country.NO', 'Country.NL', 'Country.NI', 'Country.NG', 'Country.NF', 'Country.NE', 'Country.NC', 'Country.NA', 'Country.MZ', 'Country.MY', 'Country.MX', 'Country.MW', 'Country.MV', 'Country.MU', 'Country.MT', 'Country.MS', 'Country.MR', 'Country.MQ', 'Country.MO', 'Country.MN', 'Country.MM', 'Country.ML', 'Country.MK', 'Country.MG', 'Country.MF', 'Country.ME', 'Country.MD', 'Country.MC', 'Country.MA', 'Country.LY', 'Country.LV', 'Country.LU', 'Country.LT', 'Country.LS', 'Country.LR', 'Country.LK', 'Country.LI', 'Country.LC', 'Country.LB', 'Country.LA', 'Country.KZ', 'Country.KY', 'Country.KW', 'Country.KR', 'Country.KP', 'Country.KN', 'Country.KM', 'Country.KI', 'Country.KH', 'Country.KG', 'Country.KE', 'Country.JP', 'Country.JO', 'Country.JM', 'Country.JE', 'Country.IT', 'Country.IS', 'Country.IR', 'Country.IQ', 'Country.IO', 'Country.IN', 'Country.IM', 'Country.IL', 'Country.IE', 'Country.ID', 'Country.HU', 'Country.HT', 'Country.HR', 'Country.HN', 'Country.HM', 'Country.HK', 'Country.GY', 'Country.GW', 'Country.GT', 'Country.GS', 'Country.GR', 'Country.GQ', 'Country.GP', 'Country.GN', 'Country.GM', 'Country.GL', 'Country.GI', 'Country.GH', 'Country.GG', 'Country.GF', 'Country.GE', 'Country.GD', 'Country.GB', 'Country.GA', 'Country.FR', 'Country.FO', 'Country.FK', 'Country.FJ', 'Country.FI', 'Country.ET', 'Country.ES', 'Country.ER', 'Country.EH', 'Country.EG', 'Country.EE', 'Country.EC', 'Country.DZ', 'Country.DO', 'Country.DM', 'Country.DK', 'Country.DJ', 'Country.DE', 'Country.CZ', 'Country.CY', 'Country.CX', 'Country.CV', 'Country.CU', 'Country.CR', 'Country.CO', 'Country.CN', 'Country.CM', 'Country.CL', 'Country.CK', 'Country.CI', 'Country.CH', 'Country.CG', 'Country.CF', 'Country.CD', 'Country.CC', 'Country.CA', 'Country.BZ', 'Country.BY', 'Country.BW', 'Country.BV', 'Country.BT', 'Country.BS', 'Country.BR', 'Country.BO', 'Country.BN', 'Country.BM', 'Country.BL', 'Country.BJ', 'Country.BI', 'Country.BH', 'Country.BG', 'Country.BF', 'Country.BE', 'Country.BD', 'Country.BB', 'Country.BA', 'Country.AZ', 'Country.AX', 'Country.AW', 'Country.AU', 'Country.AT', 'Country.AR', 'Country.AQ', 'Country.AO', 'Country.AN', 'Country.AM', 'Country.AL', 'Country.AI', 'Country.AG', 'Country.AF', 'Country.AE', 'Country.AD', 'Country.AC')
END

BEGIN -- Add employment status look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EmploymentStatusEnum AND [ClientDataTag] = @ClientDataTag
-- Insert veteran era Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Employed','1',@EmploymentStatusEnum, @ClientDataTag, 1)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('EmployedTerminationReceived','2',@EmploymentStatusEnum, @ClientDataTag, 2)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('UnEmployed','3',@EmploymentStatusEnum, @ClientDataTag, 3)           
END

BEGIN -- Add school status look ups
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @SchoolStatusEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert veteran era Items
	INSERT INTO [Config.ExternalLookUpItem] 
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	VALUES 
		('NA','0',@SchoolStatusEnum, @ClientDataTag, 0),
		('In_School_HS_OR_less','1',@SchoolStatusEnum, @ClientDataTag, 1),
		('In_School_Alternative_School','2',@SchoolStatusEnum, @ClientDataTag, 2),
		('In_School_Post_HS','3',@SchoolStatusEnum, @ClientDataTag, 3),
		('Not_Attending_School_OR_HS_Dropout','4',@SchoolStatusEnum, @ClientDataTag, 4),
		('Not_Attending_School_HS_Graduate','5',@SchoolStatusEnum, @ClientDataTag, 5)
END

BEGIN -- Add application status look ups
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @ApplicationStatusEnum AND [ClientDataTag] = @ClientDataTag	
	
	-- Insert application status items
	INSERT INTO [Config.ExternalLookUpItem] 
		([InternalId], [ExternalId], [ExternalLookUpType], [ClientDataTag], [FocusId])
	VALUES 
		('DidNotApply', '3', @ApplicationStatusEnum, @ClientDataTag, 5),
		('FailedToShow', '3', @ApplicationStatusEnum, @ClientDataTag, 9),
		('FailedToReportToJob', '4', @ApplicationStatusEnum, @ClientDataTag, 14),
		('FailedToRespondToInvitation', '3', @ApplicationStatusEnum, @ClientDataTag, 15),
		('Hired', '1', @ApplicationStatusEnum, @ClientDataTag, 12),
		('InterviewDenied', '7', @ApplicationStatusEnum, @ClientDataTag, 8),
		('JobAlreadyFilled', '5', @ApplicationStatusEnum, @ClientDataTag, 17),
		('NotHired', '2', @ApplicationStatusEnum, @ClientDataTag, 13),
		('NotQualified', '7', @ApplicationStatusEnum, @ClientDataTag, 18),
		('RefusedOffer', '6', @ApplicationStatusEnum, @ClientDataTag, 11),
		('DidNotApply', '3', @ApplicationStatusEnum, @ClientDataTag, 5),
		('FailedToShow', '3', @ApplicationStatusEnum, @ClientDataTag, 9),
		('FailedToReportToJob', '4', @ApplicationStatusEnum, @ClientDataTag, 14),
		('FailedToRespondToInvitation', '3', @ApplicationStatusEnum, @ClientDataTag, 15),
		('Hired', '1', @ApplicationStatusEnum, @ClientDataTag, 12),
		('InterviewDenied', '7', @ApplicationStatusEnum, @ClientDataTag, 8),
		('JobAlreadyFilled', '5', @ApplicationStatusEnum, @ClientDataTag, 17),
		('NotHired', '2', @ApplicationStatusEnum, @ClientDataTag, 13),
		('NotQualified', '7', @ApplicationStatusEnum, @ClientDataTag, 18),
		('RefusedOffer', '6', @ApplicationStatusEnum, @ClientDataTag, 11),
		('FoundJobFromOtherSource', '1001', @ApplicationStatusEnum, @ClientDataTag, 16),
		('InterviewScheduled', '1002', @ApplicationStatusEnum, @ClientDataTag, 6),
		('NewApplicant', '1003', @ApplicationStatusEnum, @ClientDataTag, 3),
		('NotYetPlaced', '1004', @ApplicationStatusEnum, @ClientDataTag, 19),
		('Recommended', '1005', @ApplicationStatusEnum, @ClientDataTag, 2),
		('RefusedReferral', '1006', @ApplicationStatusEnum, @ClientDataTag, 20),
		('UnderConsideration', '1007', @ApplicationStatusEnum, @ClientDataTag, 4)
END

BEGIN -- Add Base NIACs
	-- Clear down existing data
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @NaicsEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert new NAICS look ups
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('11', 'Agriculture, Forestry, Fishing and Hunting', @NaicsEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('21', 'Mining, Quarrying, and Oil and Gas Extraction', @NaicsEnum, @ClientDataTag, 132)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('22', 'Utilities', @NaicsEnum, @ClientDataTag, 180)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('23', 'Construction', @NaicsEnum, @ClientDataTag, 201)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('31', 'Manufacturing', @NaicsEnum, @ClientDataTag, 274)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('32', 'Manufacturing', @NaicsEnum, @ClientDataTag, 274)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('33', 'Manufacturing', @NaicsEnum, @ClientDataTag, 274)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('42', 'Wholesale Trade', @NaicsEnum, @ClientDataTag, 1038)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('45', 'Retail Trade', @NaicsEnum, @ClientDataTag, 1203)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('44', 'Retail Trade', @NaicsEnum, @ClientDataTag, 1203)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('48', 'Transportation and Warehousing', @NaicsEnum, @ClientDataTag, 1379)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('49', 'Transportation and Warehousing', @NaicsEnum, @ClientDataTag, 1379)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('51', 'Information', @NaicsEnum, @ClientDataTag, 1519)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('52', 'Finance and Insurance', @NaicsEnum, @ClientDataTag, 1597)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('53', 'Real Estate and Rental and Leasing', @NaicsEnum, @ClientDataTag, 1686)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('54', 'Professional, Scientific, and Technical Services', @NaicsEnum, @ClientDataTag, 1741)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('55', 'Management of Companies and Enterprises', @NaicsEnum, @ClientDataTag, 1835)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('56', 'Administrative and Support and Waste Management an', @NaicsEnum, @ClientDataTag, 1842)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('61', 'Educational Services', @NaicsEnum, @ClientDataTag, 1929)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('62', 'Health Care and Social Assistance', @NaicsEnum, @ClientDataTag, 1967)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('71', 'Arts, Entertainment, and Recreation', @NaicsEnum, @ClientDataTag, 2059)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('72', 'Accommodation and Food Services', @NaicsEnum, @ClientDataTag, 2120)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('81', 'Other Services (except Public Administration)', @NaicsEnum, @ClientDataTag, 2156)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('92', 'Public Administration', @NaicsEnum, @ClientDataTag, 2254)

END

BEGIN -- Add Action types
	-- Clear down existing data
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @ActionTypeEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert new action type look ups
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChangeUserName', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChangeJobSeekerSsn', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChangeSecurityQuestion', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('SavePostingSavedSearch', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('LogIn', '45', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('FindJobsForSeeker', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ExternalReferral', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('SelfReferral', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ViewJobInsights', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('SaveResume', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('RunManualJobSearch', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ChangePassword', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('CompleteResume', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('EditDefaultCompleteResume', '361', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('ResumePreparationAssistance', '37', @ActionTypeEnum, @ClientDataTag, 1)
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) VALUES ('BridgesToOpportunity', '1242', @ActionTypeEnum, @ClientDataTag, 1)
END

BEGIN -- Add durations
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DurationEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Duration.PartTimeTemporary' THEN '5'
		WHEN 'Duration.PartTimeShortTerm' THEN '4'
		WHEN 'Duration.PartTimeSeasonal' THEN '8'
		WHEN 'Duration.PartTimeRegular' THEN '6'
		WHEN 'Duration.FullTimeTemporary' THEN '2'
		WHEN 'Duration.FullTimeShorTerm' THEN '1'
		WHEN 'Duration.FullTimeSeasonal' THEN '7'
		WHEN 'Duration.FullTimeRegular' THEN '3'
	END,
	@DurationEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Durations' AND [Key] IN ('Duration.FullTimeShorTerm', 'Duration.FullTimeTemporary', 'Duration.FullTimeRegular', 'Duration.PartTimeShortTerm', 'Duration.PartTimeTemporary', 'Duration.PartTimeRegular', 'Duration.FullTimeSeasonal', 'Duration.PartTimeSeasonal')
END

BEGIN -- Add counties
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @CountiesEnum AND [ClientDataTag] = @ClientDataTag

-- Insert counties
INSERT INTO @LookupMapper ([Key], ExternalId)
VALUES
('County.OutsideUS', '0'),
('County.AdairKY', '21001'),
('County.AllenKY', '21003'),
('County.AndersonKY', '21005'),
('County.BallardKY', '21007'),
('County.BarrenKY', '21009'),
('County.BathKY', '21011'),
('County.BellKY', '21013'),
('County.BooneKY', '21015'),
('County.BourbonKY', '21017'),
('County.BoydKY', '21019'),
('County.BoyleKY', '21021'),
('County.BrackenKY', '21023'),
('County.BreathittKY', '21025'),
('County.BreckinridgeKY', '21027'),
('County.BullittKY', '21029'),
('County.ButlerKY', '21031'),
('County.CaldwellKY', '21033'),
('County.CallowayKY', '21035'),
('County.CampbellKY', '21037'),
('County.CarlisleKY', '21039'),
('County.CarrollKY', '21041'),
('County.CarterKY', '21043'),
('County.CaseyKY', '21045'),
('County.ChristianKY', '21047'),
('County.ClarkKY', '21049'),
('County.ClayKY', '21051'),
('County.ClintonKY', '21053'),
('County.CrittendenKY', '21055'),
('County.CumberlandKY', '21057'),
('County.DaviessKY', '21059'),
('County.EdmonsonKY', '21061'),
('County.ElliottKY', '21063'),
('County.EstillKY', '21065'),
('County.FayetteKY', '21067'),
('County.FlemingKY', '21069'),
('County.FloydKY', '21071'),
('County.FranklinKY', '21073'),
('County.FultonKY', '21075'),
('County.GallatinKY', '21077'),
('County.GarrardKY', '21079'),
('County.GrantKY', '21081'),
('County.GravesKY', '21083'),
('County.GraysonKY', '21085'),
('County.GreenKY', '21087'),
('County.GreenupKY', '21089'),
('County.HancockKY', '21091'),
('County.HardinKY', '21093'),
('County.HarlanKY', '21095'),
('County.HarrisonKY', '21097'),
('County.HartKY', '21099'),
('County.HendersonKY', '21101'),
('County.HenryKY', '21103'),
('County.HickmanKY', '21105'),
('County.HopkinsKY', '21107'),
('County.JacksonKY', '21109'),
('County.JeffersonKY', '21111'),
('County.JessamineKY', '21113'),
('County.JohnsonKY', '21115'),
('County.KentonKY', '21117'),
('County.KnottKY', '21119'),
('County.KnoxKY', '21121'),
('County.LarueKY', '21123'),
('County.LaurelKY', '21125'),
('County.LawrenceKY', '21127'),
('County.LeeKY', '21129'),
('County.LeslieKY', '21131'),
('County.LetcherKY', '21133'),
('County.LewisKY', '21135'),
('County.LincolnKY', '21137'),
('County.LivingstonKY', '21139'),
('County.LoganKY', '21141'),
('County.LyonKY', '21143'),
('County.MadisonKY', '21151'),
('County.MagoffinKY', '21153'),
('County.MarionKY', '21155'),
('County.MarshallKY', '21157'),
('County.MartinKY', '21159'),
('County.MasonKY', '21161'),
('County.McCrackenKY', '21145'),
('County.McCrearyKY', '21147'),
('County.McLeanKY', '21149'),
('County.MeadeKY', '21163'),
('County.MenifeeKY', '21165'),
('County.MercerKY', '21167'),
('County.MetcalfeKY', '21169'),
('County.MonroeKY', '21171'),
('County.MontgomeryKY', '21173'),
('County.MorganKY', '21175'),
('County.MuhlenbergKY', '21177'),
('County.NelsonKY', '21179'),
('County.NicholasKY', '21181'),
('County.OhioKY', '21183'),
('County.OldhamKY', '21185'),
('County.OwenKY', '21187'),
('County.OwsleyKY', '21189'),
('County.PendletonKY', '21191'),
('County.PerryKY', '21193'),
('County.PikeKY', '21195'),
('County.PowellKY', '21197'),
('County.PulaskiKY', '21199'),
('County.RobertsonKY', '21201'),
('County.RockcastleKY', '21203'),
('County.RowanKY', '21205'),
('County.RussellKY', '21207'),
('County.ScottKY', '21209'),
('County.ShelbyKY', '21211'),
('County.SimpsonKY', '21213'),
('County.SpencerKY', '21215'),
('County.TaylorKY', '21217'),
('County.ToddKY', '21219'),
('County.TriggKY', '21221'),
('County.TrimbleKY', '21223'),
('County.UnionKY', '21225'),
('County.WarrenKY', '21227'),
('County.WashingtonKY', '21229'),
('County.WayneKY', '21231'),
('County.WebsterKY', '21233'),
('County.WhitleyKY', '21235'),
('County.WolfeKY', '21237'),
('County.WoodfordKY', '21239'),
('County.AlexanderIL', '17003'),
('County.EdwardsIL', '17047'),
('County.FranklinIL', '17055'),
('County.GallatinIL', '17059'),
('County.HamiltonIL', '17065'),
('County.HardinIL', '17069'),
('County.JacksonIL', '17077'),
('County.JohnsonIL', '17087'),
('County.MassacIL', '17127'),
('County.PopeIL', '17151'),
('County.PulaskiIL', '17153'),
('County.SalineIL', '17165'),
('County.UnionIL', '17181'),
('County.WabashIL', '17185'),
('County.WayneIL', '17191'),
('County.WhiteIL', '17193'),
('County.WilliamsonIL', '17199'),
('County.BartholomewIN', '18005'),
('County.ClarkIN', '18019'),
('County.CrawfordIN', '18025'),
('County.DearbornIN', '18029'),
('County.DecaturIN', '18031'),
('County.DuboisIN', '18037'),
('County.FayetteIN', '18041'),
('County.FloydIN', '18043'),
('County.FranklinIN', '18047'),
('County.GibsonIN', '18051'),
('County.HarrisonIN', '18061'),
('County.JacksonIN', '18071'),
('County.JeffersonIN', '18077'),
('County.JenningsIN', '18079'),
('County.KnoxIN', '18083'),
('County.LawrenceIN', '18093'),
('County.OhioIN', '18115'),
('County.OrangeIN', '18117'),
('County.PerryIN', '18123'),
('County.PikeIN', '18125'),
('County.PoseyIN', '18129'),
('County.RipleyIN', '18137'),
('County.RushIN', '18139'),
('County.ScottIN', '18143'),
('County.SpencerIN', '18147'),
('County.SwitzerlandIN', '18155'),
('County.UnionIN', '18161'),
('County.VanderburghIN', '18163'),
('County.WarrickIN', '18173'),
('County.WashingtonIN', '18175'),
('County.WayneIN', '18177'),
('County.BollingerMO', '29017'),
('County.DunklinMO', '29069'),
('County.MississippiMO', '29133'),
('County.PemiscotMO', '29155'),
('County.ScottMO', '29201'),
('County.StoddardMO', '29207'),
('County.AndersonTN', '47001'),
('County.BentonTN', '47005'),
('County.CampbellTN', '47013'),
('County.CannonTN', '47015'),
('County.CarrollTN', '47017'),
('County.CheathamTN', '47021'),
('County.ClaiborneTN', '47025'),
('County.ClayTN', '47027'),
('County.CockeTN', '47029'),
('County.CrockettTN', '47033'),
('County.CumberlandTN', '47035'),
('County.DavidsonTN', '47037'),
('County.DeKalbTN', '47041'),
('County.DicksonTN', '47043'),
('County.DyerTN', '47045'),
('County.FentressTN', '47049'),
('County.GibsonTN', '47053'),
('County.GraingerTN', '47057'),
('County.GreeneTN', '47059'),
('County.HamblenTN', '47063'),
('County.HancockTN', '47067'),
('County.HawkinsTN', '47073'),
('County.HenryTN', '47079'),
('County.HoustonTN', '47083'),
('County.HumphreysTN', '47085'),
('County.JacksonTN', '47087'),
('County.JeffersonTN', '47089'),
('County.KnoxTN', '47093'),
('County.LakeTN', '47095'),
('County.LauderdaleTN', '47097'),
('County.MaconTN', '47111'),
('County.MontgomeryTN', '47125'),
('County.MorganTN', '47129'),
('County.ObionTN', '47131'),
('County.OvertonTN', '47133'),
('County.PickettTN', '47137'),
('County.PutnamTN', '47141'),
('County.RoaneTN', '47145'),
('County.RobertsonTN', '47147'),
('County.RutherfordTN', '47149'),
('County.ScottTN', '47151'),
('County.SevierTN', '47155'),
('County.SmithTN', '47159'),
('County.StewartTN', '47161'),
('County.SullivanTN', '47163'),
('County.SumnerTN', '47165'),
('County.TrousdaleTN', '47169'),
('County.UnionTN', '47173'),
('County.WashingtonTN', '47179'),
('County.WeakleyTN', '47183'),
('County.WilliamsonTN', '47187'),
('County.WilsonTN', '47189'),
('County.BuchananVA', '51027'),
('County.DickensonVA', '51051'),
('County.LeeVA', '51105'),
('County.RussellVA', '51167'),
('County.ScottVA', '51169'),
('County.SmythVA', '51173'),
('County.TazewellVA', '51185'),
('County.WashingtonVA', '51191'),
('County.WiseVA', '51195'),
('County.BooneWV', '54005'),
('County.CabellWV', '54011'),
('County.KanawhaWV', '54039'),
('County.LincolnWV', '54043'),
('County.LoganWV', '54045'),
('County.MasonWV', '54053'),
('County.McDowellWV', '54047'),
('County.MercerWV', '54055'),
('County.MingoWV', '54059'),
('County.PutnamWV', '54079'),
('County.RaleighWV', '54081'),
('County.WayneWV', '54099'),
('County.WyomingWV', '54109'),
('County.AutaugaAL', '1001'),
('County.BaldwinAL', '1003'),
('County.BarbourAL', '1005'),
('County.BibbAL', '1007'),
('County.BlountAL', '1009'),
('County.BullockAL', '1011'),
('County.ButlerAL', '1013'),
('County.CalhounAL', '1015'),
('County.ChambersAL', '1017'),
('County.CherokeeAL', '1019'),
('County.ChiltonAL', '1021'),
('County.ChoctawAL', '1023'),
('County.ClarkeAL', '1025'),
('County.ClayAL', '1027'),
('County.CleburneAL', '1029'),
('County.CoffeeAL', '1031'),
('County.ColbertAL', '1033'),
('County.ConecuhAL', '1035'),
('County.CoosaAL', '1037'),
('County.CovingtonAL', '1039'),
('County.CrenshawAL', '1041'),
('County.CullmanAL', '1043'),
('County.DaleAL', '1045'),
('County.DallasAL', '1047'),
('County.DeKalbAL', '1049'),
('County.ElmoreAL', '1051'),
('County.EscambiaAL', '1053'),
('County.EtowahAL', '1055'),
('County.FayetteAL', '1057'),
('County.FranklinAL', '1059'),
('County.GenevaAL', '1061'),
('County.GreeneAL', '1063'),
('County.HaleAL', '1065'),
('County.HenryAL', '1067'),
('County.HoustonAL', '1069'),
('County.JacksonAL', '1071'),
('County.JeffersonAL', '1073'),
('County.LamarAL', '1075'),
('County.LauderdaleAL', '1077'),
('County.LawrenceAL', '1079'),
('County.LeeAL', '1081'),
('County.LimestoneAL', '1083'),
('County.LowndesAL', '1085'),
('County.MaconAL', '1087'),
('County.MadisonAL', '1089'),
('County.MarengoAL', '1091'),
('County.MarionAL', '1093'),
('County.MarshallAL', '1095'),
('County.MobileAL', '1097'),
('County.MonroeAL', '1099'),
('County.MontgomeryAL', '1101'),
('County.MorganAL', '1103'),
('County.PerryAL', '1105'),
('County.PickensAL', '1107'),
('County.PikeAL', '1109'),
('County.RandolphAL', '1111'),
('County.RussellAL', '1113'),
('County.ShelbyAL', '1117'),
('County.St. ClairAL', '1115'),
('County.SumterAL', '1119'),
('County.TalladegaAL', '1121'),
('County.TallapoosaAL', '1123'),
('County.TuscaloosaAL', '1125'),
('County.WalkerAL', '1127'),
('County.WashingtonAL', '1129'),
('County.WilcoxAL', '1131'),
('County.WinstonAL', '1133'),
('County.Aleutians EastAK', '2013'),
('County.Aleutians WestAK', '2016'),
('County.AnchorageAK', '2020'),
('County.BethelAK', '2050'),
('County.Bristol BayAK', '2060'),
('County.DenaliAK', '2068'),
('County.DillinghamAK', '2070'),
('County.Fairbanks North StarAK', '2090'),
('County.HainesAK', '2100'),
('County.JuneauAK', '2110'),
('County.Kenai PeninsulaAK', '2122'),
('County.Ketchikan GatewayAK', '2130'),
('County.Kodiak IslandAK', '2150'),
('County.Lake and PeninsulaAK', '2164'),
('County.Matanuska-SusitnaAK', '2170'),
('County.NomeAK', '2180'),
('County.North SlopeAK', '2185'),
('County.Northwest ArcticAK', '2188'),
('County.Prince of Wales-Outer KetchikanAK', '2201'),
('County.SitkaAK', '2220'),
('County.Skagway-Hoonah-AngoonAK', '2232'),
('County.Southeast FairbanksAK', '2240'),
('County.Valdez-CordovaAK', '2261'),
('County.Wade HamptonAK', '2270'),
('County.Wrangell-PetersburgAK', '2280'),
('County.YakutatAK', '2282'),
('County.Yukon-KoyukukAK', '2290'),
('County.American SamoaAS', '60050'),
('County.ApacheAZ', '4001'),
('County.CochiseAZ', '4003'),
('County.CoconinoAZ', '4005'),
('County.GilaAZ', '4007'),
('County.GrahamAZ', '4009'),
('County.GreenleeAZ', '4011'),
('County.La PazAZ', '4012'),
('County.MaricopaAZ', '4013'),
('County.MohaveAZ', '4015'),
('County.NavajoAZ', '4017'),
('County.PimaAZ', '4019'),
('County.PinalAZ', '4021'),
('County.Santa CruzAZ', '4023'),
('County.YavapaiAZ', '4025'),
('County.YumaAZ', '4027'),
('County.ArkansasAR', '5001'),
('County.AshleyAR', '5003'),
('County.BaxterAR', '5005'),
('County.BentonAR', '5007'),
('County.BooneAR', '5009'),
('County.BradleyAR', '5011'),
('County.CalhounAR', '5013'),
('County.CarrollAR', '5015'),
('County.ChicotAR', '5017'),
('County.ClarkAR', '5019'),
('County.ClayAR', '5021'),
('County.CleburneAR', '5023'),
('County.ClevelandAR', '5025'),
('County.ColumbiaAR', '5027'),
('County.ConwayAR', '5029'),
('County.CraigheadAR', '5031'),
('County.CrawfordAR', '5033'),
('County.CrittendenAR', '5035'),
('County.CrossAR', '5037'),
('County.DallasAR', '5039'),
('County.DeshaAR', '5041'),
('County.DrewAR', '5043'),
('County.FaulknerAR', '5045'),
('County.FranklinAR', '5047'),
('County.FultonAR', '5049'),
('County.GarlandAR', '5051'),
('County.GrantAR', '5053'),
('County.GreeneAR', '5055'),
('County.HempsteadAR', '5057'),
('County.Hot SpringAR', '5059'),
('County.HowardAR', '5061'),
('County.IndependenceAR', '5063'),
('County.IzardAR', '5065'),
('County.JacksonAR', '5067'),
('County.JeffersonAR', '5069'),
('County.JohnsonAR', '5071'),
('County.LafayetteAR', '5073'),
('County.LawrenceAR', '5075'),
('County.LeeAR', '5077'),
('County.LincolnAR', '5079'),
('County.Little RiverAR', '5081'),
('County.LoganAR', '5083'),
('County.LonokeAR', '5085'),
('County.MadisonAR', '5087'),
('County.MarionAR', '5089'),
('County.MillerAR', '5091'),
('County.MississippiAR', '5093'),
('County.MonroeAR', '5095'),
('County.MontgomeryAR', '5097'),
('County.NevadaAR', '5099'),
('County.NewtonAR', '5101'),
('County.OuachitaAR', '5103'),
('County.PerryAR', '5105'),
('County.PhillipsAR', '5107'),
('County.PikeAR', '5109'),
('County.PoinsettAR', '5111'),
('County.PolkAR', '5113'),
('County.PopeAR', '5115'),
('County.PrairieAR', '5117'),
('County.PulaskiAR', '5119'),
('County.RandolphAR', '5121'),
('County.SalineAR', '5125'),
('County.ScottAR', '5127'),
('County.SearcyAR', '5129'),
('County.SebastianAR', '5131'),
('County.SevierAR', '5133'),
('County.SharpAR', '5135'),
('County.St. FrancisAR', '5123'),
('County.StoneAR', '5137'),
('County.UnionAR', '5139'),
('County.Van BurenAR', '5141'),
('County.WashingtonAR', '5143'),
('County.WhiteAR', '5145'),
('County.WoodruffAR', '5147'),
('County.YellAR', '5149'),
('County.AlamedaCA', '6001'),
('County.AlpineCA', '6003'),
('County.AmadorCA', '6005'),
('County.ButteCA', '6007'),
('County.CalaverasCA', '6009'),
('County.ColusaCA', '6011'),
('County.Contra CostaCA', '6013'),
('County.Del NorteCA', '6015'),
('County.El DoradoCA', '6017'),
('County.FresnoCA', '6019'),
('County.GlennCA', '6021'),
('County.HumboldtCA', '6023'),
('County.ImperialCA', '6025'),
('County.InyoCA', '6027'),
('County.KernCA', '6029'),
('County.KingsCA', '6031'),
('County.LakeCA', '6033'),
('County.LassenCA', '6035'),
('County.Los AngelesCA', '6037'),
('County.MaderaCA', '6039'),
('County.MarinCA', '6041'),
('County.MariposaCA', '6043'),
('County.MendocinoCA', '6045'),
('County.MercedCA', '6047'),
('County.ModocCA', '6049'),
('County.MonoCA', '6051'),
('County.MontereyCA', '6053'),
('County.NapaCA', '6055'),
('County.NevadaCA', '6057'),
('County.OrangeCA', '6059'),
('County.PlacerCA', '6061'),
('County.PlumasCA', '6063'),
('County.RiversideCA', '6065'),
('County.SacramentoCA', '6067'),
('County.San BenitoCA', '6069'),
('County.San BernardinoCA', '6071'),
('County.San DiegoCA', '6073'),
('County.San FranciscoCA', '6075'),
('County.San JoaquinCA', '6077'),
('County.San Luis ObispoCA', '6079'),
('County.San MateoCA', '6081'),
('County.Santa BarbaraCA', '6083'),
('County.Santa ClaraCA', '6085'),
('County.Santa CruzCA', '6087'),
('County.ShastaCA', '6089'),
('County.SierraCA', '6091'),
('County.SiskiyouCA', '6093'),
('County.SolanoCA', '6095'),
('County.SonomaCA', '6097'),
('County.StanislausCA', '6099'),
('County.SutterCA', '6101'),
('County.TehamaCA', '6103'),
('County.TrinityCA', '6105'),
('County.TulareCA', '6107'),
('County.TuolumneCA', '6109'),
('County.VenturaCA', '6111'),
('County.YoloCA', '6113'),
('County.YubaCA', '6115'),
('County.AdamsCO', '8001'),
('County.AlamosaCO', '8003'),
('County.ArapahoeCO', '8005'),
('County.ArchuletaCO', '8007'),
('County.BacaCO', '8009'),
('County.BentCO', '8011'),
('County.BoulderCO', '8013'),
('County.BroomfieldCO', '8014'),
('County.ChaffeeCO', '8015'),
('County.CheyenneCO', '8017'),
('County.Clear CreekCO', '8019'),
('County.ConejosCO', '8021'),
('County.CostillaCO', '8023'),
('County.CrowleyCO', '8025'),
('County.CusterCO', '8027'),
('County.DeltaCO', '8029'),
('County.DenverCO', '8031'),
('County.DoloresCO', '8033'),
('County.DouglasCO', '8035'),
('County.EagleCO', '8037'),
('County.El PasoCO', '8041'),
('County.ElbertCO', '8039'),
('County.FremontCO', '8043'),
('County.GarfieldCO', '8045'),
('County.GilpinCO', '8047'),
('County.GrandCO', '8049'),
('County.GunnisonCO', '8051'),
('County.HinsdaleCO', '8053'),
('County.HuerfanoCO', '8055'),
('County.JacksonCO', '8057'),
('County.JeffersonCO', '8059'),
('County.KiowaCO', '8061'),
('County.Kit CarsonCO', '8063'),
('County.La PlataCO', '8067'),
('County.LakeCO', '8065'),
('County.LarimerCO', '8069'),
('County.Las AnimasCO', '8071'),
('County.LincolnCO', '8073'),
('County.LoganCO', '8075'),
('County.MesaCO', '8077'),
('County.MineralCO', '8079'),
('County.MoffatCO', '8081'),
('County.MontezumaCO', '8083'),
('County.MontroseCO', '8085'),
('County.MorganCO', '8087'),
('County.OteroCO', '8089'),
('County.OurayCO', '8091'),
('County.ParkCO', '8093'),
('County.PhillipsCO', '8095'),
('County.PitkinCO', '8097'),
('County.ProwersCO', '8099'),
('County.PuebloCO', '8101'),
('County.Rio BlancoCO', '8103'),
('County.Rio GrandeCO', '8105'),
('County.RouttCO', '8107'),
('County.SaguacheCO', '8109'),
('County.San JuanCO', '8111'),
('County.San MiguelCO', '8113'),
('County.SedgwickCO', '8115'),
('County.SummitCO', '8117'),
('County.TellerCO', '8119'),
('County.WashingtonCO', '8121'),
('County.WeldCO', '8123'),
('County.YumaCO', '8125'),
('County.FairfieldCT', '9001'),
('County.HartfordCT', '9003'),
('County.LitchfieldCT', '9005'),
('County.MiddlesexCT', '9007'),
('County.New HavenCT', '9009'),
('County.New LondonCT', '9011'),
('County.TollandCT', '9013'),
('County.WindhamCT', '9015'),
('County.KentDE', '10001'),
('County.New CastleDE', '10003'),
('County.SussexDE', '10005'),
('County.District of ColumbiaDC', '11001'),
('County.AlachuaFL', '12001'),
('County.BakerFL', '12003'),
('County.BayFL', '12005'),
('County.BradfordFL', '12007'),
('County.BrevardFL', '12009'),
('County.BrowardFL', '12011'),
('County.CalhounFL', '12013'),
('County.CharlotteFL', '12015'),
('County.CitrusFL', '12017'),
('County.ClayFL', '12019'),
('County.CollierFL', '12021'),
('County.ColumbiaFL', '12023'),
('County.De SotoFL', '12027'),
('County.DixieFL', '12029'),
('County.DuvalFL', '12031'),
('County.EscambiaFL', '12033'),
('County.FlaglerFL', '12035'),
('County.FranklinFL', '12037'),
('County.GadsdenFL', '12039'),
('County.GilchristFL', '12041'),
('County.GladesFL', '12043'),
('County.GulfFL', '12045'),
('County.HamiltonFL', '12047'),
('County.HardeeFL', '12049'),
('County.HendryFL', '12051'),
('County.HernandoFL', '12053'),
('County.HighlandsFL', '12055'),
('County.HillsboroughFL', '12057'),
('County.HolmesFL', '12059'),
('County.Indian RiverFL', '12061'),
('County.JacksonFL', '12063'),
('County.JeffersonFL', '12065'),
('County.LafayetteFL', '12067'),
('County.LakeFL', '12069'),
('County.LeeFL', '12071'),
('County.LeonFL', '12073'),
('County.LevyFL', '12075'),
('County.LibertyFL', '12077'),
('County.MadisonFL', '12079'),
('County.ManateeFL', '12081'),
('County.MarionFL', '12083'),
('County.MartinFL', '12085'),
('County.Miami-DadeFL', '12086'),
('County.MonroeFL', '12087'),
('County.NassauFL', '12089'),
('County.OkaloosaFL', '12091'),
('County.OkeechobeeFL', '12093'),
('County.OrangeFL', '12095'),
('County.OsceolaFL', '12097'),
('County.Palm BeachFL', '12099'),
('County.PascoFL', '12101'),
('County.PinellasFL', '12103'),
('County.PolkFL', '12105'),
('County.PutnamFL', '12107'),
('County.Santa RosaFL', '12113'),
('County.SarasotaFL', '12115'),
('County.SeminoleFL', '12117'),
('County.St. JohnsFL', '12109'),
('County.St. LucieFL', '12111'),
('County.SumterFL', '12119'),
('County.SuwanneeFL', '12121'),
('County.TaylorFL', '12123'),
('County.UnionFL', '12125'),
('County.VolusiaFL', '12127'),
('County.WakullaFL', '12129'),
('County.WaltonFL', '12131'),
('County.WashingtonFL', '12133'),
('County.ApplingGA', '13001'),
('County.AtkinsonGA', '13003'),
('County.BaconGA', '13005'),
('County.BakerGA', '13007'),
('County.BaldwinGA', '13009'),
('County.BanksGA', '13011'),
('County.BarrowGA', '13013'),
('County.BartowGA', '13015'),
('County.Ben HillGA', '13017'),
('County.BerrienGA', '13019'),
('County.BibbGA', '13021'),
('County.BleckleyGA', '13023'),
('County.BrantleyGA', '13025'),
('County.BrooksGA', '13027'),
('County.BryanGA', '13029'),
('County.BullochGA', '13031'),
('County.BurkeGA', '13033'),
('County.ButtsGA', '13035'),
('County.CalhounGA', '13037'),
('County.CamdenGA', '13039'),
('County.CandlerGA', '13043'),
('County.CarrollGA', '13045'),
('County.CatoosaGA', '13047'),
('County.CharltonGA', '13049'),
('County.ChathamGA', '13051'),
('County.ChattahoocheeGA', '13053'),
('County.ChattoogaGA', '13055'),
('County.CherokeeGA', '13057'),
('County.ClarkeGA', '13059'),
('County.ClayGA', '13061'),
('County.ClaytonGA', '13063'),
('County.ClinchGA', '13065'),
('County.CobbGA', '13067'),
('County.CoffeeGA', '13069'),
('County.ColquittGA', '13071'),
('County.ColumbiaGA', '13073'),
('County.CookGA', '13075'),
('County.CowetaGA', '13077'),
('County.CrawfordGA', '13079'),
('County.CrispGA', '13081'),
('County.DadeGA', '13083'),
('County.DawsonGA', '13085'),
('County.DecaturGA', '13087'),
('County.DeKalbGA', '13089'),
('County.DodgeGA', '13091'),
('County.DoolyGA', '13093'),
('County.DoughertyGA', '13095'),
('County.DouglasGA', '13097'),
('County.EarlyGA', '13099'),
('County.EcholsGA', '13101'),
('County.EffinghamGA', '13103'),
('County.ElbertGA', '13105'),
('County.EmanuelGA', '13107'),
('County.EvansGA', '13109'),
('County.FanninGA', '13111'),
('County.FayetteGA', '13113'),
('County.FloydGA', '13115'),
('County.ForsythGA', '13117'),
('County.FranklinGA', '13119'),
('County.FultonGA', '13121'),
('County.GilmerGA', '13123'),
('County.GlascockGA', '13125'),
('County.GlynnGA', '13127'),
('County.GordonGA', '13129'),
('County.GradyGA', '13131'),
('County.GreeneGA', '13133'),
('County.GwinnettGA', '13135'),
('County.HabershamGA', '13137'),
('County.HallGA', '13139'),
('County.HancockGA', '13141'),
('County.HaralsonGA', '13143'),
('County.HarrisGA', '13145'),
('County.HartGA', '13147'),
('County.HeardGA', '13149'),
('County.HenryGA', '13151'),
('County.HoustonGA', '13153'),
('County.IrwinGA', '13155'),
('County.JacksonGA', '13157'),
('County.JasperGA', '13159'),
('County.Jeff DavisGA', '13161'),
('County.JeffersonGA', '13163'),
('County.JenkinsGA', '13165'),
('County.JohnsonGA', '13167'),
('County.JonesGA', '13169'),
('County.LamarGA', '13171'),
('County.LanierGA', '13173'),
('County.LaurensGA', '13175'),
('County.LeeGA', '13177'),
('County.LibertyGA', '13179'),
('County.LincolnGA', '13181'),
('County.LongGA', '13183'),
('County.LowndesGA', '13185'),
('County.LumpkinGA', '13187'),
('County.MaconGA', '13193'),
('County.MadisonGA', '13195'),
('County.MarionGA', '13197'),
('County.McDuffieGA', '13189'),
('County.McIntoshGA', '13191'),
('County.MeriwetherGA', '13199'),
('County.MillerGA', '13201'),
('County.MitchellGA', '13205'),
('County.MonroeGA', '13207'),
('County.MontgomeryGA', '13209'),
('County.MorganGA', '13211'),
('County.MurrayGA', '13213'),
('County.MuscogeeGA', '13215'),
('County.NewtonGA', '13217'),
('County.OconeeGA', '13219'),
('County.OglethorpeGA', '13221'),
('County.PauldingGA', '13223'),
('County.PeachGA', '13225'),
('County.PickensGA', '13227'),
('County.PierceGA', '13229'),
('County.PikeGA', '13231'),
('County.PolkGA', '13233'),
('County.PulaskiGA', '13235'),
('County.PutnamGA', '13237'),
('County.QuitmanGA', '13239'),
('County.RabunGA', '13241'),
('County.RandolphGA', '13243'),
('County.RichmondGA', '13245'),
('County.RockdaleGA', '13247'),
('County.SchleyGA', '13249'),
('County.ScrevenGA', '13251'),
('County.SeminoleGA', '13253'),
('County.SpaldingGA', '13255'),
('County.StephensGA', '13257'),
('County.StewartGA', '13259'),
('County.SumterGA', '13261'),
('County.TalbotGA', '13263'),
('County.TaliaferroGA', '13265'),
('County.TattnallGA', '13267'),
('County.TaylorGA', '13269'),
('County.TelfairGA', '13271'),
('County.TerrellGA', '13273'),
('County.ThomasGA', '13275'),
('County.TiftGA', '13277'),
('County.ToombsGA', '13279'),
('County.TownsGA', '13281'),
('County.TreutlenGA', '13283'),
('County.TroupGA', '13285'),
('County.TurnerGA', '13287'),
('County.TwiggsGA', '13289'),
('County.UnionGA', '13291'),
('County.UpsonGA', '13293'),
('County.WalkerGA', '13295'),
('County.WaltonGA', '13297'),
('County.WareGA', '13299'),
('County.WarrenGA', '13301'),
('County.WashingtonGA', '13303'),
('County.WayneGA', '13305'),
('County.WebsterGA', '13307'),
('County.WheelerGA', '13309'),
('County.WhiteGA', '13311'),
('County.WhitfieldGA', '13313'),
('County.WilcoxGA', '13315'),
('County.WilkesGA', '13317'),
('County.WilkinsonGA', '13319'),
('County.WorthGA', '13321'),
('County.GuamGU', '66010'),
('County.HawaiiHI', '15001'),
('County.HonoluluHI', '15003'),
('County.KauaiHI', '15007'),
('County.MauiHI', '15009'),
('County.AdaID', '16001'),
('County.AdamsID', '16003'),
('County.BannockID', '16005'),
('County.Bear LakeID', '16007'),
('County.BenewahID', '16009'),
('County.BinghamID', '16011'),
('County.BlaineID', '16013'),
('County.BoiseID', '16015'),
('County.BonnerID', '16017'),
('County.BonnevilleID', '16019'),
('County.BoundaryID', '16021'),
('County.ButteID', '16023'),
('County.CamasID', '16025'),
('County.CanyonID', '16027'),
('County.CaribouID', '16029'),
('County.CassiaID', '16031'),
('County.ClarkID', '16033'),
('County.ClearwaterID', '16035'),
('County.CusterID', '16037'),
('County.ElmoreID', '16039'),
('County.FranklinID', '16041'),
('County.FremontID', '16043'),
('County.GemID', '16045'),
('County.GoodingID', '16047'),
('County.IdahoID', '16049'),
('County.JeffersonID', '16051'),
('County.JeromeID', '16053'),
('County.KootenaiID', '16055'),
('County.LatahID', '16057'),
('County.LemhiID', '16059'),
('County.LewisID', '16061'),
('County.LincolnID', '16063'),
('County.MadisonID', '16065'),
('County.MinidokaID', '16067'),
('County.Nez PerceID', '16069'),
('County.OneidaID', '16071'),
('County.OwyheeID', '16073'),
('County.PayetteID', '16075'),
('County.PowerID', '16077'),
('County.ShoshoneID', '16079'),
('County.TetonID', '16081'),
('County.Twin FallsID', '16083'),
('County.ValleyID', '16085'),
('County.WashingtonID', '16087'),
('County.AdamsIL', '17001'),
('County.BondIL', '17005'),
('County.BooneIL', '17007'),
('County.BrownIL', '17009'),
('County.BureauIL', '17011'),
('County.CalhounIL', '17013'),
('County.CarrollIL', '17015'),
('County.CassIL', '17017'),
('County.ChampaignIL', '17019'),
('County.ChristianIL', '17021'),
('County.ClarkIL', '17023'),
('County.ClayIL', '17025'),
('County.ClintonIL', '17027'),
('County.ColesIL', '17029'),
('County.CookIL', '17031'),
('County.CrawfordIL', '17033'),
('County.CumberlandIL', '17035'),
('County.De WittIL', '17039'),
('County.DeKalbIL', '17037'),
('County.DouglasIL', '17041'),
('County.DuPageIL', '17043'),
('County.EdgarIL', '17045'),
('County.EffinghamIL', '17049'),
('County.FayetteIL', '17051'),
('County.FordIL', '17053'),
('County.FultonIL', '17057'),
('County.GreeneIL', '17061'),
('County.GrundyIL', '17063'),
('County.HancockIL', '17067'),
('County.HendersonIL', '17071'),
('County.HenryIL', '17073'),
('County.IroquoisIL', '17075'),
('County.JasperIL', '17079'),
('County.JeffersonIL', '17081'),
('County.JerseyIL', '17083'),
('County.Jo DaviessIL', '17085'),
('County.KaneIL', '17089'),
('County.KankakeeIL', '17091'),
('County.KendallIL', '17093'),
('County.KnoxIL', '17095'),
('County.La SalleIL', '17099'),
('County.LakeIL', '17097'),
('County.LawrenceIL', '17101'),
('County.LeeIL', '17103'),
('County.LivingstonIL', '17105'),
('County.LoganIL', '17107'),
('County.MaconIL', '17115'),
('County.MacoupinIL', '17117'),
('County.MadisonIL', '17119'),
('County.MarionIL', '17121'),
('County.MarshallIL', '17123'),
('County.MasonIL', '17125'),
('County.McDonoughIL', '17109'),
('County.McHenryIL', '17111'),
('County.McLeanIL', '17113'),
('County.MenardIL', '17129'),
('County.MercerIL', '17131'),
('County.MonroeIL', '17133'),
('County.MontgomeryIL', '17135'),
('County.MorganIL', '17137'),
('County.MoultrieIL', '17139'),
('County.OgleIL', '17141'),
('County.PeoriaIL', '17143'),
('County.PerryIL', '17145'),
('County.PiattIL', '17147'),
('County.PikeIL', '17149'),
('County.PutnamIL', '17155'),
('County.RandolphIL', '17157'),
('County.RichlandIL', '17159'),
('County.Rock IslandIL', '17161'),
('County.SangamonIL', '17167'),
('County.SchuylerIL', '17169'),
('County.ScottIL', '17171'),
('County.ShelbyIL', '17173'),
('County.St. ClairIL', '17163'),
('County.StarkIL', '17175'),
('County.StephensonIL', '17177'),
('County.TazewellIL', '17179'),
('County.VermilionIL', '17183'),
('County.WarrenIL', '17187'),
('County.WashingtonIL', '17189'),
('County.WhitesideIL', '17195'),
('County.WillIL', '17197'),
('County.WinnebagoIL', '17201'),
('County.WoodfordIL', '17203'),
('County.AdamsIN', '18001'),
('County.AllenIN', '18003'),
('County.BentonIN', '18007'),
('County.BlackfordIN', '18009'),
('County.BooneIN', '18011'),
('County.BrownIN', '18013'),
('County.CarrollIN', '18015'),
('County.CassIN', '18017'),
('County.ClayIN', '18021'),
('County.ClintonIN', '18023'),
('County.DaviessIN', '18027'),
('County.DeKalbIN', '18033'),
('County.DelawareIN', '18035'),
('County.ElkhartIN', '18039'),
('County.FountainIN', '18045'),
('County.FultonIN', '18049'),
('County.GrantIN', '18053'),
('County.GreeneIN', '18055'),
('County.HamiltonIN', '18057'),
('County.HancockIN', '18059'),
('County.HendricksIN', '18063'),
('County.HenryIN', '18065'),
('County.HowardIN', '18067'),
('County.HuntingtonIN', '18069'),
('County.JasperIN', '18073'),
('County.JayIN', '18075'),
('County.JohnsonIN', '18081'),
('County.KosciuskoIN', '18085'),
('County.La PorteIN', '18091'),
('County.LagrangeIN', '18087'),
('County.LakeIN', '18089'),
('County.MadisonIN', '18095'),
('County.MarionIN', '18097'),
('County.MarshallIN', '18099'),
('County.MartinIN', '18101'),
('County.MiamiIN', '18103'),
('County.MonroeIN', '18105'),
('County.MontgomeryIN', '18107'),
('County.MorganIN', '18109'),
('County.NewtonIN', '18111'),
('County.NobleIN', '18113'),
('County.OwenIN', '18119'),
('County.ParkeIN', '18121'),
('County.PorterIN', '18127'),
('County.PulaskiIN', '18131'),
('County.PutnamIN', '18133'),
('County.RandolphIN', '18135'),
('County.ShelbyIN', '18145'),
('County.St. JosephIN', '18141'),
('County.StarkeIN', '18149'),
('County.SteubenIN', '18151'),
('County.SullivanIN', '18153'),
('County.TippecanoeIN', '18157'),
('County.TiptonIN', '18159'),
('County.VermillionIN', '18165'),
('County.VigoIN', '18167'),
('County.WabashIN', '18169'),
('County.WarrenIN', '18171'),
('County.WellsIN', '18179'),
('County.WhiteIN', '18181'),
('County.WhitleyIN', '18183'),
('County.AdairIA', '19001'),
('County.AdamsIA', '19003'),
('County.AllamakeeIA', '19005'),
('County.AppanooseIA', '19007'),
('County.AudubonIA', '19009'),
('County.BentonIA', '19011'),
('County.Black HawkIA', '19013'),
('County.BooneIA', '19015'),
('County.BremerIA', '19017'),
('County.BuchananIA', '19019')

INSERT INTO @LookupMapper ([Key], ExternalId)
VALUES
('County.Buena VistaIA', '19021'),
('County.ButlerIA', '19023'),
('County.CalhounIA', '19025'),
('County.CarrollIA', '19027'),
('County.CassIA', '19029'),
('County.CedarIA', '19031'),
('County.Cerro GordoIA', '19033'),
('County.CherokeeIA', '19035'),
('County.ChickasawIA', '19037'),
('County.ClarkeIA', '19039'),
('County.ClayIA', '19041'),
('County.ClaytonIA', '19043'),
('County.ClintonIA', '19045'),
('County.CrawfordIA', '19047'),
('County.DallasIA', '19049'),
('County.DavisIA', '19051'),
('County.DecaturIA', '19053'),
('County.DelawareIA', '19055'),
('County.Des MoinesIA', '19057'),
('County.DickinsonIA', '19059'),
('County.DubuqueIA', '19061'),
('County.EmmetIA', '19063'),
('County.FayetteIA', '19065'),
('County.FloydIA', '19067'),
('County.FranklinIA', '19069'),
('County.FremontIA', '19071'),
('County.GreeneIA', '19073'),
('County.GrundyIA', '19075'),
('County.GuthrieIA', '19077'),
('County.HamiltonIA', '19079'),
('County.HancockIA', '19081'),
('County.HardinIA', '19083'),
('County.HarrisonIA', '19085'),
('County.HenryIA', '19087'),
('County.HowardIA', '19089'),
('County.HumboldtIA', '19091'),
('County.IdaIA', '19093'),
('County.IowaIA', '19095'),
('County.JacksonIA', '19097'),
('County.JasperIA', '19099'),
('County.JeffersonIA', '19101'),
('County.JohnsonIA', '19103'),
('County.JonesIA', '19105'),
('County.KeokukIA', '19107'),
('County.KossuthIA', '19109'),
('County.LeeIA', '19111'),
('County.LinnIA', '19113'),
('County.LouisaIA', '19115'),
('County.LucasIA', '19117'),
('County.LyonIA', '19119'),
('County.MadisonIA', '19121'),
('County.MahaskaIA', '19123'),
('County.MarionIA', '19125'),
('County.MarshallIA', '19127'),
('County.MillsIA', '19129'),
('County.MitchellIA', '19131'),
('County.MononaIA', '19133'),
('County.MonroeIA', '19135'),
('County.MontgomeryIA', '19137'),
('County.MuscatineIA', '19139'),
('County.O''BrienIA', '19141'),
('County.OsceolaIA', '19143'),
('County.PageIA', '19145'),
('County.Palo AltoIA', '19147'),
('County.PlymouthIA', '19149'),
('County.PocahontasIA', '19151'),
('County.PolkIA', '19153'),
('County.PottawattamieIA', '19155'),
('County.PoweshiekIA', '19157'),
('County.RinggoldIA', '19159'),
('County.SacIA', '19161'),
('County.ScottIA', '19163'),
('County.ShelbyIA', '19165'),
('County.SiouxIA', '19167'),
('County.StoryIA', '19169'),
('County.TamaIA', '19171'),
('County.TaylorIA', '19173'),
('County.UnionIA', '19175'),
('County.Van BurenIA', '19177'),
('County.WapelloIA', '19179'),
('County.WarrenIA', '19181'),
('County.WashingtonIA', '19183'),
('County.WayneIA', '19185'),
('County.WebsterIA', '19187'),
('County.WinnebagoIA', '19189'),
('County.WinneshiekIA', '19191'),
('County.WoodburyIA', '19193'),
('County.WorthIA', '19195'),
('County.WrightIA', '19197'),
('County.AllenKS', '20001'),
('County.AndersonKS', '20003'),
('County.AtchisonKS', '20005'),
('County.BarberKS', '20007'),
('County.BartonKS', '20009'),
('County.BourbonKS', '20011'),
('County.BrownKS', '20013'),
('County.ButlerKS', '20015'),
('County.ChaseKS', '20017'),
('County.ChautauquaKS', '20019'),
('County.CherokeeKS', '20021'),
('County.CheyenneKS', '20023'),
('County.ClarkKS', '20025'),
('County.ClayKS', '20027'),
('County.CloudKS', '20029'),
('County.CoffeyKS', '20031'),
('County.ComancheKS', '20033'),
('County.CowleyKS', '20035'),
('County.CrawfordKS', '20037'),
('County.DecaturKS', '20039'),
('County.DickinsonKS', '20041'),
('County.DoniphanKS', '20043'),
('County.DouglasKS', '20045'),
('County.EdwardsKS', '20047'),
('County.ElkKS', '20049'),
('County.EllisKS', '20051'),
('County.EllsworthKS', '20053'),
('County.FinneyKS', '20055'),
('County.FordKS', '20057'),
('County.FranklinKS', '20059'),
('County.GearyKS', '20061'),
('County.GoveKS', '20063'),
('County.GrahamKS', '20065'),
('County.GrantKS', '20067'),
('County.GrayKS', '20069'),
('County.GreeleyKS', '20071'),
('County.GreenwoodKS', '20073'),
('County.HamiltonKS', '20075'),
('County.HarperKS', '20077'),
('County.HarveyKS', '20079'),
('County.HaskellKS', '20081'),
('County.HodgemanKS', '20083'),
('County.JacksonKS', '20085'),
('County.JeffersonKS', '20087'),
('County.JewellKS', '20089'),
('County.JohnsonKS', '20091'),
('County.KearnyKS', '20093'),
('County.KingmanKS', '20095'),
('County.KiowaKS', '20097'),
('County.LabetteKS', '20099'),
('County.LaneKS', '20101'),
('County.LeavenworthKS', '20103'),
('County.LincolnKS', '20105'),
('County.LinnKS', '20107'),
('County.LoganKS', '20109'),
('County.LyonKS', '20111'),
('County.MarionKS', '20115'),
('County.MarshallKS', '20117'),
('County.McPhersonKS', '20113'),
('County.MeadeKS', '20119'),
('County.MiamiKS', '20121'),
('County.MitchellKS', '20123'),
('County.MontgomeryKS', '20125'),
('County.MorrisKS', '20127'),
('County.MortonKS', '20129'),
('County.NemahaKS', '20131'),
('County.NeoshoKS', '20133'),
('County.NessKS', '20135'),
('County.NortonKS', '20137'),
('County.OsageKS', '20139'),
('County.OsborneKS', '20141'),
('County.OttawaKS', '20143'),
('County.PawneeKS', '20145'),
('County.PhillipsKS', '20147'),
('County.PottawatomieKS', '20149'),
('County.PrattKS', '20151'),
('County.RawlinsKS', '20153'),
('County.RenoKS', '20155'),
('County.RepublicKS', '20157'),
('County.RiceKS', '20159'),
('County.RileyKS', '20161'),
('County.RooksKS', '20163'),
('County.RushKS', '20165'),
('County.RussellKS', '20167'),
('County.SalineKS', '20169'),
('County.ScottKS', '20171'),
('County.SedgwickKS', '20173'),
('County.SewardKS', '20175'),
('County.ShawneeKS', '20177'),
('County.SheridanKS', '20179'),
('County.ShermanKS', '20181'),
('County.SmithKS', '20183'),
('County.StaffordKS', '20185'),
('County.StantonKS', '20187'),
('County.StevensKS', '20189'),
('County.SumnerKS', '20191'),
('County.ThomasKS', '20193'),
('County.TregoKS', '20195'),
('County.WabaunseeKS', '20197'),
('County.WallaceKS', '20199'),
('County.WashingtonKS', '20201'),
('County.WichitaKS', '20203'),
('County.WilsonKS', '20205'),
('County.WoodsonKS', '20207'),
('County.WyandotteKS', '20209'),
('County.AcadiaLA', '22001'),
('County.AllenLA', '22003'),
('County.AscensionLA', '22005'),
('County.AssumptionLA', '22007'),
('County.AvoyellesLA', '22009'),
('County.BeauregardLA', '22011'),
('County.BienvilleLA', '22013'),
('County.BossierLA', '22015'),
('County.CaddoLA', '22017'),
('County.CalcasieuLA', '22019'),
('County.CaldwellLA', '22021'),
('County.CameronLA', '22023'),
('County.CatahoulaLA', '22025'),
('County.ClaiborneLA', '22027'),
('County.ConcordiaLA', '22029'),
('County.De SotoLA', '22031'),
('County.East Baton RougeLA', '22033'),
('County.East CarrollLA', '22035'),
('County.East FelicianaLA', '22037'),
('County.EvangelineLA', '22039'),
('County.FranklinLA', '22041'),
('County.GrantLA', '22043'),
('County.IberiaLA', '22045'),
('County.IbervilleLA', '22047'),
('County.JacksonLA', '22049'),
('County.JeffersonLA', '22051'),
('County.Jefferson DavisLA', '22053'),
('County.La SalleLA', '22059'),
('County.LafayetteLA', '22055'),
('County.LafourcheLA', '22057'),
('County.LincolnLA', '22061'),
('County.LivingstonLA', '22063'),
('County.MadisonLA', '22065'),
('County.MorehouseLA', '22067'),
('County.NatchitochesLA', '22069'),
('County.OrleansLA', '22071'),
('County.OuachitaLA', '22073'),
('County.PlaqueminesLA', '22075'),
('County.Pointe CoupeeLA', '22077'),
('County.RapidesLA', '22079'),
('County.Red RiverLA', '22081'),
('County.RichlandLA', '22083'),
('County.SabineLA', '22085'),
('County.St. BernardLA', '22087'),
('County.St. CharlesLA', '22089'),
('County.St. HelenaLA', '22091'),
('County.St. JamesLA', '22093'),
('County.St. John the BaptistLA', '22095'),
('County.St. LandryLA', '22097'),
('County.St. MartinLA', '22099'),
('County.St. MaryLA', '22101'),
('County.St. TammanyLA', '22103'),
('County.TangipahoaLA', '22105'),
('County.TensasLA', '22107'),
('County.TerrebonneLA', '22109'),
('County.UnionLA', '22111'),
('County.VermilionLA', '22113'),
('County.VernonLA', '22115'),
('County.WashingtonLA', '22117'),
('County.WebsterLA', '22119'),
('County.West Baton RougeLA', '22121'),
('County.West CarrollLA', '22123'),
('County.West FelicianaLA', '22125'),
('County.WinnLA', '22127'),
('County.AndroscogginME', '23001'),
('County.AroostookME', '23003'),
('County.CumberlandME', '23005'),
('County.FranklinME', '23007'),
('County.HancockME', '23009'),
('County.KennebecME', '23011'),
('County.KnoxME', '23013'),
('County.LincolnME', '23015'),
('County.OxfordME', '23017'),
('County.PenobscotME', '23019'),
('County.PiscataquisME', '23021'),
('County.SagadahocME', '23023'),
('County.SomersetME', '23025'),
('County.WaldoME', '23027'),
('County.WashingtonME', '23029'),
('County.YorkME', '23031'),
('County.AlleganyMD', '24001'),
('County.Anne ArundelMD', '24003'),
('County.BaltimoreMD', '24005'),
('County.CalvertMD', '24009'),
('County.CarolineMD', '24011'),
('County.CarrollMD', '24013'),
('County.CecilMD', '24015'),
('County.CharlesMD', '24017'),
('County.City of BaltimoreMD', '24510'),
('County.DorchesterMD', '24019'),
('County.FrederickMD', '24021'),
('County.GarrettMD', '24023'),
('County.HarfordMD', '24025'),
('County.HowardMD', '24027'),
('County.KentMD', '24029'),
('County.MontgomeryMD', '24031'),
('County.Prince George''sMD', '24033'),
('County.Queen Anne''sMD', '24035'),
('County.SomersetMD', '24039'),
('County.St. Mary''sMD', '24037'),
('County.TalbotMD', '24041'),
('County.WashingtonMD', '24043'),
('County.WicomicoMD', '24045'),
('County.WorcesterMD', '24047'),
('County.BarnstableMA', '25001'),
('County.BerkshireMA', '25003'),
('County.BristolMA', '25005'),
('County.DukesMA', '25007'),
('County.EssexMA', '25009'),
('County.FranklinMA', '25011'),
('County.HampdenMA', '25013'),
('County.HampshireMA', '25015'),
('County.MiddlesexMA', '25017'),
('County.NantucketMA', '25019'),
('County.NorfolkMA', '25021'),
('County.PlymouthMA', '25023'),
('County.SuffolkMA', '25025'),
('County.WorcesterMA', '25027'),
('County.AlconaMI', '26001'),
('County.AlgerMI', '26003'),
('County.AlleganMI', '26005'),
('County.AlpenaMI', '26007'),
('County.AntrimMI', '26009'),
('County.ArenacMI', '26011'),
('County.BaragaMI', '26013'),
('County.BarryMI', '26015'),
('County.BayMI', '26017'),
('County.BenzieMI', '26019'),
('County.BerrienMI', '26021'),
('County.BranchMI', '26023'),
('County.CalhounMI', '26025'),
('County.CassMI', '26027'),
('County.CharlevoixMI', '26029'),
('County.CheboyganMI', '26031'),
('County.ChippewaMI', '26033'),
('County.ClareMI', '26035'),
('County.ClintonMI', '26037'),
('County.CrawfordMI', '26039'),
('County.DeltaMI', '26041'),
('County.DickinsonMI', '26043'),
('County.EatonMI', '26045'),
('County.EmmetMI', '26047'),
('County.GeneseeMI', '26049'),
('County.GladwinMI', '26051'),
('County.GogebicMI', '26053'),
('County.Grand TraverseMI', '26055'),
('County.GratiotMI', '26057'),
('County.HillsdaleMI', '26059'),
('County.HoughtonMI', '26061'),
('County.HuronMI', '26063'),
('County.InghamMI', '26065'),
('County.IoniaMI', '26067'),
('County.IoscoMI', '26069'),
('County.IronMI', '26071'),
('County.IsabellaMI', '26073'),
('County.JacksonMI', '26075'),
('County.KalamazooMI', '26077'),
('County.KalkaskaMI', '26079'),
('County.KentMI', '26081'),
('County.KeweenawMI', '26083'),
('County.LakeMI', '26085'),
('County.LapeerMI', '26087'),
('County.LeelanauMI', '26089'),
('County.LenaweeMI', '26091'),
('County.LivingstonMI', '26093'),
('County.LuceMI', '26095'),
('County.MackinacMI', '26097'),
('County.MacombMI', '26099'),
('County.ManisteeMI', '26101'),
('County.MarquetteMI', '26103'),
('County.MasonMI', '26105'),
('County.MecostaMI', '26107'),
('County.MenomineeMI', '26109'),
('County.MidlandMI', '26111'),
('County.MissaukeeMI', '26113'),
('County.MonroeMI', '26115'),
('County.MontcalmMI', '26117'),
('County.MontmorencyMI', '26119'),
('County.MuskegonMI', '26121'),
('County.NewaygoMI', '26123'),
('County.OaklandMI', '26125'),
('County.OceanaMI', '26127'),
('County.OgemawMI', '26129'),
('County.OntonagonMI', '26131'),
('County.OsceolaMI', '26133'),
('County.OscodaMI', '26135'),
('County.OtsegoMI', '26137'),
('County.OttawaMI', '26139'),
('County.Presque IsleMI', '26141'),
('County.RoscommonMI', '26143'),
('County.SaginawMI', '26145'),
('County.SanilacMI', '26151'),
('County.SchoolcraftMI', '26153'),
('County.ShiawasseeMI', '26155'),
('County.St. ClairMI', '26147'),
('County.St. JosephMI', '26149'),
('County.TuscolaMI', '26157'),
('County.Van BurenMI', '26159'),
('County.WashtenawMI', '26161'),
('County.WayneMI', '26163'),
('County.WexfordMI', '26165'),
('County.AitkinMN', '27001'),
('County.AnokaMN', '27003'),
('County.BeckerMN', '27005'),
('County.BeltramiMN', '27007'),
('County.BentonMN', '27009'),
('County.Big StoneMN', '27011'),
('County.Blue EarthMN', '27013'),
('County.BrownMN', '27015'),
('County.CarltonMN', '27017'),
('County.CarverMN', '27019'),
('County.CassMN', '27021'),
('County.ChippewaMN', '27023'),
('County.ChisagoMN', '27025'),
('County.ClayMN', '27027'),
('County.ClearwaterMN', '27029'),
('County.CookMN', '27031'),
('County.CottonwoodMN', '27033'),
('County.Crow WingMN', '27035'),
('County.DakotaMN', '27037'),
('County.DodgeMN', '27039'),
('County.DouglasMN', '27041'),
('County.FaribaultMN', '27043'),
('County.FillmoreMN', '27045'),
('County.FreebornMN', '27047'),
('County.GoodhueMN', '27049'),
('County.GrantMN', '27051'),
('County.HennepinMN', '27053'),
('County.HoustonMN', '27055'),
('County.HubbardMN', '27057'),
('County.IsantiMN', '27059'),
('County.ItascaMN', '27061'),
('County.JacksonMN', '27063'),
('County.KanabecMN', '27065'),
('County.KandiyohiMN', '27067'),
('County.KittsonMN', '27069'),
('County.KoochichingMN', '27071'),
('County.Lac qui ParleMN', '27073'),
('County.LakeMN', '27075'),
('County.Lake of the WoodsMN', '27077'),
('County.Le SueurMN', '27079'),
('County.LincolnMN', '27081'),
('County.LyonMN', '27083'),
('County.MahnomenMN', '27087'),
('County.MarshallMN', '27089'),
('County.MartinMN', '27091'),
('County.McLeodMN', '27085'),
('County.MeekerMN', '27093'),
('County.Mille LacsMN', '27095'),
('County.MorrisonMN', '27097'),
('County.MowerMN', '27099'),
('County.MurrayMN', '27101'),
('County.NicolletMN', '27103'),
('County.NoblesMN', '27105'),
('County.NormanMN', '27107'),
('County.OlmstedMN', '27109'),
('County.Otter TailMN', '27111'),
('County.PenningtonMN', '27113'),
('County.PineMN', '27115'),
('County.PipestoneMN', '27117'),
('County.PolkMN', '27119'),
('County.PopeMN', '27121'),
('County.RamseyMN', '27123'),
('County.Red LakeMN', '27125'),
('County.RedwoodMN', '27127'),
('County.RenvilleMN', '27129'),
('County.RiceMN', '27131'),
('County.RockMN', '27133'),
('County.RoseauMN', '27135'),
('County.ScottMN', '27139'),
('County.SherburneMN', '27141'),
('County.SibleyMN', '27143'),
('County.St. LouisMN', '27137'),
('County.StearnsMN', '27145'),
('County.SteeleMN', '27147'),
('County.StevensMN', '27149'),
('County.SwiftMN', '27151'),
('County.ToddMN', '27153'),
('County.TraverseMN', '27155'),
('County.WabashaMN', '27157'),
('County.WadenaMN', '27159'),
('County.WasecaMN', '27161'),
('County.WashingtonMN', '27163'),
('County.WatonwanMN', '27165'),
('County.WilkinMN', '27167'),
('County.WinonaMN', '27169'),
('County.WrightMN', '27171'),
('County.Yellow MedicineMN', '27173'),
('County.AdamsMS', '28001'),
('County.AlcornMS', '28003'),
('County.AmiteMS', '28005'),
('County.AttalaMS', '28007'),
('County.BentonMS', '28009'),
('County.BolivarMS', '28011'),
('County.CalhounMS', '28013'),
('County.CarrollMS', '28015'),
('County.ChickasawMS', '28017'),
('County.ChoctawMS', '28019'),
('County.ClaiborneMS', '28021'),
('County.ClarkeMS', '28023'),
('County.ClayMS', '28025'),
('County.CoahomaMS', '28027'),
('County.CopiahMS', '28029'),
('County.CovingtonMS', '28031'),
('County.DeSotoMS', '28033'),
('County.ForrestMS', '28035'),
('County.FranklinMS', '28037'),
('County.GeorgeMS', '28039'),
('County.GreeneMS', '28041'),
('County.GrenadaMS', '28043'),
('County.HancockMS', '28045'),
('County.HarrisonMS', '28047'),
('County.HindsMS', '28049'),
('County.HolmesMS', '28051'),
('County.HumphreysMS', '28053'),
('County.IssaquenaMS', '28055'),
('County.ItawambaMS', '28057'),
('County.JacksonMS', '28059'),
('County.JasperMS', '28061'),
('County.JeffersonMS', '28063'),
('County.Jefferson DavisMS', '28065'),
('County.JonesMS', '28067'),
('County.KemperMS', '28069'),
('County.LafayetteMS', '28071'),
('County.LamarMS', '28073'),
('County.LauderdaleMS', '28075'),
('County.LawrenceMS', '28077'),
('County.LeakeMS', '28079'),
('County.LeeMS', '28081'),
('County.LefloreMS', '28083'),
('County.LincolnMS', '28085'),
('County.LowndesMS', '28087'),
('County.MadisonMS', '28089'),
('County.MarionMS', '28091'),
('County.MarshallMS', '28093'),
('County.MonroeMS', '28095'),
('County.MontgomeryMS', '28097'),
('County.NeshobaMS', '28099'),
('County.NewtonMS', '28101'),
('County.NoxubeeMS', '28103'),
('County.OktibbehaMS', '28105'),
('County.PanolaMS', '28107'),
('County.Pearl RiverMS', '28109'),
('County.PerryMS', '28111'),
('County.PikeMS', '28113'),
('County.PontotocMS', '28115'),
('County.PrentissMS', '28117'),
('County.QuitmanMS', '28119'),
('County.RankinMS', '28121'),
('County.ScottMS', '28123'),
('County.SharkeyMS', '28125'),
('County.SimpsonMS', '28127'),
('County.SmithMS', '28129'),
('County.StoneMS', '28131'),
('County.SunflowerMS', '28133'),
('County.TallahatchieMS', '28135'),
('County.TateMS', '28137'),
('County.TippahMS', '28139'),
('County.TishomingoMS', '28141'),
('County.TunicaMS', '28143'),
('County.UnionMS', '28145'),
('County.WalthallMS', '28147'),
('County.WarrenMS', '28149'),
('County.WashingtonMS', '28151'),
('County.WayneMS', '28153'),
('County.WebsterMS', '28155'),
('County.WilkinsonMS', '28157'),
('County.WinstonMS', '28159'),
('County.YalobushaMS', '28161'),
('County.YazooMS', '28163'),
('County.AdairMO', '29001'),
('County.AndrewMO', '29003'),
('County.AtchisonMO', '29005'),
('County.AudrainMO', '29007'),
('County.BarryMO', '29009'),
('County.BartonMO', '29011'),
('County.BatesMO', '29013'),
('County.BentonMO', '29015'),
('County.BooneMO', '29019'),
('County.BuchananMO', '29021'),
('County.ButlerMO', '29023'),
('County.CaldwellMO', '29025'),
('County.CallawayMO', '29027'),
('County.CamdenMO', '29029'),
('County.Cape GirardeauMO', '29031'),
('County.CarrollMO', '29033'),
('County.CarterMO', '29035'),
('County.CassMO', '29037'),
('County.CedarMO', '29039'),
('County.CharitonMO', '29041'),
('County.ChristianMO', '29043'),
('County.City of St. LouisMO', '29510'),
('County.ClarkMO', '29045'),
('County.ClayMO', '29047'),
('County.ClintonMO', '29049'),
('County.ColeMO', '29051'),
('County.CooperMO', '29053'),
('County.CrawfordMO', '29055'),
('County.DadeMO', '29057'),
('County.DallasMO', '29059'),
('County.DaviessMO', '29061'),
('County.DeKalbMO', '29063'),
('County.DentMO', '29065'),
('County.DouglasMO', '29067'),
('County.FranklinMO', '29071'),
('County.GasconadeMO', '29073'),
('County.GentryMO', '29075'),
('County.GreeneMO', '29077'),
('County.GrundyMO', '29079'),
('County.HarrisonMO', '29081'),
('County.HenryMO', '29083'),
('County.HickoryMO', '29085'),
('County.HoltMO', '29087'),
('County.HowardMO', '29089'),
('County.HowellMO', '29091'),
('County.IronMO', '29093'),
('County.JacksonMO', '29095'),
('County.JasperMO', '29097'),
('County.JeffersonMO', '29099'),
('County.JohnsonMO', '29101'),
('County.KnoxMO', '29103'),
('County.LacledeMO', '29105'),
('County.LafayetteMO', '29107'),
('County.LawrenceMO', '29109'),
('County.LewisMO', '29111'),
('County.LincolnMO', '29113'),
('County.LinnMO', '29115'),
('County.LivingstonMO', '29117'),
('County.MaconMO', '29121'),
('County.MadisonMO', '29123'),
('County.MariesMO', '29125'),
('County.MarionMO', '29127'),
('County.McDonaldMO', '29119'),
('County.MercerMO', '29129'),
('County.MillerMO', '29131'),
('County.MoniteauMO', '29135'),
('County.MonroeMO', '29137'),
('County.MontgomeryMO', '29139'),
('County.MorganMO', '29141'),
('County.New MadridMO', '29143'),
('County.NewtonMO', '29145'),
('County.NodawayMO', '29147'),
('County.OregonMO', '29149'),
('County.OsageMO', '29151'),
('County.OzarkMO', '29153'),
('County.PerryMO', '29157'),
('County.PettisMO', '29159'),
('County.PhelpsMO', '29161'),
('County.PikeMO', '29163'),
('County.PlatteMO', '29165'),
('County.PolkMO', '29167'),
('County.PulaskiMO', '29169'),
('County.PutnamMO', '29171'),
('County.RallsMO', '29173'),
('County.RandolphMO', '29175'),
('County.RayMO', '29177'),
('County.ReynoldsMO', '29179'),
('County.RipleyMO', '29181'),
('County.SalineMO', '29195'),
('County.SchuylerMO', '29197'),
('County.ScotlandMO', '29199'),
('County.ShannonMO', '29203'),
('County.ShelbyMO', '29205'),
('County.St. CharlesMO', '29183'),
('County.St. ClairMO', '29185'),
('County.St. FrancoisMO', '29187'),
('County.St. LouisMO', '29189'),
('County.Ste. GenevieveMO', '29186'),
('County.StoneMO', '29209'),
('County.SullivanMO', '29211'),
('County.TaneyMO', '29213'),
('County.TexasMO', '29215'),
('County.VernonMO', '29217'),
('County.WarrenMO', '29219'),
('County.WashingtonMO', '29221'),
('County.WayneMO', '29223'),
('County.WebsterMO', '29225'),
('County.WorthMO', '29227'),
('County.WrightMO', '29229'),
('County.BeaverheadMT', '30001'),
('County.Big HornMT', '30003'),
('County.BlaineMT', '30005'),
('County.BroadwaterMT', '30007'),
('County.CarbonMT', '30009'),
('County.CarterMT', '30011'),
('County.CascadeMT', '30013'),
('County.ChouteauMT', '30015'),
('County.CusterMT', '30017'),
('County.DanielsMT', '30019'),
('County.DawsonMT', '30021'),
('County.Deer LodgeMT', '30023'),
('County.FallonMT', '30025'),
('County.FergusMT', '30027'),
('County.FlatheadMT', '30029'),
('County.GallatinMT', '30031'),
('County.GarfieldMT', '30033'),
('County.GlacierMT', '30035'),
('County.Golden ValleyMT', '30037'),
('County.GraniteMT', '30039'),
('County.HillMT', '30041'),
('County.JeffersonMT', '30043'),
('County.Judith BasinMT', '30045'),
('County.LakeMT', '30047'),
('County.Lewis and ClarkMT', '30049'),
('County.LibertyMT', '30051'),
('County.LincolnMT', '30053'),
('County.MadisonMT', '30057'),
('County.McConeMT', '30055'),
('County.MeagherMT', '30059'),
('County.MineralMT', '30061'),
('County.MissoulaMT', '30063'),
('County.MusselshellMT', '30065'),
('County.ParkMT', '30067'),
('County.PetroleumMT', '30069'),
('County.PhillipsMT', '30071'),
('County.PonderaMT', '30073'),
('County.Powder RiverMT', '30075'),
('County.PowellMT', '30077'),
('County.PrairieMT', '30079'),
('County.RavalliMT', '30081'),
('County.RichlandMT', '30083'),
('County.RooseveltMT', '30085'),
('County.RosebudMT', '30087'),
('County.SandersMT', '30089'),
('County.SheridanMT', '30091'),
('County.Silver BowMT', '30093'),
('County.StillwaterMT', '30095'),
('County.Sweet GrassMT', '30097'),
('County.TetonMT', '30099'),
('County.TooleMT', '30101'),
('County.TreasureMT', '30103'),
('County.ValleyMT', '30105'),
('County.WheatlandMT', '30107'),
('County.WibauxMT', '30109'),
('County.YellowstoneMT', '30111'),
('County.AdamsNE', '31001'),
('County.AntelopeNE', '31003'),
('County.ArthurNE', '31005'),
('County.BannerNE', '31007'),
('County.BlaineNE', '31009'),
('County.BooneNE', '31011'),
('County.Box ButteNE', '31013'),
('County.BoydNE', '31015'),
('County.BrownNE', '31017'),
('County.BuffaloNE', '31019'),
('County.BurtNE', '31021'),
('County.ButlerNE', '31023'),
('County.CassNE', '31025'),
('County.CedarNE', '31027'),
('County.ChaseNE', '31029'),
('County.CherryNE', '31031'),
('County.CheyenneNE', '31033'),
('County.ClayNE', '31035'),
('County.ColfaxNE', '31037'),
('County.CumingNE', '31039'),
('County.CusterNE', '31041'),
('County.DakotaNE', '31043'),
('County.DawesNE', '31045'),
('County.DawsonNE', '31047'),
('County.DeuelNE', '31049'),
('County.DixonNE', '31051'),
('County.DodgeNE', '31053'),
('County.DouglasNE', '31055'),
('County.DundyNE', '31057'),
('County.FillmoreNE', '31059'),
('County.FranklinNE', '31061'),
('County.FrontierNE', '31063'),
('County.FurnasNE', '31065'),
('County.GageNE', '31067'),
('County.GardenNE', '31069'),
('County.GarfieldNE', '31071'),
('County.GosperNE', '31073'),
('County.GrantNE', '31075'),
('County.GreeleyNE', '31077'),
('County.HallNE', '31079'),
('County.HamiltonNE', '31081'),
('County.HarlanNE', '31083'),
('County.HayesNE', '31085'),
('County.HitchcockNE', '31087'),
('County.HoltNE', '31089'),
('County.HookerNE', '31091'),
('County.HowardNE', '31093'),
('County.JeffersonNE', '31095'),
('County.JohnsonNE', '31097'),
('County.KearneyNE', '31099'),
('County.KeithNE', '31101'),
('County.Keya PahaNE', '31103'),
('County.KimballNE', '31105'),
('County.KnoxNE', '31107'),
('County.LancasterNE', '31109'),
('County.LincolnNE', '31111'),
('County.LoganNE', '31113'),
('County.LoupNE', '31115'),
('County.MadisonNE', '31119'),
('County.McPhersonNE', '31117'),
('County.MerrickNE', '31121'),
('County.MorrillNE', '31123'),
('County.NanceNE', '31125'),
('County.NemahaNE', '31127'),
('County.NuckollsNE', '31129'),
('County.OtoeNE', '31131'),
('County.PawneeNE', '31133'),
('County.PerkinsNE', '31135'),
('County.PhelpsNE', '31137'),
('County.PierceNE', '31139'),
('County.PlatteNE', '31141'),
('County.PolkNE', '31143'),
('County.Red WillowNE', '31145'),
('County.RichardsonNE', '31147'),
('County.RockNE', '31149'),
('County.SalineNE', '31151'),
('County.SarpyNE', '31153'),
('County.SaundersNE', '31155'),
('County.Scotts BluffNE', '31157'),
('County.SewardNE', '31159'),
('County.SheridanNE', '31161'),
('County.ShermanNE', '31163'),
('County.SiouxNE', '31165'),
('County.StantonNE', '31167'),
('County.ThayerNE', '31169'),
('County.ThomasNE', '31171'),
('County.ThurstonNE', '31173'),
('County.ValleyNE', '31175'),
('County.WashingtonNE', '31177'),
('County.WayneNE', '31179'),
('County.WebsterNE', '31181'),
('County.WheelerNE', '31183'),
('County.YorkNE', '31185'),
('County.ChurchillNV', '32001'),
('County.City of Carson CityNV', '32510'),
('County.ClarkNV', '32003'),
('County.DouglasNV', '32005'),
('County.ElkoNV', '32007'),
('County.EsmeraldaNV', '32009'),
('County.EurekaNV', '32011'),
('County.HumboldtNV', '32013'),
('County.LanderNV', '32015'),
('County.LincolnNV', '32017'),
('County.LyonNV', '32019'),
('County.MineralNV', '32021'),
('County.NyeNV', '32023'),
('County.PershingNV', '32027'),
('County.StoreyNV', '32029'),
('County.WashoeNV', '32031'),
('County.White PineNV', '32033'),
('County.BelknapNH', '33001'),
('County.CarrollNH', '33003'),
('County.CheshireNH', '33005'),
('County.CoosNH', '33007'),
('County.GraftonNH', '33009'),
('County.HillsboroughNH', '33011'),
('County.MerrimackNH', '33013'),
('County.RockinghamNH', '33015'),
('County.StraffordNH', '33017'),
('County.SullivanNH', '33019'),
('County.AtlanticNJ', '34001'),
('County.BergenNJ', '34003'),
('County.BurlingtonNJ', '34005'),
('County.CamdenNJ', '34007'),
('County.Cape MayNJ', '34009'),
('County.CumberlandNJ', '34011'),
('County.EssexNJ', '34013'),
('County.GloucesterNJ', '34015'),
('County.HudsonNJ', '34017'),
('County.HunterdonNJ', '34019'),
('County.MercerNJ', '34021'),
('County.MiddlesexNJ', '34023'),
('County.MonmouthNJ', '34025'),
('County.MorrisNJ', '34027'),
('County.OceanNJ', '34029'),
('County.PassaicNJ', '34031'),
('County.SalemNJ', '34033'),
('County.SomersetNJ', '34035'),
('County.SussexNJ', '34037'),
('County.UnionNJ', '34039'),
('County.WarrenNJ', '34041'),
('County.BernalilloNM', '35001'),
('County.CatronNM', '35003'),
('County.ChavesNM', '35005'),
('County.CibolaNM', '35006'),
('County.ColfaxNM', '35007'),
('County.CurryNM', '35009'),
('County.De BacaNM', '35011'),
('County.Dona AnaNM', '35013'),
('County.EddyNM', '35015'),
('County.GrantNM', '35017'),
('County.GuadalupeNM', '35019'),
('County.HardingNM', '35021'),
('County.HidalgoNM', '35023'),
('County.LeaNM', '35025'),
('County.LincolnNM', '35027'),
('County.Los AlamosNM', '35028'),
('County.LunaNM', '35029'),
('County.McKinleyNM', '35031'),
('County.MoraNM', '35033'),
('County.OteroNM', '35035'),
('County.QuayNM', '35037'),
('County.Rio ArribaNM', '35039'),
('County.RooseveltNM', '35041'),
('County.San JuanNM', '35045'),
('County.San MiguelNM', '35047'),
('County.SandovalNM', '35043'),
('County.Santa FeNM', '35049'),
('County.SierraNM', '35051'),
('County.SocorroNM', '35053'),
('County.TaosNM', '35055'),
('County.TorranceNM', '35057'),
('County.UnionNM', '35059'),
('County.ValenciaNM', '35061'),
('County.AlbanyNY', '36001'),
('County.AlleganyNY', '36003'),
('County.BronxNY', '36005'),
('County.BroomeNY', '36007'),
('County.CattaraugusNY', '36009'),
('County.CayugaNY', '36011'),
('County.ChautauquaNY', '36013'),
('County.ChemungNY', '36015'),
('County.ChenangoNY', '36017'),
('County.ClintonNY', '36019'),
('County.ColumbiaNY', '36021'),
('County.CortlandNY', '36023'),
('County.DelawareNY', '36025'),
('County.DutchessNY', '36027'),
('County.ErieNY', '36029'),
('County.EssexNY', '36031'),
('County.FranklinNY', '36033'),
('County.FultonNY', '36035'),
('County.GeneseeNY', '36037'),
('County.GreeneNY', '36039'),
('County.HamiltonNY', '36041'),
('County.HerkimerNY', '36043'),
('County.JeffersonNY', '36045'),
('County.KingsNY', '36047'),
('County.LewisNY', '36049'),
('County.LivingstonNY', '36051'),
('County.MadisonNY', '36053'),
('County.MonroeNY', '36055'),
('County.MontgomeryNY', '36057'),
('County.NassauNY', '36059'),
('County.New YorkNY', '36061'),
('County.NiagaraNY', '36063'),
('County.OneidaNY', '36065'),
('County.OnondagaNY', '36067'),
('County.OntarioNY', '36069'),
('County.OrangeNY', '36071'),
('County.OrleansNY', '36073'),
('County.OswegoNY', '36075'),
('County.OtsegoNY', '36077'),
('County.PutnamNY', '36079'),
('County.QueensNY', '36081'),
('County.RensselaerNY', '36083'),
('County.RichmondNY', '36085'),
('County.RocklandNY', '36087'),
('County.SaratogaNY', '36091'),
('County.SchenectadyNY', '36093'),
('County.SchoharieNY', '36095'),
('County.SchuylerNY', '36097'),
('County.SenecaNY', '36099'),
('County.St. LawrenceNY', '36089'),
('County.SteubenNY', '36101'),
('County.SuffolkNY', '36103'),
('County.SullivanNY', '36105'),
('County.TiogaNY', '36107'),
('County.TompkinsNY', '36109'),
('County.UlsterNY', '36111'),
('County.WarrenNY', '36113'),
('County.WashingtonNY', '36115'),
('County.WayneNY', '36117'),
('County.WestchesterNY', '36119'),
('County.WyomingNY', '36121'),
('County.YatesNY', '36123'),
('County.AlamanceNC', '37001'),
('County.AlexanderNC', '37003'),
('County.AlleghanyNC', '37005'),
('County.AnsonNC', '37007'),
('County.AsheNC', '37009'),
('County.AveryNC', '37011'),
('County.BeaufortNC', '37013'),
('County.BertieNC', '37015'),
('County.BladenNC', '37017'),
('County.BrunswickNC', '37019'),
('County.BuncombeNC', '37021'),
('County.BurkeNC', '37023'),
('County.CabarrusNC', '37025'),
('County.CaldwellNC', '37027'),
('County.CamdenNC', '37029'),
('County.CarteretNC', '37031'),
('County.CaswellNC', '37033'),
('County.CatawbaNC', '37035'),
('County.ChathamNC', '37037'),
('County.CherokeeNC', '37039'),
('County.ChowanNC', '37041'),
('County.ClayNC', '37043'),
('County.ClevelandNC', '37045'),
('County.ColumbusNC', '37047'),
('County.CravenNC', '37049'),
('County.CumberlandNC', '37051'),
('County.CurrituckNC', '37053'),
('County.DareNC', '37055'),
('County.DavidsonNC', '37057'),
('County.DavieNC', '37059'),
('County.DuplinNC', '37061'),
('County.DurhamNC', '37063'),
('County.EdgecombeNC', '37065'),
('County.ForsythNC', '37067'),
('County.FranklinNC', '37069')

INSERT INTO @LookupMapper ([Key], ExternalId)
VALUES
('County.GastonNC', '37071'),
('County.GatesNC', '37073'),
('County.GrahamNC', '37075'),
('County.GranvilleNC', '37077'),
('County.GreeneNC', '37079'),
('County.GuilfordNC', '37081'),
('County.HalifaxNC', '37083'),
('County.HarnettNC', '37085'),
('County.HaywoodNC', '37087'),
('County.HendersonNC', '37089'),
('County.HertfordNC', '37091'),
('County.HokeNC', '37093'),
('County.HydeNC', '37095'),
('County.IredellNC', '37097'),
('County.JacksonNC', '37099'),
('County.JohnstonNC', '37101'),
('County.JonesNC', '37103'),
('County.LeeNC', '37105'),
('County.LenoirNC', '37107'),
('County.LincolnNC', '37109'),
('County.MaconNC', '37113'),
('County.MadisonNC', '37115'),
('County.MartinNC', '37117'),
('County.McDowellNC', '37111'),
('County.MecklenburgNC', '37119'),
('County.MitchellNC', '37121'),
('County.MontgomeryNC', '37123'),
('County.MooreNC', '37125'),
('County.NashNC', '37127'),
('County.New HanoverNC', '37129'),
('County.NorthamptonNC', '37131'),
('County.OnslowNC', '37133'),
('County.OrangeNC', '37135'),
('County.PamlicoNC', '37137'),
('County.PasquotankNC', '37139'),
('County.PenderNC', '37141'),
('County.PerquimansNC', '37143'),
('County.PersonNC', '37145'),
('County.PittNC', '37147'),
('County.PolkNC', '37149'),
('County.RandolphNC', '37151'),
('County.RichmondNC', '37153'),
('County.RobesonNC', '37155'),
('County.RockinghamNC', '37157'),
('County.RowanNC', '37159'),
('County.RutherfordNC', '37161'),
('County.SampsonNC', '37163'),
('County.ScotlandNC', '37165'),
('County.StanlyNC', '37167'),
('County.StokesNC', '37169'),
('County.SurryNC', '37171'),
('County.SwainNC', '37173'),
('County.TransylvaniaNC', '37175'),
('County.TyrrellNC', '37177'),
('County.UnionNC', '37179'),
('County.VanceNC', '37181'),
('County.WakeNC', '37183'),
('County.WarrenNC', '37185'),
('County.WashingtonNC', '37187'),
('County.WataugaNC', '37189'),
('County.WayneNC', '37191'),
('County.WilkesNC', '37193'),
('County.WilsonNC', '37195'),
('County.YadkinNC', '37197'),
('County.YanceyNC', '37199'),
('County.AdamsND', '38001'),
('County.BarnesND', '38003'),
('County.BensonND', '38005'),
('County.BillingsND', '38007'),
('County.BottineauND', '38009'),
('County.BowmanND', '38011'),
('County.BurkeND', '38013'),
('County.BurleighND', '38015'),
('County.CassND', '38017'),
('County.CavalierND', '38019'),
('County.DickeyND', '38021'),
('County.DivideND', '38023'),
('County.DunnND', '38025'),
('County.EddyND', '38027'),
('County.EmmonsND', '38029'),
('County.FosterND', '38031'),
('County.Golden ValleyND', '38033'),
('County.Grand ForksND', '38035'),
('County.GrantND', '38037'),
('County.GriggsND', '38039'),
('County.HettingerND', '38041'),
('County.KidderND', '38043'),
('County.La MoureND', '38045'),
('County.LoganND', '38047'),
('County.McHenryND', '38049'),
('County.McIntoshND', '38051'),
('County.McKenzieND', '38053'),
('County.McLeanND', '38055'),
('County.MercerND', '38057'),
('County.MortonND', '38059'),
('County.MountrailND', '38061'),
('County.NelsonND', '38063'),
('County.OliverND', '38065'),
('County.PembinaND', '38067'),
('County.PierceND', '38069'),
('County.RamseyND', '38071'),
('County.RansomND', '38073'),
('County.RenvilleND', '38075'),
('County.RichlandND', '38077'),
('County.RoletteND', '38079'),
('County.SargentND', '38081'),
('County.SheridanND', '38083'),
('County.SiouxND', '38085'),
('County.SlopeND', '38087'),
('County.StarkND', '38089'),
('County.SteeleND', '38091'),
('County.StutsmanND', '38093'),
('County.TownerND', '38095'),
('County.TraillND', '38097'),
('County.WalshND', '38099'),
('County.WardND', '38101'),
('County.WellsND', '38103'),
('County.WilliamsND', '38105'),
('County.Northern Mariana IslandsMP', '69010'),
('County.AdamsOH', '39001'),
('County.AllenOH', '39003'),
('County.AshlandOH', '39005'),
('County.AshtabulaOH', '39007'),
('County.AthensOH', '39009'),
('County.AuglaizeOH', '39011'),
('County.BelmontOH', '39013'),
('County.BrownOH', '39015'),
('County.ButlerOH', '39017'),
('County.CarrollOH', '39019'),
('County.ChampaignOH', '39021'),
('County.ClarkOH', '39023'),
('County.ClermontOH', '39025'),
('County.ClintonOH', '39027'),
('County.ColumbianaOH', '39029'),
('County.CoshoctonOH', '39031'),
('County.CrawfordOH', '39033'),
('County.CuyahogaOH', '39035'),
('County.DarkeOH', '39037'),
('County.DefianceOH', '39039'),
('County.DelawareOH', '39041'),
('County.ErieOH', '39043'),
('County.FairfieldOH', '39045'),
('County.FayetteOH', '39047'),
('County.FranklinOH', '39049'),
('County.FultonOH', '39051'),
('County.GalliaOH', '39053'),
('County.GeaugaOH', '39055'),
('County.GreeneOH', '39057'),
('County.GuernseyOH', '39059'),
('County.HamiltonOH', '39061'),
('County.HancockOH', '39063'),
('County.HardinOH', '39065'),
('County.HarrisonOH', '39067'),
('County.HenryOH', '39069'),
('County.HighlandOH', '39071'),
('County.HockingOH', '39073'),
('County.HolmesOH', '39075'),
('County.HuronOH', '39077'),
('County.JacksonOH', '39079'),
('County.JeffersonOH', '39081'),
('County.KnoxOH', '39083'),
('County.LakeOH', '39085'),
('County.LawrenceOH', '39087'),
('County.LickingOH', '39089'),
('County.LoganOH', '39091'),
('County.LorainOH', '39093'),
('County.LucasOH', '39095'),
('County.MadisonOH', '39097'),
('County.MahoningOH', '39099'),
('County.MarionOH', '39101'),
('County.MedinaOH', '39103'),
('County.MeigsOH', '39105'),
('County.MercerOH', '39107'),
('County.MiamiOH', '39109'),
('County.MonroeOH', '39111'),
('County.MontgomeryOH', '39113'),
('County.MorganOH', '39115'),
('County.MorrowOH', '39117'),
('County.MuskingumOH', '39119'),
('County.NobleOH', '39121'),
('County.OttawaOH', '39123'),
('County.PauldingOH', '39125'),
('County.PerryOH', '39127'),
('County.PickawayOH', '39129'),
('County.PikeOH', '39131'),
('County.PortageOH', '39133'),
('County.PrebleOH', '39135'),
('County.PutnamOH', '39137'),
('County.RichlandOH', '39139'),
('County.RossOH', '39141'),
('County.SanduskyOH', '39143'),
('County.SciotoOH', '39145'),
('County.SenecaOH', '39147'),
('County.ShelbyOH', '39149'),
('County.StarkOH', '39151'),
('County.SummitOH', '39153'),
('County.TrumbullOH', '39155'),
('County.TuscarawasOH', '39157'),
('County.UnionOH', '39159'),
('County.Van WertOH', '39161'),
('County.VintonOH', '39163'),
('County.WarrenOH', '39165'),
('County.WashingtonOH', '39167'),
('County.WayneOH', '39169'),
('County.WilliamsOH', '39171'),
('County.WoodOH', '39173'),
('County.WyandotOH', '39175'),
('County.AdairOK', '40001'),
('County.AlfalfaOK', '40003'),
('County.AtokaOK', '40005'),
('County.BeaverOK', '40007'),
('County.BeckhamOK', '40009'),
('County.BlaineOK', '40011'),
('County.BryanOK', '40013'),
('County.CaddoOK', '40015'),
('County.CanadianOK', '40017'),
('County.CarterOK', '40019'),
('County.CherokeeOK', '40021'),
('County.ChoctawOK', '40023'),
('County.CimarronOK', '40025'),
('County.ClevelandOK', '40027'),
('County.CoalOK', '40029'),
('County.ComancheOK', '40031'),
('County.CottonOK', '40033'),
('County.CraigOK', '40035'),
('County.CreekOK', '40037'),
('County.CusterOK', '40039'),
('County.DelawareOK', '40041'),
('County.DeweyOK', '40043'),
('County.EllisOK', '40045'),
('County.GarfieldOK', '40047'),
('County.GarvinOK', '40049'),
('County.GradyOK', '40051'),
('County.GrantOK', '40053'),
('County.GreerOK', '40055'),
('County.HarmonOK', '40057'),
('County.HarperOK', '40059'),
('County.HaskellOK', '40061'),
('County.HughesOK', '40063'),
('County.JacksonOK', '40065'),
('County.JeffersonOK', '40067'),
('County.JohnstonOK', '40069'),
('County.KayOK', '40071'),
('County.KingfisherOK', '40073'),
('County.KiowaOK', '40075'),
('County.LatimerOK', '40077'),
('County.Le FloreOK', '40079'),
('County.LincolnOK', '40081'),
('County.LoganOK', '40083'),
('County.LoveOK', '40085'),
('County.MajorOK', '40093'),
('County.MarshallOK', '40095'),
('County.MayesOK', '40097'),
('County.McClainOK', '40087'),
('County.McCurtainOK', '40089'),
('County.McIntoshOK', '40091'),
('County.MurrayOK', '40099'),
('County.MuskogeeOK', '40101'),
('County.NobleOK', '40103'),
('County.NowataOK', '40105'),
('County.OkfuskeeOK', '40107'),
('County.OklahomaOK', '40109'),
('County.OkmulgeeOK', '40111'),
('County.OsageOK', '40113'),
('County.OttawaOK', '40115'),
('County.PawneeOK', '40117'),
('County.PayneOK', '40119'),
('County.PittsburgOK', '40121'),
('County.PontotocOK', '40123'),
('County.PottawatomieOK', '40125'),
('County.PushmatahaOK', '40127'),
('County.Roger MillsOK', '40129'),
('County.RogersOK', '40131'),
('County.SeminoleOK', '40133'),
('County.SequoyahOK', '40135'),
('County.StephensOK', '40137'),
('County.TexasOK', '40139'),
('County.TillmanOK', '40141'),
('County.TulsaOK', '40143'),
('County.WagonerOK', '40145'),
('County.WashingtonOK', '40147'),
('County.WashitaOK', '40149'),
('County.WoodsOK', '40151'),
('County.WoodwardOK', '40153'),
('County.BakerOR', '41001'),
('County.BentonOR', '41003'),
('County.ClackamasOR', '41005'),
('County.ClatsopOR', '41007'),
('County.ColumbiaOR', '41009'),
('County.CoosOR', '41011'),
('County.CrookOR', '41013'),
('County.CurryOR', '41015'),
('County.DeschutesOR', '41017'),
('County.DouglasOR', '41019'),
('County.GilliamOR', '41021'),
('County.GrantOR', '41023'),
('County.HarneyOR', '41025'),
('County.Hood RiverOR', '41027'),
('County.JacksonOR', '41029'),
('County.JeffersonOR', '41031'),
('County.JosephineOR', '41033'),
('County.KlamathOR', '41035'),
('County.LakeOR', '41037'),
('County.LaneOR', '41039'),
('County.LincolnOR', '41041'),
('County.LinnOR', '41043'),
('County.MalheurOR', '41045'),
('County.MarionOR', '41047'),
('County.MorrowOR', '41049'),
('County.MultnomahOR', '41051'),
('County.PolkOR', '41053'),
('County.ShermanOR', '41055'),
('County.TillamookOR', '41057'),
('County.UmatillaOR', '41059'),
('County.UnionOR', '41061'),
('County.WallowaOR', '41063'),
('County.WascoOR', '41065'),
('County.WashingtonOR', '41067'),
('County.WheelerOR', '41069'),
('County.YamhillOR', '41071'),
('County.PalauPW', '70030'),
('County.AdamsPA', '42001'),
('County.AlleghenyPA', '42003'),
('County.ArmstrongPA', '42005'),
('County.BeaverPA', '42007'),
('County.BedfordPA', '42009'),
('County.BerksPA', '42011'),
('County.BlairPA', '42013'),
('County.BradfordPA', '42015'),
('County.BucksPA', '42017'),
('County.ButlerPA', '42019'),
('County.CambriaPA', '42021'),
('County.CameronPA', '42023'),
('County.CarbonPA', '42025'),
('County.CentrePA', '42027'),
('County.ChesterPA', '42029'),
('County.ClarionPA', '42031'),
('County.ClearfieldPA', '42033'),
('County.ClintonPA', '42035'),
('County.ColumbiaPA', '42037'),
('County.CrawfordPA', '42039'),
('County.CumberlandPA', '42041'),
('County.DauphinPA', '42043'),
('County.DelawarePA', '42045'),
('County.ElkPA', '42047'),
('County.EriePA', '42049'),
('County.FayettePA', '42051'),
('County.ForestPA', '42053'),
('County.FranklinPA', '42055'),
('County.FultonPA', '42057'),
('County.GreenePA', '42059'),
('County.HuntingdonPA', '42061'),
('County.IndianaPA', '42063'),
('County.JeffersonPA', '42065'),
('County.JuniataPA', '42067'),
('County.LackawannaPA', '42069'),
('County.LancasterPA', '42071'),
('County.LawrencePA', '42073'),
('County.LebanonPA', '42075'),
('County.LehighPA', '42077'),
('County.LuzernePA', '42079'),
('County.LycomingPA', '42081'),
('County.McKeanPA', '42083'),
('County.MercerPA', '42085'),
('County.MifflinPA', '42087'),
('County.MonroePA', '42089'),
('County.MontgomeryPA', '42091'),
('County.MontourPA', '42093'),
('County.NorthamptonPA', '42095'),
('County.NorthumberlandPA', '42097'),
('County.PerryPA', '42099'),
('County.PhiladelphiaPA', '42101'),
('County.PikePA', '42103'),
('County.PotterPA', '42105'),
('County.SchuylkillPA', '42107'),
('County.SnyderPA', '42109'),
('County.SomersetPA', '42111'),
('County.SullivanPA', '42113'),
('County.SusquehannaPA', '42115'),
('County.TiogaPA', '42117'),
('County.UnionPA', '42119'),
('County.VenangoPA', '42121'),
('County.WarrenPA', '42123'),
('County.WashingtonPA', '42125'),
('County.WaynePA', '42127'),
('County.WestmorelandPA', '42129'),
('County.WyomingPA', '42131'),
('County.YorkPA', '42133'),
('County.AdjuntasPR', '72001'),
('County.AguadaPR', '72003'),
('County.AguadillaPR', '72005'),
('County.Aguas BuenasPR', '72007'),
('County.AibonitoPR', '72009'),
('County.AnascoPR', '72011'),
('County.AreciboPR', '72013'),
('County.ArroyoPR', '72015'),
('County.BarcelonetaPR', '72017'),
('County.BarranquitasPR', '72019'),
('County.BayamonPR', '72021'),
('County.Cabo RojoPR', '72023'),
('County.CaguasPR', '72025'),
('County.CamuyPR', '72027'),
('County.CanovanasPR', '72029'),
('County.CarolinaPR', '72031'),
('County.CatanoPR', '72033'),
('County.CayeyPR', '72035'),
('County.CeibaPR', '72037'),
('County.CialesPR', '72039'),
('County.CidraPR', '72041'),
('County.CoamoPR', '72043'),
('County.ComerioPR', '72045'),
('County.CorozalPR', '72047'),
('County.CulebraPR', '72049'),
('County.DoradoPR', '72051'),
('County.FajardoPR', '72053'),
('County.FloridaPR', '72054'),
('County.GuanicaPR', '72055'),
('County.GuayamaPR', '72057'),
('County.GuayanillaPR', '72059'),
('County.GuaynaboPR', '72061'),
('County.GuraboPR', '72063'),
('County.HatilloPR', '72065'),
('County.HormiguerosPR', '72067'),
('County.HumacaoPR', '72069'),
('County.IsabelaPR', '72071'),
('County.JayuyaPR', '72073'),
('County.Juana DiazPR', '72075'),
('County.JuncosPR', '72077'),
('County.LajasPR', '72079'),
('County.LaresPR', '72081'),
('County.Las MariasPR', '72083'),
('County.Las PiedrasPR', '72085'),
('County.LoizaPR', '72087'),
('County.LuquilloPR', '72089'),
('County.ManatiPR', '72091'),
('County.MaricaoPR', '72093'),
('County.MaunaboPR', '72095'),
('County.MayaguezPR', '72097'),
('County.MocaPR', '72099'),
('County.MorovisPR', '72101'),
('County.NaguaboPR', '72103'),
('County.NaranjitoPR', '72105'),
('County.OrocovisPR', '72107'),
('County.PatillasPR', '72109'),
('County.PenuelasPR', '72111'),
('County.PoncePR', '72113'),
('County.QuebradillasPR', '72115'),
('County.RinconPR', '72117'),
('County.Rio GrandePR', '72119'),
('County.Sabana GrandePR', '72121'),
('County.SalinasPR', '72123'),
('County.San GermanPR', '72125'),
('County.San JuanPR', '72127'),
('County.San LorenzoPR', '72129'),
('County.San SebastianPR', '72131'),
('County.Santa IsabelPR', '72133'),
('County.Toa AltaPR', '72135'),
('County.Toa BajaPR', '72137'),
('County.Trujillo AltoPR', '72139'),
('County.UtuadoPR', '72141'),
('County.Vega AltaPR', '72143'),
('County.Vega BajaPR', '72145'),
('County.ViequesPR', '72147'),
('County.VillalbaPR', '72149'),
('County.YabucoaPR', '72151'),
('County.YaucoPR', '72153'),
('County.BristolRI', '44001'),
('County.KentRI', '44003'),
('County.NewportRI', '44005'),
('County.ProvidenceRI', '44007'),
('County.WashingtonRI', '44009'),
('County.AbbevilleSC', '45001'),
('County.AikenSC', '45003'),
('County.AllendaleSC', '45005'),
('County.AndersonSC', '45007'),
('County.BambergSC', '45009'),
('County.BarnwellSC', '45011'),
('County.BeaufortSC', '45013'),
('County.BerkeleySC', '45015'),
('County.CalhounSC', '45017'),
('County.CharlestonSC', '45019'),
('County.CherokeeSC', '45021'),
('County.ChesterSC', '45023'),
('County.ChesterfieldSC', '45025'),
('County.ClarendonSC', '45027'),
('County.ColletonSC', '45029'),
('County.DarlingtonSC', '45031'),
('County.DillonSC', '45033'),
('County.DorchesterSC', '45035'),
('County.EdgefieldSC', '45037'),
('County.FairfieldSC', '45039'),
('County.FlorenceSC', '45041'),
('County.GeorgetownSC', '45043'),
('County.GreenvilleSC', '45045'),
('County.GreenwoodSC', '45047'),
('County.HamptonSC', '45049'),
('County.HorrySC', '45051'),
('County.JasperSC', '45053'),
('County.KershawSC', '45055'),
('County.LancasterSC', '45057'),
('County.LaurensSC', '45059'),
('County.LeeSC', '45061'),
('County.LexingtonSC', '45063'),
('County.MarionSC', '45067'),
('County.MarlboroSC', '45069'),
('County.McCormickSC', '45065'),
('County.NewberrySC', '45071'),
('County.OconeeSC', '45073'),
('County.OrangeburgSC', '45075'),
('County.PickensSC', '45077'),
('County.RichlandSC', '45079'),
('County.SaludaSC', '45081'),
('County.SpartanburgSC', '45083'),
('County.SumterSC', '45085'),
('County.UnionSC', '45087'),
('County.WilliamsburgSC', '45089'),
('County.YorkSC', '45091'),
('County.AuroraSD', '46003'),
('County.BeadleSD', '46005'),
('County.BennettSD', '46007'),
('County.Bon HommeSD', '46009'),
('County.BrookingsSD', '46011'),
('County.BrownSD', '46013'),
('County.BruleSD', '46015'),
('County.BuffaloSD', '46017'),
('County.ButteSD', '46019'),
('County.CampbellSD', '46021'),
('County.Charles MixSD', '46023'),
('County.ClarkSD', '46025'),
('County.ClaySD', '46027'),
('County.CodingtonSD', '46029'),
('County.CorsonSD', '46031'),
('County.CusterSD', '46033'),
('County.DavisonSD', '46035'),
('County.DaySD', '46037'),
('County.DeuelSD', '46039'),
('County.DeweySD', '46041'),
('County.DouglasSD', '46043'),
('County.EdmundsSD', '46045'),
('County.Fall RiverSD', '46047'),
('County.FaulkSD', '46049'),
('County.GrantSD', '46051'),
('County.GregorySD', '46053'),
('County.HaakonSD', '46055'),
('County.HamlinSD', '46057'),
('County.HandSD', '46059'),
('County.HansonSD', '46061'),
('County.HardingSD', '46063'),
('County.HughesSD', '46065'),
('County.HutchinsonSD', '46067'),
('County.HydeSD', '46069'),
('County.JacksonSD', '46071'),
('County.JerauldSD', '46073'),
('County.JonesSD', '46075'),
('County.KingsburySD', '46077'),
('County.LakeSD', '46079'),
('County.LawrenceSD', '46081'),
('County.LincolnSD', '46083'),
('County.LymanSD', '46085'),
('County.MarshallSD', '46091'),
('County.McCookSD', '46087'),
('County.McPhersonSD', '46089'),
('County.MeadeSD', '46093'),
('County.MelletteSD', '46095'),
('County.MinerSD', '46097'),
('County.MinnehahaSD', '46099'),
('County.MoodySD', '46101'),
('County.PenningtonSD', '46103'),
('County.PerkinsSD', '46105'),
('County.PotterSD', '46107'),
('County.RobertsSD', '46109'),
('County.SanbornSD', '46111'),
('County.ShannonSD', '46113'),
('County.SpinkSD', '46115'),
('County.StanleySD', '46117'),
('County.SullySD', '46119'),
('County.ToddSD', '46121'),
('County.TrippSD', '46123'),
('County.TurnerSD', '46125'),
('County.UnionSD', '46127'),
('County.WalworthSD', '46129'),
('County.YanktonSD', '46135'),
('County.ZiebachSD', '46137'),
('County.BedfordTN', '47003'),
('County.BledsoeTN', '47007'),
('County.BlountTN', '47009'),
('County.BradleyTN', '47011'),
('County.CarterTN', '47019'),
('County.ChesterTN', '47023'),
('County.CoffeeTN', '47031'),
('County.DecaturTN', '47039'),
('County.FayetteTN', '47047'),
('County.FranklinTN', '47051'),
('County.GilesTN', '47055'),
('County.GrundyTN', '47061'),
('County.HamiltonTN', '47065'),
('County.HardemanTN', '47069'),
('County.HardinTN', '47071'),
('County.HaywoodTN', '47075'),
('County.HendersonTN', '47077'),
('County.HickmanTN', '47081'),
('County.JohnsonTN', '47091'),
('County.LawrenceTN', '47099'),
('County.LewisTN', '47101'),
('County.LincolnTN', '47103'),
('County.LoudonTN', '47105'),
('County.MadisonTN', '47113'),
('County.MarionTN', '47115'),
('County.MarshallTN', '47117'),
('County.MauryTN', '47119'),
('County.McMinnTN', '47107'),
('County.McNairyTN', '47109'),
('County.MeigsTN', '47121'),
('County.MonroeTN', '47123'),
('County.MooreTN', '47127'),
('County.PerryTN', '47135'),
('County.PolkTN', '47139'),
('County.RheaTN', '47143'),
('County.SequatchieTN', '47153'),
('County.ShelbyTN', '47157'),
('County.TiptonTN', '47167'),
('County.UnicoiTN', '47171'),
('County.Van BurenTN', '47175'),
('County.WarrenTN', '47177'),
('County.WayneTN', '47181'),
('County.WhiteTN', '47185'),
('County.AndersonTX', '48001'),
('County.AndrewsTX', '48003'),
('County.AngelinaTX', '48005'),
('County.AransasTX', '48007'),
('County.ArcherTX', '48009'),
('County.ArmstrongTX', '48011'),
('County.AtascosaTX', '48013'),
('County.AustinTX', '48015'),
('County.BaileyTX', '48017'),
('County.BanderaTX', '48019'),
('County.BastropTX', '48021'),
('County.BaylorTX', '48023'),
('County.BeeTX', '48025'),
('County.BellTX', '48027'),
('County.BexarTX', '48029'),
('County.BlancoTX', '48031'),
('County.BordenTX', '48033'),
('County.BosqueTX', '48035'),
('County.BowieTX', '48037'),
('County.BrazoriaTX', '48039'),
('County.BrazosTX', '48041'),
('County.BrewsterTX', '48043'),
('County.BriscoeTX', '48045'),
('County.BrooksTX', '48047'),
('County.BrownTX', '48049'),
('County.BurlesonTX', '48051'),
('County.BurnetTX', '48053'),
('County.CaldwellTX', '48055'),
('County.CalhounTX', '48057'),
('County.CallahanTX', '48059'),
('County.CameronTX', '48061'),
('County.CampTX', '48063'),
('County.CarsonTX', '48065'),
('County.CassTX', '48067'),
('County.CastroTX', '48069'),
('County.ChambersTX', '48071'),
('County.CherokeeTX', '48073'),
('County.ChildressTX', '48075'),
('County.ClayTX', '48077'),
('County.CochranTX', '48079'),
('County.CokeTX', '48081'),
('County.ColemanTX', '48083'),
('County.CollinTX', '48085'),
('County.CollingsworthTX', '48087'),
('County.ColoradoTX', '48089'),
('County.ComalTX', '48091'),
('County.ComancheTX', '48093'),
('County.ConchoTX', '48095'),
('County.CookeTX', '48097'),
('County.CoryellTX', '48099'),
('County.CottleTX', '48101'),
('County.CraneTX', '48103'),
('County.CrockettTX', '48105'),
('County.CrosbyTX', '48107'),
('County.CulbersonTX', '48109'),
('County.DallamTX', '48111'),
('County.DallasTX', '48113'),
('County.DawsonTX', '48115'),
('County.De WittTX', '48123'),
('County.Deaf SmithTX', '48117'),
('County.DeltaTX', '48119'),
('County.DentonTX', '48121'),
('County.DickensTX', '48125'),
('County.DimmitTX', '48127'),
('County.DonleyTX', '48129'),
('County.DuvalTX', '48131'),
('County.EastlandTX', '48133'),
('County.EctorTX', '48135'),
('County.EdwardsTX', '48137'),
('County.El PasoTX', '48141'),
('County.EllisTX', '48139'),
('County.ErathTX', '48143'),
('County.FallsTX', '48145'),
('County.FanninTX', '48147'),
('County.FayetteTX', '48149'),
('County.FisherTX', '48151'),
('County.FloydTX', '48153'),
('County.FoardTX', '48155'),
('County.Fort BendTX', '48157'),
('County.FranklinTX', '48159'),
('County.FreestoneTX', '48161'),
('County.FrioTX', '48163'),
('County.GainesTX', '48165'),
('County.GalvestonTX', '48167'),
('County.GarzaTX', '48169'),
('County.GillespieTX', '48171'),
('County.GlasscockTX', '48173'),
('County.GoliadTX', '48175'),
('County.GonzalesTX', '48177'),
('County.GrayTX', '48179'),
('County.GraysonTX', '48181'),
('County.GreggTX', '48183'),
('County.GrimesTX', '48185'),
('County.GuadalupeTX', '48187'),
('County.HaleTX', '48189'),
('County.HallTX', '48191'),
('County.HamiltonTX', '48193'),
('County.HansfordTX', '48195'),
('County.HardemanTX', '48197'),
('County.HardinTX', '48199'),
('County.HarrisTX', '48201'),
('County.HarrisonTX', '48203'),
('County.HartleyTX', '48205'),
('County.HaskellTX', '48207'),
('County.HaysTX', '48209'),
('County.HemphillTX', '48211'),
('County.HendersonTX', '48213'),
('County.HidalgoTX', '48215'),
('County.HillTX', '48217'),
('County.HockleyTX', '48219'),
('County.HoodTX', '48221'),
('County.HopkinsTX', '48223'),
('County.HoustonTX', '48225'),
('County.HowardTX', '48227'),
('County.HudspethTX', '48229'),
('County.HuntTX', '48231'),
('County.HutchinsonTX', '48233'),
('County.IrionTX', '48235'),
('County.JackTX', '48237'),
('County.JacksonTX', '48239'),
('County.JasperTX', '48241'),
('County.Jeff DavisTX', '48243'),
('County.JeffersonTX', '48245'),
('County.Jim HoggTX', '48247'),
('County.Jim WellsTX', '48249'),
('County.JohnsonTX', '48251'),
('County.JonesTX', '48253'),
('County.KarnesTX', '48255'),
('County.KaufmanTX', '48257'),
('County.KendallTX', '48259'),
('County.KenedyTX', '48261'),
('County.KentTX', '48263'),
('County.KerrTX', '48265'),
('County.KimbleTX', '48267'),
('County.KingTX', '48269'),
('County.KinneyTX', '48271'),
('County.KlebergTX', '48273'),
('County.KnoxTX', '48275'),
('County.La SalleTX', '48283'),
('County.LamarTX', '48277'),
('County.LambTX', '48279'),
('County.LampasasTX', '48281'),
('County.LavacaTX', '48285'),
('County.LeeTX', '48287'),
('County.LeonTX', '48289'),
('County.LibertyTX', '48291'),
('County.LimestoneTX', '48293'),
('County.LipscombTX', '48295'),
('County.Live OakTX', '48297'),
('County.LlanoTX', '48299'),
('County.LovingTX', '48301'),
('County.LubbockTX', '48303'),
('County.LynnTX', '48305'),
('County.MadisonTX', '48313'),
('County.MarionTX', '48315'),
('County.MartinTX', '48317'),
('County.MasonTX', '48319'),
('County.MatagordaTX', '48321'),
('County.MaverickTX', '48323'),
('County.McCullochTX', '48307'),
('County.McLennanTX', '48309'),
('County.McMullenTX', '48311'),
('County.MedinaTX', '48325'),
('County.MenardTX', '48327'),
('County.MidlandTX', '48329'),
('County.MilamTX', '48331'),
('County.MillsTX', '48333'),
('County.MitchellTX', '48335'),
('County.MontagueTX', '48337'),
('County.MontgomeryTX', '48339'),
('County.MooreTX', '48341'),
('County.MorrisTX', '48343'),
('County.MotleyTX', '48345'),
('County.NacogdochesTX', '48347'),
('County.NavarroTX', '48349'),
('County.NewtonTX', '48351'),
('County.NolanTX', '48353'),
('County.NuecesTX', '48355'),
('County.OchiltreeTX', '48357'),
('County.OldhamTX', '48359'),
('County.OrangeTX', '48361'),
('County.Palo PintoTX', '48363'),
('County.PanolaTX', '48365'),
('County.ParkerTX', '48367'),
('County.ParmerTX', '48369'),
('County.PecosTX', '48371'),
('County.PolkTX', '48373'),
('County.PotterTX', '48375'),
('County.PresidioTX', '48377'),
('County.RainsTX', '48379'),
('County.RandallTX', '48381'),
('County.ReaganTX', '48383'),
('County.RealTX', '48385'),
('County.Red RiverTX', '48387'),
('County.ReevesTX', '48389'),
('County.RefugioTX', '48391'),
('County.RobertsTX', '48393'),
('County.RobertsonTX', '48395'),
('County.RockwallTX', '48397'),
('County.RunnelsTX', '48399'),
('County.RuskTX', '48401'),
('County.SabineTX', '48403'),
('County.San AugustineTX', '48405'),
('County.San JacintoTX', '48407'),
('County.San PatricioTX', '48409'),
('County.San SabaTX', '48411'),
('County.SchleicherTX', '48413'),
('County.ScurryTX', '48415'),
('County.ShackelfordTX', '48417'),
('County.ShelbyTX', '48419'),
('County.ShermanTX', '48421'),
('County.SmithTX', '48423'),
('County.SomervellTX', '48425'),
('County.StarrTX', '48427'),
('County.StephensTX', '48429'),
('County.SterlingTX', '48431'),
('County.StonewallTX', '48433'),
('County.SuttonTX', '48435'),
('County.SwisherTX', '48437'),
('County.TarrantTX', '48439'),
('County.TaylorTX', '48441'),
('County.TerrellTX', '48443'),
('County.TerryTX', '48445'),
('County.ThrockmortonTX', '48447'),
('County.TitusTX', '48449'),
('County.Tom GreenTX', '48451'),
('County.TravisTX', '48453'),
('County.TrinityTX', '48455'),
('County.TylerTX', '48457'),
('County.UpshurTX', '48459'),
('County.UptonTX', '48461'),
('County.UvaldeTX', '48463'),
('County.Val VerdeTX', '48465'),
('County.Van ZandtTX', '48467'),
('County.VictoriaTX', '48469'),
('County.WalkerTX', '48471'),
('County.WallerTX', '48473'),
('County.WardTX', '48475'),
('County.WashingtonTX', '48477'),
('County.WebbTX', '48479'),
('County.WhartonTX', '48481'),
('County.WheelerTX', '48483'),
('County.WichitaTX', '48485'),
('County.WilbargerTX', '48487'),
('County.WillacyTX', '48489'),
('County.WilliamsonTX', '48491'),
('County.WilsonTX', '48493'),
('County.WinklerTX', '48495'),
('County.WiseTX', '48497'),
('County.WoodTX', '48499'),
('County.YoakumTX', '48501'),
('County.YoungTX', '48503'),
('County.ZapataTX', '48505'),
('County.ZavalaTX', '48507'),
('County.St. CroixVI', '78010'),
('County.St. JohnVI', '78020'),
('County.St. ThomasVI', '78030'),
('County.BeaverUT', '49001'),
('County.Box ElderUT', '49003'),
('County.CacheUT', '49005'),
('County.CarbonUT', '49007'),
('County.DaggettUT', '49009'),
('County.DavisUT', '49011'),
('County.DuchesneUT', '49013'),
('County.EmeryUT', '49015'),
('County.GarfieldUT', '49017'),
('County.GrandUT', '49019'),
('County.IronUT', '49021'),
('County.JuabUT', '49023'),
('County.KaneUT', '49025'),
('County.MillardUT', '49027'),
('County.MorganUT', '49029'),
('County.PiuteUT', '49031'),
('County.RichUT', '49033'),
('County.Salt LakeUT', '49035'),
('County.San JuanUT', '49037'),
('County.SanpeteUT', '49039'),
('County.SevierUT', '49041'),
('County.SummitUT', '49043'),
('County.TooeleUT', '49045'),
('County.UintahUT', '49047'),
('County.UtahUT', '49049'),
('County.WasatchUT', '49051'),
('County.WashingtonUT', '49053'),
('County.WayneUT', '49055'),
('County.WeberUT', '49057'),
('County.AddisonVT', '50001'),
('County.BenningtonVT', '50003'),
('County.CaledoniaVT', '50005'),
('County.ChittendenVT', '50007'),
('County.EssexVT', '50009'),
('County.FranklinVT', '50011'),
('County.Grand IsleVT', '50013'),
('County.LamoilleVT', '50015'),
('County.OrangeVT', '50017'),
('County.OrleansVT', '50019'),
('County.RutlandVT', '50021'),
('County.WashingtonVT', '50023'),
('County.WindhamVT', '50025'),
('County.WindsorVT', '50027'),
('County.AccomackVA', '51001'),
('County.AlbemarleVA', '51003'),
('County.AlleghanyVA', '51005'),
('County.AmeliaVA', '51007'),
('County.AmherstVA', '51009'),
('County.AppomattoxVA', '51011'),
('County.ArlingtonVA', '51013'),
('County.AugustaVA', '51015'),
('County.BathVA', '51017'),
('County.BedfordVA', '51019'),
('County.BlandVA', '51021'),
('County.BotetourtVA', '51023'),
('County.BrunswickVA', '51025'),
('County.BuckinghamVA', '51029'),
('County.CampbellVA', '51031'),
('County.CarolineVA', '51033'),
('County.CarrollVA', '51035'),
('County.Charles CityVA', '51036'),
('County.CharlotteVA', '51037'),
('County.ChesterfieldVA', '51041'),
('County.City of AlexandriaVA', '51510'),
('County.City of BristolVA', '51520'),
('County.City of Buena VistaVA', '51530'),
('County.City of CharlottesvilleVA', '51540'),
('County.City of ChesapeakeVA', '51550'),
('County.City of Colonial HeightsVA', '51570'),
('County.City of CovingtonVA', '51580'),
('County.City of DanvilleVA', '51590'),
('County.City of FairfaxVA', '51600'),
('County.City of Falls ChurchVA', '51610'),
('County.City of FranklinVA', '51620'),
('County.City of FredericksburgVA', '51630'),
('County.City of GalaxVA', '51640'),
('County.City of HamptonVA', '51650'),
('County.City of HarrisonburgVA', '51660'),
('County.City of HopewellVA', '51670'),
('County.City of LexingtonVA', '51678'),
('County.City of LynchburgVA', '51680'),
('County.City of ManassasVA', '51683'),
('County.City of Manassas ParkVA', '51685'),
('County.City of MartinsvilleVA', '51690'),
('County.City of Newport NewsVA', '51700'),
('County.City of NorfolkVA', '51710'),
('County.City of NortonVA', '51720'),
('County.City of PetersburgVA', '51730'),
('County.City of PoquosonVA', '51735'),
('County.City of PortsmouthVA', '51740'),
('County.City of RadfordVA', '51750'),
('County.City of RichmondVA', '51760'),
('County.City of RoanokeVA', '51770'),
('County.City of SalemVA', '51775'),
('County.City of StauntonVA', '51790'),
('County.City of SuffolkVA', '51800'),
('County.City of Virginia BeachVA', '51810'),
('County.City of WaynesboroVA', '51820'),
('County.City of WilliamsburgVA', '51830'),
('County.City of WinchesterVA', '51840'),
('County.ClarkeVA', '51043'),
('County.CraigVA', '51045'),
('County.CulpeperVA', '51047'),
('County.CumberlandVA', '51049'),
('County.DinwiddieVA', '51053'),
('County.EssexVA', '51057'),
('County.FairfaxVA', '51059'),
('County.FauquierVA', '51061'),
('County.FloydVA', '51063'),
('County.FluvannaVA', '51065'),
('County.FranklinVA', '51067'),
('County.FrederickVA', '51069'),
('County.GilesVA', '51071'),
('County.GloucesterVA', '51073'),
('County.GoochlandVA', '51075'),
('County.GraysonVA', '51077'),
('County.GreeneVA', '51079')

INSERT INTO @LookupMapper ([Key], ExternalId)
VALUES
('County.GreensvilleVA', '51081'),
('County.HalifaxVA', '51083'),
('County.HanoverVA', '51085'),
('County.HenricoVA', '51087'),
('County.HenryVA', '51089'),
('County.HighlandVA', '51091'),
('County.Isle of WightVA', '51093'),
('County.James CityVA', '51095'),
('County.King and QueenVA', '51097'),
('County.King GeorgeVA', '51099'),
('County.King WilliamVA', '51101'),
('County.LancasterVA', '51103'),
('County.LoudounVA', '51107'),
('County.LouisaVA', '51109'),
('County.LunenburgVA', '51111'),
('County.MadisonVA', '51113'),
('County.MathewsVA', '51115'),
('County.MecklenburgVA', '51117'),
('County.MiddlesexVA', '51119'),
('County.MontgomeryVA', '51121'),
('County.NelsonVA', '51125'),
('County.New KentVA', '51127'),
('County.NorthamptonVA', '51131'),
('County.NorthumberlandVA', '51133'),
('County.NottowayVA', '51135'),
('County.OrangeVA', '51137'),
('County.PageVA', '51139'),
('County.PatrickVA', '51141'),
('County.PittsylvaniaVA', '51143'),
('County.PowhatanVA', '51145'),
('County.Prince EdwardVA', '51147'),
('County.Prince GeorgeVA', '51149'),
('County.Prince WilliamVA', '51153'),
('County.PulaskiVA', '51155'),
('County.RappahannockVA', '51157'),
('County.RichmondVA', '51159'),
('County.RoanokeVA', '51161'),
('County.RockbridgeVA', '51163'),
('County.RockinghamVA', '51165'),
('County.ShenandoahVA', '51171'),
('County.SouthamptonVA', '51175'),
('County.SpotsylvaniaVA', '51177'),
('County.StaffordVA', '51179'),
('County.SurryVA', '51181'),
('County.SussexVA', '51183'),
('County.WarrenVA', '51187'),
('County.WestmorelandVA', '51193'),
('County.WytheVA', '51197'),
('County.YorkVA', '51199'),
('County.AdamsWA', '53001'),
('County.AsotinWA', '53003'),
('County.BentonWA', '53005'),
('County.ChelanWA', '53007'),
('County.ClallamWA', '53009'),
('County.ClarkWA', '53011'),
('County.ColumbiaWA', '53013'),
('County.CowlitzWA', '53015'),
('County.DouglasWA', '53017'),
('County.FerryWA', '53019'),
('County.FranklinWA', '53021'),
('County.GarfieldWA', '53023'),
('County.GrantWA', '53025'),
('County.Grays HarborWA', '53027'),
('County.IslandWA', '53029'),
('County.JeffersonWA', '53031'),
('County.KingWA', '53033'),
('County.KitsapWA', '53035'),
('County.KittitasWA', '53037'),
('County.KlickitatWA', '53039'),
('County.LewisWA', '53041'),
('County.LincolnWA', '53043'),
('County.MasonWA', '53045'),
('County.OkanoganWA', '53047'),
('County.PacificWA', '53049'),
('County.Pend OreilleWA', '53051'),
('County.PierceWA', '53053'),
('County.San JuanWA', '53055'),
('County.SkagitWA', '53057'),
('County.SkamaniaWA', '53059'),
('County.SnohomishWA', '53061'),
('County.SpokaneWA', '53063'),
('County.StevensWA', '53065'),
('County.ThurstonWA', '53067'),
('County.WahkiakumWA', '53069'),
('County.Walla WallaWA', '53071'),
('County.WhatcomWA', '53073'),
('County.WhitmanWA', '53075'),
('County.YakimaWA', '53077'),
('County.BarbourWV', '54001'),
('County.BerkeleyWV', '54003'),
('County.BraxtonWV', '54007'),
('County.BrookeWV', '54009'),
('County.CalhounWV', '54013'),
('County.ClayWV', '54015'),
('County.DoddridgeWV', '54017'),
('County.FayetteWV', '54019'),
('County.GilmerWV', '54021'),
('County.GrantWV', '54023'),
('County.GreenbrierWV', '54025'),
('County.HampshireWV', '54027'),
('County.HancockWV', '54029'),
('County.HardyWV', '54031'),
('County.HarrisonWV', '54033'),
('County.JacksonWV', '54035'),
('County.JeffersonWV', '54037'),
('County.LewisWV', '54041'),
('County.MarionWV', '54049'),
('County.MarshallWV', '54051'),
('County.MineralWV', '54057'),
('County.MonongaliaWV', '54061'),
('County.MonroeWV', '54063'),
('County.MorganWV', '54065'),
('County.NicholasWV', '54067'),
('County.OhioWV', '54069'),
('County.PendletonWV', '54071'),
('County.PleasantsWV', '54073'),
('County.PocahontasWV', '54075'),
('County.PrestonWV', '54077'),
('County.RandolphWV', '54083'),
('County.RitchieWV', '54085'),
('County.RoaneWV', '54087'),
('County.SummersWV', '54089'),
('County.TaylorWV', '54091'),
('County.TuckerWV', '54093'),
('County.TylerWV', '54095'),
('County.UpshurWV', '54097'),
('County.WebsterWV', '54101'),
('County.WetzelWV', '54103'),
('County.WirtWV', '54105'),
('County.WoodWV', '54107'),
('County.AdamsWI', '55001'),
('County.AshlandWI', '55003'),
('County.BarronWI', '55005'),
('County.BayfieldWI', '55007'),
('County.BrownWI', '55009'),
('County.BuffaloWI', '55011'),
('County.BurnettWI', '55013'),
('County.CalumetWI', '55015'),
('County.ChippewaWI', '55017'),
('County.ClarkWI', '55019'),
('County.ColumbiaWI', '55021'),
('County.CrawfordWI', '55023'),
('County.DaneWI', '55025'),
('County.DodgeWI', '55027'),
('County.DoorWI', '55029'),
('County.DouglasWI', '55031'),
('County.DunnWI', '55033'),
('County.Eau ClaireWI', '55035'),
('County.FlorenceWI', '55037'),
('County.Fond du LacWI', '55039'),
('County.ForestWI', '55041'),
('County.GrantWI', '55043'),
('County.GreenWI', '55045'),
('County.Green LakeWI', '55047'),
('County.IowaWI', '55049'),
('County.IronWI', '55051'),
('County.JacksonWI', '55053'),
('County.JeffersonWI', '55055'),
('County.JuneauWI', '55057'),
('County.KenoshaWI', '55059'),
('County.KewauneeWI', '55061'),
('County.La CrosseWI', '55063'),
('County.LafayetteWI', '55065'),
('County.LangladeWI', '55067'),
('County.LincolnWI', '55069'),
('County.ManitowocWI', '55071'),
('County.MarathonWI', '55073'),
('County.MarinetteWI', '55075'),
('County.MarquetteWI', '55077'),
('County.MenomineeWI', '55078'),
('County.MilwaukeeWI', '55079'),
('County.MonroeWI', '55081'),
('County.OcontoWI', '55083'),
('County.OneidaWI', '55085'),
('County.OutagamieWI', '55087'),
('County.OzaukeeWI', '55089'),
('County.PepinWI', '55091'),
('County.PierceWI', '55093'),
('County.PolkWI', '55095'),
('County.PortageWI', '55097'),
('County.PriceWI', '55099'),
('County.RacineWI', '55101'),
('County.RichlandWI', '55103'),
('County.RockWI', '55105'),
('County.RuskWI', '55107'),
('County.SaukWI', '55111'),
('County.SawyerWI', '55113'),
('County.ShawanoWI', '55115'),
('County.SheboyganWI', '55117'),
('County.St. CroixWI', '55109'),
('County.TaylorWI', '55119'),
('County.TrempealeauWI', '55121'),
('County.VernonWI', '55123'),
('County.VilasWI', '55125'),
('County.WalworthWI', '55127'),
('County.WashburnWI', '55129'),
('County.WashingtonWI', '55131'),
('County.WaukeshaWI', '55133'),
('County.WaupacaWI', '55135'),
('County.WausharaWI', '55137'),
('County.WinnebagoWI', '55139'),
('County.WoodWI', '55141'),
('County.AlbanyWY', '56001'),
('County.Big HornWY', '56003'),
('County.CampbellWY', '56005'),
('County.CarbonWY', '56007'),
('County.ConverseWY', '56009'),
('County.CrookWY', '56011'),
('County.FremontWY', '56013'),
('County.GoshenWY', '56015'),
('County.Hot SpringsWY', '56017'),
('County.JohnsonWY', '56019'),
('County.LaramieWY', '56021'),
('County.LincolnWY', '56023'),
('County.NatronaWY', '56025'),
('County.NiobraraWY', '56027'),
('County.ParkWY', '56029'),
('County.PlatteWY', '56031'),
('County.SheridanWY', '56033'),
('County.SubletteWY', '56035'),
('County.SweetwaterWY', '56037'),
('County.TetonWY', '56039'),
('County.UintaWY', '56041'),
('County.WashakieWY', '56043'),
('County.WestonWY', '56045')

INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(LV.Id AS NVARCHAR(50)), 
	C.ExternalId, 
	@CountiesEnum, 
	@ClientDataTag, 
	LV.Id
FROM
	@LookupMapper C
INNER JOIN [Config.LookupItemsView] LV
	ON LV.LookupType = 'Counties'
	AND LV.[Key] = C.[Key]

-- Military service branch

delete	[Config.ExternalLookUpItem]
where   ExternalLookUpType = 203
and     ClientDataTag = @ClientDataTag
  
DELETE FROM @LookupMapper

INSERT INTO @LookupMapper ([Key], ExternalId)
VALUES
	('MilitaryBranchOfService.Army', '1'),
	('MilitaryBranchOfService.AirForce', '3'),
	('MilitaryBranchOfService.MarineCorps', '4'),
	('MilitaryBranchOfService.Navy', '2'),
	('MilitaryBranchOfService.CoastGuard', '5')

INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(LV.Id AS NVARCHAR(50)), 
	C.ExternalId, 
	203, 
	@ClientDataTag, 
	LV.Id
FROM
	@LookupMapper C
INNER JOIN [Config.LookupItemsView] LV
	ON LV.LookupType = 'MilitaryBranchesOfService'
	AND LV.[Key] = C.[Key]

-- Transitioning service member types

delete	[Config.ExternalLookupItem]
where	ExternalLookUpType = 202
and		ClientDataTag = @ClientDataTag

insert	[Config.ExternalLookupItem] (InternalId, ExternalId, ExternalLookUpType, FocusId, ClientDataTag)
values	('Retirement',	1, 202, 1, @ClientDataTag),
		('Discharge',	2, 202, 2, @ClientDataTag),
		('Spouse',		3, 202, 3, @ClientDataTag)

END

