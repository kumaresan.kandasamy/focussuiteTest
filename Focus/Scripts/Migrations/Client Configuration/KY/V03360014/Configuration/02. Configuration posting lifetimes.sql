DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'DaysForForeignLaborH2ALifetime'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('DaysForForeignLaborH2ALifetime', '365', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'DaysForForeignLaborH2BLifetime'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('DaysForForeignLaborH2BLifetime', '365', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'DaysForForeignLaborOtherLifetime'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('DaysForForeignLaborOtherLifetime', '365', 1)

DELETE FROM [config.ConfigurationItem] WHERE [KEY] = 'DaysForJobPostingLifetime'
INSERT INTO [Config.ConfigurationItem] ([Key],[Value],[InternalOnly]) VALUES ('DaysForJobPostingLifetime', '365', 1)