-- The "Change User Name" field in Career Explorer has been changed to use a regular expression where there wasn't one before
-- So that existing clients won't be affected, the default value is being overridden for Career
IF NOT EXISTS(SELECT 1 FROM dbo.[Config.ConfigurationItem] WHERE [Key] = N'IntegrationClient' AND Value = N'3')
BEGIN
	DECLARE @UserTypes TABLE
	(
		UserType NVARCHAR(25)
	)
	INSERT INTO @UserTypes (UserType)
	VALUES ('CareerExplorer'), ('Career'), ('Explorer')

	INSERT INTO dbo.[Config.ConfigurationItem]
	(
		[Key],
		Value,
		InternalOnly
	)
	SELECT
		N'UserNameRegExPattern-Workforce-' + UT.UserType,
		N'.*',
		1
	FROM
		@UserTypes UT
	WHERE
		NOT EXISTS
		(
			SELECT 
				Id 
			FROM 
				dbo.[Config.ConfigurationItem] 
			WHERE 
				[Key] = N'UserNameRegExPattern-Workforce-' + UT.UserType
		)
END
GO
