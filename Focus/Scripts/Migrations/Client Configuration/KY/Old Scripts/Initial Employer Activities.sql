DELETE 
	A
FROM
	[Config.Activity] A
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE
	AC.ActivityType = 1
	
DELETE FROM 
	[Config.ActivityCategory] 
WHERE
	ActivityType = 1
GO
	
INSERT [Config.ActivityCategory] ([LocalisationKey], [Name], [Visible], [Administrable], [ActivityType]) 
VALUES 
		(N'ActivityCategory.014', N'Agreements / Services', 1, 1, 1),
		(N'ActivityCategory.016', N'Contact / Information Provided', 1, 1, 1),
		(N'ActivityCategory.017', N'Events', 1, 1, 1),
		(N'ActivityCategory.018', N'Job & Applicant Management', 1, 1, 1),
		(N'ActivityCategory.019', N'Tax Credit', 1, 1, 1),
		(N'ActivityCategory.020', N'Testing', 1, 1, 1),
		(N'ActivityCategory.021', N'Workshop / Seminars', 1, 1, 1)
GO

DECLARE @Xml XML
DECLARE @Activities TABLE
(
	Id INT IDENTITY(1, 1),
	ExternalId NVARCHAR(10),
	Name NVARCHAR(200),
	ActivityCategoryId bigint
)

SET @Xml = '
<activities>
	<activity category="Agreements / Services" code="5553528">Service: Human Resource Issues</activity>
	<activity category="Agreements / Services" code="1107943">Service: H2A Field Check</activity>
	<activity category="Agreements / Services" code="5553542">Agreement: Business Partnership</activity>
	<activity category="Agreements / Services" code="5553522">Labor Market Preview</activity>
	<activity category="Agreements / Services" code="5553521">Service: Layoff-Response Planning</activity>
	<activity category="Agreements / Services" code="5553520">Service: Link to Company Home Page</activity>
	<activity category="Agreements / Services" code="5553518">Service: Marketing Assistance</activity>
	<activity category="Agreements / Services" code="5553511">Service: Rapid Response / Business Downsizing</activity>
	<activity category="Agreements / Services" code="5553507">Service: Spider Company Home Page </activity>
	<activity category="Agreements / Services" code="5553505">Service: Statutory (legal) Issues</activity>
	<activity category="Agreements / Services" code="5553504">Service: Strategic Planning / Economic Development</activity>
	<activity category="Agreements / Services" code="0">Agreement: Exclusive Hiring</activity>
	<activity category="Agreements / Services" code="0">Agreement: OJT</activity>
	<activity category="Agreements / Services" code="0">Agreement: Other</activity>
	<activity category="Agreements / Services" code="0">Agreement:  Service Plan Created</activity>
	<activity category="Agreements / Services" code="0">Agreement:  Service Plan Updated</activity>
	<activity category="Agreements / Services" code="0">Agreement: Trial Job</activity>
	<activity category="Agreements / Services" code="0">Agreement: Work Experience</activity>
	<activity category="Agreements / Services" code="0">Service: Incumbant Worker Services</activity>
	<activity category="Agreements / Services" code="0">Service: Job Fair Assistance / Preparation</activity>
	<activity category="Agreements / Services" code="0">Service: Not Otherwise Described</activity>
	<activity category="Agreements / Services" code="0">Service: OFCCP Consultation</activity>
	<activity category="Agreements / Services" code="0">Service: Workforce Needs Assessment</activity>
	<activity category="Contact / Information Provided" code="0">E-Mail: Individual</activity>
	<activity category="Contact / Information Provided" code="0">Employer Office Tour</activity>
	<activity category="Contact / Information Provided" code="0">Employer Office Visit</activity>
	<activity category="Contact / Information Provided" code="0">Program / Services Discussion</activity>
	<activity category="Contact / Information Provided" code="0">Info: Bonding Assistance</activity>
	<activity category="Contact / Information Provided" code="0">Info: Business / Support Services (Local staff)</activity>
	<activity category="Contact / Information Provided" code="0">Info: Demand Occupations</activity>
	<activity category="Contact / Information Provided" code="0">Info: Employer Advisory Committee</activity>
	<activity category="Contact / Information Provided" code="0">Info: Equal Opportunity Employment </activity>
	<activity category="Contact / Information Provided" code="0">Info: Federal Contractor</activity>
	<activity category="Contact / Information Provided" code="0">Info: Initial Outreach</activity>
	<activity category="Contact / Information Provided" code="0">Info: Job Fair &amp; Other Events</activity>
	<activity category="Contact / Information Provided" code="0">Info: Job Skill Needs</activity>
	<activity category="Contact / Information Provided" code="0">Info: Labor Market Information / Customized</activity>
	<activity category="Contact / Information Provided" code="0">Info: Labor Market Information / Preview</activity>
	<activity category="Contact / Information Provided" code="0">Info: Local Area Performance</activity>
	<activity category="Contact / Information Provided" code="0">Info: NCRC / Work Keys Program</activity>
	<activity category="Contact / Information Provided" code="0">Info: NCRC / Work Keys Profiling</activity>
	<activity category="Contact / Information Provided" code="0">Info: Online Services / Activities</activity>
	<activity category="Contact / Information Provided" code="0">Info: Other Self-Services</activity>
	<activity category="Contact / Information Provided" code="0">Info: Program Costs / Performance</activity>
	<activity category="Contact / Information Provided" code="0">Info: Rapid Response</activity>
	<activity category="Contact / Information Provided" code="0">Info: Registration / Job Order</activity>
	<activity category="Contact / Information Provided" code="0">Info: Tax Credits</activity>
	<activity category="Contact / Information Provided" code="0">Info: Testing Services</activity>
	<activity category="Contact / Information Provided" code="0">Info: Training / Workshops</activity>
	<activity category="Contact / Information Provided" code="0">Info: USERRA</activity>
	<activity category="Contact / Information Provided" code="0">Info: Workforce Services</activity>
	<activity category="Contact / Information Provided" code="5553500">Info: Unemployment Insurance</activity>
	<activity category="Contact / Information Provided" code="5553499">Info: Untapped Labor-Pool Activities</activity>
	<activity category="Contact / Information Provided" code="5553537">Info: Foreign Labor Certification</activity>
	<activity category="Contact / Information Provided" code="5553539">Info: Economic / Labor Market Issues</activity>
	<activity category="Contact / Information Provided" code="5553536">E-Mail: Blast</activity>
	<activity category="Contact / Information Provided" code="5553529">Follow-Up: General</activity>
	<activity category="Contact / Information Provided" code="5553519">Letter / Mailing</activity>
	<activity category="Contact / Information Provided" code="5553514">Visit Employer Location / On-Site</activity>
	<activity category="Contact / Information Provided" code="5553512">Phone Call / Left Message</activity>
	<activity category="Contact / Information Provided" code="5553502">Tax Audit</activity>
	<activity category="Contact / Information Provided" code="5553497">Info: Veteran Services Outreach</activity>
	<activity category="Contact / Information Provided" code="5553543">Info: Business / Support Services (Central staff)</activity>
	<activity category="Events" code="5553535">Employer Committee Meeting</activity>
	<activity category="Events" code="5553534">Employer Committee Member</activity>
	<activity category="Events" code="5553525">Job Fair Participation</activity>
	<activity category="Events" code="5553515">On-Site Review/Customized LMI</activity>
	<activity category="Events" code="5553516">On-Site Recruitment</activity>
	<activity category="Events" code="1710438">Rapid Response</activity>
	<activity category="Events" code="0">On-Site Interviewing</activity>
	<activity category="Job &amp; Applicant Management" code="0">Job-Order Taking:  Apprenticeship</activity>
	<activity category="Job &amp; Applicant Management" code="0">Job-Order Follow-up</activity>
	<activity category="Job &amp; Applicant Management" code="0">Job-Seeker Eligibility Screening</activity>
	<activity category="Job &amp; Applicant Management" code="0">Job-Seeker Pre-Screening</activity>
	<activity category="Job &amp; Applicant Management" code="0">Job-Seeker Recruitment</activity>
	<activity category="Job &amp; Applicant Management" code="0">Job Development: Regular</activity>
	<activity category="Job &amp; Applicant Management" code="0">Job Development: Targeted</activity>
	<activity category="Job &amp; Applicant Management" code="5553545">Account Management</activity>
	<activity category="Job &amp; Applicant Management" code="5553526">Job Analysis</activity>
	<activity category="Job &amp; Applicant Management" code="5553524">Job-Order Taking:  Regular</activity>
	<activity category="Job &amp; Applicant Management" code="5553523">Job-Order Writing Assistance</activity>
	<activity category="Job &amp; Applicant Management" code="5553544">Job-Seeker Screening</activity>
	<activity category="Job &amp; Applicant Management" code="5553510">Review Employment Applications</activity>
	<activity category="Job &amp; Applicant Management" code="5553509">Self-Directed Search</activity>
	<activity category="Job &amp; Applicant Management" code="5553495">Workforce Recruitment</activity>
	<activity category="Tax Credit" code="5553540">Certification</activity>
	<activity category="Tax Credit" code="5553531">Enterprise Zone</activity>
	<activity category="Tax Credit" code="5553498">UTC</activity>
	<activity category="Tax Credit" code="5553496">Welfare-to-Work</activity>
	<activity category="Tax Credit" code="5553493">Work Opportunities</activity>
	<activity category="Tax Credit" code="0">Other </activity>
	<activity category="Testing" code="0">NCRC / Work Keys: Profiling Completed</activity>
	<activity category="Testing" code="0">NCRC / Work Keys: Profiling Requested</activity>
	<activity category="Testing" code="0">NCRC / Work Keys: Test Applicants</activity>
	<activity category="Testing" code="0">NCRC / Work Keys: Test Incumbants</activity>
	<activity category="Testing" code="5553541">CAB (Comprehensive Ability Battery)</activity>
	<activity category="Testing" code="5553538">Educational/Basic Skill Level</activity>
	<activity category="Testing" code="5553532">Employer Provided (Valid)</activity>
	<activity category="Testing" code="5553506">State-Specific Testing</activity>
	<activity category="Testing" code="5553503">TABE-WF</activity>
	<activity category="Testing" code="1107975">Testing/General</activity>
	<activity category="Testing" code="5553501">Typing / Clerical / Keyboarding</activity>
	<activity category="Testing" code="5553494">Work Keys/NCRC</activity>
	<activity category="Workshop / Seminars" code="5553527">Incumbent Worker Services</activity>
	<activity category="Workshop / Seminars" code="5553513">Other Seminar</activity>
	<activity category="Workshop / Seminars" code="1710441">Self-Service Training</activity>
	<activity category="Workshop / Seminars" code="5553508">Workforce Services</activity>
	<activity category="Workshop / Seminars" code="0">OJT Contracting Assistance</activity>
	<activity category="Workshop / Seminars" code="0">Tax Credit Assistance</activity>
	<activity category="Workshop / Seminars" code="0">Unemployment Insurance Services</activity>
	<activity category="Workshop / Seminars" code="0">Workshop / Seminar Attended</activity>
</activities>'

INSERT INTO @Activities
(
	Name,
	ExternalId,
	ActivityCategoryId
)

SELECT
	T.C.value('text()[1]', 'NVARCHAR(200)') AS Name,
	T.C.value('@code', 'NVARCHAR(100)') AS ExternalId,
	c.Id as ActivityCategoryId

FROM
	@Xml.nodes('/activities/activity') T(C)
	INNER JOIN [Config.ActivityCategory] c ON c.Name = T.C.value('@category','NVARCHAR(100)') AND c.ActivityType = 1


BEGIN TRANSACTION

INSERT INTO [Config.Activity]
(
	LocalisationKey,
	Name,
	Visible,
	Administrable,
	SortOrder,
	Used,
	ExternalId,
	ActivityCategoryId
)
SELECT
	N'Activity.' + RIGHT(N'000' + CAST((SELECT COUNT(Id) + A.Id FROM [Config.Activity]) AS NVARCHAR(3)), 3),
	A.Name,
	1,
	1,
	0,
	0,
	A.ExternalId,
	A.ActivityCategoryId
FROM
	@Activities A
	WHERE A.Name NOT IN (SELECT Name FROM [Config.Activity])


IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

COMMIT TRANSACTION


GO

