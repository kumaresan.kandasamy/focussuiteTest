﻿DELETE 
	A
FROM
	[Config.Activity] A
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE
	AC.ActivityType = 0
	
DELETE FROM 
	[Config.ActivityCategory] 
WHERE
	ActivityType = 0
GO
	
INSERT [Config.ActivityCategory] ([LocalisationKey], [Name], [Visible], [Administrable], [ActivityType]) 
VALUES (N'ActivityCategory.001', N'Assessment', 1, 1, 0),
	     (N'ActivityCategory.002', N'Case Management', 1, 1, 0),
       (N'ActivityCategory.003', N'Counseling', 1, 1, 0),
       (N'ActivityCategory.004', N'Customer Utilization', 1, 1, 0),
       (N'ActivityCategory.005', N'Events', 1, 1, 0),
       (N'ActivityCategory.006', N'Labor Exchange', 1, 1, 0),
	     (N'ActivityCategory.007', N'Orientation', 1, 1, 0),
       (N'ActivityCategory.008', N'Referrals', 1, 1, 0),
       (N'ActivityCategory.009', N'Testing', 1, 1, 0),
       (N'ActivityCategory.010', N'Training', 1, 1, 0),
       (N'ActivityCategory.011', N'UI/Reemployment', 1, 1, 0),
       (N'ActivityCategory.012', N'Veterans', 1, 1, 0),
       (N'ActivityCategory.013', N'Workshops', 1, 1, 0)
GO


DECLARE @Xml XML
DECLARE @Activities TABLE
(
	Id INT IDENTITY(1, 1),
	ExternalId NVARCHAR(10),
	Name NVARCHAR(200),
	ActivityCategoryId bigint
)


SET @Xml = '
<activities>
	<activity category="Assessment" code="9">Assessment Interview - Initial Assessment</activity>
	<activity category="Assessment" code="11">Assessment Services - Career Assessment</activity>
	<activity category="Testing" code="1016">Assessment Test for Adult Basic Education (TABE)</activity>
	<activity category="Assessment" code="1173">Assessment Toyota</activity>
	<activity category="Testing" code="1122">Assessment WorkKeys/National Career Readiness Certificate (NCRC)</activity>
	<activity category="Case Management" code="16">Assigned Case Management</activity>
	<activity category="Veterans" code="15">Assigned Case Manager (VETS)</activity>
	<activity category="Testing" code="25">BEAG Test</activity>
	<activity category="Counseling" code="1252">Career Advising Plan</activity>
	<activity category="Counseling" code="330">Career Guidance</activity>
	<activity category="Case Management" code="106">Case Management</activity>
	<activity category="Case Management" code="1180">Case Management Closed</activity>
	<activity category="Case Management" code="1019">Case Management Other</activity>
	<activity category="Counseling" code="13">Counseling - Group Sessions</activity>
	<activity category="Counseling" code="12">Counseling Individual and Career Planning</activity>
	<activity category="Assessment" code="21">Eligibility Determination</activity>
	<activity category="UI/Reemployment" code="1001">Eligibility Review Program (ERP)</activity>
	<activity category="Counseling" code="240">Employability Skills</activity>
	<activity category="Training" code="131">Entrepreneurial Training</activity>
	<activity category="UI/Reemployment" code="318">Exempted From UI Profiling Mandatory Participation</activity>
	<activity category="Labor Exchange" code="371">External Job Referral</activity>
	<activity category="UI/Reemployment" code="1218">Failure to Report (Individual Re-employment Plan)</activity>
	<activity category="UI/Reemployment" code="1219">Failure to Report (REA)</activity>
	<activity category="Labor Exchange" code="390">Follow Up</activity>
	<activity category="Testing" code="203">General Aptitude Test Battery – Validity Generalization (GATB/VG)</activity>
	<activity category="Testing" code="23">General Aptitude Test Battery (GATB)</activity>
	<activity category="Veterans" code="1214">Incarcerated Veteran Outreach</activity>
	<activity category="Counseling" code="111">Individual Employment Plan (IEP)</activity>
	<activity category="Counseling" code="31">Interest Inventory</activity>
	<activity category="Labor Exchange" code="1242">Interested in Bridges to Opportunity</activity>
	<activity category="Counseling" code="40">Job Coaching</activity>
	<activity category="Labor Exchange" code="38">Job Development Contact</activity>
	<activity category="Events" code="44">Job Fair Attendance</activity>
	<activity category="Events" code="1181">Job Fair Information Provided</activity>
	<activity category="Events" code="36">Job Finding Club</activity>
	<activity category="Counseling" code="1043">Job Preparation Interviewing Skills</activity>
	<activity category="Counseling" code="32">Job Search Planning</activity>
	<activity category="Workshops" code="35">Job Search Workshop</activity>
	<activity category="Referrals" code="1249">KCCGO NEG</activity>
	<activity category="Referrals" code="246">Kentucky Employ Network (KEN)</activity>
	<activity category="UI/Reemployment" code="1227">Labor Market and Career Information Provided (REA)</activity>
	<activity category="Testing" code="24">NATB Test</activity>
	<activity category="Labor Exchange" code="1207">Obtained Employment</activity>
	<activity category="Orientation" code="119">Orientation (Other)</activity>
	<activity category="Orientation" code="301">Orientation (Rapid Response)</activity>
	<activity category="UI/Reemployment" code="1217">Orientation (REA)</activity>
	<activity category="Orientation" code="1232">Orientation (Trade)</activity>
	<activity category="UI/Reemployment" code="362">Orientation (UI Reemployment Service)</activity>
	<activity category="Labor Exchange" code="71">Other Reportable Services (ES, DVOP, LVER)</activity>
	<activity category="Training" code="210">Placed in Training (WIA)</activity>
	<activity category="Case Management" code="18">Received Case Management Services</activity>
	<activity category="Veterans" code="17">Received Case Management Services (VETS)</activity>
	<activity category="Referrals" code="1025">Referral to Adult Education</activity>
	<activity category="Training" code="1157">Referral to External Training Provider</activity>
	<activity category="Referrals" code="1174">Referral to Kentucky Community and Technical College System (KCTCS)</activity>
	<activity category="Referrals" code="1177">Referral to KY Farmworkers</activity>
	<activity category="Referrals" code="1225">Referral to Non-Partner</activity>
	<activity category="Referrals" code="1147">Referral to Office for the Blind (OFB)</activity>
	<activity category="Referrals" code="1035">Referral to Office of Vocational Rehabilitation (OVR)</activity>
	<activity category="Referrals" code="1034">Referral to Veteran Program</activity>
	<activity category="Testing" code="1206">Referral to WorkKeys/National Career Readiness Certificate (NCRC) Testing</activity>
	<activity category="Workshops" code="1250">Referral to Workshop</activity>
	<activity category="Labor Exchange" code="1243">Referred to Bridges to Opportunities</activity>
	<activity category="Referrals" code="57">Referred to Supportive Services - Non-Partner</activity>
	<activity category="Referrals" code="56">Referred to Supportive Services - Partner</activity>
	<activity category="Training" code="205">Referred to Training</activity>
	<activity category="Referrals" code="204">Referred to WIA</activity>
	<activity category="UI/Reemployment" code="1231">RES/REA EUC Focus Career Resume Completed or Updated</activity>
	<activity category="UI/Reemployment" code="1229">RES/REA EUC Job Search Eligibility Confirmed</activity>
	<activity category="UI/Reemployment" code="1230">RES/REA EUC Requirements Fulfilled</activity>
	<activity category="Labor Exchange" code="37">Resume Preparation Assistance</activity>
	<activity category="Labor Exchange" code="34">Resume Writing</activity>
	<activity category="Testing" code="29">SATB Test</activity>
	<activity category="Referrals" code="1209">State Energy Sector Partnership (SESP)</activity>
	<activity category="Referrals" code="1216">Teleworks USA</activity>
	<activity category="Veterans" code="363">Transition Assistance Program Workshop TAP (VETS)</activity>
	<activity category="Customer Utilization" code="1186">Translation Service</activity>
	<activity category="UI/Reemployment" code="1172">Unemployment Insurance Benefits Right Interview</activity>
	<activity category="UI/Reemployment" code="1176">Unemployment Insurance Information</activity>
	<activity category="Customer Utilization" code="46">Utilizing Resource Rooms</activity>
	<activity category="Counseling" code="20">Vocational Guidance (Other)</activity>
	<activity category="Veterans" code="19">Vocational Guidance (VETS)</activity>
	<activity category="Labor Exchange" code="39">Workforce Information Services Staff Assisted (LMI)</activity>
	<activity category="Workshops" code="1251">Workshop Attendance</activity>
</activities>'

INSERT INTO @Activities
(
	Name,
	ExternalId,
	ActivityCategoryId
)

SELECT
	T.C.value('text()[1]', 'NVARCHAR(200)') AS Name,
	T.C.value('@code', 'NVARCHAR(100)') AS ExternalId,
	c.Id as ActivityCategoryId

FROM
	@Xml.nodes('/activities/activity') T(C)
	INNER JOIN [Config.ActivityCategory] c ON c.Name = T.C.value('@category','NVARCHAR(100)') AND c.ActivityType = 0


BEGIN TRANSACTION

INSERT INTO [Config.Activity]
(
	LocalisationKey,
	Name,
	Visible,
	Administrable,
	SortOrder,
	Used,
	ExternalId,
	ActivityCategoryId
)
SELECT
	N'Activity.' + RIGHT(N'000' + CAST((SELECT COUNT(Id) + A.Id FROM [Config.Activity]) AS NVARCHAR(3)), 3),
	A.Name,
	1,
	1,
	0,
	0,
	A.ExternalId,
	A.ActivityCategoryId
FROM
	@Activities A
	WHERE A.Name NOT IN (SELECT Name FROM [Config.Activity])


IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

COMMIT TRANSACTION


GO

