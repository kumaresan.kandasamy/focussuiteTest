DECLARE @ActivityCategories TABLE
(
	OldCategoryName NVARCHAR(50),
	ActivityType INT,
	NewCategoryName NVARCHAR(50),
	NewCategoryKey NVARCHAR(50)
)
INSERT INTO @ActivityCategories (OldCategoryName, ActivityType, NewCategoryName, NewCategoryKey)
VALUES
	('Assessment', 0, 'Intake/Needs Assessment', 'ActivityCategory.001'),
	('Case Management', 0, 'Counseling/Case Management Service (non-Vets)', 'ActivityCategory.002'),
	('Counseling', 0, 'Counseling', 'ActivityCategory.003'),
	('Customer Utilization', 0, 'Customer Utilization', 'ActivityCategory.004'),
	('Events', 0, 'Events', 'ActivityCategory.005'),
	('Labor Exchange', 0, 'Job Search', 'ActivityCategory.006'),
	('Orientation', 0, 'Orientations/Workshops (non-UI)', 'ActivityCategory.007'),
	('Referrals', 0, 'Program /Supportive Service Referrals', 'ActivityCategory.008'),
	('Testing', 0, 'Testing/Assessments', 'ActivityCategory.009'),
	('Training', 0, 'Training Enrollments/Referrals', 'ActivityCategory.010'),
	('UI/Reemployment', 0, 'UI/Re-employment', 'ActivityCategory.011'),
	('Veterans', 0, 'Counseling/Case Management/Services (Vets)', 'ActivityCategory.012'),
	('Workshops', 0, 'Workshops', 'ActivityCategory.013'),
	('', 0, 'Youth Services', 'ActivityCategory.023'),
	
	('Agreements / Services', 1, 'Agreements/Services', 'ActivityCategory.014'),
	('Contact / Information Provided', 1, 'Contact/Info Provided', 'ActivityCategory.016'),
	('Events', 1, 'Events', 'ActivityCategory.017'),
	('Job & Applicant Management', 1, 'Job and Applicant Management', 'ActivityCategory.018'),
	('Tax Credit', 1, 'Tax Credits', 'ActivityCategory.019'),
	('Testing', 1, 'Testing/Assessments', 'ActivityCategory.020'),
	('Workshop / Seminars', 1, 'Workshops/Seminars', 'ActivityCategory.021')
	
UPDATE
	AC
SET
	Name = TAC.NewCategoryName
FROM
	@ActivityCategories TAC
INNER JOIN [Config.ActivityCategory] AC
	ON AC.ActivityType = TAC.ActivityType
	AND AC.Name = TAC.OldCategoryName
WHERE
	TAC.OldCategoryName <> ''
	AND TAC.NewCategoryName <> AC.Name
	
INSERT INTO [Config.ActivityCategory] (LocalisationKey, Name, Visible, Administrable, ActivityType, Editable)
SELECT
	TAC.NewCategoryKey,
	TAC.NewCategoryName,
	1,
	1,
	TAC.ActivityType,
	0
FROM
	@ActivityCategories TAC
WHERE
	TAC.OldCategoryName = ''
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.ActivityCategory] AC
		WHERE
			AC.ActivityType = TAC.ActivityType
			AND AC.Name = TAC.NewCategoryName
	)

DECLARE @Activities TABLE
(
	ActivityCategoryName NVARCHAR(250),
	ActivityType INT,
	OldActivityName NVARCHAR(250),
	NewActivityName NVARCHAR(250),
	NewActivityKey NVARCHAR(20),
	ExternalId INT,
	OldExternalId INT
)
INSERT INTO @Activities (ActivityCategoryName, ActivityType, OldActivityName, NewActivityName, NewActivityKey, ExternalId)
VALUES 
	('Intake/Needs Assessment', 0, 'Assessment Services - Career Assessment', 'Career Assessment: Initial', 'Activity.800', 11),
	('Intake/Needs Assessment', 0, '', 'Eligibility Determination-HCTC', 'Activity.801', 1149),
	('Intake/Needs Assessment', 0, '', 'Eligibility Determination-Trade', 'Activity.802', 1202),
	('Intake/Needs Assessment', 0, '', 'Eligibility Determination-Trade A/RTAA', 'Activity.803', 1200),
	('Intake/Needs Assessment', 0, 'Eligibility Determination', 'Eligibility Determination-Regular', 'Activity.804', 21),
	('Intake/Needs Assessment', 0, 'Assessment Interview - Initial Assessment', 'Initial Interview/Assessment', 'Activity.805', 9),
	('Intake/Needs Assessment', 0, '', 'Gold Card Services Eligible-Accepted', 'Activity.806', 1220),
	('Intake/Needs Assessment', 0, '', 'Gold Card Services Eligible-Declined', 'Activity.807', 1221),

	('Counseling/Case Management Service (non-Vets)', 0, 'Assigned Case Management', 'Case Management: Assigned (Non-Vets)', 'Activity.808', 16),
	('Counseling/Case Management Service (non-Vets)', 0, 'Case Management', 'Case Management: Client-Centered', 'Activity.809', 106),
	('Counseling/Case Management Service (non-Vets)', 0, 'Case Management Closed', 'Case Management: Closed', 'Activity.810', 1080),
	('Counseling/Case Management Service (non-Vets)', 0, 'Follow Up', 'Case Management: Follow-up (General)', 'Activity.811', 390),
	('Counseling/Case Management Service (non-Vets)', 0, 'Case Management Other', 'Case Management: Other', 'Activity.812', 1019),
	('Counseling/Case Management Service (non-Vets)', 0, 'Received Case Management Services', 'Case Management: Received Services (Non-Vets)', 'Activity.813', 18),
	('Counseling/Case Management Service (non-Vets)', 0, 'Career Assessment: Initial', 'Counseling: Career Assessment/Comprehensive', 'Activity.814', 11),
	('Counseling/Case Management Service (non-Vets)', 0, 'Career Guidance', 'Counseling: Career Guidance', 'Activity.815', 330),
	('Counseling/Case Management Service (non-Vets)', 0, 'Employability Skills', 'Counseling: Employability Skills', 'Activity.816', 240),
	('Counseling/Case Management Service (non-Vets)', 0, 'Counseling - Group Sessions', 'Counseling: Group Sessions', 'Activity.817', 13),
	('Counseling/Case Management Service (non-Vets)', 0, 'Individual Employment Plan (IEP)', 'Counseling: Individual Employment Plan Developed', 'Activity.818', 111),
	('Counseling/Case Management Service (non-Vets)', 0, 'Counseling Individual and Career Planning', 'Counseling: Individual & Career Planning', 'Activity.819', 12),
	('Counseling/Case Management Service (non-Vets)', 0, 'Interest Inventory', 'Counseling: Interest Inventory', 'Activity.820', 31),
	('Counseling/Case Management Service (non-Vets)', 0, 'Job Coaching', 'Counseling: Job Coaching', 'Activity.821', 40),
	('Counseling/Case Management Service (non-Vets)', 0, 'Job Finding Club', 'Counseling: Job Finding Club', 'Activity.822', 36),
	('Counseling/Case Management Service (non-Vets)', 0, 'Job Preparation Interviewing Skills', 'Counseling: Job Preparation/Interviewing Skills', 'Activity.823', 1043),
	('Counseling/Case Management Service (non-Vets)', 0, 'Job Search Planning', 'Counseling: Job Search Planning', 'Activity.824', 32),
	('Counseling/Case Management Service (non-Vets)', 0, '', 'Counseling: Post-Placement', 'Activity.825', 42),
	('Counseling/Case Management Service (non-Vets)', 0, 'Vocational Guidance (Other)', 'Counseling: Vocational Guidance-Other', 'Activity.826', 20),
	('Counseling/Case Management Service (non-Vets)', 0, '', 'Service: Alternate Work Experience', 'Activity.827', 105),
	('Counseling/Case Management Service (non-Vets)', 0, '', 'Service: Incumbant Worker Service', 'Activity.828', 1151),
	('Counseling/Case Management Service (non-Vets)', 0, '', 'Service: Internship', 'Activity.829', 113),
	('Counseling/Case Management Service (non-Vets)', 0, 'Other Reportable Services (ES, DVOP, LVER)', 'Service: Other Reportable WF', 'Activity.830', 71),
	('Counseling/Case Management Service (non-Vets)', 0, '', 'Service: Relocation Assistance', 'Activity.831', 1044),
	('Counseling/Case Management Service (non-Vets)', 0, 'Resume Preparation Assistance', 'Service: Resume Preparation Assistance', 'Activity.832', 37),
	('Counseling/Case Management Service (non-Vets)', 0, 'Workforce Information Services Staff Assisted (LMI)', 'Workforce Info Services-Staff-Assisted LMI', 'Activity.833', 39),
	
	('Counseling/Case Management/Services (Vets)', 0, '', 'Info: VRAP - Cannot contact customer', 'Activity.834', 1239),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Info: VRAP - Customer contact attempted', 'Activity.835', 1235),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Info: VRAP - Customer found employment', 'Activity.836', 1237),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Info: VRAP - Customer needs assistance', 'Activity.837', 1236),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Info: VRAP - Customer needs no assistance', 'Activity.838', 1238),
	('Counseling/Case Management/Services (Vets)', 0, 'Assigned Case Manager (VETS)', 'Case Management: Assigned (Veterans)', 'Activity.839', 15),
	('Counseling/Case Management/Services (Vets)', 0, 'Case Management: Client-Centered', 'Case Management: Client-Centered', 'Activity.840', 106),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Case Management: Closed (Veterans)', 'Activity.841', 1080),
	('Counseling/Case Management/Services (Vets)', 0, 'Follow Up', 'Case Management: Follow-up (General)', 'Activity.842', 390),
	('Counseling/Case Management/Services (Vets)', 0, 'Case Management: Other', 'Case Management: Other', 'Activity.843', 1019),
	('Counseling/Case Management/Services (Vets)', 0, 'Received Case Management Services (VETS)', 'Case Management: Received Services (Veterans)', 'Activity.844', 17),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Career Assessment/Comprehensive', 'Activity.845', 11),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Career Guidence', 'Activity.846', 330),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Employability Skills', 'Activity.847', 240),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Group Sessions', 'Activity.848', 13),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Individual Employment Plan Developed', 'Activity.849', 111),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Individual & Career Planning', 'Activity.850', 12),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Interest Inventory', 'Activity.851', 31),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Job Coaching', 'Activity.852', 40),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Job Finding Club', 'Activity.853', 36),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Job Preparation/ Interviewing Skills', 'Activity.854', 1043),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Counseling: Job Search Planning', 'Activity.855', 32),
	('Counseling/Case Management/Services (Vets)', 0, 'Vocational Guidance (VETS)', 'Counseling: Vocational Guidance (Vets)', 'Activity.856', 19),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Service: Alternate Work Experience', 'Activity.857', 105),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Service: Gold Card Outreach', 'Activity.858', 1222),
	('Counseling/Case Management/Services (Vets)', 0, 'Incarcerated Veteran Outreach', 'Service: Incarcerated Veteran Outreach', 'Activity.859', 1214),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Service: Incumbant Worker', 'Activity.860', 1151),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Service: Internship', 'Activity.861', 113),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Service: Other Reportable Veteran', 'Activity.862', 71),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Service: Relocation Assistance', 'Activity.863', 1044),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Service: Resume Preparation Assistance', 'Activity.864', 37),
	('Counseling/Case Management/Services (Vets)', 0, '', 'Workforce Info Services-Staff-Assisted LMI', 'Activity.865', 39),
	('Counseling/Case Management/Services (Vets)', 0, 'Transition Assistance Program Workshop TAP (VETS)', 'Workshop: TAP', 'Activity.866', 363),
	
	('Customer Utilization', 0, '', 'Mobile Job Center', 'Activity.867', 1188),
	('Customer Utilization', 0, 'Utilizing Resource Rooms', 'Resource Rooms', 'Activity.868', 46),
	('Customer Utilization', 0, 'Translation Service', 'Translation Services', 'Activity.869', 1186),
	
	('Events', 0, 'Job Fair Attendance', 'Job Fair Attended', 'Activity.870', 44),
	('Events', 0, 'Job Fair Information Provided', 'Job Fair Information Provided', 'Activity.871', 1181),
	
	('Job Search', 0, '', 'Assisted Job Search', 'Activity.872', 114),
	('Job Search', 0, 'External Job Referral', 'External Job Referral', 'Activity.873', 371),
	('Job Search', 0, 'Job Development Contact', 'Job Development Contact', 'Activity.874', 38),
	('Job Search', 0, 'Obtained Employment', 'Obtained Employment', 'Activity.875', 1207),
	('Job Search', 0, '', 'Refused Referral (Talent feed)', 'Activity.876', 54),
	
	('Orientations/Workshops (non-UI)', 0, 'Orientation (Other)', 'Orientation: Other', 'Activity.877', 119),
	('Orientations/Workshops (non-UI)', 0, 'Orientation (Rapid Response)', 'Orientation: Rapid Response', 'Activity.878', 301),
	('Orientations/Workshops (non-UI)', 0, 'Orientation (Trade)', 'Orientation: Trade', 'Activity.879', 1232),
	('Orientations/Workshops (non-UI)', 0, '', 'Workshop: Attended', 'Activity.880', 1250),
	('Orientations/Workshops (non-UI)', 0, '', 'Workshop: Referred', 'Activity.881', 1251),
	('Orientations/Workshops (non-UI)', 0, '', 'Workshop: Interview Skills', 'Activity.882', 1041),
	('Orientations/Workshops (non-UI)', 0, '', 'Workshop: Job Search', 'Activity.883', 35),
	('Orientations/Workshops (non-UI)', 0, '', 'Workshop: Occupational Skills', 'Activity.884', 134),
	('Orientations/Workshops (non-UI)', 0, '', 'Workshop: Resume Writing', 'Activity.885', 34),

	('Program /Supportive Service Referrals', 0, '', 'Adult Daycare', 'Activity.886', 1086),
	('Program /Supportive Service Referrals', 0, '', 'Bridges to Opportunities: Interest', 'Activity.887', 1242),
	('Program /Supportive Service Referrals', 0, '', 'Bridges to Opportunities: Referral', 'Activity.888', 1243),
	('Program /Supportive Service Referrals', 0, '', 'Child Care: General', 'Activity.889', 123),
	('Program /Supportive Service Referrals', 0, '', 'Energy: LIHEAP', 'Activity.890', 1066), 
	('Program /Supportive Service Referrals', 0, 'State Energy Sector Partnership (SESP)', 'Energy: State Energy Sector Partnership (SESP)', 'Activity.891', 1209),
	('Program /Supportive Service Referrals', 0, '', 'Goodwill', 'Activity.892', 1026),
	('Program /Supportive Service Referrals', 0, '', 'Headstart: Early', 'Activity.893', 1082),
	('Program /Supportive Service Referrals', 0, '', 'Headstart: Regular', 'Activity.894', 1081),
	('Program /Supportive Service Referrals', 0, '', 'HOME', 'Activity.895', 1240),
	('Program /Supportive Service Referrals', 0, '', 'Housing: Regular Assistance', 'Activity.896', 122),
	('Program /Supportive Service Referrals', 0, '', 'Housing: Tenant-Based Rental Assistance', 'Activity.897', 1075),
	('Program /Supportive Service Referrals', 0, 'KCCGO NEG', 'KCCGO NEG', 'Activity.898', 1249),
	('Program /Supportive Service Referrals', 0, 'Kentucky Employ Network (KEN)', 'Kentucky Employ Network (KEN)', 'Activity.899', 246),
	('Program /Supportive Service Referrals', 0, 'Teleworks USA', 'Kentucky Teleworks', 'Activity.900', 1216),
	('Program /Supportive Service Referrals', 0, '', 'Services for Victims-Spouse Abuse Shelter', 'Activity.901', 1080),
	('Program /Supportive Service Referrals', 0, '', 'Services for Victims-VOCA', 'Activity.902', 1084),
	('Program /Supportive Service Referrals', 0, '', 'Services for Visual Assistance/Exams', 'Activity.903', 1089),
	('Program /Supportive Service Referrals', 0, 'Referral to Office for the Blind (OFB)', 'Services for Visually Impaired', 'Activity.904', 1147),
	('Program /Supportive Service Referrals', 0, 'Referral to Office of Vocational Rehabilitation (OVR)', 'Services for Vocational Rehabilitation', 'Activity.905', 1035),
	('Program /Supportive Service Referrals', 0, 'Referred to Supportive Services - Non-Partner', 'Supportive Service-Non-Partner', 'Activity.906', 57),
	('Program /Supportive Service Referrals', 0, 'Referred to Supportive Services - Partner', 'Supportive Service-Partner', 'Activity.907', 56),
	('Program /Supportive Service Referrals', 0, '', 'Transportation Assistance', 'Activity.908', 125),
	('Program /Supportive Service Referrals', 0, '', 'Wagner-Peyser Program', 'Activity.909', 1036),
	
	('Testing/Assessments', 0, 'BEAG Test', 'BEAG', 'Activity.910', 25),
	('Testing/Assessments', 0, '', 'Career Advising Plan', 'Activity.911', 1252),
	('Testing/Assessments', 0, 'General Aptitude Test Battery (GATB)', 'GATB', 'Activity.912', 23),
	('Testing/Assessments', 0, 'General Aptitude Test Battery � Validity Generalization (GATB/VG)', 'GATB/VG', 'Activity.913', 203),
	('Testing/Assessments', 0, '', 'Literacy Skills', 'Activity.914', 26),
	('Testing/Assessments', 0, 'NATB Test', 'NATB', 'Activity.915', 24),
	('Testing/Assessments', 0, 'Assessment WorkKeys/National Career Readiness Certificate (NCRC)', 'NCRC: National Career Readiness Certificate', 'Activity.916', 1122),
	('Testing/Assessments', 0, 'Referral to WorkKeys/National Career Readiness Certificate (NCRC) Testing', 'NCRC/Work Keys Referral', 'Activity.917', 1206),
	('Testing/Assessments', 0, '', 'Other/Merit', 'Activity.918', 30),
	('Testing/Assessments', 0, '', 'Predictive Index Behavioral Assessment', 'Activity.919', 1233),
	('Testing/Assessments', 0, 'SATB Test', 'SATB', 'Activity.920', 29),
	('Testing/Assessments', 0, 'Assessment Test for Adult Basic Education (TABE)', 'TABE-WF: Assessment', 'Activity.921', 1016), 
	('Testing/Assessments', 0, '', 'Toyota: Assessment', 'Activity.922', 1173),
	
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Adult Education & Literacy', 'Activity.923', 132),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Apprenticeship', 'Activity.924', 397),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Basic Skills', 'Activity.925', 128),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Computer Skills', 'Activity.926', 224),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Customized Training', 'Activity.927', 129),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: English as Second Language', 'Activity.928', 130),
	('Training Enrollments/Referrals', 0, 'Entrepreneurial Training', 'Enrolled: Entrepreneurial', 'Activity.929', 131),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Ethics S.E.N.S.E.', 'Activity.930', 1246),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Job Corps', 'Activity.931', 211),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: KY Farmworkers', 'Activity.932', 1178),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: On-the-Job Training', 'Activity.933', 135),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Other', 'Activity.934', 67),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Other Federal (Excluding OJT-DAV)', 'Activity.935', 212),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Post-secondary', 'Activity.936', 66),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Private-Sector Training', 'Activity.937', 139),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Secondary', 'Activity.938', 65),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Skills Upgrade Retraining', 'Activity.939', 138),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: TAA-CCCT', 'Activity.940', 2141),
	('Training Enrollments/Referrals', 0, 'Placed in Training (WIA)', 'Enrolled: WIA Services', 'Activity.941', 210),
	('Training Enrollments/Referrals', 0, '', 'Enrolled: Workplace Training Instruction', 'Activity.942', 141),
	('Training Enrollments/Referrals', 0, '', 'Referred: Adult Education', 'Activity.943', 1025),
	('Training Enrollments/Referrals', 0, '', 'Referred: Adult Education & Literacy', 'Activity.944', 132),
	('Training Enrollments/Referrals', 0, '', 'Referred: Apprenticeships', 'Activity.945', 397),
	('Training Enrollments/Referrals', 0, '', 'Referred: Basic Skills', 'Activity.946', 60),
	('Training Enrollments/Referrals', 0, '', 'Referred: Community/Technical College', 'Activity.947', 1174),
	('Training Enrollments/Referrals', 0, '', 'Referred: Dislocated Worker', 'Activity.948', 1038),
	('Training Enrollments/Referrals', 0, '', 'Referred: TAA-CCCT', 'Activity.949', 1246),
	('Training Enrollments/Referrals', 0, 'Referral to External Training Provider', 'Referred: External Provider', 'Activity.950', 1157),
	('Training Enrollments/Referrals', 0, '', 'Referred: GED Training (standalone)', 'Activity.951', 1197),
	('Training Enrollments/Referrals', 0, '', 'Referred: Job Corps', 'Activity.952', 64),
	('Training Enrollments/Referrals', 0, '', 'Referred: KY Farmworkers', 'Activity.953', 1177),
	('Training Enrollments/Referrals', 0, '', 'Referred: MSFW (Federal)', 'Activity.954', 1029),
	('Training Enrollments/Referrals', 0, '', 'Referred: Native American', 'Activity.955', 1031),
	('Training Enrollments/Referrals', 0, '', 'Referral to Non-Partner', 'Activity.956', 1225),
	('Training Enrollments/Referrals', 0, '', 'Referred: Older Worker', 'Activity.957', 1033),
	('Training Enrollments/Referrals', 0, '', 'Referred: On-the-Job Training', 'Activity.958', 135),
	('Training Enrollments/Referrals', 0, '', 'Referred: Private-Sector Training', 'Activity.959', 139),
	('Training Enrollments/Referrals', 0, '', 'Referred: Skills Upgrade Retraining', 'Activity.960', 138),
	('Training Enrollments/Referrals', 0, '', 'Referred: Trade Services', 'Activity.961', 1203),
	('Training Enrollments/Referrals', 0, '', 'Referred: TAA-NAFTA', 'Activity.962', 1030),
	('Training Enrollments/Referrals', 0, 'Referred to Training', 'Referred: Training', 'Activity.963', 205),
	('Training Enrollments/Referrals', 0, '', 'Referred: Veterans Federal', 'Activity.964', 1034),
	('Training Enrollments/Referrals', 0, '', 'Referred: WIA Services', 'Activity.965', 204),
	('Training Enrollments/Referrals', 0, '', 'Referred: WIA Other Program', 'Activity.966', 1037),
	('Training Enrollments/Referrals', 0, '', 'Referred: Workplace Training Instruction', 'Activity.967', 141),
	('Training Enrollments/Referrals', 0, '', 'Training Termination: Successful Other', 'Activity.968', 70),
	('Training Enrollments/Referrals', 0, '', 'Training Termination: Successful Post-secondary', 'Activity.969', 69),
	('Training Enrollments/Referrals', 0, '', 'Training Termination: Successful Secondary', 'Activity.970', 68),
	
	('UI/Re-employment', 0, 'FTR Call-in', 'Basic UI:  FTR Call-in', 'Activity.971', 305),
	('UI/Re-employment', 0, 'Unemployment Insurance Information', 'Basic UI: Information Provided', 'Activity.972', 1176),
	('UI/Re-employment', 0, 'Unemployment Insurance Benefits Right Interview', 'Unemployment Insurance Benefits Right Interview', 'Activity.973', 1172),
	('UI/Re-employment', 0, 'Orientation (UI Reemployment Service)', 'Orientation: Re-employment', 'Activity.974', 362),
	('UI/Re-employment', 0, 'Eligibility Review Program (ERP)', 'ERP: Eligiblilty Review Program', 'Activity.975', 1001),
	('UI/Re-employment', 0, 'Failure to Report (Individual Re-employment Plan)', 'IRP: Failed to Report', 'Activity.976', 1218),
	('UI/Re-employment', 0, '', 'Profiling: Case Management', 'Activity.977', 245),
	('UI/Re-employment', 0, 'Exempted From UI Profiling Mandatory Participation', 'Profiling: Exempted Mandatory', 'Activity.978', 318),
	('UI/Re-employment', 0, '', 'Profiling: Failed to Report', 'Activity.979', 319),
	('UI/Re-employment', 0, '', 'Profiling: Orientation /UI', 'Activity.980', 14),
	('UI/Re-employment', 0, 'Failure to Report (REA)', 'REA: Failed to Report', 'Activity.981', 1219),
	('UI/Re-employment', 0, 'Labor Market and Career Information Provided (REA)', 'REA: Labor Market & Career Information', 'Activity.982', 1227),
	('UI/Re-employment', 0, 'Orientation (REA)', 'REA: Orientation', 'Activity.983', 1217),
	('UI/Re-employment', 0, 'RES/REA EUC Focus Career Resume Completed or Updated', 'REA: Orientation / Online /Self-Services', 'Activity.984', 1231),
	('UI/Re-employment', 0, 'RES/REA EUC Job Search Eligibility Confirmed', 'REA/EUC: Job Search Eligible', 'Activity.985', 1229),
	('UI/Re-employment', 0, 'RES/REA EUC Requirements Fulfilled', 'REA/EUC: Requirements Fulfilled', 'Activity.986', 1230),
	
	('Youth Services', 0, '', 'Referred: Youth Services', 'Activity.987', 1039),
	('Youth Services', 0, '', 'Service: Adult Mentoring', 'Activity.988', 41),
	('Youth Services', 0, '', 'Service: Dropout Prevention Strategies', 'Activity.989', 140),
	('Youth Services', 0, '', 'Service: Internships', 'Activity.990', 113),
	('Youth Services', 0, '', 'Training: Tutoring', 'Activity.991', 140)
	
INSERT INTO @Activities (ActivityCategoryName, ActivityType, OldActivityName, NewActivityName, NewActivityKey, ExternalId, OldExternalId)
VALUES
	('Agreements/Services', 1, 'Agreement: Business Partnership', 'Agreement: Business Partnership', 'Activity.1000', 1009, 5553542),
	('Agreements/Services', 1, 'Agreement: OJT', 'Agreement: OJT', 'Activity.1001', 1024, 0),
	('Agreements/Services', 1, 'Service: Human Resource Issues', 'Service: Human Resource Issues', 'Activity.1002', 10, 5553528),
	('Agreements/Services', 1, 'Service: H2A Field Check', 'Service: H2A Field Check', 'Activity.1003', 1025, 1107943),
	('Agreements/Services', 1, 'Service: Incumbant Worker Services', 'Service: Incumbant Worker Services', 'Activity.1004', 33, 0),
	('Agreements/Services', 1, 'Service: Layoff-Response Planning', 'Service: Layoff-Response Planning', 'Activity.1005', 35, 5553521),
	('Agreements/Services', 1, 'Service: Link to Company Home Page', 'Service: Link to Company Home Page', 'Activity.1006', 9, 5553520),
	('Agreements/Services', 1, 'Service: Marketing Assistance', 'Service: Marketing Assistance', 'Activity.1007', 5, 5553518),
	('Agreements/Services', 1, 'Service: Rapid Response / Business Downsizing', 'Service: Rapid Response/Business Downsizing', 'Activity.1008', 34, 5553511),
	('Agreements/Services', 1, 'Service: Statutory (legal) Issues', 'Service: Statutory (legal) Issues', 'Activity.1009', 11, 5553505),
	('Agreements/Services', 1, 'Service: Strategic Planning / Economic Development', 'Service: Strategic Planning/Economic Development', 'Activity.1010', 30, 5553504),
	('Agreements/Services', 1, '', 'Service: Training Services', 'Activity.1011', 32, 0),
	
	('Contact/Info Provided', 1, 'E-Mail: Blast', 'E-Mail: Blast', 'Activity.1012', 1005, 5553536), 
	('Contact/Info Provided', 1, 'E-Mail: Individual', 'E-Mail: Individual', 'Activity.1013', 1005, 0),
	('Contact/Info Provided', 1, 'Follow-Up: General', 'Follow-Up: General', 'Activity.1014', 1004, 5553529),
	('Contact/Info Provided', 1, 'Letter / Mailing', 'Letter/Mailing', 'Activity.1015', 1, 5553519),
	('Contact/Info Provided', 1, 'Phone Call / Left Message', 'Phone Call/Left Message', 'Activity.1016', 3, 5553512),
	('Contact/Info Provided', 1, '', 'Referred: Business Services', 'Activity.1017', 1026, 0),
	('Contact/Info Provided', 1, 'Tax Audit', 'Tax Audit', 'Activity.1018', 26, 5553502),
	('Contact/Info Provided', 1, 'Visit Employer Location / On-Site', 'Visit Employer Location/On-Site', 'Activity.1019', 2, 5553514),
	('Contact/Info Provided', 1, 'Info: Business / Support Services (Central staff)', 'Info: Business/Support Services (Central staff)', 'Activity.1020', 28, 5553543),
	('Contact/Info Provided', 1, 'Info: Business / Support Services (Local staff)', 'Info: Business/Support Services (Local staff)', 'Activity.1021', 28, 0),
	('Contact/Info Provided', 1, 'Info: Economic / Labor Market Issues', 'Info: Economic/Labor Market Issues', 'Activity.1022', 12, 5553539),
	('Contact/Info Provided', 1, '', 'Info: Employer Company Profile', 'Activity.1023', 4, 0),
	('Contact/Info Provided', 1, 'Info: Foreign Labor Certification', 'Info: Foreign Labor Certifications', 'Activity.1024', 22, 5553537),
	('Contact/Info Provided', 1, 'Info: Job Fair & Other Events', 'Info: Job Fair & Other Events', 'Activity.1025', 14, 0),
	('Contact/Info Provided', 1, '', 'Info: JobFit Employer Documentation', 'Activity.1026', 1001, 0),
	('Contact/Info Provided', 1, '', 'Info: JobFit Surveys', 'Activity.1027', 1002, 0),
	('Contact/Info Provided', 1, 'Info: Labor Market Information / Customized', 'Info: Labor Market Information/Customized', 'Activity.1028', 23, 0),
	('Contact/Info Provided', 1, 'Info: Labor Market Information / Preview', 'Info: Labor Market Information/Preview', 'Activity.1029', 19, 0),
	('Contact/Info Provided', 1, 'Info: NCRC / Work Keys Program', 'Info: NCRC/Work Keys Program', 'Activity.1030', 1023, 0),
	('Contact/Info Provided', 1, 'Info: NCRC / Work Keys Profiling', 'Info: NCRC/Work Keys Profiling', 'Activity.1031', 1023, 0),
	('Contact/Info Provided', 1, 'Info: Rapid Response', 'Info: Rapid Response', 'Activity.1032', 1007, 0),
	('Contact/Info Provided', 1, 'Info: Unemployment Insurance', 'Info: Unemployment Insurance', 'Activity.1033', 27, 5553500),
	('Contact/Info Provided', 1, 'Info: Untapped Labor-Pool Activities', 'Info: Untapped Labor-Pool Activities', 'Activity.1034', 31, 5553499),
	('Contact/Info Provided', 1, 'Info: Veteran Services Outreach', 'Info: Veteran Services Outreach', 'Activity.1035', 1006, 5553497),
	
	('Events', 1, 'Employer Committee Meeting', 'Employer Committee Meeting', 'Activity.1036', 8, 5553535),
	('Events', 1, 'Employer Committee Member', 'Employer Committee Member', 'Activity.1036', 7, 5553534),
	('Events', 1, 'Job Fair Participation', 'Job Fair Participation', 'Activity.1037', 14, 5553525),
	('Events', 1, 'On-Site Review/Customized LMI', 'On-Site Review/Customized LMI', 'Activity.1038', 23, 5553515),
	('Events', 1, 'On-Site Recruitment', 'On-Site Recruitment', 'Activity.1039', 17, 5553516),
	('Events', 1, 'Rapid Response', 'Rapid Response', 'Activity.1040', 1007, 1710438),
	
	('Job and Applicant Management', 1, 'Account Management', 'Account Management', 'Activity.1041', 1008, 5553545),
	('Job and Applicant Management', 1, '', 'Employment / Work History', 'Activity.1042', 1016, 0),
	('Job and Applicant Management', 1, 'Job Analysis', 'Job Analysis', 'Activity.1043', 15, 5553526),
	('Job and Applicant Management', 1, 'Job-Order Taking: Regular', 'Job-Order Taking: Regular', 'Activity.1044', 16, 5553524),
	('Job and Applicant Management', 1, 'Job-Order Writing Assistance', 'Job-Order Writing Assistance', 'Activity.1045', 25, 5553523),
	('Job and Applicant Management', 1, 'Job-Seeker Screening', 'Job-Seeker Screening', 'Activity.1046', 18, 5553544),
	('Job and Applicant Management', 1, '', 'KY Manufacturing Skills Standards', 'Activity.1047', 1022, 0),
	('Job and Applicant Management', 1, 'Review Employment Applications', 'Review Employment Applications', 'Activity.1048', 24, 5553510),
	('Job and Applicant Management', 1, 'Self-Directed Search', 'Self-Directed Search', 'Activity.1049', 1017, 5553509),
	('Job and Applicant Management', 1, 'Workforce Recruitment', 'Workforce Recruitment', 'Activity.1050', 29, 5553495),
	
	('Tax Credits', 1, 'Certification', 'Certification', 'Activity.1051', 21, 5553540),
	('Tax Credits', 1, 'Enterprise Zone', 'Enterprise Zone', 'Activity.1052', 1010, 5553531),
	('Tax Credits', 1, 'UTC', 'UTC', 'Activity.1053', 1011, 5553498),
	('Tax Credits', 1, 'Welfare-to-Work', 'Welfare-to-Work', 'Activity.1054', 1012, 5553496),
	('Tax Credits', 1, 'Work Opportunities', 'Work Opportunities (WOTC)', 'Activity.1055', 1013, 5553493),
	
	('Testing/Assessments', 1, 'CAB (Comprehensive Ability Battery)', 'CAB (Comprehensive Ability Battery)', 'Activity.1056', 1014, 5553541),
	('Testing/Assessments', 1, 'Educational/Basic Skill Level', 'Educational/Basic Skill Level', 'Activity.1057', 1015, 5553538),
	('Testing/Assessments', 1, 'Employer Provided (Valid)', 'Employer Provided (Valid)', 'Activity.1058', 1021, 5553532),
	('Testing/Assessments', 1, 'NCRC / Work Keys: Profiling Completed', 'NCRC/Work Keys: Profiling Completed', 'Activity.1059', 1023, 0),
	('Testing/Assessments', 1, 'NCRC / Work Keys: Profiling Requested', 'NCRC/Work Keys: Profiling Requested', 'Activity.1060', 1023, 0),
	('Testing/Assessments', 1, 'NCRC / Work Keys: Test Applicants', 'NCRC/Work Keys: Test Applicants', 'Activity.1061', 1023, 0),
	('Testing/Assessments', 1, 'NCRC / Work Keys: Test Incumbants', 'NCRC/Work Keys: Test Incumbants', 'Activity.1062', 1023, 0),
	('Testing/Assessments', 1, '', 'TABE', 'Activity.1063', 1018, 0),
	('Testing/Assessments', 1, 'TABE-WF', 'TABE-WF', 'Activity.1064', 1019, 5553503),
	('Testing/Assessments', 1, 'Testing/General', 'Testing/General', 'Activity.1065', 20, 1107975),
	('Testing/Assessments', 1, 'Typing / Clerical / Keyboarding', 'Typing/Clerical/Keyboarding', 'Activity.1066', 1020, 5553501),

	('Workshops/Seminars', 1, 'Incumbent Worker Services', 'Incumbent Worker Services', 'Activity.1067', 33, 5553527),
	('Workshops/Seminars', 1, '', 'JobFit Employer Orientation', 'Activity.1068', 1000, 0),
	('Workshops/Seminars', 1, '', 'JobFit Employer Training', 'Activity.1069', 1003, 0),
	('Workshops/Seminars', 1, 'Other Seminar', 'Other Seminar', 'Activity.1070', 30, 5553513),
	('Workshops/Seminars', 1, 'Self-Service Training', 'Self-Service Training', 'Activity.1071', 6, 1710441)
	
UPDATE
	A
SET
	Name = TA.NewActivityName,
	ExternalId = CASE
		WHEN TA.OldExternalId IS NOT NULL AND TA.OldExternalId = A.ExternalId THEN TA.ExternalId
		ELSE A.ExternalId
	END
FROM
	@Activities TA
INNER JOIN [Config.Activity] A
	ON A.Name = TA.OldActivityName
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
	AND AC.Name = TA.ActivityCategoryName
	AND AC.ActivityType = TA.ActivityType
WHERE
	TA.OldActivityName <> ''

INSERT INTO [Config.Activity] (LocalisationKey, Name, Visible, Administrable, SortOrder, Used, ExternalId, ActivityCategoryId)
SELECT
	TA.NewActivityKey,
	TA.NewActivityName,
	1,
	1,
	0,
	0,
	TA.ExternalId,
	AC.Id
FROM
	@Activities TA
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Name = TA.ActivityCategoryName
	AND AC.ActivityType = TA.ActivityType
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.Activity] A2
		INNER JOIN [Config.ActivityCategory] AC2
			ON AC2.Id = A2.ActivityCategoryId
		WHERE
			AC2.Name = TA.ActivityCategoryName
			AND AC2.ActivityType = TA.ActivityType
			AND (A2.Name = TA.NewActivityName OR A2.ExternalId = TA.ExternalId)
	)

-- Counseling no longer used
UPDATE 
	[Config.ActivityCategory] 
SET 
	Visible = 0 
WHERE 
	[Name] IN ('Counseling', 'Workshops')
	AND ActivityType = 0
	
UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	AC.Name IN ('Counseling', 'Workshops')
	AND AC.ActivityType = 0

-- Incorrect ExternalId in original list
UPDATE 
	A
SET 
	ExternalId = 1080 
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.[Name] = 'Case Management: Closed'
	AND AC.Name = 'Counseling/Case Management Service (non-Vets)'
	AND AC.ActivityType = 0
	
-- Job finding club no longer in Intake/Needs Assessment	
UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0, 
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.[Name] = 'Assessment Toyota' 
	AND AC.Name = 'Intake/Needs Assessment'
	AND AC.ActivityType = 0
	
-- Job finding club no longer in Events	
UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0, 
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.[Name] = 'Job finding club' 
	AND AC.Name = 'Events'
	AND AC.ActivityType = 0

-- Unused/moved activities in Job Search
UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId NOT IN (114, 371, 38, 1207, 54)
	AND AC.Name = 'Job Search'
	AND AC.ActivityType = 0

-- Unused/moved activities in Program /Supportive Service Referrals
UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId IN (1025, 1174, 1177, 1225, 1034, 204)
	AND AC.Name = 'Program /Supportive Service Referrals'
	AND AC.ActivityType = 0
	
-- Unused Employer ones
UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId NOT IN (1009, 1024, 10, 1025, 33, 35, 9, 5, 34, 11, 30, 32)
	AND AC.Name IN ('Agreements/Services')
	AND AC.ActivityType = 1
	
UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId NOT IN (1005, 1004, 1, 3, 1026, 26, 2, 28, 12, 4, 22, 14, 1001, 1002, 23, 19, 1023, 1007, 27, 31, 1006)
	AND AC.Name IN ('Contact/Info Provided')
	AND AC.ActivityType = 1
	
UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId NOT IN (8, 7, 14, 23, 17, 1007)
	AND AC.Name IN ('Events')
	AND AC.ActivityType = 1

UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId NOT IN (1008, 1016, 15, 16, 25, 18, 1022, 24, 1017, 29)
	AND AC.Name IN ('Job and Applicant Management')
	AND AC.ActivityType = 1

UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId NOT IN (21, 1010, 1011, 1012, 1013)
	AND AC.Name IN ('Tax Credits')
	AND AC.ActivityType = 1

UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId NOT IN (1014, 1015, 1021, 1023, 1018, 1019, 20, 1020)
	AND AC.Name IN ('Testing/Assessments')
	AND AC.ActivityType = 1

UPDATE 
	A
SET 
	Visible = 0,
	Administrable = 0,
	ExternalId = 0
FROM
	[Config.Activity] A 
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE 
	A.ExternalId NOT IN (33, 1000, 1003, 30, 6)
	AND AC.Name IN ('Workshops/Seminars')
	AND AC.ActivityType = 1

RETURN

-- RENAME ACTIVITIES (IF NEEDED)
DECLARE @Rename TABLE
(
	CategoryName NVARCHAR(250),
	OldActivityName NVARCHAR(250),
	ActivityType INT,
	ExternalId INT,
	NewActivityName NVARCHAR(250)
)
INSERT INTO @Rename
(
	CategoryName,
	OldActivityName,
	ActivityType,
	ExternalId,
	NewActivityName
)
VALUES
('Intake/Needs Assessment', 'Career Assessment: Initial ', 0, 11, 'Assessment Services - Career Assessment'),
('Intake/Needs Assessment', 'Eligibility Determination-HCTC', 0, 1149, 'HCTC Eligibility Determination'),
('Intake/Needs Assessment', 'Eligibility Determination-Trade', 0, 1202, 'Trade Eligibility Determination'),
('Intake/Needs Assessment', 'Eligibility Determination-Trade A/RTAA', 0, 1200, 'TAA-RTAA Elibility Determination'),
('Intake/Needs Assessment', 'Eligibility Determination-Regular', 0, 21, 'Eligibility Determination'),
('Intake/Needs Assessment', 'Initial Interview/Assessment', 0, 9, 'Assessment Interview - Initial Assessment'),
('Intake/Needs Assessment', 'Gold Card Services Eligible-Accepted', 0, 1220, 'Gold Card - Accepted'),
('Intake/Needs Assessment', 'Gold Card Services Eligible-Declined', 0, 1221, 'Gold Card - Declined'),
('Counseling/Case Management Service (non-Vets)', 'Case Management: Assigned (Non-Vets)', 0, 16, 'Assigned Case Management'),
('Counseling/Case Management Service (non-Vets)', 'Case Management: Client-Centered', 0, 106, 'Case Management'),
('Counseling/Case Management Service (non-Vets)', 'Case Management: Closed', 0, 1080, 'Case Management Closed'),
('Counseling/Case Management Service (non-Vets)', 'Case Management: Follow-up (General)', 0, 390, 'Follow Up'),
('Counseling/Case Management Service (non-Vets)', 'Case Management: Other', 0, 1019, 'Case Management Other'),
('Counseling/Case Management Service (non-Vets)', 'Case Management: Received Services (Non-Vets)', 0, 18, 'Received Case Management Services'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Career Assessment/Comprehensive', 0, 11, 'Assessment Services - Career Assessment'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Career Guidance', 0, 330, 'Career Guidance'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Employability Skills', 0, 240, 'Employability Skills'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Group Sessions', 0, 13, 'Counseling - Group Sessions'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Individual Employment Plan Developed', 0, 111, 'Individual Employment Plan (IEP)'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Individual & Career Planning', 0, 12, 'Counseling Individual & Career Planning'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Interest Inventory', 0, 31, 'Interest Inventory'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Job Coaching', 0, 40, 'Job Coaching'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Job Finding Club', 0, 36, 'Job Finding Club'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Job Preparation/Interviewing Skills', 0, 1043, 'Job Preparation Interviewing Skills'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Job Search Planning', 0, 32, 'Job Search Planning'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Post-Placement', 0, 42, 'Post-Placement Counseling'),
('Counseling/Case Management Service (non-Vets)', 'Counseling: Vocational Guidance-Other', 0, 20, 'Vocational Guidance-Other'),
('Counseling/Case Management Service (non-Vets)', 'Service: Alternate Work Experience', 0, 105, 'Alternate Work Experience'),
('Counseling/Case Management Service (non-Vets)', 'Service: Incumbant Worker Service', 0, 1151, 'Incumbent Worker'),
('Counseling/Case Management Service (non-Vets)', 'Service: Internship', 0, 113, 'Internships'),
('Counseling/Case Management Service (non-Vets)', 'Service: Other Reportable WF', 0, 71, 'Other Reportable Services (ES, DVOP, LVER)'),
('Counseling/Case Management Service (non-Vets)', 'Service: Relocation Assistance', 0, 1044, 'Relocation Assistance'),
('Counseling/Case Management Service (non-Vets)', 'Service: Resume Preparation Assistance', 0, 37, 'Resume Preparation Assistance'),
('Counseling/Case Management Service (non-Vets)', 'Workforce Info Services-Staff-Assisted LMI', 0, 39, 'Workforce Information Services Staff-Assisted (LMI)'),
('Counseling/Case Management/Services (Vets)', 'Info: VRAP - Cannot contact customer', 0, 1239, 'VRAP Contact CANNOT be made'),
('Counseling/Case Management/Services (Vets)', 'Info: VRAP - Customer contact attempted', 0, 1235, 'VRAP Contact Attempt'),
('Counseling/Case Management/Services (Vets)', 'Info: VRAP - Customer found employment', 0, 1237, 'VRAP Contact Made - Customer already found employment'),
('Counseling/Case Management/Services (Vets)', 'Info: VRAP - Customer needs assistance', 0, 1236, 'VRAP Contact Made - Customer needs further assistance'),
('Counseling/Case Management/Services (Vets)', 'Info: VRAP - Customer needs no assistance', 0, 1238, 'VRAP Contact Made - Customer not seeking further assistance'),
('Counseling/Case Management/Services (Vets)', 'Case Management: Assigned (Veterans)', 0, 15, 'Assigned Case Manager (VETS)'),
('Counseling/Case Management/Services (Vets)', 'Case Management: Client-Centered', 0, 106, 'Case Management'),
('Counseling/Case Management/Services (Vets)', 'Case Management: Closed (Veterans)', 0, 1080, 'Case Management Closed'),
('Counseling/Case Management/Services (Vets)', 'Case Management: Follow-up (General)', 0, 390, 'Follow Up'),
('Counseling/Case Management/Services (Vets)', 'Case Management: Other', 0, 1019, 'Case Management Other'),
('Counseling/Case Management/Services (Vets)', 'Case Management: Received Services (Veterans)', 0, 17, 'Received Case Management Services (VETS)'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Career Assessment/Comprehensive', 0, 11, 'Assessment Services - Career Assessment'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Career Guidence', 0, 330, 'Career Guidance'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Employability Skills', 0, 240, 'Employability Skills'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Group Sessions', 0, 13, 'Counseling - Group Sessions'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Individual Employment Plan Developed', 0, 111, 'Individual Employment Plan (IEP)'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Individual & Career Planning', 0, 12, 'Counseling Individual & Career Planning'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Interest Inventory', 0, 31, 'Interest Inventory'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Job Coaching', 0, 40, 'Job Coaching'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Job Finding Club', 0, 36, 'Job Finding Club'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Job Preparation/ Interviewing Skills', 0, 1043, 'Job Preparation'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Job Search Planning', 0, 32, 'Job Search Planning'),
('Counseling/Case Management/Services (Vets)', 'Counseling: Vocational Guidance (Vets) ', 0, 19, 'Vocational Guidance (VETS)'),
('Counseling/Case Management/Services (Vets)', 'Service: Alternate Work Experience', 0, 105, 'Alternate Work Experience'),
('Counseling/Case Management/Services (Vets)', 'Service: Gold Card Outreach', 0, 1222, 'Gold Card Outreach'),
('Counseling/Case Management/Services (Vets)', 'Service: Incarcerated Veteran Outreach', 0, 1214, 'Incarcerated Veteran Outreach'),
('Counseling/Case Management/Services (Vets)', 'Service: Incumbant Worker ', 0, 1151, 'Incumbent Worker'),
('Counseling/Case Management/Services (Vets)', 'Service: Internship', 0, 113, 'Internships'),
('Counseling/Case Management/Services (Vets)', 'Service: Other Reportable Veteran ', 0, 71, 'Other Reportable Services (ES, DVOP, LVER)'),
('Counseling/Case Management/Services (Vets)', 'Service: Relocation Assistance', 0, 1044, 'Relocation Assistance'),
('Counseling/Case Management/Services (Vets)', 'Service: Resume Preparation Assistance', 0, 37, 'Resume Preparation Assistance'),
('Counseling/Case Management/Services (Vets)', 'Workforce Info Services-Staff-Assisted LMI', 0, 39, 'Workforce Info Services-Staff-Assisted LMI'),
('Counseling/Case Management/Services (Vets)', 'Workshop: TAP', 0, 363, 'Transition Assistance Program Workshop TAP (VETS)'),
('Customer Utilization', 'Mobile Job Center', 0, 1188, 'Mobile Job Center'),
('Customer Utilization', 'Resource Rooms', 0, 46, 'Utilizing Resource Rooms'),
('Customer Utilization', 'Translation Services', 0, 1186, 'Translation Services'),
('Events', 'Job Fair Attended', 0, 44, 'Job Fair Attendance'),
('Events', 'Job Fair Information Provided', 0, 1181, 'Job Fair Information Provided'),
('Job Search', 'Assisted Job Search', 0, 114, 'Job Search, Placement Assistance, Career Counseling'),
('Job Search', 'External Job Referral', 0, 371, 'External Job Referral'),
('Job Search', 'Job Development Contact', 0, 38, 'Job Development Contact'),
('Job Search', 'Obtained Employment', 0, 1207, 'Obtained Employment'),
('Job Search', 'Refused Referral (Talent feed)', 0, 54, 'Refused Referral'),
('Orientations/Workshops (non-UI)', 'Orientation: Other', 0, 119, 'Orientation (Other)'),
('Orientations/Workshops (non-UI)', 'Orientation: Rapid Response', 0, 301, 'Orientation (Rapid Response)'),
('Orientations/Workshops (non-UI)', 'Orientation: Trade     ', 0, 1232, 'Orientation (Trade)'),
('Orientations/Workshops (non-UI)', 'Workshop: Attended', 0, 1250, 'Workshop Attendance'),
('Orientations/Workshops (non-UI)', 'Workshop: Referred', 0, 1251, 'Referral to Workshop'),
('Orientations/Workshops (non-UI)', 'Workshop: Interview Skills', 0, 1041, 'Interviewing Skills'),
('Orientations/Workshops (non-UI)', 'Workshop: Job Search', 0, 35, 'Job Search Workshop'),
('Orientations/Workshops (non-UI)', 'Workshop: Occupational Skills', 0, 134, 'Occupational Skills Training'),
('Orientations/Workshops (non-UI)', 'Workshop: Resume Writing', 0, 34, 'Resume Writing (Workshop)'),
('Program /Supportive Service Referrals', 'Adult Daycare', 0, 1086, 'Referral to Adult Daycare'),
('Program /Supportive Service Referrals', 'Bridges to Opportunities: Interest', 0, 1242, 'Interested in Bridges to Opportunity'),
('Program /Supportive Service Referrals', 'Bridges to Opportunities: Referral', 0, 1243, 'Referred to Bridges to Opportunities'),
('Program /Supportive Service Referrals', 'Child Care: General', 0, 123, 'Supportive Services - child care'),
('Program /Supportive Service Referrals', 'Energy: LIHEAP', 0, 1066, 'Referral to LIHEAP'),
('Program /Supportive Service Referrals', 'Energy: State Energy Sector Partnership (SESP)', 0, 1209, 'State Energy Sector Partnership (SESP)'),
('Program /Supportive Service Referrals', 'Goodwill', 0, 1026, 'Referral to Goodwill'),
('Program /Supportive Service Referrals', 'Headstart: Early', 0, 1082, 'Early Headstart Referral'),
('Program /Supportive Service Referrals', 'Headstart: Regular', 0, 1081, 'Headstart Referral'),
('Program /Supportive Service Referrals', 'HOME', 0, 1240, 'HOME'),
('Program /Supportive Service Referrals', 'Housing: Regular Assistance', 0, 122, 'Supportive Services - housing'),
('Program /Supportive Service Referrals', 'Housing: Tenant-Based Rental Assistance', 0, 1075, 'Tenant-based rental assistance referral'),
('Program /Supportive Service Referrals', 'KCCGO NEG', 0, 1249, 'KCCGO NEG'),
('Program /Supportive Service Referrals', 'Kentucky Employ Network (KEN)', 0, 246, 'Kentucky Employ Network (KEN)'),
('Program /Supportive Service Referrals', 'Kentucky Teleworks', 0, 1216, 'Kentucky Teleworks'),
('Program /Supportive Service Referrals', 'Services for Victims-Spouse Abuse Shelter', 0, 1080, 'Domestic Abuse Center Referral'),
('Program /Supportive Service Referrals', 'Services for Victims-VOCA', 0, 1084, 'VOCA Referral'),
('Program /Supportive Service Referrals', 'Services for Visual Assistance/Exams', 0, 1089, 'Referral to Vision Care'),
('Program /Supportive Service Referrals', 'Services for Visually Impaired', 0, 1147, 'Referral to Office for the Blind (OFB)'),
('Program /Supportive Service Referrals', 'Services for Vocational Rehabilitation', 0, 1035, 'Referral to Office of Vocational Rehabilitation (OVR)'),
('Program /Supportive Service Referrals', 'Supportive Service-Non-Partner', 0, 57, 'Supportive Service-Non-Partner'),
('Program /Supportive Service Referrals', 'Supportive Service-Partner', 0, 56, 'Supportive Services-Partner'),
('Program /Supportive Service Referrals', 'Transportation Assistance', 0, 125, 'Supportive Services - Transporation'),
('Program /Supportive Service Referrals', 'Wagner-Peyser Program', 0, 1036, 'Referral to Wagner-Peyser'),
('Testing/Assessments', 'BEAG', 0, 25, 'BEAG Test'),
('Testing/Assessments', 'Career Advising Plan', 0, 1252, 'Career Advising Plan'),
('Testing/Assessments', 'GATB', 0, 23, 'General Aptitude Test Battery (GATB)'),
('Testing/Assessments', 'GATB/VG', 0, 203, 'General Aptitude Test Battery � Validity Generalization (GATB/VG)'),
('Testing/Assessments', 'Literacy Skills', 0, 26, 'Literacy testing'),
('Testing/Assessments', 'NATB', 0, 24, 'NATB Test'),
('Testing/Assessments', 'NCRC: National Career Readiness Certificate', 0, 1122, 'Assessment WorkKeys/National Career Readiness Certificate (NCRC)'),
('Testing/Assessments', 'NCRC/Work Keys Referral', 0, 1206, 'Referral to WorkKeys/National Career Readiness Certificate (NCRC) Testing'),
('Testing/Assessments', 'Other/Merit', 0, 30, 'Other testing'),
('Testing/Assessments', 'Predictive Index Behavioral Assessment', 0, 1233, 'Predictive Index'),
('Testing/Assessments', 'SATB', 0, 29, 'SATB Test'),
('Testing/Assessments', 'TABE-WF: Assessment ', 0, 1016, 'Assessment Test for Adult Basic Education (TABE)'),
('Testing/Assessments', 'Toyota: Assessment', 0, 1173, 'Assessment Toyota'),
('Training Enrollments/Referrals', 'Enrolled: Adult Education & Literacy', 0, 132, 'Literacy Training'),
('Training Enrollments/Referrals', 'Enrolled: Apprenticeship ', 0, 397, 'Apprenticeship Training'),
('Training Enrollments/Referrals', 'Enrolled: Basic Skills ', 0, 128, 'Basic Skills / Life Skills'),
('Training Enrollments/Referrals', 'Enrolled: Computer Skills', 0, 224, 'Computer Skills Training'),
('Training Enrollments/Referrals', 'Enrolled: Customized Training', 0, 129, 'Customized Training'),
('Training Enrollments/Referrals', 'Enrolled: English as Second Language', 0, 130, 'English as Second Language (ESL)'),
('Training Enrollments/Referrals', 'Enrolled: Entrepreneurial', 0, 131, 'Entrepreneurial Training'),
('Training Enrollments/Referrals', 'Enrolled: Ethics S.E.N.S.E.', 0, 1246, 'Ethics S.E.N.S.E.'),
('Training Enrollments/Referrals', 'Enrolled: Job Corps', 0, 211, 'Placed in Training: Job Corps'),
('Training Enrollments/Referrals', 'Enrolled: KY Farmworkers', 0, 1178, 'Placed in KY Farmworkers'),
('Training Enrollments/Referrals', 'Enrolled: On-the-Job Training', 0, 135, 'On the job training'),
('Training Enrollments/Referrals', 'Enrolled: Other', 0, 67, 'Placed in Training Other'),
('Training Enrollments/Referrals', 'Enrolled: Other Federal (Excluding OJT-DAV)', 0, 212, 'Placed in Training-Other Federal'),
('Training Enrollments/Referrals', 'Enrolled: Post-secondary', 0, 66, 'Placed in Post-secondary training'),
('Training Enrollments/Referrals', 'Enrolled: Private-Sector Training', 0, 139, 'Training Programs Operated by the Private Sector'),
('Training Enrollments/Referrals', 'Enrolled: Secondary', 0, 65, 'Placed in Secondary training'),
('Training Enrollments/Referrals', 'Enrolled: Skills Upgrade Retraining', 0, 138, 'Skills Upgrading and Retraining'),
('Training Enrollments/Referrals', 'Enrolled: TAA-CCCT', 0, 2141, 'TAA-CCCT Grant Participation'),
('Training Enrollments/Referrals', 'Enrolled: WIA Services', 0, 210, 'Placed in Training (WIA)'),
('Training Enrollments/Referrals', 'Enrolled: Workplace Training Instruction', 0, 141, 'Workplace Training'),
('Training Enrollments/Referrals', 'Referred: Adult Education', 0, 1025, 'Referral to Adult Education'),
('Training Enrollments/Referrals', 'Referred: Adult Education & Literacy', 0, 132, 'Literacy Training'),
('Training Enrollments/Referrals', 'Referred: Apprenticeships', 0, 397, 'Apprenticeship Training'),
('Training Enrollments/Referrals', 'Referred: Basic Skills ', 0, 60, 'Referred to basic skills training'),
('Training Enrollments/Referrals', 'Referred: Community/Technical College', 0, 1174, 'Referral to Kentucky Community and Technical College System (KCTCS)'),
('Training Enrollments/Referrals', 'Referred: Dislocated Worker', 0, 1038, 'Referral to Dislocated Workers'),
('Training Enrollments/Referrals', 'Referred: TAA-CCCT', 0, 1246, 'TAA-CCCT Grant Participation'),
('Training Enrollments/Referrals', 'Referred: External Provider', 0, 1157, 'Referral to External Training Provider'),
('Training Enrollments/Referrals', 'Referred: GED Training (standalone)', 0, 1197, 'KY-GED'),
('Training Enrollments/Referrals', 'Referred: Job Corps', 0, 64, 'Referred to Job Corps'),
('Training Enrollments/Referrals', 'Referred: KY Farmworkers ', 0, 1177, 'Referral to KY Farmworkers'),
('Training Enrollments/Referrals', 'Referred: MSFW (Federal)', 0, 1029, 'Referral to Migrant and Seasonal Farmworkers'),
('Training Enrollments/Referrals', 'Referred: Native American', 0, 1031, 'Referral to Native Americans'),
('Training Enrollments/Referrals', 'Referral to Non-Partner', 0, 1225, 'Referral to Non-Partner'),
('Training Enrollments/Referrals', 'Referred: Older Worker', 0, 1033, 'Referral to Older Worker'),
('Training Enrollments/Referrals', 'Referred: On-the-Job Training', 0, 135, 'On the job training'),
('Training Enrollments/Referrals', 'Referred: Private-Sector Training', 0, 139, 'Training Programs Operated by the Private Sector'),
('Training Enrollments/Referrals', 'Referred: Skills Upgrade Retraining', 0, 138, 'Skills Upgrading and Retraining'),
('Training Enrollments/Referrals', 'Referred: Trade Services', 0, 1203, 'Referred to Trade'),
('Training Enrollments/Referrals', 'Referred: TAA-CCCT', 0, 1246, 'TAA-CCCT Grant Participation'),
('Training Enrollments/Referrals', 'Referred: TAA-NAFTA', 0, 1030, 'Referral to NAFTA-TAA'),
('Training Enrollments/Referrals', 'Referred: Training', 0, 205, 'Referred to Training'),
('Training Enrollments/Referrals', 'Referred: Veterans Federal', 0, 1034, 'Referral to Veteran Program'),
('Training Enrollments/Referrals', 'Referred: WIA Services', 0, 204, 'Referred to WIA'),
('Training Enrollments/Referrals', 'Referred: WIA Other Program', 0, 1037, 'Referred to WIA Adult'),
('Training Enrollments/Referrals', 'Referred: Workplace Training Instruction', 0, 141, 'Workplace Training'),
('Training Enrollments/Referrals', 'Training Termination: Successful Other', 0, 70, 'Training Termination: Successful Other'),
('Training Enrollments/Referrals', 'Training Termination: Successful Post-secondary', 0, 69, 'Training Termination: Successful Post-secondary'),
('Training Enrollments/Referrals', 'Training Termination: Successful Secondary', 0, 68, 'Training Termination: Successful Secondary'),
('UI/Re-employment', '', 0, 305, 'Basic UI:  FTR Call-in'),
('UI/Re-employment', 'Basic UI: Information Provided', 0, 1176, 'Unemployment Insurance Information'),
('UI/Re-employment', 'Unemployment Insurance Benefits Right Interview', 0, 1172, 'Unemployment Insurance Benefits Right Interview'),
('UI/Re-employment', 'Orientation: Re-employment', 0, 362, 'Orientation (UI Reemployment Service)'),
('UI/Re-employment', 'ERP: Eligiblilty Review Program', 0, 1001, 'Eligibility Review Program (ERP)'),
('UI/Re-employment', 'IRP: Failed to Report', 0, 1218, 'Failure to Report (Individual Re-employment Plan)'),
('UI/Re-employment', 'Profiling: Case Management', 0, 245, 'Profiled Case Managed'),
('UI/Re-employment', 'Profiling: Exempted Mandatory', 0, 318, 'Exempted From UI Profiling Mandatory Participation'),
('UI/Re-employment', 'Profiling: Failed to Report', 0, 319, 'FTR Profiling Orientation'),
('UI/Re-employment', 'Profiling: Orientation /UI', 0, 14, 'UI Profiling Orientation'),
('UI/Re-employment', 'REA: Failed to Report', 0, 1219, 'Failure to Report (REA)'),
('UI/Re-employment', 'REA: Labor Market & Career Information', 0, 1227, 'Labor Market and Career Information Provided (REA)'),
('UI/Re-employment', 'REA: Orientation', 0, 1217, 'Orientation (REA)'),
('UI/Re-employment', 'REA: Orientation / Online /Self-Services', 0, 1231, 'RES/REA EUC Focus Career Resume Completed or Updated'),
('UI/Re-employment', 'REA/EUC: Job Search Eligible', 0, 1229, 'RES/REA EUC Job Search Eligibility Confirmed'),
('UI/Re-employment', 'REA/EUC: Requirements Fulfilled', 0, 1230, 'RES/REA EUC Requirements Fulfilled'),
('Youth Services', 'Referred: Youth Services', 0, 1039, 'Referral to WIA Youth'),
('Youth Services', 'Service: Adult Mentoring', 0, 41, 'Mentoring'),
('Youth Services', 'Service: Dropout Prevention Strategies', 0, 140, 'Tutoring Study Skills Training, Drop-out Prevention Strategies'),
('Youth Services', 'Service: Internships', 0, 113, 'Internships'),
('Youth Services', 'Training: Tutoring', 0, 140, 'Tutoring Study Skills Training, Drop-out Prevention Strategies'),
('Agreements/Services', 'Agreement: Business Partnership', 1, 1009, 'Business Partnership Agreement (KEN)'),
('Agreements/Services', 'Agreement: OJT', 1, 1024, 'OJT -On the Job Training Contract'),
('Agreements/Services', 'Service: Human Resource Issues', 1, 10, 'Human Resource Issues'),
('Agreements/Services', 'Service: H2A Field Check', 1, 1025, 'H-2A Field Checks'),
('Agreements/Services', 'Service: Incumbant Worker Services', 1, 33, 'Incumbent Worker Training Services'),
('Agreements/Services', 'Service: Layoff-Response Planning', 1, 35, 'Layoff-Response Planning'),
('Agreements/Services', 'Service: Link to Company Home Page', 1, 9, 'Link to Company Home Page'),
('Agreements/Services', 'Service: Marketing Assistance', 1, 5, 'Marketing Services Program'),
('Agreements/Services', 'Service: Rapid Response/Business Downsizing', 1, 34, 'Rapid Response/Business Downsizing Assistance'),
('Agreements/Services', 'Service: Statutory (legal) Issues', 1, 11, 'Statuatory (legal) Issues'),
('Agreements/Services', 'Service: Strategic Planning/Economic Development', 1, 30, 'Strategic Planning/Economic Development Activities'),
('Agreements/Services', 'Service: Training Services', 1, 32, 'Training Services'),
('Contact/Info Provided', 'E-Mail: Blast', 1, 1005, 'Employer Contact E-Mail'),
('Contact/Info Provided', 'E-Mail: Individual', 1, 1005, 'Employer Contact E-Mail'),
('Contact/Info Provided', 'Follow-Up: General', 1, 1004, 'Employer Follow-up'),
('Contact/Info Provided', 'Letter/Mailing', 1, 1, 'Employer Contact Mailing'),
('Contact/Info Provided', 'Phone Call/Left Message', 1, 3, 'Employer Contact Phone Call'),
('Contact/Info Provided', 'Referred: Business Services', 1, 1026, 'Referral to Business Services Team'),
('Contact/Info Provided', 'Tax Audit', 1, 26, 'Tax Auditor Contact'),
('Contact/Info Provided', 'Visit Employer Location/On-Site', 1, 2, 'Employer On-Site Visit'),
('Contact/Info Provided', 'Info: Business/Support Services (Central staff)', 1, 28, 'Business Information and Support Services'),
('Contact/Info Provided', 'Info: Business/Support Services (Local staff)', 1, 28, 'Business Information and Support Services'),
('Contact/Info Provided', 'Info: Economic/Labor Market Issues', 1, 12, 'Economic/Labor Market Issues'),
('Contact/Info Provided', 'Info: Employer Company Profile', 1, 4, 'Employer Company Profile'),
('Contact/Info Provided', 'Info: Foreign Labor Certifications', 1, 22, 'Alien Labor Certification'),
('Contact/Info Provided', 'Info: Job Fair & Other Events', 1, 14, 'Job Fairs'),
('Contact/Info Provided', 'Info: JobFit Employer Documentation', 1, 1001, 'JobFit Employer Documentation'),
('Contact/Info Provided', 'Info: JobFit Surveys', 1, 1002, 'JobFit Surveys'),
('Contact/Info Provided', 'Info: Labor Market Information/Customized', 1, 23, 'On-site review customized LMI'),
('Contact/Info Provided', 'Info: Labor Market Information/Preview', 1, 19, 'Labor Market Preview'),
('Contact/Info Provided', 'Info: NCRC/Work Keys Program', 1, 1023, 'Work Keys / NCRC'),
('Contact/Info Provided', 'Info: NCRC/Work Keys Profiling', 1, 1023, 'Work Keys / NCRC'),
('Contact/Info Provided', 'Info: Rapid Response', 1, 1007, 'Rapid Response'),
('Contact/Info Provided', 'Info: Unemployment Insurance', 1, 27, 'UI Information Issues'),
('Contact/Info Provided', 'Info: Untapped Labor-Pool Activities', 1, 31, 'Untapped Labor Pool Activities'),
('Contact/Info Provided', 'Info: Veteran Services Outreach', 1, 1006, 'Veterans Outreach'),
('Events', 'Employer Committee Meeting', 1, 8, 'JSEC Meeting'),
('Events', 'Employer Committee Member', 1, 7, 'JSEC Member'),
('Events', 'Job Fair Participation', 1, 14, 'Job Fairs'),
('Events', 'On-Site Review/Customized LMI', 1, 23, 'On-Site Review/Customized LMI'),
('Events', 'On-Site Recruitment', 1, 17, 'On-Site Recruitment'),
('Events', 'Rapid Response', 1, 1007, 'Rapid Response'),
('Job and Applicant Management', 'Account Management', 1, 1008, 'Account Management'),
('Job and Applicant Management', 'Employment / Work History', 1, 1016, 'Employment / Work History'),
('Job and Applicant Management', 'Job Analysis', 1, 15, 'Job Analysis'),
('Job and Applicant Management', 'Job-Order Taking: Regular', 1, 16, 'Job-Order Taking'),
('Job and Applicant Management', 'Job-Order Writing Assistance', 1, 25, 'Assistance with writing job descriptions'),
('Job and Applicant Management', 'Job-Seeker Screening', 1, 18, 'Job-Seeker Screening'),
('Job and Applicant Management', 'KY Manufacturing Skills Standards', 1, 1022, 'KY Manufacturing Skills Standards'),
('Job and Applicant Management', 'Review Employment Applications', 1, 24, 'Review Employment Applications'),
('Job and Applicant Management', 'Self-Directed Search', 1, 1017, 'SDS (Self-Directed Search)'),
('Job and Applicant Management', 'Workforce Recruitment', 1, 29, 'Workforce Recruitment Assistance'),
('Tax Credits', 'Certification', 1, 21, 'Certification'),
('Tax Credits', 'Enterprise Zone', 1, 1010, 'Enterprise Zone'),
('Tax Credits', 'UTC', 1, 1011, 'UTC'),
('Tax Credits', 'Welfare-to-Work', 1, 1012, 'Welfare-to-Work'),
('Tax Credits', 'Work Opportunities (WOTC)', 1, 1013, 'Work Opportunities'),
('Testing/Assessments', 'CAB (Comprehensive Ability Battery)', 1, 1014, 'CAB (Comprehensive Ability Battery)'),
('Testing/Assessments', 'Educational/Basic Skill Level', 1, 1015, 'Educational/Basic Skill Level'),
('Testing/Assessments', 'Employer Provided (Valid)', 1, 1021, 'Valid Test Provided by Employer'),
('Testing/Assessments', 'NCRC/Work Keys: Profiling Completed', 1, 1023, 'Work Keys / NCRC'),
('Testing/Assessments', 'NCRC/Work Keys: Profiling Requested', 1, 1023, 'Work Keys / NCRC'),
('Testing/Assessments', 'NCRC/Work Keys: Test Applicants', 1, 1023, 'Work Keys / NCRC'),
('Testing/Assessments', 'NCRC/Work Keys: Test Incumbants', 1, 1023, 'Work Keys / NCRC'),
('Testing/Assessments', 'TABE', 1, 1018, 'TABE'),
('Testing/Assessments', 'TABE-WF', 1, 1019, 'TABE / WF'),
('Testing/Assessments', 'Testing/General', 1, 20, 'Testing'),
('Testing/Assessments', 'Typing/Clerical/Keyboarding', 1, 1020, 'Typing/Clerical'),
('Workshops/Seminars', 'Incumbent Worker Services', 1, 33, 'Incumbent Worker Training Services'),
('Workshops/Seminars', 'JobFit Employer Orientation', 1, 1000, 'JobFit Employer Orientation'),
('Workshops/Seminars', 'JobFit Employer Training', 1, 1003, 'JobFit Employer Training'),
('Workshops/Seminars', 'Other Seminar', 1, 30, 'Other Seminar'),
('Workshops/Seminars', 'Self-Service Training', 1, 6, 'Self-Service Training Session')

UPDATE
	A
SET
	Name = TR.NewActivityName
FROM
	@Rename TR
INNER JOIN [Config.Activity] A
	ON A.Name = TR.OldActivityName
	AND A.ExternalId = TR.ExternalId
INNER JOIN [Config.ActivityCategory] C
	ON C.Id = A.ActivityCategoryId
	AND C.ActivityType = TR.ActivityType
	AND C.Name = TR.CategoryName