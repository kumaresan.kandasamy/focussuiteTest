DECLARE @Xml XML
DECLARE @NextId BIGINT

/******************** OFFCIES ********************/

DECLARE @Offices TABLE
(
	Id INT IDENTITY(1, 1),
	OfficeName NVARCHAR(20),
	InActive BIT,
	IsDefault BIT,
	ExternalId NVARCHAR(36),
	AssignedPostcodeZip NVARCHAR(4000)
)

DECLARE @OfficeAssignedZips TABLE
(
	ExternalId NVARCHAR(36),
	PostcodeZip NVARCHAR(20)
)

DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.KY'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

SET @Xml = '
<office_list>
  <office office_id="KYH020">Adair Co Adult Learning Ctr</office>
  <office office_id="KYH006">Adair County WIA</office>
  <office office_id="KY0099">Administrator Services</office>
  <office office_id="KYE005">Airport One Stop Terminal 1WIA</office>
  <office office_id="KY5701">Airport Terminal One OS OET</office>
  <office office_id="KY6102">Albany OET</office>
  <office office_id="KYK010">Allen County BOE AE</office>
  <office office_id="KYF011">Ashland OFB</office>
  <office office_id="KY0069">Ashland OS OET</office>
  <office office_id="KYF010">Ashland OVR</office>
  <office office_id="KYAEAS">Ashland One Stop Adult Education</office>
  <office office_id="KYEWAS">Ashland One Stop Experience Works</office>
  <office office_id="KYF002">Ashland One Stop WIA</office>
  <office office_id="KY0088">Bardstown Career Ctr OET</office>
  <office office_id="KYK008">Barren County BOE AE</office>
  <office office_id="KYK001">Barren River ADD WIA</office>
  <office office_id="KYKKKK">Barren River Office WIA</office>
  <office office_id="KYG024">Bell Co JobSight Bell-Whitley CSBG</office>
  <office office_id="KYG014">Bell Co JobSight Bell-Whitley Early HS</office>
  <office office_id="KYG015">Bell Co JobSight Bell-Whitley Headstart</office>
  <office office_id="KYG038">Bell Co JobSight Bell-Whitley Housing</office>
  <office office_id="KYG030">Bell Co JobSight Bell-Whitley Tax Prep</office>
  <office office_id="KYG010">Bell Co JobSight CEOC</office>
  <office office_id="KYG049">Bell Co JobSight Experience Works</office>
  <office office_id="KYG029">Bell Co JobSight Job Corps</office>
  <office office_id="KYG003">Bell Co JobSight OFB</office>
  <office office_id="KYAEBC">Bell Co JobSight SEKCTC</office>
  <office office_id="KYTRBC">Bell Co JobSight WIA</office>
  <office office_id="KYI004">Bluegrass ADD WIA</office>
  <office office_id="KYK002">Bowling Green Area Career Ctr WIA</office>
  <office office_id="KYK006">Bowling Green OFB</office>
  <office office_id="KY0055">Bowling Green OS Career Ctr OET</office>
  <office office_id="KYK005">Bowling Green OVR</office>
  <office office_id="KYK017">Bowling Green Technical College AE</office>
  <office office_id="KY7501">Brandenburg OET</office>
  <office office_id="KYA006">Breathitt Career Ctr WIA</office>
  <office office_id="KYG063">Breathitt Co Experience Works WIA</office>
  <office office_id="KYG051">Breathitt Co Middle KY River WIA</office>
  <office office_id="KYF001">Buffalo Trace ADD WIA</office>
  <office office_id="KYC002">Bullitt Co. Schools Adult Learning WIA</office>
  <office office_id="KYK011">Butler County BOE AE</office>
  <office office_id="KYA011">Caldwell Co DCBS Office</office>
  <office office_id="KYAECA">Campbellsville Career Ctr Adult Ed</office>
  <office office_id="KYEWCA">Campbellsville Career Ctr Exp Works</office>
  <office office_id="KY0078">Campbellsville Career Ctr OET</office>
  <office office_id="KYH005">Campbellsville Career Ctr WIA</office>
  <office office_id="KY7802">Campbellsville DCI OET</office>
  <office office_id="KYA008">Career Advancement Ctr WIA</office>
  <office office_id="KYA004">Career Discovery Center WIA</office>
  <office office_id="KYC003" inactive="true">Career Resources, Inc. WIA</office>
  <office office_id="KYE008" inactive="true">Carroll Co CFC</office>
  <office office_id="KY0015">Carrollton OET</office>
  <office office_id="KYE004">Carrollton One Stop Affiliate WIA</office>
  <office office_id="KYG052">Carter Co Northeast WIA</office>
  <office office_id="KYH021">Casey Co Adult Learning Ctr</office>
  <office office_id="KYH007">Casey Co WIA</office>
  <office office_id="KY3156">Center for Accessible Living,OET</office>
  <office office_id="KYI003">Central Kentucky Job Ctr Danville WIA</office>
  <office office_id="KYI022">Central Kentucky Job Ctr Frankfort WIA</office>
  <office office_id="KYI002">Central Kentucky Job Ctr Georgetown WIA</office>
  <office office_id="KYI005">Central Kentucky Job Ctr Lexington WIA</office>
  <office office_id="KYI001">Central Kentucky Job Ctr Richmond WIA</office>
  <office office_id="KYI009">Central Kentucky Job Ctr Winchester WIA</office>
  <office office_id="KY0000">Central Office - FLC</office>
  <office office_id="KY00CO">Central Office - KEWES</office>
  <office office_id="KYG016">Clay Co JobSight DB Transportation</office>
  <office office_id="KYG017">Clay Co JobSight DBDC Childcare Ctr</office>
  <office office_id="KYG025">Clay Co JobSight Daniel Boone CSBG</office>
  <office office_id="KYG004">Clay Co JobSight OFB</office>
  <office office_id="KYG042">Clay Co JobSight OVR</office>
  <office office_id="KYKCCC">Clay Co JobSight Somerset Comm College</office>
  <office office_id="KYTRCC">Clay County JobSight WIA</office>
  <office office_id="KYH022">Clinton Co Adult Education Program</office>
  <office office_id="KYH010">Clinton Co Community Ctr WIA</office>
  <office office_id="KY7801">Columbia OET</office>
  <office office_id="KYAECO">Corbin Career Ctr Adult Education</office>
  <office office_id="KYH004">Corbin Career Ctr WIA</office>
  <office office_id="KYH034">Corbin OFB</office>
  <office office_id="KY0064">Corbin OS Career Ctr OET</office>
  <office office_id="KYH030">Corbin OVR</office>
  <office office_id="KY0057">Covington OS OET</office>
  <office office_id="KYCBCV" inactive="true">Covington One Stop DCBS</office>
  <office office_id="KYGCCV">Covington One Stop Gateway Comm Tech</office>
  <office office_id="KYJCCV" inactive="true">Covington One Stop Job Corps</office>
  <office office_id="KYE001">Covington One Stop WIA</office>
  <office office_id="KYH023" inactive="true">Cumberland Co Adult Ed &amp; Family Literacy</office>
  <office office_id="KYH011">Cumberland Co DCBS Office WIA</office>
  <office office_id="KYH015">Cumberland Valley ADD WIA</office>
  <office office_id="KYHHHH" inactive="true">DO NOT USE - CUMBERLAND OFFICE</office>
  <office office_id="KYWWWW">DTR (JAG/JKG) Office WIA</office>
  <office office_id="KY0001" isdefault="true">DTS EKOS</office>
  <office office_id="KYSEC1">DTS Security</office>
  <office office_id="KYI016">Danville Central Kentucky One Stop OVR</office>
  <office office_id="KY0065">Danville Central Ky Job Ctr OET</office>
  <office office_id="KYI015">Danville OVR</office>
  <office office_id="KYJ019">Daviess County Public Library</office>
  <office office_id="KYG001">EKCEP LWIA</office>
  <office office_id="KYGGGG">EKCEP OFFICE</office>
  <office office_id="KYI011">EKU Adult Education Ctr WIA</office>
  <office office_id="KYK012">Edmonson County BOE AE</office>
  <office office_id="KY0075">Elizabethtown Career Ctr OET</office>
  <office office_id="KYB005">Elizabethtown OVR</office>
  <office office_id="KYG053">Elliott Co Northeast WIA</office>
  <office office_id="KYF003">FIVCO ADD WIA</office>
  <office office_id="KY5702">Falmouth OS Career Alliance OET</office>
  <office office_id="KYE007">Falmouth One Stop Affiliate Site WIA</office>
  <office office_id="KYI006" inactive="true">Family Care Center WIA</office>
  <office office_id="KYE015">Florence OFB</office>
  <office office_id="KYE013">Florence OVR</office>
  <office office_id="KYAEFL" inactive="true">Florence One Stop Adult Education</office>
  <office office_id="KYE014">Florence One Stop Annex</office>
  <office office_id="KYCBFL" inactive="true">Florence One Stop DCBS</office>
  <office office_id="KY0081">Florence One Stop OET</office>
  <office office_id="KYE002">Florence One Stop WIA</office>
  <office office_id="KYG054">Floyd Co Big Sandy CAP WIA</office>
  <office office_id="KY5201">Fort Campbell OET</office>
  <office office_id="KY0084">Fort Knox OET</office>
  <office office_id="KY0059">Frankfort Career Center OET</office>
  <office office_id="KY0019">Frankfort Central Office KEWES</office>
  <office office_id="KY8080">Frankfort Central Office OET</office>
  <office office_id="KYI012">Frankfort OFB</office>
  <office office_id="KYI014">Frankfort OVR</office>
  <office office_id="KYA014">Ft Campbell Comm Activities Business Ctr</office>
  <office office_id="KYE009" inactive="true">Gallatin Co CFC</office>
  <office office_id="KYF005" inactive="true">Gateway ADD WIA</office>
  <office office_id="KYI020">Georgetown Central Kentucky Job Ctr OFB</office>
  <office office_id="KY0085">Georgetown Central Ky Job Ctr OET</office>
  <office office_id="KY0073">Glasgow Area Career Ctr OET</office>
  <office office_id="KYK004">Glasgow Area Career Ctr OVR</office>
  <office office_id="KYK003">Glasgow Career Ctr WIA</office>
  <office office_id="KYE012" inactive="true">Grant Co CFC</office>
  <office office_id="KYH024">Green Co Adult Learning Ctr</office>
  <office office_id="KYH008">Green Co Adult Learning Ctr WIA</office>
  <office office_id="KYJ003">Green River ADD WIA</office>
  <office office_id="KYJ014">Hancock Co Career Ctr AE</office>
  <office office_id="KYJ005">Hancock Co Career Ctr WIA</office>
  <office office_id="KY7502">Hardinsburg Career Ctr OET</office>
  <office office_id="KYG050">Harlan Co CAA WIA</office>
  <office office_id="KYG071">Harlan Co CAA WIA Cumberland Campus</office>
  <office office_id="KYG070">Harlan Co CAA WIA Harlan Campus</office>
  <office office_id="KY0066">Harlan OET</office>
  <office office_id="KY6502">Harrodsburg Job Ctr OET</office>
  <office office_id="KYK013">Hart County BOE AE</office>
  <office office_id="KY0068">Hazard OET</office>
  <office office_id="KYJ004">Henderson Career Connections Ctr WIA</office>
  <office office_id="KYj013">Henderson Co AE</office>
  <office office_id="KY0053">Henderson OET</office>
  <office office_id="KYJ008">Henderson OET WIA</office>
  <office office_id="KYJ010">Henderson OVR</office>
  <office office_id="KYA020">Hopkins County Public Library</office>
  <office office_id="KY0052">Hopkinsville OET</office>
  <office office_id="KYA016">Hopkinsville OVR</office>
  <office office_id="KYA019">Hopkinsville PCEP OVR</office>
  <office office_id="KYC021">JCPS Youth Opportunities Unlimited WIA</office>
  <office office_id="KYC018">JCTC Bullitt Co WIA</office>
  <office office_id="KYC015">JCTC Cedar WIA</office>
  <office office_id="KYC024">JCTC Jeffersontown WIA</office>
  <office office_id="KYC019">JCTC Riverport WIA</office>
  <office office_id="KYC020">JCTC Shelby Co Campus WIA</office>
  <office office_id="KYC017">JCTC-Southwest WIA</office>
  <office office_id="KYG055">Jackson Co Daniel Boone WIA</office>
  <office office_id="KY0076">Jackson OET</office>
  <office office_id="KY6802">Jeff JobSight OET</office>
  <office office_id="KYC004">Jefferson Co. Public Schools WIA</office>
  <office office_id="KYC014">Jefferson Community &amp; Technical College</office>
  <office office_id="KY1256">Jefferson ST Baptist Ctr OET</office>
  <office office_id="KYI023">Jessamine County Public Library</office>
  <office office_id="KYA007">JobNet Career Ctr WIA</office>
  <office office_id="KYG047">Johnson Co Big Sandy CAP WIA</office>
  <office office_id="KYG074">KCEOC Community Action Partnership, Inc.</office>
  <office office_id="KYEWBC" inactive="true">KYEWBC</office>
  <office office_id="KYE017">Kenton County Public Library</office>
  <office office_id="KYC001">Kentuckiana Works WIA</office>
  <office office_id="KYC027">Kentuckiana Works YCC (Henry Co)</office>
  <office office_id="KYC028">Kentuckiana Works YCC (Oldham Co)</office>
  <office office_id="KYC029">Kentuckiana Works YCC (Trimble Co)</office>
  <office office_id="KYG056">Knott Co LKLP WIA</office>
  <office office_id="KYG048">Knox Co KCEOC WIA</office>
  <office office_id="KYKP01">Ky Farmworker Program E&amp;T Bowling Green</office>
  <office office_id="KYHP01">Ky Farmworker Program E&amp;T Columbia</office>
  <office office_id="KYAP01">Ky Farmworker Program E&amp;T Hopkinsville</office>
  <office office_id="KYHP03">Ky Farmworker Program E&amp;T Jamestown</office>
  <office office_id="KYAP02">Ky Farmworker Program E&amp;T Mayfield</office>
  <office office_id="KYHP02">Ky Farmworker Program E&amp;T Somerset</office>
  <office office_id="KYI018">Ky Indian Manpower Program, Lexington</office>
  <office office_id="KYC012">Ky Indian Manpower Program, Louisville</office>
  <office office_id="KYBBBB">LINCOLN TRAIL OFFICE</office>
  <office office_id="KYCCCC">LOUISVILLE OFFICE</office>
  <office office_id="KYH001">Lake Cumberland ADD WIA</office>
  <office office_id="KYH027">Laurel Co Adult Ed &amp; Literacy</office>
  <office office_id="KYG072">Lawrence Co Northeast WIA</office>
  <office office_id="KY8801">Lebanon Career Ctr OET</office>
  <office office_id="KY7503">Leitchfield Career Ctr OET</office>
  <office office_id="KYG057">Leslie Co LKLP WIA</office>
  <office office_id="KYG058">Letcher Co LKLP WIA</office>
  <office office_id="KY0060">Lexington Central Ky Job Ctr OET</office>
  <office office_id="KYI019">Lexington OFB</office>
  <office office_id="KYI017">Lexington OVR</office>
  <office office_id="KYI024">Lexington Public Library</office>
  <office office_id="KY6101">Liberty OET</office>
  <office office_id="KYB001">Lincoln Trail ADD WIA</office>
  <office office_id="KYB003">Lincoln Trail Career Ctr Bardstown WIA</office>
  <office office_id="KYB002">Lincoln Trail Career Ctr Etown WIA</office>
  <office office_id="KYTRLE">Lincoln Trail Career Ctr Leitchfield</office>
  <office office_id="KYB004">Lincoln Trail Career Ctr Leitchfield WIA</office>
  <office office_id="KYK014">Logan County BOE AE</office>
  <office office_id="KY6401">London OET</office>
  <office office_id="KYH012">London State Office Building  WIA</office>
  <office office_id="KY2356">Louisville Cathedral Assumption OET</office>
  <office office_id="KYC009">Louisville Cedar OVR</office>
  <office office_id="KY1056">Louisville Cedar St OET</office>
  <office office_id="KYC007">Louisville Dixie Hwy OVR</office>
  <office office_id="KYC026">Louisville Free Public Library</office>
  <office office_id="KYC022">Louisville JCTC JFVS WIA</office>
  <office office_id="KYC023">Louisville JCTC Nia Ctr WIA</office>
  <office office_id="KY3256">Louisville JFVS OET</office>
  <office office_id="KY1956">Louisville Job Bank OET</office>
  <office office_id="KYC010">Louisville Juneau OVR</office>
  <office office_id="KY2256">Louisville NIA Center OET</office>
  <office office_id="KYC008">Louisville Nia Ctr OVR</office>
  <office office_id="KYC013">Louisville OFB</office>
  <office office_id="KY1356">Louisville Preston Hwy OET</office>
  <office office_id="KY1456">Louisville Riverport OET</office>
  <office office_id="KY2156">Louisville ST Johns Ctr OET</office>
  <office office_id="KY2456">Louisville VA Dupont Clinic OET</office>
  <office office_id="KY2556">Louisville VA Medical Ctr OET</office>
  <office office_id="KY4356">Louisville VOA OET</office>
  <office office_id="KY2656">Louisville Veterans Ctr OET</office>
  <office office_id="KYA013">Lyon Co DCBS Office</office>
  <office office_id="KY7202">Madisonville JobNet Career Ctr OET</office>
  <office office_id="KY0072">Madisonville OET</office>
  <office office_id="KYA018">Madisonville OVR</office>
  <office office_id="KYG059">Magoffin Co Big Sandy CAP WIA</office>
  <office office_id="KY0086">Manchester JobSight OET</office>
  <office office_id="KYG060">Martin Co Big Sandy CAP WIA</office>
  <office office_id="KY0051">Mayfield Career Ctr OET</office>
  <office office_id="KYA010">Mayfield Career Ctr OVR</office>
  <office office_id="KYA003">Mayfield Career Ctr WIA</office>
  <office office_id="KYF012">Maysville Comm &amp; Tech College</office>
  <office office_id="KY0062">Maysville OS Govt Ctr OET</office>
  <office office_id="KYF009">Maysville OVR</office>
  <office office_id="KYF004">Maysville One Stop WIA</office>
  <office office_id="KYA021">McCracken County Public Library</office>
  <office office_id="KYH025">McCreary Co Adult Education</office>
  <office office_id="KYH038">McCreary County OET</office>
  <office office_id="KYH003">McCreary County WIA Office</office>
  <office office_id="KY6402">McKee OET</office>
  <office office_id="KYJ015">McLean Co AE</office>
  <office office_id="KYJ006">McLean Co Career Ctr WIA</office>
  <office office_id="KYG061">Menifee Co Gateway WIA</office>
  <office office_id="KYK015">Metcalfe County BOE AE</office>
  <office office_id="KY0074">Middlesboro OET</office>
  <office office_id="KYK009">Monroe County BOE AE</office>
  <office office_id="KY6108">Monticello OET</office>
  <office office_id="KY0067">Morehead Career Ctr OET</office>
  <office office_id="KYF007">Morehead Gateway One Stop Ctr  WIA</office>
  <office office_id="KYF008">Morehead OVR</office>
  <office office_id="KYG062">Morgan Co Gateway WIA</office>
  <office office_id="KYF006">Mount Sterling WIA</office>
  <office office_id="KY0083">Mt. Sterling OET</office>
  <office office_id="KY7203">Muhlenberg Career Advancement Ctr</office>
  <office office_id="KY0098">Murray Career Ctr OET</office>
  <office office_id="KYE003">Northern Kentucky ADD WIA</office>
  <office office_id="KYJ016">Ohio Co AE</office>
  <office office_id="KYJ002">Ohio Co Career Ctr WIA</office>
  <office office_id="KYE011" inactive="true">Owen Co CFC</office>
  <office office_id="KYJ011">Owensboro Area Career Ctr OFB</office>
  <office office_id="KYJ009">Owensboro Area Career Ctr OVR</office>
  <office office_id="KYJ001">Owensboro Area Career Ctr WIA</office>
  <office office_id="KYJ012">Owensboro CTC AE</office>
  <office office_id="KY0054">Owensboro OS Career Ctr OET</office>
  <office office_id="KY0050">Paducah Career Ctr OET</office>
  <office office_id="KYA017">Paducah Career Ctr OVR</office>
  <office office_id="KYA002">Paducah Career Ctr WIA</office>
  <office office_id="KYE010" inactive="true">Pendleton Co CFC</office>
  <office office_id="KYA005">Pennyrile ADD WIA</office>
  <office office_id="KYG012">Perry Co JobSight CEOC</office>
  <office office_id="KYG046">Perry Co JobSight EKCCC</office>
  <office office_id="KYG007">Perry Co JobSight Experience Works</office>
  <office office_id="KYG002">Perry Co JobSight Hazard CTC</office>
  <office office_id="KYJCPC">Perry Co JobSight Job Corps</office>
  <office office_id="KYG008">Perry Co JobSight KRADD</office>
  <office office_id="KYAEPC">Perry Co JobSight KVEC</office>
  <office office_id="KYG018">Perry Co JobSight LKLP Adult Day Care</office>
  <office office_id="KYG026">Perry Co JobSight LKLP CSBG</office>
  <office office_id="KYG019">Perry Co JobSight LKLP Early Headstart</office>
  <office office_id="KYG020">Perry Co JobSight LKLP Headstart</office>
  <office office_id="KYG035">Perry Co JobSight LKLP Housing</office>
  <office office_id="KYG036">Perry Co JobSight LKLP Tax Preparation</office>
  <office office_id="KYG034">Perry Co JobSight LKLP Transportation</office>
  <office office_id="KYG022">Perry Co JobSight LKLP Victims of Crime</office>
  <office office_id="KYG005">Perry Co JobSight OFB</office>
  <office office_id="KYG043">Perry Co JobSight OVR</office>
  <office office_id="KYG028">Perry Co JobSight WIA</office>
  <office office_id="KYG021">Perry JobSight LKLP Compassion Capital</office>
  <office office_id="KYG023">Perry JobSight LKLP Self-Help Parenting</office>
  <office office_id="KYG040">Perry JobSight LKLP Spouse Abuse Center</office>
  <office office_id="KYG039">Perry JobSight LKLP Sr Citizen Nutrition</office>
  <office office_id="KYG075">Pike Co Big Sandy CAP WIA</office>
  <office office_id="KYG009">Pike Co JobSight Big Sandy CAP</office>
  <office office_id="KYG027">Pike Co JobSight Big Sandy CSBG</office>
  <office office_id="KYKCPK">Pike Co JobSight Big Sandy CTC</office>
  <office office_id="KYG037">Pike Co JobSight Big Sandy Tax Prep</office>
  <office office_id="KYAEPK">Pike Co JobSight Board of Ed</office>
  <office office_id="KYG013">Pike Co JobSight CEOC</office>
  <office office_id="KYJCPK">Pike Co JobSight Job Corps</office>
  <office office_id="KYG006">Pike Co JobSight OFB</office>
  <office office_id="KYG044">Pike Co JobSight OVR</office>
  <office office_id="KYGO50">Pike Co JobSight WIA</office>
  <office office_id="KY7101">Pike County JobSight OET</office>
  <office office_id="KY0071">Pikeville OET</office>
  <office office_id="KYG068">Pikeville OVR</office>
  <office office_id="KYH032" inactive="true">Pine Knot Job Corps</office>
  <office office_id="KY0070">Prestonsburg OET</office>
  <office office_id="KYG065">Prestonsburg OVR</office>
  <office office_id="KYH031">Pulaski Adult Learning Center</office>
  <office office_id="KYH037">Pulaski County Public Library</office>
  <office office_id="KYA001">Purchase ADD WIA</office>
  <office office_id="KYI013">Richmond Central KY Career Ctr OVR</office>
  <office office_id="KYI021">Richmond Central Kentucky Job Ctr OFB</office>
  <office office_id="KY0082">Richmond OS Career Ctr OET</office>
  <office office_id="KYH036">Rockcastle AE WIA - Brodhead</office>
  <office office_id="KYH013">Rockcastle Adult Learning Ctr WIA</office>
  <office office_id="KYH026">Russell Co Adult Education</office>
  <office office_id="KYH016">Russell Co Learning Ctr WIA</office>
  <office office_id="KYH035">Russell Co OFB</office>
  <office office_id="KY6104">Russell Springs OET</office>
  <office office_id="KY2956">Salvation Army Center of Hope OET</office>
  <office office_id="KYI008" inactive="true">Senior Citizens'' Ctr WIA</office>
  <office office_id="KY1856">Shelbyville OET</office>
  <office office_id="KY1656">Shepherdsville OET</office>
  <office office_id="KYK016">Simpson County BOE AE</office>
  <office office_id="KYAESO" inactive="true">Somerset Career Ctr Adult Education</office>
  <office office_id="KYCASO">Somerset Career Ctr Community Action</office>
  <office office_id="KYEWSO">Somerset Career Ctr Experience Works</office>
  <office office_id="KYH002">Somerset Career Ctr WIA</office>
  <office office_id="KYH018">Somerset OFB</office>
  <office office_id="KY0061">Somerset OS Career Ctr OET</office>
  <office office_id="KYH017">Somerset OVR</office>
  <office office_id="KY8802">Springfield OET</office>
  <office office_id="KY3056">St. Vincent DePaul OET</office>
  <office office_id="KYFFFF">TENCO OFFICE</office>
  <office office_id="KY2856">Taylorsville OET</office>
  <office office_id="KYC006">Taylorsville OVR</office>
  <office office_id="KYA015">Todd Co DCBS Office</office>
  <office office_id="KY7302">Tompkinsville OET</office>
  <office office_id="KYH009">Tradeway Center WIA</office>
  <office office_id="KYA012">Trigg Co DCBS Office</office>
  <office office_id="KYC025">Trimble County Public Library</office>
  <office office_id="KYJ018">Union Co AE</office>
  <office office_id="KYJ007">Union Co Career Ctr WIA</office>
  <office office_id="KYB007">VA Healthcare Ctr OET</office>
  <office office_id="KYK007">Warren County Public Library</office>
  <office office_id="KYH029">Wayne Co Adult Education</office>
  <office office_id="KY3356">Wayside Christian Mission OET</office>
  <office office_id="KYJ017">Webster Co AE</office>
  <office office_id="KYG069">West Liberty OVR</office>
  <office office_id="KY0077">Whitesburg OET</office>
  <office office_id="KYG064">Whitesburg OVR</office>
  <office office_id="KY6105">Whitley City OET</office>
  <office office_id="KYH019">Whitley Co Adult Learning Ctr</office>
  <office office_id="KYH014">Williamsburg WIA</office>
  <office office_id="KY0063">Winchester OET</office>
  <office office_id="KYC011">Youth Opportunities Unlimited WIA</office>
  <office office_id="KYG)43" inactive="true">ZZ-Do Not UsePerry Co JobSight OVR</office>
  <office office_id="KY0056" inactive="true">zz0056 DO NOT USE - should be 1056</office>
  <office office_id="KY0058" inactive="true">zz0058 Nicholasville OET</office>
  <office office_id="KY1556" inactive="true">zz1556 Louisville KY Works OET</office>
  <office office_id="KY2056" inactive="true">zz2056 Louisville New Castle OET</office>
  <office office_id="KY2756" inactive="true">zz2756 Bedford OET</office>
  <office office_id="KY5101" inactive="true">zz5101 Mayfield Workforce Transition Ctr</office>
  <office office_id="KY5102" inactive="true">zz5102 Hickman OET</office>
  <office office_id="KY5103" inactive="true">zz5103 Fulton OET</office>
  <office office_id="KY5104" inactive="true">zz5104 Clinton OET</office>
  <office office_id="KY5301" inactive="true">zz5301 Dixon OET</office>
  <office office_id="KY5302" inactive="true">zz5302 Morganfield Career Ctr OET</office>
  <office office_id="KY5303" inactive="true">zz5303 Henderson Career Connection Ctr</office>
  <office office_id="KY5401" inactive="true">zz5401 Hawesville Career Ctr OET</office>
  <office office_id="KY5402" inactive="true">zz5402 Calhoun OET</office>
  <office office_id="KY5403" inactive="true">zz5403 Hartford Career Ctr OET</office>
  <office office_id="KY5501" inactive="true">zz5501 Scottsville OET</office>
  <office office_id="KY5502" inactive="true">zz5502 Russellville OS Career Ctr OET</office>
  <office office_id="KY6103" inactive="true">zz6103 Burkesville OET</office>
  <office office_id="KY6106" inactive="true">zz6106 Jamestown OET</office>
  <office office_id="KY6201" inactive="true">zz6201 Flemingsburg OET</office>
  <office office_id="KY6501" inactive="true">zz6501 Stanford OET</office>
  <office office_id="KY6801" inactive="true">zz6801 Hindman OET</office>
  <office office_id="KY6803" inactive="true">zz6803 Booneville OET</office>
  <office office_id="KY6804" inactive="true">zz6804 Campton OET</office>
  <office office_id="KY6805" inactive="true">zz6805 Beattyville OET</office>
  <office office_id="KY6901" inactive="true">zz6901 Ashland OET</office>
  <office office_id="KY6902" inactive="true">zz6902 Greenup OET</office>
  <office office_id="KY6904" inactive="true">zz6904 Sandy Hook OET</office>
  <office office_id="KY6906" inactive="true">zz6906 Louisa OET</office>
  <office office_id="KY7001" inactive="true">zz7001 Paintsville OET</office>
  <office office_id="KY7002" inactive="true">zz7002 Inez OET</office>
  <office office_id="KY7003" inactive="true">zz7003 Salyersville OET</office>
  <office office_id="KY7201" inactive="true">zz7201 Greenville OET</office>
  <office office_id="KY7301">zz7301 Munfordville OET</office>
  <office office_id="KY7401" inactive="true">zz7401 Pineville JobSight OET</office>
  <office office_id="KY8501" inactive="true">zz8501 Paris OS Job Ctr OET</office>
  <office office_id="KYG045" inactive="true">zzBell Co JobSight EKCCC</office>
  <office office_id="KYG041" inactive="true">zzBell Co JobSight OVR</office>
  <office office_id="KYSEBC" inactive="true">zzBell Co JobSight Southeast KY CTC</office>
  <office office_id="KYAECC" inactive="true">zzClay Co JobSight Board of Education</office>
  <office office_id="KYG011" inactive="true">zzClay Co JobSight CEOC</office>
  <office office_id="KYG031" inactive="true">zzClay Co JobSight DBDC Compassion Capit</office>
  <office office_id="KYG032" inactive="true">zzClay Co JobSight DBDC Housing</office>
  <office office_id="KYEWCC" inactive="true">zzClay Co JobSight Experience Works</office>
  <office office_id="KYJCCC" inactive="true">zzClay Co JobSight Job Corps</office>
  <office office_id="KYG033" inactive="true">zzClay Co JobSight KCEOC Homeless Progra</office>
  <office office_id="KYE016" inactive="true">zzCovington OVR</office>
  <office office_id="KYTRPK" inactive="true">zzDO NOT USE Pike County JobSight WIA</office>
  <office office_id="KYTRI1" inactive="true">zzDTR Bluegrass WIA</office>
  <office office_id="KYTR00" inactive="true">zzDTR Central Office WIA</office>
  <office office_id="KYIIII" inactive="true">zzDo Not Use - BLUEGRASS OFFICE</office>
  <office office_id="KY5503" inactive="true">zzFranklin OET</office>
  <office office_id="KYJJJJ" inactive="true">zzGREEN RIVER OFFICE</office>
  <office office_id="KYG067" inactive="true">zzHarlan OVR</office>
  <office office_id="KYKCHE" inactive="true">zzHenderson Career Connections Ctr KCTCS</office>
  <office office_id="KYI007" inactive="true">zzHope Ctr WIA</office>
  <office office_id="KY1156" inactive="true">zzInterlink Counseling Services OET</office>
  <office office_id="KY4056" inactive="true">zzJCPS-Youth Opportunities Unlimited WIA</office>
  <office office_id="KYC016" inactive="true">zzJCTC Crew Ctr WIA</office>
  <office office_id="KY3756" inactive="true">zzJCTC-Bullitt Co WIA</office>
  <office office_id="KY3456" inactive="true">zzJCTC-Cedar WIA</office>
  <office office_id="KY3556" inactive="true">zzJCTC-Crew Ctr WIA</office>
  <office office_id="KY3856" inactive="true">zzJCTC-Riverport WIA</office>
  <office office_id="KY3956" inactive="true">zzJCTC-Shelby Co Campus WIA</office>
  <office office_id="KY3656" inactive="true">zzJCTC-Southwest WIA</office>
  <office office_id="KY1756" inactive="true">zzLa Grange OET</office>
  <office office_id="KY5901" inactive="true">zzLawrenceburg Central Ky Job Ctr OET</office>
  <office office_id="KY4156" inactive="true">zzLouisville JCTC JFVS WIA</office>
  <office office_id="KY4256" inactive="true">zzLouisville JCTC Nia Ctr WIA</office>
  <office office_id="KYG066" inactive="true">zzMiddlesboro OVR</office>
  <office office_id="KYEEEE" inactive="true">zzNORTHERN KENTUCKY OFFICE</office>
  <office office_id="KYAAAA" inactive="true">zzPURCHASE /PENNYRILE OFFICE</office>
  <office office_id="KYH028" inactive="true">zzRockcastle Co Adult Learning Ctr</office>
  <office office_id="KYG073" inactive="true">zzThelma OVR</office>
  <office office_id="KYA009" inactive="true">zzWorkforce Transition Ctr WIA</office>
</office_list>
'

INSERT INTO @Offices
(
	OfficeName,
	InActive,
	IsDefault,
	ExternalId
)
SELECT
	LEFT(T.C.value('text()[1]', 'NVARCHAR(100)'), 20) AS OfficeName,
	CASE T.C.value('@inactive', 'NVARCHAR(5)') WHEN 'true' THEN 1 ELSE 0 END AS Inactive,
	CASE T.C.value('@isdefault', 'NVARCHAR(5)') WHEN 'true' THEN 1 ELSE 0 END AS IsDefault,
	T.C.value('@office_id', 'NVARCHAR(100)') AS ExternalId
FROM
	@Xml.nodes('/office_list/office') T(C)

-- isdefault
SET @Xml = '
<zip_office>
<zip code="24202" office_id="KY0071" status="1"/>
<zip code="24202" office_id="KYGGGG" status="0"/>
<zip code="24210" office_id="KY0071" status="1"/>
<zip code="24210" office_id="KYGGGG" status="0"/>
<zip code="24211" office_id="KY0071" status="1"/>
<zip code="24211" office_id="KYGGGG" status="0"/>
<zip code="24212" office_id="KY0071" status="1"/>
<zip code="24212" office_id="KYGGGG" status="0"/>
<zip code="24215" office_id="KY0071" status="1"/>
<zip code="24215" office_id="KYGGGG" status="0"/>
<zip code="24216" office_id="KY0071" status="1"/>
<zip code="24216" office_id="KYGGGG" status="0"/>
<zip code="24217" office_id="KY0071" status="1"/>
<zip code="24217" office_id="KYGGGG" status="0"/>
<zip code="24218" office_id="KY0074" status="1"/>
<zip code="24218" office_id="KYGGGG" status="0"/>
<zip code="24219" office_id="KY0071" status="1"/>
<zip code="24219" office_id="KYGGGG" status="0"/>
<zip code="24220" office_id="KY0071" status="1"/>
<zip code="24220" office_id="KYGGGG" status="0"/>
<zip code="24221" office_id="KY0074" status="1"/>
<zip code="24221" office_id="KYGGGG" status="0"/>
<zip code="24224" office_id="KY0071" status="1"/>
<zip code="24224" office_id="KYGGGG" status="0"/>
<zip code="24225" office_id="KY0071" status="1"/>
<zip code="24225" office_id="KYGGGG" status="0"/>
<zip code="24226" office_id="KY0071" status="1"/>
<zip code="24226" office_id="KYGGGG" status="0"/>
<zip code="24228" office_id="KY0071" status="1"/>
<zip code="24228" office_id="KYGGGG" status="0"/>
<zip code="24230" office_id="KY0071" status="1"/>
<zip code="24230" office_id="KYGGGG" status="0"/>
<zip code="24236" office_id="KY0071" status="1"/>
<zip code="24236" office_id="KYGGGG" status="0"/>
<zip code="24237" office_id="KY0071" status="1"/>
<zip code="24237" office_id="KYGGGG" status="0"/>
<zip code="24239" office_id="KY0071" status="1"/>
<zip code="24239" office_id="KYGGGG" status="0"/>
<zip code="24243" office_id="KY0074" status="1"/>
<zip code="24243" office_id="KYGGGG" status="0"/>
<zip code="24244" office_id="KY0071" status="1"/>
<zip code="24244" office_id="KYGGGG" status="0"/>
<zip code="24245" office_id="KY0071" status="1"/>
<zip code="24245" office_id="KYGGGG" status="0"/>
<zip code="24246" office_id="KY0071" status="1"/>
<zip code="24246" office_id="KYGGGG" status="0"/>
<zip code="24248" office_id="KY0074" status="1"/>
<zip code="24248" office_id="KYGGGG" status="0"/>
<zip code="24250" office_id="KY0071" status="1"/>
<zip code="24250" office_id="KYGGGG" status="0"/>
<zip code="24251" office_id="KY0071" status="1"/>
<zip code="24251" office_id="KYGGGG" status="0"/>
<zip code="24256" office_id="KY0071" status="1"/>
<zip code="24256" office_id="KYGGGG" status="0"/>
<zip code="24258" office_id="KY0071" status="1"/>
<zip code="24258" office_id="KYGGGG" status="0"/>
<zip code="24260" office_id="KY0071" status="1"/>
<zip code="24260" office_id="KYGGGG" status="0"/>
<zip code="24263" office_id="KY0074" status="1"/>
<zip code="24263" office_id="KYGGGG" status="0"/>
<zip code="24265" office_id="KY0074" status="1"/>
<zip code="24265" office_id="KYGGGG" status="0"/>
<zip code="24266" office_id="KY0071" status="1"/>
<zip code="24266" office_id="KYGGGG" status="0"/>
<zip code="24269" office_id="KY0071" status="1"/>
<zip code="24269" office_id="KYGGGG" status="0"/>
<zip code="24270" office_id="KY0071" status="1"/>
<zip code="24270" office_id="KYGGGG" status="0"/>
<zip code="24271" office_id="KY0071" status="1"/>
<zip code="24271" office_id="KYGGGG" status="0"/>
<zip code="24272" office_id="KY0071" status="1"/>
<zip code="24272" office_id="KYGGGG" status="0"/>
<zip code="24277" office_id="KY0074" status="1"/>
<zip code="24277" office_id="KYGGGG" status="0"/>
<zip code="24279" office_id="KY0071" status="1"/>
<zip code="24279" office_id="KYGGGG" status="0"/>
<zip code="24280" office_id="KY0071" status="1"/>
<zip code="24280" office_id="KYGGGG" status="0"/>
<zip code="24281" office_id="KY0074" status="1"/>
<zip code="24281" office_id="KYGGGG" status="0"/>
<zip code="24282" office_id="KY0074" status="1"/>
<zip code="24282" office_id="KYGGGG" status="0"/>
<zip code="24283" office_id="KY0071" status="1"/>
<zip code="24283" office_id="KYGGGG" status="0"/>
<zip code="24285" office_id="KY0071" status="1"/>
<zip code="24285" office_id="KYGGGG" status="0"/>
<zip code="24289" office_id="KY0071" status="1"/>
<zip code="24289" office_id="KYGGGG" status="0"/>
<zip code="24290" office_id="KY0071" status="1"/>
<zip code="24290" office_id="KYGGGG" status="0"/>
<zip code="24293" office_id="KY0071" status="1"/>
<zip code="24293" office_id="KYGGGG" status="0"/>
<zip code="24311" office_id="KY0071" status="1"/>
<zip code="24311" office_id="KYGGGG" status="0"/>
<zip code="24316" office_id="KY0071" status="1"/>
<zip code="24316" office_id="KYGGGG" status="0"/>
<zip code="24319" office_id="KY0071" status="1"/>
<zip code="24319" office_id="KYGGGG" status="0"/>
<zip code="24327" office_id="KY0071" status="1"/>
<zip code="24327" office_id="KYGGGG" status="0"/>
<zip code="24340" office_id="KY0071" status="1"/>
<zip code="24340" office_id="KYGGGG" status="0"/>
<zip code="24354" office_id="KY0071" status="1"/>
<zip code="24354" office_id="KYGGGG" status="0"/>
<zip code="24361" office_id="KY0071" status="1"/>
<zip code="24361" office_id="KYGGGG" status="0"/>
<zip code="24370" office_id="KY0071" status="1"/>
<zip code="24370" office_id="KYGGGG" status="0"/>
<zip code="24373" office_id="KY0071" status="1"/>
<zip code="24373" office_id="KYGGGG" status="0"/>
<zip code="24375" office_id="KY0071" status="1"/>
<zip code="24375" office_id="KYGGGG" status="0"/>
<zip code="24377" office_id="KY0071" status="1"/>
<zip code="24377" office_id="KYGGGG" status="0"/>
<zip code="24601" office_id="KY0071" status="1"/>
<zip code="24601" office_id="KYGGGG" status="0"/>
<zip code="24602" office_id="KY0071" status="1"/>
<zip code="24602" office_id="KYGGGG" status="0"/>
<zip code="24603" office_id="KY0071" status="1"/>
<zip code="24603" office_id="KYGGGG" status="0"/>
<zip code="24605" office_id="KY0071" status="1"/>
<zip code="24605" office_id="KYGGGG" status="0"/>
<zip code="24606" office_id="KY0071" status="1"/>
<zip code="24606" office_id="KYGGGG" status="0"/>
<zip code="24607" office_id="KY0071" status="1"/>
<zip code="24607" office_id="KYGGGG" status="0"/>
<zip code="24608" office_id="KY0071" status="1"/>
<zip code="24608" office_id="KYGGGG" status="0"/>
<zip code="24609" office_id="KY0071" status="1"/>
<zip code="24609" office_id="KYGGGG" status="0"/>
<zip code="24612" office_id="KY0071" status="1"/>
<zip code="24612" office_id="KYGGGG" status="0"/>
<zip code="24613" office_id="KY0071" status="1"/>
<zip code="24613" office_id="KYGGGG" status="0"/>
<zip code="24614" office_id="KY0071" status="1"/>
<zip code="24614" office_id="KYGGGG" status="0"/>
<zip code="24618" office_id="KY0071" status="1"/>
<zip code="24618" office_id="KYGGGG" status="0"/>
<zip code="24620" office_id="KY0071" status="1"/>
<zip code="24620" office_id="KYGGGG" status="0"/>
<zip code="24622" office_id="KY0071" status="1"/>
<zip code="24622" office_id="KYGGGG" status="0"/>
<zip code="24624" office_id="KY0071" status="1"/>
<zip code="24624" office_id="KYGGGG" status="0"/>
<zip code="24627" office_id="KY0071" status="1"/>
<zip code="24627" office_id="KYGGGG" status="0"/>
<zip code="24628" office_id="KY0071" status="1"/>
<zip code="24628" office_id="KYGGGG" status="0"/>
<zip code="24630" office_id="KY0071" status="1"/>
<zip code="24630" office_id="KYGGGG" status="0"/>
<zip code="24631" office_id="KY0071" status="1"/>
<zip code="24631" office_id="KYGGGG" status="0"/>
<zip code="24634" office_id="KY0071" status="1"/>
<zip code="24634" office_id="KYGGGG" status="0"/>
<zip code="24635" office_id="KY0071" status="1"/>
<zip code="24635" office_id="KYGGGG" status="0"/>
<zip code="24637" office_id="KY0071" status="1"/>
<zip code="24637" office_id="KYGGGG" status="0"/>
<zip code="24639" office_id="KY0071" status="1"/>
<zip code="24639" office_id="KYGGGG" status="0"/>
<zip code="24640" office_id="KY0071" status="1"/>
<zip code="24640" office_id="KYGGGG" status="0"/>
<zip code="24641" office_id="KY0071" status="1"/>
<zip code="24641" office_id="KYGGGG" status="0"/>
<zip code="24646" office_id="KY0071" status="1"/>
<zip code="24646" office_id="KYGGGG" status="0"/>
<zip code="24647" office_id="KY0071" status="1"/>
<zip code="24647" office_id="KYGGGG" status="0"/>
<zip code="24649" office_id="KY0071" status="1"/>
<zip code="24649" office_id="KYGGGG" status="0"/>
<zip code="24651" office_id="KY0071" status="1"/>
<zip code="24651" office_id="KYGGGG" status="0"/>
<zip code="24656" office_id="KY0071" status="1"/>
<zip code="24656" office_id="KYGGGG" status="0"/>
<zip code="24657" office_id="KY0071" status="1"/>
<zip code="24657" office_id="KYGGGG" status="0"/>
<zip code="24658" office_id="KY0071" status="1"/>
<zip code="24658" office_id="KYGGGG" status="0"/>
<zip code="24701" office_id="KY0071" status="1"/>
<zip code="24701" office_id="KYGGGG" status="0"/>
<zip code="24712" office_id="KY0071" status="1"/>
<zip code="24712" office_id="KYGGGG" status="0"/>
<zip code="24714" office_id="KY0071" status="1"/>
<zip code="24714" office_id="KYGGGG" status="0"/>
<zip code="24715" office_id="KY0071" status="1"/>
<zip code="24715" office_id="KYGGGG" status="0"/>
<zip code="24716" office_id="KY0071" status="1"/>
<zip code="24716" office_id="KYGGGG" status="0"/>
<zip code="24719" office_id="KY0071" status="1"/>
<zip code="24719" office_id="KYGGGG" status="0"/>
<zip code="24724" office_id="KY0071" status="1"/>
<zip code="24724" office_id="KYGGGG" status="0"/>
<zip code="24726" office_id="KY0071" status="1"/>
<zip code="24726" office_id="KYGGGG" status="0"/>
<zip code="24729" office_id="KY0071" status="1"/>
<zip code="24729" office_id="KYGGGG" status="0"/>
<zip code="24731" office_id="KY0071" status="1"/>
<zip code="24731" office_id="KYGGGG" status="0"/>
<zip code="24732" office_id="KY0071" status="1"/>
<zip code="24732" office_id="KYGGGG" status="0"/>
<zip code="24733" office_id="KY0071" status="1"/>
<zip code="24733" office_id="KYGGGG" status="0"/>
<zip code="24736" office_id="KY0071" status="1"/>
<zip code="24736" office_id="KYGGGG" status="0"/>
<zip code="24737" office_id="KY0071" status="1"/>
<zip code="24737" office_id="KYGGGG" status="0"/>
<zip code="24738" office_id="KY0071" status="1"/>
<zip code="24738" office_id="KYGGGG" status="0"/>
<zip code="24739" office_id="KY0071" status="1"/>
<zip code="24739" office_id="KYGGGG" status="0"/>
<zip code="24740" office_id="KY0071" status="1"/>
<zip code="24740" office_id="KYGGGG" status="0"/>
<zip code="24747" office_id="KY0071" status="1"/>
<zip code="24747" office_id="KYGGGG" status="0"/>
<zip code="24751" office_id="KY0071" status="1"/>
<zip code="24751" office_id="KYGGGG" status="0"/>
<zip code="24818" office_id="KY0071" status="1"/>
<zip code="24818" office_id="KYGGGG" status="0"/>
<zip code="24822" office_id="KY0071" status="1"/>
<zip code="24822" office_id="KYGGGG" status="0"/>
<zip code="24823" office_id="KY0071" status="1"/>
<zip code="24823" office_id="KYGGGG" status="0"/>
<zip code="24827" office_id="KY0071" status="1"/>
<zip code="24827" office_id="KYGGGG" status="0"/>
<zip code="24834" office_id="KY0071" status="1"/>
<zip code="24834" office_id="KYGGGG" status="0"/>
<zip code="24839" office_id="KY0071" status="1"/>
<zip code="24839" office_id="KYGGGG" status="0"/>
<zip code="24845" office_id="KY0071" status="1"/>
<zip code="24845" office_id="KYGGGG" status="0"/>
<zip code="24847" office_id="KY0071" status="1"/>
<zip code="24847" office_id="KYGGGG" status="0"/>
<zip code="24849" office_id="KY0071" status="1"/>
<zip code="24849" office_id="KYGGGG" status="0"/>
<zip code="24851" office_id="KY0071" status="1"/>
<zip code="24851" office_id="KYGGGG" status="0"/>
<zip code="24854" office_id="KY0071" status="1"/>
<zip code="24854" office_id="KYGGGG" status="0"/>
<zip code="24857" office_id="KY0071" status="1"/>
<zip code="24857" office_id="KYGGGG" status="0"/>
<zip code="24859" office_id="KY0071" status="1"/>
<zip code="24859" office_id="KYGGGG" status="0"/>
<zip code="24860" office_id="KY0071" status="1"/>
<zip code="24860" office_id="KYGGGG" status="0"/>
<zip code="24862" office_id="KY0071" status="1"/>
<zip code="24862" office_id="KYGGGG" status="0"/>
<zip code="24867" office_id="KY0071" status="1"/>
<zip code="24867" office_id="KYGGGG" status="0"/>
<zip code="24869" office_id="KY0071" status="1"/>
<zip code="24869" office_id="KYGGGG" status="0"/>
<zip code="24870" office_id="KY0071" status="1"/>
<zip code="24870" office_id="KYGGGG" status="0"/>
<zip code="24873" office_id="KY0071" status="1"/>
<zip code="24873" office_id="KYGGGG" status="0"/>
<zip code="24874" office_id="KY0071" status="1"/>
<zip code="24874" office_id="KYGGGG" status="0"/>
<zip code="24880" office_id="KY0071" status="1"/>
<zip code="24880" office_id="KYGGGG" status="0"/>
<zip code="24882" office_id="KY0071" status="1"/>
<zip code="24882" office_id="KYGGGG" status="0"/>
<zip code="24896" office_id="KY0071" status="1"/>
<zip code="24896" office_id="KYGGGG" status="0"/>
<zip code="24898" office_id="KY0071" status="1"/>
<zip code="24898" office_id="KYGGGG" status="0"/>
<zip code="25003" office_id="KY0069" status="1"/>
<zip code="25003" office_id="KYFFFF" status="0"/>
<zip code="25004" office_id="KY0071" status="1"/>
<zip code="25004" office_id="KYGGGG" status="0"/>
<zip code="25007" office_id="KY0071" status="1"/>
<zip code="25007" office_id="KYGGGG" status="0"/>
<zip code="25008" office_id="KY0071" status="1"/>
<zip code="25008" office_id="KYGGGG" status="0"/>
<zip code="25009" office_id="KY0069" status="1"/>
<zip code="25009" office_id="KYFFFF" status="0"/>
<zip code="25010" office_id="KY0069" status="1"/>
<zip code="25010" office_id="KYFFFF" status="0"/>
<zip code="25011" office_id="KY0069" status="1"/>
<zip code="25011" office_id="KYFFFF" status="0"/>
<zip code="25015" office_id="KY0069" status="1"/>
<zip code="25015" office_id="KYFFFF" status="0"/>
<zip code="25021" office_id="KY0069" status="1"/>
<zip code="25021" office_id="KYFFFF" status="0"/>
<zip code="25022" office_id="KY0071" status="1"/>
<zip code="25022" office_id="KYGGGG" status="0"/>
<zip code="25024" office_id="KY0069" status="1"/>
<zip code="25024" office_id="KYFFFF" status="0"/>
<zip code="25025" office_id="KY0069" status="1"/>
<zip code="25025" office_id="KYFFFF" status="0"/>
<zip code="25026" office_id="KY0069" status="1"/>
<zip code="25026" office_id="KYFFFF" status="0"/>
<zip code="25028" office_id="KY0069" status="1"/>
<zip code="25028" office_id="KYFFFF" status="0"/>
<zip code="25033" office_id="KY0069" status="1"/>
<zip code="25033" office_id="KYFFFF" status="0"/>
<zip code="25035" office_id="KY0069" status="1"/>
<zip code="25035" office_id="KYFFFF" status="0"/>
<zip code="25039" office_id="KY0069" status="1"/>
<zip code="25039" office_id="KYFFFF" status="0"/>
<zip code="25044" office_id="KY0071" status="1"/>
<zip code="25044" office_id="KYGGGG" status="0"/>
<zip code="25045" office_id="KY0069" status="1"/>
<zip code="25045" office_id="KYFFFF" status="0"/>
<zip code="25047" office_id="KY0071" status="1"/>
<zip code="25047" office_id="KYGGGG" status="0"/>
<zip code="25048" office_id="KY0071" status="1"/>
<zip code="25048" office_id="KYGGGG" status="0"/>
<zip code="25049" office_id="KY0069" status="1"/>
<zip code="25049" office_id="KYFFFF" status="0"/>
<zip code="25051" office_id="KY0069" status="1"/>
<zip code="25051" office_id="KYFFFF" status="0"/>
<zip code="25053" office_id="KY0069" status="1"/>
<zip code="25053" office_id="KYFFFF" status="0"/>
<zip code="25054" office_id="KY0069" status="1"/>
<zip code="25054" office_id="KYFFFF" status="0"/>
<zip code="25060" office_id="KY0071" status="1"/>
<zip code="25060" office_id="KYGGGG" status="0"/>
<zip code="25061" office_id="KY0069" status="1"/>
<zip code="25061" office_id="KYFFFF" status="0"/>
<zip code="25062" office_id="KY0071" status="1"/>
<zip code="25062" office_id="KYGGGG" status="0"/>
<zip code="25064" office_id="KY0069" status="1"/>
<zip code="25064" office_id="KYFFFF" status="0"/>
<zip code="25067" office_id="KY0069" status="1"/>
<zip code="25067" office_id="KYFFFF" status="0"/>
<zip code="25070" office_id="KY0069" status="1"/>
<zip code="25070" office_id="KYFFFF" status="0"/>
<zip code="25071" office_id="KY0069" status="1"/>
<zip code="25071" office_id="KYFFFF" status="0"/>
<zip code="25075" office_id="KY0069" status="1"/>
<zip code="25075" office_id="KYFFFF" status="0"/>
<zip code="25076" office_id="KY0071" status="1"/>
<zip code="25076" office_id="KYGGGG" status="0"/>
<zip code="25079" office_id="KY0069" status="1"/>
<zip code="25079" office_id="KYFFFF" status="0"/>
<zip code="25081" office_id="KY0069" status="1"/>
<zip code="25081" office_id="KYFFFF" status="0"/>
<zip code="25082" office_id="KY0069" status="1"/>
<zip code="25082" office_id="KYFFFF" status="0"/>
<zip code="25083" office_id="KY0069" status="1"/>
<zip code="25083" office_id="KYFFFF" status="0"/>
<zip code="25086" office_id="KY0069" status="1"/>
<zip code="25086" office_id="KYFFFF" status="0"/>
<zip code="25093" office_id="KY0069" status="1"/>
<zip code="25093" office_id="KYFFFF" status="0"/>
<zip code="25095" office_id="KY0069" status="1"/>
<zip code="25095" office_id="KYFFFF" status="0"/>
<zip code="25102" office_id="KY0069" status="1"/>
<zip code="25102" office_id="KYFFFF" status="0"/>
<zip code="25103" office_id="KY0069" status="1"/>
<zip code="25103" office_id="KYFFFF" status="0"/>
<zip code="25106" office_id="KY0069" status="1"/>
<zip code="25106" office_id="KYFFFF" status="0"/>
<zip code="25107" office_id="KY0069" status="1"/>
<zip code="25107" office_id="KYFFFF" status="0"/>
<zip code="25108" office_id="KY0069" status="1"/>
<zip code="25108" office_id="KYFFFF" status="0"/>
<zip code="25109" office_id="KY0069" status="1"/>
<zip code="25109" office_id="KYFFFF" status="0"/>
<zip code="25110" office_id="KY0069" status="1"/>
<zip code="25110" office_id="KYFFFF" status="0"/>
<zip code="25112" office_id="KY0069" status="1"/>
<zip code="25112" office_id="KYFFFF" status="0"/>
<zip code="25114" office_id="KY0069" status="1"/>
<zip code="25114" office_id="KYFFFF" status="0"/>
<zip code="25121" office_id="KY0071" status="1"/>
<zip code="25121" office_id="KYGGGG" status="0"/>
<zip code="25122" office_id="KY0069" status="1"/>
<zip code="25122" office_id="KYFFFF" status="0"/>
<zip code="25123" office_id="KY0069" status="1"/>
<zip code="25123" office_id="KYFFFF" status="0"/>
<zip code="25124" office_id="KY0069" status="1"/>
<zip code="25124" office_id="KYFFFF" status="0"/>
<zip code="25126" office_id="KY0069" status="1"/>
<zip code="25126" office_id="KYFFFF" status="0"/>
<zip code="25130" office_id="KY0069" status="1"/>
<zip code="25130" office_id="KYFFFF" status="0"/>
<zip code="25132" office_id="KY0069" status="1"/>
<zip code="25132" office_id="KYFFFF" status="0"/>
<zip code="25134" office_id="KY0069" status="1"/>
<zip code="25134" office_id="KYFFFF" status="0"/>
<zip code="25140" office_id="KY0071" status="1"/>
<zip code="25140" office_id="KYGGGG" status="0"/>
<zip code="25142" office_id="KY0069" status="1"/>
<zip code="25142" office_id="KYFFFF" status="0"/>
<zip code="25143" office_id="KY0069" status="1"/>
<zip code="25143" office_id="KYFFFF" status="0"/>
<zip code="25147" office_id="KY0069" status="1"/>
<zip code="25147" office_id="KYFFFF" status="0"/>
<zip code="25148" office_id="KY0069" status="1"/>
<zip code="25148" office_id="KYFFFF" status="0"/>
<zip code="25149" office_id="KY0069" status="1"/>
<zip code="25149" office_id="KYFFFF" status="0"/>
<zip code="25154" office_id="KY0069" status="1"/>
<zip code="25154" office_id="KYFFFF" status="0"/>
<zip code="25156" office_id="KY0069" status="1"/>
<zip code="25156" office_id="KYFFFF" status="0"/>
<zip code="25159" office_id="KY0069" status="1"/>
<zip code="25159" office_id="KYFFFF" status="0"/>
<zip code="25160" office_id="KY0069" status="1"/>
<zip code="25160" office_id="KYFFFF" status="0"/>
<zip code="25162" office_id="KY0069" status="1"/>
<zip code="25162" office_id="KYFFFF" status="0"/>
<zip code="25165" office_id="KY0069" status="1"/>
<zip code="25165" office_id="KYFFFF" status="0"/>
<zip code="25168" office_id="KY0069" status="1"/>
<zip code="25168" office_id="KYFFFF" status="0"/>
<zip code="25169" office_id="KY0069" status="1"/>
<zip code="25169" office_id="KYFFFF" status="0"/>
<zip code="25174" office_id="KY0071" status="1"/>
<zip code="25174" office_id="KYGGGG" status="0"/>
<zip code="25177" office_id="KY0069" status="1"/>
<zip code="25177" office_id="KYFFFF" status="0"/>
<zip code="25180" office_id="KY0071" status="1"/>
<zip code="25180" office_id="KYGGGG" status="0"/>
<zip code="25181" office_id="KY0069" status="1"/>
<zip code="25181" office_id="KYFFFF" status="0"/>
<zip code="25182" office_id="KY0069" status="1"/>
<zip code="25182" office_id="KYFFFF" status="0"/>
<zip code="25183" office_id="KY0071" status="1"/>
<zip code="25183" office_id="KYGGGG" status="0"/>
<zip code="25187" office_id="KY0069" status="1"/>
<zip code="25187" office_id="KYFFFF" status="0"/>
<zip code="25193" office_id="KY0069" status="1"/>
<zip code="25193" office_id="KYFFFF" status="0"/>
<zip code="25201" office_id="KY0069" status="1"/>
<zip code="25201" office_id="KYFFFF" status="0"/>
<zip code="25202" office_id="KY0069" status="1"/>
<zip code="25202" office_id="KYFFFF" status="0"/>
<zip code="25203" office_id="KY0069" status="1"/>
<zip code="25203" office_id="KYFFFF" status="0"/>
<zip code="25204" office_id="KY0069" status="1"/>
<zip code="25204" office_id="KYFFFF" status="0"/>
<zip code="25205" office_id="KY0069" status="1"/>
<zip code="25205" office_id="KYFFFF" status="0"/>
<zip code="25206" office_id="KY0069" status="1"/>
<zip code="25206" office_id="KYFFFF" status="0"/>
<zip code="25208" office_id="KY0069" status="1"/>
<zip code="25208" office_id="KYFFFF" status="0"/>
<zip code="25209" office_id="KY0071" status="1"/>
<zip code="25209" office_id="KYGGGG" status="0"/>
<zip code="25213" office_id="KY0069" status="1"/>
<zip code="25213" office_id="KYFFFF" status="0"/>
<zip code="25214" office_id="KY0069" status="1"/>
<zip code="25214" office_id="KYFFFF" status="0"/>
<zip code="25247" office_id="KY0069" status="1"/>
<zip code="25247" office_id="KYFFFF" status="0"/>
<zip code="25250" office_id="KY0069" status="1"/>
<zip code="25250" office_id="KYFFFF" status="0"/>
<zip code="25253" office_id="KY0069" status="1"/>
<zip code="25253" office_id="KYFFFF" status="0"/>
<zip code="25260" office_id="KY0069" status="1"/>
<zip code="25260" office_id="KYFFFF" status="0"/>
<zip code="25265" office_id="KY0069" status="1"/>
<zip code="25265" office_id="KYFFFF" status="0"/>
<zip code="25287" office_id="KY0069" status="1"/>
<zip code="25287" office_id="KYFFFF" status="0"/>
<zip code="25301" office_id="KY0069" status="1"/>
<zip code="25301" office_id="KYFFFF" status="0"/>
<zip code="25302" office_id="KY0069" status="1"/>
<zip code="25302" office_id="KYFFFF" status="0"/>
<zip code="25303" office_id="KY0069" status="1"/>
<zip code="25303" office_id="KYFFFF" status="0"/>
<zip code="25304" office_id="KY0069" status="1"/>
<zip code="25304" office_id="KYFFFF" status="0"/>
<zip code="25305" office_id="KY0069" status="1"/>
<zip code="25305" office_id="KYFFFF" status="0"/>
<zip code="25306" office_id="KY0069" status="1"/>
<zip code="25306" office_id="KYFFFF" status="0"/>
<zip code="25309" office_id="KY0069" status="1"/>
<zip code="25309" office_id="KYFFFF" status="0"/>
<zip code="25311" office_id="KY0069" status="1"/>
<zip code="25311" office_id="KYFFFF" status="0"/>
<zip code="25312" office_id="KY0069" status="1"/>
<zip code="25312" office_id="KYFFFF" status="0"/>
<zip code="25313" office_id="KY0069" status="1"/>
<zip code="25313" office_id="KYFFFF" status="0"/>
<zip code="25314" office_id="KY0069" status="1"/>
<zip code="25314" office_id="KYFFFF" status="0"/>
<zip code="25315" office_id="KY0069" status="1"/>
<zip code="25315" office_id="KYFFFF" status="0"/>
<zip code="25317" office_id="KY0069" status="1"/>
<zip code="25317" office_id="KYFFFF" status="0"/>
<zip code="25320" office_id="KY0069" status="1"/>
<zip code="25320" office_id="KYFFFF" status="0"/>
<zip code="25321" office_id="KY0069" status="1"/>
<zip code="25321" office_id="KYFFFF" status="0"/>
<zip code="25322" office_id="KY0069" status="1"/>
<zip code="25322" office_id="KYFFFF" status="0"/>
<zip code="25323" office_id="KY0069" status="1"/>
<zip code="25323" office_id="KYFFFF" status="0"/>
<zip code="25324" office_id="KY0069" status="1"/>
<zip code="25324" office_id="KYFFFF" status="0"/>
<zip code="25325" office_id="KY0069" status="1"/>
<zip code="25325" office_id="KYFFFF" status="0"/>
<zip code="25326" office_id="KY0069" status="1"/>
<zip code="25326" office_id="KYFFFF" status="0"/>
<zip code="25327" office_id="KY0069" status="1"/>
<zip code="25327" office_id="KYFFFF" status="0"/>
<zip code="25328" office_id="KY0069" status="1"/>
<zip code="25328" office_id="KYFFFF" status="0"/>
<zip code="25329" office_id="KY0069" status="1"/>
<zip code="25329" office_id="KYFFFF" status="0"/>
<zip code="25330" office_id="KY0069" status="1"/>
<zip code="25330" office_id="KYFFFF" status="0"/>
<zip code="25331" office_id="KY0069" status="1"/>
<zip code="25331" office_id="KYFFFF" status="0"/>
<zip code="25332" office_id="KY0069" status="1"/>
<zip code="25332" office_id="KYFFFF" status="0"/>
<zip code="25333" office_id="KY0069" status="1"/>
<zip code="25333" office_id="KYFFFF" status="0"/>
<zip code="25334" office_id="KY0069" status="1"/>
<zip code="25334" office_id="KYFFFF" status="0"/>
<zip code="25335" office_id="KY0069" status="1"/>
<zip code="25335" office_id="KYFFFF" status="0"/>
<zip code="25336" office_id="KY0069" status="1"/>
<zip code="25336" office_id="KYFFFF" status="0"/>
<zip code="25337" office_id="KY0069" status="1"/>
<zip code="25337" office_id="KYFFFF" status="0"/>
<zip code="25338" office_id="KY0069" status="1"/>
<zip code="25338" office_id="KYFFFF" status="0"/>
<zip code="25339" office_id="KY0069" status="1"/>
<zip code="25339" office_id="KYFFFF" status="0"/>
<zip code="25350" office_id="KY0069" status="1"/>
<zip code="25350" office_id="KYFFFF" status="0"/>
<zip code="25356" office_id="KY0069" status="1"/>
<zip code="25356" office_id="KYFFFF" status="0"/>
<zip code="25357" office_id="KY0069" status="1"/>
<zip code="25357" office_id="KYFFFF" status="0"/>
<zip code="25358" office_id="KY0069" status="1"/>
<zip code="25358" office_id="KYFFFF" status="0"/>
<zip code="25360" office_id="KY0069" status="1"/>
<zip code="25360" office_id="KYFFFF" status="0"/>
<zip code="25361" office_id="KY0069" status="1"/>
<zip code="25361" office_id="KYFFFF" status="0"/>
<zip code="25362" office_id="KY0069" status="1"/>
<zip code="25362" office_id="KYFFFF" status="0"/>
<zip code="25364" office_id="KY0069" status="1"/>
<zip code="25364" office_id="KYFFFF" status="0"/>
<zip code="25365" office_id="KY0069" status="1"/>
<zip code="25365" office_id="KYFFFF" status="0"/>
<zip code="25375" office_id="KY0069" status="1"/>
<zip code="25375" office_id="KYFFFF" status="0"/>
<zip code="25387" office_id="KY0069" status="1"/>
<zip code="25387" office_id="KYFFFF" status="0"/>
<zip code="25389" office_id="KY0069" status="1"/>
<zip code="25389" office_id="KYFFFF" status="0"/>
<zip code="25392" office_id="KY0069" status="1"/>
<zip code="25392" office_id="KYFFFF" status="0"/>
<zip code="25396" office_id="KY0069" status="1"/>
<zip code="25396" office_id="KYFFFF" status="0"/>
<zip code="25501" office_id="KY0069" status="1"/>
<zip code="25501" office_id="KYFFFF" status="0"/>
<zip code="25502" office_id="KY0069" status="1"/>
<zip code="25502" office_id="KYFFFF" status="0"/>
<zip code="25503" office_id="KY0069" status="1"/>
<zip code="25503" office_id="KYFFFF" status="0"/>
<zip code="25504" office_id="KY0069" status="1"/>
<zip code="25504" office_id="KYFFFF" status="0"/>
<zip code="25505" office_id="KY0071" status="1"/>
<zip code="25505" office_id="KYGGGG" status="0"/>
<zip code="25506" office_id="KY0069" status="1"/>
<zip code="25506" office_id="KYFFFF" status="0"/>
<zip code="25507" office_id="KY0069" status="1"/>
<zip code="25507" office_id="KYFFFF" status="0"/>
<zip code="25508" office_id="KY0071" status="1"/>
<zip code="25508" office_id="KYGGGG" status="0"/>
<zip code="25510" office_id="KY0069" status="1"/>
<zip code="25510" office_id="KYFFFF" status="0"/>
<zip code="25511" office_id="KY0069" status="1"/>
<zip code="25511" office_id="KYFFFF" status="0"/>
<zip code="25512" office_id="KY0069" status="1"/>
<zip code="25512" office_id="KYFFFF" status="0"/>
<zip code="25514" office_id="KY0069" status="1"/>
<zip code="25514" office_id="KYFFFF" status="0"/>
<zip code="25515" office_id="KY0069" status="1"/>
<zip code="25515" office_id="KYFFFF" status="0"/>
<zip code="25517" office_id="KY0069" status="1"/>
<zip code="25517" office_id="KYFFFF" status="0"/>
<zip code="25519" office_id="KY0069" status="1"/>
<zip code="25519" office_id="KYFFFF" status="0"/>
<zip code="25520" office_id="KY0069" status="1"/>
<zip code="25520" office_id="KYFFFF" status="0"/>
<zip code="25521" office_id="KY0069" status="1"/>
<zip code="25521" office_id="KYFFFF" status="0"/>
<zip code="25523" office_id="KY0069" status="1"/>
<zip code="25523" office_id="KYFFFF" status="0"/>
<zip code="25524" office_id="KY0069" status="1"/>
<zip code="25524" office_id="KYFFFF" status="0"/>
<zip code="25526" office_id="KY0069" status="1"/>
<zip code="25526" office_id="KYFFFF" status="0"/>
<zip code="25529" office_id="KY0069" status="1"/>
<zip code="25529" office_id="KYFFFF" status="0"/>
<zip code="25530" office_id="KY0069" status="1"/>
<zip code="25530" office_id="KYFFFF" status="0"/>
<zip code="25534" office_id="KY0069" status="1"/>
<zip code="25534" office_id="KYFFFF" status="0"/>
<zip code="25535" office_id="KY0069" status="1"/>
<zip code="25535" office_id="KYFFFF" status="0"/>
<zip code="25537" office_id="KY0069" status="1"/>
<zip code="25537" office_id="KYFFFF" status="0"/>
<zip code="25540" office_id="KY0069" status="1"/>
<zip code="25540" office_id="KYFFFF" status="0"/>
<zip code="25541" office_id="KY0069" status="1"/>
<zip code="25541" office_id="KYFFFF" status="0"/>
<zip code="25544" office_id="KY0069" status="1"/>
<zip code="25544" office_id="KYFFFF" status="0"/>
<zip code="25545" office_id="KY0069" status="1"/>
<zip code="25545" office_id="KYFFFF" status="0"/>
<zip code="25547" office_id="KY0071" status="1"/>
<zip code="25547" office_id="KYGGGG" status="0"/>
<zip code="25550" office_id="KY0069" status="1"/>
<zip code="25550" office_id="KYFFFF" status="0"/>
<zip code="25555" office_id="KY0069" status="1"/>
<zip code="25555" office_id="KYFFFF" status="0"/>
<zip code="25557" office_id="KY0069" status="1"/>
<zip code="25557" office_id="KYFFFF" status="0"/>
<zip code="25559" office_id="KY0069" status="1"/>
<zip code="25559" office_id="KYFFFF" status="0"/>
<zip code="25560" office_id="KY0069" status="1"/>
<zip code="25560" office_id="KYFFFF" status="0"/>
<zip code="25562" office_id="KY0069" status="1"/>
<zip code="25562" office_id="KYFFFF" status="0"/>
<zip code="25564" office_id="KY0069" status="1"/>
<zip code="25564" office_id="KYFFFF" status="0"/>
<zip code="25565" office_id="KY0069" status="1"/>
<zip code="25565" office_id="KYFFFF" status="0"/>
<zip code="25567" office_id="KY0069" status="1"/>
<zip code="25567" office_id="KYFFFF" status="0"/>
<zip code="25569" office_id="KY0069" status="1"/>
<zip code="25569" office_id="KYFFFF" status="0"/>
<zip code="25570" office_id="KY0069" status="1"/>
<zip code="25570" office_id="KYFFFF" status="0"/>
<zip code="25571" office_id="KY0069" status="1"/>
<zip code="25571" office_id="KYFFFF" status="0"/>
<zip code="25572" office_id="KY0069" status="1"/>
<zip code="25572" office_id="KYFFFF" status="0"/>
<zip code="25573" office_id="KY0069" status="1"/>
<zip code="25573" office_id="KYFFFF" status="0"/>
<zip code="25601" office_id="KY0071" status="1"/>
<zip code="25601" office_id="KYGGGG" status="0"/>
<zip code="25606" office_id="KY0071" status="1"/>
<zip code="25606" office_id="KYGGGG" status="0"/>
<zip code="25607" office_id="KY0071" status="1"/>
<zip code="25607" office_id="KYGGGG" status="0"/>
<zip code="25608" office_id="KY0071" status="1"/>
<zip code="25608" office_id="KYGGGG" status="0"/>
<zip code="25611" office_id="KY0071" status="1"/>
<zip code="25611" office_id="KYGGGG" status="0"/>
<zip code="25612" office_id="KY0071" status="1"/>
<zip code="25612" office_id="KYGGGG" status="0"/>
<zip code="25614" office_id="KY0071" status="1"/>
<zip code="25614" office_id="KYGGGG" status="0"/>
<zip code="25617" office_id="KY0071" status="1"/>
<zip code="25617" office_id="KYGGGG" status="0"/>
<zip code="25621" office_id="KY0071" status="1"/>
<zip code="25621" office_id="KYGGGG" status="0"/>
<zip code="25623" office_id="KY0071" status="1"/>
<zip code="25623" office_id="KYGGGG" status="0"/>
<zip code="25624" office_id="KY0071" status="1"/>
<zip code="25624" office_id="KYGGGG" status="0"/>
<zip code="25625" office_id="KY0071" status="1"/>
<zip code="25625" office_id="KYGGGG" status="0"/>
<zip code="25628" office_id="KY0071" status="1"/>
<zip code="25628" office_id="KYGGGG" status="0"/>
<zip code="25630" office_id="KY0071" status="1"/>
<zip code="25630" office_id="KYGGGG" status="0"/>
<zip code="25632" office_id="KY0071" status="1"/>
<zip code="25632" office_id="KYGGGG" status="0"/>
<zip code="25634" office_id="KY0071" status="1"/>
<zip code="25634" office_id="KYGGGG" status="0"/>
<zip code="25635" office_id="KY0071" status="1"/>
<zip code="25635" office_id="KYGGGG" status="0"/>
<zip code="25636" office_id="KY0071" status="1"/>
<zip code="25636" office_id="KYGGGG" status="0"/>
<zip code="25637" office_id="KY0071" status="1"/>
<zip code="25637" office_id="KYGGGG" status="0"/>
<zip code="25638" office_id="KY0071" status="1"/>
<zip code="25638" office_id="KYGGGG" status="0"/>
<zip code="25639" office_id="KY0071" status="1"/>
<zip code="25639" office_id="KYGGGG" status="0"/>
<zip code="25644" office_id="KY0071" status="1"/>
<zip code="25644" office_id="KYGGGG" status="0"/>
<zip code="25645" office_id="KY0071" status="1"/>
<zip code="25645" office_id="KYGGGG" status="0"/>
<zip code="25646" office_id="KY0071" status="1"/>
<zip code="25646" office_id="KYGGGG" status="0"/>
<zip code="25647" office_id="KY0071" status="1"/>
<zip code="25647" office_id="KYGGGG" status="0"/>
<zip code="25649" office_id="KY0071" status="1"/>
<zip code="25649" office_id="KYGGGG" status="0"/>
<zip code="25650" office_id="KY0071" status="1"/>
<zip code="25650" office_id="KYGGGG" status="0"/>
<zip code="25651" office_id="KY0071" status="1"/>
<zip code="25651" office_id="KYGGGG" status="0"/>
<zip code="25652" office_id="KY0071" status="1"/>
<zip code="25652" office_id="KYGGGG" status="0"/>
<zip code="25653" office_id="KY0071" status="1"/>
<zip code="25653" office_id="KYGGGG" status="0"/>
<zip code="25654" office_id="KY0071" status="1"/>
<zip code="25654" office_id="KYGGGG" status="0"/>
<zip code="25661" office_id="KY0071" status="1"/>
<zip code="25661" office_id="KYGGGG" status="0"/>
<zip code="25665" office_id="KY0071" status="1"/>
<zip code="25665" office_id="KYGGGG" status="0"/>
<zip code="25666" office_id="KY0071" status="1"/>
<zip code="25666" office_id="KYGGGG" status="0"/>
<zip code="25667" office_id="KY0071" status="1"/>
<zip code="25667" office_id="KYGGGG" status="0"/>
<zip code="25670" office_id="KY0071" status="1"/>
<zip code="25670" office_id="KYGGGG" status="0"/>
<zip code="25671" office_id="KY0071" status="1"/>
<zip code="25671" office_id="KYGGGG" status="0"/>
<zip code="25672" office_id="KY0071" status="1"/>
<zip code="25672" office_id="KYGGGG" status="0"/>
<zip code="25676" office_id="KY0071" status="1"/>
<zip code="25676" office_id="KYGGGG" status="0"/>
<zip code="25678" office_id="KY0071" status="1"/>
<zip code="25678" office_id="KYGGGG" status="0"/>
<zip code="25682" office_id="KY0071" status="1"/>
<zip code="25682" office_id="KYGGGG" status="0"/>
<zip code="25685" office_id="KY0071" status="1"/>
<zip code="25685" office_id="KYGGGG" status="0"/>
<zip code="25686" office_id="KY0071" status="1"/>
<zip code="25686" office_id="KYGGGG" status="0"/>
<zip code="25688" office_id="KY0071" status="1"/>
<zip code="25688" office_id="KYGGGG" status="0"/>
<zip code="25690" office_id="KY0071" status="1"/>
<zip code="25690" office_id="KYGGGG" status="0"/>
<zip code="25691" office_id="KY0071" status="1"/>
<zip code="25691" office_id="KYGGGG" status="0"/>
<zip code="25692" office_id="KY0071" status="1"/>
<zip code="25692" office_id="KYGGGG" status="0"/>
<zip code="25694" office_id="KY0071" status="1"/>
<zip code="25694" office_id="KYGGGG" status="0"/>
<zip code="25696" office_id="KY0071" status="1"/>
<zip code="25696" office_id="KYGGGG" status="0"/>
<zip code="25697" office_id="KY0071" status="1"/>
<zip code="25697" office_id="KYGGGG" status="0"/>
<zip code="25699" office_id="KY0069" status="1"/>
<zip code="25699" office_id="KYFFFF" status="0"/>
<zip code="25701" office_id="KY0069" status="1"/>
<zip code="25701" office_id="KYFFFF" status="0"/>
<zip code="25702" office_id="KY0069" status="1"/>
<zip code="25702" office_id="KYFFFF" status="0"/>
<zip code="25703" office_id="KY0069" status="1"/>
<zip code="25703" office_id="KYFFFF" status="0"/>
<zip code="25704" office_id="KY0069" status="1"/>
<zip code="25704" office_id="KYFFFF" status="0"/>
<zip code="25705" office_id="KY0069" status="1"/>
<zip code="25705" office_id="KYFFFF" status="0"/>
<zip code="25706" office_id="KY0069" status="1"/>
<zip code="25706" office_id="KYFFFF" status="0"/>
<zip code="25707" office_id="KY0069" status="1"/>
<zip code="25707" office_id="KYFFFF" status="0"/>
<zip code="25708" office_id="KY0069" status="1"/>
<zip code="25708" office_id="KYFFFF" status="0"/>
<zip code="25709" office_id="KY0069" status="1"/>
<zip code="25709" office_id="KYFFFF" status="0"/>
<zip code="25710" office_id="KY0069" status="1"/>
<zip code="25710" office_id="KYFFFF" status="0"/>
<zip code="25711" office_id="KY0069" status="1"/>
<zip code="25711" office_id="KYFFFF" status="0"/>
<zip code="25712" office_id="KY0069" status="1"/>
<zip code="25712" office_id="KYFFFF" status="0"/>
<zip code="25713" office_id="KY0069" status="1"/>
<zip code="25713" office_id="KYFFFF" status="0"/>
<zip code="25714" office_id="KY0069" status="1"/>
<zip code="25714" office_id="KYFFFF" status="0"/>
<zip code="25715" office_id="KY0069" status="1"/>
<zip code="25715" office_id="KYFFFF" status="0"/>
<zip code="25716" office_id="KY0069" status="1"/>
<zip code="25716" office_id="KYFFFF" status="0"/>
<zip code="25717" office_id="KY0069" status="1"/>
<zip code="25717" office_id="KYFFFF" status="0"/>
<zip code="25718" office_id="KY0069" status="1"/>
<zip code="25718" office_id="KYFFFF" status="0"/>
<zip code="25719" office_id="KY0069" status="1"/>
<zip code="25719" office_id="KYFFFF" status="0"/>
<zip code="25720" office_id="KY0069" status="1"/>
<zip code="25720" office_id="KYFFFF" status="0"/>
<zip code="25721" office_id="KY0069" status="1"/>
<zip code="25721" office_id="KYFFFF" status="0"/>
<zip code="25722" office_id="KY0069" status="1"/>
<zip code="25722" office_id="KYFFFF" status="0"/>
<zip code="25723" office_id="KY0069" status="1"/>
<zip code="25723" office_id="KYFFFF" status="0"/>
<zip code="25724" office_id="KY0069" status="1"/>
<zip code="25724" office_id="KYFFFF" status="0"/>
<zip code="25725" office_id="KY0069" status="1"/>
<zip code="25725" office_id="KYFFFF" status="0"/>
<zip code="25726" office_id="KY0069" status="1"/>
<zip code="25726" office_id="KYFFFF" status="0"/>
<zip code="25727" office_id="KY0069" status="1"/>
<zip code="25727" office_id="KYFFFF" status="0"/>
<zip code="25728" office_id="KY0069" status="1"/>
<zip code="25728" office_id="KYFFFF" status="0"/>
<zip code="25729" office_id="KY0069" status="1"/>
<zip code="25729" office_id="KYFFFF" status="0"/>
<zip code="25755" office_id="KY0069" status="1"/>
<zip code="25755" office_id="KYFFFF" status="0"/>
<zip code="25770" office_id="KY0069" status="1"/>
<zip code="25770" office_id="KYFFFF" status="0"/>
<zip code="25771" office_id="KY0069" status="1"/>
<zip code="25771" office_id="KYFFFF" status="0"/>
<zip code="25772" office_id="KY0069" status="1"/>
<zip code="25772" office_id="KYFFFF" status="0"/>
<zip code="25773" office_id="KY0069" status="1"/>
<zip code="25773" office_id="KYFFFF" status="0"/>
<zip code="25774" office_id="KY0069" status="1"/>
<zip code="25774" office_id="KYFFFF" status="0"/>
<zip code="25775" office_id="KY0069" status="1"/>
<zip code="25775" office_id="KYFFFF" status="0"/>
<zip code="25776" office_id="KY0069" status="1"/>
<zip code="25776" office_id="KYFFFF" status="0"/>
<zip code="25777" office_id="KY0069" status="1"/>
<zip code="25777" office_id="KYFFFF" status="0"/>
<zip code="25778" office_id="KY0069" status="1"/>
<zip code="25778" office_id="KYFFFF" status="0"/>
<zip code="25779" office_id="KY0069" status="1"/>
<zip code="25779" office_id="KYFFFF" status="0"/>
<zip code="25801" office_id="KY0071" status="1"/>
<zip code="25801" office_id="KYGGGG" status="0"/>
<zip code="25802" office_id="KY0071" status="1"/>
<zip code="25802" office_id="KYGGGG" status="0"/>
<zip code="25810" office_id="KY0071" status="1"/>
<zip code="25810" office_id="KYGGGG" status="0"/>
<zip code="25811" office_id="KY0071" status="1"/>
<zip code="25811" office_id="KYGGGG" status="0"/>
<zip code="25813" office_id="KY0071" status="1"/>
<zip code="25813" office_id="KYGGGG" status="0"/>
<zip code="25816" office_id="KY0071" status="1"/>
<zip code="25816" office_id="KYGGGG" status="0"/>
<zip code="25817" office_id="KY0071" status="1"/>
<zip code="25817" office_id="KYGGGG" status="0"/>
<zip code="25818" office_id="KY0071" status="1"/>
<zip code="25818" office_id="KYGGGG" status="0"/>
<zip code="25820" office_id="KY0071" status="1"/>
<zip code="25820" office_id="KYGGGG" status="0"/>
<zip code="25823" office_id="KY0071" status="1"/>
<zip code="25823" office_id="KYGGGG" status="0"/>
<zip code="25825" office_id="KY0071" status="1"/>
<zip code="25825" office_id="KYGGGG" status="0"/>
<zip code="25826" office_id="KY0071" status="1"/>
<zip code="25826" office_id="KYGGGG" status="0"/>
<zip code="25827" office_id="KY0071" status="1"/>
<zip code="25827" office_id="KYGGGG" status="0"/>
<zip code="25832" office_id="KY0071" status="1"/>
<zip code="25832" office_id="KYGGGG" status="0"/>
<zip code="25836" office_id="KY0071" status="1"/>
<zip code="25836" office_id="KYGGGG" status="0"/>
<zip code="25839" office_id="KY0071" status="1"/>
<zip code="25839" office_id="KYGGGG" status="0"/>
<zip code="25841" office_id="KY0071" status="1"/>
<zip code="25841" office_id="KYGGGG" status="0"/>
<zip code="25843" office_id="KY0071" status="1"/>
<zip code="25843" office_id="KYGGGG" status="0"/>
<zip code="25844" office_id="KY0071" status="1"/>
<zip code="25844" office_id="KYGGGG" status="0"/>
<zip code="25845" office_id="KY0071" status="1"/>
<zip code="25845" office_id="KYGGGG" status="0"/>
<zip code="25847" office_id="KY0071" status="1"/>
<zip code="25847" office_id="KYGGGG" status="0"/>
<zip code="25848" office_id="KY0071" status="1"/>
<zip code="25848" office_id="KYGGGG" status="0"/>
<zip code="25849" office_id="KY0071" status="1"/>
<zip code="25849" office_id="KYGGGG" status="0"/>
<zip code="25851" office_id="KY0071" status="1"/>
<zip code="25851" office_id="KYGGGG" status="0"/>
<zip code="25853" office_id="KY0071" status="1"/>
<zip code="25853" office_id="KYGGGG" status="0"/>
<zip code="25856" office_id="KY0071" status="1"/>
<zip code="25856" office_id="KYGGGG" status="0"/>
<zip code="25857" office_id="KY0071" status="1"/>
<zip code="25857" office_id="KYGGGG" status="0"/>
<zip code="25860" office_id="KY0071" status="1"/>
<zip code="25860" office_id="KYGGGG" status="0"/>
<zip code="25865" office_id="KY0071" status="1"/>
<zip code="25865" office_id="KYGGGG" status="0"/>
<zip code="25870" office_id="KY0071" status="1"/>
<zip code="25870" office_id="KYGGGG" status="0"/>
<zip code="25871" office_id="KY0071" status="1"/>
<zip code="25871" office_id="KYGGGG" status="0"/>
<zip code="25873" office_id="KY0071" status="1"/>
<zip code="25873" office_id="KYGGGG" status="0"/>
<zip code="25875" office_id="KY0071" status="1"/>
<zip code="25875" office_id="KYGGGG" status="0"/>
<zip code="25876" office_id="KY0071" status="1"/>
<zip code="25876" office_id="KYGGGG" status="0"/>
<zip code="25878" office_id="KY0071" status="1"/>
<zip code="25878" office_id="KYGGGG" status="0"/>
<zip code="25882" office_id="KY0071" status="1"/>
<zip code="25882" office_id="KYGGGG" status="0"/>
<zip code="25902" office_id="KY0071" status="1"/>
<zip code="25902" office_id="KYGGGG" status="0"/>
<zip code="25906" office_id="KY0071" status="1"/>
<zip code="25906" office_id="KYGGGG" status="0"/>
<zip code="25908" office_id="KY0071" status="1"/>
<zip code="25908" office_id="KYGGGG" status="0"/>
<zip code="25909" office_id="KY0071" status="1"/>
<zip code="25909" office_id="KYGGGG" status="0"/>
<zip code="25911" office_id="KY0071" status="1"/>
<zip code="25911" office_id="KYGGGG" status="0"/>
<zip code="25913" office_id="KY0071" status="1"/>
<zip code="25913" office_id="KYGGGG" status="0"/>
<zip code="25915" office_id="KY0071" status="1"/>
<zip code="25915" office_id="KYGGGG" status="0"/>
<zip code="25916" office_id="KY0071" status="1"/>
<zip code="25916" office_id="KYGGGG" status="0"/>
<zip code="25918" office_id="KY0071" status="1"/>
<zip code="25918" office_id="KYGGGG" status="0"/>
<zip code="25919" office_id="KY0071" status="1"/>
<zip code="25919" office_id="KYGGGG" status="0"/>
<zip code="25920" office_id="KY0071" status="1"/>
<zip code="25920" office_id="KYGGGG" status="0"/>
<zip code="25921" office_id="KY0071" status="1"/>
<zip code="25921" office_id="KYGGGG" status="0"/>
<zip code="25922" office_id="KY0071" status="1"/>
<zip code="25922" office_id="KYGGGG" status="0"/>
<zip code="25926" office_id="KY0071" status="1"/>
<zip code="25926" office_id="KYGGGG" status="0"/>
<zip code="25927" office_id="KY0071" status="1"/>
<zip code="25927" office_id="KYGGGG" status="0"/>
<zip code="25928" office_id="KY0071" status="1"/>
<zip code="25928" office_id="KYGGGG" status="0"/>
<zip code="25932" office_id="KY0071" status="1"/>
<zip code="25932" office_id="KYGGGG" status="0"/>
<zip code="25934" office_id="KY0071" status="1"/>
<zip code="25934" office_id="KYGGGG" status="0"/>
<zip code="25943" office_id="KY0071" status="1"/>
<zip code="25943" office_id="KYGGGG" status="0"/>
<zip code="25971" office_id="KY0071" status="1"/>
<zip code="25971" office_id="KYGGGG" status="0"/>
<zip code="25989" office_id="KY0071" status="1"/>
<zip code="25989" office_id="KYGGGG" status="0"/>
<zip code="37010" office_id="KY0052" status="1"/>
<zip code="37010" office_id="KYAAAA" status="0"/>
<zip code="37011" office_id="KY0055" status="1"/>
<zip code="37011" office_id="KYKKKK" status="0"/>
<zip code="37013" office_id="KY0055" status="1"/>
<zip code="37013" office_id="KYKKKK" status="0"/>
<zip code="37014" office_id="KY0052" status="1"/>
<zip code="37014" office_id="KYAAAA" status="0"/>
<zip code="37015" office_id="KY0052" status="1"/>
<zip code="37015" office_id="KYAAAA" status="0"/>
<zip code="37016" office_id="KY0073" status="1"/>
<zip code="37016" office_id="KYKKKK" status="0"/>
<zip code="37022" office_id="KY0055" status="1"/>
<zip code="37022" office_id="KYKKKK" status="0"/>
<zip code="37023" office_id="KY0052" status="1"/>
<zip code="37023" office_id="KYAAAA" status="0"/>
<zip code="37024" office_id="KY0055" status="1"/>
<zip code="37024" office_id="KYKKKK" status="0"/>
<zip code="37026" office_id="KY0073" status="1"/>
<zip code="37026" office_id="KYKKKK" status="0"/>
<zip code="37027" office_id="KY0055" status="1"/>
<zip code="37027" office_id="KYKKKK" status="0"/>
<zip code="37028" office_id="KY0052" status="1"/>
<zip code="37028" office_id="KYAAAA" status="0"/>
<zip code="37029" office_id="KY0052" status="1"/>
<zip code="37029" office_id="KYAAAA" status="0"/>
<zip code="37030" office_id="KY0073" status="1"/>
<zip code="37030" office_id="KYKKKK" status="0"/>
<zip code="37031" office_id="KY0055" status="1"/>
<zip code="37031" office_id="KYKKKK" status="0"/>
<zip code="37032" office_id="KY0055" status="1"/>
<zip code="37032" office_id="KYKKKK" status="0"/>
<zip code="37035" office_id="KY0052" status="1"/>
<zip code="37035" office_id="KYAAAA" status="0"/>
<zip code="37036" office_id="KY0052" status="1"/>
<zip code="37036" office_id="KYAAAA" status="0"/>
<zip code="37037" office_id="KY0055" status="1"/>
<zip code="37037" office_id="KYKKKK" status="0"/>
<zip code="37040" office_id="KY0052" status="1"/>
<zip code="37040" office_id="KYAAAA" status="0"/>
<zip code="37041" office_id="KY0052" status="1"/>
<zip code="37041" office_id="KYAAAA" status="0"/>
<zip code="37042" office_id="KY0052" status="1"/>
<zip code="37042" office_id="KYAAAA" status="0"/>
<zip code="37043" office_id="KY0052" status="1"/>
<zip code="37043" office_id="KYAAAA" status="0"/>
<zip code="37044" office_id="KY0052" status="1"/>
<zip code="37044" office_id="KYAAAA" status="0"/>
<zip code="37046" office_id="KY0052" status="1"/>
<zip code="37046" office_id="KYAAAA" status="0"/>
<zip code="37048" office_id="KY0055" status="1"/>
<zip code="37048" office_id="KYKKKK" status="0"/>
<zip code="37049" office_id="KY0055" status="1"/>
<zip code="37049" office_id="KYKKKK" status="0"/>
<zip code="37050" office_id="KY0052" status="1"/>
<zip code="37050" office_id="KYAAAA" status="0"/>
<zip code="37051" office_id="KY0052" status="1"/>
<zip code="37051" office_id="KYAAAA" status="0"/>
<zip code="37052" office_id="KY0052" status="1"/>
<zip code="37052" office_id="KYAAAA" status="0"/>
<zip code="37055" office_id="KY0052" status="1"/>
<zip code="37055" office_id="KYAAAA" status="0"/>
<zip code="37056" office_id="KY0052" status="1"/>
<zip code="37056" office_id="KYAAAA" status="0"/>
<zip code="37057" office_id="KY0073" status="1"/>
<zip code="37057" office_id="KYKKKK" status="0"/>
<zip code="37058" office_id="KY0052" status="1"/>
<zip code="37058" office_id="KYAAAA" status="0"/>
<zip code="37061" office_id="KY0052" status="1"/>
<zip code="37061" office_id="KYAAAA" status="0"/>
<zip code="37062" office_id="KY0052" status="1"/>
<zip code="37062" office_id="KYAAAA" status="0"/>
<zip code="37063" office_id="KY0055" status="1"/>
<zip code="37063" office_id="KYKKKK" status="0"/>
<zip code="37064" office_id="KY0052" status="1"/>
<zip code="37064" office_id="KYAAAA" status="0"/>
<zip code="37065" office_id="KY0052" status="1"/>
<zip code="37065" office_id="KYAAAA" status="0"/>
<zip code="37066" office_id="KY0055" status="1"/>
<zip code="37066" office_id="KYKKKK" status="0"/>
<zip code="37067" office_id="KY0052" status="1"/>
<zip code="37067" office_id="KYAAAA" status="0"/>
<zip code="37068" office_id="KY0052" status="1"/>
<zip code="37068" office_id="KYAAAA" status="0"/>
<zip code="37069" office_id="KY0052" status="1"/>
<zip code="37069" office_id="KYAAAA" status="0"/>
<zip code="37070" office_id="KY0055" status="1"/>
<zip code="37070" office_id="KYKKKK" status="0"/>
<zip code="37071" office_id="KY0055" status="1"/>
<zip code="37071" office_id="KYKKKK" status="0"/>
<zip code="37072" office_id="KY0055" status="1"/>
<zip code="37072" office_id="KYKKKK" status="0"/>
<zip code="37073" office_id="KY0055" status="1"/>
<zip code="37073" office_id="KYKKKK" status="0"/>
<zip code="37074" office_id="KY0073" status="1"/>
<zip code="37074" office_id="KYKKKK" status="0"/>
<zip code="37075" office_id="KY0055" status="1"/>
<zip code="37075" office_id="KYKKKK" status="0"/>
<zip code="37076" office_id="KY0055" status="1"/>
<zip code="37076" office_id="KYKKKK" status="0"/>
<zip code="37077" office_id="KY0055" status="1"/>
<zip code="37077" office_id="KYKKKK" status="0"/>
<zip code="37078" office_id="KY0052" status="1"/>
<zip code="37078" office_id="KYAAAA" status="0"/>
<zip code="37079" office_id="KY0052" status="1"/>
<zip code="37079" office_id="KYAAAA" status="0"/>
<zip code="37080" office_id="KY0055" status="1"/>
<zip code="37080" office_id="KYKKKK" status="0"/>
<zip code="37082" office_id="KY0052" status="1"/>
<zip code="37082" office_id="KYAAAA" status="0"/>
<zip code="37083" office_id="KY0073" status="1"/>
<zip code="37083" office_id="KYKKKK" status="0"/>
<zip code="37085" office_id="KY0055" status="1"/>
<zip code="37085" office_id="KYKKKK" status="0"/>
<zip code="37086" office_id="KY0055" status="1"/>
<zip code="37086" office_id="KYKKKK" status="0"/>
<zip code="37087" office_id="KY0055" status="1"/>
<zip code="37087" office_id="KYKKKK" status="0"/>
<zip code="37088" office_id="KY0055" status="1"/>
<zip code="37088" office_id="KYKKKK" status="0"/>
<zip code="37089" office_id="KY0055" status="1"/>
<zip code="37089" office_id="KYKKKK" status="0"/>
<zip code="37090" office_id="KY0055" status="1"/>
<zip code="37090" office_id="KYKKKK" status="0"/>
<zip code="37095" office_id="KY0073" status="1"/>
<zip code="37095" office_id="KYKKKK" status="0"/>
<zip code="37101" office_id="KY0052" status="1"/>
<zip code="37101" office_id="KYAAAA" status="0"/>
<zip code="37115" office_id="KY0055" status="1"/>
<zip code="37115" office_id="KYKKKK" status="0"/>
<zip code="37116" office_id="KY0055" status="1"/>
<zip code="37116" office_id="KYKKKK" status="0"/>
<zip code="37118" office_id="KY0073" status="1"/>
<zip code="37118" office_id="KYKKKK" status="0"/>
<zip code="37119" office_id="KY0055" status="1"/>
<zip code="37119" office_id="KYKKKK" status="0"/>
<zip code="37121" office_id="KY0055" status="1"/>
<zip code="37121" office_id="KYKKKK" status="0"/>
<zip code="37122" office_id="KY0055" status="1"/>
<zip code="37122" office_id="KYKKKK" status="0"/>
<zip code="37127" office_id="KY0055" status="1"/>
<zip code="37127" office_id="KYKKKK" status="0"/>
<zip code="37128" office_id="KY0055" status="1"/>
<zip code="37128" office_id="KYKKKK" status="0"/>
<zip code="37129" office_id="KY0055" status="1"/>
<zip code="37129" office_id="KYKKKK" status="0"/>
<zip code="37130" office_id="KY0055" status="1"/>
<zip code="37130" office_id="KYKKKK" status="0"/>
<zip code="37131" office_id="KY0055" status="1"/>
<zip code="37131" office_id="KYKKKK" status="0"/>
<zip code="37132" office_id="KY0055" status="1"/>
<zip code="37132" office_id="KYKKKK" status="0"/>
<zip code="37133" office_id="KY0055" status="1"/>
<zip code="37133" office_id="KYKKKK" status="0"/>
<zip code="37134" office_id="KY0052" status="1"/>
<zip code="37134" office_id="KYAAAA" status="0"/>
<zip code="37135" office_id="KY0052" status="1"/>
<zip code="37135" office_id="KYAAAA" status="0"/>
<zip code="37136" office_id="KY0055" status="1"/>
<zip code="37136" office_id="KYKKKK" status="0"/>
<zip code="37138" office_id="KY0055" status="1"/>
<zip code="37138" office_id="KYKKKK" status="0"/>
<zip code="37141" office_id="KY0055" status="1"/>
<zip code="37141" office_id="KYKKKK" status="0"/>
<zip code="37142" office_id="KY0052" status="1"/>
<zip code="37142" office_id="KYAAAA" status="0"/>
<zip code="37143" office_id="KY0052" status="1"/>
<zip code="37143" office_id="KYAAAA" status="0"/>
<zip code="37145" office_id="KY0073" status="1"/>
<zip code="37145" office_id="KYKKKK" status="0"/>
<zip code="37146" office_id="KY0052" status="1"/>
<zip code="37146" office_id="KYAAAA" status="0"/>
<zip code="37148" office_id="KY0055" status="1"/>
<zip code="37148" office_id="KYKKKK" status="0"/>
<zip code="37149" office_id="KY0073" status="1"/>
<zip code="37149" office_id="KYKKKK" status="0"/>
<zip code="37150" office_id="KY0073" status="1"/>
<zip code="37150" office_id="KYKKKK" status="0"/>
<zip code="37151" office_id="KY0073" status="1"/>
<zip code="37151" office_id="KYKKKK" status="0"/>
<zip code="37152" office_id="KY0055" status="1"/>
<zip code="37152" office_id="KYKKKK" status="0"/>
<zip code="37155" office_id="KY0052" status="1"/>
<zip code="37155" office_id="KYAAAA" status="0"/>
<zip code="37165" office_id="KY0052" status="1"/>
<zip code="37165" office_id="KYAAAA" status="0"/>
<zip code="37167" office_id="KY0055" status="1"/>
<zip code="37167" office_id="KYKKKK" status="0"/>
<zip code="37171" office_id="KY0052" status="1"/>
<zip code="37171" office_id="KYAAAA" status="0"/>
<zip code="37172" office_id="KY0055" status="1"/>
<zip code="37172" office_id="KYKKKK" status="0"/>
<zip code="37175" office_id="KY0052" status="1"/>
<zip code="37175" office_id="KYAAAA" status="0"/>
<zip code="37178" office_id="KY0052" status="1"/>
<zip code="37178" office_id="KYAAAA" status="0"/>
<zip code="37179" office_id="KY0052" status="1"/>
<zip code="37179" office_id="KYAAAA" status="0"/>
<zip code="37181" office_id="KY0052" status="1"/>
<zip code="37181" office_id="KYAAAA" status="0"/>
<zip code="37184" office_id="KY0055" status="1"/>
<zip code="37184" office_id="KYKKKK" status="0"/>
<zip code="37185" office_id="KY0052" status="1"/>
<zip code="37185" office_id="KYAAAA" status="0"/>
<zip code="37186" office_id="KY0073" status="1"/>
<zip code="37186" office_id="KYKKKK" status="0"/>
<zip code="37187" office_id="KY0052" status="1"/>
<zip code="37187" office_id="KYAAAA" status="0"/>
<zip code="37188" office_id="KY0055" status="1"/>
<zip code="37188" office_id="KYKKKK" status="0"/>
<zip code="37189" office_id="KY0055" status="1"/>
<zip code="37189" office_id="KYKKKK" status="0"/>
<zip code="37190" office_id="KY0073" status="1"/>
<zip code="37190" office_id="KYKKKK" status="0"/>
<zip code="37191" office_id="KY0052" status="1"/>
<zip code="37191" office_id="KYAAAA" status="0"/>
<zip code="37201" office_id="KY0055" status="1"/>
<zip code="37201" office_id="KYKKKK" status="0"/>
<zip code="37202" office_id="KY0055" status="1"/>
<zip code="37202" office_id="KYKKKK" status="0"/>
<zip code="37203" office_id="KY0055" status="1"/>
<zip code="37203" office_id="KYKKKK" status="0"/>
<zip code="37204" office_id="KY0055" status="1"/>
<zip code="37204" office_id="KYKKKK" status="0"/>
<zip code="37205" office_id="KY0055" status="1"/>
<zip code="37205" office_id="KYKKKK" status="0"/>
<zip code="37206" office_id="KY0055" status="1"/>
<zip code="37206" office_id="KYKKKK" status="0"/>
<zip code="37207" office_id="KY0055" status="1"/>
<zip code="37207" office_id="KYKKKK" status="0"/>
<zip code="37208" office_id="KY0055" status="1"/>
<zip code="37208" office_id="KYKKKK" status="0"/>
<zip code="37209" office_id="KY0055" status="1"/>
<zip code="37209" office_id="KYKKKK" status="0"/>
<zip code="37210" office_id="KY0055" status="1"/>
<zip code="37210" office_id="KYKKKK" status="0"/>
<zip code="37211" office_id="KY0055" status="1"/>
<zip code="37211" office_id="KYKKKK" status="0"/>
<zip code="37212" office_id="KY0055" status="1"/>
<zip code="37212" office_id="KYKKKK" status="0"/>
<zip code="37213" office_id="KY0055" status="1"/>
<zip code="37213" office_id="KYKKKK" status="0"/>
<zip code="37214" office_id="KY0055" status="1"/>
<zip code="37214" office_id="KYKKKK" status="0"/>
<zip code="37215" office_id="KY0055" status="1"/>
<zip code="37215" office_id="KYKKKK" status="0"/>
<zip code="37216" office_id="KY0055" status="1"/>
<zip code="37216" office_id="KYKKKK" status="0"/>
<zip code="37217" office_id="KY0055" status="1"/>
<zip code="37217" office_id="KYKKKK" status="0"/>
<zip code="37218" office_id="KY0055" status="1"/>
<zip code="37218" office_id="KYKKKK" status="0"/>
<zip code="37219" office_id="KY0055" status="1"/>
<zip code="37219" office_id="KYKKKK" status="0"/>
<zip code="37220" office_id="KY0055" status="1"/>
<zip code="37220" office_id="KYKKKK" status="0"/>
<zip code="37221" office_id="KY0055" status="1"/>
<zip code="37221" office_id="KYKKKK" status="0"/>
<zip code="37222" office_id="KY0055" status="1"/>
<zip code="37222" office_id="KYKKKK" status="0"/>
<zip code="37224" office_id="KY0055" status="1"/>
<zip code="37224" office_id="KYKKKK" status="0"/>
<zip code="37227" office_id="KY0055" status="1"/>
<zip code="37227" office_id="KYKKKK" status="0"/>
<zip code="37228" office_id="KY0055" status="1"/>
<zip code="37228" office_id="KYKKKK" status="0"/>
<zip code="37229" office_id="KY0055" status="1"/>
<zip code="37229" office_id="KYKKKK" status="0"/>
<zip code="37230" office_id="KY0055" status="1"/>
<zip code="37230" office_id="KYKKKK" status="0"/>
<zip code="37232" office_id="KY0055" status="1"/>
<zip code="37232" office_id="KYKKKK" status="0"/>
<zip code="37234" office_id="KY0055" status="1"/>
<zip code="37234" office_id="KYKKKK" status="0"/>
<zip code="37235" office_id="KY0055" status="1"/>
<zip code="37235" office_id="KYKKKK" status="0"/>
<zip code="37236" office_id="KY0055" status="1"/>
<zip code="37236" office_id="KYKKKK" status="0"/>
<zip code="37237" office_id="KY0055" status="1"/>
<zip code="37237" office_id="KYKKKK" status="0"/>
<zip code="37238" office_id="KY0055" status="1"/>
<zip code="37238" office_id="KYKKKK" status="0"/>
<zip code="37239" office_id="KY0055" status="1"/>
<zip code="37239" office_id="KYKKKK" status="0"/>
<zip code="37240" office_id="KY0055" status="1"/>
<zip code="37240" office_id="KYKKKK" status="0"/>
<zip code="37241" office_id="KY0055" status="1"/>
<zip code="37241" office_id="KYKKKK" status="0"/>
<zip code="37242" office_id="KY0055" status="1"/>
<zip code="37242" office_id="KYKKKK" status="0"/>
<zip code="37243" office_id="KY0055" status="1"/>
<zip code="37243" office_id="KYKKKK" status="0"/>
<zip code="37244" office_id="KY0055" status="1"/>
<zip code="37244" office_id="KYKKKK" status="0"/>
<zip code="37245" office_id="KY0055" status="1"/>
<zip code="37245" office_id="KYKKKK" status="0"/>
<zip code="37246" office_id="KY0055" status="1"/>
<zip code="37246" office_id="KYKKKK" status="0"/>
<zip code="37247" office_id="KY0055" status="1"/>
<zip code="37247" office_id="KYKKKK" status="0"/>
<zip code="37248" office_id="KY0055" status="1"/>
<zip code="37248" office_id="KYKKKK" status="0"/>
<zip code="37249" office_id="KY0055" status="1"/>
<zip code="37249" office_id="KYKKKK" status="0"/>
<zip code="37250" office_id="KY0055" status="1"/>
<zip code="37250" office_id="KYKKKK" status="0"/>
<zip code="37601" office_id="KY0074" status="1"/>
<zip code="37601" office_id="KYGGGG" status="0"/>
<zip code="37602" office_id="KY0074" status="1"/>
<zip code="37602" office_id="KYGGGG" status="0"/>
<zip code="37604" office_id="KY0074" status="1"/>
<zip code="37604" office_id="KYGGGG" status="0"/>
<zip code="37605" office_id="KY0074" status="1"/>
<zip code="37605" office_id="KYGGGG" status="0"/>
<zip code="37614" office_id="KY0074" status="1"/>
<zip code="37614" office_id="KYGGGG" status="0"/>
<zip code="37615" office_id="KY0074" status="1"/>
<zip code="37615" office_id="KYGGGG" status="0"/>
<zip code="37616" office_id="KY0074" status="1"/>
<zip code="37616" office_id="KYGGGG" status="0"/>
<zip code="37617" office_id="KY0074" status="1"/>
<zip code="37617" office_id="KYGGGG" status="0"/>
<zip code="37618" office_id="KY0074" status="1"/>
<zip code="37618" office_id="KYGGGG" status="0"/>
<zip code="37620" office_id="KY0074" status="1"/>
<zip code="37620" office_id="KYGGGG" status="0"/>
<zip code="37621" office_id="KY0074" status="1"/>
<zip code="37621" office_id="KYGGGG" status="0"/>
<zip code="37625" office_id="KY0074" status="1"/>
<zip code="37625" office_id="KYGGGG" status="0"/>
<zip code="37641" office_id="KY0074" status="1"/>
<zip code="37641" office_id="KYGGGG" status="0"/>
<zip code="37642" office_id="KY0074" status="1"/>
<zip code="37642" office_id="KYGGGG" status="0"/>
<zip code="37645" office_id="KY0074" status="1"/>
<zip code="37645" office_id="KYGGGG" status="0"/>
<zip code="37656" office_id="KY0074" status="1"/>
<zip code="37656" office_id="KYGGGG" status="0"/>
<zip code="37659" office_id="KY0074" status="1"/>
<zip code="37659" office_id="KYGGGG" status="0"/>
<zip code="37660" office_id="KY0074" status="1"/>
<zip code="37660" office_id="KYGGGG" status="0"/>
<zip code="37662" office_id="KY0074" status="1"/>
<zip code="37662" office_id="KYGGGG" status="0"/>
<zip code="37663" office_id="KY0074" status="1"/>
<zip code="37663" office_id="KYGGGG" status="0"/>
<zip code="37664" office_id="KY0074" status="1"/>
<zip code="37664" office_id="KYGGGG" status="0"/>
<zip code="37665" office_id="KY0074" status="1"/>
<zip code="37665" office_id="KYGGGG" status="0"/>
<zip code="37669" office_id="KY0074" status="1"/>
<zip code="37669" office_id="KYGGGG" status="0"/>
<zip code="37681" office_id="KY0074" status="1"/>
<zip code="37681" office_id="KYGGGG" status="0"/>
<zip code="37684" office_id="KY0074" status="1"/>
<zip code="37684" office_id="KYGGGG" status="0"/>
<zip code="37686" office_id="KY0074" status="1"/>
<zip code="37686" office_id="KYGGGG" status="0"/>
<zip code="37690" office_id="KY0074" status="1"/>
<zip code="37690" office_id="KYGGGG" status="0"/>
<zip code="37694" office_id="KY0074" status="1"/>
<zip code="37694" office_id="KYGGGG" status="0"/>
<zip code="37699" office_id="KY0074" status="1"/>
<zip code="37699" office_id="KYGGGG" status="0"/>
<zip code="37705" office_id="KY0074" status="1"/>
<zip code="37705" office_id="KYGGGG" status="0"/>
<zip code="37707" office_id="KY0074" status="1"/>
<zip code="37707" office_id="KYGGGG" status="0"/>
<zip code="37708" office_id="KY0074" status="1"/>
<zip code="37708" office_id="KYGGGG" status="0"/>
<zip code="37709" office_id="KY0074" status="1"/>
<zip code="37709" office_id="KYGGGG" status="0"/>
<zip code="37710" office_id="KY0074" status="1"/>
<zip code="37710" office_id="KYGGGG" status="0"/>
<zip code="37711" office_id="KY0074" status="1"/>
<zip code="37711" office_id="KYGGGG" status="0"/>
<zip code="37713" office_id="KY0074" status="1"/>
<zip code="37713" office_id="KYGGGG" status="0"/>
<zip code="37714" office_id="KY0074" status="1"/>
<zip code="37714" office_id="KYGGGG" status="0"/>
<zip code="37715" office_id="KY0074" status="1"/>
<zip code="37715" office_id="KYGGGG" status="0"/>
<zip code="37716" office_id="KY0074" status="1"/>
<zip code="37716" office_id="KYGGGG" status="0"/>
<zip code="37717" office_id="KY0074" status="1"/>
<zip code="37717" office_id="KYGGGG" status="0"/>
<zip code="37719" office_id="KY0061" status="1"/>
<zip code="37719" office_id="KYHHHH" status="0"/>
<zip code="37721" office_id="KY0074" status="1"/>
<zip code="37721" office_id="KYGGGG" status="0"/>
<zip code="37722" office_id="KY0074" status="1"/>
<zip code="37722" office_id="KYGGGG" status="0"/>
<zip code="37723" office_id="KY0061" status="1"/>
<zip code="37723" office_id="KYHHHH" status="0"/>
<zip code="37724" office_id="KY0074" status="1"/>
<zip code="37724" office_id="KYGGGG" status="0"/>
<zip code="37725" office_id="KY0074" status="1"/>
<zip code="37725" office_id="KYGGGG" status="0"/>
<zip code="37726" office_id="KY0061" status="1"/>
<zip code="37726" office_id="KYHHHH" status="0"/>
<zip code="37727" office_id="KY0074" status="1"/>
<zip code="37727" office_id="KYGGGG" status="0"/>
<zip code="37729" office_id="KY0074" status="1"/>
<zip code="37729" office_id="KYGGGG" status="0"/>
<zip code="37730" office_id="KY0074" status="1"/>
<zip code="37730" office_id="KYGGGG" status="0"/>
<zip code="37731" office_id="KY0074" status="1"/>
<zip code="37731" office_id="KYGGGG" status="0"/>
<zip code="37732" office_id="KY0064" status="1"/>
<zip code="37732" office_id="KYHHHH" status="0"/>
<zip code="37733" office_id="KY0061" status="1"/>
<zip code="37733" office_id="KYHHHH" status="0"/>
<zip code="37738" office_id="KY0074" status="1"/>
<zip code="37738" office_id="KYGGGG" status="0"/>
<zip code="37743" office_id="KY0074" status="1"/>
<zip code="37743" office_id="KYGGGG" status="0"/>
<zip code="37744" office_id="KY0074" status="1"/>
<zip code="37744" office_id="KYGGGG" status="0"/>
<zip code="37745" office_id="KY0074" status="1"/>
<zip code="37745" office_id="KYGGGG" status="0"/>
<zip code="37748" office_id="KY0074" status="1"/>
<zip code="37748" office_id="KYGGGG" status="0"/>
<zip code="37752" office_id="KY0074" status="1"/>
<zip code="37752" office_id="KYGGGG" status="0"/>
<zip code="37753" office_id="KY0074" status="1"/>
<zip code="37753" office_id="KYGGGG" status="0"/>
<zip code="37754" office_id="KY0074" status="1"/>
<zip code="37754" office_id="KYGGGG" status="0"/>
<zip code="37755" office_id="KY0064" status="1"/>
<zip code="37755" office_id="KYHHHH" status="0"/>
<zip code="37756" office_id="KY0064" status="1"/>
<zip code="37756" office_id="KYHHHH" status="0"/>
<zip code="37757" office_id="KY0074" status="1"/>
<zip code="37757" office_id="KYGGGG" status="0"/>
<zip code="37760" office_id="KY0074" status="1"/>
<zip code="37760" office_id="KYGGGG" status="0"/>
<zip code="37762" office_id="KY0074" status="1"/>
<zip code="37762" office_id="KYGGGG" status="0"/>
<zip code="37763" office_id="KY0074" status="1"/>
<zip code="37763" office_id="KYGGGG" status="0"/>
<zip code="37764" office_id="KY0074" status="1"/>
<zip code="37764" office_id="KYGGGG" status="0"/>
<zip code="37765" office_id="KY0074" status="1"/>
<zip code="37765" office_id="KYGGGG" status="0"/>
<zip code="37766" office_id="KY0074" status="1"/>
<zip code="37766" office_id="KYGGGG" status="0"/>
<zip code="37769" office_id="KY0074" status="1"/>
<zip code="37769" office_id="KYGGGG" status="0"/>
<zip code="37770" office_id="KY0061" status="1"/>
<zip code="37770" office_id="KYHHHH" status="0"/>
<zip code="37773" office_id="KY0074" status="1"/>
<zip code="37773" office_id="KYGGGG" status="0"/>
<zip code="37778" office_id="KY0074" status="1"/>
<zip code="37778" office_id="KYGGGG" status="0"/>
<zip code="37779" office_id="KY0074" status="1"/>
<zip code="37779" office_id="KYGGGG" status="0"/>
<zip code="37806" office_id="KY0074" status="1"/>
<zip code="37806" office_id="KYGGGG" status="0"/>
<zip code="37807" office_id="KY0074" status="1"/>
<zip code="37807" office_id="KYGGGG" status="0"/>
<zip code="37809" office_id="KY0074" status="1"/>
<zip code="37809" office_id="KYGGGG" status="0"/>
<zip code="37810" office_id="KY0074" status="1"/>
<zip code="37810" office_id="KYGGGG" status="0"/>
<zip code="37811" office_id="KY0074" status="1"/>
<zip code="37811" office_id="KYGGGG" status="0"/>
<zip code="37813" office_id="KY0074" status="1"/>
<zip code="37813" office_id="KYGGGG" status="0"/>
<zip code="37814" office_id="KY0074" status="1"/>
<zip code="37814" office_id="KYGGGG" status="0"/>
<zip code="37815" office_id="KY0074" status="1"/>
<zip code="37815" office_id="KYGGGG" status="0"/>
<zip code="37816" office_id="KY0074" status="1"/>
<zip code="37816" office_id="KYGGGG" status="0"/>
<zip code="37818" office_id="KY0074" status="1"/>
<zip code="37818" office_id="KYGGGG" status="0"/>
<zip code="37819" office_id="KY0074" status="1"/>
<zip code="37819" office_id="KYGGGG" status="0"/>
<zip code="37820" office_id="KY0074" status="1"/>
<zip code="37820" office_id="KYGGGG" status="0"/>
<zip code="37821" office_id="KY0074" status="1"/>
<zip code="37821" office_id="KYGGGG" status="0"/>
<zip code="37822" office_id="KY0074" status="1"/>
<zip code="37822" office_id="KYGGGG" status="0"/>
<zip code="37824" office_id="KY0074" status="1"/>
<zip code="37824" office_id="KYGGGG" status="0"/>
<zip code="37825" office_id="KY0074" status="1"/>
<zip code="37825" office_id="KYGGGG" status="0"/>
<zip code="37828" office_id="KY0074" status="1"/>
<zip code="37828" office_id="KYGGGG" status="0"/>
<zip code="37829" office_id="KY0061" status="1"/>
<zip code="37829" office_id="KYHHHH" status="0"/>
<zip code="37830" office_id="KY0074" status="1"/>
<zip code="37830" office_id="KYGGGG" status="0"/>
<zip code="37831" office_id="KY0074" status="1"/>
<zip code="37831" office_id="KYGGGG" status="0"/>
<zip code="37840" office_id="KY0074" status="1"/>
<zip code="37840" office_id="KYGGGG" status="0"/>
<zip code="37841" office_id="KY0064" status="1"/>
<zip code="37841" office_id="KYHHHH" status="0"/>
<zip code="37842" office_id="KY0061" status="1"/>
<zip code="37842" office_id="KYHHHH" status="0"/>
<zip code="37843" office_id="KY0074" status="1"/>
<zip code="37843" office_id="KYGGGG" status="0"/>
<zip code="37845" office_id="KY0061" status="1"/>
<zip code="37845" office_id="KYHHHH" status="0"/>
<zip code="37847" office_id="KY0074" status="1"/>
<zip code="37847" office_id="KYGGGG" status="0"/>
<zip code="37848" office_id="KY0074" status="1"/>
<zip code="37848" office_id="KYGGGG" status="0"/>
<zip code="37849" office_id="KY0074" status="1"/>
<zip code="37849" office_id="KYGGGG" status="0"/>
<zip code="37852" office_id="KY0064" status="1"/>
<zip code="37852" office_id="KYHHHH" status="0"/>
<zip code="37854" office_id="KY0074" status="1"/>
<zip code="37854" office_id="KYGGGG" status="0"/>
<zip code="37857" office_id="KY0074" status="1"/>
<zip code="37857" office_id="KYGGGG" status="0"/>
<zip code="37860" office_id="KY0074" status="1"/>
<zip code="37860" office_id="KYGGGG" status="0"/>
<zip code="37861" office_id="KY0074" status="1"/>
<zip code="37861" office_id="KYGGGG" status="0"/>
<zip code="37862" office_id="KY0074" status="1"/>
<zip code="37862" office_id="KYGGGG" status="0"/>
<zip code="37863" office_id="KY0074" status="1"/>
<zip code="37863" office_id="KYGGGG" status="0"/>
<zip code="37864" office_id="KY0074" status="1"/>
<zip code="37864" office_id="KYGGGG" status="0"/>
<zip code="37866" office_id="KY0074" status="1"/>
<zip code="37866" office_id="KYGGGG" status="0"/>
<zip code="37867" office_id="KY0074" status="1"/>
<zip code="37867" office_id="KYGGGG" status="0"/>
<zip code="37868" office_id="KY0074" status="1"/>
<zip code="37868" office_id="KYGGGG" status="0"/>
<zip code="37869" office_id="KY0074" status="1"/>
<zip code="37869" office_id="KYGGGG" status="0"/>
<zip code="37870" office_id="KY0074" status="1"/>
<zip code="37870" office_id="KYGGGG" status="0"/>
<zip code="37871" office_id="KY0074" status="1"/>
<zip code="37871" office_id="KYGGGG" status="0"/>
<zip code="37872" office_id="KY0061" status="1"/>
<zip code="37872" office_id="KYHHHH" status="0"/>
<zip code="37873" office_id="KY0074" status="1"/>
<zip code="37873" office_id="KYGGGG" status="0"/>
<zip code="37876" office_id="KY0074" status="1"/>
<zip code="37876" office_id="KYGGGG" status="0"/>
<zip code="37877" office_id="KY0074" status="1"/>
<zip code="37877" office_id="KYGGGG" status="0"/>
<zip code="37879" office_id="KY0074" status="1"/>
<zip code="37879" office_id="KYGGGG" status="0"/>
<zip code="37880" office_id="KY0074" status="1"/>
<zip code="37880" office_id="KYGGGG" status="0"/>
<zip code="37881" office_id="KY0074" status="1"/>
<zip code="37881" office_id="KYGGGG" status="0"/>
<zip code="37887" office_id="KY0061" status="1"/>
<zip code="37887" office_id="KYHHHH" status="0"/>
<zip code="37888" office_id="KY0074" status="1"/>
<zip code="37888" office_id="KYGGGG" status="0"/>
<zip code="37890" office_id="KY0074" status="1"/>
<zip code="37890" office_id="KYGGGG" status="0"/>
<zip code="37891" office_id="KY0074" status="1"/>
<zip code="37891" office_id="KYGGGG" status="0"/>
<zip code="37892" office_id="KY0064" status="1"/>
<zip code="37892" office_id="KYHHHH" status="0"/>
<zip code="37901" office_id="KY0074" status="1"/>
<zip code="37901" office_id="KYGGGG" status="0"/>
<zip code="37902" office_id="KY0074" status="1"/>
<zip code="37902" office_id="KYGGGG" status="0"/>
<zip code="37909" office_id="KY0074" status="1"/>
<zip code="37909" office_id="KYGGGG" status="0"/>
<zip code="37912" office_id="KY0074" status="1"/>
<zip code="37912" office_id="KYGGGG" status="0"/>
<zip code="37914" office_id="KY0074" status="1"/>
<zip code="37914" office_id="KYGGGG" status="0"/>
<zip code="37915" office_id="KY0074" status="1"/>
<zip code="37915" office_id="KYGGGG" status="0"/>
<zip code="37916" office_id="KY0074" status="1"/>
<zip code="37916" office_id="KYGGGG" status="0"/>
<zip code="37917" office_id="KY0074" status="1"/>
<zip code="37917" office_id="KYGGGG" status="0"/>
<zip code="37918" office_id="KY0074" status="1"/>
<zip code="37918" office_id="KYGGGG" status="0"/>
<zip code="37919" office_id="KY0074" status="1"/>
<zip code="37919" office_id="KYGGGG" status="0"/>
<zip code="37920" office_id="KY0074" status="1"/>
<zip code="37920" office_id="KYGGGG" status="0"/>
<zip code="37921" office_id="KY0074" status="1"/>
<zip code="37921" office_id="KYGGGG" status="0"/>
<zip code="37922" office_id="KY0074" status="1"/>
<zip code="37922" office_id="KYGGGG" status="0"/>
<zip code="37923" office_id="KY0074" status="1"/>
<zip code="37923" office_id="KYGGGG" status="0"/>
<zip code="37924" office_id="KY0074" status="1"/>
<zip code="37924" office_id="KYGGGG" status="0"/>
<zip code="37927" office_id="KY0074" status="1"/>
<zip code="37927" office_id="KYGGGG" status="0"/>
<zip code="37928" office_id="KY0074" status="1"/>
<zip code="37928" office_id="KYGGGG" status="0"/>
<zip code="37929" office_id="KY0074" status="1"/>
<zip code="37929" office_id="KYGGGG" status="0"/>
<zip code="37930" office_id="KY0074" status="1"/>
<zip code="37930" office_id="KYGGGG" status="0"/>
<zip code="37931" office_id="KY0074" status="1"/>
<zip code="37931" office_id="KYGGGG" status="0"/>
<zip code="37932" office_id="KY0074" status="1"/>
<zip code="37932" office_id="KYGGGG" status="0"/>
<zip code="37933" office_id="KY0074" status="1"/>
<zip code="37933" office_id="KYGGGG" status="0"/>
<zip code="37938" office_id="KY0074" status="1"/>
<zip code="37938" office_id="KYGGGG" status="0"/>
<zip code="37939" office_id="KY0074" status="1"/>
<zip code="37939" office_id="KYGGGG" status="0"/>
<zip code="37940" office_id="KY0074" status="1"/>
<zip code="37940" office_id="KYGGGG" status="0"/>
<zip code="37950" office_id="KY0074" status="1"/>
<zip code="37950" office_id="KYGGGG" status="0"/>
<zip code="37990" office_id="KY0074" status="1"/>
<zip code="37990" office_id="KYGGGG" status="0"/>
<zip code="37995" office_id="KY0074" status="1"/>
<zip code="37995" office_id="KYGGGG" status="0"/>
<zip code="37996" office_id="KY0074" status="1"/>
<zip code="37996" office_id="KYGGGG" status="0"/>
<zip code="37997" office_id="KY0074" status="1"/>
<zip code="37997" office_id="KYGGGG" status="0"/>
<zip code="37998" office_id="KY0074" status="1"/>
<zip code="37998" office_id="KYGGGG" status="0"/>
<zip code="38001" office_id="KY0050" status="1"/>
<zip code="38001" office_id="KYAAAA" status="0"/>
<zip code="38006" office_id="KY0050" status="1"/>
<zip code="38006" office_id="KYAAAA" status="0"/>
<zip code="38007" office_id="KY0050" status="1"/>
<zip code="38007" office_id="KYAAAA" status="0"/>
<zip code="38021" office_id="KY0050" status="1"/>
<zip code="38021" office_id="KYAAAA" status="0"/>
<zip code="38024" office_id="KY0050" status="1"/>
<zip code="38024" office_id="KYAAAA" status="0"/>
<zip code="38025" office_id="KY0050" status="1"/>
<zip code="38025" office_id="KYAAAA" status="0"/>
<zip code="38030" office_id="KY0050" status="1"/>
<zip code="38030" office_id="KYAAAA" status="0"/>
<zip code="38034" office_id="KY0050" status="1"/>
<zip code="38034" office_id="KYAAAA" status="0"/>
<zip code="38037" office_id="KY0050" status="1"/>
<zip code="38037" office_id="KYAAAA" status="0"/>
<zip code="38040" office_id="KY0050" status="1"/>
<zip code="38040" office_id="KYAAAA" status="0"/>
<zip code="38041" office_id="KY0050" status="1"/>
<zip code="38041" office_id="KYAAAA" status="0"/>
<zip code="38047" office_id="KY0050" status="1"/>
<zip code="38047" office_id="KYAAAA" status="0"/>
<zip code="38050" office_id="KY0050" status="1"/>
<zip code="38050" office_id="KYAAAA" status="0"/>
<zip code="38056" office_id="KY0050" status="1"/>
<zip code="38056" office_id="KYAAAA" status="0"/>
<zip code="38059" office_id="KY0050" status="1"/>
<zip code="38059" office_id="KYAAAA" status="0"/>
<zip code="38063" office_id="KY0050" status="1"/>
<zip code="38063" office_id="KYAAAA" status="0"/>
<zip code="38070" office_id="KY0050" status="1"/>
<zip code="38070" office_id="KYAAAA" status="0"/>
<zip code="38077" office_id="KY0050" status="1"/>
<zip code="38077" office_id="KYAAAA" status="0"/>
<zip code="38079" office_id="KY0050" status="1"/>
<zip code="38079" office_id="KYAAAA" status="0"/>
<zip code="38080" office_id="KY0050" status="1"/>
<zip code="38080" office_id="KYAAAA" status="0"/>
<zip code="38201" office_id="KY0052" status="1"/>
<zip code="38201" office_id="KYAAAA" status="0"/>
<zip code="38220" office_id="KY0052" status="1"/>
<zip code="38220" office_id="KYAAAA" status="0"/>
<zip code="38221" office_id="KY0052" status="1"/>
<zip code="38221" office_id="KYAAAA" status="0"/>
<zip code="38222" office_id="KY0052" status="1"/>
<zip code="38222" office_id="KYAAAA" status="0"/>
<zip code="38223" office_id="KY0052" status="1"/>
<zip code="38223" office_id="KYAAAA" status="0"/>
<zip code="38224" office_id="KY0052" status="1"/>
<zip code="38224" office_id="KYAAAA" status="0"/>
<zip code="38225" office_id="KY0050" status="1"/>
<zip code="38225" office_id="KYAAAA" status="0"/>
<zip code="38226" office_id="KY0050" status="1"/>
<zip code="38226" office_id="KYAAAA" status="0"/>
<zip code="38229" office_id="KY0050" status="1"/>
<zip code="38229" office_id="KYAAAA" status="0"/>
<zip code="38230" office_id="KY0050" status="1"/>
<zip code="38230" office_id="KYAAAA" status="0"/>
<zip code="38231" office_id="KY0052" status="1"/>
<zip code="38231" office_id="KYAAAA" status="0"/>
<zip code="38232" office_id="KY0050" status="1"/>
<zip code="38232" office_id="KYAAAA" status="0"/>
<zip code="38233" office_id="KY0050" status="1"/>
<zip code="38233" office_id="KYAAAA" status="0"/>
<zip code="38235" office_id="KY0052" status="1"/>
<zip code="38235" office_id="KYAAAA" status="0"/>
<zip code="38236" office_id="KY0052" status="1"/>
<zip code="38236" office_id="KYAAAA" status="0"/>
<zip code="38237" office_id="KY0050" status="1"/>
<zip code="38237" office_id="KYAAAA" status="0"/>
<zip code="38238" office_id="KY0050" status="1"/>
<zip code="38238" office_id="KYAAAA" status="0"/>
<zip code="38240" office_id="KY0050" status="1"/>
<zip code="38240" office_id="KYAAAA" status="0"/>
<zip code="38241" office_id="KY0050" status="1"/>
<zip code="38241" office_id="KYAAAA" status="0"/>
<zip code="38242" office_id="KY0052" status="1"/>
<zip code="38242" office_id="KYAAAA" status="0"/>
<zip code="38251" office_id="KY0052" status="1"/>
<zip code="38251" office_id="KYAAAA" status="0"/>
<zip code="38253" office_id="KY0050" status="1"/>
<zip code="38253" office_id="KYAAAA" status="0"/>
<zip code="38254" office_id="KY0050" status="1"/>
<zip code="38254" office_id="KYAAAA" status="0"/>
<zip code="38255" office_id="KY0050" status="1"/>
<zip code="38255" office_id="KYAAAA" status="0"/>
<zip code="38256" office_id="KY0052" status="1"/>
<zip code="38256" office_id="KYAAAA" status="0"/>
<zip code="38257" office_id="KY0050" status="1"/>
<zip code="38257" office_id="KYAAAA" status="0"/>
<zip code="38258" office_id="KY0052" status="1"/>
<zip code="38258" office_id="KYAAAA" status="0"/>
<zip code="38259" office_id="KY0050" status="1"/>
<zip code="38259" office_id="KYAAAA" status="0"/>
<zip code="38260" office_id="KY0050" status="1"/>
<zip code="38260" office_id="KYAAAA" status="0"/>
<zip code="38261" office_id="KY0050" status="1"/>
<zip code="38261" office_id="KYAAAA" status="0"/>
<zip code="38271" office_id="KY0050" status="1"/>
<zip code="38271" office_id="KYAAAA" status="0"/>
<zip code="38281" office_id="KY0050" status="1"/>
<zip code="38281" office_id="KYAAAA" status="0"/>
<zip code="38316" office_id="KY0050" status="1"/>
<zip code="38316" office_id="KYAAAA" status="0"/>
<zip code="38317" office_id="KY0052" status="1"/>
<zip code="38317" office_id="KYAAAA" status="0"/>
<zip code="38318" office_id="KY0052" status="1"/>
<zip code="38318" office_id="KYAAAA" status="0"/>
<zip code="38320" office_id="KY0052" status="1"/>
<zip code="38320" office_id="KYAAAA" status="0"/>
<zip code="38321" office_id="KY0052" status="1"/>
<zip code="38321" office_id="KYAAAA" status="0"/>
<zip code="38324" office_id="KY0052" status="1"/>
<zip code="38324" office_id="KYAAAA" status="0"/>
<zip code="38330" office_id="KY0050" status="1"/>
<zip code="38330" office_id="KYAAAA" status="0"/>
<zip code="38331" office_id="KY0050" status="1"/>
<zip code="38331" office_id="KYAAAA" status="0"/>
<zip code="38333" office_id="KY0052" status="1"/>
<zip code="38333" office_id="KYAAAA" status="0"/>
<zip code="38336" office_id="KY0050" status="1"/>
<zip code="38336" office_id="KYAAAA" status="0"/>
<zip code="38337" office_id="KY0050" status="1"/>
<zip code="38337" office_id="KYAAAA" status="0"/>
<zip code="38338" office_id="KY0050" status="1"/>
<zip code="38338" office_id="KYAAAA" status="0"/>
<zip code="38341" office_id="KY0052" status="1"/>
<zip code="38341" office_id="KYAAAA" status="0"/>
<zip code="38342" office_id="KY0052" status="1"/>
<zip code="38342" office_id="KYAAAA" status="0"/>
<zip code="38343" office_id="KY0050" status="1"/>
<zip code="38343" office_id="KYAAAA" status="0"/>
<zip code="38344" office_id="KY0052" status="1"/>
<zip code="38344" office_id="KYAAAA" status="0"/>
<zip code="38346" office_id="KY0050" status="1"/>
<zip code="38346" office_id="KYAAAA" status="0"/>
<zip code="38348" office_id="KY0052" status="1"/>
<zip code="38348" office_id="KYAAAA" status="0"/>
<zip code="38358" office_id="KY0050" status="1"/>
<zip code="38358" office_id="KYAAAA" status="0"/>
<zip code="38369" office_id="KY0050" status="1"/>
<zip code="38369" office_id="KYAAAA" status="0"/>
<zip code="38382" office_id="KY0050" status="1"/>
<zip code="38382" office_id="KYAAAA" status="0"/>
<zip code="38387" office_id="KY0052" status="1"/>
<zip code="38387" office_id="KYAAAA" status="0"/>
<zip code="38389" office_id="KY0050" status="1"/>
<zip code="38389" office_id="KYAAAA" status="0"/>
<zip code="38390" office_id="KY0052" status="1"/>
<zip code="38390" office_id="KYAAAA" status="0"/>
<zip code="38476" office_id="KY0052" status="1"/>
<zip code="38476" office_id="KYAAAA" status="0"/>
<zip code="38501" office_id="KY0073" status="1"/>
<zip code="38501" office_id="KYKKKK" status="0"/>
<zip code="38502" office_id="KY0073" status="1"/>
<zip code="38502" office_id="KYKKKK" status="0"/>
<zip code="38503" office_id="KY0073" status="1"/>
<zip code="38503" office_id="KYKKKK" status="0"/>
<zip code="38504" office_id="KY0061" status="1"/>
<zip code="38504" office_id="KYHHHH" status="0"/>
<zip code="38505" office_id="KY0073" status="1"/>
<zip code="38505" office_id="KYKKKK" status="0"/>
<zip code="38506" office_id="KY0073" status="1"/>
<zip code="38506" office_id="KYKKKK" status="0"/>
<zip code="38541" office_id="KY0073" status="1"/>
<zip code="38541" office_id="KYKKKK" status="0"/>
<zip code="38542" office_id="KY0073" status="1"/>
<zip code="38542" office_id="KYKKKK" status="0"/>
<zip code="38543" office_id="KY0073" status="1"/>
<zip code="38543" office_id="KYKKKK" status="0"/>
<zip code="38544" office_id="KY0073" status="1"/>
<zip code="38544" office_id="KYKKKK" status="0"/>
<zip code="38545" office_id="KY0073" status="1"/>
<zip code="38545" office_id="KYKKKK" status="0"/>
<zip code="38547" office_id="KY0073" status="1"/>
<zip code="38547" office_id="KYKKKK" status="0"/>
<zip code="38548" office_id="KY0073" status="1"/>
<zip code="38548" office_id="KYKKKK" status="0"/>
<zip code="38549" office_id="KY0061" status="1"/>
<zip code="38549" office_id="KYHHHH" status="0"/>
<zip code="38551" office_id="KY0073" status="1"/>
<zip code="38551" office_id="KYKKKK" status="0"/>
<zip code="38552" office_id="KY0073" status="1"/>
<zip code="38552" office_id="KYKKKK" status="0"/>
<zip code="38553" office_id="KY0061" status="1"/>
<zip code="38553" office_id="KYHHHH" status="0"/>
<zip code="38554" office_id="KY0073" status="1"/>
<zip code="38554" office_id="KYKKKK" status="0"/>
<zip code="38555" office_id="KY0061" status="1"/>
<zip code="38555" office_id="KYHHHH" status="0"/>
<zip code="38556" office_id="KY0061" status="1"/>
<zip code="38556" office_id="KYHHHH" status="0"/>
<zip code="38557" office_id="KY0061" status="1"/>
<zip code="38557" office_id="KYHHHH" status="0"/>
<zip code="38558" office_id="KY0061" status="1"/>
<zip code="38558" office_id="KYHHHH" status="0"/>
<zip code="38560" office_id="KY0073" status="1"/>
<zip code="38560" office_id="KYKKKK" status="0"/>
<zip code="38562" office_id="KY0073" status="1"/>
<zip code="38562" office_id="KYKKKK" status="0"/>
<zip code="38563" office_id="KY0073" status="1"/>
<zip code="38563" office_id="KYKKKK" status="0"/>
<zip code="38564" office_id="KY0073" status="1"/>
<zip code="38564" office_id="KYKKKK" status="0"/>
<zip code="38565" office_id="KY0061" status="1"/>
<zip code="38565" office_id="KYHHHH" status="0"/>
<zip code="38567" office_id="KY0073" status="1"/>
<zip code="38567" office_id="KYKKKK" status="0"/>
<zip code="38568" office_id="KY0073" status="1"/>
<zip code="38568" office_id="KYKKKK" status="0"/>
<zip code="38569" office_id="KY0073" status="1"/>
<zip code="38569" office_id="KYKKKK" status="0"/>
<zip code="38570" office_id="KY0073" status="1"/>
<zip code="38570" office_id="KYKKKK" status="0"/>
<zip code="38573" office_id="KY0073" status="1"/>
<zip code="38573" office_id="KYKKKK" status="0"/>
<zip code="38574" office_id="KY0073" status="1"/>
<zip code="38574" office_id="KYKKKK" status="0"/>
<zip code="38575" office_id="KY0073" status="1"/>
<zip code="38575" office_id="KYKKKK" status="0"/>
<zip code="38577" office_id="KY0061" status="1"/>
<zip code="38577" office_id="KYHHHH" status="0"/>
<zip code="38578" office_id="KY0061" status="1"/>
<zip code="38578" office_id="KYHHHH" status="0"/>
<zip code="38580" office_id="KY0073" status="1"/>
<zip code="38580" office_id="KYKKKK" status="0"/>
<zip code="38582" office_id="KY0073" status="1"/>
<zip code="38582" office_id="KYKKKK" status="0"/>
<zip code="38588" office_id="KY0073" status="1"/>
<zip code="38588" office_id="KYKKKK" status="0"/>
<zip code="38589" office_id="KY0061" status="1"/>
<zip code="38589" office_id="KYHHHH" status="0"/>
<zip code="40003" office_id="KY1056" status="1"/>
<zip code="40003" office_id="KYCCCC" status="0"/>
<zip code="40004" office_id="KY0075" status="1"/>
<zip code="40004" office_id="KYBBBB" status="0"/>
<zip code="40006" office_id="KY1056" status="1"/>
<zip code="40006" office_id="KYCCCC" status="0"/>
<zip code="40007" office_id="KY1056" status="1"/>
<zip code="40007" office_id="KYCCCC" status="0"/>
<zip code="40008" office_id="KY0075" status="1"/>
<zip code="40008" office_id="KYBBBB" status="0"/>
<zip code="40009" office_id="KY0075" status="1"/>
<zip code="40009" office_id="KYBBBB" status="0"/>
<zip code="40010" office_id="KY1056" status="1"/>
<zip code="40010" office_id="KYCCCC" status="0"/>
<zip code="40011" office_id="KY1056" status="1"/>
<zip code="40011" office_id="KYCCCC" status="0"/>
<zip code="40012" office_id="KY0075" status="1"/>
<zip code="40012" office_id="KYBBBB" status="0"/>
<zip code="40013" office_id="KY0075" status="1"/>
<zip code="40013" office_id="KYBBBB" status="0"/>
<zip code="40014" office_id="KY1056" status="1"/>
<zip code="40014" office_id="KYCCCC" status="0"/>
<zip code="40017" office_id="KY1056" status="1"/>
<zip code="40017" office_id="KYCCCC" status="0"/>
<zip code="40018" office_id="KY1056" status="1"/>
<zip code="40018" office_id="KYCCCC" status="0"/>
<zip code="40019" office_id="KY1056" status="1"/>
<zip code="40019" office_id="KYCCCC" status="0"/>
<zip code="40020" office_id="KY0075" status="1"/>
<zip code="40020" office_id="KYBBBB" status="0"/>
<zip code="40022" office_id="KY1056" status="1"/>
<zip code="40022" office_id="KYCCCC" status="0"/>
<zip code="40023" office_id="KY1056" status="1"/>
<zip code="40023" office_id="KYCCCC" status="0"/>
<zip code="40025" office_id="KY1056" status="1"/>
<zip code="40025" office_id="KYCCCC" status="0"/>
<zip code="40026" office_id="KY1056" status="1"/>
<zip code="40026" office_id="KYCCCC" status="0"/>
<zip code="40027" office_id="KY1056" status="1"/>
<zip code="40027" office_id="KYCCCC" status="0"/>
<zip code="40028" office_id="KY0075" status="1"/>
<zip code="40028" office_id="KYBBBB" status="0"/>
<zip code="40031" office_id="KY1056" status="1"/>
<zip code="40031" office_id="KYCCCC" status="0"/>
<zip code="40032" office_id="KY1056" status="1"/>
<zip code="40032" office_id="KYCCCC" status="0"/>
<zip code="40033" office_id="KY0075" status="1"/>
<zip code="40033" office_id="KYBBBB" status="0"/>
<zip code="40036" office_id="KY1056" status="1"/>
<zip code="40036" office_id="KYCCCC" status="0"/>
<zip code="40037" office_id="KY0075" status="1"/>
<zip code="40037" office_id="KYBBBB" status="0"/>
<zip code="40040" office_id="KY0075" status="1"/>
<zip code="40040" office_id="KYBBBB" status="0"/>
<zip code="40041" office_id="KY1056" status="1"/>
<zip code="40041" office_id="KYCCCC" status="0"/>
<zip code="40045" office_id="KY1056" status="1"/>
<zip code="40045" office_id="KYCCCC" status="0"/>
<zip code="40046" office_id="KY1056" status="1"/>
<zip code="40046" office_id="KYCCCC" status="0"/>
<zip code="40047" office_id="KY1056" status="1"/>
<zip code="40047" office_id="KYCCCC" status="0"/>
<zip code="40048" office_id="KY0075" status="1"/>
<zip code="40048" office_id="KYBBBB" status="0"/>
<zip code="40049" office_id="KY0075" status="1"/>
<zip code="40049" office_id="KYBBBB" status="0"/>
<zip code="40050" office_id="KY1056" status="1"/>
<zip code="40050" office_id="KYCCCC" status="0"/>
<zip code="40051" office_id="KY0075" status="1"/>
<zip code="40051" office_id="KYBBBB" status="0"/>
<zip code="40052" office_id="KY0075" status="1"/>
<zip code="40052" office_id="KYBBBB" status="0"/>
<zip code="40055" office_id="KY1056" status="1"/>
<zip code="40055" office_id="KYCCCC" status="0"/>
<zip code="40056" office_id="KY1056" status="1"/>
<zip code="40056" office_id="KYCCCC" status="0"/>
<zip code="40057" office_id="KY1056" status="1"/>
<zip code="40057" office_id="KYCCCC" status="0"/>
<zip code="40058" office_id="KY1056" status="1"/>
<zip code="40058" office_id="KYCCCC" status="0"/>
<zip code="40059" office_id="KY1056" status="1"/>
<zip code="40059" office_id="KYCCCC" status="0"/>
<zip code="40060" office_id="KY0075" status="1"/>
<zip code="40060" office_id="KYBBBB" status="0"/>
<zip code="40061" office_id="KY0075" status="1"/>
<zip code="40061" office_id="KYBBBB" status="0"/>
<zip code="40062" office_id="KY0075" status="1"/>
<zip code="40062" office_id="KYBBBB" status="0"/>
<zip code="40063" office_id="KY0075" status="1"/>
<zip code="40063" office_id="KYBBBB" status="0"/>
<zip code="40065" office_id="KY1056" status="1"/>
<zip code="40065" office_id="KYCCCC" status="0"/>
<zip code="40066" office_id="KY1056" status="1"/>
<zip code="40066" office_id="KYCCCC" status="0"/>
<zip code="40067" office_id="KY1056" status="1"/>
<zip code="40067" office_id="KYCCCC" status="0"/>
<zip code="40068" office_id="KY1056" status="1"/>
<zip code="40068" office_id="KYCCCC" status="0"/>
<zip code="40069" office_id="KY0075" status="1"/>
<zip code="40069" office_id="KYBBBB" status="0"/>
<zip code="40070" office_id="KY1056" status="1"/>
<zip code="40070" office_id="KYCCCC" status="0"/>
<zip code="40071" office_id="KY1056" status="1"/>
<zip code="40071" office_id="KYCCCC" status="0"/>
<zip code="40075" office_id="KY1056" status="1"/>
<zip code="40075" office_id="KYCCCC" status="0"/>
<zip code="40076" office_id="KY1056" status="1"/>
<zip code="40076" office_id="KYCCCC" status="0"/>
<zip code="40077" office_id="KY1056" status="1"/>
<zip code="40077" office_id="KYCCCC" status="0"/>
<zip code="40078" office_id="KY0075" status="1"/>
<zip code="40078" office_id="KYBBBB" status="0"/>
<zip code="40104" office_id="KY0075" status="1"/>
<zip code="40104" office_id="KYBBBB" status="0"/>
<zip code="40106" office_id="KY0075" status="1"/>
<zip code="40106" office_id="KYBBBB" status="0"/>
<zip code="40107" office_id="KY0075" status="1"/>
<zip code="40107" office_id="KYBBBB" status="0"/>
<zip code="40108" office_id="KY0075" status="1"/>
<zip code="40108" office_id="KYBBBB" status="0"/>
<zip code="40109" office_id="KY1056" status="1"/>
<zip code="40109" office_id="KYCCCC" status="0"/>
<zip code="40110" office_id="KY1056" status="1"/>
<zip code="40110" office_id="KYCCCC" status="0"/>
<zip code="40111" office_id="KY0075" status="1"/>
<zip code="40111" office_id="KYBBBB" status="0"/>
<zip code="40114" office_id="KY0075" status="1"/>
<zip code="40114" office_id="KYBBBB" status="0"/>
<zip code="40115" office_id="KY0075" status="1"/>
<zip code="40115" office_id="KYBBBB" status="0"/>
<zip code="40117" office_id="KY0075" status="1"/>
<zip code="40117" office_id="KYBBBB" status="0"/>
<zip code="40118" office_id="KY1056" status="1"/>
<zip code="40118" office_id="KYCCCC" status="0"/>
<zip code="40119" office_id="KY0075" status="1"/>
<zip code="40119" office_id="KYBBBB" status="0"/>
<zip code="40121" office_id="KY0075" status="1"/>
<zip code="40121" office_id="KYBBBB" status="0"/>
<zip code="40129" office_id="KY1056" status="1"/>
<zip code="40129" office_id="KYCCCC" status="0"/>
<zip code="40140" office_id="KY0075" status="1"/>
<zip code="40140" office_id="KYBBBB" status="0"/>
<zip code="40142" office_id="KY0075" status="1"/>
<zip code="40142" office_id="KYBBBB" status="0"/>
<zip code="40143" office_id="KY0075" status="1"/>
<zip code="40143" office_id="KYBBBB" status="0"/>
<zip code="40144" office_id="KY0075" status="1"/>
<zip code="40144" office_id="KYBBBB" status="0"/>
<zip code="40145" office_id="KY0075" status="1"/>
<zip code="40145" office_id="KYBBBB" status="0"/>
<zip code="40146" office_id="KY0075" status="1"/>
<zip code="40146" office_id="KYBBBB" status="0"/>
<zip code="40150" office_id="KY1056" status="1"/>
<zip code="40150" office_id="KYCCCC" status="0"/>
<zip code="40152" office_id="KY0075" status="1"/>
<zip code="40152" office_id="KYBBBB" status="0"/>
<zip code="40153" office_id="KY0075" status="1"/>
<zip code="40153" office_id="KYBBBB" status="0"/>
<zip code="40155" office_id="KY0075" status="1"/>
<zip code="40155" office_id="KYBBBB" status="0"/>
<zip code="40157" office_id="KY0075" status="1"/>
<zip code="40157" office_id="KYBBBB" status="0"/>
<zip code="40159" office_id="KY0075" status="1"/>
<zip code="40159" office_id="KYBBBB" status="0"/>
<zip code="40160" office_id="KY0075" status="1"/>
<zip code="40160" office_id="KYBBBB" status="0"/>
<zip code="40161" office_id="KY0075" status="1"/>
<zip code="40161" office_id="KYBBBB" status="0"/>
<zip code="40162" office_id="KY0075" status="1"/>
<zip code="40162" office_id="KYBBBB" status="0"/>
<zip code="40164" office_id="KY0075" status="1"/>
<zip code="40164" office_id="KYBBBB" status="0"/>
<zip code="40165" office_id="KY1056" status="1"/>
<zip code="40165" office_id="KYCCCC" status="0"/>
<zip code="40170" office_id="KY0075" status="1"/>
<zip code="40170" office_id="KYBBBB" status="0"/>
<zip code="40171" office_id="KY0075" status="1"/>
<zip code="40171" office_id="KYBBBB" status="0"/>
<zip code="40175" office_id="KY0075" status="1"/>
<zip code="40175" office_id="KYBBBB" status="0"/>
<zip code="40176" office_id="KY0075" status="1"/>
<zip code="40176" office_id="KYBBBB" status="0"/>
<zip code="40177" office_id="KY0075" status="1"/>
<zip code="40177" office_id="KYBBBB" status="0"/>
<zip code="40178" office_id="KY0075" status="1"/>
<zip code="40178" office_id="KYBBBB" status="0"/>
<zip code="40201" office_id="KY1056" status="1"/>
<zip code="40201" office_id="KYCCCC" status="0"/>
<zip code="40202" office_id="KY1056" status="1"/>
<zip code="40202" office_id="KYCCCC" status="0"/>
<zip code="40203" office_id="KY1056" status="1"/>
<zip code="40203" office_id="KYCCCC" status="0"/>
<zip code="40204" office_id="KY1056" status="1"/>
<zip code="40204" office_id="KYCCCC" status="0"/>
<zip code="40205" office_id="KY1056" status="1"/>
<zip code="40205" office_id="KYCCCC" status="0"/>
<zip code="40206" office_id="KY1056" status="1"/>
<zip code="40206" office_id="KYCCCC" status="0"/>
<zip code="40207" office_id="KY1056" status="1"/>
<zip code="40207" office_id="KYCCCC" status="0"/>
<zip code="40208" office_id="KY1056" status="1"/>
<zip code="40208" office_id="KYCCCC" status="0"/>
<zip code="40209" office_id="KY1056" status="1"/>
<zip code="40209" office_id="KYCCCC" status="0"/>
<zip code="40210" office_id="KY1056" status="1"/>
<zip code="40210" office_id="KYCCCC" status="0"/>
<zip code="40211" office_id="KY1056" status="1"/>
<zip code="40211" office_id="KYCCCC" status="0"/>
<zip code="40212" office_id="KY1056" status="1"/>
<zip code="40212" office_id="KYCCCC" status="0"/>
<zip code="40213" office_id="KY1056" status="1"/>
<zip code="40213" office_id="KYCCCC" status="0"/>
<zip code="40214" office_id="KY1056" status="1"/>
<zip code="40214" office_id="KYCCCC" status="0"/>
<zip code="40215" office_id="KY1056" status="1"/>
<zip code="40215" office_id="KYCCCC" status="0"/>
<zip code="40216" office_id="KY1056" status="1"/>
<zip code="40216" office_id="KYCCCC" status="0"/>
<zip code="40217" office_id="KY1056" status="1"/>
<zip code="40217" office_id="KYCCCC" status="0"/>
<zip code="40218" office_id="KY1056" status="1"/>
<zip code="40218" office_id="KYCCCC" status="0"/>
<zip code="40219" office_id="KY1056" status="1"/>
<zip code="40219" office_id="KYCCCC" status="0"/>
<zip code="40220" office_id="KY1056" status="1"/>
<zip code="40220" office_id="KYCCCC" status="0"/>
<zip code="40221" office_id="KY1056" status="1"/>
<zip code="40221" office_id="KYCCCC" status="0"/>
<zip code="40222" office_id="KY1056" status="1"/>
<zip code="40222" office_id="KYCCCC" status="0"/>
<zip code="40223" office_id="KY1056" status="1"/>
<zip code="40223" office_id="KYCCCC" status="0"/>
<zip code="40224" office_id="KY1056" status="1"/>
<zip code="40224" office_id="KYCCCC" status="0"/>
<zip code="40225" office_id="KY1056" status="1"/>
<zip code="40225" office_id="KYCCCC" status="0"/>
<zip code="40228" office_id="KY1056" status="1"/>
<zip code="40228" office_id="KYCCCC" status="0"/>
<zip code="40229" office_id="KY1056" status="1"/>
<zip code="40229" office_id="KYCCCC" status="0"/>
<zip code="40231" office_id="KY1056" status="1"/>
<zip code="40231" office_id="KYCCCC" status="0"/>
<zip code="40232" office_id="KY1056" status="1"/>
<zip code="40232" office_id="KYCCCC" status="0"/>
<zip code="40233" office_id="KY1056" status="1"/>
<zip code="40233" office_id="KYCCCC" status="0"/>
<zip code="40241" office_id="KY1056" status="1"/>
<zip code="40241" office_id="KYCCCC" status="0"/>
<zip code="40242" office_id="KY1056" status="1"/>
<zip code="40242" office_id="KYCCCC" status="0"/>
<zip code="40243" office_id="KY1056" status="1"/>
<zip code="40243" office_id="KYCCCC" status="0"/>
<zip code="40245" office_id="KY1056" status="1"/>
<zip code="40245" office_id="KYCCCC" status="0"/>
<zip code="40250" office_id="KY1056" status="1"/>
<zip code="40250" office_id="KYCCCC" status="0"/>
<zip code="40251" office_id="KY1056" status="1"/>
<zip code="40251" office_id="KYCCCC" status="0"/>
<zip code="40252" office_id="KY1056" status="1"/>
<zip code="40252" office_id="KYCCCC" status="0"/>
<zip code="40253" office_id="KY1056" status="1"/>
<zip code="40253" office_id="KYCCCC" status="0"/>
<zip code="40255" office_id="KY1056" status="1"/>
<zip code="40255" office_id="KYCCCC" status="0"/>
<zip code="40256" office_id="KY1056" status="1"/>
<zip code="40256" office_id="KYCCCC" status="0"/>
<zip code="40257" office_id="KY1056" status="1"/>
<zip code="40257" office_id="KYCCCC" status="0"/>
<zip code="40258" office_id="KY1056" status="1"/>
<zip code="40258" office_id="KYCCCC" status="0"/>
<zip code="40259" office_id="KY1056" status="1"/>
<zip code="40259" office_id="KYCCCC" status="0"/>
<zip code="40261" office_id="KY1056" status="1"/>
<zip code="40261" office_id="KYCCCC" status="0"/>
<zip code="40266" office_id="KY1056" status="1"/>
<zip code="40266" office_id="KYCCCC" status="0"/>
<zip code="40268" office_id="KY1056" status="1"/>
<zip code="40268" office_id="KYCCCC" status="0"/>
<zip code="40269" office_id="KY1056" status="1"/>
<zip code="40269" office_id="KYCCCC" status="0"/>
<zip code="40270" office_id="KY1056" status="1"/>
<zip code="40270" office_id="KYCCCC" status="0"/>
<zip code="40272" office_id="KY1056" status="1"/>
<zip code="40272" office_id="KYCCCC" status="0"/>
<zip code="40280" office_id="KY1056" status="1"/>
<zip code="40280" office_id="KYCCCC" status="0"/>
<zip code="40281" office_id="KY1056" status="1"/>
<zip code="40281" office_id="KYCCCC" status="0"/>
<zip code="40282" office_id="KY1056" status="1"/>
<zip code="40282" office_id="KYCCCC" status="0"/>
<zip code="40283" office_id="KY1056" status="1"/>
<zip code="40283" office_id="KYCCCC" status="0"/>
<zip code="40285" office_id="KY1056" status="1"/>
<zip code="40285" office_id="KYCCCC" status="0"/>
<zip code="40287" office_id="KY1056" status="1"/>
<zip code="40287" office_id="KYCCCC" status="0"/>
<zip code="40289" office_id="KY1056" status="1"/>
<zip code="40289" office_id="KYCCCC" status="0"/>
<zip code="40290" office_id="KY1056" status="1"/>
<zip code="40290" office_id="KYCCCC" status="0"/>
<zip code="40291" office_id="KY1056" status="1"/>
<zip code="40291" office_id="KYCCCC" status="0"/>
<zip code="40292" office_id="KY1056" status="1"/>
<zip code="40292" office_id="KYCCCC" status="0"/>
<zip code="40293" office_id="KY1056" status="1"/>
<zip code="40293" office_id="KYCCCC" status="0"/>
<zip code="40294" office_id="KY1056" status="1"/>
<zip code="40294" office_id="KYCCCC" status="0"/>
<zip code="40295" office_id="KY1056" status="1"/>
<zip code="40295" office_id="KYCCCC" status="0"/>
<zip code="40296" office_id="KY1056" status="1"/>
<zip code="40296" office_id="KYCCCC" status="0"/>
<zip code="40297" office_id="KY1056" status="1"/>
<zip code="40297" office_id="KYCCCC" status="0"/>
<zip code="40298" office_id="KY1056" status="1"/>
<zip code="40298" office_id="KYCCCC" status="0"/>
<zip code="40299" office_id="KY1056" status="1"/>
<zip code="40299" office_id="KYCCCC" status="0"/>
<zip code="40309" office_id="KY0063" status="1"/>
<zip code="40309" office_id="KYIIII" status="0"/>
<zip code="40310" office_id="KY0065" status="1"/>
<zip code="40310" office_id="KYIIII" status="0"/>
<zip code="40311" office_id="KY0059" status="1"/>
<zip code="40311" office_id="KYIIII" status="0"/>
<zip code="40312" office_id="KY0063" status="1"/>
<zip code="40312" office_id="KYIIII" status="0"/>
<zip code="40313" office_id="KY0067" status="1"/>
<zip code="40313" office_id="KYFFFF" status="0"/>
<zip code="40316" office_id="KY0067" status="1"/>
<zip code="40316" office_id="KYGGGG" status="0"/>
<zip code="40317" office_id="KY0067" status="1"/>
<zip code="40317" office_id="KYFFFF" status="0"/>
<zip code="40319" office_id="KY0067" status="1"/>
<zip code="40319" office_id="KYFFFF" status="0"/>
<zip code="40320" office_id="KY0063" status="1"/>
<zip code="40320" office_id="KYIIII" status="0"/>
<zip code="40322" office_id="KY0067" status="1"/>
<zip code="40322" office_id="KYGGGG" status="0"/>
<zip code="40324" office_id="KY0059" status="1"/>
<zip code="40324" office_id="KYIIII" status="0"/>
<zip code="40328" office_id="KY0075" status="1"/>
<zip code="40328" office_id="KYBBBB" status="0"/>
<zip code="40329" office_id="KY0067" status="1"/>
<zip code="40329" office_id="KYFFFF" status="0"/>
<zip code="40330" office_id="KY0065" status="1"/>
<zip code="40330" office_id="KYIIII" status="0"/>
<zip code="40334" office_id="KY0067" status="1"/>
<zip code="40334" office_id="KYFFFF" status="0"/>
<zip code="40336" office_id="KY0063" status="1"/>
<zip code="40336" office_id="KYIIII" status="0"/>
<zip code="40337" office_id="KY0067" status="1"/>
<zip code="40337" office_id="KYFFFF" status="0"/>
<zip code="40339" office_id="KY0060" status="1"/>
<zip code="40339" office_id="KYIIII" status="0"/>
<zip code="40340" office_id="KY0060" status="1"/>
<zip code="40340" office_id="KYIIII" status="0"/>
<zip code="40341" office_id="KY0064" status="1"/>
<zip code="40341" office_id="KYHHHH" status="0"/>
<zip code="40342" office_id="KY0059" status="1"/>
<zip code="40342" office_id="KYIIII" status="0"/>
<zip code="40345" office_id="KY0067" status="1"/>
<zip code="40345" office_id="KYGGGG" status="0"/>
<zip code="40346" office_id="KY0067" status="1"/>
<zip code="40346" office_id="KYGGGG" status="0"/>
<zip code="40347" office_id="KY0059" status="1"/>
<zip code="40347" office_id="KYIIII" status="0"/>
<zip code="40348" office_id="KY0059" status="1"/>
<zip code="40348" office_id="KYIIII" status="0"/>
<zip code="40350" office_id="KY0059" status="1"/>
<zip code="40350" office_id="KYIIII" status="0"/>
<zip code="40351" office_id="KY0067" status="1"/>
<zip code="40351" office_id="KYFFFF" status="0"/>
<zip code="40353" office_id="KY0067" status="1"/>
<zip code="40353" office_id="KYFFFF" status="0"/>
<zip code="40355" office_id="KY0057" status="1"/>
<zip code="40355" office_id="KYEEEE" status="0"/>
<zip code="40356" office_id="KY0060" status="1"/>
<zip code="40356" office_id="KYIIII" status="0"/>
<zip code="40357" office_id="KY0059" status="1"/>
<zip code="40357" office_id="KYIIII" status="0"/>
<zip code="40358" office_id="KY0067" status="1"/>
<zip code="40358" office_id="KYFFFF" status="0"/>
<zip code="40359" office_id="KY0057" status="1"/>
<zip code="40359" office_id="KYEEEE" status="0"/>
<zip code="40360" office_id="KY0067" status="1"/>
<zip code="40360" office_id="KYFFFF" status="0"/>
<zip code="40361" office_id="KY0059" status="1"/>
<zip code="40361" office_id="KYIIII" status="0"/>
<zip code="40362" office_id="KY0059" status="1"/>
<zip code="40362" office_id="KYIIII" status="0"/>
<zip code="40363" office_id="KY0057" status="1"/>
<zip code="40363" office_id="KYEEEE" status="0"/>
<zip code="40365" office_id="KY0067" status="1"/>
<zip code="40365" office_id="KYGGGG" status="0"/>
<zip code="40366" office_id="KY0067" status="1"/>
<zip code="40366" office_id="KYFFFF" status="0"/>
<zip code="40370" office_id="KY0059" status="1"/>
<zip code="40370" office_id="KYIIII" status="0"/>
<zip code="40371" office_id="KY0067" status="1"/>
<zip code="40371" office_id="KYFFFF" status="0"/>
<zip code="40372" office_id="KY0065" status="1"/>
<zip code="40372" office_id="KYIIII" status="0"/>
<zip code="40374" office_id="KY0067" status="1"/>
<zip code="40374" office_id="KYFFFF" status="0"/>
<zip code="40376" office_id="KY0063" status="1"/>
<zip code="40376" office_id="KYIIII" status="0"/>
<zip code="40379" office_id="KY0059" status="1"/>
<zip code="40379" office_id="KYIIII" status="0"/>
<zip code="40380" office_id="KY0063" status="1"/>
<zip code="40380" office_id="KYIIII" status="0"/>
<zip code="40383" office_id="KY0059" status="1"/>
<zip code="40383" office_id="KYIIII" status="0"/>
<zip code="40384" office_id="KY0059" status="1"/>
<zip code="40384" office_id="KYIIII" status="0"/>
<zip code="40385" office_id="KY0063" status="1"/>
<zip code="40385" office_id="KYIIII" status="0"/>
<zip code="40386" office_id="KY0059" status="1"/>
<zip code="40386" office_id="KYIIII" status="0"/>
<zip code="40387" office_id="KY0067" status="1"/>
<zip code="40387" office_id="KYGGGG" status="0"/>
<zip code="40390" office_id="KY0060" status="1"/>
<zip code="40390" office_id="KYIIII" status="0"/>
<zip code="40391" office_id="KY0063" status="1"/>
<zip code="40391" office_id="KYIIII" status="0"/>
<zip code="40392" office_id="KY0063" status="1"/>
<zip code="40392" office_id="KYIIII" status="0"/>
<zip code="40402" office_id="KY0064" status="1"/>
<zip code="40402" office_id="KYGGGG" status="0"/>
<zip code="40403" office_id="KY0063" status="1"/>
<zip code="40403" office_id="KYIIII" status="0"/>
<zip code="40404" office_id="KY0063" status="1"/>
<zip code="40404" office_id="KYIIII" status="0"/>
<zip code="40405" office_id="KY0063" status="1"/>
<zip code="40405" office_id="KYIIII" status="0"/>
<zip code="40409" office_id="KY0064" status="1"/>
<zip code="40409" office_id="KYHHHH" status="0"/>
<zip code="40410" office_id="KY0065" status="1"/>
<zip code="40410" office_id="KYIIII" status="0"/>
<zip code="40415" office_id="KY0063" status="1"/>
<zip code="40415" office_id="KYIIII" status="0"/>
<zip code="40417" office_id="KY0064" status="1"/>
<zip code="40417" office_id="KYHHHH" status="0"/>
<zip code="40419" office_id="KY0065" status="1"/>
<zip code="40419" office_id="KYIIII" status="0"/>
<zip code="40420" office_id="KY0063" status="1"/>
<zip code="40420" office_id="KYIIII" status="0"/>
<zip code="40421" office_id="KY0064" status="1"/>
<zip code="40421" office_id="KYGGGG" status="0"/>
<zip code="40422" office_id="KY0065" status="1"/>
<zip code="40422" office_id="KYIIII" status="0"/>
<zip code="40423" office_id="KY0065" status="1"/>
<zip code="40423" office_id="KYIIII" status="0"/>
<zip code="40426" office_id="KY0063" status="1"/>
<zip code="40426" office_id="KYIIII" status="0"/>
<zip code="40430" office_id="KY0064" status="1"/>
<zip code="40430" office_id="KYGGGG" status="0"/>
<zip code="40434" office_id="KY0064" status="1"/>
<zip code="40434" office_id="KYGGGG" status="0"/>
<zip code="40435" office_id="KY0064" status="1"/>
<zip code="40435" office_id="KYGGGG" status="0"/>
<zip code="40437" office_id="KY0065" status="1"/>
<zip code="40437" office_id="KYIIII" status="0"/>
<zip code="40440" office_id="KY0065" status="1"/>
<zip code="40440" office_id="KYIIII" status="0"/>
<zip code="40441" office_id="KY0064" status="1"/>
<zip code="40441" office_id="KYGGGG" status="0"/>
<zip code="40442" office_id="KY0065" status="1"/>
<zip code="40442" office_id="KYIIII" status="0"/>
<zip code="40444" office_id="KY0065" status="1"/>
<zip code="40444" office_id="KYIIII" status="0"/>
<zip code="40445" office_id="KY0064" status="1"/>
<zip code="40445" office_id="KYHHHH" status="0"/>
<zip code="40446" office_id="KY0065" status="1"/>
<zip code="40446" office_id="KYIIII" status="0"/>
<zip code="40447" office_id="KY0064" status="1"/>
<zip code="40447" office_id="KYGGGG" status="0"/>
<zip code="40448" office_id="KY0065" status="1"/>
<zip code="40448" office_id="KYIIII" status="0"/>
<zip code="40452" office_id="KY0065" status="1"/>
<zip code="40452" office_id="KYIIII" status="0"/>
<zip code="40456" office_id="KY0064" status="1"/>
<zip code="40456" office_id="KYHHHH" status="0"/>
<zip code="40460" office_id="KY0064" status="1"/>
<zip code="40460" office_id="KYHHHH" status="0"/>
<zip code="40461" office_id="KY0065" status="1"/>
<zip code="40461" office_id="KYIIII" status="0"/>
<zip code="40464" office_id="KY0065" status="1"/>
<zip code="40464" office_id="KYIIII" status="0"/>
<zip code="40467" office_id="KY0064" status="1"/>
<zip code="40467" office_id="KYGGGG" status="0"/>
<zip code="40468" office_id="KY0065" status="1"/>
<zip code="40468" office_id="KYIIII" status="0"/>
<zip code="40471" office_id="KY0063" status="1"/>
<zip code="40471" office_id="KYIIII" status="0"/>
<zip code="40472" office_id="KY0063" status="1"/>
<zip code="40472" office_id="KYIIII" status="0"/>
<zip code="40473" office_id="KY0064" status="1"/>
<zip code="40473" office_id="KYHHHH" status="0"/>
<zip code="40474" office_id="KY0063" status="1"/>
<zip code="40474" office_id="KYIIII" status="0"/>
<zip code="40475" office_id="KY0063" status="1"/>
<zip code="40475" office_id="KYIIII" status="0"/>
<zip code="40476" office_id="KY0063" status="1"/>
<zip code="40476" office_id="KYIIII" status="0"/>
<zip code="40481" office_id="KY0064" status="1"/>
<zip code="40481" office_id="KYGGGG" status="0"/>
<zip code="40484" office_id="KY0065" status="1"/>
<zip code="40484" office_id="KYIIII" status="0"/>
<zip code="40486" office_id="KY0064" status="1"/>
<zip code="40486" office_id="KYGGGG" status="0"/>
<zip code="40488" office_id="KY0064" status="1"/>
<zip code="40488" office_id="KYGGGG" status="0"/>
<zip code="40489" office_id="KY0065" status="1"/>
<zip code="40489" office_id="KYIIII" status="0"/>
<zip code="40492" office_id="KY0064" status="1"/>
<zip code="40492" office_id="KYHHHH" status="0"/>
<zip code="40494" office_id="KY0064" status="1"/>
<zip code="40494" office_id="KYGGGG" status="0"/>
<zip code="40495" office_id="KY0063" status="1"/>
<zip code="40495" office_id="KYIIII" status="0"/>
<zip code="40502" office_id="KY0060" status="1"/>
<zip code="40502" office_id="KYIIII" status="0"/>
<zip code="40503" office_id="KY0060" status="1"/>
<zip code="40503" office_id="KYIIII" status="0"/>
<zip code="40504" office_id="KY0060" status="1"/>
<zip code="40504" office_id="KYIIII" status="0"/>
<zip code="40505" office_id="KY0060" status="1"/>
<zip code="40505" office_id="KYIIII" status="0"/>
<zip code="40506" office_id="KY0060" status="1"/>
<zip code="40506" office_id="KYIIII" status="0"/>
<zip code="40507" office_id="KY0060" status="1"/>
<zip code="40507" office_id="KYIIII" status="0"/>
<zip code="40508" office_id="KY0060" status="1"/>
<zip code="40508" office_id="KYIIII" status="0"/>
<zip code="40509" office_id="KY0060" status="1"/>
<zip code="40509" office_id="KYIIII" status="0"/>
<zip code="40510" office_id="KY0060" status="1"/>
<zip code="40510" office_id="KYIIII" status="0"/>
<zip code="40511" office_id="KY0060" status="1"/>
<zip code="40511" office_id="KYIIII" status="0"/>
<zip code="40512" office_id="KY0060" status="1"/>
<zip code="40512" office_id="KYIIII" status="0"/>
<zip code="40513" office_id="KY0060" status="1"/>
<zip code="40513" office_id="KYIIII" status="0"/>
<zip code="40514" office_id="KY0060" status="1"/>
<zip code="40514" office_id="KYIIII" status="0"/>
<zip code="40515" office_id="KY0060" status="1"/>
<zip code="40515" office_id="KYIIII" status="0"/>
<zip code="40516" office_id="KY0060" status="1"/>
<zip code="40516" office_id="KYIIII" status="0"/>
<zip code="40517" office_id="KY0060" status="1"/>
<zip code="40517" office_id="KYIIII" status="0"/>
<zip code="40522" office_id="KY0060" status="1"/>
<zip code="40522" office_id="KYIIII" status="0"/>
<zip code="40523" office_id="KY0060" status="1"/>
<zip code="40523" office_id="KYIIII" status="0"/>
<zip code="40524" office_id="KY0060" status="1"/>
<zip code="40524" office_id="KYIIII" status="0"/>
<zip code="40526" office_id="KY0060" status="1"/>
<zip code="40526" office_id="KYIIII" status="0"/>
<zip code="40533" office_id="KY0060" status="1"/>
<zip code="40533" office_id="KYIIII" status="0"/>
<zip code="40536" office_id="KY0060" status="1"/>
<zip code="40536" office_id="KYIIII" status="0"/>
<zip code="40544" office_id="KY0060" status="1"/>
<zip code="40544" office_id="KYIIII" status="0"/>
<zip code="40546" office_id="KY0060" status="1"/>
<zip code="40546" office_id="KYIIII" status="0"/>
<zip code="40550" office_id="KY0060" status="1"/>
<zip code="40550" office_id="KYIIII" status="0"/>
<zip code="40555" office_id="KY0060" status="1"/>
<zip code="40555" office_id="KYIIII" status="0"/>
<zip code="40574" office_id="KY0060" status="1"/>
<zip code="40574" office_id="KYIIII" status="0"/>
<zip code="40575" office_id="KY0060" status="1"/>
<zip code="40575" office_id="KYIIII" status="0"/>
<zip code="40576" office_id="KY0060" status="1"/>
<zip code="40576" office_id="KYIIII" status="0"/>
<zip code="40577" office_id="KY0060" status="1"/>
<zip code="40577" office_id="KYIIII" status="0"/>
<zip code="40578" office_id="KY0060" status="1"/>
<zip code="40578" office_id="KYIIII" status="0"/>
<zip code="40579" office_id="KY0060" status="1"/>
<zip code="40579" office_id="KYIIII" status="0"/>
<zip code="40580" office_id="KY0060" status="1"/>
<zip code="40580" office_id="KYIIII" status="0"/>
<zip code="40581" office_id="KY0060" status="1"/>
<zip code="40581" office_id="KYIIII" status="0"/>
<zip code="40582" office_id="KY0060" status="1"/>
<zip code="40582" office_id="KYIIII" status="0"/>
<zip code="40583" office_id="KY0060" status="1"/>
<zip code="40583" office_id="KYIIII" status="0"/>
<zip code="40588" office_id="KY0060" status="1"/>
<zip code="40588" office_id="KYIIII" status="0"/>
<zip code="40591" office_id="KY0060" status="1"/>
<zip code="40591" office_id="KYIIII" status="0"/>
<zip code="40598" office_id="KY0060" status="1"/>
<zip code="40598" office_id="KYIIII" status="0"/>
<zip code="40601" office_id="KY0059" status="1"/>
<zip code="40601" office_id="KYIIII" status="0"/>
<zip code="40602" office_id="KY0059" status="1"/>
<zip code="40602" office_id="KYIIII" status="0"/>
<zip code="40603" office_id="KY0059" status="1"/>
<zip code="40603" office_id="KYIIII" status="0"/>
<zip code="40604" office_id="KY0059" status="1"/>
<zip code="40604" office_id="KYIIII" status="0"/>
<zip code="40618" office_id="KY0059" status="1"/>
<zip code="40618" office_id="KYIIII" status="0"/>
<zip code="40619" office_id="KY0059" status="1"/>
<zip code="40619" office_id="KYIIII" status="0"/>
<zip code="40620" office_id="KY0059" status="1"/>
<zip code="40620" office_id="KYIIII" status="0"/>
<zip code="40621" office_id="KY0059" status="1"/>
<zip code="40621" office_id="KYIIII" status="0"/>
<zip code="40622" office_id="KY0059" status="1"/>
<zip code="40622" office_id="KYIIII" status="0"/>
<zip code="40701" office_id="KY0064" status="1"/>
<zip code="40701" office_id="KYHHHH" status="0"/>
<zip code="40702" office_id="KY0064" status="1"/>
<zip code="40702" office_id="KYHHHH" status="0"/>
<zip code="40724" office_id="KY0064" status="1"/>
<zip code="40724" office_id="KYHHHH" status="0"/>
<zip code="40729" office_id="KY0064" status="1"/>
<zip code="40729" office_id="KYHHHH" status="0"/>
<zip code="40730" office_id="KY0064" status="1"/>
<zip code="40730" office_id="KYHHHH" status="0"/>
<zip code="40734" office_id="KY0074" status="1"/>
<zip code="40734" office_id="KYGGGG" status="0"/>
<zip code="40737" office_id="KY0064" status="1"/>
<zip code="40737" office_id="KYHHHH" status="0"/>
<zip code="40740" office_id="KY0064" status="1"/>
<zip code="40740" office_id="KYHHHH" status="0"/>
<zip code="40741" office_id="KY0064" status="1"/>
<zip code="40741" office_id="KYHHHH" status="0"/>
<zip code="40742" office_id="KY0064" status="1"/>
<zip code="40742" office_id="KYHHHH" status="0"/>
<zip code="40743" office_id="KY0064" status="1"/>
<zip code="40743" office_id="KYHHHH" status="0"/>
<zip code="40744" office_id="KY0064" status="1"/>
<zip code="40744" office_id="KYHHHH" status="0"/>
<zip code="40745" office_id="KY0064" status="1"/>
<zip code="40745" office_id="KYHHHH" status="0"/>
<zip code="40751" office_id="KY0064" status="1"/>
<zip code="40751" office_id="KYHHHH" status="0"/>
<zip code="40754" office_id="KY0064" status="1"/>
<zip code="40754" office_id="KYHHHH" status="0"/>
<zip code="40755" office_id="KY0064" status="1"/>
<zip code="40755" office_id="KYHHHH" status="0"/>
<zip code="40759" office_id="KY0064" status="1"/>
<zip code="40759" office_id="KYHHHH" status="0"/>
<zip code="40763" office_id="KY0064" status="1"/>
<zip code="40763" office_id="KYHHHH" status="0"/>
<zip code="40769" office_id="KY0064" status="1"/>
<zip code="40769" office_id="KYHHHH" status="0"/>
<zip code="40771" office_id="KY0074" status="1"/>
<zip code="40771" office_id="KYGGGG" status="0"/>
<zip code="40801" office_id="KY0064" status="1"/>
<zip code="40801" office_id="KYGGGG" status="0"/>
<zip code="40803" office_id="KY0064" status="1"/>
<zip code="40803" office_id="KYGGGG" status="0"/>
<zip code="40806" office_id="KY0064" status="1"/>
<zip code="40806" office_id="KYGGGG" status="0"/>
<zip code="40807" office_id="KY0064" status="1"/>
<zip code="40807" office_id="KYGGGG" status="0"/>
<zip code="40808" office_id="KY0064" status="1"/>
<zip code="40808" office_id="KYGGGG" status="0"/>
<zip code="40810" office_id="KY0064" status="1"/>
<zip code="40810" office_id="KYGGGG" status="0"/>
<zip code="40813" office_id="KY0074" status="1"/>
<zip code="40813" office_id="KYGGGG" status="0"/>
<zip code="40815" office_id="KY0064" status="1"/>
<zip code="40815" office_id="KYGGGG" status="0"/>
<zip code="40816" office_id="KY0064" status="1"/>
<zip code="40816" office_id="KYGGGG" status="0"/>
<zip code="40818" office_id="KY0064" status="1"/>
<zip code="40818" office_id="KYGGGG" status="0"/>
<zip code="40819" office_id="KY0064" status="1"/>
<zip code="40819" office_id="KYGGGG" status="0"/>
<zip code="40820" office_id="KY0064" status="1"/>
<zip code="40820" office_id="KYGGGG" status="0"/>
<zip code="40823" office_id="KY0064" status="1"/>
<zip code="40823" office_id="KYGGGG" status="0"/>
<zip code="40824" office_id="KY0064" status="1"/>
<zip code="40824" office_id="KYGGGG" status="0"/>
<zip code="40825" office_id="KY0064" status="1"/>
<zip code="40825" office_id="KYGGGG" status="0"/>
<zip code="40826" office_id="KY0064" status="1"/>
<zip code="40826" office_id="KYGGGG" status="0"/>
<zip code="40827" office_id="KY0064" status="1"/>
<zip code="40827" office_id="KYGGGG" status="0"/>
<zip code="40828" office_id="KY0064" status="1"/>
<zip code="40828" office_id="KYGGGG" status="0"/>
<zip code="40829" office_id="KY0064" status="1"/>
<zip code="40829" office_id="KYGGGG" status="0"/>
<zip code="40830" office_id="KY0064" status="1"/>
<zip code="40830" office_id="KYGGGG" status="0"/>
<zip code="40831" office_id="KY0064" status="1"/>
<zip code="40831" office_id="KYGGGG" status="0"/>
<zip code="40840" office_id="KY0064" status="1"/>
<zip code="40840" office_id="KYGGGG" status="0"/>
<zip code="40843" office_id="KY0064" status="1"/>
<zip code="40843" office_id="KYGGGG" status="0"/>
<zip code="40844" office_id="KY0064" status="1"/>
<zip code="40844" office_id="KYGGGG" status="0"/>
<zip code="40845" office_id="KY0074" status="1"/>
<zip code="40845" office_id="KYGGGG" status="0"/>
<zip code="40847" office_id="KY0064" status="1"/>
<zip code="40847" office_id="KYGGGG" status="0"/>
<zip code="40849" office_id="KY0064" status="1"/>
<zip code="40849" office_id="KYGGGG" status="0"/>
<zip code="40854" office_id="KY0064" status="1"/>
<zip code="40854" office_id="KYGGGG" status="0"/>
<zip code="40855" office_id="KY0064" status="1"/>
<zip code="40855" office_id="KYGGGG" status="0"/>
<zip code="40856" office_id="KY0074" status="1"/>
<zip code="40856" office_id="KYGGGG" status="0"/>
<zip code="40858" office_id="KY0064" status="1"/>
<zip code="40858" office_id="KYGGGG" status="0"/>
<zip code="40861" office_id="KY0064" status="1"/>
<zip code="40861" office_id="KYGGGG" status="0"/>
<zip code="40862" office_id="KY0064" status="1"/>
<zip code="40862" office_id="KYGGGG" status="0"/>
<zip code="40863" office_id="KY0064" status="1"/>
<zip code="40863" office_id="KYGGGG" status="0"/>
<zip code="40865" office_id="KY0064" status="1"/>
<zip code="40865" office_id="KYGGGG" status="0"/>
<zip code="40867" office_id="KY0064" status="1"/>
<zip code="40867" office_id="KYGGGG" status="0"/>
<zip code="40868" office_id="KY0064" status="1"/>
<zip code="40868" office_id="KYGGGG" status="0"/>
<zip code="40870" office_id="KY0064" status="1"/>
<zip code="40870" office_id="KYGGGG" status="0"/>
<zip code="40873" office_id="KY0064" status="1"/>
<zip code="40873" office_id="KYGGGG" status="0"/>
<zip code="40874" office_id="KY0064" status="1"/>
<zip code="40874" office_id="KYGGGG" status="0"/>
<zip code="40902" office_id="KY0074" status="1"/>
<zip code="40902" office_id="KYGGGG" status="0"/>
<zip code="40903" office_id="KY0074" status="1"/>
<zip code="40903" office_id="KYGGGG" status="0"/>
<zip code="40906" office_id="KY0074" status="1"/>
<zip code="40906" office_id="KYGGGG" status="0"/>
<zip code="40911" office_id="KY0074" status="1"/>
<zip code="40911" office_id="KYGGGG" status="0"/>
<zip code="40913" office_id="KY0074" status="1"/>
<zip code="40913" office_id="KYGGGG" status="0"/>
<zip code="40914" office_id="KY0064" status="1"/>
<zip code="40914" office_id="KYGGGG" status="0"/>
<zip code="40915" office_id="KY0074" status="1"/>
<zip code="40915" office_id="KYGGGG" status="0"/>
<zip code="40921" office_id="KY0074" status="1"/>
<zip code="40921" office_id="KYGGGG" status="0"/>
<zip code="40923" office_id="KY0074" status="1"/>
<zip code="40923" office_id="KYGGGG" status="0"/>
<zip code="40927" office_id="KY0064" status="1"/>
<zip code="40927" office_id="KYGGGG" status="0"/>
<zip code="40930" office_id="KY0074" status="1"/>
<zip code="40930" office_id="KYGGGG" status="0"/>
<zip code="40931" office_id="KY0064" status="1"/>
<zip code="40931" office_id="KYGGGG" status="0"/>
<zip code="40932" office_id="KY0064" status="1"/>
<zip code="40932" office_id="KYGGGG" status="0"/>
<zip code="40935" office_id="KY0074" status="1"/>
<zip code="40935" office_id="KYGGGG" status="0"/>
<zip code="40936" office_id="KY0064" status="1"/>
<zip code="40936" office_id="KYGGGG" status="0"/>
<zip code="40939" office_id="KY0074" status="1"/>
<zip code="40939" office_id="KYGGGG" status="0"/>
<zip code="40940" office_id="KY0074" status="1"/>
<zip code="40940" office_id="KYGGGG" status="0"/>
<zip code="40941" office_id="KY0064" status="1"/>
<zip code="40941" office_id="KYGGGG" status="0"/>
<zip code="40943" office_id="KY0074" status="1"/>
<zip code="40943" office_id="KYGGGG" status="0"/>
<zip code="40944" office_id="KY0064" status="1"/>
<zip code="40944" office_id="KYGGGG" status="0"/>
<zip code="40946" office_id="KY0074" status="1"/>
<zip code="40946" office_id="KYGGGG" status="0"/>
<zip code="40949" office_id="KY0074" status="1"/>
<zip code="40949" office_id="KYGGGG" status="0"/>
<zip code="40951" office_id="KY0064" status="1"/>
<zip code="40951" office_id="KYGGGG" status="0"/>
<zip code="40953" office_id="KY0074" status="1"/>
<zip code="40953" office_id="KYGGGG" status="0"/>
<zip code="40955" office_id="KY0074" status="1"/>
<zip code="40955" office_id="KYGGGG" status="0"/>
<zip code="40958" office_id="KY0074" status="1"/>
<zip code="40958" office_id="KYGGGG" status="0"/>
<zip code="40962" office_id="KY0064" status="1"/>
<zip code="40962" office_id="KYGGGG" status="0"/>
<zip code="40964" office_id="KY0064" status="1"/>
<zip code="40964" office_id="KYGGGG" status="0"/>
<zip code="40965" office_id="KY0074" status="1"/>
<zip code="40965" office_id="KYGGGG" status="0"/>
<zip code="40970" office_id="KY0074" status="1"/>
<zip code="40970" office_id="KYGGGG" status="0"/>
<zip code="40972" office_id="KY0064" status="1"/>
<zip code="40972" office_id="KYGGGG" status="0"/>
<zip code="40977" office_id="KY0074" status="1"/>
<zip code="40977" office_id="KYGGGG" status="0"/>
<zip code="40978" office_id="KY0064" status="1"/>
<zip code="40978" office_id="KYGGGG" status="0"/>
<zip code="40979" office_id="KY0064" status="1"/>
<zip code="40979" office_id="KYGGGG" status="0"/>
<zip code="40981" office_id="KY0070" status="1"/>
<zip code="40981" office_id="KYGGGG" status="0"/>
<zip code="40982" office_id="KY0074" status="1"/>
<zip code="40982" office_id="KYGGGG" status="0"/>
<zip code="40983" office_id="KY0064" status="1"/>
<zip code="40983" office_id="KYGGGG" status="0"/>
<zip code="40988" office_id="KY0074" status="1"/>
<zip code="40988" office_id="KYGGGG" status="0"/>
<zip code="40995" office_id="KY0074" status="1"/>
<zip code="40995" office_id="KYGGGG" status="0"/>
<zip code="40997" office_id="KY0074" status="1"/>
<zip code="40997" office_id="KYGGGG" status="0"/>
<zip code="40999" office_id="KY0074" status="1"/>
<zip code="40999" office_id="KYGGGG" status="0"/>
<zip code="41001" office_id="KY0057" status="1"/>
<zip code="41001" office_id="KYEEEE" status="0"/>
<zip code="41002" office_id="KY0062" status="1"/>
<zip code="41002" office_id="KYFFFF" status="0"/>
<zip code="41003" office_id="KY0059" status="1"/>
<zip code="41003" office_id="KY0059" status="1"/>
<zip code="41003" office_id="KYFFFF" status="0"/>
<zip code="41003" office_id="KYIIII" status="0"/>
<zip code="41004" office_id="KY0062" status="1"/>
<zip code="41004" office_id="KYFFFF" status="0"/>
<zip code="41005" office_id="KY0057" status="1"/>
<zip code="41005" office_id="KYEEEE" status="0"/>
<zip code="41006" office_id="KY0057" status="1"/>
<zip code="41006" office_id="KYEEEE" status="0"/>
<zip code="41007" office_id="KY0057" status="1"/>
<zip code="41007" office_id="KYEEEE" status="0"/>
<zip code="41008" office_id="KY0057" status="1"/>
<zip code="41008" office_id="KYEEEE" status="0"/>
<zip code="41009" office_id="KY0057" status="1"/>
<zip code="41009" office_id="KYEEEE" status="0"/>
<zip code="41010" office_id="KY0057" status="1"/>
<zip code="41010" office_id="KY0057" status="1"/>
<zip code="41010" office_id="KYAAAA" status="0"/>
<zip code="41010" office_id="KYEEEE" status="0"/>
<zip code="41011" office_id="KY0057" status="1"/>
<zip code="41011" office_id="KYEEEE" status="0"/>
<zip code="41012" office_id="KY0057" status="1"/>
<zip code="41012" office_id="KYEEEE" status="0"/>
<zip code="41014" office_id="KY0057" status="1"/>
<zip code="41014" office_id="KYEEEE" status="0"/>
<zip code="41015" office_id="KY0057" status="1"/>
<zip code="41015" office_id="KYEEEE" status="0"/>
<zip code="41016" office_id="KY0057" status="1"/>
<zip code="41016" office_id="KYEEEE" status="0"/>
<zip code="41017" office_id="KY0057" status="1"/>
<zip code="41017" office_id="KYEEEE" status="0"/>
<zip code="41018" office_id="KY0057" status="1"/>
<zip code="41018" office_id="KYEEEE" status="0"/>
<zip code="41019" office_id="KY0057" status="1"/>
<zip code="41019" office_id="KYEEEE" status="0"/>
<zip code="41022" office_id="KY0057" status="1"/>
<zip code="41022" office_id="KYEEEE" status="0"/>
<zip code="41030" office_id="KY0057" status="1"/>
<zip code="41030" office_id="KY0057" status="1"/>
<zip code="41030" office_id="KYEEEE" status="0"/>
<zip code="41031" office_id="KY0059" status="1"/>
<zip code="41031" office_id="KY0059" status="1"/>
<zip code="41031" office_id="KYFFFF" status="0"/>
<zip code="41031" office_id="KYIIII" status="0"/>
<zip code="41033" office_id="KY0057" status="1"/>
<zip code="41033" office_id="KYEEEE" status="0"/>
<zip code="41034" office_id="KY0062" status="1"/>
<zip code="41034" office_id="KYFFFF" status="0"/>
<zip code="41035" office_id="KY0057" status="1"/>
<zip code="41035" office_id="KY0057" status="1"/>
<zip code="41035" office_id="KYAAAA" status="0"/>
<zip code="41035" office_id="KYEEEE" status="0"/>
<zip code="41037" office_id="KY0062" status="1"/>
<zip code="41037" office_id="KYFFFF" status="0"/>
<zip code="41039" office_id="KY0062" status="1"/>
<zip code="41039" office_id="KYFFFF" status="0"/>
<zip code="41040" office_id="KY0057" status="1"/>
<zip code="41040" office_id="KYEEEE" status="0"/>
<zip code="41041" office_id="KY0062" status="1"/>
<zip code="41041" office_id="KYFFFF" status="0"/>
<zip code="41042" office_id="KY0057" status="1"/>
<zip code="41042" office_id="KYEEEE" status="0"/>
<zip code="41043" office_id="KY0062" status="1"/>
<zip code="41043" office_id="KYFFFF" status="0"/>
<zip code="41044" office_id="KY0062" status="1"/>
<zip code="41044" office_id="KYFFFF" status="0"/>
<zip code="41045" office_id="KY0057" status="1"/>
<zip code="41045" office_id="KYEEEE" status="0"/>
<zip code="41046" office_id="KY0057" status="1"/>
<zip code="41046" office_id="KYEEEE" status="0"/>
<zip code="41048" office_id="KY0057" status="1"/>
<zip code="41048" office_id="KYEEEE" status="0"/>
<zip code="41049" office_id="KY0062" status="1"/>
<zip code="41049" office_id="KYFFFF" status="0"/>
<zip code="41051" office_id="KY0057" status="1"/>
<zip code="41051" office_id="KYEEEE" status="0"/>
<zip code="41052" office_id="KY0057" status="1"/>
<zip code="41052" office_id="KY0057" status="1"/>
<zip code="41052" office_id="KYAAAA" status="0"/>
<zip code="41052" office_id="KYEEEE" status="0"/>
<zip code="41053" office_id="KY0057" status="1"/>
<zip code="41053" office_id="KYEEEE" status="0"/>
<zip code="41054" office_id="KY0057" status="1"/>
<zip code="41054" office_id="KY0057" status="1"/>
<zip code="41054" office_id="KYAAAA" status="0"/>
<zip code="41054" office_id="KYEEEE" status="0"/>
<zip code="41055" office_id="KY0062" status="1"/>
<zip code="41055" office_id="KYFFFF" status="0"/>
<zip code="41056" office_id="KY0062" status="1"/>
<zip code="41056" office_id="KYFFFF" status="0"/>
<zip code="41059" office_id="KY0057" status="1"/>
<zip code="41059" office_id="KYEEEE" status="0"/>
<zip code="41061" office_id="KY0062" status="1"/>
<zip code="41061" office_id="KYFFFF" status="0"/>
<zip code="41062" office_id="KY0062" status="1"/>
<zip code="41062" office_id="KYFFFF" status="0"/>
<zip code="41063" office_id="KY0057" status="1"/>
<zip code="41063" office_id="KYEEEE" status="0"/>
<zip code="41064" office_id="KY0062" status="1"/>
<zip code="41064" office_id="KYFFFF" status="0"/>
<zip code="41065" office_id="KY0062" status="1"/>
<zip code="41065" office_id="KYFFFF" status="0"/>
<zip code="41071" office_id="KY0057" status="1"/>
<zip code="41071" office_id="KYEEEE" status="0"/>
<zip code="41072" office_id="KY0057" status="1"/>
<zip code="41072" office_id="KYEEEE" status="0"/>
<zip code="41073" office_id="KY0057" status="1"/>
<zip code="41073" office_id="KYEEEE" status="0"/>
<zip code="41074" office_id="KY0057" status="1"/>
<zip code="41074" office_id="KYEEEE" status="0"/>
<zip code="41075" office_id="KY0057" status="1"/>
<zip code="41075" office_id="KYEEEE" status="0"/>
<zip code="41076" office_id="KY0057" status="1"/>
<zip code="41076" office_id="KYEEEE" status="0"/>
<zip code="41080" office_id="KY0057" status="1"/>
<zip code="41080" office_id="KYEEEE" status="0"/>
<zip code="41081" office_id="KY0062" status="1"/>
<zip code="41081" office_id="KYFFFF" status="0"/>
<zip code="41083" office_id="KY0057" status="1"/>
<zip code="41083" office_id="KYEEEE" status="0"/>
<zip code="41085" office_id="KY0057" status="1"/>
<zip code="41085" office_id="KYEEEE" status="0"/>
<zip code="41086" office_id="KY0057" status="1"/>
<zip code="41086" office_id="KYEEEE" status="0"/>
<zip code="41091" office_id="KY0057" status="1"/>
<zip code="41091" office_id="KYEEEE" status="0"/>
<zip code="41092" office_id="KY0057" status="1"/>
<zip code="41092" office_id="KYEEEE" status="0"/>
<zip code="41093" office_id="KY0062" status="1"/>
<zip code="41093" office_id="KYFFFF" status="0"/>
<zip code="41094" office_id="KY0057" status="1"/>
<zip code="41094" office_id="KYEEEE" status="0"/>
<zip code="41095" office_id="KY0057" status="1"/>
<zip code="41095" office_id="KYEEEE" status="0"/>
<zip code="41096" office_id="KY0062" status="1"/>
<zip code="41096" office_id="KYFFFF" status="0"/>
<zip code="41097" office_id="KY0057" status="1"/>
<zip code="41097" office_id="KY0057" status="1"/>
<zip code="41097" office_id="KYEEEE" status="0"/>
<zip code="41098" office_id="KY0057" status="1"/>
<zip code="41098" office_id="KYEEEE" status="0"/>
<zip code="41099" office_id="KY0057" status="1"/>
<zip code="41099" office_id="KYEEEE" status="0"/>
<zip code="41101" office_id="KY0069" status="1"/>
<zip code="41101" office_id="KYFFFF" status="0"/>
<zip code="41102" office_id="KY0069" status="1"/>
<zip code="41102" office_id="KYFFFF" status="0"/>
<zip code="41105" office_id="KY0069" status="1"/>
<zip code="41105" office_id="KYFFFF" status="0"/>
<zip code="41114" office_id="KY0069" status="1"/>
<zip code="41114" office_id="KYFFFF" status="0"/>
<zip code="41121" office_id="KY0069" status="1"/>
<zip code="41121" office_id="KYFFFF" status="0"/>
<zip code="41124" office_id="KY0069" status="1"/>
<zip code="41124" office_id="KYGGGG" status="0"/>
<zip code="41125" office_id="KY0069" status="1"/>
<zip code="41125" office_id="KYGGGG" status="0"/>
<zip code="41127" office_id="KY0062" status="1"/>
<zip code="41127" office_id="KYFFFF" status="0"/>
<zip code="41128" office_id="KY0069" status="1"/>
<zip code="41128" office_id="KYGGGG" status="0"/>
<zip code="41129" office_id="KY0069" status="1"/>
<zip code="41129" office_id="KYFFFF" status="0"/>
<zip code="41131" office_id="KY0062" status="1"/>
<zip code="41131" office_id="KYFFFF" status="0"/>
<zip code="41132" office_id="KY0069" status="1"/>
<zip code="41132" office_id="KYGGGG" status="0"/>
<zip code="41135" office_id="KY0062" status="1"/>
<zip code="41135" office_id="KYFFFF" status="0"/>
<zip code="41137" office_id="KY0062" status="1"/>
<zip code="41137" office_id="KYFFFF" status="0"/>
<zip code="41139" office_id="KY0069" status="1"/>
<zip code="41139" office_id="KYFFFF" status="0"/>
<zip code="41141" office_id="KY0062" status="1"/>
<zip code="41141" office_id="KYFFFF" status="0"/>
<zip code="41142" office_id="KY0069" status="1"/>
<zip code="41142" office_id="KYGGGG" status="0"/>
<zip code="41143" office_id="KY0069" status="1"/>
<zip code="41143" office_id="KYGGGG" status="0"/>
<zip code="41144" office_id="KY0069" status="1"/>
<zip code="41144" office_id="KYFFFF" status="0"/>
<zip code="41146" office_id="KY0069" status="1"/>
<zip code="41146" office_id="KYGGGG" status="0"/>
<zip code="41149" office_id="KY0069" status="1"/>
<zip code="41149" office_id="KYGGGG" status="0"/>
<zip code="41150" office_id="KY0069" status="1"/>
<zip code="41150" office_id="KYGGGG" status="0"/>
<zip code="41156" office_id="KY0069" status="1"/>
<zip code="41156" office_id="KYFFFF" status="0"/>
<zip code="41159" office_id="KY0069" status="1"/>
<zip code="41159" office_id="KYGGGG" status="0"/>
<zip code="41160" office_id="KY0069" status="1"/>
<zip code="41160" office_id="KYGGGG" status="0"/>
<zip code="41163" office_id="KY0069" status="1"/>
<zip code="41163" office_id="KYFFFF" status="0"/>
<zip code="41164" office_id="KY0069" status="1"/>
<zip code="41164" office_id="KYGGGG" status="0"/>
<zip code="41166" office_id="KY0062" status="1"/>
<zip code="41166" office_id="KYFFFF" status="0"/>
<zip code="41168" office_id="KY0069" status="1"/>
<zip code="41168" office_id="KYFFFF" status="0"/>
<zip code="41169" office_id="KY0069" status="1"/>
<zip code="41169" office_id="KYFFFF" status="0"/>
<zip code="41170" office_id="KY0062" status="1"/>
<zip code="41170" office_id="KYFFFF" status="0"/>
<zip code="41171" office_id="KY0069" status="1"/>
<zip code="41171" office_id="KYGGGG" status="0"/>
<zip code="41173" office_id="KY0069" status="1"/>
<zip code="41173" office_id="KYGGGG" status="0"/>
<zip code="41174" office_id="KY0069" status="1"/>
<zip code="41174" office_id="KYFFFF" status="0"/>
<zip code="41175" office_id="KY0069" status="1"/>
<zip code="41175" office_id="KYFFFF" status="0"/>
<zip code="41177" office_id="KY0069" status="1"/>
<zip code="41177" office_id="KYGGGG" status="0"/>
<zip code="41179" office_id="KY0062" status="1"/>
<zip code="41179" office_id="KYFFFF" status="0"/>
<zip code="41180" office_id="KY0069" status="1"/>
<zip code="41180" office_id="KYGGGG" status="0"/>
<zip code="41181" office_id="KY0069" status="1"/>
<zip code="41181" office_id="KYGGGG" status="0"/>
<zip code="41183" office_id="KY0069" status="1"/>
<zip code="41183" office_id="KYFFFF" status="0"/>
<zip code="41189" office_id="KY0062" status="1"/>
<zip code="41189" office_id="KYFFFF" status="0"/>
<zip code="41201" office_id="KY0069" status="1"/>
<zip code="41201" office_id="KYGGGG" status="0"/>
<zip code="41203" office_id="KY0070" status="1"/>
<zip code="41203" office_id="KYGGGG" status="0"/>
<zip code="41204" office_id="KY0070" status="1"/>
<zip code="41204" office_id="KYGGGG" status="0"/>
<zip code="41211" office_id="KY0069" status="1"/>
<zip code="41211" office_id="KYGGGG" status="0"/>
<zip code="41214" office_id="KY0070" status="1"/>
<zip code="41214" office_id="KYGGGG" status="0"/>
<zip code="41215" office_id="KY0070" status="1"/>
<zip code="41215" office_id="KYGGGG" status="0"/>
<zip code="41216" office_id="KY0070" status="1"/>
<zip code="41216" office_id="KYGGGG" status="0"/>
<zip code="41219" office_id="KY0070" status="1"/>
<zip code="41219" office_id="KYGGGG" status="0"/>
<zip code="41222" office_id="KY0070" status="1"/>
<zip code="41222" office_id="KYGGGG" status="0"/>
<zip code="41224" office_id="KY0070" status="1"/>
<zip code="41224" office_id="KYGGGG" status="0"/>
<zip code="41226" office_id="KY0070" status="1"/>
<zip code="41226" office_id="KYGGGG" status="0"/>
<zip code="41228" office_id="KY0070" status="1"/>
<zip code="41228" office_id="KYGGGG" status="0"/>
<zip code="41230" office_id="KY0069" status="1"/>
<zip code="41230" office_id="KYGGGG" status="0"/>
<zip code="41231" office_id="KY0070" status="1"/>
<zip code="41231" office_id="KYGGGG" status="0"/>
<zip code="41232" office_id="KY0069" status="1"/>
<zip code="41232" office_id="KYGGGG" status="0"/>
<zip code="41234" office_id="KY0070" status="1"/>
<zip code="41234" office_id="KYGGGG" status="0"/>
<zip code="41237" office_id="KY0070" status="1"/>
<zip code="41237" office_id="KYGGGG" status="0"/>
<zip code="41238" office_id="KY0070" status="1"/>
<zip code="41238" office_id="KYGGGG" status="0"/>
<zip code="41240" office_id="KY0070" status="1"/>
<zip code="41240" office_id="KYGGGG" status="0"/>
<zip code="41250" office_id="KY0070" status="1"/>
<zip code="41250" office_id="KYGGGG" status="0"/>
<zip code="41254" office_id="KY0070" status="1"/>
<zip code="41254" office_id="KYGGGG" status="0"/>
<zip code="41255" office_id="KY0070" status="1"/>
<zip code="41255" office_id="KYGGGG" status="0"/>
<zip code="41256" office_id="KY0070" status="1"/>
<zip code="41256" office_id="KYGGGG" status="0"/>
<zip code="41257" office_id="KY0070" status="1"/>
<zip code="41257" office_id="KYGGGG" status="0"/>
<zip code="41258" office_id="KY0070" status="1"/>
<zip code="41258" office_id="KYGGGG" status="0"/>
<zip code="41260" office_id="KY0070" status="1"/>
<zip code="41260" office_id="KYGGGG" status="0"/>
<zip code="41261" office_id="KY0070" status="1"/>
<zip code="41261" office_id="KYGGGG" status="0"/>
<zip code="41262" office_id="KY0070" status="1"/>
<zip code="41262" office_id="KYGGGG" status="0"/>
<zip code="41263" office_id="KY0070" status="1"/>
<zip code="41263" office_id="KYGGGG" status="0"/>
<zip code="41264" office_id="KY0069" status="1"/>
<zip code="41264" office_id="KYGGGG" status="0"/>
<zip code="41265" office_id="KY0070" status="1"/>
<zip code="41265" office_id="KYGGGG" status="0"/>
<zip code="41266" office_id="KY0070" status="1"/>
<zip code="41266" office_id="KYGGGG" status="0"/>
<zip code="41267" office_id="KY0070" status="1"/>
<zip code="41267" office_id="KYGGGG" status="0"/>
<zip code="41268" office_id="KY0070" status="1"/>
<zip code="41268" office_id="KYGGGG" status="0"/>
<zip code="41269" office_id="KY0070" status="1"/>
<zip code="41269" office_id="KYGGGG" status="0"/>
<zip code="41271" office_id="KY0070" status="1"/>
<zip code="41271" office_id="KYGGGG" status="0"/>
<zip code="41274" office_id="KY0070" status="1"/>
<zip code="41274" office_id="KYGGGG" status="0"/>
<zip code="41301" office_id="KY0070" status="1"/>
<zip code="41301" office_id="KY0070" status="1"/>
<zip code="41301" office_id="KYGGGG" status="0"/>
<zip code="41301" office_id="KYGGGG" status="0"/>
<zip code="41307" office_id="KY0064" status="1"/>
<zip code="41307" office_id="KYGGGG" status="0"/>
<zip code="41310" office_id="KY0070" status="1"/>
<zip code="41310" office_id="KYGGGG" status="0"/>
<zip code="41311" office_id="KY0070" status="1"/>
<zip code="41311" office_id="KYGGGG" status="0"/>
<zip code="41313" office_id="KY0070" status="1"/>
<zip code="41313" office_id="KY0070" status="1"/>
<zip code="41313" office_id="KYGGGG" status="0"/>
<zip code="41313" office_id="KYGGGG" status="0"/>
<zip code="41314" office_id="KY0070" status="1"/>
<zip code="41314" office_id="KYGGGG" status="0"/>
<zip code="41315" office_id="KY0070" status="1"/>
<zip code="41315" office_id="KY0070" status="1"/>
<zip code="41315" office_id="KYGGGG" status="0"/>
<zip code="41315" office_id="KYGGGG" status="0"/>
<zip code="41317" office_id="KY0070" status="1"/>
<zip code="41317" office_id="KYGGGG" status="0"/>
<zip code="41323" office_id="KY0070" status="1"/>
<zip code="41323" office_id="KYGGGG" status="0"/>
<zip code="41328" office_id="KY0070" status="1"/>
<zip code="41328" office_id="KYGGGG" status="0"/>
<zip code="41331" office_id="KY0070" status="1"/>
<zip code="41331" office_id="KYGGGG" status="0"/>
<zip code="41332" office_id="KY0070" status="1"/>
<zip code="41332" office_id="KY0070" status="1"/>
<zip code="41332" office_id="KYGGGG" status="0"/>
<zip code="41332" office_id="KYGGGG" status="0"/>
<zip code="41333" office_id="KY0070" status="1"/>
<zip code="41333" office_id="KYGGGG" status="0"/>
<zip code="41338" office_id="KY0070" status="1"/>
<zip code="41338" office_id="KYGGGG" status="0"/>
<zip code="41339" office_id="KY0064" status="1"/>
<zip code="41339" office_id="KYGGGG" status="0"/>
<zip code="41340" office_id="KY0070" status="1"/>
<zip code="41340" office_id="KYGGGG" status="0"/>
<zip code="41342" office_id="KY0070" status="1"/>
<zip code="41342" office_id="KY0070" status="1"/>
<zip code="41342" office_id="KYGGGG" status="0"/>
<zip code="41342" office_id="KYGGGG" status="0"/>
<zip code="41343" office_id="KY0070" status="1"/>
<zip code="41343" office_id="KYGGGG" status="0"/>
<zip code="41344" office_id="KY0070" status="1"/>
<zip code="41344" office_id="KYGGGG" status="0"/>
<zip code="41346" office_id="KY0070" status="1"/>
<zip code="41346" office_id="KYGGGG" status="0"/>
<zip code="41347" office_id="KY0070" status="1"/>
<zip code="41347" office_id="KYGGGG" status="0"/>
<zip code="41348" office_id="KY0070" status="1"/>
<zip code="41348" office_id="KYGGGG" status="0"/>
<zip code="41351" office_id="KY0070" status="1"/>
<zip code="41351" office_id="KYGGGG" status="0"/>
<zip code="41352" office_id="KY0067" status="1"/>
<zip code="41352" office_id="KYGGGG" status="0"/>
<zip code="41357" office_id="KY0070" status="1"/>
<zip code="41357" office_id="KYGGGG" status="0"/>
<zip code="41358" office_id="KY0070" status="1"/>
<zip code="41358" office_id="KYGGGG" status="0"/>
<zip code="41360" office_id="KY0070" status="1"/>
<zip code="41360" office_id="KY0070" status="1"/>
<zip code="41360" office_id="KYGGGG" status="0"/>
<zip code="41360" office_id="KYGGGG" status="0"/>
<zip code="41362" office_id="KY0070" status="1"/>
<zip code="41362" office_id="KYGGGG" status="0"/>
<zip code="41363" office_id="KY0070" status="1"/>
<zip code="41363" office_id="KYGGGG" status="0"/>
<zip code="41364" office_id="KY0070" status="1"/>
<zip code="41364" office_id="KYGGGG" status="0"/>
<zip code="41365" office_id="KY0070" status="1"/>
<zip code="41365" office_id="KY0070" status="1"/>
<zip code="41365" office_id="KYGGGG" status="0"/>
<zip code="41365" office_id="KYGGGG" status="0"/>
<zip code="41366" office_id="KY0070" status="1"/>
<zip code="41366" office_id="KYGGGG" status="0"/>
<zip code="41367" office_id="KY0070" status="1"/>
<zip code="41367" office_id="KYGGGG" status="0"/>
<zip code="41368" office_id="KY0070" status="1"/>
<zip code="41368" office_id="KYGGGG" status="0"/>
<zip code="41377" office_id="KY0070" status="1"/>
<zip code="41377" office_id="KYGGGG" status="0"/>
<zip code="41378" office_id="KY0070" status="1"/>
<zip code="41378" office_id="KYGGGG" status="0"/>
<zip code="41385" office_id="KY0070" status="1"/>
<zip code="41385" office_id="KYGGGG" status="0"/>
<zip code="41386" office_id="KY0070" status="1"/>
<zip code="41386" office_id="KYGGGG" status="0"/>
<zip code="41390" office_id="KY0070" status="1"/>
<zip code="41390" office_id="KYGGGG" status="0"/>
<zip code="41396" office_id="KY0070" status="1"/>
<zip code="41396" office_id="KYGGGG" status="0"/>
<zip code="41397" office_id="KY0070" status="1"/>
<zip code="41397" office_id="KYGGGG" status="0"/>
<zip code="41406" office_id="KY0067" status="1"/>
<zip code="41406" office_id="KYGGGG" status="0"/>
<zip code="41408" office_id="KY0067" status="1"/>
<zip code="41408" office_id="KYGGGG" status="0"/>
<zip code="41409" office_id="KY0070" status="1"/>
<zip code="41409" office_id="KYGGGG" status="0"/>
<zip code="41410" office_id="KY0070" status="1"/>
<zip code="41410" office_id="KYGGGG" status="0"/>
<zip code="41413" office_id="KY0067" status="1"/>
<zip code="41413" office_id="KYGGGG" status="0"/>
<zip code="41417" office_id="KY0067" status="1"/>
<zip code="41417" office_id="KYGGGG" status="0"/>
<zip code="41419" office_id="KY0070" status="1"/>
<zip code="41419" office_id="KYGGGG" status="0"/>
<zip code="41421" office_id="KY0067" status="1"/>
<zip code="41421" office_id="KYGGGG" status="0"/>
<zip code="41422" office_id="KY0070" status="1"/>
<zip code="41422" office_id="KYGGGG" status="0"/>
<zip code="41425" office_id="KY0067" status="1"/>
<zip code="41425" office_id="KYGGGG" status="0"/>
<zip code="41426" office_id="KY0070" status="1"/>
<zip code="41426" office_id="KYGGGG" status="0"/>
<zip code="41427" office_id="KY0070" status="1"/>
<zip code="41427" office_id="KYGGGG" status="0"/>
<zip code="41430" office_id="KY0070" status="1"/>
<zip code="41430" office_id="KYGGGG" status="0"/>
<zip code="41433" office_id="KY0070" status="1"/>
<zip code="41433" office_id="KYGGGG" status="0"/>
<zip code="41443" office_id="KY0067" status="1"/>
<zip code="41443" office_id="KYGGGG" status="0"/>
<zip code="41444" office_id="KY0070" status="1"/>
<zip code="41444" office_id="KYGGGG" status="0"/>
<zip code="41447" office_id="KY0067" status="1"/>
<zip code="41447" office_id="KYGGGG" status="0"/>
<zip code="41451" office_id="KY0067" status="1"/>
<zip code="41451" office_id="KYGGGG" status="0"/>
<zip code="41452" office_id="KY0070" status="1"/>
<zip code="41452" office_id="KYGGGG" status="0"/>
<zip code="41456" office_id="KY0067" status="1"/>
<zip code="41456" office_id="KYGGGG" status="0"/>
<zip code="41457" office_id="KY0067" status="1"/>
<zip code="41457" office_id="KYGGGG" status="0"/>
<zip code="41459" office_id="KY0067" status="1"/>
<zip code="41459" office_id="KYGGGG" status="0"/>
<zip code="41464" office_id="KY0070" status="1"/>
<zip code="41464" office_id="KYGGGG" status="0"/>
<zip code="41465" office_id="KY0070" status="1"/>
<zip code="41465" office_id="KYGGGG" status="0"/>
<zip code="41466" office_id="KY0070" status="1"/>
<zip code="41466" office_id="KYGGGG" status="0"/>
<zip code="41467" office_id="KY0067" status="1"/>
<zip code="41467" office_id="KYGGGG" status="0"/>
<zip code="41472" office_id="KY0067" status="1"/>
<zip code="41472" office_id="KYGGGG" status="0"/>
<zip code="41474" office_id="KY0067" status="1"/>
<zip code="41474" office_id="KYGGGG" status="0"/>
<zip code="41477" office_id="KY0067" status="1"/>
<zip code="41477" office_id="KYGGGG" status="0"/>
<zip code="41501" office_id="KY0071" status="1"/>
<zip code="41501" office_id="KYGGGG" status="0"/>
<zip code="41502" office_id="KY0071" status="1"/>
<zip code="41502" office_id="KYGGGG" status="0"/>
<zip code="41503" office_id="KY0071" status="1"/>
<zip code="41503" office_id="KYGGGG" status="0"/>
<zip code="41512" office_id="KY0071" status="1"/>
<zip code="41512" office_id="KYGGGG" status="0"/>
<zip code="41513" office_id="KY0071" status="1"/>
<zip code="41513" office_id="KYGGGG" status="0"/>
<zip code="41514" office_id="KY0071" status="1"/>
<zip code="41514" office_id="KYGGGG" status="0"/>
<zip code="41517" office_id="KY0064" status="1"/>
<zip code="41517" office_id="KYGGGG" status="0"/>
<zip code="41518" office_id="KY0071" status="1"/>
<zip code="41518" office_id="KYGGGG" status="0"/>
<zip code="41519" office_id="KY0071" status="1"/>
<zip code="41519" office_id="KYGGGG" status="0"/>
<zip code="41520" office_id="KY0071" status="1"/>
<zip code="41520" office_id="KYGGGG" status="0"/>
<zip code="41521" office_id="KY0071" status="1"/>
<zip code="41521" office_id="KYGGGG" status="0"/>
<zip code="41522" office_id="KY0071" status="1"/>
<zip code="41522" office_id="KYGGGG" status="0"/>
<zip code="41524" office_id="KY0071" status="1"/>
<zip code="41524" office_id="KYGGGG" status="0"/>
<zip code="41526" office_id="KY0071" status="1"/>
<zip code="41526" office_id="KYGGGG" status="0"/>
<zip code="41527" office_id="KY0071" status="1"/>
<zip code="41527" office_id="KYGGGG" status="0"/>
<zip code="41528" office_id="KY0071" status="1"/>
<zip code="41528" office_id="KYGGGG" status="0"/>
<zip code="41529" office_id="KY0071" status="1"/>
<zip code="41529" office_id="KYGGGG" status="0"/>
<zip code="41531" office_id="KY0071" status="1"/>
<zip code="41531" office_id="KYGGGG" status="0"/>
<zip code="41534" office_id="KY0071" status="1"/>
<zip code="41534" office_id="KYGGGG" status="0"/>
<zip code="41535" office_id="KY0071" status="1"/>
<zip code="41535" office_id="KYGGGG" status="0"/>
<zip code="41536" office_id="KY0071" status="1"/>
<zip code="41536" office_id="KYGGGG" status="0"/>
<zip code="41537" office_id="KY0064" status="1"/>
<zip code="41537" office_id="KYGGGG" status="0"/>
<zip code="41538" office_id="KY0071" status="1"/>
<zip code="41538" office_id="KYGGGG" status="0"/>
<zip code="41539" office_id="KY0071" status="1"/>
<zip code="41539" office_id="KYGGGG" status="0"/>
<zip code="41540" office_id="KY0071" status="1"/>
<zip code="41540" office_id="KYGGGG" status="0"/>
<zip code="41542" office_id="KY0071" status="1"/>
<zip code="41542" office_id="KYGGGG" status="0"/>
<zip code="41543" office_id="KY0071" status="1"/>
<zip code="41543" office_id="KYGGGG" status="0"/>
<zip code="41544" office_id="KY0071" status="1"/>
<zip code="41544" office_id="KYGGGG" status="0"/>
<zip code="41546" office_id="KY0071" status="1"/>
<zip code="41546" office_id="KYGGGG" status="0"/>
<zip code="41547" office_id="KY0071" status="1"/>
<zip code="41547" office_id="KYGGGG" status="0"/>
<zip code="41548" office_id="KY0071" status="1"/>
<zip code="41548" office_id="KYGGGG" status="0"/>
<zip code="41549" office_id="KY0071" status="1"/>
<zip code="41549" office_id="KYGGGG" status="0"/>
<zip code="41550" office_id="KY0071" status="1"/>
<zip code="41550" office_id="KYGGGG" status="0"/>
<zip code="41551" office_id="KY0071" status="1"/>
<zip code="41551" office_id="KYGGGG" status="0"/>
<zip code="41553" office_id="KY0071" status="1"/>
<zip code="41553" office_id="KYGGGG" status="0"/>
<zip code="41554" office_id="KY0071" status="1"/>
<zip code="41554" office_id="KYGGGG" status="0"/>
<zip code="41555" office_id="KY0071" status="1"/>
<zip code="41555" office_id="KYGGGG" status="0"/>
<zip code="41557" office_id="KY0071" status="1"/>
<zip code="41557" office_id="KYGGGG" status="0"/>
<zip code="41558" office_id="KY0071" status="1"/>
<zip code="41558" office_id="KYGGGG" status="0"/>
<zip code="41559" office_id="KY0071" status="1"/>
<zip code="41559" office_id="KYGGGG" status="0"/>
<zip code="41560" office_id="KY0071" status="1"/>
<zip code="41560" office_id="KYGGGG" status="0"/>
<zip code="41561" office_id="KY0071" status="1"/>
<zip code="41561" office_id="KYGGGG" status="0"/>
<zip code="41562" office_id="KY0071" status="1"/>
<zip code="41562" office_id="KYGGGG" status="0"/>
<zip code="41563" office_id="KY0071" status="1"/>
<zip code="41563" office_id="KYGGGG" status="0"/>
<zip code="41564" office_id="KY0071" status="1"/>
<zip code="41564" office_id="KYGGGG" status="0"/>
<zip code="41566" office_id="KY0071" status="1"/>
<zip code="41566" office_id="KYGGGG" status="0"/>
<zip code="41567" office_id="KY0071" status="1"/>
<zip code="41567" office_id="KYGGGG" status="0"/>
<zip code="41568" office_id="KY0071" status="1"/>
<zip code="41568" office_id="KYGGGG" status="0"/>
<zip code="41569" office_id="KY0071" status="1"/>
<zip code="41569" office_id="KYGGGG" status="0"/>
<zip code="41570" office_id="KY0071" status="1"/>
<zip code="41570" office_id="KYGGGG" status="0"/>
<zip code="41571" office_id="KY0071" status="1"/>
<zip code="41571" office_id="KYGGGG" status="0"/>
<zip code="41572" office_id="KY0071" status="1"/>
<zip code="41572" office_id="KYGGGG" status="0"/>
<zip code="41574" office_id="KY0071" status="1"/>
<zip code="41574" office_id="KYGGGG" status="0"/>
<zip code="41601" office_id="KY0070" status="1"/>
<zip code="41601" office_id="KYGGGG" status="0"/>
<zip code="41602" office_id="KY0070" status="1"/>
<zip code="41602" office_id="KYGGGG" status="0"/>
<zip code="41603" office_id="KY0070" status="1"/>
<zip code="41603" office_id="KYGGGG" status="0"/>
<zip code="41604" office_id="KY0070" status="1"/>
<zip code="41604" office_id="KYGGGG" status="0"/>
<zip code="41605" office_id="KY0070" status="1"/>
<zip code="41605" office_id="KYGGGG" status="0"/>
<zip code="41606" office_id="KY0070" status="1"/>
<zip code="41606" office_id="KYGGGG" status="0"/>
<zip code="41607" office_id="KY0070" status="1"/>
<zip code="41607" office_id="KYGGGG" status="0"/>
<zip code="41612" office_id="KY0070" status="1"/>
<zip code="41612" office_id="KYGGGG" status="0"/>
<zip code="41614" office_id="KY0070" status="1"/>
<zip code="41614" office_id="KYGGGG" status="0"/>
<zip code="41615" office_id="KY0070" status="1"/>
<zip code="41615" office_id="KYGGGG" status="0"/>
<zip code="41616" office_id="KY0070" status="1"/>
<zip code="41616" office_id="KYGGGG" status="0"/>
<zip code="41619" office_id="KY0070" status="1"/>
<zip code="41619" office_id="KYGGGG" status="0"/>
<zip code="41621" office_id="KY0070" status="1"/>
<zip code="41621" office_id="KYGGGG" status="0"/>
<zip code="41622" office_id="KY0070" status="1"/>
<zip code="41622" office_id="KYGGGG" status="0"/>
<zip code="41626" office_id="KY0070" status="1"/>
<zip code="41626" office_id="KYGGGG" status="0"/>
<zip code="41627" office_id="KY0070" status="1"/>
<zip code="41627" office_id="KYGGGG" status="0"/>
<zip code="41629" office_id="KY0070" status="1"/>
<zip code="41629" office_id="KYGGGG" status="0"/>
<zip code="41630" office_id="KY0070" status="1"/>
<zip code="41630" office_id="KYGGGG" status="0"/>
<zip code="41631" office_id="KY0070" status="1"/>
<zip code="41631" office_id="KYGGGG" status="0"/>
<zip code="41632" office_id="KY0070" status="1"/>
<zip code="41632" office_id="KYGGGG" status="0"/>
<zip code="41633" office_id="KY0070" status="1"/>
<zip code="41633" office_id="KYGGGG" status="0"/>
<zip code="41635" office_id="KY0070" status="1"/>
<zip code="41635" office_id="KYGGGG" status="0"/>
<zip code="41636" office_id="KY0070" status="1"/>
<zip code="41636" office_id="KYGGGG" status="0"/>
<zip code="41637" office_id="KY0070" status="1"/>
<zip code="41637" office_id="KYGGGG" status="0"/>
<zip code="41639" office_id="KY0070" status="1"/>
<zip code="41639" office_id="KYGGGG" status="0"/>
<zip code="41640" office_id="KY0070" status="1"/>
<zip code="41640" office_id="KYGGGG" status="0"/>
<zip code="41641" office_id="KY0070" status="1"/>
<zip code="41641" office_id="KYGGGG" status="0"/>
<zip code="41642" office_id="KY0070" status="1"/>
<zip code="41642" office_id="KYGGGG" status="0"/>
<zip code="41643" office_id="KY0070" status="1"/>
<zip code="41643" office_id="KYGGGG" status="0"/>
<zip code="41645" office_id="KY0070" status="1"/>
<zip code="41645" office_id="KYGGGG" status="0"/>
<zip code="41647" office_id="KY0070" status="1"/>
<zip code="41647" office_id="KYGGGG" status="0"/>
<zip code="41649" office_id="KY0070" status="1"/>
<zip code="41649" office_id="KYGGGG" status="0"/>
<zip code="41650" office_id="KY0070" status="1"/>
<zip code="41650" office_id="KYGGGG" status="0"/>
<zip code="41651" office_id="KY0070" status="1"/>
<zip code="41651" office_id="KYGGGG" status="0"/>
<zip code="41653" office_id="KY0070" status="1"/>
<zip code="41653" office_id="KYGGGG" status="0"/>
<zip code="41655" office_id="KY0070" status="1"/>
<zip code="41655" office_id="KYGGGG" status="0"/>
<zip code="41659" office_id="KY0070" status="1"/>
<zip code="41659" office_id="KYGGGG" status="0"/>
<zip code="41660" office_id="KY0070" status="1"/>
<zip code="41660" office_id="KYGGGG" status="0"/>
<zip code="41663" office_id="KY0070" status="1"/>
<zip code="41663" office_id="KYGGGG" status="0"/>
<zip code="41666" office_id="KY0070" status="1"/>
<zip code="41666" office_id="KYGGGG" status="0"/>
<zip code="41667" office_id="KY0070" status="1"/>
<zip code="41667" office_id="KYGGGG" status="0"/>
<zip code="41668" office_id="KY0070" status="1"/>
<zip code="41668" office_id="KYGGGG" status="0"/>
<zip code="41669" office_id="KY0070" status="1"/>
<zip code="41669" office_id="KYGGGG" status="0"/>
<zip code="41701" office_id="KY0070" status="1"/>
<zip code="41701" office_id="KYGGGG" status="0"/>
<zip code="41702" office_id="KY0070" status="1"/>
<zip code="41702" office_id="KYGGGG" status="0"/>
<zip code="41710" office_id="KY0070" status="1"/>
<zip code="41710" office_id="KYGGGG" status="0"/>
<zip code="41712" office_id="KY0070" status="1"/>
<zip code="41712" office_id="KYGGGG" status="0"/>
<zip code="41713" office_id="KY0070" status="1"/>
<zip code="41713" office_id="KYGGGG" status="0"/>
<zip code="41714" office_id="KY0064" status="1"/>
<zip code="41714" office_id="KYGGGG" status="0"/>
<zip code="41719" office_id="KY0070" status="1"/>
<zip code="41719" office_id="KYGGGG" status="0"/>
<zip code="41720" office_id="KY0070" status="1"/>
<zip code="41720" office_id="KYGGGG" status="0"/>
<zip code="41721" office_id="KY0070" status="1"/>
<zip code="41721" office_id="KYGGGG" status="0"/>
<zip code="41722" office_id="KY0070" status="1"/>
<zip code="41722" office_id="KYGGGG" status="0"/>
<zip code="41723" office_id="KY0070" status="1"/>
<zip code="41723" office_id="KYGGGG" status="0"/>
<zip code="41725" office_id="KY0070" status="1"/>
<zip code="41725" office_id="KYGGGG" status="0"/>
<zip code="41727" office_id="KY0070" status="1"/>
<zip code="41727" office_id="KYGGGG" status="0"/>
<zip code="41728" office_id="KY0064" status="1"/>
<zip code="41728" office_id="KYGGGG" status="0"/>
<zip code="41729" office_id="KY0070" status="1"/>
<zip code="41729" office_id="KYGGGG" status="0"/>
<zip code="41730" office_id="KY0064" status="1"/>
<zip code="41730" office_id="KYGGGG" status="0"/>
<zip code="41731" office_id="KY0070" status="1"/>
<zip code="41731" office_id="KYGGGG" status="0"/>
<zip code="41732" office_id="KY0064" status="1"/>
<zip code="41732" office_id="KYGGGG" status="0"/>
<zip code="41733" office_id="KY0070" status="1"/>
<zip code="41733" office_id="KYGGGG" status="0"/>
<zip code="41735" office_id="KY0070" status="1"/>
<zip code="41735" office_id="KYGGGG" status="0"/>
<zip code="41736" office_id="KY0070" status="1"/>
<zip code="41736" office_id="KYGGGG" status="0"/>
<zip code="41739" office_id="KY0070" status="1"/>
<zip code="41739" office_id="KYGGGG" status="0"/>
<zip code="41740" office_id="KY0070" status="1"/>
<zip code="41740" office_id="KYGGGG" status="0"/>
<zip code="41743" office_id="KY0070" status="1"/>
<zip code="41743" office_id="KYGGGG" status="0"/>
<zip code="41745" office_id="KY0070" status="1"/>
<zip code="41745" office_id="KYGGGG" status="0"/>
<zip code="41746" office_id="KY0070" status="1"/>
<zip code="41746" office_id="KYGGGG" status="0"/>
<zip code="41747" office_id="KY0070" status="1"/>
<zip code="41747" office_id="KYGGGG" status="0"/>
<zip code="41749" office_id="KY0064" status="1"/>
<zip code="41749" office_id="KYGGGG" status="0"/>
<zip code="41751" office_id="KY0070" status="1"/>
<zip code="41751" office_id="KYGGGG" status="0"/>
<zip code="41754" office_id="KY0070" status="1"/>
<zip code="41754" office_id="KYGGGG" status="0"/>
<zip code="41756" office_id="KY0070" status="1"/>
<zip code="41756" office_id="KYGGGG" status="0"/>
<zip code="41759" office_id="KY0070" status="1"/>
<zip code="41759" office_id="KYGGGG" status="0"/>
<zip code="41760" office_id="KY0070" status="1"/>
<zip code="41760" office_id="KYGGGG" status="0"/>
<zip code="41762" office_id="KY0064" status="1"/>
<zip code="41762" office_id="KYGGGG" status="0"/>
<zip code="41763" office_id="KY0070" status="1"/>
<zip code="41763" office_id="KYGGGG" status="0"/>
<zip code="41764" office_id="KY0064" status="1"/>
<zip code="41764" office_id="KYGGGG" status="0"/>
<zip code="41766" office_id="KY0064" status="1"/>
<zip code="41766" office_id="KYGGGG" status="0"/>
<zip code="41771" office_id="KY0070" status="1"/>
<zip code="41771" office_id="KYGGGG" status="0"/>
<zip code="41772" office_id="KY0070" status="1"/>
<zip code="41772" office_id="KYGGGG" status="0"/>
<zip code="41773" office_id="KY0070" status="1"/>
<zip code="41773" office_id="KYGGGG" status="0"/>
<zip code="41774" office_id="KY0070" status="1"/>
<zip code="41774" office_id="KYGGGG" status="0"/>
<zip code="41775" office_id="KY0064" status="1"/>
<zip code="41775" office_id="KYGGGG" status="0"/>
<zip code="41776" office_id="KY0064" status="1"/>
<zip code="41776" office_id="KYGGGG" status="0"/>
<zip code="41777" office_id="KY0064" status="1"/>
<zip code="41777" office_id="KYGGGG" status="0"/>
<zip code="41778" office_id="KY0070" status="1"/>
<zip code="41778" office_id="KYGGGG" status="0"/>
<zip code="41801" office_id="KY0070" status="1"/>
<zip code="41801" office_id="KYGGGG" status="0"/>
<zip code="41804" office_id="KY0064" status="1"/>
<zip code="41804" office_id="KYGGGG" status="0"/>
<zip code="41805" office_id="KY0070" status="1"/>
<zip code="41805" office_id="KYGGGG" status="0"/>
<zip code="41810" office_id="KY0064" status="1"/>
<zip code="41810" office_id="KYGGGG" status="0"/>
<zip code="41811" office_id="KY0064" status="1"/>
<zip code="41811" office_id="KYGGGG" status="0"/>
<zip code="41812" office_id="KY0064" status="1"/>
<zip code="41812" office_id="KYGGGG" status="0"/>
<zip code="41815" office_id="KY0064" status="1"/>
<zip code="41815" office_id="KYGGGG" status="0"/>
<zip code="41817" office_id="KY0070" status="1"/>
<zip code="41817" office_id="KYGGGG" status="0"/>
<zip code="41819" office_id="KY0064" status="1"/>
<zip code="41819" office_id="KYGGGG" status="0"/>
<zip code="41821" office_id="KY0064" status="1"/>
<zip code="41821" office_id="KYGGGG" status="0"/>
<zip code="41822" office_id="KY0070" status="1"/>
<zip code="41822" office_id="KYGGGG" status="0"/>
<zip code="41823" office_id="KY0070" status="1"/>
<zip code="41823" office_id="KYGGGG" status="0"/>
<zip code="41824" office_id="KY0064" status="1"/>
<zip code="41824" office_id="KYGGGG" status="0"/>
<zip code="41825" office_id="KY0064" status="1"/>
<zip code="41825" office_id="KYGGGG" status="0"/>
<zip code="41826" office_id="KY0064" status="1"/>
<zip code="41826" office_id="KYGGGG" status="0"/>
<zip code="41828" office_id="KY0070" status="1"/>
<zip code="41828" office_id="KYGGGG" status="0"/>
<zip code="41829" office_id="KY0064" status="1"/>
<zip code="41829" office_id="KYGGGG" status="0"/>
<zip code="41831" office_id="KY0070" status="1"/>
<zip code="41831" office_id="KYGGGG" status="0"/>
<zip code="41832" office_id="KY0064" status="1"/>
<zip code="41832" office_id="KYGGGG" status="0"/>
<zip code="41833" office_id="KY0064" status="1"/>
<zip code="41833" office_id="KYGGGG" status="0"/>
<zip code="41834" office_id="KY0070" status="1"/>
<zip code="41834" office_id="KYGGGG" status="0"/>
<zip code="41835" office_id="KY0064" status="1"/>
<zip code="41835" office_id="KYGGGG" status="0"/>
<zip code="41836" office_id="KY0070" status="1"/>
<zip code="41836" office_id="KYGGGG" status="0"/>
<zip code="41837" office_id="KY0064" status="1"/>
<zip code="41837" office_id="KYGGGG" status="0"/>
<zip code="41838" office_id="KY0064" status="1"/>
<zip code="41838" office_id="KYGGGG" status="0"/>
<zip code="41839" office_id="KY0070" status="1"/>
<zip code="41839" office_id="KYGGGG" status="0"/>
<zip code="41840" office_id="KY0064" status="1"/>
<zip code="41840" office_id="KYGGGG" status="0"/>
<zip code="41843" office_id="KY0070" status="1"/>
<zip code="41843" office_id="KYGGGG" status="0"/>
<zip code="41844" office_id="KY0070" status="1"/>
<zip code="41844" office_id="KYGGGG" status="0"/>
<zip code="41845" office_id="KY0064" status="1"/>
<zip code="41845" office_id="KYGGGG" status="0"/>
<zip code="41847" office_id="KY0070" status="1"/>
<zip code="41847" office_id="KYGGGG" status="0"/>
<zip code="41848" office_id="KY0064" status="1"/>
<zip code="41848" office_id="KYGGGG" status="0"/>
<zip code="41849" office_id="KY0064" status="1"/>
<zip code="41849" office_id="KYGGGG" status="0"/>
<zip code="41855" office_id="KY0064" status="1"/>
<zip code="41855" office_id="KYGGGG" status="0"/>
<zip code="41858" office_id="KY0064" status="1"/>
<zip code="41858" office_id="KYGGGG" status="0"/>
<zip code="41859" office_id="KY0070" status="1"/>
<zip code="41859" office_id="KYGGGG" status="0"/>
<zip code="41861" office_id="KY0070" status="1"/>
<zip code="41861" office_id="KYGGGG" status="0"/>
<zip code="41862" office_id="KY0070" status="1"/>
<zip code="41862" office_id="KYGGGG" status="0"/>
<zip code="42001" office_id="KY0050" status="1"/>
<zip code="42001" office_id="KYAAAA" status="0"/>
<zip code="42002" office_id="KY0050" status="1"/>
<zip code="42002" office_id="KYAAAA" status="0"/>
<zip code="42003" office_id="KY0050" status="1"/>
<zip code="42003" office_id="KYAAAA" status="0"/>
<zip code="42020" office_id="KY0050" status="1"/>
<zip code="42020" office_id="KYAAAA" status="0"/>
<zip code="42021" office_id="KY0050" status="1"/>
<zip code="42021" office_id="KYAAAA" status="0"/>
<zip code="42022" office_id="KY0050" status="1"/>
<zip code="42022" office_id="KYAAAA" status="0"/>
<zip code="42023" office_id="KY0050" status="1"/>
<zip code="42023" office_id="KYAAAA" status="0"/>
<zip code="42024" office_id="KY0050" status="1"/>
<zip code="42024" office_id="KYAAAA" status="0"/>
<zip code="42025" office_id="KY0050" status="1"/>
<zip code="42025" office_id="KYAAAA" status="0"/>
<zip code="42026" office_id="KY0050" status="1"/>
<zip code="42026" office_id="KYAAAA" status="0"/>
<zip code="42027" office_id="KY0050" status="1"/>
<zip code="42027" office_id="KYAAAA" status="0"/>
<zip code="42028" office_id="KY0050" status="1"/>
<zip code="42028" office_id="KYAAAA" status="0"/>
<zip code="42029" office_id="KY0050" status="1"/>
<zip code="42029" office_id="KYAAAA" status="0"/>
<zip code="42031" office_id="KY0050" status="1"/>
<zip code="42031" office_id="KYAAAA" status="0"/>
<zip code="42032" office_id="KY0050" status="1"/>
<zip code="42032" office_id="KYAAAA" status="0"/>
<zip code="42033" office_id="KY0072" status="1"/>
<zip code="42033" office_id="KYAAAA" status="0"/>
<zip code="42035" office_id="KY0050" status="1"/>
<zip code="42035" office_id="KYAAAA" status="0"/>
<zip code="42036" office_id="KY0050" status="1"/>
<zip code="42036" office_id="KYAAAA" status="0"/>
<zip code="42037" office_id="KY0072" status="1"/>
<zip code="42037" office_id="KYAAAA" status="0"/>
<zip code="42038" office_id="KY0052" status="1"/>
<zip code="42038" office_id="KYAAAA" status="0"/>
<zip code="42039" office_id="KY0050" status="1"/>
<zip code="42039" office_id="KYAAAA" status="0"/>
<zip code="42040" office_id="KY0050" status="1"/>
<zip code="42040" office_id="KYAAAA" status="0"/>
<zip code="42041" office_id="KY0050" status="1"/>
<zip code="42041" office_id="KYAAAA" status="0"/>
<zip code="42044" office_id="KY0050" status="1"/>
<zip code="42044" office_id="KYAAAA" status="0"/>
<zip code="42045" office_id="KY0050" status="1"/>
<zip code="42045" office_id="KYAAAA" status="0"/>
<zip code="42046" office_id="KY0050" status="1"/>
<zip code="42046" office_id="KYAAAA" status="0"/>
<zip code="42047" office_id="KY0050" status="1"/>
<zip code="42047" office_id="KYAAAA" status="0"/>
<zip code="42048" office_id="KY0050" status="1"/>
<zip code="42048" office_id="KYAAAA" status="0"/>
<zip code="42049" office_id="KY0050" status="1"/>
<zip code="42049" office_id="KYAAAA" status="0"/>
<zip code="42050" office_id="KY0050" status="1"/>
<zip code="42050" office_id="KYAAAA" status="0"/>
<zip code="42051" office_id="KY0050" status="1"/>
<zip code="42051" office_id="KYAAAA" status="0"/>
<zip code="42053" office_id="KY0050" status="1"/>
<zip code="42053" office_id="KYAAAA" status="0"/>
<zip code="42054" office_id="KY0050" status="1"/>
<zip code="42054" office_id="KYAAAA" status="0"/>
<zip code="42055" office_id="KY0052" status="1"/>
<zip code="42055" office_id="KYAAAA" status="0"/>
<zip code="42056" office_id="KY0050" status="1"/>
<zip code="42056" office_id="KYAAAA" status="0"/>
<zip code="42058" office_id="KY0050" status="1"/>
<zip code="42058" office_id="KYAAAA" status="0"/>
<zip code="42059" office_id="KY0050" status="1"/>
<zip code="42059" office_id="KYAAAA" status="0"/>
<zip code="42060" office_id="KY0050" status="1"/>
<zip code="42060" office_id="KYAAAA" status="0"/>
<zip code="42061" office_id="KY0050" status="1"/>
<zip code="42061" office_id="KYAAAA" status="0"/>
<zip code="42063" office_id="KY0050" status="1"/>
<zip code="42063" office_id="KYAAAA" status="0"/>
<zip code="42064" office_id="KY0072" status="1"/>
<zip code="42064" office_id="KYAAAA" status="0"/>
<zip code="42066" office_id="KY0050" status="1"/>
<zip code="42066" office_id="KYAAAA" status="0"/>
<zip code="42069" office_id="KY0050" status="1"/>
<zip code="42069" office_id="KYAAAA" status="0"/>
<zip code="42070" office_id="KY0050" status="1"/>
<zip code="42070" office_id="KYAAAA" status="0"/>
<zip code="42071" office_id="KY0050" status="1"/>
<zip code="42071" office_id="KYAAAA" status="0"/>
<zip code="42076" office_id="KY0050" status="1"/>
<zip code="42076" office_id="KYAAAA" status="0"/>
<zip code="42078" office_id="KY0050" status="1"/>
<zip code="42078" office_id="KYAAAA" status="0"/>
<zip code="42079" office_id="KY0050" status="1"/>
<zip code="42079" office_id="KYAAAA" status="0"/>
<zip code="42081" office_id="KY0050" status="1"/>
<zip code="42081" office_id="KYAAAA" status="0"/>
<zip code="42082" office_id="KY0050" status="1"/>
<zip code="42082" office_id="KYAAAA" status="0"/>
<zip code="42083" office_id="KY0050" status="1"/>
<zip code="42083" office_id="KYAAAA" status="0"/>
<zip code="42084" office_id="KY0072" status="1"/>
<zip code="42084" office_id="KYAAAA" status="0"/>
<zip code="42085" office_id="KY0050" status="1"/>
<zip code="42085" office_id="KYAAAA" status="0"/>
<zip code="42086" office_id="KY0050" status="1"/>
<zip code="42086" office_id="KYAAAA" status="0"/>
<zip code="42087" office_id="KY0050" status="1"/>
<zip code="42087" office_id="KYAAAA" status="0"/>
<zip code="42088" office_id="KY0050" status="1"/>
<zip code="42088" office_id="KYAAAA" status="0"/>
<zip code="42101" office_id="KY0055" status="1"/>
<zip code="42101" office_id="KYKKKK" status="0"/>
<zip code="42102" office_id="KY0055" status="1"/>
<zip code="42102" office_id="KYKKKK" status="0"/>
<zip code="42103" office_id="KY0055" status="1"/>
<zip code="42103" office_id="KYKKKK" status="0"/>
<zip code="42104" office_id="KY0055" status="1"/>
<zip code="42104" office_id="KYKKKK" status="0"/>
<zip code="42120" office_id="KY0055" status="1"/>
<zip code="42120" office_id="KYKKKK" status="0"/>
<zip code="42122" office_id="KY0055" status="1"/>
<zip code="42122" office_id="KYKKKK" status="0"/>
<zip code="42123" office_id="KY0073" status="1"/>
<zip code="42123" office_id="KYKKKK" status="0"/>
<zip code="42124" office_id="KY0073" status="1"/>
<zip code="42124" office_id="KYKKKK" status="0"/>
<zip code="42127" office_id="KY0073" status="1"/>
<zip code="42127" office_id="KYKKKK" status="0"/>
<zip code="42128" office_id="KY0055" status="1"/>
<zip code="42128" office_id="KYKKKK" status="0"/>
<zip code="42129" office_id="KY0073" status="1"/>
<zip code="42129" office_id="KYKKKK" status="0"/>
<zip code="42130" office_id="KY0073" status="1"/>
<zip code="42130" office_id="KYKKKK" status="0"/>
<zip code="42131" office_id="KY0073" status="1"/>
<zip code="42131" office_id="KYKKKK" status="0"/>
<zip code="42133" office_id="KY0073" status="1"/>
<zip code="42133" office_id="KYKKKK" status="0"/>
<zip code="42134" office_id="KY0055" status="1"/>
<zip code="42134" office_id="KYKKKK" status="0"/>
<zip code="42135" office_id="KY0055" status="1"/>
<zip code="42135" office_id="KYKKKK" status="0"/>
<zip code="42140" office_id="KY0073" status="1"/>
<zip code="42140" office_id="KYKKKK" status="0"/>
<zip code="42141" office_id="KY0073" status="1"/>
<zip code="42141" office_id="KYKKKK" status="0"/>
<zip code="42142" office_id="KY0073" status="1"/>
<zip code="42142" office_id="KYKKKK" status="0"/>
<zip code="42150" office_id="KY0055" status="1"/>
<zip code="42150" office_id="KYKKKK" status="0"/>
<zip code="42151" office_id="KY0073" status="1"/>
<zip code="42151" office_id="KYKKKK" status="0"/>
<zip code="42152" office_id="KY0073" status="1"/>
<zip code="42152" office_id="KYKKKK" status="0"/>
<zip code="42153" office_id="KY0055" status="1"/>
<zip code="42153" office_id="KYKKKK" status="0"/>
<zip code="42154" office_id="KY0073" status="1"/>
<zip code="42154" office_id="KYKKKK" status="0"/>
<zip code="42156" office_id="KY0073" status="1"/>
<zip code="42156" office_id="KYKKKK" status="0"/>
<zip code="42157" office_id="KY0073" status="1"/>
<zip code="42157" office_id="KYKKKK" status="0"/>
<zip code="42159" office_id="KY0055" status="1"/>
<zip code="42159" office_id="KYKKKK" status="0"/>
<zip code="42160" office_id="KY0073" status="1"/>
<zip code="42160" office_id="KYKKKK" status="0"/>
<zip code="42163" office_id="KY0055" status="1"/>
<zip code="42163" office_id="KYKKKK" status="0"/>
<zip code="42164" office_id="KY0055" status="1"/>
<zip code="42164" office_id="KYKKKK" status="0"/>
<zip code="42166" office_id="KY0073" status="1"/>
<zip code="42166" office_id="KYKKKK" status="0"/>
<zip code="42167" office_id="KY0073" status="1"/>
<zip code="42167" office_id="KYKKKK" status="0"/>
<zip code="42170" office_id="KY0055" status="1"/>
<zip code="42170" office_id="KYKKKK" status="0"/>
<zip code="42171" office_id="KY0055" status="1"/>
<zip code="42171" office_id="KYKKKK" status="0"/>
<zip code="42201" office_id="KY0055" status="1"/>
<zip code="42201" office_id="KYKKKK" status="0"/>
<zip code="42202" office_id="KY0055" status="1"/>
<zip code="42202" office_id="KYKKKK" status="0"/>
<zip code="42203" office_id="KY0052" status="1"/>
<zip code="42203" office_id="KYAAAA" status="0"/>
<zip code="42204" office_id="KY0052" status="1"/>
<zip code="42204" office_id="KYAAAA" status="0"/>
<zip code="42206" office_id="KY0055" status="1"/>
<zip code="42206" office_id="KYKKKK" status="0"/>
<zip code="42207" office_id="KY0055" status="1"/>
<zip code="42207" office_id="KYKKKK" status="0"/>
<zip code="42209" office_id="KY0055" status="1"/>
<zip code="42209" office_id="KYKKKK" status="0"/>
<zip code="42210" office_id="KY0055" status="1"/>
<zip code="42210" office_id="KYKKKK" status="0"/>
<zip code="42211" office_id="KY0052" status="1"/>
<zip code="42211" office_id="KYAAAA" status="0"/>
<zip code="42214" office_id="KY0073" status="1"/>
<zip code="42214" office_id="KYKKKK" status="0"/>
<zip code="42215" office_id="KY0052" status="1"/>
<zip code="42215" office_id="KYAAAA" status="0"/>
<zip code="42216" office_id="KY0052" status="1"/>
<zip code="42216" office_id="KYAAAA" status="0"/>
<zip code="42217" office_id="KY0052" status="1"/>
<zip code="42217" office_id="KYAAAA" status="0"/>
<zip code="42219" office_id="KY0055" status="1"/>
<zip code="42219" office_id="KYKKKK" status="0"/>
<zip code="42220" office_id="KY0052" status="1"/>
<zip code="42220" office_id="KYAAAA" status="0"/>
<zip code="42221" office_id="KY0052" status="1"/>
<zip code="42221" office_id="KYAAAA" status="0"/>
<zip code="42223" office_id="KY0052" status="1"/>
<zip code="42223" office_id="KYAAAA" status="0"/>
<zip code="42232" office_id="KY0052" status="1"/>
<zip code="42232" office_id="KYAAAA" status="0"/>
<zip code="42234" office_id="KY0052" status="1"/>
<zip code="42234" office_id="KYAAAA" status="0"/>
<zip code="42235" office_id="KY0055" status="1"/>
<zip code="42235" office_id="KYKKKK" status="0"/>
<zip code="42236" office_id="KY0052" status="1"/>
<zip code="42236" office_id="KYAAAA" status="0"/>
<zip code="42240" office_id="KY0052" status="1"/>
<zip code="42240" office_id="KYAAAA" status="0"/>
<zip code="42241" office_id="KY0052" status="1"/>
<zip code="42241" office_id="KYAAAA" status="0"/>
<zip code="42250" office_id="KY0055" status="1"/>
<zip code="42250" office_id="KYKKKK" status="0"/>
<zip code="42251" office_id="KY0055" status="1"/>
<zip code="42251" office_id="KYKKKK" status="0"/>
<zip code="42252" office_id="KY0055" status="1"/>
<zip code="42252" office_id="KYKKKK" status="0"/>
<zip code="42254" office_id="KY0052" status="1"/>
<zip code="42254" office_id="KYAAAA" status="0"/>
<zip code="42256" office_id="KY0062" status="1"/>
<zip code="42256" office_id="KYKKKK" status="0"/>
<zip code="42257" office_id="KY0055" status="1"/>
<zip code="42257" office_id="KYKKKK" status="0"/>
<zip code="42259" office_id="KY0055" status="1"/>
<zip code="42259" office_id="KYKKKK" status="0"/>
<zip code="42261" office_id="KY0055" status="1"/>
<zip code="42261" office_id="KYKKKK" status="0"/>
<zip code="42262" office_id="KY0052" status="1"/>
<zip code="42262" office_id="KYAAAA" status="0"/>
<zip code="42265" office_id="KY0055" status="1"/>
<zip code="42265" office_id="KYKKKK" status="0"/>
<zip code="42266" office_id="KY0052" status="1"/>
<zip code="42266" office_id="KYAAAA" status="0"/>
<zip code="42267" office_id="KY0055" status="1"/>
<zip code="42267" office_id="KYKKKK" status="0"/>
<zip code="42268" office_id="KY0055" status="1"/>
<zip code="42268" office_id="KYKKKK" status="0"/>
<zip code="42270" office_id="KY0055" status="1"/>
<zip code="42270" office_id="KYKKKK" status="0"/>
<zip code="42273" office_id="KY0055" status="1"/>
<zip code="42273" office_id="KYKKKK" status="0"/>
<zip code="42274" office_id="KY0055" status="1"/>
<zip code="42274" office_id="KYKKKK" status="0"/>
<zip code="42275" office_id="KY0055" status="1"/>
<zip code="42275" office_id="KYKKKK" status="0"/>
<zip code="42276" office_id="KY0055" status="1"/>
<zip code="42276" office_id="KYKKKK" status="0"/>
<zip code="42280" office_id="KY0052" status="1"/>
<zip code="42280" office_id="KYAAAA" status="0"/>
<zip code="42283" office_id="KY0055" status="1"/>
<zip code="42283" office_id="KYKKKK" status="0"/>
<zip code="42284" office_id="KY0055" status="1"/>
<zip code="42284" office_id="KYKKKK" status="0"/>
<zip code="42285" office_id="KY0055" status="1"/>
<zip code="42285" office_id="KYKKKK" status="0"/>
<zip code="42286" office_id="KY0052" status="1"/>
<zip code="42286" office_id="KYAAAA" status="0"/>
<zip code="42287" office_id="KY0055" status="1"/>
<zip code="42287" office_id="KYKKKK" status="0"/>
<zip code="42288" office_id="KY0055" status="1"/>
<zip code="42288" office_id="KYKKKK" status="0"/>
<zip code="42301" office_id="KY0054" status="1"/>
<zip code="42301" office_id="KYJJJJ" status="0"/>
<zip code="42302" office_id="KY0054" status="1"/>
<zip code="42302" office_id="KYJJJJ" status="0"/>
<zip code="42303" office_id="KY0054" status="1"/>
<zip code="42303" office_id="KYJJJJ" status="0"/>
<zip code="42304" office_id="KY0054" status="1"/>
<zip code="42304" office_id="KYJJJJ" status="0"/>
<zip code="42320" office_id="KY0054" status="1"/>
<zip code="42320" office_id="KYJJJJ" status="0"/>
<zip code="42321" office_id="KY0072" status="1"/>
<zip code="42321" office_id="KYAAAA" status="0"/>
<zip code="42322" office_id="KY0054" status="1"/>
<zip code="42322" office_id="KYJJJJ" status="0"/>
<zip code="42323" office_id="KY0072" status="1"/>
<zip code="42323" office_id="KYAAAA" status="0"/>
<zip code="42324" office_id="KY0072" status="1"/>
<zip code="42324" office_id="KYAAAA" status="0"/>
<zip code="42325" office_id="KY0072" status="1"/>
<zip code="42325" office_id="KYAAAA" status="0"/>
<zip code="42326" office_id="KY0072" status="1"/>
<zip code="42326" office_id="KYAAAA" status="0"/>
<zip code="42327" office_id="KY0054" status="1"/>
<zip code="42327" office_id="KYJJJJ" status="0"/>
<zip code="42328" office_id="KY0054" status="1"/>
<zip code="42328" office_id="KYJJJJ" status="0"/>
<zip code="42330" office_id="KY0072" status="1"/>
<zip code="42330" office_id="KYAAAA" status="0"/>
<zip code="42332" office_id="KY0072" status="1"/>
<zip code="42332" office_id="KYAAAA" status="0"/>
<zip code="42333" office_id="KY0054" status="1"/>
<zip code="42333" office_id="KYJJJJ" status="0"/>
<zip code="42334" office_id="KY0054" status="1"/>
<zip code="42334" office_id="KYJJJJ" status="0"/>
<zip code="42337" office_id="KY0072" status="1"/>
<zip code="42337" office_id="KYAAAA" status="0"/>
<zip code="42338" office_id="KY0054" status="1"/>
<zip code="42338" office_id="KYJJJJ" status="0"/>
<zip code="42339" office_id="KY0072" status="1"/>
<zip code="42339" office_id="KYAAAA" status="0"/>
<zip code="42343" office_id="KY0054" status="1"/>
<zip code="42343" office_id="KYJJJJ" status="0"/>
<zip code="42344" office_id="KY0072" status="1"/>
<zip code="42344" office_id="KYAAAA" status="0"/>
<zip code="42345" office_id="KY0072" status="1"/>
<zip code="42345" office_id="KYAAAA" status="0"/>
<zip code="42347" office_id="KY0054" status="1"/>
<zip code="42347" office_id="KYJJJJ" status="0"/>
<zip code="42348" office_id="KY0054" status="1"/>
<zip code="42348" office_id="KYJJJJ" status="0"/>
<zip code="42349" office_id="KY0054" status="1"/>
<zip code="42349" office_id="KYJJJJ" status="0"/>
<zip code="42350" office_id="KY0054" status="1"/>
<zip code="42350" office_id="KYJJJJ" status="0"/>
<zip code="42351" office_id="KY0054" status="1"/>
<zip code="42351" office_id="KYJJJJ" status="0"/>
<zip code="42352" office_id="KY0054" status="1"/>
<zip code="42352" office_id="KYJJJJ" status="0"/>
<zip code="42354" office_id="KY0054" status="1"/>
<zip code="42354" office_id="KYJJJJ" status="0"/>
<zip code="42355" office_id="KY0054" status="1"/>
<zip code="42355" office_id="KYJJJJ" status="0"/>
<zip code="42356" office_id="KY0054" status="1"/>
<zip code="42356" office_id="KYJJJJ" status="0"/>
<zip code="42358" office_id="KY0054" status="1"/>
<zip code="42358" office_id="KYJJJJ" status="0"/>
<zip code="42361" office_id="KY0054" status="1"/>
<zip code="42361" office_id="KYJJJJ" status="0"/>
<zip code="42364" office_id="KY0054" status="1"/>
<zip code="42364" office_id="KYJJJJ" status="0"/>
<zip code="42365" office_id="KY0072" status="1"/>
<zip code="42365" office_id="KYAAAA" status="0"/>
<zip code="42366" office_id="KY0054" status="1"/>
<zip code="42366" office_id="KYJJJJ" status="0"/>
<zip code="42367" office_id="KY0072" status="1"/>
<zip code="42367" office_id="KYAAAA" status="0"/>
<zip code="42368" office_id="KY0054" status="1"/>
<zip code="42368" office_id="KYJJJJ" status="0"/>
<zip code="42369" office_id="KY0054" status="1"/>
<zip code="42369" office_id="KYJJJJ" status="0"/>
<zip code="42370" office_id="KY0054" status="1"/>
<zip code="42370" office_id="KYJJJJ" status="0"/>
<zip code="42371" office_id="KY0054" status="1"/>
<zip code="42371" office_id="KYJJJJ" status="0"/>
<zip code="42372" office_id="KY0054" status="1"/>
<zip code="42372" office_id="KYJJJJ" status="0"/>
<zip code="42373" office_id="KY0054" status="1"/>
<zip code="42373" office_id="KYJJJJ" status="0"/>
<zip code="42374" office_id="KY0072" status="1"/>
<zip code="42374" office_id="KYAAAA" status="0"/>
<zip code="42375" office_id="KY0054" status="1"/>
<zip code="42375" office_id="KYJJJJ" status="0"/>
<zip code="42376" office_id="KY0054" status="1"/>
<zip code="42376" office_id="KYJJJJ" status="0"/>
<zip code="42377" office_id="KY0054" status="1"/>
<zip code="42377" office_id="KYJJJJ" status="0"/>
<zip code="42378" office_id="KY0054" status="1"/>
<zip code="42378" office_id="KYJJJJ" status="0"/>
<zip code="42402" office_id="KY0053" status="1"/>
<zip code="42402" office_id="KYJJJJ" status="0"/>
<zip code="42403" office_id="KY0053" status="1"/>
<zip code="42403" office_id="KYJJJJ" status="0"/>
<zip code="42404" office_id="KY0053" status="1"/>
<zip code="42404" office_id="KYJJJJ" status="0"/>
<zip code="42406" office_id="KY0053" status="1"/>
<zip code="42406" office_id="KYJJJJ" status="0"/>
<zip code="42408" office_id="KY0072" status="1"/>
<zip code="42408" office_id="KYAAAA" status="0"/>
<zip code="42409" office_id="KY0053" status="1"/>
<zip code="42409" office_id="KYJJJJ" status="0"/>
<zip code="42410" office_id="KY0072" status="1"/>
<zip code="42410" office_id="KYAAAA" status="0"/>
<zip code="42411" office_id="KY0052" status="1"/>
<zip code="42411" office_id="KYAAAA" status="0"/>
<zip code="42413" office_id="KY0072" status="1"/>
<zip code="42413" office_id="KYAAAA" status="0"/>
<zip code="42419" office_id="KY0053" status="1"/>
<zip code="42419" office_id="KYJJJJ" status="0"/>
<zip code="42420" office_id="KY0053" status="1"/>
<zip code="42420" office_id="KYJJJJ" status="0"/>
<zip code="42431" office_id="KY0072" status="1"/>
<zip code="42431" office_id="KYAAAA" status="0"/>
<zip code="42436" office_id="KY0072" status="1"/>
<zip code="42436" office_id="KYAAAA" status="0"/>
<zip code="42437" office_id="KY0053" status="1"/>
<zip code="42437" office_id="KYJJJJ" status="0"/>
<zip code="42440" office_id="KY0072" status="1"/>
<zip code="42440" office_id="KYAAAA" status="0"/>
<zip code="42441" office_id="KY0072" status="1"/>
<zip code="42441" office_id="KYAAAA" status="0"/>
<zip code="42442" office_id="KY0072" status="1"/>
<zip code="42442" office_id="KYAAAA" status="0"/>
<zip code="42444" office_id="KY0053" status="1"/>
<zip code="42444" office_id="KYJJJJ" status="0"/>
<zip code="42445" office_id="KY0052" status="1"/>
<zip code="42445" office_id="KYAAAA" status="0"/>
<zip code="42450" office_id="KY0053" status="1"/>
<zip code="42450" office_id="KYJJJJ" status="0"/>
<zip code="42451" office_id="KY0053" status="1"/>
<zip code="42451" office_id="KYJJJJ" status="0"/>
<zip code="42452" office_id="KY0053" status="1"/>
<zip code="42452" office_id="KYJJJJ" status="0"/>
<zip code="42453" office_id="KY0072" status="1"/>
<zip code="42453" office_id="KYAAAA" status="0"/>
<zip code="42455" office_id="KY0053" status="1"/>
<zip code="42455" office_id="KYJJJJ" status="0"/>
<zip code="42456" office_id="KY0053" status="1"/>
<zip code="42456" office_id="KYJJJJ" status="0"/>
<zip code="42457" office_id="KY0053" status="1"/>
<zip code="42457" office_id="KYJJJJ" status="0"/>
<zip code="42458" office_id="KY0053" status="1"/>
<zip code="42458" office_id="KYJJJJ" status="0"/>
<zip code="42459" office_id="KY0053" status="1"/>
<zip code="42459" office_id="KYJJJJ" status="0"/>
<zip code="42460" office_id="KY0053" status="1"/>
<zip code="42460" office_id="KYJJJJ" status="0"/>
<zip code="42461" office_id="KY0053" status="1"/>
<zip code="42461" office_id="KYJJJJ" status="0"/>
<zip code="42462" office_id="KY0053" status="1"/>
<zip code="42462" office_id="KYJJJJ" status="0"/>
<zip code="42463" office_id="KY0053" status="1"/>
<zip code="42463" office_id="KYJJJJ" status="0"/>
<zip code="42464" office_id="KY0072" status="1"/>
<zip code="42464" office_id="KYAAAA" status="0"/>
<zip code="42501" office_id="KY0061" status="1"/>
<zip code="42501" office_id="KYHHHH" status="0"/>
<zip code="42502" office_id="KY0061" status="1"/>
<zip code="42502" office_id="KYHHHH" status="0"/>
<zip code="42503" office_id="KY0061" status="1"/>
<zip code="42503" office_id="KYHHHH" status="0"/>
<zip code="42504" office_id="KY0061" status="1"/>
<zip code="42504" office_id="KYHHHH" status="0"/>
<zip code="42516" office_id="KY0061" status="1"/>
<zip code="42516" office_id="KYHHHH" status="0"/>
<zip code="42518" office_id="KY0061" status="1"/>
<zip code="42518" office_id="KYHHHH" status="0"/>
<zip code="42519" office_id="KY0061" status="1"/>
<zip code="42519" office_id="KYHHHH" status="0"/>
<zip code="42528" office_id="KY0061" status="1"/>
<zip code="42528" office_id="KYHHHH" status="0"/>
<zip code="42532" office_id="KY0061" status="1"/>
<zip code="42532" office_id="KYHHHH" status="0"/>
<zip code="42533" office_id="KY0061" status="1"/>
<zip code="42533" office_id="KYHHHH" status="0"/>
<zip code="42539" office_id="KY0061" status="1"/>
<zip code="42539" office_id="KYHHHH" status="0"/>
<zip code="42541" office_id="KY0061" status="1"/>
<zip code="42541" office_id="KYHHHH" status="0"/>
<zip code="42544" office_id="KY0061" status="1"/>
<zip code="42544" office_id="KYHHHH" status="0"/>
<zip code="42553" office_id="KY0061" status="1"/>
<zip code="42553" office_id="KYHHHH" status="0"/>
<zip code="42554" office_id="KY0061" status="1"/>
<zip code="42554" office_id="KYHHHH" status="0"/>
<zip code="42555" office_id="KY0061" status="1"/>
<zip code="42555" office_id="KYHHHH" status="0"/>
<zip code="42558" office_id="KY0061" status="1"/>
<zip code="42558" office_id="KYHHHH" status="0"/>
<zip code="42563" office_id="KY0061" status="1"/>
<zip code="42563" office_id="KYHHHH" status="0"/>
<zip code="42564" office_id="KY0061" status="1"/>
<zip code="42564" office_id="KYHHHH" status="0"/>
<zip code="42565" office_id="KY0061" status="1"/>
<zip code="42565" office_id="KYHHHH" status="0"/>
<zip code="42566" office_id="KY0061" status="1"/>
<zip code="42566" office_id="KYHHHH" status="0"/>
<zip code="42567" office_id="KY0061" status="1"/>
<zip code="42567" office_id="KYHHHH" status="0"/>
<zip code="42601" office_id="KY0061" status="1"/>
<zip code="42601" office_id="KYHHHH" status="0"/>
<zip code="42602" office_id="KY0061" status="1"/>
<zip code="42602" office_id="KYHHHH" status="0"/>
<zip code="42603" office_id="KY0061" status="1"/>
<zip code="42603" office_id="KYHHHH" status="0"/>
<zip code="42607" office_id="KY0061" status="1"/>
<zip code="42607" office_id="KYHHHH" status="0"/>
<zip code="42611" office_id="KY0061" status="1"/>
<zip code="42611" office_id="KYHHHH" status="0"/>
<zip code="42613" office_id="KY0061" status="1"/>
<zip code="42613" office_id="KYHHHH" status="0"/>
<zip code="42618" office_id="KY0061" status="1"/>
<zip code="42618" office_id="KYHHHH" status="0"/>
<zip code="42629" office_id="KY0061" status="1"/>
<zip code="42629" office_id="KYHHHH" status="0"/>
<zip code="42631" office_id="KY0061" status="1"/>
<zip code="42631" office_id="KYHHHH" status="0"/>
<zip code="42632" office_id="KY0061" status="1"/>
<zip code="42632" office_id="KYHHHH" status="0"/>
<zip code="42633" office_id="KY0061" status="1"/>
<zip code="42633" office_id="KYHHHH" status="0"/>
<zip code="42634" office_id="KY0061" status="1"/>
<zip code="42634" office_id="KYHHHH" status="0"/>
<zip code="42635" office_id="KY0061" status="1"/>
<zip code="42635" office_id="KYHHHH" status="0"/>
<zip code="42638" office_id="KY0061" status="1"/>
<zip code="42638" office_id="KYHHHH" status="0"/>
<zip code="42640" office_id="KY0061" status="1"/>
<zip code="42640" office_id="KYHHHH" status="0"/>
<zip code="42642" office_id="KY0078" status="1"/>
<zip code="42642" office_id="KYHHHH" status="0"/>
<zip code="42643" office_id="KY0061" status="1"/>
<zip code="42643" office_id="KYHHHH" status="0"/>
<zip code="42647" office_id="KY0061" status="1"/>
<zip code="42647" office_id="KYHHHH" status="0"/>
<zip code="42648" office_id="KY0061" status="1"/>
<zip code="42648" office_id="KYHHHH" status="0"/>
<zip code="42649" office_id="KY0078" status="1"/>
<zip code="42649" office_id="KYHHHH" status="0"/>
<zip code="42653" office_id="KY0061" status="1"/>
<zip code="42653" office_id="KYHHHH" status="0"/>
<zip code="42655" office_id="KY0061" status="1"/>
<zip code="42655" office_id="KYHHHH" status="0"/>
<zip code="42701" office_id="KY0075" status="1"/>
<zip code="42701" office_id="KYBBBB" status="0"/>
<zip code="42702" office_id="KY0075" status="1"/>
<zip code="42702" office_id="KYBBBB" status="0"/>
<zip code="42711" office_id="KY0061" status="1"/>
<zip code="42711" office_id="KYHHHH" status="0"/>
<zip code="42712" office_id="KY0075" status="1"/>
<zip code="42712" office_id="KYBBBB" status="0"/>
<zip code="42713" office_id="KY0073" status="1"/>
<zip code="42713" office_id="KYKKKK" status="0"/>
<zip code="42715" office_id="KY0078" status="1"/>
<zip code="42715" office_id="KYHHHH" status="0"/>
<zip code="42716" office_id="KY0075" status="1"/>
<zip code="42716" office_id="KYBBBB" status="0"/>
<zip code="42717" office_id="KY0061" status="1"/>
<zip code="42717" office_id="KYHHHH" status="0"/>
<zip code="42718" office_id="KY0078" status="1"/>
<zip code="42718" office_id="KYHHHH" status="0"/>
<zip code="42719" office_id="KY0078" status="1"/>
<zip code="42719" office_id="KYHHHH" status="0"/>
<zip code="42720" office_id="KY0078" status="1"/>
<zip code="42720" office_id="KYHHHH" status="0"/>
<zip code="42721" office_id="KY0075" status="1"/>
<zip code="42721" office_id="KYBBBB" status="0"/>
<zip code="42722" office_id="KY0073" status="1"/>
<zip code="42722" office_id="KYKKKK" status="0"/>
<zip code="42723" office_id="KY0078" status="1"/>
<zip code="42723" office_id="KYHHHH" status="0"/>
<zip code="42724" office_id="KY0075" status="1"/>
<zip code="42724" office_id="KYBBBB" status="0"/>
<zip code="42726" office_id="KY0075" status="1"/>
<zip code="42726" office_id="KYBBBB" status="0"/>
<zip code="42728" office_id="KY0078" status="1"/>
<zip code="42728" office_id="KYHHHH" status="0"/>
<zip code="42729" office_id="KY0073" status="1"/>
<zip code="42729" office_id="KYKKKK" status="0"/>
<zip code="42730" office_id="KY0078" status="1"/>
<zip code="42730" office_id="KYHHHH" status="0"/>
<zip code="42731" office_id="KY0061" status="1"/>
<zip code="42731" office_id="KYHHHH" status="0"/>
<zip code="42732" office_id="KY0075" status="1"/>
<zip code="42732" office_id="KYBBBB" status="0"/>
<zip code="42733" office_id="KY0078" status="1"/>
<zip code="42733" office_id="KYHHHH" status="0"/>
<zip code="42735" office_id="KY0078" status="1"/>
<zip code="42735" office_id="KYHHHH" status="0"/>
<zip code="42736" office_id="KY0078" status="1"/>
<zip code="42736" office_id="KYHHHH" status="0"/>
<zip code="42740" office_id="KY0075" status="1"/>
<zip code="42740" office_id="KYBBBB" status="0"/>
<zip code="42741" office_id="KY0078" status="1"/>
<zip code="42741" office_id="KYHHHH" status="0"/>
<zip code="42742" office_id="KY0078" status="1"/>
<zip code="42742" office_id="KYHHHH" status="0"/>
<zip code="42743" office_id="KY0078" status="1"/>
<zip code="42743" office_id="KYHHHH" status="0"/>
<zip code="42746" office_id="KY0073" status="1"/>
<zip code="42746" office_id="KYKKKK" status="0"/>
<zip code="42748" office_id="KY0075" status="1"/>
<zip code="42748" office_id="KYBBBB" status="0"/>
<zip code="42749" office_id="KY0073" status="1"/>
<zip code="42749" office_id="KYKKKK" status="0"/>
<zip code="42753" office_id="KY0078" status="1"/>
<zip code="42753" office_id="KYHHHH" status="0"/>
<zip code="42754" office_id="KY0075" status="1"/>
<zip code="42754" office_id="KYBBBB" status="0"/>
<zip code="42755" office_id="KY0075" status="1"/>
<zip code="42755" office_id="KYBBBB" status="0"/>
<zip code="42757" office_id="KY0075" status="1"/>
<zip code="42757" office_id="KYBBBB" status="0"/>
<zip code="42758" office_id="KY0078" status="1"/>
<zip code="42758" office_id="KYHHHH" status="0"/>
<zip code="42759" office_id="KY0061" status="1"/>
<zip code="42759" office_id="KYHHHH" status="0"/>
<zip code="42761" office_id="KY0078" status="1"/>
<zip code="42761" office_id="KYHHHH" status="0"/>
<zip code="42762" office_id="KY0075" status="1"/>
<zip code="42762" office_id="KYBBBB" status="0"/>
<zip code="42764" office_id="KY0075" status="1"/>
<zip code="42764" office_id="KYBBBB" status="0"/>
<zip code="42765" office_id="KY0073" status="1"/>
<zip code="42765" office_id="KYKKKK" status="0"/>
<zip code="42766" office_id="KY0075" status="1"/>
<zip code="42766" office_id="KYBBBB" status="0"/>
<zip code="42772" office_id="KY0073" status="1"/>
<zip code="42772" office_id="KYKKKK" status="0"/>
<zip code="42776" office_id="KY0075" status="1"/>
<zip code="42776" office_id="KYBBBB" status="0"/>
<zip code="42779" office_id="KY0075" status="1"/>
<zip code="42779" office_id="KYBBBB" status="0"/>
<zip code="42780" office_id="KY0075" status="1"/>
<zip code="42780" office_id="KYBBBB" status="0"/>
<zip code="42781" office_id="KY0075" status="1"/>
<zip code="42781" office_id="KYBBBB" status="0"/>
<zip code="42782" office_id="KY0078" status="1"/>
<zip code="42782" office_id="KYHHHH" status="0"/>
<zip code="42783" office_id="KY0075" status="1"/>
<zip code="42783" office_id="KYBBBB" status="0"/>
<zip code="42784" office_id="KY0075" status="1"/>
<zip code="42784" office_id="KYBBBB" status="0"/>
<zip code="42785" office_id="KY0075" status="1"/>
<zip code="42785" office_id="KYBBBB" status="0"/>
<zip code="42786" office_id="KY0061" status="1"/>
<zip code="42786" office_id="KYHHHH" status="0"/>
<zip code="42788" office_id="KY0075" status="1"/>
<zip code="42788" office_id="KYBBBB" status="0"/>
<zip code="43101" office_id="KY0062" status="1"/>
<zip code="43101" office_id="KYFFFF" status="0"/>
<zip code="43115" office_id="KY0062" status="1"/>
<zip code="43115" office_id="KYFFFF" status="0"/>
<zip code="45001" office_id="KY0057" status="1"/>
<zip code="45001" office_id="KYEEEE" status="0"/>
<zip code="45002" office_id="KY0057" status="1"/>
<zip code="45002" office_id="KYEEEE" status="0"/>
<zip code="45003" office_id="KY0057" status="1"/>
<zip code="45003" office_id="KYEEEE" status="0"/>
<zip code="45004" office_id="KY0057" status="1"/>
<zip code="45004" office_id="KYEEEE" status="0"/>
<zip code="45005" office_id="KY0057" status="1"/>
<zip code="45005" office_id="KYEEEE" status="0"/>
<zip code="45011" office_id="KY0057" status="1"/>
<zip code="45011" office_id="KYEEEE" status="0"/>
<zip code="45012" office_id="KY0057" status="1"/>
<zip code="45012" office_id="KYEEEE" status="0"/>
<zip code="45013" office_id="KY0057" status="1"/>
<zip code="45013" office_id="KYEEEE" status="0"/>
<zip code="45014" office_id="KY0057" status="1"/>
<zip code="45014" office_id="KYEEEE" status="0"/>
<zip code="45015" office_id="KY0057" status="1"/>
<zip code="45015" office_id="KYEEEE" status="0"/>
<zip code="45018" office_id="KY0057" status="1"/>
<zip code="45018" office_id="KYEEEE" status="0"/>
<zip code="45020" office_id="KY0057" status="1"/>
<zip code="45020" office_id="KYEEEE" status="0"/>
<zip code="45023" office_id="KY0057" status="1"/>
<zip code="45023" office_id="KYEEEE" status="0"/>
<zip code="45025" office_id="KY0057" status="1"/>
<zip code="45025" office_id="KYEEEE" status="0"/>
<zip code="45026" office_id="KY0057" status="1"/>
<zip code="45026" office_id="KYEEEE" status="0"/>
<zip code="45030" office_id="KY0057" status="1"/>
<zip code="45030" office_id="KYEEEE" status="0"/>
<zip code="45032" office_id="KY0057" status="1"/>
<zip code="45032" office_id="KYEEEE" status="0"/>
<zip code="45033" office_id="KY0057" status="1"/>
<zip code="45033" office_id="KYEEEE" status="0"/>
<zip code="45034" office_id="KY0057" status="1"/>
<zip code="45034" office_id="KYEEEE" status="0"/>
<zip code="45036" office_id="KY0057" status="1"/>
<zip code="45036" office_id="KYEEEE" status="0"/>
<zip code="45039" office_id="KY0057" status="1"/>
<zip code="45039" office_id="KYEEEE" status="0"/>
<zip code="45040" office_id="KY0057" status="1"/>
<zip code="45040" office_id="KYEEEE" status="0"/>
<zip code="45041" office_id="KY0057" status="1"/>
<zip code="45041" office_id="KYEEEE" status="0"/>
<zip code="45042" office_id="KY0057" status="1"/>
<zip code="45042" office_id="KYEEEE" status="0"/>
<zip code="45043" office_id="KY0057" status="1"/>
<zip code="45043" office_id="KYEEEE" status="0"/>
<zip code="45044" office_id="KY0057" status="1"/>
<zip code="45044" office_id="KYEEEE" status="0"/>
<zip code="45050" office_id="KY0057" status="1"/>
<zip code="45050" office_id="KYEEEE" status="0"/>
<zip code="45051" office_id="KY0057" status="1"/>
<zip code="45051" office_id="KYEEEE" status="0"/>
<zip code="45052" office_id="KY0057" status="1"/>
<zip code="45052" office_id="KYEEEE" status="0"/>
<zip code="45053" office_id="KY0057" status="1"/>
<zip code="45053" office_id="KYEEEE" status="0"/>
<zip code="45054" office_id="KY0057" status="1"/>
<zip code="45054" office_id="KYEEEE" status="0"/>
<zip code="45055" office_id="KY0057" status="1"/>
<zip code="45055" office_id="KYEEEE" status="0"/>
<zip code="45056" office_id="KY0057" status="1"/>
<zip code="45056" office_id="KYEEEE" status="0"/>
<zip code="45061" office_id="KY0057" status="1"/>
<zip code="45061" office_id="KYEEEE" status="0"/>
<zip code="45062" office_id="KY0057" status="1"/>
<zip code="45062" office_id="KYEEEE" status="0"/>
<zip code="45063" office_id="KY0057" status="1"/>
<zip code="45063" office_id="KYEEEE" status="0"/>
<zip code="45064" office_id="KY0057" status="1"/>
<zip code="45064" office_id="KYEEEE" status="0"/>
<zip code="45065" office_id="KY0057" status="1"/>
<zip code="45065" office_id="KYEEEE" status="0"/>
<zip code="45066" office_id="KY0057" status="1"/>
<zip code="45066" office_id="KYEEEE" status="0"/>
<zip code="45067" office_id="KY0057" status="1"/>
<zip code="45067" office_id="KYEEEE" status="0"/>
<zip code="45068" office_id="KY0057" status="1"/>
<zip code="45068" office_id="KYEEEE" status="0"/>
<zip code="45069" office_id="KY0057" status="1"/>
<zip code="45069" office_id="KYEEEE" status="0"/>
<zip code="45070" office_id="KY0057" status="1"/>
<zip code="45070" office_id="KYEEEE" status="0"/>
<zip code="45071" office_id="KY0057" status="1"/>
<zip code="45071" office_id="KYEEEE" status="0"/>
<zip code="45073" office_id="KY0057" status="1"/>
<zip code="45073" office_id="KYEEEE" status="0"/>
<zip code="45099" office_id="KY0057" status="1"/>
<zip code="45099" office_id="KYEEEE" status="0"/>
<zip code="45101" office_id="KY0062" status="1"/>
<zip code="45101" office_id="KYFFFF" status="0"/>
<zip code="45102" office_id="KY0057" status="1"/>
<zip code="45102" office_id="KYEEEE" status="0"/>
<zip code="45103" office_id="KY0057" status="1"/>
<zip code="45103" office_id="KYEEEE" status="0"/>
<zip code="45105" office_id="KY0062" status="1"/>
<zip code="45105" office_id="KYFFFF" status="0"/>
<zip code="45106" office_id="KY0057" status="1"/>
<zip code="45106" office_id="KYEEEE" status="0"/>
<zip code="45107" office_id="KY0057" status="1"/>
<zip code="45107" office_id="KYEEEE" status="0"/>
<zip code="45110" office_id="KY0062" status="1"/>
<zip code="45110" office_id="KYFFFF" status="0"/>
<zip code="45111" office_id="KY0057" status="1"/>
<zip code="45111" office_id="KYEEEE" status="0"/>
<zip code="45112" office_id="KY0057" status="1"/>
<zip code="45112" office_id="KYEEEE" status="0"/>
<zip code="45113" office_id="KY0057" status="1"/>
<zip code="45113" office_id="KYEEEE" status="0"/>
<zip code="45114" office_id="KY0057" status="1"/>
<zip code="45114" office_id="KYEEEE" status="0"/>
<zip code="45115" office_id="KY0062" status="1"/>
<zip code="45115" office_id="KYFFFF" status="0"/>
<zip code="45118" office_id="KY0062" status="1"/>
<zip code="45118" office_id="KYFFFF" status="0"/>
<zip code="45119" office_id="KY0062" status="1"/>
<zip code="45119" office_id="KYFFFF" status="0"/>
<zip code="45120" office_id="KY0057" status="1"/>
<zip code="45120" office_id="KYEEEE" status="0"/>
<zip code="45121" office_id="KY0062" status="1"/>
<zip code="45121" office_id="KYFFFF" status="0"/>
<zip code="45122" office_id="KY0057" status="1"/>
<zip code="45122" office_id="KYEEEE" status="0"/>
<zip code="45130" office_id="KY0062" status="1"/>
<zip code="45130" office_id="KYFFFF" status="0"/>
<zip code="45131" office_id="KY0062" status="1"/>
<zip code="45131" office_id="KYFFFF" status="0"/>
<zip code="45132" office_id="KY0062" status="1"/>
<zip code="45132" office_id="KYFFFF" status="0"/>
<zip code="45133" office_id="KY0062" status="1"/>
<zip code="45133" office_id="KYFFFF" status="0"/>
<zip code="45135" office_id="KY0062" status="1"/>
<zip code="45135" office_id="KYFFFF" status="0"/>
<zip code="45138" office_id="KY0057" status="1"/>
<zip code="45138" office_id="KYEEEE" status="0"/>
<zip code="45140" office_id="KY0057" status="1"/>
<zip code="45140" office_id="KYEEEE" status="0"/>
<zip code="45142" office_id="KY0062" status="1"/>
<zip code="45142" office_id="KYFFFF" status="0"/>
<zip code="45144" office_id="KY0062" status="1"/>
<zip code="45144" office_id="KYFFFF" status="0"/>
<zip code="45145" office_id="KY0057" status="1"/>
<zip code="45145" office_id="KYEEEE" status="0"/>
<zip code="45146" office_id="KY0057" status="1"/>
<zip code="45146" office_id="KYEEEE" status="0"/>
<zip code="45147" office_id="KY0057" status="1"/>
<zip code="45147" office_id="KYEEEE" status="0"/>
<zip code="45148" office_id="KY0057" status="1"/>
<zip code="45148" office_id="KYEEEE" status="0"/>
<zip code="45150" office_id="KY0057" status="1"/>
<zip code="45150" office_id="KYEEEE" status="0"/>
<zip code="45152" office_id="KY0057" status="1"/>
<zip code="45152" office_id="KYEEEE" status="0"/>
<zip code="45153" office_id="KY0057" status="1"/>
<zip code="45153" office_id="KYEEEE" status="0"/>
<zip code="45154" office_id="KY0062" status="1"/>
<zip code="45154" office_id="KYFFFF" status="0"/>
<zip code="45155" office_id="KY0062" status="1"/>
<zip code="45155" office_id="KYFFFF" status="0"/>
<zip code="45156" office_id="KY0057" status="1"/>
<zip code="45156" office_id="KYEEEE" status="0"/>
<zip code="45157" office_id="KY0057" status="1"/>
<zip code="45157" office_id="KYEEEE" status="0"/>
<zip code="45158" office_id="KY0057" status="1"/>
<zip code="45158" office_id="KYEEEE" status="0"/>
<zip code="45159" office_id="KY0057" status="1"/>
<zip code="45159" office_id="KYEEEE" status="0"/>
<zip code="45160" office_id="KY0057" status="1"/>
<zip code="45160" office_id="KYEEEE" status="0"/>
<zip code="45162" office_id="KY0057" status="1"/>
<zip code="45162" office_id="KYEEEE" status="0"/>
<zip code="45164" office_id="KY0057" status="1"/>
<zip code="45164" office_id="KYEEEE" status="0"/>
<zip code="45165" office_id="KY0062" status="1"/>
<zip code="45165" office_id="KYFFFF" status="0"/>
<zip code="45166" office_id="KY0057" status="1"/>
<zip code="45166" office_id="KYEEEE" status="0"/>
<zip code="45167" office_id="KY0062" status="1"/>
<zip code="45167" office_id="KYFFFF" status="0"/>
<zip code="45168" office_id="KY0062" status="1"/>
<zip code="45168" office_id="KYFFFF" status="0"/>
<zip code="45169" office_id="KY0057" status="1"/>
<zip code="45169" office_id="KYEEEE" status="0"/>
<zip code="45171" office_id="KY0062" status="1"/>
<zip code="45171" office_id="KYFFFF" status="0"/>
<zip code="45172" office_id="KY0062" status="1"/>
<zip code="45172" office_id="KYFFFF" status="0"/>
<zip code="45174" office_id="KY0057" status="1"/>
<zip code="45174" office_id="KYEEEE" status="0"/>
<zip code="45176" office_id="KY0062" status="1"/>
<zip code="45176" office_id="KYFFFF" status="0"/>
<zip code="45177" office_id="KY0057" status="1"/>
<zip code="45177" office_id="KYEEEE" status="0"/>
<zip code="45201" office_id="KY0057" status="1"/>
<zip code="45201" office_id="KYEEEE" status="0"/>
<zip code="45202" office_id="KY0057" status="1"/>
<zip code="45202" office_id="KYEEEE" status="0"/>
<zip code="45203" office_id="KY0057" status="1"/>
<zip code="45203" office_id="KYEEEE" status="0"/>
<zip code="45204" office_id="KY0057" status="1"/>
<zip code="45204" office_id="KYEEEE" status="0"/>
<zip code="45205" office_id="KY0057" status="1"/>
<zip code="45205" office_id="KYEEEE" status="0"/>
<zip code="45206" office_id="KY0057" status="1"/>
<zip code="45206" office_id="KYEEEE" status="0"/>
<zip code="45207" office_id="KY0057" status="1"/>
<zip code="45207" office_id="KYEEEE" status="0"/>
<zip code="45208" office_id="KY0057" status="1"/>
<zip code="45208" office_id="KYEEEE" status="0"/>
<zip code="45209" office_id="KY0057" status="1"/>
<zip code="45209" office_id="KYEEEE" status="0"/>
<zip code="45210" office_id="KY0057" status="1"/>
<zip code="45210" office_id="KYEEEE" status="0"/>
<zip code="45211" office_id="KY0057" status="1"/>
<zip code="45211" office_id="KYEEEE" status="0"/>
<zip code="45212" office_id="KY0057" status="1"/>
<zip code="45212" office_id="KYEEEE" status="0"/>
<zip code="45213" office_id="KY0057" status="1"/>
<zip code="45213" office_id="KYEEEE" status="0"/>
<zip code="45214" office_id="KY0057" status="1"/>
<zip code="45214" office_id="KYEEEE" status="0"/>
<zip code="45215" office_id="KY0057" status="1"/>
<zip code="45215" office_id="KYEEEE" status="0"/>
<zip code="45216" office_id="KY0057" status="1"/>
<zip code="45216" office_id="KYEEEE" status="0"/>
<zip code="45217" office_id="KY0057" status="1"/>
<zip code="45217" office_id="KYEEEE" status="0"/>
<zip code="45218" office_id="KY0057" status="1"/>
<zip code="45218" office_id="KYEEEE" status="0"/>
<zip code="45219" office_id="KY0057" status="1"/>
<zip code="45219" office_id="KYEEEE" status="0"/>
<zip code="45220" office_id="KY0057" status="1"/>
<zip code="45220" office_id="KYEEEE" status="0"/>
<zip code="45221" office_id="KY0057" status="1"/>
<zip code="45221" office_id="KYEEEE" status="0"/>
<zip code="45222" office_id="KY0057" status="1"/>
<zip code="45222" office_id="KYEEEE" status="0"/>
<zip code="45223" office_id="KY0057" status="1"/>
<zip code="45223" office_id="KYEEEE" status="0"/>
<zip code="45224" office_id="KY0057" status="1"/>
<zip code="45224" office_id="KYEEEE" status="0"/>
<zip code="45225" office_id="KY0057" status="1"/>
<zip code="45225" office_id="KYEEEE" status="0"/>
<zip code="45226" office_id="KY0057" status="1"/>
<zip code="45226" office_id="KYEEEE" status="0"/>
<zip code="45227" office_id="KY0057" status="1"/>
<zip code="45227" office_id="KYEEEE" status="0"/>
<zip code="45228" office_id="KY0057" status="1"/>
<zip code="45228" office_id="KYEEEE" status="0"/>
<zip code="45229" office_id="KY0057" status="1"/>
<zip code="45229" office_id="KYEEEE" status="0"/>
<zip code="45230" office_id="KY0057" status="1"/>
<zip code="45230" office_id="KYEEEE" status="0"/>
<zip code="45231" office_id="KY0057" status="1"/>
<zip code="45231" office_id="KYEEEE" status="0"/>
<zip code="45232" office_id="KY0057" status="1"/>
<zip code="45232" office_id="KYEEEE" status="0"/>
<zip code="45233" office_id="KY0057" status="1"/>
<zip code="45233" office_id="KYEEEE" status="0"/>
<zip code="45234" office_id="KY0057" status="1"/>
<zip code="45234" office_id="KYEEEE" status="0"/>
<zip code="45235" office_id="KY0057" status="1"/>
<zip code="45235" office_id="KYEEEE" status="0"/>
<zip code="45236" office_id="KY0057" status="1"/>
<zip code="45236" office_id="KYEEEE" status="0"/>
<zip code="45237" office_id="KY0057" status="1"/>
<zip code="45237" office_id="KYEEEE" status="0"/>
<zip code="45238" office_id="KY0057" status="1"/>
<zip code="45238" office_id="KYEEEE" status="0"/>
<zip code="45239" office_id="KY0057" status="1"/>
<zip code="45239" office_id="KYEEEE" status="0"/>
<zip code="45240" office_id="KY0057" status="1"/>
<zip code="45240" office_id="KYEEEE" status="0"/>
<zip code="45241" office_id="KY0057" status="1"/>
<zip code="45241" office_id="KYEEEE" status="0"/>
<zip code="45242" office_id="KY0057" status="1"/>
<zip code="45242" office_id="KYEEEE" status="0"/>
<zip code="45243" office_id="KY0057" status="1"/>
<zip code="45243" office_id="KYEEEE" status="0"/>
<zip code="45244" office_id="KY0057" status="1"/>
<zip code="45244" office_id="KYEEEE" status="0"/>
<zip code="45245" office_id="KY0057" status="1"/>
<zip code="45245" office_id="KYEEEE" status="0"/>
<zip code="45246" office_id="KY0057" status="1"/>
<zip code="45246" office_id="KYEEEE" status="0"/>
<zip code="45247" office_id="KY0057" status="1"/>
<zip code="45247" office_id="KYEEEE" status="0"/>
<zip code="45248" office_id="KY0057" status="1"/>
<zip code="45248" office_id="KYEEEE" status="0"/>
<zip code="45249" office_id="KY0057" status="1"/>
<zip code="45249" office_id="KYEEEE" status="0"/>
<zip code="45250" office_id="KY0057" status="1"/>
<zip code="45250" office_id="KYEEEE" status="0"/>
<zip code="45251" office_id="KY0057" status="1"/>
<zip code="45251" office_id="KYEEEE" status="0"/>
<zip code="45252" office_id="KY0057" status="1"/>
<zip code="45252" office_id="KYEEEE" status="0"/>
<zip code="45253" office_id="KY0057" status="1"/>
<zip code="45253" office_id="KYEEEE" status="0"/>
<zip code="45254" office_id="KY0057" status="1"/>
<zip code="45254" office_id="KYEEEE" status="0"/>
<zip code="45255" office_id="KY0057" status="1"/>
<zip code="45255" office_id="KYEEEE" status="0"/>
<zip code="45258" office_id="KY0057" status="1"/>
<zip code="45258" office_id="KYEEEE" status="0"/>
<zip code="45262" office_id="KY0057" status="1"/>
<zip code="45262" office_id="KYEEEE" status="0"/>
<zip code="45263" office_id="KY0057" status="1"/>
<zip code="45263" office_id="KYEEEE" status="0"/>
<zip code="45264" office_id="KY0057" status="1"/>
<zip code="45264" office_id="KYEEEE" status="0"/>
<zip code="45267" office_id="KY0057" status="1"/>
<zip code="45267" office_id="KYEEEE" status="0"/>
<zip code="45268" office_id="KY0057" status="1"/>
<zip code="45268" office_id="KYEEEE" status="0"/>
<zip code="45269" office_id="KY0057" status="1"/>
<zip code="45269" office_id="KYEEEE" status="0"/>
<zip code="45270" office_id="KY0057" status="1"/>
<zip code="45270" office_id="KYEEEE" status="0"/>
<zip code="45271" office_id="KY0057" status="1"/>
<zip code="45271" office_id="KYEEEE" status="0"/>
<zip code="45273" office_id="KY0057" status="1"/>
<zip code="45273" office_id="KYEEEE" status="0"/>
<zip code="45274" office_id="KY0057" status="1"/>
<zip code="45274" office_id="KYEEEE" status="0"/>
<zip code="45277" office_id="KY0057" status="1"/>
<zip code="45277" office_id="KYEEEE" status="0"/>
<zip code="45296" office_id="KY0057" status="1"/>
<zip code="45296" office_id="KYEEEE" status="0"/>
<zip code="45298" office_id="KY0057" status="1"/>
<zip code="45298" office_id="KYEEEE" status="0"/>
<zip code="45299" office_id="KY0057" status="1"/>
<zip code="45299" office_id="KYEEEE" status="0"/>
<zip code="45301" office_id="KY0057" status="1"/>
<zip code="45301" office_id="KYEEEE" status="0"/>
<zip code="45305" office_id="KY0057" status="1"/>
<zip code="45305" office_id="KYEEEE" status="0"/>
<zip code="45307" office_id="KY0057" status="1"/>
<zip code="45307" office_id="KYEEEE" status="0"/>
<zip code="45309" office_id="KY0057" status="1"/>
<zip code="45309" office_id="KYEEEE" status="0"/>
<zip code="45311" office_id="KY0057" status="1"/>
<zip code="45311" office_id="KYEEEE" status="0"/>
<zip code="45314" office_id="KY0057" status="1"/>
<zip code="45314" office_id="KYEEEE" status="0"/>
<zip code="45315" office_id="KY0057" status="1"/>
<zip code="45315" office_id="KYEEEE" status="0"/>
<zip code="45320" office_id="KY0057" status="1"/>
<zip code="45320" office_id="KYEEEE" status="0"/>
<zip code="45321" office_id="KY0057" status="1"/>
<zip code="45321" office_id="KYEEEE" status="0"/>
<zip code="45322" office_id="KY0057" status="1"/>
<zip code="45322" office_id="KYEEEE" status="0"/>
<zip code="45324" office_id="KY0057" status="1"/>
<zip code="45324" office_id="KYEEEE" status="0"/>
<zip code="45325" office_id="KY0057" status="1"/>
<zip code="45325" office_id="KYEEEE" status="0"/>
<zip code="45327" office_id="KY0057" status="1"/>
<zip code="45327" office_id="KYEEEE" status="0"/>
<zip code="45330" office_id="KY0057" status="1"/>
<zip code="45330" office_id="KYEEEE" status="0"/>
<zip code="45335" office_id="KY0057" status="1"/>
<zip code="45335" office_id="KYEEEE" status="0"/>
<zip code="45338" office_id="KY0057" status="1"/>
<zip code="45338" office_id="KYEEEE" status="0"/>
<zip code="45342" office_id="KY0057" status="1"/>
<zip code="45342" office_id="KYEEEE" status="0"/>
<zip code="45343" office_id="KY0057" status="1"/>
<zip code="45343" office_id="KYEEEE" status="0"/>
<zip code="45345" office_id="KY0057" status="1"/>
<zip code="45345" office_id="KYEEEE" status="0"/>
<zip code="45347" office_id="KY0057" status="1"/>
<zip code="45347" office_id="KYEEEE" status="0"/>
<zip code="45354" office_id="KY0057" status="1"/>
<zip code="45354" office_id="KYEEEE" status="0"/>
<zip code="45370" office_id="KY0057" status="1"/>
<zip code="45370" office_id="KYEEEE" status="0"/>
<zip code="45377" office_id="KY0057" status="1"/>
<zip code="45377" office_id="KYEEEE" status="0"/>
<zip code="45378" office_id="KY0057" status="1"/>
<zip code="45378" office_id="KYEEEE" status="0"/>
<zip code="45381" office_id="KY0057" status="1"/>
<zip code="45381" office_id="KYEEEE" status="0"/>
<zip code="45382" office_id="KY0057" status="1"/>
<zip code="45382" office_id="KYEEEE" status="0"/>
<zip code="45384" office_id="KY0057" status="1"/>
<zip code="45384" office_id="KYEEEE" status="0"/>
<zip code="45385" office_id="KY0057" status="1"/>
<zip code="45385" office_id="KYEEEE" status="0"/>
<zip code="45387" office_id="KY0057" status="1"/>
<zip code="45387" office_id="KYEEEE" status="0"/>
<zip code="45401" office_id="KY0057" status="1"/>
<zip code="45401" office_id="KYEEEE" status="0"/>
<zip code="45402" office_id="KY0057" status="1"/>
<zip code="45402" office_id="KYEEEE" status="0"/>
<zip code="45403" office_id="KY0057" status="1"/>
<zip code="45403" office_id="KYEEEE" status="0"/>
<zip code="45404" office_id="KY0057" status="1"/>
<zip code="45404" office_id="KYEEEE" status="0"/>
<zip code="45405" office_id="KY0057" status="1"/>
<zip code="45405" office_id="KYEEEE" status="0"/>
<zip code="45406" office_id="KY0057" status="1"/>
<zip code="45406" office_id="KYEEEE" status="0"/>
<zip code="45407" office_id="KY0057" status="1"/>
<zip code="45407" office_id="KYEEEE" status="0"/>
<zip code="45408" office_id="KY0057" status="1"/>
<zip code="45408" office_id="KYEEEE" status="0"/>
<zip code="45409" office_id="KY0057" status="1"/>
<zip code="45409" office_id="KYEEEE" status="0"/>
<zip code="45410" office_id="KY0057" status="1"/>
<zip code="45410" office_id="KYEEEE" status="0"/>
<zip code="45412" office_id="KY0057" status="1"/>
<zip code="45412" office_id="KYEEEE" status="0"/>
<zip code="45413" office_id="KY0057" status="1"/>
<zip code="45413" office_id="KYEEEE" status="0"/>
<zip code="45414" office_id="KY0057" status="1"/>
<zip code="45414" office_id="KYEEEE" status="0"/>
<zip code="45415" office_id="KY0057" status="1"/>
<zip code="45415" office_id="KYEEEE" status="0"/>
<zip code="45416" office_id="KY0057" status="1"/>
<zip code="45416" office_id="KYEEEE" status="0"/>
<zip code="45417" office_id="KY0057" status="1"/>
<zip code="45417" office_id="KYEEEE" status="0"/>
<zip code="45418" office_id="KY0057" status="1"/>
<zip code="45418" office_id="KYEEEE" status="0"/>
<zip code="45419" office_id="KY0057" status="1"/>
<zip code="45419" office_id="KYEEEE" status="0"/>
<zip code="45420" office_id="KY0057" status="1"/>
<zip code="45420" office_id="KYEEEE" status="0"/>
<zip code="45422" office_id="KY0057" status="1"/>
<zip code="45422" office_id="KYEEEE" status="0"/>
<zip code="45423" office_id="KY0057" status="1"/>
<zip code="45423" office_id="KYEEEE" status="0"/>
<zip code="45424" office_id="KY0057" status="1"/>
<zip code="45424" office_id="KYEEEE" status="0"/>
<zip code="45426" office_id="KY0057" status="1"/>
<zip code="45426" office_id="KYEEEE" status="0"/>
<zip code="45427" office_id="KY0057" status="1"/>
<zip code="45427" office_id="KYEEEE" status="0"/>
<zip code="45428" office_id="KY0057" status="1"/>
<zip code="45428" office_id="KYEEEE" status="0"/>
<zip code="45429" office_id="KY0057" status="1"/>
<zip code="45429" office_id="KYEEEE" status="0"/>
<zip code="45430" office_id="KY0057" status="1"/>
<zip code="45430" office_id="KYEEEE" status="0"/>
<zip code="45431" office_id="KY0057" status="1"/>
<zip code="45431" office_id="KYEEEE" status="0"/>
<zip code="45432" office_id="KY0057" status="1"/>
<zip code="45432" office_id="KYEEEE" status="0"/>
<zip code="45433" office_id="KY0057" status="1"/>
<zip code="45433" office_id="KYEEEE" status="0"/>
<zip code="45434" office_id="KY0057" status="1"/>
<zip code="45434" office_id="KYEEEE" status="0"/>
<zip code="45435" office_id="KY0057" status="1"/>
<zip code="45435" office_id="KYEEEE" status="0"/>
<zip code="45437" office_id="KY0057" status="1"/>
<zip code="45437" office_id="KYEEEE" status="0"/>
<zip code="45439" office_id="KY0057" status="1"/>
<zip code="45439" office_id="KYEEEE" status="0"/>
<zip code="45440" office_id="KY0057" status="1"/>
<zip code="45440" office_id="KYEEEE" status="0"/>
<zip code="45441" office_id="KY0057" status="1"/>
<zip code="45441" office_id="KYEEEE" status="0"/>
<zip code="45448" office_id="KY0057" status="1"/>
<zip code="45448" office_id="KYEEEE" status="0"/>
<zip code="45449" office_id="KY0057" status="1"/>
<zip code="45449" office_id="KYEEEE" status="0"/>
<zip code="45454" office_id="KY0057" status="1"/>
<zip code="45454" office_id="KYEEEE" status="0"/>
<zip code="45458" office_id="KY0057" status="1"/>
<zip code="45458" office_id="KYEEEE" status="0"/>
<zip code="45459" office_id="KY0057" status="1"/>
<zip code="45459" office_id="KYEEEE" status="0"/>
<zip code="45463" office_id="KY0057" status="1"/>
<zip code="45463" office_id="KYEEEE" status="0"/>
<zip code="45469" office_id="KY0057" status="1"/>
<zip code="45469" office_id="KYEEEE" status="0"/>
<zip code="45470" office_id="KY0057" status="1"/>
<zip code="45470" office_id="KYEEEE" status="0"/>
<zip code="45475" office_id="KY0057" status="1"/>
<zip code="45475" office_id="KYEEEE" status="0"/>
<zip code="45479" office_id="KY0057" status="1"/>
<zip code="45479" office_id="KYEEEE" status="0"/>
<zip code="45481" office_id="KY0057" status="1"/>
<zip code="45481" office_id="KYEEEE" status="0"/>
<zip code="45482" office_id="KY0057" status="1"/>
<zip code="45482" office_id="KYEEEE" status="0"/>
<zip code="45490" office_id="KY0057" status="1"/>
<zip code="45490" office_id="KYEEEE" status="0"/>
<zip code="45601" office_id="KY0062" status="1"/>
<zip code="45601" office_id="KYFFFF" status="0"/>
<zip code="45612" office_id="KY0069" status="1"/>
<zip code="45612" office_id="KYFFFF" status="0"/>
<zip code="45613" office_id="KY0069" status="1"/>
<zip code="45613" office_id="KYFFFF" status="0"/>
<zip code="45614" office_id="KY0069" status="1"/>
<zip code="45614" office_id="KYFFFF" status="0"/>
<zip code="45616" office_id="KY0062" status="1"/>
<zip code="45616" office_id="KYFFFF" status="0"/>
<zip code="45617" office_id="KY0062" status="1"/>
<zip code="45617" office_id="KYFFFF" status="0"/>
<zip code="45618" office_id="KY0062" status="1"/>
<zip code="45618" office_id="KYFFFF" status="0"/>
<zip code="45619" office_id="KY0069" status="1"/>
<zip code="45619" office_id="KYFFFF" status="0"/>
<zip code="45620" office_id="KY0069" status="1"/>
<zip code="45620" office_id="KYFFFF" status="0"/>
<zip code="45621" office_id="KY0069" status="1"/>
<zip code="45621" office_id="KYFFFF" status="0"/>
<zip code="45623" office_id="KY0069" status="1"/>
<zip code="45623" office_id="KYFFFF" status="0"/>
<zip code="45624" office_id="KY0069" status="1"/>
<zip code="45624" office_id="KYFFFF" status="0"/>
<zip code="45628" office_id="KY0062" status="1"/>
<zip code="45628" office_id="KYFFFF" status="0"/>
<zip code="45629" office_id="KY0069" status="1"/>
<zip code="45629" office_id="KYFFFF" status="0"/>
<zip code="45630" office_id="KY0069" status="1"/>
<zip code="45630" office_id="KYFFFF" status="0"/>
<zip code="45631" office_id="KY0069" status="1"/>
<zip code="45631" office_id="KYFFFF" status="0"/>
<zip code="45633" office_id="KY0062" status="1"/>
<zip code="45633" office_id="KYFFFF" status="0"/>
<zip code="45634" office_id="KY0069" status="1"/>
<zip code="45634" office_id="KYFFFF" status="0"/>
<zip code="45636" office_id="KY0069" status="1"/>
<zip code="45636" office_id="KYFFFF" status="0"/>
<zip code="45638" office_id="KY0069" status="1"/>
<zip code="45638" office_id="KYFFFF" status="0"/>
<zip code="45640" office_id="KY0069" status="1"/>
<zip code="45640" office_id="KYFFFF" status="0"/>
<zip code="45642" office_id="KY0069" status="1"/>
<zip code="45642" office_id="KYFFFF" status="0"/>
<zip code="45643" office_id="KY0069" status="1"/>
<zip code="45643" office_id="KYFFFF" status="0"/>
<zip code="45644" office_id="KY0062" status="1"/>
<zip code="45644" office_id="KYFFFF" status="0"/>
<zip code="45645" office_id="KY0069" status="1"/>
<zip code="45645" office_id="KYFFFF" status="0"/>
<zip code="45646" office_id="KY0069" status="1"/>
<zip code="45646" office_id="KYFFFF" status="0"/>
<zip code="45647" office_id="KY0062" status="1"/>
<zip code="45647" office_id="KYFFFF" status="0"/>
<zip code="45648" office_id="KY0069" status="1"/>
<zip code="45648" office_id="KYFFFF" status="0"/>
<zip code="45650" office_id="KY0062" status="1"/>
<zip code="45650" office_id="KYFFFF" status="0"/>
<zip code="45651" office_id="KY0069" status="1"/>
<zip code="45651" office_id="KYFFFF" status="0"/>
<zip code="45652" office_id="KY0069" status="1"/>
<zip code="45652" office_id="KYFFFF" status="0"/>
<zip code="45653" office_id="KY0069" status="1"/>
<zip code="45653" office_id="KYFFFF" status="0"/>
<zip code="45656" office_id="KY0069" status="1"/>
<zip code="45656" office_id="KYFFFF" status="0"/>
<zip code="45657" office_id="KY0062" status="1"/>
<zip code="45657" office_id="KYFFFF" status="0"/>
<zip code="45658" office_id="KY0069" status="1"/>
<zip code="45658" office_id="KYFFFF" status="0"/>
<zip code="45659" office_id="KY0069" status="1"/>
<zip code="45659" office_id="KYFFFF" status="0"/>
<zip code="45660" office_id="KY0062" status="1"/>
<zip code="45660" office_id="KYFFFF" status="0"/>
<zip code="45661" office_id="KY0069" status="1"/>
<zip code="45661" office_id="KYFFFF" status="0"/>
<zip code="45662" office_id="KY0069" status="1"/>
<zip code="45662" office_id="KYFFFF" status="0"/>
<zip code="45663" office_id="KY0069" status="1"/>
<zip code="45663" office_id="KYFFFF" status="0"/>
<zip code="45669" office_id="KY0069" status="1"/>
<zip code="45669" office_id="KYFFFF" status="0"/>
<zip code="45671" office_id="KY0069" status="1"/>
<zip code="45671" office_id="KYFFFF" status="0"/>
<zip code="45672" office_id="KY0069" status="1"/>
<zip code="45672" office_id="KYFFFF" status="0"/>
<zip code="45673" office_id="KY0062" status="1"/>
<zip code="45673" office_id="KYFFFF" status="0"/>
<zip code="45674" office_id="KY0069" status="1"/>
<zip code="45674" office_id="KYFFFF" status="0"/>
<zip code="45675" office_id="KY0069" status="1"/>
<zip code="45675" office_id="KYFFFF" status="0"/>
<zip code="45677" office_id="KY0069" status="1"/>
<zip code="45677" office_id="KYFFFF" status="0"/>
<zip code="45678" office_id="KY0069" status="1"/>
<zip code="45678" office_id="KYFFFF" status="0"/>
<zip code="45679" office_id="KY0062" status="1"/>
<zip code="45679" office_id="KYFFFF" status="0"/>
<zip code="45680" office_id="KY0069" status="1"/>
<zip code="45680" office_id="KYFFFF" status="0"/>
<zip code="45681" office_id="KY0062" status="1"/>
<zip code="45681" office_id="KYFFFF" status="0"/>
<zip code="45682" office_id="KY0069" status="1"/>
<zip code="45682" office_id="KYFFFF" status="0"/>
<zip code="45683" office_id="KY0069" status="1"/>
<zip code="45683" office_id="KYFFFF" status="0"/>
<zip code="45684" office_id="KY0062" status="1"/>
<zip code="45684" office_id="KYFFFF" status="0"/>
<zip code="45685" office_id="KY0069" status="1"/>
<zip code="45685" office_id="KYFFFF" status="0"/>
<zip code="45686" office_id="KY0069" status="1"/>
<zip code="45686" office_id="KYFFFF" status="0"/>
<zip code="45687" office_id="KY0069" status="1"/>
<zip code="45687" office_id="KYFFFF" status="0"/>
<zip code="45688" office_id="KY0069" status="1"/>
<zip code="45688" office_id="KYFFFF" status="0"/>
<zip code="45690" office_id="KY0069" status="1"/>
<zip code="45690" office_id="KYFFFF" status="0"/>
<zip code="45692" office_id="KY0069" status="1"/>
<zip code="45692" office_id="KYFFFF" status="0"/>
<zip code="45693" office_id="KY0062" status="1"/>
<zip code="45693" office_id="KYFFFF" status="0"/>
<zip code="45694" office_id="KY0069" status="1"/>
<zip code="45694" office_id="KYFFFF" status="0"/>
<zip code="45695" office_id="KY0069" status="1"/>
<zip code="45695" office_id="KYFFFF" status="0"/>
<zip code="45696" office_id="KY0069" status="1"/>
<zip code="45696" office_id="KYFFFF" status="0"/>
<zip code="45697" office_id="KY0062" status="1"/>
<zip code="45697" office_id="KYFFFF" status="0"/>
<zip code="45698" office_id="KY0069" status="1"/>
<zip code="45698" office_id="KYFFFF" status="0"/>
<zip code="45699" office_id="KY0069" status="1"/>
<zip code="45699" office_id="KYFFFF" status="0"/>
<zip code="45720" office_id="KY0069" status="1"/>
<zip code="45720" office_id="KYFFFF" status="0"/>
<zip code="45741" office_id="KY0069" status="1"/>
<zip code="45741" office_id="KYFFFF" status="0"/>
<zip code="45743" office_id="KY0069" status="1"/>
<zip code="45743" office_id="KYFFFF" status="0"/>
<zip code="45760" office_id="KY0069" status="1"/>
<zip code="45760" office_id="KYFFFF" status="0"/>
<zip code="45769" office_id="KY0069" status="1"/>
<zip code="45769" office_id="KYFFFF" status="0"/>
<zip code="45770" office_id="KY0069" status="1"/>
<zip code="45770" office_id="KYFFFF" status="0"/>
<zip code="45771" office_id="KY0069" status="1"/>
<zip code="45771" office_id="KYFFFF" status="0"/>
<zip code="45772" office_id="KY0069" status="1"/>
<zip code="45772" office_id="KYFFFF" status="0"/>
<zip code="45775" office_id="KY0069" status="1"/>
<zip code="45775" office_id="KYFFFF" status="0"/>
<zip code="45779" office_id="KY0069" status="1"/>
<zip code="45779" office_id="KYFFFF" status="0"/>
<zip code="45783" office_id="KY0069" status="1"/>
<zip code="45783" office_id="KYFFFF" status="0"/>
<zip code="45944" office_id="KY0057" status="1"/>
<zip code="45944" office_id="KYEEEE" status="0"/>
<zip code="45999" office_id="KY0057" status="1"/>
<zip code="45999" office_id="KYEEEE" status="0"/>
<zip code="46104" office_id="KY0057" status="1"/>
<zip code="46104" office_id="KYEEEE" status="0"/>
<zip code="46115" office_id="KY0057" status="1"/>
<zip code="46115" office_id="KYEEEE" status="0"/>
<zip code="46124" office_id="KY1056" status="1"/>
<zip code="46124" office_id="KYCCCC" status="0"/>
<zip code="46127" office_id="KY0057" status="1"/>
<zip code="46127" office_id="KYEEEE" status="0"/>
<zip code="46133" office_id="KY0057" status="1"/>
<zip code="46133" office_id="KYEEEE" status="0"/>
<zip code="46146" office_id="KY0057" status="1"/>
<zip code="46146" office_id="KYEEEE" status="0"/>
<zip code="46155" office_id="KY0057" status="1"/>
<zip code="46155" office_id="KYEEEE" status="0"/>
<zip code="46156" office_id="KY0057" status="1"/>
<zip code="46156" office_id="KYEEEE" status="0"/>
<zip code="46173" office_id="KY0057" status="1"/>
<zip code="46173" office_id="KYEEEE" status="0"/>
<zip code="47001" office_id="KY0057" status="1"/>
<zip code="47001" office_id="KYEEEE" status="0"/>
<zip code="47003" office_id="KY0057" status="1"/>
<zip code="47003" office_id="KYEEEE" status="0"/>
<zip code="47006" office_id="KY0057" status="1"/>
<zip code="47006" office_id="KYEEEE" status="0"/>
<zip code="47010" office_id="KY0057" status="1"/>
<zip code="47010" office_id="KYEEEE" status="0"/>
<zip code="47011" office_id="KY0057" status="1"/>
<zip code="47011" office_id="KYEEEE" status="0"/>
<zip code="47012" office_id="KY0057" status="1"/>
<zip code="47012" office_id="KYEEEE" status="0"/>
<zip code="47016" office_id="KY0057" status="1"/>
<zip code="47016" office_id="KYEEEE" status="0"/>
<zip code="47017" office_id="KY0057" status="1"/>
<zip code="47017" office_id="KYEEEE" status="0"/>
<zip code="47018" office_id="KY0057" status="1"/>
<zip code="47018" office_id="KYEEEE" status="0"/>
<zip code="47019" office_id="KY0057" status="1"/>
<zip code="47019" office_id="KYEEEE" status="0"/>
<zip code="47020" office_id="KY0057" status="1"/>
<zip code="47020" office_id="KYEEEE" status="0"/>
<zip code="47021" office_id="KY0057" status="1"/>
<zip code="47021" office_id="KYEEEE" status="0"/>
<zip code="47022" office_id="KY0057" status="1"/>
<zip code="47022" office_id="KYEEEE" status="0"/>
<zip code="47023" office_id="KY0057" status="1"/>
<zip code="47023" office_id="KYEEEE" status="0"/>
<zip code="47024" office_id="KY0057" status="1"/>
<zip code="47024" office_id="KYEEEE" status="0"/>
<zip code="47025" office_id="KY0057" status="1"/>
<zip code="47025" office_id="KYEEEE" status="0"/>
<zip code="47030" office_id="KY0057" status="1"/>
<zip code="47030" office_id="KYEEEE" status="0"/>
<zip code="47031" office_id="KY0057" status="1"/>
<zip code="47031" office_id="KYEEEE" status="0"/>
<zip code="47032" office_id="KY0057" status="1"/>
<zip code="47032" office_id="KYEEEE" status="0"/>
<zip code="47033" office_id="KY0057" status="1"/>
<zip code="47033" office_id="KYEEEE" status="0"/>
<zip code="47034" office_id="KY0057" status="1"/>
<zip code="47034" office_id="KYEEEE" status="0"/>
<zip code="47035" office_id="KY0057" status="1"/>
<zip code="47035" office_id="KYEEEE" status="0"/>
<zip code="47036" office_id="KY0057" status="1"/>
<zip code="47036" office_id="KYEEEE" status="0"/>
<zip code="47037" office_id="KY0057" status="1"/>
<zip code="47037" office_id="KYEEEE" status="0"/>
<zip code="47038" office_id="KY0057" status="1"/>
<zip code="47038" office_id="KYEEEE" status="0"/>
<zip code="47039" office_id="KY0057" status="1"/>
<zip code="47039" office_id="KYEEEE" status="0"/>
<zip code="47040" office_id="KY0057" status="1"/>
<zip code="47040" office_id="KYEEEE" status="0"/>
<zip code="47041" office_id="KY0057" status="1"/>
<zip code="47041" office_id="KYEEEE" status="0"/>
<zip code="47042" office_id="KY0057" status="1"/>
<zip code="47042" office_id="KYEEEE" status="0"/>
<zip code="47043" office_id="KY0057" status="1"/>
<zip code="47043" office_id="KYEEEE" status="0"/>
<zip code="47060" office_id="KY0057" status="1"/>
<zip code="47060" office_id="KYEEEE" status="0"/>
<zip code="47102" office_id="KY1056" status="1"/>
<zip code="47102" office_id="KYCCCC" status="0"/>
<zip code="47104" office_id="KY1056" status="1"/>
<zip code="47104" office_id="KYCCCC" status="0"/>
<zip code="47106" office_id="KY1056" status="1"/>
<zip code="47106" office_id="KYCCCC" status="0"/>
<zip code="47107" office_id="KY1056" status="1"/>
<zip code="47107" office_id="KYCCCC" status="0"/>
<zip code="47108" office_id="KY1056" status="1"/>
<zip code="47108" office_id="KYCCCC" status="0"/>
<zip code="47110" office_id="KY1056" status="1"/>
<zip code="47110" office_id="KYCCCC" status="0"/>
<zip code="47111" office_id="KY1056" status="1"/>
<zip code="47111" office_id="KYCCCC" status="0"/>
<zip code="47112" office_id="KY1056" status="1"/>
<zip code="47112" office_id="KYCCCC" status="0"/>
<zip code="47114" office_id="KY1056" status="1"/>
<zip code="47114" office_id="KYCCCC" status="0"/>
<zip code="47115" office_id="KY1056" status="1"/>
<zip code="47115" office_id="KYCCCC" status="0"/>
<zip code="47116" office_id="KY1056" status="1"/>
<zip code="47116" office_id="KYCCCC" status="0"/>
<zip code="47117" office_id="KY1056" status="1"/>
<zip code="47117" office_id="KYCCCC" status="0"/>
<zip code="47118" office_id="KY1056" status="1"/>
<zip code="47118" office_id="KYCCCC" status="0"/>
<zip code="47119" office_id="KY1056" status="1"/>
<zip code="47119" office_id="KYCCCC" status="0"/>
<zip code="47120" office_id="KY1056" status="1"/>
<zip code="47120" office_id="KYCCCC" status="0"/>
<zip code="47122" office_id="KY1056" status="1"/>
<zip code="47122" office_id="KYCCCC" status="0"/>
<zip code="47123" office_id="KY1056" status="1"/>
<zip code="47123" office_id="KYCCCC" status="0"/>
<zip code="47124" office_id="KY1056" status="1"/>
<zip code="47124" office_id="KYCCCC" status="0"/>
<zip code="47125" office_id="KY1056" status="1"/>
<zip code="47125" office_id="KYCCCC" status="0"/>
<zip code="47126" office_id="KY1056" status="1"/>
<zip code="47126" office_id="KYCCCC" status="0"/>
<zip code="47129" office_id="KY1056" status="1"/>
<zip code="47129" office_id="KYCCCC" status="0"/>
<zip code="47130" office_id="KY1056" status="1"/>
<zip code="47130" office_id="KYCCCC" status="0"/>
<zip code="47131" office_id="KY1056" status="1"/>
<zip code="47131" office_id="KYCCCC" status="0"/>
<zip code="47132" office_id="KY1056" status="1"/>
<zip code="47132" office_id="KYCCCC" status="0"/>
<zip code="47133" office_id="KY1056" status="1"/>
<zip code="47133" office_id="KYCCCC" status="0"/>
<zip code="47134" office_id="KY1056" status="1"/>
<zip code="47134" office_id="KYCCCC" status="0"/>
<zip code="47135" office_id="KY1056" status="1"/>
<zip code="47135" office_id="KYCCCC" status="0"/>
<zip code="47136" office_id="KY1056" status="1"/>
<zip code="47136" office_id="KYCCCC" status="0"/>
<zip code="47137" office_id="KY1056" status="1"/>
<zip code="47137" office_id="KYCCCC" status="0"/>
<zip code="47138" office_id="KY1056" status="1"/>
<zip code="47138" office_id="KYCCCC" status="0"/>
<zip code="47139" office_id="KY1056" status="1"/>
<zip code="47139" office_id="KYCCCC" status="0"/>
<zip code="47140" office_id="KY1056" status="1"/>
<zip code="47140" office_id="KYCCCC" status="0"/>
<zip code="47141" office_id="KY1056" status="1"/>
<zip code="47141" office_id="KYCCCC" status="0"/>
<zip code="47142" office_id="KY1056" status="1"/>
<zip code="47142" office_id="KYCCCC" status="0"/>
<zip code="47143" office_id="KY1056" status="1"/>
<zip code="47143" office_id="KYCCCC" status="0"/>
<zip code="47144" office_id="KY1056" status="1"/>
<zip code="47144" office_id="KYCCCC" status="0"/>
<zip code="47145" office_id="KY1056" status="1"/>
<zip code="47145" office_id="KYCCCC" status="0"/>
<zip code="47146" office_id="KY1056" status="1"/>
<zip code="47146" office_id="KYCCCC" status="0"/>
<zip code="47147" office_id="KY1056" status="1"/>
<zip code="47147" office_id="KYCCCC" status="0"/>
<zip code="47150" office_id="KY1056" status="1"/>
<zip code="47150" office_id="KYCCCC" status="0"/>
<zip code="47151" office_id="KY1056" status="1"/>
<zip code="47151" office_id="KYCCCC" status="0"/>
<zip code="47160" office_id="KY1056" status="1"/>
<zip code="47160" office_id="KYCCCC" status="0"/>
<zip code="47161" office_id="KY1056" status="1"/>
<zip code="47161" office_id="KYCCCC" status="0"/>
<zip code="47162" office_id="KY1056" status="1"/>
<zip code="47162" office_id="KYCCCC" status="0"/>
<zip code="47163" office_id="KY1056" status="1"/>
<zip code="47163" office_id="KYCCCC" status="0"/>
<zip code="47164" office_id="KY1056" status="1"/>
<zip code="47164" office_id="KYCCCC" status="0"/>
<zip code="47165" office_id="KY1056" status="1"/>
<zip code="47165" office_id="KYCCCC" status="0"/>
<zip code="47166" office_id="KY1056" status="1"/>
<zip code="47166" office_id="KYCCCC" status="0"/>
<zip code="47167" office_id="KY1056" status="1"/>
<zip code="47167" office_id="KYCCCC" status="0"/>
<zip code="47170" office_id="KY1056" status="1"/>
<zip code="47170" office_id="KYCCCC" status="0"/>
<zip code="47172" office_id="KY1056" status="1"/>
<zip code="47172" office_id="KYCCCC" status="0"/>
<zip code="47174" office_id="KY1056" status="1"/>
<zip code="47174" office_id="KYCCCC" status="0"/>
<zip code="47175" office_id="KY1056" status="1"/>
<zip code="47175" office_id="KYCCCC" status="0"/>
<zip code="47177" office_id="KY1056" status="1"/>
<zip code="47177" office_id="KYCCCC" status="0"/>
<zip code="47199" office_id="KY1056" status="1"/>
<zip code="47199" office_id="KYCCCC" status="0"/>
<zip code="47201" office_id="KY1056" status="1"/>
<zip code="47201" office_id="KYCCCC" status="0"/>
<zip code="47202" office_id="KY1056" status="1"/>
<zip code="47202" office_id="KYCCCC" status="0"/>
<zip code="47203" office_id="KY1056" status="1"/>
<zip code="47203" office_id="KYCCCC" status="0"/>
<zip code="47220" office_id="KY1056" status="1"/>
<zip code="47220" office_id="KYCCCC" status="0"/>
<zip code="47223" office_id="KY1056" status="1"/>
<zip code="47223" office_id="KYCCCC" status="0"/>
<zip code="47224" office_id="KY1056" status="1"/>
<zip code="47224" office_id="KYCCCC" status="0"/>
<zip code="47225" office_id="KY0057" status="1"/>
<zip code="47225" office_id="KYEEEE" status="0"/>
<zip code="47226" office_id="KY1056" status="1"/>
<zip code="47226" office_id="KYCCCC" status="0"/>
<zip code="47227" office_id="KY1056" status="1"/>
<zip code="47227" office_id="KYCCCC" status="0"/>
<zip code="47228" office_id="KY1056" status="1"/>
<zip code="47228" office_id="KYCCCC" status="0"/>
<zip code="47229" office_id="KY1056" status="1"/>
<zip code="47229" office_id="KYCCCC" status="0"/>
<zip code="47230" office_id="KY1056" status="1"/>
<zip code="47230" office_id="KYCCCC" status="0"/>
<zip code="47231" office_id="KY1056" status="1"/>
<zip code="47231" office_id="KYCCCC" status="0"/>
<zip code="47232" office_id="KY1056" status="1"/>
<zip code="47232" office_id="KYCCCC" status="0"/>
<zip code="47235" office_id="KY1056" status="1"/>
<zip code="47235" office_id="KYCCCC" status="0"/>
<zip code="47236" office_id="KY1056" status="1"/>
<zip code="47236" office_id="KYCCCC" status="0"/>
<zip code="47240" office_id="KY0057" status="1"/>
<zip code="47240" office_id="KYEEEE" status="0"/>
<zip code="47243" office_id="KY1056" status="1"/>
<zip code="47243" office_id="KYCCCC" status="0"/>
<zip code="47244" office_id="KY1056" status="1"/>
<zip code="47244" office_id="KYCCCC" status="0"/>
<zip code="47245" office_id="KY1056" status="1"/>
<zip code="47245" office_id="KYCCCC" status="0"/>
<zip code="47246" office_id="KY1056" status="1"/>
<zip code="47246" office_id="KYCCCC" status="0"/>
<zip code="47247" office_id="KY1056" status="1"/>
<zip code="47247" office_id="KYCCCC" status="0"/>
<zip code="47249" office_id="KY1056" status="1"/>
<zip code="47249" office_id="KYCCCC" status="0"/>
<zip code="47250" office_id="KY1056" status="1"/>
<zip code="47250" office_id="KYCCCC" status="0"/>
<zip code="47260" office_id="KY1056" status="1"/>
<zip code="47260" office_id="KYCCCC" status="0"/>
<zip code="47261" office_id="KY0057" status="1"/>
<zip code="47261" office_id="KYEEEE" status="0"/>
<zip code="47262" office_id="KY1056" status="1"/>
<zip code="47262" office_id="KYCCCC" status="0"/>
<zip code="47263" office_id="KY0057" status="1"/>
<zip code="47263" office_id="KYEEEE" status="0"/>
<zip code="47264" office_id="KY1056" status="1"/>
<zip code="47264" office_id="KYCCCC" status="0"/>
<zip code="47265" office_id="KY1056" status="1"/>
<zip code="47265" office_id="KYCCCC" status="0"/>
<zip code="47270" office_id="KY1056" status="1"/>
<zip code="47270" office_id="KYCCCC" status="0"/>
<zip code="47272" office_id="KY0057" status="1"/>
<zip code="47272" office_id="KYEEEE" status="0"/>
<zip code="47273" office_id="KY1056" status="1"/>
<zip code="47273" office_id="KYCCCC" status="0"/>
<zip code="47274" office_id="KY1056" status="1"/>
<zip code="47274" office_id="KYCCCC" status="0"/>
<zip code="47280" office_id="KY1056" status="1"/>
<zip code="47280" office_id="KYCCCC" status="0"/>
<zip code="47281" office_id="KY1056" status="1"/>
<zip code="47281" office_id="KYCCCC" status="0"/>
<zip code="47282" office_id="KY1056" status="1"/>
<zip code="47282" office_id="KYCCCC" status="0"/>
<zip code="47283" office_id="KY0057" status="1"/>
<zip code="47283" office_id="KYEEEE" status="0"/>
<zip code="47322" office_id="KY0057" status="1"/>
<zip code="47322" office_id="KYEEEE" status="0"/>
<zip code="47324" office_id="KY0057" status="1"/>
<zip code="47324" office_id="KYEEEE" status="0"/>
<zip code="47325" office_id="KY0057" status="1"/>
<zip code="47325" office_id="KYEEEE" status="0"/>
<zip code="47327" office_id="KY0057" status="1"/>
<zip code="47327" office_id="KYEEEE" status="0"/>
<zip code="47330" office_id="KY0057" status="1"/>
<zip code="47330" office_id="KYEEEE" status="0"/>
<zip code="47331" office_id="KY0057" status="1"/>
<zip code="47331" office_id="KYEEEE" status="0"/>
<zip code="47335" office_id="KY0057" status="1"/>
<zip code="47335" office_id="KYEEEE" status="0"/>
<zip code="47339" office_id="KY0057" status="1"/>
<zip code="47339" office_id="KYEEEE" status="0"/>
<zip code="47341" office_id="KY0057" status="1"/>
<zip code="47341" office_id="KYEEEE" status="0"/>
<zip code="47345" office_id="KY0057" status="1"/>
<zip code="47345" office_id="KYEEEE" status="0"/>
<zip code="47346" office_id="KY0057" status="1"/>
<zip code="47346" office_id="KYEEEE" status="0"/>
<zip code="47353" office_id="KY0057" status="1"/>
<zip code="47353" office_id="KYEEEE" status="0"/>
<zip code="47357" office_id="KY0057" status="1"/>
<zip code="47357" office_id="KYEEEE" status="0"/>
<zip code="47370" office_id="KY0057" status="1"/>
<zip code="47370" office_id="KYEEEE" status="0"/>
<zip code="47374" office_id="KY0057" status="1"/>
<zip code="47374" office_id="KYEEEE" status="0"/>
<zip code="47375" office_id="KY0057" status="1"/>
<zip code="47375" office_id="KYEEEE" status="0"/>
<zip code="47392" office_id="KY0057" status="1"/>
<zip code="47392" office_id="KYEEEE" status="0"/>
<zip code="47393" office_id="KY0057" status="1"/>
<zip code="47393" office_id="KYEEEE" status="0"/>
<zip code="47420" office_id="KY1056" status="1"/>
<zip code="47420" office_id="KYCCCC" status="0"/>
<zip code="47421" office_id="KY1056" status="1"/>
<zip code="47421" office_id="KYCCCC" status="0"/>
<zip code="47430" office_id="KY1056" status="1"/>
<zip code="47430" office_id="KYCCCC" status="0"/>
<zip code="47432" office_id="KY1056" status="1"/>
<zip code="47432" office_id="KYCCCC" status="0"/>
<zip code="47437" office_id="KY1056" status="1"/>
<zip code="47437" office_id="KYCCCC" status="0"/>
<zip code="47446" office_id="KY1056" status="1"/>
<zip code="47446" office_id="KYCCCC" status="0"/>
<zip code="47451" office_id="KY1056" status="1"/>
<zip code="47451" office_id="KYCCCC" status="0"/>
<zip code="47452" office_id="KY1056" status="1"/>
<zip code="47452" office_id="KYCCCC" status="0"/>
<zip code="47454" office_id="KY1056" status="1"/>
<zip code="47454" office_id="KYCCCC" status="0"/>
<zip code="47467" office_id="KY1056" status="1"/>
<zip code="47467" office_id="KYCCCC" status="0"/>
<zip code="47469" office_id="KY1056" status="1"/>
<zip code="47469" office_id="KYCCCC" status="0"/>
<zip code="47470" office_id="KY1056" status="1"/>
<zip code="47470" office_id="KYCCCC" status="0"/>
<zip code="47512" office_id="KY0053" status="1"/>
<zip code="47512" office_id="KYJJJJ" status="0"/>
<zip code="47513" office_id="KY0054" status="1"/>
<zip code="47513" office_id="KYJJJJ" status="0"/>
<zip code="47514" office_id="KY0054" status="1"/>
<zip code="47514" office_id="KYJJJJ" status="0"/>
<zip code="47515" office_id="KY0054" status="1"/>
<zip code="47515" office_id="KYJJJJ" status="0"/>
<zip code="47516" office_id="KY0053" status="1"/>
<zip code="47516" office_id="KYJJJJ" status="0"/>
<zip code="47520" office_id="KY0054" status="1"/>
<zip code="47520" office_id="KYJJJJ" status="0"/>
<zip code="47521" office_id="KY0054" status="1"/>
<zip code="47521" office_id="KYJJJJ" status="0"/>
<zip code="47522" office_id="KY0054" status="1"/>
<zip code="47522" office_id="KYJJJJ" status="0"/>
<zip code="47523" office_id="KY0054" status="1"/>
<zip code="47523" office_id="KYJJJJ" status="0"/>
<zip code="47524" office_id="KY0053" status="1"/>
<zip code="47524" office_id="KYJJJJ" status="0"/>
<zip code="47525" office_id="KY0054" status="1"/>
<zip code="47525" office_id="KYJJJJ" status="0"/>
<zip code="47527" office_id="KY0054" status="1"/>
<zip code="47527" office_id="KYJJJJ" status="0"/>
<zip code="47528" office_id="KY0053" status="1"/>
<zip code="47528" office_id="KYJJJJ" status="0"/>
<zip code="47531" office_id="KY0054" status="1"/>
<zip code="47531" office_id="KYJJJJ" status="0"/>
<zip code="47532" office_id="KY0054" status="1"/>
<zip code="47532" office_id="KYJJJJ" status="0"/>
<zip code="47535" office_id="KY0053" status="1"/>
<zip code="47535" office_id="KYJJJJ" status="0"/>
<zip code="47536" office_id="KY0054" status="1"/>
<zip code="47536" office_id="KYJJJJ" status="0"/>
<zip code="47537" office_id="KY0054" status="1"/>
<zip code="47537" office_id="KYJJJJ" status="0"/>
<zip code="47541" office_id="KY0054" status="1"/>
<zip code="47541" office_id="KYJJJJ" status="0"/>
<zip code="47542" office_id="KY0054" status="1"/>
<zip code="47542" office_id="KYJJJJ" status="0"/>
<zip code="47545" office_id="KY0054" status="1"/>
<zip code="47545" office_id="KYJJJJ" status="0"/>
<zip code="47546" office_id="KY0054" status="1"/>
<zip code="47546" office_id="KYJJJJ" status="0"/>
<zip code="47547" office_id="KY0054" status="1"/>
<zip code="47547" office_id="KYJJJJ" status="0"/>
<zip code="47549" office_id="KY0054" status="1"/>
<zip code="47549" office_id="KYJJJJ" status="0"/>
<zip code="47550" office_id="KY0054" status="1"/>
<zip code="47550" office_id="KYJJJJ" status="0"/>
<zip code="47551" office_id="KY0054" status="1"/>
<zip code="47551" office_id="KYJJJJ" status="0"/>
<zip code="47552" office_id="KY0054" status="1"/>
<zip code="47552" office_id="KYJJJJ" status="0"/>
<zip code="47553" office_id="KY0054" status="1"/>
<zip code="47553" office_id="KYJJJJ" status="0"/>
<zip code="47556" office_id="KY0054" status="1"/>
<zip code="47556" office_id="KYJJJJ" status="0"/>
<zip code="47557" office_id="KY0053" status="1"/>
<zip code="47557" office_id="KYJJJJ" status="0"/>
<zip code="47564" office_id="KY0053" status="1"/>
<zip code="47564" office_id="KYJJJJ" status="0"/>
<zip code="47567" office_id="KY0053" status="1"/>
<zip code="47567" office_id="KYJJJJ" status="0"/>
<zip code="47573" office_id="KY0053" status="1"/>
<zip code="47573" office_id="KYJJJJ" status="0"/>
<zip code="47574" office_id="KY0054" status="1"/>
<zip code="47574" office_id="KYJJJJ" status="0"/>
<zip code="47575" office_id="KY0054" status="1"/>
<zip code="47575" office_id="KYJJJJ" status="0"/>
<zip code="47576" office_id="KY0054" status="1"/>
<zip code="47576" office_id="KYJJJJ" status="0"/>
<zip code="47577" office_id="KY0054" status="1"/>
<zip code="47577" office_id="KYJJJJ" status="0"/>
<zip code="47578" office_id="KY0053" status="1"/>
<zip code="47578" office_id="KYJJJJ" status="0"/>
<zip code="47579" office_id="KY0054" status="1"/>
<zip code="47579" office_id="KYJJJJ" status="0"/>
<zip code="47580" office_id="KY0054" status="1"/>
<zip code="47580" office_id="KYJJJJ" status="0"/>
<zip code="47581" office_id="KY0054" status="1"/>
<zip code="47581" office_id="KYJJJJ" status="0"/>
<zip code="47584" office_id="KY0053" status="1"/>
<zip code="47584" office_id="KYJJJJ" status="0"/>
<zip code="47585" office_id="KY0053" status="1"/>
<zip code="47585" office_id="KYJJJJ" status="0"/>
<zip code="47586" office_id="KY0054" status="1"/>
<zip code="47586" office_id="KYJJJJ" status="0"/>
<zip code="47588" office_id="KY0054" status="1"/>
<zip code="47588" office_id="KYJJJJ" status="0"/>
<zip code="47590" office_id="KY0053" status="1"/>
<zip code="47590" office_id="KYJJJJ" status="0"/>
<zip code="47591" office_id="KY0053" status="1"/>
<zip code="47591" office_id="KYJJJJ" status="0"/>
<zip code="47596" office_id="KY0053" status="1"/>
<zip code="47596" office_id="KYJJJJ" status="0"/>
<zip code="47597" office_id="KY0053" status="1"/>
<zip code="47597" office_id="KYJJJJ" status="0"/>
<zip code="47598" office_id="KY0053" status="1"/>
<zip code="47598" office_id="KYJJJJ" status="0"/>
<zip code="47601" office_id="KY0053" status="1"/>
<zip code="47601" office_id="KYJJJJ" status="0"/>
<zip code="47610" office_id="KY0053" status="1"/>
<zip code="47610" office_id="KYJJJJ" status="0"/>
<zip code="47611" office_id="KY0054" status="1"/>
<zip code="47611" office_id="KYJJJJ" status="0"/>
<zip code="47612" office_id="KY0053" status="1"/>
<zip code="47612" office_id="KYJJJJ" status="0"/>
<zip code="47613" office_id="KY0053" status="1"/>
<zip code="47613" office_id="KYJJJJ" status="0"/>
<zip code="47614" office_id="KY0053" status="1"/>
<zip code="47614" office_id="KYJJJJ" status="0"/>
<zip code="47615" office_id="KY0054" status="1"/>
<zip code="47615" office_id="KYJJJJ" status="0"/>
<zip code="47616" office_id="KY0053" status="1"/>
<zip code="47616" office_id="KYJJJJ" status="0"/>
<zip code="47617" office_id="KY0054" status="1"/>
<zip code="47617" office_id="KYJJJJ" status="0"/>
<zip code="47618" office_id="KY0053" status="1"/>
<zip code="47618" office_id="KYJJJJ" status="0"/>
<zip code="47619" office_id="KY0053" status="1"/>
<zip code="47619" office_id="KYJJJJ" status="0"/>
<zip code="47620" office_id="KY0053" status="1"/>
<zip code="47620" office_id="KYJJJJ" status="0"/>
<zip code="47629" office_id="KY0053" status="1"/>
<zip code="47629" office_id="KYJJJJ" status="0"/>
<zip code="47630" office_id="KY0053" status="1"/>
<zip code="47630" office_id="KYJJJJ" status="0"/>
<zip code="47631" office_id="KY0053" status="1"/>
<zip code="47631" office_id="KYJJJJ" status="0"/>
<zip code="47633" office_id="KY0053" status="1"/>
<zip code="47633" office_id="KYJJJJ" status="0"/>
<zip code="47634" office_id="KY0054" status="1"/>
<zip code="47634" office_id="KYJJJJ" status="0"/>
<zip code="47635" office_id="KY0054" status="1"/>
<zip code="47635" office_id="KYJJJJ" status="0"/>
<zip code="47637" office_id="KY0053" status="1"/>
<zip code="47637" office_id="KYJJJJ" status="0"/>
<zip code="47638" office_id="KY0053" status="1"/>
<zip code="47638" office_id="KYJJJJ" status="0"/>
<zip code="47639" office_id="KY0053" status="1"/>
<zip code="47639" office_id="KYJJJJ" status="0"/>
<zip code="47640" office_id="KY0053" status="1"/>
<zip code="47640" office_id="KYJJJJ" status="0"/>
<zip code="47647" office_id="KY0053" status="1"/>
<zip code="47647" office_id="KYJJJJ" status="0"/>
<zip code="47648" office_id="KY0053" status="1"/>
<zip code="47648" office_id="KYJJJJ" status="0"/>
<zip code="47649" office_id="KY0053" status="1"/>
<zip code="47649" office_id="KYJJJJ" status="0"/>
<zip code="47654" office_id="KY0053" status="1"/>
<zip code="47654" office_id="KYJJJJ" status="0"/>
<zip code="47660" office_id="KY0053" status="1"/>
<zip code="47660" office_id="KYJJJJ" status="0"/>
<zip code="47665" office_id="KY0053" status="1"/>
<zip code="47665" office_id="KYJJJJ" status="0"/>
<zip code="47666" office_id="KY0053" status="1"/>
<zip code="47666" office_id="KYJJJJ" status="0"/>
<zip code="47670" office_id="KY0053" status="1"/>
<zip code="47670" office_id="KYJJJJ" status="0"/>
<zip code="47683" office_id="KY0053" status="1"/>
<zip code="47683" office_id="KYJJJJ" status="0"/>
<zip code="47701" office_id="KY0053" status="1"/>
<zip code="47701" office_id="KYJJJJ" status="0"/>
<zip code="47702" office_id="KY0053" status="1"/>
<zip code="47702" office_id="KYJJJJ" status="0"/>
<zip code="47703" office_id="KY0053" status="1"/>
<zip code="47703" office_id="KYJJJJ" status="0"/>
<zip code="47704" office_id="KY0053" status="1"/>
<zip code="47704" office_id="KYJJJJ" status="0"/>
<zip code="47705" office_id="KY0053" status="1"/>
<zip code="47705" office_id="KYJJJJ" status="0"/>
<zip code="47706" office_id="KY0053" status="1"/>
<zip code="47706" office_id="KYJJJJ" status="0"/>
<zip code="47708" office_id="KY0053" status="1"/>
<zip code="47708" office_id="KYJJJJ" status="0"/>
<zip code="47710" office_id="KY0053" status="1"/>
<zip code="47710" office_id="KYJJJJ" status="0"/>
<zip code="47711" office_id="KY0053" status="1"/>
<zip code="47711" office_id="KYJJJJ" status="0"/>
<zip code="47712" office_id="KY0053" status="1"/>
<zip code="47712" office_id="KYJJJJ" status="0"/>
<zip code="47713" office_id="KY0053" status="1"/>
<zip code="47713" office_id="KYJJJJ" status="0"/>
<zip code="47714" office_id="KY0053" status="1"/>
<zip code="47714" office_id="KYJJJJ" status="0"/>
<zip code="47715" office_id="KY0053" status="1"/>
<zip code="47715" office_id="KYJJJJ" status="0"/>
<zip code="47716" office_id="KY0053" status="1"/>
<zip code="47716" office_id="KYJJJJ" status="0"/>
<zip code="47719" office_id="KY0053" status="1"/>
<zip code="47719" office_id="KYJJJJ" status="0"/>
<zip code="47720" office_id="KY0053" status="1"/>
<zip code="47720" office_id="KYJJJJ" status="0"/>
<zip code="47721" office_id="KY0053" status="1"/>
<zip code="47721" office_id="KYJJJJ" status="0"/>
<zip code="47722" office_id="KY0053" status="1"/>
<zip code="47722" office_id="KYJJJJ" status="0"/>
<zip code="47724" office_id="KY0053" status="1"/>
<zip code="47724" office_id="KYJJJJ" status="0"/>
<zip code="47727" office_id="KY0053" status="1"/>
<zip code="47727" office_id="KYJJJJ" status="0"/>
<zip code="47728" office_id="KY0053" status="1"/>
<zip code="47728" office_id="KYJJJJ" status="0"/>
<zip code="47730" office_id="KY0053" status="1"/>
<zip code="47730" office_id="KYJJJJ" status="0"/>
<zip code="47731" office_id="KY0053" status="1"/>
<zip code="47731" office_id="KYJJJJ" status="0"/>
<zip code="47732" office_id="KY0053" status="1"/>
<zip code="47732" office_id="KYJJJJ" status="0"/>
<zip code="47733" office_id="KY0053" status="1"/>
<zip code="47733" office_id="KYJJJJ" status="0"/>
<zip code="47734" office_id="KY0053" status="1"/>
<zip code="47734" office_id="KYJJJJ" status="0"/>
<zip code="47735" office_id="KY0053" status="1"/>
<zip code="47735" office_id="KYJJJJ" status="0"/>
<zip code="47736" office_id="KY0053" status="1"/>
<zip code="47736" office_id="KYJJJJ" status="0"/>
<zip code="47737" office_id="KY0053" status="1"/>
<zip code="47737" office_id="KYJJJJ" status="0"/>
<zip code="47739" office_id="KY0053" status="1"/>
<zip code="47739" office_id="KYJJJJ" status="0"/>
<zip code="47740" office_id="KY0053" status="1"/>
<zip code="47740" office_id="KYJJJJ" status="0"/>
<zip code="47741" office_id="KY0053" status="1"/>
<zip code="47741" office_id="KYJJJJ" status="0"/>
<zip code="47744" office_id="KY0053" status="1"/>
<zip code="47744" office_id="KYJJJJ" status="0"/>
<zip code="47747" office_id="KY0053" status="1"/>
<zip code="47747" office_id="KYJJJJ" status="0"/>
<zip code="47750" office_id="KY0053" status="1"/>
<zip code="47750" office_id="KYJJJJ" status="0"/>
<zip code="62280" office_id="KY0050" status="1"/>
<zip code="62280" office_id="KYAAAA" status="0"/>
<zip code="62410" office_id="KY0053" status="1"/>
<zip code="62410" office_id="KYJJJJ" status="0"/>
<zip code="62446" office_id="KY0053" status="1"/>
<zip code="62446" office_id="KYJJJJ" status="0"/>
<zip code="62476" office_id="KY0053" status="1"/>
<zip code="62476" office_id="KYJJJJ" status="0"/>
<zip code="62805" office_id="KY0050" status="1"/>
<zip code="62805" office_id="KYAAAA" status="0"/>
<zip code="62806" office_id="KY0053" status="1"/>
<zip code="62806" office_id="KYJJJJ" status="0"/>
<zip code="62809" office_id="KY0053" status="1"/>
<zip code="62809" office_id="KYJJJJ" status="0"/>
<zip code="62811" office_id="KY0053" status="1"/>
<zip code="62811" office_id="KYJJJJ" status="0"/>
<zip code="62812" office_id="KY0050" status="1"/>
<zip code="62812" office_id="KYAAAA" status="0"/>
<zip code="62815" office_id="KY0053" status="1"/>
<zip code="62815" office_id="KYJJJJ" status="0"/>
<zip code="62817" office_id="KY0053" status="1"/>
<zip code="62817" office_id="KYJJJJ" status="0"/>
<zip code="62818" office_id="KY0053" status="1"/>
<zip code="62818" office_id="KYJJJJ" status="0"/>
<zip code="62819" office_id="KY0050" status="1"/>
<zip code="62819" office_id="KYAAAA" status="0"/>
<zip code="62820" office_id="KY0053" status="1"/>
<zip code="62820" office_id="KYJJJJ" status="0"/>
<zip code="62821" office_id="KY0053" status="1"/>
<zip code="62821" office_id="KYJJJJ" status="0"/>
<zip code="62822" office_id="KY0050" status="1"/>
<zip code="62822" office_id="KYAAAA" status="0"/>
<zip code="62823" office_id="KY0053" status="1"/>
<zip code="62823" office_id="KYJJJJ" status="0"/>
<zip code="62825" office_id="KY0050" status="1"/>
<zip code="62825" office_id="KYAAAA" status="0"/>
<zip code="62827" office_id="KY0053" status="1"/>
<zip code="62827" office_id="KYJJJJ" status="0"/>
<zip code="62828" office_id="KY0053" status="1"/>
<zip code="62828" office_id="KYJJJJ" status="0"/>
<zip code="62829" office_id="KY0053" status="1"/>
<zip code="62829" office_id="KYJJJJ" status="0"/>
<zip code="62832" office_id="KY0050" status="1"/>
<zip code="62832" office_id="KYAAAA" status="0"/>
<zip code="62833" office_id="KY0053" status="1"/>
<zip code="62833" office_id="KYJJJJ" status="0"/>
<zip code="62834" office_id="KY0053" status="1"/>
<zip code="62834" office_id="KYJJJJ" status="0"/>
<zip code="62835" office_id="KY0053" status="1"/>
<zip code="62835" office_id="KYJJJJ" status="0"/>
<zip code="62836" office_id="KY0050" status="1"/>
<zip code="62836" office_id="KYAAAA" status="0"/>
<zip code="62837" office_id="KY0053" status="1"/>
<zip code="62837" office_id="KYJJJJ" status="0"/>
<zip code="62840" office_id="KY0050" status="1"/>
<zip code="62840" office_id="KYAAAA" status="0"/>
<zip code="62841" office_id="KY0050" status="1"/>
<zip code="62841" office_id="KYAAAA" status="0"/>
<zip code="62842" office_id="KY0053" status="1"/>
<zip code="62842" office_id="KYJJJJ" status="0"/>
<zip code="62843" office_id="KY0053" status="1"/>
<zip code="62843" office_id="KYJJJJ" status="0"/>
<zip code="62844" office_id="KY0053" status="1"/>
<zip code="62844" office_id="KYJJJJ" status="0"/>
<zip code="62845" office_id="KY0053" status="1"/>
<zip code="62845" office_id="KYJJJJ" status="0"/>
<zip code="62850" office_id="KY0053" status="1"/>
<zip code="62850" office_id="KYJJJJ" status="0"/>
<zip code="62851" office_id="KY0053" status="1"/>
<zip code="62851" office_id="KYJJJJ" status="0"/>
<zip code="62852" office_id="KY0053" status="1"/>
<zip code="62852" office_id="KYJJJJ" status="0"/>
<zip code="62855" office_id="KY0053" status="1"/>
<zip code="62855" office_id="KYJJJJ" status="0"/>
<zip code="62856" office_id="KY0050" status="1"/>
<zip code="62856" office_id="KYAAAA" status="0"/>
<zip code="62859" office_id="KY0053" status="1"/>
<zip code="62859" office_id="KYJJJJ" status="0"/>
<zip code="62860" office_id="KY0050" status="1"/>
<zip code="62860" office_id="KYAAAA" status="0"/>
<zip code="62861" office_id="KY0053" status="1"/>
<zip code="62861" office_id="KYJJJJ" status="0"/>
<zip code="62862" office_id="KY0053" status="1"/>
<zip code="62862" office_id="KYJJJJ" status="0"/>
<zip code="62863" office_id="KY0053" status="1"/>
<zip code="62863" office_id="KYJJJJ" status="0"/>
<zip code="62865" office_id="KY0050" status="1"/>
<zip code="62865" office_id="KYAAAA" status="0"/>
<zip code="62867" office_id="KY0053" status="1"/>
<zip code="62867" office_id="KYJJJJ" status="0"/>
<zip code="62869" office_id="KY0053" status="1"/>
<zip code="62869" office_id="KYJJJJ" status="0"/>
<zip code="62871" office_id="KY0053" status="1"/>
<zip code="62871" office_id="KYJJJJ" status="0"/>
<zip code="62874" office_id="KY0050" status="1"/>
<zip code="62874" office_id="KYAAAA" status="0"/>
<zip code="62878" office_id="KY0053" status="1"/>
<zip code="62878" office_id="KYJJJJ" status="0"/>
<zip code="62884" office_id="KY0050" status="1"/>
<zip code="62884" office_id="KYAAAA" status="0"/>
<zip code="62886" office_id="KY0053" status="1"/>
<zip code="62886" office_id="KYJJJJ" status="0"/>
<zip code="62887" office_id="KY0053" status="1"/>
<zip code="62887" office_id="KYJJJJ" status="0"/>
<zip code="62890" office_id="KY0050" status="1"/>
<zip code="62890" office_id="KYAAAA" status="0"/>
<zip code="62891" office_id="KY0050" status="1"/>
<zip code="62891" office_id="KYAAAA" status="0"/>
<zip code="62895" office_id="KY0053" status="1"/>
<zip code="62895" office_id="KYJJJJ" status="0"/>
<zip code="62896" office_id="KY0050" status="1"/>
<zip code="62896" office_id="KYAAAA" status="0"/>
<zip code="62897" office_id="KY0050" status="1"/>
<zip code="62897" office_id="KYAAAA" status="0"/>
<zip code="62901" office_id="KY0050" status="1"/>
<zip code="62901" office_id="KYAAAA" status="0"/>
<zip code="62902" office_id="KY0050" status="1"/>
<zip code="62902" office_id="KYAAAA" status="0"/>
<zip code="62903" office_id="KY0050" status="1"/>
<zip code="62903" office_id="KYAAAA" status="0"/>
<zip code="62905" office_id="KY0050" status="1"/>
<zip code="62905" office_id="KYAAAA" status="0"/>
<zip code="62906" office_id="KY0050" status="1"/>
<zip code="62906" office_id="KYAAAA" status="0"/>
<zip code="62907" office_id="KY0050" status="1"/>
<zip code="62907" office_id="KYAAAA" status="0"/>
<zip code="62908" office_id="KY0050" status="1"/>
<zip code="62908" office_id="KYAAAA" status="0"/>
<zip code="62909" office_id="KY0050" status="1"/>
<zip code="62909" office_id="KYAAAA" status="0"/>
<zip code="62910" office_id="KY0050" status="1"/>
<zip code="62910" office_id="KYAAAA" status="0"/>
<zip code="62912" office_id="KY0050" status="1"/>
<zip code="62912" office_id="KYAAAA" status="0"/>
<zip code="62913" office_id="KY0050" status="1"/>
<zip code="62913" office_id="KYAAAA" status="0"/>
<zip code="62914" office_id="KY0050" status="1"/>
<zip code="62914" office_id="KYAAAA" status="0"/>
<zip code="62915" office_id="KY0050" status="1"/>
<zip code="62915" office_id="KYAAAA" status="0"/>
<zip code="62916" office_id="KY0050" status="1"/>
<zip code="62916" office_id="KYAAAA" status="0"/>
<zip code="62917" office_id="KY0050" status="1"/>
<zip code="62917" office_id="KYAAAA" status="0"/>
<zip code="62918" office_id="KY0050" status="1"/>
<zip code="62918" office_id="KYAAAA" status="0"/>
<zip code="62919" office_id="KY0050" status="1"/>
<zip code="62919" office_id="KYAAAA" status="0"/>
<zip code="62920" office_id="KY0050" status="1"/>
<zip code="62920" office_id="KYAAAA" status="0"/>
<zip code="62921" office_id="KY0050" status="1"/>
<zip code="62921" office_id="KYAAAA" status="0"/>
<zip code="62922" office_id="KY0050" status="1"/>
<zip code="62922" office_id="KYAAAA" status="0"/>
<zip code="62923" office_id="KY0050" status="1"/>
<zip code="62923" office_id="KYAAAA" status="0"/>
<zip code="62924" office_id="KY0050" status="1"/>
<zip code="62924" office_id="KYAAAA" status="0"/>
<zip code="62926" office_id="KY0050" status="1"/>
<zip code="62926" office_id="KYAAAA" status="0"/>
<zip code="62927" office_id="KY0050" status="1"/>
<zip code="62927" office_id="KYAAAA" status="0"/>
<zip code="62928" office_id="KY0050" status="1"/>
<zip code="62928" office_id="KYAAAA" status="0"/>
<zip code="62930" office_id="KY0050" status="1"/>
<zip code="62930" office_id="KYAAAA" status="0"/>
<zip code="62931" office_id="KY0053" status="1"/>
<zip code="62931" office_id="KYJJJJ" status="0"/>
<zip code="62932" office_id="KY0050" status="1"/>
<zip code="62932" office_id="KYAAAA" status="0"/>
<zip code="62933" office_id="KY0050" status="1"/>
<zip code="62933" office_id="KYAAAA" status="0"/>
<zip code="62934" office_id="KY0053" status="1"/>
<zip code="62934" office_id="KYJJJJ" status="0"/>
<zip code="62935" office_id="KY0050" status="1"/>
<zip code="62935" office_id="KYAAAA" status="0"/>
<zip code="62938" office_id="KY0050" status="1"/>
<zip code="62938" office_id="KYAAAA" status="0"/>
<zip code="62939" office_id="KY0050" status="1"/>
<zip code="62939" office_id="KYAAAA" status="0"/>
<zip code="62940" office_id="KY0050" status="1"/>
<zip code="62940" office_id="KYAAAA" status="0"/>
<zip code="62941" office_id="KY0050" status="1"/>
<zip code="62941" office_id="KYAAAA" status="0"/>
<zip code="62942" office_id="KY0050" status="1"/>
<zip code="62942" office_id="KYAAAA" status="0"/>
<zip code="62943" office_id="KY0050" status="1"/>
<zip code="62943" office_id="KYAAAA" status="0"/>
<zip code="62944" office_id="KY0050" status="1"/>
<zip code="62944" office_id="KYAAAA" status="0"/>
<zip code="62946" office_id="KY0050" status="1"/>
<zip code="62946" office_id="KYAAAA" status="0"/>
<zip code="62947" office_id="KY0050" status="1"/>
<zip code="62947" office_id="KYAAAA" status="0"/>
<zip code="62948" office_id="KY0050" status="1"/>
<zip code="62948" office_id="KYAAAA" status="0"/>
<zip code="62949" office_id="KY0050" status="1"/>
<zip code="62949" office_id="KYAAAA" status="0"/>
<zip code="62950" office_id="KY0050" status="1"/>
<zip code="62950" office_id="KYAAAA" status="0"/>
<zip code="62951" office_id="KY0050" status="1"/>
<zip code="62951" office_id="KYAAAA" status="0"/>
<zip code="62952" office_id="KY0050" status="1"/>
<zip code="62952" office_id="KYAAAA" status="0"/>
<zip code="62953" office_id="KY0050" status="1"/>
<zip code="62953" office_id="KYAAAA" status="0"/>
<zip code="62954" office_id="KY0053" status="1"/>
<zip code="62954" office_id="KYJJJJ" status="0"/>
<zip code="62955" office_id="KY0050" status="1"/>
<zip code="62955" office_id="KYAAAA" status="0"/>
<zip code="62956" office_id="KY0050" status="1"/>
<zip code="62956" office_id="KYAAAA" status="0"/>
<zip code="62957" office_id="KY0050" status="1"/>
<zip code="62957" office_id="KYAAAA" status="0"/>
<zip code="62958" office_id="KY0050" status="1"/>
<zip code="62958" office_id="KYAAAA" status="0"/>
<zip code="62959" office_id="KY0050" status="1"/>
<zip code="62959" office_id="KYAAAA" status="0"/>
<zip code="62960" office_id="KY0050" status="1"/>
<zip code="62960" office_id="KYAAAA" status="0"/>
<zip code="62961" office_id="KY0050" status="1"/>
<zip code="62961" office_id="KYAAAA" status="0"/>
<zip code="62962" office_id="KY0050" status="1"/>
<zip code="62962" office_id="KYAAAA" status="0"/>
<zip code="62963" office_id="KY0050" status="1"/>
<zip code="62963" office_id="KYAAAA" status="0"/>
<zip code="62964" office_id="KY0050" status="1"/>
<zip code="62964" office_id="KYAAAA" status="0"/>
<zip code="62965" office_id="KY0050" status="1"/>
<zip code="62965" office_id="KYAAAA" status="0"/>
<zip code="62966" office_id="KY0050" status="1"/>
<zip code="62966" office_id="KYAAAA" status="0"/>
<zip code="62967" office_id="KY0050" status="1"/>
<zip code="62967" office_id="KYAAAA" status="0"/>
<zip code="62969" office_id="KY0050" status="1"/>
<zip code="62969" office_id="KYAAAA" status="0"/>
<zip code="62970" office_id="KY0050" status="1"/>
<zip code="62970" office_id="KYAAAA" status="0"/>
<zip code="62971" office_id="KY0050" status="1"/>
<zip code="62971" office_id="KYAAAA" status="0"/>
<zip code="62972" office_id="KY0050" status="1"/>
<zip code="62972" office_id="KYAAAA" status="0"/>
<zip code="62973" office_id="KY0050" status="1"/>
<zip code="62973" office_id="KYAAAA" status="0"/>
<zip code="62974" office_id="KY0050" status="1"/>
<zip code="62974" office_id="KYAAAA" status="0"/>
<zip code="62975" office_id="KY0050" status="1"/>
<zip code="62975" office_id="KYAAAA" status="0"/>
<zip code="62976" office_id="KY0050" status="1"/>
<zip code="62976" office_id="KYAAAA" status="0"/>
<zip code="62977" office_id="KY0050" status="1"/>
<zip code="62977" office_id="KYAAAA" status="0"/>
<zip code="62979" office_id="KY0053" status="1"/>
<zip code="62979" office_id="KYJJJJ" status="0"/>
<zip code="62982" office_id="KY0050" status="1"/>
<zip code="62982" office_id="KYAAAA" status="0"/>
<zip code="62983" office_id="KY0050" status="1"/>
<zip code="62983" office_id="KYAAAA" status="0"/>
<zip code="62984" office_id="KY0053" status="1"/>
<zip code="62984" office_id="KYJJJJ" status="0"/>
<zip code="62985" office_id="KY0050" status="1"/>
<zip code="62985" office_id="KYAAAA" status="0"/>
<zip code="62987" office_id="KY0050" status="1"/>
<zip code="62987" office_id="KYAAAA" status="0"/>
<zip code="62988" office_id="KY0050" status="1"/>
<zip code="62988" office_id="KYAAAA" status="0"/>
<zip code="62990" office_id="KY0050" status="1"/>
<zip code="62990" office_id="KYAAAA" status="0"/>
<zip code="62991" office_id="KY0050" status="1"/>
<zip code="62991" office_id="KYAAAA" status="0"/>
<zip code="62992" office_id="KY0050" status="1"/>
<zip code="62992" office_id="KYAAAA" status="0"/>
<zip code="62993" office_id="KY0050" status="1"/>
<zip code="62993" office_id="KYAAAA" status="0"/>
<zip code="62994" office_id="KY0050" status="1"/>
<zip code="62994" office_id="KYAAAA" status="0"/>
<zip code="62995" office_id="KY0050" status="1"/>
<zip code="62995" office_id="KYAAAA" status="0"/>
<zip code="62996" office_id="KY0050" status="1"/>
<zip code="62996" office_id="KYAAAA" status="0"/>
<zip code="62998" office_id="KY0050" status="1"/>
<zip code="62998" office_id="KYAAAA" status="0"/>
<zip code="62999" office_id="KY0050" status="1"/>
<zip code="62999" office_id="KYAAAA" status="0"/>
<zip code="63662" office_id="KY0050" status="1"/>
<zip code="63662" office_id="KYAAAA" status="0"/>
<zip code="63701" office_id="KY0050" status="1"/>
<zip code="63701" office_id="KYAAAA" status="0"/>
<zip code="63702" office_id="KY0050" status="1"/>
<zip code="63702" office_id="KYAAAA" status="0"/>
<zip code="63703" office_id="KY0050" status="1"/>
<zip code="63703" office_id="KYAAAA" status="0"/>
<zip code="63705" office_id="KY0050" status="1"/>
<zip code="63705" office_id="KYAAAA" status="0"/>
<zip code="63730" office_id="KY0050" status="1"/>
<zip code="63730" office_id="KYAAAA" status="0"/>
<zip code="63735" office_id="KY0050" status="1"/>
<zip code="63735" office_id="KYAAAA" status="0"/>
<zip code="63736" office_id="KY0050" status="1"/>
<zip code="63736" office_id="KYAAAA" status="0"/>
<zip code="63738" office_id="KY0050" status="1"/>
<zip code="63738" office_id="KYAAAA" status="0"/>
<zip code="63739" office_id="KY0050" status="1"/>
<zip code="63739" office_id="KYAAAA" status="0"/>
<zip code="63740" office_id="KY0050" status="1"/>
<zip code="63740" office_id="KYAAAA" status="0"/>
<zip code="63742" office_id="KY0050" status="1"/>
<zip code="63742" office_id="KYAAAA" status="0"/>
<zip code="63743" office_id="KY0050" status="1"/>
<zip code="63743" office_id="KYAAAA" status="0"/>
<zip code="63744" office_id="KY0050" status="1"/>
<zip code="63744" office_id="KYAAAA" status="0"/>
<zip code="63745" office_id="KY0050" status="1"/>
<zip code="63745" office_id="KYAAAA" status="0"/>
<zip code="63747" office_id="KY0050" status="1"/>
<zip code="63747" office_id="KYAAAA" status="0"/>
<zip code="63750" office_id="KY0050" status="1"/>
<zip code="63750" office_id="KYAAAA" status="0"/>
<zip code="63751" office_id="KY0050" status="1"/>
<zip code="63751" office_id="KYAAAA" status="0"/>
<zip code="63752" office_id="KY0050" status="1"/>
<zip code="63752" office_id="KYAAAA" status="0"/>
<zip code="63753" office_id="KY0050" status="1"/>
<zip code="63753" office_id="KYAAAA" status="0"/>
<zip code="63755" office_id="KY0050" status="1"/>
<zip code="63755" office_id="KYAAAA" status="0"/>
<zip code="63758" office_id="KY0050" status="1"/>
<zip code="63758" office_id="KYAAAA" status="0"/>
<zip code="63760" office_id="KY0050" status="1"/>
<zip code="63760" office_id="KYAAAA" status="0"/>
<zip code="63764" office_id="KY0050" status="1"/>
<zip code="63764" office_id="KYAAAA" status="0"/>
<zip code="63766" office_id="KY0050" status="1"/>
<zip code="63766" office_id="KYAAAA" status="0"/>
<zip code="63767" office_id="KY0050" status="1"/>
<zip code="63767" office_id="KYAAAA" status="0"/>
<zip code="63769" office_id="KY0050" status="1"/>
<zip code="63769" office_id="KYAAAA" status="0"/>
<zip code="63771" office_id="KY0050" status="1"/>
<zip code="63771" office_id="KYAAAA" status="0"/>
<zip code="63772" office_id="KY0050" status="1"/>
<zip code="63772" office_id="KYAAAA" status="0"/>
<zip code="63774" office_id="KY0050" status="1"/>
<zip code="63774" office_id="KYAAAA" status="0"/>
<zip code="63779" office_id="KY0050" status="1"/>
<zip code="63779" office_id="KYAAAA" status="0"/>
<zip code="63780" office_id="KY0050" status="1"/>
<zip code="63780" office_id="KYAAAA" status="0"/>
<zip code="63781" office_id="KY0050" status="1"/>
<zip code="63781" office_id="KYAAAA" status="0"/>
<zip code="63782" office_id="KY0050" status="1"/>
<zip code="63782" office_id="KYAAAA" status="0"/>
<zip code="63784" office_id="KY0050" status="1"/>
<zip code="63784" office_id="KYAAAA" status="0"/>
<zip code="63785" office_id="KY0050" status="1"/>
<zip code="63785" office_id="KYAAAA" status="0"/>
<zip code="63787" office_id="KY0050" status="1"/>
<zip code="63787" office_id="KYAAAA" status="0"/>
<zip code="63801" office_id="KY0050" status="1"/>
<zip code="63801" office_id="KYAAAA" status="0"/>
<zip code="63820" office_id="KY0050" status="1"/>
<zip code="63820" office_id="KYAAAA" status="0"/>
<zip code="63821" office_id="KY0050" status="1"/>
<zip code="63821" office_id="KYAAAA" status="0"/>
<zip code="63822" office_id="KY0050" status="1"/>
<zip code="63822" office_id="KYAAAA" status="0"/>
<zip code="63823" office_id="KY0050" status="1"/>
<zip code="63823" office_id="KYAAAA" status="0"/>
<zip code="63824" office_id="KY0050" status="1"/>
<zip code="63824" office_id="KYAAAA" status="0"/>
<zip code="63825" office_id="KY0050" status="1"/>
<zip code="63825" office_id="KYAAAA" status="0"/>
<zip code="63826" office_id="KY0050" status="1"/>
<zip code="63826" office_id="KYAAAA" status="0"/>
<zip code="63827" office_id="KY0050" status="1"/>
<zip code="63827" office_id="KYAAAA" status="0"/>
<zip code="63828" office_id="KY0050" status="1"/>
<zip code="63828" office_id="KYAAAA" status="0"/>
<zip code="63829" office_id="KY0050" status="1"/>
<zip code="63829" office_id="KYAAAA" status="0"/>
<zip code="63830" office_id="KY0050" status="1"/>
<zip code="63830" office_id="KYAAAA" status="0"/>
<zip code="63833" office_id="KY0050" status="1"/>
<zip code="63833" office_id="KYAAAA" status="0"/>
<zip code="63834" office_id="KY0050" status="1"/>
<zip code="63834" office_id="KYAAAA" status="0"/>
<zip code="63837" office_id="KY0050" status="1"/>
<zip code="63837" office_id="KYAAAA" status="0"/>
<zip code="63838" office_id="KY0050" status="1"/>
<zip code="63838" office_id="KYAAAA" status="0"/>
<zip code="63839" office_id="KY0050" status="1"/>
<zip code="63839" office_id="KYAAAA" status="0"/>
<zip code="63840" office_id="KY0050" status="1"/>
<zip code="63840" office_id="KYAAAA" status="0"/>
<zip code="63841" office_id="KY0050" status="1"/>
<zip code="63841" office_id="KYAAAA" status="0"/>
<zip code="63845" office_id="KY0050" status="1"/>
<zip code="63845" office_id="KYAAAA" status="0"/>
<zip code="63846" office_id="KY0050" status="1"/>
<zip code="63846" office_id="KYAAAA" status="0"/>
<zip code="63847" office_id="KY0050" status="1"/>
<zip code="63847" office_id="KYAAAA" status="0"/>
<zip code="63848" office_id="KY0050" status="1"/>
<zip code="63848" office_id="KYAAAA" status="0"/>
<zip code="63849" office_id="KY0050" status="1"/>
<zip code="63849" office_id="KYAAAA" status="0"/>
<zip code="63850" office_id="KY0050" status="1"/>
<zip code="63850" office_id="KYAAAA" status="0"/>
<zip code="63851" office_id="KY0050" status="1"/>
<zip code="63851" office_id="KYAAAA" status="0"/>
<zip code="63852" office_id="KY0050" status="1"/>
<zip code="63852" office_id="KYAAAA" status="0"/>
<zip code="63853" office_id="KY0050" status="1"/>
<zip code="63853" office_id="KYAAAA" status="0"/>
<zip code="63855" office_id="KY0050" status="1"/>
<zip code="63855" office_id="KYAAAA" status="0"/>
<zip code="63857" office_id="KY0050" status="1"/>
<zip code="63857" office_id="KYAAAA" status="0"/>
<zip code="63860" office_id="KY0050" status="1"/>
<zip code="63860" office_id="KYAAAA" status="0"/>
<zip code="63862" office_id="KY0050" status="1"/>
<zip code="63862" office_id="KYAAAA" status="0"/>
<zip code="63863" office_id="KY0050" status="1"/>
<zip code="63863" office_id="KYAAAA" status="0"/>
<zip code="63866" office_id="KY0050" status="1"/>
<zip code="63866" office_id="KYAAAA" status="0"/>
<zip code="63867" office_id="KY0050" status="1"/>
<zip code="63867" office_id="KYAAAA" status="0"/>
<zip code="63868" office_id="KY0050" status="1"/>
<zip code="63868" office_id="KYAAAA" status="0"/>
<zip code="63869" office_id="KY0050" status="1"/>
<zip code="63869" office_id="KYAAAA" status="0"/>
<zip code="63870" office_id="KY0050" status="1"/>
<zip code="63870" office_id="KYAAAA" status="0"/>
<zip code="63871" office_id="KY0050" status="1"/>
<zip code="63871" office_id="KYAAAA" status="0"/>
<zip code="63873" office_id="KY0050" status="1"/>
<zip code="63873" office_id="KYAAAA" status="0"/>
<zip code="63874" office_id="KY0050" status="1"/>
<zip code="63874" office_id="KYAAAA" status="0"/>
<zip code="63875" office_id="KY0050" status="1"/>
<zip code="63875" office_id="KYAAAA" status="0"/>
<zip code="63876" office_id="KY0050" status="1"/>
<zip code="63876" office_id="KYAAAA" status="0"/>
<zip code="63877" office_id="KY0050" status="1"/>
<zip code="63877" office_id="KYAAAA" status="0"/>
<zip code="63878" office_id="KY0050" status="1"/>
<zip code="63878" office_id="KYAAAA" status="0"/>
<zip code="63879" office_id="KY0050" status="1"/>
<zip code="63879" office_id="KYAAAA" status="0"/>
<zip code="63880" office_id="KY0050" status="1"/>
<zip code="63880" office_id="KYAAAA" status="0"/>
<zip code="63881" office_id="KY0050" status="1"/>
<zip code="63881" office_id="KYAAAA" status="0"/>
<zip code="63882" office_id="KY0050" status="1"/>
<zip code="63882" office_id="KYAAAA" status="0"/>
<zip code="63933" office_id="KY0050" status="1"/>
<zip code="63933" office_id="KYAAAA" status="0"/>
<zip code="63936" office_id="KY0050" status="1"/>
<zip code="63936" office_id="KYAAAA" status="0"/>
<zip code="63960" office_id="KY0050" status="1"/>
<zip code="63960" office_id="KYAAAA" status="0"/>
<zip code="72310" office_id="KY0050" status="1"/>
<zip code="72310" office_id="KYAAAA" status="0"/>
<zip code="72313" office_id="KY0050" status="1"/>
<zip code="72313" office_id="KYAAAA" status="0"/>
<zip code="72315" office_id="KY0050" status="1"/>
<zip code="72315" office_id="KYAAAA" status="0"/>
<zip code="72316" office_id="KY0050" status="1"/>
<zip code="72316" office_id="KYAAAA" status="0"/>
<zip code="72319" office_id="KY0050" status="1"/>
<zip code="72319" office_id="KYAAAA" status="0"/>
<zip code="72321" office_id="KY0050" status="1"/>
<zip code="72321" office_id="KYAAAA" status="0"/>
<zip code="72329" office_id="KY0050" status="1"/>
<zip code="72329" office_id="KYAAAA" status="0"/>
<zip code="72330" office_id="KY0050" status="1"/>
<zip code="72330" office_id="KYAAAA" status="0"/>
<zip code="72338" office_id="KY0050" status="1"/>
<zip code="72338" office_id="KYAAAA" status="0"/>
<zip code="72350" office_id="KY0050" status="1"/>
<zip code="72350" office_id="KYAAAA" status="0"/>
<zip code="72351" office_id="KY0050" status="1"/>
<zip code="72351" office_id="KYAAAA" status="0"/>
<zip code="72358" office_id="KY0050" status="1"/>
<zip code="72358" office_id="KYAAAA" status="0"/>
<zip code="72370" office_id="KY0050" status="1"/>
<zip code="72370" office_id="KYAAAA" status="0"/>
<zip code="72381" office_id="KY0050" status="1"/>
<zip code="72381" office_id="KYAAAA" status="0"/>
<zip code="72391" office_id="KY0050" status="1"/>
<zip code="72391" office_id="KYAAAA" status="0"/>
<zip code="72395" office_id="KY0050" status="1"/>
<zip code="72395" office_id="KYAAAA" status="0"/>
<zip code="72412" office_id="KY0050" status="1"/>
<zip code="72412" office_id="KYAAAA" status="0"/>
<zip code="72422" office_id="KY0050" status="1"/>
<zip code="72422" office_id="KYAAAA" status="0"/>
<zip code="72424" office_id="KY0050" status="1"/>
<zip code="72424" office_id="KYAAAA" status="0"/>
<zip code="72425" office_id="KY0050" status="1"/>
<zip code="72425" office_id="KYAAAA" status="0"/>
<zip code="72426" office_id="KY0050" status="1"/>
<zip code="72426" office_id="KYAAAA" status="0"/>
<zip code="72428" office_id="KY0050" status="1"/>
<zip code="72428" office_id="KYAAAA" status="0"/>
<zip code="72430" office_id="KY0050" status="1"/>
<zip code="72430" office_id="KYAAAA" status="0"/>
<zip code="72435" office_id="KY0050" status="1"/>
<zip code="72435" office_id="KYAAAA" status="0"/>
<zip code="72436" office_id="KY0050" status="1"/>
<zip code="72436" office_id="KYAAAA" status="0"/>
<zip code="72438" office_id="KY0050" status="1"/>
<zip code="72438" office_id="KYAAAA" status="0"/>
<zip code="72439" office_id="KY0050" status="1"/>
<zip code="72439" office_id="KYAAAA" status="0"/>
<zip code="72441" office_id="KY0050" status="1"/>
<zip code="72441" office_id="KYAAAA" status="0"/>
<zip code="72442" office_id="KY0050" status="1"/>
<zip code="72442" office_id="KYAAAA" status="0"/>
<zip code="72443" office_id="KY0050" status="1"/>
<zip code="72443" office_id="KYAAAA" status="0"/>
<zip code="72450" office_id="KY0050" status="1"/>
<zip code="72450" office_id="KYAAAA" status="0"/>
<zip code="72451" office_id="KY0050" status="1"/>
<zip code="72451" office_id="KYAAAA" status="0"/>
<zip code="72453" office_id="KY0050" status="1"/>
<zip code="72453" office_id="KYAAAA" status="0"/>
<zip code="72454" office_id="KY0050" status="1"/>
<zip code="72454" office_id="KYAAAA" status="0"/>
<zip code="72456" office_id="KY0050" status="1"/>
<zip code="72456" office_id="KYAAAA" status="0"/>
<zip code="72461" office_id="KY0050" status="1"/>
<zip code="72461" office_id="KYAAAA" status="0"/>
<zip code="72464" office_id="KY0050" status="1"/>
<zip code="72464" office_id="KYAAAA" status="0"/>
<zip code="72470" office_id="KY0050" status="1"/>
<zip code="72470" office_id="KYAAAA" status="0"/>
<zip code="72474" office_id="KY0050" status="1"/>
<zip code="72474" office_id="KYAAAA" status="0"/>
</zip_office>
'

INSERT INTO @OfficeAssignedZips
(
	ExternalId,
	PostcodeZip
)
SELECT
	T.C.value('@office_id', 'NVARCHAR(36)') AS ExternalId,
	T.C.value('@code', 'NVARCHAR(20)') AS PostcodeZip
FROM
	@Xml.nodes('/zip_office/zip') T(C)
WHERE
	T.C.value('@status', 'BIT') = 1
	
-- This step concatenates the assigned zip codes for offices into a single comma-delimited string for each office
;WITH AssignedZips AS
(
	SELECT
		t1.ExternalId,
		STUFF(
			(SELECT
					',' + t2.PostcodeZip
				FROM 
					@OfficeAssignedZips t2
				WHERE 
					t1.ExternalId = t2.ExternalId
				ORDER BY 
					t2.PostcodeZip
				FOR XML PATH(''), TYPE
			).value('.', 'NVARCHAR(4000)')
			,1 ,1 , '') AS AssignedPostcodeZip
	FROM 
		@Offices t1
	GROUP BY 
		t1.ExternalId
)
UPDATE
	O
SET
	AssignedPostcodeZip = AZ.AssignedPostcodeZip
FROM
	@Offices O
INNER JOIN AssignedZips AZ
	ON AZ.ExternalId = O.ExternalId
	
IF EXISTS(SELECT 1 FROM [Data.Application.Office] WHERE DefaultType > 0)
BEGIN
	UPDATE
		@Offices
	SET
		IsDefault = 0
END

BEGIN TRANSACTION

SELECT @NextId = NextId FROM [KeyTable]

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

INSERT INTO [Data.Application.Office]
(
	Id,
	OfficeName,
	InActive,
	ExternalId,
	Line1,
	Line2,
	TownCity,
	CountyId,
	StateId,
	CountryId,
	PostcodeZip,
	UnassignedDefault,
	AssignedPostcodeZip,
	DefaultType
)
SELECT 
	O.Id + @NextId - 1,
	LEFT(O.ExternalId + ' ' + O.OfficeName, 20),
	O.InActive,
	O.ExternalId,
	'',
	'',
	'',
	NULL,
	@StateId,
	@CountryId,
	'',
	O.IsDefault,
	O.AssignedPostcodeZip,
	CASE O.IsDefault WHEN 1 THEN 7 ELSE 0 END
FROM 
	@Offices O
WHERE 
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.Office] O2
		WHERE
			O2.ExternalId = O.ExternalId
	)

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

UPDATE
	O
SET
	OfficeName = LEFT(O2.ExternalId + ' ' + O2.OfficeName, 20),
	InActive = O2.InActive,
	AssignedPostcodeZip = O2.AssignedPostcodeZip
FROM
	[Data.Application.Office] O
INNER JOIN @Offices O2
	ON O2.ExternalId = O.ExternalId

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

UPDATE
	[KeyTable]
SET
	NextId = (SELECT MAX(Id) + 1 FROM [Data.Application.Office])

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

COMMIT TRANSACTION

/******************** ACTIVITIES ********************/

DECLARE @Activities TABLE
(
	Id INT IDENTITY(1, 1),
	ExternalId NVARCHAR(4),
	Name NVARCHAR(100)
)

DECLARE @ActivityCategoryId BIGINT

SET @Xml = '
<activities>
  <activity code="305">FTR Call-in</activity>
  <activity code="319">FTR Profiling Orientation</activity>
  <activity code="246">Kentucky Employ Network</activity>
  <activity code="114">Job Search, Assistance, Counseling</activity>
  <activity code="35">Job Search Workshop</activity>
  <activity code="119">Orientation - Other</activity>
  <activity code="245">Profiled Case Management</activity>
  <activity code="1025">Referral to Adult Ed</activity>
  <activity code="1037">Referral to WIA Adult</activity>
  <activity code="64">Referred to Job Corps</activity>
  <activity code="57">Referred to Supportive Service - Non-Partner</activity>
  <activity code="56">Referred to Supportive Service - Partner</activity>
  <activity code="1206">Referred to Testing</activity>
  <activity code="205">Referred to Training - Other</activity>
  <activity code="1203">Referred to Trade</activity>
  <activity code="204">Referred to WIA</activity>
  <activity code="54">Refused Referral</activity>
  <activity code="37">Resume Preparation Assistance</activity>
  <activity code="45">Self-Service Systems (Non-OSOS)</activity>
  <activity code="1172">UI Benefits Right Interview</activity>
  <activity code="1001">UI Eligiblity Review - ERP</activity>
  <activity code="1176">UI Information</activity>
  <activity code="362">UI Reemployment Orientation</activity>
  <activity code="361">Workforce Info. Services Self-Service - LMI</activity>
  <activity code="39">Workforce Info. Services Staff-Assisted - LMI</activity>
</activities>
'

INSERT INTO @Activities
(
	Name,
	ExternalId
)
SELECT
	T.C.value('text()[1]', 'NVARCHAR(100)') AS Name,
	T.C.value('@code', 'NVARCHAR(100)') AS ExternalId
FROM
	@Xml.nodes('/activities/activity') T(C)
	
BEGIN TRANSACTION

DELETE FROM [Config.Activity]

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

DELETE FROM [Config.ActivityCategory]

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

INSERT INTO [Config.ActivityCategory]
(
	LocalisationKey,
	Name,
	Visible,
	Administrable,
	ActivityType
)
VALUES
(
	'ActivityCategory.001',
	'Default Category',
	1,
	1,
	0
)

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

SET @ActivityCategoryId = SCOPE_IDENTITY()

INSERT INTO [Config.Activity]
(
	LocalisationKey,
	Name,
	Visible,
	Administrable,
	SortOrder,
	Used,
	ExternalId,
	ActivityCategoryId
)
SELECT
	N'Activity.' + RIGHT(N'000' + CAST(A.Id AS NVARCHAR(3)), 3),
	A.Name,
	1,
	1,
	0,
	0,
	A.ExternalId,
	@ActivityCategoryId
FROM
	@Activities A

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

COMMIT TRANSACTION

/******************** NEARBY STATES ********************/

DECLARE @States TABLE
(
	StateKey NVARCHAR(2)
)

DECLARE @StateList NVARCHAR(4000)

SET @Xml = '
<states_list>
	<states state="IL">Illinois</states>
	<states state="IN">Indiana</states>
	<states state="KY">Kentucky</states>
	<states state="MO">Missouri</states>
	<states state="OH">Ohio</states>
	<states state="TN">Tennessee</states>
	<states state="VA">Virginia</states>
	<states state="WV">West Virginia</states>
</states_list>
'

INSERT INTO @States 
(
	StateKey
)
SELECT
	T.C.value('@state', 'NVARCHAR(100)') AS StateKey
FROM
	@Xml.nodes('/states_list/states') T(C)
	
SET @StateList = SUBSTRING(
		(SELECT
			',State.' + S.StateKey
		FROM 
			@States S
		ORDER BY 
			S.StateKey
		FOR XML PATH(''), TYPE
	).value('.', 'NVARCHAR(4000)'), 2, 3998)
	
UPDATE
	[Config.ConfigurationItem]
SET
	Value = @StateList
WHERE
	[Key] = 'NearbyStateKeys'

IF @@ROWCOUNT = 0
BEGIN
	INSERT INTO [Config.ConfigurationItem]
	(
		[Key],
		Value,
		InternalOnly
	)
	VALUES
	(
		'NearbyStateKeys',
		@StateList,
		0
	)
END