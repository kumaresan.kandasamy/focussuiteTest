if not exists(select 1 from [Config.LocalisationItem] where [key] = 'MyResumes2.Label' and [localisationId] = 12090)
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('MyResumes2.Label', '<a href="/resume/create">Create or upload a resume</a>', 0, 12090)
end
else
begin
	update	[Config.LocalisationItem]
	set		[Value] = '<a href="/resume/create">Create or upload a resume</a>'
	where	[Key] = 'MyResumes2.Label'
	and		[LocalisationId] = 12090
end

if not exists(select 1 from [Config.LocalisationItem] where [key] = 'MyResumes2Trailer.Label' and [localisationId] = 12090)
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('MyResumes2Trailer.Label', ' ', 0, 12090)
end
else
begin
	update	[Config.LocalisationItem]
	set		[Value] = ' '
	where	[Key] = 'MyResumes2Trailer.Label'
	and		[LocalisationId] = 12090
end

if not exists(select 1 from [Config.LocalisationItem] where [key] = 'CreateUploadResume2.LinkText' and [localisationId] = 12090)
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('CreateUploadResume2.LinkText', '&nbsp;', 0, 12090)
end
else
begin
	update	[Config.LocalisationItem]
	set		[Value] = '&nbsp;'
	where	[Key] = 'CreateUploadResume2.LinkText'
	and		[LocalisationId] = 12090
end

if not exists(select 1 from [Config.LocalisationItem] where [key] = 'LandingPage2.Label' and [localisationId] = 12090)
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('LandingPage2.Label', '&nbsp;', 0, 12090)
end
else
begin
	update	[Config.LocalisationItem]
	set		[Value] = '&nbsp;'
	where	[Key] = 'LandingPage2.Label'
	and		[LocalisationId] = 12090
end

if not exists(select 1 from [Config.LocalisationItem] where [key] = 'Promo31.LinkText' and [localisationId] = 12090)
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Promo31.LinkText', 'Widen your net: see matches statewide &#187;', 0, 12090)
end
else
begin
	update	[Config.LocalisationItem]
	set		[Value] = 'Widen your net: see matches statewide &#187;'
	where	[Key] = 'Promo31.LinkText'
	and		[LocalisationId] = 12090
end

if not exists(select 1 from [Config.LocalisationItem] where [key] = 'Promo32.LinkText' and [localisationId] = 12090)
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Promo32.LinkText', 'Don''t see what you''re looking for? &#187;', 0, 12090)
end
else
begin
	update	[Config.LocalisationItem]
	set		[Value] = 'Don''t see what you''re looking for? &#187;'
	where	[Key] = 'Promo32.LinkText'
	and		[LocalisationId] = 12090
end



