DECLARE @LocalisationId BIGINT

SELECT @LocalisationId = Id FROM [Config.Localisation] WHERE Culture = '**-**'

IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'HideObjectives')
BEGIN
	RAISERROR ('Start - Configuration Item - Hide objectives', 0, 1) WITH NOWAIT

	INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
	VALUES ('HideObjectives', 'True', 1)

	RAISERROR ('End - Configuration Item - Hide objectives', 0, 1) WITH NOWAIT
END

IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'HideMinimumAge')
BEGIN
	RAISERROR ('Start - Configuration Item - Hide minimum age', 0, 1) WITH NOWAIT

	INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
	VALUES ('HideMinimumAge', 'True', 1)

	RAISERROR ('End - Configuration Item - Hide minimum age', 0, 1) WITH NOWAIT
END

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideResumeProfile'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HideResumeProfile', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideObjectives'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HideObjectives', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideJobConditions'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HideJobConditions', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideWOTC'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HideWOTC', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideMinimumAge'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HideMinimumAge', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideSeasonalDuration'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('HideSeasonalDuration', 'True', 1)
	
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowExpIndustries'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ShowExpIndustries', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowEmployeeNumbers'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ShowEmployeeNumbers', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideFEIN'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('HideFEIN', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'JobSeekerReferralsEnabled'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('JobSeekerReferralsEnabled', 'False', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowLocalisedEduLevels'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ShowLocalisedEduLevels', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowYearsOfExperienceInPostingResults'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ShowYearsOfExperienceInPostingResults', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'JobDescriptionCharsShown'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('JobDescriptionCharsShown', '200', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowHomeLocationInSearchResults'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ShowHomeLocationInSearchResults', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideYearsOfExpInSearchResults'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('HideYearsOfExpInSearchResults', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'AccountTypeDisplayType'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('AccountTypeDisplayType', 'Mandatory', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowPersonalEmail'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ShowPersonalEmail', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowTargetIndustries'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ShowTargetIndustries', 'True', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowContactDetails'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ShowContactDetails', '2', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowBrandingStatement'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('ShowBrandingStatement', 'true', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'NationWideInstance'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('NationWideInstance', 'true', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'NearbyStateKeys'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('NearbyStateKeys', 'State.AA,State.AE,State.AK,State.AL,State.AP,State.AR,State.AS,State.AZ,State.CA,State.CO,State.CT,State.DC,State.DE,State.FL,State.FM,State.GA,State.GU,State.HI,State.IA,State.ID,State.IL,State.IN,State.KS,State.KY,State.LA,State.MA,State.MD,State.ME,State.MH,State.MI,State.MN,State.MO,State.MP,State.MS,State.MT,State.NC,State.ND,State.NE,State.NH,State.NJ,State.NM,State.NV,State.NY,State.OH,State.OK,State.OR,State.PA,State.PR,State.PW,State.RI,State.SC,State.SD,State.TN,State.TX,State.UT,State.VA,State.VI,State.VT,State.WA,State.WI,State.WV,State.WY', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeFilteringType'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('ExplorerDegreeFilteringType', '4', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeClientDataTag'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('ExplorerDegreeClientDataTag', 'LSCS', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideDrivingLicence'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HideDrivingLicence', 'true', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideEnrollmentStatus'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HideEnrollmentStatus', 'true', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideReferences'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HideReferences', 'true', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HidePersonalInformation'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('HidePersonalInformation', 'true', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'BrandIdentifier'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('BrandIdentifier', 'RightManagement', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'MaxPreferredSalaryThreshold'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('MaxPreferredSalaryThreshold', '500000', 1)

Print 'Start - Localisation Items - EducationLevel'

	IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'EducationLevel.Bachelors_OR_Equivalent_24')
	BEGIN
		RAISERROR ('Start - Localisation Item - EducationLevel.Bachelors_OR_Equivalent_24', 0, 1) WITH NOWAIT

		INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
		VALUES ('', 'EducationLevel.Bachelors_OR_Equivalent_24', 'Bachelor''s Degree', 0, @LocalisationId)
		
		RAISERROR ('End - Localisation Item - EducationLevel.Bachelors_OR_Equivalent_24', 0, 1) WITH NOWAIT
	END

	IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'EducationLevel.Masters_Degree_25')
	BEGIN
		RAISERROR ('Start - Localisation Item - EducationLevel.Masters_Degree_25', 0, 1) WITH NOWAIT

		INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
		VALUES ('', 'EducationLevel.Masters_Degree_25', 'Master''s Degree', 0, @LocalisationId)
		
		RAISERROR ('End - Localisation Item - EducationLevel.Masters_Degree_25', 0, 1) WITH NOWAIT
	END

	IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'EducationLevel.Doctorate_Degree_26')
	BEGIN
		RAISERROR ('Start - Localisation Item - EducationLevel.Doctorate_Degree_26', 0, 1) WITH NOWAIT

		INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
		VALUES ('', 'EducationLevel.Doctorate_Degree_26', 'Doctorate', 0, @LocalisationId)
		
		RAISERROR ('End - Localisation Item - EducationLevel.Doctorate_Degree_26', 0, 1) WITH NOWAIT
	END

Print 'End - Localisation Item - EducationLevel'

-- CUSTOM FOOTER --

DECLARE @CareerPath NVARCHAR(1000)
SELECT @CareerPath = Value FROM [Config.ConfigurationItem] WHERE [Key] = 'CareerApplicationPath'

Print 'Start - Configuration Item - Use Custom Footers'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'UseCustomFooterHtml'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('UseCustomFooterHtml','True',1)

Print 'End - Configuration Item - Use Custom Footers'

Print 'Start - Configuration Item - Workforce Career Custom Footer'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml-Workforce-CareerExplorer'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('CustomFooterHtml-Workforce-CareerExplorer','<a href="' + @CareerPath + '/privacy">Privacy and Security</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="' + @CareerPath + '/terms">Terms of Use</a>',1)

Print 'End - Configuration Item - Workforce Career Custom Footer'
	
-- TERMS OF USE --

Print 'Start - Localisation Item - Global.HelpTitle-TermsOfUse.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpTitle-TermsOfUse.Text'

INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], Value, Localised, LocalisationId) 
VALUES ('', 'Global.HelpTitle-TermsOfUse.Text', 'Terms of Use', 0, @LocalisationId)

Print 'End - Localisation Item - Global.HelpTitle-TermsOfUse.Text' 

DECLARE @TermsOfUse NVARCHAR(MAX)

SET @TermsOfUse = '
<p>Please carefully read these Terms and Conditions. Your access to this Right Management Inc. ("Right Management") Web Site and any of its pages constitutes your agreement to be bound by the following Terms and Conditions. If you do not agree to the following Terms and Conditions, do not access this Web Site or any of its pages.</p>
<h3>USE OF INFORMATION AND MATERIALS.</h3>
<p>The information contained in this Web Site is subject to change without notice. The Site is not intended to be (i) a comprehensive or detailed statement concerning the matters addressed; (ii) investment, tax, banking, accounting, legal or other professional or expert advice or recommendations; or (iii) an offer or recommendation to sell or buy any stock, bond or other financial instrument or any product or service.  You should seek appropriate, qualified professional advice and recommendations before acting or omitting to act based upon any information provided on or though the Site.  You acknowledge that any reliance on any information contained in the Site shall be at your own risk.  Right Management may in its discretion refuse permission to access and use the Site and reserves the right to correct any errors or omissions in any portion of the Site.</p>
<h3>SITE COMMUNICATIONS AND ORDERS</h3>
<p>The Site is a portal and information conduit to other web sites and business operated by Right Management''s parent company, ManpowerGroup. ("ManpowerGroup") and Right Management''s various affiliates ("Affiliates").  If you use the Site to initiate communication regarding your staffing needs, the information you submit may be disclosed to, and processed and responded to by, ManpowerGroup and Affiliates.  You authorize Right Management to: (a) accept communications that they receive from you by means of the Site or email as if those communications had been given directly by you in writing and signed by you; (b) disclose your communications to any ManpowerGroup and Affiliates, and Right Management workers by means of the Site, email or other communications; and (c) respond to your communications by means of Internet communications, email or other communications. Communications you send to Right Management by means of the Site or email are not effective unless and until they are processed by the responsible Right Management representative.  If you submit any incorrect or incomplete communications, or if any communications are damaged or distorted during transmission to Right Management, you will be liable for any loss, damage or additional costs that you, Right Management, ManpowerGroup and Affiliates, or other persons may incur as a result.  Right Management may refuse to process any communications sent to Right Management by means of the Site or email, or may reverse the processing of any communications sent to Right Management by means of the Site or email, at any time in Right Management''s discretion, and without any notice or liability to you or any other person, including, without limitation, if: (a) Right Management cannot process the communications; (b) the communications violate any provision in these Terms and Conditions or any other agreement that you or any other person may have with Right Management, ManpowerGroup or Affiliates; (c) Right Management considers that the communications conflict with any other instructions or agreements with you or any person you represent; or (d) there is an operational failure or malfunction in connection with the transmission of the communications.</p>
<p>Any information submitted to Right Management, ManpowerGroup or Affiliates via this Web Site shall become and remain the property of Right Management, ManpowerGroup and Affiliates. Right Management, ManpowerGroup and Affiliates shall be free to use such information for any purpose and shall not be subject to any obligations of confidentiality regarding such information, except as required by law or as set forth in the privacy practices or policies applicable to the relationship. Users should be aware that information sent by e-mail may not be secure and may be intercepted by third parties. Please do not use e-mail to send Right Management any confidential information or information which will need immediate attention.</p>
<h3>WARRANTY DISCLAIMER; LIMITATION OF LIABILITY; RELEASE; INDEMNITY.</h3>
<p>RIGHT MANAGEMENT MAKES NO EXPRESS OR IMPLIED WARRANTIES OR REPRESENTATIONS OF ANY KIND WITH RESPECT TO THIS SITE, AND EXPRESSLY DISCLAIMS ALL WARRANTIES INCLUDING WITHOUT LIMITATION WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, PERFORMANCE, DURABILITY, AVAILABILITY, TIMELINESS, ACCURACY OR COMPLETENESS.</p>
<p>The Internet is not a secure medium, may be subject to interruption and disruption, and inadvertent or deliberate breaches of security and privacy.  The operation of the Site may be affected by numerous factors beyond Right Management''s control.  The operation of the Site may not be continuous or uninterrupted, secure or private. IN NO EVENT WILL RIGHT MANAGEMENT, ManpowerGroup OR AFFILIATES BE LIABLE FOR ANY DAMAGES OR LOSSES RESULTING FROM OR IN ANY WAY RELATING TO ANY PERSON''S USE OF OR INABILITY TO USE THIS WEB SITE OR ANY PART HEREOF OR ARISING OUT OF OR IN CONNECTION WITH THIS WEB SITE AND THE INFORMATION CONTAINED AT THIS SITE, INCLUDING WITHOUT LIMITATION LOSS OF DATA, LOSS OF USE OR LOSS OF PROFITS OR ANY SPECIAL, INCIDENTAL, CONSEQUENTIAL, INDIRECT OR PUNITIVE DAMAGES, REGARDLESS OF WHETHER RIGHT MANAGEMENT, ManpowerGroup OR AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSSES.</p>
<p>YOU HEREBY RELEASE EACH OF RIGHT MANAGEMENT, ManpowerGroup AND AFFILIATES AND ALL RELATED, ASSOCIATED, OR CONNECTED PERSONS AND ENTITIES, FROM ANY AND ALL MANNER OF RIGHTS, CLAIMS, COMPLAINTS, DEMANDS, CAUSES OF ACTION, PROCEEDINGS, LIABILITIES, OBLIGATIONS, LEGAL FEES, COSTS, AND DISBURSEMENTS OF ANY NATURE AND KIND WHATSOEVER AND HOWSOEVER ARISING, WHETHER KNOWN OR UNKNOWN, WHICH NOW OR HEREAFTER EXIST, WHICH ARISE FROM, RELATE TO, OR ARE CONNECTED WITH YOUR USE OF THE SITE.</p>
<p>YOU AGREE TO INDEMNIFY, DEFEND AND HOLD HARMLESS EACH OF RIGHT MANAGEMENT, ManpowerGroup AND AFFILIATES AND ALL RELATED, ASSOCIATED, OR CONNECTED PERSONS OR ENTITIES (COLLECTIVELY, THE "INDEMNIFIED PARTIES") FROM AND AGAINST ANY AND ALL LIABILITIES, EXPENSES AND COSTS, INCLUDING WITHOUT LIMITATION REASONABLE LEGAL FEES AND EXPENSES, INCURRED BY THE INDEMNIFIED PARTIES IN CONNECTION WITH YOUR USE OF THE SITE OR YOUR BREACH OF THESE TERMS AND CONDITIONS. YOU WILL ASSIST AND COOPERATE AS FULLY AS REASONABLY REQUIRED BY THE INDEMNIFIED PARTIES IN THE DEFENSE OF ANY CLAIM OR DEMAND.</p>
<h3>COPYRIGHT</h3>
<p>Copyright � 2009 Right Management.  All Rights Reserved.  The Site and all information (in text, graphical, video and audio forms), images, icons, software, designs, applications, calculators, models, data, and other elements available on or through the Site are the property of Right Management, ManpowerGroup, Affiliates and others, and are protected by United States and international copyright, trademark, and other laws.  Your use of the Site does not transfer to you any ownership or other rights in the Site or its content.</p>
<p>The Site may only be used in the manner described expressly in these Terms and Conditions.  In particular, except as expressly stated otherwise in these Terms and Conditions, the Web Site may not be copied, imitated, reproduced, republished, uploaded, posted, transmitted, modified, indexed, catalogued, mirrored or distributed in any way, in whole or in part, without the express prior written consent of Right Management. The Site may be used only for lawful purposes.  The Site may be accessed and used only using commercially available, SSL-capable Web browser software.</p>
<h3>TRADEMARK INFORMATION</h3>
<p>RIGHT MANAGEMENT�, RIGHT.COM�, the Right Management Logo, and other marks and logos appearing on the Site are registered and unregistered trademarks, trade names and service marks owned or licensed by Right Management.  Other product and company names and logos appearing on the Site may be registered or unregistered trade names, trademarks and service marks of their respective owners. Any use of the trade names, trademarks, service marks and logos (collectively "Marks") displayed on the Site is strictly prohibited.  Nothing appearing on the Site or elsewhere shall be construed as granting, by implication, estoppel, or otherwise, any license or right to use any Marks displayed on the Site.</p>
<h3>PERSONAL INFORMATION / PRIVACY</h3>
<p>Right Management collects, uses and discloses information regarding your use of the Site and your personal information in accordance with the Privacy Policy, which is available on the Site. Right Management may change the Privacy Policy from time to time in its discretion without prior notice or liability to you or any other person.  By using the Site, you consent to Right Management''s collection, use and disclosure of your personal information in accordance with the Privacy Policy as it then reads without any further notice or any liability to you or any other person.</p>
<h3>INTERNATIONAL USERS AND CHOICE OF LAW</h3>
<p>This Site is controlled, operated and administered by Right Management from its offices within the United States.  Right Management makes no representation that materials on this Site are appropriate or available for use at locations outside the US and access to them from territories where their contents are illegal is prohibited.  Persons using the Site must comply with all applicable laws.  These Terms and Conditions are governed by the laws of the Commonwealth of Pennsylvania, without giving effect to conflicts of law provisions.  Any controversy or claim arising out of or relating to this Agreement, or the breach thereof, shall be settled by arbitration administered by the American Arbitration Association under its Commercial Arbitration Rules, and judgment on the award rendered by the arbitrator may be entered in any court having jurisdiction thereof.  The arbitration will be before a single arbitrator. The place of arbitration will be Philadelphia, Pennsylvania, United States of America.  Notwithstanding the foregoing, you or Right Management may seek injunctive relief from an appropriate court located in Philadelphia, Pennsylvania prior to or during the arbitration.  Any cause of action you may have with respect to your use of the Site must be commenced within six (6) months after the cause of action arises.  These Terms and Conditions constitute the entire agreement between Right Management and you with regard to your use of the Site.</p>'

Print 'Start - Localisation Item - Global.HelpContent-TermsOfUse.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpContent-TermsOfUse.Text'

INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], Value, Localised, LocalisationId) 
VALUES ('', 'Global.HelpContent-TermsOfUse.Text', @TermsOfUse, 0, @LocalisationId)

Print 'End - Localisation Item - Global.HelpContent-TermsOfUse.Text' 

Print 'Start - Localisation Item - Focus.CareerExplorer.WebAuth.Controls.LogInOrRegister.Consent.Label'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.TermsOfUseEducation.Text'

INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], Value, Localised, LocalisationId) 
VALUES ('', 'Global.TermsOfUseEducation.Text', @TermsOfUse, 0, @LocalisationId)

Print 'End - Localisation Item - Global.TermsOfUseEducation.Text'

Print 'Start - Localisation Item - Focus.Web.Code.Controls.User.EmployerRegistrationStep5.TermsOfUseWorkforce.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.Web.Code.Controls.User.EmployerRegistrationStep5.TermsOfUseWorkforce.Text'

INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], Value, Localised, LocalisationId) 
VALUES ('', 'Focus.Web.Code.Controls.User.EmployerRegistrationStep5.TermsOfUseWorkforce.Text', @TermsOfUse, 0, @LocalisationId)

Print 'End - Localisation Item - Focus.Web.Code.Controls.User.EmployerRegistrationStep5.TermsOfUseWorkforce.Text' 

-- PRIVACY --

Print 'Start - Localisation Item - Global.HelpTitle-PrivacyAndSecurity.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpTitle-PrivacyAndSecurity.Text'

INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], Value, Localised, LocalisationId) 
VALUES ('', 'Global.HelpTitle-PrivacyAndSecurity.Text', 'Privacy Policy', 0, @LocalisationId)

Print 'End - Localisation Item - Global.HelpTitle-PrivacyAndSecurity.Text' 

Print 'Start - Localisation Item - Global.HelpContent-PrivacyAndSecurity.Text'

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Global.HelpContent-PrivacyAndSecurity.Text'

INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], Value, Localised, LocalisationId) 
VALUES 
(
	'', 
	'Global.HelpContent-PrivacyAndSecurity.Text', 
	'
<h3>PRIVACY NOTICE FOR THE RIGHT MANAGEMENT GLOBAL WEB SITE AS OF JUNE 2009.</h3>
<p>In general, you may visit this Web site without providing any personal information to us. If you choose to provide information voluntarily, we will use that information in accordance with our data privacy principles and notice for individuals.</p>
<h3>RIGHT MANAGEMENT''S GLOBAL DATA PRIVACY PRINCIPLES</h3>
<p>Right Management has long recognized the importance of maintaining the privacy of personal and sensitive information of our employees, candidates, clients, vendors and partners. The nature of our business requires us to collect and handle such information, and we have a responsibility to protect this information for as long as it is in our possession. At Right Management, respecting our employees, candidates, clients, vendors and partners is a part of our core values, along with being committed to providing services in an ethical manner.</p>
<p>To support these obligations, Right Management has created a set of global data privacy principles, which guide our efforts as they relate to privacy and the handling and protection of personal and sensitive information.</p>
<p>We respect your privacy by:</p>
<ul>
<li>Offering privacy notices that explain how and why we handle personal information.</li>
<li>Respecting your choices about our collection, use and sharing of information, where appropriate.</li>
<li>Collecting, using and retaining only personal information that is relevant and useful to our business interactions.</li>
<li>Using reasonable efforts to keep personal information accurate and up-to-date.</li>
<li>Using safeguards to protect personal information.</li>
<li>Limiting access to and disclosure of personal information.</li>
<li>Retaining only the personal information that is needed to fill our legal and business obligations.</li>
<li>Offering you the ability to view and update your personal information, where appropriate.</li>
<li>Providing you with an opportunity to ask questions and register complaints about privacy.</li>
</ul>
<br />
<p>For more information on our privacy practices for individuals, please see the following notice:<p>
<h3>DATA PRIVACY NOTICE FOR INDIVIDUALS at this RIGHT MANAGEMENT GLOBAL WEB SITE</h3>
<p>Right Management Inc. cares about the privacy of our applicants, employees and clients. This notice contains information about how we handle your personal information. We collect and process your personal information for the following purposes where necessary:</p>
<ul>
<li>to maintain our contractual or business relationship with you,</li>
<li>for employment-related services where applicable,</li>
<li>to tell you about the products and services we offer,</li>
<li>to contact and correspond with you,</li>
<li>for the management and defense of legal claims and actions, compliance with court orders and other legal obligations and regulatory requirements, and as otherwise permitted by law.</li>
</ul>
<br />
<p>Right Management may disclose your personal information for these purposes to other Right Management entities, affiliates, suppliers, subcontractors who perform services on our behalf, clients if you are seeking employment, an acquiring organization if Right Management is involved in the sale or transfer of some or all of its business, and where we are otherwise required to do so, such as by court order.</p>
<p>Right Management collects, processes and discloses sensitive personal information only if required to comply with legal obligations or if there is a compelling business reason to do so, or with your consent.</p>
<p>If you would like more information about Right Management''s privacy practices, please contact us by emailing us at <a href="mailto:privacy@right.com">privacy@right.com</a> or by writing to us at:<br />Right Management Inc., 1818 Market Street, 33rd Floor, Philadelphia, PA  19103, Attention: General Counsel</p>
<h3>WEBSITE PRACTICES NOTICE for this RIGHT MANAGEMENT GLOBAL WEB SITE AUTOMATICALLY COLLECTED INFORMATION</h3>
<p>Non-Identifiable Information - Like many other Web sites, this Web site automatically collects certain non-identifiable information regarding Web site users, such as the Internet Protocol (IP) address of your computer, the IP address of your Internet Service Provider, the date and time you access the Web site, the Internet address of the Web site from which you linked directly to the Web site, the operating system you are using, the sections of the Web site you visit, the Web site pages accessed and information viewed, and the materials you post to or download from the Web site. This non-identifiable information is used for Web sites and system administration purposes and to improve the Web site. Your non-identifiable information may be disclosed to others and permanently archived for future use.</p>
<p>Cookies - This Web site uses "cookies", a technology that installs information on a Web site user''s computer to permit the Web site to recognize future visits using that computer. Cookies enhance the convenience and use of the Web site. For example, the information provided through cookies is used to recognize you as a previous user of the Web site, to track your activity at the Web site in order to respond to your needs, to offer personalized Web page content and information for your use, to automatically populate online forms with your personal information for your convenience, and to otherwise facilitate your Web site experience.</p>
<p>You may choose to decline cookies if your browser permits, but doing so may affect your use of this Web site and your ability to access or use certain Web site features.</p>
<h3>INFORMATION USED</h3>
<p>Tracking Information - Right Management may use non-personal information to create aggregate tracking information reports regarding Web site user demographics and the use of this Web site, and then provide those reports to others. None of the tracking information in the reports is connected to the identities or other personal information of individual users.</p>
<h3>OTHER MATTERS</h3>
<p>Security - Right Management works to keep our servers, applications and databases secure and free from unauthorized access and use, by using physical, administrative and technological measure to protect the information we maintain. Unfortunately, no one can guarantee 100% security. If you have particular security concerns about certain personal information, please do not transmit that information over the Internet.</p>
<p>Other Web sites - This Web site may contain links to other Web sites or Internet resources. When you click on one of those links you are contacting another Web site or Internet resource that may collect information about you voluntarily or through cookies or other technologies. Right Management has no responsibility, liability for, or control over those other Web sites or Internet resources or their collection, use and disclosure of your personal information. You should review the privacy policies of those other Web sites and Internet resources to understand how they collect and use information.</p>
<p>Privacy Notice Changes - Right Management may change, supplement or amend this Notice as it relates to your future use of this Web site from time to time, for any reason, by posting a revised Notice on this Web site. This notice was last updated in June 2009.</p>
	', 
	0, 
	@LocalisationId
)

Print 'End - Localisation Item - Global.HelpContent-PrivacyAndSecurity.Text' 

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebCareer.JobMatching.GoodMatchDescription.Text'
 INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.CareerExplorer.WebCareer.JobMatching.GoodMatchDescription.Text', 'People who work in <b>{0}</b> jobs typically have the characteristics listed below. In Section 5, we�ve identified possible gaps in your resume and skill set information, plus made suggestions for addressing them.', 0, @LocalisationId)


DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.Web.WebTalent.Register.SuccessfulSubmitValidFEIN.Title'
 INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.Web.WebTalent.Register.SuccessfulSubmitValidFEIN.Title', 'Thank you for registering on Right Talent!', 0, @LocalisationId)

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEINNoAccess.Title'
 INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEINNoAccess.Title', 'Thank you for registering on Right Talent!', 0, @LocalisationId)

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEIN.Titlee'
 INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEIN.Title', 'Thank you for registering on Right Talent!', 0, @LocalisationId)


DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEIN.Body'
 INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEIN.Body', 'Our Job Market Expert team will review your account request and get back to you in 24-48 hours. In the meantime, you may begin to create job postings in the system, which will stay in the queue until your account has been approved. If you need immediate assistance, please email us at rightmarketconnections@right.com. Thank you!', 0, @LocalisationId)


 INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEINNoAccess.Body', 'Our Job Market Expert team will review your account request and get back to you in 24-48 hours. In the meantime, you may begin to create job postings in the system, which will stay in the queue until your account has been approved. If you need immediate assistance, please email us at rightmarketconnections@right.com. Thank you!', 0, @LocalisationId)

 INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.Web.WebTalent.Register.SuccessfulSubmitValidFEIN.Body', 'Our Job Market Expert team will review your account request and get back to you in 24-48 hours. In the meantime, you may begin to create job postings in the system, which will stay in the queue until your account has been approved. If you need immediate assistance, please email us at rightmarketconnections@right.com. Thank you!', 0, @LocalisationId)

  
UPDATE [Config.LocalisationItem]
SET [Value] ='Hourly "Non-exempt"'
WHERE [KEY] = 'JobStatuses.Hourly' 

UPDATE [Config.LocalisationItem]
SET [Value] ='Contractor'
WHERE [KEY] = 'JobStatuses.Contractor' 

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebCareer.JobPosting.Button.HowToApply'
 INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.CareerExplorer.WebCareer.JobPosting.Button.HowToApply', 'Apply to this job', 0, @LocalisationId)

DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'Focus.CareerExplorer.WebCareer.Controls.Preferences.WagesInstruction.Label'
INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
VALUES ('', 'Focus.CareerExplorer.WebCareer.Controls.Preferences.WagesInstruction.Label', 'Edit your salary to expand your search into other salary levels.', 0, @LocalisationId)
