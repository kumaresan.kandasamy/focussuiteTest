﻿DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'AllowSelfReferralOnSpideredPostingsWithNoResume'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('AllowSelfReferralOnSpideredPostingsWithNoResume','True',1)

