﻿--Update to SPCollege Settings and Localisation

DELETE FROM [dbo].[Config.ConfigurationItem] WHERE [Key] = 'SupportEmail-Education-Talent'
INSERT [dbo].[Config.ConfigurationItem] ([Key], [Value], [InternalOnly]) VALUES ('SupportEmail-Education-Talent', 'careerservices@spcollege.edu', 1)

DELETE FROM [dbo].[Config.ConfigurationItem] WHERE [Key] = 'SupportPhone-Education-Talent'
INSERT [dbo].[Config.ConfigurationItem] ([Key], [Value], [InternalOnly]) VALUES ('SupportPhone-Education-Talent', '727-341-3019', 1)

DECLARE @LocalisationId as bigint = (SELECT [Id] FROM [dbo].[Config.Localisation] WHERE [Culture] = '**-**')
DELETE FROM [dbo].[Config.LocalisationItem] WHERE [Key] = 'WelcomeContentLiteral.Text'   
INSERT INTO [dbo].[Config.LocalisationItem]([ContextKey],[Key],[Value],[Localised],[LocalisationId]) VALUES
('',
'WelcomeContentLiteral.Text',
'<b>Welcome to #FOCUSTALENT#</b><br/>
<br/>
Please click the appropriate tab above to post jobs or search for talent.<br/>
<br/>
If you have problems navigating the site, please contact Support at #SUPPORTPHONE# or <a href="mailto:#SUPPORTEMAIL#">#SUPPORTEMAIL#</a>. 
<br/>
',
0,
@LocalisationId)
