﻿-- Run this script to import all the WI external look items required for integration/migration
DECLARE @EducationLevelEnum INT
DECLARE @StatesEnum INT
DECLARE @CountryEnum INT
DECLARE @PhoneTypeEnum INT
DECLARE @BooleanEnum INT
DECLARE @DrivingLicenceClassEnum INT
DECLARE @DisabilityCategoryEnum INT
DECLARE @GenderEnum INT
DECLARE @EthnicityEnum INT
DECLARE @RaceEnum INT
DECLARE @DisabilityStatus INT
DECLARE @WorkShiftEnum INT
DECLARE @EmploymentStatusEnum INT
DECLARE @SalaryUnitEnum INT
DECLARE @RadiusEnum INT
DECLARE @SchoolStatusEnum INT
DECLARE @WorkDurationEnum INT
DECLARE @WorkWeekEnum INT
DECLARE @JobTypeEnum INT
DECLARE @JobEmploymentStatusEnum INT
DECLARE @JobEducationLevelEnum INT
DECLARE @EmployerOwnershipTypeEnum INT
DECLARE @CountyEnum INT
DECLARE @ClientDataTag NVARCHAR(10)

SET @EducationLevelEnum = 0
SET @GenderEnum = 2
SET @DisabilityStatus = 3
SET @SalaryUnitEnum = 4
SET @WorkShiftEnum = 5
SET @DrivingLicenceClassEnum = 6
SET @WorkDurationEnum = 11
SET @EthnicityEnum = 12
SET @EmploymentStatusEnum = 17
SET @SchoolStatusEnum = 18
SET @WorkWeekEnum = 19
SET @DisabilityCategoryEnum = 20
SET @BooleanEnum = 24
SET @RaceEnum = 25
SET @RadiusEnum = 26
SET @PhoneTypeEnum = 27
SET @JobTypeEnum = 30
SET @JobEmploymentStatusEnum = 31
SET @JobEducationLevelEnum = 32
SET @EmployerOwnershipTypeEnum = 15
SET @CountyEnum = 300
SET @StatesEnum = 301
SET @CountryEnum = 302
SET @ClientDataTag = 'WI'
DELETE FROM [Config.ExternalLookUpItem]
BEGIN -- Add states
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @StatesEnum AND [ClientDataTag] = @ClientDataTag
-- Insert states
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'State.AL' THEN '01'
		WHEN 'State.AK' THEN '02'
		WHEN 'State.AZ' THEN '04'
		WHEN 'State.AR' THEN '05'
		WHEN 'State.CA' THEN '06'
		WHEN 'State.CO' THEN '08'
		WHEN 'State.CT' THEN '09'
		WHEN 'State.DE' THEN '10'
		WHEN 'State.DC' THEN '11'
		WHEN 'State.FL' THEN '12'
		WHEN 'State.GA' THEN '13'
		WHEN 'State.HI' THEN '15'
		WHEN 'State.ID' THEN '16'
		WHEN 'State.IL' THEN '17'
		WHEN 'State.IN' THEN '18'
		WHEN 'State.IA' THEN '19'
		WHEN 'State.KS' THEN '20'
		WHEN 'State.KY' THEN '21'
		WHEN 'State.LA' THEN '22'
		WHEN 'State.ME' THEN '23'
		WHEN 'State.MD' THEN '24'
		WHEN 'State.MA' THEN '25'
		WHEN 'State.MI' THEN '26'
		WHEN 'State.MN' THEN '27'
		WHEN 'State.MS' THEN '28'
		WHEN 'State.MO' THEN '29'
		WHEN 'State.MT' THEN '30'
		WHEN 'State.NE' THEN '31'
		WHEN 'State.NV' THEN '32'
		WHEN 'State.NH' THEN '33'
		WHEN 'State.NJ' THEN '34'
		WHEN 'State.NM' THEN '35'
		WHEN 'State.NY' THEN '36'
		WHEN 'State.NC' THEN '37'
		WHEN 'State.ND' THEN '38'
		WHEN 'State.OH' THEN '39'
		WHEN 'State.OK' THEN '40'
		WHEN 'State.OR' THEN '41'
		WHEN 'State.PA' THEN '42'
		WHEN 'State.RI' THEN '44'
		WHEN 'State.SC' THEN '45'
		WHEN 'State.SD' THEN '46'
		WHEN 'State.TN' THEN '47'
		WHEN 'State.TX' THEN '48'
		WHEN 'State.VT' THEN '50'
		WHEN 'State.VA' THEN '51'
		WHEN 'State.WA' THEN '53'
		WHEN 'State.WV' THEN '54'
		WHEN 'State.UT' THEN '49'
		WHEN 'State.WI' THEN '55'
		WHEN 'State.WY' THEN '56'
		WHEN 'State.NA' THEN '93'
		WHEN 'State.AS' THEN '60'
		WHEN 'State.FM' THEN '64'
		WHEN 'State.GU' THEN '66'
		WHEN 'State.MH' THEN '68'
		WHEN 'State.MP' THEN '69'
		WHEN 'State.PW' THEN '70'
		WHEN 'State.PR' THEN '72'
		WHEN 'State.UM' THEN '74'
		WHEN 'State.VI' THEN '78'
		WHEN 'State.AE' THEN '90'
		WHEN 'State.AA' THEN '91'
		WHEN 'State.AP' THEN '92'
		WHEN 'State.ZZ' THEN '93'
	END,
	@StatesEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'States' AND [Key] IN ('State.AE', 'State.AA', 'State.AP', 'State.AL', 'State.AK', 'State.AS', 'State.AZ', 'State.AR', 'State.CA', 'State.CO', 'State.CT', 'State.DE', 'State.DC', 'State.FM', 'State.FL', 'State.GA', 'State.GU', 'State.HI', 'State.ID', 'State.IL', 'State.IN', 'State.IA', 'State.KS', 'State.KY', 'State.LA', 'State.ME', 'State.MH', 'State.MD', 'State.MA', 'State.MI', 'State.MN', 'State.MS', 'State.MO', 'State.MT', 'State.NE', 'State.NV', 'State.NH', 'State.NJ', 'State.NM', 'State.NY', 'State.NC', 'State.ND', 'State.MP', 'State.OH', 'State.OK', 'State.OR', 'State.ZZ', 'State.PW', 'State.PA', 'State.PR', 'State.RI', 'State.SC', 'State.SD', 'State.TN', 'State.TX', 'State.VI', 'State.UT', 'State.VT', 'State.VA', 'State.WA', 'State.WV', 'State.WI', 'State.WY')
END
-- Insert states
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'State.AE' THEN 'AE'
		WHEN 'State.AA' THEN 'AA'
		WHEN 'State.AP' THEN 'AP'
		WHEN 'State.AL' THEN 'AL'
		WHEN 'State.AK' THEN 'AK'
		WHEN 'State.AS' THEN 'AS'
		WHEN 'State.AZ' THEN 'AZ'
		WHEN 'State.AR' THEN 'AR'
		WHEN 'State.CA' THEN 'CA'
		WHEN 'State.CO' THEN 'CO'
		WHEN 'State.CT' THEN 'CT'
		WHEN 'State.DE' THEN 'DE'
		WHEN 'State.DC' THEN 'DC'
		WHEN 'State.FM' THEN 'FM'
		WHEN 'State.FL' THEN 'FL'
		WHEN 'State.GA' THEN 'GA'
		WHEN 'State.GU' THEN 'GU'
		WHEN 'State.HI' THEN 'HI'
		WHEN 'State.ID' THEN 'ID'
		WHEN 'State.IL' THEN 'IL'
		WHEN 'State.IN' THEN 'IN'
		WHEN 'State.IA' THEN 'IA'
		WHEN 'State.KS' THEN 'KS'
		WHEN 'State.KY' THEN 'KY'
		WHEN 'State.LA' THEN 'LA'
		WHEN 'State.ME' THEN 'ME'
		WHEN 'State.MH' THEN 'MH'
		WHEN 'State.MD' THEN 'MD'
		WHEN 'State.MA' THEN 'MA'
		WHEN 'State.MI' THEN 'MI'
		WHEN 'State.MN' THEN 'MN'
		WHEN 'State.MS' THEN 'MS'
		WHEN 'State.MO' THEN 'MO'
		WHEN 'State.MT' THEN 'MT'
		WHEN 'State.NE' THEN 'NE'
		WHEN 'State.NV' THEN 'NV'
		WHEN 'State.NH' THEN 'NH'
		WHEN 'State.NJ' THEN 'NJ'
		WHEN 'State.NM' THEN 'NM'
		WHEN 'State.NY' THEN 'NY'
		WHEN 'State.NC' THEN 'NC'
		WHEN 'State.ND' THEN 'ND'
		WHEN 'State.MP' THEN 'MP'
		WHEN 'State.OH' THEN 'OH'
		WHEN 'State.OK' THEN 'OK'
		WHEN 'State.OR' THEN 'OR'
		WHEN 'State.ZZ' THEN 'ZZ'
		WHEN 'State.PW' THEN 'PW'
		WHEN 'State.PA' THEN 'PA'
		WHEN 'State.PR' THEN 'PR'
		WHEN 'State.RI' THEN 'RI'
		WHEN 'State.SC' THEN 'SC'
		WHEN 'State.SD' THEN 'SD'
		WHEN 'State.TN' THEN 'TN'
		WHEN 'State.TX' THEN 'TX'
		WHEN 'State.VI' THEN 'VI'
		WHEN 'State.UT' THEN 'UT'
		WHEN 'State.VT' THEN 'VT'
		WHEN 'State.VA' THEN 'VA'
		WHEN 'State.WA' THEN 'WA'
		WHEN 'State.WV' THEN 'WV'
		WHEN 'State.WI' THEN 'WI'
		WHEN 'State.WY' THEN 'WY'
	END,
	@StatesEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'States' AND [Key] IN ('State.AE', 'State.AA', 'State.AP', 'State.AL', 'State.AK', 'State.AS', 'State.AZ', 'State.AR', 'State.CA', 'State.CO', 'State.CT', 'State.DE', 'State.DC', 'State.FM', 'State.FL', 'State.GA', 'State.GU', 'State.HI', 'State.ID', 'State.IL', 'State.IN', 'State.IA', 'State.KS', 'State.KY', 'State.LA', 'State.ME', 'State.MH', 'State.MD', 'State.MA', 'State.MI', 'State.MN', 'State.MS', 'State.MO', 'State.MT', 'State.NE', 'State.NV', 'State.NH', 'State.NJ', 'State.NM', 'State.NY', 'State.NC', 'State.ND', 'State.MP', 'State.OH', 'State.OK', 'State.OR', 'State.ZZ', 'State.PW', 'State.PA', 'State.PR', 'State.RI', 'State.SC', 'State.SD', 'State.TN', 'State.TX', 'State.VI', 'State.UT', 'State.VT', 'State.VA', 'State.WA', 'State.WV', 'State.WI', 'State.WY')

BEGIN -- Add states
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @CountryEnum AND [ClientDataTag] = @ClientDataTag
-- Insert states
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Country.US' THEN 'US'
	END,
	@CountryEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Countries' AND [Key] IN ('Country.US')
END

BEGIN -- Add phone types
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @PhoneTypeEnum AND [ClientDataTag] = @ClientDataTag
-- Insert phone types
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Home','H',@PhoneTypeEnum, @ClientDataTag, 1),
           ('Work','W',@PhoneTypeEnum, @ClientDataTag, 1),
           ('Cell','C',@PhoneTypeEnum, @ClientDataTag, 1),
           ('Fax','F',@PhoneTypeEnum, @ClientDataTag, 1),
           ('NonUS','I',@PhoneTypeEnum, @ClientDataTag, 1)
END

BEGIN -- Add boolean
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @BooleanEnum AND [ClientDataTag] = @ClientDataTag
-- Insert phone types
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('true','Y',@BooleanEnum, @ClientDataTag, 1),
           ('false','N',@BooleanEnum, @ClientDataTag, 1)
END


BEGIN -- Add driving licence class
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DrivingLicenceClassEnum AND [ClientDataTag] = @ClientDataTag
-- Insert driving licence class
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	'Regular',
	@DrivingLicenceClassEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'DrivingLicenceClasses' AND [Key] IN ('DrivingLicenceClasses.ClassD')
END



BEGIN -- Add employment status look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EmploymentStatusEnum AND [ClientDataTag] = @ClientDataTag
-- Insert employment status Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Employed','1',@EmploymentStatusEnum, @ClientDataTag, 1)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('EmployedTerminationReceived','3',@EmploymentStatusEnum, @ClientDataTag, 2)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('EmployedTerminationReceived','4',@EmploymentStatusEnum, @ClientDataTag, 2)
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('UnEmployed','2',@EmploymentStatusEnum, @ClientDataTag, 3)           
END

BEGIN -- Add gender look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @GenderEnum AND [ClientDataTag] = @ClientDataTag
-- Insert gender Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Female','2',@GenderEnum, @ClientDataTag, 1)
-- Insert gender Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('Male','1',@GenderEnum, @ClientDataTag, 2)
-- Insert gender Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('NotDisclosed','0',@GenderEnum, @ClientDataTag, 3)
END

BEGIN -- Add ethnicity
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EthnicityEnum AND [ClientDataTag] = @ClientDataTag
-- Insert ethnicity items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'EthnicHeritage.HispanicLatino' THEN '1'
		WHEN 'EthnicHeritage.NonHispanicLatino' THEN '2'
	END,
	@EthnicityEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'EthnicHeritages' AND [Key] IN ('EthnicHeritage.HispanicLatino', 'EthnicHeritage.NonHispanicLatino')

END

BEGIN -- Add race
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @RaceEnum AND [ClientDataTag] = @ClientDataTag
-- Insert ethnicity items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Race.White' THEN 'Race_White_Flag'
		WHEN 'Race.BlackAfricanAmerican' THEN 'Race_Black_Or_African_American_Flag'
		WHEN 'Race.AlaskanAmericanIndian' THEN 'Race_American_Indian_Or_Alaskan_Native_Flag'
		WHEN 'Race.Asian' THEN 'Race_Asian_Flag'
		WHEN 'Race.HawaiianPacificIslander' THEN 'Race_Hawaiian_Native_Or_Other_Pacific_Islander_Flag'
		WHEN 'Race.NotDisclosed' THEN 'Race_Other_Flag' -- Map other to "Not Disclosed"
	END,
	@RaceEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Races' AND [Key] IN ('Race.White', 'Race.BlackAfricanAmerican', 'Race.AlaskanAmericanIndian', 'Race.Asian', 'Race.HawaiianPacificIslander', 'Race.NotDisclosed')

END

BEGIN -- Add Disability Status
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DisabilityStatus AND [ClientDataTag] = @ClientDataTag
-- Insert Disability Status
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('NotDisclosed','0',@DisabilityStatus, @ClientDataTag, 1),
           ('Disabled','1',@DisabilityStatus, @ClientDataTag, 1),
           ('NotDisabled','2',@DisabilityStatus, @ClientDataTag, 1)
END

BEGIN -- Add disability category
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @DisabilityCategoryEnum AND [ClientDataTag] = @ClientDataTag

	-- Insert disability categories
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'DisabilityCategory.NotDisclosed' THEN '9'
			WHEN 'DisabilityCategory.PhysicalImpairment' THEN '1'
			WHEN 'DisabilityCategory.MentalImpairment' THEN '2'
			WHEN 'DisabilityCategory.BothPhysicalMentalImpairment' THEN 'NotDisclosed'
		END,
		@DisabilityCategoryEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'DisabilityCategories' 
		AND [Key] IN 
		(
			'DisabilityCategory.NotDisclosed', 'DisabilityCategory.PhysicalImpairment', 'DisabilityCategory.MentalImpairment', 'DisabilityCategory.BothPhysicalMentalImpairment'
		)
END

BEGIN -- Add workshift look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @WorkShiftEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'WorkShifts.FirstDay' THEN '01'
		WHEN 'WorkShifts.SecondEvening' THEN '02'
		WHEN 'WorkShifts.ThirdNight' THEN '03'
		WHEN 'WorkShifts.Rotating' THEN '04'
		WHEN 'WorkShifts.Varies' THEN '05'
	END,
	@WorkShiftEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'WorkShifts' AND [Key] IN ('WorkShifts.FirstDay', 'WorkShifts.SecondEvening', 'WorkShifts.ThirdNight', 'WorkShifts.Rotating', 'WorkShifts.Varies')
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'WorkShifts.Varies' THEN '06'
	END,
	@WorkShiftEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'WorkShifts' AND [Key] IN ('WorkShifts.Varies')
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'WorkShifts.Varies' THEN '07'
	END,
	@WorkShiftEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'WorkShifts' AND [Key] IN ('WorkShifts.Varies')
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'WorkShifts.Varies' THEN '08'
	END,
	@WorkShiftEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'WorkShifts' AND [Key] IN ('WorkShifts.Varies')
END

BEGIN -- Add salary unit look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @SalaryUnitEnum AND [ClientDataTag] = @ClientDataTag
-- Insert salary unit Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Frequencies.Hourly' THEN '01'
		WHEN 'Frequencies.Daily' THEN '02'
		WHEN 'Frequencies.Weekly' THEN '03'
		WHEN 'Frequencies.Monthly' THEN '05'
		WHEN 'Frequencies.Yearly' THEN '06'
	END,
	@SalaryUnitEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Frequencies' AND [Key] IN ('Frequencies.Hourly', 'Frequencies.Daily', 'Frequencies.Weekly', 'Frequencies.Monthly', 'Frequencies.Yearly')
-- Insert salary unit Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Frequencies.Hourly' THEN 'Per Hour'
		WHEN 'Frequencies.Daily' THEN 'Per Day'
		WHEN 'Frequencies.Weekly' THEN 'Per Week'
		WHEN 'Frequencies.Monthly' THEN 'Per Month'
		WHEN 'Frequencies.Yearly' THEN 'Per Year'
	END,
	@SalaryUnitEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Frequencies' AND [Key] IN ('Frequencies.Hourly', 'Frequencies.Daily', 'Frequencies.Weekly', 'Frequencies.Monthly', 'Frequencies.Yearly')
END

BEGIN -- Add radius look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @RadiusEnum AND [ClientDataTag] = @ClientDataTag
-- Insert radius Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	[Key],
	@RadiusEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Radiuses' AND [Key] IN ('Radius.TwentyFiveMiles')
END

BEGIN -- Add school status look ups
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @SchoolStatusEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert veteran era Items
	INSERT INTO [Config.ExternalLookUpItem] 
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	VALUES
		('In_School_HS_OR_less','1',@SchoolStatusEnum, @ClientDataTag, 1),
		('In_School_Alternative_School','2',@SchoolStatusEnum, @ClientDataTag, 2),
		('In_School_Post_HS','3',@SchoolStatusEnum, @ClientDataTag, 3),
		('Not_Attending_School_OR_HS_Dropout','4',@SchoolStatusEnum, @ClientDataTag, 4),
		('Not_Attending_School_HS_Graduate','5',@SchoolStatusEnum, @ClientDataTag, 5)
END

BEGIN -- Add education level look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EducationLevelEnum AND [ClientDataTag] = @ClientDataTag
-- Insert education external Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('No_Grade_00','00',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_1_01','01',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_2_02','02',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_3_03','03',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_4_04','04',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_5_05','05',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_6_06','06',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_7_07','07',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_8_08','08',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_9_09','09',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_10_10','10',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_11_11','11',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_12_No_Diploma_12','11',@EducationLevelEnum, @ClientDataTag, 0),
           ('GED_88','88',@EducationLevelEnum, @ClientDataTag, 0),
           ('Grade_12_HS_Graduate_13','87',@EducationLevelEnum, @ClientDataTag, 0),
           ('HS_1_Year_College_OR_VOC_Tech_No_Degree_15','13',@EducationLevelEnum, @ClientDataTag, 0),
           ('HS_2_Year_College_OR_VOC_Tech_No_Degree_16','14',@EducationLevelEnum, @ClientDataTag, 0),
           ('HS_3_Year_College_OR_VOC_Tech_No_Degree_17','15',@EducationLevelEnum, @ClientDataTag, 0),
           ('Bachelors_OR_Equivalent_24','16',@EducationLevelEnum, @ClientDataTag, 0),
           ('Masters_Degree_25','MA',@EducationLevelEnum, @ClientDataTag, 0),
           ('Doctorate_Degree_26','PH',@EducationLevelEnum, @ClientDataTag, 0)

		   -- Need to enforce hierarchy so insert these afterwards to ensure higher Ids
			INSERT INTO [Config.ExternalLookUpItem]
					   ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
			VALUES
					   ('HS_3_Year_Associates_Degree_23','91',@EducationLevelEnum, @ClientDataTag, 0),
					   ('HS_2_Year_Associates_Degree_22','91',@EducationLevelEnum, @ClientDataTag, 0),
					   ('HS_1_Year_Associates_Degree_21','91',@EducationLevelEnum, @ClientDataTag, 0),
					   ('HS_3_Year_Vocational_Degree_20','90',@EducationLevelEnum, @ClientDataTag, 0),
					   ('HS_2_Year_Vocational_Degree_19','90',@EducationLevelEnum, @ClientDataTag, 0),
					   ('HS_1_Year_Vocational_Degree_18','90',@EducationLevelEnum, @ClientDataTag, 0),
					   ('Disabled_w_Cert_IEP_14','90',@EducationLevelEnum, @ClientDataTag, 0)
END

BEGIN -- Add education level look ups
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @JobEducationLevelEnum AND [ClientDataTag] = @ClientDataTag
-- Insert education external Items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
VALUES
           ('HighSchoolDiploma','01',@JobEducationLevelEnum, @ClientDataTag, 0),
           ('AssociatesDegree','02',@JobEducationLevelEnum, @ClientDataTag, 0),
           ('AssociatesDegree','03',@JobEducationLevelEnum, @ClientDataTag, 0),
           ('AssociatesDegree','04',@JobEducationLevelEnum, @ClientDataTag, 0),
           ('BachelorsDegree','05',@JobEducationLevelEnum, @ClientDataTag, 0),
           ('GraduateDegree','06',@JobEducationLevelEnum, @ClientDataTag, 0),
           ('DoctoralDegree','07',@JobEducationLevelEnum, @ClientDataTag, 0)
END

BEGIN -- Add work duration
-- Clear down this look up to avoid duplicates
DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @WorkDurationEnum AND [ClientDataTag] = @ClientDataTag
-- Insert workshift items
INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Duration.FullTimeTemporary' THEN 'Full-time temporary'
		WHEN 'Duration.FullTimeRegular' THEN 'Full-time'
		WHEN 'Duration.PartTimeTemporary' THEN 'Part-time temporary'
		WHEN 'Duration.PartTimeRegular' THEN 'Part-time'
	END,
	@WorkDurationEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Durations' AND [Key] IN ('Duration.FullTimeTemporary', 'Duration.FullTimeRegular', 'Duration.PartTimeTemporary', 'Duration.PartTimeRegular')

INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Duration.FullTimeTemporary' THEN 'Project/Contract'
		WHEN 'Duration.FullTimeRegular' THEN 'Apprenticeship'
		WHEN 'Duration.PartTimeTemporary' THEN 'On Call Temporary'
		WHEN 'Duration.PartTimeRegular' THEN 'On Call'
	END,
	@WorkDurationEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Durations' AND [Key] IN ('Duration.FullTimeTemporary', 'Duration.FullTimeRegular', 'Duration.PartTimeTemporary', 'Duration.PartTimeRegular')

INSERT INTO [Config.ExternalLookUpItem]
           ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
SELECT
	CAST(Id AS NVARCHAR(50)),
	CASE [Key]
		WHEN 'Duration.FullTimeTemporary' THEN 'Internship'
	END,
	@WorkDurationEnum, @ClientDataTag, Id
FROM
	[Config.LookUpItemsView]
WHERE
	LookUpType = 'Durations' AND [Key] IN ('Duration.FullTimeTemporary')
END

BEGIN -- Add work week
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @WorkWeekEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert workshift items
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'WorkWeeks.Any' THEN 'ANY'
		END,
		@WorkWeekEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'WorkWeeks' 
		AND [Key] IN ('WorkWeeks.Any')
END

BEGIN -- Add (job) employment status
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @JobEmploymentStatusEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert workshift items
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'EmploymentStatuses.Parttime' THEN 'Part-Time'
			WHEN 'EmploymentStatuses.Fulltime' THEN 'Full-Time'
		END,
		@JobEmploymentStatusEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'EmploymentStatuses' 
		AND [Key] IN ('EmploymentStatuses.Parttime', 'EmploymentStatuses.Fulltime')-- Insert workshift items
		
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'EmploymentStatuses.Parttime' THEN 'Part-Time Temporary'
			WHEN 'EmploymentStatuses.Fulltime' THEN 'Part-Time Temporary'
		END,
		@JobEmploymentStatusEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'EmploymentStatuses' 
		AND [Key] IN ('EmploymentStatuses.Parttime', 'EmploymentStatuses.Fulltime')
		
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'EmploymentStatuses.Parttime' THEN 'On Call'
			WHEN 'EmploymentStatuses.Fulltime' THEN 'Full-Time/Part-Time'
		END,
		@JobEmploymentStatusEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'EmploymentStatuses' 
		AND [Key] IN ('EmploymentStatuses.Parttime', 'EmploymentStatuses.Fulltime')
		
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'EmploymentStatuses.Parttime' THEN 'On Call Temporary'
			WHEN 'EmploymentStatuses.Fulltime' THEN 'Full-Time/Part-Time Temporary'
		END,
		@JobEmploymentStatusEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'EmploymentStatuses' 
		AND [Key] IN ('EmploymentStatuses.Parttime', 'EmploymentStatuses.Fulltime')
		
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'EmploymentStatuses.Fulltime' THEN 'Apprenticeship'
		END,
		@JobEmploymentStatusEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'EmploymentStatuses' 
		AND [Key] IN ('EmploymentStatuses.Fulltime')
END

BEGIN -- Add employer ownership type
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EmployerOwnershipTypeEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert ownership types items
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'OwnershipTypes.StateGovernment' THEN '2'
			WHEN 'OwnershipTypes.PrivateCorporation' THEN '5'
			WHEN 'OwnershipTypes.LocalGovernment' THEN '3'
			WHEN 'OwnershipTypes.ForeignInternational' THEN '4'
			WHEN 'OwnershipTypes.FederalGovernment' THEN '1'
		END,
		@EmployerOwnershipTypeEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'OwnershipTypes' 
		AND [Key] IN ('OwnershipTypes.StateGovernment', 'OwnershipTypes.PrivateCorporation', 'OwnershipTypes.LocalGovernment', 'OwnershipTypes.ForeignInternational', 'OwnershipTypes.FederalGovernment')
END
BEGIN -- Add jobtype
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @JobTypeEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert job types
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'JobTypes.Permanent' THEN 'Full-Time'
			WHEN 'JobTypes.Temporary' THEN 'Full-Time Temporary'
			WHEN 'JobTypes.Interim' THEN 'On Call'
		END,
		@JobTypeEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'JobTypes' 
		AND [Key] IN ('JobTypes.Permanent', 'JobTypes.Temporary', 'JobTypes.Interim')
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'JobTypes.Permanent' THEN 'Part-Time'
			WHEN 'JobTypes.Temporary' THEN 'Part-Time Temporary'
			WHEN 'JobTypes.Interim' THEN 'On Call Temporary'
		END,
		@JobTypeEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'JobTypes' 
		AND [Key] IN ('JobTypes.Permanent', 'JobTypes.Temporary', 'JobTypes.Interim')
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'JobTypes.Permanent' THEN 'Full-Time/Part-Time'
			WHEN 'JobTypes.Temporary' THEN 'Full-Time/Part-Time Temporary'
		END,
		@JobTypeEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'JobTypes' 
		AND [Key] IN ('JobTypes.Permanent', 'JobTypes.Temporary')
END

BEGIN -- Add employer ownership type
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @EmployerOwnershipTypeEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert ownership types items
	INSERT INTO [Config.ExternalLookUpItem]
		([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId])
	SELECT
		CAST(Id AS NVARCHAR(50)),
		CASE [Key]
			WHEN 'OwnershipTypes.StateGovernment' THEN '2'
			WHEN 'OwnershipTypes.PrivateCorporation' THEN '5'
			WHEN 'OwnershipTypes.LocalGovernment' THEN '3'
			WHEN 'OwnershipTypes.ForeignInternational' THEN '4'
			WHEN 'OwnershipTypes.FederalGovernment' THEN '1'
		END,
		@EmployerOwnershipTypeEnum, 
		@ClientDataTag, 
		Id
	FROM
		[Config.LookUpItemsView]
	WHERE
		LookUpType = 'OwnershipTypes' 
		AND [Key] IN ('OwnershipTypes.StateGovernment', 'OwnershipTypes.PrivateCorporation', 'OwnershipTypes.LocalGovernment', 'OwnershipTypes.ForeignInternational', 'OwnershipTypes.FederalGovernment')
END
BEGIN -- Add Counties
	-- Clear down this look up to avoid duplicates
	DELETE FROM [Config.ExternalLookUpItem] WHERE [ExternalLookUpType] = @CountyEnum AND [ClientDataTag] = @ClientDataTag
	-- Insert counties
	INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '001', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Adams'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '003', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Ashland'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '005', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Barron'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '007', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Bayfield'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '009', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Brown'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '011', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Buffalo'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '013', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Burnett'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '015', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Calumet'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '017', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Chippewa'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '019', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Clark'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '021', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Columbia'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '023', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Crawford'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '025', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Dane'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '027', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Dodge'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '029', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Door'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '031', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Douglas'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '033', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Dunn'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '035', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Eau Claire'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '037', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Florence'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '039', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Fond Du Lac'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '041', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Forest'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '043', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Grant'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '045', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Green'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '047', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Green Lake'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '049', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Iowa'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '051', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Iron'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '053', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Jackson'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '055', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Jefferson'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '057', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Juneau'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '059', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Kenosha'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '061', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Kewaunee'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '063', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'La Crosse'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '065', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Lafayette'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '067', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Langlade'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '069', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Lincoln'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '071', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Manitowoc'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '073', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Marathon'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '075', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Marinette'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '077', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Marquette'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '078', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Menominee'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '079', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Milwaukee'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '081', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Monroe'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '083', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Oconto'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '085', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Oneida'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '087', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Outagamie'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '089', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Ozaukee'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '091', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Pepin'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '093', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Pierce'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '095', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Polk'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '097', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Portage'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '099', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Price'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '101', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Racine'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '103', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Richland'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '105', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Rock'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '107', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Rusk'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '109', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'St. Croix'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '111', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Sauk'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '113', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Sawyer'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '115', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Shawano'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '117', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Sheboygan'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '119', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Taylor'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '121', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Trempealeau'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '123', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Vernon'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '125', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Vilas'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '127', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Walworth'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '129', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Washburn'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '131', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Washington'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '133', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Waukesha'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '135', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Waupaca'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '137', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Waushara'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '139', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Winnebago'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '141', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Wood'
-- Below items don't have a mapping but are included for completeness
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '986', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-Waukesha-Ozaukee-Washington'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '987', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-South Central'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '988', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-Milwaukee County'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '989', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-Fox Valley'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '990', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-Southeastern'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '991', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-Bay Area'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '992', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-North Central'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '993', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-Northwest'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '994', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-West Central'
INSERT INTO [Config.ExternalLookUpItem] ([InternalId],[ExternalId],[ExternalLookUpType],[ClientDataTag],[FocusId]) SELECT CAST(Id AS NVARCHAR(50)), '995', @CountyEnum, 'WI', Id FROM [Config.LookUpItemsView] WHERE LookUpType = 'Counties'  AND ParentKey = 'State.WI' AND [Value] = 'Out of State-Western'

END