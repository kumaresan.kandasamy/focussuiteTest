﻿DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'DetailedDegreeLevels.CertificateOrAssociate.NoEdit'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('DetailedDegreeLevels.CertificateOrAssociate.NoEdit', 'Diploma or Associate', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'Focus.CareerExplorer.WebExplorer.Report.CertificateOrAssociate'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Focus.CareerExplorer.WebExplorer.Report.CertificateOrAssociate', 'Diploma or Associate', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'ReportTypes.DegreeCertification.NoEdit'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('ReportTypes.DegreeCertification.NoEdit', 'Degree/Diploma', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'Focus.CareerExplorer.WebExplorer.Report.TypeADegreeCertification.Label'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Focus.CareerExplorer.WebExplorer.Report.TypeADegreeCertification.Label', 'type a degree or diploma', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'ExplorerEducationLevels.Certificate.NoEdit'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('ExplorerEducationLevels.Certificate.NoEdit', 'Diploma', 0, 12090)
end


DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'Focus.CareerExplorer.WebExplorer.Explore.StudyDegree.Label'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Focus.CareerExplorer.WebExplorer.Explore.StudyDegree.Label', 'Degrees and diplomas in demand', 0, 12090)
end



DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationList.DegreeList.Header.CertificateAndAssociateDegrees'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationList.DegreeList.Header.CertificateAndAssociateDegrees', 'Diploma and Associate Degrees', 0, 12090)
end


DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationList.DegreeList.Header.GraduateDegreesAndCertificates'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Focus.CareerExplorer.WebExplorer.Controls.DegreeCertificationList.DegreeList.Header.GraduateDegreesAndCertificates', 'Graduate Degrees and Diplomas', 0, 12090)
end
DELETE FROM [Config.LocalisationItem] WHERE [Config.LocalisationItem].[Key] = 'Focus.CareerExplorer.WebExplorer.Report.SpecificDegreeOrCertification.Text'
begin
	insert	[Config.LocalisationItem]
			([Key], [Value], [Localised], [LocalisationId])
	values	('Focus.CareerExplorer.WebExplorer.Report.SpecificDegreeOrCertification.Text', 'this specific degree or diploma', 0, 12090)
end

