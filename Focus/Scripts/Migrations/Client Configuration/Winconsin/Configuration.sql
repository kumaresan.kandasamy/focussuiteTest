﻿-- =============================================
-- Script Template
-- =============================================
RAISERROR ('Start - Configuration Item - Show contact details', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowContactDetails'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('ShowContactDetails', '1', 1)

RAISERROR ('End - Configuration Item - Show contact details', 0, 1) WITH NOWAIT

---

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'AccountTypeDisplayType'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('AccountTypeDisplayType', 'Optional', 1)

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HidePersonalInformation'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('HidePersonalInformation', 'True', 1)

---

RAISERROR ('Start - Configuration Item - Work Availability Search', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'WorkAvailabilitySearch'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('WorkAvailabilitySearch', 'True', 1)

RAISERROR ('End - Configuration Item - Work Availability Search', 0, 1) WITH NOWAIT

---

RAISERROR ('Start - Configuration Item - Has Minimum Hours Per Week', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HasMinimumHoursPerWeek'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('HasMinimumHoursPerWeek', 'True', 1)

RAISERROR ('End - Configuration Item - Has Minimum Hours Per Week', 0, 1) WITH NOWAIT


---

RAISERROR ('Start - Configuration Item - Work Net Links', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('ShowWorkNetLink', 'WorkNetLink')

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('ShowWorkNetLink', 'True', 1)

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('WorkNetLink', 'http://worknet.wisconsin.gov/worknet/jsoccsrch_results.aspx?menuselection=js&area={0}&areaname={1}&occ={2}', 1)

RAISERROR ('End - Configuration Item - Work Net Links', 0, 1) WITH NOWAIT

---

RAISERROR ('Start - Configuration Item - Integration client', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('IntegrationClient')

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('IntegrationClient', '7', 1)

RAISERROR ('End - Configuration Item - Integration client', 0, 1) WITH NOWAIT

---

RAISERROR ('Start - Configuration Item - Front end import', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('EnableFrontEndMigration')

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('EnableFrontEndMigration', 'true', 1)

RAISERROR ('End - Configuration Item - Front end import', 0, 1) WITH NOWAIT

---

RAISERROR ('Start - Configuration Item - Screening Preferences', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('ScreeningPrefExclusiveToMinStarMatch')

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('ScreeningPrefExclusiveToMinStarMatch', 'true', 1)

RAISERROR ('End - Configuration Item - Screening Preferences', 0, 1) WITH NOWAIT

---
RAISERROR ('Start - Configuration Item - Integration settings', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationSettings'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) 
	VALUES ('IntegrationSettings', '{"ServiceSecurityMode":"2","FocusServiceEndpoint":"https://acc.dwd.wisconsin.gov/NJCW/services/focus.svc","ConversionServiceEndpoint":"https://acc.dwd.wisconsin.gov/NJCW/services/conversion.svc","TokenServiceEndpoint":"https://fsextacc.wisconsin.gov/adfs/services/trust/13/usernamemixed","RelyingParty":"https://acc.dwd.wisconsin.gov/NJCW/","Username":"WIEXTACC\\AA-US-DET-BG-FS","Password":"Inst$gr#m_acc0unt" }', 1)

RAISERROR ('End - Configuration Item - Integration settings', 0, 1) WITH NOWAIT
---

---
RAISERROR ('Start - Configuration Item - Integration settings', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('ScreeningPrefBelowStarMatchApproval')

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('ScreeningPrefBelowStarMatchApproval', 'false', 1)

RAISERROR ('End - Configuration Item - Integration settings', 0, 1) WITH NOWAIT
---

---
RAISERROR ('Start - Configuration Item - Username as account link', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('UseUsernameAsAccountLink')

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('UseUsernameAsAccountLink', 'True', 1)

RAISERROR ('End - Configuration Item - Username as account link', 0, 1) WITH NOWAIT
---

---
RAISERROR ('Start - Configuration Item - Display JobActivityLog in Talent', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('DisplayJobActivityLogInTalent')

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('DisplayJobActivityLogInTalent', 'True', 1)

RAISERROR ('End - Configuration Item - Display JobActivityLog in Talent', 0, 1) WITH NOWAIT
---

---
RAISERROR ('Start - Configuration Item - Use resume template', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('UseResumeTemplate')

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('UseResumeTemplate', 'True', 1)

RAISERROR ('End - Configuration Item - Use resume template', 0, 1) WITH NOWAIT
---

---
RAISERROR ('Start - Configuration Item - Enable deactivate job seeker', 0, 1) WITH NOWAIT

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('SSOEnableDeactivateJobSeeker')

INSERT INTO [COnfig.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('SSOEnableDeactivateJobSeeker', 'true', 1)

RAISERROR ('End - Configuration Item - Enable deactivate job seeker', 0, 1) WITH NOWAIT
---
