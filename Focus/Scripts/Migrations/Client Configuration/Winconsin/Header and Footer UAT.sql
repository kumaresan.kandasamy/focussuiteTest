DECLARE @CustomFooterHtml NVARCHAR(MAX)
DECLARE @CustomHeaderHtml NVARCHAR(MAX)
DECLARE @CustomHeaderHtmlAssist NVARCHAR(MAX)
DECLARE @CustomHeaderHtmlTalent NVARCHAR(MAX)
DECLARE @CustomHeaderHtmlCareerExplorer NVARCHAR(MAX)

SET @CustomFooterHtml = N'<div id="dwd_left_connect_icons"> <a class="dwdNobg dwdNodec" href="http://twitter.com/intent/user?screen_name=JobCenterWI"> <img title="JCW on Twitter" alt="JCW on Twitter" src="https://dwd.wisconsin.gov/det/include/njcw/twitter.png"/></a> <a class="dwdNobg dwdNodec" href="http://www.facebook.com/jobcenterofwisconsin"> <img title="JCW on Facebook" alt="JCW on Facebook" src="https://dwd.wisconsin.gov/det/include/njcw/facebook.png"/></a> <a class="dwdNobg dwdNodec" href="https://jobcenterofwisconsin.com/services/RSS.aspx"> <img title="JCW RSS Feed" alt="JCW RSS Feed" src="https://dwd.wisconsin.gov/det/include/njcw/feed.png"/></a>  <a class="dwdNobg dwdNodec" href="mailto:JobCenterofWisconsin@dwd.wisconsin.gov"> <img title="Email JCW" alt="Email JCW" src="https://dwd.wisconsin.gov/det/include/njcw/mail.png"/></a> </div><div class="dwdNoprint" id="dwdFooter" role="contentinfo"> <nav class="dwdNoprint" role="navigation"> <ul> <li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/EOE.aspx">EOE</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/TermsofUse.aspx">Terms of Use</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/LegalInformation.aspx">Legal Information</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/ContactUs.aspx">Contact Us</a></li><li><a href="http://www.wisconsinjobcenter.org/jcw/warnings.htm ">Protecting Yourself Online</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/DWD.aspx">Wisconsin DWD</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Privacy.aspx">Privacy Notice</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/SupportedBrowsers.aspx">Supported Browsers</a></li><li class="dwdNo_border"><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Accessibility.aspx">Accessibility</a></li></ul> <div id="dwd_bottom_connect_icons"> <a class="dwdNobg dwdNodec" href="http://twitter.com/intent/user?screen_name=JobCenterWI"> <img title="JCW on Twitter" alt="JCW on Twitter" src="https://dwd.wisconsin.gov/det/include/njcw/twitter.png"/></a> <a class="dwdNobg dwdNodec" href="http://www.facebook.com/jobcenterofwisconsin"> <img title="JCW on Facebook" alt="JCW on Facebook" src="https://dwd.wisconsin.gov/det/include/njcw/facebook.png"/></a> <a class="dwdNobg dwdNodec" href="https://jobcenterofwisconsin.com/services/RSS.aspx"> <img title="JCW RSS Feed" alt="JCW RSS Feed" src="https://dwd.wisconsin.gov/det/include/njcw/feed.png"/></a>  <a class="dwdNobg dwdNodec" href="mailto:JobCenterofWisconsin@dwd.wisconsin.gov"> <img title="Email JCW" alt="Email JCW" src="https://dwd.wisconsin.gov/det/include/njcw/mail.png"/></a> </div></nav> </div><script src="https://dwd.wisconsin.gov/det/include/njcw/responsive.js" type="text/javascript"></script>'
SET @CustomHeaderHtml = N'<link href="https://dwd.wisconsin.gov/det/include/njcw/dwd_main.css" type="text/css" rel="stylesheet"/><span class="dwdSkip"> <a href="#skipnavigation" title="Jump past the navigation menu to the main content">Skip To Main Content</a> | <a title="Get information on site accessibility features" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Accessibility.aspx#skipnavigation">Accessibility Features </a> </span><div id="dwdBanner" role="banner"><span style="position: absolute;"><a id="dwdTop"></a></span><div style="text-align: center"><a id="dwdScreenimg" href="https://acc.dwd.wisconsin.gov/njcw/"><img title="Job Center of Wisconsin" id="dwdPrintimg" alt="JCW Logo" src="https://dwd.wisconsin.gov/det/include/njcw/logo.png"/> </a> </div><div id="dwdMenuContainer"> <div id="dwdResponsiveMenu"> <img width="120" height="40" id="dwdShowMenuBtn" alt="Menu button" src="https://dwd.wisconsin.gov/det/include/njcw/menu_btn.png"/> <img width="120" height="40" id="dwdHideMenuBtn" style="display: none;" alt="Menu button hide" src="https://dwd.wisconsin.gov/det/include/njcw/menu_btn_close.png"/> </div><div id="dwdTopnav"> <ul id="dwdFirst_lvl_btns"> <li class="dwdNav-btn" id="dwdTab-jobseeker"> <a id="dwdLink-jobseeker" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/Default.aspx">Job Seeker Tools</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/jobsearch/searchjobpostings">Search for Jobs</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/MyJCW.aspx">My JCW</a></li><li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/explorer/explore">Explore Careers</a></li><li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/resume/create">Create a Resume</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/RESAssessmentSummary.aspx">Re-employment Services Orientation/Assessment</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/RESSessionSummary.aspx">Re-employment Services Session</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/OnlineWorkshopSummary.aspx">Online Workshops</a></li><li><a href="http://www.wisconsinjobcenter.org/jobfairs/">Job Fairs</a></li><li><a href="http://www.wisconsinjobcenter.org/careerplanning/default.htm">Career Planning Tools</a></li><li><a href="http://www.wisconsinjobcenter.org/ncrc/jobseeker/">National Career Readiness Certificate</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/OtherResources.aspx">Other Resources</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-industries"><a id="dwdLink-industries" href="https://acc.dwd.wisconsin.gov/njcw/Industries/Default.aspx">Industries</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://acc.dwd.wisconsin.gov/njcw/Agriculture/Default.aspx">Agriculture</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Finance/Default.aspx">Finance/Insurance</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Manufacturing/Default.aspx">Manufacturing</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Trucking/Default.aspx">Trucking</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-employer"><a id="dwdLink-employer" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Employers/Default.aspx">Employer Tools</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://uat-wisconsin.focus-career.com/talent/jobs/active">Post a Job</a></li><li><a href="https://uat-wisconsin.focus-career.com/talent/pool/allresumes/0">Search Resumes</a></li><li><a href="http://www.wisconsinjobcenter.org/ncrc/employer/">National Career Readiness Certificate</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/UnemploymentInsurance.aspx">Unemployment Insurance</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/NowHiringReport.aspx">New Hire Reporting</a></li><li><a href="http://www.wisconsinjobcenter.org/businessassistance/taxcredits.htm">Hiring Incentives for Business</a></li><li><a href="http://www.wisconsinjobcenter.org/jobfairs/">Job Fairs</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Employers/OtherResources.aspx">Other Resources</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-about"><a id="dwdLink-about" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/AboutUs.aspx">About Us</a></li><li class="dwdNav-btn" id="dwdTab-help"><a id="dwdLink-help" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Help.aspx">Help</a></li></ul> </div></div></div><a name="skipnavigation"></a>'
SET @CustomHeaderHtmlAssist = N'<link href="https://dwd.wisconsin.gov/det/include/njcw/dwd_main.css" type="text/css" rel="stylesheet"/><span class="dwdSkip"> <a href="#skipnavigation" title="Jump past the navigation menu to the main content">Skip To Main Content</a> | <a title="Get information on site accessibility features" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Accessibility.aspx#skipnavigation">Accessibility Features </a> </span><div id="dwdBanner" role="banner"><span style="position: absolute;"><a id="dwdTop"></a></span><div style="text-align: center"><a id="dwdScreenimg" href="https://acc.dwd.wisconsin.gov/njcw/default.aspx"><img title="Job Center of Wisconsin Logo and Link to the JCW Homepage " id="dwdPrintimg" alt="Job Center of Wisconsin Logo and Link to the JCW Homepage" src="https://dwd.wisconsin.gov/det/include/njcw/logo.png"/> </a> </div><div id="dwdMenuAssist"></div></div><a name="skipnavigation"></a>'
SET @CustomHeaderHtmlTalent = N'<link href="https://dwd.wisconsin.gov/det/include/njcw/dwd_main.css" type="text/css" rel="stylesheet"/><span class="dwdSkip"> <a href="#skipnavigation" title="Jump past the navigation menu to the main content">Skip To Main Content</a> | <a title="Get information on site accessibility features" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Accessibility.aspx#skipnavigation">Accessibility Features </a> </span><div id="dwdBanner" role="banner"><span style="position: absolute;"><a id="dwdTop"></a></span><div style="text-align: center"><a id="dwdScreenimg" href="https://acc.dwd.wisconsin.gov/njcw/"><img title="Job Center of Wisconsin Logo and Link to the JCW Homepage " id="dwdPrintimg" alt="Job Center of Wisconsin Logo and Link to the JCW Homepage" src="https://dwd.wisconsin.gov/det/include/njcw/logo.png"/> </a> </div><div id="dwdMenuContainer"> <div id="dwdResponsiveMenu"> <img width="120" height="40" id="dwdShowMenuBtn" alt="Menu button" src="https://dwd.wisconsin.gov/det/include/njcw/menu_btn.png"/> <img width="120" height="40" id="dwdHideMenuBtn" style="display: none;" alt="Menu button hide" src="https://dwd.wisconsin.gov/det/include/njcw/menu_btn_close.png"/> </div><div id="dwdTopnav"> <ul id="dwdFirst_lvl_btns"> <li class="dwdNav-btn" id="dwdTab-jobseeker"> <a id="dwdLink-jobseeker" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/Default.aspx">Job Seeker Tools</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/jobsearch/searchjobpostings">Search for Jobs</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/MyJCW.aspx">My JCW</a></li><li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/explorer/explore">Explore Careers</a></li><li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/resume/create">Create a Resume</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/RESAssessmentSummary.aspx">Re-employment Services Orientation/Assessment</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/RESSessionSummary.aspx">Re-employment Services Session</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/OnlineWorkshopSummary.aspx">Online Workshops</a></li><li><a href="http://www.wisconsinjobcenter.org/jobfairs/">Job Fairs</a></li><li><a href="http://www.wisconsinjobcenter.org/careerplanning/default.htm">Career Planning Tools</a></li><li><a href="http://www.wisconsinjobcenter.org/ncrc/jobseeker/">National Career Readiness Certificate</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/OtherResources.aspx">Other Resources</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-industries"><a id="dwdLink-industries" href="https://acc.dwd.wisconsin.gov/njcw/Industries/Default.aspx">Industries</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://acc.dwd.wisconsin.gov/njcw/Agriculture/Default.aspx">Agriculture</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Finance/Default.aspx">Finance/Insurance</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Manufacturing/Default.aspx">Manufacturing</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Trucking/Default.aspx">Trucking</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-employer"><a id="dwdLink-employer" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Employers/Default.aspx">Employer Tools</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://uat-wisconsin.focus-career.com/talent/jobs/active">Post a Job</a></li><li><a href="https://uat-wisconsin.focus-career.com/talent/pool/allresumes/0">Search Resumes</a></li><li><a href="http://www.wisconsinjobcenter.org/ncrc/employer/">National Career Readiness Certificate</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/UnemploymentInsurance.aspx">Unemployment Insurance</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/NowHiringReport.aspx">New Hire Reporting</a></li><li><a href="http://www.wisconsinjobcenter.org/businessassistance/taxcredits.htm">Hiring Incentives for Business</a></li><li><a href="http://www.wisconsinjobcenter.org/jobfairs/">Job Fairs</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Employers/OtherResources.aspx">Other Resources</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-about"><a id="dwdLink-about" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/AboutUs.aspx">About Us</a></li><li class="dwdNav-btn" id="dwdTab-help"><a id="dwdLink-help" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Help.aspx">Help</a></li></ul> </div></div></div><script type="text/javascript">if (SiteMaster_LoggedInUserName && SiteMaster_LoggedInUserName != null){var startText = ''Hi, '',nameToShow;if (SiteMaster_IsShadowingUser) {startText  = ''HELPING '';nameToShow = SiteMaster_LoggedInUserName;} else {firstName  = SiteMaster_LoggedInUserName.split(" ");nameToShow = firstName[0];} document.write(''<span class="dwdUsernameSpeechBubble" id="dwdUsernameSpeechBubblePanel"><span class="dwdUsernameSpeechBubbleLeft"></span><span class="dwdUsernameSpeechBubbleRight"><span class="dwdUsernameSpeechBubbleText">'' + startText + nameToShow + ''</span></span></span>'');document.write(''<div id="dwdToolbarBottom"><nav><ul><li><a href="https://acc.dwd.wisconsin.gov/njcw/presentation/AccountManagement.aspx">My account</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/signout.aspx">Sign out</a></li></ul></nav></div>'')} </script><a name="skipnavigation"></a>'
SET @CustomHeaderHtmlCareerExplorer = N'<link href="https://dwd.wisconsin.gov/det/include/njcw/dwd_main.css" type="text/css" rel="stylesheet"/><span class="dwdSkip"> <a href="#skipnavigation" title="Jump past the navigation menu to the main content">Skip To Main Content</a> | <a title="Get information on site accessibility features" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Accessibility.aspx#skipnavigation">Accessibility Features </a> </span><div id="dwdBanner" role="banner"><span style="position: absolute;"><a id="dwdTop"></a></span><div style="text-align: center"><a id="dwdScreenimg" href="https://acc.dwd.wisconsin.gov/njcw/"><img title="Job Center of Wisconsin Logo and Link to the JCW Homepage " id="dwdPrintimg" alt="Job Center of Wisconsin Logo and Link to the JCW Homepage" src="https://dwd.wisconsin.gov/det/include/njcw/logo.png"/> </a> </div><div id="dwdMenuContainer"> <div id="dwdResponsiveMenu"> <img width="120" height="40" id="dwdShowMenuBtn" alt="Menu button" src="https://dwd.wisconsin.gov/det/include/njcw/menu_btn.png"/> <img width="120" height="40" id="dwdHideMenuBtn" style="display: none;" alt="Menu button hide" src="https://dwd.wisconsin.gov/det/include/njcw/menu_btn_close.png"/> </div><div id="dwdTopnav"> <ul id="dwdFirst_lvl_btns"> <li class="dwdNav-btn" id="dwdTab-jobseeker"> <a id="dwdLink-jobseeker" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/Default.aspx">Job Seeker Tools</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/jobsearch/searchjobpostings">Search for Jobs</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/MyJCW.aspx">My JCW</a></li><li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/explorer/explore">Explore Careers</a></li><li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/resume/create">Create a Resume</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/RESAssessmentSummary.aspx">Re-employment Services Orientation/Assessment</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/RESSessionSummary.aspx">Re-employment Services Session</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/OnlineWorkshopSummary.aspx">Online Workshops</a></li><li><a href="http://www.wisconsinjobcenter.org/jobfairs/">Job Fairs</a></li><li><a href="http://www.wisconsinjobcenter.org/careerplanning/default.htm">Career Planning Tools</a></li><li><a href="http://www.wisconsinjobcenter.org/ncrc/jobseeker/">National Career Readiness Certificate</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/JobSeekers/OtherResources.aspx">Other Resources</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-industries"><a id="dwdLink-industries" href="https://acc.dwd.wisconsin.gov/njcw/Industries/Default.aspx">Industries</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://acc.dwd.wisconsin.gov/njcw/Agriculture/Default.aspx">Agriculture</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Finance/Default.aspx">Finance/Insurance</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Manufacturing/Default.aspx">Manufacturing</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Trucking/Default.aspx">Trucking</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-employer"><a id="dwdLink-employer" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Employers/Default.aspx">Employer Tools</a> <ul class="dwdHideOnMedium dwdHideonMobile"> <li><a href="https://uat-wisconsin.focus-career.com/talent/jobs/active">Post a Job</a></li><li><a href="https://uat-wisconsin.focus-career.com/talent/pool/allresumes/0">Search Resumes</a></li><li><a href="http://www.wisconsinjobcenter.org/ncrc/employer/">National Career Readiness Certificate</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/UnemploymentInsurance.aspx">Unemployment Insurance</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/NowHiringReport.aspx">New Hire Reporting</a></li><li><a href="http://www.wisconsinjobcenter.org/businessassistance/taxcredits.htm">Hiring Incentives for Business</a></li><li><a href="http://www.wisconsinjobcenter.org/jobfairs/">Job Fairs</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Employers/OtherResources.aspx">Other Resources</a></li></ul> </li><li class="dwdNav-btn" id="dwdTab-about"><a id="dwdLink-about" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/AboutUs.aspx">About Us</a></li><li class="dwdNav-btn" id="dwdTab-help"><a id="dwdLink-help" href="https://acc.dwd.wisconsin.gov/njcw/Presentation/Help.aspx">Help</a></li></ul> </div></div></div><script type="text/javascript">if (SiteMaster_LoggedInUserName && SiteMaster_LoggedInUserName != null){var startText = ''Hi, '',nameToShow;if (SiteMaster_IsShadowingUser) {startText  = ''HELPING '';nameToShow = SiteMaster_LoggedInUserName;} else {firstName  = SiteMaster_LoggedInUserName.split(" ");nameToShow = firstName[0];} document.write(''<span class="dwdUsernameSpeechBubble" id="dwdUsernameSpeechBubblePanel"><span class="dwdUsernameSpeechBubbleLeft"></span><span class="dwdUsernameSpeechBubbleRight"><span class="dwdUsernameSpeechBubbleText">'' + startText + nameToShow + ''</span></span></span>'');document.write(''<div id="dwdToolbarBottom"><nav><ul><li><a href="https://uat-wisconsin.focus-career.com/careerexplorer/mybookmarks">My bookmarks</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/presentation/AccountManagement.aspx">My account</a></li><li><a href="https://acc.dwd.wisconsin.gov/njcw/signout.aspx">Sign out</a></li></ul></nav></div>'')} </script><a name="skipnavigation"></a>'

IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'UseCustomHeaderHtml')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'UseCustomHeaderHtml',
		'True',
		1
		)
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'UseCustomFooterHtml')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'UseCustomFooterHtml',
		'True',
		1
		)
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomHeaderHtml')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'CustomHeaderHtml',
		@CustomHeaderHtml,
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = @CustomHeaderHtml 
		WHERE [Key] = 'CustomHeaderHtml'
	END

IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomHeaderHtml-Workforce-Talent')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'CustomHeaderHtml-Workforce-Talent',
		@CustomHeaderHtmlTalent,
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = @CustomHeaderHtmlTalent 
		WHERE [Key] = 'CustomHeaderHtml-Workforce-Talent'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomHeaderHtml-Workforce-Assist')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'CustomHeaderHtml-Workforce-Assist',
		@CustomHeaderHtmlAssist,
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = @CustomHeaderHtmlAssist 
		WHERE [Key] = 'CustomHeaderHtml-Workforce-Assist'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomHeaderHtml-Workforce-CareerExplorer')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'CustomHeaderHtml-Workforce-CareerExplorer',
		@CustomHeaderHtmlCareerExplorer,
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = @CustomHeaderHtmlCareerExplorer 
		WHERE [Key] = 'CustomHeaderHtml-Workforce-CareerExplorer'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'CustomFooterHtml',
		@CustomFooterHtml,
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = @CustomFooterHtml  
		WHERE [Key] = 'CustomFooterHtml'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml-Workforce-Talent')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'CustomFooterHtml-Workforce-Talent',
		@CustomFooterHtml,
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = @CustomFooterHtml 
		WHERE [Key] = 'CustomFooterHtml-Workforce-Talent'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml-Workforce-Assist')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'CustomFooterHtml-Workforce-Assist',
		@CustomFooterHtml,
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = @CustomFooterHtml 
		WHERE [Key] = 'CustomFooterHtml-Workforce-Assist'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'CustomFooterHtml-Workforce-CareerExplorer')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'CustomFooterHtml-Workforce-CareerExplorer',
		@CustomFooterHtml,
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = @CustomFooterHtml 
		WHERE [Key] = 'CustomFooterHtml-Workforce-CareerExplorer'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'BrandIdentifier')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'BrandIdentifier',
		'Wisconsin',
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = 'Wisconsin' 
		WHERE [Key] = 'BrandIdentifier'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowBurningGlassLogoInCareerExplorer')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'ShowBurningGlassLogoInCareerExplorer',
		'False',
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = 'False' 
		WHERE [Key] = 'ShowBurningGlassLogoInCareerExplorer'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowBurningGlassLogoInAssist')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'ShowBurningGlassLogoInAssist',
		'False',
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = 'False' 
		WHERE [Key] = 'ShowBurningGlassLogoInAssist'
	END
	
IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowBurningGlassLogoInTalent')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] 
		(
		[Key],
		[Value],
		[InternalOnly]
		)
		VALUES
		(
		'ShowBurningGlassLogoInTalent',
		'False',
		1
		)
	END
ELSE
	BEGIN
		UPDATE [Config.ConfigurationItem] 
		SET [Value] = 'False' 
		WHERE [Key] = 'ShowBurningGlassLogoInTalent'
	END