DECLARE @OfficeIdToRemove BIGINT
DECLARE @OfficeIdToAssign BIGINT

-- THESE IDs WILL NEED TO BE CHANGED DURING TESTING
SET @OfficeIdToRemove = 4928289
SET @OfficeIdToAssign = 4927769
 
DECLARE @JobSeekers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	PersonId BIGINT
)
 
 --Identify Jobseekers who are assigned to the office to be deleted but are also assigned to another office
 INSERT INTO @JobSeekers
 SELECT
 pom.PersonId
FROM
 [Data.Application.User] U
INNER JOIN [Data.Application.Person] P
 ON P.Id = U.PersonId
INNER JOIN [Data.Application.PersonOfficeMapper] POM
 ON POM.PersonId = P.Id 
 AND POM.OfficeId = @OfficeIdToRemove
WHERE 
 U.UserType = 12
 AND EXISTS
 (
  SELECT
   1
  FROM
   [Data.Application.PersonOfficeMapper] POM2
  WHERE
   POM2.PersonId = P.Id
   AND POM2.OfficeId <> @OfficeIdToRemove
 )

BEGIN TRANSACTION

	--For the jobseekers who are also assigned to another office (identified above) just simply delete the POM row assigning them to the office to be deleted
	DELETE pom FROM [Data.Application.PersonOfficeMapper] pom
	INNER JOIN @JobSeekers j ON pom.PersonId = j.PersonId
	WHERE OfficeId = @OfficeIdToRemove

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	--Update all remaining jobseekers to be assigned to the new office
	UPDATE pom
	SET OfficeId = @OfficeIdToAssign
	FROM  [Data.Application.PersonOfficeMapper] pom
	INNER JOIN [Data.Application.Person] p on pom.PersonId = p.Id
	INNER JOIN [Data.Application.User] u on p.Id = u.PersonId
	where u.UserType= 12 AND pom.OfficeId = @OfficeIdToRemove
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION