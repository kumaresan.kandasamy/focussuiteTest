UPDATE
	[Data.Application.Job] 
SET
	ExtendVeteranPriority = NULL,
	VeteranPriorityEndDate = NULL
WHERE 
	ExtendVeteranPriority IN (1, 2)

