IF EXISTS (SELECT 1 FROM [Config.ConfigurationItem] WHERE [key] = 'IntegrationLoggableActions')		
	IF NOT EXISTS (SELECT 1 FROM  [Config.ConfigurationItem] WHERE [key] = 'IntegrationLoggableActions' and value like '%DeleteResume%')
	BEGIN
		UPDATE [Config.ConfigurationItem] SET [value] = [value] + ',DeleteResume' WHERE [key] = 'IntegrationLoggableActions'	
		PRINT '''DeleteResume'' added to existing IntegrationLoggableActions setting within table [Config.ConfigurationItem]'
	END
	ELSE
		PRINT '''DeleteResume'' already exists as a value for the IntegrationLoggableActions setting within table [Config.ConfigurationItem]'		
ELSE
BEGIN
	INSERT INTO [Config.ConfigurationItem] 
		VALUES ('IntegrationLoggableActions',
				'SaveResume,DeleteResume,LensJobSearch,ViewedPostingFromSearch,FindJobsForSeeker,SelfReferral,ExternalReferral,UpdateApplicationStatusToHired,
				UpdateApplicationStatusToNotHired,UpdateApplicationStatusToDidNotApply,UpdateApplicationStatusToFailedToRespondToInvitation,
				UpdateApplicationStatusToFailedToReportToJob,UpdateApplicationStatusToFoundJobFromOtherSource,UpdateApplicationStatusToInterviewDenied,
				UpdateApplicationStatusToInterviewScheduled,UpdateApplicationStatusToJobAlreadyFilled,UpdateApplicationStatusToNotQualified,UpdateApplicationStatusToRecommended,
				UpdateApplicationStatusToRefusedOffer,UpdateApplicationStatusToRefusedReferral,UpdateApplicationStatusToUnderConsideration,RegisterAccount,CompleteResume,
				CompleteNonDefaultResume,LogIn,ChangeJobSeekerSsn,UpdateCandidateStatus,AssignActivityToCandidate,ExternalStaffReferral,CreateCandidateApplication,
				CreateSingleSignOn,ReferralRequest,ApproveCandidateReferral,PostJobToLens,AutoApprovedReferralBypass,InviteJobSeekerToApply,UnregisterJobFromLens,
				UpdateJobAssignedOffice', 
				1)
	PRINT '''IntegrationLoggableActions'' setting added to table [Config.ConfigurationItem]'
END