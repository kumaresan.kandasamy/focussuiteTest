DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('IntegrationClient', '5', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationSettings'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('IntegrationSettings', '{"Url": "https://demo.atworksolutionsinc.com/BGToT1/BGToT1.asmx?op=FullXML", "Customer": "T1Demo", "AuthCode": "P3@nutButt3r@ndJ3lly"}', 1)