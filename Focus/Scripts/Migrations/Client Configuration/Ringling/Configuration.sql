﻿-- =============================================
-- Script Template
-- =============================================
Print 'Start - Configuration Item - Show program area'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowProgramArea'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('ShowProgramArea','False',1)

Print 'End - Configuration Item - Show program area'

Print 'Start - Configuration Item - Degree client data tag'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeClientDataTag'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('ExplorerDegreeClientDataTag','RING',1)

Print 'End - Configuration Item - Degree client data tag'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES ('TalentModulePresent','False',1)

Print 'Start - Configuration Item - Degree filter'

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerDegreeFilteringType'

INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ExplorerDegreeFilteringType', '2', 1)

Print 'End - Configuration Item - Degree filter'
