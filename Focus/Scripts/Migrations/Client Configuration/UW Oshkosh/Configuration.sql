-- University of Wisconsin Oshkosh configuration settings
DELETE FROM [Config.COnfigurationItem] WHERE [Key] = 'ExplorerDegreeClientDataTag'
INSERT INTO [Config.COnfigurationItem]([Key], Value, InternalOnly) VALUES ('ExplorerDegreeClientDataTag', 'UWOSHKOSH', 1)
DELETE FROM [Config.COnfigurationItem] WHERE [Key] = 'ExplorerDegreeFilteringType'
INSERT INTO [Config.COnfigurationItem]([Key], Value, InternalOnly) VALUES ('ExplorerDegreeFilteringType', '2', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'SupportEmail'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('SupportEmail','cservice@uwosh.edu',1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'SupportPhone'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('SupportPhone','920-424-2181 ',1)
