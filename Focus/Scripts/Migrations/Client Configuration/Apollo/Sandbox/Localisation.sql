﻿DECLARE @Items TABLE
(
 [Key] NVARCHAR(200),
 [Value] NVARCHAR(4000)
)
INSERT INTO @Items([Key], [Value])
VALUES
('Focus.Web.WebTalent.Controls.WelcomeToTalentModal.WelcomeContentLiteral.Text', '<b>Welcome to #FOCUSTALENT#</b><br/>
<br/>
Please click the appropriate tab above to post jobs or search for talent.<br/>
<br/> For user support, please contact us at <a href="mailto:education.careers@phoenix.edu">education.careers@phoenix.edu</a>
<br/>
For all technical inquiries, email <a href="mailto:technical.support@apollo.edu">technical.support@apollo.edu </a><br/>or call (877) 832-4867')

INSERT INTO @Items([Key], [Value])
VALUES
('Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEIN.Body','Our staff will review your account request and get back to you shortly. In the meantime, you may continue to search for qualified talent or create job listings to post. Bear in mind that any job post you create or job candidate you contact through our system will be queued up while your account is under review. Once approved, these job listings and contacts will automatically post. For all technical inquiries, email #SUPPORTEMAIL# or call #SUPPORTPHONE# for registration assistance.')

INSERT INTO @Items([Key], [Value])
VALUES
('Focus.Web.WebTalent.Register.SuccessfulSubmitInvalidFEINNoAccess.Body','Our staff will review your account request and get back to you shortly. Once approved, you will be able to search for qualified talent or create job listings to post. For all technical inquiries, email #SUPPORTEMAIL# or call #SUPPORTPHONE# for registration assistance.')

INSERT INTO @Items([Key], [Value])
VALUES
('Focus.Web.WebTalent.Register.SuccessfulSubmitValidFEIN.Body','We''ve sent an email to your inbox with a hyperlink to confirm your email address and complete your account registration. The email will also include information about the account validation process and provide contact information for questions or assistance. When you check your email, click on the link to authenticate your account. If you do not receive this email within 24 hours, please check your computer or network''s filters, which could block your receipt or place mail in a spam folder.<br />For all technical inquiries, email #SUPPORTEMAIL# or call #SUPPORTPHONE# for registration assistance.')


DECLARE @LocalisationId BIGINT
SELECT @LocalisationId = Id FROM [Config.Localisation] WHERE Culture = '**-**'

INSERT INTO [Config.LocalisationItem] 
(
 ContextKey, 
 [Key], 
 Value, 
 Localised, 
 LocalisationId
)
SELECT
 '',
 I.[Key],
 I.[Value],
 0,
 @LocalisationId
FROM
 @Items I
WHERE
 NOT EXISTS
 (
  SELECT
   1
  FROM
   [Config.LocalisationItem] LI
  WHERE
   LI.[Key] = I.[Key]
   AND LI.LocalisationId = @LocalisationId
 )



  ----Remove reference to WF text from EDU Talent sign up see: WPS-552

DECLARE @LocalisationId as bigint = (SELECT [Id] FROM [dbo].[Config.Localisation] WHERE [Culture] = '**-**')
DELETE FROM [dbo].[Config.LocalisationItem] WHERE [Key] = 'EmployerWasNotFound.Item2a'   
INSERT INTO [dbo].[Config.LocalisationItem]([ContextKey],[Key],[Value],[Localised],[LocalisationId]) VALUES
('',
'EmployerWasNotFound.Item2a',
'If you are a new #BUSINESS#:LOWER, our staff must confirm your FEIN to approve your registration. This normally takes {0} business days.',
0,
@LocalisationId)
