DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('IntegrationClient', '4', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationSettings'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('IntegrationSettings', '{"LiveInstance":"true", "BaseUrl": "https://developer.aptimus.com/api/", "JobUrlSuffix": "job-service/1/jobs", "Username": "jmacbeth@burning-glass.com", "Password":"01252Apt"}', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'SplitJobsByLocation'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('SplitJobsByLocation', 'True', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'CareerSearchResumesSearchable'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('CareerSearchResumesSearchable', '1', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ClientApplyToPostingUrl'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('ClientApplyToPostingUrl', 'https://careers.phoenix.edu/job-search/job-details.49.#ID#.html', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideInterviewContactPreferences'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('HideInterviewContactPreferences', 'True', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'HideScreeningPreferences'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('HideScreeningPreferences', 'True', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'JobSeekerReferralsEnabled'
INSERT INTO [Config.ConfigurationItem] ([Key], [Value], InternalOnly) VALUES ('JobSeekerReferralsEnabled', 'False', 1)
DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'ShowMilitaryStatus'
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly) VALUES ('ShowMilitaryStatus','True',1)