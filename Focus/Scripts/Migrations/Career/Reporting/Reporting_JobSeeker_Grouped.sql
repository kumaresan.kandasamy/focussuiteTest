USE [FocusTestRepReporting]
GO

/****** Object:  StoredProcedure [dbo].[Reporting_JobSeeker_Grouped]    Script Date: 02/25/2013 14:52:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









  
  
ALTER	 PROCEDURE [dbo].[Reporting_JobSeeker_Grouped]  
 @EducationLevel INT,  
 @CertAndLicenses NVARCHAR(MAX) = NULL,  
 @Skills NVARCHAR(MAX),  
 @OccupationGroup NVARCHAR(MAX),  
 @DetailedOccupation NVARCHAR(MAX), 
 @IndustryGroup NVARCHAR(MAX), -- To do (Awaiting DB structure)  
 @DetailedIndustry NVARCHAR(MAX), -- To do (Awaiting DB structure)  
 @SalaryFrequency INT,  
 @MinSalary DECIMAL,  
 @MaxSalary DECIMAL,  
 @County NVARCHAR(MAX),  
 @Office NVARCHAR(MAX),  
 @WIB NVARCHAR(MAX),  
 @Radius INT,  
 @ZIP NVARCHAR(20),  
 @DateFrom DATETIME,  
 @DateTo DATETIME,  
 @VeteranDefinition INT,  
 @VeteranType INT,  
 @VeteranEmploymentStatus INT,  
 @VeteranTransactionType INT,  
 @VeteranMilitaryDischarge INT,  
 @VeteranBranchOfService INT,  
 @ClaimantStatus INT,  
 @ClientStatus INT,  
 @Services NVARCHAR(MAX),  
 @ResultsStatus INT,  
 @SearchJobTitle BIT,  
 @SearchJobDescription BIT,  
 @SearchEmployerName BIT,  
 @SearchTerms NVARCHAR(MAX),  
 @SearchType INT,  
 @GroupBy VARCHAR(20),  
 @PageSize INT  
AS  
  
  
BEGIN  
  
CREATE TABLE #JobSeeker 
(
	Id BIGINT PRIMARY KEY, 
	ClientStatus INT NULL, 
	EducationLevel INT NULL,
	JobOrderId INT NULL,
	County NVARCHAR(202) NULL, -- JobOrder SIZES
	WibLocation NVARCHAR(100) NULL, -- JobOrder SIZES
	Office NVARCHAR(100) NULL, -- JobOrder SIZES
	FilterSkills INT NULL,
	FilterApplications INT NULL, 
	FilterSalary INT NULL, 
	FilterReceivedServices INT NULL, 
	FilterCertificationLicense INT NULL,
	FilterCounties INT NULL, 
	FilterOffices INT NULL, 
	FilterWibs INT NULL, 
	FilterOnetOccupations INT NULL, 
	FilterOnetDetailOccupations INT NULL,
	FilterZips INT NULL
)
DECLARE @CertificationLicenseChosen TABLE (	Id int, Value nvarchar(100))
DECLARE @SkillsChosen TABLE ( Id int, Value nvarchar(100))
DECLARE @ServicesChosen TABLE (	Id int, Value nvarchar(100))
DECLARE @KeywordSearchTerms TABLE (	Id int, Value nvarchar(100), Processed bit default 0)
DECLARE @ONetOccupationGroups TABLE ( Id int, Value nvarchar(100))
DECLARE @ONetDetailedOccupations TABLE ( Id int, Value nvarchar(100))
DECLARE @ONetIndustryGroup TABLE ( Id int, Value nvarchar(100))
DECLARE @ONetDetailedIndustry TABLE ( Id int, Value nvarchar(100))
DECLARE @Counties TABLE ( Id int, Value nvarchar(100))
DECLARE @Offices TABLE ( Id int, Value nvarchar(100))
DECLARE @WIBs TABLE ( Id int, Value nvarchar(100))
DECLARE @EducationLevels TABLE (Id int)

CREATE TABLE #Zips ( ZipCode varchar(10))
CREATE TABLE #KeyWordJobSeekers (Id	bigint )

INSERT INTO @CertificationLicenseChosen (Id, Value) SELECT * FROM dbo.Split(NULLIF(@CertAndLicenses,''), '|');
INSERT INTO @SkillsChosen ( Id, Value) SELECT * FROM dbo.Split(NULLIF(@Skills,''), '|');
INSERT INTO @ServicesChosen ( Id, Value ) SELECT * FROM dbo.Split(NULLIF(@Services,''), '|');
INSERT INTO @KeywordSearchTerms( Id, Value) SELECT * FROM dbo.Split(NULLIF(@SearchTerms,''), '|');	
INSERT INTO @ONetDetailedIndustry ( Id, Value ) SELECT * FROM dbo.Split(NULLIF(@DetailedIndustry, ''), '|');	
INSERT INTO @ONetDetailedOccupations ( Id, Value ) SELECT * FROM dbo.Split(NULLIF(@DetailedOccupation, ''), '|');	
INSERT INTO @ONetIndustryGroup ( Id, Value) SELECT * FROM dbo.Split(NULLIF(@IndustryGroup, ''), '|');	
INSERT INTO @ONetOccupationGroups( Id, Value) SELECT * FROM dbo.Split(NULLIF(@OccupationGroup, ''), '|');		
INSERT INTO @Counties ( Id, Value ) SELECT * FROM dbo.Split(NULLIF(@County, ''), '|');	
INSERT INTO @Offices ( Id, Value ) SELECT * FROM dbo.Split(NULLIF(@Office, ''), '|');	
INSERT INTO @WIBs ( Id, Value ) SELECT * FROM dbo.Split(NULLIF(@WIB, ''), '|');

DECLARE @TrimmedZip NVARCHAR(20)
SET @TrimmedZip = RTRIM(LTRIM(@ZIP))
IF (@TrimmedZip <> '')
	BEGIN
		INSERT INTO #Zips
		SELECT z2.ZipCode
		FROM dbo.ZipCodes z1
		CROSS JOIN dbo.ZipCodes z2
		WHERE z1.zipcode = @TrimmedZip
		AND dbo.CalculateDistance(z1.Longitude, z1.Latitude, z2.Longitude, z2.Latitude) < @Radius
	END
ELSE
	BEGIN
		INSERT INTO #Zips VALUES (NULL) 
	END

-- This is needed be cause job seekers can have more than one education level ticked
-- If multiple are ticked, and the report is grouped by education, the job seeker will be counted multiple times
IF @GroupBy = 'education'  
BEGIN  
 INSERT INTO @EducationLevels (Id)  
 SELECT 1 WHERE 1 & @EducationLevel <> 0 or @EducationLevel = 0
 UNION  
 SELECT 2 WHERE 2 & @EducationLevel <> 0 or @EducationLevel = 0
 UNION  
 SELECT 4 WHERE 4 & @EducationLevel <> 0 or @EducationLevel = 0
 UNION  
 SELECT 8 WHERE 8 & @EducationLevel <> 0 or @EducationLevel = 0
 UNION  
 SELECT 16 WHERE 16 & @EducationLevel <> 0 or @EducationLevel = 0
 UNION  
 SELECT 0 WHERE @EducationLevel = 0
END  
ELSE  
BEGIN  
 INSERT INTO @EducationLevels (Id)  
 SELECT 31  
END
  	
DECLARE @KeyWordSearchSql nvarchar(max)
DECLARE @CurrentKeyWord nvarchar(max)
DECLARE @CurrentKeyWordId nvarchar(max)
DECLARE @ProcessKeyWords BIT
DECLARE @SearchOperator nvarchar(4)
SET @ProcessKeyWords = 0
SET @KeyWordSearchSql= ''

IF @SearchType = 1
	SET @SearchOperator = 'and'
ELSE IF @SearchType = 2
	SET @SearchOperator = 'or'	  
	
IF (@SearchEmployerName = 1 OR @SearchJobDescription = 1 OR @SearchJobTitle = 1) AND @SearchType <> 0 AND EXISTS (SELECT 1 FROM @KeywordSearchTerms WHERE Value IS NOT NULL)
	BEGIN
		SET @ProcessKeyWords = 1
		WHILE EXISTS (SELECT 1 FROM @KeywordSearchTerms WHERE Processed = 0)
			BEGIN
				SELECT TOP 1 @CurrentKeyWordId = Id, @CurrentKeyWord = Value FROM @KeywordSearchTerms WHERE Processed = 0
				
				DECLARE @KeyWordSearchSqlLen INT
				DECLARE @CleanCurrentKeyWord NVARCHAR(MAX)
				SET @KeyWordSearchSqlLen = LEN(@KeyWordSearchSql)
				SET @CleanCurrentKeyWord = REPLACE(REPLACE(REPLACE(@CurrentKeyWord, '''',''''''), '--', ''),';','')
				
				IF @SearchEmployerName = 1
				BEGIN
					IF @KeyWordSearchSqlLen > 0
						SET @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '	
				
					SET @KeyWordSearchSql = @KeyWordSearchSql + 'jsv.EmployerName LIKE ''' + @CleanCurrentKeyWord + '%'''
				END
				
				IF @SearchJobTitle = 1
				BEGIN	
					IF @KeyWordSearchSqlLen > 0
						SET @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '
				
					SET @KeyWordSearchSql = @KeyWordSearchSql + 'jsv.JobTitle LIKE ''' + @CleanCurrentKeyWord + '%'''
				END
				
				IF @SearchJobDescription = 1
				BEGIN
					IF @KeyWordSearchSqlLen > 0
						SET @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '
				
					SET @KeyWordSearchSql = @KeyWordSearchSql + 'jsv.JobDescription LIKE ''' + @CleanCurrentKeyWord + '%'''
				END
							
				UPDATE @KeywordSearchTerms SET Processed = 1 WHERE @CurrentKeyWordId = Id AND @CurrentKeyWord = Value
			END
		SET @KeyWordSearchSql = 'INSERT INTO #KeyWordJobSeekers (id) SELECT DISTINCT id FROM JobSeekerReportView jsv WHERE ' + @KeyWordSearchSql
		-- PRINT @KeyWordSearchSql
		EXECUTE sp_executesql @KeyWordSearchSql
	END
ELSE
	BEGIN
		INSERT INTO #KeyWordJobSeekers (Id) VALUES (NULL)
	END
	
DECLARE @FilterSkill INT
SELECT TOP 1 @FilterSkill = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @SkillsChosen

DECLARE @FilterApplications INT
SELECT TOP 1 @FilterApplications = CASE @ResultsStatus WHEN 0 THEN 1 ELSE 0 END 

DECLARE @FilterSalaryFrequency INT
SELECT TOP 1 @FilterSalaryFrequency = CASE ISNULL(@SalaryFrequency, 0) WHEN 0  THEN 1 ELSE 0 END

DECLARE @FilterCounties INT
SELECT TOP 1 @FilterCounties = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @Counties

DECLARE @FilterOffices INT
SELECT TOP 1 @FilterOffices = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @Offices

DECLARE @FilterWibs INT
SELECT TOP 1 @FilterWibs = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @WIBs

DECLARE @FilterOnetOccupations INT
SELECT TOP 1 @FilterOnetOccupations = CASE ISNULL(Value, '') WHEN '' THEN 1 ELSE 0 END FROM @ONetOccupationGroups

DECLARE @FilterOnetDetailOccupations INT
SELECT TOP 1 @FilterOnetDetailOccupations = CASE ISNULL(Value, '') WHEN '' THEN 1 ELSE 0 END FROM @ONetDetailedOccupations

DECLARE @FilterServices INT
SELECT TOP 1 @FilterServices = CASE ISNULL(Value, '') WHEN '' THEN 1 ELSE 0 END FROM @ServicesChosen

DECLARE @FilterCertificates INT
SELECT TOP 1 @FilterCertificates = CASE ISNULL(Value, '') WHEN '' THEN 1 ELSE 0 END FROM @CertificationLicenseChosen

DECLARE @FilterZips INT
SELECT TOP 1 @FilterZips = CASE ISNULL(ZipCode, '') WHEN ''  THEN 1 ELSE 0 END FROM #Zips

INSERT INTO #JobSeeker 
SELECT 
	jsi.Id,
	jsi.ClientStatus,
	jsi.EducationLevel,
	NULL, --JobOrderId
	NULL, --County
	NULL, --WibLocation
	NULL, --Office
	@FilterSkill, 
	@FilterApplications, 
	@FilterSalaryFrequency, 
	@FilterServices, 
	@FilterCertificates, 
	@FilterCounties, 
	@FilterOffices, 
	@FilterWibs, 
	@FilterOnetOccupations, 
	@FilterOnetDetailOccupations,
	@FilterZips
FROM dbo.JobSeeker jsi
INNER JOIN #KeyWordJobSeekers kstEmp on jsi.Id = kstEmp.Id or @ProcessKeyWords = 0
where
	(jsi.EducationLevel & @EducationLevel <> 0 OR @EducationLevel = 0)
AND (jsi.VeteranDefinition & @VeteranDefinition <>0 OR @VeteranDefinition = 0)
AND (jsi.VeteranType & @VeteranType <> 0 OR @VeteranType = 0)
AND (jsi.VeteranEmploymentStatus & @VeteranEmploymentStatus <> 0 OR @VeteranEmploymentStatus = 0)
AND (jsi.VeteranTransactionType & @VeteranTransactionType <> 0 OR @VeteranTransactionType = 0)
AND (jsi.VeteranMilitaryDischarge & @VeteranMilitaryDischarge <> 0 OR @VeteranMilitaryDischarge = 0)
AND (jsi.VeteranBranchOfService & @VeteranBranchOfService <> 0 OR @VeteranBranchOfService = 0)
AND (jsi.AccountCreationDate BETWEEN @DateFrom AND @DateTo)

UPDATE #JobSeeker SET
JobOrderId = jo.Id,
County = jo.County,
WibLocation = jo.WibLocation, 
Office = jo.Office
FROM #JobSeeker js 
INNER JOIN dbo.JobSeekerApplication jsa ON js.Id = jsa.JobSeekerId
INNER JOIN dbo.JobOrder jo ON jsa.JobOrderId = jo.Id

IF (@FilterSkill = 0 )
	BEGIN
		UPDATE #JobSeeker SET
		FilterSkills = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerSkill jss ON js.Id = jss.JobSeekerId 
		INNER JOIN @SkillsChosen sjss on jss.Skill LIKE '%' + sjss.Value + '%' --OR sjss.Value IS NULL
	END
	
IF(@FilterApplications = 0)
	BEGIN
		UPDATE #JobSeeker SET
		FilterApplications = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerApplication jsa ON js.Id = jsa.JobSeekerId
		INNER JOIN dbo.JobOrder j ON jsa.JobOrderId = j.Id
		WHERE (jsa.ApplicationResult & @ResultsStatus <> 0)-- or @ResultsStatus = 0)
	END
	
IF(@FilterSalaryFrequency = 0)
	BEGIN
		UPDATE #JobSeeker SET
		FilterSalary = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerApplication jsa ON js.Id = jsa.JobSeekerId
		INNER JOIN dbo.JobOrder j ON jsa.JobOrderId = j.Id
		WHERE FilterApplications = 1 
		AND (ISNULL(@SalaryFrequency,0) = 0
			OR
				(j.SalaryFrequency = @SalaryFrequency
				AND	j.MinSalary < @MaxSalary
				AND j.MaxSalary > @MinSalary
				)
			)
	END
	
IF(@FilterCounties = 0)
	BEGIN 
		UPDATE #JobSeeker SET
		FilterCounties = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerApplication jsa ON js.Id = jsa.JobSeekerId
		INNER JOIN dbo.JobOrder j ON jsa.JobOrderId = j.Id
		INNER JOIN @Counties c on j.County LIKE '%' + c.Value + '%'-- OR c.Value IS NULL
		WHERE FilterApplications = 1
	END

IF (@FilterOffices = 0)
	BEGIN 
		UPDATE #JobSeeker SET
		FilterOffices = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerApplication jsa ON js.Id = jsa.JobSeekerId
		INNER JOIN dbo.JobOrder j ON jsa.JobOrderId = j.Id
		INNER JOIN @Offices o on j.Office LIKE '%' + o.Value + '%'-- OR o.Value IS NULL
		WHERE FilterApplications = 1
	END
	
IF (@FilterWibs = 0)
	BEGIN 
		UPDATE #JobSeeker SET
		FilterWibs = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerApplication jsa ON js.Id = jsa.JobSeekerId
		INNER JOIN dbo.JobOrder j ON jsa.JobOrderId = j.Id
		INNER JOIN @WIBs w on j.WIBLocation LIKE '%' + w.Value + '%' --OR w.Value IS NULL
		WHERE FilterApplications = 1
	END

IF (@FilterOnetOccupations = 0)
	BEGIN 
		UPDATE #JobSeeker SET
		FilterOnetOccupations = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerApplication jsa ON js.Id = jsa.JobSeekerId
		INNER JOIN dbo.JobOrder j ON jsa.JobOrderId = j.Id
		INNER JOIN @ONetOccupationGroups oog on j.OnetOccupationGroup LIKE '%' + oog.Value + '%' --OR oog.Value IS NULL
		WHERE FilterApplications = 1
	END

IF (@FilterOnetDetailOccupations = 0 )
	BEGIN 
		UPDATE #JobSeeker SET
		FilterOnetDetailOccupations = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerApplication jsa ON js.Id = jsa.JobSeekerId
		INNER JOIN dbo.JobOrder j ON jsa.JobOrderId = j.Id
		INNER JOIN @ONetDetailedOccupations odo on j.OnetDetailedOccupation LIKE '%' + odo.Value + '%' --OR odo.Value IS NULL
		WHERE FilterApplications = 1
	END

IF (@FilterServices = 0)
	BEGIN 
		UPDATE #JobSeeker SET
		FilterReceivedServices = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerServicesReceived jssr ON js.Id = jssr.JobSeekerId
		INNER JOIN @ServicesChosen sjsr on jssr.ServiceReceived LIKE '%' + sjsr.Value + '%' --OR sjsr.Value IS NULL
	END

IF (@FilterCertificates = 0)
	BEGIN 
		UPDATE #JobSeeker SET
		FilterCertificationLicense = 1
		FROM #JobSeeker js 
		INNER JOIN dbo.JobSeekerCertificationLicense jscl ON js.Id = jscl.JobSeekerId
		INNER JOIN @CertificationLicenseChosen sjsr on jscl.CertificationLicense LIKE '%' + sjsr.Value + '%' --OR sjsr.Value IS NULL
	END
	
IF (@FilterZips = 0)
	BEGIN 
		UPDATE #JobSeeker SET
		FilterZips = 1
		FROM #JobSeeker js
		INNER JOIN JobSeeker jsi ON js.Id = jsi.Id
		INNER JOIN #Zips z ON jsi.ZipCode = z.ZipCode
	END;

WITH JobSeekers AS
(  
 	SELECT  
		#JobSeeker.Id,  
		CASE @GroupBy
			WHEN 'education' THEN el.Id -- CAST(js.EducationLevel as NVARCHAR(10))  
			WHEN 'county' THEN County  
			WHEN 'wib' THEN WIBLocation  
			WHEN 'office' THEN Office  
			WHEN 'specialstatus' THEN ClientStatus  
			else CAST(el.Id as NVARCHAR(10))     
		end as aggregator  
	FROM #JobSeeker
	INNER JOIN @EducationLevels el  
		ON EducationLevel & el.Id <> 0
	WHERE 
		FilterSkills = 1
		AND	FilterReceivedServices = 1 
		AND	FilterApplications = 1 
		AND	FilterSalary = 1 
		AND	FilterReceivedServices = 1 
		AND	FilterCertificationLicense = 1
		AND	FilterCounties = 1 
		AND	FilterOffices = 1 
		AND	FilterWibs = 1 
		AND	FilterOnetOccupations = 1 
		AND	FilterOnetDetailOccupations = 1 
		AND FilterZips = 1 
)  
  
select TOP(@PageSize)  
 js.aggregator,  
 count(1) as itemcount  
from  
 jobseekers js  
group by  
 js.aggregator  
order by  
 case when LOWER(@GroupBy) = 'education'  
 then CAST(js.aggregator AS int) END asc,  
 case when LOWER(@GroupBy) <> 'education'   
 then count(1) end desc  
 
END  





GO

