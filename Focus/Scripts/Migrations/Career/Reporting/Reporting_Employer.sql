USE [FocusTestRepReporting]
GO

/****** Object:  StoredProcedure [dbo].[Reporting_Employer]    Script Date: 02/25/2013 14:49:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Reporting_Employer]
	@DateFrom	DATETIME,
	@DateTo	DATETIME,
	@County	NVARCHAR(MAX),
	@Office NVARCHAR(MAX),
	@WIB NVARCHAR(MAX),
	@Radius INT,
	@ZIP NVARCHAR(20),
	@OccupationGroup NVARCHAR(MAX),
	@DetailedOccupation NVARCHAR(MAX),
	@SalaryFrequency INT,
	@MinSalary	DECIMAL,
	@MaxSalary	DECIMAL,
	@JobStatus	INT,
	@EducationLevel	INT,
	@SearchTerms NVARCHAR(MAX),
	@SearchType INT,
	@KeyWordsInJobTitle BIT,
	@TargetedSectors NVARCHAR(MAX), -- To do
	@WOTCInterests NVARCHAR(MAX),
	@FEIN NVARCHAR(10),
	@EmployerName NVARCHAR(50),
	@JobTitle NVARCHAR(50),
	@Flags INT,
	@SearchAccountCreationDate BIT,
	@SearchAssistanceRequested BIT,
	@Compliance INT,
	@SuppressJobOrder BIT,
	@SelfService BIT,
	@StaffAssisted BIT,
	@Spidered BIT,
	@PageNumber	INT,
	@PageSize	INT,
	@SortOrder	VARCHAR(20)

AS
CREATE TABLE #JobCounter
(
	Id INT, 
	EmployerName NVARCHAR(50),
	ContactFullName NVARCHAR(100), 
	City NVARCHAR(100), 
	SpecialConditions NVARCHAR(100), 
	AccountCreationDate DATETIME,
	JobId INT,
	JobCount INT DEFAULT(0),
	FilterCounties INT, 
	FilterOffices INT,
	FilterZips INT,
	FilterWOTCInterest INT,
	FilterONetOccupationGroups INT,
	FilterONetDetailedOccupations INT
)

DECLARE @Counties TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @Offices TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @WIBs TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @ONetOccupationGroups TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @ONetDetailedOccupations TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @TargetedSector TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @WOTCInterest TABLE ( Id INT, Value NVARCHAR(100))
DECLARE @KeywordSearchTerms TABLE (	Id INT, Value NVARCHAR(100), Processed BIT DEFAULT 0)
CREATE TABLE #Zips ( ZipCode VARCHAR(10))

CREATE TABLE #KeyWordEmployer( Id bigint )

INSERT INTO @KeywordSearchTerms ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@SearchTerms,''), '|');
INSERT INTO @ONetDetailedOccupations ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@DetailedOccupation, ''), '|');
INSERT INTO @ONetOccupationGroups (	Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@OccupationGroup, ''), '|');
INSERT INTO @Counties (	Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@County, ''), '|');
INSERT INTO @Offices ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@Office, ''), '|');
INSERT INTO @WIBs (	Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@WIB, ''), '|');
INSERT INTO @TargetedSector ( Id, Value ) SELECT Id, Value FROM	dbo.Split(NULLIF(@TargetedSectors, ''), '|');
INSERT INTO @WOTCInterest ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@WOTCInterests, ''), '|');

DECLARE @TrimmedZip NVARCHAR(20)
SET @TrimmedZip = RTRIM(LTRIM(@ZIP))
IF (@TrimmedZip <> '')
	BEGIN
		INSERT INTO #Zips
		SELECT z2.ZipCode
		FROM ZipCodes z1
		CROSS JOIN ZipCodes z2
		WHERE z1.zipcode = @TrimmedZip
		AND dbo.CalculateDistance(z1.Longitude, z1.Latitude, z2.Longitude, z2.Latitude) < @Radius
	END
ELSE
	BEGIN
		INSERT INTO #Zips VALUES (NULL) 
	END
	
DECLARE @KeyWordSearchSql NVARCHAR(max)
DECLARE @CurrentKeyWord NVARCHAR(max)
DECLARE @CurrentKeyWordId NVARCHAR(max)
DECLARE @ProcessKeyWords BIT
DECLARE @SearchOperator NVARCHAR(4)
SET @ProcessKeyWords = 0
SET @KeyWordSearchSql= ''

IF @SearchType = 1
	SET @SearchOperator = 'and'
ELSE IF @SearchType = 2 or @KeyWordsInJobTitle = 1
	SET @SearchOperator = 'or'

IF EXISTS (SELECT 1 FROM @KeywordSearchTerms WHERE Value IS NOT NULL)
BEGIN
	SET @ProcessKeyWords = 1
	WHILE EXISTS (SELECT 1 FROM @KeywordSearchTerms where Processed = 0)
	BEGIN
		SELECT TOP 1 @CurrentKeyWordId = Id, @CurrentKeyWord = Value FROM @KeywordSearchTerms where Processed = 0
		
		IF LEN(@KeyWordSearchSql) > 0
			SET @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '
		ELSE
			SET @KeyWordSearchSql = ' '	
		
		IF @KeyWordsInJobTitle = 1 -- We only need to worry about job title and not the others
			set @KeyWordSearchSql = @KeyWordSearchSql + 'jo.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
		ELSE
		BEGIN
			SET @KeyWordSearchSql = @KeyWordSearchSql + '(jo.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
			SET @KeyWordSearchSql = @KeyWordSearchSql + ' or e.EmployerName like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
			SET @KeyWordSearchSql = @KeyWordSearchSql + ' or jo.JobDescription like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'')'
		END
						
		UPDATE @KeywordSearchTerms SET Processed = 1 WHERE @CurrentKeyWordId = Id AND @CurrentKeyWord = Value
	END
	SET @KeyWordSearchSql = 'insert into #KeyWordEmployer (id) select distinct e.id from dbo.Employer e left join dbo.JobOrder jo on e.Id = jo.EmployerId where ' + @KeyWordSearchSql
	EXECUTE sp_executesql @KeyWordSearchSql;
END
ELSE
BEGIN
	INSERT INTO #KeyWordEmployer (Id) Values (NULL)
END;

DECLARE @FilterCounties INT
SELECT TOP 1 @FilterCounties = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @Counties

DECLARE @FilterOffices INT
SELECT TOP 1 @FilterOffices = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @Offices

DECLARE @FilterZips INT
SELECT TOP 1 @FilterZips = CASE ISNULL(ZipCode, '') WHEN ''  THEN 1 ELSE 0 END FROM #Zips

DECLARE @FilterWOTCInterest INT
SELECT TOP 1 @FilterWOTCInterest = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @WOTCInterest

DECLARE @FilterONetOccupationGroups INT
SELECT TOP 1 @FilterONetOccupationGroups = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @ONetOccupationGroups

DECLARE @FilterONetDetailedOccupations INT
SELECT TOP 1 @FilterONetDetailedOccupations = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @ONetDetailedOccupations

INSERT INTO #JobCounter
(
	Id, 
	EmployerName,
	ContactFullName, 
	City, 
	SpecialConditions, 
	AccountCreationDate,
	JobId,
	JobCount,
	FilterCounties, 
	FilterOffices,
	FilterZips,
	FilterWOTCInterest,
	FilterONetOccupationGroups,
	FilterONetDetailedOccupations
)
SELECT DISTINCT
	e.Id, 
	e.EmployerName,
	e.ContactFullName, 
	e.City, 
	substring(e.SpecialConditions,1,100) as SpecialConditions, 
	e.AccountCreationDate,
	jo.Id,
	0,
	@FilterCounties,
	@FilterOffices,
	@FilterZips,
	@FilterWOTCInterest,
	@FilterONetOccupationGroups,
	@FilterONetDetailedOccupations
FROM 
	dbo.Employer e
INNER JOIN #KeyWordEmployer kst
	ON kst.Id = e.Id OR @ProcessKeyWords = 0
LEFT JOIN dbo.JobOrder jo
	ON e.Id = jo.EmployerId
WHERE
	(ISNULL(@SalaryFrequency,0) = 0
	OR
		(jo.SalaryFrequency = @SalaryFrequency
		AND	jo.MinSalary < @MaxSalary
		AND jo.MaxSalary > @MinSalary
		)
	)
	AND (@JobStatus = 0 OR jo.JobStatus & @JobStatus <> 0)
	AND (@EducationLevel = 0 OR jo.EducationLevel & @EducationLevel <> 0)
	AND ISNULL(e.FederalEmployerIdentificationNumber, '') LIKE '%' + ISNULL(@FEIN, '') + '%'
	AND ISNULL(jo.EmployerName, '') LIKE '%' + ISNULL(@EmployerName,'') + '%'
	AND ISNULL(jo.JobTitle, '') LIKE '%' + ISNULL(@JobTitle,'') + '%'
	AND (@Flags = 0 OR jo.JobFlag & @Flags <> 0)
	AND ((e.RequestedScreeningAssistance = 1 and @SearchAssistanceRequested = 1) or @SearchAssistanceRequested = 0)
	AND (@Compliance = 0 OR jo.Compliance & @Compliance <> 0)
	AND ((jo.SuppressJobOrder = 1 and @SuppressJobOrder = 1) or @SuppressJobOrder = 0)
	AND ((jo.IsSelfService = 1 and @SelfService = 1) or @SelfService = 0)
	AND ((jo.IsStaffAssisted = 1 and @StaffAssisted = 1) or @StaffAssisted = 0)
	AND ((jo.Spidered = 1 and @Spidered = 1) or @Spidered = 0)
	AND ((@SearchAccountCreationDate = 1 and e.AccountCreationDate between @DateFrom and @DateTo) or @SearchAccountCreationDate = 0)

IF(@FilterCounties = 0)
BEGIN 
	UPDATE #JobCounter SET
	FilterCounties = 1
	FROM #JobCounter jc
	INNER JOIN Employer e ON jc.Id = e.id
	INNER JOIN @Counties c on e.County LIKE '%' + c.Value + '%'
END

IF (@FilterOffices = 0)
BEGIN 
	UPDATE #JobCounter SET
	FilterOffices = 1
	FROM #JobCounter jc
	INNER JOIN Employer e ON jc.Id = e.id
	INNER JOIN @Offices o on e.Office LIKE '%' + o.Value + '%'-- OR o.Value IS NULL
END

IF (@FilterZips = 0)
BEGIN 
	UPDATE #JobCounter SET
	FilterZips = 1
	FROM #JobCounter jc
	INNER JOIN Employer e ON jc.Id = e.id
	INNER JOIN #Zips z ON e.ZipCode = z.ZipCode
END

IF (@FilterWOTCInterest = 0)
BEGIN 
	UPDATE #JobCounter SET
	FilterWOTCInterest = 1
	FROM #JobCounter jc
	INNER JOIN dbo.JobOrder jo ON jc.jobId = jo.Id
	INNER JOIN @WOTCInterest wi	ON jo.WotcInterest LIKE '%' + wi.Value + '%'
END

IF (@FilterONetOccupationGroups = 0)
BEGIN 
	UPDATE #JobCounter SET
	FilterONetOccupationGroups = 1
	FROM #JobCounter jc
	INNER JOIN dbo.JobOrder jo ON jc.jobId = jo.Id
	INNER JOIN @ONetOccupationGroups oog on jo.OnetOccupationGroup LIKE '%' + oog.Value + '%'
END

IF (@FilterONetDetailedOccupations = 0)
BEGIN 
	UPDATE #JobCounter SET
	FilterONetDetailedOccupations = 1
	FROM #JobCounter jc
	INNER JOIN dbo.JobOrder jo ON jc.jobId = jo.Id
	INNER JOIN @ONetDetailedOccupations odo ON jo.OnetDetailedOccupation LIKE '%' + odo.Value + '%'
END;

WITH joborders AS
(
	SELECT
		CASE lower(@SortOrder)
			WHEN 'employer_asc' then ROW_NUMBER()OVER (ORDER BY jc.EmployerName ASC, jc.Id ASC)
			WHEN 'contact_asc' then ROW_NUMBER()OVER (ORDER BY jc.ContactFullName ASC, jc.Id ASC)
			WHEN 'city_asc' then ROW_NUMBER()OVER (ORDER BY jc.City ASC, jc.Id ASC)
			WHEN 'listings_asc' then ROW_NUMBER()OVER (ORDER BY COUNT(jc.JobId) ASC, jc.Id ASC)
			WHEN 'specialcond_asc' then ROW_NUMBER()OVER (ORDER BY jc.SpecialConditions ASC, jc.Id ASC)
			WHEN 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY jc.AccountCreationDate ASC, jc.Id ASC)
			WHEN 'accountcreation_desc' then ROW_NUMBER()OVER (ORDER BY jc.AccountCreationDate DESC, jc.Id ASC)
			WHEN 'specialcond_desc' then ROW_NUMBER()OVER (ORDER BY jc.SpecialConditions DESC, jc.Id ASC)
			WHEN 'listings_desc' then ROW_NUMBER()OVER (ORDER BY COUNT(jc.JobId) DESC, jc.Id ASC)
			WHEN 'city_desc' then ROW_NUMBER()OVER (ORDER BY jc.City DESC, jc.Id ASC)
			WHEN 'contact_desc' then ROW_NUMBER()OVER (ORDER BY jc.ContactFullName DESC, jc.Id ASC)
			WHEN 'employer_desc' then ROW_NUMBER()OVER (ORDER BY jc.EmployerName DESC, jc.Id ASC)
			ELSE ROW_NUMBER()OVER (ORDER BY jc.EmployerName ASC, jc.Id ASC)
		END as pagingId,
		CASE @SortOrder
			WHEN 'employer_asc' then ROW_NUMBER()OVER (ORDER BY jc.EmployerName DESC, jc.Id DESC)
			WHEN 'contact_asc' then ROW_NUMBER()OVER (ORDER BY jc.ContactFullName DESC, jc.Id DESC)
			WHEN 'city_asc' then ROW_NUMBER()OVER (ORDER BY jc.City DESC, jc.Id DESC)
			WHEN 'listings_asc' then ROW_NUMBER()OVER (ORDER BY COUNT(jc.JobId) DESC, jc.Id DESC)
			WHEN 'specialcond_asc' then ROW_NUMBER()OVER (ORDER BY jc.SpecialConditions DESC, jc.Id DESC)
			WHEN 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY jc.AccountCreationDate DESC, jc.Id DESC)
			WHEN 'accountcreation_desc' then ROW_NUMBER()OVER (ORDER BY jc.AccountCreationDate ASC, jc.Id DESC)
			WHEN 'specialcond_desc' then ROW_NUMBER()OVER (ORDER BY jc.SpecialConditions ASC, jc.Id DESC)
			WHEN 'listings_desc' then ROW_NUMBER()OVER (ORDER BY COUNT(jc.JobId) ASC, jc.Id DESC)
			WHEN 'city_desc' then ROW_NUMBER()OVER (ORDER BY jc.City ASC, jc.Id DESC)
			WHEN 'contact_desc' then ROW_NUMBER()OVER (ORDER BY jc.ContactFullName ASC, jc.Id DESC)
			WHEN 'employer_desc' then ROW_NUMBER()OVER (ORDER BY jc.EmployerName ASC, jc.Id DESC)
			ELSE ROW_NUMBER()OVER (ORDER BY jc.EmployerName DESC, jc.Id DESC)
		END as pagingRevId,
		jc.Id,
		jc.EmployerName, 
		jc.ContactFullName, 
		jc.City, 
		jc.SpecialConditions, 
		jc.AccountCreationDate,
		COUNT(jc.JobId) AS Listings
	FROM
		#JobCounter jc
	WHERE FilterCounties = 1 
		AND	FilterOffices = 1 
		AND FilterZips = 1
		AND FilterWOTCInterest = 1
		AND FilterONetOccupationGroups = 1
		AND FilterONetDetailedOccupations = 1
	GROUP BY
		jc.Id,
		jc.EmployerName, 
		jc.ContactFullName, 
		jc.City, 
		jc.SpecialConditions, 
		jc.AccountCreationDate
)
SELECT
	jo.Id,
	jo.pagingId,
	jo.pagingRevId,
	jo.EmployerName,
	jo.ContactFullName, 
	jo.City, 
	jo.Listings,
	jo.SpecialConditions,
	jo.AccountCreationDate
FROM
	joborders jo
WHERE
	pagingId BETWEEN  ((@PageNumber - 1) * @PageSize) + 1 and (@PageNumber * @PageSize)
ORDER BY
	pagingId ASC








GO

