/****** Object:  Index [IX_Resume_ModifiedDate_JobSeekerID]    Script Date: 01/30/2013 08:24:32 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[resume]') AND name = N'IX_Resume_ModifiedDate_JobSeekerID')
DROP INDEX [IX_Resume_ModifiedDate_JobSeekerID] ON [dbo].[resume] WITH ( ONLINE = OFF )
GO

USE [FocusCareer01]
GO

/****** Object:  Index [IX_Resume_ModifiedDate_JobSeekerID]    Script Date: 01/30/2013 08:24:32 ******/
CREATE NONCLUSTERED INDEX [IX_Resume_ModifiedDate_JobSeekerID] ON [dbo].[resume] 
(
	[modifieddate] ASC
)
INCLUDE ( [jobseekerid]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


/****** Object:  Index [IX_JobSeeker_CustomerCentreCode_JobSeekerId_RegisteredDate_CustomerRegistrationId]    Script Date: 01/30/2013 08:44:21 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[jobseeker]') AND name = N'IX_JobSeeker_CustomerCentreCode_JobSeekerId_RegisteredDate_CustomerRegistrationId')
DROP INDEX [IX_JobSeeker_CustomerCentreCode_JobSeekerId_RegisteredDate_CustomerRegistrationId] ON [dbo].[jobseeker] WITH ( ONLINE = OFF )
GO

USE [FocusCareer01]
GO

/****** Object:  Index [IX_JobSeeker_CustomerCentreCode_JobSeekerId_RegisteredDate_CustomerRegistrationId]    Script Date: 01/30/2013 08:44:21 ******/
CREATE NONCLUSTERED INDEX [IX_JobSeeker_CustomerCentreCode_JobSeekerId_RegisteredDate_CustomerRegistrationId] ON [dbo].[jobseeker] 
(
	[customercentrecode] ASC
)
INCLUDE ( [jobseekerid],
[registereddate],
[customerregistrationid]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

USE [FocusCareer01]
GO

/****** Object:  Index [IX_CustomerJobs_CustomerId_DateModified]    Script Date: 01/30/2013 11:07:23 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[customerjobs]') AND name = N'IX_CustomerJobs_CustomerId_DateModified')
DROP INDEX [IX_CustomerJobs_CustomerId_DateModified] ON [dbo].[customerjobs] WITH ( ONLINE = OFF )
GO

/****** Object:  Index [IX_CustomerJobs_CustomerId_DateModified]    Script Date: 01/30/2013 11:07:23 ******/
CREATE NONCLUSTERED INDEX [IX_CustomerJobs_CustomerId_DateModified] ON [dbo].[customerjobs] 
(
	[customerid] ASC,
	[datemodified] ASC
)
INCLUDE ( [focusjobid],
[customerjobid],
[jobtitle],
[employer],
[description],
[closedate],
[dateregistered],
[onetcode],
[city],
[state],
[zip],
[status],
[originid]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_JobSeekerSkill_JobSeekerId]
ON [dbo].[JobSeekerSkill] ([JobSeekerId])
INCLUDE ([Skill])

CREATE NONCLUSTERED INDEX [IX_JobSeeker_AccountCreationDate]
ON [dbo].[JobSeeker] ([AccountCreationDate])
INCLUDE ([Id],[FirstName],[LastName],[EmailAddress],[LastJobTitle],[LastEmployer],[VeteranDefinition],[VeteranType],[VeteranEmploymentStatus],[VeteranTransactionType],[VeteranMilitaryDischarge],[VeteranBranchOfService],[EducationLevel],[City],[ClientStatus],[WeeksSinceAccountCreated],[FullName],[ZipCode])
