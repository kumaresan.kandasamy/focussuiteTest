USE [FocusTestRepReporting]
GO

/****** Object:  StoredProcedure [dbo].[Reporting_Activity]    Script Date: 02/25/2013 14:49:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER PROCEDURE [dbo].[Reporting_Activity]
	@County	NVARCHAR(MAX),
	@Office NVARCHAR(MAX),
	@WIB NVARCHAR(MAX),
	@DateFrom	DATETIME,
	@DateTo	DATETIME,
	@NoOfLoginsDays INT,
	@JobOutcome INT,
	@PageNumber	INT,
	@PageSize	INT,
	@SortOrder	VARCHAR(20)
AS
BEGIN
SET NOCOUNT ON;

CREATE TABLE #Activity
(
	Id BIGINT,
	UserID BIGINT,
	fullname NVARCHAR(102),
	Location NVARCHAR(200),
	LastLogin DATETIME,
	DaysSinceLastLogin INT,
	loginCount INT,
	Referrals INT,
	ResumesCreated INT,
	ResumesEdited INT,
	JobSeekerAssisted INT,
	SeekersUnsubscribed INT,
	EmployerAssisted INT,
	JobCreated INT,
	JobEdited INT,
	County NVARCHAR(50),
	Office NVARCHAR(50),
	WIB NVARCHAR(50),
	FilterCounties INT,
	FilterOffices INT,
	FilterWIBs INT
)

DECLARE @Counties TABLE ( Id int, Value nvarchar(100))
DECLARE @Offices TABLE ( Id int, Value nvarchar(100))
DECLARE @WIBs TABLE ( Id int, Value nvarchar(100))

INSERT INTO @Counties (	Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@County, ''), '|');
INSERT INTO @Offices ( Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@Office, ''), '|');
INSERT INTO @WIBs (	Id, Value ) SELECT Id, Value FROM dbo.Split(NULLIF(@WIB, ''), '|');

DECLARE @FilterCounties INT
SELECT TOP 1 @FilterCounties = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @Counties

DECLARE @FilterOffices INT
SELECT TOP 1 @FilterOffices = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @Offices

DECLARE @FilterWIBs INT
SELECT TOP 1 @FilterWIBs = CASE ISNULL(Value, '') WHEN ''  THEN 1 ELSE 0 END FROM @WIBs

INSERT INTO #Activity
(
	Id,
	UserId,
	Fullname,
	Location,
	LastLogin,
	DaysSinceLastLogin,
	loginCount,
	Referrals,
	ResumesCreated,
	ResumesEdited,
	JobSeekerAssisted,
	SeekersUnsubscribed,
	EmployerAssisted,
	JobCreated,
	JobEdited,
	County,
	Office,
	WIB,
	FilterCounties,
	FilterOffices,
	FilterWIBs
)
SELECT
	a.Id,
	a.UserId,
	a.fullname,
	a.Location,
	a.LastLogin,
	DATEDIFF(DD,a.LastLogin, GETDATE()),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	a.County,
	a.Office,
	a.WIBLocation,
	@FilterCounties,
	@FilterOffices,
	@FilterWIBs
FROM dbo.Activity a

UPDATE #Activity SET
	Referrals = ISNULL(eaa.referrals,0),
	EmployerAssisted = ISNULL(eaa.employerassisted, 0),
	jobcreated = ISNULL(eaa.jobcreated,0),
	jobedited = ISNULL(eaa.jobedited,0)
FROM #Activity a
INNER JOIN
(
	SELECT
		ea.UserId,
		SUM(CASE WHEN Activity = 'ReferJob' then 1 else 0 end) as referrals,
		SUM(CASE WHEN ea.AssistAction = 1 then 1 else 0 end) as employerassisted,
		SUM(CASE WHEN Activity = 'PostJob' then 1 else 0 end) as jobcreated,
		SUM(CASE WHEN Activity = 'RefreshJob' then 1 else 0 end) as jobedited
	FROM dbo.EmployerActivity ea
	WHERE ea.ActivityDate BETWEEN @DateFrom AND @DateTo
	GROUP BY ea.UserId
) eaa
	ON a.UserId = eaa.UserId

UPDATE #Activity SET
	ResumesCreated = ISNULL(jsaa.ResumesCreated,0),
	ResumesEdited = ISNULL(jsaa.ResumesEdited,0),
	jobseekerassisted = ISNULL(jsaa.seekerassisted, 0),
	SeekersUnsubscribed = ISNULL(jsaa.SeekerUnsubscribed,0)
FROM #Activity a
INNER JOIN
(
	SELECT
		jsa.UserId,
		SUM(CASE WHEN jsa.Activity = 'ResumesCreated' THEN 1 ELSE 0 END) AS ResumesCreated,
		SUM(CASE WHEN jsa.Activity = 'ResumesEdited' THEN 1 ELSE 0 END) AS ResumesEdited,
		SUM(CASE WHEN jsa.AssistAction = 1 THEN 1 ELSE 0 END) AS seekerassisted,
		SUM(CASE WHEN jsa.Activity = 'UnsubscribedFromService' THEN 1 ELSE 0 END) AS SeekerUnsubscribed
	FROM dbo.JobSeekerActivity jsa
	WHERE jsa.ActivityDate BETWEEN @DateFrom AND @DateTo
	GROUP BY jsa.UserId
) jsaa ON a.UserId = jsaa.UserId

DECLARE @ActionDateTime DATETIME
SET @ActionDateTime = DATEADD(dd, (@NoOfLoginsDays * -1), GETDATE())

UPDATE #Activity SET
	logincount = ISNULL(sua.logincount,0)
FROM #Activity a
INNER JOIN
(
	SELECT
		asu.UserId,
		COUNT(1) as logincount
	FROM dbo.SystemUsage su
	INNER JOIN dbo.Activity asu ON asu.Id = su.UserId
	WHERE su.ActionDateTime BETWEEN GETDATE() AND @ActionDateTime
	GROUP BY asu.UserId
) sua ON a.UserId = sua.UserId
	
IF(@FilterCounties = 0)
BEGIN 
	UPDATE #Activity SET
	FilterCounties = 1
	FROM #Activity a
	INNER JOIN @Counties c on a.County LIKE '%' + c.Value + '%'
END

IF (@FilterOffices = 0)
BEGIN 
	UPDATE #Activity SET
	FilterOffices = 1
	FROM #Activity a
	INNER JOIN @Offices o on a.Office LIKE '%' + o.Value + '%'
END

IF (@FilterWIBs = 0)
BEGIN 
	UPDATE #Activity SET
	FilterWIBs = 1
	FROM #Activity a
	INNER JOIN @WIBs w on a.WIB LIKE '%' + w.Value + '%'
END;

WITH activitydata AS
(
	SELECT CASE LOWER(@SortOrder)
			WHEN 'staff_asc' THEN ROW_NUMBER() OVER (ORDER BY a.FullName ASC, a.Id ASC)
			WHEN 'location_asc' THEN ROW_NUMBER() OVER (ORDER BY a.Location ASC, a.Id ASC)
			WHEN 'lastlogin_asc' THEN ROW_NUMBER() OVER (ORDER BY a.LastLogin ASC, a.Id ASC)
			WHEN 'nooflogins_asc' THEN ROW_NUMBER() OVER (ORDER BY logincount ASC, a.Id ASC)
			WHEN 'dayssincelastlogin_asc' THEN ROW_NUMBER() OVER (ORDER BY DATEDIFF(DD,a.LastLogin, GETDATE()) ASC, a.Id ASC)
			WHEN 'referrals_asc' THEN ROW_NUMBER() OVER (ORDER BY referrals ASC, a.Id ASC)
			WHEN 'resumescreated_asc' THEN ROW_NUMBER() OVER (ORDER BY resumescreated ASC, a.Id ASC)
			WHEN 'resumesedited_asc' THEN ROW_NUMBER() OVER (ORDER BY resumesedited ASC, a.Id ASC)
			WHEN 'seekerassisted_asc' THEN ROW_NUMBER() OVER (ORDER BY jobseekerassisted ASC, a.Id ASC)
			WHEN 'seekerunsubscribed_asc' THEN ROW_NUMBER() OVER (ORDER BY seekersunsubscribed ASC, a.Id ASC)
			--WHEN 'employercreated_asc' THEN ROW_NUMBER() OVER (ORDER BY seekersunsubscribed ASC, a.Id ASC)
			WHEN 'employerassisted_asc' THEN ROW_NUMBER() OVER (ORDER BY employerassisted ASC, a.Id ASC)
			WHEN 'jobordercreated_asc' THEN ROW_NUMBER() OVER (ORDER BY jobcreated ASC, a.Id ASC)
			WHEN 'joborderedited_asc' THEN ROW_NUMBER() OVER (ORDER BY jobedited ASC, a.Id ASC)
			
			WHEN 'joborderedited_desc' THEN ROW_NUMBER() OVER (ORDER BY jobedited DESC, a.Id ASC)
			WHEN 'jobordercreated_desc' THEN ROW_NUMBER() OVER (ORDER BY jobcreated DESC, a.Id ASC)
			WHEN 'employerassisted_desc' THEN ROW_NUMBER() OVER (ORDER BY employerassisted DESC, a.Id ASC)
			--WHEN 'employercreated_desc' THEN ROW_NUMBER() OVER (ORDER BY seekersunsubscribed DESC, a.Id ASC)
			WHEN 'seekerunsubscribed_desc' THEN ROW_NUMBER() OVER (ORDER BY seekersunsubscribed DESC, a.Id ASC)
			WHEN 'seekerassisted_desc' THEN ROW_NUMBER() OVER (ORDER BY jobseekerassisted DESC, a.Id ASC)			
			WHEN 'resumesedited_desc' THEN ROW_NUMBER() OVER (ORDER BY resumesedited DESC, a.Id ASC)
			WHEN 'resumescreated_desc' THEN ROW_NUMBER() OVER (ORDER BY resumescreated DESC, a.Id ASC)
			WHEN 'referalls_desc' THEN ROW_NUMBER() OVER (ORDER BY referrals DESC, a.Id ASC)
			WHEN 'dayssincelastlogin_desc' THEN ROW_NUMBER() OVER (ORDER BY DATEDIFF(DD,a.LastLogin, GETDATE()) DESC, a.Id ASC)
			WHEN 'nooflogins_desc' THEN ROW_NUMBER() OVER (ORDER BY logincount DESC, a.Id ASC)
			WHEN 'lastlogin_desc' THEN ROW_NUMBER() OVER (ORDER BY a.LastLogin DESC, a.Id ASC)
			WHEN 'location_desc' THEN ROW_NUMBER() OVER (ORDER BY a.Location DESC, a.Id ASC)
			WHEN 'staff_desc' THEN ROW_NUMBER() OVER (ORDER BY a.FullName DESC, a.Id ASC)
			else ROW_NUMBER()OVER (ORDER BY a.FullName ASC, a.Id ASC)
		END AS pagingId,
		CASE LOWER(@SortOrder)
			WHEN 'staff_asc' THEN ROW_NUMBER() OVER (ORDER BY a.FullName DESC, a.Id DESC)
			WHEN 'location_asc' THEN ROW_NUMBER() OVER (ORDER BY a.Location DESC, a.Id DESC)
			WHEN 'lastlogin_asc' THEN ROW_NUMBER() OVER (ORDER BY a.LastLogin DESC, a.Id DESC)
			WHEN 'nooflogins_asc' THEN ROW_NUMBER() OVER (ORDER BY logincount DESC, a.Id DESC)
			WHEN 'dayssincelastlogin_asc' THEN ROW_NUMBER() OVER (ORDER BY DATEDIFF(DD,a.LastLogin, GETDATE()) DESC, a.Id DESC)
			WHEN 'referrals_asc' THEN ROW_NUMBER() OVER (ORDER BY referrals DESC, a.Id DESC)
			WHEN 'resumescreated_asc' THEN ROW_NUMBER() OVER (ORDER BY resumescreated DESC, a.Id DESC)
			WHEN 'resumesedited_asc' THEN ROW_NUMBER() OVER (ORDER BY resumesedited DESC, a.Id DESC)
			WHEN 'seekerassisted_asc' THEN ROW_NUMBER() OVER (ORDER BY jobseekerassisted DESC, a.Id DESC)
			WHEN 'seekerunsubscribed_asc' THEN ROW_NUMBER() OVER (ORDER BY seekersunsubscribed DESC, a.Id DESC)
			--WHEN 'employercreated_asc' THEN ROW_NUMBER() OVER (ORDER BY seekersunsubscribed DESC, a.Id DESC)
			WHEN 'employerassisted_asc' THEN ROW_NUMBER() OVER (ORDER BY employerassisted DESC, a.Id DESC)
			WHEN 'jobordercreated_asc' THEN ROW_NUMBER() OVER (ORDER BY jobcreated DESC, a.Id DESC)
			WHEN 'joborderedited_asc' THEN ROW_NUMBER() OVER (ORDER BY jobedited DESC, a.Id DESC)
			
			WHEN 'joborderedited_desc' THEN ROW_NUMBER() OVER (ORDER BY jobedited ASC, a.Id DESC)
			WHEN 'jobordercreated_desc' THEN ROW_NUMBER() OVER (ORDER BY jobcreated ASC, a.Id DESC)
			WHEN 'employerassisted_desc' THEN ROW_NUMBER() OVER (ORDER BY employerassisted ASC, a.Id DESC)
			--WHEN 'employercreated_desc' THEN ROW_NUMBER() OVER (ORDER BY seekersunsubscribed ASC, a.Id DESC)
			WHEN 'seekerunsubscribed_desc' THEN ROW_NUMBER() OVER (ORDER BY seekersunsubscribed ASC, a.Id DESC)
			WHEN 'seekerassisted_desc' THEN ROW_NUMBER() OVER (ORDER BY jobseekerassisted ASC, a.Id DESC)			
			WHEN 'resumesedited_desc' THEN ROW_NUMBER() OVER (ORDER BY resumesedited ASC, a.Id DESC)
			WHEN 'resumescreated_desc' THEN ROW_NUMBER() OVER (ORDER BY resumescreated ASC, a.Id DESC)
			WHEN 'referalls_desc' THEN ROW_NUMBER() OVER (ORDER BY referrals ASC, a.Id DESC)
			WHEN 'dayssincelastlogin_desc' THEN ROW_NUMBER() OVER (ORDER BY DATEDIFF(DD,a.LastLogin, GETDATE()) ASC, a.Id DESC)
			WHEN 'nooflogins_desc' THEN ROW_NUMBER() OVER (ORDER BY logincount ASC, a.Id DESC)
			WHEN 'lastlogin_desc' THEN ROW_NUMBER() OVER (ORDER BY a.LastLogin ASC, a.Id DESC)
			WHEN 'location_desc' THEN ROW_NUMBER() OVER (ORDER BY a.Location ASC, a.Id DESC)
			WHEN 'staff_desc' THEN ROW_NUMBER() OVER (ORDER BY a.FullName ASC, a.Id DESC)
			ELSE ROW_NUMBER()OVER (ORDER BY a.FullName DESC, a.Id DESC)
		END AS pagingRevId,
		a.Id,
		a.Fullname,
		a.Location,
		a.LastLogin,
		a.DaysSinceLastLogin,
		a.loginCount,
		a.Referrals,
		a.ResumesCreated,
		a.ResumesEdited,
		a.JobSeekerAssisted,
		a.SeekersUnsubscribed,
		a.EmployerAssisted,
		a.JobCreated,
		a.JobEdited
	FROM #Activity a
	WHERE a.FilterCounties = 1
	AND a.FilterOffices = 1
	AND a.FilterWIBs = 1
)
SELECT
	ad.pagingId,
	ad.pagingRevId,
	ad.Id,
	ad.FullName,
	ad.Location,
	ad.LastLogin,
	ad.logincount,
	ad.dayssincelastlogin,
	ad.referrals,
	ad.resumescreated,
	ad.resumesedited,
	ad.jobseekerassisted,
	ad.seekersunsubscribed,
	ad.employerassisted,
	ad.jobcreated,
	ad.jobedited
FROM
	activitydata ad
WHERE
	pagingId between  ((@PageNumber - 1) * @PageSize) + 1 and (@PageNumber * @PageSize)
ORDER BY
	pagingId ASC


END




GO

