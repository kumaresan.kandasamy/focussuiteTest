IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerNoteReminderRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerNoteReminderRequests]
GO

CREATE PROCEDURE [Migrations_JobSeekerNoteReminderRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.SavedNotes

IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
		
SELECT TOP (@BatchSize)
	CASE
		WHEN SN.Reminder = 0 THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS IsNote,
	SN.Notes,
	CASE 
		WHEN SN.RemindBy IN (2, 3) THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS ReminderViaDashboard,
	CASE 
		WHEN SN.RemindBy IN (1, 3) THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS ReminderViaEmail,
	SN.ReminderDate,
	CASE 
		WHEN SN.RemindTo IN (1, 3) THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS ReminderToJobSeeker,
	CASE 
		WHEN SN.RemindTo IN (2, 3) THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS ReminderToStaff,
	SN.CreatedDate AS CreatedDate,
	CAST(SN.savednotesid AS VARCHAR(10)) AS MigrationId,
	CAST(SN.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	SN.staffID AS StaffMigrationId,
	SN.savednotesid AS SavedNotesId
FROM
	dbo.SavedNotes SN
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = SN.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	SN.savednotesid > @LastId
	AND C.Name = @CustomerId
	AND SN.CreatedDate > @CutOffDate
ORDER BY
	SN.savednotesid
GO

--GRANT EXEC ON [Migrations_JobSeekerNoteReminderRequests] TO [FocusAgent]
