IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_EmployeeResumeSearchRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_EmployeeResumeSearchRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_EmployeeResumeSearchRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.savedsearches
	
--IF @CutOffDate IS NULL
--	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())

SELECT TOP (@BatchSize)
	srs.searchname AS [Name],
	srsd.searchcriteria AS CriteriaXml,
	CAST(srs.savedsearchid AS VARCHAR(10)) AS MigrationId,
	CAST(srs.employerrepid AS VARCHAR(10)) AS EmployeeMigrationId,
	srs.savedsearchid AS SavedSearchId
FROM
	dbo.savedresumesearches srs
INNER JOIN dbo.savedresumesearchdetails srsd
	ON srsd.savedsearchid = srs.savedsearchid
INNER JOIN dbo.employerrepdetails erd
	ON erd.employerrepid = srs.employerrepid
INNER JOIN dbo.employer e
	ON e.employerid = erd.employerid
INNER JOIN Customer C
	ON C.CustomerID = e.CustomerID
WHERE
	C.Name = @CustomerId
	AND srs.savedsearchid > @LastId
	AND (@CutOffDate IS NULL OR srsd.savedsearchtime > @CutOffDate)
	AND DATALENGTH(srsd.searchcriteria) > 0
ORDER BY
	srs.savedsearchid
GO

--GRANT EXEC ON [Migrations_EmployeeResumeSearchRequests] TO [FocusAgent]
