IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerActivities]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerActivities]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerActivities]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@EOS VARCHAR(30) = NULL,
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
AS

IF @LastId IS NULL
	SET @LastId = 0
	
SET @QueryLastId = @LastId	

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Activities
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId	

DECLARE @DateFrom DATETIME
SET @DateFrom = COALESCE(@CutOffDate, DATEADD(YEAR, -100, GETDATE()))
	
select top (@BatchSize) *
from (	
SELECT top (@BatchSize)  
	A.ActivityId,
	A.CustomerActivityId,
	A.ActivityTime,
	A.CustomerRepId,
	A.AddedBy,
	CAST(A.ActivityId AS VARCHAR(10)) AS MigrationId,
	CAST(A.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId
FROM 
	dbo.Activities A
INNER JOIN dbo.JobSeeker JS	ON JS.JobSeekerId = A.JobSeekerId
INNER JOIN dbo.CustomerRepDetails CRD ON CRD.CustomerRepId = JS.CustomerRepId
WHERE
	
	CRD.customerid = @CustomerKey
	AND A.ActivityId > @QueryLastId
	AND A.activitytime > @DateFrom
	AND A.CUSTOMERACTIVITYID <> 'SMRY01'
	AND A.CUSTOMERACTIVITYID <> 'UNC01'
	AND A.CUSTOMERACTIVITYID <> 'PWC01'
	AND A.CUSTOMERACTIVITYID <> 'SQA01'
	AND A.CUSTOMERACTIVITYID <> 'SSNC01'
	AND A.CUSTOMERACTIVITYID <> 'STSC01'
	AND A.CUSTOMERACTIVITYID <> 'REF01'	
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			dbo.activities A2
		WHERE
			A2.jobseekerid = A.jobseekerid 
			AND A2.customeractivityid = A.customeractivityid
			AND A2.activitytime > A.ActivityTime
			AND A2.activitytime < DATEADD(DAY, 1, CONVERT(DATE, A.ActivityTime))
	)
	union
	select		top (@BatchSize)  
				A.ActivityId,
				A.CustomerActivityId,
				A.ActivityTime,
				A.CustomerRepId,
				A.AddedBy,
				CAST(A.ActivityId AS VARCHAR(10)) AS MigrationId,
				CAST(A.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId
	from		activities a 
	INNER JOIN	dbo.JobSeeker JS			ON JS.JobSeekerId = A.JobSeekerId
	INNER JOIN	dbo.CustomerRepDetails CRD	ON CRD.CustomerRepId = JS.CustomerRepId
	where		A.CUSTOMERACTIVITYID = 'SMRY01'
	AND			a.activitytime = (select Max(activitytime) from activities a2 where a2.jobseekerid = a.jobseekerid group by jobseekerid)
	AND	CRD.customerid = @CustomerKey
	AND A.ActivityId > @QueryLastId
	AND A.activitytime > @DateFrom
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			dbo.activities A2
		WHERE
			A2.jobseekerid = A.jobseekerid 
			AND A2.customeractivityid = A.customeractivityid
			AND A2.activitytime > A.ActivityTime
			AND A2.activitytime < DATEADD(DAY, 1, CONVERT(DATE, A.ActivityTime))
	)
) as activityUnion
order by	activityUnion.activityid



GO


--GRANT EXEC ON [Migrations_JobSeekerActivities] TO [FocusAgent]

--CREATE INDEX IX_Activities_jobseeker_activitytime ON Activities(jobseekerid, activitytime)

--sp_helpindex 'Activities'

--GRANT EXEC ON [Migrations_JobSeekerActivities] TO [FocusAgent]

--CREATE INDEX IX_Activities_jobseeker_activitytime ON Activities(jobseekerid, activitytime)

--sp_helpindex 'Activities'