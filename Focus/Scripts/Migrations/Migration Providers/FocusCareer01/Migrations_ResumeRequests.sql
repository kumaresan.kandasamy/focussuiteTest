IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_ResumeRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_ResumeRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_ResumeRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.[Resume]
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId

SELECT TOP (@BatchSize)
	R.registereddate AS ResumeDate,
	JS.emailaddress AS EmailAddress,
	'Resume' + ISNULL(BR.fileextension, '.html') AS ResumeName,
	ISNULL(BR.FileExtension, '') AS FileExtension,
	R.registereddate,
	BR.[resume] AS ResumeData,
	TR.[resume] AS TaggedResume,
	CAST(R.resumeid AS VARCHAR(10)) AS MigrationId,
	CAST(R.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	R.resumeid As ResumeId
FROM
	dbo.[Resume] R WITH (FORCESEEK)
INNER JOIN dbo.taggedresumes TR
	ON TR.resumeid = R.resumeid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerID = R.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
LEFT OUTER JOIN dbo.binaryresumes BR
	ON BR.jobseekerid = R.jobseekerid
WHERE
	CRD.customerid = @CustomerKey
	AND R.ResumeId > @LastId
	AND R.active = 1
	AND R.modifieddate > @CutOffDate  	
	AND DATALENGTH(TR.[Resume]) <> 0
ORDER BY
	R.ResumeId
GO

--GRANT EXEC ON [Migrations_ResumeRequests] TO [FocusAgent]