﻿
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_SpideredJobOrderRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_SpideredJobOrderRequests]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_SpideredJobOrderRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS
BEGIN

	SET NOCOUNT ON;

IF @BatchSize IS NULL
SELECT @BatchSize = COUNT(1) 
FROM dbo.customerjobs cj 
WHERE NOT EXISTS(SELECT 1 FROM posting P WHERE P.talentjobid = CJ.CustomerJobId)

DECLARE @DateFrom DATETIME
SET @DateFrom = COALESCE(@CutOffDate, DATEADD(YEAR, -100, GETDATE()))

IF @LastId IS NULL
	SET @LastId = 0

DECLARE @CustomerKey INT

SELECT @CustomerKey = CustomerId
FROM dbo.customer 
WHERE [name] = @CustomerId

SELECT TOP (@BatchSize)
CJ.focusjobid,
CJ.lensjobid as LensPostingId,
CJ.JobTitle,
CJ.Employer as EmployerName,
CJ.datemodified as DateUpdated,
CJ.dateregistered as DateCreated,
CJ.originid as OriginId,
CJ.[Status]
FROM 					customerjobs CJ			WITH (NOLOCK)
INNER JOIN		jobs j					on j.focusjobid = CJ.focusjobid
WHERE					CJ.focusjobid > @LastId
AND						CJ.datemodified >= @DateFrom
AND						CJ.originid in (1,2,3,11,12,13,14,15,16,17,18,999) /* spidered jobs and ky jobs */

ORDER BY		CJ.focusjobid
END

GO


