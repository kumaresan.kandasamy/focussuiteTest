﻿/****** Object:  StoredProcedure [dbo].[Migrations_DeleteEmployerTradeNamesMigrationTable]    Script Date: 01/12/2015 16:55:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_DeleteEmployerTradeNamesMigrationTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_DeleteEmployerTradeNamesMigrationTable]
GO

/****** Object:  StoredProcedure [dbo].[Migrations_DeleteEmployerTradeNamesMigrationTable]    Script Date: 01/12/2015 16:55:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Migrations_DeleteEmployerTradeNamesMigrationTable]
AS
BEGIN
	SET NOCOUNT ON;

	DROP TABLE EmployerTradeNamesMigration;
	
END

GO