﻿/****** Object:  StoredProcedure [dbo].[Migrations_GetPagedEmployerTradeNames]    Script Date: 01/13/2015 10:08:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_GetPagedEmployerTradeNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_GetPagedEmployerTradeNames]
GO

/****** Object:  StoredProcedure [dbo].[Migrations_GetPagedEmployerTradeNames]    Script Date: 01/13/2015 10:08:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_GetPagedEmployerTradeNames]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME
AS
BEGIN
	SET NOCOUNT ON;

--IF(@LastId is null OR @LastId = 0)
--BEGIN
--	SELECT @LastId = MIN(TradeNameId) FROM EmployerTradeNamesMigration
--END

DECLARE @StartRowNumber int;
DECLARE @EndRowNumber int;

SELECT	@StartRowNumber = COUNT('X') + 1
FROM	EmployerTradeNamesMigration 
WHERE	TradeNameId <= @LastId

SELECT	@EndRowNumber = @StartRowNumber + @BatchSize

SELECT		TradeNameId,
			TradeName,
			IsDefault,
			EmployerId,
			LegalName,
			EmployeeId,
			Fein,
			Street1,
			Street2,
			City,
			State,
			County,
			Country,
			PostalCode
FROM 
(
	SELECT 	TradeNameId,
			TradeName,
			IsDefault,
			EmployerId,
			LegalName,
			EmployeeId,
			Fein,
			Street1,
			Street2,
			City,
			State,
			County,
			Country,
			PostalCode, 
			ROW_NUMBER() OVER (ORDER BY TradeNameId) AS RowNumber
	FROM	EmployerTradeNamesMigration
) AS NumberedEmployerTradeNames
WHERE RowNumber 
BETWEEN @StartRowNumber 
AND @EndRowNumber
END


GO