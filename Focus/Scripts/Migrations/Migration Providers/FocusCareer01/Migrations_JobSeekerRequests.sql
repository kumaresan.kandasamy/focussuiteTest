IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0

SET @QueryLastId = @LastId

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.JobSeeker
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -20, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId

SELECT TOP (@BatchSize)
	JS.username AS UserName,
	JS.passcode AS Passcode,
	JS.emailaddress AS EmailAddress,
	ISNULL(JS.SSN, '') AS SSN,
	ISNULL(JS.securityquestion, '') AS SecurityQuestion,
	ISNULL(JS.securityanswer, '') AS SecurityAnswer,
	ISNULL(JS.Reserved02, '') AS ExtraInfo,
	JS.lastlogin,
	JS.RegisteredDate,
	TR.[Resume] AS [Resume],
	JS.customerregistrationid AS ExternalJobSeekerId,
	CAST(JS.jobseekerid AS VARCHAR(10)) AS MigrationId,
	JS.jobseekerid AS JobSeekerId
FROM
	dbo.JobSeeker JS --WITH (FORCESEEK)
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
LEFT OUTER JOIN
( 
	dbo.[Resume] R --WITH (INDEX (IX_resume_jobseekerid))
	INNER JOIN dbo.TaggedResumes TR
		ON TR.ResumeId = R.ResumeId
)
	ON R.JobSeekerId = JS.JobSeekerId
WHERE
	CRD.CustomerId = @CustomerKey
	AND JS.jobseekerid > @QueryLastId
	AND JS.LastLogin > @CutOffDate
ORDER BY
	JS.jobseekerid
GO

--GRANT EXEC ON [dbo].[Migrations_JobSeekerRequests] TO [FocusAgent]

-- CREATE INDEX IX_resume_jobseekerid ON dbo.[Resume] (JobSeekerId)

