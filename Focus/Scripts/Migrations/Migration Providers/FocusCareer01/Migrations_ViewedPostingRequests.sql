IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_ViewedPostingRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_ViewedPostingRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_ViewedPostingRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0

SET @QueryLastId = @LastId

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.jobsviewed

IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

;WITH Viewed AS
(
	SELECT
		JV.viewedid,
		JV.jobseekerid,
		JV.ViewedTime,
		JV.FocusJobId,
		ROW_NUMBER() OVER (PARTITION BY JV.JobSeekerId ORDER BY JV.ViewedId DESC) AS RowNumber
	FROM
		dbo.jobsviewed JV WITH (INDEX (ix_migrations_jobsviewed))
)	
SELECT TOP (@BatchSize)
	JV.ViewedTime AS ViewedDate,
	J.lensjobid AS LensPostingId,
	CAST(JV.ViewedId AS VARCHAR(10)) AS MigrationId,
	CAST(JV.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	JV.ViewedId AS JobsViewedId
FROM
	Viewed JV
INNER JOIN dbo.jobs J
	ON J.focusjobid = JV.focusjobid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = JV.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.Name = @CustomerId
	AND JV.ViewedId > @QueryLastId
	AND JV.ViewedTime >= @CutOffDate
	AND JV.RowNumber <= 5
ORDER BY
	JV.ViewedId
GO

--CREATE INDEX IX_JobsViewed_ViewedId ON JobsViewed (ViewedId)
--CREATE INDEX IX_JobsViewed_JobSeekerId_ViewedId ON JobsViewed (JobSeekerId, ViewedId DESC)

--GRANT EXEC ON [Migrations_ViewedPostingRequests] TO [FocusAgent]

