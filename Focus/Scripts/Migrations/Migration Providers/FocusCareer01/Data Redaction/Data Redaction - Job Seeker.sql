IF OBJECT_ID('tempdb..#JobSeeker') IS NOT NULL
	DROP TABLE #JobSeeker

IF OBJECT_ID('tempdb..#Resume') IS NOT NULL
	DROP TABLE #Resume

CREATE TABLE #JobSeeker
(
	Id INT IDENTITY(0, 1) PRIMARY KEY,
	JobSeekerId INT,
	FirstName VARCHAR(50),
	Surname VARCHAR(50),
	SSN VARCHAR(12),
	UserName VARCHAR(100),
	EmailAddress VARCHAR(100),
	RedactedUserName VARCHAR(100),
	RedactedEmailAddress VARCHAR(100),
	StreetAddress VARCHAR(100),
	PhoneNumber1 VARCHAR(10),
	PhoneNumber2 VARCHAR(10),
	PhoneNumber3 VARCHAR(10),
	PhoneNumber4 VARCHAR(10),
	DateOfBirth DATETIME,
	DOB VARCHAR(10),
	DOB_ISO VARCHAR(10),
	DOB_Days INT,
	SecurityAnswer VARCHAR(10),
	CustomerRegistrationId VARCHAR(20)
)

CREATE TABLE #Resume
(
	ResumeId INT PRIMARY KEY,
	JobSeekerId INT,
	[Resume] XML,
	DOB VARCHAR(10),
	DOB_ISO VARCHAR(10),
	DOB_Days INT
)

CREATE INDEX IX_Temp_JobSeeker_JobSeekerId ON #JobSeeker(JobSeekerId)
CREATE INDEX IX_Temp_Resume_JobSeekerId ON #Resume(JobSeekerId)

-- Get job seekers, and set some random fields for later update
INSERT INTO #JobSeeker 
(
	JobSeekerId,
	EmailAddress,
	UserName
)
SELECT
	JS.JobSeekerId,
	JS.EmailAddress,
	JS.UserName
FROM 
	dbo.JobSeeker JS
ORDER BY
	JS.JobSeekerId ASC
	
RAISERROR ('Got job seeker ids', 0, 1) WITH NOWAIT

-- Get the resumes
INSERT INTO #Resume 
(
	ResumeId,
	JobSeekerId, 
	[Resume]
)
SELECT
	R.ResumeId,
	JS.JobSeekerId, 
	CAST(TR.[Resume] AS XML)
FROM 
	#JobSeeker JS
INNER JOIN dbo.[Resume] R
	ON R.JobSeekerId = JS.JobSeekerId
INNER JOIN dbo.TaggedResumes TR
	ON TR.ResumeId = R.ResumeId
	
RAISERROR ('Got resumes', 0, 1) WITH NOWAIT

-- Extract DOB from the resume
UPDATE
	#Resume
SET
	DOB = NULLIF([Resume].value('(/ResDoc/resume/statements/personal/dob/text())[1]', 'VARCHAR(10)'), ''),
	DOB_ISO = NULLIF([Resume].value('(/ResDoc/resume/statements/personal/dob/@iso8601)[1]', 'VARCHAR(10)'), ''),
	DOB_Days = NULLIF([Resume].value('(/ResDoc/resume/statements/personal/dob/@days)[1]', 'INT'), '')

RAISERROR ('Extracted date of births', 0, 1) WITH NOWAIT

-- Randomise fields on job seeker
DECLARE @FirstNameMaxSize INT
DECLARE @FirstNamePadder VARCHAR(100)

DECLARE @LastNameMaxSize INT
DECLARE @LastNamePadder VARCHAR(100)

SELECT @FirstNameMaxSize = MAX(LEN(FirstName)) FROM dbo.ResumeInfo
SET @FirstNamePadder = REPLICATE('a', @FirstNameMaxSize)

SELECT @LastNameMaxSize = MAX(LEN(LastName)) FROM dbo.ResumeInfo
SET @LastNamePadder = REPLICATE('a', @FirstNameMaxSize)

UPDATE
	JS
SET
	FirstName = RIGHT(@FirstNamePadder + LTRIM(
		ISNULL(CHAR(97 + NULLIF(JS.Id / 11881376, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(JS.Id / 456976, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(JS.Id / 17576, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(JS.Id / 676, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(JS.Id / 26, 0) % 26), '')
		+ CHAR(97 + JS.Id % 26)), @FirstNameMaxSize),
	StreetAddress = CAST(JS.Id % 99 + 1 AS VARCHAR(2)) + ' ' 
		+ RTRIM(SUBSTRING('High  Lower Upper Main  New   Kings Queens', JS.Id % 7 * 6 + 1, 6)) + ' '
		+ RTRIM(SUBSTRING('StreetRoad  AvenueWay   Lane  ', JS.Id % 5 * 6 + 1, 6)),
	PhoneNumber1 = CAST(1111111111 + JS.Id AS VARCHAR(10)), 
	PhoneNumber2 = CAST(2222222222 + JS.Id AS VARCHAR(10)), 
	PhoneNumber3 = CAST(3333333333 + JS.Id AS VARCHAR(10)), 
	PhoneNumber4 = CAST(4444444444 + JS.Id AS VARCHAR(10)), 
	SecurityAnswer = RTRIM(SUBSTRING('Red   OrangeGreen Blue  Yellow', JS.Id % 5 * 6 + 1, 6)),
	SSN = RIGHT('000000000' + CAST(Id + 1 AS VARCHAR(9)), 9),
	DateOfBirth = 
		CASE ISDATE(R.DOB)
			WHEN 0 THEN DATEADD(MONTH, JS.Id % 360, '01 January 1960')
			ELSE DATEADD(DAY, 1 - DATEPART(DAY, CAST(R.DOB AS DATETIME)), CAST(R.DOB AS DATETIME))
		END,
	CustomerRegistrationId = 'KY' + CAST(100000000 + JS.JobSeekerId AS VARCHAR(10))
FROM
	#JobSeeker JS
LEFT OUTER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
	AND R.DOB IS NOT NULL
	
RAISERROR ('Prepared redacted job seeker data - Step 1', 0, 1) WITH NOWAIT

UPDATE
	#JobSeeker
SET
	Surname = RIGHT(@LastNamePadder + FirstName, @LastNameMaxSize),
	RedactedEmailAddress = FirstName + '@test.burning-glass.com',
	RedactedUserName = FirstName + '@test.burning-glass.com',
	DOB = ISNULL(CONVERT(VARCHAR(10), DateOfBirth, 101), ''),
	DOB_ISO = ISNULL(LEFT(CONVERT(VARCHAR(10), DateOfBirth, 120), 10), ''),
	DOB_Days = 693598 + DATEDIFF(DAY, '01 January 1900', DateOfBirth)
	
RAISERROR ('Prepared redacted job seeker data - Step 2', 0, 1) WITH NOWAIT

UPDATE
	#JobSeeker
SET
	EmailAddress = CASE ISNULL(EmailAddress, '') WHEN '' THEN EmailAddress ELSE RedactedEmailAddress END,
	UserName = CASE ISNULL(UserName, '') WHEN '' THEN UserName ELSE RedactedUserName END
	
RAISERROR ('Prepared redacted job seeker data - Step 3', 0, 1) WITH NOWAIT

IF EXISTS(SELECT 1 FROM #Resume R WHERE R.DOB IS NOT NULL AND ISDATE(R.DOB) = 0)
BEGIN
	SELECT
		'Invalid date of birth found: ' + R.DOB
	FROM
		#Resume R
	WHERE
		R.DOB IS NOT NULL
		AND ISDATE(R.DOB) = 0
END

-- Update ResumeXML with randomised job seeker data
UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/contact/name/givenname/text())[1] with sql:column("FirstName")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/contact/name/givenname/text())[1][. != ""]') = 1
		
RAISERROR ('Prepared redacted resume data - First name', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/contact/name/surname/text())[1] with sql:column("Surname")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/contact/name/surname/text())[1][. != ""]') = 1
	
RAISERROR ('Prepared redacted resume data - Surname', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/contact/email/text())[1] with sql:column("RedactedEmailAddress")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/contact/email/text())[1][. != ""]') = 1
	
RAISERROR ('Prepared redacted resume data - Email address', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/statements/personal/userinfo/username/text())[1] with sql:column("RedactedUserName")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/statements/personal/userinfo/username/text())[1][. != ""]') = 1
	
RAISERROR ('Prepared redacted resume data - User name', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/statements/personal/userinfo/ssn/text())[1] with sql:column("SSN")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/statements/personal/userinfo/ssn/text())[1][. != ""]') = 1
			
RAISERROR ('Prepare redacted resume data - SSN', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/contact/address/street/text())[1] with sql:column("StreetAddress")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/contact/address/street/text())[1][. != ""]') = 1
	
RAISERROR ('Prepare redacted resume data - Street address', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/contact/phone[1]/text())[1] with sql:column("PhoneNumber1")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/contact/phone[1]/text())[1][. != ""]') = 1
	
RAISERROR ('Prepare redacted resume data - Phone number 1', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/contact/phone[2]/text())[1] with sql:column("PhoneNumber2")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/contact/phone[2]/text())[1][. != ""]') = 1
	
RAISERROR ('Prepare redacted resume data - Phone number 2', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/contact/phone[3]/text())[1] with sql:column("PhoneNumber3")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/contact/phone[3]/text())[1][. != ""]') = 1

RAISERROR ('Prepare redacted resume data - Phone number 3', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/contact/phone[4]/text())[1] with sql:column("PhoneNumber4")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/contact/phone[4]/text())[1][. != ""]') = 1
	
RAISERROR ('Prepare redacted resume data - Phone number 4', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/statements/personal/dob/text())[1] with sql:column("JS.DOB")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/statements/personal/dob/text())[1][. != ""]') = 1
	
RAISERROR ('Prepare redacted resume data - Date of birth', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/statements/personal/dob/@iso8601)[1] with sql:column("JS.DOB_ISO")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/statements/personal/dob/@iso8601)[1][. != ""]') = 1
	
RAISERROR ('Prepare redacted resume data - Date of birth (ISO)', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	[Resume].modify('replace value of (/ResDoc/resume/statements/personal/dob/@days)[1] with sql:column("JS.DOB_Days")')
FROM 
	#JobSeeker JS
INNER JOIN #Resume R
	ON R.JobSeekerId = JS.JobSeekerId
WHERE 
	[Resume].exist('(/ResDoc/resume/statements/personal/dob/@days)[1][. != ""]') = 1
	
RAISERROR ('Prepare redacted resume data - Date of birth (Days)', 0, 1) WITH NOWAIT

-- Update fields on job seeker table
UPDATE
	JS
SET
	UserName = JS1.UserName,
	EmailAddress = JS1.EmailAddress,
	SecurityQuestion = 'What is your favorite color?',
	SecurityAnswer = JS1.SecurityAnswer,
	SSN = CASE ISNULL(JS.SSN, '') WHEN '' THEN JS.SSN ELSE JS1.SSN END,
	CustomerRegistrationId = CASE ISNULL(JS.CustomerRegistrationId, '') WHEN '' THEN JS.CustomerRegistrationId ELSE JS1.CustomerRegistrationId END
FROM
	dbo.JobSeeker JS
INNER JOIN #JobSeeker JS1
	ON JS1.JobSeekerId = JS.JobSeekerID	
	
RAISERROR ('Redacted job seeker data', 0, 1) WITH NOWAIT

-- Update fields on the resume info table
UPDATE
	RI
SET
	FirstName = CASE RI.FirstName WHEN '' THEN '' ELSE JS1.FirstName END,
	LastName = CASE RI.LastName WHEN '' THEN '' ELSE JS1.SurName END,
	Street = CASE RI.Street WHEN '' THEN '' ELSE JS1.StreetAddress END,
	WorkPhone = CASE RI.WorkPhone WHEN '' THEN '' ELSE JS1.PhoneNumber1 END,
	HomePhone = CASE RI.HomePhone WHEN '' THEN '' ELSE JS1.PhoneNumber2 END,
	MobilePhone = CASE RI.MobilePhone WHEN '' THEN '' ELSE JS1.PhoneNumber3 END,
	Fax = CASE RI.Fax WHEN '' THEN '' ELSE JS1.PhoneNumber4 END,
	Email = CASE RI.Email WHEN '' THEN '' ELSE JS1.EmailAddress END,
	dob = CAST(JS1.DOB AS DATETIME)
FROM
	dbo.ResumeInfo RI
INNER JOIN dbo.[Resume] R
	ON R.ResumeId = RI.ResumeId
INNER JOIN #JobSeeker JS1
	ON JS1.JobSeekerId = R.JobSeekerID	

RAISERROR ('Redacted resume data', 0, 1) WITH NOWAIT

-- Update the tagged resume xml
UPDATE
	TR
SET
	[Resume] = CAST(CAST(R1.[Resume] AS VARCHAR(MAX)) AS TEXT)
FROM
	dbo.TaggedResumes TR
INNER JOIN #Resume R1
	ON R1.ResumeId = TR.ResumeId

RAISERROR ('Redacted tagged resume data', 0, 1) WITH NOWAIT
