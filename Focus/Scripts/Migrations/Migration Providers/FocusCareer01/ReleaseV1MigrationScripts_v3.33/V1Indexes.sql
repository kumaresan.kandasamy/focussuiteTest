﻿IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_Activities_jobseeker_activitytime')BEGINCREATE INDEX IX_Activities_jobseeker_activitytime ON activities(		[jobseekerid] ASC , [activitytime] ASC )   ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'activitytime')BEGINCREATE INDEX activitytime ON activities(			[jobseekerid] ASC , [customeractivityid] ASC , [activitytime] ASC )  ON [PRIMARY]END 
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_applicantdata')BEGINCREATE UNIQUE CLUSTERED INDEX PK_applicantdata ON applicantdata(		[jobseekerid] ASC , [employerrepid] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__applican__FFF0EF105BCD9859')BEGINCREATE UNIQUE INDEX UQ__applican__FFF0EF105BCD9859 ON applicants(		[jobseekerid] ASC , [postingid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__binaryre__162997532942188C')BEGINCREATE UNIQUE CLUSTERED INDEX PK__binaryre__162997532942188C ON binaryresumes(	[jobseekerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__careerop__74143AEE06CD04F7')BEGINCREATE UNIQUE CLUSTERED INDEX PK__careerop__74143AEE06CD04F7 ON careeroperations(	[careeroperation] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__careerop__4E2E925509A971A2')BEGINCREATE UNIQUE INDEX UQ__careerop__4E2E925509A971A2 ON careeroperations(	[careeroperationid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_certificatecategories')BEGINCREATE UNIQUE CLUSTERED INDEX PK_certificatecategories ON certificatecategories(	[category] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__certific__A15DB2871EA48E88')BEGINCREATE UNIQUE INDEX UQ__certific__A15DB2871EA48E88 ON certificatelookup(	[certificateid] ASC )   ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__certific__A04C4B425812160E')BEGINCREATE UNIQUE CLUSTERED INDEX PK__certific__A04C4B425812160E ON certificateorganization(	[organizationname] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__certific__297579405AEE82B9')BEGINCREATE UNIQUE INDEX UQ__certific__297579405AEE82B9 ON certificateorganization(	[organizationid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__certific__A64470BC5165187F')BEGINCREATE UNIQUE CLUSTERED INDEX PK__certific__A64470BC5165187F ON certificatetype(	[typename] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__certific__86BE2DBD5441852A')BEGINCREATE UNIQUE INDEX UQ__certific__86BE2DBD5441852A ON certificatetype(	[typecode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__citieslo__B86F2CE1114A936A')BEGINCREATE UNIQUE CLUSTERED INDEX PK__citieslo__B86F2CE1114A936A ON citieslookup(			[city] ASC , [statecodeid] ASC , [countrycodeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__citieslo__EF7DE16314270015')BEGINCREATE UNIQUE INDEX UQ__citieslo__EF7DE16314270015 ON citieslookup(	[lookupid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__cleanres__C2E77C405006DFF2')BEGINCREATE UNIQUE CLUSTERED INDEX PK__cleanres__C2E77C405006DFF2 ON cleanresumes(	[resumeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__cleanres__C2E77C4152E34C9D')BEGINCREATE UNIQUE INDEX UQ__cleanres__C2E77C4152E34C9D ON cleanresumes(	[resumeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__countryc__C70472824AB81AF0')BEGINCREATE UNIQUE CLUSTERED INDEX PK__countryc__C70472824AB81AF0 ON countrycodes(	[countrycode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__countryc__2B9FBC744D94879B')BEGINCREATE UNIQUE INDEX UQ__countryc__2B9FBC744D94879B ON countrycodes(	[countrycodeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_countylookup')BEGINCREATE UNIQUE CLUSTERED INDEX PK_countylookup ON countylookup(	[countycode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__customer__72E12F1A17F790F9')BEGINCREATE UNIQUE CLUSTERED INDEX PK__customer__72E12F1A17F790F9 ON customer(	[name] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__customer__B61ED7F41AD3FDA4')BEGINCREATE UNIQUE INDEX UQ__customer__B61ED7F41AD3FDA4 ON customer(	[customerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__customer__478BB93C56E8E7AB')BEGINCREATE UNIQUE CLUSTERED INDEX PK__customer__478BB93C56E8E7AB ON customerblacklistedsources(		[customerid] ASC , [source] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__customer__347735A559C55456')BEGINCREATE UNIQUE INDEX UQ__customer__347735A559C55456 ON customerblacklistedsources(	[blacklistedsourceid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__customer__C522AF35503BEA1C')BEGINCREATE UNIQUE CLUSTERED INDEX PK__customer__C522AF35503BEA1C ON customercentrecodes(		[customerid] ASC , [centrecode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__customer__4BC4DA0B531856C7')BEGINCREATE UNIQUE INDEX UQ__customer__4BC4DA0B531856C7 ON customercentrecodes(	[customercentercodeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__customer__5A207353498EEC8D')BEGINCREATE UNIQUE CLUSTERED INDEX PK__customer__5A207353498EEC8D ON customercounties(		[customerid] ASC , [county] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__customer__6F7BD9684C6B5938')BEGINCREATE UNIQUE INDEX UQ__customer__6F7BD9684C6B5938 ON customercounties(	[customercountyid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__customer__2A4D771F42E1EEFE')BEGINCREATE UNIQUE CLUSTERED INDEX PK__customer__2A4D771F42E1EEFE ON customerjobs(	[lensjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__customer__9D97842E45BE5BA9')BEGINCREATE UNIQUE INDEX UQ__customer__9D97842E45BE5BA9 ON customerjobs(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fkcjcustomerid')BEGINCREATE INDEX fkcjcustomerid ON customerjobs(	[customerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fklensserverid')BEGINCREATE INDEX fklensserverid ON customerjobs(	[lensserverid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerjobs_onetcode')BEGINCREATE INDEX IX_customerjobs_onetcode ON customerjobs(	[onetcode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerjobs_city')BEGINCREATE INDEX IX_customerjobs_city ON customerjobs(	[city] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ind_customerjobs_lensjobid_status')BEGINCREATE INDEX ind_customerjobs_lensjobid_status ON customerjobs(		[lensjobid] ASC , [status] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerjobs_customerid_customerjobid_isactive')BEGINCREATE INDEX IX_customerjobs_customerid_customerjobid_isactive ON customerjobs(			[customerid] ASC , [customerjobid] ASC , [isactive] ASC )   ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerjobs_customerid_dateregistered')BEGINCREATE INDEX IX_customerjobs_customerid_dateregistered ON customerjobs(		[customerid] ASC , [dateregistered] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_CustomerJobs_customerId_OriginId')BEGINCREATE INDEX IX_CustomerJobs_customerId_OriginId ON customerjobs(		[customerjobid] ASC , [originid] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerjobs_customerjobid_isactive')BEGINCREATE INDEX IX_customerjobs_customerjobid_isactive ON customerjobs(		[customerjobid] ASC , [isactive] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerjobs_dateregistered')BEGINCREATE INDEX IX_customerjobs_dateregistered ON customerjobs(	[dateregistered] ASC )  INCLUDE ( [customerid] )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerjobs_state')BEGINCREATE INDEX IX_customerjobs_state ON customerjobs(	[state] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_CustomerJobs_Status')BEGINCREATE INDEX IX_CustomerJobs_Status ON customerjobs(		[lensjobid] ASC , [status] ASC )  INCLUDE ( [focusjobid],[jobtitle],[employer],[opendate],[closedate],[city],[state],[reserved01],[originid] )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_CustomerJobs_Status_Extra_Temp')BEGINCREATE INDEX IX_CustomerJobs_Status_Extra_Temp ON customerjobs(	[status] ASC )  INCLUDE ( [focusjobid],[customerid],[lensjobid],[jobtitle],[employer],[opendate],[closedate],[city],[state],[reserved01],[originid] )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_CustomerJobs_Status_focusjobid_customerlensid_lensjobid')BEGINCREATE INDEX IX_CustomerJobs_Status_focusjobid_customerlensid_lensjobid ON customerjobs(	[status] ASC )  INCLUDE ( [focusjobid],[customerid],[lensjobid] )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_Migration_SpideredJobs2')BEGINCREATE INDEX IX_Migration_SpideredJobs2 ON customerjobs(				[customerid] ASC , [originid] ASC , [focusjobid] ASC , [datemodified] ASC )  INCLUDE ( [customerjobid],[lensjobid],[jobtitle],[employer] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_customerjobsinfo')BEGINCREATE UNIQUE CLUSTERED INDEX PK_customerjobsinfo ON customerjobsinfo(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__customer__B50BCD353C34F16F')BEGINCREATE UNIQUE CLUSTERED INDEX PK__customer__B50BCD353C34F16F ON customerlensservice(	[customerlensserviceid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__customer__B50BCD343F115E1A')BEGINCREATE UNIQUE INDEX UQ__customer__B50BCD343F115E1A ON customerlensservice(	[customerlensserviceid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fkclientlenslabel')BEGINCREATE INDEX fkclientlenslabel ON customerlensservice(	[lenslabel] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fkclientlensservicesclientid')BEGINCREATE INDEX fkclientlensservicesclientid ON customerlensservice(	[customerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__customer__A1EFC6BF6166761E')BEGINCREATE UNIQUE CLUSTERED INDEX PK__customer__A1EFC6BF6166761E ON customerrepdetails(				[email] ASC , [centreid] ASC , [customerid] ASC , [reptypeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__customer__118D1E276442E2C9')BEGINCREATE UNIQUE INDEX UQ__customer__118D1E276442E2C9 ON customerrepdetails(	[customerrepid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerrepdetails_customerid_customerrepid_email_centreid_reptypeid')BEGINCREATE INDEX IX_customerrepdetails_customerid_customerrepid_email_centreid_reptypeid ON customerrepdetails(					[customerid] ASC , [customerrepid] ASC , [email] ASC , [centreid] ASC , [reptypeid] ASC )  INCLUDE ( [postalcode] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerrepdetails_isactive_centreid_email_customerid_retypeid')BEGINCREATE INDEX IX_customerrepdetails_isactive_centreid_email_customerid_retypeid ON customerrepdetails(					[isactive] ASC , [centreid] ASC , [email] ASC , [customerid] ASC , [reptypeid] ASC )  INCLUDE ( [customerrepid],[firstname],[lastname] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_customerrepdetails_postalcode')BEGINCREATE INDEX IX_customerrepdetails_postalcode ON customerrepdetails(	[postalcode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__customer__E4645E94440B1D61')BEGINCREATE UNIQUE CLUSTERED INDEX PK__customer__E4645E94440B1D61 ON customerreptypes(	[reptype] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__customer__DF13095246E78A0C')BEGINCREATE UNIQUE INDEX UQ__customer__DF13095246E78A0C ON customerreptypes(	[customerreptypeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__degreelo__2A741F623D5E1FD2')BEGINCREATE UNIQUE CLUSTERED INDEX PK__degreelo__2A741F623D5E1FD2 ON degreelookup(	[degree] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__degreelo__EF7DE163403A8C7D')BEGINCREATE UNIQUE INDEX UQ__degreelo__EF7DE163403A8C7D ON degreelookup(	[lookupid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__domainda__923D3B7D70099B30')BEGINCREATE UNIQUE CLUSTERED INDEX PK__domainda__923D3B7D70099B30 ON domaindata(	[dataid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_domaindata_status_domainid')BEGINCREATE INDEX IX_domaindata_status_domainid ON domaindata(		[status] ASC , [domainid] ASC )  INCLUDE ( [code],[value] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__domainva__67666957668030F6')BEGINCREATE UNIQUE CLUSTERED INDEX PK__domainva__67666957668030F6 ON domainvalues(		[eos] ASC , [focusdomain] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__domainva__4A884FA8695C9DA1')BEGINCREATE UNIQUE INDEX UQ__domainva__4A884FA8695C9DA1 ON domainvalues(	[domainid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__employer__F45FF8E846D27B73')BEGINCREATE UNIQUE CLUSTERED INDEX PK__employer__F45FF8E846D27B73 ON employer(	[employerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__employer__2991AF6749AEE81E')BEGINCREATE UNIQUE INDEX UQ__employer__2991AF6749AEE81E ON employer(	[fein] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_Employer_CustomerId')BEGINCREATE INDEX IX_Employer_CustomerId ON employer(	[customerid] ASC )  INCLUDE ( [employerid],[legalname],[lastmodified] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__employer__F9579B6A53385258')BEGINCREATE UNIQUE CLUSTERED INDEX PK__employer__F9579B6A53385258 ON employerdescriptions(	[descriptionid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__employer__3B875A4E5CC1BC92')BEGINCREATE UNIQUE CLUSTERED INDEX PK__employer__3B875A4E5CC1BC92 ON employerfeedback(	[employerfeedbackid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__employer__49A38B535708E33C')BEGINCREATE UNIQUE CLUSTERED INDEX PK__employer__49A38B535708E33C ON employerlogos(	[logoid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_employerlookup')BEGINCREATE UNIQUE CLUSTERED INDEX PK_employerlookup ON employerlookup(	[employerlookupid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__employer__A269C68A3E3D3572')BEGINCREATE UNIQUE CLUSTERED INDEX PK__employer__A269C68A3E3D3572 ON employerrepdetails(	[employerrepid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_employerrepdetails_employerid')BEGINCREATE INDEX IX_employerrepdetails_employerid ON employerrepdetails(	[employerid] ASC )  INCLUDE ( [employerrepid],[street1],[street2],[city],[state],[county],[country],[postalcode],[lastmodified] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__employer__9F87A7814D7F7902')BEGINCREATE UNIQUE CLUSTERED INDEX PK__employer__9F87A7814D7F7902 ON employertradenames(	[tradenameid] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__feedback__9D97842F5D95E53A')BEGINCREATE UNIQUE CLUSTERED INDEX PK__feedback__9D97842F5D95E53A ON feedbackresults(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fkfeedbackresultsjobid')BEGINCREATE INDEX fkfeedbackresultsjobid ON feedbackresults(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__jobalert__6C3031DB4959E263')BEGINCREATE UNIQUE CLUSTERED INDEX PK__jobalert__6C3031DB4959E263 ON jobalertlog(	[jobalertid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__jobalert__68DE0F694C364F0E')BEGINCREATE UNIQUE INDEX UQ__jobalert__68DE0F694C364F0E ON jobalertlog(	[jobalertlogid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fkjobalertlogid')BEGINCREATE INDEX fkjobalertlogid ON jobalertlog(	[jobalertid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__jobalert__D402CEED7D0E9093')BEGINCREATE UNIQUE CLUSTERED INDEX PK__jobalert__D402CEED7D0E9093 ON jobalerts(		[jobseekerid] ASC , [alertname] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__jobalert__6C3031DA7FEAFD3E')BEGINCREATE UNIQUE INDEX UQ__jobalert__6C3031DA7FEAFD3E ON jobalerts(	[jobalertid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fkjobalertsuserid')BEGINCREATE INDEX fkjobalertsuserid ON jobalerts(	[jobseekerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobalerts_alertinterval_status_email')BEGINCREATE INDEX IX_jobalerts_alertinterval_status_email ON jobalerts(			[alertinterval] ASC , [status] ASC , [email] ASC )  INCLUDE ( [jobalertid],[jobseekerid],[alertname] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_Migrations_SearchAlert')BEGINCREATE INDEX IX_Migrations_SearchAlert ON jobalerts(					[jobalertid] ASC , [alertname] ASC , [alertinterval] ASC , [alertmailformat] ASC , [email] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__jobarchi__9D97842F0D64F3ED')BEGINCREATE UNIQUE CLUSTERED INDEX PK__jobarchi__9D97842F0D64F3ED ON jobarchive(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__jobmatch__52458B0720ACD28B')BEGINCREATE UNIQUE CLUSTERED INDEX PK__jobmatch__52458B0720ACD28B ON jobmatchesstats(	[jobmatchesstatsid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__jobs__9D97842F02FC7413')BEGINCREATE UNIQUE CLUSTERED INDEX PK__jobs__9D97842F02FC7413 ON jobs(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'jobs_indx')BEGINCREATE INDEX jobs_indx ON jobs(			[focusjobid] ASC , [lensjobid] ASC , [customerjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobs_lensjobid')BEGINCREATE UNIQUE INDEX IX_jobs_lensjobid ON jobs(	[lensjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__jobseeke__F3DBC57376619304')BEGINCREATE UNIQUE CLUSTERED INDEX PK__jobseeke__F3DBC57376619304 ON jobseeker(	[username] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__jobseeke__16299752793DFFAF')BEGINCREATE UNIQUE INDEX UQ__jobseeke__16299752793DFFAF ON jobseeker(	[jobseekerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'index_jobseeker_emailaddress')BEGINCREATE INDEX index_jobseeker_emailaddress ON jobseeker(	[emailaddress] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobseeker_ssn')BEGINCREATE INDEX IX_jobseeker_ssn ON jobseeker(	[SSN] ASC )  INCLUDE ( [jobseekerid],[username],[status] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobseeker_isveteran_jobseekerid_username')BEGINCREATE INDEX IX_jobseeker_isveteran_jobseekerid_username ON jobseeker(			[isveteran] ASC , [jobseekerid] ASC , [username] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobseeker_customerregistrationid')BEGINCREATE INDEX IX_jobseeker_customerregistrationid ON jobseeker(	[customerregistrationid] ASC )  INCLUDE ( [jobseekerid] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobseeker_customerrepid_status_reportstatus')BEGINCREATE INDEX IX_jobseeker_customerrepid_status_reportstatus ON jobseeker(			[customerrepid] ASC , [status] ASC , [reportstatus] ASC )  INCLUDE ( [jobseekerid] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobseeker_customerrepid_username')BEGINCREATE INDEX IX_jobseeker_customerrepid_username ON jobseeker(		[customerrepid] ASC , [username] ASC )  INCLUDE ( [jobseekerid],[emailaddress],[status],[registereddate],[lastlogin],[SSN],[securityquestion],[securityanswer],[consent],[ismigrated],[reserved01],[reserved02] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobseeker_passcode_customerrepid_username')BEGINCREATE INDEX IX_jobseeker_passcode_customerrepid_username ON jobseeker(			[passcode] ASC , [customerrepid] ASC , [username] ASC )  INCLUDE ( [jobseekerid],[emailaddress],[status] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_activities_query')BEGINCREATE INDEX ix_activities_query ON jobseeker(		[jobseekerid] ASC , [customerrepid] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__jobseeke__16299753113584D1')BEGINCREATE UNIQUE CLUSTERED INDEX PK__jobseeke__16299753113584D1 ON jobseekerdetails(	[jobseekerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__jobssent__85E9499942ACE4D4')BEGINCREATE UNIQUE CLUSTERED INDEX PK__jobssent__85E9499942ACE4D4 ON jobssentinalerts(		[jobalertid] ASC , [focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__jobssent__A585B1FD4589517F')BEGINCREATE UNIQUE INDEX UQ__jobssent__A585B1FD4589517F ON jobssentinalerts(	[jobsentalertid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fkfocusjobid')BEGINCREATE INDEX fkfocusjobid ON jobssentinalerts(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'fkjobalertid')BEGINCREATE INDEX fkjobalertid ON jobssentinalerts(	[jobalertid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobsentinelalerts_jobsalertid_jobsenttime')BEGINCREATE INDEX IX_jobsentinelalerts_jobsalertid_jobsenttime ON jobssentinalerts(		[jobalertid] ASC , [jobsenttime] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_jobsentinelalerts_searchtime_score')BEGINCREATE INDEX IX_jobsentinelalerts_searchtime_score ON jobssentinalerts(		[searchtime] ASC , [score] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__jobsview__FFF0EF1003BB8E22')BEGINCREATE UNIQUE INDEX UQ__jobsview__FFF0EF1003BB8E22 ON jobsviewed(		[jobseekerid] ASC , [focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_jobtype_1')BEGINCREATE UNIQUE CLUSTERED INDEX PK_jobtype_1 ON jobtype(	[jobtypeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_exportid')BEGINCREATE UNIQUE CLUSTERED INDEX PK_exportid ON legacyexportactivities(	[exportid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_transactionid')BEGINCREATE UNIQUE CLUSTERED INDEX PK_transactionid ON legacytransactionlog(	[transactionid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__lenscust__E90BBE4E75A278F5')BEGINCREATE UNIQUE CLUSTERED INDEX PK__lenscust__E90BBE4E75A278F5 ON lenscustomfilterlookup(	[lenscustomfilter] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__lenscust__EF7DE163787EE5A0')BEGINCREATE UNIQUE INDEX UQ__lenscust__EF7DE163787EE5A0 ON lenscustomfilterlookup(	[lookupid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__lensserv__C46F957D6EF57B66')BEGINCREATE UNIQUE CLUSTERED INDEX PK__lensserv__C46F957D6EF57B66 ON lensserverdetails(	[lenslabel] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__lensserv__7075B97771D1E811')BEGINCREATE UNIQUE INDEX UQ__lensserv__7075B97771D1E811 ON lensserverdetails(	[lensserverid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__majorloo__1110818C68487DD7')BEGINCREATE UNIQUE CLUSTERED INDEX PK__majorloo__1110818C68487DD7 ON majorlookup(	[major] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__majorloo__EF7DE1636B24EA82')BEGINCREATE UNIQUE INDEX UQ__majorloo__EF7DE1636B24EA82 ON majorlookup(	[lookupid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__MethodEr__3214EC0708D548FA')BEGINCREATE UNIQUE CLUSTERED INDEX PK__MethodEr__3214EC0708D548FA ON MethodErrors(	[Id] ASC )  ON [PRIMARY]END 
GO
 
 
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_onetskills')BEGINCREATE UNIQUE CLUSTERED INDEX PK_onetskills ON onetskills(			[onetcode] ASC , [skill] ASC , [skilltype] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__PageErro__3214EC077F4BDEC0')BEGINCREATE UNIQUE CLUSTERED INDEX PK__PageErro__3214EC077F4BDEC0 ON PageErrors(	[Id] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__posting__48B1085C49AEE81E')BEGINCREATE UNIQUE CLUSTERED INDEX PK__posting__48B1085C49AEE81E ON posting(	[postingid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__posting__68DCDD3B41B8C09B')BEGINCREATE UNIQUE INDEX UQ__posting__68DCDD3B41B8C09B ON posting(	[talentjobid] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_posting_spideredjobreferrals')BEGINCREATE INDEX IX_posting_spideredjobreferrals ON posting(	[eosregistrationid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__postingi__48B1085C6EE06CCD')BEGINCREATE UNIQUE CLUSTERED INDEX PK__postingi__48B1085C6EE06CCD ON postinginfo(	[postingid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__probleme__AB6E61655EBF139D')BEGINCREATE UNIQUE CLUSTERED INDEX PK__probleme__AB6E61655EBF139D ON problememailaddresses(	[email] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__probleme__3213E83E619B8048')BEGINCREATE UNIQUE INDEX UQ__probleme__3213E83E619B8048 ON problememailaddresses(	[id] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__ProfileE__3214EC0777AABCF8')BEGINCREATE UNIQUE CLUSTERED INDEX PK__ProfileE__3214EC0777AABCF8 ON ProfileEvents(	[Id] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__recently__BCF51D5A1209AD79')BEGINCREATE UNIQUE CLUSTERED INDEX PK__recently__BCF51D5A1209AD79 ON recentlyviewedjobs(		[focusjobid] ASC , [jobseekerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__recently__D6711F4914E61A24')BEGINCREATE UNIQUE INDEX UQ__recently__D6711F4914E61A24 ON recentlyviewedjobs(	[viewedjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_recentlyviewedjobs_jobseekerid_focusjobid_viewedtime')BEGINCREATE INDEX IX_recentlyviewedjobs_jobseekerid_focusjobid_viewedtime ON recentlyviewedjobs(			[jobseekerid] ASC , [focusjobid] ASC , [viewedtime] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__referral__2041BB51369C13AA')BEGINCREATE UNIQUE CLUSTERED INDEX PK__referral__2041BB51369C13AA ON referralnotes(	[referralnoteid] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__referral__FFF0EF100E391C95')BEGINCREATE UNIQUE INDEX UQ__referral__FFF0EF100E391C95 ON referrals(		[jobseekerid] ASC , [focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_referrals_referreddate')BEGINCREATE INDEX IX_referrals_referreddate ON referrals(	[referreddate] ASC )  INCLUDE ( [referralid],[jobseekerid],[focusjobid],[score] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_Referrals')BEGINCREATE INDEX IX_Referrals ON referrals(							[referralid] ASC , [jobseekerid] ASC , [focusjobid] ASC , [staffid] ASC , [referreddate] ASC , [status] ASC , [score] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_migration_referrals_spideredjobs1')BEGINCREATE INDEX IX_migration_referrals_spideredjobs1 ON referrals(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_Migration_SpideredJobs')BEGINCREATE INDEX IX_Migration_SpideredJobs ON referrals(	[focusjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__resume__C2E77C40078C1F06')BEGINCREATE UNIQUE CLUSTERED INDEX PK__resume__C2E77C40078C1F06 ON resume(	[resumeid] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__resume__C2E77C410A688BB1')BEGINCREATE UNIQUE INDEX UQ__resume__C2E77C410A688BB1 ON resume(	[resumeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_resume_status')BEGINCREATE INDEX IX_resume_status ON resume(	[status] ASC )  INCLUDE ( [resumeid] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__resumeal__60429F3415FA39EE')BEGINCREATE UNIQUE CLUSTERED INDEX PK__resumeal__60429F3415FA39EE ON resumealerts(		[employerrepid] ASC , [alertname] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__resumebu__56E9D23F7C4F7684')BEGINCREATE UNIQUE CLUSTERED INDEX PK__resumebu__56E9D23F7C4F7684 ON resumebuilderquestions(	[rbqid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__resumebu__56E9D23E7F2BE32F')BEGINCREATE UNIQUE INDEX UQ__resumebu__56E9D23E7F2BE32F ON resumebuilderquestions(	[rbqid] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__resumecu__E90BBE4E2FEF161B')BEGINCREATE UNIQUE CLUSTERED INDEX PK__resumecu__E90BBE4E2FEF161B ON resumecustomfilterlookup(	[lenscustomfilter] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__resumecu__EF7DE16332CB82C6')BEGINCREATE UNIQUE INDEX UQ__resumecu__EF7DE16332CB82C6 ON resumecustomfilterlookup(	[lookupid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__resumein__C2E77C403BFFE745')BEGINCREATE UNIQUE CLUSTERED INDEX PK__resumein__C2E77C403BFFE745 ON resumeinfo(	[resumeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__resumein__FD2B37F73EDC53F0')BEGINCREATE UNIQUE INDEX UQ__resumein__FD2B37F73EDC53F0 ON resumeinfo(	[resumeinfoid] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__savedjob__BCF51D5A1C873BEC')BEGINCREATE UNIQUE CLUSTERED INDEX PK__savedjob__BCF51D5A1C873BEC ON savedjobs(		[focusjobid] ASC , [jobseekerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__savedjob__371116151F63A897')BEGINCREATE UNIQUE INDEX UQ__savedjob__371116151F63A897 ON savedjobs(	[savedjobid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_savedjobs_jobseekerid')BEGINCREATE INDEX IX_savedjobs_jobseekerid ON savedjobs(	[jobseekerid] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__savednot__8990797818B6AB08')BEGINCREATE UNIQUE INDEX UQ__savednot__8990797818B6AB08 ON savednotes(	[savednotesid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__savedres__DD360F0B02925FBF')BEGINCREATE UNIQUE CLUSTERED INDEX PK__savedres__DD360F0B02925FBF ON savedresumesearchdetails(	[savedsearchid] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__savedres__67109363318258D2')BEGINCREATE UNIQUE CLUSTERED INDEX PK__savedres__67109363318258D2 ON savedresumesearches(		[savedsearchid] ASC , [employerrepid] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__savedres__DD360F0A345EC57D')BEGINCREATE UNIQUE INDEX UQ__savedres__DD360F0A345EC57D ON savedresumesearches(	[savedsearchid] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__savedsea__DD360F0B373B3228')BEGINCREATE UNIQUE CLUSTERED INDEX PK__savedsea__DD360F0B373B3228 ON savedsearchdetails(	[savedsearchid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__savedsea__665F25CE29E1370A')BEGINCREATE UNIQUE CLUSTERED INDEX PK__savedsea__665F25CE29E1370A ON savedsearches(		[jobseekerid] ASC , [searchname] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__savedsea__DD360F0A2CBDA3B5')BEGINCREATE UNIQUE INDEX UQ__savedsea__DD360F0A2CBDA3B5 ON savedsearches(	[savedsearchid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_savedsearches_jobseekerid_searchname')BEGINCREATE INDEX IX_savedsearches_jobseekerid_searchname ON savedsearches(		[jobseekerid] ASC , [searchname] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__session__162997532334397B')BEGINCREATE UNIQUE CLUSTERED INDEX PK__session__162997532334397B ON session(	[jobseekerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__session__162997522610A626')BEGINCREATE UNIQUE INDEX UQ__session__162997522610A626 ON session(	[jobseekerid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_session_authenticationcode')BEGINCREATE INDEX IX_session_authenticationcode ON session(	[authenticationcode] ASC )  INCLUDE ( [jobseekerid],[lastrequestreceived] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__skillcat__AB9019351CF15040')BEGINCREATE UNIQUE CLUSTERED INDEX PK__skillcat__AB9019351CF15040 ON skillcategory(	[categorycode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__skillcat__AB9019341FCDBCEB')BEGINCREATE UNIQUE INDEX UQ__skillcat__AB9019341FCDBCEB ON skillcategory(	[categorycode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__skillloo__3B9025B622751F6C')BEGINCREATE UNIQUE CLUSTERED INDEX PK__skillloo__3B9025B622751F6C ON skilllookup(			[skillname] ASC , [skilltype] ASC , [skillcategory] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__skillloo__AE6B6FE625518C17')BEGINCREATE UNIQUE INDEX UQ__skillloo__AE6B6FE625518C17 ON skilllookup(	[skillid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_skilllokkup_noiseskill')BEGINCREATE INDEX IX_skilllokkup_noiseskill ON skilllookup(	[noiseskill] ASC )  INCLUDE ( [skillname],[skilltype] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__skilltyp__86BE2DBC398D8EEE')BEGINCREATE UNIQUE CLUSTERED INDEX PK__skilltyp__86BE2DBC398D8EEE ON skilltype(	[typecode] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_srch_exact_phrase_equivalent')BEGINCREATE CLUSTERED INDEX IX_srch_exact_phrase_equivalent ON srch_exact(		[phrase] ASC , [equivalent] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK_srch_words')BEGINCREATE UNIQUE CLUSTERED INDEX PK_srch_words ON srch_words(	[item_id] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__staff__F3DBC57354B68676')BEGINCREATE UNIQUE CLUSTERED INDEX PK__staff__F3DBC57354B68676 ON staff(	[username] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__staffoff__A57430D750E5F592')BEGINCREATE UNIQUE CLUSTERED INDEX PK__staffoff__A57430D750E5F592 ON staffoffice(	[officeid] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__stateloo__B00B6A502E1BDC42')BEGINCREATE UNIQUE CLUSTERED INDEX PK__stateloo__B00B6A502E1BDC42 ON statelookup(		[statecode] ASC , [countryid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__stateloo__EF7DE16330F848ED')BEGINCREATE UNIQUE INDEX UQ__stateloo__EF7DE16330F848ED ON statelookup(	[lookupid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__taggedpo__48B1085C6B0FDBE9')BEGINCREATE UNIQUE CLUSTERED INDEX PK__taggedpo__48B1085C6B0FDBE9 ON taggedpostings(	[postingid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__taggedre__C2E77C40308E3499')BEGINCREATE UNIQUE CLUSTERED INDEX PK__taggedre__C2E77C40308E3499 ON taggedresumes(	[resumeid] ASC )  WITH (FILLFACTOR = 80,  PAD_INDEX = OFF,  ALLOW_PAGE_LOCKS = OFF,  ALLOW_ROW_LOCKS = OFF,  STATISTICS_NORECOMPUTE = OFF,  DROP_EXISTING = ON )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__taggedre__C2E77C41336AA144')BEGINCREATE UNIQUE INDEX UQ__taggedre__C2E77C41336AA144 ON taggedresumes(	[resumeid] ASC )  ON [PRIMARY]END 
GO 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__titleloo__E52A1BB207F6335A')BEGINCREATE UNIQUE CLUSTERED INDEX PK__titleloo__E52A1BB207F6335A ON titlelookup(	[title] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__titleloo__EF7DE1630AD2A005')BEGINCREATE UNIQUE INDEX UQ__titleloo__EF7DE1630AD2A005 ON titlelookup(	[lookupid] ASC )  ON [PRIMARY]END 
GO
 
 
 
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'IX_toolstechnology_ONetSOCCode')BEGINCREATE INDEX IX_toolstechnology_ONetSOCCode ON toolstechnology(	[ONetSOCCode] ASC )  INCLUDE ( [CommodityTitle] )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__transact__9B52C2FB29221CFB')BEGINCREATE UNIQUE INDEX UQ__transact__9B52C2FB29221CFB ON transactionlog(	[transactionid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__transact__CAE9BABA164452B1')BEGINCREATE UNIQUE CLUSTERED INDEX PK__transact__CAE9BABA164452B1 ON transactiontypes(	[transactiontype] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__transact__4B98A7DB1920BF5C')BEGINCREATE UNIQUE INDEX UQ__transact__4B98A7DB1920BF5C ON transactiontypes(	[transactiontypeid] ASC )  ON [PRIMARY]END 
GO
 
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__US_resum__56E9D23F10766AC2')BEGINCREATE UNIQUE CLUSTERED INDEX PK__US_resum__56E9D23F10766AC2 ON US_resumebuilderquestions(	[rbqid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__US_resum__56E9D23E1352D76D')BEGINCREATE UNIQUE INDEX UQ__US_resum__56E9D23E1352D76D ON US_resumebuilderquestions(	[rbqid] ASC )  ON [PRIMARY]END 
GO
 
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__users__F3DBC5733C54ED00')BEGINCREATE UNIQUE CLUSTERED INDEX PK__users__F3DBC5733C54ED00 ON users(	[username] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__users__CBA1B2563F3159AB')BEGINCREATE UNIQUE INDEX UQ__users__CBA1B2563F3159AB ON users(	[userid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__usersess__CBA1B2574301EA8F')BEGINCREATE UNIQUE CLUSTERED INDEX PK__usersess__CBA1B2574301EA8F ON usersession(	[userid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__usertype__10C7F78A00551192')BEGINCREATE UNIQUE CLUSTERED INDEX PK__usertype__10C7F78A00551192 ON usertypes(	[usertype] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__usertype__A1CB004C03317E3D')BEGINCREATE UNIQUE INDEX UQ__usertype__A1CB004C03317E3D ON usertypes(	[usertypeid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'PK__vendorlo__7AE12B512EDAF651')BEGINCREATE UNIQUE CLUSTERED INDEX PK__vendorlo__7AE12B512EDAF651 ON vendorlookup(		[customerid] ASC , [vendorname] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'UQ__vendorlo__EF7DE16331B762FC')BEGINCREATE UNIQUE INDEX UQ__vendorlo__EF7DE16331B762FC ON vendorlookup(	[lookupid] ASC )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_migrations_activities')BEGINCREATE INDEX ix_migrations_activities ON activities(			[customeractivityid] ASC , [activityid] ASC , [activitytime] ASC )  INCLUDE ( [jobseekerid],[customerrepid],[addedby] )  ON [PRIMARY]END 
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_migrations_activities')BEGINCREATE INDEX ix_migrations_activities ON activities(			[customeractivityid] ASC , [activityid] ASC , [activitytime] ASC )  INCLUDE ( [jobseekerid],[customerrepid],[addedby] )  ON [PRIMARY]END 
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_migrations_employerrepdetails')BEGINCREATE INDEX ix_migrations_employerrepdetails ON employerrepdetails(	[employerid] ASC )  INCLUDE ( [employerrepid],[street1],[street2],[city],[state],[county],[country],[postalcode],[lastmodified] )  ON [PRIMARY]END 
GO
IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_migrations_jobsviewed')BEGINCREATE INDEX ix_migrations_jobsviewed ON jobsviewed(	[viewedid] ASC )  INCLUDE ( [jobseekerid],[focusjobid],[viewedtime] )  ON [PRIMARY]END 
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_migrations_jobsviewed2')BEGINCREATE INDEX ix_migrations_jobsviewed2 ON jobsviewed(		[jobseekerid] ASC , [focusjobid] ASC )  INCLUDE ( [viewedid],[viewedtime] )  ON [PRIMARY]END 
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_migrations_posting')BEGINCREATE INDEX ix_migrations_posting ON posting(	[eosregistrationid] ASC )  INCLUDE ( [postingid] )  ON [PRIMARY]END 
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_migrations_resume')BEGINCREATE INDEX ix_migrations_resume ON resume(	[jobseekerid] ASC )  INCLUDE ( [resumeid] )  ON [PRIMARY]END 
GO

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES WHERE name = 'ix_migrations_resume2')BEGINCREATE INDEX ix_migrations_resume2 ON resume(			[active] ASC , [resumeid] ASC , [modifieddate] ASC )  INCLUDE ( [jobseekerid],[registereddate] )  ON [PRIMARY]END 
GO