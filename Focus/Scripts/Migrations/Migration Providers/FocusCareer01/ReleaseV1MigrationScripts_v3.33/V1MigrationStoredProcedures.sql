IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_BusinessUnitDescriptionRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_BusinessUnitDescriptionRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_BusinessUnitDescriptionRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30)
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.EmployerDescriptions

SELECT TOP (@BatchSize)
	ED.DescriptionId,
	ED.DescriptionName AS Title,
    LEFT(ISNULL(CAST(ED.[Description] AS VARCHAR(MAX)), ''), 2000) AS [Description],
    CAST(0 AS BIT) AS IsPrimary,
    CAST(ED.DescriptionId AS VARCHAR(10)) AS MigrationId,
	CAST(MIN(ERD.EmployerRepId) AS VARCHAR(10)) AS EmployeeMigrationId
FROM
	dbo.EmployerDescriptions ED
INNER JOIN dbo.Employer E
	ON E.EmployerID = ED.EmployerID
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerId
INNER JOIN dbo.EmployerRepDetails ERD
	ON ERD.EmployerId = E.EmployerId
INNER JOIN dbo.Users U
	ON U.UserID = ERD.UserID	
WHERE
	C.[name] = @CustomerId
	AND ED.IsDefault = 0
	AND DATALENGTH(ED.[Description]) > 0
	AND ED.DescriptionId > @LastId
GROUP BY
	ED.DescriptionId,
	ED.DescriptionName,
	CAST(ED.[Description] AS VARCHAR(MAX))
ORDER BY
	ED.DescriptionId
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_BusinessUnitLogoRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_BusinessUnitLogoRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_BusinessUnitLogoRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30)
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.EmployerLogos

;WITH Logos AS
(
	SELECT
		EL.LogoId,
		ROW_NUMBER() OVER (PARTITION BY EL.EmployerID ORDER BY EL.LogoId) AS LogoNumber,
		MIN(ERD.EmployerRepId) AS EmployeeMigrationId
	FROM
		dbo.EmployerLogos EL
	INNER JOIN dbo.Employer E
		ON E.EmployerID = EL.EmployerID
	INNER JOIN dbo.Customer C
		ON C.CustomerID = E.CustomerId
	INNER JOIN dbo.EmployerRepDetails ERD
		ON ERD.EmployerId = E.EmployerId
	WHERE
		C.[name] = @CustomerId
	GROUP BY
		EL.LogoId,
		EL.EmployerID
)
SELECT TOP (@BatchSize)
	EL.LogoId,
	CASE EL.LogoName WHEN '' THEN 'Logo ' + CAST(TL.LogoNumber AS NVARCHAR(10)) ELSE EL.logoname END AS [Name],
	EL.Logo,
    CAST(TL.LogoId AS VARCHAR(10)) AS MigrationId,
	CAST(TL.EmployeeMigrationId AS VARCHAR(10)) AS EmployeeMigrationId
FROM Logos TL
INNER JOIN dbo.EmployerLogos EL
	ON EL.LogoId = TL.LogoId
WHERE
	EL.LogoId > @LastId
ORDER BY
	EL.LogoId

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_DeleteEmployerTradeNamesMigrationTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_DeleteEmployerTradeNamesMigrationTable]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Migrations_DeleteEmployerTradeNamesMigrationTable]
AS
BEGIN
	SET NOCOUNT ON;

	DROP TABLE EmployerTradeNamesMigration;
	
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_EmployeeResumeAlertRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_EmployeeResumeAlertRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_EmployeeResumeAlertRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.savedsearches
	
SELECT TOP (@BatchSize)
	RA.alertname AS [Name],
	RA.criteria AS CriteriaXml,
	RA.alertinterval AS Frequency,
	RA.AlertMailFormat AS [Format],
	RA.email AS EmailAddress,
	CAST(RA.resumealertid AS VARCHAR(10)) AS MigrationId,
	CAST(RA.employerrepid AS VARCHAR(10)) AS EmployeeMigrationId,
	RA.resumealertid AS SearchAlertId
FROM
	dbo.resumealerts RA
INNER JOIN dbo.employerrepdetails erd
	ON erd.employerrepid = RA.employerrepid
INNER JOIN dbo.employer E
	ON E.employerid = erd.employerid
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerID
WHERE
	C.Name = @CustomerId
	AND RA.resumealertid > @LastId
	AND (@CutOffDate IS NULL OR RA.modifieddate > @CutOffDate)
	AND RA.[status] = 1
	AND DATALENGTH(RA.Criteria) > 0
ORDER BY
	RA.resumealertid
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_EmployeeResumeSearchRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_EmployeeResumeSearchRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_EmployeeResumeSearchRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.savedsearches
	
--IF @CutOffDate IS NULL
--	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())

SELECT TOP (@BatchSize)
	srs.searchname AS [Name],
	srsd.searchcriteria AS CriteriaXml,
	CAST(srs.savedsearchid AS VARCHAR(10)) AS MigrationId,
	CAST(srs.employerrepid AS VARCHAR(10)) AS EmployeeMigrationId,
	srs.savedsearchid AS SavedSearchId
FROM
	dbo.savedresumesearches srs
INNER JOIN dbo.savedresumesearchdetails srsd
	ON srsd.savedsearchid = srs.savedsearchid
INNER JOIN dbo.employerrepdetails erd
	ON erd.employerrepid = srs.employerrepid
INNER JOIN dbo.employer e
	ON e.employerid = erd.employerid
INNER JOIN Customer C
	ON C.CustomerID = e.CustomerID
WHERE
	C.Name = @CustomerId
	AND srs.savedsearchid > @LastId
	AND (@CutOffDate IS NULL OR srsd.savedsearchtime > @CutOffDate)
	AND DATALENGTH(srsd.searchcriteria) > 0
ORDER BY
	srs.savedsearchid
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_EmployerRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_EmployerRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_EmployerRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.EmployerRepDetails
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

SELECT TOP (@BatchSize)
	ERD.EmployerRepId,
	U.UserName AS UserName,
	U.Passcode AS Passcode,
	ERD.PersonalTitle AS EmployeePersonTitle,
	ERD.FirstName AS EmployeePersonFirstName,
	ISNULL(LEFT(ERD.MiddleName, 1), '') AS EmployeePersonMiddleInitial,
	ERD.LastName AS EmployeePersonLastName,
	ISNULL(ERD.Designation, '') AS EmployeePersonJobTitle,
	ERD.Street1 AS EmployeeAddressLine1,
	ISNULL(ERD.Street2, '') AS EmployeeAddressLine2,
	ERD.City AS EmployeeAddressTownCity,
	ISNULL(ERD.County, '') AS EmployeeAddressCounty,
	ERD.[State] AS EmployeeAddressState,
	ERD.Country  AS EmployeeAddressCountry,
	ERD.PostalCode AS EmployeeAddressPostcodeZip,
	SUBSTRING(ERD.Phone1, 2, 20) AS EmployeePrimaryPhone,
	LEFT(ERD.Phone1, 1) AS EmployeePrimaryPhoneType,
	SUBSTRING(ERD.Phone2, 2, 20) AS EmployeeAlternatePhone1,
	LEFT(ERD.Phone2, 1) AS EmployeeAlternatePhoneType1,
	SUBSTRING(ERD.Phone3, 2, 20) AS EmployeeAlternatePhone2,
	LEFT(ERD.Phone3, 1) AS EmployeeAlternatePhoneType2,
	ERD.EmailAddress AS EmployeePersonEmailAddress,
	ERD.TimeCreated AS EmployeeCreatedOn,
	ERD.LastModified AS EmployeeUpdatedOn,
	E.LegalName AS EmployerName,
	ISNULL(ED.DescriptionId, 0 - E.employerid) AS EmployerDescriptionId,
	SUBSTRING(ISNULL(ED.[Description], ''), 1, 2000) AS EmployerDescription,
	ISNULL(ED.DescriptionName, 'Company Description') AS EmployerDescriptionTitle,
	E.[Status] AS EmployerStatus,
	E.Fein AS FederalEmployerIdentificationNumber,
	ISNULL(E.Sein, '') AS StateEmployerIdentificationNumber,
	E.Url AS EmployerUrl,
	E.OwnershipType AS EmployerOwnershipType,
	SUBSTRING(E.Phone1, 2, 20) AS EmployerPrimaryPhone,
	LEFT(E.Phone1, 1) AS EmployerPrimaryPhoneType,
	SUBSTRING(E.Phone2, 2, 20) AS EmployerAlternatePhone1,
	LEFT(E.Phone2, 1) AS EmployerAlternatePhoneType1,
	SUBSTRING(E.Phone3, 2, 20) AS EmployerAlternatePhone2,
	LEFT(E.Phone3, 1) AS EmployerAlternatePhoneType2,
	E.IndustryClass AS EmployerIndustrialClassification,
	E.TimeCreated AS EmployerCreatedOn,
	E.LastModified AS EmployerUpdatedOn,
	E.Street1 AS EmployerAddressLine1,
	ISNULL(E.Street2, '') AS EmployerAddressLine2,
	E.City AS EmployerAddressTownCity,
	ISNULL(E.County, '') AS EmployerAddressCounty,
	E.[State] AS EmployerAddressState,
	E.Country  AS EmployerAddressCountry,
	E.PostalCode AS EmployerAddressPostcodeZip,
	--E.EmployerId AS EmployerExternalId,
	E.customerregistrationid AS EmployerExternalId,
	ERD.customerregistrationid AS EmployeeExternalId,
	CAST(ERD.EmployerRepId AS VARCHAR(10)) AS MigrationId,
	E.timecreated as EmployerCreatedOn,
	E.lastmodified as EmployerUpdatedOn,
	ERD.timecreated as EmployeeCreatedOn,
	ERD.lastmodified as EmployeeUpdatedOn
FROM
	dbo.Employer E
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerId	
INNER JOIN dbo.EmployerRepDetails ERD
	ON ERD.EmployerID = E.EmployerID
INNER JOIN dbo.Users U
	ON U.UserID = ERD.UserID
LEFT OUTER JOIN dbo.EmployerDescriptions ED
	ON ED.EmployerID = E.EmployerID
	AND ED.IsDefault = 1
	AND DATALENGTH(ED.[Description]) > 0
WHERE
	C.[name] = @CustomerId
	AND ERD.EmployerRepId > @LastId
	AND (E.LastModified > @CutOffDate OR ERD.LastModified > @CutOffDate)
ORDER BY
	ERD.EmployerRepId
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_FlaggedJobSeekers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_FlaggedJobSeekers]
GO

CREATE PROCEDURE [dbo].[Migrations_FlaggedJobSeekers]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.ApplicantData
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())

SELECT TOP (@BatchSize) 
	AD.ApplicantId,
	CAST(AD.ApplicantId AS VARCHAR(10)) AS MigrationId,
	CAST(AD.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CAST(ERD.EmployerRepId AS VARCHAR(10)) AS EmployeeeMigrationId
FROM 
	dbo.ApplicantData AD
INNER JOIN dbo.JobSeeker JS
	ON JS.JobSeekerId = AD.JobSeekerId
INNER JOIN dbo.EmployerRepDetails ERD
	ON ERD.EmployerRepId = AD.EmployerRepId
INNER JOIN dbo.Employer E
	ON E.EmployerId = ERD.EmployerId
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerId	
WHERE
	C.[name] = @CustomerId
	AND AD.ApplicantId > @LastId
	AND JS.EmailAddress LIKE '%@%'
	AND JS.LastLogin > @CutOffDate
	AND AD.Flagged = 1
ORDER BY
	AD.ApplicantId
GO

/****** Object:  StoredProcedure [dbo].[Migrations_GetPagedEmployerTradeNames]    Script Date: 01/13/2015 10:08:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_GetPagedEmployerTradeNames]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_GetPagedEmployerTradeNames]
GO

/****** Object:  StoredProcedure [dbo].[Migrations_GetPagedEmployerTradeNames]    Script Date: 01/13/2015 10:08:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_GetPagedEmployerTradeNames]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME
AS
BEGIN
	SET NOCOUNT ON;

--IF(@LastId is null OR @LastId = 0)
--BEGIN
--	SELECT @LastId = MIN(TradeNameId) FROM EmployerTradeNamesMigration
--END

DECLARE @StartRowNumber int;
DECLARE @EndRowNumber int;

SELECT	@StartRowNumber = COUNT('X') + 1
FROM	EmployerTradeNamesMigration 
WHERE	TradeNameId <= @LastId

SELECT	@EndRowNumber = @StartRowNumber + @BatchSize

SELECT		TradeNameId,
			TradeName,
			IsDefault,
			EmployerId,
			LegalName,
			EmployeeId,
			Fein,
			Street1,
			Street2,
			City,
			State,
			County,
			Country,
			PostalCode
FROM 
(
	SELECT 	TradeNameId,
			TradeName,
			IsDefault,
			EmployerId,
			LegalName,
			EmployeeId,
			Fein,
			Street1,
			Street2,
			City,
			State,
			County,
			Country,
			PostalCode, 
			ROW_NUMBER() OVER (ORDER BY TradeNameId) AS RowNumber
	FROM	EmployerTradeNamesMigration
) AS NumberedEmployerTradeNames
WHERE RowNumber 
BETWEEN @StartRowNumber 
AND @EndRowNumber
END


GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobOrderRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobOrderRequests]
GO

CREATE PROCEDURE [Migrations_JobOrderRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Posting
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

SELECT TOP (@BatchSize)
	P.PostingID,
	P.EmployerId,
	ISNULL(P.[status], 0) AS [Status],
	I.JobTitle,
	I.[Description],
	P.DateCreated AS CreatedOn,
	P.LastModified AS UpdatedOn,
	P.DateCreated AS PostedOn,
	I.CloseDate AS ClosingOn,
	P.LastModified AS ClosedOn,
	I.Positions AS NumberOfOpenings,
	P.LastModified AS ApprovedOn,
	I.[Address] AS JobAddressLine1,
	I.City AS JobAddressTownCity,
	I.[State] AS JobAddressState,
	I.zipcode AS JobAddressPostcodeZip,
	CASE
		WHEN I.City = E.City AND I.[State] = E.[State] AND I.ZipCode = E.postalcode THEN 1
		ELSE 0
	END AS IsMainSite,
	TP.Posting AS JobXml,
	E.[Status] AS EmployerStatus,
	ISNULL(J.LensJobId, '') AS LensJobId,
	P.eosregistrationid AS ExternalId,
	P.EmployerRepId AS EmployeeMigrationId,
	P.PostingId AS JobMigrationId
FROM
	dbo.Posting P
INNER JOIN dbo.PostingInfo I
	ON I.PostingID = P.PostingID
INNER JOIN dbo.Employer E
	ON E.EmployerID = P.EmployerID
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerId
INNER JOIN dbo.TaggedPostings TP
	ON TP.PostingId = P.PostingId
LEFT OUTER JOIN dbo.Jobs J
	ON J.CustomerJobId = P.EOSRegistrationId
WHERE
	C.[name] = @CustomerId
	AND P.PostingId > @LastId
	AND P.[status] <> 1791 -- DELETED
	AND P.LastModified > @CutOffDate
	AND P.EmployerRepId IS NOT NULL
	AND NOT EXISTS(SELECT 1 FROM dbo.Jobs J2 WHERE J2.CustomerJobId = P.EOSRegistrationId AND J.focusjobid < J2.focusjobid)
ORDER BY
	P.PostingId
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerActivities]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerActivities]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerActivities]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@EOS VARCHAR(30) = NULL,
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
AS

IF @LastId IS NULL
	SET @LastId = 0
	
SET @QueryLastId = @LastId	

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Activities
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId	

DECLARE @DateFrom DATETIME
SET @DateFrom = COALESCE(@CutOffDate, DATEADD(YEAR, -100, GETDATE()))
	
select top (@BatchSize) *
from (	
SELECT top (@BatchSize)  
	A.ActivityId,
	A.CustomerActivityId,
	A.ActivityTime,
	A.CustomerRepId,
	A.AddedBy,
	CAST(A.ActivityId AS VARCHAR(10)) AS MigrationId,
	CAST(A.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId
FROM 
	dbo.Activities A
INNER JOIN dbo.JobSeeker JS	ON JS.JobSeekerId = A.JobSeekerId
INNER JOIN dbo.CustomerRepDetails CRD ON CRD.CustomerRepId = JS.CustomerRepId
WHERE
	
	CRD.customerid = @CustomerKey
	AND A.ActivityId > @QueryLastId
	AND A.activitytime > @DateFrom
	AND A.CUSTOMERACTIVITYID <> 'SMRY01'
	AND A.CUSTOMERACTIVITYID <> 'UNC01'
	AND A.CUSTOMERACTIVITYID <> 'PCW01'
	AND A.CUSTOMERACTIVITYID <> 'SQA01'
	AND A.CUSTOMERACTIVITYID <> 'SSNC01'
	AND A.CUSTOMERACTIVITYID <> 'STSC01'
	AND A.CUSTOMERACTIVITYID <> 'REF01'	
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			dbo.activities A2
		WHERE
			A2.jobseekerid = A.jobseekerid 
			AND A2.customeractivityid = A.customeractivityid
			AND A2.activitytime > A.ActivityTime
			AND A2.activitytime < DATEADD(DAY, 1, CONVERT(DATE, A.ActivityTime))
	)
	union
	select		top (@BatchSize)  
				A.ActivityId,
				A.CustomerActivityId,
				A.ActivityTime,
				A.CustomerRepId,
				A.AddedBy,
				CAST(A.ActivityId AS VARCHAR(10)) AS MigrationId,
				CAST(A.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId
	from		activities a 
	INNER JOIN	dbo.JobSeeker JS			ON JS.JobSeekerId = A.JobSeekerId
	INNER JOIN	dbo.CustomerRepDetails CRD	ON CRD.CustomerRepId = JS.CustomerRepId
	where		A.CUSTOMERACTIVITYID = 'SMRY01'
	AND			a.activitytime = (select Max(activitytime) from activities a2 where a2.jobseekerid = a.jobseekerid group by jobseekerid)
	AND	CRD.customerid = @CustomerKey
	AND A.ActivityId > @QueryLastId
	AND A.activitytime > @DateFrom
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			dbo.activities A2
		WHERE
			A2.jobseekerid = A.jobseekerid 
			AND A2.customeractivityid = A.customeractivityid
			AND A2.activitytime > A.ActivityTime
			AND A2.activitytime < DATEADD(DAY, 1, CONVERT(DATE, A.ActivityTime))
	)
) as activityUnion
order by	activityUnion.activityid



GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerNoteReminderRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerNoteReminderRequests]
GO

CREATE PROCEDURE [Migrations_JobSeekerNoteReminderRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.SavedNotes

IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
		
SELECT TOP (@BatchSize)
	CASE
		WHEN SN.Reminder = 0 THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS IsNote,
	SN.Notes,
	CASE 
		WHEN SN.RemindBy IN (2, 3) THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS ReminderViaDashboard,
	CASE 
		WHEN SN.RemindBy IN (1, 3) THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS ReminderViaEmail,
	SN.ReminderDate,
	CASE 
		WHEN SN.RemindTo IN (1, 3) THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS ReminderToJobSeeker,
	CASE 
		WHEN SN.RemindTo IN (2, 3) THEN CAST(1 AS BIT)
		ELSE CAST(0 AS BIT)
	END AS ReminderToStaff,
	SN.CreatedDate AS CreatedDate,
	CAST(SN.savednotesid AS VARCHAR(10)) AS MigrationId,
	CAST(SN.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	SN.staffID AS StaffMigrationId,
	SN.savednotesid AS SavedNotesId
FROM
	dbo.SavedNotes SN
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = SN.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	SN.savednotesid > @LastId
	AND C.Name = @CustomerId
	AND SN.CreatedDate > @CutOffDate
ORDER BY
	SN.savednotesid
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0

SET @QueryLastId = @LastId

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.JobSeeker
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -20, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId

SELECT TOP (@BatchSize)
	JS.username AS UserName,
	JS.passcode AS Passcode,
	JS.emailaddress AS EmailAddress,
	ISNULL(JS.SSN, '') AS SSN,
	ISNULL(JS.securityquestion, '') AS SecurityQuestion,
	ISNULL(JS.securityanswer, '') AS SecurityAnswer,
	ISNULL(JS.Reserved02, '') AS ExtraInfo,
	JS.lastlogin,
	JS.RegisteredDate,
	TR.[Resume] AS [Resume],
	JS.customerregistrationid AS ExternalJobSeekerId,
	CAST(JS.jobseekerid AS VARCHAR(10)) AS MigrationId,
	JS.jobseekerid AS JobSeekerId
FROM
	dbo.JobSeeker JS --WITH (FORCESEEK)
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
LEFT OUTER JOIN
( 
	dbo.[Resume] R --WITH (INDEX (IX_resume_jobseekerid))
	INNER JOIN dbo.TaggedResumes TR
		ON TR.ResumeId = R.ResumeId
)
	ON R.JobSeekerId = JS.JobSeekerId
WHERE
	CRD.CustomerId = @CustomerKey
	AND JS.jobseekerid > @QueryLastId
	AND JS.LastLogin > @CutOffDate
ORDER BY
	JS.jobseekerid
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerSavedSearchRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerSavedSearchRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerSavedSearchRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.savedsearches
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

SELECT TOP (@BatchSize)
	ss.searchname AS [Name],
	ssd.SavedSearchTime AS CreatedOn,
	ssd.searchcriteria AS CriteriaXml,
	CAST(SS.savedsearchid AS VARCHAR(10)) AS MigrationId,
	CAST(SS.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	SS.savedsearchid AS SavedSearchId
FROM
	dbo.savedsearches SS
INNER JOIN dbo.savedsearchdetails SSD
	ON SSD.savedsearchid = SS.savedsearchid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = SS.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.Name = @CustomerId
	AND SS.savedsearchid > @LastId
	AND JS.LastLogin > @CutOffDate
	AND ssd.savedsearchtime > @CutOffDate
	AND DATALENGTH(SSD.searchcriteria) > 0
	AND NOT EXISTS(SELECT 1 FROM jobalerts ja WHERE ja.jobseekerid = Ss.jobseekerid AND ja.alertname = SS.searchname AND JA.[status] = 1)
ORDER BY
	SS.savedsearchid
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerSearchAlertRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerSearchAlertRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerSearchAlertRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.savedsearches
	
SELECT TOP (@BatchSize)
	JA.alertname AS [Name],
	JA.SubscribedDate AS CreatedOn,
	JA.modifieddate AS UpdatedOn,
	JA.criteria AS CriteriaXml,
	JA.alertinterval AS Frequency,
	JA.AlertMailFormat AS [Format],
	JA.email AS EmailAddress,
	CAST(JA.jobalertid AS VARCHAR(10)) AS MigrationId,
	CAST(JA.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	JA.jobalertid AS SearchAlertId
FROM
	dbo.jobalerts JA WITH (FORCESEEK)
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = JA.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.Name = @CustomerId
	AND JA.jobalertid > @LastId
	AND JA.modifieddate > @CutOffDate
	AND JA.[status] = 1
	AND DATALENGTH(JA.Criteria) > 0
ORDER BY
	JA.jobalertid
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_Referrals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_Referrals]
GO

CREATE PROCEDURE [dbo].[Migrations_Referrals]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0
	
SET @QueryLastId = @LastId

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Applicants
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId

SELECT TOP (@BatchSize) 
	R.ReferralId,
	R.ReferredDate,
	A.DateApplied,
	ISNULL(A.[Score], R.[Score]) AS [Score],
	ISNULL(A.[Status], R.[Status]) AS [Status],
	R.StaffId,
	CAST(R.ReferralId AS VARCHAR(10)) AS MigrationId,
	CAST(R.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CAST(P.PostingId AS VARCHAR(10)) AS JobOrderMigrationId
FROM 
	dbo.referrals R WITH (NOLOCK FORCESEEK)
INNER JOIN dbo.JobSeeker JS WITH (NOLOCK)
	ON JS.JobSeekerId = R.JobSeekerId
INNER JOIN dbo.CustomerRepDetails CRD WITH (NOLOCK)
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN customerjobs CJ WITH (NOLOCK)
	ON CJ.FocusJobId = R.FocusJobId
INNER JOIN dbo.posting P WITH (NOLOCK)
	ON P.EOSRegistrationId = CJ.CustomerJobId 
LEFT OUTER JOIN dbo.applicants A WITH (NOLOCK)
	ON A.jobseekerid = JS.jobseekerid
	AND A.postingid = P.postingid
WHERE
	CRD.customerid = @CustomerKey
	AND R.ReferralId > @QueryLastId
	AND R.referreddate >= @CutOffDate
ORDER BY
	R.ReferralId
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_ResumeDocumentRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_ResumeDocumentRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_ResumeDocumentRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.[Resume]
	
--IF @CutOffDate IS NULL
--	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

SELECT TOP (@BatchSize)
	'Resume' + ISNULL(BR.fileextension, '.html') AS ResumeName,
	ISNULL(BR.FileExtension, '') AS FileExtension,
	BR.[resume] AS ResumeData,
	TR.[resume] AS TaggedResume,
	CAST(R.resumeid AS VARCHAR(10)) AS MigrationId,
	CAST(R.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	R.resumeid As ResumeId
FROM
	dbo.[Resume] R 
INNER JOIN dbo.taggedresumes TR
	ON TR.resumeid = R.resumeid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerID = R.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
LEFT OUTER JOIN dbo.binaryresumes BR
	ON BR.jobseekerid = R.jobseekerid
WHERE
	C.Name = @CustomerId
	AND R.ResumeId > @LastId
	AND R.active = 1
	AND DATALENGTH(TR.[Resume]) <> 0
	AND JS.EmailAddress LIKE '%@%'
	AND (@CutOffDate IS NULL OR JS.LastLogin > @CutOffDate)
ORDER BY
	R.ResumeId
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_ResumeRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_ResumeRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_ResumeRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.[Resume]
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId

SELECT TOP (@BatchSize)
	R.registereddate AS ResumeDate,
	JS.emailaddress AS EmailAddress,
	'Resume' + ISNULL(BR.fileextension, '.html') AS ResumeName,
	ISNULL(BR.FileExtension, '') AS FileExtension,
	R.registereddate,
	BR.[resume] AS ResumeData,
	TR.[resume] AS TaggedResume,
	CAST(R.resumeid AS VARCHAR(10)) AS MigrationId,
	CAST(R.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	R.resumeid As ResumeId
FROM
	dbo.[Resume] R WITH (FORCESEEK)
INNER JOIN dbo.taggedresumes TR
	ON TR.resumeid = R.resumeid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerID = R.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
LEFT OUTER JOIN dbo.binaryresumes BR
	ON BR.jobseekerid = R.jobseekerid
WHERE
	CRD.customerid = @CustomerKey
	AND R.ResumeId > @LastId
	AND R.active = 1
	AND R.modifieddate > @CutOffDate  	
	AND DATALENGTH(TR.[Resume]) <> 0
ORDER BY
	R.ResumeId
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_SavedJobs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_SavedJobs]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_SavedJobs]
	@LastId BIGINT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0
		
DECLARE @DateFrom DATETIME
SET @DateFrom = COALESCE(@CutOffDate, DATEADD(YEAR, -100, GETDATE()))

DECLARE @CustomerKey INT

SELECT @CustomerKey = CustomerId
FROM dbo.customer 
WHERE [name] = @CustomerId

SELECT TOP (@BatchSize) 
	SJ.SavedJobId,
	CJ.JobTitle,
	CAST(CJ.LensJobId AS VARCHAR(200)) AS LensPostingId,
	CAST(SJ.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CJ.FocusJobId
FROM dbo.savedjobs SJ	
INNER JOIN customerjobs CJ				ON CJ.FocusJobId = SJ.FocusJobId
INNER JOIN dbo.JobSeeker JS				ON SJ.jobseekerid = JS.JobseekerId
LEFT JOIN posting P						ON P.EOSRegistrationId = CJ.CustomerJobId
WHERE
	CJ.customerid = @CustomerKey
	AND SJ.SavedJobId > @LastId
	AND	CJ.datemodified >= @DateFrom
	AND	CJ.originid in (1,2,3,11,12,13,14,15,16,17,18,999) /* spidered jobs and ky jobs */
ORDER BY
	SJ.SavedJobId
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_SpideredJobOrderRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_SpideredJobOrderRequests]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_SpideredJobOrderRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS
BEGIN

	SET NOCOUNT ON;

IF @BatchSize IS NULL
SELECT @BatchSize = COUNT(1) 
FROM dbo.customerjobs cj 
WHERE NOT EXISTS(SELECT 1 FROM posting P WHERE P.EOSRegistrationId = CJ.CustomerJobId)
AND EXISTS(SELECT 1 FROM referrals R WHERE R.focusjobid = CJ.focusjobid)

DECLARE @DateFrom DATETIME
SET @DateFrom = COALESCE(@CutOffDate, DATEADD(YEAR, -100, GETDATE()))

DECLARE @CustomerKey INT

SELECT @CustomerKey = CustomerId
FROM dbo.customer 
WHERE [name] = @CustomerId

SELECT TOP (@BatchSize)
CJ.focusjobid,
CJ.lensjobid as LensPostingId,
CJ.JobTitle,
CJ.Employer as EmployerName,
CJ.datemodified as DateUpdated,
CJ.dateregistered as DateCreated,
CJ.originid as OriginId,
CJ.[Status]
FROM 
				customerjobs CJ			WITH (NOLOCK)
inner join		customerjobsinfo cji	on cji.focusjobid = CJ.focusjobid
inner join		jobs j					on j.focusjobid = CJ.focusjobid
left join		posting p				on p.EOSRegistrationId = CJ.CustomerJobId
WHERE			CJ.customerid = @CustomerKey
AND				CJ.focusjobid > @LastId
AND				CJ.datemodified >= @DateFrom
AND EXISTS		(SELECT 1 FROM referrals R WHERE R.focusjobid = CJ.focusjobid)
AND				CJ.originid in (1,2,3,11,12,13,14,15,16,17,18,999) /* spidered jobs and ky jobs */

ORDER BY		CJ.focusjobid
END

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_SpideredReferrals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_SpideredReferrals]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_SpideredReferrals]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Applicants
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

DECLARE @DateFrom DATETIME
SET @DateFrom = COALESCE(@CutOffDate, DATEADD(YEAR, -100, GETDATE()))

SELECT TOP (@BatchSize) 
	R.ReferralId,
	R.ReferredDate,
	R.StaffId,
	CAST(R.ReferralId AS VARCHAR(10)) AS MigrationId,
	CAST(R.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CJ.FocusJobId,
	CJ.lensjobid as LensPostingId,
	RS.ResumeId as MigrationResumeId
FROM 
	dbo.referrals R
INNER JOIN dbo.JobSeeker JS				ON JS.JobSeekerId = R.JobSeekerId
INNER JOIN dbo.CustomerRepDetails CRD	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN dbo.Customer C				ON C.CustomerID = CRD.CustomerID
INNER JOIN customerjobs CJ				ON CJ.FocusJobId = R.FocusJobId
INNER JOIN dbo.Resume RS				ON RS.JobSeekerId = JS.JobSeekerId
WHERE
	C.[name] = @CustomerId
	AND R.ReferralId > @LastId
	AND JS.EmailAddress LIKE '%@%'
	AND JS.LastLogin > @DateFrom
	AND	CJ.originid in (1,2,3,11,12,13,14,15,16,17,18,999) /* spidered jobs and ky jobs */
	AND NOT EXISTS	(SELECT 1 FROM posting P WHERE P.EOSRegistrationId = CJ.CustomerJobId)
	AND R.Status = 1
ORDER BY
	R.ReferralId

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_UserRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_UserRequests]
GO

CREATE PROCEDURE [Migrations_UserRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30)
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Staff

SELECT TOP (@BatchSize)
	S.UserName,
	S.FirstName AS UserPersonFirstName, 
	ISNULL(LEFT(S.MiddleName, 1), '') AS UserPersonMiddleInitial,
	S.LastName AS UserPersonLastName,
	'' AS UserPersonJobTitle,
	S.email AS UserPersonEmailAddress,
	ISNULL(SO.Street1, '') AS UserAddressLine1,
	ISNULL(SO.Street2, '') AS UserAddressLine2,
	ISNULL(SO.City, '') AS UserAddressTownCity,
	ISNULL(SO.PostalCode, '') AS UserAddressPostcodeZip,
	ISNULL(SO.[State], '') AS UserAddressState,
	ISNULL(SO.County, 'US') AS UserAddressCountry,
	ISNULL(NULLIF(SO.phone1, ''), 'Unspecified') AS UserPhone,
	'' AS UserPhoneExtension,
	ISNULL(SO.phone2, '') AS UserAlternatePhone,
	'' AS UserAlternatePhoneExtension,
	ISNULL(SO.Fax, '') AS UserFax,
	CAST(S.StaffID AS VARCHAR(10)) AS AccountExternalId,
	ISNULL(SO.OfficeName, 'Undefined') AS UserOffice,
	CAST(S.StaffCode AS VARCHAR(10)) AS MigrationId,
	S.StaffId
FROM
	dbo.Staff S
INNER JOIN dbo.StaffOffice SO
	ON SO.OfficeId = S.OfficeId
INNER JOIN dbo.Customer C
	ON C.CustomerID = S.CustomerID
WHERE
	C.Name = @CustomerId
	AND S.StaffId > @LastId
ORDER BY
	S.StaffId
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId]
	@LastId INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME
AS

IF @LastId IS NULL
	SET @LastId = 0
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

DECLARE @EmployerTradeNameOneEmployeeOnly TABLE(
EmployerId int NOT NULL
);

IF OBJECT_ID('EmployerTradeNamesMigration') is not null
BEGIN
		DROP TABLE EmployerTradeNamesMigration;
END

CREATE TABLE EmployerTradeNamesMigration
(
	TradeNameId int not null,
	TradeName varchar(250),
	IsDefault int,
	EmployerId int,
	LegalName varchar(250),
	EmployeeId int,
	Fein varchar(15),
	Street1 varchar(250),
	Street2 varchar(250),
	City varchar(250),
	State varchar(250),
	County varchar(250),
	Country varchar(250),
	PostalCode varchar(250)
)

INSERT INTO		@EmployerTradeNameOneEmployeeOnly
SELECT			employerid
FROM			dbo.employerrepdetails
GROUP BY		EMPLOYERID
HAVING			COUNT(EMPLOYERrepID) = 1

INSERT INTO EmployerTradeNamesMigration
		SELECT * FROM 
		(
			/* GET THE RECORDS WHERE THERE ARE 0 REPS, CAN'T TELL WHO IS THE HIRING MANAGER, DEFAULT TO THE EMPLOYER ADDRESS */
			SELECT		etn.tradenameid AS 'TradeNameId',
						etn.tradename AS 'TradeName',
						etn.isdefault AS 'IsDefault',
						e.employerid AS 'EmployerId',
						e.legalname AS 'LegalName',
						erd.employerrepid AS 'EmployeeId',
						e.fein AS 'Fein',
						e.Street1 AS 'Street1',
						e.Street2 AS 'Street2',
						e.City AS 'City',
						e.State AS 'State',
						e.County AS 'County',
						e.Country AS 'Country',
						e.PostalCode AS 'PostalCode'
			FROM		dbo.employerTradeNames etn
			INNER JOIN	dbo.employer e ON etn.employerid = e.employerid
			INNER JOIN	dbo.Customer C ON C.CustomerID = E.CustomerId	
			LEFT JOIN	dbo.employerrepdetails erd ON erd.employerid = e.employerid
			WHERE		C.[name] = @CustomerId
			AND			etn.tradenameid > @LastId
			AND			(e.LastModified > @CutOffDate OR erd.LastModified > @CutOffDate)
			AND NOT EXISTS (SELECT EMPLOYERID FROM @EmployerTradeNameOneEmployeeOnly WHERE EMPLOYERID = erd.employerid)
			AND			erd.employerrepid is null

			UNION
			/* GET THE RECORDS WHERE THERE IS >1 REP, CAN'T TELL WHO IS THE HIRING MANAGER, DEFAULT TO THE EMPLOYER ADDRESS*/
			SELECT		etn.tradenameid AS 'TradeNameId',
						etn.tradename AS 'TradeName',
						etn.isdefault AS 'IsDefault',
						e.employerid AS 'EmployerId',
						e.legalname AS 'LegalName',
						erd.employerrepid AS 'EmployeeId',
						e.fein AS 'Fein',
						e.Street1 AS 'Street1',
						e.Street2 AS 'Street2',
						e.City AS 'City',
						e.State AS 'State',
						e.County AS 'County',
						e.Country AS 'Country',
						e.PostalCode AS 'PostalCode'
			FROM		dbo.employerTradeNames etn
			INNER JOIN	dbo.employer e ON etn.employerid = e.employerid
			INNER JOIN	dbo.Customer C ON C.CustomerID = E.CustomerId	
			INNER JOIN	dbo.employerrepdetails erd ON erd.employerid = e.employerid
			WHERE		C.[name] = @CustomerId
			AND			etn.tradenameid > @LastId
			AND			(e.LastModified > @CutOffDate OR erd.LastModified > @CutOffDate)
			AND NOT EXISTS (SELECT EMPLOYERID FROM @EmployerTradeNameOneEmployeeOnly WHERE EMPLOYERID = erd.employerid)
			AND			erd.employerrepid = (select min(employerrepid) from employerrepdetails where employerid = e.employerid)
			
			UNION
			--/* GET THE RECORDS WHERE THERE IS A SINGLE REP, ASSUMED TO BE THE HIRING MANAGER */
			SELECT	 	etn.tradenameid AS 'TradeNameId',
						etn.tradename AS 'TradeName',
						etn.isdefault AS 'IsDefault',
						e.employerid AS 'EmployerId',
						e.legalname AS 'LegalName',
						erd.employerrepid AS 'EmployeeId',
						e.fein AS 'Fein',
						erd.street1 AS 'Street1',
						erd.street2 AS 'Street2',
						erd.city AS 'City',
						erd.[state] AS 'State',
						erd.county AS 'County',
						erd.country AS 'Country',
						erd.postalcode AS 'PostalCode'
			FROM		dbo.employerTradeNames etn
			INNER JOIN	dbo.employer e ON etn.employerid = e.employerid
			INNER JOIN	dbo.Customer C ON C.CustomerID = E.CustomerId	
			INNER JOIN	dbo.employerrepdetails erd ON erd.employerid = e.employerid
			INNER JOIN @EmployerTradeNameOneEmployeeOnly TEMP ON TEMP.EMPLOYERID = erd.employerid
			WHERE		C.[name] = @CustomerId
			AND			etn.tradenameid > @LastId
			AND			(E.LastModified > @CutOffDate OR erd.LastModified > @CutOffDate)
		) BusinessUnit
		ORDER BY TradeNameId

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_ViewedPostingRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_ViewedPostingRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_ViewedPostingRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.jobsviewed

IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -1, GETDATE())
	
SELECT TOP (@BatchSize)
	JV.ViewedTime AS ViewedDate,
	J.lensjobid AS LensPostingId,
	CAST(JV.ViewedId AS VARCHAR(10)) AS MigrationId,
	CAST(JV.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	JV.ViewedId AS JobsViewedId
FROM
	dbo.jobsviewed JV
INNER JOIN dbo.jobs J
	ON J.focusjobid = JV.focusjobid
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = JV.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.Name = @CustomerId
	AND JV.ViewedId > @LastId
	AND JS.LastLogin > @CutOffDate
	AND JV.ViewedId IN
	(
		SELECT TOP 5
			JV2.viewedid
		FROM 
			dbo.jobsviewed JV2
		WHERE 
			JV2.jobseekerid = JV.jobseekerid
		ORDER BY 
			JV2.ViewedTime DESC
	)
ORDER BY
	JV.ViewedId
GO

