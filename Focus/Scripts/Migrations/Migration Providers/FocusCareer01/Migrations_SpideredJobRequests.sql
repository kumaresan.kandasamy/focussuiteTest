IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_SpideredJobsRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_SpideredJobsRequests]
GO

CREATE PROCEDURE [Migrations_SpideredJobsRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0
	
SET @QueryLastId = @LastId

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.customerjobs cj 
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId

SELECT TOP (@BatchSize)
	CJ.focusjobid,
	CJ.lensjobid,
	CJ.JobTitle,
	CJ.Employer,
	CAST(CJ.focusjobid AS VARCHAR(10)) AS MigrationId,
	CJ.lensjobid AS PostingMigrationId
FROM 
	customerjobs CJ WITH (NOLOCK FORCESEEK)
WHERE
	NOT EXISTS(SELECT 1 FROM posting P WHERE P.EOSRegistrationId = CJ.CustomerJobId)
	AND EXISTS(SELECT 1 FROM referrals R WHERE R.focusjobid = CJ.focusjobid)
	AND CJ.customerid = @CustomerKey
	AND CJ.focusjobid > @QueryLastId
	AND CJ.datemodified >= @CutOffDate
ORDER BY
	CJ.focusjobid
GO

--CREATE INDEX IX_referrals_focusjobid ON referrals(focusjobid)