IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_EmployeeResumeAlertRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_EmployeeResumeAlertRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_EmployeeResumeAlertRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.savedsearches
	
SELECT TOP (@BatchSize)
	RA.alertname AS [Name],
	RA.criteria AS CriteriaXml,
	RA.alertinterval AS Frequency,
	RA.AlertMailFormat AS [Format],
	RA.email AS EmailAddress,
	CAST(RA.resumealertid AS VARCHAR(10)) AS MigrationId,
	CAST(RA.employerrepid AS VARCHAR(10)) AS EmployeeMigrationId,
	RA.resumealertid AS SearchAlertId
FROM
	dbo.resumealerts RA
INNER JOIN dbo.employerrepdetails erd
	ON erd.employerrepid = RA.employerrepid
INNER JOIN dbo.employer E
	ON E.employerid = erd.employerid
INNER JOIN dbo.Customer C
	ON C.CustomerID = E.CustomerID
WHERE
	C.Name = @CustomerId
	AND RA.resumealertid > @LastId
	AND (@CutOffDate IS NULL OR RA.modifieddate > @CutOffDate)
	--AND RA.[status] = 1 --KY requested change
	AND DATALENGTH(RA.Criteria) > 0
ORDER BY
	RA.resumealertid
GO

