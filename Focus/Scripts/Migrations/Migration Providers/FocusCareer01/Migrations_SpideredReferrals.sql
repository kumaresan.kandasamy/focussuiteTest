﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_SpideredReferrals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_SpideredReferrals]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_SpideredReferrals]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0

SET @QueryLastId = @LastId

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.referrals
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

SELECT TOP (@BatchSize) 
	R.ReferralId,
	R.ReferredDate,
	R.StaffId,
	CAST(R.ReferralId AS VARCHAR(10)) AS MigrationId,
	CAST(R.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CJ.FocusJobId,
	CJ.lensjobid as LensPostingId,
	RS.ResumeId as MigrationResumeId
FROM 
	dbo.referrals R
INNER JOIN dbo.JobSeeker JS				ON JS.JobSeekerId = R.JobSeekerId
INNER JOIN dbo.CustomerRepDetails CRD	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN dbo.Customer C				ON C.CustomerID = CRD.CustomerID
INNER JOIN customerjobs CJ				ON CJ.FocusJobId = R.FocusJobId
INNER JOIN dbo.Resume RS				ON RS.JobSeekerId = JS.JobSeekerId
WHERE
	C.[name] = @CustomerId
	AND R.ReferralId > @QueryLastId
	AND R.ReferredDate >= @CutOffDate
	AND	CJ.originid in (1,2,3,11,12,13,14,15,16,17,18,999) /* spidered jobs and ky jobs */
ORDER BY
	R.ReferralId





GO


