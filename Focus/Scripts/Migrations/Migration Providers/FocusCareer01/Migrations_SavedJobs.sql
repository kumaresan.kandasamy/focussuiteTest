﻿
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_SavedJobs]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_SavedJobs]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_SavedJobs]
	@LastId BIGINT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0
		
DECLARE @DateFrom DATETIME
SET @DateFrom = COALESCE(@CutOffDate, DATEADD(YEAR, -100, GETDATE()))

DECLARE @CustomerKey INT

SELECT @CustomerKey = CustomerId
FROM dbo.customer 
WHERE [name] = @CustomerId

SELECT TOP (@BatchSize) 
	SJ.SavedJobId,
	CJ.JobTitle,
	CAST(CJ.LensJobId AS VARCHAR(200)) AS LensPostingId,
	CAST(SJ.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CJ.FocusJobId
FROM dbo.savedjobs SJ	
INNER JOIN customerjobs CJ				ON CJ.FocusJobId = SJ.FocusJobId
INNER JOIN dbo.JobSeeker JS				ON SJ.jobseekerid = JS.JobseekerId
WHERE
	CJ.customerid = @CustomerKey
	AND SJ.SavedJobId > @LastId
	AND	CJ.datemodified >= @DateFrom
ORDER BY
	SJ.SavedJobId









GO


