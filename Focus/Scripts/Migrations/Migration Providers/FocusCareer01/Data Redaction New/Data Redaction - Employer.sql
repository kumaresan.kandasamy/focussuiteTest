IF OBJECT_ID('tempdb..#Employer') IS NOT NULL
	DROP TABLE #Employer

IF OBJECT_ID('tempdb..#EmployerRepDetail') IS NOT NULL
	DROP TABLE #EmployerRepDetail

CREATE TABLE #Employer
(
	Id INT IDENTITY(0, 1) PRIMARY KEY,
	EmployerId INT,
	LegalName VARCHAR(250),
	EmailAddress VARCHAR(100),
	FEIN VARCHAR(15),
	SEIN VARCHAR(15),
	URL VARCHAR(250),
	StreetAddress VARCHAR(100),
	PhoneNumber1 VARCHAR(12),
	PhoneNumber2 VARCHAR(12),
	PhoneNumber3 VARCHAR(12),
	CustomerRegistrationId VARCHAR(20)
)

CREATE TABLE #EmployerRepDetail
(
	Id INT IDENTITY(0, 1) PRIMARY KEY,
	EmployerRepId INT,
	EmployerId INT,
	FirstName VARCHAR(120),
	LastName VARCHAR(120),
	EmailAddress VARCHAR(100),
	StreetAddress VARCHAR(100),
	PhoneNumber1 VARCHAR(12),
	PhoneNumber2 VARCHAR(12),
	PhoneNumber3 VARCHAR(12),
	CustomerRegistrationId VARCHAR(20)
)

-- Get employers, and set some random fields for later update
INSERT INTO #Employer 
(
	EmployerId
)
SELECT
	E.EmployerId
FROM 
	dbo.Employer E
ORDER BY
	E.EmployerId ASC

RAISERROR ('Got employer ids', 0, 1) WITH NOWAIT

-- Get the employer reps, and set some random fields for later update
INSERT INTO #EmployerRepDetail 
(
	EmployerRepId,
	EmployerId
)
SELECT
	ERD.EmployerRepId,
	ERD.EmployerId
FROM 
	dbo.EmployerRepDetails ERD
ORDER BY
	ERD.EmployerId ASC
	
RAISERROR ('Got employer rep ids', 0, 1) WITH NOWAIT

-- Randomise fields on employer
DECLARE @LegalNameMaxSize INT
DECLARE @LegalNamePadder VARCHAR(100)

SELECT @LegalNameMaxSize = MAX(LEN(LegalName)) FROM dbo.Employer
SET @LegalNamePadder = REPLICATE('a', @LegalNameMaxSize)

UPDATE
	E
SET
	LegalName = RIGHT(@LegalNamePadder + LTRIM(
		ISNULL(CHAR(97 + NULLIF(E.Id / 11881376, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(E.Id / 456976, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(E.Id / 17576, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(E.Id / 676, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(E.Id / 26, 0) % 26), '')
		+ CHAR(97 + E.Id % 26)), @LegalNameMaxSize),
	FEIN = CAST(100000000 + E.Id AS VARCHAR(10)),
	SEIN = CAST(1000000000 + E.Id AS VARCHAR(10)),
	StreetAddress = CAST(E.Id % 99 + 1 AS VARCHAR(2)) + ' ' 
		+ RTRIM(SUBSTRING('High  Lower Upper Main  New   Kings Queens', E.Id % 7 * 6 + 1, 6)) + ' '
		+ RTRIM(SUBSTRING('StreetRoad  AvenueWay   Lane  ', E.Id % 5 * 6 + 1, 6)),
	PhoneNumber1 = 'L' + CAST(1111111111 + E.Id AS VARCHAR(10)), 
	PhoneNumber2 = 'M' +  CAST(2222222222 + E.Id AS VARCHAR(10)), 
	PhoneNumber3 = 'F' + CAST(3333333333 + E.Id AS VARCHAR(10)),
	CustomerRegistrationId = 'KY' + CAST(100000000 + E.EmployerId AS VARCHAR(10))
FROM
	#Employer E
	
RAISERROR ('Prepared redacted employer data - Step 1', 0, 1) WITH NOWAIT

UPDATE
	E
SET
	LegalName = E.LegalName,
	EmailAddress = 'company.' + RIGHT(E.LegalName, 69) + '@test.burning-glass.com',
	Url = CASE Url WHEN '' THEN '' ELSE 'http://www.' + RIGHT(E.LegalName, 85) + '.com' END
FROM
	#Employer E

RAISERROR ('Prepared redacted employer data - Step 2', 0, 1) WITH NOWAIT

-- Randomise fields on employer rep details
DECLARE @FirstNameMaxSize INT
DECLARE @FirstNamePadder VARCHAR(100)

DECLARE @LastNameMaxSize INT
DECLARE @LastNamePadder VARCHAR(100)

SELECT @FirstNameMaxSize = MAX(LEN(FirstName)) FROM dbo.EmployerRepDetails
SET @FirstNamePadder = REPLICATE('a', @FirstNameMaxSize)

SELECT @LastNameMaxSize = MAX(LEN(LastName)) FROM dbo.EmployerRepDetails
SET @LastNamePadder = REPLICATE('a', @FirstNameMaxSize)

UPDATE
	ERD
SET
	FirstName = RIGHT(@FirstNamePadder + LTRIM(
		ISNULL(CHAR(97 + NULLIF(ERD.Id / 11881376, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(ERD.Id / 456976, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(ERD.Id / 17576, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(ERD.Id / 676, 0) % 26), '')
		+ ISNULL(CHAR(97 + NULLIF(ERD.Id / 26, 0) % 26), '')
		+ CHAR(97 + ERD.Id % 26)), @FirstNameMaxSize),
	StreetAddress = CAST(ERD.Id % 99 + 1 AS VARCHAR(2)) + ' ' 
		+ RTRIM(SUBSTRING('High  Lower Upper Main  New   Kings Queens', ERD.Id % 7 * 6 + 1, 6)) + ' '
		+ RTRIM(SUBSTRING('StreetRoad  AvenueWay   Lane  ', ERD.Id % 5 * 6 + 1, 6)),
	PhoneNumber1 = 'L' + CAST(1111111111 + ERD.Id AS VARCHAR(10)), 
	PhoneNumber2 = 'M' + CAST(2222222222 + ERD.Id AS VARCHAR(10)), 
	PhoneNumber3 = 'F' + CAST(3333333333 + ERD.Id AS VARCHAR(10)),
	CustomerRegistrationId = CAST(1000000 + ERD.EmployerRepId AS VARCHAR(10))
FROM
	#EmployerRepDetail ERD

RAISERROR ('Prepared redacted employer rep data - Step 1', 0, 1) WITH NOWAIT

UPDATE
	ERD
SET
	LastName = RIGHT(@LastNamePadder + FirstName, @LastNameMaxSize),
	EmailAddress = 'employee.' + RIGHT(ERD.FirstName, 68) + '@test.burning-glass.com'
FROM
	#EmployerRepDetail ERD

RAISERROR ('Prepared redacted employer rep data - Step 2', 0, 1) WITH NOWAIT

-- Update fields on employer table
UPDATE
	E
SET
	LegalName = E1.LegalName,
	EmailAddress = CASE E.EmailAddress WHEN '' THEN '' ELSE E1.EmailAddress END,
	FEIN = CASE E.FEIN WHEN '' THEN '' ELSE E1.FEIN END,
	SEIN = CASE E.SEIN WHEN '' THEN '' ELSE E1.SEIN END,
	URL = CASE E.URL WHEN '' THEN '' ELSE E1.URL END,
	Street1 = CASE E.Street1 WHEN '' THEN '' ELSE E1.StreetAddress END,
	Street2 = NULL,
	Phone1 = CASE WHEN LEN(E.Phone1) <= 1 THEN E.Phone1 ELSE E1.PhoneNumber1 END,
	Phone2 = CASE WHEN LEN(E.Phone2) <= 1 THEN E.Phone2 ELSE E1.PhoneNumber2 END,
	Phone3 = CASE WHEN LEN(E.Phone3) <= 1 THEN E.Phone3 ELSE E1.PhoneNumber3 END,
	CustomerRegistrationId = CASE ISNULL(E.CustomerRegistrationId, '') WHEN '' THEN E.CustomerRegistrationId ELSE E1.CustomerRegistrationId END
FROM
	dbo.Employer E
INNER JOIN #Employer E1
	ON E1.EmployerId = E.EmployerId

RAISERROR ('Redacted employer data', 0, 1) WITH NOWAIT

-- Update fields on the employer rep table
UPDATE
	ERD
SET
	FirstName = ERD1.FirstName,
	LastName = ERD1.LastName,
	EmailAddress = ERD1.EmailAddress,
	Street1 = CASE ERD.Street1 WHEN '' THEN '' ELSE ERD1.StreetAddress END,
	Street2 = NULL,
	Phone1 = CASE WHEN LEN(ERD.Phone1) <= 1 THEN ERD.Phone1 ELSE ERD1.PhoneNumber1 END,
	Phone2 = CASE WHEN LEN(ERD.Phone2) <= 1 THEN ERD.Phone2 ELSE ERD1.PhoneNumber2 END,
	Phone3 = CASE WHEN LEN(ERD.Phone3) <= 1 THEN ERD.Phone3 ELSE ERD1.PhoneNumber3 END,
	CustomerRegistrationId = CASE ISNULL(ERD.CustomerRegistrationId, '') WHEN '' THEN ERD.CustomerRegistrationId ELSE ERD1.CustomerRegistrationId END
FROM
	dbo.EmployerRepDetails ERD
INNER JOIN #EmployerRepDetail ERD1
	ON ERD1.EmployerRepId = ERD.EmployerRepId

RAISERROR ('Redacted employer rep data', 0, 1) WITH NOWAIT
