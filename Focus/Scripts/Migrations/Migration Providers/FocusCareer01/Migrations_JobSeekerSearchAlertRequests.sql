IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_JobSeekerSearchAlertRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_JobSeekerSearchAlertRequests]
GO

CREATE PROCEDURE [dbo].[Migrations_JobSeekerSearchAlertRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.savedsearches
	
SELECT TOP (@BatchSize)
	JA.alertname AS [Name],
	JA.SubscribedDate AS CreatedOn,
	JA.modifieddate AS UpdatedOn,
	JA.criteria AS CriteriaXml,
	JA.alertinterval AS Frequency,
	JA.AlertMailFormat AS [Format],
	JA.email AS EmailAddress,
	CAST(JA.jobalertid AS VARCHAR(10)) AS MigrationId,
	CAST(JA.jobseekerid AS VARCHAR(10)) AS JobSeekerMigrationId,
	JA.jobalertid AS SearchAlertId
FROM
	dbo.jobalerts JA WITH (FORCESEEK)
INNER JOIN dbo.jobseeker JS
	ON JS.JobSeekerId = JA.JobSeekerId
INNER JOIN CustomerRepDetails CRD
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN Customer C
	ON C.CustomerID = CRD.CustomerID
WHERE
	C.Name = @CustomerId
	AND JA.jobalertid > @LastId
	AND JA.modifieddate > @CutOffDate
	AND JA.[status] = 1
	AND DATALENGTH(JA.Criteria) > 0
ORDER BY
	JA.jobalertid
GO

