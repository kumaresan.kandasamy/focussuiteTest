IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_Referrals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_Referrals]
GO

CREATE PROCEDURE [dbo].[Migrations_Referrals]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME = NULL,
	@QueryLastId INT = NULL -- This is here as it seems to affect SQL's execution planning
WITH RECOMPILE
AS

IF @LastId IS NULL
	SET @LastId = 0
	
SET @QueryLastId = @LastId

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.referrals
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())
	
DECLARE @CustomerKey INT

SELECT
	@CustomerKey = CustomerId
FROM
	dbo.customer 
WHERE
	[name] = @CustomerId

SELECT TOP (@BatchSize) 
	R.ReferralId,
	R.ReferredDate,
	A.DateApplied,
	ISNULL(A.[Score], R.[Score]) AS [Score],
	ISNULL(A.[Status], R.[Status]) AS [Status],
	R.StaffId,
	CAST(R.ReferralId AS VARCHAR(10)) AS MigrationId,
	CAST(R.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId,
	CAST(P.PostingId AS VARCHAR(10)) AS JobOrderMigrationId
FROM 
	dbo.referrals R WITH (NOLOCK FORCESEEK)
INNER JOIN dbo.JobSeeker JS WITH (NOLOCK)
	ON JS.JobSeekerId = R.JobSeekerId
INNER JOIN dbo.CustomerRepDetails CRD WITH (NOLOCK)
	ON CRD.CustomerRepId = JS.CustomerRepId
INNER JOIN customerjobs CJ WITH (NOLOCK)
	ON CJ.FocusJobId = R.FocusJobId
INNER JOIN dbo.posting P WITH (NOLOCK)
	ON P.EOSRegistrationId = CJ.CustomerJobId 
LEFT OUTER JOIN dbo.applicants A WITH (NOLOCK)
	ON A.jobseekerid = JS.jobseekerid
	AND A.postingid = P.postingid
WHERE
	CRD.customerid = @CustomerKey
	AND R.ReferralId > @QueryLastId
	AND R.referreddate >= @CutOffDate
ORDER BY
	R.ReferralId
GO

--GRANT EXEC ON [Migrations_Referrals] TO [FocusAgent]

-- CREATE INDEX IX_posting_EOSRegistrationId ON Posting ( EOSRegistrationId )
-- CREATE INDEX IX_Referrals_ReferralsId ON Referrals ( ReferralId )

