IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_UserRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_UserRequests]
GO

CREATE PROCEDURE [Migrations_UserRequests]
	@LastId INT = NULL,
	@BatchSize INT = NULL,
	@CustomerId VARCHAR(30)
AS

IF @LastId IS NULL
	SET @LastId = 0

IF @BatchSize IS NULL
	SELECT @BatchSize = COUNT(1) FROM dbo.Staff

SELECT TOP (@BatchSize)
	S.UserName,
	S.FirstName AS UserPersonFirstName, 
	ISNULL(LEFT(S.MiddleName, 1), '') AS UserPersonMiddleInitial,
	S.LastName AS UserPersonLastName,
	'' AS UserPersonJobTitle,
	S.email AS UserPersonEmailAddress,
	ISNULL(SO.Street1, '') AS UserAddressLine1,
	ISNULL(SO.Street2, '') AS UserAddressLine2,
	ISNULL(SO.City, '') AS UserAddressTownCity,
	ISNULL(SO.PostalCode, '') AS UserAddressPostcodeZip,
	ISNULL(SO.[State], '') AS UserAddressState,
	ISNULL(SO.County, 'US') AS UserAddressCountry,
	ISNULL(NULLIF(SO.phone1, ''), 'Unspecified') AS UserPhone,
	'' AS UserPhoneExtension,
	ISNULL(SO.phone2, '') AS UserAlternatePhone,
	'' AS UserAlternatePhoneExtension,
	ISNULL(SO.Fax, '') AS UserFax,
	CAST(S.StaffID AS VARCHAR(10)) AS AccountExternalId,
	ISNULL(SO.OfficeName, 'Undefined') AS UserOffice,
	CAST(S.StaffCode AS VARCHAR(10)) AS MigrationId,
	S.StaffId
FROM
	dbo.Staff S
INNER JOIN dbo.StaffOffice SO
	ON SO.OfficeId = S.OfficeId
INNER JOIN dbo.Customer C
	ON C.CustomerID = S.CustomerID
WHERE
	C.Name = @CustomerId
	AND S.StaffId > @LastId
ORDER BY
	S.StaffId
GO

--GRANT EXEC ON [Migrations_UserRequests] TO [FocusAgent]
