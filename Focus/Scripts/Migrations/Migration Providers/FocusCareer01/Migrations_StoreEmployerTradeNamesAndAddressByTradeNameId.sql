﻿/****** Object:  StoredProcedure [dbo].[Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId]    Script Date: 01/12/2015 16:51:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId]
GO

/****** Object:  StoredProcedure [dbo].[Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId]    Script Date: 01/12/2015 16:51:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Migrations_StoreEmployerTradeNamesAndAddressByTradeNameId]
	@LastId INT = NULL,
	@CustomerId VARCHAR(30),
	@CutOffDate DATETIME
AS

IF @LastId IS NULL
	SET @LastId = 0
	
IF @CutOffDate IS NULL
	SET @CutOffDate = DATEADD(YEAR, -100, GETDATE())

DECLARE @EmployerTradeNameOneEmployeeOnly TABLE(
EmployerId int NOT NULL
);

IF OBJECT_ID('EmployerTradeNamesMigration') is not null
BEGIN
		DROP TABLE EmployerTradeNamesMigration;
END

CREATE TABLE EmployerTradeNamesMigration
(
	TradeNameId int not null,
	TradeName varchar(250),
	IsDefault int,
	EmployerId int,
	LegalName varchar(250),
	EmployeeId int,
	Fein varchar(15),
	Street1 varchar(250),
	Street2 varchar(250),
	City varchar(250),
	State varchar(250),
	County varchar(250),
	Country varchar(250),
	PostalCode varchar(250)
)

INSERT INTO		@EmployerTradeNameOneEmployeeOnly
SELECT			employerid
FROM			dbo.employerrepdetails
GROUP BY		EMPLOYERID
HAVING			COUNT(EMPLOYERrepID) = 1

INSERT INTO EmployerTradeNamesMigration
		SELECT * FROM 
		(
			/* GET THE RECORDS WHERE THERE ARE 0 REPS, CAN'T TELL WHO IS THE HIRING MANAGER, DEFAULT TO THE EMPLOYER ADDRESS */
			SELECT		etn.tradenameid AS 'TradeNameId',
						etn.tradename AS 'TradeName',
						etn.isdefault AS 'IsDefault',
						e.employerid AS 'EmployerId',
						e.legalname AS 'LegalName',
						erd.employerrepid AS 'EmployeeId',
						e.fein AS 'Fein',
						e.Street1 AS 'Street1',
						e.Street2 AS 'Street2',
						e.City AS 'City',
						e.State AS 'State',
						e.County AS 'County',
						e.Country AS 'Country',
						e.PostalCode AS 'PostalCode'
			FROM		dbo.employerTradeNames etn
			INNER JOIN	dbo.employer e ON etn.employerid = e.employerid
			INNER JOIN	dbo.Customer C ON C.CustomerID = E.CustomerId	
			LEFT JOIN	dbo.employerrepdetails erd ON erd.employerid = e.employerid
			WHERE		C.[name] = @CustomerId
			AND			etn.tradenameid > @LastId
			AND			(e.LastModified > @CutOffDate OR erd.LastModified > @CutOffDate)
			AND NOT EXISTS (SELECT EMPLOYERID FROM @EmployerTradeNameOneEmployeeOnly WHERE EMPLOYERID = erd.employerid)
			AND			erd.employerrepid is null

			UNION
			/* GET THE RECORDS WHERE THERE IS >1 REP, CAN'T TELL WHO IS THE HIRING MANAGER, DEFAULT TO THE EMPLOYER ADDRESS*/
			SELECT		etn.tradenameid AS 'TradeNameId',
						etn.tradename AS 'TradeName',
						etn.isdefault AS 'IsDefault',
						e.employerid AS 'EmployerId',
						e.legalname AS 'LegalName',
						erd.employerrepid AS 'EmployeeId',
						e.fein AS 'Fein',
						e.Street1 AS 'Street1',
						e.Street2 AS 'Street2',
						e.City AS 'City',
						e.State AS 'State',
						e.County AS 'County',
						e.Country AS 'Country',
						e.PostalCode AS 'PostalCode'
			FROM		dbo.employerTradeNames etn
			INNER JOIN	dbo.employer e ON etn.employerid = e.employerid
			INNER JOIN	dbo.Customer C ON C.CustomerID = E.CustomerId	
			INNER JOIN	dbo.employerrepdetails erd ON erd.employerid = e.employerid
			WHERE		C.[name] = @CustomerId
			AND			etn.tradenameid > @LastId
			AND			(e.LastModified > @CutOffDate OR erd.LastModified > @CutOffDate)
			AND NOT EXISTS (SELECT EMPLOYERID FROM @EmployerTradeNameOneEmployeeOnly WHERE EMPLOYERID = erd.employerid)
			AND			erd.employerrepid = (select min(employerrepid) from employerrepdetails where employerid = e.employerid)
			
			UNION
			--/* GET THE RECORDS WHERE THERE IS A SINGLE REP, ASSUMED TO BE THE HIRING MANAGER */
			SELECT	 	etn.tradenameid AS 'TradeNameId',
						etn.tradename AS 'TradeName',
						etn.isdefault AS 'IsDefault',
						e.employerid AS 'EmployerId',
						e.legalname AS 'LegalName',
						erd.employerrepid AS 'EmployeeId',
						e.fein AS 'Fein',
						erd.street1 AS 'Street1',
						erd.street2 AS 'Street2',
						erd.city AS 'City',
						erd.[state] AS 'State',
						erd.county AS 'County',
						erd.country AS 'Country',
						erd.postalcode AS 'PostalCode'
			FROM		dbo.employerTradeNames etn
			INNER JOIN	dbo.employer e ON etn.employerid = e.employerid
			INNER JOIN	dbo.Customer C ON C.CustomerID = E.CustomerId	
			INNER JOIN	dbo.employerrepdetails erd ON erd.employerid = e.employerid
			INNER JOIN @EmployerTradeNameOneEmployeeOnly TEMP ON TEMP.EMPLOYERID = erd.employerid
			WHERE		C.[name] = @CustomerId
			AND			etn.tradenameid > @LastId
			AND			(E.LastModified > @CutOffDate OR erd.LastModified > @CutOffDate)
		) BusinessUnit
		ORDER BY TradeNameId






GO

