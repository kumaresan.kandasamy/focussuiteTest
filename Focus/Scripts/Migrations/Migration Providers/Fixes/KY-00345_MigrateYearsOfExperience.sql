update [data.application.resume] set [data.application.resume].yearsexperience = ri.yearsexp
from [data.application.resume]
inner join [data.migration.jobseekerrequest] jsr on [data.application.resume].id = jsr.focusid
inner join [focuscareer02].dbo.resume rs on jsr.externalid = 'FC01:' + cast(rs.resumeid as nvarchar)
inner join [focuscareer02].dbo.resumeinfo ri on ri.resumeid = rs.resumeid
