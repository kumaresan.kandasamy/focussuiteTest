/* During a v1 to v3 migration all activities are migrated as non-vet activities. This script updates the activities to the vet version for jobseekers who are veterans */
DECLARE @NonVetCategoryId INT;
SELECT @NonVetCategoryId = id FROM [FocusCareer01].dbo.[config.activitycategory] WHERE name like 'Counseling/Case Management/Services (non-Vets)';
/*SELECT @NonVetCategoryId*/
DECLARE @VetCategoryId int;
SELECT @VetCategoryId = id FROM [FocusCareer01].dbo.[config.activitycategory] WHERE name like 'Counseling/Case Management/Services (Vets)';
/*SELECT @VetCategoryId;*/

WITH v1activities AS (
SELECT activityUnion.customeractivityid, activityUnion.customeractivitymessage
FROM (	
SELECT   A.customeractivitymessage,
	A.ActivityId,
	A.CustomerActivityId,
	A.ActivityTime,
	A.CustomerRepId,
	A.AddedBy,
	CAST(A.ActivityId AS VARCHAR(10)) AS MigrationId,
	CAST(A.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId
FROM 
	[focuscareer02].dbo.Activities A
INNER JOIN [focuscareer02].dbo.JobSeeker JS	ON JS.JobSeekerId = A.JobSeekerId
INNER JOIN [focuscareer02].dbo.CustomerRepDetails CRD ON CRD.CustomerRepId = JS.CustomerRepId
WHERE
	
	CRD.customerid = 1
	AND A.ActivityId > 0
	AND A.activitytime > DATEADD(YEAR, -100, GETDATE())
	AND A.CUSTOMERACTIVITYID <> 'SMRY01'
	AND A.CUSTOMERACTIVITYID <> 'UNC01'
	AND A.CUSTOMERACTIVITYID <> 'PWC01'
	AND A.CUSTOMERACTIVITYID <> 'SQA01'
	AND A.CUSTOMERACTIVITYID <> 'SSNC01'
	AND A.CUSTOMERACTIVITYID <> 'STSC01'
	AND A.CUSTOMERACTIVITYID <> 'REF01'	
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[focuscareer02].dbo.activities A2
		WHERE
			A2.jobseekerid = A.jobseekerid 
			AND A2.customeractivityid = A.customeractivityid
			AND A2.activitytime > A.ActivityTime
			AND A2.activitytime < DATEADD(DAY, 1, CONVERT(DATE, A.ActivityTime))
	)
	UNION
	SELECT		  A.customeractivitymessage,
				A.ActivityId,
				A.CustomerActivityId,
				A.ActivityTime,
				A.CustomerRepId,
				A.AddedBy,
				CAST(A.ActivityId AS VARCHAR(10)) AS MigrationId,
				CAST(A.JobseekerId AS VARCHAR(10)) AS JobSeekerMigrationId
	FROM		[focuscareer02].dbo.activities a 
	INNER JOIN	[focuscareer02].dbo.JobSeeker JS			ON JS.JobSeekerId = A.JobSeekerId
	INNER JOIN	[focuscareer02].dbo.CustomerRepDetails CRD	ON CRD.CustomerRepId = JS.CustomerRepId
	WHERE		A.CUSTOMERACTIVITYID = 'SMRY01'
	AND			a.activitytime = (SELECT MAX(activitytime) FROM [focuscareer02].dbo.activities a2 WHERE a2.jobseekerid = a.jobseekerid GROUP BY jobseekerid)
	AND	CRD.customerid = 1
	AND A.ActivityId > 0
	AND A.activitytime > DATEADD(YEAR, -100, GETDATE())
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[focuscareer02].dbo.activities A2
		WHERE
			A2.jobseekerid = A.jobseekerid 
			AND A2.customeractivityid = A.customeractivityid
			AND A2.activitytime > A.ActivityTime
			AND A2.activitytime < DATEADD(DAY, 1, CONVERT(DATE, A.ActivityTime))
	)
) AS activityUnion
GROUP BY activityUnion.customeractivitymessage, activityUnion.customeractivityid
)
UPDATE [FocusCareer01].DBO.[DATA.CORE.ACTIONEVENT]
SET ENTITYIDADDITIONAL01 = (SELECT [config.activity].id FROM [config.activity] WHERE a.name = [config.activity].name and activitycategoryid = @VetCategoryId)
FROM [FocusCareer01].[dbo].[Data.Core.ActionEvent] ae
  INNER JOIN [FocusCareer01].dbo.[data.core.actiontype] at ON ae.actiontypeid = at.id
  INNER JOIN [FocusCareer01].dbo.[config.activity] a ON ae.entityidadditional01 = a.id
  INNER JOIN [FocusCareer01].dbo.[Data.Application.Person] p ON p.Id = ae.EntityId
  RIGHT JOIN v1activities act1 ON act1.customeractivityid = CAST(a.externalid AS VARCHAR)
  WHERE entityidadditional01 IS NOT NULL
  AND IsVeteran = 1
  AND a.activityCategoryid = @NonVetCategoryId
  