DECLARE @FullTimeSeasonalID BIGINT
DECLARE @PartTimeTimeSeasonalID BIGINT

SELECT @FullTimeSeasonalID = Id FROM [Config.LookupItemsView] LI WHERE LI.[Key] = 'Duration.FullTimeSeasonal'
SELECT @PartTimeTimeSeasonalID = Id FROM [Config.LookupItemsView] LI WHERE LI.[Key] = 'Duration.PartTimeSeasonal'

UPDATE
	J
SET 
	J.JobTypeId = CASE
		WHEN TP.Posting LIKE '%<jobduration>7</jobduration>%' THEN @FullTimeSeasonalID
		ELSE @PartTimeTimeSeasonalID
	END
FROM 
	[Data.Application.Job] J 
INNER JOIN [Data.Migration.JobOrderRequest] JOR 
	ON J.Id = JOR.FocusId
INNER JOIN [focuscareer02].dbo.Posting P1 
	ON JOR.ExternalId = 'FC01:' + CAST(P1.PostingId AS NVARCHAR(30))
INNER JOIN [focuscareer02].dbo.TaggedPostings TP1 
	ON TP1.PostingId = P1.PostingId
WHERE
	TP1.Posting LIKE '%<jobduration>[78]</jobduration>%'


