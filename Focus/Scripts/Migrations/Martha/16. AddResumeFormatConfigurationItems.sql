BEGIN TRANSACTION

DECLARE @StartId BIGINT
DECLARE @NumberRowsToInsert BIGINT
DECLARE @MaxNextId BIGINT
DECLARE @ConfigurationItemCount INT

DECLARE @Item TABLE (ItemKey nvarchar(200), ItemValue nvarchar(500))

-- Populate the @Items
INSERT INTO @Item VALUES
('ResumeFormat_Chronological', '~\Assets\Xslts\ChronologicalFormat.xsl'),
('ResumeFormat_Classic', '~\Assets\Xslts\ClassicFormat.xsl'),
('ResumeFormat_Contemporary', '~\Assets\Xslts\ContemporaryFormat.xsl'),
('ResumeFormat_Modern', '~\Assets\Xslts\ModernFormat.xsl'),
('ResumeFormat_Professional', '~\Assets\Xslts\ProfessionalFormat.xsl'),
('ResumeFormat_Standard', '~\Assets\Xslts\FocusCareerFormat.xsl')

-- Determine the number of rows to create
SELECT @ConfigurationItemCount = COUNT(*)  FROM @Item 
SET @NumberRowsToInsert = @ConfigurationItemCount

-- Update the Next ID to be >= the number of rows we expect
UPDATE KeyTable SET NextId = NextId + @NumberRowsToInsert
SELECT @MaxNextId = NextId FROM KeyTable
SET @StartId = @MaxNextId - @NumberRowsToInsert

-- Create the ConfigurationItems from @Group
INSERT INTO ConfigurationItem (Id, [Key], Value)
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), ItemKey, ItemValue FROM @Item


COMMIT TRANSACTION