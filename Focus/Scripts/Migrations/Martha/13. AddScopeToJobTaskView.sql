ALTER VIEW [dbo].[JobTaskView]
AS

SELECT
  jt.[Id] AS [Id],
  jtli.[PrimaryValue] AS Prompt,
  jtli.[SecondaryValue] AS Response,
  jt.[JobTaskType] AS JobTaskType,
  jt.[OnetId],
  li.[Culture],
  jt.Scope AS Scope,
  jt.[Certificate] AS [Certificate]
FROM
  [JobTask] jt WITH (NOLOCK) 
	INNER JOIN [JobTaskLocalisationItem] jtli WITH (NOLOCK) ON jt.[Key] = jtli.[Key]
	INNER JOIN [Localisation] li WITH (NOLOCK) ON jtli.[LocalisationId] = li.[Id]