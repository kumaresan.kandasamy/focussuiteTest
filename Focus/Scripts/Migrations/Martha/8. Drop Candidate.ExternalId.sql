DROP INDEX IX_Candidate_ExternalId ON Candidate

GO

ALTER TABLE Candidate
DROP COLUMN ExternalId

GO