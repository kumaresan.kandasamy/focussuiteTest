DECLARE @LocalisationId bigint

SELECT @LocalisationId = [Id] FROM [Config.Localisation] WHERE Culture = '**-**'

/*** ADD NEW ITEMS ***/

DECLARE @NewItems TABLE
(
	[Key] NVARCHAR(400),
	[Value] NVARCHAR(1000)
)

INSERT INTO @NewItems ([Key], [Value])
VALUES
--('EmergingSector.TransportDistribute.ToolTip', ''),
--('EmergingSector.SMART.ToolTip', ''),
--('EmergingSector.ResearchandDevelopment.ToolTip', ''),
--('EmergingSector.InformationTechnology.ToolTip', ''),
--('EmergingSector.HealthInformatics.ToolTip', ''),
('EmergingSector.Healthcare.ToolTip', 'Jobs involving the intersecting disciplines of information science, computer science, and healthcare. They deal with the resources, devices, and methods required to optimize the acquisition, storage, retrieval and use of information in healthcare and biomedicine. May also apply to nursing, clinical care, dentistry, pharmacy, public health, occupational therapy, and biomedical research.'),
('EmergingSector.Green.ToolTip', 'Jobs involving the policies, information, materials, technologies and promotion of operations that contribute to minimizing environmental impact; conserving energy; developing alternative, sustainable or high-efficiency energy sources; controlling pollution; developing organic chemical replacements; and recycling or reducing waste.'),
('EmergingSector.Energy.ToolTip', 'Jobs involving the extraction, production, manufacture, sale, and maintenance of energy resources, fuels, and systems that fulfill the need for power, including petroleum, natural gas, electrical, hydroelectric, coal, nuclear, solar, wind, and alternative fuels.'),
('EmergingSector.Consulting.ToolTip', 'Jobs involving the provision of opinion, advice, counsel, or targeted services, covering a wide range of professions such as biotechnology, engineering, environmental, faculty, financial, franchising, human resources, information technology, legal, management, performance, personal dynamics, politics, public relations, manufacturing, supply-chain.'),
('EmergingSector.BusinessServices.ToolTip', 'Jobs involving the management of business-information technology alignment that promotes a customer-centric approach to service delivery through strategic planning, operations, and continuous improvement.'),
('EmergingSector.BioTechnology.ToolTip', 'Jobs involving the development and modification of useful products from living organisms with applications in genomics, recombinant gene technologies, applied immunology, pharmaceutical therapies, diagnostic testing, medicine, agriculture, and food production.'),
('EmergingSector.Aerospace.ToolTip', 'Jobs involving research, design, manufacture, operation, or maintenance of aeronautic and astronautic vehicles with diverse commercial, industrial, and military applications.'),
('EmergingSector.AdvancedManufacturing.ToolTip', 'Jobs involving the innovative use of technology to improve products or processes.')

DELETE TI FROM @NewItems TI
INNER JOIN [Config.LocalisationItem] LI ON LI.[Key] = TI.[Key]

BEGIN TRANSACTION

INSERT INTO [Config.LocalisationItem] ([Key], [Value], LocalisationId, ContextKey, Localised)
SELECT [Key], [Value], @LocalisationId, '', 0
FROM @NewItems

COMMIT TRANSACTION

/*** UPDATE EXISTING ITEMS ***/

BEGIN TRANSACTION

DECLARE @UpdateItems TABLE
(
	[Key] NVARCHAR(400),
	[OldValue] NVARCHAR(1000),
	[NewValue] NVARCHAR(1000)
)

INSERT INTO @UpdateItems ([Key], [OldValue], [NewValue])
VALUES
('EmergingSector.BioTechnology', 'Bio-Technology', 'Biotechnology'),
('EmergingSector.ResearchandDevelopment', 'Research and Development', 'Research & Development'),
('EmergingSector.TransportDistribute', 'Transport/Distribute', 'Transportation, Distribution & Logistics')

UPDATE 
	LI
SET 
	[Value] = TI.NewValue
FROM 
	[Config.LocalisationItem] LI
INNER JOIN @UpdateItems TI
	ON TI.[Key] = LI.[Key]
WHERE 
	LI.LocalisationId = @LocalisationId
	AND LI.Value = TI.OldValue

COMMIT TRANSACTION
