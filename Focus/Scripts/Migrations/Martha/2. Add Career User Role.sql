﻿BEGIN TRAN 
  
-- DECLARE Variables to track counts and ids
DECLARE @StartId BIGINT
DECLARE @NumberRowsToInsert BIGINT

SET @NumberRowsToInsert = 1
  
-- Update the Next ID to be >= the number of rows we expect
UPDATE KeyTable SET NextId = NextId + @NumberRowsToInsert
SELECT @StartId = NextId FROM KeyTable

-- Create a new Career User Role  
INSERT INTO Role
(Id, [Key], Value)
VALUES
(@StartId, 'CareerUser', 'Career User')

COMMIT