
/*

Add the issues tables and view

*/
IF object_id('Issues', 'U') IS NULL

BEGIN

CREATE TABLE [dbo].[Issues](
	[Id] [bigint] NOT NULL,
	[PersonId] [bigint] NOT NULL,
	[JobOfferRejectionCount] [int] NOT NULL,
	[JobOfferRejectionResolvedDate] [datetime] NULL,
	[NotReportingToInterviewCount] [int] NOT NULL,
	[NotReportingToInterviewResolvedDate] [datetime] NULL,
	[NotClickingOnLeadsCount] [int] NOT NULL,
	[NotClickingOnLeadsResolvedDate] [datetime] NULL,
	[FollowUpRequested] [bit] NOT NULL,
	[JobOfferRejectionTriggered] [bit] NOT NULL,
	[NotReportingToInterviewTriggered] [bit] NOT NULL,
	[NotClickingOnLeadsTriggered] [bit] NOT NULL,
	[NotRespondingToEmployerInvitesCount] [int] NOT NULL,
	[NotRespondingToEmployerInvitesResolvedDate] [datetime] NULL,
	[NotRespondingToEmployerInvitesTriggered] [bit] NOT NULL,
	[ShowingLowQualityMatchesCount] [int] NOT NULL,
	[ShowingLowQualityMatchesResolvedDate] [datetime] NULL,
	[ShowingLowQualityMatchesTriggered] [bit] NOT NULL,
	[PostingLowQualityResumeCount] [int] NOT NULL,
	[PostingLowQualityResumeResolvedDate] [datetime] NULL,
	[PostingLowQualityResumeTriggered] [bit] NOT NULL,
	[PostHireFollowUpTriggered] [bit] NOT NULL,
	[PostHireFollowUpResolvedDate] [datetime] NULL,
	[NoLoginTriggered] [bit] NOT NULL,
	[NoLoginResolvedDate] [datetime] NULL,
 CONSTRAINT [PK__Issues__3214EC074D005615] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[Issues] ADD  CONSTRAINT [DF__Issues__JobOffer__4EE89E87]  DEFAULT ((0)) FOR [JobOfferRejectionCount]

ALTER TABLE [dbo].[Issues] ADD  CONSTRAINT [DF__Issues__NotRepor__0FC23DAB]  DEFAULT ((0)) FOR [NotReportingToInterviewCount]

ALTER TABLE [dbo].[Issues] ADD  CONSTRAINT [DF__Issues__NotClick__10B661E4]  DEFAULT ((0)) FOR [NotClickingOnLeadsCount]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [FollowUpRequested]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [JobOfferRejectionTriggered]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [NotReportingToInterviewTriggered]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [NotClickingOnLeadsTriggered]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [NotRespondingToEmployerInvitesCount]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [NotRespondingToEmployerInvitesTriggered]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [ShowingLowQualityMatchesCount]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [ShowingLowQualityMatchesTriggered]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [PostingLowQualityResumeCount]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [PostingLowQualityResumeTriggered]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [PostHireFollowUpTriggered]

ALTER TABLE [dbo].[Issues] ADD  DEFAULT ((0)) FOR [NoLoginTriggered]

END

GO

IF object_id('Issues', 'U') IS NULL
BEGIN

CREATE VIEW [dbo].[CandidateIssueView]
AS
SELECT p.Id, p.FirstName, p.LastName, re.DateOfBirth, p.SocialSecurityNumber, re.TownCity, re.StateId, re.EmailAddress, u.LastLoggedInOn
	  ,i.NoLoginTriggered
      ,i.JobOfferRejectionTriggered
      ,i.NotReportingToInterviewTriggered
      ,i.NotClickingOnLeadsTriggered
      ,i.NotRespondingToEmployerInvitesTriggered
      ,i.ShowingLowQualityMatchesTriggered
      ,i.PostingLowQualityResumeTriggered
      ,i.PostHireFollowUpTriggered
      ,i.FollowUpRequested
FROM  dbo.[User] AS u INNER JOIN
               dbo.Person AS p ON u.PersonId = p.Id INNER JOIN
               dbo.Resume AS re ON p.Id = re.PersonId
               LEFT JOIN dbo.Issues i on p.Id = i.PersonId
WHERE ((u.UserType = 4) OR
               (u.UserType = 5)) AND (re.IsDefault = 1)
END