BEGIN TRANSACTION

alter table ConfigurationItem alter COLUMN [Value] nvarchar(1000) not null

COMMIT TRANSACTION