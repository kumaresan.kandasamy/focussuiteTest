﻿BEGIN TRANSACTION

DECLARE @StatusChangesCount int
DECLARE @NextId bigint
DECLARE @EndOfBlockNextId bigint
DECLARE @ApplicationEntityId bigint

SELECT @StatusChangesCount = COUNT(ActionEvent.Id) FROM ActionEvent 
INNER JOIN ActionType ON ActionEvent.ActionTypeId = ActionType.Id
INNER JOIN CandidateApplication ON ActionEvent.EntityIdAdditional02 = CandidateApplication.Id
WHERE Name = 'SaveJobSeeker' AND EntityIdAdditional02 is not null
AND EntityIdAdditional02 IN (SELECT DISTINCT EntityId FROM ActionEvent 
INNER JOIN ActionType ON ActionEvent.ActionTypeId = ActionType.Id
WHERE Name = 'ApproveCandidateReferral') OR ApprovalStatus = 1 OR ApprovalStatus = 3

SELECT @ApplicationEntityId = Id FROM EntityType WHERE Name = 'CandidateApplication' 
		
SELECT @NextId = NextId FROM KeyTable
		

PRINT N'Status updates = ' + CAST(@StatusChangesCount AS nvarchar(10));
PRINT N'NextId = ' + CAST(@NextId AS nvarchar(10));

SELECT @EndOfBlockNextId = @NextId + @StatusChangesCount + 100

PRINT N'Updating key table to ' + CAST(@EndOfBlockNextId AS nvarchar(10));

UPDATE KeyTable SET NextId = @EndOfBlockNextId


PRINT N'Updating Status Log'

DECLARE actionEventCursor Cursor For SELECT ActionEvent.Id FROM ActionEvent 
INNER JOIN ActionType ON ActionEvent.ActionTypeId = ActionType.Id
INNER JOIN CandidateApplication ON ActionEvent.EntityIdAdditional02 = CandidateApplication.Id
WHERE Name = 'SaveJobSeeker' AND EntityIdAdditional02 is not null
AND EntityIdAdditional02 IN (SELECT DISTINCT EntityId FROM ActionEvent 
INNER JOIN ActionType ON ActionEvent.ActionTypeId = ActionType.Id
WHERE Name = 'ApproveCandidateReferral') OR ApprovalStatus = 1 OR ApprovalStatus = 3

DECLARE @ActionEventId bigint

OPEN actionEventCursor

FETCH next FROM actionEventCursor into @ActionEventId

WHILE @@FETCH_STATUS=0 BEGIN
	SELECT @NextId = @NextId + 1;
	
	INSERT INTO StatusLog(Id, EntityId, EntityTypeId, OriginalStatus, NewStatus, UserId, ActionedOn)
	SELECT @NextId, EntityIdAdditional02, @ApplicationEntityId, 0, 1, UserId, ActionedOn  FROM ActionEvent
	INNER JOIN ActionType ON ActionEvent.ActionTypeId = ActionType.Id
	WHERE ActionEvent.Id = @ActionEventId
	
	FETCH next FROM actionEventCursor into @ActionEventId	
END

CLOSE actionEventCursor
DEALLOCATE actionEventCursor

COMMIT TRANSACTION