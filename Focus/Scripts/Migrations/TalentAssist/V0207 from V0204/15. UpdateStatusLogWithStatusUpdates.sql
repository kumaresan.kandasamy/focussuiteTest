﻿BEGIN TRANSACTION

DECLARE @StatusChangesCount int
DECLARE @NextId bigint
DECLARE @EndOfBlockNextId bigint

SELECT @StatusChangesCount = COUNT(ActionEvent.Id) FROM ActionEvent 
INNER JOIN ActionType ON ActionEvent.ActionTypeId = ActionType.Id
WHERE Name IN ('UpdateApplicationStatusToRecommended',
		'UpdateApplicationStatusToFailedToShow',
		'UpdateApplicationStatusToHired',
		'UpdateApplicationStatusToInterviewScheduled',
		'UpdateApplicationStatusToInterviewed',
		'UpdateApplicationStatusToNewApplicant',
		'UpdateApplicationStatusToNotApplicable',
		'UpdateApplicationStatusToOfferMade',
		'UpdateApplicationStatusToNotHired',
		'UpdateApplicationStatusToUnderConsideration',
		'UpdateApplicationStatusToDidNotApply',
		'UpdateApplicationStatusToInterviewDenied',
		'UpdateApplicationStatusToRefusedOffer',
		'UpdateApplicationStatusToSelfReferred')
		
SELECT @NextId = NextId FROM KeyTable
		

PRINT N'Status updates = ' + CAST(@StatusChangesCount AS nvarchar(10));
PRINT N'NextId = ' + CAST(@NextId AS nvarchar(10));

SELECT @EndOfBlockNextId = @NextId + @StatusChangesCount + 100

PRINT N'Updating key table to ' + CAST(@EndOfBlockNextId AS nvarchar(10));

UPDATE KeyTable SET NextId = @EndOfBlockNextId


PRINT N'Updating Status Log'

DECLARE actionEventCursor Cursor For SELECT ActionEvent.Id FROM ActionEvent 
INNER JOIN ActionType ON ActionEvent.ActionTypeId = ActionType.Id
WHERE Name IN ('UpdateApplicationStatusToRecommended',
		'UpdateApplicationStatusToFailedToShow',
		'UpdateApplicationStatusToHired',
		'UpdateApplicationStatusToInterviewScheduled',
		'UpdateApplicationStatusToInterviewed',
		'UpdateApplicationStatusToNewApplicant',
		'UpdateApplicationStatusToNotApplicable',
		'UpdateApplicationStatusToOfferMade',
		'UpdateApplicationStatusToNotHired',
		'UpdateApplicationStatusToUnderConsideration',
		'UpdateApplicationStatusToDidNotApply',
		'UpdateApplicationStatusToInterviewDenied',
		'UpdateApplicationStatusToRefusedOffer',
		'UpdateApplicationStatusToSelfReferred')

DECLARE @ActionEventId bigint

OPEN actionEventCursor

FETCH next FROM actionEventCursor into @ActionEventId

WHILE @@FETCH_STATUS=0 BEGIN
	SELECT @NextId = @NextId + 1;
	
	INSERT INTO StatusLog(Id, EntityId, EntityTypeId, OriginalStatus, NewStatus, UserId, ActionedOn)
	SELECT @NextId, EntityId, EntityTypeId, 0,
	CASE Name
		WHEN 'UpdateApplicationStatusToRecommended' THEN 2
		WHEN 'UpdateApplicationStatusToFailedToShow' THEN 9
		WHEN 'UpdateApplicationStatusToHired' THEN 12
		WHEN 'UpdateApplicationStatusToInterviewScheduled' THEN 6
		WHEN 'UpdateApplicationStatusToInterviewed' THEN 7
		WHEN 'UpdateApplicationStatusToNewApplicant' THEN 3
		WHEN 'UpdateApplicationStatusToNotApplicable' THEN 0
		WHEN 'UpdateApplicationStatusToOfferMade' THEN 10
		WHEN 'UpdateApplicationStatusToNotHired' THEN 13
		WHEN 'UpdateApplicationStatusToUnderConsideration' THEN 4
		WHEN 'UpdateApplicationStatusToDidNotApply' THEN 5
		WHEN 'UpdateApplicationStatusToInterviewDenied' THEN 8
		WHEN 'UpdateApplicationStatusToRefusedOffer' THEN 11
		WHEN 'UpdateApplicationStatusToSelfReferred' THEN 1
		ELSE 0
		END, 
	UserId, ActionedOn  FROM ActionEvent
	INNER JOIN ActionType ON ActionEvent.ActionTypeId = ActionType.Id
	WHERE ActionEvent.Id = @ActionEventId
	
	FETCH next FROM actionEventCursor into @ActionEventId	
END

CLOSE actionEventCursor
DEALLOCATE actionEventCursor

COMMIT TRANSACTION