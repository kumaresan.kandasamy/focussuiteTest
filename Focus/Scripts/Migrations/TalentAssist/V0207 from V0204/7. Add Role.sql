﻿BEGIN TRANSACTION 

DECLARE @NextId bigint
DECLARE @EndOfBlockNextId bigint
DECLARE @IdsRequired bigint

SELECT @IdsRequired = 5

UPDATE KeyTable SET NextId = NextId + @IdsRequired

SELECT @EndOfBlockNextId = NextId FROM KeyTable

SELECT @NextId = @EndOfBlockNextId - @IdsRequired
 
INSERT INTO [Role]
VALUES (@NextId,'AssistEmployerCompanyAddress', 'Assist Employer Company Address') 

COMMIT TRANSACTION