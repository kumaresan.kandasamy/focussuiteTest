/****** Object:  View [dbo].[ApplicationView]    Script Date: 12/07/2012 10:45:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ActiveSearchAlertView]
AS

SELECT 
	SavedSearch.Id,
	Name,
	SearchCriteria,
	AlertEmailRequired,
	AlertEmailFrequency,
	AlertEmailFormat,
	AlertEmailAddress,
	AlertEmailScheduledOn,
	[Type]
FROM 
	SavedSearch WITH (NOLOCK)
	INNER JOIN (SELECT DISTINCT
					SavedSearchId 
				FROM 
					SavedSearchUser WITH (NOLOCK)
					INNER JOIN [User] WITH (NOLOCK) ON SavedSearchUser.UserId = [User].Id 
				WHERE
					[User].[Enabled] = 1) AS SearchWithActiveUser ON SavedSearch.Id = SearchWithActiveUser.SavedSearchId
WHERE 
	AlertEmailRequired = 1 


GO