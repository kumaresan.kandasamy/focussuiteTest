IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Issues' AND COLUMN_NAME = 'InappropriateEmailAddress')
BEGIN
	ALTER TABLE [Data.Application.Issues]
		ADD InappropriateEmailAddress BIT DEFAULT(0)
END