
/****** Object:  View [dbo].[Data.Application.CandidateIssueView]    Script Date: 11/25/2013 11:41:12 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.CandidateIssueView]'))
DROP VIEW [dbo].[Data.Application.CandidateIssueView]
GO


/****** Object:  View [dbo].[Data.Application.CandidateIssueView]    Script Date: 11/25/2013 11:41:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.CandidateIssueView]  
AS  
SELECT 
	p.Id, 
	p.FirstName, 
	p.LastName, 
	re.DateOfBirth, 
	p.SocialSecurityNumber, 
	re.TownCity, 
	re.StateId, 
	p.EmailAddress, 
	u.LoggedInOn AS LastLoggedInOn,
	i.NoLoginTriggered,
	i.JobOfferRejectionTriggered,
	i.NotReportingToInterviewTriggered,  
    i.NotClickingOnLeadsTriggered,
    i.NotRespondingToEmployerInvitesTriggered,
    i.ShowingLowQualityMatchesTriggered,
    i.PostingLowQualityResumeTriggered,
    i.PostHireFollowUpTriggered,
    i.FollowUpRequested,
    i.NotSearchingJobsTriggered,
    i.InappropriateEmailAddress,
    p.AssignedToId,
    re.IsVeteran,
    u.ExternalId
FROM  dbo.[Data.Application.User] AS u WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id 
	LEFT JOIN dbo.[Data.Application.Resume] AS re WITH (NOLOCK) ON p.Id = re.PersonId AND re.IsDefault = 1 AND re.IsActive = 1
    LEFT JOIN dbo.[Data.Application.Issues] AS i WITH (NOLOCK) on p.Id = i.PersonId  
WHERE (u.UserType & 4 = 4)








GO


