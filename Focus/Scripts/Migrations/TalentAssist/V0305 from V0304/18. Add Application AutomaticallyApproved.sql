

-- Add column AutomaticallyApproved of type Boolean to table Data.application.application
ALTER TABLE [Data.application.application] ADD [AutomaticallyApproved] BIT NULL;
