/****** Object:  View [dbo].[Data.Application.NewApplicantReferralView]    Script Date: 11/28/2013 17:32:32 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.NewApplicantReferralView]'))
DROP VIEW [dbo].[Data.Application.NewApplicantReferralView]
GO

/****** Object:  View [dbo].[Data.Application.NewApplicantReferralView]    Script Date: 11/28/2013 17:32:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Data.Application.NewApplicantReferralView]
AS
	SELECT 
		A.Id,
		P.Id AS PersonId,
		PO.JobTitle,
		PO.EmployerName,
		ApprovalRequiredReason AS ActionType,
		A.ApplicationStatus AS CandidateApplicationStatus,
		A.Id AS ApplicationId,
		R.IsVeteran,
		J.VeteranPriorityEndDate
	FROM 
		[Data.Application.Person] P
	INNER JOIN [Data.Application.Resume] R
		ON R.PersonId = P.Id
	INNER JOIN [Data.Application.Application] A
		ON A.ResumeId = R.Id
	INNER JOIN [Data.Application.Posting] PO
		ON PO.Id = A.PostingId
	INNER JOIN [Data.Application.Job] J
		ON J.Id = PO.JobId
	WHERE
		PO.JobId IS NOT NULL
		AND A.ApplicationStatus = 3

GO


