ALTER VIEW [dbo].[EmployeeSearchView] AS
SELECT     
 dbo.Employee.Id,     
 dbo.Person.FirstName,     
 dbo.Person.LastName,    
 dbo.Person.EmailAddress,    
 dbo.PhoneNumber.Number AS PhoneNumber,    
 dbo.Employer.Id AS EmployerId,     
 dbo.Employer.Name AS EmployerName,     
 dbo.BusinessUnit.IndustrialClassification,     
 dbo.Employer.FederalEmployerIdentificationNumber,     
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 1)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 2)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 4)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 8)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 16)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 32)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 64)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 128)) AS WorkOpportunitiesTaxCreditHires,     
 dbo.Person.JobTitle,     
 SUM(DISTINCT(PostingFlags & 1)) +    
 SUM(DISTINCT(PostingFlags & 2)) +    
 SUM(DISTINCT(PostingFlags & 4)) +    
 SUM(DISTINCT(PostingFlags & 8)) AS PostingFlags,     
 Max(dbo.Job.ScreeningPreferences) AS ScreeningPreferences,    
 dbo.Employer.CreatedOn AS EmployerCreatedOn,    
 dbo.[User].[Enabled] AS AccountEnabled,    
 dbo.Employer.ApprovalStatus AS EmployerApprovalStatus,    
 dbo.Employee.ApprovalStatus AS EmployeeApprovalStatus,  
 dbo.BusinessUnitAddress.TownCity,  
 dbo.LookupItemsView.Value AS [State],
 dbo.BusinessUnit.Name AS BusinessUnitName,
 dbo.BusinessUnit.Id AS BusinessUnitId
FROM      
 dbo.Person WITH (NOLOCK)    
 INNER JOIN dbo.Employee WITH (NOLOCK) ON dbo.Person.Id = dbo.Employee.PersonId    
 INNER JOIN dbo.Employer WITH (NOLOCK) ON dbo.Employee.EmployerId = dbo.Employer.Id  
 LEFT OUTER JOIN dbo.PhoneNumber WITH (NOLOCK) ON dbo.Person.Id = dbo.PhoneNumber.PersonId AND dbo.PhoneNumber.IsPrimary = 1    
 INNER JOIN dbo.[User] WITH (NOLOCK) ON dbo.Person.Id = dbo.[User].PersonId     
 INNER JOIN dbo.EmployeeBusinessUnit WITH (NOLOCK) ON dbo.Employee.Id = dbo.EmployeeBusinessUnit.EmployeeId
 INNER JOIN dbo.BusinessUnit WITH (NOLOCK) ON dbo.EmployeeBusinessUnit.BusinessUnitId = dbo.BusinessUnit.Id
 INNER JOIN dbo.BusinessUnitAddress WITH (NOLOCK) ON dbo.BusinessUnit.Id = dbo.BusinessUnitAddress.BusinessUnitId
 INNER JOIN dbo.LookupItemsView WITH (NOLOCK) ON dbo.BusinessUnitAddress.StateId = dbo.LookupItemsView.Id
 LEFT OUTER JOIN dbo.Job WITH (NOLOCK) ON dbo.BusinessUnit.Id = dbo.Job.BusinessUnitId
GROUP BY    
 dbo.Employee.Id,     
 dbo.Person.FirstName,     
 dbo.Person.LastName,    
 dbo.Person.EmailAddress,    
 dbo.PhoneNumber.Number,     
 dbo.Employer.Id,     
 dbo.Employer.Name,     
  dbo.BusinessUnit.IndustrialClassification,     
  dbo.Employer.FederalEmployerIdentificationNumber,    
  dbo.Person.JobTitle,    
  dbo.Employer.CreatedOn,    
  dbo.[User].[Enabled],    
  dbo.Employer.ApprovalStatus,    
  dbo.Employee.ApprovalStatus,  
 dbo.BusinessUnitAddress.TownCity,  
 dbo.LookupItemsView.Value,
 dbo.BusinessUnit.Name,
  dbo.BusinessUnit.Id