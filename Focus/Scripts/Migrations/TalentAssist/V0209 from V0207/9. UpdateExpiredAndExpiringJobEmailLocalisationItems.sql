UPDATE [LocalisationItem]  SET [Value] = 'Message from #FOCUSTALENT#; Job order {0}, ID:{1} has been closed.'  WHERE [Id] = 1707668
UPDATE [LocalisationItem]  SET [Value] =  
'The following job order has been automatically closed as it has reached it''s closing date.
  
Job order title; {0}
Job order ID;	{1}
Job order closing date; {2}
DBA name; {3}'
WHERE [Id] = 1707669

UPDATE [LocalisationItem]  SET [Value] = 'Message from #FOCUSTALENT#; Job order {0}, ID:{1} will expire in {2} days.'  WHERE [Id] = 1707438
UPDATE [LocalisationItem]  SET [Value] =  
'The following job order will expire in {4} days.
To extend the closing date on your posting, please log in to your #FOCUSTALENT# account and run the Refresh action found against the job order.
  
Job order title; {0}
Job order ID;	{1}
Job order closing date; {2}
DBA name; {3}'
  
WHERE [Id] = 1707439