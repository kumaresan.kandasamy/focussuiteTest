/*
Run this script on:

        BOSDBSVR001.FocusStage_Backup_20130109    -  This database will be modified

to synchronize it with:

        BOSDBSVR001.FocusStage

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 09/01/2013 09:42:06

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[Job]'
GO
ALTER TABLE [dbo].[Job] ADD
[IsConfidential] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobPostingReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobPostingReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ExpiringJobView]'
GO
EXEC sp_refreshview N'[dbo].[ExpiringJobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[JobView]'
GO
ALTER VIEW dbo.JobView
AS
SELECT j.Id, j.EmployerId, e.Name AS EmployerName, j.JobTitle, bu.Name AS BusinessUnitName, j.BusinessUnitId, j.JobStatus, j.PostedOn, j.ClosingOn, j.HeldOn, 
               j.ClosedOn, j.UpdatedOn, ISNULL(cag.ApplicationCount, 0) AS ApplicationCount, j.EmployeeId, j.ApprovalStatus, ISNULL(crg.ReferralCount, 0) AS ReferralCount, 
               j.FederalContractor, j.ForeignLabourCertificationH2A, j.ForeignLabourCertificationH2B, j.ForeignLabourCertificationOther, j.CourtOrderedAffirmativeAction, 
               u.UserName, j.CreatedOn, j.YellowProfanityWords, j.IsConfidential
FROM  dbo.Job AS j WITH (NOLOCK) LEFT OUTER JOIN
               dbo.Employee AS employee WITH (NOLOCK) ON j.EmployeeId = employee.Id LEFT OUTER JOIN
               dbo.[User] AS u WITH (NOLOCK) ON employee.PersonId = u.PersonId LEFT OUTER JOIN
               dbo.Employer AS e WITH (NOLOCK) ON j.EmployerId = e.Id LEFT OUTER JOIN
               dbo.BusinessUnit AS bu WITH (NOLOCK) ON bu.Id = j.BusinessUnitId LEFT OUTER JOIN
                   (SELECT JobId, COUNT(1) AS ApplicationCount
                    FROM   dbo.CandidateApplication AS ca WITH (NOLOCK)
                    WHERE (ApprovalStatus = 2)
                    GROUP BY JobId) AS cag ON cag.JobId = j.Id LEFT OUTER JOIN
                   (SELECT JobId, COUNT(1) AS ReferralCount
                    FROM   dbo.CandidateApplication AS ca WITH (NOLOCK)
                    WHERE (ApprovalStatus = 1)
                    GROUP BY JobId) AS crg ON crg.JobId = j.Id
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobSeekerReferralView]'
GO
EXEC sp_refreshview N'[dbo].[JobSeekerReferralView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationStatusLogView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationStatusLogView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobActionEventView]'
GO
EXEC sp_refreshview N'[dbo].[JobActionEventView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[EmployeeSearchView]'
GO
EXEC sp_refreshview N'[dbo].[EmployeeSearchView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
