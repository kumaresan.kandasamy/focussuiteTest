
ALTER VIEW [dbo].[ExpiringJobView]
AS
SELECT     dbo.Job.Id, dbo.Job.JobTitle, dbo.Job.ClosingOn, DATEDIFF(day, GETDATE(), dbo.Job.ClosingOn) AS DaysToClosing, dbo.Job.EmployerId, dbo.Job.EmployeeId, 
                      dbo.Job.PostedOn, dbo.[User].Id AS UserID, dbo.Person.FirstName, dbo.Job.BusinessUnitId, bu.Name AS BusinessUnitName
FROM         dbo.Job WITH (NOLOCK) INNER JOIN
                      dbo.Employee WITH (NOLOCK) ON dbo.Job.EmployeeId = dbo.Employee.Id INNER JOIN
                      dbo.Person WITH (NOLOCK) ON dbo.Employee.PersonId = dbo.Person.Id INNER JOIN
                      dbo.[User] WITH (NOLOCK) ON dbo.Person.Id = dbo.[User].PersonId LEFT OUTER JOIN
											dbo.BusinessUnit AS bu WITH (NOLOCK) ON bu.Id = Job.BusinessUnitId



GO


