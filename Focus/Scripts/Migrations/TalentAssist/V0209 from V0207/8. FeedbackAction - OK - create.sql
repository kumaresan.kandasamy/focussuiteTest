BEGIN TRAN 

-- DECLARE Variables to track counts and ids
DECLARE @StartId BIGINT
DECLARE @NumberRowsToInsert BIGINT
DECLARE @MaxNextId BIGINT
DECLARE @CodeGroupCount INT
DECLARE @CodeItemCount INT

-- Declare table variables to capture the data we want to push to the db
DECLARE @Group TABLE (GroupKey NVARCHAR(200))
DECLARE @Item TABLE (GroupKey NVARCHAR(200), ItemKey NVARCHAR(200), Value NVARCHAR(MAX), ParentKey NVARCHAR(200) )

-- Populate @Groups with Code Groups
INSERT INTO @Group VALUES
('GeneralIssues'),
('JobSeekerIssues'),
('EmployerIssues'),
('CareerExplorationIssues'),
('StaffIssues')

-- Populate @Items with Code Items
INSERT INTO @Item VALUES
--FeedbackIssues
('FeedbackIssues', 'FeedbackIssues.GeneralIssues', 'General issues', null),
('FeedbackIssues', 'FeedbackIssues.JobSeekerIssues', 'Job seeker issues', null),
('FeedbackIssues', 'FeedbackIssues.EmployerIssues', 'Employer issues', null),
('FeedbackIssues', 'FeedbackIssues.CareerExplorationIssues', 'Career exploration issues', null),
('FeedbackIssues', 'FeedbackIssues.StaffIssues', 'Staff issues', null),
--GeneralIssues
('GeneralIssues', 'GeneralIssues.SystemIsDown', 'System is down', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.DataNotGoingToDatabaseAPIs', 'Data not going to database/APIs', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.SearchResults', 'Search results', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.MatchResultsStarMatches','Match results/Star matches', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.ResumesNotDisplaying', 'Resumes not displaying', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.JobsNotDisplaying', 'Jobs not displaying', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.SystemEmailNotWorking', 'System email not working', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.LanguageTranslation', 'Language translation', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.Printing', 'Printing', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.SystemPermissionsAccess', 'System permissions/access', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.Browsers', 'Browsers', 'FeedbackIssues.GeneralIssues'),
('GeneralIssues', 'GeneralIssues.OtherIssue', 'Other issue', 'FeedbackIssues.GeneralIssues'),
--JobSeekerIssues
('JobSeekerIssues', 'JobSeekerIssues.SeekerAccountRegistration', 'Seeker account registration', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.SeekerCantSignIntoAccount', 'Seeker can''t sign into account', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.ResumeBuildingEditing', 'Resume building/editing', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.ResumeUploadDownload', 'Resume upload/download', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.ResumeReviewDisplay', 'Resume review/display', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.MultipleResumes', 'Multiple resumes', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.JobAlerts', 'Job alerts', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.CantSearchForApplyToJobs', 'Can''t search for/apply to jobs', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.CantFindMoreJobsLikeThis', 'Can''t find more jobs like this', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.CantReportOutcomes', 'Can''t report outcomes', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.CantUseAccountSettings', 'Can''t use Account settings', 'FeedbackIssues.JobSeekerIssues'),
('JobSeekerIssues', 'JobSeekerIssues.OtherIssue', 'Other issue', 'FeedbackIssues.JobSeekerIssues'),
--EmployerIssues
('EmployerIssues', 'EmployerIssues.EmployerAccountRegistration', 'Employer account registration', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues', 'EmployerIssues.EmployerCantSignIntoAccount', 'Employer can''t sign into account', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.JobPostBuildingEditing', 'Job post building/editing', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.JobPostUploadDownload', 'Job post upload/download', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.JobPostReviewDisplay', 'Job post review/display', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.MultipleDBAsTradeNames', 'Multiple DBAs/trade names', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.ResumeAlerts', 'Resume alerts', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.CantSearchForManageApplicants', 'Can''t search for/manage applicants', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.CantFindMoreSeekersLikeThis', 'Can''t find more seekers like this', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.CantReportOutcomes', 'Can''t report outcomes', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.CantUseAccountSettings', 'Can''t use Account settings', 'FeedbackIssues.EmployerIssues'),
('EmployerIssues','EmployerIssues.OtherIssue', 'Other issue', 'FeedbackIssues.EmployerIssues'),
--CareerExplorationIssues
('CareerExplorationIssues','CareerExplorationIssues.CantSearchByLocationCriteria', 'Can''t search by location/criteria', 'FeedbackIssues.CareerExplorationIssues'),
('CareerExplorationIssues','CareerExplorationIssues.DataDoesNotDisplay', 'Data does not display', 'FeedbackIssues.CareerExplorationIssues'),
('CareerExplorationIssues','CareerExplorationIssues.CantSaveBookmarks', 'Can''t save bookmarks', 'FeedbackIssues.CareerExplorationIssues'),
('CareerExplorationIssues','CareerExplorationIssues.CantSeePostingsForAJob', 'Can''t see postings for a job', 'FeedbackIssues.CareerExplorationIssues'),
('CareerExplorationIssues','CareerExplorationIssues.CantUploadResumeForPersonalResults', 'Can''t upload resume for personal results', 'FeedbackIssues.CareerExplorationIssues'),
('CareerExplorationIssues','CareerExplorationIssues.CantMatchSkillsToJobChoices', 'Can''t match skills to job choices', 'FeedbackIssues.CareerExplorationIssues'),
('CareerExplorationIssues','CareerExplorationIssues.CantAnalyzeSkillGaps', 'Can''t analyze skill gaps', 'FeedbackIssues.CareerExplorationIssues'),
('CareerExplorationIssues','CareerExplorationIssues.OtherIssue', 'Other issue', 'FeedbackIssues.CareerExplorationIssues'),
--StaffIssues
('StaffIssues','StaffIssues.CantSignIntoStaffAccount', 'Can''t sign into staff account', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantAccessACustomerAccount', 'Can''t access a customer account', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantCreateACustomerAccount', 'Can''t create a customer account', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantFindACustomerRecord', 'Can''t find a customer record', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantEditSaveChangesToACustomerRecord', 'Can''t edit/save changes to a customer record', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantReferJobSeekers', 'Can''t refer job seekers', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.QueueReferralRequestsApprovals', 'Queue � Referral requests approvals', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.QueueEmployerAccountApprovals', 'Queue � Employer account approvals', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.QueueJobPostApprovals', 'Queue � Job post approvals', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.FiltersProfanityCriminaBackground', 'Filters � profanity, criminal background', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantPostActivitiesServices', 'Can''t post activities/services', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantShareMyAccount', 'Can''t share my account', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantUseAccountSettings', 'Can''t use Account settings', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.CantAssignCustomersToStaff', 'Can''t assign customers to staff', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.DashboardIssueFlags', 'Dashboard/issue flags', 'FeedbackIssues.StaffIssues'),
('StaffIssues','StaffIssues.OtherIssue', 'Other issue', 'FeedbackIssues.StaffIssues')

-- Delete current feedback issues
DELETE FROM CodeGroupItem WHERE CodeGroupId IN (SELECT Id FROM CodeGroup WHERE [Key] = 'FeedbackIssues')

-- Determine the number of rows to create
SELECT @CodeGroupCount = COUNT(*)  FROM @Group 
SELECT @CodeItemCount = COUNT(*)  FROM @Item 
-- 1 set for CodeGroup then a set for the LocalisationItem
-- 1 set for CodeItem, CodeGroupItem & LocalisationItem
SET @NumberRowsToInsert = (@CodeGroupCount) + (@CodeItemCount * 3)


-- Update the Next ID to be >= the number of rows we expect
UPDATE KeyTable SET NextId = NextId + @NumberRowsToInsert
SELECT @MaxNextId = NextId FROM KeyTable
SET @StartId = @MaxNextId - @NumberRowsToInsert

--SELECT @NumberRowsToInsert, @StartId

-- Create the CodeGroups from @Group
INSERT INTO CodeGroup (Id, [Key])
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY GroupKey DESC), GroupKey FROM @Group
SET @StartId = @StartId + @CodeGroupCount

-- Create the CodeItems from @Item
INSERT INTO CodeItem (Id, [Key], IsSystem, ParentKey)
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), ItemKey, 1, ParentKey  FROM (SELECT DISTINCT ItemKey, ParentKey FROM @Item) AS Tmp
SET @StartId = @StartId + @CodeItemCount

-- Create the CodeItems LocalisationItems from @Item
INSERT INTO LocalisationItem (Id, [Key], Value, LocalisationId, ContextKey, Localised)
SELECT @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), ItemKey, Value, 12090, '', 0 FROM @Item
SET @StartId = @StartId + @CodeItemCount

-- Create the link between CodeGroup & CodeItem from the @Item table
INSERT INTO CodeGroupItem (Id, DisplayOrder, CodeGroupId, CodeItemId)
SELECT  @StartId + ROW_NUMBER() OVER (ORDER BY ItemKey DESC), 0, cg.id, ci.id
FROM @Item i
INNER JOIN CodeGroup cg ON i.GroupKey = cg.[Key] 
INNER JOIN CodeItem ci ON i.ItemKey = ci.[Key]
ORDER BY i.GroupKey, i.ItemKey


SELECT * 
FROM CodeGroup cg
INNER JOIN CodeGroupItem cgi ON cg.Id = cgi.CodeGroupId
INNER JOIN LocalisationItem licg ON cg.[Key] = licg.[Key]
INNER JOIN CodeItem ci ON cgi.CodeItemId = ci.Id
INNER JOIN LocalisationItem lici ON ci.[Key] = lici.[Key]
WHERE cg.[Key] IN (SELECT GroupKey FROM @Group)
ORDER BY cg.[Key], ci.[Key]

SELECT COUNT(*) FROM @Item WHERE GroupKey = 'CareerExplorationIssues'

SELECT COUNT(*)
FROM CodeGroup cg
INNER JOIN CodeGroupItem cgi ON cg.Id = cgi.CodeGroupId
INNER JOIN CodeItem ci ON cgi.CodeItemId = ci.Id
WHERE cg.[Key] = 'CareerExplorationIssues'

SELECT MAX(Id), @MaxNextId FROM CodeGroupItem

COMMIT 