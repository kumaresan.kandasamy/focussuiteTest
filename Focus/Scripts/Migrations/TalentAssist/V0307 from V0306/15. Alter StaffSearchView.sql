-- Add the Blocked field to the StaffSearchView for Deactivating/Activating staff accounts.
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[Data.Application.StaffSearchView]
AS

SELECT 
	[User].Id, 
	Person.FirstName, 
	Person.LastName,
	Person.EmailAddress,
	Person.ExternalOffice,
	[User].[Enabled],
	Person.Id AS PersonId,
	Person.Manager,
	[User].Blocked
FROM  
	[Data.Application.Person] AS Person WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId
	INNER JOIN dbo.[Data.Application.UserRole] AS UserRole WITH (NOLOCK) ON [User].Id = UserRole.UserId
	INNER JOIN dbo.[Data.Application.Role] AS Role WITH (NOLOCK) ON UserRole.RoleId = [Role].Id
WHERE
	[Role].[Key] = 'AssistUser'	

GO

