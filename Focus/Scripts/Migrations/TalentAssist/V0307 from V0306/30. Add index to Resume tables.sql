IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.Resume_Person' AND object_id = OBJECT_ID('[Data.Application.Resume]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.Resume_Person]
		ON [Data.Application.Resume] (PersonId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.ResumeDocument_ResumeId' AND object_id = OBJECT_ID('[Data.Application.ResumeDocument]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.ResumeDocument_ResumeId]
		ON [Data.Application.ResumeDocument] (ResumeId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.ResumeJob_ResumeId' AND object_id = OBJECT_ID('[Data.Application.ResumeJob]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.ResumeJob_ResumeId]
		ON [Data.Application.ResumeJob] (ResumeId)
END