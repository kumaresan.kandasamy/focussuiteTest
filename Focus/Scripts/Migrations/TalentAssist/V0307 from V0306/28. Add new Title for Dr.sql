DECLARE @LocalisationId BIGINT
DECLARE @CodeGroupId BIGINT
DECLARE @CodeItemId BIGINT

SELECT @LocalisationId = Id FROM [Config.Localisation] WHERE Culture = '**-**'
SELECT @CodeGroupId = Id FROM [Config.CodeGroup] WHERE [Key] = 'Titles'

BEGIN TRANSACTION

IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'Title.Dr')
BEGIN
	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], Value, Localised, LocalisationId)
	VALUES ('', 'Title.Dr', 'Dr', 0, @LocalisationId)
END

IF NOT EXISTS(SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'Title.Dr')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ParentKey, ExternalId, CustomFilterId) 
	VALUES ('Title.Dr', 0, NULL, NULL, NULL)
	
	SET @CodeItemId = SCOPE_IDENTITY()
	
	INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId)
	VALUES(0, @CodeGroupId, @CodeItemId)
END

COMMIT TRANSACTION
