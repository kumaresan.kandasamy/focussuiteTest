DELETE
	CGI
FROM [Config.CodeGroup] CG
INNER JOIN [Config.CodeGroupItem] CGI
	ON CGI.CodeGroupId = CG.Id
INNER JOIN [Config.CodeItem] CI
	ON CI.Id = CGI.CodeItemId
WHERE CG.[Key] = 'Counties'
	AND CI.[Key] = 'County.OutsideUS'
	AND EXISTS
	(
		SELECT TOP 1 
			1 
		FROM 
			[Config.CodeGroupItem] CGI2 
		WHERE 
			CGI2.CodeGroupId = CGI.CodeGroupId
			AND CGI2.CodeItemId = CGI.CodeItemId
			AND CGI2.Id < CGI.Id
	)
