IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.PersonAddress_Person' AND object_id = OBJECT_ID('[Data.Application.PersonAddress]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.PersonAddress_Person]
		ON [Data.Application.PersonAddress] (PersonId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.Issues_Person' AND object_id = OBJECT_ID('[Data.Application.Issues]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.Issues_Person]
		ON [Data.Application.Issues] (PersonId)
END
