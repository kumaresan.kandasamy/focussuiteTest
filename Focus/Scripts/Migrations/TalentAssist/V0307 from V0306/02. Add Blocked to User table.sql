
-- Add column Blocked of type Boolean to table Data.application.user
ALTER TABLE [Data.application.user] ADD [Blocked] BIT NOT NULL DEFAULT 0;
