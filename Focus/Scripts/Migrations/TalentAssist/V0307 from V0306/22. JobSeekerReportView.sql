
/****** Object:  View [dbo].[Data.Application.JobSeekerReportView]    Script Date: 03/25/2014 15:34:29 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobSeekerReportView]'))
DROP VIEW [dbo].[Data.Application.JobSeekerReportView]
GO

/****** Object:  View [dbo].[Data.Application.JobSeekerReportView]    Script Date: 03/25/2014 15:34:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.JobSeekerReportView]

AS
	SELECT 
		P.Id,
		U.UserType,
		P.FirstName,
		P.LastName,
		P.MiddleInitial,
		P.EmailAddress,
		PA.Line1,
		PA.CountyId,
		PA.StateId,
		PA.PostcodeZip,
		P.SocialSecurityNumber
	FROM
		[Data.Application.Person] P
	INNER JOIN [Data.Application.User] U
		ON U.PersonId = P.Id
	LEFT OUTER JOIN [Data.Application.PersonAddress] PA
		ON PA.PersonId = P.Id
		AND PA.IsPrimary = 1

GO


