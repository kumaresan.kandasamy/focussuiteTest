DECLARE @Items TABLE
(
	[Key] NVARCHAR(400),
	[Value] NVARCHAR(1000)
)

DECLARE @LocalisationId bigint

SELECT @LocalisationId = [Id] FROM [Config.Localisation] WHERE Culture = '**-**'

INSERT INTO @Items ([Key], [Value])
VALUES('ErrorType.UserBlocked', 'User blocked')

DELETE TI FROM @Items TI
INNER JOIN [Config.LocalisationItem]  LI ON LI.[Key] = TI.[Key]

BEGIN TRANSACTION

INSERT INTO [Config.LocalisationItem]  ([Key], [Value], LocalisationId, ContextKey, Localised)
SELECT [Key], [Value], @LocalisationId, '', 0
FROM @Items

COMMIT TRANSACTION