
IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobOrdersOpen')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.EmployerAction'
	AND C.name = 'JobOrdersOpen'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.EmployerAction] DROP COLUMN JobOrdersOpen
END
GO

IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'JobSeekersRejected')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.EmployerAction'
	AND C.name = 'JobSeekersRejected'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.EmployerAction] DROP COLUMN JobSeekersRejected
END
GO

IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.EmployerAction' AND COLUMN_NAME = 'ResumesReferred')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.EmployerAction'
	AND C.name = 'ResumesReferred'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.EmployerAction] DROP COLUMN ResumesReferred
END
GO

IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobOrderAction' AND COLUMN_NAME = 'ApplicantsRejected')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.JobOrderAction'
	AND C.name = 'ApplicantsRejected'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.JobOrderAction] DROP COLUMN ApplicantsRejected
END
GO