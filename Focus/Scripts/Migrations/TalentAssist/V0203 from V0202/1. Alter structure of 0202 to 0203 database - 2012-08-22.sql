
/*
Run this script on:

        10.0.1.35.FocusTR    -  This database will be modified

to synchronize it with:

        10.0.1.35.FocusDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 22/08/2012 17:03:48

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[ActionType]'
GO
ALTER TABLE [dbo].ActionType ADD
[ReportOn] [bit] NULL,
[AssistAction] [bit] NULL
GO
UPDATE [dbo].ActionType 
SET ReportOn = 0
GO
UPDATE [dbo].ActionType 
SET ReportOn = 1
WHERE Name IN ('LogIn', 'PostJob', 'ReferJob')
GO
PRINT N'Creating [dbo].[CandidateWorkHistory]'
GO
CREATE TABLE [dbo].[CandidateWorkHistory]
(
[Id] [bigint] NOT NULL,
[JobTitle] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Employer] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartYear] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndYear] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CandidateId] [bigint] NOT NULL CONSTRAINT [DF__Candidate__Candi__1ADEEA9C] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Candidat__3214EC0718F6A22A] on [dbo].[CandidateWorkHistory]'
GO
ALTER TABLE [dbo].[CandidateWorkHistory] ADD CONSTRAINT [PK__Candidat__3214EC0718F6A22A] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Candidate]'
GO
ALTER TABLE [dbo].[Candidate] ADD
[YearsExperience] [int] NULL,
[IsVeteran] [bit] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[CandidateNote]'
GO
ALTER TABLE [dbo].[CandidateNote] ADD
[DateAdded] [datetime] NOT NULL CONSTRAINT [DF_CandidateNote_DateAdded] DEFAULT (getdate())
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[StaffProfile]'
GO
CREATE VIEW [dbo].[StaffProfile]
AS
SELECT 
	dbo.[User].Id, 
	dbo.Person.FirstName, 
	dbo.Person.LastName,
	dbo.Person.EmailAddress,
	dbo.Person.Office,
	dbo.[User].[Enabled],
	pn.Number as PhoneNumber,
	pn1.Number as FaxNumber,
	pa.TownCity,
	pa.StateId
FROM  
	dbo.Person WITH (NOLOCK)
	INNER JOIN dbo.[User] WITH (NOLOCK) ON dbo.Person.Id = dbo.[User].PersonId
	LEFT JOIN dbo.PersonAddress pa WITH (NOLOCK) ON dbo.Person.Id = pa.PersonId
	LEFT JOIN dbo.PhoneNumber pn WITH (NOLOCK) ON Person.Id = pn.PersonId and pn.PhoneType = 0
	LEFT JOIN dbo.PhoneNumber pn1 WITH (NOLOCK) ON Person.Id = pn1.PersonId and pn.PhoneType = 4
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobView]'
GO
EXEC sp_refreshview N'[dbo].[JobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PersonResume]'
GO
CREATE TABLE [dbo].[PersonResume]
(
[Id] [bigint] NOT NULL,
[FileName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__PersonRes__FileN__133DC8D4] DEFAULT (''),
[ContentType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Resume] [varbinary] (max) NOT NULL,
[PrimaryOnet] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PersonId] [bigint] NOT NULL CONSTRAINT [DF__PersonRes__Perso__15261146] DEFAULT ((0)),
[PrimaryROnet] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResumeSkills] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__PersonRe__3214EC0711558062] on [dbo].[PersonResume]'
GO
ALTER TABLE [dbo].[PersonResume] ADD CONSTRAINT [PK__PersonRe__3214EC0711558062] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Profanity]'
GO
ALTER TABLE [dbo].[Profanity] ADD
[WhiteList] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ExpiringJobView]'
GO

ALTER VIEW [dbo].[ExpiringJobView]
AS
SELECT     dbo.Job.Id, dbo.Job.JobTitle, dbo.Job.ClosingOn, DATEDIFF(day, GETDATE(), dbo.Job.ClosingOn) AS DaysToClosing, dbo.Job.EmployerId, dbo.Job.EmployeeId, 
                      dbo.Job.PostedOn, dbo.[User].Id AS UserID, dbo.Person.FirstName
FROM         dbo.Job WITH (NOLOCK) INNER JOIN
                      dbo.Employee WITH (NOLOCK) ON dbo.Job.EmployeeId = dbo.Employee.Id INNER JOIN
                      dbo.Person WITH (NOLOCK) ON dbo.Employee.PersonId = dbo.Person.Id INNER JOIN
                      dbo.[User] WITH (NOLOCK) ON dbo.Person.Id = dbo.[User].PersonId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[ApplicationView]'
GO
EXEC sp_refreshview N'[dbo].[ApplicationView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[LookupItemsView]'
GO
EXEC sp_refreshview N'[dbo].[LookupItemsView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[JobSeekerReferralView]'
GO
ALTER VIEW dbo.JobSeekerReferralView
AS
SELECT dbo.CandidateApplication.Id, dbo.Candidate.FirstName + ' ' + dbo.Candidate.LastName AS Name, dbo.CandidateApplication.CreatedOn AS ReferralDate, 
               dbo.GetBusinessDays(dbo.CandidateApplication.CreatedOn, GETDATE()) AS TimeInQueue, dbo.Job.JobTitle, dbo.Employer.Name AS EmployerName, 
               dbo.Employer.Id AS EmployerId, dbo.Job.Posting, dbo.Candidate.Id AS CandidateId, dbo.Job.Id AS JobId, dbo.Candidate.ExternalId AS CandidateExternalId, 
               dbo.CandidateApplication.ApprovalRequiredReason, 
               CASE WHEN dbo.Candidate.IsVeteran = 1 THEN 'Yes' WHEN dbo.Candidate.IsVeteran = 0 THEN 'No' ELSE '' END AS Veteran, 
               dbo.EmployerAddress.TownCity AS Town, dbo.LookupItemsView.Value AS State
FROM  dbo.Candidate WITH (NOLOCK) INNER JOIN
               dbo.CandidateApplication WITH (NOLOCK) ON dbo.Candidate.Id = dbo.CandidateApplication.CandidateId INNER JOIN
               dbo.Job WITH (NOLOCK) ON dbo.CandidateApplication.JobId = dbo.Job.Id INNER JOIN
               dbo.Employer WITH (NOLOCK) ON dbo.Job.EmployerId = dbo.Employer.Id INNER JOIN
               dbo.EmployerAddress ON dbo.Employer.Id = dbo.EmployerAddress.EmployerId INNER JOIN
               dbo.LookupItemsView ON dbo.EmployerAddress.StateId = dbo.LookupItemsView.Id
WHERE (dbo.CandidateApplication.ApprovalStatus = 1)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PersonListCandidateView]'
GO






ALTER VIEW [dbo].[PersonListCandidateView]
AS

SELECT 
	PersonListCandidate.Id AS Id,
	PersonList.ListType AS ListType,
	PersonList.PersonId AS PersonId,
	Candidate.Id AS CandidateId,
	Candidate.ExternalId AS CandidateExternalId,
	Candidate.FirstName AS CandidateFirstName,
	Candidate.LastName AS CandidateLastName,
	ISNULL(Candidate.IsVeteran, 'false') AS CandidateIsVeteran
FROM 
	PersonListCandidate WITH (NOLOCK)
	INNER JOIN PersonList WITH (NOLOCK) ON PersonListCandidate.PersonListId = PersonList.Id
	INNER JOIN Candidate WITH (NOLOCK) ON PersonListCandidate.CandidateId = Candidate.Id



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[CandidateNoteView]'
GO
ALTER VIEW [dbo].[CandidateNoteView]  
AS  

SELECT   
 CandidateNote.Id AS Id,  
 CandidateNote.Note AS Note,  
 CandidateNote.EmployerId AS EmployerId,  
 Candidate.Id AS CandidateId,  
 Candidate.ExternalId AS CandidateExternalId,
 CandidateNote.DateAdded AS NoteAddedDateTime,
 Employer.Name AS EmployerName  
FROM   
 CandidateNote WITH (NOLOCK)  
 INNER JOIN Candidate WITH (NOLOCK) ON CandidateNote.CandidateId = Candidate.Id  
 INNER JOIN Employer WITH (NOLOCK) ON CandidateNote.EmployerID = Employer.ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[EmployeeSearchView]'
GO
ALTER VIEW [dbo].[EmployeeSearchView] AS
SELECT     
 dbo.Employee.Id,     
 dbo.Person.FirstName,     
 dbo.Person.LastName,    
 dbo.Person.EmailAddress,    
 dbo.PhoneNumber.Number AS PhoneNumber,    
 dbo.Employer.Id AS EmployerId,     
 dbo.Employer.Name AS EmployerName,     
 dbo.Employer.IndustrialClassification,     
 dbo.Employer.FederalEmployerIdentificationNumber,     
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 1)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 2)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 4)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 8)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 16)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 32)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 64)) +    
 SUM(DISTINCT(WorkOpportunitiesTaxCreditHires & 128)) AS WorkOpportunitiesTaxCreditHires,     
 dbo.Person.JobTitle,     
 SUM(DISTINCT(PostingFlags & 1)) +    
 SUM(DISTINCT(PostingFlags & 2)) +    
 SUM(DISTINCT(PostingFlags & 4)) +    
 SUM(DISTINCT(PostingFlags & 8)) AS PostingFlags,     
 Max(dbo.Job.ScreeningPreferences) AS ScreeningPreferences,    
 dbo.Employer.CreatedOn AS EmployerCreatedOn,    
 dbo.[User].[Enabled] AS AccountEnabled,    
 dbo.Employer.ApprovalStatus AS EmployerApprovalStatus,    
 dbo.Employee.ApprovalStatus AS EmployeeApprovalStatus,  
 dbo.EmployerAddress.TownCity,  
 dbo.LookupItemsView.Value AS [State],
 dbo.BusinessUnit.Name AS BusinessUnitName
FROM      
 dbo.Person WITH (NOLOCK)    
 INNER JOIN dbo.Employee WITH (NOLOCK) ON dbo.Person.Id = dbo.Employee.PersonId    
 INNER JOIN dbo.Employer WITH (NOLOCK) ON dbo.Employee.EmployerId = dbo.Employer.Id  
 LEFT OUTER JOIN dbo.Job WITH (NOLOCK) ON dbo.Employer.Id = dbo.Job.EmployerId    
 LEFT OUTER JOIN dbo.PhoneNumber WITH (NOLOCK) ON dbo.Person.Id = dbo.PhoneNumber.PersonId AND dbo.PhoneNumber.IsPrimary = 1    
 INNER JOIN dbo.[User] WITH (NOLOCK) ON dbo.Person.Id = dbo.[User].PersonId     
 INNER JOIN dbo.EmployerAddress WITH (NOLOCK) ON dbo.Employer.Id = dbo.EmployerAddress.EmployerId  
 INNER JOIN dbo.LookupItemsView WITH (NOLOCK) ON dbo.EmployerAddress.StateId = dbo.LookupItemsView.Id
 LEFT JOIN dbo.BusinessUnit WITH (NOLOCK) ON dbo.Employer.Id = dbo.BusinessUnit.EmployerId
GROUP BY    
 dbo.Employee.Id,     
 dbo.Person.FirstName,     
 dbo.Person.LastName,    
 dbo.Person.EmailAddress,    
 dbo.PhoneNumber.Number,     
 dbo.Employer.Id,     
 dbo.Employer.Name,     
  dbo.Employer.IndustrialClassification,     
  dbo.Employer.FederalEmployerIdentificationNumber,    
  dbo.Person.JobTitle,    
  dbo.Employer.CreatedOn,    
  dbo.[User].[Enabled],    
  dbo.Employer.ApprovalStatus,    
  dbo.Employee.ApprovalStatus,  
 dbo.EmployerAddress.TownCity,  
 dbo.LookupItemsView.Value,
 dbo.BusinessUnit.Name
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CandidateWorkHistory]'
GO
ALTER TABLE [dbo].[CandidateWorkHistory] ADD CONSTRAINT [FK__Candidate__Candi__1BD30ED5] FOREIGN KEY ([CandidateId]) REFERENCES [dbo].[Candidate] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PersonResume]'
GO
ALTER TABLE [dbo].[PersonResume] ADD CONSTRAINT [FK__PersonRes__Perso__161A357F] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating extended properties'
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Job"
            Begin Extent = 
               Top = 4
               Left = 7
               Bottom = 262
               Right = 551
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employee"
            Begin Extent = 
               Top = 15
               Left = 580
               Bottom = 132
               Right = 743
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Person"
            Begin Extent = 
               Top = 13
               Left = 777
               Bottom = 130
               Right = 969
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User"
            Begin Extent = 
               Top = 264
               Left = 38
               Bottom = 381
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'ExpiringJobView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'ExpiringJobView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -373
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Candidate"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 285
               Right = 237
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CandidateApplication"
            Begin Extent = 
               Top = 11
               Left = 298
               Bottom = 152
               Right = 536
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Job"
            Begin Extent = 
               Top = 81
               Left = 710
               Bottom = 222
               Right = 1006
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "Employer"
            Begin Extent = 
               Top = 448
               Left = 48
               Bottom = 589
               Right = 395
            End
            DisplayFlags = 280
            TopColumn = 18
         End
         Begin Table = "EmployerAddress"
            Begin Extent = 
               Top = 480
               Left = 598
               Bottom = 621
               Right = 825
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "LookupItemsView"
            Begin Extent = 
               Top = 321
               Left = 981
               Bottom = 462
               Right = 1165
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
   ', 'SCHEMA', N'dbo', 'VIEW', N'JobSeekerReferralView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'      Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'JobSeekerReferralView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'JobSeekerReferralView', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
