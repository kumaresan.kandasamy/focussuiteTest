IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobSeekerSurveyTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Data.Application.JobSeekerSurveyTotals]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:		Jane Harrison
-- Create date: 01 November 2013
-- Description:	Get job seeker survey totals for the job seeker
-- ===============================================================
CREATE PROCEDURE [dbo].[Data.Application.JobSeekerSurveyTotals]
	@JobSeekerId BIGINT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		JSS.PersonId AS Id,
		COUNT(CASE JSS.SatisfactionLevel WHEN 0 THEN 1 END) AS VerySatisfied,
		COUNT(CASE JSS.SatisfactionLevel WHEN 1 THEN 1 END) AS SomewhatSatisfied,
		COUNT(CASE JSS.SatisfactionLevel WHEN 2 THEN 1 END) AS SomewhatDissatisfied,
		COUNT(CASE JSS.SatisfactionLevel WHEN 3 THEN 1 END) AS Dissatisfied,
		COUNT(NULLIF(JSS.UnanticipatedMatches, 0)) AS UnanticipatedMatches,
		COUNT(NULLIF(JSS.UnanticipatedMatches, 1)) AS NoUnanticipatedMatches,
		COUNT(NULLIF(JSS.WasInvitedDidApply, 0)) AS WasInvitedDidApply,
		COUNT(NULLIF(JSS.WasInvitedDidApply, 1)) AS WasNotInvitedOrDidNotApply
	FROM 
	 [Data.Application.JobSeekerSurvey] AS JSS 
	WHERE 
	 JSS.PersonId = @JobSeekerId
	GROUP BY JSS.PersonId
	
END
GO

GRANT EXEC ON [dbo].[Data.Application.JobSeekerSurveyTotals] TO [FocusAgent]
GO