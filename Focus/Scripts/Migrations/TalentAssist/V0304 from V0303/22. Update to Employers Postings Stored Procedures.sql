SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[Data_Application_GetEmployersPostingsOrderByNumberOfPositionsAsc]'
GO



-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Number of Positions Asc 
-- ======================================================================
ALTER PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByNumberOfPositionsAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@MinimumNumberOfPostings int,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	DECLARE @EmployerRowCount int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
	ORDER BY 
		COUNT(Id) desc, EmployerName
		
	SELECT @EmployerRowCount = @@ROWCOUNT
	
	IF @RowCount = 0 OR @EmployerRowCount <= @RowCount
	BEGIN
		SELECT @RowCount = @EmployerRowCount
	END
	ELSE
	BEGIN 
		DELETE FROM @Employers
		WHERE
			TableId > @RowCount
	END	

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, PostingDate DESC
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameAsc]'
GO



-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Employer Name Asc 
-- ======================================================================
ALTER PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@MinimumNumberOfPostings int,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	DECLARE @EmployerRowCount int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)
	
	DECLARE @EmployersOrderedByPostings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	-- First if required get the top employers based on number of postings
	INSERT INTO @EmployersOrderedByPostings
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
	ORDER BY 
		COUNT(Id) DESC, EmployerName
	
	SELECT @EmployerRowCount = @@ROWCOUNT
	
	IF @RowCount > 0 AND @EmployerRowCount >= @RowCount 
	BEGIN
		DELETE @EmployersOrderedByPostings 
		WHERE
			TableId > @RowCount
	END
	
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		PostingsCount
	FROM 
		@EmployersOrderedByPostings 
	ORDER BY 
		EmployerName	

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, EmployerName, PostingDate DESC
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameDesc]'
GO



-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Employer Name Desc 
-- ======================================================================
ALTER PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@MinimumNumberOfPostings int,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	DECLARE @EmployerRowCount int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)
	
	DECLARE @EmployersOrderedByPostings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	-- First if required get the top employers based on number of postings
	INSERT INTO @EmployersOrderedByPostings
	SELECT 
		EmployerName,
		COUNT(Id)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
	ORDER BY 
		COUNT(Id) DESC, EmployerName
	
	SELECT @EmployerRowCount = @@ROWCOUNT
	
	IF @RowCount > 0 AND @EmployerRowCount >= @RowCount 
	BEGIN
		DELETE @EmployersOrderedByPostings 
		WHERE
			TableId > @RowCount
	END
	
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		PostingsCount
	FROM 
		@EmployersOrderedByPostings
	ORDER BY 
		EmployerName DESC	

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, PostingDate DESC
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleDesc]'
GO





-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Job Title Desc 
-- ======================================================================
ALTER PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@MinimumNumberOfPostings int,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	DECLARE @EmployerRowCount int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)
	
	DECLARE @EmployersOrderedByPostings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int,
		MaxJobTitle nvarchar(255)
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	-- First if required get the top employers based on number of postings
	INSERT INTO @EmployersOrderedByPostings
	SELECT 
		EmployerName,
		COUNT(Id),
		MAX(JobTitle)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
	ORDER BY 
		COUNT(Id) DESC, EmployerName
	
	SELECT @EmployerRowCount = @@ROWCOUNT
	
	IF @RowCount > 0 AND @EmployerRowCount >= @RowCount 
	BEGIN
		DELETE @EmployersOrderedByPostings 
		WHERE
			TableId > @RowCount
	END
	
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		PostingsCount
	FROM 
		@EmployersOrderedByPostings
	ORDER BY 
		MaxJobTitle DESC
		

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, JobTitle DESC, PostingDate DESC
END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleAsc]'
GO





-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Job Title Asc 
-- ======================================================================
ALTER PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@MinimumNumberOfPostings int,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	DECLARE @EmployerRowCount int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)
	
	DECLARE @EmployersOrderedByPostings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int,
		MinJobTitle nvarchar(255)
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	-- First if required get the top employers based on number of postings
	INSERT INTO @EmployersOrderedByPostings
	SELECT 
		EmployerName,
		COUNT(Id),
		MIN(JobTitle)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
	ORDER BY 
		COUNT(Id) DESC, EmployerName
	
	SELECT @EmployerRowCount = @@ROWCOUNT
	
	IF @RowCount > 0 AND @EmployerRowCount >= @RowCount 
	BEGIN
		DELETE @EmployersOrderedByPostings 
		WHERE
			TableId > @RowCount
	END
	
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		PostingsCount
	FROM 
		@EmployersOrderedByPostings
	ORDER BY 
		MinJobTitle
		

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, JobTitle, PostingDate DESC
END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateAsc]'
GO



-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Posting Date Asc 
-- ======================================================================
ALTER PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@MinimumNumberOfPostings int,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	DECLARE @EmployerRowCount int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)
	
	DECLARE @EmployersOrderedByPostings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int,
		MaxPostingDate datetime
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
	
	-- Populate the employer details
	-- First if required get the top employers based on number of postings
	INSERT INTO @EmployersOrderedByPostings
	SELECT 
		EmployerName,
		COUNT(Id),
		MAX(PostingDate)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
	ORDER BY 
		COUNT(Id) DESC, EmployerName
	
	SELECT @EmployerRowCount = @@ROWCOUNT
	
	IF @RowCount > 0 AND @EmployerRowCount >= @RowCount 
	BEGIN
		DELETE @EmployersOrderedByPostings 
		WHERE
			TableId > @RowCount
	END
	
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		PostingsCount
	FROM 
		@EmployersOrderedByPostings
	ORDER BY 
		MaxPostingDate
		

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, EmployerName, PostingDate DESC
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateDesc]'
GO



-- ======================================================================
-- Author:		Andy Jones
-- Create date: 5 September 2013
-- Description:	Get Employers Postings Ordered By Posting Date Desc 
-- ======================================================================
ALTER PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@SpideredJobs bit = null,
	@HasMatches bit = null,
	@MinimumNumberOfPostings int,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
	DECLARE @EmployerRowCount int
	
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PersonPostingMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)

	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		OriginId bigint,
		EmployerName nvarchar(255),
		PostingDate datetime,
		EmployerEmailAddress nvarchar(255),
		HasPersonMatches bit, 
		EmployerTableId int
	)
	
	DECLARE @EmployersOrderedByPostings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int,
		MaxPostingDate datetime
	)

	DECLARE @Employers AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		EmployerName nvarchar(255),
		PostingsCount int
	)

	-- Populate the matches for this User
	INSERT INTO @PersonPostingMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
	GROUP BY 
		ppm.PostingId

	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.OriginId,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		'' AS EmployerEmailAddress,
		CASE
			WHEN ppm.MatchCount > 0 THEN 1
			ELSE 0
		END AS HasPersonMatches,
		null
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		LEFT OUTER JOIN @PersonPostingMatches ppm  ON p.Id = ppm.PostingId
	WHERE 
		((p.OriginId = 999 AND @SpideredJobs = 1) OR (p.OriginId <> 999 AND @SpideredJobs = 0) OR @SpideredJobs is null)
		AND (p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		AND ((@HasMatches = 1 AND ppm.MatchCount > 0) OR (@HasMatches = 0 AND(ppm.MatchCount = 0 OR ppm.MatchCount is null)) OR @HasMatches is null)
		
	-- Populate the employer details
	-- First if required get the top employers based on number of postings
	INSERT INTO @EmployersOrderedByPostings
	SELECT 
		EmployerName,
		COUNT(Id),
		MAX(PostingDate)
	FROM 
		@Postings 
	GROUP BY 
		EmployerName
	HAVING 
		COUNT(Id) >= @MinimumNumberOfPostings
	ORDER BY 
		COUNT(Id) DESC, EmployerName
	
	SELECT @EmployerRowCount = @@ROWCOUNT
	
	IF @RowCount > 0 AND @EmployerRowCount >= @RowCount 
	BEGIN
		DELETE @EmployersOrderedByPostings 
		WHERE
			TableId > @RowCount
	END
	
	INSERT INTO @Employers
	SELECT 
		EmployerName,
		PostingsCount
	FROM 
		@EmployersOrderedByPostings
	ORDER BY 
		MaxPostingDate DESC
		

	SELECT @RowCount = @@ROWCOUNT

	-- Delete Employers not in paged data
	DELETE @Employers
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)

	-- Delete Postings whose Employers not in paged data
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- Update the Postings table to allow sorting
	UPDATE @Postings 
	SET
		EmployerTableId = e.TableId
	FROM 
		@Employers e
		INNER JOIN @Postings p ON e.EmployerName = p.EmployerName

	-- Return data
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		OriginId,
		EmployerName,
		PostingDate,
		EmployerEmailAddress,
		HasPersonMatches
	FROM  
		@Postings
	ORDER BY 
		EmployerTableId, EmployerName, PostingDate DESC
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO