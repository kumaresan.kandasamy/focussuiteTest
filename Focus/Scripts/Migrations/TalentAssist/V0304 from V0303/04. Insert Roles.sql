  BEGIN TRAN
  
  INSERT INTO [Data.Application.Role]
  (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [Data.Application.Role]), 'AEOverrideCriminalExclusions', 'A E Override Criminal Exclusions')
  
  INSERT INTO [Data.Application.Role]
  (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [Data.Application.Role]), 'AEJobDevelopmentManager', 'A E Job Development Manager')
  
  COMMIT
  
  GO
  