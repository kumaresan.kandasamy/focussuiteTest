-- Add viewed column to Application table (FJO3)
ALTER TABLE dbo.[Data.Application.Application]
ADD Viewed BIT NOT NULL DEFAULT 0

GO
ALTER TABLE [Data.Application.Application] ADD [PostHireFollowUpStatus] INT NULL
GO

ALTER VIEW [dbo].[Data.Application.ApplicationView]
AS

SELECT
	[Application].Id AS Id,
	Job.Id AS JobId,
	Job.EmployerId AS EmployerId,
	Job.EmployeeId AS EmployeeId,
	Job.BusinessUnitId AS BusinessUnitId,
	[Resume].PersonId AS CandidateId,
	[Application].ApplicationStatus AS CandidateApplicationStatus,
	[Application].ApplicationScore AS CandidateApplicationScore,
	[Application].CreatedOn AS CandidateApplicationReceivedOn,
	[Application].ApprovalStatus AS CandidateApplicationApprovalStatus,
	[Resume].FirstName AS CandidateFirstName,
	[Resume].LastName AS CandidateLastName,
	[Resume].YearsExperience AS CandidateYearsExperience,
	[Resume].IsVeteran AS CandidateIsVeteran,
	[Application].StatusLastChangedOn AS ApplicationStatusLastChangedOn,
	[Application].Viewed AS Viewed,
	[Application].PostHireFollowUpStatus AS PostHireFollowUpStatus
FROM [Data.Application.Application] AS [Application] WITH (NOLOCK)
	INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN [Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id
	INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
GO
