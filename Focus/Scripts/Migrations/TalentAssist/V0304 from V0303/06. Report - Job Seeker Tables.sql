IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeeker' AND COLUMN_NAME = 'HasPendingResume')
BEGIN
	ALTER TABLE [Report.JobSeeker] ADD HasPendingResume BIT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'ReferralsApproved')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD ReferralsApproved INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'FindJobsForSeeker')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD FindJobsForSeeker INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'FailedToReportToJob')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD FailedToReportToJob INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'FailedToRespondToInvitation')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD FailedToRespondToInvitation INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'FoundJobFromOtherSource')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD FoundJobFromOtherSource INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'JobAlreadyFilled')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD JobAlreadyFilled INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'NotQualified')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD NotQualified INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'NotYetPlaced')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD NotYetPlaced INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'RefusedReferral')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD RefusedReferral INT NOT NULL DEFAULT(0)
END
GO

IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveyResponses')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.JobSeekerAction'
	AND C.name = 'SurveyResponses'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.JobSeekerAction] DROP COLUMN SurveyResponses
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveySatisfied')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD SurveySatisfied INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveyVerySatisfied')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD SurveyVerySatisfied INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveyDissatisfied')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD SurveyDissatisfied INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveyVeryDissatisfied')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD SurveyVeryDissatisfied INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveyNoUnexpectedMatches')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD SurveyNoUnexpectedMatches INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveyUnexpectedMatches')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD SurveyUnexpectedMatches INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveyReceivedInvitations')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD SurveyReceivedInvitations INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'SurveyDidNotReceiveInvitations')
BEGIN
	ALTER TABLE [Report.JobSeekerAction] ADD SurveyDidNotReceiveInvitations INT NOT NULL DEFAULT(0)
END
GO

IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'ResumesCreated')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.JobSeekerAction'
	AND C.name = 'ResumesCreated'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.JobSeekerAction] DROP COLUMN ResumesCreated
END
GO

IF EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeekerAction' AND COLUMN_NAME = 'ResumesEdited')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Report.JobSeekerAction'
	AND C.name = 'ResumesEdited'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Report.JobSeekerAction] DROP COLUMN ResumesEdited
END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerOffice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Report.JobSeekerOffice](
	[Id] [bigint] NOT NULL,
	[OfficeName] [nvarchar](100) NOT NULL,
	[OfficeId] [bigint] NULL,
	[JobSeekerId] [bigint] NOT NULL DEFAULT(0),
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = OBJECT_ID(N'[dbo].[Report.JobSeeker]') AND parent_object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerOffice]'))
ALTER TABLE [dbo].[Report.JobSeekerOffice]  WITH CHECK ADD FOREIGN KEY([JobSeekerId])
REFERENCES [dbo].[Report.JobSeeker] ([Id])
GO

