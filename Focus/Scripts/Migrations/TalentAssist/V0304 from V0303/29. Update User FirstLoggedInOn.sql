UPDATE [dbo].[Data.Application.User]
  SET FirstLoggedInOn = CreatedOn
  WHERE LoggedInOn is not null