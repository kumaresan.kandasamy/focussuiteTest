SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateDesc]'
GO
DROP PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateDesc]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateAsc]'
GO
DROP PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByPostingDateAsc]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleAsc]'
GO
DROP PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleAsc]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleDesc]'
GO
DROP PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByJobTitleDesc]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameDesc]'
GO
DROP PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameDesc]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameAsc]'
GO
DROP PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByEmployerNameAsc]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[Data_Application_GetEmployersPostingsOrderByNumberOfPositionsAsc]'
GO
DROP PROCEDURE [dbo].[Data_Application_GetEmployersPostingsOrderByNumberOfPositionsAsc]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc]'
GO


-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Posting Count Desc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		SortOrderRank int
	) 
	
	DECLARE @Employers AS TABLE
	(
		EmployerName nvarchar(255),
		PostingCount int
	)
	
	DECLARE @RankedEmployers AS TABLE
	(
		Id int IDENTITY PRIMARY KEY,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		NumberOfPostings int
	)
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		RANK() OVER (PARTITION BY p.EmployerName ORDER BY p.CreatedOn DESC, p.JobTitle)
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	WHERE 
		(p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName, 
		COUNT(Id) 
	FROM 
		@Postings
	GROUP BY 
		EmployerName
	HAVING
		COUNT(Id) >= @MinimumNumberOfPostings
			
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Postings whose Employers have been deleted
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- DELETE Postings no longer required
	DELETE @Postings 
	WHERE
		SortOrderRank > 1
	
	INSERT INTO @RankedEmployers
	SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		@Employers e
	INNER JOIN
		@Postings p ON e.EmployerName = p.EmployerName
	ORDER BY
		e.PostingCount DESC, e.EmployerName
	
	SELECT 
		*
	FROM @RankedEmployers
	WHERE
		Id > @FirstRec AND Id < @LastRec
		
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Employer Name Asc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		SortOrderRank int
	) 
	
	DECLARE @Employers AS TABLE
	(
		EmployerName nvarchar(255),
		PostingCount int
	)
	
	DECLARE @RankedEmployers AS TABLE
	(
		Id int IDENTITY PRIMARY KEY,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		NumberOfPostings int
	)
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		RANK() OVER (PARTITION BY p.EmployerName ORDER BY p.CreatedOn DESC, p.JobTitle)
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	WHERE 
		(p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName, 
		COUNT(Id) 
	FROM 
		@Postings
	GROUP BY 
		EmployerName
	HAVING
		COUNT(Id) >= @MinimumNumberOfPostings
			
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Postings whose Employers have been deleted
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- DELETE Postings no longer required
	DELETE @Postings 
	WHERE
		SortOrderRank > 1
	
	INSERT INTO @RankedEmployers
	SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		@Employers e
	INNER JOIN
		@Postings p ON e.EmployerName = p.EmployerName
	ORDER BY
		e.EmployerName, e.PostingCount
	
	SELECT 
		*
	FROM @RankedEmployers
	WHERE
		Id > @FirstRec AND Id < @LastRec
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Employer Name Desc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		SortOrderRank int
	) 
	
	DECLARE @Employers AS TABLE
	(
		EmployerName nvarchar(255),
		PostingCount int
	)
	
	DECLARE @RankedEmployers AS TABLE
	(
		Id int IDENTITY PRIMARY KEY,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		NumberOfPostings int
	)
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		RANK() OVER (PARTITION BY p.EmployerName ORDER BY p.CreatedOn DESC, p.JobTitle)
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	WHERE 
		(p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName, 
		COUNT(Id) 
	FROM 
		@Postings
	GROUP BY 
		EmployerName
	HAVING
		COUNT(Id) >= @MinimumNumberOfPostings
			
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Postings whose Employers have been deleted
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- DELETE Postings no longer required
	DELETE @Postings 
	WHERE
		SortOrderRank > 1
	
	INSERT INTO @RankedEmployers
	SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		@Employers e
	INNER JOIN
		@Postings p ON e.EmployerName = p.EmployerName
	ORDER BY
		e.EmployerName DESC, e.PostingCount
	
	SELECT 
		*
	FROM @RankedEmployers
	WHERE
		Id > @FirstRec AND Id < @LastRec
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Job Title Asc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		SortOrderRank int
	) 
	
	DECLARE @Employers AS TABLE
	(
		EmployerName nvarchar(255),
		PostingCount int
	)
	
	DECLARE @RankedEmployers AS TABLE
	(
		Id int IDENTITY PRIMARY KEY,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		NumberOfPostings int
	)
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		RANK() OVER (PARTITION BY p.EmployerName ORDER BY p.JobTitle, p.CreatedOn DESC)
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	WHERE 
		(p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName, 
		COUNT(Id) 
	FROM 
		@Postings
	GROUP BY 
		EmployerName
	HAVING
		COUNT(Id) >= @MinimumNumberOfPostings
			
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Postings whose Employers have been deleted
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- DELETE Postings no longer required
	DELETE @Postings 
	WHERE
		SortOrderRank > 1
	
	INSERT INTO @RankedEmployers
	SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		@Employers e
	INNER JOIN
		@Postings p ON e.EmployerName = p.EmployerName
	ORDER BY
		p.JobTitle, e.EmployerName, e.PostingCount
	
	SELECT 
		*
	FROM @RankedEmployers
	WHERE
		Id > @FirstRec AND Id < @LastRec
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Job Title Desc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		SortOrderRank int
	) 
	
	DECLARE @Employers AS TABLE
	(
		EmployerName nvarchar(255),
		PostingCount int
	)
	
	DECLARE @RankedEmployers AS TABLE
	(
		Id int IDENTITY PRIMARY KEY,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		NumberOfPostings int
	)
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		RANK() OVER (PARTITION BY p.EmployerName ORDER BY p.JobTitle DESC, p.CreatedOn DESC)
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	WHERE 
		(p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName, 
		COUNT(Id) 
	FROM 
		@Postings
	GROUP BY 
		EmployerName
	HAVING
		COUNT(Id) >= @MinimumNumberOfPostings
			
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Postings whose Employers have been deleted
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- DELETE Postings no longer required
	DELETE @Postings 
	WHERE
		SortOrderRank > 1
	
	INSERT INTO @RankedEmployers
	SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		@Employers e
	INNER JOIN
		@Postings p ON e.EmployerName = p.EmployerName
	ORDER BY
		p.JobTitle DESC, e.EmployerName, e.PostingCount
	
	SELECT 
		*
	FROM @RankedEmployers
	WHERE
		Id > @FirstRec AND Id < @LastRec
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc]'
GO


-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Posting Date Desc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		SortOrderRank int
	) 
	
	DECLARE @Employers AS TABLE
	(
		EmployerName nvarchar(255),
		PostingCount int
	)
	
	DECLARE @RankedEmployers AS TABLE
	(
		Id int IDENTITY PRIMARY KEY,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		NumberOfPostings int
	)
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		RANK() OVER (PARTITION BY p.EmployerName ORDER BY p.CreatedOn DESC, p.JobTitle)
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	WHERE 
		(p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName, 
		COUNT(Id) 
	FROM 
		@Postings
	GROUP BY 
		EmployerName
	HAVING
		COUNT(Id) >= @MinimumNumberOfPostings
			
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Postings whose Employers have been deleted
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- DELETE Postings no longer required
	DELETE @Postings 
	WHERE
		SortOrderRank > 1
	
	INSERT INTO @RankedEmployers
	SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		@Employers e
	INNER JOIN
		@Postings p ON e.EmployerName = p.EmployerName
	ORDER BY
		p.PostingDate DESC, e.EmployerName, e.PostingCount
	
	SELECT 
		*
	FROM @RankedEmployers
	WHERE
		Id > @FirstRec AND Id < @LastRec
		
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 27 November 2013
-- Description:	Gets spidered employers with good matches ordered by Posting Date Asc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumNumberOfPostings int,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		SortOrderRank int
	) 
	
	DECLARE @Employers AS TABLE
	(
		EmployerName nvarchar(255),
		PostingCount int
	)
	
	DECLARE @RankedEmployers AS TABLE
	(
		Id int IDENTITY PRIMARY KEY,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		EmployerName nvarchar(255),
		PostingDate datetime,
		NumberOfPostings int
	)
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.EmployerName,
		p.CreatedOn AS PostingDate,
		RANK() OVER (PARTITION BY p.EmployerName ORDER BY p.CreatedOn, p.JobTitle)
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	WHERE 
		(p.EmployerName LIKE '%' + @EmployerName + '%' OR @EmployerName is null)
		
	-- Populate the employer details
	INSERT INTO @Employers
	SELECT 
		EmployerName, 
		COUNT(Id) 
	FROM 
		@Postings
	GROUP BY 
		EmployerName
	HAVING
		COUNT(Id) >= @MinimumNumberOfPostings
			
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Postings whose Employers have been deleted
	DELETE @Postings 
	WHERE NOT 
		(EmployerName IN (SELECT EmployerName FROM @Employers))
	
	-- DELETE Postings no longer required
	DELETE @Postings 
	WHERE
		SortOrderRank > 1
	
	INSERT INTO @RankedEmployers
	SELECT 
		p.LensPostingId,
		p.JobTitle,
		e.EmployerName,
		p.PostingDate,
		e.PostingCount
	FROM 
		@Employers e
	INNER JOIN
		@Postings p ON e.EmployerName = p.EmployerName
	ORDER BY
		p.PostingDate, e.EmployerName, e.PostingCount
	
	SELECT 
		*
	FROM @RankedEmployers
	WHERE
		Id > @FirstRec AND Id < @LastRec
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateAsc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 28 November 2013
-- Description:	Gets spidered postings with good matches ordered by posting date Asc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		PostingDate datetime
	) 
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
		AND p.EmployerName = @EmployerName
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.CreatedOn AS PostingDate
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	ORDER BY
		p.CreatedOn, p.JobTitle
	 		
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Matches not in paged data
	DELETE @Postings
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)
	
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		PostingDate
	FROM
		@Postings
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateDesc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 28 November 2013
-- Description:	Gets spidered postings with good matches ordered by posting date Desc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		PostingDate datetime
	) 
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
		AND p.EmployerName = @EmployerName
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.CreatedOn AS PostingDate
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	ORDER BY
		p.CreatedOn DESC, p.JobTitle
	 		
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Matches not in paged data
	DELETE @Postings
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)
	
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		PostingDate
	FROM
		@Postings
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByJobTitleDesc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 28 November 2013
-- Description:	Gets spidered postings with good matches ordered by job title desc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByJobTitleDesc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		PostingDate datetime
	) 
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
		AND p.EmployerName = @EmployerName
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.CreatedOn AS PostingDate
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	ORDER BY
		p.JobTitle DESC, p.CreatedOn DESC
	 		
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Matches not in paged data
	DELETE @Postings
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)
	
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		PostingDate
	FROM
		@Postings
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByJobTitleAsc]'
GO



-- =============================================
-- Author:		Andy Jones
-- Create date: 28 November 2013
-- Description:	Gets spidered postings with good matches ordered by job title asc
-- =============================================
CREATE PROCEDURE [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByJobTitleAsc] 
	@UserID bigint,
	@EmployerName nvarchar(255) = null,
	@MinimumPostingDate datetime,
	@PageNumber int,
	@PageSize int,
	@RowCount int out
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @FirstRec int
	DECLARE @LastRec int
		
	SET @FirstRec = (@PageNumber - 1) * @PageSize
	SET @LastRec = (@PageNumber * @PageSize + 1)

	DECLARE @PostingPersonMatches AS TABLE
	(
		PostingId bigint,
		MatchCount int
	)
	
	DECLARE @Postings AS TABLE
	(
		TableId int IDENTITY PRIMARY KEY,
		Id bigint,
		LensPostingId nvarchar(150),
		JobTitle nvarchar(255),
		PostingDate datetime
	) 
	
	-- Populate the matches for this User
	INSERT INTO @PostingPersonMatches
	SELECT 
		ppm.PostingId, 
		COUNT(ppm.PersonId)
	FROM 
		[Data.Application.PersonPostingMatch] ppm WITH (NOLOCK)
		INNER JOIN [Data.Application.Posting] p WITH (NOLOCK) ON  ppm.PostingId = p.Id
		LEFT OUTER JOIN [Data.Application.PersonPostingMatchToIgnore] ppmti WITH (NOLOCK) ON
			ppm.PersonId = ppmti.PersonId AND ppm.PostingId = ppmti.PostingId AND ppmti.UserId = @UserId
	WHERE
		ppmti.UserId is null
		AND p.CreatedOn > @MinimumPostingDate
		AND p.OriginId = '999'
		AND p.EmployerName = @EmployerName
	GROUP BY 
		ppm.PostingId 
		
	-- Populate the postings that match the passed filters
	INSERT INTO @Postings	
	SELECT  
		p.Id,
		p.LensPostingId,
		p.JobTitle,
		p.CreatedOn AS PostingDate
	FROM 
		[Data.Application.Posting] p WITH (NOLOCK)
		INNER JOIN @PostingPersonMatches ppm ON p.Id = ppm.PostingId
	ORDER BY
		p.JobTitle, p.CreatedOn DESC
	 		
	SELECT @RowCount = @@ROWCOUNT
	
	-- Delete Matches not in paged data
	DELETE @Postings
	WHERE 
		NOT (TableId > @FirstRec AND TableId < @LastRec)
	
	SELECT 
		Id,
		LensPostingId,
		JobTitle,
		PostingDate
	FROM
		@Postings
		
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateAsc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateAsc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateDesc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByPostingDateDesc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByJobTitleDesc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByJobTitleDesc] TO [FocusAgent]
GO
PRINT N'Altering permissions on [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByJobTitleAsc]'
GO
GRANT EXECUTE ON  [dbo].[Data_Application_GetSpideredPostingsWithGoodMatchesOrderByJobTitleAsc] TO [FocusAgent]
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO