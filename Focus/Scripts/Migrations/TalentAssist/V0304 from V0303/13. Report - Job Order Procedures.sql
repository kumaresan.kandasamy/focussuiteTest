/****** Object:  StoredProcedure [dbo].[Report.JobOrderKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderKeywordFilter]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobOrderKeywordFilter]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobOrderKeywordFilter]    Script Date: 09/13/2013 12:00:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 26 September 2013
-- Description:	Filter job Orders by keyword
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobOrderKeywordFilter]
	@Keywords XML,
	@CheckTitle BIT,
	@CheckDescription BIT,
	@CheckEmployer BIT,
	@SearchAll BIT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #Keywords
	(
		KeywordId INT IDENTITY(1, 1),
		TitleKeyword NVARCHAR(500),
		DescriptionKeyword NVARCHAR(500),
		EmployerKeyword NVARCHAR(500)
	)
	
	DECLARE @KeywordsToCheck INT
	
	INSERT INTO #KeyWords ( TitleKeyword, DescriptionKeyword, EmployerKeyword )
    SELECT
		CASE @CheckTitle WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckDescription WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END,
		CASE @CheckEmployer WHEN 1 THEN '%' + t.value('.', 'varchar(500)') + '%' END
    FROM @Keywords.nodes('//keyword') AS x(t) 
    
	SET @KeywordsToCheck = CASE @SearchAll WHEN 1 THEN @@ROWCOUNT ELSE 1 END

	SELECT 
		J.Id
	FROM 
		[Report.JobOrder] J
	INNER JOIN [Report.Employer] E
		ON E.Id = J.EmployerId
	INNER JOIN #Keywords K
		ON J.JobTitle LIKE K.TitleKeyword
		OR J.[Description] LIKE K.DescriptionKeyword
		OR E.Name LIKE K.EmployerKeyword
	GROUP BY 
		J.Id
	HAVING
		COUNT(DISTINCT K.KeywordId) >= @KeywordsToCheck
END
GO

GRANT EXEC ON [dbo].[Report.JobOrderKeywordFilter] TO [FocusAgent]
GO


/****** Object:  StoredProcedure [dbo].[Report.JobOrderActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderActionTotals]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobOrderActionTotals]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobOrderActionTotals]    Script Date: 09/09/2013 17:11:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 27 September 2013
-- Description:	Get report totals for job Order actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobOrderActionTotals]
	@FromDate DATETIME,
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		JobOrderId AS Id,
		SUM(StaffReferrals) AS StaffReferrals,  
		SUM(SelfReferrals) AS SelfReferrals,  
		SUM(ReferralsRequested) AS ReferralsRequested,
		SUM(EmployerInvitationsSent) AS EmployerInvitationsSent,  
		SUM(ApplicantsInterviewed) AS ApplicantsInterviewed,  
		SUM(ApplicantsFailedToShow) AS ApplicantsFailedToShow,  
		SUM(ApplicantsDeniedInterviews) AS ApplicantsDeniedInterviews,  
		SUM(ApplicantsHired) AS ApplicantsHired,  
		SUM(JobOffersRefused) AS JobOffersRefused,  
		SUM(ApplicantsDidNotApply) AS ApplicantsDidNotApply,
		SUM(InvitedJobSeekerViewed) AS InvitedJobSeekerViewed,
		SUM(InvitedJobSeekerClicked) AS InvitedJobSeekerClicked,
		SUM(ApplicantsNotHired) AS ApplicantsNotHired, 
		SUM(ApplicantsNotYetPlaced) AS ApplicantsNotYetPlaced, 
		SUM(FailedToRespondToInvitation) AS FailedToRespondToInvitation, 
		SUM(FailedToReportToJob) AS FailedToReportToJob, 
		SUM(FoundJobFromOtherSource) AS FoundJobFromOtherSource, 
		SUM(JobAlreadyFilled) AS JobAlreadyFilled, 
		SUM(NewApplicant) AS NewApplicant, 
		SUM(NotQualified) AS NotQualified, 
		SUM(ApplicantRecommended) AS ApplicantRecommended, 
		SUM(RefusedReferral) AS RefusedReferral, 
		SUM(UnderConsideration) AS UnderConsideration
	FROM 
		[Report.JobOrderAction]
	WHERE 
		ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		JobOrderId
END
GO

GRANT EXEC ON [dbo].[Report.JobOrderActionTotals] TO [FocusAgent]
GO


/****** Object:  StoredProcedure [dbo].[Report.JobOrderActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobOrderActionTotalsForIds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobOrderActionTotalsForIds]
GO

/****** Object:  StoredProcedure [dbo].[Report.JobOrderActionTotalsForIds]    Script Date: 09/16/2013 10:13:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Tim Case
-- Create date: 27 September 2013
-- Description:	Get report totals for job Order actions
-- =============================================
CREATE PROCEDURE [dbo].[Report.JobOrderActionTotalsForIds]
	@FromDate DATETIME,
	@ToDate DATETIME,
	@JobOrderIds XML
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #JobOrderIds
	(
		Id BIGINT
	)
	
	INSERT INTO #JobOrderIds ( Id )
    SELECT t.value('.', 'bigint')
    FROM @JobOrderIds.nodes('//id') AS x(t) 

	SELECT
		JSA.JobOrderId AS Id,
		SUM(JSA.StaffReferrals) AS StaffReferrals,  
		SUM(JSA.SelfReferrals) AS SelfReferrals,  
		SUM(JSA.ReferralsRequested) AS ReferralsRequested,
		SUM(JSA.EmployerInvitationsSent) AS EmployerInvitationsSent,  
		SUM(JSA.ApplicantsInterviewed) AS ApplicantsInterviewed,  
		SUM(JSA.ApplicantsFailedToShow) AS ApplicantsFailedToShow,  
		SUM(JSA.ApplicantsDeniedInterviews) AS ApplicantsDeniedInterviews,  
		SUM(JSA.ApplicantsHired) AS ApplicantsHired,  
		SUM(JSA.JobOffersRefused) AS JobOffersRefused,  
		SUM(JSA.ApplicantsDidNotApply) AS ApplicantsDidNotApply,
		SUM(JSA.InvitedJobSeekerViewed) AS InvitedJobSeekerViewed,
		SUM(JSA.InvitedJobSeekerClicked) AS InvitedJobSeekerClicked,
		SUM(JSA.ApplicantsNotHired) AS ApplicantsNotHired, 
		SUM(JSA.ApplicantsNotYetPlaced) AS ApplicantsNotYetPlaced, 
		SUM(JSA.FailedToRespondToInvitation) AS FailedToRespondToInvitation, 
		SUM(JSA.FailedToReportToJob) AS FailedToReportToJob, 
		SUM(JSA.FoundJobFromOtherSource) AS FoundJobFromOtherSource, 
		SUM(JSA.JobAlreadyFilled) AS JobAlreadyFilled, 
		SUM(JSA.NewApplicant) AS NewApplicant, 
		SUM(JSA.NotQualified) AS NotQualified, 
		SUM(JSA.ApplicantRecommended) AS ApplicantRecommended, 
		SUM(JSA.RefusedReferral) AS RefusedReferral, 
		SUM(JSA.UnderConsideration) AS UnderConsideration
	FROM 
		[Report.JobOrderAction] JSA
	INNER JOIN #JobOrderIds JSI
		ON JSA.JobOrderId = JSI.Id
	WHERE 
		JSA.ActionDate BETWEEN @FromDate AND @ToDate
	GROUP BY
		JSA.JobOrderId
END
GO

GRANT EXEC ON [dbo].[Report.JobOrderActionTotalsForIds] TO [FocusAgent]
GO