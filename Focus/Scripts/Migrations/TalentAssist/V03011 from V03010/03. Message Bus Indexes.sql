IF EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Messaging.Message_Priority' AND object_id = OBJECT_ID('[Messaging.Message]'))
BEGIN
	DROP INDEX [Messaging.Message].[IX_Messaging.Message_Priority]
END

IF EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Messaging.Message_Status' AND object_id = OBJECT_ID('[Messaging.Message]'))
BEGIN
	DROP INDEX [Messaging.Message].[IX_Messaging.Message_Status]
END

IF EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Messaging.Message_Version' AND object_id = OBJECT_ID('[Messaging.Message]'))
BEGIN
	DROP INDEX [Messaging.Message].[IX_Messaging.Message_Version]
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Messaging.Message_Status_Version_Priority_Enqueued' AND object_id = OBJECT_ID('[Messaging.Message]'))
BEGIN
	CREATE NONCLUSTERED INDEX [Message_Status_Version_Priority_Enqueued]
		ON [Messaging.Message] ([Status], [Version] DESC, [Priority] DESC, [EnqueuedOn])
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Messaging.MessageLog_MessageId' AND object_id = OBJECT_ID('[Messaging.MessageLog]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Messaging.MessageLog_MessageId]
		ON [Messaging.MessageLog] (MessageId)
END

--EXEC sp_helpindex '[Messaging.Message]'
--EXEC sp_helpindex '[Messaging.MessageLog]'

