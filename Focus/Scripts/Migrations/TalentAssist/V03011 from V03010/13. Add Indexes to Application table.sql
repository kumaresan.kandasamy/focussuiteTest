IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.Application_ResumeId' AND object_id = OBJECT_ID('[Data.Application.Application]'))
BEGIN
	CREATE NONCLUSTERED INDEX 
		[IX_Data.Application.Application_ResumeId]
	ON 
		[Data.Application.Application] (ResumeId)
END
