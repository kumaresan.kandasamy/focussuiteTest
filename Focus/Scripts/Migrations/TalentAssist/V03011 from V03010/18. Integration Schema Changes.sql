/****** Object:  Table [dbo].[Config.ExternalLookUpItem]    Script Date: 06/06/2014 11:19:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Config.ExternalLookUpItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Config.ExternalLookUpItem](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[InternalId] [nvarchar](50) NULL,
	[ExternalId] [nvarchar](50) NULL,
	[ExternalLookUpType] [int] NOT NULL,
 CONSTRAINT [PK_Integration.ExternalLookUpItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Data.Core.IntegrationLog]    Script Date: 06/24/2014 08:59:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Data.Core.IntegrationLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IntegrationPoint] [int] NOT NULL,
	[RequestededBy] [bigint] NOT NULL,
	[RequestedOn] [datetime] NOT NULL,
	[RequestStart] [datetime] NOT NULL,
	[RequestEnd] [datetime] NOT NULL,
	[Outcome] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK__Data.Cor__3214EC074F5D9FD3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Data.Core.IntegrationException]    Script Date: 06/24/2014 08:59:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationException]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Data.Core.IntegrationException](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](1000) NOT NULL,
	[StackTrace] [nvarchar](max) NOT NULL,
	[IntegrationLogId] [bigint] NOT NULL,
 CONSTRAINT [PK__Data.Cor__3214EC07560A9D62] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Data.Core.IntegrationRequestResponse]    Script Date: 06/24/2014 08:59:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationRequestResponse]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Data.Core.IntegrationRequestResponse](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Url] [nvarchar](1000) NOT NULL,
	[RequestSentOn] [datetime] NOT NULL,
	[ResponseReceivedOn] [datetime] NOT NULL,
	[IntegrationLogId] [bigint] NOT NULL,
 CONSTRAINT [PK__Data.Cor__3214EC075DABBF2A] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Data.Core.IntegrationRequestResponseMessages]    Script Date: 06/24/2014 08:59:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationRequestResponseMessages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Data.Core.IntegrationRequestResponseMessages](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RequestMessage] [nvarchar](max) NOT NULL,
	[ResponseMessage] [nvarchar](max) NULL,
	[IntegrationRequestResponseId] [bigint] NOT NULL,
 CONSTRAINT [PK__Data.Cor__3214EC076458BCB9] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Default [DF__Data.Core__Integ__59DB2E46]    Script Date: 06/24/2014 08:59:28 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Data.Core__Integ__59DB2E46]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationException]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Data.Core__Integ__59DB2E46]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Data.Core.IntegrationException] ADD  CONSTRAINT [DF__Data.Core__Integ__59DB2E46]  DEFAULT ((0)) FOR [IntegrationLogId]
END


End
GO
/****** Object:  Default [DF__Data.Core__Integ__5145E845]    Script Date: 06/24/2014 08:59:28 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Data.Core__Integ__5145E845]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationLog]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Data.Core__Integ__5145E845]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Data.Core.IntegrationLog] ADD  CONSTRAINT [DF__Data.Core__Integ__5145E845]  DEFAULT ((0)) FOR [IntegrationPoint]
END


End
GO
/****** Object:  Default [DF__Data.Core__Reque__523A0C7E]    Script Date: 06/24/2014 08:59:28 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Data.Core__Reque__523A0C7E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationLog]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Data.Core__Reque__523A0C7E]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Data.Core.IntegrationLog] ADD  CONSTRAINT [DF__Data.Core__Reque__523A0C7E]  DEFAULT ((0)) FOR [RequestededBy]
END


End
GO
/****** Object:  Default [DF__Data.Core__Outco__532E30B7]    Script Date: 06/24/2014 08:59:28 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Data.Core__Outco__532E30B7]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationLog]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Data.Core__Outco__532E30B7]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Data.Core.IntegrationLog] ADD  CONSTRAINT [DF__Data.Core__Outco__532E30B7]  DEFAULT ((0)) FOR [Outcome]
END


End
GO
/****** Object:  Default [DF__Data.Core__Integ__60882BD5]    Script Date: 06/24/2014 08:59:28 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Data.Core__Integ__60882BD5]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationRequestResponse]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Data.Core__Integ__60882BD5]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Data.Core.IntegrationRequestResponse] ADD  CONSTRAINT [DF__Data.Core__Integ__60882BD5]  DEFAULT ((0)) FOR [IntegrationLogId]
END


End
GO
/****** Object:  Default [DF__Data.Core__Integ__68294D9D]    Script Date: 06/24/2014 08:59:28 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Data.Core__Integ__68294D9D]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationRequestResponseMessages]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Data.Core__Integ__68294D9D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Data.Core.IntegrationRequestResponseMessages] ADD  CONSTRAINT [DF__Data.Core__Integ__68294D9D]  DEFAULT ((0)) FOR [IntegrationRequestResponseId]
END


End
GO
/****** Object:  ForeignKey [FK__Data.Core__Integ__5ACF527F]    Script Date: 06/24/2014 08:59:28 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__Data.Core__Integ__5ACF527F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationException]'))
ALTER TABLE [dbo].[Data.Core.IntegrationException]  WITH CHECK ADD  CONSTRAINT [FK__Data.Core__Integ__5ACF527F] FOREIGN KEY([IntegrationLogId])
REFERENCES [dbo].[Data.Core.IntegrationLog] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__Data.Core__Integ__5ACF527F]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationException]'))
ALTER TABLE [dbo].[Data.Core.IntegrationException] CHECK CONSTRAINT [FK__Data.Core__Integ__5ACF527F]
GO
/****** Object:  ForeignKey [FK__Data.Core__Integ__617C500E]    Script Date: 06/24/2014 08:59:28 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__Data.Core__Integ__617C500E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationRequestResponse]'))
ALTER TABLE [dbo].[Data.Core.IntegrationRequestResponse]  WITH CHECK ADD  CONSTRAINT [FK__Data.Core__Integ__617C500E] FOREIGN KEY([IntegrationLogId])
REFERENCES [dbo].[Data.Core.IntegrationLog] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__Data.Core__Integ__617C500E]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationRequestResponse]'))
ALTER TABLE [dbo].[Data.Core.IntegrationRequestResponse] CHECK CONSTRAINT [FK__Data.Core__Integ__617C500E]
GO
/****** Object:  ForeignKey [FK__Data.Core__Integ__691D71D6]    Script Date: 06/24/2014 08:59:28 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__Data.Core__Integ__691D71D6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationRequestResponseMessages]'))
ALTER TABLE [dbo].[Data.Core.IntegrationRequestResponseMessages]  WITH CHECK ADD  CONSTRAINT [FK__Data.Core__Integ__691D71D6] FOREIGN KEY([IntegrationRequestResponseId])
REFERENCES [dbo].[Data.Core.IntegrationRequestResponse] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__Data.Core__Integ__691D71D6]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationRequestResponseMessages]'))
ALTER TABLE [dbo].[Data.Core.IntegrationRequestResponseMessages] CHECK CONSTRAINT [FK__Data.Core__Integ__691D71D6]
GO
