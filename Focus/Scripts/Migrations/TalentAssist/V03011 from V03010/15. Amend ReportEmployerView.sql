
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerView]'))
DROP VIEW [dbo].[Report.EmployerView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Report.EmployerView]
AS
	SELECT
		E.Id,
		E.FocusBusinessUnitId,
		E.FocusEmployerId,
		E.[Name],
		E.CountyId,
		E.County,
		E.StateId,
		E.[State],
		E.PostalCode,
		E.Office,
		E.LatitudeRadians,
		E.LongitudeRadians,
		E.FederalEmployerIdentificationNumber,
		J.JobTitle,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ForeignLabourType,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		J.MinSalary,
		J.MaxSalary,
		J.SalaryFrequency,
		J.JobStatus,
		J.MinimumEducationLevel,
		J.[Description] AS JobDescription
	FROM 
		[Report.Employer] E
	LEFT OUTER JOIN [Report.JobOrder] J
		ON J.EmployerId = E.Id 
GO


