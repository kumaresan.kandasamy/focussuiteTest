

/*
Run this script on:

        10.0.1.35.FocusStage_test    -  This database will be modified

to synchronize it with:

        10.0.1.35.FocusDev

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.0.0 from Red Gate Software Ltd at 10/10/2012 14:22:54

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[ActionEvent]'
GO
ALTER TABLE [dbo].[ActionEvent] DROP CONSTRAINT[FK__ActionEve__Actio__4C364F0E]
ALTER TABLE [dbo].[ActionEvent] DROP CONSTRAINT[FK__ActionEve__Entit__4D2A7347]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ActionType]'
GO
ALTER TABLE [dbo].[ActionType] ALTER COLUMN [ReportOn] [bit] NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[JobView]'
GO
EXEC sp_refreshview N'[dbo].[JobView]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ActionEvent]'
GO
ALTER TABLE [dbo].[ActionEvent] ADD CONSTRAINT [FK__ActionEve__Actio__405A880E] FOREIGN KEY ([ActionTypeId]) REFERENCES [dbo].[ActionType] ([Id])
ALTER TABLE [dbo].[ActionEvent] ADD CONSTRAINT [FK__ActionEve__Entit__5441852A] FOREIGN KEY ([EntityTypeId]) REFERENCES [dbo].[EntityType] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
--PRINT N'Altering extended properties'
--GO
--EXEC sp_updateextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
--Begin DesignProperties = 
--   Begin PaneConfigurations = 
--      Begin PaneConfiguration = 0
--         NumPanes = 4
--         Configuration = "(H (1[41] 4[25] 2[22] 3) )"
--      End
--      Begin PaneConfiguration = 1
--         NumPanes = 3
--         Configuration = "(H (1 [50] 4 [25] 3))"
--      End
--      Begin PaneConfiguration = 2
--         NumPanes = 3
--         Configuration = "(H (1 [50] 2 [25] 3))"
--      End
--      Begin PaneConfiguration = 3
--         NumPanes = 3
--         Configuration = "(H (4 [30] 2 [40] 3))"
--      End
--      Begin PaneConfiguration = 4
--         NumPanes = 2
--         Configuration = "(H (1 [56] 3))"
--      End
--      Begin PaneConfiguration = 5
--         NumPanes = 2
--         Configuration = "(H (2 [66] 3))"
--      End
--      Begin PaneConfiguration = 6
--         NumPanes = 2
--         Configuration = "(H (4 [50] 3))"
--      End
--      Begin PaneConfiguration = 7
--         NumPanes = 1
--         Configuration = "(V (3))"
--      End
--      Begin PaneConfiguration = 8
--         NumPanes = 3
--         Configuration = "(H (1[56] 4[18] 2) )"
--      End
--      Begin PaneConfiguration = 9
--         NumPanes = 2
--         Configuration = "(H (1 [75] 4))"
--      End
--      Begin PaneConfiguration = 10
--         NumPanes = 2
--         Configuration = "(H (1[66] 2) )"
--      End
--      Begin PaneConfiguration = 11
--         NumPanes = 2
--         Configuration = "(H (4 [60] 2))"
--      End
--      Begin PaneConfiguration = 12
--         NumPanes = 1
--         Configuration = "(H (1) )"
--      End
--      Begin PaneConfiguration = 13
--         NumPanes = 1
--         Configuration = "(V (4))"
--      End
--      Begin PaneConfiguration = 14
--         NumPanes = 1
--         Configuration = "(V (2))"
--      End
--      ActivePaneConfig = 0
--   End
--   Begin DiagramPane = 
--      Begin Origin = 
--         Top = 0
--         Left = 0
--      End
--      Begin Tables = 
--         Begin Table = "j"
--            Begin Extent = 
--               Top = 7
--               Left = 48
--               Bottom = 148
--               Right = 344
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "bu"
--            Begin Extent = 
--               Top = 154
--               Left = 48
--               Bottom = 295
--               Right = 232
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "e"
--            Begin Extent = 
--               Top = 131
--               Left = 444
--               Bottom = 272
--               Right = 791
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "cag"
--            Begin Extent = 
--               Top = 7
--               Left = 787
--               Bottom = 112
--               Right = 977
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--         Begin Table = "crg"
--            Begin Extent = 
--               Top = 7
--               Left = 392
--               Bottom = 112
--               Right = 576
--            End
--            DisplayFlags = 280
--            TopColumn = 0
--         End
--      End
--   End
--   Begin SQLPane = 
--   End
--   Begin DataPane = 
--      Begin ParameterDefaults = ""
--      End
--   End
--   Begin CriteriaPane = 
--      Begin ColumnWidths = 11
--         Column = 1440
--         Alias = 1536
--         Table = 1170
--         Output = 720
--         Append = 1400
--         NewValue = 1170
--         SortType = 1350
--         SortOrder = 1410
--         GroupBy = 1350
--         Filter = 1350
--         Or = 1350
--         Or = 1350
--         Or = 1350
--      End
--   End
--End
--', 'SCHEMA', N'dbo', 'VIEW', N'JobView', NULL, NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--EXEC sp_updateextendedproperty N'MS_DiagramPane2', N'
--', 'SCHEMA', N'dbo', 'VIEW', N'JobView', NULL, NULL
--GO
--IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
--GO
--IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
--GO
--IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
