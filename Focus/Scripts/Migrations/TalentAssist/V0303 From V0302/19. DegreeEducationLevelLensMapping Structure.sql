SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[Library.DegreeMapping]'
GO
CREATE TABLE [dbo].[Library.DegreeMapping]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[RcipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LensId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Library.DegreeMapping] on [dbo].[Library.DegreeMapping]'
GO
ALTER TABLE [dbo].[Library.DegreeMapping] ADD CONSTRAINT [PK_Library.DegreeMapping] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Library.DegreeMapping_RcipCode] on [dbo].[Library.DegreeMapping]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Library.DegreeMapping_RcipCode] ON [dbo].[Library.DegreeMapping] ([RcipCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Library.DegreeEducationLevelLensMapping]'
GO

CREATE VIEW [dbo].[Library.DegreeEducationLevelLensMapping]
AS

SELECT 
	dm.Id,
	dm.LensId,
	d.RcipCode,
	del.Id AS DegreeEducationLevelId,
	pad.ProgramAreaId AS ProgramAreaId
FROM [Library.DegreeMapping] dm WITH(NOLOCK)
	INNER JOIN [Library.Degree] d WITH(NOLOCK) ON dm.RcipCode = d.RcipCode
	INNER JOIN [Library.DegreeEducationLevel] del WITH(NOLOCK) ON del.DegreeId = d.Id
	LEFT OUTER JOIN [Library.ProgramAreaDegree] AS pad ON del.DegreeId = pad.DegreeId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO