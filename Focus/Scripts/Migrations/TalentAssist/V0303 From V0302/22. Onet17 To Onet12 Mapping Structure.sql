SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[Library.Onet]'
GO
ALTER TABLE [dbo].[Library.Onet] DROP
COLUMN [Onet17Code]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Library.JobTaskView]'
GO







ALTER VIEW [dbo].[Library.JobTaskView]
AS

SELECT
	jt.[Id] AS [Id],
	jtli.[PrimaryValue] AS Prompt,
	jtli.[SecondaryValue] AS Response,
	jt.[JobTaskType] AS JobTaskType,
	jt.[OnetId],
	li.Culture AS Culture,
	jt.Scope AS Scope,
	jt.[Certificate] AS [Certificate],
	lo.OnetCode
FROM
	[Library.JobTask] jt WITH (NOLOCK) 
	INNER JOIN [Library.JobTaskLocalisationItem] jtli WITH (NOLOCK) ON jt.[Key] = jtli.[Key]
	INNER JOIN [Library.Localisation] li WITH (NOLOCK) ON jtli.[LocalisationId] = li.[Id]
	INNER JOIN [Library.Onet] lo WITH (NOLOCK) ON jt.OnetId = lo.Id




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Library.Onet17Onet12Mapping]'
GO
CREATE TABLE [dbo].[Library.Onet17Onet12Mapping]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[Onet17Code] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Library.O__Onet1__66E023A9] DEFAULT (''),
[OnetId] [bigint] NOT NULL CONSTRAINT [DF__Library.O__OnetI__67D447E2] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__Library.__3214EC0764F7DB37] on [dbo].[Library.Onet17Onet12Mapping]'
GO
ALTER TABLE [dbo].[Library.Onet17Onet12Mapping] ADD CONSTRAINT [PK__Library.__3214EC0764F7DB37] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Library.Onet17Onet12Mapping_Onet17Code] on [dbo].[Library.Onet17Onet12Mapping]'
GO
CREATE NONCLUSTERED INDEX [IX_Library.Onet17Onet12Mapping_Onet17Code] ON [dbo].[Library.Onet17Onet12Mapping] ([Onet17Code])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Library.Onet17Onet12MappingView]'
GO

CREATE VIEW [dbo].[Library.Onet17Onet12MappingView]
AS

SELECT
	loom.Id,
	loom.Onet17Code,
	lo.OnetCode AS Onet12Code,
	lo.Id AS OnetId
FROM
	[Library.Onet17Onet12Mapping] AS loom WITH (NOLOCK)
	INNER JOIN [Library.Onet] AS lo WITH (NOLOCK) ON loom.OnetId = lo.Id
	





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Library.Onet17Onet12Mapping]'
GO
ALTER TABLE [dbo].[Library.Onet17Onet12Mapping] ADD CONSTRAINT [FK__Library.O__OnetI__68C86C1B] FOREIGN KEY ([OnetId]) REFERENCES [dbo].[Library.Onet] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO