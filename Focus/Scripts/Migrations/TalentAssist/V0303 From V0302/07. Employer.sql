-- This file contains a log of the SQL statements that LightSpeed ran
-- against your database.  It is specific to the current state of the
-- LightSpeed model and the previous state of your database.  Review the
-- script carefully if you plan to apply it to another database.  Please
-- note that due to scoping issues you may not be able to run all logged
-- SQL as a single batch.

-- Delete column OfficeId and associated foreign key from table Data.application.employer
DECLARE @fknamee499bf20cd9d47b19586b7a7ef091abf NVARCHAR(600);

SELECT TOP(1) @fknamee499bf20cd9d47b19586b7a7ef091abf = fk.name
FROM sys.foreign_key_columns fkc
     left outer join sys.tables t on fkc.parent_object_id = t.object_id
     left outer join sys.foreign_keys fk on fk.object_id = fkc.constraint_object_id
WHERE t.name = 'Data.application.employer' and col_name(t.object_id, fkc.parent_column_id) = 'OfficeId';

IF @fknamee499bf20cd9d47b19586b7a7ef091abf IS NOT NULL
BEGIN
  DECLARE @dropfk7a165e2aae3d4edb86ac4881f695d1bb NVARCHAR(640);
  SET @dropfk7a165e2aae3d4edb86ac4881f695d1bb = 'ALTER TABLE [dbo].[Data.application.employer] DROP CONSTRAINT [' + @fknamee499bf20cd9d47b19586b7a7ef091abf + ']';
  EXEC sp_executesql @dropfk7a165e2aae3d4edb86ac4881f695d1bb;
END

DECLARE @dfname6e210f0f6b1d4232a928351a18b0f1c0 NVARCHAR(600);

SELECT TOP(1) @dfname6e210f0f6b1d4232a928351a18b0f1c0 = df.name
FROM sys.default_constraints df
     left outer join sys.tables t on df.parent_object_id = t.object_id
WHERE t.name = 'Data.application.employer' and col_name(t.object_id, df.parent_column_id) = 'OfficeId';

IF @dfname6e210f0f6b1d4232a928351a18b0f1c0 IS NOT NULL
BEGIN
  DECLARE @dropdff98225e637f040c3b97e3c3f140c8eb1 NVARCHAR(640);
  SET @dropdff98225e637f040c3b97e3c3f140c8eb1 = 'ALTER TABLE [dbo].[Data.application.employer] DROP CONSTRAINT [' + @dfname6e210f0f6b1d4232a928351a18b0f1c0 + ']';
  EXEC sp_executesql @dropdff98225e637f040c3b97e3c3f140c8eb1;
END

ALTER TABLE [Data.application.employer] DROP COLUMN [OfficeId];
/* ERROR:
Office: Delete column OfficeId and associated foreign key from table Data.application.employer
  - Incorrect syntax near '.'.
The object 'FK__Data.Appl__Offic__67B44C51' is dependent on column 'OfficeId'.
ALTER TABLE DROP COLUMN OfficeId failed because one or more objects access this column.
*/;
-- Delete column OfficeUnassigned
DECLARE @fknamedd99657d93ef49c18c8e52953b1a7ea0 NVARCHAR(600);

SELECT TOP(1) @fknamedd99657d93ef49c18c8e52953b1a7ea0 = fk.name
FROM sys.foreign_key_columns fkc
     left outer join sys.tables t on fkc.parent_object_id = t.object_id
     left outer join sys.foreign_keys fk on fk.object_id = fkc.constraint_object_id
WHERE t.name = 'Data.application.employer' and col_name(t.object_id, fkc.parent_column_id) = 'OfficeUnassigned';

IF @fknamedd99657d93ef49c18c8e52953b1a7ea0 IS NOT NULL
BEGIN
  DECLARE @dropfkca9bc690730746b39ffc7b58682477c9 NVARCHAR(640);
  SET @dropfkca9bc690730746b39ffc7b58682477c9 = 'ALTER TABLE [dbo].[Data.application.employer] DROP CONSTRAINT [' + @fknamedd99657d93ef49c18c8e52953b1a7ea0 + ']';
  EXEC sp_executesql @dropfkca9bc690730746b39ffc7b58682477c9;
END

DECLARE @dfname19bd5067816248618530dc7a9342843d NVARCHAR(600);

SELECT TOP(1) @dfname19bd5067816248618530dc7a9342843d = df.name
FROM sys.default_constraints df
     left outer join sys.tables t on df.parent_object_id = t.object_id
WHERE t.name = 'Data.application.employer' and col_name(t.object_id, df.parent_column_id) = 'OfficeUnassigned';

IF @dfname19bd5067816248618530dc7a9342843d IS NOT NULL
BEGIN
  DECLARE @dropdf6d77ae195d734a8a8e236805a4f1fcf2 NVARCHAR(640);
  SET @dropdf6d77ae195d734a8a8e236805a4f1fcf2 = 'ALTER TABLE [dbo].[Data.application.employer] DROP CONSTRAINT [' + @dfname19bd5067816248618530dc7a9342843d + ']';
  EXEC sp_executesql @dropdf6d77ae195d734a8a8e236805a4f1fcf2;
END

ALTER TABLE [Data.application.employer] DROP COLUMN [OfficeUnassigned];

-- One or more of the above statements failed.  Review the log for error info.
-- Depending on your database, changes may have been rolled back or may have
-- been partially applied.
