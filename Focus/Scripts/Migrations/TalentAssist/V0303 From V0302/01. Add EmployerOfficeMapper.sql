-- Add table Data.Application.EmployerOfficeMapper to the database
CREATE TABLE [Data.Application.EmployerOfficeMapper] (Id BIGINT NOT NULL PRIMARY KEY ,
[OfficeUnassigned] BIT NULL,
[EmployerId] BIGINT NOT NULL DEFAULT 0,
[OfficeId] BIGINT NOT NULL DEFAULT 0,
FOREIGN KEY (EmployerId) REFERENCES [Data.Application.Employer](Id),
FOREIGN KEY (OfficeId) REFERENCES [Data.Application.Office](Id));