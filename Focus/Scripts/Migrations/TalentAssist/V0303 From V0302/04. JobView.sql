
/****** Object:  View [dbo].[Data.Application.JobView]    Script Date: 09/24/2013 16:18:34 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobView]'))
DROP VIEW [dbo].[Data.Application.JobView]
GO

/****** Object:  View [dbo].[Data.Application.JobView]    Script Date: 09/24/2013 16:19:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[Data.Application.JobView] as
SELECT j.Id, 
	j.EmployerId, 
	e.Name AS EmployerName, 
	j.JobTitle, 
	bu.Name AS BusinessUnitName, 
	j.BusinessUnitId, 
	j.JobStatus, 
	j.PostedOn, 
	j.ClosingOn, 
	j.HeldOn,   
    j.ClosedOn, 
    j.UpdatedOn, 
    ISNULL(cag.ApplicationCount, 0) AS ApplicationCount, 
    j.EmployeeId, j.ApprovalStatus, 
    ISNULL(crg.ReferralCount, 0) AS ReferralCount,   
    j.FederalContractor, 
    j.ForeignLabourCertificationH2A, 
    j.ForeignLabourCertificationH2B, 
    j.ForeignLabourCertificationOther, 
    j.CourtOrderedAffirmativeAction,   
    u.UserName, 
    p.FirstName AS HiringManagerFirstName, 
    p.LastName AS HiringManagerLastName, 
    j.CreatedOn, 
    j.YellowProfanityWords, 
    j.IsConfidential, 
    j.CreatedBy,
    j.UpdatedBy, 
    j.JobType,
    j.AssignedToId
FROM  dbo.[Data.Application.Job] AS j WITH (NOLOCK) 
	LEFT OUTER JOIN dbo.[Data.Application.Employee] AS employee WITH (NOLOCK) ON j.EmployeeId = employee.Id 
	LEFT OUTER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON employee.PersonId = p.Id 
	LEFT OUTER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON employee.PersonId = u.PersonId 
	LEFT OUTER JOIN dbo.[Data.Application.Employer] AS e WITH (NOLOCK) ON j.EmployerId = e.Id 
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnit] AS bu WITH (NOLOCK) ON bu.Id = j.BusinessUnitId 
	LEFT OUTER JOIN (SELECT p.JobId, COUNT(1) AS ApplicationCount  
                    FROM dbo.[Data.Application.Application] AS ca WITH (NOLOCK) 
                    INNER JOIN dbo.[Data.Application.Posting] AS p WITH (NOLOCK) ON ca.PostingId = p.Id  
                    WHERE (ca.ApprovalStatus = 2)  
                    GROUP BY p.JobId) AS cag ON cag.JobId = j.Id 
	LEFT OUTER JOIN (SELECT p.JobId, COUNT(1) AS ReferralCount  
                    FROM dbo.[Data.Application.Application] AS ca WITH (NOLOCK) 
                    INNER JOIN dbo.[Data.Application.Posting] AS p WITH (NOLOCK) ON ca.PostingId = p.Id  
                    WHERE (ca.ApprovalStatus = 1)  
                    GROUP BY p.JobId) AS crg ON crg.JobId = j.Id  





GO


