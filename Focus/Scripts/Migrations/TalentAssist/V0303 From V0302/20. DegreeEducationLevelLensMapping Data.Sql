SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Add 7 rows to [dbo].[Library.DegreeMapping]
SET IDENTITY_INSERT [dbo].[Library.DegreeMapping] ON
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (1, N'BE.1013', 300)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (2, N'BE.1008', 301)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (3, N'BE.1010', 302)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (4, N'BE.1009', 303)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (5, N'BE.1011', 304)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (6, N'BE.1007', 305)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (7, N'BE.1012', 306)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (8, N'LS.0000', 307)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (9, N'LS.0001', 308)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (10, N'LS.0002', 309)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (11, N'LS.0003', 310)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (12, N'LS.0004', 311)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (13, N'LS.0005', 312)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (14, N'LS.0006', 313)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (15, N'LS.0007', 314)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (16, N'LS.0008', 315)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (17, N'LS.0009', 316)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (18, N'LS.0010', 317)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (19, N'LS.0011', 318)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (20, N'LS.0012', 319)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (21, N'LS.0013', 320)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (22, N'LS.0014', 321)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (23, N'LS.0015', 322)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (24, N'LS.0016', 323)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (25, N'LS.0017', 324)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (26, N'LS.0018', 325)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (27, N'LS.0020', 326)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (28, N'LS.0021', 327)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (29, N'LS.0022', 328)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (30, N'LS.0023', 329)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (31, N'LS.0024', 330)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (32, N'LS.0025', 331)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (33, N'LS.0026', 332)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (34, N'LS.0027', 333)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (35, N'LS.0028', 334)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (36, N'LS.0029', 335)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (37, N'LS.0030', 336)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (38, N'LS.0031', 337)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (39, N'LS.0032', 338)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (40, N'LS.0033', 339)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (41, N'LS.0034', 340)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (42, N'LS.0035', 341)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (43, N'LS.0036', 342)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (44, N'LS.0037', 343)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (45, N'LS.0038', 344)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (46, N'LS.0039', 345)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (47, N'LS.0040', 346)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (48, N'LS.0041', 347)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (49, N'LS.0042', 348)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (50, N'LS.0043', 349)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (51, N'LS.0044', 350)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (52, N'LS.0045', 351)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (53, N'LS.0046', 352)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (54, N'LS.0047', 353)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (55, N'LS.0048', 354)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (56, N'LS.0049', 355)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (57, N'LS.0050', 356)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (58, N'LS.0051', 357)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (59, N'LS.0052', 358)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (60, N'LS.0053', 359)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (61, N'LS.0054', 360)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (62, N'LS.0055', 361)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (63, N'LS.0056', 362)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (64, N'LS.0057', 363)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (65, N'LS.0058', 364)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (66, N'LS.0059', 365)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (67, N'LS.0060', 366)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (68, N'LS.0061', 367)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (69, N'LS.0062', 368)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (70, N'LS.0063', 369)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (71, N'LS.0064', 370)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (72, N'LS.0065', 371)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (73, N'BE.1001', 372)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (74, N'BE.1004', 373)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (75, N'BE.1005', 374)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (76, N'BE.1006', 375)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (77, N'BE.1002', 376)
INSERT INTO [dbo].[Library.DegreeMapping] ([Id], [RcipCode], [LensId]) VALUES (78, N'BE.1003', 377)

SET IDENTITY_INSERT [dbo].[Library.DegreeMapping] OFF
COMMIT TRANSACTION
GO