SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)

-- Drop constraints from [dbo].[Library.OnetROnet]
ALTER TABLE [dbo].[Library.OnetROnet] DROP CONSTRAINT [FK__Library.O__OnetI__61F08603]

-- Delete rows from [dbo].[Library.OnetROnet]
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2887
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2889
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2890
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2898
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2901
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2902
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2904
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2905
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2907
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2914
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2915
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2920
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2924
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2925
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2926
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2927
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2931
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2932
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2933
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2934
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2938
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2940
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2945
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2948
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2949
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2953
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2955
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2958
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2959
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2965
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2968
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2975
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2978
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2980
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2986
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=2996
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3002
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3005
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3007
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3008
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3009
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3010
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3016
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3019
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3025
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3026
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3029
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3030
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3031
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3032
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3033
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3038
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3043
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3045
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3046
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3047
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3048
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3049
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3051
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3053
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3058
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3059
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3060
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3061
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3062
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3063
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3065
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3066
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3067
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3073
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3079
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3080
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3082
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3084
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3098
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3099
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3105
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3106
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3108
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3111
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3115
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3117
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3121
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3132
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3159
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3170
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3181
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3183
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3184
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3185
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3190
DELETE FROM [dbo].[Library.OnetROnet] WHERE [Id]=3193
-- Operation applied to 92 rows out of 92

-- Update rows in [dbo].[Library.OnetROnet]
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1933
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1940
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1948
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1968
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1973
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1980
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1984
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1990
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1996
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=1997
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2004
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2007
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2013
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2017
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2022
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2023
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2038
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2039
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2041
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2047
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2059
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2067
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2106
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2115
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2127
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2134
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2145
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2149
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2150
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2156
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2159
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2162
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2167
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2183
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2186
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2194
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2203
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2207
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2211
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2213
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2218
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2228
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2242
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2264
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2276
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2278
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2290
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2298
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2304
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2305
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2327
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2334
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2341
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2343
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2344
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2348
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2351
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2354
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2367
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2383
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2390
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2400
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2403
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2405
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2408
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2414
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2418
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2421
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2447
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2460
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2462
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2465
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2476
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2486
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2489
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2515
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2516
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2518
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2521
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2525
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2527
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2547
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2567
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2590
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2598
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2604
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2606
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2608
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2610
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2620
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2627
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2635
UPDATE [dbo].[Library.OnetROnet] SET [OnetROnetVariance]=1 WHERE [Id]=2636
UPDATE [dbo].[Library.OnetROnet] SET [ROnetOnetVariance]=1 WHERE [Id]=2652
UPDATE [dbo].[Library.OnetROnet] SET [ROnetOnetVariance]=1 WHERE [Id]=2999
UPDATE [dbo].[Library.OnetROnet] SET [ROnetOnetVariance]=1 WHERE [Id]=3072
-- Operation applied to 96 rows out of 96

-- Add rows to [dbo].[Library.OnetROnet]
SET IDENTITY_INSERT [dbo].[Library.OnetROnet] ON
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3199, 2, 113037, 82, 1)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3200, 2, 113091, 108, 1)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3201, 2, 113130, 131, 1)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3202, 2, 113565, 143, 1)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3203, 1, 113721, 247, 1)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3204, 2, 114246, 329, 1)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3205, 2, 114294, 352, 1)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3206, 2, 114369, 383, 1)
INSERT INTO [dbo].[Library.OnetROnet] ([Id], [OnetROnetVariance], [OnetId], [ROnetId], [ROnetOnetVariance]) VALUES (3207, 2, 114393, 395, 1)
SET IDENTITY_INSERT [dbo].[Library.OnetROnet] OFF
-- Operation applied to 9 rows out of 9

-- Add constraints to [dbo].[Library.OnetROnet]
ALTER TABLE [dbo].[Library.OnetROnet] ADD CONSTRAINT [FK__Library.O__OnetI__61F08603] FOREIGN KEY ([OnetId]) REFERENCES [dbo].[Library.Onet] ([Id])
COMMIT TRANSACTION
GO