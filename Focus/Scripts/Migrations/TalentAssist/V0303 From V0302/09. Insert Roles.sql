  BEGIN TRAN
  
  --SELECT *
  --FROM [Data.Application.Role]
  
  INSERT INTO [Data.Application.Role]
  (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [Data.Application.Role]), 'AJSOfficeAdmin', 'A JS Office Admin')
  
  INSERT INTO [Data.Application.Role]
  (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [Data.Application.Role]), 'AJOOfficeAdmin', 'A JO Office Admin')
  
  INSERT INTO [Data.Application.Role]
  (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [Data.Application.Role]), 'AEOfficeAdmin', 'A E Office Admin')
  
  --SELECT *
  --FROM [Data.Application.Role]
  
  --ROLLBACK
  COMMIT
  
  GO
  