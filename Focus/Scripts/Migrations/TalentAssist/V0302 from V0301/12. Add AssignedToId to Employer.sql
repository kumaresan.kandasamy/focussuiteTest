-- Add column AssignedToId and associated foreign key to table Data.Application.Employer
ALTER TABLE [Data.Application.Employer] ADD [AssignedToId] BIGINT NULL REFERENCES [Data.Application.Person](Id);;
