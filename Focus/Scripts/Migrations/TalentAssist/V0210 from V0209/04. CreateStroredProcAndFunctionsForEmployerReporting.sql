


CREATE PROCEDURE [dbo].[Reporting_Employer]
	@DateFrom	DATETIME,
	@DateTo	DATETIME,
	@County	NVARCHAR(MAX),
	@Office NVARCHAR(MAX),
	@WIB NVARCHAR(MAX),
	@Radius INT,
	@ZIP NVARCHAR(20),
	@OccupationGroup NVARCHAR(MAX),
	@DetailedOccupation NVARCHAR(MAX),
	@SalaryFrequency INT,
	@MinSalary	DECIMAL,
	@MaxSalary	DECIMAL,
	@JobStatus	INT,
	@EducationLevel	INT,
	@SearchTerms NVARCHAR(MAX),
	@SearchType INT,
	@KeyWordsInJobTitle BIT,
	@TargetedSectors NVARCHAR(MAX), -- To do
	@WOTCInterests NVARCHAR(MAX),
	@FEIN NVARCHAR(10),
	@EmployerName NVARCHAR(50),
	@JobTitle NVARCHAR(50),
	@Flags INT,
	@SearchAccountCreationDate BIT,
	@SearchAssistanceRequested BIT,
	@Compliance INT,
	@SuppressJobOrder BIT,
	@SelfService BIT,
	@StaffAssisted BIT,
	@Spidered BIT,
	@PageNumber	INT,
	@PageSize	INT,
	@SortOrder	VARCHAR(20)

AS
DECLARE @JobCount TABLE
(
	EmployerId int,
	JobCount int
)
DECLARE @Counties TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @Offices TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @WIBs TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @ONetOccupationGroups TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @ONetDetailedOccupations TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @TargetedSector TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @WOTCInterest TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @KeywordSearchTerms TABLE
(		
	Id int,
	Value nvarchar(100),
	Processed bit default 0
)
DECLARE @Zips TABLE
(
	ZipCode varchar(10)
)
CREATE TABLE #JobCounter
(
	Id INT, 
	EmployerName NVARCHAR(50),
	ContactFullName NVARCHAR(100), 
	City NVARCHAR(100), 
	AccountCreationDate DATETIME,
	JobId INT,
	JobCount INT DEFAULT(0)
)
CREATE TABLE #KeyWordEmployer
(
	Id	bigint
)

DECLARE @KeyWordSearchSql nvarchar(max)
DECLARE @CurrentKeyWord nvarchar(max)
DECLARE @CurrentKeyWordId nvarchar(max)
DECLARE @ProcessKeyWords BIT
DECLARE @SearchOperator nvarchar(4)
set @ProcessKeyWords = 0
set @KeyWordSearchSql= ''

if @SearchType = 1
	set @SearchOperator = 'and'
else if @SearchType = 2 or @KeyWordsInJobTitle = 1
	set @SearchOperator = 'or'


INSERT INTO @KeywordSearchTerms
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@SearchTerms,''), '|');

INSERT INTO @ONetDetailedOccupations
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@DetailedOccupation, ''), '|');


INSERT INTO @ONetOccupationGroups
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@OccupationGroup, ''), '|');
		
INSERT INTO @Counties
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@County, ''), '|');
	
INSERT INTO @Offices
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@Office, ''), '|');
	
INSERT INTO @WIBs
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@WIB, ''), '|');


INSERT INTO @TargetedSector
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@TargetedSectors, ''), '|');
	
INSERT INTO @WOTCInterest
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@WOTCInterests, ''), '|');


if (rtrim(ltrim(@ZIP)) <> '')
begin
	insert into @Zips
	select
		z2.Code
	from
		PostalCode z1
	cross join PostalCode z2
	where
		z1.Code = rtrim(ltrim(@ZIP))
	and dbo.CalculateDistance(z1.longitude, z1.latitude, z2.longitude, z2.latitude) < @Radius
end
else
begin
	insert into @Zips
	select null
end

if exists (select 1 from @KeywordSearchTerms where Value is not null)
begin
	set @ProcessKeyWords = 1
	while exists (select 1 from @KeywordSearchTerms where Processed = 0)
	begin
		select top 1 @CurrentKeyWordId = Id, @CurrentKeyWord = Value from @KeywordSearchTerms where Processed = 0
		
		if LEN(@KeyWordSearchSql) > 0
			set @KeyWordSearchSql = @KeyWordSearchSql + ' ' + @SearchOperator + ' '
		else
			set @KeyWordSearchSql = ' '	
		
		if @KeyWordsInJobTitle = 1 -- We only need to worry about job title and not the others
			set @KeyWordSearchSql = @KeyWordSearchSql + 'jo.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
		else
		begin
			set @KeyWordSearchSql = @KeyWordSearchSql + '(jo.JobTitle like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
			set @KeyWordSearchSql = @KeyWordSearchSql + ' or bu.Name like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'''
			set @KeyWordSearchSql = @KeyWordSearchSql + ' or jo.Description like ''%' + replace(replace(replace(@CurrentKeyWord, '''',''''''), '--', ''),';','') + '%'')'
		end
						
		update @KeywordSearchTerms set Processed = 1 where @CurrentKeyWordId = Id and @CurrentKeyWord = Value
	end
	set @KeyWordSearchSql = 'insert into #KeyWordEmployer (id) select distinct e.id from Employee e left join Job jo on e.Id = jo.EmployeeId where ' + @KeyWordSearchSql
	execute sp_executesql @KeyWordSearchSql;
end
else
begin
	insert into #KeyWordEmployer (Id) Values (NULL)
end;
	
INSERT INTO #JobCounter
(
	Id, 
	EmployerName,
	ContactFullName, 
	City, 
	AccountCreationDate,
	JobId
)
select DISTINCT
	bu.Id, 
	bu.Name,
	p.FirstName + ' ' + p.LastName, 
	bua.TownCity + ', ' + stli.Value, 
	u.CreatedOn,
	jo.Id
	
--from BusinessUnit bu
--inner join EmployeeBusinessUnit ebu
--	on ebu.BusinessUnitId = bu.Id
--inner join Employee e
--	on e.Id = ebu.EmployeeId

-- from Employee e	
--inner join EmployeeBusinessUnit ebu
--	on e.Id = ebu.EmployeeId
--inner join BusinessUnit bu
--	on ebu.BusinessUnitId = bu.Id

from EmployeeBusinessUnit ebu
inner join Employee e
on e.Id = ebu.EmployeeId
inner join BusinessUnit bu
on bu.Id = ebu.BusinessUnitId
		
left join Employer emp
	on emp.Id = e.EmployerId
	
left join Job jo
	on e.Id = jo.EmployeeId
	
left join Person p
	on e.PersonId = p.Id
	
left join BusinessUnitAddress bua
	on bu.Id = bua.BusinessUnitId
	
left join [User] u
	on p.Id = u.PersonId

left join CodeItem coci
	on coci.Id = bua.CountyId
left join LocalisationItem coli
	on coli.[Key] = coci.[Key]
	
left join CodeItem sfci
	on sfci.Id = jo.SalaryFrequencyId
left join LocalisationItem sfli
	on sfli.[Key] = sfci.[Key]
	
left join CodeItem stci
	on stci.Id = bua.StateId
left join LocalisationItem stli
	on stli.[Key] = stci.[Key]
		
left join Onet o
	on  o.Id = jo.OnetId
left join CodeItem jfci
	on jfci.[Id] = o.JobFamilyId
left join LocalisationItem jfli
	on jfli.[Key] = jfci.[key]
	

	
left join @Counties joc
	on coli.Value LIKE '%' + joc.Value + '%' OR joc.Value IS NULL
left join @Offices joo
	on bua.TownCity LIKE '%' + joo.Value + '%' OR joo.Value IS NULL
left join @Zips z
	on bua.PostcodeZip = z.ZipCode or z.ZipCode IS NULL
left join #KeyWordEmployer kst
	on kst.Id = e.Id or @ProcessKeyWords = 0
left join @WOTCInterest wi
	on jo.WorkOpportunitiesTaxCreditHires LIKE '%' + wi.Value + '%' OR wi.Value IS NULL
left join @ONetOccupationGroups oog
	on jfli.Value LIKE '%' + oog.Value + '%' OR oog.Value IS NULL
left join @ONetDetailedOccupations odo
	on o.[Key] LIKE '%' + odo.Value + '%' OR odo.Value IS NULL
WHERE
	(ISNULL(@SalaryFrequency,0) = 0
	OR
		(sfli.Value = @SalaryFrequency
		AND	jo.MinSalary < @MaxSalary
		AND jo.MaxSalary > @MinSalary
		)
	)
and (@JobStatus = 0 OR jo.JobStatus & @JobStatus <> 0)
and (@EducationLevel = 0 OR jo.MinimumEducationLevel & @EducationLevel <> 0)
and ISNULL(emp.FederalEmployerIdentificationNumber, '') LIKE '%' + ISNULL(@FEIN, '') + '%'
and ISNULL(bu.Name, '') LIKE '%' + ISNULL(@EmployerName,'') + '%'
and ISNULL(jo.JobTitle, '') LIKE '%' + ISNULL(@JobTitle,'') + '%'
and (@Flags = 0 OR jo.WorkOpportunitiesTaxCreditHires & @Flags <> 0)
and ((jo.ScreeningPreferences > 0 and @SearchAssistanceRequested = 1) or @SearchAssistanceRequested = 0)
and (@Compliance = 0 OR (jo.ForeignLabourCertification + 2 * jo.ForeignLabourCertificationOther + 4 * jo.CourtOrderedAffirmativeAction + 8 * jo.FederalContractor) & @Compliance <> 0)
--and ((jo.SuppressJobOrder = 1 and @SuppressJobOrder = 1) or @SuppressJobOrder = 0)
--and ((jo.IsSelfService = 1 and @SelfService = 1) or @SelfService = 0)
--and ((jo.IsStaffAssisted = 1 and @StaffAssisted = 1) or @StaffAssisted = 0)
--and ((jo.Spidered = 1 and @Spidered = 1) or @Spidered = 0)
--and ((@SearchAccountCreationDate = 1 and u.CreatedOn between @DateFrom and @DateTo) or @SearchAccountCreationDate = 0)

	
INSERT INTO @JobCount
(
	EmployerId,
	JobCount
)
SELECT
	jc.Id,
	COUNT(1)
FROM
	#JobCounter jc
WHERE
	jc.JobId IS NOT NULL
GROUP BY
	jc.Id;


with joborders as
(
	select
		CASE lower(@SortOrder)
			when 'employer_asc' then ROW_NUMBER()OVER (ORDER BY joi.EmployerName ASC, joi.Id ASC)
			when 'contact_asc' then ROW_NUMBER()OVER (ORDER BY joi.ContactFullName ASC, joi.Id ASC)
			when 'city_asc' then ROW_NUMBER()OVER (ORDER BY joi.City ASC, joi.Id ASC)
			when 'listings_asc' then ROW_NUMBER()OVER (ORDER BY joi.Listings ASC, joi.Id ASC)
			when 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY joi.AccountCreationDate ASC, joi.Id ASC)
			when 'accountcreation_desc' then ROW_NUMBER()OVER (ORDER BY joi.AccountCreationDate DESC, joi.Id ASC)
			when 'listings_desc' then ROW_NUMBER()OVER (ORDER BY joi.Listings DESC, joi.Id ASC)
			when 'city_desc' then ROW_NUMBER()OVER (ORDER BY joi.City DESC, joi.Id ASC)
			when 'contact_desc' then ROW_NUMBER()OVER (ORDER BY joi.ContactFullName DESC, joi.Id ASC)
			when 'employer_desc' then ROW_NUMBER()OVER (ORDER BY joi.EmployerName DESC, joi.Id ASC)
			else ROW_NUMBER()OVER (ORDER BY joi.EmployerName ASC, joi.Id ASC)
		END as pagingId,
		CASE @SortOrder
			when 'employer_asc' then ROW_NUMBER()OVER (ORDER BY joi.EmployerName DESC, joi.Id DESC)
			when 'contact_asc' then ROW_NUMBER()OVER (ORDER BY joi.ContactFullName DESC, joi.Id DESC)
			when 'city_asc' then ROW_NUMBER()OVER (ORDER BY joi.City DESC, joi.Id DESC)
			when 'listings_asc' then ROW_NUMBER()OVER (ORDER BY joi.Listings DESC, joi.Id DESC)
			when 'accountcreation_asc' then ROW_NUMBER()OVER (ORDER BY joi.AccountCreationDate DESC, joi.Id DESC)
			when 'accountcreation_desc' then ROW_NUMBER()OVER (ORDER BY joi.AccountCreationDate ASC, joi.Id DESC)
			when 'listings_desc' then ROW_NUMBER()OVER (ORDER BY joi.Listings ASC, joi.Id DESC)
			when 'city_desc' then ROW_NUMBER()OVER (ORDER BY joi.City ASC, joi.Id DESC)
			when 'contact_desc' then ROW_NUMBER()OVER (ORDER BY joi.ContactFullName ASC, joi.Id DESC)
			when 'employer_desc' then ROW_NUMBER()OVER (ORDER BY joi.EmployerName ASC, joi.Id DESC)
			else ROW_NUMBER()OVER (ORDER BY joi.EmployerName DESC, joi.Id DESC)
		END as pagingRevId,
		joi.Id,
		joi.EmployerName, 
		joi.ContactFullName, 
		joi.City, 
		joi.AccountCreationDate,
		joi.Listings
	from
	(
		SELECT DISTINCT
			jc.Id,
			jc.EmployerName, 
			jc.ContactFullName, 
			jc.City, 
			jc.AccountCreationDate,
			ISNULL(jco.JobCount,0) AS Listings
		FROM
			#JobCounter jc
		left join @JobCount jco on jc.Id =  jco.EmployerId
	) joi	
)
select
	jo.Id,
	jo.pagingId,
	jo.pagingRevId,
	jo.EmployerName,
	jo.ContactFullName, 
	jo.City, 
	jo.Listings,
	jo.AccountCreationDate
from
	joborders jo
where
	pagingId between  ((@PageNumber - 1) * @PageSize) + 1 and (@PageNumber * @PageSize)
order by
	pagingId asc


GO


CREATE FUNCTION [dbo].[Split]
(
	@List nvarchar(2000),
	@SplitOn nvarchar(5)
)  
RETURNS @RtnValue table 
(
		
	Id int identity(1,1),
	Value nvarchar(100)
) 
AS  
BEGIN
While (Charindex(@SplitOn,@List)>0)
 Begin
	Insert Into @RtnValue (value)
	Select 
		Value = ltrim(rtrim(Substring(@List,1,Charindex(@SplitOn,@List)-1)))
		
	Set @List = Substring(@List,Charindex(@SplitOn,@List)+len(@SplitOn),len(@List))
End
Insert Into @RtnValue (Value)
Select Value = ltrim(rtrim(@List))

Return
END

GO



CREATE Function [dbo].[CalculateDistance]
    (@Longitude1 Decimal(8,5),
    @Latitude1   Decimal(8,5),
    @Longitude2  Decimal(8,5),
    @Latitude2   Decimal(8,5))
Returns Float
As
Begin
Declare @Temp Float
 
Set @Temp = sin(@Latitude1/57.2957795130823) * sin(@Latitude2/57.2957795130823) + cos(@Latitude1/57.2957795130823) * cos(@Latitude2/57.2957795130823) * cos(@Longitude2/57.2957795130823 - @Longitude1/57.2957795130823)
 
if @Temp > 1
    Set @Temp = 1
Else If @Temp < -1
    Set @Temp = -1
 
Return (3958.75586574 * acos(@Temp) )
 
End

GO


