
ALTER VIEW [dbo].[EmployerAccountReferralView]
AS
SELECT 
	Employee.Id,
	Employer.Id AS EmployerId,
	Employer.Name AS EmployerName,
	Employee.Id AS EmployeeId,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	[USER].CreatedOn AS AccountCreationDate,
	[dbo].[GetBusinessDays]([USER].CreatedOn, GETDATE()) AS TimeInQueue,
	BusinessUnitAddress.Line1 AS EmployerAddressLine1,
	BusinessUnitAddress.TownCity AS EmployerAddressTownCity,
	BusinessUnitAddress.StateId AS EmployerAddressStateId,
	BusinessUnitAddress.PostcodeZip AS EmployerAddressPostcodeZip,
	CASE 
		WHEN BusinessUnit.PrimaryPhoneType = 'Phone' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.PrimaryPhoneType = 'Mobile' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.AlternatePhone1Type = 'Phone' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone1Type = 'Mobile' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone2Type = 'Phone' THEN BusinessUnit.AlternatePhone2
		WHEN BusinessUnit.AlternatePhone2Type = 'Mobile' THEN BusinessUnit.AlternatePhone2
	END AS EmployerPhoneNumber,
	CASE 
		WHEN BusinessUnit.PrimaryPhoneType = 'Fax' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.AlternatePhone1Type = 'Fax' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone2Type = 'Fax' THEN BusinessUnit.AlternatePhone2
	END AS EmployerFaxNumber,
	PhoneNumber.Number AS EmployeePhoneNumber,
	FaxNumber.Number AS EmployeeFaxNumber,
	BusinessUnit.Url AS EmployerUrl,
	Employer.FederalEmployerIdentificationNumber AS EmployerFederalEmployerIdentificationNumber,
	Employer.StateEmployerIdentificationNumber AS EmployerStateEmployerIdentificationNumber,
	BusinessUnit.IndustrialClassification AS EmployerIndustrialClassification,
	PersonAddress.Line1 AS EmployeeAddressLine1,
	PersonAddress.TownCity AS EmployeeAddressTownCity,
	PersonAddress.StateId AS EmployeeAddressStateId,
	PersonAddress.PostcodeZip AS EmployeeAddressPostcodeZip,
	Person.EmailAddress AS EmployeeEmailAddress,
	Employee.ApprovalStatus AS EmployeeApprovalStatus,
	Employer.ApprovalStatus AS EmployerApprovalStatus
FROM 
	Employer WITH (NOLOCK)
	INNER JOIN Employee  WITH (NOLOCK) ON Employer.Id = Employee.EmployerId
	INNER JOIN Person  WITH (NOLOCK) ON Employee.PersonId = Person.Id
	INNER JOIN [User]  WITH (NOLOCK) ON Person.Id = [User].PersonId
	INNER JOIN dbo.EmployeeBusinessUnit WITH (NOLOCK) ON dbo.Employee.Id = dbo.EmployeeBusinessUnit.EmployeeId AND dbo.EmployeeBusinessUnit.[Default] = 1
	INNER JOIN dbo.BusinessUnit WITH (NOLOCK) ON dbo.EmployeeBusinessUnit.BusinessUnitId = dbo.BusinessUnit.Id
	INNER JOIN dbo.BusinessUnitAddress WITH (NOLOCK) ON dbo.BusinessUnit.Id = dbo.BusinessUnitAddress.BusinessUnitId AND dbo.BusinessUnitAddress.IsPrimary = 1
	LEFT OUTER JOIN PersonAddress  WITH (NOLOCK) ON Person.Id = PersonAddress.PersonId 
	LEFT OUTER JOIN PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND (PhoneNumber.PhoneType = 0 OR PhoneNumber.PhoneType = 1) AND PhoneNumber.IsPrimary = 1
	LEFT OUTER JOIN PhoneNumber AS FaxNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND PhoneNumber.PhoneType = 4
WHERE
	Employer.ApprovalStatus = 1 OR Employee.ApprovalStatus = 1 OR Employee.ApprovalStatus = 4 OR Employer.ApprovalStatus = 4
















GO

