

CREATE PROCEDURE [dbo].[Reporting_StaffActivity]
	@County	NVARCHAR(MAX),
	@Office NVARCHAR(MAX),
	@WIB NVARCHAR(MAX),
	@DateFrom	DATETIME,
	@DateTo	DATETIME,
	@NoOfLoginsDays INT,
	@JobOutcome INT,
	@PageNumber	INT,
	@PageSize	INT,
	@SortOrder	VARCHAR(20)
AS
BEGIN
SET NOCOUNT ON;


DECLARE @Counties TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @Offices TABLE
(		
	Id int,
	Value nvarchar(100)
)
DECLARE @WIBs TABLE
(		
	Id int,
	Value nvarchar(100)
)


INSERT INTO @Counties
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@County, ''), '|');
	
INSERT INTO @Offices
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@Office, ''), '|');
	
INSERT INTO @WIBs
(
	Id,
	Value
)
SELECT
	*
FROM
	dbo.Split(NULLIF(@WIB, ''), '|');


with activitydata as(
	select
		case LOWER(@SortOrder)
			when 'staff_asc' then ROW_NUMBER()OVER (ORDER BY p.FirstName ASC, p.LastName ASC, u.Id ASC)
			when 'location_asc' then ROW_NUMBER()OVER (ORDER BY pa.TownCity ASC, u.Id ASC)
			when 'lastlogin_asc' then ROW_NUMBER()OVER (ORDER BY u.LoggedInOn ASC, u.Id ASC)
			when 'nooflogins_asc' then ROW_NUMBER()OVER (ORDER BY logincount ASC, u.Id ASC)
			when 'dayssincelastlogin_asc' then ROW_NUMBER()OVER (ORDER BY DATEDIFF(DD,u.LoggedInOn, GETDATE()) ASC, u.Id ASC)
			when 'referrals_asc' then ROW_NUMBER()OVER (ORDER BY referrals ASC, u.Id ASC)
			--when 'resumescreated_asc' then ROW_NUMBER()OVER (ORDER BY resumescreated ASC, u.Id ASC)
			--when 'resumesedited_asc' then ROW_NUMBER()OVER (ORDER BY resumesedited ASC, u.Id ASC)
			--when 'seekerassisted_asc' then ROW_NUMBER()OVER (ORDER BY seekerassisted ASC, u.Id ASC)
			--when 'seekerunsubscribed_asc' then ROW_NUMBER()OVER (ORDER BY seekerunsubscribed ASC, u.Id ASC)
			--when 'employercreated_asc' then ROW_NUMBER()OVER (ORDER BY employercreated ASC, u.Id ASC)
			--when 'employerassisted_asc' then ROW_NUMBER()OVER (ORDER BY employerassisted ASC, u.Id ASC)
			when 'jobordercreated_asc' then ROW_NUMBER()OVER (ORDER BY jobcreated ASC, u.Id ASC)
			when 'joborderedited_asc' then ROW_NUMBER()OVER (ORDER BY jobeditted ASC, u.Id ASC)
			when 'joborderedited_desc' then ROW_NUMBER()OVER (ORDER BY jobeditted DESC, u.Id ASC)
			when 'jobordercreated_desc' then ROW_NUMBER()OVER (ORDER BY jobcreated DESC, u.Id ASC)
			--when 'employerassisted_desc' then ROW_NUMBER()OVER (ORDER BY employerassisted DESC, u.Id ASC)
			--when 'employercreated_desc' then ROW_NUMBER()OVER (ORDER BY employercreated DESC, u.Id ASC)
			--when 'seekerunsubscribed_desc' then ROW_NUMBER()OVER (ORDER BY seekerunsubscribed DESC, u.Id ASC)
			--when 'seekerassisted_desc' then ROW_NUMBER()OVER (ORDER BY seekerassisted DESC, u.Id ASC)			
			--when 'resumesedited_desc' then ROW_NUMBER()OVER (ORDER BY resumesedited DESC, u.Id ASC)
			--when 'resumescreated_desc' then ROW_NUMBER()OVER (ORDER BY resumescreated DESC, u.Id ASC)
			when 'referalls_desc' then ROW_NUMBER()OVER (ORDER BY referrals DESC, u.Id ASC)
			when 'dayssincelastlogin_desc' then ROW_NUMBER()OVER (ORDER BY DATEDIFF(DD,u.LoggedInOn, GETDATE()) DESC, u.Id ASC)
			when 'nooflogins_desc' then ROW_NUMBER()OVER (ORDER BY logincount DESC, u.Id ASC)
			when 'lastlogin_desc' then ROW_NUMBER()OVER (ORDER BY u.LoggedInOn DESC, u.Id ASC)
			when 'location_desc' then ROW_NUMBER()OVER (ORDER BY pa.TownCity DESC, u.Id ASC)
			when 'staff_desc' then ROW_NUMBER()OVER (ORDER BY p.FirstName DESC, p.LastName DESC, u.Id ASC)
			else ROW_NUMBER()OVER (ORDER BY p.FirstName ASC, p.LastName ASC, u.Id ASC)
		end as pagingId,
		case LOWER(@SortOrder)
			when 'staff_asc' then ROW_NUMBER()OVER (ORDER BY p.FirstName DESC, p.LastName DESC, u.Id DESC)
			when 'location_asc' then ROW_NUMBER()OVER (ORDER BY pa.TownCity DESC, u.Id DESC)
			when 'lastlogin_asc' then ROW_NUMBER()OVER (ORDER BY u.LoggedInOn DESC, u.Id DESC)
			when 'nooflogins_asc' then ROW_NUMBER()OVER (ORDER BY logincount DESC, u.Id DESC)
			when 'dayssincelastlogin_asc' then ROW_NUMBER()OVER (ORDER BY DATEDIFF(DD,u.LoggedInOn, GETDATE()) DESC, u.Id DESC)
			when 'referrals_asc' then ROW_NUMBER()OVER (ORDER BY referrals DESC, u.Id DESC)
			--when 'resumescreated_asc' then ROW_NUMBER()OVER (ORDER BY resumescreated DESC, u.Id DESC)
			--when 'resumesedited_asc' then ROW_NUMBER()OVER (ORDER BY resumesedited DESC, u.Id DESC)
			--when 'seekerassisted_asc' then ROW_NUMBER()OVER (ORDER BY seekerassisted DESC, u.Id DESC)
			--when 'seekerunsubscribed_asc' then ROW_NUMBER()OVER (ORDER BY seekerunsubscribed DESC, u.Id DESC)
			--when 'employercreated_asc' then ROW_NUMBER()OVER (ORDER BY seekerunsubscribed DESC, u.Id DESC)
			--when 'employerassisted_asc' then ROW_NUMBER()OVER (ORDER BY employerassisted DESC, u.Id DESC)
			when 'jobordercreated_asc' then ROW_NUMBER()OVER (ORDER BY jobcreated DESC, u.Id DESC)
			when 'joborderedited_asc' then ROW_NUMBER()OVER (ORDER BY jobeditted DESC, u.Id DESC)
			when 'joborderedited_desc' then ROW_NUMBER()OVER (ORDER BY jobeditted ASC, u.Id DESC)
			when 'jobordercreated_desc' then ROW_NUMBER()OVER (ORDER BY jobcreated ASC, u.Id DESC)
			--when 'employerassisted_desc' then ROW_NUMBER()OVER (ORDER BY employerassisted ASC, u.Id DESC)
			--when 'employercreated_desc' then ROW_NUMBER()OVER (ORDER BY seekerunsubscribed ASC, u.Id DESC)
			--when 'seekerunsubscribed_desc' then ROW_NUMBER()OVER (ORDER BY seekerunsubscribed ASC, u.Id DESC)
			--when 'seekerassisted_desc' then ROW_NUMBER()OVER (ORDER BY seekerassisted ASC, u.Id DESC)			
			--when 'resumesedited_desc' then ROW_NUMBER()OVER (ORDER BY resumesedited ASC, u.Id DESC)
			--when 'resumescreated_desc' then ROW_NUMBER()OVER (ORDER BY resumescreated ASC, u.Id DESC)
			when 'referalls_desc' then ROW_NUMBER()OVER (ORDER BY referrals ASC, u.Id DESC)
			when 'dayssincelastlogin_desc' then ROW_NUMBER()OVER (ORDER BY DATEDIFF(DD,u.LoggedInOn, GETDATE()) ASC, u.Id DESC)
			when 'nooflogins_desc' then ROW_NUMBER()OVER (ORDER BY logincount ASC, u.Id DESC)
			when 'lastlogin_desc' then ROW_NUMBER()OVER (ORDER BY u.LoggedInOn ASC, u.Id DESC)
			when 'location_desc' then ROW_NUMBER()OVER (ORDER BY pa.TownCity ASC, u.Id DESC)
			when 'staff_desc' then ROW_NUMBER()OVER (ORDER BY p.FirstName ASC, p.LastName ASC, u.Id DESC)
			else ROW_NUMBER()OVER (ORDER BY p.FirstName DESC, p.LastName DESC, u.Id DESC)
		end as pagingRevId,
		u.Id,
		(p.FirstName + ' ' + p.LastName) as FullName,
		(pa.TownCity + ', ' + stli.Value) as Location,
		(u.LoggedInOn) as LastLogin,
		DATEDIFF(DD,u.LoggedInOn, GETDATE()) as dayssincelastlogin,
		isNull(sua.logincount,0) as logincount,
		isNull(eaa.referrals,0) as referrals,
		0 as resumescreated,
		0 as resumesedited,
		0 as jobseekerassisted,
		0 as seekersunsubscribed,
		0 as employerassisted,
		isNull(eaa.jobcreated,0) as jobcreated,
		isNull(eaa.jobeditted,0) as jobedited
		
		-- TEMP CHANGE TO GET PAGE WORKING SINCE JOBSEEKER DATA NOT AVAILABLE
		--isNull(jsaa.ResumesCreated,0) as resumescreated,
		--isNull(jsaa.ResumesEdited,0) as resumesedited,
		--ISNULL(jsaa.seekerassisted, 0) as jobseekerassisted,
		--isNull(jsaa.SeekerUnsubscribed,0) as seekersunsubscribed,
		--ISNULL(eaa.employerassisted, 0) as employerassisted,
		--isNull(eaa.jobeditted,0) as jobedited
						
		from [User] u
		
		inner join Person p on p.Id = u.PersonId
		left join PersonAddress pa on p.id = pa.PersonId
		left join CodeItem stci	on stci.Id = pa.StateId
		left join LocalisationItem stli	on stli.[Key] = stci.[Key]	
		left join CodeItem coci	on coci.Id = pa.CountyId
		left join LocalisationItem coli	on coli.[Key] = coci.[Key]
		
		left join @Counties c on coli.Value LIKE '%' + c.Value + '%' OR c.Value IS NULL
		left join @Offices o	on pa.TownCity LIKE '%' + o.Value + '%' OR o.Value IS NULL
		-- inner join @WIBs w on e.WIBLocation LIKE '%' + w.Value + '%' OR w.Value IS NULL
		left join
		(
			select
				ea.UserId,
				SUM(case when ActionTypeId = 4784212 then 1 else 0 end) as referrals,
				--SUM(case when ea.AssistAction = 1 then 1 else 0 end) as employerassisted,
				SUM(case when ActionTypeId = 5520920 then 1 else 0 end) as jobcreated,
				SUM(case when ActionTypeId = 99962 then 1 else 0 end) as jobeditted
			from ActionEvent ea
			where ea.ActionedOn between @DateFrom and @DateTo
			group by ea.UserId
		) eaa on u.Id = eaa.UserId
		--left join
		--(
		--	select
		--		jsa.UserId,
		--		SUM(case when jsa.Activity = 'ResumesCreated' then 1 else 0 end) as ResumesCreated,
		--		SUM(case when jsa.Activity = 'ResumesEdited' then 1 else 0 end) as ResumesEdited,
		--		SUM(case when jsa.AssistAction = 1 then 1 else 0 end) as seekerassisted,
		--		SUM(case when jsa.Activity = 'UnsubscribedFromService' then 1 else 0 end) as SeekerUnsubscribed
		--	from JobSeekerActivity jsa
		--	where jsa.ActivityDate between @DateFrom and @DateTo
		--	group by jsa.UserId
		--) jsaa on a.UserId = jsaa.UserId
		left join		
		(
			select
				us.Id,
				COUNT(1) as logincount
			from ActionEvent ae
			inner join [User] us on us.Id = ae.EntityId
			where ae.ActionedOn between GETDATE() and DATEADD(dd, (@NoOfLoginsDays * -1), GETDATE()) and ae.ActionTypeId = 80660
			group by us.Id
		) sua on u.Id = sua.Id		
		
	where u.UserType = 1			
)

select
	ad.pagingId,
	ad.pagingRevId,
	ad.Id,
	ad.FullName,
	ad.Location,
	ad.LastLogin,
	ad.logincount,
	ad.dayssincelastlogin,
	ad.referrals,
	ad.resumescreated,
	ad.resumesedited,
	ad.jobseekerassisted,
	ad.seekersunsubscribed,
	ad.employerassisted,
	ad.jobcreated,
	ad.jobedited
from
	activitydata ad
where
	pagingId between  ((@PageNumber - 1) * @PageSize) + 1 and (@PageNumber * @PageSize)
order by
	pagingId asc

END






GO


