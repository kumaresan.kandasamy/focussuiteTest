

CREATE VIEW [dbo].[AssistUserJobSeekersAssistedView]
AS

SELECT   dbo.[User].Id AS UserId,
					dbo.Person.FirstName + ' ' + dbo.Person.LastName AS Name,
				  dbo.ActionType.Name AS ActionName,
				  dbo.ActionEvent.ActionedOn

FROM    dbo.[ActionEvent] WITH (NOLOCK)
INNER JOIN dbo.[User] WITH (NOLOCK) ON dbo.[User].Id = dbo.ActionEvent.UserId

-- needed for ActionName
INNER JOIN dbo.ActionType WITH (NOLOCK) ON dbo.ActionEvent.ActionTypeId = dbo.ActionType.Id

-- Assumption that created events will have UserId as EntityId
LEFT JOIN dbo.[User] AS UserAction WITH (NOLOCK) ON UserAction.Id = dbo.ActionEvent.EntityId
LEFT JOIN dbo.Person  WITH (NOLOCK) ON dbo.Person.Id = UserAction.PersonId

WHERE    ((dbo.ActionType.Name = 'SelfReferral') OR
                      (dbo.ActionType.Name = 'UpdateResume') OR                      
                      (dbo.ActionType.Name = 'EmailResume') OR
                      (dbo.ActionType.Name = 'DownloadResume') OR
                      (dbo.ActionType.Name = 'SaveJobAlert'))


GO


