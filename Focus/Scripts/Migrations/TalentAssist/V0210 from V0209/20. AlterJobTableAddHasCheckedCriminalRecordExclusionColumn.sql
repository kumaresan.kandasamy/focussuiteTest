USE [FocusDev]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER TABLE [dbo].[Job]
ADD  [HasCheckedCriminalRecordExclusion] [bit] NOT NULL DEFAULT 0

GO
