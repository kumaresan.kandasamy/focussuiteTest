-- Create CandidateIssueView
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create VIEW [dbo].[CandidateIssueView] AS
select u.Id, u.PersonId, u.LastLoggedInOn, re.FirstName, re.LastName, re.DateOfBirth, p.SocialSecurityNumber, re.EmailAddress 
from [user] u 
inner join person p on u.PersonId = p.Id
inner join [resume] re on p.Id = re.PersonId
where u.UserType = 4 or u.UserType = 5
and re.IsDefault = 1
GO


