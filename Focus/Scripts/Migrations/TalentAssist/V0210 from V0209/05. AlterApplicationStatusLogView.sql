USE [FocusDev]
GO

/****** Object:  View [dbo].[ApplicationStatusLogView]    Script Date: 01/24/2013 13:08:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











ALTER VIEW [dbo].[ApplicationStatusLogView]
AS

SELECT 
	StatusLog.Id AS Id,
	StatusLog.ActionedOn AS ActionedOn,
	CandidateApplication.ApplicationScore AS CandidateApplicationScore,
	Candidate.FirstName AS CandidateFirstName,
	Candidate.LastName AS CandidateLastName,
	Candidate.YearsExperience AS CandidateYearsExperience,
	Candidate.IsVeteran AS CandidateIsVeteran,
	StatusLog.NewStatus AS CandidateApplicationStatus,
	CandidateApplication.JobId AS JobId,
	LatestJob.JobTitle AS LatestJobTitle,
	LatestJob.Employer AS LatestJobEmployer,
	LatestJob.StartYear AS LatestJobStartYear,
	LatestJob.EndYear AS LatestJobEndYear,
	CandidateApplication.Id AS CandidateApplicationId,
	Job.BusinessUnitId AS BusinessUnitId,
	Job.JobTitle AS JobTitle

FROM StatusLog WITH (NOLOCK)
	INNER JOIN CandidateApplication WITH (NOLOCK) ON StatusLog.EntityId = CandidateApplication.Id
	INNER JOIN Candidate WITH (NOLOCK) ON CandidateApplication.CandidateId = Candidate.Id
	
	INNER JOIN Job WITH (NOLOCK) ON Job.Id = CandidateApplication.JobId
	
	LEFT OUTER JOIN (SELECT JobTitle, Employer, StartYear, EndYear, CandidateId FROM 
						(SELECT JobTitle, Employer, StartYear, EndYear, CandidateId, RANK() OVER (PARTITION BY CandidateId ORDER BY EndYear desc, StartYear desc, Id) RankScore
						FROM CandidateWorkHistory WITH (NOLOCK)) AS RankedCandidateWorkHistory WHERE RankScore = 1) AS LatestJob 
	ON Candidate.Id = LatestJob.CandidateId











GO


