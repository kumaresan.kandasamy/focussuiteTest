UPDATE [Config.CodeItem] SET CustomFilterId = '104' WHERE [Key] = 'DrivingLicenceEndorsements.Airbrakes'
UPDATE [Config.CodeItem] SET CustomFilterId = '102' WHERE [Key] = 'DrivingLicenceEndorsements.DoublesTriples'
UPDATE [Config.CodeItem] SET CustomFilterId = '103' WHERE [Key] = 'DrivingLicenceEndorsements.HazerdousMaterials'
UPDATE [Config.CodeItem] SET CustomFilterId = '109' WHERE [Key] = 'DrivingLicenceEndorsements.Limo'
UPDATE [Config.CodeItem] SET CustomFilterId = '108' WHERE [Key] = 'DrivingLicenceEndorsements.Motorcycle'
UPDATE [Config.CodeItem] SET CustomFilterId = '101' WHERE [Key] = 'DrivingLicenceEndorsements.PassTransport'
UPDATE [Config.CodeItem] SET CustomFilterId = '106' WHERE [Key] = 'DrivingLicenceEndorsements.SchoolBus'
UPDATE [Config.CodeItem] SET CustomFilterId = '107' WHERE [Key] = 'DrivingLicenceEndorsements.TankHazard'
UPDATE [Config.CodeItem] SET CustomFilterId = '105' WHERE [Key] = 'DrivingLicenceEndorsements.TankVehicle'
