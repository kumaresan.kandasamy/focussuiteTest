IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistJobSeekerReferralApprovalViewer')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistJobSeekerReferralApprovalViewer', 'Assist Job Seeker Referral Approval Viewer')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistEmployerAccountApprovalViewer')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistEmployerAccountApprovalViewer', 'Assist Employer Account Approval Viewer')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistPostingApprovalViewer')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistPostingApprovalViewer', 'Assist Posting Approval Viewer')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistStaffView')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistStaffView', 'Assist Staff View')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistStaffViewEdit')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistStaffViewEdit', 'Assist Staff View Edit')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistStaffResetPassword')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistStaffResetPassword', 'Assist Staff Reset Password')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistStaffDeactivateActivate')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistStaffDeactivateActivate', 'Assist Staff Deactivate Activate')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistStaffDelete')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistStaffDelete', 'Assist Staff Delete')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistJobSeekersAccountBlocker')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistJobSeekersAccountBlocker', 'Assist Job Seekers Account Blocker')
END

IF EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistOfficesCreateDelete')
BEGIN
	UPDATE [dbo].[Data.Application.Role]
	SET [Key] = 'AssistOfficesCreate', Value = 'Assist Create Offices'
  WHERE [Key] = 'AssistOfficesCreateDelete'
END
ELSE
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistOfficesCreate', 'Assist Create Offices')
END


IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AssistOfficesActivateDeactivate')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AssistOfficesActivateDeactivate', 'Assist Activate Deactivate Office')
END