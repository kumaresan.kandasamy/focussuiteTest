IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 34)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		34, 
		'#FOCUSTALENT# Password Reset',
		'Thank you for requesting password help. Please click the URL below to access #FOCUSTALENT# and change your password. Because your security is important to us, we encourage you to change this credential routinely in your Account Settings, and to retain your username and password in a secure location. 

#URL#

 After you log in, please take advantage of our online and staff-supported services to recruit qualified candidates, and to manage your job postings and applicants. We value your business and encourage you to post jobs often with #FOCUSEXPLORER# Don�t hesitate to contact our staff if you need additional assistance. We wish you the best in finding your next great employee! 


#FOCUSTALENT# Customer Support Team
', 
		'', 
		NULL
	)
END
