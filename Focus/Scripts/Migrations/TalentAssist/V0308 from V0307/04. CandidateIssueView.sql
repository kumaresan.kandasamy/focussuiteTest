IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.CandidateIssueView]'))
DROP VIEW [dbo].[Data.Application.CandidateIssueView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.CandidateIssueView]  
AS  
SELECT 
	p.Id, 
	p.FirstName, 
	p.LastName, 
	p.DateOfBirth, 
	p.SocialSecurityNumber, 
	ISNULL(pa.TownCity, re.TownCity) AS TownCity, 
	ISNULL(pa.StateId, re.StateId) AS StateId,
	p.EmailAddress, 
	u.LoggedInOn AS LastLoggedInOn,
	i.NoLoginTriggered,
	i.JobOfferRejectionTriggered,
	i.NotReportingToInterviewTriggered,  
    i.NotClickingOnLeadsTriggered,
    i.NotRespondingToEmployerInvitesTriggered,
    i.ShowingLowQualityMatchesTriggered,
    i.PostingLowQualityResumeTriggered,
    i.PostHireFollowUpTriggered,
    i.FollowUpRequested,
    i.NotSearchingJobsTriggered,
    i.InappropriateEmailAddress,
    i.GivingPositiveFeedback,  
    i.GivingNegativeFeedback,  
    p.AssignedToId,
    re.IsVeteran,
    u.ExternalId,
    u.Blocked
FROM  dbo.[Data.Application.User] AS u WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id 
	LEFT JOIN dbo.[Data.Application.Resume] AS re WITH (NOLOCK) ON p.Id = re.PersonId AND re.IsDefault = 1 AND re.StatusId = 1
    LEFT JOIN dbo.[Data.Application.Issues] AS i WITH (NOLOCK) on p.Id = i.PersonId  
    LEFT JOIN dbo.[Data.Application.PersonAddress] AS pa WITH (NOLOCK) ON p.id = pa.PersonId AND pa.IsPrimary = 1
WHERE (u.UserType & 4 = 4)

GO


