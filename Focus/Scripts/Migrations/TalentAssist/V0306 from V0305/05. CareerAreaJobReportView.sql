IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Library.CareerAreaJobReportView]'))
DROP VIEW [dbo].[Library.CareerAreaJobReportView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Library.CareerAreaJobReportView]

AS

SELECT 
	NEWID() AS Id,
	CA.Id AS CareerAreaId,
	JR.StateAreaId,
	JR.JobId,
	JR.HiringDemand
FROM 
	[Library.CareerArea] CA
INNER JOIN [Library.JobCareerArea] JCA
	ON JCA.CareerAreaId = CA.Id
INNER JOIN [Library.JobReport] JR
	ON JR.JobId = JCA.JobId
GO