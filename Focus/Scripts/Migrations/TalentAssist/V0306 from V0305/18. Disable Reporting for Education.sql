IF EXISTS(SELECT * FROM [Config.ConfigurationItem] WHERE [Key] = 'ExplorerSectionOrder' AND Value LIKE '%School%')
BEGIN
	UPDATE
		[Config.ConfigurationItem] 
	SET
		Value = 'False'
	WHERE 
		[Key] = 'ReportingEnabled'
END