
DECLARE @NextId BIGINT
DECLARE @CampusCount INT

DECLARE @CampusList TABLE
(
	[Index] INT IDENTITY(0, 1),
	[Name] NVARCHAR(100),
	IsNonCampus BIT DEFAULT(0)
)

INSERT INTO @CampusList ([Name], IsNonCampus)
VALUES('Non-campus', 1)

/*
INSERT INTO @CampusList ([Name])
VALUES 
('Location 1', 0),
('Location 2', 0),
('Location 3', 0)
*/

DELETE CL
FROM @CampusList CL
WHERE EXISTS(SELECT 1 FROM [Data.Application.Campus] C WHERE C.[Name] = CL.[Name])

SELECT @CampusCount = COUNT(1)
FROM @CampusList CL

BEGIN TRANSACTION

SELECT @NextId = NextId FROM KeyTable

UPDATE KeyTable SET NextId = NextId + @CampusCount

INSERT INTO [Data.Application.Campus] (Id, [Name], IsNonCampus)
SELECT @NextId + [Index], [Name], IsNonCampus FROM @CampusList

SELECT * FROM [Data.Application.Campus]

COMMIT TRANSACTION

