IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.ReferralView')
BEGIN
	DROP VIEW [dbo].[Data.Application.ReferralView]
END
GO

CREATE VIEW [dbo].[Data.Application.ReferralView]
AS

SELECT
	Posting.Id AS Id,
	[Posting].LensPostingId,
	[Application].CreatedOn AS ApplicationDate,
	Posting.JobTitle,
	[BusinessUnit].Name AS EmployerName,
	Posting.EmployerName AS PostingEmployerName,
	[Application].AutomaticallyApproved AS CandidateApplicationAutomaticallyApproved,
	[Application].PreviousApprovalStatus,
	[Person].FirstName + ' ' + [Person].LastName AS EmployeeName,
	[Resume].PersonId AS CandidateId,
	[Application].ApplicationScore AS CandidateApplicationScore,
	[Application].ApprovalStatus AS ApplicationApprovalStatus,
	[PhoneNumber].Number AS EmployeePhoneNumber,
	Person.EmailAddress AS EmployeeEmail,
	Location.Location,
	Job.ClosingOn AS JobClosingOn,
	Posting.CreatedOn AS PostingCreatedOn,
	Job.Id AS JobId,
	Job.VeteranPriorityEndDate,
	[Resume].IsVeteran
FROM [Data.Application.Application] AS [Application] WITH (NOLOCK)
	INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN [Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id
	INNER JOIN [Data.Application.JobLocation] AS Location WITH (NOLOCK) ON Location.JobId = Job.Id
	INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	INNER JOIN [Data.Application.BusinessUnit] AS [BusinessUnit] WITH (NOLOCK) ON Job.BusinessUnitId = [BusinessUnit].Id
	INNER JOIN [Data.Application.Employee] AS [Employee] WITH (NOLOCK) ON Job.EmployeeId = [Employee].Id
	INNER JOIN [Data.Application.Person] AS [Person] WITH (NOLOCK) ON Employee.PersonId = Person.Id
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS [PhoneNumber] WITH (NOLOCK) ON [Person].Id = [PhoneNumber].PersonId AND [PhoneNumber].IsPrimary = 1

UNION 

SELECT	Posting.Id AS Id, 
		Posting.LensPostingId AS LensPostingId,
		ae.ActionedOn AS ApplicationDate, 
		Posting.JobTitle AS JobTitle,
		bu.Name AS EmployerName,
		Posting.EmployerName AS PostingEmployerName,
		CAST(1 AS bit) AS CandidateApplicationAutomaticallyApproved,
		CAST(1 AS bit) As PreviousApprovalStatus,
		employee.FirstName + ' ' + employee.LastName AS EmployeeName,
		candidate.Id AS CandidateId,
		0 AS CandidateApplicationScore,
		'' AS ApplicationApprovalStatus,
		[PhoneNumber].Number AS EmployeePhoneNumber,
		employee.EmailAddress AS EmployeeEmail,
		Location.Location,
		J.ClosingOn AS JobClosingOn,
		Posting.CreatedOn AS PostingCreatedOn,
		J.Id AS JobId,
		J.VeteranPriorityEndDate,
		R.IsVeteran
FROM  dbo.[Data.Core.ActionType] AS at WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Core.ActionEvent] AS ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId
	INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON ae.EntityId = Posting.Id
	LEFT JOIN 
	(
		dbo.[Data.Application.Job] AS j WITH (NOLOCK)  
		INNER JOIN dbo.[Data.Application.BusinessUnit] AS bu WITH (NOLOCK) ON j.BusinessUnitId = bu.Id
		INNER JOIN dbo.[Data.Application.Employee] AS e WITH (NOLOCK) ON j.EmployeeId = e.Id
		INNER JOIN dbo.[Data.Application.Person] AS employee WITH (NOLOCK) ON e.PersonId = employee.Id
		LEFT JOIN [Data.Application.JobLocation] AS Location WITH (NOLOCK) ON Location.JobId = j.Id
	) ON Posting.JobId = j.Id 
	INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON ae.EntityIdAdditional02 = u.Id 
	INNER JOIN dbo.[Data.Application.Person] AS candidate WITH (NOLOCK) ON u.PersonId = candidate.Id
	INNER JOIN [Data.Application.Resume] AS R (NOLOCK) ON R.PersonId = candidate.Id AND R.IsDefault = 1 AND R.StatusId = 1
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS [PhoneNumber] WITH (NOLOCK) ON employee.Id = [PhoneNumber].PersonId AND [PhoneNumber].IsPrimary = 1
WHERE at.Name IN ('SelfReferral', 'ExternalReferral')



GO

