IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.UserActionTypeActivity' AND COLUMN_NAME = 'ActionType')
BEGIN
	ALTER TABLE
		[Data.Application.UserActionTypeActivity]
	ADD
		ActionType NVARCHAR(200) NULL
END
