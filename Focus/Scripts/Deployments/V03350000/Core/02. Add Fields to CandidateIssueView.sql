﻿IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.CandidateIssueView')
BEGIN
	DROP VIEW [dbo].[Data.Application.CandidateIssueView]
END
GO

CREATE VIEW [dbo].[Data.Application.CandidateIssueView]
AS
SELECT
	p.Id,
	p.FirstName,
	p.MiddleInitial,
	p.LastName,
	p.DateOfBirth,
	p.SocialSecurityNumber,
	p.AssignedToId,
	p.IsVeteran,
	p.EmailAddress,
	p.MigrantSeasonalFarmWorkerVerified,
	pa.TownCity,
	pa.StateId,
	i.NoLoginTriggered,
	i.JobOfferRejectionTriggered,
	i.NotReportingToInterviewTriggered,
	i.NotClickingOnLeadsTriggered,
	i.NotRespondingToEmployerInvitesTriggered,
	i.ShowingLowQualityMatchesTriggered,
	i.PostingLowQualityResumeTriggered,
	i.PostHireFollowUpTriggered,
	i.FollowUpRequested,
	i.NotSearchingJobsTriggered,
	i.InappropriateEmailAddress,
	i.GivingPositiveFeedback,
	i.GivingNegativeFeedback,
	i.MigrantSeasonalFarmWorkerTriggered,
	u.LoggedInOn AS LastLoggedInOn,
	u.ExternalId,
	u.Blocked,
	u.[Enabled],
	u.UserName
FROM  dbo.[Data.Application.User]               AS u  WITH (NOLOCK) 
INNER JOIN dbo.[Data.Application.Person]        AS p  WITH (NOLOCK) ON u.PersonId = p.Id
INNER JOIN dbo.[Data.Application.Issues]        AS i  WITH (NOLOCK) ON p.Id = i.PersonId
INNER JOIN dbo.[Data.Application.PersonAddress] AS pa WITH (NOLOCK) ON p.id = pa.PersonId AND pa.IsPrimary = 1
WHERE u.UserType IN (4, 8, 12);

GO
