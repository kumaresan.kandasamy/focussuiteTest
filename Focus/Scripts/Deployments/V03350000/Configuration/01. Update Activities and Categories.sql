﻿--Jobseekers
--Agreements / Services formerly Assistance

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.001')
BEGIN
	DECLARE @ActivityCategory001Id as int
	
	SELECT @ActivityCategory001Id = Id
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.001'
		
	UPDATE [dbo].[Config.ActivityCategory]
	SET [Name] = 'Intake / Needs Assessment' WHERE [LocalisationKey] = 'ActivityCategory.001'
	
	UPDATE [dbo].[Config.Activity]
	SET [Name] = 'Career Assessment: Initial' WHERE [LocalisationKey] = 'Activity.001'
	
	UPDATE [dbo].[Config.Activity]
	SET [Name] = 'Initial Interview / Assessment' WHERE [LocalisationKey] = 'Activity.006'

	IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.287')
		BEGIN
			INSERT INTO [dbo].[Config.Activity]
					   ([LocalisationKey]
					   ,[Name]
					   ,[Visible]
					   ,[Administrable]
					   ,[SortOrder]
					   ,[Used]
					   ,[ExternalId]
					   ,[ActivityCategoryId])
				 VALUES
					   ('Activity.287', 'Eligibility Screening', 1, 1, 0, 0, 0, @ActivityCategory001Id),
					   ('Activity.288', 'REA Assessment', 1, 1, 0, 0, 0, @ActivityCategory001Id),
					   ('Activity.289', 'REA Assessment / Online / Self-Services', 1, 1, 0, 0, 0, @ActivityCategory001Id),
					   ('Activity.290', 'REA Enrolled', 1, 1, 0, 0, 0, @ActivityCategory001Id),
					   ('Activity.291', 'RES Assessment', 1, 1, 0, 0, 0, @ActivityCategory001Id),
					   ('Activity.292', 'RES Assessment / Online / Self-Services', 1, 1, 0, 0, 0, @ActivityCategory001Id),
					   ('Activity.293', 'RES Enrolled', 1, 1, 0, 0, 0, @ActivityCategory001Id),
					   ('Activity.294', 'Service Needs Evaluation', 1, 1, 0, 0, 0, @ActivityCategory001Id)
		END
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.001')
	WHERE [LocalisationKey] IN ('Activity.130','Activity.131')
	
END

IF NOT EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.022')
	BEGIN

		INSERT INTO [dbo].[Config.ActivityCategory]
				   ([LocalisationKey]
				   ,[Name]
				   ,[Visible]
				   ,[Administrable]
				   ,[ActivityType]
				   ,[Editable])
			VALUES
				   ('ActivityCategory.022', 'Contact / Information Provided', 1, 1, 0, 0)

		DECLARE @ActivityCategory022Id as int
		SELECT @ActivityCategory022Id = Id
				FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.022'

	IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.295')
		BEGIN
			INSERT INTO [dbo].[Config.Activity]
					   ([LocalisationKey]
					   ,[Name]
					   ,[Visible]
					   ,[Administrable]
					   ,[SortOrder]
					   ,[Used]
					   ,[ExternalId]
					   ,[ActivityCategoryId])
				 VALUES
					   ('Activity.295', 'E-Mail: Blast', 1, 1, 1, 0, 0, @ActivityCategory022Id),
					   ('Activity.296', 'E-Mail: Individual', 1, 1, 2, 0, 0, @ActivityCategory022Id),
					   ('Activity.297', 'Follow-up:  General', 1, 1, 3, 0, 0, @ActivityCategory022Id),
					   ('Activity.298', 'Letter', 1, 1, 4, 0, 0, @ActivityCategory022Id),
					   ('Activity.299', 'Phone Call / Left Message', 1, 1, 5, 0, 0, @ActivityCategory022Id),
					   ('Activity.300', 'Info: Assessment Services', 1, 1, 6, 0, 0, @ActivityCategory022Id),
					   ('Activity.728', 'Info: Bonding', 1, 1, 7, 0, 0, @ActivityCategory022Id),
					   ('Activity.301', 'Info: Career Counseling Services', 1, 1, 8, 0, 0, @ActivityCategory022Id),
					   ('Activity.302', 'Info: Demand Occupations', 1, 1, 9, 0, 0, @ActivityCategory022Id),
					   ('Activity.303', 'Info: Financial Aid', 1, 1, 10, 0, 0, @ActivityCategory022Id),
					   ('Activity.304', 'Info: Job Placement', 1, 1, 11, 0, 0, @ActivityCategory022Id),
					   ('Activity.305', 'Info: Job Search', 1, 1, 12, 0, 0, @ActivityCategory022Id),
					   ('Activity.306', 'Info: Job Skill Needs', 1, 1, 13, 0, 0, @ActivityCategory022Id),
					   ('Activity.307', 'Info: Labor Market Information', 1, 1, 14, 0, 0, @ActivityCategory022Id),
					   ('Activity.308', 'Info: Labor Market Information / Customized', 1, 1, 15, 0, 0, @ActivityCategory022Id),
					   ('Activity.309', 'Info: Local Area Performance', 1, 1, 16, 0, 0, @ActivityCategory022Id),
					   ('Activity.310', 'Info: Online Services & Activities', 1, 1, 17, 0, 0, @ActivityCategory022Id),
					   ('Activity.311', 'Info: Other Self-Services', 1, 1, 18, 0, 0, @ActivityCategory022Id),
					   ('Activity.312', 'Info: Supportive Services', 1, 1, 19, 0, 0, @ActivityCategory022Id),
					   ('Activity.313', 'Info: Training / Workshop Information', 1, 1, 19, 0, 0, @ActivityCategory022Id),
					   ('Activity.314', 'Info: Vocational Guidance', 1, 1, 20, 0, 0, @ActivityCategory022Id),
					   ('Activity.315', 'Info: Workforce Services', 1, 1, 21, 0, 0, @ActivityCategory022Id)
		END
	END			   


IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.002')
BEGIN

	DECLARE @ActivityCategory002Id as int
	
	SELECT @ActivityCategory002Id = Id
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.002'
		
	UPDATE [dbo].[Config.ActivityCategory]
	SET [Name] = 'Counseling / Case Management / Services (Non-Vets)' WHERE Id = @ActivityCategory002Id

	UPDATE [dbo].[Config.Activity]
	SET [Name] = 'Case Management: Assigned (Non-Vets)' WHERE [LocalisationKey] = 'Activity.007'
	
	UPDATE [dbo].[Config.Activity]
	SET [Name] = 'Case Management: Client-Centered' WHERE [LocalisationKey] = 'Activity.008'
	
	UPDATE [dbo].[Config.Activity]
	SET [Name] = 'Case Management: Closed' WHERE [LocalisationKey] = 'Activity.009'

	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Case Management: Follow-up (General)'
	WHERE [LocalisationKey] = 'Activity.034'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Case Management: Follow-up (Trade)'
	WHERE [LocalisationKey] = 'Activity.010'
	
	UPDATE [dbo].[Config.Activity]
	SET [Name] = 'Case Management: Other' WHERE [LocalisationKey] = 'Activity.011'
	
	UPDATE [dbo].[Config.Activity]
	SET [Name] = 'Case Management: Received Services (Non-Vets)' WHERE [LocalisationKey] = 'Activity.012'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Career Guidance'
	WHERE [LocalisationKey] = 'Activity.013'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Career / Labor Market Info'
	WHERE [LocalisationKey] = 'Activity.023'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Employability Skills'
	WHERE [LocalisationKey] = 'Activity.014'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Group Sessions'
	WHERE [LocalisationKey] = 'Activity.015'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Individual Employment Plan Developed'
	WHERE [LocalisationKey] = 'Activity.016'
			
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Individual & Career Planning'
	WHERE [LocalisationKey] = 'Activity.017'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Interest Inventory'
	WHERE [LocalisationKey] = 'Activity.018'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Job Coaching'
	WHERE [LocalisationKey] = 'Activity.019'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Job Finding Club'
	WHERE [LocalisationKey] = 'Activity.031'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Job Preparation / Interviewing Skills'
	WHERE [LocalisationKey] = 'Activity.020'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Job Search Planning'
	WHERE [LocalisationKey] = 'Activity.022'

	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Vocational Guidance - Other'
	WHERE [LocalisationKey] = 'Activity.024'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Vocational Rehab - Other'
	WHERE [LocalisationKey] = 'Activity.025'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Service: Other Reportable WF'
	WHERE [LocalisationKey] = 'Activity.036'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Service: Resume Preparation Assistance'
	WHERE [LocalisationKey] = 'Activity.039'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)	,
	[Name] = 'Counseling: Job Retention Services'
	WHERE [LocalisationKey] = 'Activity.021'
			
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory002Id)
	WHERE [LocalisationKey] = 'Activity.041'

	IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.316')
		BEGIN
			INSERT INTO [dbo].[Config.Activity]
					   ([LocalisationKey]
					   ,[Name]
					   ,[Visible]
					   ,[Administrable]
					   ,[SortOrder]
					   ,[Used]
					   ,[ExternalId]
					   ,[ActivityCategoryId])
				 VALUES
					   ('Activity.316', 'Case Management: Follow-up (Post-Case)', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.317', 'Counseling: Career Assessment / Comprehensive', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.318', 'Counseling: Career Assessment / Test Results Review', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.319', 'Counseling: Case Management Contact', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.320', 'Counseling: Continuing Employment Assessment', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.321', 'Counseling: Final Employment Assessment', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.322', 'Counseling: Job Finding Club / Claimant Re-employment', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.323', 'Counseling: Soft Skills Assessment', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.324', 'Counseling: Training Waiver Review', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.325', 'Service: ATAA / RTAA', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.326', 'Service: Basic Skills or Literacy', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.327', 'Service: Disaster Relief Employment', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.328', 'Service: Incumbant Worker Service', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.329', 'Service: Internship', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.330', 'Service: Job Post Review', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.331', 'Service: Needs-Based Payment', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.332', 'Service: Other Intensive', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.658', 'Service: Other Reportable Veteran', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.333', 'Service: Other Reportable WF Follow-up', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.334', 'Service: Planned Gap in Service', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.335', 'Service: Pre-Layoff Assistance', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.336', 'Service: Pre-Vocational Assistance', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.337', 'Service: Re-employment Trade Adjustment Assistance', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.338', 'Service: Relocation Assistance', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.339', 'Service: Supported Employment', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.340', 'Service: WOTC', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.341', 'Service: Work Experience', 1, 1, 0, 0, 0, @ActivityCategory002Id),
					   ('Activity.342', 'Service: Work-Search Review', 1, 1, 0, 0, 0, @ActivityCategory002Id)			   
		END		   
	END			   

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.012')
BEGIN

	DECLARE @ActivityCategory012Id as int
	
	SELECT @ActivityCategory012Id = Id
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.012'
		
	UPDATE [dbo].[Config.ActivityCategory]
	SET [Name] = 'Counseling / Case Management / Services (Vets)' WHERE Id = @ActivityCategory012Id

	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Case Management: Assigned (Veterans)'
	WHERE [LocalisationKey] = 'Activity.128'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Case Management: Assigned (Veterans)'
	WHERE [LocalisationKey] = 'Activity.128'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Case Management: Received Services (Veterans)'
	WHERE [LocalisationKey] = 'Activity.134'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Counseling: Vocational Guidance (Vets)'
	WHERE [LocalisationKey] = 'Activity.136'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Counseling: Vocational Rehab-VA'
	WHERE [LocalisationKey] = 'Activity.137'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Service: Gold Card Outreach'
	WHERE [LocalisationKey] = 'Activity.129'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Service: Incarcerated Veteran Outreach'
	WHERE [LocalisationKey] = 'Activity.132'

	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Service: Other Reportable Veteran Follow-up'
	WHERE [LocalisationKey] = 'Activity.133'
	
	UPDATE [dbo].[Config.Activity]
	SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory012Id)	,
	[Name] = 'Workshop: TAP'
	WHERE [LocalisationKey] = 'Activity.135'

	IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.343')
		BEGIN
		INSERT INTO [dbo].[Config.Activity]
				   ([LocalisationKey]
				   ,[Name]
				   ,[Visible]
				   ,[Administrable]
				   ,[SortOrder]
				   ,[Used]
				   ,[ExternalId]
				   ,[ActivityCategoryId])
			 VALUES
				   ('Activity.343', 'Info: Veterans Priority of Service', 1, 1, 1, 0, 0, @ActivityCategory012Id),
				   ('Activity.344', 'Case Management: Closed (Veterans)', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.345', 'Case Management: Follow-up (General)', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.346', 'Case Management: Follow-up (Post-Case-Vets)', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.347', 'Case Management: Other', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.348', 'Counseling: Career Assessment - Comprehensive', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.349', 'Counseling: Career Assessment - Test Results Review', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.350', 'Counseling: Career Guidance', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.351', 'Counseling: Career / Labor Market Info', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.352', 'Counseling: Case Management Contact (Veterans)', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.353', 'Counseling: Continuing Employment Assessment', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.354', 'Counseling: Employability Skills', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.355', 'Counseling: Final Employment Assessment', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.356', 'Counseling: Group Sessions', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.357', 'Counseling: Individual Employment Plan Developed', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.358', 'Counseling: Individual Employment Plan Review', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.359', 'Counseling: Individual & Career Planning', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.360', 'Counseling: Interest Inventory', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.361', 'Counseling: Job Coaching', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.362', 'Counseling: Job Finding Club', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.363', 'Counseling: Job Finding Club - Claimant Re-employed', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.364', 'Counseling: Job Preparation - Interviewing Skills', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.365', 'Counseling: Job Retention Services', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.366', 'Counseling: Job Search Planning', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.367', 'Counseling: Soft Skills Assessment', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.368', 'Service: Basic Skills or Literacy Service', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.369', 'Service: Disaster Relief Employment', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.370', 'Service: Incumbant Worker Service', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.371', 'Service: Internship', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.372', 'Service: Job Post Review', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.373', 'Service: Needs-Based Payment', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.374', 'Service: Other Intensive', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.375', 'Service: Other Reportable Veteran', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.376', 'Service: Planned Gap in Service', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.377', 'Service: Pre-Layoff Assistance', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.378', 'Service: Pre-Vocational Assistance', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.379', 'Service: Re-employment Trade Adjustment Assistance', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.380', 'Service: Relocation Assistance', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.381', 'Service: Resume Preparation Assistance', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.382', 'Service: Supported Employment', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.383', 'Service: WOTC', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.384', 'Service: Work Experience', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.385', 'Service: Work-Search Review', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.386', 'Workforce Info Services-Staff-Assisted LMI', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.387', 'Workshop: Other (DVOP-provided)', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.388', 'Workshop: TAP (Not Used)', 0, 0, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.389', 'Workshop: TAP Attended (DVOP-provided)', 1, 1, 0, 0, 0, @ActivityCategory012Id),
				   ('Activity.390', 'Workshop: TAP Attended (Off-Base)', 1, 1, 0, 0, 0, @ActivityCategory012Id)			   
		END
	END

--Events Category

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.005')
	BEGIN

		DECLARE @ActivityCategory005Id as int
		
		SELECT @ActivityCategory005Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.005'
			
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.391')
			BEGIN
			
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
						   ('Activity.391', 'JobFair Screening', 1, 1, 0, 0, 0, @ActivityCategory005Id)
			END
	END

--Job Search (formerly Labor Exchange)

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.006')
	BEGIN

		DECLARE @ActivityCategory006Id as int
		
		SELECT @ActivityCategory006Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.006'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Job Search' WHERE Id = @ActivityCategory006Id

		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory006Id)	,
		[Name] = 'Refused Referral (Non-Talent Feed)'
		WHERE [LocalisationKey] = 'Activity.038'
				
		IF EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.270')
		BEGIN
			DELETE FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.270'
		END
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.392')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
						   ('Activity.392', 'Assisted Job Search', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.393', 'Assisted Job Search - Out of Area', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.395', 'Job Development Contact (DVOP)', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.396', 'Job Development Contact (LVER)', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.398', 'Placed: Federal Contractor Job (Non-Talent Feed)', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.399', 'Placed: Federal Job (Non-Talent Feed)', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.400', 'Referred: Federal Contractor Job (Non-Talent Feed)', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.401', 'Referred: Federal Job (Non-Talent Feed)', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.402', 'Referred: Job Bank - Portal', 1, 1, 0, 0, 0, @ActivityCategory006Id),
						   ('Activity.403', 'Referred: Registered Apprenticeship', 1, 1, 0, 0, 0, @ActivityCategory006Id)
			END
	END

--Orientations & Workshops (non-UI) formerly separate categories Orientations and Workshops

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.007')
	BEGIN

		DECLARE @ActivityCategory007Id as int
		
		SELECT @ActivityCategory007Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.007'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Orientations & Workshops (Non-UI)' WHERE Id = @ActivityCategory007Id

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Orientation: Informational Services'
		WHERE [LocalisationKey] = 'Activity.042'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Orientation: Other'
		WHERE [LocalisationKey] = 'Activity.043'

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Orientation: Rapid Response'
		WHERE [LocalisationKey] = 'Activity.044'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory007Id)	,
		[Name] = 'Workshop: Job Search'
		WHERE [LocalisationKey] = 'Activity.138'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.405')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
						   ('Activity.405', 'Workshop: Attended', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.406', 'Workshop: Attended - Online', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.407', 'Workshop: Attended - Other', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.408', 'Workshop: Referred', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.409', 'Workshop: Referred - Online', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.410', 'Workshop: Referred - Other', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.411', 'Workshop: Basic Computer Skills', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.412', 'Workshop: Career Exploration', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.413', 'Workshop: Financial / Stress Management', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.414', 'Workshop: Interview Skills', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.415', 'Workshop: Keytrain', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.416', 'Workshop: Networking', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.417', 'Workshop: Occupational Skills', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.418', 'Workshop: Resume Writing', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.419', 'Workshop: Salary Negotiations', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.420', 'Workshop: Social Media', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.421', 'Workshop: Soft Skills', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.729', 'Orientation: Ex-Offender', 1, 1, 0, 0, 0, @ActivityCategory007Id),
						   ('Activity.660', 'Orientation: Workforce Center Services', 1, 1, 0, 0, 0, @ActivityCategory007Id)
			END
	END			 

--Program / Supported Service Referrals formerly Referrals

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.008')
	BEGIN

		DECLARE @ActivityCategory008Id as int
		
		SELECT @ActivityCategory008Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.008'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Program / Supported Service Referrals' WHERE Id = @ActivityCategory008Id

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Homeless Program - Shelter'
		WHERE [LocalisationKey] = 'Activity.060'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Housing: Regular Assistance'
		WHERE [LocalisationKey] = 'Activity.061'

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'TANF-Temporary Assistance for Needy Families'
		WHERE [LocalisationKey] = 'Activity.077'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Services for Visually Impaired'
		WHERE [LocalisationKey] = 'Activity.073'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Services for Vocational Rehabilitation'
		WHERE [LocalisationKey] = 'Activity.074'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Veterans Service Program/Other Organization'
		WHERE [LocalisationKey] = 'Activity.082'
			
		IF EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.050')
		BEGIN
			DELETE FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.050'
		END
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.422')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.422','Bonding Assistance',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.423','Bridges to Opportunities: Interest',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.424','Bridges to Opportunities: Referral',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.425','Corrections',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.426','Food Share - Food Pantry Program',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.427','Housing & Urban Development',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.428','Other Non-WIA Program',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.429','Services for Job - Workplace Accommodation',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.430','SNAP-Supplemental Nutritional Assistance Program',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.431','Supportive Service-General',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.432','Supportive Service-Other',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.433','Supportive Service -Staff Arranged',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.434','Veterans Program',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.435','Wagner-Peyser Program',1,1,0,0,0,@ActivityCategory008Id),
							('Activity.436','Welfare-to-Work',1,1,0,0,0,@ActivityCategory008Id)
			END  	   
	END

--Testing - Assessments formerly Testing

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.009')
	BEGIN

		DECLARE @ActivityCategory009Id as int
		
		SELECT @ActivityCategory009Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.009'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Testing / Assessments' WHERE Id = @ActivityCategory009Id

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Literacy Skills'
		WHERE [LocalisationKey] = 'Activity.087'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'NCRC / Work Keys Referral'
		WHERE [LocalisationKey] = 'Activity.096'

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Skills Proficiency'
		WHERE [LocalisationKey] = 'Activity.091'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'TABE-WF: Assessment'
		WHERE [LocalisationKey] = 'Activity.093'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'TABE-WF: Testing'
		WHERE [LocalisationKey] = 'Activity.094'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.438')
			BEGIN							
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.438','Ability Profiling',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.439','Career Aptitude Assessment',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.440','Career Interest Assessment',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.659','GATB Interpretation',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.441','GATB / VG Interpretation',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.442','Interest Inventory/Testing',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.443','Interest Interpretation',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.444','JOBehavior Trucking Assessment',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.445','Literacy Interpretation',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.446','NCRC: National Career Readiness Certificate',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.447','NCRC / Work Keys Assessment: Reading for Info',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.448','NCRC / Work Keys Assessment: Locating Info',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.449','NCRC / Work Keys Assessment: Applied Technology',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.450','NCRC / Work Keys Assessment: Listening',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.451','NCRC / Work Keys Assessment: Readiness Indicators',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.452','NCRC / Work Keys Assessment: Talent',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.453','NCRC / Work Keys Assessment: Teamwork',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.454','NCRC / Work Keys Assessment: Workplace Observation',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.455','Other / Merit Interpretation',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.456','SATB Interpretation',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.457','Soft Skills Assessment',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.458','Typing / Clerical / Keyboarding ',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.459','Typing  Interpretation',1,1,0,0,0,@ActivityCategory009Id),
							('Activity.661','NCRC / Work Keys Assessment: Applied Math',1,1,0,0,0,@ActivityCategory009Id)
			END
	END		 

--Training Enrollments & Referrals formerly Training

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.010')
	BEGIN

		DECLARE @ActivityCategory010Id as int
		
		SELECT @ActivityCategory010Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.010'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Training Enrollments & Referrals' WHERE Id = @ActivityCategory010Id

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'OJT: Contract'
		WHERE [LocalisationKey] = 'Activity.100'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id)
		WHERE [LocalisationKey] = 'Activity.037'
					
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Enrolled: Job Corps'
		WHERE [LocalisationKey] = 'Activity.106'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Enrolled: Other'
		WHERE [LocalisationKey] = 'Activity.103'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Enrolled: Other Federal (Excluding  OJT-DAV)'
		WHERE [LocalisationKey] = 'Activity.107'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Enrolled: Post-secondary'
		WHERE [LocalisationKey] = 'Activity.104'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Enrolled: Secondary'
		WHERE [LocalisationKey] = 'Activity.105'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Enrolled: WIA Services'
		WHERE [LocalisationKey] = 'Activity.108'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Adult Education'
		WHERE [LocalisationKey] = 'Activity.046'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Referred: Basic Skills'
		WHERE [LocalisationKey] = 'Activity.097'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Community / Technical College'
		WHERE [LocalisationKey] = 'Activity.048'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Referred: Computer Skills'
		WHERE [LocalisationKey] = 'Activity.098'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Referred: External Provider',
		[ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id)
		WHERE [LocalisationKey] = 'Activity.248'
			
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Health Care Tax Credit (HCTC)'
		WHERE [LocalisationKey] = 'Activity.057'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Job Corps'
		WHERE [LocalisationKey] = 'Activity.063'

		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: MSFW (Federal)'
		WHERE [LocalisationKey] = 'Activity.064'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Native American'
		WHERE [LocalisationKey] = 'Activity.066'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Older Worker'
		WHERE [LocalisationKey] = 'Activity.067'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Postsecondary Education'
		WHERE [LocalisationKey] = 'Activity.068'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Trade Services'
		WHERE [LocalisationKey] = 'Activity.078'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: Trade- A/RTAA'
		WHERE [LocalisationKey] = 'Activity.079'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Referred: WIA Services'
		WHERE [LocalisationKey] = 'Activity.081'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory010Id),
		[Name] = 'Enrolled: Entrepreneurial'
		WHERE [LocalisationKey] = 'Activity.099'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.494')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.494','Referred: Entrepreneurial',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.495','Referred: Internal',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.496','Referred: MSFW(State)',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.497','Other Federal (Excluding OJT-DAV)',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.498','Benchmark Training Review',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.499','OJT: Department of Veterans Affairs',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.500','Enrolled: Adult Education',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.501','Enrolled: Apprenticeship ',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.502','Enrolled: Basic Skills ',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.503','Enrolled: Community / Technical College',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.504','Enrolled: Computer Skills',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.505','Enrolled: Customized Training',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.506','Enrolled: English as Second Language',1,1,0,0,0,@ActivityCategory010Id),
							--('Activity.507','Enrolled: Entrepreneurial',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.508','Enrolled: External Provider',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.509','Enrolled: GED Training (standalone)',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.510','Enrolled: Internal',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.511','Enrolled: Job Readiness ',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.512','Enrolled: MSFW (Federal)',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.513','Enrolled: MSFW (State)',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.514','Enrolled: Native American',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.515','Enrolled: Occupational Classroom',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.516','Enrolled: Older Worker',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.517','Enrolled: On-the-Job Training',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.518','Enrolled: Prerequisite Education',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.519','Enrolled: Private-Sector Training',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.520','Enrolled: Skills Upgrade Retraining',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.521','Enrolled: Trade Services',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.522','Enrolled: Trade- A/RTAA',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.523','Enrolled: Trade Adjustment Assistance ',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.524','Enrolled: TAA-NAFTA',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.525','Enrolled: TAA-Remedial Education',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.526','Enrolled: TAA-Subsistence',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.527','Enrolled: Training',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.528','Enrolled: Veterans Federal',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.529','Enrolled: WIA Other Program',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.530','Enrolled: Workplace Skills & Related Instruction',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.531','Enrolled: Workplace Training Instruction',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.532','Referred: Apprenticeships',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.533','Referred: Customized Training',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.534','Referred: English as Second Language',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.535','Referred: GED Training (standalone)',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.536','Referred: Job Readiness ',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.537','Referred: Non-WIA Partner Program',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.538','Referred: Occupational Classroom',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.539','Referred: On-the-Job Training',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.540','Referred: Prerequisite Education',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.541','Referred: Private-Sector Training',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.542','Referred: Secondary Education',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.543','Referred: Skills Upgrade Retraining',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.544','Referred: Trade Adjustment Assistance ',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.545','Referred: TAA-NAFTA',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.546','Referred: TAA-Remedial Education',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.547','Referred: TAA-Subsistence',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.548','Referred: Training / Workshop',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.549','Referred: Training / Workshop - Online',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.550','Referred: Veterans Federal',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.551','Referred: WIA Other Program',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.552','Referred: Workplace Skills & Related Instruction',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.553','Referred: Workplace Training Instruction',1,1,0,0,0,@ActivityCategory010Id),
							('Activity.662','Referred: Other Federal(Excluding OJT-DAV)',1,1,0,0,0,@ActivityCategory010Id)
			END
	END		 

--UI/Re-employment

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.011')
	BEGIN

		DECLARE @ActivityCategory011Id as int
		
		SELECT @ActivityCategory011Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.011'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'UI / Re-employment' WHERE Id = @ActivityCategory011Id
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Basic UI: Information Provided'
		WHERE [LocalisationKey] = 'Activity.120'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'ERP: Eligiblilty Review Program'
		WHERE [LocalisationKey] = 'Activity.115'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'IRP: Failed to Report'
		WHERE [LocalisationKey] = 'Activity.117'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Profiling: Case Management'
		WHERE [LocalisationKey] = 'Activity.114'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Profiling: Exempted Mandatory'
		WHERE [LocalisationKey] = 'Activity.116'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Profiling: Failed to Report'
		WHERE [LocalisationKey] = 'Activity.118'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Profiling: Orientation / UI'
		WHERE [LocalisationKey] = 'Activity.121'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'REA: Failed to Report'
		WHERE [LocalisationKey] = 'Activity.119'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'REA: Labor Market & Career Information'
		WHERE [LocalisationKey] = 'Activity.126'	
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'REA: Orientation'
		WHERE [LocalisationKey] = 'Activity.122'	
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'REA: Requirements Fulfilled'
		WHERE [LocalisationKey] = 'Activity.127'	
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'REA / EUC: Job Search Eligible'
		WHERE [LocalisationKey] = 'Activity.124'	
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'REA: Labor Market & Career Information'
		WHERE [LocalisationKey] = 'Activity.126'	
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'REA / EUC: Regular'
		WHERE [LocalisationKey] = 'Activity.125'	
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'RES: Orientation'
		WHERE [LocalisationKey] = 'Activity.123'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Benefit Rights Interview'
		WHERE [LocalisationKey] = 'Activity.112'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.554')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.554','Basic UI: Orientation',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.555','Basic UI: Workshop',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.558','ERP: Failed to Report (other UI claimant)',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.559','ERP: Failed',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.560','ERP: Passed',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.561','IRP: Individual Re-employment Plan Developed',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.562','IRP: Individual Re-employment Plan Review ',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.563','Profiling: Information Provided',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.564','Profiling: Orientation /REA',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.565','Profiling: Orientation/REU',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.566','REA: Orientation / Online / Self-Services',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.567','REA / EUC: Failed to Report',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.568','REA / EUC: Requirements Fulfilled',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.569','RES: Failed to Report',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.570','RES: Orientation / Online/Self-Services',1,1,0,0,0,@ActivityCategory011Id),
							('Activity.571','RES / EUC:  Failed to Report',1,1,0,0,0,@ActivityCategory011Id)
			END
	END

--Youth Services

IF NOT EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.023')
	BEGIN

		INSERT INTO [dbo].[Config.ActivityCategory]
					   ([LocalisationKey]
					   ,[Name]
					   ,[Visible]
					   ,[Administrable]
					   ,[ActivityType]
					   ,[Editable])
				VALUES
					   ('ActivityCategory.023', 'Youth Services', 1, 1, 0, 0)

		DECLARE @ActivityCategory023Id as int
		
		SELECT @ActivityCategory023Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.023'
				
		INSERT INTO [dbo].[Config.Activity]
				   ([LocalisationKey]
				   ,[Name]
				   ,[Visible]
				   ,[Administrable]
				   ,[SortOrder]
				   ,[Used]
				   ,[ExternalId]
				   ,[ActivityCategoryId])
			 VALUES
					('Activity.572','Enrolled: Youth Services',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.573','Referred: Youth Services',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.574','Design Framework: Assessment',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.575','Design Framework: Case Management',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.576','Design Framework: Individual Service Strategy',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.577','Service: Adult Basic Education',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.578','Service: Adult Mentoring',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.579','Service: Alternative Secondary School',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.580','Service: Apprenticeship',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.581','Service: Career Development',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.582','Service: Comprehensive Guidance / Counseling',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.583','Service: Dropout Prevention Strategies',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.584','Service: Instruction Leading to Secondary School Completion',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.585','Service: Internships',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.586','Service: Job Search',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.587','Service: Job Shadowing',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.588','Service: Needs-Related Payments / Stipends',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.589','Service: Paid Work Experience',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.590','Service: Planned Gap in Service',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.591','Service: Summer Youth Employment Opportunities',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.592','Service: Supportive Services',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.593','Service: Unpaid Work Experience',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.594','Service: Unsubsidized Employment',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.595','Training: Financial Literacy / Budgeting',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.596','Training: Tutoring',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.597','Training: Leadership Development',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.598','Training: Occupation Skills',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.599','Training: On-the-Job',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.600','Training: Parental Skills',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.601','Training: Study Skills',1,1,0,0,0,@ActivityCategory023Id),
					('Activity.602','Training: Work Readiness',1,1,0,0,0,@ActivityCategory023Id)
	END

--Employers
--Agreements / Services formerly Assistance

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.014')
	BEGIN

		DECLARE @ActivityCategory014Id as int
		
		SELECT @ActivityCategory014Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.014'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Agreements / Services' WHERE Id = @ActivityCategory014Id

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Agreement: Business Partnership'
		WHERE [LocalisationKey] = 'Activity.144'
							
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Service: Human Resource Issues'
		WHERE [LocalisationKey] = 'Activity.142'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory014Id)	,
		[Name] = 'Service: H2A Field Check'
		WHERE [LocalisationKey] = 'Activity.157'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Service: Layoff-Response Planning'
		WHERE [LocalisationKey] = 'Activity.147'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Service: Link to Company Home Page'
		WHERE [LocalisationKey] = 'Activity.148'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Service: Marketing Assistance'
		WHERE [LocalisationKey] = 'Activity.149'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Service: Rapid Response / Business Downsizing'
		WHERE [LocalisationKey] = 'Activity.150'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Service: Spider Company Home Page '
		WHERE [LocalisationKey] = 'Activity.151'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Service: Statutory (legal) Issues'
		WHERE [LocalisationKey] = 'Activity.152'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory014Id),
		[Name] = 'Service: Strategic Planning / Economic Development'
		WHERE [LocalisationKey] = 'Activity.153'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.603')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.603','Agreement: Exclusive Hiring',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.604','Agreement: OJT',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.605','Agreement: Other',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.606','Agreement:  Service Plan Created',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.607','Agreement:  Service Plan Updated',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.608','Agreement: Trial Job',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.609','Agreement: Work Experience',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.610','Service: Incumbant Worker Services',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.611','Service: Job Fair Assistance / Preparation',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.612','Service: Not Otherwise Described',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.613','Service: OFCCP Consultation',1,1,0,0,0,@ActivityCategory014Id),
							('Activity.614','Service: Workforce Needs Assessment',1,1,0,0,0,@ActivityCategory014Id)
			END
	END	
	
--Contact / Info Provided formerly Contact
IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.016')
	BEGIN

		DECLARE @ActivityCategory016Id as int
		
		SELECT @ActivityCategory016Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.016'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Contact / Information Provided' WHERE Id = @ActivityCategory016Id

		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'E-Mail: Blast'
		WHERE [LocalisationKey] = 'Activity.158'
							
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Follow-Up: General'
		WHERE [LocalisationKey] = 'Activity.159'
				
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Letter / Mailing'
		WHERE [LocalisationKey] = 'Activity.160'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Phone Call / Left Message'
		WHERE [LocalisationKey] = 'Activity.162'
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Visit Employer Location / On-Site'
		WHERE [LocalisationKey] = 'Activity.161'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory016Id),
		[Name] = 'Info: Business / Support Services (Central staff)'
		WHERE [LocalisationKey] = 'Activity.143'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory016Id),
		[Name] = 'Info: Economic / Labor Market Issues'
		WHERE [LocalisationKey] = 'Activity.145'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory016Id),
		[Name] = 'Info: Foreign Labor Certification'
		WHERE [LocalisationKey] = 'Activity.156'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory016Id),
		[Name] = 'Info: Unemployment Insurance'
		WHERE [LocalisationKey] = 'Activity.154'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory016Id),
		[Name] = 'Info: Untapped Labor-Pool Activities'
		WHERE [LocalisationKey] = 'Activity.155'
		
		UPDATE [dbo].[Config.Activity]
		SET [ActivityCategoryId] = (SELECT Id FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory016Id),
		[Name] = 'Info: Veteran Services Outreach'
		WHERE [LocalisationKey] = 'Activity.177'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.615')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.615','E-Mail: Individual',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.616','Employer Office Tour',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.617','Employer Office Visit',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.618','Program / Services Discussion',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.619','Info: Bonding Assistance',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.620','Info: Business / Support Services (Local staff)',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.621','Info: Demand Occupations',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.622','Info: Employer Advisory Committee',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.623','Info: Equal Opportunity Employment ',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.624','Info: Federal Contractor',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.625','Info: Initial Outreach',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.626','Info: Job Fair & Other Events',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.627','Info: Job Skill Needs',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.628','Info: Labor Market Information / Customized',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.629','Info: Labor Market Information / Preview',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.630','Info: Local Area Performance',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.631','Info: NCRC / Work Keys Program',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.632','Info: NCRC / Work Keys Profiling',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.633','Info: Online Services / Activities',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.634','Info: Other Self-Services',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.635','Info: Program Costs / Performance',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.636','Info: Rapid Response',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.637','Info: Registration / Job Order',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.638','Info: Tax Credits',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.639','Info: Testing Services',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.640','Info: Training / Workshops',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.641','Info: USERRA',1,1,0,0,0,@ActivityCategory016Id),
							('Activity.642','Info: Workforce Services',1,1,0,0,0,@ActivityCategory016Id)
			END
	END

--Events
IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.017')
	BEGIN

		DECLARE @ActivityCategory017Id as int
		
		SELECT @ActivityCategory017Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.017'
			
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Job Fair Participation'
		WHERE [LocalisationKey] = 'Activity.166'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.643')		
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.643','On-Site Interviewing',1,1,0,0,0,@ActivityCategory017Id)
			END
	END

--Job & Applicant Management formerly Labor Exchange
IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.018')
	BEGIN

		DECLARE @ActivityCategory018Id as int
		
		SELECT @ActivityCategory018Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.018'
			
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Job & Applicant Management'
		WHERE id = @ActivityCategory018Id
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Job-Order Taking:  Regular'
		WHERE [LocalisationKey] = 'Activity.172'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.644')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.644','Job-Order Taking:  Apprenticeship',1,1,0,0,0,@ActivityCategory018Id),
							('Activity.645','Job-Order Follow-up',1,1,0,0,0,@ActivityCategory018Id),
							('Activity.646','Job-Seeker Eligibility Screening',1,1,0,0,0,@ActivityCategory018Id),
							('Activity.647','Job-Seeker Pre-Screening',1,1,0,0,0,@ActivityCategory018Id),
							('Activity.648','Job-Seeker Recruitment',1,1,0,0,0,@ActivityCategory018Id),
							('Activity.663','Job Development: Regular',1,1,0,0,0,@ActivityCategory018Id),
							('Activity.664','Job Development: Targeted',1,1,0,0,0,@ActivityCategory018Id)
			END
	END
	
--Tax Credit
IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.019')
	BEGIN

		DECLARE @ActivityCategory019Id as int
		
		SELECT @ActivityCategory019Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.019'
	
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.649')
			BEGIN	
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.649','Other ',1,1,0,0,0,@ActivityCategory019Id)
			END
	END

--Testing
IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.020')
	BEGIN

		DECLARE @ActivityCategory020Id as int
		
		SELECT @ActivityCategory020Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.020'
			
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Typing / Clerical / Keyboarding'
		WHERE [LocalisationKey] = 'Activity.190'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.650')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.650','NCRC / Work Keys: Profiling Completed',1,1,0,0,0,@ActivityCategory020Id),
							('Activity.651','NCRC / Work Keys: Profiling Requested',1,1,0,0,0,@ActivityCategory020Id),
							('Activity.652','NCRC / Work Keys: Test Applicants',1,1,0,0,0,@ActivityCategory020Id),
							('Activity.653','NCRC / Work Keys: Test Incumbants',1,1,0,0,0,@ActivityCategory020Id)
			END
	END

--Workshop Seminars
IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.021')
	BEGIN

		DECLARE @ActivityCategory021Id as int
		
		SELECT @ActivityCategory021Id = Id
			FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.021'
		
		UPDATE [dbo].[Config.ActivityCategory]
		SET [Name] = 'Workshop / Seminars'
		WHERE id = @ActivityCategory021Id	
		
		UPDATE [dbo].[Config.Activity]
		SET [Name] = 'Workforce Services'
		WHERE [LocalisationKey] = 'Activity.196'
		
		IF NOT EXISTS (SELECT * FROM [dbo].[Config.Activity] WHERE [LocalisationKey] = 'Activity.654')
			BEGIN
				INSERT INTO [dbo].[Config.Activity]
						   ([LocalisationKey]
						   ,[Name]
						   ,[Visible]
						   ,[Administrable]
						   ,[SortOrder]
						   ,[Used]
						   ,[ExternalId]
						   ,[ActivityCategoryId])
					 VALUES
							('Activity.654','OJT Contracting Assistance',1,1,0,0,0,@ActivityCategory021Id),
							('Activity.655','Tax Credit Assistance',1,1,0,0,0,@ActivityCategory021Id),
							('Activity.656','Unemployment Insurance Services',1,1,0,0,0,@ActivityCategory021Id),
							('Activity.657','Workshop / Seminar Attended',1,1,0,0,0,@ActivityCategory021Id)
			END
	END

--Delete redundant Alien Labor Category

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.015')
BEGIN
		DECLARE @ActivityCategory015Id as int
	
		SELECT @ActivityCategory015Id = Id
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'ActivityCategory.015'
		
		DELETE FROM [dbo].[Config.Activity] WHERE ActivityCategoryId = @ActivityCategory015Id
		DELETE FROM [dbo].[Config.ActivityCategory] WHERE Id = @ActivityCategory015Id

END

IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.003')
BEGIN
	UPDATE [dbo].[Config.ActivityCategory] SET Visible = 0 WHERE [LocalisationKey] = 'ActivityCategory.003'
END
IF EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'ActivityCategory.013')
BEGIN
	UPDATE [dbo].[Config.ActivityCategory] SET Visible = 0 WHERE [LocalisationKey] = 'ActivityCategory.013'
END
