IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobLocation' AND COLUMN_NAME = 'County')
BEGIN
	ALTER TABLE
		[Data.Application.JobLocation]
	ADD
		County NVARCHAR(127) NULL
END
GO

DECLARE @Location TABLE
(
	Id BIGINT,
	Location NVARCHAR(1000),
	City NVARCHAR(127),	
	StateCode NVARCHAR(25),
	[State] NVARCHAR(127),
	Zip NVARCHAR(10),
	CommaPos INT,
	LastCommaPos INT,
	OpenBracketPos INT,
	CloseBracketPos INT
)
INSERT INTO @Location (Id, Location, CommaPos, LastCommaPos, OpenBracketPos, CloseBracketPos)
SELECT 
	JL.Id,
	JL.Location,
	CHARINDEX(',', JL.Location),
	LEN(JL.Location) - CHARINDEX(',', REVERSE(JL.Location)) + 1,
	LEN(JL.Location) - CHARINDEX('(', REVERSE(JL.Location)) + 1,
	LEN(JL.Location) - CHARINDEX(')', REVERSE(JL.Location)) + 1
FROM 
	[Data.Application.JobLocation] JL
WHERE 
	JL.Location LIKE '%(%)'
	AND JL.AddressLine1 IS NULL
	AND JL.Location NOT LIKE '<a href=%'

UPDATE 
	TL
SET
	StateCode = CASE TL.CommaPos
		WHEN 0 THEN LTRIM(RTRIM(SUBSTRING(TL.Location, 1, TL.OpenBracketPos - 1)))
		ELSE LTRIM(RTRIM(SUBSTRING(TL.Location, TL.LastCommaPos + 1, TL.OpenBracketPos - TL.LastCommaPos - 1)))
	END
FROM 
	@Location TL

UPDATE
	JL
SET
	City = CASE TL.CommaPos
		WHEN 0 THEN NULL 
		ELSE SUBSTRING(TL.Location, 1, TL.CommaPos - 1) 
	END, 
	[State] = LU.Value, 
	Zip = SUBSTRING(TL.Location, TL.OpenBracketPos + 1, TL.CloseBracketPos - TL.OpenBracketPos - 1)
FROM 
	[Data.Application.JobLocation] JL
INNER JOIN @Location TL
	ON TL.Id = JL.Id
INNER JOIN [Config.LookupItemsView] LU
	ON LU.LookupType = N'States'
	AND RIGHT(LU.[Key], 2) = TL.StateCode

UPDATE
	JL
SET
	City = CASE TL.CommaPos
		WHEN 0 THEN NULL 
		ELSE SUBSTRING(TL.Location, 1, TL.CommaPos - 1) 
	END, 
	[State] = LU.Value, 
	Zip = SUBSTRING(TL.Location, TL.OpenBracketPos + 1, TL.CloseBracketPos - TL.OpenBracketPos - 1)
FROM 
	[Data.Application.JobLocation] JL
INNER JOIN @Location TL
	ON TL.Id = JL.Id
INNER JOIN [Config.LookupItemsView] LU
	ON LU.LookupType = N'States'
	AND LU.Value = TL.StateCode