﻿
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.EmployerAccountActivityView]'))
DROP VIEW [dbo].[Data.Application.EmployerAccountActivityView]
GO


CREATE VIEW [dbo].[Data.Application.EmployerAccountActivityView] 
AS
SELECT
	ae.Id AS Id,
	bu.Name AS BusinessUnitName,
	p.FirstName + ' ' + p.LastName AS EmployeeName,
	p.EmailAddress AS EmployeeEmail,
	e.ApprovalStatus AS EmployeeApprovalStatus,
	at.Name AS ActivityType,
	ae.CreatedOn AS ActivityCreatedOn
FROM  [Data.Core.ActionType] at WITH (NOLOCK) 
	INNER JOIN [Data.Core.ActionEvent] ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId 
	INNER JOIN [Data.Application.User] u WITH (NOLOCK) ON ae.EntityId = u.Id 
	INNER JOIN [Data.Application.Person] p WITH (NOLOCK) ON u.PersonId = p.Id 
	INNER JOIN [Data.Application.Employee] e WITH (NOLOCK) ON p.Id = e.PersonId
	INNER JOIN [Data.Application.EmployeeBusinessUnit] ebu WITH (NOLOCK) ON ebu.EmployeeId = e.Id
	INNER JOIN [Data.Application.BusinessUnit] bu WITH (NOLOCK) ON ebu.BusinessUnitId = bu.Id
WHERE u.UserType = 2 AND
			ebu.[Default] = 1 AND
		at.Name IN ('LogIn', 'RegisterAccount', 'SearchCandidates')
		
GO