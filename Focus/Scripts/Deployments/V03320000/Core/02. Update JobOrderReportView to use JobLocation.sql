IF EXISTS(select * FROM sys.views where name = 'Data.Application.JobOrderReportView')
BEGIN
	DROP VIEW [dbo].[Data.Application.JobOrderReportView]
END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.JobOrderReportView]
AS
WITH JL AS (
  SELECT
    County,
	State,
	Zip,
	JobId,
    rnk = ROW_NUMBER() OVER (PARTITION BY JobId ORDER BY ID)
  FROM [Data.Application.JobLocation]
)
	SELECT 
		J.Id,
		J.BusinessUnitId,
		J.OnetId,
		J.JobStatus,
		JL.County,
		JL.State,
		JL.Zip,
		J.JobTitle,
		J.MinimumEducationLevel,
		J.PostingFlags,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ForeignLabourCertificationH2A,
		J.ForeignLabourCertificationH2B,
		J.ForeignLabourCertificationOther,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		J.ScreeningPreferences,
		J.SalaryFrequencyId,
		J.MinSalary,
		J.MaxSalary,
		J.PostedOn,
		J.ClosingOn,
		J.[Description],
		J.CareerReadinessLevel,
		J.CareerReadinessLevelRequired
	FROM
		[Data.Application.Job] J
	LEFT OUTER JOIN JL
		ON JL.JobId = J.Id AND rnk = 1

GO
