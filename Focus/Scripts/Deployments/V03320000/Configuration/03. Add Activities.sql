﻿
	  /******************** ACTIVITIES ********************/
DECLARE @Xml XML
DECLARE @Activities TABLE
(
	Id INT IDENTITY(1, 1),
	ExternalId NVARCHAR(4),
	Name NVARCHAR(100),
	ActivityCategoryId bigint
)


SET @Xml = '
<activities>
  <activity category="Counseling" code="1252">Career Advising Plan</activity>
  <activity category="Case Management" code="106">Received Case Management</activity>
  <activity category="Labor Exchange" code="1207">Obtained Employment</activity>
  <activity category="Orientation" code="1232">Orientation (Trade)</activity>
  <activity category="Referrals" code="1225">Referral to Non-Partner</activity>
</activities>'

INSERT INTO @Activities
(
	Name,
	ExternalId,
	ActivityCategoryId
)

SELECT
	T.C.value('text()[1]', 'NVARCHAR(100)') AS Name,
	T.C.value('@code', 'NVARCHAR(100)') AS ExternalId,
	c.Id as ActivityCategoryId

FROM
	@Xml.nodes('/activities/activity') T(C)
	INNER JOIN [Config.ActivityCategory] c ON c.Name = T.C.value('@category','NVARCHAR(100)') AND c.ActivityType = 0


BEGIN TRANSACTION

INSERT INTO [Config.Activity]
(
	LocalisationKey,
	Name,
	Visible,
	Administrable,
	SortOrder,
	Used,
	ExternalId,
	ActivityCategoryId
)
SELECT 
	N'Activity.' + RIGHT(N'000' + CAST((SELECT COUNT(Id) + A.Id FROM [Config.Activity]) AS NVARCHAR(3)), 3),
	A.Name,
	1,
	1,
	0,
	0,
	A.ExternalId,
	A.ActivityCategoryId
FROM
	@Activities A
WHERE NOT EXISTS(SELECT * From [Config.Activity] config WHERE config.Name = A.Name)

IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

COMMIT TRANSACTION

GO


DECLARE @Xml XML
DECLARE @Activities TABLE
(
	Id INT IDENTITY(1, 1),
	ExternalId NVARCHAR(10),
	Name NVARCHAR(200),
	ActivityCategoryId bigint
)


SET @Xml = '
<activities>
<activity category="Assessment" code="5555157">Career Assessment</activity>
<activity category="Assessment" code="1107788">Eligibility Determination-HCTC</activity>
<activity category="Assessment" code="5555152">Eligibility Determination-Trade</activity>
<activity category="Assessment" code="1107790">Eligibility Determination-Trade A/RTAA</activity>
<activity category="Assessment" code="5555153">Eligibility Determination-Regular</activity>
<activity category="Assessment" code="5555142">Initial Interview/Assessment</activity>
<activity category="Case Management" code="5555160">Assigned-Non-Vets</activity>
<activity category="Case Management" code="1107794">Client-Centered</activity>
<activity category="Case Management" code="5555155">Closed</activity>
<activity category="Case Management" code="5555188">Follow-up-Trade</activity>
<activity category="Case Management" code="5555130">Other</activity>
<activity category="Case Management" code="5555117">Received Services-Non-Vets</activity>
<activity category="Counseling" code="5555156">Career Guidance</activity>
<activity category="Counseling" code="5555151">Employability Skills</activity>
<activity category="Counseling" code="5555145">Group Sessions</activity>
<activity category="Counseling" code="5555185">IEP - Individual Employment Plan</activity>
<activity category="Counseling" code="5555144">Individual &amp; Career Planning</activity>
<activity category="Counseling" code="5555141">Interest Inventory</activity>
<activity category="Counseling" code="5555140">Job Coaching</activity>
<activity category="Counseling" code="5555138">Job Prep Interviewing Skills</activity>
<activity category="Counseling" code="5555137">Job Retention Services</activity>
<activity category="Counseling" code="5555135">Job Search Planning</activity>
<activity category="Counseling" code="5555119">Career/Labor Market Info</activity>
<activity category="Counseling" code="5555125">Vocational Guidance-Other</activity>
<activity category="Counseling" code="5555163">Vocational Rehab-Other</activity>
<activity category="Customer Utilization" code="5555166">Mobile Job Center</activity>
<activity category="Customer Utilization" code="5555165">Resource Rooms</activity>
<activity category="Customer Utilization" code="5555170">Translation Services</activity>
<activity category="Events" code="5555182">Job Fair Attended</activity>
<activity category="Events" code="5555181">Job Fair Information Provided</activity>
<activity category="Events" code="5555180">Job Finding Club</activity>
<activity category="Labor Exchange" code="5555190">External Job Match</activity>
<activity category="Labor Exchange" code="5559266">External Job Referral</activity>
<activity category="Labor Exchange" code="5555189">Follow-up-General</activity>
<activity category="Labor Exchange" code="5555183">Job Development Contact</activity>
<activity category="Labor Exchange" code="5555149">Other Reportable Services-ES, DVOP, LVER</activity>
<activity category="Labor Exchange" code="1107823">Placed in HCTC</activity>
<activity category="Labor Exchange" code="5555175">Refused Referral</activity>
<activity category="Labor Exchange" code="5555174">Resume Preparation Assistance</activity>
<activity category="Labor Exchange" code="5555112">Workforce Info Services-Self-Service LMI</activity>
<activity category="Labor Exchange" code="5555111">Workforce Info Services-Staff-Assisted LMI</activity>
<activity category="Orientation" code="5555143">Informational Services</activity>
<activity category="Orientation" code="5555126">Other</activity>
<activity category="Orientation" code="5555118">Rapid Response</activity>
<activity category="Referrals" code="1107831">Adult Daycare</activity>
<activity category="Referrals" code="1107832">Adult Education</activity>
<activity category="Referrals" code="1107833">Child Care: General</activity>
<activity category="Referrals" code="1107834">Community/Technical College</activity>
<activity category="Referrals" code="1107835">CSBG: Elderly/Low Income</activity>
<activity category="Referrals" code="5559267">ES/UI</activity>
<activity category="Referrals" code="1107837">Emergency: ESG</activity>
<activity category="Referrals" code="5559265">Emergency: FEMA</activity>
<activity category="Referrals" code="1107839">Energy: LIHEAP</activity>
<activity category="Referrals" code="1107840">Energy: Wintercare</activity>
<activity category="Referrals" code="1107841">Faith-Based CBO</activity>
<activity category="Referrals" code="1107842">Goodwill</activity>
<activity category="Referrals" code="1107843">HCTC - Health Coverage Tax Credit</activity>
<activity category="Referrals" code="1107844">Headstart: Early</activity>
<activity category="Referrals" code="1107845">Headstart: Regular</activity>
<activity category="Referrals" code="1107846">Homeless Program</activity>
<activity category="Referrals" code="1107847">Housing: Regular</activity>
<activity category="Referrals" code="1107848">Housing: Tenant-Based Rental Assistance</activity>
<activity category="Referrals" code="5559264">Job Corps</activity>
<activity category="Referrals" code="5559263">MSFW: Federal</activity>
<activity category="Referrals" code="5559262">MSFW: State</activity>
<activity category="Referrals" code="5559261">Native American Program</activity>
<activity category="Referrals" code="5559260">Older Worker Program</activity>
<activity category="Referrals" code="1107854">Post-Secondary Education</activity>
<activity category="Referrals" code="1107855">Services for Hearing Impaired</activity>
<activity category="Referrals" code="1107856">Services for Victims-Spouse Abuse Shelter</activity>
<activity category="Referrals" code="1107857">Services for Victims-VOCA</activity>
<activity category="Referrals" code="1107858">Services for Visual Assistance/Exams</activity>
<activity category="Referrals" code="1107859">Services for Visually Impaired</activity>
<activity category="Referrals" code="5555124">Services for Vocational Rehabilitation</activity>
<activity category="Referrals" code="5559259">Supportive Service-Non-Partner</activity>
<activity category="Referrals" code="5559258">Supportive Service-Partner</activity>
<activity category="Referrals" code="1107863">TANF-Needy Families</activity>
<activity category="Referrals" code="5559257">Trade</activity>
<activity category="Referrals" code="1107865">Trade-A/RTAA</activity>
<activity category="Referrals" code="1107866">Transportation Assistance</activity>
<activity category="Referrals" code="5559253">WIA</activity>
<activity category="Referrals" code="5559254">Veterans Program</activity>
<activity category="Testing" code="1107869">ASSET Entrance Exam</activity>
<activity category="Testing" code="5555158">BEAG</activity>
<activity category="Testing" code="5555147">GATB</activity>
<activity category="Testing" code="5555146">GATB/VG</activity>
<activity category="Testing" code="5555134">Literacy</activity>
<activity category="Testing" code="5555133">NATB</activity>
<activity category="Testing" code="5555127">Other/Merit</activity>
<activity category="Testing" code="1107876">Pre-GED</activity>
<activity category="Testing" code="5555122">Proficiency</activity>
<activity category="Testing" code="5555114">SATB</activity>
<activity category="Testing" code="5555107">TABE Assessment</activity>
<activity category="Testing" code="1107880">TABE-WF Assessment</activity>
<activity category="Testing" code="5555100">Work Keys/NCRC Assessment</activity>
<activity category="Testing" code="5559252">Work Keys/NCRC referral</activity>
<activity category="Training" code="5559268">Basic Skills Referral</activity>
<activity category="Training" code="5555154">Computer Skills Referral</activity>
<activity category="Training" code="5555150">Entrepreneurial Referral</activity>
<activity category="Training" code="5555132">OJT Contract</activity>
<activity category="Training" code="5559255">Training Referral-External Provider</activity>
<activity category="Training" code="5559256">Training Referral-Internal</activity>
<activity category="Training" code="5555128">In Training-Other</activity>
<activity category="Training" code="5555123">In Training-Post-secondary</activity>
<activity category="Training" code="5555113">In Training-Secondary</activity>
<activity category="Training" code="5555139">Placed in Training-Job Corps</activity>
<activity category="Training" code="5555129">Placed in Training-Other Federal</activity>
<activity category="Training" code="5555101">Placed in Training-WIA</activity>
<activity category="Training" code="5555110">Training Termination-Successful Other</activity>
<activity category="Training" code="5555109">Training Termination-Successful Post-secondary</activity>
<activity category="Training" code="5555108">Training Termination-Successful Secondary</activity>
<activity category="UI/Reemployment" code="5555169">Benefit Rights Interview</activity>
<activity category="UI/Reemployment" code="5555105">Case Management-Intensive</activity>
<activity category="UI/Reemployment" code="5555121">Case Management-Profiled</activity>
<activity category="UI/Reemployment" code="5555168">ERP - Eligiblilty Review Program</activity>
<activity category="UI/Reemployment" code="1107902">Exempted Mandatory Profiling</activity>
<activity category="UI/Reemployment" code="5555187">Failed to Report-Individual Re-employment Plan</activity>
<activity category="UI/Reemployment" code="5555186">Failed to Report-Profiled</activity>
<activity category="UI/Reemployment" code="1107905">Failed to Report-REA</activity>
<activity category="UI/Reemployment" code="5555167">Information Provided - UI</activity>
<activity category="UI/Reemployment" code="5555120">Orientation-Profiled</activity>
<activity category="UI/Reemployment" code="1107908">Orientation-REA</activity>
<activity category="UI/Reemployment" code="5555104">Orientation-Re-employment</activity>
<activity category="UI/Reemployment" code="1107910">REA-EUC Job Search Eligible</activity>
<activity category="UI/Reemployment" code="1107911">REA-EUC Regular</activity>
<activity category="UI/Reemployment" code="1107912">REA-Labor Market &amp; Career Information</activity>
<activity category="UI/Reemployment" code="1107913">REA-Requirements Fulfilled</activity>
<activity category="Veterans" code="5555159">Assigned Case Management</activity>
<activity category="Veterans" code="1107915">Gold Card Outreach</activity>
<activity category="Veterans" code="1107916">Gold Card Services Eligible - Accepted</activity>
<activity category="Veterans" code="1107917">Gold Card Services Eligible - Declined</activity>
<activity category="Veterans" code="1107918">Incarcerated Veteran Outreach</activity>
<activity category="Veterans" code="5555148">Other Reportable Services Follow-up</activity>
<activity category="Veterans" code="5555116">Received Case Management</activity>
<activity category="Veterans" code="5555106">TAP Workshop</activity>
<activity category="Veterans" code="5555102">Vocational Guidance</activity>
<activity category="Veterans" code="5555103">Vocational Rehab-VA</activity>
<activity category="Workshops" code="5555136">Job Search Workshop</activity>
<activity category="Workshops" code="5555115">Resume Writing Workshop</activity>
<activity category="Workshops" code="5555131">Workshop Attended-Other</activity>
<activity category="Workshops" code="5559251">Workshop Referral</activity>
</activities>'

INSERT INTO @Activities
(
	Name,
	ExternalId,
	ActivityCategoryId
)

SELECT
	T.C.value('text()[1]', 'NVARCHAR(200)') AS Name,
	T.C.value('@code', 'NVARCHAR(100)') AS ExternalId,
	c.Id as ActivityCategoryId

FROM
	@Xml.nodes('/activities/activity') T(C)
	INNER JOIN [Config.ActivityCategory] c ON c.Name = T.C.value('@category','NVARCHAR(100)') AND c.ActivityType = 0


BEGIN TRANSACTION

INSERT INTO [Config.Activity]
(
	LocalisationKey,
	Name,
	Visible,
	Administrable,
	SortOrder,
	Used,
	ExternalId,
	ActivityCategoryId
)
SELECT
	N'Activity.' + RIGHT(N'000' + CAST((SELECT COUNT(Id) + A.Id FROM [Config.Activity]) AS NVARCHAR(3)), 3),
	A.Name,
	1,
	1,
	0,
	0,
	A.ExternalId,
	A.ActivityCategoryId
FROM
	@Activities A
	WHERE A.Name NOT IN (SELECT Name FROM [Config.Activity])


IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

COMMIT TRANSACTION


GO

DECLARE @ActivitiesUpdate TABLE
(
    Id INT IDENTITY(1, 1),
	OldActivityName NVARCHAR(100),
	NewActivityName NVARCHAR(100),
	CategoryName NVARCHAR(100),
	ExternalId  NVARCHAR(4)
)
INSERT INTO @ActivitiesUpdate
(
	OldActivityName,
	NewActivityName,
	CategoryName,
	ExternalId
)

VALUES('Initial Interview/Assessment','Assessment Interview - Initial Assessment','Assessment','9'),
	  ('Career Assessment','Assessment Services - Career Assessment','Assessment','11'),
	  ('TABE-WF Assessment','Assessment Test for Adult Basic Education (TABE)','Testing','1016'),
	  ('Work Keys/NCRC assessment','Assessment WorkKeys/National Career Readiness Certificate (NCRC)','Testing','1122'),
	  ('Assigned-Non-Vets','Assigned Case Management','Case Management','16'),
	  ('Assigned Case Management','Assigned Case Manager (VETS)','Veterans','15'),
	  ('BEAG','BEAG Test','Testing','25'),
	  ('Career Guidance','Career Guidance','Counseling','330'),
	  ('Closed','Case Management Closed','Case Management','1180'),
	  ('Other','Case Management Other','Case Management','1019'),
	  ('Group Sessions','Counseling - Group Sessions','Counseling','13'),
	  ('Individual and Career Planning','Counseling Individual and Career Planning','Counseling','12'),
	  ('Eligibility Determination – Regular','Eligibility Determination','Assessment','21'),
	  ('ERP-Eligibility Review Program','Eligibility Review Program (ERP)','UI/Reemployment','1001'),
	  ('Employability Skills','Employability Skills','Counseling','240'),
	  ('Entrepreneurial Referral','Entrepreneurial Training','Training','131'),
	  ('Exempted Mandatory Profiling','Exempted From UI Profiling Mandatory Participation',' UI/Reemployment','318'),
	  ('External Job Referral','External Job Referral','Labor Exchange','371'),
	  ('Employability Skills','Failure to Report (Individual Re-employment Plan)','UI/Reemployment','1218'),
	  ('Failed to Report-REA','Failure to Report (REA)','UI/Reemployment','1219'),
	  ('Follow-up-General','Follow Up','Labor Exchange','390'),
	  ('GABTB/VG','General Aptitude Test Battery – Validity Generalization (GATB/VG)','Testing','203'),
	  ('GATB','General Aptitude Test Battery (GATB)','Testing','23'),
	  ('Incarcerated Veterans Outreach','Incarcerated Veteran Outreach','Veterans','1214'),
	  ('IEP-Individual Employment Plan','Individual Employment Plan (IEP)','Counseling','111'),
	  ('Interest Inventory','Interest Inventory','Counseling','31'),
    ('Job Coaching','Job Coaching','Counseling','40'),
    ('Job Development Contact','Job Development Contact','Labour Exchange','38'),
    ('Job Fair Attended','Job Fair Attendance','Events','44'),
	  ('Job Fair Information Provided','Job Fair Information Provided','Events','1181'),
	  ('Job Finding Club','Job Finding Club','Events','36'),
	  ('Job Prep Interviewing Skills','Job Preparation Interviewing Skills','Counseling','1043'),
	  ('Job Search Planning','Job Search Planning','Counseling','32'),
	  ('Job Search Workshop','Job Search Workshop','Workshop','35'),
	  ('REA-Labor Market & Career Information','Labor Market and Career Information Provided (REA)','UI/Reemployment','1227'),
    ('NATB','NATB Test','Testing','24'),
	  ('Other','Orientation (Other)','Orientation','119'),
	  ('Rapid Response','Orientation(Rapid Response)','Orientation','301'),
	  ('Orientation-REA','Orientation (REA)','UI/Re-employment','1217'),
	  ('Orientation-Reemployment','Orientation (UI Reemployment Service)','UI/Reemployment','362'),
    ('Other Reportable Services ES, DVOP, LVER','Other Reportable Services (ES, DVOP, LVER)','Labour Exchange','71'),
    ('Placed in Training-WIA','Placed in Training (WIA)','Training','210'),
    ('Received Services Non-Vets','Received Case Management Services','Case Management','18'),
    ('Received Case Management','Received Case Management Services (VETS)','Veterans','17'),
    ('Adult Education','Referral to Adult Education','Referrals','1025'),
	  ('Training Referral – External Provider','Referral to External Training Provider','Training','1157'),
	  ('MSFW: State','Referral to KY Farmworkers','Referrals','1177'),
	  ('Services for Visually Impaired','Referral to Office for the Blind (OFB)','Referrals','1147'),
	  ('Services for Vocational Rehabilitation','Referral to Office of Vocational Rehabilitation (OVR)','Referrals','1035'),
	  ('Veterans Program','Referral to Veteran Program','Referrals','1034'),
	  ('Work Keys/NCRC referral','Referral to WorkKeys/National Career Readiness Certificate (NCRC) Testing','Testing','1206'),
	  ('Workshop Referral','Referral to Workshop','Workshops','1251'),
	  ('Supportive Services – Non-Partner','Referred to Supportive Services - Non-Partner','Referrals','57'),
	  ('Supportive Services – Partner','Referred to Supportive Services - Partner','Referrals','56'),
	  ('Training Referral-Internal','Referred to Training','Training','205'),
	  ('WIA','Referred to WIA','Referrals','204'),
	  ('REA-EUC Job Search Eligible','RES/REA EUC Job Search Eligibility Confirmed','UI/Reemployment','1229'),
	  ('REA-Requirement Fulfilled','RES/REA EUC Requirements Fulfilled','UI/Reemployment','1230'),
	  ('Resume Preparation Assistance','Resume Preparation Assistance','Labor Exchange','37'),
	  ('SATB','SATB Test','Testing','29'),
	  ('TAP Workshop','Transition Assistance Program Workshop TAP (VETS)','Veterans','363'),
	  ('Translation Services','Translation Service','Customer Utilization','1186'),
	  ('Benefit Rights Interview','Unemployment Insurance Benefits Right Interview','UI/Reemployment','1172'),
	  ('Information Provided-UI','Unemployment Insurance Information','UI/Reemployment','1176'),
	  ('Resource Rooms','Utilizing Resource Rooms','Customer Utilization','46'),
    ('Vocational Guidance-Other','Vocational Guidance (Other)','Counseling','20'),
	  ('Vocational Guidance','Vocational Guidance (VETS)','Veterans','19'),
	  ('Workforce Info Services-Staff-Assisted (LMI)','Workforce Information Services Staff Assisted (LMI)','Labor Exchange','39'),
	  ('Workshop Attended-Other','Workshop Attendance','Workshops','1250')


INSERT INTO [Config.Activity]
(
	LocalisationKey,
	Name,
	Visible,
	Administrable,
	SortOrder,
	Used,
	ExternalId,
	ActivityCategoryId
)
SELECT
	N'Activity.' + RIGHT(N'000' + CAST((SELECT COUNT(Id) + AU.Id FROM [Config.Activity]) AS NVARCHAR(3)), 3),
	AU.OldActivityName,
	1,
	1,
	0,
	0,
	AU.ExternalId,
	AC.Id
FROM
	@ActivitiesUpdate AU
	LEFT JOIN [Config.Activity] A ON A.Name = AU.OldActivityName
	INNER JOIN [Config.ActivityCategory] AC	ON AC.Name = AU.CategoryName
	WHERE AC.ActivityType = 0 AND AU.OldActivityName NOT IN (SELECT Name FROM [Config.Activity])



UPDATE 
       A
SET
       [Name] = AU.NewActivityName,
	   [ActivityCategoryId] = AC.Id,
       [ExternalId] = AU.ExternalId
FROM 
      @ActivitiesUpdate AU
INNER JOIN [Config.Activity] A
      ON A.Name = AU.OldActivityName
INNER JOIN [Config.ActivityCategory] AC
      ON AC.Id = A.ActivityCategoryId
    AND AC.Name = AU.CategoryName


DELETE  [dbo].[Config.Activity]
FROM [dbo].[Config.Activity]
LEFT OUTER JOIN (
   SELECT MIN(Id) as RowId,Name ,Visible
   FROM [dbo].[Config.Activity] 
   GROUP BY Name,Visible
) as KeepRows ON
   [dbo].[Config.Activity].Id = KeepRows.RowId
WHERE
   KeepRows.RowId IS NULL

  DELETE FROM
  [Config.Activity]
  WHERE Name = 'Workforce Info Services-Self-Service LMI' OR Name = 'Workforce Information Services Self-Service (LMI)' 
