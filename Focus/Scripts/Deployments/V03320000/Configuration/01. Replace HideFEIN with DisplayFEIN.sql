INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
SELECT 
	'DisplayFEIN',
	CASE Value
		WHEN 'True' THEN 'Hidden'
		ELSE 'Mandatory'
	END,		
	InternalOnly
FROM 
	[Config.ConfigurationItem] 
WHERE 
	[Key] = 'HideFEIN'
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.ConfigurationItem]
		WHERE
			[Key] = 'DisplayFEIN'
	)