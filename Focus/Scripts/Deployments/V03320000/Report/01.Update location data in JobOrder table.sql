UPDATE [dbo].[Report.JobOrder]
   SET [County] = RV.County
      ,[State] = RV.[State]
      ,[PostalCode] = RV.Zip
      ,[LatitudeRadians] = PC.Latitude * PI() / 180
      ,[LongitudeRadians] = PC.Longitude * PI() / 180
	FROM
		[dbo].[Report.JobOrder] as JO
	INNER JOIN
		[dbo].[Data.Application.JobOrderReportView] as RV
	ON
		JO.FocusJobId = RV.Id
	INNER JOIN
		[dbo].[Library.PostalCode] as PC
	ON
		RV.ZIP like PC.Code + '%'
GO


