IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeeker' AND COLUMN_NAME = 'Suffix')
BEGIN
	ALTER TABLE
		[Report.JobSeeker]
	ADD
		Suffix NVARCHAR(25) 
END
GO

IF NOT EXISTS(SELECT 1 FROM [Report.JobSeeker] WHERE Suffix IS NOT NULL)
BEGIN
	UPDATE
		JS
	SET
		Suffix = LU.Value
	FROM
		[Report.JobSeeker] JS
	INNER JOIN [Data.Application.Person] P
		ON P.Id = JS.FocusPersonId
	LEFT OUTER JOIN [Config.LookupItemsView] LU
		ON LU.Id = P.SuffixId
		AND LU.LookupType = 'Suffixes'
END

