IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'AwaitingApprovalActionedBy')
BEGIN
	ALTER TABLE
		[Data.Application.Job]
	ADD
		AwaitingApprovalActionedBy BIGINT NULL
END
GO