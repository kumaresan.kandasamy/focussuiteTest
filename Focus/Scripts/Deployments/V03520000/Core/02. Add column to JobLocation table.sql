IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobLocation' AND COLUMN_NAME = 'CreatedOn')
BEGIN
	ALTER TABLE
		[Data.Application.JobLocation]
	ADD
		CreatedOn DATETIME NOT NULL CONSTRAINT [DF_Data.Application.JobLocation_CreatedOn] DEFAULT('01 January 1900')
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobLocation' AND COLUMN_NAME = 'NewLocation')
BEGIN
	ALTER TABLE
		[Data.Application.JobLocation]
	ADD
		NewLocation BIT
END
GO
