IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient') IN ('Georgia', '8')
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',UnregisterJobFromLens'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%UnregisterJobFromLens%'

	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',UpdateJobAssignedOffice'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%UpdateJobAssignedOffice%'
END
