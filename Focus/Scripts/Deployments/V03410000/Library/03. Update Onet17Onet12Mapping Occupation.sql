IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Library.Onet17OccupationData')

UPDATE   [dbo].[Library.Onet17Onet12Mapping]
SET [dbo].[Library.Onet17Onet12Mapping].[Occupation] = [dbo].[Library.Onet17OccupationData].[Title]
FROM [dbo].[Library.Onet17Onet12Mapping] JOIN [dbo].[Library.Onet17OccupationData] 
   ON [dbo].[Library.Onet17Onet12Mapping].Onet17Code = [dbo].[Library.Onet17OccupationData].[O*NET-SOC Code]
   
GO