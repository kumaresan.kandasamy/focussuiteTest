IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Library.Onet17Onet12MappingView' AND COLUMN_NAME = 'Occupation')

/****** Object:  View [dbo].[Library.Onet17Onet12MappingView]    Script Date: 08/25/2015 13:38:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[Library.Onet17Onet12MappingView]
AS

SELECT
	loom.Id,
	loom.Onet17Code,
	lo.OnetCode AS Onet12Code,
	lo.Id AS OnetId,
	loom.Occupation
FROM
	[Library.Onet17Onet12Mapping] AS loom WITH (NOLOCK)
	INNER JOIN [Library.Onet] AS lo WITH (NOLOCK) ON loom.OnetId = lo.Id
	

GO


