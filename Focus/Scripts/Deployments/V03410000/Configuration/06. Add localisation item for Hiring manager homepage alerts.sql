﻿IF NOT EXISTS(SELECT * FROM [Config.LocalisationItem] WHERE [Key] = 'AlertMessage.HiringManagerMessage')
BEGIN
	DECLARE @LocalisationId BIGINT

	SELECT
		@LocalisationId = Localisation.Id
	FROM
		[Config.Localisation] Localisation
	WHERE
		Localisation.Culture = '**-**';

	INSERT INTO [Config.LocalisationItem]
	(
		[Key],
		Value,
		LocalisationId
	)
	VALUES
	(
		'AlertMessage.HiringManagerMessage',
		'Message from {0} {1}, posted on {2} - {3}',
		@LocalisationId
	)
END
GO
