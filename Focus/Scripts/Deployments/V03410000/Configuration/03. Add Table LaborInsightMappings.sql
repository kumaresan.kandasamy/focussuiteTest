IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Config.LaborInsightMapping')
BEGIN

CREATE TABLE [dbo].[Config.LaborInsightMapping](
	[Id] [uniqueidentifier] NOT NULL,
	[LaborInsightId] [nvarchar](200) NOT NULL,
	[LaborInsightValue] [nvarchar](200) NOT NULL,
	[CodeGroupKey] [nvarchar](200) NOT NULL,
	[CodeItemKey] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Config.LaborInsightMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[Config.LaborInsightMapping] ADD  CONSTRAINT [DF_Config.LaborInsightMapping_Id]  DEFAULT (newid()) FOR [Id]

END

