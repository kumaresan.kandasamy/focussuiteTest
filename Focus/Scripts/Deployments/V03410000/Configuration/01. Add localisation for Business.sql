﻿IF NOT EXISTS(SELECT * FROM [Config.LocalisationItem] WHERE [Key] = 'Global.Business')
BEGIN
	DECLARE @LocalisationId BIGINT

	SELECT
		@LocalisationId = Localisation.Id
	FROM
		[Config.Localisation] Localisation
	WHERE
		Localisation.Culture = '**-**';

	INSERT INTO [Config.LocalisationItem]
	(
		[Key],
		Value,
		LocalisationId
	)
	VALUES
	(
		'Global.Business',
		'Business',
		@LocalisationId
	)
END
GO

IF NOT EXISTS(SELECT * FROM [Config.LocalisationItem] WHERE [Key] = 'Global.Businesses')
BEGIN
	DECLARE @LocalisationId BIGINT

	SELECT
		@LocalisationId = Localisation.Id
	FROM
		[Config.Localisation] Localisation
	WHERE
		Localisation.Culture = '**-**';

	INSERT INTO [Config.LocalisationItem]
	(
		[Key],
		Value,
		LocalisationId
	)
	VALUES
	(
		'Global.Businesses',
		'Businesses',
		@LocalisationId
	)
END

