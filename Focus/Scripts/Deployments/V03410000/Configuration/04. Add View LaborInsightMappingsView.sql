IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Config.LaborInsightMappingView]'))
DROP VIEW [dbo].[Config.LaborInsightMappingView]
GO


CREATE VIEW [dbo].[Config.LaborInsightMappingView]
AS
	
SELECT 
	NEWID() As Id,
	ci.Id AS CodeGroupItemId,	
	cg.[Key] As CodeGroupKey,
	lim.LaborInsightId,
	lim.LaborInsightValue
FROM 
	[Config.CodeGroup] cg WITH (NOLOCK)
	INNER JOIN [Config.CodeGroupItem] cgi WITH (NOLOCK) ON cg.Id = cgi.CodeGroupId 
	INNER JOIN [Config.CodeItem] ci WITH (NOLOCK) ON cgi.CodeItemId = ci.Id
	INNER JOIN [Config.LaborInsightMapping] lim WITH (NOLOCK) ON lim.CodeGroupKey = cg.[Key] AND lim.CodeItemKey = ci.[Key]
GO


