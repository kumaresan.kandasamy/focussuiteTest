﻿IF NOT EXISTS(SELECT * FROM [Config.LocalisationItem] WHERE [Key] = 'Global.EmploymentType.Workforce')
BEGIN
	DECLARE @LocalisationId BIGINT

	SELECT
		@LocalisationId = Localisation.Id
	FROM
		[Config.Localisation] Localisation
	WHERE
		Localisation.Culture = '**-**';

	INSERT INTO [Config.LocalisationItem]
	(
		[Key],
		Value,
		LocalisationId
	)
	VALUES
	(
		'Global.EmploymentType.Workforce',
		'Job post',
		@LocalisationId
	)
END

IF NOT EXISTS(SELECT * FROM [Config.LocalisationItem] WHERE [Key] = 'Global.EmploymentTypes.Workforce')
BEGIN
	DECLARE @LocalisationsId BIGINT

	SELECT
		@LocalisationsId = Localisation.Id
	FROM
		[Config.Localisation] Localisation
	WHERE
		Localisation.Culture = '**-**';

	INSERT INTO [Config.LocalisationItem]
	(
		[Key],
		Value,
		LocalisationId
	)
	VALUES
	(
		'Global.EmploymentTypes.Workforce',
		'Job posts',
		@LocalisationsId
	)
END

