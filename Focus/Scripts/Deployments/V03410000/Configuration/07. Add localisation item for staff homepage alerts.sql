﻿IF NOT EXISTS(SELECT * FROM [Config.LocalisationItem] WHERE [Key] = 'AlertMessage.StaffMessage')
BEGIN
	DECLARE @LocalisationId BIGINT

	SELECT
		@LocalisationId = Localisation.Id
	FROM
		[Config.Localisation] Localisation
	WHERE
		Localisation.Culture = '**-**';

	INSERT INTO [Config.LocalisationItem]
	(
		[Key],
		Value,
		LocalisationId
	)
	VALUES
	(
		'AlertMessage.StaffMessage',
		'Message from {0} {1}, posted on {2} - {3}',
		@LocalisationId
	)
END
GO
