﻿
-- Now populate new table from the User table

IF NOT EXISTS (SELECT * FROM [dbo].[Data.Application.UserSecurityQuestion])
BEGIN
	CREATE TABLE #UserSecurityQuestion
	(
			[Id]     BIGINT NOT NULL IDENTITY(1,1),
			[UserId] BIGINT NOT NULL,
			[QuestionIndex] INT NOT NULL,
			[SecurityQuestion]        NVARCHAR (100) NULL,
			[SecurityAnswerHash]      NVARCHAR (86)  NULL,
			[SecurityAnswerEncrypted] NVARCHAR (250) NULL
	);

	-- Get a list of all 1st security questions and answers
	EXEC(N'
	INSERT INTO #UserSecurityQuestion
	SELECT	Id AS UserId,
					1 AS [QuestionIndex],
					SecurityQuestion,
					SecurityAnswerHash,
					SecurityAnswerEncrypted
	FROM [dbo].[Data.Application.User]
	WHERE SecurityQuestion IS NOT NULL;');

	-- Get a list of all 2nd security questions and answers
	EXEC(N'
	INSERT INTO #UserSecurityQuestion
	SELECT	Id AS UserId,
					2 AS [QuestionIndex],
					SecurityQuestion2,
					SecurityAnswer2Hash,
					SecurityAnswer2Encrypted
	FROM [dbo].[Data.Application.User]
	WHERE SecurityQuestion2 IS NOT NULL;')

	-- Get a list of all 3rd security questions and answers
	EXEC(N'
	INSERT INTO #UserSecurityQuestion
	SELECT	Id AS UserId,
					3 AS [QuestionIndex],
					SecurityQuestion3,
					SecurityAnswer3Hash,
					SecurityAnswer3Encrypted
	FROM [dbo].[Data.Application.User]
	WHERE SecurityQuestion3 IS NOT NULL;')

	-- Finally populate the new table with all of the security questions and answers

	INSERT INTO dbo.[Data.Application.UserSecurityQuestion]
	(
		[Id], 
		[UserId],
		[QuestionIndex],
		[SecurityQuestion],
		[SecurityAnswerHash],
		[SecurityAnswerEncrypted], 
		SecurityQuestionId
	)
	SELECT 
		[Id], 
		[UserId],
		[QuestionIndex],
		CASE WHEN ISNUMERIC([SecurityQuestion]) <> 1 THEN [SecurityQuestion] ELSE NULL END,
		[SecurityAnswerHash],
		[SecurityAnswerEncrypted], 
		CASE WHEN ISNUMERIC([SecurityQuestion]) = 1 THEN CONVERT(int,[SecurityQuestion]) ELSE NULL END
	FROM 
		#UserSecurityQuestion;
	
	-- We can now remove the old columns from the User table

	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityQuestion;
	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityAnswerHash;
	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityAnswerEncrypted;
	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityQuestion2;
	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityAnswer2Hash;
	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityAnswer2Encrypted;
	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityQuestion3;
	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityAnswer3Hash;
	ALTER TABLE dbo.[Data.Application.User] DROP COLUMN SecurityAnswer3Encrypted;

END
