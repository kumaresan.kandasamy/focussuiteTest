﻿IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.UserSecurityQuestion')
BEGIN

			CREATE TABLE [dbo].[Data.Application.UserSecurityQuestion]
			(
						[Id]                      BIGINT         NOT NULL,
						[UserId]                  BIGINT         NOT NULL,
						[QuestionIndex]           INT            NOT NULL,
						[SecurityQuestion]        NVARCHAR (100) NOT NULL,
						[SecurityAnswerHash]      NVARCHAR (86)  NULL,
						[SecurityAnswerEncrypted] NVARCHAR (250) NULL,
						[SecurityQuestionId]      BIGINT          NULL
			);

END
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.UserSecurityQuestion' AND COLUMN_NAME = 'SecurityQuestionId')
BEGIN
			ALTER TABLE [dbo].[Data.Application.UserSecurityQuestion]
			ADD [SecurityQuestionId] BIGINT NULL;
END

ALTER TABLE [dbo].[Data.Application.UserSecurityQuestion]
ALTER COLUMN [SecurityQuestion] NVARCHAR (100) NULL;
GO

-- Now the table is set, up delete all indexes and constraints

DECLARE @IndexName sysname;
DECLARE @IsPrimary bit;

WHILE EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
							WHERE TABLE_NAME = 'Data.Application.UserSecurityQuestion')
BEGIN
	SELECT TOP 1 @IndexName = CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS 
	WHERE TABLE_NAME = 'Data.Application.UserSecurityQuestion'
	
	PRINT 'ALTER TABLE dbo.[Data.Application.UserSecurityQuestion] DROP CONSTRAINT ' + @IndexName + ']';
	EXEC ('ALTER TABLE dbo.[Data.Application.UserSecurityQuestion] DROP CONSTRAINT [' + @IndexName + ']');
	
END

WHILE EXISTS (SELECT * 
FROM sys.indexes si
INNER JOIN sys.tables t ON si.object_id = t.object_id
WHERE t.name = 'Data.Application.UserSecurityQuestion'
AND si.name IS NOT NULL)

BEGIN

	SELECT TOP 1 @IndexName = si.name, @IsPrimary = si.is_primary_key
	FROM sys.indexes si
	INNER JOIN sys.tables t ON si.object_id = t.object_id
	WHERE t.name = 'Data.Application.UserSecurityQuestion'
	AND si.name IS NOT NULL;
	
	IF @IndexName IS NOT NULL
	BEGIN
		IF @IsPrimary = 1
		BEGIN
			PRINT 'ALTER TABLE dbo.[Data.Application.UserSecurityQuestion] DROP CONSTRAINT ' + @IndexName + ']';
			EXEC ('ALTER TABLE dbo.[Data.Application.UserSecurityQuestion] DROP CONSTRAINT [' + @IndexName + ']');
		END
		ELSE
		BEGIN
			PRINT 'DROP INDEX [' + @IndexName + '] ON dbo.[Data.Application.UserSecurityQuestion]';
			EXEC ('DROP INDEX [' + @IndexName + '] ON dbo.[Data.Application.UserSecurityQuestion]');
		END
		IF @@ERROR <> 0
			BREAK;
	END

END

-- Now add the indexes and keys that we want

ALTER TABLE [dbo].[Data.Application.UserSecurityQuestion] ADD CONSTRAINT [PK_Data.Application.UserSecurityQuestion] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY];

CREATE UNIQUE CLUSTERED INDEX [UQ_Data.Application.UserSecurityQuestion_UserId]
		ON [dbo].[Data.Application.UserSecurityQuestion]([UserId] ASC, [QuestionIndex] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0);

ALTER TABLE [dbo].[Data.Application.UserSecurityQuestion]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.UserSecurityQuestion_UserId] FOREIGN KEY([UserId])
	REFERENCES [dbo].[Data.Application.User] ([Id]);

ALTER TABLE [dbo].[Data.Application.UserSecurityQuestion] CHECK CONSTRAINT [FK_Data.Application.UserSecurityQuestion_UserId];
GO
