IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.RegistrationPin' AND COLUMN_NAME = 'CreatedOn')
BEGIN
	ALTER TABLE
		dbo.[Data.Application.RegistrationPin]
	ADD
		CreatedOn DATETIME NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.RegistrationPin' AND COLUMN_NAME = 'CreatedBy')
BEGIN
	ALTER TABLE
		dbo.[Data.Application.RegistrationPin]
	ADD
		CreatedBy BIGINT NULL
END
GO

