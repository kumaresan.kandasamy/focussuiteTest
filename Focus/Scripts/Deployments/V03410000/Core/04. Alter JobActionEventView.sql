IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.JobActionEventView')
BEGIN
	DROP VIEW [dbo].[Data.Application.JobActionEventView]
END
GO

CREATE VIEW [dbo].[Data.Application.JobActionEventView]
AS
SELECT	ae.Id, 
		j.Id AS JobId, 
		ae.UserId, 
		p.FirstName, 
		p.LastName,		
		ae.ActionedOn, 
		at.Name AS ActionName, 
		ae.AdditionalDetails,
		'' AS AdditionalName
FROM  dbo.[Data.Core.ActionType] AS at WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Core.ActionEvent] AS ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId
	INNER JOIN dbo.[Data.Application.Job] AS j WITH (NOLOCK) ON ae.EntityId = j.Id 
	INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON ae.UserId = u.Id 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id
WHERE at.Name IN (
	'PostJob',
	'SaveJob',
	'HoldJob',
	'CloseJob',
	'RefreshJob',
	'ReactivateJob',
	'CreateJob',
	'ApprovePostingReferral',
	'SelfReferral',
	'ReapplyReferralRequest',
	'AutoApprovedReferralBypass',
	'AutoResolvedIssue')
	
UNION ALL

SELECT 
	ae.Id, 
	po.JobId, 
	ae.UserId, 
	p.FirstName, 
	p.LastName, 
	ae.ActionedOn, 
	at.Name AS ActionName, 
	ae.AdditionalDetails,
	ISNULL(js.FirstName + ' ' + js.LastName, '') AS AdditionalName
FROM  dbo.[Data.Core.ActionType] AS at WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Core.ActionEvent] AS ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId 
	INNER JOIN dbo.[Data.Application.Application] AS ca WITH (NOLOCK) ON ae.EntityId = ca.Id 
	INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON ae.UserId = u.Id 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id 
	INNER JOIN dbo.[Data.Application.Posting] AS po WITH (NOLOCK) ON ca.PostingId = po.Id
	LEFT OUTER JOIN dbo.[Data.Application.Person] js WITH (NOLOCK) ON js.Id = ae.EntityIdAdditional02 AND at.Name = 'WaivedRequirements'
WHERE at.Name IN (
	'ApproveCandidateReferral', 
	'DenyCandidateReferral', 
	'CreateCandidateApplication',
	'WaivedRequirements',
	'SelfReferral',
	'ReferralRequest',
	'ReapplyReferralRequest',
	'AutoApprovedReferralBypass')
