IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.BusinessUnitActivityView')
BEGIN
	DROP VIEW [dbo].[Data.Application.BusinessUnitActivityView]
END
GO

CREATE VIEW [dbo].[Data.Application.BusinessUnitActivityView]
AS
SELECT		ActionEvent.Id AS Id,
			BusinessUnit.EmployerId, 
			BusinessUnit.Id AS BUId, 
			[User].UserName, 
			BusinessUnit.Name AS BUName, 
			ActionType.Name AS ActionName, 
            ActionEvent.ActionedOn, 
            ActionEvent.Id AS ActionId, 
            Job.Id as JobId, 
            Job.JobTitle
FROM        dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK) 
			INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id
			INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Job.Id = ActionEvent.EntityId 
			INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].Id = ActionEvent.UserId
			INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON BusinessUnit.Id = Job.BusinessUnitId												    
WHERE		ActionType.Name IN 
			('PostJob',
			'SaveJob',
			'HoldJob',
			'CloseJob',
			'RefreshJob',
			'ReactivateJob',
			'CreateJob')

UNION

SELECT		ActionEvent.Id AS Id,
			BusinessUnit.EmployerId, 
			BusinessUnit.Id AS BUId, 
			[User].UserName, 
			BusinessUnit.Name AS BUName, 
			ActionType.Name AS ActionName, 
            ActionEvent.ActionedOn, 
            ActionEvent.Id AS ActionId, 
            0 as JobId, 
            '' as JobTitle
FROM        dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK) 
			INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id
			INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].Id = ActionEvent.UserId
			INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON BusinessUnit.Id = ActionEvent.EntityId 											    
WHERE       ActionType.Name IN ('UnblockAllEmployerEmployeesAccount', 'BlockAllEmployerEmployeesAccount')
GO

