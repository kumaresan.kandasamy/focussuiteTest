IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.HiringManagerActivityView')
BEGIN
	DROP VIEW [dbo].[Data.Application.HiringManagerActivityView]
END
GO

CREATE VIEW [dbo].[Data.Application.HiringManagerActivityView]
AS
SELECT
	ae.Id AS Id,
	ebu.Id AS EmployeeBuId,
	ae.UserId,
	ae.EntityIdAdditional01,
	at.Name AS ActionName,
	p.FirstName AS EmployeeFirstName, 
	p.LastName AS EmployeeLastName,
	ae.ActionedOn,
	NULL AS JobId,
	NULL AS JobTitle,
	NULL AS AdditionalName
FROM  [Data.Core.ActionType] at WITH (NOLOCK) 
	INNER JOIN [Data.Core.ActionEvent] ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId 
	INNER JOIN [Data.Application.User] u WITH (NOLOCK) ON ae.UserId = u.Id 
	INNER JOIN [Data.Application.Person] p WITH (NOLOCK) ON u.PersonId = p.Id 
	INNER JOIN [Data.Application.Employee] e WITH (NOLOCK) ON e.Id = ae.EntityId 
	INNER JOIN [Data.Application.EmployeeBusinessUnit] ebu WITH (NOLOCK) ON ebu.EmployeeId = e.Id
WHERE at.Name IN ('AssignActivityToEmployee', 'BlockEmployeeAccount', 'UnblockEmployeeAccount')

UNION ALL

SELECT
	ae.Id,
	ebu.Id AS EmployeeBuId,
	ae.UserId,
	ae.EntityIdAdditional01,
	at.Name,
	p.FirstName, 
	p.LastName,
	ae.ActionedOn,
	NULL,
	NULL,
	adminPerson.FirstName + ' ' + adminPerson.LastName
FROM  [Data.Core.ActionType] at WITH (NOLOCK) 
	INNER JOIN [Data.Core.ActionEvent] ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId 
	INNER JOIN [Data.Application.User] u WITH (NOLOCK) ON ae.UserId = u.Id 
	INNER JOIN [Data.Application.Person] p WITH (NOLOCK) ON u.PersonId = p.Id 
	INNER JOIN [Data.Application.Employee] e WITH (NOLOCK) ON e.Id = ae.EntityId 
	INNER JOIN [Data.Application.EmployeeBusinessUnit] ebu WITH (NOLOCK) ON ebu.EmployeeId = e.Id 
	LEFT JOIN [Data.Application.User] adminUser WITH (NOLOCK) ON adminUser.Id = ae.EntityIdAdditional01 
	LEFT JOIN [Data.Application.Person] adminPerson WITH (NOLOCK) ON adminPerson.Id = adminUser.PersonId
WHERE at.Name IN ('AssignEmployeeToStaff', 'EmailEmployee')

UNION ALL

SELECT
	ae.Id,
	ebu.Id,
	ae.UserId,
	ae.EntityIdAdditional01,
	at.Name,
	p.FirstName,
	p.LastName,
	ae.ActionedOn,
	j.Id,
	j.JobTitle,
	js.FirstName + ' ' + js.LastName
FROM [Data.Core.ActionEvent] ae WITH (NOLOCK)
	inner join [Data.Core.ActionType] at WITH (NOLOCK) ON at.Id = ae.ActionTypeId
	inner join [Data.Application.User] u WITH (NOLOCK) ON u.Id = ae.UserId
	inner join [Data.Application.Person] p WITH (NOLOCK) ON p.Id = u.PersonId
	-- Application related actions have Application as EntityId
	inner join [Data.Application.Application] a on a.Id = ae.EntityId
	inner join [Data.Application.Posting] po on po.Id = a.PostingId
	inner join [Data.Application.Job] j on j.Id = po.JobId	
	inner join [Data.Application.EmployeeBusinessUnit] ebu on ebu.EmployeeId = j.EmployeeId
	inner join [Data.Application.Resume] r1 on r1.Id = a.ResumeId
	inner join [Data.Application.Person] js on js.Id = r1.PersonId
WHERE at.Name IN ('UpdateApplicationStatusToRecommended',
				  'UpdateApplicationStatusToFailedToShow',
				  'UpdateApplicationStatusToHired',
				  'UpdateApplicationStatusToInterviewScheduled',
				  'UpdateApplicationStatusToNewApplicant',
				  'UpdateApplicationStatusToNotApplicable',
				  'UpdateApplicationStatusToOfferMade',
				  'UpdateApplicationStatusToNotHired',
				  'UpdateApplicationStatusToUnderConsideration',
				  'UpdateApplicationStatusToDidNotApply',
				  'UpdateApplicationStatusToInterviewDenied',
				  'UpdateApplicationStatusToRefusedOffer',
				  'UpdateApplicationStatusToSelfReferred')
				  
UNION ALL

SELECT
	ae.Id,
	ebu.Id,
	ae.UserId,
	ae.EntityIdAdditional01,
	at.Name,
	p.FirstName,
	p.LastName,
	ae.ActionedOn,
	j.Id,
	j.JobTitle,
	NULL
FROM [Data.Core.ActionEvent] ae WITH (NOLOCK)
	inner join [Data.Core.ActionType] at WITH (NOLOCK) ON at.Id = ae.ActionTypeId
	inner join [Data.Application.User] u WITH (NOLOCK) ON u.Id = ae.UserId
	inner join [Data.Application.Person] p WITH (NOLOCK) ON p.Id = u.PersonId
	inner join [Data.Application.Job] j WITH (NOLOCK) ON j.Id = ae.EntityId
	inner join [Data.Application.EmployeeBusinessUnit] ebu WITH (NOLOCK) ON ebu.EmployeeId = j.EmployeeId
WHERE at.Name IN ('PostJob',
				  'SaveJob',
				  'HoldJob',
				  'CloseJob',
				  'RefreshJob',
				  'ReactivateJob',
				  'CreateJob')

GO


