
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_Data.Core.ActionType_Name')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Core.ActionType_Name] ON dbo.[Data.Core.ActionType] ([Name])
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_Data.Core.ActionEvent_EntityIdAdditional01')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Core.ActionEvent_EntityIdAdditional01] ON dbo.[Data.Core.ActionEvent] ([EntityIdAdditional01]) WITH (FILLFACTOR = 90)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_Data.Application.Job_BusinessUnitId')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.Job_BusinessUnitId] ON dbo.[Data.Application.Job] ([BusinessUnitId])
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IX_Data.Application.BusinessUnitDescription_BusinessUnitId')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.BusinessUnitDescription_BusinessUnitId] ON dbo.[Data.Application.BusinessUnitDescription] ([BusinessUnitId])
END