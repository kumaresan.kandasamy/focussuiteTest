IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'Data.Core.Session_ExpireSessions')
BEGIN
	DROP PROCEDURE [dbo].[Data.Core.Session_ExpireSessions]
END
GO

CREATE PROCEDURE [dbo].[Data.Core.Session_ExpireSessions] 
	@RemoveDays INT = NULL,
	@RemoveDate DATETIME = NULL,
	@BatchSize INT = 500
AS
BEGIN
	SET NOCOUNT ON
	
    DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @ErrorSeverity INT
    DECLARE @ErrorState INT

    IF @RemoveDays IS NOT NULL AND @RemoveDate IS NOT NULL
    BEGIN
		RAISERROR('Only one of @RemoveDays or @RemoveDate must be specified', 16, 1)
		RETURN
    END    
        
    IF @RemoveDays IS NULL AND @RemoveDate IS NULL
    BEGIN
		SET @RemoveDays = 1

		SELECT @RemoveDays = CAST(Value AS INT)
		FROM [Config.ConfigurationItem]
		WHERE [Key] = 'SessionRetentionDays'
    END
 
	IF @RemoveDate IS NULL
	BEGIN
		SET @RemoveDate = CAST(DATEADD(DAY, 0 - @RemoveDays, GETUTCDATE()) AS DATE)
	END

	DECLARE @SessionIds TABLE
	(
		Id UNIQUEIDENTIFIER PRIMARY KEY
	)

	DECLARE @Done BIT
	SET @Done = 0

	WHILE @Done = 0
	BEGIN
		INSERT INTO @SessionIds
		(
			Id
		)
		SELECT TOP (@BatchSize)
			S.Id
		FROM
			[Data.Core.Session] S
		WHERE
			S.LastRequestOn < @RemoveDate	
		
		IF @@ROWCOUNT > 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				DELETE
				  SS
				FROM
					[Data.Core.SessionState] SS
				INNER JOIN @SessionIds TS
					ON TS.Id = SS.SessionId

				DELETE
				  S
				FROM
					[Data.Core.Session] S
				INNER JOIN @SessionIds TS
					ON TS.Id = S.Id
					
				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION
				END
				
				SELECT 
					@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE()

				RAISERROR (@ErrorMessage, @ErrorSeverity,@ErrorState)
				
				RETURN
			END	CATCH
			
			DELETE FROM @SessionIds
		END
		ELSE
		BEGIN
			SET @Done = 1
		END
	END
END
GO
