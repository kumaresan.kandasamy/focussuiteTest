IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient') IN ('Georgia', '8')
BEGIN		
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',ActivateAccount'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%,ActivateAccount%'
	
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',InactivateAccount'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%InactivateAccount%'	
		
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',BlockUser'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%,BlockUser%'
		
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',UnBlockUser'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%UnBlockUser%'	

	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',ReapplyReferralRequest'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%ReapplyReferralRequest%'	
		
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',HoldCandidateReferral'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%,HoldCandidateReferral%'	

	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',DenyCandidateReferral'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%,DenyCandidateReferral%'
		
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',UpdateReferralStatusToAutoOnHold'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%,UpdateReferralStatusToAutoOnHold%'	

	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',UpdateReferralStatusToAutoDenied'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%,UpdateReferralStatusToAutoDenied%'
		
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',UpdateApplicationStatusToFailedToShow'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%UpdateApplicationStatusToFailedToShow%'
			
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',UpdateApplicationStatusToNotYetPlaced'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%UpdateApplicationStatusToNotYetPlaced%'
END