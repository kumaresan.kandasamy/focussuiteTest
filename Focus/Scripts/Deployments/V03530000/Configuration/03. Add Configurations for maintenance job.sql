DECLARE @RetentionDays TABLE
(
	Config NVARCHAR(100),
	[Days] NVARCHAR(3)
)
INSERT INTO @RetentionDays (Config, [Days])
VALUES
	('SessionRetentionDays', '1'),
	('PageStateRentionDays', '1'),
	('LogEventRetentionDays', '30'),
	('ProfileEventRetentionDays', '30'),
	('MessageRetentionDays', '30')
	
INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
SELECT
	Config,
	[Days],
	1
FROM
	@RetentionDays RD
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.ConfigurationItem] CI
		WHERE
			CI.[Key] = RD.Config
	)
	
