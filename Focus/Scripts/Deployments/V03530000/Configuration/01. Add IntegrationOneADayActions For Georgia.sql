IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient') IN ('Georgia', '8')
BEGIN
	IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationOneADayActions')
	BEGIN
		INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
		VALUES ('IntegrationOneADayActions', 'SaveResume,LensJobSearch,ViewedPostingFromSearch,ExternalReferral,ExternalStaffReferral,LogIn,FindJobsForSeeker', 1)
	END
END
