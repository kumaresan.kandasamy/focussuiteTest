IF (SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'DateOfBirth') IN ('nvarchar', 'varchar')
BEGIN
	ALTER TABLE 
		[Data.Application.Resume] 
	ALTER COLUMN 
		DateOfBirth DATETIME
END
GO

IF (SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Person' AND COLUMN_NAME = 'DateOfBirth') IN ('nvarchar', 'varchar')
BEGIN
	ALTER TABLE 
		[Data.Application.Person] 
	ALTER COLUMN 
		DateOfBirth DATETIME
END
GO

/*
sp_helptext '[Data.Application.CandidateView]'
sp_helptext '[Data.Application.CandidateIssueView]'
sp_helptext '[Data.Application.JobSeekerProfileView]'
sp_helptext '[Data.Application.StudentAlumniIssueView]'
*/