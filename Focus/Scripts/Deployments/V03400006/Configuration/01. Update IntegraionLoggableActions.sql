IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient') = 'Georgia'
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',AutoApprovedReferralBypass'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%AutoApprovedReferralBypass%'
END