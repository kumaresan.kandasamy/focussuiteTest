DECLARE @LocalisationId BIGINT
DECLARE @CodeGroupId BIGINT
DECLARE @CodeItemId BIGINT

SELECT @LocalisationId = Id FROM [Config.Localisation] WHERE Culture = '**-**'
SELECT @CodeGroupId = Id FROM [Config.CodeGroup] WHERE [Key] = 'MilitaryDischargeTypes'

BEGIN TRANSACTION

IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'MilitaryDischargeType.Medical')
BEGIN
	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], Value, Localised, LocalisationId)
	VALUES ('', 'MilitaryDischargeType.Medical', 'Medical', 0, @LocalisationId)
END

IF NOT EXISTS(SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'MilitaryDischargeType.Medical')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ParentKey, ExternalId, CustomFilterId) 
	VALUES ('MilitaryDischargeType.Medical', 1, NULL, 'Medical', NULL)
	
	SET @CodeItemId = SCOPE_IDENTITY()
	
	INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId)
	VALUES(0, @CodeGroupId, @CodeItemId)
END

COMMIT TRANSACTION