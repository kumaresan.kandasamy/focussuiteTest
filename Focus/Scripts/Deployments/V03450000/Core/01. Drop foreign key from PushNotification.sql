﻿IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[PK_Data.Application.PushNotification_PersonMobileDeviceId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Application.PushNotification]'))
ALTER TABLE [dbo].[Data.Application.PushNotification] DROP CONSTRAINT [PK_Data.Application.PushNotification_PersonMobileDeviceId]
GO
