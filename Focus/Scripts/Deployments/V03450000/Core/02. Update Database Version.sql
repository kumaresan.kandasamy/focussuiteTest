﻿IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Config.DbVersion')
	DROP TABLE [dbo].[Config.DbVersion];

CREATE TABLE [dbo].[Config.DbVersion]
(
	[VersionNumber] varchar(30) NULL
)

INSERT INTO [dbo].[Config.DbVersion]
([VersionNumber])
VALUES
('3.45')