﻿IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.EmployerAccountReferralInnerView')
	DROP VIEW dbo.[Data.Application.EmployerAccountReferralInnerView]
GO
CREATE VIEW dbo.[Data.Application.EmployerAccountReferralInnerView]
AS
WITH EmployersAndEmployees
AS
(
	SELECT					Employer.Id AS EmployerId,
									Employer.ApprovalStatus AS EmployerApprovalStatus,
									ROW_NUMBER() OVER (PARTITION BY Employer.Id ORDER BY Employee.Id) AS EmployerEmployeeNumber,
									ROW_NUMBER() OVER (PARTITION BY BusinessUnit.Id ORDER BY Employee.Id) AS BusinessUnitEmployeeNumber,
									BusinessUnit.ApprovalStatus As BusinessUnitApprovalStatus,
									EmployeeBusinessUnit.[Default] AS BusinessUnitIsDefault,
									Employee.ApprovalStatus AS EmployeeApprovalStatus,
									Employee.Id AS EmployeeId,
									BusinessUnit.Id AS BusinessUnitId,
									BusinessUnit.IsPrimary AS EmployerIsPrimary
	FROM						dbo.[Data.Application.Employer] AS Employer WITH (NOLOCK)
	INNER JOIN			dbo.[Data.Application.Employee] AS Employee WITH (NOLOCK) ON Employer.Id = Employee.EmployerId
	INNER JOIN			dbo.[Data.Application.EmployeeBusinessUnit] AS EmployeeBusinessUnit WITH (NOLOCK) ON Employee.Id = EmployeeBusinessUnit.EmployeeId
	INNER JOIN			dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.BusinessUnitId = BusinessUnit.Id
)
SELECT	EmployersAndEmployees.EmployerId AS Id,
				EmployersAndEmployees.EmployerId,
				EmployersAndEmployees.EmployerApprovalStatus,
				EmployersAndEmployees.BusinessUnitApprovalStatus,
				EmployersAndEmployees.BusinessUnitIsDefault,
				EmployersAndEmployees.EmployeeId,
				EmployersAndEmployees.EmployerIsPrimary,
				EmployersAndEmployees.EmployeeApprovalStatus,
				EmployersAndEmployees.BusinessUnitId,
				1 AS TypeOfApproval
FROM		EmployersAndEmployees
WHERE		EmployersAndEmployees.EmployerApprovalStatus IN (1, 4, 3)
AND			EmployersAndEmployees.EmployerEmployeeNumber = 1

UNION

SELECT	EmployersAndEmployees.EmployeeId AS Id,
				EmployersAndEmployees.EmployerId,
				EmployersAndEmployees.EmployerApprovalStatus,
				EmployersAndEmployees.BusinessUnitApprovalStatus,
				EmployersAndEmployees.BusinessUnitIsDefault,
				EmployersAndEmployees.EmployeeId,
				EmployersAndEmployees.EmployerIsPrimary,
				EmployersAndEmployees.EmployeeApprovalStatus,
				EmployersAndEmployees.BusinessUnitId,
				3 AS TypeOfApproval
FROM		EmployersAndEmployees
WHERE		EmployersAndEmployees.EmployeeApprovalStatus IN (1, 4, 3)
AND
(
	(EmployersAndEmployees.BusinessUnitEmployeeNumber > 1 AND EmployersAndEmployees.EmployerEmployeeNumber > 1)
	OR
	(EmployersAndEmployees.EmployerApprovalStatus IN (0, 2) AND EmployersAndEmployees.BusinessUnitApprovalStatus  IN (0, 2) AND EmployersAndEmployees.BusinessUnitIsDefault = 1)
)
AND NOT EXISTS
(
	SELECT			1
	FROM				[Data.Application.EmployeeBusinessUnit] EBU
	INNER JOIN	[Data.Application.BusinessUnit] BU ON BU.Id = EBU.BusinessUnitId
	WHERE				EBU.EmployeeId = EmployersAndEmployees.EmployeeId
	AND					BU.Id <> EmployersAndEmployees.BusinessUnitId
	AND					BU.ApprovalStatus IN (1, 3, 4)
)

UNION

SELECT	EmployersAndEmployees.BusinessUnitId AS Id,
				EmployersAndEmployees.EmployerId,
				EmployersAndEmployees.EmployerApprovalStatus,
				EmployersAndEmployees.BusinessUnitApprovalStatus,
				EmployersAndEmployees.BusinessUnitIsDefault,
				EmployersAndEmployees.EmployeeId,
				EmployersAndEmployees.EmployerIsPrimary,
				EmployersAndEmployees.EmployeeApprovalStatus,
				EmployersAndEmployees.BusinessUnitId,
				2 AS TypeOfApproval
FROM		EmployersAndEmployees
WHERE		EmployersAndEmployees.BusinessUnitApprovalStatus IN (1, 4, 3)
AND			(EmployersAndEmployees.EmployerIsPrimary = 0 OR EmployersAndEmployees.EmployerApprovalStatus IN (0, 2))
AND			EmployersAndEmployees.BusinessUnitEmployeeNumber = 1
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.EmployerAccountReferralView')
	DROP VIEW dbo.[Data.Application.EmployerAccountReferralView]
GO
CREATE VIEW dbo.[Data.Application.EmployerAccountReferralView]
AS

SELECT					FilteredList.Id,
								FilteredList.EmployerId,
								CASE FilteredList.TypeOfApproval
									WHEN 1 THEN Employer.Name
									ELSE BusinessUnit.Name
								END AS EmployerName,
								FilteredList.EmployerIsPrimary,
								FilteredList.BusinessUnitIsDefault,
								FilteredList.EmployerApprovalStatus,
								FilteredList.TypeOfApproval,
								FilteredList.EmployeeId,
								Person.FirstName AS EmployeeFirstName,
								Person.LastName AS EmployeeLastName,
								FilteredList.EmployeeApprovalStatus,
								FilteredList.BusinessUnitApprovalStatus,
								CASE FilteredList.TypeOfApproval
									WHEN 2 THEN COALESCE(BusinessUnit.AwaitingApprovalDate, Employee.AwaitingApprovalDate, [User].CreatedOn)
									ELSE	ISNULL(Employee.AwaitingApprovalDate, [User].CreatedOn)
								END AS AwaitingApprovalDate,
								[User].CreatedOn AS AccountCreationDate,
								BusinessUnitAddress.Line1 AS EmployerAddressLine1,
								BusinessUnitAddress.Line2 AS EmployerAddressLine2,
								BusinessUnitAddress.TownCity AS EmployerAddressTownCity,
								BusinessUnitAddress.StateId AS EmployerAddressStateId,
								BusinessUnitAddress.PostcodeZip AS EmployerAddressPostcodeZip,
								BusinessUnitAddress.PublicTransitAccessible AS EmployerPublicTransitAccessible,
								BusinessUnit.PrimaryPhone,
								BusinessUnit.PrimaryPhoneType,
								BusinessUnit.AlternatePhone1,
								BusinessUnit.AlternatePhone1Type,
								BusinessUnit.AlternatePhone2,
								BusinessUnit.AlternatePhone2Type,
								CASE WHEN BusinessUnit.PrimaryPhoneType = 'Phone' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.PrimaryPhoneType = 'Mobile' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.AlternatePhone1Type = 'Phone' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone1Type = 'Mobile' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone2Type = 'Phone' THEN BusinessUnit.AlternatePhone2 WHEN BusinessUnit.AlternatePhone2Type = 'Mobile' THEN BusinessUnit.AlternatePhone2 END AS EmployerPhoneNumber,
								CASE WHEN BusinessUnit.PrimaryPhoneType = 'Fax' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.AlternatePhone1Type = 'Fax' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone2Type = 'Fax' THEN BusinessUnit.AlternatePhone2 END AS EmployerFaxNumber,
								PhoneNumber.Number AS EmployeePhoneNumber,
								FaxNumber.Number AS EmployeeFaxNumber,
								BusinessUnit.Url AS EmployerUrl,
								--BusinessUnit.AwaitingApprovalDate AS BusinessUnitAwaitingApprovalDate,
								Employer.FederalEmployerIdentificationNumber AS EmployerFederalEmployerIdentificationNumber,
								Employer.StateEmployerIdentificationNumber AS EmployerStateEmployerIdentificationNumber,
								BusinessUnit.IndustrialClassification AS EmployerIndustrialClassification,
								PersonAddress.Line1 AS EmployeeAddressLine1,
								PersonAddress.Line2 AS EmployeeAddressLine2,
								PersonAddress.TownCity AS EmployeeAddressTownCity,
								PersonAddress.StateId AS EmployeeAddressStateId,
								PersonAddress.PostcodeZip AS EmployeeAddressPostcodeZip,
								Person.EmailAddress AS EmployeeEmailAddress,
								--Employer.ApprovalStatus AS EmployerApprovalStatus,
								BusinessUnit.OwnershipTypeId AS EmployerOwnershipTypeId,
								Employer.AssignedToId AS EmployerAssignedToId,
								Employee.RedProfanityWords,
								Employee.YellowProfanityWords,
								ISNULL(BusinessUnitDescription.Description, '') AS BusinessUnitDescription,
								BusinessUnit.Name AS BusinessUnitName,
								FilteredList.BusinessUnitId,
								Employee.ApprovalStatusChangedBy,
								Employee.ApprovalStatusChangedOn,
								BusinessUnit.AccountTypeId AS EmployerAccountTypeId,
								BusinessUnit.LegalName AS BusinessUnitLegalName,
								BusinessUnit.RedProfanityWords AS BusinessUnitRedProfanityWords,
								BusinessUnit.YellowProfanityWords AS BusinessUnitYellowProfanityWords,
								Person.SuffixId,
								'' AS Suffix,
								CAST(Employer.LockVersion as nvarchar(10)) + '.' + CAST(BusinessUnit.LockVersion as nvarchar(10)) + '.' + CAST(Employee.LockVersion as nvarchar(10)) as AccountLockVersion
FROM						dbo.[Data.Application.EmployerAccountReferralInnerView] FilteredList
INNER JOIN			dbo.[Data.Application.Employer] AS Employer WITH (NOLOCK) ON Employer.Id = FilteredList.EmployerId
INNER JOIN			dbo.[Data.Application.Employee] AS Employee WITH (NOLOCK) ON Employee.Id = FilteredList.EmployeeId
INNER JOIN			dbo.[Data.Application.Person] AS Person WITH (NOLOCK) ON Person.Id = Employee.PersonId
INNER JOIN			dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].PersonId =Person.Id
INNER JOIN			dbo.[Data.Application.EmployeeBusinessUnit] AS EmployeeBusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.EmployeeId = FilteredList.EmployeeId
INNER JOIN			dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON BusinessUnit.Id = FilteredList.BusinessUnitId
INNER JOIN			dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress WITH (NOLOCK) ON BusinessUnitAddress.BusinessUnitId = FilteredList.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
LEFT OUTER JOIN	dbo.[Data.Application.BusinessUnitDescription] AS BusinessUnitDescription WITH (NOLOCK) ON BusinessUnitDescription.BusinessUnitId = FilteredList.BusinessUnitId AND BusinessUnitDescription.IsPrimary = 1
LEFT OUTER JOIN dbo.[Data.Application.PersonAddress] AS PersonAddress WITH (NOLOCK) ON Person.Id = PersonAddress.PersonId
LEFT OUTER JOIN dbo.[Data.Application.PhoneNumber] AS PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND (PhoneNumber.PhoneType = 0 OR PhoneNumber.PhoneType = 1) AND PhoneNumber.IsPrimary = 1
LEFT OUTER JOIN dbo.[Data.Application.PhoneNumber] AS FaxNumber WITH (NOLOCK) ON Person.Id = FaxNumber.PersonId AND FaxNumber.PhoneType = 4
GO

