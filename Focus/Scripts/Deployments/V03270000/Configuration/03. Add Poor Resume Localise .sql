﻿DECLARE @LocalisationId BIGINT

SELECT @LocalisationId = Id FROM [Config.Localisation] WHERE Culture = '**-**'

IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'PostingLowQualityResume.Text')
	BEGIN
		RAISERROR ('Start - Localisation Item - PostingLowQualityResume.Text', 0, 1) WITH NOWAIT

		INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
		VALUES ('', 'PostingLowQualityResume.Text', 'Your default resume has been deemed poor quality. Please contact your local Career Center for advise on how it could be improved', 0, @LocalisationId)
		
		RAISERROR ('End - Localisation Item - PostingLowQualityResume.Text', 0, 1) WITH NOWAIT
END




