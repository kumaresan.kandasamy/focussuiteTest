﻿UPDATE cgi
SET     cgi.DisplayOrder =  
			CASE  
				WHEN ci.[Key] = 'MilitaryDischargeType.OtherThanHonorable' THEN 2 
				WHEN ci.[Key] = 'MilitaryDischargeType.Honorable' THEN 1 
				WHEN ci.[Key] = 'MilitaryDischargeType.General' THEN 3 
				WHEN ci.[Key] = 'MilitaryDischargeType.Dishonorable' THEN 5
				WHEN ci.[Key] = 'MilitaryDischargeType.BadConduct' THEN 4
				ELSE 0
			END 
FROM [dbo].[Config.CodeGroupItem] cgi
JOIN [dbo].[Config.CodeItem] ci on cgi.CodeItemId = ci.Id
JOIN [dbo].[Config.CodeGroup] cg on cgi.CodeGroupId = cg.Id
WHERE cg.[Key] = 'MilitaryDischargeTypes'
