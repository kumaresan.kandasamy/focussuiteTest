DECLARE @NextId BIGINT

IF NOT EXISTS(SELECT 1 FROM [Data.Core.ActionType] WHERE [Name] = 'SaveJob')
BEGIN
	SELECT @NextId = NextId FROM KeyTable
	
	INSERT INTO [Data.Core.ActionType] (Id, Name, ReportOn, AssistAction)
	VALUES (@NextId, 'SaveJob', 0, 0)
	
	UPDATE KeyTable SET NextId = @NextId + 1
END

IF NOT EXISTS(SELECT 1 FROM [Data.Core.EntityType] WHERE [Name] = 'Job')
BEGIN
	SELECT @NextId = NextId FROM KeyTable
	
	INSERT INTO [Data.Core.EntityType] (Id, Name)
	VALUES (@NextId, 'Job')
	
	UPDATE KeyTable SET NextId = @NextId + 1
END
