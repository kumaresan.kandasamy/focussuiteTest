IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.ResumeAddIn')
BEGIN
	CREATE TABLE [Data.Application.ResumeAddIn]
	(
		Id BIGINT NOT NULL PRIMARY KEY,
		ResumeId BIGINT NOT NULL CONSTRAINT [FK_Data.Application.ResumeAddIn_Resume] FOREIGN KEY REFERENCES [Data.Application.Resume] (Id),
		ResumeSectionType INT NOT NULL,
		[Description] NVARCHAR(MAX),
	)
	
	CREATE NONCLUSTERED INDEX [IX_Data.Application.ResumeAddIn_ResumeId]  ON [Data.Application.ResumeAddIn] (ResumeId) 
END
GO

SET NOCOUNT ON

--DROP TABLE [Data.Application.ResumeAddIn]

IF NOT EXISTS(SELECT 1 FROM [Data.Application.ResumeAddIn])
BEGIN
	CREATE TABLE TEMP_Resume
	(
		ResumeId INT,
		[Resume] XML
	)

	CREATE TABLE #ResumeAddInProfessional
	(
		ResumeId INT,
		Affiliations NVARCHAR(MAX),
		Publications NVARCHAR(MAX)
	)

	CREATE TABLE #ResumeAddInSpecial
	(
		ResumeId INT,
		Honours NVARCHAR(MAX),
		Interests NVARCHAR(MAX),
		Internships NVARCHAR(MAX),
		Personal NVARCHAR(MAX),
		TechnicalSkills NVARCHAR(MAX),
		VolunteerActivities NVARCHAR(MAX),
		Branding NVARCHAR(MAX)
	)
	
	CREATE TABLE #ResumeAddInSummary
	(
		ResumeId INT,
		Objective NVARCHAR(MAX)
	)
	
	CREATE TABLE #ResumeAddInProfessional2
	(
		ResumeId INT,
		ProfessionalDevelopment NVARCHAR(MAX)
	)

	CREATE TABLE #ResumeAddInResume
	(
		ResumeId INT,
		[References] NVARCHAR(MAX)
	)
		
	DECLARE @NextId BIGINT
	DECLARE @RowCount INT
	DECLARE @StartTime DATETIME
	DECLARE @TimeTaken INT
	
	SELECT @NextId = NextId FROM [KeyTable]

	SET @StartTime = GETDATE()
	RAISERROR ('Populating Resumes', 0, 1) WITH NOWAIT

	INSERT INTO TEMP_Resume 
	(
		ResumeId,
		[Resume]
	)
	SELECT
		R.Id,
		CAST(R.ResumeXml AS XML)
	FROM 
		dbo.[Data.Application.Resume] R

	SET @RowCount = @@ROWCOUNT
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Populating Resumes - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT

	--- Affiliations/Publications
	
	SET @StartTime = GETDATE()
	RAISERROR ('Extracting Affiliations/Publications', 0, 1) WITH NOWAIT
		
	INSERT INTO #ResumeAddInProfessional (ResumeId, Affiliations, Publications)
	SELECT
		R.ResumeId,
		professionalNode.value('affiliations[1]', 'NVARCHAR(MAX)') AS Affiliations, -- 1
		professionalNode.value('publications[1]', 'NVARCHAR(MAX)') AS Publications -- 64
	FROM
		TEMP_Resume R
	CROSS APPLY R.[Resume].nodes('/ResDoc/resume/professional') AS r1(professionalNode)
		
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Extracting Affiliations/Publications - Time Taken: %d seconds', 0, 1, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT	
	
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Affiliations', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 1, Affiliations 
	FROM #ResumeAddInProfessional 
	WHERE Affiliations IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding Affiliations - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
			
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Publications', 0, 1) WITH NOWAIT

	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 64, Publications 
	FROM #ResumeAddInProfessional 
	WHERE Publications IS NOT NULL
						
	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding Publications - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
	
	DROP TABLE #ResumeAddInProfessional

	-- Honours/Interests/Internships/Personal/TechnicalSkills/VolunteerActivities/Branding

	SET @StartTime = GETDATE()
	RAISERROR ('Extracting Honours/Interests/Internships/Personal/TechnicalSkills/VolunteerActivities/Branding', 0, 1) WITH NOWAIT
	
	INSERT INTO #ResumeAddInSpecial(ResumeId, Honours, Interests, Internships, Personal, TechnicalSkills, VolunteerActivities, Branding)
	SELECT
		R.ResumeId,
		specialNode.value('honors[1]', 'NVARCHAR(MAX)') AS Honours, -- 16,
		specialNode.value('interests[1]', 'NVARCHAR(MAX)') AS Interests, -- 128
		specialNode.value('internships[1]', 'NVARCHAR(MAX)') AS Internships, -- 2,
		specialNode.value('personal[1]', 'NVARCHAR(MAX)') AS Personal, -- 256,
		specialNode.value('technicalskills[1]', 'NVARCHAR(MAX)') AS TechnicalSkills, -- 2048
		specialNode.value('volunteeractivities[1]', 'NVARCHAR(MAX)') AS VolunteerActivities, -- 8
		specialNode.value('branding[1]', 'NVARCHAR(MAX)') AS Branding -- 8192
	FROM
		TEMP_Resume R
	CROSS APPLY R.[Resume].nodes('/ResDoc/special') AS r1(specialNode)
		
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Extracting Honours/Interests/Internships/Personal/TechnicalSkills/VolunteerActivities/Branding - Time Taken: %d seconds', 0, 1, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT	
	
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Honours', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 16, Honours 
	FROM #ResumeAddInSpecial 
	WHERE Honours IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding Honours - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT

	SET @StartTime = GETDATE()
	RAISERROR ('Adding Interests', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 128, Interests 
	FROM #ResumeAddInSpecial 
	WHERE Interests IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding Interests - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
	
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Internships', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 2, Internships 
	FROM #ResumeAddInSpecial 
	WHERE Internships IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding Internships - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
	
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Personal', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 256, Personal 
	FROM #ResumeAddInSpecial 
	WHERE Personal IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding Personal - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT	

	SET @StartTime = GETDATE()
	RAISERROR ('Adding TechnicalSkills', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 2048, TechnicalSkills 
	FROM #ResumeAddInSpecial 
	WHERE TechnicalSkills IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding TechnicalSkills - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT

	SET @StartTime = GETDATE()
	RAISERROR ('Adding VolunteerActivities', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 8, VolunteerActivities 
	FROM #ResumeAddInSpecial 
	WHERE VolunteerActivities IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding VolunteerActivities - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
		
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Branding', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 8192, Branding 
	FROM #ResumeAddInSpecial 
	WHERE Branding IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding Branding - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
		
	DROP TABLE #ResumeAddInSpecial
	
	--- Objective
	
	SET @StartTime = GETDATE()
	RAISERROR ('Extracting Objective', 0, 1) WITH NOWAIT
		
	INSERT INTO #ResumeAddInSummary (ResumeId, Objective)
	SELECT
		R.ResumeId,
		summaryNode.value('objective[1]', 'NVARCHAR(MAX)') AS Objective -- 32
	FROM
		TEMP_Resume R
	CROSS APPLY R.[Resume].nodes('/ResDoc/resume/summary') AS r1(summaryNode)
		
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Extracting Objective - Time Taken: %d seconds', 0, 1, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT	
	
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Objective', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 32, Objective 
	FROM #ResumeAddInSummary 
	WHERE Objective IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding Objective - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
			
	DROP TABLE #ResumeAddInSummary
	
	-- Professional Development
	
	SET @StartTime = GETDATE()
	RAISERROR ('Extracting ProfessionalDevelopment', 0, 1) WITH NOWAIT
		
	INSERT INTO #ResumeAddInProfessional2 (ResumeId, ProfessionalDevelopment)
	SELECT
		R.ResumeId,
		professionalNode.value('description[1]', 'NVARCHAR(MAX)') AS ProfessionalDevelopment -- 4
	FROM
		TEMP_Resume R
	CROSS APPLY R.[Resume].nodes('/ResDoc/professional') AS r1(professionalNode)
		
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Extracting Description - Time Taken: %d seconds', 0, 1, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT	
	
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Description', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 4, ProfessionalDevelopment 
	FROM #ResumeAddInProfessional2
	WHERE ProfessionalDevelopment IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding ProfessionalDevelopment - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
			
	DROP TABLE #ResumeAddInProfessional2

	-- References
	
	SET @StartTime = GETDATE()
	RAISERROR ('Extracting References', 0, 1) WITH NOWAIT
		
	INSERT INTO #ResumeAddInResume (ResumeId, [References])
	SELECT
		R.ResumeId,
		resumeNode.value('references[1]', 'NVARCHAR(MAX)') AS [References] -- 512
	FROM
		TEMP_Resume R
	CROSS APPLY R.[Resume].nodes('/ResDoc/resume') AS r1(resumeNode)
	WHERE 
		R.[Resume].exist('/ResDoc/resume/references') = 1
		
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Extracting Description - Time Taken: %d seconds', 0, 1, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT	
	
	SET @StartTime = GETDATE()
	RAISERROR ('Adding Description', 0, 1) WITH NOWAIT
		
	INSERT INTO [Data.Application.ResumeAddIn] (Id,	ResumeId, ResumeSectionType, [Description])
	SELECT @NextId + ROW_NUMBER() OVER(ORDER BY ResumeId ASC) - 1, ResumeId, 512, [References]
	FROM #ResumeAddInResume
	WHERE [References] IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @NextId = @NextId + @RowCount
	UPDATE [KeyTable] SET NextId = @NextId
	
	SET @TimeTaken = DATEDIFF(SECOND, @StartTime, GETDATE())
	RAISERROR ('Adding References - Rows %d - Time Taken: %d seconds', 0, 1, @RowCount, @TimeTaken) WITH NOWAIT
	RAISERROR ('', 0, 1) WITH NOWAIT
			
	DROP TABLE #ResumeAddInResume

	DROP TABLE TEMP_Resume
END
GO

--SELECT * FROM [Data.Application.ResumeAddIn] ORDER BY ResumeId, ResumeSectionType
