IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.JobOfficeMapper'
		AND C.name = 'JobId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.JobOfficeMapper_JobId] 
	ON 
		[Data.Application.JobOfficeMapper] (JobId)
END
GO

