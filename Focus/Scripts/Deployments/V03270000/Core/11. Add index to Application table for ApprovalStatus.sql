IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	INNER JOIN sys.index_columns IC2
		ON IC2.index_id = I.index_id
		AND IC2.object_id = T.object_id
	INNER JOIN sys.columns C2
		ON C2.column_id = IC2.column_id
		AND C2.object_id = T.object_id
	WHERE T.name = 'Data.Application.Application'
		AND C.name = 'ApprovalStatus'
		AND IC.key_ordinal = 1
		AND C2.name = 'ApplicationStatus'
		AND IC2.key_ordinal = 2
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.Application_ApprovalStatus_ApplicationStatus] 
	ON 
		[Data.Application.Application] (ApprovalStatus, ApplicationStatus) INCLUDE (PostingId)
END
GO

