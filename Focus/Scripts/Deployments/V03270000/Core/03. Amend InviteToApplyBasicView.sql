IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.InviteToApplyBasicView')
BEGIN
	DROP VIEW [Data.Application.InviteToApplyBasicView]
END
GO

CREATE VIEW [dbo].[Data.Application.InviteToApplyBasicView] 
AS
SELECT 
	ITA.Id AS Id,
	ITA.JobId AS JobId,		
	ITA.PersonId AS PersonId,
	PE.FirstName AS FirstName,
	PE.LastName AS LastName,
	R.TownCity AS Town,
	R.StateId AS StateId,
	ITA.CreatedOn AS CreatedOn,
	ISNULL([AddIn].[Description], '')AS Branding,
	R.IsContactInfoVisible AS IsContactInfoVisible,
	R.YearsExperience AS YearsExperience,
	R.NcrcLevelId AS NcrcLevelId,
	ISNULL(R.IsVeteran, 0) AS CandidateIsVeteran 
FROM [Data.Application.InviteToApply] ITA
	INNER JOIN [Data.Application.Person] AS PE ON ITA.PersonId = PE.Id
	INNER JOIN [Data.Application.Resume] AS R ON ITA.PersonId = R.PersonId
	LEFT OUTER JOIN [Data.Application.ResumeAddIn] AS [AddIn] WITH (NOLOCK) ON [AddIn].ResumeId = R.Id AND [AddIn].ResumeSectionType = 8192 -- Branding Section
WHERE 
	NOT EXISTS
	(
		SELECT 
			1 
		FROM [Data.Application.Application] AS A
		INNER JOIN [Data.Application.Resume] AS R 
			ON A.ResumeID = R.Id
		INNER JOIN [Data.Application.Posting] AS P 
			ON A.PostingId = P.Id
		WHERE P.JobId IS NOT NULL
			AND P.JobId = ITA.JobId
			AND R.PersonId = ITA.PersonId
	)
	AND R.IsDefault = 1