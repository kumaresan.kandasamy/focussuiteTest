IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'Location')
BEGIN
	ALTER TABLE
		[Data.Application.Job]
	ADD
		Location NVARCHAR(255)
END
GO

IF EXISTS(SELECT 1 FROM [Data.Application.Job] WHERE Location IS NULL)
BEGIN
	UPDATE
		J
	SET
		Location = TJL.Location
	FROM
		[Data.Application.Job] J
	CROSS APPLY
	(
		SELECT
			JL.Location,
			ROW_NUMBER() OVER (PARTITION BY JL.JobId ORDER BY JL.Id ASC) AS LocationNumber
		FROM
			[Data.Application.JobLocation] JL
		WHERE
			JL.JobId = J.Id
	) TJL
	WHERE
		TJL.LocationNumber = 1
END

