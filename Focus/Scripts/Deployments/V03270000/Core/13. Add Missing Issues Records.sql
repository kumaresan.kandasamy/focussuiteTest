DECLARE @NextID INT
DECLARE @RowCount INT

SET @RowCount = -1

WHILE @RowCount <> 0
BEGIN
	SELECT @NextID = NextId FROM [KeyTable]
	
	INSERT INTO [Data.Application.Issues] 
	(
		Id, 
		PersonId
	)
	SELECT TOP 10000 
		@NextID + ROW_NUMBER() OVER (ORDER BY PersonId ASC) - 1, 
		U.PersonId
	FROM 
		[Data.Application.User] U
	WHERE 
		U.UserType IN (4, 8, 12)
		AND NOT EXISTS(SELECT I.Id FROM [Data.Application.Issues] I WHERE I.PersonId = U.PersonId)
	ORDER BY 
		U.PersonId ASC
	
	SET @RowCount = @@ROWCOUNT
	
	UPDATE [KeyTable] SET NextId = NextId + @RowCount
END
GO