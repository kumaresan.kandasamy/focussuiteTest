IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'ResetSearchable')
BEGIN
	ALTER TABLE
		[Data.Application.Resume]
	ADD
		ResetSearchable BIT 
END
