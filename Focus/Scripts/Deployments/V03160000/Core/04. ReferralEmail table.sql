IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.ReferralEmail')
BEGIN
	CREATE TABLE [Data.Application.ReferralEmail]
	(
		Id BIGINT NOT NULL PRIMARY KEY,
		PersonId BIGINT NOT NULL,
		EmailText NVARCHAR(MAX) NOT NULL,
		EmailSentOn DATETIME NOT NULL,
		EmailSentBy BIGINT NOT NULL,
		ApprovalStatus INT NULL,
		ApplicationId BIGINT NULL,
		EmployeeId BIGINT NULL,
		FOREIGN KEY (ApplicationId) REFERENCES [Data.Application.Application](Id),
		FOREIGN KEY (EmployeeId) REFERENCES [Data.Application.Employee](Id)
	)
END
GO

IF EXISTS(SELECT 1 FROM [Data.Application.EmployeeEmail]) AND NOT EXISTS(SELECT 1 FROM [Data.Application.ReferralEmail]) 
BEGIN
	INSERT INTO [Data.Application.ReferralEmail]
	(
		Id,
		PersonId,
		EmailText,
		EmailSentOn,
		EmailSentBy,
		ApprovalStatus,
		EmployeeId
	)
	SELECT
		EE.Id,
		E.PersonId,
		EE.EmailText,
		EE.EmailSentOn,
		EE.EmailSentBy,
		EE.ApprovalStatus,
		EE.EmployeeId
	FROM
		[Data.Application.EmployeeEmail] EE
	INNER JOIN [Data.Application.Employee] E
		ON E.Id = EE.EmployeeId
END