IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Application' AND COLUMN_NAME = 'StatusLastChangedBy')
BEGIN
	ALTER TABLE
		[Data.Application.Application]
	ADD
		StatusLastChangedBy BIGINT NULL
END