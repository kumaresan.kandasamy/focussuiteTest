if not exists(select * from sys.columns where Name = N'ExternalID' and Object_ID = Object_ID(N'[Data.Application.Employer]'))
BEGIN
  ALTER TABLE [Data.Application.Employer] ADD ExternalID NVARCHAR(50) NULL
END
