IF NOT EXISTS(SELECT TOP 1 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Office' AND COLUMN_NAME = 'BusinessOutreachMailbox')
BEGIN
	ALTER TABLE
		[Data.Application.Office]
	ADD
		BusinessOutreachMailbox NVARCHAR(200)
END