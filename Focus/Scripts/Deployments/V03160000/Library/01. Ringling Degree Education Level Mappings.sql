﻿BEGIN TRANSACTION

DELETE FROM [Library.DegreeEducationLevelMapping] WHERE DegreeEducationLevelId IN (16267,16268,16269,16270,16271,16272,16273,16274,16275,16276,16277,16278,16279,16280)

INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16267,360)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16268,361)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16269,362)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16270,363)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16271,364)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16272,365)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16273,366)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16274,367)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16275,368)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16276,369)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16277,370)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16278,371)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16279,372)
INSERT INTO [Library.DegreeEducationLevelMapping] (DegreeEducationLevelId, LensId) VALUES (16280,373)

COMMIT TRANSACTION


