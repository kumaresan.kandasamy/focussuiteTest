UPDATE [dbo].[Config.EmailTemplate]
   SET [Subject] = 'Notification of WOTC preferences'
      ,[Body] = 'The following hiring manager has opted to recruit from one or more WOTC programs in Focus Talent.

#HIRINGMANAGERNAME#
#HIRINGMANAGERPHONENUMBER#
#HIRINGMANAGEREMAIL#
#JOBPOSTINGDATE#
#WOTC#

This is an automated email sent from #FOCUSTALENT#. Please do not reply directly to it.'
      ,[Salutation] = ''
      ,[Recipient] = NULL
      ,[SenderEmailType] = 0
      ,[ClientSpecificEmailAddress] = NULL
 WHERE [EmailTemplateType] = 16
GO


