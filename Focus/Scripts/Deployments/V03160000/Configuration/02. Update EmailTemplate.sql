  BEGIN TRANSACTION
  
	  UPDATE [Config.EmailTemplate]
	  SET Subject = 'Approved referral request',
	  Body = 
'The referral you have requested for the #JOBTITLE# position at #EMPLOYERNAME# has been approved.
Your resume has been forwarded to the employer for them to review.

You can track the outcome of your referral on your #FocusCareerExplorer# dashboard in the section titled "Jobs to which you''ve been referred".

Click here to sign in;
#FOCUSCAREERURL#

Regards,
#SENDERNAME#'
	  WHERE EmailTemplateType = 9
	  
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION
  