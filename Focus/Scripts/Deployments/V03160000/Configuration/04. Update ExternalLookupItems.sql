﻿if not exists(select * from sys.columns where Name = N'FocusId' and Object_ID = Object_ID(N'[Config.ExternalLookUpItem]'))
begin
    ALTER TABLE [Config.ExternalLookUpItem] ADD FocusId BIGINT NOT NULL
end

if not exists(select * from sys.columns where Name = N'ClientDataTag' and Object_ID = Object_ID(N'[Config.ExternalLookUpItem]'))
begin
    ALTER TABLE [Config.ExternalLookUpItem] ADD ClientDataTag NVARCHAR(50) NOT NULL
end