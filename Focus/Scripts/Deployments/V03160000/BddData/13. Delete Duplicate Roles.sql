DECLARE @DuplicateRoles TABLE
(
	RoleId BIGINT,
	DeleteRoleId BIGINT
)
INSERT INTO @DuplicateRoles
(
	DeleteRoleId,
	RoleId
)
SELECT
	R2.Id,
	R.Id
FROM
	[Data.Application.Role] R 
INNER JOIN [Data.Application.Role] R2
	ON R2.[Key] = R.[Key]
	AND R2.Id > R.Id
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.Role] R3
		WHERE 
			R3.[Key] = R.[Key]
			AND R3.Id < R.Id
	)

-- Delete user roles where the base role is already assigned
DELETE
	UR
FROM
	[Data.Application.UserRole] UR
INNER JOIN @DuplicateRoles DR
	ON DR.DeleteRoleId = UR.Id
WHERE
	EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.UserRole] UR2
		WHERE
			UR2.UserId = UR.UserId
			AND UR2.RoleId = DR.RoleId
	)
	
-- Update user roles where the base role is not already assigned
UPDATE
	UR
SET
	RoleId = DR.RoleId
FROM
	[Data.Application.UserRole] UR
INNER JOIN @DuplicateRoles DR
	ON DR.DeleteRoleId = UR.Id
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.UserRole] UR2
		WHERE
			UR2.UserId = UR.UserId
			AND UR2.RoleId = DR.RoleId
	)
	
-- Delete the duplicate roles
DELETE
	R
FROM
	[Data.Application.Role] R
INNER JOIN @DuplicateRoles DR
	ON DR.DeleteRoleId = R.Id