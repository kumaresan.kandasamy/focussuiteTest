DECLARE @Jobs TABLE
(
	Id BIGINT IDENTITY(1, 1),
	JobTitle NVARCHAR(40),
	EmployeeEmailAddress NVARCHAR(255),
	BusinessUnitId BIGINT,
	EmployeeId BIGINT,
	EmployeeUserId BIGINT,
	EmployerId BIGINT,
	BusinessUnitDescriptionId BIGINT,
	EmployerName NVARCHAR(255),
	JobStatus INT,
	JobWizardStep INT,
	CreatedOn DATETIME,
	ClosingDate DATETIME
)

INSERT INTO @Jobs (JobTitle, EmployeeEmailAddress, JobStatus, JobWizardStep, CreatedOn, ClosingDate)
VALUES 
	('WOTC job assigned to default office', 'dev@employer.com', 1, 6, GETDATE(), '01/01/2020'),
	('WOTC job 2 assigned to default office', 'dev@employer.com', 1, 6, GETDATE(), '01/01/2020')
	
DECLARE @NextId BIGINT

DECLARE @JobTitle NVARCHAR(50)
DECLARE @JobStatus INT
DECLARE @JobWizardStep INT
DECLARE @ClosingDate DATETIME
DECLARE @CreatedOn DATETIME

DECLARE @JobId BIGINT
DECLARE @JobAddressId BIGINT
DECLARE @JobLocationId BIGINT
DECLARE @BusinessUnitId BIGINT
DECLARE @EmployeeId BIGINT
DECLARE @EmployeeUserId BIGINT
DECLARE @EmployerId BIGINT
DECLARE @BusinessUnitDescriptionId BIGINT
DECLARE @EmployerName NVARCHAR(255)
DECLARE @PostingId BIGINT

DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @OnetId BIGINT

SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @OnetId = Id FROM [Library.Onet] WHERE OnetCode = '11-3042.00'
	
UPDATE
	TJ
SET
	BusinessUnitId = EBU.BusinessUnitId,
	EmployeeId = E.Id,
	EmployeeUserId = U.Id,
	EmployerId = E.EmployerId,
	BusinessUnitDescriptionId = BUD.Id,
	EmployerName = BU.Name
FROM
	@Jobs TJ
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = TJ.EmployeeEmailAddress
INNER JOIN [Data.Application.User] U
	ON U.PersonId = P.Id
INNER JOIN [Data.Application.Employee] E
	ON E.PersonId = P.Id
INNER JOIN [Data.Application.EmployeeBusinessUnit] EBU
	ON EBU.EmployeeId = E.Id
	AND EBU.[Default] = 1
INNER JOIN [Data.Application.BusinessUnit] BU
	ON BU.Id = EBU.BusinessUnitId
INNER JOIN [Data.Application.BusinessUnitDescription] BUD
	ON BUD.BusinessUnitId = EBU.BusinessUnitId
	AND BUD.IsPrimary = 1

DECLARE @JobCount INT
DECLARE @TotalJobs INT

SET @JobCount = 1
SELECT @TotalJobs = COUNT(1) FROM @Jobs

WHILE @JobCount <= @TotalJobs
BEGIN
	SELECT 
		@JobTitle = JobTitle,
		@JobStatus = JobStatus,
		@JobWizardStep = JobWizardStep,
		@ClosingDate = ClosingDate,
		@CreatedOn = CreatedOn,
		@BusinessUnitId = BusinessUnitId,
		@EmployeeId = EmployeeId,
		@EmployeeUserId = EmployeeUserId,
		@EmployerId = EmployerId,
		@BusinessUnitDescriptionId = BusinessUnitDescriptionId,
		@EmployerName = EmployerName
	FROM
		@Jobs
	WHERE
		Id = @JobCount
	
	IF @BusinessUnitId IS NOT NULL
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Job] WHERE JobTitle = @JobTitle AND BusinessUnitId = @BusinessUnitId)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 4
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @JobId = @NextId
		SET @JobAddressId = @NextId + 1
		SET @JobLocationId = @NextId + 2
		SET @PostingId = @NextId + 3
		
		INSERT INTO [Data.Application.Job]
		(
			Id,
			JobTitle,
			CreatedBy,
			UpdatedBy,
			ApprovalStatus,
			JobStatus,
			MinSalary,
			MaxSalary,
			SalaryFrequencyId,
			HoursPerWeek,
			OverTimeRequired,
			ClosingOn,
			NumberOfOpenings,
			PostedOn,
			PostedBy,
			[Description],
			PostingHtml,
			EmployerDescriptionPostingPosition,
			WizardStep,
			WizardPath,
			HeldOn,
			HeldBy,
			ClosedOn,
			ClosedBy,
			JobLocationType,
			FederalContractor,
			ForeignLabourCertification,
			CourtOrderedAffirmativeAction,
			FederalContractorExpiresOn,
			WorkOpportunitiesTaxCreditHires,
			PostingFlags,
			IsCommissionBased,
			NormalWorkDays,
			WorkDaysVary,
			NormalWorkShiftsId,
			LeaveBenefits,
			RetirementBenefits,
			InsuranceBenefits,
			MiscellaneousBenefits,
			OtherBenefitsDetails,
			InterviewContactPreferences,
			InterviewEmailAddress,
			InterviewApplicationUrl,
			InterviewMailAddress,
			InterviewFaxNumber,
			InterviewPhoneNumber,
			InterviewDirectApplicationDetails,
			InterviewOtherInstructions,
			ScreeningPreferences,
			MinimumEducationLevel,
			MinimumEducationLevelRequired,
			MinimumAge,
			MinimumAgeReason,
			MinimumAgeRequired,
			LicencesRequired,
			CertificationRequired,
			LanguagesRequired,
			Tasks,
			RedProfanityWords,
			YellowProfanityWords,
			EmploymentStatusId,
			JobTypeId,
			JobStatusId,
			ApprovedOn,
			ApprovedBy,
			ExternalId,
			AwaitingApprovalOn,
			MinimumExperience,
			MinimumExperienceMonths,
			MinimumExperienceRequired,
			DrivingLicenceClassId,
			DrivingLicenceRequired,
			HideSalaryOnPosting,
			ForeignLabourCertificationH2A,
			ForeignLabourCertificationH2B,
			ForeignLabourCertificationOther,
			HiringFromTaxCreditProgramNotificationSent,
			IsConfidential,
			HasCheckedCriminalRecordExclusion,
			LastPostedOn,
			LastRefreshedOn,
			MinimumAgeReasonValue,
			OtherSalary,
			HideOpeningsOnPosting,
			StudentEnrolled,
			MinimumCollegeYears,
			CreatedOn,
			UpdatedOn,
			LockVersion,
			BusinessUnitId,
			EmployeeId,
			EmployerId,
			BusinessUnitDescriptionId,
			BusinessUnitLogoId,
			ProgramsOfStudyRequired,
			StartDate,
			EndDate,
			JobType,
			OnetId,
			HideEducationOnPosting,
			HideExperienceOnPosting,
			HideMinimumAgeOnPosting,
			HideProgramOfStudyOnPosting,
			HideDriversLicenceOnPosting,
			HideLicencesOnPosting,
			HideCertificationsOnPosting,
			HideLanguagesOnPosting,
			HideSpecialRequirementsOnPosting,
			HideWorkWeekOnPosting,
			DescriptionPath,
			WorkWeekId,
			AssignedToId,
			CriminalBackgroundExclusionRequired,
			ROnetId,
			IsSalaryAndCommissionBased,
			VeteranPriorityEndDate,
			MeetsMinimumWageRequirement
		)
		VALUES
		(
			@JobId,
			@JobTitle,
			@EmployeeUserId,
			@EmployeeUserId,
			2,
			@JobStatus,
			NULL,
			NULL,
			0,
			NULL,
			0,
			@ClosingDate,
			2,
			CASE @JobStatus WHEN 0 THEN NULL ELSE @CreatedOn END,
			CASE @JobStatus WHEN 0 THEN NULL ELSE @EmployeeUserId END,
			'Test Job Description',
			'<html><body><p><b>' + @JobTitle + '</b><br>' + @EmployerName + '<br>Bluegrove, TX (76352)(public transit accessible)<br>Number of openings: 2<br>Application closing date: 22/07/2015<br></p><p>Test Job Description</p><p><b>Benefits</b><br>No benefits are offered with this job</p><p><b>About ' + @EmployerName + '</b><br>Company Description</p><p><b>How to apply</b><br><a href="http://localhost:57990" target="_blank">Log in to Focus/Career and submit your resume</a></p><p><small>REF/4760944/4761031</small></p></body>#POSTINGFOOTER#</html>',
			0,
			@JobWizardStep,
			1,
			NULL,
			NULL,
			NULL,
			NULL,
			0,
			0,
			NULL,
			0,
			NULL,
			35,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			'',
			64,
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			0,
			0,
			0,
			NULL,
			'',
			0,
			0,
			0,
			0,
			NULL,
			NULL,
			NULL,
			0,
			0,
			0,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			0,
			NULL,
			0,
			0,
			0,
			0,
			0,
			NULL,
			0,
			0,
			CASE @JobStatus WHEN 0 THEN NULL ELSE @CreatedOn END,
			NULL,
			0,
			NULL,
			NULL,
			0,
			NULL,
			@CreatedOn,
			@CreatedOn,
			10,
			@BusinessUnitId,
			@EmployeeId,
			@EmployerId,
			@BusinessUnitDescriptionId,
			NULL,
			NULL,
			NULL,
			NULL,
			1,
			@OnetId,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			NULL,
			NULL,
			0,
			NULL,
			0,
			NULL,
			1
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.JobAddress]
		(
			Id,
			JobId,
			Line1,
			Line2,
			Line3,
			TownCity,
			CountyId,
			PostcodeZip,
			StateId,
			CountryId,
			IsPrimary
		)
		VALUES
		(
			@JobAddressId,
			@JobId,
			'Address Line 1',
			'',
			'',
			'Bluegrove',
			@CountyId,
			'76352',
			@StateId,
			@CountryId,
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.JobLocation]
		(
			Id,
			JobId,
			Location,
			IsPublicTransitAccessible
		)
		VALUES
		(
			@JobLocationId,
			@JobId,
			'Bluegrove, TX (76352)',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		IF @JobStatus <> 0
		BEGIN
			INSERT INTO [Data.Application.Posting]
			(
				Id,
				JobId,
				LensPostingId,
				JobTitle,
				PostingXml,
				EmployerName,
				Url,
				ExternalId,
				StatusId,
				OriginId,
				Html,
				ViewedCount,
				CreatedOn,
				UpdatedOn
			)
			VALUES
			(
				@PostingId,
				@JobId,
				'ABCDEF_' + REPLACE(NEWID(), '-', ''),
				@JobTitle,
				'<jobposting><JobDoc><posting><duties>Test Job Description<title onet="11-3042.00">' + @JobTitle + '</title><onettitle>Training and Development Managers</onettitle></duties><contact><address><city>Bluegrove</city><majorcity>Bluegrove</majorcity><state>TX</state><postalcode>76352</postalcode></address><company>' + @EmployerName + '</company><person>Dev Employer</person></contact><jobinfo><positioncount>2</positioncount><EDUCATION_CD>0</EDUCATION_CD><JOB_TYPE>1</JOB_TYPE></jobinfo></posting><special><mode>insert</mode><originid>7</originid><jobtitle>' + @JobTitle + '</jobtitle><jobemployer>' + @EmployerName + '</jobemployer><job_status_cd>1</job_status_cd><jobtype>4</jobtype><JOBTYPE_CD>4</JOBTYPE_CD></special></JobDoc><postinghtml>&lt;html&gt;    &lt;body&gt;      &lt;p&gt;&lt;b&gt;' + @JobTitle + '&lt;/b&gt;&lt;br&gt;' + @EmployerName + '&lt;br&gt;Bluegrove, TX (76352)(public transit accessible)&lt;br&gt;Number of openings: 2&lt;br&gt;Application closing date: 22/07/2015&lt;br&gt;&lt;/p&gt;      &lt;p&gt;Test Job Description&lt;/p&gt;      &lt;p&gt;&lt;b&gt;Benefits&lt;/b&gt;&lt;br&gt;No benefits are offered with this job&lt;/p&gt;      &lt;p&gt;&lt;b&gt;About ' + @EmployerName + '&lt;/b&gt;&lt;br&gt;Company Description&lt;/p&gt;      &lt;p&gt;&lt;b&gt;How to apply&lt;/b&gt;&lt;br&gt;&lt;a href="http://localhost:57990" target="_blank"&gt;Log in to Focus/Career and submit your resume&lt;/a&gt;&lt;/p&gt;      &lt;p&gt;&lt;small&gt;REF/4760944/4761031&lt;/small&gt;&lt;/p&gt;    &lt;/body&gt;     #POSTINGFOOTER#    &lt;/html&gt;</postinghtml><REQUIREMENTS screening="-1"></REQUIREMENTS><clientjobdata><CONTACT_SEND_DIRECT_FLAG>0</CONTACT_SEND_DIRECT_FLAG><CONTACT_POSTAL_FLAG>0</CONTACT_POSTAL_FLAG><CONTACT_PHONE_FLAG>0</CONTACT_PHONE_FLAG><CONTACT_FAX_FLAG>0</CONTACT_FAX_FLAG><CONTACT_EMAIL_FLAG>0</CONTACT_EMAIL_FLAG><CONTACT_URL_FLAG>0</CONTACT_URL_FLAG><CONTACT_TALENT_FLAG>1</CONTACT_TALENT_FLAG><SAL_MIN>0.00</SAL_MIN><SAL_MAX>0.00</SAL_MAX><SAL_UNIT_CD /><JOB_CREATE_DATE>2014-07-22</JOB_CREATE_DATE><JOB_LAST_OPEN_DATE>2015-07-22</JOB_LAST_OPEN_DATE></clientjobdata></jobposting>',
				@EmployerName,
				'',
				NULL,
				1,
				7,
				'<html><body><p><b>' + @JobTitle + '</b><br>' + @EmployerName + '<br>Bluegrove, TX (76352)(public transit accessible)<br>Number of openings: 2<br>Application closing date: 22/07/2015<br></p><p>Test Job Description</p><p><b>Benefits</b><br>No benefits are offered with this job</p><p><b>About ' + @EmployerName + '</b><br>Company Description</p><p><b>How to apply</b><br><a href="http://localhost:57990" target="_blank">Log in to Focus/Career and submit your resume</a></p><p><small>REF/4760944/4761031</small></p></body>#POSTINGFOOTER#</html>',
				0,
				GETDATE(),
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
		END
		
		COMMIT TRANSACTION
	END
	
	SET @JobCount = @JobCount + 1
END

UPDATE [dbo].[Data.Application.Office]
   SET [BusinessOutreachMailbox] = 'businessoutreach@' + LOWER(REPLACE([OfficeName], ' ', '')) + '.com'
GO