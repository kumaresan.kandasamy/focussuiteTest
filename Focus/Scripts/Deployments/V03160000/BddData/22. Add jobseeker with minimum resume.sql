﻿DECLARE @JobSeekers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	EmailAddress NVARCHAR(255),
	PhoneNumber NVARCHAR(20),
	ProgamAreaId BIGINT,
	DegreeId BIGINT
)

INSERT INTO @JobSeekers (FirstName, LastName, EmailAddress, PhoneNumber, ProgamAreaId, DegreeId)
VALUES 
	('FVN-360', 'basic-resume', 'fvn360@basicresume.com', '1111111111', NULL, NULL)

DECLARE @SeekerIndex INT
DECLARE @SeekerCount INT

DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @PhoneNumberId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @IssuesId BIGINT

DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @EmailAddress NVARCHAR(255)
DECLARE @PhoneNumber NVARCHAR(20)
DECLARE @ProgamAreaId BIGINT
DECLARE @DegreeId BIGINT

DECLARE @PersonEmailAddress NVARCHAR(255)
DECLARE @ResumeName NVARCHAR(255)
DECLARE @IsDefault BIT
DECLARE @ResumeId BIGINT
DECLARE @OverrideResumeXml NVARCHAR(MAX)
DECLARE @CompletionDate DATETIME
DECLARE @IsVeteran BIT
	
SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'CareerUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

SET @SeekerIndex = 1
SELECT @SeekerCount = COUNT(1) FROM @JobSeekers

WHILE @SeekerIndex <= @SeekerCount
BEGIN
	SELECT 
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@PhoneNumber = PhoneNumber,
		@ProgamAreaId = ProgamAreaId,
		@DegreeId = DegreeId
	FROM
		@JobSeekers
	WHERE
		Id = @SeekerIndex
		
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 6
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @UserRoleId = @NextId + 4
		SET @IssuesId = @NextId + 5
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			ProgramAreaId,
			CampusId,
			DegreeId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			@ProgamAreaId,
			NULL,
			@DegreeId
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			@PhoneNumber,
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			CreatedOn,
			UpdatedOn
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			12,
			1,
			@FirstName + ' ' + @LastName,
			1,
			1,
			GETDATE(),
			GETDATE()
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		VALUES
		(
			@UserRoleId,
			@RoleId,
			@UserId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.Issues] 
		(
			Id, 
			PersonId
		)
		VALUES
		(
			@IssuesId,
			@PersonId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	
	SET @SeekerIndex = @SeekerIndex + 1
END

DECLARE @Resumes TABLE
(
	Id BIGINT IDENTITY(1, 1),
	PersonEmailAddress NVARCHAR(255),
	ResumeName NVARCHAR(255),
	IsDefault BIT,
	PersonId BIGINT,
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	OverrideResumeXml NVARCHAR(Max),
	CompletionDate DATETIME,
	IsVeteran BIT
)

INSERT INTO @Resumes (PersonEmailAddress, ResumeName, IsDefault, CompletionDate, OverrideResumeXml, IsVeteran)
VALUES 
	('fvn360@basicresume.com', 'basic resume - contact only', 1, NULL, NULL, 0)
	
UPDATE
	TR
SET
	PersonId = P.Id,
	FirstName = P.FirstName,
	LastName = P.LastName
FROM
	@Resumes TR
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = TR.PersonEmailAddress
	
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'

DECLARE @ResumeCount INT
DECLARE @TotalResumes INT

SET @ResumeCount = 1
SELECT @TotalResumes = COUNT(1) FROM @Resumes

WHILE @ResumeCount <= @TotalResumes
BEGIN
	SELECT 
		@PersonId = PersonId,
		@ResumeName = ResumeName,
		@IsDefault = IsDefault,
		@PersonEmailAddress = PersonEmailAddress,
		@FirstName = FirstName,
		@LastName = LastName,
		@OverrideResumeXml = OverrideResumeXml,
		@CompletionDate = CompletionDate,
		@IsVeteran = IsVeteran
	FROM
		@Resumes
	WHERE
		Id = @ResumeCount
		
	IF @PersonId IS NOT NULL 
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Resume] WHERE PersonId = @PersonId AND ResumeName = @ResumeName AND StatusId = 1)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 1
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @ResumeId = @NextId

		INSERT INTO [Data.Application.Resume]
		(
			Id,
			PrimaryOnet,
			PrimaryROnet,
			ResumeSkills,
			FirstName,
			LastName,
			MiddleName,
			DateOfBirth,
			Gender,
			AddressLine1,
			TownCity,
			StateId,
			CountryId,
			PostcodeZip,
			WorkPhone,
			HomePhone,
			MobilePhone,
			FaxNumber,
			EmailAddress,
			HighestEducationLevel,
			YearsExperience,
			IsVeteran,
			IsActive,
			StatusId,
			ResumeXml,
			ResumeCompletionStatusId,
			IsDefault,
			ResumeName,
			IsSearchable,
			CreatedOn,
			UpdatedOn,
			PersonId,
			SkillsAlreadyCaptured,
			CountyId,
			SkillCount,
			WordCount,
			VeteranPriorityServiceAlertsSubscription,
			EducationCompletionDate
		)
		VALUES
		(
			@ResumeId,
			'',
			'',
			'',
			@FirstName,
			@LastName,
			NULL,
			'01 Jan 1980 00:00:00.000',
			2,
			'Address Line 1',
			'Bluegrove',
			@StateId,
			@CountryId,
			'76352',
			NULL,
			'1111111111',
			NULL,
			NULL,
			@PersonEmailAddress,
			20,
			0,
			@IsVeteran,
			1,
			1,
			ISNULL(@OverrideResumeXml, '<ResDoc>
  <special />
  <resume>
    <contact>
      <name>
        <givenname>FVN-360</givenname>
        <surname>basic-resume</surname>
      </name> Home:<phone type="home">1111111111</phone><email>fvn-360@basicresume.com</email><website></website><address><street>Address Line 1</street><street></street><city>Bluegrove</city><state_county>48077</state_county><county_fullname>Clay</county_fullname><state>TX</state><state_fullname>Texas</state_fullname><postalcode>76352</postalcode><country>US</country><country_fullname>US</country_fullname></address>
			</contact>
  </resume>
</ResDoc>'),
			63,
			@IsDefault,
			@ResumeName,
			0,
			GETDATE(),
			GETDATE(),
			@PersonId,
			NULL,
			@CountyId,
			0,
			49,
			0,
			@CompletionDate
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END

		COMMIT TRANSACTION
	END
	
	SET @ResumeCount = @ResumeCount + 1
END