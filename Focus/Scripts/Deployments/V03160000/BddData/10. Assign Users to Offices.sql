DECLARE @OfficeId BIGINT
DECLARE @OfficeName NVARCHAR(255)
DECLARE @EmailAddress NVARCHAR(255)

DECLARE @Offices TABLE
(
	Id BIGINT IDENTITY(1, 1),
	EmailAddress NVARCHAR(255),
	OfficeName NVARCHAR(255)
)

INSERT INTO @Offices (EmailAddress, OfficeName)
VALUES 
	('2Offices@explorer.com', 'Office 1'),
	('2Offices@explorer.com', 'Office 2'),
	('FVN336.Office1@assist.com', 'Office 1')

DECLARE @PersonId BIGINT
DECLARE @NextId BIGINT
DECLARE @PersonOfficeMapperId BIGINT
DECLARE @PomIndex INT
DECLARE @PomCount INT

SET @PomIndex = 1
SELECT @PomCount = COUNT(1) FROM @Offices

WHILE @PomIndex <= @PomCount
BEGIN

	SELECT 
		@EmailAddress = EmailAddress,
		@OfficeName = OfficeName
	FROM
		@Offices
	WHERE
		Id = @PomIndex
		
	SELECT @OfficeId = Id FROM [Data.Application.Office] WHERE OfficeName = @OfficeName
	SELECT @PersonId = Id FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress

	IF NOT EXISTS(SELECT 1 FROM [Data.Application.PersonOfficeMapper] WHERE OfficeId = @OfficeId AND PersonId = @PersonId)
	BEGIN
		BEGIN TRANSACTION
		
			SELECT @NextId = NextId FROM KeyTable
			
			UPDATE KeyTable SET NextId = NextId + 1
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			SET @PersonOfficeMapperId = @NextId
			
			INSERT INTO [Data.Application.PersonOfficeMapper] 
			(
				Id,
				StateId, 
				PersonId,
				OfficeId,
				OfficeUnassigned,
				CreatedOn
			)
			VALUES
			(
				@PersonOfficeMapperId,
				NULL,
				@PersonId,
				@OfficeId,
				1,
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END

			COMMIT TRANSACTION
	END
	
	SET @PomIndex = @PomIndex + 1
	
END