﻿-- To set the Log Severity to Error

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'LogSeverity'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('LogSeverity','Never',1)

-- To change On Error Email

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'OnErrorEmail'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('OnErrorEmail','',1)

-- To disable Emails

DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'EmailsEnabled'

INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
VALUES
('EmailsEnabled','False',1)