DECLARE @Roles TABLE
(
	Id INT IDENTITY(1, 1),
	UserName NVARCHAR(255),
	RoleKey  NVARCHAR(255)
)
INSERT INTO @Roles (UserName, RoleKey)
VALUES ('dev@client.com', 'AssistOfficesCreate')

DECLARE @NextId BIGINT
SELECT @NextId = NextId FROM KeyTable

INSERT INTO [Data.Application.UserRole] 
(
	Id,
	RoleId, 
	UserId
)
SELECT 
	@NextId + TR.Id - 1,
	R.Id,
	U.Id
FROM 
	@Roles TR
INNER JOIN [Data.Application.User] U
	ON U.UserName = TR.UserName
INNER JOIN [Data.Application.Role] R
	ON R.[Key] = TR.RoleKey
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.UserRole] UR
		WHERE
			UR.UserId = U.Id
			AND UR.RoleId = R.Id
	)

UPDATE KeyTable SET NextId = NextId + @@ROWCOUNT

