DECLARE @NextId BIGINT

DECLARE @SessionId NVARCHAR(200)
SET @SessionId = '8A5447E7-5527-4E3C-B832-E504FE75D000'

DECLARE @RequestId NVARCHAR(200)
SET @RequestId = '28147988-3E2B-4884-83D0-94D1FFE7B643'

DECLARE @DevClientUserId BIGINT
DECLARE @UserId BIGINT

SELECT @DevClientUserId = Id FROM [Data.Application.User] WHERE UserName = 'dev@client.com'

DECLARE @EmployeeId BIGINT

SELECT 
	@EmployeeId = e.Id 
FROM 
	[Data.Application.Employee] e
	JOIN [Data.Application.User] u ON e.PersonId = u.PersonId 
WHERE 
	u.UserName = 'fvn174-3@employer.com'

IF NOT EXISTS(SELECT 1 FROM [Data.Core.ActionType] WHERE Name = 'AssignActivityToEmployee')
BEGIN
	BEGIN TRANSACTION
		SELECT @NextId = NextId FROM KeyTable
				
		UPDATE KeyTable SET NextId = NextId + 1
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Core.ActionType]
		(
			Id,
			Name,
			ReportOn,
			AssistAction
		)
		VALUES
		(
			@NextId,
			'AssignActivityToEmployee',
			0,
			0
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
END

DECLARE @AssignActivityToEmployeeActionTypeId BIGINT
SELECT @AssignActivityToEmployeeActionTypeId = Id FROM [Data.Core.ActionType] WHERE Name = 'AssignActivityToEmployee'

IF NOT EXISTS(SELECT 1 FROM [Data.Core.EntityType] WHERE Name = 'Employee')
BEGIN
	BEGIN TRANSACTION
		SELECT @NextId = NextId FROM KeyTable
				
		UPDATE KeyTable SET NextId = NextId + 1
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Core.EntityType]
		(
			Id,
			Name
		)
		VALUES
		(
			@NextId,
			'Employee'
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
END

DECLARE @EntityTypeId BIGINT
SELECT @EntityTypeId = Id FROM [Data.Core.EntityType] WHERE Name = 'Employee'

DECLARE @Activities TABLE
(
	Id BIGINT IDENTITY(1, 1),
	ActionTypeId BIGINT,
	ActivityId BIGINT,
	UserId BIGINT,
	EmployeePersonId BIGINT
)

INSERT INTO @Activities 
	(ActionTypeId,
	ActivityId,
	UserId,
	EmployeePersonId)
SELECT 
	@AssignActivityToEmployeeActionTypeId,  
	a.Id, 
	@DevClientUserId,
	@EmployeeId
FROM [Config.Activity] a
  JOIN [Config.ActivityCategory] ac ON a.ActivityCategoryId = ac.Id
  WHERE ac.ActivityType = 1

BEGIN TRANSACTION 
	SELECT @NextId = NextId FROM KeyTable

	INSERT INTO [Data.Core.ActionEvent]
	(	Id,
		SessionId,
		RequestId,
		UserId,
		ActionedOn,
		EntityId,
		EntityIdAdditional01,
		EntityIdAdditional02,
		AdditionalDetails,
		ActionTypeId,
		EntityTypeId
	) 
	SELECT 
		@NextId + Id,
		@SessionId,
		@RequestId,
		UserId,
		GETDATE(),
		EmployeePersonId,
		ActivityId,
		NULL,
		NULL,
		ActionTypeId,
		@EntityTypeId
	FROM
		@Activities 
		
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	SELECT @NextId = Max(Id) FROM [Data.Core.ActionEvent]
	
	UPDATE KeyTable SET NextId = @NextId + 1
		
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION