DECLARE @PersonId BIGINT
SELECT @PersonId = Id FROM [Data.Application.Person] WHERE EmailAddress = 'FVN336.Office1@assist.com'

BEGIN TRANSACTION
	UPDATE 
		[Data.Application.Person]
	SET 
		AssignedToId = @PersonId
	WHERE 
		EmailAddress = 'office1@jobseeker.com'
	
	COMMIT TRANSACTION