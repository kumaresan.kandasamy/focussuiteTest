﻿DECLARE @JobSeekers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	EmailAddress NVARCHAR(255),
	PhoneNumber NVARCHAR(20),
	ProgamAreaId BIGINT,
	DegreeId BIGINT
)

INSERT INTO @JobSeekers (FirstName, LastName, EmailAddress, PhoneNumber, ProgamAreaId, DegreeId)
VALUES 
	('FVN-362', 'completed-resume', 'fvn362@completedresume.com', '1111111111', NULL, NULL)

DECLARE @SeekerIndex INT
DECLARE @SeekerCount INT

DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @PhoneNumberId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @IssuesId BIGINT

DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @EmailAddress NVARCHAR(255)
DECLARE @PhoneNumber NVARCHAR(20)
DECLARE @ProgamAreaId BIGINT
DECLARE @DegreeId BIGINT

DECLARE @PersonEmailAddress NVARCHAR(255)
DECLARE @ResumeName NVARCHAR(255)
DECLARE @IsDefault BIT
DECLARE @ResumeId BIGINT
DECLARE @OverrideResumeXml NVARCHAR(MAX)
DECLARE @CompletionDate DATETIME
DECLARE @IsVeteran BIT

DECLARE @pResumeXml NVARCHAR(MAX)
DECLARE @ResumeXML xml = '<ResDoc>
  <special />
  <resume>
    <contact>
      <name>
        <givenname>FVN-362</givenname>
        <surname>completed-resume</surname>
      </name> Home:<phone type="home">1111111111</phone><email>fvn362@completedresume.com</email><website></website><address><street>Address Line 1</street><street></street><city>Bluegrove</city><state_county>48077</state_county><county_fullname>Clay</county_fullname><state>TX</state><state_fullname>Texas</state_fullname><postalcode>76352</postalcode><country>US</country><country_fullname>US</country_fullname></address></contact>
    <experience>
      <employment_status_cd>1</employment_status_cd>
      <job>
        <jobid>c4b2b55b-ce0a-4175-bc80-e77cf1557979</jobid>
        <employer>Soft Dev</employer>
        <title>Soft Dev</title>
        <description>* Studied legal records to establish boundaries of local, national, and international properties.
* Knowledge sets include: dev.</description>
        <address>
          <city>Boston</city>
          <state>MA</state>
          <state_fullname>Massachusetts</state_fullname>
          <country>US</country>
          <country_fullname>United States</country_fullname>
        </address>
        <daterange>
          <start>01-2003</start>
          <end currentjob="1">08-2014</end>
        </daterange>
      </job>
      <months_experience>139</months_experience>
    </experience>
    <education>
      <school_status_cd>3</school_status_cd>
      <norm_edu_level_cd>14</norm_edu_level_cd>
      <school id="2">
        <institution>Big school</institution>
        <completiondate>01/2003</completiondate>
        <degree>Associate in Science (AS)</degree>
        <major>Physical Science</major>
        <address>
          <state>MA</state>
          <state_fullname>Massachusetts</state_fullname>
          <country>US</country>
          <country_fullname>United States</country_fullname>
        </address>
        <gpa></gpa>
      </school>
    </education>
    <header>
      <default>1</default>
      <buildmethod>1</buildmethod>

      <onet>15-1131.00</onet>
      <ronet>15-1131.00</ronet>
      <status>0</status>
      <completionstatus>127</completionstatus>
      <updatedon>20/08/2014 14:58:25</updatedon>
    </header>
    <statements>
      <personal>
        <sex>3</sex>
        <ethnic_heritages>
          <ethnic_heritage>
            <ethnic_id>3</ethnic_id>
            <selection_flag>-1</selection_flag>
          </ethnic_heritage>
          <ethnic_heritage>
            <ethnic_id>4</ethnic_id>
            <selection_flag>0</selection_flag>
          </ethnic_heritage>
          <ethnic_heritage>
            <ethnic_id>5</ethnic_id>
            <selection_flag>0</selection_flag>
          </ethnic_heritage>
          <ethnic_heritage>
            <ethnic_id>2</ethnic_id>
            <selection_flag>0</selection_flag>
          </ethnic_heritage>
          <ethnic_heritage>
            <ethnic_id>6</ethnic_id>
            <selection_flag>0</selection_flag>
          </ethnic_heritage>
          <ethnic_heritage>
            <ethnic_id>1</ethnic_id>
            <selection_flag>0</selection_flag>
          </ethnic_heritage>
          <ethnic_heritage>
            <ethnic_id>-1</ethnic_id>
            <selection_flag>-1</selection_flag>
          </ethnic_heritage>
        </ethnic_heritages>
        <citizen_flag>-1</citizen_flag>
        <disability_status>1</disability_status>
        <license>
          <driver_flag>0</driver_flag>
        </license>
        <veteran>
          <vet_flag>0</vet_flag>
          <subscribe_vps>0</subscribe_vps>
        </veteran>
        <preferences>
          <postings_interested_in>0</postings_interested_in>
          <shift />
        </preferences>
      </personal>
    </statements>
    <summary>
      <summary include="1">I have 12 years of experience, including as a Soft Dev.

Most recently, I have been working as a Soft Dev at Soft Dev from January 2003 to August 2014.

I hold an Associate in Science (AS) degree in Physical Science from Big school.</summary>
    </summary>
    <skills>
      <internship_skills />
    </skills>
    <skillrollup />
    <hide_options>
      <hide_daterange>0</hide_daterange>
      <hide_eduDates>0</hide_eduDates>
      <hide_militaryserviceDates>0</hide_militaryserviceDates>
      <hide_workDates>0</hide_workDates>
      <hide_contact>0</hide_contact>
      <hide_name>0</hide_name>
      <hide_email>0</hide_email>
      <hide_homePhoneNumber>0</hide_homePhoneNumber>
      <hide_workPhoneNumber>0</hide_workPhoneNumber>
      <hide_cellPhoneNumber>0</hide_cellPhoneNumber>
      <hide_faxPhoneNumber>0</hide_faxPhoneNumber>
      <hide_nonUSPhoneNumber>0</hide_nonUSPhoneNumber>
      <hide_affiliations>0</hide_affiliations>
      <hide_certifications_professionallicenses>0</hide_certifications_professionallicenses>
      <hide_employer>0</hide_employer>
      <hide_driver_license>0</hide_driver_license>
      <unhide_driver_license>0</unhide_driver_license>
      <hide_honors>0</hide_honors>
      <hide_interests>0</hide_interests>
      <hide_internships>0</hide_internships>
      <hide_languages>0</hide_languages>
      <hide_veteran>0</hide_veteran>
      <hide_objective>0</hide_objective>
      <hide_personalinformation>0</hide_personalinformation>
      <hide_professionaldevelopment>0</hide_professionaldevelopment>
      <hide_publications>0</hide_publications>
      <hide_references>0</hide_references>
      <hide_skills>0</hide_skills>
      <hide_technicalskills>0</hide_technicalskills>
      <hide_volunteeractivities>0</hide_volunteeractivities>
      <hide_thesismajorprojects>0</hide_thesismajorprojects>
      <hidden_skills></hidden_skills>
    </hide_options>
  </resume>
</ResDoc>'
	
SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'CareerUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

SET @SeekerIndex = 1
SELECT @SeekerCount = COUNT(1) FROM @JobSeekers

WHILE @SeekerIndex <= @SeekerCount
BEGIN
	SELECT 
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@PhoneNumber = PhoneNumber,
		@ProgamAreaId = ProgamAreaId,
		@DegreeId = DegreeId
	FROM
		@JobSeekers
	WHERE
		Id = @SeekerIndex
		
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 6
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @UserRoleId = @NextId + 4
		SET @IssuesId = @NextId + 5
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			ProgramAreaId,
			CampusId,
			DegreeId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			@ProgamAreaId,
			NULL,
			@DegreeId
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			@PhoneNumber,
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			CreatedOn,
			UpdatedOn
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			12,
			1,
			@FirstName + ' ' + @LastName,
			1,
			1,
			GETDATE(),
			GETDATE()
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		VALUES
		(
			@UserRoleId,
			@RoleId,
			@UserId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.Issues] 
		(
			Id, 
			PersonId
		)
		VALUES
		(
			@IssuesId,
			@PersonId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	
	SET @SeekerIndex = @SeekerIndex + 1
END

DECLARE @Resumes TABLE
(
	Id BIGINT IDENTITY(1, 1),
	PersonEmailAddress NVARCHAR(255),
	ResumeName NVARCHAR(255),
	IsDefault BIT,
	PersonId BIGINT,
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	OverrideResumeXml NVARCHAR(Max),
	CompletionDate DATETIME,
	IsVeteran BIT
)

INSERT INTO @Resumes (PersonEmailAddress, ResumeName, IsDefault, CompletionDate, OverrideResumeXml, IsVeteran)
VALUES 
	('fvn362@completedresume.com', 'completed resume', 1, NULL, NULL, 0)
	
UPDATE
	TR
SET
	PersonId = P.Id,
	FirstName = P.FirstName,
	LastName = P.LastName
FROM
	@Resumes TR
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = TR.PersonEmailAddress
	
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'

DECLARE @ResumeCount INT
DECLARE @TotalResumes INT

SET @ResumeCount = 1
SELECT @TotalResumes = COUNT(1) FROM @Resumes

WHILE @ResumeCount <= @TotalResumes
BEGIN
	SELECT 
		@PersonId = PersonId,
		@ResumeName = ResumeName,
		@IsDefault = IsDefault,
		@PersonEmailAddress = PersonEmailAddress,
		@FirstName = FirstName,
		@LastName = LastName,
		@OverrideResumeXml = OverrideResumeXml,
		@CompletionDate = CompletionDate,
		@IsVeteran = IsVeteran
	FROM
		@Resumes
	WHERE
		Id = @ResumeCount
		
	IF @PersonId IS NOT NULL 
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Resume] WHERE PersonId = @PersonId AND ResumeName = @ResumeName AND StatusId = 1)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 1
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END

		SET @ResumeId = @NextId
		SET @ResumeXml.modify('insert <resumeid>{sql:variable("@ResumeId")}</resumeid>
							  into (/ResDoc/resume/header)[1]')
						
		SET @pResumeXml = Convert(NVARCHAR(MAX),@ResumeXml)

		INSERT INTO [Data.Application.Resume]
		(
			Id,
			PrimaryOnet,
			PrimaryROnet,
			ResumeSkills,
			FirstName,
			LastName,
			MiddleName,
			DateOfBirth,
			Gender,
			AddressLine1,
			TownCity,
			StateId,
			CountryId,
			PostcodeZip,
			WorkPhone,
			HomePhone,
			MobilePhone,
			FaxNumber,
			EmailAddress,
			HighestEducationLevel,
			YearsExperience,
			IsVeteran,
			IsActive,
			StatusId,
			ResumeXml,
			ResumeCompletionStatusId,
			IsDefault,
			ResumeName,
			IsSearchable,
			CreatedOn,
			UpdatedOn,
			PersonId,
			SkillsAlreadyCaptured,
			CountyId,
			SkillCount,
			WordCount,
			VeteranPriorityServiceAlertsSubscription,
			EducationCompletionDate
		)
		VALUES
		(
			@ResumeId,
			'',
			'',
			'',
			@FirstName,
			@LastName,
			NULL,
			'01 Jan 1980 00:00:00.000',
			2,
			'Address Line 1',
			'Bluegrove',
			@StateId,
			@CountryId,
			'76352',
			NULL,
			'1111111111',
			NULL,
			NULL,
			@PersonEmailAddress,
			20,
			0,
			@IsVeteran,
			1,
			1,
			ISNULL(@OverrideResumeXml,@pResumeXml),
			127,
			@IsDefault,
			@ResumeName+ ' ' + Convert(NVARCHAR,@ResumeCount),
			0,
			GETDATE(),
			GETDATE(),
			@PersonId,
			NULL,
			@CountyId,
			0,
			49,
			0,
			@CompletionDate
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END

		COMMIT TRANSACTION
	END
	
	SET @ResumeCount = @ResumeCount + 1
END