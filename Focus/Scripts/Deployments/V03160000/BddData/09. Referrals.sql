DECLARE @Referrals TABLE
(
	Id BIGINT IDENTITY(1, 1),
	ResumeName NVARCHAR(255),
	EmailAddress NVARCHAR(255),
	JobTitle NVARCHAR(255),
	BusinessUnitName NVARCHAR(255),
	ResumeId BIGINT,
	PostingId BIGINT,
	ApprovalStatus INT
)

INSERT INTO @Referrals (ResumeName, EmailAddress, JobTitle, BusinessUnitName, ApprovalStatus)
VALUES 
	('Job seeker assigned to Office 1 resume', 'office1@jobseeker.com', 'Test Developer 1', 'Test Employer 1', 1),
	('Job seeker assigned to 2 offices resume', '2Offices@explorer.com', 'Test Developer 1', 'Test Employer 1', 1),
	('Job seeker referral denied resume', 'Referral-Denied@explorer.com', 'Test Developer 1', 'Test Employer 1', 3)

UPDATE
	TR
SET
	ResumeId = R.Id,
	PostingId = PO.Id
FROM
	@Referrals TR
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = TR.EmailAddress
INNER JOIN [Data.Application.Resume] R
	ON R.PersonId = P.Id
	AND R.ResumeName = TR.ResumeName
	AND R.StatusId = 1
INNER JOIN [Data.Application.BusinessUnit] BU
	ON BU.Name = TR.BusinessUnitName
INNER JOIN [Data.Application.Job] J
	ON J.JobTitle = TR.JobTitle
	AND J.BusinessUnitId = BU.Id
INNER JOIN [Data.Application.Posting] PO
	ON PO.JobId = J.Id
	
DECLARE @ReferralIndex INT
DECLARE @ReferralCount INT
DECLARE @ResumeId BIGINT
DECLARE @PostingId BIGINT
DECLARE @ReferralId BIGINT
DECLARE @StatusLogId BIGINT
DECLARE @ApprovalStatus INT
DECLARE @NextId BIGINT
DECLARE @EntityTypeId BIGINT
	
SET @ReferralIndex = 1
SELECT @ReferralCount = COUNT(1) FROM @Referrals

SELECT
	@EntityTypeId = Id
FROM
	[Data.Core.EntityType]
WHERE
	Name = 'Application'
	
IF @EntityTypeId IS NULL
BEGIN
		SELECT @EntityTypeId = NextId FROM KeyTable
		UPDATE KeyTable SET NextId = NextId + 1
		INSERT INTO [Data.Core.EntityType](Id,Name) VALUES(@EntityTypeId, 'Application')
END

WHILE @ReferralIndex <= @ReferralCount
BEGIN
	SELECT 
		@ResumeId = ResumeId,
		@PostingId = PostingId,
		@ApprovalStatus = ApprovalStatus
	FROM
		@Referrals
	WHERE
		Id = @ReferralIndex
		
	IF @ResumeId IS NOT NULL AND @PostingId IS NOT NULL 
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Application] WHERE ResumeId = @ResumeId AND PostingId = @PostingId)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 2
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @ReferralId = @NextId
		SET @StatusLogId = @NextId + 1

		INSERT INTO [Data.Application.Application]
		(
			Id,
			ApprovalStatus,
			ApprovalRequiredReason,
			ApplicationStatus,
			ApplicationScore,
			StatusLastChangedOn,
			CreatedOn,
			UpdatedOn,
			ResumeId,
			PostingId,
			Viewed,
			PostHireFollowUpStatus,
			AutomaticallyApproved
		)
		VALUES
		(
			@ReferralId,
			@ApprovalStatus,
			'All referrals are approved automatically',
			1,
			25,
			GETDATE(),
			GETDATE(),
			GETDATE(),
			@ResumeId,
			@PostingId,
			1,
			NULL,
			CASE @ApprovalStatus WHEN 2 THEN 1 ELSE 0 END
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Core.StatusLog]
		(
			Id,
			EntityId,
			OriginalStatus,
			NewStatus,
			UserId,
			ActionedOn,
			EntityTypeId
		)
		VALUES
		(
			@StatusLogId,
			@ReferralId,
			0,
			1,
			0,
			GETDATE(),
			@EntityTypeId
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	
	SET @ReferralIndex = @ReferralIndex + 1
END
