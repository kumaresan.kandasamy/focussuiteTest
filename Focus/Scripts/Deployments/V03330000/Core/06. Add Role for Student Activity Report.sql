DECLARE @NextID INT
DECLARE @RoleKey NVARCHAR(255)
DECLARE @RoleDesc NVARCHAR(255)

SET @RoleKey = 'AssistStudentActivityReport'
SET @RoleDesc = 'Assist Student Activity Report'

IF NOT EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = @RoleKey)
BEGIN
	SELECT @NextID = NextId FROM [dbo].[KeyTable]
	
	INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
	VALUES (@NextID, @RoleKey, @RoleDesc)
	
	UPDATE [KeyTable] SET NextId = NextId + 1
END