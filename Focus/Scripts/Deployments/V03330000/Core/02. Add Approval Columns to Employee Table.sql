IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Employee' AND COLUMN_NAME = 'ApprovalStatusReason')
BEGIN
	ALTER TABLE
		[Data.Application.Employee]
	ADD
		ApprovalStatusReason INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Employee' AND COLUMN_NAME = 'AwaitingApprovalDate')
BEGIN
	ALTER TABLE
		[Data.Application.Employee]
	ADD
		AwaitingApprovalDate DATETIME NULL
END
GO