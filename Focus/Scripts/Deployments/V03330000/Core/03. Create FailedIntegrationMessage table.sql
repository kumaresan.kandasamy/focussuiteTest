﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.FailedIntegrationMessage')
BEGIN
	CREATE TABLE [Data.Application.FailedIntegrationMessage] (
		Id BIGINT NOT NULL PRIMARY KEY,
		[DateSubmitted] DATETIME NOT NULL,
		[MessageType] VARCHAR(127) NOT NULL,
		[ErrorDescription] NVARCHAR(2047) NOT NULL,
		[Request] NVARCHAR(MAX) NOT NULL,
    [IsIgnored] BIT NOT NULL,
    [DateResent] DATETIME NULL )
END

