IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnitDescription' AND COLUMN_NAME = 'RedProfanityWords')
BEGIN
	ALTER TABLE
		[Data.Application.BusinessUnitDescription]
	ADD
		RedProfanityWords NVARCHAR(1000) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnitDescription' AND COLUMN_NAME = 'YellowProfanityWords')
BEGIN
	ALTER TABLE
		[Data.Application.BusinessUnitDescription]
	ADD
		YellowProfanityWords NVARCHAR(1000) NULL
END
GO
