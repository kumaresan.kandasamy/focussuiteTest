IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnit' AND COLUMN_NAME = 'ApprovalStatusReason')
BEGIN
	ALTER TABLE
		[Data.Application.BusinessUnit]
	ADD
		ApprovalStatusReason INT NOT NULL DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnit' AND COLUMN_NAME = 'AwaitingApprovalDate')
BEGIN
	ALTER TABLE
		[Data.Application.BusinessUnit]
	ADD
		AwaitingApprovalDate DATETIME NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnit' AND COLUMN_NAME = 'RedProfanityWords')
BEGIN
	ALTER TABLE
		[Data.Application.BusinessUnit]
	ADD
		RedProfanityWords NVARCHAR(1000) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnit' AND COLUMN_NAME = 'YellowProfanityWords')
BEGIN
	ALTER TABLE
		[Data.Application.BusinessUnit]
	ADD
		YellowProfanityWords NVARCHAR(1000) NULL
END
GO
