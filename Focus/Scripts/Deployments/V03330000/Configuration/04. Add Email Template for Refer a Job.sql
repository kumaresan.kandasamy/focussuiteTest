﻿IF NOT EXISTS(SELECT 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 42)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType,
		[Subject],
		Body,
		Salutation,
		SenderEmailType
	)
	VALUES
	(
		42,
		'Job recommendation from #FOCUSASSIST#',
		'You are a great match for the following posting:
#JOBTITLE# at #EMPLOYERNAME#
You should consider making an application. Click on the following link to view the posting within your #FOCUSCAREER# account.
#JOBLINKURL#
Best regards,
#SENDERNAME#',
		'Dear #RECIPIENTNAME#',
		1
	)
END
