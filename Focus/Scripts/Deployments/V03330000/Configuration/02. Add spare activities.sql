IF NOT EXISTS (SELECT * FROM [dbo].[Config.ActivityCategory] WHERE [LocalisationKey] = 'SpareActivityCategory.1')
BEGIN
	INSERT INTO [dbo].[Config.ActivityCategory]
			   ([LocalisationKey]
			   ,[Name]
			   ,[Visible]
			   ,[Administrable]
			   ,[ActivityType]
			   ,[Editable])
		VALUES
			   ('SpareActivityCategory.1', 'zzSpareCategory1', 0, 1, 0, 1),
			   ('SpareActivityCategory.2', 'zzSpareCategory2', 0, 1, 0, 1),
			   ('SpareActivityCategory.3', 'zzSpareCategory3', 0, 1, 0, 1),
			   ('SpareActivityCategory.4', 'zzSpareCategory4', 0, 1, 0, 1),
			   ('SpareActivityCategory.5', 'zzSpareCategory5', 0, 1, 0, 1)

	DECLARE @CustomActivityCategory1Id as int
	DECLARE @CustomActivityCategory2Id as int
	DECLARE @CustomActivityCategory3Id as int
	DECLARE @CustomActivityCategory4Id as int
	DECLARE @CustomActivityCategory5Id as int

	SELECT @CustomActivityCategory1Id = Id
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'SpareActivityCategory.1'
	SELECT @CustomActivityCategory2Id = Id							  
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'SpareActivityCategory.2'
	SELECT @CustomActivityCategory3Id = Id							  
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'SpareActivityCategory.3'
	SELECT @CustomActivityCategory4Id = Id							  
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'SpareActivityCategory.4'
	SELECT @CustomActivityCategory5Id = Id							  
		FROM [dbo].[Config.ActivityCategory] where LocalisationKey = 'SpareActivityCategory.5'

	INSERT INTO [dbo].[Config.Activity]
			   ([LocalisationKey]
			   ,[Name]
			   ,[Visible]
			   ,[Administrable]
			   ,[SortOrder]
			   ,[Used]
			   ,[ExternalId]
			   ,[ActivityCategoryId])
		 VALUES
			   ('SpareActivity.1','',0,1,0,0,0,@CustomActivityCategory1Id),
			   ('SpareActivity.2','',0,1,0,0,0,@CustomActivityCategory1Id),
			   ('SpareActivity.3','',0,1,0,0,0,@CustomActivityCategory1Id),
			   ('SpareActivity.4','',0,1,0,0,0,@CustomActivityCategory1Id),
			   ('SpareActivity.5','',0,1,0,0,0,@CustomActivityCategory1Id),
			 
			   ('SpareActivity.6','',0,1,0,0,0,@CustomActivityCategory2Id),
			   ('SpareActivity.7','',0,1,0,0,0,@CustomActivityCategory2Id),
			   ('SpareActivity.8','',0,1,0,0,0,@CustomActivityCategory2Id),
			   ('SpareActivity.9','',0,1,0,0,0,@CustomActivityCategory2Id),
			   ('SpareActivity.10','',0,1,0,0,0,@CustomActivityCategory2Id),
			 
			   ('SpareActivity.11','',0,1,0,0,0,@CustomActivityCategory3Id),
			   ('SpareActivity.12','',0,1,0,0,0,@CustomActivityCategory3Id),
			   ('SpareActivity.13','',0,1,0,0,0,@CustomActivityCategory3Id),
			   ('SpareActivity.14','',0,1,0,0,0,@CustomActivityCategory3Id),
			   ('SpareActivity.15','',0,1,0,0,0,@CustomActivityCategory3Id),
			 
			   ('SpareActivity.16','',0,1,0,0,0,@CustomActivityCategory4Id),
			   ('SpareActivity.17','',0,1,0,0,0,@CustomActivityCategory4Id),
			   ('SpareActivity.18','',0,1,0,0,0,@CustomActivityCategory4Id),
			   ('SpareActivity.19','',0,1,0,0,0,@CustomActivityCategory4Id),
			   ('SpareActivity.20','',0,1,0,0,0,@CustomActivityCategory4Id),
			 
			   ('SpareActivity.21','',0,1,0,0,0,@CustomActivityCategory5Id),
			   ('SpareActivity.22','',0,1,0,0,0,@CustomActivityCategory5Id),
			   ('SpareActivity.23','',0,1,0,0,0,@CustomActivityCategory5Id),
			   ('SpareActivity.24','',0,1,0,0,0,@CustomActivityCategory5Id),
			   ('SpareActivity.25','',0,1,0,0,0,@CustomActivityCategory5Id)
END



