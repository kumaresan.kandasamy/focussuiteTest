
if not exists(select * from INFORMATION_SCHEMA.columns
            where COLUMN_NAME = N'Editable' and TABLE_NAME = N'Config.ActivityCategory')
begin
    ALTER TABLE dbo.[Config.ActivityCategory]
	ADD	Editable bit NOT NULL
	CONSTRAINT EditableDefaultConstraint DEFAULT 0
end

IF EXISTS(select * FROM sys.views where name = 'Config.ActivityView')
BEGIN
	DROP VIEW [dbo].[Config.ActivityView]
END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[Config.ActivityView] as  
SELECT 
	a.Id,
	ac.Id as ActivityCategoryId,
	ac.LocalisationKey as CategoryLocalisationKey,
	ac.Name as CategoryName,
	ac.visible as CategoryVisible,
	ac.Administrable as CategoryAdministrable,  
	ac.ActivityType,
	a.LocalisationKey as ActivityLocalisationKey,
	a.Name as ActivityName,
	a.visible as ActivityVisible,
	a.Administrable as ActivityAdministrable,
	a.SortOrder,
	a.ExternalId as ActivityExternalId,
	ac.Editable
FROM
	[Config.ActivityCategory] ac WITH (NOLOCK) 
	INNER JOIN [Config.Activity] a WITH (NOLOCK) on ac.id = a.activitycategoryid
GO