IF NOT EXISTS (SELECT 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 41)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient,
		SenderEmailType
	)
	VALUES 
	(
		41, 
		'Student activity report', 
		'Please find attached your student activity report for the following reporting period; 

#REPORTFROMDATE# to #REPORTTODATE#', 
		NULL, 
		NULL,
		1
)
END