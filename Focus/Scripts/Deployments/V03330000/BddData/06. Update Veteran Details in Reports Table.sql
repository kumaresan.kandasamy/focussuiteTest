;WITH JobSeekerRows AS
(
	SELECT
		ROW_NUMBER() OVER (ORDER BY Id ASC) - 1 AS RowIndex,
		Id
	FROM
		[Report.JobSeeker]
)
UPDATE 
	JS
SET
	VeteranType = R.RowIndex % 3 + 2,
	VeteranTransitionType = R.RowIndex % 3 + 1,
	VeteranMilitaryDischarge = R.RowIndex % 5 + 1,
	VeteranBranchOfService = R.RowIndex % 5 + 1
FROM
	[Report.JobSeeker] JS
INNER JOIN JobSeekerRows R
	ON R.Id = JS.Id
