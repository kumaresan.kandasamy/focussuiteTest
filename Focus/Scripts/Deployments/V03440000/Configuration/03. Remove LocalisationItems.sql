﻿DELETE FROM [Config.LocalisationItem] 
WHERE [Key] = 'Global.EmploymentType.Workforce' AND ([Value] = 'Job post' OR [Value] = 'Job posting' OR [Value] = 'Job order')

GO

DELETE FROM [Config.LocalisationItem] 
WHERE [Key] = 'Global.EmploymentTypes.Workforce' AND ([Value] = 'Job posts' OR [Value] = 'Job postings' OR [Value] = 'Job orders')

GO

DELETE FROM [Config.LocalisationItem] 
WHERE [Key] = 'Global.Businesses' AND ([Value] = 'Businesses' OR [Value] = 'Employers')

GO

DELETE FROM [Config.LocalisationItem] 
WHERE [Key] = 'Global.Business' AND ([Value] = 'Business' OR [Value] = 'Employer')

GO