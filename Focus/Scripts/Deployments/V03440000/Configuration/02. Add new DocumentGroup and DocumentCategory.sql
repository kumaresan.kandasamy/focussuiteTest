DECLARE @Lookups TABLE
(
	[Key] NVARCHAR(50),
	LookupType NVARCHAR(50),
	DisplayOrder INT,
	Value NVARCHAR(255)
)

DECLARE @Parents TABLE
(
	ChildKey NVARCHAR(50),
	ParentKey NVARCHAR(50)
)

DECLARE @LocalisationId BIGINT

SELECT 
	@LocalisationId = L.Id
FROM 
	[Config.Localisation] L
WHERE 
	L.Culture = '**-**'
	
INSERT INTO @Lookups ([Key], LookupType, DisplayOrder, Value)
VALUES 
	('DocumentCategories.Templates', 'DocumentCategories', 2, 'Templates'),
	('DocumentGroups.StaffUpload', 'DocumentGroups', 1, 'Staff Upload')
	
INSERT INTO @Parents (ChildKey, ParentKey)
VALUES 
	('DocumentGroups.StaffUpload', 'DocumentCategories.Templates'),
	('DocumentGroups.CriminalBackgroundCheck', 'DocumentCategories.LegalNotice')
	
INSERT INTO [Config.LocalisationItem]
(
	[Key],
	Value,
	LocalisationId
)
SELECT
	[Key],
	Value,
	@LocalisationId
FROM
	@Lookups L
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.LocalisationItem] LI
		WHERE
			LI.[Key] = L.[Key]
	)
	
INSERT INTO [Config.CodeItem]
(
	[Key]
)
SELECT
	[Key]
FROM
	@Lookups L
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeItem] CI
		WHERE
			CI.[Key] = L.[Key]
	)
	
INSERT INTO [Config.CodeGroupItem]
(
	CodeGroupId,
	CodeItemId,
	DisplayOrder
)
SELECT
	CG.Id,
	CI.Id,
	L.DisplayOrder
FROM
	@Lookups L
INNER JOIN [Config.CodeGroup] CG
	ON CG.[Key] = L.LookupType
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = L.[Key]
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeGroupItem] CGI
		WHERE
			CGI.CodeItemId = CI.Id
			AND CGI.CodeGroupId = CG.Id
	)
	
UPDATE 
	CI
SET
	ParentKey = P.ParentKey
FROM
	@Parents P
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = P.ChildKey

	