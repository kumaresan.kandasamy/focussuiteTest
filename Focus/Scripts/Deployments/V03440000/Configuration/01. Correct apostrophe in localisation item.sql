UPDATE [dbo].[Config.LocalisationItem]
   SET [Value] = 'At least half of my income or my family''s income was from farm work'
 where [Key] = 'MSFWQuestions.HalfFromFarmWork'
GO