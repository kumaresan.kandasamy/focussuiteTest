﻿DELETE FROM dbo.[Config.LocalisationItem]
WHERE [Key] = 'Focus.Web.WebAssist.Controls.DashboardReferralsReport.JobPostings.Header.NoEdit';

IF EXISTS( SELECT * FROM [dbo].[Config.ConfigurationItem] WHERE [Key] = 'Theme' AND Value = 'Education')
BEGIN
	DECLARE @LocalisationId int = (SELECT Id FROM [dbo].[Config.Localisation] WHERE Culture = '**-**');

	INSERT INTO dbo.[Config.LocalisationItem]
	(ContextKey,[Key],Value,Localised,LocalisationId)
	VALUES
	('','Focus.Web.WebAssist.Controls.DashboardReferralsReport.JobPostings.Header.NoEdit','Job Order/Internships',0,@LocalisationId);
END