﻿DELETE FROM 
	[Config.EmailTemplate] 
WHERE 
	EmailTemplateType = 47 
AND Body LIKE '%Please sign in to your #FOCUSTALENT#%'

IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 47)
BEGIN
INSERT INTO [dbo].[Config.EmailTemplate]
           ([EmailTemplateType],
           [Subject],
           [Body],
           [Salutation],
           [Recipient],
           [SenderEmailType],
           [ClientSpecificEmailAddress])
     VALUES
           (47,
           'New candidate application received (#JOBTITLE#, #JOBID#)',
'An application has been made against your open job posting.
           
#JOBTITLE#
#JOBID#
#SENDERNAME#

Please sign in to your #FOCUSTALENT# account to view this application from your account dashboard',
           'Dear #RECIPIENTNAME#',
           null,
           1,
           null)
END


