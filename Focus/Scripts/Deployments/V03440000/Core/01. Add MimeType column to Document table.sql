IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Document' AND COLUMN_NAME = 'MimeType')
BEGIN
	ALTER TABLE
		[Data.Application.Document]
	ADD
		MimeType NVARCHAR(100)
END
GO

UPDATE
	[Data.Application.Document]
SET
	MimeType = CASE SUBSTRING([FileName], CHARINDEX('.', [FileName]) + 1, DATALENGTH([FileName]) - CHARINDEX('.', [FileName]))
				WHEN 'pdf' THEN 'application/pdf'
				WHEN 'xlsx' THEN 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
				WHEN 'xls' THEN 'application/vnd.ms-excel'
			   END
WHERE
	MimeType IS NULL
