﻿IF NOT EXISTS(SELECT 1 FROM [Data.Application.PersonMobileDevice] WHERE [Token] = 'TEST001')
BEGIN
INSERT INTO [Data.Application.PersonMobileDevice]
           ([Id]
           ,[Token]
           ,[ApiKey]
           ,[DeviceType]
           ,[CreatedOn]
           ,[PersonId])
     VALUES
           (1
           ,'TEST001'
           ,'123456'
           ,1
           ,CURRENT_TIMESTAMP
           ,1701625)

END