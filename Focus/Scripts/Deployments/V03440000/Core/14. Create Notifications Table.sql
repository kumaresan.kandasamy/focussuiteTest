﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.PushNotification')
BEGIN
CREATE TABLE [dbo].[Data.Application.PushNotification] (
    [Id]                      BIGINT         NOT NULL,
    [Title]                   NVARCHAR (MAX) DEFAULT ('') NOT NULL,
    [Text]                    NVARCHAR (MAX) DEFAULT ('') NOT NULL,
    [Sent]                    BIT            DEFAULT ((0)) NOT NULL,
    [PersonMobileDeviceToken] NVARCHAR (MAX) DEFAULT ('') NOT NULL,
    [CreatedOn]               DATETIME       NOT NULL,
    [PersonMobileDeviceId]    BIGINT         DEFAULT ((0)) NOT NULL,
    [Viewed]                    BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Data.Application.PushNotification] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);
END
GO

IF NOT EXISTS(SELECT 1 FROM [Data.Application.PushNotification])
BEGIN
INSERT INTO [Data.Application.PushNotification]
           ([Id]
           ,[Title]
           ,[Text]
           ,[PersonMobileDeviceToken]
           ,[Sent]
           ,[CreatedOn]
           ,[PersonMobileDeviceId]
           )
     VALUES
           (99999991
           ,'Test Notification 1'
           ,'This is a test notification message'
           ,'TEST001'
           ,1
           ,CURRENT_TIMESTAMP
           ,1
           )
END
GO

