IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.Referral')
BEGIN
	CREATE TABLE [Data.Application.Referral]
	(
		Id BIGINT NOT NULL,
		PostingId BIGINT NOT NULL,
		PersonId BIGINT NOT NULL,
		UserId BIGINT NOT NULL,
		ActionerId BIGINT NOT NULL,
		ReferralDate DATETIME NOT NULL,
		ActionTypeId BIGINT NOT NULL,
		CONSTRAINT [PK_Data.Application.Referral] PRIMARY KEY CLUSTERED ([Id] ASC)
	)
END
GO

IF NOT EXISTS(SELECT 1 FROM [Data.Application.Referral])
BEGIN
	INSERT INTO [Data.Application.Referral]
	(
		Id,
		PostingId,
		PersonId,
		UserId,
		ActionerId,
		ReferralDate,
		ActionTypeId
	)
	SELECT
		AE.Id,
		AE.EntityId,
		U.PersonId,
		U.Id,
		AE.EntityIdAdditional02,
		AE.ActionedOn,
		AT.Id
	FROM
		[Data.Core.ActionEvent] AE
	INNER JOIN [Data.Core.ActionType] AT
		ON AT.Id = AE.ActionTypeId
	INNER JOIN [Data.Application.User] U
		ON U.PersonId = AE.EntityIdAdditional01
	WHERE
		AT.Name IN ('SelfReferral', 'ExternalReferral')
		AND AE.EntityIdAdditional01 IS NOT NULL
		AND AE.EntityId > 0
END
GO
