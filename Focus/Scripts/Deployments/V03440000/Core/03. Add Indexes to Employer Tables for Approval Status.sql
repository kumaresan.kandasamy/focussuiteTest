IF NOT EXISTS(SELECT * FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Data.Application.Employer]') AND [name] = 'IX_Data.Application.Employer_ApprovalStatus')
BEGIN
	CREATE INDEX [IX_Data.Application.Employer_ApprovalStatus] ON [Data.Application.Employer] (ApprovalStatus)
END
GO

IF NOT EXISTS(SELECT * FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Data.Application.BusinessUnit]') AND [name] = 'IX_Data.Application.BusinessUnit_ApprovalStatus')
BEGIN
	CREATE INDEX [IX_Data.Application.BusinessUnit_ApprovalStatus] ON [Data.Application.BusinessUnit] (ApprovalStatus)
END
GO

IF NOT EXISTS(SELECT * FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Data.Application.Employee]') AND [name] = 'IX_Data.Application.Employee_ApprovalStatus')
BEGIN
	CREATE INDEX [IX_Data.Application.Employee_ApprovalStatus] ON [Data.Application.Employee] (ApprovalStatus)
END
GO

