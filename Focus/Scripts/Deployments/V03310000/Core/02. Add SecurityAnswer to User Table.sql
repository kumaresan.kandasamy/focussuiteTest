IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'SecurityAnswerEncrypted')
BEGIN
	ALTER TABLE 
		[Data.Application.User]
	ADD
		SecurityAnswerEncrypted NVARCHAR(250)
END

