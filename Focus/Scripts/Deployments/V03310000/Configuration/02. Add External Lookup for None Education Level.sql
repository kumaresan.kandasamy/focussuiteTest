IF NOT EXISTS (	SELECT	*
				FROM	[Config.ExternalLookUpItem]
				WHERE	InternalId = 'None'
				AND		ClientDataTag = 'KY'	)
BEGIN
	INSERT	[Config.ExternalLookUpItem]
			([InternalId],[ExternalId],[ExternalLookUpType],[FocusId],[ClientDataTag])
	VALUES
			('None', 0, 0, 0, 'KY')
END