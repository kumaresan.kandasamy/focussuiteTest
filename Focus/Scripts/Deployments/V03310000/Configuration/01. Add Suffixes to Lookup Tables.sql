DECLARE @LookupType NVARCHAR(10)
DECLARE @CodeGroupId BIGINT
DECLARE @LocalisationId BIGINT

SET @LookupType = 'Suffixes'

DECLARE @Suffixes TABLE
(
	[Key] NVARCHAR(50),
	DisplayOrder INT,
	Value NVARCHAR(255),
	ExternalId NVARCHAR(10)
)

INSERT INTO @Suffixes ([Key], DisplayOrder, Value, ExternalId)
VALUES 
(@LookupType + '.Sr', 1, 'Sr.', 'SR'),
(@LookupType + '.Jr', 2, 'Jr.', 'JR'),
(@LookupType + '.II', 3, 'II', 'II'),
(@LookupType + '.III', 4, 'III', 'III'),
(@LookupType + '.IV', 5, 'IV', 'IV'),
(@LookupType + '.V', 6, 'V', 'V'),
(@LookupType + '.Esq', 7, 'Esq.', 'ESQ')

IF NOT EXISTS(SELECT 1 FROM [Config.CodeGroup] WHERE [Key] = @LookupType)
BEGIN
	INSERT INTO [Config.CodeGroup] ([Key])
	VALUES (@LookupType)
END

INSERT INTO [Config.CodeItem] 
(
	[Key], 
	IsSystem,
	ExternalId
)
SELECT
	S.[Key],
	0,
	S.ExternalId
FROM
	@Suffixes S
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeItem] CI
		WHERE
			CI.[Key] = S.[Key]
	)

UPDATE
	CI
SET
	ExternalId = S.ExternalId
FROM
	@Suffixes S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
WHERE
	ISNULL(CI.ExternalId, '') <> S.ExternalId	

SELECT
	@CodeGroupId = Id
FROM 
	[Config.CodeGroup]
WHERE
	[Key] = @LookupType

INSERT INTO [Config.CodeGroupItem]
(
	DisplayOrder,
	CodeGroupId,
	CodeItemId
)
SELECT
	S.DisplayOrder,
	@CodeGroupId,
	CI.Id
FROM
	@Suffixes S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeGroupItem] CGI
		WHERE
			CGI.CodeGroupId = @CodeGroupId
			AND CGI.CodeItemId = CI.Id
	)
	
UPDATE
	CGI
SET
	DisplayOrder = S.DisplayOrder
FROM
	@Suffixes S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
INNER JOIN [Config.CodeGroupItem] CGI
	ON CGI.CodeGroupId = @CodeGroupId
	AND CGI.CodeItemId = CI.Id
WHERE
	CGI.DisplayOrder <> S.DisplayOrder
	
SELECT 
	@LocalisationId = L.Id
FROM 
	[Config.Localisation] L
WHERE 
	L.Culture = '**-**'

INSERT INTO [Config.LocalisationItem]
(
	[Key],
	Value,
	LocalisationId
)
SELECT
	S.[Key],
	S.Value,
	@LocalisationId
FROM 
	@Suffixes S
WHERE
	NOT EXISTS
	(
		SELECT 
			1
		FROM
			[Config.LocalisationItem] LI
		WHERE
			LI.[Key] = S.[Key]
	)

	
UPDATE
	LI
SET
	Value = S.Value
FROM
	@Suffixes S
INNER JOIN [Config.LocalisationItem] LI
	ON LI.[Key] = S.[Key]
WHERE
	LI.Value <> S.Value