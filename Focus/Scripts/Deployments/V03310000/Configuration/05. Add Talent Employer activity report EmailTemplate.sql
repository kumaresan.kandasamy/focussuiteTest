BEGIN TRANSACTION

DELETE FROM [Config.EmailTemplate] WHERE EmailTemplateType = 40

INSERT INTO [Config.EmailTemplate]
(
	EmailTemplateType, 
	[Subject], 
	Body, 
	Salutation, 
	Recipient
)
VALUES 
(
	40, 
	'#FOCUSTALENT# Employer activity report', 
	'Please find attached your employer activity report for the following reporting period; 

#REPORTFROMDATE# to #REPORTTODATE#', 
	NULL, 
	NULL
)
--ROLLBACK TRANSACTION
COMMIT TRANSACTION