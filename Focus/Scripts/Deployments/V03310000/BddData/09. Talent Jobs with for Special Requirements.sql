DECLARE @Jobs TABLE
(
	Id BIGINT IDENTITY(1, 1),
	JobTitle NVARCHAR(40),
	JobStatus INT,
	EmployeeEmailAddress NVARCHAR(255),
	BusinessUnitId BIGINT,
	EmployeeId BIGINT,
	EmployeeUserId BIGINT,
	EmployerId BIGINT,
	BusinessUnitDescriptionId BIGINT,
	EmployerName NVARCHAR(255),
	LensPostingId NVARCHAR(255),
	PostalCode NVARCHAR(10),
	PostingXml NVARCHAR(MAX)
)

INSERT INTO @Jobs (JobTitle, EmployeeEmailAddress, JobStatus, PostalCode, LensPostingId, PostingXml)
VALUES 
	('Draft Job 1753', 'dev@employer.com', 0, '92101', NULL, '<jobposting><JobDoc><posting><duties>Test Job Description<title onet="11-3042.00">##JOBTITLE##</title><onettitle>Training and Development Managers</onettitle></duties><contact><address><city>San Diego</city><majorcity>San Diego</majorcity><state>CA</state><postalcode>##POSTALCODE##</postalcode></address><company>Employer FVN638</company><person>Dev Employer</person></contact><jobinfo><positioncount>2</positioncount><EDUCATION_CD>0</EDUCATION_CD><JOB_TYPE>1</JOB_TYPE></jobinfo><benefits><workdays><monday>0</monday><tuesday>0</tuesday><wednesday>0</wednesday><thursday>0</thursday><friday>0</friday><saturday>1</saturday><sunday>1</sunday><varies>1</varies></workdays></benefits></posting><special><mode>insert</mode><originid>7</originid><jobtitle>Job with auto-approved referral</jobtitle><jobemployer>Employer FVN638</jobemployer><job_status_cd>1</job_status_cd><jobtype>4</jobtype><JOBTYPE_CD>4</JOBTYPE_CD></special></JobDoc><postinghtml>&lt;html&gt;    &lt;body&gt;      &lt;p&gt;&lt;b&gt;Job with all applicants to be screened&lt;/b&gt;&lt;br&gt;Employer FVN638&lt;br&gt;San Diego, CA (##POSTALCODE##)(public transit accessible)&lt;br&gt;Number of openings: 2&lt;br&gt;Application closing date: 15/05/2015&lt;br&gt;&lt;/p&gt;      &lt;p&gt;Test Job Description&lt;/p&gt;      &lt;p&gt;&lt;b&gt;Benefits&lt;/b&gt;&lt;br&gt;No benefits are offered with this job&lt;/p&gt;      &lt;p&gt;&lt;b&gt;About Employer FVN638&lt;/b&gt;&lt;br&gt;Company Description&lt;/p&gt;      &lt;p&gt;&lt;b&gt;How to apply&lt;/b&gt;&lt;br&gt;&lt;a href="http://localhost:57990" target="_blank"&gt;Log in to Focus/Career and submit your resume&lt;/a&gt;&lt;/p&gt;      &lt;p&gt;&lt;small&gt;REF/4760944/4761031&lt;/small&gt;&lt;/p&gt;    &lt;/body&gt;     #POSTINGFOOTER#    &lt;/html&gt;</postinghtml><REQUIREMENTS screening="-1"></REQUIREMENTS><clientjobdata><CONTACT_SEND_DIRECT_FLAG>0</CONTACT_SEND_DIRECT_FLAG><CONTACT_POSTAL_FLAG>0</CONTACT_POSTAL_FLAG><CONTACT_PHONE_FLAG>0</CONTACT_PHONE_FLAG><CONTACT_FAX_FLAG>0</CONTACT_FAX_FLAG><CONTACT_EMAIL_FLAG>0</CONTACT_EMAIL_FLAG><CONTACT_URL_FLAG>0</CONTACT_URL_FLAG><CONTACT_TALENT_FLAG>1</CONTACT_TALENT_FLAG><SAL_MIN>0.00</SAL_MIN><SAL_MAX>0.00</SAL_MAX><SAL_UNIT_CD /><JOB_CREATE_DATE>2014-07-22</JOB_CREATE_DATE><JOB_LAST_OPEN_DATE>2015-07-22</JOB_LAST_OPEN_DATE></clientjobdata></jobposting>'),
	('Active Job 1753', 'dev@employer.com', 1, '92101', 'ABCDEF_5078D98023CD4DE09299A0BA46D2ECC8', '<jobposting><JobDoc><posting><duties>Test Job Description<title onet="11-3042.00">##JOBTITLE##</title><onettitle>Training and Development Managers</onettitle></duties><contact><address><city>San Diego</city><majorcity>San Diego</majorcity><state>CA</state><postalcode>##POSTALCODE##</postalcode></address><company>Employer FVN638</company><person>Dev Employer</person></contact><jobinfo><positioncount>2</positioncount><EDUCATION_CD>0</EDUCATION_CD><JOB_TYPE>1</JOB_TYPE></jobinfo><benefits><workdays><monday>0</monday><tuesday>0</tuesday><wednesday>0</wednesday><thursday>0</thursday><friday>0</friday><saturday>1</saturday><sunday>1</sunday><varies>1</varies></workdays></benefits></posting><special><mode>insert</mode><originid>7</originid><jobtitle>Job with auto-approved referral</jobtitle><jobemployer>Employer FVN638</jobemployer><job_status_cd>1</job_status_cd><jobtype>4</jobtype><JOBTYPE_CD>4</JOBTYPE_CD></special></JobDoc><postinghtml>&lt;html&gt;    &lt;body&gt;      &lt;p&gt;&lt;b&gt;Job with all applicants to be screened&lt;/b&gt;&lt;br&gt;Employer FVN638&lt;br&gt;San Diego, CA (##POSTALCODE##)(public transit accessible)&lt;br&gt;Number of openings: 2&lt;br&gt;Application closing date: 15/05/2015&lt;br&gt;&lt;/p&gt;      &lt;p&gt;Test Job Description&lt;/p&gt;      &lt;p&gt;&lt;b&gt;Benefits&lt;/b&gt;&lt;br&gt;No benefits are offered with this job&lt;/p&gt;      &lt;p&gt;&lt;b&gt;About Employer FVN638&lt;/b&gt;&lt;br&gt;Company Description&lt;/p&gt;      &lt;p&gt;&lt;b&gt;How to apply&lt;/b&gt;&lt;br&gt;&lt;a href="http://localhost:57990" target="_blank"&gt;Log in to Focus/Career and submit your resume&lt;/a&gt;&lt;/p&gt;      &lt;p&gt;&lt;small&gt;REF/4760944/4761031&lt;/small&gt;&lt;/p&gt;    &lt;/body&gt;     #POSTINGFOOTER#    &lt;/html&gt;</postinghtml><REQUIREMENTS screening="-1"></REQUIREMENTS><clientjobdata><CONTACT_SEND_DIRECT_FLAG>0</CONTACT_SEND_DIRECT_FLAG><CONTACT_POSTAL_FLAG>0</CONTACT_POSTAL_FLAG><CONTACT_PHONE_FLAG>0</CONTACT_PHONE_FLAG><CONTACT_FAX_FLAG>0</CONTACT_FAX_FLAG><CONTACT_EMAIL_FLAG>0</CONTACT_EMAIL_FLAG><CONTACT_URL_FLAG>0</CONTACT_URL_FLAG><CONTACT_TALENT_FLAG>1</CONTACT_TALENT_FLAG><SAL_MIN>0.00</SAL_MIN><SAL_MAX>0.00</SAL_MAX><SAL_UNIT_CD /><JOB_CREATE_DATE>2014-07-22</JOB_CREATE_DATE><JOB_LAST_OPEN_DATE>2015-07-22</JOB_LAST_OPEN_DATE></clientjobdata></jobposting>')
	
UPDATE 
	@Jobs
SET
	PostingXml = REPLACE(REPLACE(PostingXml, 
					'##JOBTITLE##', JobTitle), 
					'##POSTALCODE##', PostalCode)

DECLARE @NextId BIGINT

DECLARE @JobTitle NVARCHAR(50)

DECLARE @JobId BIGINT
DECLARE @JobAddressId BIGINT
DECLARE @JobLocationId BIGINT
DECLARE @BusinessUnitId BIGINT
DECLARE @EmployeeId BIGINT
DECLARE @EmployeeUserId BIGINT
DECLARE @EmployerId BIGINT
DECLARE @BusinessUnitDescriptionId BIGINT
DECLARE @EmployerName NVARCHAR(255)
DECLARE @PostingId BIGINT
DECLARE @PostingXml NVARCHAR(MAX)
DECLARE @PostalCode NVARCHAR(10)
DECLARE @LensPostingId NVARCHAR(255)
DECLARE @PostingHtml NVARCHAR(MAX)
DECLARE @JobStatus INT
DECLARE @ActionEventId BIGINT

DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @OnetId BIGINT
DECLARE @ROnetId BIGINT

SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.San DiegoCA'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.CA'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @OnetId = Id FROM [Library.Onet] WHERE OnetCode = '11-3042.00'
SELECT TOP 1 @ROnetId = Id FROM [Library.ROnet] WHERE OnetId = @OnetId

UPDATE
	TJ
SET
	BusinessUnitId = EBU.BusinessUnitId,
	EmployeeId = E.Id,
	EmployeeUserId = U.Id,
	EmployerId = E.EmployerId,
	BusinessUnitDescriptionId = BUD.Id,
	EmployerName = BU.Name
FROM
	@Jobs TJ
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = TJ.EmployeeEmailAddress
INNER JOIN [Data.Application.User] U
	ON U.PersonId = P.Id
INNER JOIN [Data.Application.Employee] E
	ON E.PersonId = P.Id
INNER JOIN [Data.Application.EmployeeBusinessUnit] EBU
	ON EBU.EmployeeId = E.Id
	AND EBU.[Default] = 1
INNER JOIN [Data.Application.BusinessUnit] BU
	ON BU.Id = EBU.BusinessUnitId
INNER JOIN [Data.Application.BusinessUnitDescription] BUD
	ON BUD.BusinessUnitId = EBU.BusinessUnitId
	AND BUD.IsPrimary = 1

DECLARE @JobCount INT
DECLARE @TotalJobs INT

SET @JobCount = 1
SELECT @TotalJobs = COUNT(1) FROM @Jobs

WHILE @JobCount <= @TotalJobs
BEGIN
	SELECT 
		@JobTitle = JobTitle,
		@BusinessUnitId = BusinessUnitId,
		@EmployeeId = EmployeeId,
		@EmployeeUserId = EmployeeUserId,
		@EmployerId = EmployerId,
		@BusinessUnitDescriptionId = BusinessUnitDescriptionId,
		@EmployerName = EmployerName,
		@PostingXml = PostingXml,
		@LensPostingId = LensPostingId,
		@JobStatus = JobStatus,
		@PostalCode = PostalCode
	FROM
		@Jobs
	WHERE
		Id = @JobCount
	
	IF @BusinessUnitId IS NOT NULL
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Job] WHERE JobTitle = @JobTitle AND BusinessUnitId = @BusinessUnitId)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 5
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @JobId = @NextId
		SET @JobAddressId = @NextId + 1
		SET @JobLocationId = @NextId + 2
		SET @PostingId = @NextId + 3
		SET @ActionEventId = @NextId + 4
		
		SET @PostingHtml = '<html><body><p><b>' + @JobTitle + '</b><br>' + @EmployerName + '<br>San Diego, CA (' + @PostalCode + ')(public transit accessible)<br>Number of openings: 2<br>Application closing date: 15/05/2015<br></p><p>Test Job Description</p> <p><b>Benefits</b><br>No benefits are offered with this job</p><p><b>About ' + @EmployerName + '</b><br>Company Description</p><p><b>How to apply</b><br><a href="http://localhost:57990" target="_blank">Log in to Focus/Career and submit your resume</a></p><p><small>REF/4760944/4761031</small></p></body>#POSTINGFOOTER#</html>'
		
		INSERT INTO [Data.Application.Job]
		(
			Id,
			JobTitle,
			CreatedBy,
			UpdatedBy,
			ApprovalStatus,
			JobStatus,
			MinSalary,
			MaxSalary,
			SalaryFrequencyId,
			HoursPerWeek,
			OverTimeRequired,
			ClosingOn,
			NumberOfOpenings,
			PostedOn,
			PostedBy,
			[Description],
			PostingHtml,
			EmployerDescriptionPostingPosition,
			WizardStep,
			WizardPath,
			HeldOn,
			HeldBy,
			ClosedOn,
			ClosedBy,
			JobLocationType,
			FederalContractor,
			ForeignLabourCertification,
			CourtOrderedAffirmativeAction,
			FederalContractorExpiresOn,
			WorkOpportunitiesTaxCreditHires,
			PostingFlags,
			IsCommissionBased,
			NormalWorkDays,
			WorkDaysVary,
			NormalWorkShiftsId,
			LeaveBenefits,
			RetirementBenefits,
			InsuranceBenefits,
			MiscellaneousBenefits,
			OtherBenefitsDetails,
			InterviewContactPreferences,
			InterviewEmailAddress,
			InterviewApplicationUrl,
			InterviewMailAddress,
			InterviewFaxNumber,
			InterviewPhoneNumber,
			InterviewDirectApplicationDetails,
			InterviewOtherInstructions,
			ScreeningPreferences,
			MinimumEducationLevel,
			MinimumEducationLevelRequired,
			MinimumAge,
			MinimumAgeReason,
			MinimumAgeRequired,
			LicencesRequired,
			CertificationRequired,
			LanguagesRequired,
			Tasks,
			RedProfanityWords,
			YellowProfanityWords,
			EmploymentStatusId,
			JobTypeId,
			JobStatusId,
			ApprovedOn,
			ApprovedBy,
			ExternalId,
			AwaitingApprovalOn,
			MinimumExperience,
			MinimumExperienceMonths,
			MinimumExperienceRequired,
			DrivingLicenceClassId,
			DrivingLicenceRequired,
			HideSalaryOnPosting,
			ForeignLabourCertificationH2A,
			ForeignLabourCertificationH2B,
			ForeignLabourCertificationOther,
			HiringFromTaxCreditProgramNotificationSent,
			IsConfidential,
			HasCheckedCriminalRecordExclusion,
			LastPostedOn,
			LastRefreshedOn,
			MinimumAgeReasonValue,
			OtherSalary,
			HideOpeningsOnPosting,
			StudentEnrolled,
			MinimumCollegeYears,
			CreatedOn,
			UpdatedOn,
			LockVersion,
			BusinessUnitId,
			EmployeeId,
			EmployerId,
			BusinessUnitDescriptionId,
			BusinessUnitLogoId,
			ProgramsOfStudyRequired,
			StartDate,
			EndDate,
			JobType,
			OnetId,
			HideEducationOnPosting,
			HideExperienceOnPosting,
			HideMinimumAgeOnPosting,
			HideProgramOfStudyOnPosting,
			HideDriversLicenceOnPosting,
			HideLicencesOnPosting,
			HideCertificationsOnPosting,
			HideLanguagesOnPosting,
			HideSpecialRequirementsOnPosting,
			HideWorkWeekOnPosting,
			DescriptionPath,
			WorkWeekId,
			AssignedToId,
			CriminalBackgroundExclusionRequired,
			ROnetId,
			IsSalaryAndCommissionBased,
			VeteranPriorityEndDate,
			MeetsMinimumWageRequirement,
			CareerReadinessLevel,
			CareerReadinessLevelRequired
		)
		VALUES
		(
			@JobId,
			@JobTitle,
			@EmployeeUserId,
			@EmployeeUserId,
			2,
			@JobStatus,
			NULL,
			NULL,
			0,
			NULL,
			0,
			'15 May 2015',
			2,
			GETDATE(),
			@EmployeeUserId,
			'Test Job Description',
			@PostingHtml,
			0,
			7,
			1,
			NULL,
			NULL,
			NULL,
			NULL,
			0,
			0,
			NULL,
			0,
			NULL,
			0,
			0,
			0,
			96,
			1,
			0,
			0,
			0,
			0,
			0,
			'',
			64,
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			1,
			0,
			0,
			NULL,
			'',
			0,
			0,
			0,
			0,
			NULL,
			NULL,
			NULL,
			0,
			0,
			0,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			0,
			NULL,
			0,
			0,
			0,
			0,
			0,
			NULL,
			0,
			0,
			GETDATE(),
			NULL,
			0,
			NULL,
			NULL,
			0,
			NULL,
			GETDATE(),
			GETDATE(),
			10,
			@BusinessUnitId,
			@EmployeeId,
			@EmployerId,
			@BusinessUnitDescriptionId,
			NULL,
			NULL,
			NULL,
			NULL,
			1,
			@OnetId,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			NULL,
			NULL,
			0,
			@ROnetId,
			0,
			NULL,
			1,
			NULL,
			0
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.JobAddress]
		(
			Id,
			JobId,
			Line1,
			Line2,
			Line3,
			TownCity,
			CountyId,
			PostcodeZip,
			StateId,
			CountryId,
			IsPrimary
		)
		VALUES
		(
			@JobAddressId,
			@JobId,
			'Address Line 1',
			'',
			'',
			'San Deigo',
			@CountyId,
			@PostalCode,
			@StateId,
			@CountryId,
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.JobLocation]
		(
			Id,
			JobId,
			Location,
			IsPublicTransitAccessible
		)
		VALUES
		(
			@JobLocationId,
			@JobId,
			'San Diego, CA (' + @PostalCode + ')',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		IF @JobStatus > 0 
		BEGIN
			INSERT INTO [Data.Application.Posting]
			(
				Id,
				JobId,
				LensPostingId,
				JobTitle,
				PostingXml,
				EmployerName,
				Url,
				ExternalId,
				StatusId,
				OriginId,
				Html,
				ViewedCount,
				CreatedOn,
				UpdatedOn
			)
			VALUES
			(
				@PostingId,
				@JobId,
				ISNULL(@LensPostingId, 'ABCDEF_' + REPLACE(NEWID(), '-', '')),
				@JobTitle,
				@PostingXml,
				@EmployerName,
				'',
				NULL,
				1,
				7,
				@PostingHtml,
				0,
				GETDATE(),
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
		END
		
		INSERT INTO [Data.Core.ActionEvent] 
		(
			Id,
			SessionId,
			RequestId,
			UserId,
			ActionedOn,
			EntityId,
			ActionTypeId,
			EntityTypeId
		)
		SELECT
			@ActionEventId,
			NEWID(),
			NEWID(),
			@EmployeeUserId,
			GETDATE(),
			@JobId,
			A.Id,
			E.Id
		FROM
			[Data.Core.ActionType] A
		CROSS JOIN [Data.Core.EntityType] E
		WHERE
			A.Name = 'SaveJob'
			AND E.Name = 'Job'
			
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	
	SET @JobCount = @JobCount + 1
END
