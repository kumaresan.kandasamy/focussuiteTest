IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Messaging.Message' AND COLUMN_NAME = 'ProcessOn')
BEGIN
	ALTER TABLE [Messaging.Message]
		ADD ProcessOn DATETIME NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Messaging.Message' AND COLUMN_NAME = 'EntityKey')
BEGIN
	ALTER TABLE [Messaging.Message]
		ADD EntityKey BIGINT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Messaging.Message' AND COLUMN_NAME = 'EntityName')
BEGIN
	ALTER TABLE [Messaging.Message]
		ADD EntityName NVARCHAR(200) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Messaging.Message_MessageType_Entity' AND object_id = OBJECT_ID('[Messaging.Message]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Messaging.Message_MessageType_Entity]
		ON [Messaging.Message] ([Version], MessageTypeId, EntityName, EntityKey, [Status])
END
