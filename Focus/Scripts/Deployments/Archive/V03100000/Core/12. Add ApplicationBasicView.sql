IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.ApplicationBasicView]'))
DROP VIEW [dbo].[Data.Application.ApplicationBasicView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.ApplicationBasicView]
AS

SELECT
	[Application].Id AS Id,
	[Application].ApplicationStatus AS CandidateApplicationStatus,
	[Application].ApplicationScore AS CandidateApplicationScore,
	[Application].StatusLastChangedOn AS ApplicationStatusLastChangedOn,
	Posting.JobId,
	[Resume].PersonId AS CandidateId
FROM 
	[Data.Application.Application] AS [Application] WITH (NOLOCK)
INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) 
	ON [Application].PostingId = Posting.Id
INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) 
	ON [Application].ResumeId = [Resume].Id
WHERE	
	Posting.JobId IS NOT NULL
GO


