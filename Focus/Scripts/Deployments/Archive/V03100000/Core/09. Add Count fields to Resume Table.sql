IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'SkillCount')
BEGIN
	ALTER TABLE [Data.Application.Resume] 
		ADD SkillCount INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'WordCount')
BEGIN
	ALTER TABLE [Data.Application.Resume] 
		ADD WordCount INT NULL
END
GO

UPDATE
	[Data.Application.Resume]
SET
	SkillCount = CASE ISNULL(ResumeSkills, '') WHEN '' THEN 0 ELSE LEN(ResumeSkills) - LEN(REPLACE(ResumeSkills, ',', '')) + 1 END
WHERE
	SkillCount IS NULL