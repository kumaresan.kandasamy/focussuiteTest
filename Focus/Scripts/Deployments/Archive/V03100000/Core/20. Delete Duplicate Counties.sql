DECLARE @OldId BIGINT
DECLARE @NewId BIGINT

SELECT @OldId = Id FROM [Config.CodeItem] WHERE [Key] = 'County.CapeGirardeauMO'
SELECT @NewId = Id FROM [Config.CodeItem] WHERE [Key] = 'County.Cape GirardeauMO'

IF @OldId IS NOT NULL AND @NewId IS NOT NULL
BEGIN
  -- Update any existing records to use "County.Cape GirardeauMO"
	UPDATE [Data.Application.BusinessUnitAddress] SET CountyId = @NewId WHERE CountyId = @OldId
	UPDATE [Data.Application.EmployerAddress] SET CountyId = @NewId WHERE CountyId = @OldId
	UPDATE [Data.Application.JobAddress] SET CountyId = @NewId WHERE CountyId = @OldId
	UPDATE [Data.Application.PersonAddress] SET CountyId = @NewId WHERE CountyId = @OldId
	UPDATE [Data.Application.Resume] SET CountyId = @NewId WHERE CountyId = @OldId
	UPDATE [Data.Application.Office] SET CountyId = @NewId WHERE CountyId = @OldId
	UPDATE [Report.Employer] SET CountyId = @NewId WHERE CountyId = @OldId
	UPDATE [Report.JobOrder] SET CountyId = @NewId WHERE CountyId = @OldId
	UPDATE [Report.JobSeeker] SET CountyId = @NewId WHERE CountyId = @OldId

	DELETE FROM [Config.CodeGroupItem] WHERE CodeItemId = @OldId
	DELETE FROM [Config.CodeItem] WHERE Id = @OldId
END
