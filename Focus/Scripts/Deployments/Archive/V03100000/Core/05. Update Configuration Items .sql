--SELECT * FROM [Config.ConfigurationItem] WHERE [Key] = 'BatchProcessSettings'

DECLARE @BatchSettings NVARCHAR(4000)

SELECT @BatchSettings = Value FROM [Config.ConfigurationItem] WHERE [Key] = 'BatchProcessSettings'

IF NOT (@BatchSettings LIKE '%{"Process":8,%')
BEGIN
	UPDATE 
		[Config.ConfigurationItem] 
	SET
		Value = LEFT(@BatchSettings, LEN(@BatchSettings) - 1) + ',{"Process":8,"Identifier":"6bd589e04e8540209d1594133ca898"}]'
	WHERE 
		[Key] = 'BatchProcessSettings'
		AND Value = @BatchSettings
END
