-- Job Seeker Tables

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeeker_FocusPersonId' AND object_id = OBJECT_ID('[Report.JobSeeker]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeeker_FocusPersonId]
		ON [Report.JobSeeker] (FocusPersonId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeeker_DateOfBirth' AND object_id = OBJECT_ID('[Report.JobSeeker]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeeker_DateOfBirth]
		ON [Report.JobSeeker] (DateOfBirth)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerAction_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerAction]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerAction_JobSeekerId]
		ON [Report.JobSeekerAction] (JobSeekerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerAction_JobSeekerId_ActionDate' AND object_id = OBJECT_ID('[Report.JobSeekerAction]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerAction_JobSeekerId_ActionDate]
		ON [Report.JobSeekerAction] (JobSeekerId, ActionDate)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerActivityAssignment_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerActivityAssignment]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerActivityAssignment_JobSeekerId]
		ON [Report.JobSeekerActivityAssignment] (JobSeekerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerData_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerData]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerData_JobSeekerId]
		ON [Report.JobSeekerData] (JobSeekerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerData_DataType' AND object_id = OBJECT_ID('[Report.JobSeekerData]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerData_DataType]
		ON [Report.JobSeekerData] (DataType)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerOffice_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerOffice]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerOffice_JobSeekerId]
		ON [Report.JobSeekerOffice] (JobSeekerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobSeekerJobHistory_JobSeekerId' AND object_id = OBJECT_ID('[Report.JobSeekerJobHistory]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeekerJobHistory_JobSeekerId]
		ON [Report.JobSeekerJobHistory] (JobSeekerId)
END

-- Job Order Tables

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobOrder_FocusJobId' AND object_id = OBJECT_ID('[Report.JobOrder]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobOrder_FocusJobId]
		ON [Report.JobOrder] (FocusJobId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobOrder_EmployerId' AND object_id = OBJECT_ID('[Report.JobOrder]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobOrder_EmployerId]
		ON [Report.JobOrder] (EmployerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobOrderAction_JobOrderId' AND object_id = OBJECT_ID('[Report.JobOrderAction]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobOrderAction_JobOrderId]
		ON [Report.JobOrderAction] (JobOrderId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobOrderAction_JobOrderId_ActionDate' AND object_id = OBJECT_ID('[Report.JobOrderAction]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobOrderAction_JobOrderId_ActionDate]
		ON [Report.JobOrderAction] (JobOrderId, ActionDate)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobOrderOffice_JobOrderId' AND object_id = OBJECT_ID('[Report.JobOrderOffice]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobOrderOffice_JobOrderId]
		ON [Report.JobOrderOffice] (JobOrderId)
END

-- Employer Tables
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.Employer_FocusBusinessUnitId' AND object_id = OBJECT_ID('[Report.Employer]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.Employer_FocusBusinessUnitId]
		ON [Report.Employer] (FocusBusinessUnitId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.JobOrder_FocusJobId' AND object_id = OBJECT_ID('[Report.JobOrder]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.JobOrder_FocusJobId]
		ON [Report.JobOrder] (FocusJobId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.EmployerAction_EmployerId' AND object_id = OBJECT_ID('[Report.EmployerAction]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.EmployerAction_EmployerId]
		ON [Report.EmployerAction] (EmployerId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.EmployerAction_EmployerId_ActionDate' AND object_id = OBJECT_ID('[Report.EmployerAction]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.EmployerAction_EmployerId_ActionDate]
		ON [Report.EmployerAction] (EmployerId, ActionDate)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.EmployerOffice_EmployerId' AND object_id = OBJECT_ID('[Report.EmployerOffice]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.EmployerOffice_EmployerId]
		ON [Report.EmployerOffice] (EmployerId)
END
