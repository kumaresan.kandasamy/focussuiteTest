﻿IF NOT EXISTS (SELECT 1 FROM [dbo].[Messaging.MessageType] WHERE [TypeName] LIKE 'Focus.Services.Messages.IntegrationRequestMessage,%')
BEGIN
	INSERT INTO [dbo].[Messaging.MessageType]
			   ([Id]
			   ,[TypeName])
	VALUES
	   ('B2364FE8-55DD-47A4-9D94-7DB46E313CDF'
	   ,'Focus.Services.Messages.IntegrationRequestMessage, Focus.Services, Version=2.0.6.0, Culture=neutral, PublicKeyToken=null')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Messaging.MessageType] WHERE [TypeName] LIKE 'Focus.Services.Messages.LogIntegrationEventMessage,%')
BEGIN
INSERT INTO [dbo].[Messaging.MessageType]
           ([Id]
           ,[TypeName])
VALUES
   ('3DD43853-23FC-47E2-8CF4-5B153FD52A95'
   ,'Focus.Services.Messages.LogIntegrationEventMessage, Focus.Services, Version=2.0.6.0, Culture=neutral, PublicKeyToken=null')
END

UPDATE [Data.Application.Job] SET [ExternalId] = NULL