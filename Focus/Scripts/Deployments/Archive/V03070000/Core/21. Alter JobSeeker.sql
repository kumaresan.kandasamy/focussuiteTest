-- Add column EnrollmentStatus of type Int32 to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [EnrollmentStatus] INT NULL;
-- Add column MilitaryStartDate of type DateTime to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [MilitaryStartDate] DATETIME NULL;
-- Add column MilitaryEndDate of type DateTime to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [MilitaryEndDate] DATETIME NULL;
-- Add column IsHomelessVeteran of type Boolean to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [IsHomelessVeteran] BIT NULL;
-- Add column SocialSecurityNumber of type String to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [SocialSecurityNumber] NVARCHAR(MAX) NULL;
-- Add column MiddleInitial of type String to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [MiddleInitial] NVARCHAR(MAX) NULL;
-- Add column AddressLine1 of type String to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [AddressLine1] NVARCHAR(MAX) NULL;
-- Add column IsCitizen of type Boolean to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [IsCitizen] BIT NULL;
-- Add column AlienCardExpires of type DateTime to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [AlienCardExpires] DATETIME NULL;
-- Add column AlienCardId of type String to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [AlienCardId] NVARCHAR(MAX) NULL;
-- Add column VeteranAttendedTapCourse of type Boolean to table Report.jobSeeker
ALTER TABLE [Report.jobSeeker] ADD [VeteranAttendedTapCourse] BIT NULL;