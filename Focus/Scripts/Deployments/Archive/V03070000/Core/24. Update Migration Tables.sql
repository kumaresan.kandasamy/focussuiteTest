-- Add ParentExternalId column

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.EmployerRequest' AND COLUMN_NAME = 'ParentExternalId')
BEGIN
	ALTER TABLE [Data.Migration.EmployerRequest] ADD [ParentExternalId] NVARCHAR(200) NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobOrderRequest' AND COLUMN_NAME = 'ParentExternalId')
BEGIN
	ALTER TABLE [Data.Migration.JobOrderRequest] ADD [ParentExternalId] NVARCHAR(200) NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobSeekerRequest' AND COLUMN_NAME = 'ParentExternalId')
BEGIN
	ALTER TABLE [Data.Migration.JobSeekerRequest] ADD [ParentExternalId] NVARCHAR(200) NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.UserRequest' AND COLUMN_NAME = 'ParentExternalId')
BEGIN
	ALTER TABLE [Data.Migration.UserRequest] ADD [ParentExternalId] NVARCHAR(200) NULL
END

-- Add Batch Number column

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.EmployerRequest' AND COLUMN_NAME = 'BatchNumber')
BEGIN
	ALTER TABLE [Data.Migration.EmployerRequest] ADD [BatchNumber] INT
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobOrderRequest' AND COLUMN_NAME = 'BatchNumber')
BEGIN
	ALTER TABLE [Data.Migration.JobOrderRequest] ADD [BatchNumber] INT
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobSeekerRequest' AND COLUMN_NAME = 'BatchNumber')
BEGIN
	ALTER TABLE [Data.Migration.JobSeekerRequest] ADD [BatchNumber] INT
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.UserRequest' AND COLUMN_NAME = 'BatchNumber')
BEGIN
	ALTER TABLE [Data.Migration.UserRequest] ADD [BatchNumber] INT
END

-- Add IsUpdate column

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.EmployerRequest' AND COLUMN_NAME = 'IsUpdate')
BEGIN
	ALTER TABLE [Data.Migration.EmployerRequest] ADD [IsUpdate] BIT NOT NULL DEFAULT(0)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobOrderRequest' AND COLUMN_NAME = 'IsUpdate')
BEGIN
	ALTER TABLE [Data.Migration.JobOrderRequest] ADD [IsUpdate] BIT NOT NULL DEFAULT(0)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobSeekerRequest' AND COLUMN_NAME = 'IsUpdate')
BEGIN
	ALTER TABLE [Data.Migration.JobSeekerRequest] ADD [IsUpdate] BIT NOT NULL DEFAULT(0)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.UserRequest' AND COLUMN_NAME = 'IsUpdate')
BEGIN
	ALTER TABLE [Data.Migration.UserRequest] ADD [IsUpdate] BIT NOT NULL DEFAULT(0)
END

-- Add EmailRequired column

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.EmployerRequest' AND COLUMN_NAME = 'EmailRequired')
BEGIN
	ALTER TABLE [Data.Migration.EmployerRequest] ADD [EmailRequired] BIT NOT NULL DEFAULT(0)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobOrderRequest' AND COLUMN_NAME = 'EmailRequired')
BEGIN
	ALTER TABLE [Data.Migration.JobOrderRequest] ADD [EmailRequired] BIT NOT NULL DEFAULT(0)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobSeekerRequest' AND COLUMN_NAME = 'EmailRequired')
BEGIN
	ALTER TABLE [Data.Migration.JobSeekerRequest] ADD [EmailRequired] BIT NOT NULL DEFAULT(0)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.UserRequest' AND COLUMN_NAME = 'EmailRequired')
BEGIN
	ALTER TABLE [Data.Migration.UserRequest] ADD [EmailRequired] BIT NOT NULL DEFAULT(0)
END

-- Amend ExternalId column to be NVARCHAR(200)

DECLARE @Sql NVARCHAR(500)

IF -1 = (SELECT CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.JobSeekerRequest' AND COLUMN_NAME = 'ExternalId')
BEGIN
	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Data.Migration.JobSeekerRequest'
	AND C.name = 'ExternalId'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Data.Migration.JobSeekerRequest] 
		ALTER COLUMN [ExternalId] NVARCHAR(200) NOT NULL 
	ALTER TABLE [Data.Migration.JobSeekerRequest] 
		ADD CONSTRAINT [DF_Data.Migration.JobSeekerRequest_ExternalId] DEFAULT '' FOR [ExternalId] 
END

IF -1 = (SELECT CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.UserRequest' AND COLUMN_NAME = 'ExternalId')
BEGIN
	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Data.Migration.UserRequest'
	AND C.name = 'ExternalId'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE [Data.Migration.UserRequest] 
		ALTER COLUMN [ExternalId] NVARCHAR(200) NOT NULL 
	ALTER TABLE [Data.Migration.UserRequest] 
		ADD CONSTRAINT [DF_Data.Migration.UserRequest_ExternalId] DEFAULT '' FOR [ExternalId] 
END

-- Add index to RecordType + Status columns

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.EmployerRequest_RecordType1' AND object_id = OBJECT_ID('[Data.Migration.EmployerRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.EmployerRequest_RecordType1]
		ON [Data.Migration.EmployerRequest] (RecordType, [Status])
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.JobOrderRequest_RecordType1' AND object_id = OBJECT_ID('[Data.Migration.JobOrderRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.JobOrderRequest_RecordType1]
		ON [Data.Migration.JobOrderRequest] (RecordType, [Status])
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.JobSeekerRequest_RecordType1' AND object_id = OBJECT_ID('[Data.Migration.JobSeekerRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.JobSeekerRequest_RecordType1]
		ON [Data.Migration.JobSeekerRequest] (RecordType, [Status])
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.UserRequest_RecordType1' AND object_id = OBJECT_ID('[Data.Migration.UserRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.UserRequest_RecordType1]
		ON [Data.Migration.UserRequest] (RecordType, [Status])
END

-- Add index to RecordType + ExternalId columns

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.EmployerRequest_RecordType2' AND object_id = OBJECT_ID('[Data.Migration.EmployerRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.EmployerRequest_RecordType2]
		ON [Data.Migration.EmployerRequest] (RecordType, ExternalId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.JobOrderRequest_RecordType2' AND object_id = OBJECT_ID('[Data.Migration.JobOrderRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.JobOrderRequest_RecordType2]
		ON [Data.Migration.JobOrderRequest] (RecordType, ExternalId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.JobSeekerRequest_RecordType2' AND object_id = OBJECT_ID('[Data.Migration.JobSeekerRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.JobSeekerRequest_RecordType2]
		ON [Data.Migration.JobSeekerRequest] (RecordType, ExternalId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.UserRequest_RecordType2' AND object_id = OBJECT_ID('[Data.Migration.UserRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.UserRequest_RecordType2]
		ON [Data.Migration.UserRequest] (RecordType, ExternalId)
END

-- Add index to RecordType + Status + BatchNumber columns
IF EXISTS
(
	SELECT 1 FROM sys.indexes I 
	INNER JOIN sys.index_columns IC 
		ON IC.index_id = I.index_id 
		AND IC.object_id = I.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = IC.object_id
	WHERE I.[name] = 'IX_Data.Migration.EmployerRequest_RecordType3'
		AND C.[name] = 'ExternalId'
)
BEGIN
	DROP INDEX [Data.Migration.EmployerRequest].[IX_Data.Migration.EmployerRequest_RecordType3]
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.EmployerRequest_RecordType3' AND object_id = OBJECT_ID('[Data.Migration.EmployerRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.EmployerRequest_RecordType3]
		ON [Data.Migration.EmployerRequest] (RecordType, [Status], BatchNumber)
END

IF EXISTS
(
	SELECT 1 FROM sys.indexes I 
	INNER JOIN sys.index_columns IC 
		ON IC.index_id = I.index_id 
		AND IC.object_id = I.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = IC.object_id
	WHERE I.[name] = 'IX_Data.Migration.JobOrderRequest_RecordType3'
		AND C.[name] = 'ExternalId'
)
BEGIN
	DROP INDEX [Data.Migration.JobOrderRequest].[IX_Data.Migration.JobOrderRequest_RecordType3]
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.JobOrderRequest_RecordType3' AND object_id = OBJECT_ID('[Data.Migration.JobOrderRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.JobOrderRequest_RecordType3]
		ON [Data.Migration.JobOrderRequest] (RecordType, [Status], BatchNumber)
END

IF EXISTS
(
	SELECT 1 FROM sys.indexes I 
	INNER JOIN sys.index_columns IC 
		ON IC.index_id = I.index_id 
		AND IC.object_id = I.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = IC.object_id
	WHERE I.[name] = 'IX_Data.Migration.JobSeekerRequest_RecordType3'
		AND C.[name] = 'ExternalId'
)
BEGIN
	DROP INDEX [Data.Migration.JobSeekerRequest].[IX_Data.Migration.JobSeekerRequest_RecordType3]
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.JobSeekerRequest_RecordType3' AND object_id = OBJECT_ID('[Data.Migration.JobSeekerRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.JobSeekerRequest_RecordType3]
		ON [Data.Migration.JobSeekerRequest] (RecordType, [Status], BatchNumber)
END

IF EXISTS
(
	SELECT 1 FROM sys.indexes I 
	INNER JOIN sys.index_columns IC 
		ON IC.index_id = I.index_id 
		AND IC.object_id = I.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = IC.object_id
	WHERE I.[name] = 'IX_Data.Migration.UserRequest_RecordType3'
		AND C.[name] = 'ExternalId'
)
BEGIN
	DROP INDEX [Data.Migration.UserRequest].[IX_Data.Migration.UserRequest_RecordType3]
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.UserRequest_RecordType3' AND object_id = OBJECT_ID('[Data.Migration.UserRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.UserRequest_RecordType3]
		ON [Data.Migration.UserRequest] (RecordType, [Status], BatchNumber)
END

-- Add index to RecordType + ParentExternalId columns

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.EmployerRequest_RecordType4' AND object_id = OBJECT_ID('[Data.Migration.EmployerRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.EmployerRequest_RecordType4]
		ON [Data.Migration.EmployerRequest] (RecordType, ParentExternalId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.JobOrderRequest_RecordType4' AND object_id = OBJECT_ID('[Data.Migration.JobOrderRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.JobOrderRequest_RecordType4]
		ON [Data.Migration.JobOrderRequest] (RecordType, ParentExternalId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.JobSeekerRequest_RecordType4' AND object_id = OBJECT_ID('[Data.Migration.JobSeekerRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.JobSeekerRequest_RecordType4]
		ON [Data.Migration.JobSeekerRequest] (RecordType, ParentExternalId)
END

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Migration.UserRequest_RecordType4' AND object_id = OBJECT_ID('[Data.Migration.UserRequest]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Migration.UserRequest_RecordType4]
		ON [Data.Migration.UserRequest] (RecordType, ParentExternalId)
END