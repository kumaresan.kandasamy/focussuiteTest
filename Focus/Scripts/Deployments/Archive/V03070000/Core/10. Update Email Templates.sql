UPDATE 
	[Config.EmailTemplate] 
SET
	[Subject] = 'Notification of edited posting',
	Body = 'Thank you for submitting your posting. 
	
Our team have reviewed the contents of your posting and have recommended some changes. 

Please follow the URL below to access your job posting and the amendments within your #FOCUSTALENT# account. If you are in agreement with the changes, please re-post your job.

#JOBLINKURL#

If you would like to discuss further, our support team can be contacted at #SUPPORTEMAIL# or #SUPPORTPHONE#. #SUPPORTHOURSOFBUSINESS#

Regards,

#SENDERNAME#'
WHERE 
	EmailTemplateType = 8