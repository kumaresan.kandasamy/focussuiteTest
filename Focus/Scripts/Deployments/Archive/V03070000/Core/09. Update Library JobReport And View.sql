-- Script may have been run on 03.06 already but this SQL will check for that

IF NOT EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'HiringTrendSubLevel' AND [object_id] = OBJECT_ID(N'[Library.JobReport]'))
BEGIN
	ALTER TABLE [Library.JobReport] ADD HiringTrendSubLevel NVARCHAR(4) NOT NULL DEFAULT ''
	Print 'Column HiringTrendSubLevel added to [Library.JobReport]'
END

/****** Object:  View [dbo].[Library.JobReportView]    Script Date: 02/11/2014 11:53:33 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Library.JobReportView]'))
DROP VIEW [dbo].[Library.JobReportView]
GO
/****** Object:  View [dbo].[Library.JobReportView]    Script Date: 02/11/2014 11:53:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Library.JobReportView]
AS
SELECT 
	JobReport.Id,
	JobReport.JobId,
	Job.Name,
	Job.DegreeIntroStatement,
	JobReport.StateAreaId,
	JobReport.HiringTrend,
	JobReport.HiringTrendPercentile,
	JobReport.HiringTrendSubLevel, 
	JobReport.HiringDemand,
	JobReport.GrowthTrend,
	JobReport.GrowthPercentile,
	JobReport.SalaryTrend,
	JobReport.SalaryTrendPercentile,
	JobReport.SalaryTrendAverage,
	JobReport.SalaryTrendMin,
	JobReport.SalaryTrendMax,
	JobReport.SalaryTrendRealtime,
	JobReport.SalaryTrendRealtimeAverage,
	JobReport.SalaryTrendRealtimeMin,
	JobReport.SalaryTrendRealtimeMax,
	Job.ROnet,
	Job.StarterJob
FROM
	[Library.JobReport] AS JobReport WITH (NOLOCK)
	INNER JOIN [Library.Job] AS Job WITH (NOLOCK) ON JobReport.JobId = Job.Id