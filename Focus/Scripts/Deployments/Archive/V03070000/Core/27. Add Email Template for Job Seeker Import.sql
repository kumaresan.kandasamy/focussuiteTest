IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 35)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		35, 
		'#FREETEXT# account verification required',
		'Please follow the link below to reset your password on your #FREETEXT# account.

#URL#
', 
		'', 
		NULL
	)
END
