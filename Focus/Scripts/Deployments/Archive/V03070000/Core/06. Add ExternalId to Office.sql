
-- Add column ExternalId of type String to table Data.application.office
ALTER TABLE [Data.application.office] ADD [ExternalId] NVARCHAR(36) NULL;
