UPDATE
	CI
SET
	ExternalId = CASE CI.[Key] WHEN 'EthnicHeritage.NotDisclosed' THEN '-1' ELSE '0' END
FROM
	[Config.CodeItem] CI
WHERE
	CI.[Key] IN ('EthnicHeritage.NotDisclosed', 'EthnicHeritage.NonHispanicLatino')

UPDATE
	CI
SET
	ExternalId = '-1'
FROM
	[Config.CodeItem] CI
WHERE
	CI.[Key] IN ('Race.NotDisclosed')
		