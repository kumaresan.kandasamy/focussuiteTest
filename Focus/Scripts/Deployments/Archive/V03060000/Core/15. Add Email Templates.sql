IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 33)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		[Subject], 
		Body, 
		Salutation, 
		Recipient
	)
	VALUES 
	(
		33, 
		'Message from #FOCUSTALENT#; Job order #JOBTITLE#. ID:#JOBID# has closed',
		'Your job order has now closed.
It is available to be refreshed for the next 30 days. Click on the link below to take further action

#JOBLINKURL#
', 
		'', 
		NULL
	)
END
