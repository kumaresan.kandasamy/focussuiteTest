BEGIN TRANSACTION

UPDATE [Config.LocalisationItem] SET Value = '#CANDIDATETYPE# issues' WHERE [Key] = 'FeedbackIssues.JobSeekerIssues'
UPDATE [Config.LocalisationItem] SET Value = 'Can''t refer #CANDIDATETYPES#' WHERE [Key] = 'StaffIssues.CantReferJobSeekers'
UPDATE [Config.LocalisationItem] SET Value = '#CANDIDATETYPE# can''t sign into account' WHERE [Key] = 'JobSeekerIssues.SeekerCantSignIntoAccount'
UPDATE [Config.LocalisationItem] SET Value = 'Can''t find more #CANDIDATETYPES# like this' WHERE [Key] = 'EmployerIssues.CantFindMoreSeekersLikeThis'
UPDATE [Config.LocalisationItem] SET Value = '#CANDIDATETYPE# account registration' WHERE [Key] = 'JobSeekerIssues.SeekerAccountRegistration'

COMMIT TRANSACTION