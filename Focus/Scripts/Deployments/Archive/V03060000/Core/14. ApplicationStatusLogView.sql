IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.ApplicationStatusLogView]'))
DROP VIEW [dbo].[Data.Application.ApplicationStatusLogView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.ApplicationStatusLogView]
AS
SELECT 
	StatusLog.Id AS Id, 
	StatusLog.ActionedOn AS ActionedOn, 
	[Application].ApplicationScore AS CandidateApplicationScore, 
    [Resume].FirstName AS CandidateFirstName, 
    [Resume].LastName AS CandidateLastName, 
    [Resume].YearsExperience AS CandidateYearsExperience, 
    [Resume].IsVeteran AS CandidateIsVeteran, 
    StatusLog.NewStatus AS CandidateApplicationStatus, 
    Posting.JobId AS JobId, 
    LatestJob.JobTitle AS LatestJobTitle, 
    LatestJob.Employer AS LatestJobEmployer, 
    LatestJob.StartYear AS LatestJobStartYear, 
    LatestJob.EndYear AS LatestJobEndYear, 
    [Application].Id AS CandidateApplicationId, 
    Job.BusinessUnitId AS BusinessUnitId, 
    Job.JobTitle AS JobTitle,
    [Resume].PersonId,
    [Application].ApprovalStatus AS CandidateApprovalStatus,
    Job.VeteranPriorityEndDate
FROM  
	[Data.Core.StatusLog] AS StatusLog WITH (NOLOCK) 
	INNER JOIN [Data.Application.Application] AS [Application] WITH (NOLOCK) ON StatusLog.EntityId = [Application].Id 
	INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id 
	INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN [Data.Application.Job] AS Job WITH (NOLOCK) ON Job.Id = [Posting].JobId 
	LEFT OUTER JOIN (SELECT JobTitle, Employer, StartYear, EndYear, ResumeId
                    FROM (SELECT JobTitle, Employer, StartYear, EndYear, ResumeId, RANK() OVER (PARTITION BY ResumeId
                        ORDER BY EndYear DESC, StartYear DESC, Id) RankScore
                    FROM  [Data.Application.ResumeJob] WITH (NOLOCK)) AS RankedCandidateWorkHistory
                    WHERE RankScore = 1) AS LatestJob ON [Resume].Id = LatestJob.ResumeId



GO


