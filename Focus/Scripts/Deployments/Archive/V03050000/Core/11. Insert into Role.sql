IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'ACourtOrderedAffirmativeActionApprover')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'ACourtOrderedAffirmativeActionApprover', 'Assist Court Ordered Affirmative Action Approver')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AFederalContractorApprover')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AFederalContractorApprover', 'Assist Federal Contractor Approver')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AForeignLabourH2AAgApprover')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AForeignLabourH2AAgApprover', 'Assist Foreign Labour H2A Ag Approver')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AForeignLabourH2BNonAgApprover')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AForeignLabourH2BNonAgApprover', 'Assist Foreign Labour H2B Non Ag Approver')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'AForeignLabourOtherApprover')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'AForeignLabourOtherApprover', 'Assist Foreign Labour Other Approver')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'ACommissionOnlyApprover')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'ACommissionOnlyApprover', 'Assist Commission Only Approver')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'ASalaryAndCommissionApprover')
BEGIN
  INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]), 'ASalaryAndCommissionApprover', 'Assist Salary And Commission Approver')
END