IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'IsSalaryAndCommissionBased')
BEGIN
	ALTER TABLE [Data.Application.Job] ADD [IsSalaryAndCommissionBased] BIT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'VeteranPriorityEndDate')
BEGIN
	ALTER TABLE [Data.Application.Job] ADD VeteranPriorityEndDate DATETIME NULL
END


