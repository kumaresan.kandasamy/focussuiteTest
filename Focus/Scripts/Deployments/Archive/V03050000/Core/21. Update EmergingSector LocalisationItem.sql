DECLARE @LocalisationId bigint

SELECT @LocalisationId = [Id] FROM [Config.Localisation] WHERE Culture = '**-**'

/*** UPDATE EXISTING ITEMS ***/

BEGIN TRANSACTION

DECLARE @UpdateItems TABLE
(
	[Key] NVARCHAR(400),
	[OldValue] NVARCHAR(1000),
	[NewValue] NVARCHAR(1000)
)

INSERT INTO @UpdateItems ([Key], [OldValue], [NewValue])
VALUES
('EmergingSector.TransportDistribute', 'Transportation, Distribution & Logistics', 'Transportation & Distribution'),
('EmergingSector.TransportDistribute', 'Transport/Distribute', 'Transportation & Distribution')

UPDATE 
	LI
SET 
	[Value] = TI.NewValue
FROM 
	[Config.LocalisationItem] LI
INNER JOIN @UpdateItems TI
	ON TI.[Key] = LI.[Key]
WHERE 
	LI.LocalisationId = @LocalisationId
	AND LI.Value = TI.OldValue

COMMIT TRANSACTION
