-- Add table Data.Application.PersonsCurrentOffice to the database
CREATE TABLE [Data.Application.PersonsCurrentOffice] (Id BIGINT NOT NULL PRIMARY KEY ,
[StartTime] DATETIME NOT NULL,
[PersonId] BIGINT NOT NULL DEFAULT 0,
[OfficeId] BIGINT NOT NULL DEFAULT 0,
FOREIGN KEY (PersonId) REFERENCES [Data.Application.Person](Id),
FOREIGN KEY (OfficeId) REFERENCES [Data.Application.Office](Id));