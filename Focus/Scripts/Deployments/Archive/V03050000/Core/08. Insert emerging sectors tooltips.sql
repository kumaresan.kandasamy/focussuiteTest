-- Get rid of any existing data that may be incorrect
DELETE FROM [dbo].[Config.LocalisationItem] WHERE [Key] like 'EmergingSector%ToolTip'

-- Insert the new tooltips where the matching checkbox exists
INSERT INTO [dbo].[Config.LocalisationItem]
           ([ContextKey]
           ,[Key]
           ,[Value]
           ,[Localised]
           ,[LocalisationId])
     SELECT
           ''
           ,[Key] + '.ToolTip'
           ,CASE [Key]
				WHEN 'EmergingSector.TransportDistribute'
					THEN 'Jobs involving the planning, management, and movement of people, materials, and products by road, air, rail, and water. These include related professional and technical services such as infrastructure planning and management, logistics, warehousing, and maintenance of equipment and facilities.'
				WHEN 'EmergingSector.SMART'
					THEN 'Jobs involving science, mathematics, research, technology and engineering. SMART sector jobs are aligned with opportunities in government, military, and corporate markets, and also may be aligned with designated colleges and universities offering SMART curricula.'
				WHEN 'EmergingSector.ResearchandDevelopment'
					THEN 'Jobs involving the development of new products, knowledge, processes, and services, differing significantly from one model or industry to the next. R&D activities generally staff engineers and scientists who may work in both corporate or government sectors.'
				WHEN 'EmergingSector.InformationTechnology'
					THEN 'Jobs involving the application of computers and telecommunications equipment to store, retrieve, transmit, and manipulate data often in a business context. It encompasses industries associated with computer hardware, software, electronics, semi-conductors, internet, telecom equipment, e-commerce, computer services, and distributive technologies such as television and communications.'
				WHEN 'EmergingSector.HealthInformatics'
					THEN 'Jobs involving the intersecting disciplines of information science, computer science, and healthcare. They deal with the resources, devices, and methods required to optimize the acquisition, storage, retrieval and use of information in healthcare and biomedicine. May also apply to nursing, clinical care, dentistry, pharmacy, public health, occupational therapy, and biomedical research.'
				WHEN 'EmergingSector.Healthcare'
					THEN 'Jobs involving the provision of goods and services to treat patients with curative, preventative, rehabilitative, and palliative care. Today�s healthcare sector has many sub-sectors and depends on interdisciplinary teams of trained professionals and para-professionals that serve various populations.'
				WHEN 'EmergingSector.Green'
					THEN 'Jobs involving the policies, information, materials, technologies and promotion of operations that contribute to minimizing environmental impact; conserving energy; developing alternative, sustainable or high-efficiency energy sources; controlling pollution; developing organic chemical replacements; and recycling or reducing waste.'
				WHEN 'EmergingSector.Energy'
					THEN 'Jobs involving the extraction, production, manufacture, sale, and maintenance of energy resources, fuels, and systems that fulfill the need for power, including petroleum, natural gas, electrical, hydroelectric, coal, nuclear, solar, wind, and alternative fuels.'
				WHEN 'EmergingSector.Consulting'
					THEN 'Jobs involving the provision of opinion, advice, counsel, or targeted services, covering a wide range of professions such as biotechnology, engineering, environmental, faculty, financial, franchising, human resources, information technology, legal, management, performance, personal dynamics, politics, public relations, manufacturing, supply-chain.'
				WHEN 'EmergingSector.BusinessServices'
					THEN 'Jobs involving the management of business-information technology alignment that promotes a customer-centric approach to service delivery through strategic planning, operations, and continuous improvement.'
				WHEN 'EmergingSector.BioTechnology'
					THEN 'Jobs involving the development and modification of useful products from living organisms with applications in genomics, recombinant gene technologies, applied immunology, pharmaceutical therapies, diagnostic testing, medicine, agriculture, and food production.'
				WHEN 'EmergingSector.Aerospace'
					THEN 'Jobs involving research, design, manufacture, operation, or maintenance of aeronautic and astronautic vehicles with diverse commercial, industrial, and military applications.'
				WHEN 'EmergingSector.AdvancedManufacturing'
					THEN 'Jobs involving the innovative use of technology to improve products or processes.'
			END
           ,0
           ,[dbo].[Config.Localisation].[Id]
      FROM [dbo].[Config.LocalisationItem]
      CROSS JOIN [dbo].[Config.Localisation]
      WHERE [Culture] = '**-**' AND [Key] like 'EmergingSector%'
GO


