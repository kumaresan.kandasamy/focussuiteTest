

-- Add column RedProfanityWords of type String to table Data.application.employee
ALTER TABLE [Data.application.employee] ADD [RedProfanityWords] NVARCHAR(MAX) NULL;
-- Add column YellowProfanityWords of type String to table Data.application.employee
ALTER TABLE [Data.application.employee] ADD [YellowProfanityWords] NVARCHAR(MAX) NULL;
