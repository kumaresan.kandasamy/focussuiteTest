IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'EducationCompletionDate')
BEGIN
	ALTER TABLE
		[Data.Application.Resume]
	ADD
		EducationCompletionDate DATETIME NULL
END
GO
