IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.EmployeeEmail')
BEGIN
	CREATE TABLE [Data.Application.EmployeeEmail]
	(
		Id BIGINT NOT NULL PRIMARY KEY,
		EmployeeId BIGINT NOT NULL,
		EmailText NVARCHAR(MAX) NOT NULL,
		EmailSentOn DATETIME NOT NULL,
		EmailSentBy BIGINT NOT NULL,
		ApprovalStatus INT NULL,
		FOREIGN KEY (EmployeeId) REFERENCES [Data.Application.Employee](Id)
	)
END