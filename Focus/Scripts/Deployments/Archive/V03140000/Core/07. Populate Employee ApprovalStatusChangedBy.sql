WITH Employees AS
(
	SELECT 
		E.Id,
		AE.UserId,
		TA.ActionedOn
	FROM 
		[Data.Application.Employee] E
	CROSS APPLY
	(
		SELECT
			T2.Id AS ActionTypeId,
			MAX(AE2.ActionedOn) AS ActionedOn
		FROM 
			[Data.Core.ActionEvent] AE2
		INNER JOIN [Data.Core.ActionType] T2
			ON T2.Id = AE2.ActionTypeId 
		WHERE
			AE2.EntityId = E.Id
			AND T2.[Name] = 'HoldEmployeeReferral'
		GROUP BY
			T2.Id
	) TA
	INNER JOIN [Data.Core.ActionEvent] AE
		ON AE.EntityId = E.Id
		AND AE.ActionTypeId = TA.ActionTypeId
		AND AE.ActionedOn = TA.ActionedOn
	WHERE
		E.ApprovalStatus = 4
)
UPDATE
	E
SET
	ApprovalStatusChangedBy = E1.UserId,
	ApprovalStatusChangedOn = E1.ActionedOn
FROM
	[Data.Application.Employee] E
INNER JOIN Employees E1
	ON E1.Id = E.Id
WHERE
	E.ApprovalStatusChangedBy IS NULL
GO
