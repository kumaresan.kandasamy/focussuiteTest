SET DATEFORMAT YMD

CREATE TABLE #Resume
(
	ResumeId INT PRIMARY KEY,
	[Resume] XML
)

CREATE TABLE #ResumeDates
(
	ResumeId INT PRIMARY KEY,
	CompletionDate NVARCHAR(10),
	ExpectedCompletionDate NVARCHAR(10)
)

INSERT INTO #Resume 
(
	ResumeId,
	[Resume]
)
SELECT
	R.Id,
	CAST(R.ResumeXml AS XML)
FROM 
	dbo.[Data.Application.Resume] R
WHERE
	R.StatusId = 1

;WITH Resumes (ResumeId, CompletionDate, ExpectedCompletionDate) AS
(
	SELECT
		R.ResumeId,
		c.value('completiondate[1]', 'NVARCHAR(7)') AS CompletionDate,
		c.value('expectedcompletiondate[1]', 'NVARCHAR(7)') AS ExpectedCompletionDate
	FROM
		#Resume R
	CROSS APPLY R.[Resume].nodes('//ResDoc/resume/education/school[completiondate != "" or expectedcompletiondate != ""]') as t(c)
)
INSERT INTO	#ResumeDates 
(
	ResumeId,
	CompletionDate,
	ExpectedCompletionDate
)
SELECT
	TR.ResumeId,
	MAX(RIGHT(TR.CompletionDate, 4) + '-' + LEFT(TR.CompletionDate, 2) + '-01'),
	MAX(RIGHT(TR.ExpectedCompletionDate, 4) + '-' + LEFT(TR.ExpectedCompletionDate, 2) + '-01')
FROM
	Resumes TR
GROUP BY
	TR.ResumeId
	
UPDATE
	R
SET
	EducationCompletionDate = 
		CASE ISDATE(ISNULL(TR.CompletionDate, TR.ExpectedCompletionDate))
			WHEN 1 THEN CAST(ISNULL(TR.CompletionDate, TR.ExpectedCompletionDate) AS DATETIME)
		END
FROM
	[Data.Application.Resume] R
INNER JOIN #ResumeDates TR
	ON TR.ResumeId = R.Id
	
DROP TABLE #Resume
DROP TABLE #ResumeDates