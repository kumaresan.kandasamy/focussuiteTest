IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Employee' AND COLUMN_NAME = 'ApprovalStatusChangedBy')
BEGIN
	ALTER TABLE
		[Data.Application.Employee]
	ADD
		ApprovalStatusChangedBy BIGINT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Employee' AND COLUMN_NAME = 'ApprovalStatusChangedOn')
BEGIN
	ALTER TABLE
		[Data.Application.Employee]
	ADD
		ApprovalStatusChangedOn DATETIME NULL
END
GO

