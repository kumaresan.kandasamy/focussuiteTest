IF TYPE_ID(N'ConfigTableType') IS NULL
	CREATE TYPE [dbo].[ConfigTableType] AS TABLE
	(
		[Key] [nvarchar](200) NOT NULL,
		[Value] [nvarchar](2000) NULL,
		[InternalOnly] [bit] NULL,
		PRIMARY KEY CLUSTERED 
		( [Key] ASC ) WITH (IGNORE_DUP_KEY = OFF)
	)