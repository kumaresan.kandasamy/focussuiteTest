DECLARE @Items TABLE
(
	[Key] NVARCHAR(200),
	[Value] NVARCHAR(4000)
)
INSERT INTO @Items([Key], [Value])
VALUES
('Focus.Web.WebAssist.Controls.JobReferralSummaryList.NoMatches.Text:Education', 'No applications have been received'),
('Focus.Web.WebAssist.Pool.AllResumesTab.Label:Education', 'Search resumes'),
('Focus.Web.WebAssist.Pool.MatchesForThisJob.Label:Education', 'Program of study matches')

DECLARE @LocalisationId BIGINT
SELECT @LocalisationId = Id FROM [Config.Localisation] WHERE Culture = '**-**'

INSERT INTO [Config.LocalisationItem] 
(
	ContextKey, 
	[Key], 
	Value, 
	Localised, 
	LocalisationId
)
SELECT
	'',
	I.[Key],
	I.[Value],
	0,
	@LocalisationId
FROM
	@Items I
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.LocalisationItem] LI
		WHERE
			LI.[Key] = I.[Key]
			AND LI.LocalisationId = @LocalisationId
	)
	
