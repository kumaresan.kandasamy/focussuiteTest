IF 'YES' = (SELECT IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'SubscribeToVeteranPriorityServiceAlerts')
BEGIN
	DECLARE @Sql NVARCHAR(500)

	SELECT @Sql = 'ALTER TABLE [' + T.name + '] DROP CONSTRAINT [' + D.Name + ']'
	FROM sys.tables T
	INNER JOIN sys.default_constraints D
		ON D.parent_object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.object_id = T.object_id
		AND C.column_id = D.parent_column_id
	WHERE T.name = 'Data.Application.Resume'
	AND C.name = 'SubscribeToVeteranPriorityServiceAlerts'

	IF @Sql IS NOT NULL
		EXECUTE sp_executeSql @Sql
		
	ALTER TABLE
		[Data.Application.Resume]
	DROP COLUMN 
		SubscribeToVeteranPriorityServiceAlerts
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'VeteranPriorityServiceAlertsSubscription')
BEGIN
	ALTER TABLE
		[Data.Application.Resume]
	ADD 
		VeteranPriorityServiceAlertsSubscription BIT NOT NULL DEFAULT(1)
END
