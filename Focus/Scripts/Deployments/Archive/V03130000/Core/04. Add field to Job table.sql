IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'MeetsMinimumWageRequirement')
BEGIN
	ALTER TABLE
		[Data.Application.Job]
	ADD
		MeetsMinimumWageRequirement BIT NOT NULL DEFAULT(0)
END
