IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.PhoneNumber_PersonId' AND object_id = OBJECT_ID('[Data.Application.PhoneNumber]'))
BEGIN
	CREATE NONCLUSTERED INDEX 
		[IX_Data.Application.PhoneNumber_PersonId]
	ON 
		[Data.Application.PhoneNumber] (PersonId, IsPrimary)
END

