IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Core.ActionEvent_ActionTypeId' AND object_id = OBJECT_ID('[Data.Core.ActionEvent]'))
BEGIN
	CREATE NONCLUSTERED INDEX 
		[IX_Data.Core.ActionEvent_ActionTypeId]
	ON 
		[Data.Core.ActionEvent] (ActionTypeId)
END

