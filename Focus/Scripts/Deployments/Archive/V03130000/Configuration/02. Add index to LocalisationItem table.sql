IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Config.LocalisationItem_Key' AND object_id = OBJECT_ID('[Config.LocalisationItem]'))
BEGIN
  CREATE INDEX 
    [IX_Config.LocalisationItem_Key] 
  ON 
    [Config.LocalisationItem] ([Key])
END