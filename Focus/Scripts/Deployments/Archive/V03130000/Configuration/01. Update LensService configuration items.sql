UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '"customFilters"', '"newjobspostingdelaydays":1,"customFilters"')
WHERE [Key] = 'LensService'
	AND Value LIKE '%"serviceType":"Posting"%'
	AND (Value LIKE '%"vendor":"spidered%' OR Value LIKE '%"description":"Spidered%')
	AND NOT Value LIKE '%"newjobspostingdelaydays"%'