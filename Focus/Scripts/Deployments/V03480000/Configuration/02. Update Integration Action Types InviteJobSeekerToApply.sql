﻿IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient') = 'Georgia'
BEGIN
	UPDATE
		[Config.ConfigurationItem]
	SET
		Value = Value + ',InviteJobSeekerToApply'
	WHERE
		[Key] = 'IntegrationLoggableActions'
		AND Value NOT LIKE '%InviteJobSeekerToApply%'
END

