﻿
 IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 50)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType, 
		Recipient,
		Salutation,
		[Subject], 
		Body,
		SenderEmailType
	)
	VALUES 
	(
		50, 
		NULL,
		'Dear #RECIPIENTNAME#', 
		'Notification of posting referral', 
		'We have reviewed your resume and deem your skills and experience suitable to apply for the position of #JOBTITLE# at #EMPLOYERNAME#. 
		A referral has been made on your behalf.

To view the full job posting, please access the following web page; #JOBLINKURL#
You can also view the interview contact information for this posting from your #FOCUSCAREER# homepage.

Regards, 

#SENDERNAME#',
0
	)
END