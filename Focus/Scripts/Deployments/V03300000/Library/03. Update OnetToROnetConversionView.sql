﻿IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Library.OnetToROnetConversionView')
BEGIN
	DROP VIEW [Library.OnetToROnetConversionView]
END
GO

CREATE VIEW [dbo].[Library.OnetToROnetConversionView]
AS
SELECT
	oro.Id,
	oro.OnetId,
	o.OnetCode,
	o.[Key] As OnetKey,
	oro.ROnetId,
	r.Code AS ROnetCode,
	r.[Key] AS ROnetKey,
	oro.BestFit
FROM 
	[Library.Onet] o
INNER JOIN [Library.OnetROnet] oro on o.Id = oro.OnetId
INNER JOIN [Library.ROnet] r ON r.Id = oro.ROnetId
GO
