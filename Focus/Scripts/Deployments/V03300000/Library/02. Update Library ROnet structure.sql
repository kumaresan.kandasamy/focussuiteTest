﻿DECLARE @ConstraintName nvarchar(200)

IF EXISTS (	SELECT	* FROM INFORMATION_SCHEMA.Columns
			WHERE	[Table_Name] = N'Library.ROnet'
			AND		[Column_Name] = N'NumericCode'	)
BEGIN
	SELECT @ConstraintName = Name 
	FROM SYS.DEFAULT_CONSTRAINTS
	WHERE PARENT_OBJECT_ID = OBJECT_ID('[Library.ROnet]') 
	AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = 'NumericCode' AND object_id = OBJECT_ID('[Library.ROnet]'))
	
	IF @ConstraintName IS NOT NULL
		EXEC('ALTER TABLE [Library.ROnet] DROP CONSTRAINT [' + @ConstraintName + ']')
	
	ALTER TABLE [Library.ROnet]
	DROP COLUMN NumericCode
END
