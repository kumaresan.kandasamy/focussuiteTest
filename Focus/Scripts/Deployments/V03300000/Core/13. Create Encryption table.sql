﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.Encryption')
BEGIN

	CREATE TABLE [Data.Application.Encryption] (
	Id BIGINT NOT NULL PRIMARY KEY ,
	[EntityTypeId] INT NOT NULL DEFAULT 0,
	[EntityId] BIGINT NOT NULL DEFAULT 0,
	[Vector] NVARCHAR(MAX) NOT NULL DEFAULT '',
	[TargetTypeId] INT NOT NULL DEFAULT 0);

END