﻿IF NOT EXISTS (	SELECT	* FROM INFORMATION_SCHEMA.Columns
			WHERE	[Table_Name] = N'Data.Application.Office'
			AND		[Column_Name] = N'UnassignedDefault'	)
BEGIN
	ALTER TABLE	[Data.Application.Office]
	ADD			UnassignedDefault bit NOT NULL DEFAULT (0)
END
