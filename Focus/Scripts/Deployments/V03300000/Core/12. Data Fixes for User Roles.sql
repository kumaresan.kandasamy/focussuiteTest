--DROP TABLE #DuplicateRoles

IF OBJECT_ID('tempdb..#DuplicateRoles') IS NOT NULL
BEGIN
   DROP TABLE #DuplicateRoles
END

CREATE TABLE #DuplicateRoles
(
	RoleId BIGINT,
	DeleteRoleId BIGINT
)

INSERT INTO #DuplicateRoles
(
	DeleteRoleId,
	RoleId
)
SELECT
	R2.Id,
	R.Id
FROM
	[Data.Application.Role] R 
INNER JOIN [Data.Application.Role] R2
	ON R2.[Key] = R.[Key]
	AND R2.Id > R.Id
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.Role] R3
		WHERE 
			R3.[Key] = R.[Key]
			AND R3.Id < R.Id
	)
	
-- Update user roles that were using the roles to be deleted so that they use the base role instead
UPDATE
	UR
SET
	RoleId = DR.RoleId
FROM
	[Data.Application.UserRole] UR
INNER JOIN #DuplicateRoles DR
	ON DR.DeleteRoleId = UR.RoleId

-- Delete any duplicate User Roles
DELETE
	UR
FROM
	[Data.Application.UserRole] UR
INNER JOIN #DuplicateRoles DR
	ON DR.RoleId = UR.RoleId
WHERE
	EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.UserRole] UR2
		WHERE
			UR2.UserId = UR.UserId
			AND UR2.RoleId = UR.RoleId
			AND UR2.Id < UR.Id
	)
	
-- Delete the duplicate roles
DELETE
	R
FROM
	[Data.Application.Role] R
INNER JOIN #DuplicateRoles DR
	ON DR.DeleteRoleId = R.Id
	
-- Add in some missing roles
DECLARE @NextID INT

IF NOT EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = 'AssistJobSeekersSendMessages')
BEGIN
	SELECT @NextID = NextId FROM [dbo].[KeyTable]
	
	INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
	VALUES (@NextID , 'AssistJobSeekersSendMessages', 'Assist Job Seekers Send Messages')
	
	UPDATE [KeyTable] SET NextId = NextId + 1
END

IF NOT EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = 'AssistEmployersSendMessages')
BEGIN
	SELECT @NextID = NextId FROM [dbo].[KeyTable]
	
	INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
	VALUES (@NextID, 'AssistEmployersSendMessages', 'Assist Employers Send Messages')
	
	UPDATE [KeyTable] SET NextId = NextId + 2
END

-- Update some existing role names
UPDATE
	[Data.Application.Role]
SET
	Value = 'Assist Offices List Staff'
WHERE
	[Key] = 'AssistOfficesListStaff'
	AND Value <> 'Assist Offices List Staff'
	
