IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC1
		ON IC1.index_id = I.index_id
		AND IC1.object_id = T.object_id
	INNER JOIN sys.columns C1
		ON C1.column_id = IC1.column_id
		AND C1.object_id = T.object_id
	INNER JOIN sys.index_columns IC2
		ON IC2.index_id = I.index_id
		AND IC2.object_id = T.object_id
	INNER JOIN sys.columns C2
		ON C2.column_id = IC2.column_id
		AND C2.object_id = T.object_id
	WHERE T.name = 'Data.Core.ActionEvent'
		AND C1.name = 'ActionTypeId'
		AND IC1.key_ordinal = 1
		AND C2.name = 'EntityId'
		AND IC2.key_ordinal = 2)
BEGIN
	CREATE INDEX 
		[IX.Data.Core.ActionEvent_ActionTypeId_EntityId] 
	ON 
		[Data.Core.ActionEvent] (ActionTypeId, EntityId)
END
GO
