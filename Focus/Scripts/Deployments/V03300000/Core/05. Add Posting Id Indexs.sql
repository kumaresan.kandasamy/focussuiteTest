﻿IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.JobPostingOfInterestSent'
		AND C.name = 'PostingId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.JobPostingOfInterestSent_PostingId] 
	ON 
		[Data.Application.JobPostingOfInterestSent] (PostingId)
END
GO

IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.JobPostingOfInterestSent'
		AND C.name = 'JobId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.JobPostingOfInterestSent_JobId] 
	ON 
		[Data.Application.JobPostingOfInterestSent] (JobId)
END
GO

IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.JobPostingOfInterestSent'
		AND C.name = 'PersonId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.JobPostingOfInterestSent_PersonId] 
	ON 
		[Data.Application.JobPostingOfInterestSent] (PersonId)
END
GO

IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.PersonPostingMatch'
		AND C.name = 'PostingId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.PersonPostingMatch_PostingId] 
	ON 
		[Data.Application.PersonPostingMatch] (PostingId)
END
GO

IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.PersonPostingMatch'
		AND C.name = 'PersonId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.PersonPostingMatch_PersonId] 
	ON 
		[Data.Application.PersonPostingMatch] (PersonId)
END
GO

IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.PersonPostingMatchToIgnore'
		AND C.name = 'PostingId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.PersonPostingMatchToIgnore_PostingId] 
	ON 
		[Data.Application.PersonPostingMatchToIgnore] (PostingId)
END
GO

IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.PersonPostingMatchToIgnore'
		AND C.name = 'PersonId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.PersonPostingMatchToIgnore_PersonId] 
	ON 
		[Data.Application.PersonPostingMatchToIgnore] (PersonId)
END
GO


IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Application.PersonPostingMatchToIgnore'
		AND C.name = 'UserId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Data.Application.PersonPostingMatchToIgnore_UserId] 
	ON 
		[Data.Application.PersonPostingMatchToIgnore] (UserId)
END
GO