﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.person' AND COLUMN_NAME = 'Unsubscribed')
BEGIN
	ALTER TABLE [Data.application.person] ADD [Unsubscribed] BIT NULL;
END