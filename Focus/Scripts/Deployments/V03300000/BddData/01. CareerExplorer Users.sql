﻿DECLARE @JobSeekers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	EmailAddress NVARCHAR(255),
	PhoneNumber NVARCHAR(20),
	ProgamAreaId BIGINT,
	DegreeId BIGINT,
	SecurityQuestion NVARCHAR(255),
	SecurityAnswerEncrypted NVARCHAR(255),
	Vector NVARCHAR(100)
)

INSERT INTO @JobSeekers (FirstName, LastName, EmailAddress, PhoneNumber, ProgamAreaId, DegreeId, SecurityQuestion, SecurityAnswerEncrypted, Vector)
VALUES 
	('Security', 'Question', 'security@question.com', '1111111134', NULL, NULL, 'What is your favorite color?', 'aZ4jpMAKXPHBCT30xt/bDQ==', '493E5872-BF17-48')

DECLARE @SeekerIndex INT
DECLARE @SeekerCount INT

DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @PhoneNumberId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @IssuesId BIGINT

DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @EmailAddress NVARCHAR(255)
DECLARE @PhoneNumber NVARCHAR(20)
DECLARE @ProgamAreaId BIGINT
DECLARE @DegreeId BIGINT
DECLARE @SecurityQuestion NVARCHAR(255)
DECLARE @SecurityAnswerEncrypted NVARCHAR(255)
DECLARE @SecurityQuestionId NVARCHAR(10)
	
SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'CareerUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

SET @SeekerIndex = 1
SELECT @SeekerCount = COUNT(1) FROM @JobSeekers

WHILE @SeekerIndex <= @SeekerCount
BEGIN
	SELECT 
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@PhoneNumber = PhoneNumber,
		@ProgamAreaId = ProgamAreaId,
		@DegreeId = DegreeId,
		@SecurityQuestion = SecurityQuestion,
		@SecurityAnswerEncrypted = SecurityAnswerEncrypted
	FROM
		@JobSeekers
	WHERE
		Id = @SeekerIndex
		
	SELECT
		@SecurityQuestionId = CAST(Id AS NVARCHAR(10))
	FROM
		[Config.LookupItemsView] 
	WHERE
		LookupType = 'SecurityQuestions'
		AND Value = @SecurityQuestion
		
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 6
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @UserRoleId = @NextId + 4
		SET @IssuesId = @NextId + 5
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			ProgramAreaId,
			CampusId,
			DegreeId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			@ProgamAreaId,
			NULL,
			@DegreeId
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			@PhoneNumber,
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			SecurityQuestion,
			SecurityAnswerEncrypted,
			CreatedOn,
			UpdatedOn
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			12,
			1,
			@FirstName + ' ' + @LastName,
			1,
			1,
			ISNULL(@SecurityQuestionId, @SecurityQuestion),
			@SecurityAnswerEncrypted,
			GETDATE(),
			GETDATE()
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		VALUES
		(
			@UserRoleId,
			@RoleId,
			@UserId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.Issues] 
		(
			Id, 
			PersonId
		)
		VALUES
		(
			@IssuesId,
			@PersonId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	
	SET @SeekerIndex = @SeekerIndex + 1
END

SELECT @NextId = NextId FROM KeyTable

INSERT INTO [Data.Application.Encryption]
(
	Id,
	EntityTypeId,
	EntityId,
	Vector,
	TargetTypeId
)
SELECT
	@NextId,
	2,
	U.Id,
	'493E5872-BF17-48',
	3
FROM
	@JobSeekers JS
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = JS.EmailAddress
INNER JOIN [Data.Application.User] U
	ON U.PersonId = P.Id
WHERE
	NOT EXISTS
	(
		SELECT 
			1
		FROM
			[Data.Application.Encryption] E
		WHERE
			E.EntityTypeId = 2
			AND E.EntityId = U.Id
			AND E.TargetTypeId = 3
	)
	
UPDATE [KeyTable] SET NextId = NextId + @@ROWCOUNT
GO




	
