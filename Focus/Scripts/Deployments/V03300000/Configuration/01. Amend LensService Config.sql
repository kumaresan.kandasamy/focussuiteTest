/*
UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '{"key":"HomeBased","tag":"cf037"}', '{"key":"HomeBased","tag":"cf037"},{"key":"MaximumHoursPerWeek","tag":"cf039"}')
WHERE [Key] = 'LensService'
	AND Value LIKE '%customFilters%'
	AND Value LIKE '%"serviceType":"Posting"%'
	AND Value NOT LIKE '%MaximumHoursPerWeek%'

UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '{"key":"HomeBased","tag":"cf037"}', '{"key":"HomeBased","tag":"cf037"},{"key":"MinimumHoursPerWeek","tag":"cf038"}')
WHERE [Key] = 'LensService'
	AND Value LIKE '%customFilters%'
	AND Value LIKE '%"serviceType":"Posting"%'
	AND Value NOT LIKE '%MinimumHoursPerWeek%'
*/

-- Remove custom filters (not yet on live anyway) as client is changing requirements and they may not be needed
UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '{"key":"MaximumHoursPerWeek","tag":"cf039"}', '')
WHERE [Key] = 'LensService'
	AND Value LIKE '%customFilters%'
	AND Value LIKE '%"serviceType":"Posting"%'
	AND Value LIKE '%MaximumHoursPerWeek%'

UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '{"key":"MinimumHoursPerWeek","tag":"cf038"}', '')
WHERE [Key] = 'LensService'
	AND Value LIKE '%customFilters%'
	AND Value LIKE '%"serviceType":"Posting"%'
	AND Value LIKE '%MinimumHoursPerWeek%'
GO