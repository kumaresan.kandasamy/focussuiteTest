IF DB_NAME() NOT LIKE 'FocusBase%' AND NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'SharedEncryptionKey')
BEGIN
	INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
	VALUES ('SharedEncryptionKey', LEFT(NEWID(), 32), 1)
END
