﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.jobSeeker' AND COLUMN_NAME = 'Unsubscribed')
BEGIN
	ALTER TABLE [Report.jobSeeker] ADD [Unsubscribed] BIT NOT NULL DEFAULT 0;
END