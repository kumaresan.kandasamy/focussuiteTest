IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cum*')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'accumulate, accumulated, accumulates, accumulative, accumulatively, encumber, encumbered, encumbers, cumber, cumberland, cumbersome, cumbrance, cumbria, cumbrous, cumgranosalis, cumin, cum laude, cumlaude, cummerbund, cummings, cumquat, cumshaw, cumulate, cumulated, cumulating, cumulation, cumulative, cumulatively, cumliform, cumulonimbus, cumulous, cumuli, cumulus, cummins, cumming' where [Phrase] = 'cum*'
Print 'Updated White List Items for ''cum*'''
END
IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'beaver')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'angry beaver, beaver carwash, beaver creek, beavercreek, beaver crossing, beaver dam, beaver falls, beaver family, beaver general, beaver janitor, beaver lake, beaver road, beaver run, beaver stump, beaver''s bbq, eager beaver, eager beaver''s, mad beaver, beaver board, beaver board, beaver-board, beaver-boards' where [Phrase] = 'beaver'
Print 'Updated White List Items for ''beaver'''
END

