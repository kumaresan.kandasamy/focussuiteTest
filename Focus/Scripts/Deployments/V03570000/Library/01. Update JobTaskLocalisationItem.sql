BEGIN TRANSACTION

BEGIN TRY
	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'binders? board'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'binder''s board'
		WHERE PrimaryValue = 'binders? board'
	END
	ELSE
		PRINT 'binders? board Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'bindersn++ board'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'binder''s board'
		WHERE PrimaryValue = 'bindersn++ board'
	END
	ELSE
		PRINT 'bindersn++ board Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'client?s taste'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'client''s taste'
		WHERE PrimaryValue = 'client?s taste'
	END
	ELSE
		PRINT 'client?s taste Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'clientn++s taste'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'client''s taste'
		WHERE PrimaryValue = 'clientn++s taste'
	END
	ELSE
		PRINT 'clientn++s taste Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'QSR?s'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'QSR''s'
		WHERE PrimaryValue = 'QSR?s'
	END
	ELSE
		PRINT 'QSR?s Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'QSRn++s'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'QSR''s'
		WHERE PrimaryValue = 'QSRn++s'
	END
	ELSE
		PRINT 'QSRn++s Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'saut?ing'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'sauteing'
		WHERE PrimaryValue = 'saut?ing'
	END
	ELSE
		PRINT 'saut''ing Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'sautn++ing'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'sauteing'
		WHERE PrimaryValue = 'sautn++ing'
	END
	ELSE
		PRINT 'sautn++ing Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'client?s lifestyle'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'client''s lifestyle'
		WHERE PrimaryValue = 'client?s lifestyle'
	END
	ELSE
		PRINT 'client?s lifestyle Not found'

	IF (
			EXISTS (
				SELECT *
				FROM [Library.JobTaskLocalisationItem]
				WHERE PrimaryValue = 'clientn++s lifestyle'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'client''s lifestyle'
		WHERE PrimaryValue = 'clientn++s lifestyle'
	END
	ELSE
		PRINT 'clientn++s lifestyle Not found'

	COMMIT
END TRY

BEGIN CATCH
	ROLLBACK

	DECLARE @ErrorMessage NVARCHAR(max)
	DECLARE @ErrorSeverity NVARCHAR(max)
	DECLARE @ErrorState NVARCHAR(50)

	PRINT 'catch'

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
END CATCH
