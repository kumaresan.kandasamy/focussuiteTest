IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'Log.ProfileEvent_ExpireProfileEvents')
BEGIN
	DROP PROCEDURE [dbo].[Log.ProfileEvent_ExpireProfileEvents]
END
GO

CREATE PROCEDURE [dbo].[Log.ProfileEvent_ExpireProfileEvents] 
	@RemoveDays INT = NULL,
	@RemoveDate DATETIME = NULL,
	@BatchSize INT = 500
AS
BEGIN
	SET NOCOUNT ON
	
    DECLARE @ErrorMessage NVARCHAR(4000)
    DECLARE @ErrorSeverity INT
    DECLARE @ErrorState INT
    
    IF @RemoveDays IS NULL AND @RemoveDate IS NULL
    BEGIN
		RAISERROR('Either @RemoveDays or @RemoveDate must be specified', 16, 1)
		RETURN
    END

    IF @RemoveDays IS NOT NULL AND @RemoveDate IS NOT NULL
    BEGIN
		RAISERROR('Only one of @RemoveDays or @RemoveDate must be specified', 16, 1)
		RETURN
    END    
    
	IF @RemoveDate IS NULL
	BEGIN
		SET @RemoveDate = CAST(DATEADD(DAY, 0 - @RemoveDays, GETUTCDATE()) AS DATE)
	END
	
	DECLARE @ProfileEventIds TABLE
	(
		Id BIGINT PRIMARY KEY
	)

	DECLARE @Done BIT
	SET @Done = 0

	WHILE @Done = 0
	BEGIN
		INSERT INTO @ProfileEventIds
		(
			Id
		)
		SELECT TOP (@BatchSize)
			PE.Id
		FROM
			[Log.ProfileEvent] PE
		WHERE
			PE.InTime < @RemoveDate	
		
		IF @@ROWCOUNT > 0
		BEGIN
			BEGIN TRY
				BEGIN TRANSACTION

				DELETE
					PE
				FROM
					[Log.ProfileEvent] PE
				INNER JOIN @ProfileEventIds TPE
					ON TPE.Id = PE.Id
				
				COMMIT TRANSACTION
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0
				BEGIN
					ROLLBACK TRANSACTION
				END
				
				SELECT 
					@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE()

				RAISERROR (@ErrorMessage, @ErrorSeverity,@ErrorState)
				
				RETURN
			END	CATCH
				
			DELETE FROM @ProfileEventIds
		END
		ELSE
		BEGIN
			SET @Done = 1
		END
	END
END
GO
