/****** Object:  StoredProcedure [dbo].[Data_Core_GetFailedIntegrationRequests]    Script Date: 02/18/2016 13:14:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data_Core_GetFailedIntegrationRequests]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Data_Core_GetFailedIntegrationRequests]
GO

/****** Object:  StoredProcedure [dbo].[Data_Core_GetFailedIntegrationRequests]    Script Date: 02/18/2016 13:14:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Data_Core_GetFailedIntegrationRequests] 
	@InStartDate datetime, 
	@InEndDate datetime,
	@InCheckOnly bit
	
AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @IntegrationMessageTypeId uniqueidentifier;
	DECLARE @IntegrationErrorCount int;
	DECLARE @UtcStartDate datetime;
	DECLARE @UtcEndDate datetime;
	
	IF(@InStartDate = NULL)
	BEGIN
		SET @InStartDate = GETDATE();
	END
	IF(@InEndDate = NULL)
	BEGIN
		SET @InEndDate = GETDATE();
	END	

	IF OBJECT_ID('tempdb..#Messages') IS NOT NULL DROP TABLE #Messages
	
	CREATE TABLE #Messages
	(
		MessageId uniqueidentifier,
		HandledOn datetime,
		RequestId uniqueidentifier
	)
	CREATE INDEX Messages_MessageId ON #Messages (MessageId)
	CREATE INDEX Messages_RequestId ON #Messages (RequestId)

	-- Set date parameters
	SET @UtcStartDate = DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InStartDate)
	SET @UtcEndDate = DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InEndDate)

	-- Get integration request message type id
	SELECT @IntegrationMessageTypeId = Id 
	FROM dbo.[Messaging.MessageType] WITH (NOLOCK) 
	WHERE TypeName LIKE 'Focus.Services.Messages.IntegrationRequestMessage%'

	-- Get all integration messages between the dates in question and strip out the request id from the payload 
	INSERT INTO #Messages
	SELECT m.id, m.HandledOn, SUBSTRING(mp.Data, CHARINDEX('"RequestId":"', mp.Data) + 13, 36)
	FROM dbo.[Messaging.Message] m WITH (NOLOCK) 
	INNER JOIN dbo.[Messaging.MessagePayload] mp WITH (NOLOCK) on m.MessagePayloadId = mp.id
	WHERE m.HandledOn >= @UtcStartDate 
	AND m.HandledOn < @UtcEndDate
	AND m.MessageTypeId = @IntegrationMessageTypeId
	AND mp.Data like '%RequestId%'
	
	IF(@InCheckOnly = 1)
	BEGIN

		SELECT @IntegrationErrorCount = COUNT(1) 
		FROM #Messages me WITH (NOLOCK)
		INNER JOIN dbo.[Data.Core.IntegrationLog] il WITH (NOLOCK) ON me.RequestId = il.RequestId
		LEFT JOIN dbo.[Data.Core.IntegrationException] ie WITH (NOLOCK) on ie.IntegrationLogId = il.id
		WHERE il.RequestedOn >= @InStartDate 
		AND il.RequestedOn < @InEndDate
		AND il.Outcome = 1	
		
		IF(@IntegrationErrorCount > 0)
		BEGIN
			RETURN 1
		END
		ELSE
		BEGIN
			RETURN 0
		END
		
	END
	ELSE
	BEGIN
		
		-- DEBUG Get all messages that failed
		SELECT TOP 400
			CAST(me.MessageId AS VARCHAR(40)) 'MessageId',
			CAST(me.HandledOn AS VARCHAR(30)) 'HandledOn',
			CAST(me.RequestId AS VARCHAR(40)) 'RequestId',
			CAST(ie.[Message] AS NVARCHAR(1000)) 'Exception',
			CAST(@InStartDate AS VARCHAR(30)) 'InStartDate', 
			CAST(@UtcStartDate AS VARCHAR(30)) 'UtcStartDate', 
			CAST(@InEndDate AS VARCHAR(30)) 'InEndDate', 
			CAST(@UtcEndDate AS VARCHAR(30)) 'UtcEndDate'
		FROM #Messages me WITH (NOLOCK)
		INNER JOIN dbo.[Data.Core.IntegrationLog] il WITH (NOLOCK) ON me.RequestId = il.RequestId
		LEFT JOIN dbo.[Data.Core.IntegrationException] ie WITH (NOLOCK) on ie.IntegrationLogId = il.id
		WHERE il.Outcome = 1
		ORDER BY il.requestid

		END
END

GO