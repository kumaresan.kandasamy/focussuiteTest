IF NOT EXISTS(SELECT 1 FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Log.LogEvent]') AND [name] = 'IX_Log.LogEvent_EventDate')
BEGIN
	CREATE INDEX [IX_Log.LogEvent_EventDate] ON [Log.LogEvent] (EventDate) WITH ( FILLFACTOR = 90 )
END
GO

IF NOT EXISTS(SELECT 1 FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Log.ProfileEvent]') AND [name] = 'IX_Log.ProfileEvent_InTime')
BEGIN
	CREATE INDEX [IX_Log.ProfileEvent_InTime] ON [Log.ProfileEvent] (InTime) WITH ( FILLFACTOR = 90 )
END
GO

IF NOT EXISTS(SELECT 1 FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Data.Core.PageState]') AND [name] = 'IX_Data.Core.PageState_LastAccessed')
BEGIN
	CREATE INDEX [IX_Data.Core.PageState_LastAccessed] ON [Data.Core.PageState] (LastAccessed) WITH ( FILLFACTOR = 90 )
END
GO

--sp_helpindex '[Data.Core.SessionState]'