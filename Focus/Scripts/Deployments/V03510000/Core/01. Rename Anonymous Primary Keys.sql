﻿DECLARE @ConstraintName varchar(200);
DECLARE @NewConstraintName varchar(200);
DECLARE @SQL varchar(2000);
DECLARE @PrimaryKeys TABLE (ConstraintName varchar(200), NewConstraintName varchar(200));

INSERT INTO @PrimaryKeys 
(
	ConstraintName,
	NewConstraintName
)
SELECT 
	pk.name AS ConstraintName, 
	('PK_'+t.name) AS NewConstraintName
FROM 
	sys.key_constraints pk
INNER JOIN sys.tables t 
	ON pk.parent_object_id = t.object_id
WHERE 
	pk.type_desc = 'PRIMARY_KEY_CONSTRAINT'
	AND pk.name <> 'PK_'+t.name

WHILE EXISTS (SELECT * FROM @PrimaryKeys)
BEGIN
	SELECT TOP 1 @ConstraintName = ConstraintName, @NewConstraintName = NewConstraintName
	FROM @PrimaryKeys;

	SELECT @SQL = 'exec sp_rename @objname = ''dbo.'+Quotename(@ConstraintName)+''', @newname = '''+@NewConstraintName + ''', @objtype = ''OBJECT''';
	PRINT @SQL;
	EXEC (@SQL);

	DELETE FROM @PrimaryKeys
	WHERE @ConstraintName = ConstraintName;
END
