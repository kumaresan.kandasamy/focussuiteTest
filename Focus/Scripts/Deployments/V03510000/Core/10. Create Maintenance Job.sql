IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'ScheduledJobs_CreateDailyMaintenanceJob')
BEGIN
	DROP PROCEDURE [dbo].[ScheduledJobs_CreateDailyMaintenanceJob]
END
GO

CREATE PROCEDURE [dbo].[ScheduledJobs_CreateDailyMaintenanceJob]
	@DayLimitForSteps INT = 30,
	@BatchSizeForSteps INT = 500,
	@Database SYSNAME = NULL
AS

DECLARE @JobName SYSNAME
DECLARE @StepSQL NVARCHAR(400)
DECLARE @StepParameters NVARCHAR(200)

SET @StepParameters = '
   @RemoveDays = 30,
   @BatchSize = 500'

IF @Database IS NULL
BEGIN
	SET @Database = DB_NAME()
END

SET @JobName = @Database + N'_Focus_Maintenance'

DECLARE @jobId BINARY(16)
SELECT @jobId = job_id FROM msdb.dbo.sysjobs_view WHERE name = @JobName

IF @jobId IS NOT NULL
BEGIN
	RAISERROR('%s job already exists', 16, 1, @JobName)
	RETURN
END

BEGIN TRANSACTION

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category 
		@class = N'JOB', 
		@type = N'LOCAL', 
		@name = N'[Uncategorized (Local)]'

	IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
		GOTO QuitWithRollback
END

EXEC @ReturnCode = msdb.dbo.sp_add_job 
	@job_name = @JobName, 
	@enabled = 1, 
	@notify_level_eventlog = 0, 
	@notify_level_email = 0, 
	@notify_level_netsend = 0, 
	@notify_level_page = 0, 
	@delete_level = 0, 
	@description = N'Daily Maintenance Job', 
	@category_name = N'[Uncategorized (Local)]', 
	@owner_login_name = N'sa', 
	@job_id = @jobId OUTPUT
	
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
	
SET @StepSQL = N'EXEC dbo.[Data.Core.Session_ExpireSessions]' + @StepParameters
	
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
		@job_id = @jobId, 
		@step_name = N'Delete Expired Sessions', 
		@step_id = 1, 
		@cmdexec_success_code = 0, 
		@on_success_action = 3, 
		@on_success_step_id = 0, 
		@on_fail_action = 2, 
		@on_fail_step_id = 0, 
		@retry_attempts = 0, 
		@retry_interval = 0, 
		@os_run_priority = 0, 
		@subsystem = N'TSQL', 
		@command = @StepSQL, 
		@database_name = @Database, 
		@flags = 0
		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback

SET @StepSQL = N'EXEC dbo.[Data.Core.SessionState_HardDeleteStates]'
	
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
		@job_id = @jobId, 
		@step_name = N'Hard Delete Sessions States', 
		@step_id = 2, 
		@cmdexec_success_code = 0, 
		@on_success_action = 3, 
		@on_success_step_id = 0, 
		@on_fail_action = 2, 
		@on_fail_step_id = 0, 
		@retry_attempts = 0, 
		@retry_interval = 0, 
		@os_run_priority = 0, 
		@subsystem = N'TSQL', 
		@command = @StepSQL, 
		@database_name = @Database, 
		@flags = 0
		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
	
SET @StepSQL = N'EXEC [Log.LogEvent_ExpireLogEvents]' + @StepParameters
	
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
        @job_id = @jobId, 
        @step_name = N'Delete Expired Log Events', 
		@step_id = 3, 
		@cmdexec_success_code = 0, 
		@on_success_action = 3, 
		@on_success_step_id = 0, 
		@on_fail_action = 2, 
		@on_fail_step_id = 0, 
		@retry_attempts = 0, 
		@retry_interval = 0, 
		@os_run_priority = 0, 
		@subsystem = N'TSQL', 
		@command = @StepSQL, 
		@database_name = @Database, 
		@flags = 0
		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback

SET @StepSQL = N'EXEC dbo.[Log.ProfileEvent_ExpireProfileEvents]' + @StepParameters
	
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
		@job_id = @jobId, 
		@step_name = N'Delete Expired Profile Events', 
		@step_id = 4, 
		@cmdexec_success_code = 0, 
		@on_success_action = 3, 
		@on_success_step_id = 0, 
		@on_fail_action = 2, 
		@on_fail_step_id = 0, 
		@retry_attempts = 0, 
		@retry_interval = 0, 
		@os_run_priority = 0, 
		@subsystem = N'TSQL', 
		@command = @StepSQL, 
		@database_name = @Database, 
		@flags = 0
		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback

SET @StepSQL = N'EXEC dbo.[PageState_ExpirePageState]' + @StepParameters
	
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete Expired Page States', 
		@step_id = 5, 
		@cmdexec_success_code = 0, 
		@on_success_action = 3, 
		@on_success_step_id = 0, 
		@on_fail_action = 2, 
		@on_fail_step_id = 0, 
		@retry_attempts = 0, 
		@retry_interval = 0, 
		@os_run_priority = 0, 
		@subsystem = N'TSQL', 
		@command = @StepSQL, 
		@database_name = @Database, 
		@flags = 0

IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback

SET @StepSQL = N'EXEC dbo.[Messaging_Delete_Handled]' + @StepParameters
	
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
		@job_id = @jobId, 
		@step_name = N'Delete Expired Messages', 
		@step_id = 6, 
		@cmdexec_success_code = 0, 
		@on_success_action = 1, 
		@on_success_step_id = 0, 
		@on_fail_action = 2, 
		@on_fail_step_id = 0, 
		@retry_attempts = 0, 
		@retry_interval = 0, 
		@os_run_priority = 0, 
		@subsystem = N'TSQL', 
		@command = @StepSQL, 
		@database_name = @Database, 
		@flags = 0
		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
	
EXEC @ReturnCode = msdb.dbo.sp_update_job 
	@job_id = @jobId, 
	@start_step_id = 1

IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback
	
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule 
		@job_id = @jobId, 
		@name = N'Daily', 
		@enabled = 1, 
		@freq_type = 4, 
		@freq_interval = 1, 
		@freq_subday_type = 1, 
		@freq_subday_interval = 0, 
		@freq_relative_interval = 0, 
		@freq_recurrence_factor = 0, 
		@active_start_date = 20160222, 
		@active_end_date = 99991231, 
		@active_start_time = 20000, 
		@active_end_time = 235959
		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobserver 
	@job_id = @jobId, 
	@server_name = N'(local)'

IF (@@ERROR <> 0 OR @ReturnCode <> 0) 
	GOTO QuitWithRollback

COMMIT TRANSACTION

GOTO EndSave

QuitWithRollback:

IF (@@TRANCOUNT > 0) 
    ROLLBACK TRANSACTION
    
EndSave:

GO


