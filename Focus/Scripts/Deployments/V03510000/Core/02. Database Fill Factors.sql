ALTER INDEX [PK_Data.Application.Job] ON [Data.Application.Job] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Data.Application.Person] ON [Data.Application.Person] REBUILD WITH ( FILLFACTOR = 95 )
ALTER INDEX [IX_Data.Application.PersonPostingMatch_PostingId] ON [Data.Application.PersonPostingMatch] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Data.Application.PersonPostingMatch_PersonId] ON [Data.Application.PersonPostingMatch] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Data.Application.PersonPostingMatch] ON [Data.Application.PersonPostingMatch] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Data.Application.Posting] ON [Data.Application.Posting] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [UQ_Data.Application.Posting_LensPostingId] ON [Data.Application.Posting] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Data.Application.Resume] ON [Data.Application.Resume] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Data.Application.ResumeJob] ON [Data.Application.ResumeJob] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Data.Application.User] ON [Data.Application.User] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Data.Core.ActionEvent_ActionTypeId] ON [Data.Core.ActionEvent] REBUILD WITH ( FILLFACTOR = 90 )

IF EXISTS(SELECT * FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Data.Core.ActionEvent]') AND Name = 'IX_Data.Core.ActionEvent_ActionTypeId_EntityId')
BEGIN
	ALTER INDEX [IX_Data.Core.ActionEvent_ActionTypeId_EntityId] ON [Data.Core.ActionEvent] REBUILD WITH ( FILLFACTOR = 90 )
END

ALTER INDEX [PK_Data.Core.ActionEvent] ON [Data.Core.ActionEvent] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Data.Core.ActionEvent_ActionTypeId_ActionedOn] ON [Data.Core.ActionEvent] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Data.Core.PageState] ON [Data.Core.PageState] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Data.Core.Session] ON [Data.Core.Session] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Data.Core.Session_LastRequestOn] ON [Data.Core.Session] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Library.JobReport] ON [Library.JobReport] REBUILD
ALTER INDEX [PK_Library.PostalCode] ON [Library.PostalCode] REBUILD
ALTER INDEX [PK_Log.LogEvent] ON [Log.LogEvent] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Messaging.Message_Version_MessageTypeId_EntityName_EntityKey_Status] ON [Messaging.Message] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Messaging.Message] ON [Messaging.Message] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Messaging.Message_Status_Version_Priority_EnqueuedOn] ON [Messaging.Message] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Messaging.MessageLog_MessageId] ON [Messaging.MessageLog] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Messaging.MessageLog] ON [Messaging.MessageLog] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Report.JobSeeker] ON [Report.JobSeeker] REBUILD WITH ( FILLFACTOR = 95 )
ALTER INDEX [PK_Report.JobSeeker_NonPurged] ON [Report.JobSeeker_NonPurged] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Report.JobSeekerData_JobSeekerId] ON [Report.JobSeekerData] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [PK_Report.JobSeekerData] ON [Report.JobSeekerData] REBUILD WITH ( FILLFACTOR = 90 )
ALTER INDEX [IX_Report.JobSeekerData_DataType] ON [Report.JobSeekerData] REBUILD WITH ( FILLFACTOR = 90 )
