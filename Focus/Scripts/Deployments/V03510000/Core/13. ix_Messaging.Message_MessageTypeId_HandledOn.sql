IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Messaging.Message]') AND name = N'IX_Messaging.Message_MessageTypeId_HandledOn')
DROP INDEX [IX_Messaging.Message_MessageTypeId_HandledOn] ON [dbo].[Messaging.Message] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_Messaging.Message_MessageTypeId_HandledOn] ON [dbo].[Messaging.Message] 
(
	[MessageTypeId] ASC,
	[HandledOn] ASC
)
INCLUDE ( [Id],
[MessagePayloadId]) WITH (FILLFACTOR = 90) ON [PRIMARY]
GO