IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Data.Core.IntegrationLog]') AND name = N'IX_Data.Core.Integrationlog_Outcome_Requestedon')
DROP INDEX [IX_Data.Core.Integrationlog_Outcome_Requestedon] ON [dbo].[Data.Core.IntegrationLog] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_Data.Core.Integrationlog_Outcome_Requestedon] ON [dbo].[Data.Core.IntegrationLog] 
(
	[Outcome] ASC,
	[RequestedOn] ASC
)
INCLUDE ( [Id],
[RequestId]) WITH (FILLFACTOR = 90) ON [PRIMARY]
GO


