﻿-- =============================================
-- Script Template
-- =============================================
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Config.DbVersion')
BEGIN
	CREATE TABLE [dbo].[Config.DbVersion]
	(
		[VersionNumber] varchar(30) NULL
	)
END

DELETE FROM [dbo].[Config.DbVersion]

INSERT INTO [dbo].[Config.DbVersion] ([VersionNumber])
VALUES ('3.51')