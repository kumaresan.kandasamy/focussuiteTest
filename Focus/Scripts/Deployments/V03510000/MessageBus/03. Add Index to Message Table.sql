IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Messaging.Message]') AND name = N'IX_Messaging.Message_MessagePayloadId')
BEGIN
	DROP INDEX [IX_Messaging.Message_MessagePayloadId] ON [dbo].[Messaging.Message] WITH ( ONLINE = OFF )
END
GO

CREATE NONCLUSTERED INDEX [IX_Messaging.Message_MessagePayloadId] ON [dbo].[Messaging.Message] 
(
	[MessagePayloadId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO