BEGIN TRANSACTION

CREATE TABLE #IndexesToReorganise (IndexName nvarchar(100), TableName nvarchar(100), Processed bit);
CREATE TABLE #IndexesToRebuild (IndexName nvarchar(100), TableName nvarchar(100), Processed bit);

INSERT INTO #IndexesToReorganise
SELECT name, OBJECT_NAME(a.[object_id]), 0
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS a
    JOIN sys.indexes AS b ON a.object_id = b.object_id AND a.index_id = b.index_id
    where object_name(a.[object_id]) in ('Messaging.Message', 'Messaging.MessageLog', 'Messaging.MessagePayLoad')
    and avg_fragmentation_in_percent >= 5 and avg_fragmentation_in_percent < 30
    and name is not null
GO

INSERT INTO #IndexesToRebuild
SELECT name, OBJECT_NAME(a.[object_id]), 0
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS a
    JOIN sys.indexes AS b ON a.object_id = b.object_id AND a.index_id = b.index_id
    where object_name(a.[object_id]) in ('Messaging.Message', 'Messaging.MessageLog', 'Messaging.MessagePayLoad')
    and avg_fragmentation_in_percent >= 30
    and name is not null

SELECT * FROM #IndexesToRebuild

DECLARE @IndexName nvarchar(100);
DECLARE @TableName nvarchar(100);
DECLARE @SqlToRun nvarchar(255);

SELECT TOP 1 @IndexName = IndexName, @TableName = TableName FROM #IndexesToReorganise WHERE Processed = 0 AND IndexName IS NOT NULL

WHILE(@IndexName IS NOT NULL)
BEGIN

SET @SqlToRun = 'ALTER INDEX [' + @IndexName + '] ON [' + @TableName + '] REORGANIZE;';

BEGIN TRANSACTION
PRINT @IndexName + ' starting...'
EXEC sp_executesql @SqlToRun;
UPDATE #IndexesToReorganise SET Processed = 1 where IndexName = @IndexName;
PRINT @IndexName + ' reorganised';
COMMIT

SET @IndexName = NULL;
SELECT TOP 1 @IndexName = IndexName, @TableName = TableName FROM #IndexesToReorganise WHERE Processed = 0 AND IndexName IS NOT NULL

END


SELECT @IndexName = IndexName, @TableName = TableName FROM #IndexesToRebuild WHERE Processed = 0 AND IndexName IS NOT NULL

WHILE(@IndexName IS NOT NULL)
BEGIN
PRINT @IndexName;
SET @SqlToRun = 'ALTER INDEX [' + @IndexName + '] ON [' + @TableName + '] REBUILD;';

BEGIN TRANSACTION
PRINT @IndexName + ' starting...'
EXEC sp_executesql @SqlToRun;
UPDATE #IndexesToRebuild SET Processed = 1 where IndexName = @IndexName;
PRINT @IndexName + ' rebuilt';
COMMIT

SET @IndexName = NULL;
SELECT TOP 1 @IndexName = IndexName, @TableName = TableName FROM #IndexesToRebuild WHERE Processed = 0 AND IndexName IS NOT NULL

END

DROP TABLE #IndexesToRebuild;
DROP TABLE #IndexesToReorganise;
COMMIT
