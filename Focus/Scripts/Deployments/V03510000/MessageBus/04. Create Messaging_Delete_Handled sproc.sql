IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'Messaging_Delete_Handled')
BEGIN
	DROP PROCEDURE [dbo].[Messaging_Delete_Handled]
END
GO

CREATE PROCEDURE [dbo].[Messaging_Delete_Handled]
	@RemoveDays int = null,
	@RemoveDate datetime = null,
	@BatchSize int = 100,
	@MaxLoops int = 0,
	@IncludeFailed bit = 0,
	@Interactive bit = 0
AS
SET NOCOUNT ON

IF @RemoveDays IS NULL AND @RemoveDate IS NULL
BEGIN
	RAISERROR('Either @RemoveDays or @RemoveDate must be specified', 16, 1)
	RETURN
END

IF @RemoveDays IS NOT NULL AND @RemoveDate IS NOT NULL
BEGIN
	RAISERROR('Only one of @RemoveDays or @RemoveDate must be specified', 16, 1)
	RETURN
END    

IF (@RemoveDate IS NULL)
BEGIN
	SET @RemoveDate = CONVERT(datetime,CONVERT(varchar,DATEADD(DAY, 0 - @RemoveDays, getutcdate()),112))
END

DECLARE @LoopCount int, @MessagesDeleted int, @PayloadsDeleted int, @MessageLogsDeleted int
SET @LoopCount = 0

DECLARE @Messages TABLE
(
	MessageId uniqueidentifier not null,
	PayloadId uniqueidentifier not null,
	PRIMARY KEY (MessageId,PayloadId)
)

IF (@IncludeFailed = 0)
BEGIN
	INSERT INTO @Messages
	SELECT TOP (@BatchSize) Id, MessagePayloadId
	FROM [Messaging.Message] WITH (NOLOCK)
	WHERE [Status] = 3
	AND EnqueuedOn < @RemoveDate
	AND HandledOn IS NOT NULL 
	AND [Failed] = 0
	ORDER BY EnqueuedOn
END
ELSE
BEGIN
	INSERT INTO @Messages
	SELECT TOP (@BatchSize) Id, MessagePayloadId
	FROM [Messaging.Message] WITH (NOLOCK)
	WHERE [Status] = 3
	AND EnqueuedOn < @RemoveDate
	AND HandledOn IS NOT NULL
	ORDER BY EnqueuedOn
END

WHILE ((SELECT COUNT(*) FROM @Messages) > 0)
BEGIN
	BEGIN TRY
		BEGIN TRAN

		DELETE FROM [Messaging.MessageLog] WHERE MessageId IN (SELECT MessageId FROM @Messages)
		SET @MessageLogsDeleted = @@ROWCOUNT
		
		DELETE FROM [Messaging.Message] WHERE Id IN (SELECT MessageId FROM @Messages)
		SET @MessagesDeleted = @@ROWCOUNT
		
		DELETE FROM [Messaging.MessagePayload] WHERE Id IN (SELECT PayloadId FROM @Messages)
		SET @PayloadsDeleted = @@ROWCOUNT

		COMMIT TRAN
	END TRY
	BEGIN CATCH
	  -- Determine if an error occurred.
	  IF @@TRANCOUNT > 0
		 ROLLBACK TRANSACTION

	  -- Return the error information.
	  DECLARE @ErrorMessage nvarchar(4000),  @ErrorSeverity int;
	  SELECT @ErrorMessage = ERROR_MESSAGE(),@ErrorSeverity = ERROR_SEVERITY();
	  RAISERROR(@ErrorMessage, @ErrorSeverity, 1);
	  RETURN
	END CATCH

	IF (@Interactive = 1)
	BEGIN
		PRINT CONVERT(varchar, getutcdate(), 121) + ' - ' + CONVERT(varchar, @MessageLogsDeleted) + ' message logs deleted, ' + CONVERT(varchar, @MessagesDeleted) + ' messages deleted, ' + CONVERT(varchar, @PayloadsDeleted) + ' payloads deleted'
	END

	DELETE FROM @Messages
	
	SET @LoopCount = @LoopCount + 1
	IF (@MaxLoops > 0 AND @LoopCount >= @MaxLoops)
	BEGIN
		BREAK
	END
	
	IF (@IncludeFailed = 0)
	BEGIN
		INSERT INTO @Messages
		SELECT TOP (@BatchSize) Id, MessagePayloadId
		FROM [Messaging.Message] WITH (NOLOCK)
		WHERE [Status] = 3
		AND EnqueuedOn < @RemoveDate
		AND HandledOn IS NOT NULL
		AND [Failed] = 0
		ORDER BY EnqueuedOn
	END
	ELSE
	BEGIN
		INSERT INTO @Messages
		SELECT TOP (@BatchSize) Id, MessagePayloadId
		FROM [Messaging.Message] WITH (NOLOCK)
		WHERE [Status] = 3
		AND EnqueuedOn < @RemoveDate
		AND HandledOn IS NOT NULL
		ORDER BY EnqueuedOn
	END
END
GO


