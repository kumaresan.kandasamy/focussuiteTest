IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Maintenance.ArchiveData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Data.Maintenance.ArchiveData]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[Data.Maintenance.ArchiveData]
 @RemoveDate datetime = NULL,
 @RemoveDays INT = NULL
AS
SET NOCOUNT ON

IF @RemoveDays IS NOT NULL AND @RemoveDate IS NOT NULL
BEGIN
	RAISERROR('Only one of @RemoveDays or @RemoveDate must be specified', 16, 1)
	RETURN
END    
    
IF @RemoveDays IS NULL AND @RemoveDate IS NULL
BEGIN
	SET @RemoveDays = 3
END 

IF (@RemoveDate IS NULL)
BEGIN
  set @RemoveDate = CAST(DATEADD(MONTH, 0 - @RemoveDays, GETUTCDATE()) AS DATE)
END

 
 Begin Transaction
 
 if(Exists(Select * from Sys.databases where name = 'FocusSuite_Archive'))
 Begin
  If(Exists(select * from FocusSuite_Archive.sys.tables where name = 'Data.Application.PersonPostingMatch'))
  Begin
   
   INSERT INTO [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatch] 
   SELECT PPM.* FROM [dbo].[Data.Application.PersonPostingMatch] PPM INNER JOIN [dbo].[Data.Application.Posting] P ON PPM.PostingId =  P.Id AND P.CreatedOn < @RemoveDate AND P.OriginId='999'
   IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatch]', 16, 1)
			RETURN 
		END
  END
  else
  Begin
   
   SELECT * INTO [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatch] FROM [dbo].[Data.Application.PersonPostingMatch] WHERE 1 = 0
   INSERT INTO [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatch] 
   SELECT PPM.* FROM [dbo].[Data.Application.PersonPostingMatch] PPM INNER JOIN [dbo].[Data.Application.Posting] P ON PPM.PostingId =  P.Id AND P.CreatedOn < @RemoveDate AND P.OriginId='999'
   IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatch]', 16, 1)
			RETURN 
		END
  END
  
  If(Exists(select * from FocusSuite_Archive.sys.tables where name = 'Data.Application.PersonPostingMatchToIgnore'))
  Begin
   
   INSERT INTO [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatchToIgnore] 
   SELECT PMI.* FROM [dbo].[Data.Application.PersonPostingMatchToIgnore] PMI INNER JOIN [dbo].[Data.Application.Posting] P ON PMI.PostingId =  P.Id AND P.CreatedOn < @RemoveDate AND P.OriginId='999'
   IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatchToIgnore]', 16, 1)
			RETURN 
		END
  END
  else
  Begin
   
   SELECT * INTO [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatchToIgnore] FROM [dbo].[Data.Application.PersonPostingMatchToIgnore] WHERE 1 = 0
   INSERT INTO [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatchToIgnore] 
   SELECT PMI.* FROM [dbo].[Data.Application.PersonPostingMatchToIgnore] PMI INNER JOIN [dbo].[Data.Application.Posting] P ON PMI.PostingId =  P.Id AND P.CreatedOn < @RemoveDate AND P.OriginId='999'
   IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Data.Application.PersonPostingMatchToIgnore]', 16, 1)
			RETURN 
		END
  END
  
   If(Exists(select * from FocusSuite_Archive.sys.tables where name = 'Data.Application.JobPostingOfInterestSent'))
  Begin
   
  INSERT INTO [FocusSuite_Archive].[dbo].[Data.Application.JobPostingOfInterestSent] 
   SELECT JPI.* FROM [dbo].[Data.Application.JobPostingOfInterestSent] JPI INNER JOIN [dbo].[Data.Application.Posting] P ON JPI.PostingId =  P.Id AND P.CreatedOn < @RemoveDate AND P.OriginId='999'
   IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Data.Application.JobPostingOfInterestSent]', 16, 1)
			RETURN 
		END
  END
  else
  Begin
   
   SELECT * INTO [FocusSuite_Archive].[dbo].[Data.Application.JobPostingOfInterestSent] FROM [dbo].[Data.Application.JobPostingOfInterestSent] WHERE 1 = 0
   INSERT INTO [FocusSuite_Archive].[dbo].[Data.Application.JobPostingOfInterestSent] 
   SELECT JPI.* FROM [dbo].[Data.Application.JobPostingOfInterestSent] JPI INNER JOIN [dbo].[Data.Application.Posting] P ON JPI.PostingId =  P.Id AND P.CreatedOn < @RemoveDate AND P.OriginId='999'
   IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Data.Application.JobPostingOfInterestSent]', 16, 1)
			RETURN 
		END
  END
  
  if(Exists(Select * from FocusSuite_Archive.sys.tables where name = 'Report.JobSeekerData'))
  Begin
   
    INSERT INTO [FocusSuite_Archive].[dbo].[Report.JobSeekerData] SELECT JSD.* FROM [dbo].[Report.JobSeekerData] JSD INNER JOIN [dbo].[Report.JobSeeker]JS	ON JS.Id = JSD.JobSeekerId  AND  JS.AccountCreationDate < @RemoveDate
    IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Report.JobSeekerData]', 16, 1)
			RETURN 
		END
  END
  else
  Begin
   
   SELECT * INTO [FocusSuite_Archive].[dbo].[Report.JobSeekerData]  FROM [dbo].[Report.JobSeekerData] WHERE 1 = 0
   INSERT INTO [FocusSuite_Archive].[dbo].[Report.JobSeekerData] SELECT JSD.* FROM [dbo].[Report.JobSeekerData] JSD INNER JOIN [dbo].[Report.JobSeeker]JS	ON JS.Id = JSD.JobSeekerId  AND  JS.AccountCreationDate < @RemoveDate
    IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Report.JobSeekerData]', 16, 1)
			RETURN 
		END
  END
  
  if(Exists(Select * from FocusSuite_Archive.sys.tables where name = 'Data.Application.JobsSentInAlerts'))
  Begin
   
    INSERT INTO [FocusSuite_Archive].[dbo].[Data.Application.JobsSentInAlerts] SELECT JSA.* FROM [dbo].[Data.Application.JobsSentInAlerts] JSA INNER JOIN [dbo].[Data.Application.SavedSearch]SS ON SS.Id = JSA.SavedSearchId  AND  JSA.AlertDate < @RemoveDate
    IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Data.Application.JobsSentInAlerts]', 16, 1)
			RETURN 
		END
  END
  else
  Begin
   
   SELECT * INTO [FocusSuite_Archive].[dbo].[Data.Application.JobsSentInAlerts]  FROM [dbo].[Data.Application.JobsSentInAlerts] WHERE 1 = 0
   INSERT INTO [FocusSuite_Archive].[dbo].[Data.Application.JobsSentInAlerts] SELECT JSA.* FROM [dbo].[Data.Application.JobsSentInAlerts] JSA INNER JOIN [dbo].[Data.Application.SavedSearch]SS	ON SS.Id = JSA.SavedSearchId  AND  JSA.AlertDate < @RemoveDate
    IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while copying data to [FocusSuite_Archive].[dbo].[Data.Application.JobsSentInAlerts]', 16, 1)
			RETURN 
		END
  END
 COMMIT
 
 BEGIN TRANSACTION
 
 /****** delete for JobPostingOfInterestSent table ******/		
		DELETE [dbo].[Data.Application.JobPostingOfInterestSent]WHERE PostingId IN (select Id from [dbo].[Data.Application.Posting] P
		WHERE CreatedOn < @RemoveDate AND OriginId='999')
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while deleting data from [dbo].[Data.Application.JobPostingOfInterestSent]', 16, 1)
			RETURN -1
		END

	/****** delete for PersonPostingMatch table ******/		
		DELETE [dbo].[Data.Application.PersonPostingMatch] WHERE PostingId IN (select Id from [dbo].[Data.Application.Posting] P
		WHERE CreatedOn < @RemoveDate AND OriginId='999')
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while deleting data from [dbo].[Data.Application.PersonPostingMatch]', 16, 1)
			RETURN -1
		END
	/****** delete for PersonPostingMatchToIgnore table ******/		
		DELETE [dbo].[Data.Application.PersonPostingMatchToIgnore]WHERE PostingId IN (select Id from [dbo].[Data.Application.Posting] P
		WHERE CreatedOn < @RemoveDate  AND OriginId='999')
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while deleting data from [dbo].[Data.Application.PersonPostingMatchToIgnore]', 16, 1)
			RETURN 
		END
	/****** delete for JobSeekerData table ******/		
		DELETE [dbo].[Report.JobSeekerData]
		WHERE JobSeekerId IN
		(
			SELECT Id
			FROM [dbo].[Report.JobSeeker]
			WHERE AccountCreationDate < @RemoveDate
		)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while deleting data from [dbo].[Report.JobSeekerData]', 16, 1)
			RETURN
		END
		
	/****** delete for JobSeekerData table ******/		
		DELETE [dbo].[Data.Application.JobsSentInAlerts]
		WHERE AlertDate < @RemoveDate AND SavedSearchId IN
		(
			SELECT Id
			FROM [dbo].[Data.Application.SavedSearch]
		)
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRAN
			RAISERROR ('Error occured while deleting data from [dbo].[Data.Application.JobsSentInAlerts]', 16, 1)
			RETURN
		END
		
	IF @@TRANCOUNT > 0
	BEGIN
		COMMIT TRAN
		RETURN 0
	END
COMMIT 
 
END
GO


