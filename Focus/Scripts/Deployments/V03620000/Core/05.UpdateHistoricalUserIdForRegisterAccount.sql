DECLARE @tempActionEvent table (UserId bigint, EntityId bigint)

Insert into @tempActionEvent
select ae.UserId, ae.EntityIdAdditional01 from [Data.Core.ActionEvent] as ae
where ae.ActionTypeId = (select Id from [Data.Core.ActionType] as at where at.Name = 'AddPersonOffice')
and ae.UserId != '0' group by ae.EntityIdAdditional01,ae.UserId

Update ae set ae.UserId = t.UserId from [Data.Core.ActionEvent] as ae
inner join  @tempActionEvent t on t.EntityId = ae.EntityId
where ae.ActionTypeId = (select Id from [Data.Core.ActionType] as at where at.Name = 'RegisterAccount')
and ae.UserId = '0'
