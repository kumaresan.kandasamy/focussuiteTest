
/****** Object:  Table [dbo].[Data.Application.JobsSentInAlerts]    Script Date: 8/3/2017 4:33:06 PM ******/

IF NOT  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobsSentInAlerts]') AND type in (N'U'))
Begin
CREATE TABLE [dbo].[Data.Application.JobsSentInAlerts](
	[Id] [bigint] NOT NULL,
	[SavedSearchId] [bigint] NOT NULL,
	[AlertDate] [datetime] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Postings] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_JobsSentInAlerts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

ALTER TABLE [dbo].[Data.Application.JobsSentInAlerts] ADD  CONSTRAINT [DF_JobsSentInAlerts_SavedSearchId]  DEFAULT ((0)) FOR [SavedSearchId]

ALTER TABLE [dbo].[Data.Application.JobsSentInAlerts] ADD  CONSTRAINT [DF_JobsSentInAlerts_UserId]  DEFAULT ((0)) FOR [UserId]

ALTER TABLE [dbo].[Data.Application.JobsSentInAlerts]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.JobsSentInAlerts_Data.Application.User] FOREIGN KEY([UserId])
REFERENCES [dbo].[Data.Application.User] ([Id])

ALTER TABLE [dbo].[Data.Application.JobsSentInAlerts] CHECK CONSTRAINT [FK_Data.Application.JobsSentInAlerts_Data.Application.User]

End