IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Data.Maintenance.CreateArchiveDatabase]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Data.Maintenance.CreateArchiveDatabase]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[Data.Maintenance.CreateArchiveDatabase]
As
Begin
 set nocount on
 
 if ( Exists( Select * from sys.databases where name = 'FocusSuite_Archive'))
    BEGIN
		EXEC [Data.Maintenance.ArchiveData]
	END
 Else
	BEGIN
		Create database [FocusSuite_Archive]
		EXEC [Data.Maintenance.ArchiveData]
	END
End


GO


