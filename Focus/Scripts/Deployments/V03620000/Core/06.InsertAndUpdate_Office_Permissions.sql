-- Declaration
DECLARE @MaxRoleId BIGINT

-- Insert "ReassignJobSeekerToNewOffice" and "AssistAssignBusinessOfficeFromDefault" role
BEGIN TRANSACTION
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = 'ReassignJobSeekerToNewOffice')
	BEGIN
		SELECT @MaxRoleId = MAX([Id]) + 1 FROM [Data.Application.Role]
		INSERT INTO [Data.Application.Role] VALUES (@MaxRoleId,'ReassignJobSeekerToNewOffice','Reassign Job Seeker To New Office')
	END

	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = 'AssistAssignBusinessOfficeFromDefault')
	BEGIN
		SELECT @MaxRoleId = MAX([Id]) + 1 FROM [Data.Application.Role]
		INSERT INTO [Data.Application.Role] VALUES (@MaxRoleId,'AssistAssignBusinessOfficeFromDefault','Assist Assign Business Office From Default')
	END
COMMIT TRANSACTION


-- Declaration
DECLARE @AJSOfficeAdmin BIGINT
DECLARE @AEOfficeAdmin BIGINT
DECLARE @AJOOfficeAdmin BIGINT
DECLARE @AssistAssignJobSeekerOfficeFromDefault BIGINT
DECLARE @ReassignJobSeekerToNewOffice BIGINT
DECLARE @AssistAssignBusinessOfficeFromDefault BIGINT
DECLARE @ReassignBusinessToNewOffice BIGINT
DECLARE @ReassignJobPostingToNewOffice BIGINT

-- Initialisation
SELECT @AJSOfficeAdmin = [Id] FROM [Data.Application.Role] WHERE [Key] = 'AJSOfficeAdmin'
SELECT @AEOfficeAdmin = [Id] FROM [Data.Application.Role] WHERE [Key] = 'AEOfficeAdmin'
SELECT @AJOOfficeAdmin = [Id] FROM [Data.Application.Role] WHERE [Key] = 'AJOOfficeAdmin'
SELECT @AssistAssignJobSeekerOfficeFromDefault = [Id] FROM [Data.Application.Role] WHERE [Key] = 'AssistAssignJobSeekerOfficeFromDefault'
SELECT @ReassignJobSeekerToNewOffice = [Id] FROM [Data.Application.Role] WHERE [Key] = 'ReassignJobSeekerToNewOffice'
SELECT @AssistAssignBusinessOfficeFromDefault = [Id] FROM [Data.Application.Role] WHERE [Key] = 'AssistAssignBusinessOfficeFromDefault'
SELECT @ReassignBusinessToNewOffice = [Id] FROM [Data.Application.Role] WHERE [Key] = 'ReassignBusinessToNewOffice'
SELECT @ReassignJobPostingToNewOffice = [Id] FROM [Data.Application.Role] WHERE [Key] = 'ReassignJobPostingToNewOffice'


-- Update Permissions
BEGIN TRANSACTION
	IF EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = 'ReassignJobSeekerToNewOffice')
	BEGIN
		IF EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = 'AssistAssignBusinessOfficeFromDefault')
		BEGIN
		-- AJOOfficeAdmin to ReassignJobPostingToNewOffice
			UPDATE [dbo].[Data.Application.UserRole]
			SET [RoleId] = @ReassignJobPostingToNewOffice
			WHERE [RoleId] = @AJOOfficeAdmin

		-- AEOfficeAdmin to AssistAssignBusinessOfficeFromDefault
			UPDATE [dbo].[Data.Application.UserRole]
			SET [RoleId] = @AssistAssignBusinessOfficeFromDefault
			WHERE [RoleId] = @AEOfficeAdmin

		-- AssistAssignJobSeekerOfficeFromDefault to ReassignJobSeekerToNewOffice
			UPDATE [dbo].[Data.Application.UserRole]
			SET [RoleId] = @ReassignJobSeekerToNewOffice
			WHERE [RoleId] = @AssistAssignJobSeekerOfficeFromDefault

		-- AJSOfficeAdmin to AssistAssignJobSeekerOfficeFromDefault
			UPDATE [dbo].[Data.Application.UserRole]
			SET [RoleId] = @AssistAssignJobSeekerOfficeFromDefault
			WHERE [RoleId] = @AJSOfficeAdmin

		END
	END
COMMIT TRANSACTION