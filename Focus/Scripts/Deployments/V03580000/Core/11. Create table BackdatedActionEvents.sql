﻿IF NOT  EXISTS (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'Data.Core.BackdatedActionEvents')

Begin

CREATE TABLE [dbo].[Data.Core.BackdatedActionEvents](
	[Id] [bigint] NOT NULL,
	[ActionId] [bigint] NOT NULL,
	[SessionId] [uniqueidentifier] NOT NULL,
	[RequestId] [uniqueidentifier] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ActionedOn] [datetime] NOT NULL,
	[EntityId] [bigint] NULL,
	[EntityIdAdditional01] [bigint] NULL,
	[EntityIdAdditional02] [bigint] NULL,
	[AdditionalDetails] [nvarchar](max) NULL,
    [CreatedOn] [datetime] NOT NULL,
	[ActionTypeId] [bigint] NOT NULL,
	[EntityTypeId] [bigint] NULL,
	[ActionBackdatedOn] [datetime] NOT NULL
) ON [PRIMARY]

End