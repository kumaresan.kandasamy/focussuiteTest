﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'SingleParent')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [SingleParent] BIT 
	CONSTRAINT [SingleParent] DEFAULT ('0') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'LowIncomeStatus')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [LowIncomeStatus] BIT 
	CONSTRAINT [LowIncomeStatus] DEFAULT ('0') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'DisplacedHomemaker')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [DisplacedHomemaker] BIT 
	CONSTRAINT [DisplacedHomemaker] DEFAULT ('0') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'LowLevelLiteracy')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [LowLevelLiteracy] BIT 
	CONSTRAINT [LowLevelLiteracy] DEFAULT ('0') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'CulturalBarriers')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [CulturalBarriers] BIT 
	CONSTRAINT [CulturalBarriers] DEFAULT ('0') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'NoOfDependents')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [NoOfDependents] NVARCHAR(50) 
	CONSTRAINT [NoOfDependents] DEFAULT ('Not disclosed') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'EstMonthlyIncome')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [EstMonthlyIncome] NVARCHAR(50) 
	CONSTRAINT [EstMonthlyIncome] DEFAULT ('Not disclosed') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'PreferredLanguage')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [PreferredLanguage] NVARCHAR(50) 
	CONSTRAINT [PreferredLanguage] DEFAULT ('Not disclosed') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'NativeLanguage')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [NativeLanguage] NVARCHAR(50) 
	CONSTRAINT [NativeLanguage] DEFAULT ('Not disclosed') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'CommonLanguage')
BEGIN
	ALTER TABLE [Data.Application.Resume] ADD [CommonLanguage] NVARCHAR(50) 
	CONSTRAINT [CommonLanguage] DEFAULT ('Not disclosed') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'HomelessNoShelter')
BEGIN
 ALTER TABLE [Data.Application.Resume] ADD [HomelessNoShelter] bit 
 CONSTRAINT [HomelessNoShelter] DEFAULT ('0') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'HomelessWithShelter')
BEGIN
 ALTER TABLE [Data.Application.Resume] ADD [HomelessWithShelter] bit 
 CONSTRAINT [HomelessWithShelter] DEFAULT ('0') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'RunawayYouth18OrUnder')
BEGIN
 ALTER TABLE [Data.Application.Resume] ADD [RunawayYouth18OrUnder] bit 
 CONSTRAINT [RunawayYouth18OrUnder] DEFAULT ('0') NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'ExOffender')
BEGIN
 ALTER TABLE [Data.Application.Resume] ADD [ExOffender] bit 
 CONSTRAINT [ExOffender] DEFAULT ('0') NOT NULL
END