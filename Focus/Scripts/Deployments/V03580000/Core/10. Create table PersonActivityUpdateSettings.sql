IF NOT  EXISTS (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'Data.Application.PersonActivityUpdateSettings')

Begin

CREATE TABLE [dbo].[Data.Application.PersonActivityUpdateSettings](
	[Id] [bigint] NOT NULL,
	[PersonId] [bigint] NOT NULL,
	[BackdateMyEntriesJSActionMenuMaxDays] [bigint] NULL,
	[BackdateAnyEntryJSActivityLogMaxDays] [bigint] NULL,
	[BackdateMyEntriesJSActivityLogMaxDays] [bigint] NULL,
	[DeleteAnyEntryJSActivityLogMaxDays] [bigint] NULL,
	[DeleteMyEntriesJSActivityLogMaxDays] [bigint] NULL,
	[BackdateMyEntriesHMActionMenuMaxDays] [bigint] NULL,
	[BackdateAnyEntryHMActivityLogMaxDays] [bigint] NULL,
	[BackdateMyEntriesHMActivityLogMaxDays] [bigint] NULL,
	[DeleteAnyEntryHMActivityLogMaxDays] [bigint] NULL,
	[DeleteMyEntriesHMActivityLogMaxDays] [bigint] NULL
) ON [PRIMARY]

End