﻿IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.PhysicalImpairment' and ExternalId='1')
BEGIN
UPDATE [Config.CodeItem] SET [Key] = 'DisabilityCategory.CognitiveIntellectual' WHERE [Key] = 'DisabilityCategory.PhysicalImpairment' and ExternalId='1';

UPDATE [Config.LocalisationItem] SET [Key] = 'DisabilityCategory.CognitiveIntellectual' WHERE [Key] = 'DisabilityCategory.PhysicalImpairment';

UPDATE [Config.LocalisationItem] SET [Value] = 'Cognitive / intellectual disability' WHERE [Key] = 'DisabilityCategory.CognitiveIntellectual';
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.MentalImpairment' and ExternalId='2')
BEGIN
UPDATE [Config.CodeItem] SET [Key] = 'DisabilityCategory.HearingRelatedImpairment' WHERE [Key] = 'DisabilityCategory.MentalImpairment' and ExternalId='2';

UPDATE [Config.LocalisationItem] SET [Key] = 'DisabilityCategory.HearingRelatedImpairment' WHERE [Key] = 'DisabilityCategory.MentalImpairment';

UPDATE [Config.LocalisationItem] SET [Value] = 'Hearing-related impairment' WHERE [Key] = 'DisabilityCategory.HearingRelatedImpairment';
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.BothPhysicalMentalImpairment' and ExternalId='3')
BEGIN
UPDATE [Config.CodeItem] SET [Key] = 'DisabilityCategory.LearningDisability' WHERE [Key] = 'DisabilityCategory.BothPhysicalMentalImpairment' and ExternalId='3';

UPDATE [Config.LocalisationItem] SET [Key] = 'DisabilityCategory.LearningDisability' WHERE [Key] = 'DisabilityCategory.BothPhysicalMentalImpairment';

UPDATE [Config.LocalisationItem] SET [Value] = 'Learning disability' WHERE [Key] = 'DisabilityCategory.LearningDisability';
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.NotDisclosed' and ExternalId='4')
BEGIN
UPDATE [Config.CodeItem] SET ExternalId = '8' WHERE [Key] = 'DisabilityCategory.NotDisclosed';

UPDATE [Config.CodeGroupItem] SET DisplayOrder = 8 WHERE  CodeItemId = (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.NotDisclosed' );
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.PsychiatricEmotional')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('DisabilityCategory.PsychiatricEmotional', 1, '4');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'DisabilityCategory.PsychiatricEmotional', 'Mental illness or psychiatric/emotional disability', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (4, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'DisabilityCategories' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.PsychiatricEmotional' ));
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.ChronicHealthConditions')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('DisabilityCategory.ChronicHealthConditions', 1, '5');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'DisabilityCategory.ChronicHealthConditions', 'Physical / chronic health conditions', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (5, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'DisabilityCategories' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.ChronicHealthConditions' ));
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.PhysicalMobilityImpairment')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('DisabilityCategory.PhysicalMobilityImpairment', 1, '6');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'DisabilityCategory.PhysicalMobilityImpairment', 'Physical / mobility impairment', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (6, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'DisabilityCategories' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.PhysicalMobilityImpairment' ));
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.VisionRelatedImpairment')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('DisabilityCategory.VisionRelatedImpairment', 1, '7');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'DisabilityCategory.VisionRelatedImpairment', 'Vision-related impairment', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (7, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'DisabilityCategories' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.VisionRelatedImpairment' ));
END



