﻿/****** Object:  View [dbo].[Data.Application.BusinessUnitActivityView]    Script Date: 06/07/2017 15:50:12 ******/
DROP VIEW [dbo].[Data.Application.BusinessUnitActivityView]
GO

/****** Object:  View [dbo].[Data.Application.BusinessUnitActivityView]    Script Date: 06/07/2017 15:50:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Data.Application.BusinessUnitActivityView]
AS
SELECT		ActionEvent.Id AS Id,
			BusinessUnit.EmployerId, 
			BusinessUnit.Id AS BUId, 
			Person.FirstName+ ' '+ Person.LastName AS UserName, 
			BusinessUnit.Name AS BUName, 
			ActionType.Name AS ActionName, 
            ActionEvent.ActionedOn, 
            ActionEvent.Id AS ActionId, 
            Job.Id as JobId, 
            Job.JobTitle
FROM        dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK) 
			INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id
			INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Job.Id = ActionEvent.EntityId 
			INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].Id = ActionEvent.UserId
			INNER JOIN dbo.[Data.Application.Person] AS Person WITH (NOLOCK) ON Person.Id = [User].PersonId
			INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON BusinessUnit.Id = Job.BusinessUnitId												    
WHERE		ActionType.Name IN 
			('PostJob',
			'SaveJob',
			'HoldJob',
			'CloseJob',
			'RefreshJob',
			'ReactivateJob',
			'CreateJob')

UNION

SELECT		ActionEvent.Id AS Id,
			BusinessUnit.EmployerId, 
			BusinessUnit.Id AS BUId, 
			Person.FirstName+ ' '+ Person.LastName AS UserName, 
			BusinessUnit.Name AS BUName, 
			ActionType.Name AS ActionName, 
            ActionEvent.ActionedOn, 
            ActionEvent.Id AS ActionId, 
            0 as JobId, 
            '' as JobTitle
FROM        dbo.[Data.Core.ActionEvent] AS ActionEvent WITH (NOLOCK) 
			INNER JOIN dbo.[Data.Core.ActionType] AS ActionType WITH (NOLOCK) ON ActionEvent.ActionTypeId = ActionType.Id
			INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON [User].Id = ActionEvent.UserId
			INNER JOIN dbo.[Data.Application.Person] AS Person WITH (NOLOCK) ON Person.Id = [User].PersonId
			INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON BusinessUnit.Id = ActionEvent.EntityId 											    
WHERE       ActionType.Name IN ('UnblockAllEmployerEmployeesAccount', 'BlockAllEmployerEmployeesAccount')

GO


