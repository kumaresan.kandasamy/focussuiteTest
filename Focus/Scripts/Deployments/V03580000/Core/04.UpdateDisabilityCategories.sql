﻿IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.PhysicalMobilityImpairment' and [ExternalId]='6')
BEGIN
UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.PhysicalMobility' WHERE [Key]='DisabilityCategory.PhysicalMobilityImpairment'

UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.PhysicalMobilityImpairment' WHERE [Key]='DisabilityCategory.CognitiveIntellectual' AND [ExternalId]='1'
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.PsychiatricEmotional' and [ExternalId]='4')
BEGIN
  UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.Psychiatric' WHERE [Key]='DisabilityCategory.PsychiatricEmotional'

  UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.PsychiatricEmotional' WHERE [Key]='DisabilityCategory.HearingRelatedImpairment' AND [ExternalId]='2'
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.ChronicHealthConditions' and [ExternalId]='5')
BEGIN
  UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.ChronicHealth' WHERE [Key]='DisabilityCategory.ChronicHealthConditions' 

  UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.ChronicHealthConditions' WHERE [Key]='DisabilityCategory.LearningDisability' AND [ExternalId]='3'
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.NotDisclosed'and [ExternalId]='8')
BEGIN
  UPDATE [dbo].[Config.CodeItem] SET [ExternalId]='4' WHERE [Key]='DisabilityCategory.NotDisclosed' 
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.Psychiatric')
BEGIN
  UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.HearingRelatedImpairment',[ExternalId]='8' WHERE [Key]='DisabilityCategory.Psychiatric'
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.PhysicalMobility')
BEGIN
  UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.CognitiveIntellectual' WHERE [Key]='DisabilityCategory.PhysicalMobility'
END

IF EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'DisabilityCategory.ChronicHealth')
BEGIN
  UPDATE [dbo].[Config.CodeItem] SET [Key]='DisabilityCategory.LearningDisability' WHERE [Key]='DisabilityCategory.ChronicHealth'
END