﻿IF EXISTS (Select J.* from [Report.JobSeeker]J inner join [Data.Application.Resume]R on ResumeXml like '%<disability_category_cd>3%' and R.PersonId = J.FocusPersonId and R.ResumeCompletionStatusId=127 and R.StatusId=1)
Begin

Update J
set J.DisabilityType ='8' 
from [Report.JobSeeker]J inner join [Data.Application.Resume]R on ResumeXml like '%<disability_category_cd>3%' and R.PersonId = J.FocusPersonId and R.ResumeCompletionStatusId=127 and R.StatusId=1

End

IF EXISTS (Select J.* from [Report.JobSeeker]J inner join [Data.Application.Resume]R on ResumeXml like '%<disability_category_cd>2%' and R.PersonId = J.FocusPersonId and R.ResumeCompletionStatusId=127 and R.StatusId=1)
Begin

Update J
set J.DisabilityType ='4' 
from [Report.JobSeeker]J inner join [Data.Application.Resume]R on ResumeXml like '%<disability_category_cd>2%' and R.PersonId = J.FocusPersonId and R.ResumeCompletionStatusId=127 and R.StatusId=1

End