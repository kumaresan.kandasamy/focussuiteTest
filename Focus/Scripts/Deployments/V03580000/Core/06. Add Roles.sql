﻿-- =============================================
-- Script Template
-- =============================================
IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'BackdateMyEntriesJSActionMenuMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'BackdateMyEntriesJSActionMenuMaxDays','Backdate MyEntries JSAction MenuMaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'BackdateAnyEntryJSActivityLogMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'BackdateAnyEntryJSActivityLogMaxDays','Backdate AnyEntry JSActivity LogMaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'BackdateMyEntriesJSActivityLogMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'BackdateMyEntriesJSActivityLogMaxDays','Backdate MyEntries JSActivity LogMaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'DeleteAnyEntryJSActivityLogMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'DeleteAnyEntryJSActivityLogMaxDays','Delete AnyEntry JSActivity LogMaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'DeleteMyEntriesJSActivityLogMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'DeleteMyEntriesJSActivityLogMaxDays','Delete MyEntries JSActivity LogMaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'BackdateMyEntriesHMActionMenuMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'BackdateMyEntriesHMActionMenuMaxDays','Backdate MyEntries HMActionMenu MaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'BackdateAnyEntryHMActivityLogMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'BackdateAnyEntryHMActivityLogMaxDays','Backdate AnyEntry HMActivity LogMaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'BackdateMyEntriesHMActivityLogMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'BackdateMyEntriesHMActivityLogMaxDays','Backdate MyEntries HMActivity LogMaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'DeleteAnyEntryHMActivityLogMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'DeleteAnyEntryHMActivityLogMaxDays','Delete AnyEntry HMActivity LogMaxDays')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'DeleteMyEntriesHMActivityLogMaxDays')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'DeleteMyEntriesHMActivityLogMaxDays','Delete MyEntries HMActivity LogMaxDays')
END
