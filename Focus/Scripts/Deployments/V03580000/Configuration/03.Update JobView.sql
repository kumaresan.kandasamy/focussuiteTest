﻿DROP VIEW [dbo].[Data.Application.JobView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.JobView] 
AS
SELECT 
	j.Id, 
	j.EmployerId, 
	e.Name AS EmployerName, 
	j.JobTitle, 
	bu.Name AS BusinessUnitName, 
	j.BusinessUnitId, 
	j.JobStatus, 
	j.PostedOn, 
	j.ClosingOn, 
	j.HeldOn,   
    j.ClosedOn, 
    j.UpdatedOn, 
    j.EmployeeId, 
    j.ApprovalStatus, 
    ISNULL(crg.ReferralCount, 0) AS ReferralCount,   
    j.FederalContractor, 
    j.ForeignLabourCertificationH2A, 
    j.ForeignLabourCertificationH2B, 
    j.ForeignLabourCertificationOther, 
    j.CourtOrderedAffirmativeAction,   
    u.UserName, 
    p.FirstName AS HiringManagerFirstName, 
    p.LastName AS HiringManagerLastName, 
    j.CreatedOn, 
    j.YellowProfanityWords, 
    j.IsConfidential, 
    j.CreatedBy,
    j.UpdatedBy, 
    j.JobType,
    j.AssignedToId,
    j.NumberOfOpenings,
    j.VeteranPriorityEndDate,
    j.ExtendVeteranPriority,
    j.Location,
    j.CriminalBackgroundExclusionRequired As HasCheckedCriminalRecordExclusion,
    j.RedProfanityWords,
    j.ClosedBy,
    j.AwaitingApprovalOn,
    po.id as PostingId,
	j.JobLocationType,
	j.SuitableForHomeWorker,
	j.MinimumAgeReasonValue,
	j.IsSalaryAndCommissionBased,
	j.IsCommissionBased,
	j.CreditCheckRequired,
	js.Requirement as JobSpecialRequirement
FROM  
	dbo.[Data.Application.Job] AS j WITH (NOLOCK) 
INNER JOIN dbo.[Data.Application.Employee] AS employee WITH (NOLOCK) 
	ON j.EmployeeId = employee.Id 
INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) 
	ON employee.PersonId = p.Id 
INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) 
	ON employee.PersonId = u.PersonId 
INNER JOIN dbo.[Data.Application.Employer] AS e WITH (NOLOCK) 
	ON j.EmployerId = e.Id 
LEFT OUTER JOIN
(SELECT *
FROM   (SELECT *,
           ROW_NUMBER()
             OVER(PARTITION BY jobid ORDER BY jobid) AS rowid
     FROM dbo.[Data.Application.JobSpecialRequirement]) a
WHERE rowid = 1) js
ON js.JobId=j.Id
INNER JOIN dbo.[Data.Application.BusinessUnit] AS bu WITH (NOLOCK) 
	ON bu.Id = j.BusinessUnitId
LEFT JOIN dbo.[Data.Application.Posting] AS po WITH (NOLOCK) 
	ON po.JobId = j.id
OUTER APPLY (
	SELECT 
		COUNT(1) AS ReferralCount  
	FROM 
		dbo.[Data.Application.Application] AS ca WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Posting] AS p WITH (NOLOCK)
		ON p.Id = ca.PostingId
	WHERE 
		p.JobId = j.Id
		AND ca.ApprovalStatus IN (1, 5)  
	) AS crg

GO


