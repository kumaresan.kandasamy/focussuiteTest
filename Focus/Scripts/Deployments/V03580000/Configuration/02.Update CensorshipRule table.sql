﻿IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wahoo')
BEGIN
update [Config.CensorshipRule]
set WhiteList='Wahoo Docks' where [Phrase]='wahoo'
Print 'Added ''Wahoo Docks'' to the WhiteList of ''wahoo'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'boy*')
BEGIN
update [Config.CensorshipRule]
set [WhiteList]='boyle, boysenberry, boysenberries, boys rock, boy''s casual, boy''s class, boy''s classroom, boy''s clothing, boy''s department, boy''s sports, boy''s sportsgear, boy''s sportswear, boy''s wear,  boys'' casual, boys'' class, boys'' classroom, boys'' clothing, boys'' department, boys'' sports, boys'' sportsgear, boys'' sportswear, boys'' wear, Boy''s Clubs of America, Boy Scouts of America, Boyd' where [Phrase]='boy*'
Print 'Added ''Boyd'' to the WhiteList of ''boy'''
END
