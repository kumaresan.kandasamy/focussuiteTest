ALTER VIEW [dbo].[Data.Application.PersonListCandidateView]  
AS  
  
SELECT   
 PersonListPerson.Id AS Id,  
 PersonList.ListType AS ListType,  
 PersonList.PersonId AS PersonId,  
 [Resume].Id AS CandidateId,  
 [Resume].FirstName AS CandidateFirstName,  
 [Resume].LastName AS CandidateLastName,  
 ISNULL([Resume].IsVeteran, 'false') AS CandidateIsVeteran ,
 PersonListPerson.PersonId As CandidatePersonId,
 [Resume].TownCity As CandidateTownCity,
 [Resume].StateId As CandidateStateId,
 [Resume].IsContactInfoVisible As CandidateIsContactInfoVisible,
 [Resume].NcrcLevelId AS CandidateNcrcLevelId
FROM   
 [Data.Application.PersonListPerson] AS PersonListPerson WITH (NOLOCK)  
 INNER JOIN [Data.Application.PersonList] AS PersonList WITH (NOLOCK) ON PersonListPerson.PersonListId = PersonList.Id  
INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON PersonListPerson.PersonId = [Resume].PersonId AND [Resume].IsDefault = 1 




GO


