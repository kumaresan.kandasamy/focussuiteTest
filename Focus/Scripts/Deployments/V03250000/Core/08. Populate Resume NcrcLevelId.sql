CREATE TABLE #Resume
(
	ResumeId BIGINT PRIMARY KEY,
	NcrcLevelId NVARCHAR(MAX)
)

INSERT INTO #Resume 
(
	ResumeId,
	NcrcLevelId
)
SELECT
	R.Id,
	SUBSTRING(R.ResumeXml, CHARINDEX('<ncrc_level>', R.ResumeXml) + 12, CHARINDEX('</ncrc_level>', R.ResumeXml) - CHARINDEX('<ncrc_level>', R.ResumeXml) - 12)
FROM 
	dbo.[Data.Application.Resume] R
WHERE
	R.ResumeXml LIKE '%<ncrc_level>%</ncrc_level>%'
	
UPDATE
	R
SET
	NcrcLevelId = CAST(TR.NcrcLevelId AS BIGINT)
FROM
	[Data.Application.Resume] R
INNER JOIN #Resume TR
	ON TR.ResumeId = R.Id

DROP TABLE #Resume
