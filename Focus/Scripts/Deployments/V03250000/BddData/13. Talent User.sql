DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @EmployerId BIGINT
DECLARE @EmployerAddressId BIGINT
DECLARE @BusinessUnitId BIGINT
DECLARE @BusinessUnitAddressId BIGINT
DECLARE @BusinessUnitDescriptionId BIGINT
DECLARE @EmployeeId BIGINT
DECLARE @EmployeeBusinessUnitId BIGINT
DECLARE @PhoneNumberId BIGINT

DECLARE @EmployerName NVARCHAR(200)
DECLARE @BusinessUnitName NVARCHAR(200)
DECLARE @FirstName NVARCHAR(50)
DECLARE @LastName NVARCHAR(50)
DECLARE @EmailAddress NVARCHAR(200)
DECLARE @ScreenName NVARCHAR(50)
DECLARE @CompanyDescription NVARCHAR(2000)
DECLARE @EmployeeApprovalStatus INT
DECLARE @RedWords NVARCHAR(100)
DECLARE @YellowWords NVARCHAR(100)
DECLARE @EmployerApprovalStatus INT
DECLARE @OverrideFein NVARCHAR(36)
DECLARE @BuApprovalStatus INT

DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @OwnershipTypeId BIGINT
DECLARE @AssistUserId BIGINT
DECLARE @Guid NVARCHAR(36)

SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'TalentUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @OwnershipTypeId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'OwnershipTypes' AND [Key] = 'OwnershipTypes.PrivateCorporation'
SELECT @AssistUserId = Id FROM [Data.Application.User] WHERE UserName = 'dev@client.com'

SELECT @Guid = Left( NEWID(),36)

DECLARE @Employers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	EmployerName NVARCHAR(200),
	BusinessUnitName NVARCHAR(200),
	FirstName NVARCHAR(50),
	LastName NVARCHAR(50),
	EmailAddress NVARCHAR(200),
	ScreenName NVARCHAR(50),
	CompanyDescription  NVARCHAR(2000),
	EmployeeApprovalStatus INT,
	RedWords NVARCHAR(100),
	YellowWords NVARCHAR(100),
	EmployerApprovalStatus INT,
	OverrideFein NVARCHAR(36),
	BuApprovalStatus INT
)

INSERT INTO @Employers 
	(EmployerName, BusinessUnitName, FirstName, LastName, EmailAddress, ScreenName, CompanyDescription, EmployeeApprovalStatus, RedWords, YellowWords, EmployerApprovalStatus, OverrideFein, BuApprovalStatus)
VALUES 
	('New hiring mgr who registered a new employer', 'New Pending Employer', 'Pending', 'EmployerHiringManager', 'newpending@employer.com', 'Bob', 'New hiring mgr who has registered a new employer', 1, NULL, NULL, 1, NULL, 1),
	('New hiring mgr who registered a new business unit against an existing employer', 'New Pending Business Unit', 'Pending', 'BuHiringManager', 'pending@businessunit.com', 'Fred', 'New hiring mgr who registered a new business unit against an existing employer', 1, NULL, NULL, 2, NULL, 1),
	('New hiring mgr who registered against an existing business unit and employer', 'Approved Business Unit', 'Pending', 'HiringManager', 'pending@hiringmanager.com', 'John', 'New hiring mgr who registered against an existing business unit and employer', 1, NULL, NULL, 2, NULL, 2),
	('On hold hiring mgr, on hold employer, on hold BU', 'On hold Business Unit', 'On Hold', 'EmployerHiringManager', 'onhold@employer.com', 'Bob', 'On hold hiring mgr, on hold employer, on hold BU', 4, NULL, NULL, 4, NULL, 4),
	('On hold hiring mgr, approved employer, on hold BU', 'On hold Business Unit', 'On Hold', 'EmployerHiringManager', 'onhold@businessunit.com', 'Bob', 'On hold hiring mgr, approved employer, on hold BU', 4, NULL, NULL, 2, NULL, 4),
	('On hold hiring mgr, approved employer, approved BU', 'Approved Business Unit', 'On Hold', 'EmployerHiringManager', 'onhold@hiringmanager.com', 'Bob', 'On hold hiring mgr, approved employer, approved BU', 4, NULL, NULL, 2, NULL, 2)
	
DECLARE @EmployerCount INT
DECLARE @TotalEmployers INT

SET @EmployerCount = 1
SELECT @TotalEmployers = COUNT(1) FROM @Employers

WHILE @EmployerCount <= @TotalEmployers
BEGIN
	
	SELECT 
		@EmployerName = EmployerName,
		@BusinessUnitName = BusinessUnitName,
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@ScreenName = ScreenName, 
		@CompanyDescription = CompanyDescription,
		@EmployeeApprovalStatus = EmployeeApprovalStatus,
		@RedWords = RedWords,
		@YellowWords = YellowWords,
		@EmployerApprovalStatus = EmployerApprovalStatus,
		@OverrideFein = OverrideFein,
		@BuApprovalStatus = BuApprovalStatus
	FROM
		@Employers
	WHERE
		Id = @EmployerCount
	
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 12
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @UserRoleId = @NextId + 4
		SET @EmployerId = @NextId + 5
		SET @EmployerAddressId = @NextId + 6
		SET @BusinessUnitId = @NextId + 7
		SET @BusinessUnitAddressId = @NextId + 8
		SET @BusinessUnitDescriptionId = @NextId + 9
		SET @EmployeeId = @NextId + 10
		SET @EmployeeBusinessUnitId = @NextId + 11
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			ProgramAreaId,
			CampusId,
			DegreeId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			NULL,
			NULL,
			NULL
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			'4234234234',
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			CreatedOn,
			UpdatedOn
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			2,
			1,
			@ScreenName,
			1,
			1,
			GETDATE(),
			GETDATE()
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		VALUES
		(
			@UserRoleId,
			@RoleId,
			@UserId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		IF NOT EXISTS(SELECT 1 FROM [Data.Application.Employer] WHERE Name = @EmployerName)
		BEGIN
			INSERT INTO [Data.Application.Employer]
			(
				Id,
				Name,
				CommencedOn,
				FederalEmployerIdentificationNumber,
				IsValidFederalEmployerIdentificationNumber,
				StateEmployerIdentificationNumber,
				ApprovalStatus,
				Url,
				ExpiredOn,
				OwnershipTypeId,
				IndustrialClassification,
				TermsAccepted,
				PrimaryPhone,
				PrimaryPhoneExtension,
				PrimaryPhoneType,
				AlternatePhone1,
				AlternatePhone1Type,
				AlternatePhone2,
				AlternatePhone2Type,
				IsRegistrationComplete,
				CreatedOn,
				UpdatedOn
			)
			VALUES
			(
				@EmployerId,
				@EmployerName,
				'01 January 2014',
				ISNULL(@OverrideFein, '11-7777777'),
				1,
				'',
				@EmployerApprovalStatus,
				'',
				NULL,
				@OwnershipTypeId,
				'11194 - Hay Farming',
				1,
				'1111111111',
				'',
				'Phone',
				'',
				'Phone',
				'',
				'Phone',
				1,
				GETDATE(),
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
		
			INSERT INTO [Data.Application.EmployerAddress]
			(
				Id,
				EmployerId,
				Line1,
				Line2,
				Line3,
				TownCity,
				CountyId,
				PostcodeZip,
				StateId,
				CountryId,
				IsPrimary,
				PublicTransitAccessible
			)
			VALUES
			(
				@EmployerAddressId,
				@EmployerId,
				'Address Line 1',
				'',
				'',
				'Bluegrove',
				@CountyId,
				'76352',
				@StateId,
				@CountryId,
				1,
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
		END
		ELSE
		BEGIN
			SELECT 
				@EmployerId = Id 
			FROM 
				[Data.Application.Employer] 
			WHERE 
				Name = @EmployerName
		END
		
		IF NOT EXISTS(SELECT 1 FROM [Data.Application.BusinessUnit] WHERE Name = @BusinessUnitName)
		BEGIN
			INSERT INTO [Data.Application.BusinessUnit]
			(
				Id,
				EmployerId,
				Name,
				IsPrimary,
				Url,
				OwnershipTypeId,
				IndustrialClassification,
				PrimaryPhone,
				PrimaryPhoneExtension,
				PrimaryPhoneType,
				AlternatePhone1,
				AlternatePhone1Type,
				AlternatePhone2,
				AlternatePhone2Type,
				IsPreferred,
				ApprovalStatus
			)
			VALUES
			(
				@BusinessUnitId,
				@EmployerId,
				@BusinessUnitName,
				1,
				'',
				@OwnershipTypeId,
				'11194 - Hay Farming',
				'1111111111',
				'',
				'Phone',
				'',
				'Phone',
				'',
				'Phone',
				1,
				@BuApprovalStatus
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnitAddress]
			(
				Id,
				BusinessUnitId,
				Line1,
				Line2,
				Line3,
				TownCity,
				CountyId,
				PostcodeZip,
				StateId,
				CountryId,
				IsPrimary,
				PublicTransitAccessible
			)
			VALUES
			(
				@BusinessUnitAddressId,
				@BusinessUnitId,
				'Address Line 1',
				'',
				'',
				'Bluegrove',
				@CountyId,
				'76352',
				@StateId,
				@CountryId,
				1,
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnitDescription]
			(
				[Id],
				[BusinessUnitId],
				[Title],
				[Description],
				[IsPrimary]
			)
			VALUES
			(
				@BusinessUnitDescriptionId,
				@BusinessUnitId,
				@CompanyDescription,
				@CompanyDescription,
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
		END
		ELSE
		BEGIN
			SELECT 
				@BusinessUnitId = Id 
			FROM 
				[Data.Application.BusinessUnit]
			WHERE 
				Name = @BusinessUnitName
		END
		
		INSERT INTO [Data.Application.Employee]
		(
			Id,
			EmployerId,
			ApprovalStatus,
			StaffUserId,
			PersonId,
			RedProfanityWords,
			YellowProfanityWords,
			ApprovalStatusChangedBy,
			ApprovalStatusChangedOn
		)
		VALUES
		(
			@EmployeeId,
			@EmployerId,
			@EmployeeApprovalStatus,
			NULL,
			@PersonId,
			@RedWords,
			@YellowWords,
			CASE WHEN @EmployeeApprovalStatus IN (3, 4) THEN @AssistUserId ELSE NULL END,
			CASE WHEN @EmployeeApprovalStatus IN (3, 4) THEN GETDATE() ELSE NULL END
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.EmployeeBusinessUnit]
		(
			Id,
			[Default],
			[BusinessUnitId],
			[EmployeeId]
		)
		VALUES
		(
			@EmployeeBusinessUnitId,
			1,
			@BusinessUnitId,
			@EmployeeId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	SET @EmployerCount = @EmployerCount + 1
END