﻿DECLARE @CandidatePersonId BIGINT
DECLARE @EmployerPersonId BIGINT
DECLARE @PersonListId BIGINT
DECLARE @PersonListPersonId BIGINT

SELECT @EmployerPersonId = Id FROM [Data.Application.Person] WHERE EmailAddress = 'dev@employer.com'
SELECT @CandidatePersonId = Id FROM [Data.Application.Person] WHERE EmailAddress = 'fvn1005seek@invitee.com'

IF NOT EXISTS(SELECT 1 FROM [Data.Application.PersonList] pl JOIN [Data.Application.PersonListPerson] plp ON pl.Id = plp.PersonListId WHERE plp.PersonId = @CandidatePersonId AND pl.ListType = 3 AND pl.PersonId = @EmployerPersonId)
BEGIN
BEGIN TRANSACTION
	
	SELECT @PersonListId = NextId FROM KeyTable
	
	UPDATE KeyTable SET NextId = NextId + 2
	
	SET @PersonListPersonId = @PersonListId + 1
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	INSERT INTO [Data.Application.PersonList]
	(
		Id
		,Name
		,ListType
		,PersonId
	)
	VALUES
	(
		@PersonListId,
		'Flagged applicants',
		3,
		@EmployerPersonId
	)

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	INSERT INTO [Data.Application.PersonListPerson]
	(
		Id
		,PersonId
		,PersonListId
	)
	VALUES
	(
		@PersonListPersonId,
		@CandidatePersonId,
		@PersonListId
	)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION
END