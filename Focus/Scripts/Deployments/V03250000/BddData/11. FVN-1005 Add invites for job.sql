﻿DECLARE @Invites TABLE
(
	Id BIGINT IDENTITY(1, 1),
	LensPostingId NVARCHAR(150),
	Viewed BIT,
	EmailAddress NVARCHAR(255),
	JobTitle NVARCHAR(255), 
	CreatedByUsername NVARCHAR(100),
	JobId BIGINT,
	PersonId BIGINT,
	CreatedBy BIGINT
)

INSERT INTO @Invites(EmailAddress, JobTitle, Viewed, CreatedByUsername)
VALUES
	('fvn1005@invitee.com', 'FVN-1005 Job with invitee', 0, 'dev@employer.com'),
	('fvn1005seek@invitee.com', 'FVN-1005 Job with invitee', 0, 'dev@employer.com')

UPDATE
	I
SET
	PersonId = P.Id,
	JobId = J.Id,
	LensPostingId = PO.LensPostingId,
	CreatedBy = U.Id
FROM
	@Invites I
INNER JOIN [Data.Application.Person] P ON P.EmailAddress = I.EmailAddress
INNER JOIN [Data.Application.Job] J ON J.JobTitle = I.JobTitle
INNER JOIN [Data.Application.Posting] PO ON J.Id = PO.JobId 
INNER JOIN [Data.Application.User] U ON U.Username = I.CreatedByUsername

DECLARE @NextId BIGINT
DECLARE @InviteCount INT
DECLARE @InviteIndex INT

DECLARE @LensPostingId NVARCHAR(150)
DECLARE @Viewed BIT
DECLARE @JobId BIGINT
DECLARE @PersonId BIGINT
DECLARE @InviteId BIGINT
DECLARE @CreatedBy BIGINT

SET @InviteIndex = 1
SELECT @InviteCount = COUNT(1) FROM @Invites

WHILE @InviteIndex <= @InviteCount
BEGIN
	SELECT 
		@LensPostingId = LensPostingId,
		@Viewed = Viewed,
		@JobId = JobId,
		@PersonId = PersonId,
		@CreatedBy = CreatedBy
	FROM
		@Invites
	WHERE
		Id = @InviteIndex

	IF @PersonId IS NOT NULL AND @JobId IS NOT NULL
	BEGIN 
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 1
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @InviteId = @NextId

		INSERT INTO [Data.Application.InviteToApply]
		(
			Id,
			LensPostingId,
			Viewed,
			CreatedBy,
			CreatedOn,
			UpdatedOn,
			JobId,
			PersonId
		)
		VALUES
		(
			@InviteId,
			@LensPostingId,
			@Viewed,
			@CreatedBy,
			GETDATE(),
			GETDATE(),
			@JobId,
			@PersonId
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END

		COMMIT TRANSACTION
	END

	SET @InviteIndex = @InviteIndex + 1
END



