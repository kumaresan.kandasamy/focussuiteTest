﻿DECLARE @Resumes TABLE
(
	Id BIGINT IDENTITY(1, 1),
	PersonEmailAddress NVARCHAR(255),
	ResumeName NVARCHAR(255),
	IsDefault BIT,
	PersonId BIGINT,
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	OverrideResumeXml NVARCHAR(Max),
	CompletionDate DATETIME,
	IsVeteran BIT
)

INSERT INTO @Resumes (PersonEmailAddress, ResumeName, IsDefault, CompletionDate, OverrideResumeXml, IsVeteran)
VALUES 
	('fvn1005@invitee.com', 'FVN-1005 job with invitee resume', 1, NULL, NULL, 0),
	('fvn1005app@invitee.com', 'FVN-1005 job with invitee applicant resume', 1, NULL, NULL, 0),
	('fvn1005seek@invitee.com', 'FVN-1005 job with invitee seeker resume', 1, NULL, NULL, 0)
	
UPDATE
	TR
SET
	PersonId = P.Id,
	FirstName = P.FirstName,
	LastName = P.LastName
FROM
	@Resumes TR
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = TR.PersonEmailAddress

DECLARE @PersonId INT
DECLARE @PersonEmailAddress NVARCHAR(255)
DECLARE @ResumeName NVARCHAR(255)
DECLARE @IsDefault BIT
DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @ResumeId BIGINT
DECLARE @OverrideResumeXml NVARCHAR(MAX)
DECLARE @CompletionDate DATETIME
DECLARE @IsVeteran BIT
DECLARE @DefaultResumeXml NVARCHAR(MAX)

DECLARE @NextId BIGINT

DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @CountyId BIGINT

SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'

DECLARE @ResumeCount INT
DECLARE @TotalResumes INT

SET @ResumeCount = 1
SELECT @TotalResumes = COUNT(1) FROM @Resumes

WHILE @ResumeCount <= @TotalResumes
BEGIN
	SELECT 
		@PersonId = PersonId,
		@ResumeName = ResumeName,
		@IsDefault = IsDefault,
		@PersonEmailAddress = PersonEmailAddress,
		@FirstName = FirstName,
		@LastName = LastName,
		@OverrideResumeXml = OverrideResumeXml,
		@CompletionDate = CompletionDate,
		@IsVeteran = IsVeteran
	FROM
		@Resumes
	WHERE
		Id = @ResumeCount
		
	SET @DefaultResumeXml = N'<ResDoc><special> <branding include="1">FVN-1005 branding statement</branding> <preserved_format>Standard</preserved_format><preserved_order>Emphasize my experience</preserved_order></special><resume><contact><name><givenname>' + @FirstName + N'</givenname><surname>' + @LastName + N'</surname></name> Home:<phone type="home">1111111111</phone><email>' + @PersonEmailAddress + N'</email><website></website><address><street>Address Line 1</street><street></street><city>Bluegrove</city><state_county>48077</state_county><county_fullname>Clay</county_fullname><state>TX</state><state_fullname>Texas</state_fullname><postalcode>76352</postalcode><country>US</country><country_fullname>US</country_fullname></address></contact><experience><employment_status_cd>1</employment_status_cd><job>          <jobid>7c312b3d-6fc3-46ee-872b-0e68b7940fe1</jobid>          <employer>Bulletproof Software</employer>          <title>Software Developer</title>          <description>* Debugged programs to ensure they run smoothly.  * Adapted programs to new requirements.  * Applied programming language skills including: C#, Javascript, HTML    Test Job Description C#, HTML, Javascript, BDD, debugging applications.</description>          <address>            <city>Boston</city>            <state>MA</state>            <state_fullname>Massachusetts</state_fullname>            <country>US</country>            <country_fullname>United States</country_fullname>          </address>          <daterange>            <start>01-2010</start>            <end>01-2013</end>          </daterange>        </job>        <months_experience>36</months_experience>      </experience><education><school_status_cd>5</school_status_cd><norm_edu_level_cd>20</norm_edu_level_cd></education><header><default>1</default><buildmethod>1</buildmethod><resumeid>4760449</resumeid><status>1</status><completionstatus>127</completionstatus><updatedon>23/07/2014 08:44:57</updatedon></header><statements><personal><sex>2</sex><ethnic_heritages><ethnic_heritage><ethnic_id>3</ethnic_id><selection_flag>-1</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>4</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>5</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>2</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>6</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>1</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>-1</ethnic_id><selection_flag>-1</selection_flag></ethnic_heritage></ethnic_heritages><citizen_flag>-1</citizen_flag><disability_status>3</disability_status><license><driver_flag>0</driver_flag></license><veteran><vet_flag>0</vet_flag><subscribe_vps>0</subscribe_vps></veteran><preferences><sal>10</sal><salary_unit_cd>1</salary_unit_cd><resume_searchable>0</resume_searchable><postings_interested_in>0</postings_interested_in><relocate>-1</relocate><shift><shift_first_flag>-1</shift_first_flag><shift_second_flag>-1</shift_second_flag><shift_third_flag>-1</shift_third_flag><shift_rotating_flag>-1</shift_rotating_flag><shift_split_flag>-1</shift_split_flag><shift_varies_flag>-1</shift_varies_flag><work_type>1</work_type><work_over_time>-1</work_over_time></shift></preferences></personal></statements><summary><summary include="1">Summary for FVN-1005</summary></summary><skills><internship_skills /></skills><skillrollup /><hide_options><hide_daterange>0</hide_daterange><hide_eduDates>0</hide_eduDates><hide_militaryserviceDates>0</hide_militaryserviceDates><hide_workDates>0</hide_workDates><hide_contact>0</hide_contact><hide_name>0</hide_name><hide_email>0</hide_email><hide_homePhoneNumber>0</hide_homePhoneNumber><hide_workPhoneNumber>0</hide_workPhoneNumber><hide_cellPhoneNumber>0</hide_cellPhoneNumber><hide_faxPhoneNumber>0</hide_faxPhoneNumber><hide_nonUSPhoneNumber>0</hide_nonUSPhoneNumber><hide_affiliations>0</hide_affiliations><hide_certifications_professionallicenses>0</hide_certifications_professionallicenses><hide_employer>0</hide_employer><hide_driver_license>0</hide_driver_license><unhide_driver_license>0</unhide_driver_license><hide_honors>0</hide_honors><hide_interests>0</hide_interests><hide_internships>0</hide_internships><hide_languages>0</hide_languages><hide_veteran>0</hide_veteran><hide_objective>0</hide_objective><hide_personalinformation>0</hide_personalinformation><hide_professionaldevelopment>0</hide_professionaldevelopment><hide_publications>0</hide_publications><hide_references>0</hide_references><hide_skills>0</hide_skills><hide_technicalskills>0</hide_technicalskills><hide_volunteeractivities>0</hide_volunteeractivities><hide_thesismajorprojects>0</hide_thesismajorprojects><hidden_skills></hidden_skills></hide_options></resume></ResDoc>'
	
	IF @PersonId IS NOT NULL 
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Resume] WHERE PersonId = @PersonId AND ResumeName = @ResumeName AND StatusId = 1)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 1
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @ResumeId = @NextId

		INSERT INTO [Data.Application.Resume]
		(
			Id,
			PrimaryOnet,
			PrimaryROnet,
			ResumeSkills,
			FirstName,
			LastName,
			MiddleName,
			DateOfBirth,
			Gender,
			AddressLine1,
			TownCity,
			StateId,
			CountryId,
			PostcodeZip,
			WorkPhone,
			HomePhone,
			MobilePhone,
			FaxNumber,
			EmailAddress,
			HighestEducationLevel,
			YearsExperience,
			IsVeteran,
			IsActive,
			StatusId,
			ResumeXml,
			ResumeCompletionStatusId,
			IsDefault,
			ResumeName,
			IsSearchable,
			CreatedOn,
			UpdatedOn,
			PersonId,
			SkillsAlreadyCaptured,
			CountyId,
			SkillCount,
			WordCount,
			VeteranPriorityServiceAlertsSubscription,
			EducationCompletionDate
		)
		VALUES
		(
			@ResumeId,
			'',
			'',
			'',
			@FirstName,
			@LastName,
			NULL,
			'01 Jan 1980 00:00:00.000',
			2,
			'Address Line 1',
			'Bluegrove',
			@StateId,
			@CountryId,
			'76352',
			NULL,
			'1111111111',
			NULL,
			NULL,
			@PersonEmailAddress,
			20,
			0,
			@IsVeteran,
			1,
			1,
			ISNULL(REPLACE(@OverrideResumeXml, '##RESUMEID##', @ResumeId), REPLACE(@DefaultResumeXml, '##RESUMEID##', @ResumeId)),
			127,
			1,
			@ResumeName,
			1,
			GETDATE(),
			GETDATE(),
			@PersonId,
			NULL,
			@CountyId,
			0,
			49,
			0,
			@CompletionDate
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END

		COMMIT TRANSACTION
	END
	
	SET @ResumeCount = @ResumeCount + 1
END