IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Migration.UserRequest' AND COLUMN_NAME = 'SecondaryExternalId')
BEGIN
	ALTER TABLE
		[Data.Migration.UserRequest]
	ADD
		SecondaryExternalId NVARCHAR(200) NULL
		
	CREATE INDEX 
		[IX_Data.Migration.UserRequest_RecordType5] 
	ON 
		[Data.Migration.UserRequest] (RecordType, SecondaryExternalId)
END
GO
