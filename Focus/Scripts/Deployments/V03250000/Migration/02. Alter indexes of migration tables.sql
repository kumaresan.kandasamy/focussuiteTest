-- Job Seeker --

IF EXISTS
(
	SELECT 
		1 
	FROM 
		sys.indexes 
	WHERE 
		object_id = OBJECT_ID('[Data.Migration.JobSeekerRequest]')
		AND name = 'IX_Data.Migration.JobSeekerRequest_RecordType'
)
BEGIN
	DROP INDEX [Data.Migration.JobSeekerRequest].[IX_Data.Migration.JobSeekerRequest_RecordType] 
END
GO

IF EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Migration.JobSeekerRequest'
		AND I.name = 'IX_Data.Migration.JobSeekerRequest_RecordType3'
		AND C.name = 'BatchNumber'
		AND key_ordinal = 3
)
BEGIN
	DROP INDEX [Data.Migration.JobSeekerRequest].[IX_Data.Migration.JobSeekerRequest_RecordType3]
	
	CREATE INDEX 
		[IX_Data.Migration.JobSeekerRequest_RecordType3] 
	ON 
		[Data.Migration.JobSeekerRequest] (RecordType, [Status], Id)
END
GO

-- Job Order --

IF EXISTS
(
	SELECT 
		1 
	FROM 
		sys.indexes 
	WHERE 
		object_id = OBJECT_ID('[Data.Migration.JobOrderRequest]')
		AND name = 'IX_Data.Migration.JobOrderRequest_RecordType'
)
BEGIN
	DROP INDEX [Data.Migration.JobOrderRequest].[IX_Data.Migration.JobOrderRequest_RecordType] 
END
GO

IF EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Migration.JobOrderRequest'
		AND I.name = 'IX_Data.Migration.JobOrderRequest_RecordType3'
		AND C.name = 'BatchNumber'
		AND key_ordinal = 3
)
BEGIN
	DROP INDEX [Data.Migration.JobOrderRequest].[IX_Data.Migration.JobOrderRequest_RecordType3]
	
	CREATE INDEX 
		[IX_Data.Migration.JobOrderRequest_RecordType3] 
	ON 
		[Data.Migration.JobOrderRequest] (RecordType, [Status], Id)
END
GO

-- Employer --

IF EXISTS
(
	SELECT 
		1 
	FROM 
		sys.indexes 
	WHERE 
		object_id = OBJECT_ID('[Data.Migration.EmployerRequest]')
		AND name = 'IX_Data.Migration.EmployerRequest_RecordType'
)
BEGIN
	DROP INDEX [Data.Migration.EmployerRequest].[IX_Data.Migration.EmployerRequest_RecordType] 
END
GO

IF EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Migration.EmployerRequest'
		AND I.name = 'IX_Data.Migration.EmployerRequest_RecordType3'
		AND C.name = 'BatchNumber'
		AND key_ordinal = 3
)
BEGIN
	DROP INDEX [Data.Migration.EmployerRequest].[IX_Data.Migration.EmployerRequest_RecordType3]
	
	CREATE INDEX 
		[IX_Data.Migration.EmployerRequest_RecordType3] 
	ON 
		[Data.Migration.EmployerRequest] (RecordType, [Status], Id)
END
GO

-- User --

IF EXISTS
(
	SELECT 
		1 
	FROM 
		sys.indexes 
	WHERE 
		object_id = OBJECT_ID('[Data.Migration.UserRequest]')
		AND name = 'IX_Data.Migration.UserRequest_RecordType'
)
BEGIN
	DROP INDEX [Data.Migration.UserRequest].[IX_Data.Migration.UserRequest_RecordType] 
END
GO

IF EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Migration.UserRequest'
		AND I.name = 'IX_Data.Migration.UserRequest_RecordType3'
		AND C.name = 'BatchNumber'
		AND key_ordinal = 3
)
BEGIN
	DROP INDEX [Data.Migration.UserRequest].[IX_Data.Migration.UserRequest_RecordType3]
	
	CREATE INDEX 
		[IX_Data.Migration.UserRequest_RecordType3] 
	ON 
		[Data.Migration.UserRequest] (RecordType, [Status], Id)
END
GO
