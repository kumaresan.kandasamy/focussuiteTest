UPDATE [Config.CodeItem] SET ExternalId = '10,12,13,14' WHERE [Key] = 'RequiredEducationLevel.LessThanBachelorsDegree';

UPDATE [Config.CodeGroupItem] SET DisplayOrder = 0 WHERE  CodeItemId = (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.LessThanBachelorsDegree' );

UPDATE [Config.CodeGroupItem] SET DisplayOrder = 0 WHERE  CodeItemId = (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.GraduateDegree' );

UPDATE [Config.CodeGroupItem] SET DisplayOrder = 5 WHERE  CodeItemId = (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.BachelorsDegree' );

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.NoDiploma')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('RequiredEducationLevel.NoDiploma', 1, '10');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'RequiredEducationLevel.NoDiploma', 'No diploma', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (1, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'RequiredEducationLevels' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.NoDiploma' ));
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.HighSchoolDiplomaOrEquivalent')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('RequiredEducationLevel.HighSchoolDiplomaOrEquivalent', 1, '12');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'RequiredEducationLevel.HighSchoolDiplomaOrEquivalent', 'High school diploma or equivalent', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (2, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'RequiredEducationLevels' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.HighSchoolDiplomaOrEquivalent' ));
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.SomeCollegeNoDegree')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('RequiredEducationLevel.SomeCollegeNoDegree', 1, '13');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'RequiredEducationLevel.SomeCollegeNoDegree', 'Some college, no degree', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (3, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'RequiredEducationLevels' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.SomeCollegeNoDegree' ));
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.AssociatesDegree')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('RequiredEducationLevel.AssociatesDegree', 1, '14');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'RequiredEducationLevel.AssociatesDegree', 'Associate''s or vocational degree', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (4, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'RequiredEducationLevels' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.AssociatesDegree' ));
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.MastersDegree')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('RequiredEducationLevel.MastersDegree', 1, '18');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'RequiredEducationLevel.MastersDegree', 'Master''s degree', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (6, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'RequiredEducationLevels' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.MastersDegree' ));
END

IF NOT EXISTS (SELECT 1 FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.DoctorateDegree')
BEGIN
	INSERT INTO [Config.CodeItem] ([Key], IsSystem, ExternalId) VALUES ('RequiredEducationLevel.DoctorateDegree', 1, '21');

	INSERT INTO [Config.LocalisationItem] (ContextKey, [Key], [Value], Localised, LocalisationId) VALUES ('', 'RequiredEducationLevel.DoctorateDegree', 'Doctorate degree', 0, 12090);
    
    INSERT INTO [Config.CodeGroupItem] (DisplayOrder, CodeGroupId, CodeItemId) VALUES  (7, (SELECT Id FROM [Config.CodeGroup] WHERE [Key] = 'RequiredEducationLevels' ), (SELECT Id FROM [Config.CodeItem] WHERE [Key] = 'RequiredEducationLevel.DoctorateDegree' ));
END
