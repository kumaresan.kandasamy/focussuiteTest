BEGIN TRANSACTION

BEGIN TRY
	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Ensure compliance with regulations and laws governing health, quality, and safety'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019789'
	END
	ELSE
		PRINT 'Ensure compliance with regulations and laws governing health, quality, and safety Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Verify that transportation and handling procedures meet regulatory requirements'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019790'
	END
	ELSE
		PRINT 'Verify that transportation and handling procedures meet regulatory requirements Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Collect environmental samples'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019791'
	END
	ELSE
		PRINT 'Collect environmental samples Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Route samples for testing'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019792'
	END
	ELSE
		PRINT 'Route samples for testing Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Interpret rules, laws, and procedures'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019793'
	END
	ELSE
		PRINT 'Interpret rules, laws, and procedures Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Explain required standards to agricultural workers'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019795'
	END
	ELSE
		PRINT 'Explain required standards to agricultural workers Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Prepare inspection reports'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019796'
	END
	ELSE
		PRINT 'Prepare inspection reports Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Make corrective recommendations'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019797'
	END
	ELSE
		PRINT 'Make corrective recommendations Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Advise farmers, growers, or processors of corrective actions'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019798'
	END
	ELSE
		PRINT 'Advise farmers, growers, or processors of corrective actions Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Inspect the cleanliness and practices of establishment employees'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019799'
	END
	ELSE
		PRINT 'Inspect the cleanliness and practices of establishment employees Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Ensure food and meat processing facilities meet quality standards'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019800'
	END
	ELSE
		PRINT 'Ensure food and meat processing facilities meet quality standards Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Inspect livestock'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019801'
	END
	ELSE
		PRINT 'Inspect livestock Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Check and grade condition of food products'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019802'
	END
	ELSE
		PRINT 'Check and grade condition of food products Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Determine whether products are safe for consumption'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019803'
	END
	ELSE
		PRINT 'Determine whether products are safe for consumption Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Take emergency actions when product safety is compromised'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019804'
	END
	ELSE
		PRINT 'Take emergency actions when product safety is compromised Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Operate a variety of inspection equipment'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019805'
	END
	ELSE
		PRINT 'Operate a variety of inspection equipment Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Protect the public from health and safety hazards'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019806'
	END
	ELSE
		PRINT 'Protect the public from health and safety hazards Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Ability to work long and irregular hours'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019807'
	END
	ELSE
		PRINT 'Ability to work long and irregular hours Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Exhibit good judgment in difficult situations'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019808'
	END
	ELSE
		PRINT 'Exhibit good judgment in difficult situations Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Other certification(s): [What other related certifications do you have?]'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019809'
	END
	ELSE
		PRINT 'Other certification(s): [What other related certifications do you have?] Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Used software tools including: [What industry specific software have you used?]'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019810'
	END
	ELSE
		PRINT 'Used software tools including: [What industry specific software have you used?] Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Language skills: [Do you have foreign language skills? If so please list languages and level of proficiency.]'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019811'
	END
	ELSE
		PRINT 'Language skills: [Do you have foreign language skills? If so please list languages and level of proficiency.] Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Performed inspections of the following: [What specifically did you inspect? e.g., agricultural commodities, processing equipment, etc.]'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019812'
	END
	ELSE
		PRINT 'Performed inspections of the following: [What specifically did you inspect? e.g., agricultural commodities, processing equipment, etc.] Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Investigate complaints of'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019813'
	END
	ELSE
		PRINT 'Investigate complaints of Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Ensure compliance with regulations and laws governing health, quality, and safety'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613587'
	END
	ELSE
		PRINT 'Ensure compliance with regulations and laws governing health, quality, and safety Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Verify that transportation and handling procedures meet regulatory requirements'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613588'
	END
	ELSE
		PRINT 'Verify that transportation and handling procedures meet regulatory requirements Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Collect environmental samples'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613589'
	END
	ELSE
		PRINT 'Collect environmental samples Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Route samples for testing'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613590'
	END
	ELSE
		PRINT 'Route samples for testing Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Interpret rules, laws, and procedures'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613591'
	END
	ELSE
		PRINT 'Interpret rules, laws, and procedures Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Explain required standards to agricultural workers'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613593'
	END
	ELSE
		PRINT 'Explain required standards to agricultural workers Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Prepare inspection reports'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613594'
	END
	ELSE
		PRINT 'Prepare inspection reports Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Make corrective recommendations'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613595'
	END
	ELSE
		PRINT 'Make corrective recommendations Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Advise farmers, growers, or processors of corrective actions'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613596'
	END
	ELSE
		PRINT 'Advise farmers, growers, or processors of corrective actions Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Inspect the cleanliness and practices of establishment employees'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613597'
	END
	ELSE
		PRINT 'Inspect the cleanliness and practices of establishment employees Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Ensure food and meat processing facilities meet quality standards'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613598'
	END
	ELSE
		PRINT 'Ensure food and meat processing facilities meet quality standards Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Inspect livestock'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613599'
	END
	ELSE
		PRINT 'Inspect livestock Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Check and grade condition of food products'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613600'
	END
	ELSE
		PRINT 'Check and grade condition of food products Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Determine whether products are safe for consumption'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613601'
	END
	ELSE
		PRINT 'Determine whether products are safe for consumption Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Take emergency actions when product safety is compromised'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613602'
	END
	ELSE
		PRINT 'Take emergency actions when product safety is compromised Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Operate a variety of inspection equipment'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613603'
	END
	ELSE
		PRINT 'Operate a variety of inspection equipment Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Protect the public from health and safety hazards'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613604'
	END
	ELSE
		PRINT 'Protect the public from health and safety hazards Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Ability to work long and irregular hours'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613605'
	END
	ELSE
		PRINT 'Ability to work long and irregular hours Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Exhibit good judgment in difficult situations'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613606'
	END
	ELSE
		PRINT 'Exhibit good judgment in difficult situations Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Other certification(s): [What other related certifications do the job seeker have?]'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613607'
	END
	ELSE
		PRINT 'Other certification(s): [What other related certifications do the job seeker have?] Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Used software tools including: [What industry specific software have the job seeker used?]'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613608'
	END
	ELSE
		PRINT 'Used software tools including: [What industry specific software have the job seeker used?] Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Language skills: [Do the job seeker have foreign language skills? If so please list languages and level of proficiency.]'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613609'
	END
	ELSE
		PRINT 'Language skills: [Do the job seeker have foreign language skills? If so please list languages and level of proficiency.] Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Performed inspections of the following: [What specifically did the job seeker inspect? e.g., agricultural commodities, processing equipment, etc.]'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613610'
	END
	ELSE
		PRINT 'Performed inspections of the following: [What specifically did the job seeker inspect? e.g., agricultural commodities, processing equipment, etc.] Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Investigate complaints of'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613611'
	END
	ELSE
		PRINT 'Investigate complaints of Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Enforce farmersn++ market rules and regulations'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1019794'
	END
	ELSE
		PRINT 'Primary Value: Enforce farmersn++ market rules and regulations Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Enforce farmers? market rules and regulations'
				)
			)
	BEGIN
		DELETE
		FROM dbo.[Library.JobTaskLocalisationItem]
		WHERE dbo.[Library.JobTaskLocalisationItem].[Key] = 'JobTask.45-2011.00.1613592'
	END
	ELSE
		PRINT 'Primary Value: Enforce farmers? market rules and regulations Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Valid state or international driver?s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Valid state or international driver''s license'
		WHERE SecondaryValue = 'Valid state or international driver?s license'
	END
	ELSE
		PRINT 'Secondary Value: Valid state or international driver?s license Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Valid state or international driver?s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Valid state or international driver''s license'
		WHERE PrimaryValue = 'Valid state or international driver?s license'
	END
	ELSE
		PRINT 'Primary Value: Valid state or international driver?s license Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Used inventory management software?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Used inventory management software.'
		WHERE SecondaryValue = 'Used inventory management software?'
	END
	ELSE
		PRINT 'Secondary Value: Used inventory management software? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Valid driver?s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Valid driver''s license.'
		WHERE SecondaryValue = 'Valid driver?s license'
	END
	ELSE
		PRINT 'Secondary Value: Valid driver?s license Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Valid driver?s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Valid driver''s license.'
		WHERE PrimaryValue = 'Valid driver?s license'
	END
	ELSE
		PRINT 'Primary Value: Valid driver?s license Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography?mass spectrometry LC/MSMSMS to confirm detected drugs'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography-mass spectrometry LC/MSMSMS to confirm detected drugs'
		WHERE SecondaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography?mass spectrometry LC/MSMSMS to confirm detected drugs'
	END
	ELSE
		PRINT 'Secondary Value: Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography?mass spectrometry LC/MSMSMS to confirm detected drugs  Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Research products? market appeal'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Research product''s market appeal'
		WHERE SecondaryValue = 'Research products? market appeal'
	END
	ELSE
		PRINT 'Secondary Value: Research products? market appeal  Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Prepared checks that listed and totaled meal costs and sales taxes?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Prepare checks that list and total meal costs and sales taxes.'
		WHERE SecondaryValue = 'Prepared checks that listed and totaled meal costs and sales taxes?'
	END
	ELSE
		PRINT 'Secondary Value: Prepared checks that listed and totaled meal costs and sales taxes? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Enforce farmers? market rules and regulations'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Enforce farmer''s market rules and regulations.'
		WHERE SecondaryValue = 'Enforce farmers? market rules and regulations'
	END
	ELSE
		PRINT 'Secondary Value: Enforce farmers? market rules and regulations Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Enforce farmers? market rules and regulations'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Enforce farmer''s market rules and regulations.'
		WHERE PrimaryValue = 'Enforce farmers? market rules and regulations'
	END
	ELSE
		PRINT 'Primary Value: Enforce farmers? market rules and regulations Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Work with outdoor and home d?cor products such as'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Work with outdoor and home decor products such as:'
		WHERE SecondaryValue = 'Work with outdoor and home d?cor products such as'
	END
	ELSE
		PRINT 'Secondary Value: Work with outdoor and home d?cor products such as Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Work with outdoor and home d?cor products such as'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Work with outdoor and home decor products such as:'
		WHERE PrimaryValue = 'Work with outdoor and home d?cor products such as'
	END
	ELSE
		PRINT 'Primary Value: Work with outdoor and home d?cor products such as Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Valid commercial driver?s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Valid commercial driver''s license.'
		WHERE SecondaryValue = 'Valid commercial driver?s license'
	END
	ELSE
		PRINT 'Secondary Value: Valid commercial driver?s license Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Valid commercial driver?s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Valid commercial driver''s license.'
		WHERE PrimaryValue = 'Valid commercial driver?s license'
	END
	ELSE
		PRINT 'Primary Value: Valid commercial driver?s license Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Create models of patients? mouths'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Create models of patient''s mouths'
		WHERE SecondaryValue = 'Create models of patients? mouths'
	END
	ELSE
		PRINT 'Secondary Value: Create models of patients? mouths Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Create models of patients? mouths'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Create models of patient''s mouths'
		WHERE PrimaryValue = 'Create models of patients? mouths'
	END
	ELSE
		PRINT 'Primary Value: Create models of patients? mouths Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Assess children?s developmental needs on an ongoing basis'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Assess children''s developmental needs on an ongoing basis'
		WHERE SecondaryValue = 'Assess children?s developmental needs on an ongoing basis'
	END
	ELSE
		PRINT 'Secondary Value: Assess children?s developmental needs on an ongoing basis Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Assess children?s developmental needs on an ongoing basis'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Assess children''s developmental needs on an ongoing basis'
		WHERE PrimaryValue = 'Assess children?s developmental needs on an ongoing basis'
	END
	ELSE
		PRINT 'Primary Value: Assess children?s developmental needs on an ongoing basis Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Monitor animals? clinical symptoms'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Monitor animal''s clinical symptoms.'
		WHERE SecondaryValue = 'Monitor animals? clinical symptoms'
	END
	ELSE
		PRINT 'Secondary Value: Monitor animals? clinical symptoms Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Monitor animals? clinical symptoms'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Monitor animal''s clinical symptoms.'
		WHERE PrimaryValue = 'Monitor animals? clinical symptoms'
	END
	ELSE
		PRINT 'Primary Value: Monitor animals? clinical symptoms Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Clean animals? teeth'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Clean animal''s teeth'
		WHERE SecondaryValue = 'Clean animals? teeth'
	END
	ELSE
		PRINT 'Secondary Value: Clean animal''s teeth Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Clean animals? teeth'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Clean animal''s teeth'
		WHERE PrimaryValue = 'Clean animals? teeth'
	END
	ELSE
		PRINT 'Primary Value: Clean animal''s teeth Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Print tickets to be picked up at ?will call" counter'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Print tickets to be picked up at "will call" counter.'
		WHERE SecondaryValue = 'Print tickets to be picked up at ?will call" counter'
	END
	ELSE
		PRINT 'Secondary Value: Print tickets to be picked up at ?will call" counter Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Print tickets to be picked up at ?will call" counter'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Print tickets to be picked up at "will call" counter.'
		WHERE PrimaryValue = 'Print tickets to be picked up at ?will call" counter'
	END
	ELSE
		PRINT 'Secondary Value: Print tickets to be picked up at ?will call" counter Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Write programs in the language of machine?s users.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Write programs in the language of machine''s users.'
		WHERE PrimaryValue = 'Write programs in the language of machine?s users.'
	END
	ELSE
		PRINT 'Primary Value: Write programs in the language of machine?s users. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Match performer?s attributes to specific roles'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Match performer''s attributes to specific roles'
		WHERE PrimaryValue = 'Match performer?s attributes to specific roles'
	END
	ELSE
		PRINT 'Primary Value: Match performer?s attributes to specific roles Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Research products? market appeal'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Research product''s market appeal'
		WHERE PrimaryValue = 'Research products? market appeal'
	END
	ELSE
		PRINT 'Primary Value: Research products? market appeal Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Have forensic training?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Have forensic training'
		WHERE PrimaryValue = 'Have forensic training?'
	END
	ELSE
		PRINT 'Have forensic training? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Coordinate resources for manufacturing?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Coordinate resources for manufacturing'
		WHERE PrimaryValue = 'Coordinate resources for manufacturing?'
	END
	ELSE
		PRINT 'Coordinate resources for manufacturing? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Use inventory management software?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Use inventory management software'
		WHERE PrimaryValue = 'Use inventory management software?'
	END
	ELSE
		PRINT 'Use inventory management software? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Confer with other managers to determine production priorities?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Confer with other managers to determine production priorities'
		WHERE PrimaryValue = 'Confer with other managers to determine production priorities?'
	END
	ELSE
		PRINT 'Confer with other managers to determine production priorities? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Licensed pest control applicator?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Licensed pest control applicator'
		WHERE PrimaryValue = 'Licensed pest control applicator?'
	END
	ELSE
		PRINT 'Licensed pest control applicator? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Licensed arborist?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Licensed arborist'
		WHERE PrimaryValue = 'Licensed arborist?'
	END
	ELSE
		PRINT 'Licensed arborist? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Licensed irrigator?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Licensed irrigator'
		WHERE PrimaryValue = 'Licensed irrigator?'
	END
	ELSE
		PRINT 'Licensed irrigator? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified substance abuse counselor?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified substance abuse counselor'
		WHERE PrimaryValue = 'Certified substance abuse counselor?'
	END
	ELSE
		PRINT 'Certified substance abuse counselor? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified mental health counselor?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified mental health counselor'
		WHERE PrimaryValue = 'Certified mental health counselor?'
	END
	ELSE
		PRINT 'Certified mental health counselor? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified Patient Care Technician (PCT)?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified Patient Care Technician (PCT)'
		WHERE PrimaryValue = 'Certified Patient Care Technician (PCT)?'
	END
	ELSE
		PRINT 'Certified Patient Care Technician (PCT)? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified surgical technologist?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified surgical technologist'
		WHERE PrimaryValue = 'Certified surgical technologist?'
	END
	ELSE
		PRINT 'Certified surgical technologist? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified Patient Care Assistant (PCA)?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified Patient Care Assistant (PCA)'
		WHERE PrimaryValue = 'Certified Patient Care Assistant (PCA)?'
	END
	ELSE
		PRINT 'Certified Patient Care Assistant (PCA)? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified surgical technician?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified surgical technician'
		WHERE PrimaryValue = 'Certified surgical technician?'
	END
	ELSE
		PRINT 'Certified surgical technician? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified in BLS (Basic Life Support) including CPR (Cardiopulmonary Resuscitation)?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified in BLS (Basic Life Support) including CPR (Cardiopulmonary Resuscitation)'
		WHERE PrimaryValue = 'Certified in BLS (Basic Life Support) including CPR (Cardiopulmonary Resuscitation)?'
	END
	ELSE
		PRINT 'Certified in BLS (Basic Life Support) including CPR (Cardiopulmonary Resuscitation)? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified Nursing Assistant (CNA)?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified Nursing Assistant (CNA)'
		WHERE PrimaryValue = 'Certified Nursing Assistant (CNA)?'
	END
	ELSE
		PRINT 'Certified Nursing Assistant (CNA)? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified Medical Assistant (CMA)?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified Medical Assistant (CMA)'
		WHERE PrimaryValue = 'Certified Medical Assistant (CMA)?'
	END
	ELSE
		PRINT 'Certified Medical Assistant (CMA)? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Registered Nurse (RN)?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Registered Nurse (RN)'
		WHERE PrimaryValue = 'Registered Nurse (RN)?'
	END
	ELSE
		PRINT 'Registered Nurse (RN)? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Licensed Vocational Nurse (LVN)?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Licensed Vocational Nurse (LVN)'
		WHERE PrimaryValue = 'Licensed Vocational Nurse (LVN)?'
	END
	ELSE
		PRINT 'Licensed Vocational Nurse (LVN)? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Help endoscopy team perform procedures efficiently?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Help endoscopy team perform procedures efficiently'
		WHERE PrimaryValue = 'Help endoscopy team perform procedures efficiently?'
	END
	ELSE
		PRINT 'Help endoscopy team perform procedures efficiently? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'First aid certified?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'First aid certified'
		WHERE PrimaryValue = 'First aid certified?'
	END
	ELSE
		PRINT 'First aid certified? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'CPR/AED (Automated External Defibrillator) certified?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'CPR/AED (Automated External Defibrillator) certified'
		WHERE PrimaryValue = 'CPR/AED (Automated External Defibrillator) certified?'
	END
	ELSE
		PRINT 'CPR/AED (Automated External Defibrillator) certified? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Licensed principal?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Licensed principal'
		WHERE PrimaryValue = 'Licensed principal?'
	END
	ELSE
		PRINT 'Licensed principal? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified educator?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified educator'
		WHERE PrimaryValue = 'Certified educator?'
	END
	ELSE
		PRINT 'Certified educator? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Give emotional support to patients and their families?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Give emotional support to patients and their families'
		WHERE PrimaryValue = 'Give emotional support to patients and their families?'
	END
	ELSE
		PRINT 'Give emotional support to patients and their families? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Provide companionship to keep patients mentally healthy?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Provide companionship to keep patients mentally healthy'
		WHERE PrimaryValue = 'Provide companionship to keep patients mentally healthy?'
	END
	ELSE
		PRINT 'Provide companionship to keep patients mentally healthy? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified vocational counselor?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified vocational counselor'
		WHERE PrimaryValue = 'Certified vocational counselor?'
	END
	ELSE
		PRINT 'Certified vocational counselor? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Employ specialized massage techniques including Trigger Point, Shiatsu, or Hot Stone Therapy massage?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Employ specialized massage techniques including Trigger Point, Shiatsu, or Hot Stone Therapy massage'
		WHERE PrimaryValue = 'Employ specialized massage techniques including Trigger Point, Shiatsu, or Hot Stone Therapy massage?'
	END
	ELSE
		PRINT 'Employ specialized massage techniques including Trigger Point, Shiatsu, or Hot Stone Therapy massage? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'AHDI/AAMT Certified medical transcriptionist?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'AHDI/AAMT Certified medical transcriptionist'
		WHERE PrimaryValue = 'AHDI/AAMT Certified medical transcriptionist?'
	END
	ELSE
		PRINT 'AHDI/AAMT Certified medical transcriptionist? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'AHDI/AAMT Registered medical transcriptionist?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'AHDI/AAMT Registered medical transcriptionist'
		WHERE PrimaryValue = 'AHDI/AAMT Registered medical transcriptionist?'
	END
	ELSE
		PRINT 'AHDI/AAMT Registered medical transcriptionist? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Enter and retrieve information from computerized medical records systems?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Enter and retrieve information from computerized medical records systems'
		WHERE PrimaryValue = 'Enter and retrieve information from computerized medical records systems?'
	END
	ELSE
		PRINT 'Enter and retrieve information from computerized medical records systems? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Experience using the SOAP (Subjective, Objective, Assessment, Plan) note format?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Experience using the SOAP (Subjective, Objective, Assessment, Plan) note format'
		WHERE PrimaryValue = 'Experience using the SOAP (Subjective, Objective, Assessment, Plan) note format?'
	END
	ELSE
		PRINT 'Experience using the SOAP (Subjective, Objective, Assessment, Plan) note format? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Order taxis or other transportation for drunk patrons?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Order taxis or other transportation for drunk patrons'
		WHERE PrimaryValue = 'Order taxis or other transportation for drunk patrons?'
	END
	ELSE
		PRINT 'Order taxis or other transportation for drunk patrons? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Perform reference and background checks on applicants?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Perform reference and background checks on applicants'
		WHERE PrimaryValue = 'Perform reference and background checks on applicants?'
	END
	ELSE
		PRINT 'Perform reference and background checks on applicants? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Answer questions regarding specific energy costs, including home appliances?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Answer questions regarding specific energy costs, including home appliances'
		WHERE PrimaryValue = 'Answer questions regarding specific energy costs, including home appliances?'
	END
	ELSE
		PRINT 'Answer questions regarding specific energy costs, including home appliances? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Certified rehabilitation counselor?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Certified rehabilitation counselor'
		WHERE PrimaryValue = 'Certified rehabilitation counselor?'
	END
	ELSE
		PRINT 'Certified rehabilitation counselor? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Select furnishings and d?cor'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Select furnishings and decor'
		WHERE PrimaryValue = 'Select furnishings and d?cor'
	END
	ELSE
		PRINT 'Select furnishings and d?cor Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Demonstrate good computer skills?'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Demonstrate good computer skills'
		WHERE PrimaryValue = 'Demonstrate good computer skills?'
	END
	ELSE
		PRINT 'Demonstrate good computer skills? Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Valid state or international drivern++s license.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Valid state or international driver''s license.'
		WHERE SecondaryValue = 'Valid state or international drivern++s license.'
	END
	ELSE
		PRINT 'Valid state or international drivern++s license. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Valid drivern++s license.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Valid driver''s license.'
		WHERE SecondaryValue = 'Valid drivern++s license.'
	END
	ELSE
		PRINT 'Valid drivern++s license. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Used gas chromatography-mass spectroscopy (GC-MS) and liquid chromatographyn++mass spectrometry LC/MSMSMS to confirm detected drugs.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Used gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography-mass spectrometry LC/MSMSMS to confirm detected drugs.'
		WHERE SecondaryValue = 'Used gas chromatography-mass spectroscopy (GC-MS) and liquid chromatographyn++mass spectrometry LC/MSMSMS to confirm detected drugs.'
	END
	ELSE
		PRINT 'Used gas chromatography-mass spectroscopy (GC-MS) and liquid chromatographyn++mass spectrometry LC/MSMSMS to confirm detected drugs. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Researched productsn++ market appeal.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Researched product''s market appeal.'
		WHERE SecondaryValue = 'Researched productsn++ market appeal.'
	END
	ELSE
		PRINT 'Researched productsn++ market appeal. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Selected furnishings and dn++cor.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Selected furnishings and decor.'
		WHERE SecondaryValue = 'Selected furnishings and dn++cor.'
	END
	ELSE
		PRINT 'Selected furnishings and dn++cor. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Enforced farmersn++ market rules and regulations.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Enforced farmer''s market rules and regulations.'
		WHERE SecondaryValue = 'Enforced farmersn++ market rules and regulations.'
	END
	ELSE
		PRINT 'Enforced farmersn++ market rules and regulations. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Worked with outdoor and home dn++cor products such as'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Worked with outdoor and home decor products such as'
		WHERE SecondaryValue = 'Worked with outdoor and home dn++cor products such as'
	END
	ELSE
		PRINT 'Worked with outdoor and home dn++cor products such as Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Demonstrated good computer skills.n++'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Demonstrated good computer skills.'
		WHERE SecondaryValue = 'Demonstrated good computer skills.n++'
	END
	ELSE
		PRINT 'Demonstrated good computer skills.n++ Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Valid commercial drivern++s license.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Valid commercial driver''s license.'
		WHERE SecondaryValue = 'Valid commercial drivern++s license.'
	END
	ELSE
		PRINT 'Valid commercial drivern++s license. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Created models of patientsn++ mouths.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Created models of patient''s mouths.'
		WHERE SecondaryValue = 'Created models of patientsn++ mouths.'
	END
	ELSE
		PRINT 'Created models of patientsn++ mouths. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Assessed childrenn++s developmental needs on an ongoing basis.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Assessed children''s developmental needs on an ongoing basis.'
		WHERE SecondaryValue = 'Assessed childrenn++s developmental needs on an ongoing basis.'
	END
	ELSE
		PRINT 'Assessed childrenn++s developmental needs on an ongoing basis. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Monitored animalsn++ clinical symptoms.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Monitored animal''s clinical symptoms.'
		WHERE SecondaryValue = 'Monitored animalsn++ clinical symptoms.'
	END
	ELSE
		PRINT 'Monitored animalsn++ clinical symptoms. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Cleaned animalsn++ teeth.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Cleaned animal''s teeth.'
		WHERE SecondaryValue = 'Cleaned animalsn++ teeth.'
	END
	ELSE
		PRINT 'Cleaned animalsn++ teeth. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.SecondaryValue = 'Printed tickets to be picked up at n++will call" counter.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET SecondaryValue = 'Printed tickets to be picked up at "will call" counter.'
		WHERE SecondaryValue = 'Printed tickets to be picked up at n++will call" counter.'
	END
	ELSE
		PRINT 'Printed tickets to be picked up at n++will call" counter. Not Found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Valid state or international drivern++s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Valid state or international driver''s license.'
		WHERE PrimaryValue = 'Valid state or international drivern++s license'
	END
	ELSE
		PRINT 'Valid state or international drivern++s license Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Speak another language including: [Do you have foreign language skills? If so please list languages and level of proficiency.] n++n++n++n++'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Speak another language including: [Do you have foreign language skills? If so please list languages and level of proficiency.]'
		WHERE PrimaryValue = 'Speak another language including: [Do you have foreign language skills? If so please list languages and level of proficiency.] n++n++n++n++'
	END
	ELSE
		PRINT 'Speak another language including: [Do you have foreign language skills? If so please list languages and level of proficiency.] n++n++n++n++ Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Valid drivern++s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Valid driver''s license.'
		WHERE PrimaryValue = 'Valid drivern++s license'
	END
	ELSE
		PRINT 'Valid drivern++s license Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatographyn++mass spectrometry LC/MSMSMS to confirm detected drugs.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography-mass spectrometry LC/MSMSMS to confirm detected drugs.'
		WHERE PrimaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatographyn++mass spectrometry LC/MSMSMS to confirm detected drugs.'
	END
	ELSE
		PRINT 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatographyn++mass spectrometry LC/MSMSMS to confirm detected drugs. Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Research productsn++ market appeal'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Research product''s market appeal.'
		WHERE PrimaryValue = 'Research productsn++ market appeal'
	END
	ELSE
		PRINT 'Research productsn++ market appeal Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Select furnishings and dn++cor'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Select furnishings and decor.'
		WHERE PrimaryValue = 'Select furnishings and dn++cor'
	END
	ELSE
		PRINT 'Select furnishings and dn++cor Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Write programs in the language of machinen++s users.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Write programs in the language of machine''s users.'
		WHERE PrimaryValue = 'Write programs in the language of machinen++s users.'
	END
	ELSE
		PRINT 'Write programs in the language of machinen++s users. Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Enforce farmersn++ market rules and regulations'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Enforce farmer''s market rules and regulations.'
		WHERE PrimaryValue = 'Enforce farmersn++ market rules and regulations'
	END
	ELSE
		PRINT 'Enforce farmersn++ market rules and regulations Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Work with outdoor and home dn++cor products such as'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Work with outdoor and home decor products such as'
		WHERE PrimaryValue = 'Work with outdoor and home dn++cor products such as'
	END
	ELSE
		PRINT 'Work with outdoor and home dn++cor products such as Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Demonstrate good computer skillsn++'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Demonstrate good computer skills.'
		WHERE PrimaryValue = 'Demonstrate good computer skillsn++'
	END
	ELSE
		PRINT 'Demonstrate good computer skillsn++ Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Valid commercial drivern++s license'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Valid commercial driver''s license.'
		WHERE PrimaryValue = 'Valid commercial drivern++s license'
	END
	ELSE
		PRINT 'Valid commercial drivern++s license Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Create models of patientsn++ mouths'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Create models of patient''s mouths.'
		WHERE PrimaryValue = 'Create models of patientsn++ mouths'
	END
	ELSE
		PRINT 'Create models of patientsn++ mouths Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Assess childrenn++s developmental needs on an ongoing basis'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Assess children''s developmental needs on an ongoing basis.'
		WHERE PrimaryValue = 'Assess childrenn++s developmental needs on an ongoing basis'
	END
	ELSE
		PRINT 'Assess childrenn++s developmental needs on an ongoing basis Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Match performern++s attributes to specific roles'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Match performer''s attributes to specific roles.'
		WHERE PrimaryValue = 'Match performern++s attributes to specific roles'
	END
	ELSE
		PRINT 'Match performern++s attributes to specific roles Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Monitor animalsn++ clinical symptoms'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Monitor animal''s clinical symptoms.'
		WHERE PrimaryValue = 'Monitor animalsn++ clinical symptoms'
	END
	ELSE
		PRINT 'Monitor animalsn++ clinical symptoms Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Clean animalsn++ teeth'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Clean animal''s teeth.'
		WHERE PrimaryValue = 'Clean animalsn++ teeth'
	END
	ELSE
		PRINT 'Clean animalsn++ teeth Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography?mass spectrometry LC/MSMSMS to confirm detected drugs.'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography-mass spectrometry LC/MSMSMS to confirm detected drugs.'
		WHERE PrimaryValue = 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography?mass spectrometry LC/MSMSMS to confirm detected drugs.'
	END
	ELSE
		PRINT 'Use gas chromatography-mass spectroscopy (GC-MS) and liquid chromatography?mass spectrometry LC/MSMSMS to confirm detected drugs. counter Not found'

	IF (
			EXISTS (
				SELECT *
				FROM dbo.[Library.JobTask] AS j, dbo.[Library.JobTaskLocalisationItem] AS jt
				WHERE j.[Key] = jt.[Key]
					AND jt.PrimaryValue = 'Print tickets to be picked up at n++will call" counter'
				)
			)
	BEGIN
		UPDATE dbo.[Library.JobTaskLocalisationItem]
		SET PrimaryValue = 'Print tickets to be picked up at "will call" counter.'
		WHERE PrimaryValue = 'Print tickets to be picked up at n++will call" counter'
	END
	ELSE
		PRINT 'Print tickets to be picked up at n++will call" counter Not found'

	COMMIT
END TRY

BEGIN CATCH
	ROLLBACK

	DECLARE @ErrorMessage NVARCHAR(max)
	DECLARE @ErrorSeverity NVARCHAR(max)
	DECLARE @ErrorState NVARCHAR(50)

	PRINT 'catch'

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
END CATCH
