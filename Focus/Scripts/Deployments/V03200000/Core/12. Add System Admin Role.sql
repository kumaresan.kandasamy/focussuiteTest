﻿/****** Add system admin role to role table ******/
DECLARE @NextID INT

BEGIN
	SELECT @NextID = NextId FROM [dbo].[KeyTable]
	
	INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((@NextID + 1), 'SystemAdmin', 'System Admin')
	
	
	UPDATE [KeyTable] SET NextId = NextId + 2
END