﻿ IF NOT EXISTS
(
	SELECT * FROM sys.columns
	INNER JOIN sys.tables on sys.columns.object_id = sys.tables.object_id
    WHERE sys.columns.[name] = 'NoOfEmployees'
	AND sys.tables.[name] = 'Report.Employer'
)
/***** Add account type columns *****/
ALTER TABLE dbo.[Report.Employer] 
ADD NoOfEmployees bigint null
GO

/****** Object:  View [dbo].[Data.Application.EmployerReportView]    Script Date: 10/25/2014 09:57:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.EmployerReportView]'))
DROP VIEW [dbo].[Data.Application.EmployerReportView]
GO

/****** Object:  View [dbo].[Data.Application.EmployerReportView]    Script Date: 10/25/2014 09:57:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.EmployerReportView]

AS
	SELECT 
		BU.Id,
		BU.[Name],
		BUA.CountyId,
		BUA.StateId,
		BUA.PostcodeZip,
		E.FederalEmployerIdentificationNumber,
		E.Id AS EmployerId,
		E.AccountTypeId,
		BU.NoOfEmployees
	FROM
		[Data.Application.BusinessUnit] BU
	INNER JOIN [Data.Application.Employer] E
		ON E.Id = BU.EmployerId
	LEFT OUTER JOIN [Data.Application.BusinessUnitAddress] BUA
		ON BUA.BusinessUnitId = BU.Id
		AND BUA.IsPrimary = 1

GO

/****** Object:  View [dbo].[Report.EmployerView]    Script Date: 10/25/2014 13:45:47 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerView]'))
DROP VIEW [dbo].[Report.EmployerView]
GO

/****** Object:  View [dbo].[Report.EmployerView]    Script Date: 10/25/2014 13:45:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Report.EmployerView]
AS
	SELECT
		E.Id,
		E.FocusBusinessUnitId,
		E.FocusEmployerId,
		E.[Name],
		E.CountyId,
		E.County,
		E.StateId,
		E.[State],
		E.PostalCode,
		E.Office,
		E.LatitudeRadians,
		E.LongitudeRadians,
		E.FederalEmployerIdentificationNumber,
		J.JobTitle,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ForeignLabourType,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		J.MinSalary,
		J.MaxSalary,
		J.SalaryFrequency,
		J.JobStatus,
		J.MinimumEducationLevel,
		J.[Description] AS JobDescription,
		E.AccountTypeId,
		E.NoOfEmployees
	FROM 
		[Report.Employer] E
	LEFT OUTER JOIN [Report.JobOrder] J
		ON J.EmployerId = E.Id 

GO