﻿IF NOT EXISTS
(
	SELECT * FROM sys.columns
	INNER JOIN sys.tables on sys.columns.object_id = sys.tables.object_id
    WHERE sys.columns.[name] = 'Archived'
	AND sys.tables.[name] = 'Data.Application.Employer'
)
BEGIN
    ALTER TABLE [dbo].[Data.Application.Employer]
	ADD [Archived] [bit] NOT NULL DEFAULT 0
END
