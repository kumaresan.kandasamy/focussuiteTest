﻿/****** Object:  View [dbo].[Data.Application.EmployerReportView]    Script Date: 10/30/2014 15:56:56 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.EmployerReportView]'))
DROP VIEW [dbo].[Data.Application.EmployerReportView]
GO

/****** Object:  View [dbo].[Data.Application.EmployerReportView]    Script Date: 10/30/2014 15:56:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Data.Application.EmployerReportView]

AS
	SELECT 
		BU.Id,
		BU.[Name],
		BUA.CountyId,
		BUA.StateId,
		BUA.PostcodeZip,
		E.FederalEmployerIdentificationNumber,
		E.Id AS EmployerId,
		BU.AccountTypeId,
		BU.NoOfEmployees
	FROM
		[Data.Application.BusinessUnit] BU
	INNER JOIN [Data.Application.Employer] E
		ON E.Id = BU.EmployerId
	LEFT OUTER JOIN [Data.Application.BusinessUnitAddress] BUA
		ON BUA.BusinessUnitId = BU.Id
		AND BUA.IsPrimary = 1


GO

