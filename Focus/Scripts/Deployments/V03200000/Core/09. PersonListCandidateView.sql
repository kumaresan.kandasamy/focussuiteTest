
/****** Object:  View [dbo].[Data.Application.PersonListCandidateView]    Script Date: 10/30/2014 14:48:09 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.PersonListCandidateView]'))
DROP VIEW [dbo].[Data.Application.PersonListCandidateView]
GO

/****** Object:  View [dbo].[Data.Application.PersonListCandidateView]    Script Date: 10/30/2014 14:48:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.PersonListCandidateView]  
AS  
  
SELECT   
 PersonListPerson.Id AS Id,  
 PersonList.ListType AS ListType,  
 PersonList.PersonId AS PersonId,  
 [Resume].Id AS CandidateId,  
 [Resume].FirstName AS CandidateFirstName,  
 [Resume].LastName AS CandidateLastName,  
 ISNULL([Resume].IsVeteran, 'false') AS CandidateIsVeteran ,
 PersonListPerson.PersonId As CandidatePersonId,
 [Resume].TownCity As CandidateTownCity,
 [Resume].StateId As CandidateStateId,
 [Resume].IsContactInfoVisible As CandidateIsContactInfoVisible
FROM   
 [Data.Application.PersonListPerson] AS PersonListPerson WITH (NOLOCK)  
 INNER JOIN [Data.Application.PersonList] AS PersonList WITH (NOLOCK) ON PersonListPerson.PersonListId = PersonList.Id  
INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON PersonListPerson.PersonId = [Resume].PersonId AND [Resume].IsDefault = 1 



GO


