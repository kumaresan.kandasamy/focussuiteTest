
/****** Object:  View [dbo].[Data.Application.ApplicationView]    Script Date: 10/31/2014 11:22:48 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.ApplicationView]'))
DROP VIEW [dbo].[Data.Application.ApplicationView]
GO

/****** Object:  View [dbo].[Data.Application.ApplicationView]    Script Date: 10/31/2014 11:22:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.ApplicationView]
AS

SELECT
	[Application].Id AS Id,
	Job.Id AS JobId,
	Job.EmployerId AS EmployerId,
	Job.EmployeeId AS EmployeeId,
	Job.BusinessUnitId AS BusinessUnitId,
	[Resume].PersonId AS CandidateId,
	[Resume].FirstName AS CandidateFirstName,
	[Resume].LastName AS CandidateLastName,
	[Resume].YearsExperience AS CandidateYearsExperience,
	[Resume].IsVeteran AS CandidateIsVeteran,
	[Application].ApplicationStatus AS CandidateApplicationStatus,
	[Application].ApplicationScore AS CandidateApplicationScore,
	[Application].CreatedOn AS CandidateApplicationReceivedOn,
	[Application].ApprovalStatus AS CandidateApplicationApprovalStatus,
	[Application].StatusLastChangedOn AS ApplicationStatusLastChangedOn,
	[Application].Viewed AS Viewed,
	[Application].PostHireFollowUpStatus AS PostHireFollowUpStatus,
	[BusinessUnit].Name AS BusinuessUnitName,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	Person.EmailAddress AS EmployeeEmail,
	Job.ClosingOn AS JobClosingOn,
	Job.JobTitle,
	Job.VeteranPriorityEndDate,
	[Posting].LensPostingId,
	[PhoneNumber].Number AS EmployeePhoneNumber,
	[Application].AutomaticallyApproved AS CandidateApplicationAutomaticallyApproved,
	[Resume].IsContactInfoVisible AS CandidateIsContactInfoVisible
FROM [Data.Application.Application] AS [Application] WITH (NOLOCK)
	INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN [Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id
	INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	INNER JOIN [Data.Application.BusinessUnit] AS [BusinessUnit] WITH (NOLOCK) ON Job.BusinessUnitId = [BusinessUnit].Id
	INNER JOIN [Data.Application.Employee] AS [Employee] WITH (NOLOCK) ON Job.EmployeeId = [Employee].Id
	INNER JOIN [Data.Application.Person] AS [Person] WITH (NOLOCK) ON Employee.PersonId = Person.Id
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS [PhoneNumber] WITH (NOLOCK) ON [Person].Id = [PhoneNumber].PersonId AND [PhoneNumber].IsPrimary = 1


GO


