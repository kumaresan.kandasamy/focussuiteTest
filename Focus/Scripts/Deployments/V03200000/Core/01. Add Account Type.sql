﻿  /***** Add account type columns *****/
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Employer' AND COLUMN_NAME = 'AccountTypeId')
	ALTER TABLE [Data.Application.Employer] 
  ADD AccountTypeId bigint null
  GO


IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnit' AND COLUMN_NAME = 'AccountTypeId')
  ALTER TABLE [Data.Application.BusinessUnit]
  ADD AccountTypeId bigint null
  GO


	
/****** Add account type column to the view ******/
/****** Object:  View [dbo].[Data.Application.EmployerAccountReferralView]    Script Date: 10/15/2014 14:46:48 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.EmployerAccountReferralView]'))
DROP VIEW [dbo].[Data.Application.EmployerAccountReferralView]
GO

/****** Object:  View [dbo].[Data.Application.EmployerAccountReferralView]    Script Date: 10/15/2014 14:46:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.EmployerAccountReferralView]
AS
SELECT 
	Employee.Id,
	Employer.Id AS EmployerId,
	Employer.Name AS EmployerName,
	Employee.Id AS EmployeeId,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	[User].CreatedOn AS AccountCreationDate,
	[dbo].[Data.Application.GetBusinessDays]([User].CreatedOn, GETDATE()) AS TimeInQueue,
	BusinessUnitAddress.Line1 AS EmployerAddressLine1,
	BusinessUnitAddress.Line2 AS EmployerAddressLine2,
	BusinessUnitAddress.TownCity AS EmployerAddressTownCity,
	BusinessUnitAddress.StateId AS EmployerAddressStateId,
	BusinessUnitAddress.PostcodeZip AS EmployerAddressPostcodeZip,
	BusinessUnitAddress.PublicTransitAccessible AS EmployerPublicTransitAccessible,
	BusinessUnit.PrimaryPhone,
	BusinessUnit.PrimaryPhoneType,
	BusinessUnit.AlternatePhone1,
	BusinessUnit.AlternatePhone1Type,
	BusinessUnit.AlternatePhone2,
	BusinessUnit.AlternatePhone2Type,
	CASE 
		WHEN BusinessUnit.PrimaryPhoneType = 'Phone' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.PrimaryPhoneType = 'Mobile' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.AlternatePhone1Type = 'Phone' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone1Type = 'Mobile' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone2Type = 'Phone' THEN BusinessUnit.AlternatePhone2
		WHEN BusinessUnit.AlternatePhone2Type = 'Mobile' THEN BusinessUnit.AlternatePhone2
	END AS EmployerPhoneNumber,
	CASE 
		WHEN BusinessUnit.PrimaryPhoneType = 'Fax' THEN BusinessUnit.PrimaryPhone
		WHEN BusinessUnit.AlternatePhone1Type = 'Fax' THEN BusinessUnit.AlternatePhone1
		WHEN BusinessUnit.AlternatePhone2Type = 'Fax' THEN BusinessUnit.AlternatePhone2
	END AS EmployerFaxNumber,
	PhoneNumber.Number AS EmployeePhoneNumber,
	FaxNumber.Number AS EmployeeFaxNumber,
	BusinessUnit.Url AS EmployerUrl,
	Employer.FederalEmployerIdentificationNumber AS EmployerFederalEmployerIdentificationNumber,
	Employer.StateEmployerIdentificationNumber AS EmployerStateEmployerIdentificationNumber,
	BusinessUnit.IndustrialClassification AS EmployerIndustrialClassification,
	PersonAddress.Line1 AS EmployeeAddressLine1,
	PersonAddress.Line2 AS EmployeeAddressLine2,
	PersonAddress.TownCity AS EmployeeAddressTownCity,
	PersonAddress.StateId AS EmployeeAddressStateId,
	PersonAddress.PostcodeZip AS EmployeeAddressPostcodeZip,
	Person.EmailAddress AS EmployeeEmailAddress,
	Employee.ApprovalStatus AS EmployeeApprovalStatus,
	Employer.ApprovalStatus AS EmployerApprovalStatus,
	BusinessUnit.OwnershipTypeId AS EmployerOwnershipTypeId,
	Employer.AssignedToId AS EmployerAssignedToId,
	Employee.RedProfanityWords AS RedProfanityWords,
	Employee.YellowProfanityWords AS YellowProfanityWords,
	ISNULL(BusinessUnitDescription.Description, '') AS BusinessUnitDescription,
	BusinessUnit.Name AS BusinessUnitName,
	BusinessUnit.Id AS BusinessUnitId,
	Employee.ApprovalStatusChangedBy,
	Employee.ApprovalStatusChangedOn,
	BusinessUnit.AccountTypeId AS EmployerAccountTypeId
FROM 
	[Data.Application.Employer] AS Employer WITH (NOLOCK)
	INNER JOIN [Data.Application.Employee] AS Employee  WITH (NOLOCK) ON Employer.Id = Employee.EmployerId
	INNER JOIN [Data.Application.Person] AS Person WITH (NOLOCK) ON Employee.PersonId = Person.Id
	INNER JOIN [Data.Application.User] AS [User]  WITH (NOLOCK) ON Person.Id = [User].PersonId
	INNER JOIN dbo.[Data.Application.EmployeeBusinessUnit] AS [EmployeeBusinessUnit] WITH (NOLOCK) ON Employee.Id = EmployeeBusinessUnit.EmployeeId AND EmployeeBusinessUnit.[Default] = 1
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.BusinessUnitId = BusinessUnit.Id
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	LEFT OUTER JOIN dbo.[Data.Application.BusinessUnitDescription] AS BusinessUnitDescription WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitDescription.BusinessUnitId AND BusinessUnitDescription.IsPrimary = 1
	LEFT OUTER JOIN [Data.Application.PersonAddress] AS PersonAddress WITH (NOLOCK) ON Person.Id = PersonAddress.PersonId 
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND (PhoneNumber.PhoneType = 0 OR PhoneNumber.PhoneType = 1) AND PhoneNumber.IsPrimary = 1
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS FaxNumber WITH (NOLOCK) ON Person.Id = FaxNumber.PersonId AND FaxNumber.PhoneType = 4
WHERE
	Employer.ApprovalStatus = 1 OR Employee.ApprovalStatus = 1 OR Employee.ApprovalStatus = 4 OR Employer.ApprovalStatus = 4 OR Employee.ApprovalStatus = 3 OR Employer.ApprovalStatus = 3



GO

/****** Add the configuration data to support configurable account types ******/
IF NOT EXISTS(SELECT 1 FROM [Config.CodeGroup] WHERE [Key] = 'AccountTypes')
BEGIN
	INSERT INTO [dbo].[Config.CodeGroup]
	VALUES ('AccountTypes')
	DECLARE @AccountTypesId BIGINT;
	SELECT @AccountTypesId = SCOPE_IDENTITY();
END
ELSE
	BEGIN
		SELECT @AccountTypesId = Id FROM [Config.CodeGroup] WHERE [Key] = 'AccountTypes'
	END
 
IF NOT EXISTS(SELECT 1 FROM [config.codeitem] WHERE [Key] = 'AccountTypes.DirectEmployer')
BEGIN
	INSERT INTO [dbo].[config.codeitem]
	VALUES ('AccountTypes.DirectEmployer', 0, null, null, null)
	DECLARE @DirectEmployerId BIGINT;
	SELECT @DirectEmployerId = SCOPE_IDENTITY();
END
ELSE
	BEGIN
		SELECT @DirectEmployerId = Id FROM [config.codeitem] WHERE [Key] = 'AccountTypes.DirectEmployer'
	END
  
IF NOT EXISTS(SELECT 1 FROM [config.codeitem] WHERE [Key] = 'AccountTypes.StaffingAgency')
BEGIN
	INSERT INTO [dbo].[config.codeitem]
	VALUES ('AccountTypes.StaffingAgency', 0, null, null, null)
	DECLARE @StaffingAgencyId BIGINT;
	SELECT @StaffingAgencyId = SCOPE_IDENTITY();
END
ELSE
	BEGIN
		SELECT @StaffingAgencyId = Id FROM [config.codeitem] WHERE [Key] = 'AccountTypes.StaffingAgency'
	END
 
IF NOT EXISTS(SELECT 1 FROM [config.codegroupitem] WHERE CodeGroupId = @AccountTypesId)
BEGIN
	INSERT INTO [dbo].[config.codegroupitem]
	VALUES ('1', @AccountTypesId, @DirectEmployerId)
  
	INSERT INTO [dbo].[config.codegroupitem]
	VALUES ('2', @AccountTypesId, @StaffingAgencyId)
END

DECLARE @CultureId bigint;
SELECT @CultureId = Id from dbo.[Config.Localisation] WHERE Culture = '**-**'

IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'AccountTypes.DirectEmployer')
BEGIN
	INSERT INTO [dbo].[Config.LocalisationItem]
	([Key], Value, Localised, LocalisationId)
	VALUES ('AccountTypes.DirectEmployer', 'Direct Employer', 0, @CultureId)
END
  
IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'AccountTypes.StaffingAgency')
BEGIN
	INSERT INTO [dbo].[Config.LocalisationItem]
	([Key], Value, Localised, LocalisationId)
	VALUES ('AccountTypes.StaffingAgency', 'Staffing Agency', 0, @CultureId)
END

    