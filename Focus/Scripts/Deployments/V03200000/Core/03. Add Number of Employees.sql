﻿  IF NOT EXISTS
(
	SELECT * FROM sys.columns
	INNER JOIN sys.tables on sys.columns.object_id = sys.tables.object_id
    WHERE sys.columns.[name] = 'NoOfEmployees'
	AND sys.tables.[name] = 'Data.Application.Employer'
)
BEGIN
  	ALTER TABLE [Data.Application.Employer] 
   ADD NoOfEmployees bigint null
END

GO

  IF NOT EXISTS
(
	SELECT * FROM sys.columns
	INNER JOIN sys.tables on sys.columns.object_id = sys.tables.object_id
    WHERE sys.columns.[name] = 'NoOfEmployees'
	AND sys.tables.[name] = 'Data.Application.BusinessUnit'
)
BEGIN
    
  ALTER TABLE [Data.Application.BusinessUnit]
  ADD NoOfEmployees bigint null
END

	
