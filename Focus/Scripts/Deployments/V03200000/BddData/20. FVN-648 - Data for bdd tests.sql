﻿/****** Object:  StoredProcedure [dbo].[GetNextIdAndIncrementKeyTableByOne]    Script Date: 10/30/2014 08:38:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetNextIdAndIncrementKeyTableByOne]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetNextIdAndIncrementKeyTableByOne]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Baydr Earles
-- Create date: 30/10/2014
-- Description:	Returns the next id from the key table and increments the next id in the table by one
-- =============================================
CREATE PROCEDURE GetNextIdAndIncrementKeyTableByOne
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @NextId BIGINT;
	
	SELECT @NextId = NextId FROM dbo.KeyTable
	
	UPDATE	dbo.KeyTable
	SET		NextId = NextId + 1;
	
	RETURN @NextId
END
GO

DECLARE @ResumeId int;
DECLARE @PostingId int;
DECLARE @PersonId int;
DECLARE @ApplicationId int;
DECLARE @JobId int;
DECLARE @EmployerId int;
DECLARE @EmployeeId int;
DECLARE @BusinessUnitDescriptionId int;
DECLARE @BusinessUnit int;
DECLARE @BusinessUnitAddressId int;
DECLARE @ReferralEmailId int;
DECLARE @UserId int;
DECLARE @PersonId2 int;

exec @ResumeId = GetNextIdAndIncrementKeyTableByOne
exec @PostingId = GetNextIdAndIncrementKeyTableByOne
exec @PersonId = GetNextIdAndIncrementKeyTableByOne
exec @ApplicationId = GetNextIdAndIncrementKeyTableByOne
exec @JobId = GetNextIdAndIncrementKeyTableByOne
exec @EmployerId = GetNextIdAndIncrementKeyTableByOne
exec @BusinessUnitDescriptionId = GetNextIdAndIncrementKeyTableByOne
exec @BusinessUnit = GetNextIdAndIncrementKeyTableByOne
exec @BusinessUnitAddressId = GetNextIdAndIncrementKeyTableByOne
exec @EmployeeId = GetNextIdAndIncrementKeyTableByOne
exec @ReferralEmailId = GetNextIdAndIncrementKeyTableByOne
exec @UserId = GetNextIdAndIncrementKeyTableByOne
exec @PersonId2 = GetNextIdAndIncrementKeyTableByOne



INSERT [dbo].[Data.Application.Employer] ([Id], [Name], [CommencedOn], [FederalEmployerIdentificationNumber], [IsValidFederalEmployerIdentificationNumber], [StateEmployerIdentificationNumber], [ApprovalStatus], [Url], [ExpiredOn], [OwnershipTypeId], [IndustrialClassification], [TermsAccepted], [PrimaryPhone], [PrimaryPhoneExtension], [PrimaryPhoneType], [AlternatePhone1], [AlternatePhone1Type], [AlternatePhone2], [AlternatePhone2Type], [IsRegistrationComplete], [CreatedOn], [UpdatedOn], [AssignedToId], [ExternalID], [AccountTypeId], [NoOfEmployees], [Archived]) VALUES (@EmployerId, N'Employer FVN648', CAST(0x0000A2A600000000 AS DateTime), N'55-6677889', 1, N'', 2, N'', NULL, 4755002, N'11194 - Hay Farming', 1, N'1111111111', N'', N'Phone', N'', N'Phone', N'', N'Phone', 1, CAST(0x0000A3D400E55914 AS DateTime), CAST(0x0000A3D400E55914 AS DateTime), NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[Data.Application.BusinessUnit] ([Id], [Name], [IsPrimary], [Url], [OwnershipTypeId], [IndustrialClassification], [PrimaryPhone], [PrimaryPhoneExtension], [PrimaryPhoneType], [AlternatePhone1], [AlternatePhone1Type], [AlternatePhone2], [AlternatePhone2Type], [EmployerId], [IsPreferred], [AccountTypeId], [NoOfEmployees]) VALUES (@BusinessUnit, N'Employer FVN648', 0, NULL, 4755002, N'11194 - Hay Farming', N'1111111111', N'', N'Phone', N'', N'Phone', N'', N'Phone', @EmployerId, 1, NULL, NULL)
INSERT [dbo].[Data.Application.BusinessUnitAddress] ([Id], [Line1], [Line2], [Line3], [TownCity], [CountyId], [PostcodeZip], [StateId], [CountryId], [IsPrimary], [PublicTransitAccessible], [BusinessUnitId]) VALUES (@BusinessUnitAddressId, N'Address Line 1', N'', N'', N'Bluegrove', 1586411, N'76352', 12896, 13370, 1, 1, @BusinessUnit)
INSERT [dbo].[Data.Application.BusinessUnitDescription] ([Id], [Title], [Description], [IsPrimary], [BusinessUnitId]) VALUES (@BusinessUnitDescriptionId, N'Default description', N'Employer FVN675 Multiple BU Non Primary 2', 1, @BusinessUnit)

INSERT [dbo].[Data.Application.Person] (
[Id], 
[TitleId], 
[FirstName], 
[MiddleInitial], 
[LastName], 
[DateOfBirth], 
[SocialSecurityNumber], 
[JobTitle], 
[EmailAddress], 
[EnrollmentStatus], 
[ProgramAreaId], 
[DegreeId], 
[Manager], 
[ExternalOffice], 
[AssignedToId], 
[LastSurveyedOn], 
[CampusId], 
[IsVeteran], 
[IsUiClaimant]) 
VALUES (
@PersonId2,
(select id from [dbo].[Config.LocalisationItem] where [key] = 'Title.Mr'), 
N'FVN648User', 
N'', 
N'Explorer1', 
NULL, 
NULL, 
NULL, 
N'FVN648@explorer.com', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL)

INSERT [dbo].[Data.Application.Person] (
[Id], 
[TitleId], 
[FirstName], 
[MiddleInitial], 
[LastName], 
[DateOfBirth], 
[SocialSecurityNumber], 
[JobTitle], 
[EmailAddress], 
[EnrollmentStatus], 
[ProgramAreaId], 
[DegreeId], 
[Manager], 
[ExternalOffice], 
[AssignedToId], 
[LastSurveyedOn], 
[CampusId], 
[IsVeteran], 
[IsUiClaimant]) 
VALUES (
@PersonId,
(select id from [dbo].[Config.LocalisationItem] where [key] = 'Title.Mr'), 
N'FVN648', 
N'', 
N'Explorer1', 
NULL, 
NULL, 
NULL, 
N'FVN648@explorer.com', 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL, 
NULL)

INSERT [dbo].[Data.Application.Employee] ([Id], [ApprovalStatus], [StaffUserId], [EmployerId], [PersonId], [RedProfanityWords], [YellowProfanityWords], [ApprovalStatusChangedBy], [ApprovalStatusChangedOn]) VALUES (@EmployeeId, 5, NULL, @EmployerId, @PersonId, NULL, NULL, NULL, NULL)


INSERT [dbo].[Data.Application.Job] (
[Id], [JobTitle], [CreatedBy], [UpdatedBy], [ApprovalStatus], [JobStatus], [MinSalary], [MaxSalary], [SalaryFrequencyId], [HoursPerWeek], [OverTimeRequired], [ClosingOn], [NumberOfOpenings], [PostedOn], [PostedBy], [Description], --16
[PostingHtml], [EmployerDescriptionPostingPosition], [WizardStep], [WizardPath], [HeldOn], [HeldBy], [ClosedOn], [ClosedBy], [JobLocationType], [FederalContractor], [ForeignLabourCertification], --11
[CourtOrderedAffirmativeAction], [FederalContractorExpiresOn], [WorkOpportunitiesTaxCreditHires], [PostingFlags], [IsCommissionBased], [NormalWorkDays], [WorkDaysVary], [NormalWorkShiftsId], [LeaveBenefits], --9
[RetirementBenefits], [InsuranceBenefits], [MiscellaneousBenefits], [OtherBenefitsDetails], [InterviewContactPreferences], [InterviewEmailAddress], [InterviewApplicationUrl], [InterviewMailAddress], [InterviewFaxNumber], --9
[InterviewPhoneNumber], [InterviewDirectApplicationDetails], [InterviewOtherInstructions], [ScreeningPreferences], [MinimumEducationLevel], [MinimumEducationLevelRequired], [MinimumAge], [MinimumAgeReason], --8
[MinimumAgeRequired], [LicencesRequired], [CertificationRequired], [LanguagesRequired], [Tasks], [RedProfanityWords], [YellowProfanityWords], [EmploymentStatusId], [JobTypeId], [JobStatusId], [ApprovedOn], [ApprovedBy], --12
[ExternalId], [AwaitingApprovalOn], [MinimumExperience], [MinimumExperienceMonths], [MinimumExperienceRequired], [DrivingLicenceClassId], [DrivingLicenceRequired], [HideSalaryOnPosting], [ForeignLabourCertificationH2A], --9
[ForeignLabourCertificationH2B], [ForeignLabourCertificationOther], [HiringFromTaxCreditProgramNotificationSent], [IsConfidential], [HasCheckedCriminalRecordExclusion], [LastPostedOn], [LastRefreshedOn], ---7
[MinimumAgeReasonValue], [OtherSalary], [HideOpeningsOnPosting], [StudentEnrolled], [MinimumCollegeYears], [CreatedOn], [UpdatedOn], [LockVersion], [BusinessUnitId], [EmployeeId], [EmployerId], [BusinessUnitDescriptionId], --12
[BusinessUnitLogoId], [ProgramsOfStudyRequired], [StartDate], [EndDate], [JobType], [OnetId], [HideEducationOnPosting], [HideExperienceOnPosting], [HideMinimumAgeOnPosting], [HideProgramOfStudyOnPosting], --10
[HideDriversLicenceOnPosting], [HideLicencesOnPosting], [HideCertificationsOnPosting], [HideLanguagesOnPosting], [HideSpecialRequirementsOnPosting], [HideWorkWeekOnPosting], [DescriptionPath], [WorkWeekId], [AssignedToId], --9
[CriminalBackgroundExclusionRequired], [ROnetId], [IsSalaryAndCommissionBased], [VeteranPriorityEndDate], [MeetsMinimumWageRequirement]) --5
VALUES (@JobId, N'FVN648 Job Title', 4841095, 4841095, 2, 1, 
NULL, NULL, 0, NULL, 0, CAST(0x0000A4DD00000000 AS DateTime), 2, CAST(0x0000A3D200C401F0 AS DateTime), 4841095, N'Test Job Description', N'<html><body><p><b>Job with all applicants to be screened</b><br>Employer FVN648<br>Bluegrove, TX (76352)(public transit accessible)<br>Number of openings: 2<br>Application closing date: 22/07/2015<br></p><p>Test Job Description</p><p><b>Benefits</b><br>No benefits are offered with this job</p><p><b>About Employer FVN663</b><br>Company Description</p><p><b>How to apply</b><br><a href="http://localhost:57990" target="_blank">Log in to Focus/Career and submit your resume</a></p><p><small>REF/4760944/4761031</small></p></body>#POSTINGFOOTER#</html>', 
0, 7, 1, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 64, N'', N'', N'', N'', N'', N'', N'', 1, 0, 0, NULL, N'', 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 
0, NULL, 0, 0, 0, 0, 0, NULL, 0, 0, CAST(0x0000A3D200C401F0 AS DateTime), NULL, 0, NULL, NULL, 0, NULL, CAST(0x0000A3D200C401F0 AS DateTime), CAST(0x0000A3D200C401F0 AS DateTime), 10, @BusinessUnit, @EmployeeId, @EmployerId, @BusinessUnitDescriptionId, 
NULL, NULL, NULL, NULL, 1, 112800, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, 0, NULL, 0, NULL, 1)



INSERT [dbo].[Data.Application.Resume] (
[Id], 
[PrimaryOnet], 
[PrimaryROnet], 
[ResumeSkills], 
[FirstName], 
[LastName], 
[MiddleName], 
[DateOfBirth], 
[Gender], 
[AddressLine1], 
[TownCity], 
[StateId], 
[CountryId], 
[PostcodeZip], 
[WorkPhone], 
[HomePhone], 
[MobilePhone], 
[FaxNumber], 
[EmailAddress], 
[HighestEducationLevel], 
[YearsExperience], 
[IsVeteran], 
[IsActive], 
[StatusId], 
[ResumeXml], 
[ResumeCompletionStatusId], 
[IsDefault], 
[ResumeName], 
[IsSearchable], 
[CreatedOn], 
[UpdatedOn], 
[PersonId], 
[SkillsAlreadyCaptured], 
[CountyId], 
[SkillCount], 
[WordCount], 
[VeteranPriorityServiceAlertsSubscription], 
[EducationCompletionDate], 
[ResetSearchable], 
[IsContactInfoVisible]) 
VALUES (
@ResumeId, 
N'', 
N'', 
N'', 
N'FVN648', 
N'Explorer1', 
NULL, 
N'01 Jan 1980 00:00:00.000', 
2, 
N'Address Line 1', 
N'Bluegrove', 
(select id from [dbo].[Config.LocalisationItem] where [key] = 'State.KY'), 
(select id from [dbo].[Config.LocalisationItem] where [key] = 'Country.US'), 
N'76352', 
NULL, 
N'1111111111', 
NULL, 
NULL, 
N'FVN648@explorer.com', 
20, 
0, 
0, 
1, 
1, 
N'<ResDoc><special><preserved_format>Standard</preserved_format><preserved_order>Emphasize my experience</preserved_order></special><resume><contact><name><givenname>FVN663</givenname><surname>Explorer1</surname></name> Home:<phone type="home">1111111111</phone><email>fvn648@reconsider.com</email><website></website><address><street>Address Line 1</street><street></street><city>Bluegrove</city><state_county>48077</state_county><county_fullname>Clay</county_fullname><state>TX</state><state_fullname>Texas</state_fullname><postalcode>76352</postalcode><country>US</country><country_fullname>US</country_fullname></address></contact><experience><employment_status_cd>1</employment_status_cd></experience><education><school_status_cd>5</school_status_cd><norm_edu_level_cd>20</norm_edu_level_cd></education><header><default>1</default><buildmethod>1</buildmethod><resumeid>4760449</resumeid><status>1</status><completionstatus>127</completionstatus><updatedon>23/07/2014 08:44:57</updatedon></header><statements><personal><sex>2</sex><ethnic_heritages><ethnic_heritage><ethnic_id>3</ethnic_id><selection_flag>-1</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>4</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>5</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>2</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>6</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>1</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>-1</ethnic_id><selection_flag>-1</selection_flag></ethnic_heritage></ethnic_heritages><citizen_flag>-1</citizen_flag><disability_status>3</disability_status><license><driver_flag>0</driver_flag></license><veteran><vet_flag>0</vet_flag><subscribe_vps>0</subscribe_vps></veteran><preferences><sal>10</sal><salary_unit_cd>1</salary_unit_cd><resume_searchable>0</resume_searchable><postings_interested_in>0</postings_interested_in><relocate>-1</relocate><shift><shift_first_flag>-1</shift_first_flag><shift_second_flag>-1</shift_second_flag><shift_third_flag>-1</shift_third_flag><shift_rotating_flag>-1</shift_rotating_flag><shift_split_flag>-1</shift_split_flag><shift_varies_flag>-1</shift_varies_flag><work_type>1</work_type><work_over_time>-1</work_over_time></shift></preferences></personal></statements><summary><summary include="1">Summary</summary></summary><skills><internship_skills /></skills><skillrollup /><hide_options><hide_daterange>0</hide_daterange><hide_eduDates>0</hide_eduDates><hide_militaryserviceDates>0</hide_militaryserviceDates><hide_workDates>0</hide_workDates><hide_contact>0</hide_contact><hide_name>0</hide_name><hide_email>0</hide_email><hide_homePhoneNumber>0</hide_homePhoneNumber><hide_workPhoneNumber>0</hide_workPhoneNumber><hide_cellPhoneNumber>0</hide_cellPhoneNumber><hide_faxPhoneNumber>0</hide_faxPhoneNumber><hide_nonUSPhoneNumber>0</hide_nonUSPhoneNumber><hide_affiliations>0</hide_affiliations><hide_certifications_professionallicenses>0</hide_certifications_professionallicenses><hide_employer>0</hide_employer><hide_driver_license>0</hide_driver_license><unhide_driver_license>0</unhide_driver_license><hide_honors>0</hide_honors><hide_interests>0</hide_interests><hide_internships>0</hide_internships><hide_languages>0</hide_languages><hide_veteran>0</hide_veteran><hide_objective>0</hide_objective><hide_personalinformation>0</hide_personalinformation><hide_professionaldevelopment>0</hide_professionaldevelopment><hide_publications>0</hide_publications><hide_references>0</hide_references><hide_skills>0</hide_skills><hide_technicalskills>0</hide_technicalskills><hide_volunteeractivities>0</hide_volunteeractivities><hide_thesismajorprojects>0</hide_thesismajorprojects><hidden_skills></hidden_skills></hide_options></resume></ResDoc>', 
127, 
1, 
N'FVN648 Resume Name', 
0, 
CAST(0x0000A3D200C40200 AS DateTime), 
CAST(0x0000A3D200C40200 AS DateTime), 
@PersonId, 
NULL, 
(select top 1 id from [dbo].[Config.LocalisationItem] where [key] like 'County.%'),
0, 
49, 
0, 
NULL, 
NULL, 
0)

INSERT [dbo].[Data.Application.Posting] ([Id], [LensPostingId], [JobTitle], [PostingXml], [EmployerName], [Url], [ExternalId], [StatusId], [OriginId], [Html], [ViewedCount], [CreatedOn], [UpdatedOn], [JobId]) 
VALUES (@PostingId, N'ABCDEF_EA863D853D044E7BB682D0439E77D25B', N'Job with all applicants to be screened', 
N'<jobposting><JobDoc><posting><duties>Test Job Description<title onet="11-3042.00">Job with all applicants to be screened</title><onettitle>Training and Development Managers</onettitle></duties><contact><address><city>Bluegrove</city><majorcity>Bluegrove</majorcity><state>TX</state><postalcode>76352</postalcode></address><company>Employer FVN663</company><person>Dev Employer</person></contact><jobinfo><positioncount>2</positioncount><EDUCATION_CD>0</EDUCATION_CD><JOB_TYPE>1</JOB_TYPE></jobinfo></posting><special><mode>insert</mode><originid>7</originid><jobtitle>Job with all applicants to be screened</jobtitle><jobemployer>Employer FVN663</jobemployer><job_status_cd>1</job_status_cd><jobtype>4</jobtype><JOBTYPE_CD>4</JOBTYPE_CD></special></JobDoc><postinghtml>&lt;html&gt;    &lt;body&gt;      &lt;p&gt;&lt;b&gt;Job with all applicants to be screened&lt;/b&gt;&lt;br&gt;Employer FVN663&lt;br&gt;Bluegrove, TX (76352)(public transit accessible)&lt;br&gt;Number of openings: 2&lt;br&gt;Application closing date: 22/07/2015&lt;br&gt;&lt;/p&gt;      &lt;p&gt;Test Job Description&lt;/p&gt;      &lt;p&gt;&lt;b&gt;Benefits&lt;/b&gt;&lt;br&gt;No benefits are offered with this job&lt;/p&gt;      &lt;p&gt;&lt;b&gt;About Employer FVN663&lt;/b&gt;&lt;br&gt;Company Description&lt;/p&gt;      &lt;p&gt;&lt;b&gt;How to apply&lt;/b&gt;&lt;br&gt;&lt;a href="http://localhost:57990" target="_blank"&gt;Log in to Focus/Career and submit your resume&lt;/a&gt;&lt;/p&gt;      &lt;p&gt;&lt;small&gt;REF/4760944/4761031&lt;/small&gt;&lt;/p&gt;    &lt;/body&gt;     #POSTINGFOOTER#    &lt;/html&gt;</postinghtml><REQUIREMENTS screening="1"></REQUIREMENTS><clientjobdata><CONTACT_SEND_DIRECT_FLAG>0</CONTACT_SEND_DIRECT_FLAG><CONTACT_POSTAL_FLAG>0</CONTACT_POSTAL_FLAG><CONTACT_PHONE_FLAG>0</CONTACT_PHONE_FLAG><CONTACT_FAX_FLAG>0</CONTACT_FAX_FLAG><CONTACT_EMAIL_FLAG>0</CONTACT_EMAIL_FLAG><CONTACT_URL_FLAG>0</CONTACT_URL_FLAG><CONTACT_TALENT_FLAG>1</CONTACT_TALENT_FLAG><SAL_MIN>0.00</SAL_MIN><SAL_MAX>0.00</SAL_MAX><SAL_UNIT_CD /><JOB_CREATE_DATE>2014-07-22</JOB_CREATE_DATE><JOB_LAST_OPEN_DATE>2015-07-22</JOB_LAST_OPEN_DATE></clientjobdata></jobposting>', N'Employer FVN663', N'', NULL, 1, 7, N'<html><body><p><b>Job with all applicants to be screened</b><br>Employer FVN663<br>Bluegrove, TX (76352)(public transit accessible)<br>Number of openings: 2<br>Application closing date: 22/07/2015<br></p><p>Test Job Description</p><p><b>Benefits</b><br>No benefits are offered with this job</p><p><b>About Employer FVN663</b><br>Company Description</p><p><b>How to apply</b><br><a href="http://localhost:57990" target="_blank">Log in to Focus/Career and submit your resume</a></p><p><small>REF/4760944/4761031</small></p></body>#POSTINGFOOTER#</html>', 
0, CAST(0x0000A3D200C401F0 AS DateTime), CAST(0x0000A3D200C401F0 AS DateTime), @JobId)

INSERT [dbo].[Data.Application.User] ([Id], [UserName], [PasswordHash], [PasswordSalt], [Enabled], [ValidationKey], [UserType], [LoggedInOn], [LastLoggedInOn], [ExternalId], [IsClientAuthenticated], [ScreenName], [SecurityQuestion], [SecurityAnswerHash], [IsMigrated], [RegulationsConsent], [CreatedOn], [UpdatedOn], [DeletedOn], [PersonId], [Pin], [FirstLoggedInOn], [Blocked], [ValidationKeyExpiry], [AccountDisabledReason]) VALUES (@UserId, N'FN648@Reconsider.com', N'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA', N'cded5112', 1, NULL, 2, NULL, NULL, NULL, 0, N'Red Yellow', NULL, NULL, 1, 1, CAST(0x0000A37700A743D1 AS DateTime), CAST(0x0000A37700A743D1 AS DateTime), NULL, @PersonId2, NULL, NULL, 0, NULL, NULL)

INSERT [dbo].[Data.Application.Application] ([Id], [ApprovalStatus], [ApprovalRequiredReason], [ApplicationStatus], [ApplicationScore], [StatusLastChangedOn], [CreatedOn], [UpdatedOn], [ResumeId], [PostingId], [Viewed], 
[PostHireFollowUpStatus], [AutomaticallyApproved], [StatusLastChangedBy], [AutomaticallyDenied]) 
VALUES (@ApplicationId, 5, N'Referral was denied but job seeker has requested referral be reconsidered', 1, 25, CAST(0x0000A3D200C4020C AS DateTime), CAST(0x0000A3D200C4020C AS DateTime), CAST(0x0000A3D200C4020C AS DateTime), @ResumeId, @PostingId, 1, NULL, 0, @UserId, 0)

INSERT [dbo].[Data.Application.ReferralEmail] ([Id], [PersonId], [EmailText], [EmailSentOn], [EmailSentBy], [ApprovalStatus], [ApplicationId], [EmployeeId]) VALUES (@ReferralEmailId, @PersonId, N'Dear Sir, You are not suitable for this position', CAST(0x0000A37900E6B680 AS DateTime), 2, 5, @ApplicationId, @EmployeeId)


