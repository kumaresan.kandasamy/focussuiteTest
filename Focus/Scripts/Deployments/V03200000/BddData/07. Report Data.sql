﻿Declare @ReportJobSeekerId BIGINT
Declare @UserId BIGINT


SELECT @UserId = PersonId FROM [Data.Application.User] WHERE UserName = 'fvn595@explorer.com'

SELECT @ReportJobSeekerId = MAX(Id) + 1 FROM [Report.JobSeeker];

IF @ReportJobSeekerId IS NULL
	BEGIN
	SET @ReportJobSeekerId = 1
	END



INSERT INTO [Report.JobSeeker]
           ([Id]
           ,[FocusPersonId]
           ,[FirstName]
           ,[LastName]
           ,[AccountCreationDate]
           ,[ResumeCount]
           ,[LatitudeRadians]
           ,[LongitudeRadians]
           ,[CreatedOn]
           ,[UpdatedOn]
           ,[HasPendingResume])

     VALUES
           (@ReportJobSeekerId,
					  @UserId,
            'FVN-595',
            'explorer',
            GETDATE(),
						1,
						0.587718681658067,
						1.71443168824627,
						GETDATE(),
						GETDATE(),
					  0	)


Declare @ReportDataId BIGINT

SELECT @ReportDataId = MAX(Id) FROM [Report.JobSeekerData];
IF @ReportDataId IS NULL
	BEGIN
	SET @ReportDataId = 1
	END

	INSERT INTO [Report.JobSeekerData]
		(
			Id,
			Description,
			DataType,
			JobSeekerId
		)
		VALUES
		(
			@ReportDataId +1,
			'212',
			 14,
			 @ReportJobSeekerId
		)

		INSERT INTO [Report.JobSeekerData]
		(
			Id,
			Description,
			DataType,
			JobSeekerId
		)
		VALUES
		(
			@ReportDataId +2,
			'11116',
			 13,
			@ReportJobSeekerId
		)