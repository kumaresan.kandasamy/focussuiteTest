﻿DECLARE @NextId BIGINT

DECLARE @EmployerId BIGINT
DECLARE @BusinessUnitId BIGINT
DECLARE @BusinessUnitAddressId BIGINT
DECLARE @BusinessUnitDescriptionId BIGINT
DECLARE @EmployeeId BIGINT
DECLARE @EmployeeBusinessUnitId BIGINT

DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @OwnershipTypeId BIGINT

SELECT @OwnershipTypeId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'OwnershipTypes' AND [Key] = 'OwnershipTypes.PrivateCorporation'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

BEGIN TRANSACTION
		
SELECT @EmployerId = Id FROM [Data.Application.Employer] WHERE FederalEmployerIdentificationNumber = '33-3333333'

SELECT @EmployeeId = E.Id FROM [Data.Application.Employee] AS E 
INNER JOIN [Data.Application.User] AS U ON E.PersonId = U.PersonId
WHERE UserName = 'fvn675MultipleBU@employer.com'

SELECT @NextId = NextId FROM KeyTable
		
UPDATE KeyTable SET NextId = NextId + 4

SET @BusinessUnitId = @NextId
SET @BusinessUnitAddressId = @NextId + 1
SET @BusinessUnitDescriptionId = @NextId + 2
SET @EmployeeBusinessUnitId = @NextId + 3

INSERT INTO [Data.Application.BusinessUnit]
(
	Id,
	EmployerId,
	Name,
	IsPrimary,
	Url,
	OwnershipTypeId,
	IndustrialClassification,
	PrimaryPhone,
	PrimaryPhoneExtension,
	PrimaryPhoneType,
	AlternatePhone1,
	AlternatePhone1Type,
	AlternatePhone2,
	AlternatePhone2Type,
	IsPreferred
)
VALUES
(
	@BusinessUnitId,
	@EmployerId,
	'Employer FVN675 Multiple BU Non Primary 2',
	0,
	null,
	@OwnershipTypeId,
	'11194 - Hay Farming',
	'1111111111',
	'',
	'Phone',
	'',
	'Phone',
	'',
	'Phone',
	1
)
		
IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END
		
INSERT INTO [Data.Application.BusinessUnitAddress]
(
	Id,
	BusinessUnitId,
	Line1,
	Line2,
	Line3,
	TownCity,
	CountyId,
	PostcodeZip,
	StateId,
	CountryId,
	IsPrimary,
	PublicTransitAccessible
)
VALUES
(
	@BusinessUnitAddressId,
	@BusinessUnitId,
	'Address Line 1',
	'',
	'',
	'Bluegrove',
	@CountyId,
	'76352',
	@StateId,
	@CountryId,
	1,
	1
)
		
IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END
		
INSERT INTO [Data.Application.BusinessUnitDescription]
(
	[Id],
	[BusinessUnitId],
	[Title],
	[Description],
	[IsPrimary]
)
VALUES
(
	@BusinessUnitDescriptionId,
	@BusinessUnitId,
	'Default description',
	'Employer FVN675 Multiple BU Non Primary 2',
	1
)
		
IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

INSERT INTO [Data.Application.EmployeeBusinessUnit]
(
	Id,
	[Default],
	[BusinessUnitId],
	[EmployeeId]
)
VALUES
(
	@EmployeeBusinessUnitId,
	0,
	@BusinessUnitId,
	@EmployeeId
)
		
IF @@ERROR <> 0
BEGIN
	ROLLBACK TRANSACTION
	RETURN
END

COMMIT TRANSACTION
