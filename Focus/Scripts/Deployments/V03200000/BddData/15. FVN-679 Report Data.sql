﻿
DECLARE @EmployerId BIGINT
SET @EmployerId = (SELECT MAX([Id]) FROM [Data.Application.Employer])

INSERT INTO [Report.Employer] (
		   [Id]
		   ,[Name]
           ,[FederalEmployerIdentificationNumber]
           ,[StateId]
           ,[PostalCode]
           ,[CountyId]
           ,[FocusBusinessUnitId]
           ,[State]
           ,[County]
           ,[LatitudeRadians]
           ,[LongitudeRadians]
           ,[CreatedOn]
           ,[UpdatedOn]
           ,[FocusEmployerId]
           ,[NoOfEmployees]) VALUES
(@EmployerId +1, 'Employer with 1-49 Employees' , '11-7777777',12896,76352,1586411,4760070,'Texas' ,'Clay',0.587718681658067,1.71443168824627,'2014-10-28','2014-10-28',4760068,1),
(@EmployerId +2, 'Employer with 50-99 Employees' , '11-7777777',12896,76352,1586411,4760070,'Texas' ,'Clay',0.587718681658067,1.71443168824627,'2014-10-28','2014-10-28',4760068,2),
(@EmployerId +3, 'Employer with 100-499 Employees' , '11-7777777',12896,76352,1586411,4760070,'Texas' ,'Clay',0.587718681658067,1.71443168824627,'2014-10-28','2014-10-28',4760068,3),
(@EmployerId +4, 'Employer with 500-999 Employees' , '11-7777777',12896,76352,1586411,4760070,'Texas' ,'Clay',0.587718681658067,1.71443168824627,'2014-10-28','2014-10-28',4760068,4),
(@EmployerId +5, 'Employer with 1000-4999 Employees' , '11-7777777',12896,76352,1586411,4760070,'Texas' ,'Clay',0.587718681658067,1.71443168824627,'2014-10-28','2014-10-28',4760068,5),
(@EmployerId +6, 'Employer with 5000+ Employees' , '11-7777777',12896,76352,1586411,4760070,'Texas' ,'Clay',0.587718681658067,1.71443168824627,'2014-10-28','2014-10-28',4760068,6)