﻿	/* get the dev@client.com userid */
  DECLARE @devClientUserId INT;
  SELECT @devClientUserId = Id 
  FROM dbo.[Data.Application.User]
  WHERE LOWER(username) = 'dev@client.com'
  
	/* Debug statement */
  /*SELECT @devClientUserId;*/
  
  DECLARE @RoleIds TABLE
  (
	Id BIGINT null
  )
  
  /* get the roleids not already assigned to dev@client.com */
  INSERT INTO @RoleIds
  SELECT Id
  FROM dbo.[Data.Application.Role] ar
  WHERE NOT EXISTS
  (
	SELECT RoleId
	FROM dbo.[Data.Application.UserRole] ur
	WHERE UserId = @devClientUserId
	AND ar.Id = ur.RoleId
  )
  
	/* Debug statement */
  /*SELECT * FROM @RoleIds*/
  
	/* iterate over the role ids and add to the user role table */
  WHILE (SELECT COUNT('X') FROM @RoleIds) > 0
  BEGIN
  
	 DECLARE @TopRoleId BIGINT;
	 SELECT @TopRoleId = MAX(Id) 
						 FROM @RoleIds;
						 
	 DECLARE @NextId BIGINT;
	 SELECT	@NextId = NextId 
	 FROM	dbo.[KeyTable]
  
	 INSERT INTO [FocusSuite].[dbo].[Data.Application.UserRole]
           ([Id]
           ,[RoleId]
           ,[UserId])
     VALUES
           (@NextId
           ,@TopRoleId 
           ,@devClientUserId)
     
		 /* remove the role just added from the list of user roles */
     DELETE FROM @RoleIds WHERE Id = @TopRoleId
     
		 /* update the key to the next value */
     UPDATE dbo.[KeyTable]
     SET	NextId = (@NextId + 1)
     
  END
  

GO

 DECLARE @NextId BIGINT;
 SELECT	@NextId = NextId 
 FROM	dbo.[KeyTable]

INSERT [dbo].[Report.Employer] 
(
	[Id], 
	[Name], 
	[FederalEmployerIdentificationNumber], 
	[StateId], 
	[PostalCode], 
	[Office], 
	[CreationDateTime], 
	[CountyId], 
	[FocusBusinessUnitId], 
	[State], 
	[County], 
	[LatitudeRadians], 
	[LongitudeRadians], 
	[CreatedOn], 
	[UpdatedOn], 
	[FocusEmployerId], 
	[AccountTypeId]
) 
VALUES 
(
	@NextId,
	N'Direct Employer', 
	N'95-6956956', 
	12904, 
	N'22222-2222', 
	N'Default Office', 
	NULL, 
	1586679, 
	4841393, 
	N'Virginia', 
	N'Arlington', 
	0.67815640750865669, 
	1.3447814246493868, 
	CAST(0x0000A3D100AEB1FD AS DateTime), 
	CAST(0x0000A3D100AEB21A AS DateTime), 
	4841395, 
	(SELECT		Id
	 FROM		dbo.[Config.CodeItem]
	 WHERE		[Key] = 'AccountTypes.DirectEmployer')
)

 /* update the key to the next value */
 UPDATE dbo.[KeyTable]
 SET	NextId = (@NextId + 1)
 
 SELECT	@NextId = NextId 
 FROM	dbo.[KeyTable]



INSERT [dbo].[Report.Employer] 
(
	[Id], 
	[Name], 
	[FederalEmployerIdentificationNumber], 
	[StateId], 
	[PostalCode], 
	[Office], 
	[CreationDateTime], 
	[CountyId], 
	[FocusBusinessUnitId], 
	[State], 
	[County], 
	[LatitudeRadians], 
	[LongitudeRadians], 
	[CreatedOn], 
	[UpdatedOn], 
	[FocusEmployerId], 
	[AccountTypeId]
) 
VALUES 
(
	@NextId, 
	N'Staffing Agency', 
	N'95-6956956', 
	12904, 
	N'22222-2222', 
	N'Default Office', 
	NULL, 
	1586679, 
	4841393, 
	N'Virginia', 
	N'Arlington', 
	0.67815640750865669, 
	1.3447814246493868, 
	CAST(0x0000A3D100AEB1FD AS DateTime), 
	CAST(0x0000A3D100AEB21A AS DateTime), 
	4841395, 
	(SELECT		Id
	 FROM		dbo.[Config.CodeItem]
	 WHERE		[Key] = 'AccountTypes.StaffingAgency')
)

 /* update the key to the next value */
 UPDATE dbo.[KeyTable]
 SET	NextId = (@NextId + 1)
 
 SELECT	@NextId = NextId 
 FROM	dbo.[KeyTable]

INSERT [dbo].[Report.Employer] 
(
	[Id], 
	[Name], 
	[FederalEmployerIdentificationNumber], 
	[StateId], 
	[PostalCode], 
	[Office], 
	[CreationDateTime], 
	[CountyId], 
	[FocusBusinessUnitId], 
	[State], 
	[County], 
	[LatitudeRadians], 
	[LongitudeRadians], 
	[CreatedOn], 
	[UpdatedOn], 
	[FocusEmployerId], 
	[AccountTypeId]
) 
VALUES 
(
	@NextId, 
	N'Not Indicated', 
	N'95-6956956', 
	12904, 
	N'22222-2222', 
	N'Default Office', 
	NULL, 
	1586679, 
	4841393, 
	N'Virginia', 
	N'Arlington', 
	0.67815640750865669, 
	1.3447814246493868, 
	CAST(0x0000A3D100AEB1FD AS DateTime), 
	CAST(0x0000A3D100AEB21A AS DateTime), 
	4841395, 
	null
)
GO