﻿DECLARE @ResumeId int;
DECLARE @PostingId int;

SELECT @ResumeId = (select top 1 Id from dbo.[Data.Application.Resume] order by id desc)
SELECT @PostingId = (select top 1 Id FROM dbo.[Data.Application.Posting] order by id desc)

INSERT [dbo].[Data.Application.Application] (
[Id], 
[ApprovalStatus], 
[ApprovalRequiredReason], 
[ApplicationStatus], 
[ApplicationScore], 
[StatusLastChangedOn], 
[CreatedOn], 
[UpdatedOn], 
[ResumeId], 
[PostingId], 
[Viewed], 
[PostHireFollowUpStatus], 
[AutomaticallyApproved], 
[StatusLastChangedBy], 
[AutomaticallyDenied]) 
VALUES (
(SELECT NEXTID FROM DBO.KEYTABLE), 
5, 
N'All referrals have to be reviewed by staff', 
1, 
25, 
CAST(0x0000A3D200C4020C AS DateTime), 
CAST(0x0000A3D200C4020C AS DateTime), 
CAST(0x0000A3D200C4020C AS DateTime), 
@ResumeId, 
@PostingId, 
1, 
NULL, 
0, 
NULL, 
0)
