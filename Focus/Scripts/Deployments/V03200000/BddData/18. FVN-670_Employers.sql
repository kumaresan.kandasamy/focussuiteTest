DECLARE @NextId BIGINT

DECLARE @MrTitleId BIGINT
DECLARE @MrsTitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @OwnershipTypeId BIGINT
DECLARE @AssistUserId BIGINT

SELECT @MrTitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @MrsTitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mrs'

SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'TalentUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @OwnershipTypeId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'OwnershipTypes' AND [Key] = 'OwnershipTypes.PrivateCorporation'
SELECT @AssistUserId = Id FROM [Data.Application.User] WHERE UserName = 'dev@client.com'

DECLARE @ParentEmployerName NVARCHAR(255)
DECLARE @ParentEmployerUserEmail NVARCHAR(255)
DECLARE @ParentEmployerId BIGINT
DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @PersonPhoneNumberId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @ParentEmployerAddressId BIGINT

DECLARE @EmployeeId BIGINT
DECLARE @EmployeeBusinessUnitId BIGINT
DECLARE @PersonTitleId BIGINT
DECLARE @PersonFirstName NVARCHAR(255)
DECLARE @PersonMiddleInitial NVARCHAR(5)
DECLARE @PersonLastName NVARCHAR(255)
DECLARE @PersonEmailAddress NVARCHAR(255)
DECLARE @BusinessUnitId BIGINT
DECLARE @BusinessUnitAddressId BIGINT
DECLARE @BusinessUnitDescriptionId BIGINT
DECLARE @BusinessUnitName NVARCHAR(255)
DECLARE @BusinessUnitTitle NVARCHAR(255)
DECLARE @BusinessUnitDescription NVARCHAR(255)
DECLARE @BusinessUnitAddressLine1 NVARCHAR(255)
DECLARE @NumberOfHiringManagers INT

DECLARE @PersonCount INT
SET @PersonCount = 1

DECLARE @BusinessUnitCount INT
SET @BusinessUnitCount = 1
DECLARE @TotalBusinessUnits INT

DECLARE @HiringManagerCount INT
SET @HiringManagerCount = 1

SET @ParentEmployerName = 'Employer FVN670 Parent'
SET @ParentEmployerUserEmail = 'david@fvn670.com'

DECLARE @People TABLE
(
	Id BIGINT IDENTITY(1, 1),
	TitleId BIGINT,
	FirstName NVARCHAR(255),
	MiddleInitial NVARCHAR(5),
	LastName NVARCHAR(50),
	EmailAddress NVARCHAR(200)
)

INSERT INTO @People
	(TitleId, FirstName, MiddleInitial, LastName, EmailAddress)
VALUES
	(@MrTitleId, 'David', NULL, 'Donnell', @ParentEmployerUserEmail)

INSERT INTO @People
	(TitleId, FirstName, MiddleInitial, LastName, EmailAddress)
VALUES
	(@MrTitleId, 'David', 'M', 'Ford', 'dford@fvn670.com')
	
INSERT INTO @People
	(TitleId, FirstName, MiddleInitial, LastName, EmailAddress)
VALUES
	(@MrsTitleId, 'Sonia', 'C', 'Starr', 'sstarr@fvn670.com')
	
INSERT INTO @People
	(TitleId, FirstName, MiddleInitial, LastName, EmailAddress)
VALUES
	(@MrsTitleId, 'Florence', 'D', 'Ochs', 'fochs@fvn670.com')
	
INSERT INTO @People
	(TitleId, FirstName, MiddleInitial, LastName, EmailAddress)
VALUES
	(@MrTitleId, 'Robert', 'L', 'Denton', 'rdenton@fvn670.com')
	
INSERT INTO @People
	(TitleId, FirstName, MiddleInitial, LastName, EmailAddress)
VALUES
	(@MrsTitleId, 'Helen', 'F', 'Taylor', 'htaylor@fvn670.com')
	
INSERT INTO @People
	(TitleId, FirstName, MiddleInitial, LastName, EmailAddress)
VALUES
	(@MrsTitleId, 'Suzanne', 'P', 'Sperry', 'ssperry@fvn670.com')
	
INSERT INTO @People
	(TitleId, FirstName, MiddleInitial, LastName, EmailAddress)
VALUES
	(@MrTitleId, 'John', 'E', 'Brown', 'jbrown@fvn670.com')
	

DECLARE @BusinessUnits TABLE
(
	Id BIGINT IDENTITY(1, 1),
	BusinessUnitName NVARCHAR(255),
	Title NVARCHAR(255),
	[Description] NVARCHAR(255),
	AddressLine1 NVARCHAR(255),
	NumberOfHiringManagers INT
)

INSERT INTO @BusinessUnits
	(BusinessUnitName, Title, [Description], AddressLine1, NumberOfHiringManagers)
VALUES
	(@ParentEmployerName, 'Parent Employer for FVN670 Tests', 'Parent Employer for FVN670 Tests', 'FVN670 House', 1)

INSERT INTO @BusinessUnits
	(BusinessUnitName, Title, [Description], AddressLine1, NumberOfHiringManagers)
VALUES
	('FVN670 Linked Business Unit 1', 'Linked Business Unit 1 for FVN670 Tests', 'Linked Business Unit with 1 hiring manager', 'FVN670 Office', 1)
	
INSERT INTO @BusinessUnits
	(BusinessUnitName, Title, [Description], AddressLine1, NumberOfHiringManagers)
VALUES
	('FVN670 Linked Business Unit 2', 'Linked Business Unit 2 for FVN670 Tests', 'Linked Business Unit with 2 hiring managers', 'FVN670 Division', 2)
	
INSERT INTO @BusinessUnits
	(BusinessUnitName, Title, [Description], AddressLine1, NumberOfHiringManagers)
VALUES
	('FVN670 Linked Business Unit 3', 'Linked Business Unit 3 for FVN670 Tests', 'Linked Business Unit with 4 hiring managers', 'FVN670 Depot', 4)


IF NOT EXISTS(SELECT 1 FROM [Data.Application.Employer] WHERE Name = @ParentEmployerName) AND
		NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @ParentEmployerUserEmail)
	BEGIN
		BEGIN TRANSACTION
		
			SELECT @NextId = NextId FROM KeyTable		
			UPDATE KeyTable SET NextId = NextId + 12
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			SET @PersonId = @NextId
			SET @PersonAddressId = @NextId + 1
			SET @PersonPhoneNumberId = @NextId + 2
			SET @UserId = @NextId + 3
			SET @UserRoleId = @NextId + 4
			SET @ParentEmployerId = @NextId + 5
			SET @ParentEmployerAddressId = @NextId + 6
			SET @BusinessUnitId = @NextId + 7
			SET @BusinessUnitAddressId = @NextId + 8
			SET @BusinessUnitDescriptionId = @NextId + 9
			SET @EmployeeId = @NextId + 10
			SET @EmployeeBusinessUnitId = @NextId + 11
			
			-- Begin Parent Employer
			
			SELECT
				@PersonTitleId = TitleId,
				@PersonFirstName = FirstName,
				@PersonMiddleInitial = MiddleInitial,
				@PersonLastName = LastName,
				@PersonEmailAddress = EmailAddress 
			FROM @People 
			WHERE Id = @PersonCount
			
			INSERT INTO [Data.Application.Person]
			(
				Id,
				TitleId,
				FirstName,
				MiddleInitial,
				LastName,
				DateOfBirth,
				SocialSecurityNumber,
				EmailAddress,
				EnrollmentStatus,
				ProgramAreaId,
				CampusId,
				DegreeId
			)
			VALUES
			(
				@PersonId,
				@PersonTitleId,
				@PersonFirstName,
				@PersonMiddleInitial,
				@PersonLastName,
				NULL,
				NULL,
				@PersonEmailAddress,
				NULL,
				NULL,
				NULL,
				NULL
			)
			
			SET @PersonCount = @PersonCount + 1
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.PersonAddress]
			(
				Id,
				PersonId,
				Line1,
				Line2,
				TownCity,
				StateId ,
				CountyId,
				CountryId,
				PostcodeZip,
				IsPrimary
			)
			VALUES
			(
				@PersonAddressId,
				@PersonId,
				'Address Line 1',
				'',
				'Bluegrove',
				@StateId,
				@CountyId,
				@CountryId,
				'76352',
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.PhoneNumber]
			(
				Id,
				PersonId,
				Number,
				PhoneType,
				IsPrimary,
				Extension,
				ProviderId
			)
			VALUES
			(
				@PersonPhoneNumberId,
				@PersonId,
				'4234234234',
				0,
				1,
				NULL,
				NULL
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.User]
			(
				Id,
				PersonId,
				UserName,
				PasswordHash,
				PasswordSalt,
				UserType,
				[Enabled],
				ScreenName,
				IsMigrated,
				RegulationsConsent,
				CreatedOn,
				UpdatedOn
			)
			VALUES
			(
				@UserId,
				@PersonId,
				@PersonEmailAddress,
				'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
				'cded5112',
				2,
				1,
				@PersonFirstName + ' ' + @PersonLastName,
				1,
				1,
				GETDATE(),
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.UserRole]
			(
				Id,
				RoleId,
				UserId
			)
			VALUES
			(
				@UserRoleId,
				@RoleId,
				@UserId
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.Employer]
			(
				Id,
				Name,
				CommencedOn,
				FederalEmployerIdentificationNumber,
				IsValidFederalEmployerIdentificationNumber,
				StateEmployerIdentificationNumber,
				ApprovalStatus,
				Url,
				ExpiredOn,
				OwnershipTypeId,
				IndustrialClassification,
				TermsAccepted,
				PrimaryPhone,
				PrimaryPhoneExtension,
				PrimaryPhoneType,
				AlternatePhone1,
				AlternatePhone1Type,
				AlternatePhone2,
				AlternatePhone2Type,
				IsRegistrationComplete,
				CreatedOn,
				UpdatedOn
			)
			VALUES
			(
				@ParentEmployerId,
				@ParentEmployerName,
				'01 January 2014',
				'11-7777777',
				1,
				'',
				2,
				'',
				NULL,
				@OwnershipTypeId,
				'11194 - Hay Farming',
				1,
				'1111111111',
				'',
				'Phone',
				'',
				'Alternate Phone',
				'',
				'Cell Phone',
				1,
				GETDATE(),
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.EmployerAddress]
			(
				Id,
				EmployerId,
				Line1,
				Line2,
				Line3,
				TownCity,
				CountyId,
				PostcodeZip,
				StateId,
				CountryId,
				IsPrimary,
				PublicTransitAccessible
			)
			VALUES
			(
				@ParentEmployerAddressId,
				@ParentEmployerId,
				'FVN670 House',
				'',
				'',
				'Bluegrove',
				@CountyId,
				'76352',
				@StateId,
				@CountryId,
				1,
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			SELECT 
				@BusinessUnitName = BusinessUnitName,
				@BusinessUnitTitle = Title,
				@BusinessUnitDescription = Description,
				@BusinessUnitAddressLine1 = AddressLine1
			FROM @BusinessUnits
			WHERE Id = @BusinessUnitCount
			
			INSERT INTO [Data.Application.BusinessUnit]
			(
				Id,
				EmployerId,
				Name,
				IsPrimary,
				Url,
				OwnershipTypeId,
				IndustrialClassification,
				PrimaryPhone,
				PrimaryPhoneExtension,
				PrimaryPhoneType,
				AlternatePhone1,
				AlternatePhone1Type,
				AlternatePhone2,
				AlternatePhone2Type,
				IsPreferred
			)
			VALUES
			(
				@BusinessUnitId,
				@ParentEmployerId,
				@BusinessUnitName,
				1,
				'',
				@OwnershipTypeId,
				'11194 - Hay Farming',
				'1111111111',
				'',
				'Phone',
				'',
				'Phone',
				'',
				'Phone',
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnitAddress]
			(
				Id,
				BusinessUnitId,
				Line1,
				Line2,
				Line3,
				TownCity,
				CountyId,
				PostcodeZip,
				StateId,
				CountryId,
				IsPrimary,
				PublicTransitAccessible
			)
			VALUES
			(
				@BusinessUnitAddressId,
				@BusinessUnitId,
				@BusinessUnitAddressLine1,
				'',
				'',
				'Bluegrove',
				@CountyId,
				'76352',
				@StateId,
				@CountryId,
				1,
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnitDescription]
			(
				[Id],
				[BusinessUnitId],
				[Title],
				[Description],
				[IsPrimary]
			)
			VALUES
			(
				@BusinessUnitDescriptionId,
				@BusinessUnitId,
				@BusinessUnitTitle,
				@BusinessUnitDescription,
				1
			)
			
			SET @BusinessUnitCount = @BusinessUnitCount + 1
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.Employee]
			(
				Id,
				EmployerId,
				ApprovalStatus,
				StaffUserId,
				PersonId,
				RedProfanityWords,
				YellowProfanityWords,
				ApprovalStatusChangedBy,
				ApprovalStatusChangedOn
			)
			VALUES
			(
				@EmployeeId,
				@ParentEmployerId,
				2,
				NULL,
				@PersonId,
				NULL,
				NULL,
				NULL,
				NULL
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.EmployeeBusinessUnit]
			(
				Id,
				[Default],
				[BusinessUnitId],
				[EmployeeId]
			)
			VALUES
			(
				@EmployeeBusinessUnitId,
				1,
				@BusinessUnitId,
				@EmployeeId
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			-- End Parent Employer
			
			-- Begin Child Business Units
			
			SELECT @TotalBusinessUnits = COUNT(1) FROM @BusinessUnits
			
			WHILE @BusinessUnitCount <= @TotalBusinessUnits
			BEGIN
			
				SELECT
					@BusinessUnitName = BusinessUnitName,
					@BusinessUnitTitle = Title,
					@BusinessUnitDescription = Description,
					@BusinessUnitAddressLine1 = AddressLine1,
					@NumberOfHiringManagers = NumberOfHiringManagers
				FROM @BusinessUnits
				WHERE Id = @BusinessUnitCount
				
				SELECT @NextId = NextId FROM KeyTable
				UPDATE KeyTable SET NextId = @NextId + 3
				
				SET @BusinessUnitId = @NextId
				SET @BusinessUnitAddressId = @NextId + 1
				SET @BusinessUnitDescriptionId = @NextId + 2
				
				SET @HiringManagerCount = 1
				
				INSERT INTO [Data.Application.BusinessUnit]
				(
					Id,
					EmployerId,
					Name,
					IsPrimary,
					Url,
					OwnershipTypeId,
					IndustrialClassification,
					PrimaryPhone,
					PrimaryPhoneExtension,
					PrimaryPhoneType,
					AlternatePhone1,
					AlternatePhone1Type,
					AlternatePhone2,
					AlternatePhone2Type,
					IsPreferred
				)
				VALUES
				(
					@BusinessUnitId,
					@ParentEmployerId,
					@BusinessUnitName,
					0,
					'',
					@OwnershipTypeId,
					'11194 - Hay Farming',
					'1111111111',
					'',
					'Phone',
					'',
					'Phone',
					'',
					'Phone',
					0
				)
				
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END
			
				INSERT INTO [Data.Application.BusinessUnitAddress]
				(
					Id,
					BusinessUnitId,
					Line1,
					Line2,
					Line3,
					TownCity,
					CountyId,
					PostcodeZip,
					StateId,
					CountryId,
					IsPrimary,
					PublicTransitAccessible
				)
				VALUES
				(
					@BusinessUnitAddressId,
					@BusinessUnitId,
					@BusinessUnitAddressLine1,
					'',
					'',
					'Bluegrove',
					@CountyId,
					'76352',
					@StateId,
					@CountryId,
					1,
					1
				)
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END
				
				INSERT INTO [Data.Application.BusinessUnitDescription]
				(
					[Id],
					[BusinessUnitId],
					[Title],
					[Description],
					[IsPrimary]
				)
				VALUES
				(
					@BusinessUnitDescriptionId,
					@BusinessUnitId,
					@BusinessUnitTitle,
					@BusinessUnitDescription,
					1
				)
				
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END
				
				WHILE @HiringManagerCount <= @NumberOfHiringManagers
				BEGIN
					SELECT
						@PersonTitleId = TitleId,
						@PersonFirstName = FirstName,
						@PersonMiddleInitial = MiddleInitial,
						@PersonLastName = LastName,
						@PersonEmailAddress = EmailAddress 
					FROM @People 
					WHERE Id = @PersonCount
					
					SELECT @NextId = NextId FROM KeyTable
					UPDATE KeyTable SET NextId = NextId + 7
					
					SET @PersonId = @NextId
					SET @PersonAddressId = @NextId + 1
					SET @PersonPhoneNumberId = @NextId + 2
					SET @UserId = @NextId + 3
					SET @UserRoleId = @NextId + 4
					SET @EmployeeId = @NextId + 5
					SET @EmployeeBusinessUnitId = @NextId + 6
					
					INSERT INTO [Data.Application.Person]
					(
						Id,
						TitleId,
						FirstName,
						MiddleInitial,
						LastName,
						DateOfBirth,
						SocialSecurityNumber,
						EmailAddress,
						EnrollmentStatus,
						ProgramAreaId,
						CampusId,
						DegreeId
					)
					VALUES
					(
						@PersonId,
						@PersonTitleId,
						@PersonFirstName,
						@PersonMiddleInitial,
						@PersonLastName,
						NULL,
						NULL,
						@PersonEmailAddress,
						NULL,
						NULL,
						NULL,
						NULL
					)
					
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRANSACTION
						RETURN
					END
					
					INSERT INTO [Data.Application.PersonAddress]
					(
						Id,
						PersonId,
						Line1,
						Line2,
						TownCity,
						StateId ,
						CountyId,
						CountryId,
						PostcodeZip,
						IsPrimary
					)
					VALUES
					(
						@PersonAddressId,
						@PersonId,
						'Address Line 1',
						'',
						'Bluegrove',
						@StateId,
						@CountyId,
						@CountryId,
						'76352',
						1
					)
					
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRANSACTION
						RETURN
					END
					
					INSERT INTO [Data.Application.PhoneNumber]
					(
						Id,
						PersonId,
						Number,
						PhoneType,
						IsPrimary,
						Extension,
						ProviderId
					)
					VALUES
					(
						@PersonPhoneNumberId,
						@PersonId,
						'4244244244',
						0,
						1,
						NULL,
						NULL
					)
					
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRANSACTION
						RETURN
					END
					
					INSERT INTO [Data.Application.User]
					(
						Id,
						PersonId,
						UserName,
						PasswordHash,
						PasswordSalt,
						UserType,
						[Enabled],
						ScreenName,
						IsMigrated,
						RegulationsConsent,
						CreatedOn,
						UpdatedOn
					)
					VALUES
					(
						@UserId,
						@PersonId,
						@PersonEmailAddress,
						'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
						'cded5112',
						2,
						1,
						@PersonFirstName + ' ' + @PersonLastName,
						1,
						1,
						GETDATE(),
						GETDATE()
					)
					
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRANSACTION
						RETURN
					END
					
					INSERT INTO [Data.Application.UserRole]
					(
						Id,
						RoleId,
						UserId
					)
					VALUES
					(
						@UserRoleId,
						@RoleId,
						@UserId
					)
					
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRANSACTION
						RETURN
					END
					
					INSERT INTO [Data.Application.Employee]
					(
						Id,
						EmployerId,
						ApprovalStatus,
						StaffUserId,
						PersonId,
						RedProfanityWords,
						YellowProfanityWords,
						ApprovalStatusChangedBy,
						ApprovalStatusChangedOn
					)
					VALUES
					(
						@EmployeeId,
						@ParentEmployerId,
						2,
						NULL,
						@PersonId,
						NULL,
						NULL,
						NULL,
						NULL
					)
					
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRANSACTION
						RETURN
					END
					
					INSERT INTO [Data.Application.EmployeeBusinessUnit]
					(
						Id,
						[Default],
						[BusinessUnitId],
						[EmployeeId]
					)
					VALUES
					(
						@EmployeeBusinessUnitId,
						1,
						@BusinessUnitId,
						@EmployeeId
					)
					
					IF @@ERROR <> 0
					BEGIN
						ROLLBACK TRANSACTION
						RETURN
					END
					
					SET @PersonCount = @PersonCount + 1
					SET @HiringManagerCount = @HiringManagerCount + 1
				END
			
				SET @BusinessUnitCount = @BusinessUnitCount + 1
			
			END		
			
			-- End Child Business Units
		
		COMMIT TRANSACTION
	END
