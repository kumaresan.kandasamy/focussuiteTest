DECLARE @NextId BIGINT

DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @OwnershipTypeId BIGINT
DECLARE @AssistUserId BIGINT

SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'TalentUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @OwnershipTypeId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'OwnershipTypes' AND [Key] = 'OwnershipTypes.PrivateCorporation'
SELECT @AssistUserId = Id FROM [Data.Application.User] WHERE UserName = 'dev@client.com'

DECLARE @ParentEmployerName NVARCHAR(255)
DECLARE @ChildBusinessUnitName NVARCHAR(255)
DECLARE @ParentEmployerUserEmail NVARCHAR(255)
DECLARE @ChildBusinessUnitUserEmail NVARCHAR(255)
DECLARE @ParentPersonId BIGINT
DECLARE @ParentPersonAddressId BIGINT
DECLARE @ParentPhoneNumberId BIGINT
DECLARE @ParentUserId BIGINT
DECLARE @ParentUserRoleId BIGINT
DECLARE @ParentEmployerId BIGINT
DECLARE @ParentEmployerAddressId BIGINT
DECLARE @ParentBusinessUnitId BIGINT
DECLARE @ParentBusinessUnitAddressId BIGINT
DECLARE @ParentBusinessUnitDescriptionId BIGINT
DECLARE @ParentEmployeeId BIGINT
DECLARE @ParentEmployeeBusinessUnitId BIGINT

DECLARE @ChildPersonId BIGINT
DECLARE @ChildPersonAddressId BIGINT
DECLARE @ChildPhoneNumberId BIGINT
DECLARE @ChildUserId BIGINT
DECLARE @ChildUserRoleId BIGINT
DECLARE @ChildBusinessUnitId BIGINT
DECLARE @ChildBusinessUnitAddressId BIGINT
DECLARE @ChildBusinessUnitDescriptionId BIGINT
DECLARE @ChildEmployeeId BIGINT
DECLARE @ChildEmployeeBusinessUnitId BIGINT

SET @ParentEmployerName = 'Employer FVN671 Parent'
SET @ChildBusinessUnitName = 'Employer FVN671 Child'
SET @ParentEmployerUserEmail = 'fvn671parent@employer.com'
SET @ChildBusinessUnitUserEmail = 'fvn671child@employer.com'

IF NOT EXISTS(SELECT 1 FROM [Data.Application.Employer] WHERE Name = @ParentEmployerName) AND
		NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @ParentEmployerUserEmail)
	BEGIN
		BEGIN TRANSACTION
		
			SELECT @NextId = NextId FROM KeyTable		
			UPDATE KeyTable SET NextId = NextId + 22
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			SET @ParentPersonId = @NextId
			SET @ParentPersonAddressId = @NextId + 1
			SET @ParentPhoneNumberId = @NextId + 2
			SET @ParentUserId = @NextId + 3
			SET @ParentUserRoleId = @NextId + 4
			SET @ParentEmployerId = @NextId + 5
			SET @ParentEmployerAddressId = @NextId + 6
			SET @ParentBusinessUnitId = @NextId + 7
			SET @ParentBusinessUnitAddressId = @NextId + 8
			SET @ParentBusinessUnitDescriptionId = @NextId + 9
			SET @ParentEmployeeId = @NextId + 10
			SET @ParentEmployeeBusinessUnitId = @NextId + 11
			SET @ChildPersonId = @NextId + 12
			SET @ChildPersonAddressId = @NextId + 13
			SET @ChildPhoneNumberId = @NextId + 14
			SET @ChildUserId = @NextId + 15
			SET @ChildUserRoleId = @NextId + 16
			SET @ChildBusinessUnitId = @NextId + 17
			SET @ChildBusinessUnitAddressId = @NextId + 18
			SET @ChildBusinessUnitDescriptionId = @NextId + 19
			SET @ChildEmployeeId = @NextId + 20
			SET @ChildEmployeeBusinessUnitId = @NextId + 21
			
			-- Begin Parent Employer
			
			INSERT INTO [Data.Application.Person]
			(
				Id,
				TitleId,
				FirstName,
				MiddleInitial,
				LastName,
				DateOfBirth,
				SocialSecurityNumber,
				EmailAddress,
				EnrollmentStatus,
				ProgramAreaId,
				CampusId,
				DegreeId
			)
			VALUES
			(
				@ParentPersonId,
				@TitleId,
				'FVN671',
				'',
				'Parent',
				NULL,
				NULL,
				@ParentEmployerUserEmail,
				NULL,
				NULL,
				NULL,
				NULL
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.PersonAddress]
			(
				Id,
				PersonId,
				Line1,
				Line2,
				TownCity,
				StateId ,
				CountyId,
				CountryId,
				PostcodeZip,
				IsPrimary
			)
			VALUES
			(
				@ParentPersonAddressId,
				@ParentPersonId,
				'Address Line 1',
				'',
				'Bluegrove',
				@StateId,
				@CountyId,
				@CountryId,
				'76352',
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.PhoneNumber]
			(
				Id,
				PersonId,
				Number,
				PhoneType,
				IsPrimary,
				Extension,
				ProviderId
			)
			VALUES
			(
				@ParentPhoneNumberId,
				@ParentPersonId,
				'4234234234',
				0,
				1,
				NULL,
				NULL
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.User]
			(
				Id,
				PersonId,
				UserName,
				PasswordHash,
				PasswordSalt,
				UserType,
				[Enabled],
				ScreenName,
				IsMigrated,
				RegulationsConsent,
				CreatedOn,
				UpdatedOn
			)
			VALUES
			(
				@ParentUserId,
				@ParentPersonId,
				@ParentEmployerUserEmail,
				'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
				'cded5112',
				2,
				1,
				'FVN671 Parent',
				1,
				1,
				GETDATE(),
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.UserRole]
			(
				Id,
				RoleId,
				UserId
			)
			VALUES
			(
				@ParentUserRoleId,
				@RoleId,
				@ParentUserId
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.Employer]
			(
				Id,
				Name,
				CommencedOn,
				FederalEmployerIdentificationNumber,
				IsValidFederalEmployerIdentificationNumber,
				StateEmployerIdentificationNumber,
				ApprovalStatus,
				Url,
				ExpiredOn,
				OwnershipTypeId,
				IndustrialClassification,
				TermsAccepted,
				PrimaryPhone,
				PrimaryPhoneExtension,
				PrimaryPhoneType,
				AlternatePhone1,
				AlternatePhone1Type,
				AlternatePhone2,
				AlternatePhone2Type,
				IsRegistrationComplete,
				CreatedOn,
				UpdatedOn
			)
			VALUES
			(
				@ParentEmployerId,
				@ParentEmployerName,
				'01 January 2014',
				'11-7777777',
				1,
				'',
				2,
				'',
				NULL,
				@OwnershipTypeId,
				'11194 - Hay Farming',
				1,
				'1111111111',
				'',
				'Phone',
				'',
				'Phone',
				'',
				'Phone',
				1,
				GETDATE(),
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.EmployerAddress]
			(
				Id,
				EmployerId,
				Line1,
				Line2,
				Line3,
				TownCity,
				CountyId,
				PostcodeZip,
				StateId,
				CountryId,
				IsPrimary,
				PublicTransitAccessible
			)
			VALUES
			(
				@ParentEmployerAddressId,
				@ParentEmployerId,
				'FVN671 House',
				'',
				'',
				'Bluegrove',
				@CountyId,
				'76352',
				@StateId,
				@CountryId,
				1,
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnit]
			(
				Id,
				EmployerId,
				Name,
				IsPrimary,
				Url,
				OwnershipTypeId,
				IndustrialClassification,
				PrimaryPhone,
				PrimaryPhoneExtension,
				PrimaryPhoneType,
				AlternatePhone1,
				AlternatePhone1Type,
				AlternatePhone2,
				AlternatePhone2Type,
				IsPreferred
			)
			VALUES
			(
				@ParentBusinessUnitId,
				@ParentEmployerId,
				@ParentEmployerName,
				1,
				'',
				@OwnershipTypeId,
				'11194 - Hay Farming',
				'1111111111',
				'',
				'Phone',
				'',
				'Phone',
				'',
				'Phone',
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnitAddress]
			(
				Id,
				BusinessUnitId,
				Line1,
				Line2,
				Line3,
				TownCity,
				CountyId,
				PostcodeZip,
				StateId,
				CountryId,
				IsPrimary,
				PublicTransitAccessible
			)
			VALUES
			(
				@ParentBusinessUnitAddressId,
				@ParentBusinessUnitId,
				'FVN671 House',
				'',
				'',
				'Bluegrove',
				@CountyId,
				'76352',
				@StateId,
				@CountryId,
				1,
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnitDescription]
			(
				[Id],
				[BusinessUnitId],
				[Title],
				[Description],
				[IsPrimary]
			)
			VALUES
			(
				@ParentBusinessUnitDescriptionId,
				@ParentBusinessUnitId,
				'Parent Employer for FVN671 tests',
				'Parent Employer for FVN671 tests',
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.Employee]
			(
				Id,
				EmployerId,
				ApprovalStatus,
				StaffUserId,
				PersonId,
				RedProfanityWords,
				YellowProfanityWords,
				ApprovalStatusChangedBy,
				ApprovalStatusChangedOn
			)
			VALUES
			(
				@ParentEmployeeId,
				@ParentEmployerId,
				2,
				NULL,
				@ParentPersonId,
				NULL,
				NULL,
				NULL,
				NULL
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.EmployeeBusinessUnit]
			(
				Id,
				[Default],
				[BusinessUnitId],
				[EmployeeId]
			)
			VALUES
			(
				@ParentEmployeeBusinessUnitId,
				1,
				@ParentBusinessUnitId,
				@ParentEmployeeId
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			-- End Parent Employer
			
			-- Begin Child Employer
			
			IF EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @ChildBusinessUnitUserEmail)
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END
			
			INSERT INTO [Data.Application.Person]
			(
				Id,
				TitleId,
				FirstName,
				MiddleInitial,
				LastName,
				DateOfBirth,
				SocialSecurityNumber,
				EmailAddress,
				EnrollmentStatus,
				ProgramAreaId,
				CampusId,
				DegreeId
			)
			VALUES
			(
				@ChildPersonId,
				@TitleId,
				'FVN671',
				'',
				'Child',
				NULL,
				NULL,
				@ChildBusinessUnitUserEmail,
				NULL,
				NULL,
				NULL,
				NULL
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.PersonAddress]
			(
				Id,
				PersonId,
				Line1,
				Line2,
				TownCity,
				StateId ,
				CountyId,
				CountryId,
				PostcodeZip,
				IsPrimary
			)
			VALUES
			(
				@ChildPersonAddressId,
				@ChildPersonId,
				'Address Line 1',
				'',
				'Bluegrove',
				@StateId,
				@CountyId,
				@CountryId,
				'76352',
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.PhoneNumber]
			(
				Id,
				PersonId,
				Number,
				PhoneType,
				IsPrimary,
				Extension,
				ProviderId
			)
			VALUES
			(
				@ChildPhoneNumberId,
				@ChildPersonId,
				'4244244244',
				0,
				1,
				NULL,
				NULL
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.User]
			(
				Id,
				PersonId,
				UserName,
				PasswordHash,
				PasswordSalt,
				UserType,
				[Enabled],
				ScreenName,
				IsMigrated,
				RegulationsConsent,
				CreatedOn,
				UpdatedOn
			)
			VALUES
			(
				@ChildUserId,
				@ChildPersonId,
				@ChildBusinessUnitUserEmail,
				'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
				'cded5112',
				2,
				1,
				'FVN671 Child',
				1,
				1,
				GETDATE(),
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.UserRole]
			(
				Id,
				RoleId,
				UserId
			)
			VALUES
			(
				@ChildUserRoleId,
				@RoleId,
				@ChildUserId
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnit]
			(
				Id,
				EmployerId,
				Name,
				IsPrimary,
				Url,
				OwnershipTypeId,
				IndustrialClassification,
				PrimaryPhone,
				PrimaryPhoneExtension,
				PrimaryPhoneType,
				AlternatePhone1,
				AlternatePhone1Type,
				AlternatePhone2,
				AlternatePhone2Type,
				IsPreferred
			)
			VALUES
			(
				@ChildBusinessUnitId,
				@ParentEmployerId,
				@ChildBusinessUnitName,
				0,
				'',
				@OwnershipTypeId,
				'11194 - Hay Farming',
				'1111111111',
				'',
				'Phone',
				'',
				'Phone',
				'',
				'Phone',
				0
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnitAddress]
			(
				Id,
				BusinessUnitId,
				Line1,
				Line2,
				Line3,
				TownCity,
				CountyId,
				PostcodeZip,
				StateId,
				CountryId,
				IsPrimary,
				PublicTransitAccessible
			)
			VALUES
			(
				@ChildBusinessUnitAddressId,
				@ChildBusinessUnitId,
				'FVN671 Depot',
				'',
				'',
				'Bluegrove',
				@CountyId,
				'76352',
				@StateId,
				@CountryId,
				1,
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.BusinessUnitDescription]
			(
				[Id],
				[BusinessUnitId],
				[Title],
				[Description],
				[IsPrimary]
			)
			VALUES
			(
				@ChildBusinessUnitDescriptionId,
				@ChildBusinessUnitId,
				'Child Employer for FVN671 tests',
				'Child Employer for FVN671 tests',
				1
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.Employee]
			(
				Id,
				EmployerId,
				ApprovalStatus,
				StaffUserId,
				PersonId,
				RedProfanityWords,
				YellowProfanityWords,
				ApprovalStatusChangedBy,
				ApprovalStatusChangedOn
			)
			VALUES
			(
				@ChildEmployeeId,
				@ParentEmployerId,
				2,
				NULL,
				@ChildPersonId,
				NULL,
				NULL,
				NULL,
				NULL
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			INSERT INTO [Data.Application.EmployeeBusinessUnit]
			(
				Id,
				[Default],
				[BusinessUnitId],
				[EmployeeId]
			)
			VALUES
			(
				@ChildEmployeeBusinessUnitId,
				1,
				@ChildBusinessUnitId,
				@ParentEmployeeId
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			-- End Child Employer
		
		COMMIT TRANSACTION
	END
