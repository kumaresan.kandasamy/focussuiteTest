USE [FocusSuite]
GO

INSERT INTO [dbo].[Data.Application.Application]
           ([Id]
           ,[ApprovalStatus]
           ,[ApprovalRequiredReason]
           ,[ApplicationStatus]
           ,[ApplicationScore]
           ,[StatusLastChangedOn]
           ,[CreatedOn]
           ,[UpdatedOn]
           ,[ResumeId]
           ,[PostingId]
           ,[Viewed]
           ,[PostHireFollowUpStatus]
           ,[AutomaticallyApproved]
           ,[StatusLastChangedBy]
           ,[AutomaticallyDenied])
		SELECT TOP 1
		[Data.Application.Application].[Id] - 1
		,4
		,[ApprovalRequiredReason]
		,[ApplicationStatus]
		,[ApplicationScore]
		,'2014-07-07 11:01:02.003'
		,[Data.Application.Application].[CreatedOn]
		,'2014-07-07 11:01:02.003'
		,[Data.Application.Resume].[Id]
		,[Data.Application.Posting].[Id]
		,0
		,[PostHireFollowUpStatus]
		,0
		,[StatusLastChangedBy]
		,0
  FROM [dbo].[Data.Application.Posting]
  INNER JOIN [dbo].[Data.Application.Job] on [Data.Application.Job].[Id] = [dbo].[Data.Application.Posting].[JobId]
  CROSS JOIN [dbo].[Data.Application.Resume]
  INNER JOIN [Data.Application.BusinessUnit] ON [BusinessUnitId] = [Data.Application.BusinessUnit].[Id]
  CROSS JOIN [dbo].[Data.Application.Application]
  WHERE [Data.Application.BusinessUnit].[Name] = 'Test Employer 1'
  AND [Data.Application.Job].[JobTitle] = 'Education Developer 2'
  AND [ResumeName] = 'Dev Explorer Default Resume'


