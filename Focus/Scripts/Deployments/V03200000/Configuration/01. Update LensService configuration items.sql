UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '}]}', '},{"key":"ResumeIndustries","tag":"cf026"}]}')
WHERE [Key] = 'LensService'
	AND Value LIKE '%customFilters%'
	AND Value NOT LIKE '%ResumeIndustries%'