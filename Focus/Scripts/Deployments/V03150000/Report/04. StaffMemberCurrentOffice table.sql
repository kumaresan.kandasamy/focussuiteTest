IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.StaffMemberCurrentOffice')
BEGIN
	CREATE TABLE [dbo].[Report.StaffMemberCurrentOffice](
		[Id] [bigint] NOT NULL,
		[OfficeName] [nvarchar](100) NOT NULL,
		[OfficeId] [bigint] NULL,
		StartDate DATETIME NULL,
		[StaffMemberId] [bigint] NOT NULL DEFAULT(0),
		FOREIGN KEY (StaffMemberId) REFERENCES [Report.StaffMember](Id)
	)
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.StaffMemberCurrentOffice_StaffMemberId' AND object_id = OBJECT_ID('[Report.StaffMemberCurrentOffice]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.StaffMemberCurrentOffice_StaffMemberId]
		ON [Report.StaffMemberCurrentOffice] (StaffMemberId)
END

-- Populate any existing data
IF NOT EXISTS(SELECT 1 FROM [Report.StaffMemberCurrentOffice])
BEGIN
	DECLARE @NextId BIGINT
	DECLARE @Count INT
	DECLARE @OfficeCount INT
	DECLARE @RowCount INT
	
	SELECT 
		@Count = COUNT(1) 
	FROM 
		[Data.Application.User] U
	INNER JOIN [Data.Application.PersonsCurrentOffice] PCO
		ON PCO.PersonId = U.PersonId
	WHERE
		U.UserType = 1

	SELECT @NextId = NextId FROM [KeyTable]
	UPDATE [KeyTable] SET NextId = NextId + @Count

	INSERT INTO [Report.StaffMemberCurrentOffice]
	(
		Id,
		OfficeName,
		OfficeId,
		StartDate,
		StaffMemberId
	)
	SELECT
		@NextId - 1 + ROW_NUMBER() OVER(ORDER BY U.Id ASC),
		O.OfficeName,
		O.Id,
		PCO.StartTime,
		SM.Id
	FROM [Data.Application.User] U
	INNER JOIN [Data.Application.PersonsCurrentOffice] PCO
		ON PCO.PersonId = U.PersonId
	INNER JOIN [Data.Application.Office] O
		ON O.Id = PCO.OfficeId	
	INNER JOIN [Report.StaffMember] SM
		ON SM.FocusPersonId = PCO.PersonId
	WHERE
		U.UserType = 1
END