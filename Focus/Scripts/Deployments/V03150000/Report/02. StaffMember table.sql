IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.StaffMember')
BEGIN
	CREATE TABLE [Report.StaffMember]
	(
		Id BIGINT NOT NULL PRIMARY KEY,
		FirstName NVARCHAR(100),
		LastName NVARCHAR(100),
		AddressLine1 NVARCHAR(200),
		CountyId BIGINT,
		County NVARCHAR(400),
		StateId BIGINT,
		[State] NVARCHAR(400),
		PostalCode NVARCHAR(40),
		Office NVARCHAR(400),
		EmailAddress NVARCHAR(500) NOT NULL,
		AccountCreationDate DATETIME NOT NULL,
		LatitudeRadians FLOAT NOT NULL DEFAULT(0),
		LongitudeRadians FLOAT NOT NULL DEFAULT(0),
		FocusPersonId BIGINT NOT NULL,
		CreatedOn DATETIME NOT NULL,
		UpdatedOn DATETIME NOT NULL
	)
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.StaffMember_FocusPersonId' AND object_id = OBJECT_ID('[Report.StaffMember]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.StaffMember_FocusPersonId]
		ON [Report.StaffMember] (FocusPersonId)
END

-- Populate any existing data
IF NOT EXISTS(SELECT 1 FROM [Report.StaffMember])
BEGIN
	DECLARE @NextId BIGINT
	DECLARE @Count INT

	SELECT 
		@Count = COUNT(1) 
	FROM 
		[Data.Application.User]
	WHERE
		UserType = 1

	SELECT @NextId = NextId FROM [KeyTable]
	UPDATE [KeyTable] SET NextId = NextId + @Count

	INSERT INTO [Report.StaffMember]
	(
		Id,
		FirstName,
		LastName,
		AddressLine1,
		CountyId,
		County,
		StateId,
		[State],
		PostalCode,
		Office,
		EmailAddress,
		AccountCreationDate,
		LatitudeRadians,
		LongitudeRadians,
		FocusPersonId,
		CreatedOn,
		UpdatedOn
	)
	SELECT
		@NextId - 1 + ROW_NUMBER() OVER(ORDER BY P.Id ASC),
		P.FirstName,
		P.LastName,
		PA.Line1,
		PA.CountyId,
		LC.Value,
		PA.StateId,
		LS.Value,
		PA.PostcodeZip,
		'' AS Office,
		P.EmailAddress,
		U.CreatedOn,
		PI() * ISNULL(PC.Latitude, 0) / 180.0,
		PI() * ISNULL(PC.Longitude, 0) / 180.0,
		P.Id,
		GETDATE(),
		GETDATE()
	FROM
		[Data.Application.User] U
	INNER JOIN [Data.Application.Person] P
		ON P.Id = U.PersonId
	LEFT OUTER JOIN [Data.Application.PersonAddress] PA
		ON PA.PersonId = P.Id
		AND PA.IsPrimary = 1
	LEFT OUTER JOIN [Config.LookupItemsView] LS
		ON LS.Id = PA.StateId
	LEFT OUTER JOIN [Config.LookupItemsView] LC
		ON LC.Id = PA.CountyId
	LEFT OUTER JOIN [Library.PostalCode] PC
		ON PC.Code = SUBSTRING(PA.PostcodeZip, 1, 5)
	WHERE
		U.UserType = 1
END
