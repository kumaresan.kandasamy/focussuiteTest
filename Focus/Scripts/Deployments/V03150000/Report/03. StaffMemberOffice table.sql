IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.StaffMemberOffice')
BEGIN
	CREATE TABLE [dbo].[Report.StaffMemberOffice](
		[Id] [bigint] NOT NULL,
		[OfficeName] [nvarchar](100) NOT NULL,
		[OfficeId] [bigint] NULL,
		AssignedDate DATETIME NULL,
		[StaffMemberId] [bigint] NOT NULL DEFAULT(0),
		FOREIGN KEY (StaffMemberId) REFERENCES [Report.StaffMember](Id)
	)
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Report.StaffMemberOffice_StaffMemberId' AND object_id = OBJECT_ID('[Report.StaffMemberOffice]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Report.StaffMemberOffice_StaffMemberId]
		ON [Report.StaffMemberOffice] (StaffMemberId)
END

-- Populate any existing data
IF NOT EXISTS(SELECT 1 FROM [Report.StaffMemberOffice])
BEGIN
	DECLARE @NextId BIGINT
	DECLARE @Count INT
	DECLARE @OfficeCount INT
	DECLARE @RowCount INT
	
	SELECT 
		@Count = COUNT(1) 
	FROM 
		[Data.Application.User] U
	INNER JOIN [Data.Application.PersonOfficeMapper] POM
		ON POM.PersonId = U.PersonId
	WHERE
		U.UserType = 1
		AND POM.OfficeId IS NOT NULL

	SELECT @NextId = NextId FROM [KeyTable]
	UPDATE [KeyTable] SET NextId = NextId + @Count

	INSERT INTO [Report.StaffMemberOffice]
	(
		Id,
		OfficeName,
		OfficeId,
		AssignedDate,
		StaffMemberId
	)
	SELECT
		@NextId - 1 + ROW_NUMBER() OVER(ORDER BY U.Id ASC),
		O.OfficeName,
		O.Id,
		POM.CreatedOn,
		SM.Id
	FROM [Data.Application.User] U
	INNER JOIN [Data.Application.PersonOfficeMapper] POM
		ON POM.PersonId = U.PersonId
	INNER JOIN [Data.Application.Office] O
		ON O.Id = POM.OfficeId	
	INNER JOIN [Report.StaffMember] SM
		ON SM.FocusPersonId = POM.PersonId
	WHERE
		U.UserType = 1
		AND POM.OfficeId IS NOT NULL

	-- 'Office' field on StaffMember table is concatenated value of all assigned offices
	UPDATE
		SM
	SET	
		Office = SMO.OfficeName
	FROM
		[Report.StaffMember] SM
	INNER JOIN [Report.StaffMemberOffice] SMO
		ON SMO.StaffMemberId = SM.Id
		
	SET @RowCount = @@ROWCOUNT
	SET @OfficeCount = 2

	WHILE @RowCount > 0
	BEGIN
		;WITH StaffMemberOffices AS
		(
			SELECT
				SMO.StaffMemberId,
				ROW_NUMBER() OVER(PARTITION BY SMO.StaffMemberId ORDER BY SMO.Id ASC) AS RowNumber,
				SMO.OfficeName
			FROM
				[Report.StaffMemberOffice] SMO
		)
		UPDATE
			SM
		SET	
			Office = SM.Office + '||' + SMO.OfficeName
		FROM
			[Report.StaffMember] SM
		INNER JOIN StaffMemberOffices SMO
			ON SMO.StaffMemberId = SM.Id
			AND SMO.RowNumber = @OfficeCount

		SET @RowCount = @@ROWCOUNT
		SET @OfficeCount = @OfficeCount + 1
	END
END