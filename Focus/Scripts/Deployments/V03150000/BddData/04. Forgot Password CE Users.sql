-- Registered Seekers

DECLARE @JobSeekers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	EmailAddress NVARCHAR(255),
	PhoneNumber NVARCHAR(20),
	ProgamAreaId BIGINT,
	DegreeId BIGINT,
	ValidationKey NVARCHAR(86)
)

INSERT INTO @JobSeekers (FirstName, LastName, EmailAddress, PhoneNumber, ProgamAreaId, DegreeId, ValidationKey)
VALUES 
	('Forgot', 'Password1', 'Test130902115702165153@burning-glass.com', '1111111111', NULL, NULL, NULL),
	('Forgot', 'Password2', 'Test130902115259001093@burning-glass.com', '2222222222', NULL, NULL, NULL),
	('Validation', 'Key', 'FVN293@test.bgt.com', '34940494094', NULL, NULL, 'Llg2mcpEd4pGqLkTA9VntPQQ5xnHQVTYEKBoD2052fcJCYv6Yy42yi7RK5wZMf2A9Zz5gIVh0rT4ztWB2an6g')

DECLARE @SeekerIndex INT
DECLARE @SeekerCount INT

DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @PhoneNumberId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @IssuesId BIGINT

DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @EmailAddress NVARCHAR(255)
DECLARE @PhoneNumber NVARCHAR(20)
DECLARE @ProgamAreaId BIGINT
DECLARE @DegreeId BIGINT
DECLARE @ValidationKey NVARCHAR(86)
	
SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'CareerUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

SET @SeekerIndex = 1
SELECT @SeekerCount = COUNT(1) FROM @JobSeekers

WHILE @SeekerIndex <= @SeekerCount
BEGIN
	SELECT 
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@PhoneNumber = PhoneNumber,
		@ProgamAreaId = ProgamAreaId,
		@DegreeId = DegreeId,
		@ValidationKey = ValidationKey
	FROM
		@JobSeekers
	WHERE
		Id = @SeekerIndex
		
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 6
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @UserRoleId = @NextId + 4
		SET @IssuesId = @NextId + 5
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			ProgramAreaId,
			CampusId,
			DegreeId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			@ProgamAreaId,
			NULL,
			@DegreeId
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			@PhoneNumber,
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			CreatedOn,
			UpdatedOn,
			ValidationKey
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			12,
			1,
			@FirstName + ' ' + @LastName,
			1,
			1,
			GETDATE(),
			GETDATE(),
			@ValidationKey
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		VALUES
		(
			@UserRoleId,
			@RoleId,
			@UserId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.Issues] 
		(
			Id, 
			PersonId
		)
		VALUES
		(
			@IssuesId,
			@PersonId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	
	SET @SeekerIndex = @SeekerIndex + 1
END

-- Non-registered seekers (EDU Only)
DECLARE @Pins TABLE
(
	Id BIGINT IDENTITY(1, 1),
	EmailAddress NVARCHAR(255),
	Pin NVARCHAR(255)
)

INSERT INTO @Pins (EmailAddress, Pin)
VALUES ('Pin130902115259001093@burning-glass.com', '99a11825fe1d4ad')
	
SELECT @NextId = NextId FROM KeyTable
		
UPDATE KeyTable SET NextId = NextId + 1

INSERT INTO [dbo].[Data.Application.RegistrationPin]
(	
	Id,
	EmailAddress,
	Pin
)
SELECT
	@NextId + Id - 1,
	P.EmailAddress,
	P.Pin
FROM
	@Pins P
WHERE
	NOT EXISTS(SELECT 1 FROM [Data.Application.RegistrationPin] RP WHERE RP.EmailAddress = P.EmailAddress)

