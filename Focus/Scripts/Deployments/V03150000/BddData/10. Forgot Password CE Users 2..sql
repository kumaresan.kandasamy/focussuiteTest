-- Registered Seekers

DECLARE @JobSeekers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	EmailAddress NVARCHAR(255),
	PhoneNumber NVARCHAR(20),
	ProgamAreaId BIGINT,
	DegreeId BIGINT,
	ValidationKey NVARCHAR(86),
	ValidationKeyExpiry DATETIME
)

INSERT INTO @JobSeekers (FirstName, LastName, EmailAddress, PhoneNumber, ProgamAreaId, DegreeId, ValidationKey, ValidationKeyExpiry)
VALUES 
	('FVN308', 'One', 'FVN308.One@test.bgt.com', '23234424244', NULL, NULL, '35QhSH62uxPTyu202/0fhJ7lsiIk2N3Loyo3qmBEAaEcUyWEjyxIxfpCK0XQqBlQK0kh+l0f5WI9YeOCPn2Nvw', DATEADD(DAY, 365, GETDATE())),
	('FVN308', 'Two', 'FVN308.Two@test.bgt.com', '43234424242', NULL, NULL, 'wvN2nPCOeY9IW5f0l+hk0KQlBqQX0KCpfxIxyjEWyUcEaAEBmq3oyoL3N2kIisl7Jhf0/202uyTPxu26HShQ53', DATEADD(DAY, -1, GETDATE())),
	('FVN308', 'Three', 'FVN308.Three@test.bgt.com', '43234524242', NULL, NULL, 'j320r3290u3i940u23r90n803094u2390u49jincong03wr99023f0nc03c0n3f0nu34fnu340nf304nf340nf', NULL)
	
DECLARE @SeekerIndex INT
DECLARE @SeekerCount INT

DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @PhoneNumberId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @IssuesId BIGINT
DECLARE @LogId BIGINT

DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @EmailAddress NVARCHAR(255)
DECLARE @PhoneNumber NVARCHAR(20)
DECLARE @ProgamAreaId BIGINT
DECLARE @DegreeId BIGINT
DECLARE @ValidationKey NVARCHAR(86)
DECLARE @ValidationKeyExpiry DATETIME
	
SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'CareerUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

SET @SeekerIndex = 1
SELECT @SeekerCount = COUNT(1) FROM @JobSeekers

WHILE @SeekerIndex <= @SeekerCount
BEGIN
	SELECT 
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@PhoneNumber = PhoneNumber,
		@ProgamAreaId = ProgamAreaId,
		@DegreeId = DegreeId,
		@ValidationKey = ValidationKey,
		@ValidationKeyExpiry = ValidationKeyExpiry
	FROM
		@JobSeekers
	WHERE
		Id = @SeekerIndex
		
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 7
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @UserRoleId = @NextId + 4
		SET @IssuesId = @NextId + 5
		SET @LogId = @NextId + 6
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			ProgramAreaId,
			CampusId,
			DegreeId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			@ProgamAreaId,
			NULL,
			@DegreeId
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			@PhoneNumber,
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			CreatedOn,
			UpdatedOn,
			ValidationKey,
			ValidationKeyExpiry
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			12,
			1,
			@FirstName + ' ' + @LastName,
			1,
			1,
			GETDATE(),
			GETDATE(),
			CASE WHEN @ValidationKeyExpiry IS NULL THEN NULL ELSE @ValidationKey END,
			@ValidationKeyExpiry
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		VALUES
		(
			@UserRoleId,
			@RoleId,
			@UserId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.Issues] 
		(
			Id, 
			PersonId
		)
		VALUES
		(
			@IssuesId,
			@PersonId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		IF @ValidationKeyExpiry IS NULL
		BEGIN
			INSERT INTO [Data.Application.UserValidationKeyLog]
			(
				Id,
				UserId,
				ValidationKey,
				[Status],
				LogDate
			)
			VALUES
			(
				@LogId,
				@UserId,
				@ValidationKey,
				2,
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
		END
		
		COMMIT TRANSACTION
	END
	
	SET @SeekerIndex = @SeekerIndex + 1
END

-- Non-registered seekers (EDU Only)
DECLARE @Pins TABLE
(
	Id BIGINT IDENTITY(1, 1),
	EmailAddress NVARCHAR(255),
	Pin NVARCHAR(255)
)

INSERT INTO @Pins (EmailAddress, Pin)
VALUES ('Pin130902115259001093@burning-glass.com', '99a11825fe1d4ad')
	
SELECT @NextId = NextId FROM KeyTable
		
UPDATE KeyTable SET NextId = NextId + 1

INSERT INTO [dbo].[Data.Application.RegistrationPin]
(	
	Id,
	EmailAddress,
	Pin
)
SELECT
	@NextId + Id - 1,
	P.EmailAddress,
	P.Pin
FROM
	@Pins P
WHERE
	NOT EXISTS(SELECT 1 FROM [Data.Application.RegistrationPin] RP WHERE RP.EmailAddress = P.EmailAddress)

