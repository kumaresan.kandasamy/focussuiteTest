DECLARE @NextId BIGINT
DECLARE @Email NVARCHAR(200)

SET @Email = 'test1@bgt.com'

IF NOT EXISTS(SELECT 1 FROM [Data.Application.RegistrationPin] WHERE EmailAddress = @Email)
BEGIN
	BEGIN TRANSACTION 
		SELECT @NextId = NextId FROM KeyTable
			
		UPDATE KeyTable SET NextId = NextId + 1
	
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [dbo].[Data.Application.RegistrationPin]
			(	Id,
			Pin,
			EmailAddress
			)
		VALUES
		(@NextId,
		'70c09c41a20d9a9',
		'test1@bgt.com'
		)
	
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
	
		COMMIT TRANSACTION
END