  -- Script to update existing jobseekers in the BDD test DB to add SSNs and External IDs for testing purposes

	UPDATE [dbo].[Data.Application.Person]
  SET [SocialSecurityNumber] = '222222222'
  WHERE [Id] = 4760056
  
  UPDATE [dbo].[Data.Application.Person]
  SET [SocialSecurityNumber] = '333333333'
  WHERE [Id] = 4760060

	UPDATE [FocusSuite].[dbo].[Data.Application.User]
  SET [ExternalId] = 'AB12345'
  WHERE [PersonId] = 4760056
  
  UPDATE [FocusSuite].[dbo].[Data.Application.User]
  SET [ExternalId] = 'CD67890'
  WHERE [PersonId] = 4760060
		