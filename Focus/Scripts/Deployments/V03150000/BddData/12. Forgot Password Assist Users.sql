-- Assist Users

DECLARE @AssistUsers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	EmailAddress NVARCHAR(255),
	PhoneNumber NVARCHAR(20),
	ValidationKey NVARCHAR(86),
	ValidationKeyExpiry DATETIME
)

INSERT INTO @AssistUsers (FirstName, LastName, EmailAddress, PhoneNumber, ValidationKey, ValidationKeyExpiry)
VALUES 
	('FVN308', 'Assist1', 'FVN308.Assist1@test.bgt.com', '23234424244', 'j30jjgej04jiv0jvni0njdbnjdflkbs0aw87f3fguvov7a89a89weg0ewbvb770wve97g0vweb7v7g9ew79gvf', DATEADD(DAY, 365, GETDATE())),
	('FVN308', 'Assist2', 'FVN308.Assist2@test.bgt.com', '53234424242', 'wie9fhwefwoehfowhfuowfhuweoqhfuwofhweuoqhweuofhwequfohwufohweuowhfuowqhfuwoqhfuwofwuoh', DATEADD(DAY, -1, GETDATE())),
	('FVN308', 'Assist3', 'FVN308.Assist3@test.bgt.com', '53234524242', 'fneigj4380g4nhg0nv40vn24nv4200924cu450hc0427cg02h79znbchxhb7x2byg7509xcyg792by7xb9y7xb', NULL)
	
DECLARE @AssistUserIndex INT
DECLARE @AssistUserCount INT

DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @PhoneNumberId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @LogId BIGINT

DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @EmailAddress NVARCHAR(255)
DECLARE @PhoneNumber NVARCHAR(20)
DECLARE @ValidationKey NVARCHAR(86)
DECLARE @ValidationKeyExpiry DATETIME
	
SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'AssistUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

SET @AssistUserIndex = 1
SELECT @AssistUserCount = COUNT(1) FROM @AssistUsers

WHILE @AssistUserIndex <= @AssistUserCount
BEGIN
	SELECT 
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@PhoneNumber = PhoneNumber,
		@ValidationKey = ValidationKey,
		@ValidationKeyExpiry = ValidationKeyExpiry
	FROM
		@AssistUsers
	WHERE
		Id = @AssistUserIndex
		
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 6
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @UserRoleId = @NextId + 4
		SET @LogId = @NextId + 5
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			CampusId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			NULL
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			@PhoneNumber,
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			CreatedOn,
			UpdatedOn,
			ValidationKey,
			ValidationKeyExpiry
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			1,
			1,
			@FirstName + ' ' + @LastName,
			1,
			1,
			GETDATE(),
			GETDATE(),
			CASE WHEN @ValidationKeyExpiry IS NULL THEN NULL ELSE @ValidationKey END,
			@ValidationKeyExpiry
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		VALUES
		(
			@UserRoleId,
			@RoleId,
			@UserId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END

		IF @ValidationKeyExpiry IS NULL
		BEGIN
			INSERT INTO [Data.Application.UserValidationKeyLog]
			(
				Id,
				UserId,
				ValidationKey,
				[Status],
				LogDate
			)
			VALUES
			(
				@LogId,
				@UserId,
				@ValidationKey,
				2,
				GETDATE()
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
		END
		
		COMMIT TRANSACTION
	END
	
	SET @AssistUserIndex = @AssistUserIndex + 1
END
