DECLARE @NextId BIGINT

DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'


DECLARE @OfficeName NVARCHAR(20)
DECLARE @UnassignedDefault BIT

DECLARE @Offices TABLE
(
	Id BIGINT IDENTITY(1, 1),
	OfficeName NVARCHAR(20),
	UnassignedDefault bit
)

INSERT INTO @Offices 
	(OfficeName, UnassignedDefault)
VALUES 
	('Default Office', 1),
	('Office 1', 0),
	('Office 2', 0),
	('Office DO NOT ASSIGN', 0)

DECLARE @OfficeCount INT
DECLARE @TotalOffices INT

SET @OfficeCount = 1
SELECT @TotalOffices = COUNT(1) FROM @Offices

WHILE @OfficeCount <= @TotalOffices
BEGIN
	SELECT 
		@OfficeName = OfficeName,
		@UnassignedDefault = UnassignedDefault
	FROM
		@Offices
	WHERE
		Id = @OfficeCount

	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Office] WHERE OfficeName = @OfficeName)
	BEGIN

		BEGIN TRANSACTION 
			SELECT @NextId = NextId FROM KeyTable
					
			UPDATE KeyTable SET NextId = NextId + 1
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
					
			INSERT INTO [dbo].[Data.Application.Office]
			  (	Id,
				OfficeName,
				Line1,
				Line2,
				TownCity,
				CountyId,
				StateId,
				CountryId,
				PostcodeZip,
				AssignedPostcodeZip,
				UnassignedDefault,
				OfficeManagerMailbox,
				ExternalId,
				InActive
				)
			VALUES
			(@NextId,
			@OfficeName,
			'Address Line 1',
			'',
			'Bluegrove',
			@CountyId,
			@StateId,
			@CountryId,
			'76352',
			NULL,
			@UnassignedDefault,
			NULL,
			NULL,
			0
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
			
			COMMIT TRANSACTION
	END
	SET @OfficeCount = @OfficeCount + 1
END