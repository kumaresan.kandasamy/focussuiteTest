IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'ValidationKeyExpiry')
BEGIN
	ALTER TABLE
		[Data.Application.User]
	ADD
		ValidationKeyExpiry DATETIME NULL
END
GO

UPDATE
	[Data.Application.User]
SET
	ValidationKeyExpiry = DATEADD(HOUR, 24, GETDATE())
WHERE
	ValidationKey IS NOT NULL
	AND ValidationKeyExpiry IS NULL
