IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.User_UserType' AND object_id = OBJECT_ID('[Data.Application.User]'))
BEGIN
	CREATE NONCLUSTERED INDEX 
		[IX_Data.Application.User_UserType] 
	ON 
		[dbo].[Data.Application.User] ([UserType]) 
	INCLUDE 
		([PersonId])
END
