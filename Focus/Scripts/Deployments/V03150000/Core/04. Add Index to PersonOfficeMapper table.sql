IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.PersonOfficeMapper_OfficeId' AND object_id = OBJECT_ID('[Data.Application.PersonOfficeMapper]'))
BEGIN
	CREATE INDEX 
		[IX_Data.Application.PersonOfficeMapper_OfficeId] 
	ON 
		[Data.Application.PersonOfficeMapper] (OfficeId)
END
