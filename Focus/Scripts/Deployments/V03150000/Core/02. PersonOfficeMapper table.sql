IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.PersonOfficeMapper' AND COLUMN_NAME = 'CreatedOn')
BEGIN
	ALTER TABLE
		[Data.Application.PersonOfficeMapper]
	ADD
		CreatedOn DATETIME NULL
END
GO

UPDATE
	[Data.Application.PersonOfficeMapper]
SET
	CreatedOn = '01 January 1900'
WHERE
	CreatedOn IS NULL