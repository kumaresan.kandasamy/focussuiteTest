IF NOT EXISTS(SELECT 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 23)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType,
		[Subject],
		Body,
		Salutation,
		SenderEmailType
	)
	VALUES
	(
		23,
		'Password Reset for #FOCUSCAREER#',
		'This email is being sent to you because you forgot your password. Please click on the following link to reset your password:

#URL#

With this information, you should have no problem accessing your account. We do encourage you to come back often to #FOCUSCAREER# to manage your resume and job search.
We wish you luck and look forward to helping you find your next job!

#FOCUSCAREERURL#
',
		'Dear #RECIPIENTNAME#',
		0
	)
END
ELSE
BEGIN
	UPDATE
		[Config.EmailTemplate]
	SET
		[Subject] = 'Password Reset for #FOCUSCAREER#',
		Body = 'This email is being sent to you because you forgot your password. Please click on the following link to reset your password:

#URL#

With this information, you should have no problem accessing your account. We do encourage you to come back often to #FOCUSCAREER# to manage your resume and job search.
We wish you luck and look forward to helping you find your next job!

#FOCUSCAREERURL#
'
	WHERE
		EmailTemplateType = 23
		AND (Body LIKE '%#PASSWORD#%' OR Body LIKE '%#AUTHENTICATEURL#%' OR [Subject] = 'New Password for #FOCUSCAREER#')
END
