DECLARE @Items TABLE
(
	[Key] NVARCHAR(200),
	[Value] NVARCHAR(4000)
)
INSERT INTO @Items([Key], [Value])
VALUES
('Focus.Web.WebAssist.JobSeekerProfile.SelfReferrals:Education', 'Applications to college exclusive postings'),
('Focus.CareerExplorer.WebAuth.ResetPassword.UserValidationKeyExpired.Message:Education', 'Your request to reset your password has expired. Click on the link below to return to the #FOCUSCAREER# login page and click "Forgot your password/PIN?" to request another reset'),
('Focus.CareerExplorer.WebAuth.ResetPassword.UserValidationKeyAlreadyUsed.Message:Education', 'Your password has already been reset. Click on the link below to return to the #FOCUSCAREER# login page and enter your updated password. If you have forgotten it, click "Forgot your password/PIN?" to reset it again')

DECLARE @LocalisationId BIGINT
SELECT @LocalisationId = Id FROM [Config.Localisation] WHERE Culture = '**-**'

INSERT INTO [Config.LocalisationItem] 
(
	ContextKey, 
	[Key], 
	Value, 
	Localised, 
	LocalisationId
)
SELECT
	'',
	I.[Key],
	I.[Value],
	0,
	@LocalisationId
FROM
	@Items I
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.LocalisationItem] LI
		WHERE
			LI.[Key] = I.[Key]
			AND LI.LocalisationId = @LocalisationId
	)
