UPDATE 
	[Data.Application.Office] 
SET 
	OfficeManagerMailbox = 'Office1@test.bgt.com' 
WHERE 
	OfficeName = 'Office 1'
	AND OfficeManagerMailbox IS NULL