DECLARE @ResumeId BIGINT

SELECT @ResumeId = Id FROM [Data.Application.Resume] WHERE ResumeName = 'FVN648 Resume Name'

IF (@ResumeId > 0)
BEGIN
	DECLARE @ApplicationId BIGINT

	SELECT @ApplicationId = Id FROM [Data.Application.Application] WHERE ResumeId = @ResumeId
	
	IF (@ApplicationId > 0)
	BEGIN
	
		UPDATE [Data.Application.ReferralEmail]
		SET ApprovalStatus = 3
		WHERE ApplicationId = @ApplicationId AND
			ApprovalStatus = 5
	
	END

END



