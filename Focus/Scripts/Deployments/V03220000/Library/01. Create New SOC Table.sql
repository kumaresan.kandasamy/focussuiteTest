IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Library.SOC')
BEGIN
	CREATE TABLE [Library.SOC]
	(
		Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
		SOC2010Code NVARCHAR(10),
		[Key] NVARCHAR(200),
		Title NVARCHAR(255)
	)
END
GO

DECLARE @SOC TABLE
(
	SOC2010Code NVARCHAR(10),
	[Key] NVARCHAR(200),
	Title NVARCHAR(255)
)
DECLARE @SOCXML XML

SET @SOCXML = 
'<socs>
	<soc code="11-1011" key="SOC2010.11-1011" title="Chief Executives" />
	<soc code="11-1021" key="SOC2010.11-1021" title="General and Operations Managers" />
	<soc code="11-1031" key="SOC2010.11-1031" title="Legislators" />
	<soc code="11-2011" key="SOC2010.11-2011" title="Advertising and Promotions Managers" />
	<soc code="11-2021" key="SOC2010.11-2021" title="Marketing Managers" />
	<soc code="11-2022" key="SOC2010.11-2022" title="Sales Managers" />
	<soc code="11-2031" key="SOC2010.11-2031" title="Public Relations and Fundraising Managers" />
	<soc code="11-3011" key="SOC2010.11-3011" title="Administrative Services Managers" />
	<soc code="11-3021" key="SOC2010.11-3021" title="Computer and Information Systems Managers" />
	<soc code="11-3031" key="SOC2010.11-3031" title="Financial Managers" />
	<soc code="11-3051" key="SOC2010.11-3051" title="Industrial Production Managers" />
	<soc code="11-3061" key="SOC2010.11-3061" title="Purchasing Managers" />
	<soc code="11-3071" key="SOC2010.11-3071" title="Transportation, Storage, and Distribution Managers" />
	<soc code="11-3111" key="SOC2010.11-3111" title="Compensation and Benefits Managers" />
	<soc code="11-3121" key="SOC2010.11-3121" title="Human Resources Managers" />
	<soc code="11-3131" key="SOC2010.11-3131" title="Training and Development Managers" />
	<soc code="11-9013" key="SOC2010.11-9013" title="Farmers, Ranchers, and Other Agricultural Managers" />
	<soc code="11-9021" key="SOC2010.11-9021" title="Construction Managers" />
	<soc code="11-9031" key="SOC2010.11-9031" title="Education Administrators, Preschool and Childcare Center/Program" />
	<soc code="11-9032" key="SOC2010.11-9032" title="Education Administrators, Elementary and Secondary School" />
	<soc code="11-9033" key="SOC2010.11-9033" title="Education Administrators, Postsecondary" />
	<soc code="11-9039" key="SOC2010.11-9039" title="Education Administrators, All Other" />
	<soc code="11-9041" key="SOC2010.11-9041" title="Architectural and Engineering Managers" />
	<soc code="11-9051" key="SOC2010.11-9051" title="Food Service Managers" />
	<soc code="11-9061" key="SOC2010.11-9061" title="Funeral Service Managers" />
	<soc code="11-9071" key="SOC2010.11-9071" title="Gaming Managers" />
	<soc code="11-9081" key="SOC2010.11-9081" title="Lodging Managers" />
	<soc code="11-9111" key="SOC2010.11-9111" title="Medical and Health Services Managers" />
	<soc code="11-9121" key="SOC2010.11-9121" title="Natural Sciences Managers" />
	<soc code="11-9131" key="SOC2010.11-9131" title="Postmasters and Mail Superintendents" />
	<soc code="11-9141" key="SOC2010.11-9141" title="Property, Real Estate, and Community Association Managers" />
	<soc code="11-9151" key="SOC2010.11-9151" title="Social and Community Service Managers" />
	<soc code="11-9161" key="SOC2010.11-9161" title="Emergency Management Directors" />
	<soc code="11-9199" key="SOC2010.11-9199" title="Managers, All Other" />
	<soc code="13-1011" key="SOC2010.13-1011" title="Agents and Business Managers of Artists, Performers, and Athletes" />
	<soc code="13-1021" key="SOC2010.13-1021" title="Buyers and Purchasing Agents, Farm Products" />
	<soc code="13-1022" key="SOC2010.13-1022" title="Wholesale and Retail Buyers, Except Farm Products" />
	<soc code="13-1023" key="SOC2010.13-1023" title="Purchasing Agents, Except Wholesale, Retail, and Farm Products" />
	<soc code="13-1031" key="SOC2010.13-1031" title="Claims Adjusters, Examiners, and Investigators" />
	<soc code="13-1032" key="SOC2010.13-1032" title="Insurance Appraisers, Auto Damage" />
	<soc code="13-1041" key="SOC2010.13-1041" title="Compliance Officers" />
	<soc code="13-1051" key="SOC2010.13-1051" title="Cost Estimators" />
	<soc code="13-1071" key="SOC2010.13-1071" title="Human Resources Specialists" />
	<soc code="13-1074" key="SOC2010.13-1074" title="Farm Labor Contractors" />
	<soc code="13-1075" key="SOC2010.13-1075" title="Labor Relations Specialists" />
	<soc code="13-1081" key="SOC2010.13-1081" title="Logisticians" />
	<soc code="13-1111" key="SOC2010.13-1111" title="Management Analysts" />
	<soc code="13-1121" key="SOC2010.13-1121" title="Meeting, Convention, and Event Planners" />
	<soc code="13-1131" key="SOC2010.13-1131" title="Fundraisers" />
	<soc code="13-1141" key="SOC2010.13-1141" title="Compensation, Benefits, and Job Analysis Specialists" />
	<soc code="13-1151" key="SOC2010.13-1151" title="Training and Development Specialists" />
	<soc code="13-1161" key="SOC2010.13-1161" title="Market Research Analysts and Marketing Specialists" />
	<soc code="13-1199" key="SOC2010.13-1199" title="Business Operations Specialists, All Other" />
	<soc code="13-2011" key="SOC2010.13-2011" title="Accountants and Auditors" />
	<soc code="13-2021" key="SOC2010.13-2021" title="Appraisers and Assessors of Real Estate" />
	<soc code="13-2031" key="SOC2010.13-2031" title="Budget Analysts" />
	<soc code="13-2041" key="SOC2010.13-2041" title="Credit Analysts" />
	<soc code="13-2051" key="SOC2010.13-2051" title="Financial Analysts" />
	<soc code="13-2052" key="SOC2010.13-2052" title="Personal Financial Advisors" />
	<soc code="13-2053" key="SOC2010.13-2053" title="Insurance Underwriters" />
	<soc code="13-2061" key="SOC2010.13-2061" title="Financial Examiners" />
	<soc code="13-2071" key="SOC2010.13-2071" title="Credit Counselors" />
	<soc code="13-2072" key="SOC2010.13-2072" title="Loan Officers" />
	<soc code="13-2081" key="SOC2010.13-2081" title="Tax Examiners and Collectors, and Revenue Agents" />
	<soc code="13-2082" key="SOC2010.13-2082" title="N/A" />
	<soc code="13-2099" key="SOC2010.13-2099" title="Financial Specialists, All Other" />
	<soc code="15-1111" key="SOC2010.15-1111" title="Computer and Information Research Scientists" />
	<soc code="15-1121" key="SOC2010.15-1121" title="Computer Systems Analysts" />
	<soc code="15-1122" key="SOC2010.15-1122" title="Information Security Analysts" />
	<soc code="15-1131" key="SOC2010.15-1131" title="Computer Programmers" />
	<soc code="15-1132" key="SOC2010.15-1132" title="Software Developers, Applications" />
	<soc code="15-1133" key="SOC2010.15-1133" title="Software Developers, Systems Software" />
	<soc code="15-1134" key="SOC2010.15-1134" title="Web Developers" />
	<soc code="15-1141" key="SOC2010.15-1141" title="Database Administrators" />
	<soc code="15-1142" key="SOC2010.15-1142" title="Network and Computer Systems Administrators" />
	<soc code="15-1143" key="SOC2010.15-1143" title="Computer Network Architects" />
	<soc code="15-1151" key="SOC2010.15-1151" title="Computer User Support Specialists" />
	<soc code="15-1152" key="SOC2010.15-1152" title="Computer Network Support Specialists" />
	<soc code="15-1199" key="SOC2010.15-1199" title="Computer Occupations, All Other" />
	<soc code="15-2011" key="SOC2010.15-2011" title="Actuaries" />
	<soc code="15-2021" key="SOC2010.15-2021" title="Mathematicians" />
	<soc code="15-2031" key="SOC2010.15-2031" title="Operations Research Analysts" />
	<soc code="15-2041" key="SOC2010.15-2041" title="Statisticians" />
	<soc code="15-2091" key="SOC2010.15-2091" title="Mathematical Technicians" />
	<soc code="15-2099" key="SOC2010.15-2099" title="Mathematical Science Occupations, All Other" />
	<soc code="17-1011" key="SOC2010.17-1011" title="Architects, Except Landscape and Naval" />
	<soc code="17-1012" key="SOC2010.17-1012" title="Landscape Architects" />
	<soc code="17-1021" key="SOC2010.17-1021" title="Cartographers and Photogrammetrists" />
	<soc code="17-1022" key="SOC2010.17-1022" title="Surveyors" />
	<soc code="17-2011" key="SOC2010.17-2011" title="Aerospace Engineers" />
	<soc code="17-2021" key="SOC2010.17-2021" title="Agricultural Engineers" />
	<soc code="17-2031" key="SOC2010.17-2031" title="Biomedical Engineers" />
	<soc code="17-2041" key="SOC2010.17-2041" title="Chemical Engineers" />
	<soc code="17-2051" key="SOC2010.17-2051" title="Civil Engineers" />
	<soc code="17-2061" key="SOC2010.17-2061" title="Computer Hardware Engineers" />
	<soc code="17-2071" key="SOC2010.17-2071" title="Electrical Engineers" />
	<soc code="17-2072" key="SOC2010.17-2072" title="Electronics Engineers, Except Computer" />
	<soc code="17-2081" key="SOC2010.17-2081" title="Environmental Engineers" />
	<soc code="17-2111" key="SOC2010.17-2111" title="Health and Safety Engineers, Except Mining Safety Engineers and Inspectors" />
	<soc code="17-2112" key="SOC2010.17-2112" title="Industrial Engineers" />
	<soc code="17-2121" key="SOC2010.17-2121" title="Marine Engineers and Naval Architects" />
	<soc code="17-2131" key="SOC2010.17-2131" title="Materials Engineers" />
	<soc code="17-2141" key="SOC2010.17-2141" title="Mechanical Engineers" />
	<soc code="17-2151" key="SOC2010.17-2151" title="Mining and Geological Engineers, Including Mining Safety Engineers" />
	<soc code="17-2161" key="SOC2010.17-2161" title="Nuclear Engineers" />
	<soc code="17-2171" key="SOC2010.17-2171" title="Petroleum Engineers" />
	<soc code="17-2199" key="SOC2010.17-2199" title="Engineers, All Other" />
	<soc code="17-3011" key="SOC2010.17-3011" title="Architectural and Civil Drafters" />
	<soc code="17-3012" key="SOC2010.17-3012" title="Electrical and Electronics Drafters" />
	<soc code="17-3013" key="SOC2010.17-3013" title="Mechanical Drafters" />
	<soc code="17-3019" key="SOC2010.17-3019" title="Drafters, All Other" />
	<soc code="17-3021" key="SOC2010.17-3021" title="Aerospace Engineering and Operations Technicians" />
	<soc code="17-3022" key="SOC2010.17-3022" title="Civil Engineering Technicians" />
	<soc code="17-3023" key="SOC2010.17-3023" title="Electrical and Electronic Engineering Technicians" />
	<soc code="17-3024" key="SOC2010.17-3024" title="Electro-Mechanical Technicians" />
	<soc code="17-3025" key="SOC2010.17-3025" title="Environmental Engineering Technicians" />
	<soc code="17-3026" key="SOC2010.17-3026" title="Industrial Engineering Technicians" />
	<soc code="17-3027" key="SOC2010.17-3027" title="Mechanical Engineering Technicians" />
	<soc code="17-3029" key="SOC2010.17-3029" title="Engineering Technicians, Except Drafters, All Other" />
	<soc code="17-3031" key="SOC2010.17-3031" title="Surveying and Mapping Technicians" />
	<soc code="19-1011" key="SOC2010.19-1011" title="Animal Scientists" />
	<soc code="19-1012" key="SOC2010.19-1012" title="Food Scientists and Technologists" />
	<soc code="19-1013" key="SOC2010.19-1013" title="Soil and Plant Scientists" />
	<soc code="19-1020" key="SOC2010.19-1020" title="N/A" />
	<soc code="19-1021" key="SOC2010.19-1021" title="Biochemists and Biophysicists" />
	<soc code="19-1022" key="SOC2010.19-1022" title="Microbiologists" />
	<soc code="19-1023" key="SOC2010.19-1023" title="Zoologists and Wildlife Biologists" />
	<soc code="19-1029" key="SOC2010.19-1029" title="Biological Scientists, All Other" />
	<soc code="19-1031" key="SOC2010.19-1031" title="Conservation Scientists" />
	<soc code="19-1032" key="SOC2010.19-1032" title="Foresters" />
	<soc code="19-1041" key="SOC2010.19-1041" title="Epidemiologists" />
	<soc code="19-1042" key="SOC2010.19-1042" title="Medical Scientists, Except Epidemiologists" />
	<soc code="19-1099" key="SOC2010.19-1099" title="Life Scientists, All Other" />
	<soc code="19-2011" key="SOC2010.19-2011" title="Astronomers" />
	<soc code="19-2012" key="SOC2010.19-2012" title="Physicists" />
	<soc code="19-2021" key="SOC2010.19-2021" title="Atmospheric and Space Scientists" />
	<soc code="19-2031" key="SOC2010.19-2031" title="Chemists" />
	<soc code="19-2032" key="SOC2010.19-2032" title="Materials Scientists" />
	<soc code="19-2041" key="SOC2010.19-2041" title="Environmental Scientists and Specialists, Including Health" />
	<soc code="19-2042" key="SOC2010.19-2042" title="Geoscientists, Except Hydrologists and Geographers" />
	<soc code="19-2043" key="SOC2010.19-2043" title="Hydrologists" />
	<soc code="19-2099" key="SOC2010.19-2099" title="Physical Scientists, All Other" />
	<soc code="19-3011" key="SOC2010.19-3011" title="Economists" />
	<soc code="19-3022" key="SOC2010.19-3022" title="Survey Researchers" />
	<soc code="19-3031" key="SOC2010.19-3031" title="Clinical, Counseling, and School Psychologists" />
	<soc code="19-3032" key="SOC2010.19-3032" title="Industrial-Organizational Psychologists" />
	<soc code="19-3039" key="SOC2010.19-3039" title="Psychologists, All Other" />
	<soc code="19-3041" key="SOC2010.19-3041" title="Sociologists" />
	<soc code="19-3051" key="SOC2010.19-3051" title="Urban and Regional Planners" />
	<soc code="19-3091" key="SOC2010.19-3091" title="Anthropologists and Archeologists" />
	<soc code="19-3092" key="SOC2010.19-3092" title="Geographers" />
	<soc code="19-3093" key="SOC2010.19-3093" title="Historians" />
	<soc code="19-3094" key="SOC2010.19-3094" title="Political Scientists" />
	<soc code="19-3099" key="SOC2010.19-3099" title="Social Scientists and Related Workers, All Other" />
	<soc code="19-4011" key="SOC2010.19-4011" title="Agricultural and Food Science Technicians" />
	<soc code="19-4021" key="SOC2010.19-4021" title="Biological Technicians" />
	<soc code="19-4031" key="SOC2010.19-4031" title="Chemical Technicians" />
	<soc code="19-4041" key="SOC2010.19-4041" title="Geological and Petroleum Technicians" />
	<soc code="19-4051" key="SOC2010.19-4051" title="Nuclear Technicians" />
	<soc code="19-4061" key="SOC2010.19-4061" title="Social Science Research Assistants" />
	<soc code="19-4091" key="SOC2010.19-4091" title="Environmental Science and Protection Technicians, Including Health" />
	<soc code="19-4092" key="SOC2010.19-4092" title="Forensic Science Technicians" />
	<soc code="19-4093" key="SOC2010.19-4093" title="Forest and Conservation Technicians" />
	<soc code="19-4099" key="SOC2010.19-4099" title="Life, Physical, and Social Science Technicians, All Other" />
	<soc code="21-1011" key="SOC2010.21-1011" title="Substance Abuse and Behavioral Disorder Counselors" />
	<soc code="21-1012" key="SOC2010.21-1012" title="Educational, Guidance, School, and Vocational Counselors" />
	<soc code="21-1013" key="SOC2010.21-1013" title="Marriage and Family Therapists" />
	<soc code="21-1014" key="SOC2010.21-1014" title="Mental Health Counselors" />
	<soc code="21-1015" key="SOC2010.21-1015" title="Rehabilitation Counselors" />
	<soc code="21-1019" key="SOC2010.21-1019" title="Counselors, All Other" />
	<soc code="21-1021" key="SOC2010.21-1021" title="Child, Family, and School Social Workers" />
	<soc code="21-1022" key="SOC2010.21-1022" title="Healthcare Social Workers" />
	<soc code="21-1023" key="SOC2010.21-1023" title="Mental Health and Substance Abuse Social Workers" />
	<soc code="21-1029" key="SOC2010.21-1029" title="Social Workers, All Other" />
	<soc code="21-1091" key="SOC2010.21-1091" title="Health Educators" />
	<soc code="21-1092" key="SOC2010.21-1092" title="Probation Officers and Correctional Treatment Specialists" />
	<soc code="21-1093" key="SOC2010.21-1093" title="Social and Human Service Assistants" />
	<soc code="21-1094" key="SOC2010.21-1094" title="Community Health Workers" />
	<soc code="21-1099" key="SOC2010.21-1099" title="Community and Social Service Specialists, All Other" />
	<soc code="21-2011" key="SOC2010.21-2011" title="Clergy" />
	<soc code="21-2021" key="SOC2010.21-2021" title="Directors, Religious Activities and Education" />
	<soc code="21-2099" key="SOC2010.21-2099" title="Religious Workers, All Other" />
	<soc code="23-1011" key="SOC2010.23-1011" title="Lawyers" />
	<soc code="23-1012" key="SOC2010.23-1012" title="Judicial Law Clerks" />
	<soc code="23-1021" key="SOC2010.23-1021" title="Administrative Law Judges, Adjudicators, and Hearing Officers" />
	<soc code="23-1022" key="SOC2010.23-1022" title="Arbitrators, Mediators, and Conciliators" />
	<soc code="23-1023" key="SOC2010.23-1023" title="Judges, Magistrate Judges, and Magistrates" />
	<soc code="23-2011" key="SOC2010.23-2011" title="Paralegals and Legal Assistants" />
	<soc code="23-2091" key="SOC2010.23-2091" title="Court Reporters" />
	<soc code="23-2093" key="SOC2010.23-2093" title="Title Examiners, Abstractors, and Searchers" />
	<soc code="23-2099" key="SOC2010.23-2099" title="Legal Support Workers, All Other" />
	<soc code="25-1011" key="SOC2010.25-1011" title="Business Teachers, Postsecondary" />
	<soc code="25-1021" key="SOC2010.25-1021" title="Computer Science Teachers, Postsecondary" />
	<soc code="25-1022" key="SOC2010.25-1022" title="Mathematical Science Teachers, Postsecondary" />
	<soc code="25-1031" key="SOC2010.25-1031" title="Architecture Teachers, Postsecondary" />
	<soc code="25-1032" key="SOC2010.25-1032" title="Engineering Teachers, Postsecondary" />
	<soc code="25-1041" key="SOC2010.25-1041" title="Agricultural Sciences Teachers, Postsecondary" />
	<soc code="25-1042" key="SOC2010.25-1042" title="Biological Science Teachers, Postsecondary" />
	<soc code="25-1043" key="SOC2010.25-1043" title="Forestry and Conservation Science Teachers, Postsecondary" />
	<soc code="25-1051" key="SOC2010.25-1051" title="Atmospheric, Earth, Marine, and Space Sciences Teachers, Postsecondary" />
	<soc code="25-1052" key="SOC2010.25-1052" title="Chemistry Teachers, Postsecondary" />
	<soc code="25-1053" key="SOC2010.25-1053" title="Environmental Science Teachers, Postsecondary" />
	<soc code="25-1054" key="SOC2010.25-1054" title="Physics Teachers, Postsecondary" />
	<soc code="25-1061" key="SOC2010.25-1061" title="Anthropology and Archeology Teachers, Postsecondary" />
	<soc code="25-1062" key="SOC2010.25-1062" title="Area, Ethnic, and Cultural Studies Teachers, Postsecondary" />
	<soc code="25-1063" key="SOC2010.25-1063" title="Economics Teachers, Postsecondary" />
	<soc code="25-1064" key="SOC2010.25-1064" title="Geography Teachers, Postsecondary" />
	<soc code="25-1065" key="SOC2010.25-1065" title="Political Science Teachers, Postsecondary" />
	<soc code="25-1066" key="SOC2010.25-1066" title="Psychology Teachers, Postsecondary" />
	<soc code="25-1067" key="SOC2010.25-1067" title="Sociology Teachers, Postsecondary" />
	<soc code="25-1069" key="SOC2010.25-1069" title="Social Sciences Teachers, Postsecondary, All Other" />
	<soc code="25-1071" key="SOC2010.25-1071" title="Health Specialties Teachers, Postsecondary" />
	<soc code="25-1072" key="SOC2010.25-1072" title="Nursing Instructors and Teachers, Postsecondary" />
	<soc code="25-1081" key="SOC2010.25-1081" title="Education Teachers, Postsecondary" />
	<soc code="25-1082" key="SOC2010.25-1082" title="Library Science Teachers, Postsecondary" />
	<soc code="25-1111" key="SOC2010.25-1111" title="Criminal Justice and Law Enforcement Teachers, Postsecondary" />
	<soc code="25-1112" key="SOC2010.25-1112" title="Law Teachers, Postsecondary" />
	<soc code="25-1113" key="SOC2010.25-1113" title="Social Work Teachers, Postsecondary" />
	<soc code="25-1121" key="SOC2010.25-1121" title="Art, Drama, and Music Teachers, Postsecondary" />
	<soc code="25-1122" key="SOC2010.25-1122" title="Communications Teachers, Postsecondary" />
	<soc code="25-1123" key="SOC2010.25-1123" title="English Language and Literature Teachers, Postsecondary" />
	<soc code="25-1124" key="SOC2010.25-1124" title="Foreign Language and Literature Teachers, Postsecondary" />
	<soc code="25-1125" key="SOC2010.25-1125" title="History Teachers, Postsecondary" />
	<soc code="25-1126" key="SOC2010.25-1126" title="Philosophy and Religion Teachers, Postsecondary" />
	<soc code="25-1191" key="SOC2010.25-1191" title="Graduate Teaching Assistants" />
	<soc code="25-1192" key="SOC2010.25-1192" title="Home Economics Teachers, Postsecondary" />
	<soc code="25-1193" key="SOC2010.25-1193" title="Recreation and Fitness Studies Teachers, Postsecondary" />
	<soc code="25-1194" key="SOC2010.25-1194" title="Vocational Education Teachers, Postsecondary" />
	<soc code="25-1199" key="SOC2010.25-1199" title="Postsecondary Teachers, All Other" />
	<soc code="25-2011" key="SOC2010.25-2011" title="Preschool Teachers, Except Special Education" />
	<soc code="25-2012" key="SOC2010.25-2012" title="Kindergarten Teachers, Except Special Education" />
	<soc code="25-2021" key="SOC2010.25-2021" title="Elementary School Teachers, Except Special Education" />
	<soc code="25-2022" key="SOC2010.25-2022" title="Middle School Teachers, Except Special and Career/Technical Education" />
	<soc code="25-2023" key="SOC2010.25-2023" title="Career/Technical Education Teachers, Middle School" />
	<soc code="25-2031" key="SOC2010.25-2031" title="Secondary School Teachers, Except Special and Career/Technical Education" />
	<soc code="25-2032" key="SOC2010.25-2032" title="Career/Technical Education Teachers, Secondary School" />
	<soc code="25-2051" key="SOC2010.25-2051" title="Special Education Teachers, Preschool" />
	<soc code="25-2052" key="SOC2010.25-2052" title="Special Education Teachers, Kindergarten and Elementary School" />
	<soc code="25-2053" key="SOC2010.25-2053" title="Special Education Teachers, Middle School" />
	<soc code="25-2054" key="SOC2010.25-2054" title="Special Education Teachers, Secondary School" />
	<soc code="25-2059" key="SOC2010.25-2059" title="Special Education Teachers, All Other" />
	<soc code="25-3011" key="SOC2010.25-3011" title="Adult Basic and Secondary Education and Literacy Teachers and Instructors" />
	<soc code="25-3021" key="SOC2010.25-3021" title="Self-Enrichment Education Teachers" />
	<soc code="25-3099" key="SOC2010.25-3099" title="Teachers and Instructors, All Other" />
	<soc code="25-4011" key="SOC2010.25-4011" title="Archivists" />
	<soc code="25-4012" key="SOC2010.25-4012" title="Curators" />
	<soc code="25-4013" key="SOC2010.25-4013" title="Museum Technicians and Conservators" />
	<soc code="25-4021" key="SOC2010.25-4021" title="Librarians" />
	<soc code="25-4031" key="SOC2010.25-4031" title="Library Technicians" />
	<soc code="25-9011" key="SOC2010.25-9011" title="Audio-Visual and Multimedia Collections Specialists" />
	<soc code="25-9021" key="SOC2010.25-9021" title="Farm and Home Management Advisors" />
	<soc code="25-9031" key="SOC2010.25-9031" title="Instructional Coordinators" />
	<soc code="25-9041" key="SOC2010.25-9041" title="Teacher Assistants" />
	<soc code="25-9099" key="SOC2010.25-9099" title="Education, Training, and Library Workers, All Other" />
	<soc code="27-1011" key="SOC2010.27-1011" title="Art Directors" />
	<soc code="27-1012" key="SOC2010.27-1012" title="Craft Artists" />
	<soc code="27-1013" key="SOC2010.27-1013" title="Fine Artists, Including Painters, Sculptors, and Illustrators" />
	<soc code="27-1014" key="SOC2010.27-1014" title="Multimedia Artists and Animators" />
	<soc code="27-1019" key="SOC2010.27-1019" title="Artists and Related Workers, All Other" />
	<soc code="27-1021" key="SOC2010.27-1021" title="Commercial and Industrial Designers" />
	<soc code="27-1022" key="SOC2010.27-1022" title="Fashion Designers" />
	<soc code="27-1023" key="SOC2010.27-1023" title="Floral Designers" />
	<soc code="27-1024" key="SOC2010.27-1024" title="Graphic Designers" />
	<soc code="27-1025" key="SOC2010.27-1025" title="Interior Designers" />
	<soc code="27-1026" key="SOC2010.27-1026" title="Merchandise Displayers and Window Trimmers" />
	<soc code="27-1027" key="SOC2010.27-1027" title="Set and Exhibit Designers" />
	<soc code="27-1029" key="SOC2010.27-1029" title="Designers, All Other" />
	<soc code="27-2011" key="SOC2010.27-2011" title="Actors" />
	<soc code="27-2012" key="SOC2010.27-2012" title="Producers and Directors" />
	<soc code="27-2021" key="SOC2010.27-2021" title="Athletes and Sports Competitors" />
	<soc code="27-2022" key="SOC2010.27-2022" title="Coaches and Scouts" />
	<soc code="27-2023" key="SOC2010.27-2023" title="Umpires, Referees, and Other Sports Officials" />
	<soc code="27-2031" key="SOC2010.27-2031" title="Dancers" />
	<soc code="27-2032" key="SOC2010.27-2032" title="Choreographers" />
	<soc code="27-2041" key="SOC2010.27-2041" title="Music Directors and Composers" />
	<soc code="27-2042" key="SOC2010.27-2042" title="Musicians and Singers" />
	<soc code="27-2099" key="SOC2010.27-2099" title="Entertainers and Performers, Sports and Related Workers, All Other" />
	<soc code="27-3011" key="SOC2010.27-3011" title="Radio and Television Announcers" />
	<soc code="27-3012" key="SOC2010.27-3012" title="Public Address System and Other Announcers" />
	<soc code="27-3021" key="SOC2010.27-3021" title="Broadcast News Analysts" />
	<soc code="27-3022" key="SOC2010.27-3022" title="Reporters and Correspondents" />
	<soc code="27-3031" key="SOC2010.27-3031" title="Public Relations Specialists" />
	<soc code="27-3041" key="SOC2010.27-3041" title="Editors" />
	<soc code="27-3042" key="SOC2010.27-3042" title="Technical Writers" />
	<soc code="27-3043" key="SOC2010.27-3043" title="Writers and Authors" />
	<soc code="27-3091" key="SOC2010.27-3091" title="Interpreters and Translators" />
	<soc code="27-3099" key="SOC2010.27-3099" title="Media and Communication Workers, All Other" />
	<soc code="27-4011" key="SOC2010.27-4011" title="Audio and Video Equipment Technicians" />
	<soc code="27-4012" key="SOC2010.27-4012" title="Broadcast Technicians" />
	<soc code="27-4013" key="SOC2010.27-4013" title="Radio Operators" />
	<soc code="27-4014" key="SOC2010.27-4014" title="Sound Engineering Technicians" />
	<soc code="27-4021" key="SOC2010.27-4021" title="Photographers" />
	<soc code="27-4031" key="SOC2010.27-4031" title="Camera Operators, Television, Video, and Motion Picture" />
	<soc code="27-4032" key="SOC2010.27-4032" title="Film and Video Editors" />
	<soc code="27-4099" key="SOC2010.27-4099" title="Media and Communication Equipment Workers, All Other" />
	<soc code="29-1011" key="SOC2010.29-1011" title="Chiropractors" />
	<soc code="29-1021" key="SOC2010.29-1021" title="Dentists, General" />
	<soc code="29-1022" key="SOC2010.29-1022" title="Oral and Maxillofacial Surgeons" />
	<soc code="29-1023" key="SOC2010.29-1023" title="Orthodontists" />
	<soc code="29-1024" key="SOC2010.29-1024" title="Prosthodontists" />
	<soc code="29-1029" key="SOC2010.29-1029" title="Dentists, All Other Specialists" />
	<soc code="29-1031" key="SOC2010.29-1031" title="Dietitians and Nutritionists" />
	<soc code="29-1041" key="SOC2010.29-1041" title="Optometrists" />
	<soc code="29-1051" key="SOC2010.29-1051" title="Pharmacists" />
	<soc code="29-1061" key="SOC2010.29-1061" title="Anesthesiologists" />
	<soc code="29-1062" key="SOC2010.29-1062" title="Family and General Practitioners" />
	<soc code="29-1063" key="SOC2010.29-1063" title="Internists, General" />
	<soc code="29-1064" key="SOC2010.29-1064" title="Obstetricians and Gynecologists" />
	<soc code="29-1065" key="SOC2010.29-1065" title="Pediatricians, General" />
	<soc code="29-1066" key="SOC2010.29-1066" title="Psychiatrists" />
	<soc code="29-1067" key="SOC2010.29-1067" title="Surgeons" />
	<soc code="29-1069" key="SOC2010.29-1069" title="Physicians and Surgeons, All Other" />
	<soc code="29-1071" key="SOC2010.29-1071" title="Physician Assistants" />
	<soc code="29-1081" key="SOC2010.29-1081" title="Podiatrists" />
	<soc code="29-1122" key="SOC2010.29-1122" title="Occupational Therapists" />
	<soc code="29-1123" key="SOC2010.29-1123" title="Physical Therapists" />
	<soc code="29-1124" key="SOC2010.29-1124" title="Radiation Therapists" />
	<soc code="29-1125" key="SOC2010.29-1125" title="Recreational Therapists" />
	<soc code="29-1126" key="SOC2010.29-1126" title="Respiratory Therapists" />
	<soc code="29-1127" key="SOC2010.29-1127" title="Speech-Language Pathologists" />
	<soc code="29-1128" key="SOC2010.29-1128" title="Exercise Physiologists" />
	<soc code="29-1129" key="SOC2010.29-1129" title="Therapists, All Other" />
	<soc code="29-1131" key="SOC2010.29-1131" title="Veterinarians" />
	<soc code="29-1141" key="SOC2010.29-1141" title="Registered Nurses" />
	<soc code="29-1151" key="SOC2010.29-1151" title="Nurse Anesthetists" />
	<soc code="29-1161" key="SOC2010.29-1161" title="Nurse Midwives" />
	<soc code="29-1171" key="SOC2010.29-1171" title="Nurse Practitioners" />
	<soc code="29-1181" key="SOC2010.29-1181" title="Audiologists" />
	<soc code="29-1199" key="SOC2010.29-1199" title="Health Diagnosing and Treating Practitioners, All Other" />
	<soc code="29-2011" key="SOC2010.29-2011" title="Medical and Clinical Laboratory Technologists" />
	<soc code="29-2012" key="SOC2010.29-2012" title="Medical and Clinical Laboratory Technicians" />
	<soc code="29-2021" key="SOC2010.29-2021" title="Dental Hygienists" />
	<soc code="29-2031" key="SOC2010.29-2031" title="Cardiovascular Technologists and Technicians" />
	<soc code="29-2032" key="SOC2010.29-2032" title="Diagnostic Medical Sonographers" />
	<soc code="29-2033" key="SOC2010.29-2033" title="Nuclear Medicine Technologists" />
	<soc code="29-2034" key="SOC2010.29-2034" title="Radiologic Technologists" />
	<soc code="29-2035" key="SOC2010.29-2035" title="Magnetic Resonance Imaging Technologists" />
	<soc code="29-2041" key="SOC2010.29-2041" title="Emergency Medical Technicians and Paramedics" />
	<soc code="29-2051" key="SOC2010.29-2051" title="Dietetic Technicians" />
	<soc code="29-2052" key="SOC2010.29-2052" title="Pharmacy Technicians" />
	<soc code="29-2053" key="SOC2010.29-2053" title="Psychiatric Technicians" />
	<soc code="29-2054" key="SOC2010.29-2054" title="Respiratory Therapy Technicians" />
	<soc code="29-2055" key="SOC2010.29-2055" title="Surgical Technologists" />
	<soc code="29-2056" key="SOC2010.29-2056" title="Veterinary Technologists and Technicians" />
	<soc code="29-2057" key="SOC2010.29-2057" title="Ophthalmic Medical Technicians" />
	<soc code="29-2061" key="SOC2010.29-2061" title="Licensed Practical and Licensed Vocational Nurses" />
	<soc code="29-2071" key="SOC2010.29-2071" title="Medical Records and Health Information Technicians" />
	<soc code="29-2081" key="SOC2010.29-2081" title="Opticians, Dispensing" />
	<soc code="29-2091" key="SOC2010.29-2091" title="Orthotists and Prosthetists" />
	<soc code="29-2092" key="SOC2010.29-2092" title="Hearing Aid Specialists" />
	<soc code="29-2099" key="SOC2010.29-2099" title="Health Technologists and Technicians, All Other" />
	<soc code="29-9011" key="SOC2010.29-9011" title="Occupational Health and Safety Specialists" />
	<soc code="29-9012" key="SOC2010.29-9012" title="Occupational Health and Safety Technicians" />
	<soc code="29-9091" key="SOC2010.29-9091" title="Athletic Trainers" />
	<soc code="29-9092" key="SOC2010.29-9092" title="Genetic Counselors" />
	<soc code="29-9099" key="SOC2010.29-9099" title="Healthcare Practitioners and Technical Workers, All Other" />
	<soc code="31-1011" key="SOC2010.31-1011" title="Home Health Aides" />
	<soc code="31-1013" key="SOC2010.31-1013" title="Psychiatric Aides" />
	<soc code="31-1014" key="SOC2010.31-1014" title="Nursing Assistants" />
	<soc code="31-1015" key="SOC2010.31-1015" title="Orderlies" />
	<soc code="31-2011" key="SOC2010.31-2011" title="Occupational Therapy Assistants" />
	<soc code="31-2012" key="SOC2010.31-2012" title="Occupational Therapy Aides" />
	<soc code="31-2021" key="SOC2010.31-2021" title="Physical Therapist Assistants" />
	<soc code="31-2022" key="SOC2010.31-2022" title="Physical Therapist Aides" />
	<soc code="31-9011" key="SOC2010.31-9011" title="Massage Therapists" />
	<soc code="31-9091" key="SOC2010.31-9091" title="Dental Assistants" />
	<soc code="31-9092" key="SOC2010.31-9092" title="Medical Assistants" />
	<soc code="31-9093" key="SOC2010.31-9093" title="Medical Equipment Preparers" />
	<soc code="31-9094" key="SOC2010.31-9094" title="Medical Transcriptionists" />
	<soc code="31-9095" key="SOC2010.31-9095" title="Pharmacy Aides" />
	<soc code="31-9096" key="SOC2010.31-9096" title="Veterinary Assistants and Laboratory Animal Caretakers" />
	<soc code="31-9097" key="SOC2010.31-9097" title="Phlebotomists" />
	<soc code="31-9099" key="SOC2010.31-9099" title="Healthcare Support Workers, All Other" />
	<soc code="33-1011" key="SOC2010.33-1011" title="First-Line Supervisors of Correctional Officers" />
	<soc code="33-1012" key="SOC2010.33-1012" title="First-Line Supervisors of Police and Detectives" />
	<soc code="33-1021" key="SOC2010.33-1021" title="First-Line Supervisors of Fire Fighting and Prevention Workers" />
	<soc code="33-1099" key="SOC2010.33-1099" title="First-Line Supervisors of Protective Service Workers, All Other" />
	<soc code="33-2011" key="SOC2010.33-2011" title="Firefighters" />
	<soc code="33-2021" key="SOC2010.33-2021" title="Fire Inspectors and Investigators" />
	<soc code="33-2022" key="SOC2010.33-2022" title="Forest Fire Inspectors and Prevention Specialists" />
	<soc code="33-3011" key="SOC2010.33-3011" title="Bailiffs" />
	<soc code="33-3012" key="SOC2010.33-3012" title="Correctional Officers and Jailers" />
	<soc code="33-3021" key="SOC2010.33-3021" title="Detectives and Criminal Investigators" />
	<soc code="33-3031" key="SOC2010.33-3031" title="Fish and Game Wardens" />
	<soc code="33-3041" key="SOC2010.33-3041" title="Parking Enforcement Workers" />
	<soc code="33-3051" key="SOC2010.33-3051" title="Police and Sheriff''s Patrol Officers" />
	<soc code="33-3052" key="SOC2010.33-3052" title="Transit and Railroad Police" />
	<soc code="33-9011" key="SOC2010.33-9011" title="Animal Control Workers" />
	<soc code="33-9021" key="SOC2010.33-9021" title="Private Detectives and Investigators" />
	<soc code="33-9031" key="SOC2010.33-9031" title="Gaming Surveillance Officers and Gaming Investigators" />
	<soc code="33-9032" key="SOC2010.33-9032" title="Security Guards" />
	<soc code="33-9091" key="SOC2010.33-9091" title="Crossing Guards" />
	<soc code="33-9092" key="SOC2010.33-9092" title="Lifeguards, Ski Patrol, and Other Recreational Protective Service" />
	<soc code="33-9093" key="SOC2010.33-9093" title="Transportation Security Screeners" />
	<soc code="33-9099" key="SOC2010.33-9099" title="Protective Service Workers, All Other" />
	<soc code="35-1011" key="SOC2010.35-1011" title="Chefs and Head Cooks" />
	<soc code="35-1012" key="SOC2010.35-1012" title="First-Line Supervisors of Food Preparation and Serving Workers" />
	<soc code="35-2011" key="SOC2010.35-2011" title="Cooks, Fast Food" />
	<soc code="35-2012" key="SOC2010.35-2012" title="Cooks, Institution and Cafeteria" />
	<soc code="35-2013" key="SOC2010.35-2013" title="Cooks, Private Household" />
	<soc code="35-2014" key="SOC2010.35-2014" title="Cooks, Restaurant" />
	<soc code="35-2015" key="SOC2010.35-2015" title="Cooks, Short Order" />
	<soc code="35-2019" key="SOC2010.35-2019" title="Cooks, All Other" />
	<soc code="35-2021" key="SOC2010.35-2021" title="Food Preparation Workers" />
	<soc code="35-3011" key="SOC2010.35-3011" title="Bartenders" />
	<soc code="35-3021" key="SOC2010.35-3021" title="Combined Food Preparation and Serving Workers, Including Fast Food" />
	<soc code="35-3022" key="SOC2010.35-3022" title="Counter Attendants, Cafeteria, Food Concession, and Coffee Shop" />
	<soc code="35-3031" key="SOC2010.35-3031" title="Waiters and Waitresses" />
	<soc code="35-3041" key="SOC2010.35-3041" title="Food Servers, Nonrestaurant" />
	<soc code="35-9011" key="SOC2010.35-9011" title="Dining Room and Cafeteria Attendants and Bartender Helpers" />
	<soc code="35-9021" key="SOC2010.35-9021" title="Dishwashers" />
	<soc code="35-9031" key="SOC2010.35-9031" title="Hosts and Hostesses, Restaurant, Lounge, and Coffee Shop" />
	<soc code="35-9099" key="SOC2010.35-9099" title="Food Preparation and Serving Related Workers, All Other" />
	<soc code="37-1011" key="SOC2010.37-1011" title="First-Line Supervisors of Housekeeping and Janitorial Workers" />
	<soc code="37-1012" key="SOC2010.37-1012" title="First-Line Supervisors of Landscaping, Lawn Service, and Groundskeeping Workers" />
	<soc code="37-2011" key="SOC2010.37-2011" title="Janitors and Cleaners, Except Maids and Housekeeping Cleaners" />
	<soc code="37-2012" key="SOC2010.37-2012" title="Maids and Housekeeping Cleaners" />
	<soc code="37-2019" key="SOC2010.37-2019" title="Building Cleaning Workers, All Other" />
	<soc code="37-2021" key="SOC2010.37-2021" title="Pest Control Workers" />
	<soc code="37-3011" key="SOC2010.37-3011" title="Landscaping and Groundskeeping Workers" />
	<soc code="37-3012" key="SOC2010.37-3012" title="Pesticide Handlers, Sprayers, and Applicators, Vegetation" />
	<soc code="37-3013" key="SOC2010.37-3013" title="Tree Trimmers and Pruners" />
	<soc code="37-3019" key="SOC2010.37-3019" title="Grounds Maintenance Workers, All Other" />
	<soc code="39-1011" key="SOC2010.39-1011" title="Gaming Supervisors" />
	<soc code="39-1012" key="SOC2010.39-1012" title="Slot Supervisors" />
	<soc code="39-1021" key="SOC2010.39-1021" title="First-Line Supervisors of Personal Service Workers" />
	<soc code="39-2011" key="SOC2010.39-2011" title="Animal Trainers" />
	<soc code="39-2021" key="SOC2010.39-2021" title="Nonfarm Animal Caretakers" />
	<soc code="39-3011" key="SOC2010.39-3011" title="Gaming Dealers" />
	<soc code="39-3012" key="SOC2010.39-3012" title="Gaming and Sports Book Writers and Runners" />
	<soc code="39-3019" key="SOC2010.39-3019" title="Gaming Service Workers, All Other" />
	<soc code="39-3021" key="SOC2010.39-3021" title="Motion Picture Projectionists" />
	<soc code="39-3031" key="SOC2010.39-3031" title="Ushers, Lobby Attendants, and Ticket Takers" />
	<soc code="39-3091" key="SOC2010.39-3091" title="Amusement and Recreation Attendants" />
	<soc code="39-3092" key="SOC2010.39-3092" title="Costume Attendants" />
	<soc code="39-3093" key="SOC2010.39-3093" title="Locker Room, Coatroom, and Dressing Room Attendants" />
	<soc code="39-3099" key="SOC2010.39-3099" title="Entertainment Attendants and Related Workers, All Other" />
	<soc code="39-4011" key="SOC2010.39-4011" title="Embalmers" />
	<soc code="39-4021" key="SOC2010.39-4021" title="Funeral Attendants" />
	<soc code="39-4031" key="SOC2010.39-4031" title="Morticians, Undertakers, and Funeral Directors" />
	<soc code="39-5011" key="SOC2010.39-5011" title="Barbers" />
	<soc code="39-5012" key="SOC2010.39-5012" title="Hairdressers, Hairstylists, and Cosmetologists" />
	<soc code="39-5091" key="SOC2010.39-5091" title="Makeup Artists, Theatrical and Performance" />
	<soc code="39-5092" key="SOC2010.39-5092" title="Manicurists and Pedicurists" />
	<soc code="39-5093" key="SOC2010.39-5093" title="Shampooers" />
	<soc code="39-5094" key="SOC2010.39-5094" title="Skincare Specialists" />
	<soc code="39-6011" key="SOC2010.39-6011" title="Baggage Porters and Bellhops" />
	<soc code="39-6012" key="SOC2010.39-6012" title="Concierges" />
	<soc code="39-7011" key="SOC2010.39-7011" title="Tour Guides and Escorts" />
	<soc code="39-7012" key="SOC2010.39-7012" title="Travel Guides" />
	<soc code="39-9011" key="SOC2010.39-9011" title="Childcare Workers" />
	<soc code="39-9021" key="SOC2010.39-9021" title="Personal Care Aides" />
	<soc code="39-9031" key="SOC2010.39-9031" title="Fitness Trainers and Aerobics Instructors" />
	<soc code="39-9032" key="SOC2010.39-9032" title="Recreation Workers" />
	<soc code="39-9041" key="SOC2010.39-9041" title="Residential Advisors" />
	<soc code="39-9099" key="SOC2010.39-9099" title="Personal Care and Service Workers, All Other" />
	<soc code="41-1011" key="SOC2010.41-1011" title="First-Line Supervisors of Retail Sales Workers" />
	<soc code="41-1012" key="SOC2010.41-1012" title="First-Line Supervisors of Non-Retail Sales Workers" />
	<soc code="41-2011" key="SOC2010.41-2011" title="Cashiers" />
	<soc code="41-2012" key="SOC2010.41-2012" title="Gaming Change Persons and Booth Cashiers" />
	<soc code="41-2021" key="SOC2010.41-2021" title="Counter and Rental Clerks" />
	<soc code="41-2022" key="SOC2010.41-2022" title="Parts Salespersons" />
	<soc code="41-2031" key="SOC2010.41-2031" title="Retail Salespersons" />
	<soc code="41-3011" key="SOC2010.41-3011" title="Advertising Sales Agents" />
	<soc code="41-3021" key="SOC2010.41-3021" title="Insurance Sales Agents" />
	<soc code="41-3031" key="SOC2010.41-3031" title="Securities, Commodities, and Financial Services Sales Agents" />
	<soc code="41-3041" key="SOC2010.41-3041" title="Travel Agents" />
	<soc code="41-3099" key="SOC2010.41-3099" title="Sales Representatives, Services, All Other" />
	<soc code="41-4011" key="SOC2010.41-4011" title="Sales Representatives, Wholesale and Manufacturing, Technical and Scientific Products" />
	<soc code="41-4012" key="SOC2010.41-4012" title="Sales Representatives, Wholesale and Manufacturing, Except Technical and Scientific Products" />
	<soc code="41-9011" key="SOC2010.41-9011" title="Demonstrators and Product Promoters" />
	<soc code="41-9012" key="SOC2010.41-9012" title="Models" />
	<soc code="41-9021" key="SOC2010.41-9021" title="Real Estate Brokers" />
	<soc code="41-9022" key="SOC2010.41-9022" title="Real Estate Sales Agents" />
	<soc code="41-9031" key="SOC2010.41-9031" title="Sales Engineers" />
	<soc code="41-9041" key="SOC2010.41-9041" title="Telemarketers" />
	<soc code="41-9091" key="SOC2010.41-9091" title="Door-to-Door Sales Workers, News and Street Vendors, and Related Workers" />
	<soc code="41-9099" key="SOC2010.41-9099" title="Sales and Related Workers, All Other" />
	<soc code="43-1011" key="SOC2010.43-1011" title="First-Line Supervisors of Office and Administrative Support Workers" />
	<soc code="43-2011" key="SOC2010.43-2011" title="Switchboard Operators, Including Answering Service" />
	<soc code="43-2021" key="SOC2010.43-2021" title="Telephone Operators" />
	<soc code="43-2099" key="SOC2010.43-2099" title="Communications Equipment Operators, All Other" />
	<soc code="43-3011" key="SOC2010.43-3011" title="Bill and Account Collectors" />
	<soc code="43-3021" key="SOC2010.43-3021" title="Billing and Posting Clerks" />
	<soc code="43-3031" key="SOC2010.43-3031" title="Bookkeeping, Accounting, and Auditing Clerks" />
	<soc code="43-3041" key="SOC2010.43-3041" title="Gaming Cage Workers" />
	<soc code="43-3051" key="SOC2010.43-3051" title="Payroll and Timekeeping Clerks" />
	<soc code="43-3061" key="SOC2010.43-3061" title="Procurement Clerks" />
	<soc code="43-3071" key="SOC2010.43-3071" title="Tellers" />
	<soc code="43-3099" key="SOC2010.43-3099" title="Financial Clerks, All Other" />
	<soc code="43-4011" key="SOC2010.43-4011" title="Brokerage Clerks" />
	<soc code="43-4021" key="SOC2010.43-4021" title="Correspondence Clerks" />
	<soc code="43-4031" key="SOC2010.43-4031" title="Court, Municipal, and License Clerks" />
	<soc code="43-4041" key="SOC2010.43-4041" title="Credit Authorizers, Checkers, and Clerks" />
	<soc code="43-4051" key="SOC2010.43-4051" title="Customer Service Representatives" />
	<soc code="43-4061" key="SOC2010.43-4061" title="Eligibility Interviewers, Government Programs" />
	<soc code="43-4071" key="SOC2010.43-4071" title="File Clerks" />
	<soc code="43-4081" key="SOC2010.43-4081" title="Hotel, Motel, and Resort Desk Clerks" />
	<soc code="43-4111" key="SOC2010.43-4111" title="Interviewers, Except Eligibility and Loan" />
	<soc code="43-4121" key="SOC2010.43-4121" title="Library Assistants, Clerical" />
	<soc code="43-4131" key="SOC2010.43-4131" title="Loan Interviewers and Clerks" />
	<soc code="43-4141" key="SOC2010.43-4141" title="New Accounts Clerks" />
	<soc code="43-4151" key="SOC2010.43-4151" title="Order Clerks" />
	<soc code="43-4161" key="SOC2010.43-4161" title="Human Resources Assistants, Except Payroll and Timekeeping" />
	<soc code="43-4171" key="SOC2010.43-4171" title="Receptionists and Information Clerks" />
	<soc code="43-4181" key="SOC2010.43-4181" title="Reservation and Transportation Ticket Agents and Travel Clerks" />
	<soc code="43-4199" key="SOC2010.43-4199" title="Information and Record Clerks, All Other" />
	<soc code="43-5011" key="SOC2010.43-5011" title="Cargo and Freight Agents" />
	<soc code="43-5021" key="SOC2010.43-5021" title="Couriers and Messengers" />
	<soc code="43-5031" key="SOC2010.43-5031" title="Police, Fire, and Ambulance Dispatchers" />
	<soc code="43-5032" key="SOC2010.43-5032" title="Dispatchers, Except Police, Fire, and Ambulance" />
	<soc code="43-5041" key="SOC2010.43-5041" title="Meter Readers, Utilities" />
	<soc code="43-5051" key="SOC2010.43-5051" title="Postal Service Clerks" />
	<soc code="43-5052" key="SOC2010.43-5052" title="Postal Service Mail Carriers" />
	<soc code="43-5053" key="SOC2010.43-5053" title="Postal Service Mail Sorters, Processors, and Processing Machine Operators" />
	<soc code="43-5061" key="SOC2010.43-5061" title="Production, Planning, and Expediting Clerks" />
	<soc code="43-5071" key="SOC2010.43-5071" title="Shipping, Receiving, and Traffic Clerks" />
	<soc code="43-5081" key="SOC2010.43-5081" title="Stock Clerks and Order Fillers" />
	<soc code="43-5111" key="SOC2010.43-5111" title="Weighers, Measurers, Checkers, and Samplers, Recordkeeping" />
	<soc code="43-6011" key="SOC2010.43-6011" title="Executive Secretaries and Executive Administrative Assistants" />
	<soc code="43-6012" key="SOC2010.43-6012" title="Legal Secretaries" />
	<soc code="43-6013" key="SOC2010.43-6013" title="Medical Secretaries" />
	<soc code="43-6014" key="SOC2010.43-6014" title="Secretaries and Administrative Assistants, Except Legal, Medical, and Executive" />
	<soc code="43-9011" key="SOC2010.43-9011" title="Computer Operators" />
	<soc code="43-9021" key="SOC2010.43-9021" title="Data Entry Keyers" />
	<soc code="43-9022" key="SOC2010.43-9022" title="Word Processors and Typists" />
	<soc code="43-9031" key="SOC2010.43-9031" title="Desktop Publishers" />
	<soc code="43-9041" key="SOC2010.43-9041" title="Insurance Claims and Policy Processing Clerks" />
	<soc code="43-9051" key="SOC2010.43-9051" title="Mail Clerks and Mail Machine Operators, Except Postal Service" />
	<soc code="43-9061" key="SOC2010.43-9061" title="Office Clerks, General" />
	<soc code="43-9071" key="SOC2010.43-9071" title="Office Machine Operators, Except Computer" />
	<soc code="43-9081" key="SOC2010.43-9081" title="Proofreaders and Copy Markers" />
	<soc code="43-9111" key="SOC2010.43-9111" title="Statistical Assistants" />
	<soc code="43-9199" key="SOC2010.43-9199" title="Office and Administrative Support Workers, All Other" />
	<soc code="45-1011" key="SOC2010.45-1011" title="First-Line Supervisors of Farming, Fishing, and Forestry Workers" />
	<soc code="45-2011" key="SOC2010.45-2011" title="Agricultural Inspectors" />
	<soc code="45-2021" key="SOC2010.45-2021" title="Animal Breeders" />
	<soc code="45-2041" key="SOC2010.45-2041" title="Graders and Sorters, Agricultural Products" />
	<soc code="45-2091" key="SOC2010.45-2091" title="Agricultural Equipment Operators" />
	<soc code="45-2092" key="SOC2010.45-2092" title="Farmworkers and Laborers, Crop, Nursery, and Greenhouse" />
	<soc code="45-2093" key="SOC2010.45-2093" title="Farmworkers, Farm, Ranch, and Aquacultural Animals" />
	<soc code="45-2099" key="SOC2010.45-2099" title="Agricultural Workers, All Other" />
	<soc code="45-3011" key="SOC2010.45-3011" title="Fishers and Related Fishing Workers" />
	<soc code="45-3021" key="SOC2010.45-3021" title="Hunters and Trappers" />
	<soc code="45-4011" key="SOC2010.45-4011" title="Forest and Conservation Workers" />
	<soc code="45-4021" key="SOC2010.45-4021" title="Fallers" />
	<soc code="45-4022" key="SOC2010.45-4022" title="Logging Equipment Operators" />
	<soc code="45-4023" key="SOC2010.45-4023" title="Log Graders and Scalers" />
	<soc code="45-4029" key="SOC2010.45-4029" title="Logging Workers, All Other" />
	<soc code="47-1011" key="SOC2010.47-1011" title="Supervisors of Construction and Extraction Workers" />
	<soc code="47-2011" key="SOC2010.47-2011" title="Boilermakers" />
	<soc code="47-2021" key="SOC2010.47-2021" title="Brickmasons and Blockmasons" />
	<soc code="47-2022" key="SOC2010.47-2022" title="Stonemasons" />
	<soc code="47-2031" key="SOC2010.47-2031" title="Carpenters" />
	<soc code="47-2041" key="SOC2010.47-2041" title="Carpet Installers" />
	<soc code="47-2042" key="SOC2010.47-2042" title="Floor Layers, Except Carpet, Wood, and Hard Tiles" />
	<soc code="47-2043" key="SOC2010.47-2043" title="Floor Sanders and Finishers" />
	<soc code="47-2044" key="SOC2010.47-2044" title="Tile and Marble Setters" />
	<soc code="47-2051" key="SOC2010.47-2051" title="Cement Masons and Concrete Finishers" />
	<soc code="47-2053" key="SOC2010.47-2053" title="Terrazzo Workers and Finishers" />
	<soc code="47-2061" key="SOC2010.47-2061" title="Construction Laborers" />
	<soc code="47-2071" key="SOC2010.47-2071" title="Paving, Surfacing, and Tamping Equipment Operators" />
	<soc code="47-2072" key="SOC2010.47-2072" title="Pile-Driver Operators" />
	<soc code="47-2073" key="SOC2010.47-2073" title="Operating Engineers and Other Construction Equipment Operators" />
	<soc code="47-2081" key="SOC2010.47-2081" title="Drywall and Ceiling Tile Installers" />
	<soc code="47-2082" key="SOC2010.47-2082" title="Tapers" />
	<soc code="47-2111" key="SOC2010.47-2111" title="Electricians" />
	<soc code="47-2121" key="SOC2010.47-2121" title="Glaziers" />
	<soc code="47-2131" key="SOC2010.47-2131" title="Insulation Workers, Floor, Ceiling, and Wall" />
	<soc code="47-2132" key="SOC2010.47-2132" title="Insulation Workers, Mechanical" />
	<soc code="47-2141" key="SOC2010.47-2141" title="Painters, Construction and Maintenance" />
	<soc code="47-2142" key="SOC2010.47-2142" title="Paperhangers" />
	<soc code="47-2151" key="SOC2010.47-2151" title="Pipelayers" />
	<soc code="47-2152" key="SOC2010.47-2152" title="Plumbers, Pipefitters, and Steamfitters" />
	<soc code="47-2161" key="SOC2010.47-2161" title="Plasterers and Stucco Masons" />
	<soc code="47-2171" key="SOC2010.47-2171" title="Reinforcing Iron and Rebar Workers" />
	<soc code="47-2181" key="SOC2010.47-2181" title="Roofers" />
	<soc code="47-2211" key="SOC2010.47-2211" title="Sheet Metal Workers" />
	<soc code="47-2221" key="SOC2010.47-2221" title="Structural Iron and Steel Workers" />
	<soc code="47-2231" key="SOC2010.47-2231" title="Solar Photovoltaic Installers" />
	<soc code="47-3011" key="SOC2010.47-3011" title="Helpers--Brickmasons, Blockmasons, Stonemasons, and Tile and Marble Setters" />
	<soc code="47-3012" key="SOC2010.47-3012" title="Helpers--Carpenters" />
	<soc code="47-3013" key="SOC2010.47-3013" title="Helpers--Electricians" />
	<soc code="47-3014" key="SOC2010.47-3014" title="Helpers--Painters, Paperhangers, Plasterers, and Stucco Masons" />
	<soc code="47-3015" key="SOC2010.47-3015" title="Helpers--Pipelayers, Plumbers, Pipefitters, and Steamfitters" />
	<soc code="47-3016" key="SOC2010.47-3016" title="Helpers--Roofers" />
	<soc code="47-3019" key="SOC2010.47-3019" title="Helpers, Construction Trades, All Other" />
	<soc code="47-4011" key="SOC2010.47-4011" title="Construction and Building Inspectors" />
	<soc code="47-4021" key="SOC2010.47-4021" title="Elevator Installers and Repairers" />
	<soc code="47-4031" key="SOC2010.47-4031" title="Fence Erectors" />
	<soc code="47-4041" key="SOC2010.47-4041" title="Hazardous Materials Removal Workers" />
	<soc code="47-4051" key="SOC2010.47-4051" title="Highway Maintenance Workers" />
	<soc code="47-4061" key="SOC2010.47-4061" title="Rail-Track Laying and Maintenance Equipment Operators" />
	<soc code="47-4071" key="SOC2010.47-4071" title="Septic Tank Servicers and Sewer Pipe Cleaners" />
	<soc code="47-4091" key="SOC2010.47-4091" title="Segmental Pavers" />
	<soc code="47-4099" key="SOC2010.47-4099" title="Construction and Related Workers, All Other" />
	<soc code="47-5011" key="SOC2010.47-5011" title="Derrick Operators, Oil and Gas" />
	<soc code="47-5012" key="SOC2010.47-5012" title="Rotary Drill Operators, Oil and Gas" />
	<soc code="47-5013" key="SOC2010.47-5013" title="Service Unit Operators, Oil, Gas, and Mining" />
	<soc code="47-5021" key="SOC2010.47-5021" title="Earth Drillers, Except Oil and Gas" />
	<soc code="47-5031" key="SOC2010.47-5031" title="Explosives Workers, Ordnance Handling Experts, and Blasters" />
	<soc code="47-5041" key="SOC2010.47-5041" title="Continuous Mining Machine Operators" />
	<soc code="47-5042" key="SOC2010.47-5042" title="Mine Cutting and Channeling Machine Operators" />
	<soc code="47-5049" key="SOC2010.47-5049" title="Mining Machine Operators, All Other" />
	<soc code="47-5051" key="SOC2010.47-5051" title="Rock Splitters, Quarry" />
	<soc code="47-5061" key="SOC2010.47-5061" title="Roof Bolters, Mining" />
	<soc code="47-5071" key="SOC2010.47-5071" title="Roustabouts, Oil and Gas" />
	<soc code="47-5081" key="SOC2010.47-5081" title="Helpers--Extraction Workers" />
	<soc code="47-5099" key="SOC2010.47-5099" title="Extraction Workers, All Other" />
	<soc code="49-1011" key="SOC2010.49-1011" title="First-Line Supervisors of Mechanics, Installers, and Repairers" />
	<soc code="49-2011" key="SOC2010.49-2011" title="Computer, Automated Teller, and Office Machine Repairers" />
	<soc code="49-2021" key="SOC2010.49-2021" title="Radio, Cellular, and Tower Equipment Installers and Repairers" />
	<soc code="49-2022" key="SOC2010.49-2022" title="Telecommunications Equipment Installers and Repairers, Except Line Installers" />
	<soc code="49-2091" key="SOC2010.49-2091" title="Avionics Technicians" />
	<soc code="49-2092" key="SOC2010.49-2092" title="Electric Motor, Power Tool, and Related Repairers" />
	<soc code="49-2093" key="SOC2010.49-2093" title="Electrical and Electronics Installers and Repairers, Transportation Equipment" />
	<soc code="49-2094" key="SOC2010.49-2094" title="Electrical and Electronics Repairers, Commercial and Industrial Equipment" />
	<soc code="49-2095" key="SOC2010.49-2095" title="Electrical and Electronics Repairers, Powerhouse, Substation, and Relay" />
	<soc code="49-2096" key="SOC2010.49-2096" title="Electronic Equipment Installers and Repairers, Motor Vehicles" />
	<soc code="49-2097" key="SOC2010.49-2097" title="Electronic Home Entertainment Equipment Installers and Repairers" />
	<soc code="49-2098" key="SOC2010.49-2098" title="Security and Fire Alarm Systems Installers" />
	<soc code="49-3011" key="SOC2010.49-3011" title="Aircraft Mechanics and Service Technicians" />
	<soc code="49-3021" key="SOC2010.49-3021" title="Automotive Body and Related Repairers" />
	<soc code="49-3022" key="SOC2010.49-3022" title="Automotive Glass Installers and Repairers" />
	<soc code="49-3023" key="SOC2010.49-3023" title="Automotive Service Technicians and Mechanics" />
	<soc code="49-3031" key="SOC2010.49-3031" title="Bus and Truck Mechanics and Diesel Engine Specialists" />
	<soc code="49-3041" key="SOC2010.49-3041" title="Farm Equipment Mechanics and Service Technicians" />
	<soc code="49-3042" key="SOC2010.49-3042" title="Mobile Heavy Equipment Mechanics, Except Engines" />
	<soc code="49-3043" key="SOC2010.49-3043" title="Rail Car Repairers" />
	<soc code="49-3051" key="SOC2010.49-3051" title="Motorboat Mechanics and Service Technicians" />
	<soc code="49-3052" key="SOC2010.49-3052" title="Motorcycle Mechanics" />
	<soc code="49-3053" key="SOC2010.49-3053" title="Outdoor Power Equipment and Other Small Engine Mechanics" />
	<soc code="49-3091" key="SOC2010.49-3091" title="Bicycle Repairers" />
	<soc code="49-3092" key="SOC2010.49-3092" title="Recreational Vehicle Service Technicians" />
	<soc code="49-3093" key="SOC2010.49-3093" title="Tire Repairers and Changers" />
	<soc code="49-9011" key="SOC2010.49-9011" title="Mechanical Door Repairers" />
	<soc code="49-9012" key="SOC2010.49-9012" title="Control and Valve Installers and Repairers, Except Mechanical Door" />
	<soc code="49-9021" key="SOC2010.49-9021" title="Heating, Air Conditioning, and Refrigeration Mechanics and Installers" />
	<soc code="49-9031" key="SOC2010.49-9031" title="Home Appliance Repairers" />
	<soc code="49-9041" key="SOC2010.49-9041" title="Industrial Machinery Mechanics" />
	<soc code="49-9043" key="SOC2010.49-9043" title="Maintenance Workers, Machinery" />
	<soc code="49-9044" key="SOC2010.49-9044" title="Millwrights" />
	<soc code="49-9045" key="SOC2010.49-9045" title="Refractory Materials Repairers, Except Brickmasons" />
	<soc code="49-9051" key="SOC2010.49-9051" title="Electrical Power-Line Installers and Repairers" />
	<soc code="49-9052" key="SOC2010.49-9052" title="Telecommunications Line Installers and Repairers" />
	<soc code="49-9061" key="SOC2010.49-9061" title="Camera and Photographic Equipment Repairers" />
	<soc code="49-9062" key="SOC2010.49-9062" title="Medical Equipment Repairers" />
	<soc code="49-9063" key="SOC2010.49-9063" title="Musical Instrument Repairers and Tuners" />
	<soc code="49-9064" key="SOC2010.49-9064" title="Watch Repairers" />
	<soc code="49-9069" key="SOC2010.49-9069" title="Precision Instrument and Equipment Repairers, All Other" />
	<soc code="49-9071" key="SOC2010.49-9071" title="Maintenance and Repair Workers, General" />
	<soc code="49-9081" key="SOC2010.49-9081" title="Wind Turbine Service Technicians" />
	<soc code="49-9091" key="SOC2010.49-9091" title="Coin, Vending, and Amusement Machine Servicers and Repairers" />
	<soc code="49-9092" key="SOC2010.49-9092" title="Commercial Divers" />
	<soc code="49-9093" key="SOC2010.49-9093" title="Fabric Menders, Except Garment" />
	<soc code="49-9094" key="SOC2010.49-9094" title="Locksmiths and Safe Repairers" />
	<soc code="49-9095" key="SOC2010.49-9095" title="Manufactured Building and Mobile Home Installers" />
	<soc code="49-9096" key="SOC2010.49-9096" title="Riggers" />
	<soc code="49-9097" key="SOC2010.49-9097" title="Signal and Track Switch Repairers" />
	<soc code="49-9098" key="SOC2010.49-9098" title="Helpers--Installation, Maintenance, and Repair Workers" />
	<soc code="49-9099" key="SOC2010.49-9099" title="Installation, Maintenance, and Repair Workers, All Other" />
	<soc code="51-1011" key="SOC2010.51-1011" title="First-Line Supervisors of Production and Operating Workers" />
	<soc code="51-2011" key="SOC2010.51-2011" title="Aircraft Structure, Surfaces, Rigging, and Systems Assemblers" />
	<soc code="51-2021" key="SOC2010.51-2021" title="Coil Winders, Tapers, and Finishers" />
	<soc code="51-2022" key="SOC2010.51-2022" title="Electrical and Electronic Equipment Assemblers" />
	<soc code="51-2023" key="SOC2010.51-2023" title="Electromechanical Equipment Assemblers" />
	<soc code="51-2031" key="SOC2010.51-2031" title="Engine and Other Machine Assemblers" />
	<soc code="51-2041" key="SOC2010.51-2041" title="Structural Metal Fabricators and Fitters" />
	<soc code="51-2091" key="SOC2010.51-2091" title="Fiberglass Laminators and Fabricators" />
	<soc code="51-2092" key="SOC2010.51-2092" title="Team Assemblers" />
	<soc code="51-2093" key="SOC2010.51-2093" title="Timing Device Assemblers and Adjusters" />
	<soc code="51-2099" key="SOC2010.51-2099" title="Assemblers and Fabricators, All Other" />
	<soc code="51-3011" key="SOC2010.51-3011" title="Bakers" />
	<soc code="51-3021" key="SOC2010.51-3021" title="Butchers and Meat Cutters" />
	<soc code="51-3022" key="SOC2010.51-3022" title="Meat, Poultry, and Fish Cutters and Trimmers" />
	<soc code="51-3023" key="SOC2010.51-3023" title="Slaughterers and Meat Packers" />
	<soc code="51-3091" key="SOC2010.51-3091" title="Food and Tobacco Roasting, Baking, and Drying Machine Operators and Tenders" />
	<soc code="51-3092" key="SOC2010.51-3092" title="Food Batchmakers" />
	<soc code="51-3093" key="SOC2010.51-3093" title="Food Cooking Machine Operators and Tenders" />
	<soc code="51-3099" key="SOC2010.51-3099" title="Food Processing Workers, All Other" />
	<soc code="51-4011" key="SOC2010.51-4011" title="Computer-Controlled Machine Tool Operators, Metal and Plastic" />
	<soc code="51-4012" key="SOC2010.51-4012" title="Computer Numerically Controlled Machine Tool Programmers, Metal and Plastic" />
	<soc code="51-4021" key="SOC2010.51-4021" title="Extruding and Drawing Machine Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4022" key="SOC2010.51-4022" title="Forging Machine Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4023" key="SOC2010.51-4023" title="Rolling Machine Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4031" key="SOC2010.51-4031" title="Cutting, Punching, and Press Machine Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4032" key="SOC2010.51-4032" title="Drilling and Boring Machine Tool Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4033" key="SOC2010.51-4033" title="Grinding, Lapping, Polishing, and Buffing Machine Tool Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4034" key="SOC2010.51-4034" title="Lathe and Turning Machine Tool Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4035" key="SOC2010.51-4035" title="Milling and Planing Machine Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4041" key="SOC2010.51-4041" title="Machinists" />
	<soc code="51-4051" key="SOC2010.51-4051" title="Metal-Refining Furnace Operators and Tenders" />
	<soc code="51-4052" key="SOC2010.51-4052" title="Pourers and Casters, Metal" />
	<soc code="51-4061" key="SOC2010.51-4061" title="Model Makers, Metal and Plastic" />
	<soc code="51-4062" key="SOC2010.51-4062" title="Patternmakers, Metal and Plastic" />
	<soc code="51-4071" key="SOC2010.51-4071" title="Foundry Mold and Coremakers" />
	<soc code="51-4072" key="SOC2010.51-4072" title="Molding, Coremaking, and Casting Machine Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4081" key="SOC2010.51-4081" title="Multiple Machine Tool Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4111" key="SOC2010.51-4111" title="Tool and Die Makers" />
	<soc code="51-4121" key="SOC2010.51-4121" title="Welders, Cutters, Solderers, and Brazers" />
	<soc code="51-4122" key="SOC2010.51-4122" title="Welding, Soldering, and Brazing Machine Setters, Operators, and Tenders" />
	<soc code="51-4191" key="SOC2010.51-4191" title="Heat Treating Equipment Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4192" key="SOC2010.51-4192" title="Layout Workers, Metal and Plastic" />
	<soc code="51-4193" key="SOC2010.51-4193" title="Plating and Coating Machine Setters, Operators, and Tenders, Metal and Plastic" />
	<soc code="51-4194" key="SOC2010.51-4194" title="Tool Grinders, Filers, and Sharpeners" />
	<soc code="51-4199" key="SOC2010.51-4199" title="Metal Workers and Plastic Workers, All Other" />
	<soc code="51-5111" key="SOC2010.51-5111" title="Prepress Technician and Workers" />
	<soc code="51-5112" key="SOC2010.51-5112" title="Printing Press Operators" />
	<soc code="51-5113" key="SOC2010.51-5113" title="Print Binding and Finishing Workers" />
	<soc code="51-6011" key="SOC2010.51-6011" title="Laundry and Dry-Cleaning Workers" />
	<soc code="51-6021" key="SOC2010.51-6021" title="Pressers, Textile, Garment, and Related Materials" />
	<soc code="51-6031" key="SOC2010.51-6031" title="Sewing Machine Operators" />
	<soc code="51-6041" key="SOC2010.51-6041" title="Shoe and Leather Workers and Repairers" />
	<soc code="51-6042" key="SOC2010.51-6042" title="Shoe Machine Operators and Tenders" />
	<soc code="51-6051" key="SOC2010.51-6051" title="Sewers, Hand" />
	<soc code="51-6052" key="SOC2010.51-6052" title="Tailors, Dressmakers, and Custom Sewers" />
	<soc code="51-6061" key="SOC2010.51-6061" title="Textile Bleaching and Dyeing Machine Operators and Tenders" />
	<soc code="51-6062" key="SOC2010.51-6062" title="Textile Cutting Machine Setters, Operators, and Tenders" />
	<soc code="51-6063" key="SOC2010.51-6063" title="Textile Knitting and Weaving Machine Setters, Operators, and Tenders" />
	<soc code="51-6064" key="SOC2010.51-6064" title="Textile Winding, Twisting, and Drawing Out Machine Setters, Operators, and Tenders" />
	<soc code="51-6091" key="SOC2010.51-6091" title="Extruding and Forming Machine Setters, Operators, and Tenders, Synthetic and Glass Fibers" />
	<soc code="51-6092" key="SOC2010.51-6092" title="Fabric and Apparel Patternmakers" />
	<soc code="51-6093" key="SOC2010.51-6093" title="Upholsterers" />
	<soc code="51-6099" key="SOC2010.51-6099" title="Textile, Apparel, and Furnishings Workers, All Other" />
	<soc code="51-7011" key="SOC2010.51-7011" title="Cabinetmakers and Bench Carpenters" />
	<soc code="51-7021" key="SOC2010.51-7021" title="Furniture Finishers" />
	<soc code="51-7031" key="SOC2010.51-7031" title="Model Makers, Wood" />
	<soc code="51-7032" key="SOC2010.51-7032" title="Patternmakers, Wood" />
	<soc code="51-7041" key="SOC2010.51-7041" title="Sawing Machine Setters, Operators, and Tenders, Wood" />
	<soc code="51-7042" key="SOC2010.51-7042" title="Woodworking Machine Setters, Operators, and Tenders, Except Sawing" />
	<soc code="51-7099" key="SOC2010.51-7099" title="Woodworkers, All Other" />
	<soc code="51-8011" key="SOC2010.51-8011" title="Nuclear Power Reactor Operators" />
	<soc code="51-8012" key="SOC2010.51-8012" title="Power Distributors and Dispatchers" />
	<soc code="51-8013" key="SOC2010.51-8013" title="Power Plant Operators" />
	<soc code="51-8021" key="SOC2010.51-8021" title="Stationary Engineers and Boiler Operators" />
	<soc code="51-8031" key="SOC2010.51-8031" title="Water and Wastewater Treatment Plant and System Operators" />
	<soc code="51-8091" key="SOC2010.51-8091" title="Chemical Plant and System Operators" />
	<soc code="51-8092" key="SOC2010.51-8092" title="Gas Plant Operators" />
	<soc code="51-8093" key="SOC2010.51-8093" title="Petroleum Pump System Operators, Refinery Operators, and Gaugers" />
	<soc code="51-8099" key="SOC2010.51-8099" title="Plant and System Operators, All Other" />
	<soc code="51-9011" key="SOC2010.51-9011" title="Chemical Equipment Operators and Tenders" />
	<soc code="51-9012" key="SOC2010.51-9012" title="Separating, Filtering, Clarifying, Precipitating, and Still Machine Setters, Operators, and Tenders" />
	<soc code="51-9021" key="SOC2010.51-9021" title="Crushing, Grinding, and Polishing Machine Setters, Operators, and Tenders" />
	<soc code="51-9022" key="SOC2010.51-9022" title="Grinding and Polishing Workers, Hand" />
	<soc code="51-9023" key="SOC2010.51-9023" title="Mixing and Blending Machine Setters, Operators, and Tenders" />
	<soc code="51-9031" key="SOC2010.51-9031" title="Cutters and Trimmers, Hand" />
	<soc code="51-9032" key="SOC2010.51-9032" title="Cutting and Slicing Machine Setters, Operators, and Tenders" />
	<soc code="51-9041" key="SOC2010.51-9041" title="Extruding, Forming, Pressing, and Compacting Machine Setters, Operators, and Tenders" />
	<soc code="51-9051" key="SOC2010.51-9051" title="Furnace, Kiln, Oven, Drier, and Kettle Operators and Tenders" />
	<soc code="51-9061" key="SOC2010.51-9061" title="Inspectors, Testers, Sorters, Samplers, and Weighers" />
	<soc code="51-9071" key="SOC2010.51-9071" title="Jewelers and Precious Stone and Metal Workers" />
	<soc code="51-9081" key="SOC2010.51-9081" title="Dental Laboratory Technicians" />
	<soc code="51-9082" key="SOC2010.51-9082" title="Medical Appliance Technicians" />
	<soc code="51-9083" key="SOC2010.51-9083" title="Ophthalmic Laboratory Technicians" />
	<soc code="51-9111" key="SOC2010.51-9111" title="Packaging and Filling Machine Operators and Tenders" />
	<soc code="51-9121" key="SOC2010.51-9121" title="Coating, Painting, and Spraying Machine Setters, Operators, and Tenders" />
	<soc code="51-9122" key="SOC2010.51-9122" title="Painters, Transportation Equipment" />
	<soc code="51-9123" key="SOC2010.51-9123" title="Painting, Coating, and Decorating Workers" />
	<soc code="51-9141" key="SOC2010.51-9141" title="Semiconductor Processors" />
	<soc code="51-9151" key="SOC2010.51-9151" title="Photographic Process Workers and Processing Machine Operators" />
	<soc code="51-9191" key="SOC2010.51-9191" title="Adhesive Bonding Machine Operators and Tenders" />
	<soc code="51-9192" key="SOC2010.51-9192" title="Cleaning, Washing, and Metal Pickling Equipment Operators and Tenders" />
	<soc code="51-9193" key="SOC2010.51-9193" title="Cooling and Freezing Equipment Operators and Tenders" />
	<soc code="51-9194" key="SOC2010.51-9194" title="Etchers and Engravers" />
	<soc code="51-9195" key="SOC2010.51-9195" title="Molders, Shapers, and Casters, Except Metal and Plastic" />
	<soc code="51-9196" key="SOC2010.51-9196" title="Paper Goods Machine Setters, Operators, and Tenders" />
	<soc code="51-9197" key="SOC2010.51-9197" title="Tire Builders" />
	<soc code="51-9198" key="SOC2010.51-9198" title="Helpers--Production Workers" />
	<soc code="51-9199" key="SOC2010.51-9199" title="Production Workers, All Other" />
	<soc code="53-1011" key="SOC2010.53-1011" title="Aircraft Cargo Handling Supervisors" />
	<soc code="53-1021" key="SOC2010.53-1021" title="First-Line Supervisors of Helpers, Laborers, and Material Movers, Hand" />
	<soc code="53-1031" key="SOC2010.53-1031" title="First-Line Supervisors of Transportation and Material-Moving Machine and Vehicle Operators" />
	<soc code="53-2011" key="SOC2010.53-2011" title="Airline Pilots, Copilots, and Flight Engineers" />
	<soc code="53-2012" key="SOC2010.53-2012" title="Commercial Pilots" />
	<soc code="53-2021" key="SOC2010.53-2021" title="Air Traffic Controllers" />
	<soc code="53-2022" key="SOC2010.53-2022" title="Airfield Operations Specialists" />
	<soc code="53-2031" key="SOC2010.53-2031" title="Flight Attendants" />
	<soc code="53-3011" key="SOC2010.53-3011" title="Ambulance Drivers and Attendants, Except Emergency Medical Technicians" />
	<soc code="53-3021" key="SOC2010.53-3021" title="Bus Drivers, Transit and Intercity" />
	<soc code="53-3022" key="SOC2010.53-3022" title="Bus Drivers, School or Special Client" />
	<soc code="53-3031" key="SOC2010.53-3031" title="Driver/Sales Workers" />
	<soc code="53-3032" key="SOC2010.53-3032" title="Heavy and Tractor-Trailer Truck Drivers" />
	<soc code="53-3033" key="SOC2010.53-3033" title="Light Truck or Delivery Services Drivers" />
	<soc code="53-3041" key="SOC2010.53-3041" title="Taxi Drivers and Chauffeurs" />
	<soc code="53-3099" key="SOC2010.53-3099" title="Motor Vehicle Operators, All Other" />
	<soc code="53-4011" key="SOC2010.53-4011" title="Locomotive Engineers" />
	<soc code="53-4012" key="SOC2010.53-4012" title="Locomotive Firers" />
	<soc code="53-4013" key="SOC2010.53-4013" title="Rail Yard Engineers, Dinkey Operators, and Hostlers" />
	<soc code="53-4021" key="SOC2010.53-4021" title="Railroad Brake, Signal, and Switch Operators" />
	<soc code="53-4031" key="SOC2010.53-4031" title="Railroad Conductors and Yardmasters" />
	<soc code="53-4041" key="SOC2010.53-4041" title="Subway and Streetcar Operators" />
	<soc code="53-4099" key="SOC2010.53-4099" title="Rail Transportation Workers, All Other" />
	<soc code="53-5011" key="SOC2010.53-5011" title="Sailors and Marine Oilers" />
	<soc code="53-5021" key="SOC2010.53-5021" title="Captains, Mates, and Pilots of Water Vessels" />
	<soc code="53-5022" key="SOC2010.53-5022" title="Motorboat Operators" />
	<soc code="53-5031" key="SOC2010.53-5031" title="Ship Engineers" />
	<soc code="53-6011" key="SOC2010.53-6011" title="Bridge and Lock Tenders" />
	<soc code="53-6021" key="SOC2010.53-6021" title="Parking Lot Attendants" />
	<soc code="53-6031" key="SOC2010.53-6031" title="Automotive and Watercraft Service Attendants" />
	<soc code="53-6041" key="SOC2010.53-6041" title="Traffic Technicians" />
	<soc code="53-6051" key="SOC2010.53-6051" title="Transportation Inspectors" />
	<soc code="53-6061" key="SOC2010.53-6061" title="Transportation Attendants, Except Flight Attendants" />
	<soc code="53-6099" key="SOC2010.53-6099" title="Transportation Workers, All Other" />
	<soc code="53-7011" key="SOC2010.53-7011" title="Conveyor Operators and Tenders" />
	<soc code="53-7021" key="SOC2010.53-7021" title="Crane and Tower Operators" />
	<soc code="53-7031" key="SOC2010.53-7031" title="Dredge Operators" />
	<soc code="53-7032" key="SOC2010.53-7032" title="Excavating and Loading Machine and Dragline Operators" />
	<soc code="53-7033" key="SOC2010.53-7033" title="Loading Machine Operators, Underground Mining" />
	<soc code="53-7041" key="SOC2010.53-7041" title="Hoist and Winch Operators" />
	<soc code="53-7051" key="SOC2010.53-7051" title="Industrial Truck and Tractor Operators" />
	<soc code="53-7061" key="SOC2010.53-7061" title="Cleaners of Vehicles and Equipment" />
	<soc code="53-7062" key="SOC2010.53-7062" title="Laborers and Freight, Stock, and Material Movers, Hand" />
	<soc code="53-7063" key="SOC2010.53-7063" title="Machine Feeders and Offbearers" />
	<soc code="53-7064" key="SOC2010.53-7064" title="Packers and Packagers, Hand" />
	<soc code="53-7071" key="SOC2010.53-7071" title="Gas Compressor and Gas Pumping Station Operators" />
	<soc code="53-7072" key="SOC2010.53-7072" title="Pump Operators, Except Wellhead Pumpers" />
	<soc code="53-7073" key="SOC2010.53-7073" title="Wellhead Pumpers" />
	<soc code="53-7081" key="SOC2010.53-7081" title="Refuse and Recyclable Material Collectors" />
	<soc code="53-7111" key="SOC2010.53-7111" title="Mine Shuttle Car Operators" />
	<soc code="53-7121" key="SOC2010.53-7121" title="Tank Car, Truck, and Ship Loaders" />
	<soc code="53-7199" key="SOC2010.53-7199" title="Material Moving Workers, All Other" />
	<soc code="55-1011" key="SOC2010.55-1011" title="Air Crew Officers" />
	<soc code="55-1012" key="SOC2010.55-1012" title="Aircraft Launch and Recovery Officers" />
	<soc code="55-1013" key="SOC2010.55-1013" title="Armored Assault Vehicle Officers" />
	<soc code="55-1014" key="SOC2010.55-1014" title="Artillery and Missile Officers" />
	<soc code="55-1015" key="SOC2010.55-1015" title="Command and Control Center Officers" />
	<soc code="55-1016" key="SOC2010.55-1016" title="Infantry Officers" />
	<soc code="55-1017" key="SOC2010.55-1017" title="Special Forces Officers" />
	<soc code="55-1019" key="SOC2010.55-1019" title="Military Officer Special and Tactical Operations Leaders, All Other" />
	<soc code="55-2011" key="SOC2010.55-2011" title="First-Line Supervisors of Air Crew Members" />
	<soc code="55-2012" key="SOC2010.55-2012" title="First-Line Supervisors of Weapons Specialists/Crew Members" />
	<soc code="55-2013" key="SOC2010.55-2013" title="First-Line Supervisors of All Other Tactical Operations Specialists" />
	<soc code="55-3011" key="SOC2010.55-3011" title="Air Crew Members" />
	<soc code="55-3012" key="SOC2010.55-3012" title="Aircraft Launch and Recovery Specialists" />
	<soc code="55-3013" key="SOC2010.55-3013" title="Armored Assault Vehicle Crew Members" />
	<soc code="55-3014" key="SOC2010.55-3014" title="Artillery and Missile Crew Members" />
	<soc code="55-3015" key="SOC2010.55-3015" title="Command and Control Center Specialists" />
	<soc code="55-3016" key="SOC2010.55-3016" title="Infantry" />
	<soc code="55-3017" key="SOC2010.55-3017" title="Radar and Sonar Technicians" />
	<soc code="55-3018" key="SOC2010.55-3018" title="Special Forces" />
	<soc code="55-3019" key="SOC2010.55-3019" title="Military Enlisted Tactical Operations and Air/Weapons Specialists and Crew Members, All Other" />
</socs>'

INSERT INTO @SOC 
(
	SOC2010Code, 
	[Key], 
	Title
)
SELECT 
	S.c.value('@code', 'NVARCHAR(10)'),
	S.c.value('@key', 'NVARCHAR(200)'),
	S.c.value('@title', 'NVARCHAR(255)')
FROM   
	@SOCXML.nodes('/socs/soc') S(c)
	
INSERT INTO [Library.SOC]
(
	SOC2010Code, 
	[Key], 
	Title
)
SELECT
	TS.SOC2010Code, 
	TS.[Key], 
	TS.Title
FROM 
	@SOC TS
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Library.SOC] S
		WHERE
			S.SOC2010Code = TS.SOC2010Code
	)
	
UPDATE
	S
SET
	[Key] = TS.[Key],
	S.Title = TS.Title
FROM
	[Library.SOC] S
INNER JOIN @SOC TS
	ON TS.SOC2010Code = S.SOC2010Code
WHERE
	S.[Key] <> TS.[Key]
	OR S.Title <> TS.Title
GO